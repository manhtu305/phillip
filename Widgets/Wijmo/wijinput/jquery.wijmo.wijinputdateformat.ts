﻿/// <reference path="../External/declarations/jquery.d.ts"/>
/// <reference path="../External/declarations/globalize.d.ts"/>
/// <reference path="../wijutil/jquery.wijmo.wijutil.ts"/>

/*
 * Depends:
 *	jquery-1.11.1.js
 *	globalize.js
 *  jquery.wijmo.wijutil.js
 *
 */
module wijmo.input {


	/** @ignore */
	export function paddingZero(val, aCount = 2) {
		var text = '' + val + '';
		if (text.length > aCount) {
			text = text.substr(text.length - aCount);
		} else while (text.length < aCount) {
			text = '0' + text;
		}
		return text;
	}

	export enum DescriptorType {
		liternal = -1,


		OneDigitYear = 1,
		TwoDigitYear = 2,
		FourDigitYear = 10,

		TwoDigitMonth = 20,
		Month = 25,
		AbbreviatedMonthNames = 26,
		MonthNames = 27,

		EraYear = 70,
		TwoEraYear = 71,
		EraName = 72,
		TwoEraName = 73,
		ThreeEraName = 74,
		EraYearBig = 75,

		AD = 80,

		TwoDigityDayOfMonth = 30,
		DayOfMonth = 31,
		AbbreviatedDayNames = 100,
		DayNames = 101,

		h = 45,
		hh = 46,
		H = 47,
		HH = 48,

		ShortAmPm = 250,
		AmPm = 251,

		mm = 50,
		m = 51,

		ss = 60,
		s = 61
	}

	////////////////////////////////////////////////////////////////////////////////
	// _iDateDescriptor

	/** @ignore */
	export class _iDateDescriptor {
		_txtProvider: wijDateTextFormatter;
		startIndex = 0;
		name: string = null;
		type = DescriptorType.liternal;
		maxLen = 2;
		formatString = "";

		constructor(textProvider: wijDateTextFormatter, public id: number) {
			this._txtProvider = textProvider;
			this.startIndex = 0;
		}

		getText() {
			return null;
		}

		getRealText(text) {
			var opt = this._txtProvider.inputWidget.options;
			if (opt.date == null) {
				if (opt.promptChar !== '') {
					var tmp = '';
					for (var i = 0; i < this.formatString.length; i++) {
						tmp += opt.promptChar;
					}
					return tmp;
				}
				return this.formatString;
			}
			return text;
		}

		setText(value: string, allowchangeotherpart, result, isSmartInputMode?: boolean) {
			return false;
		}

		inc() { }
		dec() { }
		needAdjustInsertPos() {
			return true;
		}
		reachMaxLen() {
			var t = this.getText();
			do {
				if (t.charAt(0) === '0') {
					t = t.slice(1);
				} else {
					break;
				}
			} while (t.length > 0);
			return t.length >= this.maxLen;
        }

        _isForceMonth() {
            return this._txtProvider.inputWidget && this._txtProvider.inputWidget._isForceMonth && this._txtProvider.inputWidget._isForceMonth();
        }
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor

	class _dateDescriptor extends _iDateDescriptor {
		liternal = '';
		maxLen = 100;

		constructor(owner, id) { super(owner, id); }

		getText() {
			return this.liternal;
		}
	}

	class EraYearDescriptor extends _iDateDescriptor {
		constructor(owner, id) { super(owner, id); }

		inc() {
			var date = this._txtProvider.getDate();
			var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
			var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();

			if (DateTimeInfo.Equal(date, maxDate)) {
				if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
					this._txtProvider.setDate(new Date(minDate.valueOf()));
				}
			}
			else {
				var increment = this._txtProvider.inputWidget._getInnerIncrement()
				this._txtProvider.setYear(this._txtProvider.getYear() + increment, null, true);
				if (this._txtProvider.getDate() > maxDate) {
					this._txtProvider.setDate(new Date(maxDate.valueOf()));
				}
			}
		}

		dec() {
			var date = this._txtProvider.getDate();
			var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
			var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();

			if (DateTimeInfo.Equal(date, minDate)) {
				if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
					this._txtProvider.setDate(new Date(maxDate.valueOf()));
				}
			}
			else {
				var increment = this._txtProvider.inputWidget._getInnerIncrement()
				this._txtProvider.setYear(this._txtProvider.getYear() - increment, null, true);
				if (this._txtProvider.getDate() < minDate) {
					this._txtProvider.setDate(new Date(minDate.valueOf()));
				}
			}
		}
	}
	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor70
	class _dateDescriptor70 extends EraYearDescriptor {
		name = 'Era Year';
		formatString = "e";
		type = DescriptorType.EraYear;

		constructor(owner, id) { super(owner, id); }

		getText() {
			var eraDate = DateTimeInfo.GetEraDate(this._txtProvider.getDate());
			if (eraDate.eraYear === -1) {
				return "";
			}
			eraDate.eraYear = eraDate.eraYear > 99 ? 99 : eraDate.eraYear;
			return this.getRealText(String(eraDate.eraYear));
		}

		setText(value, allowchangeotherpart, result) {
			if (isNaN(parseInt(value))) {
				return false;
			}

			var date = this._txtProvider.getDate();
			var eraDate = DateTimeInfo.GetEraDate(date);
			if (eraDate.eraYear !== -1) {
				var eraYear = parseInt(value);
				if (eraYear >= DateTimeInfo.GetEraYears()[eraDate.era]) {
					eraYear = DateTimeInfo.GetEraYears()[eraDate.era];
				}

				var nullFlag = this._txtProvider.inputWidget.options.date == null;
				if (nullFlag) {
					eraYear = parseInt(value);
                }

                if (eraYear == 0) {
                    return true;
                }

				var newDate = DateTimeInfo.ConvertToGregorianDate(eraDate.era, eraYear, date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), false);

				if (eraDate.era < DateTimeInfo.GetEraCount() - 1) {
					var maxEraDate = DateTimeInfo.GetEraDates()[eraDate.era + 1];
					if (newDate > maxEraDate) {
						eraYear = DateTimeInfo.GetEraYears()[eraDate.era] - 1;
						newDate = DateTimeInfo.ConvertToGregorianDate(eraDate.era, eraYear, date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), false);
					}
                }

                if (eraYear == 1 && eraDate.era >= 0 && eraDate.era < DateTimeInfo.GetEraCount()) {
                    var minEraDate = DateTimeInfo.GetEraDates()[eraDate.era];
                    if (newDate < minEraDate) {
                        newDate = DateTimeInfo.ConvertToGregorianDate(eraDate.era, eraYear, minEraDate.getMonth() + 1, minEraDate.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), false);
                    }
                }

				this._txtProvider.setDate(newDate);
				return true;
			}

			return false;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor71
	class _dateDescriptor71 extends EraYearDescriptor {
		name = 'Two Era Year';
		formatString = "ee";
		type = DescriptorType.TwoEraYear;

		constructor(owner, id) { super(owner, id); }

		getText() {
			var eraDate = DateTimeInfo.GetEraDate(this._txtProvider.getDate());
			if (eraDate.eraYear === -1) {
				return "";
			}

			eraDate.eraYear = eraDate.eraYear > 99 ? 99 : eraDate.eraYear;
			return this.getRealText(paddingZero(eraDate.eraYear, 2));
		}

		setText(value, allowchangeotherpart, result) {
			if (isNaN(parseInt(value))) {
				return false;
			}

			var date = this._txtProvider.getDate();
			var eraDate = DateTimeInfo.GetEraDate(date);
			if (eraDate.eraYear !== -1) {
				var newValue = String(eraDate.eraYear) + value;
				var eraYear = parseInt(newValue.substring(newValue.length - 2, newValue.length));
				if (eraYear > DateTimeInfo.GetEraYears()[eraDate.era]) {
					eraYear = DateTimeInfo.GetEraYears()[eraDate.era];
				}

				var nullFlag = this._txtProvider.inputWidget.options.date == null;
				if (nullFlag) {
					eraYear = parseInt(value);
                }

                if (eraYear == 0) {
                    return true;
                }

				var newDate = DateTimeInfo.ConvertToGregorianDate(eraDate.era, eraYear, date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), false);
				if (eraDate.era < DateTimeInfo.GetEraCount() - 1) {
					var maxEraDate = DateTimeInfo.GetEraDates()[eraDate.era + 1];
					if (newDate > maxEraDate) {
						eraYear = DateTimeInfo.GetEraYears()[eraDate.era] - 1;
						newDate = DateTimeInfo.ConvertToGregorianDate(eraDate.era, eraYear, date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), false);
					}
                }

                if (eraYear == 1 && eraDate.era >= 0 && eraDate.era < DateTimeInfo.GetEraCount()) {
                    var minEraDate = DateTimeInfo.GetEraDates()[eraDate.era];
                    if (newDate < minEraDate) {
                        newDate = DateTimeInfo.ConvertToGregorianDate(eraDate.era, eraYear, minEraDate.getMonth() + 1, minEraDate.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), false);
                    }
                }

				this._txtProvider.setDate(newDate);
				return true;
			}

			return false;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor75
	class _dateDescriptor75 extends _dateDescriptor70 {
		name = 'Era Year Big';
		formatString = "E";
		type = DescriptorType.EraYearBig;
		constructor(owner, id) { super(owner, id); }
		getText() {
			var result = super.getText();
			return result == 1 ? "\u5143" : result;
		}
	}

	class EraNameDescriptor extends _iDateDescriptor {
		constructor(owner, id) { super(owner, id); }

		inc() {
			var date = this._txtProvider.getDate();
			var eraDate = DateTimeInfo.GetEraDate(date);
			if (eraDate.era === -1) {
				return;
			}

			var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
			var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();

			if (DateTimeInfo.Equal(date, maxDate)) {
				if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
					this._txtProvider.setDate(new Date(minDate.valueOf()));
				}
			}
			else {
				if (eraDate.era >= DateTimeInfo.GetEraCount() - 1) {
					this._txtProvider.setDate(new Date(maxDate.valueOf()));
				}
				else {
					var era = eraDate.era + this._txtProvider.inputWidget._getInnerIncrement();
					era = era > DateTimeInfo.GetEraCount() - 1 ? DateTimeInfo.GetEraCount() - 1 : era;

					var eraYear = eraDate.eraYear > DateTimeInfo.GetEraYears()[era] ? DateTimeInfo.GetEraYears()[era] : eraDate.eraYear;
					var newDate = DateTimeInfo.ConvertToGregorianDate(era, eraYear, date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), true);
					newDate = newDate == null ? DateTimeInfo.GetEraDates()[era] : newDate;

					if (newDate > maxDate) {
						this._txtProvider.setDate(new Date(maxDate.valueOf()));
					}
					else {
						this._txtProvider.setDate(newDate);
					}
				}
			}
		}

		dec() {
			var date = this._txtProvider.getDate();
			var eraDate = DateTimeInfo.GetEraDate(date);
			if (eraDate.era === -1) {
				return;
			}

			var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
			var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();

			if (DateTimeInfo.Equal(date, minDate)) {
				if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
					this._txtProvider.setDate(new Date(maxDate.valueOf()));
				}
			}
			else {
				if (eraDate.era == 0) {
					this._txtProvider.setDate(new Date(minDate.valueOf()));
				}
				else {
					var era = eraDate.era - this._txtProvider.inputWidget._getInnerIncrement();
					era = era < 0 ? 0 : era;

					var eraYear = eraDate.eraYear > DateTimeInfo.GetEraYears()[era] ? DateTimeInfo.GetEraYears()[era] : eraDate.eraYear;
					var newDate = DateTimeInfo.ConvertToGregorianDate(era, eraYear, date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), true);

					if (newDate == null) {
						newDate = new Date(DateTimeInfo.GetEraDates()[era + 1]);;
						newDate.setDate(newDate.getDate() - 1);
					}

					if (newDate > maxDate) {
						this._txtProvider.setDate(new Date(minDate.valueOf()));
					}
					else {
						this._txtProvider.setDate(newDate);
					}
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor72
	class _dateDescriptor72 extends EraNameDescriptor {
		name = 'Era Name';
		formatString = "g";
		type = DescriptorType.EraName;

		constructor(owner, id) { super(owner, id); }

		getText() {
			if (DateTimeInfo.GetEraCount() === 1) {
				return DateTimeInfo.GetEraSymbols()[0];
			}

			var eraDate = DateTimeInfo.GetEraDate(this._txtProvider.getDate());
			if (eraDate.era === -1) {
				return "";
			}

			return this.getRealText(DateTimeInfo.GetEraSymbols()[eraDate.era]);
		}

		setText(value, allowchangeotherpart, result) {
			var singleValue = value.substr(value.length - 1, 1);
			var era = 0;
			for (era = 0; era < DateTimeInfo.GetEraCount(); era++) {
				if ((singleValue.toLowerCase() === DateTimeInfo.GetEraShortcuts()[era].toLowerCase()) ||
					(value.toLowerCase() === DateTimeInfo.GetEraShortNames()[era].toLowerCase())) {
					break;
				}
			}

			if (era == DateTimeInfo.GetEraCount()) {
				return true;
			}

			var date = this._txtProvider.getDate();
			var eraDate = DateTimeInfo.GetEraDate(date);
			if (eraDate.era === -1) {
				return true;
			}

			var eraYear = eraDate.eraYear > DateTimeInfo.GetEraYears()[era] ? DateTimeInfo.GetEraYears()[era] : eraDate.eraYear;
			var newDate = DateTimeInfo.ConvertToGregorianDate(era, eraYear, date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), true);
			newDate = newDate == null ? DateTimeInfo.GetEraDates()[era] : newDate;

			if (era < DateTimeInfo.GetEraCount() - 1) {
				var maxEraDate = DateTimeInfo.GetEraDates()[era + 1];
				newDate = newDate > maxEraDate ? maxEraDate : newDate;
			}

			this._txtProvider.setDate(newDate);
			return true;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor73
	class _dateDescriptor73 extends EraNameDescriptor {
		name = 'Two Era Name';
		formatString = "gg";
		type = DescriptorType.TwoEraName;

		constructor(owner, id) { super(owner, id); }

		getText() {
			if (DateTimeInfo.GetEraCount() === 1) {
				return DateTimeInfo.GetEraAbbreviations()[0];
			}

			var eraDate = DateTimeInfo.GetEraDate(this._txtProvider.getDate());
			if (eraDate.era === -1) {
				return "";
			}
			return this.getRealText(DateTimeInfo.GetEraAbbreviations()[eraDate.era]);
		}

		setText(value, allowchangeotherpart, result) {
			var singleValue = value.substr(value.length - 1, 1);
			var era = 0;
			for (era = 0; era < DateTimeInfo.GetEraCount(); era++) {
				if ((singleValue.toLowerCase() === DateTimeInfo.GetEraShortcuts()[era].toLowerCase()) ||
					(value.toLowerCase() === DateTimeInfo.GetEraShortNames()[era].toLowerCase()) ||
					(value.toLowerCase() === DateTimeInfo.GetEraAbbreviations()[era].toLowerCase())) {
					break;
				}
			}

			if (era == DateTimeInfo.GetEraCount()) {
				return true;
			}

			var date = this._txtProvider.getDate();
			var eraDate = DateTimeInfo.GetEraDate(date);
			if (eraDate.era === -1) {
				return true;
			}

			var eraYear = eraDate.eraYear > DateTimeInfo.GetEraYears()[era] ? DateTimeInfo.GetEraYears()[era] : eraDate.eraYear;
			var newDate = DateTimeInfo.ConvertToGregorianDate(era, eraYear, date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), true);
			newDate = newDate == null ? DateTimeInfo.GetEraDates()[era] : newDate;

			if (era < DateTimeInfo.GetEraCount() - 1) {
				var maxEraDate = DateTimeInfo.GetEraDates()[era + 1];
				newDate = newDate > maxEraDate ? maxEraDate : newDate;
			}

			this._txtProvider.setDate(newDate);
			return true;
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor74
	class _dateDescriptor74 extends EraNameDescriptor {
		name = 'Three Era Name';
		formatString = "ggg";
		type = DescriptorType.ThreeEraName;

		constructor(owner, id) { super(owner, id); }

		getText() {
			if (DateTimeInfo.GetEraCount() === 1) {
				return DateTimeInfo.GetEraNames()[0];
			}

			var eraDate = DateTimeInfo.GetEraDate(this._txtProvider.getDate());
			if (eraDate.era === -1) {
				return "";
			}
			return this.getRealText(DateTimeInfo.GetEraNames()[eraDate.era]);
		}

		setText(value, allowchangeotherpart, result) {
			var singleValue = value.substr(value.length - 1, 1);
			var era = 0;
			for (era = 0; era < DateTimeInfo.GetEraCount(); era++) {
				if ((singleValue.toLowerCase() === DateTimeInfo.GetEraShortcuts()[era].toLowerCase()) ||
					(value.toLowerCase() === DateTimeInfo.GetEraShortNames()[era].toLowerCase()) ||
					(value.toLowerCase() === DateTimeInfo.GetEraAbbreviations()[era].toLowerCase()) ||
					(value.toLowerCase() === DateTimeInfo.GetEraNames()[era].toLowerCase())) {
					break;
				}
			}

			if (era == DateTimeInfo.GetEraCount()) {
				return false;
			}

			var date = this._txtProvider.getDate();
			var eraDate = DateTimeInfo.GetEraDate(date);
			if (eraDate.era === -1) {
				return true;
			}

			var eraYear = eraDate.eraYear > DateTimeInfo.GetEraYears()[era] ? DateTimeInfo.GetEraYears()[era] : eraDate.eraYear;
			var newDate = DateTimeInfo.ConvertToGregorianDate(era, eraYear, date.getMonth() + 1, date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), true);
			newDate = newDate == null ? DateTimeInfo.GetEraDates()[era] : newDate;

			if (era < DateTimeInfo.GetEraCount() - 1) {
				var maxEraDate = DateTimeInfo.GetEraDates()[era + 1];
				newDate = newDate > maxEraDate ? maxEraDate : newDate;
			}

			this._txtProvider.setDate(newDate);
			return true;
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor80
	class _dateDescriptor80 extends _iDateDescriptor {
		name = 'AD';
		formatString = "A";
		type = DescriptorType.AD;
		constructor(owner, id) { super(owner, id); }
		getText() {
			return "A.D.";
		}
	}

	class MonthDateDescriptor extends _iDateDescriptor {
		constructor(owner, id) { super(owner, id); }

		inc() {
			super.inc();

			var date = this._txtProvider.getDate();
			var year = date.getFullYear();
			var month = date.getMonth();
			var day = date.getDate();
			var hour = date.getHours();
			var minute = date.getMinutes();
			var second = date.getSeconds();

			var newMonth = month + this._txtProvider.inputWidget._getInnerIncrement();
			var dayCount = DateTimeInfo.DaysInMonth(year, newMonth + 1);
			if (day > dayCount) {
				day = dayCount;
			}

			var newDate = new Date(year, newMonth, day, hour, minute, second);

			if (this._txtProvider._isEraFormatExist()) {
				var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();

				if (newDate >= maxDate) {
					if (!DateTimeInfo.Equal(this._txtProvider.getDate(), maxDate)) {
						this._txtProvider.setDate(maxDate);
					}
					else if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(minDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
			else {
				var minDate = this._txtProvider.inputWidget._getRealMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealMaxDate();

				if (!this._txtProvider.inputWidget._isValidDate(newDate, true)) {
					if (!DateTimeInfo.Equal(this._txtProvider.getDate(), maxDate)) {
						this._txtProvider.setDate(maxDate);
					}
					else if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(minDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
		}

		dec() {
			super.inc();
			var date = this._txtProvider.getDate();
			var year = date.getFullYear();
			var month = date.getMonth();
			var day = date.getDate();
			var hour = date.getHours();
			var minute = date.getMinutes();
			var second = date.getSeconds();

			var newMonth = month - this._txtProvider.inputWidget._getInnerIncrement();
			var dayCount = DateTimeInfo.DaysInMonth(year, newMonth + 1);
			if (day > dayCount) {
				day = dayCount;
			}

			var newDate = new Date(year, newMonth, day, hour, minute, second);
			if (year == 1) {
				newDate.setFullYear(1);
				if (newMonth < 0) {
					newDate.setMonth(newMonth);
				}
			}

			if (this._txtProvider._isEraFormatExist()) {
				var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();

				if (newDate < minDate) {
					if (!DateTimeInfo.Equal(this._txtProvider.getDate(), minDate)) {
						this._txtProvider.setDate(minDate);
					}
					else if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(maxDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
			else {
				var minDate = this._txtProvider.inputWidget._getRealMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealMaxDate();
				if (!this._txtProvider.inputWidget._isValidDate(newDate, true)) {
					if (!DateTimeInfo.Equal(this._txtProvider.getDate(), minDate)) {
						this._txtProvider.setDate(minDate);
					}
					else if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(maxDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
		}

		setText(value, allowchangeotherpart, result) {
			if (parseInt(value) > 12) {
				value = value.substr(value.length - 1, 1);
			}
			var returnResult = this._txtProvider.setMonth(value, allowchangeotherpart, result);
			if (returnResult) {
				if (this._txtProvider._isEraFormatExist()) {
					var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
					var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();
					var date = this._txtProvider.getDate();

					if (date > maxDate) {
						this._txtProvider.setDate(maxDate);
					}
					else if (date < minDate) {
						this._txtProvider.setDate(minDate);
					}
				}
			}

			return returnResult;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor20
	class _dateDescriptor20 extends MonthDateDescriptor {
		name = 'Two-digit month';
		formatString = "MM";
		type = DescriptorType.TwoDigitMonth;
		constructor(owner, id) { super(owner, id); }

		getText() {
			var m = '' + this._txtProvider.getMonth() + '';
			return this.getRealText(m.length === 1 ? ('0' + m) : m);
		}

		setText(value, allowchangeotherpart, result) {
			if (value === "0") {
				return true;
			}

			return super.setText(value, allowchangeotherpart, result);
		}

	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor25
	class _dateDescriptor25 extends MonthDateDescriptor {
		name = 'month';
		formatString = "M";
		type = DescriptorType.Month;
		constructor(owner, id) { super(owner, id); }

		getText() {
			var m = '' + this._txtProvider.getMonth() + '';
			return this.getRealText(m);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor26
	class _dateDescriptor26 extends MonthDateDescriptor {
		name = 'AbbreviatedMonthNames';
		formatString = "MMM";
		type = DescriptorType.AbbreviatedMonthNames;
		maxLen = DescriptorType.AbbreviatedMonthNames;
		constructor(owner, id) { super(owner, id); }

		getText() {
			var m = this._txtProvider.getMonth(),
				culture = this._txtProvider._getCulture();
			return this.getRealText(culture.calendars.standard.months.namesAbbr[m - 1]);
		}

		setText(value, allowchangeotherpart, result) {
			var m = -1,
				cf = this._txtProvider.inputWidget._getCulture().calendars.standard;
			m = this._txtProvider.findAlikeArrayItemIndex(cf.months.namesAbbr, value);
			if (m === -1) {
				return false;
			}
			return this._txtProvider.setMonth(m + 1, allowchangeotherpart, result);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor27
	class _dateDescriptor27 extends MonthDateDescriptor {
		name = 'MonthNames';
		formatString = "MMMM";
		type = DescriptorType.MonthNames;
		maxLen = 100;
		constructor(owner, id) { super(owner, id); }

		getText() {
			var m = this._txtProvider.getMonth(),
				culture = this._txtProvider._getCulture();
			return this.getRealText(culture.calendars.standard.months.names[m - 1]);
		}

		setText(value, allowchangeotherpart, result) {
			var m = -1, culture;
			if (result && result.isfullreset) {
				m = 1;
			}
			else {
				culture = this._txtProvider._getCulture();
				m = this._txtProvider.findAlikeArrayItemIndex(culture
					.calendars.standard.months.names, value);
				if (m === -1) {
					return false;
				}
			}
			return this._txtProvider.setMonth(m + 1, allowchangeotherpart, result);
		}
	}

	class DayOfMonthDescriptor extends _iDateDescriptor {
		constructor(owner, id) { super(owner, id); }

		inc() {
			var newDay = this._txtProvider.getDayOfMonth() + this._txtProvider.inputWidget._getInnerIncrement();
			var newDate = new Date(this._txtProvider.getDate().valueOf());
			newDate.setDate(newDay)
			if (this._txtProvider._isEraFormatExist()) {
				var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();

				if (newDate > maxDate) {
					if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(minDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
			else {
				var minDate = this._txtProvider.inputWidget._getRealMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealMaxDate();

				if (!this._txtProvider.inputWidget._isValidDate(newDate, true)) {
					if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(minDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
		}

		dec() {
			var newDay = this._txtProvider.getDayOfMonth() - this._txtProvider.inputWidget._getInnerIncrement();
			var newDate = new Date(this._txtProvider.getDate().valueOf());
			newDate.setDate(newDay);
			if (this._txtProvider._isEraFormatExist()) {
				var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();

				if (newDate < minDate) {
					if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(maxDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
			else {
				var minDate = this._txtProvider.inputWidget._getRealMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealMaxDate();

				if (!this._txtProvider.inputWidget._isValidDate(newDate, true)) {
					if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(maxDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
		}

		setText(value: string, allowchangeotherpart, result) {
			var date = this._txtProvider.getDate();
			var dayCount = DateTimeInfo.DaysInMonth(date.getFullYear(), date.getMonth() + 1);
			if (parseInt(value) > dayCount) {
				value = dayCount + "";
			}
			var returnResult = this._txtProvider.setDayOfMonth(value, allowchangeotherpart, result);
			if (returnResult) {
				if (this._txtProvider._isEraFormatExist()) {
					var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
					var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();
					var date = this._txtProvider.getDate();

					if (date > maxDate) {
						this._txtProvider.setDate(maxDate);
					}
					else if (date < minDate) {
						this._txtProvider.setDate(minDate);
					}
				}
			}

			return returnResult;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor30
	class _dateDescriptor30 extends DayOfMonthDescriptor {
		name = 'Two-digit day of month';
		formatString = "dd";
		type = DescriptorType.TwoDigityDayOfMonth;
		constructor(owner, id) { super(owner, id); }

		getText() {
			return this.getRealText(paddingZero(this._txtProvider.getDayOfMonth()));
		}

		setText(value: string, allowchangeotherpart, result) {
			if (value === "0") {
				return true;
			}

			return super.setText(value, allowchangeotherpart, result);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor31
	class _dateDescriptor31 extends DayOfMonthDescriptor {
		name = 'Day of month';
		formatString = "d";
		type = DescriptorType.DayOfMonth;
		constructor(owner, id) { super(owner, id); }

		getText() {
			var dom = this._txtProvider.getDayOfMonth();
			return this.getRealText('' + dom + '');
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor100
	class _dateDescriptor100 extends DayOfMonthDescriptor {
		name = 'AbbreviatedDayNames';
		formatString = "ddd";
		type = DescriptorType.AbbreviatedDayNames;
		maxLen = 100;
		constructor(owner, id) { super(owner, id); }

		getText() {
			var dw = this._txtProvider.getDayOfWeek(),
				culture = this._txtProvider._getCulture();
			return this.getRealText(culture.calendars.standard.days.namesAbbr[dw - 1]);
		}

		setText(value: string, allowchangeotherpart, result): boolean {
			var dw = -1, culture = this._txtProvider._getCulture();
			dw = this._txtProvider.findAlikeArrayItemIndex(culture
				.calendars.standard.days.namesShort, value);
			if (dw === -1) {
				return false;
			}
			return this._txtProvider.setDayOfWeek(dw + 1);
		}

		needAdjustInsertPos() {
			return false;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor101
	class _dateDescriptor101 extends DayOfMonthDescriptor {
		name = 'DayNames';
		formatString = "dddd";
		type = DescriptorType.DayNames;
		maxLen = 100;
		constructor(owner, id) { super(owner, id); }

		getText() {
			var dw = this._txtProvider.getDayOfWeek(),
				culture = this._txtProvider._getCulture();
			return this.getRealText(culture.calendars.standard.days.names[dw - 1]);
		}

		setText(value, allowchangeotherpart, result) {
			var dw = -1, culture = this._txtProvider._getCulture();
			dw = this._txtProvider
				.findAlikeArrayItemIndex(culture.calendars.standard.days.names, value);
			if (dw === -1) {
				return false;
			}
			return this._txtProvider.setDayOfWeek(dw + 1);
		}

		needAdjustInsertPos() {
			return false;
		}
	}

	class YearDateDescriptor extends _iDateDescriptor {
		constructor(owner, id) { super(owner, id); }

		inc() {
			var newYear = this._txtProvider.getYear() + this._txtProvider.inputWidget._getInnerIncrement();
            var newDate = new Date(this._txtProvider.getDate().valueOf());

            if (this._isForceMonth()) {
                this._txtProvider._setJsFullYear(newDate, newYear, true);
            } else {
                newDate.setFullYear(newYear);
            }

			if (this._txtProvider._isEraFormatExist()) {
				var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();

				if (newDate > maxDate) {
					if (!DateTimeInfo.Equal(this._txtProvider.getDate(), maxDate)) {
						this._txtProvider.setDate(maxDate);
					}
					else if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(minDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
			else {
				var minDate = this._txtProvider.inputWidget._getRealMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealMaxDate();

				if (!this._txtProvider.inputWidget._isValidDate(newDate, true)) {
					if (!DateTimeInfo.Equal(this._txtProvider.getDate(), maxDate)) {
						this._txtProvider.setDate(maxDate);
					}
					else if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(minDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
		}

		dec() {
			var newYear = this._txtProvider.getYear() - +this._txtProvider.inputWidget._getInnerIncrement();
            var newDate = new Date(this._txtProvider.getDate().valueOf());

            if (this._isForceMonth()) {
                this._txtProvider._setJsFullYear(newDate, newYear, true);
            } else {
                newDate.setFullYear(newYear);
            }

			if (this._txtProvider._isEraFormatExist()) {
				var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();

				if (newDate < minDate) {
					if (!DateTimeInfo.Equal(this._txtProvider.getDate(), minDate)) {
						this._txtProvider.setDate(minDate);
					}
					else if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(maxDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
			else {
				var minDate = this._txtProvider.inputWidget._getRealMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealMaxDate();

				if (!this._txtProvider.inputWidget._isValidDate(newDate, true)) {
					if (!DateTimeInfo.Equal(this._txtProvider.getDate(), minDate)) {
						this._txtProvider.setDate(minDate);
					}
					else if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(maxDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
		}
	}

	class TwoDigitYearDescriptor extends YearDateDescriptor {
		constructor(owner, id) { super(owner, id); }

		setText(value, allowchangeotherpart, result) {
			value = paddingZero(value);
			var y = paddingZero(this._txtProvider.getYear(), 4),
				m, dom, h, min, s;
			if (value === '00') {
				m = this._txtProvider.getMonth();
				dom = this._txtProvider.getDayOfMonth();
				h = this._txtProvider.getHours();
				min = this._txtProvider.getMinutes();
				s = this._txtProvider.getSeconds();
				if (m === 1 && dom === 1 && !h && !min && !s) {
					y = '0001';
					value = '01';
				}
			}
			if (y.length >= 2) {
				y = y.substr(0, 2) + value.substr(0, 2);
			}

			return this._txtProvider.setYear(parseInt(y), result, false);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor10
	class _dateDescriptor10 extends YearDateDescriptor {
		name = 'Four-digit year';
		formatString = "yyyy";
		type = DescriptorType.FourDigitYear;
		maxLen = 4;
		constructor(owner, id) { super(owner, id); }

		getText() {
			return this.getRealText(paddingZero(this._txtProvider.getYear(), 4));
		}

		setText(value, allowchangeotherpart, result, isSmartInputMode?: boolean) {
			if (isSmartInputMode && result) {
				value = value * 1;
				value = value % 100;

				var startYear = 1900 + 100;
				if (this._txtProvider.inputWidget.options.startYear) {
					startYear = this._txtProvider.inputWidget.options.startYear;
				}

				var lastYear = startYear % 100;
				var firstYear = startYear - lastYear;

				if (value >= lastYear) {
					value = firstYear + value;
				}
				else {
					value = firstYear + value + 100;
				}

				//    startYearStr: string,
				//    endYear, curDate, thisYear, inputNum, century, addYear, s;

				//endYear = startYear + 100 - 1;
				//startYearStr = this._txtProvider.paddingZero(startYear, 4);
				//endYear = this._txtProvider.paddingZero(endYear, 4);
				//if (result.pos === 0 || result.pos === 1) {
				//    curDate = new Date();
				//    thisYear = this._txtProvider
				//        .paddingZero(this._txtProvider.getYear(), 4);
				//    if (thisYear.charAt(0) === '0' &&
				//        thisYear.charAt(1) === '0' && result.pos <= 1) {
				//        inputNum = result.val;
				//        century = '00';
				//        if (inputNum >= 5) {
				//            century = startYearStr.slice(0, 2);
				//        }
				//        else {
				//            century = endYear.slice(0, 2);
				//        }
				//        addYear = result.val + thisYear.slice(3, 4);
				//        s = century + addYear;
				//        result.offset = 2 - result.pos;
				//        this._txtProvider.setYear(s, result);
				//        return true;
				//    }
				//}
			}

			return this._txtProvider.setYear(value, result, false);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor1
	class _dateDescriptor1 extends TwoDigitYearDescriptor {
		name = 'One-digit year';
		formatString = "y";
		type = DescriptorType.OneDigitYear;
		constructor(owner, id) { super(owner, id); }

		getText() {
			var y = paddingZero(this._txtProvider.getYear());
			if (y[0] === '0') {
				y = y[1];
			}
			return this.getRealText(y);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor2
	class _dateDescriptor2 extends TwoDigitYearDescriptor {
		name = 'Two-digit year';
		formatString = "yy";
		type = DescriptorType.TwoDigitYear;
		constructor(owner, id) { super(owner, id); }

		getText() {
			if (this._txtProvider.inputWidget.isFocused() && this._txtProvider.inputWidget.options.date == null) {
				return "yy";
			}
			return this.getRealText(paddingZero(this._txtProvider.getYear(), 2));
		}
	}

	class HourDescriptor extends _iDateDescriptor {
		constructor(owner, id) { super(owner, id); }

		inc() {
			var newHour = this._txtProvider.getHours() + this._txtProvider.inputWidget._getInnerIncrement();
			var newDate = new Date(this._txtProvider.getDate().valueOf());
			newDate.setHours(newHour)

			if (this._txtProvider._isEraFormatExist()) {
				var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();

				if (newDate > maxDate) {
					if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(minDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
			else {
				var minDate = this._txtProvider.inputWidget._getRealMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealMaxDate();

				if (!this._txtProvider.inputWidget._isValidDate(newDate, true)) {
					if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(minDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
		}

		dec() {
			var newHour = this._txtProvider.getHours() - this._txtProvider.inputWidget._getInnerIncrement();
			var newDate = new Date(this._txtProvider.getDate().valueOf());
			newDate.setHours(newHour);
			if (this._txtProvider._isEraFormatExist()) {
				var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();

				if (newDate < minDate) {
					if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(maxDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
			else {
				var minDate = this._txtProvider.inputWidget._getRealMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealMaxDate();

				if (!this._txtProvider.inputWidget._isValidDate(newDate, true)) {
					if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(maxDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
		}
	}

	class TwelveHourDescriptor extends HourDescriptor {
		constructor(owner, id) { super(owner, id); }

		setText(value, allowchangeotherpart, result) {
            var h = this._txtProvider.getHours();
			if (h >= 12 && value < 12) {
				value = ((value * 1) + 12);
			}
			else if (h < 12 && value == 12) {
				value = 0;
			}
			return this._txtProvider.setHours(value, allowchangeotherpart);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor45
	class _dateDescriptor45 extends TwelveHourDescriptor {
		name = 'h';
		formatString = "h";
		type = DescriptorType.h;
		constructor(owner, id) { super(owner, id); }

		getText() {
			if (this._txtProvider.inputWidget.isFocused() && this._txtProvider.inputWidget.options.date == null) {
				return "h";
			}

			var h = this._txtProvider.getHours();
			if (h > 12) {
				h = h - 12;
			}
			else if (h === 0) {
				h = 12;
			}

			if (h == 12 && this._txtProvider.inputWidget.options.hour12As0) {
				h = 0;
			}

			return this.getRealText('' + h + '');
		}

	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor46
	class _dateDescriptor46 extends TwelveHourDescriptor {
		name = 'hh';
		formatString = "hh";
		type = DescriptorType.hh;
		constructor(owner, id) { super(owner, id); }

		getText() {
			var h = this._txtProvider.getHours();
			if (h > 12) {
				h -= 12;
			} else if (h === 0) {
				h = 12;
			}

			if (h == 12 && this._txtProvider.inputWidget.options.hour12As0) {
				h = 0;
			}

			return this.getRealText(paddingZero(h, 2));
		}
	}

	class TwentyFourHourDescriptor extends HourDescriptor {
		constructor(owner, id) { super(owner, id); }

		setText(value, allowchangeotherpart, result) {
			return this._txtProvider.setHours(value, allowchangeotherpart);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor47
	class _dateDescriptor47 extends TwentyFourHourDescriptor {
		name = 'H';
		formatString = "H";
		type = DescriptorType.H;
		constructor(owner, id) { super(owner, id); }

		getText() {
			var h = this._txtProvider.getHours();

			if (h == 0 && !this._txtProvider.inputWidget.options.midnightAs0) {
				h = 24;
			}

			return this.getRealText('' + h + '');
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor48
	class _dateDescriptor48 extends TwentyFourHourDescriptor {
		name = 'HH';
		formatString = "HH";
		type = DescriptorType.HH;
		constructor(owner, id) { super(owner, id); }

		getText() {
			var h = this._txtProvider.getHours();
			if (h == 0 && !this._txtProvider.inputWidget.options.midnightAs0) {
				h = 24;
			}
			return this.getRealText(paddingZero(h, 2));
		}
	}

	class AmPmDescriptor extends _iDateDescriptor {
		constructor(owner, id) { super(owner, id); }

		inc() {
			var h = (this._txtProvider.getHours() + 12) % 24;
			this._txtProvider.setHours(h, true);
		}

		dec() {
			var h = (this._txtProvider.getHours() + 12) % 24;
			this._txtProvider.setHours(h, true);
		}

		setText(value: string, allowchangeotherpart, result) {
			var h;
			if (value.toLowerCase().indexOf('a') >= 0) {
				h = this._txtProvider.getHours() % 12;
				this._txtProvider.setHours(h, true);
			} else if (value.toLowerCase().indexOf('p') >= 0) {
				h = this._txtProvider.getHours() % 12 + 12;
				this._txtProvider.setHours(h, true);
			}

			return true;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor250
	class _dateDescriptor250 extends AmPmDescriptor {
		name = 't';
		formatString = "t";
		type = DescriptorType.ShortAmPm;
		constructor(owner, id) { super(owner, id); }

		getText() {
			var hours = this._txtProvider.getHours();
			var culture = this._txtProvider._getCulture();
			var am = this._txtProvider.inputWidget._getInnerAmDesignator();
			var pm = this._txtProvider.inputWidget._getInnerPmDesignator();
			var designator = hours < 12 ? am : pm;
			return this.getRealText(designator.charAt(0) || " ");
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor251
	class _dateDescriptor251 extends AmPmDescriptor {
		name = 'tt';
		formatString = "tt";
		type = DescriptorType.AmPm;
		constructor(owner, id) { super(owner, id); }

		getText() {
			var hours = this._txtProvider.getHours();
			var culture = this._txtProvider._getCulture();
			var am = this._txtProvider.inputWidget._getInnerAmDesignator();
			var pm = this._txtProvider.inputWidget._getInnerPmDesignator();
			var designator = hours < 12 ? am : pm;

			if (designator.length <= 0) {
				designator = ' ';
			}
			return this.getRealText(designator);
		}
	}

	class MinuteDescriptor extends _iDateDescriptor {
		constructor(owner, id) { super(owner, id); }

		inc() {
			var newMinute = this._txtProvider.getMinutes() + this._txtProvider.inputWidget._getInnerIncrement();
			var newDate = new Date(this._txtProvider.getDate().valueOf());
			newDate.setMinutes(newMinute);
			if (this._txtProvider._isEraFormatExist()) {
				var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();

				if (newDate > maxDate) {
					if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(minDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
			else {
				var minDate = this._txtProvider.inputWidget._getRealMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealMaxDate();

				if (!this._txtProvider.inputWidget._isValidDate(newDate, true)) {
					if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(minDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
		}

		dec() {
			var newMinite = this._txtProvider.getMinutes() - this._txtProvider.inputWidget._getInnerIncrement();
			var newDate = new Date(this._txtProvider.getDate().valueOf());
			newDate.setMinutes(newMinite);

			if (this._txtProvider._isEraFormatExist()) {
				var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();

				if (newDate < minDate) {
					if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(maxDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
			else {
				var minDate = this._txtProvider.inputWidget._getRealMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealMaxDate();

				if (!this._txtProvider.inputWidget._isValidDate(newDate, true)) {
					if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(maxDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
		}

		setText(value, allowchangeotherpart, result) {
			if (result && result.isfullreset) {
				value = '0';
			}

			return this._txtProvider.setMinutes(value, allowchangeotherpart);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor50
	class _dateDescriptor50 extends MinuteDescriptor {
		name = 'mm';
		formatString = "mm";
		type = DescriptorType.mm;
		constructor(owner, id) { super(owner, id); }

		getText() {
			return this.getRealText(paddingZero(this._txtProvider.getMinutes()));
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor51
	class _dateDescriptor51 extends MinuteDescriptor {
		name = 'm';
		formatString = "m";
		type = DescriptorType.m;
		delta = 12;
		constructor(owner, id) { super(owner, id); }

		getText() {
			return this.getRealText(this._txtProvider.getMinutes().toString());
		}
	}

	class SecondDescriptor extends _iDateDescriptor {
		constructor(owner, id) { super(owner, id); }

		inc() {
			var newSecond = this._txtProvider.getSeconds() + this._txtProvider.inputWidget._getInnerIncrement();
			var newDate = new Date(this._txtProvider.getDate().valueOf());
			newDate.setSeconds(newSecond);

			if (this._txtProvider._isEraFormatExist()) {
				var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();

				if (newDate > maxDate) {
					if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(minDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
			else {
				var minDate = this._txtProvider.inputWidget._getRealMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealMaxDate();

				if (!this._txtProvider.inputWidget._isValidDate(newDate, true)) {
					if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(minDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
		}

		dec() {
			var newSecond = this._txtProvider.getSeconds() - this._txtProvider.inputWidget._getInnerIncrement();
			var newDate = new Date(this._txtProvider.getDate().valueOf());
			newDate.setSeconds(newSecond);

			if (this._txtProvider._isEraFormatExist()) {
				var minDate = this._txtProvider.inputWidget._getRealEraMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealEraMaxDate();

				if (newDate < minDate) {
					if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(maxDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
			else {
				var minDate = this._txtProvider.inputWidget._getRealMinDate();
				var maxDate = this._txtProvider.inputWidget._getRealMaxDate();

				if (!this._txtProvider.inputWidget._isValidDate(newDate, true)) {
					if (this._txtProvider.inputWidget._getAllowSpinLoop()) {
						this._txtProvider.setDate(maxDate);
					}
				}
				else {
					this._txtProvider.setDate(newDate);
				}
			}
		}

		setText(value, allowchangeotherpart, result) {
			if (result && result.isfullreset) {
				value = '0';
			}
			return this._txtProvider.setSeconds(value, allowchangeotherpart);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor60
	class _dateDescriptor60 extends SecondDescriptor {
		name = 'ss';
		formatString = "ss";
		type = DescriptorType.ss;
		constructor(owner, id) { super(owner, id); }

		getText() {
			return this.getRealText(paddingZero(this._txtProvider.getSeconds()));
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// _dateDescriptor61
	class _dateDescriptor61 extends SecondDescriptor {
		name = 's';
		formatString = "s";
		type = DescriptorType.s;
		constructor(owner, id) { super(owner, id); }

		getText() {
			return this.getRealText(this._txtProvider.getSeconds().toString());
		}
	}


	/** @ignore */
	export interface DescriptorPosition {
		desc: _iDateDescriptor;
		pos: number;
		text: string;
		length: number;
	}
	/** @ignore */
	export class DateTimeInfo {
		static EraDates = new Array(new Date(1868, 9 - 1, 8), new Date(1912, 7 - 1, 30), new Date(1926, 12 - 1, 25), new Date(1989, 1 - 1, 8));
		static EraCount = 4;
		static EraYears = new Array(45, 15, 64, 99);
		static EraMax = new Date(2087, 12 - 1, 31, 23, 59, 59);
		static EraMin = new Date(1868, 9 - 1, 8);
		static EraKeys = new Array("1", "2", "3", "4", "m", "t", "s", "h");
		static EraIndices = new Array(0, 1, 2, 3, 0, 1, 2, 3);
		static DateLongFormat = "yyyyMMddHHmmss";
		static EraNames = new Array("M", "T", "S", "H", "\u660E", "\u5927", "\u662D", "\u5E73", "\u660E\u6CBB", "\u5927\u6B63", "\u662D\u548C", "\u5E73\u6210");
		static WeekDays = new Array("\u65e5\u66dc\u65e5", "\u6708\u66dc\u65e5", "\u706b\u66dc\u65e5", "\u6c34\u66dc\u65e5", "\u6728\u66dc\u65e5", "\u91d1\u66dc\u65e5", "\u571f\u66dc\u65e5");
		static MonthNames = new Array("\u0031\u6708", "\u0032\u6708", "\u0033\u6708", "\u0034\u6708", "\u0035\u6708", "\u0036\u6708", "\u0037\u6708", "\u0038\u6708", "\u0039\u6708", "\u0031\u0030\u6708", "\u0031\u0031\u6708", "\u0031\u0032\u6708");
		static ShortWeekDays = new Array("\u65e5", "\u6708", "\u706b", "\u6c34", "\u6728", "\u91d1", "\u571f");
		static ShortMonthNames = new Array("\u0031", "\u0032", "\u0033", "\u0034", "\u0035", "\u0036", "\u0037", "\u0038", "\u0039", "\u0031\u0030", "\u0031\u0031", "\u0031\u0032");
		static RokuyouTextArray = new Array("\u5148\u52dd", "\u53cb\u5f15", "\u5148\u8ca0", "\u4ecf\u6ec5", "\u5927\u5b89", "\u8d64\u53e3");
		static RokuyouTextArrayEn = new Array("Senshou", "Tomobiki", "Senbu", "Butsumetsu", "Taian", "Shakkou");
		static DefaultTwoDigitYear = 2029;
		static Digits = 2;
		static EraYearMax = 99;
		static RokuyouMin = new Date(1960, 0, 28);
		static RokuyouMax = new Date(2050, 0, 22);
		static LunarInfo = new Array(0x0aea6, 0x0ab50, 0x04d60, 0x0aae4, 0x0a570, 0x05270, 0x07263, 0x0d950, 0x06b57, 0x056a0,
			0x09ad0, 0x04dd5, 0x04ae0, 0x0a4e0, 0x0d4d4, 0x0d250, 0x0d598, 0x0b540, 0x0d6a0, 0x0195a6,
			0x095b0, 0x049b0, 0x0a9b4, 0x0a4b0, 0x0b27a, 0x06a50, 0x06d40, 0x0b756, 0x02b60, 0x095b0,
			0x04b75, 0x04970, 0x064b0, 0x074a3, 0x0ea50, 0x06d98, 0x055d0, 0x02b60, 0x096e5, 0x092e0,
			0x0c960, 0x0e954, 0x0d4a0, 0x0da50, 0x07552, 0x056c0, 0x0abb7, 0x025d0, 0x092d0, 0x0cab5,
			0x0a950, 0x0b4a0, 0x1b4a3, 0x0b550, 0x055d9, 0x04ba0, 0x0a5b0, 0x05575, 0x052b0, 0x0a950,
			0x0b954, 0x06aa0, 0x0ad50, 0x06b52, 0x04b60, 0x0a6e6, 0x0a570, 0x05270, 0x06a65, 0x0d930,
			0x05aa0, 0x0b6a3, 0x096d0, 0x04bd7, 0x04ae0, 0x0a4d0, 0x01d0d6, 0x0d250, 0x0d520, 0x0dd45,
			0x0b6a0, 0x096d0, 0x055b2, 0x049b0, 0x0a577, 0x0a4b0, 0x0b250, 0x01b255, 0x06d40, 0x0ada0);
		static year;
		static month;
		static day;
		static isLeap;
		static IsValidEraDate(date) {
			if (date < this.GetEraMin() || date > this.GetEraMax()) {
				return false;
			}
			return true;
		}
		static ConvertToGregorianYear(era, eraYear, strict) {
			if (era < 0 || era >= this.GetEraCount() || eraYear < 1 || strict && eraYear > this.GetEraYears()[era]) {
				return -1;
			}
			return this.GetEraDates()[era].getFullYear() + eraYear - 1;
		}
		static GetEraDates() {
			if (window.eras != undefined) {
				var eraDates = new Array();
				for (var i = 0; i < window.eras.length; i++) {
					var date = new Date(window.eras[i].startDate.replace(/-/g, "/"));
					eraDates[i] = date;
				}
				return eraDates;
			}
			return this.EraDates;
		}
		static GetEraNames() {
			var eraNames = new Array();
			if (window.eras != undefined) {
				for (var i = 0; i < window.eras.length; i++) {
					eraNames[i] = window.eras[i].name;
				}
				return eraNames;
			}

			for (var i = 0; i < this.EraCount; i++) {
				eraNames[i] = this.EraNames[i + 2 * this.EraCount];
			}
			return eraNames;
		}

		static GetEraSymbols() {
			var eraSymbol = new Array();
			if (window.eras != undefined) {
				for (var i = 0; i < window.eras.length; i++) {
					eraSymbol[i] = window.eras[i].symbol;
				}
				return eraSymbol;
			}

			for (var i = 0; i < this.EraCount; i++) {
				eraSymbol[i] = this.EraNames[i];;
			}
			return eraSymbol;
		}


		static GetEraAbbreviations() {
			var eraAbbreviation = new Array();
			if (window.eras != undefined) {
				for (var i = 0; i < window.eras.length; i++) {
					eraAbbreviation[i] = window.eras[i].abbreviation;
				}
				return eraAbbreviation;
			}

			for (var i = 0; i < this.EraCount; i++) {
				eraAbbreviation[i] = this.EraNames[i + this.EraCount];
			}
			return eraAbbreviation;
		}

		static GetEraShortNames() {
			var eraShortName = new Array();
			if (window.eras != undefined) {
				for (var i = 0; i < window.eras.length; i++) {
					eraShortName[i] = window.eras[i].shortcuts.split(',')[1];
				}
				return eraShortName;
			}

			for (var i = 0; i < this.EraCount; i++) {
				eraShortName[i] = this.EraNames[i];
			}
			return eraShortName;
		}

		static GetEraShortcuts() {
			var eraShortcuts = new Array();
			if (window.eras != undefined) {
				for (var i = 0; i < window.eras.length; i++) {
					eraShortcuts[i] = window.eras[i].shortcuts.split(',')[0];
				}
				return eraShortcuts;
			}

			for (var i = 0; i < this.EraCount; i++) {
				eraShortcuts[i] = this.EraKeys[i];
			}
			return eraShortcuts;
		}

		static GetEraMax() {
			if (window.eras != undefined) {
				if (window.eras.length > 0) {
					var date = new Date(window.eras[window.eras.length - 1].startDate.replace(/-/g, "/"));
					date.setFullYear(date.getFullYear() + 99);
					return date;
				}

			}
			return this.EraMax;
		}

		static GetEraMin() {
			if (window.eras != undefined) {
				if (window.eras.length > 0) {
					var date = new Date(window.eras[0].startDate.replace(/-/g, "/"));
					return date;
				}

			}
			return this.EraMin;
		}

		static GetEraCount() {
			if (window.eras != undefined) {
				return window.eras.length;
			}
			return this.EraCount;
		}

		static GetEraYears() {
			if (window.eras != undefined) {
				var eraYears = new Array();
				for (var i = 1; i < window.eras.length; i++) {
					var date1 = new Date(window.eras[i - 1].startDate.replace(/-/g, "/"));
					var date2 = new Date(window.eras[i].startDate.replace(/-/g, "/"));
					eraYears[i - 1] = date2.getFullYear() - date1.getFullYear() + 1;
				}
				eraYears[i - 1] = 99;
				return eraYears;
			}
			return this.EraYears;
		}

		static ConvertToGregorianDate(era, eraYear, month, day, hour, minute, second, strict) {
			var year = this.ConvertToGregorianYear(era, eraYear, strict);

			if (year < 1 || year > 9999) {
				return null;
			}

			if (month < 1 || month > 12) {
				return null;
			}

			var maxdays = this.DaysInMonth(year, month);

			if (day < 1 || day > maxdays) {
				return null;
			}
			if (hour < 0 || hour > 23) {
				return null;
			}
			if (minute < 0 || minute > 59) {
				return null;
			}
			if (second < 0 || second > 59) {
				return null;
			}
			var dateTime = new Date(year, month - 1, day, hour, minute, second);

			if (strict) {
				var startDate = this.GetEraDates()[era];
				var endDate = era + 1 != this.GetEraCount() ? this.AddMilliseconds(this.GetEraDates()[era + 1], -1) : this.GetEraMax();

				if (dateTime < startDate || dateTime > endDate) {
					return null;
				}
			}

			return dateTime;
		}

		static String2Date(value) {
			if (value == null || value.Length == 0) {
				return null;
			}

			var date = new Date(Date.parse(value));
			return date;
		}

		static ToString(value, length, ch?, position?) {
			var val = value + "";

			//It is same as String.PadLeft(int, char) in C#.
			if (ch != null) {
				while (val.length < length) {
					if (position) {
						val = val + ch;
					}
					else {
						val = ch + val;
					}
				}

				return val;
			}

			//add the value length times.
			while (val.length < length) {
				val += value + "";
			}

			return val;
		}

		static Date2String(date, isJapan, IsjqDate, IsjqTime) {
			var strDate = "";

			try {
				if (isJapan == true) {
					if (IsjqDate == true) {
						strDate = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
					}
					else if (IsjqTime == true) {
						strDate = date.getHours() + ":" + DateTimeInfo.ToString(date.getMinutes(), 2, "0") + ":" + DateTimeInfo.ToString(date.getSeconds(), 2, "0");
					}
					else {
						strDate = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate() + " " + date.getHours() + ":" + DateTimeInfo.ToString(date.getMinutes(), 2, "0") + ":" + DateTimeInfo.ToString(date.getSeconds(), 2, "0");
					}
				}
				else {
					if (IsjqDate == true) {
						strDate = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear();
					}
					else if (IsjqTime == true) {
						strDate = date.getHours() + ":" + DateTimeInfo.ToString(date.getMinutes(), 2, "0") + ":" + DateTimeInfo.ToString(date.getSeconds(), 2, "0");
					}
					else {
						strDate = (date.getMonth() + 1) + "/" + date.getDate() + "/" + date.getFullYear() + " " + date.getHours() + ":" + DateTimeInfo.ToString(date.getMinutes(), 2, "0") + ":" + DateTimeInfo.ToString(date.getSeconds(), 2, "0");
					}
				}
			}
			catch (ex) {
				strDate = "";
			}

			return strDate;
		}

		static Date2Long(date) {
			try {
				return (date.getFullYear() * 100 + date.getMonth() + 1) * 100 + date.getDate();
			}
			catch (ex) {
				return 0;
			}
		}

		static Date2Number(date) {
			if (date == null) {
				return 0;
			}

			return ((((date.getFullYear() * 100 + date.getMonth() + 1) * 100 + date.getDate()) * 100 + date.getHours()) * 100 + date.getMinutes()) * 100 + date.getSeconds();
		}

		static Long2Date(value) {
			return new Date(Math.floor(value / 10000), Math.floor((value % 10000) / 100) - 1, value % 100);
		}

		static GetEraDate(date) {
			var eraDate: any = new Object();
			eraDate.era = -1;
			eraDate.eraYear = -1;

			if (!this.IsValidEraDate(date)) {
				return eraDate;
			}

			for (var i = 0; i < this.GetEraCount(); i++) {
				var nextDate = i + 1 != this.GetEraCount() ? this.GetEraDates()[i + 1] : this.AddMilliseconds(this.GetEraMax(), 1);

				if (date < nextDate) {
					eraDate.era = i;
					eraDate.eraYear = date.getFullYear() - this.GetEraDates()[i].getFullYear() + 1;

					break;
				}
			}

			return eraDate;
		}

		static MakeDate(era, erayear, month, day): any {
			var date = new Date();

			date = this.ConvertToGregorianDate(era, erayear, month, day, 0, 0, 0, true);

			if (date == null) {
				return -1;
			}

			return date;
		}

		static GetValidMonthRange(era, eraYear) {
			var monthRange: any = {};
			monthRange.min = 1;
			monthRange.max = 12;

			if (era == -1 || eraYear == -1) {
				return monthRange;
			}

			if (eraYear == 1) {
				monthRange.min = this.GetEraDates()[era].getMonth() + 1;
			}
			else if (eraYear == this.EraYears[era] && era < this.EraCount - 1) {
				monthRange.max = this.AddMilliseconds(this.GetEraDates()[era + 1], -1).getMonth() + 1;
			}

			return monthRange;
		}

		static GetValidDayRange(era, eraYear, month) {
			var dayRange: any = new Object();
			dayRange.min = 1;
			dayRange.max = this.DaysInMonth(this.ConvertToGregorianYear(era, eraYear, true), month);
			var eraMin = this.GetEraDates()[era];
			var eraMax = era < this.GetEraCount() - 1 ? this.AddMilliseconds(this.GetEraDates()[era + 1], -1) : this.GetEraMax();

			if (era == -1 || eraYear == -1) {
				return dayRange;
			}

			if (eraYear == 1 && month == eraMin.getMonth() + 1) {
				dayRange.min = eraMin.getDate();
			}
			else if (eraYear == this.EraYears[era] && month == eraMax.getMonth() + 1) {
				dayRange.max = eraMax.getDate();
			}

			return dayRange;
		}

		static IsLeapYear(year) {
			if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
				return true;
			}
			else {
				return false;
			}
		}

		static DayOfYear(year, month, day) {
			var sum = 0;
			for (var i = 1; i < month; i++) {
				sum = sum + this.DaysInMonth(year, i);
			}

			sum = sum + day;
			return sum;
		}

		static DaysInMonth(year, month) {
			if (month > 12) {
				year += Math.floor(month / 12);
				month = month % 12;
			}
			else if (month < 0) {
				var zMonth = month * (-1);
				year -= Math.ceil(zMonth / 12);
				month = 12 - zMonth % 12;
			}
			switch (month) {
				case 2:
					if (this.IsLeapYear(year)) {
						return 29;
					}
					else {
						return 28;
					}
					break;
				case 4:
				case 6:
				case 9:
				case 11:
					return 30;
					break;
				default:
					return 31;
					break;
			}
		}

		static Equal(date1, date2, strict?) {
			if (date1 == null && date2 == null) {
				return true;
			}
			else if (date1 == null) {
				return false;
			}
			else if (date2 == null) {
				return false;
			}

			try {
				if (!strict) {
					if (date1.getFullYear() == date2.getFullYear()
						&& date1.getMonth() == date2.getMonth()
						&& date1.getDate() == date2.getDate()) {
						return true;
					}
					else {
						return false;
					}
				}

				if (date1.getFullYear() == date2.getFullYear()
					&& date1.getMonth() == date2.getMonth()
					&& date1.getDate() == date2.getDate()
					&& date1.getHours() == date2.getHours()
					&& date1.getMinutes() == date2.getMinutes()
					&& date1.getSeconds() == date2.getSeconds()) {
					return true;
				}
				else {
					return false;
				}
			}
			catch (e) {
				return false;
			}
		}

		static YearMonthEqual(date1, date2) {
			if (date1 == null && date2 == null) {
				return true;
			}
			else if (date1 == null) {
				return false;
			}
			else if (date2 == null) {
				return false;
			}

			try {

				if (date1.getFullYear() == date2.getFullYear()
					&& date1.getMonth() == date2.getMonth()
					) {
					return true;
				}
				else {
					return false;
				}
			}
			catch (e) {
				return false;
			}
		}

		static SetDate(year, month, day) {
			var newDate = new Date();
			var maxMonth = 9999 * 12 + 12;

			if (year * 12 + month > maxMonth) {
				newDate = new Date(9999, 11, 31);
			}
			else if (year * 12 + month < 100) {
				newDate = new Date(100, 0, 1);
			}
			else {
				var days = this.DaysInMonth(year, month);
				if (day > days) {
					day = days;
				}
				newDate = new Date(year, month - 1, day);
			}

			return newDate;
		}

		static SetFullDate(year, month, day) {
			var newDate = new Date();
			newDate.setDate(1);
			var maxMonth = 9999 * 12 + 12;

			var days = this.DaysInMonth(year, month);

			if ((month > 0) && (month < 12) && (day > days)) {
				day = days;
			}

			newDate.setFullYear(year);
			newDate.setMonth(month - 1);
			newDate.setDate(day);
			return newDate;
		}

		static SetFullDateByDate(date) {
			var newDate = new Date();
			newDate.setDate(1);
			newDate.setFullYear(date.getFullYear());
			newDate.setMonth(date.getMonth());
			newDate.setDate(date.getDate());
			newDate.setHours(0, 0, 0, 0);
			return newDate;
		}

		static LYearDays(y) {
			var i, sum = 348;
			for (i = 0x8000; i > 0x8; i >>= 1) sum += (this.LunarInfo[y - 1960] & i) ? 1 : 0;
			return (sum + this.leapDays(y));
		}

		static leapDays(y) {
			if (this.LeapMonth(y)) return ((this.LunarInfo[y - 1960] & 0x10000) ? 30 : 29);
			else return (0);
		}

		static LeapDays(y) {
			if (this.LeapMonth(y)) return ((this.LunarInfo[y - 1960] & 0x10000) ? 30 : 29);
			else return (0);
		}

		static LeapMonth(y) {
			return (this.LunarInfo[y - 1960] & 0xf);
		}

		static MonthDays(y, m) {
			return ((this.LunarInfo[y - 1960] & (0x10000 >> m)) ? 30 : 29);

		}

		static GetRokuyou(date) {

			if ((date - <any>this.RokuyouMin) < 0 || (date - <any>this.RokuyouMax) > 0) {
				return;
			}
			var i, leap = 0, temp = 0;
			var baseDate = new Date(1960, 0, 28);
			var offset = Math.round((date - <any>baseDate) / 86400000);

			for (i = 1960; i < 2050 && offset > 0; i++) {
				temp = this.LYearDays(i);
				offset -= temp;
			}
			if (offset < 0) {
				offset += temp;
				i--;
			}

			this.year = i;

			leap = this.LeapMonth(i);
			this.isLeap = false;

			for (i = 1; i < 13 && offset > 0; i++) {

				if (leap > 0 && i == (leap + 1) && this.isLeap == false) {
					--i;
					this.isLeap = true;
					temp = this.LeapDays(this.year);
				}
				else {
					temp = this.MonthDays(this.year, i);
				}


				if (this.isLeap == true && i == (leap + 1))
					this.isLeap = false;

				offset -= temp;
			}

			if (offset == 0 && leap > 0 && i == leap + 1)
				if (this.isLeap) {
					this.isLeap = false;
				}
				else {
					this.isLeap = true;
					--i;
				}

			if (offset < 0) {
				offset += temp;
				--i;
			}

			this.month = i;
			this.day = offset + 1;

			var index = (this.month + this.day - 2) % 6;

			var errorRange1Min = new Date(1996, 6, 15);
			var errorRange1Max = new Date(1996, 7, 13);
			if (this.IsDateInRange(date, errorRange1Min, errorRange1Max)) {
				index = (6 + index - 1) % 6;
			}

			var errorRange2Min = new Date(1996, 8, 12);
			var errorRange2Max = new Date(1996, 9, 11);
			if (this.IsDateInRange(date, errorRange2Min, errorRange2Max)) {
				index = (6 + index - 1) % 6;
			}

			var errorRange3Min = new Date(2033, 7, 25);
			var errorRange3Max = new Date(2033, 11, 21);
			if (this.IsDateInRange(date, errorRange3Min, errorRange3Max)) {
				index = (index + 1) % 6;
			}

			if (this.Equal(date, errorRange1Min)) {
				index = (6 + index - 1) % 6;
			}
			if (this.Equal(date, errorRange2Min)) {
				index = (6 + index - 1) % 6;
			}


			var rokuyou;
			//switch (index) {
			//    case 0:
			//        rokuyou = GrapeCity.IM.Rokuyou.Senshou;
			//        break;
			//    case 1:
			//        rokuyou = GrapeCity.IM.Rokuyou.Tomobiki;
			//        break;
			//    case 2:
			//        rokuyou = GrapeCity.IM.Rokuyou.Senbu;
			//        break;
			//    case 3:
			//        rokuyou = GrapeCity.IM.Rokuyou.Butsumetsu;
			//        break;
			//    case 4:
			//        rokuyou = GrapeCity.IM.Rokuyou.Taian;
			//        break;
			//    case 5:
			//        rokuyou = GrapeCity.IM.Rokuyou.Shakkou;
			//        break;
			//    default:
			//        break;
			//}
			return rokuyou;
		}

		static IsDateInRange(dt, min, max) {
			var dtYear = dt.getYear();
			var minYear = min.getYear();
			var maxYear = max.getYear();
			var dtMonth = dt.getMonth();
			var minMonth = min.getMonth();
			var maxMonth = max.getMonth();
			var dtDay = dt.getDate();
			var minDay = min.getDate();
			var maxDay = max.getDate();
			var dtValue = dtYear * 10000 + dtMonth * 100 + dtDay;
			var minValue = minYear * 10000 + minMonth * 100 + minDay;
			var maxValue = maxYear * 10000 + maxMonth * 100 + maxDay;

			return dtValue >= minValue && dtValue <= maxValue;
		}

		static GetRokuyouText(rokuyou) {
			var rokuyouArray = new Array();
			//if (GrapeCity.IM.Localization.Region == "ja") {
			rokuyouArray = this.RokuyouTextArray.slice(0);
			//}
			//else {
			//    rokuyouArray = this.RokuyouTextArrayEn.slice(0);
			//}

			switch (rokuyou) {
				//case GrapeCity.IM.Rokuyou.None:
				//    return "";
				//    break;
				//case GrapeCity.IM.Rokuyou.Senshou:
				//    return rokuyouArray[0];
				//    break;
				//case GrapeCity.IM.Rokuyou.Tomobiki:
				//    return rokuyouArray[1];
				//    break;
				//case GrapeCity.IM.Rokuyou.Senbu:
				//    return rokuyouArray[2];
				//    break;
				//case GrapeCity.IM.Rokuyou.Butsumetsu:
				//    return rokuyouArray[3];
				//    break;
				//case GrapeCity.IM.Rokuyou.Taian:
				//    return rokuyouArray[4];
				//    break;
				//case GrapeCity.IM.Rokuyou.Shakkou:
				//    return rokuyouArray[5];
				//    break;
				default:
					return "";
					break;
			}

		}

		static DaysInSpecialWeek(year, month, weekFlay) {

			var selections = new Array();
			var days = this.DaysInMonth(year, month + 1);
			var date = new Date(year, month, 1);
			var firstDateOfWeekTitle;

			//Get the firstDateOfWeekTitle, then break
			for (var i = 1; i <= days; i++) {
				date.setDate(i);
				if (date.getDay() == weekFlay) {
					var selectedDate = new Date(year, month, i);
					selections.push(selectedDate);
					firstDateOfWeekTitle = i;
					break;
				}

			}

			firstDateOfWeekTitle += 7;
			while (firstDateOfWeekTitle <= days) {
				date.setDate(firstDateOfWeekTitle);
				var followSelectedDate = new Date(year, month, firstDateOfWeekTitle);
				selections.push(followSelectedDate);
				firstDateOfWeekTitle += 7;
			}
			return selections;

		}

		static GetDaysByFirstWeekday(firstWeekday) {
			var days = new Array();
			days.push(firstWeekday);
			var date = new Date(firstWeekday);
			while (date.getDate() + 7 <= this.DaysInMonth(firstWeekday.getFullYear(), firstWeekday.getMonth() + 1)) {
				date = new Date(date.getFullYear(), date.getMonth(), date.getDate() + 7);
				days.push(date);
			}
			return days;
		}

		static AddMilliseconds(date, msec) {
			var newDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds());
			newDate.setMilliseconds(newDate.getMilliseconds() + msec);
			return new Date(newDate.valueOf());
		}

		static GetWeekIndexByDate(date) {
			return Math.floor(date.getDate() / 7);
		}

	}

	/** @ignore */
	export interface IwijInputDate {
		_getCulture();
		_getRealEraMinDate();
		_getRealEraMaxDate();

		_safeGetDate(ignoreCheckRange?: boolean);
		_safeSetDate(date, ignoreCheckRange?: boolean);
		_getAllowSpinLoop();
		_getRealMinDate();
		_getRealMaxDate();
		_setData(val);
		_isValidDate(date, chkBounds?: boolean);
		_getInnerAmDesignator();
		_getInnerPmDesignator();
		_getInnerIncrement();
		isFocused();
        _internalSetDate(date);

        _tempDate: Date;
        _isForceMonth();

		options: IDateOptions;
	}

	export interface IDateOptions {
		/**  @ignore */
		format: string;
		/** Indicates the culture that the format library will use.
		  */
		culture: string;
		/** Indicates the cultureCalendar that the format library will use.
		 */
		cultureCalendar: string;
		/** Determines string designator for hours that are "ante meridiem" (before noon).
		  */
		amDesignator: string;
		/** Determines the string designator for hours that are "post meridiem" (after noon).
		  */
		pmDesignator: string;
		/** A boolean value determines the range of hours that can be entered in the control.
		  */
		hour12As0: boolean;
		/** A boolean value determines whether to express midnight as 24:00.
		  */
		midnightAs0: boolean;

		/** @ignore */
		increment: number;
		/** @ignore */
		date: Date;
		/** @ignore */
		maxDate: Date;
		/** @ignore */
		minDate: Date;
		/** @ignore */
		startYear: number;
		/** @ignore */
		promptChar: string;
	}


	/** @ignore */
	export class wijDateTextFormatter {
		private noMask: boolean;
		private maskPartsCount: number = 0;
		private isDisplayFormat: boolean = true;
		public descriptors: _iDateDescriptor[];
		public desPostions: DescriptorPosition[];
		public fields: _iDateDescriptor[];
		public pattern: string = 'M/d/yyyy';

		constructor(public inputWidget: IwijInputDate, format: string, isDisplayFormat: boolean) {
			this.descriptors = new Array(0);
			this.desPostions = new Array(0);
			this.fields = new Array(0);
			this.isDisplayFormat = isDisplayFormat;
			this._setFormat(format);
		}

		private _parseFormat(pattern: string) {
			var descriptors = [];
			var curPattern = '',
				prevCh = '',
				isBegin = false,
				liternalNext = false, i, ch;
			for (i = 0; i < pattern.length; i++) {
				ch = pattern.charAt(i);
				if (liternalNext) {
					descriptors.push(this.createDescriptor(-1, ch));
					curPattern = '';
					liternalNext = false;
					continue;
				}
				if (ch === '\\') {
					liternalNext = true;
					if (curPattern.length > 0) {
						if (!this.handlePattern(curPattern, descriptors)) {
							descriptors.push(this.createDescriptor(-1, prevCh));
						}
						curPattern = '';
					}
					continue;
				}
				if (ch === '\'') {
					if (isBegin) {
						isBegin = false;
						curPattern = '';
					} else {
						isBegin = true;
						if (curPattern.length > 0) {
							if (!this.handlePattern(curPattern, descriptors)) {
								descriptors.push(this.createDescriptor(-1, prevCh));
							}
							curPattern = '';
						}

					}
					continue;
				}
				if (isBegin) {
					descriptors.push(this.createDescriptor(-1, ch));
					curPattern = '';
					continue;
				}
				if (!i) {
					prevCh = ch;
				}
				if (prevCh !== ch && curPattern.length > 0) {
					if (!this.handlePattern(curPattern, descriptors)) {
						descriptors.push(this.createDescriptor(-1, prevCh));
					}
					curPattern = '';
				}
				curPattern += ch;
				prevCh = ch;
			}
			if (curPattern.length > 0) {
				if (!this.handlePattern(curPattern, descriptors)) {
					descriptors.push(this.createDescriptor(-1, prevCh));
				}
			}

			return descriptors;
		}

		private _parseFormatToPattern(format: string) {
			var cf = this.inputWidget._getCulture().calendar,
				pattern = cf.patterns.d;
			if (format.length <= 1) {
				switch (format) {
					case "":
					case "d": // ShortDatePattern
						pattern = cf.patterns.d;
						break;
					case "D": // LongDatePattern
						pattern = cf.patterns.D;
						break;
					case "f": // Full date and time (long date and short time)
						pattern = cf.patterns.D + " " + cf.patterns.t;
						break;
					case "F": // Full date and time (long date and long time)
						pattern = cf.patterns.D + " " + cf.patterns.T;
						break;
					case "g": // General (short date and short time)
						pattern = cf.patterns.d + " " + cf.patterns.t;
						break;
					case "G": // General (short date and long time)
						pattern = cf.patterns.d + " " + cf.patterns.T;
						break;
					case "m": // MonthDayPattern
						pattern = cf.patterns.M;
						break;
					case "M": // monthDayPattern
						pattern = cf.patterns.M;
						break;
					case "s": // SortableDateTimePattern
						pattern = cf.patterns.S;
						break;
					case "t": // shortTimePattern
						pattern = cf.patterns.t;
						break;
					case "T": // LongTimePattern
						pattern = cf.patterns.T;
						break;
					case "u": // UniversalSortableDateTimePattern
						pattern = cf.patterns.S;
						break;
					case "U":
						// Full date and time (long date and long time) using universal time
						pattern = cf.patterns.D + " " + cf.patterns.T;
						break;
					case "y": // YearMonthPattern
						pattern = cf.patterns.Y;
						break;
					case "Y": // yearMonthPattern
						pattern = cf.patterns.Y;
						break;
					case "r": // RFC1123Pattern
					case "R":
						pattern = "ddd, dd MMM yyyy HH:mm:ss G\\M\\T";
						break;
					case "E":
						pattern = "E";
						break;
				}
			} else {
				pattern = format;
			}

			return pattern;
		}

		private createDescriptor(t: DescriptorType, liternal?) {
			var desc = null,
				id = this.maskPartsCount++;
			switch (t) {
				case -1:
					desc = new _dateDescriptor(this, id);
					desc.liternal = liternal;
					break;
				case 20:
					desc = new _dateDescriptor20(this, id);
					break;
				case 25:
					desc = new _dateDescriptor25(this, id);
					break;
				case 26:
					desc = new _dateDescriptor26(this, id);
					break;
				case 27:
					desc = new _dateDescriptor27(this, id);
					break;
				case 30:
					desc = new _dateDescriptor30(this, id);
					break;
				case 31:
					desc = new _dateDescriptor31(this, id);
					break;
				case 100:
					desc = new _dateDescriptor100(this, id);
					break;
				case 101:
					desc = new _dateDescriptor101(this, id);
					break;
				case 10:
					desc = new _dateDescriptor10(this, id);
					break;
				case 1:
					desc = new _dateDescriptor1(this, id);
					break;
				case 2:
					desc = new _dateDescriptor2(this, id);
					break;
				case 45:
					desc = new _dateDescriptor45(this, id);
					break;
				case 46:
					desc = new _dateDescriptor46(this, id);
					break;
				case 47:
					desc = new _dateDescriptor47(this, id);
					break;
				case 48:
					desc = new _dateDescriptor48(this, id);
					break;
				case 250:
					desc = new _dateDescriptor250(this, id);
					break;
				case 251:
					desc = new _dateDescriptor251(this, id);
					break;
				case 50:
					desc = new _dateDescriptor50(this, id);
					break;
				case 51:
					desc = new _dateDescriptor51(this, id);
					break;
				case 60:
					desc = new _dateDescriptor60(this, id);
					break;
				case 61:
					desc = new _dateDescriptor61(this, id);
					break;
				case 70:
					desc = new _dateDescriptor70(this, id);
					break;
				case 71:
					desc = new _dateDescriptor71(this, id);
					break;
				case 72:
					desc = new _dateDescriptor72(this, id);
					break;
				case 73:
					desc = new _dateDescriptor73(this, id);
					break;
				case 74:
					desc = new _dateDescriptor74(this, id);
					break;
				case 75:
					desc = new _dateDescriptor75(this, id);
					break;
				case 80:
					desc = new _dateDescriptor80(this, id);
					break;
				default:
					break;
			}
			return desc;
		}

		private handlePattern(p: string, descriptors: any) {
			var reg = new RegExp('y{3,10}'),
				suc = reg.test(p);
			if (suc) {
				if (this.inputWidget) {
					if (this.inputWidget.options.cultureCalendar === "Japanese") {
						descriptors.push(this.createDescriptor(71));
						return true;
					}
				}
				descriptors.push(this.createDescriptor(10));
				return true;
			}
			reg = new RegExp('y{2,2}');
			suc = reg.test(p);
			if (suc) {
				if (this.inputWidget) {
					if (this.inputWidget.options.cultureCalendar === "Japanese") {
						descriptors.push(this.createDescriptor(71));
						return true;
					}
				}
				descriptors.push(this.createDescriptor(2));
				return true;
			}
			reg = new RegExp('y{1,1}');
			suc = reg.test(p);
			if (suc) {
				if (this.inputWidget) {
					if (this.inputWidget.options.cultureCalendar === "Japanese") {
						descriptors.push(this.createDescriptor(70));
						return true;
					}
				}
				descriptors.push(this.createDescriptor(1));
				return true;
			}
			reg = new RegExp('d{4,4}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(101));
				return true;
			}
			reg = new RegExp('d{3,3}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(100));
				return true;
			}
			reg = new RegExp('d{2,2}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(30));
				return true;
			}
			reg = new RegExp('d{1,1}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(31));
				return true;
			}
			reg = new RegExp('M{4,4}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(27));
				return true;
			}
			reg = new RegExp('M{3,3}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(26));
				return true;
			}
			reg = new RegExp('M{2,2}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(20));
				return true;
			}
			reg = new RegExp('M{1,1}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(25));
				return true;
			}
			reg = new RegExp('h{2,2}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(46));
				return true;
			}
			reg = new RegExp('h{1,1}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(45));
				return true;
			}
			reg = new RegExp('H{2,2}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(48));
				return true;
			}
			reg = new RegExp('H{1,1}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(47));
				return true;
			}
			reg = new RegExp('m{2,2}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(50));
				return true;
			}
			reg = new RegExp('m{1,1}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(51));
				return true;
			}
			reg = new RegExp('s{2,2}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(60));
				return true;
			}
			reg = new RegExp('s{1,1}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(61));
				return true;
			}
			reg = new RegExp('t{2,2}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(251));
				return true;
			}
			reg = new RegExp('t{1,1}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(250));
				return true;
			}
			reg = new RegExp('e{2,10}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(71));
				return true;
			}
			reg = new RegExp('e{1,1}');
			suc = reg.test(p);
			if (suc) {
				descriptors.push(this.createDescriptor(70));
				return true;
			}
			reg = new RegExp('g{3,10}');
			suc = reg.test(p);
			if (suc) {

				descriptors.push(this.createDescriptor(74));
				return true;
			}
			reg = new RegExp('g{2,2}');
			suc = reg.test(p);
			if (suc) {
				if (this.inputWidget) {
					if (this.inputWidget.options.cultureCalendar === "Japanese") {
						descriptors.push(this.createDescriptor(74));
						return true;
					}
				}
				descriptors.push(this.createDescriptor(73));
				return true;
			}
			reg = new RegExp('g{1,1}');
			suc = reg.test(p);
			if (suc) {
				if (this.inputWidget) {
					if (this.inputWidget.options.cultureCalendar === "Japanese") {
						descriptors.push(this.createDescriptor(74));
						return true;
					}
				}
				descriptors.push(this.createDescriptor(72));
				return true;
			}

			if (this.isDisplayFormat) {
				reg = new RegExp('E{1,10}');
				suc = reg.test(p);
				if (suc) {
					descriptors.push(this.createDescriptor(75));
					return true;
				}

				reg = new RegExp('A{1,1}');
				suc = reg.test(p);
				if (suc) {
					descriptors.push(this.createDescriptor(80));
					return true;
				}
			}

			return false;
		}

		private daysInMonth(m: number, y: number) {
			m = m - 1;
			var d = new Date(y, ++m, 1, -1).getDate();
			return d;
		}

		private _isDigitString(s: string) {
			s = $.trim(s);
			if (s.length === 0) {
				return true;
			}

			var c = s.charAt(0), f, t;
			if (c === '+' || c === '-') {
				s = s.substr(1);
				s = $.trim(s);
			}
			if (s.length === 0) {
				return true;
			}
			try {
				f = parseFloat(s);
				t = f.toString();
				return t === s;
			}
			catch (e) {
				return false;
			}
		}

		private _internalSetDate(date) {
			if (this.inputWidget) {
				this.inputWidget._internalSetDate(date);
			}
		}

		private _isValidDate(dt) {
			return this.inputWidget._isValidDate(dt);
		}

		public _setFormat(format: string) {
			var culture = this.inputWidget._getCulture();
			if (culture == null) {
				return;
			}

			this.pattern = this._parseFormatToPattern(format);
			this.descriptors = this._parseFormat(this.pattern);
			this.fields = $.grep(this.descriptors, function (d) {
				return d.type !== -1;
			});
		}

		public _isEraFormatExist() {
			for (var i = 0; i < this.descriptors.length; i++) {
				if (this.descriptors[i].type >= 70 && this.descriptors[i].type <= 75) {
					return true;
				}
			}

			return false;
		}

        public getDate() {
            // a. If you don't want the delay to show inputting, please include the following codes.
            // But including it will cause another problem:
            // 1. when it has "2016/2/29", put the focus on the year portion.
            // 2. Input "2020", the value will be changed to "2020/2/28". 
            //    Because every when you input, the value will be changed according to your input.
            //    When you input the first character "2", the year will be changed to "0002". 
            //    It is a common year. So its value is changed to "0002/2/28".
            // b. If you want to fix this problem above, please exclude the following codes.
            if (this.inputWidget && this.inputWidget._isForceMonth() && this.inputWidget._tempDate) {
                return new Date(this.inputWidget._tempDate.getTime());
            }

			return (!!this.inputWidget) ?
				new Date(this.inputWidget._safeGetDate(true).getTime()) : undefined;
		}

		public setDate(value) {
			if (this.inputWidget) {
				this.inputWidget._setData(value);
			}
		}

		public getYear() {
			return this.getDate().getFullYear();
		}

        public setYear(val, resultObj, chkBounds?: boolean) {
            try {
                if (resultObj && resultObj.isfullreset) {
                    resultObj.offset = 1;
                    val = '1970';
                }
                if (typeof val === 'string') {
                    if (!this._isDigitString(val)) {
                        return false;
                    }
                }
                val = val * 1;

                var o = this.inputWidget.options,
                    minYear = 1,
                    maxYear = 9999, currentDate, testDate, mmm;

                if (chkBounds) {
                    if (o.minDate) {
                        minYear = Math.max(minYear, o.minDate.getFullYear());
                    }

                    if (o.maxDate) {
                        maxYear = Math.min(maxYear, o.maxDate.getFullYear());
                    }
                }

                if (resultObj && resultObj.isreset) {
                    val = minYear;
                }

                if (val < minYear) {
                    val = minYear;

                }

                if (val > maxYear) {
                    val = maxYear;
                }

                currentDate = this.getDate();
                testDate = new Date(currentDate.getTime());

                if (this.inputWidget && this.inputWidget._isForceMonth()) {
                    this._setJsFullYear(testDate, val, true);
                    if (this._isValidDate(testDate)) {
                        this._internalSetDate(testDate);
                        return true;
                    } else {
                        if (resultObj && resultObj.isreset) {
                            this._setJsFullYear(currentDate, 1, true);
                            this._internalSetDate(currentDate);
                            return true;
                        }
                        return false;
                    }
                } else {
                    testDate.setFullYear(val);
                    if (this._isValidDate(testDate)) {
                        mmm = this.daysInMonth(this.getMonth(), this.getYear());
                        if (mmm === currentDate.getDate()) {
                            testDate = new Date(currentDate.getTime());
                            testDate.setDate(1);
                            testDate.setFullYear(val);
                            //mmm = this.daysInMonth((testDate.getMonth() + 1),
                            //   testDate.getFullYear());
                            testDate.setDate(mmm);
                            if (this._isValidDate(testDate)) {
                                this._internalSetDate(testDate);
                                return true;
                            } else {
                                return false;
                            }
                        }
                        currentDate.setFullYear(val);
                        this._internalSetDate(currentDate);
                        return true;
                    }
                    else {
                        if (resultObj && resultObj.isreset) {
                            currentDate.setFullYear(1);
                            this._internalSetDate(currentDate);
                            return true;
                        }
                        return false;
                    }
                }
            }
            catch (e) {
                return false;
            }
        }

        private _getDaysInMonth(y, m): number {
            var d = new Date(y, m, 1, -1);
            return d.getDate();
        }

        _setJsFullYear(date, year, forceMonth = false) {
            if (forceMonth) {
                var currentDay = date.getDate();
                var mmm = this._getDaysInMonth(year, date.getMonth() + 1);
                if (mmm < currentDay) {
                    date.setDate(mmm);
                }
            }

            date.setFullYear(year);
        }

        _setJsMonth(date, month, forceMonth) {
            if (forceMonth) {
                var currentDay = date.getDate();
                var mmm = this._getDaysInMonth(date.getFullYear(), month + 1);
                if (mmm < currentDay) {
                    date.setDate(mmm);
                }
            }

            date.setMonth(month);
        }

		public getMonth() {
			return (this.getDate().getMonth() + 1);
		}

        public setMonth(val, allowChangeOtherParts, resultObj?) {
            try {
                if (resultObj && resultObj.isfullreset) {
                    val = '1';
                }
                val = val * 1;
                var currentDate = this.getDate(), mmm, testDate;
                if (typeof (allowChangeOtherParts) !== 'undefined' &&
                    !allowChangeOtherParts) {
                    if (val > 12 || val < 1) {
                        if (resultObj && resultObj.isreset) {
                            val = 1;
                        } else {
                            return false;
                        }
                    }
                }

                if (this.inputWidget && this.inputWidget._isForceMonth()) {
                    var currentDay = this.getDate().getDate();
                    testDate = new Date(currentDate.getTime());
                    this._setJsMonth(testDate, val - 1, true);
                    if (this._isValidDate(testDate)) {
                        this._internalSetDate(testDate);
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    mmm = this.daysInMonth(this.getMonth(), this.getYear());
                    if (mmm === this.getDate().getDate()) {
                        testDate = new Date(currentDate.getTime());
                        testDate.setDate(1);
                        testDate.setMonth(val - 1);
                        mmm = this.daysInMonth((testDate.getMonth() + 1),
                            testDate.getFullYear());
                        testDate.setDate(mmm);
                        if (this._isValidDate(testDate)) {
                            this._internalSetDate(testDate);
                            return true;
                        } else {
                            return false;
                        }
                    }
                    else {
                        testDate = new Date(currentDate.getTime());
                        testDate.setMonth(val - 1);
                        if (this._isValidDate(testDate)) {
                            this._internalSetDate(testDate);
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            } catch (e) {
                return false;
            }
        }

		public getHours() {
			return this.getDate().getHours();
		}

		public setHours(val, allowChangeOtherParts) {
			try {
				val = val * 1;
				if (typeof (allowChangeOtherParts) !== 'undefined' &&
					!allowChangeOtherParts) {
					if (val > 24) {
						return false;
					}
				}
				var testDate = new Date(this.getDate().getTime());
				testDate.setHours(val);
				if (this._isValidDate(testDate)) {
					this._internalSetDate(testDate);
					return true;
				} else {
					return false;
				}
			}
			catch (e) {
				return false;
			}
		}

		public getMinutes() {
			return this.getDate().getMinutes();
		}

		public setMinutes(val, allowChangeOtherParts) {
			try {
				val = val * 1;
				if (typeof (allowChangeOtherParts) !== 'undefined' &&
					!allowChangeOtherParts) {
					if (val > 60) {
						return false;
					}
				}
				var testDate = new Date(this.getDate().getTime());
				testDate.setMinutes(val);
				if (this._isValidDate(testDate)) {
					this._internalSetDate(testDate);
					return true;
				} else {
					return false;
				}
			}
			catch (e) {
				return false;
			}
		}

		public getSeconds() {
			return this.getDate().getSeconds();
		}

		public setSeconds(val, allowChangeOtherParts) {
			try {
				val = val * 1;
				if (typeof (allowChangeOtherParts) !== 'undefined' &&
					!allowChangeOtherParts) {
					if (val > 60) {
						return false;
					}
				}
				var testDate = new Date(this.getDate().getTime());
				testDate.setSeconds(val);
				if (this._isValidDate(testDate)) {
					this._internalSetDate(testDate);
					return true;
				} else {
					return false;
				}
			}
			catch (e) {
				return false;
			}
		}

		public getDayOfMonth() {
			return this.getDate().getDate();
		}

		public setDayOfMonth(val, allowChangeOtherParts, resultObj?): boolean {
			try {
				if (resultObj && resultObj.isfullreset) {
					return this.setDayOfMonth(1, allowChangeOtherParts);
				}
				var currentDate = this.getDate(), mmm, testDate;
				val = val * 1;
				if (typeof (allowChangeOtherParts) !== 'undefined' &&
					!allowChangeOtherParts) {
					mmm = this.daysInMonth(this.getMonth(), this.getYear());
					if (val > mmm || val < 1) {
						if (resultObj && resultObj.isreset) {
							return this.setDayOfMonth(1,
								allowChangeOtherParts, resultObj);
						}
						return false;
					}
				}
				testDate = new Date(currentDate.getTime());
				testDate.setDate(val);
				if (this._isValidDate(testDate)) {
					this._internalSetDate(testDate);
					return true;
				} else {
					return false;
				}
			}
			catch (e) {
				return false;
			}
		}

		public getDayOfWeek() {
			return (this.getDate().getDay() + 1);
		}

		public setDayOfWeek(val) {
			try {
				val = val * 1;
				var aDif = val - this.getDayOfWeek();
				return this.setDayOfMonth(this.getDayOfMonth() + aDif, true);
			}
			catch (e) {
				return false;
			}
		}

		public _getCulture() {
			return this.inputWidget._getCulture();
		}

		public findAlikeArrayItemIndex(arr: string[], txt: string) {
			var index = -1,
				pos = 99999, i, k;
			for (i = 0; i < arr.length; i++) {
				k = arr[i].toLowerCase().indexOf(txt.toLowerCase());
				if (k !== -1 && k < pos) {
					pos = k;
					index = i;
				}
			}
			return index;
		}

		public toString(): string {
			if (this._isEraFormatExist()) {
				if (this.inputWidget.options.date != null) {
					var minDate = this.inputWidget._getRealEraMinDate();
					var maxDate = this.inputWidget._getRealEraMaxDate();
					if (this.inputWidget.options.date < minDate || this.inputWidget.options.date > maxDate) {
						return "";
					}
				}
			}

			var s = '', l = 0, i, txt, j;
			this.desPostions = new Array(0);
			for (i = 0; i < this.descriptors.length; i++) {
				this.descriptors[i].startIndex = s.length;
				txt = '' || this.descriptors[i].getText();
				s += txt;
				for (j = 0; j < txt.length; j++) {
					this.desPostions.push({
						desc: this.descriptors[i],
						pos: j,
						text: txt,
						length: txt.length
					});
					l++;
					if (this.desPostions.length !== l) {
						throw 'Fatal Error !!!!!!!!!!!!!!!';
					}
				}
			}
			return s;
		}

	};


	//#region format library
	/** @ignore */
	export class wijInputDateImpl implements IwijInputDate {
        _tempDate: Date;

		constructor(public options: IDateOptions) {
			options.format = options.format || "M/d/yyyy";
			options.amDesignator = options.amDesignator || "";
			options.pmDesignator = options.pmDesignator || "";
			if (options.hour12As0 === undefined) {
				options.hour12As0 = false;
			}
			if (options.midnightAs0 === undefined) {
				options.midnightAs0 = true;
			}
		}

		_getInnerIncrement() {
			return 1;
		}

		_getCulture() {
			return Globalize.findClosestCulture(this.options.culture);
		}

		_getRealEraMaxDate() {
			if (this.options.maxDate) {
				return DateTimeInfo.GetEraMax() < this.options.maxDate ? DateTimeInfo.GetEraMax() : this.options.maxDate;
			}

			return DateTimeInfo.GetEraMax();
		}

		_getRealEraMinDate() {
			if (this.options.minDate) {
				return DateTimeInfo.GetEraMin() > this.options.minDate ? DateTimeInfo.GetEraMin() : this.options.minDate;
			}

			return DateTimeInfo.GetEraMin();
		}

		_safeGetDate(ignoreCheckRange?: boolean) {
			var date = this.options.date;
			if (date == null) {
				date = new Date();
			}
			if (!ignoreCheckRange) {
				date = this._checkRange(date);
			}
			return date;
		}

		_safeSetDate(date, ignoreCheckRange?: boolean) {
			var cache = date;

			if (!ignoreCheckRange) {
				date = this._checkRange(date);
			}

			if (isNaN(date)) {
				date = cache;
			}

			this.options.date = date;
			return true;
		}

		_getAllowSpinLoop() {
			return false;
		}

		_getRealMinDate() {
			if (this.options.minDate) {
				return this.options.minDate;
			}

			var minDate = new Date(1, 0, 1, 0, 0, 0);
			minDate.setFullYear(1);

			return minDate;
		}

		_getRealMaxDate() {
			return this.options.maxDate ? this.options.maxDate : new Date(9999, 11, 31, 23, 59, 59);
		}

		_setData(val) {
			this.options.date = val;
		}

		_isValidDate(date, chkBounds?: boolean) {
			if (date === undefined) {
				return false;
			}

			if (isNaN(date)) {
				return false;
			}

			if (date.getFullYear() < 1 || date.getFullYear() > 9999) {
				return false;
			}

			if (chkBounds) {
				if (this.options.minDate) {
					if (date < this.options.minDate) {
						return false;
					}
				}

				if (this.options.maxDate) {
					if (date > this.options.maxDate) {
						return false;
					}
				}
			}

			return true;
		}

		_getInnerAmDesignator() {
			return this.options.amDesignator == "" ? this._getStandardAMPM("AM") : this.options.amDesignator;
		}

		_getInnerPmDesignator() {
			return this.options.pmDesignator == "" ? this._getStandardAMPM("PM") : this.options.pmDesignator;
		}

		isFocused() { return false; }

		_internalSetDate(date) { }

		private _checkRange(date: Date) {
			if (date) {
				if (this.options.minDate && date < this.options.minDate) {
					date = new Date(Math.max(this.options.minDate.valueOf(), date.valueOf()));
				}

				if (this.options.maxDate && date > this.options.maxDate) {
					date = new Date(Math.min(this.options.maxDate.valueOf(), date.valueOf()));
				}
			}

			return date;
		}

		private _getStandardAMPM(value) {
			var culture = this._getCulture();
			if (culture && culture.calendar) {
				var tmp = culture.calendars.standard[value];
				if (tmp) {
					return tmp[0];
				}
			}
			return value;
        }

        _isForceMonth(): boolean {
            return false;
        }
	}

	function dateFormatter(date: Date, format?: string, options?: IDateOptions): string {
		if (!(date instanceof Date)) {
			return "";
		}
		if (!format) {
			format = 'd';
		}

		if (!options) {
			if (typeof format === "string") {   //$.wijinputcore.format(new Date(2013,8,12), "yyyy/MM/dd");
				options = <IDateOptions>{};
			}
			else {  //$.wijinputcore.format(new Date(2013, 8, 12), { format: "yyyy/MM/dd" });
				options = <any>format;
			}
		}
		if (typeof format === "string") {   //$.wijinputcore.format(new Date(2013, 8, 12), "yyyy/MM/dd", { culture: "ja-JP" });
			options.format = format;
		}
		options.date = date;

		var wijInputDate = new wijInputDateImpl(options);

		var _formatter = new wijDateTextFormatter(wijInputDate, options.format, true);

		return _formatter.toString();
	}

	var $ = jQuery;
	$.wijinputcore = $.wijinputcore || (<wijmo.input.IFormatValidateLib>{});;
	$.wijinputcore.formatdate = $.wijinputcore.formatdate || dateFormatter;
	//#endregion

	//#region parse library
	function dateParser(value: string, format?: string, culture?: string): Date {
		if (Globalize.findClosestCulture(format)) {
			culture = format;
			format = undefined;
		}

		var cf = Globalize.findClosestCulture(culture).calendars.standard,
			pattern = cf.patterns.d;

		if (format) {
			if (format.length <= 1) {
				pattern = parseShortPattern(format, cf.patterns);
			}
			else {
				pattern = format;
			}
		}
		else {
			pattern = cf.patterns.d;
		}

		var wijInputDate = new wijInputDateImpl(<IDateOptions>{});
		var _formatter = new wijDateTextFormatter(wijInputDate, pattern, true);

		if (hasEraYear(_formatter.descriptors)) {
			return parseEraDate(value, _formatter, cf);
		}
		else {
			return Globalize.parseDate(value, pattern, culture);
		}
	}

	function hasEraYear(descriptors) {
		for (var i = 0; i < descriptors.length; i++) {
			if (descriptors[i].type === DescriptorType.EraYear ||
				descriptors[i].type === DescriptorType.TwoEraYear ||
				descriptors[i].type === DescriptorType.EraName ||
				descriptors[i].type === DescriptorType.TwoEraName ||
				descriptors[i].type === DescriptorType.ThreeEraName ||
				descriptors[i].type === DescriptorType.EraYearBig) {
				return true;
			}
		}
		return false;
	}

	function parseEraDate(value, _formatter, cultureFormat) {
		var ch = "", chs = "", charIndex = 0;
		var era = 0,
			eraYear = 1,
			month = 1,
			date = 1,
			hour = 0,
			minute = 0,
			second = 0;

		var isNextReachSeparator = function () {
			return (charIndex < value.length &&
				i + 1 < _formatter.descriptors.length &&
				_formatter.descriptors[i + 1].type === DescriptorType.liternal &&
				value.charAt(charIndex) !== (<_dateDescriptor>_formatter.descriptors[i + 1]).liternal ||
				i === _formatter.descriptors.length - 1 && charIndex + 1 <= value.length);
		};

		for (var i = 0; i < _formatter.descriptors.length; i++) {
			var breakToNextDesc = false;

			for (; charIndex < value.length;) {

				ch = value.charAt(charIndex++);
				chs += ch;

				for (var j = 0; j < DateTimeInfo.GetEraCount(); j++) {
					var isMatched_g =
						chs.toLocaleLowerCase() === DateTimeInfo.GetEraShortNames()[j].toLowerCase() &&
						_formatter.descriptors[i].type === DescriptorType.EraName;
					var isMatched_gg =
						chs.toLocaleLowerCase() === DateTimeInfo.GetEraAbbreviations()[j].toLowerCase() &&
						_formatter.descriptors[i].type === DescriptorType.TwoEraName;
					var isMatched_ggg =
						chs.toLocaleLowerCase() === DateTimeInfo.GetEraNames()[j].toLowerCase() &&
						_formatter.descriptors[i].type === DescriptorType.ThreeEraName;

					if (isMatched_g || isMatched_gg || isMatched_ggg) {
						era = j;
						chs = "";
						breakToNextDesc = true;
						break;
					}
				}
				if (breakToNextDesc) {
					break;
				}

				if (_formatter.descriptors[i].type === DescriptorType.liternal &&
					(<_dateDescriptor>_formatter.descriptors[i]).liternal === chs) {
					chs = "";
					break;
				}

				switch (_formatter.descriptors[i].type) {
					case DescriptorType.EraYear:
					case DescriptorType.TwoEraYear:
						if (isNextReachSeparator()) {
							break;
						}

						eraYear = parseInt(chs, 10);
						if (isNaN(eraYear)) {
							eraYear = 1;
						}
						chs = "";
						breakToNextDesc = true;
						break;
					case DescriptorType.EraYearBig:
						if (isNextReachSeparator()) {
							break;
						}

						if (chs === '\u5143') {
							eraYear = 1;
						}
						else {
							eraYear = parseInt(chs, 10);
							if (isNaN(eraYear)) {
								eraYear = 1;
							}
						}
						chs = "";
						breakToNextDesc = true;
						break;
					case DescriptorType.TwoDigitMonth:
					case DescriptorType.Month:
						if (isNextReachSeparator()) {
							break;
						}

						month = parseInt(chs, 10);
						if (isNaN(month)) {
							month = 1;
						}
						chs = "";
						breakToNextDesc = true;
						break;
					case DescriptorType.AbbreviatedMonthNames:
						if (isNextReachSeparator()) {
							break;
						}
						for (var m = 0; m < cultureFormat.months.namesAbbr.length; m++) {
							if (chs.toLocaleLowerCase() === cultureFormat.months.namesAbbr[m]) {
								month = m + 1;
								chs = "";
								break;
							}
						}
						breakToNextDesc = true;
						break;
					case DescriptorType.MonthNames:
						if (isNextReachSeparator()) {
							break;
						}
						for (var k = 0; k < cultureFormat.months.names.length; k++) {
							if (chs.toLocaleLowerCase() === cultureFormat.months.names[k]) {
								month = k + 1;
								chs = "";
								break;
							}
						}
						breakToNextDesc = true;
						break;
					case DescriptorType.TwoDigityDayOfMonth:
					case DescriptorType.DayOfMonth:
						if (isNextReachSeparator()) {
							break;
						}

						date = parseInt(chs, 10);
						if (isNaN(date)) {
							date = 1;
						}
						chs = "";
						breakToNextDesc = true;
						break;

					case DescriptorType.h:
					case DescriptorType.hh:
					case DescriptorType.H:
					case DescriptorType.HH:
						if (isNextReachSeparator()) {
							break;
						}

						hour = parseInt(chs, 10);
						if (isNaN(hour)) {
							hour = 1;
						}
						chs = "";
						breakToNextDesc = true;
						break;

					case DescriptorType.m:
					case DescriptorType.mm:
						if (isNextReachSeparator()) {
							break;
						}

						minute = parseInt(chs, 10);
						if (isNaN(minute)) {
							minute = 1;
						}
						chs = "";
						breakToNextDesc = true;
						break;
					case DescriptorType.s:
					case DescriptorType.ss:
						if (isNextReachSeparator()) {
							break;
						}

						second = parseInt(chs, 10);
						if (isNaN(second)) {
							second = 1;
						}
						chs = "";
						breakToNextDesc = true;
						break;

					case DescriptorType.ShortAmPm:
					case DescriptorType.AmPm:
						if (isNextReachSeparator()) {
							break;
						}
						for (var l = 0; l < cultureFormat.PM.length; l++) {
							if (chs.toLocaleLowerCase() === cultureFormat.PM[l]) {
								hour += 12;
								break;
							}
						}
						chs = "";
						breakToNextDesc = true;
						break;

					case DescriptorType.OneDigitYear:
					case DescriptorType.TwoDigitYear:
					case DescriptorType.FourDigitYear:

					case DescriptorType.AbbreviatedDayNames:
					case DescriptorType.DayNames:

					case DescriptorType.AD:
						if (isNextReachSeparator()) {
							break;
						}

						chs = "";
						breakToNextDesc = true;
						break;

					default:
				}
				if (breakToNextDesc) {
					break;
				}
			}

		}

		return DateTimeInfo.ConvertToGregorianDate(era, eraYear, month, date, hour, minute, second, false);
	}

	function parseShortPattern(shortPattern: string, culturePatterns) {
		var pattern = shortPattern;
		if (shortPattern && shortPattern.length <= 1) {
			switch (shortPattern) {
				case "":
				case "d": // ShortDatePattern
					pattern = culturePatterns.patterns.d;
					break;
				case "D": // LongDatePattern
					pattern = culturePatterns.patterns.D;
					break;
				case "f": // Full date and time (long date and short time)
					pattern = culturePatterns.patterns.D + " " + culturePatterns.patterns.t;
					break;
				case "F": // Full date and time (long date and long time)
					pattern = culturePatterns.patterns.D + " " + culturePatterns.patterns.T;
					break;
				case "g": // General (short date and short time)
					pattern = culturePatterns.patterns.d + " " + culturePatterns.patterns.t;
					break;
				case "G": // General (short date and long time)
					pattern = culturePatterns.patterns.d + " " + culturePatterns.patterns.T;
					break;
				case "m": // MonthDayPattern
					pattern = culturePatterns.patterns.M;
					break;
				case "M": // monthDayPattern
					pattern = culturePatterns.patterns.M;
					break;
				case "s": // SortableDateTimePattern
					pattern = culturePatterns.patterns.S;
					break;
				case "t": // shortTimePattern
					pattern = culturePatterns.patterns.t;
					break;
				case "T": // LongTimePattern
					pattern = culturePatterns.patterns.T;
					break;
				case "u": // UniversalSortableDateTimePattern
					pattern = culturePatterns.patterns.S;
					break;
				case "U":
					// Full date and time (long date and long time) using universal time
					pattern = culturePatterns.patterns.D + " " + culturePatterns.patterns.T;
					break;
				case "y": // YearMonthPattern
					pattern = culturePatterns.patterns.Y;
					break;
				case "Y": // yearMonthPattern
					pattern = culturePatterns.patterns.Y;
					break;
				case "r": // RFC1123Pattern
				case "R":
					pattern = "ddd, dd MMM yyyy HH:mm:ss G\\M\\T";
					break;
			}
		}
		return pattern;
	}



	$.wijinputcore.parseDate = $.wijinputcore.parseDate || dateParser;
	//#endregion

	//#region validate library
	function dateValidator(value: string, minDate: Date, maxDate: Date, format?: string, culture?: string): boolean {
		var dateValue = dateParser(value, format, culture);
		return dateValue >= minDate && dateValue <= maxDate;
	}


	$.wijinputcore.validateDate = $.wijinputcore.validateDate || dateValidator;
	//#endregion
}

interface Window {
	eras: any;
}