﻿/// <reference path="jquery.wijmo.wijstringinfo.ts"/>
/// <reference path="jquery.wijmo.wijinputcore.ts"/>
/// <reference path="jquery.wijmo.wijinpututility.ts"/>
/// <reference path="jquery.wijmo.wijinputmaskcore.ts"/>
/// <reference path="jquery.wijmo.wijinputmaskformat.ts"/>

/*globals wijinputcore wijMaskedTextProvider wijCharDescriptor wijInputResult jQuery*/
/*
 * Depends:
 *	jquery-1.4.2.js
 *	jquery.ui.core.js
 *	jquery.ui.widget.js
 *	jquery.ui.position.js
 *	jquery.effects.core.js	
 *	jquery.effects.blind.js
 *	globalize.js
 *	jquery.wijmo.wijpopup.js
 *	jquery.wijmo.wijcharex.js
 *	jquery.wijmo.wijstringinfo.js
 *	jquery.wijmo.wijinputcore.js
 *  jquery.wijmo.wijinputmaskcore.js
 *  jquery.wijmo.wijinputmaskformat.js
 *
 */

module wijmo.input {


	var $ = jQuery;

	//#region wijinputMask



	/** @widget */
	export class wijinputmask extends wijinputcore implements IwijInputMask {

		private static _imMaskAssociatedoptions: string[] =
		["disabled", "disableUserInput", "blurOnLastChar", "imeMode", "blurOnLeftRightKey",
			"tabAction", "nullText", "placeholder", "readonly", "promptChar", "autoConvert",
			"showNullText", "hideEnter", "hidePromptOnLeave", "highlightText"];

		public _textProvider: wijMaskedTextProvider;
		private _imMask: MaskControl = null;
		private _advanceMode = false;

		public _createTextProvider() {
			this._textProvider = new wijMaskedTextProvider(this, this.options.mask, false);
		}

		private _checkMaskAndPassword() {
			var noMask = !this.options.mask || this.options.mask.length < 0;
			if (noMask && this._isPassword()) {
				throw 'Option "passwordChar" requires a mask';
			}
		}

		public _updateIsPassword() {
			var isPassword = this.options.passwordChar.length > 0 && this.element.attr('type') !== 'password';
			this.element.data('isPassword', isPassword);
			this._checkMaskAndPassword();
		}

		public _beginUpdate() {
			var element = this.element;
			element.addClass(this.options.wijCSS.wijinputmask);
			element.attribute("aria-label", "wijinputmask");
			this._updateIsPassword();
			element.data('defaultText', this.options.text);
		}

		public _setOption(key: string, value) {
			switch (key) {
				case "maskFormat":
					this._super(key, value);
					key = "mask";
					break;
			}

			super._setOption(key, value);

			if (this._advanceMode) {
				if (wijinputmask._imMaskAssociatedoptions.indexOf) {
					if (wijinputmask._imMaskAssociatedoptions.indexOf(key) !== -1) {
						this._syncProperties();
					}
				}
				else {
					for (var i = 0; i < wijinputmask._imMaskAssociatedoptions.length; i++) {
						if (wijinputmask._imMaskAssociatedoptions[i] === key) {
							this._syncProperties();
							break;
						}
					}
				}
			}

			switch (key) {
				case 'text':
					if (this._advanceMode && this.options.textWithPromptAndLiterals) {
						value = this.options.textWithPromptAndLiterals;
					}
					this.setText(value);
					break;

				case 'mask':
				case 'culture':
					if (typeof (value) === 'undefined' || value.length <= 0) {
						value = "";
					}
					this._checkMaskAndPassword();
					var text = this.getText();
					if (text === this._getNullText()) {
						text = "";
					}
					if (key === "mask") {
						if (!this._isAdvanceMode() && value instanceof RegExp) {
							return;
						}
						this._textProvider.mask = value;
					}
					this._checkFormatPattern(value);
					if (this._advanceMode) {
						this._imMask.SetText(text);
						this._updateText();
					}
					else {
						this._textProvider.initialize();
						this._textProvider.setText(text);
						this._updateText();
					}

					break;
				case 'promptChar':
				case 'nullText':
					if (this._textProvider) {
						this._textProvider.updatePromptChar();
						this._updateText();
					}

					break;
				case 'hidePromptOnLeave':
				case 'resetOnPrompt':
					this._updateText();
					break;
				case 'passwordChar':
					this._updateIsPassword();
					this._updateText();
					break;
			}
		}

		private _isAdvanceMode() {
			return typeof (MaskControl) !== "undefined";
		}

		private _checkFormatPattern(value) {
			if (value instanceof RegExp || this.options.formatMode === "advanced") {
				if (!this._advanceMode) {
					this._detachInputEvent();
					if (!this._imMask) {
						this._imMask = new MaskControl();
						this._addimMaskCustomEvent();
					}
					this._imMask.AttachInput(this.element.get(0));
					this._attachKeyEventForDropDownList();

					this._advanceMode = true;
				}
				if (value instanceof RegExp) {
					var reg: RegExp = value;
					this._imMask.SetFormatPattern(reg.source);
				}
				else {
					this._imMask.SetFormatPattern(value);
				}
				this._syncProperties();
			}
			else {
				if (this._advanceMode) {
					this._imMask.DetachInput();
					this._detachEventForDropDownList();
					super._attachInputEvent();
				}
				this._advanceMode = false;
			}
		}

		private _attachKeyEventForDropDownList() {
			this.element.bind({
				'keydown.wijinput': $.proxy(this._onKeyDownForDropDownList, this)
			});
		}

		private _detachEventForDropDownList() {
			this.element.unbind(".wijinput");
		}

		private _onKeyDownForDropDownList(e) {
			super._processKeyForDropDownList(e);
		}

		private _syncProperties() {
			this._imMask.SetEnabled(!this._isDisabled());
			this._imMask.SetReadOnly(!!this.options.disableUserInput);
			this._imMask.SetExitOnLastChar(!!this.options.blurOnLastChar);
			this._imMask.SetImeMode(this.options.imeMode);
			this._imMask.SetExitOnLeftRightKey(this._convertExistOnLeftRightKey(this.options.blurOnLeftRightKey));
			this._imMask.SetTabAction(this._convertTabAction(this.options.tabAction));
			this._imMask.SetDisplayNull(this.options.nullText);

			this._imMask.SetHideEnter(!!this.options.hideEnter);

			this._imMask.SetPromptChar(this.options.promptChar);
			this._imMask.SetAutoConvert(!!this.options.autoConvert);
			this._imMask.SetHidePromptOnLeave(!!this.options.hidePromptOnLeave);
			this._imMask.SetHighlightText(this._convertHighlightText(this.options.highlightText));
		}
		private _convertExistOnLeftRightKey(str: string) {
			if (!str) {
				str = "none";
			}
			str = str.toLowerCase();
			if (str === "none") {
				return ExitOnLeftRightKey.None;
			}
			else if (str === "left") {
				return ExitOnLeftRightKey.Left;
			}
			else if (str === "right") {
				return ExitOnLeftRightKey.Right;
			}
			else if (str === "both") {
				return ExitOnLeftRightKey.Both;
			}
		}

		private _convertHighlightText(str: string) {
			if (!str) {
				return HighlightText.None;
			}

			str = str.toLowerCase();
			if (str === "field") {
				return HighlightText.Field;
			}
			else if (str === "all") {
				return HighlightText.All;
			}
			else {
				return HighlightText.None;
			}
		}

		private _convertTabAction(str: string) {
			if (!str) {
				str = "control";
			}
			str = str.toLowerCase();
			if (str === "control") {
				return TabAction.Control;
			}
			else {
				return TabAction.Field;
			}
		}

		private _addimMaskCustomEvent() {
			this._imMask.OnInvalidInput((e) => { this._fireIvalidInputEvent(""); });
			this._imMask.OnTextChanged((e) => { this._raiseTextChanged(); });
			this._imMask.OnEditingTextChanged((e) => { this._updateText(true) });
			this._imMask.OnKeyExit((e) => { this._trigger("keyExit"); });
		}

		public _processClearButton() {
			this._textProvider.clear(new wijInputResult());
			this._updateText();
			var firstEditPosition = this._textProvider.findEditPositionFrom(0, true);
			this.selectText(firstEditPosition, firstEditPosition);
		}

		public _resetData() {
			var txt = this.element.data('defaultText');
			if (txt === undefined || txt === null) {
				txt = this.element.data('elementValue');
			}

			if (txt === undefined || txt === null) {
				txt = "";
			}

			this.setText(txt);
		}

		_isPassword() {
			return !!this.element.data('isPassword');
		}

		private _getTextWithPrompts() {
			return !this._isInitialized() ? this.element.val() : this._textProvider.toString(true, true, false);
		}

		private _getTextWithLiterals(): string {
			return !this._isInitialized() ? this.element.val() : this._textProvider.toString(true, false, true);
		}

		private _getTextWithPromptAndLiterals(): string {
			return !this._isInitialized() ? this.element.val() : this._textProvider.toString(true, true, true);
		}

		private _getTextWithPromptAndLiteralsAndPassword(): string {
			return !this._isInitialized() ? this.element.val() : this._textProvider.toString(false, true, true);
		}



		public _onChange() {
			if (!this.element || this._imeCompostiing) {
				return;
			}
			var val = this.element.val();
			if (this.getText() !== val
				&& this._getTextWithPrompts() !== val
				&& this._getTextWithPromptAndLiterals() !== val) {
				if (this._textProvider.getPasswordChar() !== "") {
                    if (this._getTextWithPromptAndLiteralsAndPassword() !== val) {
                        if (this.element.data("customRaiseChange") == true && !!this.options.hidePromptOnLeave) {
                             // DaryLuo 2015/1026 fix bug 128164.
                        } else {
                            this.setText(val);    
                        }
						
					}
				}
				else {
					this.setText(val);
				}
			}
		}

		_onMouseDown(e) {
			super._onMouseDown(e);
			this.element.data('prevSelection', null);
		}

		public _afterFocused() {
			if (this._isNullText() || !!this.options.hidePromptOnLeave) {
				this._doFocus();
			}
			if (this.options.highlightText === "none") {
				var prevPos = this.element.data('prevSelection');
				if (prevPos) {
					this.element.wijtextselection(prevPos.start, prevPos.end);
				}
			}
			else if (this.options.highlightText === "field") {
				var fieldState = this._getCurrentField();
				var currentField = fieldState.currentField;
				var fieldList = fieldState.fieldList;
				if (currentField >= fieldList.length) {
					currentField = fieldList.length - 1;
				}
				var curField = fieldList[currentField];
				if (curField) {
					this.selectText(curField.start, curField.end + 1);
				}
			}
			else if (this.options.highlightText === "all") {
				var input = <HTMLInputElement>this.element.get(0);
				this.selectText(0, input.value.length);
			}
		}

		public _processTabKey(e): boolean {
			var tabAction = this.options.tabAction;
			tabAction = tabAction ? tabAction.toLowerCase() : "control";
			if (tabAction === "control") {

				setTimeout(() => {
					this._trigger("keyExit");
				}, 0);
				return false;
			}
			else {
				return this._moveField(!e.shiftKey);
			}
		}

		private _getCurrentField() {
			var fieldList = this._textProvider._getFieldList();
			var range = this.element.wijtextselection();
			var start = range.start;
			var currentField = -1;

			if (fieldList.length > 0) {
				if (start > fieldList[fieldList.length - 1].end) {
					currentField = fieldList.length;
				}
			}
			if (currentField === -1) {
				for (var i = 0; i < fieldList.length; i++) {
					if (start >= fieldList[i].start && start <= fieldList[i].end) {
						currentField = i;
						break;
					}

					if (start < fieldList[i].start) {
						if (i != 0) {
							currentField = i;
						}
						else {
							currentField = -1;
						}
						break;
					}
				}
			}

			var ret: any = {};
			ret.currentField = currentField;
			ret.fieldList = fieldList;
			return ret;
		}

		private _moveField(direction): boolean {
			var fieldState = this._getCurrentField();
			var currentField = fieldState.currentField;
			var fieldList = fieldState.fieldList;
			if (direction) {
				if (currentField === fieldList.length - 1) {
					return false;
				}
				this.element.wijtextselection(fieldList[currentField + 1].start, fieldList[currentField + 1].start);
			}
			else {
				if (currentField <= 0) {
					return false;
				}
				this.element.wijtextselection(fieldList[currentField - 1].start, fieldList[currentField - 1].start);
			}


			return true;
		}

		public _processLeftRightKey(isLeft: boolean) {
			var range = this.element.wijtextselection();
			var exitLeftRightKey = this.options.blurOnLeftRightKey ? this.options.blurOnLeftRightKey.toLowerCase() : "none";

			if (isLeft && range.start === 0) {
				if (exitLeftRightKey === "both" || exitLeftRightKey === "left") {
					this._moveControl(this.element.get(0), false, true);
					return true;
				}
			}
			else {

				var exitSettingToRight = exitLeftRightKey === "both" || exitLeftRightKey === "right";
				var caretInLast = false;
				if (this._textProvider.noMask && range.start === this.options.text.length) {
					caretInLast = true;
				}
				else if (!this._textProvider.noMask && range.start === this._textProvider.testString.length) {
					caretInLast = true;
				}
				if (!isLeft && exitSettingToRight && caretInLast) {
					this._moveControl(this.element.get(0), true, true);
					return true;
				}
			}
			return false;
		}

		_onKeyPress(e) {
			if (!this._textProvider.noMask && this.options.blurOnLastChar) {
				var before = this._textProvider._isLastCharAssigned();
			}
			super._onKeyPress(e);
			if (!this._textProvider.noMask && this.options.blurOnLastChar) {
				var end = this._textProvider._isLastCharAssigned();
				if (!before && end) {
					this._moveControl(this.element.get(0), true, true);
				}
			}
		}


		/** Sets the text displayed in the input box.
		  * @example
		  * // This example sets text of a wijinputcore to "Hello"
		  * $(".selector").wijinputcore("setText", "Hello");
		  */
		setText(text) {
			if (text === undefined) {
				return;
			}
			if (this._advanceMode) {
				if (text == this.options.textWithPromptAndLiterals) {
					this.options.textWithPromptAndLiterals = undefined;
					this._imMask.SetText(text);
				}
				else {
					this._imMask.SetValue(text);
				}
			}
			else {
				super.setText(text);
			}
		}

		/** Gets the text displayed in the input box.
		  */
		getText() {
			if (this._advanceMode) {
				return this._imMask.GetTextOnSimpleMode();
			}
			else {
				return super.getText();
			}
		}

		/** Selects a range of text in the widget.
		  * @param {Number} start Start of the range.
		  * @param {Number} end End of the range. 
		  * @example
		  * // Select first two symbols in a wijinputmask
		  * $(".selector").wijinputmask("selectText", 0, 2);
		  */
		selectText(start = 0, end = this.getText().length) {
			if (isNaN(start)) {
				start = 0;
			}
			if (isNaN(end)) {
				end = 0;
			}
			if (this._advanceMode) {
				this._imMask.SetSelectionStart(start);
				this._imMask.SetSelectionLength(Math.abs(end - start));
				this._imMask.SetFocus();
			}
			else {
				super.selectText(start, end);
			}
		}

		/**  Gets the selected text. 
		  */
		getSelectedText() {
			if (this._advanceMode) {
				return this._imMask.GetSelectedText();
			}
			else {
				return super.getSelectedText();
			}
		}



		_attachInputEvent() {
			if (this._advanceMode) {
				this._imMask.AttachInput(this.element.get(0));
			}
			else {
				super._attachInputEvent();
			}
		}

		_detachInputEvent() {
			if (this._advanceMode) {
				this._imMask.DetachInput();
			}
			else {
				super._detachInputEvent();
			}
		}

		/** Gets the text value when the container 
		  * form is posted back to server.
		  */
		getPostValue() {
			if (this._advanceMode) {
				return this._imMask.GetPostValueOnSimpleMode();
			}
			else {
				return super.getPostValue();
			}
		}

		_initialize() {
			if (this.options.maskFormat) {
				this.options.mask = this.options.maskFormat;
			}
			super._initialize();
		}

		_init() {
			super._init();

			var val = this.options.text || this.element.val();
			if (this.options.mask) {
				this._setOption("mask", this.options.mask);
			}

			if (val && val !== this._getNullText()) {
				this._setOption("text", val);
			}

			if (!CoreUtility.IsIE()) {
				this.element.data('prevSelection', { "start": 0, "end": 0 });
			}
		}

		_updateText(keepSelection = false) {

			if (!this.element.data("initialized")) {
				if (this.options.mask instanceof RegExp || this.options.formatMode === "advanced") {
					return;
				}
			}
			if (this._advanceMode) {
				if (!this._isInitialized()) {
					return;

				}
				if (this.element.is(':disabled')) {
					return;
				}
				var text = this._imMask.GetTextOnSimpleMode();
				this.options.advancedText = this._imMask.GetText();
				if (text !== this.options.text) {
					this.options.text = text;
					this._raiseTextChanged();
				}
			}
			else {
				super._updateText(keepSelection);
				// DaryLuo 2014/04/04 fix bug 48485.
				// placeHolder value should never set to the option's text value.
				if (this.options.text === this._getNullText()) {
					this.options.text = "";
				}
			}
		}

		/** Determines whether the widget has the focus. 
		  */
		isFocused() {
			if (this._advanceMode) {
				return this._imMask.IsFocused();
			}
			else {
				return super.isFocused();
			}
		}

		public _getAllowPromptAsInput(): boolean {
			return this.options.allowPromptAsInput;
		}

		public _getPasswordChar(): string {
			return this.options.passwordChar
		}

		public _getResetOnPrompt(): boolean {
			return this.options.resetOnPrompt;
		}

		public _getResetOnSpace(): boolean {
			return this.options.resetOnSpace;
		}

		public _getSkipLiterals(): boolean {
			return this.options.skipLiterals;
		}

		public _getHidePromptOnLeave(): boolean {
			return this.options.hidePromptOnLeave;
		}

		public _getPromptChar(): string {
			return this.options.promptChar;
		}

		public _getAutoConvert(): boolean {
			return this.options.autoConvert;
		}

		public _getNullText(): string {
			return this.options.nullText;
		}

	}

	class wijinputmask_options {
		wijCSS = {
			wijinputmask: wijinputcore.prototype.options.wijCSS.wijinput + "-mask"
		};

		/** Gets whether the control automatically converts to the proper format according to the format setting.
		  * @example
		  * // In this example, when input the lower case alphabet, it will be convert to upper case alphabet.
		  * $(".selector").wijinputmask({    
		  *   maskFormat: /\A{3}-\A{4}/,
		  *   autoConvert:true
		  * });  
		  */
		autoConvert = true;

		/** Determines the default text. 
		  */
		text: string = null;

		/** Determines whether to highlight the control's text on receiving input focus.
		  * Possible values are : "none", "field", "all";
		  * The default value is "none";
		  */
		highlightText: string = "none";

		/** Determines the input mask to use at run time. 
		  * Mask must be a string composed of one or more of the masking elements. 
		  * Obsoleted, use maskFormat instead.
		  * @ignore
		  */
		mask = "";

		/** Determines the input mask to use at run time. 
		  * @remarks
		  * <pre>
		  * The property can be a string composed of one or more of the masking elements as shown in the following table,
		  * 0  --------- Digit, required. This element will accept any single digit between 0 and 9.<br />
		  * 9  --------- Digit or space, optional. 
		  * #  --------- Digit or space, optional. Plus (+) and minus (-) signs are allowed.      
		  * L  --------- Letter, required. Restricts input to the ASCII letters a-z and A-Z.      
		  *              This mask element is equivalent to [a-zA-Z] in regular expressions.      
		  * ?  --------- Letter, optional. Restricts input to the ASCII letters a-z and A-Z.      
		  *              This mask element is equivalent to [a-zA-Z]? in regular expressions.     
		  * &amp;  --------- Character, required.<br />
		  * C  --------- Character, optional. Any non-control character.<br />
		  * A  --------- Alphanumeric, optional. 
		  * a  --------- Alphanumeric, optional. 
		  * .  --------- Decimal placeholder. The actual display character used will be the decimal placeholder appropriate to the culture option.<br />
		  * ,  --------- Thousands placeholder. The actual display character used will be the thousands placeholder appropriate to the culture option. 
		  * :  --------- Time separator. The actual display character used will be the time placeholder appropriate to the culture option. 
		  * /  --------- Date separator. The actual display character used will be the date placeholder appropriate to the culture option. 
		  * $  --------- Currency symbol. The actual character displayed will be the currency symbol appropriate to the culture option. 
		  * &lt;  --------- Shift down. Converts all characters that follow to lowercase. 
		  * &gt;  --------- Shift up. Converts all characters that follow to uppercase. 
		  * |  --------- Disable a previous shift up or shift down. 
		  * H  --------- All SBCS characters. 
		  * K  --------- SBCS Katakana. 
		  * ９ --------- DBCS Digit. 
		  * Ｋ --------- DBCS Katakana. 
		  * Ｊ --------- Hiragana. 
		  * Ｚ --------- All DBCS characters.  
		  * N  --------- All SBCS big Katakana.
		  * Ｎ --------- Matches DBCS big Katakana.
		  * Ｇ --------- Matches DBCS big Hiragana.
		  * \\ --------- Escape. Escapes a mask character, turning it into a literal. The escape sequence for a backslash is: \\\\  
		  * All other characters, Literals. All non-mask elements appear as themselves within wijinputmask. 
		  * Literals always occupy a static position in the mask at run time, and cannot be moved or deleted by the user. 
		  * 
		  * 
		  * The following table shows example masks.
		  *     00/00/0000            A date (day, numeric month, year) in international date format. The "/" character is a logical date separator, and will appear to the user as the date separator appropriate to the application's current culture. 
		  *     00-&gt;L&lt;LL-0000         A date (day, month abbreviation, and year) in United States format in which the three-letter month abbreviation is displayed with an initial uppercase letter followed by two lowercase letters. 
		  *     (999)-000-0000        United States phone number, area code optional. If users do not want to enter the optional characters, they can either enter spaces or place the mouse pointer directly at the position in the mask represented by the first 0.  
		  *     $999,999.00           A currency value in the range of 0 to 999999. The currency, thousandth, and decimal characters will be replaced at run time with their culture-specific equivalents.  
		 *  For example: 
		 *  $(".selector").wijinputmask({   
		 *      maskFormat: "(999)-000-0000 " 
		 *  }); 
		 *  
		 *  Value of maskFormat can also be a regex value, attentation, only the following key word supported by the regex value.<br />
		 *  \A   ----------- Matches any upper case alphabet [A-Z]. 
		 *  \a   ----------- Matches any lower case alphabet [a-z]. 
		 *  \D   ----------- Matches any decimal digit. Same as [0-9]. 
		 *  \W   ----------- Matches any word character. It is same as [a-zA-Z_0-9]. 
		 *  \K   ----------- Matches SBCS Katakana. 
		 *  \H   ----------- Matches all SBCS characters. 
		 *  \Ａ  ----------- Matches any upper case DBCS alphabet [Ａ-Ｚ]. 
		 *  \ａ  ----------- Matches any lower case DBCS alphabet [ａ-ｚ]. 
		 *  \Ｄ  ----------- Matches any DBCS decimal digit. Same as [０-９]. 
		 *  \Ｗ  ----------- Matches any DBCS word character. It is same as [ａ-ｚＡ-Ｚ＿０-９]. 
		 *  \Ｋ  ----------- DBCS Katakana 
		 *  \Ｊ  ----------- Hiragana 
		 *  \Ｚ  ----------- All DBCS characters. 
		 *  \N   ----------- Matches all SBCS big Katakana.
		 *  \Ｎ  ----------- Matches DBCS big Katakana.
		 *  \Ｇ  ----------- Matches DBCS big Hiragana.
		 *  \Ｔ  ----------- Matchese surrogate char.
		 *  [^] Used to express an exclude subset. 
		 *  - Used to define a contiguous character range. 
		 *  {} Specifies that the pattern. 
		 *  * The short expression of {0,}. 
		 *  + The short expression of {1,}. 
		 *  ? The short expression of {0,1}. 
		 *  
		 *  
		 *  The following shows some example use Regex value. 
		 *  \D{3}-\D{4}                      Zip Code (012-3456) 
		 *  ℡ \D{2,4}-\D{2,4}-\D{4}/        Phone number (℡ 012-345-6789) 
		 *  \D{2,4}-\D{2,4}-\D{4}            Phone number (012-345-6789) 
		 *  Attentation, because maskFormat requrie that it uses a regex value, so assign the value as follows example. 
		 * </pre>
		 *  @example
		 *  <pre>
		 *  $(".selector").wijinputmask({
		 *      maskFormat: /\D{3}-\D{4}/
		 *  }); <br />
		 *  $(".selector").wijinputmask({
		 *          maskFormat: new RegExp("\\D{3}-\\D{4}")
		 *  });
		 *  </pre>
		 */
		maskFormat = "";

		/** Determines the character that appears when the widget has focus but no input has been entered. 
		  */
		promptChar = '_';

		/** Indicates whether the prompt characters in the input mask are hidden when the input loses focus. 
		  * @remarks
		  * This property doesn't take effect when maskFormat property is set to a Regex value.
		  */
		hidePromptOnLeave = false;

		/** Determines how an input character that matches 
		  * the prompt character should be handled. 
		  * @remarks
		  * This property doesn't take effect when maskFormat property is set to a Regex value.
		  */
		resetOnPrompt = true;

		/** Indicates whether promptChar can be entered as valid data by the user.
		  * @remarks
		  * This property doesn't take effect when maskFormat property is set to a Regex value.
		  */
		allowPromptAsInput = false;

		/** Determines the character to be substituted for the actual input characters. 
		  * @remarks
		  * This property doesn't take effect when maskFormat property is set to a Regex value.
		  */
		passwordChar = '';

		/** Determines how a space input character should be handled. 
		  * @remarks
		  * This property doesn't take effect when maskFormat property is set to a Regex value.
		  */
		resetOnSpace = true;

		/** Indicates whether the user is allowed to re-enter literal values. 
		  * @remarks
		  * This property doesn't take effect when maskFormat property is set to a Regex value.
		  */
		skipLiterals = true;

		/** Determines whether or not the next control in the tab order receives 
			the focus as soon as the control is filled at the last character.
		  */
		blurOnLastChar = false;

		/** Determines whether the focus will move to the next filed or the next control when press the tab key. 
		  * Possible values are "control", "field".
		  * @remarks
		  * The caret moves to the next field when this property is "field".
		  * If the caret is in the last field, the input focus leaves this control when pressing the TAB key. 
		  * If the value is "control", the behavior is similar to the standard control.
		  */
		tabAction = "control";

		/** Determines the input method setting of widget.
		  * Possible values are: 'auto', 'active', 'inactive', 'disabled'
		  * @remarks
		  * This property only take effect on IE and firefox browser.
		  */
		imeMode = "auto";

		/** @ignore */
		allowSpinLoop: boolean = false;

		/** @ignore */
		spinnerAlign: string = "verticalRight";

		/** @ignore */
		showSpinner = false;
	}
	wijinputmask.prototype.options = <any> $.extend(true, {}, wijinputcore.prototype.options, new wijinputmask_options());

	$.wijmo.registerWidget("wijinputmask", wijinputmask.prototype);
}

/** @ignore */
interface JQuery {
	wijinputmask: JQueryWidgetFunction;
}