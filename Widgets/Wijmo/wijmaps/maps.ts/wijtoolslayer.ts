﻿/// <reference path="../../wijslider/jquery.wijmo.wijslider.ts" />
/// <reference path="CoordConverter.ts" />

module wijmo.maps {
	  
	var $ = jQuery,
		defSpeed = 0.5,
		defMinPixels = 20,
		defMaxPixels = 158,
		css = {
			layerContainerCss: "wijmo-wijmaps-layercontainer",
			toolsLayerCss: "wijmo-wijmaps-toolslayer",
			panCss: "wijmo-wijmaps-pan",
			panpointCss: "wijmo-wijmaps-panpoint",
			zoomCss: "wijmo-wijmaps-zoomer",
			scaleCss: "wijmo-wijmaps-scaler",
			scaleLableCss: "wijmo-wijmaps-content",
			scaleValueCss: "wijmo-wijmaps-scalervalue"
		};

	/**
	* @widget
	* @ignore
	* The tools layer for wijmaps.
	*/
	export class wijtoolslayer extends wijmoWidget {
		/**
		* The options of the layer.
		*/
		public options: wijtoolslayer_options;

		_on: any;

		private _panPanel: JQuery;
		private _panPoint: JQuery;

		private _zoomPanel: JQuery;
		private _zoomSlider: JQuery;

		private _scalePanel: JQuery;
		private _metersLabel: JQuery;
		private _milesLabel: JQuery;
		private _metersValue: JQuery;
		private _milesValue: JQuery;

		private _isPanning: boolean;
		private _panningTimer: number;
		private _speed: number;

		/** @ignore */
		_create() {
			var self = this;
			self.element.addClass(css.toolsLayerCss);

			self._on(self.element, {
				"dblclick": function (event, data) {
					event.stopPropagation();
				}
			});

			self._speed = defSpeed;
			self._isPanning = false;
			self._createPanPanel();
			self._createZoomPanel();
			self._createScalePanel();
		}

		_innerDisable() {
			super._innerDisable();
			this._handleDisabledOption(true);
		}

		_innerEnable() {
			super._innerEnable();
			this._handleDisabledOption(false);
		}

		_handleDisabledOption(disabled: boolean) {
			var self = this,
				options = { disabled: disabled };

			if (self._zoomSlider) {
				self._zoomSlider.wijslider(options);
			}
		}

		/** @ignore */
		_setOption(key: string, value: any) {
			var self = this, o = this.options;
			if (o[key] !== value) {
				switch (key) {
					case 'center':
						self._setCenter(value);
						return;
					case 'zoom':
						self._setZoom(value);
						return;
				}
			}
			super._setOption(key, value);
		}

		private _setCenter(center: Point) {
			var self = this, o = self.options;
			o.center = center;
			self._updateScalePanel();
		}

		private _setZoom(zoom: number) {
			var self = this, o = self.options;
			o.zoom = zoom;
			self._zoomSlider.wijslider({ orientation: "vertical", range: false, min: o.minZoom, max: o.maxZoom, step: 1, value: o.zoom, values: null });
			self._updateScalePanel();
		}

		private _createPanPanel() {
			var self = this, o = self.options, e = self.element,
				offsetX: number, offsetY: number,
				leftArrow: JQuery, rightArrow: JQuery, upArrow: JQuery, downArrow: JQuery, hOffset: number, vOffset: number;

			self._panPanel = $("<div class=\"" + css.panCss + " " + o.wijCSS.stateDefault + "\"></div>").appendTo(e);
			self._on(self._panPanel, {
				"mousedown": (event, data) => {
					self._isPanning = true;
					self._startPanning(self._panPanel, self._getPanPointOffset(self._panPanel, new Point(event.pageX, event.pageY)));
					$(document).disableSelection();
					event.stopPropagation();
				},
				"dragstart": function (event, data) {
					//prevent image being dragged in firefox.
					event.preventDefault();
				},
				"selectstart": function (event, data) {
					//prevent image is selected in ie.
					event.preventDefault();
				}
			});

			self._on($(document), {
				"mouseup": (event, data) => {
					self._isPanning = false;
					self._stopPanning();
					$(document).enableSelection();
				},
				"mousemove": (event, data) => {
					if (self._isPanning) {
						self._startPanning(self._panPanel, self._getPanPointOffset(self._panPanel, new Point(event.pageX, event.pageY)));
					}
				}
			});

			self._panPoint = $("<div class=\"" + css.panpointCss + " " + o.wijCSS.content + " " + o.wijCSS.stateActive + "\"></div>").hide().appendTo(e);

			leftArrow = $("<div class=\"" + o.wijCSS.icon + " " + o.wijCSS.iconArrowLeft + "\"></div>");
			rightArrow = $("<div class=\"" + o.wijCSS.icon + " " + o.wijCSS.iconArrowRight + "\"></div>");
			upArrow = $("<div class=\"" + o.wijCSS.icon + " " + o.wijCSS.iconArrowUp + "\"></div>");
			downArrow = $("<div class=\"" + o.wijCSS.icon + " " + o.wijCSS.iconArrowDown + "\"></div>");

			self._panPanel.append(leftArrow, rightArrow, upArrow, downArrow);
		}

		private _getPanPointOffset(panPanel: JQuery, panPoint: Point): Point {
			var panPanelOffset = panPanel.offset();
			return panPoint.offset(-panPanelOffset.left, -panPanelOffset.top);
		}

		private _startPanning(target: JQuery, position: Point) {
			var self = this, o = self.options,
				viewCenter = o.converter.getViewCenter(),
				panPanelCenter = new Point(target.outerWidth() / 2, target.outerHeight() / 2),
				panOffset = position.offset(-panPanelCenter.x, -panPanelCenter.y),
				length = Math.sqrt(Math.pow(panOffset.x, 2) + Math.pow(panOffset.y, 2)),
				panPointPosition = new Point(position.x, position.y), newCenter: IPoint;

			if (self._panningTimer) {
				clearInterval(self._panningTimer);
			}
			self._panningTimer = setInterval(() => {
				newCenter = o.converter.viewToGeographic(
					new Point(panOffset.scale(self._speed, self._speed)).offset(viewCenter.x, viewCenter.y));
				self._trigger("targetCenterChanged", null, { "targetCenter": newCenter });
			}, 20);

			if (length > panPanelCenter.x) {
				panPointPosition = panPanelCenter.scale(panOffset.x / length, panOffset.y / length).offset(panPanelCenter.x, panPanelCenter.y);
			}
			panPointPosition = panPointPosition.offset(target.position().left, target.position().top);
			self._panPoint.css("left", panPointPosition.x - self._panPoint.width() / 2).css("top", panPointPosition.y - self._panPoint.height() / 2).show();
		}

		private _stopPanning() {
			var self = this;
			if (self._panningTimer) {
				clearInterval(self._panningTimer);
			}
			self._panPoint.hide();
		}

		private _createZoomPanel() {
			var self = this, o = self.options, e = self.element;
			self._zoomSlider = $("<div></div>").css("height", "100%").css("font-size", "13px");	// need refactor. set it to 13px to make the sliding block align center.
			self._zoomPanel = $("<div class=\"" + css.zoomCss + "\"></div>").append(self._zoomSlider);
			e.append(self._zoomPanel);
			self._zoomSlider.wijslider({ orientation: "vertical", range: false, min: o.minZoom, max: o.maxZoom, step: 1, value: o.zoom, values: null });

			self._on(self._zoomPanel, {
				"mousedown": (event, data) => {
					event.stopPropagation();
				}
			});
			self._on(true, self._zoomPanel, {
				"wijsliderchange": (event, data) => {
					self._trigger("targetZoomChanged", null, { "targetZoom": data.value });
				}
			});
		}

		private _createScalePanel() {
			var self = this, o = self.options, e = self.element;
			self._scalePanel = $("<div class=\"" + css.scaleCss + " " + o.wijCSS.content + "\"></div>").appendTo(e);
			self._on(self._scalePanel, {
				"mousedown": (event, data) => {
					event.stopPropagation();
				},
				"dblclick": function (event, data) {
					event.stopPropagation();
				}
			});

			self._metersLabel = $("<span class=\"" + css.scaleLableCss + "\"></span>");
			self._milesLabel = $("<span class=\"" + css.scaleLableCss + "\"></span>");
			self._metersValue = $("<span class=\"" + css.scaleValueCss + " " + o.wijCSS.stateDefault + "\"></span>");
			self._milesValue = $("<span class=\"" + css.scaleValueCss + " " + o.wijCSS.stateDefault + "\"></span>");

			self._scalePanel.append(self._metersLabel, self._metersValue, $("<br />"), self._milesLabel, self._milesValue);
			self._updateScalePanel();
		}

		private _updateScalePanel() {
			var self = this;
			self._updateScale(self._metersValue, self._metersLabel, 1, 1000, "km", "m");
			self._updateScale(self._milesValue, self._milesLabel, 3.2808399, 5280, "mi", "ft");
		}

		private _updateScale(scale: JQuery, label: JQuery, meterToUnit: number, largeToSmall: number, largeUnit: string, unit: string) {
			if (scale) {
				var self = this, o = self.options, minPixels = defMinPixels, maxPixels = defMaxPixels,
					minDistance = self._getDistance(minPixels) * meterToUnit,
					maxDistance = self._getDistance(maxPixels) * meterToUnit;

				var roundest = self._roundest(Math.floor(minDistance), Math.floor(maxDistance));
				if (roundest.toString().length <= Math.ceil(Math.LOG10E*Math.log(largeToSmall))) {
					if (label)
						label.text(roundest + " " + unit);
				}
				else {
					minDistance /= largeToSmall;
					maxDistance /= largeToSmall;
					roundest = self._roundest(Math.floor(minDistance), Math.floor(maxDistance));
					if (label)
						label.text(roundest + " " + largeUnit);
				}
			}

			var alpha = (roundest - minDistance) * 1.0 / (maxDistance - minDistance);
			scale.width(Math.max(minPixels * (1 - alpha) + maxPixels * alpha, 0));
		}

		// returns the distance in meters from the center of the map to a point 'pixels' pixels to the right
		private _getDistance(pixels): number {
			var self = this, o = self.options;
			return o.converter.distance(o.center,
				o.converter.viewToGeographic(
					new Point(o.converter.getViewCenter()).offset(o.center.x < 0 ? pixels : -pixels, 0)));
		}

		// returns the largest number with more trailing zeros between min and max
		private _roundest(min: number, max: number): number {
			var self = this, maxs = max.toString(), mins = min.toString();

			for (var i = 0; i < maxs.length; ++i) {
				if (maxs.length > mins.length || maxs[i] != mins[i]) {
					return parseInt(self._padRight(maxs.substring(0, i + 1), maxs.length, '0'));
				}
			}
			return max;
		}

		private _padRight(str: string, totalWidth: number, paddingChar: string) {
			if (str.length < totalWidth) {
				var i, paddingString = new String();
				for (i = 1; i <= (totalWidth - str.length); i++) {
					paddingString += paddingChar;
				}
				return (str + paddingString);
			} else {
				return str;
			}
		}

		/**
		* Removes the wijtoolslayer functionality completely. This will return the element back to its pre-init state.
		*/
		public destroy() {
			var self = this;
			$(document).off("." + self.widgetName);
			self._panPanel.remove();
			self._zoomSlider.wijslider("destroy").remove();
			self._panPanel.off("." + self.widgetName).remove();
			self._zoomPanel.off("." + self.widgetName).remove();
			self._scalePanel.off("." + self.widgetName).remove();
			self.element.removeClass(css.toolsLayerCss).off("." + self.widgetName);
			super.destroy();
		}
	}

	/**
	* @ignore
	* The options for wijtoolslayer.
	*/
	export class wijtoolslayer_options {
		/**
		* The current center of the map, in geographic unit.
		*/
		center: IPoint;

		/**
		* The current zoom level of the map.
		*/
		zoom: number;

		/**
		* The minmum zoom level supported by the map.
		*/
		minZoom: number;

		/**
		* The maximum zoom level supported by the map.
		*/
		maxZoom: number;

		/**
		* The converter used to convert the point coordinates.
		*/
		converter: ICoordConverter;

		/** 
		* @ignore 
		* The following styles are used:
		* <ul>
		* <li>stateDefault</li>
		* <li>stateActive</li>
		* <li>icon</li>
		* <li>iconArrowLeft</li>
		* <li>iconArrowRight</li>
		* <li>iconArrowUp</li>
		* <li>iconArrowDown</li>
		* <li>content</li>
		* </ul>
		*/
		wijCSS: WijmoCSS;
	}

	wijtoolslayer.prototype.options = $.extend(true, {}, wijmo.wijmoWidget.prototype.options, new wijtoolslayer_options());

	$.wijmo.registerWidget("wijtoolslayer", wijtoolslayer.prototype);
}