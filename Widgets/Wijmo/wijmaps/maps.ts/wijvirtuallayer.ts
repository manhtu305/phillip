﻿/// <reference path="Drawing.ts"/>
/// <reference path="Utils.ts"/>
/// <reference path="wijlayer.ts"/>
/// <reference path="wijitemslayer.ts" />
/// <reference path="../../Base/jquery.wijmo.widget.ts" />
/// <reference path="../../wijutil/jquery.wijmo.raphael.ts"/> 

module wijmo.maps {
	  

	var $ = jQuery,
		layerContainerCss: string = "wijmo-wijmaps-layercontainer";

	/**
	* @widget
	* The virtual layer for the wijmaps. 
	* Display its items positioned geographically and only shows the items actually in view.
	*/
	export class wijvirtuallayer extends wijlayer {
		private _slices: MapSlice[];
		private _visibleRegions: Region[];
		private _container: JQuery;
		private _paper: RaphaelPaper;
		private _maxZoom = 20;

		/**
		* The options of the wijvirtuallayer.
		*/
		public options: wijvirtuallayer_options;

		/** @ignore */
		_create() {
			var self = this;

			self._container = $("<div class=\"" + layerContainerCss + "\"></div>");
			self.element.append(self._container);
			self._paper = self._createPaper(self._container[0], self.options.converter.getViewSize());
			super._create();

			self._setRequest(self.options.request);
		}

		/** @ignore */
		_setOption(key: string, value: any) {
			var self = this, o = this.options;
			if (o[key] !== value) {
				switch (key) {
					case 'request':
						self._setRequest(value);
						return;
					case 'slices':
						self._setSlices(value);
						return;
				}
			}
			super._setOption(key, value);
		}

		private _setRequest(request: any) {
			var self = this;
			self.options.request = request;
			self._refreshLayer();
		}

		private _setSlices(slices: any) {
			var self = this;
			self.options.slices = slices;
			self._refreshLayer();
		}

		private _refreshLayer() {
			var self = this, o = self.options;
			self._slices = self._sortSlices(o.slices);
			self._clear();
			self.update();
		}

		/** 
		* @ignore 
		* override 
		*/
		_render() {
			var self = this, o = self.options,
				slice = self._getMatchedSlice();

			if (slice === null) {
				self._clear();
				return;
			}

			self._resizePaper(self._paper, o.converter.getViewSize());

			var index = $.inArray(slice, self._slices),
				maxZoom = index > 0 ? self._slices[index - 1].zoom : self._maxZoom,
				logicBounds = self._getLogicBounds(),
				topLeft = logicBounds.location,
				bottomRight = new Point(logicBounds.getRight(), logicBounds.getBottom()),
				currentVisibleRegions : Region[] = [];

			for (var i = Math.floor(topLeft.x * slice.longitudeSlices); i < bottomRight.x * slice.longitudeSlices; i++) {
				for (var j = Math.floor(topLeft.y * slice.latitudeSlices); j < bottomRight.y * slice.latitudeSlices; j++) {
					currentVisibleRegions.push(new Region(slice, i, j));
				}
			}

			var invisibleRegions = $.grep(self._visibleRegions, (visibleRegion) => {
				for (var i = 0; i < currentVisibleRegions.length; i++) {
					if (visibleRegion.equals(currentVisibleRegions[i])) {
						return false;
					}
				}
				return true;
			});
			$.each(invisibleRegions, (regionIndex, region) => {
				self._removeRegion(region);
			});

			var newRegions = $.grep(currentVisibleRegions, (currentVisibleRegion)=> {
				for (var i = 0; i < self._visibleRegions.length; i++) {
					if (currentVisibleRegion.equals(self._visibleRegions[i])) {
						return false;
					}
				}
				return true;
			});

			var remainVisibleRegions = $.grep(self._visibleRegions, (region) => {
				return $.inArray(region, invisibleRegions) === -1;
			});

			self._visibleRegions = remainVisibleRegions.concat(newRegions);

			$.each(newRegions, (regionIndex, region)=> {
				var regionLowerLeft = o.converter.logicToGeographic(new Point(region.longitude / slice.longitudeSlices, (region.latitude + 1) / slice.latitudeSlices)),
					regionUpperRight = o.converter.logicToGeographic(new Point((region.longitude + 1) / slice.longitudeSlices, region.latitude / slice.latitudeSlices)),
					result = o.request(self._paper, self.options.zoom, maxZoom, regionLowerLeft, regionUpperRight);
				if (result) {
					region.items = result.items;
					if (result.items) {
						wijitemslayer._setItemsData(result.items);
					}
				}
			});

			self._relayout();
		}

		private _relayout() {
			var self = this, o = self.options, converter = o.converter;
			wijitemslayer._relayoutRegions(converter, self._visibleRegions);
		}

		private _removeRegion(region: Region) {
			if (region.items) {
				$.each(region.items, (index, item)=> {
					item.elements.remove();
				});
			}
		}

		private _clear() {
			var self = this;
			self._visibleRegions = [];
			if (self._paper) {
				self._paper.clear();
			}
		}

		private _getMatchedSlice(): MapSlice {
			var self = this;
			if (!self._slices) return null;

			var len = self._slices.length,
				i, slice;
			for(i = 0; i < len;i++)
			{
				slice = self._slices[i];
				if (slice.zoom <= self.options.zoom) {
					return slice;
				}
			}

			return null;
		}

		private _sortSlices(slices: MapSlice[]): MapSlice[]{
			if (!slices) return [];

			return slices.sort((a, b) => {
				return -(a.zoom - b.zoom);
			});
		}

		private _getLogicBounds(): Rectangle {
			var self = this, converter = self.options.converter,
				location = converter.viewToLogic(new Point(0, 0)),
				size = converter.getLogicSize();
			return new Rectangle(location, size);
		}

		/**
		* Force to request the data to get the newest one.
		*/
		public refresh() {
			this._refreshLayer();
			super.refresh();
		}

		/**
		* Removes the wijvirtuallayer functionality completely. This will return the element back to its pre-init state.
		*/
		public destroy() {
			var self = this;
			if (self._paper) self._paper.remove();
			self._container.remove();

			super.destroy();
		}
	}

	/**
	* Defines the optoins of the wijmaps virtual layer.
	*/
	export interface IVirualLayerOptions extends ILayerOptions {
		/**
		* Specifies how the map is partitioned for virtualization.
		* @remarks Each slice defines a set of regions this virtual layer will get items form when needed.
		* The minimum zoom of a slice is the value value of it's <b>zoom</b> property, 
		* while the maximum zoom is the value from the <b>zoom</b> property of the next slice.
		* Each slice divides the map in a uniform grid of regions according to the current projection.
		*/
		slices: IMapSlice[];

		/**
		* The function to be called when request the data.
		* @event
		* @param {RaphaelPaper} paper The Raphael paper to draw the items.
		* @param {number} minZoom The min zoom suits the current request slice.
		* @param {number} maxZoom The max zoom suits the current request slice.
		* @param {IPoint} lowerLeft The coordinates of the lower left point of the current request slice, in geographic unit.
		* @param {IPoint} upperRight The coordinates of the upper right point of the current request slice, in geographic unit.
		* @returns {IVirualLayerRequestResult} The object which contains items to be drawn.
		* @remarks 
		* <p>The virtual layer request the items to be drawn when the slice need be redrawn.
		* If want to refresh the items, call refresh() method if necessary.</p>
		* <p>The items will be arranged to the position based on it's location.</p>
		*/
		request: (paper: RaphaelPaper, minZoom: number, maxZoom: number, lowerLeft: IPoint, upperRight: IPoint) => IVirtualLayerRequestResult;
	}
	
	/**
	* Defines the object of the request result for the virtual layer.
	*/
	export interface IVirtualLayerRequestResult {
		/**
		* The array of individual item to be drawn.
		*/
		items: IVirtualLayerItem[];
	}

	/**
	* Defines the object of the requested individual item for the virtual layer.
	*/
	export interface IVirtualLayerItem  extends IItemsLayerItem {
	}

	/**
	* Represents the options for the wijvirtuallayer.
	*/
	export class wijvirtuallayer_options extends wijlayer_options implements IVirualLayerOptions {
		/**
		* Specifies how the map is partitioned for virtualization.
		* @remarks Each slice defines a set of regions this virtual layer will get items form when needed.
		* The minimum zoom of a slice is the value value of it's <b>zoom</b> property, 
		* while the maximum zoom is the value from the <b>zoom</b> property of the next slice.
		* Each slice divides the map in a uniform grid of regions according to the current projection.
		*/
		slices: IMapSlice[] = [];

		/**
		* The function to be called when request the data.
		* @event
		* @param {RaphaelPaper} paper The Raphael paper to draw the items.
		* @param {number} minZoom The min zoom suits the current request slice.
		* @param {number} maxZoom The max zoom suits the current request slice.
		* @param {IPoint} lowerLeft The coordinates of the lower left point of the current request slice, in geographic unit.
		* @param {IPoint} upperRight The coordinates of the upper right point of the current request slice, in geographic unit.
		* @returns {IVirualLayerRequestResult} The object which contains items to be drawn.
		* @remarks 
		* <p>The virtual layer request the items to be drawn when the slice need be redrawn.
		* If want to refresh the items, call refresh() method if necessary.</p>
		* <p>The items will be arranged to the position based on it's location.</p>
		*/
		request: (paper: RaphaelPaper, minZoom: number, maxZoom: number, lowerLeft: IPoint, upperRight: IPoint) => IVirtualLayerRequestResult;
	}

	wijvirtuallayer.prototype.options = $.extend(true, {}, wijmo.wijmoWidget.prototype.options, new wijvirtuallayer_options());


	$.wijmo.registerWidget("wijvirtuallayer", wijvirtuallayer.prototype);
}