﻿/// <reference path="Drawing.ts" />
module wijmo.maps {


	/** @ignore */
	export interface IMapProjection {
		project(longLat: IPoint): IPoint;
		unproject(point: IPoint): IPoint;
	}

	/** @ignore */
	export class MercatorProjection implements IMapProjection {
		public project(lonLat: IPoint): IPoint {
			return new Point((lonLat.x + 180) / 360, (1 - Math.log(Math.tan(lonLat.y * Math.PI / 180) + 1 / Math.cos(lonLat.y * Math.PI / 180)) / Math.PI) / 2);
		}

		public unproject(p: IPoint): IPoint {
			var n = Math.PI - 2 * Math.PI * p.y;
			return new Point((p.x * 360 - 180), 180 / Math.PI * Math.atan(0.5 * (Math.exp(n) - Math.exp(-n))));
		}
	}
} 