﻿/// <reference path="MapSources.ts" />
/// <reference path="wijlayer.ts" />
/// <reference path="CoordConverter.ts" />
/// <reference path="../../Base/jquery.wijmo.widget.ts" />

interface JQuery {
	wijmultiscaleimage: JQueryWidgetFunction;
}

interface JQuery {
	wijtoolslayer: JQueryWidgetFunction;
}

module wijmo.maps {
	  
	var $ = jQuery,
		defMinZoom = 1,
		defMaxZoom = 20,
		defTileWidth = 256,
		defTileHeight = 256,
		wijmapsCss: string = "wijmo-wijmaps",
		containerCss: string = "wijmo-wijmaps-container",
		tilesContainerCss: string = "wijmo-wijmaps-tileslayer",
		layerContainerCss: string = "wijmo-wijmaps-layercontainer";

	/** @widget */
	export class wijmaps extends wijmoWidget {
		/**
		* The options for the wijmaps.
		*/
		public options: wijmaps_options;

		_on: any;

		private _isZooming: boolean;
		private _layers: ILayerItem[];
		private _container: JQuery;
		private _layersContainer: JQuery;
		private _multiScaleImageContainer: JQuery;
		private _toolsLayerContainer: JQuery;
		private _disabledCover: JQuery;
		private _isPanning: boolean;
		private _isCenterChanging: boolean;
		private _panningStartPosition: Point;
		private _targetZoomTimer: number;
		private _targetCenterTimer: number;
		private _coordConverter: CoordConverter;
		private _minZoom: number;
		private _maxZoom: number;
		private _innerSource: IMultiScaleTileSource;
		private _keyCode : JQueryUI.KeyCode;
		private _authenticationResult: string;
		private _sessionId: string;

		/** @ignore */
		_create() {
			var self = this, o = self.options;
			self._setInnerSource(o.source, () => { self._createMaps(); });
			super._create();
		}

		private _createMaps() {
			var self = this, e = self.element, o = self.options, offsetX: number, offsetY: number, targetZoom: number, targetCenter: IPoint, leftTopGeographicCoord: Point, rightBottomGeographicCoord: Point;
			e.addClass(wijmapsCss);
			e.addClass((<any>o).wijCSS.content);
			e.attr("tabindex", -1);

			self._isPanning = false;
			self._isZooming = false;
			self._isCenterChanging = false;
			self._keyCode = wijmo.getKeyCodeEnum();

			if (self._innerSource) {

				self._minZoom = self._innerSource.minZoom;
				self._maxZoom = self._innerSource.maxZoom;
				self._coordConverter = new CoordConverter(e, new Point(o.center.x, o.center.y), o.zoom, self._innerSource.tileWidth, self._innerSource.tileHeight);
			}
			else {
				self._minZoom = defMinZoom;
				self._maxZoom = defMaxZoom;
				self._coordConverter = new CoordConverter(e, new Point(o.center.x, o.center.y), o.zoom, defTileWidth, defTileHeight);
			}

			if (!o.targetCenter) {
				o.targetCenter = $.extend({}, o.center);
			}

			if (o.targetZoom === undefined) {
				o.targetZoom = o.zoom;
			}

			self._createLayers();

			if (o.targetZoom !== o.zoom) {
				targetZoom = o.targetZoom;
				o.targetZoom = o.zoom;
				self._setTargetZoom(targetZoom);
			}

			if (o.targetCenter.x !== o.center.x || o.targetCenter.y !== o.center.y) {
				targetCenter = o.targetCenter;
				o.targetCenter = o.center;
				self._setTargetCenter(targetCenter);
			}

			self._on(e, {
				"dragstart": function (event, data) {
					//prevent image being dragged in firefox.
					event.preventDefault();
				},
				"selectstart": function (event, data) {
					//prevent image is selected in ie.
					event.preventDefault();
				},
				"mousewheel": function (event, data) {
					var position = new Point(event.pageX - e.offset().left, event.pageY - e.offset().top);
					if (data > 0) {
						self._zoomIn(position);
					}
					else {
						self._zoomOut(position);
					}
					event.preventDefault();
				},
				"dblclick": function (event, data) {
					var position = new Point(event.pageX - e.offset().left, event.pageY - e.offset().top);
					self._zoomIn(position);
				},
				"mousedown": function (event, data) {
					e.focus();
					self._isPanning = true;
					self._panningStartPosition = new Point(event.pageX, event.pageY);
					$(document).disableSelection();
				},
				"keydown": function (event, data) {
					if (self._onKeyDown(event)) {
						event.preventDefault();
					}
				},
				"keypress": function (event, data) {
					if (self._onKeyPress(event)) {
						event.preventDefault();
					}
				}
			});

			self._on($(document), {
				"mouseup": function (event, data) {
					self._isPanning = false;
					$(document).enableSelection();
				},
				"mousemove": function (event, data) {
					if (self._isPanning) {
						offsetX = self._panningStartPosition.x - event.pageX;
						offsetY = self._panningStartPosition.y - event.pageY;
						self._panningStartPosition = new Point(event.pageX, event.pageY);
						self._panning(offsetX, offsetY);
					}
				}
			});
		}

		_innerDisable() {
			super._innerDisable();
			this._handleDisabledOption(true);
		}

		_innerEnable() {
			super._innerEnable();
			this._handleDisabledOption(false);
		}

		_handleDisabledOption(disabled: boolean) {
			var self = this, o = self.options,
				layers = self._layers,
				options = { disabled: disabled };

			if (layers) {
				$.each(layers, function (key, item: ILayerItem) {
					item.element[item.wijname](options);
				});
			}

			if (self._innerSource) {
				self._multiScaleImageContainer.wijmultiscaleimage(options);
			}

			if (o.showTools) {
				self._toolsLayerContainer.wijtoolslayer(options);
			}

			if (disabled) {
				self._disabledCover.show();
			}
			else {
				self._disabledCover.hide();
			}
		}

		/** @ignore */
		_setOption(key: string, value: any) {
			var self = this, o = this.options;
			if (o[key] !== value) {
				switch (key) {
					case 'center':
						self._setCenter(value);
						return;
					case 'targetCenter':
						self._setTargetCenter(value);
						return;
					case 'zoom':
						self._setZoom(value);
						return;
					case 'targetZoom':
						self._setTargetZoom(value);
						return;
					case 'layers':
						self._setLayers(value);
						return;
					case 'showTools':
						self._setShowTools(value);
						return;
					case 'source':
						self._setSource(value);
						return;
				}
			}
			super._setOption(key, value);
		}

		private _setInnerSource(source: any, callback: () => void) {
			var self = this, o = self.options, isBingMapsSource: boolean;

			switch (source) {
				case "bingMapsRoadSource":
					self._innerSource = MapSource.bingMapsRoadSource;
					isBingMapsSource = true;
					break;
				case "bingMapsAerialSource":
					self._innerSource = MapSource.bingMapsAerialSource;
					isBingMapsSource = true;
					break;
				case "bingMapsHybridSource":
					self._innerSource = MapSource.bingMapsHybridSource;
					isBingMapsSource = true;
					break;
				default:
					self._innerSource = source;
					isBingMapsSource = false;
					break;
			}

			if (isBingMapsSource && o.bingMapsKey && o.bingMapsKey !== "") {
				$.ajax({
					url: "http://dev.virtualearth.net/webservices/v1/LoggingService/LoggingService.svc/Log?entry=0&auth=" + o.bingMapsKey + "&fmt=1&type=3&group=wijmo&name=wijmaps&mkt=en-us&jsonp=?",
					data: null,
					context: null,
					success: function (result) {
						self._authenticationResult = result ? result["authenticationResultCode"] : "";
						self._sessionId = result ? result["sessionId"] : "";
						callback();
					},
					error: function (result) {
						// authentication failed.
						callback();
					},
					dataType: "json",
					timeout: 5000
				});
			}
			else {
				callback();
			}
		}

		private _setShowTools(showTools: boolean) {
			var self = this, o = self.options;

			o.showTools = showTools;
			if (showTools) {
				self._createToolsLayer();
			}
			else {
				self._destroyToolsLayer();
			}
		}

		private _setSource(source: any) {
			var self = this, o = self.options;
			o.source = source;
			self._setInnerSource(source, () => { self._refreshTileLayer(); });
		}

		private _refreshTileLayer() {
			var self = this;
			self._destroyTileLayer();
			if (self._innerSource) {
				self._minZoom = self._innerSource.minZoom;
				self._maxZoom = self._innerSource.maxZoom;
				self._coordConverter.tileWidth = self._innerSource.tileWidth;
				self._coordConverter.tileHeight = self._innerSource.tileHeight;
				self._createMultiScaleImageLayer();
			}
			else {
				self._minZoom = defMinZoom;
				self._maxZoom = defMaxZoom;
				self._coordConverter.tileWidth = defTileWidth;
				self._coordConverter.tileHeight = defTileHeight;
			}
		}

		private _setLayers(layers: ILayerOptions[]) {
			var self = this;
			self.options.layers = layers;
			self._destroyInnerLayers();
			self._createInnerLayers();
		}

		private _setCenter(center: IPoint) {
			var self = this;
			self.options.targetCenter = center;
			self._trigger("targetCenterChanged", null, { "targetCenter": center });
			self._setCenterInternal(new Point(center.x, center.y));
		}

		private _setCenterInternal(center: Point) {
			var self = this, o = self.options;
			o.center = { "x": center.x, "y": center.y };
			self._coordConverter.center = center;
			self._updateLayers();
			self._trigger("centerChanged", null, { "center": o.center });
		}

		private _setZoom(zoom: number, position?: Point) {
			var self = this, o = self.options;
			if (zoom !== o.zoom && zoom >= self._minZoom && zoom <= self._maxZoom) {
				self.options.targetZoom = zoom;
				self._trigger("targetZoomChanged", null, { "targetZoom": zoom });
				self._setZoomInternal(zoom, position);
			}
		}

		private _setZoomInternal(zoom: number, position?: IPoint) {
			var self = this, o = self.options, zoomPosition: IPoint, viewCenter: IPoint, offset: Size;
				viewCenter = self._coordConverter.getViewCenter();
				zoomPosition = position ? position : viewCenter;
				offset = new Size(zoomPosition.x - viewCenter.x, zoomPosition.y - viewCenter.y).scale(1 - Math.pow(2, o.zoom - zoom), 1 - Math.pow(2, o.zoom - zoom));
				self._offsetCenter(offset.width, offset.height);

				o.zoom = zoom;
				self._coordConverter.zoom = zoom;
				self._updateLayers();
				self._trigger("zoomChanged", null, { "zoom": o.zoom });
			}

		private _zoomIn(zoomPosition: IPoint) {
			var self = this, o = self.options,
				newZoom = Math.min(Math.round(o.targetZoom + 1), self._maxZoom);

			if (o.zoom !== newZoom) {
				self._setTargetZoom(newZoom, zoomPosition);
			}
		}

		private _zoomOut(zoomPosition: IPoint) {
			var self = this, o = self.options,
				newZoom = Math.max(Math.round(o.targetZoom - 1), self._minZoom);

			if (o.zoom !== newZoom) {
				self._setTargetZoom(newZoom, zoomPosition);
			}
		}

		private _setTargetZoom(targetZoom: number, position?: IPoint) {
			var self = this, o = self.options, intermediateZoom: number;

			if (targetZoom > self._maxZoom) targetZoom = self._maxZoom;
			if (targetZoom < self._minZoom) targetZoom = self._minZoom;

			if (targetZoom !== o.targetZoom) {
				o.targetZoom = targetZoom;
				self._trigger("targetZoomChanged", null, { "targetZoom": targetZoom });

				if (self._isZooming) return;

				self._isZooming = true;
				clearInterval(self._targetZoomTimer);
				self._targetZoomTimer = setInterval(() => {
					if (self._isZooming) {
						if (Math.abs(o.zoom - o.targetZoom) < 0.001) {
							intermediateZoom = o.targetZoom;
							self._isZooming = false;
							clearInterval(self._targetZoomTimer);
						}
						else {
							intermediateZoom = o.targetZoom * o.targetZoomSpeed + o.zoom * (1 - o.targetZoomSpeed);
						}

						self._setZoomInternal(intermediateZoom, position);
					}
				}, 20);
			}
		}

		private _setTargetCenter(targetCenter: IPoint) {
			var self = this, o = self.options, intermediateCenter: Point, converter = self._coordConverter;

			if (targetCenter.x < converter.minLongitude) targetCenter.x = converter.minLongitude;
			if (targetCenter.x > converter.maxLongitude) targetCenter.x = converter.maxLongitude;
			if (targetCenter.y < converter.minLatitude) targetCenter.y = converter.minLatitude;
			if (targetCenter.y > converter.maxLatitude) targetCenter.y = converter.maxLatitude;

			if (o.targetCenter.x !== targetCenter.x || o.targetCenter.y !== targetCenter.y) {
				o.targetCenter = targetCenter;
				self._trigger("targetCenterChanged", null, { "targetCenter": targetCenter });

				if (self._isCenterChanging) return;

				self._isCenterChanging = true;
				clearInterval(self._targetCenterTimer);
				self._targetCenterTimer = setInterval(() => {
					if (self._isCenterChanging) {
						if (Math.abs(o.targetCenter.x - o.center.x) < 0.001 && Math.abs(o.targetCenter.y - o.center.y) < 0.001) {
							intermediateCenter = new Point(o.targetCenter.x, o.targetCenter.y);
							self._isCenterChanging = false;
							clearInterval(self._targetCenterTimer);
						}
						else {
							intermediateCenter = new Point(
								o.targetCenter.x * o.targetCenterSpeed + o.center.x * (1 - o.targetCenterSpeed),
								o.targetCenter.y * o.targetCenterSpeed + o.center.y * (1 - o.targetCenterSpeed));
						}

						self._setCenterInternal(intermediateCenter);
					}
				}, 20);
			}
		}

		private _panning(offsetX: number, offsetY: number) {
			var self = this, o = self.options;
			if (offsetX !== 0 || offsetY !== 0) {
				self._offsetCenter(offsetX, offsetY);
			}
		}

		private _offsetCenter(offsetX: number, offsetY: number) {
			if (offsetX === 0 && offsetY == 0) {
				return;
			}

			var self = this, c = self._coordConverter,
				fullMapSize = c.getFullMapSize(),
				logicCenter = new Point(c.getLogicCenter()).offset(offsetX / fullMapSize.width, offsetY / fullMapSize.height),
				newCenter = c.logicToGeographic(logicCenter);

			self._setCenter({ "x": newCenter.x, "y": newCenter.y });
		}

		private _onKeyDown(event: JQueryEventObject): boolean {
			var self = this,o=self.options,c=self._coordConverter,
				panSpeed = 5, center:IPoint,
				keyCode = self._keyCode,
				viewSize = c.getViewSize(),
				offsetX = 0, offsetY = 0;
			switch (event.which) {
				case keyCode.UP:
					offsetY = -panSpeed;
					break;
				case keyCode.DOWN:
					offsetY = panSpeed;
					break;
				case keyCode.LEFT:
					offsetX = -panSpeed;
					break;
				case keyCode.RIGHT:
					offsetX = panSpeed;
					break;
				case keyCode.HOME:
					offsetX = -viewSize.width;
					break;
				case keyCode.END:
					offsetX = viewSize.width;
					break;
				case keyCode.PAGE_UP:
					offsetY = -viewSize.height;
					break;
				case keyCode.PAGE_DOWN:
					offsetY = viewSize.height;
					break;
				default:
					return false;
			}

			if (offsetX !== 0 || offsetY !== 0) {
				center = c.geographicToView(o.targetCenter);
				self._setTargetCenter(c.viewToGeographic({ "x": center.x + offsetX, "y": center.y + offsetY }));
			}

			return true;
		}

		private _onKeyPress(event: JQueryEventObject): boolean {
			var self = this,
				targetZoom = self.options.targetZoom,
				newTargetZoom = targetZoom;
			switch (String.fromCharCode(event.which)) {
				case "+":
					newTargetZoom++;
					break;
				case "-":
					newTargetZoom--;
					break;
				default:
					return false;
			}

			if (targetZoom !== newTargetZoom) {
				self._setTargetZoom(newTargetZoom);
			}

			return true;
		}

		private _createLayers() {
			var self = this, o = self.options;

			self._container = $("<div class='" + containerCss + "'></div>").appendTo(self.element);

			self._multiScaleImageContainer = $("<div class=\"" + layerContainerCss + " " + tilesContainerCss + "\"></div>").appendTo(self._container);
			if (self._innerSource) {
				self._createMultiScaleImageLayer();
			}

			self._layersContainer = $("<div class=\"" + layerContainerCss + "\"></div>").appendTo(self._container);
			self._createInnerLayers();

			self._toolsLayerContainer = $("<div class=\"" + layerContainerCss + "\"></div>").appendTo(self._container);
			if (o.showTools) {
				self._createToolsLayer();
			}

			self._disabledCover = $("<div class='" + containerCss + "'></div>").hide().appendTo(self.element);
		}

		private _createMultiScaleImageLayer() {
			var self = this, o = self.options;
			self._multiScaleImageContainer.wijmultiscaleimage(self._createMultiscaleimageOptions());
		} 

		private _createInnerLayers() {
			var self = this, o = self.options, e = self.element;
			if (o.layers) {
				self._layers = [];
				$.each(o.layers, function (key, options: ILayerOptions) {
					var wijname = (options.type === LayerType[LayerType.custom]) ? (<ICustomLayerOptions>options).widgetName : "wij" + options.type + "layer",
						layer = self._createLayer(wijname, options);
					if (layer) {
						self._layers.push({ wijname:wijname, options: options, element: layer });
					}
				});
			}
		}

		private _createToolsLayer() {
			var self = this, o = self.options;
			self._toolsLayerContainer.wijtoolslayer({ "center": new Point(o.center.x, o.center.y), "zoom": o.targetZoom, "minZoom": self._minZoom, "maxZoom": self._maxZoom, "converter": self._coordConverter });
			self._toolsLayerContainer.on("wijtoolslayertargetzoomchanged." + self.widgetName, function (e, d) {
				if (o.targetZoom !== d["targetZoom"]) {
					self._setTargetZoom(d["targetZoom"]);
				}
			}).on("wijtoolslayertargetcenterchanged." + self.widgetName, function (e, d) {
				if (d.targetCenter && (!o.targetCenter || o.targetCenter.x !== d.targetCenter.x || o.targetCenter.y !== d.targetCenter.y )) {
					self._setTargetCenter({ "x": d.targetCenter.x, "y": d.targetCenter.y });
				}
			});
		}

		private _createLayer(wijname:string, options: ILayerOptions): JQuery {
			var self = this,
				wo = self._createWijlayerOptions(options),
				element = $("<div class=\"" + layerContainerCss + "\"></div>");
			self._layersContainer.append(element);
			if (element[wijname]) {
				return element[wijname](wo);
			}
			return null;
		}

		private _createWijlayerOptions(lo?: ILayerOptions): wijlayer_options {
			var self = this, o = self.options;

			return $.extend(new wijlayer_options(), {
				disabled: self._isDisabled(),
				center: new Point(o.center.x, o.center.y),
				zoom: o.zoom,
				targetCenter: new Point(o.targetCenter.x, o.targetCenter.y),
				targetZoom: o.targetZoom,
				isZooming: self._isZooming,
				converter: self._coordConverter
			}, lo);
		}

		private _updateLayers() {
			var self = this, o = self.options,
				layers = self._layers,
				options: wijlayer_options;

			self._coordConverter._updateSize();

			if (layers) {
				$.each(layers, function (key, item: ILayerItem) {
					options = self._createWijlayerOptions(item.options);
					item.element[item.wijname]("beginUpdate");
					item.element[item.wijname](options);
					item.element[item.wijname]("endUpdate");
				});
			}

			if (self._innerSource) {
				self._multiScaleImageContainer.wijmultiscaleimage(self._createMultiscaleimageOptions());
			}

			if (o.showTools) {
				self._toolsLayerContainer.wijtoolslayer({ "center": new Point(o.center.x, o.center.y), "zoom": o.targetZoom, "converter": self._coordConverter });
			}
		}

		private _createMultiscaleimageOptions() {
			var self = this, o = self.options;
			return { "source": self._innerSource, "zoom": o.zoom, "center": self._coordConverter.geographicToLogic(new Point(o.center.x, o.center.y)) };
		}

		/**
		* Refresh the map layers. Usually used when the items of the layer data changed.
		*/
		public refreshLayers() {
			var self = this, o = self.options,
				layers = self._layers;
			if (layers) {
				$.each(layers, function (key, item: ILayerItem) {
					item.element[item.wijname]("refresh");
				});
			}
		}

		/**
		* Convert the point from screen unit (pixel) to  geographic unit (longitude and latitude).
		* @param {IPoint} position The point to convert.
		*/
		public viewToGeographic(position: IPoint): IPoint {
			return this._coordConverter.viewToGeographic(position);
		}

		/**
		* Convert the point from geographic unit (longitude and latitude) to screen unit (pixel).
		* @param {IPoint} position The point to convert.
		*/
		public geographicToView(position: IPoint): IPoint {
			return this._coordConverter.geographicToView(position);
		}

		/**
		* Convert the point from screen unit (pixel) to logic unit (percentage).
		* @param {IPoint} position The point to convert.
		*/
		public viewToLogic(position: IPoint): IPoint {
			return this._coordConverter.viewToLogic(position);
		}

		/**
		* Convert the point from logic unit (percentage) to screen unit (pixel).
		* @param {IPoint} position The point to convert.
		*/
		public logicToView(position: IPoint): IPoint {
			return this._coordConverter.logicToView(position);
		}

		/**
		* Calculate the distance between two points.
		* @param {Point} lonLat1 The coordinate of first point, in geographic unit.
		* @param {Point} longLat2 The coordinate of second point, in geographic unit.
		* @returns {number} The distance between to points, in meters.
		*/
		public distance(lonLat1: IPoint, lonLat2: IPoint): number {
			return this._coordConverter.distance(lonLat1, lonLat2);
		}

		/**
		* Removes the wijmaps functionality completely. This will return the element back to its pre-init state.
		*/
		public destroy() {
			var self = this, o = self.options, e = self.element;

			e.removeClass(wijmapsCss);
			e.removeClass((<any>o).wijCSS.content);
			e.removeAttr("tabindex");

			self.element.off("." + self.widgetName);
			$(document).off("." + self.widgetName);
			self._destroyLayers();
			super.destroy();
		}

		private _destroyLayers() {
			var self = this, o = self.options;

			if (self._innerSource) {
				self._destroyTileLayer();
			}

			self._destroyInnerLayers();

			if (o.showTools) {
				self._destroyToolsLayer();
			}

			self._container.remove();
			self._disabledCover.remove();
		}

		private _destroyTileLayer() {
			var self = this;
			if (self._multiScaleImageContainer.data()["wijmoWijmultiscaleimage"]) {
				self._multiScaleImageContainer.off("." + self.widgetName).wijmultiscaleimage("destroy");
			}
		}

		private _destroyToolsLayer() {
			var self = this;
			if (self._toolsLayerContainer.data()["wijmoWijtoolslayer"]) {
				this._toolsLayerContainer.off("." + self.widgetName).wijtoolslayer("destroy");
			}
		}

		private _destroyInnerLayers() {
			var self = this;
			if (self._layers) {
				$.each(self._layers, (index, layer)=> {
					layer.element[layer.wijname]("destroy")
						.remove();
				});
				self._layers = null;
			}
		}
	}

	/**
	* The options of the wijmaps widget.
	*/
	export class wijmaps_options {
		/**
		* The source of the wijmaps tile layer. Wijmaps provides some built in sources:
		* <ul>
		* <li>bingMapsRoadSource</li>
		* <li>bingMapsAerialSource</li>
		* <li>bingMapsHybridSource</li>
		* </ul>
		* Set to null if want to hide the tile layer.<br>
		* Set to a IMultiScaleTileSource object if want to use another map tile server.
		*/
		source: any = "bingMapsRoadSource";

		/**
		* The bing mpas key of the bing maps source.
		* 
		*/
		bingMapsKey: string = "";

		/**
		* The center of wijmaps view port, in geographic unit.
		*/
		center: IPoint = { "x": 0, "y": 0 };

		/**
		* The current zoom of the wijmaps.
		*/
		zoom: number = 1;

		/**
		* Determine whether show the tools layer.
		*/
		showTools: boolean = true;

		/**
		* The target zoom of the wijmaps to scale.
		*/
		targetZoom: number;

		/**
		* The speed for scaling the wijmaps from current zoom to target zoom.
		*/
		targetZoomSpeed: number = 0.3;

		/**
		* The targer center of the wijmaps to translate.
		*/
		targetCenter: IPoint;

		/**
		* The speed for translate the wijmaps from current center to target center.
		*/
		targetCenterSpeed: number = 0.3;

		/** 
		* Defines the array of layers append to the wijmaps.
		*/
		layers: ILayerOptions[] = [];

		/**
		* This is a callback function called after the zoom of the wijmaps changed.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {IZoomChangedEventData} data The data contains zoom relates to this event.
		* @dataKey {number} zoom The zoom changed.
		*/
		zoomChanged: (event, data: IZoomChangedEventData) => void;

		/**
		* This is a callback function called after the target zoom of the wijmaps changed.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {ITargetZoomChangedEventData} data The data contains targetZoom relates to this event.
		* @dataKey {number} targetZoom The target zoom changed.
		*/
		targetZoomChanged: (event, data: ITargetZoomChangedEventData) => void;

		/**
		* This is a callback function called after the center of the wijmaps changed.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {ICenterChangedEventData} data The data contains center relates to this event.
		* @dataKey {Point} center The center changed.
		*/
		centerChanged: (event, data: ICenterChangedEventData) => void;

		/**
		* This is a callback function called after the target center of the wijmaps changed.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {ITargetCenterChangedEventData} data The data contains targetCenter relates to this event.
		* @dataKey {number} targetCenter The target center changed.
		*/
		targetCenterChanged: (event, data: ITargetCenterChangedEventData) => void;
	}

	/**
	* The data relates to the zoomChanged event.
	*/
	export interface IZoomChangedEventData {
		/** 
		* The zoom changed.
		*/
		zoom: number;
	}

	/**
	* The data relates to the targetZoomChanged event.
	*/
	export interface ITargetZoomChangedEventData {
		/** 
		* The target zoom changed.
		*/
		targetZoom: number;
	}

	/**
	* The data relates to the centerChanged event.
	*/
	export interface ICenterChangedEventData {
		/** 
		* The center changed.
		*/
		center: IPoint;
	}

	/**
	* The data relates to the targetCenterChanged event.
	*/
	export interface ITargetCenterChangedEventData {
		/** 
		* The target center changed.
		*/
		targetCenter: IPoint;
	}

	/** @ignore */
	interface ILayerItem {
		wijname: string;
		options: ILayerOptions;
		element: JQuery;
	}

	wijmaps.prototype.options = $.extend(true, {}, wijmo.wijmoWidget.prototype.options, new wijmaps_options());

	$.wijmo.registerWidget("wijmaps", wijmaps.prototype);
}