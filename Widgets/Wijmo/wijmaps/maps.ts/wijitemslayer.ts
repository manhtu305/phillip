﻿/// <reference path="wijvirtuallayer.ts" />
///<reference path="Drawing.ts"/>
///<reference path="Utils.ts"/>
///<reference path="wijlayer.ts"/>
/// <reference path="../../Base/jquery.wijmo.widget.ts" />
/// <reference path="../../wijutil/jquery.wijmo.raphael.ts"/> 
module wijmo.maps {
	  

	var $ = jQuery,
		layerContainerCss: string = "wijmo-wijmaps-layercontainer";

	/**
	* Defines slice info on specific zoom. Used by virtual layer.
	*/ 
	export interface IMapSlice {
		/**
		* The number of latitude divisions for this slice.
		*/
		latitudeSlices: number;
		/**
		* The number of longitude divisions for this slice. 
		*/
		longitudeSlices: number;
		/**
		* The minimum zoom of this slice.
		*/
		zoom: number;
	}

	/**
	* @ignore
	* Defines slice info on specific zoom. Used by virtual layer.
	*/ 
	export class MapSlice implements IMapSlice {
		/**
		* @param {number} latitudeSlices The number of latitude divisions for this slice.
		* @param {number} longitudeSlices The number of longitude divisions for this slice. 
		* @param {number} zoom The minimum zoom of this slice.
		*/
		constructor(public latitudeSlices: number, public longitudeSlices: number, public zoom: number) { }
	}

	/** @ignore */
	export class Region {
		public items: IItemsLayerItem[];

		constructor(public slice: MapSlice, public longitude: number, public latitude: number) { }

		public equals(that: Region): boolean {
			var self = this;
			return self.slice === that.slice &&
				self.longitude === that.longitude &&
				self.latitude === that.latitude;
		}
	}

	/** 
	* @widget
	* The items layer for the wijmaps. Display its items positioned geographically.
	*/
	export class wijitemslayer extends wijlayer {
		private _container: JQuery;
		private _paper: RaphaelPaper;
		private _visibleRegions: Region[];

		/**
		* The options of the wijitemslayer.
		*/
		public options: wijitemslayer_options;

		/** @ignore */
		_create() {
			var self = this;

			self._container = $("<div class=\"" + layerContainerCss + "\"></div>");
			self.element.append(self._container);
			self._paper = self._createPaper(self._container[0], self.options.converter.getViewSize());
			super._create();

			self._setRequest(self.options.request);
		}

		/** @ignore */
		_setOption(key: string, value: any) {
			var self = this, o = this.options;
			if (o[key] !== value) {
				switch (key) {
					case 'request':
						
						self._setRequest(value);
						return;
				}
			}
			super._setOption(key, value);
		}

		private _setRequest(request : any) {
			var self = this;

			self.options.request = request;
			self._clear();
			if (request) {
				var result: IItemsLayerRequestResult = request(self._paper);
				if (result) {
					var region = new Region(new MapSlice(1, 1, 0), 0, 0);
					region.items = result.items;
					wijitemslayer._setItemsData(result.items);
					self._visibleRegions = [region];
				}
			}
			self.update();
		}

		/** @ignore */
		_render() {
			var self = this, converter = self.options.converter;
			self._resizePaper(self._paper, converter.getViewSize());
			wijitemslayer._relayoutRegions(self.options.converter, self._visibleRegions);
		}

		private _clear() {
			var self = this;
			self._visibleRegions = null;
			if (self._paper) {
				self._paper.clear();
			}
		}

		/** @ignore */
		static _relayoutRegions(converter:ICoordConverter, regions : Region[]) {
			if (!regions) return;

			var viewSize = converter.getViewSize(),
				scale = viewSize.width / converter.getLogicSize().width,
				center = converter.getLogicCenter(),
				offsetX = viewSize.width / 2 - center.x * scale,
				offsetY = viewSize.height / 2 - center.y * scale;

			$.each(regions, (regionIndex, region)=> {
				if (region.items) {
					$.each(region.items, (itemIndex, item)=> {
						if (item.elements) {
							var logicLocation = converter.geographicToLogic(item.location),
								itemLeft = logicLocation.x * scale + offsetX - ((!!item.pinpoint) ? item.pinpoint.x : 0),
								itemTop = logicLocation.y * scale + offsetY - ((!!item.pinpoint) ? item.pinpoint.y : 0),
								translate = "t" + itemLeft + "," + itemTop;
							$.each(item.elements, (elementIndex, element)=> {
								element.transform(element.data("wijmo-transform") + translate);
							});
						}
					});
				}
			});
		}

		/** @ignore */
		static _setItemsData(items: IItemsLayerItem[]) {
			if (items) {
				$.each(items, (itemIndex, item)=> {
					if (item.elements) {
						$.each(item.elements, (elementIndex, element)=> {
							element.data("wijmo-transform", element.transform());
						});
					}
				});
			}
		}

		/**
		* Force to request the data to get the newest one.
		*/
		public refresh() {
			var self = this;
			self._setRequest(self.options.request);

			super.refresh();
		}

		/**
		* Removes the wijitemslayer functionality completely. This will return the element back to its pre-init state.
		*/
		public destroy() {
			var self = this;
			if (self._paper) self._paper.remove();
			self._container.remove();

			super.destroy();
		}
	}

	/**
	* Defines the wijmaps items layer.
	*/
	export interface IItemsLayerOptions extends ILayerOptions {
		/**
		* The function to be called when request the data.
		* @event
		* @param {RaphaelPaper} paper The Raphael paper to draw the items.
		* @returns {IItemsLayerRequestResult} The object which contains items to be drawn. 
		* @remarks 
		* <p>The items layer request the items to be drawn on the first update. 
		* If want to refresh the items, call refresh() method if necessary.</p>
		* <p>The items will be arranged to the position based on it's location.</p>
		*/
		request: (paper: RaphaelPaper) => IItemsLayerRequestResult;
	}

	/**
	* Defines the object of the request result for the items layer.
	*/
	export interface IItemsLayerRequestResult {
		/**
		* The array of individual item to be drawn.
		*/
		items: IItemsLayerItem[];
	}

	/**
	* Defines the object of the requested individual item for the items layer.
	*/
	export interface IItemsLayerItem {
		/**
		* The location of the item, in geographic unit.
		*/
		location: IPoint;

		/**
		* The point within the item to pin the element on the location. The (0,0) is used if not specified.
		*/ 
		pinpoint?: IPoint;

		/**
		* The set of Rapahel elements to be drawn.
		*/
		elements: RaphaelSet;
	}

	/**
	* The options of the wijitemslayer.
	*/
	export class wijitemslayer_options extends wijlayer_options implements IItemsLayerOptions {
		/**
		* The function to be called when request the data.
		* @event
		* @param {RaphaelPaper} paper The Raphael paper to draw the items.
		* @returns {IItemsLayerRequestResult} The object which contains items to be drawn. 
		* @remarks 
		* <p>The items layer request the items to be drawn on the first update. 
		* If want to refresh the items, call refresh() method if necessary.</p>
		* <p>The items will be arranged to the position based on it's location.</p>
		*/
		request: (paper: RaphaelPaper) => IItemsLayerRequestResult;
	}

	wijitemslayer.prototype.options = $.extend(true, {}, wijmo.wijmoWidget.prototype.options, new wijitemslayer_options());

	$.wijmo.registerWidget("wijitemslayer", wijitemslayer.prototype);
}