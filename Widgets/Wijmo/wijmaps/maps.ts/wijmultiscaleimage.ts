﻿/// <reference path="mapsources.ts" />
/// <reference path="drawing.ts" />
/// <reference path="../../Base/jquery.wijmo.widget.ts" />
module wijmo.maps {
	  
	var $ = jQuery,
		tileCss: string = "wijmo-wijmaps-tile";

	/** 
	* @widget
	* The tile layer for wijmaps. Draw the map tiles.
	*/
	export class wijmultiscaleimage extends wijmoWidget {
		/** 
		* The options for the wijmultiscaleimage widget.
		*/
		public options: wijmultiscaleimage_options;

		private _tileItems: any;
		private _tileScale: number;

		/** @ignore */
		_create() {
			var self = this, o = self.options;
			self._tileItems = {};
			self._tileScale = self._getTileScale(Math.round(o.zoom), o.zoom);
			self._renderTiles();
		}

		/** @ignore */
		_setOption(key: string, value: any) {
			var self = this, o = self.options;
			if (o[key] !== value) {
				switch (key) {
					case 'center':
						self._setCenter(value);
						return;
					case 'zoom':
						self._setZoom(value);
						return;
					case 'source':
						self._setSource(value);
						return;
				}
			}
			super._setOption(key, value);
		}

		private _setCenter(center: Point) {
			var self = this, o = self.options, offset = new Size(center.x - o.center.x, center.y - o.center.y);

			self._offsetTiles(offset);
			o.center = center;
			self._renderTiles();
		}

		private _setZoom(zoom: number) {
			var self = this, o = self.options, oldZoom = o.zoom, curZoom: number, roundZoom: number;
			if (zoom >= o.source.minZoom && zoom <= o.source.maxZoom) {
				self._tileScale = self._getTileScale(Math.round(zoom), zoom);

				//scale old tiles to target zoom.
				self._scaleTiles(self._getTileScale(oldZoom, zoom));

				//render new tiles by target zoom.
				o.zoom = zoom;
				setTimeout(() => { self._renderTiles(); }, 10);  //make the zooming more smooth.
			}
		}

		private _setSource(source: IMultiScaleTileSource) {
			var self = this, o = self.options;
			o.source = source;
			$.each(self._tileItems, (key: string, val: TileItem) => {
				val.img.remove();
				delete self._tileItems[key];
			});
			self._renderTiles();
		}

		private _offsetTiles(offset: Size) {
			var self = this, fullSize = self._getViewFullSize();
			$.each(self._tileItems, function (key, val: TileItem) {
				val.bounds.location = new Point(val.bounds.location).offset(-offset.width * fullSize.width, -offset.height * fullSize.height);
				val.img.css("left", val.bounds.location.x).css("top", val.bounds.location.y);
			});
		}

		private _getTileScale(baseZoom: number, targetZoom: number): number {
			return Math.pow(2, targetZoom - baseZoom);
		}

		private _scaleTiles(scale) {
			var self = this, viewSize = self._getViewSize(), viewRect = new Rectangle(new Point(0, 0), viewSize);
			$.each(self._tileItems, (key: string, val: TileItem) => {
				val.bounds.location.x = viewSize.width / 2 - (viewSize.width / 2 - val.bounds.location.x) * scale;
				val.bounds.location.y = viewSize.height / 2 - (viewSize.height / 2 - val.bounds.location.y) * scale;
				val.bounds.size = new Size(val.bounds.size).scale(scale, scale);

				if (viewRect.intersect(val.bounds)) {
					val.img
						.css("left", val.bounds.location.x)
						.css("top", val.bounds.location.y)
						.css("width", val.bounds.size.width)
						.css("height", val.bounds.size.height);
				}
				else {
					val.img.remove();
					delete self._tileItems[key];
				}
			});
		}

		private _renderTiles() {
			var self = this, o = self.options,
				baseZoom = Math.round(o.zoom),
				leftTopTileItem = self._getLeftTopTileItem(),
				viewSize = self._getViewSize(),
				tileSize = self._getTileSize(),
				maxTileCount = self._getMaxTileCount(o.zoom),
				maxHorizontalTileCount = maxTileCount - leftTopTileItem.columnIndex,
				maxVerticalTileCount = maxTileCount - leftTopTileItem.rowIndex,
				horizontalTileCount = Math.min(maxHorizontalTileCount, Math.ceil((viewSize.width - leftTopTileItem.bounds.location.x) / tileSize.width)),
				verticalTileCount = Math.min(maxVerticalTileCount, Math.ceil((viewSize.height - leftTopTileItem.bounds.location.y) / tileSize.height)),
				left, top, rowIndex, columnIndex, tile: TileItem, oldTiles = $.extend({}, self._tileItems), preLoadTiles = [];

			for (rowIndex = leftTopTileItem.rowIndex; rowIndex < leftTopTileItem.rowIndex + verticalTileCount; rowIndex++) {
				for (columnIndex = leftTopTileItem.columnIndex; columnIndex < leftTopTileItem.columnIndex + horizontalTileCount; columnIndex++) {
					left = leftTopTileItem.bounds.location.x + tileSize.width * (columnIndex - leftTopTileItem.columnIndex);
					top = leftTopTileItem.bounds.location.y + tileSize.height * (rowIndex - leftTopTileItem.rowIndex);
					tile = new TileItem(columnIndex, rowIndex, new Rectangle(new Point(left, top), new Size(tileSize.width, tileSize.height)), baseZoom);
					self._renderTile(tile, baseZoom, oldTiles, preLoadTiles);
				}
			}
		}

		private _renderTile(tile: TileItem, baseZoom: number, oldTiles: any, preLoadTiles: Array<string>) {
			var self = this, o = self.options, location = tile.bounds.location, size = tile.bounds.size,
				style: string, tileUrl: string, oldTile: TileItem, img: JQuery,
				tileItemKey = "tile_" + tile.columnIndex + "_" + tile.rowIndex + "_" + tile.zoomLevel;

			if (!self._tileItems[tileItemKey]) {
				self._tileItems[tileItemKey] = tile;
				tileUrl = o.source.getUrl(tile.zoomLevel, tile.columnIndex, tile.rowIndex);
				style = "left:" + location.x + "px; top:" + location.y + "px; width:" + size.width + "px; height:" + size.height + "px; display:none";
				tile.img = $("<img class=\"" + tileCss + "\" style=\"" + style + "\" src=\"" + tileUrl + "\"></img>");
				preLoadTiles.push(tileItemKey);
				tile.img.one("load.wijmultiscaleimage", function (event, data) {
					self._onTileLoad(tile.img, tileItemKey, baseZoom, oldTiles, preLoadTiles);
				});
				self.element.append(tile.img);
			}
			else {
				oldTile = self._tileItems[tileItemKey];
				oldTile.bounds = tile.bounds;
				oldTile.img.css("left", location.x).css("top", location.y).css("width", size.width).css("height", size.height);
				self.element.append(oldTile.img);
			}
		}

		private _onTileLoad(tileImg: JQuery, tileItemKey: string, baseZoom: number, oldTiles: any, preLoadTiles: Array<string>) {
			var self = this, o = self.options;
			tileImg.fadeIn(600, () => {
				preLoadTiles.splice($.inArray(tileItemKey, preLoadTiles), 1);
				if (preLoadTiles.length==0) {
					$.each(oldTiles, (key: string, val: TileItem) => {
						if (self._isUselessTile(val, baseZoom)) {
							val.img.remove();
							// need refactor. sometimes the remove method has no effect.
							if ($("img[src='" + val.img.attr("src") + "']").length == 0) {
								delete oldTiles[key];
								delete self._tileItems[key];
							}
						}
					});
				}
			});
		}

		private _isUselessTile(tile: TileItem, baseZoom: number): boolean {
			var self = this, o = self.options,
				curBaseZoom = Math.round(o.zoom),
				viewRect = new Rectangle(Point.empty, self._getViewSize()),
				intersectRect = viewRect.intersect(tile.bounds);

			if (tile.zoomLevel === baseZoom || tile.zoomLevel === curBaseZoom) {
				return !intersectRect;
			}
			return true;
		}

		private _isPointInRect(p: Point, rect: Rectangle): boolean {
			return p.x >= rect.location.x && p.x <= rect.location.x + rect.size.width
				&& p.y >= rect.location.y && p.y <= rect.location.y + rect.size.height;
		}

		private _getLeftTopTileItem(): TileItem {
			var self = this, o = self.options,
				maxTileCount = self._getMaxTileCount(o.zoom),
				centerTileLocation = new Point(o.center).scale(maxTileCount, maxTileCount),
				tileSize = self._getTileSize(),
				viewSize = self._getViewSize(),
				tileRowIndex = Math.floor(centerTileLocation.y),
				tileColumnIndex = Math.floor(centerTileLocation.x),
				tilePosition = self._getViewCenter().offset(- (centerTileLocation.x % 1) * tileSize.width, - (centerTileLocation.y % 1) * tileSize.height);

			while (tileColumnIndex > 0 && tilePosition.x > 0) {
				tileColumnIndex -= 1;
				tilePosition.x -= tileSize.width;
			}
			while (tileRowIndex > 0 && tilePosition.y > 0) {
				tileRowIndex -= 1;
				tilePosition.y -= tileSize.height;
			}

			return new TileItem(tileColumnIndex, tileRowIndex, new Rectangle(tilePosition, tileSize), Math.round(o.zoom));
		}

		private _getTileSize(): Size {
			var self = this, o = self.options;
			return new Size(o.source.tileWidth, o.source.tileHeight).scale(self._tileScale, self._tileScale);
		}

		private _getMaxTileCount(zoom: number): number {
			return Math.pow(2, Math.round(zoom));
		}

		private _getViewSize(): Size {
			var self = this;
			return new Size(self.element.width(), self.element.height());
		}

		private _getViewCenter(): Point {
			var self = this, viewSize = self._getViewSize();
			return new Point(viewSize.width / 2, viewSize.height / 2);
		}

		private _getViewFullSize(): Size {
			var self = this, o = self.options, scaledTileCount = Math.pow(2, o.zoom);
			return new Size(o.source.tileWidth, o.source.tileHeight).scale(scaledTileCount, scaledTileCount);
		}

		/**
		* Removes the wijmultiscaleimage functionality completely. This will return the element back to its pre-init state.
		*/
		public destroy() {
			this.element.children().remove();
			super.destroy();
		}
	}

	/** @ignore */
	class TileItem {
		constructor(public columnIndex: number, public rowIndex: number, public bounds: Rectangle, public zoomLevel: number, public img?: JQuery) { }
	}

	/**
	* The options of wijmultiscaleimage.
	*/
	export class wijmultiscaleimage_options {
		/**
		* The source of the wijmaps tile layer. Wijmaps provides some built in sources:
		* <ul>
		* <li>wijmo.maps.MapSources.bingMapsRoadSource</li>
		* <li>wijmo.maps.MapSources.bingMapsAerialSource</li>
		* <li>wijmo.maps.MapSources.bingMapsHybridSource</li>
		* </ul>
		* Set to null if want to hide the tile layer.<br>
		* Set to a new object if want to use another map tile server.
		*/
		source: IMultiScaleTileSource;

		/**
		* The current center of the layer, in geographic unit.
		*/
		center: IPoint = new Point(0, 0);

		/**
		* The current zoom level of the layer.
		*/
		zoom: number = 3;
	}

	wijmultiscaleimage.prototype.options = $.extend(true, {}, wijmo.wijmoWidget.prototype.options, new wijmultiscaleimage_options());

	$.wijmo.registerWidget("wijmultiscaleimage", wijmultiscaleimage.prototype);
}