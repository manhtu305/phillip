﻿///<reference path="CoordConverter.ts"/>

module wijmo.maps {
	  
	
	var $ = jQuery;

	/** @ignore */
	export enum LayerType {
		vector,
		items,
		virtual,
		custom
	}

	/** 
	* @widget
	* The base widget of the wijmaps layer.
	*/
	export class wijlayer extends wijmoWidget implements IWijlayer {
		private _updating: boolean;
		private _needUpdate: boolean;

		/** 
		* The options for the wijlayer widget.
		*/
		public options: wijlayer_options;

		/** @ignore */
		_create() {
			super._create();

			var self = this;
			self._updating = false;
			self._needUpdate = false;
		}

		/**
		* Create the Raphael paper within the container and specific size.
		*/
		_createPaper(container, size:ISize): RaphaelPaper {
			var clip, paper = Raphael(container, 1, 1);
			$(paper.canvas).css("overflow", "visible");
			this._resizePaper(paper, size);
			return paper;
		}

		/**
		* Update the Raphael paper size based on the container size.
		*/
		_resizePaper(paper: RaphaelPaper, size: ISize) {
			if (Raphael.svg) {
				$(paper.canvas).css("width", size.width + "px").css("height", size.height + "px");
			}
			else {
				$(paper.canvas).css("clip", "rect(0px," + size.width + "px," + size.height + "px,0px)");
				// vml, keept 1px*1px;
			}
		}

		/** @ignore */
		_setOption(key: string, value: any) {
			var self = this, o = this.options;
			if (o[key] !== value) {
				switch (key) {
					case 'center':
						self._setCenter(value);
						return;
					case 'zoom':
						self._setZoom(value);
						return;
				}
			}
			super._setOption(key, value);
		}

		/**
		* Set the center of the layer.
		* @param {Point} center The current center of the layer, in geographic unit.
		*/
		_setCenter(center: IPoint) {
			var self = this, o = self.options;
			if (o.center.x !== center.x || o.center.y !== center.y) {
				o.center = center;
				self.update();
			}
		}

		/** 
		* Set the zoom of the layer.
		* @param {number} zoom The current zoom of the layer.
		*/
		_setZoom(zoom: number) {
			var self = this, o = self.options;
			if (o.zoom !== zoom) {
				o.zoom = zoom;
				self.update();
			}
		}

		/**
		* Draw the layer.
		*/
		_render() {
		}

		/**
		* Update the layer, redraw the layer if needed. It's called when the center and zoom changed.
		*/ 
		public update() {
			var self = this, o = self.options;
			if (self._updating) {
				self._needUpdate = true;
				return;
			}

			self._render();
		}

		/**
		* Called when the wijmaps begins to update the layer status.
		*/ 
		public beginUpdate() {
			this._updating = true;
		}

		/** 
		* Called when the wijmaps ends update the layer status. The layer will be updated after this function called.
		*/
		public endUpdate() {
			var self = this;
			self._updating = false;
			if (self._needUpdate) {
				self.update();
			}
		}

		/**
		* Refresh the layer. Usually used if want to apply the new data to the layer.
		*/
		public refresh() {
			this.update();
		}
	}

	/**
	* The layer which can append to the wijmaps.
	*/ 
	export interface IWijlayer {
		/**
		* Called when the wijmaps begins to update the layer status.
		*/ 
		beginUpdate: () => void;

		/** 
		* Called when the wijmaps ends update the layer status. The layer will be updated after this function called.
		*/
		endUpdate: () => void;

		/**
		* Update the layer, redraw the layer if needed. It's called when the center and zoom changed.
		*/ 
		update: () => void;

		/**
		* Refresh the layer. Usually used if want to apply the new data to the layer.
		*/
		refresh: () => void;

		/**
		* The options of the wijmaps layer.
		*/
		options: wijlayer_options;
	}

	/**
	* Defines the wijmaps layer.
	*/
	export interface ILayerOptions {
		/**
		* The type of the wijmaps layer. Possible values are:
		* <ul>
		* <li>vector: The vector layer. Used to draw a shape or placemark. </li>
		* <li>items: The items layer. Draw user defined items in the map. </li>
		* <li>virtual: The virual layer. Similar to items layer. Can draw different items in diffrent zoom.</li>
		* <li>custom: The user customized layer. </li>
		* </ul>
		*/
		type: string;
	}

	/**
	* Defines the custom wijmaps layer.
	*/
	export interface ICustomLayerOptions extends ILayerOptions {
		/**
		* The name of the widget which is used to render the custom wijmaps layer.
		* @remarks The widget need implements the IWijlayer interface. 
		*/
		widgetName: string;
	}

	/**
	* The options of the wijlayer.
	*/ 
	export class wijlayer_options implements WidgetOptions, ILayerOptions{
		/**
		* Indicates whether the layer is disabled or not.
		*/
		disabled: boolean;

		/**
		* The type of the wijlayer. Wijmaps supports built-in vector, items and virtual layer. 
		* For custom layer, the type is "custom".
		*/
		type: string;

		/**
		* The current center of the layer, in geographic unit.
		*/
		center: IPoint;

		/** 
		* The target center of the layer, in geographic unit.
		*/
		targetCenter: IPoint;

		/**
		* The current zoom of the layer.
		*/
		zoom: number;

		/** 
		* The target zoom of the layer.
		*/
		targetZoom: number;

		/**
		* Specifies whether the map is on zooming.
		*/
		isZooming: boolean;

		/**
		* The converter used to convert the coordinates in geograhic unit (longitude and latitude), logic unit(percentage) and screen unit (pixel).
		*/
		converter: ICoordConverter;
	}

	wijlayer.prototype.options = $.extend(true, {}, wijmo.wijmoWidget.prototype.options, new wijlayer_options());

	$.wijmo.registerWidget("wijlayer", wijlayer.prototype);
}
