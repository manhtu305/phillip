﻿/// <reference path="utils.ts" />
/// <reference path="../../wijutil/jquery.wijmo.raphael.ts"/> 
module wijmo.maps {
	  

	/**
	* Defines a point with horizontal and vertical coordinates.
	*/
	export interface IPoint {
		/**
		* The coordinate in horizontal.
		*/
		x: number;

		/**
		* The coordinate in vertical.
		*/
		y: number;
	}

	/**
	* @ignore
	* Defines a point with horizontal and vertical coordinates.
	*/
	export class Point implements IPoint {
		x: number;
		y: number;

		/**
		* The Point object at (0,0).
		*/
		public static empty: Point = new Point(0, 0);

		/**
		* @param {number} x The coordinate in horizontal.
		* @param {number} y The coordinate in vertical.
		*/
		constructor(point: IPoint);
		constructor(x: number, y: number);
		constructor(pointOrX:any, y? : number)
		{
			var self = this;
			if (typeof (pointOrX) === "number") {
				self.x = <number>pointOrX;
				self.y = y;
			}
			else {
				var point = <IPoint>pointOrX;
				self.x = point.x;
				self.y = point.y;
			}
		}

		/** 
		* Gets a new Point which has a offset to the current Point.
		* @param {number} offsetX The offset in horizontal.
		* @param {number} offsetY The offset in vertical.
		* @returns {Point} The new Point after offset.
		*/
		public offset(offsetX: number, offsetY: number): Point {
			return new Point(this.x + offsetX, this.y + offsetY);
		}

		/**
		* Gets a new Point which has a scale to the current Point.
		* @param {number} scaleX The scale in horizontal.
		* @param {number} scaleY The scale in vertical.
		* @returns {Point} The new point after scale.
		*/
		public scale(scaleX: number, scaleY: number): Point {
			return new Point(this.x * scaleX, this.y * scaleY);
		}
	}

	/**
	* Defines an ordered pair  of number, which specify a width and height.
	*/
	export interface ISize {
		/**
		* The width of the size.
		*/
		width: number;
		/**
		* The height of the size.
		*/ 
		height: number;
	}

	/**
	* @ignore
	* Defines an ordered pair  of number, which specify a width and height.
	*/
	export class Size implements ISize {
		width: number;
		height: number;

		/**
		* The Size object with zero size (0*0).
		*/
		public static empty: Size = new Size(0, 0);

		/**
		* @param {number} width The width of the size.
		* @param {number} height The height of the size.
		*/
		constructor(width: number, height: number);
		constructor(size: ISize);
		constructor(sizeOrWidth: any, height?: number) {
			var self = this;
			if (typeof (sizeOrWidth) === "number") {
				self.width = <number>sizeOrWidth;
				self.height = height;
			} else {
				var size = <ISize>sizeOrWidth;
				self.width = size.width;
				self.height = size.height;
			}
		}

		/** 
		* Gets a new Size object which has a scale to the current Size.
		* @param {number} scaleX The scale of the width.
		* @param {number} scaleY The scale of the height.
		* @returns {Size} The new Size object after scale.
		*/
		public scale(scaleX: number, scaleY: number): Size {
			return new Size(this.width * scaleX, this.height * scaleY);
		}
	}

	/**
	* Defines a rectangle with location and size.
	*/
	export interface IRectangle {
		/**
		* The upper-left point of the rectangle.
		*/
		location: IPoint;
		/**
		* The size of rectangle.
		*/
		size: ISize;
	}

	/**
	* @ignore
	* Defines a rectangle with location and size.
	*/
	export class Rectangle implements IRectangle {
		/**
		* The rectangle object with empty location and empty size.
		*/
		public static empty: Rectangle = new Rectangle(Point.empty, Size.empty);

		/**
		* @param {Point} location The upper-left point of the rectangle.
		* @param {Size} size The size of the rectangle.
		*/
		constructor(public location: IPoint, public size: ISize) { }

		/**
		* Gets the rectangle which is the intersection of itself and the specified rectangle.
		* @param {Rectangle} rect The rectangle with which to intersect.
		* @returns {Rectangle} The intersection rectangle of itself and the specified rectangle.
		*/
		public intersect(rect: Rectangle): Rectangle {
			var self = this,
				x = Math.max(self.location.x, rect.location.x),
				right = Math.min(self.location.x + self.size.width, rect.location.x + rect.size.width),
				y = Math.max(self.location.y, rect.location.y),
				bottom = Math.min(self.location.y + self.size.height, rect.location.y + rect.size.height);

			if (right >= x && bottom >= y) {
				return new Rectangle(new Point(x, y), new Size(right - x, bottom - y));
			}

			return null;
		}

		/**
		* Tests whether the location and size are both empty.
		* @returns {boolean} true if the location and size are both empty; otherwise, false.
		*/ 
		public isEmpty(): boolean {
			var self = this;
			return self.size.width === 0 && self.size.height === 0 && self.location.x === 0 && self.location.y === 0;
		}

		/**
		* Gets the right of the rectangle. 
		* @returns The right of the rectangle which is left + width.
		*/
		public getRight(): number {
			return this.location.x + this.size.width;
		}

		/**
		* Gets the bottom of the rectangle.
		* @returns The bottom of the rectangle which is top + height.
		*/
		public getBottom(): number {
			return this.location.y + this.size.height;
		}

		/**
		* Determines if this rectangle intersects with the specified rectangle.
		* @param {Rectangle} rect The rectangle with which to check.
		* @returns {boolean} true if this rectangle intersects with the specified rectangle; otherwise, false.
		*/
		public intersectsWith(rect: Rectangle): boolean {
			if (!rect) return false;
			var self = this;
			return ((((self.location.x <= (rect.location.x + rect.size.width)) &&
				((self.location.x + self.size.width) >= rect.location.x)) &&
				(self.location.y <= (rect.location.y + rect.size.height))) &&
				((self.location.y + self.size.height) >= rect.location.y));
		}

		/**
		* Gets a new rectangle which is the union of itself and the specified rectangle.
		* @param {Rectangle} rect The rectangle with which to union.
		* @returns {Rectangle} The new rectangle which is the union of this rectangle and the specified rectangle.
		*/
		public union(rect: Rectangle): Rectangle {
			var self = this;
			if (!rect)
				return $.extend(true, {}, self);

			var left = Math.min(self.location.x, rect.location.x),
				top = Math.min(self.location.y, rect.location.y),
				right = Math.max(self.location.x + self.size.width, rect.location.x + rect.size.width),
				bottom = Math.max(self.location.y + self.size.height, rect.location.y + rect.size.height);

			return new Rectangle(new Point(left, top), new Size(right - left, bottom - top));
		}
	}

	/** 
	* @ignore 
	* Represents the vector shape.
	*/
	export class Shape {
		fill: string = "transparent";
		fillOpacity: number = 1;
		stroke: string = "#000";
		strokeOpacity: number = 1;
		strokeWidth: number = 1.0;
		strokeDashArray: string="";
		strokeLineCap: PenLineCap = PenLineCap.butt;
		strokeLineJoin: PenLineJoin = PenLineJoin.bevel;
		strokeMiterLimit: number = 1.0;

		public createElement(paper : RaphaelPaper): RaphaelSet {
			return null;
		}

		_setElementAttributes(st: RaphaelSet) {
			var self = this;
			if (st) {
				st.attr({
					fill: self.fill,
					"fill-opacity": self.fillOpacity,
					stroke: self.stroke,
					"stroke-opacity": self.strokeOpacity,
					"stroke-width": self.strokeWidth,
					"stroke-dasharray": self.strokeDashArray,
					"stroke-linecap": PenLineCap[self.strokeLineCap] || PenLineCap[PenLineCap.butt],
					"stroke-linejoin": PenLineJoin[self.strokeLineJoin] || PenLineJoin[PenLineJoin.bevel],
					"stroke-miterlimit":self.strokeMiterLimit
				});

				if (self.fill === "transparent") {
					st.attr("fill", "white")
						.attr("fill-opacity", "0");
				}
			}
		}
	}

	/** @ignore */
	export class Polyline extends Shape {
		points: Point[];

		public createElement(paper: RaphaelPaper): RaphaelSet {
			paper.setStart();
			var self = this,
				path = Utils.createPath(self.points, false),
				st: any;

			if (path) {
				paper.path(path);
			}
			
			st = paper.setFinish();
			self._setElementAttributes(st);
			return st;
		}
	}

	/** @ignore */
	export class MultiPolyline extends Shape {
		points: IPoint[][];

		public createElement(paper: RaphaelPaper): RaphaelSet {
			paper.setStart();
			var self = this,
				path, st;

			if (self.points) {
				$.each(self.points, function (index, linePoints) {
					path = Utils.createPath(linePoints, false);
					if (path) {
						paper.path(path);
					}
				});
			}

			st = paper.setFinish();
			self._setElementAttributes(st);
			return st;
		}
	}

	/** @ignore */
	export class Polygon extends Shape {
		fillRule: FillRule = FillRule.evenOdd;
		pointsList: IPoint[][] = [];

		public createElement(paper: RaphaelPaper): RaphaelSet {
			paper.setStart();
			var self = this,
				path = Utils.createListPath(self.pointsList, true),
				st: any;

			if (path) {
				paper.path(path);
			}

			st = paper.setFinish();
			self._setElementAttributes(st);
			return st;
		}
	}

	/** @ignore */
	export enum FillRule {
		evenOdd,
		nonezero
	}

	/** @ignore */
	export enum PenLineCap {
		butt,
		square,
		round,
	}

	/** @ignore */
	export enum PenLineJoin {
		bevel,
		round,
		miter
	}
}
