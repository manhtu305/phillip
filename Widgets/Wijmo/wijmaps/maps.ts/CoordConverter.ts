﻿/// <reference path="MapProjection.ts" />
/// <reference path="../../Base/jquery.wijmo.widget.ts" />
module wijmo.maps {
	  

	/** 
	* The converter to convert the coordinates in geographic unit (longitude and latitude), logic unit(percentage) and screen unit (pixel).
	*/
	export interface ICoordConverter {
		/** @ignore */
		minLongitude: number;
		/** @ignore */
		maxLongitude: number;
		/** @ignore */
		minLatitude: number;
		/** @ignore */
		maxLatitude: number;

		/**
		* The jQuery object that represents the wijmaps element.
		*/
		element: JQuery;

		/**
		* The current center of the map, in geographic unit.
		*/
		center: IPoint;

		/**
		* The current zoom of the map.
		*/
		zoom: number;

		/**
		* The width of the tile, in pixel.
		*/
		tileWidth: number;

		/**
		* The height of the tile, in pixel.
		*/
		tileHeight: number;

		/**
		* Convert the point coordinates from screen unit (pixel) to geographic unit (longitude and latitude).
		* @param {IPoint} position The point to convert, in screen unit.
		* @returns {IPoint} The new point after converted, in geographic unit.
		*/
		viewToGeographic(position: IPoint): IPoint;

		/**
		* Convert the point coordinates from geographic unit (longitude and latitude) to screen unit (pixel).
		* @param {IPoint} position The point to convert, in geographic unit.
		* @returns {IPoint} The new point after converted, in screen unit.
		*/
		geographicToView(position: IPoint): IPoint;

		/**
		* Convert the point coordinates from screen unit (pixel) to logic unit (percentage).
		* @param {IPoint} position The point to convert, in screen unit.
		* @returns {IPoint} The new point after converted, in logic unit.
		*/
		viewToLogic(position: IPoint): IPoint;

		/**
		* Convert the point coordinates from logic unit (percentage) to screen unit (pixel).
		* @param {IPoint} position The point to convert, in loogic unit.
		* @returns {IPoint} The new point after converted, in screen unit.
		*/
		logicToView(position: IPoint): IPoint;

		/**
		* Convert the point coordinates from geographic unit (longitude and latitude) to logic unit (percentage).
		* @param {IPoint} position The point to convert, in geographic unit.
		* @returns {IPoint} The new point after converted, in logic unit.
		*/
		geographicToLogic(lonLat: IPoint): IPoint;

		/**
		* Convert the point coordinates from logic unit (percentage) to geographic unit (longitude and latitude).
		* @param {IPoint} position The point to convert, in logic unit.
		* @returns {IPoint} The new point after converted, in geographic unit.
		*/
		logicToGeographic(position: IPoint): IPoint;

		/**
		* Gets the size of the map viewport.
		@returns {ISize} The size of the map viewport, in screen unit (pixel).
		*/
		getViewSize(): ISize;

		/**
		* Gets the size of the map viewport.
		* returns {ISize} The size of the map viewport, in logic unit (percentage).
		*/
		getLogicSize(): ISize;

		/**
		* Gets the center of the map viewport.
		* returns {IPoint} The center of the map view port, in screen unit (pixel).
		*/
		getViewCenter(): IPoint;

		/**
		* Gets the center of the map viewport.
		* returns {IPoint} The center of the map view port, in logic unit (percentage).
		*/
		getLogicCenter(): IPoint;

		/**
		* Gets the size of the full map in current zoom level.
		* @returns {ISize} The size of the full map, in screen unit (pixel).
		*/
		getFullMapSize(): ISize;

		/**
		* Calculate the distance between two points.
		* @param {IPoint} lonLat1 The coordinate of first point, in geographic unit.
		* @param {IPoint} longLat2 The coordinate of second point, in geographic unit.
		* @returns {number} The distance between to points, in meters.
		*/
		distance(lonLat1: IPoint, lonLat2: IPoint): number;
	}

	/** 
	* @ignore
	* The converter to convert the coordinates in geograhic unit (longitude and latitude), logic unit(percentage) and screen unit (pixel).
	*/
	export class CoordConverter implements ICoordConverter{
		private _projection: IMapProjection;

		/** @ignore */
		minLongitude: number;
		/** @ignore */
		maxLongitude: number;
		/** @ignore */
		minLatitude: number;
		/** @ignore */
		maxLatitude: number;

		private _size: Size;

		/**
		* @param {JQuery} element The jQuery object that represents the wijmaps element.
		* @param {Point} center The current center of the map, in geographic unit.
		* @param {number} zoom The current zoom of the map.
		* @param {number} tileWidth The width of the tile, in pixel.
		* @param {number} tileHeight The height of the tile, in pixel.
		*/
		constructor(public element: JQuery, public center: IPoint, public zoom: number, public tileWidth: number, public tileHeight: number) {
			var self = this, leftTopGeographicCoord: IPoint, rightBottomGeographicCoord: IPoint;
			self._updateSize();
			self._projection = new MercatorProjection();
			leftTopGeographicCoord = self.logicToGeographic(new Point(0, 0));
			rightBottomGeographicCoord = self.logicToGeographic(new Point(1, 1));
			self.minLongitude = leftTopGeographicCoord.x;
			self.maxLongitude = rightBottomGeographicCoord.x;
			self.minLatitude = rightBottomGeographicCoord.y;
			self.maxLatitude = leftTopGeographicCoord.y;
		}

		/** 
		* @ignore 
		* Update the cached size when the size of the map changed.
		*/
		_updateSize() {
			var self = this, e = self.element;
			self._size = new Size(e.width(), e.height());
		}

		/**
		* Convert the point coordinates from screen unit (pixel) to geographic unit (longitude and latitude).
		* @param {IPoint} position The point to convert, in screen unit.
		* @returns {IPoint} The new point after converted, in geographic unit.
		*/ 
		public viewToGeographic(position: IPoint): IPoint {
			var self = this;
			return self.logicToGeographic(self.viewToLogic(position));
		}

		/**
		* Convert the point coordinates from geographic unit (longitude and latitude) to screen unit (pixel).
		* @param {IPoint} position The point to convert, in geographic unit.
		* @returns {IPoint} The new point after converted, in screen unit.
		*/
		public geographicToView(position: IPoint): IPoint {
			var self = this;
			return self.logicToView(self.geographicToLogic(position));
		}

		/**
		* Convert the point coordinates from screen unit (pixel) to logic unit (percentage).
		* @param {IPoint} position The point to convert, in screen unit.
		* @returns {IPoint} The new point after converted, in logic unit.
		*/
		public viewToLogic(position: IPoint): IPoint {
			var self = this,
				viewCenter = self.getViewCenter(),
				logicCenter = self.getLogicCenter(),
				fullMapSize = self.getFullMapSize(),
				offset = new Size(position.x - viewCenter.x, position.y - viewCenter.y).scale(1 / fullMapSize.width, 1 / fullMapSize.height);
			return self._rangeLogicPoint(new Point(logicCenter).offset(offset.width, offset.height));
		}

		/**
		* Convert the point coordinates from logic unit (percentage) to screen unit (pixel).
		* @param {IPoint} position The point to convert, in loogic unit.
		* @returns {IPoint} The new point after converted, in screen unit.
		*/
		public logicToView(position: IPoint): IPoint {
			var self = this,
				logicPosition = self._rangeLogicPoint(position),
				viewCenter = self.getViewCenter(),
				logicCenter = self.getLogicCenter(),
				fullMapSize = self.getFullMapSize(),
				offset = new Size(logicPosition.x - logicCenter.x, logicPosition.y - logicCenter.y).scale(fullMapSize.width, fullMapSize.height);
			return new Point(viewCenter).offset(offset.width, offset.height);
		}

		/**
		* Convert the point coordinates from geographic unit (longitude and latitude) to logic unit (percentage).
		* @param {IPoint} position The point to convert, in geographic unit.
		* @returns {IPoint} The new point after converted, in logic unit.
		*/
		public geographicToLogic(lonLat: IPoint): IPoint {
			var self = this;
			return self._rangeLogicPoint(self._projection.project(lonLat));
		}

		/**
		* Convert the point coordinates from logic unit (percentage) to geographic unit (longitude and latitude).
		* @param {IPoint} position The point to convert, in logic unit.
		* @returns {IPoint} The new point after converted, in geographic unit.
		*/
		public logicToGeographic(position: IPoint): IPoint {
			var self = this, logicPosition = self._rangeLogicPoint(position);
			return self._projection.unproject(logicPosition);
		}

		/**
		* Gets the size of the map viewport.
		@returns {ISize} The size of the map viewport, in screen unit (pixel).
		*/
		public getViewSize(): ISize {
			return this._size;
		}
		
		/**
		* Gets the size of the map viewport.
		* returns {ISize} The size of the map viewport, in logic unit (percentage).
		*/
		public getLogicSize(): ISize {
			var self = this,
				viewSize = self.getViewSize(),
				fullMapSize = self.getFullMapSize();
			return new Size(viewSize).scale(1 / fullMapSize.width, 1 / fullMapSize.height);
		}

		/**
		* Gets the center of the map viewport.
		* returns {IPoint} The center of the map view port, in screen unit (pixel).
		*/
		public getViewCenter(): IPoint {
			var self = this, size = self._size;
			return new Point(size.width, size.height).scale(0.5, 0.5);
		}

		/**
		* Gets the center of the map viewport.
		* returns {IPoint} The center of the map view port, in logic unit (percentage).
		*/
		public getLogicCenter(): IPoint {
			var self = this;
			return self.geographicToLogic(self.center);
		}

		/** 
		* Gets the size of the full map in current zoom level.
		* @returns {ISize} The size of the full map, in screen unit (pixel).
		*/
		public getFullMapSize(): ISize {
			var self = this;
			return new Size(self.tileWidth, self.tileHeight).scale(Math.pow(2, self.zoom), Math.pow(2, self.zoom));
		}

		/**
		* Calculate the distance between two points.
		* @param {IPoint} lonLat1 The coordinate of first point, in geographic unit.
		* @param {IPoint} longLat2 The coordinate of second point, in geographic unit.
		* @returns {number} The distance between to points, in meters.
		*/
		public distance(lonLat1: IPoint, lonLat2: IPoint): number {
			// taken from http://www.movable-type.co.uk/scripts/latlong.html
			var r = 6371000.0, // m
				dLat = (lonLat2.y - lonLat1.y) * (Math.PI / 180),
				dLon = (lonLat2.x - lonLat1.x) * (Math.PI / 180),
				a = Math.sin(dLat * 0.5) * Math.sin(dLat * 0.5) +
					Math.cos(lonLat1.y * (Math.PI / 180)) * Math.cos(lonLat2.y * (Math.PI / 180)) *
					Math.sin(dLon * 0.5) * Math.sin(dLon * 0.5),
				c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			return r * c;
		}

		private _rangeLogicPoint(position: IPoint): IPoint{
			var min = 0, max = 0.9999999999;
			if (position.x < min) position.x = min;
			if (position.y < min) position.y = min;
			if (position.x > max) position.x = max;
			if (position.y > max) position.y = max;
			return position;
		}
	}
} 