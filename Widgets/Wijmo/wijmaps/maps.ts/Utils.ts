﻿/// <reference path="drawing.ts" />
/// <reference path="../../External/declarations/geojson.d.ts"/>

module wijmo.maps {
	  
	
	/** @ignore */
	export class Utils{
		public static getBounds(pts: IPoint[]): Rectangle {
			if (!pts || pts.length === 0) return null;

			var minx = Number.MAX_VALUE,
				miny = minx,
				maxx = -Number.MAX_VALUE,
				maxy = maxx;

			$.each(pts, (index, pt)=> {
				if (pt.x < minx)
					minx = pt.x;
				if (pt.x > maxx)
					maxx = pt.x;
				if (pt.y < miny)
					miny = pt.y;
				if (pt.y > maxy)
					maxy = pt.y;
			});

			return new Rectangle(new Point(minx, miny), new Size(maxx - minx, maxy - miny));
		}

		public static getListBounds(ptsList: IPoint[][]): Rectangle {
			var listBounds: Rectangle, bounds: Rectangle;

			$.each(ptsList, function (index, pts) {
				bounds = Utils.getBounds(pts);
				if (bounds) {
					listBounds = bounds.union(listBounds);
				}
			});

			return listBounds;
		}
	
		public static getPoints(coordinates: number[]): Point[] {
			if (!coordinates) return null;

			var points: Point[] = [],
				len = coordinates.length;

			len = len - len % 2;
			for (var i = 0; i < len; i++) {
				var pt = new Point(coordinates[i], coordinates[++i]);
				points.push(pt);
			}

			return points;
		}

		public static sinh(value: number): number {
			return (Math.exp(value) - Math.exp(-value)) / 2;
		}

		public static createListPath(pointsList: IPoint[][], closed?: boolean): string {
			var paths = "", path: string;
			$.each(pointsList, function (index, points: IPoint[]) {
				path = Utils.createPath(points);
				if (path) {
					paths += path;
				}
			});

			return paths;
		}

		public static createPath(points: IPoint[], closed?: boolean): string {
			if (!points)
				return null;

			var len = points.length,
				point: IPoint,
				path: string = null,
				start: IPoint;

			if (len > 0) {
				start = points[0];
				path = "M" + start.x + " " + start.y;

				for (var i = 1; i < len; i++) {
					point = points[i];
					path += "L" + point.x + " " + point.y;
				}

				if (closed) {
					path += "Z";
				}
			}

			return path;
		}

		static getPointData(coords: GeoJSON.Position): Point {
			var point: Point = null;
			if (coords && coords.length > 0) {
				point = new Point(coords[0], coords[1]);
			}

			return point;
		}

		static getLineData(coords: GeoJSON.Position[]): Point[]{
			var points: Point[] = null;
			if (coords && coords.length > 0) {
				points = [];
				$.each(coords, function (index, position) {
					points.push(Utils.getPointData(position));
				});
			}

			return points;
		}

		static getPolygonData(coords: GeoJSON.Position[][]): Point[][] {
			var pointsList: Point[][] = null;
			if (coords && coords.length > 0) {
				pointsList = [];
				$.each(coords, function (index, positions) {
					pointsList.push(Utils.getLineData(positions));
				});
			}

			return pointsList;
		}
	}
}