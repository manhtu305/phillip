﻿/// <reference path="Drawing.ts"/>
/// <reference path="Utils.ts"/>
/// <reference path="ClippingEngine.ts"/>
/// <reference path="wijlayer.ts" />
/// <reference path="../../Base/jquery.wijmo.widget.ts" />
/// <reference path="../../wijutil/jquery.wijmo.raphael.ts"/> 
/// <reference path="../../External/declarations/geojson.d.ts"/>

module wijmo.maps {
	  

	var $ = jQuery,
		vectorCanvasCss: string = "wijmo-wijmaps-vectorlayer-canvas",
		layerContainerCss: string = "wijmo-wijmaps-layercontainer",
		vectorShapeCss: string = "wijmo-wijmaps-vectorlayer-shape",
		vectorMarkCss: string = "wijmo-wijmaps-vectorlayer-mark",
		vectorMarkLabelCss: string = "wijmo-wijmaps-vectorlayer-marklabel",
		vectorData: string = "wijmoWijvectorlayerData";

	/** @ignore */
	enum VectorType {
		placemark,
		polyline,
		polygon
	}

	/** @ignore */
	enum LabelVisibility {
		hidden,
		autoHide,
		visible
	}

	/** @ignore */
	enum LabelPosition {
		center,
		left,
		right,
		top,
		bottom
	}

	/** @ignore */
	export class LOD implements ILOD {
		minSize: number = 0;
		maxSize: number = 0;
		minZoom: number = 0;
		maxZoom: number = 0;

		isDefault(): boolean {
			return this.minSize === 0 && this.maxSize === 0 && this.minZoom === 0 && this.maxZoom === 0;
		}

		constructor(data?: ILOD) {
			if (!data) {
				return;
			}

			var self = this;
			self.minSize = data.minSize || 0;
			self.maxSize = data.maxSize || 0;
			self.minZoom = data.minZoom || 0;
			self.maxZoom = data.maxZoom || 0;
		}
	}

	/**
	* The ILOD (Level Of Detail) interface associates element visibility with the map scale (zoom and size).
	**/
	export interface ILOD {
		/**
		* When the pixel size of element is less than minSize the element is not visible.
		**/
		minSize?: number;

		/**
		* When the pixel size of element is greater than maxSize the element is not visible.
		**/
		maxSize?: number;

		/**
		* When the map zoom is less than minZoom the element is not visible.
		**/
		minZoom?: number;

		/**
		* When the map zoom is greater than maxZoom the element is not visible.
		**/
		maxZoom?: number;
	}

	/** @ignore */
	class MapState {
		zoom: number;
		center: IPoint;
	}

	/** @ignore */
	class ProjectionFast implements IMapProjection {
		static toRad: number = Math.PI / 180;
		static toDeg: number = 180 / Math.PI;

		_converter: ICoordConverter;

		_scaleX: number;
		_scaleY: number;
		_offsetX: number;
		_offsetY: number;

		_ax: number;
		_ay: number;
		_bx: number;
		_by: number;
		_extraX: number;
		_extraY: number;

		constructor(scaleX: number, scaleY: number, offsetX: number, offsetY: number, extraX: number, extraY: number) {
			var self = this;
			self._scaleX = scaleX;
			self._scaleY = scaleY;
			self._offsetX = offsetX;
			self._offsetY = offsetY;
			self._extraX = extraX;
			self._extraY = extraY;
		}

		public init(o: wijlayer_options): void {
			var self = this,
				converter = o.converter,
				viewSize = converter.getViewSize(),
				logicSize = converter.getLogicSize(),
				geographicCenter = o.center,

				scale = viewSize.width / logicSize.width,
				logicCenter = converter.geographicToLogic(geographicCenter),
				offsetX = 0.5 * viewSize.width - logicCenter.x * scale,
				offsetY = 0.5 * viewSize.height - logicCenter.y * scale;

			self._ax = ProjectionFast.toRad * self._scaleX * scale;
			self._bx = self._offsetX * scale + offsetX + self._extraX;

			self._ay = 0.5 * self._scaleY * scale;
			self._by = self._offsetY * scale + offsetY + self._extraY;

			self._converter = converter;
		}

		public project(longLat: IPoint): IPoint {
			var self = this, converter = self._converter;

			if (converter) {
				if (longLat.y > converter.maxLatitude) longLat.y = converter.maxLatitude;
				if (longLat.y < converter.minLatitude) longLat.y = converter.minLatitude;
			}

			var sinLat = Math.sin(longLat.y * ProjectionFast.toRad),
				x = (longLat.x * self._ax + self._bx),
				y = (Math.log((1.0 + sinLat) / (1.0 - sinLat)) * self._ay + self._by);

			return new Point(x, y);
		}

		public unproject(point: Point): Point {
			var self = this,
				x = ((point.x - self._offsetX) * ProjectionFast.toDeg / self._scaleX),
				y = (Math.atan(Utils.sinh((point.y - self._offsetY) / self._scaleY)) * ProjectionFast.toDeg);

			return new Point(x, y);
		}
	}

	/** 
	* @widget
	* The vector layer for the wijmaps. Draw the shapes and placemarks.
	*/
	export class wijvectorlayer extends wijlayer {
		/**
		* @ignore
		* the horizontal offset of the vector layer container.
		*/
		static _offsetX: number = -2000;
		/**
		* @ignore
		* the vertical offset of the vector layer container.
		*/
		static _offsetY: number = -2000;
		/**
		* @ignore
		* the clip of the vector layer container.
		*/
		static _clipRect: Rectangle = new Rectangle(
			new Point(-wijvectorlayer._offsetX - 1000, -wijvectorlayer._offsetY - 1000),
			new Size(3000, 3000));
		
		private _proj: ProjectionFast;
		private _cache: MapState;
		private _data: any;
		private _needFullUpdateShape: boolean;
		private _needDrawMarkShape: boolean;
		private _marks: IMarkCache[];
		private _container: JQuery;
		private _shapeContainer: JQuery;
		private _placemarkContainer: JQuery;
		private _paper: RaphaelPaper;
		private _group: IRaphaelGroup;
		private _vectorObj: IVectorObject;

		/** 
		* The options for the wijvectorlayer widget.
		*/
		public options: wijvectorlayer_options;

		/** @ignore */
		_create() {
			var self = this,
				selector = "." + vectorShapeCss;

			self._proj = new ProjectionFast(1 / (Math.PI * 2), -1 / (Math.PI * 2), 0.5, 0.5, -wijvectorlayer._offsetX, -wijvectorlayer._offsetY);
			self._cache = null;
			self._needFullUpdateShape = false;
			self._needDrawMarkShape = false;

			self._container = $("<div class=\"" + vectorCanvasCss + "\"></div>")
				.appendTo(self.element)
				.on("click." + self.widgetName, selector, function (e) {
					self._onItemMouseEvent("click", e, this);
				})
				.on("mousedown." + self.widgetName, selector, function (e) {
					self._onItemMouseEvent("mouseDown", e, this);
				})
				.on("mouseup." + self.widgetName, selector, function (e) {
					self._onItemMouseEvent("mouseUp", e, this);
				})
				.on("mousemove." + self.widgetName, selector, function (e) {
					self._onItemMouseEvent("mouseMove", e, this);
				})
				.on("mouseenter." + self.widgetName, selector, function (e) {
					self._onItemMouseEvent("mouseEnter", e, this);
				})
				.on("mouseleave." + self.widgetName, selector, function (e) {
					self._onItemMouseEvent("mouseLeave", e, this);
				});

			super._create();

			self._setData(self.options.data);
		}

		/** @ignore */
		_setOption(key: string, value: any) {
			var self = this, o = this.options;
			if (o[key] !== value) {
				switch (key) {
					case 'dataType':
						self._setDataType(value);
						return;
					case 'data':
						self._setData(value);
						return;
				}
			}
			super._setOption(key, value);
		}

		private _setDataType(dataType: string) {
			var self = this;
			self.options.dataType = dataType;
		}

		private _setData(data: any) {
			var self = this, o = self.options;
			o.data = data;

			if (typeof (data) === "string") {
				self._prepareDataFromUri(data);
			}
			else if ($.isFunction(data)) {
				self._applyData(data());
			}
			else {
				self._applyData(data);
			}
		}

		private _prepareDataFromUri(dataUri: string) {
			var self = this;
			$.ajax({
				url: dataUri,
				data: null,
				context: null,
				success: (result) => {
					self._uriLoaded(result);
				},
				error: null,
				dataType: "json"
			});
		}

		private _uriLoaded(result) {
			var self = this;
			self._applyData(result);
		}

		private _applyData(data) {
			var self = this;

			if (self._data === data) return;
			self._data = data;

			self._applyDataInternal(data);
		}

		private _applyDataInternal(data) {
			var self = this, o = self.options;
			if (o.dataType === DataJsonType.wijJson) {
				self._applyWijJsonData(data);
			}
			else if (o.dataType === DataJsonType.geoJson) {
				self._applyGeoJsonData(data);
			}

			self._needFullUpdateShape = true;
			self.update();
		}

		private _applyWijJsonData(data: IWijJsonData) {
			this._createVectors(data);
		}

		private _applyGeoJsonData(data: GeoJSON.GeoJsonObject) {
			var self = this, obj = GeoObject.create(data);
			if (obj) {
				obj.applyData(data, {});
			}
			self._vectorObj = obj;
		}

		private _createVectors(data: IWijJsonData) {
			var self = this,
				list = new VectorItemCollection();
			if (data && data.vectors) {
				$.each(data.vectors, (index, vector: IVector) => {
					var item = this._createVector(vector);
					if (item) {
						var wrapper = new VectorItemWrapper(item);
						wrapper.applyData(vector);
						list.addChild(wrapper);
					}
				});
			}

			self._vectorObj = list;
		}

		private _createVector(vectorData: IVector): VectorItem {
			switch (VectorType[vectorData.type]) {
				case VectorType.polygon:
					return new VectorPolygon();
				case VectorType.placemark:
					return new VectorPlacemark();
				case VectorType.polyline:
					return new VectorPolyline();
			}

			return null;
		}

		/** 
		* @ignore 
		* override
		*/
		_render() {
			var self = this;
			super._render();
			self._updateShape();
		}

		private _createContainer() {
			var self = this, clip = wijvectorlayer._clipRect;

			self._destroyContainer();

			self._shapeContainer = $("<div class=\"" + layerContainerCss + "\"></div>");
			self._paper = self._createPaper(self._shapeContainer[0], new Size(clip.getRight(), clip.getBottom()));
			self._group = self._paper.group();
			self._placemarkContainer = $("<div class=\"" + layerContainerCss + "\" style='visibility:hidden;'></div>");
		}

		private _destroyContainer() {
			var self = this;
			if (self._paper) self._paper.remove();
			if (self._shapeContainer) {
				self._shapeContainer.off("." + self.widgetName).remove();
			}
			if (self._placemarkContainer) {
				self._placemarkContainer.off("." + self.widgetName).remove();
			}
		}

		private _refreshPaper() {
			var self = this;
			self._container.css("left", wijvectorlayer._offsetX + "px").css("top", wijvectorlayer._offsetY + "px");
			self._createContainer();

			self._marks = [];
			if (self._vectorObj) {
				self._drawVectorObj(self._vectorObj);
			}

			self._container.append(self._shapeContainer);
			self._container.append(self._placemarkContainer);
			self._arrangeMarks();
			self._arrangeLabels();
			self._placemarkContainer.css("visibility", "visible");
		}

		private _drawVectorObj(vector: IVectorObject) {
			var self = this;

			self._drawSingleVectorObj(vector);

			if (vector && vector.children && vector.children.length > 0) {
				$.each(vector.children, function (index, child) {
					self._drawVectorObj(child);
				});
			}
		}

		private _drawSingleVectorObj(vector: IVectorObject) {
			var self = this, shape: Shape, els: RaphaelSet, el: RaphaelSet,
				data : IVectorEventData = { vector: vector, shape: null };
			if (vector.vectorItems) {
				$.each(vector.vectorItems, function (index, item) {
					if (item.visible) {
						shape = item.getShape();
						if (shape) {
							el = shape.createElement(self._paper);
							if (el) {
								self._group.push(el);
								if (els) {
									el.forEach(function (e) {
										els.push(e);
									});
								} else {
									els = el;
								}
							}
						}

						if (item instanceof VectorPlacemark) {
							self._drawPlacemark(<VectorPlacemark>item, data);
						}
					}
				});
				
				if (els) {
					data.shape = els;
					$.each(els, (index, element: RaphaelElement) => {
						$.wijraphael.addClass($(element.node), vectorShapeCss);
						$(element.node).data(vectorData, data);
					});
					self._onShapeCreated("shapeCreated", data);
				}
			}
		}

		private _drawPlacemark(placemark: VectorPlacemark, data) {
			var self = this, cache : IMarkCache = { location: placemark.point };
			if (!self._needDrawMarkShape) {
				self._drawMark(placemark, data, cache);
			}
			self._drawLabel(placemark, cache);

			if (cache.mark || cache.label) {
				self._marks.push(cache);
			}
		}

		private _drawMark(placemark: VectorPlacemark, data, cache : IMarkCache) {
			var self = this, mo = self.options.placemark,
				point: Point, mark: JQuery, size: Size, pin: Point, top, left;

			if (mo === null) return;

			if (mo.render) {
				mark = mo.render(data);
			}
			else if (mo.image) {
				mark = $("<img src='" + mo.image + "' />");
			}

			if (!mark || !mark.length) return;

			cache.mark = mark;
			mark.addClass(vectorMarkCss)
				.data(vectorData, data)
				.appendTo(self._placemarkContainer);
		}

		private _arrangeMarks() {
			var self = this, o = self.options, mo = o.placemark || {},
				location: IPoint, mark: JQuery, size: ISize, pin: IPoint, top, left;

			if (self._marks) {
				$.each(self._marks, (index, cache) => {
					if (cache.mark) {
						mark = cache.mark;
						location = o.converter.geographicToView(cache.location);
						size = cache.markSize || mo.size || new Size(mark.width(), mark.height());
						pin = mo.pinPoint || new Point(size.width / 2, size.height / 2);

						location.x -= wijvectorlayer._offsetX;
						location.y -= wijvectorlayer._offsetY;

						left = location.x - pin.x;
						top = location.y - pin.y;

						cache.markSize = size;
						mark.css("position", "absolute").css("left", left + "px").css("top", top + "px");
					}
				});
			}
		}

		private _drawLabel(placemark: VectorPlacemark, cache : IMarkCache) {
			var self = this, mo = self.options.placemark || {},
				visibility: LabelVisibility = LabelVisibility[mo.labelVisibility] || LabelVisibility.hidden,
				label = placemark.label || placemark.name;

			if (visibility === LabelVisibility.hidden || !label) {
				return;
			}

			var point = placemark.getViewBounds().location,
				text = $("<span style='position:absolute;' class='" + vectorMarkLabelCss + "'>" + label + "</span>").appendTo(self._placemarkContainer);

			cache.label = text;
		}

		private _arrangeLabels(visibility?: LabelVisibility) {
			var self = this, o = self.options, mo = o.placemark || {},
				labelBounds: Rectangle[] = [];
			visibility = visibility || LabelVisibility[mo.labelVisibility] || LabelVisibility.hidden;

			$.each(self._marks, (index, cache) => {
				if (cache.label) {
					var location: IPoint, rect: Rectangle,
						overlap = false,
						len = labelBounds.length, i;

					location = o.converter.geographicToView(cache.location);
					location.x -= wijvectorlayer._offsetX;
					location.y -= wijvectorlayer._offsetY;
					rect = self._calculateLabelBounds(location, cache.label, cache.labelSize);

					if (visibility === LabelVisibility.autoHide) {
						for (i = 0; i < len; i++) {
							if (rect.intersectsWith(labelBounds[i])) {
								overlap = true;
								break;
							}
						}
					}

					if (overlap) {
						cache.label.remove();
					}
					else {
						cache.label.css("left", rect.location.x + "px").css("top", rect.location.y + "px");
						cache.labelSize = rect.size;
						if (visibility === LabelVisibility.autoHide) {
							labelBounds.push(rect);
						}
					}
				}
			});
		}

		private _calculateLabelBounds(point: IPoint, text: JQuery, size? : ISize): Rectangle {
			var self = this, mo = self.options.placemark || {},
				position: LabelPosition = LabelPosition[mo.labelPosition] || LabelPosition.center,
				width: number = size ? size.width : text.outerWidth(),
				height: number = size ? size.height : text.outerHeight(),
				left: number, top: number;

			if (position === LabelPosition.left) {
				left = point.x - width;
				top = point.y - height / 2;
			}
			else if (position === LabelPosition.right) {
				left = point.x;
				top = point.y - height / 2;
			}
			else if (position === LabelPosition.top) {
				left = point.x - width / 2;
				top = point.y - height;
			}
			else if (position === LabelPosition.bottom) {
				left = point.x - width / 2;
				top = point.y;
			}
			else { // center
				left = point.x - width / 2;
				top = point.y - height / 2;
			}

			return new Rectangle(new Point(left, top), new Size(width, height));
		}

		private _translatePaper(dx:number, dy:number) {
			var self = this,
				left = wijvectorlayer._offsetX + dx,
				top = wijvectorlayer._offsetY + dy;
			self._container.css("left", left + "px").css("top", top + "px");
		}

		private _scalePaper(sx: number, sy: number, dx: number, dy: number) {
			var self = this;
			if (Raphael.vml) {
				self._container.empty();
				return;
			}
			
			var viewSize = self.options.converter.getViewSize(),
				left = wijvectorlayer._offsetX,
				top = wijvectorlayer._offsetY,
				cx = viewSize.width / 2 - wijvectorlayer._offsetX,
				cy = viewSize.height / 2 - wijvectorlayer._offsetY,
				matrix = Raphael.matrix(1, 0, 0, 1, 0, 0);

			self._container.css("left", left + "px").css("top", top + "px");
			self._arrangeMarks();
			self._arrangeLabels(LabelVisibility.visible);

			matrix.translate(dx, dy);
			matrix.scale(sx, sy, cx, cy);
			self._group.transform(matrix);
		}

		private _updateShape() {
			var self = this;
			if (!self._cache || self._needFullUpdateShape) {
				self._fullUpdateShape();
				return;
			}

			var o = self.options,
				converter = o.converter,
				pt1 = converter.geographicToView(o.center),
				pt2 = converter.geographicToView(self._cache.center),
				dx = pt2.x - pt1.x,
				dy = pt2.y - pt1.y;

			if (o.zoom === o.targetZoom) { // || !o.isZooming) {
				var clip = wijvectorlayer._clipRect;
				if (o.zoom !== self._cache.zoom || self._needFullUpdateShape ||
					Math.abs(dx) > 0.4 * clip.size.width || Math.abs(dy) > 0.4 * clip.size.height) {
					self._fullUpdateShape();
					return;
				}
				else {
					self._translatePaper(dx, dy);
				}
			}
			else {
				var scale = Math.pow(2, o.zoom) / Math.pow(2, self._cache.zoom);
				self._scalePaper(scale, scale, dx, dy);
			}

			return;
		}

		private _updateVectorObj(vector: IVectorObject, pc, gclip, minsz2, zoom) {
			var self = this;

			self._updateSingleVectorObj(vector, pc, gclip, minsz2, zoom);

			if (vector && vector.children && vector.children.length > 0) {
				$.each(vector.children, function (index, child) {
					self._updateVectorObj(child, pc, gclip, minsz2, zoom);
				});
			}
		}

		private _updateSingleVectorObj(vector: IVectorObject, pc, gclip, minsz2, zoom) {
			var self = this;
			if (vector.vectorItems) {
				$.each(vector.vectorItems, function (index, item) {
					self._updateVectorItem(item, pc, gclip, minsz2, zoom);
				});
			}
		}

		private _updateVectorItem(item: VectorItem, pc, gclip, minsz2, zoom) {
			item.visible = false;
			if (!gclip.intersectsWith(item.getBounds()))
				return;
			item.getViewBounds(pc);
			if (item.isVisible(minsz2, zoom)) {
				item.visible = true;
				item.updateShape(pc);
			}
		}

		private _fullUpdateShape(): void {
			var self = this,
				o = self.options,
				mo = o.placemark || {},
				minSize = (o.minSize == null) ? 3 : o.minSize,
				converter = o.converter,
				minsz2 = minSize * minSize;

			self._needDrawMarkShape = !(mo.image || mo.render);
			self._needFullUpdateShape = false;

			if (!self._vectorObj) return;

			self._cache = { zoom: o.zoom, center: o.center };

			var pc: PointConverter = (p) => { return self._proj.project(p); };

			self._proj.init(o);

			var clip = wijvectorlayer._clipRect,
				left = clip.location.x,
				top = clip.location.y,
				right = left + clip.size.width,
				bottom = top + clip.size.height,
				pt1 = converter.viewToGeographic(new Point(left + wijvectorlayer._offsetX, top + wijvectorlayer._offsetY)),
				pt2 = converter.viewToGeographic(new Point(right + wijvectorlayer._offsetX, bottom + wijvectorlayer._offsetY)),
				gclip = Utils.getBounds([pt1, pt2]);

			self._updateVectorObj(self._vectorObj, pc, gclip, minsz2, o.zoom);

			self._refreshPaper();
		}

		private _onShapeCreated(eventName, data: IVectorEventData) {
			this._trigger(eventName, null, data);
		}

		private _onItemMouseEvent(eventName: string, e, target) {
			var self = this;
			if (self._isDisabled()) return;

			self._trigger(eventName, e, $(target).data(vectorData));
		}

		/**
		* Force to request the data to get the newest one.
		*/
		public refresh() {
			var self = this;
			self._data = null;
			this._setData(self.options.data);
		}

		/**
		* Removes the wijvectorlayer functionality completely. This will return the element back to its pre-init state.
		*/
		public destroy() {
			var self = this;
			if (self._paper) self._paper.remove();
			self._container.off("." + self.widgetName).remove();

			super.destroy();
		}
	}

	/** @ignore */
	enum GeoObjectType {
		Point,
		MultiPoint,
		LineString,
		MultiLineString,
		Polygon,
		MultiPolygon,
		GeometryCollection,
		Feature,
		FeatureCollection
	}

	/**
	* The vector object construct from the incoming data.
	*/
	export interface IVectorObject {
		/**
		* The type of the vector object. <p/>
		* The possible values for wijJson data are:
		* <ul>
		* <li>VectorCollection: The collection of vector items.</li>
		* <li>Vector: The individual vector item.</li>
		* </ul>
		* <p/>
		* The possible values for geoJson data are:
		* <ul>
		* <li>Object: The vector for GeoJSON object.</li>
		* <li>Geometry: The vector for GeoJSON Geometry object. The base of Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon and GeometryCollection.</li>
		* <li>Point: The vector for GeoJSON Point object.</li>
		* <li>MultiPoint: The vector for GeoJSON MultiPoint object.</li>
		* <li>LineString: The vector for GeoJSON LineString object.</li>
		* <li>MultiLineString: The vector for GeoJSON MultiLineString object.</li>
		* <li>Polygon: The vector for GeoJSON Polygon object.</li>
		* <li>MultiPolygon: The vector for GeoJSON MultiPolygon object.</li>
		* <li>GeometryCollection: The vector for GeoJSON GeometryCollection object.</li>
		* <li>Feature: The vector for GeoJSON Feature object.</li>
		* <li>FeaturesCollectiton: The vector for GeoJSON FeaturesCollection object.</li>
		* </ul>
		*/
		type: string;

		/**
		* The original data from the wijJson or geoJson.
		*/
		data;

		/**
		* The parent vector of this vector.
		*/
		parent: IVectorObject;

		/** @ignore */
		applyData: (data, properties?) => void;
		/** @ignore */
		vectorItems: VectorItem[];
		/** @ignore */
		children: IVectorObject[];
	}

	/** @ignore */
	class VectorItemCollection implements IVectorObject {
		type = "VectorCollection";
		data;
		parent: IVectorObject = null;
		children : IVectorObject[] = [];
		vectorItems : VectorItem[] = null;

		applyData(data) {
			
		}

		addChild(child: IVectorObject) {
			this.children.push(child);
			child.parent = this;
	}
	}

	/** @ignore */
	class VectorItemWrapper<T extends VectorItem> implements IVectorObject {
		type = "Vector";
		data: IVector;
		parent: IVectorObject;
		children: IVectorObject[];
		vectorItems: VectorItem[];

		constructor(public item: T) {
			this.vectorItems = [item];
		}

		applyData(data) {
			this.data = data;
			this.item.applyData(data);
		}
	}

	/** @ignore */
	class GeoObject implements IVectorObject {
		type: string = "Object";
		data;
		vectorItems: VectorItem[];
		parent: GeoObject;
		children: GeoObject[];
		properties;

		applyData(data : GeoJSON.GeoJsonObject, properties) {
			this.data = data;
			this.properties = properties;
		}

		_addChild(child: GeoObject) {
			var self = this;
			self.children = self.children || [];
			self.children.push(child);
			child.parent = self;
		}

		public static create(data: GeoJSON.GeoJsonObject): GeoObject {
			switch (data.type) {
				case GeoObjectType[GeoObjectType.Point]:
					return new GeoPoint();
				case GeoObjectType[GeoObjectType.MultiPoint]:
					return new GeoMultiPoint();
				case GeoObjectType[GeoObjectType.LineString]:
					return new GeoLineString();
				case GeoObjectType[GeoObjectType.MultiLineString]:
					return new GeoMultiLineString();
				case GeoObjectType[GeoObjectType.Polygon]:
					return new GeoPolygon();
				case GeoObjectType[GeoObjectType.MultiPolygon]:
					return new GeoMultiPolygon();
				case GeoObjectType[GeoObjectType.GeometryCollection]:
					return new GeoGeometryCollection();
				case GeoObjectType[GeoObjectType.Feature]:
					return new GeoFeature();
				case GeoObjectType[GeoObjectType.FeatureCollection]:
					return new GeoFeatureCollection();
			}
			return null;
		}
	}

	/** @ignore */
	class GeoGeometryObject extends GeoObject {
		type = "Geometry";

		_createVectorItem(type : VectorType): VectorItem {
			switch (type) {
				case VectorType.polygon:
					return new VectorPolygon();
				case VectorType.placemark:
					return new VectorPlacemark();
				case VectorType.polyline:
					return new VectorPolyline();
			}

			return null;
		}
	}

	/** @ignore */
	class GeoPoint extends GeoGeometryObject {
		type = "Point";
		data: GeoJSON.Point;

		public applyData(data: GeoJSON.Point, properties) {
			var self = this, vector: VectorItem;
			super.applyData(data, properties);
			self.data = data;
			if (data.coordinates) {
				vector = self._createVectorItem(VectorType.placemark);
				vector.applyGeoData(data.coordinates, properties);
				self.vectorItems = [vector];
			}
		}
	}

	/** @ignore */
	class GeoMultiPoint extends GeoGeometryObject {
		type = "MultiPoint";
		data: GeoJSON.MultiPoint;

		public applyData(data: GeoJSON.MultiPoint, properties) {
			var self = this, vector: VectorItem;
			super.applyData(data, properties);
			self.data = data;
			if (data.coordinates) {
				self.vectorItems = [];
				$.each(data.coordinates, function (index, single) {
					vector = self._createVectorItem(VectorType.placemark);
					vector.applyGeoData(single, properties);
					self.vectorItems.push(vector);
				});
			}
		}
	}

	/** @ignore */
	class GeoLineString extends GeoGeometryObject {
		type = "LineString";
		data: GeoJSON.LineString;

		public applyData(data: GeoJSON.LineString, properties) {
			var self = this, vector: VectorItem;
			super.applyData(data, properties);
			self.data = data;
			if (data.coordinates) {
				vector = self._createVectorItem(VectorType.polyline);
				vector.applyGeoData(data.coordinates, properties);
				self.vectorItems = [vector];
			}
		}
	}

	/** @ignore */
	class GeoMultiLineString extends GeoGeometryObject {
		type = "MultiLineString";
		data: GeoJSON.MultiLineString;

		public applyData(data: GeoJSON.MultiLineString, properties) {
			var self = this, vector: VectorItem;
			super.applyData(data, properties);
			self.data = data;
			if (data.coordinates) {
				self.vectorItems = [];
				$.each(data.coordinates, function (index, single) {
					vector = self._createVectorItem(VectorType.polyline);
					vector.applyGeoData(single, properties);
					self.vectorItems.push(vector);
				});
			}
		}
	}

	/** @ignore */
	class GeoPolygon extends GeoGeometryObject {
		type = "Polygon";
		data: GeoJSON.Polygon;

		public applyData(data: GeoJSON.Polygon, properties) {
			var self = this, vector: VectorItem;
			super.applyData(data, properties);
			self.data = data;
			if (data.coordinates) {
				vector = self._createVectorItem(VectorType.polygon);
				vector.applyGeoData(data.coordinates, properties);
				self.vectorItems = [vector];
			}
		}
	}

	/** @ignore */
	class GeoMultiPolygon extends GeoGeometryObject {
		type = "MultiPolygon";
		data: GeoJSON.MultiPolygon;

		public applyData(data: GeoJSON.MultiPolygon, properties) {
			var self = this, vector: VectorItem;
			super.applyData(data, properties);
			self.data = data;
			if (data.coordinates) {
				self.vectorItems = [];
				$.each(data.coordinates, function (index, single) {
					vector = self._createVectorItem(VectorType.polygon);
					vector.applyGeoData(single, properties);
					self.vectorItems.push(vector);
				});
			}
		}
	}

	/** @ignore */
	class GeoGeometryCollection extends GeoGeometryObject {
		type = "GeometryCollection";
		data: GeoJSON.GeometryCollection;
		geometries: GeoObject[];

		public applyData(data: GeoJSON.GeometryCollection, properties) {
			var self = this, child : GeoObject;
			super.applyData(data, properties);
			if (data.geometries) {
				$.each(data.geometries, function (index, geometry) {
					child = GeoObject.create(geometry);
					child.applyData(geometry, properties);
					self._addChild(child);
				});
			}
			self.geometries = self.children;
		}
	}

	/** @ignore */
	class GeoFeature extends GeoObject {
		type = "Feature";
		data: GeoJSON.Feature;
		geometry: GeoObject;

		public applyData(data: GeoJSON.Feature){
			var self = this, geometry: GeoObject;
			super.applyData(data, data.properties);
			if (data.geometry) {
				geometry = GeoObject.create(data.geometry);
				geometry.applyData(data.geometry, data.properties);
				self._addChild(geometry);
				self.geometry = geometry;
			}
		}
	}

	/** @ignore */
	class GeoFeatureCollection extends GeoObject {
		type = "FeaturesCollectiton";
		data: GeoJSON.FeatureCollection;
		features: GeoObject[];

		public applyData(data: GeoJSON.FeatureCollection) {
			var self = this, child: GeoObject;
			super.applyData(data, null);
			if (data.features) {
				$.each(data.features, function (index, feature) {
					child = GeoObject.create(feature);
					child.applyData(feature, null);
					self._addChild(child);
				});
			}
			self.features = self.children;
		}
	}

	/** @ignore */
	export class VectorItem {
		/** @ignore */
		_lod: LOD = new LOD();

		name: string;
		fill: string;
		fillOpacity: number;
		stroke: string;
		strokeOpacity: number;
		strokeWidth: number;
		strokeDashArray: string;
		strokeLineCap: PenLineCap;
		strokeLineJoin: PenLineJoin;
		strokeMiterLimit: number;
		visible: boolean = true;

		//virtual
		public applyData(data: IVector): void {
			this._applyProperties(data);
		}

		public applyGeoData(coords, properties): void {
			this._applyProperties(properties);
		}

		_applyProperties(properties) {
			var self = this;
			if (properties) {
				self.name = properties["name"];
				self.fill = properties["fill"];
				self.fillOpacity = properties["fillOpacity"];
				self.stroke = properties["stroke"];
				self.strokeOpacity = properties["strokeOpacity"];
				self.strokeWidth = properties["strokeWidth"];
				self.strokeDashArray = properties["strokeDashArray"];
				self.strokeLineCap = PenLineCap[<string>properties["strokeLineCap"]];
				self.strokeLineJoin = PenLineJoin[<string>properties["strokeLineJoin"]];
				self.strokeMiterLimit = properties["strokeMiterLimit"];
				self._lod = new LOD(properties.lod);
			}
		}

		//virtual
		public getBounds(): Rectangle {
			return null;
		}

		//virtual
		public getViewBounds(pc?: PointConverter): Rectangle {
			return null;
		}

		public isVisible(minsz2: number, zoom: number): boolean {
			var s2 = 0,
				bnds = this.getViewBounds();

			if (!bnds.isEmpty()) {
				s2 = bnds.size.width * bnds.size.width + bnds.size.height * bnds.size.height;
			}

			var lod = this._lod;
			if (lod && lod.isDefault()) {
				return s2 >= minsz2;
			}
			else {
				var visible = (zoom >= lod.minZoom) && (zoom <= lod.maxZoom);
				if (visible) {
					if (lod.maxSize === 0)
						return s2 >= lod.minSize * lod.minSize;
					else if (this._lod.minSize === 0)
						return s2 <= lod.maxSize * lod.maxSize;
					else
						return s2 >= lod.minSize * lod.minSize && s2 <= lod.maxSize * lod.maxSize;
				}
				else {
					return false;
				}
			}
		}

		//virtual
		public getShape(): Shape {
			return null;
		}

		_initShape(shape: Shape): void {
			var self = this;
			if (self.fill != null) shape.fill = self.fill;
			if (self.fillOpacity != null) shape.fillOpacity = self.fillOpacity;
			if (self.stroke != null) shape.stroke = self.stroke;
			if (self.strokeOpacity != null) shape.strokeOpacity = self.strokeOpacity;
			if (self.strokeWidth != null) shape.strokeWidth = self.strokeWidth;
			if (self.strokeDashArray) shape.strokeDashArray = self.strokeDashArray;
			if (self.strokeLineCap != null) shape.strokeLineCap = self.strokeLineCap;
			if (self.strokeLineJoin) shape.strokeLineJoin = self.strokeLineJoin;
			if (self.strokeMiterLimit) shape.strokeMiterLimit = self.strokeMiterLimit;
		}

		//virtual
		public updateShape(pc: PointConverter) {
		}

		static optRad: number = 1.5;
		static convertPoints(pts: IPoint[], pc: PointConverter) {
			var filterPts: IPoint[] = null;
			if (pts!=null) {
				var len = pts.length,
					prev = pc(pts[0]);

				filterPts = [];
				filterPts.push(prev);
				for (var i = 1; i < len; i++) {
					var pt = pc(pts[i]);
					if (Math.abs(pt.x - prev.x) >= VectorItem.optRad || Math.abs(pt.y - prev.y) >= VectorItem.optRad) {
						filterPts.push(pt);
						prev = pt;
					}
				}
			}

			return filterPts;
		}

		static convertPointsList(ptsList: IPoint[][], pc: PointConverter) : IPoint[][] {
			var filterPtsList : IPoint[][] = null;
			if (ptsList != null && ptsList.length > 0) {
				filterPtsList = [];
				$.each(ptsList, function (index, pts) {
					var filterPts = VectorItem.convertPoints(pts, pc);
					if (filterPts) {
						filterPtsList.push(filterPts);
					}
				});
			}

			return filterPtsList;
		}
	}

	/** @ignore */
	class VectorPlacemark extends VectorItem {
		private _bnds: Rectangle;

		point: Point;
		label: string;

		public applyData(data: IVector) {
			super.applyData(data);
			if (data.coordinates && data.coordinates.length > 1) {
				this.point = new Point(data.coordinates[0], data.coordinates[1]);
			}
		}

		public applyGeoData(coords: GeoJSON.Position, properties) {
			super.applyGeoData(coords, properties);
			this.point = Utils.getPointData(coords);
		}

		_applyProperties(properties) {
			var self = this;
			if (properties) {
				super._applyProperties(properties);
				self.label = properties["label"];
			}
		}

		public getBounds(): Rectangle {
			var self = this;
			return new Rectangle(new Point(self.point.x, self.point.y), new Size(0, 0));
		}

		public getViewBounds(): Rectangle {
			return this._bnds;
		}

		public isVisible(minsz2: number, zoom: number): boolean {
			var lod = this._lod,
				visible = true;
			if (!lod.isDefault()) {
				visible = (zoom >= lod.minZoom) && (zoom <= lod.maxZoom);
			}

			return visible;
		}

		public getShape() : Shape {
			return null;
		}

		public updateShape(pc: PointConverter) {
			var self = this,
				pt = pc(self.point),
				bnds = new Rectangle(new Point(pt.x, pt.y), new Size(0, 0));
			self._bnds = bnds;
		}
	}

	/** @ignore */
	class VectorPolyline extends VectorItem {
		points: Point[];
		private _gbnds: Rectangle;
		private _bnds: Rectangle;
		private _plines: MultiPolyline;

		public applyData(data: IVector) {
			var self = this;
			super.applyData(data);
			self.points = Utils.getPoints(data.coordinates);
			self._gbnds = Utils.getBounds(self.points);
		}

		public applyGeoData(coords: GeoJSON.Position[], properties) {
			super.applyGeoData(coords, properties);
			var self = this;
			self.points = Utils.getLineData(coords);
			self._gbnds = Utils.getBounds(self.points);
		}

		public getBounds(): Rectangle {
			return this._gbnds;
		}

		public getViewBounds(pc?: PointConverter): Rectangle {
			var self = this;

			if (pc) {
				var pt1: IPoint = new Point(self._gbnds.location.x, self._gbnds.location.y);
				var pt2: IPoint = new Point(self._gbnds.getRight(), self._gbnds.getBottom());

				pt1 = pc(pt1);
				pt2 = pc(pt2);

				self._bnds = Utils.getBounds([pt1, pt2]);
			}

			return self._bnds;
		}

		public getShape(): Shape {
			return this._plines;
		}

		public updateShape(pc: PointConverter) {
			var self = this,
				clip = wijvectorlayer._clipRect,
				pts = VectorItem.convertPoints(self.points, pc);

			if (!self._plines) {
				self._plines = new MultiPolyline();
				self._initShape(self._plines);
			}

			self._bnds = Utils.getBounds(pts);

			if (self._bnds.size.width > clip.size.width ||
				self._bnds.size.height > clip.size.height) {
				self._plines.points = ClippingEngine.createClippedLines(pts, clip);
				self._bnds = self._bnds.intersect(clip);
			}
			else {
				self._plines.points = [pts];
			}

			return this._bnds;
		}
	}

	/** @ignore */
	class VectorPolygon extends VectorItem {
		private _bnds: Rectangle;
		private _gbnds: Rectangle;
		private _polygon: Polygon;

		pointsList: IPoint[][];

		public applyData(data: IVector) {
			var self = this, geoBounds: Rectangle, bounds:Rectangle;

			super.applyData(data);
			self.pointsList = [Utils.getPoints(data.coordinates)];
			self._gbnds = Utils.getListBounds(self.pointsList);
		}

		public applyGeoData(coords: GeoJSON.Position[][], properties) {
			var self = this;
			super.applyGeoData(coords, properties);
			self.pointsList = Utils.getPolygonData(coords);
			self._gbnds = Utils.getListBounds(self.pointsList);
		}

		public getBounds(): Rectangle {
			return this._gbnds;
		}

		public getViewBounds(pc?: PointConverter): Rectangle {
			var self = this;

			if (pc) {
				var pt1: IPoint = new Point(self._gbnds.location.x, self._gbnds.location.y),
					pt2: IPoint= new Point(this._gbnds.getRight(), this._gbnds.getBottom());

				pt1 = pc(pt1);
				pt2 = pc(pt2);

				self._bnds = Utils.getBounds([pt1, pt2]);
			}

			return self._bnds;
		}

		public getShape(): Shape {
			return this._polygon;
		}

		public updateShape(pc: PointConverter) {
			var self = this,
				clip = wijvectorlayer._clipRect,
				pts = VectorItem.convertPointsList(self.pointsList, pc);

			if (!self._polygon) {
				self._polygon = new Polygon();
				self._initShape(self._polygon);
			}

			self._bnds = Utils.getListBounds(pts);

			if (self._bnds.size.width > clip.size.width ||
				self._bnds.size.height > clip.size.height) {
				pts = ClippingEngine.clipPolygonList(pts, clip);
				self._bnds = Utils.getListBounds(pts);
			}

			self._polygon.pointsList = pts;
		}
	}

	/**
	* The options of the wijvectorlayer.
	*/
	export class wijvectorlayer_options extends wijlayer_options implements IVectorLayerOptions {
		/** 
		* The type of the data object. The vector layer supports the following type:
		* <ul>
		* <li>wijJson: the json object with the C1 format. </li>
		* <li>geoJson: the json object with the GeoJson format.</li>
		* </ul>
		*/
		dataType: string = DataJsonType.wijJson;

		/** 
		* The data of the data source.
		* @remarks The data may be the following:
		* <ul>
		* <li>string: the uri of the json data.</li>
		* <li>function: the function to be called to get the data object.</li>
		* <li>object: the data object.</li>
		* </ul>
		*/
		data: any;

		/**
		* Specifies minimal size at which vectors become visible. The default value is 3.
		*/
		minSize = 3;

		/**
		* Customize the rendering of placemark.
		*/
		placemark: IPlacemark = null;

		/**
		* This is a callback function called after the shape of the vector created for rendering.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {IVectorEventData} data The data contains the vector and shape.
		* @dataKey {vector} The vector object which wraps the original data from data source.
		* @dataKey {shape} The set of Raphael elements which will be drawn.
		*/
		shapeCreated: IVectorEvent = null;

		/**
		* This is a callback function called when the mouse enter the shape.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {IVectorEventData} data The data contains the vector and shape.
		* @dataKey {vector} The vector object which wraps the original data from data source.
		* @dataKey {shape} The set of Raphael elements which will be drawn.
		*/
		mouseEnter: IVectorEvent = null;

		/**
		* This is a callback function called when the mouse leave the shape.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {IVectorEventData} data The data contains the vector and shape.
		* @dataKey {vector} The vector object which wraps the original data from data source.
		* @dataKey {shape} The set of Raphael elements which will be drawn.
		*/
		mouseLeave: IVectorEvent = null;
		/**
		* This is a callback function called when click mouse down on the shape.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {IVectorEventData} data The data contains the vector and shape.
		* @dataKey {vector} The vector object which wraps the original data from data source.
		* @dataKey {shape} The set of Raphael elements which will be drawn.
		*/
		mouseDown: IVectorEvent = null;

		/**
		* This is a callback function called when click mouse up on the shape.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {IVectorEventData} data The data contains the vector and shape.
		* @dataKey {vector} The vector object which wraps the original data from data source.
		* @dataKey {shape} The set of Raphael elements which will be drawn.
		*/
		mouseUp: IVectorEvent = null;

		/**
		* This is a callback function called when move mouse on the shape.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {IVectorEventData} data The data contains the vector and shape.
		* @dataKey {vector} The vector object which wraps the original data from data source.
		* @dataKey {shape} The set of Raphael elements which will be drawn.
		*/
		mouseMove: IVectorEvent = null;

		/**
		* This is a callback function called when click mouse on the shape.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {IVectorEventData} data The data contains the vector and shape.
		* @dataKey {vector} The vector object which wraps the original data from data source.
		* @dataKey {shape} The set of Raphael elements which will be drawn.
		*/
		click: IVectorEvent = null;
	}

	/** @ignore */
	class DataJsonType {
		public static wijJson = "wijJson";
		public static geoJson = "geoJson";
	}

	/** @ignore */
	export interface PointConverter {
		(point: IPoint): IPoint;
	}

	/** @ignore */
	interface IMarkCache {
		location: Point;
		mark?: JQuery;
		markSize?: ISize;
		label?: JQuery;
		labelSize?: ISize;
	}

	/** @ignore */
	interface IRaphaelGroup {
		push(item, ...others): IRaphaelGroup;
		transform(matix: RaphaelMatrix): void;
	}

	/**
	* Defines the data object (wijJson) of the data for the vector layer.
	*/
	export interface IWijJsonData {
		/**
		* The array of individual vector data.
		*/
		vectors: IVector[];
	}

	/** 
	* The data for the wijvectorlayer event handler.
	*/
	export interface IVectorEventData {
		/**
		* The vector object which wraps the original data from data source.
		*/
		vector: IVectorObject;

		/**
		* The set of Raphael elements which will be drawn.
		*/
		shape?: RaphaelSet;
	}

	/**
	* The event handler for the wijvectorlayer events.
	*/
	export interface IVectorEvent {
		/**
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {IVectorEventData} data The data contains the vector and shape.
		*/
		(event, data:IVectorEventData): void;
	}

	/**
	* Defines the wijmaps vector layer.
	*/
	export interface IVectorLayerOptions extends ILayerOptions {
		/** 
		* The type of the data object. The vector layer supports the following type:
		* <ul>
		* <li>wijJson: the json object with the C1 format. </li>
		* <li>geoJson: the json object with the GeoJson format.</li>
		* </ul>
		*/
		dataType: string;

		/** 
		* The data of the data source.
		* @remarks The data may be the following:
		* <ul>
		* <li>string: the uri of the json data.</li>
		* <li>function: the function to be called to get the data object.</li>
		* <li>object: the data object.</li>
		* </ul>
		*/
		data: any;

		/**
		* Customize the rendering of placemark.
		*/
		placemark?: IPlacemark;

		/**
		* Specifies minimal size at which vectors become visible. The default value is 3.
		*/
		minSize?: number;

		/**
		* This is a callback function called after the shape of the vector created for rendering.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {IVectorEventData} data The data contains the vector and shape.
		* @dataKey {vector} The vector object which wraps the original data from data source.
		* @dataKey {shape} The set of Raphael elements which will be drawn.
		*/
		shapeCreated?: IVectorEvent;

		/**
		* This is a callback function called when the mouse enter the shape.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {IVectorEventData} data The data contains the vector and shape.
		* @dataKey {vector} The vector object which wraps the original data from data source.
		* @dataKey {shape} The set of Raphael elements which will be drawn.
		*/
		mouseEnter?: IVectorEvent;

		/**
		* This is a callback function called when the mouse leave the shape.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {IVectorEventData} data The data contains the vector and shape.
		* @dataKey {vector} The vector object which wraps the original data from data source.
		* @dataKey {shape} The set of Raphael elements which will be drawn.
		*/
		mouseLeave?: IVectorEvent;

		/**
		* This is a callback function called when click mouse down on the shape.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {IVectorEventData} data The data contains the vector and shape.
		* @dataKey {vector} The vector object which wraps the original data from data source.
		* @dataKey {shape} The set of Raphael elements which will be drawn.
		*/
		mouseDown?: IVectorEvent;

		/**
		* This is a callback function called when click mouse up on the shape.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {IVectorEventData} data The data contains the vector and shape.
		* @dataKey {vector} The vector object which wraps the original data from data source.
		* @dataKey {shape} The set of Raphael elements which will be drawn.
		*/
		mouseUp?: IVectorEvent;

		/**
		* This is a callback function called when move mouse on the shape.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {IVectorEventData} data The data contains the vector and shape.
		* @dataKey {vector} The vector object which wraps the original data from data source.
		* @dataKey {shape} The set of Raphael elements which will be drawn.
		*/
		mouseMove?: IVectorEvent;

		/**
		* This is a callback function called when click mouse on the shape.
		* @event
		* @param {jQuery.Event} event A jQuery Event object.
		* @param {IVectorEventData} data The data contains the vector and shape.
		* @dataKey {vector} The vector object which wraps the original data from data source.
		* @dataKey {shape} The set of Raphael elements which will be drawn.
		*/
		click?: IVectorEvent;
	}

	/**
	* Determine how to render the placemark.
	*/
	export interface IPlacemark {
		/**
		* The image to be drawn for the mark.
		*/
		image?: string;

		/**
		* The callback functiton for drawing the mark.
		* @event
		* @param {IVectorObject} item The vector object which wraps the original data from data source.
		* @returns The jQuery object which will be append to the DOM.
		* @remarks The <b>image</b> is ignored if it is set.
		*/
		render?: (item: IVectorObject) => JQuery;

		/**
		* The size of the mark. 
		* @remarks The size of the image is used if not specified.
		*/
		size?: ISize;

		/**
		* The point within the mark to pin the mark relates to the specific placemark location. 
		* @remarks The (0,0) is used if not specified.
		*/
		pinPoint?: IPoint;

		/**
		* Determine when the label will be visible. 
		* The possible values are:
		* <ul>
		* <li>hidden: Always not draw the label.</li>
		* <li>autoHide: Does not draw the label if it overlaps with other labels.</li>
		* <li>visible: Always draw the label.</li>
		* </ul>
		*/
		labelVisibility?: string;

		/**
		* Specifies the label position relates to the placemark location. 
		* The possible values are:
		* <ul>
		* <li>center: label is centered.</li>
		* <li>left: label is at the left.</li>
		* <li>right: label is at the right.</li>
		* <li>top: label is at the top.</li>
		* <li>bottom: label is at the bottom.</li>
		* </ul>
		*/
		labelPosition?: string;
	}

	/** 
	* Defines the data for one individual vector item.
	*/
	export interface IVector {
		/**
		* The type of the vector.
		* Possible values are:
		* <ul>
		* <li>placemark: Represents a placrmark on the map.</li>
		* <li>polyline: Represents a polyline on the map.</li>
		* <li>polygon: Represents a polygon on the map.</li>
		* </ul>
		*/
		type: string;

		/** 
		* An array of coordinates which represents the vector item, in geographic unit (longitude and latitude).
		*/
		coordinates: number[];

		/**
		* Optional. The name of the vector item.The default is empty string.
		*/
		name?: string;

		/**
		* Optional. The label of the vector item. The default is empty string.
		* @remarks For placemark, the label text is from <b>label</b> and then <b>name</b>.
		*/
		label?: string;

		/**
		* Optional. The fill color of the shape. The default is "transparent".
		*/
		fill?: string;

		/**
		* Optional. The fill opacity of the shape, from 0 to 1.
		*/
		fillOpacity?: number;

		/**
		* Opitional. The stroke color of the shape. The default is "#000".
		*/
		stroke?: string;

		/**
		* Optional. The stroke opacity of the shape, from 0 to 1.
		*/
		strokeOpacity?: number;

		/**
		* Optional. The stroke width of the shape, in screen unit (pixel). The default is 1.
		*/
		strokeWidth?: number;

		/**
		* Optional. The arrary to define the stroke dash style. The default is empty string.
		* @example [“”, “-”, “.”, “-.”, “-..”, “. ”, “- ”, “--”, “- .”, “--.”, “--..”]
		*/
		strokeDashArray?: string;

		/**
		* Optional. The shape at the end of the line. 
		* Possible values are:
		* <ul>
		* <li>butt</li>
		* <li>square</li>
		* <li>round</li>
		* </ul>
		* The default is "butt".
		*/
		strokeLineCap?: string;

		/**
		* Optional. The shape that joins two lines. 
		* Possible values are:
		* <ul>
		* <li>bevel</li>
		* <li>round</li>
		* <li>miter</li>
		* </ul>
		* The default is "bevel".
		*/
		strokeLineJoin?: string;

		/**
		* The limit on the ratio of the miter length to half the stroke thickness of the shape. 
		* This value is always a positive number that is greater than or equal to 1.
		* The default is 1.
		*/
		strokeMiterLimit?: number;

		/**
		* Optional. The level of details of the vector.
		**/
		lod?: ILOD;
	}

	wijvectorlayer.prototype.options = $.extend(true, {}, wijmo.wijmoWidget.prototype.options, new wijvectorlayer_options());

	$.wijmo.registerWidget("wijvectorlayer", $.wijmo.wijlayer, wijvectorlayer.prototype);

}

/** @ignore */
interface JQuery {
	wijvectorlayer: JQueryWidgetFunction;
}
