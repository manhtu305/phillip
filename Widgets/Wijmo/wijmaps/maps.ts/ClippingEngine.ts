﻿/// <reference path="drawing.ts" />
module wijmo.maps {


	/** @ignore */
	export class ClippingEngine {
		static createClippedLines(pts: IPoint[], clipBounds: IRectangle): IPoint[][] {
			var pointsList: IPoint[][] = [],
				len = pts.length;

			for (var i = 1; i < len; i++) {
				var clippedLine = ClippingEngine._clipSegmentCS(clipBounds, [pts[i - 1], pts[i]]);
				if (clippedLine) {
					pointsList.push(clippedLine);
				}
			}

			return pointsList;
		}

		private static _clipSegmentCS(clipBounds: IRectangle, pts: IPoint[]): IPoint[] {
			var visible = false,
				pt0 = pts[0],
				pt1 = pts[1],
				cn = ClippingEngine._csCode(clipBounds, pt0),
				ck = ClippingEngine._csCode(clipBounds, pt1),

				dx = pt1.x - pt0.x,
				dy = pt1.y - pt0.y,

				dxdy = 0,
				dydx = 0;

			if (dx !== 0)
				dydx = dy / dx;
			else {
				if (dy === 0) {
					if (cn === 0 && ck === 0)
						return [pt0, pt1];
					else
						return null;
				}
			}

			if (dy !== 0)
				dxdy = dx / dy;

			var ii = 4,
				s: number,
				r: number;

			do {
				if ((cn & ck) > 0) {
					break;
				}

				if (cn === 0 && ck === 0) {
					visible = true;
					break;
				}

				if (cn === 0) {
					s = cn;
					cn = ck;
					ck = s;

					r = pt0.x;
					pt0.x = pt1.x;
					pt1.x = r;

					r = pt0.y;
					pt0.y = pt1.y;
					pt1.y = r;
				}

				if ((cn & 1) > 0) {
					var left = clipBounds.location.x;
					pt0.y += dydx * (left - pt0.x);
					pt0.x = left;
				}
				else if ((cn & 2) > 0) {
					var right = clipBounds.location.x + clipBounds.size.width;
					pt0.y += dydx * (right - pt0.x);
					pt0.x = right;
				}
				else if ((cn & 4) > 0) {
					var bottom = clipBounds.location.y + clipBounds.size.height;
					pt0.x += dxdy * (bottom - pt0.y);
					pt0.y = bottom;
				}
				else if ((cn & 8) > 0) {
					var top = clipBounds.location.y;
					pt0.x += dxdy * (top - pt0.y);
					pt0.y = top;
				}

				cn = ClippingEngine._csCode(clipBounds, pt0);
			} while (--ii >= 0);

			if (visible)
				return [pt0, pt1];

			return null;
		}

		static clipPolygon(pts: IPoint[], clipBounds: IRectangle): IPoint[] {
			var len = pts.length;
			if (len < 2) return null;


			var list: IPoint[] = [];
			for (var i = 0; i < len; i++) {
				list.push(pts[i]);
			}

			var left = clipBounds.location.x,
				top = clipBounds.location.y,
				right = left + clipBounds.size.width,
				bottom = top + clipBounds.size.height;
			list = ClippingEngine._clipPolygonByLine(list, new Point(left, top), new Point(right, top));
			list = ClippingEngine._clipPolygonByLine(list, new Point(right, top), new Point(right, bottom));
			list = ClippingEngine._clipPolygonByLine(list, new Point(right, bottom), new Point(left, bottom));
			list = ClippingEngine._clipPolygonByLine(list, new Point(left, bottom), new Point(left, top));

			return list;
		}

		static clipPolygonList(ptsList: IPoint[][], clipBounds: IRectangle): IPoint[][] {
			var list: IPoint[][] = [];
			$.each(ptsList, function (index, pts) {
				list.push(ClippingEngine.clipPolygon(pts, clipBounds));
			});
			return list;
		}

		private static _clipPolygonByLine(pts: IPoint[], cp1: IPoint, cp2: IPoint): IPoint[] {
			var list: IPoint[] = [],
				len = pts.length;

			if (len > 0) {

				var p0 = pts[len - 1];
				for (var i = 0; i < len; i++) {
					var p = pts[i];
					if (this._inside(p, cp1, cp2)) {
						if (this._inside(p0, cp1, cp2))
							list.push(p);
						else {
							list.push(this._intersect(p0, p, cp1, cp2));
							list.push(p);
						}
					}
					else {
						if (this._inside(p0, cp1, cp2)) {
							list.push(this._intersect(p0, p, cp1, cp2));
						}
					}

					p0 = p;
				}
			}

			return list;
		}

		private static _inside(p: IPoint, cp0: IPoint, cp1: IPoint): boolean {
			if (cp1.x > cp0.x)              /*bottom edge*/
				if (p.y >= cp0.y) return true;
			if (cp1.x < cp0.x)              /*top edge*/
				if (p.y <= cp0.y) return true;
			if (cp1.y > cp0.y)              /*right edge*/
				if (p.x <= cp1.x) return true;
			if (cp1.y < cp0.y)              /*left edge*/
				if (p.x >= cp1.x) return true;
			return false;
		}

		private static _intersect(p0: IPoint, p1: IPoint, cp0: IPoint, cp1: IPoint): Point {
			var p = new Point(0, 0);
			if (cp0.y === cp1.y) {
				p.y = cp0.y;
				p.x = p0.x + (cp0.y - p0.y) *
				(p1.x - p0.x) / (p1.y - p0.y);
			}
			else {
				p.x = cp0.x;
				p.y = p0.y + (cp0.x - p0.x) *
				(p1.y - p0.y) / (p1.x - p0.x);
			}

			return p;
		}

		private static _csCode(rect: IRectangle, pt: IPoint): number {
			var i = 0,

				left = rect.location.x,
				top = rect.location.y,
				right = left + rect.size.width,
				bottom = top + rect.size.height;

			if (pt.x < left)
				++i;
			else if (pt.x > right)
				i += 2;

			if (pt.y > bottom)
				i += 4;
			else if (pt.y < top)
				i += 8;

			return i;
		}
	}
}
