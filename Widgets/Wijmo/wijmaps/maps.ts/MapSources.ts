﻿/// <reference path="../../External/declarations/jquery.d.ts"/>
module wijmo.maps {
	  
	var $ = jQuery;

	/**
	* This object provides the mapping relationship between html language and bing maps language,
	* user can override this object to custom mapping relationship.
	*/
	export var cultureMapping = {
		"en": "en-US",
		"zh": "zh-Hans",
		"ja": "ja-JP"
	};

	/**
	* Defines an object that represents the tile source of the map.
	*/
	export interface IMultiScaleTileSource {
		/**
		* The width of the tile, in pixel. The default is 256.
		*/
		tileWidth: number;

		/**
		* The height of the tile, in pixel. The default is 256.
		*/
		tileHeight: number;

		/**
		* The minmum zoom level supports by this tile source. The default is 1.
		*/
		minZoom: number;

		/**
		* The maximum zoom level supports by this tile source. The default is 19.
		*/
		maxZoom: number;

		/**
		* The callback function to get the url of the specific tile.
		* @param {number} zoom The current zoom level.
		* @param {number} x The column index of the tile, in 0 base. The amount of columns is 2^zoom.
		* @param {number} y The row index of the tile, in 0 base. The amount of rows is 2^zoom.
		* @return {string} The url of the specific tile.
		*/
		getUrl(zoom: number, x: number, y: number): string;
	}

	/** 
	* @ignore
	* The built-in tile source which gets the tile from bing maps.
	*/
	class BingMapsSourceBase implements IMultiScaleTileSource {
		/**
		* The width of the tile, in pixel. The value is 256.
		*/
		public tileWidth: number = 256;
		/**
		* The height of the tile, in pixel. The value is 256.
		*/
		public tileHeight: number = 256;

		/**
		* The minmum zoom level which can show the tile. The value is 1.
		*/
		public minZoom: number = 1;

		/**
		* The maximum zoom level which can show the tile. The value is 19.
		*/
		public maxZoom: number = 19;

		_uriFormat: string = "";
		_subdomains: string[] = ["0", "1", "2", "3", "4", "5", "6", "7"];

		/**
		* The callback function to get the url of the specific tile.
		* @param {number} zoom The current zoom level.
		* @param {number} x The column index of the tile, in 0 base. The amount of columns is 2^zoom.
		* @param {number} y The row index of the tile, in 0 base. The amount of rows is 2^zoom.
		* @return {string} The url of the specific tile.
		*/
		public getUrl(zoom: number, x: number, y: number): string {
			var self = this, subdomain = self._subdomains[(y * Math.pow(2, zoom) + x) % self._subdomains.length];
			return self._uriFormat.replace("{subdomain}", subdomain).replace("{quadkey}", self._getQuadkey(zoom, x, y)).replace("{culture}", self._getCulture());
		}

		// see http://msdn.microsoft.com/en-us/library/bb545006.aspx or http://www.cadmaps.com/gisblog/?p=7
		// for an explanation of how the quad tree tiles are codified
		private _getQuadkey(zoom: number, x: number, y: number): string {
			var quadkey = "", quadCode = [0, 2, 1, 3];
			for (var i = zoom; i > 0; --i) {
				quadkey += quadCode[((x >> (i - 1) << 1) & 2) | ((y >> (i - 1)) & 1)];
			}
			return quadkey;
		}

		private _getCulture(): string {
			var bodyLang = $("body").attr("lang"), htmlLang = $("html").attr("lang"),
				lang = bodyLang || htmlLang || "en";
			return wijmo.maps.cultureMapping[lang] || "en-US";
		}
	}

	/**
	* @ignore
	* The built-in tile source which gets the tile from bing maps with road style.
	*/
	class BingMapsRoadSource extends BingMapsSourceBase {
		//"http://t{subdomain}.tiles.ditu.live.com/tiles/r{quadkey}.png?g=41";
		_uriFormat: string = "http://ecn.t{subdomain}.tiles.virtualearth.net/tiles/r{quadkey}.jpeg?g=2889&mkt={culture}&shading=hill";
	}

	/**
	* @ignore
	* The built-in tile source which gets the tile from bing maps with aerial style.
	*/
	class BingMapsAerialSource extends BingMapsSourceBase {
		_uriFormat: string = "http://ecn.t{subdomain}.tiles.virtualearth.net/tiles/a{quadkey}.jpeg?g=2889&mkt={culture}&shading=hill";
	}

	/**
	* @ignore
	* The built-in tile source which gets the tile from bing maps with hybrid style.
	*/
	class BingMapsHybridSource extends BingMapsSourceBase {
		_uriFormat: string = "http://ecn.t{subdomain}.tiles.virtualearth.net/tiles/h{quadkey}.jpeg?g=2889&mkt={culture}&shading=hill";
	}

	/**
	* @ignore
	* Defines the built-in tile sources.
	*/
	export class MapSource {
		/**
		* Bing maps with road style.
		*/
		public static bingMapsRoadSource = new BingMapsRoadSource();
		/**
		* Bing maps with aerial style.
		*/
		public static bingMapsAerialSource = new BingMapsAerialSource();
		/**
		* Bing maps with hybrid style.
		*/
		public static bingMapsHybridSource = new BingMapsHybridSource();
	}
} 