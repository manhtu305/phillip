﻿/// <reference path="../Base/jquery.wijmo.widget.ts" />
/// <reference path="../wijtooltip/jquery.wijmo.wijtooltip.ts" />

/*globals jQuery*/
/*
* Depends:
*  jquery.js
*  jquery-ui.js
*/
module wijmo.treemap {

	var $ = jQuery,
		widgetName = "wijtreemap",
		cls = {
			wijtreemap: "wijmo-wijtreemap",
			container: "wijmo-wijtreemap-container",
			item: "wijmo-wijtreemap-item",
			itemContainer: "wijmo-wijtreemap-itemcontainer",
			title: "wijmo-wijtreemap-title",
			label: "wijmo-wijtreemap-label",
			stateDefault: "wijstate-default",
			stateHover: "wijstate-hover",
			buttons: "wijmo-wijtreemap-buttons",
			backButton: "wijmo-wijtreemap-back",
			homeButton: "wijmo-wijtreemap-home"
		},
		defaultColors = [{
			max: "#00277d", mid: "#5f7ec1", min: "#a8bbe6"
		}, {
				max: "#7d1f00", mid: "#c1785f", min: "#e6b7a8"
			}, {
				max: "#007d27", mid: "#5fc17e", min: "#a8e6bc"
			}, {
				max: "#7d003c", mid: "#c15f8f", min: "#e6a8c6"
			}, {
				max: "#7d4300", mid: "#c1945f", min: "#e6c9a8"
			}, {
				max: "#51007d", mid: "#9f5fc2", min: "#d1aae6"
			}, {
				max: "#7d7400", mid: "#c2bb5f", min: "#e6e2a8"
			}, {
				max: "#970000", mid: "#c25f5f", min: "#e6a9a9"
			}];

	/** @widget */
	export class wijtreemap extends wijmoWidget {
		private width: number;
		private height: number;
		private container: JQuery;
		private tmItemContainer: ITMItemContainer;
		private colorConverter: ColorConverter;
		private currentContainer: ITMItemContainer;
		private buttons: JQuery;

		_setOption(key: string, value: any) {
			var self = this,
				o = <WijTreeMapOptions>self.options;
			if (value === o[key]) {
				return;
			}
			super._setOption(key, value);

			switch (key) {
				case "width":
					self._setWidth(value);
					self._repaintBox();
					break;
				case "height":
					self._setHeight(value);
					self._repaintBox();
					break;
				case "type":
					self._repaintBox();
					break;
				case "showDepth":
					break;
				case "data":
					self._refresh();
					break;
				case "valueBinding":
					self._resetBinding("value");
					self._repaint();
					break;
				case "labelBinding":
					self._resetBinding("label");
					self._resetLabels();
					break;
				case "colorBinding":
					self._resetBinding("color");
					self._repaintColor();
					break;
				case "animation":
					break;
				case "showLabel":
					self._showLabels();
					break;
				case "labelFormatter":
					self._resetLabels();
					break;
				case "showTooltip":
					break;
				case "tooltipOptions":
					self._destroyTooltip();
					self._createTooltip();
					break;
				case "showTitle":
				case "titleHeight":
					self._repaintBox();
					break;
				case "titleFormatter":
					self._resetTitles();
					break;
				case "minColor":
				case "minColorValue":
				case "midColor":
				case "midColorValue":
				case "maxColor":
				case "maxColorValue":
					self._resetColor(key, value);
					break;
				case "itemPainting":
				case "itemPainted":
				case "painting":
				case "painted":
					//if the previous 4 events are reset, need to recreate items to trigger them.
					self._refresh();
					break;
				default:
					break;
			}
		}

		/**
		* This method refreshes the treemap.
		*/
		refresh() {
			this._refresh();
		}

		_refresh() {
			var self = this;
			self._calcBounds();
			self._recreateItems();
			self._createColor();
			self._destroyTooltip();
			self._createTooltip();
			self._recreateBackButtons();
		}

		_create() {
			var self = this;
			super._create();

			self._decorate(true);
			self._calcBounds();
			self._createItems();
			self._createColor();
			self._bindEvents();
			self._createBackButtons();
			self._createTooltip();
		}

		_createBackButtons() {
			var self = this,
				wijCSS = (<WijTreeMapOptions>self.options).wijCSS,
				backBtnCls = [cls.backButton, wijCSS.tmbackButton, wijCSS.icon, wijCSS.iconArrowReturn],
				homeBtnCls = [cls.homeButton, wijCSS.tmhomeButton, wijCSS.icon, wijCSS.iconHome],
				buttonContainer = $("<span><span").addClass(cls.buttons).addClass(wijCSS.tmbuttons),
				backBtn = $("<span title='" + self.localizeString("backButtonTitle", "back") + "'></span>").addClass(backBtnCls.join(" ")),
				homeBtn = $("<span title='" + self.localizeString("homeButtonTitle", "home") + "'></span>").addClass(homeBtnCls.join(" "));

			buttonContainer.append(backBtn).append(homeBtn);
			self.tmItemContainer.ele.append(buttonContainer);
			self.buttons = buttonContainer;
			self._showBackButtons();
			self.buttons.bind("click", function (e) {
				e.stopPropagation();
				e.preventDefault();
				var target = $(e.target);
				if (target.hasClass(cls.backButton)) {
					self._play(self.currentContainer, (<TMItemContainer>self.currentContainer).parent);
				} else if (target.hasClass(cls.homeButton)) {
					self._play(self.currentContainer, self.tmItemContainer);
				}
			});
		}

		/** @ignore */
		private localizeString(key, defaultValue) {
			var o = this.options;
			if (o.localization && o.localization[key]) {
				return o.localization[key];
			}
			return defaultValue;
		}

		_destroyBackButtons() {
			var btns = this.buttons;
			if (btns) {
				btns.unbind("click");
				btns.remove();
				this.buttons = null;
			}
		}

		_recreateBackButtons() {
			this._destroyBackButtons();
			this._createBackButtons();
		}

		_showBackButtons() {
			var self = this,
				o = <WijTreeMapOptions>self.options;
			if (!self.buttons) {
				return;
			}

			if (!o.showBackButtons || self.currentContainer === self.tmItemContainer) {
				self.buttons.hide();
			} else {
				self.buttons.show();
			}
		}

		_createTooltip() {
			if (!(<any>$).wijmo.wijtooltip) {
				return;
			}
			var self = this,
				o = <WijTreeMapOptions>self.options,
				tooltipOptions = o.tooltipOptions,
				content = tooltipOptions && tooltipOptions.content,
				showing = tooltipOptions && tooltipOptions.showing;

			$("." + cls.item + "." + cls.stateDefault, self.element).wijtooltip($.extend(true, {
				position: {
					at: "right-30 top+30"
				}
			}, tooltipOptions, {
					content: function () {
						var tmItem = <TMItem>self._getItemByEle(this);
						if (tmItem) {
							if (content) {
								return content.call(this, tmItem.data);
							} else {
								return tmItem.label() + ": " + tmItem.value();
							}
						}
					},
					showing: function () {
						if (!o.showTooltip || self._isDisabled()) {
							return false;
						}
						if (showing) {
							return showing.apply(this, arguments);
						}
					}
				}));
		}

		_destroyTooltip() {
			if ((<any>$).wijmo.wijtooltip) {
				var tooltipEle = $("." + cls.item + "." + cls.stateDefault, this.element);
				if (tooltipEle.data("wijmo-wijtooltip")) {
					tooltipEle.wijtooltip("destroy");
				}
			}
		}


		_bindEvents() {
			var self = this;
			self.element.on("click.wijtreemap", $.proxy(self._onClick, self))
				.on("contextmenu.wijtreemap", $.proxy(self._onRightClick, self))
				.on("mouseover.wijtreemap", $.proxy(self._onMouseOver, self))
				.on("mouseout.wijtreemap", $.proxy(self._onMouseOut, self));
		}

		_onClick(e) {
			if (this._isDisabled()) {
				return;
			}
			var item = <ITMItem>this._getItemByEle($(e.target));
			if (item) {
				//this._play(item, (<TMItem>item).parent);
				//change behavior accordiong to the customer's request.
				this._play(item, item);
			}
		}

		_play(item, container) {
			var self = this,
				tmC = self.tmItemContainer,
				c = <ITMItemContainer>container,
				isDrillDown;
			if (c === self.currentContainer) {
				return;
			}

			isDrillDown = self._isDrillDown(self.currentContainer.ele, c.ele);
			if (self._triggerPlaying(item, self.currentContainer, c, isDrillDown) === false) {
				return;
			}

			if (c !== tmC) {
				tmC.box.width = 0;
				tmC.box.height = 0;
				tmC.applyBox();
			}

			self.currentContainer = c;
			self._resetContainerBox(c);
			c.applyBox();

			self._triggerPlayed(c, isDrillDown);
			self._showBackButtons();
		}

		_resetContainerBox(c) {
			c.box.width = this.width;
			c.box.height = this.height;
			c.box.top = 0;
			c.box.left = 0;
		}

		_isDrillDown(currentEle: JQuery, targetEle: JQuery): boolean {
			if ($.contains(currentEle.get(0), targetEle.get(0))) {
				return true;
			} else {
				return false;
			}
		}

		_triggerPlaying(item, currentContainer, targetContainer, isDrillDown: boolean): boolean {
			var it = <ITMItem>item,
				cc = <ITMItemContainer>currentContainer,
				tc = <ITMItemContainer>targetContainer,
				data;

			data = {
				currentData: cc.data,
				targetData: tc.data,
				clickItemData: it == null ? null : it.data
			}

			if (isDrillDown) {
				return this._triggerDrillingDown(data);
			} else {
				return this._triggerRollingUp(data);
			}
			return true;
		}

		_triggerDrillingDown(args): boolean {
			return this._trigger("drillingDown", null, args);
		}

		_triggerRollingUp(args): boolean {
			return this._trigger("rollingUp", null, args);
		}

		_triggerPlayed(currentContainer, isDrillDown: boolean) {
			var data = (<ITMItemContainer>currentContainer).data;
			if (isDrillDown) {
				this._triggerDrilledDown(data);
			} else {
				this._triggerRolledUp(data);
			}
		}

		_triggerDrilledDown(args) {
			this._trigger("drilledDown", null, args);
		}

		_triggerRolledUp(args) {
			this._trigger("rolledUp", null, args);
		}

		_onRightClick(e) {
			if (this._isDisabled()) {
				return false;
			}
			var self = this,
				item = <ITMItem>self._getItemByEle($(e.target));;
			if (self.currentContainer === self.tmItemContainer) {
				return false;
			}
			self._play(item, (<TMItem>self.currentContainer).parent);
			return false;
		}

		_onMouseOver(e) {
			if (this._isDisabled()) {
				return;
			}
			var item = <ITMItem>this._getItemByEle($(e.target));
			if (item) {
				item.mouseOver();
			}
		}

		_onMouseOut(e) {
			if (this._isDisabled()) {
				return;
			}
			var item = <ITMItem>this._getItemByEle($(e.target));
			if (item) {
				item.mouseOut();
			}
		}

		_getItemByEle(ele: JQuery) {
			var self = this,
				itemEle: JQuery,
				index: number[],
				items = self.tmItemContainer.items,
				item;
			itemEle = ele.closest("." + cls.item);
			if (itemEle && itemEle.length) {
				index = <number[]>itemEle.data("index");
				if (index == null) {
					return null;
				}
				$.each(index, (idx, val) => {
					item = items[val];
					if (item && item.items) {
						items = item.items;
					} else {
						return false;
					}
				});
				return item;
			}
			return null;
		}

		/** 
		* The destroy method will remove the treemap functionality completely and will return the element to its pre-init state.
		* @example $("selector").wijtreemap("destroy");
		*/
		destroy() {
			var self = this;
			self._unbindEvents();
			self._destroyItems();
			self._destroyBackButtons();
			self._getContainer().remove();
			self._decorate(false);
			self.colorConverter = null;
			self._destroyTooltip();
			super.destroy();
		}

		_unbindEvents() {
			this.element.off(".wijtreemap");
		}

		_destroyItems() {
			this.tmItemContainer.destroy();
			this._getContainer().empty();
		}

		_createItems() {
			var self = this,
				o = <WijTreeMapOptions>self.options,
				container = self._getContainer();
			self._triggerPainting();
			self.tmItemContainer = new TMItemContainer(container, o.data, self);
			self._resetContainerBox(self.tmItemContainer);
			self.tmItemContainer.render();
			self.currentContainer = self.tmItemContainer;
			container.appendTo(self.element);
			self._triggerPainted();
		}

		_triggerPainting() {
			this._trigger("painting", null, this.element);
		}

		_triggerPainted() {
			this._trigger("painted");
		}

		_recreateItems() {
			this._destroyItems();
			this._createItems();
		}

		_createColor() {
			var self = this,
				o = <WijTreeMapOptions>self.options,
				minValue = o.minColorValue, maxValue = o.maxColorValue,
				minMaxValue;
			if (minValue == null || maxValue == null) {
				minMaxValue = self._getMinMaxItemValue(self.tmItemContainer);
				if (minValue == null) {
					minValue = minMaxValue.min;
				}
				if (maxValue == null) {
					maxValue = minMaxValue.max;
				}
			}
			if (self.colorConverter) {
				self.colorConverter = null;
			}
			//default color setting of level 1 items is added, so "#000000" and "#ffffff" is only for compling errors and doesn't work here.
			self.colorConverter = new ColorConverter(o.minColor || "#000000", minValue,
				o.maxColor || "#ffffff", maxValue, o.midColor, o.midColorValue);
			self.tmItemContainer.colorConverter = self.colorConverter;
			self._repaintColor();
		}

		_resetColor(key: string, value) {
			this.colorConverter["_reset" + key](value);
			this._repaintColor();
		}

		_getColor(val: number): string {
			return this.colorConverter.getColor(val);
		}

		_getMinMaxItemValue(container) {
			var self = this,
				itemConatiner: ITMItemContainer = <ITMItemContainer>container,
				value = {
					min: Number.MAX_VALUE,
					max: Number.MIN_VALUE
				}, val: number;

			$.each(itemConatiner.items, (idx, item) => {
				var v = self._getMinMaxItemValue(item);
				if (v.max > value.max) {
					value.max = v.max;
				}
				if (v.min < value.min) {
					value.min = v.min;
				}
			});
			//only compare tail item's value.
			//if (container.value) {
			if (container.value && itemConatiner.items.length === 0) {
				val = container.value();
				if (val > value.max) {
					value.max = val;
				}
				if (val < value.min) {
					value.min = val;
				}
			}
			return value;
		}

		_decorate(decorate: boolean) {
			var self = this,
				ele = self.element,
				o = <WijTreeMapOptions>self.options,
				classNames = [cls.wijtreemap, o.wijCSS.widget, o.wijCSS.wijtreemap].join(" ");
			ele.toggleClass(classNames, decorate);
		}

		_getContainer() {
			var self = this,
				o = <WijTreeMapOptions>self.options,
				conCls = [cls.container, o.wijCSS.tmcontainer, cls.itemContainer, o.wijCSS.tmitemContainer].join(" ");
			if (!self.container) {
				self.container = $("<div></div>").addClass(conCls);
			}
			return self.container;
		}

		_calcBounds() {
			var self = this,
				ele = self.element,
				o = <WijTreeMapOptions>self.options,
				width = o.width ? o.width : ele.width(),
				height = o.height ? o.height : ele.height();
			self._setWidth(width);
			self._setHeight(height);
		}
		_setWidth(width: number) {
			this.width = width;
			this._getContainer().css("width", width.toString());
			if (this.tmItemContainer) {
				this.tmItemContainer.box.width = width;
			}
		}
		_setHeight(height: number) {
			this.height = height;
			this._getContainer().css("height", height.toString());
			if (this.tmItemContainer) {
				this.tmItemContainer.box.height = height;
			}
		}

		_showLabels() {
			this.tmItemContainer.toggleLabelVisibility();
		}

		_resetBinding(key: string) {
			this.tmItemContainer.setBinding(key);
		}

		_resetLabels() {
			this.tmItemContainer.setLabels(false);
		}

		_resetTitles() {
			this.tmItemContainer.setLabels(true);
		}

		_repaintBox() {
			this.currentContainer.applyBox();
		}

		_repaintColor() {
			this.tmItemContainer.applyColor();
		}

		_repaint() {
			this._repaintBox();
			this._repaintColor();
		}
	}

	/** @ignore */
	class ColorConverter {
		minColor: RGBColor;
		midColor: RGBColor;
		originalMidColor: RGBColor;
		maxColor: RGBColor;
		minColorValue: number;
		midColorValue: number;
		originalMidColorValue: number;
		maxColorValue: number;

		constructor(minColor: string, minColorValue: number, maxColor: string, maxColorValue: number,
			midColor?: string, midColorValue?: number) {
			var self = this;
			self.minColor = self._convertColorToRGB(minColor);
			self.minColorValue = minColorValue;
			self.maxColor = self._convertColorToRGB(maxColor);
			self.maxColorValue = maxColorValue;
			self.midColorValue = self.originalMidColorValue = midColorValue;
			self._calculateMidColorValue();
			self.midColor = self.originalMidColor = self._convertColorToRGB(midColor);
			self._calculateMidColor();
		}

		_resetminColor(val: string) {
			var self = this;
			self.minColor = self._convertColorToRGB(val);
			self._calculateMidColor();
		}
		_resetmidColor(val: string) {
			var self = this;
			self.midColor = self.originalMidColor = self._convertColorToRGB(val);
			self._calculateMidColor();
		}
		_resetmaxColor(val: string) {
			var self = this;
			self.maxColor = self._convertColorToRGB(val);
			self._calculateMidColor();
		}
		_resetminColorValue(val: number) {
			var self = this;
			self.minColorValue = val;
			self._calculateMidColorValue();
		}
		_resetmidColorValue(val: number) {
			var self = this;
			self.midColorValue = self.originalMidColorValue = val;
			self._calculateMidColorValue();
		}
		_resetmaxColorValue(val: number) {
			var self = this;
			self.maxColorValue = val;
			self._calculateMidColorValue();
		}

		_convertColorToRGB(color: string): RGBColor {
			if (color == null) {
				return null;
			}
			var rgbStr: string,
				rex: string[];
			rgbStr = $("<div></div>").css("background-color", color).css("background-color");
			rex = rgbStr.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
			if (rex.length <= 3) {
				return;
			}
			return <RGBColor>{
				R: parseInt(rex[1]),
				G: parseInt(rex[2]),
				B: parseInt(rex[3])
			};
		}

		_calculateMidColorValue() {
			var self = this;
			if (self.originalMidColorValue == null) {
				self.midColorValue = (self.maxColorValue + self.minColorValue) / 2;
			}
		}

		_calculateMidColor() {
			var self = this;
			if (self.originalMidColor == null) {
				self.midColor = self._calculateColorByVal(self.midColorValue, true);
			}
		}

		_calculateColorByVal(val: number, skipMidCheck: boolean = false): RGBColor {
			var self = this,
				r: number, g: number, b: number,
				maxColor: RGBColor = self.maxColor,
				minColor: RGBColor = self.minColor,
				maxVal: number = self.maxColorValue,
				minVal: number = self.minColorValue;
			if (val > self.maxColorValue) {
				return <RGBColor>$.extend({}, self.maxColor);
			}
			if (val < self.minColorValue) {
				return <RGBColor>$.extend({}, self.minColor);
			}
			if (!skipMidCheck) {
				if (val === self.midColorValue) {
					return <RGBColor>$.extend({}, self.midColor);
				}
				if (val < self.midColorValue) {
					maxColor = self.midColor;
					maxVal = self.midColorValue;
				} else {
					minColor = self.midColor;
					minVal = self.midColorValue;
				}
			}
			return self._getColor(val, maxColor, maxVal, minColor, minVal);
		}

		_getColor(val: number, max: RGBColor, maxVal: number, min: RGBColor, minVal: number): RGBColor {
			var self = this;
			return <RGBColor>{
				R: self._getValueByRatio(val, max.R, maxVal, min.R, minVal),
				G: self._getValueByRatio(val, max.G, maxVal, min.G, minVal),
				B: self._getValueByRatio(val, max.B, maxVal, min.B, minVal)
			}
		}

		_getValueByRatio(val: number, max: number, maxVal: number, min: number, minVal: number): number {
			var self = this;
			return Math.abs(min + Math.round((val - minVal) * (max - min) / (maxVal - minVal)));
		}

		getColor(val: number): string {
			var self = this,
				color: RGBColor = self._calculateColorByVal(val);
			return "#" + self._hex(color.R) + self._hex(color.G) + self._hex(color.B);
		}

		_hex(val: number): string {
			return ("0" + val.toString(16)).slice(-2);
		}
	}

	/** @ignore */
	interface RGBColor {
		R: number;
		G: number;
		B: number;
	}

	/** @ignore */
	class TMUtils {
		//"Squarified Treemap" http://www.win.tue.nl/~vanwijk/stm.pdf,
		//by Mark Bruls, Kees Huizing and Jarke J. van Wijk
		static squarified(container: TMItemContainer) {
			var b = container.box,
				bounds: IBox = $.extend(true, {}, b),
				items = container.items.slice(),
				ratio: number,
				titleBox = container.titleBox,
				titleLength = titleBox.width || titleBox.height;

			titleLength = (bounds.width === 0 || bounds.height === 0) ? 0 : titleLength;
			bounds.top += titleLength;
			bounds.height -= titleLength;
			ratio = bounds.width * bounds.height / container.total;
			titleBox.width = bounds.width;
			titleBox.height = titleLength;
			do {
				var rowedItems = TMUtils.getRowedItems(items, bounds, ratio);
				TMUtils.layoutRowedItems(b, rowedItems, bounds, bounds.width > bounds.height);
			} while (items.length);
		}

		static horizontal(container: TMItemContainer) {
			var b = container.box,
				bounds: IBox = $.extend(true, {}, b),
				items = container.items,
				titleBox = container.titleBox,
				titleLength = titleBox.width || titleBox.height;

			titleLength = (bounds.width === 0 || bounds.height === 0) ? 0 : titleLength;
			bounds.left += titleLength;
			bounds.width -= titleLength;
			titleBox.width = titleLength;
			titleBox.height = bounds.height;
			$.each(items, (idx, item) => {
				var rowedItems = [{
					item: item,
					val: item.value() * (b.width - titleLength) * b.height / container.total
				}];
				TMUtils.layoutRowedItems(b, rowedItems, bounds, false);
			});
		}

		static vertical(container: TMItemContainer) {
			var b = container.box,
				bounds: IBox = $.extend(true, {}, b),
				items = container.items,
				titleBox = container.titleBox,
				titleLength = titleBox.width || titleBox.height;

			titleLength = (bounds.width === 0 || bounds.height === 0) ? 0 : titleLength;
			bounds.top += titleLength;
			bounds.height -= titleLength;
			titleBox.width = bounds.width;
			titleBox.height = titleLength;
			$.each(items, (idx, item) => {
				var rowedItems = [{
					item: item,
					val: item.value() * b.width * (b.height - titleLength) / container.total
				}];
				TMUtils.layoutRowedItems(b, rowedItems, bounds, true);
			});
		}

		static getNarrowLen(bounds: IBox): number {
			return Math.min(bounds.width, bounds.height);
		}

		static getRowedItem(item: ITMItem, bounds: IBox, ratio: number): RowedItem {
			var itemSquare: number = ratio * item.value();
			return <RowedItem>{
				item: item,
				val: itemSquare
			};
		}

		static getRowedItems(items: ITMItem[], bounds: IBox, ratio: number) {
			var item: ITMItem = items.shift(),
				row: RowedItem[] = [], newRow: RowedItem[] = [],
				len: number = TMUtils.getNarrowLen(bounds),
				rowedItem: RowedItem = TMUtils.getRowedItem(item, bounds, ratio);
			row.push(rowedItem);
			newRow.push(rowedItem);
			if (items.length > 0) {
				do {
					newRow.push(TMUtils.getRowedItem(items[0], bounds, ratio));
					if (TMUtils.worst(row, len) > TMUtils.worst(newRow, len)) {
						row = newRow.slice();
						items.shift();
					} else {
						break;
					}
				} while (items.length);
			}
			return row;
		}

		static layoutRowedItems(containerBox: IBox, rowedItems: RowedItem[], b: IBox, isVertical: boolean) {
			var left = b.left,
				top = b.top,
				maxX: number = left + b.width,
				maxY: number = top + b.height,
				rowHeight: number,
				sum: number = TMUtils.sumRowedArray(rowedItems);
			if (isVertical) {
				//if (b.width > b.height) {
				rowHeight = b.height === 0 ? 0 : sum / b.height;
				if (left + rowHeight >= maxX) {
					rowHeight = maxX - left;
				}
				$.each(rowedItems, (idx: number, item: RowedItem) => {
					var len = rowHeight === 0 ? 0 : item.val / rowHeight;
					if ((top + len) > maxY || idx === rowedItems.length - 1) {
						len = maxY - top;
					}
					$.extend(item.item.box, <IBox>{
						left: left - containerBox.left,
						top: top - containerBox.top,
						//left: left,
						//top: top,
						width: rowHeight,
						height: len
					});
					top += len;
				});
				b.left += rowHeight;
				b.width -= rowHeight;
			} else {
				rowHeight = b.width === 0 ? 0 : sum / b.width;
				if (top + rowHeight >= maxY) {
					rowHeight = maxY - top;
				}
				$.each(rowedItems, (idx: number, item: RowedItem) => {
					var len = rowHeight === 0 ? 0 : item.val / rowHeight;
					if ((left + len) > maxX || idx === rowedItems.length - 1) {
						len = maxX - left;
					}
					$.extend(item.item.box, <IBox>{
						left: left - containerBox.left,
						top: top - containerBox.top,
						width: len,
						height: rowHeight
					});
					left += len;
				});
				b.top += rowHeight;
				b.height -= rowHeight;
			}
		}

		static sumRowedArray(array: RowedItem[]): number {
			//http://jsperf.com/summing-array-elements-underscore-reduce-vs-for/2
			//http://jsperf.com/eval-join-vs-reduce
			//for loop is faster.
			var sum: number = 0, len: number = array.length;
			for (var i: number = 0; i < len; i++) {
				sum += array[i].val;
			}
			return sum;
		}

		static worst(arr: RowedItem[], w: number): number {
			var max: number, min: number, tmp: RowedItem,
				sum: number = TMUtils.sumRowedArray(arr),
				sumSquare: number = sum * sum,
				wSquare: number = w * w;
			max = min = arr[0].val;
			//Can't use Math.min/max directly, for loop is the fastest way.
			$.each(arr, (idx: number, item: RowedItem) => {
				if (item.val > max) {
					max = item.val;
				} else if (item.val < min) {
					min = item.val;
				}
			});
			return Math.max((wSquare * max) / sumSquare, sumSquare / (wSquare * min));
		}
	}

	/** @ignore */
	class TMItemContainer implements ITMItemContainer {
		box: IBox;
		titleBox: IBox;
		ele: JQuery;
		data: any;
		parent: ITMItemContainer;
		depth: number;
		tm: wijtreemap;
		total: number;
		level: number;
		items: ITMItem[];
		colorConverter: ColorConverter;

		constructor(element: JQuery, data: any, tm: wijtreemap, parent: ITMItemContainer = null) {
			var self = this;
			self.level = (parent && !isNaN(parent.level)) ? parent.level + 1 : 0;
			self.box = self._initBox();
			self.titleBox = self._initBox();
			self.tm = tm;
			self.ele = element;
			self.data = data;
			self.parent = parent;
			self.depth = self.parent === null ? 0 : (self.parent.depth + 1);
			self.total = 0;
			self.items = [];
			self._init();
		}

		_initBox(): IBox {
			return <IBox> {
				width: 0,
				height: 0,
				left: 0,
				top: 0
			};
		}

		_init() {
			var self = this,
				tmItem: ITMItem;
			if (!self.data || self.data.length === 0) {
				return;
			}
			$.each(self.data, (idx, val) => {
				tmItem = self._createTMItem(val, idx);
				self.items.push(tmItem);
				self.total += tmItem.value();
			});
		}

		_createTMItem(data, index: number): ITMItem {
			var self = this,
				tmItemEle: JQuery = $("<div></div>"),
				tmOptions = <WijTreeMapOptions>self.tm.options,
				itemClass: string[] = [cls.item, tmOptions.wijCSS.tmitem],
				tmItem: ITMItem;
			tmItem = new TMItem(tmItemEle, data, self.tm, self, index);
			if (tmItem.items.length > 0) {
				itemClass.push(cls.itemContainer, tmOptions.wijCSS.tmitemContainer);
			} else {
				itemClass.push(tmOptions.wijCSS.stateDefault, cls.stateDefault);
			}
			tmItemEle.addClass(itemClass.join(" "));
			return tmItem;
		}

		getColorConverter(): ColorConverter {
			if (this.colorConverter) {
				return this.colorConverter;
			}
			if (this.parent) {
				return this.parent.getColorConverter();
			}
			return null;
		}

		toggleLabelVisibility() {
			var self = this;
			if (self.items && self.items.length) {
				$.each(self.items, (idx, item) => {
					item.toggleLabelVisibility();
				});
			}
		}

		setLabels(isTitle: boolean) {
			var self = this;
			if (self.items && self.items.length) {
				$.each(self.items, (idx, item) => {
					item.setLabels(isTitle);
				});
			}
		}

		setBinding(key: string) {
			var self = this;
			if (self.items && self.items.length) {
				$.each(self.items, (idx, item) => {
					item.setBinding(key);
				});
			}
		}

		applyBox() {
			var self = this,
				renderOptions = <IRenderOptions> {
					applyBox: true,
					applyColor: false,
					applyHtml: false
				};
			self.titleBox = self._initBox();
			self._render(renderOptions);
			self._resetParentTitle();
		}

		_resetParentTitle() {
			var self = this,
				tmOptions = <WijTreeMapOptions>self.tm.options,
				parent = self.parent, ele;
			if (tmOptions.showTitle && parent != null && parent instanceof TMItem) {
				ele = (<TMItem>parent).ele;
				ele.css({
					left: 0,
					top: 0
				});
				(<TMItemContainer>parent)._resetParentTitle();
			}
		}

		applyColor() {
			var self = this,
				renderOptions = <IRenderOptions> {
					applyBox: false,
					applyColor: true,
					applyHtml: false
				};
			self._render(renderOptions);
		}

		render() {
			var renderOptions = <IRenderOptions> {
				applyBox: true,
				applyColor: false,
				applyHtml: true
			};
			this._render(renderOptions);
		}

		_render(options: IRenderOptions) {
			var self = this;
			if (self.items && self.items.length) {
				self._renderChildren(options);
			} else {
				self._renderContent(options);
			}
		}

		_renderChildren(options: IRenderOptions) {
			var self = this,
				tmOptions = <WijTreeMapOptions>self.tm.options,
				type: string = tmOptions.type || "squarified";
			if (options.applyBox) {
				TMUtils[type](self);
			}
			$.each(self.items, (idx, item) => {
				item._render(options);
			});
		}

		_renderContent(options: IRenderOptions) { }

		_renderColor() { }

		destroy() {
			$.each(this.items, (idx, item) => {
				item.destroy();
			});
			this.items = null;
		}
	}

	/** @ignore */
	class TMItem extends TMItemContainer implements ITMItem {
		_label: string;
		_value: number;
		_color: string;
		_isContent: boolean;
		_isCustomizeHtml: boolean;
		titleEle: JQuery;
		labelEle: JQuery;
		index: number[];

		constructor(element: JQuery, data: any, tm: wijtreemap, parent: ITMItemContainer, index: number) {
			var self = this;
			self._label = "label";
			self._value = 0;
			self._color = null;
			self.titleEle = $("<div></div>");
			self.labelEle = $("<span></span>");
			self._isContent = false;
			self._isCustomizeHtml = false;
			if (parent instanceof TMItem) {
				self.index = (<TMItem>parent).index.concat([index]);
			} else {
				self.index = [index];
			}
			element.data("index", self.index);
			super(element, data, tm, parent);
		}

		_init() {
			var self = this,
				d = self.data,
				tmOpts = <WijTreeMapOptions>self.tm.options,
				defColor = defaultColors[self.index[self.index.length - 1] % defaultColors.length],
				tmItem: ITMItem;
			self._reset("label")._reset("value")._reset("color");
			if (!d.items || d.items.length === 0) {
				return;
			}
			$.each(d.items, (idx, val) => {
				tmItem = self._createTMItem(val, idx);
				self.items.push(tmItem);
				//self.total += tmItem.value();
			});
			//If value of an item is not total value of its child items, remove this line.
			self.total = self.value();

			if (d.minColor && d.maxColor) {
				self._createColorConverter(d.maxColor, d.midColor, d.minColor);
			} else if (self.level === 1 && (tmOpts.maxColor == null || tmOpts.minColor == null)) {
				//set default color settings to first level items if minColor/maxColor is not set.
				self._createColorConverter(defColor.max, defColor.mid, defColor.min);
			}
		}

		_createColorConverter(maxColor: string, midColor: string, minColor: string) {
			var self = this,
				d = self.data,
				minValue = d.minColorValue, maxValue = d.maxColorValue,
				minMaxValue;
			if (minValue == null || maxValue == null) {
				minMaxValue = self.tm._getMinMaxItemValue(self);
				if (minValue == null) {
					minValue = minMaxValue.min;
				}
				if (maxValue == null) {
					maxValue = minMaxValue.max;
				}
			}
			if (self.colorConverter) {
				self.colorConverter = null;
			}
			self.colorConverter = new ColorConverter(minColor, minValue,
				maxColor, maxValue, midColor, d.midColorValue);
		}

		_applyBox() {
			var self = this,
				box = self.box;
			self.ele.css({
				//self.ele.animate({
				width: box.width,
				height: box.height,
				left: box.left,
				top: box.top
			});
		}

		_applyTitleBox() {
			var self = this,
				tmOptions = <WijTreeMapOptions>self.tm.options,
				box = self.titleBox;
			self.titleEle.css({
				//self.titleEle.animate({
				top: 0,
				left: 0,
				width: box.width,
				height: box.height
			});
			if (tmOptions.showTitle) {
				self.titleEle.show();
			} else {
				self.titleEle.hide();
			}
		}

		_createTitle() {
			var self = this,
				css = (<WijTreeMapOptions>self.tm.options).wijCSS;

			self.titleEle.addClass([cls.title, css.header, css.tmtitle].join(" "));
			self.titleEle.prependTo(self.ele);
		}

		_setTitle() {
			var self = this,
				tmOptions = <WijTreeMapOptions>self.tm.options,
				title = self.label(),
				titleFormatter = tmOptions.titleFormatter;

			if (titleFormatter && $.isFunction(titleFormatter)) {
				title = $.proxy(<any>titleFormatter, self.data)();
			}
			this.titleEle.html(title);
		}

		_createLabel() {
			var self = this,
				tmOptions = <WijTreeMapOptions>self.tm.options;

			self.labelEle.addClass([cls.label, tmOptions.wijCSS.tmlabel].join(" "));
			self.labelEle.prependTo(self.ele);
		}

		_setLabel() {
			var self = this,
				tmOptions = <WijTreeMapOptions>self.tm.options,
				label = self.label(),
				labelFormatter = tmOptions.labelFormatter;

			if (labelFormatter && $.isFunction(labelFormatter)) {
				label = $.proxy(<any>labelFormatter, self.data)();
			}
			self.labelEle.html(label);
		}

		_render(options: IRenderOptions) {
			var self = this;
			super._render(options);
			if (options.applyBox) {
				self._applyBox();
				self._applyTitleBox();
			}
			if (options.applyHtml) {
				self.ele.appendTo(self.parent.ele);
			}
		}

		_renderChildren(options: IRenderOptions) {
			var self = this,
				tmOptions = <WijTreeMapOptions>self.tm.options;
			//Add title to container.
			self._isContent = false;
			if (options.applyHtml) {
				self._createTitle();
				self._setTitle();
			}
			if (options.applyBox) {
				self.titleBox.height = 0;
				if (tmOptions.showTitle) {
					self.titleBox.width = parseInt(<any>tmOptions.titleHeight);
				} else {
					self.titleBox.width = 0;
				}
			}
			super._renderChildren(options);
		}

		_renderContent(options: IRenderOptions) {
			var self = this,
				applyHtml = options.applyHtml,
				template: string,
				tm = self.tm;
			self._isContent = true;
			super._renderContent(options);

			if (applyHtml) {
				template = self._triggerItemPainting($.extend(true, { htmlTemplate: "" }, self.data));
				if (typeof template === "string" && template.length > 0) {
					self._isCustomizeHtml = true;
					self.ele.empty();
					self.ele.html(template);
					self._triggerItemPainted();
				} else {
					self._isCustomizeHtml = false;
				}
			}
			if (!self._isCustomizeHtml) {
				if (applyHtml) {
					self._renderHtml();
					self._triggerItemPainted();
				}
				if (options.applyColor) {
					self._renderColor();
				}
			}
		}

		_triggerItemPainting(args: any): string {
			this.tm._trigger("itemPainting", null, args);
			return args.htmlTemplate;
		}

		_triggerItemPainted() {
			this.tm._trigger("itemPainted");
		}

		_renderHtml() {
			var self = this;
			self.ele.empty();
			self._createLabel();
			self._setLabel();
			self.toggleLabelVisibility();
		}

		_renderColor() {
			var self = this,
				tm = self.tm,
				color = self.color();
			if (color == null) {
				//color = tm._getColor(self.value());
				color = self.parent.getColorConverter().getColor(self.value());
			}
			self.ele.css("background-color", color);
		}

		toggleLabelVisibility() {
			var self = this,
				tmOptions = <WijTreeMapOptions>self.tm.options;
			if (self._isContent) {
				if (tmOptions.showLabel) {
					self.labelEle.show();
				} else {
					self.labelEle.hide();
				}
			} else {
				super.toggleLabelVisibility();
			}
		}

		setLabels(isTitle: boolean) {
			var self = this;
			if (self._isContent) {
				if (!isTitle) {
					self._setLabel();
				}
			} else {
				if (isTitle) {
					self._setTitle();
				}
				super.setLabels(isTitle);
			}
		}

		setBinding(key: string) {
			this._reset(key);
			if (!this._isContent) {
				super.setBinding(key);
			}
		}

		destroy() {
			super.destroy();
			if (this.colorConverter) {
				this.colorConverter = null;
			}
		}

		_reset(key: string): TMItem {
			var self = this,
				tmOptions = <WijTreeMapOptions>self.tm.options,
				keyVal = tmOptions[key + "Binding"] || key;
			if (keyVal in self.data) {
				self["_" + key] = self.data[keyVal];
			}
			return self;
		}

		label(value?: string): any {
			var self = this;
			if (value === undefined) {
				return self._label;
			} else {
				self._label = value;
				return self;
			}
		}
		value(value?: number): any {
			var self = this;
			if (value === undefined) {
				return self._value;
			} else {
				self._value = value;
				return self;
			}
		}
		color(value?: string): any {
			var self = this;
			if (value === undefined) {
				return self._color;
			} else {
				self._color = value;
				return self;
			}
		}

		click(e) {
		}

		mouseOver() {
			if (this._isContent) {
				var tmOptions = <WijTreeMapOptions>this.tm.options;
				this.ele.addClass(tmOptions.wijCSS.stateHover).addClass(cls.stateHover);
			}
		}
		mouseOut() {
			if (this._isContent) {
				var tmOptions = <WijTreeMapOptions>this.tm.options;
				this.ele.removeClass(tmOptions.wijCSS.stateHover).removeClass(cls.stateHover);
			}
		}
	}

	/** @ignore */
	interface ITMItemContainer {
		data: any;
		box: IBox;
		ele: JQuery;
		applyBox(): void;
		applyColor(): void;
		render? (): void;
		_render(options: IRenderOptions): void;
		destroy(): void;
		depth: number;
		tm: wijtreemap;
		total: number;
		level: number;
		items: ITMItem[];
		toggleLabelVisibility(): void;
		setLabels(isTitle: boolean): void;
		setBinding(key: string): void;
		getColorConverter(): ColorConverter;
		colorConverter: ColorConverter;
	}
	/** @ignore */
	interface ITMItem extends ITMItemContainer {
		label(value?: string): any;
		value(value?: number): any;
		color(value?: string): any;
		index: number[];
		click(event): void;
		mouseOver(): void;
		mouseOut(): void;
	}

	/** @ignore */
	interface IBox {
		width: number;
		height: number;
		left: number;
		top: number;
	}
	/** @ignore */
	interface IRenderOptions {
		applyBox: boolean;
		applyColor: boolean;
		applyHtml: boolean;
	}

	/** @ignore */
	interface RowedItem {
		item: ITMItem;
		val: number;
	}

	/** @ignore */
	export interface WijTreeMapOptions extends WidgetOptions {
		wijCSS?: WijTreeMapCSS;
		width?: number;
		height?: number;
		type?: string;
		showDepth?: number;
		data?: any;
		valueBinding?: string;
		labelBinding?: string;
		colorBinding?: string;
		animation?: any; //TODO: add interface of animation
		showLabel?: boolean;
		labelFormatter?: Function;
		showTooltip?: boolean;
		tooltipOptions?: any;
		showTitle?: boolean;
		titleHeight?: number;
		titleFormatter?: Function;
		minColor?: string;
		minColorValue?: number;
		midColor?: string;
		midColorValue?: number;
		maxColor?: string;
		maxColorValue?: number;
		showBackButtons?: boolean;
		//Events
		itemPainting?: Function;
		itemPainted?: Function;
		painting?: Function;
		painted?: Function;
		drillingDown?: Function;
		drilledDown?: Function;
		rollingUp?: Function;
		rolledUp?: Function;
		click?: Function;
	}

	/** @ignore */
	export interface WijTreeMapCSS extends WijmoCSS {
		wijtreemap: string;
		tmcontainer: string;
		tmitem: string;
		tmitemContainer: string;
		tmtitle: string;
		tmlabel: string;
		tmbuttons: string;
		tmbackButton: string;
		tmhomeButton: string;
	}
	class wijtreemap_options implements WijTreeMapOptions {

		/** Selector option for auto self initialization. This option is internal. 
		* @ignore
		*/
		initSelector: string = ":jqmData(role='wijtreemap')";
		/** wijtreemap css, extend from $.wijmo.wijCSS 
		* @ignore
		*/
		wijCSS: WijTreeMapCSS = {
			wijtreemap: "",
			tmcontainer: "",
			tmitem: "",
			tmitemContainer: "",
			tmtitle: "",
			tmlabel: "",
			tmbuttons: "",
			tmbackButton: "",
			tmhomeButton: ""
		};
		/** wijtreemap css, extend from $.wijmo.wijCSS 
		* @ignore
		*/
		wijMobileCSS = {
		};
		/** 
		* A value that indicates the width of the treemap in pixels. 
		* @remarks
		* Note that this value overrides any value you may set in the <div> element that 
		* you use in the body of the HTML page.
		* @type {number}
		*/
		width: number = null;
		/** 
		* A value that indicates the height of the treemap in pixels. 
		* @remarks
		* Note that this value overrides any value you may set in the <div> element that 
		* you use in the body of the HTML page.
		* @type {number}
		*/
		height: number = null;
		/** A value that indicates the type of treemap to be displayed.
		  * @remarks Options are 'squarified', 'horizontal' and 'vertical'.
		  */
		type: string = "squarified";
		/***/
		/*
		showDepth: number = 0;*/
		/**
		* A value that indicates the array to use as a source that you can bind to treemap. 
		* @remarks
		* Use the valueBinding, labelBinding, colorBinding option, and bind values to treemap.
		*/
		data: any[] = null;
		/** A value that indicates the field to each item's value. Default value field is 'value' if valueBinding is not set. */
		valueBinding: string = null;
		/** A value that indicates the field to each item's label  Default label field is 'label' if labelBinding is not set.*/
		labelBinding: string = null;
		/** A value that indicates the field to each item's color  Default color field is 'color' if colorBinding is not set.*/
		colorBinding: string = null;
		/***/
		/*
		animation: any = null;*/
		/** A value that indicates whether to show the label. */
		showLabel: boolean = true;
		/**
		* A value that indicates a function which is used to format the label of treemap item.
		* @type {Function}
		*/
		labelFormatter: Function = null;
		/** A value that indicates whether to show the tooltip. */
		showTooltip: boolean = false;
		/** A value that indicates options of tooltip. 
		* @remarks Its value is wijtooltip's option, visit
		* http://wijmo.com/docs/wijmo/#Wijmo~jQuery.fn.-~wijtooltip.html for more details.*/
		tooltipOptions: any = null;
		/** A value that indicates whether to show the title. */
		showTitle: boolean = true;
		/** A value that indicates the height of the title. */
		titleHeight: number = 20;
		/**
		* A value that indicates a function which is used to format the title of treemap item.
		* @type {Function}
		*/
		titleFormatter: Function = null;
		/** 
		* A value that indicates the color of min value.
		*/
		minColor: string = null;
		/** 
		* A value that indicates min value. 
		* If this option is not set, treemap will calculate min value automatically.
		*/
		minColorValue: number = null;
		/** 
		* A value that indicates the color of mid value.
		*/
		midColor: string = null;
		/** 
		* A value that indicates mid value.
		* If this option is not set, treemap will calculate mid value automatically.
		*/
		midColorValue: number = null;
		/** 
		* A value that indicates the color of max value.
		*/
		maxColor: string = null;
		/** 
		* A value that indicates max value.
		* If this option is not set, treemap will calculate max value automatically.
		*/
		maxColorValue: number = null;
		/** 
		* A value that indicates whether to show back button after treemap drilled down.
		*/
		showBackButtons: boolean = false;
		//Events
		/**
		* This event fires before item is painting. User can create customize item in this event.
		* @event
		* @param {Object} e The jQuery.Event object.
		* @param {Object} data Data of item to be painted, set value to data.htmlTemplate to customize treemap item.
		*/
		itemPainting = null;
		/**
		* This event fires after item is painted.
		* @event
		*/
		itemPainted = null;
		/**
		* This event fires before treemap is painting.
		* @event
		* @param {Object} e The jQuery.Event object.
		* @param {JQuery} args The element of wijtreemap.
		*/
		painting = null;
		/**
		* This event fires after treemap is painted.
		* @event
		*/
		painted = null;
		/**
		* This event fires before drill down. This event can be cancelled, use "return false;" to cancel the event.
		* @event
		* @dataKey clickItemData The data of clicked item.
		* @dataKey currentData The data of current item.
		* @dataKey targetData The data of target item to be drilled down.
		*/
		drillingDown = null;
		/**
		* This event fires after drill down.
		* @event
		* @param {Object} e The jQuery.Event object.
		* @param {Object} data The data of item that is drilled down.
		*/
		drilledDown = null;
		/**
		* This event fires before roll up. This event can be cancelled, use "return false;" to cancel the event.
		* @event
		* @dataKey clickItemData The data of clicked item.
		* @dataKey currentData The data of current item.
		* @dataKey targetData The data of target item to be rolled up.
		*/
		rollingUp = null;
		/**
		* This event fires after roll up.
		* @event
		* @param {Object} e The jQuery.Event object.
		* @param {Object} data The data of item that is rolled up.
		*/
		rolledUp = null;
		/*
		click = null;
		*/
	};

	wijtreemap.prototype.options = $.extend(true, {}, wijmoWidget.prototype.options, new wijtreemap_options());
	$.wijmo.registerWidget(widgetName, wijtreemap.prototype);
}

/** @ignore */
interface JQuery {
	wijtreemap: JQueryWidgetFunction;
}