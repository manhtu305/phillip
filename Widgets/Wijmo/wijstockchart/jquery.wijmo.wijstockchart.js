﻿/*
* Depends:
*   jquery.js
*	raphael.js
*   jquery.wijmo.raphael.js
*	globalize.min.js
*	jquery.ui.widget.js
*	jquery.wijmo.wijchartcore.js
*
*/

(function ($) {
	"use strict";

	if (!window.Raphael) {
		return;
	}

	// extend group
	Raphael.fn.group = function () {

		var r = this,
		cfg = (arguments[0] instanceof Array) ? {} : arguments[0],
		items = (arguments[0] instanceof Array) ? arguments[0] : arguments[1];

		function Group(cfg, items) {
			var inst,
			set = r.set(items),
			group = r.raphael.vml ?
				document.createElement("group") :
				document.createElementNS("http://www.w3.org/2000/svg", "g");

			r.canvas.appendChild(group);

			inst = {
				push: function (item) {
					function pushOneRaphaelVector(it) {
						var i;
						if (it.type === 'set') {
							for (i = 0; i < it.length; i++) {
								pushOneRaphaelVector(it[i]);
							}
						} else {
							group.appendChild(it.node);
							set.push(it);
						}
					}
					pushOneRaphaelVector(item)
					return this;
				},
				getBBox: function () {
					return set.getBBox();
				},
				type: 'group',
				node: group,
				clip: function (clippath) {
					$(group).attr("clip-path", clippath);
				},
				wijRemove: function () {
					$(group).remove();
				}
			};

			return inst;
		}

		return Group(cfg, items);

	};

	if (!$.wijstockchart) {
		$.wijstockchart = {};
	}

	// extend a class to handle the x datetime axis data.
	$.extend($.wijstockchart, {
		datetimeUtil: function (datetimeArr) {
			var self = this;

			self.init = function () {
				var times = $.map(datetimeArr, function (n, i) {
					return $.toOADate(n);
				}),
					dic = {},
					length, i, key;

				self.times = times.sort();

				length = self.times.length;
				for (i = 0; i < length; i++) {
					key = self.times[i];
					dic[key] = i;
				}
				self.timeDic = dic;
			}

			self.init();

			self.getOATime = function (i) {
				return self.times[i];
			}

			self.getTime = function (i) {
				return $.fromOADate(self.times[i]);
			}
			self.getTimeIndex = function (datetime) {
				return self.timeDic[$.toOADate(datetime)];
			}

			self.getCount = function () {
				return self.times.length;
			}

			self.dispose = function () {
				self.times = null;
				self.timeDic = null;
			}
		}
	});

	$.widget("wijmo.wijstockchart", $.wijmo.wijchartcore, {
		// widget options    
		options: {
			/// <summary>
			/// A value that indicates which sharp of the stock element. 
			/// Possible value is hloc, hl, candlestick
			/// Type: String
			/// Default: "hloc"
			/// Code example:
			/// $("#stockchart").wijstockchart("option", "type", "hloc")
			/// </summary>
			type: "hloc",
			/// <summary>
			/// A value that indicates whether to show animation 
			///	and the duration for the animation.
			/// Default: {enabled:true, duration:400, easing: ">"}.
			/// Type: Object.
			/// Code example:
			///  $("#stockchart").wijstockchart({
			///      animation: {
			///			enabled: true, duration: 1000, easing: "<"
			///		}
			///  });
			/// </summary>
			animation: {
				/// <summary>
				/// A value that determines whether to show animation.
				/// Default: true.
				/// Type: Boolean.
				/// </summary>
				enabled: true,
				/// <summary>
				/// A value that indicates the duration for the animation.
				/// Default: 400.
				/// Type: Number.
				/// </summary>
				duration: 400,
				/// <summary>
				/// A value that indicates the easing for the animation.
				/// Default: ">".
				/// Type: string.
				/// </summary>
				easing: ">"
			},
			/// <summary>
			/// A value that indicates the max width of each stock element.
			/// Type: Number
			/// Default: 15
			/// Code example:
			/// $("#stockchart").wijstockchart("option", "maxWidth", 18)
			/// </summary>
			maxWidth: 15,
			/// <summary>
			/// A callback to format the stock element.
			/// Type: Function
			/// Default: null
			/// Code example:
			/// $("#stockchart").wijstockchart({ barFormater: function(){} })
			/// </summary>
			barFormater: null,
			/// <summary>
			/// Fires when the user clicks a mouse button.
			/// Default: null.
			/// Type: Function.
			/// Code example:
			/// Supply a function as an option.
			///  $("#stockchart").wijstockchart({mouseDown: function(e, data) { } });
			/// Bind to the event by type: wijstockchartmousedown
			/// $("#stockchart").bind("wijstockchartmousedown", function(e, data) {} );
			/// </summary>
			/// <param name="e" type="eventObj">
			/// jQuery.Event object.
			///	</param>
			/// <param name="data" type="Object">
			/// An object that contains all the series infos of the mousedown bubble.
			/// data.bubble: the Raphael object of the bubble.
			/// data.data: data of the series of the bubble.
			/// data.hoverStyle: hover style of series of the bubble.
			/// data.index: index of the bubble.
			/// data.label: label of the series of the bubble.
			/// data.legendEntry: legend entry of the series of the bubble.
			/// data.style: style of the series of the bubble.
			/// data.type: "bubble"
			/// data.x: the x value of the bubble.
			/// data.y: The y value of the bubble.
			/// data.y1: The y1 value of the bubble
			///	</param>
			mouseDown: null,
			/// <summary>
			/// Fires when the user releases a mouse button
			/// while the pointer is over the chart element.
			/// Default: null.
			/// Type: Function.
			/// Code example:
			/// Supply a function as an option.
			///  $("#stockchart").wijstockchart({mouseUp: function(e, data) { } });
			/// Bind to the event by type: wijstockchartmousedown
			/// $("#stockchart").bind("wijstockchartmouseup", function(e, data) {} );
			/// </summary>
			/// <param name="e" type="eventObj">
			/// jQuery.Event object.
			///	</param>
			/// <param name="data" type="Object">
			/// An object that contains all the series infos of the mouseup bubble.
			/// data.bubble: the Raphael object of the bubble.
			/// data.data: data of the series of the bubble.
			/// data.hoverStyle: hover style of series of the bubble.
			/// data.index: index of the bubble.
			/// data.label: label of the series of the bubble.
			/// data.legendEntry: legend entry of the series of the bubble.
			/// data.style: style of the series of the bubble.
			/// data.type: "bubble"
			/// data.x: the x value of the bubble.
			/// data.y: The y value of the bubble.
			/// data.y1: The y1 value of the bubble
			///	</param>
			mouseUp: null,
			/// <summary>
			/// Fires when the user first places the pointer over the chart element.
			/// Default: null.
			/// Type: Function.
			/// Code example:
			/// Supply a function as an option.
			///  $("#stockchart").wijstockchart({mouseOver: function(e, data) { } });
			/// Bind to the event by type: wijstockchartmouseover
			/// $("#stockchart").bind("wijstockchartmouseover", function(e, data) {} );
			/// </summary>
			/// <param name="e" type="eventObj">
			/// jQuery.Event object.
			///	</param>
			/// <param name="data" type="Object">
			/// An object that contains all the series infos of the mouseover bubble.
			/// data.bubble: the Raphael object of the bubble.
			/// data.data: data of the series of the bubble.
			/// data.hoverStyle: hover style of series of the bubble.
			/// data.index: index of the bubble.
			/// data.label: label of the series of the bubble.
			/// data.legendEntry: legend entry of the series of the bubble.
			/// data.style: style of the series of the bubble.
			/// data.type: "bubble"
			/// data.x: the x value of the bubble.
			/// data.y: The y value of the bubble.
			/// data.y1: The y1 value of the bubble
			///	</param>
			mouseOver: null,
			/// <summary>
			/// Fires when the user moves the pointer off of the chart element.
			/// Default: null.
			/// Type: Function.
			/// Code example:
			/// Supply a function as an option.
			///  $("#stockchart").wijstockchart({mouseOut: function(e, data) { } });
			/// Bind to the event by type: wijstockchartmouseout
			/// $("#stockchart").bind("wijstockchartmouseout", function(e, data) {} );
			/// </summary>
			/// <param name="e" type="eventObj">
			/// jQuery.Event object.
			///	</param>
			/// <param name="data" type="Object">
			/// An object that contains all the series infos of the mouseout bubble.
			/// data.bubble: the Raphael object of the bubble.
			/// data.data: data of the series of the bubble.
			/// data.hoverStyle: hover style of series of the bubble.
			/// data.index: index of the bubble.
			/// data.label: label of the series of the bubble.
			/// data.legendEntry: legend entry of the series of the bubble.
			/// data.style: style of the series of the bubble.
			/// data.type: "bubble"
			/// data.x: the x value of the bubble.
			/// data.y: The y value of the bubble.
			/// data.y1: The y1 value of the bubble
			///	</param>
			mouseOut: null,
			/// <summary>
			/// Fires when the user moves the mouse pointer
			/// while it is over a chart element.
			/// Default: null.
			/// Type: Function.
			/// Code example:
			/// Supply a function as an option.
			///  $("#stockchart").wijstockchart({mouseMove: function(e, data) { } });
			/// Bind to the event by type: wijstockchartmousemove
			/// $("#stockchart").bind("wijstockchartmousemove", function(e, data) {} );
			/// </summary>
			/// <param name="e" type="eventObj">
			/// jQuery.Event object.
			///	</param>
			/// <param name="data" type="Object">
			/// An object that contains all the series infos of the mousemove bubble.
			/// data.bubble: the Raphael object of the bubble.
			/// data.data: data of the series of the bubble.
			/// data.hoverStyle: hover style of series of the bubble.
			/// data.index: index of the bubble.
			/// data.label: label of the series of the bubble.
			/// data.legendEntry: legend entry of the series of the bubble.
			/// data.style: style of the series of the bubble.
			/// data.type: "bubble"
			/// data.x: the x value of the bubble.
			/// data.y: The y value of the bubble.
			/// data.y1: The y1 value of the bubble
			///	</param>
			mouseMove: null,
			/// <summary>
			/// Fires when the user clicks the chart element. 
			/// Default: null.
			/// Type: Function.
			/// Code example:
			/// Supply a function as an option.
			///  $("#stockchart").wijstockchart({click: function(e, data) { } });
			/// Bind to the event by type: wijstockchartclick
			/// $("#stockchart").bind("wijstockchartclick", function(e, data) {} );
			/// </summary>
			/// <param name="e" type="eventObj">
			/// jQuery.Event object.
			///	</param>
			/// <param name="data" type="Object">
			/// An object that contains all the series infos of the clicked bubble.
			/// data.bubble: the Raphael object of the bubble.
			/// data.data: data of the series of the bubble.
			/// data.hoverStyle: hover style of series of the bubble.
			/// data.index: index of the bubble.
			/// data.label: label of the series of the bubble.
			/// data.legendEntry: legend entry of the series of the bubble.
			/// data.style: style of the series of the bubble.
			/// data.type: "bubble"
			/// data.x: the x value of the bubble.
			/// data.y: The y value of the bubble.
			/// data.y1: The y1 value of the bubble
			///	</param>
			click: null
		},

		_paintChartArea: function () {
			var self = this,
				o = self.options,
				data = o.data;

			if (self._isSeriesDataEmpty()) {
				return;
			}

			// add the hl to data.y
			$.each(o.seriesList, function (i, n) {
				var data = n.data;
				data.y = [].concat(data.high).concat(data.low);
			})

			$.wijmo.wijchartcore.prototype._paintChartArea.apply(this, arguments);

			// delete data.y
			$.each(o.seriesList, function (i, n) {
				delete n.data.y;
			})
		},

		_isSeriesDataEmpty: function () {
			var self = this,
				sl = self.options.seriesList,
				type = self.options.type;

			if (!sl || sl.length === 0) {
				return true;
			}

			$.each(sl, function (idx, s) {
				if (type === "hloc" || type === "candlestick") {
					if (!s.data || ((!s.data.x || !s.data.high ||
						!s.data.low || !s.data.open || !s.data.close))) {
						return true;
					}
				}
				else {
					if (!s.data || ((!s.data.x || !s.data.high || !s.data.low))) {
						return true;
					}
				}
			});

			return false;
		},

		_validateSeriesData: function (series) {
			var type = this.options.type,
				data = series.data;

			if (type === "hl") {
				if (data.x === undefined && data.high === undefined &&
					data.low === undefined) {
					return false;
				}
			}
			else {
				if (data.x === undefined && data.high === undefined &&
					data.low === undefined && data.open === undefined &&
					data.close === undefined) {
					return false;
				}
			}

			return true;
		},

		_isStockChart: function () {
			return true;
		},

		_getLegendStyle: function (seriesStyle) {
			var style = seriesStyle.highLow;
			if (style) {
				style = $.extend({}, style);
				delete style.width;
				delete style.width;
				if (!style.stroke) {
					style.stroke = style.fill;
				}
				return style;
			}
			return seriesStyle;
		},

		_showHideSeries: function (seriesEle, type) {
			$.each(seriesEle, function (i, stockNode) {
				if (stockNode.hl) {
					stockNode.hl[type]();
				}

				if (stockNode.o) {
					stockNode.o[type]();
				}

				if (stockNode.c) {
					stockNode.c[type]();
				}

				if (stockNode.path) {
					stockNode.path[type]();
				}

				if (stockNode.h) {
					stockNode.h[type]();
				}

				if (stockNode.l) {
					stockNode.l[type]();
				}

				if (stockNode.oc) {
					stockNode.oc[type]();
				}
			});
		},

		_showSerieEles: function (seriesEle) {
			this._showHideSeries(seriesEle, "show");
		},

		_hideSerieEles: function (seriesEle) {
			this._showHideSeries(seriesEle, "hide");
		},

		_paintPlotArea: function () {
			var self = this,
				o = self.options;

			self.chartElement.wijstock({
				canvas: self.canvas,
				bounds: self.canvasBounds,
				tooltip: self.tooltip,
				widgetName: self.widgetName,
				axis: o.axis,
				seriesList: o.seriesList,
				seriesStyles: o.seriesStyles,
				seriesHoverStyles: o.seriesHoverStyles,
				seriesTransition: o.seriesTransition,
				showChartLabels: o.showChartLabels,
				textStyle: o.textStyle,
				chartLabelStyle: o.chartLabelStyle,
				chartLabelFormatString: o.chartLabelFormatString,
				shadow: o.shadow,
				disabled: o.disabled,
				animation: o.animation,
				culture: self._getCulture(),
				type: o.type,
				isXTime: self.axisInfo.x.isTime,
				mouseDown: $.proxy(self._mouseDown, self),
				mouseUp: $.proxy(self._mouseUp, self),
				mouseOver: $.proxy(self._mouseOver, self),
				mouseOut: $.proxy(self._mouseOut, self),
				mouseMove: $.proxy(self._mouseMove, self),
				click: $.proxy(self._click, self),
				timeUtil: self.timeUtil,
				maxWidth: o.maxWidth,
				barFormater: o.barFormater
			});
		},

		_setDefaultFill: function (style, prop, fill) {
			var p = style[prop] || {};
			if (!p.fill) {
				p.fill = fill;
			}
			style[prop] = p;
		},

		_create: function () {
			var self = this,
				o = self.options,
				type = o.type,
				defFill = self._getDefFill(),
				sopen, sclose, shighlow;

			// default some fills
			$.each(o.seriesStyles, function (idx, style) {
				var defaultFill = defFill[idx];
				if (type === "hl") {
					self._setDefaultFill(style, "highLow", defaultFill);
				}
				else if (type === "hloc") {
					self._setDefaultFill(style, "open", defaultFill);
					self._setDefaultFill(style, "close", defaultFill);
					self._setDefaultFill(style, "highLow", defaultFill);
				}
				else {
					self._setDefaultFill(style, "fallingClose", defaultFill);
					self._setDefaultFill(style, "raisingClose", defaultFill);
					self._setDefaultFill(style, "highLow", defaultFill);
				}

				if (!style.fill) {
					style.fill = defFill[idx];
				}
			});
			defFill = null;
			self._handleXData();
			$.wijmo.wijchartcore.prototype._create.apply(self, arguments);

			self.chartElement.addClass("wijmo-wijstockchart");
		},

		// handle data bind
		_bindData: function () {
			var self = this,
				o = self.options,
				dataSource = self.dataList || o.dataSource,
				seriesList = o.seriesList,
				shareData = o.data, sharedXList,
				isDataBind = false;

			$.each(seriesList, function (i, series) {
				var data = series.data, dataX,
					dataHigh, dataLow, dataOpen, dataClose,
					ds = series.dataList || series.dataSource || dataSource;

				if (ds && data) {
					dataX = data.x;

					if (dataX && dataX.bind) {
						data.x = self._getBindData(ds, dataX.bind);
						isDataBind = true;
					}
					else if (shareData && shareData.x && shareData.x.bind) {
						if (sharedXList === undefined) {
							sharedXList = self._getBindData(ds, shareData.x.bind);
							isDataBind = true;
						}
						data.x = sharedXList;
					}

					dataHigh = data.high;
					dataLow = data.low;
					dataOpen = data.open;
					dataClose = data.close;

					if (dataHigh && dataHigh.bind) {
						data.high = self._getBindData(ds, dataHigh.bind);
					}
					if (dataLow && dataLow.bind) {
						data.low = self._getBindData(ds, dataLow.bind);
					}
					if (dataOpen && dataOpen.bind) {
						data.open = self._getBindData(ds, dataOpen.bind);
					}
					if (dataClose && dataClose.bind) {
						data.close = self._getBindData(ds, dataClose.bind);
					}

				}
			});
			if (isDataBind) {
				self._handleXDaa();
			}
		},

		_setOptions: function (key, value) {
			if (key === "seriesList") {
				if (!value) {
					value = [];
				}

				ev = $.Event("beforeserieschange");
				if (self._trigger("beforeSeriesChange", ev, {
					oldSeriesList: o.seriesList,
					newSeriesList: value
				}) === false) {
					return false;
				}
				o.seriesList = value;
				self.seriesList = $.arrayClone(value);
				self._trigger("seriesChanged", null, value);
				self.seriesTransition = true;
				self._handleXData();
				self._init();
			}
			else if (key === "type" || key === "maxWidth") {
				self._init();
				$.wijchartcore.prototype._setOption.apply(self, arguments);
			}
			else {
				$.wijchartcore.prototype._setOption.apply(self, arguments);
			}
		},

		_handleXData: function () {
			var self = this,
				o = self.options,
				seriesList = o.seriesList,
				xValues = [];

			$.each(seriesList, function (i, series) {
				if ($.isArray(series.data.x)) {
					xValues = xValues.concat(series.data.x);
				}
			});


			self.timeUtil = new $.wijstockchart.datetimeUtil(xValues);

			$.each(seriesList, function (i, series) {
				if ($.isArray(series.data.x)) {
					series.data.x = $.map(series.data.x, function (n, i) {
						return self.timeUtil.getTimeIndex(n);
					})
				}
			});
		},

		destroy: function () {
			/// <summary>
			/// Remove the functionality completely. This will return the 
			/// element back to its pre-init state.
			/// Code example:
			/// $("#barchart").wijbarchart("destroy");
			/// </summary>
			var self = this,
				element = self.chartElement;

			element.removeClass("wijmo-wijstockchart");

			$.wijmo.wijchartcore.prototype.destroy.apply(this, arguments);

			if (self.timeUtil) {
				self.timeUtil.dispose();
			}

			element.data("fields", null);
		},

		_clearChartElement: function () {
			var self = this,
				o = self.options,
				fields = self.chartElement.data("fields"),
				chartElements, stockEles, pathSet, group;


			if (fields && fields.chartElements) {
				chartElements = fields.chartElements;
				stockEles = chartElements.stockEles;
				$.each(stockEles, function (i, StockEle) {
					$.each(StockEle, function (key, node) {
						if (node && node[0]) {
							node.wijRemove();
						}
						StockEle[key] = null;
					});
					stockEles[i] = null;
				});
				chartElements.stockEles = null;

				chartElements.group.wijRemove();
				chartElements.group = null;

				chartElements.pathSet.wijRemove();
				chartElements.pathSet = null;
			}

			$.wijmo.wijchartcore.prototype._clearChartElement.apply(self, arguments);
		},

		_calculateParameters: function (axisInfo, options) {
			$.wijmo.wijchartcore.prototype._calculateParameters.apply(this, arguments);

			// check for bar chart and x axis expansion
			if (axisInfo.id === "x") {
				var minor = options.unitMinor,
					autoMin = options.autoMin,
					autoMax = options.autoMax,
					adj = this._getStockAdjustment(axisInfo);

				if (this.timeUtil && this.timeUtil.getCount) {
					axisInfo.max = this.timeUtil.getCount() - 1;
				}

				if (adj === 0) {
					adj = minor;
				} else {
					if (minor < adj && minor !== 0) {
						adj = Math.floor(adj / minor) * minor;
					}
				}

				if (autoMin) {
					axisInfo.min -= adj;
				}

				if (autoMax) {
					axisInfo.max += adj;
				}

				this._calculateMajorMinor(options, axisInfo);
			}
		},
		_getStockAdjustment: function (axisInfo) {
			var self = this,
				o = self.options,
				max = axisInfo.max,
				min = axisInfo.min,
				length,
				series = o.seriesList;

			$.each(series, function (i, s) {
				if (!length) {
					length = s.data.x.length;
				}
				else {
					length = Math.max(s.data.x.length);
				}
			})

			return (max - min) / length;

		},



		_calculateMajorMinor: function (axisOptions, axisInfo) {
			var self = this,
				o = self.options,
				canvasBounds = self.canvasBounds,
				autoMajor = axisOptions.autoMajor,
				autoMinor = axisOptions.autoMinor,
				maxData = axisInfo.max,
				minData = axisInfo.min,
				isTime = axisInfo.isTime,
				tinc = axisInfo.tinc,
				formatString = axisInfo.annoFormatString,
				maxText = null,
				minText = null,
				sizeMax = null,
				sizeMin = null,
				mx = null,
				mn = null,
				prec = null,
				_prec = null,
				textStyle = null,
				dx = maxData - minData,
				width = 0,
				height = 0,
				nticks = 0,
				major = 0;

			if (autoMajor) {
				textStyle = $.extend(true, {}, o.textStyle,
					axisOptions.textStyle, axisOptions.labels.style);

				if (axisInfo.id === "x") {

					if (minData < 0) {
						minData = 0;
					}

					if (maxData > self.timeUtil.getCount() - 1) {
						maxData = self.timeUtil.getCount() - 1;
					}

					maxData = self.timeUtil.getOATime(maxData);
					minData = self.timeUtil.getOATime(minData);



					if (!self.formatString) {
						formatString = formatString || self._getTimeDefaultFormat(maxData, minData);
						self.formatString = formatString;
					}
					else {
						formatString = self.formatString;
					}

					maxText =
					Globalize.format($.fromOADate(maxData), formatString,
						self._getCulture());
					minText =
					Globalize.format($.fromOADate(minData), formatString,
						self._getCulture());

					mx = self._text(-1000, -1000, maxText).attr(textStyle);
					mn = self._text(-1000, -1000, minText).attr(textStyle);
					sizeMax = mx.wijGetBBox();
					sizeMin = mn.wijGetBBox();

					mx.wijRemove();
					mx = null;
					mn.wijRemove();
					mn = null;
				}

				else {
					prec = self._nicePrecision(dx);
					_prec = prec + 1;

					if (_prec < 0 || _prec > 15) {
						_prec = 0;
					}

					mx = self._text(-1000, -1000,
						$.round(maxData, _prec)).attr(textStyle);
					mn = self._text(-1000, -1000,
						$.round(minData, _prec)).attr(textStyle);

					sizeMax = mx.wijGetBBox();
					sizeMin = mn.wijGetBBox();

					mx.wijRemove();
					mx = null;
					mn.wijRemove();
					mn = null;
				}

				if (sizeMax.width < sizeMin.width) {
					sizeMax.width = sizeMin.width;
				}

				if (sizeMax.height < sizeMin.height) {
					sizeMax.height = sizeMin.height;
				}

				if (!self._isVertical(axisOptions.compass)) {
					// Add comments by RyanWu@20100907.
					// Subtract axisTextOffset because we must left
					// the space between major text and major rect.
					width = canvasBounds.endX - canvasBounds.startX -
						axisInfo.vOffset - axisInfo.axisTextOffset;
					major = width / sizeMax.width;

					if (Number.POSITIVE_INFINITY === major) {
						nticks = 0;
					} else {
						nticks = parseInt(major, 10);
					}
				} else {
					height = canvasBounds.endY - canvasBounds.startY -
						axisInfo.vOffset - axisInfo.axisTextOffset;
					major = height / sizeMax.height;

					if (Number.POSITIVE_INFINITY === major) {
						nticks = 0;
					} else {
						nticks = parseInt(major, 10);
					}
				}

				major = dx;
				if (nticks > 0) {
					dx /= nticks;
					axisInfo.tprec = self._nicePrecision(dx);
					major = self._niceNumber(2 * dx, -prec, true);

					if (major < dx) {
						major = self._niceNumber(dx, -prec + 1, false);
					}

					if (major < dx) {
						major = self._niceTickNumber(dx);
					}
					//					}
				}

				axisOptions.unitMajor = major;
			}

			if (autoMinor && axisOptions.unitMajor &&
				axisOptions.unitMajor === +axisOptions.unitMajor) {
				axisOptions.unitMinor = axisOptions.unitMajor / 2;
			}
		},


		_getTickText: function (text) {
			//return text;
			var self = this,
				formatString = self.formatString;

			return Globalize.format(self.timeUtil.getTime(text), formatString,
						self._getCulture());
		},

		_isHoverStock: function () {

		},


		_mouseMoveInsidePlotArea: function (e, mousePos) {
			var self = this,
				tooltip = self.tooltip,
				hint = self.options.hint,
				markers,
				virtualMarkers,
				idx = 0,
				p, point, valueX, valueY,
				s = null,
				dataObj = null,
				op = null,
				title = hint.title,
				content = hint.content,
				isTitleFunc = $.isFunction(title),
				isContentFunc = $.isFunction(content),
				distance = 0;

			if (tooltip) {
				op = tooltip.getOptions();
			}
			if (self.hoverLine) {
				if (self.isNewLine) {
					if (hint.enable && tooltip) {
						tooltip.hide();
					}
					self.isNewLine = false;
				}
				markers = self.hoverLine.lineMarkers;
				virtualMarkers = self.hoverLine.virtualMarkers;
				idx = -1;
				p = { x: 0, y: 0 };
				if (markers && markers.length) {
					$.each(markers, function (i, marker) {
						if (marker.removed) {
							return true;
						}
						var box = marker.wijGetBBox(),
							pos = box.x + box.width / 2,
							dis = Math.abs(pos - mousePos.left);
						if (i === 0 || dis < distance) {
							distance = dis;
							idx = i;
							p = {
								x: pos,
								y: box.y + box.height / 2
							};
						}
					});
					if (self.hoverPoint && self.hoverPoint.index === idx) {
						return;
					}
					if (idx > -1) {
						if (markers[idx].removed) {
							return;
						}
						point = $(markers[idx].node).data("wijchartDataObj");

						if (point) {
							if (self.hoverPoint && !self.hoverPoint.isSymbol) {
								if (!self.hoverPoint.removed) {
									self.hoverPoint.marker
										.wijAttr(self.hoverPoint.markerStyle);
									self.hoverPoint.marker.transform("s1");
								}
							}
							if (!point.isSymbol) {
								if (!point.marker.removed) {
									point.marker.wijAttr(point.markerHoverStyle);
								}
							}
						}

						self.hoverPoint = point;
						self.hoverVirtualPoint = virtualMarkers[idx];
					}
				} else {
					$.each(virtualMarkers, function (i, marker) {
						var dis = Math.abs(marker.x - mousePos.left);
						if (i === 0 || dis < distance) {
							distance = dis;
							idx = i;
							p = {
								x: marker.x,
								y: marker.y
							};
						}
					});
					if (self.hoverVirtualPoint && self.hoverVirtualPoint.index === idx) {
						return;
					}
					if (idx > -1) {
						self.hoverPoint = null;
						self.hoverVirtualPoint = virtualMarkers[idx];
					}
				}
				if (tooltip) {
					dataObj = self.hoverVirtualPoint;
					valueX = dataObj.valX;
					valueY = dataObj.valY;
					//dataObj = self.hoverPoint;
					//valueX = dataObj.valX;
					//valueY = dataObj.valY;
					if (isTitleFunc || isContentFunc) {
						if (isTitleFunc) {
							op.title = function () {
								var obj = {
									pointIndex: idx,
									//lineIndex: dataObj.lineSeries.index,
									lineIndex: self.hoverLine.index,
									x: valueX,
									y: valueY,
									//label: dataObj.lineSeries.label,
									label: self.hoverLine.label,
									data: dataObj,
									fmt: title
								},
									fmt = $.proxy(obj.fmt, obj),
									tit = fmt();
								return tit;
							};
						}
						if (isContentFunc) {
							op.content = function () {
								var obj = {
									pointIndex: idx,
									//lineIndex: dataObj.lineSeries.index,
									lineIndex: self.hoverLine.index,
									x: valueX,
									y: valueY,
									//label: dataObj.lineSeries.label,
									label: self.hoverLine.label,
									data: dataObj,
									fmt: content
								},
									fmt = $.proxy(obj.fmt, obj),
									con = fmt();
								return con;
							};
						}
					}
					s = $.extend({
						stroke: self.hoverLine.path.attr("stroke")
					}, hint.style);
					op.style.stroke = s.stroke;
					tooltip.showAt(p);
				}
			}

			$.wijmo.wijchartcore.prototype
				._mouseMoveInsidePlotArea.apply(self, arguments);
		},

		_mouseMoveOutsidePlotArea: function (e, mousePos) {
			var self = this;
			self._clearHoverState();
			$.wijmo.wijchartcore.prototype
				._mouseMoveOutsidePlotArea.apply(self, arguments);
		},

		_clearHoverState: function () {
			var self = this,
			tooltip = self.tooltip,
			hint = self.options.hint;

			if (hint.enable && tooltip) {
				tooltip.hide();
			}

			if (self.hoverLine) {
				if (!self.hoverLine.path.removed) {
					self.hoverLine.path.wijAttr(self.hoverLine.lineStyle);
					if (self.hoverPoint && !self.hoverPoint.isSymbol) {
						self.hoverPoint.marker.wijAttr(self.hoverPoint.markerStyle);
						//hoverPoint.marker.scale(1, 1);
						self.hoverPoint.marker.transform("s1");
					}
				}
			}
			self.hoverLine = null;
			self.hoverPoint = null;
			self.hoverVirtualPoint = null;
		},


		_getTooltipText: function (fmt, target) {
			var tar = $(target.node),
				dataObj;

			dataObj = tar.data("wijchartDataObj");
			return $.proxy(fmt, dataObj)();
		}
	});

} (jQuery));

(function ($) {
	"use strict";
	
	if (!window.Raphael) {
		return;
	}

	$.fn.extend({
		wijstock: function (options) {
			var addClass = $.wijraphael.addClass,
				getTranslation = $.wijchart.getTranslation,
				element = this,
				canvas = options.canvas,
				widgetName = options.widgetName,
				seriesList = $.arrayClone(options.seriesList),
				nSeries = seriesList.length,
				seriesStyles = [].concat(options.seriesStyles.slice(0, nSeries)),
				seriesHoverStyles = [].concat(
					options.seriesHoverStyles.slice(0, nSeries)),
				bounds = options.bounds,
				animation = options.animation,
				animated = animation && animation.enabled,
				type = options.type,
				startLocation = { x: bounds.startX, y: bounds.startY },
				width = bounds.endX - startLocation.x,
				height = bounds.endY - startLocation.y,
				xaxis = options.axis.x,
				yaxis = options.axis.y,
				disabled = options.disabled,
				mouseDown = options.mouseDown,
				mouseUp = options.mouseUp,
				mouseOver = options.mouseOver,
				mouseOut = options.mouseOut,
				mouseMove = options.mouseMove,
				click = options.click,
				fields = element.data("fields") || {},
				chartElements = fields.chartElements || {},
				minX = xaxis.min,
				minY = yaxis.min,
				maxX = xaxis.max,
				maxY = yaxis.max,
				kx = width / (maxX - minX),
				ky = height / (maxY - minY),
				culture = options.culture,
				isXTime = options.isXTime, 
				pathSet = canvas.set(),
				timeUtil = options.timeUtil,
				maxWidth = options.maxWidth,
				barFormater = options.barFormater;

				function paintStock(series, seriesStyle, seriesHoverStyle){
					var data = series.data,
						length = Math.min(data.x.length, data.high.length, data.low.length),
						stockwidth = width/length,						
						i = 0,
						stockEles, pathNode,
						dataObj = $.extend({type: "stock", stockType: type}, series), 
						seriesEls = [];

						if(data.open && data.close){
							length = Math.min(length, data.open.length, data.close.length);
						}

						for(i; i<length; i++){
							stockEles = paintStockGraphic(data, i, stockwidth, seriesStyle, seriesHoverStyle);
							if(stockEles.path){
								pathNode = $(stockEles.path.node);
								addClass(pathNode, "wijchart-canvas-object wijstockchart");
								if(type !== "hl"){
									dataObj = $.extend({ 
										high: data.high[i], 
										low: data.low[i],
										open: data.open[i],
										close: data.close[i]
									}, dataObj)
								}
								else{
									dataObj = $.extend({
										high: data.high[i],
										low: data.low[i]
									}, dataObj);
								}

								dataObj = $.extend({
									index: i,
									x: data.x[i],
									stockEles: stockEles,
									style: seriesStyle,
									seriesHoverStyle: seriesHoverStyle
								}, dataObj)

								pathNode.data("wijchartDataObj", dataObj);
							}
							seriesEls.push(stockEles);
						}
						return seriesEls;
				}

				function bindLiveEvents(){
					var isFunction = $.isFunction;
					$(".wijstockchart", element)
					.live("mousedown." + widgetName, function (e) {
						if (disabled) {
							return;
						}

						if (isFunction(mouseDown)) {
							var target = $(e.target),
								dataObj;
							dataObj = target.data("wijchartDataObj");
							mouseDown.call(element, e, dataObj);
							dataObj = null;
						}
					})
					.live("mouseup." + widgetName, function (e) {
						if (disabled) {
							return;
						}
						if (isFunction(mouseUp)) {
							var target = $(e.target),
								dataObj;							
							dataObj = target.data("wijchartDataObj");
							mouseUp.call(element, e, dataObj);
							dataObj = null;
						}
					})
					.live("mouseover." + widgetName, function (e) {
						if (disabled) {
							return;
						}
						if (isFunction(mouseOver)) {
							var target = $(e.target),
								dataObj;							
							dataObj = target.data("wijchartDataObj");
							mouseOver.call(element, e, dataObj);
							dataObj = null;
						}
					})
					.live("mouseout." + widgetName, function (e) {
						if (disabled) {
							return;
						}
						var target = $(e.target),
							dataObj, bar;						
						dataObj = target.data("wijchartDataObj");
						bar = dataObj.bar;					

						if (isFunction(mouseOut)) {
							mouseOut.call(element, e, dataObj);
						}
						dataObj = null;
					})
					.live("mousemove." + widgetName, function (e) {
						if (disabled) {
							return;
						}

						var target = $(e.target),
							dataObj, bar;						
						dataObj = target.data("wijchartDataObj");
						bar = dataObj.bar;					

						if (isFunction(mouseMove)) {
							mouseMove.call(element, e, dataObj);
						}
					})
					.live("click." + widgetName, function (e) {
						if (disabled) {
							return;
						}

						if (isFunction(click)) {
							var target = $(e.target),
								dataObj;
							dataObj = target.data("wijchartDataObj");
							click.call(element, e, dataObj);
						}
					});
				}

				function unbindLiveEvents(){
					$(".wijstockchart", element).die(widgetName).die( "." + widgetName);
				}

				function paintStockGraphic(data, index, stockWidth, seriesStyle, seriesHoverStyle){
					var x = data.x[index], 
						high = data.high[index],
						low = data.low[index],
						open = data.open? data.open[index] : 0,
						close = data.close? data.close[index] : 0,
						hlStyle, cStyle, oStyle, hlwidth, owidth, cwidth, 
						originX = bounds.startX,
						originY = bounds.endY,						
						ox, oy, cx, cy, hlwidth, owidth, cwidth, hx, hy, lx, ly, stockEles,
						hasOC = type !== "hl", halfWidth,
						fallingCloseStyle, raisingCloseStyle, callbackObj,
						unchangeCloseStyle, extendStyle, closeWidth;

						if(isXTime){
							x = timeUtil.getTimeIndex(x);
							//x = $.toOADate(x);
						}

						oStyle = seriesStyle.open;
						cStyle = seriesStyle.close;
						hlStyle = seriesStyle.highLow;

						fallingCloseStyle = seriesStyle.fallingClose;
						raisingCloseStyle = seriesStyle.raisingClose;
						unchangeCloseStyle = seriesStyle.unchangeClose;
						

						hlwidth = hlStyle["stroke-width"] || 0;

						hx = (x-minX) * kx + startLocation.x - hlwidth/2;
						hy = originY - (high-minY)*ky;
						lx = hx;
						ly = originY - (low-minY)*ky;

						if(hasOC){
							if(type === "hloc"){							
								owidth = oStyle["stroke-width"] || 0;
								cwidth = cStyle["stroke-width"] || 0;							
							}
							else{
								if( open > close){
									owidth = cwidth = fallingCloseStyle["stroke-width"] || 0;
									closeWidth = fallingCloseStyle.width || 0;
								}
								else{
									owidth = cwidth = raisingCloseStyle["stroke-width"] || 0;
									closeWidth = raisingCloseStyle.width || 0;
								}
							}

							if(type === "candlestick"){
								stockWidth = stockWidth - (owidth+cwidth)/2;
								stockWidth = stockWidth < 2? 2 : stockWidth;
							}

							stockWidth = Math.min(maxWidth, stockWidth);
							halfWidth = (stockWidth / 2) * 0.8;
							
							if (type === "candlestick" && halfWidth > 0) {
								halfWidth = Math.min(closeWidth / 2, halfWidth);
							}

							halfWidth = halfWidth < 1? 1: halfWidth;

							ox = hx - halfWidth;
							oy = originY - (open - minY)*ky;
							cx = hx + halfWidth;
							cy = originY - (close - minY)*ky;

							oy = Math.max(oy, hy + owidth/2);
							oy = Math.min(oy, ly - owidth/2);

							cy = Math.max(cy, hy + cwidth/2);
							cy = Math.min(cy, ly - cwidth/2);

							
						}

					if(type !== "candlestick"){
						if (unchangeCloseStyle && open === close) {
							extendStyle = {fill: unchangeCloseStyle.fill, stroke: unchangeCloseStyle.stroke};
						}

						if (fallingCloseStyle && open > close) {
							extendStyle = {fill: fallingCloseStyle.fill, stroke: fallingCloseStyle.stroke};
						}

						if (raisingCloseStyle && open < close) {
							extendStyle = {fill: raisingCloseStyle.fill, stroke: raisingCloseStyle.stroke};
						}

						if(extendStyle){
							hlStyle = $.extend({}, hlStyle, extendStyle);
							if (oStyle) {
								oStyle = $.extend({}, oStyle, extendStyle);
							}
							if (cStyle) {
								cStyle = $.extend({}, cStyle, extendStyle);
							}
						}
					}

					if(type==="hloc"){
						

						if (raisingCloseStyle && close > open) {
							
						}
										
						stockEles = paintHLOC({h:{x:hx, y:hy}, l:{x:lx, y:ly}, o:{x:ox, y:oy}, c:{x:cx, y:cy}});
						stockEles.o.attr(oStyle);
						stockEles.c.attr(cStyle);
					}
					else if(type === "hl"){
						stockEles = paintHL({h:{x:hx, y:hy}, l:{x:lx, y:ly}});
					}
					else {
						stockEles = paintCandlestick({h:{x:hx, y:hy}, l:{x:lx, y:ly}, o:{x:ox, y:oy}, c:{x:cx, y:cy}})
						stockEles.h.attr(hlStyle);
						stockEles.l.attr(hlStyle);
						stockEles.oc.attr(stockEles.isFalling? fallingCloseStyle : raisingCloseStyle);
					}
					if(stockEles.hl){
						stockEles.hl.attr(hlStyle);
					}

					if (barFormater && $.isFunction(barFormater)) {
						callbackObj = {
							index: index
						}

						if(type === "hloc"){
							callbackObj = $.extend({
								data: {high: high, low: low, open: open, close: close},
								eles: {
									highLow: stockEles.hl,
									open: stockEles.o,
									close: stockEles.c
								},
								style: {
									highLow: hlStyle,
									open: oStyle,
									close: cStyle
								}
							}, callbackObj);
						}
						else if (type === "hl") {
							callbackObj = $.extend({
								data:{
									high: high,
									low: low
								},
								eles: {
									highLow: stockEles.hl
								},
								style: {
									highLow: hlStyle
								}
							}, callbackObj);
						}
						else {
							callbackObj = $.extend({
								data: {high: high, low: low, open: open, close: close},
								eles: {
									high: stockEles.h,
									low: stockEles.l,
									openClose: stockEles.oc
								},
								style:{
									highLow: hlStyle,
									fallingClose: fallingCloseStyle,
									raisingClose: raisingCloseStyle
								}
							}, callbackObj);
						}

						barFormater.call(this, callbackObj)
					}

					pathSet.push(stockEles.path);

					return stockEles;
				}

				function paintHLOC(opt){
						var hlEle,oel, cel, path,
							h = opt.h,
							l = opt.l,
							o = opt.o,
							c = opt.c,
							hlpath = ["M", h.x,",", h.y, "V", l.y],
							opath = ["M", o.x,",", o.y, "H", h.x],
							cpath = ["M", h.x,",", c.y, "H", c.x];

						hlEle = canvas.path(hlpath.join());
						oel = canvas.path(opath.join());
						cel = canvas.path(cpath.join());
						path = canvas.path(hlpath.concat(opath).concat(cpath).join());

						path.attr({opacity:0});

						return {hl: hlEle, o: oel, c: cel, path: path};
				}

				function paintHL(opt){
					var h = opt.h,
						l = opt.l, hlEl;

					hlEl = canvas.path(["M", h.x,",", h.y, "V", l.y].join());					
					return {hl: hlEl, path: hlEl};
				}
				

				function paintCandlestick(opt){
					var h = opt.h,
						l = opt.l,
						o = opt.o,
						c = opt.c,
						ocel, hel, lel, topY, bottomY, isfalling, hpath, lpath, ocpath, path;

					//hlec = canvas(["M", h.x, ",", h.y, "V", ])

					if(c.y < o.y){
						topY = c.y;
						bottomY = o.y;
						isfalling = false;
					}
					else{
						topY = o.y;
						bottomY = c.y;
						isfalling = true;
					}

					hpath = ["M", h.x, ",", h.y, "V", topY];
					lpath = ["M", l.x, ",", l.y, "V", bottomY];
					ocpath = ["M", o.x, ",", topY, "H", c.x, "V", bottomY, "H", o.x, "V", topY, "Z" ];
					hel = canvas.path(hpath.join());
					lel = canvas.path(lpath.join());
					ocel = canvas.path(ocpath);
					path = canvas.path(hpath.concat(lpath).concat(ocpath));
					path.attr({opacity:0});
					return {h: hel, l: lel, oc: ocel, isFalling: isfalling, path: path};
				}

				function handleStyle(style, notHandleWidth){
					var s;

					if(!style){
						return style;
					}

					if(!style.stroke){
						style.stroke = style.fill;
					}

					if(style.width && notHandleWidth !== true){
						s = $.extend({}, style, {"stroke-width": style.width});
						delete s.width;
					}
					else if(style.height && notHandleWidth !== true){
						s = $.extend({}, style, {"stroke-width": style.height});
						delete s.height;
					}				
					else{
						return style;
					}

					return s;
				}

				function extendStyles(style){
					style = $.extend(true, {}, style);
					if (style) {
						style.highLow = handleStyle(style.highLow);
						style.close = handleStyle(style.close);
						style.open = handleStyle(style.open);

						style.fallingClose = handleStyle(style.fallingClose, true);
						style.raisingClose = handleStyle(style.raisingClose, true);						
						style.unchangeClose = handleStyle(style.unchangeClose);
					}
					return style;
				}

				function paintStocks(){
					var seriesEls = [], seriesEl,
						stockEles = chartElements.stockEles || [],
						trackers = [];
					$.each(seriesList, function(i, series){
						var seriesStyle = extendStyles(seriesStyles[i]),
							seriesHoverStyle = seriesHoverStyles[i];
						seriesEl = paintStock(series, seriesStyle, seriesHoverStyle);
						stockEles = stockEles.concat(seriesEl);	
						seriesEls.push(seriesEl);					
					});
					fields.seriesEles = seriesEls;
					chartElements.stockEles = stockEles;
					fields.chartElements = chartElements;
					$.each(stockEles, function(i, stockEl){
						if(stockEl.path){
							trackers.push(stockEl.path);
						}
					});
					fields.trackers = trackers;
				}

				function createClip(x, y, width, height){
					var node = canvas.rect(0, 0, 1, 1), clipPath, clip, css;
					node.attr("clip-rect", [x,y,width,height].join(","));					
					if(Raphael.svg){
						clipPath = $(node.node).attr("clip-path");
						clip = node.clip;
						node.wijRemove();
						return {path: clipPath, clip: clip};
					}
					else {
						css = $(node.node).parent().attr("style");
						$(node.node).parent().remove();
						node.wijRemove();
						return css;
					}
				}

				function playAnimation(){
					var group = canvas.group(), clip, clipPath, pathID, clipRect,
						x = bounds.startX,
						y = bounds.startY,
						set = canvas.set(),
						jQEasing = {
							">" : "easeInCubic",
							"<" : "easeOutCubic",
							"<>": "easeInOutCubic",
							"backIn": "easeInBack",
							"backOut": "easeOutBack",
							"elastic": "easeOutElastic",
							"bounce": "easeOutBounce"
						};
					if(animated){
						$.each(chartElements.stockEles, function(i, stockEl){
							$.each(stockEl, function(key, obj){
								if (obj.node) {
									group.push(obj);
									set.push(obj);
								}
							})
						});

						animation = $.extend({}, animation);
						if (animation.easing && jQEasing[animation.easing]) {
							animation.easing = jQEasing[animated.easing]
						}
						//pathSet.hide();

						clip = createClip(bounds.startX, bounds.startY, 0, height);

						if(Raphael.svg){							
							clipPath = clip.path;
							group.clip(clipPath);
							$(clip.clip).animate({width: width}, $.extend({step: function(val){
								$(clip.clip).attr("width", val);
							}, complete: function(){
								//pathSet.show();
							}}, animation));
						}
						else{
							$(group.node).attr("style",clip);
							$(group.node).animate({value: width}, $.extend({step: function(val){
								// handle cliprect
								group.node.style.clip = Raphael.format("rect({0}px,{1}px,{2}px,{3}px)", y, x+val, y+height, x);

							},complete: function(){
								//pathSet.show();
							}}, animation));
						}																
						
						fields.chartElements.group = group;
						fields.chartElements.pathSet = pathSet;
					}
				}


				unbindLiveEvents();
				paintStocks();
				element.data("fields", fields);
				bindLiveEvents();
				playAnimation();
		}

	});	
}(jQuery));