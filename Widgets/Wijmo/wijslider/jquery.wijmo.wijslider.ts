/// <reference path="../Base/jquery.wijmo.widget.ts" />
/*globals window,document,jQuery*/
/*
* Depends:
*  jquery.js
*  jquery.ui.js
*  jquery.wijmo.wijutil.js
*/

module wijmo.slider {

	var $ = jQuery,
		widgetName = "wijslider",
		uiSliderHandleClass = "ui-slider-handle";

	/** @widget */
	export class JQueryUISlider extends JQueryUIWidget {
		_super: Function;
		_superApply: Function;
		options: any;
		element: JQuery;
		range: JQuery;
		_valueMin() { return $.ui.slider.prototype._valueMin.apply(this, arguments); }
		_valueMax() { return $.ui.slider.prototype._valueMax.apply(this, arguments); }
		_refreshValue() { return $.ui.slider.prototype._refreshValue.apply(this, arguments); }
		_create() { return $.ui.slider.prototype._create.apply(this, arguments); }
		_createHandles() { return $.ui.slider.prototype._createHandles.apply(this, arguments); }
		/** This option can be used to specify multiple handles.
		 * @remarks
		 *    If the range option is set to true, the length of values should be 2.
		 * @param {?number} index the first value.
		 * @param {?number} val the second value. 
		 */
		values(index?: number, val?: number) {
			return $.ui.slider.prototype.values.apply(this, arguments);
		}
		/** Determines the value of the slider, if there's only one handle. 
		 * @remarks
		 *   If there is more than one handle, determines the value of the first handle.
		 * @param {?number} val the specified value.
		 */
		value(val?: number) {
			return $.ui.slider.prototype.value.apply(this, arguments);
		}

		_slide(event, index, newValue) { return $.ui.slider.prototype._slide.apply(this, arguments); }
		_change(event, index) { return $.ui.slider.prototype._change.apply(this, arguments); }
		_normValueFromMouse(position) { return $.ui.slider.prototype._normValueFromMouse.apply(this, arguments); }
		elementSize: { width: number; height: number; };
		elementOffset: { left: number; top: number; };
	}

	/** @widget 
	  * @extends jQuery.ui.slider
	  */
	export class wijslider extends JQueryUISlider {

		_dragFillTarget: boolean;
		_dragFillStart: number;
		_rangeValue: number;
		_oldValue1: number;
		_oldValue2: number;
		_oldX: number;
		_oldY: number;
		_intervalID: any;
		_oriStyle: any;
		_animateOff: boolean;
		_container: JQuery;
		_lastChangedValue: number;
		_handleIndex: number;

		disabledDiv: JQuery;
		handles: any;

		_setOption(key, value) {
			var oldDisabled = this._isDisabled(), newDisabled;
			if (key === "values") {
				value = this._pre_set_values(value);
				this._parseToNumberArray(value);
				this.options[key] = value;
				this._setValuesOption();
			} else if (key === "disabled") {
				this.options[key] = value;
				newDisabled = this._isDisabled();
				if (oldDisabled === newDisabled) return;

				this._toggleDisabledSlider();
			} else {
				value = this._parseToNumberValue(key, value);
				$.ui.slider.prototype._setOption.call(this, key, value);
			}

			//Add for support disabled option at 2011/7/8
			if (key === "range") {
				this._setRangeOption(value);
			}
			//end for disabled option
			return this;
		}

		_toggleDisabledSlider() {
			var wijCSS = this.options.wijCSS, disabled = this._isDisabled(),
				decreBtn = this._container.find("." + wijCSS.wijmoSliderDecButton),
				increBtn = this._container.find("." + wijCSS.wijmoSliderIncButton);

			this.element.toggleClass(wijCSS.stateDisabled, disabled);
			if (decreBtn && decreBtn.length > 0)
				decreBtn.toggleClass(wijCSS.stateDisabled, disabled);
			if (increBtn && increBtn.length > 0)
				increBtn.toggleClass(wijCSS.stateDisabled, disabled);
		}

		_setRangeOption(value) {
			var self = this, o = self.options, valueMin;

			if (value === true) {
				if (!o.values || (o.values && o.values.length === 0)) {
					valueMin = self._valueMin();
					o.values = [valueMin, valueMin];
				} else if (o.values.length && o.values.length !== 2) {
					valueMin = o.values[0];
					o.values = [valueMin, valueMin];
				}
				self._refresh_handle(2);
			}
			self._re_createRange();
			self._refreshValue();
		}

		_setValuesOption() {
			var self = this, valsLength = 0, i;

			self._animateOff = true;
			self._refreshValue();
			if ($.isArray(self.options.values)) {
				valsLength = self.options.values.length;
			}
			for (i = 0; i < valsLength; i++) {
				self._change(null, i);
			}
			self._animateOff = false;
		}

		_re_createRange() {
			var self = this, o = self.options, wijCSS = o.wijCSS;

			if (self.range) {
				self.range.remove();
				//update for jquery ui 1.10 upgrade
				if ($(".ui-slider-range", self.element).length > 0) {
					$(".ui-slider-range", self.element).remove();
				}
			}
			if (o.range) {
				self.range = $("<div></div>").appendTo(self.element).addClass(wijCSS.uiSliderRange)
					.addClass(wijCSS.header);

				if (o.range === "min") {
					self.range.addClass(wijCSS.uiSliderRangeMin);
				} else if (o.range === "max") {
					self.range.addClass(wijCSS.uiSliderRangeMax);
				}
			}
		}

		_pre_set_values(values) {
			var self = this, o = self.options, newHandleLen = 1, value;

			newHandleLen = values && values.length ? values.length : 1;
			if (o.range === true) {
				if (!values || (values && values.length === 0)) {
					value = self._valueMin();
					values = [value, value];
				} else if (values.length && values.length !== 2) {
					value = values[0];
					values = [value, value];
				}
				newHandleLen = 2;
			}
			self._refresh_handle(newHandleLen);
			self._re_createRange();
			return values;
		}

		_createHandles() {
			var wijCSS = this.options.wijCSS;
			super._createHandles();
			this.handles.each(function (i) {
				$(this).removeClass("ui-slider-handle").removeClass("ui-state-default").removeClass("ui-corner-all")
					.addClass(uiSliderHandleClass).addClass(wijCSS.uiSliderHandle)
					.addClass(wijCSS.stateDefault).addClass(wijCSS.cornerAll);
			});
		}

		_refresh_handle(newHandleLen) {
			var self = this, wijCSS = this.options.wijCSS, handleLen = self.handles.length,
				handle = "<a class='" + uiSliderHandleClass + " " + wijCSS.uiSliderHandle + " " +
				wijCSS.stateDefault + " " + wijCSS.cornerAll + "' href='#'></a>",
				handles = [], i;

			if (handleLen !== newHandleLen) {
				if (newHandleLen > handleLen) {
					for (i = handleLen; i < newHandleLen; i++) {
						handles.push(handle);
					}
					self.element.append(handles.join(""));
				} else {
					self.element.find("." + uiSliderHandleClass).eq(newHandleLen - 1)
						.nextAll().remove();
				}
				self.handles = self.element.find("." + uiSliderHandleClass);
			}
		}

		_initState() {
			this._dragFillTarget = false;
			this._dragFillStart = 0;
			this._rangeValue = 0;
			this._oldValue1 = 0;
			this._oldValue2 = 0;
			this._oldX = 0;
			this._oldY = 0;
		}

		_create() {
			///	<summary>
			///		Creates Slider DOM elements and binds interactive events.
			///	</summary>
			var self = this, element = self.element, o = self.options, wijCSS = o.wijCSS, jqElement,
				val, vals, idx, len, ctrlWidth, ctrlHeight, container, decreBtn, increBtn, thumb;

			$.each(o, (key, optionVal) => {
				if (key === "values") {
					self._parseToNumberArray(optionVal);
				} else {
					o[key] = self._parseToNumberValue(key, optionVal);
				}
			});

			// enable touch support:
			if (window.wijmoApplyWijTouchUtilEvents) {
				$ = window.wijmoApplyWijTouchUtilEvents($);
			}

			this._initState();
			self._oriStyle = element.attr("style");

			if (element.is(":input")) {
				if (o.orientation === "horizontal") {
					jqElement = $("<div></div>").width(element.width()).appendTo(document.body);
				} else {
					jqElement = $("<div></div>").height(element.height()).appendTo(document.body);
				}

				val = element.val();
				if (val !== "") {
					try {
						vals = val.split(";");
						len = vals.length;

						if (len > 0) {
							for (idx = 0; idx < len; idx++) {
								vals[idx] = parseInt(vals[idx], 10);
							}

							if (len === 1) {
								o.value = vals[0];
							} else {
								o.values = vals;
							}
						}
					} catch (e) {
					}
				}

				element.data(self.widgetName, jqElement.wijslider(o))
					.after($(document.body).children("div:last")).hide();

				if (this._isDisabled()) {
					this._toggleDisabledSlider();
				}
				return;
			}

			super._create();

			if (self.range) {
				self.range.removeClass("ui-slider-range")
					.addClass(wijCSS.uiSliderRange);
			}

			element.removeClass("ui-slider").removeClass("ui-slider-" + this.options.orientation)
				.removeClass("ui-widget").removeClass("ui-widget-content").removeClass("ui-corner-all")
				.addClass(wijCSS.uiSlider)
				.addClass(o.orientation === "horizontal" ? wijCSS.uiSliderHorizontal : wijCSS.uiSliderVertical)
				.addClass(wijCSS.widget).addClass(wijCSS.content).addClass(wijCSS.cornerAll);

			element.data("originalStyle", element.attr("style"));
			element.data("originalContent", element.html());

			ctrlWidth = element.width();
			ctrlHeight = element.height();
			container = $("<div></div>");

			if (o.orientation === "horizontal") {
				container.addClass(wijCSS.wijmoSliderHorizontal);
			} else {
				container.addClass(wijCSS.wijmoSliderVertical);
			}
			container.width(ctrlWidth).height(ctrlHeight);

			decreBtn = $("<a><span></span></a>").addClass(wijCSS.wijmoSliderDecButton);
			increBtn = $("<a><span></span></a>").addClass(wijCSS.wijmoSliderIncButton);
			element.wrap(container).before(decreBtn).after(increBtn);

			self._container = element.parent();
			self._attachClass();
			thumb = element.find("." + uiSliderHandleClass);

			self._adjustSliderLayout(decreBtn, increBtn, thumb);

			if (this._isDisabled()) {
				this._toggleDisabledSlider();
			}

			//update for visibility change
			if (self.element.is(":hidden") && self.element.wijAddVisibilityObserver) {
				self.element.wijAddVisibilityObserver(function () {
					self._refreshSlider();
					if (self.element.wijRemoveVisibilityObserver) {
						self.element.wijRemoveVisibilityObserver();
					}
				}, "wijslider");
			}

			self._bindEvents();
		}

		/** This option can be used to specify multiple handles.
		 * @remarks
		 *    If the range option is set to true, the length of values should be 2.
		 * @param {?number} index the first value.
		 * @param {?number} val the second value. 
		 */
		values(index?: number, val?: number) {
			return $.ui.slider.prototype.values.apply(this, arguments);
		}

		/** Determines the value of the slider, if there's only one handle. 
		 * @remarks
		 *   If there is more than one handle, determines the value of the first handle.
		 * @param {?number} val the specified value.
		 */
		value(val?: number) {
			return $.ui.slider.prototype.value.apply(this, arguments);
		}

		/** Refresh the wijslider widget. */
		refresh() {
			// note: when the original element's width is setted by percent
			// it's hard to adjust the position and size, so first destroy then
			// recreate
			//this._refresh();
			var widgetObject = this.element.data("wijslider"),
				wijmoWidgetObject = this.element.data("wijmo-wijslider");

			this.destroy();

			this.element.data("wijslider", widgetObject);
			this.element.data("wijmo-wijslider", wijmoWidgetObject);

			this._create();
		}

		_refreshSlider() {
			var self = this, wijCSS = self.options.wijCSS, increBtn, decreBtn, thumb;

			decreBtn = self._container.find("." + wijCSS.wijmoSliderDecButton);
			increBtn = self._container.find("." + wijCSS.wijmoSliderIncButton);
			thumb = self._container.find("." + uiSliderHandleClass);

			self._adjustSliderLayout(decreBtn, increBtn, thumb);
			self._refreshValue();
		}

		_adjustSliderLayout(decreBtn, increBtn, thumb) {
			var self = this, element = self.element, o = self.options, ctrlWidth, ctrlHeight,
				decreBtnWidth, decreBtnHeight, increBtnWidth, increBtnHeight, thumbWidth, thumbHeight,
				dbtop, ibtop, dbleft, ibleft;

			ctrlWidth = self._container.width();
			ctrlHeight = self._container.height();
			decreBtnWidth = decreBtn.outerWidth();
			decreBtnHeight = decreBtn.outerHeight();
			increBtnWidth = increBtn.outerWidth();
			increBtnHeight = increBtn.outerHeight();

			thumbWidth = thumb.outerWidth();
			thumbHeight = thumb.outerHeight();

			if (o.orientation === "horizontal") {
				dbtop = ctrlHeight / 2 - decreBtnHeight / 2;
				decreBtn.css("top", dbtop).css("left", 0);
				ibtop = ctrlHeight / 2 - increBtnHeight / 2;
				increBtn.css("top", ibtop).css("right", 0);

				element.css("left", decreBtnWidth + thumbWidth / 2 - 1)
					.css("top", ctrlHeight / 2 - element.outerHeight() / 2)
					.width(ctrlWidth - decreBtnWidth - increBtnWidth - thumbWidth - 2);
			} else {
				dbleft = ctrlWidth / 2 - decreBtnWidth / 2;
				decreBtn.css("left", dbleft).css("top", 0);
				ibleft = ctrlWidth / 2 - increBtnWidth / 2;
				increBtn.css("left", ibleft).css("bottom", 0);

				element.css("left", ctrlWidth / 2 - element.outerWidth() / 2)
					.css("top", decreBtnHeight + thumbHeight / 2 + 1)
					.height(ctrlHeight - decreBtnHeight - increBtnHeight - thumbHeight - 2);
			}
		}

		/**
		* Remove the functionality completely. This will return the element back to its pre-init state.
		*/
		destroy() {
			var self = this, decreBtn, increBtn, wijCSS = self.options.wijCSS;

			decreBtn = this._getDecreBtn();
			increBtn = this._getIncreBtn();
			decreBtn.unbind('.' + self.widgetName);
			increBtn.unbind('.' + self.widgetName);

			self.element.removeClass(wijCSS.uiSlider).removeClass(wijCSS.uiSliderHorizontal)
				.removeClass(wijCSS.uiSliderVertical).removeClass(wijCSS.widget)
				.removeClass(wijCSS.content).removeClass(wijCSS.cornerAll).removeClass(uiSliderHandleClass);

			super.destroy();

			//update for destroy by wh at 2011/11/11
			//this.element.parent().removeAttr("class");
			//this.element.parent().html("");
			$("a", self.element.parent()).remove();
			self.element.unbind('.' + self.widgetName);
			self.element.unwrap();
			if (self._oriStyle === undefined) {
				self.element.removeAttr("style");
			} else {
				self.element.attr("style", self._oriStyle);
			}
			self.element.removeData(self.widgetName).removeData("originalStyle").removeData("originalContent");
			//end

			//Add for support disabled option at 2011/7/8
			if (self.disabledDiv) {
				self.disabledDiv.remove();
				self.disabledDiv = null;
			}
			//end for disabled option
		}

		_slide(event, index, newVal) {
			var self = this, o = self.options, minRange = o.minRange, newValue = newVal, values;

			if (o.range === true) {
				values = self.values();
				if (index === 0 && values[1] - minRange < newVal) {
					newValue = values[1] - minRange;
				} else if (index === 1 && values[0] + minRange > newVal) {
					newValue = values[0] + minRange;
				}
			}
			super._slide(event, index, newValue);
		}

		_getDecreBtn() {
			var decreBtn = this.element.parent().find("." + this.options.wijCSS.wijmoSliderDecButton);
			return decreBtn;
		}

		_getIncreBtn() {
			var increBtn = this.element.parent().find("." + this.options.wijCSS.wijmoSliderIncButton);
			return increBtn;
		}

		_attachClass() {
			var wijCSS = this.options.wijCSS;
			this._getDecreBtn().addClass(wijCSS.cornerAll).addClass(wijCSS.stateDefault)
				.attr("role", "button");
			this._getIncreBtn().addClass(wijCSS.cornerAll).addClass(wijCSS.stateDefault)
				.attr("role", "button");

			this.element.parent().attr("role", "slider").attribute("aria-valuemin", this.options.min)
				.attribute("aria-valuenow", "0").attribute("aria-valuemax", this.options.max);

			if (this.options.orientation === "horizontal") {
				this.element.parent().addClass(wijCSS.wijmoSliderHorizontal);
				this._getDecreBtn().find("> span").addClass(wijCSS.icon).addClass(wijCSS.iconArrowLeft);
				this._getIncreBtn().find("> span").addClass(wijCSS.icon).addClass(wijCSS.iconArrowRight)
			} else {
				this.element.parent().addClass(wijCSS.wijmoSliderVertical);
				this._getDecreBtn().find("> span").addClass(wijCSS.icon).addClass(wijCSS.iconArrowUp);
				this._getIncreBtn().find("> span").addClass(wijCSS.icon).addClass(wijCSS.iconArrowDown);
			}
		}

		_bindEvents() {
			var self = this, decreBtn, increBtn, ele;

			decreBtn = this._getDecreBtn();
			increBtn = this._getIncreBtn();
			ele = self.element;
			//
			decreBtn.bind('click.' + self.widgetName, self, self._decreBtnClick);
			increBtn.bind('click.' + self.widgetName, self, self._increBtnClick);
			//
			decreBtn.bind('mouseover.' + self.widgetName, self, self._decreBtnMouseOver);
			decreBtn.bind('mouseout.' + self.widgetName, self, self._decreBtnMouseOut);
			decreBtn.bind('mousedown.' + self.widgetName, self, self._decreBtnMouseDown);
			decreBtn.bind('mouseup.' + self.widgetName, self, self._decreBtnMouseUp);

			increBtn.bind('mouseover.' + self.widgetName, self, self._increBtnMouseOver);
			increBtn.bind('mouseout.' + self.widgetName, self, self._increBtnMouseOut);
			increBtn.bind('mousedown.' + self.widgetName, self, self._increBtnMouseDown);
			increBtn.bind('mouseup.' + self.widgetName, self, self._increBtnMouseUp);

			ele.bind('mouseup.' + self.widgetName, self, self._elementMouseupEvent);
		}

		_decreBtnMouseOver(e) {
			var self = e.data, data, decreBtn;

			if (self._isDisabled()) {
				return;
			}

			data = { buttonType: "decreButton" };
			self._trigger('buttonMouseOver', e, data);
			decreBtn = self._getDecreBtn();
			decreBtn.addClass(self.options.wijCSS.stateHover);
		}

		_increBtnMouseOver(e) {
			var self = e.data, data, increBtn;

			if (self._isDisabled()) {
				return;
			}

			data = { buttonType: "increButton" };
			self._trigger('buttonMouseOver', e, data);
			increBtn = self._getIncreBtn();
			increBtn.addClass(self.options.wijCSS.stateHover);
		}

		_decreBtnMouseOut(e) {
			var self = e.data, data, decreBtn, wijCSS = self.options.wijCSS;

			if (self._isDisabled()) {
				return;
			}

			data = { buttonType: "decreButton" };
			self._trigger('buttonMouseOut', e, data);
			decreBtn = self._getDecreBtn();
			decreBtn.removeClass(wijCSS.stateHover).removeClass(wijCSS.stateActive);
		}

		_increBtnMouseOut(e) {
			var self = e.data, data, increBtn, wijCSS = self.options.wijCSS;

			if (self._isDisabled()) {
				return;
			}

			data = { buttonType: "increButton" };
			self._trigger('buttonMouseOut', e, data);
			increBtn = self._getIncreBtn();
			increBtn.removeClass(wijCSS.stateHover).removeClass(wijCSS.stateActive);
		}

		_decreBtnMouseDown(e) {
			var self = e.data, data, decreBtn;

			if (self._isDisabled()) {
				return;
			}

			data = { buttonType: "decreButton" };
			self._trigger('buttonMouseDown', e, data);
			decreBtn = self._getDecreBtn();
			decreBtn.addClass(self.options.wijCSS.stateActive);

			//if the mouse release util the mouse out, the track still take effect.
			//added by wuhao 2011/7/16
			$(document).bind("mouseup." + self.widgetName, {
				self: self,
				ele: decreBtn
			}, self._documentMouseUp);

			if (self._intervalID !== null) {
				window.clearInterval(self._intervalID);
				self._intervalID = null;
			}
			//end for mouse release

			self._intervalID = window.setInterval(function () {
				self._decreBtnHandle(self);
			}, 200);

		}

		_documentMouseUp(e) {
			var self = e.data.self, ele = e.data.ele;
			if (self._isDisabled()) {
				return;
			}

			ele.removeClass(self.options.wijCSS.stateActive);

			if (self._intervalID !== null) {
				window.clearInterval(self._intervalID);
				self._intervalID = null;
			}

			$(document).unbind("mouseup." + self.widgetName, self._documentMouseUp);
		}

		_increBtnMouseDown(e) {
			var self = e.data, data, increBtn;

			if (self._isDisabled()) {
				return;
			}

			data = { buttonType: "increButton" };
			self._trigger('buttonMouseDown', e, data);
			increBtn = self._getIncreBtn();
			increBtn.addClass(self.options.wijCSS.stateActive);

			//if the mouse release util the mouse out, the track still take effect.
			//added by wuhao 2011/7/16
			$(document).bind("mouseup." + self.widgetName, {
				self: self,
				ele: increBtn
			}, self._documentMouseUp);

			if (self._intervalID !== null) {
				window.clearInterval(self._intervalID);
				self._intervalID = null;
			}
			//end for mouse release

			self._intervalID = window.setInterval(function () {
				self._increBtnHandle(self);
			}, 200);
		}

		_decreBtnMouseUp(e) {
			var self = e.data, data, decreBtn;

			if (self._isDisabled()) {
				return;
			}

			data = { buttonType: "decreButton" };
			self._trigger('buttonMouseUp', e, data);
			decreBtn = self._getDecreBtn();
			decreBtn.removeClass(self.options.wijCSS.stateActive);

			window.clearInterval(self._intervalID);
		}

		_increBtnMouseUp(e) {
			var self = e.data, data, increBtn;

			if (self._isDisabled()) {
				return;
			}

			data = { buttonType: "increButton" };
			self._trigger('buttonMouseUp', e, data);
			increBtn = self._getIncreBtn();
			increBtn.removeClass(self.options.wijCSS.stateActive);

			window.clearInterval(self._intervalID);
		}

		_decreBtnHandle(sender) {
			if (sender.options.orientation === "horizontal") {
				sender._decre();
			} else {
				sender._incre();
			}
		}

		_decreBtnClick(e) {
			var self = e.data, data;

			if (self._isDisabled()) {
				return;
			}

			//note: step1: slide the slider btn, the change event has fired;
			//step2: then click the decre button, the change event don't fired.
			self._mouseSliding = false;
			//end
			self._decreBtnHandle(self);
			data = { buttonType: "decreButton", value: self.value() };
			self._trigger('buttonClick', e, data);
		}

		_increBtnHandle(sender) {
			if (sender.options.orientation === "horizontal") {
				sender._incre();
			} else {
				sender._decre();
			}
		}

		_increBtnClick(e) {
			var self = e.data, data;

			if (self._isDisabled()) {
				return;
			}
			//note: step1: slide the slider btn, the change event has fired;
			//step2: then click the decre button, the change event don't fired.
			self._mouseSliding = false;
			//end
			self._increBtnHandle(self);
			data = { buttonType: "increButton", value: self.value() };
			self._trigger('buttonClick', e, data);
		}

		_decre() {
			var self = this, curVal = self.value(), o = self.options, min = o.min, step = o.step;

			if (o.values && o.values.length) {
				curVal = self.values(0);
				if (curVal <= min) {
					self.values(0, min);
				} else {
					self.values(0, curVal - step);
				}
			} else {
				curVal = self.value();
				if (curVal <= min) {
					self.value(min);
				} else {
					self.value(curVal - step);
				}
			}
			self.element.parent().attribute("aria-valuenow", self.value());
		}

		_incre() {
			var self = this, curVal = self.value(), o = self.options, max = o.max, step = o.step, index;

			if (o.values && o.values.length) {
				index = o.values.length === 1 ? 0 : 1;
				curVal = self.values(index);
				if (curVal >= max) {
					self.values(index, max);
				} else {
					self.values(index, curVal + step);
				}
			} else {
				curVal = self.value();
				if (curVal >= max) {
					self.value(max);
				} else {
					self.value(curVal + step);
				}
			}
			self.element.parent()
				.attribute("aria-valuenow", self.value());

		}

		_elementMouseupEvent(e) {
			var self = e.data;
			if (self.options.dragFill && self.options.range) {
				if (self._dragFillStart > 0) {
					self._dragFillStart = 0;
				}
			}
		}

		_isDisabled() {
			var opts = this.options;
			return opts.disabledState === true || opts.disabled === true;
		}

		_isUISlider(ele) {
			var wijCSS = this.options.wijCSS,
				$ele = $(ele);
			return ($ele.hasAllClasses(wijCSS.uiSliderRange) && $ele.hasAllClasses(wijCSS.header)
				&& $ele.hasAllClasses(wijCSS.cornerAll)) ||
				//for older version of jQuery UI
				($ele.hasAllClasses(wijCSS.uiSliderRange) && $ele.hasAllClasses(wijCSS.header));
		}

		_mouseCapture(event) {
			this.element.parent().attribute("aria-valuenow", this.value());
			if (this.options.dragFill) {
				if (this._isUISlider(event.target)) {
					this.elementSize = {
						width: this.element.outerWidth(),
						height: this.element.outerHeight()
					};
					this.elementOffset = this.element.offset();
					return true;
				} else {
					try {
						return $.ui.slider.prototype._mouseCapture.apply(this, arguments);
					} catch (e) {

					}
				}
			} else {
				try {
					return $.ui.slider.prototype._mouseCapture.apply(this, arguments);
				} catch (e) {

				}
			}
		}

		_mouseStart(event) {
			if (this._isDisabled()) {
				return false;
			}
			if (this.options.dragFill) {
				if (event.target) {
					if (this._isUISlider(event.target)) {
						this._setStateForDragFill(event);
						return this._trigger("start", event, null);
					}
				}
				this._dragFillTarget = false;
			}
			return true;
		}

		_mouseDrag(event) {
			if (this.options.dragFill) {
				if (this._processDragFill(event)) {
					return false;
				} else {
					return $.ui.slider.prototype._mouseDrag.apply(this, arguments);
				}
			} else {
				return $.ui.slider.prototype._mouseDrag.apply(this, arguments);
			}
		}

		_mouseStop(event) {
			var returnVal = $.ui.slider.prototype._mouseStop.apply(this, arguments);
			if (this.options.dragFill) {
				this._resetStateForDragFill();
			}
			return returnVal;
		}

		_setStateForDragFill(event) {
			this._dragFillTarget = true;
			this._rangeValue = this.values(1) - this.values(0);
			this._oldValue1 = this.values(0);
			this._oldValue2 = this.values(1);
			this._oldX = event.pageX;
			this._oldY = event.pageY;
		}

		_resetStateForDragFill() {
			$(document.documentElement).css("cursor", "default");
			window.setTimeout(function () {
				this._dragFillTarget = false;
				this._dragFillStart = 0;
			}, 500);
		}

		_processDragFill(event) {
			var distance, eleLength, movValue, v, v0, v1, pageX, pageY;

			distance = event.pageX - this._oldX;
			eleLength = this.element.outerWidth();
			if (this.options.orientation === "vertical") {
				eleLength = this.element.outerHeight();
				distance = -(event.pageY - this._oldY);
			}
			movValue = (this.options.max - this.options.min) / eleLength * distance;

			if (this._dragFillTarget) {
				if (this.options.orientation === "vertical") {
					$(document.documentElement).css("cursor", "s-resize");
				} else {
					$(document.documentElement).css("cursor", "w-resize");
				}
				if (this._dragFillStart > 0) {
					v = this._rangeValue;
					this.values(0, this._oldValue1 + movValue);
					this.values(1, this._oldValue1 + movValue + v);
					v0 = this.values(0);
					v1 = this.values(1);
					if (v0 + v > this.options.max) {
						this.values(0, this.options.max - v);
					}
					if (v1 - v < this.options.min) {
						this.values(1, this.options.min + v);
					}
				}
				this._dragFillStart++;

				return true;
			} else {
				return false;
			}

		}

		_parseToNumberValue(key: string, value: any) {
			var val;
			switch (key) {
				case "minRange":
				case "max":
				case "min":
				case "step":
				case "value":
					if (typeof value === "string") {
						val = parseFloat(value);
					} else {
						val = value;
					}
					break;
				default:
					val = value;
					break;
			}

			return val;
		}

		_parseToNumberArray(values: any[]) {
			if (values && $.isArray(values)) {
				$.each(values, (idx, val) => {
					if (typeof val === "string") {
						values[idx] = parseFloat(val);
					}
				});
			}
		}
	}

	wijslider.prototype.widgetEventPrefix = "wijslider";

	class wijslider_options {
		/**
		* All CSS classes used in widgets.
		* @ignore
		*/
		wijCSS = {
			uiSliderRange: "ui-slider-range",
			uiSliderHandle: "ui-slider-handle",
			wijmoSliderDecButton: "wijmo-wijslider-decbutton",
			wijmoSliderIncButton: "wijmo-wijslider-incbutton",
			wijmoSliderHorizontal: "wijmo-wijslider-horizontal",
			wijmoSliderVertical: "wijmo-wijslider-vertical",
			uiSliderRangeMax: "ui-slider-range-max",
			uiSliderRangeMin: "ui-slider-range-min",
			uiSlider: "ui-slider",
			uiSliderHorizontal: "ui-slider-horizontal",
			uiSliderVertical: "ui-slider-vertical"
		};
		/** The buttonMouseOver event is raised when the mouse is over 
		 *  the decrement button or the increment button.
		 * @event
		 * @param {Object} e The jQuery.Event object.
		 * @param {IButtonEventArgs} args The data with this event.
		 */
		buttonMouseOver: (e: JQueryEventObject, args: IButtonEventArgs) => void = null;
		/** The buttonMouseOut event is raised when the mouse leaves 
		 *  the decrement button or the increment button.
		 * @event
		 * @param {Object} e The jQuery.Event object.
		 * @param {IButtonEventArgs} args The data with this event.
		 */
		buttonMouseOut: (e: JQueryEventObject, args: IButtonEventArgs) => void = null;
		/** The buttonMouseDown event is raised when the mouse is down
		 *  on the decrement button or the increment button.
		 * @event
		 * @param {Object} e The jQuery.Event object.
		 * @param {IButtonEventArgs} args The data with this event.
		 */
		buttonMouseDown: (e: JQueryEventObject, args: IButtonEventArgs) => void = null;
		/** The buttonMouseUp event is raised when the mouse is up
		 *  on the decrement button or the increment button.
		 * @event
		 * @param {Object} e The jQuery.Event object.
		 * @param {IButtonEventArgs} args The data with this event.
		 */
		buttonMouseUp: (e: JQueryEventObject, args: IButtonEventArgs) => void = null;
		/** The buttonClick event is raised when the decrement button
		 *  or the increment button is clicked.
		 * @event
		 * @param {Object} e The jQuery.Event object.
		 * @param {IButtonEventArgs} args The data with this event.
		 */
		buttonClick: (e: JQueryEventObject, args: IButtonEventArgs) => void = null;
		/** The dragFill option, when set to true, allows the user to drag 
		 *  the fill between the thumb buttons on the slider widget.
		 */
		dragFill = true;
		/** The minRange option prevents the two range handles (thumb buttons) 
		 *  from being placed on top of one another.
		 */
		minRange = 0;
		/** The animate option defines the sliding animation that is 
		 *  applied to the slider handle when a user clicks outside the handle on the bar. 
		 * @remarks This option will accept a string representing one of the three predefined 
		 * speeds or a number representing the length of time in 
		 * milliseconds that the animation will run. The three predefined speeds are slow, normal, or fast.
		 */
		animate = false;
		/** The max option defines the maximum value of the slider widget.
		 */
		max = 100;
		/** The min option defines the minimum value of the slider widget.
		 */
		min = 0;
		/** The orientation option determines whether the wijslider is positioned horizontally or vertically.
		 * @remarks  If the slider is positioned horizontally, then the min value will be at the left and the max 
		 * value will be at the right. If the slider is positioned vertically, then the min value is at the bottom and 
		 * the max value is at the top of the widget. By default, the widget's orientation is horizontal.
		 */
		orientation = "horizontal";
		/** The range option, if set to true, allows the slider to detect if you have two handles.
		 * @type {boolean|string}
		 * @remarks It will then create a stylable range element between the two handles. 
		 * You can also create a stylable range with one handle by using the 'min' and 'max' values. 
		 * A min range goes from the slider min value to the range handle. A max range goes from the range 
		 * handle to the slider max value.
		 */
		range = false;
		/** The step option determines the size of each interval between the slider minimum value and the slider maximum value.
		 * @remarks The full specified value range of the slider (from the minimum value to the maximum value) 
		 * must be evenly divisible by the step interval.
		 */
		step = 1;
		/** The value option determines the total value of the slider widget when there is only one range handle.
		 * @remarks If there are two range handles, then the value option determines the value of the first handle.
		 */
		value = 0;
		/** The values option can be used to specify multiple handles.
		 * @remarks If the range option is set to true, then the 'values' option must be set in order to have two handles.
		 */
		values = null;
		/** The change event is triggered when the user stops moving the range handle or 
		  * when a value is changed programatically.
		  * @remarks This event takes the event and ui arguments. please refer: http://api.jqueryui.com/slider/
		  * Use event.originalEvent to detect if the value was changed programatically or through mouse or keyboard interaction.
		  * @event
		  */
		change = null;
		/** Triggered on every mouse move during slide. 
		  * @remarks please refer: http://api.jqueryui.com/slider/
		  * @event
		  */
		slide = null;
		/** The start event is triggered when the user begins to move the slider thumb.
		  * @remarks please refer: http://api.jqueryui.com/slider/
		  * @event
		  */
		start = null;
		/** The stop event is triggered when the user stops sliding the slider thumb.
		  * @remarks please refer: http://api.jqueryui.com/slider/
		  * @event
		  */
		stop = null;
	};

	if ($.ui && $.ui.slider) {
		wijslider.prototype.options = $.extend(true, {}, $.ui.slider.prototype.options, wijmo.wijmoWidget.prototype.options, new wijslider_options());
		$.wijmo.registerWidget(widgetName, $.ui.slider, wijslider.prototype);
	}

	/** Provides data for the button event of the wijslider. */
	export interface IButtonEventArgs {
		/** A string value that indicates the type name of button. */
		buttonType: string;
	}
}


/** @ignore */
interface JQuery {
	wijslider: JQueryWidgetFunction;
}

/** @ignore */
interface JQueryUI {
	slider: any;
}