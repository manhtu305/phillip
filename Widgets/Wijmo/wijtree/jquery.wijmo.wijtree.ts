/// <reference path="../Base/jquery.wijmo.widget.ts" />
/// <reference path="../wijtextbox/jquery.wijmo.wijtextbox.ts" />
/// <reference path="../wijutil/jquery.wijmo.wijutil.ts" />

/*globals jQuery, window, XMLHttpRequest*/

/*
* Depends:
*   jquery.ui.core.js
*   jquery.ui.widget.js
*/

module wijmo.tree {

	var $: JQueryStatic = jQuery,
		widgetName: string = "wijtree",
		nodeWidgetName: string = "wijtreenode",
		localCSS = {
			wijtreeInner: "wijmo-wijtree-inner"
		},
		formatString = function (format: string, ...args: string[]): string {
			return format.replace(/{(\d+)}/g, function (match, index) {
				return typeof args[index] !== 'undefined'
					? args[index]
					: match;
			});
		},
		generateTreeCheckMarkup = function (checked: boolean, checkState: string) {
			var css = $.wijmo.wijtreenode.prototype.options.wijCSS,
				html = "<div class='wijmo-checkbox ui-widget' role='checkbox'><div class='wijmo-checkbox-box ui-widget ui-corner-all {0}' style='position: relative;'><span class='{1}'></span></div></div>",
				stateClasses = [css.stateDefault],
				iconClasses = ["wijmo-checkbox-icon"];

			if (checkState === "triState" || checkState === "indeterminate") {
				stateClasses.push(css.stateActive);
				iconClasses.push(css.icon);
				iconClasses.push(css.iconStop);
			} else if (checked) {
				stateClasses.push(css.stateActive);
				iconClasses.push(css.icon);
				iconClasses.push(css.iconCheck);
			}
			return formatString(html, stateClasses.join(" "), iconClasses.join(" "));
		},
		generateItemsMarkup = function (items: WijTreeNodeOptions[], showExpandCollapse: boolean, showCheckBoxes: boolean = false) {
			var lis: string = "", opts: WijTreeNodeOptions = $.wijmo.wijtreenode.prototype.options;

			$.each(items, function (i, item) {
				if (!$.isPlainObject(item)) {
					return true;
				}
				var u = item.navigateUrl || "#",
					hasChildren = item.nodes && item.nodes.length > 0,
					expanded = (item.expanded === null || item.expanded === undefined) ? opts.expanded : item.expanded,
					wijCSS = (item.wijCSS === null || item.wijCSS === undefined) ? opts.wijCSS : item.wijCSS,
					li = "<li class='"
						+ (hasChildren ? "wijmo-wijtree-parent" : "wijmo-wijtree-item")
						+ "'><div class='wijmo-wijtree-node " + wijCSS.stateDefault
						+ (hasChildren ? " wijmo-wijtree-header " : "")
						+ (showExpandCollapse && hasChildren ? (expanded ? "wijmo-state-expanded" : "wijmo-state-collapsed") : "")
						+ "' role='treeitem' aria-expanded='false' aria-checked='false' aria-selected='false'><span class='"
						+ localCSS.wijtreeInner + " " + wijCSS.wijtreeInner + " " + wijCSS.helperClearFix + " " + wijCSS.cornerAll
						+ "'>";

				if (hasChildren) {
					li += "<span class='wijmo-wijtree-hitarea "
						+ (showExpandCollapse ? (wijCSS.icon + " " + (expanded ? wijCSS.iconArrowRightDown : wijCSS.iconArrowRight)) : "")
						+ "'></span>";
				}
				li += "<span class='wijmo-wijtree-nodeimage'></span>";
				if (showCheckBoxes) {
					li += generateTreeCheckMarkup(item.checked, item.checkState);
				}
				li += $.browser.safari ? "<a href='" + u + "'>" : "<a class='wijmo-wijtree-link' href='" + u + "'>";
				if (typeof item.text === "string" && item.text) {
					li += "<span>" + item.text + "</span>";
				}
				li += "</a></span></div>";
				if (hasChildren) {
					li += "<ul class='wijmo-wijtree-list wijmo-wijtree-child " + wijCSS.helperReset + "'";
					if (showExpandCollapse && !expanded) {
						li += " style='display: none'";
					}
					li += ">";
					li += generateItemsMarkup(item.nodes, showExpandCollapse, showCheckBoxes);
					li += "</ul>";
				}
				li += "</li>";
				lis += li;
			});

			return lis;
		},
		createNodeWidget = function ($li: JQuery, options: any, owner: any, nodeWidgetName: string, treeWidgetFullName: string): JQuery {
			if ($.fn[nodeWidgetName]) {
				$li.data("owner", owner);
				if (!!options && $.isPlainObject(options)) {
					$.extend(options, { treeClass: treeWidgetFullName });
					$li[nodeWidgetName](options);
				} else {
					$li[nodeWidgetName]({ treeClass: treeWidgetFullName });
				}
			}
			return $li;
		},
		createTreeNodeWidget = function ($li: JQuery, options: any, owner: any, nodeWidgetName: string, treeWidgetFullName: string): wijtreenode {
			if (!$.fn[nodeWidgetName]) {
				return null;
			}
			createNodeWidget($li, options, owner, nodeWidgetName, treeWidgetFullName);
			return $li.data(<string>$li.data("widgetName"));
		},
		setHTML = function (target: JQuery, content: string) {
			if (content == undefined) {
				return;
			}
			return target.each(function () {
				this.innerHTML = '';
				$(this).append(content);
			});
		},
		getTreeNodeWidget = function ($ele): wijtreenode {
			var treeNode = $ele.data(<string>$ele.data("widgetName"));
			return $.isPlainObject(treeNode) ? null : treeNode;
		},
		enableTabFocus = function ($ele: JQuery, enable: Boolean) {
			if (enable) {
				$ele.removeAttr("tabindex");
			} else if ($ele.attr("tabindex") !== "-1") {
				$ele.attr("tabindex", "-1");
			}
		},
		removeARIA = function (ele: any) {
			$(ele).find('.wijmo-wijtree-node').removeAttr('role').removeAttribute('aria-expanded')
				.removeAttribute('aria-checked').removeAttribute('aria-selected');
		};

	/** @widget */
	export class wijtree extends wijmoWidget {
		options: WijTreeOptions;
		widgetDom: JQuery;
		_selectedNodes: wijtreenode[];
		_checkedNodes: wijtreenode[];
		_insertPosition: string;
		nodeWidgetName: string;
		$nodes: JQuery;
		_hasChildren: boolean;
		nodes: wijtreenode[];
		_isDragging: boolean;
		_editMode: boolean;
		_overNode: wijtreenode;
		_focusNode: wijtreenode;
		_ctrlKey: boolean;
		_nodesCntPerPage: number;
		_totalNodesCreated: number;
		_nextNodeIndex: number;
		_allNodesCreated: boolean;
		_initNodesCreated: boolean;
		_creatingNode: boolean;
		_initializingTree: boolean;

		_create() {
			if (window.wijmoApplyWijTouchUtilEvents) {
				$ = window.wijmoApplyWijTouchUtilEvents($);
			}
			this._initState();
			this._createTree();
			this._attachEvent();
			this._attachNodeEvent();
			super._create();
		}

		_setOption(key: string, value: any) {
			var self = this, isResetHitArea = false,
				o = self.options, check: JQuery;

			switch (key) {
				case "allowDrag":
					self._setAllowDrag(value);
					break;
				case "allowDrop":
					self._setAllowDrop(value);
					break;
				case "showCheckBoxes":
					self._setCheckBoxes(value);
					break;
				case "showExpandCollapse":
					if (self.options.showExpandCollapse !== value) {
						isResetHitArea = true;
					}
					break;
				default:
					break;
			}
			super._setOption(key, value);

			if (key === "nodes") {
				if (value && value.length) {
					self._createChildNodes();
				} else {
					self._clearChildNodes();
				}
			}

			if (isResetHitArea === true) {
				self._setHitArea(value);
			}
		}

		_innerDisable() {
			super._innerDisable();
			this._toggleDisableTree(true);
		}

		_innerEnable() {
			super._innerEnable();
			this._toggleDisableTree(false);
		}

		_toggleDisableTree(disabled: boolean) {
			this._setAllowDrag(!disabled);
		}

		_initState() { //declare the properties of tree
			var self = this, o = self.options;
			self._selectedNodes = [];
			self._checkedNodes = [];
			if (!o.nodes) {
				o.nodes = [];
			}

			self._insertPosition = "unKnown"; //end,after,before
			self.nodeWidgetName = "wijtreenode";
		}

		_initVariable() {
			var self = this;
			self._creatingNode = false;
			self._allNodesCreated = false;
			self._initNodesCreated = false;
			self._initializingTree = false;
			self._nextNodeIndex = 0;
			self._nodesCntPerPage = 0;
			self._totalNodesCreated = 0;
		}

		_createTree() { //create by dom
			var self = this, o = self.options,
				treeClass = ["wijmo-wijtree", o.wijCSS.widget, o.wijCSS.content,
					o.wijCSS.helperClearFix, o.wijCSS.cornerAll].join(' '),
				nodesClass = ["wijmo-wijtree-list ", o.wijCSS.helperReset,
					(o.showExpandCollapse ? "" : "wijmo-wijtree-allexpanded")].join(' '),
				ele: JQuery = self.element;

			if (ele.is("ul")) {
				self.$nodes = ele;
				ele.wrap("<div></div>");
				self.widgetDom = ele.parent();
			} else if (ele.is("div")) {
				self.widgetDom = ele;
				self.$nodes = self.widgetDom.children("ul").eq(0);
			}

			if (self.$nodes.length) {
				self.widgetDom.addClass(treeClass);
				self.$nodes.addClass(nodesClass);

				self._createChildNodes();
				self.widgetDom.append($("<div>").css("clear", "both"));
				enableTabFocus(self.element.find(".wijmo-wijtree-link:gt(0)"), false);

				if (self.options.nodes.length > 0) {
					self.widgetDom.attribute({
						role: "tree",
						"aria-multiselectable": true
					});
				}
			}
		}

		_clearChildNodes() {
			this.$nodes.empty();
			this.nodes && this.nodes.length && (this.nodes.length = 0);
		}

		_getItemHeight(): number {
			return this.$nodes.children("li:first").children(".wijmo-wijtree-node").outerHeight(true);
		}

		_createNodesAtIdle() {
			if (!this._creatingNode) {
				this._createNodesPartially(false);
			}
			return;
		}

		_createNodesPartially(createRest: boolean) {
			this._creatingNode = true;
			if (this._allNodesCreated || !this.options.nodes || !this.options.nodes.length) {
				this._unbindCreateNodesEvent();
				return;
			}
			var self = this,
				options = {
					nIndex: undefined,
					htmlGenerated: true
				},
				nodes: wijtreenode[],
				lis = self.$nodes.children("li"),
				widget: wijtreenode,
				currentNodesCnt: number = self._totalNodesCreated;

			nodes = self._getField("nodes");
			if (!nodes) {
				return;
			}
			while (self._nextNodeIndex < lis.length) {
				if (nodes[self._nextNodeIndex] != null) {
					self._nextNodeIndex++;
					continue;
				}
				options.nIndex = self._nextNodeIndex;
				widget = createTreeNodeWidget($(lis[self._nextNodeIndex]), options, self, self.nodeWidgetName, self.widgetFullName);
				nodes[self._nextNodeIndex] = widget;
				self._nextNodeIndex++;
				if (!createRest && currentNodesCnt + self._nodesCntPerPage < self._totalNodesCreated) {
					break;
				}
			}
			if (this._nextNodeIndex >= lis.length) {
				this._unbindCreateNodesEvent();
				this._allNodesCreated = true;
			}
			this._creatingNode = false;
		}

		_bindCreateNodesEvent() {
			var self = this;
			$(document).bind("scroll." + this.widgetName, $.proxy(self._createNodesAtIdle, self))
				.bind("mousemove." + this.widgetName, $.proxy(self._createNodesAtIdle, self))
				.bind("click." + this.widgetName, $.proxy(self._createNodesAtIdle, self))
				.bind("keydown." + this.widgetName, $.proxy(self._createNodesAtIdle, self));
		}

		_unbindCreateNodesEvent() {
			$(document).unbind('.' + this.widgetName);
		}

		_createChildNodes() {
			var self = this, o = self.options,
				options = {
					nIndex: undefined,
					htmlGenerated: false
				},
				nodeWidgets: wijtreenode[] = [],
				$ul = self.$nodes,
				lis: JQuery,
				widget: wijtreenode, $li: JQuery;

			if (o.nodes && o.nodes.length) {
				self._initVariable();
				setHTML($ul, generateItemsMarkup(o.nodes, o.showExpandCollapse, o.showCheckBoxes));
				self._nodesCntPerPage = document.documentElement.clientHeight / self._getItemHeight();
				options.htmlGenerated = true;
				self._initializingTree = true;
				lis = $ul.children("li");
				lis.each((idx, ele) => {
					$li = $(ele);

					options.nIndex = idx;
					if (!self._initNodesCreated) {
						widget = createTreeNodeWidget($li, options, self, self.nodeWidgetName, self.widgetFullName);
						self._nextNodeIndex++;
					} else {
						widget = null;
						self._allNodesCreated = false;
					}
					nodeWidgets.push(widget);
					if (self._totalNodesCreated >= self._nodesCntPerPage * 2) {
						self._initNodesCreated = true;
					}
				});
				self._initializingTree = false;
				self._allNodesCreated = self._nextNodeIndex >= lis.length;
				if (!self._allNodesCreated) {
					self._bindCreateNodesEvent();
				}
			} else {
				lis = $ul.children("li");
				lis.each((idx, ele) => {
					$li = $(ele);

					widget = createTreeNodeWidget($li, options, self, self.nodeWidgetName, self.widgetFullName);
					nodeWidgets.push(widget);
					if (!$.browser.safari && widget.$navigateUrl
						&& widget.$navigateUrl.length
						&& widget.$navigateUrl.is("a")) {
						widget.$navigateUrl.addClass("wijmo-wijtree-link");
					}
					o.nodes.push(widget.options);
				});
			}
			self._hasChildren = nodeWidgets.length > 0;
			self._setField("nodes", nodeWidgets);
			self.nodes = nodeWidgets;
		}

		_createNodeWidget($li: JQuery, options: any) {
			return createNodeWidget($li, options, this, this.nodeWidgetName, this.widgetFullName);
		}

		/*tree event*/
		_attachEvent() {
			this._attachDroppable();
		}

		_attachDroppable() {
			var self = this, o = self.options,
				droppable = o.droppable,
				options: JQueryUI.DroppableOptions = {
					accept: "li",
					scope: "tree"
				},
				events: JQueryUI.DroppableEvents = {
					drop: function (event, ui) {
						var d = ui.draggable,
							dragNode: wijtreenode = self._getNodeWidget(d),
							dropNode: wijtreenode,
							position: string,
							oldOwner: any,
							parent: wijtreenode,
							brothers: wijtreenode[],
							idx: number,
							oldPosition: number,
							newPosition: number = -1,
							oldShowCheckBoxes = dragNode._tree.options.showCheckBoxes;

						droppable = o.droppable;

						if (self._trigger("nodeBeforeDropped", event, ui) === false ||
							!dragNode || self._isDisabled()) {
							self._isDragging = false;
							return;
						}
						dropNode = dragNode._dropTarget;
						position = dragNode._insertPosition;
						if (dropNode && position !== "unKnown") {
							oldOwner = dragNode._getOwner();

							if (oldOwner) {
								oldPosition = d.index();
							}
							/*reset old tree*/
							dragNode._tree._isDragging = false;

							if (position === "end") {
								newPosition = dropNode.getNodes().length;
								parent = dropNode;
							} else if (position === "before" || position === "after") {
								parent = dropNode._getOwner();
								brothers = parent.getNodes();
								idx = $.inArray(dropNode, brothers);
								if (idx !== -1) {
									newPosition = position === "before" ? idx : idx + 1;
								}
							}
							if (!parent._isAllowDrop()) {
								self._isDragging = false;
								return;
							}

							if (droppable && $.isFunction(droppable.drop)) {
								ui["oldParent"] = oldOwner.element;
								ui["newParent"] = parent.element;
								ui["oldIndex"] = oldPosition;
								ui["newIndex"] = newPosition;
								droppable.drop.call(self.element, event, ui);
							} else {
								if (oldOwner) {
									oldOwner.remove(d);
								}
								if (newPosition !== -1) {
									// re-calculate position (when old parent == new parent)
									if (position === "before" || position === "after") {
										if (idx !== $.inArray(dropNode, brothers)) {
											newPosition--;
										}
									}

									// Handling when the drop tree has no child-nodes
									if (self.$nodes.children("li").length) {
										parent.add(d, newPosition);
									} else {
										self.add(d, newPosition);
									}
								}
							}

							if (oldShowCheckBoxes !== self.options.showCheckBoxes) {
								dragNode._setCheckBoxes(self.options.showCheckBoxes);
							}
							/*reset old tree*/
							$("a", d).eq(0).blur();
							if (dragNode.options.selected) {
								dragNode._setSelected(false);
							}
							/*set tree*/
							setTriState(oldOwner);
							setTriState(parent);

							$.extend(ui, {
								sourceParent: oldOwner ? oldOwner.element : null,
								sIndex: oldPosition,
								targetParent: parent.element,
								tIndex: newPosition,
								widget: dragNode
							});
							self._trigger("nodeDropped", event, ui);
						}
					}
				},
				setTriState = function (node: wijtreenode) {
					if (!node.element.is(":" + self.widgetFullName) &&
						node.getNodes().length > 0 &&
						node._tree.options.showCheckBoxes &&
						node._tree.options.allowTriState) {
						node.getNodes()[0]
							._setParentCheckState();
					}
				};

			$.extend(options, droppable, events); // mind the order: events must be at last.
			if ($.fn.droppable) {
				self.widgetDom.droppable(options);
			}
		}

		_attachNodeEvent() {
			// Note: 1. focus/blur do not support bubbling. 2. focusin/focusout is always available using jQuery, but only in webkit can you get widget from "event.data".

			this.element.bind("focusin." + this.widgetName, $.proxy(this._onFocus, this))
				.bind("focusout." + this.widgetName, $.proxy(this._onBlur, this))
				.bind("click." + this.widgetName, $.proxy(this._onClick, this))
				.bind("mouseover." + this.widgetName, $.proxy(this._onMouseOver, this))
				.bind("mouseout." + this.widgetName, $.proxy(this._onMouseOut, this))
				.bind("keydown." + this.widgetName, $.proxy(this._onKeyDown, this));
		}

		_onClick(event: JQueryEventObject) {
			this._callEvent(event, '_onClick');
			if ($.browser.webkit) {
				this.widgetDom.focus();
			}
		}

		_onFocus(event: JQueryEventObject) {
			var $nodeDom: JQuery;
			if (!event.data) {
				$nodeDom = this._getNodeByDom(<Element>event.target);
				event.data = $nodeDom.data($nodeDom.data("widgetName"));
			}
			this._callEvent(event, '_onFocus');
		}

		_onBlur(event: JQueryEventObject) {
			var $nodeDom: JQuery;
			if (!event.data) {
				$nodeDom = this._getNodeByDom(<Element>event.target);
				event.data = $nodeDom.data($nodeDom.data("widgetName"));
			}
			this._callEvent(event, '_onBlur');
		}

		_onKeyDown(event: JQueryEventObject) {
			this._callEvent(event, '_onKeyDown');
		}

		_onMouseOut(event: JQueryEventObject) {
			this._callEvent(event, '_onMouseOut');
		}

		_onMouseOver(event: JQueryEventObject) {
			this._callEvent(event, '_onMouseOver');
		}

		_callEvent(event: JQueryEventObject, type: string) {
			if (!this._allNodesCreated && this.options.nodes && this.options.nodes.length) {
				this._createNodesAtIdle();
			}
			var el = <Element>event.target, node: wijtreenode;
			if (el) {
				node = this._getNodeWidgetByDom(el);
				if (node === null) {
					return;
				} else if (node._hasChildren && node._getField("nodes")) {
					if (!node._allNodesCreated && node.options.nodes && node.options.nodes.length) {
						node._createChildNodesAtIdle();
					}
				}
				node[type](event);
			}
		}

		_nodeSelector() {
			return ":wijmo-wijtreenode";
		}

		/*public methods*/

		/** 
		* The getSelectedNodes method gets the selected nodes.
		* @example $("selector").wijtree("getSelectedNodes");
		* @returns {array}
		*/
		getSelectedNodes(): any[] {
			return this._selectedNodes;
		}

		/** 
		* The getCheckedNodes method gets the nodes which are checked.
		* @example $("selector").wijtree("getCheckedNodes");
		* @returns {array}
		*/
		getCheckedNodes(): any[] {
			var self = this, checkedNodes = [], treeNodeWidgetName = self.nodeWidgetName;

			$(self._nodeSelector(), self.element).each((idx, ele) => {
				if ($(ele)[treeNodeWidgetName]("option", "checked") &&
					$(ele)[treeNodeWidgetName]("option", "checkState") !== "indeterminate") {
					checkedNodes.push($(ele));
				}
			});
			return checkedNodes;
		}

		/** 
		* The destroy method will remove the rating functionality completely and will return the element to its pre-init state.
		* @example $("selector").wijtree("destroy");
		*/
		destroy() {
			var self = this, $nodes = self.$nodes, o = self.options,
				c = ["wijmo-wijtree", o.wijCSS.widget,
					o.wijCSS.content, o.wijCSS.helperClearFix, o.wijCSS.cornerAll].join(' ');
			self.widgetDom.removeClass(c).removeAttr("role").removeAttribute("aria-multiselectable");
			enableTabFocus(self.element.find(".wijmo-wijtree-link"), true);

			if (self.widgetDom.data("ui-droppable")) {
				self.widgetDom.droppable("destroy");
			}

			self._unbindCreateNodesEvent();
			self.element.unbind('.' + self.widgetName);
			self.widgetDom.children("div[style]:last").remove();
			$nodes.removeData("nodes")
				.removeClass("wijmo-wijtree-list" + " " + o.wijCSS.helperReset);
			$nodes.children("li").each((idx, ele) => {
				var nodeWidget = self._getNodeWidget($(ele));
				if (nodeWidget) {
					nodeWidget.destroy();
				}
			});
			$.wijmo.wijtree.prototype.options.nodes = null;

			super.destroy();
		}

		/** 
		* The add method adds a node to the tree widget.
		* @example $("#tree").wijtree("add", "node 1", 1);
		* @param {string|object} node 
		* 1.markup html.such as "<li><a>node</a></li>" as a node.
		* 2.wijtreenode widget.
		* 3.object options according to the options of wijtreenode.
		* 4.node's text.
		* @param {number} position The position to insert at.
		*/
		add(node: any, position: number) {
			var nodeWidget: wijtreenode = null, o = {}, $node: JQuery,
				self = this, i: number,
				originalLength: number,
				itemDom: string,
				cnodes: wijtreenode[], $li;

			if ($.browser.safari) {
				itemDom = "<li><a href='{0}'>{1}</a></li>";
			} else {
				itemDom = "<li><a href='{0}' class='wijmo-wijtree-link'>{1}</a></li>";
			}
			if (typeof node === "string") {
				$node = $(itemDom.replace(/\{0\}/, "#").replace(/\{1\}/, node));
				self._createNodeWidget($node, o);
				nodeWidget = $node.data(<string>$node.data("widgetName"));
			} else if (node.jquery) {
				if (!node.data("widgetName")) {
					self._createNodeWidget(node, o);
				}
				nodeWidget = node.data(<string>node.data("widgetName"));
				nodeWidget._setField("owner", self);
			} else if (node.nodeType) {
				$node = $(node);
				self._createNodeWidget($node, o);
				nodeWidget = $node.data(<string>$node.data("widgetName"));
			} else if ($.isPlainObject(node)) {
				$node = $(itemDom.replace(/\{0\}/, node.url ? node.url : "#")
					.replace(/\{1\}/, node.text)); //node
				node.isAddedNodeWithOptions = true;
				self._createNodeWidget($node, node);
				nodeWidget = $node.data(<string>$node.data("widgetName"));
			}

			if (nodeWidget === null) {
				return;
			}
			originalLength = self.nodes.length;
			if (!position || position > originalLength) {
				if (position !== 0) {
					position = originalLength;
				}
			}
			cnodes = nodeWidget.getNodes();
			nodeWidget._tree = self;
			for (i = 0; i < cnodes.length; i++) {
				cnodes[i]._tree = self;
			}

			if (originalLength > 0 && originalLength !== position) {
				$li = this.$nodes.find(">li:eq(" + position + ")");
				if (nodeWidget.element.get(0) !== $li.get(0)) {
					nodeWidget.element.insertBefore($li);
				}
			} else {
				self.$nodes.append(nodeWidget.element);
			}
			self._changeCollection(position, nodeWidget);
			nodeWidget._initNodeClass();
		}

		/** 
		* The remove method removes the indicated node from the wijtree element.
		* @example $("#tree").wijtree("remove", 1);
		* @param {number|object} node
		* which node to be removed
		* 1.wijtreenode element.
		* 2.the zero-based index of which node you determined to remove.
		*/
		remove(node: any) {
			var idx = -1, nodeWidget: wijtreenode, lis = this.$nodes.children("li");
			if (node && node.jquery) {
				idx = node.index();
			} else if (typeof node === "number") {
				idx = node;
			}
			if (idx < 0 || idx >= this.nodes.length) {
				return;
			}
			nodeWidget = this.nodes[idx];
			if (nodeWidget) {
				nodeWidget.element.detach();
				removeARIA(nodeWidget.element);
			} else {
				$(lis[idx]).detach();
				removeARIA(lis[idx]);
			}
			this._changeCollection(idx);
		}

		_changeCollection(idx: number, nodeWidget?: wijtreenode) {
			var nodes: wijtreenode[] = this.nodes,
				ons = this.options.nodes, widgetDom = this.widgetDom;
			if (nodeWidget) {
				nodes.splice(idx, 0, nodeWidget);
				ons.splice(idx, 0, nodeWidget.options);
			} else {
				nodes.splice(idx, 1);
				ons.splice(idx, 1);
			}

			if (nodes.length === 0) {
				widgetDom.removeAttr("role");
				widgetDom.removeAttribute("aria-multiselectable");
			} else {
				widgetDom.attr("role", "tree");
				widgetDom.attribute("aria-multiselectable", true);
			}
		}

		/** 
		* The getNodes method gets an array that contains the root nodes of the current tree.
		* @example $("#tree").wijtree("getNodes");
		* @return {Array}
		*/
		getNodes() {
			if (this.options.nodes && this.options.nodes.length) {
				this._createNodesPartially(true);
			}
			return this.nodes;
		}

		_createParentNodes($li: JQuery): wijtreenode {
			var self = this,
				options = { nIndex: undefined, htmlGenerated: true },
				nodeWidget: wijtreenode,
				$lis: JQuery[] = [],
				$seachedNode = $li;

			if ($seachedNode.closest("ul").parent().hasClass("wijmo-wijtree")) {
				options.nIndex = this.element.children("li").index($seachedNode);
				createTreeNodeWidget($seachedNode, options, self, self.nodeWidgetName, self.widgetFullName);
			} else {
				while (!$seachedNode.closest("ul").parent().hasClass("wijmo-wijtree")) {
					$seachedNode = $seachedNode.closest("ul").closest("li");
					$lis.push($seachedNode);
					if (getTreeNodeWidget($seachedNode)) break;
				}
				$lis.reverse();
				$.each($lis, function (index, $ele) {
					nodeWidget = getTreeNodeWidget($ele);
					if (!nodeWidget) {
						options.nIndex = $ele.parent().children("li").index($ele);
						nodeWidget = createTreeNodeWidget($ele, options, self, self.nodeWidgetName, self.widgetFullName);
					}
					nodeWidget._createChildNodeWidget();
				});
			}
			return getTreeNodeWidget($li);
		}

		/** 
		* The findNodeByText method finds a node by the specified node text.
		* @example $("#tree").wijtree("findNodeByText", "node 1");
		* @param {string} txt The text of which node you want to find.
		* @return {wijtreenode}
		*/
		findNodeByText(txt: string): wijtreenode {
			var self = this, nodes = $(".wijmo-wijtree-node a>span", this.$nodes).filter(function () {
				return $(this).text() === txt;
			}), nodeWidget: wijtreenode = null, node: JQuery;
			if (nodes.length) {
				node = nodes.eq(0).closest("li");
				nodeWidget = getTreeNodeWidget(node);
				if (!nodeWidget) {
					nodeWidget = self._createParentNodes(node);
				}
			}
			return nodeWidget;
		}

		_setAllowDrag(value: boolean) {
			var self = this, $allNodes: JQuery,
				nodeSelector = self._nodeSelector(),
				nodeWName = "wijmo-" + self.nodeWidgetName;

			if (!$.fn.draggable) {
				return;
			}

			if (value) {
				$allNodes = self.element.find(nodeSelector);
				$allNodes.each(function () {
					var w = <wijtreenode>$(this).data(nodeWName);
					if (!$(this).data("ui-draggable") &&
						w.options.allowDrag !== false &&
						(!w.$navigateUrl.data("events") ||
						!w.$navigateUrl.data("events").mousedown)) {
						w.$navigateUrl.one("mousedown." + w.widgetName, w, w._onMouseDown);
					}
				});
			} else {
				$allNodes = self.element.find(nodeSelector + ":ui-draggable");
				$allNodes.each(function () {  //update for allowDrag each node
					var w = <wijtreenode>$(this).data(nodeWName);
					if (!w.options.allowDrag) {
						$(this).draggable("destroy");
					}
				});
			}
		}

		_setAllowDrop(value: boolean) {
			if (!$.fn.droppable) {
				return;
			}

			if (!this.widgetDom.data("ui-droppable")) {  //update for setting each node allowDrop
				this._attachDroppable();
			}
		}

		_setCheckBoxes(value: boolean) {
			var self = this;
			$.each(self.getNodes(), function (idx, node) {
				node._setCheckBoxes(value);
			});
		}

		_setHitArea(value: boolean) {
			var self = this;

			self.$nodes[value ? "addClass" : "removeClass"]("wijmo-wijtree-allexpanded");
			self.$nodes.children("li").each((idx, ele) => {
				var nodeWidget = self._getNodeWidget($(ele));
				if (nodeWidget !== null) {
					nodeWidget._setHitArea(value);
				}
			});
		}

		/*region methods(private)*/
		_getNodeWidget($node: JQuery): wijtreenode {
			if ($node.is(this._nodeSelector())) {
				var widget: wijtreenode = $node.data(<string>$node.data("widgetName"));
				return widget;
			}
			return null;
		}

		_getNodeWidgetByDom(el: Element): wijtreenode {
			var node = this._getNodeByDom(el);
			return this._getNodeWidget(node);
		}

		_getNodeByDom(el: Element): JQuery {
			return $(el).closest(this._nodeSelector());
		}

		_refreshNodesClass() {
			var nodes: wijtreenode[] = this.getNodes(), i: number;
			for (i = 0; i < nodes.length; i++) {
				nodes[i]._initNodeClass();
			}
		}

		_getField(key: string) {
			return this.element.data(key);
		}

		_setField(key: string, value: any) {
			this.element.data(key, value);
		}

		_isAllowDrop(): boolean {
			return this.options.allowDrop;
		}
	}

	export class wijtreenode extends wijmoWidget {
		options: WijTreeNodeOptions;
		$navigateUrl: JQuery;
		_checkState: string;
		_hasChildren: boolean;
		_dropTarget: wijtreenode;
		_text: string;
		_insertPosition: string;
		_isTemplate: boolean;
		_expanded: boolean;
		$editArea: JQuery;
		_expandTimer: number;
		_collapseTimer: number;
		_focused: boolean;
		_isClick: boolean;
		_isOverNode: boolean;
		_isOverHitArea: boolean;
		_isSorted: boolean;
		_isDecsSort: boolean;

		$nodeBody: JQuery;
		$hitArea: JQuery;
		$inner: JQuery;
		$nodes: JQuery;
		$subnodes: JQuery[];
		$text: JQuery;
		$nodeImage: JQuery;
		$checkBox: JQuery;
		$checkBoxBody: JQuery;
		$checkBoxIcon: JQuery;
		_tree: wijtree;
		_initialized: boolean;
		_setNodes: boolean;
		_index: number;
		_nextNodeIndex: number;
		_allNodesCreated: boolean;
		_initNodesCreated: boolean;

		_setOption(key: string, value: any) {
			var self = this, check: JQuery;

			switch (key) {
				case "accessKey":
					if (self.$navigateUrl !== null) {
						self.$navigateUrl.attr("accesskey", value);
					}
					break;
				case "checked":
					if (self.options[key] !== value) {
						self._checkClick();
					}
					break;
				case "collapsedIconClass":
				case "expandedIconClass":
				case "itemIconClass":
					self.options[key] = value;
					self._initNodeImg();
					break;
				case "expanded":
					self._setExpanded(value);
					break;
				case "selected":
					self._setSelected(value);
					break;
				case "text":
					self._setText(value);
					break;
				case "toolTip":
					self._setToolTip(value);
					break;
				case "navigateUrl":
					self._setNavigateUrlHref(value);
					break;
				case "allowDrag":  //update for setOption allowDrag each node
					self._setAllowDrag(value);
					break;
				default:
					break;
			}

			if (key === "nodes") {
				self.options.nodes.length = 0;
				self._setField("nodes", null);
				self._setNodes = true;
				$.each(<WijTreeNodeOptions[]>value, function (i, n) {
					self.options.nodes.push(n);
				});
				self.options.nodes.concat();
				if (value && value.length) {
					self._hasChildren = self._hasSubNodes();
					self._createChildNodes();
					self._initNodeClass();
				} else {
					self._clearChildNodes();
				}
			} else {
				super._setOption(key, value);
			}
		}

		_initVariable() {
			var self = this;
			self._allNodesCreated = false;
			self._initNodesCreated = false;
			self._nextNodeIndex = 0;
		}

		_initState() {
			var self = this;
			self._tree = null;
			self._dropTarget = null;
			self._checkState = "unChecked"; //Checked, UnChecked, Indeterminate
			self._insertPosition = "unKnown"; //end,after,before            
			self.$nodeBody = null;
			self.$checkBox = null;
			self.$hitArea = null;
			self.$nodes = null;
			self.$subnodes = null;
		}

		_create() {
			var self = this, o = self.options;
			self._setNodes = o.isAddedNodeWithOptions;
			self._initState();
			self.element.data("widgetName", "wijmo-wijtreenode");
			self._createTreeNode();
			self._initNode();

			if (o.selected) {
				self._tree._selectedNodes.push(self);
			}

			if (o.checked) {
				self._checkState = "checked";
			}
			super._create();
		}

		_createTreeNode() {
			var $li = this.element, childOpts,
				self = this, ownerOpts, o = self.options;

			if (self._tree === null) {
				self._tree = self._getTree();
			}

			if (!isNaN(o.nIndex)) {
				ownerOpts = self._getOwner().options,
				childOpts = ownerOpts.nodes[o.nIndex];
				if (childOpts && !childOpts.nodes) {
					childOpts.nodes = [];
				}
				$.extend(o, childOpts);
				ownerOpts.nodes[o.nIndex] = o;
			} else if (!o.nodes) {
				o.nodes = [];
			}

			if (self._isHtmlGenerated()) {
				self._tree._totalNodesCreated++;
				self.$nodeBody = $li.children(".wijmo-wijtree-node");
				self.$inner = self.$nodeBody.children("." + localCSS.wijtreeInner);
				self.$nodeImage = self.$inner.children(".wijmo-wijtree-nodeimage");

				if (self._tree.options.showCheckBoxes) {
					self.$checkBox = $li.find(">div>span>div.wijmo-checkbox");
					self.$checkBoxBody = self.$checkBox.children("div");
					self.$checkBoxIcon = self.$checkBoxBody.children("span");
				}
				self.$navigateUrl = self.$inner.children("a");
				self.$text = self.$navigateUrl.children("span").eq(0);
			} else {
				self.$nodeImage = $("<span>");
				self.$nodeBody = $("<div>").attribute({
					role: "treeitem",
					"aria-expanded": false,
					"aria-checked": false,
					"aria-selected": false
				});

				self.$navigateUrl = $li.children("a");

				if (self.$navigateUrl.length === 0) {
					self.$navigateUrl = $li.children("div");
					self.$navigateUrl.addClass("wijmo-wijtree-template");
					self._isTemplate = true;
				}

				if (self.$navigateUrl.length === 0) {
					self.$navigateUrl = $("<a href='#'></a>");
				}

				if (!self._isTemplate) {
					self.$text = self.$navigateUrl.children("span").eq(0);
					if (self.$text.length === 0) {
						self.$navigateUrl.wrapInner("<span></span>");
						self.$text = self.$navigateUrl.children("span").eq(0);
					}
				}
				self.$inner = $("<span></span>").addClass(o.wijCSS.helperClearFix + " " + localCSS.wijtreeInner + " " + o.wijCSS.wijtreeInner + " " + o.wijCSS.cornerAll);
				self.$inner.append(self.$nodeImage);
				if (self._tree.options.showCheckBoxes) {
					self._createTreeCheck();
				}
				self.$inner.append(self.$navigateUrl);
				self.$nodeBody.append(self.$inner);
				$li.prepend(self.$nodeBody);
			}

			self._hasChildren = self._hasSubNodes();
			self._createChildNodes();
		}

		_clearChildNodes() {
			var nodes = this._getField("nodes");
			this.$nodes.empty();
			nodes && nodes.length && (nodes.length = 0);
			this.$subnodes && this.$subnodes.length && (this.$subnodes.length = 0);
		}

		_createChildNodes() {
			var self = this, o = self.options, $li = self.element;

			self._initVariable();
			if (self._hasChildren) {
				if (self.$nodes === null) {
					self.$nodes = $li.children("ul").eq(0);
				}
				if (self._isHtmlGenerated()) {
					self.$hitArea = self.$inner.children(".wijmo-wijtree-hitarea");
				} else {
					$li.addClass("wijmo-wijtree-parent");
					self.$nodeBody.addClass("wijmo-wijtree-node wijmo-wijtree-header " + o.wijCSS.stateDefault);
					self.$hitArea = $("<span>");
					self.$inner.prepend(self.$hitArea);
					self.$nodes.addClass("wijmo-wijtree-list wijmo-wijtree-child " + o.wijCSS.helperReset);
				}
				if (o.expanded || !self._isHtmlGenerated()) {
					self._createChildNodeWidget();
				}
			} else {
				if (!self._isHtmlGenerated()) {
					$li.addClass("wijmo-wijtree-item");
					self.$nodeBody.addClass("wijmo-wijtree-node " + o.wijCSS.stateDefault);
				}
				self._setField("nodes", []);
			}
		}

		_isHtmlGenerated(): boolean {
			return !!this.options.htmlGenerated;
		}

		_createInitNode($li: JQuery,
			index: number, nodesCount: number, opts: any, subNodes?: JQuery[]): wijtreenode {
			var self = this, nodeWidget: wijtreenode;
			nodeWidget = getTreeNodeWidget($li);
			if (!nodeWidget) {
				if (subNodes) {
					subNodes.push($li);
				}
				opts.nIndex = index;
				if (!self._initNodesCreated || !self._isHtmlGenerated()) {
					nodeWidget = createTreeNodeWidget($li, opts, self, nodeWidgetName, self.options.treeClass);
					if (!subNodes && !$.browser.safari) {
						$li.children(".wijmo-wijtree-node").find("." + localCSS.wijtreeInner + " a").addClass("wijmo-wijtree-link");
					}
					nodeWidget._index = index;
					self._nextNodeIndex++;
				} else {
					nodeWidget = null;
					self._allNodesCreated = false;
				}
				if (self._tree._initializingTree) {
					if (self._tree._totalNodesCreated >= self._tree._nodesCntPerPage * 2) {
						self._initNodesCreated = true;
					}
				} else {
					if (self._nextNodeIndex >= self._tree._nodesCntPerPage * 2) {
						self._initNodesCreated = true;
					}
				}
			}
			self._allNodesCreated = self._nextNodeIndex >= nodesCount;
			return nodeWidget;
		}

		_bindCreateNodesEvent() {
			var self = this;
			$(document).bind("scroll." + this.widgetName, $.proxy(self._createChildNodesAtIdle, self))
				.bind("mousemove." + this.widgetName, $.proxy(self._createChildNodesAtIdle, self))
				.bind("click." + this.widgetName, $.proxy(self._createChildNodesAtIdle, self))
				.bind("keydown." + this.widgetName, $.proxy(self._createChildNodesAtIdle, self));
		}

		_unbindCreateNodesEvent() {
			$(document).unbind('.' + this.widgetName);
		}

		_createChildNodesAtIdle() {
			if (!this._tree._creatingNode) {
				this._createChildNodesPartially(false);
			}
			return;
		}

		_createChildNodesPartially(createRest: boolean) {
			this._tree._creatingNode = true;
			var self = this,
				options = {
					nIndex: undefined,
					htmlGenerated: true
				},
				nodes: wijtreenode[] = [],
				lis = self.$nodes.children("li"),
				widget: wijtreenode,
				currentNodesCnt: number = self._nextNodeIndex;

			if (!self._getField("nodes")) {
				self._setField("nodes", nodes);
			} else {
				nodes = self._getField("nodes");
			}
			if (this._allNodesCreated || !this.options.nodes || !this.options.nodes.length) {
				this._unbindCreateNodesEvent();
				return;
			}
			while (self._nextNodeIndex < lis.length) {
				if (nodes[self._nextNodeIndex] != null) {
					self._nextNodeIndex++;
					continue;
				}
				options.nIndex = self._nextNodeIndex;
				widget = createTreeNodeWidget($(lis[self._nextNodeIndex]), options, self, nodeWidgetName, self.options.treeClass);
				nodes[self._nextNodeIndex] = widget;
				self._nextNodeIndex++;
				if (!createRest && currentNodesCnt + self._tree._nodesCntPerPage < self._nextNodeIndex) {
					break;
				}
			}
			if (this._nextNodeIndex >= lis.length) {
				this._unbindCreateNodesEvent();
				this._allNodesCreated = true;
			}
			this._tree._creatingNode = false;
		}

		_createChildNode(): wijtreenode[] {
			var self = this, o = self.options, nodes: wijtreenode[] = [], lis: JQuery,
				opts = {
					nIndex: undefined,
					cfli: undefined,
					treeClass: o.treeClass,
					nodes: undefined,
					htmlGenerated: self._isHtmlGenerated()
				};
			if (self._setNodes && o.nodes && o.nodes.length) {
				if (!self.$nodes || !self.$nodes.length) {
					self.$nodes = $("<ul>").appendTo(self.element);
				}
				setHTML(self.$nodes, generateItemsMarkup(o.nodes, self._tree.options.showExpandCollapse, self._tree.options.showCheckBoxes));
				self.$subnodes = [];
				opts.htmlGenerated = true;
				lis = self.$nodes.children("li");
				lis.each((idx, ele) => {
					var $li = $(ele);
					nodes.push(self._createInitNode($li, idx, lis.length, opts, self.$subnodes));
				});
			} else {
				if (!o.nodes) {
					o.nodes = [];
				}
				if (self.$subnodes === null) {
					self._updateSubNodes();
				}
				$.each(self.$subnodes, function (idx, $li) {
					if (!self._isHtmlGenerated()) {
						opts.nodes = [];
					}
					nodes.push(self._createInitNode($li, idx, self.$subnodes.length, opts));
				});
			}
			if (!self._allNodesCreated) {
				self._bindCreateNodesEvent();
			}
			return nodes;
		}

		_initNode() { // init node (children, class, tree)
			var self = this, o = self.options;
			if (!self._initialized) {
				self._initialized = true;
				self._initNavigateUrl();
				if (!self._isTemplate && self.$text && self.$text.length) {
					self._text = self.$text.html();
					o.text = self._text;
				}
				self._hasChildren = self._hasSubNodes();
				self._initNodesUL();
				self._initNodeClass();
				self._initNodeImg();
				if (self._isAllowDrag()) {
					self.$navigateUrl.one("mousedown." + self.widgetName, self, self._onMouseDown);
				}
			}
		}

		_initNodeClass() {
			var self = this, o = self.options, style: string,
				nodeClass = "wijmo-wijtree-item",
				hitClass = o.wijCSS.icon + " " + (o.expanded ? o.wijCSS.iconArrowRightDown : o.wijCSS.iconArrowRight);

			if (self._tree.options.showExpandCollapse) {
				if (self._hasChildren || !!o.hasChildren) {
					if (!self._isHtmlGenerated()) {
						self.$nodeBody.removeClass("wijmo-state-expanded wijmo-state-collapsed");
						if (o.expanded) {
							self.$nodeBody.addClass("wijmo-state-expanded");
						} else {
							self.$nodeBody.addClass("wijmo-state-collapsed");
						}
					}
					if (self.$hitArea !== null) {
						self.$hitArea.removeClass([o.wijCSS.icon, o.wijCSS.iconArrowRightDown, o.wijCSS.iconArrowRight].join(' '));
						self.$hitArea.addClass(hitClass);
					} else {
						self.$hitArea = $("<span>").addClass(hitClass).prependTo(self.$inner);
						self.element.removeClass(nodeClass).addClass("wijmo-wijtree-parent");
					}

					if (self._hasChildren && self.$nodes && self.$nodes.length) {
						style = o.expanded ? "" : "none";
						self.$nodes.css({ display: style });
					}
				} else if (self.$hitArea && self.$hitArea.length) {
					self.$hitArea.remove();
					self.$hitArea = null;
					self.element
						.removeClass("wijmo-wijtree-parent")
						.addClass(nodeClass);
				}
			}

			if (!self._hasChildren && self.$nodes && self.$nodes.length) {
				self.$nodes.css({ display: "none" });
			}

			if (o.selected && self.$inner) {
				self.$inner.addClass(o.wijCSS.stateActive);
			}
		}

		_initNodesUL() {
			var self = this;

			if (!self._isHtmlGenerated() && self._tree.options.showExpandCollapse) {
				if (self._hasChildren && self.$nodes && self.$nodes.length) {
					self.$nodes[self._expanded ? 'show' : 'hide']();
				}
			}
		}

		_initNavigateUrl() {
			var self = this, href: string;

			if (self._isHtmlGenerated()) {
				return;
			}
			href = self.$navigateUrl.attr("href");
			if (!this._isTemplate) {
				if (!href || href.length === 0) {
					self._setNavigateUrlHref("#");
				}
			}
		}

		_applyIconClass($el: JQuery, o: WijTreeNodeOptions) {
			var vAttr = $el.attr("expandediconclass");

			if (vAttr) {
				o.expandedIconClass = vAttr;
				$el.removeAttr("expandediconclass");
			}
			vAttr = $el.attr("collapsediconclass");
			if (vAttr) {
				o.collapsedIconClass = vAttr;
				$el.removeAttr("collapsediconclass");
			}
			vAttr = $el.attr("itemiconclass");
			if (vAttr) {
				o.itemIconClass = vAttr;
				$el.removeAttr("itemiconclass");
			}
		}

		_initNodeImg() { // ui-icon instead of image
			var self = this, o = self.options, $el = self.element;
			if (self.$nodeImage === null || !self.$nodeImage.length) {
				self.$nodeImage = $("<span>");
			}

			/* initial html has icon attribute for asp.net mvc*/
			self._applyIconClass($el, o);
			/* end */

			if (o.collapsedIconClass !== "" && o.expandedIconClass !== "") {
				self.$nodeImage.removeClass().addClass(o.wijCSS.icon)
					.addClass(o.expanded ? o.expandedIconClass : o.collapsedIconClass);
				if (!self._tree.options.showExpandCollapse) {
					self.$nodeImage.addClass(o.expandedIconClass);
				}
				self.$nodeImage.insertBefore(self.$checkBox);
			} else if (o.itemIconClass !== "") {
				self.$nodeImage.removeClass().addClass(o.wijCSS.icon);
				self.$nodeImage.addClass(o.itemIconClass);
				self.$nodeImage.insertBefore(self.$checkBox);
			}
		}

		_setNavigateUrlHref(href: string) {
			if (this.$navigateUrl) {
				if (href === "" || typeof href === "undefined" || href === null) {
					href = "#";
				}
				this.$navigateUrl.attr("href", href);
			}
		}

		_editNode() {
			this._tree._editMode = true;
			this.$navigateUrl.hide();
			if (!this.$editArea) {
				this.$editArea = $("<input type=\"text\">").wijtextbox();
			}
			this.$editArea.val(this.$text.html());
			this.$editArea.insertBefore(this.$navigateUrl);
			this.$editArea.bind("blur", this, this._editionComplete);
			this.$editArea.focus();
		}

		_editionComplete(event: JQueryEventObject) {
			var self: wijtreenode = event.data, text: string;
			self._tree._editMode = false;
			if (self.$editArea) {
				text = self.$editArea.val();
				self.$editArea.remove();
			}
			self.$navigateUrl.show();
			self.$editArea = null;
			self._changeText(text);
		}

		_changeText(text: string) {
			var self = this, o = self.options;
			if (self.$text !== null && text !== "") {
				self.$text.text(text);
				o.text = text;
				self._tree._trigger("nodeTextChanged", null, self);
			}
		}

		/*behavior Methods*/
		_toggleExpanded() { // access
			var self = this, o = self.options;
			if (!self._isClosestDisabled()) {
				if (self._hasChildren || o.hasChildren) {
					self._setExpanded(!o.expanded);
				}
			}
		}

		_createChildNodeWidget() {
			var self = this, nodes: wijtreenode[] = [];
			if (!self._getField("nodes")) {
				if (self._hasChildren) {
					nodes = self._createChildNode();
				}
				self._setField("nodes", nodes);
			}
		}

		_expandNode(expand: boolean) {
			var self = this, treeOption = self._tree.options,
				trigger = expand ? "nodeExpanding" : "nodeCollapsing";

			if (self._tree._trigger(trigger, null, {
				node: this,
				params: this.options.params
			}) === false) {
				return;
			}

			self._createChildNodeWidget();
			self.$nodeBody.attribute("aria-expanded", expand);
			self._expanded = expand;
			self.options.expanded = expand;

			if (!self._isClosestDisabled()) {
				if (expand) {
					if (treeOption.expandDelay > 0) {
						window.clearTimeout(self._expandTimer);
						self._expandTimer = window.setTimeout(function () {
							self._expandNodeVisually();
						}, treeOption.expandDelay);
					} else {
						self._expandNodeVisually();
					}
				} else {
					if (treeOption.collapseDelay > 0) {
						window.clearTimeout(self._collapseTimer);
						self._collapseTimer = window.setTimeout(function () {
							self._collapseNodeVisually();
						}, treeOption.collapseDelay);
					} else {
						self._collapseNodeVisually();
					}
				}
			}
		}

		_expandNodeVisually() {
			var self = this, $nodes: JQuery, o = self.options;
			if (self._tree.options.autoCollapse) {
				$nodes = self.element.siblings(":" + this.widgetFullName);
				$.each($nodes, function (i) {
					var widget = self._getNodeWidget($nodes[i]);
					if (widget.options.expanded) {
						widget._setExpanded(false);
					}
				});
			}
			if (o.collapsedIconClass !== "" && o.expandedIconClass !== "") {
				self.$nodeImage.removeClass(o.collapsedIconClass)
					.addClass(o.expandedIconClass);
			}
			self._internalSetNodeClass(true);
			self._show();
		}

		_collapseNodeVisually() {
			var self = this;
			if (self.options.collapsedIconClass !== "" && self.options.expandedIconClass !== "") {
				self.$nodeImage.removeClass(self.options.expandedIconClass)
					.addClass(self.options.collapsedIconClass);
			}

			if (self._tree._focusNode &&
				self.$nodes.find(self._tree._focusNode.$navigateUrl).length) {
				self._setFocused(true);
			}
			self._internalSetNodeClass(false);
			self._hide();
		}

		_internalSetNodeClass(expanded) {
			var css = this.options.wijCSS,
				iconCss = [css.icon, css.iconArrowRightDown, css.iconArrowRight].join(' ');
			if (!this.$hitArea) {
				return;
			}
			this.$hitArea.removeClass(iconCss).addClass(css.icon).addClass(expanded ? css.iconArrowRightDown : css.iconArrowRight);
			this.$nodeBody.removeClass("wijmo-state-expanded").removeClass("wijmo-state-collapsed");
			if (expanded) {
				this.$nodeBody.addClass("wijmo-state-expanded");
			} else {
				this.$nodeBody.addClass("wijmo-state-collapsed");
			}
		}

		_show() {
			this._animation(true);
		}

		_hide() {
			this._animation(false);
		}

		_animation(show: boolean) {
			var self = this, $nodes = self.$nodes,
				animate = show ? "expandAnimation" : "collapseAnimation",
				event = show ? "nodeExpanded" : "nodeCollapsed",
				effect: string,
				animation: wijtree_animation = self._tree.options[animate],
				opacity: string,
				strOpacity: string;

			function restoreOpacity($ele: JQuery) {
				if ($ele.css(strOpacity) !== opacity) {
					$ele.css(strOpacity, opacity);
				}
			}

			if ($nodes && $nodes.length) {
				if (animation) {
					if ($.browser.msie && parseInt($.browser.version) < 9) {
						strOpacity = "filter";
					} else {
						strOpacity = "opacity";
					}
					opacity = $nodes.css(strOpacity);
					effect = animation.animated || animation.effect;
					if ($.effects && !!effect) {
						$nodes[show ? "show" : "hide"](effect, {
							easing: animation.easing
						},
							animation.duration,
							function () {
								restoreOpacity($nodes);
								self._tree._trigger(event, null, self);
							});
					} else {
						$nodes[show ? "show" : "hide"](animation.duration, function () {
							restoreOpacity($nodes);
							self._tree._trigger(event, null, self);
						});
					}
				} else {
					$nodes[show ? "show" : "hide"]();
					self._tree._trigger(event, null, self);
				}
			}
		}

		_getBounds($el: JQuery): Rect { // get top,left,height,width of element
			var h = $el.height(), w = $el.width(),
				t = $el.offset().top, l = $el.offset().left;
			return { h: h, w: w, t: t, l: l };
		}

		_isMouseInsideRect(p: Position, b: Rect): boolean { // whether mouse is over a element
			if (p.x < b.l || p.x >= b.l + b.w) {
				return false;
			}
			if (p.y <= b.t + 1 || p.y >= b.t + b.h) {
				/*fix 1px on the mouse out the element 
				(e.g. 31<30.98 now 31<30.98+1 maybe 
				pageY/PageX are int but left/top are float)*/
				return false;
			}
			return true;
		}

		/**
		 * not used
		 */
		_getNodeByMouseOn(p: Position): JQuery {
			var self = this;
			$("li").each(function () {
				var b = self._getBounds($(this));
				//$.ui.isOver is removed from jQuery UI 1.10, so remove this method.
				//if ($.ui.isOver(p.y, p.x, b.t, b.l, b.h, b.w)) {
				//    return $(this);
				//}
				if ((p.y > b.t) && (p.y < (b.t + b.h))
					&& (p.x > b.l) && (p.x < (b.l + b.w))) {
					return $(this);
				}
			});
			return null;
		}

		_drowTemplate(p: Position, $tmpl: JQuery, $target: JQuery) {
			var position = "unKnown",
				body = $target.is(".wijmo-wijtree-node") ? $target : $target.children(".wijmo-wijtree-node"),
				n = this._getBounds(body);
			$tmpl.width(body.width());

			if (p.y > n.t && p.y < n.t + n.h / 2) {
				$tmpl.offset({ left: n.l, top: n.t });
				position = "before";
			} else if (p.y > n.t + n.h / 2 && p.y < n.t + n.h) {
				$tmpl.offset({ left: n.l, top: n.t + n.h });
				position = "after";
			}
			return position;
		}

		_beginDrag(e: JQueryEventObject) { // set draggable
			var self = this, $item = self.element, $drag,
				to = self._tree.options,
				draggable = to.draggable,
				$tmpl = $("<div>").addClass("wijmo-wijtree-insertion " + to.wijCSS.stateDefault),
				options: JQueryUI.DraggableOptions = {
					cursor: "point",
					cursorAt: { top: 15, left: -25 },
					helper: function () {
						return $("<div>" + self.$navigateUrl.html() + "</div>")
							.addClass(to.wijCSS.header).addClass(to.wijCSS.cornerAll);
					},
					distance: $.browser.msie ? 1 : 10,
					handle: self.$navigateUrl,
					scope: "tree"
				},
				events: JQueryUI.DraggableEvents = {
					start: function (event, ui) {
						self._tree._isDragging = true;
						self._tree.widgetDom.prepend($tmpl);
						self._tree._trigger("nodeDragStarted", event, self);
						if (draggable && $.isFunction(draggable.start)) {
							draggable.start.call(self.element, event, ui);
						} else {
							$item.hide();
						}
					},
					drag: function (event, ui) {
						var ev = <JQueryEventObject>event,
							t = <Element>(ev.srcElement || ev.originalEvent.target),
							$target = $(t),
							dropNode: wijtreenode,
							p: Position = { x: ev.pageX, y: ev.pageY };

						if ($tmpl) {
							$tmpl.hide();
						}
						if ($target) {
							dropNode = self._getNodeWidget(t);
							if (dropNode && !dropNode._tree._isDisabled()) {
								if ($target.closest("." + localCSS.wijtreeInner, self.element).length) {
									self._insertPosition = "end"; //end,after,before
								} else {
									$tmpl.show();
									self._insertPosition = self._drowTemplate(p, $tmpl, dropNode.element);
								}
								if (dropNode !== self) {
									self._dropTarget = dropNode;
								}
							} else if ($target.is(":" + self.options.treeClass)) {
								self._dropTarget = $target.data(self.options.treeClass);
								self._insertPosition = "end";
							}
						}
						self._tree._trigger("nodeDragging", event, self);
						if (draggable && $.isFunction(draggable.drag)) {
							draggable.drag.call(self.element, event, ui);
						}
					},
					stop: function (event, ui) {
						$tmpl.remove();
						self._dropTarget = null;
						self._insertPosition = "unKnown";
						self._tree._isDragging = false;
						if (draggable && $.isFunction(draggable.stop)) {
							draggable.stop.call(self.element, event, ui);
						} else {
							$item.show();
							self._resetDrag();
						}
					}
				};

			if (typeof to.dropVisual === "string") {
				$drag = $(to.dropVisual);
				$tmpl = $drag.length ? $drag : $tmpl;
			} else if ($.isFunction(to.dropVisual)) {
				$drag = $(to.dropVisual.call());
				$tmpl = $drag.length ? $drag : $tmpl;
			}
			$tmpl.hide();

			$.extend(options, draggable, events); // mind the order: events must be at last.
			if ($.fn.draggable) {
				$item.draggable(options).trigger(e);
				if ($.browser.mozilla) {
					self._setFocused(true);
				}
			}
		}

		_resetDrag() {
			var self = this, nodes: wijtreenode[], i: number, o = self.options;
			if (!self._isAllowDrag() && self.element.data("ui-draggable")) {
				self.element.draggable("destroy");
			}
			nodes = self.getNodes();
			for (i = 0; i < nodes.length; i++) {
				nodes[i]._resetDrag();
			}
		}

		_createTreeCheck() {
			var self = this, o = self.options,
				html = generateTreeCheckMarkup(o.checked, o.checkState);

			self.$checkBox = $(html);
			self.$checkBox.insertAfter(self.$nodeImage);
			self.$checkBoxBody = self.$checkBox.children("div");
			self.$checkBoxIcon = self.$checkBoxBody.children("span");
		}

		_mouseOverTreeCheck(event: JQueryEventObject) {
			var $el = $(event.target), self = this, o = self.options, css = o.wijCSS;
			if (self._isOverCheckbox($el)) {
				self.$checkBoxBody.addClass(css.stateHover);
			}
		}

		_mouseOutTreeCheck(event: JQueryEventObject) {
			var $el = $(event.target), self = this, o = self.options, css = o.wijCSS;
			if (self._isOverCheckbox($el)) {
				self.$checkBoxBody.removeClass(css.stateHover);
			}
		}

		_setTreeCheckState(checkState: string) {
			var self = this, o = self.options, css = o.wijCSS,
				checkClass = css.icon + " " + css.iconCheck,
				triStateClass = css.icon + " " + css.iconStop;

			if (checkState === "unCheck") {
				self.$checkBoxBody.removeClass(css.stateActive);
				self.$checkBoxIcon.removeClass(checkClass + " " + triStateClass);
			} else if (checkState === "check") {
				self.$checkBoxBody.addClass(css.stateActive);
				self.$checkBoxIcon.removeClass(triStateClass).addClass(checkClass);
			} else if (checkState === "triState") {
				self.$checkBoxBody.addClass(css.stateActive);
				self.$checkBoxIcon.removeClass(checkClass).addClass(triStateClass);
			}
		}

		_checkClick() {
			var self = this, o = self.options;
			if (!self._isClosestDisabled()) {
				if (self._tree._trigger("nodeCheckChanging", null, self) === false) {
					return;
				}
				if (o.checked && self._checkState === "indeterminate") {
					self._checkState = "checked";
					self._checkItem();
				} else {
					self._checkState = o.checked ? "unChecked" : "checked";
					self._setChecked(!o.checked);
				}
				self._tree._trigger("nodeCheckChanged", null, self);
			}
		}

		_checkItem() {
			var self = this, autoCheck = false, tree = self._tree;
			if (tree === null || !tree.options.showCheckBoxes) {
				return;
			}
			if (tree.options.autoCheckNodes &&
				self._checkState !== "indeterminate") {
				autoCheck = true;
				self._changeChecked(self.options.checked);
			}
			if (tree.options.allowTriState) {
				self._setParentCheckState();
			}
			self.options.checked ? self._checkNode(autoCheck) : self._unCheckNode(autoCheck);
		}

		_checkNode(autoCheck: boolean) {
			// todo: add to tree._checkedNodes
			var self = this, o = self.options, nodes = this.getNodes(), i;
			if (self._checkState === "checked") {
				self._setTreeCheckState("check");
				o.checkState = "checked";
			} else if (self._checkState === "indeterminate") { // todo: tristate Style
				self._setTreeCheckState("triState");
				o.checkState = "indeterminate";
			}

			if (autoCheck) {
				for (i = 0; i < nodes.length; i++) {
					nodes[i]._checkNode(true);
				}
			}
		}

		_unCheckNode(autoCheck: boolean) {
			// todo: remove to tree._checkedNodes
			var nodes: wijtreenode[] = this.getNodes(), o = this.options, i;
			this._setTreeCheckState("unCheck");
			o.checkState = "unChecked";
			if (autoCheck) {
				for (i = 0; i < nodes.length; i++) {
					nodes[i]._unCheckNode(true);
				}
			}
		}

		_changeChecked(checked: boolean) {
			var nodes: wijtreenode[] = this.getNodes();
			$.each(nodes, function (i, node) {
				node.options.checked = checked;
				node.$nodeBody.attribute("aria-checked", checked);
				node._checkState = checked ? "checked" : "unChecked";
				node._changeChecked(checked);
			});
		}

		_setParentCheckState() { // set parent check state
			var owner = this._getOwner(), nodes: wijtreenode[], allChecked = true,
				hasChildrenChecked = false, triState = false, i: number, self = this;
			if (owner.element.is(":" + self.options.treeClass)) {
				return;
			}
			nodes = owner.getNodes();
			for (i = 0; i < nodes.length; i++) {
				if (nodes[i]._checkState === "indeterminate") {
					triState = true;
				}
				if (nodes[i].options.checked) {
					hasChildrenChecked = true;
				} else {
					allChecked = false;
				}
				if (!allChecked && hasChildrenChecked) {
					break;
				}
			}
			if (triState) {
				owner._checkState = "indeterminate";
				owner._setChecked(true);
			} else {
				if (hasChildrenChecked) {
					if (allChecked) {
						owner._checkState = "checked";
						owner._checkNode(false);
					} else {
						owner._checkState = "indeterminate";
					}
					owner._setChecked(true);
				} else {
					owner._checkState = "unChecked";
					owner._setChecked(false);
					owner._unCheckNode(false);
				}
			}
			owner._setParentCheckState();
		}

		/*Events*/
		// todo: make self assignment consistent. (need to refactor other places.)

		_isOverCheckbox(el: JQuery): boolean {
			return el.closest(".wijmo-checkbox", this.element).length > 0;
		}

		_isWithinHitArea(el: JQuery): boolean {
			return this.$hitArea && this.$hitArea[0] === el[0];
		}

		_isOverInner(el: JQuery): boolean {
			return el.closest("." + localCSS.wijtreeInner, this.element).length > 0;
		}

		_onKeyDown(event: JQueryEventObject) {
			var el = $(event.target), self = this;
			if (self._isOverInner(el)) {
				//update for Move focus to next control with Tab key.
				if (event.keyCode === wijmo.getKeyCodeEnum().TAB) {
					return true;
				}
				self._keyAction(event);
			}
		}

		_onClick(event: JQueryEventObject) {
			var el = $(event.target), self = this;
			if (self._isOverCheckbox(el)) {
				self._checkClick();
				event.preventDefault();
				event.stopPropagation();
			} else if (self._isWithinHitArea(el)) {
				self._toggleExpanded();
				event.preventDefault();
				event.stopPropagation();
			} else if (self._isOverInner(el)) {
				self._click(event);
			}
		}

		_onMouseDown(event: JQueryEventObject) {
			var el = $(event.target), node: wijtreenode = event.data;
			if (!node._tree._isDisabled() && node._isAllowDrag()) { // prepare for drag
				if (el.closest(".wijmo-wijtree-node", node.element).length > 0) {
					node._beginDrag(event);
				}
			}
		}

		_onMouseOver(event: JQueryEventObject) {
			var el = $(event.target), self = this, rel = $(event.relatedTarget);
			if (self._isOverInner(el) &&
				(this._tree._overNode !== self || rel.is(':' + this.widgetFullName) || rel.is('.wijmo-wijtree-node'))) {
				if (!self._isClosestDisabled()) {
					self._mouseOver(event);
				}
				this._tree._overNode = self;
			}
			if (!self._isClosestDisabled()) {
				self._mouseOverHitArea(event);
				self._mouseOverTreeCheck(event);
			}
		}

		_onMouseOut(event: JQueryEventObject) {
			var el = $(event.target), self = this,
				rel = $(event.relatedTarget), node = this._getNodeWidget(rel.get(0));
			if (self._isOverInner(el) &&
				(this._tree._overNode !== node || rel.is(':' + this.widgetFullName) ||
				rel.is('.wijmo-wijtree-list') || rel.is('.ui-effects-wrapper') || rel.is('.wijmo-wijtree-node'))) {
				if (!self._isClosestDisabled()) {
					self._mouseOut(event);
				}
				this._tree._overNode = null;
			}
			if (!self._isClosestDisabled()) {
				self._mouseOutHitArea(event);
				self._mouseOutTreeCheck(event);
			}
		}

		_onFocus(event: JQueryEventObject) {
			var el = $(event.target), self = this, css = self.options.wijCSS;
			if (self._isOverInner(el) && !self._isClosestDisabled() &&
				!(el.hasAllClasses(css.iconArrowRightDown) || el.hasAllClasses(css.iconArrowRight)) &&
				!self._isOverCheckbox(el)) {
				if (self._tree._focusNode) {
					self._tree._focusNode.$navigateUrl.trigger("focusout");
					enableTabFocus(self._tree._focusNode.$navigateUrl, false);
				} else {
					enableTabFocus(self._tree.nodes[0].$navigateUrl, false);
				}

				enableTabFocus(el, true);
				self._focused = true;
				self._tree._focusNode = this;
				self.$inner.addClass(css.stateFocus);
				self._tree._trigger("nodeFocus", event, self);
			}
		}

		_onBlur(event: JQueryEventObject) {
			var el = $(event.target), node: wijtreenode = event.data, css = node.options.wijCSS;
			if (!node._isClosestDisabled()) {
				node._focused = false;
				if (node._isOverInner(el)) {
					node.$inner.removeClass(css.stateFocus);
				}
				node._tree._trigger("nodeBlur", event, node);
			}
		}

		_click(event: JQueryEventObject) {
			var self = this, o = self.options, tree = self._tree,
				url = self.$navigateUrl.attr("href");
			if (!self._isClosestDisabled()) {
				if (!/^[#,\s]*$/.test(url)) {
					if ($.browser.msie && /^7\.[\d]*/.test($.browser.version)) {
						if (url.indexOf(window.location.href) < 0) {
							return;
						}
					} else {
						return;
					}
				}
				self._isClick = true;
				tree._ctrlKey = event.ctrlKey;
				if (o.selected && tree._ctrlKey) {
					self._setSelected(false);
				} else if (o.selected && !self._tree._editMode && tree.options.allowEdit && !self._isTemplate) {
					self._editNode();
				} else {
					self._setSelected(!o.selected);
				}
				if (!self._isTemplate) {
					event.preventDefault();
					event.stopPropagation();
				}
			} else {
				self._setNavigateUrlHref("");
			}
		}

		_selectNode(select: boolean, event?: JQueryEventObject) {
			var self = this, o = self.options, ctrlKey: boolean, idx: number;
			if (!self._isClosestDisabled() && !self._tree._isDragging) {
				ctrlKey = self._tree._ctrlKey;
				if (ctrlKey) {
					idx = $.inArray(self, self._tree._selectedNodes);
					if (idx !== -1 && !select) {
						self._tree._selectedNodes.splice(idx, 1);
						self.$inner.removeClass(o.wijCSS.stateActive);
					}
				} else {
					$.each(self._tree._selectedNodes, function (i, n) {
						n.$inner.removeClass(o.wijCSS.stateActive);
						n.options.selected = false;
						n.$nodeBody.attribute("aria-selected", false);
					});
					self._tree._selectedNodes = [];
				}
				if (select) {
					idx = $.inArray(self, self._tree._selectedNodes);
					if (idx === -1) {
						this._tree._selectedNodes.push(self);
					}
					self.$inner.addClass(o.wijCSS.stateActive);
				} else {
					self.$inner.removeClass(o.wijCSS.stateActive);
				}
				if (self._isClick) {
					self._tree._trigger("nodeClick", event, self);
				}
				self._isClick = false;
				self._tree._ctrlKey = false;
				self._tree._trigger("selectedNodeChanged", event, self);
			}
		}

		_keyAction(e: JQueryEventObject) {
			var el = e.target, self = this,
				keyCode = wijmo.getKeyCodeEnum();
			if (self._isClosestDisabled()) {
				return;
			}
			if (el) {
				if (self._tree._editMode && e.keyCode !== keyCode.ENTER) {
					return;
				}
				switch (e.keyCode) {
					case keyCode.UP:
						self._moveUp();
						break;
					case keyCode.DOWN:
						self._moveDown();
						break;
					case keyCode.RIGHT:
						if (self._tree.options.showExpandCollapse) {
							self._moveRight();
						}
						break;
					case keyCode.LEFT:
						if (self._tree.options.showExpandCollapse) {
							self._moveLeft();
						}
						break;
					case 83: //key s
						if (!self._tree._editMode && self._tree.options.allowSorting) {
							self.sortNodes();
						}
						break;
					case 113: //key f2
						if (self._tree.options.allowEdit) {
							self._editNode();
						}
						break;
					case 109: //key -
						if (self._tree.options.showExpandCollapse && this._expanded) {
							self._setExpanded(false);
						}
						break;
					case 107: //key +
						if (self._tree.options.showExpandCollapse && !this._expanded) {
							self._setExpanded(true);
						}
						break;
					case keyCode.ENTER:
						if (self._tree._editMode) {
							e.data = self;
							self._editionComplete(e);
							self._setFocused(true);
							e.preventDefault();
						}
						break;
					case keyCode.SPACE: //check
						if (self._tree.options.showCheckBoxes) {
							self._checkClick();  //fix bug 113652
						}
						break;
				}
				if (!self._isTemplate && e.keyCode !== keyCode.ENTER) {
					e.preventDefault();
					e.stopPropagation();
				}
			}
		}

		_prevNode(node: wijtreenode): wijtreenode {
			var el = node.element;
			if (el.prev().length > 0) {
				return el.prev().data(<string>el.data("widgetName"));
			}
		}

		_nextNode(node: wijtreenode): wijtreenode {
			var el = node.element;
			if (el.next().length > 0) {
				return el.next().data(<string>el.data("widgetName"));
			}
		}

		_getNextExpandedNode(node: wijtreenode): wijtreenode {
			var nextNode = node, nextNodes: wijtreenode[] = node.getNodes(), newNode: wijtreenode;
			if (node._expanded && nextNodes.length > 0) {
				newNode = nextNodes[nextNodes.length - 1];
				if (newNode !== null) {
					nextNode = this._getNextExpandedNode(newNode);
				}
			}
			return nextNode;
		}

		_getNextNode(owner: any): wijtreenode {
			var nextNode: wijtreenode = null, self = this;
			if (owner.element.is(":" + self.options.treeClass)) {
				return null;
			}
			nextNode = self._nextNode(owner);
			if (nextNode) {
				return nextNode;
			}
			return self._getNextNode(owner._getOwner());
		}

		_moveUp() {
			var level = this._getCurrentLevel(), prevNode = this._prevNode(this);
			if (!prevNode) {
				if (level > 0) {
					this._getOwner()._setFocused(true);
				}
			} else {
				this._getNextExpandedNode(prevNode)._setFocused(true);
			}
		}

		_moveDown() { // sometimes blur
			var nodes: wijtreenode[] = this.getNodes(), nextNode: wijtreenode, owner: any, pNextNode: wijtreenode;
			if (this._expanded && nodes.length > 0) {
				nodes[0]._setFocused(true);
			} else {
				nextNode = this._nextNode(this);
				if (nextNode) {
					nextNode._setFocused(true);
				} else {
					owner = this._getOwner();
					pNextNode = this._getNextNode(owner);
					if (pNextNode) {
						pNextNode._setFocused(true);
					}
				}
			}
		}

		_moveLeft() {
			var nextNode = this._getOwner();
			if (this._expanded) {
				this._setExpanded(false);
			} else if (nextNode !== null && !nextNode.element.is(":" + this.options.treeClass)) {
				nextNode._setFocused(true);
			}
		}

		_moveRight() {
			if (this._hasChildren) {
				if (!this._expanded) {
					this._setExpanded(true);
				} else {
					var nextNode = this.getNodes()[0];
					if (nextNode !== null) {
						nextNode._setFocused(true);
					}
				}
			}
		}

		_mouseOver(event: JQueryEventObject) {
			var self = this, tree = self._tree;
			if (!tree._editMode) {
				self._mouseOverNode();
				if (!tree._isDragging) {
					tree._trigger("nodeMouseOver", event, self);
				}
			}
		}

		_mouseOut(event: JQueryEventObject) {
			var self = this, tree = self._tree;
			if (!tree._editMode) {
				self._mouseOutNode();
				if (!tree._isDragging) {
					tree._trigger("nodeMouseOut", event, self);
				}
			}
		}

		_mouseOverNode() {
			if (this.$inner !== null && !this._isOverNode) {
				this.$inner.addClass(this.options.wijCSS.stateHover);
				this._isOverNode = true;
			}
		}

		_mouseOutNode() {
			if (this.$inner !== null && this._isOverNode) {
				this.$inner.removeClass(this.options.wijCSS.stateHover);
				this._isOverNode = false;
			}
		}

		_mouseOverHitArea(event: JQueryEventObject) {
			var bound: Rect, p: Position, self = this, tree = self._tree;
			if (tree.options.expandCollapseHoverUsed) {
				if (self._hasChildren && !self._isOverHitArea) {
					bound = self._getBounds(self.element);
					p = { x: event.pageX, y: event.pageY };
					if (self._isMouseInsideRect(p, bound)) {
						self._isOverHitArea = true;
						self._setExpanded(true);
					}
				}
			}
		}

		_mouseOutHitArea(event: JQueryEventObject) {
			var p: Position = { x: event.pageX, y: event.pageY }, bound: Rect,
				self = this, tree = self._tree;
			if (tree.options.expandCollapseHoverUsed) {
				if (self._hasChildren && !!self._isOverHitArea) {
					bound = self._getBounds(self.element);
					if (!self._isMouseInsideRect(p, bound)) {
						self._isOverHitArea = false;
						self._setExpanded(false);
					}
				} else if (self._getOwner().element.is(":" + self.widgetFullName)) {
					bound = self._getBounds(self._getOwner().element);
					if (!self._isMouseInsideRect(p, bound)) {
						self._getOwner()._isOverHitArea = false;
						self._getOwner()._setExpanded(false);
					}
				}
			}
		}

		/*public methods*/

		/** 
		* Destroy the node widget.
		*/
		destroy() {
			var self = this, $nodes: JQuery, o = self.options, $childnode;
			if (self.element.data("ui-draggable")) {
				self.element.draggable("destroy");
			}
			if (self.$hitArea) {
				self.$hitArea.remove();
			}
			if (self.$checkBox) {
				self.$checkBox.remove();
			}
			if (self.$nodeImage) {
				self.$nodeImage.remove();
			}
			self._unbindCreateNodesEvent();
			self.$navigateUrl.unwrap().unwrap().removeClass(o.wijCSS.stateDefault)
				.removeClass(o.wijCSS.stateActive).unbind("mousedown");
			$nodes = self.element.find("ul:first").show();
			$nodes.removeClass();

			$nodes.children("li").each(function () {
				$childnode = $(this);
				var nodeWidget = getTreeNodeWidget($childnode);
				if (nodeWidget) {
					nodeWidget.destroy();
				}
			});

			self.element.removeData("nodes").removeData("owner")
				.removeData("widgetName").removeClass();

			super.destroy();
		}

		/** 
		* The add method adds a node to the node.
		* @example $("#treenode1").wijtreenode("add", "node 1", 1);
		* @param {string|object} node 
		* 1.markup html.such as "<li><a>node</a></li>" as a node.
		* 2.wijtreenode element.
		* 3.object options according to the options of wijtreenode.
		* 4. node's text.
		* @param {number} position The position to insert at.
		*/
		add(node: any, position: number) {
			var nodeWidget: wijtreenode = null, $node: JQuery, nodes: wijtreenode[],
				self = this, cnodes: wijtreenode[], i: number,
				itemDom: string, originalLength: number, $link: JQuery, $checkbox: JQuery;

			if ($.browser.safari) {
				itemDom = "<li><a href='{0}'>{1}</a></li>";
			} else {
				itemDom = "<li><a href='{0}' class='wijmo-wijtree-link'>{1}</a></li>";
			}
			if (typeof node === "string") {
				$node = $(itemDom.replace(/\{0\}/, "#").replace(/\{1\}/, node));
				self._createNodeWidget($node);
				nodeWidget = $node.data(<string>$node.data("widgetName"));
			} else if (node.jquery) {
				if (!node.data("widgetName")) {
					self._createNodeWidget(node);
				}
				nodeWidget = node.data(node.data("widgetName"));
			} else if (node.nodeType) {
				$node = $(node);
				self._createNodeWidget($node);
				nodeWidget = $node.data(<string>$node.data("widgetName"));
			} else if ($.isPlainObject(node)) {
				$node = $(itemDom.replace(/\{0\}/, node.url ? node.url : "#")
					.replace(/\{1\}/, node.text)); //node
				node.isAddedNodeWithOptions = true;
				self._createNodeWidget($node, node);
				nodeWidget = $node.data(<string>$node.data("widgetName"));
			}

			if (nodeWidget === null) {
				return;
			}

			if (!$.browser.safari) {
				$link = self.element.children(".wijmo-wijtree-node").find("." + localCSS.wijtreeInner + " .wijmo-wijtree-link");
				if ($link && $link.length > 0) {
					$link.removeClass("wijmo-wijtree-link");
				}

				$checkbox = self.element.children(".wijmo-wijtree-node").find("." + localCSS.wijtreeInner + " .wijmo-checkbox");
				if ($checkbox && $checkbox.length > 0) {
					$checkbox.removeClass("wijmo-checkbox");
				}
			}

			nodes = self.getNodes();
			if (!position || position > nodes.length) {
				if (position !== 0) {
					position = nodes.length;
				}
			}

			if ($.mobile) {
				nodeWidget.element.find("a").addClass("ui-link");
			}

			cnodes = nodeWidget.getNodes();
			nodeWidget._tree = self._tree;
			for (i = 0; i < cnodes.length; i++) {
				cnodes[i]._tree = self._tree;
			}
			nodeWidget._setField("owner", self);
			originalLength = nodes.length;
			if (!self.$nodes || !self.$nodes.length) {
				self.$nodes = $("<ul></ul>").addClass("wijmo-wijtree-list")
					.addClass(self.options.wijCSS.helperReset).addClass("wijmo-wijtree-child");
				self.element.append(self.$nodes);
			}
			if (originalLength > 0 && originalLength !== position) {
				if (nodeWidget.element.get(0) !== nodes[position].element.get(0)) {
					nodeWidget.element.insertBefore(nodes[position].element);
				}
			} else {
				self.$nodes.append(nodeWidget.element);
			}
			self.$subnodes = null;
			self._changeCollection(position, nodeWidget);
			self._collectionChanged();
			nodeWidget._initNodeClass();

			if (!$.browser.safari) {
				self._restoreStyles($link, $checkbox);
				setTimeout(function () {
					self._restoreStyles($link, $checkbox);
				}, 0);
			}
		}

		_restoreStyles($link: JQuery, $checkbox: JQuery) {
			if ($link && $link.length > 0) {
				$link.addClass("wijmo-wijtree-link");
			}
			if ($checkbox && $checkbox.length > 0) {
				$checkbox.addClass("wijmo-checkbox");
			}
		}

		/** 
		* The remove method removes the indicated node from this node.
		* @example $("#tree").wijtree("remove", 1);
		* @param {string|object} node
		* which node to be removed
		* 1.wijtreenode element.
		* 2.the zero-based index of which node you determined to remove.
		*/
		remove(node: any) {
			var idx = -1, nodeWidget: wijtreenode, self = this, nodes: wijtreenode[] = this.getNodes();
			if (node.jquery) {
				idx = node.index();
			} else if (typeof node === "number") {
				idx = node;
			}
			if (idx < 0 || idx >= nodes.length) {
				return;
			}
			nodeWidget = nodes[idx];
			nodeWidget.element.detach();
			removeARIA(nodeWidget.element);
			self.$subnodes = null;
			self._changeCollection(idx);
			self._collectionChanged();
		}

		/** 
		* The getNodes method gets an array that contains the root nodes of the current tree node.
		* @example $("#tree").wijtree("getNodes");
		* @return {Array}
		*/
		getNodes(): wijtreenode[] {
			if (this._hasChildren) {
				this._createChildNodesPartially(true);
			} else {
				this._setField("nodes", []);
			}
			return this._getField("nodes");
		}

		_changeCollection(idx: number, nodeWidget?: wijtreenode) {
			var nodes = this.getNodes(), ons = this.options.nodes;
			if (nodeWidget) {
				nodes.splice(idx, 0, nodeWidget);
				ons.splice(idx, 0, nodeWidget.options);
			} else {
				nodes.splice(idx, 1);
				ons.splice(idx, 1);
			}
		}

		/** 
		* Sorts the child nodes of the node.
		*/
		sortNodes() {
			var nodes = this.getNodes();
			this._sort();
			$.each(nodes, function (i, childNode) {
				childNode._index = i;
				childNode._insertBefore(i);
			});
			this._refreshNodesClass();
		}

		/** 
		* Checks or unchecks the node.
		* @param {boolean} value Check or uncheck the node.
		*/
		check(value: boolean) {
			this._setOption("checked", value);
		}

		/** 
		* Selects or unselects the node.
		* @param {boolean} value select or unselect the node.
		*/
		select(value: boolean) {
			this._setOption("selected", value);
		}

		/** 
		* Get owner which contains the node.
		*/
		getOwner(): any {
			var owner = this._getOwner();
			if (owner && owner.element.is("li")) {
				return owner;
			}
			return null;
		}

		/** 
		* Expands the node.
		*/
		expand() {
			this._setOption("expanded", true);
		}

		/** 
		* Collapses the node.
		*/
		collapse() {
			this._setOption("expanded", false);
		}

		/*region prvite Methods*/
		_insertBefore(i: number) {
			var $lis = this.element.parent().children("li");
			if (this.element.index() !== i) {
				this.element.insertBefore($lis.eq(i));
			}
		}

		_sort() {
			var nodes = this.getNodes();
			if (this._isSorted) {
				if (!this._isDecsSort) {
					nodes.sort(this._compare2NodeTextAsc);
					this._isDecsSort = true;
				} else {
					nodes.sort(this._compare2NodeTextDesc);
					this._isDecsSort = false;
				}
			} else {
				nodes.sort(this._compare2NodeTextAsc);
				this._isSorted = true;
				this._isDecsSort = true;
			}
		}

		_compare2NodeTextAsc(a: wijtreenode, b: wijtreenode) {
			if (a !== null && b !== null) {
				return a._text.localeCompare(b._text);
			}
		}

		_compare2NodeTextDesc(a: wijtreenode, b: wijtreenode) {
			if (a !== null && b !== null) {
				return -1 * a._text.localeCompare(b._text);
			}
		}

		_collectionChanged() {
			this._hasChildren = this._hasSubNodes();
			this._initNodeClass();
			//this._refreshNodesClass();
		}

		_refreshNodesClass() {
			var nodes = this.getNodes(), i: number;
			for (i = 0; i < nodes.length; i++) {
				nodes[i]._initNodeClass();
			}
		}

		_setChecked(value: boolean) {
			var self = this;
			if (self.options.checked === value && self._checkState !== "indeterminate") {
				return;
			}
			self.options.checked = value;
			self.$nodeBody.attribute("aria-checked", value);
			this._checkItem();
		}

		_isClosestDisabled(): boolean {
			var self = this,
				disabledClass = "." + self._tree.widgetFullName + "-disabled,." + self.widgetFullName + "-disabled";
			if (self.element.closest(disabledClass, self._tree.element).length) {
				return true;
			}
			return false;
		}

		_setExpanded(value: boolean) {
			var self = this, o = self.options;
			if (self._expanded === value) {
				return;
			}
			if (self._hasChildren || o.hasChildren) {
				self._expandNode(value);
			}
		}

		_setFocused(value: boolean) {
			if (value) {
				this.$navigateUrl.focus();
				this._setFocusNode();
			} else {
				this.$navigateUrl.trigger("focusout");
			}
		}

		_setFocusNode() {
			if (this._tree._focusNode && $.browser.webkit) {
				this._tree._focusNode.$navigateUrl.trigger("focusout");
			}
			this._focused = true;
			this._tree._focusNode = this;
			this.$inner.addClass(this.options.wijCSS.stateFocus);
		}

		_setToolTip(value: string) {
			if (value.length) {
				this.element.attr("title", value);
			} else {
				this.element.removeAttr("title");
			}
		}

		_setText(value: string) {
			if (this._text !== value && value.length) {
				this._text = value;
				this._changeText(value);
			}
		}

		_setSelected(value: boolean) {
			var self = this, o = self.options;
			if (o.selected !== value) {
				o.selected = value;
				self.$nodeBody.attribute("aria-selected", value);
				self._selectNode(value);
				self._setFocused(value);
			}
		}

		_setCheckBoxes(value: boolean) {
			var self = this;
			if (self.$checkBox) {
				self.$checkBox[value ? 'show' : 'hide']();
			} else if (value) {
				self._createTreeCheck();
			}

			$.each(self.getNodes(), function (idx, node) {
				node._setCheckBoxes(value);
			});
		}

		_setHitArea(value: boolean) {
			var self = this;
			if (self._hasChildren) // todo: initnode class
			{
				if (value) {
					self._initNodeClass();
					if (self.$hitArea) {
						self.$hitArea.show();
					}
				} else {
					self._expanded = true;
					self.options.expanded = true;
					self.$nodeBody.attribute("aria-expanded", true);
					if (self.$nodes && self.$nodes.length) {
						self.$nodes.show();
					}
					self._initNodeClass();
					if (self.$hitArea) {
						self.$hitArea.hide();
					}
				}
			}
			$.each(self.getNodes(), function (idx, node) {
				node._setHitArea(value);
			});
		}

		_getOwner(): any {
			return this._getField("owner");
		}

		_getTree(): wijtree {
			var owner = this._getOwner();
			if (owner) {
				if (owner._tree) {
					return owner._tree;
				} else {
					return owner;
				}
			}
			return null;
		}

		_getInitElement() {
			var li = $("<li>"), self = this, ul = $("<ul>"),
				nodes = self.getNodes();
			li.append(self.$navigateUrl.clone());
			if (nodes.length) {
				li.append(ul);
				$.each(nodes, (i, n) => {
					var c = n._getInitElement();
					ul.append(c);
				});
			}
			return li;
		}

		_updateSubNodes() {
			var lis: JQuery, i: number;

			this.$subnodes = [];
			if (this.$nodes === null || !this.$nodes.length) {
				return;
			}
			lis = this.$nodes.children("li");
			for (i = 0; i < lis.length; i++) {
				this.$subnodes.push($(lis[i]));
			}
		}

		_hasSubNodes() {
			if (this.options.nodes && this.options.nodes.length > 0) {
				return true;
			}
			if (this._isHtmlGenerated()) {
				return false;
			}
			if (this.$nodes === null) {
				this.$nodes = this.element.children("ul").eq(0);
			}
			if (this.$nodes.length === 0) {
				return false;
			}
			if (this.$subnodes === null) {
				this._updateSubNodes();
			}
			return this.$subnodes.length > 0;
		}

		_getNodeWidget(el: Element): wijtreenode {
			var node = this._getNodeByDom(el), widget: wijtreenode;
			if (node.length > 0) {
				widget = node.data(<string>node.data("widgetName"));
				return widget;
			}
			return null;
		}

		_createNodeWidget($li: JQuery, options?: any): JQuery {
			return createNodeWidget($li, options, this, nodeWidgetName, this.options.treeClass);
		}

		_getNodeByDom(el: Element): JQuery {
			return $(el).closest(":" + this.widgetFullName);
		}

		_getCurrentLevel(): number {
			return this.element.parentsUntil(":" + this.options.treeClass).length - 1;
		}

		_getField(key: string): any {
			return this.element.data(key);
		}

		_setField(key, value) {
			this.element.data(key, value);
		}

		_setAllowDrag(value) {
			var self = this;
			if (value) {
				self.$navigateUrl.one("mousedown." + self.widgetName, self, self._onMouseDown);
			} else if (self.element.data("ui-draggable")) {
				self.element.draggable("destroy");
			}
		}

		_isAllowDrag(): boolean {
			var self = this, no = self.options, to = self._tree.options;
			if (no.allowDrag || (to.allowDrag && no.allowDrag !== false)) {
				return true;
			} else {
				return false;
			}
		}

		_isAllowDrop(): boolean {
			var self = this, no = self.options, to = self._tree.options;
			if (no.allowDrop || (to.allowDrop && no.allowDrop !== false)) {
				return true;
			} else {
				return false;
			}
		}
	}

	class wijtreenode_options implements WijTreeNodeOptions {
		/**
		* wijCSS
		* @ignore
		*/
		wijCSS = {
			wijtreeInner: ""
		};
		/** wijMobileCSS
		* @ignore
		*/
		wijMobileCSS = {
			header: "ui-header ui-bar-a",
			content: "ui-body-b",
			stateDefault: "ui-btn ui-btn-b",
			stateHover: "ui-btn-down-c",
			stateActive: "ui-btn-down-b"
		};
		/** @ignore */
		treeClass = "wijmo-wijtree";
		/** Selector option for auto self initialization. 
		*   This option is internal.
		* @ignore
		*/
		initSelector = ":jqmData(role='wijtreenode')";
		/** @ignore */
		accessKey = "";
		/** The checked option checks the tree node checkbox when it is set to true. It will uncheck the tree node checkbox when set to false.
		*/
		checked = false;
		/** The collapsedIconClass option sets the collapsed node icon (based on ui-icon) for the specified nodes.
		*/
		collapsedIconClass = "";
		/** The expanded option will expand the tree node if set to "true." It will collapse the tree node if set to "false.".
		*/
		expanded = false;
		/** The expandedIconClass option sets the expanded node icon (based on ui-icon) for the specified nodes.
		*/
		expandedIconClass = "";
		/** The itemIconClass option sets the node icon (based on ui-icon). It will be displayed on both expanded and collapsed nodes when the expandedIconClass and collapsedIconClass options are not specified.
		*/
		itemIconClass = "";
		/** The navigateUrl option sets the node's navigate url link.
		*/
		navigateUrl = "";
		/** The selected option selects the specified node when set to true, otherwise it unselects the node.
		*/
		selected = false;
		/** This option sets the node's text.
		*/
		text = "";
		/** The toolTip option sets the node's tooltip.
		*/
		toolTip = "";
		/** The hasChildren option determines whether the specified node has child nodes. It's always used when you're custom adding child nodes, such as in an async load.
		*/
		hasChildren = false;
		/** The params option sets the parameter needed to pass when the user is custom loading child nodes.
		*/
		params = {};
		/** Determines the child nodes of this nodes.
		*/
		nodes = null;
		/** Determines whether to enable node dragging.
		* Default value is null which means whether to enable node dragging depends on the setting of treeview's allowDrag option.
		*/
		allowDrag = null;
		/** Determines whether to enable node dropping.
		* Default value is null which means whether to enable node dropping depends on the setting of treeview's allowDrop option.
		*/
		allowDrop = null;
	}

	class wijtree_options implements WijTreeOptions {
		/**
		* wijMobileCSS
		* @ignore
		*/
		wijMobileCSS = {
			header: "ui-header ui-bar-a",
			content: "ui-body-b",
			stateDefault: "ui-btn ui-btn-b",
			stateHover: "ui-btn-down-b",
			stateActive: "ui-btn-down-c"
		};
		/** When the allowDrag option is set to true, the tree nodes can be dragged.
		*/
		allowDrag = false;
		/** When allowDrop is set to true, one tree node can be dropped within another tree node.
		*/
		allowDrop = false;
		/** The allowEdit option allows a user to edit the tree nodes at run time.
		*/
		allowEdit = false;
		/** The allowSorting option allows the tree nodes to be sorted at run time when the user presses the "s" key.
		*/
		allowSorting = true;
		/** The allowTriState option allows the tree nodes to exhibit triState behavior. This lets the node checkboxes be checked, unchecked, or indeterminate. This option must be used with the showCheckBoxes option.
		*/
		allowTriState = true;
		/** The autoCheckNodes option allows the sub-nodes to be checked when the parent nodes are checked. To use this option, showCheckboxes must be set to "true."
		*/
		autoCheckNodes = true;
		/** If this option is set to true, 
		* the expanded node will be collapsed if another node is expanded.
		*/
		autoCollapse = false;
		/** If set to true, the select, click, 
		* and check operations are disabled too.
		*/
		disabled = false;
		/** The expandCollapseHoverUsed option allows the tree to expand or collapse when the mouse hovers over the expand/collapse button. 
		*/
		expandCollapseHoverUsed = false;
		/** The showCheckBoxes option allows the node Check Box to be shown on the tree nodes.
		*/
		showCheckBoxes = false;
		/** The showExpandCollapse option determines if the tree is displayed in an expanded or collapsed state. If set to "false," then the wijtree widget will be displayed in the expanded state.
		*/
		showExpandCollapse = true;
		/** The expandAnimation option determines the animation effect, easing, and duration for showing child nodes when the parent node is expanded.
		*/
		expandAnimation: wijtree_animation = { effect: "blind", easing: "linear", duration: 200 };
		/** The expandDelay option controls the length of time in milliseconds to delay before the node is expanded.
		*/
		expandDelay = 0;
		/** The collapseAnimation option determines the animation effect, easing, and duration for hiding child nodes when the parent node is collapsed.
		*/
		collapseAnimation: wijtree_animation = { effect: "blind", easing: "linear", duration: 200 };
		/** This option controls the length of time in milliseconds to delay before the node collapses.
		*/
		collapseDelay = 0;
		/** Customize the jquery-ui-draggable plugin of wijtree.
		*/
		draggable = null;
		/** Customize the jquery-ui-droppable plugin of wijtree.
		*/
		droppable = null;
		/** Customizes the helper element to be used to display the position that
		* the node will be inserted to. 
		* If a function is specified, it must return a DOMElement.
		* @type {String|Function}
		*/
		dropVisual = null;
		/** Set the child nodes object array as the datasource of wijtree.
		* @type {Array}
		* @example 
		* // Supply a function as an option.
		* $(".selector").wijtree("option","nodes",
		* [{ text:"node1", navigateUrl:"#" }]);
		*/
		nodes = null;
		/** The nodeBlur event fired when the node loses focus.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeBlur = null;
		/** The nodeFocus event fired when the node is focused.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeFocus = null;
		/** The nodeClick event fires when a tree node is clicked.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeClick = null;
		/** The nodeCheckChanging event fires before a node is checked.
		* You can cancel this event by returning false.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeCheckChanging = null;
		/** The nodeCheckChanged event fires when a node is checked.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeCheckChanged = null;
		/** The nodeCollapsed event fires when a tree node is collapsed.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeCollapsed = null;
		/** The nodeExpanded event handler.
		* A function called when a node is expanded.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeExpanded = null;
		/** The nodeDragging event handler.A function called
		* when the node is moved during a drag-and-drop operation. 
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeDragging = null;
		/** The nodeDragStarted event fires when a user starts to drag a node.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeDragStarted = null;
		/** The nodeBeforeDropped event handler is called before a draggable node is dropped in another position. If the event handler returns false, the drop action will be prevented. 
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeBeforeDropped = null;
		/** The nodeDropped event is called when an acceptable draggable node is dropped over to another position.
		* @event
		* @dataKey {jQuery} sourceParent The source parent of current draggable node before it be dragged, a jQuery object.
		* @dataKey {number} sIndex The Index of dragged node in source parent.
		* @dataKey {jQuery} targetParent The target parent of current draggable node after it be dropped, a jQuery object.
		* @dataKey {number} tIndex The Index of dragged node in target parent.
		* @dataKey {jQuery} draggable The current draggable node.
		* @dataKey {object} offset The current absolute position of the draggable helper.
		* @dataKey {object} position The current position of the draggable helper.
		*/
		nodeDropped = null;
		/** The nodeMouseOver event fires when a user places the mouse pointer over a node.
		* @event
		* @param {object} event jQuery.Event object.
		* @param {object} data The node widget that relates to this event.
		*/
		nodeMouseOver = null;
		/** The nodeMouseOut event fires when the user moves the mouse pointer off of a node.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeMouseOut = null;
		/** The nodeTextChanged event fires when the text of a node changes.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeTextChanged = null;
		/** The selectedNodeChanged event fires when the selected node changes.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		selectedNodeChanged = null;
		/** The nodeExpanding event fires before a tree node is expanded.
		* This event can be canceled, if return false 
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeExpanding = null;
		/** The nodeCollapsing event fires before a node collapses.
		* This event can be canceled, if return false
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeCollapsing = null;
	}

	wijtreenode.prototype.options = $.extend(true, {}, wijmoWidget.prototype.options, new wijtreenode_options());
	wijtree.prototype.options = $.extend(true, {}, wijmoWidget.prototype.options, new wijtree_options());

	$.wijmo.registerWidget("wijtree", wijtree.prototype);
	$.wijmo.registerWidget("wijtreenode", wijtreenode.prototype);

	export interface wijtree_animation {
		/** The options of animation.*/
		animated?: any;
		/** The animation effect that is used when the wijtree is collapsed. 
		  * Please see the Animation Effects topic for a list of the available animation effects.
		  */
		effect: string;
		/** A value that indicates the easing function that will be applied to the animation. 
		  * For more information, please see the Easing topic.
		  */
		easing: string;
		/** The duration of the animation in milliseconds.*/
		duration: number;
	}

	/** 
	* The interface of WijTreeOptions.
	*/
	export interface WijTreeOptions extends WidgetOptions {
		/**
		* wijMobileCSS
		* @ignore
		*/
		wijMobileCSS: WijmoCSS;
		/** When the allowDrag option is set to true, the tree nodes can be dragged.
		*/
		allowDrag: boolean;
		/** When allowDrop is set to true, one tree node can be dropped within another tree node.
		*/
		allowDrop: boolean;
		/** The allowEdit option allows a user to edit the tree nodes at run time.
		*/
		allowEdit: boolean;
		/** The allowSorting option allows the tree nodes to be sorted at run time when the user presses the "s" key.
		*/
		allowSorting: boolean;
		/** The allowTriState option allows the tree nodes to exhibit triState behavior. This lets the node checkboxes be checked, unchecked, or indeterminate. This option must be used with the showCheckBoxes option.
		*/
		allowTriState: boolean;
		/** The autoCheckNodes option allows the sub-nodes to be checked when the parent nodes are checked. To use this option, showCheckboxes must be set to "true."
		*/
		autoCheckNodes: boolean;
		/** If this option is set to true, 
		* the expanded node will be collapsed if another node is expanded.
		*/
		autoCollapse: boolean;
		/** If set to true, the select, click, 
		* and check operations are disabled too.
		*/
		disabled: boolean;
		/** The expandCollapseHoverUsed option allows the tree to expand or collapse when the mouse hovers over the expand/collapse button. 
		*/
		expandCollapseHoverUsed: boolean;
		/** The showCheckBoxes option allows the node Check Box to be shown on the tree nodes.
		*/
		showCheckBoxes: boolean;
		/** The showExpandCollapse option determines if the tree is displayed in an expanded or collapsed state. If set to "false," then the wijtree widget will be displayed in the expanded state.
		*/
		showExpandCollapse: boolean;
		/** The expandAnimation option determines the animation effect, easing, and duration for showing child nodes when the parent node is expanded.
		*/
		expandAnimation: wijtree_animation;
		/** The expandDelay option controls the length of time in milliseconds to delay before the node is expanded.
		*/
		expandDelay: number;
		/** The collapseAnimation option determines the animation effect, easing, and duration for hiding child nodes when the parent node is collapsed.
		*/
		collapseAnimation: wijtree_animation;
		/** This option controls the length of time in milliseconds to delay before the node collapses.
		*/
		collapseDelay: number;
		/** Customize the jquery-ui-draggable plugin of wijtree.
		*/
		draggable: JQueryUI.Draggable;
		/** Customize the jquery-ui-droppable plugin of wijtree.
		*/
		droppable: JQueryUI.Droppable;
		/** Customizes the helper element to be used to display the position that
		* the node will be inserted to. 
		* If a function is specified, it must return a DOMElement.
		* @type {String|Function}
		*/
		dropVisual: any;
		/** Set the child nodes object array as the datasource of wijtree.
		* @type {Array}
		* @example 
		* // Supply a function as an option.
		* $(".selector").wijtree("option","nodes",
		* [{ text:"node1", navigateUrl:"#" }]);
		*/
		nodes: WijTreeNodeOptions[];
		/** The nodeBlur event fired when the node loses focus.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeBlur: (e: JQueryEventObject, data: any) => void;
		/** The nodeFocus event fired when the node is focused.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeFocus: (e: JQueryEventObject, data: any) => void;
		/** The nodeClick event fires when a tree node is clicked.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeClick: (e: JQueryEventObject, data: any) => void;
		/** The nodeCheckChanging event fires before a node is checked.
		* You can cancel this event by returning false.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeCheckChanging: (e: JQueryEventObject, data: any) => void;
		/** The nodeCheckChanged event fires when a node is checked.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeCheckChanged: (e: JQueryEventObject, data: any) => void;
		/** The nodeCollapsed event fires when a tree node is collapsed.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeCollapsed: (e: JQueryEventObject, data: any) => void;
		/** The nodeExpanded event handler.
		* A function called when a node is expanded.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeExpanded: (e: JQueryEventObject, data: any) => void;
		/** The nodeDragging event handler.A function called
		* when the node is moved during a drag-and-drop operation. 
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeDragging: (e: JQueryEventObject, data: any) => void;
		/** The nodeDragStarted event fires when a user starts to drag a node.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeDragStarted: (e: JQueryEventObject, data: any) => void;
		/** The nodeBeforeDropped event handler is called before a draggable node is dropped in another position. If the event handler returns false, the drop action will be prevented. 
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeBeforeDropped: (e: JQueryEventObject, data: any) => void;
		/** The nodeDropped event is called when an acceptable draggable node is dropped over to another position.
		* @event
		* @dataKey {jQuery} sourceParent The source parent of current draggable node before it be dragged, a jQuery object.
		* @dataKey {number} sIndex The Index of dragged node in source parent.
		* @dataKey {jQuery} targetParent The target parent of current draggable node after it be dropped, a jQuery object.
		* @dataKey {number} tIndex The Index of dragged node in target parent.
		* @dataKey {jQuery} draggable The current draggable node.
		* @dataKey {object} offset The current absolute position of the draggable helper.
		* @dataKey {object} position The current position of the draggable helper.
		*/
		nodeDropped: (e: JQueryEventObject, data: any) => void;
		/** The nodeMouseOver event fires when a user places the mouse pointer over a node.
		* @event
		* @param {object} event jQuery.Event object.
		* @param {object} data The node widget that relates to this event.
		*/
		nodeMouseOver: (e: JQueryEventObject, data: any) => void;
		/** The nodeMouseOut event fires when the user moves the mouse pointer off of a node.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeMouseOut: (e: JQueryEventObject, data: any) => void;
		/** The nodeTextChanged event fires when the text of a node changes.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeTextChanged: (e: JQueryEventObject, data: any) => void;
		/** The selectedNodeChanged event fires when the selected node changes.
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		selectedNodeChanged: (e: JQueryEventObject, data: any) => void;
		/** The nodeExpanding event fires before a tree node is expanded.
		* This event can be canceled, if return false 
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeExpanding: (e: JQueryEventObject, data: any) => void;
		/** The nodeCollapsing event fires before a node collapses.
		* This event can be canceled, if return false
		* @event
		* @param {jQuery.Event} e jQuery Event object
		* @param {object} data The node widget that relates to this event.
		*/
		nodeCollapsing: (e: JQueryEventObject, data: any) => void;
	}

	/** 
	* The interface of WijTreeNodeOptions.
	*/
	export interface WijTreeNodeOptions extends WidgetOptions {
		/**
		* wijCSS
		* @ignore
		*/
		wijCSS: WijTreeNodeCSS;
		/** wijMobileCSS
		* @ignore
		*/
		wijMobileCSS: WijmoCSS;
		/** @ignore */
		treeClass: string;
		/** @ignore */
		accessKey: string;
		/** The checked option checks the tree node checkbox when it is set to true. It will uncheck the tree node checkbox when set to false.
		*/
		checked: boolean;
		/** The collapsedIconClass option sets the collapsed node icon (based on ui-icon) for the specified nodes.
		*/
		collapsedIconClass: string;
		/** The expanded option will expand the tree node if set to "true." It will collapse the tree node if set to "false.".
		*/
		expanded: boolean;
		/** The expandedIconClass option sets the expanded node icon (based on ui-icon) for the specified nodes.
		*/
		expandedIconClass: string;
		/** The itemIconClass option sets the node icon (based on ui-icon). It will be displayed on both expanded and collapsed nodes when the expandedIconClass and collapsedIconClass options are not specified.
		*/
		itemIconClass: string;
		/** The navigateUrl option sets the node's navigate url link.
		*/
		navigateUrl: string;
		/** The selected option selects the specified node when set to true, otherwise it unselects the node.
		*/
		selected: boolean;
		/** This option sets the node's text.
		*/
		text: string;
		/** The toolTip option sets the node's tooltip.
		*/
		toolTip: string;
		/** The hasChildren option determines whether the specified node has child nodes. It's always used when you're custom adding child nodes, such as in an async load.
		*/
		hasChildren: boolean;
		/** The params option sets the parameter needed to pass when the user is custom loading child nodes.
		*/
		params: any;
		/** Determines the child nodes of this nodes.
		*/
		nodes: WijTreeNodeOptions[];
		/** Determines the node's checkState.
		* It can be set to "checked", "unchecked", or "indeterminate".
		*/
		checkState?: string;
		/** Determines the index of next node.
		*/
		nIndex?: number;
		/** The htmlGenerated option determines whether the node be generated from html.
		*/
		htmlGenerated?: boolean;
		/** The isAddedNodeWithOptions option determines whether the node be created by options.
		*/
		isAddedNodeWithOptions?: boolean;
		/** Determines whether to enable node dragging.
		* Default value is null which means whether to enable node dragging depends on the setting of wijtree's allowDrag option.
		*/
		allowDrag?: boolean;
		/** Determines whether to enable node dropping.
		* Default value is null which means whether to enable node dropping depends on the setting of wijtree's allowDrop option.
		*/
		allowDrop?: boolean;
	}

	/** @ignore */
	export interface WijTreeNodeCSS extends WijmoCSS {
		wijtreeInner: string;
	}

	/** 
	* The interface of Position.
	*/
	export interface Position {
		/** The x value of position.*/
		x: number;
		/** The y value of position.*/
		y: number;
	}

	/** 
	* The interface of Rect.
	*/
	export interface Rect {
		/** The height value of rect.*/
		h: number;
		/** The width value of rect.*/
		w: number;
		/** The left value of rect.*/
		l: number;
		/** The top value of rect.*/
		t: number;
	}
}

/** @ignore */
interface JQuery {
	wijtreenode: JQueryWidgetFunction;
	wijtree: JQueryWidgetFunction;
}
