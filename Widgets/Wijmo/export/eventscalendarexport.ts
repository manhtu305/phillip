﻿/// <reference path="../wijevcal/jquery.wijmo.wijevcal.ts"/>
/// <reference path="exportUtil.ts" />

module wijmo.exporter {

    /** Settings for eventscalendar exporting */
    export interface IEventsCalendarExportSettings extends ExportSetting {
        /** It has "Content" and "Options" mode. */
        method?: wijmo.exporter.ExportMethod;
        /**  The theme name which is applied to exported eventscalendar*/
        theme?: string;
    }

    /** @ignore */
    export class EventsCalendarExport {
        constructor(public serviceUrl: string, public method: wijmo.exporter.ExportMethod) {

        }
        options: EventsCalendarOptions;
        getExportData(options: EventsCalendarOptions): any {
            var result: any;
            this.options = options;
            this._handleExportOption();
            result = {
                fileName: options.fileName,
                width: options.width,
                height: options.height,
                type: options.type,
                pdfSettings: options.pdfSetting,
                styleString: options.styleString,
                linkList: options.linkList,
                theme: options.theme
            };
            if (this.method === wijmo.exporter.ExportMethod.Content) {
                result.content = options.HTMLContent;
            } else {
                result.options = options.widgetOptions;
                result.widgetName = options.widgetName;
            }
            return toJSON(result, (key, val) => {
                if (key === "type") {
                    //return ExportFileType[val];
                    switch (val) {
                        case ExportFileType.Bmp:
                            return "bmp";
                        case ExportFileType.Gif:
                            return "gif";
                        case ExportFileType.Jpg:
                            return "jpg";
                        case ExportFileType.Pdf:
                            return "pdf";
                        case ExportFileType.Tiff:
                            return "tiff";
                        default:
                            return "png";
                    }
                }
                return val;
            });
        }

        _handleExportOption() {
            var o = this.options, styleString: string, linkList: any[] = new Array();
            if (o.widget) {
                if (this.method === wijmo.exporter.ExportMethod.Content) {
                    o.HTMLContent = o.widget.element[0].outerHTML;
                }
                else {
                    o.widgetName = this._getWidgetName();
                    o.widgetOptions = toJSON(o.widget.options, function (k, v) {
                        if (v != null) {
                            if (k === "webServiceUrl") {
                                return "";
                            }
                            return v;
                        }
                    });
                }

                $.each($("head").children(), function (index, element) {
                    if (element.tagName === "STYLE") {
                        if ($(element).text()) {
                            styleString += $(element).text();
                        }
                    } else if (element.tagName === "LINK") {
                        linkList.push({
                            className: element.className,
                            href: element.href,
                            rel: element.rel,
                            linkType: element.type
                        });
                    }
                });
                o.styleString = styleString;
                o.linkList = linkList;
                o.width = o.widget.element.width();
                o.height = o.widget.element.height();
            }
        }

        _getWidgetName(): string {
            return this.options.widget ? "wijevcal" : "";
        }
    }

    /** @ignore */
    export interface EventsCalendarOptions {
        fileName?: string;
        width?: number;
        height?: number;
        widgetName?: string;
        widgetOptions?: any;
        HTMLContent?: string;
        type: ExportFileType;
        widget: wijmo.evcal.wijevcal;
        pdfSetting: PdfSetting;
        styleString?: string;
        linkList?: any[];
        theme?: string;
    }

    /** @ignore */
    export function innerExportEventsCalendar(exportSettings: any, type?: string, settings?: any, serviceUrl?: string, exportMethod?: string) {
        var t: string,
            mode: string,
            pdfSetting: any = {},
            sUrl: string,
            data: any,
            self = this,
            o = self.options,
            eventsCalendarExporter: any,
            options: EventsCalendarOptions,
            eventsCalendarExportSettings: IEventsCalendarExportSettings;
        t = type === undefined ? "png" : type;
        t = t.substr(0, 1).toUpperCase() + t.substr(1);
        mode = exportMethod === undefined ? "Options" : exportMethod;
        mode = mode.substr(0, 1).toUpperCase() + mode.substr(1);
        if (typeof settings === "string" && arguments.length === 3) {
            sUrl = settings;
        } else {
            sUrl = serviceUrl === undefined ? "http://demos.componentone.com/ASPNET/ExportService/exportapi/EventsCalendar" : serviceUrl;
            pdfSetting = settings;
        }

        if (!$.isPlainObject(exportSettings)) {
            eventsCalendarExportSettings = {
                serviceUrl: sUrl,
                fileName: exportSettings === undefined ? "export" : exportSettings,
                pdf: pdfSetting,
                method: ExportMethod[mode],
                exportFileType: wijmo.exporter.ExportFileType[t]
            };
        } else {
            eventsCalendarExportSettings = <IEventsCalendarExportSettings>exportSettings;
        }

        eventsCalendarExporter = new EventsCalendarExport(eventsCalendarExportSettings.serviceUrl, eventsCalendarExportSettings.method);
        options = {
            fileName: eventsCalendarExportSettings.fileName,
            widget: self,
            type: eventsCalendarExportSettings.exportFileType,
            pdfSetting: eventsCalendarExportSettings.pdf,
            theme: !!o.theme? o.theme : eventsCalendarExportSettings.theme
        };
        data = eventsCalendarExporter.getExportData(options);
        exportFile(eventsCalendarExportSettings, data);
    }

    if (wijmo.evcal && wijmo.evcal.wijevcal) {
        wijmo.evcal.wijevcal.prototype.exportEventsCalendar = innerExportEventsCalendar;
    }

    if ($.wijmo.wijevcal) {
        $.wijmo.wijevcal.prototype.exportEventsCalendar = innerExportEventsCalendar;
    }
}