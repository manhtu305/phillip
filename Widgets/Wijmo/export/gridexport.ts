/// <reference path="../wijgrid/Grid.ts/Grid/interfaces.ts" />
/// <reference path="../wijgrid/Grid.ts/Grid/wijgrid.ts" />
/// <reference path="exportUtil.ts" />
/// <reference path="../data/src/dataView.ts"/>

/** @ignore */
interface NavigatorID {
	language: string;
}

module wijmo.exporter {


	var $ = jQuery;

	/** @ignore */
	export interface Style {
		borderRightStyle?: string;
		fontWeight?: string;
		borderTopStyle?: string;
		borderRightWidth?: number;
		verticalAlign?: string;
		borderBottomStyle?: string;
		borderLeftStyle?: string;
		borderLeftWidth?: number;
		backgroundColor?: string;
		borderTopWidth?: number;
		borderLeftColor?: string;
		borderBottomColor?: string;
		borderBottomWidth?: number;
		textDecoration?: string;
		fontSize?: number;
		fontFamily?: string;
		fontStyle?: string;
		color?: string;
		textAlign?: string;
		borderRightColor?: string;
		paddingLeft?: number; // read it for text indent
	}

	/** @ignore */
	export interface CellPos {
		top: number;
		left: number;
	}


	/** The setting of grid export.*/
	export interface GridExportSetting extends ExportSetting {
		/** The setting of grid pdf export.*/
		pdf?: GridPdfSetting;
		/** The setting of grid excel export.*/
		excel?: ExcelSetting;
		/** The wijgrid that need to export.*/
		grid?: grid.wijgrid;
		/** A boolean value indicates whether only export the data in current page 
		when grid uses the paging. If not set, it works as true.*/
		onlyCurrentPage?: boolean;
		/** The row count when paging and exporting all pages.*/
		rowCountPerRequest?: number;
	}

	/** The setting of grid pdf export.*/
	export interface GridPdfSetting extends PdfSetting {
		/** A boolean value indicates whether repeat the grid header in each page. Used only for grid widget.*/
		repeatHeader?: boolean;
	}

	/** The setting of grid excel export.*/
	export interface ExcelSetting {
		/** A boolean value indicates whether row’s height is auto adjusted per the content.*/
		autoRowHeight?: boolean;
		/** Specifies the name of the person, company, or application that created this excel file.*/
		author?: string;
		/** A boolean value whether to show the gridlines in the sheet.*/
		showGridLines?: boolean;
	}

	/** @ignore */
	export interface Grid {
		style: Style;
		header: Section;
		footer: Section;
		body: Section;
		width: number;
		colWidths: number[];
	}

	/** @ignore */
	export interface Section {
		style: Style;
		rows: Row[];
		colInfos?: ColInfo[];
	}

	/** @ignore */
	export interface Row {
		style?: Style;
		height: number;
		cells?: Cell[];
		type?: RowType;
		templateType?: RowTemplateType;
		groupLevel?: number;
	}

	/** @ignore */
	export interface Cell {
		style?: Style;
		text?: string;
		value?: any;
		colIndex?: number;
		rowSpan?: number;
		colSpan?: number;
	}

	/** @ignore */
	export interface ColInfo {
		formatString: string;
		dataType: string;
		type: ColType;
		dataTypeQualifier?: string;
	}

	/** @ignore */
	export enum RowTemplateType {
		None,
		Template,
		AltTemplate,
		HeaderTemplate,
		FooterTemplate,
		GroupHeaderTemplate,
		GroupFooterTemplate
	}

	/** @ignore */
	export enum SectionType {
		Header,
		Body,
		Footer
	}

	/** @ignore */
	export enum RowType {
		Data,
		Header,
		Footer,
		GroupHeader,
		GroupFooter
	}

	/** @ignore */
	export enum ColType {
		Text = 1,
		Image,
		Link,
		Checkbox,
		Html
	};

	/** @ignore */
	export interface ImageInfo {
		src: string;
		width: number;
		height: number;
	}

	/** @ignore */
	export interface LinkInfo {
		href: string;
		text: string;
	}

	/** @ignore */
	export interface EnvironmentInfo {
		baseUrl: string;
		dpiX: number;
		dpiY: number;
		culture: string;
	}

	/** @ignore */
	export interface ExportSource {
		exportFileType: ExportFileType;
		fileName?: string;
		grid: Grid;
		environmentInfo: EnvironmentInfo;
		pdf?: GridPdfSetting;
		excel?: ExcelSetting;
	}

	/** @ignore */
	export class exportContext {
		environmentInfo: EnvironmentInfo;
		grid: grid.wijgrid;
		dataView: data.IDataView;
		isArrayData: boolean;
		showRowHeader: boolean;
		dataCount: number;
		colInfos: ColInfo[];
		colWidths: number[];
		colLeaves: grid.c1basefield[];
		$cols: JQuery;
		currentSectionType: SectionType;
		currentRowType: RowType;
		currentRowTemplateType: RowTemplateType;
		createdGroupHeaderTemplates: boolean[] = [];
		createdGroupFooterTemplates: boolean[] = [];

		constructor(grid: grid.wijgrid, environmentInfo: EnvironmentInfo) {
			var self = this;
			self.environmentInfo = environmentInfo;
			self._init(grid);
		}

		private _init(grid: grid.wijgrid) {
			var self = this,
				dataView = grid.dataView(),
				$cols = $(grid.element).find(">colgroup>col");
			self.grid = grid;
			self.showRowHeader = grid.options.showRowHeader;
			self.dataView = dataView;
			self.dataCount = dataView.count();
			if (self.dataCount > 0) {
				var first = dataView.item(0);
				self.isArrayData = $.type(first) == "array";
			}

			self._initColInfos($cols);
		}

		private _initColInfos($cols: JQuery) {
			var self = this;
			self.colLeaves = $.grep(self.grid.columns(), (element) => {
				return element.options._isLeaf;
			});
			self.colWidths = [];
			self.colInfos = $cols.map(function (colIndex: number) {
				if (colIndex < 0 || colIndex > self.colLeaves.length - 1)
					return null;

				var col = self.colLeaves[colIndex],
					type = ColType.Text,
					dataTypeQualifier = (<any>col.options).dataTypeQualifier; // c1gridview's column internal option

				if (self.isHtmlCol(col)) {
					type = ColType.Html;
				} else if (self.isLinkCol(col)) {
					type = ColType.Link;
				} else if (self.isImageCol(col)) {
					type = ColType.Image;
				} else if (self.isCheckBoxCol(col)) {
					type = ColType.Checkbox;
				}
				self.colWidths.push(self.computedWidthToPt($(this).css("width")));
				var colInfo: ColInfo = {
					formatString: (<wijmo.grid.IC1FieldOptions>col.options).dataFormatString,
					dataType: (<wijmo.grid.IC1FieldOptions>col.options).dataType,
					type: type
				};
				if (dataTypeQualifier) { colInfo.dataTypeQualifier = dataTypeQualifier; }
				return colInfo;
			}).get();
		}

		computedWidthToPt(lengthStr: string): number {
			return gridExporter.pxToPt(lengthStr, this.environmentInfo.dpiX);
		}

		computedHeightToPt(lengthStr: string): number {
			return gridExporter.pxToPt(lengthStr, this.environmentInfo.dpiY);
		}

		_getVisibleColumnIndex(colIndex: number): number {
			var result = this._findInColumns(this.grid.options.columns, colIndex);
			if (result.found) {
				return result.index;
			}
			return colIndex;
		}

		_findInColumns(columns: wijmo.grid.IColumn[], colIndex: number, index: number = 0) {
			var totalColumns = columns.length;
			var result = { index: index, col: colIndex, found: false };
			for (var i = 0; i < totalColumns; i++) {
				result = this._findInColumn(columns[i], result.col, result.index);
				if (result.found) {
					return result;
				}
			}
			return result;
		}

		_getColumnCount(column: wijmo.grid.IColumn): number {
			var subColumns = column.columns;
			if (!subColumns) {
				return 1;
			}
			var count = 0;
			subColumns.forEach((item, index, arr) => {
				count += this._getColumnCount(item);
			});
			return count;
		}

		_findInColumn(column: wijmo.grid.IColumn, colIndex: number, index: number) : any {
			var result = { index: index, col: colIndex, found: false };
			if (!column.visible) {
				var count = this._getColumnCount(column);
				result.col += count;
				result.index += count;
			} else if (!column.columns) {
				if (result.index == result.col) {
					result.found = true;
				} else {
					result.index++;
				}
			} else {
				result = this._findInColumns(column.columns, colIndex, index);
			}
			return result;
		}

		getDataValue(rowIndex: number, colIndex: number): any {
			var self = this;
			colIndex = this._getVisibleColumnIndex(colIndex);
			if (rowIndex >= self.dataCount || rowIndex < 0 || colIndex < 0) return null;
			var data = self.grid.dataView().item(rowIndex);
			if (self.isArrayData) {
				if (self.showRowHeader) colIndex--;
				if (colIndex < 0 || colIndex >= data.length) return null;
				return data[colIndex];
			}
			var cols = self.colLeaves;
			if (colIndex >= cols.length) return null;
			var dataKey = (<wijmo.grid.IC1FieldOptions>cols[colIndex].options).dataKey;
			return data[dataKey];
		}

		isCheckBoxCol(col: grid.c1basefield): boolean {
			return col instanceof grid.c1booleanfield;
		}

		isHtmlCol(col: grid.c1basefield): boolean {
			return (<wijmo.grid.IC1BaseFieldOptions>col.options).cellFormatter != null;
		}

		isImageCol(col: grid.c1basefield): boolean {
			return false;
		}

		isLinkCol(col: grid.c1basefield): boolean {
			return (col instanceof grid.c1buttonfield) && ((<grid.IC1ButtonBaseFieldOptions>col.options).buttonType === "link");
		}
	}

	/** @ignore */
	export function exportGrid(setting: GridExportSetting): void {
		gridExporter.exportFile(setting);
	}

	/** @ignore */
	export class gridExporter {
		static exportFile(setting: GridExportSetting) {
			var exportAction = exportSetting =>
				exporter.exportFile(exportSetting, toJSON(gridExporter._exportSource(exportSetting))),
				needNewSingleTable = gridExporter._needNewSingleTable(setting),
				needRequestAllData = gridExporter._needRequestAllData(setting);

			if (!needNewSingleTable && !needRequestAllData) {
				exportAction(setting);
				return;
			}

			var grid = setting.grid,
				totalRowCount = gridExporter._getTotalRowCount(grid);

			setting.rowCountPerRequest = setting.rowCountPerRequest || totalRowCount;
			if (grid.options.allowPaging && setting.rowCountPerRequest > 0
				&& setting.rowCountPerRequest < totalRowCount && !setting.onlyCurrentPage
				/*&& totalRowCount > 1000*/) {
				var pageCount = Math.ceil(totalRowCount * 1.0 / setting.rowCountPerRequest);
				gridExporter.exportChunk(setting, 0, pageCount);
				return;
			}

			var newOptions = gridExporter.createGridOptionsForAllData(setting, needRequestAllData);
			gridExporter.createGridWithExport(setting, newOptions, exportAction);
		}

		static _getTotalRowCount(grid: grid.wijgrid): number {
			return grid.options.allowPaging ? grid.pageCount() * grid.options.pageSize : -1;
		}

		static exportChunk(setting: GridExportSetting, pageIndex: number, pageCount: number, token?: string) {
			var exportAction = exportSetting => {
				var exportSource = gridExporter._exportSource(exportSetting);
					if (pageIndex + 1 == pageCount) {
						exporter.exportAllPages(setting.serviceUrl, toJSON(exportSource), token);
					} else {
						var data = {
							token: token,
							rows: exportSource.grid.body.rows
						};
						exporter.exportChunk(exportSetting, toJSON(data), token => gridExporter.exportChunk(setting, pageIndex + 1, pageCount, token));
					}
				};

			var newOptions = gridExporter.createGridOptionsForChunkData(setting, pageIndex);
			gridExporter.createGridWithExport(setting, newOptions, exportAction);
		}

		static createGridOptionsForChunkData(setting: GridExportSetting, pageIndex: number): grid.IWijgridOptions {
			var grid = <grid.wijgrid>setting.grid,
				oldOptions = grid.options,
				data,
				newOptions = <grid.IWijgridOptions>$.extend(true, {}, oldOptions, <grid.IWijgridOptions>{
					scrollMode: "none",
					allowPaging: oldOptions.allowPaging,
					allowVirtualScrolling: false,
					selectionMode: "none",
					scrollingSettings: {
						mode: "none",
						freezingMode: "none",
						staticColumnIndex: -1,
						staticColumnsAlignment: "left",
						staticRowIndex: -1,
						virtualizationSettings: {
							mode: "none",
							rowHeight: 0,
							columnWidth: 0
						}
					}
				});


			newOptions.pageIndex = pageIndex;
			newOptions.pageSize = setting.rowCountPerRequest;
			data = gridExporter._createChunkData(setting, pageIndex);

			wijmo.grid.traverse(newOptions.columns, (column, columns) => {
				if (column._dynamic) {
					// remove autogenerated columns
					var idx = $.inArray(column, columns);
					if (idx >= 0) {
						columns.splice(idx, 1);
					}
				} else {
					// restore original values
					column.dataKey = column._originalDataKey;
					column.headerText = column._originalHeaderText;
				}
			});

			newOptions.data = data;
			return newOptions;
		}

		private static _createChunkData(setting: GridExportSetting, pageIndex: number) {
			var grid = setting.grid,
				options = grid.options,
				oldData = options.data,
				newDataOptions;

			if (!oldData || !wijmo || !wijmo.data || !options.allowPaging
				|| setting.onlyCurrentPage) {
				return null;
			}

			newDataOptions = $.extend(true, {}, oldData.options || {}, { pageIndex: pageIndex, pageSize: setting.rowCountPerRequest, sort: oldData.sort(), filter: oldData.filter() });
			if (wijmo.data.ArrayDataView && oldData instanceof wijmo.data.ArrayDataView) {
				return new wijmo.data.ArrayDataView($.extend(true, [], oldData.getSource()), newDataOptions);
			}

			if (wijmo.data["AjaxDataView"] && oldData instanceof wijmo.data["AjaxDataView"]) {
				return new wijmo.data["AjaxDataView"](oldData.url, newDataOptions);
			}

			if (wijmo.data["ODataView"] && oldData instanceof wijmo.data["ODataView"]) {
				return new wijmo.data["ODataView"](oldData.url, newDataOptions);
			}

			if (wijmo.data["BreezeDataView"] && oldData instanceof wijmo.data["BreezeDataView"]) {
				return new wijmo.data["BreezeDataView"](oldData.query, oldData.manager, newDataOptions);
			}

			return null;
		}

		static createGridOptionsForAllData(setting: GridExportSetting, needRequestAllData: boolean): grid.IWijgridOptions {
			var grid = <grid.wijgrid>setting.grid,
				oldOptions = grid.options,
				data,
				newOptions = <grid.IWijgridOptions>$.extend(true, {}, oldOptions, <grid.IWijgridOptions>{
					scrollMode: "none",
					allowPaging: oldOptions.allowPaging,
					allowVirtualScrolling: false,
					selectionMode: "none",
					scrollingSettings: {
						mode: "none",
						freezingMode: "none",
						staticColumnIndex: -1,
						staticColumnsAlignment: "left",
						staticRowIndex: -1,
						virtualizationSettings: {
							mode: "none",
							rowHeight: 0,
							columnWidth: 0
						}
					}
				});

			if (needRequestAllData) {
				newOptions.pageSize = grid.pageCount() * oldOptions.pageSize;
				newOptions.pageIndex = 0;
				data = gridExporter._createAllData(setting);
			}

			wijmo.grid.traverse(newOptions.columns, (column, columns) => {
				if (column._dynamic) {
					// remove autogenerated columns
					var idx = $.inArray(column, columns);
					if (idx >= 0) {
						columns.splice(idx, 1);
					}
				} else {
					// restore original values
					column.dataKey = column._originalDataKey;
					column.headerText = column._originalHeaderText;
				}
			});

			newOptions.data = data || gridExporter._cloneData(setting.grid.dataView());
			return newOptions;
		}

		static createGridWithExport(setting: GridExportSetting, options: grid.IWijgridOptions, exportAction) {
			options.rendered = gridExporter.createGridRendered(setting, exportAction);
			$("<table>").appendTo($(document.body)).wijgrid(options);
		}

		static createGridRendered(setting: GridExportSetting, exportAction, container?: JQuery): () => void {
			return function () {
				var $e = $(this), newSetting: GridExportSetting = $.extend({}, setting);
				newSetting.grid = <any>$e.data(setting.grid.widgetFullName);
				exportAction(newSetting);
				setTimeout(() => {
					newSetting.grid.destroy();
					container = container || $e.closest(".wijmo-wijgrid");
					$e.remove();
					container.remove();
				});
			};
		}

		private static _createAllData(setting: GridExportSetting) {
			var grid = setting.grid,
				options = grid.options,
				oldData = options.data,
				newPageSize = grid.pageCount() * options.pageSize,
				newDataOptions;

			if (!oldData || !wijmo || !wijmo.data || !options.allowPaging
				|| grid.pageCount() <= 1 || setting.onlyCurrentPage) {
				return null;
			}

			newDataOptions = $.extend(true, {}, oldData.options || {}, { pageIndex: 0, pageSize: newPageSize, sort: oldData.sort(), filter: oldData.filter() });
			if (wijmo.data.ArrayDataView && oldData instanceof wijmo.data.ArrayDataView) {
				return new wijmo.data.ArrayDataView($.extend(true, [], oldData.getSource()), newDataOptions);
			}

			if (wijmo.data["AjaxDataView"] && oldData instanceof wijmo.data["AjaxDataView"]) {
				return new wijmo.data["AjaxDataView"](oldData.url, newDataOptions);
			}

			if (wijmo.data["ODataView"] && oldData instanceof wijmo.data["ODataView"]) {
				return new wijmo.data["ODataView"](oldData.url, newDataOptions);
			}

			if (wijmo.data["BreezeDataView"] && oldData instanceof wijmo.data["BreezeDataView"]) {
				return new wijmo.data["BreezeDataView"](oldData.query, oldData.manager, newDataOptions);
			}

			return null;
		}

		private static _cloneData(dataView: wijmo.data.IDataView) {
			var newData = [], count = dataView.count(), i;
			for (i = 0; i < count; i++) {
				newData.push($.extend(true, {}, dataView.item(i)));
			};
			return newData;
		}

		private static _exportSource(setting: GridExportSetting): ExportSource {
			var environmentInfo = wijmo.exporter.gridExporter._generateEnvironmentInfo();
			var exportSource: ExportSource = {
				grid: wijmo.exporter.gridExporter._generateTemplate(new exportContext(setting.grid, environmentInfo)),
				environmentInfo: environmentInfo,
				exportFileType: setting.exportFileType
			};
			if (setting.fileName) exportSource.fileName = setting.fileName;
			if (setting.pdf) exportSource.pdf = setting.pdf;
			if (setting.excel) exportSource.excel = setting.excel;
			return exportSource;
		}


		private static _needRequestAllData(setting: GridExportSetting): boolean {
			var grid = setting.grid,
				o = grid.options,
				isVirtualScrolling = o.allowVirtualScrolling,
				virtualScrollMode: string;
			if (!isVirtualScrolling && o.scrollingSettings && o.scrollingSettings.virtualizationSettings) {
				virtualScrollMode = o.scrollingSettings.virtualizationSettings.mode;
				if (virtualScrollMode === "both" || virtualScrollMode === "rows") {
					isVirtualScrolling = true;
				}
			}

			return (isVirtualScrolling && grid.options.totalRows > grid.dataView().getSource().length)
				|| (o.allowPaging && grid.pageCount() > 1 && setting.onlyCurrentPage === false);
		}

		private static _needNewSingleTable(setting: GridExportSetting): boolean {
			return setting.grid.options.scrollMode !== "none";
		}

		static pointPerInch: number = 72;

		// the length string's pattern should be like 0px
		static pxToPt(lengthStr: string, dpi: number): number {
			if (lengthStr == null) return 0;
			lengthStr =$.trim(lengthStr).toLowerCase();
			var suffix = "px";
			if (lengthStr.indexOf(suffix, lengthStr.length - suffix.length) == -1) {
				return 0;
			}
			var numberPart = parseFloat(lengthStr);
			if (isNaN(numberPart)) return 0;
			return numberPart / dpi * wijmo.exporter.gridExporter.pointPerInch;
		}

		private static _generateEnvironmentInfo(): EnvironmentInfo {
			var location = window.location,
				baseUrl = location.protocol + "//" + location.hostname + (location.port && ":" + location.port) + "/",
				dpiTestDiv = $("<div style='height:1in;left:-100%;position: absolute;top:-100%;width:1in;'></div>")
					.appendTo($(document.body));
			return {
				baseUrl: baseUrl,
				dpiX: dpiTestDiv.outerWidth(),
				dpiY: dpiTestDiv.outerHeight(),
				culture: navigator.language ? navigator.language : navigator.browserLanguage,
			}
		}

		private static _generateTemplate(context: exportContext): Grid {
			var $element = $(context.grid.element),
				style: Style = wijmo.exporter.gridExporter._readStyle(context, $element),
				header: Section = wijmo.exporter.gridExporter._readSection(context, $element.find(">thead"), SectionType.Header),
				body: Section = wijmo.exporter.gridExporter._readSection(context, $element.find(">tbody"), SectionType.Body),
				footer: Section = wijmo.exporter.gridExporter._readSection(context, $element.find(">tfoot"), SectionType.Footer),
				$parent: JQuery = $element.parent();
			if ($parent.hasClass("wijmo-wijgrid")) {
				var combinedStyle: Style = {};
				$.extend(combinedStyle, wijmo.exporter.gridExporter._readStyle(context, $parent), style);
				style = combinedStyle;
			}
			return {
				colWidths: context.colWidths,
				style: style,
				header: header,
				footer: footer,
				body: body,
				width: context.computedWidthToPt($element.css('width'))
			};
		}

		private static _readSection(context: exportContext, $section: JQuery, sectionType: SectionType): Section {
			context.currentSectionType = sectionType;
			return {
				colInfos: sectionType === SectionType.Body ? context.colInfos : null,
				style: wijmo.exporter.gridExporter._readStyle(context, $section),
				rows: wijmo.exporter.gridExporter._readRows(context, $section),
				height: context.computedHeightToPt($section.css('height'))
			};
		}

		private static _getGroupLevel($row: JQuery): number {
			var levelStr = $row.attr("aria-level");
			if (!levelStr) {
				return 0;
			}

			return parseInt(levelStr);
		}

		private static _isGroupHeader($row: JQuery): boolean {
			return $row.hasClass("wijmo-wijgrid-groupheaderrow");
		}

		private static _isGroupFooter($row: JQuery): boolean {
			return $row.hasClass("wijmo-wijgrid-groupfooterrow");
		}

		private static _readRows(context: exportContext, $section: JQuery): Row[] {
			if (!$section || !$section.length) return null;
			var dataRowIndex = -1;
			return $section.find(">tr").map(function () {
				var $row = $(this),
					rowType: RowType,
					templateType: RowTemplateType,
					row: Row = { height: context.computedHeightToPt($row.css("height")) };

				if (context.currentSectionType === SectionType.Header) {
					rowType = RowType.Header;
					templateType = RowTemplateType.HeaderTemplate;
				} else if (context.currentSectionType === SectionType.Footer) {
					rowType = RowType.Footer;
					templateType = RowTemplateType.FooterTemplate;
				} else if (gridExporter._isGroupHeader($row)) {
					rowType = RowType.GroupHeader;
					row.groupLevel = gridExporter._getGroupLevel($row);
					templateType = context.createdGroupHeaderTemplates[row.groupLevel] ?
					RowTemplateType.None : RowTemplateType.GroupHeaderTemplate;
					context.createdGroupHeaderTemplates[row.groupLevel] = true;
				} else if (gridExporter._isGroupFooter($row)) {
					rowType = RowType.GroupFooter;
					row.groupLevel = gridExporter._getGroupLevel($row);
					templateType = context.createdGroupFooterTemplates[row.groupLevel] ?
					RowTemplateType.None : RowTemplateType.GroupFooterTemplate;
					context.createdGroupFooterTemplates[row.groupLevel] = true;
				} else {
					++dataRowIndex;
					rowType = RowType.Data;
					if (dataRowIndex === 0) {
						templateType = RowTemplateType.Template;
					} else if (dataRowIndex === 1) {
						templateType = RowTemplateType.AltTemplate;
					} else {
						templateType = RowTemplateType.None;
					}
				}

				context.currentRowType = rowType;
				context.currentRowTemplateType = templateType;

				if (rowType != RowType.Data) {
					row.type = rowType;
				}

				if (templateType != RowTemplateType.None) {
					row.style = wijmo.exporter.gridExporter._readStyle(context, $row);
					row.templateType = templateType;
				}

				row.cells = gridExporter._readCells(context, $row, dataRowIndex);
				return row;
			}).get();
		}

		private static _readCells(context: exportContext, $row: JQuery, rowIndex: number): Cell[] {
			return $row.find(">th,>td").map(function (index: number) {
				var $cell = $(this),
					cell: Cell = {},
					cspan = wijmo.exporter.gridExporter._getSpan($cell, "colspan"),
					rspan = wijmo.exporter.gridExporter._getSpan($cell, "rowspan"),
					colIndex = wijmo.exporter.gridExporter._cellPos($cell).left,
					colInfo = context.colInfos[colIndex],
					innerCell = $cell.find(">.wijmo-wijgrid-innercell");

				if (innerCell.length == 0) innerCell = $cell;

				if (colIndex != index) cell.colIndex = colIndex;

				if (rspan != 1) cell.rowSpan = rspan;

				if (cspan != 1) cell.colSpan = cspan;

				if (context.currentRowType === RowType.Data && rowIndex > -1) {
					switch (colInfo.type) {
						case ColType.Image:
							var $img = $cell.find("img");
							if ($img.length != 0) {
								var imageInfo: ImageInfo = {
									src: $img.attr("src"),
									height: context.computedHeightToPt($img.css("height")),
									width: context.computedWidthToPt($img.css("width"))
								};
								cell.value = imageInfo;
							}
							break;
						case ColType.Link:
							var $link = $cell.find("a");
							if ($link.length != 0) {
								var linkInfo: LinkInfo = {
									href: $link.attr("href"),
									text: $link.text()
								};
								cell.value = linkInfo;
							}
							break;
						case ColType.Html:
							cell.value = $cell.html();
							break;
						case ColType.Text:
						case ColType.Checkbox:
							cell.value = context.getDataValue(rowIndex, colIndex);
							cell.text = $cell.text().replace(/\xA0/g, " ");
							break;
						default:
							throw "Wrong column type!";
					}
				} else {
					cell.text = $cell.text();
				}

				if (context.currentRowTemplateType != RowTemplateType.None) {
					cell.style = wijmo.exporter.gridExporter._readStyle(context, $cell);
					$.extend(cell.style, wijmo.exporter.gridExporter._readFontStyle(context, innerCell));
				}

				return cell;
			}).get();
		}

		private static _readStyle(context: exportContext, $element: JQuery): Style {
			var style: Style = {
				backgroundColor: $element.css("backgroundColor"),
				verticalAlign: $element.css("verticalAlign")
			};
			$.extend(style,
				wijmo.exporter.gridExporter._readBorderStyle(context, "Top", $element),
				wijmo.exporter.gridExporter._readBorderStyle(context, "Right", $element),
				wijmo.exporter.gridExporter._readBorderStyle(context, "Bottom", $element),
				wijmo.exporter.gridExporter._readBorderStyle(context, "Left", $element));
			return style;
		}

		private static _readBorderStyle(context: exportContext, borderType: string, $element: JQuery): Style {
			var style: Style = {},
				colorProp = "border" + borderType + "Color",
				styleProp = "border" + borderType + "Style",
				widthProp = "border" + borderType + "Width",
				borderStyle = $element.css(styleProp);
			if (borderStyle != "none") {
				var borderWidth = context.computedWidthToPt($element.css(widthProp));
				if (borderWidth != 0) {
					style[colorProp] = $element.css(colorProp);
					style[styleProp] = borderStyle;
					style[widthProp] = borderWidth;
				}
			}
			return style;
		}

		private static _readFontStyle(context: exportContext, $element: JQuery): Style {
			return {
				paddingLeft: context.computedHeightToPt($element.css("paddingLeft")), // read it for text indent
				textAlign: $element.css("textAlign"),
				color: $element.css("color"),
				fontFamily: $element.css("fontFamily"),
				fontWeight: $element.css("fontWeight"),
				fontStyle: $element.css("fontStyle"),
				fontSize: context.computedHeightToPt($element.css("fontSize")),
				textDecoration: $element.css("textDecoration")
			};
		}

		private static _cellPos($cell: JQuery, rescan?: boolean): CellPos {
			var pos: CellPos = $cell.data("cellPos");
			if (!pos || rescan) {
				var $table = $cell.closest("table, thead, tbody, tfoot");
				wijmo.exporter.gridExporter._scanTable($table);
			}
			pos = $cell.data("cellPos");
			return pos;
		}

		private static _getSpan($cell: JQuery, spanName: string): number {
			var span = $cell.attr(spanName);
			if (span) {
				var spanNumber = parseInt(span);
				if (!isNaN(spanNumber)) {
					return spanNumber;
				}
			}
			return 1;
		}

		private static _scanTable($table: JQuery): void {
			var m = [];
			$table.children("tr").each(function (y, row) {
				$(row).children("td, th").each(function (x, cell) {
					var $cell = $(cell),
						cspan = wijmo.exporter.gridExporter._getSpan($cell, "colspan"),
						rspan = wijmo.exporter.gridExporter._getSpan($cell, "rowspan"),
						tx, ty;
					cspan = cspan ? cspan : 1;
					rspan = rspan ? rspan : 1;
					for (; m[y] && m[y][x]; ++x);  //skip already occupied cells in current row
					for (tx = x; tx < x + cspan; ++tx) {  //mark matrix elements occupied by current cell with true
						for (ty = y; ty < y + rspan; ++ty) {
							if (!m[ty]) {  //fill missing rows
								m[ty] = [];
							}
							m[ty][tx] = true;
						}
					}
					var pos: CellPos = { top: y, left: x };
					$cell.data("cellPos", pos);
				});
			});
		}
	}

	var innerExportGrid = function (exportSettings: any, type: string, settings?: any, serviceUrl?: string) {
		var t: string,
			typeSetting: any = {},
			sUrl: string,
			gridExportSetting: GridExportSetting;
		t = type === undefined ? "csv" : type;
		t = t.substr(0, 1).toUpperCase() + t.substr(1);
		if (typeof settings === "string" && arguments.length === 3) {
			sUrl = settings;
		} else {
			sUrl = serviceUrl === undefined ? "http://demos.componentone.com/ASPNET/ExportService/exportapi/grid" : serviceUrl;
			typeSetting = settings;
		}

		if (!$.isPlainObject(exportSettings)) {
			gridExportSetting = {
				fileName: exportSettings === undefined ? "export" : exportSettings,
				serviceUrl: sUrl,
				grid: this,
				exportFileType: wijmo.exporter.ExportFileType[t]
			}
			if (gridExportSetting.exportFileType === ExportFileType.Pdf) {
				gridExportSetting.pdf = typeSetting;
			}
			if (gridExportSetting.exportFileType === ExportFileType.Xls ||
				gridExportSetting.exportFileType === ExportFileType.Xlsx) {
				gridExportSetting.excel = typeSetting;
			}
		} else {
			gridExportSetting = <GridExportSetting>exportSettings;
		}
		gridExporter.exportFile(gridExportSetting);
	}

	if (wijmo.grid && wijmo.grid.wijgrid) {
		wijmo.grid.wijgrid.prototype.exportGrid = innerExportGrid;
	}

	if ($.wijmo.wijgrid) {
		$.wijmo.wijgrid.prototype.exportGrid = innerExportGrid;
	}
}