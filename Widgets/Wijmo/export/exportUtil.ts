﻿/// <reference path="../external/declarations/jquery.d.ts" />

module wijmo.exporter {


	var $ = jQuery;

	export function toJSON(obj, replacer?: (key: string, value: any) => any): string {
		var originDateToJSON = Date.prototype.toJSON;
		// Use local time to json instead of UTC time.
		Date.prototype.toJSON = _localDateTimeToJSON;
		try {
			return JSON.stringify(obj, replacer);
		} finally {
			Date.prototype.toJSON = originDateToJSON;
		}
	}

	function _formatUint(uint, length): string {
		// Format unsigned integer to have at least specified digits.
		var uintStr = uint.toString();
		while (uintStr.length < length) {
			uintStr = '0' + uintStr;
		}

		return uintStr;
	}

	function _localDateTimeToJSON(): string {
		var date: Date = this;
		return _formatUint(date.getFullYear(), 4) + '-' +
			_formatUint(date.getMonth() + 1, 2) + '-' +
			_formatUint(date.getDate(), 2) + 'T' +
			_formatUint(date.getHours(), 2) + ':' +
			_formatUint(date.getMinutes(), 2) + ':' +
			_formatUint(date.getSeconds(), 2) + "." +
			_formatUint(date.getMilliseconds(), 3);
	}

	export function exportFile(setting: ExportSetting, content: any): void {
		var sender = setting.sender,
			contentType = setting.contentType,
			serviceUrl = setting.serviceUrl;
		if (sender) {
			sender(content, setting);
		}
		else if (contentType == "application/json") {
			_exportByXMLHttpRequest(content, serviceUrl, setting);
		} else {
			_exportByForm(content, serviceUrl);
		}
	}

	export function exportChunk(setting: ExportSetting, content: any, success: (token: string) => any): void {
		var serviceUrl = setting.serviceUrl + '/exportchunk';
		_exportChunk(content, serviceUrl, success);
	}

	export function exportAllPages(serviceUrl: string, content: string, token: string) {
		var url = serviceUrl + '/exportallpages/' +  token;
		_exportByForm(content, url);
	}

	function _exportChunk(content, serviceUrl, success) {
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.open('POST', serviceUrl, true);
		xmlhttp.onload = function () {
			var txt = JSON.parse(xmlhttp.responseText);
			success(txt);
		};
		xmlhttp.send(content);
		return xmlhttp;
	}

	function _exportByForm(content, serviceUrl) {
		var formInnerHtml = '<input type="hidden" name="type" value="application/json"/>';
		formInnerHtml += '<input type="hidden" name="data" value="' + _htmlSpecialCharsEntityEncode(content) + '" />';
		var $iframe = $("<iframe style='display: none' src='about:blank'></iframe>").appendTo("body");
		$iframe.ready(function () {
			var formDoc = _getiframeDocument($iframe);
			formDoc.write("<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body><form method='Post' accept-charset='utf-8' action='" + serviceUrl + "'>" + formInnerHtml + "</form>dummy windows for postback</body></html>");
			var $form = $(formDoc).find('form');
			$form.submit();
		});
	}

	function _getiframeDocument($iframe) {
		var iframeDoc = $iframe[0].contentWindow || $iframe[0].contentDocument;
		if (iframeDoc.document) {
			iframeDoc = iframeDoc.document;
		}
		return iframeDoc;
	}

	var _htmlSpecialCharsRegEx = /[<>&\r\n"']/gm;
	var _htmlSpecialCharsPlaceHolders = {
		'<': 'lt;',
		'>': 'gt;',
		'&': 'amp;',
		'\r': "#13;",
		'\n': "#10;",
		'"': 'quot;',
		"'": 'apos;' /*single quotes just to be safe*/
	};

	function _htmlSpecialCharsEntityEncode(str) {
		return str.replace(_htmlSpecialCharsRegEx, function (match) {
			return '&' + _htmlSpecialCharsPlaceHolders[match];
		});
	}

	function _exportByXMLHttpRequest(content, serviceUrl, setting) {
		var xmlhttp = new XMLHttpRequest();
		xmlhttp.open("post", serviceUrl);
		xmlhttp.send(content);
		xmlhttp.responseType = "blob";
		xmlhttp.onreadystatechange = function () {
			if (xmlhttp.response && setting.receiver) {
				setting.receiver(xmlhttp.response, setting);
			}
		}
	}

	/** Export file type options: Xls, Xlsx, Cvs, Pdf, Png, Jpg,Bmp,Gif and Tiff. */
	export enum ExportFileType {
		Xls,
		Xlsx,
		Csv,
		Pdf,
		Png,
		Jpg,
		Bmp,
		Gif,
		Tiff
	}

	/** Export mode */
	export enum ExportMethod {
		/** Sending markup to the service for exporting. */
		Content,
		/** Sending widget options to the service for exporting. */
		Options
	}

	/** Export setting. */
	export interface ExportSetting {
		/** Export file type. */
		exportFileType: ExportFileType;
		/** The name of exported file. */
		fileName?: string;
		/** Export pdf setting. */
		pdf?: PdfSetting;
		/** The url of export service. */
		serviceUrl: string;
		/** Customize the response data handling. */
		receiver?: (data: any, setting: ExportSetting) => void;
		/** Customize sending the request data to service. */
		sender?: (content: string, setting: ExportSetting) => void;
		/**  If set to “application/json”, the request data with json format will be sent to service. 
		 *   And receiver should be set. Otherwise, data will be set as “x-www-form-urlencoded” type.
		 */
		contentType?: string;
	}

	export interface PdfSetting {
		/** The page size, the unit is point. It takes effect when paperKind is custom. Used only for grid widget. */
		pageSize?: PageSize;
		/** The page margins, the unit is point. Used only for grid widget. */
		margins?: Margins;
		/** Auto adjust the content size to make it fit pdf page’s width. Used only for grid widget. */
		autoFitWidth?: boolean;
		/** The paperKind of the pdf page. It is corresponding to PaperKind Enumeration. Used only for grid widget. */
		paperKind?: PaperKind;
		/** A boolean value indicates page orientation for the pdf document. Used only for grid widget. */
		landscape?: boolean;
		/**  The name of the person that created the pdf document. */
		author?: string;
		/**  The name of the application that created the original document. */
		creator?: string;
		/** The keywords associated with the pdf document. */
		keywords: string;
		/** The name of the application that created the pdf document. */
		producer?: string;
		/** The subject of the pdf document.*/
		subject?: string;
		/** The title of the pdf document. */
		title?: string;
		/** A boolean value indicates whether the user can copy contents from the pdf document. */
		allowCopyContent?: boolean;
		/** Customize the response data handling. */
		allowEditAnnotations?: boolean;
		/**  A boolean value indicates whether the user can edit the contents of the pdf document. */
		allowEditContent?: boolean;
		/** A boolean value indicates whether the user can print the pdf document. */
		allowPrint?: boolean;
		/** Specifies the type of encryption used for pdf document. It is corresponding to PdfEncryptionType Enumeration. */
		encryption?: PdfEncryptionType;
		/** Specifies the password required to change permissions for the pdf document. */
		ownerPassword?: string;
		/**  Specifies the password required to open the pdf document. */
		userPassword?: string;
		/** The compression level to use when saving the pdf document. It is corresponding to CompressionEnum Enumeration. */
		compression?: CompressionType;
		/** The image quality to use when saving the pdf document. It is corresponding to ImageQualityEnum Enumeration.*/
		imageQuality?: ImageQuality;
		/** It indicates how fonts should be encoded when saving the document. It is corresponding to FontTypeEnum Enumeration. */
		fontType?: FontType;
	}

	/**  The font type */
	export enum FontType {
		/**  Use only standard Pdf fonts (Helvetica, Times, Symbol). */
		Standard= 0,
		/**  Use TrueType fonts, no embedding (viewer must have fonts installed). */
		TrueType= 1,
		/**  Use embedded TrueType fonts. */
		Embedded= 2
	}

	/**  The image quality. */
	export enum ImageQuality {
		/**  Low quality, small file size. */
		Low = 0,
		/**  Medium quality, medium file size. */
		Medium = 1,
		/**  High quality, medium/large file size. */
		Default = 2,
		/**  Highest quality, largest file size. */
		High = 3
	}

	/**  The compression type. */
	export enum CompressionType {
		/**  High compression, fast save. */
		Default = -1,
		/**  No compression (useful for debugging). */
		None = 0,
		/**  Low compression, fastest save. */
		BestSpeed = 1,
		/**  Highest compression, slowest save. */
		BestCompression = 9
	}

	/** The Pdf encryption type. */
	export enum PdfEncryptionType {
		/**  Encryption is unavailable due to FIPS compliance (MD5 and AES128 are not FIPS-compliant). */
		NotPermit = 0,
		/**  Standard 40 bit encryption algorithm. */
		Standard40 = 2,
		/**  Standard 128 bit encryption algorithm. */
		Standard128 = 3,
		/**  AES 128 bit encryption algorithm. */
		Aes128 = 4
	}

	/**  The size of page. */
	export interface PageSize {
		/**  The width of page size. */
		Width: number;
		/**  The height of page size. */
		Height: number;
	}

	/**  The margin settings. */
	export interface Margins {
		/**  The top of margin.*/
		top: number;
		/**  The left of margin.*/
		left: number;
		/**  The bottom of margin.*/
		bottom: number;
		/**  The right of margin.*/
		right: number;
	}

	/**  Specifies the standard paper sizes. */
	export enum PaperKind {
		/**  The paper size is defined by the user.*/
		Custom = 0,
		/** Letter paper (8.5 in. by 11 in.).*/
		Letter = 1,
		/** Letter small paper (8.5 in. by 11 in.).*/
		LetterSmall = 2,
		/** Tabloid paper (11 in. by 17 in.).*/
		Tabloid = 3,
		/** Ledger paper (17 in. by 11 in.).*/
		Ledger = 4,
		/** Legal paper (8.5 in. by 14 in.).*/
		Legal = 5,
		/** Statement paper (5.5 in. by 8.5 in.).*/
		Statement = 6,
		/** Executive paper (7.25 in. by 10.5 in.).*/
		Executive = 7,
		/** A3 paper (297 mm by 420 mm).*/
		A3 = 8,
		/** A4 paper (210 mm by 297 mm).*/
		A4 = 9,
		/** A4 small paper (210 mm by 297 mm).*/
		A4Small = 10,
		/** A5 paper (148 mm by 210 mm).*/
		A5 = 11,
		/** B4 paper (250 mm by 353 mm).*/
		B4 = 12,
		/** B5 paper (176 mm by 250 mm).*/
		B5 = 13,
		/** Folio paper (8.5 in. by 13 in.).*/
		Folio = 14,
		/**  Quarto paper (215 mm by 275 mm).*/
		Quarto = 15,
		/** Standard paper (10 in. by 14 in.).*/
		Standard10x14 = 16,
		/** Standard paper (11 in. by 17 in.).*/
		Standard11x17 = 17,
		/** Note paper (8.5 in. by 11 in.).*/
		Note = 18,
		/**  #9 envelope (3.875 in. by 8.875 in.).*/
		Number9Envelope = 19,
		/** #10 envelope (4.125 in. by 9.5 in.).*/
		Number10Envelope = 20,
		/** #11 envelope (4.5 in. by 10.375 in.).*/
		Number11Envelope = 21,
		/** #12 envelope (4.75 in. by 11 in.).*/
		Number12Envelope = 22,
		/** #14 envelope (5 in. by 11.5 in.).*/
		Number14Envelope = 23,
		/** C paper (17 in. by 22 in.).*/
		CSheet = 24,
		/** D paper (22 in. by 34 in.).*/
		DSheet = 25,
		/** E paper (34 in. by 44 in.).*/
		ESheet = 26,
		/** DL envelope (110 mm by 220 mm).*/
		DLEnvelope = 27,
		/**  C5 envelope (162 mm by 229 mm).*/
		C5Envelope = 28,
		/** C3 envelope (324 mm by 458 mm). */
		C3Envelope = 29,
		/** C4 envelope (229 mm by 324 mm). */
		C4Envelope = 30,
		/** C6 envelope (114 mm by 162 mm). */
		C6Envelope = 31,
		/** C65 envelope (114 mm by 229 mm). */
		C65Envelope = 32,
		/** B4 envelope (250 mm by 353 mm). */
		B4Envelope = 33,
		/** B5 envelope (176 mm by 250 mm). */
		B5Envelope = 34,
		/**  B6 envelope (176 mm by 125 mm). */
		B6Envelope = 35,
		/** Italy envelope (110 mm by 230 mm). */
		ItalyEnvelope = 36,
		/** Monarch envelope (3.875 in. by 7.5 in.). */
		MonarchEnvelope = 37,
		/** 6 3/4 envelope (3.625 in. by 6.5 in.). */
		PersonalEnvelope = 38,
		/** US standard fanfold (14.875 in. by 11 in.). */
		USStandardFanfold = 39,
		/** German standard fanfold (8.5 in. by 12 in.). */
		GermanStandardFanfold = 40,
		/** German legal fanfold (8.5 in. by 13 in.). */
		GermanLegalFanfold = 41,
		/** ISO B4 (250 mm by 353 mm). */
		IsoB4 = 42,
		/** Japanese postcard (100 mm by 148 mm). */
		JapanesePostcard = 43,
		/** Standard paper (9 in. by 11 in.). */
		Standard9x11 = 44,
		/** Standard paper (10 in. by 11 in.). */
		Standard10x11 = 45,
		/** Standard paper (15 in. by 11 in.). */
		Standard15x11 = 46,
		/** Invitation envelope (220 mm by 220 mm). */
		InviteEnvelope = 47,
		/** Letter extra paper (9.275 in. by 12 in.). This value is specific to the PostScript
		  *  driver and is used only by Linotronic printers in order to conserve paper. */
		LetterExtra = 50,
		/** Legal extra paper (9.275 in. by 15 in.). This value is specific to the PostScript
		  *  driver and is used only by Linotronic printers in order to conserve paper. */
		LegalExtra = 51,
		/** Tabloid extra paper (11.69 in. by 18 in.). This value is specific to the
		 *   PostScript driver and is used only by Linotronic printers in order to conserve
		 *   paper. */
		TabloidExtra = 52,
		/** A4 extra paper (236 mm by 322 mm). This value is specific to the PostScript
		 *    driver and is used only by Linotronic printers to help save paper. */
		A4Extra = 53,
		/** Letter transverse paper (8.275 in. by 11 in.). */
		LetterTransverse = 54,
		/** A4 transverse paper (210 mm by 297 mm). */
		A4Transverse = 55,
		/** Letter extra transverse paper (9.275 in. by 12 in.). */
		LetterExtraTransverse = 56,
		/** SuperA/SuperA/A4 paper (227 mm by 356 mm). */
		APlus = 57,
		/** SuperB/SuperB/A3 paper (305 mm by 487 mm). */
		BPlus = 58,
		/** Letter plus paper (8.5 in. by 12.69 in.). */
		LetterPlus = 59,
		/** A4 plus paper (210 mm by 330 mm). */
		A4Plus = 60,
		/** A5 transverse paper (148 mm by 210 mm). */
		A5Transverse = 61,
		/** JIS B5 transverse paper (182 mm by 257 mm). */
		B5Transverse = 62,
		/** A3 extra paper (322 mm by 445 mm). */
		A3Extra = 63,
		/** A5 extra paper (174 mm by 235 mm). */
		A5Extra = 64,
		/** ISO B5 extra paper (201 mm by 276 mm). */
		B5Extra = 65,
		/** A2 paper (420 mm by 594 mm). */
		A2 = 66,
		/** A3 transverse paper (297 mm by 420 mm). */
		A3Transverse = 67,
		/** A3 extra transverse paper (322 mm by 445 mm). */
		A3ExtraTransverse = 68,
		/** Japanese double postcard (200 mm by 148 mm). Requires Windows 98, Windows
		 *    NT 4.0, or later. */
		JapaneseDoublePostcard = 69,
		/** A6 paper (105 mm by 148 mm). Requires Windows 98, Windows NT 4.0, or later. */
		A6 = 70,
		/** Japanese Kaku #2 envelope. Requires Windows 98, Windows NT 4.0, or later. */
		JapaneseEnvelopeKakuNumber2 = 71,
		/** Japanese Kaku #3 envelope. Requires Windows 98, Windows NT 4.0, or later. */
		JapaneseEnvelopeKakuNumber3 = 72,
		/** Japanese Chou #3 envelope. Requires Windows 98, Windows NT 4.0, or later. */
		JapaneseEnvelopeChouNumber3 = 73,
		/** Japanese Chou #4 envelope. Requires Windows 98, Windows NT 4.0, or later. */
		JapaneseEnvelopeChouNumber4 = 74,
		/** Letter rotated paper (11 in. by 8.5 in.). */
		LetterRotated = 75,
		/** A3 rotated paper (420 mm by 297 mm). */
		A3Rotated = 76,
		/**  A4 rotated paper (297 mm by 210 mm). Requires Windows 98, Windows NT 4.0,
		 *   or later. */
		A4Rotated = 77,
		/** A5 rotated paper (210 mm by 148 mm). Requires Windows 98, Windows NT 4.0,
		 *   or later. */
		A5Rotated = 78,
		/** JIS B4 rotated paper (364 mm by 257 mm). Requires Windows 98, Windows NT
		 *   4.0, or later. */
		B4JisRotated = 79,
		/** JIS B5 rotated paper (257 mm by 182 mm). Requires Windows 98, Windows NT
		 *   4.0, or later. */
		B5JisRotated = 80,
		/** Japanese rotated postcard (148 mm by 100 mm). Requires Windows 98, Windows
		 *   NT 4.0, or later.*/
		JapanesePostcardRotated = 81,
		/** Japanese rotated double postcard (148 mm by 200 mm). Requires Windows 98,
		 *  Windows NT 4.0, or later.*/
		JapaneseDoublePostcardRotated = 82,
		/** A6 rotated paper (148 mm by 105 mm). Requires Windows 98, Windows NT 4.0,
		 *   or later.*/
		A6Rotated = 83,
		/** Japanese rotated Kaku #2 envelope. Requires Windows 98, Windows NT 4.0, or
		 *   later.*/
		JapaneseEnvelopeKakuNumber2Rotated = 84,
		/** Japanese rotated Kaku #3 envelope. Requires Windows 98, Windows NT 4.0, or
		 *   later.*/
		JapaneseEnvelopeKakuNumber3Rotated = 85,
		/** Japanese rotated Chou #3 envelope. Requires Windows 98, Windows NT 4.0, or
		 *   later.*/
		JapaneseEnvelopeChouNumber3Rotated = 86,
		/** Japanese rotated Chou #4 envelope. Requires Windows 98, Windows NT 4.0, or
		 *   later.*/
		JapaneseEnvelopeChouNumber4Rotated = 87,
		/** JIS B6 paper (128 mm by 182 mm). Requires Windows 98, Windows NT 4.0, or
		 *   later.*/
		B6Jis = 88,
		/** JIS B6 rotated paper (182 mm by 128 mm). Requires Windows 98, Windows NT
		 *   4.0, or later.*/
		B6JisRotated = 89,
		/** Standard paper (12 in. by 11 in.). Requires Windows 98, Windows NT 4.0, or
		 *   later.*/
		Standard12x11 = 90,
		/** Japanese You #4 envelope. Requires Windows 98, Windows NT 4.0, or later.*/
		JapaneseEnvelopeYouNumber4 = 91,
		/** Japanese You #4 rotated envelope. Requires Windows 98, Windows NT 4.0, or
		 *   later.*/
		JapaneseEnvelopeYouNumber4Rotated = 92,
		/** People's Republic of China 16K paper (146 mm by 215 mm). Requires Windows
			98, Windows NT 4.0, or later.*/
		Prc16K = 93,
		/** People's Republic of China 32K paper (97 mm by 151 mm). Requires Windows
			98, Windows NT 4.0, or later.*/
		Prc32K = 94,
		/** People's Republic of China 32K big paper (97 mm by 151 mm). Requires Windows
		 *   98, Windows NT 4.0, or later.*/
		Prc32KBig = 95,
		/** People's Republic of China #1 envelope (102 mm by 165 mm). Requires Windows
		 *   98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber1 = 96,
		/** People's Republic of China #2 envelope (102 mm by 176 mm). Requires Windows
			98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber2 = 97,
		/** People's Republic of China #3 envelope (125 mm by 176 mm). Requires Windows
		 *   98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber3 = 98,
		/** People's Republic of China #4 envelope (110 mm by 208 mm). Requires Windows
		 *   98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber4 = 99,
		/** People's Republic of China #5 envelope (110 mm by 220 mm). Requires Windows
			98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber5 = 100,
		/** People's Republic of China #6 envelope (120 mm by 230 mm). Requires Windows
		 *   98, Windows NT 4.0, or later.
		PrcEnvelopeNumber6 = 101,
		/** People's Republic of China #7 envelope (160 mm by 230 mm). Requires Windows
		 *   98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber7 = 102,
		/** People's Republic of China #8 envelope (120 mm by 309 mm). Requires Windows
		 *   98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber8 = 103,
		/** People's Republic of China #9 envelope (229 mm by 324 mm). Requires Windows
		 *   98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber9 = 104,
		/** People's Republic of China #10 envelope (324 mm by 458 mm). Requires Windows
		 *   98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber10 = 105,
		/** People's Republic of China 16K rotated paper (146 mm by 215 mm). Requires
		 *   Windows 98, Windows NT 4.0, or later.*/
		Prc16KRotated = 106,
		/** People's Republic of China 32K rotated paper (97 mm by 151 mm). Requires
		 *   Windows 98, Windows NT 4.0, or later.*/
		Prc32KRotated = 107,
		/** People's Republic of China 32K big rotated paper (97 mm by 151 mm). Requires
		 *   Windows 98, Windows NT 4.0, or later.*/
		Prc32KBigRotated = 108,
		/**  People's Republic of China #1 rotated envelope (165 mm by 102 mm). Requires
		 *    Windows 98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber1Rotated = 109,
		/** People's Republic of China #2 rotated envelope (176 mm by 102 mm). Requires
		 *  Windows 98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber2Rotated = 110,
		/** People's Republic of China #3 rotated envelope (176 mm by 125 mm). Requires
		 *   Windows 98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber3Rotated = 111,
		/** People's Republic of China #4 rotated envelope (208 mm by 110 mm). Requires
		 *   Windows 98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber4Rotated = 112,
		/** People's Republic of China Envelope #5 rotated envelope (220 mm by 110 mm).
		 *   Requires Windows 98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber5Rotated = 113,
		/** People's Republic of China #6 rotated envelope (230 mm by 120 mm). Requires
		 *   Windows 98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber6Rotated = 114,
		/** People's Republic of China #7 rotated envelope (230 mm by 160 mm). Requires
			Windows 98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber7Rotated = 115,
		/** People's Republic of China #8 rotated envelope (309 mm by 120 mm). Requires
		 *   Windows 98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber8Rotated = 116,
		/** People's Republic of China #9 rotated envelope (324 mm by 229 mm). Requires
		 *   Windows 98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber9Rotated = 117,
		/** People's Republic of China #10 rotated envelope (458 mm by 324 mm). Requires
		 *   Windows 98, Windows NT 4.0, or later.*/
		PrcEnvelopeNumber10Rotated = 118,
	}
}