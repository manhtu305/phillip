﻿/// <reference path="../wijchart/jquery.wijmo.wijchartcore.ts"/>
/// <reference path="exportUtil.ts" />

module wijmo.exporter {

	export enum ChartExportMethod {
		Content,
		Options
	}

    export interface ChartExportSetting extends ExportSetting {
        /** The export width setting. */
        width?: number;
        /** The export height setting. */
        height?: number;
        /** The name of wijchart */
        widgetName?: string;
        /** The options of wijchart,used for chart export options mode. */
        widgetOptions?: any;
        /** Chart widget markups, used for chart export content mode. */
        HTMLContent?: string;
        /** The wijchart object. */
        chart: wijmo.chart.wijchartcore;
        /** It has "Content" and "Options" mode. */
        method?: ChartExportMethod;
    }

    /** @ignore 
    *  Export the chart as image or pdf.
    * @param {ChartExportSetting} The export settings.
    */
    export function exportChart(setting: ChartExportSetting): void {
        var chartExporter = new chartExport(setting.serviceUrl, setting.method),
            options: ChartOptions = {
                fileName: setting.fileName,
                height: setting.height,
                width: setting.width,
                widgetName: setting.widgetName,
                widgetOptions: setting.widgetOptions,
                HTMLContent: setting.HTMLContent,
                widget: setting.chart,
                type: setting.exportFileType,
                pdfSetting: setting.pdf,
            },
            data = chartExporter.getExportData(options);
        exportFile(setting, data);
    }

    /** @ignore 
    *  Export the chart as image or pdf.
    * @param {ChartExportSetting} The export settings.
    */
	export class chartExport {
		constructor(public serviceUrl: string, public method: ChartExportMethod) {

		}
		options: ChartOptions;
		getExportData(options: ChartOptions):any {
			var result: any;
			this.options = options;
			this._handleExportOption();
			result = {
				fileName: options.fileName,
				width: options.width,
				height: options.height,
                type: options.type,
                pdfSettings: options.pdfSetting
			};
			if (this.method === ChartExportMethod.Content) {
				result.content = options.HTMLContent;
			} else {
				result.options = options.widgetOptions;
				result.widgetName = options.widgetName;
			}
			return toJSON(result, (key, val) => {
                if (key === "type") {
                    //return ExportFileType[val];
					switch (val) {
						case ExportFileType.Bmp:
							return "bmp";
						case ExportFileType.Gif:
							return "gif";
						case ExportFileType.Jpg:
							return "jpg";
						case ExportFileType.Pdf:
							return "pdf";
						case ExportFileType.Tiff:
							return "tiff";
						default:
							return "png";
					}
				}
				return val;
			});
		}

		_handleExportOption() {
			var o = this.options;
			if (o.widget) {
				if (this.method == ChartExportMethod.Content) {
					o.HTMLContent = o.widget.chartElement[0].outerHTML;
				}
				else {
					o.widgetName = this._getWidgetName();
					o.widgetOptions = toJSON(this._handleChartOptions(o.widget, o.widgetName), function (k, v) {
						if (v != null) {
							return v;
						}
					});
				}
				o.width = o.widget.chartElement.width();
				o.height = o.widget.chartElement.height();
			}
		}

		_getWidgetName(): string {
			return this.options.widget ? this.options.widget.widgetName : "";
		}

		_handleChartOptions(widget: wijmo.chart.wijchartcore, widgetName: string) {
			var chartoptions = widget.options,
				options: any = $.extend(true, {}, chartoptions, {
				animation: {
					enabled: false
				},
				seriesTransition: {
					enabled: false
				},
				hint: {
					enable: false
				}
			});

			delete options.wijCSS;
			delete options.seriesHoverStyles;

			if (widgetName === "wijbarchart") {
				this._handleBarChartOptions(options);
			} else if (widgetName === "wijcandlestickchart") {
				this._handleCandlestickChartOptions(options, widget);
			} else if (widgetName === "wijcompositechart") {
				this._handleCompositeChartOptions(options, widget);
			}

			this._handleAxis(options);
			return options;
		}

		_handleAxis(options) {
			this._removeAuto(options.axis.x);
			if ($.isArray(options.axis.y)) {
				$.each(options.axis.y, (i, axis) => {
					this._removeAuto(axis);
				})
			} else {
				this._removeAuto(options.axis.y);
			}
		}

		_handleBarChartOptions(options) {
			if (options.horizontal) {
				var compass = options.axis.x.compass;
				options.axis.x.compass = options.axis.y.compass;
				options.axis.y.compass = compass;
			}
		}

		_handleCandlestickChartOptions(options, widget) {
			$.each(options.seriesList, function (i, series) {
				if (series.data && series.data.x) {
					$.each(series.data.x, function (j, val) {
						if (typeof (val) === "number") {
							series.data.x[j] = widget.timeUtil.getTime(val);
						}
					});
				}
			});
		}

		_handleCompositeChartOptions(options, widget) {
			if (widget.isContainsCandlestick && widget.timeUtil) {
				$.each(options.seriesList, function (i, series) {
					if (series.data && series.data.x) {
						$.each(series.data.x, function (j, val) {
							if (typeof (val) === "number") {
								series.data.x[j] = widget.timeUtil.getTime(val);
							}
						});
					}
				});
			}
		}

		_removeAuto(axis: wijmo.chart.chart_axis) {
			if (axis.autoMax) {
				delete axis.max;
			}
			if (axis.autoMin) {
				delete axis.min;
			}
			if (axis.autoMajor) {
				delete axis.unitMajor;
			}
			if (axis.autoMinor) {
				delete axis.unitMinor;
			}
		}
	}

     /** @ignore */
    export interface ChartOptions {
		fileName?: string;
		width?: number;
		height?: number;
		widgetName?: string;
		widgetOptions?: any;
		HTMLContent?: string;
		type: ExportFileType;
		widget: wijmo.chart.wijchartcore;
		pdfSetting: PdfSetting;
    }

     /** @ignore */
    export function innerExportChart(exportSettings: any, type?: string, settings?: any, serviceUrl?: string, exportMethod?: string) {
        var t: string,
            mode: string,
            pdfSetting: any = {},
            sUrl: string,
            chartExportSettings: ChartExportSetting;
        t = type === undefined ? "png" : type;
        t = t.substr(0, 1).toUpperCase() + t.substr(1);
        mode = exportMethod === undefined ? "Options" : exportMethod;
        mode = mode.substr(0, 1).toUpperCase() + mode.substr(1);
        if (typeof settings === "string" && arguments.length === 3) {
            sUrl = settings;
        } else {
            sUrl = serviceUrl === undefined ? "http://demos.componentone.com/ASPNET/ExportService/exportapi/chart" : serviceUrl;
            pdfSetting = settings;
        }

        if (!$.isPlainObject(exportSettings)) {
            chartExportSettings = {
                serviceUrl: sUrl,
                fileName: exportSettings === undefined ? "export" : exportSettings,
                pdf: pdfSetting,
                method: ChartExportMethod[mode],
                chart: this,
                exportFileType: wijmo.exporter.ExportFileType[t]
            };
        } else {
            chartExportSettings = <ChartExportSetting>exportSettings;
        }
        wijmo.exporter.exportChart(chartExportSettings);
    }

    if (wijmo.chart && wijmo.chart.wijchartcore) {
		wijmo.chart.wijchartcore.prototype.exportChart = innerExportChart;
	}

	//wijmo.chart
	var charts = ["wijbarchart", "wijbubblechart", "wijcandlestickchart", "wijcompositechart", "wijlinechart", "wijpiechart", "wijscatterchart"];
	$.each(charts, (idx, chart) => {
		if (!$.wijmo[chart]) {
			return;
		}
		$.wijmo[chart].prototype.exportChart = innerExportChart;
	});
}