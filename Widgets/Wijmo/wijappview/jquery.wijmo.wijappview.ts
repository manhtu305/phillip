/// <reference path="../Base/jquery.wijmo.widget.ts" />
/// <reference path="../wijutil/jquery.wijmo.wijutil.ts" />

/*globals jQuery*/
/*
 * Depends:
 *  jquery.mobile.js 
 *
 */

module wijmo.appview {

	var $ = jQuery,
		widgetName = "wijappview",
		roles = {
			menu: "menu",
			content: "content",
			header: "header",
			footer: "footer",
			page: "appviewpage",
		},

		dataAttributes = {
			adjusted: widgetName + "-adjusted"
		},

		initialHash = "",
		forceUpdateUrl = false;

	$.fn.findRole = function (role) {
		return this.find(":jqmData(role='" + role + "')");
	};

	/** @widget */
	export class wijappview extends wijmoWidget {
		_linkList: JQuery;
		_menuDiv: JQuery;
		_jqmPageEnclosingWidget: Object;
		_pageStore: JQuery[];
		_pageContainer: JQuery;
		_firstPage: JQuery;
		_documentUrl: string;

		//#region Initialization

		_appViewCSS() {
			return this.options.wijCSS.wijappview;
		}

		_initMenu() {
			this._menuDiv = this.element.findRole(roles.menu);
			if (!this._menuDiv.length) {
				throw "A DIV with data-role='" + roles.menu + "' not found!";
			}
			this._linkList = this._menuDiv.find("ul");
			if (!this._linkList.length) {
				throw "Invalid markup. Link list not found";
			}
			this._menuDiv.addClass(this._appViewCSS().menu);
		}


		_initPages($pages: JQuery, fileUrl: string) {
			var pageStore = this._pageStore,
				css = this._appViewCSS();
			$pages.each(function () {
				var page = $(this).addClass(css.page),
					id = page.attr("id"),
					dataUrl = fileUrl;
				if (page.jqmData("url")) return;
				if (id) {
					dataUrl += "#" + id;
				}

				page.attr("data-url", dataUrl);
			});
		}
		_initPageContainer() {
			var css = this._appViewCSS(),
				pageStore = this._pageStore;

			this._pageContainer = $("<div/>").addClass(css.pageContainer)
				.prependTo(this.element);

			var pages = this.element.findRole(roles.page);
			this._initPages(pages, this._documentUrl);
			pages.each(function (index) {
				pageStore.push($(this).clone());
				if (index > 0) {
					$(this).remove();
				}
			});
			this._firstPage = pages.first()
				.addClass(css.pageActive)
				.appendTo(this._pageContainer);
			this._initCurrentPage();
		}

		_getUrl(elem: JQuery) {
			var urlAttrName = elem.is("form") ? "action" : "href",
				url = elem.attr(urlAttrName);

			if (!url || url === "#") return null;

			if ($.mobile.path.isRelativeUrl(url)) {
				elem.parents(":jqmData(url)").each((_, parent) => {
					var parentUrl = $(parent).jqmData("url");
					if (parentUrl && $.mobile.path.isAbsoluteUrl(parentUrl)) {
						url = $.mobile.path.makeUrlAbsolute(url, parentUrl);
						return false;
					}
				});
			}

			return url;
		}

		_clickLinks(e: JQueryEventObject) {
			if (e.isDefaultPrevented() || this._isDisabled()) return;
			var link = this._findClosestLink(e.target);
			if (!link || $(link).jqmData("appviewpage") === false) return;

			var url = this._getUrl($(link));
			if (url) {
				e.preventDefault();
				e.stopPropagation();
				this.changePage(url);
			}
		}

		_hijackLinks(root: JQuery) {
			root.on("click." + widgetName, "a:not(.WijListviewNestedLink), .ui-btn:not(.WijListviewNestedLink)", (e: JQueryEventObject) => {
				this._clickLinks(e);
			});
		}

		_submitForms(e: JQueryEventObject) {
			if (e.isDefaultPrevented() || this._isDisabled()) return;

			var $form = $(e.currentTarget),
				$page = $form.closest("." + <string> this._appViewCSS().page);

			if (!$.mobile.ajaxEnabled ||
				// test that the form is, itself, ajax false
				$form.is(":jqmData(ajax='false')") ||
				// test that $.mobile.ignoreContentEnabled is set and
				// the form or one of it's parents is ajax=false
				!$form.jqmHijackable().length) {
				return;
			}

			var type = $form.attr("method"),
				target = $form.attr("target"),
				url = this._getUrl($form);

			if (!url) {
				return;
			}

			e.preventDefault();
			e.stopPropagation();
			this.changePage(url, {
				type: type && type.length && type.toLowerCase() || "get",
				data: $form.serialize(),
				reloadPage: true
			});
		}

		_hijackForms(root: JQuery) {
			root.on("submit." + widgetName, "form", e => {
				this._submitForms(e);
			});
		}

		_initNavigation() {
			this._onPopStateScoped = this._onPopStateScoped || $.proxy(this._onPopState, this);
			$(window).bind("popstate." + widgetName, this._onPopStateScoped);
			$(window).bind("appviewpagehashchange." + widgetName, () => this._navigateToCurrentLocation());

			this._onNavigateScoped = this._onNavigateScoped || $.proxy(this._onNavigate, this);
			$($.mobile.pageContainer).bind("navigate." + widgetName, this._onNavigateScoped);
		}

		_create() {
			this._pageStore = [];
			this._documentUrl = this._removePageUrlParam(location.href);
			this.element.attr("data-url", this._documentUrl);
			this._jqmPageEnclosingWidget = this._findJqmPage();

			this.element.addClass(this._appViewCSS().outerDiv);

			this._initMenu()
			this._initPageContainer()
			this._hijackLinks(this.element);
			this._hijackForms(this.element);
			this._initNavigation();
			this._navigateToCurrentLocation();

			this._toggleDisable();
		}

		_toggleDisable() {
			super._toggleDisable();

			this._setInnerElementsDisabledForIE(this._isDisabled());
		}

		_setInnerElementsDisabledForIE(disabled) {
			if (!$.browser.msie) {
				return;
			}
			var css = this._appViewCSS(),
				ele = this.element,
				eleOffset = ele.offset(),
				header = this._pageContainer.find("." + css.header),
				content = this._pageContainer.find("." + css.content),
				menu = ele.find("." + css.menu),
				disabeldCSS = this.options.wijCSS.stateDisabled;

			header.toggleClass(disabeldCSS, disabled);
			content.toggleClass(disabeldCSS, disabled);
			menu.toggleClass(disabeldCSS, disabled);
		}

		destroy() {
			super.destroy();
			var dotNs = "." + widgetName;
			this.element.unbind(dotNs);
			$(window).unbind(dotNs);
			$($.mobile.pageContainer).unbind(dotNs);
		}

		//#endregion

		_findJqmPage(): Object {
			for (var elem = this.element; elem.length; elem = elem.parent()) {
				var page = elem.jqmData("mobile-page");
				if (page) {
					return page;
				}
			}

			return null;
		}

		_initCurrentPage() {
			var css = this._appViewCSS();
			var page = this.activePage();
			page.findRole(roles.content).addClass(css.content);
			var header = page.findRole(roles.header).addClass(css.header);

			var position = header.jqmData("position");
			var headerFixed = !position || position === "fixed";
			if (headerFixed) {
				header.addClass("ui-header-fixed");
			}

			this.element.toggleClass(css.withFixedHeader, headerFixed);
		}

		_findClosestLink(ele) {
			while (ele && !(typeof ele.nodeName === "string" && ele.nodeName.toLowerCase() === "a")) {
				ele = ele.parentNode;
			}
			return ele;
		}
		_clickHandler(e: JQueryEventObject) {
			var target = $(e.currentTarget),
				url: string = target.is("li") ? target.jqmData("url") : target.is("a") ? target.attr("href") : null;
			if (!url) return;

			e.preventDefault();
			e.stopPropagation();
			this.changePage(url);
		}

		_inheritAttribute(src, dest, attrName) {
			if (src.attr(attrName) && !dest.attr(attrName)) {
				dest.attr(attrName, src.attr(attrName));
			}
		}

		_findInCache(url: string) {
			return $($.grep(this._pageStore, (p) => p.jqmData("url") === url)[0]).clone();
		}

		/** Load an appview page 
		  * @param {String} url URL of the page to load
		  * @param options Load settings to override options.loadSettings
		  */
		loadPage(url: string, options: ILoadSettings) {
			url = url || "";
			// This function uses deferred notifications to let callers
			// know when the page is done loading, or if an error has occurred.
			var deferred = $.Deferred(),
				path = $.mobile.path,

				settings: any = $.extend({}, this.options.loadSettings, options),
				isLocal = !url || url.charAt(0) === "#",

				// The DOM element for the page after it has been loaded.
				page = null,

				// If the reloadPage option is true, and the page is already
				// in the DOM, dupCachedPage will be set to the page element
				// so that it can be removed after the new version of the
				// page is loaded off the network.
				dupCachedPage = null,
				absUrl = this._makeAbsoluteUrl(url);

			// ==== Disable caching for now ====
			// Check to see if the page already exists in the DOM.
			// NOTE do _not_ use the :jqmData psuedo selector because parenthesis
			//      are a valid url char and it breaks on the first occurence
			// page = this._findInCache(absUrl);

			// If the page we are interested in is already in the DOM,
			// and the caller did not indicate that we should force a
			// reload of the file, we are done. Otherwise, track the
			// existing page as a duplicated.
			//if (page.length) {
			//    if (isLocal || !settings.reloadPage) {
			//	    // enhancePage(page, settings.role);
			//	    deferred.resolve(absUrl, options, page);
			//	    return deferred.promise();
			//    }
			//    dupCachedPage = page;
			//}

			if (settings.data) {
				switch (settings.type) {
					case "get":
						absUrl = path.addSearchParams(absUrl, settings.data);
						settings.data = undefined;
						break;

					case "post":
						settings.reloadPage = true;
						break;
				}
			}

			var pblEvent = $.Event("pagebeforeload"),
				triggerData: any = { url: url, absUrl: absUrl, dataUrl: absUrl, deferred: deferred, options: settings };

			// Let listeners know we're about to load a page.
			this._trigger(pblEvent.type, pblEvent, triggerData);

			// If the default behavior is prevented, stop here!
			if (pblEvent.isDefaultPrevented()) {
				return deferred.promise();
			}

			if (!$.mobile.allowCrossDomainPages && !path.isSameDomain(path.parseLocation(), absUrl)) {
				deferred.reject(absUrl, options);
				return deferred.promise();
			}

			// The absolute version of the URL minus any dialog/subpage params.
			// In otherwords the real URL of the page to be loaded.
			var fileUrl = absUrl && path.getFilePath(absUrl).replace(/#.+/, "");

			// Load the new page.
			var xhr = $.ajax({
				url: fileUrl,
				type: settings.type,
				data: settings.data,
				dataType: "html",
				success: (html, textStatus, xhr) => {
					//pre-parse html to check for a data-url,
					//use it as the new fileUrl, base path, etc
					var all = $("<div></div>"),

						// TODO handle dialogs again
						pageElemRegex = new RegExp("(<[^>]+\\bdata-" + $.mobile.ns + "role=[\"']?" + roles.page + "[\"']?[^>]*>)"),
						dataUrlRegex = new RegExp("\\bdata-" + $.mobile.ns + "url=[\"']?([^\"'>]*)[\"']?");

					function tryMakeAbsolute(url: string) {
						if (path.isAbsoluteUrl(url) || /^(\w+:|#|\/)/.test(url)) {
							return null;
						}

						return path.makeUrlAbsolute(url, fileUrl);
					}

					if ($.browser.msie && parseFloat($.browser.version) <= 7) {
						// for some reason IE7 automatically makes all links absolute before I do it. 
						// I have to use regex in this case
						html = html.replace(/<a[^>]+href=['"][^'"]+['"]/g, (link) => {
							return link.replace(/href=['"]([^'"]+)['"]/, function (m, url) {
								url = tryMakeAbsolute(url) || url;
								return "href='" + url + "'";
							});
						});
					}

					//workaround to allow scripts to execute when included in page divs
					all.get(0).innerHTML = html;
					var allPages = this._getAllPages(all, url);

					//if page elem couldn't be found, create one and insert the body element's contents
					if (!allPages.length) {
						allPages = $("<div/>").attr("data-role", roles.page)
							.html(html.split(/<\/?body[^>]*>/gmi)[1]);
					}

					var pageTitleFromHead = html.match(/<title[^>]*>([^<]*)/) && RegExp["$1"];
					if (pageTitleFromHead && ~pageTitleFromHead.indexOf("&")) {
						pageTitleFromHead = $("<div>" + pageTitleFromHead + "</div>").text();
					}


					// fix data-title
					allPages.each((i, page: any) => {
						page = $(page);

						page.attr("data-external-page", true);

						// fix data-title
						if (!page.jqmData("title") && pageTitleFromHead) {
							page.attr("data-title", pageTitleFromHead);
						}

						//rewrite src and href attrs to use a base url
						var newPath = path.get(fileUrl);
						page.find("[src], [href]").each(function () {
							var thisAttr = $(this).is('[href]')
								? 'href'
								: $(this).is('[src]') ? 'src' : 'action',
								url = tryMakeAbsolute($(this).attr(thisAttr));
							if (url) {
								$(this).attr(thisAttr, url);
							}
						});
					});

					this._initPages(allPages, fileUrl);

					// Let listeners know the page loaded successfully.
					// Add the page reference and xhr to our triggerData.
					page = allPages.filter(function () {
						return $(this).jqmData("url") === absUrl;
					});
					if (!page.length) {
						page = allPages.first();
					}

					triggerData.xhr = xhr;
					triggerData.textStatus = textStatus;
					triggerData.page = page;
					triggerData.allPages = allPages;
					this._trigger("pageload", null, triggerData);

					allPages.each((i, p) => {
						this._pageStore.push($(p).clone());
					});

					deferred.resolve(absUrl, options, page, dupCachedPage);
				},
				error: (xhr, textStatus, errorThrown) => {
					// Add error info to our triggerData.
					triggerData.xhr = xhr;
					triggerData.textStatus = textStatus;
					triggerData.errorThrown = errorThrown;

					var plfEvent = $.Event("pageloadfailed");

					// Let listeners know the page load failed.
					this._trigger(plfEvent.type, plfEvent, triggerData);

					// If the default behavior is prevented, stop here!
					// Note that it is the responsibility of the listener/handler
					// that called preventDefault(), to resolve/reject the
					// deferred object within the triggerData.
					if (plfEvent.isDefaultPrevented()) {
						return;
					}

					// Remove loading message.
					//if (settings.showLoadMsg) {

					// Remove loading message.
					// hideMsg();

					// show error message
					// $.mobile.showPageLoadingMsg($.mobile.pageLoadErrorMessageTheme, $.mobile.pageLoadErrorMessage, true);

					// hide after delay
					// setTimeout($.mobile.hidePageLoadingMsg, 1500);
					//}

					deferred.reject(absUrl, options);
				}
			});

			deferred.fail(function () {
				xhr.abort();
			});
			return deferred;
		}

		_getAllPages(html: JQuery, url: String) {
			return html.findRole(roles.page);
		}

		/** Current active appview page DOM element */
		activePage() {
			return this._pageContainer.children("." + this._appViewCSS().pageActive).first();
		}

		/** @ignore */
		isMenuUrl(url: string) {
			var that = this;
			url = url.toLowerCase();
			return this._menuDiv.find("a")
				.is(function () {
					var getUrl = that._getUrl($(this));
					if (!getUrl) {
						return false;
					}
					return getUrl.toLowerCase() === url;
				});
		}

		_showLoading() {
			$.mobile.loading("show");
		}
		_hideLoading() {
			$.mobile.loading("hide");
		}

		_curRequest: JQueryDeferred<any>;
		/** Change current appview page
		  * @param toPage A URL or an appview page DOM element to switch to
		  * @param options Load settings to be used if 'toPage' is a URL
		  * @remarks
		  * If 'toPage' is a URL, then changePage() loads the page first and calls itself recursively
		  */
		changePage(toPage, options?: ILoadSettings) {
			options = $.extend({ updateUrl: true }, this.options.loadSettings, options);
			var triggerData = { toPage: toPage, options: options };

			if (this._curRequest && this._curRequest.state() === "pending" && this._curRequest.rejectWith) {
				this._curRequest.rejectWith(this, [toPage, options, true]);
			}

			var currentDataUrl: string;
			var toPageAbs: string;

			if (typeof toPage === "string") {
				currentDataUrl = this.activePage().data("url");
				toPageAbs = this._makeAbsoluteUrl(toPage);
				var hashPos = toPageAbs.indexOf("#");
				if (hashPos >= 0 && toPageAbs.substr(0, hashPos) === currentDataUrl.split("#")[0]) {
					var fromCache = this._findInCache(toPageAbs);
					if (fromCache.length) {
						toPage = fromCache;
					}
				}
			}

			if (typeof toPage === "string") {
				this._updateUIBeforeChange(toPageAbs);
				if (options.showLoadMsg) {
					this._showLoading();
				}
				this._curRequest = this.loadPage(toPage, options)
					.always(() => {
						if (options.showLoadMsg) {
							this._hideLoading();
						}
						this._curRequest = null;
					})
					.done((url, options, page, dupCachedPage) => {
						this._updateUIAfterChange(page.jqmData("url"));
						this.changePage(page, options);
					})
					.fail((url, options, anotherPage) => {
						if (!anotherPage) {
							this._updateUI(currentDataUrl);
						}
						this._trigger("pagechangefailed", null, triggerData);
					});
				return;
			}

			var pbcEvent = $.Event("pagebeforechange");
			this._trigger(pbcEvent.type, pbcEvent, triggerData);
			if (pbcEvent.isDefaultPrevented()) {
				return;
			}

			var absUrl = toPage.jqmData("url"),
				relUrl = null;

			this._updateUI(absUrl);

			if (absUrl) {
				relUrl = this._makeRelative(absUrl);
				if (relUrl != null) {
					relUrl = this._makeRelative(relUrl);
				}
			}

			var pageTitle = toPage.jqmData("title"),
				header = toPage.findRole("header"),
				css = this._appViewCSS();

			if (!header.length && pageTitle) {
				header = $("<div data-role='header'/>")
					.append($("<h2/>").text(pageTitle))
					.prependTo(toPage);
			}

			if (header.length && !header.jqmData(dataAttributes.adjusted)) {
				header.jqmData(dataAttributes.adjusted, true);
				var oldHeader = this._firstPage.findRole(roles.header);
				if (oldHeader) {
					this._inheritAttribute(oldHeader, header, "data-position");
				}

				if (this.isMenuUrl(absUrl) && !header.find("a[data-icon=back]").length) {
					$("<a/>").attr({
						href: this._documentUrl,
						"data-icon": "back",
					}).text("Back").prependTo(header);
				}
			}


			if (pageTitle) {
				document.title = pageTitle;
			}

			this._pageContainer.children().removeClass(css.pageActive);
			toPage.addClass(css.pageActive)
				.appendTo(this._pageContainer.empty());

			this._initCurrentPage();

			if ((<any> options).updateUrl || forceUpdateUrl) {
				this._updateUrl(absUrl, relUrl, document.title);
				forceUpdateUrl = false;
			}

			toPage.jqmData("mobile-page", this._jqmPageEnclosingWidget)
				.jqmData("page", this._jqmPageEnclosingWidget)
				.enhanceWithin()
				.trigger("pagecreate")
				.trigger(this.widgetEventPrefix + "pageinit", triggerData);

			// TODO: temporary workaround

			this._trigger("pagechange", null, triggerData);
		}


		_updateUIBeforeChange(pageUrl: string) {
			var that = this,
				isRoot = pageUrl === this._documentUrl;

			var menuItems = this._linkList.find("li"),
				activeClass = this._appViewCSS().menuItemActive;
			if (isRoot) {
				menuItems.removeClass(activeClass);
			} else {
				var newMenuItem = menuItems.filter(function () {
					var url = that._getUrl($(this).find("a"));
					return url && url.toLowerCase() === pageUrl.toLowerCase();
				});
				if (newMenuItem.length && !newMenuItem.hasClass(activeClass)) {
					menuItems.removeClass(activeClass);
					newMenuItem.addClass(activeClass);
				}
			}
		}
		_updateUIAfterChange(pageUrl: string) {
			var css = this._appViewCSS(),
				isRoot = pageUrl === this._documentUrl;
			this.element.toggleClass(css.inPage, !isRoot);
		}
		_updateUI(pageUrl) {
			this._updateUIBeforeChange(pageUrl);
			this._updateUIAfterChange(pageUrl);
		}

		_changePageIfDifferent(toPage, options?) {
			var targetUrl = typeof toPage === "string" ? toPage : toPage.jqmData("url");
			if (this.activePage().jqmData("url") !== targetUrl) {
				this.changePage(toPage, options);
			}
		}

		// ==== URL management ====
		_onNavigateScoped;
		_onNavigate(e) {
			var hashRgx = new RegExp("^#?" + this.options.urlParamName + "=");
			if (history.state && history.state.wijappview || document.location.hash.match(hashRgx)) {
				e.preventDefault();
			}
		}

		_removePageUrlParam(url: string) {
			return url.replace(/\#.*$/, "");
		}
		_addUrl(pageUrl: string) {
			var param = this.options.urlParamName;
			var url = this._removePageUrlParam(location.href);
			if (pageUrl) {
				url += "#" + param + "=" + pageUrl;
			}
			return url;
		}

		_makeRelative(absUrl: string) {
			var loc = $.mobile.path.parseLocation();
			var url = absUrl;
			if (absUrl) {
				var baseDir = loc.domain + loc.directory;
				if (absUrl.substr(0, loc.hrefNoHash.length) === loc.hrefNoHash) {
					url = absUrl.substr(loc.hrefNoHash.length);
				} else if (absUrl.substr(0, baseDir.length) === baseDir) {
					url = absUrl.substr(baseDir.length);
				}
			}
			return url;
		}

		_makeAbsoluteUrl(url: string) {
			var path = $.mobile.path,
				activePage = this.activePage(),
				curAbsUrl = path.makeUrlAbsolute(activePage.jqmData("url") || this._documentUrl, this._documentUrl);
			if (!curAbsUrl.match(/\/$/) && !curAbsUrl.match(/\./)) {
				curAbsUrl += "/";
			}
			return url && path.makeUrlAbsolute(url, curAbsUrl);
		}

		_onPopStateScoped;
		_onPopState(e) {
			if (this._updatingUrl) return;

			if (history.state && history.state.wijappview) {
				this._changePageIfDifferent(history.state.absUrl || this._firstPage, { updateUrl: false });
			} else {
				this._navigateToCurrentLocation();
			}
		}

		_navigateToCurrentLocation() {
			var rgx = new RegExp("#" + this.options.urlParamName + "=(.+)"),
				match = rgx.exec(location.href) || initialHash && rgx.exec(initialHash),
				toPage = match ? <any>$.mobile.path.makeUrlAbsolute(match[1], this._documentUrl) : <any>this._firstPage;
			initialHash = "";
			this._changePageIfDifferent(toPage, { updateUrl: false });
		}

		_updatingUrl = false;
		_updateUrl(absUrl: string, relUrl: string, title: string) {
			this._updatingUrl = true;
			try {
				$.mobile.navigate.navigator.preventHashAssignPopState = true;
				document.location.hash = absUrl === this._documentUrl
				? ""
				: this.options.urlParamName + "=" + relUrl;
			} finally {
				this._updatingUrl = false;
			}
		}
	}

	var mainClass = "wijmo-wijappview",
		classPrefix = mainClass + "-";

	/** Settings used to load appview pages */
	export interface ILoadSettings {
		/** HTML method (GET, POST, etc) to be used when requesting an appview page */
		type?: string;
		/** A hash of request parameters. Corresponds to the 'data' parameter of jQuery.ajax */
		data?;
		/** Specifies whether to show a rotating wheel during loading */
		showLoadMsg?: boolean;
	}

	/** Base class for appview event args */
	export interface IAppViewEventArgs {
		/** Settings used for loading pages */
		options: ILoadSettings;
	}

	/** Appview Page load-related event info */
	export interface IPageLoadEventArgs extends IAppViewEventArgs {
		/** The url of the loading page */
		url: string;
		/** The absolute url of the loading page */
		absUrl: string;

		/** An XHR object used for loading */
		xhr?: JQueryXHR;
		/** Text status of the HTTP resonse */
		textStatus?: string;
		/** The loaded page. If multiple pages are loaded, the main one */
		page?: JQuery;
		/** All loaded pages */
		allPages?: JQuery;
		/** An error that occurred during loading */
		errorThrown?: string;
	}

	/** Appview page change-related event info */
	export interface IPageChangeEventArgs extends IAppViewEventArgs {
		/** The target page
		  * @type {JQuery|string}
		  */
		toPage: any;
	}

	export class wijappview_options {
		/** Name of a parameter in a URL that specifies current appview page address */
		urlParamName: string = "appviewpage";

		/** Settings used to load appview pages */
		loadSettings: ILoadSettings = {
			type: "GET",
			data: undefined,
			reloadPage: false,
			showLoadMsg: false
		};

		/** Fires before a page is loaded
		  * @event
		  * @param {jQuery.Event} e Standard jQuery event object
		  * @param {IPageLoadEventArgs} args Information about an event
		  */
		pagebeforeload: (e: JQueryEventObject, args: IPageLoadEventArgs) => void = null;
		/** Fires after a page is loaded
		  * @event
		  * @param {jQuery.Event} e Standard jQuery event object
		  * @param {IPageLoadEventArgs} args Information about an event
		  */
		pageload: (e: JQueryEventObject, args: IPageLoadEventArgs) => void = null;
		/** Fires when a page load is failed
		  * @event
		  * @param {jQuery.Event} e Standard jQuery event object
		  * @param {IPageLoadEventArgs} args Information about an event
		  */
		pageloadfailed: (e: JQueryEventObject, args: IPageLoadEventArgs) => void = null;

		/** Fires before the current page is changed
		  * @event
		  * @param {jQuery.Event} e Standard jQuery event object
		  * @param {IPageChangeEventArgs} args Information about an event
		  */
		pagebeforechange: (e: JQueryEventObject, args: IPageChangeEventArgs) => void = null;
		/** Fires after a page is changed
		  * @event
		  * @param {jQuery.Event} e Standard jQuery event object
		  * @param {IPageChangeEventArgs} args Information about an event
		  */
		pagechange: (e: JQueryEventObject, args: IPageChangeEventArgs) => void = null;
		/** Fires when a page change is failed
		  * @event
		  * @param {jQuery.Event} e Standard jQuery event object
		  * @param {IPageChangeEventArgs} args Information about an event
		  */
		pagechangefailed: (e: JQueryEventObject, args: IPageChangeEventArgs) => void = null;

		wijCSS = {
			wijappview: {
				outerDiv: mainClass,
				inPage: classPrefix + "in-page",
				menu: classPrefix + "menu",
				menuItemActive: "ui-btn-down-b",
				page: classPrefix + "page",
				pageActive: classPrefix + "page-active",
				pageContainer: classPrefix + "page-container",
				content: classPrefix + "content",
				header: classPrefix + "header",
				withFixedHeader: classPrefix + "with-fixed-header"
			}
		};
	}

	wijappview.prototype.options = $.extend({}, wijmoWidget.prototype.options, new wijappview_options());

	function isAppViewPageUrl() {
		return document.location.hash.match(/^#\w+=/);
	}

	if ($.mobile) {
		$.wijmo.registerWidget(widgetName, wijappview.prototype);

		$(window).bind("hashchange", function (e) {
			if (isAppViewPageUrl()) {
				e.stopImmediatePropagation();
				$(window).trigger("appviewpagehashchange");
			}
		});

		// prevent initial jQM page transition
		if (isAppViewPageUrl()) {
			initialHash = document.location.hash;
			$(window).bind("pagecontainercreate", () => {
				document.location.hash = "";
				forceUpdateUrl = true;
			});
		}
	}
}

interface JQuery {
	wijappview: JQueryWidgetFunction;
	findRole(role: string): JQuery;
	page(): JQuery;
	jqmHijackable: Function;
}
interface JQueryStatic {
	filter: Function;
}
