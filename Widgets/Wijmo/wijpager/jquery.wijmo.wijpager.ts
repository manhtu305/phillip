/// <reference path="../Base/jquery.wijmo.widget.ts"/>
/*globals jQuery*/
/*jslint white: false */
/*
 * Depends:
 *  jquery-1.11.1.js
 *  jquery.ui.core.js
 *  jquery.ui.widget.js
 *  jquery.wijmo.widget.js
 */

module wijmo.pager {
	  

	var $ = jQuery;

	/** @widget */
	export class wijpager extends wijmoWidget {
		$ul: JQuery;

		_create() {
			var o = this.options;
			if (window.wijmoApplyWijTouchUtilEvents) {
				$ = window.wijmoApplyWijTouchUtilEvents($);
			}

			this.element.addClass(this.options.wijCSS.widget + " wijmo-wijpager " + this.options.wijCSS.helperClearFix);

			this._refresh();
			super._create();
		}

		_destroy() {
			this.element.removeClass(this.options.wijCSS.widget + " wijmo-wijpager " + this.options.wijCSS.helperClearFix);
			this.$ul.remove();
		}

		_setOption(key, value) {
			this._super(key, value);
			this._refresh();
		}

		_refresh() {
			this._validate();

			if (this.$ul) {
				this.$ul.remove();
			}

			this.element.append(this.$ul = $("<ul class=\"ui-list " + this.options.wijCSS.cornerAll + " " + this.options.wijCSS.content + " " + this.options.wijCSS.helperClearFix + "\" role=\"tablist\"></ul>"));

			switch ((this.options.mode || "").toLowerCase()) {
				case "nextprevious":
					this._createNextPrev(false);
					break;

				case "nextpreviousfirstlast":
					this._createNextPrev(true);
					break;

				case "numeric":
					this._createNumeric(false);
					break;

				case "numericfirstlast":
					this._createNumeric(true);
					break;
			}
		}

		_validate() {
			var o = this.options;

			if (isNaN(o.pageCount) || o.pageCount < 1) {
				o.pageCount = 1;
			}

			if (isNaN(o.pageIndex) || o.pageIndex < 0) {
				o.pageIndex = 0;
			} else {
				if (o.pageIndex >= o.pageCount) {
					o.pageIndex = o.pageCount - 1;
				}
			}

			if (isNaN(o.pageButtonCount) || o.pageButtonCount < 1) {
				o.pageButtonCount = 1;
			}
		}

		_createNextPrev(addFirstLast: boolean) {
			var o = this.options,
				firstPageClass = !!o.firstPageClass ?  o.firstPageClass + " " + o.wijCSS.iconSeekFirst : "",
				previousPageClass = !!o.previousPageClass ? o.previousPageClass + " " + o.wijCSS.iconSeekPrev : "",
				nextPageClass = !!o.nextPageClass ? o.nextPageClass + " " + o.wijCSS.iconSeekNext : "",
				lastPageClass = !!o.lastPageClass ? o.lastPageClass + " " + o.wijCSS.iconSeekEnd : "";

			// first button
			if (addFirstLast && o.pageIndex) {
				this.$ul.append(this._createPagerItem(false, o.firstPageText, 1, firstPageClass));
			}

			// previous button
			if (o.pageIndex) {
				this.$ul.append(this._createPagerItem(false, o.previousPageText, o.pageIndex, previousPageClass));
			}

			// next button
			if (o.pageIndex + 1 < o.pageCount) {
				this.$ul.append(this._createPagerItem(false, o.nextPageText, o.pageIndex + 2, nextPageClass));
			}

			// last button
			if (addFirstLast && (o.pageIndex + 1 < o.pageCount)) {
				this.$ul.append(this._createPagerItem(false, o.lastPageText, o.pageCount, lastPageClass));
			}
		}

		_createNumeric(addFirstLast: boolean) {
			var o = this.options,
				currentPage = o.pageIndex + 1,
				startPageNumber = 1,
				endPageNumber = Math.min(o.pageCount, o.pageButtonCount),
				i,
				firstPageClass = !!o.firstPageClass ? o.firstPageClass + " " + o.wijCSS.iconSeekFirst : "",
				lastPageClass = !!o.lastPageClass ? o.lastPageClass + " " + o.wijCSS.iconSeekEnd : "";

			if (currentPage > endPageNumber) {
				startPageNumber = (Math.floor(o.pageIndex / o.pageButtonCount)) * o.pageButtonCount + 1;

				endPageNumber = startPageNumber + o.pageButtonCount - 1;
				endPageNumber = Math.min(endPageNumber, o.pageCount);

				if (endPageNumber - startPageNumber + 1 < o.pageButtonCount) {
					startPageNumber = Math.max(1, endPageNumber - o.pageButtonCount + 1);
				}
			}

			// first + "..." buttons
			if (startPageNumber !== 1) {
				// first button
				if (addFirstLast) {
					this.$ul.append(this._createPagerItem(false, o.firstPageText, 1, firstPageClass));
				}

				// "..." button
				this.$ul.append(this._createPagerItem(false, "...", startPageNumber - 1, ""));
			}

			// page numbers buttons
			for (i = startPageNumber; i <= endPageNumber; i++) {
				this.$ul.append(this._createPagerItem(i === currentPage, i.toString(), i, ""));
			}

			// "..." + last buttons
			if (o.pageCount > endPageNumber) {
				this.$ul.append(this._createPagerItem(false, "...", endPageNumber + 1, ""));

				// last button
				if (addFirstLast) {
					this.$ul.append(this._createPagerItem(false, o.lastPageText, o.pageCount, lastPageClass));
				}
			}
		}

		_createPagerItem(active: boolean, title: string, pageIndex: number, btnClass: string) {
			var btnContent,
				wijCSS = this.options.wijCSS,
				self = this,
				$li = $("<li />")
					.addClass(wijCSS.pagerButton + " " + wijCSS.cornerAll)
					.attr({ "role": "tab", "title": title });

			try {
				$li.attribute("aria-label", title); // throws "member not found" exception in IE10\11 if IE7 compatibility mode is used.
			} catch (ex) { }

			if (active) {
				$li
					.addClass(wijCSS.stateActive)
					.attribute("aria-selected", "true");
			} else {
				$li.addClass(wijCSS.stateDefault)
					.hover(
						function () {
							if (!self._isDisabled()) {
								$(this).addClass(wijCSS.stateHover);
							}
						},
						function () {
							if (!self._isDisabled()) {
								$(this).removeClass(wijCSS.stateHover);
							}
						})
					.bind("click." + this.widgetName, { newPageIndex: pageIndex - 1 }, $.proxy(this._onClick, this)); // pageIndex is 1-based.
			}

			if (active) {
				btnContent = $("<span />");
			} else {
				btnContent = btnClass
					? $("<span />").addClass(wijCSS.icon + " " + btnClass)
					: $("<a/>").attr("href", "#");
			}

			btnContent
				.text(title);

			$li.append(btnContent);

			return $li;
		}

		_onClick(arg: JQueryEventObject) {
			if (this._isDisabled()) {
				return false;
			}

			var pagingArgs: wijmo.pager.IPageIndexChangingEventArgs = { newPageIndex: arg.data.newPageIndex, handled: false };

			if (this._trigger("pageIndexChanging", null, pagingArgs) !== false) {
				if (this.options.pageIndex !== pagingArgs.newPageIndex) {
					this.options.pageIndex = pagingArgs.newPageIndex;
					if (!pagingArgs.handled) {
						this._refresh();
					}

					var pagedArgs: wijmo.pager.IPageIndexChangedEventArgs = { newPageIndex: pagingArgs.newPageIndex };
					this._trigger("pageIndexChanged", null, pagedArgs);
				}
			}

			return false;
		}
	}

	wijpager.prototype.widgetEventPrefix = "wijpager";

	class wijpager_options implements wijmo.pager.IPagerUISettings  {
		/** @ignore */
		wijCSS = {
			pagerButton: "wijmo-wijpager-button"
		};
		/** @ignore */
		wijMobileCSS = {
			header: "ui-header ui-bar-a",
			content: "ui-body-b",
			stateDefault: "ui-btn ui-btn-b",
			stateHover: "ui-btn-down-b",
			stateActive: "ui-btn-down-c"
		};

		/**
		* An option that indicates the class of the first-page button. You can set this option with any of the jQuery UI CSS Framework icons.
		* @default "ui-icon-seek-first"
		* @example
		* // Here's the general way you'll set the option:
		* $("#element").wijpager({
		*    firstPageClass: "ui-icon-seek-first"
		* });
		*/
		firstPageClass: string = $.wijmo.widget.prototype.options.wijCSS.iconSeekFirst;

		/**
		* An option that indicates the text to display for the first-page button. The text will display like a tooltip when a user hovers over the first-page button.
		* @example
		* // Here's the general way you'll set the option:
		* $("#element").wijpager({
		*    firstPageText: "Go To First"
		* });
		*/
		firstPageText: string = "First";

		/**
		* An option that indicates the class of the last-page button. You can set this option with any of the jQuery UI CSS Framework icons.
		* @default "ui-icon-seek-end"
		* @example
		* // Here's the general way you'll set the option:
		* $("#element").wijpager({
		*    lastPageClass: "ui-icon-seek-end"
		* });
		*/
		lastPageClass: string = $.wijmo.widget.prototype.options.wijCSS.iconSeekEnd;

		/**
		* An option that indicates the text to display for the last-page button. The text will display like a tooltip when a user hovers over the last-page button.
		* @example
		* // Here's the general way you'll set the option:
		* $("#element").wijpager({
		*   lastPageText: "Go To Last"
		* });
		*/
		lastPageText: string = "Last";

		/**
		* This option determines the pager mode. The possible values for this option are: "nextPrevious", "nextPreviousFirstLast", "numeric", "numericFirstLast".
		* @remarks
		* Possible values are: "nextPrevious", "nextPreviousFirstLast", "numeric", "numericFirstLast".
		* "nextPrevious": a set of pagination controls consisting of Previous and Next buttons.
		* "nextPreviousFirstLast": a set of pagination controls consisting of Previous, Next, First, and Last buttons.
		* "numeric": a set of pagination controls consisting of numbered link buttons to access pages directly.
		* "numericFirstLast": a set of pagination controls consisting of numbered and First and Last link buttons.
		* @example
		* // Here's the general way you'll set the option: 
		* $("#element").wijpager({
		*    mode: "nextPrevious"
		* });
		*/
		mode: string = "numeric";

		/**
		* An option that indicates the class of the next-page button. You can set this option with any of the jQuery UI CSS Framework Icons. 
		* @default "ui-icon-seek-next"
		* @example
		* // Here's the general way you'll set the option:
		* $("#element").wijpager({
		*    nextPageClass: "ui-icon-seek-next"
		* });
		*/
		nextPageClass: string = $.wijmo.widget.prototype.options.wijCSS.iconSeekNext;

		/**
		* An option that indicates the text to display for the next-page button. The text appears like a tooltip when a user hovers over the next-page button.
		* @example
		* // Here's the general way you'll set the option:
		* $("#element").wijpager({
		*    nextPageText: "Go To Next"
		* });
		*/
		nextPageText: string = "Next";

		/**
		* An option that indicates the number of page buttons to display in the pager. You can customize how many page buttons are available for your users to use to page through content.
		* You can use this option in conjunction with the pageCount to customize the pager display.
		* @example
		* // Here's the general way you'll set the option:
		* $("#element").wijpager({
		*    pageButtonCount: 15
		* });
		*/
		pageButtonCount: number = 10;

		/**
		* An option that indicates the class of the previous-page button. You can set this option with any of the jQuery UI CSS Framework Icons. 
		* @type {string}
		* @default "ui-icon-seek-prev"
		* @example
		* // Here's the general way you'll set the option:
		* $("#element").wijpager({
		*    previousPageClass: "ui-icon-seek-prev"
		* });
		*/
		previousPageClass: string = $.wijmo.widget.prototype.options.wijCSS.iconSeekPrev;

		/**
		* An option that indicates the text to display for the previous-page button. The text appears like a tooltip when a user hovers over the previous-page button.
		* @example
		* // Here's the general way you'll set the option:
		* $("#element").wijpager({
		*    previousPageText: "Go To Previous"
		* });
		*/
		previousPageText: string = "Previous";

		/**
		* An option that indicates the total number of pages. This option allows you to customize the total number of pages that your users will be able to page through. You can use this option in conjunction with the pageButtonCount to customize the pager display.
		* @example
		* // Here's the general way you'll set the option:
		* $("#element").wijpager({
		*    pageCount: 10
		* });
		*/
		pageCount: number = 1;

		/**
		* An option that indicates the zero-based index of the current page. By default, your pager will display with the first pager button highlighted since its index is 0.
		* @example
		* // Here's the general way you'll set the option. This will allow your pager widget to display with the 3rd page button highlighted:
		* $("#element").wijpager({
		*    pageIndex: 2
		* });
		*/
		pageIndex: number = 0;

		/**
		* The pageIndexChanging event handler is a function called when page index is changing. This item is cancellable if you return false.
		* @event
		* @param {object} e The jQuery.Event object.
		* @param {wijmo.grid.IPageIndexChangingEventArgs} args The data associated with this event.
		* @example
		* // Supply a callback function to handle the event:
		* $("#element").wijpager({
		*    pageIndexChanging: function (e, args) {
		*       // Handle the event here.
		*    }
		* });

		* // Bind to the event by type:
		* $("#element").bind("wijpagerpageindexchanging", function (e, args) {
		*    // Handle the event here.
		* });
		*
		* // You can cancel this event be returning false:
		* $("#element").wijpager({
		*    pageIndexChanging: function(e, args) {
		*       return false;
		*    }
		* });
		*/
		pageIndexChanging: (e: JQueryEventObject, args: wijmo.pager.IPageIndexChangingEventArgs) => boolean = null;

		/**
		* The pageIndexChanged event handler is a function called when the page index is changed.
		* @event
		* @param {object} e The jQuery.Event object.
		* @param {wijmo.grid.IPageIndexChangedEventArgs} args The data associated with this event.
		* @example
		* // Supply a callback function to handle the event:
		* $("#element").wijpager({
		*    pageIndexChanged: function (e, args) {
		*       // Handle the event here.
		*    }
		* });

		* // Bind to the event by type:
		* $("#element").bind("wijpagerpageindexchanged", function (e, args) {
		*    // Handle the event here.
		* });
		*
		* // You can also use this event to get the selected page by using the args.newPageIndex parameter:
		* $("#element").wijpager({
		*    pageIndexChanged: function(e, args) {
		*       var selectedPageIndex = args.newPageIndex;
		*    }
		* });
		*/
		pageIndexChanged: (e: JQueryEventObject, args: wijmo.pager.IPageIndexChangedEventArgs) => void = null;
	};

	wijpager.prototype.options = <any>$.extend(true, {}, wijmoWidget.prototype.options, new wijpager_options());

	$.wijmo.registerWidget("wijpager", wijpager.prototype);

	/** Provides data for the pageIndexChanging event of the wijpager. */
	export interface IPageIndexChangingEventArgs {
		/** The new pageIndex. */
		newPageIndex: number;
		/**
		* @ignore
		* Infrastructure.
		*/
		handled: boolean;
	}

	/** Provides data for the pageIndexChanged event of the wijpager. */
	export interface IPageIndexChangedEventArgs {
		/** The new pageIndex. */
		newPageIndex: number;
	}

	/**	Provides data for the pageIndexChanged event of the wijpager. */
	export interface IPageIndexChanged1EventArgs {
		/** The new pageIndex */
		newPageIndex: number;
	}

	/** Represents the wijpager's UI settings. */
	export interface IPagerUISettings {
		/** An option that indicates the class of the first-page button. */
		firstPageClass: string;

		/** An option that indicates the text to display for the first-page button. */
		firstPageText: string;

		/** An option that indicates the class of the last-page button. */
		lastPageClass: string;

		/** An option that indicates the text to display for the last-page button. */
		lastPageText: string;

		/** An option that determines the pager mode. Possible values are: "nextPrevious", "nextPreviousFirstLast", "numeric", "numericFirstLast". 
		* @remarks
		* Possible values are:
		* "nextPrevious": a set of pagination controls consisting of Previous and Next buttons.
		* "nextPreviousFirstLast": a set of pagination controls consisting of Previous, Next, First, and Last buttons.
		* "numeric": a set of pagination controls consisting of numbered link buttons to access pages directly.
		* "numericFirstLast": a set of pagination controls consisting of numbered and First and Last link buttons.
		*/
		mode: string;

		/** An option that controls the class of the next-page button. */
		nextPageClass: string;

		/** An option that indicates the text to display for the next-page button. */
		nextPageText: string;

		/** An option that indicates the number of page buttons to display in the pager. */
		pageButtonCount: number;

		/** An option that indicates the class of the previous-page button. */
		previousPageClass: string;

		/** An option that indicates the text to display for the previous-page button. */
		previousPageText: string;
	};
}

/** @ignore */
interface JQuery {
	wijpager: JQueryWidgetFunction;
}