// Type definitions for Breeze 1.0
// Project: http://www.breezejs.com/
// Definitions by: Boris Yankov <https://github.com/borisyankov/>
// Definitions: https://github.com/borisyankov/DefinitelyTyped

// Updated Jan 14 2011 - Jay Traband (www.ideablade.com)


declare module "elementtree" {
    export interface IContainer {
        findall(path: string): IElement[];
        find(path: string): IElement;
        findtext(path: string);
    }

    export interface IElement extends IContainer {
        text: string;
        tag: string;
        getchildren(): IElement[];

        get (key: string, defValue?);
        set(key: string, value): void;
    }

    export interface IElementTree extends IContainer {
        getroot(): IElement;
        parse(source: string, parser?): IElement;
        write(options?: { indent?: number; lexicalOrder?: boolean; }): string;
    }

    export var ElementTree: {
        new (root: IElement): IElementTree;
    };

    export function parse(text: string): IElementTree;
    export function SubElement(parnet: IElement, name: string): IElement;
    export function Element(name: string): IElement;
}