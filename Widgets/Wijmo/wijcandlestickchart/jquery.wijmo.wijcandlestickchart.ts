﻿/// <reference path="../wijchart/jquery.wijmo.wijchartcore.ts"/> 
module wijmo.chart {
	var widgetName = "wijcandlestickchart",
		strOHLC = "ohlc",
		strCandlestick = "candlestick",
		strHL = "hl",
		strStrokeWidth = "stroke-width";

	/** @ignore */
	export class datetimeUtil {

		constructor(datetimeArr) {
			this.init(datetimeArr);
		}

		times: any[];
		timeDic: any;
		unit: number;
		max: number;
		min: number;
		count: number;

		init(datetimeArr) {
			var times:any = [],
				dic = {},
				length, i;

			$.each(datetimeArr, (i, n) => {
				var val = $.toOADate(n);
				if ($.inArray(val, times) === -1) {
					times.push(val);
				}
			})

			this.count = length = times.length;
			this.times = times = times.sort();
			this.timeDic = dic;
			// when the times contains less than 2 items, set default value.
			if (length < 2) {
				this.max = times[0] || 0;
				this.min = times[0] || 0;
				this.unit = 24 * 60 * 60 * 1000;
				if (length === 1) {
					dic[times[0]] = 0;
				}
				return;
			}

			for (i = 0; i < length; i++) {
				dic[times[i]] = i;
			}
			this._calculateUnit();
			this.max = times[this.count - 1];
			this.min = times[0];
		}

		_calculateUnit() {
			var unit = Number.MAX_VALUE,
				i = 1;
			for (; i < this.count; i++) {
				unit = Math.min(unit, this.times[i] - this.times[i - 1]);
			}
			this.unit = unit;
		}

		getOATime(index) {
			var count = this.count,
				times = this.times,
				num;
			if (index < 0) {
				return this.min + index * this.unit;
			} else if (index > count - 1) {
				return this.max + (index - count + 1) * this.unit;
			} else {
				num = parseInt(index);
				return times[num] + (index - num) * this.unit;
			}
		}

		getTime(index) {
			return $.fromOADate(this.getOATime(index));
		}

		getTimeIndex(datetime) {
			var oaDate = $.toOADate(datetime),
				index;
			// if datetime is larger than the max date of the series's x data. use max and unit to calculate the index.
			if (oaDate > this.max) {
				return this.count - 1 + (oaDate - this.max) / this.unit;
			// if the datetime is smaller than the min date of the series's  use min and unit to calculate the index
			} else if (oaDate < this.min) {
				return (oaDate - this.min) / this.unit;
			} else {
				// if the datetime is in series's x date, just get the index in the series's x data.
                if (this.timeDic && this.timeDic[oaDate] != null) {
					return this.timeDic[oaDate];
				} else {
					// if the datetime not in series's x data, use the closest date and unit to calculate the index.
					$.each(this.times, (i, time) => {
						if (time > oaDate) {
							index = i - 1;
							return false;
						}
					});
					return index + (oaDate - this.times[index]) / this.unit;
				}
			}
		}

		getCount() {
			return this.times.length;
		}

		dispose() {
			this.times = null;
			this.timeDic = null;
		}
	}

	/** @widget */
	export class wijcandlestickchart extends wijchartcore {

		timeUtil: datetimeUtil;
		formatString: string;
        bindXData: boolean;
        valueLabelsFromXData: boolean;
        xAxisLabelInfoList: any[];

		_paintChartArea() {
            var o = (<wijcandlestickchart_options>this.options);

			if (this._isSeriesListDataEmpty()) {
				return;
			}
			// Add high and low data to data.y for calculate the max and min of y axis.
			$.each(o.seriesList, (i, series) => {
				var data = series.data;
				if (!series.isTrendline) {
			        data.y = [].concat(data.high).concat(data.low);
			    }
            });
            super._paintChartArea();

            // Delete the added data.y; 
            // And recover the data.x 
            $.each(o.seriesList, (i, series) => {
                if (!series.isTrendline) {
                    delete series.data.y;
                }	
                if (series.data.originalXValue) {
                    series.data.x = series.data.originalXValue;
                    delete series.data.originalXValue;
                }			
            });
        }

        _getXAxisInfoValueLabels(xAxisInfo, xAxisOptions) {
            var self = this, annoFormatString;

            if (!xAxisOptions.annoFormatString || xAxisOptions.annoFormatString.length === 0) {
                annoFormatString = chart.ChartDataUtil.getTimeDefaultFormat(this.timeUtil.max, this.timeUtil.min);
            } else {
                annoFormatString = xAxisOptions.annoFormatString;
            }
            self.valueLabelsFromXData = false;
            self.xAxisLabelInfoList = [];
            if (xAxisOptions.annoMethod !== "valueLabels" ||
                (!xAxisOptions.valueLabels || xAxisOptions.valueLabels.length === 0)) {
                this.valueLabelsFromXData = true;
                if (this.timeUtil) {
                    $.each(this.timeUtil.times, function (idx, time) {
                        var value = {
                            value: idx,
                            text: Globalize.format(self._getXTickText(idx), annoFormatString, self._getCulture())
                        };
                        self.xAxisLabelInfoList.push(value);
                    });
                }
            } else {
                $.each(xAxisOptions.valueLabels, function (idx, vL) {
                    var value, xAxisLabelInfo;
                    if (typeof vL === "string") {
                        xAxisLabelInfo = {text: vL, value: idx};
                    } else if (typeof vL === "object") {
                        xAxisLabelInfo = { text: vL.text };
                        value = vL.value;
                        if (value !== undefined && value !== null &&
                            self._isDate(value)) {
                            value = self.timeUtil.getTimeIndex(value);
                        } else {
                            value = idx;
                        }
                        xAxisLabelInfo["value"] = value;
                        xAxisLabelInfo["gridLine"] = vL.gridLine;
                        xAxisLabelInfo["gridLineStyle"] = vL.gridLineStyle;
                    }
                    self.xAxisLabelInfoList.push(xAxisLabelInfo);
                });
            }

            xAxisInfo.annoMethod = "valueLabels";
            if (self.xAxisLabelInfoList && self.xAxisLabelInfoList.length) {
                xAxisInfo.valueLabels = self.xAxisLabelInfoList;
            }
        }

		_checkSeriesDataEmpty(series) {
			var type = (<wijcandlestickchart_options>this.options).type,
				data = series.data,
				checkEmptyData = this._checkEmptyData;

			if (!data || checkEmptyData(data.x)) {
				return true;
			}
			if (series.isTrendline) {
			    if (!series.isValid || !series.innerData) {
			        return true;
			    }
			    return checkEmptyData(data.y);
			    
			}
			if (checkEmptyData(data.high) || checkEmptyData(data.low)) {
			    return true;
			}
			if (type === strOHLC || type === strCandlestick) {
				if (checkEmptyData(data.open) || checkEmptyData(data.close)) {
					return true;
				}
			}
			return false;
		}

		_getLegendStyle(seriesStyle) {
			var style = seriesStyle.highLow;
			if (style) {
				style = $.extend({}, style);
				delete style.width;
				delete style.height;
				if (!style.stroke) {
					style.stroke = style.fill;
				}
				return style;
			}
			return seriesStyle;
		}

		_showHideSeries(seriesEles, type) {
			var nodeTypes = ["highLow", "open", "close", "path", "high", "low", "openClose"];
			$.each(seriesEles, (i, candlestickNode) => {
				$.each(nodeTypes, (idx, nodeType) => {
					if (candlestickNode[nodeType]) {
						candlestickNode[nodeType][type]();
					}
				});
				if (candlestickNode.path && $(candlestickNode.path.node).data("wijchartDataObj")) {
					$(candlestickNode.path.node).data("wijchartDataObj").visible = (type === "show");
				}
			});
		}

		_showSerieEles(seriesEles) {
		    if (seriesEles.isTrendline) {
		        TrendlineRender.showSerieEles(seriesEles);
		        return;
		    }
		    this._showHideSeries(seriesEles, "show");
		}

		_hideSerieEles(seriesEles) {
		    if (seriesEles.isTrendline) {
		        TrendlineRender.hideSerieEles(seriesEles);
		        return;
		    }
		    this._showHideSeries(seriesEles, "hide");
		}

		_paintPlotArea() {
			var o = (<wijcandlestickchart_options>this.options),
				render = new CandlestickChartRender(this.chartElement, {
				annotations: o.annotations,
				canvas: this.canvas,
				bounds: this.canvasBounds,
				tooltip: this.tooltip,
				widgetName: this.widgetName,
				axis: o.axis,
				seriesList: o.seriesList,
				seriesStyles: o.seriesStyles,
				seriesHoverStyles: o.seriesHoverStyles,
				seriesTransition: o.seriesTransition,
				showChartLabels: o.showChartLabels,
				textStyle: o.textStyle,
				chartLabelStyle: o.chartLabelStyle,
				chartLabelFormatString: o.chartLabelFormatString,
				shadow: o.shadow,
				disabled: this._isDisabled(),
				animation: o.animation,
				culture: this._getCulture(),
				type: o.type,
				wijCSS: o.wijCSS,
				mouseDown: $.proxy(this._mouseDown, this),
				mouseUp: $.proxy(this._mouseUp, this),
				mouseOver: $.proxy(this._mouseOver, this),
				mouseOut: $.proxy(this._mouseOut, this),
				mouseMove: $.proxy(this._mouseMove, this),
				click: $.proxy(this._click, this),
				timeUtil: this.timeUtil,
				candlestickFormatter: o.candlestickFormatter,
				widget: this
			});
			render.render();
		}

		_paintTooltip() {
			var fields = this.chartElement.data("fields"), trackers;
			super._paintTooltip();
			// if the series's data is empty, the field will be null.
			if (this.tooltip && fields && fields.trackers) {
				trackers = fields.trackers;
				this.tooltip.setTargets(trackers);
				this.tooltip.setOptions({ relatedElement: trackers[0] });
			}
		}

		_getTooltipText(fmt, target) {
			var dataObj = $(target.node).data("wijchartDataObj"),
				obj = {
					data: dataObj,
					label: dataObj.label,
					x: dataObj.x,
					high: dataObj.high,
					low: dataObj.low,
					open: dataObj.open,
					close: dataObj.close,
					target: target,
					fmt: fmt
				};
			return $.proxy(fmt, obj)();
		}

		_setDefaultTooltipText(data) {
			var type = (<wijcandlestickchart_options>this.options).type, tooltipText: string;

			tooltipText = data.label + " - " + Globalize.format(data.x, "d") + '\n High:' + data.high + '\n Low:' +
				data.low;

			if (type !== strHL) {
				tooltipText += '\n Open:' + data.open + '\n Close:' + data.close + '';
			}
			return tooltipText;
		}

		_getStyles(type, seriesStyles, optionName, isCompositeChart) {
		    function getStyle(type, currentStyle, defaultStyle, needDefaultFill, isCompositeChart) {
		        var resultStyle = {},
                    styleNames = [];

		        if (defaultStyle === null || defaultStyle === undefined) {
		            return;
		        }
                if (type === strHL) {
	                styleNames = ["highLow"];		            
                } else if (type === strOHLC) {
	                styleNames = ["open", "close", "highLow"];
                } else {
	                styleNames = ["fallingClose", "risingClose", "unchangeClose", "highLow"];
                }
                $.each(styleNames, (j, styleName) => {
                    if (isCompositeChart) {
                        currentStyle[styleName] = $.extend(true, {}, currentStyle, currentStyle[styleName]);
                    }
	                resultStyle[styleName] = $.extend(true, {}, defaultStyle, currentStyle[styleName]);
	                if (needDefaultFill && styleName == "risingClose"
                        && (!currentStyle[styleName] || !currentStyle[styleName].fill)) {
		                resultStyle[styleName].fill = null;
	                }
	                if (styleName === "fallingClose" && (!currentStyle[styleName] || !currentStyle[styleName][strStrokeWidth])) {
	                    resultStyle[styleName][strStrokeWidth] = 0;
                    }
                    if (styleName === "open" || styleName === "close" || styleName === "highLow") {
                        if (!currentStyle[styleName] || !currentStyle[styleName].stroke) {
                            resultStyle[styleName].stroke = resultStyle[styleName].fill;
                        }
                    }
                });
		        return resultStyle;
		    }

		    var needDefaultFill = (optionName === "seriesStyles"),
                defFills = wijchartcore.prototype._getDefFill(),
                currentCount = seriesStyles.length,
                maxCount = 0,
                defaultStyles,
                realStyles = [],
                index = 0;

		    maxCount = Math.max(wijchartcore.prototype.options[optionName].length, needDefaultFill ? defFills.length : 0, currentCount);
		    defaultStyles = $.extend(true, new Array(maxCount), wijchartcore.prototype.options[optionName]);
		    for (index = 0; index < maxCount; index++) {
		        if (index >= currentCount) {
		            seriesStyles[index] = {};
		        }
		        if (needDefaultFill && index < defFills.length) {
		            if (!defaultStyles[index]) {
		                defaultStyles[index] = {};
		            }
		            defaultStyles[index].fill = defFills[index];
		        }
		        realStyles.push(getStyle(type, seriesStyles[index], defaultStyles[index], needDefaultFill, isCompositeChart));
		    }
		    return realStyles;
		}

		_handleChartStyles() {
		    var o = (<wijcandlestickchart_options>this.options);
		    o["seriesStyles"] = this._getStyles(o.type, o["seriesStyles"], "seriesStyles", false);
		    o["seriesHoverStyles"] = this._getStyles(o.type, o["seriesHoverStyles"], "seriesHoverStyles", false);
		}

		_create() {
            this._handleChartStyles();
            this.bindXData = false;
			super._create();

			this.chartElement.addClass((<wijcandlestickchart_options>this.options).wijCSS.wijCandlestickChart);
        }

		_setOption(key, value) {
			if (key === "axis") {
				this._handleMaxMinInAxis(value);
			} else if (key === "type") {
				(<wijcandlestickchart_options>this.options).type = value;
                this._handleChartStyles();
                this.styles = {
                    style: this.options.seriesStyles.slice(),
                    hoverStyles: this.options.seriesHoverStyles.slice()
                };
			}
			super._setOption(key, value);
		}

        _seriesListSeted() {
            //if (this.timeUtil) {
            //    this.timeUtil.dispose();
            //    this.timeUtil = null;
            //}
			//this._handleXData();
		}

		// if user set max and min for x axis, they should set OADate or datetime, here need to calculate it to index by datetimeUtil.
		// If it is set other values, here will calculate wrong dateUtil.
		_handleMaxMinInAxis(axis) {
			var xAxis,
				oldAxis = (<wijcandlestickchart_options>this.options).axis,
				handleTime = (val) => {
					if (this._isDate(val)) {
						return this.timeUtil.getTimeIndex(val);
					} else {
						return this.timeUtil.getTimeIndex($.fromOADate(val));
					}
				};
			if (axis && axis.x) {
				xAxis = axis.x;
                $.each(["max", "min"], (i, name) => {
                    var newValue = xAxis[name];
                    if (newValue === undefined || newValue === null) {
                        return true;
                    }

                    if (!oldAxis || !oldAxis.x || oldAxis.x[name] !== newValue) {
                        xAxis[name] = handleTime(newValue);
					}
				});
			}
		}

		_handleXData() {
			var o = (<wijcandlestickchart_options>this.options),
				seriesList = o.seriesList = $.arrayClone(o.seriesList),
				xValues = [],
				isContainsDateTime = false,
				containsXData = (series) => {
					return series.data && series.data.x;
				},
				restoreXValues = (series) => {
					var xData = series.data.x;
					$.each(xData, (i, x) => {
						if (!this._isDate(x)) {
							xData[i] = this.timeUtil.getTime(x);
						}
					})
				};

			if (this.timeUtil && !this.bindXData) {
				return;
            }
			this.bindXData = false;

			//TODO check the x data is datetime, If it's not datetime, throw an exception.	
			$.each(seriesList, (i, series) => {
				if (containsXData(series) && $.isArray(series.data.x)) {
					xValues = xValues.concat(series.data.x);
				}
			});

			// when the timeUtil is init, and the series x data contains date value, 
			// Here need to recalculate the dateUtil and new x value.
			if (this.timeUtil) {
				$.each(xValues, (i, n) => {
					if (this._isDate(n)) {
						isContainsDateTime = true;
						return false;
					}
				});
				if (isContainsDateTime) {
					xValues = [];
					$.each(seriesList, (i, series) => {
						if (containsXData(series)) {
						restoreXValues(series);
						xValues = xValues.concat(series.data.x);
						}
					});
					this.timeUtil.dispose();
					this.timeUtil = null;
				}
			}

			if (!this.timeUtil) {
				this.timeUtil = new datetimeUtil(xValues);
            }

			$.each(seriesList, (i, series) => {
                if (containsXData(series) && $.isArray(series.data.x)) {
                    // Backup the original x values, recover it after paint.
                    series.data.originalXValue = series.data.x;

					series.data.x = $.map(series.data.x, (n, j) => {
						return this.timeUtil.getTimeIndex(n);
					});
				}
            });
		}

		_bindData() {
			super._bindData();
			//this._handleXData();
        }

        _preHandleSeriesData() {
            if (this.timeUtil) {
                this.timeUtil.dispose();
                this.timeUtil = null;
            }
            this._handleXData();

            super._preHandleSeriesData();
        }

		// handle data bind
		_bindSeriesData(ds, series, sharedXList) {
			var data = series.data;
			super._bindSeriesData(ds, series, sharedXList);
			if (data.x && $.isArray(data.x)) {
				this.bindXData = true;
			}
			$.each(["high", "low", "open", "close"], (i, name) => {
				var d = data[name];
				if (d && d.bind) {
					data[name] = this._getBindData(ds, d.bind);
				}
			});
		}

		/** Remove the functionality completely. This will return the element back to its pre-init state. */
		destroy() {
			var element = this.chartElement;
			element.removeClass((<wijcandlestickchart_options>this.options).wijCSS.wijCandlestickChart);
			super.destroy();
			if (this.timeUtil) {
				this.timeUtil.dispose();
			}
			element.data("fields", null);
		}

		/**
		* This method returns the candlestick elements, which has a set of Raphaël objects  that represent candlestick elements for the
		* series data, from the specified index.
		* @param {number} index The zero-based index of the candlestick to return.
		* @returns {Object} candlestick object which contains Raphael elements.
		*/
		getCandlestick(index:number) {
			var fields = this.chartElement.data("fields");
			if (fields && fields.chartElements && fields.chartElements.candlestickEles) {
				return fields.chartElements.candlestickEles[index] || null;
			}
			return null;
		}

		_clearChartElement() {
			var o = (<wijcandlestickchart_options>this.options),
				fields = this.chartElement.data("fields"),
				chartElements, candlestickEles;

			if (fields && fields.chartElements) {
				chartElements = fields.chartElements;
				candlestickEles = chartElements.candlestickEles;
				$.each(candlestickEles, (i, candlestickEle) => {
					$.each(candlestickEle, (key, node) => {
						if (node && node[0]) {
							node.wijRemove();
						}
						candlestickEle[key] = null;
					});
					candlestickEles[i] = null;
				});
				chartElements.candlestickEles = null;
				if (chartElements.group) {
					chartElements.group.wijRemove();
				}
				chartElements.group = null;
				if (fields.trackers) {
					fields.trackers.wijRemove();
				}
				fields.trackers = null;
			}

			super._clearChartElement();
		}

		_calculateParameters(axisInfo, options) {
			super._calculateParameters(axisInfo, options);
			if (axisInfo.id === "x") {
                var minor = options.unitMinor,
					autoMin = options.autoMin,
					autoMax = options.autoMax,
					adj = this._getCandlestickAdjustment(axisInfo, minor);

				if (autoMin) {
					axisInfo.min -= adj;
				}

				if (autoMax) {
					axisInfo.max += adj;
				}

                this._calculateMajorMinor(options, axisInfo);
			}
		}

        _calculateMajorMinor(axisOptions, axisInfo) {
            var self = this, unitMajor;
			if (axisInfo.id === "x") {
				if (!axisOptions.annoFormatString || axisOptions.annoFormatString.length === 0) {
					axisInfo.annoFormatString = ChartDataUtil.getTimeDefaultFormat(this.timeUtil.getOATime(axisInfo.max), this.timeUtil.getOATime(axisInfo.min));
				} else {
					axisInfo.annoFormatString = axisOptions.annoFormatString;
                }
			}
            super._calculateMajorMinor(axisOptions, axisInfo);
            unitMajor = axisOptions.unitMajor ? axisOptions.unitMajor : 1;
            self._adjustXValueLabelsByMajorUnit(unitMajor, axisInfo);
        }

        _adjustXValueLabelsByMajorUnit(unitMajor, xAxisInfo) {
            if (xAxisInfo.id === "x") {
                if (this.valueLabelsFromXData) {
                    xAxisInfo.valueLabels = [];
                    $.each(this.xAxisLabelInfoList, function (idx, valueLabel) {
                        if (idx % unitMajor === 0) {
                            xAxisInfo.valueLabels.push(valueLabel);
                        }
                    });
                }
            }
        }

        _getCandlestickAdjustment(axisInfo, minor) {
            var length = 0, adj;

            $.each((<wijcandlestickchart_options>this.options).seriesList, (i, s) => {
                if (s.data && s.data.x && $.isArray(s.data.x)) {
                    length = Math.max(s.data.x.length, length);
                }
            });
            adj = (axisInfo.max - axisInfo.min) / length;
            if (adj === 0) {
                adj = minor;
            } else {
                if (minor < adj && minor !== 0) {
                    adj = Math.floor(adj / minor) * minor;
                }
            }
            return adj;
        }
		
		_getXTickText(text) {
			if (this.timeUtil.getCount() === 0) {
				return <any>"";
			}
			return this.timeUtil.getTime(text);
		}

		_adjustTickValuesForCandlestickChart(tickValues) {
			var isValueLabels = (<wijcandlestickchart_options>this.options).axis.x.annoMethod === "valueLabels",
				values = [];
			if (isValueLabels && tickValues && $.isArray(tickValues)) {
				$.each(tickValues, (i, value) => {
					values[i] = this.timeUtil.getTimeIndex($.fromOADate(value));
				});
				return values;
			}
			return super._adjustTickValuesForCandlestickChart(tickValues);
		}

        _getTickTextForCalculateUnit(value, axisInfo, prec) {

            if (axisInfo.id === "x") {
                return Globalize.format(this._getXTickText(value), axisInfo.annoFormatString, this._getCulture());
            }
            return super._getTickTextForCalculateUnit(value, axisInfo, prec);
        }
	}

	/** @ignore */
	export class CandlestickChartRender extends BaseChartRender{
		element: JQuery;
		options: any;
		bounds: any;
		type: string;
		timeUtil: datetimeUtil;
		satrtLocation: any;
		fields: any;
		trackers: RaphaelSet;
		isHover: boolean;
		group: any;
		aniPathsAttr: any[];
		fieldsAniPathAttr: any[];
		paths: any[];
		animationSet: any;

		constructor(element, options) {
			super(element, options);
			var self = this;
			self.bounds = options.bounds,
			self.type = options.type;
			self.timeUtil = options.timeUtil;
			self.trackers = options.canvas.set();
			self.isHover = false;
			self.animationSet = options.canvas.set();
			self.fieldsAniPathAttr = [];
			self.paths = [];
			self.aniPathsAttr = [];
		}

		_paintCandlestickElements(series, seriesStyle, seriesHoverStyle,sIdx) {
			var data = series.data,
				length = Math.min(data.x.length, data.high.length, data.low.length),
				wijCSS = (<wijcandlestickchart_options>this.options).wijCSS,
				commonData:any = $.extend({ candlestickType: this.type, visible: true }, series),
				seriesEles = [],
				widget = (<wijcandlestickchart_options>this.options).widget,
				candlestickWidth, i, candlestickEles, pathNode, dataObj, pointX;

			commonData.type = "candlestick";
			if (data.open && data.close) {
				length = Math.min(length, data.open.length, data.close.length);
			}
			candlestickWidth = this.width / length;
			for (i = 0; i < length; i++) {
				candlestickEles = this._paintCandlestickGraphic(data, i, candlestickWidth, seriesStyle, seriesHoverStyle);

				if (this.annoPoints[sIdx] == null)
					this.annoPoints[sIdx] = {};
				this.annoPoints[sIdx][i] = candlestickEles.annotation;


				dataObj = $.extend({}, commonData);
				if (candlestickEles.path) {
					pathNode = $(candlestickEles.path.node);
					$.wijraphael.addClass(pathNode, wijCSS.canvasObject + " " + wijCSS.candlestickChart + " " + wijCSS.candlestickChartTracker);
					pathNode.css("opacity", 0);
					if (this.type !== strHL) {
						$.extend(dataObj, { open: data.open[i], close: data.close[i] });
					}
					$.extend(dataObj, {
						high: data.high[i], low: data.low[i], index: i, x: this.timeUtil.getTime(data.x[i]),
						candlestickEles: candlestickEles, style: seriesStyle, hoverStyle: seriesHoverStyle
					});
					pathNode.data("wijchartDataObj", dataObj);
				}

				if (series.visible === false) {
					$.each(candlestickEles, (i, ele) => {
						if (ele && ele.hide) {
							ele.hide();
						}
					});
				}

				seriesEles.push(candlestickEles);
				// cache the bar position to show indicator line.
				widget.dataPoints = widget.dataPoints || {};
				widget.pointXs = widget.pointXs || [];
				pointX = candlestickEles.x;

				if (!widget.dataPoints[pointX.toString()]) {
					widget.dataPoints[pointX.toString()] = [];
					widget.pointXs.push(pointX);
				}

				widget.dataPoints[pointX.toString()].push(dataObj);
			}
			return seriesEles;
		}

		_unbindLiveEvents() {
			var widgetName = (<wijcandlestickchart_options>this.options).widgetName;
			this.element.off("." + widgetName, ".wijcandlestickchart");
			TrendlineRender.unbindLiveEvents(this.element, widgetName, this.options.wijCSS);
		}

		_bindLiveEvents() {
			var o = (<wijcandlestickchart_options>this.options),
				eventPrefix = "",
				element = this.element;

			if ($.support.isTouchEnabled && $.support.isTouchEnabled()) {
				eventPrefix = "wij";
			}

			function getDataFromEventArg(e) {
				var target = $(e.target);
				return target.data("wijchartDataObj");
			}

			function fireClientEvent(event, e, dataObj) {
				var map = {
					mouseover: o.mouseOver,
					mouseout: o.mouseOut,
					mousedown:o.mouseDown,
					mouseup: o.mouseUp,
					mousemove: o.mouseMove,
					click: o.click
				};
				if ($.isFunction(map[event])) {
					return map[event].call(element, e, dataObj);
				}
			}
			
			$.each(["mouseover", "mouseout", "mousedown", "mouseup", "mousemove", "click"], (i, event) => {
				this.element.on(eventPrefix + event + "." + o.widgetName, ".wijcandlestickchart", (e) => {
					if (o.disabled) {
						return;
					}
					var dataObj = getDataFromEventArg(e);
					if (fireClientEvent(event, e, dataObj) !== false) {
						if (event === "mouseover" || event === "mouseout") {
							this["_" + event](dataObj);
						}
					}
					dataObj = null;
				});
			});

			TrendlineRender.bindLiveEvents(element, o.widgetName, o.mouseDown,
            o.mouseUp, o.mouseOver, o.mouseOut, o.mouseMove, o.click,
            o.disabled, o.wijCSS, false);
		}

		_setCandlestickStyleForHover(eles, style, open, close) {
			var setStyle = (ele, style) => {
				if (ele && style) {
					ele.attr(style);
				}
			}

			setStyle(eles.highLow, style.highLow);
			setStyle(eles.open, style.open);
			setStyle(eles.close, style.close);
			setStyle(eles.high, style.highLow);
			setStyle(eles.low, style.highLow);
			if (open !== undefined && close !== undefined) {
				if (open > close) {
					setStyle(eles.openClose, style.fallingClose);
				} else if (open < close) {
					setStyle(eles.openClose, style.risingClose);
				} else {
					setStyle(eles.openClose, style.unchangeClose);
				}
			}
		}

		_mouseover(obj) {
			if (obj.hoverStyle) {
				this.isHover = true;
				this._setCandlestickStyleForHover(obj.candlestickEles, obj.hoverStyle, obj.open, obj.close);
			}
		}

		_mouseout(obj) {
			var style = obj.style;
			if (this.isHover) {
				this._setCandlestickStyleForHover(obj.candlestickEles, style, obj.open, obj.close);
				this._formatCandlestick({
					high: obj.high, low: obj.low, open: obj.open, close: obj.close, hlStyle: style.highLow, oStyle: style.open,
					cStyle: style.close, index: obj.index, eles: obj.candlestickEles, fallingCloseStyle: style.fallingClose,
					risingCloseStyle: style.risingClose, unchangeCloseStyle: style.unchangeClose
				});
				this.isHover = false;
			}
		}

		_paintCandlestickGraphic(data, index, candlestickWidth, seriesStyle, seriesHoverStyle) {
			var o = this.options,
				x = data.x[index],
				high = data.high[index],
				low = data.low[index],
				open = data.open ? data.open[index] : 0,
				close = data.close ? data.close[index] : 0,
				originX = this.bounds.startX,
				originY = this.bounds.endY,
				hasOC = this.type !== strHL,
				axis = o.axis,
				xAxis = axis.x,
				yAxis = axis.y,
				minX = xAxis.min,
				minY = yAxis.min,
				maxX = xAxis.max,
				maxY = yAxis.max,
				oWidth = 0,
				cWidth = 0,
				kx = this.width / (maxX - minX),
				ky = this.height / (maxY - minY),
				hlStyle, cStyle, oStyle, hlWidth, ox, oy, cx, cy, hx, hy, lx, ly, candlestickEles, halfWidth,
				fallingCloseStyle, risingCloseStyle, callbackObj, unchangeCloseStyle, extendStyle, closeWidth, candlestickStyle,
				annotation;

			function getstrokeFillStroke(style) {
				if (style) {
					return { fill: style.fill, stroke: style.stroke };
				}
				return null;
			}

			oStyle = seriesStyle.open;
			cStyle = seriesStyle.close;
			hlStyle = seriesStyle.highLow;
			fallingCloseStyle = seriesStyle.fallingClose;
			risingCloseStyle = seriesStyle.risingClose;
			unchangeCloseStyle = seriesStyle.unchangeClose;
			hlWidth = hlStyle[strStrokeWidth] || 0;
			hx = (x - minX) * kx + this.bounds.startX;
			hy = (originY) - (high - minY) * ky;
			lx = hx;
			ly = originY - (low - minY) * ky;
			if (hasOC) {
				if (this.type === strOHLC) {
					oWidth = oStyle[strStrokeWidth] || 0;
					cWidth = cStyle[strStrokeWidth] || 0;
				} else {
					// candlestick
					if (open > close) {
						oWidth = cWidth = fallingCloseStyle[strStrokeWidth] || 0;
						closeWidth = fallingCloseStyle.width || 0;
						extendStyle = getstrokeFillStroke(fallingCloseStyle);
						candlestickStyle = fallingCloseStyle;
					} else if (open === close) {
						oWidth = cWidth = unchangeCloseStyle[strStrokeWidth] || 0;
						closeWidth = unchangeCloseStyle.width || 0;
						extendStyle = getstrokeFillStroke(unchangeCloseStyle);
						candlestickStyle = unchangeCloseStyle;
					} else {
						oWidth = cWidth = risingCloseStyle[strStrokeWidth] || 0;
						closeWidth = risingCloseStyle.width || 0;
						extendStyle = getstrokeFillStroke(risingCloseStyle);
						candlestickStyle = risingCloseStyle;
					}

					// if hlStyle sets a style, 
					hlStyle = $.extend({}, extendStyle, hlStyle);
					candlestickWidth = candlestickWidth - (oWidth + cWidth) / 2;					
					if (candlestickWidth < 2) {
						candlestickWidth = 2;
					}
				}

				halfWidth = (candlestickWidth / 2) * 0.8;
				if (this.type === strCandlestick) {
					halfWidth = Math.min(closeWidth / 2, halfWidth);
				}
				if (halfWidth < 1) {
					halfWidth = 1;
				}
				ox = hx - halfWidth;
				oy = originY - (open - minY) * ky;
				cx = hx + halfWidth;
				cy = originY - (close - minY) * ky;
				oy = Math.max(oy, hy + oWidth / 2);
				oy = Math.min(oy, ly - oWidth / 2);
				cy = Math.max(cy, hy + cWidth / 2);
				cy = Math.min(cy, ly - cWidth / 2);
			}

			annotation = { x: hx, y: hy };

			if (this.type === strOHLC) {
				candlestickEles = this._paintOHLC({ h: { x: hx, y: hy }, l: { x: lx, y: ly }, o: { x: ox, y: oy }, c: { x: cx, y: cy } });
				candlestickEles.open.attr(oStyle);
				candlestickEles.close.attr(cStyle);
			} else if (this.type === strHL) {
				candlestickEles = this._paintHL({ h: { x: hx, y: hy }, l: { x: lx, y: ly } });
			} else {
				candlestickEles = this._paintCandlestick({ h: { x: hx, y: hy }, l: { x: lx, y: ly }, o: { x: ox, y: oy }, c: { x: cx, y: cy } });
				candlestickEles.high.attr(hlStyle);
				candlestickEles.low.attr(hlStyle);
				candlestickEles.openClose.attr(candlestickStyle);
			}
			if (candlestickEles.highLow) {
				candlestickEles.highLow.attr(hlStyle);
			}
			this._formatCandlestick({
				high: high, low: low, open: open, close: close, hlStyle: hlStyle, oStyle: oStyle, cStyle: cStyle, index: index, eles: candlestickEles,
				fallingCloseStyle: fallingCloseStyle, risingCloseStyle: risingCloseStyle, unchangeCloseStyle: unchangeCloseStyle
			});
			// add fill for fire the events, because the opacity of this element is 0, set any fill to this element is OK.
			candlestickEles.path.attr({
				"stroke-width": Math.max(closeWidth || 1, cWidth || 1, hlWidth || 1, oWidth || 1),
				fill: "#000"
			});
			this.trackers.push(candlestickEles.path);
			// for indicator line.
			candlestickEles.x = $.round(hx);

			// set the extend style to seriesStyle for hover event.
			seriesStyle.highLow = hlStyle;

			candlestickEles.annotation = annotation;

			return candlestickEles;
		}

		_formatCandlestick(options) {
			var candlestickFormatter = (<wijcandlestickchart_options>this.options).candlestickFormatter,
				index = options.index,
				high = options.high,
				low = options.low,
				open = options.open,
				close = options.close,
				hlStyle = options.hlStyle,
				oStyle = options.oStyle,
				cStyle = options.cStyle,
				fallingCloseStyle = options.fallingCloseStyle,
				risingCloseStyle = options.risingCloseStyle,
				unchangeCloseStyle = options.unchangeCloseStyle,
				type = this.type,
				data, styles;

			if (candlestickFormatter) {
				data = { high: high, low: low };
				styles = { highLow: hlStyle };
				if (type === strOHLC) {
					$.extend(data, { open: open, close: close });
					$.extend(styles, { open: oStyle, close: cStyle });
				} else if (type === strCandlestick) {
					$.extend(data, { open: open, close: close });
					$.extend(styles, {
						fallingClose: fallingCloseStyle,
						risingClose: risingCloseStyle,
						unchangeClose: unchangeCloseStyle
					});
				}
				candlestickFormatter.call(this, {data: data, eles: options.eles, style: styles});
			}
			}

		_paintTracker(options, canvas) {
			var x = [], y = [], maxX, minX, maxY, minY;
			$.each(options, (key, point) => {
				x.push(point.x);
				y.push(point.y);
			});
			maxX = Math.max.apply(null, x);
			minX = Math.min.apply(null, x);
			maxY = Math.max.apply(null, y);
			minY = Math.min.apply(null, y);
			return canvas.rect(minX, minY, maxX - minX, maxY - minY);
		}

		_paintOHLC(options) {
			var h = options.h,
				l = options.l,
				o = options.o,
				c = options.c,
				canvas = (<wijcandlestickchart_options>this.options).canvas,
				hlPath = ["M", h.x, ",", h.y, "V", l.y],
				oPath = ["M", o.x, ",", o.y, "H", h.x],
				cPath = ["M", h.x, ",", c.y, "H", c.x],
				hlEle, oEle, cEle, path;

			hlEle = canvas.path(hlPath.join(""));
			oEle = canvas.path(oPath.join(""));
			cEle = canvas.path(cPath.join(""));
			path = this._paintTracker(options, canvas);
			this._setTrackerForCandlestickEle([hlEle, oEle, cEle], path);
			this.group.push(oEle, cEle, hlEle);
			return { highLow: hlEle, open: oEle, close: cEle, path: path };
		}

		_setTrackerForCandlestickEle(eles, tracker) {
			$.each(eles, (i, ele) => {
				ele.node.tracker = tracker;
			});
		}

		_paintHL(options) {
			var h = options.h,
				l = options.l,
				canvas = (<wijcandlestickchart_options>this.options).canvas,
				hlEle, pathEle;

			hlEle = canvas.path(["M", h.x, ",", h.y, "V", l.y].join(""));
			pathEle = hlEle.clone();
			this._setTrackerForCandlestickEle([hlEle], pathEle);
			this.group.push(hlEle);
			return { highLow: hlEle, path: pathEle };
		}

		_paintCandlestick(options) {
			var h = options.h,
				l = options.l,
				o = options.o,
				c = options.c,
				canvas = this.options.canvas,
				ocEle, hEle, lEle, topY, bottomY, hPath, lPath, ocPath, path;

			if (c.y < o.y) {
				topY = c.y;
				bottomY = o.y;
			} else {
				topY = o.y;
				bottomY = c.y;
			}
			hPath = ["M", h.x, ",", h.y, "V", topY];
			lPath = ["M", l.x, ",", l.y, "V", bottomY];
			ocPath = ["M", o.x, ",", topY, "H", c.x, "V", bottomY, "H", o.x, "V", topY, "Z"];
			hEle = canvas.path(hPath.join(""));
			lEle = canvas.path(lPath.join(""));
			ocEle = canvas.path(ocPath.join(""));
			path = this._paintTracker(options, canvas);
			this.group.push(hEle, lEle, ocEle);
			this._setTrackerForCandlestickEle([hEle, lEle, ocEle], path);
			return { high: hEle, low: lEle, openClose: ocEle, path: path };
		}

		_handleStyle(style, notHandleWidth?) {
			if (!style) {
				return;
			}
			if (notHandleWidth !== true) {
				$.each(["width", "height"], (i, prop) => {
					if (style[prop]) {
						style["stroke-width"] = style[prop];
						delete style[prop];
					}
				});
			}
			return style;
		}

		_extendStyles(style) {
			var s:any = $.extend(true, {}, style);

			if (s) {
				s.highLow = this._handleStyle(s.highLow);
				s.close = this._handleStyle(s.close);
				s.open = this._handleStyle(s.open);
				s.fallingClose = this._handleStyle(s.fallingClose, true);
				s.risingClose = this._handleStyle(s.risingClose, true);
				s.unchangeClose = this._handleStyle(s.unchangeClose, true);
			}
			return s;
		}

		_paintCandlesticks() {
		    var seriesEls = [], seriesEl,
                self = this,
				o = (<wijcandlestickchart_options>self.options),
				seriesList = o.seriesList,
				seriesStyles = o.seriesStyles,
				seriesHoverStyles = o.seriesHoverStyles,
				fields = self.element.data("fields") || {},
				chartElements = fields.chartElements || {},
				candlestickEles = chartElements.candlestickEles || [],
			    ani = o.animation,
                seTrans = o.seriesTransition,
                wijCSS = o.wijCSS,
                paths = [], shadowPaths=[],
                canvas = o.canvas;
			self.annoPoints = {};
			$.each(seriesList, (i, series) => {
			    var seriesStyle = self._extendStyles(seriesStyles[i]),
                    seriesHoverStyle = self._extendStyles(seriesHoverStyles[i]);
                if ((<wijchartcore>o.widget)._checkSeriesDataEmpty(series)) {
                    return true;
                }
			    if (series.isTrendline) {
			        TrendlineRender.renderSingleTrendLine(series, seriesStyle["highLow"], seriesHoverStyle["highLow"], o.axis,
                        undefined, self.fieldsAniPathAttr, ani, seTrans, i,
                        self.bounds, canvas, paths, shadowPaths,
                        self.animationSet, self.aniPathsAttr, wijCSS, seriesEls, false, o.shadow);
				} else {
			        seriesEl = self._paintCandlestickElements(series, seriesStyle, seriesHoverStyle,i);
				    candlestickEles = candlestickEles.concat(seriesEl);
				    seriesEls.push(seriesEl);
			    }
			});
			fields.seriesEles = seriesEls;
			fields.trackers = self.trackers;
			chartElements.candlestickEles = candlestickEles;
			chartElements.group = self.group;
            fields.trendLines = paths;
			fields.chartElements = chartElements;
			self.fields = fields;
			self.trackers.toFront();
		}

		// Using group to improve clip animation performance.
		_playAnimation() {
			var animation = (<wijcandlestickchart_options>this.options).animation,
				animated = animation && animation.enabled,
                duration, easing;

			if (animated) {
				duration = animation.duration || 400;
				easing = animation.easing;
				this.group.attr("clip-rect", [this.bounds.startX, this.bounds.startY, 0, this.height].join(","));
				this.group.wijAnimate({ "clip-rect": [this.bounds.startX, this.bounds.startY, this.width, this.height].join(",") }, duration, easing);
			}
		}

		_playTrendLineAnimation() {
			var self = this,
				o = (<wijcandlestickchart_options>self.options),
				animation = o.animation,
				animated = animation && animation.enabled,
				seriesTransition = o.seriesTransition,
				trendLines = self.fields.trendLines,
				duration,
				easing;

			if (animated) {
				duration = animation.duration || 400;
				easing = animation.easing || "linear";
				if (trendLines && trendLines.length) {
					TrendlineRender.playAnimation(animated, duration, easing, seriesTransition, o.bounds,
						trendLines, self.fieldsAniPathAttr, o.axis, o.widget.extremeValue);
				}
			}
		}

		render() {
			this.group = (<wijcandlestickchart_options>this.options).canvas.group();			
			this._paintCandlesticks();
			this.element.data("fields", this.fields);
			this._unbindLiveEvents();
			this._bindLiveEvents();
			this._playAnimation();
			this._playTrendLineAnimation();
			super.render();
		}
	}
    
    export class wijcandlestickchart_css extends wijchartcore_css
    {
		wijCandlestickChart: string = "wijmo-wijcandlestickchart";
		candlestickChart: string = "wijcandlestickchart";
        candlestickChartTracker: string = "candlesticktracker";
	}

	class wijcandlestickchart_options extends wijchartcore_options {
		/** A value that indicates how to draw the candlestick element. Possible value is ohlc, hl, candlestick*/
		type = strCandlestick;
		/**
		* The animation option defines the animation effect and controls other aspects of the widget's animation, 
		* such as duration and easing. 
		*/
		animation: chart_animation = {
			enabled: true,
			duration: 400,
			easing: ">"
		};
		/**
		* @ignore
		*/
		wijCSS = new wijmo.chart.wijcandlestickchart_css();
		/**
		* A callback to format the candlestick element.
		* @type Function
		 */
		candlestickFormatter = null;
		/** @ignore*/
		showChartLabels = true;
		/** @ignore*/
		chartLabelStyle = {};
		/** @ignore*/
		chartLabelFormatString = "";
		/**
		* Creates an object to use as the tooltip, or hint, when the mouse is over a chart element.
		*/
        hint = $.extend(true, {}, this.hint, <chart_hint>{
            contentStyle: {
                "font-size": 12
            }
        });
		/**
		* This event fires when the user clicks a mouse button.
		* @event
		* @param {jQuery.Event} e Standard jQuery event object
		* @param {ICandlestickChartEventArgs} data Information about an event
		*/
		mouseDown: (e: JQueryEventObject, args: ICandlestickChartEventArgs) => void = null;
		/**
		* Fires when the user releases a mouse button while the pointer is over the chart element.
		* @event
		* @param {jQuery.Event} e Standard jQuery event object
		* @param {ICandlestickChartEventArgs} data Information about an event
		*/
		mouseUp: (e: JQueryEventObject, args: ICandlestickChartEventArgs) => void = null;
		/**
		* Fires when the user first places the pointer over the chart element.
		* @event
		* @param {jQuery.Event} e Standard jQuery event object
		* @param {ICandlestickChartEventArgs} data Information about an event
		*/
		mouseOver: (e: JQueryEventObject, args: ICandlestickChartEventArgs) => void = null;
		/**
		* Fires when the user moves the pointer off of the chart element.
		* @event
		* @param {jQuery.Event} e Standard jQuery event object
		* @param {ICandlestickChartEventArgs} data Information about an event
		*/
		mouseOut: (e: JQueryEventObject, args: ICandlestickChartEventArgs) => void = null;
		/**
		* Fires when the user moves the mouse pointer while it is over a chart element.
		* @event
		* @param {jQuery.Event} e Standard jQuery event object
		* @param {ICandlestickChartEventArgs} data Information about an event
		*/
		mouseMove: (e: JQueryEventObject, args: ICandlestickChartEventArgs) => void = null;
		/**
		* Fires when the user clicks the chart element. 
		* @event
		* @param {jQuery.Event} e Standard jQuery event object
		* @param {ICandlestickChartEventArgs} data Information about an event
		*/
		click: (e: JQueryEventObject, args: ICandlestickChartEventArgs) => void = null;
	}

    wijcandlestickchart.prototype.options = <wijcandlestickchart_options>$.extend(true, {}, wijmoWidget.prototype.options, new wijcandlestickchart_options());
	wijcandlestickchart.prototype.widgetEventPrefix = widgetName;
	$.wijmo.registerWidget(widgetName, wijcandlestickchart.prototype);

	// TODO
	export interface ICandlestickChartEventArgs {
		/** Type of the target. It's value is "candlestick". */
		type: string;
		/** Candlestick type of the target,  It's value is "hl", "ohlc", "candlestick". */
		candlestickType: string;
		/** Label of the candlestick.*/
		label: string;
		/** Data of the series of the candlestick.*/
		data: any;
		/** Legend entry of the candlestick.*/
		legendEntry: boolean;
		/** The high value of the candlestick data. */
		high: number;
		/** The low value of the candlestick data. */
		low: number;
		/** The open value of the candlestick data. */
		open: number;
		/** The close value of the candlestick data. */
		close: number;
		/** The time value of the candlestick data. */
		x: Date;
		/** The data index in the series data. */
		index: number;
		/** The style of this target candlestick elements. */
		style: Object;
		/** The hover style of this target candlestick elements. */
		hoverStyle: Object;
	}
}

/** @ignore */
interface JQuery {
    wijcandlestickchart: JQueryWidgetFunction;
}