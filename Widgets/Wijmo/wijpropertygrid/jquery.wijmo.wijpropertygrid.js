﻿/*globals jQuery*/
/*
* Depends:
*	jquery-1.4.2.js
*	jquery.ui.core.js
*	jquery.ui.widget.js
*/
(function ($) {
	"use strict";

	var csspre = "wijmo-wijppg",
		optionEditors = {},
		optionEditorMapWij = {},
		objToObjDescptr = function (widgetName, options) {
			var objDescptr = {},
                    widget = $.wijmo[widgetName],
                    defaultOpts = {};

			if (widget) {
				defaultOpts = widget.prototype.options;
			}

			return parseObject(options, defaultOpts);
		},
		parseObject = function (object, defaultVal) {
			var objDescptr = {};
			$.each(object, function (key, value) {
				var type = getType(value);
				objDescptr[key] = {};

				if (defaultVal) {
					objDescptr[key].defaultValue = defaultVal[key];
				}
				objDescptr[key].type = type;
				objDescptr[key].desc = "";
				if (type === "object") {
					objDescptr[key].type = parseObject(value);
				}
				if (type === "array") {
					if (value.length) {
						if (getType(value[0]) === "object") {
							objDescptr[key].type = "collection";
							objDescptr[key].itemType = parseObject(value[0]);
						}
						else {
							objDescptr[key].type = "array[$1]".replace(/\$\1/, type);
						}
					}
				}
			});
			return objDescptr;
		},
		getType = function (value) {
			if (typeof value === "object") {
				if ($.isPlainObject(value)) {
					return "object";
				}
				var regExp = /\[object (\w+)\]/,
							typeStr = Object.prototype.toString.apply(value);
				if (regExp.test(typeStr)) {
					return regExp.exec(typeStr)[1].toLowerCase();
				}
				return "";
			}
			else {
				return typeof value;
			}
		};


	$.each(["array", "boolean", "collection", "datetime", "enum",
	"number", "string", "custom"], function (i, n) {
		optionEditors[n] = [];
	});

	optionEditorMapWij["array"] = "wijarrayeditor";
	optionEditorMapWij["boolean"] = "wijdropdowneditor";
	optionEditorMapWij["collection"] = "wijcollectioneditor";
	optionEditorMapWij["datetime"] = "wijdatetimeeditor";
	optionEditorMapWij["enum"] = "wijdropdowneditor";
	optionEditorMapWij["number"] = "wijnumbereditor";
	optionEditorMapWij["string"] = "wijuieditor";


	$.widget("wijmo.wijpropertygrid", {
		options: {
			optionsDesc: null,
			optionsValue: null,
			widget: {},
			valueChanged: null
		},

		_create: function () {
			var self = this,
				ele = self.element,
				o = self.options,
				w = o.widget;

			if (!$.isPlainObject(o.optionsDesc) && w.name && w.element) {
				var widget = w.element.data(w.name), options;
				if (widget) {
					options = widget.options;
					o.optionsDesc = objToObjDescptr(w.name, options);
					o.optionsValue = options;
				}
			}

			self.initValue = o.optionsValue ?
			o.optionsValue : self._createIniValueFromOptsDesc(o.optionsDesc);
			self.showCategory = false;

			ele.addClass(csspre + " ui-widget-content ui-corner-all");
			//ele.append(self._createHeader());

			if (!self.showCategory) {
				self._createOptionsNoCategory();
			} else {
				self._createOptionsByCategory();
			}
			self._createDescription();
		},

		_createDescription: function () {
			var self = this,
				ele = self.element;
			self.descArea = $("<div class='wijmo-wijppg-description'></div>");
			ele.append(self.descArea);
		},

		_setOption: function (key, value) {
			$.Widget.prototype._setOption.apply(this, arguments);
		},

		//create tree
		_createOptionsNoCategory: function () {
			var self = this,
				ele = self.element,
				treeNodes;

			self.optionsTreeNoGroup = self._createOptionsTree().appendTo(ele);

			treeNodes = self._createWijtree(self.optionsTreeNoGroup)
			.find("li.wijmo-wijtree-parent");

			treeNodes.wijtreenode("expand");
			self._wrapUiEditors();
			treeNodes.wijtreenode("collapse");
		},

		_createWijtree: function (element) {
			var self = this, options = self.options.optionsDesc;
			return element.wijtree({
				selectedNodeChanged: function (e, d) {
					var value = d.element[0].attributes["value"].value;
					if (options && options[value]) {
						self.descArea.html(options[value].desc);
					}
				}
			});
		},

		_createOptionsTree: function () {
			var self = this,
				o = self.options,
				container = $("<ul />");

			self._initOptionsTree(container, o.optionsDesc, self.initValue);
			return container;
		},

		_initOptionsTree: function (parentNode, optionsDesc, optionsValue) {
			var self = this,
				optionDesc, optionValue,
				sortedNames = self._getSortedOptNames(optionsDesc);

			$.each(sortedNames, function (i, key) {
				optionDesc = optionsDesc[key];
				if (self._isNeedExpand(optionDesc)) {
					var ulNode = $("<ul />"),
						liNode = self._createBranchNode(key);

					optionValue = optionsValue ?
						(optionsValue[key] || (optionsValue[key] = {})) : null;

					self._initOptionsTree(ulNode, optionDesc["type"], optionValue);
					ulNode.appendTo(liNode.appendTo(parentNode));
				}
				else {
					parentNode.append(self._createLeafNode(key, optionDesc, optionsValue));
				}
			});
			return parentNode;
		},

		_createNodeMarkup: function (name, createEditor) {
			var originlEditor = this._createStringEditor(),
				liContent = $("<div />")
				.append("<span  class = 'wijmo-wijppg-label'>" + name + "</span>")
				.append(originlEditor.wrap("<div class='wijmo-wijppg-editor'></div>").parent());

			createEditor(originlEditor);

			return $("<li value=" + name + " />").append(liContent);
		},

		_createBranchNode: function (name) {
			return this._createNodeMarkup(name, function (originlEditor) {
				originlEditor.wijtextbox();
			});
		},

		_createLeafNode: function (name, optionDesc, optionValue) {
			var self = this;
			return self._createNodeMarkup(name, function (originlEditor) {
				self._collectUiEditor(originlEditor, optionDesc, name, optionValue);
			});
		},

		_isNeedExpand: function (optionDesc) {
			return (!optionDesc["uieditor"] && $.isPlainObject(optionDesc["type"]));
		},
		//end create tree

		//create category
		optionsTreesByCategory: null,

		_createOptionsByCategory: function () {
			var self = this,
				ele = self.element;

			self.optionsTreesByCategory = self._createTreeByCategory()
			.appendTo(ele)
			.wijaccordion({
				requireOpenedPane: false,
				header: "h3"
			});
			self._wrapUiEditors();
		},

		_createTreeByCategory: function () {
			var self = this,
				o = self.options,
				container = $("<div>"),
				optionsDescByCategory = self._getOptionsDescByCategory(o.optionsDesc);

			if (!optionsDescByCategory) { return; }

			$.each(optionsDescByCategory, function (category, optDesc) {
				container.append(self._createCategoryContent(category, optDesc));
			});

			return container;
		},

		_createCategoryContent: function (categoryName, optionsDesc) {
			var self = this,
				treeContainer = $("<ul />"),
				categoryContent = $("<div><h3><a href='#'>" + categoryName + "</a></h3></div>");

			self._initOptionsTree(treeContainer, optionsDesc, self.initValue);
			categoryContent.append(treeContainer.wrap("<div ></div>").parent());

			self._createWijtree(treeContainer);
			return categoryContent;
		},
		//end create category

		//collect editor
		_collectUiEditor: function (originlEditor, optionDesc, optionName, optionValue) {
			var self = this,
				uiEditor = optionDesc["uieditor"],
				optionType = self._parseType(optionDesc["type"]),
				editorName = uiEditor ? "custom" :
				(optionEditors[optionType] ? optionType : "string");

			optionEditors[editorName].push({
				editor: originlEditor,
				optionDesc: optionDesc,
				optionName: optionName,
				value: optionValue
			});
		},

		_wrapUiEditors: function () {
			var self = this;

			$.each(optionEditors, function (key, value) {
				$.each(value, function (index, editorArg) {
					self._wrapDefaultEditor(editorArg);
				});
			});
		},

		_wrapDefaultEditor: function (editorArg) {
			var self = this,
				optionDesc = editorArg.optionDesc,
				parentValue = editorArg.value,
				optionName = editorArg.optionName,
				value = parentValue ? parentValue[optionName] : null,
				defaultEditor = editorArg.editor,
				editorName = self._getUiEditorByOptionDesc(optionDesc);

			defaultEditor.data("parentValue", parentValue);
			if (defaultEditor[editorName]) {
				defaultEditor[editorName]({
					optionsDesc: optionDesc,
					name: optionName,
					value: value,
					valueChanged: function (e, data) {
						self._valueChanged(e, data); 
					}
				});
			}
		},

		_getUiEditorByOptionDesc: function (optionDesc) {
			var optionType = this._parseType(optionDesc.type);

			if (optionDesc.uieditor) {
				return optionDesc.uieditor;
			}
			if (!$.isPlainObject(optionType)) {
				return optionEditorMapWij[optionType] || optionEditorMapWij["string"];
			}

		},
		//end collect uieditor

		//data handler
		_getDisplayTextByPropName: function (optionName, optionDesc) {
			return optionDesc["displayText"] ? optionName : optionDesc["displayText"];
		},

		_getDefaultValueByOptionName: function (optionName, optionDesc) {
			return optionDesc[optionName];
		},

		_getDefaultValueByOptionDesc: function (optionDesc) {
			return optionDesc ? optionDesc.defaultValue : null;
		},

		_createStringEditor: function () {
			return $("<input type ='text'/>");
		},

		_parseType: function (type) {
			return (!type || $.isPlainObject(type)) ? null : type.split("[")[0];
		},

		_parseNestedType: function (type) {
			if (!type) {
				return null; 
			}

			var parseType = type.split("[");
			if (parseType.length === 2) {
				return parseType[1].split("]")[0];
			}
		},

		_getOptionsDescByCategory: function (optionsDesc) {
			var categories = [],
				optionsDescByCategory = {},
				optionCategory;

			$.each(optionsDesc, function (key, optionDesc) {
				optionCategory = optionDesc.category;
				if (optionCategory && $.inArray(optionCategory, categories) === -1) {
					categories.push(optionCategory);
				}
			});

			categories.sort();
			$.each(categories, function (i, category) {
				optionsDescByCategory[category] = {};

			});

			$.each(optionsDesc, function (key, optionDesc) {
				optionCategory = optionDesc.category;
				if (optionCategory && $.inArray(optionCategory, categories) !== -1) {
					optionsDescByCategory[optionCategory][key] = optionDesc;
				}
			});
			return optionsDescByCategory;
		},

		_createIniValueFromOptsDesc: function (optionsDesc) {
			var value = {};
			//Note: how to handle null value
			$.each(optionsDesc, function (key, optionDesc) {
				if (optionDesc.defaultValue) {
					value[key] = optionDesc.defaultValue;
				}
			});
			return value;
		},

		_getSortedOptNames: function (optionsDesc) {
			var sortedNames = [];

			$.each(optionsDesc, function (key, optionDesc) {
				if (optionsDesc.hasOwnProperty(key)) {
					sortedNames.push(key);
				}
			});

			return sortedNames.sort();
		},
		//end data handler

		//event
		_valueChanged: function (event, data) {
			var self = this,
				name = data.name,
				parentValue = data.parentValue,
				value = data.value;

			if (parentValue) {
				if ($.isPlainObject(value)) {
					$.extend(true, parentValue[name], value);
				} else {
					parentValue[name] = value;
				}
				self._trigger("valueChanged", null, this.initValue);
			}
		},

		_changeToTreeWithCategory: function () {
			var self = this,
				ele = self.element;
			if (self.showCategory) {
				self.optionsTreesByCategory.detach();
				if (self.optionsTreeNoGroup) {
					ele.append(self.optionsTreeNoGroup.parent());
				} else {
					self._createOptionsNoCategory();
				}
				self.showCategory = false;
			}
		},

		_changeToTreeNoCategory: function () {
			var self = this,
				ele = self.element;

			if (!self.showCategory) {
				self.optionsTreeNoGroup.parent().detach();
				if (self.optionsTreesByCategory) {
					ele.append(self.optionsTreesByCategory);
				} else {
					self._createOptionsByCategory();
				}
				self.showCategory = true;
			}
		},
		//end event

		setValue: function (optionsValue, optionsDesc) {
			var self = this,
				o = self.options;

			self._destroy();
			self.optionsTreeNoGroup.remove();

			o.optionsDesc = optionsDesc;
			self.initValue = optionsValue ? optionsValue :
				self._createIniValueFromOptsDesc(optionsDesc);
			self._createOptionsNoCategory();
		},

		_destroy: function () {
			var self = this;

			$.each(optionEditors, function (key, value) {
				$.each(value, function (index, editorArg) {
					self._destroyWijUiEditor(editorArg);
				});
				optionEditors[key] = [];
			});
		},

		_destroyWijUiEditor: function (editorArg) {
			var editorName = this._getUiEditorByOptionDesc(editorArg.optionDesc);
			editorArg.editor[editorName]("destroy");
		},	

		destroy: function () {
			this.optionsTreeNoGroup.wijtree("destroy");
			$.Widget.prototype.destroy.apply(this);
		}
	});
} (jQuery));



//header
//btnFormat = "<button class='ui-button ui-widget ui-state-default " +
//			"ui-corner-all ui-button-text-icon-primary' >" +
//			"<span  class='ui-button-icon-primary ui-icon {icon}'></span>" +
//			"<span class='ui-button-text'>{text}</span></button>",
//sortBtnHtml = btnFormat.replace(/\{icon\}/, "ui-icon-arrow-1-n")
//.replace(/\{text\}/, "Alphabetical"),
//groupBtnHtml = btnFormat.replace(/\{icon\}/, "ui-icon-eject")
//.replace(/\{text\}/, "Gategorized"),

//_createHeader: function () {
//	var self = this,
//		ele = self.element,
//		header, sortBtn, groupBtn, sortBtnHtml, groupBtnHtml;
//
//	sortBtn = $(sortBtnHtml).bind("click", function () {
//		self._changeToTreeWithCategory();
//	});
//	groupBtn = $(groupBtnHtml).bind("click", function () {
//		self._changeToTreeNoCategory();
//	});
//	header = $("<div class= 'wijmo-wijppg-header'/>").append(sortBtn).append(groupBtn);
//	return header;
//},
//header
