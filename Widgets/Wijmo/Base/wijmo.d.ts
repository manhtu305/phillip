/// <reference path="../External/declarations/jquery.d.ts"/>
/// <reference path="../External/declarations/jquery.ui.d.ts"/>
/// <reference path="../External/declarations/Globalize.d.ts"/>

interface WijRange {
    start: number;
    end: number;
}

interface WijTextSelection extends WijRange {
    text: string;
}

interface JQueryWidgetFunction {
    (methodName: string, ...args: any[]);
    (options?): JQuery;
}

interface JQuery {
    wijAddVisibilityObserver(handler: Function, name: string);
    wijRemoveVisibilityObserver();

    wijtextselection(): WijTextSelection;
    wijtextselection(range: WijRange): WijTextSelection;
    wijtextselection(start: number, end?: number);

    wijContent(): any;
    wijContent(cnt: string): any;
    wijAddVisibilityObserver(h, namespace): any;
    wijRemoveVisibilityObserver(h): any;
    wijTriggerVisibility(): any;
    naNTest(num): number;
    leftBorderWidth(): number;
    rightBorderWidth(): number;
    topBorderWidth(): number;
    bottomBorderWidth(): number;
    borderSize(): { width: number; height: number; };
    setOutWidth(width): JQuery;
    setOutHeight(height): JQuery;
    getWidget(): any;
    wijshow(animation, customAnimations, customAnimationOptions, showing, shown): any;
    wijhide(animation, customAnimations, customAnimationOptions, hiding, hidden): any;
    uniqueId();
    removeUniqueId();
    wijFlip(setting: any);
    wijRevertFlip();
    hasAllClasses(className: string): boolean;
    removeData();
    attr(attributeName: string, value: boolean): JQuery;
    attribute(param, value?): JQuery;
    removeAttribute(param): JQuery;

    show(animated: any, options?: any, duration?: string, callback?: any): JQuery;
    hide(animated: any, options?: any, duration?: string, callback?: any): JQuery;
    hide(): JQuery;
    show(): JQuery;
    jqmData: any;
    zIndex(): number;
    mousewheel: any;
    closest(selector: string, context?: JQuery): JQuery;
    getBounds();
    setBounds(bounds);
    getMaxZIndex();
    bind(eventType: string, eventData?: any, handler?: (eventObject: JQueryEventObject, data: any) => any): JQuery;
    one(eventType: string, eventData?: any, handler?: (eventObject: JQueryEventObject, data: any) => any): JQuery;
}

interface JQueryStatic {
    wij;
    wijmo;
    save(element, set): JQuery;
    restore(element, set): JQuery;
    createWrapper(element): any;
    removeWrapper(element): any;
    setMode(el, mode): any;
    wijGetCulture(culture: string, cultureCalendar: string): any;
    expando: any;
    uaMatch: any;
    effects: any;
    browser: any;
    easing: any;
    data(element: Element, key: string): Object;
    map(array: any, callback: any): any;
}

declare module JQueryUI {
    interface UI {
        position?: any;
        plugin?: any;
        hasScroll? (el: HTMLElement, a: string): boolean;
    }
    interface KeyCode {
        INSERT: number;
        CAPSLOCK: number;
        CTRL: number;
        SHIFT: number;
        ALT: number;
    }
}

interface JQueryMobile {
    keyCode: any;
    widget: any;
    nsNormalize: Function;
    navigate: any;
}

interface HTMLElement {
    checked: any;
    contentWindow: any;
}

interface GlobalizeStatic {
    format(arguments: any): string;
    format(value: any, format: string, cultureSelector?: string): string;
    format(value: any, format: string, cultureSelector?: GlobalizeStatic): string;
    format(value: any, format: string, cultureSelector?: any): string;
    findClosestCulture(cultureSelector: string): GlobalizeCulture;
}

interface ParsedPath {
    domain: string;
    directory: string;
    hrefNoHash: string;
}

interface JQueryMobilePath {
    addSearchParams(url: string, data: any): string;
    isSameDomain(location, url): boolean;
    getFilePath(url: string): string;
}
