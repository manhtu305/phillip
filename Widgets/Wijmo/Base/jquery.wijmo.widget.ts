/// <reference path="../wijutil/jquery.wijmo.wijutil.ts"/>
/// <reference path="../wijutil/jquery.wijmo.wijtouchutil.ts"/>
/// <reference path="../External/declarations/jquery.d.ts"/>
/// <reference path="../External/declarations/jquery.ui.d.ts"/>
/// <reference path="../External/declarations/jquerymobile.d.ts"/>
/// <reference path="wijmo.d.ts"/>
/*
 * Depends:
 *  jquery.ui.widget.js
 *
 */


module wijmo {
    var $ = jQuery;
    class jQueryWijmo {
        static autoMobilize = true;
        //All CSS classes used in widgets that use jQuery UI CSS Framework
        static wijCSS: WijmoCSS = {
            widget: "ui-widget",
            overlay: "ui-widget-overlay",
            content: "ui-widget-content",
            header: "ui-widget-header",
            stateDisabled: "ui-state-disabled",
            stateFocus: "ui-state-focus",
            stateActive: "ui-state-active",
            stateDefault: "ui-state-default",
            stateHighlight: "ui-state-highlight",
            stateHover: "ui-state-hover",
            stateChecked: "ui-state-checked",
            stateError: "ui-state-error",
            getState: function (name) {
                name = name.charAt(0).toUpperCase() + name.substr(1);
                return $.wijmo.wijCSS["state" + name];
            },
            icon: "ui-icon",
            iconCheck: "ui-icon-check",
            iconRadioOn: "ui-icon-radio-on",
            iconRadioOff: "ui-icon-radio-off",
            iconClose: "ui-icon-close",
            iconArrow4Diag: "ui-icon-arrow-4-diag",
            iconNewWin: "ui-icon-newwin",
            iconVGripSolid: "ui-icon-grip-solid-vertical",
            iconHGripSolid: "ui-icon-grip-solid-horizontal",
            iconPlay: "ui-icon-play",
            iconPause: "ui-icon-pause",
            iconStop: "ui-icon-stop",
            iconArrowUp: "ui-icon-triangle-1-n",
            iconArrowRight: "ui-icon-triangle-1-e",
            iconArrowDown: "ui-icon-triangle-1-s",
            iconArrowLeft: "ui-icon-triangle-1-w",
            iconArrowRightDown: "ui-icon-triangle-1-se",
            iconArrowThickDown: "ui-icon-arrowthick-1-s",
            iconArrowThickUp: "ui-icon-arrowthick-1-n",
            iconCaratUp: "ui-icon-carat-1-n",
            iconCaratRight: "ui-icon-carat-1-e",
            iconCaratDown: "ui-icon-carat-1-s",
            iconCaratLeft: "ui-icon-carat-1-w",
            iconClock: "ui-icon-clock",
            iconPencil: "ui-icon-pencil",
            iconSeekFirst: "ui-icon-seek-first",
            iconSeekEnd: "ui-icon-seek-end",
            iconSeekNext: "ui-icon-seek-next",
            iconSeekPrev: "ui-icon-seek-prev",
            iconPrint: "ui-icon-print",
            iconDisk: "ui-icon-disk",
            iconSeekStart: "ui-icon-seek-start",
            iconFullScreen: "ui-icon-newwin",
            iconContinousView: "ui-icon-carat-2-n-s",
            iconZoomIn: "ui-icon-zoomin",
            iconZoomOut: "ui-icon-zoomout",
            iconBookmark: "ui-icon-bookmark",
            iconSearch: "ui-icon-search",
            iconImage: "ui-icon-image",
            iconArrowReturn: "ui-icon-arrowreturnthick-1-w",
            iconHome: "ui-icon-home",
            inputSpinnerLeft: "ui-input-spinner-left",
            inputSpinnerRight: "ui-input-spinner-right",
            inputTriggerLeft: "ui-input-trigger-left",
            inputTriggerRight: "ui-input-trigger-right",
            inputSpinnerTriggerLeft: "ui-input-spinner-trigger-left",
            inputSpinnerTriggerRight: "ui-input-spinner-trigger-right",
            cornerAll: "ui-corner-all",
            cornerLeft: "ui-corner-left",
            cornerRight: "ui-corner-right",
            cornerBottom: "ui-corner-bottom",
            cornerBL: "ui-corner-bl",
            cornerBR: "ui-corner-br",
            cornerTop: "ui-corner-top",
            cornerTL: "ui-corner-tl",
            cornerTR: "ui-corner-tr",
            helperClearFix: "ui-helper-clearfix",
            helperReset: "ui-helper-reset",
            helperHidden: "ui-helper-hidden",
            priorityPrimary: "ui-priority-primary",
            prioritySecondary: "ui-priority-secondary",
            button: "ui-button",
            buttonText: "ui-button-text",
            buttonTextOnly: "ui-button-text-only",
            tabs: "ui-tabs",
            tabsTop: "ui-tabs-top",
            tabsBottom: "ui-tabs-bottom",
            tabsLeft: "ui-tabs-left",
            tabsRight: "ui-tabs-right",
            tabsLoading: "ui-tabs-loading",
            tabsActive: "ui-tabs-active",
            tabsPanel: "ui-tabs-panel",
            tabsNav: "ui-tabs-nav",
            tabsHide: "ui-tabs-hide",
            tabsCollapsible: "ui-tabs-collapsible",
            activeMenuitem: "ui-active-menuitem"
        };
        //All CSS classes used in widgets that use Mobile theme framework
        static wijMobileCSS = {
            content: "ui-content",
            header: "ui-header",
            overlay: "ui-overlay",
            stateDisabled: "ui-state-disabled",
            stateFocus: "ui-focus",
            stateActive: "ui-btn-active",
            stateDefault: "ui-btn ui-btn-a",
            icon: "ui-icon ui-btn-icon-notext",
            iconArrowUp: "ui-icon-carat-u",
            iconArrowRight: "ui-icon-carat-r",
            iconArrowDown: "ui-icon-carat-d",
            iconArrowLeft: "ui-icon-carat-l",
            iconArrowRightDown: "ui-icon-carat-d",
            iconSeekFirst: "ui-icon-carat-l",
            iconSeekEnd: "ui-icon-carat-r",
            iconSeekNext: "ui-icon-carat-r",
            iconSeekPrev: "ui-icon-carat-l",
            iconClose: "ui-icon-delete",
            iconStop: "ui-icon-grid",
            iconCheck: "ui-icon-check",
            iconArrowReturn: "ui-icon-back"
        };

        static wijMobileThemePrefix = ["ui-bar", "ui-body", "ui-overlay", "ui-btn"];

        static registerWidget(name: string, baseType: any, def?: any, customizeInit?: Function) {
            var fullName = "wijmo." + name,
                init;
            if (typeof def === 'function') {
                init = def;
                def = null;
            }
            if (def === null || def === undefined) {
                def = $.extend(true, {}, baseType);
                baseType = $.wijmo.widget;
            }
            def.options = def.options || {};
            def.options.initSelector = def.options.initSelector ||
            ":jqmData(role='" + name + "')";
            def.initSelector = def.initSelector == null ? def.options.initSelector : def.initSelector + def.options.initSelector;

            if ($.mobile && def.options && def.options.wijMobileCSS) {
                def.options.wijCSS = def.options.wijCSS || {};
                $.extend(def.options.wijCSS, def.options.wijMobileCSS);
            }

            $.widget(fullName, baseType, def);

            if (init) {
                init.call();
            }
        }

        static addThemeToMobileCSS(theme, classes) {
            $.each(classes, function (key, cl) {
                if (typeof cl === "string") {
                    $.each(jQueryWijmo.wijMobileThemePrefix, function (idx, css) {
                        var regExp = new RegExp("\\b" + css);
                        if (regExp.test(cl)) {
                            classes[key] = cl + " " + css + "-" + theme;
                        }
                    });
                } else {
                    jQueryWijmo.addThemeToMobileCSS(theme, cl);
                }
            });
        }

    }
    $.wijmo = jQueryWijmo;

    // Declarations to support TypeScript type system
    export class JQueryUIWidget {
        _super: Function;
        _superApply: Function;

        widgetEventPrefix: string;
        widgetFullName: string;
        widgetName: string;
        namespace: string;
        options: any;
        element: JQuery;
        /** Removes the dialog functionality completely. This will return the element back to its pre-init state. */
        destroy() { }
        /** Disables the widget. */
        disable: Function;
        /** Enables the widget. */
        enable: Function;
        _trigger: Function;
        _setOption(name: string, value) { }
        /** Gets/sets the value currently associated with the specified optionName. */
        option: Function;
        _create() { }
        _init() { }
        /** Returns a jQuery object containing the original element or other relevant generated element. */
        widget() { return this.element; }
    }
    JQueryUIWidget.prototype.options = {
        wijCSS: $.wijmo.wijCSS
    };
    JQueryUIWidget.prototype.destroy = function () {
        $.Widget.prototype.destroy.apply(this, arguments);
    };
    JQueryUIWidget.prototype._setOption = function (name, value) {
        $.Widget.prototype._setOption.apply(this, arguments);
    };
    JQueryUIWidget.prototype._create = function () {
        $.Widget.prototype._create.apply(this, arguments);
    };
    JQueryUIWidget.prototype._init = function () {
        $.Widget.prototype._init.apply(this, arguments);
    };

    export class JQueryMobileWidget extends JQueryUIWidget {
        _getCreateOptions: Function;
    }

    //Fires a wijmoinit event on the document object for users to override default settings.
    //Use $(document).bind("wijmoinit", function() {//apply overrides here});
    //The event handler must be binded before jquery.wijmo.widget is loaded.
    $(window.document).trigger("wijmoinit");

    export interface WijmoCSS {
        getState?: Function;
        widget?: string;
        overlay?: string;
        content?: string;
        header?: string;
        stateDisabled?: string;
        stateFocus?: string;
        stateActive?: string;
        stateDefault?: string;
        stateHighlight?: string;
        stateHover?: string;
        stateChecked?: string;
        stateError?: string;
        icon?: string;
        iconCheck?: string;
        iconRadioOn?: string;
        iconRadioOff?: string;
        iconClose?: string;
        iconArrow4Diag?: string;
        iconNewWin?: string;
        iconVGripSolid?: string;
        iconHGripSolid?: string;
        iconPlay?: string;
        iconPause?: string;
        iconStop?: string;
        iconArrowUp?: string;
        iconArrowRight?: string;
        iconArrowDown?: string;
        iconArrowLeft?: string;
        iconArrowRightDown?: string;
        iconArrowThickDown?: string;
        iconArrowThickUp?: string;
        iconCaratUp?: string;
        iconCaratRight?: string;
        iconCaratDown?: string;
        iconCaratLeft?: string;
        iconClock?: string;
        iconPencil?: string;
        iconSeekFirst?: string;
        iconSeekEnd?: string;
        iconSeekNext?: string;
        iconSeekPrev?: string;
        iconPrint?: string;
        iconDisk?: string;
        iconSeekStart?: string;
        iconFullScreen?: string;
        iconContinousView?: string;
        iconZoomIn?: string;
        iconZoomOut?: string;
        iconBookmark?: string;
        iconSearch?: string;
        iconImage?: string;
        iconArrowReturn?: string;
        iconHome?: string;
        inputSpinnerLeft?: string;
        inputSpinnerRight?: string;
        inputTriggerLeft?: string;
        inputTriggerRight?: string;
        inputSpinnerTriggerLeft?: string;
        inputSpinnerTriggerRight?: string;
        cornerAll?: string;
        cornerLeft?: string;
        cornerRight?: string;
        cornerBottom?: string;
        cornerBL?: string;
        cornerBR?: string;
        cornerTop?: string;
        cornerTL?: string;
        cornerTR?: string;
        helperClearFix?: string;
        helperReset?: string;
        helperHidden?: string;
        priorityPrimary?: string;
        prioritySecondary?: string;
        button?: string;
        buttonText?: string;
        buttonTextOnly?: string;
        tabs?: string;
        tabsTop?: string;
        tabsBottom?: string;
        tabsLeft?: string;
        tabsRight?: string;
        tabsLoading?: string;
        tabsActive?: string;
        tabsPanel?: string;
        tabsNav?: string;
        tabsHide?: string;
        tabsCollapsible?: string;
        activeMenuitem?: string;
    }

    /** The options of base wijmo widget. */
    export interface WidgetOptions {
        /** Determines whether the widget is disabled. */
        disabled?: boolean;
        /** Determines the disabled state of the widget.
         * @ignore
         */
        disabledState?: boolean;
        /** @ignore */
        wijCSS?: WijmoCSS;
    }

    export class wijmoWidget extends JQueryMobileWidget {
        options: any;
        theme: string;
        _syncEventPrefix: boolean;
        _isMobile: boolean;
        initSelector: string;
        private _widgetCreated = false;
        _baseWidget() {
            return this._isMobile ? $.mobile.widget : $.Widget;
        }
        _createWidget(options, element) {
            this._widgetCreated = true;
            //Set widgetName to widgetEventPrefix for binding events like following,
            //$(element).bind(widgetName + eventName, function() {});
            if (this._syncEventPrefix) {
                this.widgetEventPrefix = this.widgetName;
            }
            // enable touch support:
            if (window.wijmoApplyWijTouchUtilEvents) {
                $ = window.wijmoApplyWijTouchUtilEvents($);
            }
            this._baseWidget().prototype._createWidget.apply(this, arguments);
        }
        _create() {
            this._baseWidget().prototype._create.apply(this, arguments);
            if (this._isDisabled()) {
                this._innerDisable();
            }
        }
        _init() {
            this._baseWidget().prototype._init.apply(this, arguments);
        }
        destroy() {
            this.element.unbind(".ui-disableSelection");
            this._baseWidget().prototype.destroy.apply(this, arguments);
        }
        _setOption(name, value) {
            var oldDisabled = this._isDisabled(), newDisabled;
            this._baseWidget().prototype._setOption.apply(this, arguments);

            // Modefy for the updating from jquery-ui.1.10 to jquery-ui.1.11
            if (name === "disabled") {
                newDisabled = this._isDisabled();
                if (oldDisabled === newDisabled) return;

                if (newDisabled) {
                    this._innerDisable();
                } else {
                    this._innerEnable();
                }
            }

            //Fixed an issue for jQuery mobile. when set the disabled option, the jQuery mobile set 
            // 'ui-state-disabled' css on the element.
            if (name === "disabled" && value && this._isMobile) {
                this.element.removeClass("ui-state-disabled").addClass(this.options.wijCSS.stateDisabled);
            }
        }

        _innerDisable() {
            this._toggleDisable();
        }

        _innerEnable() {
            this._toggleDisable();
        }

        _toggleDisable() {
            this.element.toggleClass("ui-state-disabled", !!this._isDisabled())
                .toggleClass(this.widgetFullName + "-disabled", !!this._isDisabled()) // jQueryUI add this class at _setOption() but not when _create()
                .attribute("aria-disabled", this._isDisabled());
        }

        _isDisabled() {
            var opts = this.options;
            return opts.disabledState === true || opts.disabled === true;
        }
    }
    wijmoWidget.prototype._syncEventPrefix = true;
    wijmoWidget.prototype._isMobile = false;

    export class wijmo_css implements WijmoCSS {
        getState = function (name) {
            name = name.charAt(0).toUpperCase() + name.substr(1);
            return this["state" + name];
        };

        widget: string = "ui-widget";
        overlay: string = "ui-widget-overlay";
        content: string = "ui-widget-content";
        header: string = "ui-widget-header";
        stateDisabled: string = "ui-state-disabled";
        stateFocus: string = "ui-state-focus";
        stateActive: string = "ui-state-active";
        stateDefault: string = "ui-state-default";
        stateHighlight: string = "ui-state-highlight";
        stateHover: string = "ui-state-hover";
        stateChecked: string = "ui-state-checked";
        stateError: string = "ui-state-error";
        icon: string = "ui-icon";
        iconCheck: string = "ui-icon-check";
        iconRadioOn: string = "ui-icon-radio-on";
        iconRadioOff: string = "ui-icon-radio-off";
        iconClose: string = "ui-icon-close";
        iconArrow4Diag: string = "ui-icon-arrow-4-diag";
        iconNewWin: string = "ui-icon-newwin";
        iconVGripSolid: string = "ui-icon-grip-solid-vertical";
        iconHGripSolid: string = "ui-icon-grip-solid-horizontal";
        iconPlay: string = "ui-icon-play";
        iconPause: string = "ui-icon-pause";
        iconStop: string = "ui-icon-stop";
        iconArrowUp: string = "ui-icon-triangle-1-n";
        iconArrowRight: string = "ui-icon-triangle-1-e";
        iconArrowDown: string = "ui-icon-triangle-1-s";
        iconArrowLeft: string = "ui-icon-triangle-1-w";
        iconArrowRightDown: string = "ui-icon-triangle-1-se";
        iconArrowThickDown: string = "ui-icon-arrowthick-1-s glyphicon glyphicon-arrow-down";
        iconArrowThickUp: string = "ui-icon-arrowthick-1-n glyphicon glyphicon-arrow-up";
        iconCaratUp: string = "ui-icon-carat-1-n";
        iconCaratRight: string = "ui-icon-carat-1-e";
        iconCaratDown: string = "ui-icon-carat-1-s";
        iconCaratLeft: string = "ui-icon-carat-1-w";
        iconClock: string = "ui-icon-clock glyphicon glyphicon-time";
        iconPencil: string = "ui-icon-pencil glyphicon glyphicon-pencil";
        iconSeekFirst: string = "ui-icon-seek-first";
        iconSeekEnd: string = "ui-icon-seek-end";
        iconSeekNext: string = "ui-icon-seek-next";
        iconSeekPrev: string = "ui-icon-seek-prev";
        iconPrint: string = "ui-icon-print";
        iconDisk: string = "ui-icon-disk";
        iconSeekStart: string = "ui-icon-seek-start";
        iconFullScreen: string = "ui-icon-newwin";
        iconContinousView: string = "ui-icon-carat-2-n-s";
        iconZoomIn: string = "ui-icon-zoomin";
        iconZoomOut: string = "ui-icon-zoomout";
        iconBookmark: string = "ui-icon-bookmark";
        iconSearch: string = "ui-icon-search";
        iconImage: string = "ui-icon-image";
        inputSpinnerLeft: string = "ui-input-spinner-left";
        inputSpinnerRight: string = "ui-input-spinner-right";
        inputTriggerLeft: string = "ui-input-trigger-left";
        inputTriggerRight: string = "ui-input-trigger-right";
        inputSpinnerTriggerLeft: string = "ui-input-spinner-trigger-left";
        inputSpinnerTriggerRight: string = "ui-input-spinner-trigger-right";
        cornerAll: string = "ui-corner-all";
        cornerLeft: string = "ui-corner-left";
        cornerRight: string = "ui-corner-right";
        cornerBottom: string = "ui-corner-bottom";
        cornerBL: string = "ui-corner-bl";
        cornerBR: string = "ui-corner-br";
        cornerTop: string = "ui-corner-top";
        cornerTL: string = "ui-corner-tl";
        cornerTR: string = "ui-corner-tr";
        helperClearFix: string = "ui-helper-clearfix";
        helperReset: string = "ui-helper-reset";
        helperHidden: string = "ui-helper-hidden";
        priorityPrimary: string = "ui-priority-primary";
        prioritySecondary: string = "ui-priority-secondary";
        button: string = "ui-button";
        buttonText: string = "ui-button-text";
        buttonTextOnly: string = "ui-button-text-only";
        tabs: string = "ui-tabs";
        tabsTop: string = "ui-tabs-top";
        tabsBottom: string = "ui-tabs-bottom";
        tabsLeft: string = "ui-tabs-left";
        tabsRight: string = "ui-tabs-right";
        tabsLoading: string = "ui-tabs-loading";
        tabsActive: string = "ui-tabs-active";
        tabsPanel: string = "ui-tabs-panel";
        tabsNav: string = "ui-tabs-nav";
        tabsHide: string = "ui-tabs-hide";
        tabsCollapsible: string = "ui-tabs-collapsible";
        activeMenuitem: string = "ui-active-menuitem";
    }

    //Check if jQuery Mobile is on the page and make sure autoMobilize is set to true (so that this default behavior can be turned off)
    if ($.mobile != null && $.wijmo.autoMobilize === true) {
        //Set mobile CSS classes to work with jQuery Mobile CSS Framework
        //wijmoWidget.options.wijCSS = $.wijmo.wijMobileCSS;
        $.extend(true, wijmoWidget.prototype.options.wijCSS, $.wijmo.wijMobileCSS);

        wijmoWidget.prototype._isMobile = true;
        wijmoWidget.prototype.options = $.extend(true, {}, wijmoWidget.prototype.options,
            wijmoWidget.prototype._baseWidget().prototype.options);

        wijmoWidget.prototype._getCreateOptions = function () {
            var ele = this.element,
                baseOptions,
                optionsParser = optionsParser = function (value) {
                    // Add quotes to key pair.
                    if (typeof value === 'undefined') {
                        return {};
                    } else if (value === null) {
                        return {};
                    }

                    var reg = /(?:(?:\{[\n\r\t\s]*(.+?)\s*\:[\n\r\t\s]*)|(?:,[\n\r\t\s]*(.+?)\s*\:[\n\r\t\s]*))('(.*?[^\\])')?/gi,
                        arrReg = /\[.*?(?=[\]\[])|[\]\[].*?(?=[\]])/gi,
                        str = value.replace(reg, function (i, str1, str2, str3) {
                            var result,
                                reg1 = /[\n\r\t\s]*['"]?([^\{,\s]+?)['"]?\s*:[\n\r\t\s]*/i,
                                reg2 = /\:[\n\r\t\s]*(?:'(.*)')?/i;

                            result = i.replace(reg1, "\"$1\":");
                            if (str3) {
                                return result.replace(reg2, ":\"$1\"");
                            }
                            return result;
                        }).replace(arrReg, function (i) {
                                var reg1 = /'(.*?[^\\])'/g;
                                return i.replace(reg1, "\"$1\"");
                            });

                    return $.parseJSON(str);
                },
                options = optionsParser(ele.attr("data-" + $.mobile.nsNormalize("options"))),
                wijCSS;

            baseOptions = $.mobile.widget.prototype._getCreateOptions.apply(this, arguments);

            //add theme support in mobile mode
            wijCSS = $.extend(true, {}, this.options.wijCSS);
            this.theme = this.options.theme !== undefined ?
            this.options.theme : this.element.jqmData("theme");
            if (this.theme) {
                $.wijmo.addThemeToMobileCSS(this.theme, wijCSS);
            }

            return $.extend(baseOptions, { wijCSS: wijCSS }, options);
        }

        $.widget("wijmo.widget", $.mobile.widget, wijmoWidget.prototype);
        $(document).on("pageshow", function (event, ui) {
            if (event.target == null)
                return;
            var page = $(event.target);
            if (page.wijTriggerVisibility) {
                page.wijTriggerVisibility();
            }
        });
    } else {
        wijmoWidget.prototype.options = $.extend(true, {}, wijmoWidget.prototype.options,
            wijmoWidget.prototype._baseWidget().prototype.options);
        //jQuery Mobile either does not exist or the autoMobilize flag has been turned off.
        $.widget("wijmo.widget", wijmoWidget.prototype);
    }
}