﻿/// <reference path="../Base/jquery.wijmo.widget.ts" />
/// <reference path="../wijsuperpanel/jquery.wijmo.wijsuperpanel.ts" />
/// <reference path="../wijpopup/jquery.wijmo.wijpopup.ts" />
/// <reference path="../External/declarations/jquery.cookie.d.ts" />

/*
 * Depends:
 *  jquery-1.4.2.js
 *  jquery.ui.core.js
 *  jquery.ui.widget.js
 *  jquery.ui.position.js
 *  jquery.effects.core.js	
 *  jquery.cookie.js
 *  jquery.wijmo.wijsuperpanel.js
 *  jquery.wijmo.wijutil.js
 *
 */

module wijmo.wizard {

	var $ = jQuery,
		configStr: ConfigDataStr = {
			WIJWIZARD: "wijwizard",
			LOADWIJWIZARD: "load.wijwizard",
			CACHEWIJWIZARD: "cache.wijwizard",
			SPINNERWIJWIZARD: "spinner.wijwizard",
			INTIDWIZARD: "intId.wijwizard",
			DESTROYWIJWIZARD: "destroy.wijwizard"
		},
		defaults: WijwizardDefault = {
			stepHeaderTemplate: '<li><h1>#{title}</h1>#{desc}</li>',
			panelTemplate: '<div></div>',
			spinner: '<em>Loading&#8230;</em>'
		};

	/** @widget*/
	export class wijwizard extends wijmoWidget {
		private _defaults: WijwizardDefault;
		private _configStr: ConfigDataStr;
		private _isPlaying: boolean;
		list: JQuery;
		lis: JQuery;
		panels: JQuery;
		container: JQuery;
		scrollWrap: JQuery;
		cookie: any;
		buttons: JQuery;
		backBtn: JQuery;
		nextBtn: JQuery;
		xhr: JQueryXHR;

		_create() {
			this._defaults = defaults;
			this._configStr = configStr;
			this._isPlaying = false;
			if (this.element.is(":hidden") && this.element.wijAddVisibilityObserver) {
				this.element.wijAddVisibilityObserver(() => {
					if (this.element.wijRemoveVisibilityObserver) {
						this.element.wijRemoveVisibilityObserver();
					}
					this._pageLize(false);
				}, this._configStr.WIJWIZARD);
				//return;
			}
			this._pageLize(true);

			super._create();
		}

		_init() {
			var o: WijWizardOptions = this.options;
			if (!this._isDisabled() && o.autoPlay) {
				this.play();
			}
		}


		_setOption(key: string, value: any) {
			super._setOption(key, value);

			switch (key) {
				case 'activeIndex':
					this.show(value);
					break;
				case 'navButtons':
				case 'backBtnText':
				case 'nextBtnText':
					this._createButtons();
					break;
				case 'delay':
					if (this._isPlaying) {
						this.stop();
						this.play();
					}
					break;
				case 'autoPlay':
					if (value === true) {
						this.play();
					} else {
						this.stop();
					}
				default:
					this._pageLize(false);
					break;
			}
		}

		/** Removes the wijwizard functionality completely.
		  * This returns the element back to its pre-init state.
		  */
		destroy() {
			var o: WijWizardOptions = this.options, self: wijwizard = this,
				elementCls: string = [o.wijCSS.wijwizard, o.wijCSS.widget, o.wijCSS.helperClearFix].join(" "),
				listCls: string = [o.wijCSS.widget, o.wijCSS.helperReset, o.wijCSS.wijwizardSteps, o.wijCSS.helperClearFix].join(" "),
				lisCls: string = [o.wijCSS.header, o.wijCSS.cornerAll, o.wijCSS.priorityPrimary, o.wijCSS.prioritySecondary].join(" "),
				panelsCls: string = [o.wijCSS.stateDefault, o.wijCSS.wijwizardActived, o.wijCSS.stateActive, o.wijCSS.stateHover, o.wijCSS.stateFocus, o.wijCSS.stateDisabled, o.wijCSS.wijwizardPanel, o.wijCSS.content, o.wijCSS.wijwizardHide].join(" ");

			this.abort();
			this.stop();
			this._removeScroller();
			this._removeButtons();
			this.element.unbind('.wijwizard')
				.removeClass(elementCls)
				.removeData(this._configStr.WIJWIZARD);

			if (this.list) {
				this.list.removeClass(listCls).removeAttr('role');
			}

			if (this.lis) {
				this.lis.removeClass(lisCls).removeAttr('role');
				$.each(this.lis, function () {
					if ($.data(this, self._configStr.DESTROYWIJWIZARD)) {
						$(this).remove();
					} else {
						$(this).removeAttribute('aria-selected');
					}
				});
			}

			$.each(this.panels, function () {
				var $this: JQuery = $(this).unbind('.wijwizard');
				$this.removeAttr("role");
				$.each(['load', 'cache'], (i, prefix) => {
					$this.removeData(prefix + '.wijwizard');
				});

				if ($.data(this, self._configStr.DESTROYWIJWIZARD)) {
					$this.remove();
				} else {
					$this.removeClass(panelsCls)
						.css({ position: '', left: '', top: '' })
						.removeAttribute('aria-hidden');
				}
			});

			this.container.replaceWith(this.container.contents());

			if (o.cookie) {
				this._cookie(null, o.cookie);
			}

			super.destroy();
		}


		private _pageLize(init: boolean) {
			var o = this.options,
				self = this,
				fragmentId: RegExp = /^#.+/; // Safari 2 reports '#' for an empty hash;

			//Fix a bug that when no title and has ul li element in its content
			this.list = this.element.children('ol,ul').eq(0);
			if (this.list && this.list.length === 0) {
				this.list = this.element.find("." + o.wijCSS.wijwizardSteps).eq(0);
				if (this.list && this.list.length === 0) {
					this.list = null;
				}
			}
			if (this.list) {
				this.lis = $('li', this.list);
			}

			if (init) {
				this.panels = $('> div', this.element);

				$.each(this.panels, (i, p) => {
					var url = $(p).attr('src');
					// inline
					if (url && !fragmentId.test(url)) {
						// mutable data
						$.data(p, self._configStr.LOADWIJWIZARD, url.replace(/#.*$/, ''));
					}
				});

				var elementCls: string = [o.wijCSS.wijwizard, o.wijCSS.widget, o.wijCSS.helperClearFix].join(" "),
					listCls: string = [o.wijCSS.widget, o.wijCSS.helperReset, o.wijCSS.wijwizardSteps, o.wijCSS.helperClearFix].join(" "),
					lisCls: string = [o.wijCSS.header, o.wijCSS.cornerAll].join(" "),
					containCls: string = [o.wijCSS.wijwizardContent, o.wijCSS.widget, o.wijCSS.content, o.wijCSS.cornerAll].join(" "),
					panelCls: string = [o.wijCSS.wijwizardPanel, o.wijCSS.content].join(" ");
				this.element.addClass(elementCls);
				if (this.list) {
					this.list.addClass(listCls)
						.attr("role", "tablist");
					this.lis.addClass(lisCls)
						.attr("role", "tab");
				}
				this.container = $('<div/>');
				this.container.addClass(containCls);
				this.container.append(this.panels);
				this.container.appendTo(this.element);
				this.panels.addClass(panelCls)
					.attr("role", "tabpanel");

				// Active a panel
				// use "activeIndex" option or try to retrieve:
				// 1. from cookie
				// 2. from actived class attribute on panel
				if (o.activeIndex === undefined) {
					if (typeof o.activeIndex !== 'number' && o.cookie) {
						o.activeIndex = parseInt(this._cookie(undefined, undefined), 10);
					}
					if (typeof o.activeIndex !== 'number' &&
						this.panels.filter('.' + o.wijCSS.wijwizardActived).length) {
						o.activeIndex = this.panels
							.index(this.panels.filter('.' + o.wijCSS.wijwizardActived));
					}
					o.activeIndex = o.activeIndex || (this.panels.length ? 0 : -1);
				} else if (o.activeIndex === null) {
					// usage of null is deprecated, TODO remove in next release
					o.activeIndex = -1;
				}

				// sanity check - default to first page...
				o.activeIndex = ((o.activeIndex >= 0 && this.panels[o.activeIndex]) ||
				o.activeIndex < 0) ? o.activeIndex : 0;

				this.panels.addClass(o.wijCSS.wijwizardHide).attribute('aria-hidden', true);
				if (o.activeIndex >= 0 && this.panels.length) {
					// check for length avoids error when initializing empty pages
					this.panels.eq(o.activeIndex).removeClass(o.wijCSS.wijwizardHide)
						.addClass(o.wijCSS.wijwizardActived).attribute('aria-hidden', false);
					this.load(o.activeIndex);
				}

				this._createButtons();
			} else {
				this.panels = $('> div', this.container);
				o.activeIndex = this.panels
					.index(this.panels.filter('.' + o.wijCSS.wijwizardActived));
			}
			this._addScrollForContent();

			this._refreshStep();
			this._initScroller();

			// set or update cookie after init and add/remove respectively
			if (o.cookie) {
				this._cookie(o.activeIndex, o.cookie);
			}

			// reset cache if switching from cached to not cached
			if (o.cache === false) {
				this.panels.removeData(this._configStr.CACHEWIJWIZARD);
			}

			if (o.showOption === undefined || o.showOption === null) {
				o.showOption = {};
			}
			this._normalizeBlindOption(o.showOption);

			if (o.hideOption === undefined || o.hideOption === null) {
				o.hideOption = {};
			}
			this._normalizeBlindOption(o.hideOption);

			// remove all handlers
			this.panels.unbind('.wijwizard');
		}

		private _removeButtons() {
			if (this.backBtn) {
				this.backBtn.unbind(".wijwizard");
			}
			if (this.nextBtn) {
				this.nextBtn.unbind(".wijwizard");
			}
			if (this.buttons) {
				this.buttons.remove();
				this.buttons = undefined;
			}
		}

		private _createButtons() {
			var self: wijwizard = this, o: WijWizardOptions = this.options, bt: string,
				backBtnText: string = o.backBtnText,
				nextBtnText: string = o.nextBtnText,
				commonCls: string = [o.wijCSS.widget, o.wijCSS.stateDefault, o.wijCSS.cornerAll, o.wijCSS.button, o.wijCSS.buttonTextOnly].join(" "),
				backBtCls: string = [o.wijCSS.wijwizardPrev, o.wijCSS.stateDefault, o.wijCSS.cornerRight].join(" "),
				nextBtlCls: string = [o.wijCSS.wijwizardNext, o.wijCSS.stateDefault, o.wijCSS.cornerLeft].join(" ");

			this._removeButtons();
			if (o.navButtons === 'none') {
				return;
			}

			if (!this.buttons) {
				bt = o.navButtons;
				if (bt === 'auto') {
					bt = this.list ? 'common' : 'edge';
				}

				this.buttons = $('<div/>');
				this.buttons.addClass(o.wijCSS.wijwizardButtons);

				if (bt === 'common') {
					this.backBtn = $("<a href='#'/>")
						.addClass(commonCls)
						.append("<span class='" + o.wijCSS.buttonText + "'>" + backBtnText + "</span>")
						.appendTo(this.buttons)
						.attr("role", "button");

					this.nextBtn = $("<a href='#'/>")
						.addClass(commonCls)
						.append("<span class='" + o.wijCSS.buttonText + "'>" + nextBtnText + "</span>")
						.appendTo(this.buttons)
						.attr("role", "button");
				} else {
					this.backBtn = $("<a href='#'/>")
						.addClass(backBtCls)
						.append("<span class='" + o.wijCSS.icon + " " + o.wijCSS.iconArrowLeft + "'></span>")
						.appendTo(this.buttons)
						.attr("role", "button");

					this.nextBtn = $("<a href='#'/>")
						.addClass(nextBtlCls)
						.append("<span class='" + o.wijCSS.icon + " " + o.wijCSS.iconArrowRight + "'></span>")
						.appendTo(this.buttons)
						.attr("role", "button");
				}

				this.buttons.appendTo(this.element);
			}

			this._setupEvent();
		}

		private _setupEvent(backBtn?: JQuery, nextBtn?: JQuery) {
			var self: wijwizard = this, o: WijWizardOptions = this.options,
				currentBackBtn: JQuery = backBtn || this.backBtn,
				currentNextBtn: JQuery = nextBtn || this.nextBtn;

			if (!currentBackBtn || !currentNextBtn) {
				return;
			}
			currentBackBtn.bind({
				'click.wijwizard': function () {
					self.back();
					return false;
				},
				'mouseover.wijwizard': self._eventHandler().addState(o.wijCSS.stateHover, currentBackBtn),
				'mouseout.wijwizard': self._eventHandler().removeState(o.wijCSS.stateHover, currentBackBtn),
				'mousedown.wijwizard': self._eventHandler().addState(o.wijCSS.stateActive, currentBackBtn),
				'mouseup.wijwizard': self._eventHandler().removeState(o.wijCSS.stateActive, currentBackBtn)
			});
			currentNextBtn.bind({
				'click.wijwizard': function () {
					self.next();
					return false;
				},
				'mouseover.wijwizard': self._eventHandler().addState(o.wijCSS.stateHover, currentNextBtn),
				'mouseout.wijwizard': self._eventHandler().removeState(o.wijCSS.stateHover, currentNextBtn),
				'mousedown.wijwizard': self._eventHandler().addState(o.wijCSS.stateActive, currentNextBtn),
				'mouseup.wijwizard': self._eventHandler().removeState(o.wijCSS.stateActive, currentNextBtn)
			});
		}

		private _eventHandler(): stateHandler {
			var self: wijwizard = this, o: WijWizardOptions = this.options,
				addState: (string, JQuery) => () => void = (state, el) => {
					return () => {
						if (self._isDisabled()) {
							return;
						}
						if (el.is(':not(.' + o.wijCSS.stateDisabled + ')')) {
							el.addClass(state);
						}
					};
				},
				removeState: (string, JQuery) => () => void = (state, el) => {
					return () => {
						if (self._isDisabled()) {
							return;
						}
						el.removeClass(state);
					};
				};

			return {
				addState: addState,
				removeState: removeState
			};
		}

		private _refreshStep() {
			var o: WijWizardOptions = this.options;

			if (this.lis) {
				this.lis.removeClass(o.wijCSS.priorityPrimary)
					.addClass(o.wijCSS.prioritySecondary).attribute('aria-selected', false);
				if (o.activeIndex >= 0 && o.activeIndex <= this.lis.length - 1) {
					if (this.lis) {
						this.lis.eq(o.activeIndex).removeClass(o.wijCSS.prioritySecondary)
							.addClass(o.wijCSS.priorityPrimary)
							.attribute('aria-selected', true);
					}
					if (this.scrollWrap) {
						this.scrollWrap.wijsuperpanel('scrollChildIntoView',
							this.lis.eq(o.activeIndex));
					}
				}
			}

			if (this.buttons && !o.loop) {
				this.backBtn[o.activeIndex <= 0 ? 'addClass' : 'removeClass'](o.wijCSS.stateDisabled)
					.attribute('aria-disabled', o.activeIndex === 0);
				this.nextBtn[o.activeIndex >= this.panels.length - 1 ? 'addClass' : 'removeClass'](o.wijCSS.stateDisabled)
					.attribute('aria-disabled', (o.activeIndex >= this.panels.length - 1));
			}
		}

		private _initScroller() {
			if (!this.lis || !this.element.is(":visible")) {
				return;
			}

			var width: number = 0;
			$.each(this.lis, function () {
				width += $(this).outerWidth(true);
			});

			if (this.element.innerWidth() < width) {
				if (this.scrollWrap === undefined) {
					this.list.wrap("<div class='scrollWrap'></div>");
					this.scrollWrap = this.list.parent();
					if ($.effects && $.effects.save) {
						$.effects.save(this.list, ['width', 'height', 'overflow']);
					} else if ($.save) {
						$.save(this.list, ['width', 'height', 'overflow']);
					}
				}

				this.list.width(width + 8);
				this.scrollWrap.height(this.list.outerHeight(true));

				this.scrollWrap.wijsuperpanel({
					allowResize: false,
					hScroller: {
						scrollBarVisibility: 'hidden'
					},
					vScroller: {
						scrollBarVisibility: 'hidden'
					}
				});

			} else {
				this._removeScroller();
			}
		}

		private _removeScroller() {
			if (this.scrollWrap) {
				this.scrollWrap.wijsuperpanel('destroy')
					.replaceWith(this.scrollWrap.contents());
				this.scrollWrap = undefined;
				if ($.effects && $.effects.restore) {
					$.effects.restore(this.list, ['width', 'height', 'overflow']);
				} else if ($.restore) {
					$.restore(this.list, ['width', 'height', 'overflow']);
				}
			}
		}

		private _cookie(index: number, c: any) {
			var cookie: any = this.cookie || (this.cookie = this.options.cookie.name);
			return $.cookie.apply(null, [cookie].concat($.makeArray(arguments)));
		}

		private _normalizeBlindOption(o: ShowOption) {
			if (o.blind === undefined) {
				o.blind = false;
			}
			if (o.fade === undefined) {
				o.fade = false;
			}
			if (o.duration === undefined) {
				o.duration = 200;
			}
			if (typeof o.duration === 'string') {
				try {
					o.duration = parseInt(o.duration, 10);
				}
				catch (e) {
					o.duration = 200;
				}
			}
		}

		private _ui(panel: Element): ShowPanelIndex {
			return {
				panel: panel,
				index: this.panels.index(panel)
			};
		}

		private _removeSpinner() {
			// restore all former loading wijwizard labels
			this.element.removeClass(this.options.wijCSS.tabsLoading);
			var spinner = this.element.data(this._configStr.SPINNERWIJWIZARD);
			if (spinner) {
				this.element.removeData(this._configStr.SPINNERWIJWIZARD);
				spinner.remove();
			}
		}

		private _showPanel(p: JQuery) {
			var o: WijWizardOptions = this.options,
				$show: JQuery = p,
				props: any;

			$show.addClass(o.wijCSS.wijwizardActived);
			if ((o.showOption.blind || o.showOption.fade) && o.showOption.duration > 0) {
				props = { duration: o.showOption.duration };
				if (o.showOption.blind) {
					props.height = 'toggle';
				}
				if (o.showOption.fade) {
					props.opacity = 'toggle';
				}
				$show.hide().removeClass(o.wijCSS.wijwizardHide)// avoid flicker that way
					.animate(props, o.showOption.duration || 'normal', "linear", () => {
						this._resetStyle($show);
						if ($show.wijTriggerVisibility) {
							$show.wijTriggerVisibility();
						}
						this._trigger('show', null, this._ui($show[0]));
						this._removeSpinner();
						$show.attribute('aria-hidden', false);
						this._trigger('activeIndexChanged', null, this._ui($show[0]));
					});
			} else {
				$show.removeClass(o.wijCSS.wijwizardHide).attribute('aria-hidden', false);
				if ($show.wijTriggerVisibility) {
					$show.wijTriggerVisibility();
				}
				this._trigger('show', null, this._ui($show[0]));
				this._removeSpinner();
				this._trigger('activeIndexChanged', null, this._ui($show[0]));
			}
		}

		private _hidePanel(p: JQuery) {
			var self: wijwizard = this, o: WijWizardOptions = this.options, $hide: JQuery = p, props: any;

			$hide.removeClass(o.wijCSS.wijwizardActived);
			if ((o.hideOption.blind || o.hideOption.fade) && o.hideOption.duration > 0) {
				props = { duration: o.hideOption.duration };
				if (o.hideOption.blind) {
					props.height = 'toggle';
				}
				if (o.hideOption.fade) {
					props.opacity = 'toggle';
				}
				$hide.animate(props, o.hideOption.duration || 'normal', "linear", () => {
					$hide.addClass(o.wijCSS.wijwizardHide).attribute('aria-hidden', true);
					this._resetStyle($hide);
					this.element.dequeue(this._configStr.WIJWIZARD);
				});
			} else {
				$hide.addClass(o.wijCSS.wijwizardHide).attribute('aria-hidden', true);
				this.element.dequeue(this._configStr.WIJWIZARD);
			}
		}

		// Reset certain styles left over from animation
		// and prevent IE's ClearType bug...
		private _resetStyle($el: JQuery) {
			$el.css({ display: '' });

			if (!$.support.opacity) {
				$el[0].style.removeAttribute('filter');
			}
		}

		private _addScrollForContent() {
			var self: wijwizard = this,
				contentHeight: number = self.element.height();

			if (!this.element.is(":visible")) {
				return;
			}

			// fix the issue 42962, if there is no buttons or pageer list in the wizard, it will throw exception. 
			if (self.buttons) {
				contentHeight -= self.buttons.outerHeight(true);
			}
			if (self.list) {
				contentHeight -= self.list.outerHeight(true);
			}
			contentHeight -= (self.container.outerHeight(true) - self.container.innerHeight()) - (self.container.innerHeight() - self.container.height());

			if (contentHeight < self.container.height()) {
				self.container.height(contentHeight);
			}

			self.container.css("overflow", "auto");
		}

		/** The add method adds a new panel.
		  * @param {number} index Zero-based position where to insert the new panel.
		  * @param {string} title The step title.
		  * @param {string} desc The step description.
		  * @example 
		  * // Add a new panel to be the second step.
		  * // It's title is "New Panel", description is "New Panel Description".
		  * $("#wizard").wijwizard("add", 1, "New Panel", "New Panel Description");
		  */
		add(index?: number, title?: string, desc?: string): wijwizard {
			if (index === undefined) {
				index = this.panels.length; // append by default
			}

			if (title === undefined) {
				title = "Step " + index;
			}

			var self: wijwizard = this, o: WijWizardOptions = this.options,
				$panel: JQuery = $(o.panelTemplate || self._defaults.panelTemplate)
					.data(this._configStr.DESTROYWIJWIZARD, true),
				panelCls: string = [o.wijCSS.wijwizardPanel, o.wijCSS.content, o.wijCSS.cornerAll, o.wijCSS.wijwizardHide].join(" "),
				liCls: string = [o.wijCSS.header, o.wijCSS.cornerAll, o.wijCSS.prioritySecondary].join(" "),
				$li: JQuery;

			$panel.addClass(panelCls)
				.attribute('aria-hidden', true);

			if (index >= this.panels.length) {
				if (this.panels.length > 0) {
					$panel.insertAfter(this.panels[this.panels.length - 1]);
				} else {
					$panel.appendTo(this.container);
				}
			} else {
				$panel.insertBefore(this.panels[index]);
			}

			if (this.list && this.lis) {
				$li = $((o.stepHeaderTemplate || self._defaults.stepHeaderTemplate)
					.replace(/#\{title\}/g, title).replace(/#\{desc\}/g, desc));
				$li.addClass(liCls)
					.data(this._configStr.DESTROYWIJWIZARD, true);

				if (index >= this.lis.length) {
					$li.appendTo(this.list);
				} else {
					$li.insertBefore(this.lis[index]);
				}
			}

			this._pageLize(false);

			if (this.panels.length === 1) { // after pagelize
				o.activeIndex = 0;
				$li.addClass(o.wijCSS.priorityPrimary);
				$panel.removeClass(o.wijCSS.wijwizardHide)
					.addClass(o.wijCSS.wijwizardActived)
					.attribute('aria-hidden', false);
				this.element.queue(this._configStr.WIJWIZARD, function () {
					self._trigger('show', null, self._ui(self.panels[0]));
				});

				this._refreshStep();
				this.load(0);
			}

			// callback
			this._trigger('add', null, this._ui(this.panels[index]));
			return this;
		}

		/** The remove method removes a panel.
		  * @param {number} index The zero-based index of the panel to be removed.
		  * @example 
		  * // Remove the second step.
		  * $("#wizard").wijwizard("remove", 1);
		  */
		remove(index: number): wijwizard {
			var o: WijWizardOptions = this.options,
				//$li = this.lis.eq(index).remove(),
				$panel: JQuery = this.panels.eq(index).remove();

			this.lis.eq(index).remove();
			if (index < o.activeIndex) {
				o.activeIndex--;
			}

			this._pageLize(false);

			//Ajust the active panel index in some case
			if ($panel.hasClass(o.wijCSS.wijwizardActived) && this.panels.length >= 1) {
				this.show(index + (index < this.panels.length ? 0 : -1));
			}

			// callback
			this._trigger('remove', null, this._ui($panel[0]));
			return this;
		}

		/** The show method selects an active panel and displays the panel at a specified position.
		  * @param {number} index The zero-based index of the panel to be actived.
		  * @example 
		  * // Show the second step.
		  * $("#wizard").wijwizard("show", 1);
		  */
		show(index: number): wijwizard {
			if (index < 0 || index >= this.panels.length) {
				return this;
			}

			// previous animation is still processing
			if (this.element.queue(this._configStr.WIJWIZARD).length > 0) {
				return this;
			}

			var o: WijWizardOptions = this.options,
				args = {
					nextIndex: 0,
					nextPanel: null
				},
				$hide: JQuery,
				$show: JQuery;

			$.extend(args, this._ui(this.panels[o.activeIndex]));
			args.nextIndex = index;
			args.nextPanel = this.panels[index];
			if (this._trigger('validating', null, args) === false) {
				return this;
			}

			$hide = this.panels.filter(':not(.' + o.wijCSS.wijwizardHide + ')');
			$show = this.panels.eq(index);
			o.activeIndex = index;

			this.abort();

			if (o.cookie) {
				this._cookie(o.activeIndex, o.cookie);
			}

			this._refreshStep();
			// show new panel
			if ($show.length) {
				if ($hide.length) {
					this.element.queue(this._configStr.WIJWIZARD, () => {
						this._hidePanel($hide);
					});
				}

				this.element.queue(this._configStr.WIJWIZARD, () => {
					this._showPanel($show);
				});

				this.load(index);
			}
			else {
				throw 'jQuery UI wijwizard: Mismatching fragment identifier.';
			}

			return this;
		}

		/** The next method moves to the next panel. */
		next(): boolean {
			var o: WijWizardOptions = this.options,
				index: number = o.activeIndex + 1;

			if (this._isDisabled()) {
				return false;
			}
			if (o.loop) {
				index = index % this.panels.length;
			}

			if (index < this.panels.length) {
				this.show(index);
				return true;
			}
			return false;
		}

		/** The back method moves to the previous panel. */
		back(): boolean {
			var o: WijWizardOptions = this.options,
				index: number = o.activeIndex - 1;

			if (this._isDisabled()) {
				return false;
			}
			if (o.loop) {
				index = index < 0 ? this.panels.length - 1 : index;
			}

			if (index >= 0) {
				this.show(index);
				return true;
			}
			return false;
		}

		_popupSpinner(): void {
			var self: wijwizard = this, ele = self.element,
				o: WijWizardOptions = self.options, spinner: JQuery;
			// load remote from here on
			ele.addClass(o.wijCSS.tabsLoading);
			if (!o.spinner) {
				return;
			}
			spinner = ele.data(self._configStr.SPINNERWIJWIZARD);
			if (!spinner) {
				spinner = $('<div/>');
				spinner.addClass(o.wijCSS.wijwizardSpinner);
				spinner.html(o.spinner || self._defaults.spinner);
				spinner.appendTo(document.body);
				ele.data(self._configStr.SPINNERWIJWIZARD, spinner);
				spinner.wijpopup({
					showEffect: 'blind',
					hideEffect: 'blind'
				});
			}

			spinner.wijpopup('show', {
				of: ele,
				my: 'center center',
				at: 'center center'
			});
		}

		/** The load method reload the content of an Ajax panel programmatically.
		  * @param {number} index The zero-based index of the panel to be loaded.
		  * @example 
		  * // Reload the content of second step.
		  * $("#wizard").wijwizard("load", 1);
		  */
		load(index: number): wijwizard {
			var self: wijwizard = this, o: WijWizardOptions = self.options,
				p: Element = self.panels.eq(index)[0], ele = self.element,
				url: string = <string>$.data(p, self._configStr.LOADWIJWIZARD);

			self.abort();

			// not remote or from cache
			if (!url || ele.queue(self._configStr.WIJWIZARD).length !== 0 &&
				$.data(p, self._configStr.CACHEWIJWIZARD)) {
				ele.dequeue(self._configStr.WIJWIZARD);
				return self;
			}

			self._popupSpinner();

			self.xhr = $.ajax($.extend({}, o.ajaxOptions, {
				url: url,
				dataType: 'html',
				success: function (r, s) {
					$(p).html(r);

					if (o.cache) {
						// if loaded once do not load them again
						$.data(p, self._configStr.CACHEWIJWIZARD, true);
					}

					// callbacks
					self._trigger('load', null, self._ui(self.panels[index]));
					try {
						if (o.ajaxOptions && o.ajaxOptions.success) {
							o.ajaxOptions.success(r, s);
						}
					}
					catch (e1) { }
				},
				error: function (xhr, s) {
					// callbacks
					self._trigger('load', null, self._ui(self.panels[index]));
					try {
						// Passing index avoid a race condition when this method is
						// called after the user has selected another panel.
						if (o.ajaxOptions && o.ajaxOptions.error) {
							o.ajaxOptions.error(xhr, s, index, p);
						}
					}
					catch (e2) { }
				}
			}));

			// last, so that load event is fired before show...
			ele.dequeue(self._configStr.WIJWIZARD);

			return self;
		}

		/** The abort method terminates all running panel ajax requests and animations. */
		abort(): wijwizard {
			this.element.queue([]);
			this.panels.stop(false, true);

			// configStr.WIJWIZARD queue must not contain more than two elements,
			// which are the callbacks for hide and show
			this.element.queue(this._configStr.WIJWIZARD,
				this.element.queue(this._configStr.WIJWIZARD).splice(-2, 2));

			// terminate pending requests from other wijwizard
			if (this.xhr) {
				this.xhr.abort();
				delete this.xhr;
			}

			// take care of spinners
			this._removeSpinner();
			return this;
		}

		/** The url method changes the url from which an Ajax (remote) panel will be loaded.
		  * @param {number} index The zero-based index of the panel of which its URL is to be updated.
		  * @param {string} url A URL the content of the panel is loaded from.
		  * @example 
		  * // Change the url content of second step.
		  * $("#wizard").wijwizard("url", 1, "http://wijmo.com/newurl.html");
		  */
		url(index: number, url: string): wijwizard {
			this.panels.eq(index).removeData(this._configStr.CACHEWIJWIZARD)
				.data(this._configStr.LOADWIJWIZARD, url);
			return this;
		}

		/**
		* The count method retrieves the number panels. 
		* @returns {number} the pabels's length
		*/
		count(): number {
			return this.panels.length;
		}

		/** The stop method stops displaying the panels in order automatically. */
		stop() {
			var id: number = this.element.data(this._configStr.INTIDWIZARD);
			if (id) {
				window.clearInterval(id);
				this.element.removeData(this._configStr.INTIDWIZARD);
			}

			this._isPlaying = false;
		}

		/** The play method begins displaying the panels in order automatically. */
		play() {
			var o = this.options,
				id: number,
				len: number = this.panels.length;

			if (!this.element.data(this._configStr.INTIDWIZARD)) {
				id = window.setInterval(() => {
					var index = o.activeIndex + 1;
					this._isPlaying = true;
					if (index >= len) {
						if (o.loop) {
							index = 0;
						} else {
							this.stop();
							return;
						}
					}
					this.show(index);
				}, o.delay);

				this.element.data(this._configStr.INTIDWIZARD, id);
			}
		}
	}
	/** Define the blind , fade and duration animation settings */
	export interface ShowOption {
		/** The blind option determines whether blind animation is applied when showing and hiding the panels. */
		blind?: boolean;
		/** The fade option determines whether fading animation is applied when showing and hiding the panels. */
		fade?: boolean;
		/** The duration option determines the time span between hiding or showing the panels. */
		duration?: any;
	}

	/** @ignore */
	interface WijwizardDefault {
		stepHeaderTemplate: string;
		panelTemplate: string;
		spinner: string;
	}
	/** @ignore */
	interface ConfigDataStr {
		WIJWIZARD: string;
		LOADWIJWIZARD: string;
		CACHEWIJWIZARD: string;
		SPINNERWIJWIZARD: string;//spinner.wijwizard
		INTIDWIZARD: string;//intId.wijwizard
		DESTROYWIJWIZARD: string;//destroy.wijwizard
	}
	/** @ignore */
	interface stateHandler {
		addState: (state: string, el: JQuery) => () => void;
		removeState: (state: string, el: JQuery) => () => void;
	}
	/** @ignore */
	interface ShowPanelIndex {
		panel: Element;
		index: number;
	}

	/** wijwizard options definition*/
	interface WijWizardOptions extends WidgetOptions {
		/** All CSS classes used in widgets. 
		  * @ignore
		  */
		wijCSS?: WijWizardCSS;
		/** @ignore*/
		wijMobileCSS?: any;
		/** The navButtons option defines the type of navigation buttons used with the wijwizard. 
		  * @remarks The possible values are 'auto', 'common', 'edge' and 'none'.
		  */
		navButtons: string;
		/** The autoPlay option allows the panels to automatically display in order. */
		autoPlay: boolean;
		/** The delay option determines the time span between displaying panels in autoplay mode. */
		delay: number;
		/** The loop option allows the wijwizard to begin again from the first panel
		* when reaching the last panel in autoPlay mode. */
		loop: boolean;
		/** The hideOption option defines the animation effects
		  * when hiding the panel content.
		  * @example 
		  * //Set hide animation to blind and duration to 500.
		  * $(".selector").wijwizard({
		  *		hideOption: {fade: false, blind: true, duration: 500}
		  * });
		  */
		hideOption: ShowOption;
		/** The showOption option defines the animation effects
		  * when showing the panel content.
		  * @example 
		  * //Set show animation to blind and duration to 500.
		  * $(".selector").wijwizard({
		  *		showOption: {fade: false, blind: true, duration: 500}
		  * });
		  */
		showOption: ShowOption;
		/** A value that indicates additional Ajax options to consider when
		  * loading panel content (see $.ajax).
		  * @type {object}
		  * @remarks Please see following link for more details,
		  * http://api.jquery.com/jQuery.ajax/ .
		  *
		  */
		ajaxOptions: JQueryAjaxSettings;
		/** An option that determines whether to cache emote wijwizard content. 
		  * @remarks Cached content is being lazy loaded, 
		  * for example only and only once for the panel is displayed. 
		  * Note that to prevent the actual Ajax requests from being cached by the browser, 
		  * you need to provide an extra cache: false flag to ajaxOptions.
		  */
		cache: boolean;
		/** The cookie option is a value that stores the latest active index in a cookie. 
		  * The cookie is then used to determine the initially active index
		  * if the activeIndex option is not defined. 
		  * @remarks This option requires a cookie plugin. 
		  * The object needs to have key/value pairs
		  * of the form the cookie plugin expects as options. 
		  * @type {object}
		  * @example 
		  * $(".selector").wijwizard({cookie:{expires: 7, path: '/', domain:  'jquery.com';, secure: true }})
		  */
		cookie: any;
		/** The stepHeaderTemplate option creates an HTML template 
		  * for the step header when a new panel is added with the
		  * add method or when creating a panel for a remote panel on the fly.
		  */
		stepHeaderTemplate: string;
		/** The panelTemplate option is an HTML template from which a new panel is created. 
		  * The new panel is created by adding a panel with the add method or when creating 
		  * a panel from a remote panel on the fly.
		  */
		panelTemplate: string;
		/** The HTML content of this string is shown in a panel
		  * while remote content is loading. 
		  * Pass the option in empty string to deactivate that behavior. */
		spinner: string;
		/** The backBtnText option defines the text for the wizard back button. */
		backBtnText: string;
		/** The nextBtnText option defines the text for the wijwizard next button. */
		nextBtnText: string;
		/** The activeIndex option defines the current selected index of the wijwizard. */
		activeIndex: number;
		/** The add event handler is a function called when a panel is added.
		  * @event
		  * @param {Object} e The jQuery.Event object.
		  * @param {IWijWizardEventArgs} args The data with this event.
		  */
		add: (e: JQueryEventObject, args: IWijWizardEventArgs) => void;
		/** The remove event handler is a function called when a panel is removed.
		  * @event
		  * @param {Object} e The jQuery.Event object.
		  * @param {IWijWizardEventArgs} args The data with this event.
		*/
		remove: (e: JQueryEventObject, args: IWijWizardEventArgs) => void;
		/** The activeIndexChanged event handler is a function called when the activeIndex is changed.
		  * @event
		  * @param {Object} e The jQuery.Event object.
		  * @param {IWijWizardEventArgs} args The data with this event.
		  */
		activeIndexChanged: (e: JQueryEventObject, args: IWijWizardEventArgs) => void;
		/** The show event handler is a function called when a panel is shown.
		  * @event
		  * @param {Object} e The jQuery.Event object.
		  * @param {IWijWizardEventArgs} args The data with this event.
		  */
		show: (e: JQueryEventObject, args: IWijWizardEventArgs) => void;
		/** The load event handler is a function called after the content of a remote panel has been loaded.
		  * @event
		  * @param {Object} e The jQuery.Event object.
		  * @param {IWijWizardEventArgs} args The data with this event.
		  */
		load: (e: JQueryEventObject, args: IWijWizardEventArgs) => void;
		/** The validating event handler is a function called before moving to next panel. 
		  * This event is Cancellable.
		  * @event
		  * @param {Object} e The jQuery.Event object.
		  * @param {IWijWizardValidatingEventArgs} args The data with this event.
		  */
		validating: (e: JQueryEventObject, args: IWijWizardValidatingEventArgs) => void;
	}

	/** Define the jQuery Ajax settings */
	interface JQueryAjaxSettings {
		/** Register a handler to be called when Ajax requests complete. */
		success?: (data?: any, textStatus?: string) => any;
		/** Register a handler to be called when Ajax requests complete with an error. */
		error?: (xhr?: JQueryXHR, textStatus?: string, index?: number, element?: Element) => any;
	}

	/** @ignore */
	interface WijWizardCSS extends WijmoCSS {
		wijwizard: string;
		wijwizardButtons: string;
		wijwizardPrev: string;
		wijwizardNext: string;
		wijwizardSteps: string;
		wijwizardContent: string;
		wijwizardPanel: string;
		wijwizardActived: string;
		wijwizardHide: string;
		wijwizardSpinner: string;
	}

	class wijwizard_options implements WijWizardOptions {
		/** All CSS classes used in widgets. 
		  * @ignore
		  */
		wijCSS: WijWizardCSS = {
			wijwizard: "wijmo-wijwizard",
			wijwizardButtons: "wijmo-wijwizard-buttons",
			wijwizardPrev: "wijmo-wijwizard-prev",
			wijwizardNext: "wijmo-wijwizard-next",
			wijwizardSteps: "wijmo-wijwizard-steps",
			wijwizardContent: "wijmo-wijwizard-content",
			wijwizardPanel: "wijmo-wijwizard-panel",
			wijwizardActived: "wijmo-wijwizard-actived",
			wijwizardHide: "wijmo-wijwizard-hide",
			wijwizardSpinner: "wijmo-wijwizard-spinner"
		};
		/** @ignore*/
		wijMobileCSS: any = {
			header: "ui-header ui-bar-a",
			content: "ui-body-b",
			stateDefault: "ui-btn ui-btn-a",
			stateHover: "ui-btn-down-a",
			stateActive: "ui-btn-down-a"
		};
		/** The navButtons option defines the type of navigation buttons used with the wijwizard. 
		  * @remarks The possible values are 'auto', 'common', 'edge' and 'none'.
		  */
		navButtons: string = 'auto';
		/** The autoPlay option allows the panels to automatically display in order. */
		autoPlay: boolean = false;
		/** The delay option determines the time span between displaying panels in autoplay mode. */
		delay: number = 3000;
		/** The loop option allows the wijwizard to begin again from the first panel
		* when reaching the last panel in autoPlay mode. */
		loop: boolean = false;
		/** The hideOption option defines the animation effects
		  * when hiding the panel content.
		  * @example 
		  * //Set hide animation to blind and duration to 500.
		  * $(".selector").wijwizard({
		  *		hideOption: {fade: false, blind: true, duration: 500}
		  * });
		  */
		hideOption: ShowOption = { fade: true };
		/** The showOption option defines the animation effects
		  * when showing the panel content.
		  * @example 
		  * //Set show animation to blind and duration to 500.
		  * $(".selector").wijwizard({
		  *		showOption: {fade: false, blind: true, duration: 500}
		  * });
		  */
		showOption: ShowOption = { fade: true, duration: 400 };
		/** A value that indicates additional Ajax options to consider when
		  * loading panel content (see $.ajax).
		  * @type {object}
		  * @remarks Please see following link for more details,
		  * http://api.jquery.com/jQuery.ajax/ .
		  *
		  */
		ajaxOptions: JQueryAjaxSettings = null;
		/** An option that determines whether to cache emote wijwizard content. 
		  * @remarks Cached content is being lazy loaded, 
		  * for example only and only once for the panel is displayed. 
		  * Note that to prevent the actual Ajax requests from being cached by the browser, 
		  * you need to provide an extra cache: false flag to ajaxOptions.
		  */
		cache: boolean = false;
		/** The cookie option is a value that stores the latest active index in a cookie. 
		  * The cookie is then used to determine the initially active index
		  * if the activeIndex option is not defined. 
		  * @remarks This option requires a cookie plugin. 
		  * The object needs to have key/value pairs
		  * of the form the cookie plugin expects as options. 
		  * @type {object}
		  * @example 
		  * $(".selector").wijwizard({cookie:{expires: 7, path: '/', domain:  'jquery.com';, secure: true }})
		  */
		cookie: any = null;
		/** The stepHeaderTemplate option creates an HTML template 
		  * for the step header when a new panel is added with the
		  * add method or when creating a panel for a remote panel on the fly.
		  */
		stepHeaderTemplate: string = '';
		/** The panelTemplate option is an HTML template from which a new panel is created. 
		  * The new panel is created by adding a panel with the add method or when creating 
		  * a panel from a remote panel on the fly.
		  */
		panelTemplate: string = '';
		/** The HTML content of this string is shown in a panel
		  * while remote content is loading. 
		  * Pass the option in empty string to deactivate that behavior. */
		spinner: string = '';
		/** The backBtnText option defines the text for the wizard back button. */
		backBtnText: string = 'back';
		/** The nextBtnText option defines the text for the wijwizard next button. */
		nextBtnText: string = 'next';
		/** The activeIndex option defines the current selected index of the wijwizard. */
		activeIndex: number;
		/** The add event handler is a function called when a panel is added.
		  * @event
		  * @param {Object} e The jQuery.Event object.
		  * @param {IWijWizardEventArgs} args The data with this event.
		  */
		add: (e: JQueryEventObject, args: IWijWizardEventArgs) => void = null;
		/** The remove event handler is a function called when a panel is removed.
		  * @event
		  * @param {Object} e The jQuery.Event object.
		  * @param {IWijWizardEventArgs} args The data with this event.
		*/
		remove: (e: JQueryEventObject, args: IWijWizardEventArgs) => void = null;
		/** The activeIndexChanged event handler is a function called when the activeIndex is changed.
		  * @event
		  * @param {Object} e The jQuery.Event object.
		  * @param {IWijWizardEventArgs} args The data with this event.
		  */
		activeIndexChanged: (e: JQueryEventObject, args: IWijWizardEventArgs) => void = null;
		/** The show event handler is a function called when a panel is shown.
		  * @event
		  * @param {Object} e The jQuery.Event object.
		  * @param {IWijWizardEventArgs} args The data with this event.
		  */
		show: (e: JQueryEventObject, args: IWijWizardEventArgs) => void = null;
		/** The load event handler is a function called after the content of a remote panel has been loaded.
		  * @event
		  * @param {Object} e The jQuery.Event object.
		  * @param {IWijWizardEventArgs} args The data with this event.
		  */
		load: (e: JQueryEventObject, args: IWijWizardEventArgs) => void = null;
		/** The validating event handler is a function called before moving to next panel. 
		  * This event is Cancellable.
		  * @event
		  * @param {Object} e The jQuery.Event object.
		  * @param {IWijWizardValidatingEventArgs} args The data with this event.
		  */
		validating: (e: JQueryEventObject, args: IWijWizardValidatingEventArgs) => void = null;
	};

	wijwizard.prototype.options = $.extend(true, {}, wijmoWidget.prototype.options, new wijwizard_options());

	$.wijmo.registerWidget("wijwizard", wijwizard.prototype);

	export interface IWijWizardEventArgs {
		/** The panel element.*/
		panel: HTMLElement;
		/** The index of the panel.*/
		index: number;
	};

	export interface IWijWizardValidatingEventArgs {
		/** The next panel element.*/
		nextPanel: HTMLElement;
		/** The index of the next panel.*/
		nextIndex: number;
	};
}

/** @ignore*/
interface JQuery {
	wijwizard: JQueryWidgetFunction;
}