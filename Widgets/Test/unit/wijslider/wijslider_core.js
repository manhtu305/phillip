﻿/*
* wijslider_core.js
*/

var el;

function create_wijslider(options) {
    var slider = $('<div id="slider1" style="width:200px; height:30px;"></div>');
    slider.appendTo("body");
    return slider.wijslider(options);
};

function create_wijsliderv(options) {
    var slider = $('<div id="slider1" style="width:30px; height:200px;"></div>');
    slider.appendTo("body");
    return slider.wijslider(options);
};
(function ($) {

    module("wijslider: core");
    test("create and destroy", function () {
        // test disconected element
        var $widget = create_wijslider({ orientation: "horizontal", range: true, min: 0, max: 500, step: 50, values: [100, 400] });
        ok($widget.hasClass("ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"), 'element css classes created.');
        ok($widget.parent().children().length > 0, 'created child nodes.');
        ok($widget.parent().children().first().hasClass("wijmo-wijslider-decbutton ui-corner-all ui-state-default"), 'decbutton element created.');
        ok($widget.parent().children().eq(1).hasClass("ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"), 'slider progress element created');
        if ($widget.parent().children().eq(1).children()) {
            ok($widget.parent().children().eq(1).children().eq(0).has("ui-slider-range ui-widget-header"), 'slider range element created.');
            ok($widget.parent().children().eq(1).children().eq(1).has("ui-slider-handle ui-state-default ui-corner-all"), 'slider thumb1 element created.');
            ok($widget.parent().children().eq(1).children().eq(2).has("ui-slider-handle ui-state-default ui-corner-all"), 'slider thumb2 element created.');
        }
        ok($widget.parent().children().last().hasClass("wijmo-wijslider-incbutton ui-corner-all ui-state-default"), 'incbutton element created.');
        $widget.stop();
        $widget.wijslider("destroy");
        ok(!$widget.hasClass("ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"), 'element css classes removed.');
        ok($widget.children().length == 0, 'child elements removed.');
    });
})(jQuery);