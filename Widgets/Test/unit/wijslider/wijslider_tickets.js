﻿/*
* wijslider_options.js
*/
(function ($) {

    module("wijslider: tickets");

    test("#37970", function () {
        var $widget = create_wijslider({ orientation: "horizontal", disabled: true });
        ok($(".ui-disabled").length === 0, "The disabled div's don't exist!");

        $widget.remove();
    });

    // the IE browser can't support the drag simulate.
    if (!$.browser.msie) {
        test("dragfill doesn't work", function () {
            var $widget = create_wijslider({
                orientation: "horizontal",
                min: 0,
                max: 50,
                range: true,
                step: 1,
                dragFill: true,
                values: [5, 15]
            }), width = $widget.width(),
            rangeEle = $widget.find(".ui-slider-range");
            ok($widget.wijslider("option", "values").join(",") === "5,15", "before drag, the values is 5, 15");
            rangeEle.simulate("drag", { dx: 10 * width / 50, dy: 0 });
            ok($widget.wijslider("option", "values").join(",") === "15,25", "after drag, the value is 15, 25");
            $widget.remove();
        });
    }

    test("#69451", function () {
        var $widget = create_wijslider({
            orientation: "horizontal",
            min: "100",
            max: "200",
            range: true,
            step: "10",
            values: ["120", "150"]
        }),
            incBtn = $widget.next(".wijmo-wijslider-incbutton");

        incBtn.simulate("click");
        ok($widget.wijslider("values", 1) === 160, "The wijslider increases correctly with setting string type option when create.");

        $widget.wijslider({
            orientation: "horizontal",
            min: "120",
            max: "320",
            range: true,
            step: "20",
            values: ["150", "200"]
        });
        incBtn.simulate("click");
        ok($widget.wijslider("values", 1) === 220, "The wijslider increases correctly with setting string type option when update option.");

        $widget.remove();
    });

    test("#88522", function () {
        var $widget = create_wijslider({
            orientation: "horizontal",
            min: "100",
            max: "200",
            range: true,
            dragFill: true,
            values: ["120", "150"],
            start: function () {
                startCount++;
            },
            stop: function () {
                stopCount++;
            }
        }),
        $rightThumb = $widget.find(".ui-slider-handle").eq(1),
        $rangeFill = $widget.find(".ui-slider-range"),
        startCount = 0, stopCount = 0;

        $rightThumb.simulate("drag", { dx: -10, dy: 0, moves: 5 });
        ok(startCount === 1 && stopCount === 1, "events fired correctly when dragging thumb.");
        $rangeFill.simulate("drag", { dx: 10, dy: 0, moves: 5 });
        ok(startCount === 2 && stopCount === 2, "events fired correctly when dragging fill.");
        $widget.remove();
    });

    test("#106534", function () {
        var $widget = create_wijslider({
            disabled: true,
            orientation: "horizontal",
            min: 0,
            max: 50,
            range: true,
            step: 1,
            dragFill: true,
            values: [5, 15],
            start: function () {
                eventTriggered = true;
            },
            change: function () {
                eventTriggered = true;
            },
            slide: function () {
                eventTriggered = true;
            },
            stop: function () {
                eventTriggered = true;
            }
        }), rangeEle = $widget.find(".ui-slider-range"), width = $widget.width(),
        eventTriggered = false;
        rangeEle.simulate("drag", { dx: 10 * width / 50, dy: 0 });

        ok(!eventTriggered, "When disabled option is true, the fill part can not drag !");
        $widget.remove();
    });
})(jQuery);