﻿(function ($) {
	module("wijslider:methods");

	test("setOption", function () {
		var $widget;

		//set range option
		$widget = create_wijslider({ range: true });
		ok($widget.find(".ui-slider-range").length === 1, "range:range is added");
		ok($widget.find(".ui-slider-handle").length === 2, "range:has two handles");
		$widget.wijslider({ range: false });
		ok($widget.find(".ui-slider-range").length === 0, "range:range is removed");
		$widget.remove();

		//set value option
		$widget = create_wijslider({ value: 10 });
		ok($widget.find(".ui-slider-handle").length === 1, "value:has single handle");
		$widget.wijslider({ values: [10, 20] });
		ok($widget.find(".ui-slider-handle").length === 2, "value:change to two handles");
		$widget.find(".ui-slider-handle:eq(1)").simulate("drag", { dx: 10, dy: 0 });
		ok($widget.wijslider("values") !== [10, 20], "");
		$widget.remove();

		//set values option
		$widget = create_wijslider({ values: [10, 30] });
		ok($widget.find(".ui-slider-handle").length === 2, "values: has 2 handles");
		$widget.wijslider({ values: [10, 20, 30] });
		ok($widget.find(".ui-slider-handle").length === 3, "values:has 3 handles");
		$widget.wijslider({ range: true });
		ok($widget.find(".ui-slider-handle").length === 2, "values:has 2 handles");
		ok($widget.has(".ui-slider-range").length > 0, "values:range is added");
		$widget.wijslider({ range: false, values: null });
		ok($widget.find(".ui-slider-handle").length === 1, "values: has 1 handle");
		$widget.wijslider({ range: true, values: [] });
		ok($widget.find(".ui-slider-handle").length === 2, "has 2 handle when range is true");
		ok($widget.wijslider("values").length === 2, "empty values array change to 2 length");
		$widget.remove();
	});

})(jQuery);