/*
* wijslider_events.js
*/
(function ($) {
    module("wijslider: events");

    test("mouseover, mousedown, mouseup, mouseout", function () {
        expect(8);
        var $widget = create_wijslider();

        $('.wijmo-wijslider-decbutton').simulate('mouseover');
        ok($('.wijmo-wijslider-decbutton').hasClass("ui-state-hover"), "decbutton mouseover event");

        $('.wijmo-wijslider-decbutton').simulate('mouseout');
        ok(!$('.wijmo-wijslider-decbutton').hasClass("ui-state-hover"), "decbutton mouseout event");

        $('.wijmo-wijslider-decbutton').simulate('mousedown');
        ok($('.wijmo-wijslider-decbutton').hasClass("ui-state-active"), "decbutton mousedown event");

        $('.wijmo-wijslider-decbutton').simulate('mouseup');
        ok(!$('.wijmo-wijslider-decbutton').hasClass("ui-state-active"), "decbutton mouseup event");

        $('.wijmo-wijslider-incbutton').simulate('mouseover');
        ok($('.wijmo-wijslider-incbutton').hasClass("ui-state-hover"), "incbutton mouseover event");

        $('.wijmo-wijslider-incbutton').simulate('mouseout');
        ok(!$('.wijmo-wijslider-incbutton').hasClass("ui-state-hover"), "incbutton mouseout event");

        $('.wijmo-wijslider-incbutton').simulate('mousedown');
        ok($('.wijmo-wijslider-incbutton').hasClass("ui-state-active"), "incbutton mousedown event");

        $('.wijmo-wijslider-incbutton').simulate('mouseup');
        ok(!$('.wijmo-wijslider-incbutton').hasClass("ui-state-active"), "incbutton mouseup event");

        $widget.remove();
    });
    
    test("event fired", function () {
    	var i = 0;
        var $widget = create_wijslider();
        
        $widget.bind("wijsliderbuttonclick", function () {
        	i = 1;
        });
        $('.wijmo-wijslider-incbutton').simulate('click');
        ok(i=== 1, "buttonclick event filed");
        $widget.remove();
    });
})(jQuery);