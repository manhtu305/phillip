/*
* wijslider_defaults.js
*/
var wijslider_defaults = {
    //cancel: ':input,option',
    cancel:'input,textarea,button,select,option',
    distance: 1,
    delay: 0,
    distance: 0,
    disabled: false,
    /// <summary>
    /// A value determines whether the slider is allow the ability to drag the fill between the buttons. 
    /// Default: true.
    /// Type: Boolean.
    /// </summary>
    dragFill: true,
    /// <summary>
    /// A value indicates whether to slide handle smoothly when user click outside handle on the bar.
    /// Default: false.
    /// Type: Boolean.
    /// </summary>
    animate: false,
    /// <summary>
    /// A value indicates the maximum value of the slider.
    /// Default: 100.
    /// Type: Number.
    /// </summary>
    max: 100,
    /// <summary>
    /// A value indicates the minimum value of the slider.
    /// Default: 0.
    /// Type: Number.
    /// </summary>
    min: 0,
    /// <summary>
    /// A value prevent the two range handles from being placed on top of 
    /// one another.
    /// Default: 0.
    /// Type: Number.
    /// </summary>
    minRange: 0,
    /// <summary>
    /// A value indicates whether the slider has the min at the left, the max at the right or the min at the bottom, the max at the top. Possible values: 'horizontal', 'vertical'.
    /// Default: 'horizontal'.
    /// Type: String.
    /// </summary>
    orientation: "horizontal",
    /// <summary>
    /// If the option value set to true, the slider will detect if you have two handles and create a stylable range element between these two. Two other possible values are 'min' and 'max'. A min range goes from the slider min to one handle. A max range goes from one handle to the slider max.
    /// Default: false.
    /// Type: Boolean.
    /// </summary>
    range: false,
    /// <summary>
    /// A value indicates the size or amount of each interval or step the slider takes between min and max. The full specified value range of the slider (max - min) needs to be evenly divisible by the step.
    /// Default: 1.
    /// Type: Number.
    /// </summary>
    step: 1,
    /// <summary>
    /// A value indicates the value of the slider, if there's only one handle. If there is more than one handle, determines the value of the first handle.
    /// Default: 0.
    /// Type: Number.
    /// </summary>
    value: 0,
    /// <summary>
    /// This option can be used to specify multiple handles. If range is set to true, the length of 'values' should be 2.
    /// Default: null.
    /// Type: Array.
    /// </summary>
    values: null,
    /// <summary>
    /// Raised when the mouse is over the decrement button or increment button.
    /// Default: null.
    /// Type: Function.
    /// </summary>
    buttonMouseOver: null,
    /// <summary>
    /// Raised when the mouse is leave the decrement button or increment button.
    /// Default: null.
    /// Type: Function.
    /// </summary>
    buttonMouseOut: null,
    /// <summary>
    /// Raised when the mouse is down on the decrement button or decrement button.
    /// Default: null.
    /// Type: Function.
    /// </summary>
    buttonMouseDown: null,
    /// <summary>
    /// Raised when the mouse is up on the decrement button or increment button.
    /// Default: null.
    /// Type: Function.
    /// </summary>
    buttonMouseUp: null,
    /// <summary>
    /// Raised when the decrement button or incremen tbutton  has been clicked.
    /// Default: null.
    /// Type: Function.
    /// </summary>
    buttonClick: null,
    change:null,
    slide:null,
    start:null,
    stop:null,
    initSelector: ":jqmData(role='wijslider')",
    create: null
};

commonWidgetTests('wijslider', {
    defaults: wijslider_defaults
});
