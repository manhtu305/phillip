﻿/*
* wijslider_options.js
*/
(function ($) {

    module("wijslider: options");

    test("value", function () {
        var $widget = create_wijslider({ value: 50 });
        ok($widget.find('.ui-slider-handle').get(0).style.left == "50%", "set value to 50");
        $widget.remove();
        var $widget = create_wijslider({ value: 30 });
        ok($widget.find('.ui-slider-handle').get(0).style.left == "30%", "set value to 30");
        $widget.remove();
    });

    test("orientation", function () {
        var $widget = create_wijsliderv({ orientation: "vertical" });
        ok($widget.hasClass('ui-slider ui-slider-vertical ui-widget ui-widget-content ui-corner-all'), "set orientation to 'vertical'");
        $widget.remove();
        var $widget = create_wijslider({ orientation: "horizontal" });
        ok($widget.hasClass('ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all'), "set orientation to 'horizontal'");
        $widget.remove();
    });

    test("values", function () {
        var $widget = create_wijslider({ values: [30, 70] });
        ok($widget.find('.ui-slider-handle').eq(0).get(0).style.left == "30%" && $widget.find('.ui-slider-handle').eq(1).get(0).style.left == "70%", "set values to [30,70]");
        $widget.remove();
        var $widget = create_wijslider({ values: [20, 80] });
        ok($widget.find('.ui-slider-handle').eq(0).get(0).style.left == "20%" && $widget.find('.ui-slider-handle').eq(1).get(0).style.left == "80%", "set values to [20,80]");
        $widget.remove();
    });

    test("range", function () {
        var $widget = create_wijslider({ range: true, values: [10, 40] });
        ok($widget.find('div').hasClass('ui-slider-range') &&
            $widget.find('div').hasClass('ui-widget-header'), "set range to true");
        $widget.remove();
        var $widget = create_wijslider({ range: false });
        ok(!$widget.find('div').hasClass('ui-slider-range ui-widget-header'), "set range to false");
        $widget.remove();
    });

    test("step", function () {
        var $widget = create_wijslider({ step: 10 });
        $('.wijmo-wijslider-incbutton').simulate('click');
        ok($widget.find('.ui-slider-handle').get(0).style.left == "10%", "set step to 10");
        $widget.remove();
        var $widget = create_wijslider({ step: 20 });
        $('.wijmo-wijslider-incbutton').simulate('click');
        ok($widget.find('.ui-slider-handle').get(0).style.left == "20%", "set step to 20");
        $widget.remove();
        var $widget = create_wijslider({ step: 30 });
        $('.wijmo-wijslider-incbutton').simulate('click');
        ok($widget.find('.ui-slider-handle').get(0).style.left == "30%", "set step to 30");
        $widget.remove();
    });

    test("disabled", function () {
        var $widget = create_wijslider({ orientation: "horizontal", disabled: true });
        ok($(".wijmo-wijslider-decbutton").hasClass("ui-state-disabled"), "the dec button has disabled class.");
        ok($(".wijmo-wijslider-incbutton").hasClass("ui-state-disabled"), "the inc button has disabled class.");
        $widget.wijslider("option", "disabled", false);
        ok(!$(".wijmo-wijslider-decbutton").hasClass("ui-state-disabled"), "the dec button has not disabled class.");
        ok(!$(".wijmo-wijslider-incbutton").hasClass("ui-state-disabled"), "the inc button has not disabled class.");
        $widget.remove();
    });

})(jQuery);