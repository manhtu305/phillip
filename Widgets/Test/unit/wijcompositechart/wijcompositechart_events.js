/*globals start, stop, test, ok, module, jQuery, 
	simpleOptions, createSimpleCompositechart, createCompositechart*/
/*
* wijcompositechart_events.js
*/
"use strict";
(function ($) {
	var compositechart,
		newSeriesList = [{
			type: "bar",
			label: "1800",
			legendEntry: true,
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [100, 31, 635, 203] }
		}, {
			type: "bar",
			label: "1900",
			legendEntry: true,
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [133, 156, 947, 408] }
		}, {
			type: "bar",
			label: "2008",
			legendEntry: true,
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [973, 914, 4054, 732] }
		}];

	module("wijcompositechart: events");

	test("mousedown", function () {
		compositechart = createSimpleCompositechart();
		compositechart.wijcompositechart({
			mouseDown: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 0, 'Press the mouse down on the first bar.');
					compositechart.remove();
				}
			}
		});
		stop();
		$(compositechart.wijcompositechart("getElement", "bar", 1).node).trigger('mousedown');
	});

	test("mouseup", function () {
		compositechart = createSimpleCompositechart();
		compositechart.wijcompositechart({
			mouseUp: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 1, 'Release the mouse on the first bar.');
					compositechart.remove();
				}
			}
		});
		stop();
		$(compositechart.wijcompositechart("getElement", "bar", 3).node).trigger('mouseup');
	});

	test("mouseover", function () {
		compositechart = createSimpleCompositechart();
		compositechart.wijcompositechart({
			mouseOver: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 2, 'Hover the last sector.');
					compositechart.remove();
				}
			}
		});
		stop();
		$(compositechart.wijcompositechart("getElement", "pie", 2).node).trigger('mouseover');
	});

	test("mouseout", function () {
		compositechart = createSimpleCompositechart();
		compositechart.wijcompositechart({
			mouseOut: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 1, 'Move the mouse out of the second line.');
					compositechart.remove();
				}
			}
		});
		stop();
		$(compositechart.wijcompositechart("getElement", "line", 1).node).trigger('mouseout');
	});

	test("mousemove", function () {
		compositechart = createSimpleCompositechart();
		compositechart.wijcompositechart({
			mouseMove: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 2, 'Move the mouse on the third marker in the first line.');
					compositechart.remove();
				}
			}
		});
		stop();
		$(compositechart.wijcompositechart("getElement", "linemarkers", 0)[2].node).trigger('mousemove');
	});

	test("click", function () {
		compositechart = createSimpleCompositechart();
		compositechart.wijcompositechart({
			click: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 3, 'Click on the fourth marker in the second line.');
					compositechart.remove();
				}
			}
		});
		stop();
		$(compositechart.wijcompositechart("getElement", "linemarkers", 1)[3].node).trigger('click');
	});

	test("beforeserieschange", function () {
		compositechart = createSimpleCompositechart();
		var oldSeriesList = compositechart.wijcompositechart("option", "seriesList");
		compositechart.wijcompositechart({
			beforeSeriesChange: function (e, seriesObj) {
				if (seriesObj !== null) {
					start();
					var seriesList = compositechart.wijcompositechart("option", "seriesList");
					ok(seriesObj.oldSeriesList === oldSeriesList, 
							'get the correct old seriesList by seriesObj.oldSeriesList.');
					ok(seriesObj.newSeriesList === newSeriesList, 
							'get the correct new seriesList by seriesObj.newSeriesList.');
					ok(seriesList === oldSeriesList, 
							"currently seriesList hasn't been changed.");
					compositechart.remove();
				}
			}
		});
		stop();
		compositechart.wijcompositechart("option", "seriesList", newSeriesList);
	});

	test("serieschanged", function () {
		compositechart = createSimpleCompositechart();
		compositechart.wijcompositechart({
			seriesChanged: function (e, seriesObj) {
				if (seriesObj !== null) {
					start();
					var seriesList = compositechart.wijcompositechart("option", "seriesList");
					ok(seriesList === newSeriesList, 
						"currently seriesList has been changed.");
					compositechart.remove();
				}
			}
		});
		stop();
		compositechart.wijcompositechart("option", "seriesList", newSeriesList);
	});

	test("beforepaint, paint", function () {
		var options = $.extend({
				beforePaint: function (e) {
					ok(true, "The beforepaint event is invoked.");
					return false;
				},
				painted: function (e) {
					ok(false, "The painted event of composite chart1 is invoked.");
				}
			}, true, simpleOptions),
			sector, options2, compositechart2;
		compositechart = createCompositechart(options);

		try {
			sector = compositechart.wijcompositechart("getElement", "bar", 1);
			ok(false, "The composite chart1 shouldn't be painted because of the cancellation.");
		}
		catch (ex) {
			ok(true, "The composite chart1 isn't painted because of the cancellation.");
		}
		compositechart.remove();

		options2 = $.extend({
			beforePaint: function (e) {
				if (e !== null) {
					ok(true, "The beforepaint event is invoked.");
				}
			},
			painted: function (e) {
				ok(true, "The painted event of composite chart2 is invoked.");
			}
		}, true, simpleOptions);
		compositechart2 = createCompositechart(options2);
		sector = compositechart2.wijcompositechart("getElement", "line", 1);
		ok(sector !== null, 'The composite chart2 is painted.');
		compositechart2.remove();
	});
}(jQuery));
