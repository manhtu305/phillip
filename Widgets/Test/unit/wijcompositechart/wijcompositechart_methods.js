/*globals test, ok, module, jQuery, createSimpleCompositechart*/
/*
 * wijcompositechart_methods.js
 */
"use strict";
(function ($) {

	module("wijcompositechart: methods");

	test("getElement", function () {
		var compositechart = createSimpleCompositechart(),
			sector;
			
		sector = compositechart.wijcompositechart('getElement', "column", 1);
		ok(sector.attrs.fill === '#223344', 'getElement:gets the second bar');
		
		sector = compositechart.wijcompositechart('getElement', "pie", 1);
		ok(sector.attrs.fill === '#556677', 'getElement:gets the second pie sector');

		sector = compositechart.wijcompositechart('getElement', "line", 1);
		ok(sector.attrs.stroke === '#89ABCD', 'getElement:gets the second line');

		compositechart.remove();
	});

	test("getCanvas", function () {
		var compositechart = createSimpleCompositechart(),
			canvas = compositechart.wijcompositechart('getCanvas');

		ok(canvas.circle !== null, 'getCanvas:gets the canvas for the composite chart');
		compositechart.remove();
	});

}(jQuery));
