﻿(function ($) {
    module("wijcompositechart: tickets");
    test("when the chart is only contains line series, an exception will be thrown", function () {
        var compositechart = createCompositechart({
            seriesList: [{ data: { x: ['x1', 'x2', 'x3', 'x4'], y: [5, 10, 12, 20] }, type: "line" }]
        });

        ok(true, 'ok.');
        compositechart.remove();
    });
    test("when resize the chart which contains pie chart, the pie sectors will be overlaped.", function () {
        var composiitechart = createCompositechart({
            seriesList: [{
                type: "pie",
                legendEntry: true,
                center: { x: 400, y: 250 },
                radius: 200,
                data: [{
                    label: "Chrome",
                    legendEntry: true,
                    data: 38,
                    offset: 0
                }, {
                    label: "Firefox",
                    legendEntry: true,
                    data: 36,
                    offset: 0
                }, {
                    label: "IE",
                    legendEntry: true,
                    data: 13,
                    offset: 0
                }, {
                    label: "Safari",
                    legendEntry: true,
                    data: 8,
                    offset: 0
                }, {
                    label: "Other",
                    legendEntry: true,
                    data: 5,
                    offset: 0
                }]
            }],
            width: 800,
            height: 600
        });
        ok($("#compositechart").data("wijmo-wijcompositechart").seriesEles[0].eles.sector.radius === 200, "the radius of the pie sector is 200.");
        composiitechart.wijcompositechart({ width: 400, height: 300 });
        ok($("#compositechart").data("wijmo-wijcompositechart").seriesEles[0].eles.sector.radius < 200, "reset the width and heigth options, the radius of the pie sector is adjusted.");
        composiitechart.remove();
    });
    test("when the pie series's radius is not set, the radius is not 50 as default.", function () {
        var composiitechart = createCompositechart({
            seriesList: [{
                type: "pie",
                legendEntry: true,
                center: { x: 400, y: 250 },               
                data: [{
                    label: "Chrome",
                    legendEntry: true,
                    data: 38,
                    offset: 0
                }, {
                    label: "Firefox",
                    legendEntry: true,
                    data: 36,
                    offset: 0
                }, {
                    label: "IE",
                    legendEntry: true,
                    data: 13,
                    offset: 0
                }, {
                    label: "Safari",
                    legendEntry: true,
                    data: 8,
                    offset: 0
                }, {
                    label: "Other",
                    legendEntry: true,
                    data: 5,
                    offset: 0
                }]
            }],
            width: 800,
            height: 600
        });
        ok(composiitechart.data("wijmo-wijcompositechart").seriesEles[0].eles.sector.radius === 50, "the radius of the pie sector is 50.");
        composiitechart.remove();
    });

    test("#38155", function () {
        var composiitechart = createCompositechart({
            seriesList: [{
                type: "pie",
                legendEntry: true,
                radius: 200,
                data: [{
                    label: "Chrome",
                    legendEntry: true,
                    data: 38,
                    offset: 0
                }, {
                    label: "Firefox",
                    legendEntry: true,
                    data: 36,
                    offset: 0
                }, {
                    label: "IE",
                    legendEntry: true,
                    data: 13,
                    offset: 0
                }, {
                    label: "Safari",
                    legendEntry: true,
                    data: 8,
                    offset: 0
                }, {
                    label: "Other",
                    legendEntry: true,
                    data: 5,
                    offset: 0
                }]
            }],
            width: 800,
            height: 600
        });
        var chartObj = composiitechart.data("wijmo-wijcompositechart"),
            canvas = chartObj.canvas,
            set = canvas.set(),
            seriesEles = chartObj.seriesEles;
        
        $.each(seriesEles, function (i, se) {
            set.push(se.eles.sector);
        });
        var bounds = set.wijGetBBox(),
            canvasBounds = chartObj.canvasBounds;
        bounds.startX = bounds.x;
        bounds.startY = bounds.y;
        bounds.endX = bounds.x + bounds.width;
        bounds.endY = bounds.y + bounds.height;

        ok(bounds.startX > canvasBounds.startX && bounds.endX < canvasBounds.endX && bounds.startY > canvasBounds.startY && bounds.endY < canvasBounds.endY, "the pie chart is not clipped");
        composiitechart.remove();
    });

    test("When min/max of axis option is set, autoMin/autoMax should be set to false", function () {
        var compositechart = createCompositechart({
            axis: {
                x: {
                    max: 10,
                    min: 0
                },
                y: {
                    min: 0,
                    max: 30
                }
            },
            seriesList: [
                { data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20] }, type: "scatter" },
                { data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20] }, type: "line" },
                { data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20], y1: [5, 10, 15, 20] }, type: "bubble" }]
        });
        
        var axis = compositechart.wijcompositechart("option", "axis");
        ok(axis.x.autoMin === false && axis.x.min === 0, "autoMin of x axis is set to false when axis.x.min is set.");
        ok(axis.x.autoMax === false && axis.x.max === 10, "autoMax of x axis is set to false when axis.x.max is set.");
        ok(axis.y.autoMin === false && axis.y.min === 0, "autoMin of y axis is set to false when axis.y.min is set.");
        ok(axis.y.autoMax === false && axis.y.max === 30, "autoMax of y axis is set to false when axis.y.max is set.");
        compositechart.remove();

        compositechart = createCompositechart({
            seriesList: [
                { data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20] }, type: "scatter" },
                { data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20] }, type: "line" },
                { data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20], y1: [5, 10, 15, 20] }, type: "bubble" }]
        });
        axis = compositechart.wijcompositechart("option", "axis");
        ok(axis.x.autoMin === true, "autoMin of x axis is true.");
        ok(axis.x.autoMax === true, "autoMax of x axis is true.");
        ok(axis.y.autoMin === true, "autoMin of y axis is true.");
        ok(axis.y.autoMax === true, "autoMax of y axis is true.");
        compositechart.wijcompositechart("option", "axis", {
            x: {
                max: 10,
                min: 0
            },
            y: {
                min: 0,
                max: 30
            }
        });
        axis = compositechart.wijcompositechart("option", "axis");
        ok(axis.x.autoMin === false && axis.x.min === 0, "autoMin of x axis is set to false when axis.x.min is set.");
        ok(axis.x.autoMax === false && axis.x.max === 10, "autoMax of x axis is set to false when axis.x.max is set.");
        ok(axis.y.autoMin === false && axis.y.min === 0, "autoMin of y axis is set to false when axis.y.min is set.");
        ok(axis.y.autoMax === false && axis.y.max === 30, "autoMax of y axis is set to false when axis.y.max is set.");
        compositechart.remove();
    });

    test("#46954", function () {
    	var compositechart = createCompositechart({
    		"seriesList": [{
    			"data": {
    				"x": ["1", "2", "3", "4", "5"], "y": [3, 6, 9, 7, 5]
    			}, "type": "spline"
    		}, {
    			"data": {
    				"x": ["1", "2", "3", "4", "5"], "y": [2, 5, 6, 4, 3]
    			}, "type": "area"
    		}],
    		"seriesStyles": [{
    			"fill": "#DE1346", "stroke": "#DE1346", "stroke-dasharray": "- ", "stroke-width": 3
    		}, {
    			"fill": "#8591C3", "stroke": "#8591C3", "stroke-width": 3
    		}]
    	}), targetSeries;

    	setTimeout(function () {
    		targetSeries = $("#compositechart").find("[stroke='#8591c3'].wijchart-canvas-object");
    		ok(targetSeries.attr("stroke-dasharray") === undefined, "\"stroke-dasharray\" attribute of the second series has not been changed!");
    		start();
    		compositechart.remove();
    	}, 1000)
    	stop();
    });

    test("#49875, test render pie chart in compositechart", function () {
    	var composiitechart = createCompositechart({
    		seriesList: [{
    			type: "pie",
    			legendEntry: true,
    			radius: 200,
    			data: [{
    				label: "Chrome",
    				legendEntry: true,
    				data: 38,
    				offset: 0
    			}, {
    				label: "Firefox",
    				legendEntry: true,
    				data: 36,
    				offset: 0
    			}, {
    				label: "IE",
    				legendEntry: true,
    				data: 13,
    				offset: 0
    			}, {
    				label: "Safari",
    				legendEntry: true,
    				data: 8,
    				offset: 0
    			}, {
    				label: "Other",
    				legendEntry: true,
    				data: 5,
    				offset: 0
    			}]
    		}]
    	});

    	ok(true, "The pie chartis rendered successfully in wijcompositechart.");

    	composiitechart.remove();
    });

    test("#52205", function () {
    	var composiitechart = createCompositechart({
    		seriesList: [{
    			type: "pie",
    			legendEntry: true,
    			radius: 200,
    			data: [{
    				label: "Chrome",
    				legendEntry: true,
    				data: 38,
    				offset: 0
    			}, {
    				label: "Firefox",
    				legendEntry: true,
    				data: 36,
    				offset: 0
    			}, {
    				label: "IE",
    				legendEntry: true,
    				data: 13,
    				offset: 0
    			}, {
    				label: "Safari",
    				legendEntry: true,
    				data: 8,
    				offset: 0
    			}, {
    				label: "Other",
    				legendEntry: true,
    				data: 5,
    				offset: 0
    			}]
    		}, {
    			type: "hloc",
    			label: "MSFT",
    			legendEntry: true,
    			data: {
    				x: [new Date("2014/1/1"), new Date("2014/1/2"), new Date("2014/1/3")],
    				high: [5, 7, 6, 4],
    				low: [1, 3, 2, 1],
    				open: [2, 5, 4, 4],
    				close: [4, 6, 3, 2]
    			}
    		}]
    	});
    	ok(true, "No exception is thrown");
    	composiitechart.remove();
    });

    test("#59179", function () {
    	var compositechart = createCompositechart({
    		animation: null,
    		seriesList: [{
    			type: "bubble",
    			label: "2010",
    			legendEntry: true,
    			data: {
    				x: ["Jan"],
    				y: [100],
    				y1: [100]
    			},
    			markers: {
    				visible: true,
    				type: "circle"
    			}
    		}]
    	});

    	$("text.wijchart-legend").simulate("click");
    	ok($(".wijbubblechart-bubble").css("display") === "none", "The bubble will be hidden after clicking the lengend.");
    	ok($(".wijbubblechart-label").css("display") === "none", "The bubble label will be hidden after clicking the lengend.");
    	ok($(".wijbubblechart-bubble.bubbletracker").css("display") === "none", "The bubble tracker will be hidden after clicking the lengend.");
    	ok($(compositechart.data("fields").bubbleInfos[0].bubble.shadow.node).css("display") === "none", "The bubble shadow will be hidden after clicking the lengend.");

    	$("text.wijchart-legend").simulate("click");
    	ok($(".wijbubblechart-bubble").css("display") !== "none", "The bubble will be shown after clicking the lengend again.");
    	ok($(".wijbubblechart-label").css("display") !== "none", "The bubble label will be shown after clicking the lengend again.");
    	ok($(".wijbubblechart-bubble.bubbletracker").css("display") !== "none", "The bubble tracker will be shown after clicking the lengend again.");
    	ok($(compositechart.data("fields").bubbleInfos[0].bubble.shadow.node).css("display") !== "none", "The bubble shadow will be shown after clicking the lengend again.");

    	compositechart.remove();
    });

    test("#56612, wijcompositechart with one String X axis value doesn't display correctly.", function () {
        var compositechart = createCompositechart({
            seriesList: [{
                type: "column",
                data: { x: ["ABC"], y: [5] }
            }]
        }), axis = compositechart.data("wijmo-wijcompositechart").axisInfo;
        ok(axis.x.min === -1.025, "The min value of the axis x is -1.025.");
        ok(axis.x.max === 1.025, "The max value of the axis x is 1.025.");

        compositechart.remove();
    });

    test("#71878", function () {
        var seriesEles, trendlinePath,
            compositechart = createCompositechart({
                seriesList: [{
                    label: "Trendline",
                    legendEntry: true,
                    isTrendline: true,
                    data: { x: [1, 2, 3], y: [5, 10, 12] }
                }],
                mouseOver: function (e, chartObj) {
                    if (chartObj !== null) {
                        ok(chartObj.type === "trendLine", 'Hover on the trendline.');
                    }
                },
                seriesStyles: [{
                    stroke: "#FFA41C", "stroke-width": 1, "stroke-opacity": 0.7
                }],
                seriesHoverStyles: [{
                    "stroke-width": 5
                }]
            });

        seriesEles = compositechart.data("wijmo-wijcompositechart").seriesEles;
        ok(seriesEles.length === 1 && (seriesEles[0].type === "trendLine"), "The trendline is drawn.");
        trendlinePath = seriesEles[0].eles.path;
        ok(trendlinePath.attr("stroke-width") === 1, "SeriesStyle is applied on trendline");
        $(trendlinePath.node).trigger('mouseover');
        ok(trendlinePath.attr("stroke-width") === 5, "HoverSeriesStyle is applied on trendline");
        $(trendlinePath.node).trigger('mouseout');
        ok(trendlinePath.attr("stroke-width") === 1, "HoverSeriesStyle is removed and seriesStyle is applied on trendline");
        compositechart.remove();
    });

    test("#74417", function () {
    	var compositechart = createCompositechart({
    		axis: {
    			x: { text: "x" },
    			y: [{ text: "y", compass: "east", }, { text: "y2", compass: "west" }]
    		},
    		seriesList: [{
    			type: "bar",
    			label: "Q1",
    			legendEntry: true,
    			yAxis: 0,
    			data: {
    				x: [1, 2, 3, 4, 5],
    				y: [12, 21, 9, 29, 30]
    			}
    		}, {
    			type: "bar",
    			label: "q2",
    			legendentry: true,
    			yaxis: 1,
    			data: {
    				x: [1, 2, 3, 4, 5],
    				y: [15, 25, 19, 35, 40]
    			}
    		}]
    	}),
		axisInfo = compositechart.wijcompositechart("option", "axis");

    	ok(true, "The composite chart with multiple axes was created without errors.");
    	ok(axisInfo.x.compass === "west", "The compass option of the x axis is adjusted to 'west'.");
    	ok(axisInfo.y[0].compass === "north", "The compass option of the first y axis is adjusted to 'north'.");
    	ok(axisInfo.y[1].compass === "south", "The compass option of the second y axis is adjusted to 'south'.");

    	compositechart.remove();
    });

    test("#74319", function () {
        var compositechart = createCompositechart({
            "seriesList": [{
                "data": {
                    "x": [new Date(2014, (01 - 1), 01, 00, 00, 00), new Date(2014, (01 - 1), 07, 00, 00, 00), ],
                    "y": [6.38, -4.54]
                },
                "type": "area",
                "label": "Series 3",
                "legendEntry": true
            }
            ],
            "indicator": {
                "visible": true
            }
        }), mouseDownEveData;

        mouseDownEveData = {
            which: 1,
            clientX: compositechart.offset().left + compositechart.width() / 2,
            clientY: compositechart.offset().top + compositechart.height() / 2
        };

        compositechart.simulate("mouseenter");
        compositechart.simulate("mouseover");
        compositechart.simulate("mousedown", mouseDownEveData);

        ok(true, "Does not throw JS error !");
        compositechart.remove();
    });

    test("#77157, test render column chart", function () {
    	var compositechart = createCompositechart({
    		axis: {
    			x: { text: "x" },
    			y: [{ text: "y", compass: "east" }, { text: "y2", compass: "west" }]
    		},
    		data: {
    			x: ["2012-Q1", "2012-Q2", "2012-Q3", "2012-Q4", "2013-Q1"]
    		},
    		seriesList: [{
    			type: "column",
    			label: "Q1",
    			legendEntry: true,
    			yAxis: 1,
    			data: {
    				y: [12, 21, 9, 29, 30]
    			}
    		}, {
    			type: "trendLine",
    			label: "Q1",
    			legendEntry: true,
    			yAxis: 1,
    			data: {
    				y: [12, 21, 9, 29, 30]
    			}
    		}]
    	});

    	ok(true, "The column chart of composite chart was created without errors, when the data series was added at the secondary axis");
    	compositechart.remove();
    });

    test("#77157, test render bubble chart", function () {
    	var compositechart = createCompositechart({
    		axis: {
    			x: { text: "x" },
    			y: [{ text: "y", compass: "east" }, { text: "y2", compass: "west" }]
    		},
    		data: {
    			x: ["2012-Q1", "2012-Q2", "2012-Q3", "2012-Q4", "2013-Q1", "2013-Q2"]
    		},
    		seriesList: [{
    			type: "bubble",
    			label: "Q1",
    			legendEntry: true,
    			yAxis: 1,
    			data: {
    				y: [58.19, 69.68, 74.48, 70.82, 75.99, 79.40], 
    				y1: [81.07, 104.8, 122.5, 144.7, 162.1, 177.9] 

    			}
    		}, {
    			type: "trendLine",
    			label: "Q1",
    			legendEntry: true,
    			yAxis: 1,
    			data: {
    				y: [58.19, 69.68, 74.48, 70.82, 75.99, 79.40]
    			}
    		}]
    	});

    	ok(true, "The bubble chart of composite chart was created without errors, when the data series was added at the secondary axis");
    	compositechart.remove();
    });

    test("#77157, test render scatter chart", function () {
    	var compositechart = createCompositechart({
    		axis: {
    			x: { text: "x" },
    			y: [{ text: "y", compass: "east" }, { text: "y2", compass: "west" }]
    		},
    		data: {
    			x: ["2012-Q1", "2012-Q2", "2012-Q3", "2012-Q4", "2013-Q1", "2013-Q2"]
    		},
    		seriesList: [{
    			type: "scatter",
    			label: "Q1",
    			legendEntry: true,
    			yAxis: 1,
    			data: {
    				y: [58.19, 69.68, 74.48, 70.82, 75.99, 79.40]
    			}
    		}, {
    			type: "trendLine",
    			label: "Q1",
    			legendEntry: true,
    			yAxis: 1,
    			data: {
    				y: [58.19, 69.68, 74.48, 70.82, 75.99, 79.40]
    			}
    		}]
    	});

    	ok(true, "The scatter chart of composite chart was created without errors, when the data series was added at the secondary axis");
    	compositechart.remove();
    });

    test("#77157, test render candlestick chart", function () {
    	var data = {
    			x: [new Date('12/1/2011'), new Date('12/2/2011'), new Date("12/5/2011"), new Date("12/6/2011"), new Date("12/7/2011")],
    			high: [10, 12, 11, 14, 16],
    			low: [7.5, 8.6, 4.4, 4.2, 8],
    			open: [8, 8.6, 11, 6.2, 13.8],
    			close: [8.6, 11, 6.2, 13.8, 15]
			},
			compositechart = createCompositechart({
    		axis: {
    			x: { text: "x" },
    			y: [{ text: "y", compass: "east" }, { text: "y2", compass: "west" }]
    		},
    		seriesList: [{
    			type: "candlestick",
    			label: "Q1",
    			legendEntry: true,
    			yAxis: 1,
    			data: data
    		}, {
    			type: "trendLine",
    			label: "Q1",
    			legendEntry: true,
    			yAxis: 1,
    			data: {
					x: data.x,
    				y: data.close
    			}
    		}]
    	});

    	ok(true, "The candlestick chart of composite chart was created without errors, when the data series was added at the secondary axis");
    	compositechart.remove();
    });

    test("#75054", function () {
        var compositechart = createCompositechart({
            seriesList: [{
                label: "2012",
                type: "sharedPie",
                data: [
                    {
                        label: "2012_Q1",
                        radius: 100,
                        data: [{
                            label: "Pie 1",
                            data: 46.78,
                        }, {
                            label: "Pie 2",
                            visible: false,
                            data: 23.18,
                        }]
                    }]
            }]
        }), legendIcons;

        legendIcons = compositechart.find("rect.wijchart-legend-icon");

        ok(parseFloat(legendIcons.eq(0).css("opacity")).toFixed(1) == 0.9, "First pie is visible");
        ok(parseFloat(legendIcons.eq(1).css("opacity")).toFixed(1) == 0.3, "Second pie is hidden");

        compositechart.remove();
    });

    test("#74106", function () {
        var compositechart = createCompositechart({
            seriesList: [{
                label: "2012",
                type: "sharedPie",
                data: [
                    {
                        label: "2012_Q1",
                        radius: 100,
                        data: [{
                            label: "Pie 1",
                            data: 46.78,
                        }, {
                            label: "Pie 2",
                            data: 23.18,
                        }]
                    }]
            }],
            disabled: true
        });

        ok(true, "Flag for error showing !");
        compositechart.remove();
    });

    test("#78520", function () {
        var tempXData1, tempXData2,
            compositechart = createCompositechart({
                seriesList: [{
                    type: "candlestick",
                    label: "Candlestick 1",
                    legendEntry: true,
                    data: {
                        x: [new Date('12/1/2011')],
                        high: [7],
                        low: [1],
                        open: [3],
                        close: [5]
                    }
                }],
                "beforePaint": function (e) {
                    tempXData1 = $(e.target).wijcompositechart("option", "seriesList")[0].data.x[0];
                }
            }),
            tempSeriesList = compositechart.wijcompositechart("option", "seriesList");

        ok(tempSeriesList[0].data.x[0].toString() === new Date('12/1/2011').toString(), "The x value has been recovered!");

        compositechart.wijcompositechart("option", "beforePaint", function (e) {
            tempXData2 = $(e.target).wijcompositechart("option", "seriesList")[0].data.x[0];
        });

        compositechart.wijcompositechart("option", "seriesList", tempSeriesList);
        ok(tempXData1 === tempXData2, "Get the same x value after set serieslist option.");

        compositechart.remove();
    });

    test("#83047", function () {
        var data = [{
            x: new Date("12/5/2011"),
            high: 11,
            low: 4.4,
            open: 11,
            close: 6.2
        }, {
            x: new Date("12/6/2011"),
            high: 14,
            low: 4.2,
            open: 6.2,
            close: 13.8
        }, {
            x: new Date("12/7/2011"),
            high: 16,
            low: 8,
            open: 13.8,
            close: 15
        }],
        compositechart = $('<div id="compositechart83047" style="width:600px;height:200px"></div>')
			.appendTo(document.body).wijcompositechart({
			    dataSource: data,
			    axis: {
			        y: {
			            textVisible: false
			        },
			    },
			    seriesList: [{
			        type: "candlestick",
			        data: {
			            x: { bind: "x" },
			            high: { bind: "high" },
			            low: { bind: "low" },
			            open: { bind: "open" },
			            close: { bind: "close" }
			        }
			    }, {
			        isTrendline: true,
			        data: {
			            x: { bind: "x" },
			            y: { bind: "close" }
			        }
			    }]
			}), axisLabels, notEmpty = true;

        axisLabels = compositechart.find("text.wijchart-axis-label");
        ok(axisLabels.length === 3, "Has painted 3 label text !");
        $.each(axisLabels, function (idx, label) {
            if ($(label).text() === "0NaN") {
                notEmpty = false;
                return false;
            }
        });
        ok(notEmpty, "All axis label has correct value !");

        compositechart.remove();
    });

    test("Test for SharedPie issue of legend clicked", function () {
        var data2012_Q1 = [{ label: "Android_2012", data: 104.8 }],
            compositechart = createCompositechart({
            seriesList: [{
                label: "2012",
                type: "sharedPie",
                data: [{
                    datasource: data2012_Q1,
                    data: {
                        label: { bind: "label" },
                        value: { bind: "data" },
                    }
                }, {
                    datasource: data2012_Q1,
                    data: {
                        label: { bind: "label" },
                        value: { bind: "data" },
                    }
                }, {
                    datasource: data2012_Q1,
                    data: {
                        label: { bind: "label" },
                        value: { bind: "data" },
                    }
                }]
            }]
        }), legendIcons;

        legendIcons = compositechart.find("rect.wijchart-legend-icon");
        legendIcons.simulate("click");

        ok(true, "Without error !");
        compositechart.remove();
    });

    test("#85258", function () {
        var data2012_Q1 = [{ label: "Android_2012", data: 104.8 }],
            compositechart = createCompositechart({
                seriesList: [{
                    label: "2012",
                    type: "sharedPie",
                    data: [{
                            radius: 100,
                            legendEntry: true,
                            data: [{
                                label: "SharedPie 1",
                                legendEntry: true,
                                data: 46.78,
                                offset: 15
                            }]
                        }]
                }, {
                    type: "candlestick",
                    data: {
                        x: [new Date("1/1/2014")],
                        high: [22],
                        low: [16],
                        open: [17],
                        close: [18]
                    }
                }]
            });

        ok(compositechart.find("path.pietracker").length > 0, "SharedPie has been rendered with candlestick item. ");
        compositechart.remove();
    });

    test("#89807", function () {
        var compositechart = $('<div id="compositechart" style = "width: 756px; height: 475px"></div>')
			.appendTo(document.body).wijcompositechart({
			    "animation": { enabled: false },
			    horizontal: false,
			    "seriesList": [{
                    type: "column",
                    "data": {
                        "x": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
			            "y": [20, 50, 40, 30, 10, 30, 40, 50, 60, 70, 90, 80, 40, 10, 30, 40, 10, 30]
			        },
			        "label": "Column 1",
			    }],
			    "hint": {
			        "enable": false
			    }
			}), axis = compositechart.data("wijmo-wijcompositechart").axisInfo;

        ok(axis.x.min === 0.5 && axis.x.max === 18.5, "There will not be extra space generated between the YAxis and the bar !");

        compositechart.remove();
    });

    test("#90298", function () {
        var compositechart = $('<div id="compositechart" style = "width: 356px; height: 275px"></div>')
			.appendTo(document.body).wijcompositechart({
			    "axis": {
			        x: { "autoMin": false, "autoMax": false, "min": 0, "max": 3 }
			    },
			    "animation": { enabled: false },
			    "seriesList": [{
			        type: "column",
			        "data": {
			            "x": [1, 2],
			            "y": [20, 50]
			        },
			        "label": "Column 1",
			    }],
			}), bartracker = compositechart.find(".wijbarchart.bartracker").eq(0), positionX;

        positionX = bartracker.position().left;
        compositechart.wijcompositechart("redraw");
        bartracker = compositechart.find(".wijbarchart.bartracker").eq(0);

        ok(positionX === bartracker.position().left, "Columns has not changed after refresh !");
        compositechart.remove();
    });

    test("#92582", function () {
        var compositechart = createCompositechart({
            seriesList: [{
                type: "column",
                data: {
                    x: [new Date("1/1/2014")],
                    y: [3]
                },
            }, {
                type: "candlestick",
                data: {
                    x: [new Date("1/1/2014")],
                    high: [22],
                    low: [16],
                    open: [17],
                    close: [18]
                }
            }]
        }), columnPositionX = compositechart.find(".wijbarchart.bartracker").attr("x");

        compositechart.wijcompositechart("redraw");

        ok(columnPositionX === compositechart.find(".wijbarchart.bartracker").attr("x"), "After compositechart redraw, position of column element has not change. ");
        compositechart.remove();
    });

    test("#93361", function () {
        var compositechart = createCompositechart({
            animation: { enabled: false },
            seriesTransition: { enabled: false },
            seriesList: [{
                type: "column",
                label: "East",
                legendEntry: true,
                data: { x: ['Desktops', 'Notebooks'], y: [3, 6] }
            }, {
                type: "line",
                label: "Steam1",
                legendEntry: true,
                data: {
                    x: ['Desktops', 'Notebooks'],
                    y: [3, 4]
                },
                markers: {
                    visible: true,
                    type: "circle"
                }
            }]
        }),
            lineTracker = compositechart.find(".linetracker"),
            columnTracker = compositechart.find(".bartracker");

        ok(lineTracker.index() > columnTracker.index(), "LineChart trackers are located behind the BarChart trackers !");

        compositechart.remove();
    });

})(jQuery);
