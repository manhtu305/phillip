/*globals test, ok, jQuery, module, document, $*/
/*
* wijcompositechart_core.js create & destroy
*/
"use strict";
function createCompositechart(o) {
	return $('<div id="compositechart" style = "width:300px;height:200px"></div>')
			.appendTo(document.body).wijcompositechart(o);
}

var simpleOptions = {
	seriesList: [{
			type: "column",
			label: "West",
			legendEntry: true,
			data: { x: ['Desktops', 'Notebooks', 'AIO', 'Tablets', 'Phones'], y: [5, 3, 4, 7, 2] }
		}, {
			type: "column",
			label: "Central",
			legendEntry: true,
			data: { x: ['Desktops', 'Notebooks', 'AIO', 'Tablets', 'Phones'], y: [2, 2, 3, 2, 1] }
		}, {
			type: "column",
			label: "East",
			legendEntry: true,
			data: { x: ['Desktops', 'Notebooks', 'AIO', 'Tablets', 'Phones'], y: [3, 4, 4, 2, 5] }
		},
		 {
			type: "pie",
			label: "Steam MacBook Hardware",
			legendEntry: true,
			center: { x: 150, y: 150 },
			radius: 60,
			data: [{
				label: "MacBook Pro",
				legendEntry: true,
				data: 46.78,
				offset: 15
			}, {
				label: "iMac",
				legendEntry: true,
				data: 23.18,
				offset: 0
			}, {
				label: "MacBook",
				legendEntry: true,
				data: 20.25,
				offset: 0
			}]
		}, 
		{
			type: "line",
			label: "Steam1",
			legendEntry: true,
			data: {
				x: ['Desktops', 'Notebooks', 'AIO', 'Tablets', 'Phones'],
				y: [3, 6, 2, 9, 5]
			},
			markers: {
				visible: true,
				type: "circle"
			}
		}, {
			type: "line",
			label: "Steam2",
			legendEntry: true,
			data: {
				x: ['Desktops', 'Notebooks', 'AIO', 'Tablets', 'Phones'],
				y: [1, 3, 4, 7, 2]
			},
			markers: {
				visible: true,
				type: "tri"
			}
		}],
	seriesStyles: [{
		opacity: 0.8, 
		fill: "#112233", 
		stroke: "#123456", 
		"stroke-width": "1"
	}, {
		opacity: 0.8, 
		fill: "#223344", 
		stroke: "#234567", 
		"stroke-width": "1"
	}, {
		opacity: 0.8, 
		fill: "#334455", 
		stroke: "#345678", 
		"stroke-width": "1"
	},{
		opacity: 0.8, 
		fill: "#445566", 
		stroke: "#456789", 
		"stroke-width": "1"
	}, {
		opacity: 0.8, 
		fill: "#556677", 
		stroke: "#56789A", 
		"stroke-width": "1"
	}, {
		opacity: 0.8,
		fill: "#667788", 
		stroke: "#6789AB", 
		"stroke-width": "1"
	}, {
		opacity: 0.8,
		fill: "#778899", 
		stroke: "#789ABC", 
		"stroke-width": "1"
	}, {
		opacity: 0.8,
		fill: "#8899AA", 
		stroke: "#89ABCD", 
		"stroke-width": "1"
	}],
	hint: {
		enable: false
	}
};

function createSimpleCompositechart() {
	return createCompositechart(simpleOptions);
}

(function ($) {

	module("wijcompositechart: core");

	test("create and destroy", function () {
		var compositechart = createCompositechart(simpleOptions);

		ok(compositechart.hasClass('ui-widget wijmo-wijcompositechart'), 
			'create:element class created.');
		
		compositechart.wijcompositechart('destroy');
		ok(!compositechart.hasClass('ui-widget wijmo-wijcompositechart'), 
			'destroy:element class destroy.');
		
		compositechart.remove();
	});

}(jQuery));

