﻿/*globals jQuery, test, ok, module, createSimpleCompositechart, createCompositechart*/
/*
* wijcompositechart_options.js
*/
"use strict";
(function ($) {
	module("wijcompositechart:options");

	test("seriesListEmpty", function() {
		var compositechart = createSimpleCompositechart(),
			seriesList = [],
			bar;
		seriesList.push({ data: { x: ['x1', 'x2', 'x3', 'x4'], y: [5, 10, 12, 20]}, type: "column" });
		seriesList.push({ });
		compositechart.wijcompositechart('option', 'seriesList', seriesList);
		ok(true, 'ok.');
		compositechart.remove();
	});

	test("stacked", function () {
		var compositechart = createSimpleCompositechart(),
			bar;
		compositechart.wijcompositechart('option', 'stacked', true);

		bar = compositechart.wijcompositechart("getElement", "bar", 0);
		ok(bar.chartLabel.attrs.text === "5", 
				'show first section of the stacked bar.');

		bar = compositechart.wijcompositechart("getElement", "bar", 1);
		ok(bar.chartLabel.attrs.text === "7", 
				'show second section of the stacked bar.');

		bar = compositechart.wijcompositechart("getElement", "bar", 2);
		ok(bar.chartLabel.attrs.text === "10", 
				'show last section of the stacked bar.');
		compositechart.remove();
	});

	// the composite chart doesn't support this option.
//	test("is100Percent", function () {
//		var compositechart = createSimpleCompositechart(),
//			bar, yaxis;
//		compositechart.wijcompositechart('option', 'stacked', true);
//		compositechart.wijcompositechart('option', 'is100Percent', true);

//		bar = compositechart.wijcompositechart("getElement", "bar", 0);
//		ok(bar.chartLabel.attrs.text === "5", 
//				'show first percentage of the stacked bar.');

//		bar = compositechart.wijcompositechart("getElement", "bar", 1);
//		ok(bar.chartLabel.attrs.text === "7", 
//				'show second percentage of the stacked bar.');

//		bar = compositechart.wijcompositechart("getElement", "bar", 2);
//		ok(bar.chartLabel.attrs.text === "10", 
//				'show last percentage of the stacked bar.');

//		yaxis = compositechart.wijcompositechart("option", "axis").y;
//		ok(yaxis.min === 0, 'get the minimum value of the y axis.');
//		ok(yaxis.max === 1, 'get the maximum value of the y axis.');
//		//compositechart.remove();
	//});

	//from chartcore.
	test("axis", function () {
		var compositechart = createCompositechart(),
			seriesList = [],
			axis, max, min, unitMajor, unitMinor,
			time, wijcompositechart, formatString;
		seriesList.push({type: "bar", data: { x: ['x1', 'x2', 'x3', 'x4'], y: [5, 10, 12, 20]} });
		compositechart.wijcompositechart('option', 'seriesList', seriesList);
		axis = compositechart.wijcompositechart('option', 'axis');
		max = axis.y.max;
		min = axis.y.min;
		unitMajor = axis.y.unitMajor;
		unitMinor = axis.y.unitMinor;
		ok(max === 20 && min === 0, 'max and min for seriesList1 are ok');
		if ($.browser.msie && $.browser.version < 9) {
			ok(unitMajor === 5 && unitMinor === 2.5, 
				'unitMajor and unitMinor for seriesList1 are ok');
		}
		else {
			ok(unitMajor === 5 && unitMinor === 2.5, 
				'unitMajor and unitMinor for seriesList1 are ok');
		}
		seriesList = [];
		seriesList.push({type: "bar",  data: { x: ['x1', 'x2', 'x3', 'x4'], 
								y: [50, 1008, 2232, 3800]} });
		compositechart.wijcompositechart('option', 'seriesList', seriesList);
		axis = compositechart.wijcompositechart('option', 'axis');
		max = axis.y.max;
		min = axis.y.min;
		unitMajor = axis.y.unitMajor;
		unitMinor = axis.y.unitMinor;
		ok(max === 4000 && min === 0, 'max and min for seriesList2 are ok');

//		if ($.browser.msie && $.browser.version < 9) {
//			ok(unitMajor === 1000 && unitMinor === 500, 
//					'unitMajor and unitMinor for seriesList2 are ok');
//		}
//		else {
//			ok(unitMajor === 500 && unitMinor === 250, 
//					'unitMajor and unitMinor for seriesList2 are ok');
//		}
		ok(unitMajor === 500 && unitMinor === 250, 
				'unitMajor and unitMinor for seriesList2 are ok');

		seriesList = [];
		seriesList.push({type: "line",  data: { x: ['x1', 'x2', 'x3', 'x4'], 
								y: [2150, 4008, 6232, 9500]} });
		compositechart.wijcompositechart('option', 'seriesList', seriesList);
		axis = compositechart.wijcompositechart('option', 'axis');
		max = axis.y.max;
		min = axis.y.min;
		unitMajor = axis.y.unitMajor;
		unitMinor = axis.y.unitMinor;
		ok(max === 10000 && min === 0, 'max and min for seriesList3 are ok');
		if ($.browser.msie || $.browser.mozilla) {
			ok(unitMajor === 1000 && unitMinor === 500, 
				'unitMajor and unitMinor for seriesList3 are ok');
		}
		else {
			ok(unitMajor === 2000 && unitMinor === 1000, 
				'unitMajor and unitMinor for seriesList3 are ok');
		}

		seriesList = [];
		seriesList.push({type: "line",  data: { x: ['1', '2', '3', '4'], y: [50, 1008, 2232, 3800]} });
		compositechart.wijcompositechart('option', 'seriesList', seriesList);
		axis = compositechart.wijcompositechart('option', 'axis');
		max = axis.x.max;
		min = axis.x.min;
		unitMajor = axis.x.unitMajor;
		unitMinor = axis.x.unitMinor;
		ok(max === 3 && min === 0, 'max and min for seriesList4 are ok');
		ok(unitMajor === 1 && unitMinor === 0.5, 
				'unitMajor and unitMinor for seriesList4 are ok');

		seriesList = [];
		time = [new Date(2009, 3, 5), new Date(2009, 4, 12), 
				new Date(2009, 5, 20), new Date(2009, 8, 30)];
		seriesList.push({ data: { x: time, y: [2150, 4008, 6232, 9500]} });
		compositechart.wijcompositechart('option', 'seriesList', seriesList);
		wijcompositechart = compositechart.data("wijmo-wijcompositechart");
		max = wijcompositechart.options.axis.x.max;
		max = $.fromOADate(max);
		min = wijcompositechart.options.axis.x.min;
		min = $.fromOADate(min);
		formatString = wijcompositechart.options.axis.x.annoFormatString;
		if(formatString.length === 0) {
			formatString = wijcompositechart.axisInfo.x.annoFormatString;
		}
		max = Globalize.format(max, formatString);
		min = Globalize.format(min, formatString);
		ok(max === 'Oct' && min === 'Apr', 'max and min for seriesList5 are ok');

		compositechart.remove();
	});

	test("chartLabelFormatter && chartLabelFormatString", function () {
	    var compositechart = createCompositechart({
	        seriesList: [{
	            type: "bar",
	            data: {
	                x: [1, 2],
	                y: [7, 8]
	            }
	        }],
	        showChartLabels: true,
	        animation: {
	            enabled: false
	        }
	    }), chartLabel0;

	    chartLabel0 = compositechart.data("wijmo-wijcompositechart").chartElement.data("fields").chartElements.chartLabels[0].attr("text");
	    ok(chartLabel0 === "7", "the first chart element label is '7'");

	    compositechart.wijcompositechart("option", "chartLabelFormatString", "n2");
	    chartLabel0 = compositechart.data("wijmo-wijcompositechart").chartElement.data("fields").chartElements.chartLabels[0].attr("text");
	    ok(chartLabel0 === "7.00", "the first chart element label is updated according to chart label format string.");

	    compositechart.wijcompositechart("option", "chartLabelFormatter", function () {
	        return "mycustomzied:" + this.value;
	    });
	    chartLabel0 = compositechart.data("wijmo-wijcompositechart").chartElement.data("fields").chartElements.chartLabels[0].attr("text");
	    ok(chartLabel0 === "mycustomzied:7", "the first chart element label is updated according to chartLabelFormatter.");

	    compositechart.remove();
	});
}(jQuery));