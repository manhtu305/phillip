﻿/*globals start, stop, test, ok, module, jQuery, window
*/
/*
* wijtouchutil_events.js
*/
"use strict";
(function ($) {
	var testContainer, 
		eventTestHash = {
			wijmouseover: false,
			wijmousemove: false,
			wijmousedown: false,
			wijmouseup: false,
			wijmouseout: false,
			wijclick: false
		}, k;

	module("wijtouchutil: events");

	window.setTimeout(function () {
		if(window.navigator.msPointerEnabled){
			$("#logcontainer").html("Mouse pointer emulation is not supported for Metro mode!");
			start();
			return;
		}
			for (k in eventTestHash) {
				if (!eventTestHash[k]) {
					ok(false, "Event " + k + " not called.");
				}
			}
			start();
	}, 2000);
	test("wijutil events", function () {

		testContainer = $("#testcontainer");
		
		
		testContainer.bind("wijmousedown", testMouseCallback);
		testContainer.bind("wijmouseover", testMouseCallback);
		testContainer.bind("wijmousemove", testMouseCallback);
		/*if(window.navigator.msPointerEnabled){
			testContainer.bind("MSPointerDown", testMouseCallback);
			testContainer.bind("MSPointerMove", testMouseCallback);
			testContainer.bind("MSPointerUp", testMouseCallback);
			testContainer.bind("MSPointerOver", testMouseCallback);
			testContainer.bind("MSPointerOut", testMouseCallback);
		}*/
		testContainer.bind("wijmouseout", testMouseCallback);
		testContainer.bind("wijmouseup", testMouseCallback);
		testContainer.bind("wijmousecancel", testMouseCallback);
		testContainer.bind("wijscrollstart", testMouseCallback);
		testContainer.bind("wijscrollstop", testMouseCallback);

		testContainer.bind("wijgesturestart", testMouseCallback);
		testContainer.bind("wijgesturechange", testMouseCallback);
		testContainer.bind("wijgestureend", testMouseCallback);

		testContainer.bind("wijclick", testMouseCallback);


		testContainer.simulate("mousedown");
		testContainer.simulate("mouseover");
		testContainer.simulate("mousemove");		
		testContainer.simulate("mouseout");
		testContainer.simulate("mouseup");
		testContainer.simulate("click");
		stop();
	});
	stop();
	function testMouseCallback(e) {
		ok(true, e.type + " called.");	
		testMajorEventFields(e);
		eventTestHash[e.type] = true;		
	}
	function testMajorEventFields(e){
		var logLabel = $("#logcontainer").find("." + e.type + "label"), 
			now = new Date(), sProps;
		if(logLabel.length < 1){
			logLabel= $("<span class=\"loglabel "+ e.type + "label\"></span>");
			$("#logcontainer").append(logLabel);
			 
		}
		if(e.type.indexOf("gesture") != -1){			
			sProps = "scale=" + e.scale + "x; ";
			sProps += "rotation=" + e.rotation + "deg.; ";
			if(window.navigator.msPointerEnabled){
				sProps += " metro only events: ";
				sProps += "velocityX/Y=" + e.velocityX + ", " + e.velocityY + "; ";
				sProps += "translationX/Y: " + e.translationX + "px, " + e.translationY + "px; ";
			}
		} else {
			sProps ="pageX=" + e.pageX + "; pageY=" + e.pageY + "; ";
		}
//

		logLabel.html(now.toLocaleTimeString() + "." + now.getMilliseconds() + " " + 
e.type + "("+e.nativeType+") " + sProps);
	}


}(jQuery));