﻿function createUpload(o) {
	var div = $("<div>").prependTo("form");
	div.wijupload(o || {});
	return div;
}
(function ($) {
	module("wijupload: core");
	test("create and destroy", function() {
		var upload = createUpload();
		var widget=upload.data("wijupload");
		ok(upload.hasClass("wijmo-wijupload"),"element add the wijupload css class.");

		upload.wijupload("destroy");
		ok(!upload.hasClass("wijmo-wijupload"),"The upload css class is removed.");
		ok(upload.children().length === 0, "The upload's children is removed.");
		ok(!upload.data("wijmo-wijupload"), "upload instance has removed.");
		upload.remove();
	});
})(jQuery);
