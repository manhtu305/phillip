﻿commonWidgetTests('wijupload', {
	defaults: {
		/// <summary>
		/// Selector option for auto self initialization. 
		///	This option is internal.
		/// </summary>
		initSelector: ":jqmData(role='wijupload')",
		wijCSS: $.wijmo.wijCSS,
		disabled: false,
		action: "",
		autoSubmit: false,
		change: null,
		upload: null,
		totalUpload: null,
		progress: null,
		totalProgress: null,
		complete: null,
		totalComplete: null,
		maximumFiles: 0,
		/// <summary>
		/// Determines whether support multiple selection. 
		/// Default: false.
		/// Type: Boolean.
		/// Code Example: 
		///		$(".selector").wijupload("multiple", true)
		/// </summary>
		multiple: true,
		/// <summary>
		/// Specifies the accept attribute of upload. 
		/// Default: "".
		/// Type: String.
		/// Code Example: 
		///		$(".selector").wijupload("accept", "image/*")
		/// </summary>
		accept: "",
		wijCSS:$.extend({
		    iconCircleArrowN: "ui-icon-circle-arrow-n",
		    iconCancel: "ui-icon-cancel"
		}, $.wijmo.wijCSS),
		localization: {},
		enableSWFUploadOnIE: false,
		enableSWFUpload: false,
		swfUploadOptions: {},
        
		handleRequest: null
	}
});
