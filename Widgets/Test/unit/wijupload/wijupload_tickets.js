(function ($) {
	module("wijupload: tickets");
	test("#42233", function () {
		var upload = createUpload(), uploadBtn = upload.find("a");

		ok($(uploadBtn).attr("href") === "javascript:void(0);");
		upload.remove();
	});

	test("#49089", function () {
		//create an upload and use destory to destroy it, and then recreate the upload.
		upload = createUpload();
		ok(upload.hasClass("wijmo-wijupload"), "element add the wijupload css class.");
		upload.wijupload("destroy");
		ok(!upload.hasClass("wijmo-wijupload"), "The upload css class is removed.");
		upload.wijupload();
		ok(upload.hasClass("wijmo-wijupload"), "uplaod widget create again.");
		upload.remove();
	})
})(jQuery);