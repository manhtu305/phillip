﻿/*
* wijupload_options.js
*/

(function ($) {
    module("wijupload: options");

    test("disabled", function () {
        upload = createUpload();
        ok($("div.ui-state-disabled").length === 0, "the disabled div has not appended.");
        upload.wijupload("option", "disabled", true);
        ok($("div.ui-state-disabled").length === 2, "the disabled div has appended.");
        upload.wijupload("option", "disabled", false);
        ok($("div.ui-state-disabled").length === 0, "the disabled div has not appended.");
        upload.remove();
    });
    
})(jQuery);
