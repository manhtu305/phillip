﻿/*globals test, ok, jQuery, module, document, $*/
/*
* wijbarchart_core.js create & destroy
*/
"use strict";
function createRadialgauge(o) {
	return $('<div id="radialgauge" style = "width:600px;height:300px"></div>')
			.appendTo(document.body).wijradialgauge(o);
}

var simpleOptions = {
	
};

function createSimpleRadialGauge() {
	return createRadialgauge(simpleOptions);
}

(function ($) {

	module("wijradialgauge: core");

	test("create and destroy", function () {
		var gauge = createSimpleRadialGauge();

		ok(gauge.hasClass('ui-widget wijmo-wijradialgauge'),
			'create:element class created.');

		gauge.wijradialgauge('destroy');
		ok(!gauge.hasClass('ui-widget wijmo-wijradialgauge'),
			'destroy:element class destroy.');

		gauge.remove();
	});

} (jQuery));

