﻿/*globals commonWidgetTests*/
/*
* wijradialgauge_defaults.js
*/
"use strict";

// override the QUnit.jsDump.typeof method.  
//If an object which has a length property will be parsed as an array type using original method.
// else if (typeof obj === "object" && typeof obj.length === "number" && obj.length >= 0) {
//		type = "array";
//	}
QUnit.jsDump.typeOf = function (obj) {
	var type;
	if (obj === null) {
		type = "null";
	} else if (typeof obj === "undefined") {
		type = "undefined";
	} else if (QUnit.is("RegExp", obj)) {
		type = "regexp";
	} else if (QUnit.is("Date", obj)) {
		type = "date";
	} else if (QUnit.is("Function", obj)) {
		type = "function";
	} else if (obj.setInterval && obj.document && !obj.nodeType) {
		type = "window";
	} else if (obj.nodeType === 9) {
		type = "document";
	} else if (obj.nodeType) {
		type = "node";
	} else if ($.isArray(obj)) {
		type = "array";
	} else {
		type = typeof obj;
	}
	return type;
}

commonWidgetTests('wijradialgauge', {
	defaults: {
		disabled: false,
		create: null,
		radius: "auto",
		startAngle: 0,
		sweepAngle: 180,
		value: 0,
		max: 100,
		min: 0,
		autoResize: true,
		isInverted: false,
		initSelector: ":jqmData(role='wijradialgauge')",
		wijCSS: $.extend({
			radialGauge: "wijmo-wijradialgauge",
			radialGaugeLabel: "wijmo-wijradialgauge-label",
			radialGaugeMarker: "wijmo-wijradialgauge-mark",
			radialGaugeFace: "wijmo-wijradialgauge-face",
			radialGaugePointer: "wijmo-wijradialgauge-pointer",
			radialGaugeCap: "wijmo-wijradialgauge-cap",
			radialGaugeRange: "wijmo-wijradialgauge-range"
		}, $.wijmo.wijCSS),
		width: 600,
		height: 400,
		tickMajor: {
			position: "inside",
			style: {
				fill: "#1E395B",
				stroke: "#1E395B",
				"stroke-width": 1
			},
			factor: 2,
			visible: true,
			marker: "rect",
			offset: 27,
			interval: 10
		},
		tickMinor: {
			position: "inside",
			style: {
				fill: "#1E395B",
				stroke: "none"
			},
			factor: 1,
			visible: true,
			marker: "rect",
			offset: 30,
			interval: 5
		},

		islogarithmic: false,
		logarithmicBase: 10,
		labels: {
			format: "",
			style: {
				fill: "#1E395B",
				"font-size": 12,
				"font-weight": "800"
			},
			visible: true,
			offset: 30
		},
		animation: {
			enabled: true,
			duration: 2000,
			easing: ">"
		},
		face: {
			style: {
				fill: "r(0.9, 0.60)#FFFFFF-#D9E3F0",
				stroke: "#7BA0CC",
				"stroke-width": 4
			},
			template: null
		},
		marginTop: 0,
		marginRight: 0,
		marginBottom: 0,
		marginLeft: 0,
		ranges: [],
		origin: {
			x: 0.5,
			y: 0.5
		},
		cap: {
			radius: 15,
			style: {
				fill: "#1E395B",
				stroke: "#1E395B"
			},
			behindPointer: false,
			visible: true,
			template: null
		},
		pointer: {
			length: 0.8,
			style: {
				fill: "#1E395B",
				stroke: "#1E395B"
			},
			width: 8,
			offset: 0.15,
			shape: "tri",
			visible: true,
			template: null
		},
		beforeValueChanged: null,
		valueChanged: null,
		painted: null
	}
});