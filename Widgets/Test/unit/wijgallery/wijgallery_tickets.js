/*
 * wijgallery_tickets.js
 */

(function ($) {
    module("wijgallery: tickets");

    test("#37625", function () {
        var htmlText = '', $widget, thumbs;
        htmlText += '<div id="wijgallery1" class="ui-widget-content ui-corner-all" style="padding: 15px 15px 15px 15px;">';
        htmlText += '    <ul class="">';
        htmlText += '        <li class=""><a href="images/art/zeka1.jpg">';
        htmlText += '            <img alt="1" src="images/art/small/zeka1.jpg" title="Word Caption 1" />';
        htmlText += '        </a></li>';
        htmlText += '        <li class=""><a href="images/art/zeka2.jpg">';
        htmlText += '            <img alt="2" src="images/art/small/zeka2.jpg" title="Word Caption 2" />';
        htmlText += '        </a></li>';
        htmlText += '        <li class=""><a href="images/art/zeka3.jpg">';
        htmlText += '            <img alt="3" src="images/art/small/zeka3.jpg" title="Word Caption 3" />';
        htmlText += '        </a></li>';
        htmlText += '        <li class=""><a href="images/art/zeka4.jpg">';
        htmlText += '            <img alt="4" src="images/art/small/zeka4.jpg" title="Word Caption 4" />';
        htmlText += '        </a></li>';
        htmlText += '        <li class=""><a href="images/art/zeka5.jpg">';
        htmlText += '            <img alt="5" src="images/art/small/zeka5.jpg" title="Word Caption 5" />';
        htmlText += '        </a></li>';
        htmlText += '        <li class=""><a href="images/art/zeka6.jpg">';
        htmlText += '            <img alt="6" src="images/art/small/zeka6.jpg" title="Word Caption 6" />';
        htmlText += '        </a></li>        ';
        htmlText += '    </ul>';
        htmlText += '</div>';

        $widget = $(htmlText).appendTo(document.body).wijgallery({ scrollWithSelection: true });
        thumbs = $widget.find(".wijmo-wijcarousel-list");
        $widget.wijgallery("show", 1);

        setTimeout(function () {
            $widget.wijgallery("show", 0);
            setTimeout(function () {
                $widget.wijgallery("show", 1);
                setTimeout(function () {
                    ok($(thumbs).css("left") === "0px", "The thumbnails doesn't scroll to the blank area.");
                    $widget.remove();
                    start();
                }, 1500);
            }, 1500);
        }, 1500);
        stop();
    });

    test("test disable wijgallery control for mouse over event.", function () {
        var $widget = createGallery({
            disabled: true
        }),
            previousButton = $widget.find(".wijmo-wijgallery-button-previous"),
            nextButton = $widget.find(".wijmo-wijgallery-button-next"),
            timerBar = $widget.find(".wijmo-wijgallery-timerbar"),
            playButton = $widget.find(".wijmo-wijgallery-button"),
            counter = $widget.find(".wijmo-wijgallery-counter"),
            previousFrame = $widget.find(".wijmo-wijgallery-frame-previous"),
            nextFrame = $widget.find(".wijmo-wijgallery-frame-next");

        nextFrame.simulate("mouseover");
        setTimeout(function () {
            ok(nextButton.is(":hidden"), "The next button won't show, when mouse over the disable wijgallery control.");
            ok(timerBar.is(":hidden"), "The timer bar won't show, when mouse over the disable wijgallery control.");
            ok(playButton.is(":hidden"), "The play button won't show, when mouse over the disable wijgallery control.");
            ok(counter.is(":hidden"), "The counter won't show, when mouse over the disable wijgallery control.");
            nextFrame.simulate("mouseout");
            previousFrame.simulate("mouseover");
            setTimeout(function () {
                ok(previousButton.is(":hidden"), "The previous button won't show, when mouse over the disable wijgallery control.");
                ok(timerBar.is(":hidden"), "The timer bar won't show, when mouse over the disable wijgallery control.");
                ok(playButton.is(":hidden"), "The play button won't show, when mouse over the disable wijgallery control.");
                ok(counter.is(":hidden"), "The counter won't show, when mouse over the disable wijgallery control.");
                $widget.remove();
                start();
            }, 400);
        }, 400);
        stop();
    });

    test("test disable wijgallery control for clicking navigation button.", function () {
        var $widget = createGallery({
            disabled: true,
            showControlsOnHover: false
        }),
            previousButton = $widget.find(".wijmo-wijgallery-button-previous"),
            nextButton = $widget.find(".wijmo-wijgallery-button-next");

        nextButton.simulate("click");
        setTimeout(function () {
            imageSrc = getImageSrc($widget);
            ok(imageSrc === "images/art/zeka1.jpg", "When the click next button of the disable wijgallery, it won't goto next image");
            $widget.wijgallery("show", 1);
            setTimeout(function () {
                previousButton.simulate("click");
                setTimeout(function () {
                    imageSrc = getImageSrc($widget);
                    ok(imageSrc === "images/art/zeka2.jpg", "When the click previous button of the disable wijgallery, it won't goto previous image");
                    $widget.remove();
                    start();
                }, 1200)
            }, 1000);
        }, 1000);
        stop();
    });

    test("test disable wijgallery control for clicking navigation frame link.", function () {
        var $widget = createGallery({
            disabled: true
        }),
            previousLink = $widget.find(".wijmo-wijgallery-frame-previous .wijmo-wijgallery-frame-link"),
            nextLink = $widget.find(".wijmo-wijgallery-frame-next .wijmo-wijgallery-frame-link");

        setTimeout(function () {
            nextLink.simulate("click");
            setTimeout(function () {
                imageSrc = getImageSrc($widget);
                ok(imageSrc === "images/art/zeka1.jpg", "When the click next frame of the disable wijgallery, it won't goto next image");
                $widget.wijgallery("show", 1);
                setTimeout(function () {
                    previousLink.simulate("click");
                    setTimeout(function () {
                        imageSrc = getImageSrc($widget);
                        ok(imageSrc === "images/art/zeka2.jpg", "When the click previous frame of the disable wijgallery, it won't goto previous image");
                        $widget.remove();
                        start();
                    }, 1200)
                }, 1000);
            }, 1000);
        }, 400);
        stop();
    });

    test("test disable wijgallery control for clicking play button.", function () {
        var $widget = createGallery({
            disabled: true,
            showControlsOnHover: false,
            interval: 800
        }),
            playButton = $widget.find(".wijmo-wijgallery-button");

        playButton.simulate("click");
        setTimeout(function () {
            playButton.simulate("click");
            imageSrc = getImageSrc($widget);
            ok(imageSrc === "images/art/zeka1.jpg", "When the click next button of the disable wijgallery, it won't goto next image");
            $widget.remove();
            start();
        }, 1000);
        stop();
    });

    test("test disable wijgallery control for clicking thumbnail item.", function () {
        var $widget = createGallery({
            disabled: true,
            showControlsOnHover: false
        }),
            thumbs = $widget.find(".wijmo-wijcarousel-list");

        $(thumbs.children("li")[1]).simulate("click");
        setTimeout(function () {
            imageSrc = getImageSrc($widget);
            ok(imageSrc === "images/art/zeka1.jpg", "When the click next button of the disable wijgallery, it won't goto next image");
            $widget.remove();
            start();
        }, 1000);
        stop();
    });

    test("test disable wijgallery control for clicking pager button", function () {
        var $widget = createGallery({
            disabled: true,
            showPager: true
        }),
            pager = $widget.find(".wijmo-wijpager");

        $(pager.find("ul > li")[1]).simulate("click");
        setTimeout(function () {
            imageSrc = getImageSrc($widget);
            ok(imageSrc === "images/art/zeka1.jpg", "When the click next button of the disable wijgallery, it won't goto next image");
            $widget.remove();
            start();
        }, 1000);
        stop();
    });

    test("Test Remove method without index parameter", function () {
        var $widget = createGallery(),
            thumbCount = $widget.wijgallery("count"),
            previousButton = $widget.find(".wijmo-wijgallery-button-previous"),
            nextButton = $widget.find(".wijmo-wijgallery-button-next"),
            playPauseButton = $widget.find(".wijmo-wijgallery-button"),
            current = $widget.find(".wijmo-wijgallery-content .wijmo-wijgallery-current"),
            counter;

        while (thumbCount > 0) {
            $widget.wijgallery("remove");
            thumbCount--;
        }

        ok(current.is(":hidden") || current.hasClass("ui-helper-hidden-accessible"), "No image shows, if all the items are removed.");
        ok($widget.wijgallery("count") === 0, "All the items are removed correctly.");
        ok(previousButton.hasClass("ui-state-disabled"), "The previous button is set to disabled, if all the items are removed.");
        ok(nextButton.hasClass("ui-state-disabled"), "The next button is set to disabled, if all the items are removed.");
        ok(playPauseButton.hasClass("ui-state-disabled"), "The play/pause button is set to disabled, if all the items are removed.");
        counter = $widget.find(".wijmo-wijgallery-counter span");
        ok(counter.text() === "0 of 0", "The text of the counter is updated to '0 of 0', if all the items are removed.");

        $widget.remove();
    });

    test("Test Remove method with nagative number", function () {
        var $widget = createGallery(),
            counter;

        $widget.wijgallery("remove", -1);

        ok($widget.wijgallery("count") === 4, "The item is removed correctly.");
        setTimeout(function () {
            counter = $widget.find(".wijmo-wijgallery-counter span");
            ok($(counter).text() === "1 of 4", "The text of the counter is updated to '1 of 4', if all the item is removed.");
            $widget.remove();
            start();
        }, 500);
        stop();
    });

    test("Test Remove method with the number greater than item count", function () {
        var $widget = createGallery({
            transitions: { animated: false }
        }),
            nextButton = $widget.find(".wijmo-wijgallery-button-next"),
            playPauseButton = $widget.find(".wijmo-wijgallery-button");

        $widget.wijgallery("show", 3);
        setTimeout(function () {
            $widget.wijgallery("remove", 6);
            ok($widget.wijgallery("count") === 4, "The item is removed correctly.");
            ok(nextButton.hasClass("ui-state-disabled"), "The next button is set to disabled, if the last item is removed.");
            ok(playPauseButton.hasClass("ui-state-disabled"), "The play/pause button is set to disabled, if the last item is removed.");
            $widget.remove();
            start();
        }, 500);
        stop();
    });

    test("Test add method without index parameter", function () {
        var $widget = createGallery({
            transitions: { animated: false }
        }),
            nextButton = $widget.find(".wijmo-wijgallery-button-next"),
            playPauseButton = $widget.find(".wijmo-wijgallery-button"),
            counter;

        $widget.wijgallery("show", 4);
        setTimeout(function () {
            ok(nextButton.hasClass("ui-state-disabled"), "The next button is set to disabled, if the last item shows.");
            ok(playPauseButton.hasClass("ui-state-disabled"), "The play/pause button is set to disabled, if the last item shows.");
            $widget.wijgallery("add", '<li><a href="images\art\zeka6.jpg"><img src="images\art\zeka6.jpg" /></a></li>');
            ok($widget.wijgallery("count") === 6, "The item is added correctly.");
            ok(!nextButton.hasClass("ui-state-disabled"), "The next button is reset to enabled, if add new item.");
            ok(!playPauseButton.hasClass("ui-state-disabled"), "The play/pause button is reset to enabled, if add new item.");
            counter = $widget.find(".wijmo-wijgallery-counter span");
            ok(counter.text() === "5 of 6", "The text of the counter is updated to '5 of 6', if add new item.");
            $widget.remove();
            start();
        }, 500);
        stop();
    });

    if ($.browser.msie && parseInt($.browser.version) > 9) {
        test("Test the navigation buttons show and hide when mouseover in IE 10", function () {
            var $widget = createGallery({ showContorlsOnHover: true }),
                prevFrame = $widget.find(".wijmo-wijgallery-frame .wijmo-wijgallery-frame-previous"),
                nextFrame = $widget.find(".wijmo-wijgallery-frame .wijmo-wijgallery-frame-next"),
                previousButton = $widget.find(".wijmo-wijgallery-button-previous"),
                nextButton = $widget.find(".wijmo-wijgallery-button-next"),
                frameEle;

            ok($(previousButton).css("display") === "none", "The previous button is invisible by default.");
            ok($(nextButton).css("display") === "none", "The next button is invisible by default.");
            prevFrame.simulate("mouseover");
            ok($(previousButton).css("display") === "inline", "The previous button is visible after mouseenter previous frame.");
            prevFrame.simulate("mouseout");
            nextFrame.simulate("mouseover");
            setTimeout(function () {
                ok($(previousButton).css("display") === "none", "The previous button is invisible after mouseout previous frame.");
                ok($(nextButton).css("display") === "inline", "The next button is visible after mouseenter next frame.");
                $widget.remove();
                start();
            }, 1000)
            stop();
        });

        test("Test click the navigation frame works in IE 10", function () {
            var $widget = createGallery({ showContorlsOnHover: true }),
                frame = $widget.find(".wijmo-wijgallery-frame"),
                prevFrame = $widget.find(".wijmo-wijgallery-frame .wijmo-wijgallery-frame-previous .wijmo-wijgallery-frame-link"),
                nextFrame = $widget.find(".wijmo-wijgallery-frame .wijmo-wijgallery-frame-next .wijmo-wijgallery-frame-link"),
                clickEvent = jQuery.Event("click"),
                imageSrc;

            setTimeout(function () {
                $(nextFrame).trigger(clickEvent);
                setTimeout(function () {
                    imageSrc = getImageSrc($widget);
                    ok(imageSrc === "images/art/zeka2.jpg", "If click the area of next frame, next sccuess!");
                    $(prevFrame).trigger(clickEvent);
                    setTimeout(function () {
                        imageSrc = getImageSrc($widget);
                        ok(imageSrc === "images/art/zeka1.jpg", "If click the area of previous frame, previous sccuess!");
                        $widget.remove();
                        start();
                    }, 1000);
                }, 1000);
            }, 500);
            stop();
        });

        test("#41915", function () {
            var $widget = createGallery({ showContorlsOnHover: false, mode: "iframe" }),
                frame = $widget.find(".wijmo-wijgallery-frame");

            frame.simulate("mousemove", { clientX: 10, clientY: 10 });
            ok("When mouse move in the gallery frame area, the wijgalley won't throw exception.");
            $widget.remove();
        });

        test("test disable wijgallery control for mouse over event in IE 10", function () {
            var $widget = createGallery({
                disabled: true,
                showContorlsOnHover: true
            }),
                frame = $widget.find(".wijmo-wijgallery-frame"),
                previousButton = $widget.find(".wijmo-wijgallery-button-previous"),
                nextButton = $widget.find(".wijmo-wijgallery-button-next"),
                timerBar = $widget.find(".wijmo-wijgallery-timerbar"),
                playButton = $widget.find(".wijmo-wijgallery-button"),
                counter = $widget.find(".wijmo-wijgallery-counter");

            $(frame).simulate("mousemove", { clientX: frame.offset().left + 20, clientY: frame.offset().top + 20 });
            setTimeout(function () {
                ok(nextButton.is(":hidden"), "The next button won't show, when mouse over the disable wijgallery control.");
                ok(timerBar.is(":hidden"), "The timer bar won't show, when mouse over the disable wijgallery control.");
                ok(playButton.is(":hidden"), "The play button won't show, when mouse over the disable wijgallery control.");
                ok(counter.is(":hidden"), "The counter won't show, when mouse over the disable wijgallery control.");
                $(frame).simulate("mousemove", { clientX: frame.offset().left + 420, clientY: frame.offset().top + 20 });
                setTimeout(function () {
                    ok(previousButton.is(":hidden"), "The previous button won't show, when mouse over the disable wijgallery control.");
                    ok(timerBar.is(":hidden"), "The timer bar won't show, when mouse over the disable wijgallery control.");
                    ok(playButton.is(":hidden"), "The play button won't show, when mouse over the disable wijgallery control.");
                    ok(counter.is(":hidden"), "The counter won't show, when mouse over the disable wijgallery control.");
                    $widget.remove();
                    start();
                }, 400);
            }, 400);
            stop();
        });

        test("Test disable wijgallery control for clicking the navigation frame in IE 10", function () {
            var $widget = createGallery({
                disabled: true,
                showContorlsOnHover: true
            }),
                frame = $widget.find(".wijmo-wijgallery-frame"),
                clickEvent = jQuery.Event("click"),
                imageSrc;

            setTimeout(function () {
                clickEvent.clientX = frame.offset().left + 420;
                clickEvent.clientY = frame.offset().top + 20;
                $(frame).trigger(clickEvent);
                setTimeout(function () {
                    imageSrc = getImageSrc($widget);
                    ok(imageSrc === "images/art/zeka1.jpg", "When the click next frame of the disable wijgallery, it won't goto next image");
                    $widget.wijgallery("show", 1);
                    clickEvent.clientX = frame.offset().left + 20;
                    $(frame).trigger(clickEvent);
                    setTimeout(function () {
                        imageSrc = getImageSrc($widget);
                        ok(imageSrc === "images/art/zeka2.jpg", "When the click previous frame of the disable wijgallery, it won't goto previous image");
                        $widget.remove();
                        start();
                    }, 1000);
                }, 1000);
            }, 500)
            stop();
        });
    };

    test("#68135", function () {
        var htmlText = "<div id='wijgallery'>" +
            "<ul>" +
            "<li><a href='http://cdn.wijmo.com/movies/skip.swf'>" +
            "<img src='http://cdn.wijmo.com/images/happygirl.png' title='Happy Girl' alt='Happy Girl' /></a>" +
            "</li>" +
            "<li><a href='http://cdn.wijmo.com/movies/caveman.swf'>" +
            "<img src='http://cdn.wijmo.com/images/caveman.png' title='Cave Man' alt='Cave Man' /></a>" +
            "</li>" +
            "</ul>" +
            "</div>",
            eventTriggerCnt = 0,
            $widget = $(htmlText).appendTo("body").wijgallery({
                mode: "swf",
                autoPlay: true,
                interval: 200,
                afterTransition: function (e, args) {
                    eventTriggerCnt++;
                    if (eventTriggerCnt === 2) {
                        ok(true, "The autoPlay option works fine for swf mode, the gallery has gone to the second flash.");
                        $widget.remove();
                        start();
                    }
                }
            });
        stop();
    });

    test("#69908", function () {
        var $widget = createGallery({
            showPager: true
        }),
            pagerList, pageButton;

        setTimeout(function () {
            pagerList = $widget.find(".wijmo-wijpager ul");
            pageButton = pagerList.children("li:first");

            ok(pagerList.height() === pageButton.outerHeight(), "All the page buttons are in the same line.");
            $widget.remove();
            start();
        }, 200);
        stop();
    });

    test("#84220", function () {
        var $widget = createGallery({
            showControlsOnHover: false,
            thumbsDisplay: 4,
            thumbsLength: 100,
            width: 200,
            height: 200,
            showCounter: false,
            showThumbnailCaptions: false,
            showCaption: false,
        });

        ok($(".wijmo-wijcarousel-text").length === 0, "thumb caption not shown.");
        $widget.wijgallery("option", "showThumbnailCaptions", true);
        ok($(".wijmo-wijcarousel-text").length > 0 && $(".wijmo-wijcarousel-text").css("display") !== "none", "thumb caption shown.");
        $widget.wijgallery("option", "showThumbnailCaptions", false);
        ok($(".wijmo-wijcarousel-text").length === 0, "thumb caption not shown.");
        $widget.remove();
    });

    test("#89506", function () {
        var imageSrc, $widget = createGallery({
            showControlsOnHover: false,
            showTimer: true,
            interval: 200
        });

        window.setTimeout(function () {
            $widget.wijgallery("play");

            window.setTimeout(function () {
                ok($widget.find(".ui-icon-pause").length === 1);
                $widget.wijgallery("option", "disabled", true);
                window.setTimeout(function () {
                    ok($widget.find(".ui-icon-play").length === 1);
                    imageSrc = getImageSrc($widget);
                    ok(imageSrc === "images/art/zeka1.jpg");
                    $widget.remove();
                    start();
                }, 400);
            }, 100);
        }, 200);
        stop();
    });

    test("#78433", function () {
        var $widget = createGallery({
            showControlsOnHover: true
        });
        $widget.data("wijmo-wijgallery").frame.mouseenter();
        setTimeout(function () {
            ok($widget.data("wijmo-wijgallery").timer.css("display") !== "none", "timer is shown on hover.");
            start();
        }, 500);
        stop();
    });

})(jQuery);