﻿
var htmlEmpty = '<div id="wijgallery2"></div>';

var htmlTextWithLink = '';
htmlTextWithLink += '<div id="wijgallery1" class="ui-widget-content ui-corner-all" style="padding: 15px 15px 15px 15px;">';
htmlTextWithLink += '    <ul class="">';
htmlTextWithLink += '        <li class=""><a href="images/art/zeka1.jpg">';
htmlTextWithLink += '            <img alt="1" src="images/art/small/zeka1.jpg" title="Word Caption 1" />';
htmlTextWithLink += '        </a></li>';
htmlTextWithLink += '        <li class=""><a href="images/art/zeka2.jpg">';
htmlTextWithLink += '            <img alt="2" src="images/art/small/zeka2.jpg" title="Word Caption 2" />';
htmlTextWithLink += '        </a></li>';
htmlTextWithLink += '        <li class=""><a href="images/art/zeka3.jpg">';
htmlTextWithLink += '            <img alt="3" src="images/art/small/zeka3.jpg" title="Word Caption 3" />';
htmlTextWithLink += '        </a></li>';
htmlTextWithLink += '        <li class=""><a href="images/art/zeka4.jpg">';
htmlTextWithLink += '            <img alt="4" src="images/art/small/zeka4.jpg" title="Word Caption 4" />';
htmlTextWithLink += '        </a></li>';
htmlTextWithLink += '        <li class=""><a href="images/art/zeka5.jpg">';
htmlTextWithLink += '            <img alt="5" src="images/art/small/zeka5.jpg" title="Word Caption 5" />';
htmlTextWithLink += '        </a></li>        ';
htmlTextWithLink += '    </ul>';
htmlTextWithLink += '</div>';

var createGallery = function (o, startIndex) {
    if (startIndex) {
        var newStr = "";
        newStr = htmlTextWithLink.replace(/zeka([0-9])/gi, function (raw, i) {
            var idx = (parseInt(i) + startIndex) % 11;
            return "zeka" + idx;
        });
        return $(newStr).appendTo(document.body).wijgallery(o);
    }
    return $(htmlTextWithLink).appendTo(document.body).wijgallery(o);
};

var createGalleryEmpty = function (o) {
    return $(htmlEmpty).appendTo(document.body).wijgallery(o);
};

var getImageSrc = function (widget) {
    var image, src;
    if (widget) {
        image = widget.find("div.wijmo-wijgallery-current img");
        if (image.length) {
            src = image.attr("src");
        }
    }
    return src;
};

(function ($) {

    module("wijgallery: core");

    test("create and destroy", function () {
        var $widget = createGallery({
            showTimer: true,
            loop: true,
            showContorlsOnHover: true
        });

        ok($widget.hasClass("wijmo-wijgallery ui-widget"), 'create:element class added.');
        ok($widget.data("wijmo-wijgallery"), 'create:element data created.');


        //check child elements//wijmo-wijgallery-caption
        ok($widget.find(".wijmo-wijgallery-frame").length, 'create:frame has been created.');

        ok($widget.find(".wijmo-wijgallery-frame").hasClass("ui-widget-content ui-helper-clearfix"), 'create:frame class has been created.');

        ok($widget.find("div.wijmo-wijgallery-thumbs").length, "create:thumbs has been created."); //

        ok($widget.find("div.wijmo-wijgallery-frame-previous").length, "create:previous frame has been created.");

        ok($widget.find("div.wijmo-wijgallery-frame-next").length, "create:next frame has been created.");

        ok($widget.find("div.wijmo-wijgallery-content").length, "create:content has been created.");

        ok($widget.find("div.wijmo-wijgallery-caption").length, "create:caption has been created.");

        $widget.wijgallery('destroy');

        ok(!$widget.hasClass("wijmo-wijgallery ui-widget"), 'destroy:element class removed.');

        ok($widget.children().length === 1 &&
        $widget.children("ul").length === 1, "destroy:ul is unwrapped.")

        ok(!$widget.find("ul:eq(0)>li:eq(0) .wijmo-wijgallery-caption," +
        "ul:eq(0)>li:eq(0) .wijmo-wijgallery-text").hasClass("wijmo-wijgallery-image"), 'destroy:image class removed.');

        ok(!$widget.find("div.wijmo-wijgallery-frame,.wijmo-wijgallery-thumbs," +
            ".wijmo-wijgallery-frame-previous," +
            ".wijmo-wijgallery-frame-next,.wijmo-wijgallery-content").length,
            "destroy:thumbs,frame,previous/next frame,pager has been removed.");

        ok(!$widget.find("li").hasClass("ui-state-default"), "destroy: default state removed.");
        ok(!$widget.find("img").hasClass("wijmo-wijcarousel-image"), "destroy: carousel class removed.");

        $widget.remove();
    });
})(jQuery);

