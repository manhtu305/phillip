﻿/*
* wijgallery_options.js
*/
(function ($) {

	module("wijgallery:options");

	test("data", function () {
		var $widget, images = [
            {
            	thumbUrl: "images/art/small/zeka1.jpg",
            	url: "images/art/zeka1.jpg",
            	content: "",
            	caption: "<span>Word Caption 1</span>"
            },
            {
            	thumbUrl: "images/art/small/zeka2.jpg",
            	url: "images/art/zeka2.jpg",
            	content: "",
            	caption: "<span>Word Caption 2</span>"
            },
			{
				thumbUrl: "images/art/small/zeka3.jpg",
				url: "images/art/zeka3.jpg",
				content: "",
				caption: "<span>Word Caption 3</span>"
			}
			];

		$widget = createGallery({ data: images });

		ok($widget.data('wijmo-wijgallery').count() === 3, "count");
		ok($widget.find('li.wijmo-wijcarousel-item')
		.length === 3, "element");
		ok($widget.data('wijmo-wijgallery').images.length === 3, "images");

		$widget.remove();
	});

	test("showPager, showCounter, showTimer", function () {

		var $widget = createGallery({
			showTimer: false,
			showCounter: false,
			showControls: false,
			showPager: false,
			loop: false,
			control: "<div class=\"utest\">Bhah</div>"
		});

		//$widget.wijgallery("option", "showPager", true);

		//ok($widget.find("div.wijmo-wijpager").length !== 0, "showPager");

		$widget.wijgallery("option", "showCounter", true);

		ok($widget.find("div.wijmo-wijgallery-counter").length !== 0, "showCounter");

		$widget.wijgallery("option", "showTimer", true);

		ok($widget.find("div.wijmo-wijgallery-timerbar").length !== 0, "showTimer");

		$widget.wijgallery("option", "showControls", true);

		ok($widget.find("div.utest").length !== 0, "showControls");

		$widget.remove();
	});

	// to test whether "extend" or "replace"
	test("framePosition, thumbnailPosition, transitions, pagingPosition", function () {
		var $widget = createGallery({
			pagingPosition: { utest: true },
			transitions: { utest: true },
			thumbnailPosition: { utest: true },
			framePosition: { utest: true }
		});

		$widget.wijgallery("option", "framePosition", {});

		ok($widget.wijgallery("option", "framePosition").utest,
		"framePosition is extend to original value");

		$widget.wijgallery("option", "thumbnailPosition", {});

		ok($widget.wijgallery("option", "thumbnailPosition").utest,
		"thumbnailPosition is extend to original value");

		$widget.wijgallery("option", "transitions", {});

		ok($widget.wijgallery("option", "transitions").utest,
		"transitions is extend to original value");


		$widget.wijgallery("option", "pagingPosition", {});

		ok($widget.wijgallery("option", "pagingPosition").utest,
		"pagingPosition is extend to original value");

		$widget.remove();
	});

	test("counter", function () {
		var $widget = createGallery({
			showTimer: false,
			showCounter: false,
			showControls: false,
			showPager: false,
			loop: false,
			control: "<div class=\"utest\">Bhah</div>"
		});

		$widget.wijgallery("option", "counter", "[i] in [n]");

		$widget.wijgallery("option", "showCounter", true);

		window.setTimeout(function () {

			start();
			ok($widget.find("div.wijmo-wijgallery-counter span:eq(0)")
			.html() == "1 in 5", "counter");
			$widget.remove();
		}, 2000);

		stop();
	});

	test("thumbsSize, thumbsDisplay, thumbnailOrientation, thumbnailPosition", function () {
		ok(true, "run in carousel");
	});

})(jQuery);