﻿/*
* wijgallery_events.js
*/


(function ($) {
	module("wijgallery: events");

	test("loadCallback", function () {
		expect(1); //1 assertions
		var flag = 0,
		el = createGallery({
			showTimer: false,
			paging: false,
			loop: true,
			transitions: { animated: null, duration: 0 },
			showCounter: false,
			loadCallback: function () {
				flag++;
			}
		});
		ok(flag == 1, 'loadCallback is fired');
		el.remove();
	});


	test("beforeTransition", function () {
		expect(1); //2 assertions
		var flag = 0, flag1 = 0,
			el = createGallery({
				showTimer: false,
				paging: false,
				loop: true,
				transitions: { animated: null, duration: 0 },
				showCounter: false
			});
		window.setTimeout(function () {
			el.bind("wijgallerybeforetransition", function () {
				flag++;
			});
			el.wijgallery("next");
			window.setTimeout(function () {
				start();
				ok(flag == 1, 'beforeTransition is fired');
				el.remove();
			}, 1500);
		}, 1000);
		stop();
	});

	test("afterTransition", function () {
		expect(1); //2 assertions
		var flag1 = 0, el = createGallery({
			showTimer: false,
			paging: false,
			loop: true,
			transitions: { animated: null, duration: 0 },
			showCounter: false
		});
		window.setTimeout(function () {
			el.bind("wijgalleryaftertransition", function () {
				flag1++;
			});
			el.wijgallery("next");
			window.setTimeout(function () {
				start();
				ok(flag1 == 1, 'afterTransition is fired');
				el.remove();
			}, 500);
		}, 1000);
		stop();
	});

})(jQuery);