﻿/*
* wijgallery_methods.js
*/
(function ($) {

	module("wijgallery:methods");

	test("add & remove & count", function () {
		var removed = false, item, li, flag = 0;
		var $widget = createGallery({
			showTimer: false,
			paging: false,
			loop: true,
			transitions: { animated: null, duration: 0 },
			showCounter: false,
			loadCallback: function () {
				flag++;
			}
		});

		item = "<a href='utest'><img src='utest'/></a>";

		//olength = $widget.wijgallery("count");

		$widget.wijgallery("add", item, 3);

		$widget.find('li.wijmo-wijcarousel-item').each(function () {
			var n = $(this);
			if (n.data("itemIndex") === 3) {
				item = n;
			}
		});

		ok(item.find("img:eq(0)").attr("src") === 'utest',
		'Add: Thumbs item element added success');

		ok($widget.data('wijmo-wijgallery').count() == 6, "Count: it's ok!");

		ok($widget.data('wijmo-wijgallery').images[3] &&
		$widget.data('wijmo-wijgallery').images[3].url === "utest",
		'Add: item data added');

		$widget.wijgallery("remove", 3);

		$widget.find('li.wijmo-wijcarousel-item').each(function () {
			var n = $(this);
			if (n.data("itemIndex") === 3) {
				item = n;
			}
		});

		ok(item.find("img:eq(0)").attr("src") !== 'utest',
		'Remove: Thumbs item removed success');

		ok($widget.data('wijmo-wijgallery').images[3] &&
		$widget.data('wijmo-wijgallery').images[3].url !== "utest",
		'Remove: item data added');

		ok($widget.find('li.wijmo-wijcarousel-item').length === 5
		&& $widget.data('wijmo-wijgallery').images.length === 5, 'Remove: Item count correct!');
		$widget.find("img").unbind();
		$widget.remove();
	});


	test('play, pause', function () {
		var imageSrc, scrollSrc,
		$widget = createGallery({
			showTimer: false,
			paging: false,
			loop: true,
			transitions: { animated: null, duration: 0 },
			showCounter: false,
			interval: 800
		},2);

		window.setTimeout(function () {
			$widget.wijgallery("play");
			window.setTimeout(function () {

				imageSrc = getImageSrc($widget);
				ok(imageSrc !== "images/art/zeka1.jpg", "play sccuess!");
				$widget.wijgallery("pause");
				scrollSrc = imageSrc;
				window.setTimeout(function () {					
					imageSrc = getImageSrc($widget);
					ok(imageSrc === scrollSrc, "pause sccuess!");
					$widget.remove();
					start();
				}, 1100);

			}, 1500);
		}, 1000);
		stop();
	});


	test('next, previous', function () {
		var imageSrc,
		$widget = createGallery({
			showTimer: false,
			transitions: { animated: null, duration: 0 },
			showCounter: false
		}, 4);

		window.setTimeout(function () {
			$widget.wijgallery("next");
			window.setTimeout(function () {

				imageSrc = getImageSrc($widget);
				ok(imageSrc === "images/art/zeka6.jpg", "next sccuess!");

				$widget.wijgallery("previous");
				window.setTimeout(function () {
					start();
					imageSrc = getImageSrc($widget);
					ok(imageSrc === "images/art/zeka5.jpg", "previous sccuess!");
					$widget.remove();
				}, 1000);

			}, 1000);
		}, 1000);
		stop();
	});

	test('show', function () {
		var imageSrc, flag = 0,
		$widget = createGallery({
			showTimer: false,
			paging: false,
			loop: true,
			transitions: { animated: null, duration: 0 },
			showCounter: false
		});

		$widget.wijgallery("show", 3);
		window.setTimeout(function () {
			start();
			imageSrc = getImageSrc($widget);
			ok(imageSrc === "images/art/zeka4.jpg", "show sccuess!");
			$widget.remove();
		}, 1000);
		stop();

	});

})(jQuery);