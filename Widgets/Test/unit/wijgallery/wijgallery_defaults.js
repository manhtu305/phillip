﻿/*
* wijgallery_defaults.js
*/


commonWidgetTests('wijgallery', {
	defaults: {
		/// <summary>
		/// Selector option for auto self initialization. 
		///	This option is internal.
		/// </summary>
		initSelector: ":jqmData(role='wijgallery')",
		wijCSS: $.wijmo.wijCSS,
		disabled:false,
		/// <summary>
		/// Allows pictures to be played automatically.
		/// Default: false.
		/// Type: Boolean.
		/// Code example: $("#element").wijgallery( { auto: true } );
		/// </summary>
		autoPlay: false,
		/// <summary>
		/// Determines the time span between pictures in autoplay mode. 
		/// Default: false.
		/// Type: Boolean.
		/// Code example: $("#element").wijgallery( { interval: 3000 } );
		/// </summary>
		showTimer: true,
		/// <summary>
		/// Determines the time span between 2 pictures showing in autoplay mode. 
		/// Default: 5000.
		/// Type: Number.
		/// Code example: $("#element").wijgallery( { interval: 3000 } );
		/// </summary>
		interval: 5000,
		/// <summary>
		/// Determines whether the caption of items should be shown.
		/// Default: true.
		/// Type: Boolean.
		/// Code example: $("#element").wijgallery( { showCaption: true } );
		/// </summary>
		showCaption: true,
		data: [],
		/// <summary>
		/// Determines whether the custom control should be shown.
		/// Default: true.
		/// Type: Boolean.
		/// Code example: $("#element").wijcarousel( { showControls: true } );
		/// </summary>
		showControls: false,
		/// <summary>
		/// Determines the innerHtml of the custom control.
		/// Default: "".
		/// Type: String.
		/// Code example: $("#element").wijgallery( { control: "<div>Blah</div>" } );
		/// </summary>
		control: "",
		/// <summary>
		/// A value that indicates the position settings for the custom control.
		/// Default: {}.
		/// Type: Object.
		/// Code example: $("#element").wijgallery( { 
		///		pagerType: {
		///			my: 'left bottom', 
		///			at: 'right top', 
		///			offset: '0 0'} 
		/// });
		/// </summary>
		controlPosition: {},
		scrollWithSelection: false,
		showCounter: true,
		counter: "[i] of [n]",
		showPager: false,
		pagingPosition: {},
		/// <summary>
		/// Determines the orientation of the thumbnails. 
		/// Possible values are: "vertical" & "horizontal"
		/// Default: "horizontal".
		/// Type: String.
		/// Code example: $("#element").wijgallery( { thumbnailOrientation: "vertical" } );
		/// </summary>
		thumbnailOrientation: "horizontal",
		/// <summary>
		/// Determines the direction of the thumbnails. 
		/// Possible values are: "before" & "after"
		/// Default: "horizontal".
		/// Type: String.
		/// Code example: $("#element").wijgallery( { thumbnailOrientation: "before" } );
		/// </summary>
		thumbnailDirection: "after",
		transitions: {
			animated: "slide",
			duration: 1000,
			easing: null
		},
		/// <summary>
		/// Determines whether the controls should be shown after created
		/// or hover on the dom element.
		/// Default: true.
		/// Type: Boolean.
		/// Code example: $("#element").wijgallery( { showControlsOnHover: true } );
		/// </summary>
		showControlsOnHover: true,
		thumbsDisplay: 5,
		thumbsLength: 100,
		/// <summary>
		/// The beforeTransition event handler.
		/// A function called before transition to another image.
		/// Default: null.
		/// Type: Function.
		/// Code example: 
		/// Supply a function as an option.
		/// $("#element").wijgallery( { beforeTransition: function () {} } );
		/// Bind to the event by type: wijcarouselbeforescroll
		/// $("#element").bind("wijgallerybeforetransition", function(e, data) { } );
		/// </summary>
		/// <param name="e" type="Object">
		/// jQuery.Event object.
		/// </param>
		/// <param name="data" type="Object">
		/// Include informations that relates to this event.
		/// data.index : the index of the current image.
		///	data.to : the index of the image that will scrolled to.
		/// </param>
		beforeTransition: null,
		afterTransition: null,
		/// <summary>
		/// The loadCallback event handler.
		/// A function called after created the dom element.
		/// Default: null.
		/// Type: Function.
		/// Code example: 
		/// Supply a function as an option.
		/// $("#element").wijgallery( { loadCallback: function () {} } );
		/// Bind to the event by type: wijcarouselloadcallback
		/// $("#element").bind("wijgalleryloadcallback", function(e, data) { } );
		/// </summary>
		/// <param name="e" type="Object">
		/// jQuery.Event object.
		/// </param>
		/// <param name="data" type="Object">
		/// The node widget that relates to this event.
		/// </param>
		loadCallback: null,
		showMovieControls: false,
		autoPlayMovies: true,
		flashParams: {
			bgcolor: "#000000",
			allowfullscreen: true,
			wmode: "transparent"
		},
		flashVars: {},
		flashVersion: "9.0.115",
		flvPlayer: 'player\\player.swf',
		flashInstall: 'player\\expressInstall.swf',
		/// Possible values: "img", "iframe", "swf", "flv"
		mode: "img",
		create: null,
		wijMobileCSS: {
		    header: "ui-header ui-bar-a",
		    content: "ui-body-a",
		    stateDefault: "ui-btn-up-a",
		    stateHover: "ui-btn-down-a",
		    stateActive: "ui-btn-down-b",
		    iconPlay: "ui-icon-arrow-r",
		    iconPause: "ui-icon-grid"
		},
	    /// <summary>
	    /// Determines whether the caption of thumbnails should be shown.
	    /// Default: true.
	    /// Type: Boolean.
	    /// Code example: $("#element").wijgallery( { showThumbnailCaptions: true } );
	    /// </summary>
		showThumbnailCaptions: false
	}
});
