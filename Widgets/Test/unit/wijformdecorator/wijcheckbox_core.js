﻿/*
* wijcheckbox_core.js
*/

function create_wijcheckbox(options) {
    var checkbox = $('<input type="checkbox" id="checkbox1" /><label for="checkbox1">test</label>');
    checkbox.appendTo("body");
    return checkbox.wijcheckbox(options);
}

function create_wijcheckboxWithChecked(options) {
    var checkbox = $('<input type="checkbox" checked id="checkbox1" /><label for="checkbox1">test</label>');
    checkbox.appendTo("body");
    return checkbox.wijcheckbox(options);
}

(function ($) {

    module("wijcheckbox: core");
    test("create and destroy", function () {
        // test disconected element
        var $widget = create_wijcheckbox();
        //$widget.appendTo("body");
        ok($widget.parent().hasClass('wijmo-checkbox-inputwrapper'), 'wraped element css classes created.');
        ok($widget.parent().parent().hasClass('wijmo-checkbox ui-widget'), 'box element css classes created.');
        ok($widget.parent().next().is("label"), 'label is moved correctly');
        ok($widget.parent().next().next().hasClass("wijmo-checkbox-box ui-widget ui-state-default ui-corner-all"), 'displayed checkbox element css classes created.');
		ok($widget.data('wijmo-wijcheckbox'),'element data created.');
        $widget.wijcheckbox("destroy");
        ok(!$widget.parent().hasClass('wijmo-checkbox-inputwrapper'), 'wraped element css classes removed');
        ok($widget.next().is("label"),"label is moved correctly");
        ok(!$widget.data('wijmo-wijcheckbox'), 'element data removed.');
        $widget.remove();
    });
    
    test("checked option", function () {
    	var $widget = create_wijcheckbox({checked:true});
    	ok($widget.parent().parent().hasClass('ui-state-checked'), 'chech has setted.');
    	ok($widget.parent().next().next().hasClass("ui-state-active"), 'displayed checkbox element css classes created.');
    	$widget.remove();
    	
    	var $widget = create_wijcheckboxWithChecked({checked:false});
    	ok(!$widget.parent().parent().hasClass('ui-state-checked'), 'chech has setted.');
    	ok(!$widget.parent().next().next().hasClass("ui-state-active"), 'displayed checkbox element css classes created.');
    	$widget.remove();
    });
})(jQuery);