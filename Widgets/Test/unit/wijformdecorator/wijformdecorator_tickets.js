﻿/*
 * wijformdecorator_tickets.js
 */

(function ($) {

    module("wijformdecorator: tickets");

    test("dropdown disabled", function () {
        var $widget = create_wijdropdown({
            disabled: true
        }),
        selectWidget = $(".wijmo-wijdropdown"),
        link = $("> a", selectWidget),
        label = $("> label", link);

        ok(link.hasClass("ui-state-disabled") && label.hasClass("ui-state-disabled"), "dropdown disabled");
        $widget.wijdropdown("destroy");
        $widget.remove();
    });

    test("dropdown document click", function () {
        var $widget = create_wijdropdown({
            showingAnimation: null,
            hidingAnimation: null
        }),
            selectWidget = $(".wijmo-wijdropdown"),
            dropDownBtn = $(".wijmo-dropdown-trigger"),
            dropDownPanel = $(".wijmo-wijsuperpanel"),
            event = $.Event("mouseup"),
            offset = $widget.offset();

        dropDownBtn.trigger("click");
        ok(dropDownPanel.css("display") === "block", "dropdown panel is opened");
        event.pageX = offset.left + 300;
        event.pageY = offset.top + 300;
        $(document).trigger(event);
        ok(dropDownPanel.css("display") === "none", "dropdown panel is closed");
        $widget.wijdropdown("destroy");
        $widget.remove();
    });

    test("dropdown'height is too large", function () {
        var select = $("<select><option>this is the test item 1111</option><option>item2</option></select>").appendTo("body");
        select.wijdropdown();
        ok(select.data("wijmo-wijdropdown")._container.outerHeight() < 25 * 2, "The dropdown is not wrap line.");
        select.remove();
    });

    test("dropdown's width is increase when call refresh method if the width of select element is not seted.", function () {
        var select = $("<select><option>this is the test item 1111</option><option>item2</option></select>").appendTo("body");
        select.wijdropdown();
        var width = select.data("wijmo-wijdropdown")._container.width();
        select.wijdropdown("refresh");
        ok(select.data("wijmo-wijdropdown")._container.width() == width, "the width is not changed when call refresh method");
        select.remove();
    });

    test("#40218", function () {
        var $widget = create_wijcheckbox();
        $widget.wijcheckbox("option", "disabled", true);
        ok($widget.parent().next().next().hasClass("ui-state-disabled"), 'disabled stlye has applied!');
        $widget.remove();

        var $widget = create_wijcheckbox({ disabled: true });
        $widget.wijcheckbox("option", "disabled", false);
        ok(!$widget.parent().next().next().hasClass("ui-state-disabled"), 'disabled stlye has removed!');
        $widget.remove();
    });

    test("#40251", function () {
        var container = $("<div>").appendTo("body"),
            createRadio = function () {
                var radio = $("<input type='radio' name='test'>").appendTo(container);
            }, radioIcons;
        for (var i = 0; i < 4; i++) {
            createRadio();
        }
        container.find("input").wijradio();

        radioIcons = container.find(".wijmo-wijradio-icon");
        radioIcons.eq(0).simulate("click");
        radioIcons.eq(1).simulate("click");
        radioIcons.eq(0).simulate("click");
        ok(radioIcons.hasClass("ui-icon-radio-off"), "when the radio click again, the checked status is seted it again.");
        container.remove();
    });

    test("#75892", function () {
        var container = $("<div>").appendTo("body"),
            createRadio = function (i) {
                var radio = $("<input type='radio' name='test' id='radio" + i + "'>").appendTo(container);
            };
        for (var i = 0; i < 3; i++) {
            createRadio(i);
        }
        container.find("input").wijradio();
        $("#radio0").wijradio("option", "checked", true);
        ok($(".wijmo-wijradio:eq(0)").hasClass("ui-state-checked"), "check state is added when set checked option to true");
        $("#radio1").wijradio("option", "checked", true);
        ok(!$(".wijmo-wijradio:eq(0)").hasClass("ui-state-checked"), "check state is removed when other radio's checked option is set to true");
        ok($(".wijmo-wijradio:eq(1)").hasClass("ui-state-checked"), "check state is added when set checked option to true");
        $("#radio2").wijradio("option", "checked", false);
        ok($(".wijmo-wijradio:eq(1)").hasClass("ui-state-checked"), "check state is not removed by setting another radio's checked option to false");
        container.remove();
    });

    test("#84203_radio", function () {
        var container = $("<div>").appendTo("body"),
            createRadio = function (i) {
                var radio = $("<input type='radio' name='test' id='radio" + i + "'>").appendTo(container);
            };
        for (var i = 0; i < 3; i++) {
            createRadio(i);
        }
        container.find("input").wijradio({ disabled: true });

        ok(container.find(".wijmo-wijradio").hasClass("ui-state-disabled"), "disable state is added.");
        container.find("input").wijradio("option", "disabled", false);
        ok(!container.find(".wijmo-wijradio").hasClass("ui-state-disabled"), "disable state is removed.");
        container.find("input").wijradio("option", "disabled", true);
        ok(container.find(".wijmo-wijradio").hasClass("ui-state-disabled"), "disable state is added.");
        container.remove();
    });

    test("#84203_textbox", function () {
        var container = $("<div>").appendTo("body"),
            input = $("<input type='text' name='test' />").appendTo(container).wijtextbox({ disabled: true });

        ok(input.is(":disabled"), "textbox is disabled.");
        input.wijtextbox("option", "disabled", false);
        ok(!input.is(":disabled"), "textbox is enabled.");
        input.wijtextbox("option", "disabled", true);
        ok(input.is(":disabled"), "textbox is disabled.");
        input.remove();
        
        input = $("<input type='text' name='test' disabled='disabled' />").appendTo(container).wijtextbox();
        ok(container.find(".wijmo-wijtextbox").hasClass("ui-state-disabled"), "disable state is added.");
        container.remove();
    });

})(jQuery);