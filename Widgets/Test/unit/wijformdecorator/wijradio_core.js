﻿/*
* wijradio_core.js
*/

function create_wijradio(options) {
    var radio = $('<input type="radio" id="radio1" /><label for="radio1">test</label>');
    radio.appendTo("body");
    return radio.wijradio(options);
}

function create_wijrediobuttondecorator_withoutlabel(options) {
    var radio = $('<input type="radio"/>');
    radio.appendTo("body");
    return radio.wijradio(options);
}

function create_wijrediobuttondecorator_withChecked(options) {
    var radio = $('<input type="radio" id="radio1" checked/><label for="radio1">test</label>');
    radio.appendTo("body");
    return radio.wijradio(options);
}

(function ($) {

    module("wijradio: core");
    test("create and destroy", function () {
        // test disconected element
        var $widget = create_wijradio();
        //$widget.appendTo("body");
        ok($widget.parent().hasClass('wijmo-wijradio-inputwrapper'), 'wraped element css classes created.');
        ok($widget.parent().parent().hasClass('wijmo-wijradio ui-widget'), 'box element css classes created.');
        ok($widget.parent().next().is("label"), 'label is moved correctly');
        ok($widget.parent().next().next().hasClass("wijmo-wijradio-box ui-widget ui-state-default ui-corner-all"), 'displayed checkbox element css classes created.');
        ok($widget.data('wijmo-wijradio'), 'element data created.');
        $widget.wijradio("destroy");
        ok(!$widget.parent().hasClass('wijmo-wijradio-inputwrapper'), 'wraped element css classes removed');
        ok($widget.next().is("label"), "label is moved correctly");
        $widget.remove();

        //test without label radiobuton.
        var $radio=create_wijrediobuttondecorator_withoutlabel();
        ok($radio.parent().hasClass('wijmo-wijradio-inputwrapper'), 'without label, wraped element css classes created.');
        ok($radio.parent().parent().hasClass('wijmo-wijradio ui-widget'), 'without label, box element css classes created.');
        ok($radio.parent().next().hasClass("wijmo-wijradio-box ui-widget ui-state-default ui-corner-all"), 'without label, displayed checkbox element css classes created.');
        $radio.wijradio("destroy");
        ok(!$radio.parent().hasClass('wijmo-wijradio-inputwrapper'), 'without label,wraped element css classes removed');
        ok(!$widget.data('wijmo-wijradio'), 'element data removed.');
        $radio.remove();
    });
    
    test("checked option", function () {
    	var $widget = create_wijradio({checked:true});
    	ok($widget.parent().parent().hasClass('ui-state-checked'), 'chech has setted.');
    	ok($widget.parent().next().next().hasClass("ui-state-active"), 'displayed radio element css classes created.');
    	$widget.remove();
    	
    	var $widget = create_wijrediobuttondecorator_withChecked({checked:false});
    	ok(!$widget.parent().parent().hasClass('ui-state-checked'), 'chech has setted.');
    	ok(!$widget.parent().next().next().hasClass("ui-state-active"), 'displayed radio element css classes created.');
    	$widget.remove();
    });

})(jQuery);