﻿/*
* wijtextbox_core.js
*/

function create_wijtextbox(options) {
    return $('<input type="text" id="testbox1" /><label for="testbox1"></label>').wijtextbox(options).appendTo($('body'));
}

(function ($) {

    module("wijtextbox: core");
    test("create and destroy", function () {
        var $widget = create_wijtextbox();
        ok($widget.hasClass('ui-widget ui-state-default ui-corner-all'), 'element css classes created.');
        ok($widget.data('wijmo-wijtextbox'), 'element data created.');
        $widget.wijtextbox('destroy');
        equals($widget.attr("class"), "", 'element css classes removed');
        ok(!$widget.data('wijmo-wijtextbox'), 'element data removed.');
        $widget.remove();
    });

    test("test hover behavior",function() {
        var $widget = create_wijtextbox();
        $widget.simulate("mouseover");
        ok($widget.hasClass('ui-state-hover'), 'hover class is added.');
        $widget.simulate("mouseout");
        ok(!$widget.hasClass('ui-state-hover'), 'hover class is removed.');
        $widget.remove();
    });

    test("test active behavior",function() {
        var $widget = create_wijtextbox();
        $widget.simulate("mousedown");
        ok($widget.hasClass('ui-state-active'), 'active class is added.');
        $widget.simulate("mouseup");
        ok(!$widget.hasClass('ui-state-active'), 'active class is removed.');
        $widget.remove();
    });

    test("test focus behavior",function() {
        var $widget = create_wijtextbox();
        $widget.focus();

        setTimeout(function() { 
            ok($widget.hasClass('ui-state-focus'), 'focus class is added.');
            $widget.blur();
            
            setTimeout(function() {
                ok(!$widget.hasClass('ui-state-focus'), 'focus class is removed.');
                $widget.remove();
                start();
            }, 10);
        }, 10);
        stop();
    });

})(jQuery);