﻿/*
* wijdropdown_core.js
*/

function create_wijdropdown(options) {
    return $('<select><option>A</option><option>B</option><option>C</option><option>D</option><option>E</option></select>')
        .appendTo("body")
        .wijdropdown(options);
}

(function ($) {

	module("wijdropdown: core");
	test("create and destroy", function () {
		// test disconected element
        var $widget, widget, ul;
		
        $widget = create_wijdropdown({
            showingAnimation: null,
            hidingAnimation: null
        });
		ok($widget.parent().parent().hasClass('wijmo-wijdropdown'), 'element css classes created.');
		ok($widget.data('wijmo-wijdropdown'), 'element data created.')
		$widget.wijdropdown("destroy");
		ok($widget.parent().parent('.wijmo-wijdropdown').length === 0, 'element css classes removed');
		ok($widget.parent().siblings('.wijmo-dropdown-trigger, .wijmo-dropdown').length === 0, 'attached elements removed');
		ok($widget.parent('.ui-helper-hidden').length === 0, 'wrapped elements removed');
		ok(!$widget.data('wijmo-wijdropdown'), 'element data removed');
		$widget.remove();

		//test group dropdownlist
		widget = $('<select><optgroup label="A-B"><option>A</option><option>B</option></optgroup><optgroup label="C-E"><option>C</option><option>D</option><option>E</option></optgroup></select>')
            .appendTo("body")
            .wijdropdown();
		ul = widget.parent().parent().find("ul:first");
		ok(ul.children("li:first").hasClass("wijmo-dropdown-optgroup"), 
            "the group is created.");
        widget.wijdropdown("destroy");
		widget.remove();
	});
	
	test("change event", function () {
		var selectedValue,
		select = $('<select><optgroup label="A-B"><option>A</option><option selected>B</option></optgroup><optgroup label="C-E"><option>C</option><option>D</option><option>E</option></optgroup></select>')
            .appendTo("body")
			.bind("change", function () {
			    selectedValue = $(this).val();
			}),
		selectWidget = select.wijdropdown({
            showingAnimation: null,
            hidingAnimation: null
        }),
		dropDownBtn = $(".wijmo-dropdown-trigger");    
		dropDownBtn.simulate('click');
		stop();	
		window.setTimeout(function () {
			var selctedItem = $($("li.wijmo-dropdown-item").get(1));	
			ok(selctedItem.hasClass("ui-state-hover"), true);
			
			dropDownBtn.simulate('click');
			
			selctedItem = $($("li.wijmo-dropdown-item").get(2));  
			selctedItem.simulate('click');
			
			window.setTimeout(function () {
                selectWidget.wijdropdown("destroy");
				select.remove();
				selectWidget.remove(); 
			}, 1000);
			start();
		}, 400);
	});
})(jQuery);