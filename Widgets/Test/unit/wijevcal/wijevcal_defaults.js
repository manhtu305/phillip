/*globals commonWidgetTests*/
/*
* wijevcal_defaults.js
*/
"use strict";
commonWidgetTests('wijevcal', {
	defaults: {
		wijMobileCSS: {
			header: "ui-header ui-bar-a",
			content: "ui-body ui-body-b",
			stateDefault: "ui-btn ui-btn-b"
		},
		initSelector: ":jqmData(role='wijevcal')",
		webServiceUrl: "",
		colors: null,
		culture: "",
		cultureCalendar: "",
		calendar: null,
		localization: null,
		dataStorage: {
			addEvent: null,
			updateEvent: null,
			deleteEvent: null,
			loadEvents: null,
			addCalendar: null,
			updateCalendar: null,
			deleteCalendar: null,
			loadCalendars: null
		},
		appointments: [],
		calendars: [],
		disabled: false,
		editCalendarTemplate: "",
		enableLogs: false,
		eventTitleFormat: "{2}",
		titleFormat: {
			day: false,
			week: "_formatWeekTitle",
			month: "_formatMonthTitle",
			list: false,
			"custom": "_formatCustomTitle"
		},
		firstDayOfWeek: 0,
		headerBarVisible: true,
		navigationBarVisible: true,
		rightPaneVisible: false,
		selectedDate: null,
		selectedDates: null,
		statusBarVisible: false,
		timeInterval: 30,
		timeIntervalHeight: 15,
		timeRulerInterval: 60,
		timeRulerFormat: "{0:h tt}",
		dayHeaderFormat: "{0:d }",
		firstRowDayHeaderFormat: "{0:ddd d}",
		dayViewHeaderFormat:
			{
				day: "all-day events",
				week: "{0:d dddd}",
				list: "{0:d dddd}",
				custom: "{0:d dddd}"
			},
		viewType: "day",
		visibleCalendars: [],
		views: ["day", "week", "month", "list"],
		eventCreating: null,
		datePagerLocalization: null,
		readOnly: false,
		dataSource: null,
		eventsData: null,
		ensureEventDialogOnBody: false,
		calendarsChanged: null,
		initialized: null,
		selectedDatesChanged: null,
		viewTypeChanged: null,
		beforeEditEventDialogShow: null,
		beforeAddEvent: null,
		beforeUpdateEvent: null,
		beforeDeleteEvent: null,
		beforeAddCalendar: null,
		beforeUpdateCalendar: null,
		beforeDeleteCalendar: null,
		eventsDataChanged: null
	}
});