﻿(function ($) {
    module("wijevcal:tickets")

    test("#106911", function () {
        var eventscalendar = createEventscalendar({
            views: [{ name: "2 Months", unit: "month", count: 2 }, { name: "2 Years", unit: "year", count: 2 }],
            selectedDate: new Date(2015, 1, 6)
        }), monthView, yearView;
        // superpanel re-create everytime
        monthView = $(".wijmo-wijev-monthview");
        monthView.find(".wijmo-wijsuperpanel").wijsuperpanel("option", { vScroller: { scrollValue: 60 } });
        $(".wijmo-wijev-date_2015_2_1 .wijmo-wijev-monthcell").simulate("click");
        $(".wijmo-wijev-save").simulate("click");
        ok(monthView.find(".wijmo-wijsuperpanel").wijsuperpanel("option", "vScroller").scrollValue === 60);
        $(".wijmo-wijev-event-title").simulate("click");
        $(".wijmo-wijev-delete").simulate("click");
        ok(monthView.find(".wijmo-wijsuperpanel").wijsuperpanel("option", "vScroller").scrollValue === 60);
        eventscalendar.find(".wijmo-wijev-custom").eq(1).simulate("click");
        yearView = $(".wijmo-wijev-yearview");
        yearView.find(".wijmo-wijsuperpanel").wijsuperpanel("option", { vScroller: { scrollValue: 60 } });
        $(".wijmo-wijev-month_2016_1_1 .wijmo-wijev-yearcell").simulate("click");
        $(".wijmo-wijev-save").simulate("click");
        ok(yearView.find(".wijmo-wijsuperpanel").wijsuperpanel("option", "vScroller").scrollValue === 60);
        $(".wijmo-wijev-event-title").simulate("click");
        $(".wijmo-wijev-delete").simulate("click");
        ok(yearView.find(".wijmo-wijsuperpanel").wijsuperpanel("option", "vScroller").scrollValue === 60);
        eventscalendar.remove();
    });

    test("set event data option", function () {
        var evcal = createEventscalendar();
        evcal.wijevcal({
            eventsData: [{ id: "appt1", subject: "app1", start: new Date(2013, 5, 23, 15, 30), end: new Date(2013, 5, 23, 17, 35) },
                { id: "appt2", subject: "app2", start: new Date(2013, 5, 23, 10, 30), end: new Date(2013, 5, 23, 11, 35) }
            ]
        })

        setTimeout(function () {
            var data = evcal.wijevcal("option", "eventsData");
            ok(data.length === 2, "the event data is set to the widget.");
            evcal.wijevcal("deleteEvent", "appt1");
            evcal.wijevcal("deleteEvent", "appt2");
            evcal.remove();
            start();
        }, 1000)

        stop();
    });

    if ($.browser.msie && parseInt($.browser.version) > 9 && $.support.isTouchEnabled && $.support.isTouchEnabled()) {
        test("In IE10, in list view mode, delete the some event, the list view item is not removed", function () {
            var evcal = createEventscalendar({
                eventsData: [{ id: "appt1", subject: "app1", start: new Date(2013, 5, 23, 15, 30), end: new Date(2013, 5, 23, 17, 35) },
                    { id: "appt2", subject: "app2", start: new Date(2013, 5, 23, 10, 30), end: new Date(2013, 5, 23, 11, 35) }
                ],
                viewType: "list"
            });
            ok($(".wijmo-wijev-agenda-event", evcal).length > 0, "The listview contains event items.");
            setTimeout(function () {
                //evcal.wijevcal("deleteEvent", "appt1");
                evcal.wijevcal("deleteEvent", "appt2");
                setTimeout(function () {
                    ok($(".wijmo-wijev-agenda-event", evcal).length === 1, "The items has removed.");
                    evcal.wijevcal("deleteEvent", "appt1");
                    evcal.remove();
                    start();
                }, 500)
            }, 1000);
            stop();
        });
        test("In IE10, the scrollbar of listview is not shown", function () {
            var evcal = createEventscalendar({
                eventsData: [{ id: "appt1", subject: "app1", start: new Date(2013, 5, 23, 15, 30), end: new Date(2013, 5, 23, 17, 35) },
                    { id: "appt2", subject: "app2", start: new Date(2013, 5, 23, 10, 30), end: new Date(2013, 5, 23, 11, 35) },
                    { id: "appt2", subject: "app2", start: new Date(2013, 5, 23, 10, 30), end: new Date(2013, 5, 23, 11, 35) },
                    { id: "appt2", subject: "app2", start: new Date(2013, 5, 23, 10, 30), end: new Date(2013, 5, 23, 11, 35) },
                    { id: "appt2", subject: "app2", start: new Date(2013, 5, 23, 10, 30), end: new Date(2013, 5, 23, 11, 35) },
                    { id: "appt2", subject: "app2", start: new Date(2013, 5, 23, 10, 30), end: new Date(2013, 5, 23, 11, 35) },
                    { id: "appt2", subject: "app2", start: new Date(2013, 5, 23, 10, 30), end: new Date(2013, 5, 23, 11, 35) },
                    { id: "appt2", subject: "app2", start: new Date(2013, 5, 23, 10, 30), end: new Date(2013, 5, 23, 11, 35) },
                    { id: "appt2", subject: "app2", start: new Date(2013, 5, 23, 10, 30), end: new Date(2013, 5, 23, 11, 35) },
                    { id: "appt2", subject: "app2", start: new Date(2013, 5, 23, 10, 30), end: new Date(2013, 5, 23, 11, 35) },
                    { id: "appt2", subject: "app2", start: new Date(2013, 5, 23, 10, 30), end: new Date(2013, 5, 23, 11, 35) }
                ],
                viewType: "list"
            });
            ok(evcal.find(".wijmo-wijev-list-details .wijmo-wijev-agenda-container .wijmo-wijsuperpanel-contentwrapper-touch").length > 0, "the scrollbar is added to event calendar.");
            evcal.wijevcal("deleteEvent", "appt1");
            evcal.wijevcal("deleteEvent", "appt2");
            evcal.remove();
        })
    }

    test("init event data", function () {
        var evcal = createEventscalendar({
            eventsData: [{ id: "appt1", subject: "app1", start: new Date(2013, 5, 23, 15, 30), end: new Date(2013, 5, 23, 17, 35) },
                { id: "appt2", subject: "app2", start: new Date(2013, 5, 23, 10, 30), end: new Date(2013, 5, 23, 11, 35) }
            ]
        });
        setTimeout(function () {
            var data = evcal.wijevcal("option", "eventsData");
            ok(data.length === 2, "the event data is set to the widget.");
            evcal.wijevcal("deleteEvent", "appt1");
            evcal.wijevcal("deleteEvent", "appt2");
            evcal.remove();
            start();
        }, 1000)
        stop();
    });

    test("set localization option", function () {
        var evcal = createEventscalendar();
        evcal.wijevcal({
            localization: {
                buttonToday: "CustomToday",
                buttonDayView: "DayView",
                buttonWeekView: "WeekView"
            }
        });
        setTimeout(function () {
            ok($(".wijmo-wijev-navigationbar .wijmo-wijev-today").text() === "CustomToday", "the localization option is applied.");
            evcal.remove();
            start();
        }, 1500)
        stop();
    });
    
    test("init event data without amplify", function () {
        var amplify_bak = window["amplify"];
        window["amplify"] = null;
        var evcal = createEventscalendar({
            eventsData: [{ id: "appt1", subject: "app1", start: new Date(2013, 5, 23, 15, 30), end: new Date(2013, 5, 23, 17, 35) },
                { id: "appt2", subject: "app2", start: new Date(2013, 5, 23, 10, 30), end: new Date(2013, 5, 23, 11, 35) }
            ],
            viewType: "list"
        });
        setTimeout(function () {            
            ok(evcal.find(".wijmo-wijev-agenda-event").length > 0, "the event data is set to the widget.");
            evcal.remove();
            window["amplify"] = amplify_bak;
            start();
        }, 1100);
        stop();
    });
    
    if (!window["openDatabase"]) {
        test("addEvent method in none support local storage", function () {
            var amplify_bak = window["amplify"];
            window["amplify"] = null;
            var evcal = createEventscalendar({ viewType: "list" });
            evcal.wijevcal("addEvent", { id: "appt1", subject: "app1", start: new Date(2013, 5, 23, 15, 30), end: new Date(2013, 5, 23, 17, 35) });
            evcal.wijevcal("addEvent", { id: "appt2", subject: "app2", start: new Date(2013, 5, 23, 10, 30), end: new Date(2013, 5, 23, 11, 35) });
            ok(evcal.find(".wijmo-wijev-agenda-event").length > 0, "the event data is added to the widget.");
            evcal.remove();
            window["amplify"] = amplify_bak;
        });
    }
    
    test("when the firstDayOfWeek is set to number string, when switch to month view the browser will Infinite loop", function () {
        var evcal = createEventscalendar({
            viewType: "month",
            firstDayOfWeek: "2"
        });
        setTimeout(function () {
            ok(true, "the event calendar works fine.");
            evcal.remove();
            start();
        }, 500);
        stop();
    });
    test("test resolve DayView Appointment Conflicts", function () {
        var evcal = createEventscalendar(), myDate = new Date();
        evcal.wijevcal({
            eventsData: [{ id: "appt1", subject: "app1", start: new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), 10, 00), end: new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), 12, 00) },
                { id: "appt2", subject: "app2", start: new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), 11, 00), end: new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), 13, 00) },
                { id: "appt3", subject: "app3", start: new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), 12, 00), end: new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), 14, 00) }
            ]
        })
        setTimeout(function () {
            var col = $(".wijmo-wijev-daycolumn", evcal), appts, style;
            appts = $(col).find(".wijmo-wijev-appointment");
            ok(appts.length === 3, "Success get the appointments");
            style = $(appts[0]).attr("style");
            ok(style.indexOf("width: 33%") !== -1, "The first width is set correctly.");
            ok(style.indexOf("margin-left: 0%") !== -1, "The first left margin is set correctly.");
            style = $(appts[1]).attr("style");
            ok(style.indexOf("width: 33%") !== -1, "The second width is set correctly.");
            ok(style.indexOf("margin-left: 33%") !== -1, "The second left margin is set correctly.");
            style = $(appts[2]).attr("style");
            ok(style.indexOf("width: 33%") !== -1, "The third width is set correctly.");
            ok(style.indexOf("margin-left: 66%") !== -1, "The third left margin is set correctly.");
            evcal.wijevcal("deleteEvent", "appt1");
            evcal.wijevcal("deleteEvent", "appt2");
            evcal.wijevcal("deleteEvent", "appt3");
            evcal.remove();
            start();
        }, 1000);
        stop();
    });

    test("test resolve DayView Appointment Conflicts 2#", function () {
        var evcal = createEventscalendar(), myDate = new Date();
        evcal.wijevcal({
            eventsData: [{ id: "appt1", subject: "app1", start: new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), 10, 00), end: new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), 14, 00) },
                { id: "appt2", subject: "app2", start: new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), 10, 00), end: new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), 11, 00) },
                { id: "appt3", subject: "app3", start: new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), 12, 00), end: new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), 14, 00) }
            ]
        })
        setTimeout(function () {
            var col = $(".wijmo-wijev-daycolumn", evcal), appts, style;
            appts = $(col).find(".wijmo-wijev-appointment");
            ok(appts.length === 3, "Success get the appointments");
            style = $(appts[0]).attr("style");
            ok(style.indexOf("width: 33%") !== -1, "The first width is set correctly.");
            ok(style.indexOf("margin-left: 0%") !== -1, "The first left margin is set correctly.");
            style = $(appts[1]).attr("style");
            ok(style.indexOf("width: 33%") !== -1, "The second width is set correctly.");
            ok(style.indexOf("margin-left: 33%") !== -1, "The second left margin is set correctly.");
            style = $(appts[2]).attr("style");
            ok(style.indexOf("width: 33%") !== -1, "The third width is set correctly.");
            ok(style.indexOf("margin-left: 66%") !== -1, "The third left margin is set correctly.");
            evcal.wijevcal("deleteEvent", "appt1");
            evcal.wijevcal("deleteEvent", "appt2");
            evcal.wijevcal("deleteEvent", "appt3");
            evcal.remove();
            start();
        }, 1000);
        stop();
    });

    test("#20597, Validation for Start-time and End-time is not provided when Start-date or End-date is changed", function () {
        var evcal = createEventscalendar(), myDate = new Date();
        evcal.wijevcal({
            eventsData: [{ id: "appt1", subject: "app1", start: new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), 10, 00), end: new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate() + 1, 9, 00) }]
        });
        var oEvcal = evcal.data("wijmo-wijevcal");
        $("div." + oEvcal._eventIdToCssClass("appt1")).trigger("click");
        var dlg = oEvcal._editEventDialog;
        dlg.find(".wijmo-wijev-end").wijinputdate("option", "date", new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), 9, 00));
        raises(function () {
            oEvcal._validateAndReadApptDialogFields(dlg);
        }, "The end date you entered occurs before the start date");
        evcal.wijevcal("deleteEvent", "appt1");
        evcal.remove();
    });
    
    if (!window["openDatabase"]) {
        test("#40308, update/delete event for none support openDatabase", function () {
            var amplify_bak = window["amplify"];
            window["amplify"] = null;
            var evcal = $("<div>").appendTo("body").wijevcal({ viewType: "list" });
            
            evcal.wijevcal("addEvent", { id: "appt1", subject: "app1", start: new Date(2013, 5, 23, 15, 30), end: new Date(2013, 5, 23, 17, 35) });
            evcal.wijevcal("deleteEvent", { id: "appt1", subject: "app1", start: new Date(2013, 5, 23, 8, 30), end: new Date(2013, 5, 23, 17, 35) });
            ok(evcal.find(".wijmo-wijev-agenda-event").length === 0, "the event is deleted by the deleteEvent method.");
            evcal.wijevcal("addEvent", { id: "appt1", subject: "app1", start: new Date(2013, 5, 23, 15, 30), end: new Date(2013, 5, 23, 17, 35) });
            evcal.find(".wijmo-wijev-agenda-event-title").simulate("click");
            evcal.find(".wijmo-wijev-appointment").simulate("click");
            evcal.find(".wijmo-wijev-start-time").wijinputdate({ date: new Date(2013, 5, 23, 8, 30) });
            evcal.find(".wijmo-wijev-save").simulate("click");
            ok(evcal.wijevcal("option", "eventsData")[0].start.toString() == new Date(2013, 5, 23, 8, 30).toString(), "the start is setted");
            evcal.remove();
            window["amplify"] = amplify_bak;
        });
    }

    test("#35656", function () {
        var evcal = $("<div>").appendTo("body").wijevcal();
        evcal.wijevcal("addCalendar", { color: "default", description: "", location: "", name: "C1" }, null, null);
        var calendars = evcal.wijevcal("option", "calendars");
        ok(calendars[0].name === "C1", "the C1 calendar added.");
        evcal.wijevcal("addCalendar", { color: "default", description: "", location: "", name: "C1" }, null, null);
        ok(calendars.length === 1, "Added an exist name calendar, it will not added to calendars.");
        evcal.wijevcal("option", "visibleCalendars", ["C1"]);
        var c1calendar = calendars[0];
        c1calendar.name = "C2";
        evcal.wijevcal("updateCalendar", c1calendar, null, null);
        ok(evcal.wijevcal("option", "visibleCalendars")[0] === "C2", "The visibleCalendars has changed to C2.");
        evcal.wijevcal("deleteCalendar", c1calendar, null, null);
        evcal.remove();
    });
    test("#41225", function () {
        var evcal = $("<div>").appendTo("body").wijevcal({ viewType: "list" }),
            length = evcal.find(".wijmo-wijev-agenda-event").length;
        evcal.wijevcal("addEvent", { id: "appt1", subject: "app1", start: new Date(2013, 5, 23, 15, 30), end: new Date(2013, 5, 23, 17, 35) });
        ok(evcal.find(".wijmo-wijev-agenda-event").length === length + 1, "the event is added.");
        evcal.wijevcal({ navigationBarVisible: false });
        evcal.wijevcal({ navigationBarVisible: true });
        ok(evcal.find(".wijmo-wijev-agenda-event").length === length + 1, "After set the options, the list event only contains one event.");
        evcal.wijevcal("deleteEvent", { id: "appt1", subject: "app1", start: new Date(2013, 5, 23, 8, 30), end: new Date(2013, 5, 23, 17, 35) });
        evcal.remove();
    });
    test("#41255", function () {
        var evcal = $("<div>").appendTo("body").wijevcal({ viewType: "list" }),
            timeinterval = $(".wijmo-wijev-timeruler>.wijmo-wijev-timerulerinterval", evcal),
            defaultLabel = timeinterval.first().text();

        evcal.wijevcal("option", "timeRulerFormat", "{0: HH mm}");
        setTimeout(function () {
            timeinterval = $(".wijmo-wijev-timeruler>.wijmo-wijev-timerulerinterval", evcal);
            ok(timeinterval.first().text() !== defaultLabel, "The label text has changed.");
            evcal.remove();
            start();
        }, 200);
        stop();
    });
    test("#41050", function () {
        var tab = $("<div id='tabs'>" +
                "<ul>" +
                "<li><a href='#tabs-1'>EventCal1</a></li>" +
                "<li><a href='#tabs-2'>EventCal2</a></li>" +
                "</ul>" +
                "<div id='tabs-1'>" +
                "</div>" +
                "<div id='tabs-2'>" +
                "</div>" +
            "</div>").appendTo(document.body).wijtabs();

        var evcal1 = $('<div id="eventscalendar1"></div>').wijevcal();
        var evcal2 = $('<div id="eventscalendar2"></div>').wijevcal();
        evcal1.appendTo(document.getElementById("tabs-1"));
        evcal1.wijevcal("refresh");

        evcal2.appendTo(document.getElementById("tabs-2"));

        var position_ViewContainer1 = evcal1.find(".wijmo-wijev-view-container").position();
        var position_View1 = evcal1.find(".wijmo-wijev-view").position();

        secondTab = $($("a", tab)[1]);
        secondTab.simulate('click');

        var position_ViewContainer2 = evcal2.find(".wijmo-wijev-view-container").position();
        var position_View2 = evcal2.find(".wijmo-wijev-view").position();

        var result = ((position_ViewContainer1.top == position_ViewContainer2.top)
            && (position_ViewContainer1.left == position_ViewContainer2.left)
            && (position_View1.top == position_View2.top)
            && (position_View1.left == position_View2.left));

        ok(result, "Position of hidden Evcal is as same as position of visible one.");

        tab.remove();
        evcal1.remove();
        evcal2.remove();
    });
    test("#41217", function () {
        // day view
        var evcal = $("<div>").appendTo("body").wijevcal(),
            calendar = evcal.find(".wijmo-wijcalendar");
        ok(calendar.find(".ui-datepicker-week-day:first").text().trim() === "Su", "the default first day of the calender is sunday.");
        evcal.wijevcal({ firstDayOfWeek: 1 });
        ok(calendar.find(".ui-datepicker-week-day:first").text().trim() === "Mo", "the first day of the calender is monday.");
        evcal.remove();
        // week view
        evcal = $("<div>").appendTo("body").wijevcal({ viewType: "week" });
        ok(evcal.find(".wijmo-wijev-dayview-inner .wijmo-wijev-daylabel:first").text().indexOf("Sunday") > -1, "The default first day of week is sunday in week view.");
        evcal.wijevcal({ firstDayOfWeek: 1 });
        ok(evcal.find(".wijmo-wijev-dayview-inner .wijmo-wijev-daylabel:first").text().indexOf("Monday") > -1, "The first day of week is monday in week view.");
        evcal.remove();
        // month view
        evcal = $("<div>").appendTo("body").wijevcal({ viewType: "month" });
        ok(evcal.find(".wijmo-wijev-monthview .wijmo-wijev-monthcellheader:first").text().indexOf("Sun") > -1, "the default first day of week is sunday in month week");
        evcal.wijevcal({ firstDayOfWeek: 1 });
        ok(evcal.find(".wijmo-wijev-monthview .wijmo-wijev-monthcellheader:first").text().indexOf("Mon") > -1, "the first day of week is monday in month week");
        evcal.remove();
    });

    test("#41489", function () {
        var evcal = $("<div>").appendTo("body").wijevcal({ viewType: "list", selectedDate: new Date("2013/7/7") }), appointElement, top;
        evcal.wijevcal("addEvent", {
            id: "event1",
            start: new Date(2013, 6, 7, 10, 30),
            end: new Date(2013, 6, 7, 11, 00),
            subject: "Subject"
        });

        appointElement = evcal.find(".wijmo-wijev-appointment");
        top = appointElement.css("top");
        evcal.wijevcal("updateEvent", {
            id: "event1",
            start: new Date(2013, 6, 7, 13, 30),
            end: new Date(2013, 6, 7, 15, 00),
            subject: "Subject1"
        });
        appointElement = evcal.find(".wijmo-wijev-appointment");
        ok(appointElement.css("top") !== top, "The appoint statetime has changed");
        ok(appointElement.find(".wijmo-wijev-title").text().trim() === "Subject1", "The title has changed.");
        evcal.wijevcal("deleteEvent", "event1");
        evcal.remove();
    });

    test("#41441", function () {
        var evcal = $("<div>").appendTo("body").wijevcal({
            viewType: "list",
            selectedDate: new Date(2013, 4, 6),
            eventsData: [{
                id: "appt1",
                start: new Date(2013, 4, 6, 17, 00),
                end: new Date(2013, 4, 6, 17, 30),
                subject: "Appointment1"
            }, {
                id: "appt2",
                start: new Date(2013, 4, 7, 17, 00),
                end: new Date(2013, 4, 7, 17, 30),
                subject: "Appointment2"
            }]
        });

        evcal.find(".wijmo-wijev-agenda-event:first").simulate("click");
        evcal.find(".wijmo-wijev-dayview .wijmo-wijev-content:first").simulate("click");
        ok(evcal.find(".wijmo-wijev-delete").is(":visible"), "the delete button is visible.");
        evcal.wijevcal("deleteEvent", "appt1");
        evcal.wijevcal("deleteEvent", "appt2");
        evcal.remove();
    });

    test("#41778", function () {
        var evcal = $("<div>").appendTo("body").wijevcal({
            viewType: "list",
            selectedDate: new Date(2013, 4, 6),
            eventsData: [{
                id: "appt1",
                start: new Date(2013, 4, 6, 17, 00),
                end: new Date(2013, 4, 6, 17, 30),
                subject: "Appointment1"
            }, {
                id: "appt2",
                start: new Date(2013, 4, 7, 17, 00),
                end: new Date(2013, 4, 7, 17, 30),
                subject: "Appointment2"
            }]
        });

        evcal.find(".wijmo-wijev-agenda-event:first").simulate("click");
        evcal.find(".wijmo-wijev-dayview .wijmo-wijev-timeinterval:first").simulate("click");
        ok(evcal.find(".wijmo-wijev-delete").is(":hidden"), "the delete button is visible.");
        evcal.wijevcal("deleteEvent", "appt1");
        evcal.wijevcal("deleteEvent", "appt2");
        evcal.remove();
    });

    test("#41787", function () {
        var container = $("<div>").appendTo("body"),
            evcal = $("<div>").appendTo(container);
        evcal.wijevcal();
        container.wijsuperpanel();
        ok(true, "no exception thrown.");
        container.remove();
    });

    test("#42231", function () {
        var eventscalendar1 = createEventscalendar();

        window.setTimeout(function () {
            $(eventscalendar1.find(".wijmo-wijev-timeinterval")[10]).simulate("click");
            $(eventscalendar1.find(".wijmo-wijinput-trigger")[0]).simulate("click");
            window.setTimeout(function () {
                $(".ui-datepicker-title").simulate("mousedown").simulate("mouseup");
                window.setTimeout(function () {
                    ok($(".wijmo-wijev-event-dialog").css("display") !== "none", "The event dialog won't be closed after the triggering mouse up event of datepicker title.");
                    eventscalendar1.remove();
                    $(".wijmo-wijcalendar").remove();
                    start();
                }, 0);
            }, 400);
        }, 400);
        stop();
    });


    test("#42178", function () {
        var evcal = $("<div>").appendTo("body").wijevcal({
            selectedDate: new Date(2013, 4, 6),
            eventsData: [{
                id: "appt1",
                start: new Date(2013, 4, 6, 17, 00),
                end: new Date(2013, 4, 6, 17, 30),
                subject: "Appointment1"
            }, {
                id: "appt2",
                start: new Date(2013, 4, 6, 16, 00),
                end: new Date(2013, 4, 6, 16, 30),
                subject: "Appointment2"
            }]
        });
        ok($("li.wijmo-wijev-agenda-event", ".wijmo-wijev-agenda-day-container").length === 2, "All the events has created");
        var containerHeight = $(".wijmo-wijev-agenda-day-container").parents(".wijmo-wijsuperpanel-statecontainer").height(),
            itemHeight = $("li.wijmo-wijev-agenda-event", ".wijmo-wijev-agenda-day-container").height();

        ok(containerHeight >= itemHeight * 2, "All the events items can see");
        evcal.wijevcal("deleteEvent", "appt1");
        evcal.wijevcal("deleteEvent", "appt2");
        evcal.remove();
    });

    test("#41607", function () {
        var container1 = $("<div style='height:20px'>").appendTo("body"),
            container2 = $("<div>").appendTo("body"),
            evcal = $("<div>").appendTo(container2), dialogElement;

        evcal.wijevcal({ ensureEventDialogOnBody: true });
        $(".wijmo-wijev-allday-cell", container2).simulate("click");

        dialogElement = evcal.data("wijmo-wijevcal")._editEventDialog;
        ok(dialogElement.offset().top < container2.offset().top, "The event dialog shows within the container of wijeval control.");
        evcal.remove();
        container1.remove();
        container2.remove();
    });

    test("#41519", function () {
        var evcal41519 = $("<div id='div41519'>").appendTo("body").wijevcal({ statusBarVisible: true });
        evcal41519.wijevcal("addEvent", { id: "appt1_41519", subject: "app1_41519", start: new Date(2013, 9, 29, 8, 30), end: new Date(2013, 9, 29, 9, 30) });

        var navigationbar = evcal41519.find(".wijmo-wijev-navigationbar");
        var statusbar = evcal41519.find(".wijmo-wijev-statusbar");
        ok($(navigationbar).position().top < $(statusbar).position().top, "Statusbar shows below Navigationbar");
        evcal41519.wijevcal("deleteEvent", "appt1_41519");
        evcal41519.remove();
    });

    test("#42892", function () {
        var evcal = $("<div>").appendTo("body").wijevcal();
        evcal.find(".wijmo-wijev-timeinterval:eq(10)").simulate("click");
        setTimeout(function () {
            ok($(".wijmo-wijev-event-dialog", evcal).is(":visible"), "the event editor dialog is shown.");
            // if use simulate, it will not bubble, use mouseup instead of it.
            evcal.find(".wijmo-wijdatepager-pagelabel").eq(10).mouseup();
            ok($(".wijmo-wijev-event-dialog", evcal).is(":hidden"), "after click mouseup another date in datepager, the dialog hide.");
            evcal.remove();
            start();
        }, 200);
        stop();
    });

    test("#43981:Test the status bar shows inside the wijevcal control", function () {
        var evcal = $("<div>").appendTo("body").wijevcal(),
            viewContainer = evcal.find(".wijmo-wijev-view-container"),
            containerHBeforeUpdating = viewContainer.outerHeight(true),
            containerHAfterUpdating,
            statusbar = evcal.find(".wijmo-wijev-statusbar"),
            statusbarH;
        
        evcal.wijevcal("status", "Testing 123");
        evcal.wijevcal("option", "statusBarVisible", true);

        containerHAfterUpdating = viewContainer.outerHeight(true);
        statusbarH = statusbar.outerHeight(true);

        ok(containerHAfterUpdating === containerHBeforeUpdating - statusbarH, "The height of the view contrainer will minus the height the height of the status bar after the status bar showing.");
        evcal.remove();
    });

    // In touch mode, the superpanel use browser's scrollbar, both in touch mode and none touch mode, the _listViewAgendaScrolled will called, and with the same arguments.
    // In none touch mode, we can easily simulate the scrollbar to scroll to end. So here just test in none touch mode.
    if (!($.support.isTouchEnabled && $.support.isTouchEnabled())) {
        test("#42885", function () {
            var evcal = $("<div>").appendTo("body").wijevcal({
                eventsData: [{
                    "id": "02FA085E-D019-5E09-4DE6-C3122B5D77F4", "calendar": "Default", "subject": "New event", "location": "", "start": "2013-10-03T07:30:00:000Z", "end": "2013-10-03T10:30:00:000Z", "description": "", "color": "cornflowerblue", "allday": false, "tag": null, "parentRecurrenceId": null, "recurrenceState": "master", "recurrencePattern": {
                        "parentRecurrenceId": null, "recurrenceType": "daily", "interval": 0, "startTime": "2013-10-03T07:30:00:000Z", "endTime": "2013-10-03T10:30:00:000Z", "patternStartDate": "2013-10-03T07:30:00:000Z", "patternEndDate": "0001-01-01T00:00:00:000Z", "occurrences": 0, "instance": "first", "dayOfWeekMask": "none", "dayOfMonth": 0, "monthOfYear": 0, "noEndDate": true, "exceptions": [], "removedOccurrences": []
                    }
                }],
                viewType: "list"
            }), superpanel;
                
            setTimeout(function () {
                superpanel = evcal.find(".wijmo-wijev-list-details .wijmo-wijev-agenda-container");
                superpanel.wijsuperpanel("vScrollTo", 100, true);
                setTimeout(function () {
                    ok(true, "no exception thrown.");
                    evcal.wijevcal("deleteEvent", "02FA085E-D019-5E09-4DE6-C3122B5D77F4");
                    evcal.remove();
                    start();
                }, 200);
            }, 500);
            stop();
        });
    }
    
    test("#44744, #57248:New Event dialog is closed down when clicking a date in dropdown calendar of Start/End date", function () {
        var evcal = $("<div>").appendTo("body").wijevcal(),
            firstTimeSlot = $(evcal.find(".wijmo-wijev-timeinterval")[0]),
            eventDialog, startDateInput, startDateInputTrigger, calendar,
            dayOutsideEventDialog;
        
        firstTimeSlot.simulate("click", { clientX: firstTimeSlot.offset().left + firstTimeSlot.width(), clientY: firstTimeSlot.offset().top });
        eventDialog = evcal.find(".wijmo-wijev-event-dialog");
        startDateInput = eventDialog.find(".wijmo-wijev-start");
        startDateInputWidget = startDateInput.data("wijmo-wijinputdate");
        calendar = startDateInputWidget.element.data("calendar");
        startDateInputTrigger = $(eventDialog.find(".wijmo-wijinput-trigger")[0]);

        startDateInputTrigger.simulate("click");
        ok(eventDialog.is(":visible"), "event dialog is visible");
        ok(calendar.is(":visible"), "calendar is visible");
        
        setTimeout(function () {
            eventDialog.simulate("mouseup");
            ok(eventDialog.is(":visible"), "event dialog is visible when mouseup in event dialog");
            ok(!calendar.is(":visible"), " inputdate's calendar isn't visible when mouseup in event dialog");
            
            //open inpudate's calendar again.
            startDateInputTrigger.simulate("click");
            ok(calendar.is(":visible"), "calendar is visible");
            dayOutsideEventDialog = $(calendar.find("table tbody tr")[0]).find("td")[6];
            $(dayOutsideEventDialog).simulate("mousedown");
            $(dayOutsideEventDialog).simulate("mouseup");
            
            //waiting for calendar closing.
            setTimeout(function () {
                ok(!calendar.is(":visible"), " inputdate's calendar isn't visible when clicking inputdate's calendar");
                ok(eventDialog.is(":visible"), "event dialog is visible when clicking inputdate's calendar");
                //open inpudate's calendar again.
                startDateInputTrigger.simulate("click");
                ok(calendar.is(":visible"), "calendar is visible");
                eventDialog.simulate("mouseup");
                ok(!calendar.is(":visible"), " inputdate's calendar isn't visible when mouseup on bocument.body");
                
                $(document.body).simulate("mouseup");
                ok(!eventDialog.is(":visible"), "event dialog isn't visible when when mouseup on bocument.body");
                evcal.remove();
                start();
            }, 500);
        }, 0);

        stop();
    });

    test("#46932", function () {
        var evcal = $("<div>").appendTo("body").wijevcal({ viewType: "month" }),
            weekTitle, headerContainer, scrollContainer, weekView;
        evcal.wijevcal({ viewType: "week" });
        weekView = $(".wijmo-wijev-weekview");
        weekTitle = weekView.find(".wijmo-wijev-header-title");
        headerContainer = weekView.find(".wijmo-wijev-headercontainer");
        scrollContainer = weekView.find(".wijmo-wijev-scrollpanel");
        ok(Math.floor(weekView.height()) === Math.floor(weekTitle.outerHeight(true) + headerContainer.outerHeight(true) + scrollContainer.outerHeight(true)), "The height is correct");
        evcal.remove();
    });

    test("#48912", function () {
        var evcal = $("<div>").appendTo("body").wijevcal({ viewType: "month" });
        evcal.wijevcal({ viewType: "week" });
        evcal.wijevcal("option", "viewType", "list");
        ok($(".wijmo-wijev-dayheadercolumn", evcal).length === 1, "only one view on list view");
        evcal.wijevcal({ viewType: "week" });
        evcal.wijevcal("option", "viewType", "day");
        ok($(".wijmo-wijev-dayheadercolumn", evcal).length === 1, "only one view on day view");
        evcal.remove();
    });

    test("#49394", function () {
        var evcal = $("<div>").appendTo("body").wijevcal();
        ok($(".wijmo-wijev-monthview", evcal).length === 0, "Now is day view");
        evcal.wijevcal({ viewType: "month" });
        evcal.wijevcal("option", "viewType", "list");
        ok($(".wijmo-wijev-monthview", evcal).length > 0, "Has chenged to month view");
        evcal.remove();
    });

    test("#51376", function () {
        var evcal1 = $("<div id=\"evcal1\">").appendTo("body").wijevcal({ viewType: "list" }),
            evcal2 = $("<div id=\"evcal2\">").appendTo("body").wijevcal({ viewType: "list" });

        evcal1.wijevcal("addEvent", {
            id: "appt1",
            subject: "event in evcal1",
            start: new Date(2014, 2, 24, 15, 30),
            end: new Date(2014, 2, 24, 17, 30)
        });
        evcal2.wijevcal("addEvent", {
            id: "appt2",
            subject: "event in evcal2",
            start: new Date(2014, 2, 24, 13, 30),
            end: new Date(2014, 2, 24, 15, 30)
        });

        ok($("#evcal1 .wijmo-wijev-agenda-list").children("li").length === 1, "Only one event was added in the first event calendar");
        ok($("#evcal2 .wijmo-wijev-agenda-list").children("li").length === 1, "Only one event was added in the second event calendar");

        evcal1.wijevcal("deleteEvent", "appt1");
        evcal2.wijevcal("deleteEvent", "appt2");
        evcal1.remove();
        evcal2.remove();
    });

    if ($.support.isTouchEnabled && $.support.isTouchEnabled()) {
        test("#56500", function () {
            var evcal1 = $("<div id=\"evcal1\">").appendTo("body").wijevcal({ viewType: "list" });

            ok($(".wijmo-wijev-daycolumn").height() > 200, "The day column's height is right!");
            evcal1.remove();
        });
    }

    test("#61529", function () {
        var container1 = $("<div style='height:100px'>").appendTo("body"),
            evcal = $("<div>").appendTo("body");

        evcal.wijevcal();
        setTimeout(function () {
            $(".wijmo-wijev-dayview .wijmo-wijev-minute-780").simulate("click");
            ok($(".wijmo-wijev-event-dialog").offset().top + $(".wijmo-wijev-event-dialog").height() < $("body").offset().top + $("body").height(), "The event dialog shows within the visible region of body");
            container1.remove();
            evcal.remove();
            start();
        }, 0);
        stop();
    });

    test("#68232", function () {
        var container68232 = $("<div id='con68232'></div>").appendTo("body"),
            evcal68232 = $("<div id='evc68232'></div>").appendTo(container68232),
            appointment, dayViewSuperpanel, sppHScrollValue;

        evcal68232.wijevcal();
        evcal68232.wijevcal("addEvent", {
            id: "app1",
            start: new Date(),
            end: new Date(),
            allday: true,
            subject: "Appointment One",
            color: "red"
        });

        appointment = evcal68232.find(".wijmo-wijev-appointment").eq(0);
        appointment.simulate("mouseover").simulate("drag", { dx: 0, dy: -20 });

        dayViewSuperpanel = evcal68232.find(".wijmo-wijev-dayview-inner .wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");

        sppHScrollValue = dayViewSuperpanel.options.hScroller.scrollValue;

        ok(!sppHScrollValue, "Superpanel does not move !");
        evcal68232.wijevcal("deleteEvent", "app1");
        evcal68232.wijevcal("destroy");
        evcal68232.remove();
        container68232.remove();
    });

    test("#68242", function () {
        var container68242 = $("<div id='con68242'></div>").appendTo("body"),
            evcal68242 = $("<div id='evc68242'></div>").appendTo(container68242),
            dayViewSuperpanel, firstScrollBarValue, lastScrollValue,
            startDate = new Date(),
            end = new Date(),
            newDate = new Date();

            startDate.setHours(8);
            startDate.setMinutes(30);
            end.setHours(8);
            end.setMinutes(30);
            newDate.setHours(23);
            newDate.setMinutes(30);

        evcal68242.wijevcal();
        evcal68242.wijevcal("addEvent", {
            id: "app1",
            start: startDate,
            end: end,
            subject: "Appointment One",
            color: "red"
        });

        dayViewSuperpanel = evcal68242.find(".wijmo-wijev-dayview-inner .wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
        firstScrollBarValue = dayViewSuperpanel.options.vScroller.scrollValue ? dayViewSuperpanel.options.vScroller.scrollValue : 0;
        evcal68242.wijevcal("goToTime", newDate);

        setTimeout(function () {
            lastScrollValue = dayViewSuperpanel.options.vScroller.scrollValue ? dayViewSuperpanel.options.vScroller.scrollValue : 0;
            ok(lastScrollValue > firstScrollBarValue, "No error, The scrollbar's position is changed!");
            evcal68242.wijevcal("deleteEvent", "app1");
            evcal68242.wijevcal("destroy");
            evcal68242.remove();
            container68242.remove();
            start();
        }, 200);
        stop();
    });

    test("#75801-starttime", function () {
        var evcal = $("<div>").appendTo("body");

        evcal.wijevcal();
        $(".wijmo-wijev-dayview .wijmo-wijev-minute-780").simulate("click");
        $(".wijmo-wijev-start-time", evcal).simulateKeyStroke("[DELETE][DELETE][DELETE][DELETE]", null, function () {
            ok(1, "the browser is not crashed.");
            evcal.remove();
            start();
        });
        stop();
    });

    test("#75801-startdate & starttime", function () {
        var evcal = $("<div>").appendTo("body");

        evcal.wijevcal();
        $(".wijmo-wijev-dayview .wijmo-wijev-minute-780").simulate("click");

        $(".wijmo-wijev-start", evcal).simulateKeyStroke("[DELETE][DELETE][DELETE][DELETE]");
        $(".wijmo-wijev-start-time", evcal).simulateKeyStroke("[UP][UP]", null, function () {

            ok(1, "the browser is not crashed.");
            evcal.remove();
            start();
        });
        stop();
    });

    test("#77151", function () {
        var evcal = $("<div></div>").appendTo("body"),
            startDate = new Date(),
            end = new Date();

        evcal.wijevcal({
            dataStorage: {
                loadEvents: function (visibleCalendars, successCallback, errorCallback) {
                    var event = evcal.data("wijmo-wijevcal")._eventsDataById["app1"];
                    if (event) {
                        successCallback([event]);
                        ok(evcal.data("wijmo-wijevcal")._eventsView.length === 1, "Has not create duplicated events");
                    }
                },
            }
        });

        evcal.wijevcal("addEvent", {
            id: "app1",
            start: startDate,
            end: end,
            subject: "Appointment One"
        });

        evcal.wijevcal();
        evcal.wijevcal("deleteEvent", "app1");
        evcal.remove();
    });

    test("#77188", function () {
        var eventscalendar = createEventscalendar({
            visibleCalendars: ["My", "Work"],
            dataStorage:
            {
                loadCalendars: function (successCallback) {
                    var calendars = [
                        { name: "My", id: "My", color: "red" },
                        { name: "Work", id: "Work", color: "blue" }
                    ];
                    successCallback(calendars);
                }
            }
        });
        window.setTimeout(function () {
            $(eventscalendar.find(".wijmo-wijev-timeinterval")[10]).simulate("click");
            window.setTimeout(function () {
                ok($(".wijmo-wijev-event-dialog .wijmo-wijev-color").hasClass("wijmo-wijev-event-color-red"));
                eventscalendar.find(".wijmo-wijev-cancel").simulate("click");
                $(eventscalendar.find(".wijmo-wijev-timeinterval")[7]).simulate("click");
                window.setTimeout(function () {
                    ok($(".wijmo-wijev-event-dialog .wijmo-wijev-color").hasClass("wijmo-wijev-event-color-red"));
                    eventscalendar.wijevcal("destroy");
                    eventscalendar.remove();
                    start();
                }, 400);
            }, 400);
        }, 400);
        stop();
    });

    test("#78524", function () {
        var eventscalendar = createEventscalendar(),
            scrollContent = eventscalendar.find(".wijmo-wijev-scrollcontent"),
            timeRuler = scrollContent.find(".wijmo-wijev-timeruler"),
            dayColumn = scrollContent.find(".wijmo-wijev-daycolumn");

        ok(scrollContent.width() === timeRuler.outerWidth() + dayColumn.outerWidth(), "ScrollContent has get suitable width !");
        eventscalendar.remove();
    });

    test("#99639", function () {
        var eventscalendar = createEventscalendar({
            editEventDialogTemplate: "<div class=\"customdialog-sample wijmo-wijev-event-dialog ui-widget-content ui-corner-all\">" +
                        "<div class=\"wijmo-wijev-angle\"></div>" +
                    "</div>"
        });

        $(eventscalendar.find(".wijmo-wijev-timeinterval")[10]).simulate("click");
        ok(true, "JS error doesn't occur !");
        eventscalendar.remove();
    });

    test("#106067", function () {
        var eventscalendar = createEventscalendar({ views: ["day", "week", { name: "2 Month", unit: "month", count: 2 }] }),
            customViewBtn = eventscalendar.find(".wijmo-wijev-custom");
        $(customViewBtn).simulate("click");

        ok(eventscalendar.find(".wijmo-wijev-monthview").length === 1);
        eventscalendar.remove();
    });

    test("#106108", function () {
        var eventscalendar = createEventscalendar();
        eventscalendar.wijevcal("option", "viewType", "");
        ok(eventscalendar.find(".wijmo-wijev-day-details").eq(0).is(':visible'));
        eventscalendar.remove();
    });

    test("#106092", function () {
        var eventscalendar = createEventscalendar({ views: ["day", "month"] });
        eventscalendar.wijevcal("option", "views", ["week"]);
        ok(eventscalendar.find(".wijmo-wijev-day-details").eq(0).is(':hidden'));
        eventscalendar.remove();
    });

    test("#105939", function () {
        var eventscalendar = createEventscalendar({
            views: ["day", "week", "list", { name: "3 Days", unit: "day", count: 3 }, { name: "1 Weeks", unit: "week", count: 1 }, { name: "1 Months", unit: "month", count: 1 },
                        { name: "2 Years", unit: "year", count: 2 }],
            viewType: "1 Weeks"
        });

        ok(eventscalendar.find(".wijmo-wijev-header-title").html().indexOf("-") > 0, "single week's header title text format should be xxx-xxx");
        eventscalendar.remove();
    });

    test("#106436", function () {
        var eventscalendar = createEventscalendar({
            views: [{ name: "2 Weeks", unit: "week", count: 3 }],
            selectedDate: new Date(2015, 1, 5)
        });
        eventscalendar.wijevcal("goLeft");
        ok(eventscalendar.find(".wijmo-wijev-header-title").html() === "1/11/2015 - 1/31/2015");
        eventscalendar.wijevcal("goRight");
        ok(eventscalendar.find(".wijmo-wijev-header-title").html() === "2/1/2015 - 2/21/2015");
        eventscalendar.wijevcal("goRight");
        eventscalendar.wijevcal("goRight");
        eventscalendar.wijevcal("goRight");
        ok(eventscalendar.find(".wijmo-wijev-header-title").html() === "4/5/2015 - 4/25/2015");
        ok(!eventscalendar.find(".wijmo-wijdatepager-pagelabel-last").hasClass("ui-state-active"));
        eventscalendar.remove();
    });

    test("#106911", function () {
        var eventscalendar = createEventscalendar({
            views: [{ name: "2 Months", unit: "month", count: 2 }, { name: "2 Years", unit: "year", count: 2 }],
            selectedDate: new Date(2015, 1, 6)
        }), monthView, yearView;
        // superpanel re-create everytime
        monthView = $(".wijmo-wijev-monthview");
        monthView.find(".wijmo-wijsuperpanel").wijsuperpanel("option", { vScroller: { scrollValue: 60 } });
        $(".wijmo-wijev-date_2015_2_1 .wijmo-wijev-monthcell").simulate("click");
        eventscalendar.find(".wijmo-wijev-save").simulate("click");
        ok(monthView.find(".wijmo-wijsuperpanel").wijsuperpanel("option", "vScroller").scrollValue === 60);
        eventscalendar.find(".wijmo-wijev-event-title").simulate("click");
        eventscalendar.find(".wijmo-wijev-delete").simulate("click");
        ok(monthView.find(".wijmo-wijsuperpanel").wijsuperpanel("option", "vScroller").scrollValue === 60);
        eventscalendar.find(".wijmo-wijev-custom").eq(1).simulate("click");
        yearView = eventscalendar.find(".wijmo-wijev-yearview");
        yearView.find(".wijmo-wijsuperpanel").wijsuperpanel("option", { vScroller: { scrollValue: 60 } });
        eventscalendar.find(".wijmo-wijev-month_2016_1_1 .wijmo-wijev-yearcell").simulate("click");
        eventscalendar.find(".wijmo-wijev-save").simulate("click");
        ok(yearView.find(".wijmo-wijsuperpanel").wijsuperpanel("option", "vScroller").scrollValue === 60);
        eventscalendar.find(".wijmo-wijev-event-title").simulate("click");
        eventscalendar.find(".wijmo-wijev-delete").simulate("click");
        ok(yearView.find(".wijmo-wijsuperpanel").wijsuperpanel("option", "vScroller").scrollValue === 60);
        eventscalendar.remove();
    });

    test("#106934", function () {
        var eventscalendar = createEventscalendar({
            views: [{ name: "3 Months", unit: "month", count: 3 }],
            selectedDate: new Date(2015, 1, 6)
        });
        ok(eventscalendar.find(".wijmo-wijev-date_2015_2_1").outerHeight() <= 82);
        eventscalendar.remove();
    });

    test("#106395", function () {
        var eventscalendar = createEventscalendar({
            selectedDate: new Date(2015, 1, 27)
        });
        eventscalendar.wijevcal("showEditCalendarDialog");
        $(".wijmo-wijev-editcalendar-dialog .wijmo-wijev-color-button").simulate("click");
        ok($(".wijmo-wijev-color-menu").is(":visible"));
        $(".wijmo-wijdialog-titlebar-close").simulate("click");
        eventscalendar.find(".wijmo-wijev-minute-0").simulate("click");
        $(".wijmo-wijev-event-dialog .wijmo-wijev-color-button").simulate("click");
        ok($(".wijmo-wijev-color-menu").is(":visible"));
        eventscalendar.remove();
    });

    test("#110032", function () {
        var eventscalendar = createEventscalendar({
            views: [{ name: "2 Weeks", unit: "week", count: 2 }],
            selectedDate: new Date(2015, 1, 25)
        });
        eventscalendar.find(".ui-icon-triangle-1-w").simulate("click");
        eventscalendar.wijevcal("goToDate", new Date(2015, 2, 6));
        ok(eventscalendar.find(".wijmo-wijdatepager-pagelabel").eq(3).hasClass("ui-state-active"))
        eventscalendar.remove();
    });

    test("#109113", function () {
        var eventscalendar = createEventscalendar(),
            monthViewBtn = eventscalendar.find(".wijmo-wijev-month"),
            listViewBtn = eventscalendar.find(".wijmo-wijev-list");
        $(monthViewBtn).simulate("click");
        $(listViewBtn).simulate("click");
        ok(eventscalendar.find(".wijmo-wijev-list-details").is(":visible"));
        eventscalendar.remove();
    });

    test("#109162", function () {
        var eventscalendar = createEventscalendar({
            views: [{ name: "2 Days", unit: "day", count: 2 }],
            selectedDate: new Date(2015, 2, 1)
        });
        eventscalendar.wijevcal("goToDate", new Date(2015, 2, 6));
        ok(eventscalendar.find(".wijmo-wijdatepager-pagelabel").eq(5).hasClass("ui-state-active"))
        eventscalendar.remove();
    });

    test("#111796", function () {
        var eventscalendar = createEventscalendar({
            views: ["month", { name: "2 Months", unit: "month", count: 2 }],
            selectedDate: new Date(2015, 2, 1)
        });
        eventscalendar.find(".wijmo-wijev-custom").simulate("click");
        ok(eventscalendar.find(".wijmo-wijdatepager-pages .ui-state-active").text() === "Mar-Apr, 2015")
        eventscalendar.remove();
    });

    test("#108535", function () {
        var panelHeight,eventscalendar = createEventscalendar({
            views: ["day", { name: "2 Months", unit: "month", count: 2 }]
        });
        panelHeight = eventscalendar.find(".wijmo-wijev-view-container").height();
        eventscalendar.wijevcal("option", "views", ["month"]);
        ok(eventscalendar.find(".wijmo-wijev-view-container").height() === panelHeight);
        eventscalendar.remove();
    });

    test("#108765", function () {
        var eventscalendar = createEventscalendar({
            views: [{ name: "1 Weeks", unit: "week", count: 1 }, { name: "1 Months", unit: "month", count: 1 },
                    { name: "2 Years", unit: "year", count: 2 }],
            viewType: "1 Weeks"
        }), headerWidth;
        headerWidth = eventscalendar.find(".wijmo-wijev-headerbar").width();
        eventscalendar.wijevcal("option", "rightPaneVisible", true);
        ok(eventscalendar.find(".wijmo-wijev-headerbar").width() < headerWidth);
        eventscalendar.remove();
    });

    test("#108482", function () {
        var scrollValue, superPanel,
            eventscalendar = createEventscalendar({
                views: ["day", { name: "2 Weeks", unit: "week", count: 2 }],
                viewType: "day"
            });
        eventscalendar.find(".wijmo-wijev-custom").simulate("click");
        superPanel = eventscalendar.find(".wijmo-wijev-customview .wijmo-wijsuperpanel");
        superPanel.wijsuperpanel("hScrollTo", 30, false);
        eventscalendar.find(".wijmo-wijev-day").simulate("click");
        scrollValue = eventscalendar.find(".wijmo-wijev-headercontainer").css("left");
        ok(scrollValue === "-1px" || scrollValue === "-2px");
        eventscalendar.find(".wijmo-wijev-custom").simulate("click");
        scrollValue = eventscalendar.find(".wijmo-wijev-headercontainer").css("left");
        ok(scrollValue === "-30px");
        eventscalendar.remove();
    });

    test("#114538", function () {
        var eventscalendar = createEventscalendar({
            views: ["day", "week", "month", { name: "2 Weeks", unit: "week", count: 2 }, { name: "2 Months", unit: "month", count: 2 }, { name: "2 Years", unit: "year", count: 2 }],
            viewType: "week"
        }), opacity;
        eventscalendar.wijevcal("option","viewType","2 Weeks");
        opacity = eventscalendar.find(".wijmo-wijev-gmtlabel").css("opacity");
        ok(parseFloat(opacity) > 0);
        eventscalendar.wijevcal("option", "viewType", "day");
        opacity = eventscalendar.find(".wijmo-wijev-gmtlabel").css("opacity");
        ok(parseFloat(opacity) === 0);
        eventscalendar.remove();
    });

    test("#126380", function () {
        var startTime = new Date(2015, 7, 10, 3, 30),
            endTime = new Date(2015, 7, 10, 4, 30),
            oEvent, eventData, event1Id, event2Id,
            evcal = createEventscalendar({
                selectedDate: new Date(2015, 7, 10)
            });
        evcal.wijevcal("addEvent", {
            id: "app1",
            subject: "app1",
            start: startTime,
            end: endTime,
            recurrenceState: "master",
            recurrencePattern: {
                startTime: startTime,
                endTime: endTime,
                recurrenceType: "daily",
                patternStartDate: startTime
            }
        });
        eventData = evcal.wijevcal("option", "eventsData");
        ok(eventData != null && eventData.length == 1, "There should be one event.");
        ok(eventData[0].recurrenceState === "master", "The event should be recurring.");
        oEvent = evcal.wijevcal("findEventById", "apptid_app1_OCCR_2015_08_10");
        ok(oEvent != null, "The occurrence for 2015/8/10 is found.");
        oEvent.end = new Date(2015, 7, 10, 6, 30);
        evcal.wijevcal("updateEvent", oEvent);
        eventData = evcal.wijevcal("option", "eventsData");
        ok(eventData != null && eventData.length == 2, "There should be two events: recurring event and exception event.");
        ok(eventData[0].recurrenceState === "master", "The first event should be recurring.");
        ok(eventData[0].recurrencePattern != null
            && eventData[0].recurrencePattern.removedOccurrences != null
            && eventData[0].recurrencePattern.removedOccurrences.length == 1, "There would be one occurrence removed from the recurring event.");
        ok(eventData[1].id != "app1_OCCR_2015_08_10", "The id for the new event is not the old one.");
        event1Id = eventData[0].id;
        event2Id = eventData[1].id;
        evcal.wijevcal("deleteEvent", event1Id);
        evcal.wijevcal("deleteEvent", event2Id);
        evcal.remove();
    });

    test("The dialog for editing event should be removed when destroying the wijevcal.", function () {
        var evcal = $("<div>").appendTo("body"), dialogElement;

        evcal.wijevcal({ ensureEventDialogOnBody: true });
        $(".wijmo-wijev-allday-cell", evcal).simulate("click");

        dialogElement = $(".wijmo-wijev-event-dialog");
        ok(dialogElement.length && dialogElement.is(":visible"), "The event dialog is shown.");
        evcal.remove();
        dialogElement = $(".wijmo-wijev-event-dialog");
        ok(!dialogElement.length, "The dialog should be removed.");
    });

    test("#108765", function () {
        var eventscalendar = createEventscalendar({
            views: [{ name: "1 Weeks", unit: "week", count: 1 }, { name: "1 Months", unit: "month", count: 1 },
                    { name: "2 Years", unit: "year", count: 2 }],
            viewType: "1 Weeks"
        }), headerWidth;
        headerWidth = eventscalendar.find(".wijmo-wijev-headerbar").width();
        eventscalendar.wijevcal("option", "rightPaneVisible", true);
        ok(eventscalendar.find(".wijmo-wijev-headerbar").width() < headerWidth);
        eventscalendar.remove();
        });

    test("#149639", function () {
        var evcal = createEventscalendar({
            visibleCalendars: ["Office", "Home"],
            dataStorage: {
                deleteCalendar: function (obj, successCallback, errorCallback) {
                    successCallback();
                },
                loadCalendars: function (successCallback, errorCallback) {
                    var calendars = [{ name: "Office", id: "Office", color: "red" },
                        { name: "School", id: "School", color: "green" },
                        { name: "Home", id: "Home", color: "blue" },
                        { name: "Personal", id: "Personal", color: "gray" }
                    ];
                    successCallback(calendars);
                }
            }
        });
        evcal.wijevcal("showEditEventDialog");
        ok($($("ul", ".wijmo-wijev-event-dialog")[3]).text() === "OfficeHome");
        $(".wijmo-wijev-cancel").simulate("click");
        evcal.wijevcal("option", "visibleCalendars", ["Default"]);
        evcal.wijevcal("showEditEventDialog");
        ok($($("ul", ".wijmo-wijev-event-dialog")[3]).text() === "Default");
        evcal.remove();
    });

    test("#149639_HideEventsOfInvisibleCalendars", function () {
        var data = {
            loadCalendars: function (successCallback, errorCallback) {
                var calendars = [
                    { name: "Office", id: "Office", color: "red" },
                    { name: "School", id: "School", color : "green" },
                    { name: "Home", id: "Home", color: "blue" }];
                successCallback(calendars);
                }
        },
        evt1 = { id: "evt1", calendar: "Office", start: new Date(2016, 5, 16, 2, 0), end: new Date(2016, 5, 16, 4, 0), subject: "Office Evt1" },
        evt2 = { id: "evt2", calendar: "School", start: new Date(2016, 5, 16, 8, 0), end: new Date(2016, 5, 16, 10, 0), subject: "School Evt2" },
        evcal = createEventscalendar({
            dataStorage: data,
            viewType: "list"
        }), events;

        evcal.wijevcal("addEvent", evt1);
        evcal.wijevcal("addEvent", evt2);
        events = $(".wijmo-wijev-agenda-event-title", evcal).text().replace(/\s+/g, '');
        evcal.wijevcal("deleteEvent", evt1.id);
        evcal.wijevcal("deleteEvent", evt2.id);

        //Default scenario
        ok(events == "OfficeEvt1SchoolEvt2");
        evcal.remove();
        evcal = createEventscalendar({
            visibleCalendars: ["Office", "Home"],
            dataStorage: data,
            viewType: "list"
        })
        evcal.wijevcal("addEvent", evt1);
        evcal.wijevcal("addEvent", evt2);
        events = $(".wijmo-wijev-agenda-event-title", evcal).text().replace(/\s+/g, '');
        evcal.wijevcal("deleteEvent", evt1.id);
        evcal.wijevcal("deleteEvent", evt2.id);
        evcal.remove();

        //Not using default visible calendars
        ok(events == "OfficeEvt1");
    });

    test("#197760", function () {
        var evcal = createEventscalendar();
        evcal.wijevcal("option", "visibleCalendars", ["Office"]);
        evcal.wijevcal("addCalendar", { name: "Office", color: "red" });
        evcal.wijevcal("addCalendar", { name: "School", color: "green" });
        evcal.wijevcal("showEditEventDialog");
        ok($($("ul", ".wijmo-wijev-event-dialog")[3]).text() === "Office");
        $(".wijmo-wijev-cancel").simulate("click");
        evcal.wijevcal("option", "visibleCalendars", ["Default"]);
        evcal.wijevcal("showEditEventDialog");
        ok($($("ul", ".wijmo-wijev-event-dialog")[3]).text() === "Default");
        evcal.wijevcal("deleteCalendar", "Office");
        evcal.wijevcal("deleteCalendar", "School");
        evcal.remove();
    });

    test("#198536", function () {
        var evcal = createEventscalendar();
        ok(evcal.wijevcal("option", "visibleCalendars").length === 0);
        evcal.wijevcal("addCalendar", { name: "Office", color: "red" });
        evcal.wijevcal("addCalendar", { name: "School", color: "green" });
        evcal.wijevcal("showEditEventDialog");
        ok($($("ul", ".wijmo-wijev-event-dialog")[3]).text() === "OfficeSchoolDefault");
        $(".wijmo-wijev-cancel").simulate("click");
        evcal.wijevcal("option", "visibleCalendars", ["Default", "Office"]);
        evcal.wijevcal("showEditEventDialog");
        ok($($("ul", ".wijmo-wijev-event-dialog")[3]).text() === "OfficeDefault");
        evcal.wijevcal("deleteCalendar", "Office");
        evcal.wijevcal("deleteCalendar", "School");
        evcal.remove();
    });

    test("#203038", function () {
        var evcal = createEventscalendar({ selectedDate: new Date(2016, 7, 5) }), header = ".wijmo-wijev-header-title";
        evcal.wijevcal("option", "viewType", "week");
        ok($(header, ".wijmo-wijev-weekview").text().replace(/\s+/g, '') === "July-August2016");
        evcal.wijevcal("option", "viewType", "month");
        ok($(header, ".wijmo-wijev-monthview").text().replace(/\s+/g, '') === "August2016");
        evcal.wijevcal("option", "viewType", "day");
        ok($(".wijmo-wijev-monthday-container").text().replace(/\s+/g, '') === "5Friday,August52016");
        evcal.remove();
    });

})(jQuery)