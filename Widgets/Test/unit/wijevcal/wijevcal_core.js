/*globals test, ok, jQuery, module, document, $*/
/*
* wijevcal_core.js create & destroy
*/
"use strict";
function createEventscalendar(o) {
	return $('<div id="eventscalendar"></div>')
		.appendTo(document.body).wijevcal(o);
}


(function ($) {

	module("wijevcal: core");

	test("create and destroy", function () {
		var eventscalendar = createEventscalendar();
//wijmo-wijevcal wijmo-wijev ui-widget ui-helper-reset ui-state-default
		ok(eventscalendar.hasClass('wijmo-wijevcal wijmo-wijev ui-widget ui-helper-reset ui-state-default'),
			'create:element css classes created.');
		
		eventscalendar.wijevcal('destroy');
		ok(!eventscalendar.hasClass('wijmo-wijevcal wijmo-wijev ui-widget ui-helper-reset ui-state-default'),
			'destroy:element css classes removed.');
		
		eventscalendar.remove();
	});


}(jQuery));

