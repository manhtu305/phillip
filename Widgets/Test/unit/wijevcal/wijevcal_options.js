﻿/*globals test, ok, module, document, jQuery, stop, start, $ */
/*
 * wijevcal_options.js
 */
"use strict";
(function ($) {

    module("wijevcal:options");

    test("colors", function () {
        var eventscalendar1 = createEventscalendar();
        eventscalendar1.wijevcal("option", "colors", ["mycustom"]);
        window.setTimeout(function () {
            $(eventscalendar1.find(".wijmo-wijev-timeinterval")[10]).simulate("click");
            window.setTimeout(function () {
                $(".wijmo-wijev-event-dialog .wijmo-wijev-color-button").simulate("click");
                var listColors = $(".wijmo-wijev-event-dialog .wijmo-wijev-color-menu .wijmo-wijev-listcolor");
                ok(listColors.length === 1 &&
                    listColors.hasClass("wijmo-wijev-event-color-mycustom"),
                    "Only one color (mycustom) is available.");
                eventscalendar1.wijevcal("destroy");
                eventscalendar1.remove();
                start();

            }, 400);
        }, 400);
        stop();
    });

    test("dataStorage", function () {
        var eventscalendar, dataStorageTestHash = {};
        eventscalendar = createEventscalendar({
            dataStorage:
            {
                addEvent: function (obj, successCallback, errorCallback) {
                    if (obj.subject === "eventtest") {
                        dataStorageTestHash["addEvent"] = true;
                    }
                    successCallback();
                },
                updateEvent: function (obj, successCallback, errorCallback) {
                    if (obj.subject === "new_subject") {
                        dataStorageTestHash["updateEvent"] = true;
                    }
                    successCallback();
                },
                deleteEvent: function (obj, successCallback, errorCallback) {
                    dataStorageTestHash["deleteEvent"] = true;
                    successCallback();
                },
                loadEvents: function (visibleCalendars,
                                        successCallback, errorCallback) {
                    dataStorageTestHash["loadEvents"] = true;
                    successCallback([]);
                },
                addCalendar: function (obj, successCallback, errorCallback) {
                    successCallback();
                },
                updateCalendar: function (obj, successCallback, errorCallback) {
                    successCallback();
                },
                deleteCalendar: function (obj, successCallback, errorCallback) {
                    successCallback();
                },
                loadCalendars: function (successCallback, errorCallback) {
                    dataStorageTestHash["loadCalendars"] = true;
                    successCallback([]);
                }
            },
            initialized: function () {
                var ev = { id: "test_id", subject: "eventtest", start: new Date(), end: new Date() },
                    cal = { id: "calendar_id", name: "calendartest" }, evcal = $(this);

                evcal.wijevcal("addEvent", ev);

                ev.subject = "new_subject";
                evcal.wijevcal("updateEvent", ev);
                evcal.wijevcal("deleteEvent", ev);

                ok(dataStorageTestHash["loadEvents"], "loadEvents called.");
                ok(dataStorageTestHash["loadCalendars"], "loadCalendars called.");
                ok(dataStorageTestHash["addEvent"], "addEvent called.");
                ok(dataStorageTestHash["updateEvent"], "updateEvent called.");
                ok(dataStorageTestHash["deleteEvent"], "deleteEvent called.");

                //evcal.wijevcal("destroy");
                setTimeout(function () {
                    evcal.remove();
                    start();
                }, 300);
                
            }
        });
        stop();
    });

    test("ensureEventDialogOnBody", function () {
        var eventscalendar1 = createEventscalendar({"ensureEventDialogOnBody": false});
        eventscalendar1.wijevcal("option", "colors", ["mycustom"]);
        window.setTimeout(function () {
            $(eventscalendar1.find(".wijmo-wijev-timeinterval")[10]).simulate("click");
            window.setTimeout(function () {
                $(".wijmo-wijev-event-dialog .wijmo-wijev-color-button").simulate("click");
                var listColors = $(".wijmo-wijev-event-dialog .wijmo-wijev-color-menu .wijmo-wijev-listcolor");
                ok($(".wijmo-wijev-event-dialog").parent().is(eventscalendar1),
                    "The dialog has appended to the eventscalendar element.");

                eventscalendar1.wijevcal("option", "ensureEventDialogOnBody", true);
                ok($(".wijmo-wijev-event-dialog").parent().is("body"),
                    "The dialog has appended to the body.");

                eventscalendar1.wijevcal("destroy");
                eventscalendar1.remove();
                start();

            }, 400);
        }, 400);
        stop();
    });

    test("statusBarVisible", function () {
        var eventscalendar1 = createEventscalendar({ "views": [{ name: "2Days", count: 2, unit: "day" }] }), containerHeightBefore, containerHeightAfter;

        containerHeightBefore = $(".wijmo-wijev-view-container").outerHeight();
        eventscalendar1.wijevcal("option", "statusBarVisible", true);
        containerHeightAfter = $(".wijmo-wijev-view-container").outerHeight();
        ok(containerHeightAfter < containerHeightBefore, "The container's height should be less after show the statusBar");
        eventscalendar1.remove();
    });

    test("culture and cultureCalendar",function() {
        var evcal, fullDate, firstDay, year;

        evcal = createEventscalendar({
            selectedDate: new Date("2014/2/1"),
        });
        fullDate = $(".wijmo-wijev-fulldate-label", evcal);
        firstDay = $(".wijmo-wijev-daycalendar th.ui-datepicker-week-day span", evcal).first();
        year = $(".wijmo-wijev-year-label", evcal);
        ok(fullDate.html().indexOf("Saturday, February 1") > -1 && firstDay.html().indexOf("Su") > -1
            && year.html().indexOf("2014") > -1, "default culture is applied to evcal"); 
        evcal.remove();

        evcal = createEventscalendar({
            selectedDate: new Date("2014/2/1"),
            culture: "ja-jp"
        });
        fullDate = $(".wijmo-wijev-fulldate-label", evcal);
        firstDay = $(".wijmo-wijev-daycalendar th.ui-datepicker-week-day span", evcal).first();
        year = $(".wijmo-wijev-year-label", evcal);
        ok(fullDate.html().indexOf("土曜日, 2月 1") > -1 && firstDay.html().indexOf("日") > -1
            && year.html().indexOf("2014") > -1, "ja-jp culture is applied to evcal"); 
        evcal.remove();

        evcal = createEventscalendar({
            selectedDate: new Date("2014/2/1"),
            culture: "ja-jp",
            cultureCalendar: "Japanese"
        });
        fullDate = $(".wijmo-wijev-fulldate-label", evcal);
        firstDay = $(".wijmo-wijev-daycalendar th.ui-datepicker-week-day span", evcal).first();
        year = $(".wijmo-wijev-year-label", evcal);
        ok(fullDate.html().indexOf("土曜日, 2月 1") > -1 && firstDay.html().indexOf("日") > -1
            && year.html().indexOf("0026") > -1, "ja-jp culture and Japanese cultureCalendar is applied to evcal"); 
        evcal.remove();

        evcal = createEventscalendar({
            selectedDate: new Date("2014/2/1"),
        });
        fullDate = $(".wijmo-wijev-fulldate-label", evcal);
        firstDay = $(".wijmo-wijev-daycalendar th.ui-datepicker-week-day span", evcal).first();
        year = $(".wijmo-wijev-year-label", evcal);
        ok(fullDate.html().indexOf("Saturday, February 1") > -1 && firstDay.html().indexOf("Su") > -1
            && year.html().indexOf("2014") > -1, "default culture is applied to evcal"); 
        evcal.wijevcal("option", "culture", "ja-jp");
        fullDate = $(".wijmo-wijev-fulldate-label", evcal);
        firstDay = $(".wijmo-wijev-daycalendar th.ui-datepicker-week-day span", evcal).first();
        year = $(".wijmo-wijev-year-label", evcal);
        ok(fullDate.html().indexOf("土曜日, 2月 1") > -1 && firstDay.html().indexOf("日") > -1
            && year.html().indexOf("2014") > -1, "ja-jp culture is applied to evcal by set option."); 
        evcal.wijevcal("option", "cultureCalendar", "Japanese");
        fullDate = $(".wijmo-wijev-fulldate-label", evcal);
        firstDay = $(".wijmo-wijev-daycalendar th.ui-datepicker-week-day span", evcal).first();
        year = $(".wijmo-wijev-year-label", evcal);
        ok(fullDate.html().indexOf("土曜日, 2月 1") > -1 && firstDay.html().indexOf("日") > -1
            && year.html().indexOf("0026") > -1, "ja-jp culture and Japanese cultureCalendar is applied to evcal by set option");
        evcal.remove();
    });

}(jQuery));