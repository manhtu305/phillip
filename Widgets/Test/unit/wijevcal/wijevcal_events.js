﻿/*globals start, stop, test, ok, module, jQuery, window, 
simpleOptions, createEventscalendar*/
/*
* wijevcal_events.js
*/
"use strict";
(function ($) {
	var eventscalendar, calendars, k,
		eventTestHash = {
			initialized: false,
			beforeAddCalendar: false,
			beforeDeleteCalendar: false,
			calendarsChanged: false
		};

	module("wijevcal: events");

	
	test("initialized", function () {
		eventscalendar = createEventscalendar({ initialized: function () {
			ok(true, "initialized called.");	
			eventTestHash.initialized = true;
			//start();			
		}, enableLogs: true
		});

		setTimeout(function () {
		    eventscalendar.remove();
		    start();
		}, 400)
		stop();
	});

	test("beforeAddCalendar/beforeDeleteCalendar/calendarsChanged", function () {
	    eventscalendar = createEventscalendar();
		eventscalendar.bind("wijevcalbeforeaddcalendar", function (e, args) {
			calendars = args.calendars;
			if (args.data.name === "testname") {
				eventscalendar.unbind("wijevcalbeforeaddcalendar");
				eventTestHash.beforeAddCalendar = true;
				ok(true, "beforeAddCalendar called, calendar 'testname' added.");
			}

		});

		eventscalendar.bind("wijevcalcalendarschanged", function (e, args) {
			eventTestHash.calendarsChanged = true;
			ok(true, "calendarsChanged called");
			eventscalendar.unbind("wijevcalcalendarschanged");

			eventscalendar.bind("wijevcalbeforedeletecalendar", function (e, args) {
				eventTestHash.beforeDeleteCalendar = true;
				ok(true, "beforeDeleteCalendar called.");
				//start();
			});
			eventscalendar.wijevcal("deleteCalendar", "testname");

		});


		var cal = { name: "testname", location: "location",
			decsription: "decsription", color: "#000000"
		};
		eventscalendar.wijevcal("addCalendar", cal);

		setTimeout(function () {
		    eventscalendar.remove();
		    start();
		}, 400)
		stop();
	});


}(jQuery));