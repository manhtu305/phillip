﻿/*globals test, ok, module, jQuery, $*/
/*
* wijtooltip_core.js
*/
"use strict";

function create_wijtooltip(options) {
    var tooltip = $('#toolTipTest');

    tooltip.wijtooltip(options);
    tooltip.wijtooltip("widget");

    return tooltip;
}

$.fn.extend({
    wijtooltipNoAnimation: function () {
        return this.wijtooltip({
            animation: null,
            showAnimation: null,
            hideAnimation: null,
            showDelay: 0,
            hideDelay: 0
        });
    }
});

function getDocSize(name) {
    var scrollValue,
    offsetValue,
    de = "documentElement",
    body = "body";

    // handle IE 6
    if ($.browser.msie && $.browser.version < 9) {
        scrollValue = Math.max(
            document[de]["scroll" + name],
            document[body]["scroll" + name]
        );

        offsetValue = Math.max(
            document[de]["offset" + name],
            document[body]["offset" + name]
        );

        return (scrollValue < offsetValue ?
            ($(window)[name.toLowerCase()]() + 'px') :
            scrollValue + 'px');
    } else {
        return $(document)[name.toLowerCase()]() + 'px';
    }
}

function getColorHex(color) {
    var aColor, strHex, i, hex;
    if (/^(rgb|RGB)/.test(color)) {
        aColor = color.replace(/(?:\(|\)|rgb|RGB)*/g, "").split(",");
        strHex = "#";
        for (i = 0; i < aColor.length; i++) {
            hex = Number(aColor[i]).toString(16);
            if (hex === "0") {
                hex += hex;
            }
            strHex += hex;
        }
        if (strHex.length !== 7) {
            strHex = color;
        }
        return strHex;
    }
    return color;
};

(function ($) {
    module("wijtooltip: core");

    test("create and destroy", function () {
        var wijTip = create_wijtooltip({
            title: "title",
            closeBehavior: "sticky"
        }),
        tooltip = wijTip.wijtooltip("widget");

        ok(tooltip.hasClass('wijmo-wijtooltip ui-widget ui-widget-content ui-corner-all'),
             'create:element class created.');

        ok(!!tooltip.children('div.wijmo-wijtooltip-title').length &&
            tooltip.children('.wijmo-wijtooltip-title')
            .hasClass('ui-widget-header ui-corner-all'), 'create:header is created.');

        ok(!!tooltip.children('a.wijmo-wijtooltip-close').length &&
            tooltip.children('a.wijmo-wijtooltip-close')
            .hasClass('ui-state-default ui-corner-all'), 'create:close link is created.');

        ok(!!tooltip.children('div.wijmo-wijtooltip-container').length,
            'create:container is created.');

        ok(!!tooltip.children('div.wijmo-wijtooltip-pointer').length &&
            tooltip.children('div.wijmo-wijtooltip-pointer')
            .hasClass('ui-widget-content'), 'create:tooltip pointer is created.');

        wijTip.wijtooltip('destroy');

        ok(!tooltip.count || tooltip.count < 0, 'destroy:element class removed.');
        ok(!!$.isPlainObject(wijTip.data()), 'destroy:data removed.');
    });
}(jQuery));