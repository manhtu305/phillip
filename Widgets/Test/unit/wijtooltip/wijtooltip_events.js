﻿/*globals test, start, stop, ok, setTimeout, module, jQuery, create_wijtooltip*/
/*
* wijtooltip_events.js
*/

"use strict";
(function ($) {
	module("wijtooltip: events");

	test("showing shown", function () {
		var showingFlag = 0, 
			shownFlag = 0,
			$widget = create_wijtooltip({
				showAnimation: {
					animated: false,
					duration: 0 
				},
				showing: function () {
					showingFlag++;
				},
				shown: function () {
					shownFlag++;
				}
			});

		$widget.wijtooltip("show");

		setTimeout(function () {
			ok(showingFlag > 0, 'showing');
			ok(shownFlag > 0, 'shown');
			$widget.wijtooltip("destroy");
			start();
		}, 300);
		stop();
	});

	test("hidding hidden", function () {
		var hidingFlag = 0, 
			hiddenFlag = 0,
			$widget = create_wijtooltip({				
				hideAnimation: {
					animated: false,
					duration:0
				},
				hiding: function () {
					hidingFlag++;
				},
				hidden: function () {
					hiddenFlag++;
				}
			});

		$widget.wijtooltip("show");
		$widget.wijtooltip("hide");

		setTimeout(function () {
			ok(hidingFlag > 0, 'hiding');
			ok(hiddenFlag > 0, 'hidden');
			$widget.wijtooltip("destroy");
			start();
		}, 300);
		stop();
	});
}(jQuery));