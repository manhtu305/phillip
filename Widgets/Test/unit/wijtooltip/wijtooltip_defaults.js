﻿/*globals commonWidgetTests*/
/*
* wijtooltip_defaults.js
*/
"use strict";

commonWidgetTests('wijtooltip', {
	defaults: {
		wijCSS: $.extend({}, $.wijmo.wijCSS, {
			tooltip: "wijmo-wijtooltip",
			tooltipContainer: "wijmo-wijtooltip-container",
			tooltipPointer: "wijmo-wijtooltip-pointer",
			tooltipPointerInner: "wijmo-wijtooltip-pointer-inner",
			tooltipTitle: "wijmo-wijtooltip-title",
			tooltipClose: "wijmo-wijtooltip-close"
		}),
		wijMobileCSS: {
		    header: "ui-header ui-bar-a",
		    content: "ui-body-a",
		    stateDefault: "ui-btn ui-btn-a",
		    stateHover: "ui-btn-down-a",
		    stateActive: "ui-btn-down-a"
		},
		initSelector: ":jqmData(role='wijtooltip')",
		create: null,
		disabled: false,
		content: '',
		title: "",
		closeBehavior: 'auto',
		mouseTrailing: false,
		triggers: 'hover',
		position: {
			my: 'left bottom',
			at: 'right top',
			offset: null
		},
		showCallout: true,
		animation: { animated: "fade", duration: 500, easing: null },
		showAnimation: {},
		hideAnimation: {},
		showDelay: 150,
		hideDelay: 150,
		calloutAnimation: { duration: 1000, disabled: false, easing: null },
		calloutFilled: false,
		modal: false,
		group: null,
		ajaxCallback: null,
		showing: null,
		shown: null,
		hiding: null,
		hidden: null,
		cssClass: "",
		controlwidth: null,
		controlheight: null
	}
});