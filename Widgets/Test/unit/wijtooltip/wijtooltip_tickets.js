﻿/*globals test, module, jQuery, ok, start, stop, create_wijtooltip, setTimeout, document*/
/*
* wijtooltip_tickets.js
*/
"use strict";

(function ($) {
    module("wijtooltip:tickets");

    test("position:right top and collision", function () {
        window.scrollTo(0, 0);
        var width = $(document).width(),
            $widget = create_wijtooltip({
                position: {
                    my: 'left bottom',
                    at: 'right top',
                    offset: null
                }
            }),
            tooltip = $widget.wijtooltip("widget");

        $widget
            .css("position", "absolute")
            .offset({
                left: width - 200,
                top: 200
            });

        $widget.trigger("mouseover");

        setTimeout(function () {
            ok(tooltip.offset().left > $widget.offset().left &&
                tooltip.offset().top < $widget.offset().top,
                "the tooltip is on the top right of the element.");

            tooltip.hide();
            $widget.offset({ left: width - 100, top: 200 });
            $widget.trigger("mouseover");

            setTimeout(function () {
                ok(tooltip.offset().left < $widget.offset().left &&
                    tooltip.offset().top < $widget.offset().top,
                    "the tooltip is on the top left of the element.");

                tooltip.hide();
                $widget.offset({ left: width - 200, top: 10 });
                $widget.trigger("mouseover");
                setTimeout(function () {
                    ok(tooltip.offset().left > $widget.offset().left &&
                        tooltip.offset().top > $widget.offset().top,
                        "the tooltip is on the bottom right of the element.");

                    tooltip.hide();
                    $widget.offset({ left: width - 100, top: 10 });
                    $widget.trigger("mouseover");
                    setTimeout(function () {
                        ok(tooltip.offset().left < $widget.offset().left &&
                            tooltip.offset().top > $widget.offset().top,
                            "the tooltip is on the bottom left of the element.");

                        tooltip.hide();
                        $widget.wijtooltip('destroy');
                        start();
                    }, 600);
                }, 600);
            }, 600);
        }, 600);
        stop();
    });

    test("position:right center and collision", function () {
        var width = $(document).width(),
            $widget = create_wijtooltip({
                position: {
                    my: 'left center',
                    at: 'right center',
                    offset: null
                }
            }),
            tooltip = $widget.wijtooltip("widget");

        $widget.css("position", "absolute")
            .offset({ left: width - 200, top: 200 });
        $widget.trigger("mouseover");

        setTimeout(function () {
            ok(tooltip.offset().left > $widget.offset().left,
                "the tooltip is on the right center of the element.");
            tooltip.hide();
            $widget.offset({ left: width - 100, top: 200 });
            $widget.trigger("mouseover");
            setTimeout(function () {
                ok(tooltip.offset().left < $widget.offset().left,
                    "the tooltip is on the left center of the element.");
                tooltip.hide();
                $widget.wijtooltip('destroy');
                start();
            }, 400);
        }, 400);
        stop();
    });

    test("position:right bottom and collision", function () {
        var width = $(document).width(),
            height = $(document).height(),
            $widget = create_wijtooltip({
                position: {
                    my: 'left top',
                    at: 'right bottom',
                    offset: null
                }
            }),
            tooltip = $widget.wijtooltip("widget");

        $widget.css("position", "absolute")
            .offset({ left: width - 200, top: 200 });
        $widget.trigger("mouseover");

        setTimeout(function () {
            ok(tooltip.offset().left > $widget.offset().left &&
                tooltip.offset().top > $widget.offset().top,
                "the tooltip is on the right bottom of the element.");
            tooltip.hide();
            $widget.offset({ left: width - 100, top: 200 });
            $widget.trigger("mouseover");
            setTimeout(function () {
                ok(tooltip.offset().left < $widget.offset().left &&
                    tooltip.offset().top > $widget.offset().top,
                    "the tooltip is on the left bottom of the element.");
                tooltip.hide();
                $widget.offset({ left: width - 200, top: height - 20 });
                $widget.trigger("mouseover");
                setTimeout(function () {
                    ok(tooltip.offset().left > $widget.offset().left &&
                        tooltip.offset().top < $widget.offset().top,
                        "the tooltip is on the right top of the element.");
                    tooltip.hide();
                    $widget.offset({ left: width - 100, top: height - 20 });
                    $widget.trigger("mouseover");
                    setTimeout(function () {
                        ok(tooltip.offset().left < $widget.offset().left &&
                            tooltip.offset().top < $widget.offset().top,
                            "the tooltip is on the left top of the element.");
                        tooltip.hide();
                        $widget.wijtooltip('destroy');
                        start();
                    }, 400);
                }, 400);
            }, 400);
        }, 400);
        stop();
    });

    test("position:bottom right and collision", function () {
        var width = $(document).width(),
            height = $(document).height(),
            $widget = create_wijtooltip({
                position: {
                    my: 'left top',
                    at: 'center bottom',
                    offset: null
                }
            }),
            tooltip = $widget.wijtooltip("widget");

        $widget.css("position", "absolute")
            .offset({ left: width - 200, top: 200 });
        $widget.trigger("mouseover");

        setTimeout(function () {
            ok(tooltip.offset().left > $widget.offset().left &&
                tooltip.offset().top > $widget.offset().top,
                "the tooltip is on the bottom right of the element.");
            tooltip.hide();
            $widget.offset({ left: width - 200, top: height - 20 });
            $widget.trigger("mouseover");
            setTimeout(function () {
                ok(tooltip.offset().left > $widget.offset().left &&
                    tooltip.offset().top < $widget.offset().top,
                    "the tooltip is on the top right of the element.");
                tooltip.hide();
                $widget.wijtooltip('destroy');
                start();
            }, 400);
        }, 400);
        stop();
    });

    test("position:bottom center and collision", function () {
        var width = $(document).width(),
            height = $(document).height(),
            $widget = create_wijtooltip({
                position: {
                    my: 'center top',
                    at: 'center bottom',
                    offset: null
                }
            }),
            tooltip = $widget.wijtooltip("widget");

        $widget.css("position", "absolute")
            .offset({ left: width - 200, top: 200 });
        $widget.trigger("mouseover");

        setTimeout(function () {
            ok(tooltip.offset().top > $widget.offset().top,
                "the tooltip is on the bottom center of the element.");
            tooltip.hide();
            $widget.offset({ left: width - 200, top: height - 20 });
            $widget.trigger("mouseover");
            setTimeout(function () {
                ok(tooltip.offset().top < $widget.offset().top,
                    "the tooltip is on the top center of the element.");
                tooltip.hide();
                $widget.wijtooltip('destroy');
                start();
            }, 400);
        }, 400);
        stop();
    });

    test("position:bottom left and collision", function () {
        var width = $(document).width(),
            height = $(document).height(),
            $widget = create_wijtooltip({
                position: {
                    my: 'right top',
                    at: 'center bottom',
                    offset: null
                }
            }),
            tooltip = $widget.wijtooltip("widget");

        $widget.css("position", "absolute")
            .offset({ left: width - 200, top: 200 });
        $widget.trigger("mouseover");

        setTimeout(function () {
            ok(tooltip.offset().left < $widget.offset().left &&
                tooltip.offset().top > $widget.offset().top,
                "the tooltip is on the bottom left of the element.");
            tooltip.hide();
            $widget.offset({ left: width - 200, top: height - 20 });
            $widget.trigger("mouseover");
            setTimeout(function () {
                ok(tooltip.offset().left < $widget.offset().left &&
                    tooltip.offset().top < $widget.offset().top,
                    "the tooltip is on the top left of the element.");
                tooltip.hide();
                $widget.wijtooltip('destroy');
                start();
            }, 400);
        }, 400);
        stop();
    });

    test("position: left top and collision", function () {
        var width = $(document).width(),
            $widget = create_wijtooltip({
                position: {
                    my: 'right bottom',
                    at: 'left top',
                    offset: null
                }
            }),
            tooltip = $widget.wijtooltip("widget");

        $widget.css("position", "absolute")
            .offset({ left: width - 200, top: 200 });
        $widget.trigger("mouseover");

        setTimeout(function () {
            ok(tooltip.offset().left < $widget.offset().left &&
                tooltip.offset().top < $widget.offset().top,
                "the tooltip is on the left top of the element.");
            tooltip.hide();
            $widget.offset({ left: 80, top: 200 });
            $widget.trigger("mouseover");
            setTimeout(function () {
                ok(tooltip.offset().left > $widget.offset().left &&
                    tooltip.offset().top < $widget.offset().top,
                    "the tooltip is on the right top of the element.");
                tooltip.hide();
                $widget.offset({ left: width - 200, top: 10 });
                $widget.trigger("mouseover");
                setTimeout(function () {
                    ok(tooltip.offset().left < $widget.offset().left &&
                        tooltip.offset().top > $widget.offset().top,
                        "the tooltip is on the left bottom of the element.");
                    tooltip.hide();
                    $widget.offset({ left: 10, top: 10 });
                    $widget.trigger("mouseover");
                    setTimeout(function () {
                        ok(tooltip.offset().left > $widget.offset().left &&
                            tooltip.offset().top > $widget.offset().top,
                            "the tooltip is on the right bottom of the element.");
                        tooltip.hide();
                        $widget.wijtooltip('destroy');
                        start();
                    }, 400);
                }, 400);
            }, 400);
        }, 400);
        stop();
    });

    test("position: left center and collision", function () {
        var width = $(document).width(),
            $widget = create_wijtooltip({
                position: {
                    my: 'right center',
                    at: 'left center',
                    offset: null
                }
            }),
            tooltip = $widget.wijtooltip("widget");

        $widget.css("position", "absolute")
            .offset({ left: width - 200, top: 200 });
        $widget.trigger("mouseover");

        setTimeout(function () {
            ok(tooltip.offset().left < $widget.offset().left,
                "the tooltip is on the left center of the element.");
            tooltip.hide();
            $widget.offset({ left: 10, top: 200 });
            $widget.trigger("mouseover");
            setTimeout(function () {
                ok(tooltip.offset().left > $widget.offset().left,
                    "the tooltip is on the right center of the element.");
                tooltip.hide();
                $widget.wijtooltip('destroy');
                start();
            }, 400);
        }, 400);
        stop();
    });

    test("position: left bottom and collision", function () {
        var width = $(document).width(),
            height = $(document).height(),
            $widget = create_wijtooltip({
                position: {
                    my: 'right top',
                    at: 'left bottom',
                    offset: null
                }
            }),
            tooltip = $widget.wijtooltip("widget");

        $widget.css("position", "absolute")
            .offset({ left: width - 200, top: 200 });
        $widget.trigger("mouseover");

        setTimeout(function () {
            ok(tooltip.offset().left < $widget.offset().left &&
                tooltip.offset().top > $widget.offset().top,
                "the tooltip is on the left bottom of the element.");
            tooltip.hide();
            $widget.offset({ left: 80, top: 200 });
            $widget.trigger("mouseover");
            setTimeout(function () {
                ok(tooltip.offset().left > $widget.offset().left &&
                    tooltip.offset().top > $widget.offset().top,
                    "the tooltip is on the right bottom of the element.");
                tooltip.hide();
                $widget.offset({ left: width - 200, top: height - 20 });
                $widget.trigger("mouseover");
                setTimeout(function () {
                    ok(tooltip.offset().left < $widget.offset().left &&
                        tooltip.offset().top < $widget.offset().top,
                        "the tooltip is on the left top of the element.");
                    tooltip.hide();
                    $widget.offset({ left: 10, top: height - 20 });
                    $widget.trigger("mouseover");
                    setTimeout(function () {
                        ok(tooltip.offset().left > $widget.offset().left &&
                            tooltip.offset().top < $widget.offset().top,
                            "the tooltip is on the right top of the element.");
                        tooltip.hide();
                        $widget.wijtooltip('destroy');
                        start();
                    }, 400);
                }, 400);
            }, 400);
        }, 400);
        stop();
    });

    test("position: top right and collision", function () {
        var width = $(document).width(),
            $widget = create_wijtooltip({
                position: {
                    my: 'left bottom',
                    at: 'center top',
                    offset: null
                }
            }),
            tooltip = $widget.wijtooltip("widget");

        $widget.css("position", "absolute")
            .offset({ left: width - 200, top: 200 });
        $widget.trigger("mouseover");

        setTimeout(function () {
            ok(tooltip.offset().left > $widget.offset().left &&
                tooltip.offset().top < $widget.offset().top,
                "the tooltip is on the top right of the element.");
            tooltip.hide();
            $widget.offset({ left: width - 200, top: 20 });
            $widget.trigger("mouseover");
            setTimeout(function () {
                ok(tooltip.offset().left > $widget.offset().left &&
                    tooltip.offset().top > $widget.offset().top,
                    "the tooltip is on the bottom right of the element.");
                tooltip.hide();
                $widget.wijtooltip('destroy');
                start();
            }, 400);
        }, 400);
        stop();
    });

    test("position: top center and collision", function () {
        var width = $(document).width(),
            $widget = create_wijtooltip({
                position: {
                    my: 'center bottom',
                    at: 'center top',
                    offset: null
                }
            }),
            tooltip = $widget.wijtooltip("widget");

        $widget.css("position", "absolute")
            .offset({ left: width - 200, top: 200 });
        $widget.trigger("mouseover");

        setTimeout(function () {
            ok(tooltip.offset().top < $widget.offset().top,
                "the tooltip is on the top center of the element.");
            tooltip.hide();
            $widget.offset({ left: width - 200, top: 20 });
            $widget.trigger("mouseover");
            setTimeout(function () {
                ok(tooltip.offset().top > $widget.offset().top,
                    "the tooltip is on the bottom center of the element.");
                tooltip.hide();
                $widget.wijtooltip('destroy');
                start();
            }, 400);
        }, 400);
        stop();
    });

    test("position: top left and collision", function () {
        var width = $(document).width(),
            $widget = create_wijtooltip({
                position: {
                    my: 'right bottom',
                    at: 'center top',
                    offset: null
                }
            }),
            tooltip = $widget.wijtooltip("widget");

        $widget.css("position", "absolute")
            .offset({ left: width - 200, top: 200 });
        $widget.trigger("mouseover");

        setTimeout(function () {
            ok(tooltip.offset().left < $widget.offset().left &&
                tooltip.offset().top < $widget.offset().top,
                "the tooltip is on the top left of the element.");
            tooltip.hide();
            $widget.offset({ left: width - 200, top: 20 });
            $widget.trigger("mouseover");
            setTimeout(function () {
                ok(tooltip.offset().left < $widget.offset().left &&
                    tooltip.offset().top > $widget.offset().top,
                    "the tooltip is on the bottom right of the element.");
                tooltip.hide();
                $widget.css("position", "").css("left", "").css("top", "");
                $widget.wijtooltip('destroy');
                start();
            }, 400);
        }, 400);
        stop();
    });

    //#21386
    test("calloutShape.indexOf('l') error: when showCallout=false, calloutShape is undefind.", function () {
        var $ToolTip = $('<input type="submit" name="Button1" value="Button" id="Button1" />').appendTo("body")
            .wijtooltip({ "content": "ToolTipContent", "title": "ToolTipTitle", "showCallout": false
            });

        setTimeout(function () {
            $("#Button1").simulate("mouseover");
            setTimeout(function () {
                $("#Button1").simulate("mouseout");
            }, 400);
        }, 400);
        $("#Button1").simulate("mouseover");
        $("#Button1").simulate("mouseout");
        ok(true, "when showCallout=false, step out calloutShape.indexOf('l'). Success!");
        $ToolTip.wijtooltip("destroy");
        $ToolTip.remove();
     });

    //#21416
    test("option.cssClass error: use user-defined cssClass to set the tooltip", function () {
        var $widget = create_wijtooltip(),
            tooltip = $widget.wijtooltip("widget");

        $widget.wijtooltip('show')
            .wijtooltip("option", "cssClass", "testClass"); //testClass

        if (tooltip.hasClass("testClass")) {
            ok(true, "Set cssClass='testClass', success!");
        } else {
            ok(false, "Set cssClass='testClass', fail!");
        }

        setTimeout(function () {
            $widget.wijtooltip("destroy");
            start();
        }, 500);
        stop();
     });

    //#21467
    test("tooltip content error: The tooltip 'red' appeared for the text 'tooltip'.", function () {
        var $widget = create_wijtooltip(),
            tooltip = $widget.wijtooltip("widget");
        var $toolTip2 = $('<a href="#" id="toolTipTest2" title="tooltip2">Test2</a>').appendTo("body >form");
        $toolTip2.wijtooltip();

        $widget.wijtooltip('show').wijtooltip("option", "content", "tooltip");
        $toolTip2.wijtooltip("option", "position", { my: "left bottom", at: "right top" });

        var content = $.trim($("div.wijmo-wijtooltip-container").html());
        ok(content === "tooltip2", "content update success!");

        setTimeout(function () {
            $widget.wijtooltip("option", "position", { offset: "10px 10px", my: "left bottom", at: "right top" });
            $toolTip2.wijtooltip("option", "position", { offset: "none none", my: "left bottom", at: "right top" });
            setTimeout(function () {
                if ($("div.wijmo-wijtooltip-pointer").css("left") !== "10px") {
                    ok(true, "reset the left success!");
                }
                setTimeout(function () {
                    if ($("div.wijmo-wijtooltip-pointer").css("top") !== "10px") {
                        ok(true, "reset the top success!");
                        $widget.wijtooltip("destroy");
                        $toolTip2.remove();
                    }
                    start();
                }, 500);
            }, 500);
        }, 300);
        stop();
     });

    //#26904
    test("mousetrailing  error: set position.my to 'center center' when mousetrailing is setting 'true'", function () {
        var $widget = create_wijtooltip(),
           tooltip, oldOffset, newOffset, event = $.Event("mousemove");

        $widget.wijtooltip({ "position": { my: "center center" }, "mouseTrailing": true });
        tooltip = $widget.wijtooltip("widget");
        oldOffset = tooltip.offset();
        event.pageX = oldOffset.left + 100;
        event.pageY = oldOffset.top + 100;

        $widget.trigger(event);
        setTimeout(function () {
            newOffset = tooltip.offset();
            ok(!(oldOffset.left === newOffset.left && oldOffset.top === newOffset.top), "set position.my to 'center center' when mousetrailing is seting 'true' success!");
            $widget.wijtooltip("destroy");
            start();
        }, 500);
        stop();
     });

    test("#41097", function () {
        var testContainer = $("<div></div>").appendTo("form"),
            ele1 = $('<a id="good" class="hovertest">Should hover</a>').appendTo(testContainer),
            ele2 = $('<a id="bad" class="hovertest">Shouldn not hover</a>'), tooltip;

        testContainer.append("<div>").append(ele2);

        testContainer.find("a").wijtooltip({
            content: "Why am I still here?",
            title: "Hello",
            showing: function (ev, ui) {
                if ($(ev.target).attr("id") === "good") {
                    return true;
                }
                else {
                    return false;
                }
            }
        });

        ele1.simulate("mouseover");
        setTimeout(function () {
            ele2.simulate("mouseover");
            setTimeout(function () {
                tooltip = ele1.data("wijmo-wijtooltip")._tooltipCache._$tooltip;
                ok(tooltip.is(":hidden"), "the tooltip is hide when mouse over to the not hover element.");
                testContainer.remove();
                start();
            }, 400);
        }, 400);
        stop();
    });

    test("#30420:support width and height setting for wijtooltip.", function () {
        var $widget = create_wijtooltip(),
            tooltip;

        $widget.wijtooltip({ controlwidth: "150px", controlheight: "100px" });
        tooltip = $widget.wijtooltip("widget");

        ok(tooltip.css("width") === "150px");
        ok(tooltip.css("height") === "100px");
        $widget.wijtooltip('destroy');
    });

    test("#39682", function () {
        var $widget = create_wijtooltip({
            animation: null,
            showAnimation: null,
            hideAnimation: null,
            showDelay: 0,
            hideDelay: 0
        }),
        tooltip = $widget.wijtooltip("widget");

        $widget.wijtooltip('option', 'triggers', 'click');
        $widget.simulate('click');
        ok(tooltip.is(':visible'), "Tooltip has open");
        $widget.simulate('mouseout');
        ok(!tooltip.is(':visible'), 'triggers:click; Mouseout hide the tooltip');

        $widget.wijtooltip('option', 'triggers', 'rightClick');
        $widget.trigger('contextmenu');
        $widget.simulate('mouseup');
        ok(tooltip.is(':visible'), "Tooltip has open");
        $widget.simulate('mouseout');
        ok(!tooltip.is(':visible'), 'triggers:rightClick; Mouseout hide the tooltip');

        $widget.wijtooltip('option', 'triggers', 'focus');
        // For IE, focus opeartion uses a method to delay which seems like multi-threading through "setTimeout". 
        // Therefore, use event binding to implement focus test. 
        $widget.one("focus", function () {
            ok(tooltip.is(':visible'), "Tooltip has open");
            $widget.trigger('mouseout');
            ok(!tooltip.is(':visible'), 'triggers:focus; Mouseout hide the tooltip');
            if ($.browser.msie) {
                $widget.wijtooltip('destroy');
                setTimeout(function () {
                    start();
                }, 200);
            } else {
                $widget.wijtooltip('destroy');
            }
        });
        $widget.focus();
        if ($.browser.msie) {
            stop();
        }
    });

    test("#48097:[Forum-Community][WijToolTip] 'aria-Describedby' attribute for the underlying DIV for WijTooltip shows 'undefined'.", function () {
        var ele1 = $('<a href="#" id="toolTipTest1" title="toolTipTest">toolTipTest with ID</a>').appendTo("body >form"),
            tooltip1;

        ele1.wijtooltip();
        tooltip1 = ele1.data("wijmo-wijtooltip")._tooltipCache._$tooltip;
        ok(tooltip1.attr("aria-describedby") === "toolTipTest1");
        tooltip1 = null;
        ele1.wijtooltip("destroy");
        ele1.remove();

        ele1 = $('<a href="#" title="toolTipTest">toolTipTest with ID</a>').appendTo("body >form");
        ele1.wijtooltip();
        tooltip1 = ele1.data("wijmo-wijtooltip")._tooltipCache._$tooltip;
        ok(tooltip1.attr("aria-describedby") == null);
        ele1.wijtooltip("destroy");
        ele1.remove();
    });

    test("Test for \"showAt\" issue: When the callout direction changed according to the position, it can't recover after move back.", function () {
            var $widget = $('<a href="#" id="toolTipShowAt" title="toolTipTest">toolTipTest</a>').appendTo("body >form"),
            tooltip, callout,
            bodyWidth = $(document.body).width(), tooltipWidth, newX;

            $widget.wijtooltip({
                title: "WIJTOOLTIP TITLE",
                content: "A long string to make the tooltip large enough !",
            });
            tooltip = $widget.wijtooltip("widget");

            $widget.wijtooltip("show");
            tooltipWidth = tooltip.width();
            newX = bodyWidth - tooltipWidth / 2;

            $widget.wijtooltip("showAt", {
                x: newX,
                y: 200
            });

            setTimeout(function () {
                $widget.wijtooltip("showAt", {
                    x: 10,
                    y: 200
                });
                setTimeout(function () {
                    callout = tooltip.find('.wijmo-wijtooltip-pointer');

                    ok(tooltip.hasClass("wijmo-wijtooltip-arrow-lb"), "wijtooltip has recovered the callout into left-bottom !");
                    ok($widget.is(':visible') &&
                        Math.abs(callout.offset().left - 10) < 10 &&
                        Math.abs(callout.offset().top + callout.outerHeight() - 200) < 10, 'show');
                    $widget.wijtooltip('destroy');
                    $widget.remove();
                    start();
                }, 1000);
            }, 550);
            stop();
        });

    test("When there are two elements with tooltip and destroy one, another tooltip should exist.", function () {
        var tooltip1 = $('<a href="#" id="tooltip1" class="testTooltip" title="toolTipTest">tooltip1</a>').appendTo("body >form"),
            tooltip2 = $('<a href="#" id="tooltip2" class="testTooltip" title="toolTipTest">tooltip2</a>').appendTo("body >form");

        $("div.wijmo-wijtooltip").remove();
        $("a.testTooltip").wijtooltip({
            closeBehavior: "sticky",
            showAnimation: false,
            hideAnimation: false
        });

        tooltip1.wijtooltip("show");
        tooltip2.wijtooltip("show");
        tooltip1.wijtooltip("destroy");
        ok($("div.wijmo-wijtooltip").length === 1, "tooltip exists.");
        ok($("div.wijmo-wijtooltip-container").length === 1, "tooltip container exists.");
        ok($("div.wijmo-wijtooltip-pointer").length === 1, "tooltip pointer exists.");
        ok($("div.wijmo-wijtooltip-pointer-inner").length === 1, "tooltip pointer inner exists.");
        ok($("div.wijmo-wijtooltip-title").length === 1, "tooltip title exists.");
        ok($("a.wijmo-wijtooltip-close").length === 1, "tooltip close button exists.");
        tooltip1.remove();
        tooltip2.wijtooltip("destroy");
        ok($("div.wijmo-wijtooltip").length === 0, "tooltip is removed.");
        ok($("div.wijmo-wijtooltip-container").length === 0, "tooltip container is removed.");
        ok($("div.wijmo-wijtooltip-pointer").length === 0, "tooltip pointer is removed.");
        ok($("div.wijmo-wijtooltip-pointer-inner").length === 0, "tooltip pointer inner is removed.");
        ok($("div.wijmo-wijtooltip-title").length === 0, "tooltip title is removed.");
        ok($("a.wijmo-wijtooltip-close").length === 0, "tooltip close button is removed.");
        tooltip2.remove();
    });

    test("#84121", function () {
        var $widget = create_wijtooltip({
            closeBehavior: "sticky",
            modal: false
        }).wijtooltipNoAnimation();
        $widget.wijtooltip("show");
        ok($("div.wijmo-wijtooltip").css("display") !== "none", "tooltip is shown.");
        $widget.wijtooltip("option", "disabled", true);
        ok(!$widget.hasClass("ui-state-disabled"), "host element is not disabled.");
        ok($("div.wijmo-wijtooltip").css("display") === "none", "tooltip is gone.");
        $widget.remove();
    });

    test("#87010", function () {
        var $widget = $("<div>").html("#87010").css({
            position: "absolute",
            top: 0,
            left: 0
        }).appendTo(document.body).wijtooltip({
            closeBehavior: "sticky",
            modal: false,
            disabled: false,
            content: "This is content",
            title: "Title"
        }).wijtooltipNoAnimation(), left, top;

        $widget.wijtooltip("show");
        ok($("div.wijmo-wijtooltip").hasClass("wijmo-wijtooltip-arrow-lt"), "tooltip flipped.");
        $widget.wijtooltip("option", "showCallout", true);
        ok($("div.wijmo-wijtooltip").hasClass("wijmo-wijtooltip-arrow-lt"), "callout not flipped.");
        $widget.wijtooltip("option", "showCallout", false);
        left = $("div.wijmo-wijtooltip").css("left");
        top = $("div.wijmo-wijtooltip").css("top");
        $widget.wijtooltip("show");
        ok($("div.wijmo-wijtooltip").css("left") === left && $("div.wijmo-wijtooltip").css("top") === top, "tooltip position not changed.");
        $widget.remove();
    });

    test("#90167", function () {
        var $host = $('<label title="Tooltip Title">Hover here to see the tooltip.</label>')
            .appendTo(document.body)
            .wijtooltip({
                closeBehavior: "auto",
                triggers: "click",
                hidden: function () {
                    hiddenCount++;
                },
                hiding: function () {
                    hidingCount++;
                }
            }).wijtooltipNoAnimation(),
            hiddenCount = 0,
            hidingCount = 0;

        $host.simulate("mouseover");
        $host.simulate("mouseout");
        ok(hiddenCount === 0 && hidingCount === 0, "Hidden and hiding event not fired.");
        $host.remove();
    });

    test("#91142", function () {
        var $host = $('<label title="Tooltip Title">Hover here to see the tooltip.</label>')
            .appendTo(document.body)
            .wijtooltip({
                closeBehavior: "sticky",
                mouseTrailing: true
            }).wijtooltipNoAnimation(),
            $tooltip = $host.data("wijmo-wijtooltip")._tooltipCache._$tooltip;

        $host.wijtooltip("show");
        ok($tooltip.is(":visible"), "tooltip is shown.");
        $host.remove();
    });
}(jQuery));