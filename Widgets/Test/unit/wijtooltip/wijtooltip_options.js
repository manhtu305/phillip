﻿/*globals test, module, jQuery, ok, start, stop, create_wijtooltip, setTimeout*/
/*
* wijtooltip_options.js
*/
"use strict";

(function ($) {
	module("wijtooltip:options");

	test("content,title", function () {
		var $widget = create_wijtooltip(),
			tooltip = $widget.wijtooltip("widget");

		$widget.wijtooltip('option', 'content', 'widget content.')
			.wijtooltip('option', 'title', 'widget title.')
			.wijtooltip('show');
		
		setTimeout(function () {
			ok(tooltip.find('div.wijmo-wijtooltip-title:eq(0)')
				.html() === 'widget title.', 'content');
			ok(tooltip.find('div.wijmo-wijtooltip-container:eq(0)')
				.html() === 'widget content.', 'title');
			$widget.wijtooltip('destroy');
			start();
		}, 200);
		stop();
	});	

	test("closeBehavior", function () {//not completed
		var $widget = create_wijtooltip(),
			tooltip = $widget.wijtooltip("widget");

		$widget.wijtooltip('show')
			.wijtooltip("option", "closeBehavior", "auto"); //sticky

		setTimeout(function () {
			ok(!tooltip.find('a.wijmo-wijtooltip-close:eq(0)')
				.is(':visible'), 'closeBehavior:auto');
			$widget.wijtooltip("option", "closeBehavior", "sticky"); //sticky
			setTimeout(function () {
				ok(!!tooltip.find('a.wijmo-wijtooltip-close:eq(0)')
					.is(':visible'), 'closeBehavior:sticky');
				$widget.wijtooltip('destroy');
				start();
			}, 160);
		}, 200);
		stop();
	});	

	test("calloutFilled", function () {
		var $widget = create_wijtooltip({
            showDelay: 0,
            calloutFilled: true,
            closeBehavior: 'none',
			showCallOut: true,
			showAnimation: false,
			hideAnimation: false,
            shown: function () {
                var self = this,
                    calloutFilled = $(self).wijtooltip("option").calloutFilled,
                    tooltip = $(self).wijtooltip("widget");

                ok(getColorHex($(".wijmo-wijtooltip-pointer-inner")
                        .css("border-right-color")) !== getColorHex(tooltip.css("background-color")), 'calloutFilled:true');
                $(self).wijtooltip('option', 'calloutFilled', false)
                        .wijtooltip('option', 'shown', function () {
                            ok(getColorHex($(".wijmo-wijtooltip-pointer-inner")
                                .css("border-right-color")) === getColorHex(tooltip.css("background-color")), 'calloutFilled:false');
                            $widget.wijtooltip('destroy');
                            start();
                        }).wijtooltip("show");
            }
		});
        
        $widget.wijtooltip("show"); 
        stop();
	});
	
	test("triggers", function () {
		var $widget = create_wijtooltip({
				showAnimation: false,
				hideAnimation: false
		}),	tooltip = $widget.wijtooltip("widget");

		$widget.wijtooltip('option', 'autoTooltipify', true);
		
		setTimeout(function () {
			$widget.wijtooltip('option', 'triggers', 'hover');

			$widget.simulate('mouseover');
			setTimeout(function () {
				ok(tooltip.is(':visible'), 'triggers:hover');
				tooltip.hide();
				$widget.wijtooltip('option', 'triggers', 'click');
				$widget.trigger('click');
				setTimeout(function () {
					ok(tooltip.is(':visible'), 'triggers:click');
					tooltip.hide();
					$widget.wijtooltip('option', 'triggers', 'focus');
					$widget.simulate("focus");
					setTimeout(function () {
						ok(tooltip.is(':visible'), 'triggers:focus');
						tooltip.hide();
						$widget.wijtooltip('option', 'triggers', 'rightClick');
						$widget.trigger('contextmenu');
						$widget.simulate('mouseup');
						setTimeout(function () {
						    ok(tooltip.is(':visible'), 'triggers:rightClick');
						    $widget.wijtooltip('destroy');
							start();
						}, 200);
					}, 200);
				}, 200);
			}, 200);
		}, 200);
		stop();
	});

	test("position", function () {
		//$("#toolTipTest").css("margin-left", "300px");
		var $widget = create_wijtooltip({
				position: { my: 'left bottom',
					at: 'right top',
					offset: null
					},
				showAnimation: { animated: false },
				hideAnimation: { animated: false },
				showDelay: 0
			}),
			tooltip = $widget.wijtooltip("widget");
		$widget.offset({ left: 200, top: 200 });

		$widget.simulate('mouseover');
		setTimeout(function () {
			ok(tooltip.hasClass("wijmo-wijtooltip-arrow-lb"), 
				"the callout is left button.");			

			$widget.wijtooltip("option", "position", { 
				my: 'center bottom',
				at: 'right top'
			});
			ok(tooltip.hasClass("wijmo-wijtooltip-arrow-bc"), 
				"the callout is button center.");

			$widget.wijtooltip("option", "position", { 
				my: 'right bottom',
				at: 'right top'
			});
			ok(tooltip.hasClass("wijmo-wijtooltip-arrow-br"), "the callout is button right.");

			$widget.wijtooltip("option", "position", { 
				my: 'left bottom',
				at: 'left top'
			});
			ok(tooltip.hasClass("wijmo-wijtooltip-arrow-bl"), "the callout is button left.");

			$widget.wijtooltip("option", "position", { 
				my: 'left center',
				at: 'right top'
			});
			ok(tooltip.hasClass("wijmo-wijtooltip-arrow-lc"), "the callout is left center.");

			$widget.wijtooltip("option", "position", { 
				my: 'left top',
				at: 'right top'
			});
			ok(tooltip.hasClass("wijmo-wijtooltip-arrow-lt"), "the callout is left top.");

			$widget.wijtooltip("option", "position", { 
				my: 'center top',
				at: 'right bottom'
			});
			ok(tooltip.hasClass("wijmo-wijtooltip-arrow-tc"), "the callout is top center.");

			$widget.wijtooltip("option", "position", { 
				my: 'left top',
				at: 'left bottom'
			});
			ok(tooltip.hasClass("wijmo-wijtooltip-arrow-tl"), "the callout is top left.");

			$widget.wijtooltip("option", "position", { 
				my: 'right top',
				at: 'right bottom'
			});
			ok(tooltip.hasClass("wijmo-wijtooltip-arrow-tr"), "the callout is top right.");

			$widget.wijtooltip("option", "position", { 
				my: 'right top',
				at: 'left bottom'
			});
			ok(tooltip.hasClass("wijmo-wijtooltip-arrow-rt"), "the callout is right top.");

			$widget.wijtooltip("option", "position", {
				my: 'right center',
				at: 'left bottom'
			});
			ok(tooltip.hasClass("wijmo-wijtooltip-arrow-rc"), "the callout is right center.");

			$widget.wijtooltip("option", "position", { 
				my: 'right bottom',
				at: 'left top'
			});
			ok(tooltip.hasClass("wijmo-wijtooltip-arrow-rb"), "the callout is right bottom.");

			$widget.simulate('mouseout');
			$widget.css("margin-left", "");
			$widget.wijtooltip('destroy');
			start();
		}, 100);
		stop();
	});

    test("modal", function () {
		var $widget = create_wijtooltip({
                modal: true,
				showAnimation: false,
				hideAnimation: false,
                shown: function() {
                    var isWidth = $(".ui-widget-overlay").width() + "px" === getDocSize("Width"),
                        isHeight = $(".ui-widget-overlay").height() + "px" === getDocSize("Height");
                    ok(isWidth && isHeight, "the modal is shown.");
                    $widget.wijtooltip('hide');
                    $widget.wijtooltip('destroy');
                    start();
                }
			}),
			tooltip = $widget.wijtooltip("widget");
            $widget.wijtooltip("show");
            stop();
    });
}(jQuery));