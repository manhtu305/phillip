﻿/*globals module, test, start, stop, setTimeout, ok, jQuery, create_wijtooltip*/
/*
* wijtooltip_methods.js
*/
"use strict";

(function ($) {
	module("wijtooltip: methods");

	test("show", function () {
		var $widget = create_wijtooltip({
				showAnimation: {
					animated: 'fade',
					duration: 0
				},
				hideAnimation: false
			}),
			tooltip = $widget.wijtooltip("widget");

		$widget.wijtooltip("show");

		setTimeout(function () {
		    ok(tooltip.is(':visible'), 'show');
		    $widget.wijtooltip('destroy');
			start();
		}, 200);
		stop();
	});

	test("showAt", function () {
		var $widget = create_wijtooltip({
				showAnimation: {
					animated: 'fade',
					duration: 0
				},
				hideAnimation: false
			}),
			tooltip = $widget.wijtooltip("widget");

		$widget.wijtooltip("showAt", {
			x: 100,
			y: 200
		});

		setTimeout(function () {
		    var callout = tooltip.find('.wijmo-wijtooltip-pointer');

		    ok(tooltip.hasClass("wijmo-wijtooltip-arrow-lb"), "wijtooltip sets correct style for callout element !");
			ok($widget.is(':visible') &&
				Math.abs(callout.offset().left - 100) < 10 && 
				Math.abs(callout.offset().top +
				callout.outerHeight() - 200) < 10, 'show');

			$widget.wijtooltip('destroy');
			start();
		}, 550);
		stop();
	});

	test("hide", function () {
		var $widget = create_wijtooltip({
			showAnimation: false,
			hideAnimation: { animated: 'fade', duration: 0 }
		}),
		tooltip = $widget.wijtooltip("widget");

		$widget.wijtooltip("show");
		$widget.wijtooltip("hide");

		setTimeout(function () {
		    ok(!tooltip.is(':visible'), 'hide');
		    $widget.wijtooltip('destroy');
			start();
		}, 100);
		stop();
	});
} (jQuery));