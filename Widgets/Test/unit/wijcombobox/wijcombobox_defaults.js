/*
 * wijcombobox_defaults.js
 */
var wijcombobox_defaults = {
    dataSource: null,
    disabled: false,
    data: null,
    labelText: null,
    minLength: 4,
    delay: 300,
    showingAnimation: null,
    hidingAnimation: null,
    showTrigger: true,
    triggerPosition: 'right',
    dropdownHeight: 300,
    dropdownWidth: 'auto',
    selectOnItemFocus: false,
    autoFilter: true,
    autoComplete: true,
    highlightMatching: true,
    dropDownListPosition: {},
    columns: [],
    selectionMode: 'single',
    multipleSelectionSeparator: ',',
    forceSelectionText: false,
    select: null,
    isEditable: true,
    selectedIndex: -1,
    inputTextInDropDownList: false,
    open: null,
    close: null,
    selectElementWidthFix: 6,
    textChanged: null,
    selectedIndexChanged: null,
    selectedIndexChanging: null,
    search: null,
    changed: null,
    selectedValue: null,
    listOptions: null,
    text: null,
    ensureDropDownOnBody: true,
    wijCSS: $.extend({
        comboboxCss: "wijmo-wijcombobox",
        comboboxWrapperCss: "wijmo-wijcombobox-wrapper",
        comoboboxInputCss: "wijmo-wijcombobox-input",
        comboboxListCss: "wijmo-wijcombobox-list",
        comboboxTriggerCss: "wijmo-wijcombobox-trigger"
    }, $.wijmo.wijCSS),
    initSelector: ":jqmData(role='wijcombobox')",
    create: null,
    adjustComboWidth: false
};

commonWidgetTests('wijcombobox', {
    defaults: wijcombobox_defaults
});
