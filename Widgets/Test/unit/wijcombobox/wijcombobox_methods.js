/*
 * wijcombobox_methods.js
 */

(function ($) {

    module("wijcombobox: methods");

    test("search and close", function () {
        // test disconected element
        var ComboBox = $('<input />').wijcombobox({ data: getArrayDataSource() });
        var count = 0;
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');
        ComboBox.val('java');
        ComboBox.wijcombobox('search', 'java');
        //ok($('.wijmo-wijcombobox-list li').size() ==2, 'two items found');
        $.each($('.wijmo-wijcombobox-list li'), function (index, ele) {
            if ($(ele).is(":visible")) {
                count++;
            }
        });
        ok(count == 2, 'two items found');
        ComboBox.wijcombobox('close');
        ok($('.wijmo-wijcombobox-list').is(':visible') == false, 'list closed');
        ComboBox.remove();
    });

    test("datasetting", function () {
        var ComboBox = $('<input />').wijcombobox({ selectedIndex: 5, data: getArrayDataSource() });
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');
        ok(ComboBox.data('wijmo-wijcombobox').selectedItem.label === 'asp' && ComboBox.wijcombobox('option', 'selectedIndex') === 5, 'selected item set');
        ok(ComboBox.val() === 'asp', 'selected item set');

        ComboBox.wijcombobox('option', "data", [{
            label: 'delphi',
            value: 'delphi'
        }, {
            label: 'visual studio',
            value: 'visual studio'
        }, {
            label: 'flash',
            value: 'flash'
        }]);
        ok(ComboBox.val() === '' && ComboBox.wijcombobox('option', 'selectedIndex') === -1, 'selected item clear');
        ComboBox.remove();
    });
})(jQuery);
