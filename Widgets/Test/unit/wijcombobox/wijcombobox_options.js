/*
 * wijcombobox_options.js
 */
(function ($) {

    module("wijcombobox:options");

    test("data", function () {
        // test disconected element
        var ComboBox = $('<input />').wijcombobox();
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');
        var testArray = [{
            label: 'c++',
            value: 'c++'
        }];
        var myReader = new wijarrayreader([
            {
                name: 'label'
            },
            {
                name: 'value'
            }
        ]);
        var datasourceOptions = {
            reader: myReader,
            data: testArray
        };
        var datasource = new wijdatasource(datasourceOptions);
        ComboBox.wijcombobox({ data: datasource });
        var t = $('.wijmo-wijcombobox-trigger');
        t.simulate('mouseover');
        t.simulate('click');
        t.simulate('mouseout');
        ComboBox.wijcombobox('close');
        ok($('.wijmo-wijcombobox-list li').size() == 1, 'data changed');
        ComboBox.remove();
    });

    test("showTrigger", function () {
        // test disconected element
        var ComboBox = $('<input />').wijcombobox({ showTrigger: true });
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');
        var t = $('.wijmo-wijcombobox-trigger');
        ok(t.size() == 1, 'has trigger');
        ComboBox.wijcombobox({ showTrigger: false });
        ComboBox.wijcombobox('repaint');
        var tri = $('.wijmo-wijcombobox-trigger');
        ok(tri.size() == 0, 'no trigger ');
        ComboBox.remove();
    });

    test("triggerPosition", function () {
        // test disconected element
        var ComboBox = $('<input />').wijcombobox({ showTrigger: true, triggerPosition: 'left' });
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');
        var trigger = $('.wijmo-wijcombobox-trigger');
        ok(
            trigger.css('left') == '0px' &&
            trigger[0].style.right == '' &&
            trigger.hasClass('ui-corner-left'),
            'trigger on left'
        );
        ComboBox.wijcombobox({ triggerPosition: 'right' });
        ComboBox.wijcombobox('repaint');
        trigger = $('.wijmo-wijcombobox-trigger');
        ok(
            trigger.css('right') == '0px' &&
            trigger[0].style.left == '' &&
            trigger.hasClass('ui-corner-right'),
            'trigger on right'
        );
        ComboBox.remove();
    });

    test("dropdownHeight", function () {
        // test disconected element
        var ComboBox = $('<input />').wijcombobox({ data: getArrayDataSource() });
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');
        var trigger = $('.wijmo-wijcombobox-trigger');
        trigger.simulate('mouseover').simulate('click').simulate('mouseout');
        ok($('.wijmo-wijcombobox-list').outerHeight() == 300, 'height of list is correct.');

        ComboBox.wijcombobox({ dropdownHeight: 357 });
        trigger.simulate('mouseover').simulate('click').simulate('mouseout');
        //trigger.simulate("click");
        //console.log($('.wijmo-wijcombobox-list').outerHeight());
        trigger.simulate('mouseover').simulate('click').simulate('mouseout');
        ok($('.wijmo-wijcombobox-list').outerHeight() == 357, 'height of list is correct.');
        ComboBox.remove();
    });

    test("dropdownWidth", function () {
        // test disconected element
        var ComboBox = $('<input />').appendTo(document.body).wijcombobox({ data: getArrayDataSource() });
        ComboBox.wijcombobox('repaint');
        var trigger = $('.wijmo-wijcombobox-trigger');
        trigger.simulate('mouseover').simulate('click').simulate('mouseout');
        ok($('.wijmo-wijcombobox-list').outerWidth() == ComboBox.wijcombobox('getComboElement').outerWidth(), 'width of list is correct.');
        ComboBox.wijcombobox({ dropdownWidth: 400 });
        trigger.simulate('mouseover').simulate('click').simulate('mouseout');
        trigger.simulate('mouseover').simulate('click').simulate('mouseout');
        ok($('.wijmo-wijcombobox-list').outerWidth() == 400, 'width of list is correct.');
        ComboBox.remove();
    });

    test("selectOnItemFocus", function () {
        // test disconected element
        var ComboBox = $('<input />').wijcombobox({ data: getArrayDataSource(), selectOnItemFocus: true });
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');
        var input = ComboBox;
        // opens list
        input.simulate('keydown', { keyCode: $.ui.keyCode.DOWN });
        input.simulate('keyup', { keyCode: $.ui.keyCode.DOWN });
        // first item selected
        input.simulate('keydown', { keyCode: $.ui.keyCode.DOWN });
        input.simulate('keyup', { keyCode: $.ui.keyCode.DOWN });
        ok(input.val() == 'c++', 'text equals to c++');
        // navigate to second item
        input.simulate('keydown', { keyCode: $.ui.keyCode.DOWN });
        input.simulate('keyup', { keyCode: $.ui.keyCode.DOWN });
        ok(input.val() == 'java', 'text equals to java');
        // back to first item
        input.simulate('keydown', { keyCode: $.ui.keyCode.UP });
        input.simulate('keyup', { keyCode: $.ui.keyCode.UP });
        ok(input.val() == 'c++', 'text equals to c++');
        ComboBox.wijcombobox({ selectOnItemFocus: false });
        input.simulate('keydown', { keyCode: $.ui.keyCode.DOWN });
        input.simulate('keyup', { keyCode: $.ui.keyCode.DOWN });
        ok(input.val() == 'c++', 'text equals to c++');
        ComboBox.remove();
    });

    test("autoFilter", function () {
        // test disconected element
        var ComboBox = $('<input />').wijcombobox({ data: getArrayDataSource(), selectOnItemFocus: true });
        var count = 0;
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');

        var input = ComboBox;
        // opens list
        //input.simulate('keydown' ,{ keyCode: $.ui.keyCode.DOWN });
        //input.simulate('keyup' ,{ keyCode: $.ui.keyCode.DOWN });
        input.simulate('keydown', { keyCode: "a" });
        input.simulate('keyup', { keyCode: "a" });
        input.val("a").keydown();
        ok($('.wijmo-wijcombobox-list li.wijmo-wijlist-item').size() == 13, 'no filter');
        stop();
        setTimeout(function () {
            ok($('.wijmo-wijcombobox-list li.ui-state-hover').text() == 'asp', 'highlight correct.');
            ComboBox.wijcombobox({ autoFilter: true });
            input.val("a").keydown();
            setTimeout(function () {
                start();
                //ok($('.wijmo-wijcombobox-list li.wijmo-wijlist-item').size() == 5, 'filtered list');
                $.each($('.wijmo-wijcombobox-list li.wijmo-wijlist-item'), function (index, ele) {
                    if ($(ele).is(":visible")) {
                        count++;
                    }
                });
                ok(count === 5, 'filtered list');
                ComboBox.remove();
            }, 500);
        }, 500);
    });

    test("autoComplete", function () {
        // test disconected element
        var ComboBox = $('<input />').wijcombobox({ autoComplete: false, data: getArrayDataSource(), selectOnItemFocus: true });
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');

        var input = ComboBox;
        // opens list
        input.simulate('keydown', { keyCode: "a" });
        input.simulate('keyup', { keyCode: "a" });
        input.val("a").keydown();
        stop();
        setTimeout(function () {
            ok(input.val() == 'a', 'no autocomplete');
            ComboBox.wijcombobox({ autoComplete: true });
            input.simulate('keydown', { keyCode: "a" });
            input.simulate('keyup', { keyCode: "a" });
            input.val("a").keydown();
            setTimeout(function () {
                ok(input.val() == 'asp', 'autocomplete finished');
                ComboBox.remove();
                start();
            }, 500);

        }, 500);
    });

    test("highlightMatching", function () {
        // test disconected element
        var ComboBox = $('<input />').wijcombobox({ highlightMatching: true, data: getArrayDataSource() });
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');

        var input = ComboBox;
        input.simulate('keydown', { keyCode: "a" });
        input.simulate('keyup', { keyCode: "a" });
        input.val("a").keydown();
        stop();
        setTimeout(function () {
            ok($($('.wijmo-wijcombobox-list li.wijmo-wijlist-item span')[0]).text() == 'a', 'highlight correct');
            ComboBox.wijcombobox({ highlightMatching: false });
            ComboBox.wijcombobox('repaint');
            input.simulate('keydown', { keyCode: "a" });
            input.simulate('keyup', { keyCode: "a" });
            input.val("a").keydown();
            setTimeout(function () {
                ok($('.wijmo-wijcombobox-list li.wijmo-wijlist-item span').size() == 0, 'no highlight');
                start();
                ComboBox.remove();
            }, 1000);
        }, 1000);
    });

    test("disabled", function () {
        var ComboBox = $('<input />').wijcombobox({ disabled: true, data: getArrayDataSource() });
        comboEle = ComboBox.data("wijmo-wijcombobox")._comboElement;
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');
        var input = ComboBox;
        ok(comboEle.hasClass("wijmo-wijcombobox-disabled"), "combo element has disabled class");
        ok(input.hasClass("ui-state-disabled"), "input has disabled class");
        ok(input.attr('disabled'), 'input is disabled');

        ComboBox.wijcombobox({ disabled: false });
        ok(!comboEle.hasClass("wijmo-wijcombobox-disabled"), "combo element has not disabled class");
        ok(!ComboBox.hasClass('wijmo-wijcombobox-disabled ui-state-disabled'), 'input has not disabled classes');
        ok(!input.attr('disabled'), 'input is not disabled');
        ComboBox.remove();
    });

    test("labelText", function () {
        var ComboBox = $('<input />').wijcombobox({ labelText: 'label text!' }), wrapper, trigger, label;
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');
        wrapper = $('.wijmo-wijcombobox-wrapper');
        trigger = $('.wijmo-wijcombobox-trigger');
        label = $('.wijmo-wijcombobox-label');
        var input = ComboBox;
        if (!$.browser.mozilla) {
            ok(input.outerWidth() == (wrapper.innerWidth() - trigger.width() - label.outerWidth() - wrapper.leftBorderWidth()), 'width correct');
        } else {
            ok(input.outerWidth() == (wrapper.width() - trigger.width() - label.outerWidth() - wrapper.leftBorderWidth()), 'width correct');
        }

        ComboBox.remove();
    });

    test("isEditable", function () {
        var ComboBox = $('<input />').wijcombobox({ isEditable: false, data: getArrayDataSource() });
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');
        // a key is pressed

        ComboBox.simulate('keydown', { keyCode: 65 });
        stop();
        setTimeout(function () {
            ok($('.wijmo-wijcombobox-list li.wijmo-wijlist-item.ui-state-hover').html() == 'asp' && ComboBox.val() == '', 'asp is activated.');
            start();
            ComboBox.remove();
        }, 500);
    });

    test("selectedIndex", function () {
        var ComboBox = $('<input />').wijcombobox({ selectedIndex: 1, data: getArrayDataSource() });
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');
        ok(ComboBox.data('wijmo-wijcombobox').selectedItem.label === 'java' && ComboBox.wijcombobox('option', 'selectedIndex') === 1, 'selected item set');
        ok(ComboBox.val() === 'java', 'selected item set');
        ComboBox.remove();
    });

    test("selectedValue", function () {
        var ComboBox = $('<input />').wijcombobox({ data: getArrayDataSource() });
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('option', "selectedValue", "java");
        ComboBox.wijcombobox('repaint');
        ok(ComboBox.data('wijmo-wijcombobox').selectedItem.label === 'java' && ComboBox.wijcombobox('option', 'selectedIndex') === 1, 'selectedValue setting effect selectedIndex');
        ok(ComboBox.val() === 'java', 'selectedValue setting effect selectedIndex');
        ComboBox.wijcombobox('option', "selectedValue", "c++");
        ComboBox.wijcombobox('repaint');
        ok(ComboBox.data('wijmo-wijcombobox').selectedItem.label === 'c++' && ComboBox.wijcombobox('option', 'selectedIndex') === 0, 'selectedValue setting effect selectedIndex');
        ok(ComboBox.val() === 'c++', 'selectedValue setting effect selectedIndex');

        ComboBox.wijcombobox({
            data: [{
                label: 'c++',
                value: 0
            },
            {
                label: 'java',
                value: 1
            }],
            selectedValue: 0
        });
        ok(ComboBox.data('wijmo-wijcombobox').selectedItem.label === 'c++' && ComboBox.wijcombobox('option', 'selectedIndex') === 0, 'The first item should be selected.');
        ComboBox.wijcombobox('option', "selectedValue", 1);
        ok(ComboBox.data('wijmo-wijcombobox').selectedItem.label === 'java' && ComboBox.wijcombobox('option', 'selectedIndex') === 1, 'The second item should be selected.');
        ComboBox.wijcombobox('option', "selectedValue", 0);
        ok(ComboBox.data('wijmo-wijcombobox').selectedItem.label === 'c++' && ComboBox.wijcombobox('option', 'selectedIndex') === 0, 'The first item should be selected.');

        ComboBox.remove();
    });

    test("selectedIndex-multiple", function () {
        var ComboBox = $('<input />').wijcombobox({ selectedIndex: [1, 2, 3], selectionMode: 'multiple', data: getArrayDataSource() });
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');
        ok(ComboBox.data('wijmo-wijcombobox').selectedItems.length === 3 && ComboBox.wijcombobox('option', 'selectedIndex').length === 3, 'selected item set');
        ok(ComboBox.val() === 'java,php,coldfusion', 'selected item set');
        ComboBox.remove();
    });

    test("autoFilter-order", function () {
        var ComboBox = $('<input />').wijcombobox({ isEditable: false, data: getArrayDataSource() });
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');
        // a key is pressed

        ComboBox.simulate('keydown', { keyCode: 80 });
        stop();
        setTimeout(function () {
            ok($('#wijmo-wijlistitem-active').html() == 'php', 'The first item is hightlight');
            ComboBox.simulate('keydown', { keyCode: $.ui.keyCode.DOWN });
            ComboBox.simulate('keydown', { keyCode: $.ui.keyCode.ENTER });
            ok($('.wijmo-wijcombobox-input').val() == 'javascript', 'The selected text is correct');
            //ComboBox.simulate('keydown' ,{ keyCode: $.ui.keyCode.DOWN });
            //ok($('#wijmo-wijlistitem-active').html() == 'javascript', 'The second item is hightlight');
            start();
            ComboBox.remove();
        }, 500);
    });

    function testAutoOrFit(opt) {
        var array = getArrayDataSource(),
            $widget = $("<input>").appendTo(document.body).wijcombobox({ data: array, dropdownWidth: opt }),
            $combo = $widget.wijcombobox("getComboElement"),
            width = $combo.outerWidth(),
            longStr = "TypeScript / CoffeeScript / Dart / ClojureScript / LiveScript",
            longItem = { label: longStr, value: longStr };

        array.push(longItem);
        $widget.wijcombobox("option", "data", array);
        ok($combo.outerWidth() === width, "width is not adjusted.");
        $widget.wijcombobox("option", "adjustComboWidth", true);
        ok($combo.outerWidth() > width, "width is adjusted.");

        array.pop();
        $widget.wijcombobox("option", "data", array);
        ok($combo.outerWidth() < width, "width is adjusted.");
        $widget.remove();

        array.push(longItem);
        $widget = $("<input>").appendTo(document.body).wijcombobox({ adjustComboWidth: true, data: array, dropdownWidth: opt });
        $combo = $widget.wijcombobox("getComboElement");
        ok($combo.outerWidth() > width, "width is adjusted.");
        $widget.remove();
    }

    test("adjustComboWidth (dropdownWidth=auto)", function () {
        testAutoOrFit("auto");
    });

    test("adjustComboWidth (dropdownWidth=fit)", function () {
        testAutoOrFit("fit");
    });

    test("adjustComboWidth (dropdownWidth=number)", function () {
        var array = getArrayDataSource(),
            specifiedWidth = 800,
            $widget = $("<input>").appendTo(document.body).wijcombobox({ data: array, dropdownWidth: specifiedWidth }),
            $combo = $widget.wijcombobox("getComboElement"),
            width = $combo.outerWidth(),
            longStr = "TypeScript / CoffeeScript / Dart / ClojureScript / LiveScript",
            longItem = { label: longStr, value: longStr };

        array.push(longItem);
        $widget.wijcombobox("option", "data", array);
        ok($combo.outerWidth() === width, "width is not adjusted.");
        $widget.wijcombobox("option", "adjustComboWidth", true);
        ok($combo.outerWidth() === specifiedWidth, "width is specified value.");

        array.pop();
        $widget.wijcombobox("option", "data", array);
        ok($combo.outerWidth() === specifiedWidth, "width is specified value.");
        $widget.remove();

        array.push(longItem);
        $widget = $("<input>").appendTo(document.body).wijcombobox({ adjustComboWidth: true, data: array, dropdownWidth: specifiedWidth });
        $combo = $widget.wijcombobox("getComboElement");
        ok($combo.outerWidth() === specifiedWidth, "width is specified value.");
        $widget.remove();
    });

    test("dropdownWidth: fit (adjustComboWidth=false)", function () {
        var array = getArrayDataSource(),
            $widget = $("<input>").appendTo(document.body).wijcombobox({ data: array, adjustComboWidth: false }),
            $combo = $widget.wijcombobox("getComboElement"),
            $list = $widget.data("wijmo-wijcombobox").menu.element,
            $trigger = $combo.find(".wijmo-wijcombobox-trigger"),
            width = $combo.outerWidth(), listWidth = 0,
            longStr = "TypeScript / CoffeeScript / Dart / ClojureScript / LiveScript",
            longItem = { label: longStr, value: longStr };

        $trigger.simulate("click");
        listWidth = $list.outerWidth();

        array.push(longItem);
        $widget.wijcombobox("option", "data", array);
        ok($combo.outerWidth() === width, "width is not adjusted.");
        ok($list.outerWidth() === listWidth, "list width is not adjusted.");
        $widget.wijcombobox("option", "dropdownWidth", "fit");
        ok($combo.outerWidth() === width, "width is not adjusted.");
        ok($list.outerWidth() > listWidth, "list width is adjusted.");

        array.pop();
        $widget.wijcombobox("option", "data", array);
        ok($combo.outerWidth() === width, "width is not adjusted.");
        ok($list.outerWidth() === width, "list width is adjusted.");
        $widget.remove();

        array.push(longItem);
        $widget = $("<input>").appendTo(document.body).wijcombobox({ dropdownWidth: "fit", data: array, adjustComboWidth: false });
        $combo = $widget.wijcombobox("getComboElement");
        $list = $widget.data("wijmo-wijcombobox").menu.element;
        $trigger = $combo.find(".wijmo-wijcombobox-trigger");
        $trigger.simulate("click");
        ok($combo.outerWidth() === width, "width is not adjusted.");
        ok($list.outerWidth() > listWidth, "list width is adjusted.");
        $widget.remove();
    });

    test("dropdownWidth: fit (adjustComboWidth=true)", function () {
        var array = getArrayDataSource(),
            $widget = $("<input>").appendTo(document.body).wijcombobox({ data: array, adjustComboWidth: true }),
            $combo = $widget.wijcombobox("getComboElement"),
            $list = $widget.data("wijmo-wijcombobox").menu.element,
            $trigger = $combo.find(".wijmo-wijcombobox-trigger"),
            width = $combo.outerWidth(), listWidth = 0,
            longStr = "TypeScript / CoffeeScript / Dart / ClojureScript / LiveScript",
            longItem = { label: longStr, value: longStr };

        $trigger.simulate("click");
        listWidth = $list.outerWidth();

        array.push(longItem);
        $widget.wijcombobox("option", "data", array);
        ok($list.outerWidth() > listWidth, "list width is adjusted.");
        ok($combo.outerWidth() === $list.outerWidth(), "width equals list width.");
        $widget.wijcombobox("option", "dropdownWidth", "fit");
        ok($list.outerWidth() > listWidth, "list width is adjusted.");
        ok($combo.outerWidth() === $list.outerWidth(), "width equals list width.");

        array.pop();
        $widget.wijcombobox("option", "data", array);
        ok($list.outerWidth() === listWidth, "list width is adjusted.");
        ok($combo.outerWidth() === $list.outerWidth(), "width equals list width.");
        $widget.remove();

        array.push(longItem);
        $widget = $("<input>").appendTo(document.body).wijcombobox({ dropdownWidth: "fit", data: array, adjustComboWidth: true });
        $combo = $widget.wijcombobox("getComboElement");
        $list = $widget.data("wijmo-wijcombobox").menu.element;
        $trigger = $combo.find(".wijmo-wijcombobox-trigger");
        $trigger.simulate("click");
        ok($list.outerWidth() > listWidth, "list width is adjusted.");
        ok($combo.outerWidth() === $list.outerWidth(), "width equals list width.");
        $widget.remove();
    });

})(jQuery);
