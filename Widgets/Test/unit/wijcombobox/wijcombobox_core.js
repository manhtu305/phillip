/*
 * c1combobox_core.js
 */

function getArrayDataSource(){
    var testArray = [
        {
            label: 'c++',
            value: 'c++'
        }, 
        {
            label: 'java',
            value: 'java'
        }, 
        {
            label: 'php',
            value: 'php'
        }, 
        {
            label: 'coldfusion',
            value: 'coldfusion'
        }, {
            label: 'javascript',
            value: 'javascript'
        }, 
        {
            label: 'asp',
            value: 'asp'
        }, 
        {
            label: 'ruby',
            value: 'ruby'
        },
        {
            label: 'python',
            value: 'python'
        },
        {
            label: 'c',
            value: 'c'
        },
        {
            label: 'scala',
            value: 'scala'
        },
        {
            label: 'groovy',
            value: 'groovy'
        },
        {
            label: 'haskell',
            value: 'haskell'
        },
        {
            label: 'perl',
            value: 'perl'
        }
    ];
    return testArray;
}

function generateData(count, cell) {
    var arr = [];
    for (var i = 0; i < count; i++) {
        var o = {};
        o.labelText = 'Label ' + (i + 1);
        o.valueText = 'Value ' + (i + 1);
        if (i == 0)
            o.labelText = '';
        var cells = [];
        for (var j = 0; j < cell; j++) {
            if (i == 0) {
                cells.push("<image src='images/P1.gif'/>");
                cells.push("Empty Data");
            }
            else {
                cells.push("<image src='images/P4.gif'/>");
                cells.push("P" + (i + 1));
            }

        }
        o.cells = cells;
        arr.push(o);
    }
    return arr;
}

function getDataSource(){
    var testArray = ["c++", "java", "php", "coldfusion", "javascript", "asp", "ruby", "python", "c", "scala", "groovy", "haskell", "perl"];
    var myReader = new wijarrayreader([
        {
            name: 'label'
        }, 
        {
            name: 'value'
        }
    ]);
    var datasourceOptions = {
        reader: myReader,
        data: testArray
    };
    var datasource = new wijdatasource(datasourceOptions);
    return datasource;
}

(function($){

    module("wijcombobox: core");

    test("create and destroy", function(){
        // test disconected element
        var disconected = $('<input />').wijcombobox();
        ok(disconected.hasClass('wijmo-wijcombobox-input'), 'element css classes created.');
        
        ok(disconected.wijcombobox('repaint')==false, 'disconected element is not visible, repaint returns false.');
        disconected.wijcombobox('getComboElement').appendTo(document.body);
        ok(disconected.wijcombobox('repaint')==true, 'disconected element is appended to body and visible, repaint returns true.');
        disconected.wijcombobox('destroy');
        ok(disconected.parent()[0] == document.body, 'all element removed');
        disconected.remove();
    });
    
    test("select element conversion", function(){
        // test disconected element
        var disconected = $('<select><option value="options1">options1</option><option value="options2">options2</option><option value="options3">options3</option></select>').appendTo(document.body).wijcombobox();
        ok(disconected.wijcombobox('option','data').length == 3,'items created');
        var t = $('.wijmo-wijcombobox-trigger');
        t.simulate('mouseover'); 
        t.simulate('click');
        t.simulate('mouseout');
        var item = $('.wijmo-wijlist-item:last');
        item.simulate('mouseover'); 
        item.simulate('click');
        ok(disconected.wijcombobox('option','selectedIndex') == 2 && disconected.wijcombobox('option','selectedIndex') == disconected[0].selectedIndex, 'selected index changed');
        disconected.wijcombobox('destroy');
        
        disconected.remove();
    });
    
    test("enter ", function(){
        var ComboBox = $('<input />').wijcombobox({isEditable:false, data: getArrayDataSource()});
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');
        // a key is pressed
        
        ComboBox.simulate('keydown' ,{keyCode:80});
        stop();
        setTimeout(function (){
            ok($('#wijmo-wijlistitem-active').html() == 'php', 'The first item is hightlight');
            ComboBox.simulate('keydown' ,{ keyCode: $.ui.keyCode.DOWN });
            ComboBox.simulate('keydown' ,{ keyCode: $.ui.keyCode.ENTER });
            ok($('.wijmo-wijcombobox-input').val() == 'javascript', 'The selected text is correct');
            //ComboBox.simulate('keydown' ,{ keyCode: $.ui.keyCode.DOWN });
            //ok($('#wijmo-wijlistitem-active').html() == 'javascript', 'The second item is hightlight');
            start();
            ComboBox.remove();
        },500);
    });

})(jQuery);

