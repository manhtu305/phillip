/*
* wijcombobox_tickets.js
*/
(function ($) {

	module("wijcombobox:tickets");

	test("combobox in iframe", function () {
		var iframe = $("<iframe src='wijcombobox_iframeSrc.html'></iframe>").appendTo("body");
        window.setTimeout(function () {
            if (iframe[0].contentWindow.document.title === "exception throws") {
				ok(false, "exception throws.");
			} else {
				ok(true, "no exception throws.");
			}
			iframe.remove();
			start();
        }, 1000);
		stop();
	});

	if ($.browser.msie && (parseInt($.browser.version, 10) === 10)) {
		test("#38753", function () {
			var ComboBox = $('<input />').wijcombobox({ data: getArrayDataSource() });
			ComboBox.wijcombobox('getComboElement').appendTo(document.body);
			ComboBox.wijcombobox('repaint');
			var trigger = $('.wijmo-wijcombobox-trigger');
			trigger.simulate('mouseover').simulate('click').simulate('mouseout');
			var firstHeight = $('.wijmo-wijcombobox-list').outerHeight();

			trigger.simulate('mouseover').simulate('click').simulate('mouseout');
			var secondHeight = $('.wijmo-wijcombobox-list').outerHeight();
			ok(firstHeight === secondHeight, 'height of list is correct under second opening.');

			ComboBox.remove();
		});
	}

	test("focus the element before create the combobox, when created the combobox widget, the focus will lost.", function () {
		var combobox = $('<input />').appendTo("body").focus();
		ok(document.activeElement == combobox[0], "the element is foucsed.");
		combobox.wijcombobox();
		ok(document.activeElement == combobox[0], "the element is foucsed.");
		combobox.remove();
	});

	test("#39687", function () {
		var data = [{ label: 'test1', value: 1 }, { label: 'test2', value: 2 }],
			combobox1 = $('<input />').appendTo("body").wijcombobox({ data: data }),
			combobox2 = $('<input />').appendTo("body").wijcombobox({ data: data }),
			ele1 = combobox1.data("wijmo-wijcombobox")._comboElement,
			ele2 = combobox2.data("wijmo-wijcombobox")._comboElement,
			list1 = combobox1.data("wijmo-wijcombobox")._menuUL,
			list2 = combobox2.data("wijmo-wijcombobox")._menuUL;

		ele1.find(".ui-icon-triangle-1-s").simulate("click");
		list1.find(".wijmo-wijlist-item:first").simulate("click");
		ok(combobox1.prop("value") === "test1", "the value is seted to combobox when click the first combobox list item.");

		ele2.find(".ui-icon-triangle-1-s").simulate("click");
		list2.find(".wijmo-wijlist-item:last").simulate("click");
		ok(combobox2.prop("value") === "test2", "the value is seted to combobox when click the second combobox list item.");
		combobox1.remove();
		combobox2.remove();
	});

	test("#40450", function () {
		var data = [{ label: 'test1', value: 1 }, { label: 'test2', value: 2 }],
			combobox1 = $('<input />').appendTo("body").wijcombobox({ data: data, selectionMode: "multiple" }),
			ele1 = combobox1.data("wijmo-wijcombobox")._comboElement,
			list1 = combobox1.data("wijmo-wijcombobox")._menuUL;

		ele1.find(".ui-icon-triangle-1-s").simulate("click");
		list1.find(".wijmo-wijlist-item:first").simulate("click");
		ok(combobox1.prop("value") === "test1", "when click the first item, the first item is selected.");
		ele1.find(".ui-icon-triangle-1-s").simulate("click");
		list1.find(".wijmo-wijlist-item:last").simulate("click");
		ok(combobox1.prop("value") === "test1,test2", "when selected the last item, the first and last item are selected.");
		ele1.find(".ui-icon-triangle-1-s").simulate("click");
		list1.find(".wijmo-wijlist-item:first").simulate("click");
		ok(combobox1.prop("value") === "test2", "when selected the first item again, the first item is deselected.");
		ele1.find(".ui-icon-triangle-1-s").simulate("click");
		list1.find(".wijmo-wijlist-item:last").simulate("click");
		ok(combobox1.prop("value") === "", "when selected the last item again, the last item is deselected.");
		combobox1.remove();
	});

	test("#40478", function () {
		var testArray = ["c++", "java", "php"],
			myReader = new wijarrayreader([{
				name: 'label'
			}, {
				name: 'value'
			}, {
				name: 'selected',
				defaultValue: false
			}]),
			datasource = new wijdatasource({
				reader: myReader,
				data: testArray
			}),
			combobox1 = $('<input />').appendTo("body").wijcombobox({ data: datasource }),
			ele1 = combobox1.data("wijmo-wijcombobox")._comboElement,
			list1 = combobox1.data("wijmo-wijcombobox")._menuUL,
			listItems;

		ele1.find(".ui-icon-triangle-1-s").simulate("click");
		listItems = list1.children();
		listItems.eq(0).simulate("click");
		ele1.find(".ui-icon-triangle-1-s").simulate("click");
		list1.find(".wijmo-wijlist-item-last").simulate("mouseover").simulate("click");
		ele1.find(".ui-icon-triangle-1-s").simulate("click");
		ok(list1.find(".wijmo-wijlist-item-selected").length == 1, "when select the second item, the first item will deselected");
		combobox1.remove();
	});

	test("#40774", function () {
		var combobox = $('<input />').appendTo("body"), comboboxEle, wrapper, trigger, label;
		combobox.wijcombobox();
		comboboxEle = combobox.data("wijmo-wijcombobox")._comboElement;
		wrapper = $('.wijmo-wijcombobox-wrapper', comboboxEle);
		trigger = $('.wijmo-wijcombobox-trigger', comboboxEle);
		label = $('.wijmo-wijcombobox-label', comboboxEle);
		ok(combobox.outerWidth() + trigger.outerWidth() + label.outerWidth() == wrapper.outerWidth() - wrapper.leftBorderWidth(), 'width correct');
		combobox.remove();
	});
	
	test("#40998", function () {
		var data = [{ label: 'test1', value: 1 }, { label: 'test2', value: 2 }],
		   combobox1 = $('<input />').appendTo("body").wijcombobox({ data: data, selectedValue: 1 }),
		   combobox2 = $('<input />').appendTo("body").wijcombobox({ data: data, selectedValue: 2 }),
			ele1 = combobox1.data("wijmo-wijcombobox")._comboElement,
			ele2 = combobox2.data("wijmo-wijcombobox")._comboElement,
			list1 = combobox1.data("wijmo-wijcombobox")._menuUL,
			list2 = combobox2.data("wijmo-wijcombobox")._menuUL;

		ele1.find(".ui-icon-triangle-1-s").simulate("click");
		ok(list1.find(".ui-state-hover").length === 1, "the combobox1 has an item selected");
		ele2.find(".ui-icon-triangle-1-s").simulate("click");
		ok(list2.find(".ui-state-hover").length === 1, "the combobox2 has an item selected");
		combobox1.remove();
		combobox2.remove();
	});

	test("#41319", function () {
		var data = [{ label: 'test1', value: 1 }, { label: 'test2', value: 2 }],
			data2 = [{ label: 'A', value: "a" }, { label: "B", value: 'b' }, { label: "C", value: 'c' }],
			combobox = $('<input />').appendTo("body").wijcombobox({ data: data }),
			ele = combobox.data("wijmo-wijcombobox")._comboElement,
			list = combobox.data("wijmo-wijcombobox")._menuUL;
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		list.find(".wijmo-wijlist-item-first").simulate("mouseover").simulate("click");
		combobox.wijcombobox({ data: data2 });
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		ok(true, "the combobox works well.");
		combobox.remove();
	});

	test("when set the selected in the data option, the combobox's text is empty", function () {
		data = [{ label: 'test1', value: 1 }, { label: 'test2', value: 2, selected: true }],
			combobox = $('<input />').appendTo("body").wijcombobox({ data: data });
		ok(combobox.val() === "test2", "the textbox has seted the value.");
		ok(combobox.wijcombobox("option", "selectedIndex") === 1, "the selected index has changed.");
		combobox.remove();
	})

	test("when set the selectedValue option, list's selected index is not changed.", function () {
		data = [{ label: 'test1', value: 1 }, { label: 'test2', value: 2, selected: true }],
			combobox = $('<input />').appendTo("body").wijcombobox({ data: data });
		ok(combobox.val() === "test2", "the textbox has seted the value.");
		ok(combobox.wijcombobox("option", "selectedIndex") === 1, "The selected index has changed.");
		combobox.remove();
	})

	test("when init combobox with text option, the combobox list is not selected.", function () {
		var data = [{ label: 'test1', value: 1 }, { label: 'test2', value: 2 }],
            combobox = $('<input />').appendTo("body").wijcombobox({ data: data, text: "test2" }),
			ele = combobox.data("wijmo-wijcombobox")._comboElement,
			list = combobox.data("wijmo-wijcombobox")._menuUL;
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		ok(list.find(".wijmo-wijlist-item:last").hasClass("wijmo-wijlist-item-selected"), "The item has added the selected class.");
		ok(combobox.val() === "test2", "the combobox value is seted.");
		ok(combobox.wijcombobox("option", "selectedIndex") === 1, "the selected Index is 1.");
		combobox.remove();
	});

	test("33666-1[Tab]", function () {
		var i = "",
			ComboBox = $('<input id="input33661" type="textbox"/>').appendTo(document.body)
            .wijcombobox({
						textChanged: function (e, item) {
							i = "aaa";
						},
						data: getArrayDataSource()
            });
		ComboBox.wijcombobox('repaint');
		ComboBox.focus();
		$("#input33661").val('1');
		ComboBox.simulate('keydown', null);
		ComboBox.trigger("blur");
		window.setTimeout(function () {
			ok(i === "aaa", 'textchanged event fired!');
			ComboBox.remove();
			start();
		}, 500);
		stop();
	});

	test("#41642", function () {
	    var data = [{ label: "test1", value: 1 }, { label: "test2", value: 2 }], result = false,
		    inputEle, menuUL,
		    combobox41642 = $("<input id='input41642' />").appendTo("body").wijcombobox({
		        data: data,
		        selectedIndex: 0,
		        open: function () {
		            result = !($("li:first", menuUL).hasClass("wijmo-wijlist-item-selected ui-state-active"));
		        }
		    });

	    inputEle = combobox41642.data("wijmo-wijcombobox")._input;
	    menuUL = combobox41642.data("wijmo-wijcombobox")._menuUL;
		
		inputEle.val("");
		inputEle.simulate("keydown");
		setTimeout(function () {
		    ok(result, "The selected item of the combobox list is reset to unselect, if set the value of the combobox to empty.");
		    combobox41642.remove();
		    start();
		}, 500);
		stop();
	});

	test("#41697", function () {
		var data = [{ label: "test1", value: 1 }, { label: "test2", value: 2 }, { label: "test3", value: 3 }],
		   combobox41697 = $("<input />").appendTo("body").wijcombobox({ data: data, selectionMode: "multiple" }), trigger, menuUL;
		combobox41697.wijcombobox({ selectedIndex: [1, 2] });
		ok(combobox41697.val() === "test2,test3", "the text is seted to the combobox.");
		trigger = combobox41697.data("wijmo-wijcombobox")._triggerArrow;
		menuUL = combobox41697.data("wijmo-wijcombobox")._menuUL;
		trigger.simulate("click");
		ok($(".wijmo-wijlist-item-selected", menuUL).length === 2, "the two items are selected.");
		combobox41697.remove();
	});

	test("#41843", function () {
		var data = [{ label: 'test1', value: 1 }, { label: 'test2', value: 2 }],
			data2 = [{ label: 'A', value: "a" }, { label: "B", value: 'b' }, { label: "C", value: 'c' }],
			combobox = $('<input />').appendTo("body").wijcombobox({ data: data }),
			ele = combobox.data("wijmo-wijcombobox")._comboElement,
			list = combobox.data("wijmo-wijcombobox")._menuUL;
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		list.find(".wijmo-wijlist-item-first").simulate("mouseover").simulate("click");
		combobox.wijcombobox({ data: data2, selectedIndex: 2 });
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		ok(!list.find(".wijmo-wijlist-item").first().hasClass("wijmo-wijlist-item-selected"), "the first item is deselected.");
		ok(list.find(".wijmo-wijlist-item").last().hasClass("wijmo-wijlist-item-selected"), "the last item is selected.");
		combobox.remove();
	});

	test("#41847", function () {
		var data = [{ label: 'test1', value: 1 }, { label: 'test2', value: 2 }],
			data2 = [{ label: 'A', value: "a" }, { label: "B", value: 'b' }, { label: "C", value: 'c' }],
			combobox = $('<input />').appendTo("body").wijcombobox({ data: data2 }),
			ele = combobox.data("wijmo-wijcombobox")._comboElement,
			list = combobox.data("wijmo-wijcombobox")._menuUL;
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		list.find(".wijmo-wijlist-item").last().simulate("mouseover").simulate("click");
		combobox.wijcombobox({ data: data });
		ok(true, "no exception thrown.");
		combobox.remove();
	});

	test("#41955", function () {
		var data = [{ label: "test1", value: 1, selected: true }, { label: "test2", value: 2 }, { label: "test3", value: 3 }],
		   combobox = $("<input />").appendTo("body").wijcombobox({ data: data, selectionMode: "multiple" }),
			ele = combobox.data("wijmo-wijcombobox")._comboElement,
			list = combobox.data("wijmo-wijcombobox")._menuUL;
		combobox.wijcombobox({ selectedIndex: -1 });
		ok(combobox.val() === "", "the text is empty");
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		ok(list.find(".wijmo-wijlist-item-selected").length === 0, "no item has selected");
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		combobox.wijcombobox({ data: data, selectedIndex: 1 });
		ok(combobox.val().trim() === "test2", "the text is empty");
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		ok(list.find(".wijmo-wijlist-item").eq(1).hasClass("wijmo-wijlist-item-selected"), "the second item has selected");
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		combobox.wijcombobox({ selectedIndex: -1 });
		combobox.wijcombobox({ selectionMode: "multiple", data: data });
		combobox.wijcombobox({ selectedIndex: -1 });
		ok(combobox.val() === "", "the text is empty");
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		ok(list.find(".wijmo-wijlist-item-selected").length === 0, "no item has selected");
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		combobox.wijcombobox({ data: data, selectedIndex: 1 });
		ok(combobox.val().trim() === "test2", "the text is empty");
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		ok(list.find(".wijmo-wijlist-item").eq(1).hasClass("wijmo-wijlist-item-selected"), "the second item has selected");
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		combobox.remove();
	});

	test("#42229:the additional added item can be selected.", function () {
		var data = [{ label: "test1", value: 1 }, { label: "test2", value: 2 }],
            combobox = $("<input />").appendTo("body").wijcombobox({ data: data }),
			list = combobox.data("wijmo-wijcombobox")._menuUL.parent();

		list.wijlist("addItem", { label: "test3", value: 3 });
		combobox.data("wijmo-wijcombobox")._triggerArrow.simulate("click");
		$(list.find(".wijmo-wijlist-item:last")).simulate("click");

		ok(combobox.val() === "test3", "The additional added item can be selected.");
		combobox.remove();
	});

	test("42012", function () {
		var i = "",
			ComboBox = $('<input  type="textbox"/>').appendTo(document.body)
			.wijcombobox(
					{
						autoComplete: false,
						textChanged: function (e, item) {
							i = "aaa";
						},
						data: getArrayDataSource()
					}
				);
		ComboBox.wijcombobox('repaint');
		ComboBox.focus();
		ComboBox.val('p');
		ComboBox.simulate('keydown', null);
		ComboBox.trigger("blur");
		window.setTimeout(function () {
			//ComboBox.simulate('keydown', { keyCode: $.ui.keyCode.TAB });
			ok(i === "aaa", 'textchanged event fired!');
			ComboBox.remove();
			start();
		}, 500);
		stop();
	});

	test("#42690", function () {
		var data = [{ value: null }, { label: 'test2', value: 2 }],
			combobox1 = $('<input />').appendTo("body").wijcombobox({ data: data }),
			ele1 = combobox1.data("wijmo-wijcombobox")._comboElement,
			list1 = combobox1.data("wijmo-wijcombobox")._menuUL;

		ele1.find(".ui-icon-triangle-1-s").simulate("click");
		ok(list1.find(".wijmo-wijlist-item:first").height() > 10, " the height of  empty field is correct!");
		combobox1.remove();
	});

	if ($.browser.msie) {
		test("#42691", function () {
			var exception;
			window.setTimeout(function () {
				try {
					var data = [{ value: 1, selected: true }, { label: "test2", value: 2 }],
					combobox = $("<input />").appendTo("body").wijcombobox({ data: data });
					combobox.focus();
					combobox.trigger("blur");
				}
				catch (e) { };
				combobox.remove();
				ok(true, "Flag for test.")
				start();
			}, 1000);

			stop();
		});
	}

	test("#38415:Set the selectElementWidthFix when create the wijcombobox control", function () {
	    var combobox38415Set = $("<select id='combo'>" +
				"<option value='c++'>c++</option>" +
				"<option value='Java'>Java</option>" +
				"<option value='php'>php</option>" +
				"<option value='coldfusion'>coldfusion</option>" +
				"<option value='csharp'>csharp</option>" +
				"<\select>").appendTo("body").wijcombobox({
				    dropdownHeight: 100,
				    selectElementWidthFix: 100
				}), comboboxData,
                selectWidth, triggerWidth, borderWidth, inputWidth;

	    comboboxData = combobox38415Set.data("wijmo-wijcombobox");

	    selectWidth = comboboxData.element.width();
        triggerWidth = comboboxData._triggerArrow.width();
        borderWidth = comboboxData._triggerArrow.leftBorderWidth();

        setTimeout(function () {
		    inputWidth = comboboxData._input.width();
		    ok(inputWidth === selectWidth + 100 - (triggerWidth + borderWidth), "The width of the wijcombobox is adjusted with the selectElementWidthFix option, if the scroll bar shows in the dropdown list.");
			combobox38415Set.remove();
			start();
		}, 500);
		stop();
	});

	test("#38415:Changing the selectElementWidthFix options works", function () {
	    var combobox38415Change = $("<select id='combo'>" +
				"<option value='c++'>c++</option>" +
				"<option value='Java'>Java</option>" +
				"<option value='php'>php</option>" +
				"<option value='coldfusion'>coldfusion</option>" +
				"<option value='csharp'>csharp</option>" +
				"<\select>").appendTo("body").wijcombobox({
					dropdownHeight: 100
				}), comboboxData,
			originalInputWidth,
			changedInputWidth;
	    comboboxData = combobox38415Change.data("wijmo-wijcombobox");

		setTimeout(function () {
		    originalInputWidth = comboboxData._input.width();
		    combobox38415Change.wijcombobox({ selectElementWidthFix: 100 });
			changedInputWidth = comboboxData._input.width();
			ok(changedInputWidth === originalInputWidth + 100 - 6 /* The default selectElementWidthFix value. */, "The width of wijcombobox is changed with the changing of selectElementWidthFix option, if the scroll bar shows in the dropdown list.");
			combobox38415Change.remove();
			start();
		}, 500);
		stop();
	});

	test("#42893", function () {
		var data = [{ label: '', value: 1 }, { label: 'test2', value: 2 }],
			combobox = $('<input />').appendTo("body").wijcombobox({ data: data }),
			ele = combobox.data("wijmo-wijcombobox")._comboElement,
			list = combobox.data("wijmo-wijcombobox")._menuUL;
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		list.find(".wijmo-wijlist-item").first().simulate("mouseover").simulate("click");
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		ok(combobox.wijcombobox("option", "selectedIndex") === 0, "the selectedIndex is correct.");
		ok(list.find(".wijmo-wijlist-item").first().hasClass("wijmo-wijlist-item-selected"), "The selected CSS class has added to the first item.");
		combobox.remove();
	});

	test("#42891", function () {
		var data = [{ value: 1 }, { label: 'test2', value: 2 }],
			combobox = $('<input type="text" />').appendTo("body").wijcombobox({ data: data }),
			ele = combobox.data("wijmo-wijcombobox")._comboElement,
			list = combobox.data("wijmo-wijcombobox")._menuUL;
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		$(list.find(".wijmo-wijlist-item:last")).simulate("click");
		combobox.simulateKeyStroke("[DOWN][DOWN]", 200, null, function () {
			ok(true, "No exception thrown.");
			combobox.remove();
			start();
		});
		stop();
	});

	test("#43293", function () {
		var data = [{ value: 1 }, { label: 'test2', value: 2 }],
			combobox = $('<input type="text" />').appendTo("body").wijcombobox({ data: data });
		combobox.val("s");
		combobox.focus();
		combobox.simulateKeyStroke("s", 200, null, function () {
			ok(true, "No exception thrown.");
			combobox.remove();
			start();
		});
		stop();
	});

	test("#43438", function () {
		var testArray = generateData(5, 1),
		 myReader = new wijarrayreader([{
			name: 'label', mapping: "labelText"
		}, {
			name: 'value', mapping: "valueText"
		}, {
			name: 'selected',
			defaultValue: false
		}, {
			name: 'cells'
		}]),
		datasource = new wijdatasource({
			reader: myReader,
			data: testArray
		}),
		combobox = $('<input type="text" />').appendTo("body").wijcombobox({
			data: datasource,
			dropdownWidth: 250,
			width: 250,
			columns: [{
				name: 'Image'
			}, {
				name: 'Name'
			}],
			open: function () {
				$(".wijmo-wijcombobox-cell", $(".wijmo-wijcombobox-cell").closest(".wijmo-wijlist-ul"))
												.not(".ui-widget-header")
												.height($(".wijmo-wijcombobox-row", $(".wijmo-wijcombobox-cell").closest(".wijmo-wijlist-ul")).height());
			}
		}), ele, list;

		ele = combobox.data("wijmo-wijcombobox")._comboElement,
		list = combobox.data("wijmo-wijcombobox")._menuUL;
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		$(list.find(".wijmo-wijlist-item:first")).simulate("click");
		
		combobox.blur();
		ele.find(".ui-icon-triangle-1-s").simulate("click");
		ok(list.find(".wijmo-wijlist-item").first().hasClass("ui-state-active"), "The selected CSS class has added to the first item.");
		combobox.remove();
	});

	test("#44326", function () {
		var i = "ok", v,
			data = [{ label: 'aa', value: 1 }, { label: 'bb', value: 2 }],
			combobox44326 = $('<input id="input44326" type="text"/>').appendTo("body").wijcombobox({
				data: data,
                selectedIndex: 0,
				textChanged: function () {
					i = "bad";
				}
			});
		$("#input44326").val("");
		combobox44326.simulate("keydown");
		window.setTimeout(function () {
			ok(i === "ok", "textChanged event don't fire!");
			combobox44326.remove();
			start();
		}, 500);
		stop();
	});

	test("#45885", function () {
		var t = 0;
		var proxy = new wijhttpproxy({
			url: "http://ws.geonames.org/searchJSON",
			dataType: "jsonp",
			data: {
				featureClass: "P",
				style: "full",
				maxRows: 12
			},
			key: 'geonames'
		});

		var myReader = new wijarrayreader([{
			name: 'label',
			mapping: function (item) {
				return item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName
			}
		}, {
			name: 'value',
			mapping: 'name'
		}, {
			name: 'selected',
			defaultValue: false
		}]);

		var datasource = new wijdatasource({
			reader: myReader,
			proxy: proxy,
			loaded: function () {
                if (t === 0) {
					t++;
					//first time
                    combobox.simulate('keydown', { keyCode: $.ui.keyCode.DOWN });
                    combobox.simulate('keydown', { keyCode: $.ui.keyCode.ENTER });
					combobox.val("");
					combobox.val("shang");
					combobox.simulate("keydown");
                } else if (t === 1) {
					//second time
					ok(true, "get correct remote datasrouce in the second time, no exception throws");
					combobox.remove();
					start();
				}
			}
		});

		var combobox = $('<input id="cmb" type="text"/>').appendTo("body").wijcombobox({
			data: datasource,
			showTrigger: false,
			//use the input value to search the list.
			search: function (e, obj) {
				obj.datasrc.proxy.options.data.name_startsWith = obj.term.value;
			}
		});

		combobox.focus();
		combobox.val("shang");
		combobox.simulate("keydown");
		stop();
	});

	test("#46239", function () {
		var testArray = ["c++", "java", "php"],
			myReader = new wijarrayreader([{
				name: 'label'
			}, {
				name: 'value'
			}, {
				name: 'selected',
				defaultValue: false
			}]),
			datasource = new wijdatasource({
				reader: myReader,
				data: testArray
			}),
			combobox1 = $('<input />').appendTo("body").wijcombobox({
				data: datasource,
				text: 'l',
				changed: function (e, data) {
					ok(true, "No exception is thown.");
					combobox1.remove();
				}
			});

		combobox1.focus();
		combobox1.val("j");
		combobox1.simulate("keydown", null);
		window.setTimeout(function () {
			combobox1.trigger("blur");
			start();
		}, 500);
		stop();
	});

	test("#47371", function () {
		var container = $('<div id="Div47371"></div>').appendTo("body"),
			combobox = $('<input />').appendTo(container), input, wrapper;
		combobox.wijcombobox({ "triggerPosition": "left" });
		input = container.find(".wijmo-wijcombobox-input");
		wrapper = container.find(".wijmo-wijcombobox-wrapper");
		ok(wrapper.innerWidth() === input.outerWidth() + parseFloat(input.css("margin-left")), "Border will not be covered!");

		combobox.wijcombobox({ "triggerPosition": "right" });
		ok(wrapper.innerWidth() === input.outerWidth() + parseFloat(input.css("margin-right")), "Input render correctly!");

		combobox.remove();
		container.remove();
	});

	test("#46972", function () {
		var container = $('<div id="Div46972"></div>').appendTo("body"),
			combobox = $('<input />').appendTo(container), selectedIndex, result = false;
		combobox.wijcombobox({
			"selectionMode": "multiple",
			data: [{
				label: 'c++',
				value: 'c++1',
				selected: true
			}, {
				label: 'java',
				value: 'java',
				selected: true
			}, {
				label: 'php',
				value: 'php',
				selected: true
			}]
		});
		selectedIndex = combobox.wijcombobox("option", "selectedIndex");
		result = selectedIndex.length === 3 && selectedIndex[0] === 0 && selectedIndex[1] === 1 && selectedIndex[2] === 2;
		ok(result, "SelectedIndex in options has correct value!");
		combobox.remove();
		container.remove();
	});

	test("#48024", function () {
		var i = 0,
			list,
			container = $('<div id="Div46972"></div>').appendTo("body"),
			combobox = $('<select id="combobox2" style="width: 100px" >' +
			'<option></option>' +
			'<option>1</option>' +
			'<option>2</option>' +
			'<option>3</option>' +
			'<option>4</option>' +
			'<option>5</option>' +
			'<option>6</option>' +
			'<option>7</option>' +
			'<option>8</option>' +
			'<option>9</option>' +
			'<option>10</option></select>').appendTo(container), selectedIndex, result = false;
		combobox.bind("change", function () {
			i = 1;
		}).wijcombobox({ autoComplete: false });

		$(".wijmo-wijcombobox-input", container).val("1");
		$(".wijmo-wijcombobox-input", container).simulate("keydown");
		list = combobox.data("wijmo-wijcombobox")._menuUL;

		window.setTimeout(function () {
			ok(list.children(":visible.wijmo-wijlist-item").length === 2, "The visible item is 2");
			$(list.children(":visible.wijmo-wijlist-item")[0]).simulate("mouseover");
			$(list.children(":visible.wijmo-wijlist-item")[0]).simulate("mousedown");
			$(".wijmo-wijcombobox-input", container).blur();
			$(list.children(":visible.wijmo-wijlist-item")[0]).simulate("click");
			ok(i === 1, "change event fired");
			start();
			combobox.remove();
			container.remove();
		}, 300);
		stop();
	});

	test("#47257, #48096", function () {
		var container = $('<div id="Div47257"></div>').appendTo("body"),
			combobox = $('<input />').appendTo(container), input, result = false;
		combobox.wijcombobox({
			data: [{
				label: 'P1',
				value: 'P1',
			}, {
				label: 'P2',
				value: 'P2',
			}, {
				label: '',
				value: '',
				selected: true
			}],
			selectedIndexChanged: function (e, data) {
				result = !data.oldItem.element.hasClass("wijmo-wijcombobox-selecteditem");
				ok(result, "Selected style has been removed");

				combobox.remove();
				container.remove();
			    // There is a delay for focus opeartion in IE.
				if ($.browser.msie) {
				    start();
				}
			}
		});

		input = container.find(".wijmo-wijcombobox-input");
		
		input.one("focus", function () {
			input.val("P1");
		})
		input.focus();
		input.blur();
		if ($.browser.msie) {
			stop();
		}
	})

	test("#48088", function () {
	    var container = $('<div id="Div48088"></div>').appendTo("body"),
	        combobox = $('<input />').appendTo(container), input, itemsList, result = false;
	    combobox.wijcombobox({
	        data: [{
	            label: 'T3',
	            value: '',
	        }, {
	            label: 'P1',
	            value: '',
	        }, {
	            label: 'FFFF',
	            value: '',
	        }, {
	            label: 'SSSS',
	            value: '',
	        }, {
	            label: 'P11',
	            value: '',
	        }, {
	            label: 'P2',
	            value: '',
	            selected: true
	        }],
	    });
	    input = container.find(".wijmo-wijcombobox-input");
	    combobox.wijcombobox("search", "1", undefined);
	    input.focus();
	    itemsList = combobox.data("wijmo-wijcombobox")._menuUL.children("li");
	    combobox.simulate('keydown', { keyCode: $.ui.keyCode.DOWN });
	    result = itemsList.eq(1).hasClass("ui-state-hover")
            && itemsList.eq(1).attr("id") === "wijmo-wijlistitem-active";
	    ok(result, "KeyDown navigation works well ! ");

	    combobox.simulate('keydown', { keyCode: $.ui.keyCode.DOWN });
	    result = itemsList.eq(4).hasClass("ui-state-hover")
            && itemsList.eq(4).attr("id") === "wijmo-wijlistitem-active";
	    ok(result, "KeyDown navigation works well ! ");
	    combobox.remove();
	    container.remove();
	})

    test("#48380", function () {
		var container = $('<div id="Div48380"></div>').appendTo("body"),
			combobox = $('<input />').appendTo(container), list, trigger;
		combobox.wijcombobox({
			data: [{
				label: 'c++',
				value: 'c++1'
			}, {
				label: 'java',
				value: 'java'
			}, {
				label: 'php',
				value: 'php'
			}]
		});
		list = combobox.data("wijmo-wijcombobox")._menuUL;
		trigger = combobox.data("wijmo-wijcombobox")._triggerArrow;
		combobox.wijcombobox("option", "disabled", false);
		trigger.simulate("click");
		ok(list.is(":visible"), "list is visible after set enable once.");
		trigger.simulate("click");
		ok(!list.is(":visible"), "list is invisible after click trigger element.");
		combobox.wijcombobox("option", "disabled", false);
		trigger.simulate("click");
		ok(list.is(":visible"), "list is visible after set enable twice.");
		combobox.remove();
		container.remove();

	});

	test("#47349", function () {
		var combobox = $('<input />').appendTo("body")
			.wijcombobox({
				data: [{
					label: 'P1',
					value: 'P1'
				}, {
					label: 'P2',
					value: 'P2',
					selected: true
				}],
				changed: function (e, data) {
					changedEventTriggered = true;
				}
			}), changedEventTriggered = false;

		combobox.wijcombobox("option", { selectedValue: "P1" });

		setTimeout(function () {
			ok(changedEventTriggered, "Changing selectedValue option will trigger changed event.");
			start();
			combobox.remove();
		}, 100);
		stop();
	});

	test("#47944", function () {
	    var data = [{ label: 'test1', value: 1 }, { label: 'test2', value: 2 }],
            combobox1 = $('<input />').appendTo("body").wijcombobox({ data: data }),
			ele1 = combobox1.data("wijmo-wijcombobox")._comboElement,
			list1 = combobox1.data("wijmo-wijcombobox")._menuUL;
	    combobox1.wijcombobox("option", "selectedIndex", 5);
	    ele1.find(".ui-icon-triangle-1-s").simulate("click");
	    list1.find(".wijmo-wijlist-item-first").simulate("mouseover").simulate("click");
	    ok(combobox1.wijcombobox("option", "selectedIndex") === 0, "when select item, no error is throwed.");
	    combobox1.remove();
	});

	test("#49116", function () {
	    var container49116 = $('<div id="Div49116"></div>').appendTo("body"),
	    combobox1 = $('<input />').appendTo(container49116).wijcombobox({ "text": "123" }),
        input = container49116.find(".wijmo-wijcombobox-input");
	    input.one("focus", function () {
	        input.val("");
	        input.blur();
	    })
        input.one("blur", function () {
	        ok(combobox1.wijcombobox("option", "text") === "", "Option \"text\" has been changed to \"\" !");

	        combobox1.remove();
	        container49116.remove();
	        if ($.browser.msie) {
	            start();
	        }
	    })
	    input.focus();
	    if ($.browser.msie) {
	        stop();
	    }
	})

    test("#55810", function () {
		var container = $("<div></div>").appendTo("body"),
			indexChangedFiredBefore = false,
			indexChangedFiredAfter = false,
			blurEventsFired = false;
		combobox = $("<input />").appendTo(container).wijcombobox({
			data: getArrayDataSource(),
			delay: 0,
			selectedIndexChanged: function () {
				indexChangedFiredBefore = true;
			}
		}),
		triggerBtn = container.find(".wijmo-wijcombobox-trigger"),
		list = combobox.data("wijmo-wijcombobox")._menuUL;
		combobox.simulate("blur");
		triggerBtn.simulate("click");
		list.find(".wijmo-wijlist-item:eq(3)").simulate("click");
		combobox.wijcombobox("destroy");

		combobox.bind("wijcomboblur", function () {
			blurEventsFired = true;
		});

		combobox.trigger("blur");

		ok(blurEventsFired === false, "the blur events has removed when destroy the widget.");

		combobox.wijcombobox({
			data: getArrayDataSource(),
			delay: 0,
			selectedIndexChanged: function () {
				indexChangedFiredAfter = true;
			}
		});
		triggerBtn = container.find(".wijmo-wijcombobox-trigger");
            list = combobox.data("wijmo-wijcombobox")._menuUL;
		combobox.simulate("blur");
	    triggerBtn.simulate("click");
		list.find(".wijmo-wijlist-item:eq(4)").simulate("click");
		ok(indexChangedFiredAfter && indexChangedFiredBefore, "the selectedIndex event fired.");
		combobox.simulate("blur");
	    triggerBtn.simulate("click");
		setTimeout(function () {
			ok(list.is(":visible"), "the list is opened");
			container.remove();
			start();
		}, 200);
		stop();
	});

	test("#52251", function () {
	    var container = $('<div id="52251"></div>').appendTo("body"),
			combobox = $('<input />').appendTo(container);
	    combobox.wijcombobox({
	        data: [{
	            label: 'c++',
	            value: 'c++1'
            }, {
	            label: 'java',
	            value: 'java'
            }, {
	            label: 'php',
	            value: 'php'
	        }]
	    });
	    ok($(".wijmo-wijlist-item .wijmo-wijlist-item").length === 0, "The duplicate class has removed.");
	    combobox.remove();
	    container.remove();
	});

	test("#56801, #58069", function () {
	    var container56801 = $("<div id='Div56801'></div>").appendTo("body"),
            combobox = $("<div id='comb56801'>" +
            "<input name='comb56801_Input' type='text' id='comb56801_Input' />" +
            "<div id='comb56801_ListContainer'><ul id='comb56801_List'>" +
            "<li><div><input type='submit' name='comb56801$ctl00$Button1' value='Button' id='comb56801_Button1_0' /></div></li>" +
            "</ul></div>" +
            "</div>").appendTo(container56801);
	    combobox.wijcombobox({
	        "columns": [{
	            "name": "ProductName", "width": 100
	        }], "commandArgument": "", "commandName": "", "data": [{
	            "value": "System.Data.DataRowView", "cells": ["Chai"],
	            "label": "System.Data.DataRowView", "staticKey": null
	        }]
	    });
	    combobox.wijcombobox("search", "", undefined);
	    ok(!combobox.data("wijmo-wijcombobox")._menuUL.is(":visible"), "DropDow list should not display !")
	    ok(true, "");
	    combobox.remove();
	    container56801.remove();
	})

	test("#60163", function () {
	    var container = $('<div id="52251"></div>').appendTo("body"),
			combobox = $('<input />').appendTo(container);
	    combobox.wijcombobox({
	        data: []
	    });
	    combobox.wijcombobox("option", "data", []);
	    combobox.wijcombobox("option", "selectedIndex", 1);
	    
	    combobox.data("wijmo-wijcombobox")._change();
	    ok(true, "No exception is throwed.");
	    combobox.remove();
	    container.remove();
	})

	test("\"labelText\" element should not be repeated !", function () {
	    var container = $('<div id="conLTxt"></div>').appendTo("body"),
            combobox = $('<input />').appendTo(container), labelElement;
	    combobox.wijcombobox();
	    combobox.wijcombobox("option", "labelText", "abc");
	    combobox.wijcombobox("option", "labelText", "123");
	    labelElement = combobox.data("wijmo-wijcombobox")._input.parent().children(".wijmo-wijcombobox-label");
	    ok(labelElement.length === 1 && labelElement.html() === "123", "\"labelText\" element is not repeated !");

	    combobox.remove();
	    container.remove();
	})

	test("#63146", function () {
	    var container = $('<div id="expander"><h3>Header</h3>  <div class="content"></div></div>').appendTo("body"),
			combobox = $("<select id='combo' style='width:100%'>" +
				"<option value='c++'>c++</option>" +
				"<option value='Java'>Java</option>" +
				"<option value='php'>php</option>" +
				"<option value='coldfusion'>coldfusion</option>" +
				"<option value='csharp'>csharp</option>" +
				"<\select>").appendTo(".content"), firstWidth, secondWidth;

	    container.wijexpander();
	    combobox.wijcombobox();

	    firstWidth = container.find(".wijmo-wijcombobox").width();
	    $(".ui-expander-header").simulate("click");
	    $(".ui-expander-header").simulate("click");
	    window.setTimeout(function () {
	        secondWidth = container.find(".wijmo-wijcombobox").width();
	        ok(firstWidth === secondWidth, "The Secondary width is same as first!");
	        start();
	        combobox.remove();
	        container.remove();
	    }, 1000);
	    stop();
	})

	test("#66482", function () {
	    var data = getArrayDataSource(),
            container = $('<div id="con66482"></div>').appendTo("body"),
            combobox = $('<input />').appendTo(container),
            menuUL, listEle, wijSuperpanel, scrollBottomBtn,
            vScroller, max, largeChange;
	    combobox.wijcombobox({ data: data, dropdownHeight: 100 });
	    combobox.wijcombobox("search", "", undefined);
	    menuUL = combobox.data("wijmo-wijcombobox")._menuUL;
	    listEle = menuUL.parents(".wijmo-wijcombobox-list");
	    wijSuperpanel = listEle.data("wijmo-wijsuperpanel");

	    scrollBottomBtn = listEle.find(".wijmo-wijsuperpanel-vbar-buttonbottom").find("span");

	    scrollBottomBtn.simulate("mouseover");
	    scrollBottomBtn.simulate("mousedown");

	    setTimeout(function () {
	        scrollBottomBtn.simulate("mouseup");
	        setTimeout(function () {
	            menuUL.find();
	            vScroller = wijSuperpanel.options.vScroller;
	            max = vScroller.scrollMax;
	            largeChange = vScroller.scrollLargeChange;
	            ok(vScroller.scrollValue < max - largeChange, "Have not scrolled to the bottom");
	            combobox.remove();
	            container.remove();
	            start();
	        }, 1500);
	    }, 100);

	    stop();
	});

	test("#66954", function () {
	    var container = $("<div id='cont66954'></div>").appendTo("body"),
            combobox = $("<select id='combo66954'>" +
    			"<option value='Java'>Java</option>" +
    			"<option value='php'>php</option>" +
    			"<option selected='selected' value='coldfusion'>coldfusion</option>" +
    			"<option value='csharp'>csharp</option>" +
    			"<\select>").appendTo(container), lastItem;
	    combobox.wijcombobox({ "selectionMode": "multiple" });

	    combobox.wijcombobox("search", "", undefined);
	    lastItem = combobox.data("wijmo-wijcombobox")._menuUL.find("li").eq(3);

	    lastItem.simulate("mouseover");
	    lastItem.simulate("click");

	    combobox.wijcombobox("destroy");
	    ok($("#combo66954")[0].selectedIndex > 0, "<select> element does not select the first item after destroy wijcombobox");
	    combobox.remove();
	    container.remove();
	});

	test("#64599", function () {
	    var comboboxShown64599 = $("<select id='comboShown64599'>" +
    			"<option value='Java'>Java</option>" +
    			"<\select>").appendTo("body"),
            container64599 = $("<div id='cont64599' style='display: none'></div>").appendTo("body"),
            comboboxHidden64599 = $("<select id='comboHidden64599'>" +
    			"<option value='Java'>Java</option>" +
    			"<\select>").appendTo(container64599),
            hiddenElement,
	        comboShownData, comboHiddenData,
	        hiddenTriggerShown;
	    comboboxShown64599.wijcombobox();
	    comboboxHidden64599.wijcombobox();
	    hiddenElement = comboboxHidden64599.data("wijmo-wijcombobox").element;

	    comboShownData = comboboxShown64599.data("wijmo-wijcombobox");
	    comboHiddenData = comboboxHidden64599.data("wijmo-wijcombobox");

	    if (hiddenElement.wijTriggerVisibility) {
	        hiddenElement.wijTriggerVisibility();
	    }

	    container64599.show();

	    if (hiddenElement.wijTriggerVisibility) {
	        hiddenElement.wijTriggerVisibility();
	    }

	    hiddenTriggerShown = comboHiddenData._triggerArrow;
	    ok(hiddenTriggerShown, "Trigger element has created !");
	    if (hiddenTriggerShown) {
	        ok(comboShownData._comboElement.width() === comboHiddenData._comboElement.width(), "Combobox width renders correctly ! ");
	        ok(comboShownData._triggerArrow.width() === comboHiddenData._triggerArrow.width(), "Combobox trigger element width renders correctly ! ");
	        ok(comboShownData._input.width() === comboHiddenData._input.width(), "Combobox input element width renders correctly ! ");
	    }

	    comboboxShown64599.remove();
	    comboboxHidden64599.remove();
	    container64599.remove();
	})

	test("#68463", function () {
	    var container = $("<div id='cont68463'></div>").appendTo("body"),
            combobox = $("<select id='combo68463'>" +
    			"<option value='Java'>Java</option>" +
    			"<option value='php'>php</option>" +
    			"<option selected='selected' value='coldfusion'>coldfusion</option>" +
    			"<option value='csharp'>csharp</option>" +
    			"<\select>").appendTo(container), triggerArrow, menuUL;

	    combobox.wijcombobox({
            disabled: true
	    })
	    triggerArrow = combobox.data("wijmo-wijcombobox")._triggerArrow;
	    menuUL = combobox.data("wijmo-wijcombobox")._menuUL;
	    combobox.wijcombobox("option", "disabled", false);


	    triggerArrow.simulate("click");
	    ok(menuUL.is(":visible"), "The dropdown has shown !");

	    combobox.remove();
	    container.remove();
	});

	test("#68976", function () {
	    var container = $("<div id='cont68976'></div>").appendTo("body"),
            combobox = $("<select id='combo68976'>" +
    			"<option value='CENTC'>Centro comercial Moctezuma</option>" +
    			"<option value='MAGAA'>Magazzini Alimentari Riuniti</option>" +
    			"<option value='RATTC'>Rattlesnake Canyon Grocery</option>" +
    			"<option value='RATTC'>csharp</option>" +
                "<option value='Java'>Java</option>" +
    			"<option value='php'>php</option>" +
    			"<\select>").appendTo(container), triggerArrow, menuUL, menu;

	    combobox.wijcombobox({
	        dropdownWidth: 230,
	        dropdownHeight: 150,
	    });

	    triggerArrow = combobox.data("wijmo-wijcombobox")._triggerArrow;
	    menuUL = combobox.data("wijmo-wijcombobox")._menuUL;
	    menu = combobox.data("wijmo-wijcombobox").menu.element;

	    triggerArrow.simulate("click");
	    triggerArrow.simulate("click");

	    combobox.wijcombobox("search", "z", undefined);

	    ok(menuUL.outerHeight() === menu.innerHeight(), "Height of menu changed according to the ul element.")
	    combobox.remove();
	    container.remove();
	});

	test("#69896", function () {
		var container = $("<div id='cont69896'></div>").appendTo("body"),
            input = $('<input type="text" />').appendTo(container).wijcombobox({
            	ensureDropDownOnBody: false,
            	data: [{
            		label: 'c++',
            		value: 'c++',
            		selected: true
            	}, {
            		label: 'java',
            		value: 'java'
            	}, {
            		label: 'php',
            		value: 'php'
            	}, {
            		label: 'coldfusion',
            		value: 'coldfusion'
            	}]
            }),
			triggerBtn = container.find(".wijmo-wijcombobox-trigger"),
			dropDownList = container.find(".wijmo-wijcombobox-list .wijmo-wijlist-ul"),
			firstListItem = dropDownList.children("li:first");

		ok(firstListItem.hasClass("wijmo-wijcombobox-selecteditem"), "The first list item is selected.");
        input.focus();
		input.val("");
        input.simulateKeyStroke("[BACKSPACE][BACKSPACE][BACKSPACE]", 400, null, function () { // default search delay is 300, so we need 400
		triggerBtn.simulate("click");
		ok(!firstListItem.hasClass("wijmo-wijcombobox-selecteditem"), "The selected item class of first list item has been removed.");
		input.remove();
		container.remove();
            start();
	});
        stop();
    });

	test("#105900", function () {
		var container = $("<div id='cont105900'></div>").appendTo("body"),
			input = $('<input />').appendTo(container).wijcombobox({
				columns: [
					{ name: 'header1' },
					{ name: 'header2' },
					{ name: 'header3' }],
				data: [{
					label: 'label1',
					cells: [" ", "", "cell1"]
				}]
			});
		triggerBtn = container.find(".wijmo-wijcombobox-trigger");
		triggerBtn.simulate("click");
		ok($(".wijmo-wijcombobox-cell.ui-state-default").eq(0).height() !== 0);
		ok($(".wijmo-wijcombobox-cell.ui-state-default").eq(1).height() !== 0);
		input.remove();
		container.remove();
	});

	test("#70935", function () {
		var container = $("<div id='cont70935'></div>").appendTo("body"),
            input = $('<input />').appendTo(container).wijcombobox({
            	ensureDropDownOnBody: false,
            	data: [{
            		label: 'c++',
            		value: 'c++',
            		selected: true
            	}, {
            		label: 'java',
            		value: 'java'
            	}, {
            		label: 'php',
            		value: 'php'
            	}, {
            		label: 'coldfusion',
            		value: 'coldfusion'
            	}]
            });

		ok(input.val() === "c++", "The first list item is selected.");

		input.wijcombobox({ selectedIndex: null });
		ok(input.val() === "", "No item is selected after setting the selectedIndex to null.");

		input.wijcombobox({ selectedIndex: 1 });
		ok(input.val() === "java", "The second list item is selected.");

		input.wijcombobox({ selectedIndex: undefined });
		ok(input.val() === "", "No item is selected after setting the selectedIndex to undefined.");

		input.remove();
		container.remove();
	});

	test("#70396", function () {
	    var container = $("<div id='cont70396'></div>").appendTo("body"),
            combobox = $("<input />").appendTo(container).wijcombobox(),
            inputEle = combobox.data("wijmo-wijcombobox")._input,
            inputWrapper = inputEle.parent();

	    combobox.wijcombobox("option", "showTrigger", false);
	    ok(inputEle.outerWidth() === inputWrapper.innerWidth(), "Inner input element doesn't overlap the wrapper !");
	    combobox.remove();
	    container.remove();
	});
	
	test("#67696", function () {
	    var container = $("<div id='cont67696'></div>").appendTo("body"), openTimes = 0,
	        combobox67696 = $('<input />').appendTo(container).wijcombobox({
	            data: [{
	                label: 'java',
	                value: 'java'
	            }],
	            open: function (e) {
	                openTimes++;
	            }
	        }), triggerBtn, inputEle;
	    triggerBtn = container.find(".wijmo-wijcombobox-trigger");
	    inputEle = combobox67696.data("wijmo-wijcombobox")._input;

	    triggerBtn.simulate("click");
	    inputEle.val("j");
	    inputEle.simulate("keydown");

	    setTimeout(function () {
	        ok(openTimes === 1, "Open event triggered " + openTimes + " times");
	        combobox67696.remove();
	        container.remove();
	        start();
	    }, 400);

	    stop();
	});

	test("#67282", function () {
	    var container = $("<div id='cont67282'></div>").appendTo("body"),
            select = $("<select id='com67282'>" +
				"<option value='c++' selected='selected'>c++</option>" +
				"<option value='Java'>Java</option>" +
				"<\select>").appendTo(container),
            combobox = select.wijcombobox({
                changed: function () {
                    ok(select.val() === "Java", "the select element has updated !");
                    combobox.remove();
                    select.remove();
                    container.remove();
                }
            });
	    combobox.wijcombobox("option", "selectedIndex", 1);
	});


	test("#78741", function () {
	    var testArray = [{
	        language: 'c++'
	    }, {
	        language: 'java'
	    }, {
	        language: 'php'
	    }, {
	        language: 'coldfusion'
	    }, {
	        language: 'javascript'
	    }],
        container = $("<div/>").appendTo("body"),
		combobox1 = $('<input type="text"/>').appendTo(container).wijcombobox({
		    dataSource: null,
		    data: { label: { bind: "language" }, value: { bind: "language" } },
		    search: function (e, args) {
		        var searchString = "ja";
		        if (searchString != "") {
		            var searchedItems = $.grep(testArray, function (item) {
		                var itemVal = item.language;
		                return (itemVal.indexOf(searchString) >= 0);
		            });
		            combobox1.wijcombobox("option", "dataSource", searchedItems);
		        }
		    }
		}),
		list1 = combobox1.data("wijmo-wijcombobox")._menuUL;
	    combobox1.val("ja");
	    combobox1.simulateKeyStroke("ja", 1000, null, function () {
	        list1.find(".wijmo-wijlist-item-last").simulate("mouseover").simulate("click");
	        ok($(".wijmo-wijcombobox-input", container).val() == "javascript", "the item can be selected.");
	        combobox1.remove();
	        container.remove();
	        start();
	    });
	    stop();
	});

	test("#82241", function () {
	    var testArray = [{
	        language: 'c++'
	    }, {
	        language: 'javascript'
	    }],
        container = $("<div />").appendTo("body"),
		combobox1 = $('<input type="text"/>').appendTo(container).wijcombobox({
		    showingAnimation: null,
		    delay: 0,
		    dataSource: testArray,
		    data: { label: { bind: "language" }, value: { bind: "language" } }
		}), allVisible = true;
	    combobox1.val("ja");
	    combobox1.simulate('keydown', null);

	    setTimeout(function () {
	        combobox1.focus().select();

	        combobox1.simulate('keydown', { keyCode: 46 });
	        combobox1.val("");
	        // Simulate press and hold the DELETE key
	        combobox1.simulate('keydown', { keyCode: 46 });
	        combobox1.simulate('keydown', { keyCode: 46 });
            // End
	        combobox1.simulate('keyup', { keyCode: 46 });
	        setTimeout(function () {
	            $.each(combobox1.data("wijmo-wijcombobox").items, function (idx, item) {
	                if (item.element.is(":hidden")) {
	                    allVisible = false;
	                    return false;
	                }
	            });
	            ok(allVisible, "Items are all visible!");
	            combobox1.remove();
	            container.remove();
	            start();
	        }, 0);
	    }, 0);
	    stop();
	});

    test("#89426", function () {
        var changedFiredCnt = 0,
            comboBox = $('<input />').wijcombobox({
                data: getArrayDataSource(),
                selectedIndex: 1,
                changed: function () {
                    changedFiredCnt++;
                }
            });
        comboBox.wijcombobox('getComboElement').appendTo(document.body);
        stop();
        setTimeout(function () {
            comboBox.simulate('keydown', { keyCode: 8 });
            comboBox.simulate('keyup', { keyCode: 8 });
            comboBox.val("").keydown();
            setTimeout(function () {
                ok(changedFiredCnt > 0);
                start();
                comboBox.remove();
            }, 500);
        }, 500);
    });

    test("#88545", function () {
        var $widget = $("<input />").appendTo(document.body).wijcombobox({
            selectionMode: "multiple",
            data: [{
                label: "item1"
            }, {
                label: "item2"
            }, {
                label: "item3"
            }, {
                selected: true
            }]
        }),
            $containter = $widget.wijcombobox("getComboElement"),
            $trigger = $containter.find(".wijmo-wijcombobox-trigger"),
            $dropdown = $widget.data("wijmo-wijcombobox")._menuUL;

        $trigger.simulate("click");
        ok($dropdown.find("li").eq(3).hasClass("wijmo-wijcombobox-selecteditem"), "blank item is selected.");
        $widget.remove();
    });

    test("#94007", function () {
        var $widget = $("<input />").appendTo(document.body).wijcombobox({
            dropdownWidth: "200",
            data: [{
                label: "item1"
            }, {
                label: "item2"
            }, {
                label: "item3"
            }]
        }),
            $combo = $widget.wijcombobox("getComboElement"),
            $list = $widget.data("wijmo-wijcombobox").menu.element,
            $trigger = $combo.find(".wijmo-wijcombobox-trigger");

        $trigger.simulate("click");
        ok($list.outerWidth() === 200, "width is set by string.");

        $widget.wijcombobox("option", "dropdownWidth", "500");
        ok($list.outerWidth() === 500, "width is set by string.");
        $widget.remove();
    });

	test("#121045", function () {
		var $widget = $("<input />").appendTo(document.body).wijcombobox({
			isEditable: false,
			data: [
				{ label: '1', value: '1' },
				{ label: '2', value: '2' },
				{ label: '', value: '3' },
				{ label: '4', value: '4' }
			]
		}),
		$containter = $widget.wijcombobox("getComboElement"),
		$trigger = $containter.find(".wijmo-wijcombobox-trigger"),
		$dropdown = $widget.data("wijmo-wijcombobox")._menuUL;
		stop();
		setTimeout(function () {
			$trigger.simulate("click");
			setTimeout(function () {
				$dropdown.find("li").eq(2).simulate("click");
				$widget.simulate('keydown', { keyCode: 38 });
				$widget.simulate('keydown', { keyCode: 38 });
				setTimeout(function () {
					ok($dropdown.find("li").eq(2).hasClass("wijmo-wijlist-item-selected") === true,
						"Blank item(3rd item) should be selected. Actual CSS :" + $dropdown.find("li").eq(2).attr("class"));
					start();
					$widget.remove();
				}, 500);
			}, 500);
		}, 500);

	});
})(jQuery);
