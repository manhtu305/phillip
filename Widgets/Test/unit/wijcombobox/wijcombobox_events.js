/*
* wijcombobox_events.js
*/
(function($) {

    module("wijcombobox:events");
    
    test("changed", function() {
    	var changedNumber = 0;
        var ComboBox = $('<input />').wijcombobox(
    	{
    		changed: function(e, data) {
    			changedNumber = 1;
    	    },
    	    data: getArrayDataSource()
    	}
    	);
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');
        stop();
    	setTimeout(function (){
    		ComboBox.val("a").keydown();
    		
    		setTimeout(function (){
    			ComboBox.trigger("blur");
    			ok(changedNumber == 1, 'changed fired');
    			ComboBox.remove();
    			start();
    		},500);
    		
    	},500);
    });

    test("select event", function() {
        var ComboBox = $('<input />').wijcombobox(
		{
		    select: function(e, item) {
		        ok(item.label == 'c++' && item.list.selectedItem == item, 'item selected');
		        start();
		        ComboBox.remove();
		    },
		    data: getArrayDataSource()
		}
		);
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');
        $('.wijmo-wijcombobox-trigger').simulate('mouseover').simulate('click').simulate('mouseout');
        //		stop();
        $('.wijmo-wijcombobox-list li.wijmo-wijlist-item:first').simulate('mouseover').simulate('click');
    });

    test("select event2", function() {
        var ComboBox = $('<input />').wijcombobox(
		{
		    select: function(e, item) {
		        ok(item.label == 'c++' && item.list.selectedItem == item, 'item selected');
		        start();
		    },
		    data: getArrayDataSource()
		}
		);
        ComboBox.wijcombobox('getComboElement').appendTo(document.body);
        ComboBox.wijcombobox('repaint');
        //		stop();
        ComboBox.val('c');
        ComboBox.simulate('keydown', null);
        window.setTimeout(function() {
            ComboBox.simulate('keydown', { keyCode: $.ui.keyCode.ENTER });
            ComboBox.remove();
        }, 500);
    });
})(jQuery);
