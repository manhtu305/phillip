﻿
"use strict";
(function ($) {
	module("wijrating: tickets");

	test("increasing count with single rating mode", function () {
		var rating = createRating({ratingMode: 'single'}),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			stars = $(".wijmo-wijrating-normal", ratingElement[0]),
			star;

		star = $(stars[3]);
		star.simulate("click");
		rating.wijrating("option", { count: 6 })
		stars = $(".wijmo-wijrating-normal", ratingElement[0])
		ok(!$(stars[0]).hasClass("wijmo-wijrating-rated") && !$(stars[1]).hasClass("wijmo-wijrating-rated") &&
		!$(stars[2]).hasClass("wijmo-wijrating-rated") && $(stars[3]).hasClass("wijmo-wijrating-rated") &&
		!$(stars[4]).hasClass("wijmo-wijrating-rated") && !$(stars[4]).hasClass("wijmo-wijrating-rated"),
		"the forth star is still rated after adding the count of start");
		rating.remove();
	});

	test("decreasing count with single rating mode", function () {
		var rating = createRating({ ratingMode: 'single' }),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			stars = $(".wijmo-wijrating-normal", ratingElement[0]),
			star;

		star = $(stars[4]);
		star.simulate("click");
		rating.wijrating("option", { count: 4 })
		stars = $(".wijmo-wijrating-normal", ratingElement[0])
		ok(!$(stars[0]).hasClass("wijmo-wijrating-rated") && !$(stars[1]).hasClass("wijmo-wijrating-rated") &&
		!$(stars[2]).hasClass("wijmo-wijrating-rated") && $(stars[3]).hasClass("wijmo-wijrating-rated"),
		"the forth star is still rated after adding the count of start");
		rating.remove();
	});

	test("increasing split with single rating mode", function () {
		var rating = createRating({ ratingMode: 'single' }),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			stars = $(".wijmo-wijrating-normal", ratingElement[0]),
			star;

		star = $(stars[3]);
		star.simulate("click");
		rating.wijrating("option", { split: 2 });
		stars = $(".wijmo-wijrating-normal", ratingElement[0])
		ok(!$(stars[0]).hasClass("wijmo-wijrating-rated") && !$(stars[1]).hasClass("wijmo-wijrating-rated") &&
		!$(stars[2]).hasClass("wijmo-wijrating-rated") && !$(stars[3]).hasClass("wijmo-wijrating-rated") &&
		!$(stars[4]).hasClass("wijmo-wijrating-rated") && !$(stars[5]).hasClass("wijmo-wijrating-rated"),
		!$(stars[6]).hasClass("wijmo-wijrating-rated") && $(stars[7]).hasClass("wijmo-wijrating-rated") &&
		!$(stars[8]).hasClass("wijmo-wijrating-rated") && !$(stars[9]).hasClass("wijmo-wijrating-rated"),
		"the eighth star is still rated after adding the count of start");
		rating.remove();
	});

	test("decreasing split with single rating mode", function () {
		var rating = createRating({ ratingMode: "single", split: 2 }),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			stars = $(".wijmo-wijrating-normal", ratingElement[0]),
			star;

		star = $(stars[4]);
		star.simulate("click");
		rating.wijrating("option", { split: 1 });
		stars = $(".wijmo-wijrating-normal", ratingElement[0])
		ok(!$(stars[0]).hasClass("wijmo-wijrating-rated") && $(stars[1]).hasClass("wijmo-wijrating-rated") &&
		!$(stars[2]).hasClass("wijmo-wijrating-rated") && !$(stars[3]).hasClass("wijmo-wijrating-rated") &&
		!$(stars[4]).hasClass("wijmo-wijrating-rated"),
		"the forth star is still rated after adding the count of start");
		rating.remove();
	});

}(jQuery));