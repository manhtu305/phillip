﻿/*
* wijtree_events.js
*/
(function ($) {
	var el;
	module("wijrating: events");

	test("rating", function () {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			stars = $(".wijmo-wijrating-normal", ratingElement[0]),
			flag = 0,
			newVal = 0,
			oldVal = 0;
		rating.bind("wijratingrating", function (e, args) {
			flag++;
			return false;
		});
		$(stars[0]).simulate("click");
		ok(flag === 1, "ok to click star 1.");
		ok(rating.wijrating("option", "value") === 3, "ok to cancel this rating.");
		rating.remove();

		rating = createRating();
		ratingElement = rating.data("wijmo-wijrating").ratingElement;
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		rating.bind("wijratingrating", function (e, args) {
			flag++;
			newVal = args.newValue;
			oldVal = args.oldValue;
		});
		$(stars[0]).simulate("click");
		ok(flag === 2, "ok to click star 1.");
		ok(oldVal === 3, "ok to get old value in args.");
		ok(newVal === 1, "ok to get new value in args.");
		rating.remove();
	});

	test("rated", function () {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			stars = $(".wijmo-wijrating-normal", ratingElement[0]),
			flag = 0;
		rating.bind("wijratingrated", function (e, args) {
			flag++;
		});
		$(stars[0]).simulate("click");
		ok(flag === 1, "ok to click star 1.");
		ok(rating.wijrating("option", "value") === 1, "ok to set value to 1.");
		rating.remove();
	});

	test("reset", function () {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			restButton = $(".wijmo-wijrating-reset", ratingElement[0]),
			flag = 0;
		rating.bind("wijratingreset", function (e, args) {
			flag++;
		});
		$(restButton).simulate("click");
		ok(flag === 1, "ok to click reset button.");
		ok(rating.wijrating("option", "value") === 0, "ok to reset value to 0");
		rating.remove();
	});

	test("hover", function () {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			stars = $(".wijmo-wijrating-normal", ratingElement[0]),
			flag = 0;
		rating.bind("wijratinghover", function (e, args) {
			if (args.value < 3) {
				flag++;
			}
		});
		$(stars[0]).simulate("mouseover");
		ok(flag === 1, "ok to hover on star 1.");
		$(stars[4]).simulate("mouseover");
		ok(flag === 1, "ok to hover on star 4.");
		rating.remove();
	});
})(jQuery)