﻿/*globals jQuery, test, ok, module, createRating*/
/*
* wijrating_options.js
*/
"use strict";
(function ($) {
	module("wijrating: options");

	test("disabled", function() {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			star = $(".wijmo-wijrating-normal:nth(0)", ratingElement[0]);
		rating.wijrating("option", "value", 0);
		rating.wijrating("option", "disabled", true);
		star.simulate('mouseover'); 
		ok(!star.hasClass("wijmo-wijrating-hover"), "ok.when disabled, star doesn't have hover effect on hovering.");
		star.simulate('click');
		ok(!star.hasClass("wijmo-wijrating-rated"), "ok.when disabled, star doesn't have rated effect on click.");
		rating.wijrating("option", "disabled", false);
		star.simulate('mouseover'); 
		ok(star.hasClass("wijmo-wijrating-hover"), "ok.when enabled, star has hover effect on hovering.");
		star.simulate('click');
		ok(star.hasClass("wijmo-wijrating-rated"), "ok.when enabled, star has rated effect on click.");
		rating.remove();
	});

	test("count", function () {
		var rating = createRating(),
			ratingElement,
			stars;
		rating = rating.wijrating("option", "count", 10);
		ratingElement = rating.data("wijmo-wijrating").ratingElement;
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		ok(stars.length === 10, "ok.stars number is 10.");

		rating.remove();
	});

	test("split", function() {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			stars;
		rating.wijrating("option", "split", 2);
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		ok(stars.length === 10, "ok.stars are split into 10 sections when split is set to 2.");
		ok($(stars[5]).hasClass("wijmo-wijrating-rated") &&
			!$(stars[6]).hasClass("wijmo-wijrating-rated"), "ok.the 6th section star is rated and the 7th section star is not rated.");
		ok($(stars[0]).html() === "0.5", "ok.the 1st section star's value is 0.5.");
		rating.wijrating("option", "totalValue", 20);
		rating.wijrating("option", "split", 4);
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		ok(stars.length === 20, "ok.stars are split into 20 sections when split is set to 4.");
		ok(rating.wijrating("option", "value") === 12, "ok.rating's value is 12");
		ok($(stars[11]).hasClass("wijmo-wijrating-rated") &&
			!$(stars[12]).hasClass("wijmo-wijrating-rated"), "ok.the 12th section star is rated and the 13th section star is not rated.");
		ok($(stars[0]).html() === "1", "ok.the 1st section star's value is 1.");
		
		rating.remove();
	});

	test("totalValue", function() {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			firstStar = $(".wijmo-wijrating-normal:nth(0)", ratingElement[0]),
			secondStar = $(".wijmo-wijrating-normal:nth(1)", ratingElement[0]),
			thirdStar = $(".wijmo-wijrating-normal:nth(2)", ratingElement[0]),
			fourthStar = $(".wijmo-wijrating-normal:nth(3)", ratingElement[0]),
			fifthStar = $(".wijmo-wijrating-normal:nth(4)", ratingElement[0]);
		rating.wijrating("option", "totalValue", 10);
		ok(firstStar.html() === "2", "ok.first star's value is 2.");
		ok(secondStar.html() === "4", "ok.second star's value is 4.");
		ok(thirdStar.html() === "6", "ok.third star's value is 6.");
		ok(fourthStar.html() === "8", "ok.fourth star's value is 8.");
		ok(fifthStar.html() === "10", "ok.fifth star's value is 10.");
		ok(rating.wijrating("option", "value") === 6, "ok.rating's value is 6.");
		rating.wijrating("option", "hint", {content: ['new hint', null, '', 'new hint']});
		rating.wijrating("option", "totalValue", 20);
		ok(secondStar.attr("title") === "8", "ok.second star's hint is '8'.");
		ok(thirdStar.attr("title") === "12", "ok.third star's hint is '12'.");
		ok(fifthStar.attr("title") === "20", "ok.fifth star's hint is '20'.");

		rating.remove();
	});

	test("value", function() {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			firstChild = $(".wijmo-wijrating-normal:nth(0)", ratingElement[0]),
			secondChild = $(".wijmo-wijrating-normal:nth(1)", ratingElement[0]),
			thirdChild = $(".wijmo-wijrating-normal:nth(2)", ratingElement[0]);
		rating.wijrating("option", "value", 0);
		ok(!firstChild.hasClass("wijmo-wijrating-rated"), "set value 0 ok.");
		rating.wijrating("option", "value", 1);
		ok(firstChild.hasClass("wijmo-wijrating-rated") &&
			!secondChild.hasClass("wijmo-wijrating-rated"), "set value 1 ok.");
		rating.wijrating("option", "value", 2);
		ok(firstChild.hasClass("wijmo-wijrating-rated") &&
			secondChild.hasClass("wijmo-wijrating-rated") &&
			!thirdChild.hasClass("wijmo-wijrating-rated"), "set value 2 ok.");

		rating.remove();
	});

	test("min", function() {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			secondChild = $(".wijmo-wijrating-normal:nth(1)", ratingElement[0]),
			thirdChild = $(".wijmo-wijrating-normal:nth(2)", ratingElement[0]);
			rating.wijrating("option", "min", 3);
			rating.wijrating("option", "value", 2);
			ok(secondChild.hasClass("wijmo-wijrating-rated") &&
				thirdChild.hasClass("wijmo-wijrating-rated"), "ok.can't set value to 2 when min is 3.");
			rating.wijrating("option", "min", 2);
			rating.wijrating("option", "value", 2);
			ok(secondChild.hasClass("wijmo-wijrating-rated") &&
				!thirdChild.hasClass("wijmo-wijrating-rated"), "ok.can set value to 2 when min is 2.");

		rating.remove();
	});

	test("max", function() {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			thirdChild = $(".wijmo-wijrating-normal:nth(2)", ratingElement[0]),
			fourthChild = $(".wijmo-wijrating-normal:nth(3)", ratingElement[0]);
			rating.wijrating("option", "max", 3);
			rating.wijrating("option", "value", 4);
			ok(thirdChild.hasClass("wijmo-wijrating-rated") &&
				!fourthChild.hasClass("wijmo-wijrating-rated"), "ok.can't set value to 4 when max is 3.");
			rating.wijrating("option", "max", 4);
			rating.wijrating("option", "value", 4);
			ok(thirdChild.hasClass("wijmo-wijrating-rated") &&
				fourthChild.hasClass("wijmo-wijrating-rated"), "ok.can set value to 4 when max is 4.");

		rating.remove();
	});

	test("resetButton", function() {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			resetButton = $(".wijmo-wijrating-reset", ratingElement[0]);

		rating.wijrating("option", "resetButton", {hint: 'new hint'});
		ok(resetButton.attr("title") === 'new hint', "ok to set reset button's hint.");
		rating.wijrating("option", "resetButton", {position: 'rightOrBottom'});
		ok(resetButton.is(":last-child"), "ok to set reset button's position to 'rightOrBottom'");
		rating.wijrating("option", "resetButton", {position: 'leftOrTop'});
		ok(resetButton.is(":first-child"), "ok to set reset button's position to 'leftOrTop'");
		rating.wijrating("option", "resetButton", {disabled: true});
		rating.wijrating("option", "disabled", true);
		ok(resetButton.is(":hidden"), "ok.reset button is hidden when rating is disabled and reset button is disabled");
		rating.wijrating("option", "resetButton", {disabled: false});
		rating.wijrating("option", "disabled", true);
		ok(resetButton.is(":hidden"), "ok.reset button is hidden when rating is disabled and reset button is enabled");
		rating.wijrating("option", "resetButton", {disabled: true});
		rating.wijrating("option", "disabled", false);
		ok(resetButton.is(":hidden"), "ok.reset button is hidden when rating is enabled and reset button is disabled");
		rating.wijrating("option", "resetButton", {disabled: false});
		rating.wijrating("option", "disabled", false);
		ok(resetButton.is(":visible"), "ok.reset button is visible when rating is enabled and reset button is enabled");
		
		rating.wijrating("option", "resetButton", {customizedClass: 'newclass', customizedHoverClass: 'newhoverclass'});
		ok(resetButton.hasClass('newclass'), "ok to set reset button's customized class.");

		resetButton.simulate("mouseover");
		ok(resetButton.hasClass('newhoverclass'), "ok to set reset button's customized hover class when mouseover.");
		resetButton.simulate("mouseout");
		ok(!resetButton.hasClass('newhoverclass'), "ok to set reset button's customized hover class when mouseout.");

		rating.remove();
	});

	test("hint", function() {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			firstStar = $(".wijmo-wijrating-normal:nth(0)", ratingElement[0]),
			secondStar = $(".wijmo-wijrating-normal:nth(1)", ratingElement[0]),
			thirdStar = $(".wijmo-wijrating-normal:nth(2)", ratingElement[0]),
			fourthStar = $(".wijmo-wijrating-normal:nth(3)", ratingElement[0]),
			fifthStar = $(".wijmo-wijrating-normal:nth(4)", ratingElement[0]);
		rating.wijrating("option", "hint", {disabled: true});
		ok(typeof firstStar.attr("title") === 'undefined', "ok to disable hint.");
		rating.wijrating("option", "hint", {disabled: false});
		ok(firstStar.attr("title").length > 0, "ok to enable hint.");
		rating.wijrating("option", "hint", {content: ['new hint', null, '', 'new hint']});
		ok(firstStar.attr("title") === "new hint", "ok.first star's hint is 'new hint'.");
		ok(secondStar.attr("title") === "2", "ok.second star's hint is '2'.");
		ok(thirdStar.attr("title") === "3", "ok.third star's hint is '3'.");
		ok(fourthStar.attr("title") === "new hint", "ok.fourth star's hint is 'new hint'.");
		ok(fifthStar.attr("title") === "5", "ok.fifth star's hint is 'new hint'.");
		rating.wijrating("option", "hint", {content: null});
		ok(firstStar.attr("title") === "1", "ok.first star's hint is '1'.");
		ok(secondStar.attr("title") === "2", "ok.second star's hint is '2'.");
		ok(thirdStar.attr("title") === "3", "ok.third star's hint is '3'.");
		ok(fourthStar.attr("title") === "4", "ok.fourth star's hint is '4'.");
		ok(fifthStar.attr("title") === "5", "ok.fifth star's hint is '5'.");

		rating.remove();
	});

	test("orientation", function() {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement;
		rating.wijrating("option", "orientation", "vertical");
		ok(ratingElement.hasClass("wijmo-wijrating-vertical"), "vertical class added.");

		rating.wijrating("option", "orientation", "horizontal");
		ok(!ratingElement.hasClass("wijmo-wijrating-vertical"), "vertical class removed.");

		rating.remove();
	});

	test("direction", function() {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			starContainer = $(".wijmo-wijrating-starcontainer", ratingElement[0]);
		rating.wijrating("option", "direction", "reversed");
		ok(!starContainer.children().first().children().hasClass("wijmo-wijrating-rated") &&
			starContainer.children().last().children().hasClass("wijmo-wijrating-rated"), "direction reversed ok.");
		rating.wijrating("option", "direction", "normal");
		ok(starContainer.children().first().children().hasClass("wijmo-wijrating-rated") &&
			!starContainer.children().last().children().hasClass("wijmo-wijrating-rated"), "direction normal ok.");
		
		rating.remove();
	});

	test("ratingMode", function() {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			firstChild = $(".wijmo-wijrating-normal:nth(0)", ratingElement[0]),
			thirdChild = $(".wijmo-wijrating-normal:nth(2)", ratingElement[0]);
		rating.wijrating("option", "ratingMode", "single");
		ok(!firstChild.hasClass("wijmo-wijrating-rated") &&
			thirdChild.hasClass("wijmo-wijrating-rated"), "rating mode single ok.");
		rating.wijrating("option", "ratingMode", "continuous");
		ok(firstChild.hasClass("wijmo-wijrating-rated") &&
			thirdChild.hasClass("wijmo-wijrating-rated"), "rating mode continuous ok.");

		rating.remove();
	});

	test("icons.iconsClass", function() {
		var rating = createRating({icons: {iconsClass: "customizedClass"}}),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		ok($(stars[0]).hasClass("customizedClass") && $(stars[1]).hasClass("customizedClass") &&
			$(stars[2]).hasClass("customizedClass") && $(stars[3]).hasClass("customizedClass") &&
			$(stars[4]).hasClass("customizedClass"),"ok to set a string value to iconsClass on init.");

		rating.remove();
		rating = createRating({icons: {iconsClass: ['icon1', 'icon2', 'icon3', 'icon4', 'icon5']}});
		ratingElement = rating.data("wijmo-wijrating").ratingElement;
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		ok($(stars[0]).hasClass("icon1") && $(stars[1]).hasClass("icon2") &&
			$(stars[2]).hasClass("icon3") && $(stars[3]).hasClass("icon4") &&
			$(stars[4]).hasClass("icon5"),"ok to set an array value to iconsClass on init.");

		rating.wijrating("option", "icons", {iconsClass: "customizedClass"});
		ok($(stars[0]).hasClass("customizedClass") && $(stars[1]).hasClass("customizedClass") &&
			$(stars[2]).hasClass("customizedClass") && $(stars[3]).hasClass("customizedClass") &&
			$(stars[4]).hasClass("customizedClass"),"ok to set a string value to iconsClass by setting option.");

		rating.wijrating("option", "icons", {iconsClass: ['icon1', 'icon2', 'icon3', 'icon4', 'icon5']});
		ok($(stars[0]).hasClass("icon1") && $(stars[1]).hasClass("icon2") &&
			$(stars[2]).hasClass("icon3") && $(stars[3]).hasClass("icon4") &&
			$(stars[4]).hasClass("icon5"),"ok to set an array value to iconsClass by setting option.");
			
		//add direction=reversed test for tfs issue 21105
		rating.wijrating("option", "direction", "reversed");
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		ok($(stars[0]).hasClass("icon5") && $(stars[1]).hasClass("icon4") &&
			$(stars[2]).hasClass("icon3") && $(stars[3]).hasClass("icon2") &&
			$(stars[4]).hasClass("icon1"),"ok to set direction when customizing icon.");
			
		rating.wijrating("option", "direction", "normal");
		rating.wijrating("option", "split", 2);
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		ok($(stars[0]).hasClass("icon1") && $(stars[1]).hasClass("icon1") &&
			$(stars[2]).hasClass("icon2") && $(stars[3]).hasClass("icon2") &&
			$(stars[4]).hasClass("icon3") && $(stars[5]).hasClass("icon3") &&
			$(stars[6]).hasClass("icon4") && $(stars[7]).hasClass("icon4") &&
			$(stars[8]).hasClass("icon5") && $(stars[9]).hasClass("icon5"),
			"ok to set an array value to iconsClass by setting option.");
		
		//add direction=reversed test for tfs issue 21105
		rating.wijrating("option", "direction", "reversed");
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		ok($(stars[0]).hasClass("icon5") && $(stars[1]).hasClass("icon5") &&
			$(stars[2]).hasClass("icon4") && $(stars[3]).hasClass("icon4") &&
			$(stars[4]).hasClass("icon3") && $(stars[5]).hasClass("icon3") &&
			$(stars[6]).hasClass("icon2") && $(stars[7]).hasClass("icon2") &&
			$(stars[8]).hasClass("icon1") && $(stars[9]).hasClass("icon1"),
			"ok to set direction when customizing icon.");

		rating.remove();
	});

	test("icons.hoverIconsClass", function() {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			stars = $(".wijmo-wijrating-normal", ratingElement[0]),
			star;
		rating.wijrating("option", "icons", {hoverIconsClass: "customizedClass"});
		star = $(stars[0]);
		star.simulate('mouseover'); 
		ok($(stars[0]).hasClass("customizedClass") && !$(stars[1]).hasClass("customizedClass") &&
			!$(stars[2]).hasClass("customizedClass") && !$(stars[3]).hasClass("customizedClass") &&
			!$(stars[4]).hasClass("customizedClass"),
			"ok to mouseover first star when hoverIconsClass is string.");
		star.simulate('mouseout'); 
		ok(!$(stars[0]).hasClass("customizedClass") && !$(stars[1]).hasClass("customizedClass") &&
			!$(stars[2]).hasClass("customizedClass") && !$(stars[3]).hasClass("customizedClass") &&
			!$(stars[4]).hasClass("customizedClass"),
			"ok to mouseout first star when hoverIconsClass is string.");
			
		rating.wijrating("option", "direction", "reversed");
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		star = $(stars[0]);
		star.simulate('mouseover'); 
		ok($(stars[0]).hasClass("customizedClass") && $(stars[1]).hasClass("customizedClass") &&
			$(stars[2]).hasClass("customizedClass") && $(stars[3]).hasClass("customizedClass") &&
			$(stars[4]).hasClass("customizedClass"),
			"ok to mouseover first star when reversed and hoverIconsClass is string.");
		star.simulate('mouseout'); 
		ok(!$(stars[0]).hasClass("customizedClass") && !$(stars[1]).hasClass("customizedClass") &&
			!$(stars[2]).hasClass("customizedClass") && !$(stars[3]).hasClass("customizedClass") &&
			!$(stars[4]).hasClass("customizedClass"),
			"ok to mouseout first star when reversed and  hoverIconsClass is string.");
		
		rating.wijrating("option", "ratingMode", "single");
		star = $(stars[2]);
		star.simulate('mouseover'); 
		ok(!$(stars[0]).hasClass("customizedClass") && !$(stars[1]).hasClass("customizedClass") &&
			$(stars[2]).hasClass("customizedClass") && !$(stars[3]).hasClass("customizedClass") &&
			!$(stars[4]).hasClass("customizedClass"),
			"ok to mouseover 3rd star when single and hoverIconsClass is string.");

		//set an array to hoverIconsClass.
		rating.wijrating("option", "direction", "normal");
		rating.wijrating("option", "ratingMode", "continuous");
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		rating.wijrating("option", "icons", {hoverIconsClass: ['icon1', 'icon2', 'icon3', 'icon4', 'icon5']});
		star = $(stars[0]);
		star.simulate('mouseover'); 
		ok($(stars[0]).hasClass("icon1") && !$(stars[1]).hasClass("icon2") &&
			!$(stars[2]).hasClass("icon3") && !$(stars[3]).hasClass("icon4") &&
			!$(stars[4]).hasClass("icon5"),
			"ok to mouseover first star when hoverIconsClass is array.");
		star.simulate('mouseout'); 
		ok(!$(stars[0]).hasClass("icon1") && !$(stars[1]).hasClass("icon2") &&
			!$(stars[2]).hasClass("icon3") && !$(stars[3]).hasClass("icon4") &&
			!$(stars[4]).hasClass("icon5"),
			"ok to mouseout first star when hoverIconsClass is array.");
			
		rating.wijrating("option", "direction", "reversed");
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		star = $(stars[0]);
		star.simulate('mouseover'); 
		ok($(stars[4]).hasClass("icon1") && $(stars[3]).hasClass("icon2") &&
			$(stars[2]).hasClass("icon3") && $(stars[1]).hasClass("icon4") &&
			$(stars[0]).hasClass("icon5"),
			"ok to mouseover first star when reversed and hoverIconsClass is array.");
		star.simulate('mouseout'); 
		ok(!$(stars[4]).hasClass("icon1") && !$(stars[3]).hasClass("icon2") &&
			!$(stars[2]).hasClass("icon3") && !$(stars[1]).hasClass("icon4") &&
			!$(stars[0]).hasClass("icon5"),
			"ok to mouseout first star when reversed and hoverIconsClass is array.");
//		
		rating.wijrating("option", "ratingMode", "single");
		star = $(stars[2]);
		star.simulate('mouseover'); 
		ok(!$(stars[4]).hasClass("icon1") && !$(stars[3]).hasClass("icon2") &&
			$(stars[2]).hasClass("icon3") && !$(stars[1]).hasClass("icon4") &&
			!$(stars[0]).hasClass("icon5"),
			"ok to mouseover 3rd star when single and hoverIconsClass is array.");
		star.simulate('mouseout'); 
		ok(!$(stars[4]).hasClass("icon1") && !$(stars[3]).hasClass("icon2") &&
			!$(stars[2]).hasClass("icon3") && !$(stars[1]).hasClass("icon4") &&
			!$(stars[0]).hasClass("icon5"),
			"ok to mouseout 3rd star when single and hoverIconsClass is array.");
			
		rating.wijrating("option", "direction", "normal");
		rating.wijrating("option", "ratingMode", "continuous");
		rating.wijrating("option", "split", 2);
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		star = $(stars[0]);
		star.simulate('mouseover'); 
		ok($(stars[0]).hasClass("icon1") && !$(stars[1]).hasClass("icon1") &&
			!$(stars[2]).hasClass("icon2") && !$(stars[3]).hasClass("icon2") &&
			!$(stars[4]).hasClass("icon3") && !$(stars[5]).hasClass("icon3") &&
			!$(stars[6]).hasClass("icon4") && !$(stars[7]).hasClass("icon4") &&
			!$(stars[8]).hasClass("icon5") && !$(stars[9]).hasClass("icon5"),
			"ok to mouseover first star when split is 2 and hoverIconsClass is array.");
		star.simulate('mouseout'); 
		ok(!$(stars[0]).hasClass("icon1") && !$(stars[1]).hasClass("icon1") &&
			!$(stars[2]).hasClass("icon2") && !$(stars[3]).hasClass("icon2") &&
			!$(stars[4]).hasClass("icon3") && !$(stars[5]).hasClass("icon3") &&
			!$(stars[6]).hasClass("icon4") && !$(stars[7]).hasClass("icon4") &&
			!$(stars[8]).hasClass("icon5") && !$(stars[9]).hasClass("icon5"),
			"ok to mouseout first star when split is 2 and hoverIconsClass is array.");
			
		rating.wijrating("option", "direction", "reversed");
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		star = $(stars[0]);
		star.simulate('mouseover'); 
		ok($(stars[9]).hasClass("icon1") && $(stars[8]).hasClass("icon1") &&
			$(stars[7]).hasClass("icon2") && $(stars[6]).hasClass("icon2") &&
			$(stars[5]).hasClass("icon3") && $(stars[4]).hasClass("icon3") &&
			$(stars[3]).hasClass("icon4") && $(stars[2]).hasClass("icon4") &&
			$(stars[1]).hasClass("icon5") && $(stars[0]).hasClass("icon5"),
			"ok to mouseover first star when split is 2 and direction reversed and hoverIconsClass is array.");
		star.simulate('mouseout'); 
		ok(!$(stars[9]).hasClass("icon1") && !$(stars[8]).hasClass("icon1") &&
			!$(stars[7]).hasClass("icon2") && !$(stars[6]).hasClass("icon2") &&
			!$(stars[5]).hasClass("icon3") && !$(stars[4]).hasClass("icon3") &&
			!$(stars[3]).hasClass("icon4") && !$(stars[2]).hasClass("icon4") &&
			!$(stars[1]).hasClass("icon5") && !$(stars[0]).hasClass("icon5"),
			"ok to mouseout first star when split is 2 and direction reversed and hoverIconsClass is array.");
		
		rating.wijrating("option", "ratingMode", "single");
		star.simulate('mouseover'); 
		ok(!$(stars[9]).hasClass("icon1") && !$(stars[8]).hasClass("icon1") &&
			!$(stars[7]).hasClass("icon2") && !$(stars[6]).hasClass("icon2") &&
			!$(stars[5]).hasClass("icon3") && !$(stars[4]).hasClass("icon3") &&
			!$(stars[3]).hasClass("icon4") && !$(stars[2]).hasClass("icon4") &&
			!$(stars[1]).hasClass("icon5") && $(stars[0]).hasClass("icon5"),
			"ok to mouseover first star when split is 2 and direction reversed and ratingmode single and hoverIconsClass is array.");
		star.simulate('mouseout'); 
		ok(!$(stars[9]).hasClass("icon1") && !$(stars[8]).hasClass("icon1") &&
			!$(stars[7]).hasClass("icon2") && !$(stars[6]).hasClass("icon2") &&
			!$(stars[5]).hasClass("icon3") && !$(stars[4]).hasClass("icon3") &&
			!$(stars[3]).hasClass("icon4") && !$(stars[2]).hasClass("icon4") &&
			!$(stars[1]).hasClass("icon5") && !$(stars[0]).hasClass("icon5"),
			"ok to mouseout first star when split is 2 and direction reversed and ratingmode single and hoverIconsClass is array.");
		
		rating.wijrating("option", "direction", "normal");
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		star = $(stars[0]);
		star.simulate('mouseover');
		ok($(stars[0]).hasClass("icon1") && !$(stars[1]).hasClass("icon1") &&
			!$(stars[2]).hasClass("icon2") && !$(stars[3]).hasClass("icon2") &&
			!$(stars[4]).hasClass("icon3") && !$(stars[5]).hasClass("icon3") &&
			!$(stars[6]).hasClass("icon4") && !$(stars[7]).hasClass("icon4") &&
			!$(stars[8]).hasClass("icon5") && !$(stars[9]).hasClass("icon5"),
			"ok to mouseover first star when split is 2 and ratingmode single and hoverIconsClass is array.");
		star.simulate('mouseout'); 
		ok(!$(stars[0]).hasClass("icon1") && !$(stars[1]).hasClass("icon1") &&
			!$(stars[2]).hasClass("icon2") && !$(stars[3]).hasClass("icon2") &&
			!$(stars[4]).hasClass("icon3") && !$(stars[5]).hasClass("icon3") &&
			!$(stars[6]).hasClass("icon4") && !$(stars[7]).hasClass("icon4") &&
			!$(stars[8]).hasClass("icon5") && !$(stars[9]).hasClass("icon5"),
			"ok to mouseout first star when split is 2 and ratingmode single and hoverIconsClass is array.");

		rating.remove();
	});

	test("icons.ratedIconsClass", function() {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			stars = $(".wijmo-wijrating-normal", ratingElement[0]),
			resetButton = $(".wijmo-wijrating-reset", ratingElement[0]),
			star;
		rating.wijrating("option", "icons", {ratedIconsClass: "customizedClass"});
		ok($(stars[0]).hasClass("customizedClass") && $(stars[1]).hasClass("customizedClass") &&
			$(stars[2]).hasClass("customizedClass") && !$(stars[3]).hasClass("customizedClass") &&
			!$(stars[4]).hasClass("customizedClass"),
			"ok to set ratedIconsClass as a string.");
		star = $(stars[0]);
		star.simulate('click');
		ok($(stars[0]).hasClass("customizedClass") && !$(stars[1]).hasClass("customizedClass") &&
			!$(stars[2]).hasClass("customizedClass") && !$(stars[3]).hasClass("customizedClass") &&
			!$(stars[4]).hasClass("customizedClass"),
			"ok to click first star when ratedIconsClass is string.");
		resetButton.simulate('click'); 
		ok(!$(stars[0]).hasClass("customizedClass") && !$(stars[1]).hasClass("customizedClass") &&
			!$(stars[2]).hasClass("customizedClass") && !$(stars[3]).hasClass("customizedClass") &&
			!$(stars[4]).hasClass("customizedClass"),
			"ok to click resetButton when ratedIconsClass is string.");
			
		rating.wijrating("option", "direction", "reversed");
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		star = $(stars[0]);
		star.simulate('click'); 
		ok($(stars[0]).hasClass("customizedClass") && $(stars[1]).hasClass("customizedClass") &&
			$(stars[2]).hasClass("customizedClass") && $(stars[3]).hasClass("customizedClass") &&
			$(stars[4]).hasClass("customizedClass"),
			"ok to click first star when reversed and ratedIconsClass is string.");
		resetButton.simulate('click'); 
		ok(!$(stars[0]).hasClass("customizedClass") && !$(stars[1]).hasClass("customizedClass") &&
			!$(stars[2]).hasClass("customizedClass") && !$(stars[3]).hasClass("customizedClass") &&
			!$(stars[4]).hasClass("customizedClass"),
			"ok to click reset button when reversed and  ratedIconsClass is string.");
		
		rating.wijrating("option", "ratingMode", "single");
		star = $(stars[2]);
		star.simulate('click'); 
		ok(!$(stars[0]).hasClass("customizedClass") && !$(stars[1]).hasClass("customizedClass") &&
			$(stars[2]).hasClass("customizedClass") && !$(stars[3]).hasClass("customizedClass") &&
			!$(stars[4]).hasClass("customizedClass"),
			"ok to click 3rd star when single and ratedIconsClass is string.");
			

		//set an array to ratedIconsClass.
		rating.wijrating("option", "direction", "normal");
		rating.wijrating("option", "ratingMode", "continuous");
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		rating.wijrating("option", "icons", {ratedIconsClass: ['icon1', 'icon2', 'icon3', 'icon4', 'icon5']});
		ok($(stars[0]).hasClass("icon1") && $(stars[1]).hasClass("icon2") &&
			$(stars[2]).hasClass("icon3") && !$(stars[3]).hasClass("icon4") &&
			!$(stars[4]).hasClass("icon5"),
			"ok to set ratedIconsClass as an array.");
		rating.wijrating("option", "split", 2);
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		ok($(stars[0]).hasClass("icon1") && $(stars[1]).hasClass("icon1") &&
			$(stars[2]).hasClass("icon2") && $(stars[3]).hasClass("icon2") &&
			$(stars[4]).hasClass("icon3") && $(stars[5]).hasClass("icon3") &&
			!$(stars[6]).hasClass("icon4") && !$(stars[7]).hasClass("icon4") &&
			!$(stars[8]).hasClass("icon5") && !$(stars[9]).hasClass("icon5") ,
			"ok to set ratedIconsClass as an array and split is 2.");
		rating.wijrating("option", "value", 5);
		ok($(stars[0]).hasClass("icon1") && $(stars[1]).hasClass("icon1") &&
			$(stars[2]).hasClass("icon2") && $(stars[3]).hasClass("icon2") &&
			$(stars[4]).hasClass("icon3") && $(stars[5]).hasClass("icon3") &&
			$(stars[6]).hasClass("icon4") && $(stars[7]).hasClass("icon4") &&
			$(stars[8]).hasClass("icon5") && $(stars[9]).hasClass("icon5") ,
			"ok to set ratedIconsClass as an array and split is 2 and value is 5.");
		rating.remove();

		rating = createRating();
		rating.wijrating("option", "icons", {ratedIconsClass: ['icon1', 'icon2', 'icon3', 'icon4', 'icon5']});
		ratingElement = rating.data("wijmo-wijrating").ratingElement;
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		resetButton = $(".wijmo-wijrating-reset", ratingElement[0]);
		star = $(stars[0]);
		star.simulate('click'); 
		ok($(stars[0]).hasClass("icon1") && !$(stars[1]).hasClass("icon2") &&
			!$(stars[2]).hasClass("icon3") && !$(stars[3]).hasClass("icon4") &&
			!$(stars[4]).hasClass("icon5"),
			"ok to click first star when ratedIconsClass is array.");
		resetButton.simulate('click'); 
		ok(!$(stars[0]).hasClass("icon1") && !$(stars[1]).hasClass("icon2") &&
			!$(stars[2]).hasClass("icon3") && !$(stars[3]).hasClass("icon4") &&
			!$(stars[4]).hasClass("icon5"),
			"ok to click reset button when ratedIconsClass is array.");
			
		rating.wijrating("option", "direction", "reversed");
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		star = $(stars[0]);
		star.simulate('click'); 
		ok($(stars[4]).hasClass("icon1") && $(stars[3]).hasClass("icon2") &&
			$(stars[2]).hasClass("icon3") && $(stars[1]).hasClass("icon4") &&
			$(stars[0]).hasClass("icon5"),
			"ok to click first star when reversed and ratedIconsClass is array.");
		resetButton.simulate('click'); 
		ok(!$(stars[4]).hasClass("icon1") && !$(stars[3]).hasClass("icon2") &&
			!$(stars[2]).hasClass("icon3") && !$(stars[1]).hasClass("icon4") &&
			!$(stars[0]).hasClass("icon5"),
			"ok to click reset button when reversed and ratedIconsClass is array.");
		
		rating.wijrating("option", "ratingMode", "single");
		star = $(stars[2]);
		star.simulate('click'); 
		ok(!$(stars[4]).hasClass("icon1") && !$(stars[3]).hasClass("icon2") &&
			$(stars[2]).hasClass("icon3") && !$(stars[1]).hasClass("icon4") &&
			!$(stars[0]).hasClass("icon5"),
			"ok to click 3rd star when single and ratedIconsClass is array.");
		resetButton.simulate('click'); 
		ok(!$(stars[4]).hasClass("icon1") && !$(stars[3]).hasClass("icon2") &&
			!$(stars[2]).hasClass("icon3") && !$(stars[1]).hasClass("icon4") &&
			!$(stars[0]).hasClass("icon5"),
			"ok to click reset button when single and ratedIconsClass is array.");
			
		rating.wijrating("option", "direction", "normal");
		rating.wijrating("option", "ratingMode", "continuous");
		rating.wijrating("option", "split", 2);
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		star = $(stars[0]);
		star.simulate('click'); 
		ok($(stars[0]).hasClass("icon1") && !$(stars[1]).hasClass("icon1") &&
			!$(stars[2]).hasClass("icon2") && !$(stars[3]).hasClass("icon2") &&
			!$(stars[4]).hasClass("icon3") && !$(stars[5]).hasClass("icon3") &&
			!$(stars[6]).hasClass("icon4") && !$(stars[7]).hasClass("icon4") &&
			!$(stars[8]).hasClass("icon5") && !$(stars[9]).hasClass("icon5"),
			"ok to click first star when split is 2 and ratedIconsClass is array.");
		star = stars.last();
		star.simulate('click'); 
		ok($(stars[0]).hasClass("icon1") && $(stars[1]).hasClass("icon1") &&
			$(stars[2]).hasClass("icon2") && $(stars[3]).hasClass("icon2") &&
			$(stars[4]).hasClass("icon3") && $(stars[5]).hasClass("icon3") &&
			$(stars[6]).hasClass("icon4") && $(stars[7]).hasClass("icon4") &&
			$(stars[8]).hasClass("icon5") && $(stars[9]).hasClass("icon5"),
			"ok to click last star when split is 2 and ratedIconsClass is array.");
		resetButton.simulate('click'); 
		ok(!$(stars[0]).hasClass("icon1") && !$(stars[1]).hasClass("icon1") &&
			!$(stars[2]).hasClass("icon2") && !$(stars[3]).hasClass("icon2") &&
			!$(stars[4]).hasClass("icon3") && !$(stars[5]).hasClass("icon3") &&
			!$(stars[6]).hasClass("icon4") && !$(stars[7]).hasClass("icon4") &&
			!$(stars[8]).hasClass("icon5") && !$(stars[9]).hasClass("icon5"),
			"ok to click rest button when split is 2 and ratedIconsClass is array.");
			
		rating.wijrating("option", "direction", "reversed");
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		star = $(stars[0]);
		star.simulate('click'); 
		ok($(stars[9]).hasClass("icon1") && $(stars[8]).hasClass("icon1") &&
			$(stars[7]).hasClass("icon2") && $(stars[6]).hasClass("icon2") &&
			$(stars[5]).hasClass("icon3") && $(stars[4]).hasClass("icon3") &&
			$(stars[3]).hasClass("icon4") && $(stars[2]).hasClass("icon4") &&
			$(stars[1]).hasClass("icon5") && $(stars[0]).hasClass("icon5"),
			"ok to click first star when split is 2 and direction reversed and ratedIconsClass is array.");
		star = stars.last();
		star.simulate('click'); 
		ok($(stars[9]).hasClass("icon1") && !$(stars[8]).hasClass("icon1") &&
			!$(stars[7]).hasClass("icon2") && !$(stars[6]).hasClass("icon2") &&
			!$(stars[5]).hasClass("icon3") && !$(stars[4]).hasClass("icon3") &&
			!$(stars[3]).hasClass("icon4") && !$(stars[2]).hasClass("icon4") &&
			!$(stars[1]).hasClass("icon5") && !$(stars[0]).hasClass("icon5"),
			"ok to click last star when split is 2 and direction reversed and ratedIconsClass is array.");
		resetButton.simulate('click'); 
		ok(!$(stars[9]).hasClass("icon1") && !$(stars[8]).hasClass("icon1") &&
			!$(stars[7]).hasClass("icon2") && !$(stars[6]).hasClass("icon2") &&
			!$(stars[5]).hasClass("icon3") && !$(stars[4]).hasClass("icon3") &&
			!$(stars[3]).hasClass("icon4") && !$(stars[2]).hasClass("icon4") &&
			!$(stars[1]).hasClass("icon5") && !$(stars[0]).hasClass("icon5"),
			"ok to click reset button when split is 2 and direction reversed and ratedIconsClass is array.");
		
		rating.wijrating("option", "ratingMode", "single");
		star = $(stars[0]);
		star.simulate('click'); 
		ok(!$(stars[9]).hasClass("icon1") && !$(stars[8]).hasClass("icon1") &&
			!$(stars[7]).hasClass("icon2") && !$(stars[6]).hasClass("icon2") &&
			!$(stars[5]).hasClass("icon3") && !$(stars[4]).hasClass("icon3") &&
			!$(stars[3]).hasClass("icon4") && !$(stars[2]).hasClass("icon4") &&
			!$(stars[1]).hasClass("icon5") && $(stars[0]).hasClass("icon5"),
			"ok to click first star when split is 2 and direction reversed and ratingmode single and ratedIconsClass is array.");
		resetButton.simulate('click'); 
		ok(!$(stars[9]).hasClass("icon1") && !$(stars[8]).hasClass("icon1") &&
			!$(stars[7]).hasClass("icon2") && !$(stars[6]).hasClass("icon2") &&
			!$(stars[5]).hasClass("icon3") && !$(stars[4]).hasClass("icon3") &&
			!$(stars[3]).hasClass("icon4") && !$(stars[2]).hasClass("icon4") &&
			!$(stars[1]).hasClass("icon5") && !$(stars[0]).hasClass("icon5"),
			"ok to click first star when split is 2 and direction reversed and ratingmode single and ratedIconsClass is array.");
		
		rating.wijrating("option", "direction", "normal");
		stars = $(".wijmo-wijrating-normal", ratingElement[0]);
		star = $(stars[0]);
		star.simulate('click');
		ok($(stars[0]).hasClass("icon1") && !$(stars[1]).hasClass("icon1") &&
			!$(stars[2]).hasClass("icon2") && !$(stars[3]).hasClass("icon2") &&
			!$(stars[4]).hasClass("icon3") && !$(stars[5]).hasClass("icon3") &&
			!$(stars[6]).hasClass("icon4") && !$(stars[7]).hasClass("icon4") &&
			!$(stars[8]).hasClass("icon5") && !$(stars[9]).hasClass("icon5"),
			"ok to click first star when split is 2 and ratingmode single and ratedIconsClass is array.");
		resetButton.simulate('click'); 
		ok(!$(stars[0]).hasClass("icon1") && !$(stars[1]).hasClass("icon1") &&
			!$(stars[2]).hasClass("icon2") && !$(stars[3]).hasClass("icon2") &&
			!$(stars[4]).hasClass("icon3") && !$(stars[5]).hasClass("icon3") &&
			!$(stars[6]).hasClass("icon4") && !$(stars[7]).hasClass("icon4") &&
			!$(stars[8]).hasClass("icon5") && !$(stars[9]).hasClass("icon5"),
			"ok to click first star when split is 2 and ratingmode single and ratedIconsClass is array.");

		rating.wijrating("option", "value", 4.5);
		ok(!$(stars[0]).hasClass("icon1") && !$(stars[1]).hasClass("icon1") &&
			!$(stars[2]).hasClass("icon2") && !$(stars[3]).hasClass("icon2") &&
			!$(stars[4]).hasClass("icon3") && !$(stars[5]).hasClass("icon3") &&
			!$(stars[6]).hasClass("icon4") && !$(stars[7]).hasClass("icon4") &&
			$(stars[8]).hasClass("icon5") && !$(stars[9]).hasClass("icon5"),
			"ok to set value to 4.5 when split is 2 and ratingmode single and ratedIconsClass is array.");
			
		rating.wijrating("option", "ratingMode", 'continuous');
		ok($(stars[0]).hasClass("icon1") && $(stars[1]).hasClass("icon1") &&
			$(stars[2]).hasClass("icon2") && $(stars[3]).hasClass("icon2") &&
			$(stars[4]).hasClass("icon3") && $(stars[5]).hasClass("icon3") &&
			$(stars[6]).hasClass("icon4") && $(stars[7]).hasClass("icon4") &&
			$(stars[8]).hasClass("icon5") && !$(stars[9]).hasClass("icon5"),
			"ok to set value to 4.5 when split is 2 and ratingmode continuous and ratedIconsClass is array.");
		rating.remove();
	});

	test("iconWidth and iconHeight", function() {
		var rating = createRating(),
			ratingElement = rating.data("wijmo-wijrating").ratingElement,
			stars = $(".wijmo-wijrating-star", ratingElement[0]),
			starsContent = $(".wijmo-wijrating-normal", ratingElement[0]),
			margin;
		//if ($.browser.msie) {
		//	margin = "auto";
		//} else {
		//	margin = "0px";
		//}
		margin = "0px";
		rating.wijrating("option", "iconWidth", 20);
		rating.wijrating("option", "iconHeight", 20);
		stars = $(".wijmo-wijrating-star", ratingElement[0]);
		starsContent = $(".wijmo-wijrating-normal", ratingElement[0]);
		ok($(stars[0]).width() === 20 && $(stars[1]).width() === 20 &&
			$(stars[2]).width() === 20 && $(stars[3]).width() === 20 &&
			$(stars[4]).width() === 20,
			"stars are ok to set iconWidth to 20.");
		ok($(starsContent[0]).width() === 20 && $(starsContent[1]).width() === 20 &&
			$(starsContent[2]).width() === 20 && $(starsContent[3]).width() === 20 &&
			$(starsContent[4]).width() === 20,
			"stars content are ok to set iconWidth to 20.");
		ok($(stars[0]).height() === 20 && $(stars[1]).height() === 20 &&
			$(stars[2]).height() === 20 && $(stars[3]).height() === 20 &&
			$(stars[4]).height() === 20,
			"stars are ok to set iconHeight to 20.");
		ok($(starsContent[0]).height() === 20 && $(starsContent[1]).height() === 20 &&
			$(starsContent[2]).height() === 20 && $(starsContent[3]).height() === 20 &&
			$(starsContent[4]).height() === 20,
			"stars content are ok to set iconHeight to 20.");

		rating.wijrating("option", "split", 2);
		stars = $(".wijmo-wijrating-star", ratingElement[0]);
		starsContent = $(".wijmo-wijrating-normal", ratingElement[0]);
		ok($(stars[0]).width() === 10 && $(stars[1]).width() === 10 &&
			$(stars[2]).width() === 10 && $(stars[3]).width() === 10 &&
			$(stars[4]).width() === 10 && $(stars[5]).width() === 10 &&
			$(stars[6]).width() === 10 && $(stars[7]).width() === 10 &&
			$(stars[8]).width() === 10 && $(stars[9]).width() === 10 ,
			"stars are ok to set iconHeight to 20 and split is 2.");
		ok($(starsContent[0]).width() === 20 && $(starsContent[1]).width() === 20 &&
			$(starsContent[2]).width() === 20 && $(starsContent[3]).width() === 20 &&
			$(starsContent[4]).width() === 20 && $(starsContent[5]).width() === 20 &&
			$(starsContent[6]).width() === 20 && $(starsContent[7]).width() === 20 &&
			$(starsContent[8]).width() === 20 && $(starsContent[9]).width() === 20 ,
			"stars content are ok to set iconHeight to 20 and split is 2.");
		ok($(starsContent[0]).css("margin-left") === margin && $(starsContent[1]).css("margin-left") === "-10px" &&
			$(starsContent[2]).css("margin-left") === margin && $(starsContent[3]).css("margin-left") === "-10px" &&
			$(starsContent[4]).css("margin-left") === margin && $(starsContent[5]).css("margin-left") === "-10px" &&
			$(starsContent[6]).css("margin-left") === margin && $(starsContent[7]).css("margin-left") === "-10px" &&
			$(starsContent[8]).css("margin-left") === margin && $(starsContent[9]).css("margin-left") === "-10px" ,
			"stars content's margin are ok to set iconHeight to 20 and split is 2.");
		rating.wijrating("option", "direction", "reversed");
		stars = $(".wijmo-wijrating-star", ratingElement[0]);
		starsContent = $(".wijmo-wijrating-normal", ratingElement[0]);
		ok($(starsContent[0]).css("margin-left") === margin && $(starsContent[1]).css("margin-left") === "-10px" &&
			$(starsContent[2]).css("margin-left") === margin && $(starsContent[3]).css("margin-left") === "-10px" &&
			$(starsContent[4]).css("margin-left") === margin && $(starsContent[5]).css("margin-left") === "-10px" &&
			$(starsContent[6]).css("margin-left") === margin && $(starsContent[7]).css("margin-left") === "-10px" &&
			$(starsContent[8]).css("margin-left") === margin && $(starsContent[9]).css("margin-left") === "-10px" ,
			"stars content's margin are ok to set iconHeight to 20 and reversed and split is 2.");
			
		rating.wijrating("option", "orientation", "vertical");
		stars = $(".wijmo-wijrating-star", ratingElement[0]);
		starsContent = $(".wijmo-wijrating-normal", ratingElement[0]);
		ok($(stars[0]).height() === 10 && $(stars[1]).height() === 10 &&
			$(stars[2]).height() === 10 && $(stars[3]).height() === 10 &&
			$(stars[4]).height() === 10 && $(stars[5]).height() === 10 &&
			$(stars[6]).height() === 10 && $(stars[7]).height() === 10 &&
			$(stars[8]).height() === 10 && $(stars[9]).height() === 10 ,
			"stars are ok to set iconHeight to 20 and vertical and split is 2.");
		ok($(starsContent[0]).height() === 20 && $(starsContent[1]).height() === 20 &&
			$(starsContent[2]).height() === 20 && $(starsContent[3]).height() === 20 &&
			$(starsContent[4]).height() === 20 && $(starsContent[5]).height() === 20 &&
			$(starsContent[6]).height() === 20 && $(starsContent[7]).height() === 20 &&
			$(starsContent[8]).height() === 20 && $(starsContent[9]).height() === 20 ,
			"stars content are ok to set iconHeight to 20 and vertical and split is 2.");
		ok($(starsContent[0]).css("margin-top") === margin && $(starsContent[1]).css("margin-top") === "-10px" &&
			$(starsContent[2]).css("margin-top") === margin && $(starsContent[3]).css("margin-top") === "-10px" &&
			$(starsContent[4]).css("margin-top") === margin && $(starsContent[5]).css("margin-top") === "-10px" &&
			$(starsContent[6]).css("margin-top") === margin && $(starsContent[7]).css("margin-top") === "-10px" &&
			$(starsContent[8]).css("margin-top") === margin && $(starsContent[9]).css("margin-top") === "-10px" ,
			"stars content's margin are ok to set iconHeight to 20 and reversed and vertical and split is 2.");
			
		rating.wijrating("option", "direction", "normal");
		stars = $(".wijmo-wijrating-star", ratingElement[0]);
		starsContent = $(".wijmo-wijrating-normal", ratingElement[0]);
		ok($(starsContent[0]).css("margin-top") === margin && $(starsContent[1]).css("margin-top") === "-10px" &&
			$(starsContent[2]).css("margin-top") === margin && $(starsContent[3]).css("margin-top") === "-10px" &&
			$(starsContent[4]).css("margin-top") === margin && $(starsContent[5]).css("margin-top") === "-10px" &&
			$(starsContent[6]).css("margin-top") === margin && $(starsContent[7]).css("margin-top") === "-10px" &&
			$(starsContent[8]).css("margin-top") === margin && $(starsContent[9]).css("margin-top") === "-10px" ,
			"stars content's margin are ok to set iconHeight to 20 and vertical and split is 2.");
		rating.remove();
	});

}(jQuery));