﻿/*globals test, ok, jQuery, module, document, $*/
/*
* wijrating_core.js create & destroy
*/
"use strict"
function createRating(o) {
	//return $('<div id="rating"></div>').appendTo(document.body).wijrating(o);
	var select = $('<select id="rating"><option value="1">Below Average</option><option value="2">Average</option><option selected="selected" value="3">Above Average</option><option value="4">Awesome</option><option value="5">Epic</option></select>').appendTo(document.body);
	return select.wijrating(o);
}

(function ($) {
	module("wijrating: core");

	test("create and destroy", function() {
		var rating = createRating(),
			ratingWidget = rating.data("wijmo-wijrating"),
			ele = ratingWidget.ratingElement;
		ok((ele.hasClass('ui-widget') && ele.hasClass('wijmo-wijrating')), 
			'create:element class created.');

		rating.wijrating('destroy');
		ok((!ele.hasClass('ui-widget') && !ele.hasClass('wijmo-wijrating') || ele.parent('body').length === 0),
			'destroy:element class destroy.');

		rating.remove();
	});
}(jQuery));