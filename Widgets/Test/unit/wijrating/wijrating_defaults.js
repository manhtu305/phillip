﻿/*globals commonWidgetTests*/
/*
* wijrating_defaults.js
*/
"use strict";
commonWidgetTests('wijrating', {
	defaults: {
		initSelector: ":jqmData(role='wijrating')",
		wijCSS: $.extend(true, {}, $.wijmo.wijCSS, {
			wijrating: "wijmo-wijrating",
			wijratingStarContainer: "wijmo-wijrating-starcontainer",
			wijratingResetButton: "wijmo-wijrating-reset",
			wijratingHoverResetButton: "wijmo-wijrating-resethover",
			wijratingStar: "wijmo-wijrating-star",
			wijratingNormalStar: "wijmo-wijrating-normal",
			wijratingHoverStar: "wijmo-wijrating-hover",
			wijratingRatedStar: "wijmo-wijrating-rated",
			wijvRating: "wijmo-wijrating-vertical"
		}),
		wijMobileCSS: {
			iconClose: "ui-icon-delete"
		},
		create: null,
		disabled: false,
		count: 5,
		split: 1,
		totalValue: 5,
		value: 0,
		min: null,
		max: null,
		resetButton: {
			disabled: false,
			hint: "cancel this rating!",
			position: "leftOrTop",
			customizedClass: "",
			customizedHoverClass: ""
		},
		hint: {
			disabled: false,
			content: null
		},
		orientation: "horizontal",
		direction: "normal",
		ratingMode: "continuous",
		icons: {
			iconsClass: null,
			hoverIconsClass: null,
			ratedIconsClass: null
		},
		iconWidth: 16,
		iconHeight: 16,
		animation: null,
		rating: null,
		rated: null,
		reset: null,
		hover: null
	}
});