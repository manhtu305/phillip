/*
 * wijexpander_options.js
 */
(function ($) {

	module("wijexpander:options");

	test("allowExpand", function () {
		var $widget = create_wijexpander({ allowExpand: false });
		$widget.prependTo(document.body);
		$widget.find(".ui-expander-header a").simulate('mouseover').simulate('click').simulate('mouseout');
		setTimeout(function () {
			ok($('.ui-expander-content').is(':visible') == true, 'expanded');
			start();
			$widget.remove();
		}, 600);
		stop();
	});

	test("disabled", function () {
		var $widget = create_wijexpander({ disabled: true });
		$widget.prependTo(document.body);
		ok($widget.hasClass('ui-state-disabled'), 'disabled class added by constructor');
		$widget.wijexpander("option", "disabled", false);
		ok(!$widget.hasClass('ui-state-disabled'), 'no disabled classes');
		$widget.wijexpander("option", "disabled", true);
		ok($widget.hasClass('ui-state-disabled'), 'disabled class added after option change');
		$widget.wijexpander("option", "disabled", false);
		ok(!$widget.hasClass('ui-state-disabled'), 'disabled class removed after option change');
		$widget.remove();
	});

	test("expanded", function () {
		var $widget = create_wijexpander({ expanded: false });
		$widget.prependTo(document.body);
		ok(!$widget.find('.ui-expander-content').is(':visible') == true, 'expanded is false');
		$widget.wijexpander("option", "expanded", true);
		setTimeout(function () {
			ok($widget.find('.ui-expander-content').is(':visible') == true, 'expanded is true (changed by option)');
			$widget.remove();
			$widget = create_wijexpander({ expanded: false });
			$widget.wijexpander("option", "expanded", false);
			setTimeout(function () {
				ok(!$widget.find('.ui-expander-content').is(':visible') == true, 'expanded is false (changed by option)');
				$widget.remove();
				start();
			}, 600);

		}, 600);
		stop();
	});

	test("expandDirection", function () {
		var $widget = create_wijexpander({ expandDirection: "right" });
		$widget.prependTo(document.body);
		ok($widget.find('.ui-expander-content')[0].offsetLeft > $widget.find('.ui-expander-header')[0].offsetLeft, 'expand direction is right, content located at right');
		$widget.wijexpander("option", "expandDirection", "left");
		ok($widget.find('.ui-expander-content')[0].offsetLeft < $widget.find('.ui-expander-header')[0].offsetLeft, 'expand direction is left, content located at left');
		$widget.wijexpander("option", "expandDirection", "top");
		ok($widget.find('.ui-expander-content')[0].offsetTop < $widget.find('.ui-expander-header')[0].offsetTop, 'expand direction is top, content located at top');
		$widget.wijexpander("option", "expandDirection", "bottom");
		ok($widget.find('.ui-expander-content')[0].offsetTop > $widget.find('.ui-expander-header')[0].offsetTop, 'expand direction is bottom, content located at bottom');
		$widget.remove();
	});

	test("contentUrl", function () {
		var $widget = create_wijexpander({ contentUrl: "" });
		$widget.prependTo(document.body);
		$widget.wijexpander("option", "contentUrl", "external_content.htm");
		setTimeout(function () {
			var $iframe = $widget.find("iframe");
			ok($iframe.length > 0 && $widget.find("iframe")[0].src.indexOf("external_content.htm") != -1, 'external content assigned');
			start();
			$widget.wijexpander("option", "contentUrl", "");
			$iframe = $widget.find("iframe");
			ok($iframe.length == 0, 'external content removed');
			$widget.remove();
		}, 100);
		stop();
	});




	/*	
	??
	animated
	// Animation easing effect name, set this option to false in order to disable animation (can be set only from widget constructor). (easing effects requires UI Effects Core).
	// You can create own animation as follow:
	//        $("#expander2").wijexpander({
	//            animated: "custom1"
	//        });
	//        jQuery.wijmo.wijexpander.animations.custom1 = function (options) {
	//            this.slide(options, {
	//                easing: "easeInBounce",
	//                duration: 900
	//            });
	//        }
	// 
	// additional options that available for the animation function:
	//  expand - value of true indicates that content element must be expanded.
	//  horizontal - value of true indicates that expander is in horizontal orientation (when expandDirection is left or right)
	//  content - jQuery object that contains content element to be expanded or collapsed
	// Default: "slide"
	

	*/
})(jQuery);
