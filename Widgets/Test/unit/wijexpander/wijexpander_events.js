/*
 * wijexpander_events.js
 */
(function ($) {

    module("wijexpander: events");

    test("beforeExpand and afterExpand", function () {
    	var beforeExpandCalled = false;
    	var afterExpandCalled = false;
    	var $widget = create_wijexpander({ expanded: false, beforeExpand: function () { beforeExpandCalled = true; }, afterExpand: function () { afterExpandCalled = true; } });
        $widget.prependTo(document.body);
        $widget.wijexpander('expand');
        setTimeout(function () {
        	ok(beforeExpandCalled == true, "beforeExpand raised");
        	ok(afterExpandCalled == true, "afterExpand raised");
            start();
            $widget.remove();
        }, 600);
        stop();
    });

    test("beforeCollapse and afterCollapse", function () {
       	var beforeCollapseCalled = false;
       	var afterCollapseCalled = false;
       	var $widget = create_wijexpander({ expanded: true, beforeCollapse: function () { beforeCollapseCalled = true; }, afterCollapse: function () { afterCollapseCalled = true; } });
        $widget.prependTo(document.body);
        $widget.wijexpander('collapse');
        setTimeout(function () {
        	ok(beforeCollapseCalled == true, "beforeCollapse raised");
        	ok(afterCollapseCalled == true, "afterCollapse raised");
            start();
            $widget.remove();
        }, 600);
        stop();
    });

})(jQuery);
