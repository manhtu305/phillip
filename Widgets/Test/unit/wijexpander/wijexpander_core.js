/*
 * wijexpander_core.js
 */

function create_wijexpander(options) {
    return $('<div><div>header</div><div>content</div></div>').wijexpander(options);
}

(function ($) {

    module("wijexpander: core");

    test("create and destroy", function () {
        // test disconected element
        var $widget = create_wijexpander(),
            widgetHTML = $.browser.msie ? '<div><div>header</div><div>content</div></div>'
                                        : '<div class=""><div class="">header</div><div class="">content</div></div>',
            header = $widget.children().eq(0), content = $widget.children().eq(1);
        ok($widget.hasClass('wijmo-wijexpander ui-expander ui-widget ui-expander-icons'), 'element css classes created.');
        // 
        ok($widget.find('.ui-expander-header').hasClass('ui-state-active ui-corner-top'), 'header css classes created.');
        $widget.appendTo(document.body);
        ok($widget.find('.ui-expander-content')[0].offsetHeight > 0, "content panel is visible");
        $widget.wijexpander('destroy');
        ok(!$widget.hasClass('wijmo-wijexpander ui-expander ui-widget ui-expander-icons'), 'destroy: element css classes removed.');

        ok(header.html() === "header" && !(header.hasClass("ui-expander-header ui-conor-top")), 'Header recovered ! ');
        ok(content.html() === "content" && !content.hasClass("ui-expander-content ui-widget-content"), 'Content recovered ! ');
        ok($widget[0].outerHTML === widgetHTML, 'Expander destroyed !');
        $widget.remove();
    });

})(jQuery);

