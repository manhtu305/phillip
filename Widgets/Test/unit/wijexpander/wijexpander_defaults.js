/*
 * wijexpander_defaults.js
 */
var wijexpander_defaults = {
    allowExpand: true,
    animated: "slide",
    disabled: false,
    create: null,
    expanded: true,
    expandDirection: "bottom",
    contentUrl: "",
    wijMobileCSS: {
        "header": "ui-header ui-bar-a",
        "content": "ui-content ui-body ui-body-b"
    },
    initSelector: ":jqmData(role='wijexpander')",
    beforeCollapse: null,

    /** Occurs before the content area expands. 
    * Return false or call event.preventDefault() in order to cancel event and 
    * prevent the content area from expanding.
    * @type {Function}
    * Event type: wijexpanderbeforeexpand
    * @example
    * Supply a callback function to handle the beforeExpand event as an option.
    * $("#expander").wijexpander({ beforeExpand: function (e) {
    *		...
    *    }
    *	});
    * Bind to the event by type: wijexpanderbeforeexpand.
    * $( "#expander" ).bind( "wijexpanderbeforeexpand", function(e) {
    *		...		
    * });
    *        
    * @event
    */
    beforeExpand: null,

    /** Occurs after the content area collapses.
    * @type: wijexpanderaftercollapse
    * @example
    * Supply a callback function to handle the afterCollapse event as an option.
    * $("#expander").wijexpander({ afterCollapse: function (e) {
    *		...
    *    }
    *	});
    * Bind to the event by type: wijexpanderaftercollapse.
    * $( "#expander" ).bind( "wijexpanderaftercollapse", function(e) {
    *		...		
    * });
    *        
    * @event
    */
    afterCollapse: null,

    /** Occurs after the content area expands.
    * @type: wijexpanderafterexpand
    * @example
    * Supply a callback function to handle the afterExpand event as an option.
    * $("#expander").wijexpander({ afterExpand: function (e) {
    *		...
    *    }
    *	});
    * Bind to the event by type: wijexpanderafterexpand.
    * $( "#expander" ).bind( "wijexpanderafterexpand", function(e) {
    *		...		
    * });                
    * @event
    */
    afterExpand: null

};

commonWidgetTests('wijexpander', {
	defaults: wijexpander_defaults
});
