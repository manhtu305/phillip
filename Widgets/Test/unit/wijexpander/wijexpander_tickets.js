/*
 * wijexpander_tickets.js
 */
(function ($) {

    module("wijexpander: tickets");

    test("rounding classes: ", function () {
        var $widget = create_wijexpander({ expandDirection: "right" });
        $widget.prependTo(document.body);
        ok($widget.find('.ui-expander-header').hasClass("ui-corner-left"), 'header for right orientation should have ui-corner-left class');
        ok($widget.find('.ui-expander-content').hasClass("ui-corner-right"), 'content for right orientation should have ui-corner-right class');

        $widget.wijexpander("option", "expandDirection", "left");
        ok($widget.find('.ui-expander-header').hasClass("ui-corner-right"), 'header for left orientation should have ui-corner-right class');
        ok($widget.find('.ui-expander-content').hasClass("ui-corner-left"), 'content for left orientation should have ui-corner-left class');

        $widget.wijexpander("option", "expandDirection", "top");
        ok($widget.find('.ui-expander-header').hasClass("ui-corner-bottom"), 'header for top orientation should have ui-corner-bottom class');
        ok($widget.find('.ui-expander-content').hasClass("ui-corner-top"), 'content for top orientation should have ui-corner-top class');
        
        $widget.wijexpander("option", "expandDirection", "bottom");
        ok($widget.find('.ui-expander-header').hasClass("ui-corner-top"), 'header for bottom orientation should have ui-corner-top class');
        ok($widget.find('.ui-expander-content').hasClass("ui-corner-bottom"), 'content for bottom orientation should have ui-corner-bottom class');
        $widget.remove();
    });

    test("#69448", function () {
    	var $widget = create_wijexpander({ animated: null }).appendTo(document.body),
    		header = $widget.find('.ui-expander-header'),
			collapseCnt = 0,
			expandCnt = 0;

    	$widget.wijexpander({
    		expandDirection: "right",
    		afterCollapse: function () {
    			collapseCnt++;
    		},
    		afterExpand: function () {
    			expandCnt++;
    		}
    	});
    	header.simulate("click");
    	setTimeout(function () {
    		ok(collapseCnt === 1 && expandCnt === 0, "The expander collapses correctly after reset expandDirection.");
    		$widget.wijexpander({
    			expandDirection: "top"
    		});
    		header.simulate("click");
    		setTimeout(function () {
    			ok(collapseCnt === 1 && expandCnt === 1, "The expander expands correctly after reset expandDirection.");
    			$widget.wijexpander({
    				expandDirection: "left"
    			});
    			header.simulate("click");
    			setTimeout(function () {
    				ok(collapseCnt === 2 && expandCnt === 1, "The expander collapses correctly after reset expandDirection.");
    				$widget.remove();
    				start();
    			}, 100);
    		}, 100);
    	}, 100);
    	stop();
    });

})(jQuery);
