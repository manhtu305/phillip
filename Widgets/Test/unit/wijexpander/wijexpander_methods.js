/*
 * wijexpander_methods.js
 */
(function ($) {

    module("wijexpander: methods");

    test("collapse and expand", function () {
        var $widget = create_wijexpander();
        $widget.prependTo(document.body);
        $widget.wijexpander('collapse');
        setTimeout(function () {
            ok($('.ui-expander-content').is(':visible') == false, 'collapsed');            
            $widget.wijexpander('expand');           
            setTimeout(function () {
                ok($('.ui-expander-content').is(':visible') == true, 'expanded');
                start();
                $widget.remove();
            }, 600);

        }, 600);
        stop();
    });
})(jQuery);
