﻿/*
* wijprogressbar_events.js
*/

(function ($) {
	module("wijprogressbar: events");

	test("beforeProgressChanging, progressChanging, progressChanged", function () {
		expect(3);
		var begin = 0, change = 0, shop = 0;
		var $widget = create_wijprogressbar({
			beforeProgressChanging: function () {
				begin++;
			},
			progressChanging: function () {
				change++;
			},
			progressChanged: function () {
				shop++;
			},
			animationOptions: {
				animated: 'progress',
				duration: 100
			}
		});

		$widget.wijprogressbar({ value: 30 });

		setTimeout(function () {
			ok(begin > 0, 'beforeProgressChanging');
			ok(change > 1, 'progressChanging');
			ok(shop > 0, 'progressChanged');
			start();
			$widget.remove();
		}, 200);
		stop();
	});
})(jQuery);