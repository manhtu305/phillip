/*
* wijprogressbar_tickets.js
*/
(function ($) {
	module("wijprogressbar:tickets");

	test("#38177", function () {
		var $widget = create_wijprogressbar({
			value: 70,
			fillDirection: "north",
			labelAlign: "north"
		})
		, label;

		setTimeout(function () {
			label = $(".ui-progressbar-label");
			ok($(label).css("top") === "16px", "The top position is adjusted to the correct position for the north align tabel");
			$widget.remove();
			start();
		}, 500)

		stop();
	});

})(jQuery);