﻿/*
* wijprogressbar_options.js
*/
(function ($) {
	module("wijprogressbar:options");

	test("labelAlign", function () {

		var $widget = create_wijprogressbar({
			value: 30,
			animationOptions: false
		});

		setTimeout(function () {
			$widget.wijprogressbar({ labelAlign: 'south' });
			ok($widget.find('span:eq(0)').hasClass('wijmo-wijprogressbar-lb-south'), 'labelAlign:south');

			$widget.wijprogressbar({ labelAlign: 'north' });
			ok($widget.find('span:eq(0)').hasClass('wijmo-wijprogressbar-lb-north'), 'labelAlign:north');

			$widget.wijprogressbar({ labelAlign: 'east' });
			ok($widget.find('span:eq(0)').hasClass('wijmo-wijprogressbar-lb-east'), 'labelAlign:east');

			$widget.wijprogressbar({ labelAlign: 'west' });
			ok($widget.find('span:eq(0)').hasClass('wijmo-wijprogressbar-lb-west'), 'labelAlign:west');

			$widget.wijprogressbar({ labelAlign: 'center' });
			ok($widget.find('span:eq(0)').hasClass('wijmo-wijprogressbar-lb-center'), 'labelAlign:center');

			$widget.wijprogressbar("destroy");
			$widget.wijprogressbar({
				labelAlign: 'running',
				value: 80,
				animationOptions: {
					animated: 'progress',
					duration: 500
				}
			});
			var left = $widget.find('span:eq(0)').offset().left;
			setTimeout(function () {
				ok($widget.find('span:eq(0)').offset().left != left, 'labelAlign:running');
				start();
				$widget.wijprogressbar("destroy");
			}, 200);
		}, 100);
		stop();
	});

	test("value,maxValue,minValue", function () {
		var $widget = create_wijprogressbar({
			value: 40,
			animationOptions: false
		});

		setTimeout(function () {
			ok($widget.find('span:eq(0)').html().split('%')[0] == 40, 'value');
			$widget.wijprogressbar({ maxValue: 200 }); //20%
			setTimeout(function () {
				ok($widget.find('span:eq(0)').html().split('%')[0] == 20, 'maxValue');
				$widget.wijprogressbar({ maxValue: 60, minValue: 20 }); //50%
				setTimeout(function () {
					ok($widget.find('span:eq(0)').html().split('%')[0] == 50, 'minValue');
					start();
					$widget.wijprogressbar("destroy");
				}, 300)
			}, 300);
		}, 300);

		stop();
	});

	test("fillDirection", function () {
		var $widget = create_wijprogressbar({
			value: 40,
			animationOptions: false
		});

		setTimeout(function () {
			var a = $widget;
			ok($widget.hasClass("wijmo-wijprogressbar-east"), 'fillDirection:east');
			$widget.wijprogressbar({ fillDirection: 'west' });

			setTimeout(function () {
				ok($widget.hasClass("wijmo-wijprogressbar-west"), 'fillDirection:west');
				$widget.wijprogressbar({ fillDirection: 'south' });

				setTimeout(function () {
					ok($widget.hasClass("wijmo-wijprogressbar-south"), 'fillDirection:south');

					$widget.wijprogressbar({ fillDirection: 'north' });
					setTimeout(function () {
						ok($widget.hasClass("wijmo-wijprogressbar-north"), 'fillDirection:north');

						start();
						$widget.remove();
					}, 200);
				}, 200);
			}, 200);
		}, 200);

		stop();
	});

	test("labelFormatString,toolTipFormatString", function () {
		var $widget = create_wijprogressbar({
			value: 20,
			animationOptions: false,
			labelFormatString: "this label is {0}%",
			toolTipFormatString: "this tooltip is {0}%"
		});

		setTimeout(function () {
			var a = $widget;
			ok(/^this label is 20\%$/.test(a.find('span:eq(0)').html()), 'labelFormatString');
			ok(/^this tooltip is 20\%$/.test(a.attr('title')), 'toolTipFormatString');

			$widget.wijprogressbar({ labelFormatString: "{1}%" });
			setTimeout(function () {
				ok(a.find('span:eq(0)').html() === "20%", "labelFormatString is {1}% is passed.");
				$widget.wijprogressbar({ labelFormatString: "{2}%", value: 30 });
				setTimeout(function () {
					ok(a.find('span:eq(0)').html() === "70%", "labelFormatString is {2}% is passed.");
					$widget.wijprogressbar({ labelFormatString: "{3}%", value: 65 });
					setTimeout(function () {
						ok(a.find('span:eq(0)').html() === "35%", "labelFormatString is {3}% is passed.");
						$widget.wijprogressbar({ labelFormatString: "{4}%", value: 32 });
						setTimeout(function () {
							ok(a.find('span:eq(0)').html() === "0%", "labelFormatString is {4}% is passed.");
							$widget.wijprogressbar({ labelFormatString: "{5}%", value: 32 });
							ok(a.find('span:eq(0)').html() === "100%", "labelFormatString is {5}% is passed.");
							start();
							$widget.remove();
						}, 200);
					}, 200);
				}, 200);
			}, 200)

		}, 200);

		stop();
	});

	//indicatorImage
	test("indicatorImage", function () {
		var $widget = create_wijprogressbar({
			value: 20,
			animationOptions: false
		});

		setTimeout(function () {
			$widget.wijprogressbar('option', 'indicatorImage', 'indicatorImage.gif');
			var a = $widget;
			ok(/indicatorImage\.gif/.test($(".ui-progressbar-value",a).get(0).style.background), 'indicatorImage');
			start();
			$widget.remove();
		}, 200);

		stop();
	});

	test("disabled", function () {
	    var $widget = create_wijprogressbar({
	        value: 20,
	        disabled: true
	    });
	    ok($widget.hasClass("ui-state-disabled"), 'The disabled style has added to element');
	    $widget.remove();

	});
})(jQuery);