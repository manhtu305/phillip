﻿/*
* wijprogressbar_defaults.js
*/

var wijprogressbar_defaults = {
	disabled: false,

	labelAlign: "center",

	value: 0,

	max: 100,

	maxValue: 100,

	minValue: 0,

	fillDirection: "east",

	labelFormatString: "{1}%",

	toolTipFormatString: "{1}%",

	initSelector: ":jqmData(role='wijprogressbar')",

	indicatorIncrement: 1,

	indicatorImage: "",

	animationDelay: 0,

	animationOptions: { disabled: false,
		easing: null,
		duration: 500
	},
	progressChanging: null,
	create: null,
	change: null,
	complete: null,
	beforeProgressChanging: null,
	progressChanged: null
};

commonWidgetTests('wijprogressbar', { defaults: wijprogressbar_defaults });