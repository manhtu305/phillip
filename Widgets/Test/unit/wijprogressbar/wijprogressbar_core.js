﻿/*
* wijprogressbar_core.js
*/

var el;

function create_wijprogressbar(options) {
    var progressbar = $('<div id="progressbar1"></div>');
    progressbar.appendTo("body");
    return progressbar.wijprogressbar(options);
}
(function ($) {

    module("wijprogressbar: core");
    test("create and destroy", function () {
        // test disconected element
        var $widget = create_wijprogressbar({
            value:30,
            animationOptions:false
        });
        stop();
        setTimeout(function() { 
            start();
            ok($widget.hasClass("wijmo-wijprogressbar ui-progressbar ui-widget ui-widget-content ui-corner-all wijmo-wijprogressbar-east"),'element css classes created.');
            ok($widget.children().length>0,'created child nodes.');
            //ok($widget.children().first().hasClass("ui-progressbar-backlabel"),'backlabel element created.');
            ok($widget.children().first().hasClass("ui-progressbar-value ui-widget-header"),'progress element created');
            ok(!!$widget.attr('title'),'element attribute is created');
            if($widget.children().first().children())
            {
                ok($widget.children().first().children().has("ui-progressbar-frontlabel ui-corner-left"),'frontlabel element created.');
            }
            
             $widget.wijprogressbar("destroy");
             ok(!$widget.hasClass("wijmo-wijprogressbar ui-widget ui-widget-content ui-corner-all"),'element css classes removed.');
             ok($widget.children().length==0,'child elements removed.');

             ok(!$widget.data('domobject'),'element data is removed');
             ok(!$widget.attr('title'),'element attribute is removed');
             $widget.remove();
        }
         ,100);
    });
})(jQuery);