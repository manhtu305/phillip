﻿/*
* wijprogressbar_methods.js
*/

(function ($) {
	module("wijprogressbar: methods");

	test("value", function () {
		var $widget = create_wijprogressbar({
			animationOptions: {
				animated: 'progress',
				duration: 0
			}
		});
		$widget.wijprogressbar("option", "value", 34 );

		setTimeout(function () {
			ok($widget.find("span:first").html().split('%')[0] == 34 && parseInt($widget.children('div:first').width()) == parseInt($widget.width() * 0.34), 'setValue');
			ok($widget.wijprogressbar('value') == 34, 'getValue');
			start();
			$widget.remove();
		}, 100);
		stop();
	});
})(jQuery);