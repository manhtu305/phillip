﻿/*
 * wijpopup_methods.js
 */
(function($) {

	module("wijpopup: tickets");

	test("popup can't hide any more after cancel the hide behavior in hiding event one time", function() {
		var hidingCanceled = false;
			$widget = createPopup({
				autoHide: true,
				shown: function(){
					window.setTimeout(function(){ 
						$(document.body).trigger('mouseup');
						window.setTimeout(function(){ 
							$(document.body).trigger('mouseup');
						}, 500);
					}, 500);
				},
				hiding: function() {
					if(!hidingCanceled) {
						hidingCanceled = true;
						return false;
					}
				},
				hidden: function(){
					ok(true, "popup can hide after cancel the hide behavior in hiding event one time");
					start();
				}
			});

		window.setTimeout(function(){
			$widget.wijpopup('showAt', 120, 280);
		}, 500);
		
		stop();
	});

	test("56398", function () {
	    var pop = $("<div class=\"ui-widget-content ui-corner-all\"" +
                        " style=\"width: 300px; height:200px; z-index: 999;\">" +
                        "Text:<input id='cbo' /></div>").appendTo("body"),
            QA = [
                   { label: 'Sonia', value: 'Sonia' },
            ],
            outerPopList = $("<div>").appendTo("body").wijlist({ listItems: QA }),
            combobox = pop.find("#cbo").wijcombobox({ data: QA, ensureDropDownOnBody: false }),
            innerPopupItem = combobox.data("wijmo-wijcombobox")._menuUL.find(".wijmo-wijlist-item"),
	        outerPopupItem = outerPopList.find(".wijmo-wijlist-item");

	    pop.wijpopup({
	        autoHide: true,
	    });
	    pop.wijpopup('showAt', 120, 280);

	    combobox.wijcombobox('search', '');
	    setTimeout(function () {
	        innerPopupItem.simulate("mouseover");
	        innerPopupItem.simulate("mouseup");
	        ok(pop.is(":visible"), "Popup should not hide when click wijlist inner popup! ");

	        outerPopupItem.simulate("mouseover");
	        outerPopupItem.simulate("mouseup");
	        ok(!pop.is(":visible"), "Popup should hide when click wijlist outer popup! ");
	        start();

	        combobox.remove();
	        pop.remove();
	        outerPopList.remove();
	    }, 100)

	    stop();
	});

	test("72437", function () {
	    var pop = $("<div class=\"ui-widget-content ui-corner-all\"" +
                        " style=\"width: 300px; height:200px;\">" +
                        "</div>").appendTo("body");

	    pop.wijpopup({
	        autoHide: true,
	    });
	    pop.wijpopup('showAt', 120, 280);

	    ok(pop.zIndex() > 0, "Get a correct index !");
	    pop.remove();
	});

})(jQuery);
