/*
 * wijpopup_defaults.js
 */

commonWidgetTests('wijpopup', {
	defaults: {
			disabled:false,
			///	<summary>
			///     Determines if the element's parent element is the outermost element. 
			///		If true, the element's parent element will be changed to the body or outermost form element.
			///	</summary>
			ensureOutermost: false,
			///	<summary>
			///     Specifies the effect to be used when the popup is shown.
			///		Possible values: 'blind', 'clip', 'drop', 'fade', 'fold', 'slide', 'pulsate'.
			///	</summary>
			showEffect: 'show',
			///	<summary>
			///     Specified the object/hash including specific options for the show effect.
			///	</summary>
			showOptions: {},
			///	<summary>
			///     Defines how long (in milliseconds) the animation duration for showing the popup will last.
			///	</summary>
			showDuration: 300,
			///	<summary>
			///     Specifies the effect to be used when the popup is hidden.
			///		Possible values: 'blind', 'clip', 'drop', 'fade', 'fold', 'slide', 'pulsate'.
			///	</summary>
			hideEffect: 'hide',
			///	<summary>
			///     Specified the object/hash including specific options for the hide effect.
			///	</summary>
			hideOptions: {},
			///	<summary>
			///     Defines how long (in milliseconds) the animation duration for hiding the popup will last.
			///	</summary>
			hideDuration: 100,
			///	<summary>
			///     Determines whether to automatically hide the popup when clicking outside the element.
			///	</summary>
			autoHide: false,
			///	<summary>
			///     Options for positioning the element, please see jquery.ui.position for possible options.
			///	</summary>
			position:{
				at: 'left bottom',
				my: 'left top'
			},
			/// <summary>
			/// The showing event handler. A function called before the element is shown. Cancellable.
			/// Default: null.
			/// Type: Function.
			/// Code example: $("#element").wijpopup({ showing: function (e, args) { } });
			/// </summary>
			showing: null,
			/// <summary>
			/// The shown event handler. A function called after the element is shown.
			/// Default: null.
			/// Type: Function.
			/// Code example: $("#element").wijpopup({ shown: function (e) { } });
			/// </summary>
			///
			/// <param name="e" type="Object">jQuery.Event object.</param>
			shown: null,
			/// <summary>
			/// The hiding event handler. A function called before the element is hidden. Cancellable.
			/// Default: null.
			/// Type: Function.
			/// Code example: $("#element").wijpopup({ hiding: function (e) { } });
			/// </summary>
			///
			/// <param name="e" type="Object">jQuery.Event object.</param>
			hiding: null,
			/// <summary>
			/// The hidden event handler. A function called after the element is hidden.
			/// Default: null.
			/// Type: Function.
			/// Code example: $("#element").wijpopup({ hidden: function (e) { } });
			/// </summary>
			///
			/// <param name="e" type="Object">jQuery.Event object.</param>
			hidden: null,
			/// <summary>
			/// The posChanged event handler. A function called when the position of the element is changed.
			/// Default: null.
			/// Type: Function.
			/// Code example: $("#element").wijpopup({ posChanged: function (e) { } });
			/// </summary>
			///
			/// <param name="e" type="Object">jQuery.Event object.</param>
			posChanged: null,
			create: null,
			initSelector: ":jqmData(role='wijpopup')",
			wijCSS: $.wijmo.wijCSS
	}
});
