/*
 * wijpopup_options.js
 */
(function($) {

	module("wijpopup: options");

	test("disabled", function () {
	    $widget = createPopup({
            disabled:true
	    });
	    $widget.wijpopup('showAt', 120, 280);
	    window.setTimeout(function () {
	        ok($widget.hasClass("ui-state-disabled"),"the popup widget has disabled appearance.");
	        $widget.remove();
	        start();
	    }, 500);

	    stop();
	});
})(jQuery);
