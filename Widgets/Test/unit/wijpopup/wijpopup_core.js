/*
 * wijpopup_core.js
 */

function createPopup(o){
	return $("<div style='width:200px;height:200px;background-color:#585858;'></div>").appendTo(document.body).wijpopup(o);
}

(function($){

	module("wijpopup: core");

	test("create and destroy", function(){
		var $widget = createPopup();
		ok($widget.data('visible.wijpopup') == false, 'element is associated with wijpopup.');
		$widget.wijpopup('destroy');
		ok($widget.data('visible.wijpopup') == undefined, 'element is release from wijpopup');
		$widget.remove();
	});

})(jQuery);

