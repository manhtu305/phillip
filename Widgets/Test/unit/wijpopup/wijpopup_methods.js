/*
 * wijpopup_methods.js
 */
(function($) {

	module("wijpopup: methods");

	test("showAt, hide", function () {
	 
		var $widget = createPopup({
			shown: function(){
			
				ok($widget.is(":visible"), 'visible');
				ok($widget.wijpopup('isVisible'), 'isVisible is ok');
			
				var offset = $(document.body).offset();
				var pos = $widget.position();
				
				pos.left -= offset.left;
				pos.top -= offset.top;
			    //jquery 1.9: position changed
				
				ok(Math.round(pos.left) == 120, 'left position is ok');
				ok(Math.round(pos.top) == 80, 'top position is ok');
				
			
				window.setTimeout(function(){ 
					$widget.wijpopup("hide");
				}, 1000);
			},
			hidden: function(){
				ok(!$widget.is(":visible"), 'hidden');
				ok(!$widget.wijpopup('isVisible'), 'isVisible is ok');
				start();
			}
		});

		window.setTimeout(function(){
			$widget.wijpopup('showAt', 120, 80);
		}, 500);
		
		stop();
	});
	
	test("autoHide", function() {
		var $widget = createPopup({
			autoHide: true,
			shown: function(){
				ok($widget.is(":visible"), 'visible');
				ok($widget.wijpopup('isVisible'), 'isVisible is ok');
				window.setTimeout(function(){ 
					$(document.body).trigger('mouseup');
				}, 1000);
			},
			hidden: function(){
				ok(!$widget.is(":visible"), 'hidden');
				ok(!$widget.wijpopup('isVisible'), 'isVisible is ok');
				start();
			}
		});

		window.setTimeout(function(){
			$widget.wijpopup('showAt', 120, 280);
		}, 500);
		
		stop();
	});

})(jQuery);
