﻿/*
 * wijlightbox_tickets.js
 */

(function ($) {
    module("wijlightbox: tickets")

    function createTestLightbox(o) {
        function creatlink(url, alt, rel, title) {
            var link = $("<a>"), img = $("<img>");
            link.attr({
                href: url,
                rel: rel
            });
            img.attr({
                src: url.replace(/600/, "150").replace(/400/, "125"),
                title: title,
                alt: alt
            })
            link.append(img);
            return link;
        }

        var lightbox = $("<div class='testLightBox'></div>");
        lightbox.append(creatlink("http://lorempixel.com/600/400/sports/1", "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "wijlightbox[stock];player=img", "Sports 1"));
        lightbox.append(creatlink("http://lorempixel.com/600/400/sports/2", "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "wijlightbox[stock];player=img", "Sports 2"));
        lightbox.append(creatlink("http://lorempixel.com/600/400/sports/3", "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "wijlightbox[stock];player=img", "Sports 3"));
        lightbox.append(creatlink("http://lorempixel.com/600/400/sports/4", "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "wijlightbox[stock];player=img", "Sports 4"));
        lightbox.appendTo("body");
        lightbox.wijlightbox(o);
        return lightbox;
    }

    test("when page contains more than one lightbox, the index of the counter will show wrong", function () {
        var lightbox1 = createTestLightbox(),
            lightbox2 = createTestLightbox(),
            widget1 = lightbox1.data("wijmo-wijlightbox"),
            widget2 = lightbox2.data("wijmo-wijlightbox"),
            counter1 = widget1.counter,
            counter2 = widget2.counter;
        ok(counter1.text() === "1 of 4", "the first light box's counter is right");
        ok(counter2.text() === "1 of 4", "the second light box's counter is right");
        lightbox1.remove();
        lightbox2.remove();
    });

    test("when the lightbox show as modal, click the modal element outsite of the lightbox, even if the closeOnOuterClick set to false, the lightbox will close", function () {
        var lightbox1 = createTestLightbox({
            modal: true,
            closeOnOuterClick: true,
            resizeAnimation: { duration: 10 }
        });
        lightbox1.find("a:first").click();
        setTimeout(function () {
            $(".ui-widget-overlay").simulate("click", { clientX: 20, clientY: 20 });
            setTimeout(function () {
                ok($(".ui-widget-overlay").length === 0, "when the closeOnOuterClick set to true, the modal element is hidden");
                lightbox1.wijlightbox({ closeOnOuterClick: false });
                lightbox1.find("a:first").click();
                setTimeout(function () {
                    $(".ui-widget-overlay").simulate("click", { clientX: 20, clientY: 20 });
                    setTimeout(function () {
                        ok($(".ui-widget-overlay").length !== 0 && $(".ui-widget-overlay").is(":visible"), "when the closeOnOuterClick set to false, the modal element is visible");
                        lightbox1.remove();
                        start();
                    }, 1000);
                }, 1000);
            }, 1000);
        }, 1000);
        stop();
    });

    test("#41271", function () {
        var lightbox = createTestLightbox({ controlsPosition: "inside" }),
            widget = lightbox.data("wijmo-wijlightbox");
        ok(widget.container.hasClass("wijmo-wijlightbox-controls-inside"), "The controls is inside");
        lightbox.wijlightbox("option", "controlsPosition", "outside");
        ok(widget.container.hasClass("wijmo-wijlightbox-controls-outside"), "The controls is outside");
        lightbox.wijlightbox({ controlsPosition: "inside" });
        ok(widget.container.hasClass("wijmo-wijlightbox-controls-inside"), "The controls is inside");
        lightbox.remove();
    });

    test("#84191", function () {
        var $widget = createTestLightbox({
            closeOnOuterClick: false,
            showControlsOnHover: false,
            showCounter: false,
            transAnimation: {
                animated: "none"
            }
        });

        ok($(".wijmo-wijlightbox-counter").length === 0, "counter not shown.");
        $widget.wijlightbox("option", "showCounter", true);
        $widget.find("a:first").click();
        setTimeout(function () {
            ok($(".wijmo-wijlightbox-counter").length > 0 && $(".wijmo-wijlightbox-counter").css("display") !== "none", "counter shown.");
            $widget.wijlightbox("option", "showCounter", false);
            ok($(".wijmo-wijlightbox-counter").length === 0, "counter not shown.");
            $widget.remove();
            start();
        }, 500);
        stop();
    });

    test("#77062", function () {
        var $widget = createTestLightbox({
            closeOnOuterClick: false,
            showControlsOnHover: false,
            transAnimation: {
                animated: "none"
            }
        });

        $widget.wijlightbox("destroy");
        $widget.find("a:first").click(function () {
            return false;
        }).simulate("click");
        ok($(".ui-widget-overlay").length === 0, "no overlay.");
        $widget.remove();
    });
})(jQuery)