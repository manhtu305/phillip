(function ($) {
	module("wijgrid: data parsers");

	test("data values", function () {
		var $widget;

		try {
			$widget = createWidget({
				data: [
				["00", "10/01/1975", "02", "true", "$100.56"],
				["10", "06/08/1955", "12", "false", "$0.254"]
			  ],

				columns: [
					{ dataType: "number" },
					{ dataType: "datetime" },
					{ dataType: "string" },
					{ dataType: "boolean" },
					{ dataType: "currency" }
				]
			});

			var reference = [
				[0, new Date(1975, 9, 1), "02", true, 100.56],
				[10, new Date(1955, 5, 8), "12", false, 0.254]
			];

			var flag = true;
			for (var row = 0; row < 2 && flag; row++) {
				for (var cell = 0; cell < 5 && flag; cell++) {
					var currentCell = $widget.wijgrid("currentCell", cell, row);
					flag &= matchObj(currentCell.value(), reference[row][cell]);
				}
			}

			ok(flag, "values are correct");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});
})(jQuery);