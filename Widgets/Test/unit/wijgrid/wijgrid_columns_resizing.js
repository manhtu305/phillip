/*globals module,test,createWidget,jQuery,ok,document,same*/
/*
* wijgrid_columns_resizing.js
*/

(function ($) {
	module("wijgrid: columns resizing");
	var gridData = [
		[00, 01, 02],
		[10, 11, 12],
		[20, 21, 22],
		[30, 31, 32],
		[40, 41, 42],
		[50, 51, 52],
		[60, 61, 62],
		[70, 71, 72],
		[80, 81, 82],
		[90, 91, 92]
	];

	test("Columns Resizing: test columns resizing with UI.", function () {
		try {
			var settings = {
					allowColSizing: true,
					data: gridData,
					scrollMode: "auto",
					ensureColumnsPxWidth: true, // prevent autosizing of the columns
					columns: [
						{ width: 200 },
						{ width: 200 },
						{ width: 200 }
					]
				},
				$widget, $outerDiv, $th, width, sp;

			$widget = createWidget(settings, $("<table style=\"width: 600px;\"></table>")); // 600px = 3*200
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			$th = $outerDiv.find("th:eq(1)");
			width = $th.outerWidth();

			//column moving
			$th.trigger({ type: "mousedown", which: 1, pageX: $th.offset().left + $th.width() - 5, pageY: $th.offset().top + $th.height() / 2 });
			$(document).trigger({ type: "mousemove", button: 1, pageX: $th.offset().left + width * 2, pageY: $th.offset().top + $th.height() / 2 });
			$(document).trigger({ type: "mouseup" });

			//check
			ok($th.outerWidth() === width * 2 || $th.outerWidth() === width * 2 + 1, "check the width");
			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(sp && sp.hNeedScrollBar && !sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});
} (jQuery));
