(function ($) {
	module("wijgrid: events");

	var data = [
		["0", "1", "2"],
		["10", "11", "12"],
		["20", "21", "22"],
		["30", "31", "32"],
		["40", "41", "42"],
		["50", "51", "52"],
		["60", "61", "62"],
		["70", "71", "72"],
		["80", "81", "82"],
		["90", "91", "92"]
	];

	test("currentCellChanging, currentCellChanged", function () {
		var currentcellchangingCalled = false,
			currentCellChanged = false,
			args = null,
			$widget;

		try {
			$widget = createWidget({
				data: data,
				currentCellChanging: function (e, p) {
					args = p;
					currentcellchangingCalled = true;
				},
				currentCellChanged: function () {
					currentcellchangedCalled = true;
				}
			});

			// -- 1
			ok(currentcellchangingCalled == true, "currentCellChanging raised");
			// -- 2
			ok(args.cellIndex === 0 && args.rowIndex === 0 && args.oldCellIndex === -1 && args.oldRowIndex === -1, "currentCellChanging args are ok");
			// -- 3
			ok(currentcellchangedCalled, "currentCellChanged raised");

			currentcellchangingCalled = false;
			currentcellchangedCalled = false;
			args = null;
			$widget.wijgrid("currentCell", 1, 1);
			// -- 4
			ok(currentcellchangingCalled, "currentCellChanging raised");
			// -- 5
			ok(args.cellIndex === 1 && args.rowIndex === 1 && args.oldCellIndex === 0 && args.oldRowIndex === 0, "currentCellChanging args are ok");
			// -- 6
			ok(currentcellchangedCalled, "currentCellChanged raised");
			currentcellchangingCalled = false;

			currentcellchangedCalled = false;
			$widget.wijgrid("option", "currentCellChanging", function () { currentcellchangingCalled = true; return false; });
			$widget.wijgrid("currentCell", 2, 2);
			// -- 7
			ok(currentcellchangingCalled, "currentCellChanging raised, cancelled");
			// -- 8
			ok(!currentcellchangedCalled, "currentCellChanged not raised");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("selectionChanged", function () {
		// -- 1
		var selectionchangedCalled = false,
			$widget;

		try {
			$widget = createWidget({
				data: data,
				selectionChanged: function (e, p) {
					selectionchangedCalled = true;
				}
			});

			ok(selectionchangedCalled, "selectionChanged raised");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// -- 2
		try {
			selectionchangedCalled = false;
			$widget = createWidget({
				data: data,
				selectionMode: "none",
				selectionChanged: function (e, p) {
					selectionchangedCalled = true;
				}
			});
			ok(!selectionchangedCalled, "selectionChanged not raised");

			// -- 3
			selectionchangedCalled = false;
			var argsIsOK = false;
			$widget.wijgrid({
				selectionChanged: function (e, args) {
					selectionchangedCalled = true;
					argsIsOK = args.addedCells.length() === 3 && args.removedCells.length() === 0 &&
						args.addedCells.item(0).value() === "0" && args.addedCells.item(1).value() === "1" &&
						args.addedCells.item(2).value() === "2";
				}
			});

			$widget.wijgrid({
				selectionMode: "singleRow"
			});

			ok(selectionchangedCalled, "selectionChanged raised");

			// -- 4
			ok(argsIsOK, "selectionChanged args are ok");

			// -- 5
			selectionchangedCalled = false;
			argsIsOK = false;
			$widget.wijgrid({
				selectionChanged: function (e, args) {
					selectionchangedCalled = true;
					argsIsOK = args.addedCells.length() === 0 && args.removedCells.length() === 3 &&
						args.removedCells.item(0).value() === "0" && args.removedCells.item(1).value() === "1" &&
						args.removedCells.item(2).value() === "2";
				}
			});
			$widget.wijgrid("selection").clear();
			ok(selectionchangedCalled, "selectionChanged raised");

			// -- 6
			ok(argsIsOK, "selectionChanged args are ok");

			// -- 7
			selectionchangedCalled = true;
			argsIsOK = false;
			$widget.wijgrid({ selectionChanged: null, selectionMode: "multiRange" });
			$widget.wijgrid("selection").clear();
			$widget.wijgrid("selection").addRange(1, 1, 1, 1);

			$widget.wijgrid("option", "selectionChanged", function (e, args) {
				var a = args.addedCells;
				argsIsOK = args.removedCells.length() === 0 && a.length() === 8 && a.item(0).value() === "0" &&
					a.item(1).value() === "1" && a.item(2).value() === "2" &&
					a.item(3).value() === "10" && a.item(4).value() === "12" &&
					a.item(5).value() === "20" && a.item(6).value() === "21" && a.item(7).value() === "22";
			});
			$widget.wijgrid("selection").addRange(0, 0, 2, 2);

			// -- 8
			ok(selectionchangedCalled, "selectionChanged raised");
			// -- 9
			ok(argsIsOK, "selectionChanged args are ok");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});


	test("invalidCellValue", function () {
		// -- 1
		var invalidcellvalueCalled = false,
		    argsOK = false,
			$widget;

		try {
			$widget = createWidget({
			   allowEditing: true,
				data: [[0]],
				columns: [
               { dataType: "number" }
            ],
				invalidCellValue: function (e, args) {
				   argsOK = (args.value === "abcd");
					invalidcellvalueCalled = true;
				}
			});

			$widget.wijgrid("beginEdit");

         $widget.wijgrid("currentCell").container().find("input").val("abcd");

         $widget.wijgrid("endEdit");

			ok(invalidcellvalueCalled, "invalidCellValue raised");
         ok(argsOK, "invalidCellValue args are ok");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});


	test("pageIndexChanging", function() {
		var eventOK = false,
			$widget,
			underlyingReverted = true,
			eventCalled = false;

		try {
			$widget = createWidget({
				data: data,
				allowPaging: true,
				pageSize: 2,
				pagerSettings: {
					position: "topAndBottom"
				},
				pageIndexChanging: function(e, args) {
					eventOK = (args.newPageIndex === 1);
					return false;
				}
			});

			$(".wijmo-wijpager .wijmo-wijpager-button").eq(1).click();

			$(".wijmo-wijpager").each(function(i, elem) {
				underlyingReverted = underlyingReverted && ($(elem).wijpager("option", "pageIndex") === 0);
			});

			ok(eventOK && underlyingReverted && ($widget.wijgrid("option", "pageIndex") === 0), "OK");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

})(jQuery);