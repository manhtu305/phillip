/*globals module,test,createWidget,jQuery,ok,document,same*/
/*
* wijgrid_scrolling.js
*/

(function ($) {
	module("wijgrid: scrolling");
	var gridData = [
		[00, 01, 02],
		[10, 11, 12]
	];

	test("FlatView: scrollMode", function () {
		var settings = {
				data: gridData,
				scrollMode: "none",
				columns: [{width: 80, ensurePxWidth: true}, {width: 80, ensurePxWidth: true}, {width: 80, ensurePxWidth: true}],
				rowStyleFormatter: function (args) {
					var $table;
					if (args.state === $.wijmo.wijgrid.renderState.rendering && args.dataRowIndex === -1) {
						$table = args.$rows.parent().parent();
						$table.css("font-size", "12pt").addClass("height240");
					}
				}
			},
			$widget, $outerDiv, sp, $hbar, $vbar;

		// no bar.
		try {
			$widget = createWidget(settings, $("<table style='width:250px;height:250px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(!sp, "no scrollbar.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// no bar
		try {
			$widget = createWidget(settings, $("<table style='width:300px;height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(!sp, "no scrollbar.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// no bar
		try {
			$widget = createWidget(settings, $("<table style='width:200px;height:300px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(!sp, "no scrollbar.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// no bar
		try {
			$widget = createWidget(settings, $("<table style='width:200px;height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(!sp, "no scrollbar.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:200px;height:250px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(!sp, "no scrollbar.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:250px;height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");
			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(!sp, "no scrollbar.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// no width
		// no bar.
		try {
			$widget = createWidget(settings, $("<table style='height:250px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(!sp, "no scrollbar.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// no bar
		try {
			$widget = createWidget(settings, $("<table style='height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(!sp, "no scrollbar.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// no height
		// no bar.
		try {
			$widget = createWidget(settings, $("<table style='width:250px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(!sp, "no scrollbar.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// no bar
		try {
			$widget = createWidget(settings, $("<table style='width:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(!sp, "no scrollbar.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// no width, no height
		// no bar.
		try {
			$widget = createWidget(settings);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(!sp, "no scrollbar.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Fixed View: scrollMode", function () {
		var settings = {
				data: gridData,
				scrollMode: "auto",
				columns: [{width: 80, ensurePxWidth: true}, {width: 80, ensurePxWidth: true}, {width: 80, ensurePxWidth: true}],
				rowStyleFormatter: function (args) {
					var $table;
					if (args.state === $.wijmo.wijgrid.renderState.rendering) {
						if (args.dataRowIndex === -1) {
							$table = args.$rows.parent().parent();
							$table.css("font-size", "12pt").addClass("height40");
						}
						else if (args.dataRowIndex === 0) {
							$table = args.$rows.parent().parent();
							$table.css("font-size", "12pt").addClass("height200");
						}
					}
				}
			},
			$widget, $outerDiv, sp, $hbar, $vbar;

		// auto, no bar.
		try {
			$widget = createWidget(settings, $("<table style='width:250px;height:250px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(sp && !sp.hNeedScrollBar && !sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created but scrollbars are invisible.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// auto, show vertical bar
		try {
			$widget = createWidget(settings, $("<table style='width:300px;height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(sp && !sp.hNeedScrollBar && sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and vertical scrollbar is visible.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// auto, show horizontal bar
		try {
			$widget = createWidget(settings, $("<table style='width:200px;height:300px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(sp && sp.hNeedScrollBar && !sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and horizontal scrollbar is visible.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// auto, show both bars
		try {
			$widget = createWidget(settings, $("<table style='width:200px;height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(sp && sp.hNeedScrollBar && sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and both scrollbars are visible.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:200px;height:250px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(sp && sp.hNeedScrollBar && sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and both scrollbars are visible.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:250px;height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(sp && sp.hNeedScrollBar && sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and both scrollbars are visible.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// horizontal enable
		try {
			settings.scrollMode = "horizontal";
			$widget = createWidget(settings, $("<table style='width:200px;height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$hbar = sp.element.find(".wijmo-wijsuperpanel-hbarcontainer");
			ok(sp && sp.hNeedScrollBar && !sp.vNeedScrollBar && $hbar.size() === 1 && !$hbar.hasClass("wijmo-wijsuperpanel-hbarcontainer-disabled"), "scrollMode is 'horizontal'. Horizontal scrollbar is visible.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// horizontal disabled
		try {
			$widget = createWidget(settings, $("<table style='width:250px;height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$hbar = sp.element.find(".wijmo-wijsuperpanel-hbarcontainer");
			ok(sp && sp.hNeedScrollBar && !sp.vNeedScrollBar && $hbar.size() === 1 && $hbar.hasClass("wijmo-wijsuperpanel-hbarcontainer-disabled"), "scrollMode is 'horizontal'. Horizontal scrollbar is visible but disabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// vertical enabled
		try {
			settings.scrollMode = "vertical";
			$widget = createWidget(settings, $("<table style='width:200px;height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$vbar = sp.element.find(".wijmo-wijsuperpanel-vbarcontainer");
			ok(sp && !sp.hNeedScrollBar && sp.vNeedScrollBar && $vbar.size() === 1 && !$vbar.hasClass("wijmo-wijsuperpanel-vbarcontainer-disabled"), "scrollMode is 'vertical'. Vertical scrollbar is visible.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// vertical disabled
		try {
			$widget = createWidget(settings, $("<table style='width:200px;height:250px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$vbar = sp.element.find(".wijmo-wijsuperpanel-vbarcontainer");
			ok(sp && !sp.hNeedScrollBar && sp.vNeedScrollBar && $vbar.size() === 1 && $vbar.hasClass("wijmo-wijsuperpanel-vbarcontainer-disabled"), "scrollMode is 'vertical'. Vertical scrollbar is visible but disabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// both
		try {
			settings.scrollMode = "both";
			$widget = createWidget(settings, $("<table style='width:300px;height:300px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$hbar = sp.element.find(".wijmo-wijsuperpanel-hbarcontainer");
			$vbar = sp.element.find(".wijmo-wijsuperpanel-vbarcontainer");
			ok(sp.hNeedScrollBar && sp.vNeedScrollBar && $hbar.size() === 1 && $hbar.hasClass("wijmo-wijsuperpanel-hbarcontainer-disabled") && $vbar.size() === 1 && $vbar.hasClass("wijmo-wijsuperpanel-vbarcontainer-disabled"), "scrollMode is 'both'. Both scrollbars are visible but disabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:250px;height:250px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$hbar = sp.element.find(".wijmo-wijsuperpanel-hbarcontainer");
			$vbar = sp.element.find(".wijmo-wijsuperpanel-vbarcontainer");
			ok(sp.hNeedScrollBar && sp.vNeedScrollBar && $hbar.size() === 1 && !$hbar.hasClass("wijmo-wijsuperpanel-hbarcontainer-disabled") && $vbar.size() === 1 && !$vbar.hasClass("wijmo-wijsuperpanel-vbarcontainer-disabled"), "scrollMode is 'both'. Both scrollbars are visible and enabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// no width
		// auto, no bar.
		try {
			settings.scrollMode = "auto";
			$widget = createWidget(settings, $("<table style='height:250px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(sp && !sp.hNeedScrollBar && !sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created but scrollbars are invisible.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// auto, show vertical bar
		try {
			$widget = createWidget(settings, $("<table style='height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(sp && !sp.hNeedScrollBar && sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and vertical scrollbar is visible.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// horizontal disabled
		try {
			settings.scrollMode = "horizontal";
			$widget = createWidget(settings, $("<table style='height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$hbar = sp.element.find(".wijmo-wijsuperpanel-hbarcontainer");
			ok(sp && sp.hNeedScrollBar && !sp.vNeedScrollBar && $hbar.size() === 1 && $hbar.hasClass("wijmo-wijsuperpanel-hbarcontainer-disabled"), "scrollMode is 'horizontal'. Horizontal scrollbar is visible but disabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='height:250px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$hbar = sp.element.find(".wijmo-wijsuperpanel-hbarcontainer");
			ok(sp && sp.hNeedScrollBar && !sp.vNeedScrollBar && $hbar.size() === 1 && $hbar.hasClass("wijmo-wijsuperpanel-hbarcontainer-disabled"), "scrollMode is 'horizontal'. Horizontal scrollbar is visible but disabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// vertical enabled
		try {
			settings.scrollMode = "vertical";
			$widget = createWidget(settings, $("<table style='height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$vbar = sp.element.find(".wijmo-wijsuperpanel-vbarcontainer");
			ok(sp && !sp.hNeedScrollBar && sp.vNeedScrollBar && $vbar.size() === 1 && !$vbar.hasClass("wijmo-wijsuperpanel-vbarcontainer-disabled"), "scrollMode is 'vertical'. Vertical scrollbar is visible.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// vertical disabled
		try {
			$widget = createWidget(settings, $("<table style='height:250px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$vbar = sp.element.find(".wijmo-wijsuperpanel-vbarcontainer");
			ok(sp && !sp.hNeedScrollBar && sp.vNeedScrollBar && $vbar.size() === 1 && $vbar.hasClass("wijmo-wijsuperpanel-vbarcontainer-disabled"), "scrollMode is 'vertical'. Vertical scrollbar is visible but disabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// both
		try {
			settings.scrollMode = "both";
			$widget = createWidget(settings, $("<table style='height:300px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$hbar = sp.element.find(".wijmo-wijsuperpanel-hbarcontainer");
			$vbar = sp.element.find(".wijmo-wijsuperpanel-vbarcontainer");
			ok(sp.hNeedScrollBar && sp.vNeedScrollBar && $hbar.size() === 1 && $hbar.hasClass("wijmo-wijsuperpanel-hbarcontainer-disabled") && $vbar.size() === 1 && $vbar.hasClass("wijmo-wijsuperpanel-vbarcontainer-disabled"), "scrollMode is 'both'. Both scrollbars are visible but disabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='height:250px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$hbar = sp.element.find(".wijmo-wijsuperpanel-hbarcontainer");
			$vbar = sp.element.find(".wijmo-wijsuperpanel-vbarcontainer");
			ok(sp.hNeedScrollBar && sp.vNeedScrollBar && $hbar.size() === 1 && $hbar.hasClass("wijmo-wijsuperpanel-hbarcontainer-disabled") && $vbar.size() === 1 && !$vbar.hasClass("wijmo-wijsuperpanel-vbarcontainer-disabled"), "scrollMode is 'both'. Vertical scrollbar is visible and enabled but horizontal scrollbar is visible but disabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// no height
		// auto, no bar.
		try {
			settings.scrollMode = "auto";
			$widget = createWidget(settings, $("<table style='width:250px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(sp && !sp.hNeedScrollBar && !sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created but scrollbars are invisible.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// auto, show horizontal bar
		try {
			$widget = createWidget(settings, $("<table style='width:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(sp && sp.hNeedScrollBar && !sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and horizontal scrollbar is visible.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// horizontal enabled
		try {
			settings.scrollMode = "horizontal";
			$widget = createWidget(settings, $("<table style='width:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$hbar = sp.element.find(".wijmo-wijsuperpanel-hbarcontainer");
			ok(sp && sp.hNeedScrollBar && !sp.vNeedScrollBar && $hbar.size() === 1 && !$hbar.hasClass("wijmo-wijsuperpanel-hbarcontainer-disabled"), "scrollMode is 'horizontal'. Horizontal scrollbar is visible.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// horizontal disabled
		try {
			$widget = createWidget(settings, $("<table style='width:250px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$hbar = sp.element.find(".wijmo-wijsuperpanel-hbarcontainer");
			ok(sp && sp.hNeedScrollBar && !sp.vNeedScrollBar && $hbar.size() === 1 && $hbar.hasClass("wijmo-wijsuperpanel-hbarcontainer-disabled"), "scrollMode is 'horizontal'. Horizontal scrollbar is visible but disabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// vertical disabled
		try {
			settings.scrollMode = "vertical";
			$widget = createWidget(settings, $("<table style='width:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$vbar = sp.element.find(".wijmo-wijsuperpanel-vbarcontainer");
			ok(sp && !sp.hNeedScrollBar && sp.vNeedScrollBar && $vbar.size() === 1 && $vbar.hasClass("wijmo-wijsuperpanel-vbarcontainer-disabled"), "scrollMode is 'vertical'. Vertical scrollbar is visible but disabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:250px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$vbar = sp.element.find(".wijmo-wijsuperpanel-vbarcontainer");
			ok(sp && !sp.hNeedScrollBar && sp.vNeedScrollBar && $vbar.size() === 1 && $vbar.hasClass("wijmo-wijsuperpanel-vbarcontainer-disabled"), "scrollMode is 'vertical'. Vertical scrollbar is visible but disabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// both
		try {
			settings.scrollMode = "both";
			$widget = createWidget(settings, $("<table style='width:300px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$hbar = sp.element.find(".wijmo-wijsuperpanel-hbarcontainer");
			$vbar = sp.element.find(".wijmo-wijsuperpanel-vbarcontainer");
			ok(sp.hNeedScrollBar && sp.vNeedScrollBar && $hbar.size() === 1 && $hbar.hasClass("wijmo-wijsuperpanel-hbarcontainer-disabled") && $vbar.size() === 1 && $vbar.hasClass("wijmo-wijsuperpanel-vbarcontainer-disabled"), "scrollMode is 'both'. Both scrollbars are visible but disabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:250px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$hbar = sp.element.find(".wijmo-wijsuperpanel-hbarcontainer");
			$vbar = sp.element.find(".wijmo-wijsuperpanel-vbarcontainer");
			ok(sp.hNeedScrollBar && sp.vNeedScrollBar && $hbar.size() === 1 && !$hbar.hasClass("wijmo-wijsuperpanel-hbarcontainer-disabled") && $vbar.size() === 1 && $vbar.hasClass("wijmo-wijsuperpanel-vbarcontainer-disabled"), "scrollMode is 'both'. Horizontal scrollbar is visible and enabled but vertical scrollbar is visible but disabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// no width, no height
		// auto, no bar.
		try {
			settings.scrollMode = "auto";
			$widget = createWidget(settings);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(sp && !sp.hNeedScrollBar && !sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created but scrollbars are invisible.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// horizontal disabled
		try {
			settings.scrollMode = "horizontal";
			$widget = createWidget(settings);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$hbar = sp.element.find(".wijmo-wijsuperpanel-hbarcontainer");
			ok(sp && sp.hNeedScrollBar && !sp.vNeedScrollBar && $hbar.size() === 1 && $hbar.hasClass("wijmo-wijsuperpanel-hbarcontainer-disabled"), "scrollMode is 'horizontal'. Horizontal scrollbar is visible but disabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// vertical disabled
		try {
			settings.scrollMode = "vertical";
			$widget = createWidget(settings);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$vbar = sp.element.find(".wijmo-wijsuperpanel-vbarcontainer");
			ok(sp && !sp.hNeedScrollBar && sp.vNeedScrollBar && $vbar.size() === 1 && $vbar.hasClass("wijmo-wijsuperpanel-vbarcontainer-disabled"), "scrollMode is 'vertical'. Vertical scrollbar is visible but disabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// both
		try {
			settings.scrollMode = "both";
			$widget = createWidget(settings);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			$hbar = sp.element.find(".wijmo-wijsuperpanel-hbarcontainer");
			$vbar = sp.element.find(".wijmo-wijsuperpanel-vbarcontainer");
			ok(sp.hNeedScrollBar && sp.vNeedScrollBar && $hbar.size() === 1 && $hbar.hasClass("wijmo-wijsuperpanel-hbarcontainer-disabled") && $vbar.size() === 1 && $vbar.hasClass("wijmo-wijsuperpanel-vbarcontainer-disabled"), "scrollMode is 'both'. Both scrollbars are visible but disabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	var gridData2 = [
		[00, 01, 02],
		[10, 11, 12],
		[20, 21, 22],
		[30, 31, 32],
		[40, 41, 42],
		[50, 51, 52],
		[60, 61, 62],
		[70, 71, 72],
		[80, 81, 82],
		[90, 91, 92],
		[100, 101, 102],
		[110, 111, 112],
		[120, 121, 122]
	];

	test("FixedView: allowKeyboardNavigation(UI)", function () {
		expect(2);
		stop();

		var settings = {
				data: gridData2,
				allowKeyboardNavigation: true,
				scrollMode: "auto"
			},
			$widget, $outerDiv, sp;

		$widget = createWidget(settings, $("<table style='width:300px;height:200px;'></table>"));
		$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

		function timeEvent(e) {
			var $element = $($widget.wijgrid("currentCell").tableCell()),
				sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel"),
				$container;

			if (sp && $element.is(":visible")) {
				$container = sp.getContentElement().parent();
				ok(isElementInVisblePart($element, $container), "currentCell is in visible part");
			}

			if ($widget.wijgrid("currentCell").rowIndex() === 0) {
				start();
				$widget.remove();
			} else {
				$widget.trigger({ type: "keydown", which: $.ui.keyCode.PAGE_UP, keyCode: $.ui.keyCode.PAGE_UP });
				window.setTimeout(timeEvent, 1000);
			}
		}

		$widget.trigger({ type: "keydown", which: $.ui.keyCode.PAGE_DOWN, keyCode: $.ui.keyCode.PAGE_DOWN });
		window.setTimeout(timeEvent, 1000);
	});

	test("FixedView: allowKeyboardNavigation(programmatically)", function () {
		expect(2);
		stop();

		var settings = {
				data: gridData2,
				allowKeyboardNavigation: true,
				scrollMode: "auto"
			},
			$widget, $outerDiv, sp;

		$widget = createWidget(settings, $("<table style='width:300px;height:200px;'></table>"));
		$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

		function timeEvent() {
			var $element = $($widget.wijgrid("currentCell").tableCell()),
				sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel"),
				$container;

			if (sp && $element.is(":visible")) {
				$container = sp.getContentElement().parent();
				ok(isElementInVisblePart($element, $container), "currentCell is in visible part");
 			}

			if ($widget.wijgrid("currentCell").rowIndex() === 0) {
				start();
				$widget.remove();
			} else {
				$widget.wijgrid("currentCell", 0, 0);
				window.setTimeout(timeEvent, 1000);
			}
		}

		$widget.wijgrid("currentCell", 2, 12);
		window.setTimeout(timeEvent, 1000);
	});

	test("Issue", function () {
		var settings = {
				data: gridData2,
				scrollMode: "auto"
			},
			$widget, $outerDiv, sp;

		try {
			$widget = createWidget(settings);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(sp && !sp.hNeedScrollBar && !sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created but scrollbars are invisible.");
			$widget.remove();

			//using staticRowIndex
			settings.staticRowIndex = 0;
			$widget = createWidget(settings);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(sp && !sp.hNeedScrollBar && !sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created but scrollbars are invisible.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
			settings.staticRowIndex = -1;
		}
	});

	test("Issue", function () {
		var settings = {
			data: gridData2,
			allowKeyboardNavigation: true,
			scrollMode: "auto"
		}, $widget;

		try {
			$widget = createWidget(settings, $("<table style='width:300px;height:200px;'></table>"));
			$widget.trigger({ type: "keydown", which: $.ui.keyCode.DOWN, keyCode: $.ui.keyCode.DOWN });
			ok(true, "no exception is thrown");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	/// TFS #23466: [Unable to get value of the property '1': object is null or undefined] is thrown on pressing DownArrow key after selection is set to last row if AllowKeyboardNavigation="True"
	test("Issue 23466 ", function () {
		var settings = {
			data: gridData2,
			allowKeyboardNavigation: true,
			scrollMode: "auto"
		}, $widget;

		try {
			$widget = createWidget(settings, $("<table style='width:300px;height:200px;'></table>"));

			$widget.wijgrid("currentCell", 0, settings.data.length - 1); // move to the last row

			$widget.trigger({ type: "keydown", which: $.ui.keyCode.DOWN, keyCode: $.ui.keyCode.DOWN });

			ok(true, "no exception is thrown");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});
} (jQuery));