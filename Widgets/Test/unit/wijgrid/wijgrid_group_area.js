/*globals module,test,createWidget,jQuery,ok,document,same*/
/*
* wijgrid_group_area.js
*/

(function ($) {
	module("wijgrid: group area");
	var gridData = [
		["12", "10", "24"],
		["91", "20", "46"],
		["66", "20", "25"],
		["57", "10", "83"],
		["18", "20", "74"]
	];

	test("Group area: add and delete a grouped column.", function () {
		var settings = {
				allowSorting: true,
				showGroupArea: true,
				data: gridData,
				columns: [
					{ aggregate: "min" },
					{
						aggregate: "count",
						groupInfo: {
							position: "header",
							headerText: "{2}"
						}
					},
					{ dataType: "number", aggregate: "sum" }
				]
			},
			$widget, $outerDiv, rows, row, cells, columnWidgets, buttons;

		try {
			$widget = createWidget(settings);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(0)[0];
			ok(row.id === "GH0-1", "the first row is grouping header(0-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "2", "the number of the items is 2");
			ok(getTextContent(cells[1]) === "2", "the sum of the items is 2");
			row = rows.item(3)[0];
			ok(row.id === "GH1-1", "the fourth row is grouping header(1-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "3", "the number of the items is 3");
			ok(getTextContent(cells[1]) === "3", "the sum of the items is 3");
			buttons = $outerDiv.find(".wijmo-wijgrid-group-button");
			ok($(buttons[0]).text() === "1", "the number of the items is 1");

			columnWidgets = $widget.wijgrid("columns");
			columnWidgets[0].option("groupInfo", {
				position: "header",
				headerText: "{2}"
			});
			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(0)[0];
			ok(row.id === "GH0-1", "the first row is grouping header(0-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "12", "the number of the items is 12");
			ok(getTextContent(cells[1]) === "1", "the sum of the items is 1");
			row = rows.item(1)[0];
			ok(row.id === "GH0-2", "the first row is grouping header(0-2)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "1", "the number of the items is 1");
			ok(getTextContent(cells[1]) === "1", "the sum of the items is 1");
			row = rows.item(3)[0];
			ok(row.id === "GH1-1", "the first row is grouping header(1-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "18", "the number of the items is 18");
			ok(getTextContent(cells[1]) === "1", "the sum of the items is 1");
			buttons = $outerDiv.find(".wijmo-wijgrid-group-button");
			ok($(buttons[0]).text() === "0", "the number of the items is 0");
			ok($(buttons[1]).text() === "1", "the number of the items is 1");


			columnWidgets = $widget.wijgrid("columns");
			columnWidgets[0].option("groupInfo", {
				position: "none"
			});

			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(0)[0];
			ok(row.id === "GH0-1", "the first row is grouping header(0-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "2", "the number of the items is 2");
			ok(getTextContent(cells[1]) === "2", "the sum of the items is 2");
			row = rows.item(3)[0];
			ok(row.id === "GH1-1", "the fourth row is grouping header(1-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "3", "the number of the items is 3");
			ok(getTextContent(cells[1]) === "3", "the sum of the items is 3");
			buttons = $outerDiv.find(".wijmo-wijgrid-group-button");
			ok($(buttons[0]).text() === "1", "the number of the items is 1");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Group area: add and delete a grouped column(UI).", function () {
		var settings = {
				allowSorting: true,
				showGroupArea: true,
				allowColMoving: true,
				data: gridData,
				columns: [
					{ aggregate: "min" },
					{
						aggregate: "count",
						groupInfo: {
							position: "header",
							headerText: "{2}"
						}
					},
					{ dataType: "number", aggregate: "sum" }
				]
			},
			$widget, $outerDiv, rows, row, cells, buttons, $element;

		try {
			$widget = createWidget(settings);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(0)[0];
			ok(row.id === "GH0-1", "the first row is grouping header(0-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "2", "the number of the items is 2");
			ok(getTextContent(cells[1]) === "2", "the sum of the items is 2");
			row = rows.item(3)[0];
			ok(row.id === "GH1-1", "the fourth row is grouping header(1-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "3", "the number of the items is 3");
			ok(getTextContent(cells[1]) === "3", "the sum of the items is 3");
			buttons = $outerDiv.find(".wijmo-wijgrid-group-button");
			ok($(buttons[0]).text() === "1", "the number of the items is 1");

		    //It seems the trigger cannot simulate drag&drop in jquery ui 1.11.0. It can works in jquery ui 1.10.1.
		    //use simulate to replace trigger in jquery ui 1.11.0.
			$element = $outerDiv.find("th:eq(0)");
			//$element.trigger({ type: "mousedown", which: 1, pageX: $element.offset().left + $element.width() / 2, pageY: $element.offset().top + $element.height() / 2 });
			$element.simulate("mousedown", { which: 1, clientX: $element.offset().left + $element.width() / 2, clientY: $element.offset().top + $element.height() / 2 });
			$element = $($outerDiv.find(".wijmo-wijsuperpanel-header").children()[0]);
			//$(document).trigger({ type: "mousemove", button: 1, pageX: $element.offset().left + $element.width() / 2, pageY: $element.offset().top + $element.height() / 2 });
			$(document).simulate("mousemove", { button: 1, clientX: $element.offset().left + $element.width() / 2, clientY: $element.offset().top + $element.height() / 2 });
			//$(document).trigger({ type: "mouseup" });
			$(document).simulate("mouseup");
			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(0)[0];
			ok(row.id === "GH0-1", "the first row is grouping header(0-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "2", "the number of the items is 2");
			ok(getTextContent(cells[1]) === "2", "the sum of the items is 2");

			row = rows.item(1)[0];
			ok(row.id === "GH0-2", "the first row is grouping header(0-2)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "0:1212", "the number of the items is 0: 12 12");
			ok(getTextContent(cells[1]) === "1", "the sum of the items is 1");


			row = rows.item(5)[0];
			ok(row.id === "GH1-1", "the first row is grouping header(1-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "3", "the number of the items is 3");
			ok(getTextContent(cells[1]) === "3", "the sum of the items is 3");
			buttons = $outerDiv.find(".wijmo-wijgrid-group-button");
			ok($(buttons[0]).text() === "1", "the number of the items is 1");
			ok($(buttons[1]).text() === "0", "the number of the items is 0");

			// ungroup second column
			$element = $outerDiv.find(".wijmo-wijgrid-group-button-close:eq(1)");
			$element.trigger({ type: "click" });

			rows = $widget.data("wijmo-wijgrid")._rows();

			row = rows.item(0)[0];
			ok(row.id === "GH0-1", "the first row is grouping header(0-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "2", "the number of the items is 2");
			ok(getTextContent(cells[1]) === "2", "the sum of the items is 2");

			row = rows.item(3)[0];
			ok(row.id === "GH1-1", "the fourth row is grouping header(1-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "3", "the number of the items is 3");
			ok(getTextContent(cells[1]) === "3", "the sum of the items is 3");
			buttons = $outerDiv.find(".wijmo-wijgrid-group-button");
			ok($(buttons[0]).text() === "1", "the number of the items is 1");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Group area: test sorting.", function () {
		var settings = {
				allowSorting: true,
				showGroupArea: true,
				data: gridData,
				columns: [
					{ aggregate: "min" },
					{
						aggregate: "count",
						groupInfo: {
							position: "header",
							headerText: "{2}"
						}
					},
					{ dataType: "number", aggregate: "sum" }
				]
			},
			$widget, $outerDiv, rows, row, cells, ths, span;

		try {
			$widget = createWidget(settings);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ths = $outerDiv.find("th");
			span = $(ths[1]).find("span.ui-icon-triangle-1-n");
			ok(span.length === 1, "check the mark");
			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(1)[0];
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok($(cells[1]).text() === "10", "the number of the items is 10");
			row = rows.item(4)[0];
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok($(cells[1]).text() === "20", "the number of the items is 20");

			$outerDiv.find(".wijmo-wijgrid-group-button:eq(0)").trigger({ type: "click" });
			ths = $outerDiv.find("th");
			span = $(ths[1]).find("span.ui-icon-triangle-1-s");
			ok(span.length === 1, "check the mark");
			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(1)[0];
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok($(cells[1]).text() === "20", "the number of the items is 20");
			row = rows.item(5)[0];
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok($(cells[1]).text() === "10", "the number of the items is 10");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});
} (jQuery));