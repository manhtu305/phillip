/*globals module,test,createWidget,jQuery,ok,document,same*/
/*
* wijgrid_options.js
*/

(function ($) {
	module("wijgrid: options");
	var gridData = [
		[00, 01, 02],
		[10, 11, 12]
	];

	test("FlatView: with no column width set.", function () {
		// auto
		var settings = {
			data: gridData,
			scrollMode: "none",
			columns: [{}, { visible: false }, { visible: false}]
		}, $widget;

		try {
			$widget = createWidget(settings, $("<table style='width:100px;height:100px;'></table>"));
			ok($widget.width() === 100, "grid width is smaller than outer div, we need to expand its width.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("FixedView: with no column width set.", function () {
		// auto
		var settings = {
			data: gridData,
			scrollMode: "auto",
			columns: [{}, { visible: false }, { visible: false}]
		}, $widget;

		try {
			$widget = createWidget(settings, $("<table style='width:100px;height:50px;'></table>"));
			ok($widget.width() === 82, "grid width is smaller than outer div, we need to expand its width.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("FlatView: with ensureColumnsPxWidth and no column width set.", function () {
		// auto
		var settings = {
			data: gridData,
			scrollMode: "none",
			ensureColumnsPxWidth: true,
			columns: [{}, { visible: false }, { visible: false}]
		}, $widget;

		try {
			$widget = createWidget(settings, $("<table style='width:100px;height:100px;'></table>"));
			ok($widget.width() < 100, "grid width is smaller than outer div.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("FixedView: with ensureColumnsPxWidth and no column width set.", function () {
		// auto
		var settings = {
			data: gridData,
			scrollMode: "auto",
			ensureColumnsPxWidth: true,
			columns: [{}, { visible: false }, { visible: false}]
		}, $widget;

		try {
			$widget = createWidget(settings, $("<table style='width:100px;height:50px;'></table>"));
			ok($widget.width() < 82, "grid width is smaller than outer div, we need to expand its width.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("FlatView: with column width set.", function () {
		// auto
		var settings = {
				data: gridData,
				scrollMode: "none",
				columns: [{ width: 100}]
			},
			$widget, $outerDiv, $td;

		// px width
		try {
			$widget = createWidget(settings, $("<table style='width:300px;height:300px;'></table>"));
			ok($widget.width() === 300, "Grid width is smaller than outer div. grid is expanded.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth > 100, "Column width is set to 100px but client width is greater than 100.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:100px;height:50px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.width() > 100 && !$outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel"), "Grid width is bigger than outer div. Scrollbar isn't shown.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth < 100, "Column width is set to 100px but client width is smaller than 100.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// percentage width
		try {
			settings.columns = [{ width: "60%"}];
			$widget = createWidget(settings, $("<table style='width:300px;height:300px;'></table>"));
			ok($widget.width() === 300, "Grid width is smaller than outer div. grid is expanded.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth > 180, "Column width is set to 60% but client width is greater than 60%.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:100px;height:50px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.width() > 100 && !$outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel"), "Grid width is bigger than outer div. Scrollbar isn't shown.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth < 60, "Column width is set to 60% but client width is smaller than 60%.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("FlatView: with ensureColumnsPxWidth and column width set.", function () {
		// auto
		var settings = {
				data: gridData,
				scrollMode: "none",
				ensureColumnsPxWidth: true,
				columns: [{ width: 100}]
			},
			$widget, $outerDiv, $td;

		// px width
		try {
			$widget = createWidget(settings, $("<table style='width:300px;height:300px;'></table>"));
			ok($widget.width() < 300, "Grid width is smaller than outer div.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth === 100 || $td[0].offsetWidth === 101, "Column width is set to 100px.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:100px;height:50px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.width() > 100 && !$outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel"), "Grid width is bigger than outer div. Scrollbar isn't shown.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth === 100 || $td[0].offsetWidth === 101, "Column width is set to 100px.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// percentage width
		try {
			settings.columns = [{ width: "60%"}];
			$widget = createWidget(settings, $("<table style='width:300px;height:300px;'></table>"));
			ok($widget.width() < 300, "Grid width is smaller than outer div.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth === 180 || $td[0].offsetWidth === 181, "Column width is set to 60%.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:100px;height:50px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.width() > 100 && !$outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel"), "Grid width is bigger than outer div. Scrollbar isn't shown.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth === 60 || $td[0].offsetWidth === 61, "Column width is set to 60%.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("FlatView: with ensurePxWidth and column width set.", function () {
		// auto
		var settings = {
				data: gridData,
				scrollMode: "none",
				columns: [{ width: 100, ensurePxWidth: true}]
			},
		 	$widget, $outerDiv, $td;

		// px width
		try {
			$widget = createWidget(settings, $("<table style='width:300px;height:300px;'></table>"));
			ok($widget.width() === 300, "Grid width is smaller than outer div. grid is expanded.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth === 100 || $td[0].offsetWidth === 101, "Column width is set to 100px.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:100px;height:50px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.width() > 100 && !$outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel"), "Grid width is bigger than outer div. Scrollbar isn't shown.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth === 100 || $td[0].offsetWidth === 101, "Column width is set to 100px.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// percentage width
		try {
			settings.columns = [{ width: "60%", ensurePxWidth: true}];
			$widget = createWidget(settings, $("<table style='width:300px;height:300px;'></table>"));
			ok($widget.width() === 300, "Grid width is smaller than outer div. grid is expanded.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth === 180 || $td[0].offsetWidth === 181, "Column width is set to 60%.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:100px;height:50px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.width() > 100 && !$outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel"), "Grid width is bigger than outer div. Scrollbar isn't shown.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth === 60 || $td[0].offsetWidth === 61, "Column width is set to 60%.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	var gridData2 = [
		[00, 01, 02],
		[10, 11, 12],
		[20, 21, 22],
		[30, 31, 32],
		[40, 41, 42],
		[50, 51, 52],
		[60, 61, 62],
		[70, 71, 72]
	];

	test("Fixed View: column width", function () {
		var settings = {
				data: gridData2,
				scrollMode: "auto"
			},
			$widget, $outerDiv, sp, headerWrapper, $td;

		// pixel width
		try {
			settings.columns = [{ width: 200 }, { width: 100}];
			$widget = createWidget(settings, $("<table style='width:500px;height:100px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijgrid-scroller").data("wijmo-wijsuperpanel");
			ok(sp && !sp.hNeedScrollBar && sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and vertical scrollbar is visible.");
			headerWrapper = $outerDiv.find(".wijmo-wijgrid-split-area-ne");
			ok(sp.getContentElement().width() === headerWrapper.width(), "Width of top and bottom area are synchronized.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth > 200, "First column width is set to 200px but client width is greater than 200.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:nth-child(2)");
			ok($td[0].offsetWidth > 100, "Second column width is set to 100px but client width is greater than 100.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:100px;height:500px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijgrid-scroller").data("wijmo-wijsuperpanel");
			ok(sp && sp.hNeedScrollBar && !sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and horizontal scrollbar is visible.");
			headerWrapper = $outerDiv.find(".wijmo-wijgrid-split-area-ne");
			ok(sp.getContentElement().width() === headerWrapper.width(), "Width of top and bottom area are synchronized.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth < 200, "First column width is set to 200px but client width is smaller than 200.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:nth-child(2)");
			ok($td[0].offsetWidth < 100, "Second column width is set to 100px but client width is smaller than 100.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// percentage width
		try {
			settings.columns = [{ width: "20%" }, { width: "20%"}];
			$widget = createWidget(settings, $("<table style='width:500px;height:100px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijgrid-scroller").data("wijmo-wijsuperpanel");
			ok(sp && !sp.hNeedScrollBar && sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and vertical scrollbar is visible.");
			headerWrapper = $outerDiv.find(".wijmo-wijgrid-split-area-ne");
			ok(sp.getContentElement().width() === headerWrapper.width(), "Width of top and bottom area are synchronized.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth > 100, "First column width is set to 20% but client width is greater than 20%.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:nth-child(2)");
			ok($td[0].offsetWidth > 100, "Second column width is set to 20% but client width is greater than 20%.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:100px;height:500px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijgrid-scroller").data("wijmo-wijsuperpanel");
			ok(sp && sp.hNeedScrollBar && !sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and horizontal scrollbar is visible.");
			headerWrapper = $outerDiv.find(".wijmo-wijgrid-split-area-ne");
			ok(sp.getContentElement().width() === headerWrapper.width(), "Width of top and bottom area are synchronized.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth > 20, "First column width is set to 20% but client width is greater than 20%.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:nth-child(2)");
			ok($td[0].offsetWidth > 20, "Second column width is set to 20% but client width is greater than 20%.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Fixed View: ensureColumnsPxWidth and column width", function () {
		var settings = {
				data: gridData2,
				scrollMode: "auto",
				ensureColumnsPxWidth: true
			},
			$widget, $outerDiv, sp, headerWrapper, $td;

		// pixel width
		try {
			settings.columns = [{ width: 200 }, { width: 100}];
			$widget = createWidget(settings, $("<table style='width:500px;height:100px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijgrid-scroller").data("wijmo-wijsuperpanel");
			ok(sp && !sp.hNeedScrollBar && sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and vertical scrollbar is visible.");
			headerWrapper = $outerDiv.find(".wijmo-wijgrid-split-area-ne");
			ok(sp.getContentElement().width() !== headerWrapper.width(), "Width of top and bottom area are not synchronized.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth === 200 || $td[0].offsetWidth === 201, "First column width is set to 200px.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:nth-child(2)");
			ok($td[0].offsetWidth === 100 || $td[0].offsetWidth === 101, "Second column width is set to 100px.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:100px;height:500px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijgrid-scroller").data("wijmo-wijsuperpanel");
			ok(sp && sp.hNeedScrollBar && !sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and horizontal scrollbar is visible.");
			headerWrapper = $outerDiv.find(".wijmo-wijgrid-split-area-ne");
			ok(sp.getContentElement().width() === headerWrapper.width(), "Width of top and bottom area are synchronized.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth === 200 || $td[0].offsetWidth === 201, "First column width is set to 200px.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:nth-child(2)");
			ok($td[0].offsetWidth === 100 || $td[0].offsetWidth === 101, "Second column width is set to 100px.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// percentage width
		try {
			settings.columns = [{ width: "30%" }, { width: "30%"}];
			$widget = createWidget(settings, $("<table style='width:500px;height:100px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijgrid-scroller").data("wijmo-wijsuperpanel");
			ok(sp && !sp.hNeedScrollBar && sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and vertical scrollbar is visible.");
			headerWrapper = $outerDiv.find(".wijmo-wijgrid-split-area-ne");
			ok(sp.getContentElement().width() !== headerWrapper.width(), "Width of top and bottom area are not synchronized.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth >= 144 && $td[0].offsetWidth <= 146, "First column width is set to 30%.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:nth-child(2)");
			ok($td[0].offsetWidth >= 144 && $td[0].offsetWidth <= 146, "Second column width is set to 30%.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:100px;height:500px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijgrid-scroller").data("wijmo-wijsuperpanel");
			ok(sp && sp.hNeedScrollBar && !sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and horizontal scrollbar is visible.");
			headerWrapper = $outerDiv.find(".wijmo-wijgrid-split-area-ne");
			ok(sp.getContentElement().width() === headerWrapper.width(), "Width of top and bottom area are synchronized.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth === 30 || $td[0].offsetWidth === 31, "First column width is set to 30%.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:nth-child(2)");
			ok($td[0].offsetWidth === 30 || $td[0].offsetWidth === 31, "Second column width is set to 30%.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Fixed View: ensurePxWidth and column width", function () {
		var settings = {
				data: gridData2,
				scrollMode: "auto"
			},
			$widget, $outerDiv, sp, headerWrapper, $td;

		// pixel width
		try {
			settings.columns = [{ width: 200, ensurePxWidth: true }, { width: 100}];
			$widget = createWidget(settings, $("<table style='width:500px;height:100px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijgrid-scroller").data("wijmo-wijsuperpanel");
			ok(sp && !sp.hNeedScrollBar && sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and vertical scrollbar is visible.");
			headerWrapper = $outerDiv.find(".wijmo-wijgrid-split-area-ne");
			ok(sp.getContentElement().width() === headerWrapper.width(), "Width of top and bottom area are synchronized.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth === 200 || $td[0].offsetWidth === 201, "First column width is set to 200px.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:nth-child(2)");
			ok($td[0].offsetWidth > 100, "Second column width is set to 100px.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:100px;height:500px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijgrid-scroller").data("wijmo-wijsuperpanel");
			ok(sp && sp.hNeedScrollBar && !sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and horizontal scrollbar is visible.");
			headerWrapper = $outerDiv.find(".wijmo-wijgrid-split-area-ne");
			ok(sp.getContentElement().width() === headerWrapper.width(), "Width of top and bottom area are synchronized.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth === 200 || $td[0].offsetWidth === 201, "First column width is set to 200px.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:nth-child(2)");
			ok($td[0].offsetWidth < 100, "Second column width is set to 100px.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		// percentage width
		try {
			settings.columns = [{ width: "30%", ensurePxWidth: true }, { width: "30%"}];
			$widget = createWidget(settings, $("<table style='width:500px;height:100px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijgrid-scroller").data("wijmo-wijsuperpanel");
			ok(sp && !sp.hNeedScrollBar && sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and vertical scrollbar is visible.");
			headerWrapper = $outerDiv.find(".wijmo-wijgrid-split-area-ne");
			ok(sp.getContentElement().width() === headerWrapper.width(), "Width of top and bottom area are synchronized.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth === 150 || $td[0].offsetWidth === 151, "First column width is set to 30%.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:nth-child(2)");
			ok($td[0].offsetWidth > 150, "Second column width is set to 30%.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:100px;height:500px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			sp = $outerDiv.find(".wijmo-wijgrid-scroller").data("wijmo-wijsuperpanel");
			ok(sp && sp.hNeedScrollBar && !sp.vNeedScrollBar, "scrollMode is 'auto'. WijSuperPanel is created and horizontal scrollbar is visible.");
			headerWrapper = $outerDiv.find(".wijmo-wijgrid-split-area-ne");
			ok(sp.getContentElement().width() === headerWrapper.width(), "Width of top and bottom area are synchronized.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:first");
			ok($td[0].offsetWidth === 30 || $td[0].offsetWidth === 31, "First column width is set to 20%.");
			$td = $widget.find(".wijmo-wijgrid-datarow:first>.wijgridtd:nth-child(2)");
			ok($td[0].offsetWidth > 30, "Second column width is set to 20%.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Issue", function () {
		var settings = {
			data: [],
			columns: [{ headerText: "ID"}],
			scrollMode: "none"
		}, $widget, sp;

		try {
			$widget = createWidget(settings);
			ok($widget.width() > 0, "header is shown");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		//fixed view
		try {
			settings.scrollMode = "auto";
			$widget = createWidget(settings);
			ok($widget.width() > 0, "header is shown");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
			settings.scrollMode = "none";
		}
	});

} (jQuery));
