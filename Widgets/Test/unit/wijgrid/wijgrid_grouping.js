/*globals module,test,createWidget,jQuery,ok,document,same*/
/*
* wijgrid_grouping.js
*/

(function ($) {
	module("wijgrid: grouping");
	var gridData = [
		["10", "12", "24"],
		["20", "91", "46"],
		["20", "66", "25"],
		["10", "57", "83"],
		["20", "18", "74"]
	];

	test("Grouping: with sorting allowed.", function () {
		var settings = {
			allowSorting: true,
			data: gridData,
			columns: [
					{
						aggregate: "count",
						groupInfo: {
							position: "header",
							headerText: "{2}"
						}
					},
					{},
					{ dataType: "number", aggregate: "sum" }
				]
		},
			$widget, rows, row, cells;

		try {
			$widget = createWidget(settings);
			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(0)[0];
			ok(row.id === "GH0-1", "the first row is grouping header(0-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "2", "the number of the items is 2");
			ok(getTextContent(cells[1]) === "107.00", "the sum of the items is 107.00");
			row = rows.item(3)[0];
			ok(row.id === "GH1-1", "the fourth row is grouping header(1-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "3", "the number of the items is 3");
			ok(getTextContent(cells[1]) === "145.00", "the sum of the items is 145.00");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			settings.columns[0].sortDirection = "descending";
			settings.columns[0].groupInfo.position = "headerAndFooter";
			settings.columns[0].groupInfo.outlineMode = "startCollapsed";
			settings.columns[2].aggregate = "average";
			$widget = createWidget(settings);
			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(2)[0];
			ok($(row).is(":visible") === false, "the third row is invisible");
			row = rows.item(4)[0];
			ok(row.id === "GF0-1", "the fifth row is grouping footer(0-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "3", "the number of the items is 3");
			ok(getTextContent(cells[1]) === "48.33", "the average of the items is 48.33");
			row = rows.item(6)[0];
			ok($(row).is(":visible") === false, "the seventh row is invisible");
			row = rows.item(8)[0];
			ok(row.id === "GF1-1", "the ninth row is grouping footer(1-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "2", "the number of the items is 2");
			ok(getTextContent(cells[1]) === "53.50", "the average of the items is 53.50");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Grouping: with sorting not allowed.", function () {
		//judge whether the events are triggered or not.
		expect(10);

		var settings = {
			data: gridData,
			groupAggregate: function (e, args) {
				if (args.groupingStart === 0) {
					ok(args.groupingStart === 0 && args.groupingEnd === 1 && args.groupText === "10", "the arguments are corrent.");
					args.text = "test1";
				} else {
					ok(args.groupingStart === 3 && args.groupingEnd === 5 && args.groupText === "20", "the arguments are corrent.");
					args.text = "test2";
				}
			},
			groupText: function (e, args) {
				if (args.groupingStart === 0) {
					ok(args.isGroupHeader === true && args.aggregate === "test1", "the arguments are corrent.");
					args.text = "testa";
				} else {
					ok(args.isGroupHeader === true && args.aggregate === "test2", "the arguments are corrent.");
					args.text = "testb";
				}
			},
			columns: [
					{
						dataType: "number",
						rowMerge: "free",
						aggregate: "sum",
						groupInfo: {
							position: "footer",
							outlineMode: "startCollapsed",
							footerText: "{1}:{0}, SUM={2}"
						}
					},
					{},
					{ dataType: "number", aggregate: "max" }
				]
		},
		$widget, rows, row, cells;


		try {
			$widget = createWidget(settings);
			rows = $widget.data("wijmo-wijgrid")._rows();
			row = $(rows.item(0)[0]);
			ok(row.is(":visible") === true, "the first row is visible");

			row = $(rows.item(1)[0]);
			ok(row.hasClass("wijmo-wijgrid-datarow"), "the second row is a data row");

			row = $(rows.item(2)[0]);
			ok(row.attr("id") === "GF0-1", "the third row is grouping footer(0-1)");

			cells = row.find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "0:10.00,SUM=20.00", "the text of the footer is 0:10.00, SUM=20.00");
			ok(getTextContent(cells[1]) === "83.00", "the max of the items is 83.00");

			row = $(rows.item(3)[0]);
			ok(row.is(":visible") === true, "the third row is visible");
			ok(row.find("td")[0].rowSpan === 3, "the rowspan of the cell is 3");

			row = $(rows.item(6)[0]);
			ok(row.attr("id") === "GF1-1", "the sixth row is grouping footer(1-1)");

			cells = row.find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "0:20.00,SUM=60.00", "the text of the footer is 0:20.00, SUM=60.00");
			ok(getTextContent(cells[1]) === "74.00", "the max of the items is 74.00");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Grouping: with event set.", function () {
		//judge whether the events are triggered or not.
		expect(10);
		var settings = {
			allowSorting: true,
			data: gridData,
			groupAggregate: function (e, args) {
				if (args.groupingStart === 0) {
					ok(args.groupingStart === 0 && args.groupingEnd === 1 && args.groupText === "10", "the arguments are corrent.");
					args.text = "test1";
				} else {
					ok(args.groupingStart === 3 && args.groupingEnd === 5 && args.groupText === "20", "the arguments are corrent.");
					args.text = "test2";
				}
			},
			groupText: function (e, args) {
				if (args.groupingStart === 0) {
					ok(args.isGroupHeader === true && args.aggregate === "test1", "the arguments are corrent.");
					args.text = "testa";
				} else {
					ok(args.isGroupHeader === true && args.aggregate === "test2", "the arguments are corrent.");
					args.text = "testb";
				}
			},
			columns: [
				{
					aggregate: "custom",
					groupInfo: {
						position: "header",
						headerText: "custom"
					}
				},
				{},
				{ dataType: "number", aggregate: "sum" }
			]
		}, $widget, rows, row, cells;

		try {
			$widget = createWidget(settings);
			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(0)[0];
			ok(row.id === "GH0-1", "the first row is grouping header(0-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok($(cells[0]).find("span").text() === "testa", "the number of the items is testa");
			ok($(cells[1]).text() === "107.00", "the sum of the items is 107.00");
			row = rows.item(3)[0];
			ok(row.id === "GH1-1", "the fourth row is grouping header(1-1)");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok($(cells[0]).find("span").text() === "testb", "the number of the items is testb");
			ok($(cells[1]).text() === "145.00", "the sum of the items is 145.00");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Grouping: aggregation with footer.", function () {
		var settings = {
			allowSorting: true,
			data: gridData,
			showFooter: true,
			columns: [
				{
					aggregate: "count"
				},
				{},
				{ dataType: "number", aggregate: "sum" }
			]
		}, $widget, $row, cells;

		try {
			$widget = createWidget(settings);
			$row = $widget.find("tfoot tr");
			ok($row.hasClass("wijmo-wijgrid-footerrow"), "the row is the footer of the wijgrid");
			cells = $row.find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "5", "the number of the items is 5");
			ok(getTextContent(cells[2]) === "252.00", "the sum of the items is 252.00");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Grouping: the grouping of multi-columns.", function () {
		var settings = {
			allowSorting: true,
			data: [
				["10", "12", "25"],
				["20", "91", "25"],
				["20", "66", "25"],
				["10", "57", "35"],
				["20", "18", "35"]
			],
			columns: [
				{
					aggregate: "count",
					groupInfo: {
						position: "header",
						headerText: "{2}",
						outlineMode: "startCollapsed"
					}
				},
				{},
				{
					dataType: "number",
					aggregate: "sum",
					sortDirection: "descending",
					groupInfo: {
						position: "header",
						headerText: "{2}"
					}
				}
			]
		}, $widget, rows, row, cells;


		try {
			$widget = createWidget(settings);
			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(4)[0];
			ok($(row).is(":visible") === false, "the fifth row is invisible");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "10", "the value of the items is 10");
			ok(getTextContent(cells[2]) === "25.00", "the value of the items is 25.00");
			row = rows.item(5)[0];
			ok($(row).is(":visible") === true && row.id === "GH1-1", "the sixth row is visible and id is GH1-1");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "3", "the number of the items is 3");
			ok(getTextContent(cells[1]) === "85.00", "the sum of the items is 85.00");
			row = rows.item(8)[0];
			ok($(row).is(":visible") === false && row.id === "GH3-2", "the ninth row is invisible and id is GH3-2");
			cells = $(row).find(".wijmo-wijgrid-innercell");
			ok(getTextContent(cells[0]) === "50.00", "the sum of the items is 50.00");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Grouping: issue - the innermost group can't be expanded when more than 2 columns are grouped.", function () {
		// when more than 2 columns are grouped then the innermost group can't be expanded by the first click if all
		// groups were collapsed previously.

		var $widget;

		try {
			$widget = createWidget({
				data: [[0, 1, 2, 3]],
				columns: [
				{ groupInfo: { position: "header"} },
				{ groupInfo: { position: "header"} },
				{ groupInfo: { position: "header"} },
				{ groupInfo: { position: "header"} }
			]
			});

			var groupButtons = $widget.find(".wijmo-wijgrid-grouptogglebtn");

			$(groupButtons[0]).trigger("click"); // collapse all groups

			$(groupButtons[0]).trigger("click"); // expand 1st group
			$(groupButtons[1]).trigger("click"); // expand 2nd group
			$(groupButtons[2]).trigger("click"); // expand 3rd group
			$(groupButtons[3]).trigger("click"); // expand 4th group

			ok($widget.find("tr.wijmo-wijgrid-datarow:visible").is(":visible"), "innermost group is expanded");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Issue: child single-row group is hidden if parentGroup.outlineMode='startCollapsed' and parentGroup.groupSingleRow=false", function() {
    	var $widget;

		try {
			var sorted = false;

	        $widget = createWidget({
				data: [
					['val0', 0],
					['val0', 1],
					['val1', 2]
				],
				columns: [
					{
						headerText: "COL0",
						groupInfo: {
							outlineMode: "startCollapsed",
							position: "header",
							groupSingleRow: false
						}
					},
					{
						headerText: "COL1",
						groupInfo: {
						    position: "header"
						}
					}
				]
			});

			ok($widget.find("tbody tr:visible").length === 3, "OK");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

} (jQuery));
