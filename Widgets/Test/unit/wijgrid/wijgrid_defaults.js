commonWidgetTests('wijgrid', {
	defaults: {
		initSelector: ":jqmData(role='wijgrid')",

		wijMobileCSS: {
			header: "ui-header ui-bar-a",
			content: "ui-body-b",
			stateHover: "ui-btn-down-c",
			stateActive: "ui-btn-down-c",
			stateHighlight: "ui-btn-down-e"
		},

		disabled: false,
		_htmlTrimMethod: 2,

		create: null,
		allowColMoving: false,
		allowColSizing: false,
		columnsAutogenerationMode: "merge",
		allowKeyboardNavigation: true,
		keyActionTab: "moveAcross",
		allowPaging: false,
		allowSorting: false,
		allowEditing: false,
		allowVirtualScrolling: false,
		freezingMode: undefined,
		columns: [],
		editingInitOption: "auto",
		filterOperatorsSortMode: "alphabeticalCustomFirst",
		groupIndent: 10,
		highlightCurrentCell: false,
		highlightOnHover: true,
		pageIndex: 0,
		pageSize: 10,
		pagerSettings: {
			mode: "numeric",
			pageButtonCount: 10,
			position: "bottom"
		},
		readAttributesFromData: false,
		culture: "",
		cultureCalendar: "",
		calendar: null,
		data: null,

		staticColumnIndex: undefined,
		staticRowIndex: undefined,
		staticColumnsAlignment: undefined,

		customFilterOperators: [],
		loadingText: "Loading...",
		nullString: "",
		scrollMode: undefined,
		selectionMode: "singleRow",
		showFilter: false,
		showFooter: false,
		showRowHeader: false,
		showSelectionOnRender: true,
		totalRows: -1,
		rowHeight: undefined,
		editingMode: "none",

		showGroupArea: false,
		groupAreaCaption: "Drag a column here to group by that column.",

		scrollingSettings: {
			freezingMode: "none",
			mode: "none",
			staticColumnIndex: -1,
			staticRowIndex: -1,
			staticColumnsAlignment: "left",
			virtualizationSettings: {
				mode: "none",
				rowHeight: 19,
				columnWidth: 100
			}
		},

		beforeCellEdit: null,
		beforeCellUpdate: null,
		cellClicked: null,
		columnDragging: null,
		columnDragged: null,
		columnDropping: null,
		columnDropped: null,
		columnGrouping: null,
		columnGrouped: null,
		columnUngrouping: null,
		columnUngrouped: null,
		columnResizing: null,
		columnResized: null,
		afterCellUpdate: null,
		afterCellEdit: null,
		invalidCellValue: null,
		currentCellChanging: null,
		currentCellChanged: null,
		filtering: null,
		filtered: null,
		filterOperatorsListShowing: null,
		groupAggregate: null,
		groupText: null,
		pageIndexChanging: null,
		pageIndexChanged: null,
		selectionChanged: null,
		sorting: null,
		sorted: null,
		detailCreating: null,
		ensureColumnsPxWidth: false,

		/*ajaxError: null,*/
		loading: null,
		loaded: null,
		dataLoading: null,
		dataLoaded: null,
		rendering: null,
		rendered: null
	}
});