emptyString = String.fromCharCode(160);

function createWidget(settings, $element) {
	if ($element == null) {
		$element = $("<table></table>");
	}

	$element.prependTo(document.body);

	var $widget = $element.wijgrid(settings);


	return $widget;
}

function isElementInVisblePart(element, container) {
	var elementOffset = element.offset(),
		containerOffset = container.offset(),
		offsetLeft = elementOffset.left - containerOffset.left,
		offsetTop = elementOffset.top - containerOffset.top;

	if (offsetLeft >= -1 && offsetTop >= -1
		&& offsetLeft + element.outerWidth() <= container.outerWidth() + 1
		&& offsetTop + element.outerHeight() <= container.outerHeight() + 1) {
		return true;
	}

	return false;
}

function matchObj(a, b) {
	// arrays?
	if ($.isArray(a) && $.isArray(b)) {
		var flag = true;

		if (a.length === b.length) {
			for (var i = 0, len = a.length; i < len && flag; i++) {
				flag = matchObj(a[i], b[i]);
			}

			return flag;
		}

		return false;
	}

	// plain objects?
	if ($.isPlainObject(a) && $.isPlainObject(b)) {
		var aKeys = 0,
			bKeys = 0,
			key;

		for (key in a) {
			if (a.hasOwnProperty(key)) {
				aKeys++;
			}
		}

		for (key in b) {
			if (b.hasOwnProperty(key)) {
				bKeys++;
			}
		}

		if (aKeys === bKeys) {
			for (key in a) {
				if (a.hasOwnProperty(key) && b.hasOwnProperty(key)) {
					if (!matchObj(a[key], b[key])) {
						return false;
					}

				}
			}

			return true;
		}

		return false;
	}

	// otherwise
	if (a instanceof Date) {
		a = a.getTime();
	}

	if (b instanceof Date) {
		b = b.getTime();
	}

	return a === b;
}

function collectActualData($widget) {
	var result = [];

	var rowsLen = $widget.find("> tbody > tr").length;

	var cellsLen = $widget.data("wijmo-wijgrid")._visibleLeaves().length;
	if ($widget.wijgrid("option", "showRowHeader") === true) {
		cellLen--;
	}

	for (var row = 0; row < rowsLen; row++) {
		var foo = [];
		for (var cell = 0; cell < cellsLen; cell++) {
			foo.push($widget.wijgrid("currentCell", cell, row).value());
		}

		if (foo.length) {
			result.push(foo);
		}
	}

	return result;
}

function getTextContent(dom) {
    return (_getTextContent(dom) || "").replace(/\s/g, "");
}

function _getTextContent(dom) {
	if (dom) {
		if (dom.nodeType === 3) {
			return dom.nodeValue;
		} else {
			var result = "",
				i = 0,
				child;

			while (child = dom.childNodes[i++]) {
				result += _getTextContent(child);
			}

			return result;
		}
	}
}