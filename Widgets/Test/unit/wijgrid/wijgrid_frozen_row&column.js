/*globals module,test,createWidget,jQuery,ok,document,same*/
/*
* wijgrid_scrolling.js
*/

(function ($) {
	module("wijgrid: frozen row / column");
	var fBounds = $.wijmo.wijgrid.bounds,
		gridData = [
			[00, 01, 02, 03, 04],
			[10, 11, 12, 13, 14],
			[20, 21, 22, 23, 24],
			[30, 31, 32, 33, 34],
			[40, 41, 42, 43, 44],
			[50, 51, 52, 53, 54],
			[60, 61, 62, 63, 64],
			[70, 71, 72, 73, 74],
			[80, 81, 82, 83, 84],
			[90, 91, 92, 93, 94]
		];

	test("Frozen Column", function () {
		var settings = {
				data: gridData,
				scrollMode: "auto",
				staticColumnIndex: 0,
				freezingMode: "both",
				columns: [
					{ width: 80, ensurePxWidth: true },
					{ width: 80, visible: false, ensurePxWidth: true },
					{ width: 80, ensurePxWidth: true },
					{ width: 80, visible: false, ensurePxWidth: true },
					{ width: 80, ensurePxWidth: true }
				],
				rowStyleFormatter: function (args) {
					var $table;
					if (args.state === $.wijmo.wijgrid.renderState.rendering) {
						if (args.dataRowIndex === -1) {
							$table = args.$rows.parent().parent();
							$table.css("font-size", "12pt").addClass("height40");
						}
						else if (args.dataRowIndex === 0) {
							$table = args.$rows.parent().parent();
							$table.css("font-size", "12pt").addClass("height200");
						}
					}
				}
			},
			$widget, $outerDiv, $th, $splitter, size;

		try {
			$widget = createWidget(settings, $("<table style='width:200px;height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.wijgrid("option", "staticColumnIndex") === 0, "staticColumnIndex is 0");
			ok($widget.data("wijmo-wijgrid")._getRealStaticColumnIndex() === 0, "realStaticColumnIndex is 0");
			$th = $outerDiv.find("th:contains(0)");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-v");
			size = fBounds($th);
			ok(Math.abs(size.left + size.width - fBounds($splitter).left) <= 1, "position is right");

			$widget.wijgrid("option", "staticColumnIndex", -1);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.wijgrid("option", "staticColumnIndex") === -1, "staticColumnIndex is -1");
			ok($widget.data("wijmo-wijgrid")._getRealStaticColumnIndex() === -1, "realStaticColumnIndex is -1");
			$th = $outerDiv.find("th:contains(0)");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-v");
			size = fBounds($th);
			ok(Math.abs(size.left - fBounds($splitter).left) <= 1, "position is right");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			settings.showRowHeader = true;
			$widget = createWidget(settings, $("<table style='width:300px;height:300px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.wijgrid("option", "staticColumnIndex") === 0, "staticColumnIndex is 0");
			ok($widget.data("wijmo-wijgrid")._getRealStaticColumnIndex() === 1, "realStaticColumnIndex is 1");
			$th = $outerDiv.find("th:contains(0)");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-v");
			size = fBounds($th);
			ok(Math.abs(size.left + size.width - fBounds($splitter).left) <= 1, "position is right");

			$widget.wijgrid("option", "staticColumnIndex", 3);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.wijgrid("option", "staticColumnIndex") === 3, "staticColumnIndex is 3");
			ok($widget.data("wijmo-wijgrid")._getRealStaticColumnIndex() === 3, "realStaticColumnIndex is 3");
			$th = $outerDiv.find("th:contains(4)");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-v");
			size = fBounds($th);
			ok(Math.abs(size.left + size.width - fBounds($splitter).left) <= 1, "position is right");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
			settings.showRowHeader = false;
		}

		try {
			settings.showRowHeader = true;
			settings.staticColumnIndex = 2;

			$widget = createWidget(settings, $("<table style='width:200px;height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.wijgrid("option", "staticColumnIndex") === 2, "staticColumnIndex is 2");
			ok($widget.data("wijmo-wijgrid")._getRealStaticColumnIndex() === 3, "realStaticColumnIndex is 3");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-v");
			ok($splitter.length === 0, "splitter is hidden");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
			settings.showRowHeader = false;
		}
	});

	test("Frozen Column(UI)", function () {
		var settings = {
				data: gridData,
				scrollMode: "auto",
				staticColumnIndex: 0,
				freezingMode: "both",
				columns: [
					{ width: 80, ensurePxWidth: true },
					{ width: 80, visible: false, ensurePxWidth: true },
					{ width: 80, ensurePxWidth: true },
					{ width: 80, visible: false, ensurePxWidth: true },
					{ width: 80, ensurePxWidth: true }
				],
				rowStyleFormatter: function (args) {
					var $table;
					if (args.state === $.wijmo.wijgrid.renderState.rendering) {
						if (args.dataRowIndex === -1) {
							$table = args.$rows.parent().parent();
							$table.css("font-size", "12pt").addClass("height40");
						}
						else if (args.dataRowIndex === 0) {
							$table = args.$rows.parent().parent();
							$table.css("font-size", "12pt").addClass("height200");
						}
					}
				}
			},
			$widget, $outerDiv, $th, $splitter, size;

		try {
			$widget = createWidget(settings, $("<table style='width:200px;height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.wijgrid("option", "staticColumnIndex") === 0, "staticColumnIndex is 0");
			ok($widget.data("wijmo-wijgrid")._getRealStaticColumnIndex() === 0, "realStaticColumnIndex is 0");
			$th = $outerDiv.find("th:contains(0)");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-v");
			size = fBounds($th);
			ok(Math.abs(size.left + size.width - fBounds($splitter).left) <= 1, "position is right");

			$splitter/*$outerDiv.find(".wijmo-wijgrid-fixedview")*/.trigger({ type: "mousedown", which: 1, pageX: $splitter.offset().left, pageY: $splitter.offset().top + $splitter.height() / 2 });
			$(document).trigger({ type: "mousemove", button: 1, pageX: $splitter.offset().left - 70, pageY: $th.offset().top + $th.height() / 2 });
			$(document).trigger({ type: "mouseup" });
			ok($widget.wijgrid("option", "staticColumnIndex") === -1, "staticColumnIndex is -1");
			ok($widget.data("wijmo-wijgrid")._getRealStaticColumnIndex() === -1, "realStaticColumnIndex is -1");
			$th = $outerDiv.find("th:contains(0)");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-v");
			size = fBounds($th);
			ok(Math.abs(size.left - fBounds($splitter).left) <= 1, "position is right");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			settings.showRowHeader = true;
			$widget = createWidget(settings, $("<table style='width:300px;height:300px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.wijgrid("option", "staticColumnIndex") === 0, "staticColumnIndex is 0");
			ok($widget.data("wijmo-wijgrid")._getRealStaticColumnIndex() === 1, "realStaticColumnIndex is 1");
			$th = $outerDiv.find("th:contains(0)");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-v");
			size = fBounds($th);
			ok(Math.abs(size.left + size.width - fBounds($splitter).left) <= 1, "position is right");

			$splitter/*$outerDiv.find(".wijmo-wijgrid-fixedview")*/.trigger({ type: "mousedown", which: 1, pageX: $splitter.offset().left, pageY: $splitter.offset().top + $splitter.height() / 2 });
			$(document).trigger({ type: "mousemove", button: 1, pageX: $splitter.offset().left + 150, pageY: $th.offset().top + $th.height() / 2 });
			$(document).trigger({ type: "mouseup" });
			ok($widget.wijgrid("option", "staticColumnIndex") === 2, "staticColumnIndex is 2");
			ok($widget.data("wijmo-wijgrid")._getRealStaticColumnIndex() === 3, "realStaticColumnIndex is 3");
			$th = $outerDiv.find("th:contains(4)");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-v");
			size = fBounds($th);
			ok(Math.abs(size.left + size.width - fBounds($splitter).left) <= 1, "position is right");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
			settings.showRowHeader = false;
		}

		try {
			settings.showRowHeader = true;
			$widget = createWidget(settings, $("<table style='width:200px;height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			$th = $outerDiv.find("th:contains(0)");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-v");
			$splitter/*$outerDiv.find(".wijmo-wijgrid-fixedview")*/.trigger({ type: "mousedown", which: 1, pageX: $splitter.offset().left, pageY: $splitter.offset().top + $splitter.height() / 2 });
			$(document).trigger({ type: "mousemove", button: 1, pageX: $splitter.offset().left + 150, pageY: $th.offset().top + $th.height() / 2 });
			$(document).trigger({ type: "mouseup" });
			ok($widget.wijgrid("option", "staticColumnIndex") === 0, "staticColumnIndex is 0");
			ok($widget.data("wijmo-wijgrid")._getRealStaticColumnIndex() === 1, "realStaticColumnIndex is 1");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-v");
			ok($splitter.length === 1, "splitter is visible");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
			settings.showRowHeader = false;
		}
	});

	test("Frozen Row", function () {
		var settings = {
				data: gridData,
				scrollMode: "auto",
				staticRowIndex: 4,
				freezingMode: "both",
				columns: [
					{ width: 80, ensurePxWidth: true },
					{ width: 80, visible: false, ensurePxWidth: true },
					{ width: 80, ensurePxWidth: true },
					{ width: 80, visible: false, ensurePxWidth: true },
					{ width: 80, ensurePxWidth: true }
				],
				rowStyleFormatter: function (args) {
					var $table;
					if (args.state === $.wijmo.wijgrid.renderState.rendering) {
						if (args.dataRowIndex === -1) {
							$table = args.$rows.parent().parent();
							$table.css("font-size", "12pt").addClass("height40");
						}
						else if (args.dataRowIndex === 0) {
							$table = args.$rows.parent().parent();
							$table.css("font-size", "12pt").addClass("height200");
						}
					}
				}
			},
			$widget, $outerDiv, $tr, $splitter, size;

		try {
			$widget = createWidget(settings, $("<table style='width:200px;height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.wijgrid("option", "staticRowIndex") === 4, "staticRowIndex is 4");
			ok($widget.data("wijmo-wijgrid")._getRealStaticRowIndex() === 5, "realStaticRowIndex is 5");
			$tr = $outerDiv.find("tr:contains(40)");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-h");
			size = fBounds($tr);
			ok(Math.abs(size.top + size.height - fBounds($splitter).top) <= 1, "position is right");

			$widget.wijgrid("option", "staticRowIndex", -1);
			ok($widget.wijgrid("option", "staticRowIndex") === -1, "staticRowIndex is -1");
			ok($widget.data("wijmo-wijgrid")._getRealStaticRowIndex() === 0, "realStaticRowIndex is 0");
			$tr = $outerDiv.find("th:contains(0)");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-h");
			size = fBounds($tr);
			ok(Math.abs(size.top + size.height - fBounds($splitter).top) <= 1, "position is right");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:300px;height:300px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			$widget.wijgrid("option", "staticRowIndex", 11);
			ok($widget.wijgrid("option", "staticRowIndex") === 11, "staticRowIndex is 11");
			ok($widget.data("wijmo-wijgrid")._getRealStaticRowIndex() === 10, "realStaticRowIndex is 10");
			$tr = $outerDiv.find("tr:contains(90)");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-h");
			size = fBounds($tr);
			ok(Math.abs(size.top + size.height - fBounds($splitter).top) <= 1, "position is right");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			settings.staticRowIndex = 9;
			$widget = createWidget(settings, $("<table style='width:200px;height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.wijgrid("option", "staticRowIndex") === 9, "staticRowIndex is 9");
			ok($widget.data("wijmo-wijgrid")._getRealStaticRowIndex() === 10, "realStaticRowIndex is 10");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-h");
			ok($splitter.length === 0, "splitter is hidden");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Frozen Row(UI)", function () {
		var settings = {
				data: gridData,
				scrollMode: "auto",
				staticRowIndex: 4,
				freezingMode: "both",
				columns: [
					{ width: 80, ensurePxWidth: true },
					{ width: 80, visible: false, ensurePxWidth: true },
					{ width: 80, ensurePxWidth: true },
					{ width: 80, visible: false, ensurePxWidth: true },
					{ width: 80, ensurePxWidth: true }
				],
				rowStyleFormatter: function (args) {
					var $table;
					if (args.state === $.wijmo.wijgrid.renderState.rendering) {
						if (args.dataRowIndex === -1) {
							$table = args.$rows.parent().parent();
							$table.css("font-size", "12pt").addClass("height40");
						}
						else if (args.dataRowIndex === 0) {
							$table = args.$rows.parent().parent();
							$table.css("font-size", "12pt").addClass("height200");
						}
					}
				}
			},
			$widget, $outerDiv, $tr, $splitter, size;

		try {
			$widget = createWidget(settings, $("<table style='width:200px;height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.wijgrid("option", "staticRowIndex") === 4, "staticRowIndex is 4");
			ok($widget.data("wijmo-wijgrid")._getRealStaticRowIndex() === 5, "realStaticRowIndex is 5");
			$tr = $outerDiv.find("tr:contains(40)");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-h");
			size = fBounds($tr);
			ok(Math.abs(size.top + size.height - fBounds($splitter).top) <= 1, "position is right");

			$tr = $outerDiv.find("th:contains(0)");
			$splitter/*$outerDiv.find(".wijmo-wijgrid-fixedview")*/.trigger({ type: "mousedown", which: 1, pageX: $splitter.offset().left + $splitter.width() / 2, pageY: $splitter.offset().top });
			$(document).trigger({ type: "mousemove", button: 1, pageX: $splitter.offset().left + $splitter.width() / 2, pageY: $tr.offset().top + $tr.height() + 5 });
			$(document).trigger({ type: "mouseup" });
			ok($widget.wijgrid("option", "staticRowIndex") === -1, "staticRowIndex is -1");
			ok($widget.data("wijmo-wijgrid")._getRealStaticRowIndex() === 0, "realStaticRowIndex is 0");
			$tr = $outerDiv.find("th:contains(0)");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-h");
			size = fBounds($tr);
			ok(Math.abs(size.top + size.height - fBounds($splitter).top) <= 1, "position is right");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:300px;height:300px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-h");
			$tr = $outerDiv.find("tr:contains(90)");
			$splitter/*$outerDiv.find(".wijmo-wijgrid-fixedview")*/.trigger({ type: "mousedown", which: 1, pageX: $splitter.offset().left + $splitter.width() / 2, pageY: $splitter.offset().top });
			$(document).trigger({ type: "mousemove", button: 1, pageX: $splitter.offset().left + $splitter.width() / 2, pageY: $tr.offset().top + $tr.height() - 5 });
			$(document).trigger({ type: "mouseup" });
			ok($widget.wijgrid("option", "staticRowIndex") === 9, "staticRowIndex is 9");
			ok($widget.data("wijmo-wijgrid")._getRealStaticRowIndex() === 10, "realStaticRowIndex is 10");
			$tr = $outerDiv.find("tr:contains(90)");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-h");
			size = fBounds($tr);
			ok(Math.abs(size.top + size.height - fBounds($splitter).top) <= 1, "position is right");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		try {
			$widget = createWidget(settings, $("<table style='width:200px;height:200px;'></table>"));
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-h");
			$tr = $outerDiv.find("tr:contains(90)");
			$splitter/*$outerDiv.find(".wijmo-wijgrid-fixedview")*/.trigger({ type: "mousedown", which: 1, pageX: $splitter.offset().left + $splitter.width() / 2, pageY: $splitter.offset().top });
			$(document).trigger({ type: "mousemove", button: 1, pageX: $splitter.offset().left + $splitter.width() / 2, pageY: $tr.offset().top + $tr.height() - 5 });
			$(document).trigger({ type: "mouseup" });
			ok($widget.wijgrid("option", "staticRowIndex") === 4, "staticRowIndex is 4");
			ok($widget.data("wijmo-wijgrid")._getRealStaticRowIndex() === 5, "realStaticRowIndex is 5");
			$splitter = $outerDiv.find(".wijmo-wijgrid-freezing-handle-h");
			ok($splitter.length === 1, "splitter is visible");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});
} (jQuery));
