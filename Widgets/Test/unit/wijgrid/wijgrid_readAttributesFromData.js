(function ($) {
	module("wijgrid: \"readAttributesFromData\" option");

	var htmlTest = "<table>" +
		  "<tr><td class='myClass' style='text-align: right' colSpan='2' rowSpan='2'>00</td></tr>" +
		  "<tr><td class='myClass3'>20</td></tr>" +
		  "<tr><td class='myClass2' style='text-align: center' colSpan='2' rowSpan='2'>10</td></tr>" +
		"</table>";

	dataTest = [
		  [["00", { "class": "myClass", "style": "text-align: right"}]],
		  [["20", { "class": "myClass3"}]],
		  [["10", { "class": "myClass2", "style": "text-align: center"}]]
		];

	test("DOM table", function () {
		var $widget;

		try {
			$widget = createWidget({
				allowSorting: true,
				columns: [
		{ dataType: "number", sortDirection: "descending", filterOperator: "Less", filterValue: 20 },
	  ],
				readAttributesFromData: true
			}, $(htmlTest));

			testValues($widget);
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("array", function () {
		var $widget;

		try {
			$widget = createWidget({
				data: dataTest,
				allowSorting: true,
				columns: [
					{ dataType: "number", sortDirection: "descending", filterOperator: "Less", filterValue: 20 },
				],
				readAttributesFromData: true
			});

			testValues($widget);
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});


	function testValues($widget) {
		ok($widget.find("tr.wijmo-wijgrid-datarow").length === 2, "data rows number is correct.");

		// first cell
		var cell = $widget.wijgrid("currentCell", 0, 0);
		ok(cell.value() === 10, "1st cell: data value is correct.");

		var $cell = $(cell.tableCell());
		ok($cell.hasClass("myClass2") && $cell.css("text-align") === "center", "1st cell: attributes are correct.");
		ok($cell.prop("rowSpan") === 1 && $cell.prop("colSpan") === 1, "1st cell: span attributes are not set.");

		// second cell
		cell = $widget.wijgrid("currentCell", 0, 1);
		ok(cell.value() === 0, "2nd cell: data value is correct.");

		$cell = $(cell.tableCell());
		ok($cell.hasClass("myClass") && $cell.css("text-align") === "right", "2nd cell: attributes are correct.");

		ok($cell.prop("rowSpan") === 1 && $cell.prop("colSpan") === 1, "2st cell: span attributes are not set.");
	}

})(jQuery);