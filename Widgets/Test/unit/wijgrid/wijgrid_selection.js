(function ($) {
	module("wijgrid: selection");

	test("selection", function () {
		var $widget, flag;

		try {
			$widget = createWidget({
				data: [
					["00", "01", "02"],
					["10", "11", "12"],
					["20", "21", "22"]
				],
				selectionMode: "multiRange"
			});

			// -- 1
			$widget.wijgrid("selection").selectAll();
			equal($widget.wijgrid("selection").selectedCells().length(), 9, "multiRange: all cells were selected, the number of selected cells = 9");

			// -- 2
			flag = false;
			$.each(["00", "01", "02", "10", "11", "12", "20", "21", "22"], function (index, value) {
				flag = $widget.wijgrid("selection").selectedCells().item(index).value() === value;
			});
			ok(flag, "multiRange: selected cells have correct values");

			// -- 3
			$widget.wijgrid("selection").clear();
			equal($widget.wijgrid("selection").selectedCells().length(), 0, "multiRange: selection was cleared, the number of selected cells = 0");

			// -- 4
			$widget.wijgrid("selection").selectAll();
			equal($widget.wijgrid("selection").selectedCells().length(), 9, "multiRange: all cells were selected, the number of selected cells = 9");

			// -- 5
			$widget.wijgrid("option", "selectionMode", "singleColumn");
			equal($widget.wijgrid("selection").selectedCells().length(), 3, "selectionMode was changed to 'singleColumn', the number of selected cells = 3");

			// -- 6
			flag = false;
			$.each(["00", "10", "20"], function (index, value) {
				flag = $widget.wijgrid("selection").selectedCells().item(index).value() === value;
			});
			ok(flag, "singleColumn: selected cells has correct values");

			// -- 7
			$widget.wijgrid("option", "selectionMode", "singleCell");
			equal($widget.wijgrid("selection").selectedCells().length(), 1, "selectionMode was changed to 'singleCell', the number of selected cells = 1");

			// -- 8
			equal($widget.wijgrid("selection").selectedCells().item(0).value(), "00", "singleCell: selected cell has correct value");

			// -- 9
			$widget.wijgrid("option", "selectionMode", "singleRow");
			equal($widget.wijgrid("selection").selectedCells().length(), 3, "selectionMode was changed to 'singleRow', the number of selected cells = 3");

			// -- 10
			flag = false;
			$.each(["00", "01", "02"], function (index, value) {
				flag = $widget.wijgrid("selection").selectedCells().item(index).value() === value;
			});
			ok(flag, "singleRow: selected cells have correct values");

			// -- 11
			$widget.wijgrid("currentCell", 1, 1);
			equal($widget.wijgrid("selection").selectedCells().length(), 3, "singleRow: current cell was moved to (1;1), the number of selected cells = 3");

			// -- 12
			flag = false;
			$.each(["00", "01", "02"], function (index, value) {
				flag = $widget.wijgrid("selection").selectedCells().item(index).value() === value;
			});
			ok(flag, "singleRow: selected cells have correct values");

			// -- 13
			$widget.wijgrid("option", "selectionMode", "multiRange");
			$widget.wijgrid("selection").addRange(0, 0, 3, 1);
			equal($widget.wijgrid("selection").selectedCells().length(), 6, "selectionMode was changed to 'multiRange' and range ((0,0) (3,1) was added, the number of selected cells = 6");

			// -- 14
			$.each(["00", "01", "02", "10", "11", "12"], function (index, value) {
				flag = $widget.wijgrid("selection").selectedCells().item(index).value() === value;
			});
			ok(flag, "multiRange: selected cells have correct values");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});
})(jQuery);