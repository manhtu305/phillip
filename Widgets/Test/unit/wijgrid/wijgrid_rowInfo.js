(function ($) {
	module("wijgrid: row information");

	var data = [
		["a", "c"],
		["b", "d"],

		["c", "e"],
		["d", "f"],

		["e", "g"],
		["f", "h"]
	  ],
	  pageSize = 2,
	  pageIndex = 2;

	function testRowInfo($grid) {
		var dataOffset = pageIndex * pageSize,
		i = dataOffset,
		len = i + pageSize,
		currentCell, row, pfx;

		for (; i < len; i++) {
			pfx = "row = " + (i - dataOffset) + "(" + i + ")" + ": ";

			currentCell = $grid.wijgrid("currentCell", 0, i - dataOffset);
			ok((row = currentCell.row()) !== undefined, pfx + "row is OK");
			ok(row.data[0] === data[i][0] && row.data[1] === data[i][1], pfx + "data values are OK");
			// ok(row.dataItemIndex === i && row.dataRowIndex === i - dataOffset && row.virtualDataItemIndex === i, pfx + "data indicies are OK");
			ok(row.dataItemIndex === i - dataOffset && row.dataRowIndex === i - dataOffset, pfx + "data indicies are OK");
		}
	};

	test("flat mode", function () {
		var $widget;

		try {
			$widget = createWidget({
				data: data,
				allowPaging: true,
				pageSize: pageSize,
				pageIndex: pageIndex
			});

			testRowInfo($widget);
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("scroll mode", function () {
		var $widget;

		try {
			$widget = createWidget({
				data: data,
				allowPaging: true,
				pageSize: pageSize,
				pageIndex: pageIndex,
				scrollMode: "auto"
			});

			testRowInfo($widget);
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("scroll mode + fixed header", function () {
		var $widget;

		try {
			$widget = createWidget({
				data: data,
				allowPaging: true,
				pageSize: pageSize,
				pageIndex: pageIndex,
				scrollMode: "auto",
				staticRowIndex: 0
			});

			testRowInfo($widget);
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});
})(jQuery);