/*globals module,test,createWidget,jQuery,ok,document,same*/
/*
* wijgrid_editing.js
*/

(function ($) {
	module("wijgrid: editing");
	var gridData = [
		[00, 01, true],
		[10, 11, false],
		[20, 21, false],
		[30, 31, true],
		[40, 41, true],
		[50, 51, true],
		[60, 61, false],
		[70, 71, false],
		[80, 81, false],
		[90, 91, true]
	];

	test("Editing: test editing input(ui).", function () {
		var settings = {
			allowEditing: true,
			data: $.extend(true, [], gridData),
			showRowHeader: true,
			selectionMode: "none",
			columns: [
				{ dataType: "number" },
				{ dataType: "currency" },
				{ dataType: "boolean" }
			]
		}, $widget, rows, row, cells, cell;

		try {
			$widget = createWidget(settings);
			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(3)[0];
			cells = row.cells;
			cell = cells[2];
			ok($(cell).find("div").text() === "$31.00", "check display value before editing");
			ok($widget.data("wijmo-wijgrid").data()[3][1] === 31, "check real value before editing");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 0, "check pencil mark");
			$(cell).trigger("click");
			$(cell).trigger("dblclick");
			ok($(cell).find("input").val() === "31", "check value while editing");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 1, "check pencil mark");
			$(cell).find("input").val("310");
			$(cells[1]).trigger("click");
			ok($(cell).find("div").text() === "$310.00", "check value after editing");
			ok($widget.data("wijmo-wijgrid").data()[3][1] === 310, "check real value after editing");

			ok($(cells[0]).find("div.ui-icon-pencil").length === 0, "check pencil mark");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Editing: test editing input(method).", function () {
		var settings = {
			allowEditing: true,
			data: $.extend(true, [], gridData),
			showRowHeader: true,
			selectionMode: "none",
			columns: [
				{ dataType: "number" },
				{ dataType: "currency" },
				{ dataType: "boolean" }
			]
		}, $widget, rows, row, cells, cell;

		try {
			$widget = createWidget(settings);
			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(3)[0];
			cells = row.cells;
			cell = cells[2];
			ok($(cell).find("div").text() === "$31.00", "check display value before editing");
			ok($widget.data("wijmo-wijgrid").data()[3][1] === 31, "check real value before editing");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 0, "check pencil mark");
			$widget.wijgrid("currentCell", 1, 3);
			$widget.wijgrid("beginEdit");
			ok($(cell).find("input").val() === "31", "check value while editing");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 1, "check pencil mark");
			$(cell).find("input").val("310");
			$widget.wijgrid("endEdit");
			ok($(cell).find("div").text() === "$310.00", "check value after editing");
			ok($widget.data("wijmo-wijgrid").data()[3][1] === 310, "check real value after editing");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 0, "check pencil mark");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Editing: test editing checkbox(ui).", function () {
		var settings = {
			allowEditing: true,
			data: $.extend(true, [], gridData),
			showRowHeader: true,
			selectionMode: "none",
			columns: [
				{ dataType: "number" },
				{ dataType: "currency" },
				{ dataType: "boolean" }
			]
		}, $widget, rows, row, cells, cell;

		//click other part except checkbox
		try {
			$widget = createWidget(settings);
			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(6)[0];
			cells = row.cells;
			cell = cells[3];
			ok($(cell).find("input").not(":checked"), "check display value before editing");
			ok($widget.data("wijmo-wijgrid").data()[6][2] === false, "check real value before editing");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 0, "check pencil mark");
			$(cell).trigger("click");
			$(cell).trigger("dblclick");
			ok($(cell).find("input").not(":checked"), "check value while editing");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 1, "check pencil mark");
			$(cell).find("input").attr("checked", "checked");
			$(cells[1]).trigger("click");
			ok($(cell).find("input").is(":checked"), "check value after editing");
			ok($widget.data("wijmo-wijgrid").data()[6][2] === true, "check real value after editing");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 0, "check pencil mark");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		//click checkbox
		try {
			settings.data = $.extend(true, [], gridData);
			$widget = createWidget(settings);
			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(6)[0];
			cells = row.cells;
			cell = cells[3];
			ok($(cell).find("input").not(":checked"), "check display value before editing");
			ok($widget.data("wijmo-wijgrid").data()[6][2] === false, "check real value before editing");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 0, "check pencil mark");
			$(cell).find("input").trigger("mousedown");
			ok($(cell).find("input").not(":checked"), "check value while editing");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 1, "check pencil mark");
			$(cell).find("input").trigger("click");
			$(cells[1]).trigger("click");
			ok($(cell).find("input").is(":checked"), "check value after editing");
			ok($widget.data("wijmo-wijgrid").data()[6][2] === true, "check real value after editing");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 0, "check pencil mark");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Editing: test editing checkbox(method).", function () {
		var settings = {
			allowEditing: true,
			data: $.extend(true, [], gridData),
			showRowHeader: true,
			selectionMode: "none",
			columns: [
				{ dataType: "number" },
				{ dataType: "currency" },
				{ dataType: "boolean" }
			]
		}, $widget, rows, row, cells, cell;

		try {
			$widget = createWidget(settings);
			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(6)[0];
			cells = row.cells;
			cell = cells[3];
			ok($(cell).find("input").not(":checked"), "check display value before editing");
			ok($widget.data("wijmo-wijgrid").data()[6][2] === false, "check real value before editing");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 0, "check pencil mark");
			$widget.wijgrid("currentCell", 2, 6);
			$widget.wijgrid("beginEdit");
			ok($(cell).find("input").not(":checked"), "check value while editing");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 1, "check pencil mark");
			$(cell).find("input").attr("checked", "checked");
			$widget.wijgrid("endEdit");
			ok($(cell).find("input").is(":checked"), "check value after editing");
			ok($widget.data("wijmo-wijgrid").data()[6][2] === true, "check real value after editing");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 0, "check pencil mark");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Editing: test editing limit.", function () {
		//assure the event triggered
		expect(6);
		var settings = {
			allowEditing: true,
			data: $.extend(true, [], gridData),
			showRowHeader: true,
			selectionMode: "none",
			invalidCellValue: function (e, args) {
				ok(args.value === null, "check the value when the value is invalid");
			},
			columns: [
				{ dataType: "number", valueRequired: true },
				{ dataType: "currency", readOnly: true },
				{ dataType: "boolean" }
			]
		}, $widget, rows, row, cells, cell;

		try {
			$widget = createWidget(settings);
			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(3)[0];
			cells = row.cells;
			//readOnly
			cell = cells[2];
			ok($(cells[0]).find("div.ui-icon-pencil").length === 0, "check pencil mark");
			$(cell).trigger("click");
			$(cell).trigger("dblclick");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 0, "check pencil mark");
			//valueRequired
			cell = cells[1];
			$(cell).trigger("click");
			$(cell).trigger("dblclick");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 1, "check pencil mark");
			$(cell).find("input").val("");
			$(cells[2]).trigger("click");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 1, "check pencil mark");
			$(cell).find("input").val("300");
			$(cells[2]).trigger("click");
			ok($(cells[0]).find("div.ui-icon-pencil").length === 0, "check pencil mark");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Editing: test editing events(sequence-value is not changed).", function () {
		expect(7);
		var settings = {
			allowEditing: true,
			data: $.extend(true, [], gridData),
			showRowHeader: true,
			selectionMode: "none",
			beforeCellEdit: function (e, args) {
				ok(eCount1 === 1 && eCount2 === 0, "check the count");
				ok(args.cell.value() === 31, "check the value");
			},
			beforeCellUpdate: function (e, args) {
				eCount2++;
				ok(eCount1 === 2 && eCount2 === 1, "check the count");
				ok(args.cell.value() === 31, "check the value");
			},
			afterCellEdit: function (e, args) {
				eCount2++;
				ok(eCount1 === 2 && eCount2 === 2, "check the count");
				ok(args.cell.value() === 31, "check the value");
			},
			afterCellUpdate: function (e, args) {
				eCount2++;
				ok(false, "should not be triggered");
			},
			columns: [
				{ dataType: "number" },
				{ dataType: "currency" },
				{ dataType: "boolean" }
			]
		}, $widget, eCount1 = 0, eCount2 = 0;

		try {
			$widget = createWidget(settings);
			$widget.wijgrid("currentCell", 1, 3);
			eCount1++;
			$widget.wijgrid("beginEdit");
			ok($($widget.data("wijmo-wijgrid")._rows().item(3)[0].cells[0]).find("div.ui-icon-pencil").length === 1);
			eCount1++;
			$widget.wijgrid("endEdit");
			eCount1++;
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Editing: test editing events(sequence-value is changed).", function () {
		expect(9);
		var settings = {
			allowEditing: true,
			data: $.extend(true, [], gridData),
			showRowHeader: true,
			selectionMode: "none",
			beforeCellEdit: function (e, args) {
				ok(eCount1 === 1 && eCount2 === 0, "check the count");
				ok(args.cell.value() === 31, "check the value");
			},
			beforeCellUpdate: function (e, args) {
				eCount2++;
				ok(eCount1 === 2 && eCount2 === 1, "check the count");
				ok(args.cell.value() === 31, "check the value");
			},
			afterCellEdit: function (e, args) {
				eCount2++;
				ok(eCount1 === 2 && eCount2 === 3, "check the count");
				ok(args.cell.value() === 310, "check the value");
			},
			afterCellUpdate: function (e, args) {
				eCount2++;
				ok(eCount1 === 2 && eCount2 === 2, "check the count");
				ok(args.cell.value() === 310, "check the value");
			},
			columns: [
				{ dataType: "number" },
				{ dataType: "currency" },
				{ dataType: "boolean" }
			]
		}, $widget, eCount1 = 0, eCount2 = 0;

		try {
			$widget = createWidget(settings);
			$widget.wijgrid("currentCell", 1, 3);
			eCount1++;
			$widget.wijgrid("beginEdit");
			ok($($widget.data("wijmo-wijgrid")._rows().item(3)[0].cells[0]).find("div.ui-icon-pencil").length === 1);
			$($widget.data("wijmo-wijgrid")._rows().item(3)[0].cells[2]).find("input").val("310");
			eCount1++;
			$widget.wijgrid("endEdit");
			eCount1++;
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Editing: test editing events(sequence-value is changed by method).", function () {
		expect(10);
		var settings = {
			allowEditing: true,
			data: $.extend(true, [], gridData),
			showRowHeader: true,
			selectionMode: "none",
			beforeCellEdit: function (e, args) {
				ok(eCount1 === 1 && eCount2 === 0, "check the count");
				ok(args.cell.value() === 31, "check the value");
			},
			beforeCellUpdate: function (e, args) {
				eCount2++;
				ok(eCount1 === 2 && eCount2 === 1, "check the count");
				ok(args.cell.value() === 31, "check the value");
				args.value = 310;
			},
			afterCellEdit: function (e, args) {
				eCount2++;
				ok(eCount1 === 2 && eCount2 === 3, "check the count");
				ok(args.cell.value() === 310, "check the value");
			},
			afterCellUpdate: function (e, args) {
				eCount2++;
				ok(eCount1 === 2 && eCount2 === 2, "check the count");
				ok(args.cell.value() === 310, "check the value");
			},
			columns: [
				{ dataType: "number" },
				{ dataType: "currency" },
				{ dataType: "boolean" }
			]
		}, $widget, eCount1 = 0, eCount2 = 0;

		try {
			$widget = createWidget(settings);
			$widget.wijgrid("currentCell", 1, 3);
			eCount1++;
			$widget.wijgrid("beginEdit");
			ok($($widget.data("wijmo-wijgrid")._rows().item(3)[0].cells[0]).find("div.ui-icon-pencil").length === 1);
			eCount1++;
			$widget.wijgrid("endEdit");
			eCount1++;
			ok($($widget.data("wijmo-wijgrid")._rows().item(3)[0].cells[2]).find("div").text() === "$310.00", "check the html value");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Editing: test editing events(sequence-edit cancel).", function () {
		expect(2);
		var settings = {
			allowEditing: true,
			data: $.extend(true, [], gridData),
			showRowHeader: true,
			selectionMode: "none",
			beforeCellEdit: function (e, args) {
				ok(eCount1 === 1 && eCount2 === 0, "check the count");
				return false;
			},
			beforeCellUpdate: function (e, args) {
				eCount2++;
				ok(false, "should not be triggered");
			},
			afterCellEdit: function (e, args) {
				eCount2++;
				ok(false, "should not be triggered");
			},
			afterCellUpdate: function (e, args) {
				eCount2++;
				ok(false, "should not be triggered");
			},
			columns: [
				{ dataType: "number" },
				{ dataType: "currency" },
				{ dataType: "boolean" }
			]
		}, $widget, eCount1 = 0, eCount2 = 0;

		try {
			$widget = createWidget(settings);
			$widget.wijgrid("currentCell", 1, 3);
			eCount1++;
			$widget.wijgrid("beginEdit");
			ok($($widget.data("wijmo-wijgrid")._rows().item(3)[0].cells[0]).find("div.ui-icon-pencil").length === 0);
			eCount1++;
			$widget.wijgrid("endEdit");
			eCount1++;
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Editing: test editing events(sequence-update cancel).", function () {
		expect(4);
		var settings = {
			allowEditing: true,
			data: $.extend(true, [], gridData),
			showRowHeader: true,
			selectionMode: "none",
			beforeCellEdit: function (e, args) {
				ok(eCount1 === 1 && eCount2 === 0, "check the count");
			},
			beforeCellUpdate: function (e, args) {
				eCount2++;
				ok(eCount1 === 2 && eCount2 === 1, "check the count");
				return false;
			},
			afterCellEdit: function (e, args) {
				eCount2++;
				ok(false, "should not be triggered");
			},
			afterCellUpdate: function (e, args) {
				eCount2++;
				ok(false, "should not be triggered");
			},
			columns: [
				{ dataType: "number" },
				{ dataType: "currency" },
				{ dataType: "boolean" }
			]
		}, $widget, eCount1 = 0, eCount2 = 0;

		try {
			$widget = createWidget(settings);
			$widget.wijgrid("currentCell", 1, 3);
			eCount1++;
			$widget.wijgrid("beginEdit");
			ok($($widget.data("wijmo-wijgrid")._rows().item(3)[0].cells[0]).find("div.ui-icon-pencil").length === 1);
			$($widget.data("wijmo-wijgrid")._rows().item(3)[0].cells[2]).find("input").val("310");
			eCount1++;
			$widget.wijgrid("endEdit");
			ok($($widget.data("wijmo-wijgrid")._rows().item(3)[0].cells[0]).find("div.ui-icon-pencil").length === 1);
			eCount1++;
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Editing: test editing input(method).", function () {
		var data = $.extend(true, [], gridData),

			settings = {
				allowEditing: true,
				data: data,
			},

			$widget, cc;

		try {
			$widget = createWidget(settings);

			cc = $widget.wijgrid("currentCell", 1, 3);
			$widget.wijgrid("beginEdit");
			$(cc.tableCell()).find("input").val("310");
			$widget.wijgrid("endEdit");


			ok(data[3][1] == "310", "check underlying datasource");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Editing: test endEdit(true).", function () {
		var data = $.extend(true, [], gridData),

			settings = {
				allowEditing: true,
				data: data,
			},

			$widget, cc;

		try {
			$widget = createWidget(settings);

			cc = $widget.wijgrid("currentCell", 0, 0);
			$widget.wijgrid("beginEdit");
			$(cc.tableCell()).find("input").val("310");
			$widget.wijgrid("endEdit", true);


			ok(data[0][0] == "0", "check underlying datasource");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

} (jQuery));