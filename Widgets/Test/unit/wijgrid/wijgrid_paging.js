/*globals module,test,createWidget,jQuery,ok,document,same*/
/*
* wijgrid_paging.js
*/

(function ($) {
	module("wijgrid: paging");
	var gridData = [
		[00, 01, 02],
		[10, 11, 12],
		[20, 21, 22],
		[30, 31, 32],
		[40, 41, 42],
		[50, 51, 52],
		[60, 61, 62],
		[70, 71, 72],
		[80, 81, 82],
		[90, 91, 92],
		[100, 101, 102],
		[110, 111, 112],
		[120, 121, 122],
		[130, 131, 132],
		[140, 141, 142],
		[150, 151, 152],
		[160, 161, 162],
		[170, 171, 172],
		[180, 181, 182],
		[190, 191, 192],
		[200, 201, 302],
		[210, 211, 312],
		[220, 221, 322],
		[230, 231, 332],
		[240, 241, 342],
		[250, 251, 352],
		[260, 261, 362],
		[270, 271, 372],
		[280, 281, 382],
		[290, 291, 392],
		[300, 301, 302],
		[310, 311, 312],
		[320, 321, 322],
		[330, 331, 332],
		[340, 341, 342],
		[350, 351, 352],
		[360, 361, 362],
		[370, 371, 372],
		[380, 381, 382],
		[390, 391, 392],
		[400, 401, 402],
		[410, 411, 412],
		[420, 421, 422],
		[430, 431, 432],
		[440, 441, 442],
		[450, 451, 452],
		[460, 461, 462],
		[470, 471, 472],
		[480, 481, 482],
		[490, 491, 492],
		[500, 501, 502],
		[510, 511, 512],
		[520, 521, 522],
		[530, 531, 532],
		[540, 541, 542],
		[550, 551, 552],
		[560, 561, 562],
		[570, 571, 572],
		[580, 581, 582],
		[590, 591, 592]
	];

	test("Paging: test options.", function () {
		var settings = {
				allowPaging: true,
				data: gridData,
				pageSize: 7
			},
			$widget, $outerDiv, lis;

		try {
			$widget = createWidget(settings);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.wijgrid("option", "pageIndex") === 0 && $widget.wijgrid("pageCount") === 9, "the options are correct.");
			ok($widget.find("tr.wijmo-wijgrid-row").length === 7, "the page size is 7");
			lis = $outerDiv.find(".wijmo-wijpager").find("li");
			ok($(lis[0]).find("span").text() === "1", "the current index of page is 1");
			ok($(lis[1]).find("a").text() === "2", "another index of page is a link(2)");
			ok(lis.length === 9, "the number of page indexes is 9");

			//change the pageIndex
			$widget.wijgrid("option", "pageIndex", 3);
			ok($widget.wijgrid("option", "pageIndex") === 3 && $widget.wijgrid("pageCount") === 9, "the options are correct.");
			lis = $outerDiv.find(".wijmo-wijpager").find("li");
			ok($(lis[0]).find("a").text() === "1", "another index of page is a link(1)");
			ok($(lis[3]).find("span").text() === "4", "the index of page is 4");
			ok(lis.length === 9, "the number of page indexes is 9");

			//change the pageSize
			$widget.wijgrid("option", "pageSize", 5);
			ok($widget.wijgrid("option", "pageIndex") === 0 && $widget.wijgrid("pageCount") === 12, "the options are correct.");
			ok($widget.find("tr.wijmo-wijgrid-row").length === 5, "the page size is 5");
			lis = $outerDiv.find(".wijmo-wijpager").find("li");
			ok($(lis[0]).find("span").text() === "1", "the index of page is 1");
			ok($(lis[1]).find("a").text() === "2", "another index of page is a link(2)");
			ok($(lis[10]).find("a").text() === "...", "another index of page is a link(...)");
			ok(lis.length === 11, "the number of page indexes is 11");
			//change the pageIndex

			$widget.wijgrid("option", "pageIndex", 10);
			ok($widget.wijgrid("option", "pageIndex") === 10 && $widget.wijgrid("pageCount") === 12, "the options are correct.");
			lis = $outerDiv.find(".wijmo-wijpager").find("li");
			ok($(lis[0]).find("a").text() === "...", "another index of page is a link(...)");
			ok($(lis[1]).find("a").text() === "3", "another index of page is a link(3)");
			ok($(lis[9]).find("span").text() === "11", "the index of page is 11");
			ok(lis.length === 11, "the number of page indexes is 11");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Paging: with pagerSettings set.", function () {
		var settings = {
				allowPaging: true,
				data: gridData,
				pagerSettings: {
					mode: "nextPreviousFirstLast",
					firstPageText: "1st",
					lastPageText: "lst",
					position: "top"
				}
			},
			$widget, $outerDiv, lis;

		try {
			$widget = createWidget(settings);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			lis = $outerDiv.find(".wijmo-wijpager").find("li");
			ok($(lis[0]).find("span").text() === "Next", "the first index of page is a link(Next)");
			ok($(lis[1]).find("span").text() === "lst", "the second index of page is a link(lst)");
			ok(lis.length === 2, "the number of page indexes is 2");

			//change the pageIndex
			$widget.wijgrid("option", "pageIndex", 3);
			lis = $outerDiv.find(".wijmo-wijpager").find("li");
			ok($(lis[0]).find("span").text() === "1st", "the first index of page is a link(1st)");
			ok($(lis[1]).find("span").text() === "Previous", "the second index of page is a link(Previous)");
			ok($(lis[2]).find("span").text() === "Next", "the third index of page is a link(Next)");
			ok($(lis[3]).find("span").text() === "lst", "the fourth index of page is a link(lst)");
			ok(lis.length === 4, "the number of page indexes is 4");

			//change the pageIndex
			$widget.wijgrid("option", "pageIndex", 5);
			lis = $outerDiv.find(".wijmo-wijpager").find("li");
			ok($(lis[0]).find("span").text() === "1st", "the first index of page is a link(1st)");
			ok($(lis[1]).find("span").text() === "Previous", "the second index of page is a link(Previous)");
			ok(lis.length === 2, "the number of page indexes is 2");

			//ui operation
			$(lis[0]).find("span").trigger("click");
			lis = $outerDiv.find(".wijmo-wijpager").find("li");
			ok($(lis[0]).find("span").text() === "Next", "the first index of page is a link(Next)");
			ok($(lis[1]).find("span").text() === "lst", "the second index of page is a link(lst)");
			ok(lis.length === 2, "the number of page indexes is 2");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Paging: with event set.", function () {
		//judge whether the event is triggered or not
		expect(19);
		var settings = {
				allowPaging: true,
				data: gridData,
				pagerSettings: {
					mode: "nextPreviousFirstLast"
				},
				pageIndexChanging: function (e, args) {
					if (args.newPageIndex === 5) {
						ok(true, "the second changing event is triggered.");
						return false;
					}
					ok(true, "the first changing event is triggered.");
				},
				pageIndexChanged: function (e, args) {
					ok(true, "the first changed event is triggered.");
				}
			},
			$widget, $outerDiv, lis;

		try {
			$widget = createWidget(settings);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			ok($widget.wijgrid("option", "pageIndex") === 0 && $widget.wijgrid("pageCount") === 6, "the options are correct.");
			lis = $outerDiv.find(".wijmo-wijpager").find("li");
			ok($(lis[0]).find("span").text() === "Next", "the first index of page is a link(Next)");
			ok($(lis[1]).find("span").text() === "Last", "the second index of page is a link(lst)");
			ok(lis.length === 2, "the number of page indexes is 2");

			//change the pageIndex
			$widget.wijgrid("option", "pageIndex", 3);
			ok($widget.wijgrid("option", "pageIndex") === 3 && $widget.wijgrid("pageCount") === 6, "the options are correct.");
			lis = $outerDiv.find(".wijmo-wijpager").find("li");
			ok($(lis[0]).find("span").text() === "First", "the first index of page is a link(1st)");
			ok($(lis[1]).find("span").text() === "Previous", "the second index of page is a link(Previous)");
			ok($(lis[2]).find("span").text() === "Next", "the third index of page is a link(Next)");
			ok($(lis[3]).find("span").text() === "Last", "the fourth index of page is a link(lst)");
			ok(lis.length === 4, "the number of page indexes is 4");

			//change the pageIndex
			$widget.wijgrid("option", "pageIndex", 5);
			ok($widget.wijgrid("option", "pageIndex") === 3 && $widget.wijgrid("pageCount") === 6, "the options are correct.");
			lis = $outerDiv.find(".wijmo-wijpager").find("li");
			ok($(lis[0]).find("span").text() === "First", "the first index of page is a link(1st)");
			ok($(lis[1]).find("span").text() === "Previous", "the second index of page is a link(Previous)");
			ok($(lis[2]).find("span").text() === "Next", "the third index of page is a link(Next)");
			ok($(lis[3]).find("span").text() === "Last", "the fourth index of page is a link(lst)");
			ok(lis.length === 4, "the number of page indexes is 4");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});
} (jQuery));
