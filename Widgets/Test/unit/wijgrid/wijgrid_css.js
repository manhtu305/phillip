(function ($) {
	module("wijgrid: core");

	var data = [
		[0, "a"],
		[1, "c"],
   ];

   var refClasses = [
		".wijmo-wijgrid-headerrow",
		".wijmo-wijgrid-filterrow",
		".wijmo-wijgrid-row.ui-widget-content.wijmo-wijgrid-groupheaderrow",
		".wijmo-wijgrid-row.ui-widget-content.wijmo-wijgrid-datarow",
		".wijmo-wijgrid-row.ui-widget-content.wijmo-wijgrid-groupfooterrow",
		".wijmo-wijgrid-row.ui-widget-content.wijmo-wijgrid-groupheaderrow",
		".wijmo-wijgrid-row.ui-widget-content.wijmo-wijgrid-datarow.wijmo-wijgrid-alternatingrow",
		".wijmo-wijgrid-row.ui-widget-content.wijmo-wijgrid-groupfooterrow",
		".wijmo-wijgrid-footerrow"
   ];
	
	test("rows css", function () {
		var $widget;

		try {
			$widget = $("<table></table>")
				.appendTo(document.body);

			$widget
				.wijgrid({
					data: data,
               showFilter: true,
					showFooter: true,
					columns: [
						{
							groupInfo: {
								position: "headerAndFooter"
							}
						}
					]
				 });

			 var view = $widget.data("wijmo-wijgrid")._view(),
				 allRows = view.rows();

			 ok(allRows.length() === 9, "rows number is OK");

			 for (var i = 0; i < allRows.length(); i++) {
			    var rowObj = allRows.item(i)
					  rowInfo = view._getRowInfo(rowObj);

				 ok(rowInfo.$rows.is(refClasses[i]), "<row[" + i + "], rowType=" + rowInfo.type + ">: css is OK");
          }
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

})(jQuery);