(function($) {
	module("wijgrid: columns visibility");

	var data = [["a0", "b0", "c0", "d0", "e0"]];

	test("columns", function() {
		// test0
		var $widget;

		try {
			 $widget = createWidget({
				data: data,
				columns: [
					{ headerText: "a" },
					{ headerText: "b" },
					{ headerText: "c" },
					{ headerText: "d" },
					{ headerText: "e" }
				]
			});

		  ok(matchObj(data, collectActualData($widget)), "test0: values are OK");
			ok(matchObj(["a", "b", "c", "d", "e"], $.map($widget.find("thead th"), function(item, idx) {
				return $(item).text();
			})), "test0: headers are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// test1
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ headerText: "a", visible: false },
					{ headerText: "b" },
					{ headerText: "c" },
					{ headerText: "d" },
					{ headerText: "e", visible: false }
				]
			});

		  ok(matchObj([["b0", "c0", "d0"]], collectActualData($widget)), "test1: values are OK");
			ok(matchObj(["b", "c", "d"], $.map($widget.find("thead th"), function(item, idx) {
				return $(item).text();
			})), "test1: headers are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// test2
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ headerText: "a", visible: false },
					{ headerText: "b" },
					{ headerText: "c", visible: false },
					{ headerText: "d", visible: false },
					{ headerText: "e" }
				]
			});

			ok(matchObj([["b0", "e0"]], collectActualData($widget)), "test2: values are OK");
			ok(matchObj(["b", "e"], $.map($widget.find("thead th"), function(item, idx) {
				return $(item).text();
			})), "test2: headers are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}
	});

	test("bands", function() {
		// test0
		var $widget;

		try {
			$widget = createWidget({
				data: data,
				columns: [
					{
						headerText: "band0",
						columns: [
							{ headerText: "a" },
							{ headerText: "b" }
						],
						visible: false
					},
					{ headerText: "c" },
					{
						headerText: "band1",
						columns: [
							{ headerText: "d" },
							{ headerText: "e" }
						],
						visible: false
					}
				]
			});

			ok(matchObj([["c0"]], collectActualData($widget)), "test0: values are OK");
			ok(matchObj(["c"], $.map($widget.find("thead th"), function(item, idx) {
				return $(item).text();
			})), "test0: headers are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// test1
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{
						headerText: "band0",
						columns: [
							{ headerText: "a"},
							{
								headerText: "band1",
								columns: [
									{ headerText: "b" }
								],
								visible: false
							},
							{
								headerText: "band2",
								columns: [
									{ headerText: "c" }
								],
								visible: false
							},
							{ headerText: "d" },
							{ headerText: "e" }
						]
					}
				]
			});

	   ok(matchObj([["a0", "d0", "e0"]], collectActualData($widget)), "test1: values are OK");
			ok(matchObj(["band0", "a", "d", "e"], $.map($widget.find("thead th"), function(item, idx) {
				return $(item).text();
			})), "test1: headers are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// test2
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{
						headerText: "band0",
						columns: [
							{ headerText: "a"},
							{
								headerText: "band1",
								columns: [
									{ headerText: "b", visible: false }
								]
							},
							{
								headerText: "band2",
								columns: [
									{ headerText: "c", visible: false }
								]
							},
							{ headerText: "d" },
							{ headerText: "e" }
						]
					}
				]
			});

			ok(matchObj(["a0", emptyString, emptyString, "d0", "e0"], $.map($widget.find("tbody td"), function(item, idx) {
				return $(item).text();
			})), "test2: values are OK");

			ok(matchObj(["band0", "a", "band1", "band2", "d", "e"], $.map($widget.find("thead th"), function(item, idx) {
				return $(item).text();
			})), "test2: headers are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}
	});
})(jQuery);