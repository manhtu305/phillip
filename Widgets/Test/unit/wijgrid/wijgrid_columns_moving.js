/*globals module,test,createWidget,jQuery,ok,document,same*/
/*
* wijgrid_columns_moving.js
*/

(function ($) {
	module("wijgrid: columns moving");
	var gridData = [
		["00", "01", "02"],
		["10", "11", "12"],
		["20", "21", "22"],
		["30", "31", "32"],
		["40", "41", "42"],
		["50", "51", "52"],
		["60", "61", "62"],
		["70", "71", "72"],
		["80", "81", "82"],
		["90", "91", "92"]
	];

	test("Columns Moving: test columns moving with UI.", function () {
		try {
			var settings = {
					allowColMoving: true,
					data: gridData,
					allowSorting: true
				},
				$widget, $outerDiv, $th;

			$widget = createWidget(settings);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			//check
			$th = $outerDiv.find("th:eq(0)");
			ok($th.find(".wijmo-wijgrid-innercell").find("a").text() === "0", "the title of first column is 0");
			ok($($widget.data("wijmo-wijgrid")._rows().item(5)[0].cells[0]).find("div").text() === "50", "the text of the sixth row is 50");

			//column moving
			$th = $outerDiv.find("th:eq(1)");		    
		    //It seems the trigger cannot simulate drag&drop in jquery ui 1.11.0. It can works in jquery ui 1.10.1.
		    //use simulate to replace trigger in jquery ui 1.11.0.
		    //$th.trigger({ type: "mousedown", which: 1, pageX: $th.offset().left + $th.width() / 2, pageY: $th.offset().top + $th.height() / 2 });
		    //$th = $outerDiv.find("th:eq(0)");
		    //$(document).trigger({ type: "mousemove", button: 1, pageX: $th.offset().left + $th.width() / 2, pageY: $th.offset().top + $th.height() / 2 });
		    //$(document).trigger({ type: "mouseup" });
			$th.simulate("mousedown", { which: 1, clientX: $th.offset().left + $th.width() / 2, clientY: $th.offset().top + $th.height() / 2 });			
			$th = $outerDiv.find("th:eq(0)");
			$(document).simulate("mousemove", { button: 1, clientX: $th.offset().left + $th.width() / 2, clientY: $th.offset().top + $th.height() / 2 });			
			$(document).simulate("mouseup");		    

			//check
			$th = $outerDiv.find("th:eq(0)");
			ok($th.find(".wijmo-wijgrid-innercell").find("a").text() === "1", "the title of first column is 1");
			ok($($widget.data("wijmo-wijgrid")._rows().item(5)[0].cells[0]).find("div").text() === "51", "the text of the sixth row is 51");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});
} (jQuery));
