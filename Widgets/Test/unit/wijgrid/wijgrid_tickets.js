﻿(function ($) {
	module("wijgrid:tickets");

	test("#40429", function () {
		var grid = $("<table>").appendTo("body").wijgrid({
			data: [[1, "a", "A"], [2, "b", "B"], [3, "c", "C"]],
			columns: [{ headerText: "Number" }, { headerText: "Lower Case" }, { headerText: "Upper Case" }],
			showSelectionOnRender: false,
			currentCellChanging: function (e, args) {
				if (args.cellIndex == 2) {
					return false;
				} else {
					return true;
				}
			}
		}),
		row = grid.find(".wijmo-wijgrid-row:last");

		row.find(".wijmo-wijgrid-innercell:last").simulate("click");
		ok(!grid.find(".wijmo-wijgrid-row:first .wijgridtd:first").is(".ui-state-highlight"), "when click the last cell,The first row is not selected.");
		row.find(".wijmo-wijgrid-innercell:first").simulate("click");
		ok(grid.find(".wijmo-wijgrid-row:last .wijgridtd:first").is(".ui-state-highlight"), "when click the first cell, the row is selected.");
		grid.remove();
	});

	test("An exception thrown when moving the current cell to the position (-1, -1).", function () {
		var settings = {
			data: [[0, 1, 2], [0, 1, 2], [0, 1, 2]]
		},
			$widget,
			thrown = true;

		try {
			$widget = createWidget(settings);

			try {
				$widget.wijgrid("currentCell", 0, 0);
				$widget.wijgrid("currentCell", -1, -1);
				thrown = false;
			}
			finally {
			}

			ok(!thrown);
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("46808, record selection is not maintained after sorting is applied", function () {
		var $widget,
			checkSoniaRowSelected = function () {
				var selectedCells = $widget.find("td[aria-selected='true'] .wijmo-wijgrid-innercell");
				return selectedCells.length === 3 && selectedCells[1].innerHTML === "Sonia";
			};

		try {

			$widget = createWidget({
				allowSorting: true,
				data: [
					[1, "Sonia", "jQuery"],
					[2, "Bella", "ASPNET MVC"]
				],
				columns: [
					{ headerText: "SDET ID" },
					{ headerText: "Name" },
					{ headerText: "Team" }
				]
			});

			var selectedCells = $widget.find("td[aria-selected='true'] .wijmo-wijgrid-innercell");
			ok(checkSoniaRowSelected(), "selected");

			var ths = $widget.find("th");
			$(ths[1]).find("a").trigger("click"); // sort

			ok(checkSoniaRowSelected(), "selection is maintaned");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});


	test("47955, \"Unable to get value of the property 'r1': object is null or undefined\" exception is thrown when grouping is applied if groupSingleRow=false and outlineMode=\"startCollapsed\"", function () {
		var $widget,
			exceptionThrown = false;

		try {
			$widget = createWidget({
				allowSorting: true,
				showGroupArea: true,
				columns: [
					{
						groupInfo: {
							position: "header"
						}
					},
					{
						groupInfo: {
							position: "header",
							groupSingleRow: false,
							outlineMode: "startCollapsed"
						}
					},
					{
						groupInfo: {
							position: "header"
						}

					}
				],
				data: [
					{ UnitPrice: 100, Discount: 0, Overseas: false }
				]
			});

		}
		catch (ex) {
			exceptionThrown = true;
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}

		ok(!exceptionThrown, "ok");
	});

	test("56761, \"Cell editing feature does not work on column of boolean data type if editingMode='row'\"", function () {
		var $widget;

		try {
			$widget = createWidget({
				editingMode: "row",
				data: [
					[1, true]
				],
				columns: [
					{},
					{ dataType: "boolean" },
					{ headerText: "Edit", showEditButton: true }
				]

			});

			$(":button", $widget).click(); // start editing

			ok(!$(":checkbox", $widget).prop("disabled"), "checkbox is enabled"); // test whether checkbox is enabled or not.
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("47731, pageIndex and data rows are not correctly displayed after removing the data records directly from the underlying array", function () {
		var $widget;

		try {
			$widget = createWidget({
				allowPaging: true,
				pageSize: 2,
				pageIndex: 1,
				data: [
					[0, "a"],
					[1, "b"],
					[2, "c"]
				],

			});

			var data = $widget.wijgrid("data");

			data.splice(2, 1); // delete the last item

			$widget.wijgrid("ensureControl", true); // rebind

			var rows = $widget.data("wijmo-wijgrid")._rows();
			var pageIndex = $widget.wijgrid("option", "pageIndex");

			ok(rows.length() === 2 && pageIndex === 0, "OK");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("#57242", function () {
		var container = $('<div id="gridContaier">').appendTo("body"),
			$widget;

		try {
			container.on("keydown", function (e) {
				if (e.which === 9) {
					ok("The tab keydown event is bubbled to the container");
				}
			});

			$widget = $("<table></table>")
				.appendTo(container)
				.wijgrid();

			$widget.simulate("keydown", { keyCode: 9 });
		} finally {
			if ($widget) {
				$widget.remove();
			}

			container.remove();
		}
	});

	test("#63777, sorting does not works when clicking on the sort icon in the column header.", function () {
		var $widget;

		try {
			var sorted = false;

			$widget = createWidget({
				allowSorting: true,
				columns: [
					{ sortDirection: "ascending" }
				],
				data: [[0]],
				sorted: function (args) {
					sorted = true;
				}
			});

			$widget.find("th .wijmo-wijgrid-sort-icon").trigger("click");

			ok(sorted, "OK");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});


	test("78523, args.formattedValue can be changed by user", function () {
		var $widget;

		try {
			var container;

			$widget = createWidget({
				columns: [
					{
						sortDirection: "ascending",
						cellFormatter: function (args) {
							if (args.row.type & wijmo.grid.rowType.data) {
								container = args.$container;
								args.formattedValue = "<b>" + args.formattedValue + "</b>";
							}
						}
					}
				],
				data: [[0]],
			});

			ok($.trim(container.html()) == "<b>0</b>", "OK");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("94433, Increasing row height each time editing cell if we enable static column and scroll", function () {
		var $widget;

		try {
			$widget = createWidget({
				data: [{ ID: 0, Text: "Text0" }],
				editingMode: 'cell',
				scrollMode: 'both',
				staticColumnIndex: 0
			});

			var grid = $widget.data("wijmo-wijgrid");

			grid.beginEdit();
			grid.endEdit();

			var h1 = grid._rows().item(0)[0].clientHeight;

			grid.beginEdit();
			grid.endEdit();

			var h2 = grid._rows().item(0)[0].clientHeight;

			ok(h1 === h2, "OK");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});
	test("113149, JavaScript error is observed when clicking the cell which contains chart", function () {
		var $widget, row, isCellClicked = false;

		try {
			$widget = createWidget({
				data: [{ ID: 0, Text: "Text0", Value: "value0" }],
				columns: [
					  { dataKey: "ID", headerText: "ID", dataType: "string" },
					  {
					  	dataKey: "Text",
					  	headerText: "Chart",
					  	cellFormatter: function (args) {
					  		if (args.row.type & $.wijmo.wijgrid.rowType.data) {
					  			var $chart = $("<div></div>")
					  			var data = [33, 11, 15, 26, 16, 27, 37, -13, 8, -8, -3, 17, 0, 22, -13, -29, 19, 8];
					  			$chart.wijsparkline({
					  				data: data, type: "line"
					  			});

					  			args.$container.empty()
									.append($chart);
					  			return true;
					  		}
					  	}
					  },
					  { dataKey: "Value", headerText: "Unit Price", dataType: "string" } ],
				cellClicked: function () {
					isCellClicked = true;
				}
			});

			row = $widget.find(".wijmo-wijgrid-row");
			row.find(".wijmo-wijgrid-innercell").eq(1).simulate("click");
			ok(isCellClicked, "cell clicked");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("119859, Error message appears on clicking 'Edit' button of different group row after clicked 'Edit' button of a group", function () {
		var $widget, row, rows, buttons, result,
		gridData = [
	["12", "10", "2"],
	["91", "10", "2"],
	["66", "10", "1"],
	["57", "20", "2"],
	["18", "20", "2"]
		];

		try {
			$widget = createWidget({
				data: gridData,
				editingMode: "row",
				showGroupArea: true,
				scrollingSettings: {
					mode: 'auto',
				},
				columns: [
					  { headerText: "Edit", showEditButton: true },
					  { dataType: "string" },
					  { dataType: "string" },
					  { dataType: "string" }, ]
			});

			columnWidgets = $widget.wijgrid("columns");
			columnWidgets[2].option("groupInfo", {
				position: "header",
				headerText: "Group-1"
			});
			columnWidgets[3].option("groupInfo", {
				position: "header",
				headerText: "Group-2"
			});

			rows = $widget.data("wijmo-wijgrid")._rows();
			buttons = $widget.find("input.wijmo-wijgrid-commandbutton");

			buttons.eq(0).trigger("click");
			result = $(rows.item(2)[0]).find("input.wijgridinput").length > 0;
			ok(result, "row 2 can be edit");

			buttons.eq(3).trigger("click");
			result = $(rows.item(8)[0]).find("input.wijgridinput").length > 0;
			ok(result, "row 8 can be edit");


		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("119989, Filter cells and data column mismatch when navigate with Tab key in filter bar", function () {
		var $widget, filterCols, $outerDiv, cellSelected, result, gridData = [
	["00", "01", "02", "03", "04", "05"],
	["10", "11", "12", "13", "14", "15"],
	["20", "21", "22", "23", "24", "25"]
		];

		try {
			$widget = createWidget({
				data: gridData,
				showFilter: true,
				scrollingSettings: {
					mode: 'both',
				},
				columns: [{ width: 80, ensurePxWidth: true }, { width: 80, ensurePxWidth: true }, { width: 80, ensurePxWidth: true }, { width: 80, ensurePxWidth: true }, { width: 80, ensurePxWidth: true }, { width: 80, ensurePxWidth: true }],
			}, $("<table style='width:280px;height:250px;'></table>"));

			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");
			filterCols = $outerDiv.find("input.wijmo-wijgrid-filter-input");

			filterCols.eq(3).trigger("click");
			cellSelected = $widget.wijgrid("currentCell");
			ok(cellSelected && cellSelected._ci === 3, "current cell has been moved to column 3");

			filterCols.eq(4).trigger("click");
			cellSelected = $widget.wijgrid("currentCell");
			ok(cellSelected && cellSelected._ci === 4, "current cell has been moved to column 4");

			filterCols.eq(5).trigger("click");
			cellSelected = $widget.wijgrid("currentCell");
			ok(cellSelected && cellSelected._ci === 5, "current cell has been moved to column 5");

		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("119860, Group rows are not collapsed on clicking expand icon of groups when setting 'VirtualizationSettings.Mode' property", function () {
		var $widget, row, sp, $outerDiv,
		gridData = [
	["12", "10", "2", "12", "10", "2"],
	["91", "10", "2", "12", "10", "2"],
	["66", "10", "1", "12", "10", "2"],
	["57", "20", "2", "12", "10", "2"],
	["18", "20", "2", "12", "10", "2"]
		];

		try {
			$widget = createWidget({
				data: gridData,
				showGroupArea: true,
				allowColMoving: true,
				scrollingSettings: {
					mode: 'both',
					virtualizationSettings: {
						mode: 'both'
					}
				},
				columns: [{ width: 80, ensurePxWidth: true }, { width: 80, ensurePxWidth: true }, { width: 80, ensurePxWidth: true }, { width: 80, ensurePxWidth: true }, { width: 80, ensurePxWidth: true }, { width: 80, ensurePxWidth: true }],
			}, $("<table id=\"demo\" style='width:180px;height:200px;'></table>"));

			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");
			sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
			ok(sp && sp.hNeedScrollBar && sp.vNeedScrollBar, "scrollMode is 'both'. Scrollbars are visible.");

			row = $widget.data("wijmo-wijgrid")._rows().item(0)[0];
			ok(row && $(row).find("td").length === 3, "HVirtualization is enabled.");

			columnWidgets = $widget.wijgrid("columns");
			columnWidgets[1].option("groupInfo", {
				position: "header",
				headerText: "Group-1"
			});
			row = $widget.data("wijmo-wijgrid")._rows().item(1)[0];
			ok(row && $(row).find("td").length === 6, "When column grouped. HVirtualization will be disabled.");

			$outerDiv.find("div.ui-icon-triangle-1-se").eq(0).trigger("click");//collapse row
			row = $widget.data("wijmo-wijgrid")._rows().item(0)[0];
			ok($(row).hasClass("wijmo-wijgrid-groupheaderrow-collapsed"), "GroupBtn works, row 0 collapsed");

		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("119988, Editing row is not disabled when setting 'disabled' option to true in parent grid", function () {
		var $widget, $outerDiv,
		 nwind = {
		 	customers: [
				{ CustomerID: "ALFKI", CompanyName: "Alfreds Futterkiste", ContactName: "Maria Anders" },
				{ CustomerID: "ANATR", CompanyName: "Ana Trujillo Emparedados y helados", ContactName: "Ana Trujillo" },
		 	],
		 	orders: [
				{ OrderID: 10643, CustomerID: "ALFKI", OrderDate: new Date(1995, 8, 26), Freight: 29.46 },
				{ OrderID: 10692, CustomerID: "ALFKI", OrderDate: new Date(1995, 10, 3), Freight: 61.02 },
				{ OrderID: 10308, CustomerID: "ANATR", OrderDate: new Date(1994, 9, 19), Freight: 1.61 },
				{ OrderID: 10625, CustomerID: "ANATR", OrderDate: new Date(1995, 8, 8), Freight: 43.9 },
		 	],
		 	orderDetails: [
				{ OrderID: 10643, ProductName: "R&#246;ssle Sauerkraut", UnitPrice: 45.6, Quantity: 15 },
				{ OrderID: 10643, ProductName: "Chartreuse verte", UnitPrice: 18, Quantity: 21 },
				{ OrderID: 10692, ProductName: "Vegie-spread", UnitPrice: 43.9, Quantity: 20 },
				{ OrderID: 10308, ProductName: "Gudbrandsdalsost", UnitPrice: 36, Quantity: 1 },
				{ OrderID: 10308, ProductName: "Outback Lager", UnitPrice: 15, Quantity: 5 },
				{ OrderID: 10625, ProductName: "Tofu", UnitPrice: 23.25, Quantity: 3 },
				{ OrderID: 10625, ProductName: "Singaporean Hokkien Fried Mee", UnitPrice: 14, Quantity: 5 },
		 	]
		 };

		try {
			$widget = createWidget({
				editingMode: "row",
				data: nwind.customers,
				columnsAutogenerationMode: "none",
				columns: [
					{ headerText: "Edit", showEditButton: true },
					{ dataKey: "CompanyName", headerText: "Company name", width: 700 },
					{ dataKey: "ContactName", headerText: "Contact name", width: 200 }
				],
				detail: {
					editingMode: "row",
					data: nwind.orders,
					columnsAutogenerationMode: "none",
					columns: [
						{ headerText: "Edit", showEditButton: true },
						{ dataKey: "OrderDate", headerText: "Order date", dataType: "datetime", width: 500 },
						{ dataKey: "Freight", headerText: "Freight", dataType: "number", width: 200 }
					],
					relation: [
						{ masterDataKey: "CustomerID", detailDataKey: "CustomerID" }
					],
					detail: {
						editingMode: "row",
						data: nwind.orderDetails,
						columnsAutogenerationMode: "none",
						columns: [
							{ headerText: "Edit", showEditButton: true },
							{ dataKey: "ProductName", headerText: "Product name" },
							{ dataKey: "UnitPrice", headerText: "Unit price", dataType: "currency" },
							{ dataKey: "Quantity", headerText: "Quantity", dataType: "number" }
						],
						relation: [
							{ masterDataKey: "OrderID", detailDataKey: "OrderID" }
						]
					}
				}
			}, $("<table></table>"));

			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			$outerDiv.find("div.ui-icon-triangle-1-e").eq(0).trigger("click");//expand level-1 row
			$outerDiv.find("div.ui-icon-triangle-1-e").eq(0).trigger("click");//expand level-2 row

			$widget.wijgrid("editRow", 0);
			$widget.wijgrid("details")[0].mDetail.editRow(0);
			$widget.wijgrid("details")[0].mDetail.details()[0].mDetail.editRow(0);

			ok($outerDiv.find(":enabled.wijmo-wijinput").length === 7 && $outerDiv.find(":enabled.wijmo-wijgrid-commandbutton").length === 9, "All inputs are enabled.");

			$widget.wijgrid("option", "disabled", true);
			ok($outerDiv.find(":disabled.wijmo-wijinput").length === 7 && $outerDiv.find(":disabled.wijmo-wijgrid-commandbutton").length === 9, "All inputs are disabled.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("122676, Header's border is out of alignment with body's border when hiding static column", function () {
		var $widget, $swCols, $nwCols, isLengthMatch, isColMatch, gridData = [
	["00", "01", "02", "03", "04", "05"],
	["10", "11", "12", "13", "14", "15"],
	["20", "21", "22", "23", "24", "25"]
		];


		try {
			$widget = createWidget({
				ensureColumnsPxWidth: true,
				data: gridData,
				scrollingSettings: {
					mode: "auto",
					staticColumnIndex: 2
				},
				columns: [
					{ width: 50, headerText: "col1" },
					{ width: 60, headerText: "col2", visible: false },
					{ width: 70, headerText: "col3" },
					{ width: 80, headerText: "col4" },
					{ width: 100, headerText: "col5" },
					{ width: 150, headerText: "col6" }]
			}, $("<table></table>"));

			ok(matchObj(["col1", "col3", "col4"], $.map($(".wijmo-wijgrid-split-area-nw").find("thead th"), function (item, idx) {
				return $(item).text();
			})), "Col2 is invisible");


			$swCols = $(".wijmo-wijgrid-split-area-sw").find("col");
			$nwCols = $(".wijmo-wijgrid-split-area-nw").find("col");

			ok(isLengthMatch = ($swCols.length === 3 && $nwCols.length === 3), "sw and nw has the same number of column definition");

			isColMatch = false;
			if (isLengthMatch) {
				$.each($swCols, function (i, col) {
					isColMatch = (col.style.width === $nwCols.eq(i).css("width"));
				});
			}

			ok(isColMatch, "column's width is perfectly match");

		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("120214, column header customized style is cleared when scrolling virtualized grid", function () {
		var $widget, $neHdTrAtt,
			$table = $("<table id=\"demo\" style=\" width:600px; height:400px\">" +
			"<thead><tr style=\"font-size: 26pt;\"><th>c0</th><th>c1</th><th>c2</th><th>c3</th><th>c4</th><th>c5</th><th>c6</th><th>c7</th></tr></thead>" +
	        "<tbody><tr><td>00</td><td>01</td><td>02</td><td>03</td><td>04</td><td>05</td><td>06</td><td>07</td></tr><tr><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td><td>17</td></tr><tr><td>20</td><td>21</td><td>22</td><td>23</td><td>24</td><td>25</td><td>26</td><td>27</td></tr></tbody>" +
	        "</table>");

		$widget = createWidget({
			readAttributesFromData: true,
			scrollingSettings: {
				mode: "auto",
				virtualizationSettings: {
					mode: "columns",
					columnWidth: 150
				}
			}
		}, $table);

		$neHdTrAtt = $(".wijmo-wijgrid-split-area-ne").find("thead tr")[0].attributes["style"].value.length;
		ok($neHdTrAtt > 0, "head row contains the customized style when loaded");

		$widget.wijgrid("currentCell", 7, 2); // move to the last cell

		setTimeout(function () {
			$neHdTrAtt = $(".wijmo-wijgrid-split-area-ne").find("thead tr")[0].attributes["style"].value.length;
			ok($neHdTrAtt > 0, "head row contains the customized style when scrolled");
			start();
			$widget.remove();
		}, 1000);

		stop();
	});

	test("129534,Multiple edit icons show and footer row displays improperly when column virtualization is applied in WijGrid", function () {
		var $widget, $row1col2, rows, cell, sp, $footer, footerHeight,
			$table = $("<table id=\"demo\" style=\" width:600px; height:400px\">" +
			"<thead><tr><th>c0</th><th>c1</th><th>c2</th><th>c3</th><th>c4</th><th>c5</th><th>c6</th><th>c7</th></tr></thead>" +
            "<tbody><tr><td>00</td><td>01</td><td>02</td><td>03</td><td>04</td><td>05</td><td>06</td><td>07</td></tr><tr><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td><td>17</td></tr><tr><td>20</td><td>21</td><td>22</td><td>23</td><td>24</td><td>25</td><td>26</td><td>27</td></tr></tbody>" +
            "</table>");

		$widget = createWidget({
			readAttributesFromData: true,
			showRowHeader: true,
			editingMode: "cell",
			showFooter: true,
			scrollingSettings: {
				mode: "auto",
				virtualizationSettings: {
					mode: "columns",
					columnWidth: 150
				}

			}
		}, $table);
		sp = $widget.closest("div.ui-widget.wijmo-wijgrid").find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
		$row1col2 = $("#demo").find("tbody tr:eq(0) td:eq(1)");
		$footer = $("#demo tr.wijmo-wijgrid-footerrow");
		rows = $widget.data("wijmo-wijgrid")._rows();
		cell = rows.item(0)[0].cells[0];

		$row1col2.trigger("click").trigger("dblclick");
		footerHeight = $footer.height();
		ok($(cell).find("div.ui-icon-pencil")[0], "edit icon shows up");

		sp.hScrollTo(500);
		setTimeout(function () {
			$footer = $("#demo tr.wijmo-wijgrid-footerrow");
			ok(!$(cell).find("div.ui-icon-pencil")[0], "edit icon has disappeared");
			ok(footerHeight === $footer.height(), "footer height doesn't get changed after virtual scrolling");
			$widget.remove();
			start();
		}, 1000);
		stop();
	});

	test("#129420,Row height is very large when clicking ‘Edit’ button in hierarchical grid", function () {
		var $widget, rowHeight,
			$table = $("<table id=\"demo\" style=\" width:600px; height:400px\">" +
			"<thead><tr><th>c0</th><th>c1</th><th>c2</th><th>c3</th><th>c4</th><th>c5</th><th>c6</th><th>c7</th></tr></thead>" +
			"<tbody><tr><td>00</td><td>01</td><td>02</td><td>03</td><td>04</td><td>05</td><td>06</td><td>07</td></tr><tr><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td><td>17</td></tr><tr><td>20</td><td>21</td><td>22</td><td>23</td><td>24</td><td>25</td><td>26</td><td>27</td></tr></tbody>" +
			"</table>");

		$widget = createWidget({
			showRowHeader: true,
			showFilter: true,
			editingMode: "row",
			showFooter: true,
			columns: [
					{ headerText: "Edit", showEditButton: true }
			],
		}, $table);

		rowHeight = $table.find("tbody tr:first").height();
		$widget.wijgrid("editRow", 0);
		ok(rowHeight == $table.find("tbody tr:first").height(), "a row in editing state doesn't change its height");
		$widget.wijgrid("updateRow");
		ok(rowHeight == $table.find("tbody tr:first").height(), "a row exited editing state doesn't change its height");
		$widget.remove();
	});

	test("#124159", function () {
		var $widget, $table = $("<table id=\"demo\" style=\" width:400px; height:150px\"></table>");

		$widget = createWidget({
			data: [
				{ CustomerID: "1", CompanyName: "Apple", Category: "Product" },
				{ CustomerID: "2", CompanyName: "Amazon", Category: "Service" },
			],
			ensureColumnsPxWidth: true,
			scrollingSettings: {
				mode: 'auto',
				staticColumnIndex: 0,
				staticColumnsAlignment: "right",
				freezingMode: "columns"
			},
			columnsAutogenerationMode: "none",
			columns: [
				 { dataKey: "CustomerID", headerText: "CustomerID", width: 120, dataType: "string" },
				 { dataKey: "CompanyName", headerText: "CompanyName" },
				 { dataKey: "Category", headerText: "Category" },
				 { headerText: "Command", showDeleteButton: true }
			]
		}, $table);
		ok($(".wijmo-wijgrid-split-area-nw").position().left > 270);
		$widget.remove();
	});

	test("124162, Footer height increases and row headers are misaligned on scrolling horizontally", function () {
		var $widget, $outerDiv, sp, pager, leftHeight, rightHeight,
		$table = $("<table id=\"demo\" style=\" width:300px; height:200px\">" +
		"<thead><tr><th>c0</th><th>c1</th><th>c2</th><th>c3</th><th>c4</th><th>c5</th><th>c6</th><th>c7</th></tr></thead>" +
		"<tbody><tr><td>00</td><td>01</td><td>02</td><td>03</td><td>04</td><td>05</td><td>06</td><td>07</td></tr><tr><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td><td>17</td></tr><tr><td>20</td><td>21</td><td>22</td><td>23</td><td>24</td><td>25</td><td>26</td><td>27</td></tr>" +
		"<tr><td>00</td><td>01</td><td>02</td><td>03</td><td>04</td><td>05</td><td>06</td><td>07</td></tr><tr><td>10</td><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td><td>16</td><td>17</td></tr><tr><td>20</td><td>21</td><td>22</td><td>23</td><td>24</td><td>25</td><td>26</td><td>27</td></tr></tbody>" +
		"</table>");

		$widget = createWidget({
			showRowHeader: true,
			showFooter: true,
			allowPaging: true,
			pageSize: 2,
			columns: [
					{ headerText: "Edit", showEditButton: true }
			],
			scrollingSettings: {
				mode: 'columns',
				virtualizationSettings: {
					mode: 'columns'
				}
			}
		}, $table);

		$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");
		sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
		pager = $outerDiv.find(".wijmo-wijpager");
		sp.hScrollTo(200);
		$(".wijmo-wijpager li:eq(1)").simulate("click");
		sp = $outerDiv.find(".wijmo-wijsuperpanel").data("wijmo-wijsuperpanel");
		sp.hScrollTo(0);
		setTimeout(function () {
			leftHeight = $(".wijmo-wijgrid-split-area-sw").find("tbody tr:eq(0)").outerHeight();
			rightHeight = $(".wijmo-wijgrid-split-area-se").find("tbody tr:eq(0)").outerHeight();
			ok(leftHeight === rightHeight, "LeftRow's height is equal to rightRow's while scrolling virtual scrollbar");
			$widget.remove();
			start();
		}, 1000);
		stop();
		
	});

	test("#144907", function () {
		var $widget, $table = $("<table id=\"demo\" style=\"height:150px\"></table>"), data = [];
		for (var i = 0; i < 100; i++) {
			data.push({ ID: i, Name: 'Name' + i });
		}
		$widget = createWidget({
			data: data,
			scrollingSettings: {
				mode: 'auto',
				virtualizationSettings: {
					mode: 'both'
				}
			},
			showRowHeader: true
		}, $table);
		$widget.closest(".wijmo-wijgrid")
			.find(".wijmo-wijsuperpanel")
			.wijsuperpanel("vScrollTo", 2300);
		setTimeout(function() {
			$("td[role = 'rowheader']").last().simulate("click");
			ok($(".ui-state-highlight").text() == "99Name99");
			$widget.remove();
			start();
		}, 1000);
		stop();
	});

	test("#144419", function () {
		var $widget, rowHeight,
			$table = $("<table id=\"demo\" style=\" width:600px; height:400px\"></table>");

		$widget = createWidget({
			data: [
				{ LastName: "a1", FirstName: "b1", Project: "A" },
				{ LastName: "a2", FirstName: "b2", Project: "B" },
				{ LastName: "a3", FirstName: "b3", Project: "C" }
			],
			columns: [
				{
					headerText: "Name",
					columns: [
						{ headerText: "Last", dataKey: "LastName" },
						{ headerText: "First", dataKey: "FirstName" }
					]
				},
				{ headerText: "Project" }
			]
		}, $table);
		$($widget).wijgrid("option", "disabled", true);
		ok(1 == $(".ui-state-disabled").length);
		$widget.remove();
	});

	test("#151964", function () {
		var $widget, $table = $("<table id=\"demo\"></table>"), items = [], i;
		for (i = 0; i < 5; i++) {
			items.push({ Id: i, Name: 'User ' + i, Team: i % 3 });
		}
		$widget = createWidget({
			data: items,
			columns: [
				{},
				{},
				{
					cellFormatter: function (args) {
						if (args.row.type & wijmo.grid.rowType.data) {
							$("<input>").appendTo(args.$container.empty()).wijcombobox({
								data: [
									{ label: 'Sales', value: '0' },
									{ label: 'Manufacturing', value: '1' },
									{ label: 'Operations', value: '2' }
								],
								selectedValue: args.formattedValue
							});
							return true;
						}
					}
				}
			],
			scrollingSettings: {
				mode: 'auto'
			}
		}, $table);
		ok($(".wijmo-wijcombobox-trigger", "#demo").length == 5);
		$widget.remove();
	});

	test("#112397", function () {
		var arr = [], grid, row, $table = $("<table id=\"demo\" style=\"width:400px;height:450px;\"></table>");

		for (var i = 0; i < 1000; i++) {
			arr.push({
				ProductID: i,
				Name: "Name - " + i,
				Supplier: i + " - S",
				Category: "Cat - " + (i % 7),
				QuantityPerUnit: i / 300,
				UnitPrice: i
			});
		};

		grid = createWidget({
			showRowHeader: true,
			data: arr,
			scrollingSettings: {
				"mode": "auto",
				"virtualizationSettings": {
					"mode": "both"
				}
			}
		}, $table);

		$(".wijmo-wijgrid-scroller").wijsuperpanel("vScrollTo", 984, true);
		setTimeout(function () {
			$(".wijmo-wijgrid-scroller").wijsuperpanel("hScrollTo", 500, true);
			row = grid.find(".wijmo-wijgrid-row:last");
			row.find(".wijmo-wijgrid-innercell:last").simulate("click");
			ok(row.text().replace(/\s+/g, '') === "999Name-999999-SCat-5");
			start();
		}, 50);
		grid.remove();
	});

	test("#204080", function () {
		var $widget, $table = $("<table id=\"demo\"></table>");
		$widget = createWidget({
			showGroupArea: true,
			allowColMoving: true,
			allowSorting: true,
			showRowHeader: true,
			data: [
				[1, 'iPhone', 'Apple'],
				[2, 'iPad', 'Apple'],
				[3, 'Galaxy Note 7', 'Samsung'],
				[4, 'Galaxy S6', 'Samsung'],
				[5, 'Redmi Pro', 'Xiaomi']
			],
			columns: [
				{ headerText: "Id" },
				{ headerText: "Name" },
				{ headerText: "Vendor", groupInfo: { position: 'header' } }
			]
		}, $table);
		ok($(".wijmo-wijgrid-groupheaderrow-expanded", $widget).length == 3);
		$(".wijmo-wijgrid-grouptogglebtn", $widget).click();
		ok($(".wijmo-wijgrid-groupheaderrow-expanded", $widget).length == 0);
		$widget.remove();
	});

	test("#211302", function () {
		var grid, superPanel, panel, table = $("<table id=\"demo\" style=\"height:211px;\"></table>"),
			data = [{
				"A": "1",
				"B": "ABCDEFGABCDEFGABCDEFGABCDEFGABCDEFGABCDEFGABCDEFGABCDEFG"
			}],
			basedata = {
				"A": "2",
				"B": "ABCDEFGABCDEFGABCDEFGABCDEFGABCDEFGABCDEFGABCDEFGABCDEFG"
			},
			G_Columns = [
				{ dataKey: "A", headerText: "€–ÚA", dataType: "string", width: 250, textAlignment: "left", allowMoving: true, visible: true },
				{ dataKey: "B", headerText: "€–ÚB", dataType: "string", width: 542, textAlignment: "left", allowMoving: true, visible: true }
			];

		for (var i = 2; i <= 2000; i++) {
			var tmpdata = {
				"A": i,
				"B": "ABCDEFGABCDEFGABCDEFGABCDEFG"
			};
			data.push(tmpdata);
		}
		grid = createWidget({
			allowSorting: false,
			allowColSizing: true,
			allowColMoving: false,
			allowEditing: false,
			scrollMode: "both",
			culture: "ja-JP",
			showSelectionOnRender: false,
			columnsAutogenerationMode: "none",
			data: data,
			columns: G_Columns,
			ensureColumnsPxWidth: true
		}, table);
		superPanel = $("#demo").closest(".wijmo-wijgrid").find(".wijmo-wijsuperpanel");
		superPanel.wijsuperpanel('option', 'scrolled', function () {
			var headerHeight = $('.wijmo-wijgrid-headerrow').height(),
			topOffset = superPanel.offset().top + headerHeight,
			bottom = topOffset + superPanel.height() - headerHeight - $('.wijmo-wijsuperpanel-hbarcontainer').height(),
			indexes = [],
			scrolledItems = $.grep($(".wijmo-wijgrid-datarow"), function (item, index) {
				var row = $(item).offset(),
					isScrolled = row.top >= topOffset && row.top + $(item).height() <= bottom;
				if (isScrolled) {
					indexes.push($("td:first", item).text());
				}
				return isScrolled;
			});
			//Verify the visible rows in viewport.
			ok('VisibleRowCount:' + indexes.length + '.Items:' + indexes.join(',') + '.' === 'VisibleRowCount:7.Items:4,5,6,7,8,9,10.');
			grid.remove();
			start();
		});
		setTimeout(function () {
			panel = superPanel.data('wijmo-wijsuperpanel');
			panel._doScrolling('bottom', panel);
		}, 500);
		stop();
	});

	test("#222802, Unable to select a row after selecting the last row, when virtual scrolling is enabled", function () {
		var widget, table = $("<table id=\"demo\" style='height:300px;'></table>");
		widget = createWidget({
			data: (function (count) {
				var arr = [];
				for (var i = 0; i < count; i++) {
					arr.push({
						ID: i,
						Name: "Name" + i,
						Value: "Value" + i
					});
				}
				return arr;
			})(100),
			scrollMode: "auto",
			allowVirtualScrolling: true
		}, table);

		for (var i = 1; i <= 11; i++) {
			setTimeout(function (x) {
				return function () {
					widget.simulate("keydown", { keyCode: x < 11 ? $.ui.keyCode.PAGE_DOWN : $.ui.keyCode.UP });
					if (x === 11) {
						ok($('.ui-state-highlight', widget).text() === '98Name98Value98');
						widget.remove();
						start();
					}
				};
			}(i), 1000 * i);
		}
		stop();
	});
})(jQuery);
