/*globals module,test,createWidget,jQuery,ok,document,same*/
/*
* wijgrid_columns_moving.js
*/

(function ($) {
	module("wijgrid: columns creation");

	var array = [
		[00, 01],
		[10, 11]
	];

	var hash = [
		{ ID: 0, Name: 1 },
		{ ID: 1, Name: 1 }
	];

	test("columnsAutogenerationMode = \"none\"", function () {
		columnsTest(array, "none", [], []);
		columnsTest(hash, "none", [], []);

		columnsTest(array, "none", [{}, {}], [undefined, undefined]);
		columnsTest(hash, "none", [{}, {}], [undefined, undefined]);
	});

	test("columnsAutogenerationMode = \"append\"", function () {
		columnsTest(array, "append", [], [0, 1]);
		columnsTest(hash, "append", [], ["ID", "Name"]);

		columnsTest(array, "append", [{}, {}], [undefined, undefined, 0, 1]);
		columnsTest(hash, "append", [{}, {}], [undefined, undefined, "ID", "Name"]);
	});

	test("columnsAutogenerationMode = \"merge\"", function () {
		columnsTest(array, "merge", [{}, {}], [0, 1]);
		columnsTest(hash, "merge", [], ["ID", "Name"]);

		columnsTest(array, "merge", [{}, {}], [0, 1]);
		columnsTest(hash, "merge", [{}, {}], ["ID", "Name"]);

		columnsTest(array, "merge", [{ dataKey: 0 }, { dataKey: 1}], [0, 1]);
		columnsTest(hash, "merge", [{ dataKey: "ID" }, { dataKey: "Name"}], ["ID", "Name"]);

		columnsTest(array, "merge", [{}, {}, { dataKey: 0}], [1, undefined, 0]);
		columnsTest(hash, "merge", [{}, {}, { dataKey: "ID"}], ["Name", undefined, "ID"]);

		columnsTest(array, "merge", [{ dataKey: null }, { dataKey: null }, { dataKey: 1 }, { dataKey: 0 }, {}], [null, null, 1, 0, undefined]);
		columnsTest(hash, "merge", [{ dataKey: null }, { dataKey: null }, { dataKey: "Name" }, { dataKey: "ID" }, {}], [null, null, "Name", "ID", undefined]);
	});


	test("column.options.disabled", function() {
		var $widget;

		try
		{
			$widget = createWidget({
				data: array,
				disabled: true
			});

			ok($widget.wijgrid("columns")[0].options.disabled === true, "OK");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}			
		}
	});

	function columnsTest(data, mode, columns, expectedDataKeys) {
		try {
			var $widget = createWidget({
				data: data,
				columnsAutogenerationMode: mode,
				columns: columns
			});

			var foo = $widget.wijgrid("option", "columns");
			ok(foo.length === expectedDataKeys.length, "Number of columns is OK.");

			var flag = true;
			for (var i = 0, len = foo.length; i < len && foo; i++) {
				flag = ((foo[i].dataKey + "") === (expectedDataKeys[i] + ""));
			}

			ok(flag, "data keys are OK.");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	}
} (jQuery));
