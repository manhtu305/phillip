(function ($) {
	module("wijgrid: core");

	test("create and destroy", function () {
		var $widget, html, className;

		try {
			$widget = $("<table class=\"myclass\"><thead><tr><th>COL0</th><th>COL1</th></tr></thead><tbody><tr><td>VAL0</td><td>VAL1</td></tr></tbody></table>")
				.appendTo(document.body);

			html = $widget.html();
			className = $widget.attr("class") || "";

			$widget
				.wijgrid({ allowPaging: true, showFilter: true, showRowHeader: true, scrollMode: "both", staticRowIndex: 0	})
				.wijgrid("destroy");
		
			ok(($widget.html() === html) && ($widget.attr("class") === className), "content restored");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("cleanup $data", function () {
		var $widget,
			dataKeys = [],
			eventKeys = [],
			data, key;

		try {
			$widget = createWidget({
				data: [[0, 1, 2]]
			});

			$widget.wijgrid("destroy");

			data = $widget.data();

			for (key in data) {
				if (data.hasOwnProperty(key)) {

					if (key === "events") {
						for (key in data["events"]) {
							if (data["events"].hasOwnProperty(key)) {
								eventKeys.push(key);
							}
						}
					} else {
						if (key.indexOf("wijgrid") >= 0) {
							dataKeys.push(key);
						}
					}
				}
			}

			ok(dataKeys.length === 0 && eventKeys.length === 0, "dataKeys: " + dataKeys.join(",") + "; " + "eventKeys: " + eventKeys.join(","));
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});
})(jQuery);