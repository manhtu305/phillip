(function ($) {
	module("wijgrid: filtering, multiple operators and values");

	var data = [
		["1", "a", "5", "e"],
		["2", "b", "6", "f"],
		["3", "c", "7", "g"],
		["4", "d", "8", "h"]
	];

	test("parameter passing", function () {

      // parameter passing
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ filterOperator: "Equals", filterValue: "1" },
					{ filterOperator: ["Equals"], filterValue: ["a"] },
					{ filterOperator: { name: "Equals" }, filterValue: [["5"]] },
					{ filterOperator: [{ name: "Equals" }], filterValue: [["e"]] }
				]
			});

			ok(matchObj([data[0]], collectActualData($widget)), "values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

     // parameter redundancy
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ filterOperator: ["Equals", "NotEqual"], filterValue: [["1", "2", "3"]] },
					{ filterOperator: ["BeginsWith", "BeginsWith"], filterValue: [["a", "b"]] },
				]
			});

			ok(matchObj([data[0], data[1]], collectActualData($widget)), "values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}
	});

	test("filtering", function () {
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{
						filterOperator: [ { name: "Equals" }, { name: "BeginsWith" }, { name: "NotEqual", condition: "and" }],
						filterValue: [["1", "4"], ["2", "3"], "2"]
						// [0][1][2][3] - [1]
					},
					{
						filterOperator: ["NotEqual", { name: "NotEqual", condition: "and" }],
						filterValue: ["a", "c"]
						// [0][2][3] - [0][2]
					}
				]
			});

			ok(matchObj([data[3]], collectActualData($widget)), "values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}
	});
})(jQuery);
