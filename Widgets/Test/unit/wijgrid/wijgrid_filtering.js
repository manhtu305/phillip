(function ($) {
	module("wijgrid: filtering");

	var data = [
		["abc", 0, "01/01/2010", null, true],
		["bcd", null, "02/01/2010", null, false],
		[null, 2, null, 2, false],
		["def", 3, "04/01/2010", 3, null],
		["", null, "05/01/2010", 4, null],
		["fgh", 5, null, 5, true]
	];

	var refParsedData = [
		["abc", 0, new Date("01/01/2010"), null, true],
		["bcd", null, new Date("02/01/2010"), null, false],
		["", 2, null, 2, false],
		["def", 3, new Date("04/01/2010"), 3, null],
		["", null, new Date("05/01/2010"), 4, null],
		["fgh", 5, null, 5, true]
	];

	test("embedded filters (csom)", function () {
		var refNoFilter = $.extend([], refParsedData),
			refContains = [refParsedData[0], refParsedData[1]],
			refNotContain = [refParsedData[2], refParsedData[3], refParsedData[4], refParsedData[5]],
			refBeginsWith = [refParsedData[1]],
			refEndsWith = [refParsedData[0]],
			refEquals = [refParsedData[0]],
			refNotEqual = [refParsedData[2], refParsedData[3], refParsedData[4]],
			refGreater = [],
			refLess = [refParsedData[1]],
			refGreaterOrEqual = [refParsedData[5]],
			refLessOrEqual = [refParsedData[1], refParsedData[3], refParsedData[4]],
			refIsEmpty = [refParsedData[2], refParsedData[4]],
			refNotIsEmpty = [refParsedData[0], refParsedData[1], refParsedData[3], refParsedData[5]],
			refIsNull = [],
			refIsNull2 = [refParsedData[2]],
			refNotIsNull = [],
			refNotIsNull2 = [refParsedData[3]],

			$widget;

		// "NoFilter"
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "NoFilter", filterValue: "abc" },
					{ dataType: "number", filterOperator: "NoFilter", filterValue: 0 },
					{ dataType: "datetime", filterOperator: "NoFilter", filterValue: "01/01/2010" },
					{ dataType: "currency", filterOperator: "NoFilter", filterValue: "$5.0", dataFormatString: "n1" },
					{ dataType: "boolean", filterOperator: "NoFilter", filterValue: true }
				]
			});

			ok(matchObj(refNoFilter, collectActualData($widget)), "\"NoFilter\": values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// "Contains"
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "Contains", filterValue: "b" },
					{ dataType: "number", filterOperator: "Contains", filterValue: 0 },
					{ dataType: "datetime", filterOperator: "Contains", filterValue: "01/01/2010" },
					{ dataType: "currency", filterOperator: "Contains", filterValue: "$5.0", dataFormatString: "n1" },
					{ dataType: "boolean", filterOperator: "Contains", filterValue: true }
				  ]
			});

			ok(matchObj(refContains, collectActualData($widget)), "\"Contains\": values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// "NotContain"
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "NotContain", filterValue: "b" },
					{ dataType: "number", filterOperator: "NotContain", filterValue: 0 },
					{ dataType: "datetime", filterOperator: "NotContain", filterValue: "01/01/2010" },
					{ dataType: "currency", filterOperator: "NotContain", filterValue: "$5.0", dataFormatString: "n1" },
					{ dataType: "boolean", filterOperator: "NotContain", filterValue: true }
				  ]
			});

			ok(matchObj(refNotContain, collectActualData($widget)), "\"NotContain\": values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// "BeginsWith"
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "BeginsWith", filterValue: "b" },
					{ dataType: "number", filterOperator: "BeginsWith", filterValue: 0 },
					{ dataType: "datetime", filterOperator: "BeginsWith", filterValue: "01/01/2010" },
					{ dataType: "currency", filterOperator: "BeginsWith", filterValue: "$5.0", dataFormatString: "n1" },
					{ dataType: "boolean", filterOperator: "BeginsWith", filterValue: true }
				  ]
			});

			ok(matchObj(refBeginsWith, collectActualData($widget)), "\"BeginsWith\": values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// "EndsWith"
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "EndsWith", filterValue: "c" },
					{ dataType: "number", filterOperator: "EndsWith", filterValue: 0 },
					{ dataType: "datetime", filterOperator: "EndsWith", filterValue: "01/01/2010" },
					{ dataType: "currency", filterOperator: "EndsWith", filterValue: "$5.0", dataFormatString: "n1" },
					{ dataType: "boolean", filterOperator: "EndsWith", filterValue: true }
				  ]
			});

			ok(matchObj(refEndsWith, collectActualData($widget)), "\"EndsWith\": values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// "Equals"
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "Equals", filterValue: "abc" },
					{ dataType: "number", filterOperator: "Equals", filterValue: 0 },
					{ dataType: "datetime", filterOperator: "Equals", filterValue: "01/01/2010" },
					{ dataType: "currency", filterOperator: "Equals", filterValue: null, dataFormatString: "n1" },
					{ dataType: "boolean", filterOperator: "Equals", filterValue: true }
				]
			});

			ok(matchObj(refEquals, collectActualData($widget)), "\"Equals\": values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// "NotEqual"
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "NotEqual", filterValue: "abc" },
					{ dataType: "number", filterOperator: "NotEqual", filterValue: 0 },
					{ dataType: "datetime", filterOperator: "NotEqual", filterValue: "01/01/2010" },
					{ dataType: "currency", filterOperator: "NotEqual", filterValue: null, dataFormatString: "n1" },
					{ dataType: "boolean", filterOperator: "NotEqual", filterValue: true }
				  ]
			});

			ok(matchObj(refNotEqual, collectActualData($widget)), "\"NotEqual\": values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// "Greater"
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "Greater", filterValue: "abc" },
					{ dataType: "number", filterOperator: "Greater", filterValue: 0 },
					{ dataType: "datetime", filterOperator: "Greater", filterValue: "01/01/2010" },
					{ dataType: "currency", filterOperator: "Greater", filterValue: null, dataFormatString: "n1" },
					{ dataType: "boolean", filterOperator: "Greater", filterValue: true }
				  ]
			});

			ok(matchObj(refGreater, collectActualData($widget)), "\"Greater\": values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// "Less"
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "Less", filterValue: "zzz" },
					{ dataType: "number", filterOperator: "Less", filterValue: 5 },
					{ dataType: "datetime", filterOperator: "Less", filterValue: "04/01/2010" },
					{ dataType: "currency", filterOperator: "Less", filterValue: 3, dataFormatString: "n1" },
					{ dataType: "boolean", filterOperator: "Less", filterValue: true }
				]
			});

			ok(matchObj(refLess, collectActualData($widget)), "\"Less\": values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// "GreaterOrEqual"
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "GreaterOrEqual", filterValue: "abc" },
					{ dataType: "number", filterOperator: "GreaterOrEqual", filterValue: 1 },
					{ dataType: "datetime", filterOperator: "GreaterOrEqual", filterValue: null },
					{ dataType: "currency", filterOperator: "GreaterOrEqual", filterValue: 3, dataFormatString: "n1" },
					{ dataType: "boolean", filterOperator: "GreaterOrEqual", filterValue: true }
				]
			});

			ok(matchObj(refGreaterOrEqual, collectActualData($widget)), "\"GreaterOrEqual\": values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// "LessOrEqual"
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "LessOrEqual", filterValue: "zzz" },
					{ dataType: "number", filterOperator: "LessOrEqual", filterValue: 5 },
					{ dataType: "datetime", filterOperator: "LessOrEqual", filterValue: "05/01/2010" },
					{ dataType: "currency", filterOperator: "LessOrEqual", filterValue: 4, dataFormatString: "n1" },
					{ dataType: "boolean", filterOperator: "LessOrEqual", filterValue: null }
				]
			});

			ok(matchObj(refLessOrEqual, collectActualData($widget)), "\"LessOrEqual\": values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// "IsEmpty"
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "IsEmpty", filterValue: "zzz" },
					{ dataType: "number", filterOperator: "IsEmpty", filterValue: 5 },
					{ dataType: "datetime", filterOperator: "IsEmpty", filterValue: "05/01/2010" },
					{ dataType: "currency", filterOperator: "IsEmpty", filterValue: 4, dataFormatString: "n1" },
					{ dataType: "boolean", filterOperator: "IsEmpty", filterValue: null }
				  ]
			});

			ok(matchObj(refIsEmpty, collectActualData($widget)), "\"IsEmpty\": values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// "NotIsEmpty"
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "NotIsEmpty", filterValue: "zzz" },
					{ dataType: "number", filterOperator: "NotIsEmpty", filterValue: 5 },
					{ dataType: "datetime", filterOperator: "NotIsEmpty", filterValue: "05/01/2010" },
					{ dataType: "currency", filterOperator: "NotIsEmpty", filterValue: 4, dataFormatString: "n1" },
					{ dataType: "boolean", filterOperator: "NotIsEmpty", filterValue: null }
				]
			});

			ok(matchObj(refNotIsEmpty, collectActualData($widget)), "\"NotIsEmpty\": values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// "IsNull"
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "IsNull", filterValue: "zzz" },
					{ dataType: "number", filterOperator: "IsNull", filterValue: 5 },
					{ dataType: "datetime", filterOperator: "IsNull", filterValue: "05/01/2010" },
					{ dataType: "currency", filterOperator: "IsNull", filterValue: 4, dataFormatString: "n1" },
					{ dataType: "boolean", filterOperator: "IsNull", filterValue: null }
				]
			});

			ok(matchObj(refIsNull, collectActualData($widget)), "\"IsNull\": values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// "IsNull2"
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "IsNull" },
					{ dataType: "number" },
					{ dataType: "datetime", filterOperator: "IsNull" },
					{ dataType: "currency" },
					{ dataType: "boolean" }
				]
			});

			ok(matchObj(refIsNull2, collectActualData($widget)), "\"IsNull2\": values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// "NotIsNull"
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "NotIsNull", filterValue: "zzz" },
					{ dataType: "number", filterOperator: "NotIsNull", filterValue: 5 },
					{ dataType: "datetime", filterOperator: "NotIsNull", filterValue: "05/01/2010" },
					{ dataType: "currency", filterOperator: "NotIsNull", filterValue: 4, dataFormatString: "n1" },
					{ dataType: "boolean", filterOperator: "NotIsNull", filterValue: null }
				]
			});

			ok(matchObj(refNotIsNull, collectActualData($widget)), "\"NotIsNull\": values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}

		// "NotIsNull2"
		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "NotIsNull" },
					{ dataType: "number", filterOperator: "NotIsNull" },
					{ dataType: "datetime", filterOperator: "NotIsNull" },
					{ dataType: "currency", filterOperator: "NotIsNull" },
					{ dataType: "boolean" }
				]
			});

			ok(matchObj(refNotIsNull2, collectActualData($widget)), "\"NotIsNull2\": values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}
	});

	test("multiple filters (csom)", function () {
		// -- 1
		var $widget;

		try {
			$widget = createWidget({
				data: data,
				columns: [
					{ dataType: "string", filterOperator: "Greater", filterValue: "abc" },
					{ dataType: "number", filterOperator: "NoFilter", filterValue: 3 },
					{ dataType: "datetime", filterOperator: "NoFilter", filterValue: "04/01/2010" },
					{ dataType: "currency" },
					{ dataType: "boolean" }
				]
			});

			var ref = [refParsedData[1], refParsedData[3], refParsedData[5]];
			ok(matchObj(ref, collectActualData($widget)), "values are OK");

			// -- 2
			$widget.wijgrid("columns")[1].option("filterOperator", "Equals");
			ref = [refParsedData[3]];
			ok(matchObj(ref, collectActualData($widget)), "values are OK");

			// -- 3
			$widget.wijgrid("columns")[2].option("filterOperator", "Equals");
			ref = [refParsedData[3]];
			ok(matchObj(ref, collectActualData($widget)), "values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}
	});

	test("custom filters (csom)", function () {
		var op1 = {
			name: "RegExTest",
			arity: 2,
			applicableTo: ["string"],
			operator: function (dataVal, filterVal) {
				return (new RegExp(filterVal)).test(dataVal);
			}
		};

		var op2 = {
			name: "EvenOrNull",
			arity: 1,
			applicableTo: ["number"],
			operator: function (dataVal) {
				return (dataVal === null || dataVal % 2 === 0);
			}
		};

		var $widget;

		try {
			$widget = createWidget({
				data: data,
				customFilterOperators: [op1, op2],
				columns: [
					{ dataType: "string", filterOperator: "RegExTest", filterValue: "^\\w{3,3}$" },
					{ dataType: "number", filterOperator: "EvenOrNull" },
					{ dataType: "datetime", filterOperator: "RegExTest", filterValue: "^\\w{3,3}$" },
					{ dataType: "currency", filterOperator: "EvenOrNull" },
					{ dataType: "boolean", filterOperator: "EvenOrNull" }
				]
			});

			var ref = [refParsedData[0], refParsedData[1]];
			ok(matchObj(ref, collectActualData($widget)), "values are OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}
	});

	test("editor options", function () {
		var $widget;

		try {
			$widget = createWidget({
				data: [[0, 2], [1, 1], [2, 0]],
				showFilter: true,
				culture: "en",
				columns: [
					{ dataType: "number", dataFormatString: "n" },
					{ dataType: "number", dataFormatString: "n0" },
				]
			});

			var options = $.map($widget.find("thead .wijmo-wijinput-numeric"), function (item) {
				return $(item).data("wijmo-wijinputnumber").options;
			});

			ok(options[0].culture === "en" && options[0].decimalPlaces === 2, "#1 is OK");
			ok(options[1].culture === "en" && options[1].decimalPlaces === 0, "#2 is OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}
	});

	test("#38170", function() {
		var $widget,
			result = false;

		try {
			$widget = createWidget({
				data: [[0, 2], [1, 1], [2, 0]],
				columns: [
					{ },
					{ filterOperator: "Equals" }
				],
				dataLoaded: function() {
					result = $(this).wijgrid("dataView").count() === 1;
				}
			});


			$widget.wijgrid("columns")[1].option("filterValue", 1);

			ok(result, "OK");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}
	
	});

	/*test("Issue", function () {
		var $widget, $outerDiv, $fd, $input;

		try {
			$widget = createWidget({
				data: [[0, 2], [1, 1], [2, 0]],
				showFilter: true,
				columns: [
					{ dataKey: null },
					{ dataType: "number", dataFormatString: "n" },
					{ dataType: "number", dataFormatString: "n0" }
				]
			});

			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			$fd = $outerDiv.find(".wijmo-wijgrid-filter:eq(0)");
			$input = $fd.find(".wijmo-wijgrid-filter-input");
			ok($input.outerWidth() + $input.leftBorderWidth() + $input.rightBorderWidth() === $fd.width() - $fd.find(".wijmo-wijgrid-filter-trigger").width(), "width is correct");
		}
		finally {
			if ($widget) {
				$widget.wijgrid("destroy");
				$widget.remove();
			}
		}
	});*/
})(jQuery);
