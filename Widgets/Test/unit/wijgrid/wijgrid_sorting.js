/*globals module,test,createWidget,jQuery,ok,document,same*/
/*
* wijgrid_sorting.js
*/

(function ($) {
	module("wijgrid: sorting");

	var gridData = [
		[00, 21, true],
		[40, 11, true],
		[80, 31, false],
		[60, 31, false],
		[10, 11, false],
		[20, 01, false],
		[50, 31, true],
		[30, 11, true],
		[70, 21, false],
		[90, 11, true]
	];

	test("Sorting: test sorting options.", function () {
		var settings = {
				allowSorting: true,
				data: gridData,
				columns: [
					{ allowSort: false, dataType: "number" },
					{ sortDirection: "descending", dataType: "currency" },
					{ dataType: "boolean" }
				]
			},
			$widget, $outerDiv, rows, row, cells, cell, ths, span;

		try {
			$widget = createWidget(settings);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(1)[0];
			cells = row.cells;

			ok($(cells[0]).find("div").text() === "60.00" && $(cells[1]).find("div").text() === "$31.00" && $(cells[2]).find("input").not(":checked"), "check the value");

			ths = $outerDiv.find("th");
			span = $(ths[1]).find("span.ui-icon-triangle-1-s");
			ok(span.length === 1, "check the mark");

			//click the column 1
			$(ths[0]).find("a").trigger("click");
			ok($(cells[0]).find("div").text() === "60.00" && $(cells[1]).find("div").text() === "$31.00" && $(cells[2]).find("input").not(":checked"), "check the value");
			span = $(ths[0]).find("span.ui-icon-triangle-1-n");
			ok(span.length === 0, "check the mark");

			//click the column 2
			$(ths[1]).find("a").trigger("click");
			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(1)[0];
			cells = row.cells;
			ok($(cells[0]).find("div").text() === "40.00" && $(cells[1]).find("div").text() === "$11.00" && $(cells[2]).find("input").is(":checked"), "check the value");

			ths = $outerDiv.find("th");
			span = $(ths[1]).find("span.ui-icon-triangle-1-n");
			ok(span.length === 1, "check the mark");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Sorting: test multi-sorting.", function () {
		var settings = {
				allowSorting: true,
				data: gridData,
				columns: [
					{ allowSort: false, dataType: "number" },
					{ sortDirection: "descending", dataType: "currency" },
					{ sortDirection: "ascending", dataType: "boolean" }
				]
			},
			$widget, $outerDiv, rows, row, cells, cell, ths, span;

		try {
			$widget = createWidget(settings);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(0)[0];
			cells = row.cells;
			ok($(cells[0]).find("div").text() === "80.00" && $(cells[1]).find("div").text() === "$31.00" && $(cells[2]).find("input").not(":checked"), "check the value");
			ths = $outerDiv.find("th");
			span = $(ths[1]).find("span.ui-icon-triangle-1-s");
			ok(span.length === 1, "check the mark");
			span = $(ths[2]).find("span.ui-icon-triangle-1-n");
			ok(span.length === 1, "check the mark");

			//ctrl click the column 3
			$(ths[2]).find("a").trigger({ type: "click", ctrlKey: true });
			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(0)[0];
			cells = row.cells;
			ok($(cells[0]).find("div").text() === "50.00" && $(cells[1]).find("div").text() === "$31.00" && $(cells[2]).find("input").is(":checked"), "check the value");
			ths = $outerDiv.find("th");
			span = $(ths[1]).find("span.ui-icon-triangle-1-s");
			ok(span.length === 1, "check the mark");
			span = $(ths[2]).find("span.ui-icon-triangle-1-s");
			ok(span.length === 1, "check the mark");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});

	test("Sorting: test sorting events.", function () {
		expect(10);
		var settings = {
				allowSorting: true,
				data: gridData,
				sorting: function (e, args) {
					if (args.column.headerText === "1") {
						ok(args.column.headerText === "1" && args.sortDirection === "ascending", "sorting handler, check the args");
						return false;
					}
					else {
						ok(args.column.headerText === "2" && args.sortDirection === "ascending", "sorting handler,check the args");
					}
				},
				sorted: function (e, args) {
					if (args.column.headerText === "1") {
						ok(false, "checked handler, should not be triggered");
					}
					else {
						ok(args.column.headerText === "2", "checked handler, check the args");
					}
				},
				columns: [
					{ allowSort: false, dataType: "number" },
					{ sortDirection: "descending", dataType: "currency" },
					{ dataType: "boolean" }
				]
			},
			$widget, $outerDiv, rows, row, cells, cell, ths, span;

		try {
			$widget = createWidget(settings);
			$outerDiv = $widget.closest("div.ui-widget.wijmo-wijgrid");

			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(0)[0];
			cells = row.cells;
			ok($(cells[0]).find("div").text() === "80.00" && $(cells[1]).find("div").text() === "$31.00" && $(cells[2]).find("input").not(":checked"), "check the value");

			ths = $outerDiv.find("th");
			span = $(ths[1]).find("span.ui-icon-triangle-1-s");
			ok(span.length === 1, "check the mark");
			$(ths[1]).find("a").trigger("click");

			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(0)[0];
			cells = row.cells;
			ok($(cells[0]).find("div").text() === "80.00" && $(cells[1]).find("div").text() === "$31.00" && $(cells[2]).find("input").not(":checked"), "check the value");

			ths = $outerDiv.find("th");
			span = $(ths[1]).find("span.ui-icon-triangle-1-s");
			ok(span.length === 1, "check the mark");

			$(ths[2]).find("a").trigger({ type: "click", ctrlKey: true });

			rows = $widget.data("wijmo-wijgrid")._rows();
			row = rows.item(0)[0];
			cells = row.cells;
			ok($(cells[0]).find("div").text() === "80.00" && $(cells[1]).find("div").text() === "$31.00" && $(cells[2]).find("input").not(":checked"), "check the value");

			ths = $outerDiv.find("th");
			span = $(ths[1]).find("span.ui-icon-triangle-1-s");
			ok(span.length === 1, "check the mark");

			span = $(ths[2]).find("span.ui-icon-triangle-1-n");
			ok(span.length === 1, "check the mark");
		}
		finally {
			if ($widget) {
				$widget.remove();
			}
		}
	});
} (jQuery));