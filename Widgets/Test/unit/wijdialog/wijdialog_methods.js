﻿/*
* wijdialog_methods.js
*/
(function ($) {

	module("wijdialog:methods");

	test("minimize", function () {
		var dialog = $('<div id="d1" title="title">test</div>').appendTo(document.body).wijdialog({
			autoOpen: true
		});

		dialog.wijdialog('minimize');
		setTimeout(function () {
			var a = dialog;
			ok(!dialog.prev().find('a.wijmo-wijdialog-titlebar-pin').is(':visible'), 'minimize:pin-visibility');
			ok(!dialog.prev().find('a.wijmo-wijdialog-titlebar-refresh').is(':visible'), 'minimize:refresh-visibility');
			ok(!dialog.prev().find('a.wijmo-wijdialog-titlebar-toggle').is(':visible'), 'minimize:toggle-visibility');
			ok(!dialog.prev().find('a.wijmo-wijdialog-titlebar-minimize').is(':visible'), 'minimize:minimize-visibility');
			ok(dialog.prev().find('a.wijmo-wijdialog-titlebar-restore').is(':visible'), 'minimize:restore-visibility');
			ok(dialog.prev().find('a.wijmo-wijdialog-titlebar-maximize').is(':visible'), 'minimize:maximize-visibility');
			ok(dialog.prev().find('a.wijmo-wijdialog-titlebar-close').is(':visible'), 'minimize:close-visibility');

			ok(dialog.parent().css('position') == 'static', 'minimize:position');
			ok(dialog.parent().css('float') == 'left', 'minimize:float');

			start();
			dialog.parent().remove();
		}, 400);
		stop();
	});

	test("maximize", function () {
		var dialog = $('<div title="title">test</div>').appendTo(document.body).wijdialog({
			autoOpen: true
		});

		$("body").css("overflow", "hidden");
		var h = $(document).height();
		var w = $(document).width();
		dialog.wijdialog('maximize');

		setTimeout(function () {
			ok(dialog.prev().find('a.wijmo-wijdialog-titlebar-pin').is(':visible'), 'maximize:pin-visibility');
			ok(dialog.prev().find('a.wijmo-wijdialog-titlebar-refresh').is(':visible'), 'maximize:refresh-visibility');
			ok(dialog.prev().find('a.wijmo-wijdialog-titlebar-toggle').is(':visible'), 'maximize:toggle-visibility');
			ok(dialog.prev().find('a.wijmo-wijdialog-titlebar-minimize').is(':visible'), 'maximize:minimize-visibility');
			ok(dialog.prev().find('a.wijmo-wijdialog-titlebar-restore').is(':visible'), 'maximize:restore-visibility');
			ok(!dialog.prev().find('a.wijmo-wijdialog-titlebar-maximize').is(':visible'), 'maximize:maximize-visibility');
			ok(dialog.prev().find('a.wijmo-wijdialog-titlebar-close').is(':visible'), 'maximize:close-visibility');

			//styles css('position', 'static');css('float', 'left');
			//ok(parseFloat(dialog.parent().outerWidth()) == parseFloat(w), 'maximize:width');
			//ok(parseFloat(dialog.parent().outerHeight()) == parseFloat(h), 'maximize:height'); //has problem when append resizable on maximized 
			ok(parseFloat(dialog.parent().css('top')) == parseFloat($(window).scrollTop()), 'maximize:top');
			ok(parseFloat(dialog.parent().css('left')) == parseFloat($(window).scrollLeft()), 'maximize:left');

			start();
			dialog.parent().remove();
		}, 200);
		stop();
	});

	test("restore", function () {
		var dialog = $('<div title="title">test</div>').appendTo(document.body).wijdialog({
			autoOpen: true
		});
		dialog.wijdialog('restore');
		setTimeout(function () {
			ok(dialog.prev().find('a.wijmo-wijdialog-titlebar-pin').is(':visible'), 'restore:pin-visibility');
			ok(dialog.prev().find('a.wijmo-wijdialog-titlebar-refresh').is(':visible'), 'restore:refresh-visibility');
			ok(dialog.prev().find('a.wijmo-wijdialog-titlebar-toggle').is(':visible'), 'restore:toggle-visibility');
			ok(dialog.prev().find('a.wijmo-wijdialog-titlebar-minimize').is(':visible'), 'restore:minimize-visibility');
			ok(!dialog.prev().find('a.wijmo-wijdialog-titlebar-restore').is(':visible'), 'restore:restore-visibility');
			ok(dialog.prev().find('a.wijmo-wijdialog-titlebar-maximize').is(':visible'), 'restore:maximize-visibility');
			ok(dialog.prev().find('a.wijmo-wijdialog-titlebar-close').is(':visible'), 'restore:close-visibility');

			start();
			dialog.parent().remove();
		}, 200);
		stop();
	});

	test("pin", function () {
		var dialog = $('<div title="title">test </div>').appendTo(document.body).wijdialog({
			autoOpen: true
		});
		dialog.wijdialog('pin');
		setTimeout(function () {
			var a = dialog.parent();
			ok(dialog.parent().data('ui-draggable').options.disabled, 'pin:effect');
			dialog.wijdialog('pin');
			ok(!dialog.parent().data('ui-draggable').options.disabled, 'pin:cancel');
			start();
			dialog.parent().remove();
		}, 200);
		stop();
	});


	test("toggle", function () {
		var dialog = $('<div title="title">test </div>').appendTo(document.body).wijdialog({
			autoOpen: true
		});
		dialog.wijdialog('toggle');
		ok(!dialog.is(':visible'), 'toggle:show');
		dialog.wijdialog('toggle');
		ok(dialog.is(':visible'), 'toggle:hide');

		dialog.parent().remove();

	});

	test("getState", function () {
		ok(true,"referece wijdialog:events - stateChanged");
	});

})(jQuery);
   