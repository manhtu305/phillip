﻿/*
* wijdialog_defaults.js
*/
var wijdialog_defaults = $.extend($.ui.dialog.prototype.options, {

		captionButtons: {},

		collapsingAnimation: null,

		expandingAnimation: null,
		zIndex: 1000,
        stack: true,
		contentUrl: '',
		initSelector: ":jqmData(role='wijdialog')",
		minimizeZoneElementId: '',
		buttonCreating: null,			
		stateChanged: null,
		blur: null
	});

commonWidgetTests('wijdialog', {
	defaults: wijdialog_defaults
});
