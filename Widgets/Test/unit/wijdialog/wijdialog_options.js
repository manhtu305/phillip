﻿/*
* wijdialog_options.js
*/
(function ($) {

	module("wijdialog:options");


	test("contentUrl", function () {
		var dialog = $('<div title="title">test </div>').appendTo(document.body).wijdialog({
			autoOpen: true,
			contentUrl:"ContentUrl.html"
		});
		setTimeout(function () {
			ok(dialog.children('iframe') && dialog.children('iframe').attr('src') == "ContentUrl.html", 'contentUrl');
			start();
			dialog.parent().remove();
		}, 200);
		stop();
	});

	test("disabled options", function () {
	    var dialog = $('<div title="title">test </div>').appendTo(document.body).wijdialog({ autoOpen: true});
	    ok($("div.ui-state-disabled").length === 0, "the disabled div has not appended.");
	    dialog.wijdialog("option", "disabled", true);
	    ok($("div.ui-state-disabled").length === 2, "the disabled div has appended.");
	    dialog.parent().remove();
	});

})(jQuery);
   