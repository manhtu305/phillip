﻿/*
* wijdialog_options.js
*/
(function ($) {

    module("wijdialog:tickets");

    //#18365
    test("18365:dialog disabled block div created when dialog is not shown", function () {
        var dialog = $('<div title="title">test </div>').appendTo(document.body).wijdialog({
            autoOpen: false,
            disabled: true
        });

        ok($(".ui-state-disabled").length == 0, "disabled is not created");
        dialog.wijdialog("open");
        ok($(".ui-state-disabled").length != 0, "disabled is created after dialog shown");
        dialog.remove();
    });

    //#16696
    test("16696:“Pin” button is not working correctly when C1Dialog is in Maximize state", function () {
        var dialog = $('<div title="title">test </div>').appendTo(document.body).wijdialog({
            autoOpen: true
        });
        dialog.wijdialog("maximize");
        dialog.wijdialog("pin");
        var widget = dialog.wijdialog("widget");

        ok(widget.draggable("option", "disabled") == true, "disabled is draggable!");
        dialog.remove();
    });

    //#??
    //test("bug on drag & resize when user set option contentURL", function () {
    //	var dialog = $('<div title="title">test </div>').appendTo(document.body).wijdialog({
    //		autoOpen: true,
    //		contentUrl: "http://google.com"
    //	});

    //	var widget = dialog.wijdialog("widget");

    //	ok($.isFunction(widget.data("draggable").options.drag), "draggable events bound");
    //	ok($.isFunction(widget.data("resizable").options.resize), "draggable events bound");
    //	dialog.remove();
    //});
    test("#36981", function () {
        var dialog = $('<div title="title">test </div>').appendTo(document.body).wijdialog({
            modal: true
        });

        var widget = dialog.data("wijmo-wijdialog");

        ok(widget.overlay.css("z-index") !== "", "model layer added the z-index");
        dialog.remove();
    });

    test("dialog is not dim although it is disabled", function () {
        var dialog = $('<div title="title">test </div>').appendTo(document.body).wijdialog({
            disabled: true
        });
        ok(dialog.data("wijmo-wijdialog").uiDialog.is("." + $.wijmo.widget.prototype.options.wijCSS.stateDisabled), "The dialog added the dim state CSS class");
        dialog.remove();
    });

    if ($.browser.msie) {
        test("when draggable is false and disabled is true, exception is throw in IE", function () {
            var dialog = $('<div title="title">test </div>').appendTo(document.body).wijdialog({
                disabled: true,
                draggable: false
            });
            ok(true, "The dialog works well in IE");
            dialog.remove();
        })
    }

    test("appendTo option is not works correctly", function () {
        var container = $("<div id='testContainer'></div>").appendTo("body"),
            dialog = $('<div title="title">test </div>').appendTo(document.body);
        dialog.wijdialog();
        ok(dialog.data("wijmo-wijdialog").uiDialog.parent().is("body"), "the dialog's parent element is body");
        dialog.remove();
        dialog.wijdialog({ appendTo: container });
        ok(dialog.data("wijmo-wijdialog").uiDialog.parent().is("#testContainer"), "the dialog's parent element is the test container");
        dialog.wijdialog("minimize");
        setTimeout(function () {
            dialog.wijdialog("restore");
            setTimeout(function () {
                ok(dialog.data("wijmo-wijdialog").uiDialog.parent().is("#testContainer"), "minimize to normal, the dialog's parent element is the test container");
                dialog.wijdialog("maximize");
                setTimeout(function () {
                    ok(dialog.data("wijmo-wijdialog").uiDialog.parent().is("#testContainer"), "normal to maximize, the dialog's parent element is the test container");
                    dialog.remove();
                    container.remove();
                    start();
                }, 500)	            
            }, 500)
        }, 500)
        stop();
    });

    test("stack option can't work", function () {
        var dialog = $('<div title="title">test </div>').appendTo(document.body).wijdialog({
            stack: true
        }),
        dialog1 = $('<div title="title">test </div>').appendTo(document.body).wijdialog({
            stack: false
        });
        ok(dialog.data("wijmo-wijdialog").uiDialog.zIndex() > dialog1.data("wijmo-wijdialog").uiDialog.zIndex(), "the first dialog is in front of the second dialog.");
        dialog.remove();
        dialog1.remove();
    });

    //#30325
    test("30325:Dialog is resized and  does not shown properly when “Show Animation” is set to Clip,Scale or Size", function () {
        var dialog = $("<div title='Basic dialog'><p>This is the default dialog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p></div>")
            .appendTo(document.body);
        dialog.wijdialog({
            autoOpen: true,
            show: 'clip',
            hide: 'explode',
            collapsingAnimation: { animated: "puff", duration: 300, easing: "easeOutExpo" },
            expandingAnimation: { animated: "highlight", duration: 300, easing: "easeOutExpo" },
            width: 500
        });
        setTimeout(function () {
            dialog.wijdialog("toggle");
            dialog.wijdialog("close");
            setTimeout(function () {
                dialog.wijdialog("open");	            
                ok(dialog.data("wijmo-wijdialog")._toggleHeight !== "0px", "The height of the dialog should not be 0.");
                dialog.remove();
                start();
            }, 1000);
        }, 1000);
        stop();
    });

    test("#40772", function () {
        dialog = $('<div title="title">test </div>').appendTo(document.body),
        dialog.wijdialog();
        dialog.wijdialog("destroy");
        ok(!dialog.data("wijmo-wijdialog"), "the dialog object has removed from data object.");
        dialog.remove();
    });

    test("#41538", function () {
        var dialog = $('<div title="title">test </div>').appendTo(document.body).wijdialog(),
            container = $("<div id='diTest'></div>").appendTo("body"),
            widget = dialog.data("wijmo-wijdialog"),
            dialogElement = widget.uiDialog, visibleButtons;

        dialogElement.find(".wijmo-wijdialog-titlebar-minimize").simulate("click");
        setTimeout(function () {
            visibleButtons = dialogElement.find(".wijmo-wijdialog-captionbutton:visible").length;
            dialog.wijdialog("option", "minimizeZoneElementId", "diTest");
            ok(dialogElement.find(".wijmo-wijdialog-captionbutton:visible").length === visibleButtons, "the count of visible buttons is not changed.");
            ok(dialogElement.parent().is("#diTest"), "the dialog elements is moved to minimizeZoneElement");
            dialog.remove();
            start();
        }, 200);
        stop();
        
    });

    test("#41530", function () {
        var dialog = $('<div title="title">test </div>').appendTo(document.body).wijdialog({
            buttons: [{
                text: "Print",
                click: function () {
                    $("#dialog").jqprint();
                }
            },
                {
                    text: "Cancel",
                    click: function () {
                        $(this).wijdialog("close");
                    }
                }
            ]
        }),
        widget = dialog.data("wijmo-wijdialog"),
        dialogElement = widget.uiDialog, buttons;
        buttons = dialogElement.find(".ui-dialog-buttonpane .ui-button");
        dialog.wijdialog({
            maxHeight: 400
        });
        setTimeout(function () {
            ok(dialogElement.find(".ui-dialog-buttonpane .ui-button").length === buttons.length, "the buttons is still on the page after set maxHeight option");
            dialog.wijdialog({
                maxWidth: 600
            });
            setTimeout(function () {
                ok(dialogElement.find(".ui-dialog-buttonpane .ui-button").length === buttons.length, "the buttons is still on the page after set maxWidth option");
                dialog.remove();
                start();
            }, 200);
        }, 200)
        stop();
    });

    test("#41526", function () {
        var dialog = $('<div title="title">test </div>').appendTo(document.body).wijdialog(),
            widget = dialog.data("wijmo-wijdialog"),
            uidialog = widget.uiDialog,
            width = uidialog.width(),
            height = uidialog.height(),
            offset = uidialog.offset();
        uidialog.find(".ui-resizable-se").simulate("mouseover").simulate("drag", { dx: 20, dy: 20 });
        dialog.wijdialog("reset");
        ok(uidialog.width() === width, "the width has reset.");
        // the height is not set to uidialog directly. when calculate the height, it will less or large than the o.height.
        ok(Math.abs(uidialog.height() - height) < 3, "the height has reset");


        uidialog.find(".ui-resizable-se").simulate("mouseover").simulate("drag", { dx: 50, dy: 50 });
        dialog.wijdialog("minimize");
        setTimeout(function () {
            dialog.wijdialog("reset");
            setTimeout(function () {
                ok(uidialog.width() === width, "the width has reset.");
                // the height is not set to uidialog directly. when calculate the height, it will less or large than the o.height.
                ok(Math.abs(uidialog.height() - height) < 4, "the height has reset");
                dialog.remove();
                start();
            }, 400);
        }, 400);
        stop();
    });

    test("#42692", function () {
        var content = $('<div id="dialog1" title="Dialog1"></div><div id="dialog2" title="Dialog2"></div>').appendTo(document.body),
            dialog1 = $('#dialog1').wijdialog({ stack: false }),
            dialog2 = $('#dialog2').wijdialog({ stack: false }),
            result1, result2;
        dialog1.wijdialog({ stack: true });
        result1 = dialog1.zIndex() > dialog2.zIndex();
        ok(result1, "Dialog1 is on the top");
        dialog1.wijdialog({ stack: false });
        result2 = dialog1.zIndex() <= dialog2.zIndex();
        ok(result2, "Dialog2 is on the top");
        dialog1.remove();
        dialog2.remove();
        content.remove();
    });

    test("#44245", function () {
        var content = $('<form id="form0"><div id="dialog1" title="Modal dialog"><input id="Submit1" type="submit" value="submit" /><input id="Button2" type="button" value="button" /></div></form>').appendTo(document.body),
            dialog1 = $('#dialog1').wijdialog({ modal: true, autoOpen: true, width: 500, height: 500 }),
            widgetData = dialog1.data("wijmo-wijdialog");
        ok(widgetData.uiDialog.parent().is("body"), "Dialog1 is removed outside of the parent form.");
        ok((widgetData.form)[0].onsubmit != null, "Inputs in dialog will be moved into form when submit.");
        dialog1.remove();
        content.remove();
    });

    if ($.browser.msie && parseInt($.browser.version) === 9) {
        test("#42672", function () {
            var dialog = $('<div title="title">test </div>').wijdialog({
                show: "blind",
                hide: "blind",
                contentUrl: "ContentUrl.html",
                autoOpen: false
            });
            dialog.wijdialog("open");
            setTimeout(function () {
                ok(true, "No exception is thrown.");
                dialog.wijdialog("close");
                setTimeout(function () {
                    ok(true, "No exception is thrown.");
                    dialog.remove();
                    start();
                }, 600);
            }, 600);
            stop();
        });
    }

    test("#65269", function () {
        var dialog65269 = $('<div id="d65269" title="title">test</div>').appendTo(document.body).wijdialog({
            autoOpen: true,
            contentUrl: "ContentUrl.html",
        }), closeButton, titleBar;

        dialog65269.wijdialog('minimize');
        closeButton = dialog65269.data('wijmo-wijdialog').closeButton;
        titleBar = dialog65269.data('wijmo-wijdialog').uiDialogTitlebar;

        setTimeout(function () {
            closeButton.simulate("mouseover");
            closeButton.simulate("click");

            ok(!titleBar.is(":visible"), "Dialog has Closed !");
            dialog65269.remove();
            start();
        }, 200);
        stop();
    });

    test("#73438", function () {
        var dialog = $('<div id="73438" title="title">test</div>').appendTo(document.body).wijdialog({position:"left"});

        ok($("#73438").parent(".wijmo-wijdialog").css("left") === "0px", "left position is take effect!");
        $("#73438").wijdialog("option", "position", "top");
        ok($("#73438").parent(".wijmo-wijdialog").css("top") === "0px", "top position is take effect!");
        $("#73438").wijdialog("option", "position", ['left', 'top']);
        ok($("#73438").parent(".wijmo-wijdialog").css("left") === "0px" && $("#73438").parent(".wijmo-wijdialog").css("top") === "0px", "left and top position is take effect!");
        $("#73438").remove();
    });

    test("#73774", function () {
        var dialog73774 = $('<div id="dialog73774" title="title">Input in dialog: <input type="text" id="innerInput1" /></div>').appendTo(document.body).wijdialog(),
            innerInput = $("#innerInput1"), result = true;
        innerInput.bind("blur", function () {
            result = false;
        })
        innerInput.simulate("mousedown").simulate("click");

        innerInput.simulate("mousedown");

        setTimeout(function () {
            ok(result);
            dialog73774.remove();
            start();
        }, 100)
        stop();

    });


    test("#74404", function () {
        var dialog73774 = $('<div id="dialog" title="title">Input in dialog: <input type="text" id="innerInput1" /></div>').appendTo(document.body).wijdialog({ autoOpen: false}),
            innerInput = $("#innerInput1"), result = 0;

        innerInput.addClass("wijmo-wijobserver-visibility").wijAddVisibilityObserver(function () {
            result++;
        })
        $("#dialog").wijdialog("open");

        window.setTimeout(function () {
            $("#dialog").wijdialog("minimize");
            $("#dialog").wijdialog("close");
            setTimeout(function () {
                $("#dialog").wijdialog("open");

                setTimeout(function () {
                    $("#dialog").wijdialog("restore");
                    ok(result === 1, "wijmovisibilitychanged has fired only once.");
                    dialog73774.remove();
                    start();
                }, 500);
            }, 500);
        }, 500);
        stop();
    });

    test("#74035", function () {
        var dialog = $('<div id="dialog" title="Modal dialog">' +
                '<div>' +
                '</div>' +
            '</div>')
            .appendTo("body")
            .wijdialog({
                width: 500,
                height: 500
            });
        setTimeout(function () {
            $("#dialog").wijdialog("minimize");
            $("#dialog").wijdialog("option", "contentUrl", "http://www.google.com");
            setTimeout(function () {
                $("#dialog").wijdialog("restore");
                ok($("#dialog").width() === 500, "the dialog show correctly.");
                dialog.remove();
                start();
            }, 500);
        }, 500);
        stop();
    });

    test("#65640", function () {
        var dialog = $('<div id="65640" title="title">test</div>').appendTo(document.body).wijdialog({ width: 500, disabled: true });
        $("#65640").wijdialog("option", "width", 800);
        ok($("div[class='ui-state-disabled']").width() > 800, "the disabled width is right!");
        ok($("div[class='ui-state-disabled']").offset.top === $(".wijmo-wijdialog").offset.top, "the disabled element's postion is right!");
        $("#65640").wijdialog("close");
        $("#65640").remove();
    });

    test("#78660", function () {
        var dialog78660 = $('<div id="dialog" title="title">Input in dialog: <input type="text" id="input78660" /></div>').appendTo(document.body).wijdialog({ autoOpen: false }),
            innerInput = $("#input78660"), exceptWidth = 0, dialogWidth = 0;

        innerInput.addClass("wijmo-wijobserver-visibility").wijAddVisibilityObserver(function () {
            dialogWidth = dialog78660.width();
        });
        dialog78660.wijdialog("open");

        window.setTimeout(function () {
            exceptWidth = dialog78660.width();
            dialog78660.wijdialog("minimize");
            dialog78660.wijdialog("close");
            setTimeout(function () {
                dialog78660.wijdialog("open");

                setTimeout(function () {
                    dialog78660.wijdialog("maximize");
                    ok(dialogWidth === exceptWidth, "wijmovisibilitychanged has fired.");

                    dialog78660.remove();
                    start();
                }, 500);
            }, 500);
        }, 500);
        stop();
    });

    test("#88083", function () {
        var $dialog = $('<div id="88083" title="title">test</div>').appendTo(document.body).wijdialog({ show: 0, hide: 0, width: 500, disabled: true }),
            $mask = $dialog.closest(".wijmo-wijdialog").nextAll("div.ui-state-disabled"),
            top = $mask.offset().top,
            left = $mask.offset().left;

        $dialog.wijdialog("close");
        ok($mask.css("display") === "none", "mask is hidden.");
        $dialog.wijdialog("open");
        ok($mask.css("display") !== "none" && $mask.offset().top === top && $mask.offset().left === left, "mask position is correct.");
        $dialog.wijdialog("destroy");
        $dialog.remove();
    });

    test("caption button's tooltip is incorrect-default", function () {
        var dialog = $('<div id="tooltipdialog" title="title">test</div>').appendTo(document.body).wijdialog({ width: 500 }), dialogOutterEle = $("#tooltipdialog").parent();
        dialog.wijdialog("open");
        ok($(".wijmo-wijdialog-titlebar-pin", dialogOutterEle).attr("title") === "Pin", "pin is right.");
        ok($(".wijmo-wijdialog-titlebar-refresh", dialogOutterEle).attr("title") === "Refresh", "refresh is right.");
        ok($(".wijmo-wijdialog-titlebar-toggle", dialogOutterEle).attr("title") === "Toggle", "toggle is right.");
        ok($(".wijmo-wijdialog-titlebar-close", dialogOutterEle).attr("title") === "Close", "close is right.");
        ok($(".wijmo-wijdialog-titlebar-minimize", dialogOutterEle).attr("title") === "Minimize", "minimize is right.");
        ok($(".wijmo-wijdialog-titlebar-maximize", dialogOutterEle).attr("title") === "Maximize", "maximize  is right.");
        $("#tooltipdialog").wijdialog("close");
        $("#tooltipdialog").remove();
    });

    test("caption button's tooltip is incorrect-title setting", function () {
        var dialog = $('<div id="tooltipdialog2" title="title">test</div>').appendTo(document.body).wijdialog({
            width: 500, captionButtons: {
                pin: { title: "Pin1" }, refresh: { title: "Refresh1" },
                toggle: { title: "Toggle1" }, close: { title: "Close1" },
                minimize: { title: "Minimize1" }, maximize: { title: "Maximize1" }
            }
        }), dialogOutterEle = $("#tooltipdialog2").parent();

        dialog.wijdialog("open");

        ok($(".wijmo-wijdialog-titlebar-pin", dialogOutterEle).attr("title") === "Pin1", "pin is right.");
        ok($(".wijmo-wijdialog-titlebar-refresh", dialogOutterEle).attr("title") === "Refresh1", "refresh is right.");
        ok($(".wijmo-wijdialog-titlebar-toggle", dialogOutterEle).attr("title") === "Toggle1", "toggle is right.");
        ok($(".wijmo-wijdialog-titlebar-close", dialogOutterEle).attr("title") === "Close1", "close is right.");
        ok($(".wijmo-wijdialog-titlebar-minimize", dialogOutterEle).attr("title") === "Minimize1", "minimize is right.");
        ok($(".wijmo-wijdialog-titlebar-maximize", dialogOutterEle).attr("title") === "Maximize1", "maximize  is right.");
        $("#tooltipdialog2").wijdialog("close");
        $("#tooltipdialog2").remove();
    });

    test("#88788", function () {
        var dialog = $('<div id="dialog88788">Content</div>').appendTo(document.body).wijdialog({
            width: 500, height: 500
        }), closeBtn = $(".wijmo-wijdialog-titlebar-close", $("#dialog88788").parent()),
            uiDialog = dialog.data("wijmo-wijdialog").uiDialog;

        closeBtn.focus();
        closeBtn.simulate("mousedown");

        ok(!uiDialog.is(":focus"), "If mousedown is on the close btn, foucs should not changed to the Dialog immediately.");
        dialog.remove();
    });

    test("#93366", function () {
        var $dialog = $('<div id="93366" title="title"></div>').appendTo("body").wijdialog({ autoOpen: false, modal: true }),
            dialogOutterEle = $("#93366").parent();
        $('#93366').wijdialog('open');
        ok($(".ui-widget-overlay").css("position") === "fixed", "The overlay div is correct.");
        $("#93366").wijdialog("close").remove();
    });

    test("#94810", function () {
        var $dialog = $('<div id="94810" title="title"></div>').appendTo("body").wijdialog({ autoOpen: false, modal: true });
        $dialog.wijdialog("option", "contentUrl", null);
        ok(true, "no exception");
        $dialog.wijdialog("option", "contentUrl", "");
        ok(true, "no exception");
        $dialog.remove();
    });

    if ($.browser.msie) {
        test("#129305", function () {
            var $dialog = $('<div id="129305" title="title"></div>').appendTo("body").wijdialog({ autoOpen: false, modal: true });
            $dialog.wijdialog('open');
            $dialog.wijdialog('open');
            ok(true, "no exception");
            $dialog.remove();
        });
    }

})(jQuery);