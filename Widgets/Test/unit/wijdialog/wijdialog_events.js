﻿/*
* wijdialog_events.js
*/
(function ($) {
	module("wijdialog:events");

	test("buttoncreating", function () {
		var count = 0;
		var dialog = $('<div title="title">test </div>').appendTo(document.body).wijdialog({
			autoOpen: true,
			buttonCreating: function () { count++; }
		});

		ok(count == 1, 'buttonCreating');
		dialog.remove();
	});

	test("blur", function () {
		var count = 0;
		var dialog = $('<div title="title">test </div>').appendTo(document.body).wijdialog({
			autoOpen: true,
			blur: function () {
				count++;

			}
		}).parent();
		dialog.simulate('mouseover');
		dialog.focus();
		dialog.simulate('mouseout');
		dialog.blur();
		ok(count > 0, 'blur');
		dialog.remove();
	});

	test("stateChanged", function () {
		var count = 0, eventData;
		var dialog = $('<div title="title">test </div>').appendTo(document.body).wijdialog({
			autoOpen: true,
			stateChanged: function (event, data) {
				count++;
				eventData = data;
			}
		});
		dialog.wijdialog("minimize");

		setTimeout(function () {

			ok(count == 1, 'stateChanged：minimize');
			ok(eventData.state === "minimized", 'stateChanged:minimize');

			dialog.wijdialog("maximize");
			setTimeout(function () {
				ok(count == 2, 'stateChanged：maximize');
				ok(eventData.state === "maximized", 'stateChange:maximize');

				dialog.wijdialog("restore");
				setTimeout(function () {
					ok(count == 3, 'stateChanged：restore');
					ok(eventData.state === "normal", 'stateChange:restore');
					dialog.remove();
					start();
				}, 300);
			}, 300);
		}, 300);

		stop();
	});

})(jQuery);
   