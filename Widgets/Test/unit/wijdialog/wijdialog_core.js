/*
 * wijdialog_core.js
 */


(function ($) {

	module("wijdialog: core");
	test("data", function () {
		// test disconnected element
		var dialog = $('<div title="title">test </div>').appendTo(document.body)
		.wijdialog({ autoOpen: true }).parent();

		ok(!!dialog.data("ui-draggable"), "draggable: apply");
		ok(!!dialog.data("ui-resizable"), "resizable: apply");

		ok(dialog.find(".wijmo-wijdialog-titlebar-pin").length, "caption button: pin created!");
		ok(dialog.find(".wijmo-wijdialog-titlebar-toggle").length,
		"caption button: toggle created!");

		ok(dialog.find(".wijmo-wijdialog-titlebar-pin").length, "caption button: pin created!");


		dialog.remove();
	});

})(jQuery);

