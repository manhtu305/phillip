(function($) {
	module("wijpager: core");

  function createWidget(settings){
      return $("<div />").wijpager(settings).appendTo(document.body);
  }

  test("create and destroy", function() {
     var $outer = $("<div><div></div></div>").appendTo(document.body),
			$inner = $outer.find(">div").addClass("myClass"),
			html = $outer.html();

		$inner
			.wijpager()
			.wijpager("destroy");

		ok(html === $outer.html(), "content restored");

		$outer.remove();
  })

  test("nextPrevious", function() {
      ok(true, "=> { pageCount: 0, pageIndex: 0 }");
      var $widget = createWidget({ pageCount: 0, pageButtonCount: 10, pageIndex: 0, mode: "nextPrevious"});
      testBaseClasses($widget);
      testNextPrev($widget);

      ok(true, "=> { pageCount: 1 }");
      $widget.wijpager( { pageCount: 1 } );
      testNextPrev($widget);

      ok(true, "=> { pageCount: 10 }");
      $widget.wijpager( { pageCount: 10 } );
      testNextPrev($widget);

      ok(true, "=> { pageIndex: 1 }");
      $widget.wijpager( { pageIndex: 1 } );
      testNextPrev($widget);

      ok(true, "=> { pageIndex: 9 }");
      $widget.wijpager( { pageIndex: 9 } );
      testNextPrev($widget);

      $widget.wijpager("destroy");
      $widget.remove();
  })

  test("nextPreviousFirstLast", function() {
      ok(true, "=> { pageCount: 0, pageIndex: 0 }");
      var $widget = createWidget({ pageCount: 0, pageButtonCount: 10, pageIndex: 0, mode: "nextPreviousFirstLast"});
      testBaseClasses($widget);
      testNextPrev($widget, true);

      ok(true, "=> { pageCount: 1 }");
      $widget.wijpager( { pageCount: 1 } );
      testNextPrev($widget, true);

      ok(true, "=> { pageCount: 10 }");
      $widget.wijpager( { pageCount: 10 } );
      testNextPrev($widget, true);

      ok(true, "=> { pageIndex: 1 }");
      $widget.wijpager( { pageIndex: 1 } );
      testNextPrev($widget, true);

      ok(true, "=> { pageIndex: 8 }");
      $widget.wijpager( { pageIndex: 8 } );
      testNextPrev($widget, true);

      ok(true, "=> { pageIndex: 9 }");
      $widget.wijpager( { pageIndex: 9 } );
      testNextPrev($widget, true);

      $widget.wijpager("destroy");
      $widget.remove();
  })

  test("numeric", function() {
      ok(true, "=> { pageCount: 0, pageIndex: 0 }");
      var $widget = createWidget({ pageCount: 0, pageButtonCount: 5, pageIndex: 0, mode: "numeric"});
      testNumeric($widget);

      ok(true, "=> { pageCount: 1 }");
      $widget.wijpager( { pageCount: 1 } );
      testNumeric($widget);

      ok(true, "=> { pageCount: 20 }");
      $widget.wijpager( { pageCount: 20 } );
      testNumeric($widget);

      ok(true, "=> { pageIndex: 5 }");
      $widget.wijpager( { pageIndex: 5 } );
      testNumeric($widget);

      ok(true, "=> { pageIndex: 9 }");
      $widget.wijpager( { pageIndex: 9 } );
      testNumeric($widget);

      ok(true, "=> { pageIndex: 10 }");
      $widget.wijpager( { pageIndex: 10 } );
      testNumeric($widget);

      ok(true, "=> { pageIndex: 15 }");
      $widget.wijpager( { pageIndex: 15 } );
      testNumeric($widget);

      ok(true, "=> { pageIndex: 19 }");
      $widget.wijpager( { pageIndex: 19 } );
      testNumeric($widget);

      $widget.wijpager("destroy");
      $widget.remove();
  })

  test("numericFirstLast", function() {
      ok(true, "=> { pageCount: 0, pageIndex: 0 }");
      var $widget = createWidget({ pageCount: 0, pageButtonCount: 5, pageIndex: 0, mode: "numericFirstLast"});
      testNumeric($widget, true);

      ok(true, "=> { pageCount: 1 }");
      $widget.wijpager( { pageCount: 1 } );
      testNumeric($widget, true);

      ok(true, "=> { pageCount: 20 }");
      $widget.wijpager( { pageCount: 20 } );
      testNumeric($widget, true);

      ok(true, "=> { pageIndex: 5 }");
      $widget.wijpager( { pageIndex: 5 } );
      testNumeric($widget, true);

      ok(true, "=> { pageIndex: 9 }");
      $widget.wijpager( { pageIndex: 9 } );
      testNumeric($widget, true);

      ok(true, "=> { pageIndex: 10 }");
      $widget.wijpager( { pageIndex: 10 } );
      testNumeric($widget, true);

      ok(true, "=> { pageIndex: 15 }");
      $widget.wijpager( { pageIndex: 15 } );
      testNumeric($widget, true);

      ok(true, "=> { pageIndex: 19 }");
      $widget.wijpager( { pageIndex: 19 } );
      testNumeric($widget, true);

      $widget.wijpager("destroy");
      $widget.remove();
  })

  function testBaseClasses($widget) {
      var mode = $widget.wijpager("option", "mode");

      ok($widget.hasClass("ui-widget wijmo-wijpager ui-helper-clearfix"), mode + ": widget css is valid");

      var container = $widget.find("> ul:first");
      ok(container.hasClass("ui-list ui-corner-all ui-widget-content ui-helper-clearfix"), mode + ": container css is valid");
  };

  function testNumeric($widget, addFirstLast) {
      var pageCount = $widget.wijpager("option", "pageCount");
      var pageIndex = $widget.wijpager("option", "pageIndex");
      var pageButtonCount = $widget.wijpager("option", "pageButtonCount");

      var currentPage = pageIndex + 1;
      var startPageNumber = 1;
      var endPageNumber = Math.min(pageCount, pageButtonCount);

      if (currentPage > endPageNumber) {
          startPageNumber = (Math.floor(pageIndex / pageButtonCount)) * pageButtonCount + 1;

          endPageNumber = startPageNumber + pageButtonCount - 1;
          endPageNumber = Math.min(endPageNumber, pageCount);

          if (endPageNumber - startPageNumber + 1 < pageButtonCount) {
              startPageNumber = Math.max(1, endPageNumber - pageButtonCount + 1);
          }
      }

      var buttons = $widget.find("> ul > li");

      if (pageCount === 1) {
          ok(buttons.length === 1, "number of buttons is valid (" + buttons.length + ")");
      } else {
          var btnCount = 0;

          // first + "..." buttons
          if (startPageNumber != 1) {
              // first button
              if (addFirstLast) {
                  btnCount++;
              }

              btnCount++;
          }

          // page numbers buttons
          btnCount += (endPageNumber - startPageNumber) + 1;

          // "..." + last buttons
          if (pageCount > endPageNumber) {
              btnCount++;

              // last button
              if (addFirstLast) {
                  btnCount++;
              }
          }

          ok(buttons.length === btnCount, "number of buttons is valid (" + buttons.length + ")");

          var btnIndex = 0;

          // first + "..." buttons
          if (startPageNumber != 1) {
              // first button
              if (addFirstLast) {
                var $btn = $widget.find("> ul > li:eq(" + btnIndex + ")");
                testBtn($btn, "firstBtn", false, false, $widget.wijpager("option", "firstPageText"), $widget.wijpager("option", "firstPageClass"));
                btnIndex++;
              }

              // "..." button
              var $btn = $widget.find("> ul > li:eq(" + btnIndex + ")");
              testBtn($btn, "'...'", false, false, "...", "");
              btnIndex++;
           }

          // page numbers buttons
          for (var i = startPageNumber; i <= endPageNumber; i++) {
              var active = (i === currentPage);

              var $btn = $widget.find("> ul > li:eq(" + btnIndex + ")");
              testBtn($btn, "btn #" + i, active, active, i.toString(), "");
              btnIndex++;
          }

          // "..." + last buttons
          if (pageCount > endPageNumber) {
              var $btn = $widget.find("> ul > li:eq(" + btnIndex + ")");
              testBtn($btn, "'...'", false, false, "...", "");
              btnIndex++;

              // last button
              if (addFirstLast) {
                var $btn = $widget.find("> ul > li:eq(" + btnIndex + ")");
                testBtn($btn, "firstBtn", false, false, $widget.wijpager("option", "lastPageText"), $widget.wijpager("option", "lastPageClass"));
                btnIndex++;
              }
          }

      }
  };


  function testNextPrev($widget, addFirstLast) {
      var pageCount = $widget.wijpager("option", "pageCount");
      var pageIndex = $widget.wijpager("option", "pageIndex");

      var buttons = $widget.find("> ul > li");

      if (pageCount === 1) {
          ok(buttons.length === 0, "number of buttons is valid (" + buttons.length + ")");
      } else {
          var btnCount = 0;

          if (pageIndex) {
              if (addFirstLast) { // first
                  btnCount++;
              }
              btnCount++; // prev
          }

          if (pageIndex + 1 < pageCount) {
              if (addFirstLast) { // next
                  btnCount++;
              }
              btnCount++; // last
          }

          ok(buttons.length === btnCount, "number of buttons is valid (" + buttons.length + ")");

          var btnIndex = 0;

          if (pageIndex) {
              if (addFirstLast) { // first
                  var $btn = $widget.find("> ul > li:eq(" + btnIndex + ")");
                  testBtn($btn, "firstBtn", false, false, $widget.wijpager("option", "firstPageText"), $widget.wijpager("option", "firstPageClass"));
                  btnIndex++;
              }

              // prev
              var $btn = $widget.find("> ul > li:eq(" + btnIndex + ")");
              testBtn($btn, "previousBtn", false, false, $widget.wijpager("option", "previousPageText"), $widget.wijpager("option", "previousPageClass"));
              btnIndex++; 
          }

          if (pageIndex + 1 < pageCount) {
              // next
              var $btn = $widget.find("> ul > li:eq(" + btnIndex + ")");
              testBtn($btn, "nextBtn", false, false, $widget.wijpager("option", "nextPageText"), $widget.wijpager("option", "nextPageClass"));
              btnIndex++;

              if (addFirstLast) {
                  var $btn = $widget.find("> ul > li:eq(" + btnIndex + ")");
                  testBtn($btn, "lastBtn", false, false, $widget.wijpager("option", "lastPageText"), $widget.wijpager("option", "lastPageClass"));
              }
          }
      }
  }

  function testBtn($btn, logMessage, active, disabled, text, css) {
      ok($btn.length > 0, logMessage + ": button created");

      ok($btn.hasClass("wijmo-wijpager-button ui-corner-all"), logMessage + ": button css is valid");
      ok(active
          ? $btn.hasClass("ui-state-active")
          : $btn.hasClass("ui-state-default"), logMessage + ": button state is valid (" + (active ? "active" : "default") + ")");

      if (disabled) {
          var $ctrl = $btn.find("> span");
          ok($ctrl.length === 1, logMessage + ": button is non-clickable");
      } else {
          var $ctrl = null;

          if (css) {
              $ctrl = $btn.find("> span");
              ok($ctrl.hasClass("ui-icon " + css), logMessage + ": button icon is valid");
          } else {
              $ctrl = $btn.find("> a");
          }

          ok($ctrl.text() === text, logMessage + ": button text is valid (\"" + text +  "\")");
      }
  }


})(jQuery);

