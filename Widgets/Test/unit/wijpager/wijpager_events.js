(function($) {
	module("wijpager: events");

  function createWidget(settings){
      return $("<div />").wijpager(settings).appendTo(document.body);
  }

	test("pageIndexChanging, pageIndexChanged", function() {
      var pageindexchangingCalled = false;
      var pageindexchangedCalled = false;
      var cancel = true;
      var args1 = null;
      var args2 = null;

      var $widget = createWidget({ pageCount: 150, pageButtonCount: 10, pageIndex: 0, mode: "numeric",
          pageIndexChanging: function(e, p) { pageindexchangingCalled = true; args1 = p; return !cancel; },
          pageIndexChanged: function(e, p) { pageindexchangedCalled = true; args2 = p; }
      });

      $widget.find("[title=\"...\"]").simulate("mouseover").simulate("click").simulate("mouseout");
      ok(pageindexchangingCalled, "pageIndexChanging raised, cancelled");
      ok(args1.newPageIndex === 10, "pageIndexChanging args are OK");
      ok(!pageindexchangedCalled, "pageIndexChanging not raised");

      cancel = false;
      pageindexchangingCalled = false;
      pageindexchangedCalled = false;
      $widget.find("[title=\"1\"]").simulate('mouseover').simulate('click').simulate('mouseout');
      ok(!pageindexchangingCalled, "pageIndexChanging not raised");
      ok(!pageindexchangedCalled, "pageIndexChanged not raised");

      $widget.find("[title=\"...\"]").simulate("mouseover").simulate("click").simulate("mouseout");
      ok(pageindexchangingCalled, "pageIndexChanging raised");
      ok(args1.newPageIndex === 10, "pageIndexChanging args are OK");
      ok(pageindexchangedCalled, "pageIndexChanged not raised");
      ok(args2.newPageIndex === 10, "pageIndexChanged args are OK");

      $widget.remove();
	});


})(jQuery);

