commonWidgetTests("wijpager", {
	defaults: {
		initSelector: ":jqmData(role='wijpager')",
		wijCSS: $.extend({}, $.wijmo.wijCSS, {
				pagerButton: "wijmo-wijpager-button"
		}),
		wijMobileCSS: {
            header: "ui-header ui-bar-a",
            content: "ui-body-b",
            stateDefault: "ui-btn ui-btn-b",
            stateHover: "ui-btn-down-b",
            stateActive: "ui-btn-down-c"
        },
	
		create: null,
		disabled: false,
		firstPageClass: "ui-icon-seek-first",
		firstPageText: "First",
		lastPageClass: "ui-icon-seek-end",
		lastPageText: "Last",
		mode: "numeric",
		nextPageClass: "ui-icon-seek-next",
		nextPageText: "Next",
		pageButtonCount: 10,
		previousPageClass: "ui-icon-seek-prev",
		previousPageText: "Previous",
		pageCount: 1,
		pageIndex: 0,
		pageIndexChanging: null,
		pageIndexChanged: null
	}
});
