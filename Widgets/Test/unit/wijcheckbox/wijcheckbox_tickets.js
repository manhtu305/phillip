﻿/*
* wijcheckbox_tickets.js
*/
(function ($) {
	module("wijcheckbox:tickets");
	test("#101466", function () {
		var $div = $("<div><label><input type='checkbox' id='mdy' />Mandalay</label><br /><label><input type='checkbox' id='ygn' />" +
		"Yangon</label><br /><label><input type='checkbox' id='pat' checked='checked' />Pathein</label><br /><label>" +
		"<input type='checkbox' id='mon' checked='checked' />Monywa</label></div>"),
		checkbox = $div.appendTo("body").find("input[type='checkbox']").wijcheckbox();
		checkbox.wijcheckbox();
		checkbox.wijcheckbox();
		ok($div.find(".wijmo-checkbox-inputwrapper").length === 4, "checkbox does not create more wrapper");
		$div.remove();
	});

})(jQuery)