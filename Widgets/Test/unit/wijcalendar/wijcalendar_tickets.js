/*
 * wijcalendar_methods.js
 */
(function ($) {

	module("wijcalendar: tickets");

	test("#30863", function () {
		var d, $widget = createCalendar({ displayDate: new Date(2008, 8, 1), disabled: true, showWeekNumbers: true, selectionMode: { month: true } });
		$d = $widget.find("a.ui-icon-triangle-1-se");
		ok($d && $d.length === 1, "MonthSelectorImage icon is diaplay");
		$widget.remove();
	});

	test("test changing allowQuickPick option to false works fine when at Month View and Year ViewState", function () {
		var $widget = createCalendar(), calendarTable, dayElement;
		$(".ui-datepicker-title").simulate("click");
		setTimeout(function () {
			$widget.wijcalendar({ allowQuickPick: false });
			calendarTable = $widget.children(".ui-datepicker-calendar");
			dayElement = $(calendarTable).find("tbody tr td")[0];
			$(dayElement).simulate("click");

			setTimeout(function () {
				ok($(calendarTable).attr("summary").trim() !== "", "the summary info of the calendarTable is valid value.");
				ok($(dayElement).attr("date").trim() !== "Invalid Date", "the date stores in the dayElement is valid value.");
				ok($(dayElement).children("a").text().trim() !== "NaN", "the text of the link in the dayElement is valid value.");

				start();
			}, 500);
		}, 500);
		stop();
		$widget.remove();
	});

	test("#32274:wijcalendar doesn't reset to the initial style after reseting the popupMode option to true", function () {
		var $widget = createCalendar();

		$widget.wijcalendar({ popupMode: true });
		ok($widget.attr("style") !== "", "The style of wijcalendar is set for popup mode.");

		$widget.wijcalendar({ popupMode: false });
		ok(!$widget.attr("style"), "The style of wijcalendar is reset to initial state after resetting popupMode to false");

		$widget.remove();
	});

	test("#44407: test setting displayDate option works fine when at Decade view", function () {
		var $widget = createCalendar();
		$widget.find(".ui-datepicker-title").simulate("click");
		setTimeout(function () {
			$widget.wijcalendar({ displayDate: new Date(2013, 8, 8) });
			ok($widget.find(".ui-datepicker-title").text().trim() === "September 2013", "The title shows correct month and year, after setting displayDate option at Decade view.");
			start();
		}, 500);
		stop();
		$widget.remove();
	});

	if (!($.support.isTouchEnabled && $.support.isTouchEnabled())) {
		test("#42890", function () {
			var $widget = createCalendar();
			$widget.find(".wijmo-wijcalendar-day-selectable:first").simulate("mousedown");
			ok($widget.data("dragging.wijcalendar") === true, "");
			$widget.find(".wijmo-wijcalendar-day-selectable:first").simulate("mouseup");
			ok($widget.data("dragging.wijcalendar") === false, "");
			$widget.remove();
		});
	}

	test("#63004", function () {
	    var $widget = createCalendar({
	        culture: "fi-FI",
	        showWeekNumbers: true,
	        displayDate: new Date(2016, 0, 8),
	        calendarWeekRule: "firstFourDayWeek"
	    });
	    
	    setTimeout(function () {
	        ok($($widget.find(".wijmo-wijcalendar-week-num")[0]).text().trim() === "53", "The week number is 53");
	        ok($($widget.find(".wijmo-wijcalendar-week-num")[1]).text().trim() === "1", "The week number is 1");
	        start();
	       $widget.remove();
	    }, 500);
	    stop();
	    
	});

	test("#64353:wijcalendar", function () {
	    var $widget = createCalendar();

	    $widget.wijcalendar("option","popupMode", true );
	    ok($widget.data("wijmo-wijpopup") , "The wijcalendar is popup mode.");

	    $widget.wijcalendar("option", "popupMode", false);
	    ok(!$widget.data("wijmo-wijpopup"), "The wijcalendar is not popup mode.");

	    $widget.remove();
	});

	test("#94422: Goes back to the top page when pressing disabled year", function () {
	    var $widget = createCalendar({ displayDate: new Date(2014, 11, 24) });
	    $widget.find(".ui-datepicker-title").simulate("click");
	    setTimeout(function () {
	        $widget.find(".ui-datepicker-title").simulate("click");

	        setTimeout(function () {
	            ok($widget.find(".ui-state-disabled > a").attr("onclick") === "return false;");
	            $widget.find(".ui-datepicker-title").simulate("click");
	            setTimeout(function () {
	                ok($widget.find(".ui-state-disabled > a").attr("onclick") === "return false;");
	                $widget.remove();
	                start();
	            }, 500);
	        }, 500);
	    }, 500);
	    stop();
	});

	test("#98538", function () {
	    var $widget = createCalendar({
	        displayDate: new Date('2014/7/31')
	    });
	    $widget.find('.ui-datepicker-title').simulate('click');
	    setTimeout(function () {
	        $widget.find(".ui-datepicker-week-day:eq(5)").simulate("click");
	        setTimeout(function () {
	            ok($widget.find('.ui-datepicker-title').html() === "June 2014")
	            $widget.remove();
	            start();
	        }, 500);
	    }, 500);
	    stop();
	});

	test("#125376", function () {
		var selectedDate = function (event, data) {
			var widget = $(event.target);
				ok(widget.data("dragging.wijcalendar"), "The dragging has been set to True before firing the selectedDatesChanged event");
				ok(true, "The selectedDatesChanged event is fired first time.");
				widget.remove();
			}, $widget = createCalendar({
				displayDate: new Date('2015/8/31'),
				selectedDatesChanged: selectedDate,
				selectionMode: { day: true, days: true }
			}), selectableDates = $widget.find("td"),
            newDate = $(selectableDates[7]);
		newDate.simulate("mousedown");
	});

	test("#142215", function () {
		var cal = createCalendar({
			displayDate: new Date(2016, 0, 20),
			initialView: "month",
			popupMode: true
		});
		cal.wijcalendar("popup", {
			of: cal
		});
		ok($(".wijmo-wijcalendar-title", cal).text().replace(/\s+/g, '') == "2016");
		cal.remove();
	});

	test("#142258", function () {
		var cal = createCalendar({
			displayDate: new Date(2016, 0, 20),
			initialView: "month",
			monthCols: 2,
			monthRows: 2
		});
		ok($(".wijmo-wijcalendar-title", cal).text().replace(/\s+/g, '') == "January2016February2016March2016April2016");
		cal.remove();
	});

	test("#205589", function () {
		var cal = createCalendar({
			displayDate: new Date(2016, 8, 1),
			initialView: "month"
		}),
		originalHeight = $(".ui-datepicker-calendar", cal)[0].style.height,
		counter = 0,
		navigate = setInterval(function () {
			cal.data('wijmo-wijcalendar')._slideToDate(new Date(2017 + counter, 8, 1));
			counter++;
			if (counter >= 15) {
				clearInterval(navigate);
			}
		}, 100);
		stop();
		setTimeout(function () {
			ok($(".ui-datepicker-calendar", cal)[0].style.height === originalHeight);
			start();
			cal.remove();
		}, 2000);
	});

})(jQuery);
