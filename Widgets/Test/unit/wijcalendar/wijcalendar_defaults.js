/*
 * wijcalendar_defaults.js
 */

commonWidgetTests('wijcalendar', {
	defaults: {
			disabled:false,
			///	<summary>
			///		The culture id
			///	</summary>
			culture: '',
			///	<summary>
			///		The culture calendar string
			///	</summary>
			cultureCalendar: '',
			///	<summary>
			///		Number of columns of month views
			///	</summary>
			monthCols:1,
			///	<summary>
			///		Number of rows of month views
			///	</summary>
			monthRows:1,
			///	<summary>
			///		The format for the title text
			///	</summary>
			titleFormat: "MMMM yyyy",
			///	<summary>
			///		The calendar title is diaplyed
			///	</summary>
			showTitle: true,
			///	<summary>
			///		The display date for the first month view 
			///	</summary>
			displayDate: undefined,
			///	<summary>
			///		Number of day rows
			///	</summary>
			dayRows: 6,
			///	<summary>
			///		Number of day columns
			///	</summary>
			dayCols: 7,
			///	<summary>
			///		Format for week day
			///	</summary>
			weekDayFormat: "short",
			///	<summary>
			///		Whether to display week days
			///	</summary>
			showWeekDays: true,
			///	<summary>
			///		Whether to display week numbers
			///	</summary>
			showWeekNumbers: false,
			///	<summary>
			///		Defines different rules for determining the first week of the year
			///	</summary>
			calendarWeekRule: "firstDay",
			///	<summary>
			///		Minimal date
			///	</summary>
			minDate: new Date(1900, 0, 1),
			///	<summary>
			///		Maximal date
			///	</summary>
			maxDate: new Date(2099, 11, 31),
			///	<summary>
			///		Whether to display the days in other month
			///	</summary>
			showOtherMonthDays: true,
			///	<summary>
			///		Whether to padding a zero to day with one digit
			///	</summary>
			showDayPadding: false,
			///	<summary>
			///		Selection mode
			///	</summary>
			selectionMode: {day:true, days:true},
			///	<summary>
			///		Whether the preview button is displayed
			///	</summary>
			allowPreview: false,
			///	<summary>
			///		Determines whether can change the view to month/year/decade while clicking on the calendar title
			///	</summary>
			allowQuickPick: true,
			///	<summary>
			///		Format for tooltip
			///	</summary>
			toolTipFormat: "dddd, MMMM dd, yyyy",
			///	<summary>
			///		Tooltip for previous button
			///	</summary>
			prevTooltip: "Previous",
			///	<summary>
			///		Tooltip for next button
			///	</summary>
			nextTooltip: "Next",
			///	<summary>
			///		Tooltip for quick previous button
			///	</summary>
			quickPrevTooltip: "Quick Previous",
			///	<summary>
			///		Tooltip for quick next button
			///	</summary>
			quickNextTooltip: "Quick Next",
			///	<summary>
			///		month view title format
			///	</summary>
			monthViewTitleFormat: 'yyyy',
			///	<summary>
			///		Tooltip for previous preview button
			///	</summary>
			prevPreviewTooltip: "",
			///	<summary>
			///		Tooltip for next preview button
			///	</summary>
			nextPreviewTooltip: "",
			///	<summary>
			///		Display type of navigation buttons 
			///	</summary>
			navButtons: 'default',
			///	<summary>
			///		Quick navigation step
			///	</summary>
			quickNavStep: 12,
			///	<summary>
			///		Month slide direction
			///	</summary>
			direction: 'horizontal',
			///	<summary>
			///		initialView type
			///	</summary>
			initialView: "day",
			///	<summary>
			///		Animation duration
			///	</summary>
			duration: 250,
			///	<summary>
			///		Animation easing
			///	</summary>
			easing: 'easeInQuad',
			///	<summary>
			///		Popup mode
			///	</summary>
			popupMode: false,
			///	<summary>
			///		Auto-hide the calendar in popup mode when clicking outside
			///	</summary>
			autoHide: true,
			/// <summary>
			/// Function used for customizing the content, style and attributes of a day cell.
			/// Default: undefined.
			/// Type: Function.
			/// Code example: $("#element").wijcalendar({ customizeDate: function($daycell, date, dayType, hover, preview){ } });
			/// </summary>
			/// <param name="$daycell" type="jQuery">jQuery object that represents table cell of the date to be customized.</param>
			/// <param name="date" type="Date">Date of the cell.</param>
			/// <param name="dayType" type="Number">Type of the day.</param>
			/// <param name="hover" type="Boolean">Whether mouse is over the day cell.</param>
			/// <param name="preview" type="Object">Whether rendering in preview container.</param>
			/// <returns type="Boolean">True if day cell content has been changed and default cell content will not be applied.</returns>
			customizeDate: null,
			/// <summary>
			/// A callback function used to customizing the title text on month view.
			/// Default: null.
			/// Type: Function.
			/// Code example: $("#element").wijcalendar({ title: function (date, format) { } });
			/// </summary>
			///
			/// <param name="date" type="Date">The display date of the month.</param>
			/// <param name="format" type="String">The title format. Determined by the options.titleFormat.</param>
			/// <returns type="String">The customized title text.</returns>
			title: null,
			/// <summary>
			/// The beforeSlide event handler. A function called before the calendar view slides to another month. Cancellable.
			/// Default: null.
			/// Type: Function.
			/// Code example: $("#element").wijcalendar({ beforeSlide: function (e) { } });
			/// </summary>
			///
			/// <param name="e" type="Object">jQuery.Event object.</param>
			beforeSlide: null,
			/// <summary>
			/// The afterSlide event handler. A function called after the calendar view slided to another month.
			/// Default: null.
			/// Type: Function.
			/// Code example: $("#element").wijcalendar({ afterSlide: function (e) { } });
			/// </summary>
			///
			/// <param name="e" type="Object">jQuery.Event object.</param>
			afterSlide: null,
			/// <summary>
			/// The beforeSelect event handler. A function called before user selects a day by mouse. Cancellable.
			/// Default: null.
			/// Type: Function.
			/// Code example: $("#element").wijcalendar({ beforeSelect: function (e, args) { } });
			/// </summary>
			///
			/// <param name="e" type="Object">jQuery.Event object.</param>
			/// <param name="args" type="Object">
			/// The data with this event.
			/// args.date: The date to be selected.
			///</param>
			beforeSelect: null,
			/// <summary>
			/// The afterSelect event handler. A function called after user selects a day by mouse.
			/// Default: null.
			/// Type: Function.
			/// Code example: $("#element").wijcalendar({ afterSelect: function (e, args) { } });
			/// </summary>
			///
			/// <param name="e" type="Object">jQuery.Event object.</param>
			/// <param name="args" type="Object">
			/// The data with this event.
			/// args.date: The selected date.
			///</param>
			afterSelect: null,
			/// <summary>
			/// The selectedDatesChanged event handler. A function called after the selectedDates collection changed.
			/// Default: null.
			/// Type: Function.
			/// Code example: $("#element").wijcalendar({ selectedDatesChanged: function (e, args) { } });
			/// </summary>
			///
			/// <param name="e" type="Object">jQuery.Event object.</param>
			/// <param name="args" type="Object">
			/// The data with this event.
			/// args.dates: The array with all selected date object.
			///</param>
			selectedDatesChanged: null,
			create:null,
			wijCSS: $.extend({
				datepickerInline: "ui-datepicker-inline",
				datepicker: "ui-datepicker",
				datepickerCalendar: "ui-datepicker-calendar",
				datepickerMulti: "ui-datepicker-multi",
				datepickerRtl: "datepicker-rtl",
				datepickerOtherMonth: "ui-datepicker-other-month",
				prioritySecondary: "ui-priority-secondary",
				effectsWrapper: "ui-effects-wrapper",
				datepickerNextHover: "ui-datepicker-next-hover",
				datepickerPrevHover: "ui-datepicker-prev-hover",
				datepickerNext: "ui-datepicker-next",
				datepickerPrev: "ui-datepicker-prev",
				datepickerTitle: "ui-datepicker-title",
				datepickerWeekDay: "ui-datepicker-week-day",
				datepickerWeekEnd: "ui-datepicker-week-end", 
				datepickerUnselectable: "ui-datepicker-unselectable",
				datepickerDaysCellOver: "ui-datepicker-days-cell-over",
				datepickerToday: "ui-datepicker-today",
				stateHighlight: "ui-state-highlight",
				datepickerCurrentDay: "ui-datepicker-current-day",
				datepickerGroup: "ui-datepicker-group",
				datepickerGroupFirst: "ui-datepicker-group-first",
				datepickerGroupLast: "ui-datepicker-group-last",
				datepickerRowBreak: "ui-datepicker-row-break",
				datepickerHeader: "ui-datepicker-header",
				uiIconGripDottedVertical: "ui-icon-grip-dotted-vertical",
				uiDatepickerWeekCol: "ui-datepicker-week-col"
			}, $.wijmo.wijCSS),
			wijMobileCSS: {  
			    header: "ui-header ui-bar-a",
			    content: "ui-body ui-body-b",
			    stateDefault: "ui-btn ui-btn-b",
			    stateHighlight: "ui-btn-down-e"
			},
			/// <summary>
			/// Selector option for auto self initialization. 
			///	This option is internal.
			/// </summary>
			initSelector: ":jqmData(role='wijcalendar')",
	}
});
