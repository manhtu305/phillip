﻿(function($) {

module("wijcalendar: options");

    test("disabled", function() {
        var selectedDateChange = 0,
            $widget, newDate, selectableDates;
        $widget = createCalendar({
            displayDate: new Date('2014/1/20'),
            selectedDatesChanged: function() {
                selectedDateChange++;
            }
        });
        selectableDates = $widget.find("td");
        
        //use 7th day to ensure selected date is the same month.
        newDate = $(selectableDates[7]);
        newDate.simulate("mousedown");
        ok(selectedDateChange === 1, "selected date changed");
        $widget.wijcalendar("option", "disabled", true);
        newDate = $(selectableDates[8]);
        newDate.simulate("mousedown");
        ok(selectedDateChange === 1, "selected date is not changed when disabled is true");
        $widget.wijcalendar("option", "disabled", false);
        selectableDates = $widget.find("td")
        newDate = $(selectableDates[8]);
        newDate.simulate("mousedown");
        ok(selectedDateChange === 2, "selected date changed");
        $widget.remove();

		$widget = createCalendar({
			disabled: true,
            displayDate: new Date('2014/1/20'),
            selectedDatesChanged: function() {
                selectedDateChange++;
            }
        });
		selectableDates = $widget.find("td");
        newDate = $(selectableDates[7]);
        newDate.simulate("mousedown");
        ok(selectedDateChange === 2, "selected date is not changed when disabled is true");
        $widget.wijcalendar("option", "disabled", false);
        selectableDates = $widget.find("td")
        newDate = $(selectableDates[7]);
        newDate.simulate("mousedown");
        ok(selectedDateChange === 3, "selected date changed");
        $widget.remove();
    });

    test("culture cultureCalendar", function() {
        var datePickerFirstDate, title,
        $widget = createCalendar({
            displayDate: new Date('2014/1/20'),
			titleFormat: "d",
			culture: "ja-jp"
        });
		datePickerFirstDate = $($("th.ui-datepicker-week-day span", $widget)[0]);
		title = $(".wijmo-wijcalendar-title", $widget);
		ok(title.html().indexOf('2014') > -1 && datePickerFirstDate.html().indexOf("日") > -1, "culture works when set to ja-jp");
		$widget.remove();
		
        $widget = createCalendar({
            displayDate: new Date('2014/1/20'),
			titleFormat: "d",
			culture: "ja-jp",
			cultureCalendar: "Japanese"
        });
		datePickerFirstDate = $($("th.ui-datepicker-week-day span", $widget)[0]);
		title = $(".wijmo-wijcalendar-title", $widget);
		ok(title.html().indexOf('平成') > -1 && datePickerFirstDate.html().indexOf("日") > -1, "cultureCalendar works when set to Japanese");
		$widget.remove();

        $widget = createCalendar({
            displayDate: new Date('2014/1/20'),
			titleFormat: "d"
        });
		datePickerFirstDate = $($("th.ui-datepicker-week-day span", $widget)[0]);
		ok(datePickerFirstDate.html().indexOf("日") === -1, "calendar is not localized.");
		$widget.wijcalendar("option", "culture", "ja-jp");
		datePickerFirstDate = $($("th.ui-datepicker-week-day span", $widget)[0]);
		title = $(".wijmo-wijcalendar-title", $widget);
		ok(title.html().indexOf('2014') > -1 && datePickerFirstDate.html().indexOf("日") > -1, "culture works when set to ja-jp by _setOption");
		$widget.wijcalendar("option", "cultureCalendar", "Japanese");
		datePickerFirstDate = $($("th.ui-datepicker-week-day span", $widget)[0]);
		title = $(".wijmo-wijcalendar-title", $widget);
		ok(title.html().indexOf('平成') > -1 && datePickerFirstDate.html().indexOf("日") > -1, "cultureCalendar works when set to Japanese by _setOption");
		$widget.remove();
    });

    test("option:navButtons", function () {
        var $widget = createCalendar();
        
        ok($(".wijmo-wijcalendar-navbutton.ui-datepicker-next").length === 1, "The nav button is default.");
        ok($(".wijmo-wijcalendar-navbutton.ui-datepicker-prev").length === 1, "The previous button is default.")

        $widget.wijcalendar("option", "navButtons", "none");
        ok($(".wijmo-wijcalendar-navbutton.ui-datepicker-next").length === 0, "The nav button is none.");
        ok($(".wijmo-wijcalendar-navbutton.ui-datepicker-prev").length === 0, "The previous button is none.");
        $widget.remove();
    });

    test("MaxDate & MinDate", function () {
        var $widget = createCalendar({ displayDate: new Date(2014, 2, 20) }),
	        maxDate = new Date(2014, 2, 25),
            minDate = new Date(2014, 2, 18);

        ok(!$("td", ".wijmo-wijcalendar").eq(0).hasClass("ui-datepicker-unselectable"), "min has not set!");
        $widget.wijcalendar("option", "minDate", minDate);
        ok($("td", ".wijmo-wijcalendar").eq(0).hasClass("ui-datepicker-unselectable"), "min has set!");
        ok(!$("td", ".wijmo-wijcalendar").eq(41).hasClass("ui-datepicker-unselectable"), "max has no set!");
        $widget.wijcalendar("option", "maxDate", maxDate);
        ok($("td", ".wijmo-wijcalendar").eq(41).hasClass("ui-datepicker-unselectable"), "max has set!");
        $widget.remove();
    });

    test("monthViewTitleFormat", function () {
        var widget = createCalendar({
            displayDate: new Date('2015/11/7'),
            monthViewTitleFormat: "gg yy 年",
            culture: "ja-jp",
            cultureCalendar: "Japanese"
        });
        $("#mycalendar").data("wijmo-wijcalendar")._onTitleClicked();
        ok($(".wijmo-wijcalendar-title").text() == "平成 27 年");
        widget.remove();
    });

    test("initialView", function () {
    	var dayCalendar = createCalendar({ displayDate: new Date(2015, 10, 30), initialView: "day" }),
        monthCalendar = createCalendar({ displayDate: new Date(2015, 10, 30), initialView: "month" }),
        yearCalendar = createCalendar({ displayDate: new Date(2015, 10, 30), initialView: "year" }),
        decadeCalendar = createCalendar({ displayDate: new Date(2015, 10, 30), initialView: "decade" });
        ok($(".wijmo-wijcalendar-title", dayCalendar).text().replace(/\s+/g, '') == "November2015");
        ok($(".wijmo-wijcalendar-title", monthCalendar).text().replace(/\s+/g, '') == "2015");
        ok($(".wijmo-wijcalendar-title", yearCalendar).text().replace(/\s+/g, '') == "2010-2019");
        ok($(".wijmo-wijcalendar-title", decadeCalendar).text().replace(/\s+/g, '') == "2000-2099");
        dayCalendar.remove();
        monthCalendar.remove();
        yearCalendar.remove();
        decadeCalendar.remove();
    });

})(jQuery);