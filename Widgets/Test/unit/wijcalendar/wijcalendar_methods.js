/*
 * wijcalendar_methods.js
 */
(function($) {

module("wijcalendar: methods");

test("select, unselect", function() {
	var $widget = createCalendar();
	var d = new Date('2010/8/10');
	$widget.wijcalendar('selectDate', d);
	var selDate = $widget.wijcalendar('getSelectedDate');
	ok(selDate.getTime() == d.getTime(), 'selected date works!');
	$widget.wijcalendar('unSelectAll');
	selDate = $widget.wijcalendar('getSelectedDate');
	ok(selDate == null, 'unselect works!');
	$widget.remove();
});

test("slide", function() {
	var d = new Date('2010/9/1');
	var $widget = createCalendar({
		displayDate: new Date('2010/8/1'),
		duration: 1000,
		afterSlide: function(){
			var dd = $widget.wijcalendar('getDisplayDate');
			ok(dd.getTime() == d.getTime(), 'slide works!');
			window.setTimeout(function(){ 
				$widget.remove();
				start();
			}, 1000);
		}
	});

	window.setTimeout(function(){ 
		$widget.find('.ui-datepicker-next').trigger('click');
		}, 1000);
	stop();
});

test("pupup, close", function() {
	var $widget = createCalendar({
		popupMode: true,
		afterPopup: function(){
			ok($widget.wijcalendar('isPopupShowing') == true, 'pop up');
			window.setTimeout(function(){ 
				start(); 
				$widget.wijcalendar('close');
			}, 1000);
		},
		afterClose: function(){
			ok($widget.wijcalendar('isPopupShowing') == false, 'closed');
			$widget.remove();
		}
	});
		
	$widget.wijcalendar('popupAt', 220, 340);
	stop();
});



})(jQuery);
