/*
 * wijcalendar_methods.js
 */
(function($) {

module("wijcalendar: selections");

test("select", function() {
	var $widget = createCalendar({displayDate: new Date(2008,8,1), showWeekDays: true, showWeekNumbers: true, selectionMode: { day: true, days: true, weekDay: true, weekNumber: true, month: true }});
	var $d = $widget.find('td[date*="Sep 10 2008"]');
	if ($d.length){
		$d.simulate('mousedown');
		ok($d.hasClass('ui-datepicker-current-day'), 'single date is selected, ok')
	}
	

	var $wd = $widget.find('span[title*="Thursday"]').closest('th');
	if ($wd.length){
		$wd.simulate('click');
		var selDates = $widget.wijcalendar('option', 'selectedDates');
		ok(selDates.length === 4, '4 days are selected, ok')

		$d = $widget.find('td[title*="September 04, 2008"]');
		ok($d.hasClass('ui-datepicker-current-day'), 'Sep 4 is selected, ok')
		
		$d = $widget.find('td[title*="September 11, 2008"]');
		ok($d.hasClass('ui-datepicker-current-day'), 'Sep 11 is selected, ok')
		
		$d = $widget.find('td[title*="September 18, 2008"]');
		ok($d.hasClass('ui-datepicker-current-day'), 'Sep 18 is selected, ok')
		
		$d = $widget.find('td[title*="September 25, 2008"]');
		ok($d.hasClass('ui-datepicker-current-day'), 'Sep 25 is selected, ok')
	}
	
	var $wn = $widget.find('#mycalendar_2008_9_rs_2');
	if ($wn.length){
		$wn.simulate('click');
		
		var selDates = $widget.wijcalendar('option', 'selectedDates');
		ok(selDates.length === 7, '7 days are selected, ok')
	}
	
	var $ms = $widget.find('.wijmo-wijcalendar-monthselector');
	if ($ms.length){
		$ms.simulate('click');
		
		var selDates = $widget.wijcalendar('option', 'selectedDates');
		ok(selDates.length === 30, '30 days are selected, ok')
	}

	$widget.remove();
});


})(jQuery);
