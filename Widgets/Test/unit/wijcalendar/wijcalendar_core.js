/*
 * wijcalendar_core.js
 */

function createCalendar(o){
	return $("<div id='mycalendar'></div>").appendTo(document.body).wijcalendar(o);
}

(function($){

	module("wijcalendar: core");

	test("create and destroy", function(){
		var $widget = createCalendar();
		ok($widget.hasClass('wijmo-wijcalendar'), 'element css classes created.');
		$widget.wijcalendar('destroy');
		ok($widget.is(':empty'), 'all element removed');
		$widget.remove();
	});

})(jQuery);

