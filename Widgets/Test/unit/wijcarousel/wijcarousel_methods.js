﻿/*
* wijcarousel_methods.js
*/
(function ($) {

	module("wijcarousel:methods");

	test("add & remove when loop === false", function () {
		var removed = false, item, olength;
		var $widget = createCarousel({
			showTimer: false,
			showPager: true,
			loop: false
		});

		item = "<img src='utest'></img>";
		olength = $widget.find('.wijmo-wijpager').find("li").length;
		$widget.wijcarousel("add", item, 3);

		ok($widget.find('li').eq(3).find("img:eq(0)").attr("src") === 'utest', 'Add: Node element added success');

		ok($widget.find('.wijmo-wijpager').find("li").length === olength + 1, 'Add: Pager refreshed');

		$widget.wijcarousel("remove", 3);

		ok($widget.find('li').eq(3).find("img:eq(0)").attr("src") !== 'utest', 'Remove: Node element removed success');

		ok($widget.find('.wijmo-wijpager').find("li").length === olength, 'Remove: Pager refreshed');

		$widget.remove();
	});

	test("add & remove when loop === true", function () {
		var removed = false, item, olength, li, flag = 0;
		var $widget = createCarousel({
			showTimer: false,
			showPager: true,
			loop: true,
			start: 2
		});

		item = "<img src='utest.jpg'>";
		olength = $widget.find('.wijmo-wijpager').find("li").length;
		$widget.wijcarousel("add", item, 3);

		$widget.find('li').each(function () {
			var n = $(this);
			if (n.data("itemIndex") === 3) {
				li = n;
				flag++;
			}
		});

		ok(li.find("img:eq(0)").attr("src") === 'utest.jpg', 'Add: Node element added success');

		ok(flag === 1, "Add: Item data refreshed!");

		ok($widget.find('.wijmo-wijpager').find("li").length === olength + 1, 'Add: Pager refreshed');

		$widget.wijcarousel("remove", 3);

		$widget.find('li').each(function () {
			var n = $(this);
			if (n.data("itemIndex") === 3) {
				li = n;
				flag++;
			}
		});

		ok(li.find("img:eq(0)").attr("src") !== 'utest.jpg', 'Remove: Node element removed success');

		ok(flag === 2, "Remove: item data refreshed!")

		ok($widget.find('.wijmo-wijpager').find("li").length === olength, 'Remove: Pager refreshed');

		$widget.remove();
	});

	/*The methods 'play, pause, next, previous' 
	on option loop === true follows the scrollTo method. */

	test('play, pause', function () {
		var left, $widget = createCarousel({
			showTimer: false,
			showPager: true,
			loop: false,
			animation: { duration: 0 },
			interval: 1000
		});

		$widget.wijcarousel("play");

		window.setTimeout(function () {

			left = calculateScrolled($widget);
			if ($.browser.webkit) {
				ok(left < -500, "play sccuess!");
			}
			else {
				ok(left === -750, "play sccuess!");
			}

			$widget.wijcarousel("pause");
			window.setTimeout(function () {
				start();
				left = calculateScrolled($widget);
				ok(left !== -1500, "pause sccuess!");
				$widget.remove();
			}, 2000);

		}, 1900);
		stop();
	});


	test('next, previous', function () {
		var left, $widget = createCarousel({
			showTimer: false,
			showPager: true,
			loop: false,
			animation: { duration: 0 }
		});

		$widget.wijcarousel("next");

		left = calculateScrolled($widget);

		if ($.browser.webkit) {
			ok(left < -500, "next sccuess!");
		}
		else {
			ok(left === -750, "next sccuess!");
		}

		$widget.wijcarousel("previous");

		left = calculateScrolled($widget);
		ok(left !== -1500, "previous sccuess!");

		$widget.remove();
	});

	test('scrollTo when loop === false', function () {
		var left, $widget = createCarousel({
			showTimer: false,
			showPager: true,
			loop: false,
			animation: { duration: 0 }
		});

		$widget.wijcarousel("scrollTo", 3);

		left = calculateScrolled($widget);

		if ($.browser.webkit) {
			ok(left < -1500, "next sccuess!");
		}
		else {
			ok(left === -2250, "scrollTo sccuess!");
		}

		$widget.remove();
	});

	test('scrollTo when loop === true', function () {
		var left, li, $widget = createCarousel({
			showTimer: false,
			showPager: true,
			loop: true,
			animation: { duration: 0 }
		});

		$widget.wijcarousel("scrollTo", 3);

		left = calculateScrolled($widget);
		ok(left === 0, "scrollTo: css is right!");

		li = $widget.find("ul.wijmo-wijcarousel-list li:eq(0)");

		ok(li.data("itemIndex") === 3, "scrollTo: position is right!");
		$widget.remove();
	});

})(jQuery);