﻿/*
* wijcarousel_defaults.js
*/


commonWidgetTests('wijcarousel', {
	defaults: {
		/// <summary>
		/// Selector option for auto self initialization. 
		///	This option is internal.
		/// </summary>
		initSelector: ":jqmData(role='wijcarousel')",
		wijCSS: $.wijmo.wijCSS,
		wijMobileCSS: {
		    header: "ui-header ui-bar-a",
		    content: "ui-body-a",
		    stateDefault: "ui-btn ui-btn-a",
		    stateHover: "ui-btn-down-a",
		    stateActive: "ui-btn-down-b",
		    iconPlay: "ui-icon-arrow-r",
		    iconPause: "ui-icon-grid"
		},
		disabled: false,
        
		data: [],

		auto: false,

		interval: 5000,

		showTimer: false,

		buttonPosition: "inside",

		showPager: false,

		prevBtnClass: {
			defaultClass: "",
			hoverClass: "",
			disableClass: ""
		},

		nextBtnClass: {
			defaultClass: "",
			hoverClass: "",
			disableClass: ""
		},

		pagerType: "numbers",

		thumbnails: {
			"mouseover": null,
			"mouseout": null,
			"mousedown": null,
			"mouseup": null,
			"click": null,
			"imageWidth": 58,
			"imageHeight": 74,
			"images": [] 
		},

		pagerPosition: {},

		orientation: "horizontal",
		sliderOrientation: "horizontal",
		loop: true,

		animation: {

			queue: true,

			disable: false,

			duration: 1000,

			easing: "linear"
		},

		start: 0,

		display: 1,

		step: 1,

		preview: false,

		showControls: false,

		control: "",

		controlPosition: {},

		showCaption: true,

		showControlsOnHover: false,

		itemClick: null,

		beforeScroll: null,

		afterScroll: null,

		loadCallback: null
	}
});
