﻿/*
* wijcarousel_options.js
*/
(function ($) {

	module("wijcarousel:options");

	test("data", function () {

		var $widget = createCarouselEmpty();
		var images = [
            {
            	imgUrl: "images/art/zeka1.jpg",
            	linkUrl: "images/art/zeka1.jpg",
            	content: "",
            	caption: "<span>Word Caption 1</span>"
            },
            {
            	imgUrl: "images/art/zeka2.jpg",
            	linkUrl: "images/art/zeka2.jpg",
            	content: "",
            	caption: "<span>Word Caption 2</span>"
            }
			];
		$widget.wijcarousel("option", "data", images);

		ok($widget.find("li.wijmo-wijcarousel-item").length === 2, "data");

		$widget.remove();
	});

	test("showControl, showPager, showTimer", function () {

		var left, $widget = createCarousel({
			showTimer: false,
			showPager: false,
			showControl: false,
			loop: false,
			animation: { duration: 0 },
			interval: 1000,
			control: "<div class=\"utest\">Bhah</div>"
		});

		$widget.wijcarousel("option", "showTimer", true);

		ok($widget.find("div.wijmo-wijcarousel-timerbar").length !== 0, "showTimer");

		$widget.wijcarousel("option", "showControls", true);

		ok($widget.find("div.utest").length !== 0, "showControls");

		$widget.wijcarousel("option", "showPager", true);

		ok($widget.find("div.wijmo-wijcarousel-pager").length !== 0, "showPager");

		$widget.remove();
	});

	test("pagerPosition, animation, controlPosition", function () {
		var $widget = createCarousel({
			showTimer: false,
			showPager: true,
			showControl: false,
			loop: false,
			pagerPosition: { utest: true },
			animation: { utest: true },
			controlPosition: { utest: true }
		});

		$widget.wijcarousel("option", "pagerPosition", {});

		ok($widget.wijcarousel("option", "pagerPosition").utest,
		"pagerPosition is extend to original value");

		$widget.wijcarousel("option", "animation", {});

		ok($widget.wijcarousel("option", "animation").utest,
		"animation is extend to original value");

		$widget.wijcarousel("option", "controlPosition", {});

		ok($widget.wijcarousel("option", "controlPosition").utest,
		"controlPosition is extend to original value");

		$widget.remove();
	});

	test("pagerType", function () {
		var $widget = createCarousel({
			showTimer: false,
			showPager: true,
			showControl: false,
			loop: false
		});

		$widget.wijcarousel("option", "pagerType", "dots");

		ok($widget.find("div.wijmo-wijcarousel-pager li:eq(0)")
		.is(".wijmo-wijcarousel-dot"), "pagerType");

		$widget.remove();
	});

	test("buttonPosition", function () {
		var left = 0, $widget = createCarousel({
			showTimer: false,
			showPager: true,
			showControl: false,
			loop: false
		});
		$widget.wijcarousel("option", "buttonPosition", "outside");

		if ($widget.find("a.wijmo-wijcarousel-button-previous").length) {
			left = parseInt($widget.find("a.wijmo-wijcarousel-button-previous").css("left"));
		}

		ok(left < -14, "buttonPosition");
		$widget.remove();
	});

	test("loop,orientation,display", function () {
		ok(true, "recreated the carousel");		
	});

	test("thumbnails", function () {
	    var $widget = createCarousel({
	        showTimer: false,
	        showPager: true,
	        showControl: false,
	        pagerType: "thumbnails",
	        loop: false
	    }),
	    thumbnails = {
	        imageWidth: 58,
	        imageHeight: 74,
	        images: [
                "http://www.componentone.com/newimages/walls/StudioEnterprise2011v2/secharts2011_thumb.png",
                "http://www.componentone.com/newimages/walls/WindowsPhone2011/wp72011_thumb.png",
                "http://www.componentone.com/newimages/walls/WindowsPhone2011/wp72011_thumb1.png",
                "http://www.componentone.com/newimages/walls/WindowsPhone2011/wp72011_thumb2.png",
                "http://www.componentone.com/newimages/walls/WindowsPhone2011/wp72011_thumb3.png",
                "http://www.componentone.com/newimages/walls/WindowsPhone2011/wp72011_thumb4.png",
                "http://www.componentone.com/newimages/Walls/SilverlightWPF2011v2/slwpf_thumb.png"]
	    }
	    ok($widget.find("li.wijmo-wijcarousel-page").size() === 0, "The thumbnails has not setted!");

	    $widget.wijcarousel("option", "thumbnails", thumbnails);
	    $widget.wijcarousel("refresh");

	    ok($widget.find("li.wijmo-wijcarousel-page").size() === 7, "The thumbnails has setted!");

	    $widget.remove();
	});

})(jQuery);