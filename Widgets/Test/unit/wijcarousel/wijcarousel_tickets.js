﻿(function ($) {
	var el;
	module("wijcarousel: tickets");

	test("#30362", function () {
		var flag = 0;

		var el = createCarouselEmpty();

		el.wijcarousel("add", '<li><img alt="Sports 2" src="http://lorempixel.com/200/150/sports/2" title="Window 2" /><span><h3>Word Caption 2</h3>Word Caption 2</span> </li>', 0);
		ok(el.find("li").length, '30362');
		el.remove();
	});

	test("#37490", function () {
		var el = createCarousel({ disabled: true });
		ok(el.hasClass("ui-state-disabled"), "the disabled class is added.");
		el.remove();
	});

	test("#37512", function () {
		var el = createCarousel({
			showControlsOnHover: true, showTimer: true,
			showPager: true,
		});
		ok(el.find(".wijmo-wijcarousel-pager").is(":hidden"), "the pagger is hidden.");
		ok(el.find(".wijmo-wijcarousel-button-next").is(":hidden"), "the next button is hidden.");
		el.find(".wijmo-wijcarousel-list").simulate("mouseover");
		ok(el.find(".wijmo-wijcarousel-pager").is(":visible"), "when mouse over the widget, the pagger is hidden.");
		ok(el.find(".wijmo-wijcarousel-button-next").is(":visible"), "when mouse over the widget, the next button is hidden.");
		el.find(".wijmo-wijcarousel-list").simulate("mouseout");
		el.wijcarousel({ showControlsOnHover: false });
		ok(el.find(".wijmo-wijcarousel-pager").is(":visible"), "the pagger is hidden.");
		ok(el.find(".wijmo-wijcarousel-button-next").is(":visible"), "the next button is hidden.");
		el.remove();
	});

	test("#37609", function () {
		var el = createCarousel({
			showTimer: true,
			showPager: true,
			auto: true,
			preview: true
		});

		el.wijcarousel("option", "showPager", false);
		el.wijcarousel("option", "preview", false);
		el.wijcarousel("option", "showPager", true);
		ok(el.find(".wijmo-wijcarousel-pager").is(":visible"), "the pager is visible");

		//for showtimer
		el.wijcarousel("option", "preview", true);
		el.wijcarousel("option", "showTimer", false);
		el.wijcarousel("option", "preview", false);
		el.wijcarousel("option", "showTimer", true);
		ok(el.find(".wijmo-wijcarousel-timerbar").is(":visible"), "the timer is visible");
		el.remove();
	});

	test("#34801", function () {
		var el = createCarousel({
			disabled: true,
			buttonPosition: "outside"
		});
		ok(el.find(".wijmo-wijcarousel-button-next").hasClass("ui-state-disabled"), "The next button has disabled CSS.");
		ok(el.find(".wijmo-wijcarousel-button-previous").hasClass("ui-state-disabled"), "The previous button has disabled CSS.");
		el.find(".wijmo-wijcarousel-button-next").simulate("click");
		ok(el.data("wijmo-wijcarousel").currentIdx === 0, "when click the next button, the current index of the carousel is not changed.");

		el.wijcarousel("option", "disabled", false);
		el.find(".wijmo-wijcarousel-button-next").simulate("click");
		setTimeout(function () {
			ok(el.data("wijmo-wijcarousel").currentIdx === 1, "when call setoption, set the disabled to false, when click the next button, the current index of the carousel is not changed.");
			ok(!el.find(".wijmo-wijcarousel-button-next").hasClass("ui-state-disabled"), "The next button removed disabled CSS.");
			ok(!el.find(".wijmo-wijcarousel-button-previous").hasClass("ui-state-disabled"), "The previous button removed disabled CSS.");
			el.wijcarousel("option", "disabled", true);
			ok(el.find(".wijmo-wijcarousel-button-next").hasClass("ui-state-disabled"), "when set the disabled option to true, The next button has disabled CSS.");
			ok(el.find(".wijmo-wijcarousel-button-previous").hasClass("ui-state-disabled"), "The previous button has disabled CSS.");
			el.find(".wijmo-wijcarousel-button-previous").simulate("click");
			ok(el.data("wijmo-wijcarousel").currentIdx === 1, "when click the previous button, the current index of the carousel is not changed.");
			el.remove();
			start();
		}, 1500);
		stop();
	});

	test("#37623", function () {
		var el = createCarousel({
			showPager: true,
			auto: false,
			preview: false,
			loop: false,
			showCaption: true
		});

		el.find(".wijmo-wijcarousel-button-next").simulate("click");
		setTimeout(function () {
			ok(el.data("wijmo-wijcarousel").currentIdx === 1, "when click the next button, the current index of the carousel is changed to next.");
			el.wijcarousel("option", "display", "2");
			el.find(".wijmo-wijcarousel-button-next").simulate("click");
			setTimeout(function () {
				ok(el.data("wijmo-wijcarousel").currentIdx === 2, "when click the next button, the current index of the carousel is changed to next correctly after changing the display option.");
				el.remove();
				start();
			}, 1500);
			stop();
			start();
		}, 1500);
		stop();
	});

	test("#34802", function () {
		var el = createCarousel({
			showTimer: true,
			auto: true,
			interval: 1500
		})
			, nextBtn = el.find(".wijmo-wijcarousel-button-next")
			, prevBtn = el.find(".wijmo-wijcarousel-button-previous")
			, pauseBtn = el.find(".wijmo-wijcarousel-button");

		nextBtn.simulate("click");
		prevBtn.simulate("click");
		setTimeout(function () {
			pauseBtn.simulate("click");
			setTimeout(function () {
				ok(el.data("wijmo-wijcarousel").currentIdx === 0, "The auto play animation paused corretly, the image of wijcarousel won't be navigated to next.");
				el.remove();
				start();
			}, 3000);
			stop();
			start();
		}, 1000);
		stop();
	});

	test("test disable wijcarousel control.", function () {
		var el = createCarousel({
			disabled: true,
			showControlsOnHover: true
		})
			, nextBtn = el.find(".wijmo-wijcarousel-button-next")
			, prevBtn = el.find(".wijmo-wijcarousel-button-previous");

		el.simulate("mouseover");
		ok(nextBtn.is(":hidden"), "The next button won't show, when mouse over the disable wijcarousel control.");
		ok(prevBtn.is(":hidden"), "The previous button won't show, when mouse over the disable wijcarousel control.");

		el.wijcarousel({ showControlsOnHover: false });
		nextBtn.simulate("click");
		setTimeout(function () {
			ok(el.data("wijmo-wijcarousel").currentIdx === 0, "The image of wijcarousel won't be navigated to next, when click the next button of the disable wijcarousel control.");
			el.wijcarousel("scrollTo", 1);
			setTimeout(function () {
				prevBtn.simulate("click");
				setTimeout(function () {
					ok(el.data("wijmo-wijcarousel").currentIdx === 1, "The image of wijcarousel won't be navigated to previous, when click the previous button of the disable wijcarousel control.");
					el.remove();
					start();
				}, 1200)
			}, 1000);
		}, 1000);
		stop();
	});

	test("test disable wijcarousel control for clicking play button.", function () {
		var el = createCarousel({
			disabled: true,
			showTimer: true,
			interval: 800
		})
			, playBtn = el.find(".wijmo-wijcarousel-button");

		playBtn.simulate("click");
		setTimeout(function () {
			ok(el.data("wijmo-wijcarousel").currentIdx === 0, "The image of wijcarousel won't be navigated to other image, when click the pager button of the disable wijcarousel control.");
			el.remove();
			start();
		}, 2000);
		stop();
	});

	test("test disable wijcarousel control for showpager mode.", function () {
		var el = createCarousel({
			disabled: true,
			showPager: true
		})
			, pager = el.find(".wijmo-wijpager");

		$(pager.find("ul > li")[1]).simulate("click");
		setTimeout(function () {
			ok(el.data("wijmo-wijcarousel").currentIdx === 0, "The image of wijcarousel won't be navigated to other image, when click the pager button of the disable wijcarousel control.");
			el.remove();
			start();
		}, 1000);
		stop();
	});

	test("#56043", function () {
		var el = createCarousel({
			disabled: true,
			showPager: true,
			pagerType: "dots"
		});

		ok(true, "No exception is thrown.");
		el.remove();
	});

	test("63305", function () {
	    var el = createCarousel({
	        disabled: true,
	        pagerType: "slider",
	        showPager: true
	    })
			, pager = el.find(".wijmo-wijcarousel-slider-wrapper");

	    $(pager.find(".wijmo-wijslider-incbutton")).simulate("click");
	    setTimeout(function () {
	        ok(el.data("wijmo-wijcarousel").currentIdx === 0, "The image of wijcarousel won't be navigated to other image, when click the pager button of the disable wijcarousel control.");
	        el.remove();
	        start();
	    }, 2000);
	    stop();
	});

	test("test disable wijcarousel control for page type change.", function () {
	    var el = createCarousel({
	        animation: { disabled: true, queue: false, duration:0},
	        disabled: true,
	        showPager: true
	    });

	    $(el.find(".wijmo-wijcarousel-pager").find("ul > li")[1]).simulate("click");
	    ok(el.data("wijmo-wijcarousel").currentIdx === 0, "The image of wijcarousel won't be navigated to other image, when click the pager button of the disable wijcarousel control.");
	    el.wijcarousel("option", "pagerType", "slider");
	    $(el.find(".wijmo-wijcarousel-pager").find(".wijmo-wijslider-incbutton")).simulate("click");
	    ok(el.data("wijmo-wijcarousel").currentIdx === 0, "The image of wijcarousel won't be navigated to other image, when click the pager button of the disable wijcarousel control.");
	    el.wijcarousel("option", "pagerType", "dots");
	    $(el.find(".wijmo-wijcarousel-pager").find("ul > li")[1]).simulate("click");
	    ok(el.data("wijmo-wijcarousel").currentIdx === 0, "The image of wijcarousel won't be navigated to other image, when click the pager button of the disable wijcarousel control.");
	    el.remove();
	});

	test("#89506", function () {
	    var $widget = createCarousel({
	        showTimer: true,
	        interval: 200
	    }), imageSrc;

	    window.setTimeout(function () {
	        $widget.wijcarousel("play");
	        window.setTimeout(function () {
	            ok($widget.find(".ui-icon-pause").length === 1);
	            $widget.wijcarousel("option", "disabled", true);

	            window.setTimeout(function () {
	                ok($widget.find(".ui-icon-play").length === 1);
	                imageSrc = $widget.find(".wijmo-wijcarousel-current img").attr("src");
	                ok(imageSrc === "images/art/zeka2.jpg");
	                $widget.remove();
	                start();
	            }, 400);

	        }, 100);
	    }, 200);
	    stop();
	});

	test("#92768", function () {
	    var $widget = createCarousel({
	        showPager: true
	    });
	    ok($widget.find(".wijmo-wijcarousel-list > li:eq(0) > img").attr("src") === "images/art/zeka2.jpg");
	    ok($widget.find(".wijmo-wijcarousel-pager ul > li:eq(0)").hasClass("ui-state-active"));

	    $widget.wijcarousel("option", "start", 1);
	    $widget.wijcarousel("refresh");
	    ok($widget.find(".wijmo-wijcarousel-list > li:eq(0) > img").attr("src") === "images/art/zeka3.jpg");
	    ok($widget.find(".wijmo-wijcarousel-pager ul > li:eq(1)").hasClass("ui-state-active"));

	    $widget.remove();
	});

	test("#69875", function () {
	    var el = createCarousel({
	        disabled: true,
	        showControlsOnHover: true,
	        pagerType: "slider",
	        showPager: false
	    });
	    el.wijcarousel("option", "showPager", true);
	    ok(el.data("wijmo-wijcarousel").currentIdx === 0);
	    var pager = el.find(".wijmo-wijcarousel-slider-wrapper");
	    $(pager.find(".wijmo-wijslider-incbutton")).simulate("click");
	    setTimeout(function () {
	        ok(el.data("wijmo-wijcarousel").currentIdx===0,"Pager UI should be disabled");
	        el.remove();
	        start();
	    }, 2000);
	    stop();
	});

	test("#113183_Case1", function () {
		var el = createCarousel({
			loop: false
		});
		ok(el.data("wijmo-wijcarousel").currentIdx === 0);
		stop();
		setTimeout(function () {
			$(el.find(".wijmo-wijcarousel-button-next")).simulate("click");
			setTimeout(function () {
				$(el.find(".wijmo-wijcarousel-button-next")).simulate("click");
				setTimeout(function () {
					el.wijcarousel("option", "preview", true);
					setTimeout(function () {
						ok(el.data("wijmo-wijcarousel").currentIdx === 2,
							"Expected Index:2.Actual Index:" + el.data("wijmo-wijcarousel").currentIdx);
						start();
						el.remove();
					}, 1500);
				}, 1500);
			}, 1500);
		}, 1500);
	});
	test("#113183_Case2", function () {
		var el = createCarousel({
			loop: false
		}), maskCount;
		stop();
		setTimeout(function () {
			el.wijcarousel("option", "preview", true);
			setTimeout(function () {
				el.wijcarousel("option", "preview", false);
				setTimeout(function () {
					$(el.find(".wijmo-wijcarousel-button-next")).simulate("click");
					setTimeout(function () {
						maskCount = el.find(".wijmo-wijcarousel-list > li > div.wijmo-wijcarousel-mask").length;
						ok(maskCount === 0,
							"Mask DIV count should be zero.Actual Count :" + maskCount);
							start();
							el.remove();
					}, 1500);
				}, 1500);
			}, 1500);
		}, 1500);
	});

})(jQuery);