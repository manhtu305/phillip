﻿/*
* wijcarousel_events.js
*/


(function ($) {
	var el;
	module("wijcarousel: events");

	test("loadCallback", function () {
		expect(1); //2 assertions
		var flag = 0;
		el = createCarousel({
			showTimer: true,
			showPager: true,
			loop: true,
			animation: { duration: 100 },
			showContorlsOnHover: true,
			loadCallback: function () {
				flag++;
			}
		});
		ok(flag == 1, 'loadCallback is fired');
	});

	test("itemClick", function () {
		expect(1); //1 assertions
		var flag = 0;

		el.bind("wijcarouselitemclick", function () {
			flag++;
		});

		el.find("li:eq(0)").trigger('click');
		start();
		ok(flag == 1, 'item is clicked');
		el.remove();
		el = null;
	});


	test("beforeScroll, afterScroll", function () {
		expect(2); //1 assertions
		var flag = 0, flag1 = 0;
		el = null;
		el = createCarousel({
			showTimer: true,
			showPager: true,
			loop: true,
			animation: { duration: 0 },
			showContorlsOnHover: true
		});

		el.bind("wijcarouselbeforescroll", function () {
			flag++;
		}).bind("wijcarouselafterscroll", function () {
			flag1++;
		});

		el.wijcarousel("next");

		ok(flag == 1, 'beforeScroll is fired');
		ok(flag1 == 1, 'afterScroll is fired');
		el.remove();
	});

})(jQuery);