﻿var htmlText = '';
htmlText += '<div id="wijcarousel0">';
htmlText += '    <ul>';
htmlText += '        <li>';
htmlText += '            <img src="images/art/zeka2.jpg" alt="Zeka 2" />';
htmlText += '            <span>Word Caption 2</span></li>';
htmlText += '        <li>';
htmlText += '            <img src="images/art/zeka3.jpg" alt="Zeka 3" />';
htmlText += '            <span>Word Caption 3</span></li>';
htmlText += '        <li>';
htmlText += '            <img src="images/art/zeka4.jpg" alt="Zeka 4" />';
htmlText += '            <span>Word Caption 4</span></li>';
htmlText += '        <li>';
htmlText += '            <img src="images/art/zeka5.jpg" alt="Zeka 5" />';
htmlText += '            <span>Word Caption 5</span></li>';
htmlText += '        <li>';
htmlText += '            <img src="images/art/zeka6.jpg" alt="Zeka 6" />';
htmlText += '            <span>Word Caption 6</span></li>';
htmlText += '        <li>';
htmlText += '            <img src="images/art/zeka7.jpg" alt="Zeka 7" />';
htmlText += '            <span>Word Caption 7</span></li>';
htmlText += '        <li>';
htmlText += '            <img src="images/art/zeka8.jpg" alt="Zeka 8" />';
htmlText += '            <span>Word Caption 8</span></li>';
htmlText += '    </ul>';
htmlText += '</div>';

var htmlEmpty = '<div id="wijcarousel2"></div>';

var htmlTextWithLink = '';
htmlTextWithLink += '<div id="wijcarousel1" class="ui-widget-content ui-corner-all" style="padding: 15px 15px 15px 15px;">';
htmlTextWithLink += '    <ul class="">';
htmlTextWithLink += '        <li class=""><a href="images/art/zeka1.jpg">';
htmlTextWithLink += '            <img alt="1" src="images/art/small/zeka1.jpg" title="Word Caption 1" />';
htmlTextWithLink += '        </a></li>';
htmlTextWithLink += '        <li class=""><a href="images/art/zeka2.jpg">';
htmlTextWithLink += '            <img alt="2" src="images/art/small/zeka2.jpg" title="Word Caption 2" />';
htmlTextWithLink += '        </a></li>';
htmlTextWithLink += '        <li class=""><a href="images/art/zeka3.jpg">';
htmlTextWithLink += '            <img alt="3" src="images/art/small/zeka3.jpg" title="Word Caption 3" />';
htmlTextWithLink += '        </a></li>';
htmlTextWithLink += '        <li class=""><a href="images/art/zeka4.jpg">';
htmlTextWithLink += '            <img alt="4" src="images/art/small/zeka4.jpg" title="Word Caption 4" />';
htmlTextWithLink += '        </a></li>';
htmlTextWithLink += '        <li class=""><a href="images/art/zeka5.jpg">';
htmlTextWithLink += '            <img alt="5" src="images/art/small/zeka5.jpg" title="Word Caption 5" />';
htmlTextWithLink += '        </a></li>        ';
htmlTextWithLink += '    </ul>';
htmlTextWithLink += '</div>';

var createCarousel = function (o) {
	return $(htmlText).appendTo(document.body).wijcarousel(o);
};

var createCarouselWithLink = function (o) {
	return $(htmlTextWithLink).appendTo(document.body).wijcarousel(o);
};

var createCarouselEmpty = function (o) {
	return $(htmlEmpty).appendTo(document.body).wijcarousel(o);
};

var calculateScrolled = function (widget) {
	var list, left = 0;
	if (widget) {
		list = widget.find("ul.wijmo-wijcarousel-list:eq(0)");
		if (list.length) {
			left = parseInt(list.css("left"));
		}
	}
	return left;
};

(function ($) {

	module("wijcarousel: core");

	test("create and destroy", function () {
		var $widget = createCarousel({
				showTimer: true,
                showPager: true,
                loop: true,
                showContorlsOnHover: true
		});

		ok($widget.hasClass("wijmo-wijcarousel ui-widget"), 'create:element class added.');
		ok($widget.data("wijmo-wijcarousel"), 'create:element data created.');

		ok($widget.find("ul:eq(0)").hasClass("wijmo-wijcarousel-list"), 'create:list class added.');
		ok($widget.find("ul:eq(0)>li:eq(0)").hasClass("wijmo-wijcarousel-item"), 'create:item class added.');
		ok($widget.find("ul:eq(0)>li:eq(0)").data("itemIndex") !== undefined, 'create:item data added.');
		ok($widget.find("ul:eq(0)>li:eq(0) img:eq(0)").hasClass("wijmo-wijcarousel-image"), 'create:image class added.');

		//check child elements
		ok($widget.children(".wijmo-wijcarousel-clip").length, 'create:clip has been created.');

		ok($widget.find(".wijmo-wijcarousel-timerbar").length, "create:timerbar has been created."); //

		ok($widget.find("a.wijmo-wijcarousel-button-previous").length, "create:previous button has been created.");
		
		ok($widget.find("a.wijmo-wijcarousel-button-next").length, "create:next button has been created.");

		ok($widget.find("div.wijmo-wijcarousel-pager").length, "create:pager has been created.");

		$widget.wijcarousel('destroy');

		ok(!$widget.hasClass("wijmo-wijcarousel ui-widget"), 'destroy:element class removed.');
		ok(!$widget.find("ul:eq(0)").hasClass("wijmo-wijcarousel-list"), 'destroy:list class removed.');
		ok(!$widget.find("ul:eq(0)>li:eq(0)").hasClass("wijmo-wijcarousel-item"), 'destroy:item class removed.');
		ok(!$widget.find("ul:eq(0)>li:eq(0) img:eq(0)").hasClass("wijmo-wijcarousel-image"), 'destroy:image class removed.');

		ok(!$widget.find("ul:eq(0)>li:eq(0) .wijmo-wijcarousel-caption,"+
		"ul:eq(0)>li:eq(0) .wijmo-wijcarousel-text").hasClass("wijmo-wijcarousel-image"), 'destroy:image class removed.');

		ok(!$widget.find(".wijmo-wijcarousel-clip,.wijmo-wijcarousel-button-next," +
			".wijmo-wijcarousel-button-previous," +
			".wijmo-wijcarousel-timerbar,.wijmo-wijpager").length,
			"destroy:clip,timerbar,previous/next button,pager has been removed.");

		$widget.remove();
	});
})(jQuery);

