﻿/*globals test, ok, jQuery, module, document, $*/
/*
* wijlineargauge_core.js create & destroy
*/
"use strict";
function createLineargauge(o) {
	return $('<div id="radialgauge" style = "width:600px;height:300px"></div>')
			.appendTo(document.body).wijlineargauge(o);
}

var simpleOptions = {

};

function createSimpleLinearGauge() {
	return createLineargauge(simpleOptions);
}

(function ($) {

	module("wijlineargauge: core");

	test("create and destroy", function () {
		var gauge = createSimpleLinearGauge();

		ok(gauge.hasClass('ui-widget wijmo-wijlineargauge'),
			'create:element class created.');

		gauge.wijlineargauge('destroy');
		ok(!gauge.hasClass('ui-widget wijmo-wijlineargauge'),
			'destroy:element class destroy.');

		gauge.remove();
	});

} (jQuery));

