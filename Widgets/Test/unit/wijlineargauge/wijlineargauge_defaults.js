﻿/*globals commonWidgetTests*/
/*
* wijradialgauge_defaults.js
*/
"use strict";

// override the QUnit.jsDump.typeof method.  
//If an object which has a length property will be parsed as an array type using original method.
// else if (typeof obj === "object" && typeof obj.length === "number" && obj.length >= 0) {
//		type = "array";
//	}
QUnit.jsDump.typeOf = function (obj) {
	var type;
	if (obj === null) {
		type = "null";
	} else if (typeof obj === "undefined") {
		type = "undefined";
	} else if (QUnit.is("RegExp", obj)) {
		type = "regexp";
	} else if (QUnit.is("Date", obj)) {
		type = "date";
	} else if (QUnit.is("Function", obj)) {
		type = "function";
	} else if (obj.setInterval && obj.document && !obj.nodeType) {
		type = "window";
	} else if (obj.nodeType === 9) {
		type = "document";
	} else if (obj.nodeType) {
		type = "node";
	} else if ($.isArray(obj)) {
		type = "array";
	} else {
		type = typeof obj;
	}
	return type;
}

commonWidgetTests('wijlineargauge', {
	defaults: {
		disabled: false,
		create: null,
		value: 0,
		max: 100,
		min: 0,
		autoResize: true,
		isInverted: false,
		initSelector: ":jqmData(role='wijlineargauge')",
		wijCSS: $.extend({
			linearGauge: "wijmo-wijlineargauge",
			linearGaugeLabel: "wijmo-wijlineargauge-label",
			linearGaugeMarker: "wijmo-wijlineargauge-mark",
			linearGaugeFace: "wijmo-wijlineargauge-face",
			linearGaugePointer: "wijmo-wijlineargauge-pointer",
			linearGaugeRange: "wijmo-wijlineargauge-range"
		}, $.wijmo.wijCSS),
		width: "auto",
		height: "auto",
		orientation: "horizontal",
		xAxisLocation: 0.1,
		xAxisLength: 0.8,
		yAxisLocation: 0.5,
		tickMajor: {
			position: "inside", 
			style: { 
				fill: "#1E395B",
				stroke: "none" 
			}, 
			factor: 2, 
			visible: true, 
			marker: "rect", 
			offset: 0, 
			interval: 10 
		},
		tickMinor: {
			position: "inside", 
			style: { 
				fill: "#1E395B",
				stroke: "none" 
			}, 
			factor: 1, 
			visible: false, 
			marker: "rect", 
			offset: 0, 
			interval: 5 
		},

		islogarithmic: false,
		logarithmicBase: 10,
		labels: {
			format: "", 
			style: {
				fill: "#1E395B",
				"font-size": 12,
				"font-weight": "800" 
			}, 
			visible: true, 
			offset: 0 
		},
		animation: {
			enabled: true,
			duration: 2000,
			easing: ">"
		},
		face: {
			style: { 
				fill: "270-#FFFFFF-#D9E3F0", 
				stroke: "#7BA0CC", 
				"stroke-width": 4
			},
			template: null
		},
		marginTop: 5,
		marginRight: 5,
		marginBottom: 5,
		marginLeft: 5,
		ranges: [],
		pointer: {
			length: 0.5, 
			style: { 
				fill: "#1E395B",
				stroke: "#1E395B" 
			}, 
			width: 4, 
			offset: 0, 
			shape: "tri", 
			visible: true, 
			template: null
		},
		beforeValueChanged: null,
		valueChanged: null,
		painted: null
	}
});