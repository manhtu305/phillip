/*
 * wijflipcard_options.js
 */
function createFlipcard(o,id) {
    var id = id ? id : "clickhflipcard";
    var flipcard = $('<div id="' + id + '"><div>front pane.</div><div>back pane.</div></div>').appendTo(document.body);
    return flipcard.wijflipcard(o);
}

function createFlipcardWithStyles(o, id) {
    var id = id ? id : "clickhflipcard";
    var flipcard = $('<div class="aa" id="' + id + '"><div id="front" style="background-color: red;">front pane.</div><div id="back" style="background-color: blue;">back pane.</div></div>').appendTo(document.body);
    return flipcard.wijflipcard(o);
}

(function($) {

    module("wijflipcard: options");
	
	test("flipping", function(){
	    var flipcard = createFlipcard({
	        flipping: function (e, data) {
	            data.frontPanel.html("new fronepanel");
	            data.backPanel.html("new backPanel");
	        },
	        flipped: function(e,data) {
	            ok(data.frontPanel.html() === "new fronepanel", "ok.the frontpanel's content has replaced.");
	            ok(data.backPanel.html() === "new backPanel", "ok.the backPanel's content has replaced.");
	            $("#clickhflipcard").remove();
	            start();
	            
	        }
	    });
	    $(".wijmo-wijflipcard-frontpanel").simulate("click");
	    stop();
	});

	test("flipped", function () {
	    var i = 0, flipcard = createFlipcard({
	        flipped: function (e, data) {                
	            ok(i === 0, "ok.flipped event fired.");
	            $("#clickhflipcard").remove();
	            start();
	           
	        }
	    });
	    $(".wijmo-wijflipcard-frontpanel").simulate("click");
	    stop();
	});

	test("flipped event judge is flipped", function () {
	    var flipcard = createFlipcard({
	        flipped: function (e, data) {
	            ok(data.isFlipped === true, "ok.the panel is flippered");
	            $("#clickhflipcard").remove();
	            start();	            
	        }
	    });
	    $(".wijmo-wijflipcard-frontpanel").simulate("click");
	    stop();
	});


	test("flipped event judge is flipped without animation", function () {
	    var flipcard = createFlipcard({
	        animation: {
	            disabled: true
	        },
	        flipped: function (e, data) {
	            ok(data.isFlipped === true, "ok.the panel is flippered");
	            $("#clickhflipcard").remove();	            
	        }
	    });
	    $(".wijmo-wijflipcard-frontpanel").simulate("click");
	    
	});

	test("flipped event judge is recover in flipped", function () {
	    var i = 0, flipcard = createFlipcard({
	        flipped: function (e, data) {
	            if (i === 1) {
	                ok(data.isFlipped === false, "ok.the panel is recovered!");
	                $("#clickhflipcard2").remove();
	                start();	                
	            }
	            i++;
	        }
	    }, "clickhflipcard2");
	    $(".wijmo-wijflipcard-frontpanel").simulate("click");
	    window.setTimeout(function () {
	        $(".wijmo-wijflipcard-backpanel").simulate("click");	        
	    }, 1500);
	    stop();
	});

	test("flipping without animation", function () {
	    var flipcard = createFlipcard({
	        animation: {
	            disabled: true
	        },
	        flipping: function (e, data) {
	            data.frontPanel.html("new fronepanel");
	            data.backPanel.html("new backPanel");
	        }
	    });
	    $(".wijmo-wijflipcard-frontpanel").simulate("click");
	    ok($(".wijmo-wijflipcard-frontpanel").html() === "new fronepanel", "ok.the frontpanel's content has replaced.");
	    ok($(".wijmo-wijflipcard-backpanel").html() === "new backPanel", "ok.the backPanel's content has replaced.");
	    $("#clickhflipcard").remove();
	});

	test("cancel flipping without animation", function () {
	    var flipcard = createFlipcard({
	        animation: {
	            disabled: true
	        },
	        flipping: function (e, data) {
	            return false;
	        }
	    });
	    $(".wijmo-wijflipcard-frontpanel").simulate("click");
	    ok($(".wijmo-wijflipcard-frontpanel").is(":visible"), "ok.the frontpanel don't flip.");
	    ok($(".wijmo-wijflipcard-backpanel").css("zIndex") === "auto" && parseInt($(".wijmo-wijflipcard-frontpanel").css("zIndex")) === 2, "ok.the backpanel don't flip.");
	    $("#clickhflipcard").remove();
	});

	test("cancel flipping with animation", function () {
	    var flipcard = createFlipcard({
	        flipping: function (e, data) {
	            return false;
	        }
	    });
	    $(".wijmo-wijflipcard-frontpanel").simulate("click");
	    ok($(".wijmo-wijflipcard-frontpanel").is(":visible"), "ok.the frontpanel don't flip.");
	    var backPanel;
	    if ($.browser.msie) {
	        backPanelIsHide = $(".wijmo-wijflipcard-backpanel").css("zIndex") === "auto";
	    } else if ($.browser.mozilla) {
	        backPanelIsHide = $(".wijmo-wijflipcard-backpanel").css("zIndex") === "3";
	    } else {
	        backPanelIsHide = $(".wijmo-wijflipcard-backpanel").css("zIndex") === "0";
	    }

	    ok(backPanelIsHide && parseInt($(".wijmo-wijflipcard-frontpanel").css("zIndex")) === 2, "ok.the backpanel don't flip.");
	    
	    $("#clickhflipcard").remove();
	});

	test("flipcard width and height", function () {
	    var flipcard = createFlipcard({
	        width:400,
	        height:300
	    });
	    ok($(".wijmo-wijflipcard").height() === 300, "height is right");
	    ok($(".wijmo-wijflipcard").width() === 400, "width is right");
	    $("#clickhflipcard").remove();
	});

	if (!$.browser.msie) {
	    test("flipcard duration", function () {
	        var flipcard = createFlipcard({
	            animation: {
	                duration: 800
	            },
	            flipped: function (e, data) {
	                ok($(".wijmo-wijflipcard-panels").css("transition-duration") === "0.8s", "duration is right");
	                $("#clickhflipcard").remove();
	                start();
	            }
	        });
	        $(".wijmo-wijflipcard-frontpanel").simulate("click");
	        stop();	       
	    });
	}

	test("disabled with animation", function () {
	    var flipcard = createFlipcard({
	        disabled:  true
	    });
	    $(".wijmo-wijflipcard-frontpanel").simulate("click");
	    ok($(".wijmo-wijflipcard-frontpanel").is(":visible"), "ok.the frontpanel don't flip.");
	    var backPanel;
	    if ($.browser.msie) {
	        backPanelIsHide = $(".wijmo-wijflipcard-backpanel").css("zIndex") === "auto";
	    } else if ($.browser.mozilla) {
	        backPanelIsHide = $(".wijmo-wijflipcard-backpanel").css("zIndex") === "3";
	    } else {
	        backPanelIsHide = $(".wijmo-wijflipcard-backpanel").css("zIndex") === "0";
	    }
	    ok(backPanelIsHide && parseInt($(".wijmo-wijflipcard-frontpanel").css("zIndex")) === 2, "ok.the backpanel don't flip.");

	    $("#clickhflipcard").remove();
	});

	test("disabled without animation", function () {
	    var flipcard = createFlipcard({
	        disabled: true,
	        animation: {
	            disabled: true
	        }
	    });
	    $(".wijmo-wijflipcard-frontpanel").simulate("click");
	    ok($(".wijmo-wijflipcard-frontpanel").is(":visible"), "ok.the frontpanel don't flip.");
	    var backPanel;
	    backPanelIsHide = $(".wijmo-wijflipcard-backpanel").css("zIndex") === "auto";
	    ok(backPanelIsHide && parseInt($(".wijmo-wijflipcard-frontpanel").css("zIndex")) === 2, "ok.the backpanel don't flip.");

	    $("#clickhflipcard").remove();
	});
})(jQuery);
