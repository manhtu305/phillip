/*
 * wijflipcard_defaults.js
 */
commonWidgetTests('wijflipcard', {
    defaults: {
        /** Selector option for auto self initialization. This option is internal.
        * @ignore
        */
        initSelector: ":jqmData(role='wijflipcard')",
        /** @ignore*/
        wijCSS: $.extend({
                wijFlipCardClass: "",
                wijFlipCardPanelClass: "",
                wijFlipCardFrontPanelClass: "",
                wijFlipCardBackPanelClass: ""
            }, $.wijmo.wijCSS),
        /** @ignore*/
        wijMobileCSS: {
        },
        /** A value that determines whether or not to disable the wijflipcard widget. */
        disabled: false,
        /** A value that indicates the width of the wijflipcard widget. */
        width: null,
        /** A value that indicates the height of the wijflipcard widget. */
        height: null,
        /** A value that indicates the event used to flip between two panels.
          * @remarks The value can be 'click', 'mouseenter'
          */
        triggerEvent: "click",
        animation: {
            disabled: false,
            type: "flip",
            duration: 500,
            direction: "horizontal"
        },
        flipping: null,
        flipped: null
    }
});
