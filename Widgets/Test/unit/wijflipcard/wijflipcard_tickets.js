﻿
(function ($) {

    module("wijflipcard: tickets");

    test("#78133", function () {
        var flipcard = createFlipcard();
        $("#clickhflipcard").wijflipcard("destroy");
        $("#clickhflipcard").wijflipcard();

        ok($(".wijmo-wijflipcard-frontpanel").html() === "front pane.", "Ok.frontpane has content!");
        ok($(".wijmo-wijflipcard-backpanel").html() === "back pane.", "Ok.backpane has content!");
        $("#clickhflipcard").remove();
    });

    // IE9 don't use css3 animation, it's diffcult to control simulate.
    if (!$.browser.msie) {
        test("#70988", function () {
            var flipcard = createFlipcard({
                flipped: function (e, data) {
                    ok(data.isFlipped === true, "ok.the panel is flippered");
                    $("#clickhflipcard").remove();
                    testBtn.remove();
                    start();
                }
            }), testBtn = $("<button/>").appendTo("body");

            testBtn.simulate("click");
            $("#clickhflipcard").wijflipcard("option", "triggerEvent", "mouseenter");
            $("#clickhflipcard").simulate("mouseover");
            stop();
        });
    }

    test("#70922", function () {
        var flipcard = createFlipcard({ disabled: true });
        ok($("#clickhflipcard").hasClass("ui-state-disabled"), "Ok.flipcard is disabled!");

        $("#clickhflipcard").wijflipcard("option", "disabled", false);
        ok(!($("#clickhflipcard").hasClass("ui-state-disabled")), "Ok.flipcard is enabled!");

        $("#clickhflipcard").remove();
    });

    if ($.browser.msie &&
        $.browser.version && parseFloat($.browser.version) <= 9) {
        test("#70988", function () {
            var i = 0; flipcard = createFlipcard({
                flipping: function (e, data) {
                    i++;
                },
                flipped: function (e, data) {
                    if (i === 2) {
                        ok(i === 2, "ok.the second flipping event has fired.");
                        $("#clickhflipcard").remove();
                        start();
                    } else {
                        window.setTimeout(function () {
                            $(".wijmo-wijflipcard-backpanel").simulate("click");
                        }, 2000);
                    }
                }
            });
            $(".wijmo-wijflipcard-frontpanel").simulate("click");
            stop();
        });
    }


    test("#71687", function () {
        var flipcard = createFlipcard({disabled:true});

        ok(!$(".wijmo-wijflipcard").hasClass("wijvflip"), "Ok.wijflipcard has no animation");

        var animation = {
            disabled: false,
            type: "flip",
            direction: "vertical",
            duration: 2000,
        };
        $("#clickhflipcard").wijflipcard("option", "animation", animation);
        ok($(".wijmo-wijflipcard").hasClass("wijvflip"), "Ok.wijflipcard has animation");

        $("#clickhflipcard").remove();
    });

    test("#72403", function () {
        var flipcard = createFlipcard({
            animation: {
                direction: "horizontal",
                duration: 500,
                type: "pulsate"
            }
        });
        $(".wijmo-wijflipcard-frontpanel").simulate("click");
        window.setTimeout(function () {
            $(".wijmo-wijflipcard-backpanel").simulate("click");
            window.setTimeout(function () {
                var animation = {
                    direction: "horizontal",
                    duration: 500,
                    type: "shake",
                };
                $("#clickhflipcard").wijflipcard("option", "animation", animation);
                ok($(".wijmo-wijflipcard-panel").css("opacity") === "1", "Ok.wijflipcard's opcity is resetting");
                $("#clickhflipcard").remove();
                start();
            }, 500);
        }, 500);
        stop();
    });

    if ($.browser.msie &&
        $.browser.version && parseFloat($.browser.version) <= 9) {
        test("#71687-2", function () {
            var flipcard = createFlipcard();

            $(".wijmo-wijflipcard-frontpanel").simulate("click");

            window.setTimeout(function () {
                $(".wijmo-wijflipcard-backpanel").simulate("click");
                window.setTimeout(function () {
                    var animation = {
                        direction: "horizontal",
                        duration: 500,
                        type: "slide",
                    };
                    $("#clickhflipcard").wijflipcard("option", "animation", animation);
                    $(".wijmo-wijflipcard-frontpanel").simulate("click");
                    window.setTimeout(function () {
                        ok($(".wijmo-wijflipcard-frontpanel").css("display") === "none", "Ok.front panel is hidden.");
                        ok($(".wijmo-wijflipcard-backpanel").css("visibility") === "visible", "Ok.back panel is show");
                        $("#clickhflipcard").remove();
                        start();
                    }, 1000);                    
                }, 1000);
            }, 1000);
            stop();
        });
    }

    test("#77590", function () {
        var flipcard = createFlipcardWithStyles({
            animation: {
                direction: "horizontal",
                duration: 400,
                type: "flip"
            }
        });        
        
        $(".wijmo-wijflipcard-frontpanel").simulate("click");
        window.setTimeout(function () {
            $(".wijmo-wijflipcard-backpanel").simulate("click");
            window.setTimeout(function () {
                $("#clickhflipcard").wijflipcard("destroy");
                
                ok($("#clickhflipcard").attr("class") === "aa", "Ok.the dom has recover.");
                ok($("#front").attr("style") === "background-color: red;", "Ok.the dom has recover.");
                ok($("#back").attr("style") === "background-color: blue;", "Ok.the dom has recover.");
                $("#clickhflipcard").remove();
                start();
            }, 800);
        }, 800);
        stop();
    });
})(jQuery);