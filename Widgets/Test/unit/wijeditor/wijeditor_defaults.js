/*
 * wijeditor_defaults.js
 */
"use strict";
var wijeditor_defaults = {
		disabled: false,
		/// <summary>
		/// Use this option to specify container which will be used 
		/// as the parent control for FullScreenMode instead 
		/// of client's area on the web page.
		/// Default: ''.
		/// Type: String.
		/// Code example:
		///  $("#wijeditor").wijeditor({
		///      fullScreenContainerSelector: "container"
		///  });
		/// </summary>
		fullScreenContainerSelector: "",
		/// <summary>
		/// Use this option to set the edit mode.
		/// It has three options: wysiwyg/code/split.
		/// Default: 'wysiwyg'.
		/// Type: String.
		/// Code example:
		///  $("#wijeditor").wijeditor({
		///      editorMode: "code"
		///  });
		/// </summary>
		editorMode: "wysiwyg",
        ///If the compactMode option is true and ribbon is not simple,it will show
        ///compact mode.*/
		compactRibbonMode : false,
		/// <summary>
		/// Use this option to true if you want to switch 
		/// editor to full screen mode.  All client area of 
		/// the page will be covered by wijeditor if this option is true.
		/// Default: false.
		/// Type: Boolean.
		/// Code example:
		///  $("#wijeditor").wijeditor({
		///      fullScreenMode: true
		///  });
		/// </summary>
		fullScreenMode: false,
		/// <summary>
		/// Use this option to indicate whether to show the path selector.
		/// Default: true.
		/// Type: Boolean.
		/// Code example:
		///  $("#wijeditor").wijeditor({
		///      showPathSelector: false
		///  });
		/// </summary>
		showPathSelector: true,
		/// <summary>
		/// Use this option to specify which toolbar(ribbon/simple/bbcode)
		/// should be rendered.
		/// Default: "ribbon".
		/// Type: String.
		/// Code example:
		///  $("#wijeditor").wijeditor({
		///      mode: "simple"
		///  });
		/// </summary>
		mode: "ribbon",
		/// <summary>
		/// Occurs when the command button is clicked.
		/// Default: null.
		/// Type: Function.
		/// Code example:
		/// Supply a function as an option.
		/// $("#wijeditor").wijeditor({commandButtonClick: function(e, data) { } });
		/// Bind to the event by type: wijeditorcommandButtonClick
		/// $("#wijeditor")
		///.bind("wijeditorcommandButtonClick", function(e, data) {} );
		/// </summary>
		/// <param name="e" type="eventObj">
		/// jQuery.Event object.
		/// </param>
		/// <param name="data" type="Object">
		/// An object that contains all command infos of the clicked command button.
		/// data.commandName: the command name of the button.
		/// data.name: the parent name of the button which means 
		///     if the drop down item is clicked, 
		///     then the name specifies the command name of the drop down button.
		///     Says that VeryLarge font size is clicked, 
		///     then commandName = "verylarge", name = "fontsize".
		/// </param>
		commandButtonClick: null,
		/// <summary>
		/// Use this option to custom simple toolbar.
		/// Default: null.
		/// Type: array.
		/// Code example:
		///  $("#wijeditor").wijeditor({
		///      simpleModeCommands: ["bold","undo"]
		///  });
		/// </summary>
		simpleModeCommands: null,
		/// <summary>
		/// Use this option to indicate whether to show the footer.
		/// Default: true.
		/// Type: Boolean.
		/// Code example:
		///  $("#wijeditor").wijeditor({
		///      showFooter: false
		///  });
		/// </summary>
		showFooter: true,
		/// <summary>
		/// Determine if the custom context menu should be shown.
		/// (The context menu is invalid in Chrome & Safari)
		/// Default: true.
		/// Type: Boolean.
		/// Code example:
		///  $("#wijeditor").wijeditor({
		///      customContextMenu: false
		///  });
		/// </summary>
		customContextMenu: true,
		/// <summary>
		/// The text of editor.
		/// Default: null.
		/// Type: String.
		/// Code example:
		///  $("#wijeditor").wijeditor({
		///      text: "Editor string"
		///  });
		/// </summary>
		text: null,
		/// <summary>
		/// Occurs when the text changed.
		/// Default: null.
		/// Type: Function.
		/// Code example:
		/// Supply a function as an option.
		/// $("#wijeditor").wijeditor({textChanged: function(e, data) { } });
		/// Bind to the event by type: textChanged
		/// $("#wijeditor")
		///.bind("textChanged", function(e, data) {} );
		/// </summary>
		/// <param name="e" type="eventObj">
		/// jQuery.Event object.
		/// </param>
		/// <param name="data" type="Object">
		/// An object that contains text of the editor.
		/// data.text: the text of the editor.
		/// </param>
		textChanged: null,
		localization: null,
		wijCSS: $.extend({
		}, $.wijmo.wijCSS),
		initSelector: ":jqmData(role='wijeditor')",
		create: null,
        /**
        * The FontSizes property specifies the list of font size 
        * which will be shown in the font size drop down list. 
        *
        */
		fontSizes : [{
		    tip: "VerySmall",
		    name: "xx-small",
		    text: "VerySmall"
		}, {
		    tip: "Smaller",
		    name: "x-small",
		    text: "Smaller"
		}, {
		    tip: "Small",
		    name: "small",
		    text: "Small"
		}, {
		    tip: "Medium",
		    name: "medium",
		    text: "Medium"
		}, {
		    tip: "Large",
		    name: "large",
		    text: "Large"
		}, {
		    tip: "Larger",
		    name: "x-large",
		    text: "Larger"
		}, {
		    tip: "VeryLarge",
		    name: "xx-large",
		    text: "VeryLarge"
		}],
        /**
        * The FontNames property specifies the list of font name 
        * which will be shown in the font name drop down list.
        */
		fontNames: [{
		    tip: "Arial",
		    name: "fn1",
		    text: "Arial"
		}, {
		    tip: "Courier New",
		    name: "fn2",
		    text: "Courier New"
		}, {
		    tip: "Garamond",
		    name: "fn3",
		    text: "Garamond"
		}, {
		    tip: "Tahoma",
		    name: "fn4",
		    text: "Tahoma"
		}, {
		    tip: "Times New Roman",
		    name: "fn5",
		    text: "Times New Roman"
		}, {
		    tip: "Verdana",
		    name: "fn6",
		    text: "Verdana"
		}, {
		    tip: "Wingdings",
		    name: "fn7",
		    text: "Wingdings"
		}],
        /*A value indicates whether to show the virtual border for table in the edit.
        *It only works when the border style is not set in table.
        */
        tableVirtualBorderShowing: true,
        readOnly: false
	};

commonWidgetTests('wijeditor', {
	defaults: wijeditor_defaults
});
