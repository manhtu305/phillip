﻿/*globals test, ok, jQuery, module, document, $*/
/*
* wijeditor_core.js 
*/
"use strict"
function createEditor(id,o) {
	var text = $('<textarea id=' + id +' style="width: 756px; height: 475px;">aaaa</textarea>').appendTo(document.body);
	return text.wijeditor(o);
}

function createEditorInWidget(o) {
    var widgetDiv = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
     text = $('<textarea id="wijeditor" style="width: 756px; height: 475px;">aaaa</textarea>').appendTo(widgetDiv);
    return text.wijeditor(o);
}

function createEditorInTabs(o) {
    var widgetDiv = $('<div id="C1Tabs1">' +
        '<ul>' +
            '<li><a href="#tabs-1">Some Sample Text</a></li>' +
            '<li><a href="#tabs-2">Wijmo Editor</a></li>' +
        '</ul>' +
        '<div id="tabs-1">' +
            '<p>Testing 123</p>' +
        '</div>' +
        '<div id="tabs-2">' +
            '<textarea id="wijeditor" style="width: 800px; height: 600px;" >' +
            '</textarea>' +
        '</div>' +
    '</div>').appendTo(document.body);
    $("#C1Tabs1").wijtabs();
    return $("#wijeditor", "#C1Tabs1").wijeditor();
}

(function ($) {
    module("wijeditor: core");
    
	test("create", function() {
	    var editor = createEditor("editor1");
	    ok($(".wijmo-wijeditor").length !== 0, "Ok.Editor has created.");
	    editor.wijeditor('destroy');
	    editor.remove();
	});

	if (!$.browser.mozilla) {
	    test("redo and undo", function(){		
		
		        var editor = createEditor("editor1");
			    var content,
			    selectAllBtn = $(".wijmo-wijribbon-selectall").parent(),
			    cutBtn = $(".wijmo-wijribbon-cut").parent(),
			    undoBtn = $(".wijmo-wijribbon-undo").parent(),
			    redoBtn = $(".wijmo-wijribbon-redo").parent();
			
			    ok(undoBtn.is(":disabled"), "Ok.undo is disabled.");
			    ok(redoBtn.is(":disabled"), "Ok.redo is disabled.");
			
			    content = editor.data("wijmo-wijeditor")._getDesignViewText();
			   
			    if (!$.browser.webkit) {

			        window.setTimeout(function () {
			            ok(content === 'aaaa', "Ok.Content is ok");
			            editor.wijeditor('focus');
			            selectAllBtn.simulate('click');
			            cutBtn.simulate('click');

			            ok(editor.data("wijmo-wijeditor")._getDesignViewText() !== 'aaaa', "Ok.Content is cutted");
			            ok(!undoBtn.is(":disabled"), "Ok.undo is enabled.");
			            ok(redoBtn.is(":disabled"), "Ok.redo is disabled.");
			            undoBtn.simulate('click');
			            ok(editor.data("wijmo-wijeditor")._getDesignViewText() === 'aaaa', "Ok.Content is recover");
			            redoBtn.simulate('click');
			            ok(editor.data("wijmo-wijeditor")._getDesignViewText() !== 'aaaa', "Ok.Content is cutted");
			            ok(redoBtn.is(":disabled"), "Ok.redo is disabled.");
			            ok(!undoBtn.is(":disabled"), "Ok.undo is enabled.");
			            start();
			            editor.wijeditor('destroy');
			            editor.remove();
			        }, 2000);
			        stop();
			    } else {
			        editor.wijeditor('destroy');
			        editor.remove();
			    }
	    });
	}

	test("destroy", function () {
	    var editor = createEditor("editor1");
		ok($(".wijmo-wijeditor").length !== 0, "Ok.Editor exist.");
		editor.wijeditor('destroy');
		ok($(".wijmo-wijeditor").length === 0, "Ok.Editor has destroy.");
		editor.remove();
	});

if (!$.browser.webkit) {
	test("two eidtor in form", function() {		
		var editor = createEditor("editor1");
		var editor2 = createEditor("editor2");
		var
        selectAllBtn1 = $(".wijmo-wijribbon-selectall", $(".wijmo-wijeditor")[0]).parent(),
        boldBtn1 = $("#wijeditor-editor1-bold");
		    window.setTimeout(function () {
		        ok(editor.data("wijmo-wijeditor")._getDesignViewText() === 'aaaa', "Ok.editor Content is right");
		        ok(editor2.data("wijmo-wijeditor")._getDesignViewText() === 'aaaa', "Ok.editor Content is right");
		        selectAllBtn1.simulate('click');		        
		        window.setTimeout(function () {
		            $("body").focus();
		            boldBtn1.simulate('click');
		            ok(editor.data("wijmo-wijeditor")._getDesignViewText() !== 'aaaa', "Ok.editor Content is bold");
		            ok(editor2.data("wijmo-wijeditor")._getDesignViewText() === 'aaaa', "Ok.editor2 Content isn't change");
		            start();
		            editor.wijeditor('destroy');
		            editor.remove();
		            editor2.wijeditor('destroy');
		            editor2.remove();		            
		        }, 1000);		        
		    }, 2000);
		    stop();		 
	});
}

test("remove dialog when call destroy method", function () {
	var container = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
			text = $('<textarea id="wijeditor49862" style="width: 756px; height: 475px;"></textarea>').appendTo(container),
			editor = text.wijeditor(),
			widget = editor.data("wijmo-wijeditor"),
			dlg = widget.dialog;

	container.find("ul:first").find("li:last").find("a").simulate("click");
	container.find(".wijmo-wijribbon-link").simulate("click");
	ok(dlg.parent().length === 1, "the dialog is added to the DOM.");
	text.remove();
	ok(dlg.parent().length === 0, "the dialog is removed");
});
}(jQuery));