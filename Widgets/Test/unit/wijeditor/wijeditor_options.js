/*
 * wijeditor_options.js
 */
function createEditor(o) {
	var text = $('<textarea id="wijeditor1" style="width: 756px; height: 475px;">aaaa</textarea>').appendTo(document.body);
	return text.wijeditor(o);
}
(function($) {

	module("wijeditor: options");
	
	test("editorMode", function(){
		var editor = createEditor();
		editor.wijeditor("option","editorMode","split");
		ok($(".wijmo-wijsplitter-h-panel1").is(":visible") , "ok.design view panel exist.");
		ok($(".wijmo-wijsplitter-h-panel2").is(":visible"), "ok.design view panel exist.");
		editor.remove();
	});
	
	test("showPathSelector", function () {
	    var editor = createEditor();
		editor.wijeditor("option","showPathSelector",false);
		ok(!($(".wijmo-wijmoeditor-pathselector").is(":visible")), "ok.path selector is invisible.");
		editor.remove();
	});
	
	test("showFooter", function () {
	    var editor = createEditor();
		editor.wijeditor("option","showFooter",false);
		ok(!($(".wijmo-wijmoeditor-footer").is(":visible")), "ok.footer is invisible.");
		editor.remove();
	});
	
	test("fullScreenMode", function () {
	    var editor = createEditor(),
	        oriDocOverFlow = $(document.documentElement).css("overflow");
	    editor.wijeditor("option", "fullScreenMode", true);
	    ok($(".wijmo-wijeditor").width() === $(window).width(), "ok.editor is fullscreen.");
	    ok($(document.documentElement).css("overflow") === "hidden", "ok.document's overflow is overhidden.");
		var 	
		codeBtn1 = $(".wijmo-wijribbon-sourceview",$(".wijmo-wijeditor")[0]).parent();
		codeBtn1.simulate('click');
		ok(parseInt($("textarea",$(".wijmo-wijeditor")[0]).height()) === 
	    	parseInt($("div.wijmo-wijsplitter-h-panel2-content",$(".wijmo-wijeditor")[0]).height()) , "ok.source height is right.");
		editor.remove();
		ok($(document.documentElement).css("overflow") === oriDocOverFlow, "ok.ok.document's overflow is recover.");
	});
	
	test("text", function () {
	    var editor = createEditor();
	    window.setTimeout(function () {
	        editor.wijeditor("option", "text", "text");
	        ok(editor.wijeditor("getText") === "text", "ok.text is set.");
	        start();
	        editor.remove();
	    }, 100);
	    stop();
	});
	
	test("fullScreenContainerSelector", function () {
	    var editor = createEditor();
		var fullScreenContainer = $("<div></div>").addClass("editor-container")
		.width(500).height(800).appendTo(document.body);
		editor.wijeditor("option", "fullScreenMode", true);
		editor.wijeditor("option", "fullScreenContainerSelector", ".editor-container");
		ok($(".wijmo-wijeditor").parent().is($(".editor-container")), "ok.fullScreenContainerSelector take effect.");
		fullScreenContainer.remove();
		editor.remove();
	});

	test("#44651- add fontsize option", function () {
	    var container = $("<div>").appendTo("body"),
            font10Text, font11Text,
        editor = $('<textarea id="wijeditor" style="width: 756px; height: 475px;">text</textarea>').appendTo(container).wijeditor({
            fontSizes: [{
                tip: "10px",
                name: "10px",
                text: "10px"
            }, {
                tip: "11px",
                name: "11px",
                text: "11px"
            }]
        });
	    window.setTimeout(function () {
	        $($(".wijmo-wijribbon-selectall", container).parent()).simulate('click');
	        $(".wijmo-wijribbon-fontsize", container).find("button").simulate('mouseup');
	        $("#wijeditor-wijeditor-10px").simulate("click");

	        if ($.browser.msie) {
	            font10Text = "font-size: 10px";
	        } else {
	            font10Text = "font-size:10px";
	        }
	        ok(editor.wijeditor("getText").toLowerCase().indexOf(font10Text) > 0,
                 "The font size has set to 10px.")

	        $($(".wijmo-wijribbon-selectall", container).parent()).simulate('click');
	        $(".wijmo-wijribbon-fontsize", container).find("button").simulate('mouseup');
	        $("#wijeditor-wijeditor-11px").simulate("click");
	        if ($.browser.msie) {
	            font11Text = "font-size: 11px";
	        } else {
	            font11Text = "font-size:11px";
	        }
	        ok(editor.wijeditor("getText").toLowerCase().indexOf(font11Text) > 0,
                 "The font size has set to 11px.")
	        ok(editor.wijeditor("getText").toLowerCase().indexOf(font10Text) < 0,
     "The font size has set to 11px.")
	        start();
	        editor.remove();
	        container.remove();
	    }, 1000);

	    stop();
	});

	test("#44651-add fontname option", function () {
	    var container = $("<div>").appendTo("body"),
            font1Text, font2Text,
        editor = $('<textarea id="wijeditor" style="width: 756px; height: 475px;">text</textarea>').appendTo(container);
	    editor.wijeditor({
            fontNames: [{
                tip: "Arial",
                name: "Arial",
                text: "Arial"
            }, {
                tip: "Garamond",
                name: "Garamond",
                text: "Garamond"
            }]
        });

	    window.setTimeout(function () {
	        $($(".wijmo-wijribbon-selectall", container).parent()).simulate('click');
	        $(".wijmo-wijribbon-fontname", container).find("button").simulate('mouseup');
	        $("#wijeditor-wijeditor-Arial", container).simulate("click");
	        font1Text = 'arial';

	        ok(editor.wijeditor("getText").toLowerCase().indexOf(font1Text) > 0,
                 "The font size has set to Arial.")

	        $($(".wijmo-wijribbon-selectall", container).parent()).simulate('click');
	        $(".wijmo-wijribbon-fontname", container).find("button").simulate('mouseup');
	        $("#wijeditor-wijeditor-Garamond", container).simulate("click");
	        font2Text = 'garamond';
	        ok(editor.wijeditor("getText").toLowerCase().indexOf(font2Text) > 0,
                 "The font size has set to Garamond.")
	        ok(editor.wijeditor("getText").toLowerCase().indexOf(font1Text) < 0,
                "The font size has set to Garamond.")
	        start();
	        editor.remove();
	        container.remove();
	    }, 1000);

	    stop();
	})

	test("#72052:add compactRibbonMode option", function () {
	    var widgetDiv = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
            editor = $('<textarea id="wijeditor1" style="width: 756px; height: 475px;"></textarea>').appendTo(widgetDiv).wijeditor();

	    ok($(".wijmo-wijribbon-actions", widgetDiv).is(":visible"), "The ribbon is full mode.");
	    editor.wijeditor("option", "compactRibbonMode", true);
	    ok($(".wijmo-wijribbon-actions", widgetDiv).is(":hidden"), "The ribbon is compact mode.");
	    editor.remove();
	    widgetDiv.remove();
	});

	test("disabled settings", function () {
	    var $widget = createEditor("widget83048");
	    ok($("div.ui-state-disabled").length === 0, "The disabled div has not appended.");
	    $widget.wijeditor("option", "disabled", true);
	    ok($("div.ui-state-disabled").length === 1, "The disabled div is append to body.");
	    $widget.wijeditor("option", "disabled", false);
	    ok($("div.ui-state-disabled").length === 0, "The disabled div is removed.");
	    $widget.remove();
	})

	test("#readOnly", function () {
		var container111339 = $("<div id='con111339'></div>").appendTo("body"),
            editor = $("<textarea style='width:700px;height:300px'></textarea>").appendTo(container111339),
	        doc, result, expectedOn, expectedOff;
		editor.wijeditor({ readOnly: true });
		editorEle = editor.data("wijmo-wijeditor").editor;

		if ($.browser.mozilla) {
			expectedOn = "designMode:off.contenteditable:undefined";
			expectedOff = "designMode:on.contenteditable:undefined";
		} else {
			expectedOn = "designMode:off.contenteditable:false";
			expectedOff = "designMode:on.contenteditable:true";
		}

		container111339.show();
		setTimeout(function () {
			doc = editorEle.find("iframe")[0].contentDocument;
			result = "designMode:" + doc.designMode + ".contenteditable:" + $(doc.body).attr("contenteditable");
			ok(result == expectedOn, "Expected:" + expectedOn + ".Actual:"+ result);

			editor.wijeditor("option", "readOnly", false);
			setTimeout(function () {
				doc = editorEle.find("iframe")[0].contentDocument;
				result = "designMode:" + doc.designMode + ".contenteditable:" + $(doc.body).attr("contenteditable");
				ok(result == expectedOff, "Expected:" + expectedOff + ".Actual:" + result);
				container111339.remove();
				start();
			}, 1200);
		}, 1200);

		stop();
	});

})(jQuery);
