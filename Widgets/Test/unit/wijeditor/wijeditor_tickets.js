﻿(function ($) {
	module("wijeditor: tickets");    
	test("spliter crash", function () {
		var editor = createEditor();
		$(".wijmo-wijribbon-sourceview").trigger("click");
		$("#wijeditor-undefined-code").trigger("click");
		ok(1, "the browser is not crashed.")
		editor.remove();
	});

	//test for ie11
	if ($.browser.msie && $.browser.version && parseFloat($.browser.version) >= 11) {
		test("#41031", function () {            
			var widgetDiv = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
				text = $('<textarea id="wijeditor1" style="width: 756px; height: 475px;"></textarea>').appendTo(widgetDiv),
				editor = text.wijeditor(),
				doc = widgetDiv.find("iframe")[0].contentWindow.document,
				insetTab = widgetDiv.find("ul:first").find("li:last").find("a"),
				radioBtn = widgetDiv.find("button[title='RadioButton']"),
				formBtn = widgetDiv.find("button[title='Form']"),
				textAreaBtn = widgetDiv.find("button[title='TextArea']");

			insetTab.simulate("click");
			window.setTimeout(function () {
				$(radioBtn).simulate("focus");
				$(radioBtn).simulate("click");
				ok($(doc.body).find("input[type='radio']").length, "Button for inserting radio button doesn't work.");
				$(formBtn).simulate("click");
				ok($(doc.body).find("form").length, "Button for inserting form doesn't work.");
				$(textAreaBtn).simulate("click");
				ok($(doc.body).find("textarea").length, "Button for inserting textarea doesn't work.");
				start();
				editor.remove();
				text.remove();
				widgetDiv.remove();
			}, 1000);            
			stop();
		});
	};
	

	////#29981
	//test("#37727", function () {
	//    var $widget = createEditor();
	//    $widget.wijeditor("option", "fullScreenMode", true);
	//    $widget.wijeditor("option", "editorMode", "split");
	//    var oriHeight = $widget.find('.wijmo-wijsplitter-h-panel1').height();
	//    $widget.find('.ui-resizable-s').simulate("mouseover").simulate("drag", {
	//       dy: -50
	//    });
		
	//    window.setTimeout(function () {
	//        ok($widget.find('.wijmo-wijsplitter-h-panel1').height() == oriHeight - 50, "resiable take effect.");
	//        console.log($widget.find('.wijmo-wijsplitter-h-panel1').height());
	//        start();
	//        $widget.remove();
	//    }, 2000);
	//    stop();
		
	//});

	if ($.browser.msie) {
		test("#41717", function () {
			var message = "Testing C1Editor For case 41717";
			var except = $.browser.msie &&
			$.browser.version && parseFloat($.browser.version) >= 11 ? "<strike><u><font color=\"#7c2d2d\">Testing C1Editor For case 41717</font></u></strike>" : "<font color=\"#7c2d2d\"><u><strike>" + message + "</strike></u></font>";
			var result;

			var wrapper41717 = $("<div id='wrapper41717'></div>").appendTo(document.body);
			var text = $('<textarea id="wijeditor1" style="width: 756px; height: 475px;">' + message + '</textarea>').appendTo(wrapper41717);
			var editor = text.wijeditor();
			var designPanel = wrapper41717.find("iframe")[0].contentWindow;
			var doc = designPanel.document;

			var underlineBtn = wrapper41717.find("label[title='Underline']");
			var strikeBtn = wrapper41717.find("label[title='Strikethrough']");
			var fontcolorBtn = wrapper41717.find("button[title='Font Color']");

			var range = doc.body.createTextRange();
			range.moveToElementText(doc.body);
			range.select();

			$(strikeBtn).simulate("click");
			$(underlineBtn).simulate("click");
			$(fontcolorBtn).simulate("click");

			var colorInput = $.find("input[type='text']");
			$(colorInput).attr("value", "#7c2d2d");
			$(colorInput).attr("style", "background-color: rgb(124, 45, 45); color: rgb(255, 255, 255);")

			var okBtn = $.find("input[value='OK']");
			$(okBtn).simulate("click");

			result = doc.body.innerHTML;

			var end = except == result;
			ok(end, except + " *** " + result);
			delete doc;
			delete designPanel;
			text.wijeditor("destroy");
			$(".wijmo-wijdialog").remove();
			text.remove();
		})
	}

	test("#38045", function () {
		var $widget = createEditorInWidget({ mode: "simple", disabled: true });

		stop();
		window.setTimeout(function () {
			$("#wrapperWijEditor").addClass("ui-widget");
			ok($("#wrapperWijEditor").find('.wijmo-wijeditor-container').height() === $("#wrapperWijEditor").find('.wijmo-wijeditor').height(), "disabled div's size is correct..");
			ok($("#wrapperWijEditor").find('.wijmo-wijeditor').height() ===  $("#wrapperWijEditor").find('div.ui-state-disabled').height(), "disabled div's size is correct..");
			start();
			$("#wrapperWijEditor").remove();
			$widget.remove();
		}, 1000);
	});

	
	if (!$.browser.msie) {
		test("#38320", function () {
			var editor = createEditor(), 
				focusinput = $('<input type="type">').appendTo("body");
			
			var designDocument = editor.data("wijmo-wijeditor")._getDesignViewDocument(),
			designWin = editor.data("wijmo-wijeditor")._getDesignViewWindow();
			var range = null, sel;
			editor.wijeditor('focus');
		   
			if (designWin) {
				window.setTimeout(function(){
					try {
						sel = designWin.getSelection();
						var textNode = sel.focusNode;
						var newOffset = sel.focusOffset + 3;
						sel.collapse(textNode, Math.min(textNode.length, newOffset));                       
						focusinput.simulate("click");
						$("label[title='Bold']").simulate("click");

						window.setTimeout(function () {
							ok($(document.activeElement).is("iframe"), "The focus is on editor!");
							start();
							editor.remove();
							focusinput.remove();
						}, 500);
					} catch (err) {
						start();
					}
				}, 1000);
				stop();
			}
		});
	}

	if ($.browser.msie && $.browser.version === "10.0") {
		test("#40137", function () {
			var editor = $('<textarea id=' + 'aaa' + ' style="width: 756px; height: 475px;">aaaa</textarea>').appendTo(document.body);
			var dropdown = $('<select><option>A</option><option>B</option><option>C</option><option>D</option><option>E</option></select>')
			.appendTo("body");
			editor.wijeditor({ mode: "simple" });
			dropdown.wijdropdown();

			window.setTimeout(function () {
				ok(1, "the browser is not crashed.")
				start();
				editor.remove();
				dropdown.remove();
			}, 200);
			stop();
		})
	}

	test("#40133", function () {
		var expander = $('<div id="expander" style="width:500px; height: 300px;"> <h1>User could type the text in build 4.0.20131.111.</h1>  <div><textarea id="wijeditor" style="width:800px; height: 200px;"> </textarea><div></div>').appendTo("body");
		$("#expander").wijexpander({ expanded: false });
		var editor = $("#wijeditor").wijeditor({ mode: "bbCode" });
		
		var editorDoc = editor.data("wijmo-wijeditor")._getDesignViewDocument();

		$("h1", "#expander").simulate("click");
		window.setTimeout(function () {            
			if ($.browser.mozilla) {
				ok(editorDoc.designMode === "on", "Ok.editor can be editable!");
			} else {
				ok(editorDoc.body["contentEditable"] === "true", "Ok.editor can be editable!");
			}
			start();
			editor.remove();
			expander.remove();
		}, 1500);
		stop();
	})

	test("#40407", function () {
		var editor = createEditor("editor1"),
			bgcolor = $(".wijmo-wijribbon-bgcolor").parent(), cancelbtn;
		
		window.setTimeout(function(){
			editor.wijeditor('focus');
			bgcolor.simulate('click');
			cancelbtn = $("input.wijmo-wijeditor-dialog-button[value='Cancel']").simulate('click');
			ok($(document.activeElement).is("iframe"), "The focus is on editor!");
			start();
			$(".wijmo-wijdialog").remove();
			editor.remove();
		}, 2000);
	 stop();		
	})

	test("#40388", function () {
		var messageWidthStyle = "<FONT color='#2431db'><STRONG><EM>Testing C1Editor</EM></STRONG></FONT>";
		var findText = "Testing";
		var replaceText = "Test";

		var div40388 = $('<div id="case40388">' + '</div>').appendTo(document.body);

		var text = $('<textarea id=' + "editor40388" + ' style="width: 756px; height: 475px;">' + messageWidthStyle + '</textarea>').appendTo(div40388);
		var editor = text.wijeditor();

		var findBtn = div40388.find(".wijmo-wijribbon-find").parent();
		
		var except, result;
		var designPanel = div40388.find("iframe")[0].contentWindow;
		var doc = designPanel.document;
		var original = doc.body.innerHTML;
		except = original.replace(findText, replaceText);

		setTimeout(function () {
			findBtn.simulate("click");

			var finddlg = $(".wijmo-wijeditor-finddlg");
			finddlg.find("textarea:first", finddlg).val("Testing");
			finddlg.find("textarea:last", finddlg).val("Test");

			finddlg.find("input:last", finddlg).simulate("click");

			var dlgheader = finddlg.parent().parent().find(".ui-dialog-titlebar");
			$("a:last", dlgheader).simulate("click");

			result = doc.body.innerHTML;

			ok(except === result, except + "  ****  " + result);
			start();
			editor.remove();
			text.remove();
			div40388.remove();
			finddlg.remove();
			
		}, 1000);

		stop();
		
	})

	test("#41554", function () {
		var editor = createEditorInTabs();

		window.setTimeout(function () {
			$($("#C1Tabs1").find("a")[1]).simulate('click');
			ok($(".wijmo-wijribbon-font").length > 0, "The font class has added");
			start();
			$("#C1Tabs1").remove();
			editor.remove();
		}, 3000);

		stop();
	});

	test("#41618", function () {
		var container = $("<div>").appendTo("body"),
			dialog,
			editor = $('<textarea style="width: 756px; height: 475px;"></textarea>').appendTo(container).wijeditor();

		setTimeout(function () {
			container.find(".ui-tabs-nav li:last a").simulate("click");
			editor.wijeditor("focus");
			setTimeout(function () {
				container.find(".wijmo-wijribbon-specialchar").simulate("click");
				dialog = $(".wijmo-wijdialog");
				editor.wijeditor("focus");
				setTimeout(function () {
					var label = dialog.find(".wijmo-wijeditor-specialchardlg-punctuation label:first");
					label.simulate("mouseover");
					setTimeout(function () {
						label.simulate("click");
						//clicl the charactor again.
						setTimeout(function () {    						
							container.find(".wijmo-wijribbon-specialchar").simulate("click");
							setTimeout(function () {
								label = dialog.find(".wijmo-wijeditor-specialchardlg-punctuation label:first");
								label.simulate("mouseover");
								setTimeout(function () {
									label.simulate("click");
									ok(editor.wijeditor("getText") === "––", "the charactor set correctly.");
									container.remove();
									start();
								}, 500);
							}, 500);
						}, 500);
					}, 500);
				}, 500);
			}, 500)
		}, 500);
		stop();
	});

	if (!$.browser.msie) {
		test("#41646", function () {
			var container = $("<div>").appendTo("body"),
				dialog,
				editor = $('<textarea style="width: 756px; height: 475px;">text</textarea>').appendTo(container).wijeditor();
			window.setTimeout(function () {
				$($(".wijmo-wijribbon-selectall", container).parent()).simulate('click');
				$($(".wijmo-wijribbon-find", container).parent()).simulate('click');
				$(".wijmo-wijeditor-finddlg-replace").find("textarea").val("aaa");
				$("input[value=Replace]").simulate('click');
				ok(editor.wijeditor("getText") === "aaa", "the text has been replaced correctly.");
				start();
				editor.remove();               
				container.remove();
				$(".wijmo-wijdialog").remove();
			}, 2000);

			stop();
		});

	}

	test("#41800", function () {
		var container = $("<div>").appendTo("body"),
			fullscreebtn,
			dialog,
			editor = $('<textarea style="width: 756px; height: 475px;">text</textarea>').appendTo(container).wijeditor();
		window.setTimeout(function () {
			fullscreebtn = $($(".wijmo-wijribbon-fullscreen", container).parent()),
			fullscreebtn.simulate('mouseover');
			fullscreebtn.simulate('click');
			ok($("span.ui-button-text", fullscreebtn).hasClass("ui-state-active"), "the state hover is kept");
			start();
			editor.remove();
			container.remove();
		}, 2000);

		stop();
	});

	if (!$.browser.webkit && !($.browser.msie &&
			$.browser.version && parseFloat($.browser.version) >= 11)) {
		test("#41758", function () {
			var widgetDiv = $("<div id='wrapperWijEditor'></div>").appendTo(document.body);
			var text = $('<textarea id="wijeditor1" style="width: 756px; height: 475px;"><table width="200" height="200" bgcolor="" border="2" cellspacing="0" cellpadding="0"><tbody><tr id="firstTR"><td>Cell 1-1</td><td>Cell 1-2</td><td>Cell 1-3</td></tr><tr><td>Cell 2-1</td><td>Cell 2-2</td><td>Cell 2-3</td></tr><tr><td>Cell 3-1</td><td>Cell 3-2</td><td>Cell 3-3</td></tr></tbody></table><br></textarea>').appendTo(widgetDiv);
			var editor = text.wijeditor();
			var designPanel = widgetDiv.find("iframe")[0].contentWindow;
			var doc = designPanel.document;
			var targetRow = $(doc.body).find("table")[0].rows[0];

			var insetTab = widgetDiv.find("ul:first").find("li:last").find("a");
			var mergeButton = widgetDiv.find("button[title='Merge Cell']");
			var splitButton = widgetDiv.find("button[title='Split Cell']");

			insetTab.simulate("click");
			window.setTimeout(function () {
				SetSelectedCells(designPanel, doc, targetRow, 0, 2);
				mergeButton.simulate("click");
				ok(targetRow.cells[0].colSpan == 3, "Merge Success!");
			}, 1000);
			window.setTimeout(function () {
				SetSelectedCells(designPanel, doc, targetRow, 0, 0);
				splitButton.simulate("click");
				ok(targetRow.cells[0].colSpan == 2 && targetRow.cells[1].colSpan == 1, "Merge Success!");
			}, 2000);
			window.setTimeout(function () {
				SetSelectedCells(designPanel, doc, targetRow, 0, 1);
				mergeButton.simulate("click");
				ok(targetRow.cells[0].colSpan == 3, "Merge Success!");
			}, 3000);
			window.setTimeout(function () {
				SetSelectedCells(designPanel, doc, targetRow, 0, 0);
				splitButton.simulate("click");
				ok(targetRow.cells[0].colSpan == 2 && targetRow.cells[1].colSpan == 1, "Merge Success!");
				start();
				splitButton.remove();
				mergeButton.remove();
				insetTab.remove();
				editor.remove();
				widgetDiv.remove();
			}, 4000);
			stop();
		});
	}

	test("#42252", function () {
		var editor = createEditorInTabs();

		window.setTimeout(function () {
			$($("#C1Tabs1").find("a")[1]).simulate('click');
			$($(".wijmo-wijribbon-splitview").parent()).simulate('click');
			ok($(".wijmo-wijsplitter-h-panel1").height() > 1, "panel1 is visible");
			start();
			$("#C1Tabs1").remove();
			editor.remove();
		}, 1000);

		stop();
	});

	test("#40702", function () {
		var editor = createEditorInTabs();

		window.setTimeout(function () {
			$($("#C1Tabs1").find("a")[1]).simulate('click');
			window.setTimeout(function () {
				var headerHeight = editor.data("wijmo-wijeditor")._getHeader().outerHeight(true),
					footerHeight = editor.data("wijmo-wijeditor")._getFooter().outerHeight(true),
					contentHeight = editor.data("wijmo-wijeditor")._getContent().outerHeight(true);
				ok(headerHeight + footerHeight + contentHeight === 600, "The height of editor is correct.");
				start();
				 $("#C1Tabs1").remove();
				 editor.remove();
			}, 1000);

		}, 1000);

		stop();
	});

	test("#42886", function () {
		var $widget = createEditor("widget42886", { fullScreenMode: true }),
			fullScreenButtonSpan = $("button[title='Fullscreen']", $widget.closest(".wijmo-wijeditor")).find("span").eq("1");
		ok(fullScreenButtonSpan.hasClass("ui-state-active"), "FullScreenMode button UI has shown.");
		$widget.wijeditor("option", "fullScreenMode", false);
		ok(!fullScreenButtonSpan.hasClass("ui-state-active"), "FullScreenMode button UI has removed.");
		$widget.wijeditor("option", "fullScreenMode", true);
		ok(fullScreenButtonSpan.hasClass("ui-state-active"), "FullScreenMode button UI has shown.");
		$widget.remove();
	})

	if ($.browser.msie) {
		test("Test for case that multi-line text in BBcode mode editor will change to single-line.", function () {
			var editorDoc, fullScreenBtn,
				container = $('<div><textarea id="wijeditorBBcode" style="width:800px; height: 200px;"><p>a</p><p>b</p><p>c</p></textarea><div></div>').appendTo("body"),
				editor = $("#wijeditorBBcode").wijeditor({ mode: "bbcode" }),
				originalHtml, resultHtml, multiLine, designViewBtn, sourceViewBtn;

			editorDoc = editor.data("wijmo-wijeditor")._getDesignViewDocument();
			originalHtml = editor.wijeditor("getText");
			multiLine = editorDoc.body.childNodes.length;

			sourceViewBtn = $("label[title='Source View']", container);
			sourceViewBtn.simulate('click');
			designViewBtn = $("label[title='Design View']", container);
			designViewBtn.simulate('click');

			editorDoc = editor.data("wijmo-wijeditor")._getDesignViewDocument();
			resultHtml = editor.wijeditor("getText");
			if ($.browser.msie &&
			$.browser.version && parseFloat($.browser.version) >= 11) {
				originalHtml += "<br>";
			}
			
			ok(originalHtml === resultHtml, "The multi-line text didn't change!");
			ok(multiLine === editorDoc.body.childNodes.length, "Count of lines didn't change!");
			editor.remove();
		})
	}

	test("#42969", function () {
		var result, expect = "ChangedText",
			container = $("<div>").appendTo("body"),
			editor = $('<textarea style="width: 756px; height: 475px;">text</textarea>').appendTo(container).wijeditor(
			{
				textChanged: function (e, data) {
					result = e.target.value;
				}
			}
			),
			btnForBlur = container.find("label[title='Bold']"),
			designPanel = container.find("iframe")[0].contentWindow,
			doc = designPanel.document, range;

		editor.wijeditor('focus');

		$(doc.body).html(expect);
		if ($.browser.msie) {
		    btnForBlur.simulate("click");
		} else {
		    btnForBlur.simulate("focus");
		}
		ok(expect === result, "Text has changed!");
		editor.remove();
		container.remove();
	})


	test("#45132", function () {
		var editor = createEditor();
		editor.find(".wijmo-wijribbon-splitview").trigger("click");
		ok(editor.find('.wijmo-wijsplitter-h-panel1').height() ===
			editor.find('.wijmo-wijsplitter-h-panel2').height(), 
			"The height of panel1 and panel2 are equal.");
		editor.remove();
	})

	if ($.browser.msie) {
		test("#45984", function () {
			var widgetDiv = $("<div id='wrapperWijEditor2'></div>").appendTo(document.body),
			 text = $('<textarea id="wijeditor2" style="width: 756px; height: 475px;"></textarea>').appendTo(widgetDiv),
			 editor = text.wijeditor(),

			 insetTab = $("#wrapperWijEditor2").find("ul:first").find("li:last").find("a"),
			 insertButton = $("#wrapperWijEditor2").find("button[title='Button']"),
			 insertText = $("#wrapperWijEditor2").find("button[title='TextArea']");

			insetTab.simulate("click");
			window.setTimeout(function () {
				$("body").focus();
				insertButton.simulate("click");
				ok(text.wijeditor("getText").indexOf("button") > 0, "insert button");
				$("body").focus();
				insertText.simulate("click");
				ok(text.wijeditor("getText").indexOf("textarea") > text.wijeditor("getText").indexOf("button") > 0, "insert text after the button");
				start();
				editor.remove();
				widgetDiv.remove();
			}, 2000);

			stop();
		})
	}

	test("#46389", function () {
		var $widget = createEditor("widget46389", { fullScreenMode: true }),
			fullScreenButton = $("button[title='Fullscreen']", $widget.closest(".wijmo-wijeditor")),
			fullscreenWidthFullScreen, fullscreenWidthNormal;

		$widget.wijeditor("option", "fullScreenMode", true);
		fullscreenWidthFullScreen = $("#widget46389").parents(".wijmo-wijeditor").width();
		fullScreenButton.simulate("click");
		fullscreenWidthNormal = $("#widget46389").parents(".wijmo-wijeditor").width();
		ok(fullscreenWidthFullScreen > fullscreenWidthNormal,
					"The fullScreen mode is correct.");
		$widget.remove();
	})

	if ($.browser.msie && $.browser.version && parseFloat($.browser.version) >= 11) {
		test("#41034", function () {
			var message = "Testing C1Editor",
			 except = "<font color=\"#7c2d2d\">" + message + "</font>",
			 result, colorInput, okBtn,

			 wrapper = $("<div id='wrapper'></div>").appendTo(document.body),
			 text = $('<textarea id="wijeditor1" style="width: 756px; height: 475px;">' +
				 message + '</textarea>').appendTo(wrapper),
			 editor = text.wijeditor(),
			 designPanel = wrapper.find("iframe")[0].contentWindow,
			 doc = designPanel.document,

			 fontcolorBtn = wrapper.find("button[title='Font Color']"),

			range = doc.body.createTextRange();
			range.moveToElementText(doc.body);
			range.select();
			$(fontcolorBtn).simulate("click");

			colorInput = $(".ui-dialog").find("input[type='text']").attr("value", "#7c2d2d");


			okBtn = $(".ui-dialog").find("input[value='OK']");
			$(okBtn).simulate("click");

			ok(except === doc.body.innerHTML, "color can set on selection in ie11!");
			editor.remove();
		})
	}

	test("#48008", function () {
		var i = 0, $widget;
		$widget = createEditor("widget48008", {
			"fullScreenMode": true,
			"showFooter": false
		});
		ok(true, "no exception is throwed!");
		$widget.remove();
	})

	if ($.browser.msie && $.browser.version && parseFloat($.browser.version) >= 11) {
		test("#47882", function () {
			var container = $("<div>").appendTo("body"),
			fullscreebtn, sourceviewbtn,
			editor = $('<textarea style="width: 756px; height: 475px;">text</textarea>').appendTo(container).wijeditor();
			window.setTimeout(function () {

				fullscreebtn = $($(".wijmo-wijribbon-fullscreen", container).parent()),
				fullscreebtn.simulate('click');
				sourceviewbtn = $($(".wijmo-wijribbon-sourceview", container).parent()),
				sourceviewbtn.trigger("click");
				ok(true, "no exception is throwed!");
				start();
				editor.remove();
				container.remove();
			}, 2000);

			stop();
		})
	}

	if ($.browser.msie && $.browser.version && parseFloat($.browser.version) >= 11) {
		test("#47841", function () {
			var container = $("<div>").appendTo("body"),
			editor = $('<textarea style="width: 756px; height: 475px;">text</textarea>').appendTo(container).wijeditor();
			window.setTimeout(function () {
				editor.wijeditor("focus");
				editor.data("wijmo-wijeditor").designView.contents().find('body').trigger("contextmenu");
				ok(true, "no exception is throwed!");
				start();
				editor.remove();
				container.remove();
			}, 2000);

			stop();
		})
	}

	test("#48857", function () {
	  var container = $("<div>").appendTo("body"),
		  editor = $('<textarea style="width: 756px; height: 475px;">text</textarea>').appendTo(container).wijeditor(),
		  sourceViewBtn,
		  designViewBtn;

		sourceViewBtn = $($(".wijmo-wijribbon-sourceview", container).parent());
		designViewBtn = $($(".wijmo-wijribbon-designview", container).parent());
		ok(!($(".ui-button-text", sourceViewBtn)).hasClass("ui-state-active"), "sourceView button is deactive");
		window.setTimeout(function () {
			editor.wijeditor("option", "editorMode", "code");
			ok($(".ui-button-text", sourceViewBtn).hasClass("ui-state-active"), "sourceView button is active");
			ok(!($(".ui-button-text", designViewBtn).hasClass("ui-state-active")), "designView button is deactive");
			start();
			editor.remove();
			container.remove();
		}, 2000);

		stop();
	})

	if ($.browser.msie) {
		test("#48389", function () {
			var widgetDiv = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
				text = $('<textarea id="wijeditor1" style="width: 756px; height: 475px;"></textarea>').appendTo(widgetDiv),
				editor = text.wijeditor(),
				insetTab = widgetDiv.find("ul:first").find("li:last").find("a"),
				HiddenBtn = widgetDiv.find("button[title='Hidden Field']"),
				formBtn = widgetDiv.find("button[title='Form']"),
				textAreaBtn = widgetDiv.find("button[title='TextArea']"),
				text;

			insetTab.simulate("click");
			window.setTimeout(function () {
				$(formBtn).simulate("click");
				$(HiddenBtn).focus();
				$(HiddenBtn).simulate("mousedown");
				$(HiddenBtn).simulate("click");
				$(textAreaBtn).focus();
				$(textAreaBtn).simulate("mousedown");
				$(textAreaBtn).simulate("click");
				text = text.wijeditor("getText");
				ok(text.indexOf("textarea") > text.indexOf("hidden"), "textarea in insertted after hidden element!");
				start();
				editor.remove();
				widgetDiv.remove();
			}, 2000);
			stop();
		});
	}
	
	test("#49236", function () {
		var $widget = createEditor("widget49236", {
			disabled: true
		});
		$widget.wijeditor("option", "editorMode", "code");
		ok(document.activeElement !== $widget[0], "editor is not focus");
		$widget.remove();
	})

	if ($.browser.msie) {
		test("#49361", function () {
			var result,
				container = $("<div>").appendTo("body"),
				editor = $('<textarea style="width: 756px; height: 475px;">text</textarea>').appendTo(container).wijeditor(
					{
						"mode": "bbcode"
					}
				);
			window.setTimeout(function () {
				editor.wijeditor("focus");
				$($(".wijmo-wijribbon-blockquote", container).parent()).simulate('click');
				ok(editor.wijeditor("getText").indexOf("blockquote") > 0, "the text has been blockquoted.");
				start();
				editor.remove();
				container.remove();
			}, 1000);

			stop();
		});
	}

	if ($.browser.msie && $.browser.version && parseFloat($.browser.version) >= 11) {
		test("Merge cell for IE11", function () {
			var widgetDiv = $("<div id='wrapperWijEditor'></div>").appendTo(document.body);
			var text = $('<textarea id="wijeditor1" style="width: 756px; height: 475px;"><table width="200" height="200" bgcolor="" border="2" cellspacing="0" cellpadding="0"><tbody><tr id="firstTR"><td>Cell 1-1</td><td>Cell 1-2</td><td>Cell 1-3</td></tr><tr><td>Cell 2-1</td><td>Cell 2-2</td><td>Cell 2-3</td></tr><tr><td>Cell 3-1</td><td>Cell 3-2</td><td>Cell 3-3</td></tr></tbody></table><br></textarea>').appendTo(widgetDiv);
			var editor = text.wijeditor();
			var designPanel = widgetDiv.find("iframe")[0].contentWindow;
			var doc = designPanel.document;
			var targetRow = $(doc.body).find("tr")[0];

			var insetTab = $("#wrapperWijEditor").find("ul:first").find("li:last").find("a");
			var mergeButton = $("#wrapperWijEditor").find("button[title='Merge Cell']");

			insetTab.simulate("click");
			window.setTimeout(function () {
				var rangeObj = doc.createRange();
				rangeObj.selectNodeContents(targetRow);
				doc.getSelection().addRange(rangeObj);
				mergeButton.simulate("click");
				ok(targetRow.cells[0].colSpan == 3, "Merge Success!");
				start();
				widgetDiv.remove();
			}, 1000);
			stop();
		});
	}

	if ($.browser.msie) {
		test("#49862", function () {
			var container = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
				text = $('<textarea id="wijeditor49862" style="width: 756px; height: 475px;">This is test a</textarea>').appendTo(container),
				editor = text.wijeditor(),
				widget = editor.data("wijmo-wijeditor"), doc, range, oldText, date, dateString, selection;
			setTimeout(function () {
				doc = widget._getDesignViewDocument();
				oldText = $(doc.body).text();
				if (doc.selection) {
					range = doc.selection.createRange();
					range.moveToElementText(doc.body);
					range.collapse(false);
					range.select();
				} else {
					range = doc.createRange();
					range.setStart(doc.body.childNodes[0], doc.body.innerText.length);
					doc.getSelection().addRange(range);
				}
				setTimeout(function () {
					$(".wijmo-wijribbon-sourceview", container).simulate("click");
					$(".wijmo-wijribbon-designview", container).simulate("click").simulate("blur");
					setTimeout(function () {
						$(".wijmo-wijribbon-designview", container).simulate("click");
						setTimeout(function () {
							container.find("ul:first").find("li:last").find("a").simulate("click");
							setTimeout(function () {
								date = new Date();
								container.find(".wijmo-wijribbon-datetime").simulate("click");
								dateString = date.toDateString() + ' ' + date.toTimeString();
								ok($(doc.body).text() == oldText + dateString, "the insert date is correct.");
								container.remove();
								start();
							}, 500);
						}, 500);
					}, 500);
				}, 500);
			}, 500);
			stop();
		});
	}

	test("#50733", function () {
		var container = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
			text = $('<textarea id="wijeditor50733" style="width: 756px; height: 475px;"></textarea>').appendTo(container),
			editor = text.wijeditor(),
			widget = editor.data("wijmo-wijeditor"),
			dlg = widget.dialog;
		
		setTimeout(function () {
			container.find("ul:first").find("li:last").find("a").simulate("click");
			widget.focus();
			setTimeout(function () {
				container.find(".wijmo-wijribbon-link").simulate("click");
				dlg.find(".wijmo-wijeditor-linkdlg-address input").val("http://www.wijmo.com");
				dlg.find("#radUrl").prop("checked", true);
				dlg.find(".wijmo-wijeditor-linkdlg-text :input").val("wijmo site");
				setTimeout(function () {
					$(".wijmo-wijeditor-dialog-button[value='OK']").click();
					widget._ribbonCommand("selectall");
					container.find(".wijmo-wijribbon-link").simulate("click");
					ok(dlg.find("#radUrl").prop("checked") === true, "the checked url is not changed.");
					container.remove();
					start();
				}, 500);
			}, 500);
		}, 500);
		stop();
	});

	test("#52041", function () {
		var container = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
			text = $('<textarea id="wijeditor52041" style="width: 756px; height: 475px;"><a href="http://www.wijmo.com">http://www.wijmo.com</a></textarea>').appendTo(container),
			editor = text.wijeditor({
				editorMode: "wysiwig",
				mode: "simple",
				showFooter: false,
				simpleModeCommands: ["Bold", "Italic", "UnderLine", "NumberedList", "BulletedList", "Link"]

			}),
			widget = editor.data("wijmo-wijeditor"),
			dlg = widget.dialog;

		setTimeout(function () {
			widget.focus();
			setTimeout(function () {
				var doc = widget._getDesignViewDocument(), aNode = $(doc).find("a")[0];
				if (doc.selection) {
					var range = doc.body.createTextRange();
					range.moveToElementText(aNode);
					range.select();
				}
				else if (doc.getSelection) {
					var selection = doc.getSelection();
					selection.removeAllRanges();
					var range = doc.createRange();;
					//range.selectNodeContents($(doc).find("a")[0]);
					range.setStart(aNode, 1);
					range.setEnd(aNode, 1);
					selection.addRange(range);
				}
				container.find(".wijmo-wijribbon-link16").simulate("click");
				ok(dlg.find("#radLinkTypeIsText").prop("checked") === true, "the link content is text.");
				dlg.find(".wijmo-wijeditor-dialog-button[value='Cancel']").click();
				container.remove();
				start();
			}, 500);
		}, 500);
		stop();
	});

	if ($.browser.msie) {
		test("#52052", function () {
			var container = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
			   text = $('<textarea id="wijeditor52041" style="width: 756px; height: 475px;">this is test</textarea>').appendTo(container),
			   editor = text.wijeditor(),
			   widget = editor.data("wijmo-wijeditor"),
			   dlg = widget.dialog, doc, oldHTML, version;
			setTimeout(function () {
				doc = widget._getDesignViewDocument();
				oldHTML = $(doc.body).html();
				if (doc.selection) {
					range = doc.selection.createRange();
					range.moveToElementText(doc.body);
					range.collapse(false);
					range.select();
				} else {
					range = doc.createRange();
					range.setStart(doc.body.childNodes[0], doc.body.innerText.length);
					doc.getSelection().addRange(range);
				}
				container.find("ul:first").find("li:last").find("a").simulate("click");
				container.find(".wijmo-wijribbon-link").simulate("click");
				dlg.find(".wijmo-wijeditor-linkdlg-address input").val("http://www.wijmo.com");
				dlg.find("#radUrl").prop("checked", true);
				dlg.find(".wijmo-wijeditor-linkdlg-target").val("_blank");
				dlg.find(".wijmo-wijeditor-linkdlg-text :input").val("wijmo site");
				setTimeout(function () {
					$(".wijmo-wijeditor-dialog-button[value='OK']").click();
					// In bellow IE9, the markup is different than other browsers.
					version = parseInt($.browser.version);
					if ($.browser.msie && version < 9) {
						if (version === 8) {
							ok($(doc.body).html() === oldHTML + '<A href="http://www.wijmo.com" target=_blank>wijmo site</A>', "The link added at correct position.");
						}
						else {
							ok($(doc.body).html() === oldHTML + '<A href="http://www.wijmo.com/" target=_blank>wijmo site</A>', "The link added at correct position.");
						}
					}
					else {
						ok($(doc.body).html() === oldHTML + '<a href="http://www.wijmo.com" target="_blank">wijmo site</a>', "The link added at correct position.");
					}
					container.remove();
					start();
				}, 500);
			}, 500);
			stop();
		});
	}
	
	test("#52236", function () {
		var container = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
			text = $('<textarea id="wijeditor50733" style="width: 756px; height: 475px;"></textarea>').appendTo(container),
			editor = text.wijeditor(),
			widget = editor.data("wijmo-wijeditor"),
			doc;

		widget.focus();
		widget._ribbonCommand("fullscreen");
		setTimeout(function () {
			doc = $("iframe")[0].contentWindow.document;
			doc.body.blur();
			widget._ribbonCommand("fullscreen");
			setTimeout(function () {
				container.find(".wijmo-wijribbon-textarea").simulate("click");
				ok($("textarea, doc.body").length === 1, "The textarea has been inserted in the wijeditor correctly.");
				container.remove();
				start();
			}, 500);
		}, 500);
		stop();
	});

	if (!$.browser.mozilla) {
		test("#57200", function () {
			var container = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
				text = $('<textarea id="wijeditor57200" style="width: 756px; height: 475px;"></textarea>').appendTo(container),
				editor = text.wijeditor({ mode: "simple" }),
				widget = editor.data("wijmo-wijeditor"),
				dlg = widget.dialog,
				doc;

			widget.focus();
			setTimeout(function () {
				doc = widget._getDesignViewDocument();
				$(doc.body).html('<A href="http://www.wijmo.com" target=_blank>wijmo site</A>');
				widget._ribbonCommand("selectall");
				widget._ribbonCommand("bold");
				widget._ribbonCommand("link");
				setTimeout(function () {
					ok(dlg.find(".wijmo-wijeditor-linkdlg-address :input").val() === "http://www.wijmo.com/", "The link dialog can get the address of the selected hyper link after applying the style for the hyper link.");
					ok(dlg.find(".wijmo-wijeditor-linkdlg-text :input").val() === "wijmo site", "The link dialog can get the text of the selected hyper link after applying the style for the hyper link.");
					container.remove();
					start();
				}, 500);
			}, 500);
			stop();
		});

		test("#63461", function () {
			var container = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
				text = $('<textarea id="wijeditor63461" style="width: 756px; height: 475px;"></textarea>').appendTo(container),
				editor = text.wijeditor({ mode: "simple" }),
				widget = editor.data("wijmo-wijeditor"),
				dlg = widget.dialog,
				doc;

			widget.focus();
			setTimeout(function () {
				doc = widget._getDesignViewDocument();
				$(doc.body).html('<A href="http://www.wijmo.com" target=_blank>wijmo site</A>');
				widget._ribbonCommand("selectall");
				widget._ribbonCommand("bold");
				widget._ribbonCommand("link");
				setTimeout(function () {
					ok(dlg.find(".wijmo-wijeditor-linkdlg-address :input").val() === "http://www.wijmo.com/", "The link dialog can get the address of the selected hyper link after applying the style for the hyper link.");
					ok(dlg.find(".wijmo-wijeditor-linkdlg-text :input").val() === "wijmo site", "The link dialog can get the text of the selected hyper link after applying the style for the hyper link.");
					editor.wijeditor("submitHyperLinkDialog");
					ok($(doc.body).html() === $.browser.msie ? '<a href="http://www.wijmo.com/" target="_blank"><strong>wijmo site</strong></a>' : '<a href="http://www.wijmo.com/" target="_blank"><b>wijmo site</b></a>', "The style of the hyper link isn't missed after submitting the Hyper Link Dialog.");
					container.remove();
					start();
				}, 500);
			}, 500);
			stop();
		});
	}

	if ($.browser.msie && parseInt($.browser.version) > 10) {
		// this issue is because the selection's startContainer is not text node, the restore selection method has some issue in this case.
		test("#52052 for IE11", function () {
			var container = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
		   text = $('<textarea id="wijeditor52041" style="width: 756px; height: 475px;"><h3>this is test<span>This is span</span></h3></textarea>').appendTo(container),
		   editor = text.wijeditor(),
		   widget = editor.data("wijmo-wijeditor"),
		   dlg = widget.dialog, doc;
			setTimeout(function () {
				doc = widget._getDesignViewDocument();
				range = doc.createRange();
				range.selectNode(doc.body.childNodes[0]);
				range.collapse(false);
				doc.getSelection().addRange(range);
				
				container.find("ul:first").find("li:last").find("a").simulate("click");
				container.find(".wijmo-wijribbon-link").simulate("click");
				dlg.find(".wijmo-wijeditor-linkdlg-address input").val("http://www.wijmo.com");
				dlg.find("#radUrl").prop("checked", true);
				dlg.find(".wijmo-wijeditor-linkdlg-target").val("_blank");
				dlg.find(".wijmo-wijeditor-linkdlg-text :input").val("wijmo site");
				setTimeout(function () {
					$(".wijmo-wijeditor-dialog-button[value='OK']").click();    				
					ok($(doc.body).html() === '<h3>this is test<span>This is span<a href="http://www.wijmo.com" target="_blank">wijmo site</a></span></h3>', "The link added at correct position.");
					container.remove();
					start();
				}, 500);
			}, 500);
			stop();
		});
	}


	test("#61414", function () {
		var container = $("<div>").appendTo("body"),
			font1Text, font2Text,
		editor = $('<textarea id="wijeditor" style="width: 756px; height: 475px;">text</textarea>').appendTo(container);
		editor.wijeditor({
			fontNames: [{
				tip: "Arial",
				name: "Arial",
				text: "Arial"
			}, {
				tip: "Times New Roman",
				name: "Times New Roman",
				text: "Times New Roman"
			}]
		});
		
		window.setTimeout(function () {
			$(".wijmo-wijribbon-fontname", container).find("button").simulate('mouseup');

			$("label[for=wijeditor-wijeditor-TimesNewRoman]", container).simulate("click");
			ok($("label[for=wijeditor-wijeditor-TimesNewRoman]>span", container).hasClass("ui-state-active"),
				"The Times New Roman has selected style.");

			start();
			editor.remove();
			container.remove();

		}, 1000);

		stop();
	})

	test("#61660", function () {
		var container = $("<div>").appendTo("body"),
			innerContainer = $("<div id='inner'>").appendTo(container),
			font1Text, font2Text, designViewBtn,
		editor = $('<textarea id="wijeditor" style="width: 756px; height: 475px;">text</textarea>').appendTo(container);
		editor.wijeditor({
			editorMode:"code"
		});
		$("textarea", container).simulate("focus");
		$("textarea", container).val("setting text");
		$("textarea", container).simulate('keydown', null);
		$("textarea", container).trigger("blur");
		$(".wijmo-wijeditor", container).appendTo($("#inner"));
		
		window.setTimeout(function () {
		    
			designViewBtn = $("label[title='Design View']", container);
			designViewBtn.simulate('click');

			editorDoc = editor.data("wijmo-wijeditor")._getDesignViewDocument();
			
			ok($(editorDoc.body).text() === "setting text", "The text is kept!");
			start();
			editor.remove();
			container.remove();
		}, 1000);

		stop();
	})

	test("63572", function () {
		var container = $("<div>").appendTo("body"),
		editor = $('<textarea id="wijeditor" style="width: 756px; height: 475px;">text</textarea>').appendTo(container);
		editor.wijeditor();
		editor.wijeditor("destroy");
		editor.wijeditor();
		$(document).simulate('keydown', { keyCode: $.ui.keyCode.ESCAPE });
		ok(1, "the browser is not crashed.");
		container.remove();
		editor.remove();
	});

	test("63572: two editors on forms", function () {
		var container = $("<div>").appendTo("body"),
		editor1 = $('<textarea id="wijeditor1" style="width: 756px; height: 475px;">text</textarea>').appendTo(container);
		editor2 = $('<textarea id="wijeditor2" style="width: 756px; height: 475px;">text</textarea>').appendTo(container);
		editor1.wijeditor();
		editor2.wijeditor();
		editor1.wijeditor("destroy");
		
		window.setTimeout(function () {
			$(".wijmo-wijribbon-fontname", container).find("button").simulate('mouseup');
			$(document).simulate('keydown', { keyCode: $.ui.keyCode.ESCAPE });
			ok(!($(".wijmo-wijribbon-dropdown", container).is(":visible")), "The font dropdown has hidden!");            
			start();
			editor1.remove();
			editor2.remove();
			container.remove();
		}, 2000);

		stop();
	});

	test("64114", function () {
		var container = $("<div>").appendTo("body"),
		editor1 = $('<textarea id="wijeditor1" class="class12">text</textarea>').appendTo(container);       
		editor1.wijeditor({ mode: "simple" });
		window.setTimeout(function () {
			ok($(".wijmo-wijeditor-content", container).height() > 0, "The height of content is right");
			start();
			editor1.remove();
			container.remove();
		}, 2000);

		stop();
	});


	test("63912", function () {
		var pop = $("<div class=\"ui-widget-content ui-corner-all\"" +
						" style=\"width: 300px; height:400px; z-index: 999;\">" +
						"<textarea id=\"edt\" style=\"width:250px;height:180px\"></textarea></div>").appendTo("body"),
			doc;

		pop.wijpopup({
			autoHide: true,
		});

		$("#edt").wijeditor({ mode: "bbcode", editorMode: "code" });
		doc = pop.find("iframe")[0].contentWindow.document;
		pop.wijpopup('showAt', 120, 280);
		$("#edt").wijeditor("executeEditorAction", "wysiwyg");
		setTimeout(function () {
			$(doc.body).simulate("mousedown");
			$(doc.body).simulate("mouseup");
			$(doc.body).simulate("click");
			ok(pop.is(":visible"), "Popup should visible when click wijeditor taht inside popup! ");
			start();
			pop.remove();
		}, 1000);
		stop();
	})


	test("64231", function () {
		var container = $("<div>").appendTo("body"),
		editor1 = $('<textarea class="class12">text</textarea>').appendTo(container);
		editor2 = $('<textarea class="class22">text</textarea>').appendTo(container);
		editor1.wijeditor({ mode: "simple", editorMode: "source" });
		editor2.wijeditor({ mode: "simple", editorMode: "source" });

		sourceViewBtn1 = $($($(".wijmo-wijribbon-sourceview", container)[0]).parent());
		designViewBtn1 = $($($(".wijmo-wijribbon-designview", container)[0]).parent());

		sourceViewBtn2 = $($($(".wijmo-wijribbon-sourceview", container)[1]).parent());
		designViewBtn2 = $($($(".wijmo-wijribbon-designview", container)[1]).parent());

		ok(!($(".ui-button-text", designViewBtn1)).hasClass("ui-state-active"), "designview1 button is deactive");
		ok(!($(".ui-button-text", designViewBtn2)).hasClass("ui-state-active"), "designview2 button is deactive");
		window.setTimeout(function () {
			designViewBtn1.simulate("click");
			ok($(".ui-button-text", designViewBtn1).hasClass("ui-state-active"), "designView1 button is active ");
			ok(!($(".ui-button-text", sourceViewBtn1).hasClass("ui-state-active")), "sourceView1 button is deactive");

			ok(!$(".ui-button-text", designViewBtn2).hasClass("ui-state-active"), "designView2 button is deactive ");
			ok(!($(".ui-button-text", sourceViewBtn2).hasClass("ui-state-active")), "sourceView2 button is deactive");
			start();
			editor1.remove();
			editor2.remove();
			container.remove();
		}, 2000);

		stop();
	});

	if ($.browser.msie) {
		test("#64321", function () {
			var container1 = $("<div id='container1'></div>").appendTo(document.body),
				container2 = $("<div id='container2'></div>").appendTo(document.body),
				editor1 = $('<textarea id="wijeditor1" style="width: 756px; height: 475px;"></textarea>').appendTo(container1).wijeditor(),
				editor2 = $('<textarea id="wijeditor2" style="width: 756px; height: 475px;"></textarea>').appendTo(container2).wijeditor(),
				insetTab1 = container1.find("ul:first").find("li:last").find("a"),
				inputBtn = container1.find("button[title='Button']");
			editor1.focus();
			editor2.focus();
			$("body").focus();
			insetTab1.simulate("click");
			window.setTimeout(function () {
				$("body").focus();
				inputBtn.simulate("click");
				ok(editor1.wijeditor("getText").indexOf("button") > 0, "insert button");
				ok(editor2.wijeditor("getText").indexOf("button") === -1, "It has no button.");
				start();
				editor1.remove();
				editor2.remove();
				container1.remove();
				container2.remove();
			}, 1000);
			stop();
		});
	};

	if ($.browser.msie) {
		test("#65072", function () {
			var container65072 = $("<div id='div65072'></div>").appendTo("body"),
				editor = $("<textarea style='width:700px;height:300px'><input id='textbox1' type='text' value='textbox2' name='textbox2' /></textarea>").appendTo(container65072),
				doc, widget, fullscreenbtn;
			editor.wijeditor({});
			widget = editor.data("wijmo-wijeditor");

			doc = widget._getDesignViewDocument();
			if (doc.body) {
				var controlRange = doc.body.createControlRange();
				controlRange.add(doc.getElementById('textbox1'));
				controlRange.select();
			}
			fullscreenbtn = $(".wijmo-wijribbon-fullscreen", container65072).parent();
			fullscreenbtn.simulate('mouseover');
			fullscreenbtn.simulate('click');
			ok(true);
			editor.remove();
			container65072.remove();
		})

		test("#65215", function () {
			var container215 = $("<div id='div215'></div>").appendTo("body"),
				editor = $("<textarea style='width:700px;height:300px'><input id='textbox' type='text' value='textbox' name='textbox' /></textarea>").appendTo(container215),
				doc, widget, insetTab, insertText, controlRange;
			editor.wijeditor({});
			widget = editor.data("wijmo-wijeditor");

			insetTab = container215.find("ul:first").find("li:last").find("a");
			insertText = container215.find("button[title='TextBox']");

			insetTab.simulate("click");

			doc = widget._getDesignViewDocument();
			controlRange = doc.body.createControlRange();
			controlRange.add(doc.getElementById('textbox'));
			controlRange.select();

			insertText.simulate("focus");
			insertText.simulate("click");
			ok(true);
			editor.remove();
			container215.remove();
		})
	}

	if ($.browser.msie && $.browser.version
		&& parseFloat($.browser.version) >= 9
		&& parseFloat($.browser.version) <= 10) {
		test("#65187", function () {
			var widgetDiv = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
				text = $('<textarea id="wijeditor1" style="width: 756px; height: 475px;"></textarea>').appendTo(widgetDiv),
				editor = text.wijeditor(),
				insetTab = widgetDiv.find("ul:first").find("li:last").find("a"),
				HiddenBtn = widgetDiv.find("button[title='Hidden Field']"),
				textAreaBtn = widgetDiv.find("button[title='TextArea']"),
				text;

			insetTab.simulate("click");
			window.setTimeout(function () {
				$(textAreaBtn).focus();
				$(textAreaBtn).simulate("mousedown");
				$(textAreaBtn).simulate("click");
				$(HiddenBtn).focus();
				$(HiddenBtn).simulate("mousedown");
				$(HiddenBtn).simulate("click");
				$(HiddenBtn).focus();
				$(HiddenBtn).simulate("mousedown");
				$(HiddenBtn).simulate("click");
				document.body.focus();
				$(textAreaBtn).focus();
				$(textAreaBtn).simulate("mousedown");
				$(textAreaBtn).simulate("click");
				text = text.wijeditor("getText");
				ok(text.indexOf("textarea") < text.indexOf("hidden"), "The first textarea in insertted after hidden element!");
				ok(text.lastIndexOf("textarea") > text.indexOf("hidden"), "The last textarea in insertted after hidden element!");
				start();
				editor.remove();
				widgetDiv.remove();
			}, 2000);
			stop();
		});
	};

	test("#65228", function () {
		var container65228 = $("<div id='div65228'></div>").appendTo("body"),
			splitter = $("<div id='vsplitter1' style='width:750px; height:400px'><div> <textarea id='wijeditor' style='width:400px;height:300px'></textarea></div><div></div></div>").appendTo(container65228),
			editor = container65228.find("#wijeditor"),
			doc, widget, contentContainer, editorSplitterPanel1;
		editor.wijeditor({"mode": "simple"});
		widget = editor.data("wijmo-wijeditor");
		contentContainer = widget.editor.find(".wijmo-wijeditor-container");
		editorSplitterPanel1 = widget.editor.find(".wijmo-wijsplitter-h-panel1");

		splitter.wijsplitter({"splitterDistance": 700});

		container65228.hide();
		setTimeout(function () {
			container65228.show();

			if (widget.editor.wijTriggerVisibility) {
				widget.editor.wijTriggerVisibility();
			}

			ok(contentContainer.height() === 300, "Height of editor content is correct.");
			ok(editorSplitterPanel1.width() === 400, "Splitter in editor renders correctly.")
			start();
			editor.remove();
			splitter.remove();
			container65228.remove();
		}, 100);

		stop();
	})

	if ($.browser.msie) {
		test("#64630", function () {
			var widgetDiv = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
				editor = $('<textarea id="wijeditor1" style="width: 756px; height: 475px;"></textarea>').appendTo(widgetDiv).wijeditor({ editorMode: 'code', fullScreenMode: true });
			ok(editor.data("wijmo-wijeditor")._getDesignViewText().length == 0, "The editor content should be empty.");
			editor.remove();
			widgetDiv.remove();
		});
	};


	if ($.browser.msie) {
	    test("#65487", function () {
	        var container = $("<div id='div65487'></div>").appendTo("body"),
                editor = $("<textarea style='width:700px;height:300px'></textarea>").appendTo(container),
                doc, widget, fullscreenbtn;
	        editor.wijeditor({});
	        widget = editor.data("wijmo-wijeditor"),
            insetTab1 = container.find("ul:first").find("li:last").find("a"),
            fullscreenbtn = $(".wijmo-wijribbon-fullscreen", container).parent();
	        inputBtn = container.find("button[title='Button']");
	        textAreaBtn = container.find("button[title='TextArea']");

	        insetTab1.simulate("click");

	        window.setTimeout(function () {
	            inputBtn.simulate("click");
	            textAreaBtn.simulate("click");
	            window.setTimeout(function () {
	                fullscreenbtn.simulate('mouseover');
	                fullscreenbtn.simulate('click');
	                window.setTimeout(function () {
	                    textAreaBtn.simulate("click");
	                    doc = widget._getDesignViewDocument();
	                    ok($(doc.body).find("textarea").length === 2, "insert two textarea button");
	                    start();
	                    editor.remove();
	                    container.remove();
	                }, 1000);
	            }, 1000);
	        }, 1000);
	        stop();

	    })
	}

	test("#68010", function () {
	    var container68010 = $("<div id='con68010' style='display:none'></div>").appendTo("body"),
            editor = $("<textarea style='width:700px;height:300px'></textarea>").appendTo(container68010),
	        widget, splitterContainerHeight, firstPanelHeight;
	    editor.wijeditor({ editorMode: "split" });
	    editorEle = editor.data("wijmo-wijeditor").editor;

	    container68010.show();
	    if (editorEle.wijTriggerVisibility) {
	        editorEle.wijTriggerVisibility();
	    }

	    setTimeout(function () {
	        splitterContainerHeight = editorEle.find(".wijmo-wijeditor-content").height();
	        firstPanelHeight = editorEle.find(".wijmo-wijsplitter-h-panel1").height();
	        ok(Math.abs(splitterContainerHeight - firstPanelHeight * 2) < 2, "SplitMode editor renders its splitter well !");
	        editor.remove();
	        container68010.remove();
	        start();
	    }, 100)

	    stop();
	});

	if ($.browser.msie && $.browser.version && parseFloat($.browser.version) < 11) {
	    test("#67383", function () {
	        var widgetDiv = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
				editor = $('<textarea id="wijeditor1" style="width: 756px; height: 475px;"></textarea>').appendTo(widgetDiv).wijeditor(),
                doc = widgetDiv.find("iframe")[0].contentWindow.document,
                insetTab = widgetDiv.find("ul:first").find("li:last").find("a"),
                textBoxBtn = widgetDiv.find("button[title='TextBox']");

	        insetTab.simulate("click");
	        window.setTimeout(function () {
	            document.body.focus();
	            $(textBoxBtn).simulate("click");
	            $(textBoxBtn).simulate("click");
	            ok($(doc.body).find("input[type='text']").length == 2, "Two textboxes are appended.");
	            start();
	            editor.remove();
	            widgetDiv.remove();
	        }, 1000);
	        stop();
	    });
	};

	if ($.browser.msie) {
	    test("#68067", function () {
	        var container68067 = $("<div id='wrapper68067'></div>").appendTo("body"),
                editor = $("<textarea id='editor68067' style='width: 500px; height: 500px;'></textarea>").appendTo(container68067),
                sourceViewBtn, splitViewBtn, activeElement;

	        editor.wijeditor({ mode: "simple" });
	        sourceViewBtn = $("label[title='Source View']", container68067);
	        splitViewBtn = $("label[title='Split View']", container68067);
	        setTimeout(function () {
	            sourceViewBtn.simulate("click");
	            setTimeout(function () {
	                splitViewBtn.simulate("click");
	                setTimeout(function () {
	                    activeElement = document.activeElement;
	                    ok($(activeElement).is("iframe"), "Focus has return to the iframe of split view");
	                    editor.remove();
	                    container68067.remove();
	                    start();
	                }, 500);
	            }, 500);
	        }, 500);
	        stop();
	    })
	}

	test("#68050", function () {
	    var container = $("<div id='con68050' ></div>").appendTo("body"),

            tab = $( '<div id="tabs">' + 
                '<ul>' +
                    '<li><a href="#tabs-1">Nunc tincidunt</a></li>' +
                    '<li><a href="#tabs-2">Proin dolor</a></li>' +
                '</ul>' +
                '<div id="tabs-1">' +
                    '<textarea id="wijeditor1" class="class1" style="width:600px;height:475px"></textarea>  ' +   
                '</div>' +
                '<div id="tabs-2">' +
                     '<textarea id="wijeditor2" class="class1" style="width:600px;height:475px"></textarea>' +
                '</div>' +
            '</div>')
            .appendTo(container),
	        li1, li2, resizeHandler;

	    tab.wijtabs();
	    $("#wijeditor1").wijeditor();
	    $("#wijeditor2").wijeditor({editorMode:"split"});

	    li1 = container.find("a[href='#tabs-1']");
	    li2 = container.find("a[href='#tabs-2']");
	    li2.simulate("click");

	    window.setTimeout(function () {
	        resizeHandler = $(".ui-resizable-handle", "#tabs-2");
	        resizeHandler.simulate("mouseover").simulate("drag", { dy: 50 });
	        li1.simulate("click");
	        
	        window.setTimeout(function () {
	            ok($("#wijeditor-wijeditor1-format").height() > 50, "the ribbon can be visible.")
	            start();
	            container.remove();
	        }, 500);    
	       
	    }, 1000);
	    stop();
	});

	if ($.browser.msie) {
	test("#69834", function () {
	    var container = $("<div>").appendTo("body"),
			fullscreebtn, splitViewBtn, insetTab, datatimebtn,
			dialog,
			editor = $('<textarea style="width: 756px; height: 475px;">text</textarea>').appendTo(container).wijeditor({ editorMode: "code" });

	    splitViewBtn = $("label[title='Split View']", container);
	    fullscreebtn = $($(".wijmo-wijribbon-fullscreen", container).parent());
	    insetTab = container.find("ul:first").find("li:last").find("a");
	    datatimebtn = container.find(".wijmo-wijribbon-datetime");

	    window.setTimeout(function () {	        
			fullscreebtn.simulate('mouseover');
			fullscreebtn.simulate('click');
			window.setTimeout(function () {
			    splitViewBtn.simulate('click');
			    insetTab.simulate("click");
			    datatimebtn.simulate("click");
			    ok(editor.wijeditor("getText").indexOf("text") > 0, "The native text don't lost.");
			    start();
			    editor.remove();
			    container.remove();
			}, 1000);
	    }, 1000);

	    stop();
	});
	}

	test("#70731", function () {
	    var widgetDiv = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
            editor = $('<textarea id="wijeditor1" style="width: 756px; height: 475px;"></textarea>').appendTo(widgetDiv).wijeditor(),
	        insetTab, btnWidth;

	    insetTab = widgetDiv.find("ul:first").find("li:last").find("a");
	    insetTab.simulate("click");
	    btnWidth = widgetDiv.find(".wijmo-wijribbon-bigbutton.ui-button-icon-only").width();
	    ok(btnWidth>5 && btnWidth<30, "the dropdown's width is less than 3.5em");
	    editor.remove();
	    widgetDiv.remove();
	});

	if ($.browser.msie) {
	    test("#72415", function () {
	        var widgetDiv = $("<div id='wrapperWijEditor72415'></div>").appendTo(document.body),
                editor = $('<textarea id="wijeditor72415" style="width: 756px; height: 475px;"></textarea>').appendTo(widgetDiv).wijeditor(),
                insetTab, btnWidth, ribbonDrop;

	        insetTab = widgetDiv.find("ul:first").find("li:last").find("a");
	        insetTab.simulate("click");
	        ribbonDrop = widgetDiv.find(".wijmo-wijribbon-bigbutton .ui-icon-triangle-1-s").parent().find(".ui-button-text");
	        ribbonDrop.simulate("mouseup");
	        widgetDiv.find("ul.wijmo-wijribbon-dropdown > li.ui-corner-all > button").eq(0).simulate("click");
	        parentDialog = editor.data("wijmo-wijeditor").dialog;
	        setTimeout(function () {
	            parentDialog.find("div.wijmo-wijeditor-tabledlg-rows > input").focus();
	            parentDialog.find("input[value='...']").simulate("mousedown").simulate("click");
	            childDialog = editor.data("wijmo-wijeditor").subDialog.parents(".ui-draggable.ui-dialog");
	            ok(childDialog.position().left > 0, "The dialog is not located in the left");
	            childDialog.simulate('keydown', { keyCode: $.ui.keyCode.ESCAPE });
	            setTimeout(function () {
	                ok(document.activeElement === parentDialog[0], "Focus has return to the parent dialog");
	                editor.remove();
	                widgetDiv.remove();
	                start();
	            }, 10);
	        }, 10);

	        var end = 100;
	        stop();
	    });
	}

	test("#74404", function () {
	    var widgetDiv = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
            editor = $('<div id="dialog" title="Modal dialog">'+
                '<div>' + 
                    '<textarea id="wijeditor" cols="0" rows="0" style="width: 350px; height: 300px;">' +           
                    '</textarea>' +
                '</div>' +
            '</div>')
            .appendTo(widgetDiv)
            .wijdialog({
                autoOpen: false,
                captionButtons: {
                    refresh: { visible: false }
                },
                width: 500,
                modal: true
            });

	    $("#wijeditor").wijeditor({
	        editorMode: "wysiwyg",
	        mode: "simple"
	    });

	    $("#dialog").wijdialog("open");	    
	    setTimeout(function () {
	        $("#dialog").wijdialog("minimize");
	        $("#dialog").wijdialog("close");
	        setTimeout(function () {
	            $("#dialog").wijdialog("open");
	            setTimeout(function () {
	                $("#dialog").wijdialog("restore");
	                ok($(".wijmo-wijsplitter-h-panel1").width() === 350, "the splitter show correctly.");
	                editor.remove();
	                widgetDiv.remove();
	                start();
	            }, 1000);
	        }, 1000);
	    }, 1000);
	    stop();
	});

	test("#61465", function () {
	    var container = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
			text = $('<textarea id="61465" style="width: 756px; height: 475px;"></textarea>').appendTo(container),
			editor = text.wijeditor(),
			widget = editor.data("wijmo-wijeditor"),
			dlg = widget.dialog;

	    setTimeout(function () {
	        container.find("ul:first").find("li:last").find("a").simulate("click");
	        widget.focus();
	        setTimeout(function () {
	            container.find(".wijmo-wijribbon-link").simulate("click");
	            dlg.find(".wijmo-wijeditor-linkdlg-address input").val("http://www.wijmo.com");
	            dlg.find("#radUrl").prop("checked", true);
	            dlg.find(".wijmo-wijeditor-linkdlg-text :input").val("wijmo site");
	            setTimeout(function () {
	                $(".wijmo-wijeditor-dialog-button[value='OK']").click();
	                ok(text.wijeditor("getText").indexOf("wijmo site") >= 0, "url has been insert successfully.");
	                container.remove();
	                start();
	            }, 500);
	        }, 500);
	    }, 500);
	    stop();
	});


	test("#90627", function () {
	    var container90627 = $("<div id='div90294'></div>").appendTo("body"),
            editor = $("<textarea style='width:500px;height:300px'>" +
           'one<br><div title="Print Page Break" style="font-size:1px;page-break-before:always;vertical-align:middle;height:1px;background-color:#c0c0c0">&nbsp;</div><br>two<br></textarea>')
            .appendTo(container90627),
            doc, widget;
	    editor.wijeditor({});
	    widget = editor.data("wijmo-wijeditor"),dlg = widget.dialog;

	    container90627.find(".wijmo-wijribbon-preview").simulate("click");
	    dlg.find(".wijmo-wijeditor-previewdlg-next").simulate("click");
	    ok($(".wijmo-wijeditor-previewdlg-previewiframe iframe").contents().find('body').html().indexOf("two") >= 0);
	    dlg.find(".wijmo-wijeditor-previewdlg-ok").simulate("click");
	    container90627.find(".wijmo-wijribbon-preview").simulate("click");
	    ok($(".wijmo-wijeditor-previewdlg-previewiframe iframe").contents().find('body').html().indexOf("one") >= 0);
	    dlg.find(".wijmo-wijeditor-previewdlg-next").simulate("click");
	    ok($(".wijmo-wijeditor-previewdlg-previewiframe iframe").contents().find('body').html().indexOf("two") >= 0);
	    editor.remove();
	    container90627.remove();
	})


	if ($.browser.msie) {
	    test("#79065", function () {
	        var widgetDiv = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
                editor = $('<div id="dialog" title="Modal dialog">' +
                    '<div>' +
                        '<textarea id="wijeditor" cols="0" rows="0" style="width: 750px; height: 300px;">' +
                        '</textarea>' +
                    '</div>' +
                '</div>')
                .appendTo(widgetDiv)
                .wijdialog({
                    width: 800
                });

	        $("#wijeditor").wijeditor({
	        });
	        window.setTimeout(function () {
	            var doc = $("#dialog").find("iframe")[0].contentWindow.document,
                    insetTab = $("#dialog").find("ul:first").find("li:last").find("a"),
                    radioBtn = $("#dialog").find("button[title='RadioButton']"),
                    textAreaBtn = $("#dialog").find("button[title='TextArea']");
	            insetTab.simulate("click");
	            window.setTimeout(function () {
	                $(radioBtn).simulate("focus");
	                $(radioBtn).simulate("click");
	                ok($(doc.body).find("input[type='radio']").length, "Button for inserting radio button work.");
	                $(".ui-dialog").find(".wijmo-wijdialog-titlebar-minimize").focus().simulate("click");
	                window.setTimeout(function () {
	                    $(".ui-dialog").find(".wijmo-wijdialog-titlebar-restore").focus().simulate("click");
	                    window.setTimeout(function () {
	                        $("#dialog").find("iframe").blur();
	                        doc = $("#dialog").find("iframe")[0].contentWindow.document,
                            $(textAreaBtn).simulate("focus");
	                        $(textAreaBtn).simulate("click");
	                        ok($(doc.body).find("textarea").length, "textarea has inserted to wijeditor.");
	                        start();
	                    }, 300);
	                }, 400);
	            }, 400);
	        }, 400);

	        stop();
	    });
	}

	test("#85126", function () {
	    var container = $("<div id='wrapperWijEditorr92233'></div>").appendTo(document.body),
            text = $('<textarea id="wijeditor92233" style="width: 756px; height: 475px;"></textarea>').appendTo(container),
            editor = text.wijeditor(),
            widget = editor.data("wijmo-wijeditor"),
            dlg = widget.dialog,
            doc;
	    var insetTab = container.find("ul:first").find("li:last").find("a");
	    insetTab.focus().simulate("click");
	    setTimeout(function () {
	        container.find(".wijmo-wijribbon-link").focus().simulate("click");
	        setTimeout(function () {
	            ok($("input", ".wijmo-wijeditor-linkdlg-address ").is(":focus"), "The focus in on dialog address input");
	            ok(!($(document.activeElement).is("iframe")), "The focus is not on editor!");
	            container.remove();
	            start();
	        }, 500);
	    }, 500);
	    stop();
	});

	if ($.browser.msie) {
	    test("#614652", function () {
	        var container = $("<div id='wrapperWijEditorr614652'></div>").appendTo(document.body),
                text = $('<textarea id="wijeditor614652" style="width: 756px; height: 475px;"></textarea>').appendTo(container),
                editor = text.wijeditor(),
                widget = editor.data("wijmo-wijeditor"),
                dlg = widget.dialog,
                doc;

	        widget.focus();
	        setTimeout(function () {
	            doc = widget._getDesignViewDocument();
	            $(doc.body).html('<A href="http://www.wijmo.com" target=_blank>wijmo&nbsp;&nbsp;&nbsp;site</A>');
	            widget._ribbonCommand("selectall");
	            widget._ribbonCommand("link");
	            setTimeout(function () {
	                ok(dlg.find(".wijmo-wijeditor-linkdlg-address :input").val() === "http://www.wijmo.com/", "The link dialog can get the address of the selected hyper link after applying the style for the hyper link.");
	                ok(dlg.find(".wijmo-wijeditor-linkdlg-text :input").val().indexOf("&nbsp;") < 0
                        , "The link dialog can get the text of the selected hyper link exclude html tag.");
	                container.remove();
	                start();
	            }, 500);
	        }, 500);
	        stop();
	    });
	}
	if ($.browser.msie && $.browser.version && parseFloat($.browser.version) >= 11) {
	    test("#92936", function () {
	        var container = $("<div id='wrapperWijEditorr92936'></div>").appendTo(document.body),
            text = $('<textarea id="wijeditor92936" style="width: 756px; height: 475px;"> <span class="wijmo-formatspan" style="font-family: Arial;">imperative</span></textarea>').appendTo(container),
            editor = text.wijeditor({
                fontSizes: [{
                    tip: "40pt",
                    name: "40pt",
                    text: "40pt"
                }]
            });
	        window.setTimeout(function () {	            
	            $(".wijmo-wijribbon-selectall", container).parent().simulate('click');
	            $(".wijmo-wijribbon-removeformat", container).parent().simulate('click');
	            ok(1, "the browser is not crashed.")
	            start();
	            editor.remove();
	            container.remove();
	        }, 500);
	        stop();
	    });
	}

	test("#119863", function () {
		var container, text, editor,insetTab, formatTabHeight, insertTabHeight;
		container = $("<div id='wrapperWijEditorr119863'></div>").appendTo(document.body);
		text = $('<textarea id="wijeditor119863" style="width: 320px; height: 280px;"> </textarea>').appendTo(container);
		editor = text.wijeditor();
		window.setTimeout(function () {
			insetTab = container.find("ul:first").find("li:last").find("a");
			formatTabHeight = $(".wijmo-wijribbon-groups")[0].offsetHeight;
			insetTab.simulate('click');
			insertTabHeight = $(".wijmo-wijribbon-groups")[1].offsetHeight;
			ok(formatTabHeight === insertTabHeight, "Format and Insert Tab heights are same.")
			start();
			editor.remove();
			container.remove();
		}, 500);
		stop();
	});

	test("#112239", function () {
		var widgetDiv = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
			text = $('<textarea id="wijeditor1" style="width: 756px; height: 475px;"></textarea>').appendTo(widgetDiv),
			editor = text.wijeditor(),
			doc = widgetDiv.find("iframe")[0].contentWindow.document,
			insetTab = widgetDiv.find("ul:first").find("li:last").find("a");

		insetTab.simulate("click");
		window.setTimeout(function () {
			$(doc.body).focus();
			pgBreakBtn = widgetDiv.find("button[title='Insert Print Page Break']");
			$(pgBreakBtn).simulate("focus");
			$(pgBreakBtn).simulate("click");
			ok($(doc.body).find("hr.pagebreak").length, "The hr tag is inserted as pagebreak.");
			start();
			editor.remove();
			text.remove();
			widgetDiv.remove();
		}, 1000);
		stop();
	});

	if ($.browser.msie && $.browser.version && ($.browser.version == 9 || $.browser.version == 10)) {
		test("#95063", function () {
			var container = $("<div></div>").appendTo("body"),
				text = $('<textarea id="wijeditor" cols="0" rows="0" style="width: 750px; height: 300px;"></textarea>')
				.appendTo(container), editor = $(text).wijeditor(), widget = editor.data("wijmo-wijeditor"),
				doc = widget._getDesignViewDocument(), isFoundException = false;
			container.find("a[href='#wijeditor-wijeditor-insert']").simulate("click");
			container.find("button[title='TextArea']").simulate("click");
			ok($(doc.body).find("textarea"), "one textarea input element has inserted into the document");
			$(doc.body).find("textarea").simulate("focus");
			window.setTimeout(function () {
				try {
					container.find("button[title='TextArea']").simulate("click");
				} catch (e) {
					isFoundException = true;
				} finally {
					start();
					ok(!isFoundException, "inserting an input element into an added one doesn't throw a exception");
					editor.remove();
					container.remove();
				}
			}, 500);
			stop();
		});
	}

	test("#124163", function () {
		var editor = $('<div id="div_124163" style="width: 756px; height: 475px;"></div>').appendTo(document.body);
		editor.wijeditor();
		editor.wijeditor("destroy");
		ok($(document.body).find("#div_124163").length);
		editor.remove();
	})

	test("#136585", function () {
		var widgetDiv = $("<div id='wrapperWijEditor'></div>").appendTo(document.body),
			text = $('<textarea id="wijeditor1" style="width: 756px; height: 475px;"></textarea>').appendTo(widgetDiv),
			editor = text.wijeditor(),
			doc = widgetDiv.find("iframe")[0].contentWindow.document,
			insetTab = widgetDiv.find("ul:first").find("li:last").find("a"),
			widget = editor.data("wijmo-wijeditor"),
			pgBreakBtn;

		insetTab.simulate("click");
		window.setTimeout(function () {
			$(doc.body).focus();
			pgBreakBtn = widgetDiv.find("button[title='Insert Print Page Break']");
			$(pgBreakBtn).simulate("focus");
			$(pgBreakBtn).simulate("click");
			ok(widget._getPreviewPageCount() === 2, "The content has been split with 2 pages.")
			editor.remove();
			text.remove();
			widgetDiv.remove();
			start();
		}, 1000);
		stop();
	});

	test("#147233", function () {
		var container = $("<div id='EmailTemplate' style='width: 756px; height: 250px;'>" +
		"<div id='ribbon'>" +
			"<ul>" +
				"<li><a href='#C1Editor1Format'>Format</a></li>" +
			"</ul>" +
			"<div id='C1Editor1Format'>" +
				"<ul>" +
					"<li>" +
						"<div title='Font Name' class='wijmo-wijribbon-dropdownbutton'>" +
							"<button title='Font Name' name='fontname'>" +
								"Font Name" +
							"</button>" +
							"<ul>" +
								"<li>" +
									"<input type='radio' id='C1Editor1_ctl74' name='fontname' />" +
									"<label for='C1Editor1_ctl74' name='fn1' title='Arial'>Arial</label>" +
								"</li>" +
								"<li>" +
									"<input type='radio' id='C1Editor1_ctl76' name='fontname' />" +
									"<label for='C1Editor1_ctl76' name='fn2' title='Courier New'>Courier New</label>" +
								"</li>" +
							"</ul>" +
						"</div>" +
						"<div title='Font Size' class='wijmo-wijribbon-dropdownbutton'>" +
							"<button title='Font Size' name='fontsize'>" +
								"Font Size" +
							"</button>" +
							"<ul>" +
								"<li>" +
									"<input type='radio' id='C1Editor2_ctl116' name='fontsize' />" +
									"<label name='xx-large' for='C1Editor2_ctl116' title='VeryLarge'>VeryLarge</label>" +
								"</li>" +
								"<li>" +
									"<input type='radio' id='C1Editor2_ctl117' name='fontsize' />" +
									"<label name='40pt' for='C1Editor2_ctl117' title='40pt'>40pt</label>" +
								"</li>" +
							"</ul>" +
						"</div>" +
						"<div>" +
							"Font" +
						"</div>" +
					"</li>" +
				"</ul>" +
			"</div>" +
		"</div>" +
		"</div>").appendTo(document.body),
			editor = container.wijeditor({ text: "Hello World" }),
			doc = container.find("iframe")[0].contentWindow.document,
			ribbon = editor.data("wijmo-wijeditor").$ribbon.data("wijmo-wijribbon"),
			fontName = ribbon.dropdowns[0],
			fontSize = ribbon.dropdowns[1];
		
		container.find("iframe")[0].contentWindow.find("World");
		ribbon._showDropDownList(fontName.find("ul"), fontName.find("button"));
		fontName.find("ul").find("li:first").find("input").trigger("click");
		ribbon._showDropDownList(fontSize.find("ul"), fontSize.find("button"));
		fontSize.find("ul").find("li:first").find("input").trigger("click");
		editor.wijeditor("option", "editorMode", "code");
		ok(editor.wijeditor("option", "text").replace(/\s+/g, ''),
			'Hello<spanclass="wijmo-formatspan"style="font-size:xx-large"><fontface="Arial">World</font></span>');
		container.remove();
	});

	test("#149751", function () {
		var widgetDiv = $("<div></div>").appendTo(document.body),
			temp = "",
			text = $('<textarea id="edt" style="width: 756px; height: 320px;"></textarea>').appendTo(widgetDiv),
			editor = text.wijeditor({
				textChanged: function(e){
					if (e.originalEvent) {
						temp = "textChanged:" + String.fromCharCode(e.originalEvent.keyCode) +
							".editorMode:" + $(this).wijeditor("option", "editorMode");
					}
				}
			}),
			doc = widgetDiv.find("iframe")[0].contentWindow.document;
		$(doc).simulate("keyup", { keyCode: 65 });
		ok(temp === "textChanged:A.editorMode:wysiwyg");

		text.wijeditor("option", "editorMode", "code");
		$(doc).simulate("keyup", { keyCode: 66});
		ok(temp === "textChanged:B.editorMode:code");

		editor.remove();
		text.remove();
		widgetDiv.remove();
	});

	test("#149732", function () {
		var widgetDiv = $("<div></div>").appendTo(document.body),
			text = $('<textarea id="edt" style="width: 756px; height: 300px;"></textarea>').appendTo(widgetDiv),
			editor = text.wijeditor({ text: "Welcome to Wijmo Editor"}),
			doc = widgetDiv.find("iframe")[0].contentWindow.document;
		$(doc.body).simulate("contextmenu");
		ok($(".wijmo-wijmenu-list").text() === "CutCopyPaste");
		editor.remove();
		text.remove();
		widgetDiv.remove();
	});
})(jQuery);

//Function for case41758.
function SetSelectedCells(designWindow, doc, targetRow, sIdx, eIdx) {
	if (doc.selection) {
		var range = doc.body.createTextRange();
		range.moveToElementText(doc.getElementById('firstTR'));
		range.select();
	}
	else if (window.getSelection) {
		var selection = designWindow.getSelection();
		selection.removeAllRanges();
		var range;
		for (var i = sIdx; i <= eIdx; i++) {
			range = doc.createRange();
			range.selectNodeContents(targetRow.cells[i]);
			selection.addRange(range);
		}
	}
}