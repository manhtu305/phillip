/*globals test, ok, module, jQuery, createSimplePiechart*/
/*
 * wijpiechart_methods.js
 */
"use strict";
(function ($) {

	module("wijpiechart: methods");

	test("getSector", function () {
		var piechart = createSimplePiechart(),
			sector = piechart.wijpiechart('getSector', 1);
		
		ok(sector.attrs.fill === 'green', 'getSector:gets the second sector');
		piechart.remove();
	});

	test("getCanvas", function () {
		var piechart = createSimplePiechart(),
			canvas = piechart.wijpiechart('getCanvas');

		ok(canvas.circle !== null, 'getCanvas:gets the canvas for the pie chart');
		piechart.remove();
	});

}(jQuery));
