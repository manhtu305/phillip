/*globals ok, test, module, jQuery*/
/*
* wijpiechart_tickets.js
*/
(function ($) {
    module("wijpiechart:tickets");

    //32172
    test("32172:Labels are clipped if radius is set to null", function () {
        var bounds, radius, minRadius,
            piechart = $('<div id="wijpiechart" style="height:419px; width: 402px;"></div>').appendTo(document.body);
        piechart.wijpiechart({
            legend: { visible: false },
            seriesList: [{
                label: "MacBook Pro",
                legendEntry: true,
                data: 40.78,
                offset: 0
            }, {
                label: "iMacbook mac",
                legendEntry: true,
                data: 29.18,
                offset: 0
            }, {
                label: "MacBook",
                legendEntry: true,
                data: 20.25,
                offset: 0
            }, {
                label: "Mac Pro",
                legendEntry: true,
                data: 5.41,
                offset: 0
            }, {
                label: "Mac mini",
                legendEntry: true,
                data: 3.44,
                offset: 0
            }],
            labels: {
                style: {
                    "font-size": 11
                },
                formatter: function () {
                    return this.chartLabel;
                },
                connectorStyle: {
                    fill: "red",
                    "stroke-width": 2
                },
                position: "outside",
                offset: 10
            }
        });
        bounds = piechart.data("wijmo-wijpiechart").canvasBounds;
        radius = (bounds.endX - bounds.startX) / 2;
        minRadius = 402 / 2 - 25;
        ok(radius < minRadius, "Radius is adjusted to fit the display area.")
        piechart.remove();
    });

    test("#65118", function () {
        var piechart65118 = $('<div id="pie65118"></div>').appendTo(document.body),
            strokeWidth1, strokeWidth2;
        piechart65118.wijpiechart({
            radius: 80, // pie radius                  
            legend: {
                text: "May 2009 - May 2010",
            },
            seriesList: [{
                label: "DX11GPU & WIN7",
                legendEntry: true,
                data: 5.6
            }, {
                label: "DX10GPU & WIN7",
                legendEntry: true,
                data: 56.36
            }],
            seriesStyles: [{
                fill: "grey", "stroke-width": 0
            }, {
                fill: "yellow", "stroke-width": 4
            }],
            width: 419,
            height: 402
        });

        strokeWidth1 = piechart65118.find(".wijchart-legend-icon").eq(0).attr("stroke-width");
        strokeWidth2 = piechart65118.find(".wijchart-legend-icon").eq(1).attr("stroke-width");

        ok(strokeWidth1 === "0" && strokeWidth2 === "1", "\"stroke-width\" of lengend renders correctly. ")

        piechart65118.remove();
    });

    test("Label style recovered after set options again", function () {
        var pieChart = $("<div id='pie118384'></div>").appendTo("body");

        pieChart.wijpiechart({
            chartLabelStyle: {
                "fontSize": "50px"
            },
            seriesTransition: {
                duration: 100
            },
            radius: 80, // pie radius                  
            seriesList: [{
                label: "DX11GPU & WIN7",
                legendEntry: true,
                data: 5.6
            }, {
                label: "DX10GPU & WIN7",
                legendEntry: true,
                data: 56.36
            }],
            width: 419,
            height: 402
        });
        pieChart.wijpiechart("option", "radius", 100);
        setTimeout(function () {
            ok(pieChart.find(".wijpiechart-label").attr("font-size") === "50px", "Label size doesn't change !");
            start();
            pieChart.remove();
        }, 200);
        stop();
    });
}(jQuery));
