/*globals ok, test, module, jQuery, createSimplePiechart*/
/*
* wijpiechart_options.js
*/
"use strict";
(function ($) {
	module("wijpiechart:options");
	
	test("seriesListEmpty", function() {
		var piechart = createSimplePiechart(),
			seriesList = [];
		seriesList.push({ label:'abc', data: 5 });
		seriesList.push({ });
		piechart.wijpiechart('option', 'seriesList', seriesList);
		ok(true, 'ok.');
		piechart.remove();
	});

	test("data has 0 value", function () {
		var piechart = createPiechart({
			hint: { enable: false },
			animation: { enabled: false },
			seriesTransition: { enabled: false },
			seriesList: [{
				label: "Firefox",
				legendEntry: true,
				data: 0
			}, {
				label: "IE",
				legendEntry: true,
				data: 30
			}, {
				label: "Chrome",
				legendEntry: true,
				data: 20
			}, {
				label: "Safari",
				legendEntry: true,
				data: 15
			}, {
				label: "Others",
				legendEntry: true,
				data: 5
			}]
		});
		ok(true, 'ok when data has 0 value');
		piechart.remove();
	});

	test("radius", function () {
		var piechart = createSimplePiechart(),
			sector;

		piechart.wijpiechart('option', 'radius', 50);
		sector = piechart.wijpiechart("getSector", 1);

		ok(sector.radius === 50, 'set radius to 50.');
		piechart.remove();
	});

	test("innerRadius", function () {
		var piechart = createSimplePiechart(),
			sector;

		piechart.wijpiechart('option', 'innerRadius', 20);
		sector = piechart.wijpiechart("getSector", 1);

		ok(sector.innerRadius === 20, 'set innerRadius to 20.');
		piechart.remove();
	});

	test("chartLabelFormatter && chartLabelFormatString && label.format", function () {
	    var piechart = createPiechart({
	        seriesList: [{
	            label: "7",
	            legendEntry: true,
	            data: 5.6
	        }, {
	            label: "8",
	            legendEntry: true,
	            data: 56.36
	        }],
	        showChartLabels: true,
	        animation: {
	            enabled: false
	        }
	    }), chartLabel0;

	    chartLabel0 = piechart.data("wijmo-wijpiechart").chartElement.data("fields").chartElements.labels[0].attr("text");
	    ok(chartLabel0 === "7", "the first chart element label is '7'");

	    piechart.wijpiechart("option", "chartLabelFormatter", function () {
	        return "mycustomzied:" + this.value;
	    });
	    chartLabel0 = piechart.data("wijmo-wijpiechart").chartElement.data("fields").chartElements.labels[0].attr("text");
	    ok(chartLabel0 === "mycustomzied:5.6", "the first chart element label is updated according to chartLabelFormatter.");

	    piechart.remove();
	});
}(jQuery));
