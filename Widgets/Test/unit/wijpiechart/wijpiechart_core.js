/*globals test, ok, module, jQuery, document, $*/
/*
* wijpiechart_core.js create & destroy
*/
"use strict";
function createPiechart(o) {
	return $('<div id="piechart" style = "width:400px;height:200px"></div>')
			.appendTo(document.body).wijpiechart(o);
}

var simpleOptions = {
	hint: {
		enable: false
	},
	animation: {
		enabled: false
	},
	seriesTransition: {
		enabled: false
	},
	seriesList: [{
		label: "Firefox",
		legendEntry: true,
		data: 25
	}, {
		label: "IE",
		legendEntry: true,
		data: 35
	}, {
		label: "Chrome",
		legendEntry: true,
		data: 20
	}, {
		label: "Safari",
		legendEntry: true,
		data: 15
	}, {
		label: "Others",
		legendEntry: true,
		data: 5
	}],			
	seriesStyles: [{
			fill: "red", 
			stroke: "none"
		}, {
			fill: "green", 
			stroke: "none"
		}, {
			fill: "blue", 
			stroke: "none"
		}, {
			fill: "lime", 
			stroke: "none"
		}, {
			fill: "purple", 
			stroke: "none"
		}]
	};
			
function createSimplePiechart() {
	return createPiechart(simpleOptions);
}

(function ($) {

	module("wijpiechart: core");

	test("create and destroy", function () {
		var piechart = createPiechart();

		ok(piechart.hasClass('ui-widget wijmo-wijpiechart'), 
				'create:element class created.');
		
		piechart.wijpiechart('destroy');
		ok(!piechart.hasClass('ui-widget wijmo-wijpiechart'), 
				'destroy:element class destroy.');
		
		piechart.remove();
	});
}(jQuery));

