/*globals test, start, stop, ok, module, jQuery, simpleOptions, 
	createSimplePiechart, createPiechart*/
/*
* wijpiechart_events.js
*/
"use strict";
(function ($) {
	var piechart,
		newSeriesList = [{
			label: "Firefox",
			legendEntry: true,
			data: 40.0
		}, {
			label: "IE",
			legendEntry: true,
			data: 26.8
		}, {
			label: "Chrome",
			legendEntry: true,
			data: 12.8
		}, {
			label: "Safari",
			legendEntry: true,
			data: 8.5
		}, {
			label: "Opera",
			legendEntry: true,
			data: 6.2
		}, {
			label: "Others",
			legendEntry: true,
			data: 0.7
		}];

	module("wijpiechart: events");

	test("mousedown", function () {
		piechart = createSimplePiechart();
		piechart.wijpiechart({
			mouseDown: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 1, 'Press the mouse down on the first sector.');
					piechart.remove();
				}
			}
		});
		stop();
		$(piechart.wijpiechart("getSector", 1).node).trigger('mousedown');
	});

	test("mouseup", function () {
		piechart = createSimplePiechart();
		piechart.wijpiechart({
			mouseUp: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 1, 'Release the mouse on the first sector.');
					piechart.remove();
				}
			}
		});
		stop();
		$(piechart.wijpiechart("getSector", 1).node).trigger('mouseup');
	});

	test("mouseover", function () {
		piechart = createSimplePiechart();
		piechart.wijpiechart({
			mouseOver: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 1, 'Hover the first sector.');
					piechart.remove();
				}
			}
		});
		stop();
		$(piechart.wijpiechart("getSector", 1).node).trigger('mouseover');
	});

	test("mouseout", function () {
		piechart = createSimplePiechart();
		piechart.wijpiechart({
			mouseOut: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 1, 'Move the mouse out of the first sector.');
					piechart.remove();
				}
			}
		});
		stop();
		$(piechart.wijpiechart("getSector", 1).node).trigger('mouseout');
	});

	test("mousemove", function () {
		piechart = createSimplePiechart();
		piechart.wijpiechart({
			mouseMove: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 1, 'Move the mouse on the first sector.');
					piechart.remove();
				}
			}
		});
		stop();
		$(piechart.wijpiechart("getSector", 1).node).trigger('mousemove');
	});

	test("click", function () {
		piechart = createSimplePiechart();
		piechart.wijpiechart({
			click: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 1, 'Click on the first sector.');
					piechart.remove();
				}
			}
		});
		stop();
		$(piechart.wijpiechart("getSector", 1).node).trigger('click');
	});

	test("beforeserieschange", function () {
		piechart = createSimplePiechart();
		var oldSeriesList = piechart.wijpiechart("option", "seriesList");
		piechart.wijpiechart({
			beforeSeriesChange: function (e, seriesObj) {
				if (seriesObj !== null) {
					start();
					var seriesList = piechart.wijpiechart("option", "seriesList");
					ok(seriesObj.oldSeriesList === oldSeriesList, 
						'get the correct old seriesList by seriesObj.oldSeriesList.');
					ok(seriesObj.newSeriesList === newSeriesList, 
						'get the correct new seriesList by seriesObj.newSeriesList.');
					ok(seriesList === oldSeriesList, 
						"currently seriesList hasn't been changed.");
					piechart.remove();
				}
			}
		});
		stop();
		piechart.wijpiechart("option", "seriesList", newSeriesList);
	});

	test("serieschanged", function () {
		piechart = createSimplePiechart();
		piechart.wijpiechart({
			seriesChanged: function (e, seriesObj) {
				if (seriesObj !== null) {
					start();
					var seriesList = piechart.wijpiechart("option", "seriesList");
					ok(seriesList === newSeriesList, 
						"currently seriesList has been changed.");
					piechart.remove();
				}
			}
		});
		stop();
		piechart.wijpiechart("option", "seriesList", newSeriesList);
	});

	test("beforepaint, paint", function () {
		var options = $.extend({
				beforePaint: function (e) {
					ok(true, "The beforepaint event is invoked.");
					return false;
				},
				painted: function (e) {
					ok(false, "The painted event of pie chart1 is invoked.");
				}
			}, true, simpleOptions),
			options2, piechart2, sector;
		piechart = createPiechart(options);

		sector = piechart.wijpiechart("getSector", 1);
			
		if (sector === null) {
			ok(true, "The pie chart1 isn't painted because of the cancellation.");
		}
		else {
			ok(false, "The pie chart1 shouldn't be painted because of the cancellation.");
		}
		piechart.remove();

		options2 = $.extend({
			beforePaint: function (e) {
				if (e !== null) {
					ok(true, "The beforepaint event is invoked.");
				}
			},
			painted: function (e) {
				ok(true, "The painted event of pie chart2 is invoked.");
			}
		}, true, simpleOptions);
		piechart2 = createPiechart(options2);
		sector = piechart2.wijpiechart("getSector", 1);
		ok(sector !== null, 'The pie chart2 is painted.');
		piechart2.remove();
	});
}(jQuery));
