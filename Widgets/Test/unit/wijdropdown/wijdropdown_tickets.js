/*
 * wijdropdown_tickets.js
 */
(function ($) {

    module("wijdropdown: tickets");

    test("#41710: Check the title attribute of the original select element", function () {
        var $widget = $("<select title='Test Title' name='testdropdown' id='testdropdown'>" +
			"<option value='Item1'>Item1</option>" +
			"<option value='Item2'>Item2</option>" +
			"</select>").appendTo("body").wijdropdown(),
			originalSelect = $(".ui-helper-hidden select");

        ok(originalSelect.attr("title"), "The title attribute of the original select element isn't removed");
        ok(originalSelect.attr("title") === "Test Title", "The title attribute of the original select element isn't changed either.");

        $widget.remove();
    });

    test("#42874: Disabled option CSS and click event test", function () {
        var $widget = $("<select id='testdropdown'>" +
			"<optgroup label='Manual Testing'>" +
                "<option id='disabled1' disabled='disabled'>Disabled1</option>" +
                "<option>Sophia</option>" +
                "<option disabled='disabled'>Disabled2</option>" +
            "</optgroup>" +
            "<optgroup label='Automated Testing'>" +
                "<option >Susan</option>" +
                "<option disabled='disabled'>Disabled4</option>" +
            "</optgroup>" +
			"</select>").appendTo("body").wijdropdown({ showingAnimation: null, hidingAnimation: null }),
	        dropdown = $widget.closest(".wijmo-wijdropdown"),
            dropdownTrigger = dropdown.find(".wijmo-dropdown-trigger"),
            itemList = dropdown.find("ul>li.wijmo-dropdown-item"),
	        disabledCSSVaild = itemList.eq(0).hasClass("ui-state-disabled") &&
	                        itemList.eq(2).hasClass("ui-state-disabled"),
            enableCSSVaild = !itemList.eq(1).hasClass("ui-state-disabled");

        ok(disabledCSSVaild && enableCSSVaild, "Disabled and enable items in <select> can be rendered in wijdropdown.");
        dropdownTrigger.simulate("click");
        itemList.eq(3).simulate("mousemove");
        itemList.eq(3).simulate("click");
        dropdownTrigger.simulate("click");

        itemList.eq(4).simulate("mousemove");
        ok(dropdown.find(":selected").html() === "Susan",
            "Hover item did not change");
        itemList.eq(4).simulate("click");
        ok(dropdown.find(".wijmo-dropdown").is(":visible"),
            "Drop down did not hidden");
        $widget.remove();
    })

    if (!$.browser.webkit) {
        test("#42874: Disabled option KEY DOWN test", function () {
            var $widget = $("<select id='testdropdown'>" +
                "<optgroup label='Manual Testing'>" +
                    "<option disabled='disabled'>Disabled1</option>" +
                    "<option selected='selected'>Sophia</option>" +
                    "<option disabled='disabled'>Disabled2</option>" +
                    "<option>June</option>" +
                    "<option disabled='disabled'>Disabled3</option>" +
                "</optgroup>" +
                "<optgroup label='Automated Testing'>" +
                    "<option disabled='disabled'>Disabled4</option>" +
                    "<option>Jennifer</option>" +
                    "<option disabled='disabled'>Disabled5</option>" +
                    "<option disabled='disabled'>Disabled6</option>" +
                    "<option disabled='disabled'>Disabled7</option>" +
                "</optgroup>" +
                "</select>").appendTo("body").wijdropdown({ showingAnimation: null, hidingAnimation: null }),

                dropdown = $widget.closest(".wijmo-wijdropdown"),
                labelWrap = dropdown.find("a");
            dropdown.find(".wijmo-dropdown-trigger").simulate("click");

            labelWrap.simulate("keydown", { keyCode: $.ui.keyCode.DOWN });
            ok(dropdown.find(":selected").html() === "June", "Ignore disabled items");
            labelWrap.simulate("keydown", { keyCode: $.ui.keyCode.DOWN });
            ok(dropdown.find(":selected").html() === "Jennifer", "Ignore optiongroup and disabled items");
            labelWrap.simulate("keydown", { keyCode: $.ui.keyCode.DOWN });
            ok(dropdown.find(":selected").html() === "Sophia", "Ignore optiongroup and disabled items and jump to top");

            labelWrap.simulate("keydown", { keyCode: $.ui.keyCode.PAGE_DOWN });
            ok(dropdown.find(":selected").html() === "Jennifer",
                "PAGE_DOWN located on enabled item");
            labelWrap.simulate("keydown", { keyCode: $.ui.keyCode.PAGE_UP });
            ok(dropdown.find(":selected").html() === "Sophia",
                "PAGE_UP located on enabled item");

            $widget.remove();
        })
    }

    test("#42874: Disabled option group test", function () {
        var $widget = $("<select id='testdropdown'>" +
			"<optgroup label='Manual Testing'>" +
                "<option disabled='disabled'>Disabled1</option>" +
                "<option selected='selected'>Sophia</option>" +
                "<option disabled='disabled'>Disabled2</option>" +
            "</optgroup>" +
            "<optgroup label='Automated Testing'>" +
                "<option disabled='disabled'>Disabled4</option>" +
                "<option>Jennifer</option>" +
            "</optgroup>" +
            "<optgroup label='Disabled group' disabled='disabled'>" +
                "<option disabled='disabled'>Disabled5</option>" +
                "<option>Susan</option>" +
            "</optgroup>" +
			"</select>").appendTo("body").wijdropdown({ showingAnimation: null, hidingAnimation: null }),
	        dropdown = $widget.closest(".wijmo-wijdropdown"),
            labelWrap = dropdown.find("a"),
            disabledGroup = dropdown.find(".wijmo-dropdown-optgroup").eq(2),
            allInnerItemsDisable = disabledGroup.hasClass("ui-state-disabled");
        disabledGroup.find("ul>li").each(function () {
            allInnerItemsDisable = allInnerItemsDisable && $(this).hasClass("ui-state-disabled");
        })
        ok(allInnerItemsDisable, "Disabled group and inside items are disabled");
        $widget.remove();
    })

    test("#42874: Test for disabled options in select tag without optgroup.", function () {
        var $widget = $("<select id='testdropdown'>" +
                "<option disabled='disabled'>Disabled1</option>" +
                "<option selected='selected'>Sophia</option>" +
                "<option disabled='disabled'>Disabled2</option>" +
                "<option disabled='disabled'>Disabled3</option>" +
                "<option>Jennifer</option>" +
                "<option disabled='disabled'>Disabled4</option>" +
                "<option>Susan</option>" +
            "</select>").appendTo("body").wijdropdown({ showingAnimation: null, hidingAnimation: null }),
            dropdown = $widget.closest(".wijmo-wijdropdown"),
            labelWrap = dropdown.find("a"),
            itemList = dropdown.find("ul>li.wijmo-dropdown-item"),
            disabledCSSVaild = itemList.eq(0).hasClass("ui-state-disabled") &&
                            itemList.eq(2).hasClass("ui-state-disabled"),
            enableCSSVaild = !itemList.eq(1).hasClass("ui-state-disabled");
        // ***** CSS test ***** 
        ok(disabledCSSVaild && enableCSSVaild, "Disabled and enable items in <select> can be rendered in wijdropdown.");
        dropdown.find(".wijmo-dropdown-trigger").simulate("click");
        // ***** Keydown test *****
        if (!$.browser.webkit) {
            labelWrap.simulate("keydown", { keyCode: $.ui.keyCode.DOWN });
            ok(dropdown.find(":selected").html() === "Jennifer", "Ignore disabled items");
            labelWrap.simulate("keydown", { keyCode: $.ui.keyCode.UP });
            ok(dropdown.find(":selected").html() === "Sophia", "Ignore disabled items");
            labelWrap.simulate("keydown", { keyCode: $.ui.keyCode.PAGE_DOWN });
            ok(dropdown.find(":selected").html() === "Susan", "PAGE_DOWN located on enabled item");
            labelWrap.simulate("keydown", { keyCode: $.ui.keyCode.PAGE_UP });
            ok(dropdown.find(":selected").html() === "Sophia", "PAGE_UP located on enabled item");
        }
        // ***** mousemove and click event test *****
        itemList.eq(5).simulate("mousemove");
        ok(dropdown.find("ul>li.ui-state-hover").find("span").html() === "Sophia",
            "Hover item did not change");
        itemList.eq(5).simulate("click");
        ok(dropdown.find(".wijmo-dropdown").is(":visible"),
            "Drop down did not hidden");
        $widget.remove();
    })

    test("#94012: Add the active styles for container", function () {
        var con94012 = $("<div class='wrapper'></div>").appendTo("body"),
            $widget = $("<select id='testdropdown' class='selectClass'>" +
                "<option>Jennifer</option>" +
                "<option>Susan</option>" +
            "</select>").appendTo(con94012).wijdropdown(),
            label = con94012.find("label.wijmo-dropdown-label"),
            container = con94012.find(".wijmo-wijdropdown.ui-widget");

        label.simulate("mouseover");

        ok(container.hasClass("ui-state-hover"), "Styles has been added into the container");
        $widget.remove();
        con94012.remove();
    });

    test("#94012: Width isn't updated when changing font-size", function () {
        var con94012 = $("<div></div>").appendTo("body"),
            $widget = $("<select id='testdropdown' class='selectClass'>" +
                "<option>Jennifer</option>" +
                "<option>Susan</option>" +
            "</select>").appendTo(con94012).wijdropdown({
                showingAnimation: null,
                hidingAnimation: null
            }),
                label = con94012.find("label.wijmo-dropdown-label"),
                containerWidth = con94012.find(".wijmo-wijdropdown.ui-widget").width(),
                listWidth;
        label.simulate("click");
        listWidth = con94012.find(".wijmo-dropdown.wijmo-wijsuperpanel").width();

        con94012.css('font-size', 'large');
        $widget.wijdropdown("refresh");

        ok(con94012.find(".wijmo-wijdropdown.ui-widget").width() > containerWidth &&
            con94012.find(".wijmo-wijdropdown.ui-widget").width() === con94012.find(".wijmo-dropdown.wijmo-wijsuperpanel").width(),
            "Width of dropdown container and dropdown list has updated.");
        $widget.remove();
        con94012.remove();
    });

    test("#108766", function () {
        var $widget = $("<select id='dropdown' disabled><option>1</option></select>").appendTo(document.body).wijdropdown()
            , clicked = false,
            dropdown = $widget.closest(".wijmo-wijdropdown");
        dropdown.find(".wijmo-dropdown-label").closest("a").click(function (e) {
            clicked = true;
        });
        ok(!clicked);
        $widget.remove();
    });
})(jQuery);
