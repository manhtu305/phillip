﻿(function ($) {
	module("wijtabs:tickets");
	if ($.browser.msie) {
		test("when scrollable is true, the tabs's header line wrapped in IE.", function () {
			var str = "<div style='width:500px;'><ul><li><a href='#C1Tabs1_Tab1'>Tab1</a></li><li><a href='#C1Tabs1_Tab2'>Tab2</a></li>" +
                "<li><a href='#C1Tabs1_Tab3'>Tab3</a></li><li><a href='#C1Tabs1_Tab4'>Tab4</a></li><li><a href='#C1Tabs1_Tab5'>Tab5</a></li>" +
                "<li><a href='#C1Tabs1_Tab6'>Tab6</a></li><li><a href='#C1Tabs1_Tab7'>Tab7</a></li><li><a href='#C1Tabs1_Tab8'>Tab8</a></li>" +
                "<li><a href='#C1Tabs1_Tab9'>Tab9</a></li><li><a href='#C1Tabs1_Tab10'>Tab10</a></li><li><a href='#C1Tabs1_Tab11'>Tab11</a></li>" +
                "<li><a href='#C1Tabs1_Tab12'>Tab12</a></li><li><a href='#C1Tabs1_Tab13'>Tab13</a></li>" +
                "<li><a href='#C1Tabs1_Tab14'>Tab14</a></li><li><a href='#C1Tabs1_Tab15'>Tab15</a></li></ul><div id='C1Tabs1_Tab1'>" +
                "</div><div id='C1Tabs1_Tab2'></div><div id='C1Tabs1_Tab3'></div><div id='C1Tabs1_Tab4'></div><div id='C1Tabs1_Tab5'>" +
                "</div><div id='C1Tabs1_Tab6'></div><div id='C1Tabs1_Tab7'></div><div id='C1Tabs1_Tab8'></div><div id='C1Tabs1_Tab9'>" +
                "</div><div id='C1Tabs1_Tab10'></div><div id='C1Tabs1_Tab11'></div><div id='C1Tabs1_Tab12'></div><div id='C1Tabs1_Tab13'>" +
                "</div><div id='C1Tabs1_Tab14'></div><div id='C1Tabs1_Tab15'></div></div>",
                tabs = $(str).appendTo("body").wijtabs({ scrollable: true }),
                container = tabs.find(".ui-tabs-nav"),
                li = container.find("li:first");

			ok(container.height() < li.height() * 2, "The header is not line wrapped.");
			tabs.remove();
		})

		test("when scrollable is false, the width of tab header is bigger after adding item in tab header.", function () {
			var str = "<div style='width:500px;'><ul><li><a href='#C1Tabs1_Tab1'>Tab1</a></li><li><a href='#C1Tabs1_Tab2'>Tab2</a></li></ul>" +
                "<div id='C1Tabs1_Tab1'></div><div id='C1Tabs1_Tab2'></div></div>",
                tabs = $(str).appendTo("body").wijtabs({ scrollable: false }),
                container = tabs.find(".ui-tabs-nav"),
                li = container.find("li:first"),
                originalLiWidth = $(li).width(),
                changedLiWidth;

			container.find("li").each(function (index, e) {
				var cnt = index + 1, originalWidth = $(this).width();
				$(this).prepend("<img src='Images/TabPage" + cnt + ".png' alt='Image' style='margin-left:5px; margin-top:10px; width:20px' />");
				$(this).find("a").css("float", "right");
			});

			changedLiWidth = $(li).width();

			ok(changedLiWidth > originalLiWidth, "The width of tab header is bigger after adding item in tab header");
			tabs.remove();
		});

		test("#41015", function () {
			var tabs = $('<div id="tabs" style="width:600px;"><ul><li><a href="#tabs-1">Tab -1</a> <span class="ui-icon ui-icon-close">Remove Tab</span></li>' +
                    '<li><a href="#tabs-2">Tab -2</a> <span class="ui-icon ui-icon-close">Remove Tab</span></li> </ul>' +
                '<div id="tabs-1"> This is Tabs 1. </div><div id="tabs-2"> Tabs 2 </div></div>').appendTo("body");
			tabs.wijtabs({ scrollable: true });
			container = tabs.find(".ui-tabs-nav"),
            li = container.find("li:first");
			ok(container.height() < li.height() * 2, "The header is not line wrapped.");
			tabs.remove();
		});

		test("#41786", function () {
			var dialog = $('<input id="btn2" type="button" value="Show Dialog"  />' +
            '<div id="dialog2" title="Dialog 2">' +
                '<div id="tabs" style="width: 400px">' +
                    '<ul style="margin-right: 100px">' +
                        '<li><a href="#tabs-1">tab1</a></li>' +
                        '<li><a href="#tabs-2">tab2</a></li>' +
                        '<li><a href="#tabs-3">tab3</a></li>' +
                    '</ul>' +
                    '<div id="tabs-1">' +
                     '   P1' +
                    '</div>' +
                    '<div id="tabs-2">' +
                     '   P4' +
                   ' </div>' +
                    '<div id="tabs-3">' +
                     '   F4' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>').appendTo("body"),
                li;
			$("#dialog2").wijdialog({ autoOpen: false, modal: true, width: "450px" });
			$('#dialog2').wijdialog('open');
			$("#tabs").wijtabs({});
			container = dialog.find(".ui-tabs-nav"),
            link3 = container.find("a:last");
			link3.focus();
			link3.simulate("click");
			$("#dialog2").data("wijmo-wijdialog")._focusTabbable();

			window.setTimeout(function () {
				ok(!container.find("li:first").hasClass("ui-state-focus"), "The second tab can't tabable.");
				start();
				dialog.remove();
			}, 200);
			stop();
		});
	}

	test("#27848", function () {
		var str = "<div style='width:500px;'><ul><li><a href='#C1Tabs1_Tab1'>Tab1</a></li><li><a href='#C1Tabs1_Tab2'>Tab2</a></li></ul>" +
                "<div id='C1Tabs1_Tab1'></div><div id='C1Tabs1_Tab2'></div></div>",
            tabs = $(str).appendTo("body").wijtabs(),
            list = tabs.children("ul");

		ok(!list.hasClass("ui-sortable"), "The sortable has not been applied to the list of the wijtab yet.");

		tabs.wijtabs({ sortable: true });
		ok(list.hasClass("ui-sortable"), "The sortable has been applied to the list of the wijtab.");
		ok(!list.sortable("option", "disabled"), "The sortable for the list of the wijtab is enable.");

		tabs.wijtabs({ sortable: false });
		ok(list.sortable("option", "disabled"), "The sortable for the list of the wijtab is disable.");

		tabs.remove();
	});

	test("#56300", function () {
		var str = "<div id='Tab1' style='width:200px'>" +
            "<ul><li><a href='#Item1'>C1BarChart</a></li><li><a href='#Item2'>C1CandlestickChart</a></li>" +
                    "<li><a href='#Item3'>C1InputNumber</a></li></ul>" +
            "<div id='Item1'>C1B</div><div id='Item2'>C1C</div><div id='Item3'>C1I</div></div>",
            tabs = $(str).appendTo("body").wijtabs(),
            list = tabs.children("ul"),
            tabItem = list.find("li>a").eq(1),
            originalWidth = list.width(), result;

		tabs.wijtabs("option", "scrollable", true);
		tabs.wijtabs("option", "scrollable", false);

		result = list.width();
		ok(result === originalWidth, "List in wijtabs has recovered its width.");
		tabItem.simulate("click");
		ok(tabs.wijtabs('option', 'selected') === 1, "Wijtabs has selected second item without error being thrown !");

		tabs.remove();
	});

	test("#56288", function () {
		var str = "<div style='width:500px;'><ul><li><a href='#C1Tabs1_Tab1'>Tab1</a></li><li><a href='#C1Tabs1_Tab2'>Tab2</a></li><li><a href='#C1Tabs1_Tab3'>Tab3</a></li></ul>" +
                "<div id='C1Tabs1_Tab1'></div><div id='C1Tabs1_Tab2'></div><div id='C1Tabs1_Tab3'></div></div>",
            tabs = $(str).appendTo("body").wijtabs({
            	sortable: true
            }),
            list = tabs.children("ul"),
            lis = list.children("li"),
            panel = $("#C1Tabs1_Tab1", tabs);

		ok(tabs.children("div").index(panel) === 0, "panel's index is 0.");
		$(lis[0]).simulate("mouseover").simulate("drag", { dx: 200, dy: 0 });
		ok(tabs.children("div").index(panel) !== 0, "panel is moved to specified position.");
		tabs.wijtabs("remove", 1);
		try {
			tabs.wijtabs("remove", 1);
			ok(true, "exception is not thrown when removing the tab item after sorting.");
		} catch (e) {
			ok(false, "exception is thrown when removing the tab item after sorting.")
		} finally {
			tabs.remove();
		}
	});

	test("#61423", function () {
		var str = "<div id='Tabs' style='width:300px'>" +
            "<ul><li><a href='#Item1'>P1</a></li><li><a href='#Item2'>P2</a></li><li><a href='#Item3'>P3</a></li></ul>" +
            "<div id='Item1'>C1B</div>" +
            "<div id='Item2'>C1C</div>" +
            "<div id='Item3'><div id='innerTabs' style='width:200px'>" +
            "<ul><li><a href='#InnerItem1'>InP1</a></li><li><a href='#InnerItem2'>InP2</a></li></ul>" +
            "<div id='InnerItem1'>Inner1</div>" +
            "<div id='InnerItem2'>Inner2</div>" +
            "</div></div></div>",
            tabs = $(str).appendTo("body").wijtabs({ sortable: true }),
            lis = tabs.children("ul").children("li"),
            innerTabs = $("#innerTabs").wijtabs(), result = false;

		$(lis[0]).simulate("mouseover").simulate("drag", { dx: 200, dy: 0 });
		$(lis[2]).find("a").simulate("click");
		result = innerTabs.children("ul").is(":visible");
		ok(result, "Inner tabs are visible !");
		innerTabs.remove();
		tabs.remove();
	});

	test("#65295", function () {
		var str = "<div id='Tabs' style='width:300px'>" +
            "<ul><li><a href='#Item1'>P1</a></li><li><a href='#Item2'>P2</a></li><li><a href='#Item3'>P3</a></li></ul>" +
            "<div id='Item1'>PANEL 1</div>" +
            "<div id='Item2'>PANEL 2</div>" +
            "<div id='Item3'>PANEL 3</div></div>",
            tabs = $(str).appendTo("body").wijtabs({ sortable: true, alignment: "left" }),
            lis = tabs.children("ul").children("li"),
            li_0 = $(lis[0]), li_2 = $(lis[2]),
            dY, result = false;
		dY = li_2.offset().top - li_0.offset().top + li_0.height() / 2;
		li_0.simulate("mouseover").simulate("drag", { dx: 0, dy: dY });
		li_2.find("a").simulate("click");

		result = !$("#Item1").is(":visible") && !li_0.hasClass("ui-tabs-active");
		ok(result, "Panels can be hidden after sorting !");
		tabs.remove();
	});

	test("#67429", function () {
		var str = "<div id='Tabs' style='width:180px'>" +
            "<ul><li><a href='#Item1'>P1</a></li><li><a href='#Item2'>P2</a></li><li><a href='#Item3'>P3</a></li>" +
            "<li><a href='#Item4'>P4</a></li><li><a href='#Item5'>P5</a></li><li><a href='#Item6'>P6</a></li></ul>" +
            "<div id='Item1'>PANEL 1</div>" +
            "<div id='Item2'>PANEL 2</div>" +
            "<div id='Item3'>PANEL 3</div>" +
            "<div id='Item3'>PANEL 4</div>" +
            "<div id='Item3'>PANEL 5</div>" +
            "<div id='Item4'>PANEL 6</div></div>",
            tabs = $(str).appendTo("body").wijtabs({ scrollable: true }),
            scrollWrap, secondLi;

		tabs.wijtabs({ collapsible: true });
		scrollWrap = tabs.children(".scrollWrap");
		ok(scrollWrap, "The scrollWrap won't be removed after updating the options of wijtabs");
		secondLi = scrollWrap.find(".wijmo-wijsuperpanel-statecontainer .wijmo-wijsuperpanel-templateouterwrapper ul").children("li")[1];
		$(secondLi).children("a").simulate("click");
		ok(tabs.children("#Item2").is(":visible"), "The tab content changes to second tab without errors.");

		tabs.remove();
	});

	test("#67749", function () {
		var str = "<div id='Tabs' style='width:300px'>" +
            "<ul><li><a href='#Item1'>P1</a></li><li><a href='#Item2'>P2</a></li><li><a href='#Item3'>P3</a></li></ul>" +
            "<div id='Item1'>PANEL 1</div>" +
            "<div id='Item2'>PANEL 2</div>" +
            "<div id='Item3'>PANEL 3</div></div>",
            tabs = $(str).appendTo("body").wijtabs({ collapsible: true, alignment: "left" }),
            list = tabs.children("ul"),
            firstTab = list.children("li:first").children("a");

		firstTab.simulate("click");
		tabs.wijtabs({ alignment: "top" });

		ok(Math.floor(tabs.width()) === 300, "The width of the wijtabs container was reset to original setting after changing the alignment option.");
		tabs.remove();
	});

	test("#67470", function () {
		var str = "<div id='Tabs67470' style='width:700px; height: 200px'>" +
            "<ul>" + "<li><a href='#Item1'>P1</a></li><li><a href='#Item2'>P2</a></li><li><a href='#Item3'>P3</a></li>" +
                     "<li><a href='#Item4'>P4</a></li><li><a href='#Item5'>P5</a></li><li><a href='#Item6'>P6</a></li>" +
                     "<li><a href='#Item7'>P7</a></li><li><a href='#Item8'>P8</a></li><li><a href='#Item9'>P9</a></li>" + "</ul>" +
            "<div id='Item1'>PANEL 1</div>" + "<div id='Item2'>PANEL 2</div>" + "<div id='Item3'>PANEL 3</div>" +
            "<div id='Item4'>PANEL 4</div>" + "<div id='Item5'>PANEL 5</div>" + "<div id='Item6'>PANEL 6</div>" +
            "<div id='Item7'>PANEL 7</div>" + "<div id='Item8'>PANEL 8</div>" + "<div id='Item9'>PANEL 9</div>" +
            "</div>",
            tabs67470 = $(str).appendTo("body").wijtabs({
            	scrollable: true,
            	alignment: "right"
            }),
            hoverElement = $(tabs67470.data("wijmo-wijtabs").lis[4]),
            scrollWrap = tabs67470.data("wijmo-wijtabs").scrollWrap, superPanel, offset;

		ok(scrollWrap, "Created scroll wrapper !");

		superPanel = scrollWrap.data("wijmo-wijsuperpanel");
		ok(superPanel, "Created the superpanel !");

		ok(scrollWrap.outerHeight() === 200, "Height of scroll is correct !");

		offset = hoverElement.offset();
		hoverElement.simulate("mousemove", { clientX: offset.left + 10, clientY: offset.top + 50 });

		setTimeout(function () {
			ok(superPanel.options.vScroller.scrollValue > 0, "ScrollWrap content moved after hover in the bottom element");
			tabs67470.remove();
			start();
		}, 1000);

		stop();
	});

	test("#83215", function () {
		var $widget = createTabs({
			scrollable: true,
			alignment: "top",
			collapsible: true
		}),
            width = $widget.find("ul").width(),
            top = $widget.find(".ui-tabs-panel").position().top;

		$widget.wijtabs("option", "sortable", true);
		$widget.wijtabs("option", "alignment", "left");
		ok($widget.find("ul").width() < width, "width is correct.");
		ok($widget.find(".ui-tabs-panel").position().top < top, "panels do not show below.");
		$widget.wijtabs("option", "alignment", "bottom");
		ok($widget.find("ul").width() === width, "width is correct.");
		$widget.remove();
	});

	test("#119721+120020", function () {
		var $widget = createTabs({
			hideOption: { blind: true, fade: true, duration: 200 },
			showOption: { blind: true, fade: true, duration: 200 }
		}), visibleDivList;

		function myAssertion(id) {
			$widget.wijtabs('select', id);
			setTimeout(function () {
				id++;
				visibleDivList = $widget.children("div:visible");
				ok(visibleDivList.length === 1 && visibleDivList[0].id === "tabs-" + id, "tab-" + id + " displayed..");

				start();

				if (id === 3) {
					$widget.remove();
				} else {

					myAssertion(id);
				}

			}, 300);
			stop();
		}

		myAssertion(0);
	 

	});

	test("#105972", function () {
		var str = "<div style='width:500px;'><ul>" +
				"<li><a href='#C1Tabs1_Tab1'>Tab1</a></li><li><a href='#C1Tabs1_Tab2'>Tab2</a></li>" +
				"<li><a href='#C1Tabs1_Tab3'>Tab3</a></li><li><a href='#C1Tabs1_Tab4'>Tab4</a></li>" +
				"<li><a href='#C1Tabs1_Tab5'>Tab5</a></li><li><a href='#C1Tabs1_Tab6'>Tab6</a></li>" +
				"<li><a href='#C1Tabs1_Tab7'>Tab7</a></li><li><a href='#C1Tabs1_Tab8'>Tab8</a></li>" +
				"<li><a href='#C1Tabs1_Tab9'>Tab9</a></li><li><a href='#C1Tabs1_Tab10'>Tab10</a></li>" +
				"</ul><div id='C1Tabs1_Tab1'>1</div><div id='C1Tabs1_Tab2'>2</div>" +
				"</ul><div id='C1Tabs1_Tab3'>3</div><div id='C1Tabs1_Tab4'>4</div>" +
				"</ul><div id='C1Tabs1_Tab5'>5</div><div id='C1Tabs1_Tab6'>6</div>" +
				"</ul><div id='C1Tabs1_Tab7'>7</div><div id='C1Tabs1_Tab8'>8</div>" +
				"</ul><div id='C1Tabs1_Tab9'>9</div><div id='C1Tabs1_Tab10'>10</div></div>",
			tabs = $(str).appendTo("body").wijtabs({
				scrollable: true,
				alignment: "left",
				collapsible: true,
			}), $tab1 = $("li a[href='#C1Tabs1_Tab1']");
		ok(($("div.scrollWrap", tabs).length > 0), "scrollWrap was created correctly(alignment:left)");
		$tab1.click();
		if(tabs.find("ul li.ui-state-default:first").hasClass("ui-state-active"))
			$tab1.click();

		ok(!(tabs.find("ul li.ui-state-default:first").hasClass("ui-state-active")), "tab1 was collapsed(left)");
		var $wrapper = tabs.find("div.scrollWrap");
		ok($wrapper.width() === Math.floor(tabs.width()), "wrapper shows correctly(left)");
		tabs.wijtabs({
			alignment: "top"
		});
		if ($("div.scrollWrap", tabs).length > 0) {
			ok(true, "scrollWrap was created correctly(alignment:top)");
			ok(!(tabs.find("ul li.ui-state-default:first").hasClass("ui-state-active")), "tab1 was collapsed(top)");
			//for wijtabs to show normally.
			$("li a[href='#C1Tabs1_Tab1']").click();
			$("li a[href='#C1Tabs1_Tab1']").click();
			$wrapper = tabs.find("div.scrollWrap");
			ok(!$wrapper.hasClass("scrollWrap-collapsed"), "tab1 was not affected by the modifying(top)");
		}
		tabs.wijtabs({
			alignment: "right"
		});
		if ($("div.scrollWrap", tabs).length > 0) {
			ok(true, "scrollWrap was created correctly(alignment:right)");
			ok(!(tabs.find("ul li.ui-state-default:first").hasClass("ui-state-active")), "tab1 was collapsed(right)");
			//for wijtabs to show normally.
			$("li a[href='#C1Tabs1_Tab1']").click();
			$("li a[href='#C1Tabs1_Tab1']").click();
			$wrapper = tabs.find("div.scrollWrap");
			ok($wrapper.width() === Math.floor(tabs.width()), "wrapper shows correctly(right)");
		}
		tabs.remove();
	});

	test("#97827", function () {
		var str = "<div id='tabs' style='width:500px;'><ul><li><a href='#C1Tabs1_Tab1'>Tab1</a></li><li><a href='#C1Tabs1_Tab2'>Tab2</a></li></ul>" +
				"<div id='C1Tabs1_Tab1'>1</div><div id='C1Tabs1_Tab2'>2</div></div>",
				$widget = $(str).wijtabs({ scrollable: true, alignment: "left", collapsible: true,selected:-1}).appendTo("body");
		$widget.wijtabs("select", 0);
		ok(Math.floor($("#tabs").width()) === 500, "Expanding tab won't clear all layout styles of wijtabs");
		$widget.remove();
		str = "<div id='tabs'></div>";
		$widget = $(str).wijtabs().appendTo("body");
		ok("Creating a wijtab control without ul and li, it won't throw exception.");
		$widget.remove();
	});

	test("#60879", function () {
		var str = "<div style='width:500px;'><ul>" +
				"<li><a href='#C1Tabs1_Tab1'>Tab1</a></li><li><a href='#C1Tabs1_Tab2'>Tab2</a></li>" +
				"<li><a href='#C1Tabs1_Tab3'>Tab3</a></li><li><a href='#C1Tabs1_Tab4'>Tab4</a></li>" +
				"<li><a href='#C1Tabs1_Tab5'>Tab5</a></li><li><a href='#C1Tabs1_Tab6'>Tab6</a></li>" +
				"<li><a href='#C1Tabs1_Tab7'>Tab7</a></li><li><a href='#C1Tabs1_Tab8'>Tab8</a></li>" +
				"<li><a href='#C1Tabs1_Tab9'>Tab9</a></li><li><a href='#C1Tabs1_Tab10'>Tab10</a></li>" +
				"</ul><div id='C1Tabs1_Tab1'>1</div><div id='C1Tabs1_Tab2'>2</div>" +
				"</ul><div id='C1Tabs1_Tab3'>3</div><div id='C1Tabs1_Tab4'>4</div>" +
				"</ul><div id='C1Tabs1_Tab5'>5</div><div id='C1Tabs1_Tab6'>6</div>" +
				"</ul><div id='C1Tabs1_Tab7'>7</div><div id='C1Tabs1_Tab8'>8</div>" +
				"</ul><div id='C1Tabs1_Tab9'>9</div><div id='C1Tabs1_Tab10'>10</div></div>",
			tabs = $(str).appendTo("body").wijtabs({
				alignment: "bottom"
			}), $tab1 = $("li a[href='#C1Tabs1_Tab1']"), $tab7 = $("li a[href='#C1Tabs1_Tab7']"),
			$tab8 = $("li:eq(8)"), prevPostion = $tab8.position(),nextPostion;
		$tab7.simulate("click");
		nextPostion = $tab8.position();
		ok(prevPostion.left ===nextPostion.left && prevPostion.top === nextPostion.top, "tab8's position isn't affected by choose another tab");
		tabs.remove();
	});
})(jQuery)