/*
 * wijtabs_methods.js
 */
 
 
(function($) {

module("wijtabs: methods");

test("select, add, remove", function() {
	var removed = false;
	var $widget = createTabs({
		hideOption: { blind: true, fade: true, duration: 200},
		showOption: { blind: true, fade: true, duration: 200},
		show: function(event, ui){
			if ($widget == undefined || removed) return;
			ok($widget.wijtabs('option', 'selected') == 3, 'tab 3 is selected!');
			window.setTimeout(function(){
				$widget.wijtabs('remove', 3);
			}, 1000);
		},
		add: function(event, ui) {
			$(ui.panel).append('<p>This is new content.</p>');
			ok(ui.index == 3, 'tab 3 is added!');
			$widget.wijtabs('select', 3);
		},
		remove: function(event, ui){
			removed = true;
			ok(true, 'tab is removed');
			window.setTimeout(function(){
				start();
				$widget.remove();
			}, 1000);
		}
	});
	$widget.wijtabs('add', '#tabs-4', 'Tab4');
	stop();
});


})(jQuery);
