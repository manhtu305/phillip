﻿

(function ($) {

	module("wijtabs: options");

	test("disabled", function () {
		var $widget = createTabs(),
			secondTab, selectedIndex;
		ok(!$widget.hasClass("ui-state-disabled"), 'disabled class is not added when disabled is false when creating');
		$widget.wijtabs("option", "disabled", true);
		ok($widget.hasClass("ui-state-disabled"), 'disabled class is added when disabled is set to true');
		secondTab = $($("a", $widget)[1]);
		secondTab.simulate('click');
		selectedIndex = $widget.wijtabs("option", "selected");
		ok(selectedIndex === 0, 'second tab can\'t be selected when clicking.');

		$widget.wijtabs("option", "disabled", false);
		ok(!$widget.hasClass("ui-state-disabled"), 'disabled class is added when disabled is set to false');
		secondTab.simulate('click');
		selectedIndex = $widget.wijtabs("option", "selected");
		ok(selectedIndex === 1, 'second tab can be selected when clicking.');
		$widget.remove();

		$widget = createTabs({ disabled: true });
		ok($widget.hasClass("ui-state-disabled"), 'disabled class is added when disabled is true when creating');
		secondTab = $($("a", $widget)[1]);
		secondTab.simulate('click');
		selectedIndex = $widget.wijtabs("option", "selected");
		ok(selectedIndex === 0, 'second tab can\'t be selected when clicking.');
		$widget.remove();
	});


})(jQuery);
