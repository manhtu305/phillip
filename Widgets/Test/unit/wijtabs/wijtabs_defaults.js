/*
 * wijtabs_defaults.js
 */

commonWidgetTests('wijtabs', {
	defaults: {
		initSelector: ":jqmData(role='wijtabs')",
		wijCSS: $.extend({}, $.wijmo.wijCSS, {
			wijtabs: "wijmo-wijtabs",
			wijtabsContent: "wijmo-wijtabs-content"
		}),
		wijMobileCSS: {
			header: "ui-header ui-bar-b",
			content: "ui-content ui-body ui-body-b",
			stateDefault: "ui-btn ui-btn-a",
			stateActive: "ui-btn-down-b"
		},
		create: null,
		disabled: false,
		///	<summary>
		///		Determines the tabs alignment to the content
		///	</summary>
		alignment: 'top',
		///	<summary>
		///		Determines whether the tab can be dragged to a new position
		///	</summary>
		sortable: false,
		///	<summary>
		///		Determines whether to wrap to the next line or scrolling is enabled when the tabs exceed the width
		///	</summary>
		scrollable: false,
		///	<summary>
		///		This event is triggered when a tab is added.
		///	</summary>
		add: null,
		///	<summary>
		///		Additional Ajax options to consider when loading tab content (see $.ajax).
		///	</summary>
		ajaxOptions: null,
		///	<summary>
		///		Whether or not to cache remote tabs content, e.g. load only once or with every click. 
		///		Cached content is being lazy loaded, e.g once and only once for the first click. 
		///		Note that to prevent the actual Ajax requests from being cached by the browser you need to provide an extra cache: 
		///		false flag to ajaxOptions.
		///	</summary>
		cache: false,
		///	<summary>
		///		Store the latest selected tab in a cookie. 
		///		The cookie is then used to determine the initially selected tab if the selected option is not defined. 
		///		Requires cookie plugin. 
		///	</summary>
		cookie: null, // e.g. { expires: 7, path: '/', domain: 'jquery.com', secure: true }
		///	<summary>
		///		Set to true to allow an already selected tab to become unselected again upon reselection.
		///	</summary>
		collapsible: false,
		///	<summary>
		///		The animation option for hidding the panel content
		///	</summary>
		hideOption: null, // e.g. { blind: true, fade: true, duration: 200}
		///	<summary>
		///		The animation option for showing the panel content
		///	</summary>
		showOption: null, // e.g. { blind: true, fade: true, duration: 200}
		///	<summary>
		///		This event is triggered when a tab is disabled.
		///	</summary>
		disable: null,
		///	<summary>
		///		An array containing the position of the tabs (zero-based index) that should be disabled on initialization.
		///	</summary>
		disabledIndexes: [],
		///	<summary>
		///		This event is triggered when a tab is enabled.
		///	</summary>
		enable: null,
		///	<summary>
		///		The type of event to be used for selecting a tab.
		///	</summary>
		event: 'click',
		///	<summary>
		///		If the remote tab, its anchor element that is, has no title attribute to generate an id from, 
		///		an id/fragment identifier is created from this prefix and a unique id returned by $.data(el), for example "ui-tabs-54".
		///	</summary>
		idPrefix: 'ui-tabs-',
		///	<summary>
		///		This event is triggered after the content of a remote tab has been loaded.
		///	</summary>
		load: null,
		///	<summary>
		///		HTML template from which a new tab panel is created in case of adding a tab with the add method or 
		///		when creating a panel for a remote tab on the fly.
		///	</summary>
		panelTemplate: '',
		///	<summary>
		///		This event is triggered when a tab is removed.
		///	</summary>
		remove: null,
		///	<summary>
		///		This event is triggered when clicking a tab.
		///	</summary>
		select: null,
		beforeShow: null,
		///	<summary>
		///		This event is triggered when a tab is shown.
		///	</summary>
		show: null,
		///	<summary>
		///		The HTML content of this string is shown in a tab title while remote content is loading. 
		///		Pass in empty string to deactivate that behavior. 
		///		An span element must be present in the A tag of the title, for the spinner content to be visible.
		///	</summary>
		spinner: '',
		///	<summary>
		///		HTML template from which a new tab is created and added. 
		///		The placeholders #{href} and #{label} are replaced with the url and tab label that are passed as 
		///		arguments to the add method.
		///	</summary>
		tabTemplate: ''
	}
});
