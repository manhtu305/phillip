﻿/*globals jQuery, test, ok, module, createSimpleBarchart, createBarchart*/
/*
* wijbarchart_options.js
*/
"use strict";
(function ($) {
	module("wijbarchart:options");

	test("seriesListEmpty", function() {
		var barchart = createSimpleBarchart(),
			seriesList = [],
			bar;
		seriesList.push({ data: { x: ['x1', 'x2', 'x3', 'x4'], y: [5, 10, 12, 20]} });
		seriesList.push({ });
		barchart.wijbarchart('option', 'seriesList', seriesList);
		ok(true, 'ok.');
		barchart.remove();
	});

	test("horizontal", function () {
		//vertical
		var barchart = createSimpleBarchart(),
			bar;

		barchart.wijbarchart('option', 'horizontal', false);

		bar = barchart.wijbarchart("getBar", 0);

		ok(bar.chartLabel.attrs.text === "107", 'show column bar.');

		//horizontal
		barchart.wijbarchart('option', 'horizontal', true);
		bar = barchart.wijbarchart("getBar", 0);

		ok(bar.chartLabel.attrs.text === "973", 'show horizontal bar.');
		barchart.remove();
	});

	test("stacked", function () {
		var barchart = createSimpleBarchart(),
			bar;
		barchart.wijbarchart('option', 'stacked', true);

		bar = barchart.wijbarchart("getBar", 0);
		ok(bar.chartLabel.attrs.text === "107", 
				'show first section of the stacked bar.');

		bar = barchart.wijbarchart("getBar", 1);
		ok(bar.chartLabel.attrs.text === "240", 
				'show second section of the stacked bar.');

		bar = barchart.wijbarchart("getBar", 2);
		ok(bar.chartLabel.attrs.text === "1213", 
				'show last section of the stacked bar.');
		barchart.remove();
	});

	test("is100Percent", function () {
		var barchart = createSimpleBarchart(),
			bar, yaxis;
		barchart.wijbarchart('option', 'stacked', true);
		barchart.wijbarchart('option', 'is100Percent', true);

		bar = barchart.wijbarchart("getBar", 0);
		ok(bar.chartLabel.attrs.text === "107", 
				'show first percentage of the stacked bar.');

		bar = barchart.wijbarchart("getBar", 1);
		ok(bar.chartLabel.attrs.text === "240", 
				'show second percentage of the stacked bar.');

		bar = barchart.wijbarchart("getBar", 2);
		ok(bar.chartLabel.attrs.text === "1213", 
				'show last percentage of the stacked bar.');

		yaxis = barchart.wijbarchart("option", "axis").y;
		ok(yaxis.min === 0, 'get the minimum value of the y axis.');
		ok(yaxis.max === 1, 'get the maximum value of the y axis.');
		barchart.remove();
	});

	//from chartcore.
	test("axis", function () {
	    var barchart = createBarchart(),
			seriesList = [],
			axis, axisOpt, max, min, unitMajor, unitMinor,
			time, wijbarchart, formatString;
		seriesList.push({ data: { x: ['x1', 'x2', 'x3', 'x4'], y: [5, 10, 12, 20]} });
		barchart.wijbarchart('option', 'seriesList', seriesList);
		axis = barchart.wijbarchart('option', 'axis');
		max = axis.y.max;
		min = axis.y.min;
		unitMajor = axis.y.unitMajor;
		unitMinor = axis.y.unitMinor;
		ok(max === 20 && min === 0, 'max and min for seriesList1 are ok');
		ok(unitMajor === 1 && unitMinor === 0.5, 
				'unitMajor and unitMinor for seriesList1 are ok');

		seriesList = [];
		seriesList.push({ data: { x: ['x1', 'x2', 'x3', 'x4'], 
								y: [50, 1008, 2232, 3800]} });
		barchart.wijbarchart('option', 'seriesList', seriesList);
		axis = barchart.wijbarchart('option', 'axis');
		max = axis.y.max;
		min = axis.y.min;
		unitMajor = axis.y.unitMajor;
		unitMinor = axis.y.unitMinor;
		ok(max === 4000 && min === 0, 'max and min for seriesList2 are ok');
		
		//if ($.browser.msie && $.browser.version < 9) {
		//	ok(unitMajor === 1000 && unitMinor === 500, 
		//			'unitMajor and unitMinor for seriesList2 are ok');
		//}
		//else {
			ok(unitMajor === 500 && unitMinor === 250, 
					'unitMajor and unitMinor for seriesList2 are ok');
		//}

		seriesList = [];
		seriesList.push({ data: { x: ['x1', 'x2', 'x3', 'x4'], 
								y: [2150, 4008, 6232, 9500]} });
		barchart.wijbarchart('option', 'seriesList', seriesList);
		axis = barchart.wijbarchart('option', 'axis');
		max = axis.y.max;
		min = axis.y.min;
		unitMajor = axis.y.unitMajor;
		unitMinor = axis.y.unitMinor;
		ok(max === 10000 && min === 0, 'max and min for seriesList3 are ok');
		//if ($.browser.msie && $.browser.version < 9) {
		//	ok(unitMajor === 2000 && unitMinor === 1000, 
		//		'unitMajor and unitMinor for seriesList3 are ok');
		//}
		//else {
			ok(unitMajor === 1000 && unitMinor === 500, 
				'unitMajor and unitMinor for seriesList3 are ok');
		//}

		seriesList = [];
		seriesList.push({ data: { x: ['1', '2', '3', '4'], y: [50, 1008, 2232, 3800]} });
		barchart.wijbarchart('option', 'seriesList', seriesList);
		axis = barchart.data("wijmo-wijbarchart").axisInfo;
		max = axis.x.max;
		min = axis.x.min;
		axisOpt = barchart.wijbarchart('option', 'axis');
		unitMajor = axisOpt.x.unitMajor;
		unitMinor = axisOpt.x.unitMinor;
		ok(max === 3.5 && min === -0.5, 'max and min for seriesList4 are ok');
		ok(unitMajor === 1 && unitMinor === 0.5, 
				'unitMajor and unitMinor for seriesList4 are ok');

		seriesList = [];
		time = [new Date(2009, 3, 5), new Date(2009, 4, 12), 
				new Date(2009, 5, 20), new Date(2009, 8, 30)];
		seriesList.push({ data: { x: time, y: [2150, 4008, 6232, 9500] } });
		barchart.wijbarchart("option", "axis", { x: {annoFormatString:"M/d/yyyy"}});
		barchart.wijbarchart('option', 'seriesList', seriesList);
		wijbarchart = barchart.data("wijmo-wijbarchart");
		max = wijbarchart.options.axis.x.max;
		min = wijbarchart.options.axis.x.min;
		unitMajor = wijbarchart.options.axis.x.unitMajor;
		unitMinor =wijbarchart.options.axis.x.unitMinor;
		//ok(max === 3467059200000 && min === 3444422400000, 'max and min for seriesList5 are ok');
		//ok(unitMajor === 2678400000 && unitMinor === 1339200000, 'unitMajor and uintMinor for seriesList5 are ok');
		barchart.remove();
	});

	test("chartLabelFormatter && chartLabelFormatString", function () {
	    var barchart = createBarchart({
	        seriesList: [{
	            data: { x: [1, 2, 3], y: [7, 8, 4] }
	        }],
	        showChartLabels: true,
	        animation: {
	            enabled: false
	        }
	    }), chartLabel0;

	    chartLabel0 = barchart.data("wijmo-wijbarchart").chartElement.data("fields").chartElements.chartLabels[0].attr("text");
	    ok(chartLabel0 === "7", "the first chart element label is '7'");

	    barchart.wijbarchart("option", "chartLabelFormatString", "n2");
	    chartLabel0 = barchart.data("wijmo-wijbarchart").chartElement.data("fields").chartElements.chartLabels[0].attr("text");
	    ok(chartLabel0 === "7.00", "the first chart element label is updated according to chart label format string.");

	    barchart.wijbarchart("option", "chartLabelFormatter", function () {
	        return "mycustomzied:" + this.value;
	    });
	    chartLabel0 = barchart.data("wijmo-wijbarchart").chartElement.data("fields").chartElements.chartLabels[0].attr("text");
	    ok(chartLabel0 === "mycustomzied:7", "the first chart element label is updated according to chartLabelFormatter.");
        
	    barchart.remove();
	});

	test("disableDefaultTextStyle", function () {
	    var barchart = createBarchart({
	        "footer": {
	            "text": "Footer", "compass": "south", "visible": true
	        },
	        disableDefaultTextStyle: true,
	        seriesList: [{
	            label: "1800",
	            legendEntry: true,
	            data: { x: ['AFRICA', ], y: [107] }
	        }]
	    }), footFamily = function() {
	        var fontFamilySelector = $.browser.mozilla ? "fontFamily" : "font-family"; 
	        if ($.browser.msie && $.browser.version < 9){
	            return barchart.find(".wijchart-footer-text").find("textpath")[0].style[fontFamilySelector];
	        } else {
	            return barchart.find(".wijchart-footer-text")[0].style[fontFamilySelector];
	        }
	    };

	    ok(!footFamily(), "Default text style is disabled !");
	    barchart.wijbarchart("option", "disableDefaultTextStyle", false);
	    ok(footFamily(), "Default text style is enabled !");
	    barchart.wijbarchart("option", "disableDefaultTextStyle", true);
	    ok(!footFamily(), "Default text style is disabled !");

	    barchart.remove();
	});
}(jQuery));