/*globals test, ok, module, jQuery, createSimpleBarchart*/
/*
 * wijbarchart_methods.js
 */
"use strict";
(function ($) {

	module("wijbarchart: methods");

	test("getBar", function () {
		var barchart = createSimpleBarchart(),
			sector = barchart.wijbarchart('getBar', 1);
		
		ok(sector.attrs.fill === '#6c8587', 'getBar:gets the second bar');
		barchart.remove();
	});

	test("getCanvas", function () {
		var barchart = createSimpleBarchart(),
			canvas = barchart.wijbarchart('getCanvas');

		ok(canvas.circle !== null, 'getCanvas:gets the canvas for the bar chart');
		barchart.remove();
	});

}(jQuery));
