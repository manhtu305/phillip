/*globals start, stop, test, ok, module, jQuery, 
	simpleOptions, createSimpleBarchart, createBarchart*/
/*
* wijbarchart_events.js
*/
"use strict";
(function ($) {
	var barchart,
		newSeriesList = [{
			label: "1800",
			legendEntry: true,
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [100, 31, 635, 203] }
		}, {
			label: "1900",
			legendEntry: true,
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [133, 156, 947, 408] }
		}, {
			label: "2008",
			legendEntry: true,
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [973, 914, 4054, 732] }
		}];

	module("wijbarchart: events");

	test("mousedown", function () {
		barchart = createSimpleBarchart();
		barchart.wijbarchart({
			mouseDown: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 0, 'Press the mouse down on the first bar.');
					barchart.remove();
				}
			}
		});
		stop();
		$(barchart.wijbarchart("getBar", 1).tracker.node).trigger('mousedown');
		
	});

	test("mouseup", function () {
		barchart = createSimpleBarchart();
		barchart.wijbarchart({
			mouseUp: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 1, 'Release the mouse on the first bar.');
					barchart.remove();
				}
			}
		});
		stop();
		$(barchart.wijbarchart("getBar", 3).node).trigger('mouseup');		
	});

	test("mouseover", function () {
		barchart = createSimpleBarchart();
		barchart.wijbarchart({
			mouseOver: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 1, 'Hover the first bar.');
					barchart.remove();
				}
			}
		});
		stop();
		$(barchart.wijbarchart("getBar", 5).node).trigger('mouseover');		
	});

	test("mouseout", function () {
		barchart = createSimpleBarchart();
		barchart.wijbarchart({
			mouseOut: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 2, 'Move the mouse out of the first bar.');
					barchart.remove();
				}
			}
		});
		stop();
		$(barchart.wijbarchart("getBar", 6).node).trigger('mouseout');				
	});

	test("mousemove", function () {
		barchart = createSimpleBarchart();
		barchart.wijbarchart({
			mouseMove: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 2, 'Move the mouse on the first bar.');
					barchart.remove();
				}
			}
		});
		stop();
		$(barchart.wijbarchart("getBar", 7).node).trigger('mousemove');		
	});

	test("click", function () {
		barchart = createSimpleBarchart();
		barchart.wijbarchart({
			click: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 2, 'Click on the first bar.');
					barchart.remove();
				}
			}
		});
		stop();
		$(barchart.wijbarchart("getBar", 8).node).trigger('click');		
	});

	test("beforeserieschange", function () {
		barchart = createSimpleBarchart();
		var oldSeriesList = barchart.wijbarchart("option", "seriesList");
		barchart.wijbarchart({
			beforeSeriesChange: function (e, seriesObj) {
				if (seriesObj !== null) {
					start();
					var seriesList = barchart.wijbarchart("option", "seriesList");
					ok(seriesObj.oldSeriesList === oldSeriesList, 
							'get the correct old seriesList by seriesObj.oldSeriesList.');
					ok(seriesObj.newSeriesList === newSeriesList, 
							'get the correct new seriesList by seriesObj.newSeriesList.');
					ok(seriesList === oldSeriesList, 
							"currently seriesList hasn't been changed.");
					barchart.remove();
				}
			}
		});
		stop();
		barchart.wijbarchart("option", "seriesList", newSeriesList);		
	});

	test("serieschanged", function () {
		barchart = createSimpleBarchart();
		barchart.wijbarchart({
			seriesChanged: function (e, seriesObj) {
				if (seriesObj !== null) {
					start();
					var seriesList = barchart.wijbarchart("option", "seriesList");
					ok(seriesList === newSeriesList, 
						"currently seriesList has been changed.");
					barchart.remove();
				}
			}
		});
		stop();
		barchart.wijbarchart("option", "seriesList", newSeriesList);		
	});

	test("beforePaint, paint", function () {
		var options = $.extend({
				beforePaint: function (e) {
					ok(true, "The beforepaint event is invoked.");
					return false;
				},
				painted: function (e) {
					ok(false, "The painted event of bar chart1 is invoked.");
				}
			}, true, simpleOptions),
			bars, sector, options2, barchart2;
		barchart = createBarchart(options);

		bars = barchart.data("fields") || [];
		
		if (!(bars || bars.length === 0)) {
		ok(false, "The bar chart1 shouldn't be painted because of the cancellation.");
		} else {
			ok(true, "The bar chart1 isn't painted because of the cancellation.");
		}
		barchart.remove();

		options2 = $.extend({
			beforePaint: function (e) {
				if (e !== null) {
					ok(true, "The beforepaint event is invoked.");
				}
			},
			painted: function (e) {
				ok(true, "The painted event of bar chart2 is invoked.");
			}
		}, true, simpleOptions);
		barchart2 = createBarchart(options2);
		sector = barchart2.wijbarchart("getBar", 1);
		ok(sector !== null, 'The bar chart2 is painted.');
		barchart2.remove();
	});
}(jQuery));
