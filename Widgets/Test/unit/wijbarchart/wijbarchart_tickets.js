﻿(function ($) {
    module("wijbarchart: tickets");
    test("setting clusterRadius to a number value and shadow to false, an exception will be thrown", function () {
        var barchart = createSimpleBarchart(),
        seriesList = [],
                bar;
        seriesList.push({ data: { x: ['x1', 'x2', 'x3', 'x4'], y: [5, 10, 12, 20] } });
        barchart.wijbarchart('option', 'seriesList', seriesList);
        barchart.wijbarchart({ clusterRadius: 3, shadow: false });
        ok(true, "OK");
        barchart.remove();
    });

    test("datetime", function () {
        var barchart = createSimpleBarchart(),
        seriesList = [],
                bar;
        seriesList.push({ data: { x: [new Date("1/3/2010"), new Date("1/4/2010"), new Date("1/6/2010"), new Date("1/7/2010")], y: [5, 10, 12, 20] } });
        barchart.wijbarchart('option', 'seriesList', seriesList);
        ok(true, "x axis datetime, y number OK");

        seriesList = [];
        seriesList.push({
            data: {
                x: [new Date("1/3/2010"), new Date("1/4/2010"), new Date("1/6/2010"), new Date("1/7/2010")],
                y: [new Date("1/1/2011"), new Date("1/3/2011"), new Date("1/6/2011"), new Date("1/12/2010")]
            }
        });
        barchart.wijbarchart('option', 'seriesList', seriesList);
        ok(true, "x axis datetime, y datetime OK");

        seriesList = [];
        seriesList.push({ data: { y: [new Date("1/3/2010"), new Date("1/4/2010"), new Date("1/6/2010"), new Date("1/7/2010")], x: [5, 10, 12, 20] } });
        barchart.wijbarchart('option', 'seriesList', seriesList);
        ok(true, "x number, y axis datetime OK");

        seriesList = [];
        seriesList.push({ data: { y: [100, 150, 100, 125], x: [5, 10, 12, 20] } });
        barchart.wijbarchart('option', 'seriesList', seriesList);
        ok(true, "x number, y axis number OK");

        seriesList = [];
        seriesList.push({ data: { x: ["2010", "2011", "2012", "2013"], y: [5, 10, 12, 20] } });
        barchart.wijbarchart('option', 'seriesList', seriesList);
        ok(true, "x string y number OK");

        seriesList = [];
        seriesList.push({ data: { x: ["2010", "2011", "2012", "2013"], y: [new Date("1/3/2010"), new Date("1/4/2010"), new Date("1/6/2010"), new Date("1/7/2010")] } });
        barchart.wijbarchart('option', 'seriesList', seriesList);
        ok(true, "x string y datetime OK");

        barchart.remove();
    });

    test("#38351", function () {
        var barchart = createBarchart({
            seriesStyles: [{ rotation: 50 }],
            seriesList: [{ data: { x: ['x1', 'x2', 'x3', 'x4'], y: [5, 10, 12, 20] } }]
        });
        var bar = barchart.wijbarchart("getBar", 0);
        ok(Math.round(bar.matrix.split().rotate) === 50, "the bar element is rotate 50.");
        setTimeout(function () {
            $(bar.node).simulate("mouseover");
            $(bar.node).simulate("mouseout");
            setTimeout(function () {
                ok(Math.round(bar.matrix.split().rotate) === 50, "when mouseover and mouse out the bar element, the bar element is rotate 50.");
                barchart.remove();
                start();
            }, 500);
        }, 500)
        stop();
    });

    test("#40134", function () {
        var barchart = createBarchart({
            axis: {
                x: {
                    text: "This is test",
                    labels: {
                        style: {
                            transform: "R90"
                        },
                        textAlign: "far"
                    }
                }
            },
            seriesList: [{ data: { x: ['x1', 'x2', 'x3', 'x4'], y: [5, 10, 12, 20] } }]
        });
        ok(true, "the chart works without exception.");
        barchart.remove();
    });
    test("#41904", function () {
        var barchart = $("<table>").appendTo("body");
        barchart.append("<thead><tr><td></td><th scope='col'>ACT Now for Safety Use</th></tr></thead>");
        for (var i = 0; i < 14; i++) {
            barchart.append("<tr><th scope='col'>Series" + i + "</th><td>" + i + "</td></tr>");
        }
        barchart.append("</table>");
        barchart.wijbarchart();
        ok(true, "no exception has thrown.");
        barchart.remove();
    });

    test("#41746, when autoMajor is set to false, don't let adjusted value infect tick values", function () {
        var barchart = createBarchart({
            axis: {
                x: {
                    autoMajor: false,
                    unitMajor: 2
                }
            },
            seriesList: [{ data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20] } }]
        });

        axisLabels = barchart.find(".wijchart-axis-label");

        ok(axisLabels.eq(0).text() === "0" &&
            axisLabels.eq(1).text() === "2" &&
            axisLabels.eq(2).text() === "4", "true when autoMajor is set to false.");
        barchart.remove();
    });

    test("When min/max of axis option is set, autoMin/autoMax should be set to false", function () {
        var barchart = createBarchart({
            axis: {
                x: {
                    max: 10,
                    min: 0
                },
                y: {
                    min: 0,
                    max: 30
                }
            },
            seriesList: [{ data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20] } }]
        });

        var axis = barchart.wijbarchart("option", "axis"),
            axisInfo = barchart.data("wijmo-wijbarchart").axisInfo,
            xMin = axisInfo.x.min || axis.x.min,
            xMax = axisInfo.x.max || axis.x.max,
            yMin = axisInfo.y.min || axis.y.min,
            yMax = axisInfo.y.max || axis.y.max;
        ok(axis.x.autoMin === false && xMin === -1.5, "autoMin of x axis is set to false when axis.x.min is set.");
        ok(axis.x.autoMax === false && xMax === 11.5, "autoMax of x axis is set to false when axis.x.max is set.");
        ok(axis.y.autoMin === false && yMin === 0, "autoMin of y axis is set to false when axis.y.min is set.");
        ok(axis.y.autoMax === false && yMax === 30, "autoMax of y axis is set to false when axis.y.max is set.");
        barchart.remove();

		barchart = createBarchart({
            seriesList: [{ data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20] } }]
        });
        axis = barchart.wijbarchart("option", "axis");
        ok(axis.x.autoMin === true, "autoMin of x axis is true.");
        ok(axis.x.autoMax === true, "autoMax of x axis is true.");
        ok(axis.y.autoMin === true, "autoMin of y axis is true.");
        ok(axis.y.autoMax === true, "autoMax of y axis is true.");
		barchart.wijbarchart("option", "axis", {
            x: {
                max: 10,
                min: 0
            },
            y: {
                min: 0,
                max: 30
            }
        });
		axis = barchart.wijbarchart("option", "axis");
		axisInfo = barchart.data("wijmo-wijbarchart").axisInfo;
		xMin = axisInfo.x.min || axis.x.min;
		xMax = axisInfo.x.max || axis.x.max;
		yMin = axisInfo.y.min || axis.y.min;
		yMax = axisInfo.y.max || axis.y.max;
		ok(axis.x.autoMin === false && xMin === -1.5, "autoMin of x axis is set to false when axis.x.min is set.");
        ok(axis.x.autoMax === false && xMax === 11.5, "autoMax of x axis is set to false when axis.x.max is set.");
        ok(axis.y.autoMin === false && yMin === 0, "autoMin of y axis is set to false when axis.y.min is set.");
        ok(axis.y.autoMax === false && yMax === 30, "autoMax of y axis is set to false when axis.y.max is set.");
        barchart.remove();
    });

    test("46539, legend item click test, in case that y-array is empty.", function () {
        var barchart = createBarchart({
            seriesList: [{
                label: "East",
                legendEntry: true,
                data: { x: ["1", "15", "60"], y: [] },
            }]
        }), legendElement, result = true;

        legendElement = barchart.find(".wijchart-legend-icon");
        result = result && (legendElement.attr("opacity") == 0.9);
        legendElement.simulate("click");
        result = result && (legendElement.attr("opacity") == 0.3);

        ok(result, "Style of legend item changed after click.");
        barchart.remove();
    });

    test("47804, apply font settings of legend test", function () {
    	var barchart = createBarchart({
    		legend: {
				textStyle:{
    				"fontSize": 15,
    				"fontweight": "bold"
				}
    		},
    		seriesList: [{
    			label: "East",
    			legendEntry: true,
    			data: { x: ["1", "15", "60"], y: [] },
    		}]
    	}), legendElement;

    	legendElement = barchart.find(".wijchart-legend-text");

    	ok(legendElement.attr("font-size") === "15px", "The font-size is set to legend correctly with 'fontSize' option.");
    	ok(legendElement.attr("font-weight") === "bold", "The font-weight is set to legend correctly with 'fontweight' option.");

    	barchart.remove();
    });

    test("47804, apply font settings of legend test with textWidth option of legend", function () {
    	var barchart = createBarchart({
    		legend: {
				textWidth: 30,
    			textStyle: {
    				"fontsize": 15,
    				"fontWeight": "bold"
    			}
    		},
    		seriesList: [{
    			label: "East",
    			legendEntry: true,
    			data: { x: ["1", "15", "60"], y: [] },
    		}]
    	}), legendElement;

    	legendElement = barchart.find(".wijchart-legend-text");

    	ok(legendElement.attr("font-size") === "15px", "The font-size is set to legend correctly with 'fontsize' option.");
    	ok(legendElement.attr("font-weight") === "bold", "The font-weight is set to legend correctly with 'fontWeight' option.");

    	barchart.remove();
    });

    test("gridstyle is changed to the last line style of valuelabels. ", function () {
        var barchart = createBarchart({
            "horizontal": false,
            "seriesList": [
                      {
                          "data": {
                              "x": [new Date(2013, (12 - 1), 15, 00, 00, 00), new Date(2013, (12 - 1), 31, 00, 00, 00), new Date(2014, (01 - 1), 15, 00, 00, 00), new Date(2014, (01 - 1), 23, 00, 00, 00), new Date(2014, (02 - 1), 01, 00, 00, 00)],
                              "y": [5, 7, 3, 9, 2]
                          }, "label": "Series 1", "legendEntry": true
                      }
            ],
            "axis": {
                "y": {
                    "gridMajor": {
                        "visible": true, "style": {
                            "stroke": "Blue", "stroke-width": 2
                        }
                    },
                    "annoMethod": "valueLabels",
                    "valueLabels": [{
                        "value": 3, "gridLineStyle": {
                            "stroke": "Yellow", "stroke-width": 7
                        }, "gridLine": true, "text": "YLabel1"
                    }]
                }
            }
        }), yGridStyle = barchart.wijbarchart("option", "axis").y.gridMajor.style;

        ok(yGridStyle["stroke-width"] == 2, "The stroke width of grid major style shouldn't be changed.");
        ok(yGridStyle["stroke"] == "Blue", "The stroke color of grid major style shouldn't be changed.");
        barchart.remove();
    });

    test("#71204: Uncaught TypeError:\"Cannot read property 'bar' of undefined\" is thrown when toggling legend item if the trendline is set in the stacked C1BarChart", function () {
        var barchart = createBarchart({
            stacked: true,
            seriesList: [
            {
                label: "New Series 1",
                legendEntry: true,
                data: {
                    x: [-9.5, -7, 0, 3.5, 5],
                    y: [22.5, 17, -5.5, 0, -9]
                }
            },
            {
                label: "Trendline 1",
                legendEntry: true,
                isTrendline: true,
                data: {
                    x: [-9.5, -7, 0, 3.5, 5],
                    y: [22.5, 17, -5.5, 0, -9]
                }
            },
            {
                label: "New Series 2",
                legendEntry: true,
                data: {
                    x: [-9.5, -7, 0, 3.5, 5],
                    y: [-9.5, -8, 11.5, -9, 1]
                }
            }]
        }), legendElement = barchart.find(".wijchart-legend-icon");

        legendElement.simulate("click");
        barchart.remove();        
    });

    test("#71130", function () {
    	var barChart = createBarchart({
    		height: 400,
			width: 600,
    		seriesList: [
            {
            	label: "New Series 1",
            	data: {
            		x: [-9.5, -7],
            		y: [22.5, 17]
            	}
            },
            {
            	label: "New Series 2",
            	data: {
            		x: [-9.5, -7],
            		y: [22.5, 17]
            	}
            },
            {
            	label: "New Series 3",
            	data: {
            		x: [-9.5, -7],
            		y: [22.5, 17]
            	}
            }, {
            	label: "New Series 4",
            	data: {
            		x: [-9.5, -7],
            		y: [22.5, 17]
            	}
            },
            {
            	label: "New Series 5",
            	data: {
            		x: [-9.5, -7],
            		y: [22.5, 17]
            	}
            },
            {
            	label: "New Series 6",
            	data: {
            		x: [-9.5, -7],
            		y: [22.5, 17]
            	}
            },
			{
				label: "New Series 7",
				data: {
					x: [-9.5, -7],
					y: [22.5, 17]
				}
			},
            {
            	label: "New Series 8",
            	data: {
            		x: [-9.5, -7],
            		y: [22.5, 17]
            	}
            },
            {
            	label: "New Series 9",
            	data: {
            		x: [-9.5, -7],
            		y: [22.5, 17]
            	}
            }, {
            	label: "New Series 10",
            	data: {
            		x: [-9.5, -7],
            		y: [22.5, 17]
            	}
            },
            {
            	label: "New Series 11",
            	data: {
            		x: [-9.5, -7],
            		y: [22.5, 17]
            	}
            },
            {
            	label: "New Series 12",
            	data: {
            		x: [-9.5, -7],
            		y: [22.5, 17]
            	}
            },
			{
				label: "New Series 13",
				data: {
					x: [-9.5, -7],
					y: [22.5, 17]
				}
			}]
    	});

    	barChart.wijbarchart("redraw");

    	ok(true, "Bar redraw withou errors.");
    	barChart.remove();
    });

    test("#71878", function () {
        var seriesEles, trendlinePath;

        barchart = createBarchart({
            seriesList: [{
                label: "Trendline",
                legendEntry: true,
                isTrendline: true,
                data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [100, 31, 635, 203] }
            }],
            mouseOver: function (e, chartObj) {
                if (chartObj !== null) {
                    ok(chartObj.type === "trendLine", 'Hover on the trendline.');
                }
            },
            seriesStyles: [{
                stroke: "#FFA41C", "stroke-width": 1, "stroke-opacity": 0.7
            }],
            seriesHoverStyles: [{
                "stroke-width": 5
            }]
        });

        seriesEles = barchart.data("wijmo-wijbarchart").chartElement.data("fields").seriesEles;
        ok(seriesEles.length === 1 && seriesEles[0].isTrendline, "The trendline is drawn.");
        trendlinePath = seriesEles[0].path;
        ok(trendlinePath.attr("stroke-width") === 1, "SeriesStyle is applied on trendline");
        $(trendlinePath.node).trigger('mouseover');
        ok(trendlinePath.attr("stroke-width") === 5, "HoverSeriesStyle is applied on trendline");
        $(trendlinePath.node).trigger('mouseout');
        ok(trendlinePath.attr("stroke-width") === 1, "HoverSeriesStyle is removed and seriesStyle is applied on trendline");
        barchart.remove();
    });

    test("#72123", function () {
        var barchart = createBarchart({
            seriesList: [{
                label: "Trendline for Orange",
                legendEntry: true,
                isTrendline: true,
                data: { x: [1, 2, 3, 4, 5], y: [7, 8, 4, 5, 8, 9] }
            }]
        }), fields = barchart.data("fields"), legends;

        ok(fields.seriesEles.length === 1 && fields.seriesEles[0].isTrendline, "The trendline should be shown.");
        ok(barchart.find(".wijchart-legend-icon").length === 1, "The trendline legend should be shown.");
        barchart.remove();
    });

    test("#65149", function () {
        var barchart = createBarchart({
            seriesList: [{
                legendEntry: true,
                data: { x: [1, 2, 3, 4, 5], y: [7, 8, 4, 5, 8, 9] }
            }],
            beforePaint: function () {
                return false;
            }
        }), events = ["mousedown", "mouseup", "mousemove", "mouseout"],
        element = barchart.data("wijmo-wijbarchart").chartElement;

        $.each(events, function (i, eName) {
            element.trigger(eName);
        });
        ok(true, "barchart works correctly!");
        barchart.remove();
    });

    test("Check the display for different origin settings of y axis.", function () {
        var barchart = createBarchart({
            horizontal: false,
            seriesList: [{
                data: { x: [1, 2], y: [5, 9] }
            }],
            animation: {
                enabled: false
            }
        }), barElement1, barElement2;

        barElement1 = barchart.wijbarchart("getBar", 0);
        barElement2 = barchart.wijbarchart("getBar", 1);
        ok(barElement1.attr("height") < barElement2.attr("height"), "the first bar element's height is smaller than the second one's.");

        barchart.wijbarchart("option", "axis", { y: { origin: 8 } });
        barElement1 = barchart.wijbarchart("getBar", 0);
        barElement2 = barchart.wijbarchart("getBar", 1);
        ok(barElement1.attr("height") > barElement2.attr("height"), "the first bar element's height is larger than the second one's.");

        barchart.remove();
        barchart = null;
        barchart = createBarchart({
            horizontal: true,
            seriesList: [{
                data: { x: [1, 2], y: [5, 9] }
            }],
            animation: {
                enabled: false
            }
        });
        barElement1 = barchart.wijbarchart("getBar", 0);
        barElement2 = barchart.wijbarchart("getBar", 1);
        ok(barElement1.attr("width") < barElement2.attr("width"), "the first bar element's width is smaller than the second one's.");

        barchart.wijbarchart("option", "axis", { y: { origin: 8 } });
        barElement1 = barchart.wijbarchart("getBar", 0);
        barElement2 = barchart.wijbarchart("getBar", 1);
        ok(barElement1.attr("width") > barElement2.attr("width"), "the first bar element's width is larger than the second one's.");

        barchart.remove();
        barchart = null;
        barchart = createBarchart({
            stacked: true,
            horizontal: false,
            seriesList: [{
                data: { x: [1, 2], y: [5, 9] }
            }, {
                data:{x:[1,2], y:[9, 10]}
            }],
            animation: {
                enabled: false
            }
        });

        barElement1 = barchart.wijbarchart("getBar", 0);
        barElement2 = barchart.wijbarchart("getBar", 1);
        ok(barElement1.attr("height") < barElement2.attr("height"), "the first bar element's height is smaller than the second one's.");

        barchart.wijbarchart("option", "axis", { y: { origin: 8 } });
        barElement1 = barchart.wijbarchart("getBar", 0);
        barElement2 = barchart.wijbarchart("getBar", 1);
        ok(barElement1.attr("height") < barElement2.attr("height"), "the first bar element's height is larger than the second one's.");

        barchart.remove();
        barchart = null;
        barchart = createBarchart({
            stacked: true,
            horizontal: true,
            seriesList: [{
                data: { x: [1, 2], y: [5, 9] }
            }, {
                data: { x: [1, 2], y: [9, 10] }
            }],
            animation: {
                enabled: false
            }
        });

        barElement1 = barchart.wijbarchart("getBar", 0);
        barElement2 = barchart.wijbarchart("getBar", 1);
        ok(barElement1.attr("width") < barElement2.attr("width"), "the first bar element's width is smaller than the second one's.");

        barchart.wijbarchart("option", "axis", { y: { origin: 8 } });
        barElement1 = barchart.wijbarchart("getBar", 0);
        barElement2 = barchart.wijbarchart("getBar", 1);
        ok(barElement1.attr("width") < barElement2.attr("width"), "the first bar element's width is larger than the second one's.");

        barchart.remove();
        barElement1 = null;
        barElement2 = null;
    });

    test("#74328", function () {
        var barchart = createBarchart({
            "horizontal": false, "animation": { enabled: false },
            "seriesList": [{
                "data": {
                    "x": [5.5],
                    "y": [5.5]
                },
                "label": "Column 1",
                "legendEntry": true
            }],
            "hint": {
                "enable": false
            },
            "indicator": {
                "visible": true
            }
        }), columnEle, mouseDownEveData;

        columnEle = barchart.find("rect.wijbarchart.bartracker");

        mouseDownEveData = {
            which: 1,
            clientX: columnEle.offset().left + columnEle.width() / 2,
            clientY: columnEle.offset().top + columnEle.height() / 2
        };

        columnEle.simulate("mouseenter");
        columnEle.simulate("mouseover");
        columnEle.simulate("mousedown", mouseDownEveData);

        ok(true, "Does not throw JS error !");

        barchart.remove();
    });

    test("#77157, test render column chart", function () {
    	var columnChart = createBarchart({
    		horizontal: false,
    		axis: {
    			x: { text: "x" },
    			y: [{ text: "y", compass: "east" }, { text: "y2", compass: "west" }]
    		},
    		data: {
    			x: ["2012-Q1", "2012-Q2", "2012-Q3", "2012-Q4", "2013-Q1"]
    		},
    		seriesList: [{
    			label: "Q1",
    			legendEntry: true,
    			yAxis: 1,
    			data: {
    				y: [12, 21, 9, 29, 30]
    			}
    		}]
    	});

    	ok(true, "The column chart was created without errors, when the data series was added at the secondary axis");
    	columnChart.remove();
    });

    test("#89807", function () {
        var barchart = $('<div id="barchart" style = "width: 856px; height: 475px"></div>')
			.appendTo(document.body).wijbarchart({
            "animation": { enabled: false },
            "seriesList": [{
                "data": {
                    "x": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
                    "y": [20, 50, 40, 30, 10, 30, 40, 50, 60, 70, 90, 80, 40, 10, 30, 40, 10, 30]
                },
                "label": "Column 1",
            }],
            "hint": {
                "enable": false
            }
        }), axis = barchart.data("wijmo-wijbarchart").axisInfo;

        ok(axis.x.min === 0.5 && axis.x.max === 18.5, "There will not be extra space generated between the YAxis and the bar !");

        barchart.remove();
    });

    test("#90298", function () {
        var barchart = $('<div id="barchart" style = "width: 356px; height: 275px"></div>')
			.appendTo(document.body).wijbarchart({
			    "axis": {
			        x: { "autoMin": false, "autoMax": false, "min": 0, "max": 3 }
			    },
			    horizontal: false,
			    "animation": { enabled: false },
			    "seriesList": [{
			        "data": {
			            "x": [1, 2],
			            "y": [20, 50]
			        },
			        "label": "Column 1",
			    }],
			}), bartracker = barchart.find(".wijbarchart.bartracker").eq(0), positionX;

        positionX = bartracker.position().left;
        barchart.wijbarchart("redraw");
        bartracker = barchart.find(".wijbarchart.bartracker").eq(0);

        ok(positionX === bartracker.position().left, "Columns has not changed after refresh !");
        barchart.remove();
    });

    test("#90595", function () {
        var barchart = $('<div id="barchart" style = "height: 400px; width:600px"></div>')
			.appendTo(document.body).wijbarchart({
			    horizontal: false,
			    "animation": { enabled: false },
			    "seriesList": [{
			        "data": {
			            x: [1, 2, 3, 4, 5],
			            y: [20, 22, 19, 24, 25]
			        },
			    }], axis: {
			        y: {
                        textVisible: false
			        },
			        x: {
			            annoMethod: "valueLabels",
			            visible: true,
			            min: 0,
			            valueLabels: ["label1", "label2", "label3"]
			        }
			    }
			});

        ok(barchart.find(".wijchart-axis-label").length === 3, "The valueLabels all have been painted !");
        barchart.remove();
    });

    test("#89023", function () {
        var container = $('<div id="container"></div>').appendTo("body"),
            barchart = $('<div id="barchart" style = "width: 356px; height: 275px"></div>').appendTo(container).wijbarchart({
                horizontal: false,
                "animation": { enabled: false },
            });

        container.hide();

        barchart.wijbarchart("option", "seriesList", [{
            "data": {
                "x": [1, 2],
                "y": [20, 50]
            },
            "label": "Column 1",
        }]);

        container.show();
        container.wijTriggerVisibility();
        ok(barchart.find("rect.wijchart-canvas-object").length === 2, "BarChart refreshed after container show !");

        barchart.remove();
        container.remove();
    });

    test("#83637", function () {
        var barchart = createBarchart({
            seriesList: [{
                label: "series1"
            }, {
                label: "Series2",
                data: { x: [new Date("1/1/2014"), new Date("1/3/2014")], y: [5, 3] }
            }]
        });

        ok(true, "Flag to find if there is JS error !");

        barchart.remove();
    });

    test("#94329", function () {
        var barChart = createBarchart({
            "animation": { enabled: false },
            disabled: true,
            data: {
                x: ["2012-Q1", "2012-Q2"]
            },
            seriesList: [{
                label: "Q1",
                legendEntry: true,
                yAxis: 1,
                data: {
                    y: [12, 21]
                }
            }]
        }), legengIcon = barChart.find(".wijchart-legend-icon"),
            barElement = barChart.find(".wijchart-canvas-object.wijbarchart").eq(0);
        legengIcon.simulate("click");

        ok(barElement.css("display") !== "none", "The chart legend has been disabled !");

        barChart.remove();
    });

    test("#100323", function () {
        var barChart = $("<div style='width: 300px; height: 200px'></div>").appendTo("body").wijbarchart({
            seriesList: [{
                label: "Orange",
                legendEntry: true,
                data: { x: [1, 2, 3, 4, 5], y: [20, 22, 19, 24, 25] }
            }],
            axis: {
                x: {
                    visible: false,
                    textVisible: false
                },
                y: {
                    annoMethod: "valueLabels",
                    text: "Y Axis",
                    visible: true,
                    valueLabels: [
                        {
                            gridLine: true,
                            text: "Y Label 1",
                            numericValue: 3,
                        },
                        {
                            gridLine: true,
                            text: "Y Label 2",
                            numericValue: 5,
                        },
                    ]
                }
            }
        }), transformValue;

        barChart.wijbarchart("option", "axis", {
            y: {
                annoMethod: "values"
            }
        });

        transformValue = barChart.find(".wijchart-axis-label").eq(0).attr("transform");
        ok(!transformValue || transformValue === "matrix(1,0,0,1,0,0)", "The rotation of axis labels has been recovered !");

        barChart.remove();
    });

    test("#113261", function () {
        var barChart = $("<div style='width: 300px; height: 200px'></div>").appendTo("body").wijbarchart({
            horizontal: false,
            stacked: false,
            showChartLabels: false,
            axis: {
                y: {
                    textVisible: false
                }
            },
            seriesList: [{
                data: {
                    x: [false, false, false, false, false],
                    y: [1, 2, 3, 4, 5]
                }
            }]
        });

        ok(barChart.find("text.wijchart-axis-label").eq(0).text() === "false", "Boolean value can be set to the Axis labels !");
        barChart.remove();
    });
})(jQuery);


