/*globals test, ok, jQuery, module, document, $*/
/*
* wijbarchart_core.js create & destroy
*/
"use strict";
function createBarchart(o) {
	return $('<div id="barchart" style = "width:600px;height:300px"></div>')
			.appendTo(document.body).wijbarchart(o);
}

var simpleOptions = {
	seriesList: [{
		label: "1800",
		legendEntry: true,
		data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [107, 31, 635, 203] }
	}, {
		label: "1900",
		legendEntry: true,
		data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [133, 156, 947, 408] }
	}, {
		label: "2008",
		legendEntry: true,
		data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [973, 914, 4054, 732] }
	}],
	seriesStyles: [{
		opacity: 0.8, 
		fill: "#537bfc", 
		stroke: "#4a6ee2", 
		"stroke-width": "1"
	}, {
		opacity: 0.8, 
		fill: "#6c8587", 
		stroke: "#617779", 
		"stroke-width": "1"
	}, {
		opacity: 0.8, 
		fill: "#53b9fc", 
		stroke: "#4aa6e2", 
		"stroke-width": "1"
	}],
	hint: {
		enable: false
	}
};

function createSimpleBarchart() {
	return createBarchart(simpleOptions);
}

(function ($) {

	module("wijbarchart: core");

	test("create and destroy", function () {
		var barchart = createBarchart();

		ok(barchart.hasClass('ui-widget wijmo-wijbarchart'), 
			'create:element class created.');
		
		barchart.wijbarchart('destroy');
		ok(!barchart.hasClass('ui-widget wijmo-wijbarchart'), 
			'destroy:element class destroy.');
		
		barchart.remove();
	});

}(jQuery));

