/*
 * wijlist_options.js
 */
(function($){

	module("wijlist:options");

test("selectionMode-single", function(){
		// test disconected element
		var datasourceOptions = getStaticDataSourceOptions();
		var list = $('<div></div>');
		list.height(150);
		list.width(300);
		list.wijlist();
		list.appendTo(document.body);
		list.wijlist('setItems', datasourceOptions);
		list.wijlist('renderList');
		list.wijlist('refreshSuperPanel');
		var ruby = $('li:contains(ruby)',list);
		var wl = list.data('wijmo-wijlist');
		wl.activate(null,ruby.data('item.wijlist'));
		ruby.simulate('click');
		ok(wl.selectedItem.label == 'ruby', '"ruby selected"');
		var python = $('li:contains(python)',list);
		wl.activate(null,python.data('item.wijlist'));
		python.simulate('click');
		ok(wl.selectedItem.label == 'python', '"python selected"');
		list.remove();
	});
	
	test("selectionMode-multiple", function(){
		// test disconected element
		var datasourceOptions = getStaticDataSourceOptions();
		var list = $('<div></div>');
		list.height(150);
		list.width(300);
		list.appendTo(document.body);
		list.wijlist({selectionMode:'multiple'});
		list.wijlist('setItems', datasourceOptions);
		list.wijlist('renderList');
		list.wijlist('refreshSuperPanel');
		var ruby = $('li:contains(ruby)',list);
		var wl = list.data('wijmo-wijlist');
		wl.activate(null,ruby.data('item.wijlist'));
		ruby.simulate('click');
		ok(wl.selectedItems.length == 1 && wl.selectedItems[0].label == 'ruby' , '"ruby selected"');
		var python = $('li:contains(python)',list);
		wl.activate(null,python.data('item.wijlist'));
		python.simulate('click');
		ok(wl.selectedItems.length == 2 && wl.selectedItems[0].label == 'ruby' && wl.selectedItems[1].label == 'python' , '"ruby,python selected"');
		list.remove();
	});

	test("set selectionMode", function () {
		var datasourceOptions = getStaticDataSourceOptions(),
			list = $('<div></div>').appendTo("body");

		list.wijlist({ selectedIndex: 1,  listItems: datasourceOptions});
		ok(list.wijlist("getSelectedItems").value === "java", "the second item is selected.");
		list.wijlist("option", "selectionMode", "multiple");
		ok(list.wijlist("getSelectedItems").length === 0, "after set selectionMode, the selectedItems will clear");
		ok(list.wijlist("option", "selectedIndex").length === 0, "The selectedIndex is empty array.");
		list.wijlist("destroy");

		list.wijlist({ selectedIndex: [1, 3], listItems: datasourceOptions, selectionMode:"multiple" });
		ok(list.wijlist("getSelectedItems").length === 2, "Two items are selected.");
		list.wijlist("option", "selectionMode", "single");
		ok(list.wijlist("getSelectedItems") === null, "after set selectionMode, the selectedItems will clear.");
		ok(list.wijlist("option", "selectedIndex") === -1, "");
		list.remove();
	});

	test("selected", function () {
		// test disconected element
		var datasourceOptions = getStaticDataSourceOptions();
		var list = $('<div></div>');
		list.height(150);
		list.width(300);
		list.wijlist({
			selectionMode:'multiple',
			selected: function (event, data){
				ok(data.item.label == 'ruby', 'selected');
				list.remove();
				//start();
			}
		});
		list.appendTo(document.body);
		list.wijlist('setItems', datasourceOptions);
		list.wijlist('renderList');
		list.wijlist('refreshSuperPanel');
		var ruby = $('li:contains(ruby)',list);
		var wl = list.data('wijmo-wijlist');
		wl.activate(null,ruby.data('item.wijlist'));
		ruby.simulate('click');
		//stop();
	});
	
	test("autoSize:5", function(){
		// test disconected element
		var datasourceOptions = getStaticDataSourceOptions();
		var list = $('<div></div>');
		list.height(150);
		list.width(300);
		list.wijlist({
			autoSize: true,
			maxItemsCount:5
		});
		list.appendTo(document.body);
		list.wijlist('setItems', datasourceOptions);
		list.wijlist('renderList');
		list.wijlist('refreshSuperPanel');
		ok(list.height() === 5 * $(".wijmo-wijlist .wijmo-wijlist-item:first").outerHeight(true), 'height correct'); 
		list.remove();
	});
	
	test("autoSize:false", function(){
		// test disconected element
		var datasourceOptions = getStaticDataSourceOptions();
		var list = $('<div></div>');
		list.height(450);
		list.width(300);
		list.wijlist({
			maxItemsCount:5
		});
		list.appendTo(document.body);
		list.wijlist('setItems', datasourceOptions);
		list.wijlist('renderList');
		list.wijlist('refreshSuperPanel');
		ok(list.height() === 450, 'height correct'); 
        
		var testArray2 = [{
            label: 'Different Array1',
            value: 'Different Array1'
        },{
            label: 'Different Array1',
            value: 'Different Array1'
        },{
            label: 'Different Array2',
            value: 'Different Array2'
        }];
		list.wijlist('setItems', testArray2);
		list.wijlist('renderList');
		list.wijlist('refreshSuperPanel');
		ok(list.height() === 450, 'height correct'); 
		list.remove();
	});

	test("selectedIndex", function () {
		var datasourceOptions = getStaticDataSourceOptions(),
			count = datasourceOptions.length,
			list = $('<div></div>').appendTo("body");

		list.wijlist({
			listItems: datasourceOptions,
			selectedIndex: 2
		});
		// single mode.
		ok(list.wijlist("getSelectedItems").value === "php", "the third item is selected.");
		ok(list.find(".wijmo-wijlist-item:eq(2)").hasClass("wijmo-wijlist-item-selected"), "the item has added selected CSS class.");
		list.wijlist("option", "selectedIndex", 4);
		ok(!list.find(".wijmo-wijlist-item:eq(2)").hasClass("wijmo-wijlist-item-selected"), "The 3rd item is not selected.");
		ok(list.wijlist("getSelectedItems").value === "javascript", "the 5th item is selected.");
		ok(list.find(".wijmo-wijlist-item:eq(4)").hasClass("wijmo-wijlist-item-selected"), "the item has added selected CSS class.");
		list.wijlist("option", "selectedIndex", -1);
		ok(list.wijlist("getSelectedItems") === null, "No Item is selected.");
		ok(list.find(".wijmo-wijlist-item-selected").length === 0, "No item has added selected CSS class.");
		list.remove();

		// multiple mode
		list = $('<div></div>').appendTo("body");
		list.wijlist({
			listItems: datasourceOptions,
			selectionMode: "multiple",
			selectedIndex: 2
		});

		ok(list.wijlist("getSelectedItems")[0].value === "php", "the third item is selected.");
		ok(list.find(".wijmo-wijlist-item:eq(2)").hasClass("wijmo-wijlist-item-selected"), "the item has added selected CSS class.");
		list.wijlist("option", "selectedIndex", [1, 4]);
		ok(list.wijlist("getSelectedItems").length === 2, "two items are selected.");
		ok(list.wijlist("getSelectedItems")[0].value === "java", "the second item is selected.");
		ok(list.wijlist("getSelectedItems")[1].value === "javascript", "the 5th item is selected.");
		list.find(".wijmo-wijlist-item:eq(4)").simulate("mouseover").simulate("click").simulate("mouseout");
		ok(list.wijlist("getSelectedItems").length === 1, "one item is deselected.");
		ok(!list.find(".wijmo-wijlist-item:eq(4)").hasClass("wijmo-wijlist-item-selected"), "the 5th item is not selected.");
		list.wijlist("option", "selectedIndex", []);
		ok(list.wijlist("getSelectedItems").length === 0, "No Item is selected.");
		list.remove();

		// test init without selected items.
		// single mode
		list = $('<div></div>').appendTo("body");
		list.wijlist({
			listItems: datasourceOptions,
			selectionMode: "single"
		});
		ok(list.wijlist("option", "selectedIndex") === -1, "If no item is selected, the selectedIndex is -1");
		list.remove();

		// test init without selected items.
		// multiple mode
		list = $('<div></div>').appendTo("body");
		list.wijlist({
			listItems: datasourceOptions,
			selectionMode: "multiple"
		});
		ok(list.wijlist("option", "selectedIndex").length === 0, "If no item is selected, the selectedIndex is empty array");
		list.remove();

		//test init selected in items.
		// single mode
		datasourceOptions[1].selected = true;
		list = $('<div></div>').appendTo("body");
		list.wijlist({
			listItems: datasourceOptions,
			selectionMode: "single"
		});
		ok(list.wijlist("getSelectedItems").value === "java", "the getSelectedItems returns correct value");
		list.remove();

		//test init selected in items.
		// multiple mode
		datasourceOptions[1].selected = true;
		list = $('<div></div>').appendTo("body");
		list.wijlist({
			listItems: datasourceOptions,
			selectionMode: "multiple"
		});
		ok(list.wijlist("getSelectedItems").length === 1 && list.wijlist("getSelectedItems")[0].value === "java", "the getSelectedItems returns correct value");
		list.remove();

		//test init selected in items.
		// multiple mode, selected more than one item
		datasourceOptions[1].selected = true;
		datasourceOptions[3].selected = true;
		list = $('<div></div>').appendTo("body");
		list.wijlist({
			listItems: datasourceOptions,
			selectionMode: "multiple"
		});
		ok(list.wijlist("getSelectedItems").length === 2 && list.wijlist("getSelectedItems")[0].value === "java" && list.wijlist("getSelectedItems")[1].value === "coldfusion",
			"getSelectedItems method returns correct value");
		list.find(".wijmo-wijlist-item:eq(3)").simulate("mouseover").simulate("click").simulate("mouseout");
		ok(list.wijlist("getSelectedItems").length === 1 && list.wijlist("getSelectedItems")[0].value === "java" ,
			"getSelectedItems method returns correct value");
		list.remove();

		//multiple mode with number selectedIndex
		list = $('<div></div>').appendTo("body");
		list.wijlist({
			listItems: datasourceOptions,
			selectionMode: "multiple",
			selectedIndex: 4
		});
		ok(list.wijlist("option", "selectedIndex")[0] === 4, "the selectedIndex is correct.");
		ok(list.wijlist("getSelectedItems").length === 1 && list.wijlist("getSelectedItems")[0].value === "javascript", "the 5th item is selected");
		ok(list.find(".wijmo-wijlist-item:eq(4)").hasClass("wijmo-wijlist-item-selected"), "the 5th item is added selected CSS class.");
		list.remove();

		// single mode with array selectedIndex.
		list = $('<div></div>').appendTo("body");
		list.wijlist({
			listItems: datasourceOptions,
			selectionMode: "single",
			selectedIndex: [2, 1, 5]
		});
		ok(list.wijlist("option", "selectedIndex") === 2, "the selectedIndex is correct.");
		ok(list.wijlist("getSelectedItems").value === "php", "getSelectedItems method returns correct value");
		ok(list.find(".wijmo-wijlist-item:eq(2)").hasClass("wijmo-wijlist-item-selected"), "the 3rd item is added selected CSS class.");
		ok(list.find(".wijmo-wijlist-item-selected").length === 1, "only one item has added selected CSS class");
		list.remove();

		// set selected index with larger than the item's lengh
		//single mode
		datasourceOptions = getStaticDataSourceOptions();
		list = $('<div></div>').appendTo("body");
		list.wijlist({
			listItems: datasourceOptions,
			selectionMode: "single",
			selectedIndex: count
		});
		ok(list.wijlist("getSelectedItems") === null && list.find(".wijmo-wijlist-item-selected").length === 0, "No items has selected");
		list.remove();

		// set selected index with larger than the item's lengh
		// multiple mode
		datasourceOptions = getStaticDataSourceOptions();
		list = $('<div></div>').appendTo("body");
		list.wijlist({
			listItems: datasourceOptions,
			selectionMode: "multiple",
			selectedIndex: count
		});
		ok(list.wijlist("getSelectedItems").length === 0 && list.find(".wijmo-wijlist-item-selected").length === 0, "No items has selected");
		list.remove();

		datasourceOptions = getStaticDataSourceOptions();
		list = $('<div></div>').appendTo("body");
		list.wijlist({
			listItems: datasourceOptions,
			selectionMode: "multiple",
			selectedIndex: [count]
		});
		ok(list.wijlist("getSelectedItems").length === 0 && list.find(".wijmo-wijlist-item-selected").length === 0, "No items has selected");
		list.remove();

		datasourceOptions = getStaticDataSourceOptions();
		list = $('<div></div>').appendTo("body");
		list.wijlist({
			listItems: datasourceOptions,
			selectionMode: "multiple",
			selectedIndex: [1, count, 4]
		});
		ok(list.wijlist("getSelectedItems").length === 2, "two items has selected");
		ok(list.wijlist("getSelectedItems")[0].value === "java" && list.wijlist("getSelectedItems")[1].value === "javascript", "selected item is correct");
		list.remove();

		datasourceOptions = getStaticDataSourceOptions();
		list = $('<div></div>').appendTo("body");
		list.wijlist({
			listItems: datasourceOptions,
			selectionMode: "single",
			selectedIndex: 3
		});
		list.wijlist({ selectedIndex: count });
		ok(list.wijlist("getSelectedItems") === null, "When invalid value is set, remove the selection.");
		list.remove();

		datasourceOptions = getStaticDataSourceOptions();
		list = $('<div></div>').appendTo("body");
		list.wijlist({
			listItems: datasourceOptions,
			selectionMode: "multiple",
			selectedIndex: [3]
		});
		list.wijlist({ selectedIndex: [count] });
		ok(list.wijlist("getSelectedItems").length === 0, "When set an invalid value, remove the selection.");
		list.remove();

		datasourceOptions = getStaticDataSourceOptions();
		list = $('<div></div>').appendTo("body");
		list.wijlist({
			listItems: datasourceOptions,
			selectionMode: "multiple",
			selectedIndex: [3]
		});
		list.wijlist({ selectedIndex: [count, 5] });
		ok(list.wijlist("getSelectedItems")[0].value === "asp" && list.wijlist("getSelectedItems").length === 1, "When selectedIndex contains invalid value, Only valid values are set.");
		list.remove();

		datasourceOptions = getStaticDataSourceOptions();
		list = $('<div></div>').appendTo("body");
		list.wijlist({
			listItems: datasourceOptions,
			selectionMode: "multiple",
			selectedIndex: [3]
		});
		list.wijlist({ selectedIndex: -1 });
		ok(list.wijlist("getSelectedItems").length === 0 && list.find(".wijmo-wijlist-item-selected").length === 0, "No item has selected");
		list.remove();

		datasourceOptions = getStaticDataSourceOptions();
		list = $('<div></div>').appendTo("body");
		list.wijlist({
			listItems: datasourceOptions,
			selectionMode: "multiple",
			selectedIndex: [3]
		});
		list.wijlist({ selectedIndex: [] });
		ok(list.wijlist("getSelectedItems").length === 0 && list.find(".wijmo-wijlist-item-selected").length === 0, "No item has selected");
		list.remove();
	});
})(jQuery);
