/*
 * wijlist_core.js
 */

function getStaticDataSourceOptions(){
var testArray = [
			{
				label: 'c++',
				value: 'c++'
			}, 
			{
				label: 'java',
				value: 'java'
			}, 
			{
				label: 'php',
				value: 'php'
			}, 
			{
				label: 'coldfusion',
				value: 'coldfusion'
			}, {
				label: 'javascript',
				value: 'javascript'
			}, 
			{
				label: 'asp',
				value: 'asp'
			}, 
			{
				label: 'ruby',
				value: 'ruby'
			},
			{
				label: 'python',
				value: 'python'
			},
			{
				label: 'c',
				value: 'c'
			},
			{
				label: 'scala',
				value: 'scala'
			},
			{
				label: 'groovy',
				value: 'groovy'
			},
			{
				label: 'haskell',
				value: 'haskell'
			},
			{
				label: 'perl',
				value: 'perl'
			}
		];
	return testArray
}

function getStaticDataSourceOptions_NumberVlaue(){
	var testArray = [
				{
					label: 'c++',
					value: '100'
				}, 
				{
					label: 'java',
					value: '200'
				}, 
				{
					label: 'php',
					value: '300'
				}, 
				{
					label: 'coldfusion',
					value: '400'
				}, {
					label: 'javascript',
					value: '500'
				}, 
				{
					label: 'asp',
					value: '600'
				}, 
				{
					label: 'ruby',
					value: '700'
				},
				{
					label: 'python',
					value: '800'
				},
				{
					label: 'c',
					value: '900'
				},
				{
					label: 'scala',
					value: '101'
				},
				{
					label: 'groovy',
					value: '102'
				},
				{
					label: 'haskell',
					value: '103'
				},
				{
					label: 'perl',
					value: '104'
				}
			];
		return testArray
	}
(function($){

	module("wijlist: core");
	
	test("create and destroy", function(){
		// test disconected element
		var disconected = $('<div></div>').wijlist();
		ok(disconected.hasClass('wijmo-wijlist') && disconected.find('ul').hasClass('wijmo-wijlist-ul'), 'list element test');
		disconected.wijlist('destroy');
		ok(disconected.get(0).className == '' && disconected.html() == '', 'no CSS class');
	});
	
	
})(jQuery);

