/*
 * wijlist_methods.js
 */
(function($){

	module("wijlist: methods");
	
	test("load, renderList, refreshSuperPanel-static data", function(){
		// test disconected element
		var datasourceOptions = getStaticDataSourceOptions();
		var list = $('<div></div>');
		list.height(150);
		list.width(300);
		list.wijlist();
		list.appendTo(document.body);
		list.wijlist('setItems', datasourceOptions);
		list.wijlist('renderList');
		list.wijlist('refreshSuperPanel');
		var wi = list.data('wijmo-wijlist');
		ok(wi.items.length == datasourceOptions.length && wi.element.find('.wijmo-wijlist-item').size() == wi.items.length , 'items correct.')
		var item = $('li:contains(ruby)',list).data('item.wijlist');
		wi.activate(null, item);
		ok(item.element.hasClass("ui-state-hover") && item.element.attr("id") == "wijmo-wijlistitem-active", 'activate item');
		wi.next();
		var python = $('li:contains(python)',list);
		ok(python.hasClass("ui-state-hover") && python.attr("id") == "wijmo-wijlistitem-active", 'moved from ruby to python');
		wi.previous();
		ok(item.element.hasClass("ui-state-hover") && item.element.attr("id") == "wijmo-wijlistitem-active", 'activate item');
		wi.deactivate();
		ok(!item.element.hasClass("ui-state-hover") && !(item.element.attr("id") == "wijmo-wijlistitem-active"), 'deactivate item');
		list.remove();
	});
	
	test("nextPage-previousPage", function(){
		// test disconected element
		var datasourceOptions = getStaticDataSourceOptions();
		var list = $('<div></div>');
		list.height(150);
		list.width(300);
		list.wijlist();
		list.appendTo(document.body);
		list.wijlist('setItems', datasourceOptions);
		list.wijlist('renderList');
		list.wijlist('refreshSuperPanel');
		list.wijlist('nextPage');
		stop();
		setTimeout(function (){
			ok($('.wijmo-wijsuperpanel-templateouterwrapper').css('top') == '-150px', 'next page: known failed on IE.');
			list.wijlist('previousPage');
			setTimeout(function (){
				ok($('.wijmo-wijsuperpanel-templateouterwrapper').css('top') == '0px', 'previous page');
				list.remove();
				start();
			}, 500);
		},500);
	});

	test("selectItems", function () {
		var activeClass = "wijmo-wijlist-item-selected ui-state-active";
		var datasourceOptions = getStaticDataSourceOptions();
		var list = $('<div></div>');
		list.height(150);
		list.width(300);
		list.wijlist();
		list.appendTo(document.body);
		list.wijlist('setItems', datasourceOptions);
		list.wijlist('renderList');
		list.wijlist('refreshSuperPanel');
		list.wijlist("selectItems", 2, null, true);
		var ul = list.find("ul");
		var php = ul.find(">li:contains('php')");
		ok(php.hasClass(activeClass), "Php is selected.");
		list.wijlist("unselectItems", 2);
		ok(!php.hasClass(activeClass), "Php is not selected.");
		list.wijlist("selectItems", "java");
		var java = ul.find(">li:contains('java')");
		ok(java.hasClass(activeClass), "Java is selected.");
		list.wijlist("unselectItems", "java");
		ok(!java.hasClass(activeClass), "Java is not selected.");
		list.wijlist({selectionMode:"multiple"});
		list.wijlist("selectItems", [1,2], null, true);
		ok(java.hasClass(activeClass) && php.hasClass(activeClass), "Java and php are selected.");
		list.wijlist("unselectItems", [1, 2]);
		ok(!java.hasClass(activeClass) && !php.hasClass(activeClass), "Java and php are not selected.");
		list.wijlist("selectItems", ["java", "php"]);
		ok(java.hasClass(activeClass) && php.hasClass(activeClass), "Java and php are selected.");
		list.wijlist("unselectItems", ["java", "php"]);
		ok(java.hasClass(activeClass) && php.hasClass(activeClass), "Java and php are not unselected.");
		list.wijlist("unselectItems", [1, 2]);
		ok(!java.hasClass(activeClass) && !php.hasClass(activeClass), "Java and php are not unselected.");
		list.remove();
	});
	
	
	test("findIndexByLabel", function () {
		var activeClass = "wijmo-wijlist-item-selected ui-state-active";
		var datasourceOptions = getStaticDataSourceOptions();
		var list = $('<div></div>');
		list.height(150);
		list.width(300);
		list.wijlist();
		list.appendTo(document.body);
		list.wijlist('setItems', datasourceOptions);
		list.wijlist('renderList');
		list.wijlist('refreshSuperPanel');
		list.wijlist("selectItems", 2);
		ok(list.wijlist("findIndexByLabel", "vvvv") === -1, "the items don't exist");
		ok(list.wijlist("findIndexByLabel", "c++") === 0, "Find the 'c++' item");
		ok(list.wijlist("findIndexByLabel", "perl") === 12, "Find the 'perl' item");
		list.remove();
	});
	
	test("selectItemsForNumberValue", function () {
		var activeClass = "wijmo-wijlist-item-selected ui-state-active";
		var datasourceOptions = getStaticDataSourceOptions_NumberVlaue();
		var list = $('<div></div>');
		list.height(150);
		list.width(300);
		list.wijlist();
		list.appendTo(document.body);
		list.wijlist('setItems', datasourceOptions);
		list.wijlist('renderList');
		list.wijlist('refreshSuperPanel');
		list.wijlist("selectItems", "300");
		var ul = list.find("ul");
		var php = ul.find(">li:contains('php')");
		ok(php.hasClass(activeClass), "Php is selected.");
		list.wijlist("unselectItems", 2);
		ok(!php.hasClass(activeClass), "Php is not selected.");
		list.wijlist("selectItems", "200");
		var java = ul.find(">li:contains('java')");
		ok(java.hasClass(activeClass), "Java is selected.");
		list.wijlist("unselectItems", "java");
		ok(!java.hasClass(activeClass), "Java is not selected.");
		list.wijlist({selectionMode:"multiple"});
		list.wijlist("selectItems", ["200", "300"]);
		ok(java.hasClass(activeClass) && php.hasClass(activeClass), "Java and php are selected.");
		list.wijlist("unselectItems", ["200", "300"]);
		ok(java.hasClass(activeClass) && php.hasClass(activeClass), "Java and php are not selected.");
		list.wijlist("unselectItems", [1, 2]);
		ok(!java.hasClass(activeClass) && !php.hasClass(activeClass), "Java and php are not selected.");
		list.remove();
	});

	test("getSelectedItems", function () {
		var datasourceOptions = getStaticDataSourceOptions_NumberVlaue(),
			list;
		// single mode, no item is selected.
		list = $('<div></div>').appendTo("body").wijlist({ listItems: datasourceOptions });
		ok(list.wijlist("getSelectedItems") === null, "No item is selected, this method returns null");
		list.remove();
		// multiple mode, no item is selected.
		datasourceOptions = getStaticDataSourceOptions_NumberVlaue()
		list.wijlist({ selectionMode: "multiple", listItems: datasourceOptions });
		ok(list.wijlist("getSelectedItems").length === 0, "No item is selected, this method returns null");
		list.remove();
		// single mode, selected set in items.
		datasourceOptions = getStaticDataSourceOptions_NumberVlaue()
		datasourceOptions[0].selected = true;
		list = $('<div></div>').appendTo("body").wijlist({ listItems: datasourceOptions });
		ok(list.wijlist("getSelectedItems").value === "100", "the method returns right value");
		list.find(".wijmo-wijlist-item:eq(5)").simulate("mouseover").simulate("click").simulate("mouseout");
		ok(list.wijlist("getSelectedItems").value === "600", "the method returns right value");
		list.wijlist("destroy");
		// multiple mode, selected set in items
		datasourceOptions = getStaticDataSourceOptions_NumberVlaue()
		datasourceOptions[0].selected = true;
		list = $('<div></div>').appendTo("body").wijlist({ listItems: datasourceOptions, selectionMode: "multiple" });
		ok(list.wijlist("getSelectedItems").length === 1 && list.wijlist("getSelectedItems")[0].value === "100", "the method returns right value");
		list.wijlist("destroy");

		// single mode, select item by selectedIndex.
		datasourceOptions = getStaticDataSourceOptions_NumberVlaue()
		list.wijlist({ listItems: datasourceOptions, selectedIndex: 5 });
		ok(list.wijlist("getSelectedItems").value === "600", "the method returns right value");
		list.wijlist("destroy");

		// multiple mode, select item by selectedIndex
		datasourceOptions = getStaticDataSourceOptions_NumberVlaue()
		list.wijlist({ listItems: datasourceOptions, selectedIndex: [1, 5], selectionMode: "multiple" });
		ok(list.wijlist("getSelectedItems").length === 2 && list.wijlist("getSelectedItems")[0].value === "200"
			&& list.wijlist("getSelectedItems")[1].value === "600", "the method returns right value");
		list.wijlist("destroy");

		// single mode, select item by mouse click.
		datasourceOptions = getStaticDataSourceOptions_NumberVlaue()
		list.wijlist({listItems: datasourceOptions });
		list.find(".wijmo-wijlist-item:eq(5)").simulate("mouseover").simulate("click").simulate("mouseout");
		ok(list.wijlist("getSelectedItems").value === "600", "the method returns right value");
		list.wijlist("destroy");

		// multiple mode, select item by mouse click.
		datasourceOptions = getStaticDataSourceOptions_NumberVlaue()
		list.wijlist({ selectionMode: "multiple", listItems: datasourceOptions });
		ok(list.wijlist("getSelectedItems").length === 0, "No items are selected.");
		list.find(".wijmo-wijlist-item:eq(5)").simulate("mouseover").simulate("click").simulate("mouseout");
		list.find(".wijmo-wijlist-item:eq(7)").simulate("mouseover").simulate("click").simulate("mouseout");
		ok(list.wijlist("getSelectedItems").length === 2 && list.wijlist("getSelectedItems")[1].value === "800", "after change the selectionMode, the method returns right value");
		list.find(".wijmo-wijlist-item:eq(7)").simulate("mouseover").simulate("click").simulate("mouseout");
		ok(list.wijlist("getSelectedItems").length === 1 && list.wijlist("getSelectedItems")[0].value === "600", "The method returns right value");
		list.remove();
	});
})(jQuery);
