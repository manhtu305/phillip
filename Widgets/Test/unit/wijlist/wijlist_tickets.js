/*
 * wijlist_tickets.js
 */
(function($) {

module("wijlist: tickets");

	//#37228
	test("37228:large change is wrong in refreshSuperPanel method when there're header and footer in superpanel", function () {
		// ticket test
		var datasourceOptions = [
			{ label: 'a', value: 'a' }, 
			{ label: 'b', value: 'b' }, 
			{ label: 'c', value: 'c' }, 
			{ label: 'd', value: 'd' }, 
			{ label: 'e', value: 'e' }, 
			{ label: 'f', value: 'f' }
		];
		var list = $('<div><div class="wijmo-wijsuperpanel-header" style="height:20px;"></div>'
			+ '<div class="wijmo-wijsuperpanel-footer" style="height:20px;"></div></div>');
		list.height(150);
		list.width(300);
		list.wijlist();
		list.appendTo(document.body);
		list.wijlist('setItems', datasourceOptions);
		list.wijlist('renderList');
		list.find(".wijmo-wijlist-item").height(20);
		list.wijlist('refreshSuperPanel');
		var largeChange = list.data("wijmo-wijsuperpanel").options.vScroller.scrollLargeChange;
		ok(largeChange < 70, 'large change is minused superpanel header and superpanel footer.');
		list.remove();
	});

	test("#39687", function () {
	    var data = [{ label: 'test1', value: 1 }, { label: 'test2', value: 2 }],
	        list = $("<div>").appendTo("body"),
	        list1 = $("<div>").appendTo("body"), li1, li2;
	    list.wijlist({ listItems: data });
	    list1.wijlist({ listItems: data });
	    li1 = list.find(".wijmo-wijlist-item:first");
	    li2 = list1.find(".wijmo-wijlist-item:first");
	    ok(li1.data("item.wijlist").element[0] != li2.data("item.wijlist").element[0], "The first wijlist's li element data is not the same as the seconds.");
	    list.remove();
	    list1.remove();
	});

	test("#24127", function () {
	    var datasourceOptions = [
			{ label: 'Steven<&lt;>&gt;"\\\'&', value: 'Steven' }
	    ];
	    var list = $('<div><div class="wijmo-wijsuperpanel-header" style="height:20px;"></div>'
			+ '<div class="wijmo-wijsuperpanel-footer" style="height:20px;"></div></div>').appendTo(document.body);
	    list.wijlist({ listItems: datasourceOptions });
	    ok(list.find("li").text() === "Steven<&lt;>&gt;\"\\\'&", "Some html characters should be displayed correctly.");
	    list.remove();
	});

	test("#40482", function () {
	    var datasourceOptions = [
			{ label: 'TestItem', templateHtml: "<input type='checkbox' /> this is Test Item", value: 'Steven' }
	    ],
	        list = $("<div>").appendTo("body");
	    list.wijlist({ listItems: datasourceOptions });
	    ok(list.find(".wijmo-wijlist-item:first").text().replace(/\s/,"") == "this is Test Item", "The template item is rendered correctly");
	    list.remove();
	});

	test("#41676", function () {
	    var data = [{ label: 'test1', value: 1 }, { label: 'test2', value: 2 }],
	        list = $("<div>").appendTo("body"),
	        li1,event = { target: null },i = 0;
	    list.wijlist({ listItems: data });
	    li1 = list.find(".wijmo-wijlist-item:first");
	    list.wijlist('next', null);
	    list.wijlist('next', null);
	    list.wijlist('next', null);
	    var event = { target: null };
	    try {
	        list.wijlist('select', event);
	    }catch(e){
	        i = 1;
	    }	    
	    ok(i === 0, "No exception throwed!");
	    list.remove();
	});

	test("#48091", function () {
		var testArray1, testArray2,
            container = $("<div id='con48091'></div>").appendTo("body"),
            list, listEmptyHeight, listHeight;
		testArray1 = [{
			label: 'c++',
			value: 'c++'
		}];
		testArray2 = [{
			label: 'c++',
			value: 'c++'
		}, {
			label: 'java',
			value: 'java'
		}];
		list = $("<div id='list'>").appendTo(container);
		list.wijlist();
		list.wijlist('renderList');
		list.wijlist('refreshSuperPanel');
		listEmptyHeight = list.height();

		list.wijlist('setItems', testArray1);
		list.wijlist('renderList');
		list.wijlist('refreshSuperPanel');
		listHeight = list.height();
		ok(listHeight > listEmptyHeight, "Wijlist renders height correctly ! ");

		list.wijlist('setItems', testArray2);
		list.wijlist('renderList');
		list.wijlist('refreshSuperPanel');
		ok(list.height() === listHeight, "Wijlist renders height correctly ! ");

		list.remove();
		container.remove();
	});

	test("52105", function () {
		var testArray1 = [
				{
					label: 'c++',
					value: 'c++',
				    templateHtml: '<div><input name="Button1" value="Button" id="Button1" type="submit"></div>;'
				}, {
					label: 'java',
					value: 'java',
				    templateHtml: '<div><input name="Button2" value="Button" id="Button2" type="submit"></div>'
				}],
            container = $("<div id='con52105'></div>").appendTo("body"),
            list, filteredItem;

		list = $("<div id='list'>").appendTo(container);
		list.wijlist({ listItems: testArray1 });
		list.wijlist('filterItems', 'c', true, true);
		filteredItem = $(".wijmo-wijlist-ul").children(":visible");

		ok(filteredItem.children().length > 0, "The template html of the list item has been decoded after filtering.");

		list.remove();
		container.remove();
	});

	test("#50273", function () {
		var testArray1 = [
				{
					label: 'c++',
					value: 'c++',
					selected: true
				}, {
					label: 'java',
					value: 'java'
				}],
			testArray2 = [
				{
					label: 'C#',
					value: 'C#'
				}, {
					label: 'javascript',
					value: 'javascript'
				}],
			container = $("<div id='con50273'></div>").appendTo("body"),
            list,
			className;

		list = $("<div id='list'>").appendTo(container);
		list.wijlist({ listItems: testArray1 });
		className = $(".wijmo-wijlist-ul").children("li:first").attr("class");
		ok(className.indexOf("wijmo-wijlist-item-selected") !== -1
			&& className.indexOf("ui-state-active") !== -1, "The first list item is set as selected state.");

		list.wijlist('setItems', testArray2);
		list.wijlist('renderList');
		className = $(".wijmo-wijlist-ul").children("li:first").attr("class");
		ok(className.indexOf("wijmo-wijlist-item-selected") === -1
			&& className.indexOf("ui-state-active") === -1, "The first list item isn't set as selected state after changing the items.");

		list.remove();
		container.remove();
	});


	test("56210", function () {
		var divitem = "<div id='tempDIV' style='font-weight: bold'><span>ITEM1 with div tag and bold style</span></div>",
            container = $("<div id='con56210'></div>").appendTo("body"),
            list = $("<div id='list56210'>").appendTo(container),
            testArray = [{
            	templateHtml: divitem,
            	label: 'label1',
            	value: 'Id1'
            }], templateDIV;
		list.wijlist({
			listItems: testArray,
		});
		templateDIV = list.find("ul > li").find("#tempDIV");
		ok(templateDIV.length > 0, "Template item has added !");
		list.remove();
		container.remove();
	});

	test("#67767", function () {
		var container = $("<div id='con67767'></div>").appendTo("body"),
			list = $("<div id='list67767'>").wijlist().appendTo(container);

		list.wijlist("setItems", null);
		ok(true, "If set null to the wijlist with setItems funtion, it won't throw errors");

		list.wijlist("setItems", undefined);
		ok(true, "If set nudefined to the wijlist with setItems funtion, it won't throw errors");

		list.remove();
		container.remove();
	});

})(jQuery);
