/*
* wijlist_defaults.js
*/
var wijlist_defaults = {
	    /// <summary>
	    /// wijdataview to which this wijlist is bound.
	    /// Default: null.
	    /// Type: wijdataview
	    /// </summary>
	    /// <remarks>
	    /// This option is used if this wijlist is bound to a wijdataview.
	    /// In that case, you can also specify a mapping option to select the properties to bind to,
	    /// and the listItems option returns an array of objects containing value and label property values determined by that mapping.
	    /// </remarks>
	    dataSource: null,
    /// <summary>
    /// An array that specifies the listItem collections of wijlist.
    /// Example: listItems: [{label: "label1", value: "value1"},
    ///                  {label: "label2", value: "value2"},
    ///                  {label: "label3", value: "value3"}]
    /// Default: [].
    /// Type: Array.
    /// </summary>
    listItems: [],
    /// <summary>
    /// Select event handler of wijList. A function will be called when any item in the list is selected.
    /// Default: null.
    /// Type: Function.
    /// </summary>
    /// <param name="e" type="eventObj">
    /// Event Object of the event.
    ///	</param>
    /// <param name="data" type="Object">
    /// By data.item to obtain the item selected. 
    /// By data.item.element to obtain the li DOM element selected.
    ///	</param>
    selected: null,
    /// <summary>
    /// A value indicates the selection mode of wijList.
    /// Default: 'single'.
    /// Type: String.
    /// </summary>
    /// <remarks>
    /// Options are 'single' and 'multiple'. This options should not be set again after initialization.
    /// </remarks>
    selectionMode: 'single',
    /// <summary>
    /// A value determines whether to auto-size wijList.
    /// Default: false.
    /// Type: String.
    /// </summary>
    autoSize: false,
    /// <summary>
    /// Max item count to display if auto size if true.
    /// Default: 5.
    /// Type: Number.
    /// </summary>
    maxItemsCount: 5,
    /// <summary>
    /// A value determines whether to add ui-state-hover class to list item when mouse enters.
    /// Default: true.
    /// Type: Boolean.
    /// </summary>
    addHoverItemClass: true,
    /// <summary>
    /// A hash value sets to supepanel options when superpanel is created.
    /// Default: Object.
    /// Type: null.
    /// </summary>
    superPanelOptions: null,
    /// <summary>
    /// A value indicating whether wijlist is disabled.
    /// Default: false.
    /// Type: Boolean.
    /// </summary>
    disabled: false,
    /// <summary>
    /// A function called before a item is focus.
    /// Default: Function.
    /// Type: null.
    /// </summary>
    /// <param name="event" type="EventObject">
    /// event object passed in to activate method.
    ///	</param>
    /// <param name="item" type="Object">
    /// The list item to be activated.
    ///	</param>
    /// <returns>
    /// returns false to cancel item focusing.
    /// </returns>
    focusing: null,
    /// <summary>
    /// A function called before a item is focus.
    /// Default: Function.
    /// Type: null.
    /// </summary>
    /// <param name="event" type="EventObject">
    /// event object passed in to activate method.
    ///	</param>
    /// <param name="item" type="Object">
    /// The list item to be activated.
    ///	</param>
    focus: null,
    /// <summary>
    /// A function called when a item loses focus.
    /// Default: Function.
    /// Type: null.
    /// </summary>
    /// <param name="event" type="EventObject">
    /// event object passed in to activate method.
    ///	</param>
    /// <param name="item" type="Object">
    /// The list item.
    ///	</param>
    blur: null,
    /// <summary>
    /// A function called when a item is selectd.
    /// Default: Function.
    /// Type: null.
    /// </summary>
    /// <param name="event" type="EventObject">
    /// event object passed in to activate method.
    ///	</param>
    /// <param name="data" type="Object">
    /// {
    ///  item: item,// selected item
    ///  previousItem: previous, // previously selected item.
    ///  data: data // data passed in from select method. 
    /// }
    ///	</param>
    selected: null,
    /// <summary>
    /// A function called before a item is rendered.
    /// Default: Function.
    /// Type: null.
    /// </summary>
    /// <param name="event" type="EventObject">
    /// event object with this event.
    ///	</param>
    /// <param name="item" type="Object">
    /// item to be rendered.
    /// item.element: LI element with this item.
    /// item.list: wijlist instance.
    /// item.label: label of item.
    /// item.value: value of item.
    /// item.text: could be set in handler to override rendered label of item.
    ///	</param>
    itemRendering: null,
    /// <summary>
    /// A function called after a item is rendered.
    /// Default: Function.
    /// Type: null.
    /// </summary>
    /// <param name="event" type="EventObject">
    /// event object with this event.
    ///	</param>
    /// <param name="item" type="Object">
    /// item rendered.
    ///	</param>
    itemRendered: null,
    /** The added event is fired after adding item in addItem method.
     * @event
     * @dataKey {object} item By added item and index.
     */
    added:null,
    /// <summary>
    /// A function called after list is rendered.
    /// Default: Function.
    /// Type: null.
    /// </summary>
    /// <param name="event" type="EventObject">
    /// event object with this event.
    ///	</param>
    /// <param name="list" type="Object">
    /// list rendered.
    ///	</param>
    listRendered: null,
    wijCSS: $.extend({	    
    listul: "wijmo-wijlist-ul",
    listItem: "wijmo-wijlist-item"         
	}, $.wijmo.wijCSS),
    wijMobileCSS:{	            
                header: "ui-header ui-bar-a",
	            content: "ui-body-b",
	            stateDefault: "ui-btn ui-btn-b",
	            stateHover: "ui-btn-up-b",
	            stateActive: "ui-btn-down-b"
	},
	initSelector: ":jqmData(role='wijlist')",
    create: null,
    keepHightlightOnMouseLeave: false,
    selectedIndex: -1
};

commonWidgetTests('wijlist', {
    defaults: wijlist_defaults
});
