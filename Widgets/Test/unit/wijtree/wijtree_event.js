﻿/*
* wijtree_events.js
*/
(function ($) {
	var el;
	module("wijtree: events");

	test("nodeClick", function () {
		expect(2);
		el = createSimpleTree();
		var flag = 0;
		var flag2 = 0;
		el.wijtree({
			nodeClick: function (e, ui) {
				flag++;
			},
			nodeFocus: function(e, ui) {
				flag2++;
			},
		});
		el.find(">li:eq(0) a:first").trigger('click');
		ok(flag == 1, 'node be clicked!');
		ok(flag2 == 1, 'node be focused!');
		el.remove();
	});

	test("selectedNodeChanged", function () {
		expect(1);
		el = createSimpleTree();
		var flag = 0;
		el.wijtree({
			selectedNodeChanged: function (e, ui) {
				flag++;
			}
		});
		el.find(">li:eq(0) a:first").trigger('click');
		ok(flag == 1, 'selectedNode changed');
		el.remove();
	});

	test("nodeMouseOver", function () {
		expect(1);
		el = createSimpleTree();
		var flag = 0;
		el.wijtree({
			nodeMouseOver: function (e, ui) {
				flag++;
			}
		});

		el.find(">li:eq(0) span.wijmo-wijtree-inner").simulate('mouseover');
		ok(flag > 0, 'mouseover node');
		el.remove();
	});

	test("nodeMouseOut", function () {
		expect(1);
		el = createSimpleTree();
		var flag = 0;
		el.wijtree({
			nodeMouseOut: function (e, ui) {
				flag++;
			}
		});

		el.find(">li:eq(0) span.wijmo-wijtree-inner").simulate('mouseover');
		el.find(">li:eq(0) span.wijmo-wijtree-inner").simulate('mouseout');
		ok(flag == 1, 'mouseout node');
		el.remove();
	});

	test("nodeExpanded", function () {
		expect(1); //2 assertions
		el = createSimpleTree();
		var flag = 0;
		el.wijtree({
			nodeExpanded: function (e, ui) {
				flag++;
			}
		});

		el.find(">li:eq(0)").wijtreenode('expand');

		setTimeout(function () {
			start();
			ok(flag == 1, 'node be expanded');
				el.remove();
		}, 500);
		stop();
	});

	test("nodeCollapse", function () {
		expect(1);
		var flag = 0;
		el.wijtree({
			nodeCollapsed: function (e, ui) {
				flag++;
			}
		});

		el.find(">li:eq(0)").wijtreenode('collapse');

		setTimeout(function () {
			ok(true, 'node be collapsed');
			start();
			el.remove();
		}, 200);
		stop();
	});

	test("nodeDragStarted & nodeDragging & nodeDropped", function () {
		var dstart = 0, drop = 0, drag = 0,
		ele = createTree({
			allowDrop: true,
			allowDrag: true,
			nodeDragStarted: function (e, ui) {
				dstart++;
			},
			nodeDragging: function (e, ui) {
				drag++;
			},
			nodeDropped: function (e, ui) {
				drop++;
			}
		});

		setTimeout(function () {
		    if (!$.browser.msie) { //can't simulate darg on IE after jquery.simulate.js updated!

		        ok(dstart > 0, "true");
		        ok(drag > 0, "true");
		    }
			//ok(drop > 0, "true");//issues: can't simulate drop node to another node
			start();
			ele.remove();
		}, 2000);

		ele.find(":wijmo-wijtreenode:eq(0)>div>span a").trigger("mousedown").simulate("drag", { dx: -10, dy: 20 });
		stop();
	});

	test("nodeExpanding & nodeCollapsing", function () {
		var dstart = 0, drag = 0,
		ele = createTree({
			allowDrop: true,
			allowDrag: true,
			nodeExpanding: function (e, ui) {
				dstart++;
			},
			nodeCollapsing: function (e, ui) {
				drag++;
			}
		});

		setTimeout(function () {
			ok(dstart > 0, "true");

			ele.find(":wijmo-wijtreenode:eq(0)").wijtreenode("collapse");
			setTimeout(function () {
				ok(drag > 0, "true");
				//ok(drop > 0, "true");//issues: can't simulate drop node to another node
				start();
				ele.remove();
			}, 1000);
		}, 1000);

		ele.find(":wijmo-wijtreenode:eq(0)").wijtreenode("expand");
		stop();
	});

	test("nodeCheckChanging & nodeCheckChanged",function() {
		var changingFlag = 0, changedFlag = 0, node, checkbox;

		el = createSimpleTree({
			showCheckBoxes: true,
			nodeCheckChanging: function (e, ui) {
				changingFlag++;
			},
			nodeCheckChanged: function (e, ui) {
				changedFlag++;
			}
		});
		node = el.children("li:first");
		checkbox = $(".wijmo-checkbox", node.children(".wijmo-wijtree-node"));
		checkbox.simulate("click");
		
		equal(changingFlag, 1, "nodeCheckChanging event is fired.");
		equal(changedFlag, 1, "nodeCheckChanged event is fired.");
		equal(node.wijtreenode("option", "checked"), true, "tree node is checked");

		el.remove();

		el = createSimpleTree({
			showCheckBoxes: true,
			nodeCheckChanging: function (e, ui) {
				changingFlag++;
				return false;
			},
			nodeCheckChanged: function (e, ui) {
				changedFlag++;
			}
		});
		node = el.children("li:first");
		checkbox = $(".wijmo-checkbox", node.children(".wijmo-wijtree-node"));
		checkbox.simulate("click");
		
		equal(changingFlag, 2, "nodeCheckChanging event is fired.");
		equal(changedFlag, 1, "nodeCheckChanged event is not fired.");
		equal(node.wijtreenode("option", "checked"), false, "tree node is not checked");

		el.remove();
	});

})(jQuery);
