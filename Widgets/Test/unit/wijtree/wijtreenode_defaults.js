﻿/*
* wijtreenode_defaults.js
*/


commonWidgetTests('wijtreenode', {
    defaults: {
        /// <summary>
        /// Selector option for auto self initialization. 
        ///	This option is internal.
        /// </summary>
        initSelector: ":jqmData(role='wijtreenode')",
        wijCSS: $.extend({ wijtreeInner: "" }, $.wijmo.wijCSS),
        accessKey: "",
        treeClass: "wijmo-wijtree",
        checked: false,

        nodes: null,

        create: null,

        collapsedIconClass: "",

        disabled: false,

        expanded: false,

        expandedIconClass: "",

        itemIconClass: "",

        navigateUrl: "",

        selected: false,

        text: "",

        toolTip: "",

        hasChildren: false,

        wijMobileCSS: {
            header: "ui-header ui-bar-a",
            content: "ui-body-b",
            stateDefault: "ui-btn ui-btn-b",
            stateHover: "ui-btn-down-c",
            stateActive: "ui-btn-down-b"
        },

        params: {},

        allowDrag: null,

        allowDrop: null
    }
});
