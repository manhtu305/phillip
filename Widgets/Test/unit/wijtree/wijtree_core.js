﻿/*
* wijtree_core.js create & destroy
*/

function createTree(o) {
	var htmlText = '';
	htmlText += '<ul id="tree">';
	htmlText += '    <li><a>Folder 1</a>';
	htmlText += '        <ul>';
	htmlText += '            <li><a>Folder 1.3</a></li>';
	htmlText += '            <li><a>Folder 1.1 Really Long tree Item</a></li>';
	htmlText += '            <li><a>Folder 1.5</a>';
	htmlText += '                <ul>';
	htmlText += '                    <li><a>Folder 1.5.1</a></li>';
	htmlText += '                </ul>';
	htmlText += '            </li>';
	htmlText += '        </ul>';
	htmlText += '    </li>';
	htmlText += '    <li><a href="#">Folder 2</a>';
	htmlText += '        <ul>';
	htmlText += '            <li><a>Folder 2.1</a></li>';
	htmlText += '        </ul>';
	htmlText += '    </li>';
	htmlText += '    <li><a href="#">Folder 3</a>';
	htmlText += '        <ul>';
	htmlText += '            <li><a>Folder 3.3</a></li>';
	htmlText += '            <li><a>Folder 3.4</a></li>';
	htmlText += '        </ul>';
	htmlText += '    </li>';
	htmlText += '</ul>';
	$.wijmo.wijtree.prototype.options.nodes = [];
	return $(htmlText).appendTo(document.body).wijtree(o);
}

function createSimpleTree(o) {
	var htmlText = '';
	htmlText += '<ul id="tree">';
	htmlText += '    <li><a>Folder 1</a>';
	htmlText += '        <ul>';
	htmlText += '            <li><a>Folder 1.3</a></li>';
	htmlText += '        </ul>';
	htmlText += '    </li>';
	htmlText += '</ul>';

	return $(htmlText).appendTo(document.body).wijtree(o);
}

function createTreeWithLotsNodes() {
    var opts = { nodes: [] }, htmlText = '<ul id="tree"></ul>';
    for (var i = 0; i < 100; i++) {
        opts.nodes[i] = { text: "node" + i, nodes: [] };
        for (var j = 0; j < 10; j++) {
            opts.nodes[i].nodes[j] = { text: "node" + i + "." + j };
        }
    }
    return $(htmlText).appendTo(document.body).wijtree(opts);
}

function createEmptyTree(o) {
    var htmlText = '<ul id="tree"></ul>';

	return $(htmlText).appendTo(document.body).wijtree(o);
}

function getTreeNode(tree, idx) {
	return tree.is(":wijmo-wijtree") ? tree.find('>li:eq(' + idx + ')') : tree.find('>ul>li:eq(' + idx + ')'); //rootnode
}

function getExactNode(t, h) {//set animation to null
	var selector = '', level = h.split(',');
	$.each(level, function (i, n) {
		if (i == 0) {
			selector += ">li:eq(" + n + ")";
		}
		else {
			t.find(selector).wijtreenode("expand");
			selector += ">ul>li:eq(" + n + ")";
		}
	})
	return t.find(selector);
}


(function($) {

	module("wijtree: core");

	test("create and destroy", function() {
		var $widget = createSimpleTree();

		ok($widget.parent().hasClass('wijmo-wijtree ui-widget ui-widget-content ui-helper-clearfix'), 'create:element class created.');
		ok($widget.hasClass('wijmo-wijtree-list ui-helper-reset'), 'create:nodes container class created.');
		ok(!!$widget.data('nodes'), 'create:widget data created');

		ok($widget.find('>li:eq(0)').hasClass('wijmo-wijtree-item') || $widget.find('>li:eq(0)').hasClass('wijmo-wijtree-parent'), 'node create:element class created.');

		ok($widget.find('>li:eq(0)').has("div.wijmo-wijtree-node"), 'node create:nodebody class created.');

		ok(!!$widget.find('>li:eq(0)').data('owner') && 
		!!$widget.find('>li:eq(0)').wijtreenode('getNodes'), 'node create:node data created.');

		if ($widget.find('>li:eq(0)').hasClass("wijmo-wijtree-parent")) {
			ok($widget.find('>li:eq(0)').has("ul.wijmo-wijtree-child"), 'node create:childNodes class created.');
		}

		$widget.wijtree('destroy');
		ok(!$widget.parent().hasClass('wijmo-wijtree ui-widget ui-widget-content  ui-helper-clearfix'), 'destroy:element class destroy.');
		ok(!$widget.find('>ui:first').hasClass('wijmo-wijtree-list ui-helper-reset'), 'destroy:nodes container class destroy.');
		ok(!$widget.data('nodes'), 'destroy:widget data removed');

		ok(!($widget.find('>li:eq(0)').hasClass('wijmo-wijtree-item') || $widget.find('>li:eq(0)').hasClass('wijmo-wijtree-parent')), 'node destroy:this widget is destroy.');
		ok(!$widget.find('>li:eq(0) div.wijmo-wijtree-node').length, 'node destroy:nodebody class destroy.');

		ok(!$widget.find('>li:eq(0)').data('owner') && 
		!$widget.find('>li:eq(0)').data('nodes'), 'node create:node data destroy.');
		if ($widget.find('>li:eq(0) ul').length) {
			ok($widget.find('>li:eq(0)').has("ul.wijmo-wijtree-child"), 'node destroy:childNodes class destroy.');
		}
		
		$widget.parent().remove();
		
		$.wijmo.wijtree.prototype.options.nodes = [];
	});
})(jQuery);

