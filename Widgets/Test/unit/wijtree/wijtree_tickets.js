﻿/*
* wijtree_tickets.js
*/
(function ($) {

    module("wijtree:tickets");

    //#17929
    test("#17929", function () {
        var $widget = createSimpleTree({ allowDrag: true, showExpandCollapse: false });
        var $node = $widget.find(':wijmo-wijtreenode:eq(0)');
        var ul = $node.parent('ul');  //initialized draggable

        ok(ul.hasClass("wijmo-wijtree-allexpanded"), '17929');
        $widget.remove();
    });

    //#16801

    test("#16801", function () {
        var $widget = createSimpleTree({ allowDrag: true, showExpandCollapse: false });

        $widget.wijtree("option", "disabled", true);
        var $node = $widget.find(':wijmo-wijtreenode:eq(0)');
        var ul = $node.parent('ul');  //initialized draggable

        ok(ul.hasClass("ui-state-disabled"), '17929');
        $widget.remove();
    });

    test("#30223", function () {
        var $widget = createSimpleTree({ nodes: [{ text: "1" }, { text: "2" }, { text: "3" }] });
        var $node = $widget.find(':wijmo-wijtreenode:eq(0)');

        ok($node.length !== 3, '#30223');
        $widget.remove();
    });

    test("#36989", function () {
        var $widget = createSimpleTree({ nodes: [{ text: "1" }, { text: "2" }, { text: "3" }] });

        ok($widget.width() <= $widget.parent().width(), '#36989');
        $widget.remove();
    });

    test("#41139", function () {
        var tree = createSimpleTree({ nodes: [{ text: "1" }, { text: "2" }, { text: "3" }] });
        ok(tree.find("li").length, "The tree contains tree node");
        tree.wijtree({ nodes: [] });
        ok(tree.find("li").length === 0, "All the nodes removed.");

        tree.remove();

        tree = createSimpleTree();
        ok(tree.find("li").length, "The tree contains tree node");
        tree.wijtree({ nodes: [] });
        ok(tree.find("li").length === 0, "All the nodes removed.");

        tree.remove();
    });

    test("#36650", function () {
        var $widget = createSimpleTree({
            nodes: [{ text: "1", expanded: true, nodes: [{ text: "1.1" }] }],
            showCheckBoxes: false,
            expanded: true,
            expandAnimation: { effect: 'blind', duration: 400 },
            collapseAnimation: { effect: 'pulsate', duration: 400 },
            nodeExpanded: function (e, ui) {
                if ($.browser.msie && parseInt($.browser.version) < 9) {
                    ok(ui.$nodes.css("filter") === filterCSS, "The opacity of the sub tree is reset to 1 before showing the subtree.");
                } else {
                    ok(ui.$nodes.css("opacity") === opacityCSS, "The opacity of the sub tree is reset to 1 before showing the subtree.");
                }
                $widget.remove();
                start();
            }
        }),
        $node = $widget.find(':wijmo-wijtreenode:eq(0)'),
        filterCSS = $node.css("filter"),
        opacityCSS = $node.css("opacity");

        $node.wijtreenode("option", "expanded", false);

        setTimeout(function () {
            $node.wijtreenode("option", "expanded", true);
        }, 600);
        stop();
    });

    test("add 'wijmo-wijtree-iconexpanded' class to the expanded treenode", function () {
        var tree = createSimpleTree({ nodes: [{ text: "1", expanded: true, nodes: [{ text: "1.1" }] }, { text: "2", nodes: [{ text: "2.1" }] }] });
        ok(tree.find(".wijmo-state-collapsed").length === 1,
            "'wijmo-state-collapsed' class is added to the collapsed treenode");
        ok(tree.find(".wijmo-state-expanded").length === 1,
            "'wijmo-state-expanded' class is added to the expanded treenode");
        tree.remove();
    });

    test("#48510", function () {
        var $widget = createSimpleTree({ nodes: [{ text: "GL AUDIT", navigateUrl: 'http://www.componentone.com/' }] });
        var $link = $widget.find(':wijmo-wijtreenode:eq(0) a');

        ok($link.length !== 0 && $link.attr("href") === "http://www.componentone.com/", 'Href should be set to the value of navigateUrl');
        $widget.remove();
    });

    if (!$.browser.safari) {
        //"wijmo-wijtree-link" class will not be added to <a> element in safari.
        test("#63313, Test creating tree view with list dom element", function () {
            var $widget = createSimpleTree(),
                $link = $(".wijmo-wijtree-node .wijmo-wijtree-inner a", $widget);
            ok($link.hasClass("wijmo-wijtree-link"), "The link style class has been added to the hyperlink element of the tree view.");
            $widget.remove();
        });

        test("#63313, Test creating tree view with nodes option", function () {
            var $widget = createSimpleTree({ nodes: [{ text: "1", expanded: true, nodes: [{ text: "1.1" }] }, { text: "2", nodes: [{ text: "2.1" }] }] }),
                $link = $(".wijmo-wijtree-node .wijmo-wijtree-inner a", $widget);
            ok($link.hasClass("wijmo-wijtree-link"), "The link style class has been added to the hyperlink element of the tree view.");
            $widget.remove();
        });

        test("#63313, Test adding tree node into tree view", function () {
            var $widget = createEmptyTree(),
                nodes = ["Beverages", "Condiments", "Dairy Products", "Confections"],
                $link;

            $.each(nodes, function (i, val) {
                $widget.wijtree("add", val);
            })

            setTimeout(function () {
                $link = $(".wijmo-wijtree-node .wijmo-wijtree-inner a", $widget);
                ok($link.hasClass("wijmo-wijtree-link"), "The link style class has been added to the hyperlink element of the tree view.");
                $widget.remove();
                start();
            }, 100);
            stop();
        });

        test("#63313, Test adding tree node into treenode", function () {
            var $widget = createSimpleTree({ nodes: [{ text: "node 1", expanded: true, nodes: [{ text: "node 1.1" }] }, { text: "node 2" }] }),
                nodes = ["Beverages", "Condiments", "Dairy Products", "Confections"],
                $link;

            $.each(nodes, function (i, val) {
                $widget.data("nodes")[1].add(val);
            })

            setTimeout(function () {
                $link = $(".wijmo-wijtree-node .wijmo-wijtree-inner a", $widget);
                ok($link.hasClass("wijmo-wijtree-link"), "The link style class has been added to the hyperlink element of the tree view.");
                $widget.remove();
                start();
            }, 100);
            stop();
        });
    }

    test("#64509, Test expanding tree node works fine after re-creating treeview", function () {
        var $widget = createSimpleTree({ nodes: [{ text: "node 1", expanded: true, nodes: [{ text: "node 1.1" }] }, { text: "node 2" }] });

        $widget.wijtree("destroy");
        $widget.wijtree();
        $widget.find(".ui-icon").simulate("click");
        setTimeout(function () {
            ok($widget.children("li:first").find("ul.wijmo-wijtree-list").is(":visible"), "The tree node expands correctly.");
            $widget.remove();
            start();
        }, 200);
        stop();
    });

    test("#69888, 'showExpandCollapse' option does not work when WijTree is bounded to Knockout", function () {
        var tree = createSimpleTree({ nodes: [{ text: "Folder 1", nodes: [{ text: "Folder 1.1" }, { text: "Folder 1.2" }] }], showExpandCollapse: false });

        ok(tree.find("span.ui-icon-triangle-1-e,span.ui-icon-triangle-1-se").length === 0, "showExpandCollapse = false");

        tree.wijtree("option", "showExpandCollapse", true);
        ok(tree.find("span.ui-icon-triangle-1-e,span.ui-icon-triangle-1-se").length > 0, "showExpandCollapse = true");
        tree.remove();
    });

    test("#68756, collapse / expand icon is still shown even through no child item is present in tree node", function () {
        var tree = createSimpleTree({ nodes: [{ text: "Folder 1", nodes: [{ text: "Folder 1.1" }, { text: "Folder 1.2" }] }] }),
            node = tree.wijtree("findNodeByText", "Folder 1");
        node.element.wijtreenode("remove", 0);
        node.element.wijtreenode("remove", 0);
        ok(tree.find(">li:eq(0)").has('span.ui-icon-triangle-1-e').length === 0, "There are no child nodes.");
        tree.remove();
    });

    test("#68795", function () {
        function create68795Tree() {
            var htmlText = '';
            htmlText += '<ul id="tree">';
            htmlText += '    <li><a>Folder 20</a>';
            htmlText += '    </li>';
            htmlText += '    <li><a>Folder 21</a>';
            htmlText += '    </li>';
            htmlText += '</ul>';

            return $(htmlText).appendTo(document.body).wijtree();
        }

        //Because it is hard to simulate drag&drop in unit test and this issue occurs in the method add & remove,
        //here I call add & remove to check it.
        var tree1 = createSimpleTree({ nodes: [{ text: "Folder 10" }, { text: "Folder 11" }] }),
            tree2 = createSimpleTree({ nodes: [{ text: "Folder 20" }, { text: "Folder 21" }] }),
            treenode20 = tree2.wijtree("findNodeByText", "Folder 20").element,
            treenode21 = tree2.wijtree("findNodeByText", "Folder 21").element,
            firstTree1Node, firstTree2Node, childNode;

        tree2.wijtree("remove", treenode20);
        tree1.wijtree("add", treenode20, 0);
        firstTree2Node = getTreeNode(tree2, 0);
        firstTree1Node = getTreeNode(tree1, 0);
        ok(firstTree1Node.find('span:last').html() === 'Folder 20', 'node is added successfully.');
        ok(firstTree2Node.find('span:last').html() !== 'Folder 20', 'node is removed successfully.');
        tree2.wijtree("remove", treenode21);
        tree1.wijtree("add", treenode21, 0);
        firstTree1Node = getTreeNode(tree1, 0);
        firstTree2Node = getTreeNode(tree2, 0);
        ok(firstTree1Node.find('span:last').html() === 'Folder 21', 'node is added successfully.');
        ok(firstTree2Node.find('span:last').html() !== 'Folder 21', 'node is removed successfully.');

        tree1.remove();
        tree2.remove();

        tree1 = createSimpleTree({ nodes: [{ text: "Folder 10" }, { text: "Folder 11" }] });
        tree2 = create68795Tree();
        treenode20 = tree2.wijtree("findNodeByText", "Folder 20").element;
        treenode21 = tree2.wijtree("findNodeByText", "Folder 21").element;
        tree2.wijtree("remove", treenode20);
        tree1.wijtree("add", treenode20, 0);
        firstTree2Node = getTreeNode(tree2, 0);
        firstTree1Node = getTreeNode(tree1, 0);
        ok(firstTree1Node.find('span:last').html() === 'Folder 20', 'node is added successfully.');
        ok(firstTree2Node.find('span:last').html() !== 'Folder 20', 'node is removed successfully.');
        tree2.wijtree("remove", treenode21);
        firstTree1Node = (tree1.wijtree("getNodes"))[0];
        firstTree1Node.element.wijtreenode("add", treenode21, 0);
        firstTree2Node = getTreeNode(tree2, 0);
        ok(firstTree2Node.find('span:last').html() !== 'Folder 21', 'node is removed successfully.');
        childNode = getTreeNode(firstTree1Node.element, 0);
        ok(childNode && childNode.length, 'child node has been added successfully.');

        tree1.remove();
        tree2.remove();
    });

    test("Create nodes using options with initial expanded and checked states", function () {
        var $widget = createSimpleTree({
            showCheckBoxes: true,
            nodes: [{
                text: "1",
                expanded: true,
                nodes: [{
                    text: "1.1",
                    checked: true
                }]
            }, {
                text: "2",
                nodes: [{
                    text: "2.1",
                    nodes: [{ text: "2.1.1" }]
                }]
            }]
        }),
            $nodeToExpand = $(".wijmo-wijtree-node", $widget).eq(0),
            $nodeToCheck = $(".wijmo-wijtree-node", $widget).eq(1);

        ok($nodeToExpand.hasClass("wijmo-state-expanded"), "Node is expanded.");
        ok($nodeToCheck.find("span.wijmo-checkbox-icon").hasClass("ui-icon-check"), "Node is checked.");

        $widget.remove();
    });

    test("#86498, JavaScript error is observed when drag and drop a node into child node", function () {
        var $widget = createTree(),
            $node = $widget.find(":wijmo-wijtreenode:eq(0)"),
            $childNode1 = getTreeNode($node, 0),
            $childNode2 = getTreeNode($node, 1),
            $childNode3 = getTreeNode($node, 2);

        ok($childNode1.wijtreenode("option", "nodes") && $childNode1.wijtreenode("option", "nodes").length === 0, "Node's options cannot be null.");
        ok($childNode2.wijtreenode("option", "nodes") && $childNode2.wijtreenode("option", "nodes").length === 0, "Node's options cannot be null.");
        ok($childNode3.wijtreenode("option", "nodes") && $childNode3.wijtreenode("option", "nodes").length === 1, "Node's options cannot be null, this node has one child node.");

        $widget.remove();
    });

    test("#86560, child nodes are not display when binding with knockout and loading child nodes with Ajax", function () {
        var tree = createEmptyTree();
        tree.wijtree("add", { text: "Folder 1", nodes: [{ text: "1.1" }, { text: "1.2" }] });
        var node = tree.find(">li:eq(0)");
        ok(node.wijtreenode("getNodes").length === 2);
        node.wijtreenode("add", { text: "Folder 1.3", nodes: [{ text: "1.3.1" }, { text: "1.3.2" }] });
        ok(node.wijtreenode("getNodes").length === 3);
        ok(node.wijtreenode("getNodes")[2].element.wijtreenode("getNodes").length === 2);
        tree.remove();
    });

    test("#80070, TreeNode's height is doubled when it gets into edit mode", function () {
        var o = {
            showCheckBoxes: true,
            nodes: [
                {
                    text: "Test"
                }, {
                    text: "Thisisatestverylongtextverylongtextverylongtextverylongtext"
                }
            ]
        },
            $widget,
            $node1,
            $node2;

        $widget = createEmptyTree(o);
        $node1 = $("#tree").find(':wijmo-wijtreenode:eq(0)');
        $node2 = $("#tree").find(':wijmo-wijtreenode:eq(1)');
        ok($node1.height() === $node2.height(), "Node text should not change line if the text is long");
        $widget.remove();
    });

    test("#80070, TreeNode's height is doubled when it gets into edit mode", function () {
        var o = {
            showCheckBoxes: true,
            nodes: [
                {
                    text: "Test"
                }, {
                    text: "Thisisatestverylongtextverylongtextverylongtextverylongtext"
                }
            ]
        },
            $widget,
            $node1,
            $node2;

        $widget = createEmptyTree(o);
        $node1 = $("#tree").find(':wijmo-wijtreenode:eq(0)');
        $node2 = $("#tree").find(':wijmo-wijtreenode:eq(1)');
        ok($node1.height() === $node2.height(), "Node text should not change line if the text is long");
        $widget.remove();
    });

    test("#84182", function () {
        var $widget = createSimpleTree();

        $widget.wijtree("option", "disabled", true);
        $widget.wijtree("add", "node", 1);
        ok($widget.find("li").length > 0 && $widget.find("li .ui-state-disabled").length === 0, "added nodes do not have disabled css.");
        $widget.remove();
    });
    test("#113652 nodeCheckChanging & nodeCheckChanged by press space and setOption", function () {
        var changingFlag = 0, changedFlag = 0, node, checkbox, nodes;

        el = createSimpleTree({
            showCheckBoxes: true,
            nodeCheckChanging: function (e, ui) {
                changingFlag++;
            },
            nodeCheckChanged: function (e, ui) {
                changedFlag++;
            }
        });
        node = el.children("li:first");
        node.find("a:first").trigger('click');
        node.find("a:first").simulate("keydown", { keyCode: $.ui.keyCode.SPACE });
        equal(changingFlag, 1, "nodeCheckChanging event is fired.");
        equal(changedFlag, 1, "nodeCheckChanged event is fired.");
        equal(node.wijtreenode("option", "checked"), true, "tree node is checked");
        node.wijtreenode("option", "checked", true);  //set the same value as option["checked"]
        node.wijtreenode("option", "checked", false);  //set the different value to option["checked"]
        equal(changingFlag, 2, "nodeCheckChanging event is fired.");
        equal(changedFlag, 2, "nodeCheckChanged event is fired.");
        equal(node.wijtreenode("option", "checked"), false, "tree node is not checked");

        el.remove();
    });

    test("Move focus to next control with Tab key.", function () {
        var $widget = createTree();
        ok($widget.find("a:first").attr("tabindex") !== -1, "first link's tabindex is right");
        ok($widget.find("a[tabindex='-1']").length === $widget.find("a").length-1, "tabindexes are right!");
         $widget.remove();
    });

})(jQuery);