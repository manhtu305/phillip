﻿/*
* wijtreenode_methods.js
*/
(function($) {

	module("wijtreenode:methods");

	test("getNodes", function () {
	    var $widget = createTree();

	    var li = $widget.find('>li:eq(0)');

	    var nodes = li.wijtreenode('getNodes');

	    ok(nodes.length === 3 && nodes[0].element.find('a:first span:last').html() === 'Folder 1.3', 'getNodes');
	    $widget.remove();

	});

    test("add, remove", function() {
		var $widget = createSimpleTree();
		var $nodeWidget = $widget.find('>li:eq(0)');
		var length = $nodeWidget.find('>li').length;

		$nodeWidget.wijtreenode('add', 'UnitTest Node2', 0);
		var $node = getTreeNode($nodeWidget, 0);

		ok($nodeWidget.wijtreenode("getNodes").length === 2, "child nodes' count will be 2 after invoking add method");
        ok($node.find('span:last').html() === 'UnitTest Node2', 'Node added success');

		$nodeWidget.wijtreenode('remove', 0);
        $node = getTreeNode($nodeWidget, 0);
		ok($nodeWidget.wijtreenode("getNodes").length === 1, "child nodes' count will be 1 after invoking remove method");
		if (length > 0) {
            ok($node.find('span:last').html() !== 'UnitTest Node2', 'Node removed success');
        } else {
            ok($nodeWidget.find('>li').length === 0, 'Node removed success');
		}
        $widget.remove();
    });

    test("add, remove node to a tree which has lots of nodes", function() {
        var $widget = createTreeWithLotsNodes();
        var $nodeWidget = $widget.wijtree('getNodes')[90];
        var length = $nodeWidget.element.find('>li').length;

        $nodeWidget.element.wijtreenode('add', 'UnitTest Node2', 0);
        var $node = getTreeNode($nodeWidget.element, 0);

        ok($nodeWidget.element.wijtreenode("getNodes").length === 11, "child nodes' count will be 11 after invoking add method");
        ok($node.find('span:last').html() === 'UnitTest Node2', 'Node added success');

        $nodeWidget.element.wijtreenode('remove', 0);
        $node = getTreeNode($nodeWidget.element, 0);
        ok($nodeWidget.element.wijtreenode("getNodes").length === 10, "child nodes' count will be 1 after invoking remove method");
        if (length > 0) {
            ok($node.find('span:last').html() !== 'UnitTest Node2', 'Node removed success');
        } else {
            ok($nodeWidget.element.find('>li').length === 0, 'Node removed success');
		}
		$widget.remove();
	});

    test("add nodes by different parameters", function() {
        var $widget = createSimpleTree(),
            $nodeWidget = $widget.find('>li:eq(0)'),
            li = document.createElement("li"),
            a = document.createElement("a");
        $(a).text("Added Node1");
        li.appendChild(a);
        $nodeWidget.wijtreenode('add', li, 0);

        var $node = getTreeNode($nodeWidget, 0);

        ok($nodeWidget.wijtreenode("getNodes").length === 2, "child nodes' count will be 2 after invoking add method");
        ok($node.find('span:last').html() === 'Added Node1', 'Node added success');

        $nodeWidget.wijtreenode('add', { text: "Added Node2" }, 0);
        $node = getTreeNode($nodeWidget, 0);

        ok($nodeWidget.wijtreenode("getNodes").length === 3, "child nodes' count will be 3 after invoking add method");
        ok($node.find('span:last').html() === 'Added Node2', 'Node added success');

        $nodeWidget.wijtreenode('add', $("<li><a>Added Node3</a></li>"), 0);
        $node = getTreeNode($nodeWidget, 0);

        ok($nodeWidget.wijtreenode("getNodes").length === 4, "child nodes' count will be 4 after invoking add method");
        ok($node.find('span:last').html() === 'Added Node3', 'Node added success');

        $widget.remove();

        $widget = createTreeWithLotsNodes();
        $nodeWidget = $widget.wijtree("getNodes")[80].element;
        $nodeWidget.wijtreenode('add', li, 0);
        $node = getTreeNode($nodeWidget, 0);

        ok($nodeWidget.wijtreenode("getNodes").length === 11, "child nodes' count will be 11 after invoking add method");
        ok($node.find('span:last').html() === 'Added Node1', 'Node added success');

        $nodeWidget.wijtreenode('add', { text: "Added Node2" }, 0);
        $node = getTreeNode($nodeWidget, 0);

        ok($nodeWidget.wijtreenode("getNodes").length === 12, "child nodes' count will be 12 after invoking add method");
        ok($node.find('span:last').html() === 'Added Node2', 'Node added success');

        $nodeWidget.wijtreenode('add', $("<li><a>Added Node3</a></li>"), 0);
        $node = getTreeNode($nodeWidget, 0);

        ok($nodeWidget.wijtreenode("getNodes").length === 13, "child nodes' count will be 13 after invoking add method");
        ok($node.find('span:last').html() === 'Added Node3', 'Node added success');

        $widget.remove();
	});

    test("getNodes from invisible node", function() {
        var $widget = createTreeWithLotsNodes();

        var nodes = $widget.wijtree('getNodes');

        nodes = nodes[90].getNodes();

        ok(nodes.length === 10 && nodes[0].element.find('a:first span:last').html() === 'node90.0', 'getNodes');
        $widget.remove();
    });

    test("getOwner", function() {
	    var $widget = createTree();

	    var li = $widget.find('>li:eq(0)');

	    var nodes = li.wijtreenode('getNodes');
	    var owner = nodes[0].element.wijtreenode('getOwner');

        ok(owner.element.find('a:first span:last').html() === 'Folder 1', 'getOwner');
	    $widget.remove();
	});

    test('check,select,expand,collapse', function() {
		ok(true, 'Reference wijtreenode_options:  Checked,Selected,Expanded');
    });

})(jQuery);
