﻿/*
* wijtree_methods.js
*/
(function ($) {

	module("wijtree: methods");
    
	test("getNodes", function () {
	    var $widget = createTree();

	    $widget.find('>li:eq(0) a:first').trigger('click');

	    var nodes = $widget.wijtree('getNodes');

	    ok(nodes.length === 3 && nodes[0].element.find('a:first span:last').html() === 'Folder 1', 'getNodes');
	    $widget.remove();
	});

	test("add, remove, findNodeByText", function () {
		var $widget = createTree();
		$widget.wijtree('add', 'UnitTest Node', 1);
		var $node = getTreeNode($widget, 1);

		ok($widget.wijtree("getNodes").length === 4, "root nodes' count will be 4 after invoking add method");
		ok($node.find('span:last').html() === 'UnitTest Node', 'add:Node added success');

		$node = $widget.wijtree('findNodeByText', 'Folder 1');
		var f = $node.element.find('a:first span:last').html() === 'Folder 1';
		ok(f, 'findNodeByText:Specify node is found');

		$widget.wijtree('remove', 1);
		$node = getTreeNode($widget, 1);
		ok($widget.wijtree("getNodes").length === 3, "root nodes' count will be 3 after invoking remove method");
		ok($node.find('span:last').html() !== 'UnitTest Node', 'remove:Node removed success');
		$widget.remove();
	});

	test("add, remove, findNodeByText node to a tree which has lots of nodes", function () {
	    var $widget = createTreeWithLotsNodes();
	    $widget.wijtree('add', 'UnitTest Node1', 1);
	    var $node = getTreeNode($widget, 1);

	    ok($widget.wijtree("getNodes").length === 101, "root nodes' count will be 101 after invoking add method");
	    ok($node.find('span:last').html() === 'UnitTest Node1', 'add:Node added success');

	    $widget.wijtree('add', 'UnitTest Node2', 80);
	    $node = getTreeNode($widget, 80);

	    ok($widget.wijtree("getNodes").length === 102, "root nodes' count will be 102 after invoking add method");
	    ok($node.find('span:last').html() === 'UnitTest Node2', 'add:Node added success');

	    $node = $widget.wijtree('findNodeByText', 'UnitTest Node2');
	    var f = $node.element.find('a:first span:last').html() === 'UnitTest Node2';
	    ok(f, 'findNodeByText:Specify node is found');

	    $node = $widget.wijtree('findNodeByText', 'node90');
	    f = $node.element.find('a:first span:last').html() === 'node90';
	    ok(f, 'findNodeByText:Specify node is found');

	    $widget.wijtree('remove', 95);
	    $node = getTreeNode($widget, 95);
	    ok($widget.wijtree("getNodes").length === 101, "root nodes' count will be 101 after invoking remove method");
	    ok($node.find('span:last').html() !== 'node93', 'already added two nodes, test if the node93 has been removed');
	    ok(!$widget.wijtree('findNodeByText', 'node93'), 'findNodeByText:Specify node is not found, because it is removed');

	    $widget.remove();
	});

	test("add nodes by different parameters", function () {
	    var $widget = createSimpleTree(),
            li = document.createElement("li"),
            a = document.createElement("a");
	    $(a).text("Added Node1");
	    li.appendChild(a);
	    $widget.wijtree('add', li, 0);

	    var $node = getTreeNode($widget, 0);

	    ok($widget.wijtree("getNodes").length === 2, "child nodes' count will be 2 after invoking add method");
	    ok($node.find('span:last').html() === 'Added Node1', 'Node added success');

	    $widget.wijtree('add', { text: "Added Node2" }, 0);
	    $node = getTreeNode($widget, 0);

	    ok($widget.wijtree("getNodes").length === 3, "child nodes' count will be 3 after invoking add method");
	    ok($node.find('span:last').html() === 'Added Node2', 'Node added success');

	    $widget.wijtree('add', $("<li><a>Added Node3</a></li>"), 0);
	    $node = getTreeNode($widget, 0);

	    ok($widget.wijtree("getNodes").length === 4, "child nodes' count will be 4 after invoking add method");
	    ok($node.find('span:last').html() === 'Added Node3', 'Node added success');

	    $widget.remove();

	    $widget = createTreeWithLotsNodes();
	    $widget.wijtree('add', li, 80);
	    $node = $widget.wijtree("getNodes")[80].element;

	    ok($widget.wijtree("getNodes").length === 101, "child nodes' count will be 101 after invoking add method");
	    ok($node.find('span:last').html() === 'Added Node1', 'Node added success');

	    $widget.wijtree('add', { text: "Added Node2" }, 0);
	    $node = getTreeNode($widget, 0);

	    ok($widget.wijtree("getNodes").length === 102, "child nodes' count will be 102 after invoking add method");
	    ok($node.find('span:last').html() === 'Added Node2', 'Node added success');

	    $widget.wijtree('add', $("<li><a>Added Node3</a></li>"), 0);
	    $node = getTreeNode($widget, 0);

	    ok($widget.wijtree("getNodes").length === 103, "child nodes' count will be 103 after invoking add method");
	    ok($node.find('span:last').html() === 'Added Node3', 'Node added success');

	    $widget.remove();
	});

	test("getSelectedNodes", function () {
		var $widget = createTree();

		$widget.find('>li:eq(0) a:first').trigger('click');

		var nodes = $widget.wijtree('getSelectedNodes');

		ok(!!nodes.length && nodes[0].element.find('a:first span:last').html() === 'Folder 1', 'getSelectedNodes');
		$widget.remove();

	});

	test("getCheckedNodes", function () {
		var $widget = createTree({ showCheckBoxes: true });

		$widget.find('>li:eq(0) .wijmo-checkbox:eq(0)').trigger('click');

		var nodes = $widget.wijtree('getCheckedNodes');

		ok(!!nodes.length && nodes[0].find('a:first span:last').html() === 'Folder 1', 'getSelectedNodes');
		$widget.remove();

	});
    
	//keyboard

	var tree;
	test("keydown:down", function () {
		var treeWidget, node;
		tree = createTree({
			expandAnimation: null,
			collapseAnimation: null
		});
		node = getExactNode(tree, "0");
		node.find("a:first").simulate("keydown", { keyCode: $.ui.keyCode.DOWN });

		treeWidget = tree.data("wijmo-wijtree");
		treeWidget._focusNode;

		ok(treeWidget._focusNode._text === "Folder 2", 'keydown:down on collapse');

		node.wijtreenode("expand");
		node.find("a:first").simulate("keydown", { keyCode: $.ui.keyCode.DOWN });
		ok(treeWidget._focusNode._text === "Folder 1.3", 'keydown:down on expand');
		tree.remove();
	});

	test("keydown:up", function () {
		var treeWidget, node;
		tree = createTree({
			expandAnimation: null,
			collapseAnimation: null
		});

		node = getExactNode(tree, "1");
		node.find("a:first").simulate("keydown", { keyCode: $.ui.keyCode.UP });

		treeWidget = tree.data("wijmo-wijtree");
		treeWidget._focusNode;

		ok(treeWidget._focusNode._text === "Folder 1", 'keydown:up on front node collapse');

		getExactNode(tree, "0").wijtreenode("expand");

		node.find("a:first").simulate("keydown", { keyCode: $.ui.keyCode.UP });
		ok(treeWidget._focusNode._text === "Folder 1.5", 'keydown:up on front node expand');

		node = getExactNode(tree, "0,0");
		node.find("a:first").simulate("keydown", { keyCode: $.ui.keyCode.UP });
		ok(treeWidget._focusNode._text === "Folder 1", 'keydown:up on first child node focused');

		tree.remove();
	});

	test("keydown:left", function () {

		var treeWidget, node;
		tree = createTree({
			expandAnimation: null,
			collapseAnimation: null
		});

		node = getExactNode(tree, "0,1");
		node.find("a:first").simulate("keydown", { keyCode: $.ui.keyCode.LEFT });

		treeWidget = tree.data("wijmo-wijtree");
		treeWidget._focusNode;

		ok(treeWidget._focusNode._text === "Folder 1", 'keydown:left on node be collapsed');

		node = getExactNode(tree, "0");
		node.find("a:first").simulate("keydown", { keyCode: $.ui.keyCode.LEFT });

		ok(!node.wijtreenode("option", "expanded"), "keydown:left on node be expanded");
		tree.remove();
	});

	test("keydown:right", function () {
		var node;
		tree = createTree({
			expandAnimation: null,
			collapseAnimation: null
		});

		node = getExactNode(tree, "0");
		node.find("a:first").simulate("keydown", { keyCode: $.ui.keyCode.RIGHT });

		ok(node.wijtreenode("option", "expanded"), "keydown:right on node be collapsed");
		ok(node.wijtreenode("getNodes").length === 3, "there are three child nodes");
		tree.remove();
	});

	test("keydown:space", function () {
		var node;
		tree = createTree({
			expandAnimation: null,
			collapseAnimation: null,
			showCheckBoxes: true
		});

		node = getExactNode(tree, "0");
		node.find("a:first").simulate("keydown", { keyCode: $.ui.keyCode.SPACE });

		ok(node.wijtreenode("option", "checked"), "keydown:SPACE on node unchecked");

		node.find("a:first").simulate("keydown", { keyCode: $.ui.keyCode.SPACE });
		ok(!node.wijtreenode("option", "checked"), "keydown:SPACE on node checked");
		tree.remove();
	});

	test("keydown:plus & minus", function () {
		var node;
		tree = createTree({
			expandAnimation: null,
			collapseAnimation: null
		});
		node = getExactNode(tree, "0");
		node.find("a:first").simulate("keydown", { keyCode: 107 });
		ok(node.wijtreenode("option", "expanded"), "keydown:+ on node collapsed");
		ok(node.wijtreenode("getNodes").length === 3, "there are three child nodes");

		node.find("a:first").simulate("keydown", { keyCode: 109 });
		ok(!node.wijtreenode("option", "expanded"), "keydown:- on node expanded");

		tree.remove();
	});



})(jQuery);
