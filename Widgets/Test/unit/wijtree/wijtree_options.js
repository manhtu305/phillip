﻿/*
* wijtree_options.js
*/
(function ($) {

	module("wijtree:options");


	test("allowDrag", function () {
		var $widget = createSimpleTree({allowDrag: true});
		var $node = $widget.find(':wijmo-wijtreenode:eq(0)');
		$node.find('a:first').trigger('mousedown');  //initialized draggable

		ok(!!$node.data('ui-draggable'), 'allowDrag:true');
		$widget.wijtree('option', 'allowDrag', false);
		ok(!$node.data('ui-draggable'), 'allowDrag:false');
		$widget.remove();
	});

	test("allowDrop", function () {
		var $widget = createSimpleTree();
		$widget.wijtree('option', 'allowDrop', true);
		ok($widget.wijtree('option', 'allowDrop'), 'allowDrop:true');
		$widget.wijtree('option', 'allowDrop', false);
		ok(!$widget.wijtree('option', 'allowDrop'), 'allowDrop:false');
		$widget.remove();
	});

	test("autoCheckNodes", function () {
		//todo: reset tree :(all nodes unchecked,unselected,collapsed)
		//$('#wijtree').wijtree('option', 'autoCheckNodes', true);
		var $widget = createTree({
			showCheckBoxes: true
		});

		$widget.wijtree('option', 'autoCheckNodes', true);
		$widget.find('li:first').wijtreenode('check', true);
		var autoCheckT = false;
		$widget.find('li:first').find('>ul>li').each(function () {
			if ($(this).wijtreenode('option', 'checked') != true) {
				autoCheckT = true;
			}
		});

		$widget.find('li:first').wijtreenode('check', false);
		var autoCheckF = false;
		$widget.find('li:first').find('>ul>li').each(function () {
			if ($(this).wijtreenode('option', 'checked') != false) {
				autoCheckF = true;
			}
		});
		ok(!!$widget.find('li:first').find('>ul>li').length
			&& !autoCheckT
			&& !autoCheckF, 'autoCheckNodes:true');

		$widget.wijtree('option', 'autoCheckNodes', false);
		$widget.find('li:first').wijtreenode('check', true);
		autoCheckT = false;
		autoCheckF = false;
		$widget.find('li:first').find('>ul>li').each(function () {
			if ($(this).wijtreenode('option', 'checked') != true) {
				autoCheckT = true;
			}
		});

		$widget.find('li:first').wijtreenode('check', false);

		$widget.find('li:first').children('ul>li').each(function () {
			if ($(this).wijtreenode('option', 'checked') != false) {
				autoCheckF = true;
			}
		});

		ok(autoCheckT || autoCheckF, 'autoCheckNodes:false');
		$widget.remove();

	});

	test("allowTriState", function () {
		//todo: reset tree :(all nodes unchecked,unselected,collapsed)
		var $widget = createTree();
		$widget.find('li:first').children('ul>li:first').wijtreenode('option', 'checked', true);
		var tri = false;
		if (!$widget.find('li:first').data('wijmo-wijtreenode')._checkState == 'indeterminate'
			&& $widget.find('li:first').data('wijmo-wijtreenode')._checked == true) {
			tri = true;
		}
		$widget.find('li:first').children('ul>li').each(function () {
			$(this).wijtreenode('option', 'checked', true);
		});
		if (!$widget.find('li:first').data('wijmo-wijtreenode')._checkState == 'checked') {
			tri = true;
		}
		ok(!tri, 'allowTriState:true');

		tri = false;
		$widget.wijtree('option', 'allowTriState', false);

		$widget.find('li:first').children('ul>li:first').wijtreenode('option', 'checked', true);
		if (!$widget.find('li:first').data('wijmo-wijtreenode')._checkState == 'unChecked') {
			tri = true;
		}
		ok(!tri, 'allowTriState:false');
		$widget.remove();
	});

	test("autoCollapse", function () {
		var $widget = createTree({
			expandAnimation: false,
			collapseAnimation: false
		});

		$widget.wijtree('option', 'autoCollapse', true);
		$widget.find(">li:eq(0)").wijtreenode('expand');
		$widget.find(">li:eq(1)").wijtreenode('expand');

		//ui-icon-triangle-1-e

		ok(!!$widget.find(">li:eq(0)").has('span.ui-icon-triangle-1-e').length
			&& !$widget.find(">li:eq(0) ul:first").is(':visible'),
			'autoCollapse:true');

		$widget.wijtree('option', 'autoCollapse', false);
		$widget.find(">li:eq(2)").wijtreenode('expand');

		ok(!!$widget.find(">li:eq(1)").has('span.ui-icon-triangle-1-se').length
			&& $widget.find(">li:eq(1) ul:first").is(':visible'),
			'autoCollapse:false');

		$widget.remove();
	});

	//showCheckBoxes
	test("showCheckBoxes", function () {
		var $widget = createSimpleTree({
			showCheckBoxes: true
		});

		$widget.wijtree('option', 'showCheckBoxes', true);
		ok(!!$widget.find("div.wijmo-checkbox:eq(0)").is(':visible'), 'showCheckBoxes:true');

		$widget.wijtree('option', 'showCheckBoxes', false);
		ok(!$widget.find("div.wijmo-checkbox:eq(0)").is(':visible'), 'showCheckBoxes:false');
		$widget.remove();
	});

	//showHitArea
	test("showExpandCollapse", function () {
		var $widget = createSimpleTree();

		$widget.wijtree('option', 'showExpandCollapse', true);
		ok(!!$widget.find("span.ui-icon-triangle-1-e,span.ui-icon-triangle-1-se").eq(0).is(':visible'),
			'showExpandCollapse:true');

		$widget.wijtree('option', 'showExpandCollapse', false);
		ok(!$widget.find("span.ui-icon-triangle-1-e,span.ui-icon-triangle-1-se").eq(0).is(':visible'),
			'showExpandCollapse:false');
		$widget.remove();
	});

	//expandCollapseHoverUsed
	//	test("expandCollapseHoverUsed", function () {
	//		var $widget = createSimpleTree(), p;
	//		$widget.wijtree('option', 'expandCollapseHoverUsed', true);
	//		p = $widget.find(">li:eq(0) span:.wijmo-wijtree-inner").offset();


	//		$widget.find(">li:eq(0) span:.wijmo-wijtree-inner").simulate('mouseover', {
	//			pageX: p.left,
	//			pageY: p.top
	//		});

	//		setTimeout(function () {
	//			start();
	//			ok($widget.find(">li:eq(0)>ul").is(':visible'), 'expandCollapseHoverUsed');
	//			//$widget.remove();
	//		}, 1000);
	//		stop();
	//	});

})(jQuery);
