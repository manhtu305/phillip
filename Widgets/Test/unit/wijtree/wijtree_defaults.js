﻿/*
* wijtreeview_defaults.js
*/

(function ($) {
    // clean the node in prototype.
    test("destroy nodes", function () {
        $.wijmo.wijtree.prototype.options.nodes = null;
    });
})(jQuery);

commonWidgetTests('wijtree', {
    defaults: {
        /// <summary>
        /// Selector option for auto self initialization. 
        ///	This option is internal.
        /// </summary>
        initSelector: ":jqmData(role='wijtree')",
        wijCSS: $.wijmo.wijCSS,
        allowDrag: false,
        wijMobileCSS: {
            header: "ui-header ui-bar-a",
            content: "ui-body-b",
            stateDefault: "ui-btn ui-btn-b",
            stateHover: "ui-btn-down-b",
            stateActive: "ui-btn-down-c"
        },
        allowDrop: false,
        allowEdit: false,
        allowSorting: true,
        allowTriState: true,
        autoCheckNodes: true,
        autoCollapse: false,
        disabled: false,
        expandCollapseHoverUsed: false,
        showCheckBoxes: false,
        showExpandCollapse: true,
        expandAnimation: { effect: 'blind', easing: 'linear', duration: 200 },
        expandDelay: 0,
        collapseAnimation: { effect: 'blind', easing: 'linear', duration: 200 },
        collapseDelay: 0,
        draggable: null,
        droppable: null,
        dropVisual: null,
        nodes: null,
        nodeBlur: null,
        nodeFocus: null,
        nodeClick: null,
        nodeCheckChanging: null,
        nodeCheckChanged: null,
        nodeCollapsed: null,
        nodeExpanded: null,
        nodeDragging: null,
        nodeDragStarted: null,
        nodeBeforeDropped: null,
        nodeDropped: null,
        nodeMouseOver: null,
        nodeMouseOut: null,
        nodeTextChanged: null,
        selectedNodeChanged: null,
        nodeExpanding: null,
        nodeCollapsing: null,
        create: null
    }
});