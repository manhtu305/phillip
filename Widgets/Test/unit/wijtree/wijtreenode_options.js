﻿/*
* wijtreenode_options.js
*/
(function ($) {

    module("wijtreenode:options");

    test("disable", function () {
        var $widget = createSimpleTree();
        var $node = $widget.find(':wijmo-wijtreenode:eq(0)');
        $node.wijtreenode('option', 'disabled', true);
        ok($node.hasClass('ui-state-disabled'), 'node disabled');

        $node.wijtreenode('option', 'disabled', false);
        ok(!$node.hasClass('ui-state-disabled'), 'node enable');
        $widget.remove();
    });

    test("expanded", function () {
        var $widget = createSimpleTree({
            expandAnimation: false,
            collapseAnimation: false
        });
        var $node = $widget.find(':wijmo-wijtreenode:eq(0)');
        $node.wijtreenode('option', 'expanded', true);
        var l = $node.has('span.ui-icon-triangle-1-se').length;
        ok(!!l
            && $node.find("ul:first").is(':visible'),
            'expanded:true');
        ok($node.wijtreenode("getNodes")[0].element.text() === "Folder 1.3");

        $node.wijtreenode('option', 'expanded', false);
        l = $node.has('span.ui-icon-triangle-1-e').length;
        ok(!!l
            && !$node.find("ul:first").is(':visible'),
            'expanded:false');

        $widget.remove();
    });

    test("expanded invisible node", function () {
        var $widget = createTreeWithLotsNodes();
        var $nodeWidget = $widget.wijtree('findNodeByText', 'node90');
        $nodeWidget.element.wijtreenode('option', 'expanded', true);
        ok($nodeWidget.element.has('span.ui-icon-triangle-1-se').length, 'expanded:true');

        $nodeWidget.element.wijtreenode('option', 'expanded', false);
        ok($nodeWidget.element.has('span.ui-icon-triangle-1-e').length, 'expanded:false');

        $widget.remove();
    });

    test("selected", function () {
        var $widget = createSimpleTree();
        var $node = $widget.find(':wijmo-wijtreenode:eq(0)');
        $node.wijtreenode('option', 'selected', true);
        ok($.inArray($node.data('wijmo-wijtreenode'), $widget.wijtree('getSelectedNodes')) != -1, 'node selected');

        $node.wijtreenode('option', 'selected', false);
        ok(!$.inArray($node.data('wijmo-wijtreenode'), $widget.wijtree('getSelectedNodes')) != -1, 'node unselected');
        $widget.remove();
    });

    test("selected invisible node", function () {
        var $widget = createTreeWithLotsNodes();
        var $nodeWidget = $widget.wijtree('findNodeByText', 'node90');
        $nodeWidget.element.wijtreenode('option', 'selected', true);
        ok($.inArray($nodeWidget.element.data('wijmo-wijtreenode'), $widget.wijtree('getSelectedNodes')) != -1, 'node selected');

        $nodeWidget.element.wijtreenode('option', 'selected', false);
        ok(!$.inArray($nodeWidget.element.data('wijmo-wijtreenode'), $widget.wijtree('getSelectedNodes')) != -1, 'node unselected');

        $nodeWidget = $widget.wijtree('findNodeByText', 'node91.9');
        $nodeWidget.element.wijtreenode('option', 'selected', true);
        ok($.inArray($nodeWidget.element.data('wijmo-wijtreenode'), $widget.wijtree('getSelectedNodes')) != -1, 'node selected');

        $nodeWidget.element.wijtreenode('option', 'selected', false);
        ok(!$.inArray($nodeWidget.element.data('wijmo-wijtreenode'), $widget.wijtree('getSelectedNodes')) != -1, 'node unselected');
        $widget.remove();
    });

    test("checked", function () {
        var $widget = createSimpleTree({
            showCheckBoxes: true
        });
        var $node = $widget.find(':wijmo-wijtreenode:eq(0)');
        $node.wijtreenode('option', 'checked', true);
        ok(!!$node.find('div.wijmo-checkbox:first').find('span.ui-icon-check').length, 'node checked'); //ui-icon-check

        $node.wijtreenode('option', 'checked', false);
        ok(!$node.find('div.wijmo-checkbox:first').find('span.ui-icon-check').length, 'node unchecked');

        $widget.remove();
    });

    test("test check option after setting autoCheckNodes", function () {
        var $widget = createSimpleTree({
            showCheckBoxes: true,
            autoCheckNodes: true
        });
        var $node = $widget.find(':wijmo-wijtreenode:eq(0)');
        $node.wijtreenode('option', 'checked', true);
        var childNode = $node.wijtreenode("getNodes")[0];
        ok(!!childNode.element.find('div.wijmo-checkbox:first').find('span.ui-icon-check').length, 'child node checked');

        $widget.wijtree({ autoCheckNodes: false });
        $node.wijtreenode('option', 'checked', false);
        ok(!!childNode.element.find('div.wijmo-checkbox:first').find('span.ui-icon-check').length, 'child node unchecked');

        $widget.remove();

        $widget = createTreeWithLotsNodes();
        $widget.wijtree({ showCheckBoxes: true });
        $node = $widget.wijtree('getNodes')[90].element;
        $node.wijtreenode('option', 'checked', true);
        childNode = $node.wijtreenode("getNodes")[0];
        ok(!!childNode.element.find('div.wijmo-checkbox:first').find('span.ui-icon-check').length, 'child node checked');

        $widget.wijtree({ autoCheckNodes: false });
        $node.wijtreenode('option', 'checked', false);
        ok(!!childNode.element.find('div.wijmo-checkbox:first').find('span.ui-icon-check').length, 'child node unchecked');

        $widget.remove();
    });

    test("collapsedIcon,expandedIcon,itemIcon", function () {
        var $widget = createSimpleTree();
        var $node = $widget.find(':wijmo-wijtreenode:eq(0)');
        $node.wijtreenode('option', 'collapsedIconClass', 'ui-icon-folder-collapsed');
        $node.wijtreenode('option', 'expandedIconClass', 'ui-icon-folder-open');
        $node.wijtreenode('expand');
        ok(!!$node.find('span.ui-icon-folder-open').length, 'expandedIcon');

        $node.wijtreenode('collapse');
        ok(!!$node.find('span.ui-icon-folder-collapsed').length, 'collapsedIcon');

        $node.wijtreenode('option', 'collapsedIconClass', '');
        $node.wijtreenode('option', 'expandedIconClass', '');
        $node.wijtreenode('option', 'itemIconClass', 'ui-icon-document');

        ok(!!$node.find('span.ui-icon-document').length, 'itemIcon');

        $widget.remove();
    });

    test("text", function () {
        var $widget = createSimpleTree();
        var $node = $widget.find(":wijmo-wijtreenode:eq(0)");
        $node.wijtreenode('option', 'text', "UnitTest Node");
        ok($node.find('>div span:last').html() == "UnitTest Node", "node checked"); // ui-icon-check
        $widget.remove();
    });

    test("nodes", function () {
        var $widget = createSimpleTree();
        var $node = $widget.find(":wijmo-wijtreenode:eq(0)");
        var addingNodes = [
            {
                text: "Data node 1.1",
                nodes: [
                {
                    text: "Data node 1.1.1"
                },
                {
                    text: "Data node 1.1.2"
                },
                {
                    text: "Data node 1.1.3"
                }
                ]
            },
            {
                text: "Data node 1.2"
            }
        ];
        $node.wijtreenode('option', 'nodes', addingNodes);
        ok($node.wijtreenode("getNodes").length === 2);
        ok($node.wijtreenode("getNodes")[1].element.text() === "Data node 1.2");
        ok($node.wijtreenode("getNodes")[0].element.wijtreenode("getNodes").length === 3);
        ok($node.wijtreenode("getNodes")[0].element.wijtreenode("getNodes")[2].element.text() === "Data node 1.1.3");
        $widget.remove();
    });

    test("allowDrag and allowDrop", function () {
        var $widget = createSimpleTree(),
            $node = $widget.find(":wijmo-wijtreenode:eq(0)");
        $node.wijtreenode('option', 'allowDrag', true);
        $node.find('a:first').trigger('mousedown');  //initialized draggable
        ok($node.data('ui-draggable'), 'allowDrag:true');
        $node.wijtreenode('option', 'allowDrag', false);
        $node.find('a:first').trigger('mousedown');  //initialized draggable
        ok(!$node.data('ui-draggable'), 'allowDrag:false');
        $node.wijtreenode('option', 'allowDrop', false);
        ok(!$node.wijtreenode('option', 'allowDrop'), 'allowDrop:false');
        $widget.remove();
    });

})(jQuery);
