/*globals commonWidgetTests*/
/*
* wijdatepager_defaults.js
*/
"use strict";
commonWidgetTests('wijdatepager', {
	defaults: {
		culture: "",
		cultureCalendar: "",
		localization: null,
		disabled: false,
		firstDayOfWeek: 0,
		selectedDate: null,
		viewType: "day",
		nextTooltip: "right",
		prevTooltip: "left",
		maxDateGroup: 6,
		initSelector: ":jqmData(role='wijdatepager')",
		customViewOptions: {unit: "day", count: 1},
		wijMobileCSS: {
			"header": "ui-header ui-bar-a",
			"content": "ui-body ui-body-b",
			"stateDefault": "ui-btn ui-btn-b"
		},
		wijCSS: $.extend(true, {
			wijdatepager: "",
			wijdatepagerIncButton: "",
			wijdatepagerDecButton: "",
			wijdatepagerContainer: "",
			wijdatepagerPages: "",
			wijdatepagerPageLabel: "",
			wijdatepagerPageLabelFirst: "",
			wijdatepagerPageLabelLast: "",
			wijdatepagerPageHeader: "",
			wijdatepagerPageRange: "",
			wijdatepagerTooltip: "",
			wijdatepagerTooltipInner: "",
			wijdatepagerTriangle: "",
			wijdatepagerWidthSmallest: "",
			wijdatepagerWidthSmall: "",
			wijdatepagerWidthMedium: "",
			wijdatepagerWidthNormal: ""
		}, $.wijmo.wijCSS),
		selectedDateChanged: null
	}
});