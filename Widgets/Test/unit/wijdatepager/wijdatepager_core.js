/*globals test, ok, jQuery, module, document, $*/
/*
* wijdatepager_core.js create & destroy
*/
"use strict";
function createDatepager(o) {
	return $('<div id="datepager"></div>')
		.appendTo(document.body).wijdatepager(o);
}


(function ($) {

	module("wijdatepager: core");

	test("create", function () {
		var datepager = createDatepager();
		ok(datepager.hasClass('wijmo-wijdatepager ui-widget ui-helper-clearfix'),
			'create:element css classes created.');		
		datepager.remove();
	});

	test("destroy", function () {
	    var $widget = $('<div id="datepager"></div>').appendTo("body");
	    domEqual($widget, function () {
	        $widget.wijdatepager().wijdatepager("destroy");
	    });
	    $widget.remove();
	});

}(jQuery));

