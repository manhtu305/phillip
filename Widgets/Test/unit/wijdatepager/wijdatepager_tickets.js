﻿/*
* wijdatepager_tickets.js
*/
"use strict";
(function ($) {

    module("wijdatepager: tickets");

    test("#80720", function () {
        var changeCount = 0, $widget = createDatepager({
            selectedDateChanged: function () {
                changeCount++;
            }
        });
        $widget.wijdatepager({ selectedDate: new Date("2/2/2014") });
        ok(changeCount === 1, "selectedDateChanged event was fired.");
        $widget.wijdatepager("option", "selectedDate", new Date("2/2/2014"));
        ok(changeCount === 1, "selectedDateChanged event was not fired.");
        $widget.wijdatepager("option", "selectedDate", new Date("2/3/2014"));
        ok(changeCount === 2, "selectedDateChanged event was fired.");
        $widget.remove();
    });

    test("#106022", function () {
        var changeCount = 0, $widget = createDatepager({firstDayOfWeek: 0, viewType: "custom", customViewOptions: { unit: "week", count: 2 }, selectedDate: new Date(2015, 0, 29) });
        ok($widget.find(".wijmo-wijdatepager-pagelabel").eq(0).html() === "Dec 14-27");
        $widget.remove();
    });

    test("#105939", function () {
        var changeCount = 0, $widget = createDatepager({ customViewOptions: { unit: "week", count: 1 }, viewType: "custom" });
        ok($widget.find(".wijmo-wijdatepager-pagelabel").eq(0).html().indexOf("-") >= 0, "single week's pager group text format should be xxx-xxx");
        $widget.remove();
    });

}(jQuery));