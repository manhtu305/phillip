﻿/*globals test, ok, module, document, jQuery, stop, start, $ */
/*
 * wijdatepager_options.js
 */
"use strict";
(function ($) {

	module("wijdatepager:options");
	
	test("culture and cultureCalendar", function () {
		var datepager, previousMonth;

		datepager = createDatepager({
			selectedDate: new Date("2014/2/1"),
			culture: "he"
		});
		previousMonth = $(".wijmo-wijdatepager-pagelabel", datepager).first();
		ok(previousMonth.html().indexOf("ינו") > -1, "Hebrew is applied to culture option.");
		datepager.remove();

		datepager = createDatepager({
			selectedDate: new Date("2014/2/1"),
			culture: "he",
			cultureCalendar: "Hebrew"
		});
		previousMonth = $(".wijmo-wijdatepager-pagelabel", datepager).first();
		ok(previousMonth.html().indexOf("תשרי") > -1, "Hebrew calendar  is applied to cultureCalendar option.");
		datepager.remove();

		datepager = createDatepager({
			selectedDate: new Date("2014/2/1")
		});
		previousMonth = $(".wijmo-wijdatepager-pagelabel", datepager).first();
		ok(previousMonth.html().indexOf("Jan") > -1, "Default culture is applied.");
		datepager.wijdatepager("option", "culture", "he");
		previousMonth = $(".wijmo-wijdatepager-pagelabel", datepager).first();
		ok(previousMonth.html().indexOf("ינו") > -1, "Hebrew is applied to culture option by set option.");
		datepager.wijdatepager("option", "cultureCalendar", "Hebrew");
		previousMonth = $(".wijmo-wijdatepager-pagelabel", datepager).first();
		ok(previousMonth.html().indexOf("תשרי") > -1, "Hebrew calendar  is applied to cultureCalendar option by set option.");
		datepager.remove();
	});

	test("culture and cultureCalendar-year and month format", function () {
	   var datepager = createDatepager({
	        viewType: "month",
	        selectedDate: new Date("2014/2/1")
	    });

	    ok($(".wijmo-wijdatepager-pagelabel-first").text() === "2013", "default year label is right!.");
	    datepager.remove();

	    datepager = createDatepager({
	        viewType: "month",
	        culture: "ja-JP",
	        selectedDate: new Date("2014/2/1")
	    });

	    ok($(".wijmo-wijdatepager-pagelabel-first").text() === "2013", "default Japanese year label is right!.");
	    datepager.remove();

	    datepager = createDatepager({
	        viewType: "month",
	        culture: "ja-JP",                    
	        cultureCalendar: "Japanese",
	        selectedDate: new Date("2014/2/1")
	    });

	    ok($(".wijmo-wijdatepager-pagelabel-first").text() === "0025", "default Japanese year label is right!.");
	    datepager.remove();

	    datepager = createDatepager({
	        viewType: "month",
	        culture: "ja-JP",
	        cultureCalendar: "Japanese",
	        localization: { monthViewYearLabelFormat: "{0:gg y}", dayViewMonthLabelFormat: "{0:MMM}" },
	        selectedDate: new Date("2014/2/1")
	    });

	    ok($(".wijmo-wijdatepager-pagelabel-first").text() === "平成 25", "year label is right by monthViewYearLabelFormat setting!.");
	    ok($(".wijmo-wijdatepager-pagelabel-last").text() === "平成 27", "year label is right by monthViewYearLabelFormat setting!.");
	    datepager.remove();

	    datepager = createDatepager({
	        viewType: "month",
	        localization: { monthViewYearLabelFormat: "{0:yyyy-aa}", dayViewMonthLabelFormat: "{0:MMM}" },
	        selectedDate: new Date("2014/2/1")
	    });

	    ok($(".wijmo-wijdatepager-pagelabel-first").text() === "2013-aa", "year label is right by monthViewYearLabelFormat setting!");
        
	    datepager.remove();

	    //month
	    datepager = createDatepager({
	        viewType: "day",	        
	        selectedDate: new Date("2014/2/1")
	    });

	    ok($(".wijmo-wijdatepager-pagelabel-first").text() === "Jan", "default month label is right.");

	    datepager.remove();

	    datepager = createDatepager({
	        viewType: "day",
	        culture: "ja-JP",
	        cultureCalendar: "Japanese",
	        selectedDate: new Date("2014/2/1")
	    });

	    ok($(".wijmo-wijdatepager-pagelabel-first").text() === "1", "default Japanese year label is right by setting cultrue!.");
	    datepager.remove();

	    datepager = createDatepager({
	        viewType: "day",
	        localization: { dayViewMonthLabelFormat: "{0:MMMM}" },
	        selectedDate: new Date("2014/2/1")
	    });

	    ok($(".wijmo-wijdatepager-pagelabel-first").text() === "January", "month label is right by localization setting.");

	    datepager.remove();

	    datepager = createDatepager({
	        viewType: "day",
	        culture: "ja-JP",
	        cultureCalendar: "Japanese",
	        localization: { dayViewMonthLabelFormat: "{0:MMMM}" },
	        selectedDate: new Date("2014/2/1")
	    });

	    ok($(".wijmo-wijdatepager-pagelabel-first").text() === "1月", "month label is right by cultrue and localization setting.");

	    datepager.remove();

	    datepager = createDatepager({
	        viewType: "day",
	        culture: "ja-JP",	        
	        selectedDate: new Date("2014/2/1")
	    });

	    ok($(".wijmo-wijdatepager-pagelabel-first").text() === "1", "month label is right by cultrue and localization setting.");

	    datepager.remove();

	    datepager = createDatepager({
	        viewType: "day",
	        culture: "ja-JP",
	        localization: { dayViewMonthLabelFormat: "{0:MMMM}" },
	        selectedDate: new Date("2014/2/1")
	    });

	    ok($(".wijmo-wijdatepager-pagelabel-first").text() === "1月", "month label is right by cultrue and localization setting.");

	    datepager.remove();
	});

	test("disabled", function () {
	    var datepager, previousMonth;

	    datepager = createDatepager({
	        selectedDate: new Date("2014/2/1"),
	        disabled: true
	    });
	    ok($(".wijmo-wijdatepager-decrement").hasClass("ui-state-disabled"), "The disabled initialization is correct");
	    datepager.remove();
	});

}(jQuery));