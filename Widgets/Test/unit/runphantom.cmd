SET RUNTEST=..\..\tools\phantomjs ..\..\tools\run-qunit.js

REM ====== Wijmo-Open ======
%RUNTEST% wijaccordion\wijaccordion.html
%RUNTEST% wijcalendar\wijcalendar.html
%RUNTEST% wijdialog\wijdialog.html
%RUNTEST% wijdropdown\wijdropdown.html
%RUNTEST% wijexpander\wijexpander.html
%RUNTEST% wijformdecorator\wijformdecorator.html
%RUNTEST% wijlist\wijlist.html
%RUNTEST% wijmenu\wijmenu.html
%RUNTEST% wijpopup\wijpopup.html
%RUNTEST% wijprogressbar\wijprogressbar.html
%RUNTEST% wijslider\wijslider.html
%RUNTEST% wijsplitter\wijsplitter.html
%RUNTEST% wijsuperpanel\wijsuperpanel.html
%RUNTEST% wijtabs\wijtabs.html
%RUNTEST% wijtooltip\wijtooltip.html
%RUNTEST% wijtouchutil\wijtouchutil.html
%RUNTEST% wijvideo\wijvideo.html
%RUNTEST% wijcheckbox\wijcheckbox.html


REM ====== Wijmo-Pro ======
%RUNTEST% wijbarchart/wijbarchart.html
%RUNTEST% wijbubblechart/wijbubblechart.html
%RUNTEST% wijcandlestickchart/wijcandlestickchart.html
%RUNTEST% wijcarousel/wijcarousel.html
%RUNTEST% wijchartnavigator/wijchartnavigator.html
%RUNTEST% wijcombobox/wijcombobox.html
%RUNTEST% wijcompositechart/wijcompositechart.html
%RUNTEST% wijdatasource/wijdatasource.html
%RUNTEST% wijdatepager/wijdatepager.html
%RUNTEST% wijevcal/wijevcal.html
%RUNTEST% wijfilter/wijfilter.html
%RUNTEST% wijgallery/wijgallery.html
%RUNTEST% wijgrid/wijgrid.html
%RUNTEST% wijlightbox/wijlightbox.html
%RUNTEST% wijlineargauge/wijlineargauge.html
%RUNTEST% wijlinechart/wijlinechart.html
%RUNTEST% wijpager/wijpager.html
%RUNTEST% wijpiechart/wijpiechart.html
%RUNTEST% wijradialgauge/wijradialgauge.html
%RUNTEST% wijrating/wijrating.html
%RUNTEST% wijscatterchart/wijscatterchart.html
%RUNTEST% wijtree/wijtree.html
%RUNTEST% wijtreemap/wijtreemap.html
%RUNTEST% wijupload/wijupload.html
%RUNTEST% wijwizard/wijwizard.html


REM ====== Others ======
%RUNTEST% others/others.html