﻿commonWidgetTests('wijwizard', {
    defaults: {
        wijCSS: $.extend({}, $.wijmo.wijCSS, {
            wijwizard: "wijmo-wijwizard",
            wijwizardButtons: "wijmo-wijwizard-buttons",
            wijwizardPrev: "wijmo-wijwizard-prev",
            wijwizardNext: "wijmo-wijwizard-next",
            wijwizardSteps: "wijmo-wijwizard-steps",
            wijwizardContent: "wijmo-wijwizard-content",
            wijwizardPanel: "wijmo-wijwizard-panel",
            wijwizardActived: "wijmo-wijwizard-actived",
            wijwizardHide: "wijmo-wijwizard-hide",
            wijwizardSpinner: "wijmo-wijwizard-spinner"
        }),
        wijMobileCSS: {
            header: "ui-header ui-bar-a",
            content: "ui-body-b",
            stateDefault: "ui-btn ui-btn-a",
            stateHover: "ui-btn-down-a",
            stateActive: "ui-btn-down-a"
        },
        navButtons: 'auto',
        autoPlay: false,
        delay: 3000,
        loop: false,
        hideOption: { fade: true },
        showOption: { fade: true, duration: 400 },
        ajaxOptions: null,
        cache: false,
        cookie: null,
        stepHeaderTemplate: '',
        panelTemplate: '',
        spinner: '',
        backBtnText: 'back',
        nextBtnText: 'next',
        add: null,
        remove: null,
        activeIndexChanged: null,
        show: null,
        load: null,
        validating: null
    }
});