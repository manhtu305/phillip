﻿(function ($) {

    module("wijwizard:options");

    test("navButtons", function () {

        var wizard = createWizard();
        wizard.wijwizard("option", "navButtons", "none");
        ok(!wizard.hasClass(".ui-button").length, "wijwizard set option navButtons none ");
        ok(wizard.find(".wijmo-wijwizard-buttons").length === 0);
        wizard.wijwizard("option", "navButtons", "auto");
        ok(wizard.find(".ui-button").length === 2);
        ok(wizard.find(".wijmo-wijwizard-buttons").length === 1);
        wizard.remove();
    });

    test("autoPlay", function () {
        stop();
        var wizard = createWizard({
            delay: 300,
            //autoPlay: true,
            showOption: { duration: 0 },
            hideOption: { duration: 0 }

        });
        wizard.wijwizard("add").wijwizard("option", "autoPlay", true);
        ok(wizard.wijwizard("option").activeIndex === 0);
        setTimeout(function () {
            ok(wizard.wijwizard("option").activeIndex === 1);
            ok(wizard.find(".wijmo-wijwizard-panel").eq(0).is(":hidden"));
            ok(wizard.find(".wijmo-wijwizard-panel").eq(1).is(":visible"));
            ok(wizard.find(".wijmo-wijwizard-panel").eq(2).is(":hidden"));
            wizard.wijwizard("option", "autoPlay", false);
            setTimeout(function () {
                ok(wizard.wijwizard("option").activeIndex === 1);
                ok(wizard.find(".wijmo-wijwizard-panel").eq(0).is(":hidden"));
                ok(wizard.find(".wijmo-wijwizard-panel").eq(1).is(":visible"));
                ok(wizard.find(".wijmo-wijwizard-panel").eq(2).is(":hidden"));
                start();
                wizard.remove();
            }, 300);
        }, 300);
    });

    test("disable", function () {
        var wizard = createWizard({ disabled: true });
        ok(wizard.hasClass('ui-state-disabled'));
        wizard.wijwizard({ disabled: false });
        ok(!wizard.hasClass('ui-state-disabled'));
        wizard.wijwizard("option", "disabled", true);
        ok(wizard.hasClass('ui-state-disabled'));
        wizard.wijwizard("option", "disabled", false);
        ok(!wizard.hasClass('ui-state-disabled'));
        wizard.remove();
    });

    test("delay", function () {
        stop();
        var wizard = createWizard({
            delay: 300,
            //autoPlay: true,
            showOption: { duration: 0 },
            hideOption: { duration: 0 }
        });
        //wizard.wijwizard("add");
        ok(wizard.wijwizard("option").activeIndex === 0);
        wizard.wijwizard("add").wijwizard("option", "autoPlay", true);
        setTimeout(function () {
            ok(wizard.wijwizard("option").activeIndex === 1);
            ok(wizard.find(".wijmo-wijwizard-panel").eq(0).is(":hidden"));
            ok(wizard.find(".wijmo-wijwizard-panel").eq(1).is(":visible"));
            ok(wizard.find(".wijmo-wijwizard-panel").eq(2).is(":hidden"));
            //wizard.wijwizard("stop");
            wizard.wijwizard("option", "delay", 100);
            wizard.wijwizard("option", "autoPlay", true);
            setTimeout(function () {
                ok(wizard.wijwizard("option").activeIndex === 2);
                ok(wizard.find(".wijmo-wijwizard-panel").eq(0).is(":hidden"));
                ok(wizard.find(".wijmo-wijwizard-panel").eq(1).is(":hidden"));
                ok(wizard.find(".wijmo-wijwizard-panel").eq(2).is(":visible"));
                wizard.remove();
                start();
            }, 100);
        }, 300);
    });

    test("loop", function () {
        stop();
        var wizard = createWizard({
            delay: 300,
            loop: true,
            autoPlay: true,
            showOption: { duration: 0 },
            hideOption: { duration: 0 }
        });
        setTimeout(function () {
            ok(wizard.wijwizard("option").activeIndex === 1);
            ok(wizard.find(".wijmo-wijwizard-panel").eq(0).is(":hidden"));
            ok(wizard.find(".wijmo-wijwizard-panel").eq(1).is(":visible"));
            setTimeout(function () {
                ok(wizard.wijwizard("option").activeIndex === 0);
                ok(wizard.find(".wijmo-wijwizard-panel").eq(0).is(":visible"));
                ok(wizard.find(".wijmo-wijwizard-panel").eq(1).is(":hidden"));
                setTimeout(function () {
                    ok(wizard.wijwizard("option").activeIndex === 1);
                    ok(wizard.find(".wijmo-wijwizard-panel").eq(0).is(":hidden"));
                    ok(wizard.find(".wijmo-wijwizard-panel").eq(1).is(":visible"));
                    start();
                    wizard.remove();
                }, 300);
            }, 300);
        }, 300);
    });

    test("Navigation after recreate buttons", function () {
        var $widget = createWizard({
            showOption: { duration: 0 },
            hideOption: { duration: 0 },
        }), nextButton, panels;
        $widget.wijwizard("option", "navButtons", "edge");

        nextButton = $widget.data("wijmo-wijwizard").nextBtn;
        nextButton.simulate("click");

        panels = $widget.data("wijmo-wijwizard").panels;
        ok($(panels[0]).is(":hidden") && $(panels[1]).is(":visible"), "NavButtons worked !");

        $widget.remove();
    });
})(jQuery)