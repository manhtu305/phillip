﻿(function ($) {
    module("wijwizard:tickets");

    test("#41816", function () {
        var $widget = createWizard(),
            list = $(".wijmo-wijwizard-steps"),
            content = $(".wijmo-wijwizard-content"),
            buttons = $(".wijmo-wijwizard-buttons");

        ok(list.outerHeight(true) + content.outerHeight(true) + buttons.outerHeight(true) === $widget.height(), "We will adjust the content container to fit in the wijwizard control, if we set the height style for wijwizard control.");
        $widget.remove();
    });

    test("#42962", function () {
        $widget = createWizard({ navButtons: "none" });
        ok(true, "No exception thrown");
        $widget.remove();
    });

    test("#44204", function () {
        var eleStr = "<div id='C1Wizard1' style='display:none; width:50px'>" +
                        "<ul><li><h1>Step1</h1></li><li><h1>Step2</h1></li><li><h1>Step3</h1></li></ul>" +
                        "<div id='C1Wizard1_C1Wizard1Step1'></div>" +
                        "<div id='C1Wizard1_C1Wizard1Step2'></div>" +
                        "<div id='C1Wizard1_C1Wizard1Step3'></div>" +
                     "</div>",
        element = $(eleStr).appendTo("body"),
        $widget = element.wijwizard();
        $widget.show();
        $widget.wijTriggerVisibility();
        ok(true, "No exception thrown");
        $widget.remove();
        element.remove();
    });

    test("ActiveIndexChanged event should fired once after setting some options", function () {
        var $widget = createWizard(), invokeCount = 0;
        $widget.wijwizard({
            showOption: { duration: 0 },
            hideOption: { duration: 0 },
            add: function (e, ui) {
                $(ui.panel).html("Content of newly added panel");
            },
            activeIndexChanged: function () {
                invokeCount++;
            }
        });
        $widget.wijwizard("add", 1, "Step 3", "This panel has been added");
        $widget.find(".ui-button-text-only").eq(1).simulate('click');
        ok(invokeCount === 1, "activeIndexChanged should be fired once");
        $widget.remove();
    });

    test("#83240", function () {
        var $widget = createWizard(),
            $largeBox = $("<div style='height: 1000px'></div>"),
            $btnNext = $widget.find(".wijmo-wijwizard-buttons>a").eq(0),
            $btnPrev = $widget.find(".wijmo-wijwizard-buttons>a").eq(1),
            scrollElement = $.browser.webkit ? "body" : "html",
            actualScrollTop;

        $widget.before($largeBox);
        $(scrollElement).scrollTop(1500);
        actualScrollTop = $(scrollElement).scrollTop();
        $btnNext.click();
        ok($(scrollElement).scrollTop() === actualScrollTop, "Clicking next button does not force page to scroll to top, or window is large enough.");
        $btnPrev.click();
        ok($(scrollElement).scrollTop() === actualScrollTop, "Clicking prev button does not force page to scroll to top, or window is large enough.");
        $widget.remove();
        $largeBox.remove();
    });
})(jQuery);