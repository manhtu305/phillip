﻿(function ($) {
    module("wijwizard:method");

    test("public method:count", function () {
        var wizard = createWizard(),
            length = wizard.wijwizard("count");
        ok(length === 2, "get wizard panel count");
        ok(wizard.find(".ui-widget-header").length === 2);
        ok(wizard.find(".wijmo-wijwizard-panel").length === 2);
        wizard.remove();
    });

    test("public method:add without params", function () {
        var wizard = createWizard(),
            length = wizard.wijwizard("count"),
            newLength;
        if (length === 2) {
            wizard.wijwizard("add");
            newLength = wizard.wijwizard("count");
            ok((newLength - length) === 1, "test wizard add ");
            ok(wizard.find(".ui-widget-header").length === 3);
            ok(wizard.find(".wijmo-wijwizard-panel").length === 3);
        } else {
            ok(false);
        }
        wizard.remove();
    });

    test("public method:add", function () {
        var wizard = createWizard(),
            length = wizard.wijwizard("count"),
            newLength;
        if (length === 2) {
            wizard.wijwizard("add", 1, "testAddTitle", "test add 1");
            newLength = wizard.wijwizard("count");
            ok((newLength - length) === 1, "test wizard add ");
            ok(wizard.find(".ui-widget-header").length === 3);
            ok(wizard.find(".wijmo-wijwizard-panel").length === 3);
            ok(wizard.find(".ui-widget-header").eq(1)[0].firstChild.innerHTML === "testAddTitle");
            ok(wizard.find(".ui-widget-header").eq(1)[0].lastChild.nodeValue === "test add 1");
        } else {
            ok(false);
        }
        wizard.remove();
    });

    test("public method:remove", function () {
        var wizard = createWizard(),
            length = wizard.wijwizard("count"),
            newLength;
        if (length === 2) {
            wizard.wijwizard("remove", 1);
            newLength = wizard.wijwizard("count");
            ok((length - newLength) === 1, "test wizard revmove");
            ok(wizard.find(".ui-widget-header").length === 1);
            ok(wizard.find(".wijmo-wijwizard-panel").length === 1);
        } else {
            ok(false);
        }
        wizard.remove();
    });

    test("public method:show", function () {
        var wizard = createWizard({
            showOption: { duration: 0 },
            hideOption: { duration: 0 },
            delay: 300
        }),
            index;

        ok(wizard.find(".wijmo-wijwizard-panel").eq(0).is(":visible"));
        ok(wizard.find(".wijmo-wijwizard-panel").eq(1).is(":hidden"));
        wizard.wijwizard("show", 1);
        index = wizard.wijwizard("option").activeIndex;
        ok(index === 1, "test wizard show ");
        ok(wizard.find(".wijmo-wijwizard-panel").eq(0).is(":hidden"));
        ok(wizard.find(".wijmo-wijwizard-panel").eq(1).is(":visible"));
        wizard.remove();
    });

    test("public method:next", function () {
        var wizard = createWizard({
            showOption: { duration: 0 },
            hideOption: { duration: 0 },
            delay: 300
        });

        ok(wizard.find(".wijmo-wijwizard-panel").eq(0).is(":visible"));
        ok(wizard.find(".wijmo-wijwizard-panel").eq(1).is(":hidden"));
        ok(wizard.wijwizard("option").activeIndex === 0, "test wizard next ");
        wizard.wijwizard("next");
        ok(wizard.wijwizard("option").activeIndex === 1, "test wizard next ");
        ok(wizard.find(".wijmo-wijwizard-panel").eq(0).is(":hidden"));
        ok(wizard.find(".wijmo-wijwizard-panel").eq(1).is(":visible"));
        wizard.remove();
    });

    test("public method:back", function () {
        var wizard = createWizard({
            showOption: { duration: 0 },
            hideOption: { duration: 0 },
            delay: 300
        });

        wizard.wijwizard("next");
        ok(wizard.find(".wijmo-wijwizard-panel").eq(0).is(":hidden"));
        ok(wizard.find(".wijmo-wijwizard-panel").eq(1).is(":visible"));
        ok(wizard.wijwizard("option").activeIndex === 1, "test wizard back ");
        wizard.wijwizard("back");
        ok(wizard.wijwizard("option").activeIndex === 0, "test wizard back ");
        ok(wizard.find(".wijmo-wijwizard-panel").eq(0).is(":visible"));
        ok(wizard.find(".wijmo-wijwizard-panel").eq(1).is(":hidden"));
        wizard.remove();

    });
    /**
    test("public method:load", function () {
        var wizard = createWizard();
    });
    */
    
    test("Test the primary header after invoke next and back method", function () {
        var wizard = createWizard({
            showOption: { duration: 0 },
            hideOption: { duration: 0 },
            add: function (e, ui) {
                $(ui.panel).html("Content of newly added panel");
            }
        });
        ok(wizard.find(".ui-widget-header").eq(0).hasClass("ui-priority-primary"));
        wizard.wijwizard("add", 1, "Step 3", "This panel has been added");
        ok(wizard.find(".ui-widget-header").eq(0).hasClass("ui-priority-primary"));
        wizard.wijwizard("next");
        ok(wizard.find(".ui-widget-header").eq(1).hasClass("ui-priority-primary"));
        wizard.wijwizard("next");
        ok(wizard.find(".ui-widget-header").eq(2).hasClass("ui-priority-primary"));
        wizard.wijwizard("remove", 1);
        wizard.wijwizard("back");
        ok(wizard.find(".ui-widget-header").eq(0).hasClass("ui-priority-primary"));
        wizard.remove();
    });

    test("public method:play", function () {
        stop();
        var wizard = createWizard({
            showOption: { duration: 0 },
            hideOption: { duration: 0 },
            delay: 300
        });
        wizard.wijwizard("play");
        setTimeout(function () {
            ok(wizard.wijwizard("option").activeIndex === 1, "test had auto play ");
            ok(wizard.find(".wijmo-wijwizard-panel").eq(0).is(":hidden"));
            ok(wizard.find(".wijmo-wijwizard-panel").eq(1).is(":visible"));
            wizard.remove();
            start();
        }, 300);
    });

    test("public method:stop", function () {
        stop();
        var wizard = createWizard({
            showOption: { duration: 0 },
            hideOption: { duration: 0 },
            delay: 300
        });
        wizard.wijwizard("add").wijwizard("add");
        wizard.wijwizard("play");
        setTimeout(function () {
            ok(wizard.find(".wijmo-wijwizard-panel").eq(0).is(":hidden"));
            ok(wizard.find(".wijmo-wijwizard-panel").eq(1).is(":visible"));
            ok(wizard.wijwizard("option").activeIndex === 1);
            wizard.wijwizard("stop");
            setTimeout(function () {
                ok(wizard.wijwizard("option").activeIndex === 1, "test auto play ");
                if (wizard.wijwizard("option").activeIndex) {
                    ok(wizard.find(".wijmo-wijwizard-panel").eq(0).is(":hidden"));
                    ok(wizard.find(".wijmo-wijwizard-panel").eq(1).is(":visible"));
                } else {
                    ok(false);
                }
                wizard.remove();
                start();
            }, 300);
        }, 300);
    });

    /**
    test("public method:url", function () {
        
        var wizard = createWizard(),
            testStr = "this is test string";
            
        wizard.wijwizard("url", 1, testStr);
         
            
    });

    test("public method:abort", function () {
        
        var wizard = createWizard();
         
    });
    */
})(jQuery)