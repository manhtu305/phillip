﻿
function getWizardDom() {
    var dom = "<div style='height:200px;'><ul><li><h1>Step 1</h1>This is the first step</li><li><h1>Step 2</h1>This is the second step</li></ul>" +
        "<div><p>" +
        "Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur necarcu. " +
        "Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. " +
        "Etiam aliquet massa et lorem. Mauris dapibus lacus auctor risus. Aenean tempor ullamcorperleo. " +
        "Vivamus sed magna quis ligula eleifend adipiscing. Duis orci. Aliquam sodales " +
        "tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel " +
        "pede varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. " +
        "Nunc tristique tempus lectus.</p></div>" +
        "<div><p>" +
        "Morbi tincidunt, dui sit amet facilisis feugiat, odio metus gravida ante, ut pharetra " +
        "massa metus id nunc. Duis scelerisque molestie turpis. Sed fringilla, massa eget " +
        "luctus malesuada, metus eros molestie lectus, ut tempus eros massa ut dolor. Aenean " +
        "aliquet fringilla sem. Suspendisse sed ligula in ligula suscipit aliquam. Praesent " +
        "in eros vestibulum mi adipiscing adipiscing. Morbi facilisis. Curabitur ornare consequat " +
        "nunc. Aenean vel metus. Ut posuere viverra nulla. Aliquam erat volutpat. Pellentesque " +
        "convallis. Maecenas feugiat, tellus pellentesque pretium posuere, felis lorem euismod " +
        "felis, eu ornare leo nisi vel felis. Mauris consectetur tortor et purus.</p></div></div>";
    return dom;
}

function createWizard(options) {
    var dom = getWizardDom();
    return $(dom).appendTo("body").wijwizard(options);
}

(function ($) {
    module("wijwizard:core");

    test("wijwizard.create", function () {
        var wizard = createWizard();
        ok((wizard.hasClass("wijmo-wijwizard ui-widget ui-helper-clearfix")), "wizard css create success ");
        ok((wizard.find("ul").hasClass("ui-widget ui-helper-reset wijmo-wijwizard-steps ui-helper-clearfix")), "ul css init is success");
        ok((wizard.find("ul").find("li").hasClass("ui-widget-header ui-corner-all")), "li css init is success");
        ok((wizard.find(".wijmo-wijwizard-buttons").find("a").hasClass("ui-widget ui-state-default ui-corner-all ui-button ui-button-text-only")), "button css init is success");

        ok(wizard.find(".wijmo-wijwizard-panel").eq(0).is(":visible"), "first panel is active is visible");
        ok(wizard.find(".wijmo-wijwizard-panel").eq(1).is(":hidden"), "other panel is unactive is hidden");
        wizard.remove();
    });

    test("wijwizard.destory", function () {
        var dom = getWizardDom();
        var $d=$(dom).appendTo("body");
        domEqual($d, function () {
            $d.wijwizard().wijwizard("destroy");
        }); 
        $d.remove();
    });
})(jQuery);