﻿(function ($) {
    module("wijwizard:event");

    test("wijwizard event test add without param", function () {
        var arg1,
            arg2,
            flag = false,
            wizard = createWizard({
                add: function (e, args) {
                    arg1 = e;
                    arg2 = args;
                    flag = true;

                }
            });

        wizard.wijwizard("add");
        ok(flag, "add event had trigger");
        ok((typeof arg1 === "object") && arg1.timeStamp > 0, "params1 is jqueryevent params");
        ok(typeof arg2 === "object" && arg2.index === 2, "the index value of params2 is right");
        ok(typeof arg2 === "object" && $(arg2.panel) !== null, "params2 is object and the panel of params2 is dom");
        wizard.remove();
    });

    test("wijwizard event test add param", function () {
        var arg1,
            arg2,
            flag = false,
            wizard = createWizard({
                add: function (e, args) {
                    arg1 = e;
                    arg2 = args;
                    flag = true;

                }
            });

        wizard.wijwizard("add", 1, "testAddTitle", "test add 1");
        ok(flag, "add event had trigger");
        ok((typeof arg1 === "object") && arg1.timeStamp > 0, "params1 is jqueryevent params");
        ok(typeof arg2 === "object" && arg2.index === 1, "the index value of params2 is right");
        ok(typeof arg2 === "object" && $(arg2.panel) !== null, "params2 is object and the panel of params2 is dom");
        wizard.remove();
    });

    test("wijwizard event test remove", function () {
        var arg1,
            arg2,
            flag = false,
            wizard = createWizard({
                remove: function (e, args) {
                    arg1 = e;
                    arg2 = args;
                    flag = true;

                }
            });

        wizard.wijwizard("remove", 0);
        ok(flag, "remove event had trigger");
        ok((typeof arg1 === "object") && arg1.timeStamp > 0, "params1 is jqueryevent params");
        ok(typeof arg2 === "object" && arg2.index === -1, "the index value of params2 is right");
        ok(typeof arg2 === "object" && $(arg2.panel) !== null, "params2 is object and the panel of params2 is dom");
        wizard.remove();
    });

    test("wijwizard event test activeIndexChanged", function () {
        var arg1,
            arg2,
            flag = false,
            wizard = createWizard({
                activeIndexChanged: function (e, args) {
                    arg1 = e;
                    arg2 = args;
                    flag = true;
                },
                showOption: { duration: 0 },
                hideOption: { duration: 0 }
            });

        wizard.wijwizard("next");
        ok(flag, "activeIndexChanged event had trigger");
        ok((typeof arg1 === "object") && arg1.timeStamp > 0, "params1 is jqueryevent params");
        ok(typeof arg2 === "object" && arg2.index === 1, "the index value of params2 is right");
        ok(typeof arg2 === "object" && $(arg2.panel) !== null, "params2 is object and the panel of params2 is dom");
        wizard.remove();
    });

    test("wijwizard event test show", function () {
        var arg1,
            arg2,
            flag = false,
            wizard = createWizard({
                show: function (e, args) {
                    arg1 = e;
                    arg2 = args;
                    flag = true;
                },
                showOption: { duration: 0 },
                hideOption: { duration: 0 }

            });
        wizard.wijwizard("show", 1);
        ok(flag, "show event had trigger");
        ok((typeof arg1 === "object") && arg1.timeStamp > 0, "params1 is jqueryevent params");
        ok(typeof arg2 === "object" && arg2.index === 1, "the index value of params2 is right");
        ok(typeof arg2 === "object" && $(arg2.panel) !== null, "params2 is object and the panel of params2 is dom");
        wizard.remove();
    });

})(jQuery)