﻿(function ($) {
    module("wijscatterchart: tickets");
    test("when call redraw method, the page throw a javascript exception.", function () {
        var scatterchart = createSimpleScatterchart(),
                seriesList = [],
                bar;
        seriesList.push({ data: { x: ['x1', 'x2', 'x3', 'x4'], y: [5, 10, 12, 20]} });
        scatterchart.wijscatterchart('option', 'seriesList', seriesList);
        scatterchart.wijscatterchart("redraw");
        ok(true, 'ok.');
        scatterchart.remove();
    });

    test("data test", function () {
        var scatterchart = createSimpleScatterchart(),
                seriesList = [],
                bar;
        seriesList.push({ data: { x: ['x1', 'x2', 'x3', 'x4'], y: [5, 10, 12, 20]} });
        scatterchart.wijscatterchart('option', 'seriesList', seriesList);
        ok(true, 'x string, y number ok.');

        seriesList = [];
        seriesList.push({ data: { x: ['x1', 'x2', 'x3', 'x4'], y: [new Date("1/3/2010"), new Date("1/4/2010"), new Date("1/6/2010"), new Date("1/8/2010")]} });
        scatterchart.wijscatterchart('option', 'seriesList', seriesList);
        ok(true, 'x string, y datetime ok.');

        seriesList = [];
        seriesList.push({ data: { x: [2, 77, 23, 45], y: [5, 10, 12, 20]} });
        scatterchart.wijscatterchart('option', 'seriesList', seriesList);
        ok(true, 'x number, y number ok.');

        seriesList = [];
        seriesList.push({ data: { x: [2, 77, 23, 45], y: [new Date("1/3/2010"), new Date("1/4/2010"), new Date("1/6/2010"), new Date("1/8/2010")]} });
        scatterchart.wijscatterchart('option', 'seriesList', seriesList);
        ok(true, 'x number, y datetime ok.');

        seriesList = [];
        seriesList.push({ data: { x: [new Date("2/5/2011"), new Date("2/10/2011"), new Date("2/7/2012"), new Date("2/11/2011")], y: [5, 10, 12, 20]} });
        scatterchart.wijscatterchart('option', 'seriesList', seriesList);
        ok(true, 'x datetime, y number ok.');

        seriesList = [];
        seriesList.push({ data: { x: [new Date("2/5/2011"), new Date("2/10/2011"), new Date("2/7/2012"), new Date("2/11/2011")], y: [new Date("1/3/2010"), new Date("1/4/2010"), new Date("1/6/2010"), new Date("1/8/2010")]} });
        scatterchart.wijscatterchart('option', 'seriesList', seriesList);
        ok(true, 'x datetime, y datetime ok.');

        scatterchart.remove();
    });

    test("When min/max of axis option is set, autoMin/autoMax should be set to false", function () {
        var scatterChart = createScatterchart({
            axis: {
                x: {
                    max: 10,
                    min: 0
                },
                y: {
                    min: 0,
                    max: 30
                }
            },
            seriesList: [{ data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20] } }]
        });

        var axis = scatterChart.wijscatterchart("option", "axis");
        ok(axis.x.autoMin === false && axis.x.min === 0, "autoMin of x axis is set to false when axis.x.min is set.");
        ok(axis.x.autoMax === false && axis.x.max === 10, "autoMax of x axis is set to false when axis.x.max is set.");
        ok(axis.y.autoMin === false && axis.y.min === 0, "autoMin of y axis is set to false when axis.y.min is set.");
        ok(axis.y.autoMax === false && axis.y.max === 30, "autoMax of y axis is set to false when axis.y.max is set.");
        scatterChart.remove();

        scatterChart = createScatterchart({
            seriesList: [{ data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20] } }]
        });
        axis = scatterChart.wijscatterchart("option", "axis");
        ok(axis.x.autoMin === true, "autoMin of x axis is true.");
        ok(axis.x.autoMax === true, "autoMax of x axis is true.");
        ok(axis.y.autoMin === true, "autoMin of y axis is true.");
        ok(axis.y.autoMax === true, "autoMax of y axis is true.");
        scatterChart.wijscatterchart("option", "axis", {
            x: {
                max: 10,
                min: 0
            },
            y: {
                min: 0,
                max: 30
            }
        });
        axis = scatterChart.wijscatterchart("option", "axis");
        ok(axis.x.autoMin === false && axis.x.min === 0, "autoMin of x axis is set to false when axis.x.min is set.");
        ok(axis.x.autoMax === false && axis.x.max === 10, "autoMax of x axis is set to false when axis.x.max is set.");
        ok(axis.y.autoMin === false && axis.y.min === 0, "autoMin of y axis is set to false when axis.y.min is set.");
        ok(axis.y.autoMax === false && axis.y.max === 30, "autoMax of y axis is set to false when axis.y.max is set.");
        scatterChart.remove();
    });

	//Test for IE8
    if ($.browser.msie && $.browser.version && parseFloat($.browser.version) <= 8) {
    	test("#67756", function () {
    		var scatterchart = createSimpleScatterchart();
    		scatterchart.wijscatterchart('redraw');
    		ok(true, "It won't throw errors when redraw the scatter chart before the animation finished.");
    		scatterchart.remove();
    	})
    }

    test("#71878", function () {
        var seriesEles, trendlinePath,
            scatterChart = createScatterchart({
                seriesList: [{
                    label: "Trendline",
                    legendEntry: true,
                    isTrendline: true,
                    data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [107, 31, 635, 203] }
                }],
                mouseOver: function (e, chartObj) {
                    if (chartObj !== null) {
                        ok(chartObj.type === "trendLine", 'Hover on the trendline.');
                    }
                },
                seriesStyles: [{
                    stroke: "#FFA41C", "stroke-width": 1, "stroke-opacity": 0.7
                }],
                seriesHoverStyles: [{
                    "stroke-width": 5
                }]
            });

        seriesEles = scatterChart.data("wijmo-wijscatterchart").chartElement.data("fields").seriesEles;
        ok(seriesEles.length === 1 && seriesEles[0].isTrendline, "The trendline is drawn.");
        trendlinePath = seriesEles[0].path;
        ok(trendlinePath.attr("stroke-width") === 1, "SeriesStyle is applied on trendline");
        $(trendlinePath.node).trigger('mouseover');
        ok(trendlinePath.attr("stroke-width") === 5, "HoverSeriesStyle is applied on trendline");
        $(trendlinePath.node).trigger('mouseout');
        ok(trendlinePath.attr("stroke-width") === 1, "HoverSeriesStyle is removed and seriesStyle is applied on trendline");
        scatterChart.remove();
    });

    if (Raphael.svg) {
        test("#75398", function () {
            var scatterChart = createScatterchart({
                animation: {
                    enabled: false
                },
                seriesList: [{ data: { x: [1], y: [5] } }]
            }), scatters, transformArray, x, y, transform;

            scatters = scatterChart.find("circle.wijchart-canvas-object.wijscatterchart");

            if (scatters.length > 0) {
                x = parseFloat(scatters.attr("cx")) / 2;
                y = parseFloat(scatters.attr("cy")) / 2;
                transformArray = ["matrix(1.5", "0", "0", "1.5", "-" + x, "-" + y + ")"];
                transform = $.browser.msie ? transformArray.join(" ") : transformArray.join(",");

                scatters.simulate("mouseover");

                ok(scatters.attr("transform") === transform, "Scale method works well");
            }
            scatterChart.remove();
        });
    }

    test("Issue about chart label show and hide", function () {
        var scatterChart = createScatterchart({
            showChartLabels: true,
            animation: {
                enabled: false
            },
            seriesList: [{ data: { x: [1], y: [5] } }]
        }), labels, legendItem, 
            ie8 = ($.browser.msie && $.browser.version && parseFloat($.browser.version) <= 8);

        labels = !ie8 ? scatterChart.find("g").find("text") : scatterChart.find("shape.wijscatterchart").next();
        legendItem = scatterChart.find(".wijchart-legend-icon");

        legendItem.trigger("click");

        ok(labels.css("display") === "none", "Labels are hidden !");

        scatterChart.remove();
    });

    test("#77157, test render scatter chart", function () {
    	var scatterChart = createScatterchart({
    		axis: {
    			x: { text: "x" },
    			y: [{ text: "y", compass: "east" }, { text: "y2", compass: "west" }]
    		},
    		data: {
    			x: ["2012-Q1", "2012-Q2", "2012-Q3", "2012-Q4", "2013-Q1", "2013-Q2"]
    		},
    		seriesList: [{
    			label: "Q1",
    			legendEntry: true,
    			yAxis: 0,
    			data: {
    				y: [58.19, 69.68, 74.48, 70.82, 75.99, 79.40]
    			}
    		}]
    	});

    	ok(true, "The scatter chart of composite chart was created without errors, when the data series was added at the secondary axis");
    	scatterChart.remove();
    });
})(jQuery);