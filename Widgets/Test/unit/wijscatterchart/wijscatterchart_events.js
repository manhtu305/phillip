﻿/*globals start, stop, test, ok, module, jQuery, 
simpleOptions, createSimpleScatterchart, createScatterchart*/
/*
* wijscatterchart_events.js
*/
"use strict";
(function ($) {
	var scatterchart,
		newSeriesList = [{
			label: "1800",
			legendEntry: true,
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [100, 31, 635, 203] }
		}, {
			label: "1900",
			legendEntry: true,
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [133, 156, 947, 408] }
		}, {
			label: "2008",
			legendEntry: true,
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [973, 914, 4054, 732] }
		}];

	module("wijscatterchart: events");

	test("mousedown", function () {
		scatterchart = createSimpleScatterchart();
		scatterchart.wijscatterchart({
			mouseDown: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 0, 'Press the mouse down on the first scatter.');
					scatterchart.remove();
				}
			}
		});
		stop();
		$(scatterchart.wijscatterchart("getScatter", 0, 0).element).trigger('mousedown');		
	});

	test("mouseup", function () {
		scatterchart = createSimpleScatterchart();
		scatterchart.wijscatterchart({
			mouseUp: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 0, 'Release the mouse on the first scatter.');
					scatterchart.remove();
				}
			}
		});
		stop();
		$(scatterchart.wijscatterchart("getScatter", 0, 0).element).trigger('mouseup');		
	});

	test("mouseover", function () {
		scatterchart = createSimpleScatterchart();
		scatterchart.wijscatterchart({
			mouseOver: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 0, 'Hover the first scatter.');
					scatterchart.remove();
				}
			}
		});
		stop();
		$(scatterchart.wijscatterchart("getScatter", 0, 0).element).trigger('mouseover');		
	});

	test("mouseout", function () {
		scatterchart = createSimpleScatterchart();
		scatterchart.wijscatterchart({
			mouseOut: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 0, 'Move the mouse out of the first scatter.');
					scatterchart.remove();
				}
			}
		});
		stop();
		$(scatterchart.wijscatterchart("getScatter", 0, 0).element).trigger('mouseout');
	});

	test("mousemove", function () {
		scatterchart = createSimpleScatterchart();
		scatterchart.wijscatterchart({
			mouseMove: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 0, 'Move the mouse on the first scatter.');
					scatterchart.remove();
				}
			}
		});
		stop();
		$(scatterchart.wijscatterchart("getScatter", 0, 0).element).trigger('mousemove');
	});

	test("click", function () {
		scatterchart = createSimpleScatterchart();
		scatterchart.wijscatterchart({
			click: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 0, 'Click on the first scatter.');
					scatterchart.remove();
				}
			}
		});
		stop();
		$(scatterchart.wijscatterchart("getScatter", 0, 0).element).trigger('click');
	});

	test("beforeserieschange", function () {
		scatterchart = createSimpleScatterchart();
		var oldSeriesList = scatterchart.wijscatterchart("option", "seriesList");
		scatterchart.wijscatterchart({
			beforeSeriesChange: function (e, seriesObj) {
				if (seriesObj !== null) {
					start();
					var seriesList = scatterchart.wijscatterchart("option", "seriesList");
					ok(seriesObj.oldSeriesList === oldSeriesList, 
							'get the correct old seriesList by seriesObj.oldSeriesList.');
					ok(seriesObj.newSeriesList === newSeriesList, 
							'get the correct new seriesList by seriesObj.newSeriesList.');
					ok(seriesList === oldSeriesList, 
							"currently seriesList hasn't been changed.");
					scatterchart.remove();
				}
			}
		});
		stop();
		scatterchart.wijscatterchart("option", "seriesList", newSeriesList);
	});

	test("serieschanged", function () {
		scatterchart = createSimpleScatterchart();
		scatterchart.wijscatterchart({
			seriesChanged: function (e, seriesObj) {
				if (seriesObj !== null) {
					start();
					var seriesList = scatterchart.wijscatterchart("option", "seriesList");
					ok(seriesList === newSeriesList, 
						"currently seriesList has been changed.");
					scatterchart.remove();
				}
			}
		});
		stop();
		scatterchart.wijscatterchart("option", "seriesList", newSeriesList);
	});

	test("beforepaint, paint", function () {
		var options = $.extend({
				beforePaint: function (e) {
					ok(true, "The beforepaint event is invoked.");
					return false;
				},
				painted: function (e) {
					ok(false, "The painted event of line chart1 is invoked.");
				}
			}, true, simpleOptions),
			scatter, options2, scatterchart2;
		scatterchart = createScatterchart(options);

		try {
			scatter = scatterchart.wijscatterchart("getScatter", 0, 0);
			if (scatter.attrs.stroke) {
				ok(false,
					"The scatter chart1 shouldn't be painted because of the cancellation.");
			}
		}
		catch (ex) {
			ok(true, "The scatter chart1 isn't painted because of the cancellation.");
		}
		scatterchart.remove();

		options2 = $.extend({
			beforePaint: function (e) {
				if (e !== null) {
					ok(true, "The beforepaint event is invoked.");
				}
			},
			painted: function (e) {
				ok(true, "The painted event of line chart2 is invoked.");
			}
		}, true, simpleOptions);
		scatterchart2 = createScatterchart(options2);
		scatter = scatterchart2.wijscatterchart("getScatter", 0, 0);
		ok(scatter !== null, 'The scatter chart2 is painted.');
		scatterchart2.remove();
	});

}(jQuery));