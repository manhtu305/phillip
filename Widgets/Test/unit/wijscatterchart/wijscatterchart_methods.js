﻿/*globals test, ok, module, jQuery, createSimpleLinechart*/
/*
* wijscatterchart_methods.js
*/
"use strict";
(function ($) {

	module("wijscatterchart: methods");

	test("getScatter", function () {
		var scatterchart = createSimpleScatterchart(),
			scatters = scatterchart.wijscatterchart('getScatter', 1),
			scatter = scatterchart.wijscatterchart('getScatter', 1, 1);
		ok(scatters.length === 4, 'getScatter:gets the scatters 1');
		ok(scatter.element.nodeName === 'circle' || scatter.symbolName === 'circle', 'getScatter:gets the scatter 1-1');
		scatterchart.remove();
	});

	test("getCanvas", function () {
		var scatterchart = createSimpleScatterchart(),
			canvas = scatterchart.wijscatterchart('getCanvas');

		ok(canvas.circle !== null, 'getCanvas:gets the canvas for the scatter chart');
		scatterchart.remove();
	});

}(jQuery));