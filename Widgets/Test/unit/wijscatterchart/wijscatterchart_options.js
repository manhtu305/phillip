﻿/*globals test, ok, module, document, jQuery*/
/*
 * wijscatterchart_options.js
 */
"use strict";
(function ($) {

	module("wijscatterchart:options");

	test("seriesListEmpty", function() {
		var scatterchart = createSimpleScatterchart(),
			seriesList = [],
			bar;
		seriesList.push({ data: { x: ['x1', 'x2', 'x3', 'x4'], y: [5, 10, 12, 20]} });
		seriesList.push({ });
		scatterchart.wijscatterchart('option', 'seriesList', seriesList);
		ok(true, 'ok.');
		scatterchart.remove();
	});

	test("axis", function () {
		var scatterchart = $('<div style = "width:400px;height:200px"></div>')
				.appendTo(document.body),
			seriesList = [],
			axis, max, min, unitMajor, unitMinor, formatString, wijscatterchart,
			time = [new Date(2009, 3, 5), new Date(2009, 4, 12), new Date(2009, 5, 20),
				new Date(2009, 8, 30)];

		scatterchart.wijscatterchart();
		seriesList.push({ data: { x: ['x1', 'x2', 'x3', 'x4'], y: [5, 10, 12, 20]} });
		scatterchart.wijscatterchart('option', 'seriesList', seriesList);

		axis = scatterchart.wijscatterchart('option', 'axis');
		max = axis.y.max;
		min = axis.y.min;
		unitMajor = axis.y.unitMajor;
		unitMinor = axis.y.unitMinor;

		if ($.browser.msie && $.browser.version < 9) {
			ok(max === 20 && min === 4, 'max and min for seriesList1 are ok');
			ok(unitMajor === 2 && unitMinor === 1,
				'unitMajor and unitMinor for seriesList1 are ok');
		}
		else {
			ok(max === 20 && min === 0, 'max and min for seriesList1 are ok');
			ok(unitMajor === 5 && unitMinor === 2.5,
				'unitMajor and unitMinor for seriesList1 are ok');
		}
		seriesList = [];
		seriesList.push({
			data: {
				x: ['x1', 'x2', 'x3', 'x4'],
				y: [50, 1008, 2232, 3800]
			}
		});
		scatterchart.wijscatterchart('option', 'seriesList', seriesList);
		axis = scatterchart.wijscatterchart('option', 'axis');
		max = axis.y.max;
		min = axis.y.min;
		unitMajor = axis.y.unitMajor;
		unitMinor = axis.y.unitMinor;
		ok(max === 4000 && min === 0, 'max and min for seriesList2 are ok');
		ok(unitMajor === 500 && unitMinor === 250,
			'unitMajor and unitMinor for seriesList2 are ok');

		seriesList = [];
		seriesList.push({
			data: {
				x: ['x1', 'x2', 'x3', 'x4'],
				y: [2150, 4008, 6232, 9500]
			}
		});
		scatterchart.wijscatterchart('option', 'seriesList', seriesList);
		axis = scatterchart.wijscatterchart('option', 'axis');
		max = axis.y.max;
		min = axis.y.min;
		unitMajor = axis.y.unitMajor;
		unitMinor = axis.y.unitMinor;
		if ($.browser.msie && $.browser.version < 9) {
			ok(max === 9500 && min === 2000, 'max and min for seriesList3 are ok');
			ok(unitMajor === 500 && unitMinor === 250,
			'unitMajor and unitMinor for seriesList3 are ok');
		}
		else {
			ok(max === 10000 && min === 2000, 'max and min for seriesList3 are ok');
			ok(unitMajor === 1000 && unitMinor === 500,
			'unitMajor and unitMinor for seriesList3 are ok');
		}
		seriesList = [];
		seriesList.push({ data: { x: ['1', '2', '3', '4'], y: [50, 1008, 2232, 3800]} });
		scatterchart.wijscatterchart('option', 'seriesList', seriesList);
		axis = scatterchart.wijscatterchart('option', 'axis');
		max = axis.x.max;
		min = axis.x.min;
		unitMajor = axis.x.unitMajor;
		unitMinor = axis.x.unitMinor;
		ok(max === 3 && min === 0, 'max and min for seriesList4 are ok');
		ok(unitMajor === 1 && unitMinor === 0.5,
			'unitMajor and unitMinor for seriesList4 are ok');

		seriesList = [];
		seriesList.push({ data: { x: time, y: [2150, 4008, 6232, 9500]} });
		scatterchart.wijscatterchart('option', 'seriesList', seriesList);
		wijscatterchart = scatterchart.data("wijmo-wijscatterchart");
		max = wijscatterchart.options.axis.x.max;
		max = $.fromOADate(max);
		min = wijscatterchart.options.axis.x.min;
		min = $.fromOADate(min);
		formatString = wijscatterchart.options.axis.x.annoFormatString;
		if(formatString.length === 0) {
			formatString = wijscatterchart.axisInfo.x.annoFormatString;
		}
		max = Globalize.format(max, formatString);
		min = Globalize.format(min, formatString);
		ok(max === 'Oct' && min === 'Apr', 'max and min for seriesList5 are ok');
		scatterchart.remove();
	});

	test("chartLabelFormatter && chartLabelFormatString", function () {
	    var scatterchart = createScatterchart({
	        seriesList: [{
	            data: { x: ['x1'], y: [7] }
	        }],
	        showChartLabels: true,
	        animation: {
	            enabled: false
	        }
	    }), chartLabel0;

	    chartLabel0 = scatterchart.data("wijmo-wijscatterchart").chartElement.data("fields").chartElements.labels[0].raphaelObj.attr("text");
	    ok(chartLabel0 === "7", "the first chart element label is '7'");

	    scatterchart.wijscatterchart("option", "chartLabelFormatString", "n2");
	    chartLabel0 = scatterchart.data("wijmo-wijscatterchart").chartElement.data("fields").chartElements.labels[0].raphaelObj.attr("text");
	    ok(chartLabel0 === "7.00", "the first chart element label is updated according to chart label format string.");

	    scatterchart.wijscatterchart("option", "chartLabelFormatter", function () {
	        return "mycustomzied:" + this.value;
	    });
	    chartLabel0 = scatterchart.data("wijmo-wijscatterchart").chartElement.data("fields").chartElements.labels[0].raphaelObj.attr("text");
	    ok(chartLabel0 === "mycustomzied:7", "the first chart element label is updated according to chartLabelFormatter.");

	    scatterchart.remove();
	});

}(jQuery));