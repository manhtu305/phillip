﻿/*
* wijmenu_tickets.js
*/
(function ($) {

    module("wijmenu: tickets");

    //#20650
    test("20650:Sliding Menu dissapear on click ", function () {
        // ticket test
        var trigger = $("<button>").appendTo("body");
        var menu = $("<ul><li><a>menuitem</a></li></ul>").appendTo("body");
        menu.wijmenu({
            trigger: trigger,
            triggerEvent: 'click',
            orientation: 'vertical',
            animation: { option: { direction: 'up' } },
            mode: 'sliding'
        });
        ok(menu.is(":hidden"), "menu element is hidden.");
        trigger.trigger('click');
        window.setTimeout(function () {
            ok(menu.is(":visible"), "click the element,show the menu item.");
            //click button again the menu should be visable.
            trigger.trigger('click');
            window.setTimeout(function () {
                ok(menu.is(":visible"), "click the element again,show the menu item.");
                //click other place to hide the menu
                $("<b>").appendTo("body").click().remove();
                window.setTimeout(function () {
                    ok(menu.is(":hidden"), "click the other elements,hide the menu. menu element is hidden.");
                    start();

                    trigger.remove();
                    menu.remove();
                }, 1000);
            }, 1000);
        }, 1000);
        stop();
    });

    //#20652
    test("20651:Menu Recreation refuse to close ", function () {
        var triggerA = $("<button>").appendTo("body");
        var menuA = $("<ul><li><a>menuitem</a></li></ul>").appendTo("body");

        menuA.wijmenu({ trigger: triggerA });
        ok(menuA.is(":hidden"), "menuA element is hidden.");

        var triggerB = $("<button>").appendTo("body");
        var menuB = $("<ul><li><a>menuitem</a></li></ul>").appendTo("body");
        menuB.wijmenu({ trigger: triggerB });
        ok(menuB.is(":hidden"), "menuB element is hidden.");

        //destory menuB, menuA must still can be hidden when clicking the other place in the page
        menuB.wijmenu('destroy');
        triggerA.click();
        window.setTimeout(function () {
            ok(menuA.is(":visible"), "click the element,show the menuA item.");
            $("<b>").appendTo("body").click().remove();
            window.setTimeout(function () {
                ok(menuA.is(":hidden"), "click the other elements, the menuA is hidden.");
                start();
                triggerA.remove();
                menuA.remove();
                triggerB.remove();
                menuB.remove();
            }, 1000);
        }, 1000);
        stop();
    });

    //#24238
    test("24238:Sub Menu item’s text is not shown when Templating", function () {
        var $Menu = $("<ul class='ui-helper-hidden-accessible'"
            + " id='C1Menu1'>"
            + "<li><a href='#'>File</a>"
            + "<ul><li class='classTest'>"
            + "<div><a href='#' class='wijmo-wijmenu-text'>New</a>"
            + "<span class='wijmo-wijmenu-icon-right'>Ctrl+N</span>"
            + "</div></li><li class='classTest'><div><a href='#' class='wijmo-wijmenu-text'>Open</a>"
            + "<span class='wijmo-wijmenu-icon-right'>Ctrl+O</span>"
            + "</div></li><li class='classTest'><div><a href='#' class='wijmo-wijmenu-text'>Save</a>"
            + "<span class='wijmo-wijmenu-icon-right'>Ctrl+S</span>" + "</div></li></ul>"
            + "</li>"
            + "<li><a href='#'>Edit</a>"
            + "<ul><li class='classTest'>" + "<div><a href='#' class='wijmo-wijmenu-text'>Undo</a>"
            + "<span class='wijmo-wijmenu-icon-right'>Ctrl+N</span></div></li>"
            + "<li class='classTest'><div><a href='#' class='wijmo-wijmenu-text'>Cut</a>"
            + "<span class='wijmo-wijmenu-icon-right'>Ctrl+O</span>"
            + "</div></li><li class='classTest'><div><a href='#' class='wijmo-wijmenu-text'>Copy</a>"
            + "<span class='wijmo-wijmenu-icon-right'>Ctrl+S</span>" + "</div></li></ul>"
            + "</li>"
            + "</ul>"
            ).appendTo("body").wijmenu();

        $.each($Menu.find("a.wijmo-wijmenu-text"), function () {
            var a_txt = $.trim($(this).parents(".ui-helper-hidden-accessible").children(":not(ul)a").text());
            var a_current_txt = $.trim($(this).text());
            ok(a_txt !== a_current_txt, "Sub Menu item’s text is shown when Templating ");
        });

        $.each($Menu.find(".classTest").next(), function () {
            var isHasClass = $(this).children("div").hasClass("wijmo-wijmenu-link")
                && $(this).hasClass("ui-corner-all");
            ok(isHasClass, "The div has had the class 'wijmo-wijmenu-link' and the class 'ui-corner-all'");
        });

        $Menu.wijmenu('destroy');
        $Menu.remove();
    });

    //#24830
    test("24830:Set the $Menu.domObject is null", function () {
        var $Menu = $("<ul class='ui-helper-hidden-accessible'"
            + " id='C1Menu1'>"
            + "<li><a href='#'>File</a>"
            + "<ul><li class='classTest'>"
            + "<div><a href='#' class='wijmo-wijmenu-text'>New</a>"
            + "<span class='wijmo-wijmenu-icon-right'>Ctrl+N</span>"
            + "</div></li><li class='classTest'><div><a href='#' class='wijmo-wijmenu-text'>Open</a>"
            + "<span class='wijmo-wijmenu-icon-right'>Ctrl+O</span>"
            + "</div></li><li class='classTest'><div><a href='#' class='wijmo-wijmenu-text'>Save</a>"
            + "<span class='wijmo-wijmenu-icon-right'>Ctrl+S</span>" + "</div></li></ul>"
            + "</li>"
            + "<li><a href='#'>Edit</a>"
            + "<ul><li class='classTest'>" + "<div><a href='#' class='wijmo-wijmenu-text'>Undo</a>"
            + "<span class='wijmo-wijmenu-icon-right'>Ctrl+N</span></div></li>"
            + "<li class='classTest'><div><a href='#' class='wijmo-wijmenu-text'>Cut</a>"
            + "<span class='wijmo-wijmenu-icon-right'>Ctrl+O</span>"
            + "</div></li><li class='classTest'><div><a href='#' class='wijmo-wijmenu-text'>Copy</a>"
            + "<span class='wijmo-wijmenu-icon-right'>Ctrl+S</span>" + "</div></li></ul>"
            + "</li>"
            + "</ul>"
            ).appendTo("body").wijmenu();

        var domObject = {
            scrollcontainer: "<div></div>",
            menucontainer: "<div></div>"
        };
        $Menu.domObject = domObject;

        if ($Menu.domObject) {
            $Menu.domObject = null;
        }
        if (!$Menu.domObject) {
            ok(true, "remove the domObject from $Menu success!");
        } else {
            ok(false, "remove the domObject from $Menu fail!");
        }
        $Menu.wijmenu('destroy');
        $Menu.remove();
    });

    //#29816
    test("29816:‘NavigateUrl” is not working properly in sub menu after navigating through keyboard when “Mode” property is set to sliding”", function () {
        $.wijmo.wijmenu.prototype.options.items = [];
        var $menu = $('<ul id="menu1">'
            + '<li><a id="menu2" href="#">menu2</a>'
            + '<ul>'
            + '<li><a id="menu21" href="#">menu21</a></li>'
            + '<li><a id="menu22" href="#">menu22</a></li>'
            + '<li><a id="menu23" href="#">menu23</a></li>'
            + '<li><a id="menu24" href="#">menu24</a></li>'
            + '</ul>'
            + '</li>'
            + '</ul>'
            ).appendTo("body").wijmenu({
                mode: "sliding",
                orientation: "vertical"
            }),
            keyCode = keycode = $.ui.keyCode,
            event;

        event = jQuery.Event("keydown", { keyCode: keycode.DOWN });
        $menu.trigger(event);
        ok($(document.activeElement).text() === "menu2", "set focus success");

        $menu.wijmenu("option", "select", function () {
            ok($(document.activeElement).text() === "menu21", "set focus success");
            $menu.wijmenu('destroy');
            $menu.remove();
            start();
        });
        event = jQuery.Event("keydown", { keyCode: keycode.RIGHT });
        $menu.trigger(event);
        stop();
    });

    test("#41038", function () {
        var container = $("<ul><li></li></ul>").appendTo("body").children(),
            menu = $('<ul id="menu1">'
            + '<li><a id="menu2" href="#">menu2</a>'
            + '<ul>'
            + '<li><a id="menu21" href="#">menu21</a></li>'
            + '<li><a id="menu22" href="#">menu22</a></li>'
            + '<li><a id="menu23" href="#">menu23</a></li>'
            + '<li><a id="menu24" href="#">menu24</a></li>'
            + '</ul>'
            + '</li>'
            + '</ul>'
            ).appendTo(container).wijmenu({
                mode: "sliding",
                orientation: "vertical"
            });

        menu.find("li a:first").simulate("click");
        ok(true, "The menu works without exception");
        container.parent().remove();
    });

    test("#39815", function () {
        var $menu = $('<ul id="menu1">'
            + '<li><a id="menu2" href="#">menu2</a>'
            + '<ul>'
            + '<li><a id="menu21" href="#">menu21</a></li>'
            + '<li><a id="menu22" href="#">menu22</a></li>'
            + '</ul>'
            + '</li>'
            + '<li><a id="menu3" href="#">menu3</a>'
            + '<ul>'
            + '<li><a id="menu31" href="#">menu31</a></li>'
            + '<li><a id="menu32" href="#">menu32</a></li>'
            + '</ul>'
            + '</li>'
            + '</ul>')
            .appendTo("body").wijmenu({
                trigger: ".wijmo-wijmenu-item",
                triggerEvent: "mouseenter",
                orientation: "vertical"
            }), subMenu1;

        subMenu1 = $menu.children("li:first").children("ul");
        $("#menu2").simulate("mouseover");

        setTimeout(function () {
            ok(subMenu1.css("display") === "block", "The submenu will be shown, when mouse enter the menu item.");
            $("#menu21").simulate("mouseout");
            setTimeout(function () {
                ok(subMenu1.css("display") === "none", "The submenu will be hidden, when mouse leave the submenu.");
                $menu.wijmenu("option", "triggerEvent", "click");
                $menu.wijmenu("option", "triggerEvent", "mouseenter");
                $("#menu2").simulate("mouseover");
                setTimeout(function () {
                    ok(subMenu1.css("display") === "block", "The submenu will be shown after changing the triggerEvent, when mouse enter the menu item");
                    $("#menu21").simulate("mouseout");
                    setTimeout(function () {
                        ok(subMenu1.css("display") === "none", "The submenu will be hidden correctly after changing the triggerEvent, when mouse leave the menu item");
                        $menu.remove();
                        start();
                    }, 1000);
                }, 500);
            }, 1000);
        }, 500)
        stop();
    });

    test("#46978", function () {
        var $container = $('<div id="Div46978"></div>').appendTo("body"),
            $menu = $('<ul id="menu1">'
            + '<li><a id="menu2" href="#">menu2</a>'
            + '<ul>'
            + '<li><a id="menu21" href="#">menu21</a></li>'
            + '<li><a id="menu22" href="#">menu22</a></li>'
            + '<li><a id="menu23" href="#">menu23</a></li>'
            + '<li><a id="menu24" href="#">menu24</a></li>'
            + '<li><a id="menu25" href="#">menu25</a></li>'
            + '<li><a id="menu26" href="#">menu26</a></li>'
            + '</ul>'
            + '</li>'
            + '</ul>')
            .appendTo($container).wijmenu({
                trigger: ".wijmo-wijmenu-item",
                triggerEvent: "mouseenter",
                showAnimation: { animated: "slide", duration: 200 },
                hideAnimation: { animated: "fade", duration: 200 },
            }), subMenu1, correctHeight;

        $("#menu2").simulate("mouseover");
        subMenu1 = $menu.children("li:first").children("ul");

        setTimeout(function () {
            correctHeight = subMenu1.height();
            $("#menu2").simulate("click");
            $("#menu2").simulate("mouseup");

            setTimeout(function () {
                $("#menu2").simulate("mouseover");
                setTimeout(function () {
                    $("#menu2").simulate("click");
                    $("#menu2").simulate("mouseup");
                    setTimeout(function () {
                        $("#menu2").simulate("mouseover");
                        setTimeout(function () {
                            ok(subMenu1.height() === correctHeight, "Sub Menu appears completely !!")
                            start();
                            $menu.remove();
                            $container.remove();
                        }, 700);
                    }, 200);
                }, 500);
            }, 200);
        }, 700);
        stop();
    });

    test("#48197", function () {
        var container = $("<div>").appendTo("body"),
            menu = $('<ul id="bmenu1">'
            + '<li><a id="bmenu2" href="#">menu21</a>'
            + '<ul>'
            + '<li id="bli2"><a id="bmenu21" href="javascript:clickItem();">menu211</a></li>'
            + '<li><a id="bmenu22" href="#">menu221</a></li>'
            + '<li><a id="bmenu23" href="#">menu231</a></li>'
            + '<li><a id="bmenu24" href="#">menu241</a></li>'
            + '</ul>'
            + '</li>'
            + '</ul>'
            ).appendTo(container).wijmenu({ items: [] });

        menu.wijmenu("setItemDisabled", $('#bli2'), true);
        menu.find("li a:first").simulate("mouseover");
        window.setTimeout(function () {
            menu.find("#bmenu21").simulate("mouseover");
            menu.find("#bmenu21").simulate("click");
            window.setTimeout(function () {
                ok($("title").text() === "wijmenu widget Unit Test", "The menuitem is disabled!");
                start();
                menu.remove();
                container.remove();
            }, 500);
        }, 1000);
        stop();
    });

    test("#50465", function () {
        var menu = $('<ul id="bmenu1">'
            + '<li><a id="bmenu2" href="#">menu21</a>'
            + '<ul>'
            + '<li id="bli2"><a href="#">menu211</a></li>'
            + '<li><a href="#">menu221</a></li>'
            + '<li><a href="#">menu231</a></li>'
            + '<li><a href="#">menu241</a></li>'
            + '</ul>'
            + '</li>'
            + '</ul>'
            ).appendTo("body").wijmenu(),
            firstItem = menu.find(">li:first>a"),
            firstSubItem = menu.find(">li:first li:first>a");
        ok(firstItem.attr("id") === "bmenu2", "the first menu item has an id.");
        ok(!firstSubItem.attr("id"), "the first sub item has no id");
        firstItem.simulate("mouseover");
        setTimeout(function () {
            firstItem.simulate("click");
            ok(menu.attr("aria-activedescendant") === "bmenu2", "the aria-activedescendant set correct value");
            setTimeout(function () {
                firstItem.simulate("mouseover");
                setTimeout(function () {
                    firstSubItem.simulate("click");
                    setTimeout(function () {
                        ok(firstItem.attr("id") === "bmenu2", "when deactivate the first item, it's id is not removed.");
                        ok(firstSubItem.attr("id"), "the first sum item has added an id");
                        ok(menu.attr("aria-activedescendant") === firstSubItem.attr("id"), "the aria-activedescendant set correct value");
                        menu.remove();
                        start();
                    }, 500);
                }, 500);
            }, 500);
        }, 500);
        stop();
    });

    test("#94646", function () {
        var menu = $('<ul id="menu"><li><a id="menu1" href="#">menu1</a></li></ul>')
            .appendTo("body").wijmenu({ disabled: true }), menuData = menu.data("wijmo-wijmenu"), maskDiv = menuData.disabledDiv;
        ok(!!maskDiv, "maskDiv created correctly");
        menu.wijmenu("refresh");
        maskDiv = menuData.disabledDiv;
        ok(maskDiv.parent().is(menuData.domObject.menucontainer), "maskDiv shows in correctly position");
        menu.remove();
    });
})(jQuery);

function clickItem() {
    $("title").html("change the title");
}