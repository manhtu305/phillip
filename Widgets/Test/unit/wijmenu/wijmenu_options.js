﻿/*
* wijmenu_options.js
*/

(function ($) {
    module("wijmenu:options");

    test("trigger-outsideMenu", function () {
        var trigger = $("<button>Test</button>").appendTo("body");
        var menu = $("<ul><li><a>menuitem</a></li></ul>").appendTo("body");
        menu.wijmenu({ trigger: trigger });
        ok(menu.is(":hidden"), "menu element is hidden.");
        trigger.trigger("click");

        setTimeout(function () {
            ok(menu.is(":visible"), "trigger click the element,show the menu item.");
            $("body").simulate("click", { clientX: 500, clientY: 500 });
            setTimeout(function () {
                ok(menu.is(":hidden"), "click the other elements,hide the menu. menu element is hidden.");
                trigger.remove();
                menu.remove();
                start();
            }, 500);
        }, 500)
        stop();
    });

    test("trigger-insideMenu", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
        var trigger = "#triggerele";
        var triggerEle = menu.find(trigger);
        menu.wijmenu({ trigger: trigger });
        var submenu = triggerEle.children("ul");
        ok(submenu.is(":hidden"), "submenu is hidden.");
        triggerEle.children("a").trigger("click");
        setTimeout(function () {
            ok(submenu.is(":visible"), "submenu shows by the trigger click.");
            $("body").simulate("click", { clientX: 500, clientY: 500 });
            window.setTimeout(function () {
                ok(submenu.is(":hidden"), "submenu hides by click the other elements.");
                menu.remove();
                start();
            }, 500);
        }, 500);
        stop();
    });

    ///test triggerEvents.
    testTriggerEvent("click");

    testTriggerEvent("mouseenter");

    testTriggerEvent("dblclick");

    testTriggerEvent("rtclick");

    test("submenuTriggerEvent-default", function () {
        var trigger = $("<button id='button'>Test</button>").appendTo("body"),
            menuST = $("<ul id='subTE'><li id='subitem1'><a>menu subitem1</a><ul id='submenu'><li><a>menu item11</a></li></ul></li></ul>").appendTo("body"),
            _subTrigger = "click", _subShownTimes = 0;
        menuST.wijmenu({
            trigger: "#button",
            animation: null,
            showAnimation: null,
            hideAnimation: null,
            showDelay: 0,
            hideDelay: 0,
            shown: function (e, self) {
                var target = $(e.currentTarget);
                if (target.is("#subitem1 > a")) {
                    _subShownTimes++;
                    ok(target.parent().children("ul").is(":visible"), "If \"submenuTriggerEvent\" is default, subMenu shows by option \"triggerEvent\" : \"" + _subTrigger + "\"");
                }
            },
        });
        trigger.trigger("click");
        _subTrigger = menuST.wijmenu("option", "triggerEvent");
        $("#subitem1").children("a").trigger(_subTrigger);
        setTimeout(function () {
            $("body").simulate("click", { clientX: 500, clientY: 500 });
            menuST.wijmenu("option", "triggerEvent", "dblclick");
            trigger.trigger("dblclick");
            _subTrigger = menuST.wijmenu("option", "triggerEvent");
            $("#subitem1").children("a").trigger(_subTrigger);
            ok(_subShownTimes === 2, "Submenu has shown 2 times.");
            trigger.remove();
            menuST.remove();
            start();
        }, 100);
        stop();
    })

    test("submenuTriggerEvent-outsideMenu", function () {
        var trigger = $("<button id='button'>Test</button>").appendTo("body"),
            menu = $("<ul><li id='item1'><a>menu item1</a><ul id='submenu'><li><a>menu item11</a></li></ul></li></ul>").appendTo("body"),
            _subTrigger = "mouseenter", testSubTriggerFunc, _subShownTimes = 0;
        menu.wijmenu({
            trigger: "#button",
            submenuTriggerEvent: "mouseenter",
            animation: null,
            showAnimation: null,
            hideAnimation: null,
            showDelay: 0,
            hideDelay: 0,
            shown: function (e, self) {
                var target = $(e.currentTarget);
                if (target.is("#item1 > a")) {
                    _subShownTimes++;
                    ok(target.parent().children("ul").is(":visible"), "SubMenu shows by option \"submenuTriggerEvent\" : \"" + _subTrigger + "\"");
                }
            },
        });

        testSubTriggerFunc = function (subTriggerEvent) {
            _subTrigger = subTriggerEvent;
            $("body").simulate("click", { clientX: 500, clientY: 500 });
            menu.wijmenu("option", "submenuTriggerEvent", subTriggerEvent);
            trigger.trigger("click");
            if (subTriggerEvent === "rtclick") {
                subTriggerEvent = "contextmenu";
            }
            $("#item1").children("a").trigger(subTriggerEvent);
        }

        trigger.trigger("click");
        $("#item1").children("a").trigger("mouseenter");
        setTimeout(function () {
            testSubTriggerFunc("click");
            setTimeout(function () {
                testSubTriggerFunc("rtclick");
                setTimeout(function () {
                    testSubTriggerFunc("dblclick");
                    ok(_subShownTimes === 4, "Submenu has shown 4 times.");
                    trigger.remove();
                    menu.remove();
                    start();
                }, 0);
            }, 0);
        }, 0);
        stop();
    });

    test("submenuTriggerEvent-insideMenu", function () {
        var menu = $("<ul id='menu'>" +
                        "<li id='item1' class='subCss'><a>menu item1</a>" +
                            "<ul id='submenu'><li><a>menu item11</a></li></ul>" +
                        "</li>" +
                        "<li id='item2' class='subCss'><a>menu item2</a>" +
                            "<ul id='submenu'><li><a>menu item11</a></li></ul>" +
                        "</li>" +
                     "</ul>").appendTo("body"),
            _subTrigger = "mouseenter", testSubTriggerFunc, message, _subShownTimes = 0;

        menu.wijmenu({
            trigger: "#menu",
            triggerEvent: "click",
            submenuTriggerEvent: "mouseenter",
            animation: null,
            showAnimation: null,
            hideAnimation: null,
            showDelay: 0,
            hideDelay: 0,
            shown: function (e, self) {
                var target = $(e.currentTarget);
                if (target.is(".subCss > a")) {
                    _subShownTimes++;
                    message = _subTrigger === menu.wijmenu("option", "submenuTriggerEvent") ?
                        "SubMenu shows by option \"submenuTriggerEvent\" : \"" + _subTrigger + "\"" :
                        "SubMenu shows not by option \"submenuTriggerEvent\" but according to \"triggerEvent\" : \"" + _subTrigger + "\"";
                    ok(target.parent().children("ul").is(":visible"), message);
                }
            },
        });

        $("#item1").children("a").trigger(_subTrigger);
        setTimeout(function () {
            $("body").simulate("click", { clientX: 500, clientY: 500 });
            _subTrigger = "dblclick";
            menu.wijmenu("option", "submenuTriggerEvent", _subTrigger);
            $("#item1").children("a").trigger(_subTrigger);
            setTimeout(function () {
                $("body").simulate("click", { clientX: 500, clientY: 500 });
                menu.wijmenu("option", "trigger", ".subCss");
                _subTrigger = menu.wijmenu("option", "triggerEvent");
                $("#item1").children("a").trigger(_subTrigger);
                setTimeout(function () {
                    $("body").simulate("click", { clientX: 500, clientY: 500 });
                    $("#item2").children("a").trigger(_subTrigger);
                    ok(_subShownTimes === 4, "Submenu has shown 4 times.");
                    menu.remove();
                    start();
                }, 0);
            }, 0);
        }, 0);
        stop();
    })

    test("mode-flyout", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu();
        ok(menu.is(":visible"), "the menu is visible.");
        var li = menu.find("li:first");
        var link = li.children("a");
        var submenu = li.children("ul");
        ok(submenu.is(":hidden"), "submenu is hidden.");
        link.trigger("mouseenter");
        window.setTimeout(function () {
            ok(submenu.is(":visible"), "mouse over the item,show it's submenu.");
            link.trigger("mouseleave");
            setTimeout(function () {
                ok(submenu.is(":hidden"), "mouse out the item,hide it's submenu.");
                link.trigger("mouseenter");
                window.setTimeout(function () {
                    ok(submenu.is(":visible"), "mouse over the item,show it's submenu.");
                    link.trigger("click");
                    setTimeout(function () {
                        ok(submenu.is(":hidden"), "mouse click the item,hide it's submenu.");
                        ok(menu.is(":visible"), "main menu is still show.");
                        menu.remove();
                        start();
                    }, 600);
                }, 920);
            }, 920)
        }, 750);
        stop();
    });

    test("mode-sliding", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu({ mode: "sliding" });
        var domObject = menu.data("wijmo-wijmenu").domObject;
        var menucontainer = domObject.menucontainer;
        var scrollcontainer = domObject.scrollcontainer;
        var wrapedDiv = menu.parent();
        ok(wrapedDiv.css("position") === "relative", "wraped div add the relative position css.");
        var breadcurmb = scrollcontainer.next();
        var triggerele = menu.find("#triggerele");
        var initLeft = menu.css("left") === "auto" ? "0px" : menu.css("left");
        triggerele.children("a").trigger("click");
        setTimeout(function () {
            ok($.trim($(".wijmo-wijmenu-breadcrumb").text()) == "Back", "back link is shown when the submenu is shown");
            var newLeft = menu.css("left");
            ok(newLeft !== initLeft, "menu item is moved.");

            breadcurmb.find("a").trigger("click");
            setTimeout(function () {
                ok($.trim(breadcurmb.text()) != "back", "back link removed.");
                newLeft = menu.css("left");
                ok(newLeft === initLeft, "menu changed to the init status.");
                menu.remove();
                start();
            }, 420);
        }, 410);
        stop();
    });

    test("checkable", function () {
        var menu = $("<ul><li><a>menu item1</a></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu();
        var menuitem1 = menu.find("li").eq(0);
        var menuitem2 = menu.find("li").eq(1);
        ok(!menuitem1.children("a").hasClass("ui-state-focus") && !menuitem2.children("a").hasClass("ui-state-focus"), "In common,the menu item 1 and menu item2 hasn't ui-state-focus css class.");
        menuitem1.children("a").trigger("click");
        ok(menuitem1.children("a").hasClass("ui-state-focus"), "click the menu item1,then the item have ui-state-focus css class.");
        ok(!menuitem1.children("a").hasClass("ui-state-active"), "menu item1 hasn't ui-state-active css class.");
        menuitem2.children("a").trigger("click");
        setTimeout(function () {
            ok(!menuitem1.children("a").hasClass("ui-state-focus") && menuitem2.children("a").hasClass("ui-state-focus"), "menu item2 clicked,menu item1 hasn't ui-state-focus css class,And menu item2 has ui-state-focus css class.");
            ok(!menuitem2.children("a").hasClass("ui-state-active"), "menu item2 hasn't ui-state-active css class.");
            menu.wijmenu("destroy");
            menu.wijmenu({ checkable: true });
            menuitem1.children("a").trigger("click");
            ok(menuitem1.children("a").hasClass("ui-state-active") && menuitem1.children("a").hasClass("ui-state-focus"), "When checkable is true,click the menu item 1,It has ui-state-active and ui-state-focus css class.");
            menuitem2.children("a").trigger("click");
            ok(menuitem2.children("a").hasClass("ui-state-active") && menuitem2.children("a").hasClass("ui-state-focus"), "menu item2 clicked, it has ui-state-active and ui-state-focus css class.");
            ok(menuitem1.children("a").hasClass("ui-state-active"), "when menu item 2 clicked,menu item 1 still has ui-state-active css class.");
            menu.remove();
            start();
        }, 500);
        stop();
    });

    test("orientation", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a><ul><li><a>menu item 111</a></li><li><a>menu item 112</a></li></ul></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu();
        var menucontainer = menu.data("wijmo-wijmenu").domObject.menucontainer;
        ok(menucontainer.hasClass("wijmo-wijmenu-horizontal"), "menu container add the ui-wijmenu-horizontal css class.");
        var submenu1 = menu.find("li:first");
        ok(submenu1.hasClass("wijmo-wijmenu-parent"), "menu item1 added the ui-wijmenu-parent css class.");
        var link1 = submenu1.children("a");
        var icon1 = link1.find(".ui-icon");
        ok(icon1.hasClass("ui-icon-triangle-1-s"), "first-level icon is ui-icon-triangle-1-s css class.");
        var subul = submenu1.children("ul");
        var submenu2 = subul.children("li:first");
        var link2 = submenu2.children("a");
        var icon2 = link2.find(".ui-icon");
        ok(icon2.hasClass("ui-icon-triangle-1-e"), "second-level icon is icon is ui-icon-triangle-1-e css class.");
        menu.wijmenu("option", "orientation", "vertical");
        ok(menucontainer.hasClass("wijmo-wijmenu-vertical"), "menu container add the ui-wijmenu-vertical css class.");
        ok(submenu1.hasClass("wijmo-wijmenu-parent"), "menu item1 added the ui-wijmenu-parent css class.");
        ok(icon1.hasClass("ui-icon-triangle-1-e"), "first-level icon is ui-icon-triangle-1-e css class.");
        ok(icon2.hasClass("ui-icon-triangle-1-e"), "second-level icon is icon is ui-icon-triangle-1-e css class.");
        menu.remove();
    });

    test("backLink", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a><ul><li><a>menu item 111</a></li><li><a>menu item 112</a></li></ul></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu({ mode: "sliding" });
        var item1 = menu.children("li:first");
        item1.children("a").trigger("click");
        var menucontainer = menu.data("wijmo-wijmenu").domObject.menucontainer;
        var breadul = menucontainer.find(".wijmo-wijmenu-breadcrumb");
        ok($.trim(breadul.text()) === "Back", "back link is show.");
        setTimeout(function () {
            menu.wijmenu("destroy");
            menu.wijmenu({ mode: "sliding", backLink: false });
            menucontainer = menu.data("wijmo-wijmenu").domObject.menucontainer;
            breadul = menucontainer.find(".wijmo-wijmenu-breadcrumb");
            ok($.trim(breadul.text()) === "Choose an option", "when backLink is false,the default breadcurmb'text is 'Choose an option'.");
            item1 = menu.children("li:first");
            item1.children("a").trigger("click");
            setTimeout(function () {
                var brItems = breadul.children("li");
                var fstItem = brItems.eq(0);
                var fslink = fstItem.children("a");
                ok($.trim(fslink.text()) === "All", "breadcurmb all link is show!");
                var britem1 = brItems.eq(1);
                ok($.trim(britem1.text()) === "menu item1", "menu item1 link is show.");
                menu.remove();
                start();
            }, 500)
        }, 500);
        stop();
    });

    test("backLinkText", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a><ul><li><a>menu item 111</a></li><li><a>menu item 112</a></li></ul></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu({ mode: "sliding", backLinkText: "back" });
        var item1 = menu.children("li:first");
        item1.children("a").trigger("click");
        setTimeout(function () {
            var menucontainer = menu.data("wijmo-wijmenu").domObject.menucontainer;
            var breadul = menucontainer.find(".wijmo-wijmenu-breadcrumb");
            ok($.trim(breadul.text()) === "back", "the backLink'text is changed to 'back'.");
            menu.remove();
            start();
        }, 410);
        stop();
    });

    test("topLinkText", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a><ul><li><a>menu item 111</a></li><li><a>menu item 112</a></li></ul></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu({ mode: "sliding", backLink: false, topLinkText: "All Items" });
        menucontainer = menu.data("wijmo-wijmenu").domObject.menucontainer;
        breadul = menucontainer.find(".wijmo-wijmenu-breadcrumb");
        item1 = menu.children("li:first");
        item1.children("a").trigger("click");
        setTimeout(function () {
            var brItems = breadul.children("li");
            var fstItem = brItems.eq(0);
            var fslink = fstItem.children("a");
            ok($.trim(fslink.text()) === "All Items", "topLink'text is changed to 'All Items'.");
            menu.remove();
            start();
        }, 410);
        stop();
    });

    test("crumbDefaultText", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a><ul><li><a>menu item 111</a></li><li><a>menu item 112</a></li></ul></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu({ mode: "sliding", backLink: false, crumbDefaultText: "choose" });
        menucontainer = menu.data("wijmo-wijmenu").domObject.menucontainer;
        breadul = menucontainer.find(".wijmo-wijmenu-breadcrumb");
        ok($.trim(breadul.text()) === "choose", "breadcurmb default text is changed to 'choose'.");
        menu.remove();
    });

    //test for menuitem options
    test("menuitem's header options", function () {
        var menu = $("<ul><li><a>aa</a></li></ul>").appendTo("body");
        menu.wijmenu({
            items: [
                { header: true, text: "header" },
                { header: true, text: "h2", items: [{ text: 'a' }] }
            ]
        });

        var li = menu.find("li");
        ok(li.eq(0).children().is("h3") && li.eq(0).text() === "header",
            "when specified header as true, the markup should be changed to h3");

        ok(li.eq(1).children().is("h3") && li.eq(1).text() === "h2" &&
            li.eq(1).find(">ul>li").length === 0,
            "when specified header as true, would create a new h3 markup and ignore items property");

        menu.remove();

        menu = $("<ul><li><a>aa</a></li></ul>").appendTo("body");
        menu.wijmenu();

        var li = menu.find("li");
        li.wijmenuitem({ header: true });

        ok(li.children().is("h3"), "the child markup has been changed to h3");
        var widget = li.data("wijmo-wijmenuitem")
        ok(widget.options.header === true, "the options.header has been set to true");

        li.wijmenuitem({ header: false });
        ok(li.children().is("a"), "the child markup has been changed to a");
        var widget = li.data("wijmo-wijmenuitem")
        ok(widget.options.header === false, "the options.header has been set to true");
        ok(li.hasClass("ui-widget wijmo-wijmenu-item ui-state-default"), "li has correct cssClass");
        ok(li.children().hasClass("wijmo-wijmenu-link ui-corner-all"), "A has correct  cssClass");
        menu.remove();
    });

    test("menuitem's  separator options", function () {
        var menu = $("<ul><li><a>aa</a></li></ul>").appendTo("body");
        menu.wijmenu({
            items: [
                { separator: true, text: "separator" },
                { separator: true, text: "h2", items: [{ text: 'a' }] }
            ]
        });

        var li = menu.find("li");
        ok(li.eq(0).find(".wijmo-wijmenu-separator-content").html() === '&nbsp;',
            "when specified header as true, the markup should be changed to h3");

        ok(li.eq(1).find(".wijmo-wijmenu-separator-content").html() === '&nbsp;' &&
            li.eq(1).find(">ul>li").length === 0,
            "when specified header as true, would create a new h3 markup and ignore items property");
        menu.remove();

        menu = $("<ul><li><a>aa</a></li></ul>").appendTo("body");
        menu.wijmenu();

        var li = menu.find("li");
        li.wijmenuitem({ separator: true });

        ok(li.hasClass('wijmo-wijmenu-separator') && li.find(".wijmo-wijmenu-separator-content").html() === '&nbsp;', "the child markup has been cleared");
        var widget = li.data("wijmo-wijmenuitem")
        ok(widget.options.separator === true, "the options.separator has been set to true");

        li.wijmenuitem({ separator: false, text: '' });
        ok(li.children().is("a"), "the child markup has been changed to a");
        var widget = li.data("wijmo-wijmenuitem");
        ok(widget.options.separator === false, "the options.header has been set to true");
        ok(li.hasClass("ui-widget wijmo-wijmenu-item ui-state-default"), "li has correct cssClass");
        ok(li.children().hasClass("wijmo-wijmenu-link ui-corner-all"), "A has correct  cssClass");

        menu.remove();
    });


    test("menuitem's  value options", function () {
        var menu = $("<ul><li><a>aa</a></li></ul>").appendTo("body");
        var options = {
            items: [{
                value: 'aaa'
            }]
        };

        menu.wijmenu(options);
        ok(menu.wijmenu('getItems')[0].options.value === options.items[0].value,
            "the value saved in the options equals the value in the meniItem's options");

        var li = menu.find("li");
        li.wijmenuitem({ value: 'b' });

        ok(menu.wijmenu('getItems')[0].options.value === 'b',
            "the value saved in the options equals the value in the meniItem's options");
        menu.remove();
    });


    test("menuitem's  text options", function () {
        var menu = $("<ul><li><a>aa</a></li><li><h3>b</h3></li><li></li></ul>").appendTo("body");
        var options = {
            items: [{
                text: 'aaa'
            },
                { text: 'bbb' }
            ]
        };

        menu.wijmenu(options);
        ok(menu.find('li:first').text() === options.items[0].text &&
            menu.find('li:eq(1)').text() === options.items[1].text,
            "the text of li have been changed to the text specified in the options");

        var li1 = menu.find('li:first');
        var li2 = menu.find('li:eq(1)');
        var li3 = menu.find('li:eq(2)');
        li1.wijmenuitem({ text: '' });
        ok(li1.text() == '', "when set text as empty string to the link,it can apply change to html");
        li2.wijmenuitem({ text: '' });
        ok(li1.text() == '', "when set text as empty string to the header,it can apply change to html");

        li3.wijmenuitem({ text: 'abc' });
        ok(li3.hasClass('wijmo-wijmenu-separator') && li3.find(".wijmo-wijmenu-separator-content").html() === '&nbsp;', "when set text to an separator, it won't change html");
        menu.remove();
    });

    test("menuitem's  navigateUrl options", function () {
        var menu = $("<ul><li><a>aa</a></li><li><h3>b</h3></li></ul>").appendTo("body");
        var options = {
            items: [{
                navigateUrl: 'aaa'
            },
                { navigateUrl: 'bbb' }
            ]
        };

        menu.wijmenu(options);
        ok(menu.find('li:first>a').attr('href') === options.items[0].navigateUrl,
            "the href of link has been changed to the navigateUrl specified in the options");

        var li = menu.find('>li:first');
        li.wijmenuitem({ navigateUrl: 'bcd' });
        ok(li.find("a:first").attr('href') === 'bcd',
            "the href of link has been changed to the navigateUrl specified in the options");

        var li2 = menu.find('>li:eq(1)');
        li2.wijmenuitem({ navigateUrl: 'bcd' });

        ok(li2.find(">:first").attr('href') === undefined, "navigateUrl won't apply to an header menuitem");
        menu.remove();
    });

    test("menuitem's  target options", function () {
        var menu = $("<ul><li><a>aa</a></li><li><h3>b</h3></li></ul>").appendTo("body");
        var options = {
            items: [{
                target: 'aaa'
            },
                { target: 'bbb' }
            ]
        };

        menu.wijmenu(options);
        ok(menu.find('li:first>a').attr('target') === options.items[0].target,
            "the target of link has been changed to the target specified in the options");

        var li = menu.find('>li:first');
        li.wijmenuitem({ target: 'bcd' });
        ok(li.find("a:first").attr('target') === 'bcd',
            "the target of link has been changed to the target specified in the options");

        var li2 = menu.find('>li:eq(1)');
        li2.wijmenuitem({ target: 'bcd' });

        ok(li2.find(">:first").attr('target') === undefined, "target won't apply to an header menuitem");
        menu.remove();
    });

    test("mode-flyout and set displayVisible as false in menuitem ", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li id='hidden_1' displayVisible='false'><a>menu item11</a></li></ul></li><li><a>menu item2</a><ul><li id='hidden_2'><a>aa</a></li><li><a>bb</a></li></ul></li></ul>").appendTo("body");
        menu.wijmenu({
            items: [{
                items: [{
                    displayVisible: false
                }]
            }, {
                items: [{
                    displayVisible: false
                }]
            }]
        });
        ok(menu.is(":visible"), "the menu is visible.");
        var li = menu.find(">li:first");
        var li2 = menu.find(">li:eq(1)");
        var link = li.children("a");
        var link2 = li2.children("a");
        var submenu = li.children("ul");
        var submenu2 = li2.children("ul");
        ok(menu.find("#hidden_1").is(":hidden"), "element should be hidden when sepcified displayVisible as false in html markup");
        ok(menu.find("#hidden_2").is(":hidden"), "element should be hidden when sepcified displayVisible as false in options");
        ok(link.find(">a>span.wijmenuitem-icon").length === 0,
        "when all child menuitem have been set displayVisible as false, the menu shold not show the default icon");

        ok(submenu.is(":hidden"), "submenu is hidden.");
        link.trigger("mouseenter");
        window.setTimeout(function () {
            ok(submenu.is(":hidden"), "mouse over the item, do not show the item.");
            link2.trigger("mouseenter");
            window.setTimeout(function () {
                ok(menu.find("#hidden_2").is(":hidden"), "mouse over the item, hieend li should not show ");
                ok(submenu2.is(":visible"), "mouse over the item, show the submenu");
                menu.remove();
                start();
            }, 750);
        }, 750);
        stop();
    });

    test("mode-flyout and modify displayVisible in set option step", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li id='hidden_1'><a>menu item11</a></li></ul></li><li><a>menu item2</a><ul><li id='hidden_2'><a>aa</a></li></ul></li></ul>").appendTo("body");
        menu.wijmenu({
            items: [{
            }, {
                items: [{
                    displayVisible: false
                }]
            }]
        });
        var li = menu.find(">li:first");
        var li2 = menu.find(">li:eq(1)");
        var link = li.children("a");
        var link2 = li2.children("a");
        var submenu = li.children("ul");
        var submenu2 = li2.children("ul");

        ok(link.children("span.ui-icon-triangle-1-s").length === 1,
        "when has any children is an visible item, show the submenu icon");

        ok(link2.children("span.ui-icon-triangle-1-s").length === 0,
        "when all child menuitem have been set displayVisible as false, the menu shold not show the default icon");

        var subli = submenu.find('li:first');
        subli.wijmenuitem({ displayVisible: false });

        var subli2 = submenu2.find('li:first');
        subli2.wijmenuitem({ displayVisible: true });

        ok(link.children("span.ui-icon-triangle-1-s").length === 0,
        "when has any children is an visible item, show the submenu icon");

        ok(link2.children("span.ui-icon-triangle-1-s").length === 1,
        "when all child menuitem have been set displayVisible as false, the menu shold not show the default icon");
        ok(submenu.is(":hidden"), "submenu is hidden.");

        link.trigger("mouseenter");
        window.setTimeout(function () {
            ok(submenu.is(":hidden"), "mouse over the item, do not show the item.");
            link2.trigger("mouseenter");

            window.setTimeout(function () {
                ok(menu.find("#hidden_2").is(":visible"), "mouse over the item, show li2");
                ok(submenu2.is(":visible"), "mouse over the item, show the submenu");
                menu.remove();
                start();
            }, 750);
        }, 750);
        stop();
    });

    test("mode-sliding and set displayVisible as false in menuitem ", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li id='hidden_1' displayVisible='false'><a>menu item11</a></li></ul></li><li><a>menu item2</a><ul><li id='hidden_2'><a>aa</a></li><li><a>bb</a></li></ul></li></ul>").appendTo("body");
        menu.wijmenu({
            mode: "sliding",
            items: [{
                items: [{
                    displayVisible: false
                }]
            }, {
                items: [{
                    displayVisible: false
                }]
            }]
        });
        ok(menu.is(":visible"), "the menu is visible.");
        var li = menu.find(">li:first");
        var li2 = menu.find(">li:eq(1)");
        var link = li.children("a");
        var link2 = li2.children("a");
        var submenu = li.children("ul");
        var submenu2 = li2.children("ul");
        ok(menu.find("#hidden_1").is(":hidden"), "element should be hidden when sepcified displayVisible as false in html markup");
        ok(menu.find("#hidden_2").is(":hidden"), "element should be hidden when sepcified displayVisible as false in options");
        ok(link.find(">a>span.wijmenuitem-icon").length === 0,
        "when all child menuitem have been set displayVisible as false, the menu shold not show the default icon");

        ok(submenu.is(":hidden"), "submenu is hidden.");
        link.trigger("click");
        window.setTimeout(function () {
            ok(submenu.is(":hidden"), "mouse over the item, do not show the item.");
            link2.trigger("click");
            window.setTimeout(function () {
                ok(menu.find("#hidden_2").is(":hidden"), "mouse over the item, hieend li should not show ");
                ok(submenu2.is(":visible"), "mouse over the item, show the submenu");
                menu.remove();
                start();
            }, 750);
        }, 750);
        stop();
    });

    test("mode-flyout and modify displayVisible in set option step", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li id='hidden_1' displayVisible='false'><a>menu item11</a></li></ul></li>"
            + "<li><a>menu item2</a><ul><li id='hidden_2'><a>aa</a></li></ul></li></ul>").appendTo("body");
        menu.wijmenu({
            mode: "sliding",
            items: [{
                items: [{}]
            }, {
                items: [{
                    displayVisible: false
                }]
            }]
        });
        ok(menu.is(":visible"), "the menu is visible.");
        var li = menu.find(">li:first");
        var li2 = menu.find(">li:eq(1)");
        var link = li.children("a");
        var link2 = li2.children("a");
        var submenu = li.children("ul");
        var submenu2 = li2.children("ul");

        ok(link.children("span.ui-icon-triangle-1-e").length === 1,
        "when has any children is an visible item, show the submenu icon");
        ok(link2.children("span.ui-icon-triangle-1-e").length === 0,
        "when all child menuitem have been set displayVisible as false, the menu shold not show the default icon");
        ok(submenu.is(":hidden"), "submenu is hidden.");

        var subli = submenu.find('li:first');
        subli.wijmenuitem({ displayVisible: false });

        var subli2 = submenu2.find('li:first');
        subli2.wijmenuitem({ displayVisible: true });

        link.trigger("click");
        window.setTimeout(function () {
            ok(submenu.is(":hidden"), "clickthe item, do not show the item.");
            link2.trigger("click");

            window.setTimeout(function () {
                ok(menu.find("#hidden_2").is(":visible"), "click the item, show li2");
                ok(submenu2.is(":visible"), "click the item, show the submenu");
                menu.remove();
                start();
            }, 750);
        }, 750);
        stop();
    });

    test("menuitem's  imagePosition and iconClass options", function () {
        var menu = $("<ul><li><a>aa</a></li><li><a>bb</a></li><li><h3>b</h3></li><li></li></ul>").appendTo("body");

        var options = {
            items: [{
                iconClass: 'abc',
                imagePosition: 'left'
            }, {
                imagePosition: 'right',
                iconClass: 'bcd'
            }, {
                iconClass: 'abc',
                imagePosition: 'right'
            }, {
                iconClass: 'abc',
                imagePosition: 'right'
            }]
        };

        menu.wijmenu(options);

        var icon0 = menu.find('li:eq(0)').find('span.wijmenuitem-icon');
        var icon1 = menu.find('li:eq(1)').find('span.wijmenuitem-icon');
        var icon2 = menu.find('li:eq(2)').find('span.wijmenuitem-icon');
        var icon3 = menu.find('li:eq(3)').find('span.wijmenuitem-icon');

        ok(icon0.length === 1 &&
            icon0.hasClass(options.items[0].iconClass) &&
            icon0.hasClass("wijmo-wijmenu-icon-left"),
        "icon0 has correct iconClass and position");

        ok(icon1.length === 1 &&
            icon1.hasClass(options.items[1].iconClass) &&
            icon1.hasClass("wijmo-wijmenu-icon-right"),
        "icon1 has correct iconClass and position");

        ok(icon2.length === 0, "the icon won't apply to the header menuitem");
        ok(icon3.length === 0, "the icon won't apply to the header menuitem");
        menu.find('li:eq(0)').wijmenuitem({
            imagePosition: 'right',
            iconClass: 'a'
        });

        menu.find('li:eq(1)').wijmenuitem({
            iconClass: 'b',
            imagePosition: 'left'
        });

        ok(icon0.length === 1 &&
            icon0.hasClass('a') &&
            icon0.hasClass("wijmo-wijmenu-icon-right"),
        "icon0 has correct iconClass and position");

        ok(icon1.length === 1 &&
            icon1.hasClass('b') &&
            icon1.hasClass("wijmo-wijmenu-icon-left"),
        "icon1 has correct iconClass and position");

        menu.remove();
    });


    test("menu's items options", function () {
        var menu = $("<ul><li><a>aa</a></li><li><a>bb</a></li><li><h3>b</h3></li><li></li></ul>").appendTo("body");

        menu.wijmenu({});

        var options = {
            items: [{
                text: 'a',
                items: [{
                    text: 'a1',
                    items: [{
                        text: 'a11'
                    }, {
                        text: 'a12'
                    }]
                }, {
                    text: 'a2'
                }]
            }, {
                header: true,
                text: 'b',
                items: [{
                    text: 'b1'
                }]
            }]
        };

        menu.wijmenu(options);
        var widget = menu.data('wijmo-wijmenu');
        ok(widget._getSublist().children().length === options.items.length,
        "When specified items argument in options, the markup should be change to the items sepcified in the options");
        ok(widget._items.length === options.items.length, "the _items property has the same count as options");
        var li = widget._getSublist().children().eq(0);
        var itemWidget = li.data('wijmo-wijmenuitem');
        ok(li.find(">ul>li").length === itemWidget.options.items.length, "child menu has correct count of item")

        var li1 = widget._getSublist().children().eq(1);
        ok(li1.children().length === 1 && li1.children().is("h3"), "When header is true, not apply items to page");

        var link = li.children("a");
        var subLink = li.find('ul>li:first>a');
        //test for flyout event
        link.trigger("mouseenter");
        window.setTimeout(function () {
            ok(li.is(":visible"), "menu set by option has flyout event");
            subLink.trigger("mouseenter");
            window.setTimeout(function () {
                ok(subLink.parent().find("ul:first>li").is(":visible"), "submenu has flyout event");
                menu.remove();
                start();
            }, 750);
        }, 750);
        stop();
    });

    test("menuitems's items options", function () {
        var menu = $("<ul><li><a>aa</a></li><li><a>bb</a></li><li><h3>b</h3></li><li><a>c</a><ul><li><a>c1</a></li></ul></li></ul>").appendTo("body");
        menu.wijmenu({});

        var options = {
            items: [{
                text: 'a',
                items: [{
                    text: 'a1',
                    items: [{
                        text: 'a11'
                    }, {
                        text: 'a12'
                    }]
                }, {
                    text: 'a2'
                }]
            }, {
                header: true,
                text: 'b',
                items: [{
                    text: 'b1'
                }]
            }]
        };
        var li = menu.find("li:eq(0)");
        li.wijmenuitem(options);
        var widget = li.data('wijmo-wijmenuitem');
        ok(li.children('ul').children().length === options.items.length,
        "When specified items argument in options, the markup should be change to the items sepcified in the options");
        ok(widget._items.length === options.items.length, "the _items property has the same count as options");

        var itemWidget = li.data('wijmo-wijmenuitem');
        ok(li.find(">ul>li").length === itemWidget.options.items.length, "child menu has correct count of item")

        var link = li.children("a");
        var subLink = li.find('ul>li:first>a');

        link.trigger("mouseenter");
        window.setTimeout(function () {
            ok(li.is(":visible"), "menu set by option has flyout event");
            subLink.trigger("mouseenter");

            window.setTimeout(function () {
                ok(subLink.parent().find("ul:first>li").is(":visible"), "submenu has flyout event");
                menu.remove();
                start();
            }, 750);
        }, 750);
        stop();
    });

    test("menuitems's items options", function () {
        var menu = $("<ul><li><a>aa</a></li><li><a>bb</a></li><li><h3>b</h3></li><li><a>c</a><ul><li><a>c1</a></li></ul></li></ul>").appendTo("body");

        menu.wijmenu({ mode: 'sliding' });
        var options = {
            items: [{
                text: 'a',
                items: [{
                    text: 'a1',
                    items: [{
                        text: 'a11'
                    }, {
                        text: 'a12'
                    }]
                }, {
                    text: 'a2'
                }]
            }, {
                header: true,
                text: 'b',
                items: [{
                    text: 'b1'
                }]
            }]
        };
        var li = menu.find("li:eq(0)");
        li.wijmenuitem(options);
        var widget = li.data('wijmo-wijmenuitem');
        ok(li.children('ul').children().length === options.items.length,
        "When specified items argument in options, the markup should be change to the items sepcified in the options");
        ok(widget._items.length === options.items.length, "the _items property has the same count as options");

        var itemWidget = li.data('wijmo-wijmenuitem');
        ok(li.find(">ul>li").length === itemWidget.options.items.length, "child menu has correct count of item")

        var link = li.children("a");
        var subLink = li.find('ul>li:first>a');

        link.trigger("click");
        window.setTimeout(function () {
            ok(li.is(":visible"), "menu set by option has drilldown event");
            subLink.trigger("click");

            window.setTimeout(function () {
                ok(subLink.parent().find("ul:first>li").is(":visible"), "submenu has drilldown event");
                menu.remove();
                start();
            }, 750);
        }, 750);
        stop();
    });

    test("menuitems's selected options", function () {
        function selected(item) {
            return item.wijmenuitem('option').selected === true &&
                item.children(':first').is('.ui-state-active')
        }

        function unselected(item) {
            return item.wijmenuitem('option').selected === false &&
                !item.children(':first').is('.ui-state-active')
        }
        function test(mode) {
            var menu = $("<ul><li id='menu1'><a>menuitem</a></li><li id='menu2'><a>menuitem</a></li>" +
                "<li id='header'><h3>header</h3></li><li id='sp'></li>" +
                "</ul>").appendTo("body");

            menu.wijmenu({
                mode: mode,
                checkable: true, items: [{
                }, {
                    selected: true
                }]
            });

            var item1 = $('#menu1'),
                item2 = $('#menu2'),
                header = $('#header'),
                sp = $('#sp');

            ok(selected(item2), 'the item2 has been selected during the inital stage');

            item1.children().click();
            ok(selected(item1), 'click the item1, the item1 has been selected');

            item1.children().click();
            ok(unselected(item1), 'click the item1 again, the item1 has been unselected');

            header.children().click();
            ok(unselected(header), 'click the header item , the item should not be selected');

            sp.click();
            ok(unselected(sp), 'click the separate item , the item should not be selected');

            var keyCode = keycode = $.ui.keyCode;
            var e = jQuery.Event("keydown", { keyCode: keycode.ENTER });
            //the flyout cannot simulate the enter event, so break these case when flyout testing
            if (mode !== 'flyout') {
                menu.wijmenu('activate', e, item1);
                item1.find('a').focus().trigger(e);
                //menu.trigger(e);
                ok(selected(item1), 'press ENTER key, the activeItem has been selected');

                menu.trigger(e);
                ok(unselected(item1), 'press ENTER key again, the activeItem has been selected');

                //set checkable as false, which would not response the event
                menu.wijmenu({ checkable: false });

                item1.children().click();
                ok(unselected(item1), 'click the item1, the item1 would not be selected');

                menu.trigger(e);
                ok(unselected(item1), 'press ENTER key, the activeItem would not be selected');
            }
            //test for setOtpions
            item1.wijmenuitem({ selected: true });
            ok(selected(item1), 'set selected as true whateveer the checkable value');

            item1.wijmenuitem({ selected: false });
            ok(unselected(item1), 'set selected as false whateveer the checkable value');

            header.wijmenuitem({ selected: true });
            ok(unselected(item1), 'the selected of header item is always false');

            sp.wijmenuitem({ selected: true });
            ok(unselected(item1), 'the selected of separate item is always false');
            menu.remove();
        }

        test('flyout');
        test('sliding');
    });

    test("ensureSubmenuOnBody option", function () {
        var menu = $("<ul id='menu1'><li><a>menuitem</a><ul>" +
            "<li><a>submenuHeader</a></li><li><a>submenuItem</a></li></ul></li></ul>").appendTo('body');
        menu.wijmenu({
            ensureSubmenuOnBody: true,
            items: [{
                text: "menuitem", items: [
                   { header: true, text: "submenuHeader", items: [] },
                   { text: "submenuItem", items: [] }
                ]
            }]
        });

        equal($('body').children('div.wijmo-wijmenu-submenu-container').length, 1, 'the submenu is appended to body directly');
        equal($('body').children('.wijmo-wijmenu').children('ul').data('wijmomenu'), 'wijmo-wijmenu', 'the data of the submenu container stores the same data of the original data');
        ok($('body').children('.wijmo-wijmenu').children('ul').data('wijmo-wijmenu'), 'the submenu container stores the wijmenu object');

        menu.wijmenu("destroy");
        menu.remove();
    });

    test("maxHeight option", function () {
        var $menu = createSlidingMenu(),
            $container = $menu.data("wijmo-wijmenu").domObject.scrollcontainer,
            height = $container.height();

        $menu.wijmenu("option", "maxHeight", 100);
        ok($container.height() < height, "menu height changed.");
        $menu.remove();
    });

})(jQuery);

function testTriggerEvent(event) {
    test("triggerEvent-" + event, function () {
        var trigger = $("<button>").appendTo("body");
        var menu = $("<ul><li><a>menuitem</a></li></ul>").appendTo("body");
        menu.wijmenu({ trigger: trigger, triggerEvent: event });
        ok(menu.is(":hidden"), "menu element is hidden.");
        if (event === "rtclick") {
            trigger.trigger("contextmenu");
        } else {
            trigger.trigger(event);
        }
        setTimeout(function () {
            ok(menu.is(":visible"), "trigger " + event + " the element,show the menu item.");
            $("<b>").appendTo("body").click().remove();
            window.setTimeout(function () {
                ok(menu.is(":hidden"), "click the other elements,hide the menu. menu element is hidden.");
                trigger.remove();
                menu.remove();
                start();
            }, 500);
        }, 500)
        stop();
    });
}