﻿/*
 * wijprogressbar_core.js
 */

function create_wijmenu(options) {
    var menu = $('<ul>'
        + '<li><h3>header</h3></li>'
        + '<li></li>'
        + '<li><a href="#">menuitem</a></li>'
        + '<li><a href="#">menuitem1</a><ul>'
        + '<li><a href="#">submenu</a></li>'
        + '</ul></li>'
        + '</ul>');
    menu.appendTo("body");
    return menu.wijmenu(options);
}

function createSlidingMenu(options) {
    var $menu = $('<ul id="menu1"><li><a href="#">MenuItem</a></li><li><a href="#">Breaking News</a><ul><li class="header"><h3>header2</h3></li><li class="separator">s1</li><li><a href="#">Entertainment</a></li><li><a href="#">Politics</a></li><li><a href="#">A&amp;E</a></li><li><a href="#">Sports</a> </li><li><a href="#">Local</a></li><li><a href="#">Health</a></li></ul></li><li><a href="#">Entertainment</a><ul><li><a href="#">Celebrity news</a></li><li><a href="#">Gossip</a></li><li><a href="#">Movies</a></li><li><a href="#">Music</a><ul><li><a href="#">Alternative</a></li><li><a href="#">Country</a></li><li><a href="#">Dance</a></li><li><a href="#">Electronica</a></li><li><a href="#">Metal</a></li><li><a href="#">Pop</a></li><li><a href="#">Rock</a><ul><li><a href="#">Bands</a><ul><li><a href="#">Dokken</a></li></ul></li><li><a href="#">Fan Clubs</a></li><li><a href="#">Songs</a></li></ul></li></ul></li><li><a href="#">Slide shows</a></li><li><a href="#">Red carpet</a></li></ul></li><li><a href="#">Finance</a><ul><li><a href="#">Personal</a><ul><li><a href="#">Loans</a></li><li><a href="#">Savings</a></li><li><a href="#">Mortgage</a></li><li><a href="#">Debt</a></li></ul></li><li><a href="#">Business</a></li></ul></li><li><a href="#">Food &#38; Cooking</a><ul><li><a href="#">Breakfast</a></li><li><a href="#">Lunch</a></li><li><a href="#">Dinner</a></li><li><a href="#">Dessert</a><ul><li><a href="#">Dump Cake</a></li><li><a href="#">Doritos</a></li><li><a href="#">Both please.</a></li></ul></li></ul></li><li><a href="#">Lifestyle</a></li><li><a href="#">News</a></li><li><a href="#">Politics</a></li><li><a href="#">Sports</a></li><li><a href="#">Novels</a></li><li><a href="#">Magazine</a></li><li><a href="#">Books</a></li><li><a href="#">Education</a></li></ul>')
        .appendTo(document.body).wijmenu($.extend(options, {
            mode: 'sliding'
        }));
    return $menu;
}

(function($) {
    module("wijmenu: core");
    test("create and destroy flyout menu", function () {
        var $widget = create_wijmenu();
        ok($widget.hasClass("wijmo-wijmenu-list ui-helper-reset"), "wijmenu element add the correct css class.");
        var domObject = $widget.data("wijmo-wijmenu").domObject;
        ok(domObject, "manucontainer and scrollcontainer created.");
        var scrollcontainer = domObject.scrollcontainer;
        ok(scrollcontainer.hasClass("scrollcontainer checkablesupport"), "scrollcontainer add correct css class.");
        var menucontainer = domObject.menucontainer;
        ok(menucontainer.hasClass("ui-widget ui-widget-header wijmo-wijmenu ui-corner-all ui-helper-clearfix"), "menucontainer add correct css class.");
        var headerItem = $("li:has(h3)", $widget);
        ok(headerItem.hasClass("ui-widget-header ui-corner-all"), "header item created.");
        var separatorItem = $("li:has(.wijmo-wijmenu-separator-content)", $widget);
        ok(separatorItem.hasClass("wijmo-wijmenu-separator ui-state-default ui-corner-all"), "separator item created.");
        var menuitem = $("li:has(a)", $widget);
        ok(menuitem.hasClass("ui-widget wijmo-wijmenu-item ui-state-default ui-corner-all"), "menu item created.")
        var linkele = menuitem.children("a");
        ok(linkele.hasClass("wijmo-wijmenu-link ui-corner-all"), "link element add correct css class.");
        var menutext = linkele.children("span:first");
        ok(menutext.hasClass("wijmo-wijmenu-text"), "menu item text add correct css class.");
        var parentItem = $("li:has(ul)", $widget);
        ok(parentItem.hasClass("ui-widget wijmo-wijmenu-item ui-state-default ui-corner-all wijmo-wijmenu-parent"), "parent menu item add correct css class.");
        var icon = parentItem.find("span.ui-icon");
        ok(icon.hasClass("ui-icon") && icon.hasClass("ui-icon-triangle-1-s"), "submenu icon displayed correct.");
        var sublist = parentItem.children("ul");
        ok(sublist.hasClass("wijmo-wijmenu-list ui-widget-content ui-corner-all ui-helper-clearfix wijmo-wijmenu-child ui-helper-reset"), "submenu element add correct css class.");

        //remove widget.
        $widget.wijmenu("destroy");
        ok(!$widget.parent().hasClass("scrollcontainer checkablesupport"), "wraped element removed.");
        ok(!$widget.hasClass("wijmo-wijmenu-list ui-helper-reset"), "menu element removed css class.");
        ok(!headerItem.hasClass("ui-widget-header ui-corner-all"), "header item removed css class.");
        ok(!separatorItem.hasClass("wijmo-wijmenu-separator ui-state-default ui-corner-all"), "separator item removed css class.");
        ok(!menuitem.hasClass("ui-widget wijmo-wijmenu-item ui-state-default ui-corner-all"), "menu item removed css class.");
        ok(!linkele.hasClass("wijmo-wijmenu-link ui-corner-all"), "link element removed css class correctly.");
        ok(linkele.children("span").length === 0, "menu item text removed css class correctly.");
        ok(!sublist.hasClass("wijmo-wijmenu-list ui-widget-content ui-corner-all ui-helper-clearfix wijmo-wijmenu-child ui-helper-reset"), "submenu element remove css class correctly.");
        ok(!$widget.data("domObject"), "data removed.");
        $widget.remove();
    });

    test("create and destroy sliding menu", function () {
        var $widget = create_wijmenu({
            mode: "sliding",
            orientation: 'vertical'
        });
        ok($widget.hasClass("wijmo-wijmenu-list ui-helper-reset wijmo-wijmenu-content wijmo-wijmenu-current ui-widget-content ui-helper-clearfix"), "menu element added css class correctly.");
        var domObject = $widget.data("wijmo-wijmenu").domObject;
        ok(domObject, "manucontainer and scrollcontainer created.");
        var scrollcontainer = domObject.scrollcontainer;
        ok(scrollcontainer.hasClass("scrollcontainer checkablesupport"), "scrollcontainer add correct css class.");
        var menucontainer = domObject.menucontainer;
        ok(menucontainer.hasClass("ui-widget ui-widget-header wijmo-wijmenu ui-corner-all ui-helper-clearfix wijmo-wijmenu-ipod wijmo-wijmenu-container"), "menucontainer add correct css class.");
        var headerItem = $("li:has(h3)", $widget);
        ok(headerItem.hasClass("ui-widget-header ui-corner-all"), "header item created.");
        var separatorItem = $("li:has(.wijmo-wijmenu-separator-content)", $widget);
        ok(separatorItem.hasClass("wijmo-wijmenu-separator ui-state-default ui-corner-all"), "separator item created.");
        var menuitem = $("li:has(a)", $widget);
        ok(menuitem.hasClass("ui-widget wijmo-wijmenu-item ui-state-default ui-corner-all"), "menu item created.")
        var linkele = menuitem.children("a");
        ok(linkele.hasClass("wijmo-wijmenu-link ui-corner-all"), "link element add correct css class.");
        var menutext = linkele.children("span:first");
        ok(menutext.hasClass("wijmo-wijmenu-text"), "menu item text add correct css class.");
        ok($(".wijmo-wijmenu-breadcrumb", menucontainer).length > 0, "breadcrumb element created.");
        $widget.wijmenu("destroy");
        ok(!$widget.hasClass("wijmo-wijmenu-list ui-helper-reset wijmo-wijmenu-content wijmo-wijmenu-current ui-widget-content ui-helper-clearfix"), "menu element removed css class correctly.");
        ok($widget.parent().is("body"), "wraped element is removed");
        ok(!headerItem.hasClass("ui-widget-header ui-corner-all"), "header item removed.");
        ok(!separatorItem.hasClass("wijmo-wijmenu-separator ui-state-default ui-corner-all"), "separator item removed.");
        ok(!menuitem.hasClass("ui-widget wijmo-wijmenu-item ui-state-default ui-corner-all"), "menu item removed.")
        ok(!linkele.hasClass("wijmo-wijmenu-link ui-corner-all"), "link element removed css class.");
        ok(linkele.has("span").length === 0, "added span element removed.");
        ok($(".wijmo-wijmenu-breadcrumb").length === 0, "breadcrumb element removed.");
        ok(!$widget.data("domObject"), "data removed.");
        $widget.remove();
    });

    test("create menuitem", function () {
        var menu = create_wijmenu();

        options = { text: 'text', navigateUrl: 'url', target: '_blank', iconClass: 'ui-icon ui-icon-arrowthick-1-w' };
        var li = $('<li><a>li</a></li>').appendTo(menu);
        li.wijmenuitem(options);
        var a = li.find('>:first');
        var icon = a.find('>span>span.ui-icon');
        ok(a.text() == options.text, 'the text of menuitem IS the value set in the options');
        ok(a.attr('href') == options.navigateUrl, 'the navigateUrl of menuitem IS the value set in the options');
        ok(a.attr('target') == options.target, 'the target of menuitem IS the value set in the options');
        ok(icon.length == 1 && icon.hasClass('ui-icon-arrowthick-1-w'), 'has the icon classes specifed in the options');

        options.header = true;
        li.wijmenuitem('destroy');
        li = $('<li><a>li</a></li>').appendTo(menu).wijmenuitem(options);
        a = li.find('>:first');
        icon = a.children('span.ui-icon');
        ok(a.is('h1,h2,h3,h4,h5'), 'set header as true, the child should be an h markup');
        ok(icon.length == 0, 'set header as true, should no icon class');

        options.header = false;
        options.separator = true;

        li.wijmenuitem('destroy').wijmenuitem(options);
        ok(li.hasClass('wijmo-wijmenu-separator') && li.find(".wijmo-wijmenu-separator-content").html() === '&nbsp;', 'the innerHtml of the li is empty');
        ok(li.hasClass('wijmo-wijmenu-separator'), 'the li has class wijmo-wijmenu-separator');
        li.wijmenuitem('destroy').remove();

        //test html markup is h1
        li = $('<li><h1>aaa</h1></li>').appendTo(menu);
        options = { text: 'text' };
        li.wijmenuitem(options);
        var widget = li.data('wijmo-wijmenuitem');
        a = li.find('>:first');
        icon = a.children('span.ui-icon');
        ok(a.is('h1'), 'the child is an h1 markup');
        ok(a.text() === options.text, 'when specified text value in options, the mark up should be modified');
        ok(icon.length == 0, 'has no icon class');
        ok(widget.options.header === true, 'the options of widegt have been set header as true');
        li.wijmenuitem('destroy').remove();

        //test markup is an empty li should be set as separator
        li = $('<li></li>').appendTo(menu);
        options = { };
        li.wijmenuitem(options);
        ok(li.hasClass('wijmo-wijmenu-separator') && li.find(".wijmo-wijmenu-separator-content").html() === '&nbsp;', 'the innerHtml of the li is empty');
        ok(li.hasClass('wijmo-wijmenu-separator'), 'the li has class wijmo-wijmenu-separator');
        li.remove();

        li = $('<li></li>').appendTo(menu);
        options = { text: 'abc' };
        li.wijmenuitem(options);
        var a = li.find('>:first');
        ok(a.is('a'), 'when markup is an empty li and specified text, the item should be a link');
        ok(a.text() == options.text, 'the text of menuitem IS the value set in the options');
        li.remove();
        menu.remove();

        //testing for iconClass
        menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a><ul><li><a>menu item 111</a></li><li><a>menu item 112</a></li></ul></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu();
        li = menu.find(':first');
        li.wijmenuitem('destroy').wijmenuitem({ iconClass: 'abc' });
        link = li.find('a');
        icon = link.find('span.wijmenuitem-icon');
        ok(icon.hasClass('abc'), 'menuitem HAS the icon user specified in the options');
        menu.remove();

        //test for creating menuitems by mixing with items and html markup to 
        menu = $("<ul><li><a>menu item1</a></li><li><a>menu item2</a></li><li><a>c</a><ul><li></li><li></li><li><a>c2</a></li></ul></li><li></li><li></li></ul>").appendTo('body');
        menu.wijmenu({
            items: [{
                text: 'a'
            }, {
                text: 'b',
                header: true,
                items: [{
                    text: 'b1'
                }]
            }, {
                items: [{
                    text: 'c1'
                }]
            }, {
                text: 'd',
                items: [{
                    text: 'd1'
                }]
            }, {
                items: [{
                    text: 'e1'
                }] //when specified as separator or header, items should make no effect
            }, {
                text: 'f1'
            }] //create the new item
        });

        var liList = menu.find('>li');
        ok(liList.eq(0).text() === 'a', 'li\'s value have been repalced to the value specified in the options');

        ok(liList.eq(1).find('ul').length === 0 && liList.eq(1).text() === 'b' && liList.eq(1).children().is('h3'),
            'li2 have been replaced to the header, and it\'s items options did not append to the page');

        ok(liList.eq(2).find('>ul>li').length == 3 &&
            liList.eq(2).find('>ul>li').eq(0).text() === 'c1' &&
            liList.eq(2).find('>ul>li').eq(2).text() === 'c2',
            'li 3 has 3 children and correct text');

        ok(liList.eq(3).find('>ul>li').length === 1 &&
            liList.eq(3).children('a').text() === 'd' &&
            liList.eq(3).find('>ul>li').eq(0).text() === 'd1',
            'the items can append to the li correctly');

        ok(liList.eq(4).find('ul').length === 0 && liList.eq(4).find(".wijmo-wijmenu-separator-content").html() === '&nbsp;',
            'when li is an separator, it\'s items options wouldn\'t append to the page');

        ok(liList.eq(5).text() === 'f1', 'can add a new item in options');
        menu.remove();
    });

    test("create menuitem by options", function () {
        var menu = $('<ul>').appendTo('body');
        menu.wijmenu({
            items: [{
                text: 'a'
            }, {
                text: 'h',
                header: true
            }, {
                text: 'c',
                items: [{
                    text: 'c1',
                    header: true
                }, {
                    text: 'c2',
                    separator: true
                }, {
                    text: 'c3',
                    items: [{
                        text: 'c31',
                        navigateUrl: 'url',
                        target: '_blank',
                        iconClass: 'ui-icon ui-icon-arrowthick-1-w',
                        imagePosition: 'left'
                    }]
                }]
            }]
        });

        var menuItems = menu.data('wijmo-wijmenu')._items;
        ok(menuItems.length === 3, 'the count of _items equals the count user specifed in the options');
        ok(menu.children(':first').children().is('a'), 'the first item is an link');
        ok(menu.children(':eq(1)').children().is('h1,h2,h3,h4,h5') && menuItems[1].options.header === true, 'the second item is an header');
        ok(menu.children(':eq(2)').children().is('a'), 'the third item is an link');

        ok(menu.children(':eq(2)').find('>ul>li').length === 3 && menuItems[2]._items.length == 3, 'the third item has 3 child items');
        var thirdItem = menu.children(':eq(2)');
        ok(thirdItem.find('>ul>li:eq(1)').find(".wijmo-wijmenu-separator-content").html() === '&nbsp;' && menuItems[2]._items[1].options.separator === true, 'the c2 item is an separator item');
        var c31 = thirdItem.find('ul>li:eq(2)').find('>ul>li');
        ok(c31.length === 1 && menuItems[2]._items[2]._items.length == 1, 'the c3 item has 1 child items');
        ok(c31.children('a').text() === 'c31' && c31.children('a').attr('href') === 'url' &&
            c31.children('a').attr('target') === '_blank' &&
            c31.find('span.ui-icon').hasClass('ui-icon-arrowthick-1-w') &&
            c31.find('span.ui-icon').hasClass('wijmo-wijmenu-icon-left'),
            'the c31 item have been set the values which specified in the options');

        menu.remove();
    });

    test("destroy and remove menuitem", function () {
        var menu = $("<ul><li id='m1'><a>menu item1</a><ul><li id='m11'><a>menu item11</a></li></ul></li><li id='m2'><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu();

        var m11 = $("#m11"),
            m1 = $("#m1"),
            m2 = $("#m2");
        m11.remove();
        ok(m1.wijmenuitem('getItems').length === 0, 'after invoked jquery.remove method, parent will remove the item from _items collection');
        m1.remove();
        ok(menu.wijmenu('getItems').length === 1 && menu.wijmenu('getItems')[0]=== m2.data('wijmo-wijmenuitem'), 'after invoked jquery.remove method, parent will remove the item from _items collection');
        m2.remove();
        ok(menu.wijmenu('getItems').length === 0, 'after invoked jquery.remove method, parent will remove the item from _items collection');
        menu.remove();
    });
})(jQuery);
