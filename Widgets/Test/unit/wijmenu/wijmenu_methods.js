﻿/*
 * wijmenu_methods.js
 */

(function ($) {

    module("wijmenu:functions");

    test("activate", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu();
        var li = menu.children("li:first");
        menu.wijmenu("activate", null, li);
        ok(li.children("a").hasClass("ui-state-focus"), "the item is active.");
        menu.remove();
    });

    test("deactivate", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu();
        var li = menu.children("li:first");
        li.children("a").click();
        ok(li.children("a").hasClass("ui-state-focus"), "the item is active.");
        menu.wijmenu("deactivate", null);
        setTimeout(function () {
            start();
            ok(!li.children("a").hasClass("ui-state-focus"), "called the 'deactivate' method, the item is not active.");
            menu.remove();
        }, 500);
        stop();
    });

    test("next", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu();
        var li = menu.children("li:first");
        li.children("a").click();
        ok(li.children("a").hasClass("ui-state-focus"), "the item is active.");
        menu.wijmenu("next");
        ok(li.next().children("a").hasClass("ui-state-focus"), "called the 'next' method, the next item is active");
        setTimeout(function () {
            start();
            ok(!li.children("a").hasClass("ui-state-focus"), "called the 'next' method, this item is not active.");
            menu.remove();
        }, 500);
        stop()
    });

    test("previous", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu();
        var li = menu.children("li:last");
        li.children("a").click();
        ok(li.children("a").hasClass("ui-state-focus"), "the item is active.");
        menu.wijmenu("previous");
        ok(li.prev().children("a").hasClass("ui-state-focus"), "called the 'previous' method, the previous item is active");
        setTimeout(function () {
            start();
            ok(!li.children("a").hasClass("ui-state-focus"), "called the 'previous' method, this item is not active.");
            menu.remove();
        }, 500);
        stop();
    });

    test("first", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu();
        var first = menu.children("li:first");
        var last = menu.children("li:last");
        first.children("a").click();
        ok(menu.wijmenu("first"), "first menu item is active.");
        last.children("a").click();
        ok(!menu.wijmenu("first"), "when the other item is active.call the 'first' method,return false.");
        menu.remove();
    });

    test("last", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu();
        var first = menu.children("li:first");
        var last = menu.children("li:last");
        first.children("a").click();
        ok(!menu.wijmenu("last"), "when other item is active,call the 'last' method,return false.");
        last.children("a").click();
        ok(menu.wijmenu("last"), "when the last item is active.call the 'last' method,return true.");
        menu.remove();
    });

    test("previousPage", function () {
        var $menu = createSlidingMenu(),
            instance = $menu.data("wijmo-wijmenu"),
            selectedIndex = function () {
                return $.inArray(instance.activeItem, instance._items);
            };
        $menu.wijmenu("previousPage");
        ok(selectedIndex() === 0, "items 0 is active.");
        $menu.wijmenu("previousPage");
        ok(selectedIndex() === 12, "items 12 is active.");
        $menu.wijmenu("previousPage");
        ok(selectedIndex() === 6, "items 6 is active.");
        $menu.remove();
    });

    test("nextPage", function () {
        var $menu = createSlidingMenu(),
            instance = $menu.data("wijmo-wijmenu"),
            selectedIndex = function () {
                return $.inArray(instance.activeItem, instance._items);
            };
        $menu.wijmenu("nextPage");
        ok(selectedIndex() === 6, "items 6 is active.");
        $menu.wijmenu("nextPage");
        ok(selectedIndex() === 12, "items 12 is active.");
        $menu.wijmenu("nextPage");
        ok(selectedIndex() === 0, "items 0 is active.");
        $menu.remove();
    });

    test("select", function () {
        var a = 0;
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu();
        var first = menu.children("li:first");
        first.children("a").click();
        menu.wijmenu({ select: function () { a = 2; } })
        menu.wijmenu("select");
        ok(a == 2, "select event triggered.");
        menu.remove();
    });

    test("refresh", function () {
        var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu.wijmenu();
        var li = $("<li><a>menu item2</a></li>");
        menu.append(li);
        li = menu.children("li:last");
        ok(!li.hasClass("wijmo-wijmenu-item"), "the added item isn't added the menu item css class.");
        menu.wijmenu("refresh");
        ok(li.hasClass("wijmo-wijmenu-item"), "called the refresh method, the added item isn't added the menu item css class.");
        menu.remove();
    });

    //test('abcd', function () {
    //    var menu = $("<ul><li><a>menu item1</a></li><li><a>menu item2</a></li></ul>").appendTo("body");
    //    menu['wijmenu']({ mode: 'sliding' });
    //    var li = menu.find('li:last');
    //    var options = {
    //        text: 'aaa',
    //        items: [{
    //            text: 'a1',
    //            items: [{
    //                text: 'a11'
    //            }]
    //        }]
    //    };
    //    li.wijmenuitem('add', options);
    //});

    test("add", function () {
        var $menu = $("<ul><li><a>menu item1</a></li><li><a>menu item2</a></li></ul>").appendTo("body");
        $menu.wijmenu();

        function testMenuOrLi(menu, widgetName) {
            var liCount = menu.find('li').length,
                options, link;

            // var li = $('<li id="liToAdd"><a>liToAdd</a></li>');
            // menu.wijmenu('add', li);
            // ok(li.parent()[0] == menu[0] , 'li have been appended to the correct place');
            // liCount++;
            // ok(menu.find('li').length === liCount, 'Both value of liCount and menu.length are ' + liCount);


            //add by element, it no repate too 
            // var ele = document.getElementById('liToAdd');
            // debugger;
            // menu.wijmenu('add', ele);
            // //no repeated element
            // ok(menu.find('li').length === liCount, 'Both value of liCount and menu.length are ' + liCount);


            //insert an li which contains a child
            // var li2 = $('<li id="liToAdd"><a>liToAddaaaaa</a><ul><li><a>ccc</a></li></ul></li>');
            // menu.wijmenu('add', li2);
            // liCount +=2;
            // ok(menu.find('li').length === liCount, 'Both value of liCount and menu.length are ' + liCount);

            //add a text as first argument
            menu[widgetName]('add', '<a>an text</a>');
            liCount++;
            ok(menu.find('li').length === liCount, 'Both value of liCount and menu.length are ' + liCount);
            ok($('li:last', menu).find('>a').text() == 'an text', 'The text of the last element IS the value we just inserted');
            testForMenuOptions($menu);

            menu[widgetName]('add', '<a href="http://a.html/">text 1</a>');
            liCount++;
            ok(menu.find('li').length === liCount, 'Both value of liCount and menu.length are ' + liCount);
            link = $('li:last', menu).find('>a');
            ok(link.text() === 'text 1' && link.attr('href') === 'http://a.html/', 'The text and href of the last element IS the value we just inserted');
            testForMenuOptions($menu);

            //add a new item 

            options = { text: 'added by options', navigateUrl: 'aaa' };
            menu[widgetName]('add', options);
            li = $('li:last', menu);
            liCount++;
            ok(menu.find('li').length === liCount, 'Both value of liCount and menu.length are ' + liCount);
            link = $('li:last', menu).find('>a');
            ok(link.text() == options.text && link.attr('href') == options.navigateUrl, 'The text and href of the last element IS the value we specified in options');
            testForMenuOptions($menu);
            //validate whether the sort of li matched with widget._items
            // var array = menu.data('wijmenu')._items;
            // var liList = menu.find('>li');
            // for (var i = 0; i < array.length; i ++){
            //     if (array[i] !== $(liList.get(i)).data('wijmenuitem')) {
            //         console.log('The item at index ' + i + ' is not matched the li');
            //     }
            // }

            //add an seperator li 
            options = { text: 'separator', navigateUrl: 'aaa', separator: true };
            menu[widgetName]('add', options);
            liCount++;
            ok(menu.find('li').length === liCount, 'Both value of liCount and menu.length are ' + liCount);
            li = menu.find('li:last');
            //an separator should be an empty li
            ok(li.hasClass('wijmo-wijmenu-separator') && li.find(".wijmo-wijmenu-separator-content").html() === '&nbsp;', 'The innerHtml of separator item is empty string');
            //has class 
            ok(li.hasClass('wijmo-wijmenu-separator') && li.hasClass('ui-state-default') && li.hasClass('ui-corner-all'),
                'The separator cssclasses have been added to the item correctly');
            testForMenuOptions($menu);
            //create an sperator item by html markup
            menu[widgetName]('add', '');
            li = menu.find('li:last');
            liCount++;
            ok(menu.find('li').length === liCount, 'Both value of liCount and menu.length are ' + liCount);
            //add an markup separator it options should be set sparator as true and has cssClass
            ok(li.data('wijmo-wijmenuitem').options.separator === true && li.hasClass('wijmo-wijmenu-separator'),
                'add an markup separator successfully, it options have  been set sparator == true and markup has cssClass');
            testForMenuOptions($menu);
            //add an header li 
            options = { text: 'header', navigateUrl: 'aaa', header: true };
            menu[widgetName]('add', options);
            liCount++;
            ok(menu.find('li').length === liCount, 'Both value of liCount and menu.length are ' + liCount);

            li = menu.find('li:last');
            //an separator should be an empty li
            ok(li.text() == options.text && li.hasClass('ui-widget-header'),
                'the header li added by option has the text which specified in the option.text and has header cssClass');
            testForMenuOptions($menu);
        }

        //test menu
        testMenuOrLi($menu, 'wijmenu');
        var li = $menu.find("li:first");
        //test menu item
        testMenuOrLi(li, 'wijmenuitem');
        $menu.remove();

        //begin test add items with POSITION
        $menu = $("<ul><li><a>menu item1</a></li><li><a>menu item2</a></li></ul>").appendTo("body");
        $menu['wijmenu']({ mode: 'sliding' });

        function testForMenuOrLi2(menu, widgetName) {
            var position = 1;
            widget = menu.data("wijmo-" + widgetName);
            liHtml = '<a>sub item 3</a>';

            menu[widgetName]('add', liHtml);
            menu[widgetName]('add', liHtml);
            menu[widgetName]('add', liHtml, position);
            //debugger;
            li = menu.find('li').eq(position);
            ok(li.find('>a').text() == 'sub item 3' && widget._items[position].element[0] === li[0],
                'The new li has been insered to the middle of the array');
            testForMenuOptions($menu);

            position = 0;
            menu[widgetName]('add', liHtml, position);
            li = menu.find('li').eq(position);
            ok(li.find('>a').text() == 'sub item 3' && widget._items[position].element[0] === li[0],
                'The new li has been insered to the beginning of the array');
            testForMenuOptions($menu);

            position = widget._items.length;
            menu[widgetName]('add', liHtml, position * 100);
            li = menu.find('li').eq(position);
            ok(li.find('>a').text() == 'sub item 3' && widget._items[position].element[0] === li[0],
                'The new li have been insered to the end of the array');
            testForMenuOptions($menu);

            options = {
                text: 'aaa',
                items: [{
                    text: 'a1',
                    items: [{
                        text: 'a11'
                    }]
                }]
            };

            menu[widgetName]('add', options);
            testForMenuOptions($menu);
            var ul = widget._getSublist() || menu.children('ul:first');
            li = ul.find('>li:last');
            var fnValidator = function (l, o, deep) {
                ok(l.children('a').text() === o.text, 'items at deep ' + deep + ' has the correct style');
                if (!o.items) {
                    return;
                }
                for (var i = 0; i < options.items.length; i++) {
                    fnValidator(l.find('>ul>li').eq(i), o.items[i], deep++);
                }
            }
            fnValidator(li, options, 0);

            // var link = li.children("a");
            // var subLink = li.find('li:first>a');
            // debugger;
            // ok(subLink.is(":hidden"), "the sub link is hidden at first");
            // link.trigger("click");
            // window.setTimeout(function () {
            //     ok(subLink.is(":visible"), "menu set by option has drilldown event");
            //     ok(subLink.parent().find("ul:first>li").is(":hidden"), "the child link of sublink is hidden");
            //     subLink.trigger("click");
            // 
            //     window.setTimeout(function () {
            //         ok(subLink.parent().find("ul:first>li").is(":visible"), "submenu has drilldown event");
            //     }, 750);
            // }, 750);

            // function testFlyout() {
            //     var m = widgetName === 'wijmenu' ? widget : widget._getMenu();
            //     m.element.wijmenu({ mode: 'flyout' });
            //     var link = li.children("a");
            //     var subLink = li.find('ul>li:first>a');
            //     link.trigger("mouseenter");
            //     window.setTimeout(function () {
            //         ok(li.is(":visible"), "menu set by option has flyout event");
            //         subLink.trigger("mouseenter");
            //         window.setTimeout(function () {
            //             ok(subLink.parent().find("ul:first>li").is(":visible"), "submenu has flyout event");
            //             start();
            //         }, 750);
            //     }, 750);
            // }
            // window.setTimeout(testFlyout, 1000 * 2);
            // stop();
        }
        testForMenuOrLi2($menu, 'wijmenu');
        var li = $menu.find('li:first');
        testForMenuOrLi2(li, 'wijmenuitem');
        $menu.remove();
    });

    test("test add method width sliding mode", function () {
        addEventTest('sliding', 'click');
    });

    test('test add method width flyoout mode', function () {
        addEventTest('flyout', 'mouseenter');
    });

    function addEventTest(mode, event) {
        var widgetName = 'wijmenu';
        var menu = $("<ul><li><a>menu item1</a></li><li><a>menu item2</a></li></ul>").appendTo("body");
        menu['wijmenu']({ mode: mode });

        var widget = menu.data("wijmo-" + widgetName);
        options = {
            text: 'aaa',
            items: [{
                text: 'a1',
                items: [{
                    text: 'a11'
                }]
            }]
        };
        menu[widgetName]('add', options);

        var ul = widget._getSublist() || menu.children('ul:first');
        li = ul.find('>li:last');
        var link = li.children("a");
        var subLink = li.find('li:first>a');

        ok(subLink.is(":hidden"), "the sub link is hidden at first");
        link.trigger(event);
        window.setTimeout(function () {
            ok(subLink.is(":visible"), "menu set by option has drilldown event");
            ok(subLink.parent().find("ul:first>li").is(":hidden"), "the child link of sublink is hidden");
            subLink.trigger(event);

            window.setTimeout(function () {
                ok(subLink.parent().find("ul:first>li").is(":visible"), "submenu has drilldown event");
                var subLi = subLink.parent().find("ul:first>li");
                //add items to subli for testing menuitem's add method
                subLi.wijmenuitem(options);
                var li3 = subLi.find('>ul>li:first');
                ok(li3.is(":hidden"), 'add new items it will hidden at default');
                subLi.find('>a:first').trigger(event);
                window.setTimeout(function () {
                    ok(li3.is(":visible"), 'li is visible');
                    menu.remove();
                    start();
                }, 750);
            }, 750);
        }, 750);
        stop();
    }

    function testForMenuOptions(menu) {
        var options = menu.wijmenu("option"),
            _items = menu.wijmenu("getItems"),
            _isSame = true;
        for (i = 0; i < options.items.length; i++) {
            if (_items[i].options !== options.items[i]) {
                _isSame = false;
                break;
            }
        }
        ok(_isSame, "menu options with the same as sub menu options");
    }

    test('remove', function () {
        var menu = $("<ul><li><a>menu item1</a></li><li><a>menu item2</a><ul>" +
            "<li><a>21</a></li><li><a>22</a></li><li><a>23</a></li>" +
            "<li><a>24</a></li><li><a>25</a></li><li><a>26</a></li>" +
            "</ul></li>" +
            "</ul>").appendTo("body");
        menu.wijmenu({ mode: 'sliding' });

        var li2 = menu.children('li:eq(1)');
        var childLicount = li2.find('>ul>li').length;
        var li2widget = li2.data('wijmo-wijmenuitem');
        while (childLicount !== 0) {
            var random = Math.round(Math.random() * (childLicount - 1))
            var cLi = li2.find('li').eq(random);
            ok(cLi.data("wijmo-wijmenuitem") === li2widget._items[random],
                "the li was matched the index in the parent._items, when randoming " + random);

            li2widget.remove(random);
            var idxOfElementToCheck = Math.max(0, random - 1);
            childLicount--;

            if (childLicount !== 0) {
                ok(childLicount === li2widget._items.length &&
                    childLicount === li2.find('>ul>li').length &&
                    li2.find('ul>li')[idxOfElementToCheck] === li2widget._items[idxOfElementToCheck].element[0],
                "li has been removed");
            }
        }

        menu.wijmenu('remove', 1);
        testForMenuOptions(menu);
        ok(menu.find(">li").length === 1, "removed second of menuitem");

        menu.wijmenu('remove', 0);
        testForMenuOptions(menu);
        ok(menu.find(">li").length === 0, "all menuitem has been removed");
        menu.remove();
    });
})(jQuery)