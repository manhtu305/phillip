﻿/*
 * wijmenu_events.js
 */

(function ($) {
	module("wijmenu:events");
	test("focus", function () {
		var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
		menu.wijmenu();
		var item1 = menu.children("li:first");
		menu.focus();
        menu.wijmenu({
            focus: function (e, item) {
			ok(item.item.element.get(0) == item1.get(0), "the event is triggered. the menu item can get it in the event.");
		}
		});
		menu.wijmenu("activate", null, item1);
		menu.remove();
	});

	test("Key pressing", function () {
		var menu = $("<ul><li ><a>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
		menu.wijmenu();

		var widget = menu.data('wijmo-wijmenu');
		//mock the methods just test whether the widget has invoked the right method
		var invokedMethod = '';
		var getInvokedMethod = function () {
			var result = invokedMethod;
			invokedMethod = '';
			return result;
		}
		widget = $.extend(widget, {
			next: function () {
				invokedMethod = 'next';
			},
			previous: function () {
				invokedMethod = 'previous';
			},
			nextPage: function () {
				invokedMethod = 'nextPage';
			},
			previousPage: function () {
				invokedMethod = 'previousPage';
			}
		});

		var menuItemToMock = {
			_showFlyoutSubmenu: function () {
				invokedMethod = '_showFlyoutSubmenu';
			},
			_hideCurrentSubmenu: function () {
				invokedMethod = '_hideCurrentSubmenu';
			}
		}

		menu.focus();
		//common
		var keyCode = keycode = $.ui.keyCode;
		var e = jQuery.Event("keydown", { keyCode: keycode.PAGE_UP });
		menu.trigger(e);
		ok(getInvokedMethod() === "previousPage", "when pressing PAGE_UP key in menu, invoke previousPage method");

		//flyout
		//test a horizontal flyout menu 
		e = jQuery.Event("keydown", { keyCode: keycode.LEFT });
		menu.trigger(e);
		ok(getInvokedMethod() === "previous", "press LEFT key in horizontal menu, go to previous menuitem");
		e = jQuery.Event("keydown", { keyCode: keycode.RIGHT });
		menu.trigger(e);
		ok(getInvokedMethod() === "next", "press RIGHT key in horizontal menu, go to next menuitem");

		//test menuitem with child menu
		var li = $('li:has(ul):first');
		liWidget = li.data('wijmo-wijmenuitem');
		liWidget = $.extend(liWidget, menuItemToMock);
		widget.activate(e, li);

		e = jQuery.Event("keydown", { keyCode: keycode.DOWN });
		menu.trigger(e);
		ok(getInvokedMethod() === "_showFlyoutSubmenu", "press DOWN key when activing an item which has child item in horizontal menu, show child menu");

		li = $('li:first', li);
		liWidget = li.data('wijmo-wijmenuitem');
		liWidget = $.extend(liWidget, menuItemToMock);
		widget.activate(e, li);
		e = jQuery.Event("keydown", { keyCode: keycode.LEFT });
		menu.trigger(e);
		ok(getInvokedMethod() === "_hideCurrentSubmenu", "press Left key when activing a child item , hide child menu");

		//test vertical orientation
		widget.activate(e, li);
		menu.wijmenu({ orientation: 'vertical' });
		e = jQuery.Event("keydown", { keyCode: keycode.UP });
		menu.trigger(e);
		ok(getInvokedMethod() === "previous", "press UP key in horizontal menu, go to previous menuitem");
		e = jQuery.Event("keydown", { keyCode: keycode.DOWN });
		menu.trigger(e);
		ok(getInvokedMethod() === "next", "press DOWN key in horizontal menu, go to next menuitem");

		menu.remove();
	});

	test("rtl key pressing", function () {
		var menu = $("<ul><li ><a>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
		menu.wijmenu({ direction: "rtl" });

		var widget = menu.data('wijmo-wijmenu');
		//mock the methods just test whether the widget has invoked the right method
		var invokedMethod = '';
		var getInvokedMethod = function () {
			var result = invokedMethod;
			invokedMethod = '';
			return result;
		}
		widget = $.extend(widget, {
			next: function () {
				invokedMethod = 'next';
			},
			previous: function () {
				invokedMethod = 'previous';
			},
			nextPage: function () {
				invokedMethod = 'nextPage';
			},
			previousPage: function () {
				invokedMethod = 'previousPage';
			}
		});

		var menuItemToMock = {
			_showFlyoutSubmenu: function () {
				invokedMethod = '_showFlyoutSubmenu';
			},
			_hideCurrentSubmenu: function () {
				invokedMethod = '_hideCurrentSubmenu';
			}
		}

		menu.focus();
		//common
		var keyCode = keycode = $.ui.keyCode;
		var e = jQuery.Event("keydown", { keyCode: keycode.PAGE_UP });
		menu.trigger(e);
		ok(getInvokedMethod() === "previousPage", "when pressing PAGE_UP key in menu, invoke previousPage method");

		//flyout
		//test a horizontal flyout menu 
		e = jQuery.Event("keydown", { keyCode: keycode.LEFT });
		menu.trigger(e);
		ok(getInvokedMethod() === "next", "press LEFT key in horizontal menu, go to previous menuitem");
		e = jQuery.Event("keydown", { keyCode: keycode.RIGHT });
		menu.trigger(e);
		ok(getInvokedMethod() === "previous", "press RIGHT key in horizontal menu, go to next menuitem");

		//test menuitem with child menu
		var li = $('li:has(ul):first');
		liWidget = li.data('wijmo-wijmenuitem');
		liWidget = $.extend(liWidget, menuItemToMock);
		widget.activate(e, li);

		e = jQuery.Event("keydown", { keyCode: keycode.DOWN });
		menu.trigger(e);
		ok(getInvokedMethod() === "_showFlyoutSubmenu", "press DOWN key when activing an item which has child item in horizontal menu, show child menu");

		li = $('li:first', li);
		liWidget = li.data('wijmo-wijmenuitem');
		liWidget = $.extend(liWidget, menuItemToMock);
		widget.activate(e, li);
		e = jQuery.Event("keydown", { keyCode: keycode.RIGHT });
		menu.trigger(e);
		ok(getInvokedMethod() === "_hideCurrentSubmenu", "press Right key when activing a child item , hide child menu");

		//test vertical orientation
		widget.activate(e, li);
		menu.wijmenu({ orientation: 'vertical' });
		e = jQuery.Event("keydown", { keyCode: keycode.UP });
		menu.trigger(e);
		ok(getInvokedMethod() === "previous", "press UP key in horizontal menu, go to previous menuitem");
		e = jQuery.Event("keydown", { keyCode: keycode.DOWN });
		menu.trigger(e);
		ok(getInvokedMethod() === "next", "press DOWN key in horizontal menu, go to next menuitem");

		menu.remove();
	});

	/*test the menu event showing & shown*/
	test("menu:showing,shown", function () {
		var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a class='a_link'>menu item2</a></li></ul>").appendTo("body");
		var $btn = $("<button id='btn1'>click here</button>").appendTo("body");
		menu.wijmenu({
			showing: function () {
				ok(true, "showing excute success.");
			},
			shown: function () {
				ok(true, "shown excute success.");
				start();
				menu.remove();
				$btn.remove();
			},
			trigger: "#btn1"
		});

		$btn.click();
		stop();
	});

	/*test the menu event hidding & hidden*/
	test("menu:hidding,hidden", function () {
		var menu = $("<ul><li id='triggerele2'><a class='a_link'>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a class='a_link2'>menu item2</a></li></ul>").appendTo("body");
		var $btn = $("<button id='btn2'>click here</button>").appendTo("body");
		menu.wijmenu({
			trigger: "#btn2",
			hidding: function () {
				ok(true, "hidding excute success.");
			},
			hidden: function () {
				ok(true, "hidden excute success.");
				start();
				menu.remove();
				$btn.remove();
			},
			shown: function () {
				var $Link2 = menu.find(".a_link");
				$Link2.simulate("click");
			}
		});
		$btn.trigger("click");
		stop();
	});


	/*test the submenu event showing & shown*/
	test("submenu:showing,shown", function () {
		var menu = $("<ul><li id='triggerele'><a class='a_link'>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a class='a_link2'>menu item2</a></li></ul>").appendTo("body");
		menu.wijmenu({
			showing: function () {
				ok(true, "showing excute success.");
			},
			shown: function () {
				ok(true, "shown excute success.");
				start();
				menu.remove();
			}
		});

		var $Link = menu.find(".a_link");
		$Link.simulate("mouseover");

		stop();
	});

	/*test the submenu event hidding & hidden*/
	test("submenu:hidding,hidden", function () {
		var menu = $("<ul><li id='triggerele2'><a class='a_link'>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a class='a_link2'>menu item2</a></li></ul>").appendTo("body");
		menu.wijmenu({
			hidding: function () {
				ok(true, "hidding excute success.");
			},
			hidden: function () {
				ok(true, "hidden excute success.");
				start();
				menu.remove();
			},
			shown: function () {
				var $Link = menu.find(".a_link");
				$Link.simulate("mouseout");
			}
		});

		var $Link = menu.find(".a_link");
		$Link.simulate("mouseover");
		stop();
	});

	test("sliding:showing, shown, hidding, hidden", function () {
        var menu = '<ul id="menu1">'
			+ '<li><a href="#">menu 1</a>'
				+ '<ul>'
					+ '<li><a href="#">menu 11</a></li>'
					+ '<li><a href="#">menu 12</a></li>'
					+ '<li><a href="#">menu 13</a></li>'
					+ '<li><a href="#">menu 1</a></li>'
				+ '</ul>'
			+ '</li>'
			+ '<li><a href="#">menu 2</a></li>'
			+ '<li><a href="#">menu 3</a></li>'
			+ '<li><a href="#">menu 4</a></li>'
		+ '</ul>',
		$wijmenu = $(menu).appendTo("body")
			.wijmenu({
				mode: 'sliding',
				showDelay: 0,
				hideDelay: 0,
				slidingAnimation: { duration: 0, easing: null },
				showing: function () {
					eventCount++;
					ok(true, 'showing is fire');
				},
				shown: function () {
					eventCount++;
					ok(true, 'shown is fire');
				},
				hidding: function () {
					eventCount++;
					ok(true, 'hidding is fire when backlink is true');
				},
				hidden: function () {
					eventCount++;
					ok(true, 'hidden is fire when backlink is true');
				}
			}),
		eventCount = 0;
		$wijmenu.find(">li>a").eq(0).click();
		$(".wijmo-wijmenu-breadcrumb>a").click();
		ok(eventCount === 4, "has 4 events are fired");
		$("#menu1").wijmenu("destroy");
		$("#menu1").remove();
	});

	test("sliding:showing, shown, hidding, hidden when backLink is false", function () {
		var eventCount = 0,
			menu =
			'<ul id="menu1">'
			+ '<li><a href="#">menu 1</a>'
				+ '<ul>'
					+ '<li><a href="#">menu 11</a></li>'
					+ '<li><a href="#">menu 12</a></li>'
					+ '<li><a href="#">menu 13</a>'
						+ '<ul>'
							+ '<li><a href="#">menu 131</a>'
								+ '<ul>'
									+ '<li><a href="#">menu 1311</a>'
									+ '<li><a href="#">menu 1312</a>'
									+ '<li><a href="#">menu 1313</a>'
								+ '</ul>'
							+ '</li>'
							+ '<li><a href="#">menu 132</a></li>'
							+ '<li><a href="#">menu 133</a></li>'
						+ '</ul>'
					+ '</li>'
					+ '<li><a href="#">menu 1</a></li>'
				+ '</ul>'
			+ '</li>'
			+ '<li><a href="#">menu 2</a></li>'
			+ '<li><a href="#">menu 3</a></li>'
			+ '<li><a href="#">menu 4</a></li>'
		+ '</ul>',
		menulevel = 0,
		$wijmenu = $(menu).appendTo("body").wijmenu({
			mode: 'sliding',
			showDelay: 0,
			hideDelay: 0,
			slidingAnimation: { duration: 0, easing: null },
			backLink: false,
			showing: function () {
				ok(true, 'showing is fire');
				
				if (menulevel === 2) {
					return false;
				}
				menulevel++;
			},
			shown: function () {
				ok(true, 'shown is fire ');
			},
			hidding: function () {
				ok(true, 'hidding is fire when backlink is false');
				
				if (menulevel === 1) {
					return false;
				}
				menulevel--;
			},
			hidden: function () {
				ok(true, 'hidden is fire when backlink is false');
			}
		});

		$wijmenu.find(">li>a").eq(0).click();
		$wijmenu.find(">li>ul>li>a").eq(2).click();
		$wijmenu.find(">li>ul>li>ul>li>a").eq(0).click();
        ok($(".wijmo-wijmenu-current").parent().find(">a").text() === "menu 13", "current menu is 'menu 13'");
		$(".wijmo-wijmenu-all-lists>a").click();
        ok($(".wijmo-wijmenu-current").parent().find(">a").text() === "menu 1", "current menu is 'menu 1'");
		$("#menu1").wijmenu("destroy");
		$("#menu1").remove();
	});
	
	// Test add iframe when showing submenu
	test("submenu:add iframe when showing", function () {
		var menu = $("<ul><li id='triggerele'><a>menu item1</a><ul><li><a>menu item11</a></li></ul></li><li><a>menu item2</a></li></ul>").appendTo("body");
		var li, link, frame;
		menu.wijmenu({
		    hidden: function () {
		        if ($.browser.msie) {
		            frame = li.children('iframe.bgiframe');
		            ok(frame.length === 0, "the iframe is removed after hiding.");
		        }
		        menu.wijmenu("destroy");
		        menu.remove();
		    },
		    shown: function () {
		        frame = li.children('iframe.bgiframe');
		        if ($.browser.msie) {
		            ok(frame.length > 0, "the iframe has been added when showing the submenu");
		            ok(frame.css('display') !== 'none', "the iframe is shown");
                } else {
		            ok(frame.length === 0, "the iframe is not needed in non-IE browser.");
		        }
		        link.trigger("mouseleave");		        
		    }
		});
		li = menu.find("li:first");
		link = li.children("a");
		frame = li.children('iframe.bgiframe');
		ok(frame.length === 0, "no iframe");
		link.trigger("mouseenter");
	});
})(jQuery)