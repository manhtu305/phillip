﻿/*
 * wijmenu_default.js
 */

var wijmenu_default = {
    initSelector: ":jqmData(role='wijmenu')",
    wijCSS: $.extend({}, $.wijmo.wijCSS, {
        wijmenu: "wijmo-wijmenu",
        wijmenuHorizontal: "wijmo-wijmenu-horizontal",
        wijmenuCurrent: "wijmo-wijmenu-current",
        wijmenuCurrentCrumb: "wijmo-wijmenu-current-crumb",
        wijmenuBreadcrumb: "wijmo-wijmenu-breadcrumb",
        wijmenuBreadcrumbText: "wijmo-wijmenu-breadcrumb-text",
        wijmenuItem: "wijmo-wijmenu-item",
        wijmenuLink: "wijmo-wijmenu-link",
        wijmenuList: "wijmo-wijmenu-list",
        wijmenuScroll: "wijmo-wijmenu-scroll",
        wijmenuContent: "wijmo-wijmenu-content",
        wijmenuFooter: "wijmo-wijmenu-footer",
        wijmenuHeader: "wijmo-wijmenu-header",
        wijmenuContainer: "wijmo-wijmenu-container",
        wijmenuIpod: "wijmo-wijmenu-ipod",
        wijmenuCrumb: "wijmo-wijmenu-crumb",
        wijmenuAllLists: "wijmo-wijmenu-all-lists",
        wijmenuPrevList: "wijmo-wijmenu-prev-list",
        wijmenuRtl: "wijmo-wijmenu-rtl",
        wijmenuBacklinktext: "wijmo-wijmenu-backlinktext",
        wijmenuSubmenuContainer: "wijmo-wijmenu-submenu-container",
        wijmenuParent: "wijmo-wijmenu-parent"
    }),
    wijMobileCSS: {
        header: "ui-header ui-bar-b",
        content: "ui-body ui-body-b",
        stateDefault: "ui-btn ui-btn-b",
        stateHover: "ui-btn-down-c",
        stateActive: "ui-btn-down-c"
    },
    create: null,
    disabled: false,
    trigger: '',
    triggerEvent: 'click',
    submenuTriggerEvent: 'default',
    position: {},
    animation: { animated: "slide", duration: 400, easing: null, option: null },
    showAnimation: {},
    hideAnimation: { animated: "fade", duration: 400, easing: null, option: null },
    showDelay: 400,
    hideDelay: 400,
    slidingAnimation: { duration: 400, easing: null },
    mode: 'flyout',
    superPanelOptions: null,
    checkable: false,
    orientation: 'horizontal',
    direction: 'ltr',
    maxHeight: 200,
    backLink: true,
    backLinkText: 'Back',
    topLinkText: 'All',
    crumbDefaultText: 'Choose an option',
    select: null,
    focus: null,
    blur: null,
    showing: null,
    shown: null,
    hidding: null,
    hidden: null,
    items: null,
    ensureSubmenuOnBody: false
};
commonWidgetTests('wijmenu', {
    defaults: wijmenu_default
});
