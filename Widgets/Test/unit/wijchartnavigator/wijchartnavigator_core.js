﻿/*
* wijchartnavigator.js
*/

function create_wijchartnavigator(options) {
    var chartnavigator = $('<div id="chartnavigator1" style="width:400px; height:30px;"></div>');
    chartnavigator.appendTo("body");
    return chartnavigator.wijchartnavigator(options);
}

(function ($) {

    module("wijchartnavigator: core");
    test("create and destroy", function () {
        var $widget = create_wijchartnavigator();
        ok($widget.hasClass("wijmo-wijchartnavigator"), "element css classes created.");
        ok($widget.hasClass("ui-widget wijmo-wijlinechart"), "LineChart has been created.");
        ok($widget.find(".wijmo-wijchartnavigator-slider.ui-slider.ui-slider-horizontal.ui-widget").length === 1, "Slider element has been created.");
        
        $widget.wijchartnavigator("destroy");
        ok(!$widget.hasClass("wijmo-wijchartnavigator"), "element css classes has been removed.");
        ok($widget.children().length === 0, "LineChart and Slider elements are been removed.");

        $widget.remove();
    });
})(jQuery);