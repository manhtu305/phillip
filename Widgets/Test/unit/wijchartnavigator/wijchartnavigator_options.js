﻿/*
 * wijchartnavigator_options.js
 */
"use strict";
(function ($) {
    module("wijchartnavigator:options");

    test("targetSelector", function () {
        var chart1 = $("<div id='chart1' style='width:400px;height:300px'></div>")
            .appendTo("body").wijlinechart({
                seriesList: [{
                    label: "1800",
                    legendEntry: true,
                    markers: { visible: true },
                    data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [107, 31, 635, 203] }
                }]
            }),
            chart2 = $("<div id='chart2' style='width:400px;height:300px'></div>")
            .appendTo("body").wijlinechart({
                seriesList: [{
                    label: "1900",
                    legendEntry: true,
                    markers: { visible: true },
                    data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [133, 156, 947, 408] }
                }]
            }),
            chartnavigator = create_wijchartnavigator({
                targetSelector: "#chart1",
                seriesList: [{
                    data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [107, 31, 635, 203] }
                }],
                rangeMin: 1,
                rangeMax: 2
            });

        ok(chart1.wijlinechart("option", "seriesList")[0].data.y.length === 2, "Target chart1 has updated its dataSource !");

        chartnavigator.wijchartnavigator("option", "targetSelector", "#chart2");
        ok(chart1.wijlinechart("option", "seriesList")[0].data.y.length === 4, "Target chart1 has been recovered !");
        ok(chart2.wijlinechart("option", "seriesList")[0].data.y.length === 2, "Target chart2 has updated its dataSource !");

        chartnavigator.wijchartnavigator("option", "targetSelector", "#chart1,#chart2");
        ok(chart1.wijlinechart("option", "seriesList")[0].data.y.length === 2, "Target chart1 has updated its dataSource !");

        chartnavigator.wijchartnavigator("option", "targetSelector", "");
        ok(chart1.wijlinechart("option", "seriesList")[0].data.y.length === 4, "Target chart1 has been recovered !");
        ok(chart2.wijlinechart("option", "seriesList")[0].data.y.length === 4, "Target chart2 has updated its dataSource !");

        chartnavigator.remove();
        chart1.remove();
        chart2.remove();
    });

    test("rangeMin", function () {
        var chart1 = $("<div id='chart1' style='width:400px;height:300px'></div>")
            .appendTo("body").wijlinechart({
                seriesList: [{
                    label: "1800",
                    legendEntry: true,
                    markers: { visible: true },
                    data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [107, 31, 635, 203] }
                }]
            }), chartnavigator = create_wijchartnavigator({
                targetSelector: "#chart1",
                seriesList: [{
                    data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [107, 31, 635, 203] }
                }],
                rangeMin: 1
            });

        ok(chart1.wijlinechart("option", "seriesList")[0].data.y[0] === 31, "Option \"rangeMin\" worked when initial !");
        chartnavigator.wijchartnavigator("option", "rangeMin", 2);
        ok(chart1.wijlinechart("option", "seriesList")[0].data.y[0] === 635, "Option \"rangeMin\" worked when set option !");

        chartnavigator.remove();
        chart1.remove();
    });

    test("rangeMax", function () {
        var chart1 = $("<div id='chart1' style='width:400px;height:300px'></div>")
            .appendTo("body").wijlinechart({
                seriesList: [{
                    label: "1800",
                    legendEntry: true,
                    markers: { visible: true },
                    data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [107, 31, 635, 203] }
                }]
            }), chartnavigator = create_wijchartnavigator({
                targetSelector: "#chart1",
                seriesList: [{
                    data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [107, 31, 635, 203] }
                }],
                rangeMax: 2
            });

        ok(chart1.wijlinechart("option", "seriesList")[0].data.y.length === 3, "Option \"rangeMax\" worked when initial !");
        chartnavigator.wijchartnavigator("option", "rangeMax", 1);
        ok(chart1.wijlinechart("option", "seriesList")[0].data.y.length === 2, "Option \"rangeMax\" worked when set option !");

        chartnavigator.remove();
        chart1.remove();
    });

    test("step", function () {
        var chart1 = $("<div id='chart1' style='width:400px;height:300px'></div>")
            .appendTo("body").wijlinechart({
                seriesList: [{
                    label: "1800",
                    legendEntry: true,
                    markers: { visible: true },
                    data: { x: [0, 100, 200, 300, 400], y: [107, 31, 635, 203, 100] }
                }]
            }), chartnavigator = create_wijchartnavigator({
                targetSelector: "#chart1",
                seriesList: [{
                    data: { x: [0, 100, 200, 300, 400], y: [107, 31, 635, 203, 100] }
                }],
                step: 100
            }), leftHandler = chartnavigator.find(".ui-slider-handle").eq(0);

        chartnavigator.wijchartnavigator("option", "rangeMin", 70);
        ok(leftHandler[0].style.left === "25%", "Option step works");
        chartnavigator.wijchartnavigator("option", "step", 50);
        chartnavigator.wijchartnavigator("option", "rangeMin", 70);
        ok(leftHandler[0].style.left === "12.5%", "Option step works");

        chartnavigator.remove();
        chart1.remove();
    });

}(jQuery));