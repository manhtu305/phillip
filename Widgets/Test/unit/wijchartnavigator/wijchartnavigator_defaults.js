﻿/*globals commonWidgetTests*/
/*
* wijchartnavigator_defaults.js
*/
"use strict";
commonWidgetTests('wijchartnavigator', {
    defaults: {
        targetSelector: "",
        dataSource: undefined,
        data: undefined,
        seriesList: [],
        seriesStyles: undefined,
        xAxis: undefined,
        indicator: undefined,
        rangeMin: undefined,
        rangeMax: undefined,
        step: undefined,
        updating: null,
        updated: null
    }
});