﻿/*
* wijchartnavigator_events.js
*/

"use strict";
(function ($) {
    var seriesList = [{
        label: "1800",
        legendEntry: true,
        data: {
            x: [100, 200, 300, 400, 500, 600, 700, 800],
            y: [100, 31, 635, 203, 133, 156, 947, 408]
        }
    }];

    module("wijchartnavigator: events");

    test("updating", function () {
        var chartnavigator = create_wijchartnavigator({
            seriesList: seriesList,
            rangeMin: 300,
            rangeMax: 500,
            updating: function (event, data) {
                triggerCounts++;
            }
        }), sliderHandle = chartnavigator.find(".ui-slider-handle").eq(0),
            rangeHandle = chartnavigator.find(".ui-slider-range"),
            triggerCounts = 0;

        sliderHandle.simulate("drag", { dx: 150, dy: 0 });
        ok(triggerCounts > 1, "Updating has been triggered !");
        triggerCounts = 0;
        rangeHandle.simulate("drag", { dx: -50, dy: 0 });
        ok(triggerCounts > 1, "Updating has been triggered !");
        chartnavigator.remove();
    });

    test("updated", function () {
        var chartnavigator = create_wijchartnavigator({
            seriesList: seriesList,
            rangeMin: 300,
            rangeMax: 500,
            updated: function (event, data) {
                triggerCounts++;
            }
        }), sliderHandles = chartnavigator.find(".ui-slider-handle"),
            rangeHandle = chartnavigator.find(".ui-slider-range"), triggerCounts = 0;

        sliderHandles.eq(0).simulate("mouseover").simulate("drag", { dx: 150, dy: 0 });
        ok(triggerCounts === 1, "Updated triggered !");
        triggerCounts = 0;
        sliderHandles.eq(1).simulate("mouseover").simulate("drag", { dx: -150, dy: 0 });
        ok(triggerCounts === 1, "Updated triggered !");
        triggerCounts = 0;
        rangeHandle.simulate("mouseover").simulate("drag", { dx: -50, dy: 0 });
        ok(triggerCounts === 1, "Updated triggered !")

        chartnavigator.remove();
    });

    test("updated by code", function () {
        var chartnavigator = create_wijchartnavigator({
            seriesList: seriesList,
            updated: function (event, data) {
                triggerCounts++;
            }
        }), triggerCounts = 0;

        chartnavigator.wijchartnavigator("option", "rangeMin", 300);
        ok(triggerCounts === 1, "Updated triggered !");

        chartnavigator.wijchartnavigator("option", "rangeMax", 400);
        ok(triggerCounts === 2, "Updated triggered !");

        chartnavigator.remove();
    });
}(jQuery));