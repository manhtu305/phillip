﻿(function ($) {
    var el;
    module("wijchartnavigator: tickets");

    var seriesList = [{
        label: "1800",
        legendEntry: true,
        data: {
            x: [100, 200, 300, 400, 500, 600, 700, 800],
            y: [100, 31, 635, 203, 133, 156, 947, 408]
        }
    }];

    test("#106055", function () {
        var triggeredUpdating = false,
            triggeredUpdated = false,
            chartnavigator = create_wijchartnavigator({
                seriesList: seriesList,
                rangeMin: 300,
                rangeMax: 500,
                updating: function (event, data) {
                    if (triggeredUpdated) {
                        triggeredUpdating = true;
                    }
                },
                updated: function () {
                    triggeredUpdated = true;
                }
            }),
            rangeHandle = chartnavigator.find(".ui-slider-range").eq(0);
        rangeHandle.simulate("mouseover").simulate("mousedown").simulate("drag", { dx: 150, dy: 0 });
        ok(!triggeredUpdating, "The \"updating\" event will not be triggered after \"updated\" .");
        chartnavigator.remove();
    });

    test("#106538", function () {
        var triggeredUpdating = false,
            triggeredUpdated = false,
            chartnavigator = create_wijchartnavigator({
                seriesList: seriesList,
                updating: function (event, data) {
                    triggeredUpdating = true;
                },
                updated: function () {
                    triggeredUpdated = true;
                }
            }),
            rangeHandle = chartnavigator.find(".ui-slider-range").eq(0);
        rangeHandle.simulate("drag", { dx: 100, dy: 0 });
        ok(!triggeredUpdating, "The \"updating\" event should not be triggered .");
        ok(!triggeredUpdated, "The \"updated\" event should not be triggered .");
        chartnavigator.remove();
    });

    test("#110235", function () {
        var triggerCounts = 0,
            chart1 = $("<div id='chart1' style='width:400px;height:300px'></div>")
            .appendTo("body").wijlinechart({
                seriesList: seriesList,
                beforePaint: function () {
                    triggerCounts++;
                    return false;
                }
            }),
            chartnavigator = create_wijchartnavigator({
                targetSelector: "#chart1",
                seriesList: seriesList,
                rangeMin: 300,
                rangeMax: 500
            });

        ok(triggerCounts === 2, "Trigger the event \"beforePaint\" twice !");
        chartnavigator.remove();
        chart1.remove();
    });

    test("If target chart is removed, JS error will occur when update navigator", function () {
        var chart1 = $("<div id='chart1' style='width:400px;height:300px'></div>")
            .appendTo("body").wijbarchart({
                seriesList: [{
                    label: "1800",
                    legendEntry: true,
                    markers: { visible: true },
                    data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [107, 31, 635, 203] }
                }]
            }),
            chartnavigator = create_wijchartnavigator({
                targetSelector: "#chart1",
                seriesList: [{
                    data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [107, 31, 635, 203] }
                }],
                rangeMin: 1,
                rangeMax: 2
            });

        chart1.wijbarchart("destroy");
        chartnavigator.wijchartnavigator("option", "rangeMin", 0);
        chartnavigator.wijchartnavigator("option", "rangeMin", 1);

        ok(true, "JS error doesn't occur !");

        chart1.wijbarchart({
            seriesList: [{
                label: "1800",
                legendEntry: true,
                markers: { visible: true },
                data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [107, 31, 635, 203] }
            }]
        });
        chartnavigator.wijchartnavigator("option", "rangeMin", 0);
        chartnavigator.wijchartnavigator("option", "rangeMin", 1);
        ok(true, "JS error doesn't occur !");

        chart1.wijbarchart("destroy");
        chartnavigator.remove();
        ok(chart1.data("wijmo-wijbarchart") === undefined, "BarChart doesn't recover.");
        chart1.remove();
    });

})(jQuery)