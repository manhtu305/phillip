﻿/*globals test, ok, jQuery, module, document, $*/
/*
* wijtreemap_core.js create & destroy
*/
"use strict"

var data = [{
        label: "a",
        value: 80,
        items: [{
            label: "a.a",
            value: 22
        },{
            label: "a.b",
            value: 25
        },{
            label: "a.c",
            value: 23
        },{
            label: "a.d",
            value: 10
        }]
    }, {
        label: "b",
        value: 50,
        items: [{
            label: "b.a",
            value: 12
        },{
            label: "b.b",
            value: 25
        },{
            label: "b.c",
            value: 13
        }]
    }, {
        label: "c",
        value: 52,
        items: [{
            label: "c.a",
            value: 12
        },{
            label: "c.b",
            value: 20
        },{
            label: "c.c",
            value: 20
        }]
    }, {
        label: "d",
        value: 130,
        items: [{
            label: "d.a",
            value: 22
        },{
            label: "d.b",
            value: 48
        },{
            label: "d.c",
            value: 50
        },{
            label: "d.d",
            value: 10
        }]
    }, {
        label: "e",
        value: 39,
        items: [{
            label: "e.a",
            value: 12
        },{
            label: "e.b",
            value: 27,
            items: [{
                label: "e.b.a",
                value: 13
            },{
                label: "e.b.b",
                value: 14
            }]
        }]
    }];

function createTreeMap(o, parent) {
    var par = parent == null ? "body": parent,
        treemap = $(getDefaultTreeMapDom()).appendTo(par).wijtreemap($.extend({
        data: data
    }, o));
    return treemap;
}

function getDefaultTreeMapDom() {
    return '<div style="width: 800px;height: 600px;" class=""></div>';
}

(function ($) {
    module("wijtreemap: core");

    test("create and destroy", function() {
        var parent = $("<div></div>").appendTo("body"),
            treemap = createTreeMap(null, parent),
            dom = getDefaultTreeMapDom(),
            treemapHTML;
        ok(treemap.hasClass("wijmo-wijtreemap"), "wijtreemap class is added to treemap element");
        ok(treemap.data("wijmo-wijtreemap"), "wijtreemap widget is added as data to treemap element");
        domEqual(dom, function () {
            treemap.wijtreemap("destroy");
			return parent.html();
        }, parent.html()); 
		parent.remove();
    });
}(jQuery));