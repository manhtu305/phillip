﻿
"use strict";
(function ($) {
    module("wijtreemap: methods");

    test("refresh", function () {
        var treemap = createTreeMap(), width = 600,
            firstItem = treemap.find(".wijmo-wijtreemap-item.wijstate-default:first");
        firstItem.simulate("click");
        ok(firstItem.outerWidth() === 800, "current view is 'a.a'");
        treemap.css("width", "600px");
        treemap.wijtreemap("refresh");
        //601 is for IE6/7
        if ($.browser.msie && $.browser.version < 8) {
            width = 601;
        }
        ok(treemap.children(".wijmo-wijtreemap-container").outerWidth() === width, "width is reset to 600px");
        firstItem = treemap.find(".wijmo-wijtreemap-item.wijstate-default:first");
        ok(firstItem.parent().outerWidth() < 600, "current view is not a.a");
        treemap.remove();
    });

}(jQuery));