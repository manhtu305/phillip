﻿/*
* wijtreemap_events.js
*/
(function ($) {
    module("wijtreemap: events");

    /*
        itemPainting?: Function;
        itemPainted?: Function;
        painting?: Function;
        painted?: Function;
        drillingDown?: Function;
        drilledDown?: Function;
        rollingUp?: Function;
        rolledUp?: Function;
    */
    test("Paint Events", function () {
        var count = {
            itemPainting: 0,
            itemPainted: 0,
            painting: 0,
            painted: 0
        }, treemap=createTreeMap({
            data: [{
                label: "a",
                value: 2,
                items: [{
                    label: "a.a",
                    value: 1
                },{
                    label: "a.b",
                    value: 1
                }]
            },{
                label: "b",
                value: 3,
                items: [{
                    label: "b.a",
                    value: 1
                },{
                    label: "b.b",
                    value: 1
                },{
                    label: "b.c",
                    value: 1
                }]
            },{
                label: "c",
                value: 3
            }],
            itemPainting: function() {
                count.itemPainting++;
            },
            itemPainted: function() {
                count.itemPainted++;

            },
            painted: function() {
                count.painted++;
            },
            painting: function() {
                count.painting++;
            }
        });
        
        ok(count.painting === 1, "painting event fires correctly.");
        ok(count.painted === 1, "painted event fires correctly.");
        ok(count.itemPainting === 6, "itemPainting event fires correctly.");
        ok(count.itemPainted === 6, "itemPainted event fires correctly.");
        treemap.remove();
    });

    test("itemPainting event to customize items",function() {
        var treemap = createTreeMap({
            data: [{
                label: "a",
                value: 1
            },{
                label: "b",
                value: 1
            },{
                label: "c",
                value: 1
            }],
            itemPainting: function(e, data) {
                data.htmlTemplate = "<span class='customizedClass'></span>";
            }
        });
        ok(treemap.find(".customizedClass").length === 3, "all 3 items are customized");
        treemap.remove();
    });

    test("DrillDown and RollUp Events",function() {
        var count = {
                drillingDown: 0,
                drilledDown: 0,
                rollingUp: 0,
                rolledUp: 0
            },
            treemap = createTreeMap({
                drillingDown: function(e, data) {
                    count.drillingDown++;
                    if(data.clickItemData.label === "b.a") {
                        console.log('a');
                        return false;
                    }
                },
                drilledDown: function() {
                    count.drilledDown++;

                },
                rollingUp: function(e, data) {
                    count.rollingUp++;
                    if(data.currentData.label === "c") {
                        return false;
                    }

                },
                rolledUp: function() {
                    count.rolledUp++;

                },
                itemPainting: function(e,data) {
                    if (data.label === "a.a") {
                        data.htmlTemplate = "<span class='c1'></span>";
                    } else if (data.label === "b.a") {
                        data.htmlTemplate = "<span class='c2'></span>";
                    } else if (data.label === "c.a") {
                        data.htmlTemplate = "<span class='c3'></span>";
                    }
                }
            }),
            firstItem = treemap.find(".c1").parent(),
            firstTitle = firstItem.parent().children(".wijmo-wijtreemap-title"),
            secondItem = treemap.find(".c2").parent(),
            secondTitle = secondItem.parent().children(".wijmo-wijtreemap-title"),
            thirdItem = treemap.find(".c3").parent(),
            thirdTitle = thirdItem.parent().children(".wijmo-wijtreemap-title");

        firstItem.simulate("click");
        ok(count.drillingDown === 1, "drillingDown event fires correctly.");
        ok(count.drilledDown === 1, "drilledDown event fires correctly.");
        firstTitle.simulate("click");
        ok(count.rollingUp === 1, "rollingUp event fires correctly.");
        ok(count.rolledUp === 1, "rolledUp event fires correctly.");
        firstItem.simulate("click");
        ok(count.drillingDown === 2, "drillingDown event fires correctly.");
        ok(count.drilledDown === 2, "drilledDown event fires correctly.");
        firstItem.trigger('contextmenu');
        ok(count.rollingUp === 2, "rollingUp event fires correctly using contextmenu.");
        ok(count.rolledUp === 2, "rolledUp event fires correctly using contextmenu.");

        treemap.wijtreemap("refresh");
        secondItem = treemap.find(".c2").parent();
        secondItem.simulate("click");
        ok(count.drillingDown === 3, "drillingDown event fires correctly.");
        ok(count.drilledDown === 2, "drilledDown event doesn't fire when returning false in drillingDown event.");
        
        treemap.wijtreemap("refresh");
        thirdItem = treemap.find(".c3").parent();
        thirdTitle = thirdItem.parent().children(".wijmo-wijtreemap-title");
        thirdItem.simulate("click");
        thirdItem.trigger('contextmenu');
        thirdItem.trigger('contextmenu');
        ok(count.rollingUp === 4, "rollingUp event fires correctly.");
        ok(count.rolledUp === 3, "rolledUp event doesn't fire when returing false in rollingUp event.");

        treemap.remove();
    });
})(jQuery)