﻿/*globals commonWidgetTests*/
/*
* wijrating_defaults.js
*/
"use strict";
commonWidgetTests('wijtreemap', {
    defaults: {
        initSelector: ":jqmData(role='wijtreemap')",
        wijCSS: $.extend(true, {}, $.wijmo.wijCSS, {
            wijtreemap: "",
            tmcontainer: "",
            tmitem: "",
            tmitemContainer: "",
            tmtitle: "",
            tmlabel: "",
            tmbuttons: "",
            tmbackButton: "",
            tmhomeButton: ""
        }),
        wijMobileCSS: {
        },
        width: null,
        height: null,
        type: "squarified",
        //showDepth: number,
        data: null,
        valueBinding: null,
        labelBinding: null,
        colorBinding: null,
        //animation: any,
        showLabel: true,
        labelFormatter: null,
        showTooltip: false,
        tooltipOptions: null,
        showTitle: true,
        titleHeight: 20,
        titleFormatter: null,
        minColor: null,
        minColorValue: null,
        midColor: null,
        midColorValue: null,
        maxColor: null,
        maxColorValue: null,
        showBackButtons: false,
        //Events
        itemPainting: null,
        itemPainted: null,
        painting: null,
        painted: null,
        drillingDown: null,
        drilledDown: null,
        rollingUp: null,
        rolledUp: null//,
        //click: null
    }
});