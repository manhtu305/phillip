﻿/*globals jQuery, test, ok, module*/
/*
* wijtreemap_options.js
*/
"use strict";

function convertRGB(color) {
    //remove space
    return color.replace(/\s/g, "")
}

(function ($) {
    module("wijtreemap: options");

    test("disabled", function() {
        var count = 0,
            treemap = createTreeMap({
                disabled: true,
                drillingDown: function() {
                    count++;
                }
            }),
            firstItem = treemap.find(".wijmo-wijtreemap-item.wijstate-default:first");
        ok(treemap.hasClass("ui-state-disabled"), "disabled class is added");
        firstItem.simulate("mouseover");
        ok(!firstItem.hasClass("ui-state-hover"), "hover class is not added");
        firstItem.simulate("click");
        ok(count === 0, "drill down is not fired.");

        treemap.wijtreemap("option", "disabled", false);
        ok(!treemap.hasClass("ui-state-disabled"), "disabled class is removed");
        firstItem.simulate("mouseover");
        ok(firstItem.hasClass("ui-state-hover"), "hover class is added");
        firstItem.simulate("mouseout");
        ok(!firstItem.hasClass("ui-state-hover"), "hover class is removed");
        firstItem.simulate("click");
        ok(count === 1, "drill down is fired.");
        
        firstItem.trigger("contextmenu");
        treemap.wijtreemap("option", "disabled", true);
        ok(treemap.hasClass("ui-state-disabled"), "disabled class is added");
        firstItem.simulate("mouseover");
        ok(!firstItem.hasClass("ui-state-hover"), "hover class is not added");
        firstItem.simulate("click");
        ok(count === 1, "drill down is not fired.");

        treemap.remove();
    });

    test("width and height", function() {
        var treemap = createTreeMap(), 
            container = treemap.children(".wijmo-wijtreemap-container"),
            width = 800, height = 600;
        
        if ($.browser.msie && $.browser.version < 8) {
            width = 801;
            height = 601;
        }
        ok(container.outerWidth() === width && container.outerHeight() === height, 
            "container get width and height from element's width/height correctly");
        treemap.remove();

        width = 600;
        height = 400;
        treemap = createTreeMap({
            width: width,
            height: height
        });
        if ($.browser.msie && $.browser.version < 8) {
            width++;
            height++;
        }
        container = treemap.children(".wijmo-wijtreemap-container");
        ok(container.outerWidth() === width && container.outerHeight() === height, 
            "container get width and height from width/height option correctly");
        width = 700;
        height = 500;
        treemap.wijtreemap("option", "width", width);
        treemap.wijtreemap("option", "height", height);
        if ($.browser.msie && $.browser.version < 8) {
            width++;
            height++;
        }
        ok(container.outerWidth() === width && container.outerHeight() === height, 
            "container get width and height correctly after reset width/height option");
        treemap.remove();
    });

    test("showLabel",function() {
        var treemap = createTreeMap(),
            firstLabel = treemap.find(".wijmo-wijtreemap-label:first");
        ok(firstLabel.is(":visible"), "label is visible");
        treemap.wijtreemap("option", "showLabel", false);
        ok(firstLabel.is(":hidden"), "label is hidden");
        treemap.wijtreemap("option", "showLabel", true);
        ok(firstLabel.is(":visible"), "label is visible");
        treemap.remove();

        treemap = createTreeMap({
            showLabel: false
        });
        firstLabel = treemap.find(".wijmo-wijtreemap-label:first");
        ok(firstLabel.is(":hidden"), "label is hidden");
        treemap.remove();
    });

    test("labelFormatter", function() {
        var treemap = createTreeMap({
                labelFormatter: function() {
                    return this.label + "labelFormatter";
                }
            }),
            firstLabel = treemap.find(".wijmo-wijtreemap-label:first");
        ok(firstLabel.html() === "a.alabelFormatter", "labelFormatter works");
        treemap.remove();
    });

    test("showTitle",function() {
        var treemap = createTreeMap(),
            firstTitle = treemap.find(".wijmo-wijtreemap-title:first");
        ok(firstTitle.is(":visible"), "title is visible");
        treemap.wijtreemap("option", "showTitle", false);
        ok(firstTitle.is(":hidden"), "title is hidden");
        treemap.wijtreemap("option", "showTitle", true);
        ok(firstTitle.is(":visible"), "title is visible");
        treemap.remove();

        treemap = createTreeMap({
            showTitle: false
        });
        firstTitle = treemap.find(".wijmo-wijtreemap-title:first");
        ok(firstTitle.is(":hidden"), "title is hidden");
        treemap.remove();
    });

    test("titleFormatter", function() {
        var treemap = createTreeMap({
                titleFormatter: function() {
                    return this.label + "titleFormatter";
                }
            }),
            firstTitle = treemap.find(".wijmo-wijtreemap-title:first");
        ok(firstTitle.html() === "atitleFormatter", "titleFormatter works");
        treemap.remove();
    });

    test("titleHeight", function() {
        var treemap = createTreeMap(),
            firstTitle = treemap.find(".wijmo-wijtreemap-title:first"),
            height = 20;
        if ($.browser.msie && $.browser.version < 8) {
            height = 21;
        }
        ok(firstTitle.outerHeight() === height, "default title height is 20");

        height = 40;
        treemap.wijtreemap("option", "titleHeight", height);
        if ($.browser.msie && $.browser.version < 8) {
            height++;
        }
        firstTitle = treemap.find(".wijmo-wijtreemap-title:first");
        ok(firstTitle.outerHeight() === height, "default title height is 40");
        treemap.remove();
        
        height = 40;
        treemap = createTreeMap({
            titleHeight: height
        });
        if ($.browser.msie && $.browser.version < 8) {
            height++;
        }
        firstTitle = treemap.find(".wijmo-wijtreemap-title:first");
        ok(firstTitle.outerHeight() === height, "title height is 40");
        treemap.remove();
    });

    test("control color rendering", function() {
        var data = [{
                label: "a",
                value: 1
            },{
                label: "b",
                value: 2
            },{
                label: "c",
                value: 3
            }],
            treemap = createTreeMap({
                data: data,
                maxColor: "rgb(254, 0, 0)",
                midColor: "rgb(128, 0, 0)",
                minColor: "rgb(0, 0, 0)",
                maxColorValue: 3,
                midColorValue: 2,
                minColorValue: 1
            }), 
            children = treemap.find(".wijmo-wijtreemap-item.wijstate-default");
        ok(convertRGB($(children[0]).css("background-color")) === "rgb(0,0,0)", "first item's bg color is min color." + $(children[0]).css("background-color"));
        ok(convertRGB($(children[1]).css("background-color")) === "rgb(128,0,0)", "second item's bg color is mid color." + $(children[1]).css("background-color"));
        ok(convertRGB($(children[2]).css("background-color")) === "rgb(254,0,0)", "third item's bg color is max color." + $(children[2]).css("background-color"));
        treemap.remove();

        treemap = createTreeMap({
            data: data,
            maxColor: "rgb(254, 0, 0)",
            minColor: "rgb(0, 0, 0)",
            maxColorValue: 3,
            minColorValue: 1
        }); 
        children = treemap.find(".wijmo-wijtreemap-item.wijstate-default");
        ok(convertRGB($(children[1]).css("background-color")) === "rgb(127,0,0)", "mid color is correct when it is not set." + $(children[1]).css("background-color"));
        treemap.remove();
    });

    test("color priority", function() {
        var treemap = createTreeMap({
            data: [{
                label: "a",
                value: 7,
                maxColor: "rgb(0, 254, 255)",
                minColor: "rgb(0, 0, 255)",
                maxColorValue: 3,
                minColorValue: 1,
                items: [{
                    label: "a.a",
                    value: 1
                },{
                    label: "a.b",
                    value: 2
                },{
                    label: "a.c",
                    value: 3
                },{
                    label: "a.d",
                    value: 1,
                    color: "rgb(0, 0, 127)"
                }]
            }, {
                label: "b",
                value: 7
            }, {
                label: "c",
                value: 4
            }, {
                label: "d",
                value: 1
            }],
            maxColor: "rgb(254, 0, 0)",
            minColor: "rgb(0, 0, 0)",
            maxColorValue: 7,
            minColorValue: 1
        }),
        children = treemap.find(".wijmo-wijtreemap-item.wijstate-default");
        
        ok(convertRGB($(children[0]).css("background-color")) === "rgb(0,0,255)", "a.a's bg color is item's min color.");
        ok(convertRGB($(children[1]).css("background-color")) === "rgb(0,127,255)", "a.b's bg color is item's mid color.");
        ok(convertRGB($(children[2]).css("background-color")) === "rgb(0,254,255)", "a.c's bg color is item's max color.");
        ok(convertRGB($(children[3]).css("background-color")) === "rgb(0,0,127)", "a.d's bg color is color key.");
        ok(convertRGB($(children[4]).css("background-color")) === "rgb(254,0,0)", "b's bg color is control's max color.");
        ok(convertRGB($(children[5]).css("background-color")) === "rgb(127,0,0)", "c's bg color is control's mid color.");
        ok(convertRGB($(children[6]).css("background-color")) === "rgb(0,0,0)", "d's bg color is control's min color.");
        treemap.remove();
    });

    test("binding",  function() {
        var treemap = createTreeMap({
                data: [{
                    label1: "a",
                    label: "aa",
                    color1: "#ff0000",
                    color: "#00ff00",
                    value1: 1,
                    value: 2
                },{
                    label1: "b",
                    label: "bb",
                    color1: "#00ff00",
                    color: "#ff0000",
                    value1: 1,
                    value: 4
                }],
                colorBinding: "color1",
                valueBinding: "value1",
                labelBinding: "label1"
            }), 
            children = treemap.find(".wijmo-wijtreemap-item.wijstate-default"),
            firstItem = children.first(),
            secondItem = children.last();
        ok(convertRGB(firstItem.css("background-color")) === "rgb(255,0,0)", "colorBinding works");
        ok(firstItem.find(".wijmo-wijtreemap-label").html() === "a", "labelBinding works");
        ok(firstItem.outerWidth() === secondItem.outerWidth(), "valueBinding works");
        treemap.remove();
    });

    test("data", function() {
        var treemap = createTreeMap({
                data: [{
                    label: "a",
                    value: 1
                },{
                    label: "b",
                    value: 2
                },{
                    label: "c",
                    value: 3
                }]
            }), 
            children = treemap.find(".wijmo-wijtreemap-item.wijstate-default");
        ok(children.length === 3, "3 items are created");
        ok($(children[0]).find(".wijmo-wijtreemap-label").html() === "a", "item a is created");
        ok($(children[1]).find(".wijmo-wijtreemap-label").html() === "b", "item b is created");
        ok($(children[2]).find(".wijmo-wijtreemap-label").html() === "c", "item c is created");

        treemap.wijtreemap("option", "data", [{
                label: "d",
                value: 1
            },{
                label: "e",
                value: 2
            }]);
        children = treemap.find(".wijmo-wijtreemap-item.wijstate-default");
        ok(children.length === 2, "2 items are created");
        ok($(children[0]).find(".wijmo-wijtreemap-label").html() === "d", "item d is created");
        ok($(children[1]).find(".wijmo-wijtreemap-label").html() === "e", "item e is created");
        treemap.remove();
    });

    test("type", function() {
        //squarified is hard to test, so only test "horizontal" and "vertical"
        var treemap = createTreeMap(),
            firstContainer = treemap.find(".wijmo-wijtreemap-item.wijmo-wijtreemap-itemcontainer:first"),
            items = firstContainer.children(".wijmo-wijtreemap-item");

        treemap.wijtreemap("option", "type", "horizontal");
        ok($(items[0]).css("left") === $(items[1]).css("left"), "a.a and a.b's left are same");
        ok($(items[0]).css("left") === $(items[2]).css("left"), "a.a and a.c's left are same");
        ok($(items[0]).css("left") === $(items[3]).css("left"), "a.a and a.d's left are same");

        treemap.wijtreemap("option", "type", "vertical");
        ok($(items[0]).css("top") === $(items[1]).css("top"), "a.a and a.b's top are same");
        ok($(items[0]).css("top") === $(items[2]).css("top"), "a.a and a.c's top are same");
        ok($(items[0]).css("top") === $(items[3]).css("top"), "a.a and a.d's top are same");
        treemap.remove();

        treemap = createTreeMap({
            type: "horizontal"
        });
        firstContainer = treemap.find(".wijmo-wijtreemap-item.wijmo-wijtreemap-itemcontainer:first");
        items = firstContainer.children(".wijmo-wijtreemap-item");
        ok($(items[0]).css("left") === $(items[1]).css("left"), "a.a and a.b's left are same");
        ok($(items[0]).css("left") === $(items[2]).css("left"), "a.a and a.c's left are same");
        ok($(items[0]).css("left") === $(items[3]).css("left"), "a.a and a.d's left are same");
        treemap.remove();

        treemap = createTreeMap({
            type: "vertical"
        });
        firstContainer = treemap.find(".wijmo-wijtreemap-item.wijmo-wijtreemap-itemcontainer:first");
        items = firstContainer.children(".wijmo-wijtreemap-item");
        ok($(items[0]).css("top") === $(items[1]).css("top"), "a.a and a.b's top are same");
        ok($(items[0]).css("top") === $(items[2]).css("top"), "a.a and a.c's top are same");
        ok($(items[0]).css("top") === $(items[3]).css("top"), "a.a and a.d's top are same");
        treemap.remove();
    });

    /*
        //showDepth: number,
        //animation: any,
        //showTooltip: boolean,
        //tooltipOptions: any,
    */

}(jQuery));