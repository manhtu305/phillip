﻿/*
* wijvideo_options.js
*/

(function ($) {
    module("wijvideo: options");

    test("fullScreenButtonVisible: true", function () {
        var video = createVideo({ fullScreenButtonVisible: true }),
            wijvideoFullScreen = video.parent().find(".wijmo-wijvideo-fullscreen");

        ok(wijvideoFullScreen.css("display") !== "none", "fullscreen button is visible.");
        video.wijvideo("option", "fullScreenButtonVisible", false);
        ok(wijvideoFullScreen.css("display") === "none", "fullscreen button is hidden.");
        video.remove();
    });

    test("fullScreenButtonVisible: false", function () {
        var video = createVideo({ fullScreenButtonVisible: false }),
            wijvideoFullScreen = video.parent().find(".wijmo-wijvideo-fullscreen");

        ok(wijvideoFullScreen.css("display") === "none", "fullscreen button is hidden.");
        video.wijvideo("option", "fullScreenButtonVisible", true);
        ok(wijvideoFullScreen.css("display") !== "none", "fullscreen button is visible.");
        video.remove();
    });

    test("showControlsOnHover: true", function () {
        var video = createVideo({ showControlsOnHover: true }),
            wijvideoControl = video.parent().find(".wijmo-wijvideo-controls"),
            wijvideoContainer = video.parent();

        ok(wijvideoControl.css("display") === "none", "control is hidden by default.");

        video.mouseenter();
        ok(wijvideoControl.css("display") !== "none", "control is visible when hover.");
        ok(wijvideoContainer.height() === video.outerHeight(), "container height equals video height.");
        video.remove();
    });

    test("showControlsOnHover: false", function () {
        var video = createVideo({ showControlsOnHover: false }),
            wijvideoControl = video.parent().find(".wijmo-wijvideo-controls"),
            wijvideoContainer = video.parent(),
            interval;

        stop();
        interval = setInterval(function () {
            if (video.prop("readyState")) {
                clearInterval(interval);

                ok(wijvideoControl.css("display") !== "none", "control is visible by default.");
                ok(wijvideoContainer.height() === video.outerHeight() + wijvideoControl.height(), "container height equals video height plus controls height.");
                start();
                video.remove();
            }
        }, 200);
    });

    test("localization", function () {
        var video = createVideo({
            showControlsOnHover: false,
            localization: {
                volumeToolTip: "VOL",
                fullScreenToolTip: "FSCR"
            }
        }),
            tooltip,
            wijvideoFullScreen = video.parent().find(".wijmo-wijvideo-fullscreen>span"),
            wijvideoVolume = video.parent().find(".wijmo-wijvideo-volume"),
            interval;

        // We cannot control tooltip showing delay in wijvideo.

        stop();
        interval = setInterval(function () {
            if (video.prop("readyState")) {
                clearInterval(interval);

                wijvideoFullScreen.mouseover();
                setTimeout(function () {
                    tooltip = $(".wijmo-wijtooltip-container");
                    ok(tooltip.text() === "FSCR", "fullscreen tooltip localized.");

                    wijvideoVolume.mouseover();
                    setTimeout(function () {
                        ok(tooltip.text() === "VOL", "volume tooltip localized.");
                        video.remove();
                        start();                        
                    }, 200);
                }, 200);
            }
        }, 200);
    });

    test("disabled option", function () {
        var interval, container = $("<div/>").appendTo("body"),
            query = (Math.random() * 1000).toFixed(0),
        video = $('<video controls="controls" id="vid1" width="720" height="486">'
        + '<source src="http://cdn.wijmo.com/movies/wijmo.theora.ogv?load=' + query + '" type="video/ogg; codecs=\"theora, vorbis\"">'
        + '<source src="http://cdn.wijmo.com/movies/wijmo.mp4video.mp4?load=' + query + '" type="video/mp4; codecs=\"avc1.42E01E, mp4a.40.2\""></video>').appendTo(container);

        video.wijvideo({ disabled: true, showControlsOnHover: false });
        stop();
        interval2 = setInterval(function () {
            if (video.prop("readyState")) {
                clearInterval(interval2);                
                ok($(".ui-state-disabled").eq(2).outerHeight() === $(".wijmo-wijvideo", container).outerHeight(), "disabled div element's height is equal container's height");
                video.remove();
                start();               
            }
        }, 200);
    });
})(jQuery);
