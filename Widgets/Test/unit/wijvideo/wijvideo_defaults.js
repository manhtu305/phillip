﻿/*
* wijvideo_defaults.js
*/

var wijvideo_defaults = {
    initSelector: ":jqmData(role='wijvideo')",
    wijCSS: $.extend(true, {
        iconVolumeOn: "ui-icon-volume-on",
        iconVolumeOff: "ui-icon-volume-off",
        wijvideo: "",
        wijvideoWrapper: "",
        wijvideoControls: "",
        wijvideoPlay: "",
        wijvideoIndex: "",
        wijvideoIndexSlider: "",
        wijvideoTimer: "",
        wijvideoVolume: "",
        wijvideoVolumeContainer: "",
        wijvideoVolumeSlider: "",
        wijvideoFullScreen: "",
        wijvideoContainerFullScreen: ""
    }, $.wijmo.wijCSS),
    fullScreenButtonVisible: true,
    showControlsOnHover: true,
    localization: null
};

commonWidgetTests('wijvideo', {
    defaults: wijvideo_defaults
});
