﻿/*
* wijvideo_tickets.js
*/

(function ($) {
    module("wijvideo: tickets");

    test("javascript exception thrown when the video loaded too quickly", function () {
        var video = createVideo();
        ok(1, "No javascript exception thrown");
        video.remove();
    });
})(jQuery);
