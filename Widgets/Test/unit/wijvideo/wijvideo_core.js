﻿/*
* wijvideo_core.js
*/

function createVideo(options) {
    var video = createVideoElement(options);
    video.wijvideo(options);
    return video;
}

function createVideoElement(options) {
    var query = (Math.random() * 1000).toFixed(0),
        video = $('<video controls="controls" id="vid1" width="720" height="486">'
        + '<source src="http://cdn.wijmo.com/movies/wijmo.theora.ogv?load=' + query + '" type="video/ogg; codecs=\"theora, vorbis\"">'
        + '<source src="http://cdn.wijmo.com/movies/wijmo.mp4video.mp4?load=' + query + '" type="video/mp4; codecs=\"avc1.42E01E, mp4a.40.2\""></video>').appendTo("body");

    return video;
}

function hasException(func) {
    try {
        func();
        return false;
    } catch (ex) {
        return true;
    }
}

(function ($) {
    module("wijvideo: core");

    test("create and destroy", function () {
        var video = createVideo(), parent;

        parent = video.parent();
        ok(parent.hasClasses("wijmo-wijvideo ui-widget-content ui-widget"), "container created.");
        ok(video.siblings().hasClass("wijmo-wijvideo-wrapper"), "controls created.");
        ok(video.data("wijmo-wijvideo"), "instance created.");
        ok(video.wijvideo("widget"), "widget created.");

        video.wijvideo("destroy");
        parent = video.parent();
        ok(parent.hasNoClasses("wijmo-wijvideo ui-widget-content ui-widget"), "container destroyed.");
        ok(!video.siblings().hasClass("wijmo-wijvideo-wrapper"), "controls destroyed.");
        ok(!video.data("wijmo-wijvideo"), "instance destroyed.");
        ok(hasException(function () { video.wijvideo("widget"); }), "widget destroyed.");

        video.remove();
    });
})(jQuery);
