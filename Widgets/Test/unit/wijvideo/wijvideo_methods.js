﻿/*
* wijvideo_methods.js
*/

(function ($) {
    module("wijvideo: methods");

    test("getWidth & setWidth", function () {
        var video = createVideo();
        ok(video.wijvideo("getWidth") === video.outerWidth(), "getWidth() works fine.");
        video.wijvideo("setWidth", 1920);
        ok(video.outerWidth() === 1920, "setWidth() works fine.");
        video.remove();
    });

    test("getHeight & setHeight", function () {
        var video = createVideo();
        ok(video.wijvideo("getHeight") === video.outerHeight(), "getHeight() works fine.");
        video.wijvideo("setHeight", 1080);
        ok(video.outerHeight() === 1080, "setHeight() works fine.");
        video.remove();
    });
})(jQuery);
