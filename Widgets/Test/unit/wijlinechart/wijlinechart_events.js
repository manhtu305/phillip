﻿/*globals start, stop, test, ok, module, jQuery, 
simpleOptions, createSimpleLinechart, createLinechart*/
/*
* wijlinechart_events.js
*/
"use strict";
(function ($) {
	var linechart,
		newSeriesList = [{
			label: "1800",
			legendEntry: true,
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [100, 31, 635, 203] }
		}, {
			label: "1900",
			legendEntry: true,
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [133, 156, 947, 408] }
		}, {
			label: "2008",
			legendEntry: true,
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [973, 914, 4054, 732] }
		}];

	module("wijlinechart: events");

	test("mousedown", function () {
		linechart = createSimpleLinechart();
		linechart.wijlinechart({
			mouseDown: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 0, 'Press the mouse down on the first line.');
					linechart.remove();
				}
			}
		});
		stop();
		$(linechart.wijlinechart("getLinePath", 0).node).trigger('mousedown');
	});

	test("mouseup", function () {
		linechart = createSimpleLinechart();
		linechart.wijlinechart({
			mouseUp: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 0, 'Release the mouse on the first line.');
					linechart.remove();
				}
			}
		});
		stop();
		$(linechart.wijlinechart("getLinePath", 0).node).trigger('mouseup');
	});

	test("mouseover", function () {
		linechart = createSimpleLinechart();
		linechart.wijlinechart({
			mouseOver: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 0, 'Hover the first line.');
					linechart.remove();
				}
			}
		});
		stop();
		$(linechart.wijlinechart("getLinePath", 0).node).trigger('mouseover');
	});

	test("mouseout", function () {
		linechart = createSimpleLinechart();
		linechart.wijlinechart({
			mouseOut: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 0, 'Move the mouse out of the first line.');
					linechart.remove();
				}
			}
		});
		stop();
		$(linechart.wijlinechart("getLinePath", 0).node).trigger('mouseout');
	});

	test("mousemove", function () {
		linechart = createSimpleLinechart();
		linechart.wijlinechart({
			mouseMove: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 0, 'Move the mouse on the first line.');
					linechart.remove();
				}
			}
		});
		stop();
		$(linechart.wijlinechart("getLinePath", 0).node).trigger('mousemove');
	});

	test("click", function () {
		linechart = createSimpleLinechart();
		linechart.wijlinechart({
			click: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 0, 'Click on the first line.');
					linechart.remove();
				}
			}
		});
		stop();
		$(linechart.wijlinechart("getLinePath", 0).node).trigger('click');
	});

	test("beforeserieschange", function () {
		linechart = createSimpleLinechart();
		var oldSeriesList = linechart.wijlinechart("option", "seriesList");
		linechart.wijlinechart({
			beforeSeriesChange: function (e, seriesObj) {
				if (seriesObj !== null) {
					start();
					var seriesList = linechart.wijlinechart("option", "seriesList");
					ok(seriesObj.oldSeriesList === oldSeriesList, 
							'get the correct old seriesList by seriesObj.oldSeriesList.');
					ok(seriesObj.newSeriesList === newSeriesList, 
							'get the correct new seriesList by seriesObj.newSeriesList.');
					ok(seriesList === oldSeriesList, 
							"currently seriesList hasn't been changed.");
					linechart.remove();
				}
			}
		});
		stop();
		linechart.wijlinechart("option", "seriesList", newSeriesList);
	});

	test("serieschanged", function () {
		linechart = createSimpleLinechart();
		linechart.wijlinechart({
			seriesChanged: function (e, seriesObj) {
				if (seriesObj !== null) {
					start();
					var seriesList = linechart.wijlinechart("option", "seriesList");
					ok(seriesList === newSeriesList, 
						"currently seriesList has been changed.");
					linechart.remove();
				}
			}
		});
		stop();
		linechart.wijlinechart("option", "seriesList", newSeriesList);
	});

	test("beforepaint, paint", function () {
		var options = $.extend({
				beforePaint: function (e) {
					ok(true, "The beforepaint event is invoked.");
					return false;
				},
				painted: function (e) {
					ok(false, "The painted event of line chart1 is invoked.");
				}
			}, true, simpleOptions),
			line, options2, linechart2;
		linechart = createLinechart(options);

		try {
			line = linechart.wijlinechart("getLinePath", 0);
			if (line.attrs.stroke) {
				ok(false,
					"The line chart1 shouldn't be painted because of the cancellation.");
			}
		}
		catch (ex) {
			ok(true, "The line chart1 isn't painted because of the cancellation.");
		}
		linechart.remove();

		options2 = $.extend({
			beforePaint: function (e) {
				if (e !== null) {
					ok(true, "The beforepaint event is invoked.");
				}
			},
			painted: function (e) {
				ok(true, "The painted event of line chart2 is invoked.");
			}
		}, true, simpleOptions);
		linechart2 = createLinechart(options2);
		line = linechart2.wijlinechart("getLinePath", 0);
		ok(line !== null, 'The line chart2 is painted.');
		linechart2.remove();
	});

}(jQuery));