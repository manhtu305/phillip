﻿/*globals test, ok, module, jQuery, createSimpleLinechart*/
/*
* wijlinechart_methods.js
*/
"use strict";
(function ($) {

	module("wijlinechart: methods");

	test("getLinePath", function () {
		var linechart = createSimpleLinechart(),
			line = linechart.wijlinechart('getLinePath', 0);
		ok(line.attrs.stroke === '#00cc00', 'getLinePath:gets the first line');
		linechart.remove();
	});

	test("getLineMarkers", function () {
		var linechart = createSimpleLinechart(),
			linemarkers = linechart.wijlinechart('getLineMarkers', 0);

		ok(linemarkers[0].type === 'circle',
			'getLineMarkers:gets the first marker of first line');
		linechart.remove();
	});

	test("getCanvas", function () {
		var linechart = createSimpleLinechart(),
			canvas = linechart.wijlinechart('getCanvas');

		ok(canvas.circle !== null, 'getCanvas:gets the canvas for the bar chart');
		linechart.remove();
	});

}(jQuery));