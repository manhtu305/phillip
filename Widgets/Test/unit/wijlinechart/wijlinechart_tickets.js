﻿/*
* wijlinechart_tickets.js
*/
(function ($) {

	module("wijlinechart: tickets");

	//#27533
	test("27533:JS error is observed on adding Serieslist when animation.direction is vertical", function () {
		var seriesList1 = [{
			label: "1800",
			legendEntry: true,
			markers: { visible: true },
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [107, 31, 635, 203] }
		}];
		var seriesList2 = [{
			label: "1800",
			legendEntry: true,
			markers: { visible: true },
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [107, 31, 635, 203] }
		}, {
			label: "1900",
			legendEntry: true,
			markers: { visible: true },
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [133, 156, 947, 408] }
		}]
		var linechart = createLinechart({
			seriesList: seriesList1,
			animation: { direction: 'vertical' }
		});
		linechart.wijlinechart({
			seriesList: seriesList2,
			painted: function () {
				ok(true, "27533 fixed.");
				linechart.remove();
			}
		});
	});
	//test("empty datasource, javascript exception thrown", function () {
	//    var linechart = createLinechart({
	//        "type": "area",
	//        "animation": {
	//            "enabled": true,
	//            "direction": "vertical"
	//        },
	//        "axis": {
	//            "x": {
	//                "textVisible": false,
	//                "text": "Time",
	//                "textStyle": {
	//                    "fill": "#204557"
	//                },
	//                "autoMin": true,
	//                "autoMax": true,
	//                "gridMajor": {
	//                    "visible": false
	//                },
	//                "unitMajor": 48,
	//                "tickMajor": {
	//                    "position": "none"
	//                },
	//                "tickMinor": {
	//                    "position": "none"
	//                },
	//                "labels": {
	//                    "style": {
	//                        "fill": "#929292"
	//                    }
	//                },
	//                "gridMinor": {
	//                    "visible": false
	//                },
	//                "unitMinor": 24
	//            },
	//            "y": {
	//                "textVisible": true,
	//                "text": "Alerts in State",
	//                "textStyle": {
	//                    "fill": "#204557"
	//                },
	//                "autoMin": false,
	//                "autoMax": false,
	//                "gridMajor": {
	//                    "visible": true
	//                },
	//                "unitMajor": 100,
	//                "tickMajor": {
	//                    "position": "cross"
	//                },
	//                "tickMinor": {
	//                    "position": "none"
	//                },
	//                "labels": {
	//                    "style": {
	//                        "fill": "#929292"
	//                    }
	//                },
	//                "min": 0,
	//                "gridMinor": {
	//                    "visible": false
	//                },
	//                "max": 900,
	//                "unitMinor": 50
	//            }
	//        },
	//        "chartLabelStyle": {
	//            "stroke": "#19475E",
	//            "font-size": "10px",
	//            "font-family": "sans-serif"
	//        },
	//        "data": {
	//            "x": {
	//                "bind": "time"
	//            }
	//        },
	//        "header": {
	//            "title": "Model Config - Unknown Wijmo Barchart",
	//            "textStyle": {
	//                "text-anchor": "end",
	//                "fill": "#2f9cc5",
	//                "width": 500
	//            },
	//            "text": "Sever Error and Unavailable Seconds"
	//        },
	//        "height": "280",
	//        "hint": {
	//            "showCallout": false,
	//            "calloutFilled": false,
	//            "style": {
	//                "stroke": "#19475E",
	//                "fill": "#f9f3b3",
	//                "fill-opacity": 0.9,
	//                "stroke-width": 0.5
	//            },
	//            "offsetY": 10,
	//            "offsetX": 10,
	//            "contentStyle": {
	//                "font-size": "10px",
	//                "fill": "#19475E",
	//                "font-family": "sans-serif"
	//            },
	//            "class": "tnd-custom",
	//            "compass": "south"
	//        },
	//        "legend": false,
	//        "seriesHoverStyles": [{
	//            "stroke": "#f7a024",
	//            "opacity": 1,
	//            "stroke-width": 2
	//        }, {
	//            "stroke": "#ebebeb",
	//            "opacity": 1,
	//            "stroke-width": 2
	//        }, {
	//            "stroke": "#f7e624",
	//            "opacity": 1,
	//            "stroke-width": 2
	//        }, {
	//            "stroke": "#C7485D",
	//            "opacity": 1,
	//            "stroke-width": 2
	//        }],
	//        "seriesStyles": [{
	//            "stroke": "#f7a024",
	//            "opacity": 1,
	//            "stroke-width": 2
	//        }, {
	//            "stroke": "#ebebeb",
	//            "opacity": 1,
	//            "stroke-width": 2
	//        }, {
	//            "stroke": "#f7e624",
	//            "opacity": 1,
	//            "stroke-width": 2
	//        }, {
	//            "stroke": "#C7485D",
	//            "opacity": 1,
	//            "stroke-width": 2
	//        }, {
	//            "stroke": "#c7465a",
	//            "opacity": 0.8
	//        }],
	//        "stacked": true,
	//        "showChartLabels": false,
	//        seriesList: [{
	//            legendEntry: true,
	//            dataSource: [],
	//            label: "ses",
	//            data: { x: { bind: "time" }, y: { bind: "value" } }
	//        }, {
	//            legendEntry: true,
	//            dataSource: [],
	//            label: "uas",
	//            data: { x: { bind: "time" }, y: { bind: "value" } }
	//        }]
	//    });

	//    ok(true, "issue fixed.");
	//});

	test("When min/max of axis option is set, autoMin/autoMax should be set to false", function () {
		var linechart = createLinechart({
			axis: {
				x: {
					max: 10,
					min: 0
				},
				y: {
					min: 0,
					max: 30
				}
			},
			seriesList: [{ data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20] } }]
		});

		var axis = linechart.wijlinechart("option", "axis");
		ok(axis.x.autoMin === false && axis.x.min === 0, "autoMin of x axis is set to false when axis.x.min is set.");
		ok(axis.x.autoMax === false && axis.x.max === 10, "autoMax of x axis is set to false when axis.x.max is set.");
		ok(axis.y.autoMin === false && axis.y.min === 0, "autoMin of y axis is set to false when axis.y.min is set.");
		ok(axis.y.autoMax === false && axis.y.max === 30, "autoMax of y axis is set to false when axis.y.max is set.");
		linechart.remove();

		linechart = createLinechart({
			seriesList: [{ data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20] } }]
		});
		axis = linechart.wijlinechart("option", "axis");
		ok(axis.x.autoMin === true, "autoMin of x axis is true.");
		ok(axis.x.autoMax === true, "autoMax of x axis is true.");
		ok(axis.y.autoMin === true, "autoMin of y axis is true.");
		ok(axis.y.autoMax === true, "autoMax of y axis is true.");
		linechart.wijlinechart("option", "axis", {
			x: {
				max: 10,
				min: 0
			},
			y: {
				min: 0,
				max: 30
			}
		});
		axis = linechart.wijlinechart("option", "axis");
		ok(axis.x.autoMin === false && axis.x.min === 0, "autoMin of x axis is set to false when axis.x.min is set.");
		ok(axis.x.autoMax === false && axis.x.max === 10, "autoMax of x axis is set to false when axis.x.max is set.");
		ok(axis.y.autoMin === false && axis.y.min === 0, "autoMin of y axis is set to false when axis.y.min is set.");
		ok(axis.y.autoMax === false && axis.y.max === 30, "autoMax of y axis is set to false when axis.y.max is set.");
		linechart.remove();
	});

	test("mouse over the line, show tooltip, and then redraw the chart, mouse move inside of the plot area, javascript exception is thrown.", function () {
		var linechart = createLinechart({
			seriesList: [{ data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20] } }]
		}),
			offset = linechart.offset();
		setTimeout(function () {
			// Simulate mouse over and mouse move to show the tooltip
			$(".wijchart-canvas-object:first").simulate("mouseover");
			linechart.simulate("mousemove", { clientX: offset.left + 100, clientY: offset.top + 100 });
			// Check the tooltip is shown
			ok(linechart.data("wijmo-wijlinechart").tooltip.container, "the tooltip is shown");
			linechart.wijlinechart("redraw");
			ok(!linechart.data("wijmo-wijlinechart").tooltip.container, "the tooltip is hide");
			setTimeout(function () {
				// After redraw, simulate mouse move event to check whether javascript exception is thrown.
				linechart.simulate("mousemove", { clientX: offset.left + 50, clientY: offset.top + 50 });
				ok(true, "No javascript exception is thrown.");
				linechart.remove();
				start();
			}, 300);
		}, 300);
		stop();
	});

	// In VML, I can't find a way to check the path is visible. bellow codes use jQuery to check the opacity and visible.
	if (Raphael.svg) {
		test("#44367", function () {
			// markers are invisible, series is invisible
			var linechart = createLinechart({
				seriesList: [{ data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20] }, label: "test", visible: false }],
				animation: { enabled: false }, seriesTransition: { enabled: false }
			}), legendText, lineData;
			legendText = linechart.find(".wijchart-legend-text");			

			ok(legendText.attr("opacity") == 0.3, "The legend's opacity is 0.3");
			ok(linechart.find(".wijlinechart.wijchart-canvas-object").css("display") === "none", "the line is invisible");
			legendText.simulate("click");
			ok(linechart.find(".wijlinechart.wijchart-canvas-object").css("display") !== "none", "the line is visible");
			

			// markers are invisible, series is visible.
			linechart.wijlinechart("option", "seriesList", [{ data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20] }, label: "test", visible: true }]);
			ok(linechart.find(".wijlinechart.wijchart-canvas-object").css("display") !== "none", "the line is visible");
			legendText = linechart.find(".wijchart-legend-text");
			legendText.simulate("click");
			ok(legendText.attr("opacity") == 0.3, "The legend's opacity is 0.3");
			ok(linechart.find(".wijlinechart.wijchart-canvas-object").css("display") === "none", "the line is invisible");

			// markers are visible, series is invisible
			linechart.wijlinechart("option", "seriesList", [{ data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20] }, label: "test", visible: false, markers: { visible: true } }]);
			ok(linechart.find(".wijchart-canvas-marker").css("display") !== "none", "the markers is visible");
			legendText = linechart.find(".wijchart-legend-text");
			legendText.simulate("click");
			ok(linechart.find(".wijchart-canvas-marker").css("display") === "none", "the markers is invisible");
			ok(legendText.attr("opacity") == 0.3, "The legend's opacity is 0.3");

			// markers are visible, series is visible
			linechart.wijlinechart("option", "seriesList", [{ data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20] }, label: "test", visible: true, markers: { visible: true } }]);
			ok(linechart.find(".wijchart-canvas-marker").css("display") !== "none", "the markers is visible");
			ok(linechart.find("path.wijlinechart.wijchart-canvas-object").css("display") !== "none", "the line is visible");
			legendText = linechart.find(".wijchart-legend-text");
			legendText.simulate("click");
			ok(linechart.find(".wijchart-canvas-marker").css("display") === "none", "the markers is invisible");
			ok(linechart.find("path.wijlinechart.wijchart-canvas-object").css("display") === "none", "the line is invisible");
			ok(legendText.attr("opacity") == 0.3, "The legend's opacity is 0.3");
			linechart.remove();
		});
	}

	test("#71822", function () {
	    var linechart71822 = createLinechart({
	        seriesList: [{ data: { x: [3, 4, 5, 6, 7], y: [20, 22, 19, 24, 25] }, label: "test" }],
	        animation: { enabled: false }, seriesTransition: { enabled: false },
	        axis: {
	            y: {
	                text: "Y Axis",
	                origin: 0,
	                visible: true,
	            },
	            x: {
	                text: "X Axis",
	                origin: 0,
	                visible: true,
	            }
	        }
	    }), axis;
	    axis = linechart71822.wijlinechart("option", "axis");

	    ok(axis.x.min === 0, "Get axis min correctly !");
	    ok(axis.y.min === 0, "Get axis min correctly !");

	    linechart71822.remove();
	});

	test("#71878", function () {
	    var seriesEles, trendlinePath,
            linechart = createLinechart({
                seriesList: [{
                    label: "Trendline",
                    legendEntry: true,
                    isTrendline: true,
                    data: { x: [3, 4, 5, 6, 7], y: [20, 22, 19, 24, 25] }
                }],
                mouseOver: function (e, chartObj) {
                    if (chartObj !== null) {
                        ok(chartObj.type === "trendLine", 'Hover on the trendline.');
                    }
                },
                seriesStyles: [{
                    stroke: "#FFA41C", "stroke-width": 1, "stroke-opacity": 0.7
                }],
                seriesHoverStyles: [{
                    "stroke-width": 5
                }]
            });

	    seriesEles = linechart.data("wijmo-wijlinechart").chartElement.data("fields").seriesEles;
	    ok(seriesEles.length === 1 && seriesEles[0].isTrendline, "The trendline is drawn.");
	    trendlinePath = seriesEles[0].path;
	    ok(trendlinePath.attr("stroke-width") === 1, "SeriesStyle is applied on trendline");
	    $(trendlinePath.node).trigger('mouseover');
	    ok(trendlinePath.attr("stroke-width") === 5, "HoverSeriesStyle is applied on trendline");
	    $(trendlinePath.node).trigger('mouseout');
	    ok(trendlinePath.attr("stroke-width") === 1, "HoverSeriesStyle is removed and seriesStyle is applied on trendline");
	    linechart.remove();
	});

	test("#71914", function () {
	    var linechart = createLinechart({
	        animation: { enabled: false }, 
	        seriesTransition: { enabled: false },
	        seriesList: [{
	            data: {
	                x: [1, 2, 3, 4, 5], y: [20, 22, 19, 24, 25]
	            },
	            markers: { visible: true, type: "circle" }
	        }, {
	            isTrendline: true,
	            data: { x: [1, 2, 3, 4, 5], y: [20, 22, 19, 24, 25] },
	            markers: { visible: true, type: "circle" }
	        }]
	    });
	    ok(linechart.find(".wijchart-legend.chart-legend-dot").length === 1, "Trendline has no marker.");
	    linechart.remove();
	});

	test("#71943", function () {
	    var linechart = createLinechart({
	        animation: { enabled: false },
	        seriesTransition: { enabled: false },
	        seriesList: [{
	            label: "Trendline for Orange",
	            isTrendline: true,
	            legendEntry: true,
	            fitType: "logarithmic",
	            order: 4,
	            data: { x: [1, 2, 3, 4, 5], y: [20, 22, 19, 24, 25] }
	        }],
	        axis: {
	            x: {
	                text: "X Axis",
	                min: 0
	            }
	        }
	    }),
        seriesEles = linechart.data("wijmo-wijlinechart").chartElement.data("fields").seriesEles;
	    ok(seriesEles[0].path.straight.indexOf("NaN") === -1, "Trendline should be drawn.");
	    linechart.remove();
	});

	test("#90648", function () {
	    var linechart = createLinechart({
	        animation: { enabled: false },
	        seriesTransition: { enabled: false },
	        seriesList: [{
	            label: "Series1",
	            legendEntry: true,
	            data: { x: [1], y: [20] }
	        }, {
	            label: "Series2",
	            legendEntry: true,
	            data: { x: [1], y: [10] }
	        }]
	    });

	    linechart.wijlinechart("option", "seriesList", [{
	        label: "Series3",
	        legendEntry: true,
	        data: { x: [1], y: [30] }
	    }]);

	    ok(linechart.find(".wijchart-legend-text").length === 1, " Extra legend has been removed ! ");

	    linechart.remove();
	});

	test("#94167", function () {
	    var linechart = createLinechart({
	        seriesList: [{
	            label: "Orange",
	            legendEntry: true,
	            data: { x: [1, 2, 3, 4, 5], y: [20, 22, 19, 24, 25] },
	        }],
	        axis: {
	            x: {
	                text: "X Axis",
	                min: 0
	            }
	        }
	    });

	    ok(linechart.find(".wijchart-axis-text >tspan").text() === "X Axis", "Axis title css has been added !")
	    linechart.remove();
	});

	test("#100444", function () {
	    var linechart = createLinechart({
	        seriesList: [{
	            label: "Orange",
	            legendEntry: true,              
	            data: { x: [1, 2, 3, 4, 5], y: [20, 22, 19, 24, 25] },
	            markers: { visible: true, type: "circle" }
	        }, {
	            label: "Trendline for Orange",
	            isTrendline: true,
	            legendEntry: true,
	            fitType: "XXX",
	            order: 4,
	            data: { x: [1, 2, 3, 4, 5], y: [20, 22, 19, 24, 25] },
	            markers: { visible: true, type: "circle" }
	        }]
	    });

	    ok(linechart.find(".wijchart-legend-text").length === 1, "The invalid trend line will not get a legend icon !");

	    linechart.remove();
	});
}(jQuery));