﻿/*globals test, ok, module, document, jQuery*/
/*
 * wijlinechart_options.js
 */
"use strict";
(function ($) {

	module("wijlinechart:options");

	test("area chart", function () {
		var linechart = createSimpleLinechart({type: "area"});
		ok(true, "create simple area chart successfully.");
		linechart.remove();
	});
	
	test("stacked line chart", function () {
		var linechart = createSimpleLinechart({stacked: true});
		ok(true, "create simple stacked chart successfully.");
		linechart.remove();
	});
	
	test("stacked area chart", function () {
		var linechart = createSimpleLinechart({stacked: true, type: "area"});
		ok(true, "create simple stacked chart successfully.");
		linechart.remove();
	});

	test("seriesListEmpty", function() {
		var linechart = createSimpleLinechart(),
			seriesList = [];
		seriesList.push({ data: { x: ['x1', 'x2', 'x3', 'x4'], y: [5, 10, 12, 20]} });
		seriesList.push({ });
		linechart.wijlinechart('option', 'seriesList', seriesList);
		ok(true, 'ok.');
		linechart.remove();
	});
	
	test("various type of seriesList", function () {
		var linechart = createSimpleLinechart(),
			seriesList;
		seriesList = [{ data: { x: ['x1', 'x2', 'x3'], y: [1, 2, 3]} }];
		linechart.wijlinechart('option', 'seriesList', seriesList);
		ok(true, 'ok when x data is string and y data is number.');
		seriesList = [{ data: { x: ['x1', 'x2', 'x3'], 
			y: [new Date('10/27/2010'), new Date('10/28/2010'), new Date('10/29/2010')]} }];
		linechart.wijlinechart('option', 'seriesList', seriesList);
		ok(true, 'ok when x data is string and y data is datetime.');
		seriesList = [{ data: { x: [1, 2, 3], y: [1, 2, 3]} }];
		linechart.wijlinechart('option', 'seriesList', seriesList);
		ok(true, 'ok when x data is number and y data is number.');
		seriesList = [{ data: { x: [1, 2, 3], 
			y: [new Date('10/27/2010'), new Date('10/28/2010'), new Date('10/29/2010')]} }];
		linechart.wijlinechart('option', 'seriesList', seriesList);
		ok(true, 'ok when x data is number and y data is datetime.');
		seriesList = [{ data: { x: [new Date('10/27/2010'), new Date('10/28/2010'), new Date('10/29/2010')], 
			y: [1, 2, 3]} }];
		linechart.wijlinechart('option', 'seriesList', seriesList);
		ok(true, 'ok when x data is datetime and y data is number.');
		seriesList = [{ data: { x: [new Date('10/27/2010'), new Date('10/28/2010'), new Date('10/29/2010')], 
			y: [new Date('10/27/2010'), new Date('10/28/2010'), new Date('10/29/2010')]} }];
		linechart.wijlinechart('option', 'seriesList', seriesList);
		ok(true, 'ok when x data is datetime and y data is datetime.');
		linechart.remove();
	});
	
	test("display option of seriesList", function () {
		//show,hide,exclude,excludeHole
		var linechart = createSimpleLinechart(),
			seriesList;
		seriesList = [{ display: "hide", data: { x: ['x1', 'x2', 'x3'], y: [null, 2, 3]} }];
		linechart.wijlinechart('option', 'seriesList', seriesList);
		ok(true, 'ok when display is hide and y axis has null value.');
		seriesList = [{ display: "exclude", data: { x: ['x1', 'x2', 'x3'], y: [1, null, 3]} }];
		linechart.wijlinechart('option', 'seriesList', seriesList);
		ok(true, 'ok when display is exclude and y axis has null value.');
		seriesList = [{ display: "excludeHole", data: { x: ['x1', 'x2', 'x3'], y: [1, 2, null]} }];
		linechart.wijlinechart('option', 'seriesList', seriesList);
		ok(true, 'ok when display is excludeHole and y axis has null value.');
		linechart.wijlinechart('option', 'hole', 3);
		seriesList = [{ display: "hide", data: { x: ['x1', 'x2', 'x3'], y: [1, 2, 3]} }];
		linechart.wijlinechart('option', 'seriesList', seriesList);
		ok(true, 'ok when display is hide and y axis has hole value.');
		seriesList = [{ display: "exclude", data: { x: ['x1', 'x2', 'x3'], y: [1, 2, 3]} }];
		linechart.wijlinechart('option', 'seriesList', seriesList);
		ok(true, 'ok when display is exclude and y axis has hole value.');
		seriesList = [{ display: "excludeHole", data: { x: ['x1', 'x2', 'x3'], y: [1, 2, 3]} }];
		linechart.wijlinechart('option', 'seriesList', seriesList);
		ok(true, 'ok when display is excludeHole and y axis has hole value.');
		linechart.remove();
	});
	
	/*
	test("compass of hint", function () {
		var linechart = createSimpleLinechart({
			animation: { enabled: false },
			seriesTransition: { enabled: false },
			hint:  {enable: true, content: "a"}
		});
		$(linechart.wijlinechart("getLinePath", 0).tracker.node).trigger('mouseover');
	});
	*/

	test("axis", function () {
		var lineChart = $('<div style = "width:400px;height:200px"></div>')
				.appendTo(document.body),
			seriesList = [],
			axis, max, min, unitMajor, unitMinor, formatString, wijlinechart,
			time = [new Date(2009, 3, 5), new Date(2009, 4, 12), new Date(2009, 5, 20),
				new Date(2009, 8, 30)];

		lineChart.wijlinechart();
		seriesList.push({ data: { x: ['x1', 'x2', 'x3', 'x4'], y: [5, 10, 12, 20]} });
		lineChart.wijlinechart('option', 'seriesList', seriesList);

		axis = lineChart.wijlinechart('option', 'axis');
		max = axis.y.max;
		min = axis.y.min;
		unitMajor = axis.y.unitMajor;
		unitMinor = axis.y.unitMinor;

		if ($.browser.msie && $.browser.version < 9) {
			ok(max === 20 && min === 4, 'max and min for seriesList1 are ok');
			ok(unitMajor === 2 && unitMinor === 1,
				'unitMajor and unitMinor for seriesList1 are ok');
		}
		else {
			ok(max === 20 && min === 0, 'max and min for seriesList1 are ok');
			ok(unitMajor === 5 && unitMinor === 2.5,
				'unitMajor and unitMinor for seriesList1 are ok');
		}

		seriesList = [];
		seriesList.push({
			data: {
				x: ['x1', 'x2', 'x3', 'x4'],
				y: [50, 1008, 2232, 3800]
			}
		});
		lineChart.wijlinechart('option', 'seriesList', seriesList);
		axis = lineChart.wijlinechart('option', 'axis');
		max = axis.y.max;
		min = axis.y.min;
		unitMajor = axis.y.unitMajor;
		unitMinor = axis.y.unitMinor;
		ok(max === 4000 && min === 0, 'max and min for seriesList2 are ok');
		ok(unitMajor === 500 && unitMinor === 250,
			'unitMajor and unitMinor for seriesList2 are ok');

		seriesList = [];
		seriesList.push({
			data: {
				x: ['x1', 'x2', 'x3', 'x4'],
				y: [2150, 4008, 6232, 9500]
			}
		});
		lineChart.wijlinechart('option', 'seriesList', seriesList);
		axis = lineChart.wijlinechart('option', 'axis');
		max = axis.y.max;
		min = axis.y.min;
		unitMajor = axis.y.unitMajor;
		unitMinor = axis.y.unitMinor;
		ok(max === 10000 && min === 2000, 'max and min for seriesList3 are ok');
		ok(unitMajor === 1000 && unitMinor === 500,
			'unitMajor and unitMinor for seriesList3 are ok');

		seriesList = [];
		seriesList.push({ data: { x: ['1', '2', '3', '4'], y: [50, 1008, 2232, 3800]} });
		lineChart.wijlinechart('option', 'seriesList', seriesList);
		axis = lineChart.wijlinechart('option', 'axis');
		max = axis.x.max;
		min = axis.x.min;
		unitMajor = axis.x.unitMajor;
		unitMinor = axis.x.unitMinor;
		ok(max === 3 && min === 0, 'max and min for seriesList4 are ok');
		ok(unitMajor === 1 && unitMinor === 0.5,
			'unitMajor and unitMinor for seriesList4 are ok');

		seriesList = [];
		seriesList.push({ data: { x: time, y: [2150, 4008, 6232, 9500]} });
		lineChart.wijlinechart('option', 'seriesList', seriesList);
		wijlinechart = lineChart.data("wijmo-wijlinechart");
		max = wijlinechart.options.axis.x.max;
		max = $.fromOADate(max);
		min = wijlinechart.options.axis.x.min;
		min = $.fromOADate(min);
		formatString = wijlinechart.options.axis.x.annoFormatString;
		if(formatString.length === 0) {
			formatString = wijlinechart.axisInfo.x.annoFormatString;
		}
		//max = $.format(max, formatString);
		max = Globalize.format(max, formatString);
		//min = $.format(min, formatString);
		min = Globalize.format(min, formatString);
		ok(max === 'Oct' && min === 'Apr', 'max and min for seriesList5 are ok');
		lineChart.remove();
	});

	test("chartLabelFormatter && chartLabelFormatString", function () {
	    var lineChart = createLinechart({
	        seriesList: [{
	            data: { x: [1, 2], y: [7, 8] }
	        }],
	        showChartLabels: true,
	        animation: {
	            enabled: false
	        }
	    }), chartLabel0;

	    chartLabel0 = lineChart.data("wijmo-wijlinechart").chartElement.data("fields").seriesEles[0].dcl[0].attr("text");
	    ok(chartLabel0 === "7", "the first chart element label is '7'");

	    lineChart.wijlinechart("option", "chartLabelFormatString", "n2");
	    chartLabel0 = lineChart.data("wijmo-wijlinechart").chartElement.data("fields").seriesEles[0].dcl[0].attr("text");
	    ok(chartLabel0 === "7.00", "the first chart element label is updated according to chart label format string.");

	    lineChart.wijlinechart("option", "chartLabelFormatter", function () {
	        return "mycustomzied:" + this.value;
	    });
	    chartLabel0 = lineChart.data("wijmo-wijlinechart").chartElement.data("fields").seriesEles[0].dcl[0].attr("text");
	    ok(chartLabel0 === "mycustomzied:7", "the first chart element label is updated according to chartLabelFormatter.");

	    lineChart.remove();
	});
}(jQuery));
