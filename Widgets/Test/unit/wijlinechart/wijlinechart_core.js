/*globals test, ok, jQuery, module, document, $*/
/*
* wijlinechart_core.js create & destroy
*/
"use strict";
function createLinechart(o) {
	return $('<div id="linechart" style = "width:400px;height:200px"></div>')
		.appendTo(document.body).wijlinechart(o);
}

var simpleOptions = {
	seriesList: [{
		label: "1800",
		legendEntry: true,
		markers: {visible: true},
		data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [107, 31, 635, 203] }
	}, {
		label: "1900",
		legendEntry: true,
		markers: { visible: true },
		data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [133, 156, 947, 408] }
	}, {
		label: "2008",
		legendEntry: true,
		markers: { visible: true },
		data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [973, 914, 4054, 732] }
	}],
	hint: {
		enable: false
	}
};

function createSimpleLinechart(o) {
	return createLinechart($.extend(true, simpleOptions, o));
}

(function ($) {

	module("wijlinechart: core");

	test("create and destroy", function () {
		var linechart = createLinechart();

		ok(linechart.hasClass('ui-widget wijmo-wijlinechart'),
			'create:element class created.');
		
		linechart.wijlinechart('destroy');
		ok(!linechart.hasClass('ui-widget wijmo-wijlinechart'),
			'destroy:element class destroy.');
		
		linechart.remove();
	});

	test("table as datasource", function () {
		var table = $('<table><thead><tr><td></td><th>x1</th><th>x2</th></tr>' +
			'</thead><tbody><tr><th scope="row">seriesName1</th><td>11</td>' +
			'<td>12</td></tr><tr><th scope="row">seriesName2</th><td>21</td>' +
			'<td>22</td></tr></tbody></table>'),
			seriesList = null;
		table.wijlinechart({
			width: 400,
			height: 200
		});
		seriesList = table.wijlinechart('option', 'seriesList');
		ok(seriesList !== null, 'created');
		ok(seriesList[0].data.y[0] === 11 && seriesList[0].data.y[1] === 12,
			'line1 value correct.');
		ok(seriesList[1].data.y[0] === 21 && seriesList[1].data.y[1] === 22,
			'line2 value correct.');
		table.wijlinechart("destroy");
	});

}(jQuery));

