/*globals commonWidgetTests*/
/*
* wijsparkline_defaults.js
*/
"use strict";
commonWidgetTests('wijsparkline', {
	defaults: {
		initSelector: ":jqmData(role='wijsparkline')",
		wijCSS: $.extend(true, {
			sparkline: "wijmo-wijsparkline",
			canvasWrapper: "wijmo-sparkline-canvas-wrapper"
		}, $.wijmo.wijCSS),
		type: "line",
		seriesList: [],
		width: null,
		height: null,
		data: null,
		bind: null,
		seriesStyles: [],
		animation: {
			enabled: true,
			duration: 2000,
			easing: "easeInCubic"
		},
		valueAxis: false,
		origin: null,
		min: null,
		max: null,
		columnWidth: 10,
		tooltipFormat: null,
		tooltipContent: null,
		mouseMove: null,
		click: null
	}
});