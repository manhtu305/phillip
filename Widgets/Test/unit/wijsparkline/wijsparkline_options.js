/*globals test, ok, module, document, jQuery*/
/*
 * wijsparkline_options.js
 */
"use strict";
(function ($) {
	module("wijsparkline:options");

	test("area sparkline", function () {
		var sparkline = createSimpleSparkline({
			type: "area"
		});
		ok(true, "create area sparkline successfully.");
		sparkline.remove();
	});

	test("valueAxis for area sparkline", function () {
		var sparkline = createSimpleSparkline({
			valueAxis: true,
			type: "area"
		});
		ok(true, "valueAxis option works fine with area sparkline.");
		sparkline.remove();
	});

	test("column sparkline", function () {
		var sparkline = createSimpleSparkline({
			type: "column"
		});
		ok(true, "create area sparkline successfully.");
		sparkline.remove();
	});

	test("valueAxis for column sparkline", function () {
		var sparkline = createSimpleSparkline({
			valueAxis: true,
			type: "column"
		});
		ok(true, "valueAxis option works fine with column sparkline.");
		sparkline.remove();
	});

	test("composite sparkline", function () {
		var sparkline = createSimpleSparkline({
			valueAxis: true,
			seriesList: [{
				type: "column"
			}, {
				type: "line"
			}]
		});
		ok(true, "create composite sparkline successfully.");
		sparkline.remove();
	});

	test("tooltipContent option works for sparkline", function () {
		var data = [{ name: "Januray", score: 73 }, { name: "February", score: 95 }, { name: "March", score: 89 },
            { name: "April", score: 66 }, { name: "May", score: 50 }, { name: "June", score: 65 },
            { name: "July", score: 70 }, { name: "August", score: 43 }, { name: "September", score: 65 },
            { name: "October", score: 27 }, { name: "November", score: 77 }, { name: "December", score: 58 }],
			sparkline = createSparkline({
				data: data,
				seriesList: [{
					bind: "score"
				}],
				tooltipContent: function () {
					return this.name + ' ' + this.score;
				},
				mouseMove: function (e, args) {
					var tooltip = $(".wijmo-wijsparkline-tooltip");

					ok(tooltip.children("span").text() === "February 95", "The tooltipContent function works fine");

					sparkline.remove();
					start();
				}
			}),
			sparklineWrapper = sparkline.next(".wijmo-sparkline-canvas-wrapper");

		stop();
		sparklineWrapper.simulate("mousemove", { clientX: 20, clientY: 5 });
	});

	test("disabled", function () {
	    var sparkline = createSparkline({
	        data: [33, 11, 15, 26, 16, 27, 37, -13, 8, -8, -3, 17, 0, 22, -13, -29, 19, 8],
	        type: "area",
	        disabled: true
	    });
	    ok($(".wijmo-wijsparkline").hasClass("ui-state-disabled"), "The disabled class has added to sparkline.");
	    sparkline.remove();
	});

}(jQuery));