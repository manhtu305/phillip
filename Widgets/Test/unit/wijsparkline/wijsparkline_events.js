/*globals start, stop, test, ok, module, jQuery, 
simpleOptions, createSimpleSparkline, createSparkline*/
/*
* wijsparkline_events.js
*/
"use strict";
(function ($) {

	module("wijsparkline: events");

	test("mouseMove", function () {
		var sparkline = createSparkline({
			data: [2, 3, 2],
			seriesList: [{
				type: "area"
			}],
			mouseMove: function (e, args) {
				start();
				if (args && args.currentRegion && args.currentRegion.length > 0) {
					var currentRegion = args.currentRegion[0];
					ok(currentRegion.index === 0, "The mouse move inside the area of the first item");
				}
				sparkline.remove();
			}
		}),
		sparklineWrapper = sparkline.next(".wijmo-sparkline-canvas-wrapper");
		stop();
		sparklineWrapper.simulate("mousemove", { clientX: 10, clientY: 5 });
	});

	test("click", function () {
		var sparkline = createSparkline({
			data: [2, 3, 2],
			seriesList: [{
				type: "column"
			}],
			click: function (e, args) {
				start();
				if (args && args.currentRegion && args.currentRegion.length > 0) {
					var currentRegion = args.currentRegion[0];
					ok(currentRegion.index === 1, "The mouse click inside the area of the second item");
				}
				sparkline.remove();
			}
		}),
		sparklineWrapper = sparkline.next(".wijmo-sparkline-canvas-wrapper");
		stop();
		sparklineWrapper.simulate("click", { clientX: 55, clientY: 5 });
	});

}(jQuery));