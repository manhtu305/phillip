/*
* wijsparkline_tickets.js
*/
"use strict";
(function ($) {

    module("wijsparkline: tickets");

    test("#71842, re-create sparkline after destroying.", function () {
        var sparkline = createSparkline(),
            sparklineWrapper = sparkline.parent();

        ok(sparklineWrapper.hasClass('ui-widget wijmo-wijsparkline'),
            'create:element class created.');

        sparkline.wijsparkline('destroy');
        ok(!sparkline.hasClass('ui-widget wijmo-wijsparkline'),
            'destroy:element class destroy.');

        sparkline.wijsparkline();
        sparklineWrapper = sparkline.parent();
        ok(sparklineWrapper.hasClass('ui-widget wijmo-wijsparkline'),
            'create:element class re-created successfully after destroying.');

        sparkline.remove();
    });

    test("#74042", function () {
        var data = [{ name: "Januray", score: 73 }, { name: "February", score: 95 }, { name: "March", score: 89 },
            { name: "April", score: 66 }, { name: "May", score: 50 }, { name: "June", score: 65 },
            { name: "July", score: 70 }, { name: "August", score: 43 }, { name: "September", score: 65 },
            { name: "October", score: 27 }, { name: "November", score: 77 }, { name: "December", score: 58 }],
            sparkline = createSimpleSparkline({
                type: "column"
            }),
            sparklineWrapper;

        sparkline.wijsparkline({
            data: data,
            mouseMove: function (e, args) {
                var tooltip;
                ok(true, "The mouse move event is triggered without errors");
                tooltip = $(".wijmo-wijsparkline-tooltip");
                ok(tooltip.length === 0, "No tooltips shows with the empty sparkline");
                sparkline.remove();
                start();
            }
        });
        ok(true, "The sparkline won't throw errors, when set an invalid data.");

        sparklineWrapper = sparkline.next(".wijmo-sparkline-canvas-wrapper");
        stop();
        sparklineWrapper.simulate("mousemove", { clientX: 0, clientY: 0 });
    });

    test("#87992", function () {
        var data = [33, 11, 15, 26, 16, 27, 37, -13, -27, -8, -3, 17, 0, 22, -13, -29, 19, 8],
            $widget = createSparkline({
                type: "column",
                data: data
            }),
            $wrapper = $widget.next(".wijmo-sparkline-canvas-wrapper"),
            indicatorTrans = "";

        $widget.wijsparkline("option", "type", "area");
        $wrapper.simulate("mousemove", { clientX: 50, clientY: 15 });
        indicatorTrans = $wrapper.find("svg path:last").attr("transform");
        ok(indicatorTrans && indicatorTrans !== "matrix(1,0,0,1,0,0)", "indicator line position is correct.");
        $widget.remove();

        $widget = createSparkline({
            seriesList: [{
                type: "column",
                data: data
            }]
        });
        $wrapper = $widget.next(".wijmo-sparkline-canvas-wrapper");
        $widget.wijsparkline("option", "seriesList", [{
            type: "line",
            data: data
        }]);
        $wrapper.simulate("mousemove", { clientX: 50, clientY: 15 });
        indicatorTrans = $wrapper.find("svg path:last").attr("transform");
        ok(indicatorTrans && indicatorTrans !== "matrix(1,0,0,1,0,0)", "indicator line position is correct.");
        $widget.remove();
    });

    test("#92768", function () {
        var data = [33, 11, 15],
            $widget = $('<div id="sparkline"></div>')
                .appendTo(document.body).wijsparkline({data: data}),
            $wrapper = $widget.next(".wijmo-sparkline-canvas-wrapper");
        ok($wrapper.find("svg path").attr("d") !== "M0,0L0,0L0,0");
        $widget.remove();
    });

    test("#92768", function() {
        var $tab = $("<div id='tabs'>" +
            "<ul><li><a href='#tabs-1'>tab1</a></li><li><a href='#tabs-2'>tab2</a></li></ul>" +
            "<div id='tabs-1'></div><div id='tabs-2'><div id='sparkline'></div></div></div>").appendTo(document.body);
        $tab.wijtabs();
        var data = [10, 20, 30],
            secondTab,
            $widget = $('#sparkline').wijsparkline({ data: data }),
            $wrapper = $widget.next(".wijmo-sparkline-canvas-wrapper");
        ok($wrapper.find("svg path").length === 0);
        secondTab = $($("a", $tab)[1]);
        secondTab.simulate('click');
        ok($wrapper.find("svg path").attr("d") !== "M0,0L0,0L0,0");
        $widget.remove();
        $tab.remove();
    });
}(jQuery));