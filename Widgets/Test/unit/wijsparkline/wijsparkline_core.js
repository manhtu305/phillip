/*globals test, ok, jQuery, module, document, $*/
/*
* wijsparkline_core.js create & destroy
*/
"use strict";
function createSparkline(o) {
    return $('<div id="sparkline" style = "width:100px;height:30px"></div>')
        .appendTo(document.body).wijsparkline(o);
}

var simpleOptions = {
    data: [21, 11, 15, -7, 16, -12, 8],
    type: "line"
}

function createSimpleSparkline(o) {
    return createSparkline($.extend(true, {}, simpleOptions, o));
}

(function ($) {

    module("wijsparkline: core");

    test("create and destroy", function () {
        var sparkline = createSparkline(),
            sparklineWrapper = sparkline.parent();

        ok(sparklineWrapper.hasClass('ui-widget wijmo-wijsparkline'),
            'create:element class created.');

        sparkline.wijsparkline('destroy');
        ok(!sparkline.hasClass('ui-widget wijmo-wijsparkline'),
            'destroy:element class destroy.');

        sparkline.remove();
    });
}(jQuery));