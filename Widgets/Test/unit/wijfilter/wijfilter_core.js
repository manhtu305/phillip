(function($) {
	module("wijfilter: core");

	test("create and destroy", function() {
		var $div = $("<div></div>")
			.appendTo(document.body);

		$div.wijfilter({
			availableOperators: [ { name: "operator1" }, { name: "operator2" }, { name: "operator3" } ]
		});

		$div.wijfilter("destroy");

		$div.remove();
	});

})(jQuery);

