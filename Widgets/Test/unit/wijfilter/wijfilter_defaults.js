commonWidgetTests("wijfilter", {
	defaults: {
	        initSelector: ":jqmData(role='wijfilter')",
			data: undefined,
			dataKey: undefined,
			disabled: false,
			nullDisplayText: "<null>",
			title: "",
			enableSortButtons: false,
			availableOperators: undefined,
			showHeader: true,
			sortDirection: undefined,
			filterValue: [],
			filterOperator: [],
			close: null
	}
});
