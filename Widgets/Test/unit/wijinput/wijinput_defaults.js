var wijinputcoreOptionDefaults = {
    disabled: false,
    culture: '',
    invalidClass: 'ui-state-error',
    nullText: '',
    showNullText: false,
    hideEnter: false,
    disableUserInput: false,
    buttonAlign: 'right',
    showTrigger: false,
    showSpinner: false,
    comboItems: undefined,
    comboWidth: undefined,
    comboHeight: undefined,
    exitOnLastChar: false,
    exitOnLeftRightKey: 'none',
    imeMode: 'auto',
    spninnerAlign: 'vertical',
    spinAllowWrap: false,
    initializing: null,
    initialized: null,
    triggerMouseDown: null,
    triggerMouseUp: null,
    textChanged: null,
    invalidInput: null,
    showNullText: false,
    nullText: undefined,
    disableUserInput: false,
    buttonAlign: null,
    showTrigger: undefined,
    triggerMouseDown: null,
    triggerMouseUp: null
};
commonWidgetTests('wijinputdate', {
    defaults: $.extend({
    }, wijinputcoreOptionDefaults, {
        amDesignator: 'AM',
        displayFormat: 'D',
        hour12As0: false,
        midnightAs0: true,
        pmDesignator: 'PM',
        showTrigger: 'right',
        tabAction: 'field',
        activeField: 0,
        keyDelay: 800,
        autoNextField: true,
        smartInputMode: true,
        dateChanged: null,
        date: null,
        minDate: null,
        maxDate: null,
        calendar: null,
        dateFormat: 'd',
        startYear: 1950,
        calendar: null,
        popupPosition: {
            offset: '0 4'
        },
        valueBoundExceeded: null
    })
});
commonWidgetTests('wijinputmask', {
    defaults: $.extend({
    }, wijinputcoreOptionDefaults, {
        mask: '',
        text: null,
        promptChar: '_',
        resetOnPrompt: true,
        resetOnSpace: true,
        allowPromptAsInput: false,
        hidePromptOnLeave: false,
        skipLiterals: true,
        passwordChar: '',
        autoConvert: true,
        tabAction: 'control'
    })
});
commonWidgetTests('wijinputnumber', {
    defaults: $.extend({
    }, wijinputcoreOptionDefaults, {
        type: 'numeric',
        value: null,
        minValue: -1000000000,
        maxValue: 1000000000,
        increment: 1,
        showGroup: false,
        decimalPlaces: 2,
        valueChanged: null,
        valueBoundsExceeded: null,
        currencySymbol: '',
        negativePrefix: '-',
        negativeSuffix: '',
        negativeClass: ' ui-state-negative',
        positivePrefix: '',
        positiveSuffix: '',
        spinAllowWrap: false,
        tabAction: 'control'
    })
});
commonWidgetTests('wijinput', {
    defaults: $.extend({
    }, wijinputcoreOptionDefaults, {
        autoConvert: true,
        format: '',
        lengthAsByte: false,
        maxLength: 0,
        passwordChar: '',
        tabAction: 'control'
    })
});
