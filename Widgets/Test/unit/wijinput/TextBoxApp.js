﻿$(function () {
    $(document).ready(function() {
        $(".tabs").wijtabs().on("tabsactivate", function(event, ui) {
        });
    });
});
$(function () {
    var widgetType = "wijinputtext",
        selector = ".wijtextboxsample";

    function BuildPropertyPage() {
        var optionsTable = document.createElement("table");
        var tbody = document.createElement("tbody");
        document.body.appendChild(optionsTable);
        optionsTable.appendChild(tbody);
        BuildOptions(tbody, $(selector));

        var methodsTable = document.createElement("table");
        var tbody = document.createElement("tbody");
        document.body.appendChild(methodsTable);
        methodsTable.appendChild(tbody);
        BuildMethod(tbody, $(selector));

        var eventsTable = document.createElement("table");
        var tbody = document.createElement("tbody");
        document.body.appendChild(eventsTable);
        eventsTable.appendChild(tbody);
        BuildEvent(tbody, $(selector));
        optionsTable.id = "divOptions";
        methodsTable.id = "divMethod";
        eventsTable.id = "divEvent";
        optionsTable.className = "tabs";
        methodsTable.className = "tabs";
        eventsTable.className = "tabs";
        var tabContainer = document.createElement("div");
        tabContainer.className = "tabs";
        document.body.appendChild(tabContainer);
        tabContainer.appendChild(BuildTab());
        tabContainer.appendChild(optionsTable);
        tabContainer.appendChild(methodsTable);
        tabContainer.appendChild(eventsTable);
    }
    function BuildTab() {
        var ul = document.createElement("ul");
        var liOption = document.createElement("li");
        var liMethod = document.createElement("li");
        var liEvent = document.createElement("li");

        var optionA = document.createElement("a");
        var methodA = document.createElement("a");
        var eventA = document.createElement("a");
        ul.appendChild(liOption);
        ul.appendChild(liMethod);
        ul.appendChild(liEvent);
        liOption.appendChild(optionA);
        liMethod.appendChild(methodA);
        liEvent.appendChild(eventA);
        optionA.href = "#divOptions";
        optionA.appendChild(document.createTextNode("Options"));
        methodA.href = "#divMethod";
        methodA.appendChild(document.createTextNode("Method"));
        eventA.href = "#divEvent";
        eventA.appendChild(document.createTextNode("Event"));
        return ul;
    };
    
    var boolType = Boolean;
    var anyType = {};
    var stringType = {};
    var numberType = {};
    var callBackType = function() {
        alert("call back");
    };
    var rangeType = { _min: 1, _max: 100 };
    var cssLengthType = { _min: 1, _max: 100 };
    var imeModeType = { _type: "enum", _data: ['auto', 'active', 'inactive', 'disabled'] };
    var exitOnLeftRightKeyType = { _type: "enum", _data: ['none', 'left', 'right', 'both'] };

    var buttonAlignType = { _type: "enum", _data: ["left", "right"] };
    //var spinnerAlign = { _type: "enum", _data: ['bothSideDownLeft', 'bothSideUpLeft', 'horizontalDownLeft', 'horizontalUpLeft', 'vertical'] };
    var ellipsisModeType={ _type: "enum", _data: ["none", "ellipsisEnd", "ellipsisPath"] };

    var widgetOptions = {
        //create: callBackType,
        //culture: stringType,
        disabled: boolType,
        readonly: boolType,
        hideEnter: boolType,
        invalidClass: anyType,
        placeholder: anyType,
        showDropDownButton: boolType,
        dropDownButtonAlign : buttonAlignType,
       // showNullText: boolType,
        //value: numberType,
        //wijCSS: anyType,        
        ellipsis: ellipsisModeType,
        ellipsisString: stringType,
        showOverflowTip: boolType,
        highlightText: boolType,
        
        imeMode: imeModeType,
        blurOnLeftRightKey: exitOnLeftRightKeyType,
        blurOnLastChar: boolType,
        autoConvert: boolType,
        lengthAsByte: boolType,
        maxLength: numberType,
        format: stringType,
        
        maxLineCount: numberType,
        countWrappedLine: boolType,
        text: stringType,
        passwordChar: stringType
    };

    var funcType = function(needOutput, parametersObj) {
        this.needOutput = !!needOutput;
        this.parametersObj = parametersObj ? parametersObj : null;
    };
    var paramType = function(name, type) {
        this.name = name;
        this.type = type;
    };
    var widgetMethods = {
        destroy: new funcType(false, null),
        focus: new funcType(false, null),
        getPostValue: new funcType(true, null),
        getText: new funcType(true, null),
        getSelectedText: new funcType(true, null),
        //getValue: new funcType(true, null),
        isDestroyed: new funcType(true, null),
        isFocused: new funcType(true, null),
        //isValueNull: new funcType(true, null),
        selectText: new funcType(false, [new paramType('start', 'number'), new paramType('end', 'number')]),
        setText: new funcType(false, [new paramType('text', 'string')]),
        //setValue: new funcType(false, [new paramType('value', 'number')]),
        widget: new funcType(true, null)
    };

    var widgetEvents = {
        initialized: "e",
        initializing: "e",
        invalidInput: "e, data",
        textChanged: "e, data",
        dropDownButtonMouseDown: "e",
        dropDownButtonMouseUp: "e",
        
        dropDownOpen: "e",
        dropDownClose: "e",
        readingImeStringOutput: "e, data",
        keyExit:"e",
    };


    function BuildOptions(tbody, jqueryObj) {
        var tr = document.createElement("tr");
        tbody.appendChild(tr);
        var tdTitle = document.createElement("td");
        tr.appendChild(tdTitle);
        
        var counter = 0;
        for (var propertyName in widgetOptions) {
            if (counter % 2 === 0) {
                var tr = document.createElement("tr");
                tbody.appendChild(tr);
            }

            counter++;
            var td = document.createElement("td");
            td.className = "inputTh";
            //td.style.backgroundColor = "lightblue";
            //td.style.width = "160px";
            //td.style.textAlign = "center";
            tr.appendChild(td);

            if (propertyName == "create" || propertyName == "comboItems") {
                td.appendChild(document.createTextNode(propertyName));
            }
            else {
                var optionBtn = $("<button>");
                optionBtn.addClass("myButton");
                //optionBtn.css("width", "150px");
                optionBtn.append(document.createTextNode(propertyName));
                td.appendChild(optionBtn.get(0));
                addOptionBtnHandler(widgetOptions[propertyName], propertyName, jqueryObj, optionBtn);
            }
            
            var tdEdit = document.createElement("td");
            tdEdit.className = "inputTd";
            //tdEdit.style.backgroundColor = "lightgreen";
            //tdEdit.style.width = "160px";
            tr.appendChild(tdEdit);
            var editControl = createEditControls(widgetOptions[propertyName], propertyName, jqueryObj);
            if (editControl) {
                editControl.id = propertyName + "_Editor";
                tdEdit.appendChild(editControl);
            }
        }
        
        var trEmpty = document.createElement("tr");
        tbody.appendChild(trEmpty);
        var tdEmpty = document.createElement("td");
        trEmpty.appendChild(tdEmpty);
        tdEmpty.style.height = "20px";
        
        function createEditControls(type, propertyName, obj) {
            if (type === Boolean) {
                var input = $("<input>");
                input.attr("type", "checkbox");
                input.bind("change", function () {
                    obj[widgetType]("option", propertyName, this.checked);
                });
                return input.get(0);
            }
            
            if (type._type && type._type === 'enum') {
                var options = $("<select>");

                for (var i = 0; i < type._data.length; i++) {
                    var opt = document.createElement("option");
                    opt.value = type._data[i];
                    opt.appendChild(document.createTextNode(type._data[i]));
                    options.get(0).appendChild(opt);
                }
                options.bind("change", function () {
                    obj[widgetType]("option", propertyName, this[this.selectedIndex].text);
                });
                return options.get(0);
            }
            if (type === cssLengthType) {
                var input = $("<input>");
                input.attr("type", "range");
                input.attr("min", cssLengthType._min);
                input.attr("max", cssLengthType._max);
                input.bind("change", function () {
                    obj[widgetType]("option", propertyName, this.value + "px");
                });
                return input.get(0);
            }
            if (type === rangeType) {
                var input = $("<input>");
                input.attr("type", "range");
                input.attr("min", rangeType._min);
                input.attr("max", rangeType._max);
                input.bind("change", function () {
                    obj[widgetType]("option", propertyName, parseFloat(this.value));
                });
                return input.get(0);
            }
            if (type === stringType) {
                var input = $("<input>");
                input.attr("type", "text");
                input.bind("change", function () {
                    obj[widgetType]("option", propertyName, this.value);
                });
                return input.get(0);
            }
            if (type === anyType) {
                var input = $("<input>");
                input.attr("type", "text");
                input.bind("change", function () {
                    obj[widgetType]("option", propertyName, this.value);
                });
                return input.get(0);
            }
            if (type === numberType) {
                var input = $("<input>");
                input.attr("type", "text");
                input.bind("change", function () {
                    obj[widgetType]("option", propertyName, parseFloat(this.value));
                });
                return input.get(0);
            }
            //if (type === comboItemsData) {
            //    setTimeout(function() {
            //        obj[widgetType]("option", propertyName, comboItemsData);
            //    }, 0);
            //    return null;
            //}
            if (type === callBackType) {
                setTimeout(function () {
                    obj[widgetType]("option", propertyName, callBackType);
                }, 0);
                return null;

            }
            return null;
        };

        function addOptionBtnHandler(type, propertyName, obj, btn) {
            btn.bind('click', function () {
                getOptionValue(type, propertyName, obj);
            });
        }

        function getOptionValue(type, propertyName, obj) {
            var editor = $("#" + propertyName + "_Editor");
            var value = obj[widgetType]("option", propertyName);
            if (value === null) {
                value = "null";
            }

            if (value === "") {
                value = "empty";
            }

            if (value.offset) {
                value = value.offset;
            }

            if (value instanceof Date) {
                value = value.getFullYear() + "/" + (value.getMonth() + 1) + "/" + value.getDate() + " " + value.getHours() + ":" + value.getMinutes() + ":" + value.getSeconds();
            }
            if (editor.attr("type") == "checkbox") {
                editor.attr("checked", value === true);
                return;
            }

            editor.val(value);
        }
    };

    function BuildMethod(tbody, jqueryObj) {
        var tr = document.createElement("tr");
        tbody.appendChild(tr);
        var tdTitle = document.createElement("td");
        tr.appendChild(tdTitle);
        //tdTitle.appendChild(document.createTextNode("Method"));
        
        var counter = 0;
        for (var methodName in widgetMethods) {
            if (counter % 2 === 0) {
                var tr = document.createElement("tr");
                tbody.appendChild(tr);
            }

            counter++;
            var td = document.createElement("td");
            td.className = "inputTh";
            //td.style.backgroundColor = "lightblue";
            //td.style.width = "160px";
            //td.style.textAlign = "center";
            tr.appendChild(td);

            var callBtn = $("<button>");
            callBtn.addClass("myButton");
            //callBtn.css("width", "150px");
            callBtn.append(document.createTextNode(methodName));
            td.appendChild(callBtn.get(0));
            var tdEdit = document.createElement("td");
            tdEdit.className = "inputTd";
            //tdEdit.style.backgroundColor = "lightgreen";
            //tdEdit.style.width = "160px";
            tr.appendChild(tdEdit);
            var editControl = createEditControls(widgetMethods[methodName], methodName);
            if (editControl) {
                tdEdit.appendChild(editControl);
            }

            addCallBtnHandler(widgetMethods[methodName], methodName, jqueryObj, callBtn);
        }
        
        var trEmpty = document.createElement("tr");
        tbody.appendChild(trEmpty);
        var tdEmpty = document.createElement("td");
        trEmpty.appendChild(tdEmpty);
        tdEmpty.style.height = "20px";

        function createEditControls(type, methodName) {
            var contentDiv = document.createElement("div");
            if (type.parametersObj) {
                for (var index = 0; index < type.parametersObj.length; index++) {
                    var input = document.createElement('input');
                    input.id = methodName + "_param" + index;
                    contentDiv.appendChild(document.createTextNode("param" + index + '_' + type.parametersObj[index].name + ': '));
                    contentDiv.appendChild(input);
                }
            }
            if (type.needOutput) {
                var output = document.createElement("input");
                output.disabled = 'disabled';
                output.id = methodName + "_output";
                contentDiv.appendChild(document.createTextNode('output: '));
                contentDiv.appendChild(output);
            }
            return contentDiv;
        };
        
        function addCallBtnHandler(type, methodName, obj, btn) {
            btn.bind('click', function () {
                setTimeout(function () {
                    doMethod(type, methodName, obj);
                }, 100);
            });
        }
        
        function doMethod(type, methodName, obj) {
            var params = [];
            if (type.parametersObj) {
                for (var index = 0; index < type.parametersObj.length; index++) {
                    var paramData = document.getElementById(methodName + "_param" + index).value;
                    if (type.parametersObj[index].type === 'number') {
                        paramData = parseFloat(paramData);
                    }
                    params.push(paramData);
                }
            }

            var result = '';
            switch (params.length) {
                case 0:
                    result = obj[widgetType](methodName);
                    break;
                case 1:
                    result = obj[widgetType](methodName, params[0]);
                    break;
                case 2:
                    result = obj[widgetType](methodName, params[0], params[1]);
                    break;
                case 3:
                    result = obj[widgetType](methodName, params[0], params[1], params[2]);
                    break;
                case 4:
                    result = obj[widgetType](methodName, params[0], params[1], params[2], params[3]);
                    break;
                default:
            }

            if (type.needOutput) {
                var output = document.getElementById(methodName + "_output");
                output.value = result;
            }
        }
    };
    
    function BuildEvent(tbody, jqueryObj) {
        var tr = document.createElement("tr");
        tbody.appendChild(tr);
        var tdTitle = document.createElement("td");
        tr.appendChild(tdTitle);
        //tdTitle.appendChild(document.createTextNode("Event"));

        var counter = 0;
        for (var eventName in widgetEvents) {
            if (counter % 2 === 0) {
                var tr = document.createElement("tr");
                tbody.appendChild(tr);
            }

            counter++;
            var td = document.createElement("td");
            td.className = "inputTh";
            //td.style.backgroundColor = "lightblue";
            //td.style.width = "160px";
            //td.style.textAlign = "center";
            tr.appendChild(td);

            var addBtn = $("<button>");
            addBtn.addClass("myButton");
            //addBtn.css("width", "150px");
            addBtn.append(document.createTextNode(eventName));
            td.appendChild(addBtn.get(0));

            var tdEventArea = document.createElement("td");
            tdEventArea.className = "inputTd";
            //tdEventArea.style.backgroundColor = "lightgreen";
            //tdEventArea.style.width = "300px";
            //tdEventArea.style.height = "80px";
            tr.appendChild(tdEventArea);

            var eventArea = document.createElement("textarea");
            eventArea.className = "myTextArea";
            //eventArea.style.width = "300px";
            //eventArea.style.height = "80px";
            eventArea.id = eventName + "Content";
            var eventString = "function(" + widgetEvents[eventName] + ") \n{ \n    console.log(new Date() + '    " + eventName + "'); \n}";
            eventArea.appendChild(document.createTextNode(eventString));
            tdEventArea.appendChild(eventArea);

            addAddBtnHandler(widgetEvents[eventName], eventName, jqueryObj, addBtn);
        }


        function addAddBtnHandler(type, methodName, obj, btn) {
            btn.bind('click', function () {
                doEvent(type, methodName, obj);
            });
        }

        function doEvent(type, eventName, obj) {

            var eventFunction = document.getElementById(eventName + "Content").value;
            var fun = CheckFunction(eventFunction);
            obj[widgetType]("option", eventName, fun);
        }


        function Trim(value) {
            if (value == "") {
                return "";
            }

            var beginIndex = 0;
            var endIndex = 0;
            for (var i = 0; i < value.length; i++) {
                if (value.CharAt(i) != " " && value.CharAt(i) != "　") {
                    beginIndex = i;
                    break;
                }
            }

            for (var i = value.length - 1; i >= 0; i--) {
                if (value.CharAt(i) != " " && value.CharAt(i) != "　") {
                    endIndex = i + 1;
                    break;
                }
            }

            try {
                var s = value.Substring(beginIndex, endIndex);
                return s;
            }
            catch (e) {
                return value;
            }


        };

        function CheckFunction(fun) {
            if (fun === undefined || fun === null) {
                return null;
            }

            if (typeof fun == "string") {
                //fun = Trim(fun);
                if (fun.length == 0) {
                    return null;
                }
                try {
                    eval("fun =" + fun);
                }
                catch (e) {
                }
            }

            if (typeof fun == "function") {
                return fun;
            }
            else {
                throw "The value is not a valid function";
            }
        };

    };

    BuildPropertyPage();
});