﻿var wijmo;
(function (wijmo) {
    (function (tests) {
        var $ = jQuery;
        QUnit.module("wijinputtext: options");
        test("placeholder", function () {
            var $widget = tests.createInputText();
            ok($widget.wijinputtext("option", "placeholder") == null, "placeholder is default value");
            $widget.wijinputtext("option", "placeholder", "");
            ok($widget.wijinputtext("option", "placeholder") == "", "placeholder is string.empty");
            $widget.wijinputtext("option", "placeholder", "test");
            ok($widget.wijinputtext("option", "placeholder") == "test", "placeholder is test");
            equal($widget.wijinputtext("getText"), "test", "placeholder is test");
            $widget.wijinputtext("option", "placeholder", "あいうえお");
            ok($widget.wijinputtext("option", "placeholder") == "あいうえお", "placeholder is あいうえお");
            equal($widget.wijinputtext("getText"), "あいうえお", "placeholder is あいうえお");
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        test("readonly", function () {
            var $widget = tests.createInputText();
            ok($widget.wijinputtext("option", "readonly") == false, "readOnly is default value");
            $widget.wijinputtext('focus');
            $widget.simulateKeyStroke("a", null, function () {
                equal($widget.val(), "a", "readOnly is default value");
                $widget.wijinputtext("option", "readonly", true);
                ok($widget.wijinputtext("option", "readonly") == true, "readOnly is true");
                $widget.simulateKeyStroke("b", null, function () {
                    equal($widget.val(), "a", "readOnly is true");
                    $widget.wijinputtext('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("autoConvert", function () {
            var $widget = tests.createInputText({
                format: "a"
            });
            ok($widget.wijinputtext("option", "autoConvert") == true, "autoConvert is true");
            $widget.wijinputtext("option", "text", "AbC");
            equal($widget.val(), "abc", "autoConvert is true");
            $widget.wijinputtext("option", "autoConvert", false);
            ok($widget.wijinputtext("option", "autoConvert") == false, "autoConvert is false");
            $widget.wijinputtext("option", "text", "dEf");
            equal($widget.val(), "df", "autoConvert is false");
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        test("autoConvert for UI", function () {
            var $widget = tests.createInputText({
                format: "A",
                autoConvert: false
            });
            $widget.wijinputtext('focus');
            $widget.simulateKeyStroke("abc", null, function () {
                equal($widget.val(), "", "autoConvert is false");
                $widget.wijinputtext("option", "autoConvert", true);
                $widget.simulateKeyStroke("def", null, function () {
                    equal($widget.val(), "DEF", "autoConvert is true");
                    $widget.wijinputtext('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("blurOnLastChar", function () {
            var $widget = tests.createInputText({
                maxLength: 3
            });
            ok($widget.wijinputtext("option", "blurOnLastChar") == false, "blurOnLastChar is default value");
            $widget.wijinputtext('focus');
            $widget.simulateKeyStroke("[HOME]123", null, function () {
                equal($widget.wijinputtext('isFocused'), true, "blurOnLastChar is default value");
                $widget.wijinputtext("option", "text", "");
                $widget.wijinputtext("option", "blurOnLastChar", true);
                ok($widget.wijinputtext("option", "blurOnLastChar") == true, "blurOnLastChar is true");
                $widget.simulateKeyStroke("[HOME]123", null, function () {
                    equal($widget.wijinputtext('isFocused'), false, "blurOnLastChar is false");
                    $widget.wijinputtext('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("blurOnLeftRightKey is none", function () {
            var $widget = tests.createInputText({
                text: "abc"
            });
            ok($widget.wijinputtext("option", "blurOnLeftRightKey") == "none", "blurOnLeftRightKey is default value");
            $widget.wijinputtext('focus');
            $widget.simulateKeyStroke("[HOME][LEFT]", null, function () {
                equal($widget.wijinputtext('isFocused'), true, "blurOnLeftRightKey is default value");
                $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                    equal($widget.wijinputtext('isFocused'), true, "blurOnLeftRightKey is default value");
                    $widget.wijinputtext('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("blurOnLeftRightKey is right", function () {
            var $widget = tests.createInputText({
                text: "abc"
            });
            $widget.wijinputtext("option", "blurOnLeftRightKey", "right");
            ok($widget.wijinputtext("option", "blurOnLeftRightKey") == "right", "blurOnLeftRightKey is right");
            $widget.wijinputtext('focus');
            $widget.simulateKeyStroke("[HOME][LEFT]", null, function () {
                equal($widget.wijinputtext('isFocused'), true, "blurOnLeftRightKey is right");
                $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                    equal($widget.wijinputtext('isFocused'), false, "blurOnLeftRightKey is right");
                    $widget.wijinputtext('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("blurOnLeftRightKey is left", function () {
            var $widget = tests.createInputText({
                text: "abc"
            });
            $widget.wijinputtext("option", "blurOnLeftRightKey", "left");
            ok($widget.wijinputtext("option", "blurOnLeftRightKey") == "left", "blurOnLeftRightKey is left");
            $widget.wijinputtext('focus');
            $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                equal($widget.wijinputtext('isFocused'), true, "blurOnLastChar is left");
                $widget.simulateKeyStroke("[HOME][LEFT]", null, function () {
                    equal($widget.wijinputtext('isFocused'), false, "blurOnLastChar is left");
                    $widget.wijinputtext('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("blurOnLeftRightKey is both", function () {
            var $widget = tests.createInputText({
                text: "abc"
            });
            $widget.wijinputtext("option", "blurOnLeftRightKey", "both");
            ok($widget.wijinputtext("option", "blurOnLeftRightKey") == "both", "blurOnLeftRightKey is default value");
            $widget.wijinputtext('focus');
            $widget.simulateKeyStroke("[HOME][LEFT]", null, function () {
                equal($widget.wijinputtext('isFocused'), false, "blurOnLastChar is default value");
                $widget.wijinputtext('focus');
                $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                    equal($widget.wijinputtext('isFocused'), false, "blurOnLastChar is false");
                    $widget.wijinputtext('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("countWrappedLine", function () {
            var $widget = tests.createInputText({
                maxLineCount: 1
            }, $("<textarea/>"));
            ok($widget.wijinputtext("option", "countWrappedLine") == false, "countWrappedLine is false");
            $widget.wijinputtext("option", "text", "this is a test, this is a test, this is a test, this is a test.");
            equal($widget.val(), "this is a test, this is a test, this is a test, this is a test.", "countWrappedLine is false");
            $widget.wijinputtext("option", "countWrappedLine", true);
            ok($widget.wijinputtext("option", "countWrappedLine") == true, "countWrappedLine is true");
            equal($widget.val(), "this is a test, this is a t", "countWrappedLine is false");
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        test("format, text", function () {
            var $widget = tests.createInputText();
            $widget.wijinputtext("option", "format", "A");
            $widget.wijinputtext("option", "text", "");
            ok($widget.wijinputtext("option", "format") == "A", "format is A");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), 'ABC', "text is invalid");
            $widget.wijinputtext("option", "text", "あいう");
            equal($widget.val(), '', "text is valid");
            $widget.wijinputtext("option", "format", "a");
            $widget.wijinputtext("option", "text", "");
            ok($widget.wijinputtext("option", "format") == "a", "format is a");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), 'abc', "text is invalid");
            $widget.wijinputtext("option", "text", "あいう");
            equal($widget.val(), '', "text is valid");
            $widget.wijinputtext("option", "format", "K");
            ok($widget.wijinputtext("option", "format") == "K", "format is ｋ");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), '', "text is invalid");
            $widget.wijinputtext("option", "text", "あいう");
            equal($widget.val(), 'ｱｲｳ', "text is valid");
            $widget.wijinputtext("option", "format", "9");
            ok($widget.wijinputtext("option", "format") == "9", "format is 9");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), '', "text is invalid");
            $widget.wijinputtext("option", "text", "123");
            equal($widget.val(), '123', "text is valid");
            $widget.wijinputtext("option", "format", "#");
            ok($widget.wijinputtext("option", "format") == "#", "format is #");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), '', "text is invalid");
            $widget.wijinputtext("option", "text", "+$123,.5");
            equal($widget.val(), '+$123,.5', "text is valid");
            $widget.wijinputtext("option", "format", "@");
            ok($widget.wijinputtext("option", "format") == "@", "format is @");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), '', "text is invalid");
            $widget.wijinputtext("option", "text", "@#$%^");
            equal($widget.val(), '@#$%^', "text is valid");
            $widget.wijinputtext("option", "format", "H");
            $widget.wijinputtext("option", "text", "");
            ok($widget.wijinputtext("option", "format") == "H", "format is HHH");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), 'abc', "text is invalid");
            $widget.wijinputtext("option", "text", "あいう");
            equal($widget.val(), 'ｱｲｳ', "text is valid");
            $widget.wijinputtext("option", "format", "Ａ");
            $widget.wijinputtext("option", "text", "");
            ok($widget.wijinputtext("option", "format") == "Ａ", "format is Ａ");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), 'ＡＢＣ', "text is invalid");
            $widget.wijinputtext("option", "text", "あいう");
            equal($widget.val(), '', "text is valid");
            $widget.wijinputtext("option", "format", "ａ");
            $widget.wijinputtext("option", "text", "");
            ok($widget.wijinputtext("option", "format") == "ａ", "format is ａ");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), 'ａｂｃ', "text is invalid");
            $widget.wijinputtext("option", "text", "あいう");
            equal($widget.val(), '', "text is valid");
            $widget.wijinputtext("option", "format", "Ｋ");
            ok($widget.wijinputtext("option", "format") == "Ｋ", "format is Ｋ");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), '', "text is invalid");
            $widget.wijinputtext("option", "text", "あいう");
            equal($widget.val(), 'アイウ', "text is valid");
            $widget.wijinputtext("option", "format", "９");
            ok($widget.wijinputtext("option", "format") == "９", "format is ９");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), '', "text is invalid");
            $widget.wijinputtext("option", "text", "123");
            equal($widget.val(), '１２３', "text is valid");
            $widget.wijinputtext("option", "format", "＃");
            ok($widget.wijinputtext("option", "format") == "＃", "format is ＃");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), '', "text is invalid");
            $widget.wijinputtext("option", "text", "+$123,.5");
            equal($widget.val(), '＋＄１２３，．５', "text is valid");
            $widget.wijinputtext("option", "format", "＠");
            ok($widget.wijinputtext("option", "format") == "＠", "format is @");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), '', "text is invalid");
            $widget.wijinputtext("option", "text", "@#$%^");
            equal($widget.val(), '＠＃＄％＾', "text is valid");
            $widget.wijinputtext("option", "format", "Ｊ");
            $widget.wijinputtext("option", "text", "");
            ok($widget.wijinputtext("option", "format") == "Ｊ", "format is Ｊ");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), '', "text is invalid");
            $widget.wijinputtext("option", "text", "アイウ");
            equal($widget.val(), 'あいう', "text is valid");
            $widget.wijinputtext("option", "format", "Ｚ");
            $widget.wijinputtext("option", "text", "");
            ok($widget.wijinputtext("option", "format") == "Ｚ", "format is Ｚ");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), 'ａｂｃ', "text is invalid");
            $widget.wijinputtext("option", "text", "アイウ");
            equal($widget.val(), 'アイウ', "text is valid");
            $widget.wijinputtext("option", "format", "Ｎ");
            $widget.wijinputtext("option", "text", "");
            ok($widget.wijinputtext("option", "format") == "Ｎ", "format is Ｎ");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), '', "text is invalid");
            $widget.wijinputtext("option", "text", "きゅ");
            equal($widget.val(), 'キユ', "text is valid");
            $widget.wijinputtext("option", "format", "N");
            $widget.wijinputtext("option", "text", "");
            ok($widget.wijinputtext("option", "format") == "N", "format is N");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), '', "text is invalid");
            $widget.wijinputtext("option", "text", "きゅ");
            equal($widget.val(), 'ｷﾕ', "text is valid");
            $widget.wijinputtext("option", "format", "Ｇ");
            $widget.wijinputtext("option", "text", "");
            ok($widget.wijinputtext("option", "format") == "Ｇ", "format is Ｇ");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), '', "text is invalid");
            $widget.wijinputtext("option", "text", "きゅ");
            equal($widget.val(), 'きゆ', "text is valid");
            $widget.wijinputtext("option", "format", "Ｔ");
            $widget.wijinputtext("option", "text", "");
            ok($widget.wijinputtext("option", "format") == "Ｔ", "format is Ｔ");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), '', "text is invalid");
            $widget.wijinputtext("option", "text", "𢰝き");
            equal($widget.val(), '𢰝', "text is valid");
            $widget.wijinputtext("option", "format", "Ｄ");
            $widget.wijinputtext("option", "text", "");
            ok($widget.wijinputtext("option", "format") == "Ｄ", "format is Ｄ");
            $widget.wijinputtext("option", "text", "ａｂｃ");
            equal($widget.val(), 'ａｂｃ', "text is valid");
            $widget.wijinputtext("option", "text", "𢰝き");
            equal($widget.val(), 'き', "text is invalid");
            $widget.wijinputtext("option", "format", "Ｓ");
            $widget.wijinputtext("option", "text", "");
            ok($widget.wijinputtext("option", "format") == "Ｓ", "format is Ｓ");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), '', "text is invalid");
            $widget.wijinputtext("option", "text", "き　 屋");
            equal($widget.val(), '　　', "text is valid");
            $widget.wijinputtext("option", "format", "S");
            $widget.wijinputtext("option", "text", "");
            ok($widget.wijinputtext("option", "format") == "S", "format is S");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), '', "text is invalid");
            $widget.wijinputtext("option", "text", "き　 屋");
            equal($widget.val(), '  ', "text is valid");
            $widget.wijinputtext("option", "format", "^ＳS");
            $widget.wijinputtext("option", "text", "");
            ok($widget.wijinputtext("option", "format") == "^ＳS", "format is S");
            $widget.wijinputtext("option", "text", "abc");
            equal($widget.val(), 'abc', "text is invalid");
            $widget.wijinputtext("option", "text", "き　 屋");
            equal($widget.val(), 'き屋', "text is valid");
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        test("lengthAsByte", function () {
            var $widget = tests.createInputText({
                maxLength: 3
            });
            ok($widget.wijinputtext("option", "lengthAsByte") == false, "lengthAsByte is false");
            $widget.wijinputtext("option", "text", "あいうえお");
            equal($widget.val(), "あいう", "lengthAsByte is false");
            $widget.wijinputtext("option", "lengthAsByte", true);
            ok($widget.wijinputtext("option", "lengthAsByte") == true, "lengthAsByte is true");
            equal($widget.val(), "あ");
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        test("maxLength", function () {
            var $widget = tests.createInputText();
            ok($widget.wijinputtext("option", "maxLength") == 0, "maxLength is 0");
            $widget.wijinputtext("option", "text", "abcdef");
            equal($widget.val(), "abcdef", "maxLength is 0");
            $widget.wijinputtext("option", "text", "あいうえお");
            equal($widget.val(), "あいうえお", "maxLength is 0");
            $widget.wijinputtext("option", "maxLength", 3);
            ok($widget.wijinputtext("option", "maxLength") == 3, "maxLength is 3");
            equal($widget.val(), "あいう", "maxLength is 3");
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        test("maxLineCount", function () {
            var $widget = tests.createInputText({
            }, $("<textarea/>"));
            ok($widget.wijinputtext("option", "maxLineCount") == 0, "maxLineCount is 0");
            $widget.wijinputtext("option", "text", "abcdef\ndef");
            equal($widget.val(), "abcdef\ndef", "maxLineCount is 0");
            $widget.wijinputtext("option", "maxLineCount", 1);
            ok($widget.wijinputtext("option", "maxLineCount") == 1, "maxLineCount is 1");
            equal($widget.val(), "abcdef", "maxLineCount is 1");
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        test("passwordChar", function () {
            var $widget = tests.createInputText({
                text: "abcdef"
            });
            ok($widget.wijinputtext("option", "passwordChar") == "", "passwordChar is default value");
            equal($widget.val(), 'abcdef', "passwordChar is default value");
            $widget.wijinputtext("option", "passwordChar", "*");
            ok($widget.wijinputtext("option", "passwordChar") == "*", "passwordChar is *");
            $widget.wijinputtext("option", "passwordChar", "あ");
            ok($widget.wijinputtext("option", "passwordChar") == "あ", "passwordChar is あ");
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        test("ellipsis", function () {
            var $widget = tests.createInputText({
                text: "this is a test, this is a test, this is a test."
            });
            ok($widget.wijinputtext("option", "ellipsis") == "none", "ellipsis is default value");
            equal($widget.wijinputtext("getText"), "this is a test, this is a test, this is a test.", "ellipsis is default value");
            $widget.wijinputtext("option", "ellipsis", "ellipsisEnd");
            ok($widget.wijinputtext("option", "ellipsis") == "ellipsisEnd", "ellipsis is ellipsisEnd");
            equal($widget.wijinputtext("getText"), "this is a test, this is a t…");
            $widget.wijinputtext("option", "ellipsis", "ellipsisPath");
            ok($widget.wijinputtext("option", "ellipsis") == "ellipsisPath", "ellipsis is ellipsisEnd");
            equal($widget.wijinputtext("getText"), "this is a test…is is a test.");
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        test("ellipsisString", function () {
            var $widget = tests.createInputText({
                text: "this is a test, this is a test, this is a test.",
                ellipsis: "ellipsisEnd"
            });
            ok($widget.wijinputtext("option", "ellipsisString") == "…", "ellipsisString is default value");
            equal($widget.wijinputtext("getText"), "this is a test, this is a t…");
            $widget.wijinputtext("option", "ellipsisString", "abc");
            ok($widget.wijinputtext("option", "ellipsisString") == "abc", "ellipsisString is abc");
            equal($widget.wijinputtext("getText"), "this is a test, this is a abc");
            $widget.wijinputtext("option", "ellipsisString", "あいう");
            ok($widget.wijinputtext("option", "ellipsisString") == "あいう", "ellipsisString is あいう");
            equal($widget.wijinputtext("getText"), "this is a test, this iあいう");
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        test("highlightText1", function () {
            var $widget = tests.createInputText({
                text: "abc"
            });
            ok($widget.wijinputtext("option", "highlightText") == false, "highlightText is default value");
            $widget.wijinputtext("focus");
            setTimeout(function () {
                equal($widget.wijinputtext("getSelectedText"), "");               
                $widget.remove();
                start();                
            }, 200);
            stop();
        });
        test("highlightText2", function () {
            var $widget = tests.createInputText({
                text: "abc"
            });
            $widget.wijinputtext("option", "highlightText", true);
            ok($widget.wijinputtext("option", "highlightText") == true, "highlightText is default value");
            $widget.wijinputtext("focus");
            setTimeout(function () {
                equal($widget.wijinputtext("getSelectedText"), "abc");               
                $widget.remove();
                start();                
            }, 200);
            stop();
        });       
        test("showOverflowTip", function () {
            var $widget = tests.createInputText({
                text: "this is a test, this is a test, this is a test.",
                ellipsis: "ellipsisEnd"
            });
            ok($widget.wijinputtext("option", "showOverflowTip") == false, "showOverflowTip is default value");
            $widget.wijinputtext("option", "showOverflowTip", true);
            ok($widget.wijinputtext("option", "showOverflowTip") == true, "showOverflowTip is default value");
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        test("Set options in constructor", function () {
            var $widget = tests.createInputText({
                autoConvert: false,
                blurOnLastChar: true,
                blurOnLeftRightKey: "both",
                countWrappedLine: true,
                dropDownButtonAlign: "left",
                culture: "ja-JP",
                format: "9",
                hideEnter: true,
                invalidClass: "test",
                imeMode: "active",
                lengthAsByte: true,
                maxLength: 3,
                maxLineCount: 2,
                passwordChar: "#",
                placeholder: "Please input...",
                readOnly: true,
                text: "123",
                highlightText: true,
                ellipsis: "ellipsisEnd",
                ellipsisString: "###",
                showOverflowTip: true,
                pickers: {
                    list: [
                        "aaa", 
                        "bbb", 
                        "ccc"
                    ],
                    width: 100,
                    height: 100
                }
            });
            ok($widget.wijinputtext("option", "autoConvert") == false);
            ok($widget.wijinputtext("option", "blurOnLastChar") == true);
            ok($widget.wijinputtext("option", "blurOnLeftRightKey") == "both");
            ok($widget.wijinputtext("option", "countWrappedLine") == true);
            ok($widget.wijinputtext("option", "culture") == "ja-JP");
            ok($widget.wijinputtext("option", "dropDownButtonAlign") == "left");
            ok($widget.wijinputtext("option", "format") == "9");
            ok($widget.wijinputtext("option", "hideEnter") == true);
            ok($widget.wijinputtext("option", "invalidClass") == "test");
            ok($widget.wijinputtext("option", "imeMode") == "active");
            ok($widget.wijinputtext("option", "lengthAsByte") == true);
            ok($widget.wijinputtext("option", "maxLength") == 3);
            ok($widget.wijinputtext("option", "maxLineCount") == 2);
            ok($widget.wijinputtext("option", "passwordChar") == "#");
            ok($widget.wijinputtext("option", "placeholder") == "Please input...");
            ok($widget.wijinputtext("option", "readOnly") == true);
            ok($widget.wijinputtext("option", "text") == "123");
            ok($widget.wijinputtext("option", "highlightText") == true);
            ok($widget.wijinputtext("option", "ellipsis") == "ellipsisEnd");
            ok($widget.wijinputtext("option", "ellipsisString") == "###");
            ok($widget.wijinputtext("option", "showOverflowTip") == true);
            var pickers = $widget.wijinputtext("option", "pickers");
            ok(pickers.list.length == 3);
            ok(pickers.width == 100);
            ok(pickers.height == 100);
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        test("Set options in constructor for multilineText", function () {
            var $widget = tests.createInputText({
                autoConvert: false,
                blurOnLastChar: true,
                blurOnLeftRightKey: "both",
                countWrappedLine: true,
                culture: "ja-JP",
                dropDownButtonAlign: "left",
                format: "9",
                hideEnter: true,
                invalidClass: "test",
                imeMode: "active",
                lengthAsByte: true,
                maxLength: 3,
                maxLineCount: 2,
                passwordChar: "#",
                placeholder: "Please input...",
                readOnly: true,
                text: "123",
                highlightText: true,
                ellipsis: "ellipsisEnd",
                ellipsisString: "###",
                showOverflowTip: true,
                pickers: {
                    list: [
                        "aaa", 
                        "bbb", 
                        "ccc"
                    ],
                    width: 100,
                    height: 100
                }
            }, $("<textarea/>"));
            ok($widget.wijinputtext("option", "autoConvert") == false);
            ok($widget.wijinputtext("option", "blurOnLastChar") == true);
            ok($widget.wijinputtext("option", "blurOnLeftRightKey") == "both");
            ok($widget.wijinputtext("option", "countWrappedLine") == true);
            ok($widget.wijinputtext("option", "culture") == "ja-JP");
            ok($widget.wijinputtext("option", "dropDownButtonAlign") == "left");
            ok($widget.wijinputtext("option", "format") == "9");
            ok($widget.wijinputtext("option", "hideEnter") == true);
            ok($widget.wijinputtext("option", "invalidClass") == "test");
            ok($widget.wijinputtext("option", "imeMode") == "active");
            ok($widget.wijinputtext("option", "lengthAsByte") == true);
            ok($widget.wijinputtext("option", "maxLength") == 3);
            ok($widget.wijinputtext("option", "maxLineCount") == 2);
            ok($widget.wijinputtext("option", "passwordChar") == "#");
            ok($widget.wijinputtext("option", "placeholder") == "Please input...");
            ok($widget.wijinputtext("option", "readOnly") == true);
            ok($widget.wijinputtext("option", "text") == "123");
            ok($widget.wijinputtext("option", "highlightText") == true);
            ok($widget.wijinputtext("option", "ellipsis") == "ellipsisEnd");
            ok($widget.wijinputtext("option", "ellipsisString") == "###");
            ok($widget.wijinputtext("option", "showOverflowTip") == true);
            var pickers = $widget.wijinputtext("option", "pickers");
            ok(pickers.list.length == 3);
            ok(pickers.width == 100);
            ok(pickers.height == 100);
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        QUnit.module("wijinputtext: methods");
        test("getPostValue", function () {
            var $widget = tests.createInputText({
                text: "1234567"
            });
            equal($widget.wijinputtext("getPostValue"), "1234567", "getPostValue returns value");
            $widget.wijinputtext("option", "format", "9");
            $widget.wijinputtext("option", "text", "abc123");
            equal($widget.wijinputtext("getPostValue"), "123", "getPostValue returns value");
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        test("getText", function () {
            var $widget = tests.createInputText({
                text: "1234567"
            });
            equal($widget.wijinputtext("getText"), "1234567", "getText returns value");
            $widget.wijinputtext("option", "format", "9");
            $widget.wijinputtext("option", "text", "abc123");
            equal($widget.wijinputtext("getText"), "123", "getText returns value");
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        test("setText", function () {
            var $widget = tests.createInputText();
            $widget.wijinputtext("setText", "1234567");
            equal($widget.val(), "1234567", "getText returns value");
            $widget.wijinputtext("option", "format", "9");
            $widget.wijinputtext("setText", "abc123");
            equal($widget.val(), "123", "getText returns value");
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        test("drop", function () {
            var $widget = tests.createInputText({
                format: "9"
            });
            var items = [
                {
                    label: 'Minimum',
                    value: "123"
                }, 
                {
                    label: 'Middle',
                    value: "456"
                }, 
                {
                    label: 'Maximum',
                    value: "789"
                }
            ];
            $widget.wijinputtext("option", "comboItems", items);
            $widget.wijinputtext("drop");
            $widget.simulateKeyStroke("[DOWN][ENTER]", null, function () {
                equal($widget.val(), "123", "Date is dropped down.");
                $widget.wijinputtext('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("selectText, getSelectedText", function () {
            var $widget = tests.createInputText({
                text: "abcde"
            });
            $widget.wijinputtext("selectText", 1, 3);
            setTimeout(function () {
                equal($widget.wijinputtext("getSelectedText"), "bc", "getSelectedText returns value");
                $widget.remove();
                start();
            }, 200);
            stop();
        });
        QUnit.module("wijinputtext: events");
        test("invalidInput", function () {
            var $widget = tests.createInputText({
                format: "9"
            });
            var events = new Array();
            $widget.wijinputtext({
                invalidInput: function (e, data) {
                    events.push("invalidInput");
                }
            });
            $widget.simulateKeyStroke("[HOME]a", null, function () {
                notEqual(events.toString().indexOf("invalidInput"), -1, "invalidInput is fired.");
                $widget.wijinputtext('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("textChanged", function () {
            var $widget = tests.createInputText();
            var events = new Array();
            $widget.wijinputtext({
                textChanged: function (e, data) {
                    events.push("textChanged");
                }
            });
            $widget.wijinputtext("option", "text", "abc");
            notEqual(events.toString().indexOf("textChanged"), -1, "textChanged is fired.");
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        test("textChanged by UI", function () {
            var $widget = tests.createInputText({
                format: '9'
            });
            var events = new Array();
            $widget.wijinputtext({
                textChanged: function (e, data) {
                    events.push("textChanged");
                }
            });
            $widget.simulateKeyStroke("[HOME]1", null, function () {
                notEqual(events.toString().indexOf("textChanged"), -1, "textChanged is fired.");
                $widget.wijinputtext('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("dropDownOpen", function () {
            var $widget = tests.createInputText({
                format: "9"
            });
            var items = [
                {
                    label: 'Minimum',
                    value: "123"
                }, 
                {
                    label: 'Middle',
                    value: "456"
                }, 
                {
                    label: 'Maximum',
                    value: "789"
                }
            ];
            $widget.wijinputtext("option", "comboItems", items);
            var events = new Array();
            $widget.wijinputtext({
                dropDownOpen: function (e, data) {
                    events.push("dropDownOpen");
                }
            });
            $widget.wijinputtext("drop");
            notEqual(events.toString().indexOf("dropDownOpen"), -1, "dropDownOpen is fired.");
            $widget.wijinputtext("drop");
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        test("dropDownClose", function () {
            var $widget = tests.createInputText({
                format: "9"
            });
            var items = [
                {
                    label: 'Minimum',
                    value: "123"
                }, 
                {
                    label: 'Middle',
                    value: "456"
                }, 
                {
                    label: 'Maximum',
                    value: "789"
                }
            ];
            $widget.wijinputtext("option", "comboItems", items);
            var events = new Array();
            $widget.wijinputtext({
                dropDownClose: function (e, data) {
                    events.push("dropDownClose");
                }
            });
            $widget.wijinputtext("drop");
            $widget.wijinputtext("drop");
            notEqual(events.toString().indexOf("dropDownClose"), -1, "dropDownClose is fired.");
            $widget.wijinputtext('destroy');
            $widget.remove();
        });
        //test("keyExit", function () {
        //    var $widget = tests.createInputText();
        //    var events = new Array();
        //    $widget.wijinputtext({
        //        keyExit: function (e, data) {
        //            events.push("keyExit");
        //        }
        //    });
        //    $widget.wijinputtext("focus");
        //    $widget.simulateKeyStroke("[TAB]", null, function () {
        //        notEqual(events.toString().indexOf("keyExit"), -1, "keyExit is fired.");
        //        $widget.wijinputtext('destroy');
        //        $widget.remove();
        //        start();
        //    });
        //    stop();
        //});
        test("keyExit", function () {
            var $widget = tests.createInputText({
                maxLength: 3,
                blurOnLastChar: true
            });
            var events = new Array();
            $widget.wijinputtext({
                keyExit: function (e, data) {
                    events.push("keyExit");
                }
            });
            $widget.wijinputtext("focus");
            $widget.simulateKeyStroke("[HOME]abc", null, function () {
                notEqual(events.toString().indexOf("keyExit"), -1, "keyExit is fired.");
                $widget.wijinputtext('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("keyExit", function () {
            var $widget = tests.createInputText({
                text: "abc",
                blurOnLeftRightKey: "both"
            });
            var events = new Array();
            $widget.wijinputtext({
                keyExit: function (e, data) {
                    events.push("keyExit");
                }
            });
            $widget.wijinputtext("focus");
            $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                notEqual(events.toString().indexOf("keyExit"), -1, "keyExit is fired.");
                $widget.wijinputtext('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
    })(wijmo.tests || (wijmo.tests = {}));
    var tests = wijmo.tests;
})(wijmo || (wijmo = {}));
