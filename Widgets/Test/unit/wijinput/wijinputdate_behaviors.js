var wijmo;
(function (wijmo) {
    (function (tests) {
        QUnit.module("wijinputdate: behaviors");
        test("nulltext", function () {
            var $widget = tests.createInputDate({
                showNullText: true,
                nullText: 'Birth date',
                date: null
            });
            ok($widget.val() == "Birth date", "null text ok");
            $widget.wijinputdate('focus');
            window.setTimeout(function () {
                ok($widget.val() !== "Birth date", "null text hidden ok");
                $widget.wijinputdate('destroy');
                $widget.remove();
                start();
            }, 100);
            stop();
        });
        test("default value", function () {
            var $widget = tests.createInputDate(null, $("<input type='text' value='5/17/2011' />"));
            ok($widget.val() == "5/17/2011", "Element value applied");
            $widget.wijinputdate('destroy');
            $widget.remove();
            $widget = tests.createInputDate({
                date: "6/1/2011"
            }, $("<input type='text' value='5/10/2011' />"));
            ok($widget.val() == "6/1/2011", "Date value applied");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
    })(wijmo.tests || (wijmo.tests = {}));
    var tests = wijmo.tests;
})(wijmo || (wijmo = {}));
