var wijmo;
(function (wijmo) {
    (function (tests) {
        var $ = jQuery;
        QUnit.module("wijinputnumber: simulate");
        test("key typing", function () {
            var $widget = tests.createInputNumber();
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[LEFT][LEFT][LEFT]123456789abc", 200, null, function () {
                ok($widget.val() == "123456789.00", "Value input ok");
                $widget.wijinputnumber('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("key typing maxValue", function () {
            var $widget = tests.createInputNumber({
                maxValue: 1000,
                minValue: -1000
            });
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[LEFT][LEFT][LEFT]123456789", 200, null, function () {
                $widget.blur();
                setTimeout(function () {
                    ok($widget.val() == "1000.00", "maxValue ok");
                    $widget.wijinputnumber('destroy');
                    $widget.remove();
                    start();
                }, 500);
            });
            stop();
        });
        test("spinUpDown", function () {
            var $widget = tests.createInputNumber();
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[UP][UP][UP][UP][UP][UP][UP][UP][DOWN][DOWN][DOWN]", 200, null, function () {
                ok($widget.val() == "5.00", "Increment is ok");
                $widget.wijinputnumber('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("spinUpDown maxValue", function () {
            var $widget = tests.createInputNumber({
                value: 999,
                maxValue: 1000,
                minValue: -1000
            });
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[UP][UP][UP][UP][UP][UP][UP][UP][DOWN][DOWN][DOWN]", 200, null, function () {
                ok($widget.val() == "997.00", "Increment is ok");
                $widget.wijinputnumber('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
    })(wijmo.tests || (wijmo.tests = {}));
    var tests = wijmo.tests;
})(wijmo || (wijmo = {}));
