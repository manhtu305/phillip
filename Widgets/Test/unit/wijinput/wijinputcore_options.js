﻿var wijmo;
(function (wijmo) {
    (function (tests) {
        var $ = jQuery;
        var controlName = window["ControlName"];
        QUnit.module(window["ControlName"] + ": options");
        test("allowSpinLoop", function () {
            var $widget = tests.createInputCore();
            ok($widget[controlName]("option", "allowSpinLoop") == false, "allowSpinLoop is false");
            $widget[controlName]("option", "allowSpinLoop", true);
            ok($widget[controlName]("option", "allowSpinLoop") == true, "allowSpinLoop is true");
            $widget[controlName]('destroy');
            $widget.remove();
        });
        test("blurOnLastChar", function () {
            var $widget = tests.createInputCore();
            if(window["ControlName"] == "wijinputnumber") {
                ok($widget[controlName]("option", "blurOnLastChar") == undefined, "blurOnLastChar is false");
            } else {
                ok($widget[controlName]("option", "blurOnLastChar") == false, "blurOnLastChar is false");
            }
            $widget[controlName]("option", "blurOnLastChar", true);
            ok($widget[controlName]("option", "blurOnLastChar") == true, "blurOnLastChar is true");
            $widget[controlName]('destroy');
            $widget.remove();
        });
        test("blurOnLeftRightKey", function () {
            var $widget = tests.createInputCore();
            ok($widget[controlName]("option", "blurOnLeftRightKey") == "none", "blurOnLeftRightKey is none");
            $widget[controlName]("option", "blurOnLeftRightKey", "left");
            ok($widget[controlName]("option", "blurOnLeftRightKey") == "left", "blurOnLeftRightKey is left");
            $widget[controlName]("option", "blurOnLeftRightKey", "right");
            ok($widget[controlName]("option", "blurOnLeftRightKey") == "right", "blurOnLeftRightKey is right");
            $widget[controlName]("option", "blurOnLeftRightKey", "none");
            ok($widget[controlName]("option", "blurOnLeftRightKey") == "none", "blurOnLeftRightKey is none");
            $widget[controlName]('destroy');
            $widget.remove();
        });
        test("culture", function () {
            var $widget = tests.createInputCore();
            ok($widget[controlName]("option", "culture") == "", "culture is false");
            $widget[controlName]("option", "culture", "ja-JP");
            ok($widget[controlName]("option", "culture") == "ja-JP", "culture is ja-JP");
            $widget[controlName]("option", "culture", "zh-CN");
            ok($widget[controlName]("option", "culture") == "zh-CN", "culture is zh-CN");
            $widget[controlName]("option", "culture", "en-US");
            ok($widget[controlName]("option", "culture") == "en-US", "culture is en-US");
            $widget[controlName]('destroy');
            $widget.remove();
        });
        test("dropDownButtonAlign", function () {
            var $widget = tests.createInputCore();
            ok($widget[controlName]("option", "dropDownButtonAlign") == "right", "dropDownButtonAlign is default");
            $widget[controlName]("option", "dropDownButtonAlign", "left");
            ok($widget[controlName]("option", "dropDownButtonAlign") == "left", "dropDownButtonAlign is left");
            $widget[controlName]("option", "dropDownButtonAlign", "right");
            ok($widget[controlName]("option", "dropDownButtonAlign") == "right", "dropDownButtonAlign is right");
            $widget[controlName]('destroy');
            $widget.remove();
        });
        test("hideEnter", function () {
            var $widget = tests.createInputCore();
            ok($widget[controlName]("option", "hideEnter") == false, "hideEnter is false");
            $widget[controlName]("option", "hideEnter", true);
            ok($widget[controlName]("option", "hideEnter") == true, "hideEnter is false");
            $widget[controlName]('destroy');
            $widget.remove();
        });
        test("imeMode", function () {
            var $widget = tests.createInputCore();
            if(window["ControlName"] == "wijinputnumber") {
                ok($widget[controlName]("option", "imeMode") == "", "imeMode is auto");
            } else {
                ok($widget[controlName]("option", "imeMode") == "auto", "imeMode is auto");
            }
            $widget[controlName]("option", "imeMode", "active");
            ok($widget[controlName]("option", "imeMode") == "active", "imeMode is active");
            $widget[controlName]("option", "imeMode", "inactive");
            ok($widget[controlName]("option", "imeMode") == "inactive", "imeMode is inactive");
            $widget[controlName]("option", "imeMode", "disabled");
            ok($widget[controlName]("option", "imeMode") == "disabled", "imeMode is disabled");
            $widget[controlName]("option", "imeMode", "auto");
            ok($widget[controlName]("option", "imeMode") == "auto", "imeMode is active");
            $widget[controlName]('destroy');
            $widget.remove();
        });
        test("invalidClass", function () {
            var $widget = tests.createInputCore();
            ok($widget[controlName]("option", "invalidClass") == "ui-state-error", "invalidClass is defalut");
            $widget[controlName]("option", "invalidClass", "test");
            ok($widget[controlName]("option", "invalidClass") == "test", "invalidClass is test");
            $widget[controlName]("option", "invalidClass", "¤¢¤¤¤¦");
            ok($widget[controlName]("option", "invalidClass") == "¤¢¤¤¤¦", "invalidClass is ¤¢¤¤¤¦");
            $widget[controlName]('destroy');
            $widget.remove();
        });
        test("placeholder", function () {
            var $widget = tests.createInputCore();
            ok($widget[controlName]("option", "placeholder") == null, "placeholder is defalut");
            $widget[controlName]("option", "placeholder", "test");
            ok($widget[controlName]("option", "placeholder") == "test", "placeholder is test");
            $widget[controlName]("option", "placeholder", "¤¢¤¤¤¦");
            ok($widget[controlName]("option", "placeholder") == "¤¢¤¤¤¦", "placeholder is ¤¢¤¤¤¦");
            $widget[controlName]('destroy');
            $widget.remove();
        });
        test("pickers.height", function () {
            var $widget = tests.createInputCore();
            var pickers = $widget[controlName]("option", "pickers");
            ok(pickers.height == undefined, "pickers.height default value");
            pickers.height = 100;
            ok(pickers.height == 100, "pickers.height is 100");
            pickers.height = 0;
            ok(pickers.height == 0, "pickers.height is 0");
            pickers.height = -1;
            ok(pickers.height == -1, "pickers.height is -1");
            $widget[controlName]('destroy');
            $widget.remove();
        });
        test("pickers.list", function () {
            var $widget = tests.createInputCore();
            var pickers = $widget[controlName]("option", "pickers");
            ok(pickers.list == undefined, "pickers.list default value");
            var items = [
                {
                    label: 'Minimum',
                    value: "123"
                }, 
                {
                    label: 'Middle',
                    value: "456"
                }, 
                {
                    label: 'Maximum',
                    value: "789"
                }
            ];
            pickers.list = items;
            ok(pickers.list == items, "picker.list is set");
            $widget[controlName]('destroy');
            $widget.remove();
        });
        test("pickers.width", function () {
            var $widget = tests.createInputCore();
            var pickers = $widget[controlName]("option", "pickers");
            ok(pickers.width == undefined, "pickers.width default value");
            pickers.width = 100;
            ok(pickers.width == 100, "pickers.width is 100");
            pickers.width = 0;
            ok(pickers.width == 0, "pickers.width is 0");
            pickers.width = -1;
            ok(pickers.width == -1, "pickers.width is -1");
            $widget[controlName]('destroy');
            $widget.remove();
        });
        test("readonly", function () {
            var $widget = tests.createInputCore();
            ok($widget[controlName]("option", "readonly") == false, "readOnly is false");
            $widget[controlName]("option", "readonly", true);
            ok($widget[controlName]("option", "readonly") == true, "readOnly is true");
            $widget[controlName]('destroy');
            $widget.remove();
        });
        test("readOnly", function () {
            if(window["ControlName"] == "wijinputmask") {
                var $widget = tests.createInputCore({
                    readonly: true,
                    maskFormat: "999"
                });
            } else {
                var $widget = tests.createInputCore({
                    readonly: true
                });
            }
            var original = $widget[controlName]("getText");
            $widget[controlName]('focus');
            ok($widget[controlName]("option", "readonly") == true, "readOnly is true");
            $widget.simulateKeyStroke("[HOME]6", null, function () {
                equal($widget[controlName]("getText"), original, "readOnly is true");
                $widget[controlName]("option", "readonly", false);
                $widget[controlName]('focus');
                $widget.simulateKeyStroke("[HOME]7", null, function () {
                    notEqual($widget[controlName]("getText"), original, "readOnly is true");
                    $widget[controlName]('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("showDropDownButton", function () {
            var $widget = tests.createInputCore();
            if(window["ControlName"] == "wijinputdate") {
                ok($widget[controlName]("option", "showDropDownButton") == true, "showDropDownButton is default");
                $widget[controlName]("option", "showDropDownButton", false);
                ok($widget[controlName]("option", "showDropDownButton") == false, "showDropDownButton is true");
            } else {
                ok($widget[controlName]("option", "showDropDownButton") == false, "showDropDownButton is default");
                $widget[controlName]("option", "showDropDownButton", true);
                ok($widget[controlName]("option", "showDropDownButton") == true, "showDropDownButton is true");
            }
            $widget[controlName]('destroy');
            $widget.remove();
        });
        test("showSpinner", function () {
            var $widget = tests.createInputCore();
            ok($widget[controlName]("option", "showSpinner") == false, "showSpinner is default");
            $widget[controlName]("option", "showSpinner", true);
            ok($widget[controlName]("option", "showSpinner") == true, "showSpinner is true");
            $widget[controlName]('destroy');
            $widget.remove();
        });
        test("spinnerAlign", function () {
            var $widget = tests.createInputCore();
            ok($widget[controlName]("option", "spinnerAlign") == "verticalRight", "spinnerAlign is default");
            $widget[controlName]("option", "spinnerAlign", "horizontalDownLeft");
            ok($widget[controlName]("option", "spinnerAlign") == "horizontalDownLeft", "spinnerAlign is horizontalDownLeft");
            $widget[controlName]("option", "spinnerAlign", "horizontalUpLeft");
            ok($widget[controlName]("option", "spinnerAlign") == "horizontalUpLeft", "spinnerAlign is horizontalUpLeft");
            $widget[controlName]("option", "spinnerAlign", "verticalLeft");
            ok($widget[controlName]("option", "spinnerAlign") == "verticalLeft", "spinnerAlign is verticalLeft");
            $widget[controlName]('destroy');
            $widget.remove();
        });
        test("tabAction", function () {
            var $widget = tests.createInputCore({
                tabAction: "field"
            });
            $widget[controlName]('focus');
            ok($widget[controlName]("option", "tabAction") == "field", "tabAction is field");
            $widget.simulateKeyStroke("[HOME][TAB]", null, function () {
                equal($widget[controlName]("isFocused"), true, "tabAction is field");
                $widget[controlName]("option", "tabAction", "control");
                $widget[controlName]('focus');
                $widget.simulateKeyStroke("[HOME][TAB]", null, function () {
                    notEqual($widget[controlName]("isFocused"), false, "tabAction is control");
                    $widget[controlName]('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        QUnit.module(window["ControlName"] + ": methods");
        test("focus, isFocused", function () {
            if(window["ControlName"] == "wijinputmask") {
                var $widget = tests.createInputCore({
                    maskFormat: "9"
                });
            } else {
                var $widget = tests.createInputCore();
            }
            equal($widget[controlName]("isFocused"), false, "isFocused returns false");
            $widget[controlName]("focus");
            $widget.simulateKeyStroke("[HOME]3", null, function () {
                if(window["ControlName"] == "wijinputdate") {
                    equal($widget[controlName]("getText"), "3/" + new Date().getDate() + "/" + new Date().getFullYear(), "isFocused returns true");
                } else if(window["ControlName"] == "wijinputnumber") {
                    equal($widget[controlName]("getText"), "3.00", "isFocused returns true");
                } else {
                    equal($widget[controlName]("getText"), "3", "isFocused returns true");
                }
                equal($widget[controlName]("isFocused"), true, "isFocused returns true");
                $widget[controlName]('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("destroy, isDestroyed", function () {
            var $widget = tests.createInputCore();
            ok($widget[controlName]("isDestroyed") == false, "isDestroyed returns false");
            $widget[controlName]("destroy");
            $widget.remove();
        });
    })(wijmo.tests || (wijmo.tests = {}));
    var tests = wijmo.tests;
})(wijmo || (wijmo = {}));
