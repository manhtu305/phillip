var wijmo;
(function (wijmo) {
    (function (tests) {
        var $ = jQuery;
        QUnit.module("wijinputdate: simulate");
        test("key typing", function () {
            var $widget = tests.createInputMask({
                mask: '>L|LLLLLLLLLLLL'
            });
            $widget.wijinputmask('focus');
            $widget.simulateKeyStroke("[HOME]This IS[BACKSPACE][BACKSPACE]is a sample", 200, null, function () {
                ok(true, $widget.val());
                $widget.wijinputmask('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
    })(wijmo.tests || (wijmo.tests = {}));
    var tests = wijmo.tests;
})(wijmo || (wijmo = {}));
