﻿(function ($) {
    module("wijinputmask:tickets");
    if ($.browser.msie && parseInt($.browser.version) < 8) {
        test("In IE7, if the widget is inside tabel cell, and the value is too long, the cell's width will increase", function () {
            var tabel = $("<table><tr><td style='width:150px;'></td></tr>").appendTo("body"),
                td = tabel.find("td:first"),
                inputmask = $("<input type='text'/>").appendTo(td);

            inputmask.wijinputmask();
            inputmask.wijinputmask("option", "text", "444444444444444444444444444444444444444444444444444444444444444");

            ok(td.width() < 160, "the td width is not extend by the input text.");
            tabel.remove();
        });

        test("In IE7, if the two wijinput is place as inline mode, the second widget line wrap.", function () {
            var container = $("<div></div>").appendTo("body"),
                inputmask = $("<input type='text'/>").appendTo(container),
                inputmask1 = $("<input type='text'/>").appendTo(container);
            inputmask.wijinputmask();
            inputmask1.wijinputmask();
            ok(container.height() < 40, "the second widget is not line wrap.");
            container.remove();
        });
    }

    test("#41124", function() {
        var inputmask = $("<input type='text'/>").val("(412) 867-5309").appendTo("body");
        inputmask.wijinputmask({ mask: '(999) 000-0000' });
        inputmask.focus();
        inputmask.wijtextselection(0, inputmask.val().length);
        inputmask.simulateKeyStroke("w");
        inputmask.blur();
        setTimeout(function() {
            ok(inputmask.val() === "(412) 867-5309", "the value is not changed.");
            inputmask.remove();
            start();
        }, 500);
        stop();
    });

    test("test issue #41123 for wijinputmask", function () {
    	var inputmask = $("<input type='text'/>").appendTo("body")
			, triggerBtn
			, dropdown;

    	inputmask.wijinputmask({
    		mask: '000-0000',
    		comboItems: ['100-1000', '200-2000', '123-2909'],
    		showTrigger: true
    	});

    	triggerBtn = $(".wijmo-wijinput-trigger");

    	triggerBtn.simulate("click");

    	dropdown = $(".wijmo-wijlist-ul");
    	$(dropdown.children()[0]).simulate("click");

    	ok(document.activeElement === inputmask[0], "After selecting item from drop down, the input element get focus.");
    	inputmask.remove();
    });
    
})(jQuery)