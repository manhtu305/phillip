var wijmo;
(function (wijmo) {
    (function (tests) {
        var $ = jQuery;
        function createInput(widgetName, options, $element) {
            $element = $element || $("<input type='text'/>");
            $element.width(155).appendTo(document.body);
            $element[widgetName](options);
            return $element;
        }
        tests.createInput = createInput;
        tests.createInputCore = function (o, e) {
            return createInput(window["ControlName"], o, e);
        };
        tests.createInputDate = function (o, e) {
            return createInput("wijinputdate", o, e);
        };
        tests.createInputMask = function (o, e) {
            return createInput("wijinputmask", o, e);
        };
        tests.createInputNumber = function (o, e) {
            return createInput("wijinputnumber", o, e);
        };
        tests.createInputText = function (o, e) {
            return createInput("wijinputtext", o, e);
        };
        function wijinputTestModule(name, moduleFn) {
            $.each([
                "date", 
                "number", 
                "mask", 
                "text"
            ], function (_, inputType) {
                var widgetName = "wijinput" + inputType;
                QUnit.module(widgetName + ": core");
                function createThisInput(options, $element) {
                    return createInput(widgetName, options, $element);
                }
                moduleFn(function (name, testFn) {
                    test(name, function () {
                        return testFn(createThisInput, widgetName, inputType);
                    });
                });
            });
        }
        tests.wijinputTestModule = wijinputTestModule;
        wijinputTestModule("core", function (test) {
            test("create and destroy", function (createInput, widgetName) {
                var $widget = createInput();
                ok($widget.hasClass('wijmo-wijinput-input'), 'element css classes created.');
                $widget[widgetName]('destroy');
                ok(!$widget.hasClass('wijmo-wijinput-input'), 'all element removed');
                $widget.remove();
            });
        });
    })(wijmo.tests || (wijmo.tests = {}));
    var tests = wijmo.tests;
})(wijmo || (wijmo = {}));
