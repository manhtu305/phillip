﻿var wijmo;
(function (wijmo) {
    (function (tests) {
        var $ = jQuery;
        QUnit.module("wijinput: format functions");
        test("date format function", function () {
            equal($.wijinputcore.format(new Date(2013, 8, 12)), "9/12/2013", "format is default value");
            equal($.wijinputcore.format(new Date(2013, 8, 12), "yyyy/MM/dd"), "2013/09/12", "format is yyyy/MM/dd");
            equal($.wijinputcore.format(new Date(2013, 8, 12), {
                format: "yyyy/MM/dd"
            }), "2013/09/12", "format is yyyy/MM/dd");
            equal($.wijinputcore.format(new Date(2013, 8, 12, 23, 22, 33), {
                format: "tt hh:mm:ss",
                culture: "ja-JP"
            }), "午後 11:22:33", "format is tt hh:mm:ss");
            equal($.wijinputcore.format(new Date(2013, 8, 12, 24, 12, 12), {
                format: "HH:mm:ss",
                midnightAs0: true
            }), "00:12:12", "format is HH:mm:ss");
        });
        test("mask format function", function () {
            equal($.wijinputcore.format("1234567", "９９９-９９９９", {
                autoConvert: true
            }), "１２３-４５６７", "format is ９９９-９９９９");
            equal($.wijinputcore.format("1234567", "９９９-９９９９", {
                autoConvert: false
            }), "       ", "format is ９９９-９９９９");
        });
        test("number format function", function () {
            equal($.wijinputcore.format(12345), "12345.00", "type is default value");
            equal($.wijinputcore.format(12345, "numeric", {
                decimalPlaces: 3
            }), "12345.000", "type is numeric");
            equal($.wijinputcore.format(12345, "percent"), "12345.00 %", "type if percent");
            equal($.wijinputcore.format(12345, "currency"), "$12345.00", "type if currency");
            equal($.wijinputcore.format(12345, "currency", {
                culture: "ja-JP"
            }), "¥12345.00", "type is currency");
            equal($.wijinputcore.format(12345, "numeric", {
                positivePrefix: "+",
                negativePrefix: "-"
            }), "+12345.00", "type is numeric");
        });
        QUnit.module("wijinput: date options");
        test("amDesignator", function () {
            equal($.wijinputcore.format(new Date(2013, 8, 12, 11, 22, 33), "tt hh:mm:ss"), "AM 11:22:33", "amDesignator is default value");
            equal($.wijinputcore.format(new Date(2013, 8, 12, 11, 22, 33), "tt hh:mm:ss", {
                amDesignator: "AA"
            }), "AA 11:22:33", "amDesignator is AA");
            equal($.wijinputcore.format(new Date(2013, 8, 12, 11, 22, 33), "tt hh:mm:ss", {
                amDesignator: "あい"
            }), "あい 11:22:33", "amDesignator is あい");
        });
        test("format", function () {
            equal($.wijinputcore.format(new Date(2013, 8, 12, 11, 22, 33)), "9/12/2013", "format is default value");
            equal($.wijinputcore.format(new Date(2013, 8, 12, 11, 22, 33), {
                format: "yyyy/MM/dd"
            }), "2013/09/12", "format is yyyy/MM/dd");
            equal($.wijinputcore.format(new Date(2013, 8, 12, 11, 22, 33), {
                format: "D"
            }), "Thursday, September 12, 2013", "format is D");
            equal($.wijinputcore.format(new Date(2013, 8, 12, 11, 22, 33), {
                format: "ggg ee/MM/dd"
            }), "平成 25/09/12", "format is yyyy/MM/dd");
        });
        test("culture", function () {
            equal($.wijinputcore.format(new Date(2013, 8, 12, 11, 22, 33), "D"), "Thursday, September 12, 2013", "culture is default value");
            equal($.wijinputcore.format(new Date(2013, 8, 12, 11, 22, 33), "D", {
                culture: "en-US"
            }), "Thursday, September 12, 2013", "culture is en-US");
            equal($.wijinputcore.format(new Date(2013, 8, 12, 11, 22, 33), "D", {
                culture: "ja-JP"
            }), "2013年9月12日", "culture is ja-JP");
        });
        test("hour12As0", function () {
            equal($.wijinputcore.format(new Date(2013, 8, 12, 12, 22, 33), "tt hh:mm:ss"), "PM 12:22:33", "hour12As0 is default value");
            equal($.wijinputcore.format(new Date(2013, 8, 12, 12, 22, 33), "tt hh:mm:ss", {
                hour12As0: true
            }), "PM 00:22:33", "hour12As0 is true");
            equal($.wijinputcore.format(new Date(2013, 8, 12, 12, 22, 33), "tt hh:mm:ss", {
                hour12As0: false
            }), "PM 12:22:33", "hour12As0 is false");
        });
        test("midnightAs0", function () {
            equal($.wijinputcore.format(new Date(2013, 8, 12, 0, 22, 33), "HH:mm:ss"), "00:22:33", "midnightAs0 is default value");
            equal($.wijinputcore.format(new Date(2013, 8, 12, 0, 22, 33), "HH:mm:ss", {
                midnightAs0: true
            }), "00:22:33", "midnightAs0 is true");
            equal($.wijinputcore.format(new Date(2013, 8, 12, 0, 22, 33), "HH:mm:ss", {
                midnightAs0: false
            }), "24:22:33", "midnightAs0 is false");
        });
        test("pmDesignator", function () {
            equal($.wijinputcore.format(new Date(2013, 8, 12, 23, 22, 33), "tt hh:mm:ss"), "PM 11:22:33", "pmDesignator is default value");
            equal($.wijinputcore.format(new Date(2013, 8, 12, 23, 22, 33), "tt hh:mm:ss", {
                pmDesignator: "PP"
            }), "PP 11:22:33", "pmDesignator is PP");
            equal($.wijinputcore.format(new Date(2013, 8, 12, 23, 22, 33), "tt hh:mm:ss", {
                pmDesignator: "あい"
            }), "あい 11:22:33", "pmDesignator is あい");
        });
        QUnit.module("wijinput: number options");
        test("currencySymbol", function () {
            equal($.wijinputcore.format(12345, "currency"), "$12345.00", "currencySymbol is default value");
            equal($.wijinputcore.format(12345, "currency", {
                currencySymbol: "&"
            }), "&12345.00", "currencySymbol is &");
            equal($.wijinputcore.format(12345, "currency", {
                currencySymbol: "あい"
            }), "あい12345.00", "currencySymbol is あい");
        });
        test("culture", function () {
            equal($.wijinputcore.format(12345, "currency"), "$12345.00", "culture is default value");
            equal($.wijinputcore.format(12345, "currency", {
                culture: "en-US"
            }), "$12345.00", "culture is en-US");
            equal($.wijinputcore.format(12345, "currency", {
                culture: "ja-JP"
            }), "¥12345.00", "culture is ja-JP");
            equal($.wijinputcore.format(12, "percent", {
                culture: "en-US"
            }), "12.00 %", "culture is en-US");
            equal($.wijinputcore.format(12, "percent", {
                culture: "ja-JP"
            }), "12.00%", "culture is ja-JP");
        });
        test("decimalPlaces", function () {
            equal($.wijinputcore.format(123, "numberic"), "123.00", "decimalPlaces is default value");
            equal($.wijinputcore.format(123, "numberic", {
                decimalPlaces: 3
            }), "123.000", "decimalPlaces is 3");
            equal($.wijinputcore.format(123, "numberic", {
                decimalPlaces: 0
            }), "123", "decimalPlaces is 0");
        });
        test("negativePrefix", function () {
            equal($.wijinputcore.format(-123, "numberic"), "-123.00", "negativePrefix is default value");
            equal($.wijinputcore.format(-123, "numberic", {
                negativePrefix: "-$"
            }), "-$123.00", "negativePrefix is -$");
            equal($.wijinputcore.format(-123, "numberic", {
                negativePrefix: "負"
            }), "負123.00", "negativePrefix is 正");
        });
        test("negativeSuffix", function () {
            equal($.wijinputcore.format(-123, "numberic"), "-123.00", "negativeSuffix is default value");
            equal($.wijinputcore.format(-123, "numberic", {
                negativeSuffix: "-"
            }), "-123.00-", "negativeSuffix is -");
            equal($.wijinputcore.format(-123, "numberic", {
                negativeSuffix: "負"
            }), "-123.00負", "negativeSuffix is 負");
        });
        test("positivePrefix", function () {
            equal($.wijinputcore.format(123, "numberic"), "123.00", "positivePrefix is default value");
            equal($.wijinputcore.format(123, "numberic", {
                positivePrefix: "+$"
            }), "+$123.00", "positivePrefix is +$");
            equal($.wijinputcore.format(123, "numberic", {
                positivePrefix: "正"
            }), "正123.00", "positivePrefix is 正");
        });
        test("positiveSuffix", function () {
            equal($.wijinputcore.format(123, "numberic"), "123.00", "positiveSuffx is default value");
            equal($.wijinputcore.format(123, "numberic", {
                positiveSuffix: "+"
            }), "123.00+", "positiveSuffix is +");
            equal($.wijinputcore.format(123, "numberic", {
                positiveSuffix: "正"
            }), "123.00正", "positiveSuffix is 正");
        });
        test("showGroup", function () {
            equal($.wijinputcore.format(123456.78, "numberic"), "123456.78", "showGroup is default value");
            equal($.wijinputcore.format(123456.78, "numberic", {
                showGroup: true
            }), "123,456.78", "showGroup is true");
            equal($.wijinputcore.format(123456.78, "numberic", {
                showGroup: false
            }), "123456.78", "showGroup is true");
        });
        test("type", function () {
            equal($.wijinputcore.format(123456.78), "123456.78", "type is default value");
            equal($.wijinputcore.format(123456.78, {
                type: "numberic"
            }), "123456.78", "type is percent");
            equal($.wijinputcore.format(123456.78, {
                type: "percent"
            }), "123456.78 %", "type is percent");
            equal($.wijinputcore.format(123456.78, {
                type: "currency"
            }), "$123456.78", "type is currency");
        });
        QUnit.module("wijinput: date format keywords");
        test("keywords", function () {
            equal($.wijinputcore.format(new Date(2013, 7, 12), "y/M/d"), "13/8/12", "format is y/M/d");
            equal($.wijinputcore.format(new Date(2013, 7, 12), "yy/MM/dd"), "13/08/12", "format is yy/MM/dd");            
            //A old bug, anyway, set it same as control's result.
            //equal($.wijinputcore.format(new Date(999, 7, 12), "yyy/MMM/ddd"), "999/Aug/Mon", "format is yyy/MMM/ddd");
            equal($.wijinputcore.format(new Date(999, 7, 12), "yyy/MMM/ddd"), "0999/Aug/Mon", "format is yyy/MMM/ddd");
            equal($.wijinputcore.format(new Date(999, 7, 12), "yyyy/MMMM/dddd"), "0999/August/Monday", "format is yyyy/MMMM/dddd");
            equal($.wijinputcore.format(new Date(999, 7, 12), "yyyyy/MMMMM/ddddd"), "0999/August/Monday", "format is yyyyy/MMMMM/ddddd");
            equal($.wijinputcore.format(new Date(2013, 7, 12), "A yyyy"), "A.D. 2013", "format is A yyyy");
            equal($.wijinputcore.format(new Date(1990, 7, 12), "g e"), "H 2", "format is g e");
            equal($.wijinputcore.format(new Date(1990, 7, 12), "gg ee"), "平 02", "format is gg ee");
            equal($.wijinputcore.format(new Date(1990, 7, 12), "ggg eee"), "平成 02", "format is ggg eee");
            equal($.wijinputcore.format(new Date(1989, 7, 12), "ggg E"), "平成 元", "format is g E");
            equal($.wijinputcore.format(new Date(1989, 7, 12), "ggg EE"), "平成 元", "format is g EE");
            equal($.wijinputcore.format(new Date(2013, 7, 12, 3, 5, 12), "H:m:s"), "3:5:12", "format is H:m:s");
            equal($.wijinputcore.format(new Date(2013, 7, 12, 3, 5, 12), "HH:mm:ss"), "03:05:12", "format is HH:mm:ss");
            equal($.wijinputcore.format(new Date(2013, 7, 12, 3, 5, 12), "HHH:mmm:sss"), "03:05:12", "format is HHH:mmm:sss");
            equal($.wijinputcore.format(new Date(999, 7, 12, 3, 5, 12), "t h:m:s"), "A 3:5:12", "format is t h:m:s");
            equal($.wijinputcore.format(new Date(999, 7, 12, 3, 5, 12), "tt hh:mm:ss"), "AM 03:05:12", "format is tt hh:mm:ss");
            equal($.wijinputcore.format(new Date(999, 7, 12, 3, 5, 12), "ttt hhh:mmm:sss"), "AM 03:05:12", "ttt hhh:mmm:sss");
            equal($.wijinputcore.format(new Date(2013, 7, 12, 3, 5, 12), "ttt hhh:mmm:sss"), "AM 03:05:12", "ttt hhh:mmm:sss");
        });
        test("short keywords", function () {
            equal($.wijinputcore.format(new Date(2013, 7, 12), "d"), "8/12/2013", "format is d");
            equal($.wijinputcore.format(new Date(2013, 7, 12), "D"), "Monday, August 12, 2013", "format is D");
            equal($.wijinputcore.format(new Date(2013, 7, 12), "f"), "Monday, August 12, 2013 12:00 AM", "format is f");
            equal($.wijinputcore.format(new Date(2013, 7, 12), "F"), "Monday, August 12, 2013 12:00:00 AM", "format is F");
            equal($.wijinputcore.format(new Date(2013, 7, 12), "g"), "8/12/2013 12:00 AM", "format is g");
            equal($.wijinputcore.format(new Date(2013, 7, 12), "G"), "8/12/2013 12:00:00 AM", "format is G");
            equal($.wijinputcore.format(new Date(2013, 7, 12), "m"), "August 12", "format is m");
            equal($.wijinputcore.format(new Date(2013, 7, 12), "M"), "August 12", "format is M");
            equal($.wijinputcore.format(new Date(2013, 7, 12), "r"), "Mon, 12 Aug 2013 00:00:00 GMT", "format is r");
            equal($.wijinputcore.format(new Date(2013, 7, 12), "R"), "Mon, 12 Aug 2013 00:00:00 GMT", "format is R");
            equal($.wijinputcore.format(new Date(2013, 7, 12), "s"), "2013-08-12T00:00:00", "format is s");
            equal($.wijinputcore.format(new Date(2013, 7, 12), "t"), "12:00 AM", "format is t");
            equal($.wijinputcore.format(new Date(2013, 7, 12), "T"), "12:00:00 AM", "format is T");
            //A old bug, anyway, set it same as control's result.
            equal($.wijinputcore.format(new Date(2013, 7, 12), "y"), "2013 August", "format is y");
            equal($.wijinputcore.format(new Date(2013, 7, 12), "Y"), "2013 August", "format is Y");

            //equal($.wijinputcore.format(new Date(2013, 7, 12), "y"), "August 2013", "format is y");
            //equal($.wijinputcore.format(new Date(2013, 7, 12), "Y"), "August 2013", "format is Y");
        });
        QUnit.module("wijinput: mask options");
        test("autoConvert", function () {
            equal($.wijinputcore.format("あいうえおかき", "ＫＫＫ-ＫＫＫＫ"), "アイウ-エオカキ", "autoConvert is default value");
            equal($.wijinputcore.format("あいうえおかき", "ＫＫＫ-ＫＫＫＫ", {
                autoConvert: true
            }), "アイウ-エオカキ", "autoConvert is default value");
            equal($.wijinputcore.format("あいうえおかき", "ＫＫＫ-ＫＫＫＫ", {
                autoConvert: false
            }), "       ", "autoConvert is default value");
        });
        test("culture", function () {
            equal($.wijinputcore.format("12345","$999.99"), "$123.45", "culture is default value");
            equal($.wijinputcore.format("12345","$999.99", {
                culture: "ja-JP"
            }), "¥123.45", "culture is ja-JP");
            equal($.wijinputcore.format("12345", "$999.99", {
                culture: "zh-CN"
            }), "¥123.45", "culture is zh-CN");
        });
        test("format", function () {
            equal($.wijinputcore.format("1234567"), "1234567", "format is default value");
            equal($.wijinputcore.format("1234567", "999-9999"), "123-4567", "format is 999-9999");
            equal($.wijinputcore.format("1234567", "９９９-９９９９"),"１２３-４５６７", "format is ９９９-９９９９");
        });
        QUnit.module("wijinput: mask format keywords");
        test("keywords", function () {
            equal($.wijinputcore.format("１２３４５６", "000-000"), "123-456", "format is 000-000");
            equal($.wijinputcore.format("１２３４５６", "999-999"), "123-456", "format is 999-999");
            equal($.wijinputcore.format("+1234567", "###,###.##"), "+12,345.67", "format is ###,###.##");
            equal($.wijinputcore.format("abcdefg", ">L|LLLLLL"), "Abcdefg", "format is >L|LLLLLL");
            equal($.wijinputcore.format("ABCDEFG", "<L|LLLLLL"), "aBCDEFG", "format is <L|LLLLLL");
            equal($.wijinputcore.format("あいう", "??-?"), "あい-う", "format is ??-?");
            equal($.wijinputcore.format("あいう", "&&-&"), "あい-う", "format is &&-&");
            equal($.wijinputcore.format("亜家う", "CC-C"), "亜家-う", "format is CC-C");
            equal($.wijinputcore.format("a12", "A-AA"), "a-12", "format is A-AA");
            equal($.wijinputcore.format("a12", "a-aa"), "a-12", "format is a-aa");
            equal($.wijinputcore.format("20130112121212", "9999/99/99 00:00:00"), "2013/01/12 12:12:12", "format is 9999/99/99 00:00:00");
            equal($.wijinputcore.format("12345", "$999.99", {
                culture: "ja-JP"
            }), "¥123.45", "format is $999.99");
            equal($.wijinputcore.format("123", "\AAAA"), "A123", "format is \AAAA");
            equal($.wijinputcore.format("あｂｃ", "H-HH"), "ｱ-bc", "format is H-HH");
            equal($.wijinputcore.format("あいう", "K-KK"), "ｱ-ｲｳ", "format is K-KK");
            equal($.wijinputcore.format("123", "９-９９"), "１-２３", "format is ９-９９");
            equal($.wijinputcore.format("あいう", "Ｋ-ＫＫ"), "ア-イウ", "format is Ｋ-ＫＫ");
            equal($.wijinputcore.format("アイウ", "Ｊ-ＪＪ"), "あ-いう", "format is Ｊ-ＪＪ");
            equal($.wijinputcore.format("ｱbc", "Ｚ-ＺＺ"), "ア-ｂｃ", "format is Ｚ-ＺＺ");
            equal($.wijinputcore.format("きゅ", "N-N"), "ｷ-ﾕ", "format is N-N");
            equal($.wijinputcore.format("きゅ", "Ｎ-Ｎ"), "キ-ユ", "format is Ｎ-Ｎ");
            equal($.wijinputcore.format("きゅ", "Ｇ-Ｇ"), "き-ゆ", "format is Ｇ-Ｇ");
        });
        QUnit.module("wijinput: validation functions");
        test("parse date function", function () {
            equal($.wijinputcore.parseDate("9/12/2013").toDateString(), "Thu Sep 12 2013", "format is default value");
            equal($.wijinputcore.parseDate("2013/09/12", "yyyy/MM/dd", "ja-JP").toDateString(), "Thu Sep 12 2013", "format is yyyy/MM/dd");            
            equal($.wijinputcore.parseDate("午後 11:22:33", "tt hh:mm:ss", "ja-JP").toLocaleString(), "‎2014‎年‎1‎月‎1‎日‎ ‎23‎:‎22‎:‎33", "format is tt hh:mm:ss");
            //equal($.wijinputcore.parseDate("午後 11:22:33", "tt hh:mm:ss", "ja-JP").toLocaleString(), new Date().getFullYear() + "‎年‎1‎月‎1‎日‎ ‎23‎:‎22‎:‎33‎", "format is tt hh:mm:ss");
            equal($.wijinputcore.parseDate("00:12:12", "HH:mm:ss", "ja-JP").toLocaleString(), "‎2014‎年‎1‎月‎1‎日‎ ‎0‎:‎12‎:‎12", "format is HH:mm:ss");
        });
        test("parse number function", function () {
            equal($.wijinputcore.parseNumber("12,345.00"), 12345, "format is default value");
            equal($.wijinputcore.parseNumber("12.345,00", "de-DE"), 12345, "culture is de");
            equal($.wijinputcore.parseNumber("45.00 %"), 0.45, "type is percent");
            equal($.wijinputcore.parseNumber("$12345.67"), 12345.67, "type is currency");
        });
        test("validate function", function () {
            equal($.wijinputcore.validateText("123", "9"), true, "validate text");
            equal($.wijinputcore.validateText("あ山９", "Ｚ"), true, "validate text");
            equal($.wijinputcore.validateText("9999999", 4, 7), true, "validate text");
            equal($.wijinputcore.validateMask("1234567", "999-9999"), false, "validate mask");
            equal($.wijinputcore.validateDate("12/3/2012", new Date(2013, 0, 1), new Date(2013, 11, 31)), false, "validate date");
            equal($.wijinputcore.validateDate("平成 25年12月16日", new Date(2013, 0, 1), new Date(2013, 11, 31), "ggg ee年MM月dd日"), true, "validate date");
            equal($.wijinputcore.validateNumber("123,45.6", 100, 100000), true, "validate number");
        });
        QUnit.module("wijinput: mask format keywords");
        test("keywords", function () {
            equal($.wijinputcore.validateMask("123-456", "000-000"), true, "validateMask is 000-000");
            equal($.wijinputcore.validateMask("１２３-４５６", "000-000"), false, "validateMask is 000-000");
            equal($.wijinputcore.validateMask("123-456", "999-999"), true, "validateMask is 999-999");
            equal($.wijinputcore.validateMask("１２３４５６", "999-999"), false, "validateMask is 999-999");
            equal($.wijinputcore.validateMask("+12,345.67", "###,###.##"), true, "validateMask is ###,###.##");
            equal($.wijinputcore.validateMask("+a2,345.67", "###,###.##"), false, "validateMask is ###,###.##");
            equal($.wijinputcore.validateMask("Abcdefg", ">L|LLLLLL"), true, "validateMask is >L|LLLLLL");
            equal($.wijinputcore.validateMask("abcdefg", ">L|LLLLLL"), false, "validateMask is >L|LLLLLL");
            equal($.wijinputcore.validateMask("aBCDEFG", "<L|LLLLLL"), true, "validateMask is <L|LLLLLL");
            equal($.wijinputcore.validateMask("ABCDEFG", "<L|LLLLLL"), false, "validateMask is <L|LLLLLL");
            equal($.wijinputcore.validateMask("あい-う", "??-?"), true, "validateMask is ??-?");
            equal($.wijinputcore.validateMask("あいう", "??-?"), false, "validateMask is ??-?");
            equal($.wijinputcore.validateMask("あい-う", "&&-&"), true, "validateMask is &&-&");
            equal($.wijinputcore.validateMask("あいう", "&&-&"), false, "validateMask is &&-&");
            equal($.wijinputcore.validateMask("亜家-う", "CC-C"), true, "validateMask is CC-C");
            equal($.wijinputcore.validateMask("亜家う", "CC-C"), false, "validateMask is CC-C");
            equal($.wijinputcore.validateMask("a-12", "A-AA"), true, "validateMask is A-AA");
            equal($.wijinputcore.validateMask("a12", "A-AA"), false, "validateMask is A-AA");
            equal($.wijinputcore.validateMask("a-12", "a-aa"), true, "validateMask is a-aa");
            equal($.wijinputcore.validateMask("a12", "a-aa"), false, "validateMask is a-aa");
            equal($.wijinputcore.validateMask("2013/01/12 12:12:12", "9999/99/99 00:00:00"), true, "validateMask is 9999/99/99 00:00:00");
            equal($.wijinputcore.validateMask("ab13/01/12 12:12:12", "9999/99/99 00:00:00"), false, "validateMask is 9999/99/99 00:00:00");
            equal($.wijinputcore.validateMask("¥123.45", "$999.99", {
                culture: "ja-JP"
            }), true, "validateMask is $999.99");
            equal($.wijinputcore.validateMask("12345", "$999.99", {
                culture: "ja-JP"
            }), false, "validateMask is $999.99");
            equal($.wijinputcore.validateMask("A123", "\AAAA"), true, "validateMask is \AAAA");
            equal($.wijinputcore.validateMask("B123", "\AAAA"), false, "validateMask is \AAAA");
            equal($.wijinputcore.validateMask("ｱ-bc", "H-HH"), true, "validateMask is H-HH");
            equal($.wijinputcore.validateMask("あｂｃ", "H-HH"), false, "validateMask is H-HH");
            equal($.wijinputcore.validateMask("ｱ-ｲｳ", "K-KK"), true, "validateMask is K-KK");
            equal($.wijinputcore.validateMask("あ-いう", "K-KK"), false, "validateMask is K-KK");
            equal($.wijinputcore.validateMask("１-２３", "９-９９"), true, "validateMask is ９-９９");
            equal($.wijinputcore.validateMask("123", "９-９９"), false, "validateMask is ９-９９");
            equal($.wijinputcore.validateMask("ア-イウ", "Ｋ-ＫＫ"), true, "validateMask is Ｋ-ＫＫ");
            equal($.wijinputcore.validateMask("あ-いう", "Ｋ-ＫＫ"), false, "validateMask is Ｋ-ＫＫ");
            equal($.wijinputcore.validateMask("あ-いう", "Ｊ-ＪＪ"), true, "validateMask is Ｊ-ＪＪ");
            equal($.wijinputcore.validateMask("ア-イウ", "Ｊ-ＪＪ"), false, "validateMask is Ｊ-ＪＪ");
            equal($.wijinputcore.validateMask("ア-ｂｃ", "Ｚ-ＺＺ"), true, "validateMask is Ｚ-ＺＺ");
            equal($.wijinputcore.validateMask("ｱ-bc", "Ｚ-ＺＺ"), false, "validateMask is Ｚ-ＺＺ");
            equal($.wijinputcore.validateMask("ｷ-ﾕ", "N-N"), true, "validateMask is N-N");
            equal($.wijinputcore.validateMask("き-ゅ", "N-N"), false, "validateMask is N-N");
            equal($.wijinputcore.validateMask("キ-ユ", "Ｎ-Ｎ"), true, "validateMask is Ｎ-Ｎ");
            equal($.wijinputcore.validateMask("き-ゅ", "Ｎ-Ｎ"), false, "validateMask is Ｎ-Ｎ");
            equal($.wijinputcore.validateMask("き-ゆ", "Ｇ-Ｇ"), true, "validateMask is Ｇ-Ｇ");
            equal($.wijinputcore.validateMask("き-ゅ", "Ｇ-Ｇ"), false, "validateMask is Ｇ-Ｇ");
        });
        QUnit.module("wijinput: text format keywords");
        test("keywords", function () {
            equal($.wijinputcore.validateText("ＡＢＣ", "Ａ"), true, "validateText is Ａ");
            equal($.wijinputcore.validateText("123", "Ａ"), false, "validateText is Ａ");
            equal($.wijinputcore.validateText("ABC", "A"), true, "validateText is A");
            equal($.wijinputcore.validateText("123", "A"), false, "validateText is A");
            equal($.wijinputcore.validateText("ａｂｃ", "ａ"), true, "validateText is ａ");
            equal($.wijinputcore.validateText("123", "ａ"), false, "validateText is ａ");
            equal($.wijinputcore.validateText("abc", "a"), true, "validateText is A");
            equal($.wijinputcore.validateText("123", "a"), false, "validateText is A");
            equal($.wijinputcore.validateText("アイウ", "Ｋ"), true, "validateText is Ｋ");
            equal($.wijinputcore.validateText("ｱｲｳ", "Ｋ"), false, "validateText is Ｋ");
            equal($.wijinputcore.validateText("ｱｲｳ", "K"), true, "validateText is K");
            equal($.wijinputcore.validateText("アイウ", "K"), false, "validateText is K");
            equal($.wijinputcore.validateText("１２３４５６", "９"), true, "validateText is ９");
            equal($.wijinputcore.validateText("123456", "９"), false, "validateText is ９");
            equal($.wijinputcore.validateText("123456", "9"), true, "validateText is 9");
            equal($.wijinputcore.validateText("１２３４５６", "9"), false, "validateText is 9");
            equal($.wijinputcore.validateText("＋１２，３４５．６７", "＃"), true, "validateText is ＃");
            equal($.wijinputcore.validateText("＋ａ２，３４５．６７", "＃"), false, "validateText is ＃");
            equal($.wijinputcore.validateText("+12,345.67", "#"), true, "validateText is #");
            equal($.wijinputcore.validateText("+a2,345.67", "#"), false, "validateText is #");
            equal($.wijinputcore.validateText("＠＃＄％＾", "＠"), true, "validateText is ＠");
            equal($.wijinputcore.validateText("ａ＠＃＄％＾", "＠"), false, "validateText is ＠");
            equal($.wijinputcore.validateText("@#$%^", "@"), true, "validateText is @");
            equal($.wijinputcore.validateText("a@#$%^", "@"), false, "validateText is @");
            equal($.wijinputcore.validateText("あいう", "Ｊ"), true, "validateText is Ｊ");
            equal($.wijinputcore.validateText("アイウ", "Ｊ"), false, "validateText is Ｊ");
            equal($.wijinputcore.validateText("アｂｃ", "Ｚ"), true, "validateText is Ｚ");
            equal($.wijinputcore.validateText("ｱbc", "Ｚ"), false, "validateText is Ｚ");
            equal($.wijinputcore.validateText("キユ", "Ｎ"), true, "validateText is Ｎ");
            equal($.wijinputcore.validateText("きゅ", "Ｎ"), false, "validateText is Ｎ");
            equal($.wijinputcore.validateText("ｷﾕ", "N"), true, "validateText is N");
            equal($.wijinputcore.validateText("きゅ", "N"), false, "validateText is N");
            equal($.wijinputcore.validateText("きゆ", "Ｇ"), true, "validateText is Ｇ");
            equal($.wijinputcore.validateText("きゅ", "Ｇ"), false, "validateText is Ｇ");
            equal($.wijinputcore.validateText("𢰝𣇄𢦏", "Ｔ"), true, "validateText is Ｔ");
            equal($.wijinputcore.validateText("亜家う", "Ｔ"), false, "validateText is Ｔ");
            equal($.wijinputcore.validateText("亜家う", "Ｄ"), true, "validateText is Ｄ");
            equal($.wijinputcore.validateText("𢰝𣇄𢦏", "Ｄ"), false, "validateText is Ｄ");
            equal($.wijinputcore.validateText("　", "Ｓ"), true, "validateText is Ｄ");
            equal($.wijinputcore.validateText(" ", "Ｓ"), false, "validateText is Ｄ");
            equal($.wijinputcore.validateText(" ", "S"), true, "validateText is Ｄ");
            equal($.wijinputcore.validateText("　", "S"), false, "validateText is Ｄ");
            equal($.wijinputcore.validateText("AAA", "\A"), true, "validateText is \A");
            equal($.wijinputcore.validateText("ABC", "\A"), false, "validateText is \A");
            equal($.wijinputcore.validateText("AB12", "A9"), true, "validateText is A9");
            equal($.wijinputcore.validateText("ab12", "A9"), false, "validateText is A9");
            equal($.wijinputcore.validateText("ABab", "^9"), true, "validateText is A9");
            equal($.wijinputcore.validateText("ab12", "^9"), false, "validateText is A9");
        });
    })(wijmo.tests || (wijmo.tests = {}));
    var tests = wijmo.tests;
})(wijmo || (wijmo = {}));
