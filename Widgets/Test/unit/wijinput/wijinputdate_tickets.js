(function ($) {
	module("wijinputdate:tickets");

	test("test issue #41123 for wijinputdate", function () {
		var inputdate = $("<input type='text'/>").appendTo("body")
			, triggerBtn
			, dropdown;

		inputdate.wijinputdate({
			dateFormat: 'yyyy/M/d',
			comboItems: [{ label: '1980/4/8', value: new Date(1980, 3, 8) }, { label: '2007/12/25', value: new Date(2007, 11, 25) }, { label: 'today', value: new Date()}],
			showTrigger: true
		});

		triggerBtn = $(".wijmo-wijinput-trigger");

		triggerBtn.simulate("click");

		dropdown = $(".wijmo-wijlist-ul");
		$(dropdown.children()[0]).simulate("click");

		ok(document.activeElement === inputdate[0], "After selecting item from drop down, the input element get focus.");
		inputdate.remove();
	});
	
	test("test issue #41407 for wijinputdate", function () {
		var inputdate1 = $("<input type='text'/>").appendTo("body");
		var inputdate2 = $("<input type='text'/>").appendTo("body");
		var date = new Date(), 
			previousDate = new Date(),
			nextDate = new Date();

		previousDate.setDate(date.getDate() - 1);
		nextDate.setDate(date.getDate() + 1);
		inputdate1.wijinputdate({
			date: date,
			showTrigger: true
		});
		inputdate2.wijinputdate({
			date: date,
			showTrigger: true
		});

		inputdate1.wijinputdate("option", "minDate", nextDate);
		ok(inputdate1.wijinputdate("option", "date") - nextDate === 0, "set minDate option is ok.");
		inputdate2.wijinputdate("option", "maxDate", previousDate);
		ok(inputdate2.wijinputdate("option", "date") - previousDate === 0, "set maxDate option is ok.");
		
		inputdate1.remove();
		inputdate2.remove();
	});

	test("#190838", function () {
		var inputdate = $("<input type='text'/>").appendTo("body");

		inputdate.wijinputdate({
			date: new Date(2016, 4, 31, 17, 0, 0),
			dateFormat: 'hh:mm tt',
			displayFormat: 'hh:mm tt',
			showDropDownButton: true,
			pickers: {
				timePicker: {
					format: "h:mm tt"
				}
			}
		});

		inputdate.simulate("keypress", { keyCode: 49 }).simulate("keypress", { keyCode: 50 });
		ok(inputdate.wijinputdate("option", "text") == "12:00 PM");

		inputdate.remove();
	});

	test("#202016", function () {
		var inputdate = $("<input type='text' value='08/03/2016 12:00:00 AM' style='width:500px' />").appendTo("body");
		inputdate.wijinputdate({
			dateFormat: 'U'
		});
		ok(inputdate.wijinputdate("option", "text").replace(/\s+/g, '') == "Wednesday,August03,201612:00:00AM");
		inputdate.remove();
	});

})(jQuery)