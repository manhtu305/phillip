﻿function load() {

   
}

$(function () {
    var widgetType = "wijinputdate";

    setTimeout(function () {
        $(".tabs").wijtabs().on("tabsactivate", function (event, ui) {
        });
    }, 0);

    function BuildPropertyPage()
    {
        var optionsTable = document.createElement("table");
        var tbody = document.createElement("tbody");
        optionsTable.appendChild(tbody);
        BuildOptions(tbody, $("#textbox1"));

        var methodsTable = document.createElement("table");
        var tbody = document.createElement("tbody");
        methodsTable.appendChild(tbody);
        BuildMethod(tbody, $("#textbox1"));

        var eventsTable = document.createElement("table");
        var tbody = document.createElement("tbody");
        eventsTable.appendChild(tbody);
        BuildEvent(tbody, $("#textbox1"));

        optionsTable.id = "divOptions";
        methodsTable.id = "divMethod";
        eventsTable.id = "divEvent";

        optionsTable.className = "tabs";
        methodsTable.className = "tabs";
        eventsTable.className = "tabs";

        var tabContainer = document.createElement("div");
        tabContainer.className = "tabs";
        document.body.appendChild(tabContainer);
        
        tabContainer.appendChild(BuildTab());
        tabContainer.appendChild(optionsTable);
        tabContainer.appendChild(methodsTable);
        tabContainer.appendChild(eventsTable);
    }

    var boolType = Boolean;

    var buttonAlignType = { _type: "enum", _data: ["right", "left"] };

    var comboItemsData = [
        { label: '1980/4/8', value: new Date(1980,3,8) },
        { label: '2007/12/25', value: new Date(2007, 11, 25) },
    ];

    var typeType = { _type: "enum", _data: ["numeric", "percent", "currency"] };

    var ImeModeType = { _type: "enum", _data: ["auto", "active", "inactive", "disabled"] };

    var ExitOnLeftRightKeyType = { _type: "enum", _data: ["none", "left", "right", "both"] };

    var SpinnerAlignType = { _type: "enum", _data: ["verticalRight", "verticalLeft", "horizontalDownLeft", "horizontalUpLeft"] };

    var TabActionType = { _type: "enum", _data: ["field", "control"] };

	var HighlightTextType = { _type: "enum", _data: ["field", "all"] };
    
	var anyType = {};

    var stringType = {};
	
	var nullableType = {};

    var dateType = {};
    
	var jsType = {};

	var pickersType = {};
	
    var comboItemsType = {};

    var callBackType = function() {
        alert("call back");
    };

    var rangeType = { _min: 1, _max: 100 };
    
    var cssLengthType = { _min: 1, _max: 100 };

    var positionType = {};

    var formatType = {};

    var funcType = function(needOutput, parametersObj) {
        this.needOutput = !!needOutput;
        this.parametersObj = parametersObj ? parametersObj : null;
    };

    var paramType = function(name, type) {
        this.name = name;
        this.type = type;
    };
    
    var formatArray = new Array()
    formatArray[0] = "d|M/d/yyyy";
    formatArray[1] = "D|dddd,MMMM dd,yyyy";
    formatArray[2] = "f|dddd,MMMM dd,yyyy h:mm tt";
    formatArray[3] = "F|dddd,MMMM dd,yyyy h:mm:ss tt";
    formatArray[4] = "g|M/d/yyyy h:mm tt";
    formatArray[5] = "G|M/d/yyyy h:mm:ss tt";
    formatArray[6] = "m|MMMM dd";
    formatArray[7] = "s|yyyy'-'MM'-'dd'T'HH':'mm':'ss";
    formatArray[8] = "t|h:mm tt";
    formatArray[9] = "T|h:mm:ss tt";
    formatArray[10] = "u|yyyy'-'MM'-'dd'T'HH':'mm':'ss";
    formatArray[11] = "U|dddd,MMMM dd,yyyy h:mm:ss tt";
    formatArray[12] = "y|yyyy MMMM";
    formatArray[13] = "r|ddd, dd MMM yyyy HH:mm:ss GMT";
    formatArray[14] = "R|ddd, dd MMM yyyy HH:mm:ss GMT";


    var widgetOptions = {
        autoNextField : boolType,
        hideEnter : boolType,
        disabled : boolType,
        showSpinner : boolType,
		showTrigger : boolType,
        showDropDownButton : boolType,
        smartInputMode : boolType,
        readonly : boolType,
        midnightAs0 : boolType,
        hour12As0: boolType,
        allowSpinLoop: boolType,
        blurOnLastChar: boolType,
		highlightText: HighlightTextType,
		increment : stringType,
        imeMode: ImeModeType,
        blurOnLeftRightKey: ExitOnLeftRightKeyType,
        tabAction: TabActionType,
        spinnerAlign: SpinnerAlignType,
        dropDownButtonAlign: buttonAlignType,
        popupPosition: positionType,
        activeField: rangeType,
        startYear : rangeType,
        keyDelay : rangeType,
        culture: stringType,
        cultureCalendar: stringType,
        amDesignator: stringType,
        pmDesignator: stringType,
		promptChar: stringType,
        date: dateType,
        maxDate: dateType,
        minDate: dateType,
        invalidClass: stringType,
        placeholder: stringType,
        dateFormat: formatType,
        displayFormat: formatType,
		pickers : pickersType
    };

    var widgetMethods = {
        spinUp: new funcType(false, null),
        spinDown: new funcType(false, null),
        drop: new funcType(false, null),
        destroy: new funcType(false, null),
        focus: new funcType(false, null),
        getPostValue: new funcType(true, null),
        getText: new funcType(true, null),
        isDestroyed: new funcType(true, null),
        isFocused: new funcType(true, null),
        setText: new funcType(false, [new paramType('text', 'string')]),
        widget: new funcType(true, null),
        selectText: new funcType(false, [new paramType('start', 'number'), new paramType('end', 'number')]),
        getSelectedText: new funcType(true, null)
    };

    var widgetEvents = {
        dateChanged: "e,data",
        initialized: "e",
        initializing: "e",
        invalidInput: "e,data",
        textChanged: "e,data",
        keyExit: "e",
        valueBoundsExceeded: "e",
        spinDown: "e",
        spinUp: "e",
        dropDownOpen: "e",
        dropDownClose: "e",
        dropDownButtonMouseDown: "e",
        dropDownButtonMouseUp: "e"
    };
    
    function IsNewFeature(name) { 
        if(name == "amDesignator"
        || name == "pmDesignator"
        || name == "displayFormat"
        || name == "blurOnLastChar"
        || name == "blurOnLeftRightKey"
        || name == "hour12As0"
        || name == "imeMode"
        || name == "midnightAs0"
        || name == "tabAction"
        || name == "spinnerAlign"
        || name == "allowSpinLoop"
        || name == "drop"
        || name == "spinDown"
        || name == "spinUp"
        || name == "dropDownOpen"
        || name == "dropDownClose"
        || name == "showDropDownButton"
        || name == "spinDown"
        || name == "spinUp"
        || name == "valueBoundExceeded"
		|| name == "pickers"
        || name === "getSelectedText"
        || name === "keyExit"
        ) {
            return true;
        }

        return false;
    };

	function IsNullAble(name) { 
        if(name == "date"
        || name == "minDate"
        || name == "maxDate"
        || name == "placeholder"
        ) {
            return true;
        }

        return false;
    };
	
    function BuildTab() {
        var ul = document.createElement("ul");
        var liOption = document.createElement("li");
        var liMethod = document.createElement("li");
        var liEvent = document.createElement("li");

        var optionA = document.createElement("a");
        var methodA = document.createElement("a");
        var eventA = document.createElement("a");

        ul.appendChild(liOption);
        ul.appendChild(liMethod);
        ul.appendChild(liEvent);

        liOption.appendChild(optionA);
        liMethod.appendChild(methodA);
        liEvent.appendChild(eventA);
        
        optionA.href = "#divOptions";
        optionA.appendChild(document.createTextNode("Options"));

        methodA.href = "#divMethod";
        methodA.appendChild(document.createTextNode("Method"));

        eventA.href = "#divEvent";
        eventA.appendChild(document.createTextNode("Event"));

        return ul;
    };
    
    function BuildOptions(tbody, jqueryObj) {
        var counter = 0;
        for (var propertyName in widgetOptions) {
            if (counter % 3 === 0 || propertyName == "pickers") {
                var tr = document.createElement("tr");
                tbody.appendChild(tr);
            }

            counter++;
            var td = document.createElement("td");
            td.className = "inputTh";
            //td.style.backgroundColor = "lightblue";
            //td.style.width = "160px";
            //td.style.textAlign = "center";
            tr.appendChild(td);

            if (propertyName == "create" || propertyName == "comboItems") {
                td.appendChild(document.createTextNode(propertyName));
            }
            else if(propertyName == "pickers")
            {
                var table = $("<table/>")
                    .css({ "width" : "100%" })
                    .appendTo(td);

                var tr2 = $("<tr>").appendTo(table);
                var td3 = $("<td>")
                .css({ "width": "100%" })
                .attr({ "colspan": 2 })
                .appendTo(tr2);
                
                var optionBtn = $("<button>")
                    .addClass("myButton")
                    .css({ "color" : "blue", "width" : "100%" })
                    .appendTo(td3)
                    .append(document.createTextNode(propertyName));

                var tr1 = $("<tr>").appendTo(table);

                var td1 = $("<td>")
                    .css({ "width" : "50%" })
                    .appendTo(tr1);

                var td2 = $("<td>")
                    .css({ "width" : "50%" })
                    .appendTo(tr1);

                var setBtn = $("<button>")
                    .css({ "color":"blue", "width":"100%"})
                    .addClass("myButton")
                    .appendTo(td2)
                    .append(document.createTextNode("Set"))
                    .bind('click', function () {
                        var js = "var picker = " + getDatePickersString();
						
					    try
						{
							eval(js);
							jqueryObj.wijinputdate("option", propertyName, picker);
						}
						catch(e)
						{
							alert(e.message);
						} 
                    });
                
                var getBtn = $("<button>")
                    .addClass("myButton")
                    .css({ "color": "blue", "width": "100%" })
                    .appendTo(td1)
                    .append(document.createTextNode("Get"))
                    .bind('click', function () {
                        getOptionValue(widgetOptions[propertyName], propertyName, jqueryObj);
                    });
            }
            else
            {
                var optionBtn = $("<button>")
                    .addClass("myButton")
                    .append(document.createTextNode(propertyName));
              
                if(IsNewFeature(propertyName)) {
                    optionBtn.css("color", "blue");
                }
                addOptionBtnHandler(widgetOptions[propertyName], propertyName, jqueryObj, optionBtn);

                if(IsNullAble(propertyName))
                {
                    var table = $("<table/>")
                        .css({ "width" : "100%" })
                        .appendTo(td);
                    
                    var tr1 = $("<tr>").appendTo(table);

                    var td1 = $("<td>")
                        .css({ "width" : "50%" })
                        .appendTo(tr1);

                    var td2 = $("<td>")
                        .css({ "width" : "50%" })
                        .appendTo(tr1);
                    
                    var setBtn = $("<button>")
                        .css({ "width":"100%"})
                        .addClass("myButton")
                        .appendTo(td2)
                        .attr("optionsName", propertyName)
                        .append(document.createTextNode("Set null"))
                        .bind('click', function (e)
                        {
                            var name = e.currentTarget.attributes["optionsname"].value;
                            jqueryObj.wijinputdate("option", name, null);
                            getOptionValue(widgetOptions[name], name, jqueryObj);
                        });
                    
                    optionBtn
                        .appendTo(td1)
                        .css({ "width": "100%" })
                }
                else
                {
                    optionBtn.appendTo(td);
                }

            }


            

            var tdEdit = document.createElement("td");
            //tdEdit.style.backgroundColor = "lightgreen";
            //tdEdit.style.width = "160px";
            tdEdit.className = "inputTd";
            tr.appendChild(tdEdit);
            
            var editControl = createEditControls(widgetOptions[propertyName], propertyName, jqueryObj);
            if (editControl) {
                editControl.id = propertyName + "_Editor";
				if(propertyName == "pickers"){
					tdEdit.setAttribute("colspan",5);
				}
                tdEdit.appendChild(editControl);
            }
        }
 
        function createEditControls(type, propertyName, obj) {
            if (type === Boolean) {
                var input = $("<input>");
                input.attr("type", "checkbox");
                input.bind("change", function () {
                    obj[widgetType]("option", propertyName, this.checked);
                });
                return input.get(0);
            }
            
            if (type._type && type._type === 'enum') {
                var options = $("<select>");
                options.css("width", "150px");
                for (var i = 0; i < type._data.length; i++) {
                    var opt = document.createElement("option");
                    opt.value = type._data[i];
                    opt.appendChild(document.createTextNode(type._data[i]));
                    options.get(0).appendChild(opt);
                }
                options.bind("change", function () {
                    obj[widgetType]("option", propertyName, this[this.selectedIndex].text);
                });
                return options.get(0);
            }
            if (type === cssLengthType) {
                var input = $("<input>");
                input.attr("type", "range");
                input.attr("min", cssLengthType._min);
                input.attr("max", cssLengthType._max);
                input.bind("change", function () {
                    obj[widgetType]("option", propertyName, this.value + "px");
                });
                return input.get(0);
            }
            if(type === rangeType) {
                var input = $("<input>");
                input.bind("change", function () {
                    obj[widgetType]("option", propertyName, parseFloat(this.value));
                });
                return input.get(0);
            }
            if (type === stringType) {
                var input = $("<input>");
                input.attr("type", "text");
                input.bind("change", function () {
                    obj[widgetType]("option", propertyName, this.value);
                });
                return input.get(0);
            }
			if(type === pickersType){
			    var input = $("<textarea id= 'pickerArea'>")
			        .css({ "width": "100%", "height": "150px" });

			    var text = getDefaultDatePickersString();
			    input.val(text);
                return input.get(0);
			}
            if (type === formatType) {
                
                var mainTable = $("<table>");

                var inputMainTr = $("<tr>");
                var inputMainTd = $("<td>");

                var buttonMainTr = $("<tr>");
                var buttonMainTd = $("<td>");

                inputMainTr.append(inputMainTd);
                mainTable.append(inputMainTr);

                buttonMainTr.append(buttonMainTd);
                mainTable.append(buttonMainTr);

                var inputID = propertyName + "_FormatTextBox";
                var inputTable = $("<table>");
                var inputTr = $("<tr>");
                var inputTd = $("<td>");
                var input = $("<input>");
                input.attr("type", "text");
                input.attr("id", inputID);
                input.bind("change", function (sender) {
                    obj[widgetType]("option", propertyName, $("#" + inputID).val());
                });
                
                inputTd.append(input);
                inputTr.append(inputTd);
                inputTable.append(inputTr);
                inputMainTd.append(inputTable);
                
                var buttonTable = $("<table>");
                inputMainTd.append(buttonTable);

                var buttonTr = null;
                for (var index = 0; index < formatArray.length; index++)
                {
                    var pattern = formatArray[index].split("|")[0];
                    var title = formatArray[index].split("|")[1];
                    
                    if (index % 5 == 0)
                    {
                        var buttonTr = $("<tr>");
                        buttonTable.append(buttonTr);
                    }
                    
                    var button = $("<input>");
                    button.attr("type", "button");
                    button.attr("value", pattern);
                    button.attr("title", title);
                    button.css("width", "30px")

                    button.bind("click", function (sender)
                    {
                        $("#" + inputID).val(sender.currentTarget.value);
                        obj[widgetType]("option", propertyName, $("#" + inputID).val());
                    });
                    var td = $("<td>");

                    td.append(button);
                    buttonTr.append(td);
                }

                return mainTable.get(0);
            }
            if (type === dateType) {
                var input = $("<input>");
                
                input.attr("type", "text");
                input.attr("value", "2013/01/01 00:00:00");
                input.bind("change", function () {
                    var date = Date.parse(this.value);
                    var value = new Date(date);
                    obj[widgetType]("option", propertyName, value);
                });
                return input.get(0);
            }
            if (type === positionType) {
                var input = $("<input>");
                input.attr("type", "text");
                input.attr("value", "0 0");
                input.bind("change", function () {
                    var value = this.value.split(" ");
                    if(value.length >= 2) {
                        var newValue = {offset: value[0]+ " " + value[1]};
                    }
                    obj[widgetType]("option", propertyName, newValue);
                });
                return input.get(0);
            }
            if (type === anyType) {
                var input = $("<input>");
                input.attr("type", "text");
                input.bind("change", function () {
                    obj[widgetType]("option", propertyName, this.value);
                });
                return input.get(0);
            }
            if (type === comboItemsData) {
                setTimeout(function() {
                    obj[widgetType]("option", propertyName, comboItemsData);
                }, 0);
                return null;
            }
            if (type === callBackType) {
                setTimeout(function () {
                    obj[widgetType]("option", propertyName, callBackType);
                }, 0);
                return null;
            }
            return null;
        };
        
        function addOptionBtnHandler(type, propertyName, obj, btn) {
            btn.bind('click', function () {
                getOptionValue(type, propertyName, obj);
            });
        }
        
        function getOptionValue(type, propertyName, obj) {
            var editor = $("#" + propertyName + "_Editor");
            var value = obj[widgetType]("option", propertyName);

            if (value === null) {
                value = "null";
            }
            
            if (value === "") {
                value = "empty";
            }

            if (value.offset) {
                value = value.offset;
            }

            if (value instanceof Date) {
                value = value.getFullYear() + "/" + (value.getMonth()+1) + "/" + value.getDate() + " " + value.getHours() + ":" + value.getMinutes() + ":" + value.getSeconds();
            }
            
            if (editor.attr("type") == "checkbox") {
                editor.attr("checked", value === true);
                return;
            }

            if(propertyName == "dateFormat")
            {
                $("#dateFormat_FormatTextBox").val(value);
            }
            else if(propertyName == "displayFormat")
            {
                $("#displayFormat_FormatTextBox").val(value);
            }
            else if(propertyName == "pickers")
            {
                editor.val(getDefaultDatePickersString());
            }
            else
            {
                editor.val(value);
            }
        }
    };

    function getDefaultDatePickersString()
    {
        var text = "{\r\n" +
                            "   width           :       undefined,\r\n" +
                            "   height          :       undefined,\r\n" +
                            "   datePicker  :   {   format      :       undefined   }, \r\n" +
                            "   timePicker  :   {   format      :       undefined   }, \r\n" +
                            "   list               :   [    '1980/4/8' ,  '2007/12/25',  '2017/3/1' ], \r\n" + 
                            "   calendar     :    {}\r\n" +
                            "}";

        return text;
    }

    function getDatePickersString()
    {
        return $("#pickers_Editor").val();
    }

    function BuildMethod(tbody, jqueryObj) {
        var counter = 0;
        for (var methodName in widgetMethods) {
            if (counter % 2 === 0) {
                var tr = document.createElement("tr");
                tbody.appendChild(tr);
            }

            counter++;
            var td = document.createElement("td");
            td.className = "inputTh";
            //td.style.backgroundColor = "lightblue";
            //td.style.width = "160px";
            //td.style.textAlign = "center";
            tr.appendChild(td);

            var callBtn = $("<button>");
            callBtn.addClass("myButton");
            if (IsNewFeature(methodName)) {
                callBtn.css("color", "blue");
            }
            //callBtn.css("width", "150px");
            callBtn.append(document.createTextNode(methodName));
            td.appendChild(callBtn.get(0));
            var tdEdit = document.createElement("td");
            tdEdit.className = "inputTd";
            //tdEdit.style.backgroundColor = "lightgreen";
            //tdEdit.style.width = "160px";
            tr.appendChild(tdEdit);
            var editControl = createEditControls(widgetMethods[methodName], methodName);
            if (editControl) {
                tdEdit.appendChild(editControl);
            }
            
            addCallBtnHandler(widgetMethods[methodName], methodName, jqueryObj, callBtn);
        }
        
        function createEditControls(type, methodName) {
            var contentDiv = document.createElement("div");
            if (type.parametersObj) {
                for (var index = 0; index < type.parametersObj.length; index++) {
                    var input = document.createElement('input');
                    input.id = methodName + "_param" + index;
                    contentDiv.appendChild(document.createTextNode(type.parametersObj[index].name + ': '));
                    contentDiv.appendChild(input);
                }
            }
            if (type.needOutput) {
                var output = document.createElement("input");
                output.disabled = 'disabled';
                output.id = methodName + "_output";
                contentDiv.appendChild(document.createTextNode('output: '));
                contentDiv.appendChild(output);
            }
            return contentDiv;
        };
        
        function addCallBtnHandler(type, methodName, obj, btn) {
            btn.bind('click', function () {
                setTimeout(function () {
                    doMethod(type, methodName, obj);
                }, 200);
                
            });
        }
        
        function doMethod(type, methodName, obj) {
            var params = [];
            if (type.parametersObj) {
                for (var index = 0; index < type.parametersObj.length; index++) {
                    var paramData = document.getElementById(methodName + "_param" + index).value;
                    if (type.parametersObj[index].type === 'number') {
                        paramData = parseFloat(paramData);
                    }
                    params.push(paramData);
                }
            }

            var result = '';
            switch (params.length) {
                case 0:
                    result = obj[widgetType](methodName);
                    break;
                case 1:
                    result = obj[widgetType](methodName, params[0]);
                    break;
                case 2:
                    result = obj[widgetType](methodName, params[0], params[1]);
                    break;
                case 3:
                    result = obj[widgetType](methodName, params[0], params[1], params[2]);
                    break;
                case 4:
                    result = obj[widgetType](methodName, params[0], params[1], params[2], params[3]);
                    break;
                default:
            }

            if (type.needOutput) {
                var output = document.getElementById(methodName + "_output");
                output.value = result;
            }
        }
    };
    
    function BuildEvent(tbody, jqueryObj) {
        var counter = 0;
        for (var eventName in widgetEvents) {
            if (counter % 2 === 0) {
                var tr = document.createElement("tr");
                tbody.appendChild(tr);
            }

            counter++;
            var td = document.createElement("td");
            td.className = "inputTh";
            //td.style.backgroundColor = "lightblue";
            //td.style.width = "160px";
            //td.style.textAlign = "center";
            tr.appendChild(td);

            var addBtn = $("<button>");
            addBtn.addClass("myButton");
            if (IsNewFeature(eventName)) {
                addBtn.css("color", "blue");
            }
            //addBtn.css("width", "150px");
            addBtn.append(document.createTextNode(eventName));
            td.appendChild(addBtn.get(0));

            var tdEventArea = document.createElement("td");
            //tdEventArea.style.backgroundColor = "lightgreen";
            //tdEventArea.style.width = "300px";
            //tdEventArea.style.height = "80px";
            tdEventArea.className = "inputTd";
            tr.appendChild(tdEventArea);

            var eventArea = document.createElement("textarea");
            //eventArea.style.width = "200px";
            eventArea.style.height = "65px";
            eventArea.className = "myTextArea";
            eventArea.id = eventName + "Content";
            
            var eventString = "function(" + widgetEvents[eventName] + ") \n{ \n    console.log(\"" + eventName + "\"); \n}";
            eventArea.appendChild(document.createTextNode(eventString));
            tdEventArea.appendChild(eventArea);

            addAddBtnHandler(widgetEvents[eventName], eventName, jqueryObj, addBtn);
        }
        

        function addAddBtnHandler(type, methodName, obj, btn) {
            btn.bind('click', function () {
                doEvent(type, methodName, obj);
            });
        }

        function doEvent(type, eventName, obj) {
            
            var eventFunction = document.getElementById(eventName + "Content").value;
            var fun = CheckFunction(eventFunction);
            obj[widgetType]("option", eventName, fun);
        }


        function Trim(value) {
            if (value == "") {
                return "";
            }

            var beginIndex = 0;
            var endIndex = 0;
            for (var i = 0; i < value.length; i++) {
                if (value.CharAt(i) != " " && value.CharAt(i) != "　") {
                    beginIndex = i;
                    break;
                }
            }

            for (var i = value.length - 1; i >= 0; i--) {
                if (value.CharAt(i) != " " && value.CharAt(i) != "　") {
                    endIndex = i + 1;
                    break;
                }
            }

            try {
                var s = value.Substring(beginIndex, endIndex);
                return s;
            }
            catch (e) {
                return value;
            }


        };

        function CheckFunction(fun) {
            if (fun === undefined || fun === null) {
                return null;
            }

            if (typeof fun == "string") {
                //fun = Trim(fun);
                if (fun.length == 0) {
                    return null;
                }
                try {
                    eval("fun =" + fun);
                }
                catch (e) {
                }
            }

            if (typeof fun == "function") {
                return fun;
            }
            else {
                throw "The value is not a valid function";
            }
        };

    };

    BuildPropertyPage();
});