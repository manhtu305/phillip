var wijmo;
(function (wijmo) {
    (function (tests) {
        var $ = jQuery;
        QUnit.module("wijinputnumber: tickets");
        test("#35993", function () {
            var $widget = $("<input type='text' value=0.0000/>").width(155).appendTo(document.body);
            $widget.wijinputnumber({
                decimalPlaces: 4
            });
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("0.25", 200, null, function () {
                ok($widget.val() == "0.2500", "value is ok");
                $widget.wijinputnumber('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("#40695", function () {
            var $widget = $("<input type='text' />").width(155).appendTo(document.body);
            $widget.wijinputnumber({
                minValue: -1,
                maxValue: 100,
                decimalPlaces: 2,
                nullText: "",
                showNullText: true
            });
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[HOME]-1", 200, null, function () {
                ok($widget.val() == "-1.00", "value is ok");
                $widget.remove();
                start();
            });
            stop();
        });
        test("shownulltext after BACKSPACE key pressing", function () {
            var $widget = tests.createInputNumber({
                showNullText: true,
                value: 0,
                nullText: "null",
                type: 'numeric',
                decimalPlaces: 0
            });
            ok($widget.val() === "0", "value setting ok");
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[BACKSPACE]", 200, null, function () {
                $widget.blur();
                window.setTimeout(function () {
                    ok($widget.val() === "null", "null text ok after deleting");
                    $widget.wijinputnumber('destroy');
                    $widget.remove();
                    start();
                }, 500);
            });
            stop();
        });
        test("shownulltext after DELETE key pressing", function () {
            var $widget = tests.createInputNumber({
                showNullText: true,
                value: 0,
                nullText: "null",
                type: 'numeric',
                decimalPlaces: 2
            });
            ok($widget.val() === "0.00", "value setting ok");
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[DELETE]", 200, null, function () {
                $widget.blur();
                window.setTimeout(function () {
                    ok($widget.val() === "null", "null text ok after deleting");
                    $widget.wijinputnumber('destroy');
                    $widget.remove();
                    start();
                }, 500);
            });
            stop();
        });
        test("test issue #41123 for wijinputnumber", function () {
            var $widget = tests.createInputNumber({
                comboItems: [
                    {
                        label: '100.12',
                        value: 100.12
                    }, 
                    {
                        label: '1200',
                        value: 1200
                    }, 
                    {
                        label: '2000',
                        value: 2000
                    }, 
                    {
                        label: '5200',
                        value: 5200
                    }
                ],
                showTrigger: true
            }), triggerBtn = $(".wijmo-wijinput-trigger"), dropdown;
            triggerBtn.simulate("click");
            dropdown = $(".wijmo-wijlist-ul");
            $(dropdown.children()[0]).simulate("click");
            ok(document.activeElement === $widget[0], "After selecting item from drop down, the input element get focus.");
            $widget.remove();
        });
        test("test issue #41713 for wijinputnumber", function () {
            var $widget = tests.createInputNumber({
                nullText: "test",
                showNullText: true,
                value: null
            });
            equal($widget.wijinputnumber("getText"), "test", "nulltext display.");
            $widget.wijinputnumber("option", "value", 123);
            equal($widget.wijinputnumber("getText"), "123.00", "the actual value display.");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("shownulltext after setting value option to null", function () {
            var $widget = tests.createInputNumber({
                showNullText: true,
                value: 1.23,
                nullText: "nullText",
                type: 'numeric',
                decimalPlaces: 2
            });
            ok($widget.val() === "1.23", "value setting ok");
            $widget.wijinputnumber('option', "value", null);
            ok($widget.val() === "nullText", "null text ok after setting vale option to null");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("#41616:shown null text after losting focus", function () {
            var $widget = tests.createInputNumber({
                value: null,
                showNullText: true,
                nullText: "nullText",
                type: 'numeric',
                decimalPlaces: 2
            });
            $widget.wijinputnumber('focus');
            setTimeout(function () {
                equal($widget.val(), "", "'' is displayed after getting focusing");
                $widget.blur();
                setTimeout(function () {
                    equal($widget.val(), "nullText", "null text is still displayed after bluring");
                    $widget.remove();
                    start();
                }, 200);
                stop();
                start();
            }, 200);
            stop();
        });
        test("#212574", function () {
            var ipt = tests.createInputNumber({
                'spinnerAlign': 'horizontalDownLeft',
                'showSpinner': true
            });
            equal(ipt.val(), '0.00');
            $('.ui-icon-plus').simulate('mousedown').simulate('mouseup');
            equal(ipt.val(), '1.00');
            ipt.wijinputnumber('option', 'spinnerAlign', 'horizontalUpLeft');
            $('.ui-icon-plus').simulate('mousedown').simulate('mouseup');
            equal(ipt.val(), '2.00');
            ipt.remove();
        });
    })(wijmo.tests || (wijmo.tests = {}));
    var tests = wijmo.tests;
})(wijmo || (wijmo = {}));
