﻿var wijmo;
(function (wijmo) {
    (function (tests) {
        var $ = jQuery;
        QUnit.module("wijinputmask: options");
        test("allowPromptAsInput", function () {
            var $widget = tests.createInputMask({
                mask: "999",
                resetOnPrompt: false,
                promptChar: "3"
            });
            ok($widget.wijinputmask("option", "allowPromptAsInput") == false, "allowPromptAsInput is default value");
            $widget.wijinputmask('focus');
            $widget.simulateKeyStroke("[HOME]3", null, function () {
                equal($widget.wijinputmask("getText"), "   ", "allowPromptAsInput is default value");
                $widget.wijinputmask("option", "allowPromptAsInput", true);
                ok($widget.wijinputmask("option", "allowPromptAsInput") == true, "allowPromptAsInput is true");
                $widget.simulateKeyStroke("[HOME]3", null, function () {
                    equal($widget.wijinputmask("getText"), "3  ", "allowPromptAsInput is true");
                    $widget.wijinputmask('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("culture", function () {
            var $widget = tests.createInputMask({
                mask: "$999.99",
                text: "12345"
            });
            ok($widget.wijinputmask("option", "culture") == "", "culture is default value");
            equal($widget.val(), "$123.45", "culture is default value");
            $widget.wijinputmask("option", "culture", "ja-JP");
            ok($widget.wijinputmask("option", "culture") == "ja-JP", "culture is ja-JP");
            equal($widget.val(), "¥123.45", "culture is default value");
            $widget.wijinputmask("option", "culture", "zh-CN");
            ok($widget.wijinputmask("option", "culture") == "zh-CN", "culture is zh-CN");
            equal($widget.val(), "¥123.45", "culture is default value");
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        test("placeholder", function () {
            var $widget = tests.createInputMask();
            ok($widget.wijinputmask("option", "placeholder") == null, "placeholder is default value");
            $widget.wijinputmask("option", "placeholder", "");
            ok($widget.wijinputmask("option", "placeholder") == "", "placeholder is string.empty");
            $widget.wijinputmask("option", "placeholder", "test");
            ok($widget.wijinputmask("option", "placeholder") == "test", "placeholder is test");
            equal($widget.wijinputmask("getText"), "test", "placeholder is test");
            $widget.wijinputmask("option", "placeholder", "あいうえお");
            ok($widget.wijinputmask("option", "placeholder") == "あいうえお", "placeholder is あいうえお");
            equal($widget.wijinputmask("getText"), "あいうえお", "placeholder is あいうえお");
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        test("hidePromptOnLeave", function () {
            var $widget = tests.createInputMask({
                mask: "000-0000"
            });
            ok($widget.wijinputmask("option", "hidePromptOnLeave") == false, "hidePromptOnLeave is default value");
            equal($widget.val(), "___-____", "hidePromptOnLeave is default value");
            $widget.wijinputmask("option", "hidePromptOnLeave", true);
            ok($widget.wijinputmask("option", "hidePromptOnLeave") == true, "hidePromptOnLeave is true");
            equal($widget.val(), "   -    ", "hidePromptOnLeave is true");
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        test("readonly", function () {
            var $widget = tests.createInputMask({
                mask: "9999"
            });
            ok($widget.wijinputmask("option", "readonly") == false, "readOnly is default value");
            $widget.wijinputmask('focus');
            $widget.simulateKeyStroke("[HOME]2001", null, function () {
                equal($widget.val(), "2001", "readOnly is default value");
                $widget.wijinputmask("option", "text", "");
                $widget.wijinputmask("option", "readonly", true);
                ok($widget.wijinputmask("option", "readonly") == true, "readOnly is true");
                $widget.simulateKeyStroke("[HOME]1999", null, function () {
                    equal($widget.val(), "____", "readOnly is true");
                    $widget.wijinputmask('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("mask, text", function () {
            var $widget = tests.createInputMask();
            $widget.wijinputmask("option", "mask", "000");
            $widget.wijinputmask("option", "text", "123");
            ok($widget.wijinputmask("option", "mask") == "000", "mask is 000");
            equal($widget.val(), "123", "mask is 000");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            equal($widget.val(), "___", "mask is 000");
            $widget.wijinputmask("option", "mask", "999");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "123");
            ok($widget.wijinputmask("option", "mask") == "999", "mask is 999");
            equal($widget.val(), "123", "mask is 999");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            equal($widget.val(), "___", "mask is 999");
            $widget.wijinputmask("option", "mask", "###");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "+-3");
            ok($widget.wijinputmask("option", "mask") == "###", "mask is ###");
            equal($widget.val(), "+-3", "mask is ###");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            equal($widget.val(), "___", "mask is ###");
            $widget.wijinputmask("option", "mask", "LLL");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            ok($widget.wijinputmask("option", "mask") == "LLL", "mask is LLL");
            equal($widget.val(), "abc", "mask is LLL");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいう");
            equal($widget.val(), "あいう", "mask is LLL");
            $widget.wijinputmask("option", "mask", "???");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            ok($widget.wijinputmask("option", "mask") == "???", "mask is ???");
            equal($widget.val(), "abc", "mask is ???");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいう");
            equal($widget.val(), "あいう", "mask is ???");
            $widget.wijinputmask("option", "mask", "&&&");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            ok($widget.wijinputmask("option", "mask") == "&&&", "mask is &&&");
            equal($widget.val(), "abc", "mask is &&&");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいう");
            equal($widget.val(), "あいう", "mask is LLL");
            $widget.wijinputmask("option", "mask", "CCC");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            ok($widget.wijinputmask("option", "mask") == "CCC", "mask is CCC");
            equal($widget.val(), "abc", "mask is CCC");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいう");
            equal($widget.val(), "あいう", "mask is CCC");
            $widget.wijinputmask("option", "mask", "AAA");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "a12");
            ok($widget.wijinputmask("option", "mask") == "AAA", "mask is AAA");
            equal($widget.val(), "a12", "mask is AAA");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいう");
            equal($widget.val(), "あいう", "mask is AAA");
            $widget.wijinputmask("option", "mask", "aaa");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "a12");
            ok($widget.wijinputmask("option", "mask") == "aaa", "mask is aaa");
            equal($widget.val(), "a12", "mask is aaa");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいう");
            equal($widget.val(), "あいう", "mask is aaa");
            $widget.wijinputmask("option", "mask", "999,999.00");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "12345678");
            ok($widget.wijinputmask("option", "mask") == "999,999.00", "mask is 999,999.00");
            equal($widget.val(), "123,456.78", "mask is 999,999.00");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abcdefgh");
            equal($widget.val(), "___,___.__", "mask is 999,999.00");
            $widget.wijinputmask("option", "mask", "$99.99");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "1234");
            ok($widget.wijinputmask("option", "mask") == "$99.99", "mask is $99.99");
            equal($widget.val(), "$12.34", "mask is $99.99");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abde");
            equal($widget.val(), "$__.__", "mask is $99.99");
            $widget.wijinputmask("option", "mask", ">L|LLLLLL");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            ok($widget.wijinputmask("option", "mask") == ">L|LLLLLL", "mask is >L|LLLLLL");
            equal($widget.val(), "Abc____", "mask is >L|LLLLLL");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "ABC");
            equal($widget.val(), "ABC____", "mask is >L|LLLLLL");
            $widget.wijinputmask("option", "promptChar", "*");
            equal($widget.val(), 'ABC****', "Prompt char is applied");
            $widget.wijinputmask("option", "promptChar", "_");
            $widget.wijinputmask("option", "mask", ">L<LLLLLL");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            ok($widget.wijinputmask("option", "mask") == ">L<LLLLLL", "mask is >L<LLLLLL");
            equal($widget.val(), "Abc____", "mask is $99.99");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "ABC");
            equal($widget.val(), "Abc____", "mask is >L<LLLLLL");
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        test("passwordChar", function () {
            var $widget = tests.createInputMask({
                mask: "LLLLLL",
                text: "abcdef"
            });
            ok($widget.wijinputmask("option", "passwordChar") == "", "passwordChar is default value");
            equal($widget.val(), 'abcdef', "passwordChar is default value");
            $widget.wijinputmask("option", "passwordChar", "*");
            ok($widget.wijinputmask("option", "passwordChar") == "*", "passwordChar is *");
            $widget.wijinputmask("option", "passwordChar", "あ");
            ok($widget.wijinputmask("option", "passwordChar") == "あ", "passwordChar is あ");
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        test("promptChar", function () {
            var $widget = tests.createInputMask({
                mask: "000-0000"
            });
            ok($widget.wijinputmask("option", "promptChar") == "_", "promptChar is default value");
            equal($widget.val(), "___-____", "promptChar is default value");
            $widget.wijinputmask("option", "promptChar", "a");
            ok($widget.wijinputmask("option", "promptChar") == "a", "promptChar is a");
            equal($widget.val(), "aaa-aaaa", "promptChar is default value");
            $widget.wijinputmask("option", "promptChar", "あ");
            ok($widget.wijinputmask("option", "promptChar") == "あ", "promptChar is あ");
            equal($widget.val(), "あああ-ああああ", "promptChar is default value");
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        test("resetOnPrompt", function () {
            var $widget = tests.createInputMask({
                mask: "999",
                allowPromptAsInput: true,
                promptChar: "3"
            });
            ok($widget.wijinputmask("option", "resetOnPrompt") == true, "resetOnPrompt is default value");
            $widget.wijinputmask('focus');
            $widget.simulateKeyStroke("[HOME]3", null, function () {
                equal($widget.wijinputmask("getText"), "   ", "resetOnPrompt is default value");
                $widget.wijinputmask("option", "resetOnPrompt", false);
                ok($widget.wijinputmask("option", "resetOnPrompt") == false, "resetOnPrompt is false");
                $widget.simulateKeyStroke("[HOME]3", null, function () {
                    equal($widget.wijinputmask("getText"), "3  ", "resetOnPrompt is false");
                    $widget.wijinputmask('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("resetOnSpace", function () {
            var $widget = tests.createInputMask({
                mask: "999"
            });
            ok($widget.wijinputmask("option", "resetOnSpace") == true, "resetOnSpace is default value");
            $widget.wijinputmask('focus');
            $widget.simulateKeyStroke("[HOME] ", null, function () {
                equal($widget.val(), "___", "resetOnPrompt is default value");
                $widget.wijinputmask("option", "resetOnSpace", false);
                ok($widget.wijinputmask("option", "resetOnSpace") == false, "resetOnSpace is false");
                $widget.simulateKeyStroke("[HOME] ", null, function () {
                    equal($widget.val(), " __", "resetOnSpace is false");
                    $widget.wijinputmask('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("skipLiterals", function () {
            var $widget = tests.createInputMask();
            ok($widget.wijinputmask("option", "skipLiterals") == true, "skipLiterals is default value");
            $widget.wijinputmask("option", "skipLiterals", false);
            ok($widget.wijinputmask("option", "skipLiterals") == false, "skipLiterals is false");
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        test("autoConvert", function () {
            var $widget = wijmo.tests.createInputMask({
                mask: "KKK"
            });
            ok($widget.wijinputmask("option", "autoConvert") == true, "autoConvert is true");
            $widget.wijinputmask("option", "text", "あいう");
            equal($widget.val(), "ｱｲｳ", "autoConvert is true");
            $widget.wijinputmask("option", "autoConvert", false);
            ok($widget.wijinputmask("option", "autoConvert") == false, "autoConvert is false");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あｳｲ");
            equal($widget.val(), "___", "autoConvert is false");
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        test("autoConvert for UI", function () {
            var $widget = wijmo.tests.createInputMask({
                mask: "９９９",
                autoConvert: false
            });
            $widget.wijinputmask('focus');
            $widget.simulateKeyStroke("[HOME]123", null, function () {
                equal($widget.val(), "___", "autoConvert is false");
                $widget.wijinputmask("option", "autoConvert", true);
                ok($widget.wijinputmask("option", "autoConvert") == true, "autoConvert is true");
                $widget.simulateKeyStroke("[HOME]456", null, function () {
                    equal($widget.val(), "４５６", "autoConvert is true");
                    $widget.wijinputmask('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("autoConvert for IM format", function () {
            var $widget = wijmo.tests.createInputMask({
                mask: /\K{3}/
            });
            ok($widget.wijinputmask("option", "autoConvert") == true, "autoConvert is true");
            $widget.wijinputmask("option", "text", "あいう");
            equal($widget.val(), "ｱｲｳ", "autoConvert is true");
            $widget.wijinputmask("option", "autoConvert", false);
            ok($widget.wijinputmask("option", "autoConvert") == false, "autoConvert is false");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あｳｲ");
            equal($widget.val(), "ｳｲ_", "autoConvert is false");
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        test("autoConvert for IM format UI", function () {
            var $widget = wijmo.tests.createInputMask({
                autoConvert: false,
                mask: /\Ｄ{3}/
            });
            $widget.wijinputmask('focus');
            $widget.simulateKeyStroke("[HOME]123", null, function () {
                equal($widget.val(), "___", "autoConvert is default value");
                $widget.wijinputmask("option", "autoConvert", true);
                ok($widget.wijinputmask("option", "autoConvert") == true, "autoConvert is true");
                $widget.simulateKeyStroke("[HOME]456", null, function () {
                    equal($widget.val(), "４５６", "autoConvert is true");
                    $widget.wijinputmask('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("blurOnLastChar", function () {
            var $widget = tests.createInputMask({
                mask: "000"
            });
            ok($widget.wijinputmask("option", "blurOnLastChar") == false, "blurOnLastChar is default value");
            $widget.wijinputmask('focus');
            $widget.simulateKeyStroke("[HOME]123", null, function () {
                equal($widget.wijinputmask('isFocused'), true, "blurOnLastChar is default value");
                $widget.wijinputmask("option", "text", "");
                $widget.wijinputmask("option", "blurOnLastChar", true);
                ok($widget.wijinputmask("option", "blurOnLastChar") == true, "blurOnLastChar is true");
                $widget.simulateKeyStroke("[HOME]123", null, function () {
                    equal($widget.wijinputmask('isFocused'), false, "blurOnLastChar is false");
                    $widget.wijinputmask('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("blurOnLeftRightKey is none", function () {
            var $widget = tests.createInputMask({
                mask: "000"
            });
            ok($widget.wijinputmask("option", "blurOnLeftRightKey") == "none", "blurOnLeftRightKey is default value");
            $widget.wijinputmask('focus');
            $widget.simulateKeyStroke("[HOME][LEFT]", null, function () {
                equal($widget.wijinputmask('isFocused'), true, "blurOnLeftRightKey is default value");
                $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                    equal($widget.wijinputmask('isFocused'), true, "blurOnLeftRightKey is default value");
                    $widget.wijinputmask('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("blurOnLeftRightKey is right", function () {
            var $widget = tests.createInputMask({
                mask: "000"
            });
            $widget.wijinputmask("option", "blurOnLeftRightKey", "right");
            ok($widget.wijinputmask("option", "blurOnLeftRightKey") == "right", "blurOnLeftRightKey is right");
            $widget.wijinputmask('focus');
            $widget.simulateKeyStroke("[HOME][LEFT]", null, function () {
                equal($widget.wijinputmask('isFocused'), true, "blurOnLeftRightKey is right");
                $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                    equal($widget.wijinputmask('isFocused'), false, "blurOnLeftRightKey is right");
                    $widget.wijinputmask('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("blurOnLeftRightKey is left", function () {
            var $widget = tests.createInputMask({
                mask: "000"
            });
            $widget.wijinputmask("option", "blurOnLeftRightKey", "left");
            ok($widget.wijinputmask("option", "blurOnLeftRightKey") == "left", "blurOnLeftRightKey is left");
            $widget.wijinputmask('focus');
            $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                equal($widget.wijinputmask('isFocused'), true, "blurOnLeftRightKey is left");
                $widget.wijinputmask('focus');
                $widget.simulateKeyStroke("[HOME][LEFT]", null, function () {
                    equal($widget.wijinputmask('isFocused'), false, "blurOnLeftRightKey is left");
                    $widget.wijinputmask('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("blurOnLeftRightKey is both", function () {
            var $widget = tests.createInputMask({
                mask: "000"
            });
            $widget.wijinputmask("option", "blurOnLeftRightKey", "both");
            ok($widget.wijinputmask("option", "blurOnLeftRightKey") == "both", "blurOnLeftRightKey is default value");
            $widget.wijinputmask('focus');
            $widget.simulateKeyStroke("[HOME][LEFT]", null, function () {
                equal($widget.wijinputmask('isFocused'), false, "blurOnLeftRightKey is default value");
                $widget.wijinputmask('focus');
                $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                    equal($widget.wijinputmask('isFocused'), false, "blurOnLeftRightKey is both");
                    $widget.wijinputmask('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("new mask, text", function () {
            var $widget = tests.createInputMask();
            $widget.wijinputmask("option", "mask", "HHH");
            $widget.wijinputmask("option", "text", "123");
            ok($widget.wijinputmask("option", "mask") == "HHH", "mask is HHH");
            equal($widget.val(), "123", "mask is HHH");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            equal($widget.val(), "abc", "mask is HHH");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいう");
            equal($widget.val(), "ｱｲｳ", "mask is HHH");
            $widget.wijinputmask("option", "mask", "KKK");
            $widget.wijinputmask("option", "text", "ｱｲｳ");
            ok($widget.wijinputmask("option", "mask") == "KKK", "mask is KKK");
            equal($widget.val(), "ｱｲｳ", "mask is KKK");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            equal($widget.val(), "___", "mask is KKK");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "うえお");
            equal($widget.val(), "ｳｴｵ", "mask is KKK");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "アイウ");
            equal($widget.val(), "ｱｲｳ", "mask is KKK");
            $widget.wijinputmask("option", "mask", "９９９");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "123");
            ok($widget.wijinputmask("option", "mask") == "９９９", "mask is ９９９");
            equal($widget.val(), "１２３", "mask is ９９９");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            equal($widget.val(), "___", "mask is ９９９");
            $widget.wijinputmask("option", "mask", "ＫＫＫ");
            $widget.wijinputmask("option", "text", "ｱｲｳ");
            ok($widget.wijinputmask("option", "mask") == "ＫＫＫ", "mask is ＫＫＫ");
            equal($widget.val(), "アイウ", "mask is ＫＫＫ");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            equal($widget.val(), "___", "mask is ＫＫＫ");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "うえお");
            equal($widget.val(), "ウエオ", "mask is ＫＫＫ");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "アイウ");
            equal($widget.val(), "アイウ", "mask is ＫＫＫ");
            $widget.wijinputmask("option", "mask", "ＪＪＪ");
            $widget.wijinputmask("option", "text", "ｱｲｳ");
            ok($widget.wijinputmask("option", "mask") == "ＪＪＪ", "mask is ＪＪＪ");
            equal($widget.val(), "あいう", "mask is ＪＪＪ");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            equal($widget.val(), "___", "mask is ＪＪＪ");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "うえお");
            equal($widget.val(), "うえお", "mask is ＪＪＪ");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "アイウ");
            equal($widget.val(), "あいう", "mask is ＪＪＪ");
            $widget.wijinputmask("option", "mask", "ＺＺＺ");
            $widget.wijinputmask("option", "text", "ｱｲｳ");
            ok($widget.wijinputmask("option", "mask") == "ＺＺＺ", "mask is ＺＺＺ");
            equal($widget.val(), "アイウ", "mask is ＺＺＺ");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            equal($widget.val(), "ａｂｃ", "mask is ＺＺＺ");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "うえお");
            equal($widget.val(), "うえお", "mask is ＺＺＺ");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "アイウ");
            equal($widget.val(), "アイウ", "mask is ＺＺＺ");
            $widget.wijinputmask("option", "mask", "NNN");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            ok($widget.wijinputmask("option", "mask") == "NNN", "mask is NNN");
            equal($widget.val(), "___", "mask is NNN");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "きゅ");
            equal($widget.val(), "ｷﾕ_", "mask is NNN");
            $widget.wijinputmask("option", "mask", "ＮＮＮ");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            ok($widget.wijinputmask("option", "mask") == "ＮＮＮ", "mask is NNN");
            equal($widget.val(), "___", "mask is ＮＮＮ");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "きゅ");
            equal($widget.val(), "キユ_", "mask is ＮＮＮ");
            $widget.wijinputmask("option", "mask", "ＧＧＧ");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            ok($widget.wijinputmask("option", "mask") == "ＧＧＧ", "mask is NNN");
            equal($widget.val(), "___", "mask is ＧＧＧ");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "きゅ");
            equal($widget.val(), "きゆ_", "mask is ＧＧＧ");
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        test("new mask, text", function () {
            var $widget = tests.createInputMask();
            $widget.wijinputmask("option", "mask", /\A{3,5}/);
            $widget.wijinputmask("option", "text", "");
            ok($widget.wijinputmask("option", "mask").source == "\\A{3,5}", "format is \\A{3,5}");
            $widget.wijinputmask("option", "text", "abcd");
            equal($widget.val(), 'ABCD', "text is valid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "1234");
            equal($widget.val(), '___', "text is invalid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいうえ");
            equal($widget.val(), '___', "text is invalid");
            $widget.wijinputmask("option", "mask", /\a{3,5}/);
            $widget.wijinputmask("option", "text", "");
            ok($widget.wijinputmask("option", "mask").source == "\\a{3,5}", "format is \\a{3,5}");
            $widget.wijinputmask("option", "text", "ABCD");
            equal($widget.val(), 'abcd', "text is valid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "1234");
            equal($widget.val(), '___', "text is invalid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいうえ");
            equal($widget.val(), '___', "text is invalid");
            $widget.wijinputmask("option", "mask", /\D{3,5}/);
            $widget.wijinputmask("option", "text", "");
            ok($widget.wijinputmask("option", "mask").source == "\\D{3,5}", "format is \\D{3,5}");
            $widget.wijinputmask("option", "text", "ABCD");
            equal($widget.val(), '___', "text is valid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "1234");
            equal($widget.val(), '1234', "text is invalid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいうえ");
            equal($widget.val(), '___', "text is invalid");
            $widget.wijinputmask("option", "mask", /\W{3,5}/);
            $widget.wijinputmask("option", "text", "");
            ok($widget.wijinputmask("option", "mask").source == "\\W{3,5}", "format is \\W{3,5}");
            $widget.wijinputmask("option", "text", "ab12");
            equal($widget.val(), 'ab12', "text is valid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいうえ");
            equal($widget.val(), '___', "text is invalid");
            $widget.wijinputmask("option", "mask", /\K{3,5}/);
            $widget.wijinputmask("option", "text", "");
            ok($widget.wijinputmask("option", "mask").source == "\\K{3,5}", "format is \\K{3,5}");
            $widget.wijinputmask("option", "text", "ABCD");
            equal($widget.val(), '___', "text is valid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "1234");
            equal($widget.val(), '___', "text is invalid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいうえ");
            equal($widget.val(), 'ｱｲｳｴ', "text is invalid");
            $widget.wijinputmask("option", "mask", /\H{3,5}/);
            $widget.wijinputmask("option", "text", "");
            ok($widget.wijinputmask("option", "mask").source == "\\H{3,5}", "format is \\H{3,5}");
            $widget.wijinputmask("option", "text", "abcd");
            equal($widget.val(), 'abcd', "text is valid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "1234");
            equal($widget.val(), '1234', "text is invalid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいうえ");
            equal($widget.val(), 'ｱｲｳｴ', "text is invalid");
            $widget.wijinputmask("option", "mask", /\Ａ{3,5}/);
            $widget.wijinputmask("option", "text", "");
            ok($widget.wijinputmask("option", "mask").source == "\\Ａ{3,5}", "format is \\Ａ{3,5}");
            $widget.wijinputmask("option", "text", "abcd");
            equal($widget.val(), 'ＡＢＣＤ', "text is valid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "1234");
            equal($widget.val(), '___', "text is invalid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいうえ");
            equal($widget.val(), '___', "text is invalid");
            $widget.wijinputmask("option", "mask", /\ａ{3,5}/);
            $widget.wijinputmask("option", "text", "");
            ok($widget.wijinputmask("option", "mask").source == "\\ａ{3,5}", "format is \\ａ{3,5}");
            $widget.wijinputmask("option", "text", "ABCD");
            equal($widget.val(), 'ａｂｃｄ', "text is valid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "1234");
            equal($widget.val(), '___', "text is invalid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいうえ");
            equal($widget.val(), '___', "text is invalid");
            $widget.wijinputmask("option", "mask", /\Ｄ{3,5}/);
            $widget.wijinputmask("option", "text", "");
            ok($widget.wijinputmask("option", "mask").source == "\\Ｄ{3,5}", "format is \\Ｄ{3,5}");
            $widget.wijinputmask("option", "text", "ABCD");
            equal($widget.val(), '___', "text is valid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "1234");
            equal($widget.val(), '１２３４', "text is invalid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいうえ");
            equal($widget.val(), '___', "text is invalid");
            $widget.wijinputmask("option", "mask", /\Ｗ{3,5}/);
            $widget.wijinputmask("option", "text", "");
            ok($widget.wijinputmask("option", "mask").source == "\\Ｗ{3,5}", "format is \\Ｗ{3,5}");
            $widget.wijinputmask("option", "text", "ab12");
            equal($widget.val(), 'ａｂ１２', "text is valid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいうえ");
            equal($widget.val(), '___', "text is invalid");
            $widget.wijinputmask("option", "mask", /\Ｋ{3,5}/);
            $widget.wijinputmask("option", "text", "");
            ok($widget.wijinputmask("option", "mask").source == "\\Ｋ{3,5}", "format is \\Ｋ{3,5}");
            $widget.wijinputmask("option", "text", "ABCD");
            equal($widget.val(), '___', "text is valid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "1234");
            equal($widget.val(), '___', "text is invalid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "あいうえ");
            equal($widget.val(), 'アイウエ', "text is invalid");
            $widget.wijinputmask("option", "mask", /\Ｊ{3,5}/);
            $widget.wijinputmask("option", "text", "");
            ok($widget.wijinputmask("option", "mask").source == "\\Ｊ{3,5}", "format is \\Ｊ{3,5}");
            $widget.wijinputmask("option", "text", "ABCD");
            equal($widget.val(), '___', "text is valid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "1234");
            equal($widget.val(), '___', "text is invalid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "アイウエ");
            equal($widget.val(), 'あいうえ', "text is invalid");
            $widget.wijinputmask("option", "mask", /\Ｚ{3,5}/);
            $widget.wijinputmask("option", "text", "");
            ok($widget.wijinputmask("option", "mask").source == "\\Ｚ{3,5}", "format is \\Ｚ{3,5}");
            $widget.wijinputmask("option", "text", "abcd");
            equal($widget.val(), 'ａｂｃｄ', "text is valid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "1234");
            equal($widget.val(), '１２３４', "text is invalid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "ｱｲｳｴ");
            equal($widget.val(), 'アイウエ', "text is valid");
            $widget.wijinputmask("option", "mask", /\N{3,5}/);
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            ok($widget.wijinputmask("option", "mask").source == "\\N{3,5}", "format is \\N{3,5}");
            equal($widget.val(), "___", "text is invalid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "きゅ");
            equal($widget.val(), "ｷﾕ_", "text is valid");
            $widget.wijinputmask("option", "mask", /\Ｎ{3,5}/);
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            ok($widget.wijinputmask("option", "mask").source == "\\Ｎ{3,5}", "format is \\Ｎ{3,5}");
            equal($widget.val(), "___", "text is invalid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "きゅ");
            equal($widget.val(), "キユ_", "text is valid");
            $widget.wijinputmask("option", "mask", /\Ｇ{3,5}/);
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            ok($widget.wijinputmask("option", "mask").source == "\\Ｇ{3,5}", "format is \\Ｇ{3,5}");
            equal($widget.val(), "___", "text is invalid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "きゅ");
            equal($widget.val(), "きゆ_", "text is valid");
            $widget.wijinputmask("option", "mask", /\Ｔ{3,5}/);
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "abc");
            ok($widget.wijinputmask("option", "mask").source == "\\Ｔ{3,5}", "format is \\Ｔ{3,5}");
            equal($widget.val(), "___", "text is invalid");
            $widget.wijinputmask("option", "text", "");
            $widget.wijinputmask("option", "text", "𢰝き");
            equal($widget.val(), "𢰝__", "text is valid");
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        test("tabAction", function () {
            var $widget = tests.createInputMask({
                mask: "000-0000",
                tabAction: "field"
            });
            ok($widget.wijinputmask("option", "tabAction") == "field", "tabAction is field");
            $widget.wijinputmask('focus');
            $widget.simulateKeyStroke("[HOME][TAB]", null, function () {
                equal($widget.wijinputmask('isFocused'), true, "tabAction is field");
                $widget.wijinputmask("option", "tabAction", "control");
                $widget.wijinputmask('destroy');
                $widget.remove();
                start();
            });
            stop();
        });

        test("highlightText1", function () {
            var $widget = tests.createInputMask({
                mask: "000-0000",
                text: "1234567"
            });
            ok($widget.wijinputmask("option", "highlightText") == "none", "highlightText is default value");
            $widget.wijinputmask("focus");
            setTimeout(function () {
                equal($widget.wijinputmask("getSelectedText"), "");
                $widget.remove();
                start();
            }, 200);
            stop();
        });

        test("highlightText2", function () {
            var $widget = tests.createInputMask({
                mask: "000-0000",
                text: "1234567"
            });
            $widget.wijinputmask("option", "highlightText", "field");
            ok($widget.wijinputmask("option", "highlightText") == "field", "highlightText is field");
            $widget.wijinputmask("focus");
            setTimeout(function () {
                equal($widget.wijinputmask("getSelectedText"), "123");
                $widget.remove();
                start();
            }, 200);
            stop();
        });

        test("highlightText2", function () {
            var $widget = tests.createInputMask({
                mask: "000-0000",
                text: "1234567"
            });
            $widget.wijinputmask("option", "highlightText", "all");
            ok($widget.wijinputmask("option", "highlightText") == "all", "highlightText is all");
            $widget.wijinputmask("focus");
            setTimeout(function () {
                equal($widget.wijinputmask("getSelectedText"), "123-4567");
                $widget.remove();
                start();
            }, 200);
            stop();
        });

        test("highlightText1 for advanced mode", function () {
            var $widget = tests.createInputMask({
                mask: /\D{3}-\D{4}/,
                text: "1234567"
            });
            ok($widget.wijinputmask("option", "highlightText") == "none", "highlightText is default value");
            $widget.wijinputmask("focus");
            setTimeout(function () {
                equal($widget.wijinputmask("getSelectedText"), "");
                $widget.remove();
                start();
            }, 200);
            stop();
        });

        test("highlightText2 for advanced mode", function () {
            var $widget = tests.createInputMask({
                mask: /\D{3}-\D{4}/,
                text: "1234567"
            });
            $widget.wijinputmask("option", "highlightText", "field");
            ok($widget.wijinputmask("option", "highlightText") == "field", "highlightText is field");
            $widget.wijinputmask("focus");
            setTimeout(function () {
                equal($widget.wijinputmask("getSelectedText"), "123");
                $widget.remove();
                start();
            }, 200);
            stop();
        });

        test("highlightText2 for advanced mode", function () {
            var $widget = tests.createInputMask({
                mask: /\D{3}-\D{4}/,
                text: "1234567"
            });
            $widget.wijinputmask("option", "highlightText", "all");
            ok($widget.wijinputmask("option", "highlightText") == "all", "highlightText is all");
            $widget.wijinputmask("focus");
            setTimeout(function () {
                equal($widget.wijinputmask("getSelectedText"), "123-4567");
                $widget.remove();
                start();
            }, 200);
            stop();
        });
        test("Set options in constructor", function () {
            var $widget = tests.createInputMask({
                allowPromptAsInput: true,
                autoConvert: false,
                blurOnLastChar: true,
                blurOnLeftRightKey: "both",
                culture: "ja-JP",
                dropDownButtonAlign: "left",
                mask: "999",
                hideEnter: true,
                hidePromptOnLeave: true,
                invalidClass: "test",
                imeMode: "active",
                passwordChar: "#",
                placeholder: "Please input...",
                promptChar: "^",
                resetOnPrompt: false,
                resetOnSpace: false,
                readOnly: true,
                skipLiterals: false,
                spinnerAlign: "bothSideUpLeft",
                showDropDownButton: true,
                showSpinner: true,
                text: "123",
                tabAction: "field",
                pickers: {
                    list: [
                        "aaa", 
                        "bbb", 
                        "ccc"
                    ],
                    width: 100,
                    height: 100
                }
            });
            ok($widget.wijinputmask("option", "allowPromptAsInput") == true);
            ok($widget.wijinputmask("option", "autoConvert") == false);
            ok($widget.wijinputmask("option", "blurOnLastChar") == true);
            ok($widget.wijinputmask("option", "blurOnLeftRightKey") == "both");
            ok($widget.wijinputmask("option", "culture") == "ja-JP");
            ok($widget.wijinputmask("option", "dropDownButtonAlign") == "left");
            ok($widget.wijinputmask("option", "mask") == "999");
            ok($widget.wijinputmask("option", "hideEnter") == true);
            ok($widget.wijinputmask("option", "hidePromptOnLeave") == true);
            ok($widget.wijinputmask("option", "invalidClass") == "test");
            ok($widget.wijinputmask("option", "imeMode") == "active");
            ok($widget.wijinputmask("option", "passwordChar") == "#");
            ok($widget.wijinputmask("option", "placeholder") == "Please input...");
            ok($widget.wijinputmask("option", "promptChar") == "^");
            ok($widget.wijinputmask("option", "resetOnPrompt") == false);
            ok($widget.wijinputmask("option", "resetOnSpace") == false);
            ok($widget.wijinputmask("option", "readOnly") == true);
            ok($widget.wijinputmask("option", "skipLiterals") == false);
            ok($widget.wijinputmask("option", "spinnerAlign") == "bothSideUpLeft");
            ok($widget.wijinputmask("option", "showDropDownButton") == true);
            ok($widget.wijinputmask("option", "showSpinner") == true);
            ok($widget.wijinputmask("option", "text") == "123");
            ok($widget.wijinputmask("option", "tabAction") == "field");
            var pickers = $widget.wijinputmask("option", "pickers");
            ok(pickers.list.length == 3);
            ok(pickers.width == 100);
            ok(pickers.height == 100);
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        test("Set options in constructor for advanced format", function () {
            var $widget = tests.createInputMask({
                allowPromptAsInput: true,
                autoConvert: false,
                blurOnLastChar: true,
                blurOnLeftRightKey: "both",
                culture: "ja-JP",
                dropDownButtonAlign: "left",
                maskFormat: /\D{3}/,
                hideEnter: true,
                hidePromptOnLeave: true,
                invalidClass: "test",
                imeMode: "active",
                passwordChar: "#",
                placeholder: "Please input...",
                promptChar: "^",
                resetOnPrompt: false,
                resetOnSpace: false,
                readOnly: true,
                skipLiterals: false,
                spinnerAlign: "bothSideUpLeft",
                showDropDownButton: true,
                showSpinner: true,
                text: "123",
                tabAction: "field",
                highlightText: "control",
                pickers: {
                    list: [
                        "aaa", 
                        "bbb", 
                        "ccc"
                    ],
                    width: 100,
                    height: 100
                }
            });
            ok($widget.wijinputmask("option", "allowPromptAsInput") == true);
            ok($widget.wijinputmask("option", "autoConvert") == false);
            ok($widget.wijinputmask("option", "blurOnLastChar") == true);
            ok($widget.wijinputmask("option", "blurOnLeftRightKey") == "both");
            ok($widget.wijinputmask("option", "culture") == "ja-JP");
            ok($widget.wijinputmask("option", "dropDownButtonAlign") == "left");
            ok($widget.wijinputmask("option", "maskFormat").source == "\\D{3}");
            ok($widget.wijinputmask("option", "hideEnter") == true);
            ok($widget.wijinputmask("option", "hidePromptOnLeave") == true);
            ok($widget.wijinputmask("option", "invalidClass") == "test");
            ok($widget.wijinputmask("option", "imeMode") == "active");
            ok($widget.wijinputmask("option", "passwordChar") == "#");
            ok($widget.wijinputmask("option", "placeholder") == "Please input...");
            ok($widget.wijinputmask("option", "promptChar") == "^");
            ok($widget.wijinputmask("option", "resetOnPrompt") == false);
            ok($widget.wijinputmask("option", "resetOnSpace") == false);
            ok($widget.wijinputmask("option", "readOnly") == true);
            ok($widget.wijinputmask("option", "skipLiterals") == false);
            ok($widget.wijinputmask("option", "spinnerAlign") == "bothSideUpLeft");
            ok($widget.wijinputmask("option", "showDropDownButton") == true);
            ok($widget.wijinputmask("option", "showSpinner") == true);
            ok($widget.wijinputmask("option", "text") == "123");
            ok($widget.wijinputmask("option", "tabAction") == "field");
            ok($widget.wijinputmask("option", "highlightText") == "control");
            var pickers = $widget.wijinputmask("option", "pickers");
            ok(pickers.list.length == 3);
            ok(pickers.width == 100);
            ok(pickers.height == 100);
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        QUnit.module("wijinputmask: methods");
        test("getPostValue", function () {
            var $widget = tests.createInputMask({
                mask: "999-9999",
                text: "1234567"
            });
            equal($widget.wijinputmask("getPostValue"), "123-4567", "getPostValue returns value");
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        test("getText", function () {
            var $widget = tests.createInputMask({
                mask: "999-9999",
                text: "1234567"
            });
            equal($widget.wijinputmask("getText"), "1234567", "getText returns value");
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        test("setText", function () {
            var $widget = tests.createInputMask({
                mask: "999-9999"
            });
            $widget.wijinputmask("setText", "1234567");
            equal($widget.val(), "123-4567", "getText returns value");
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        test("drop", function () {
            var $widget = tests.createInputMask({
                mask: "999"
            });
            var items = [
                {
                    label: 'Minimum',
                    value: "123"
                }, 
                {
                    label: 'Middle',
                    value: "456"
                }, 
                {
                    label: 'Maximum',
                    value: "789"
                }
            ];
            $widget.wijinputmask("option", "comboItems", items);
            $widget.wijinputmask("drop");
            $widget.simulateKeyStroke("[DOWN][ENTER]", null, function () {
                equal($widget.val(), "123", "Date is dropped down.");
                $widget.wijinputmask('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("selectText, getSelectedText", function () {
            var $widget = tests.createInputMask({
                maskFormat: "9999",
                text: "1234"
            });
            $widget.wijinputmask("selectText", 1, 3);
            setTimeout(function () {
                equal($widget.wijinputmask("getSelectedText"), "23", "getSelectedText returns value");
                $widget.remove();
                start();
            }, 200);
            stop();
        });
        QUnit.module("wijinputmask: events");
        test("invalidInput", function () {
            var $widget = tests.createInputMask({
                mask: "999"
            });
            var events = new Array();
            $widget.wijinputmask({
                invalidInput: function (e, data) {
                    events.push("invalidInput");
                }
            });
            $widget.simulateKeyStroke("[HOME]a", null, function () {
                notEqual(events.toString().indexOf("invalidInput"), -1, "invalidInput is fired.");
                $widget.wijinputmask('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("textChanged", function () {
            var $widget = tests.createInputMask({
                maskFormat: "CCC"
            });
            var events = new Array();
            $widget.wijinputmask({
                textChanged: function (e, data) {
                    events.push("textChanged");
                }
            });
            $widget.wijinputmask("option", "text", "abc");
            notEqual(events.toString().indexOf("textChanged"), -1, "textChanged is fired.");
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        test("textChanged by UI", function () {
            var $widget = tests.createInputMask({
                mask: /\D{3}/
            });
            var events = new Array();
            $widget.wijinputmask({
                textChanged: function (e, data) {
                    events.push("textChanged");
                }
            });
            $widget.simulateKeyStroke("[HOME]1", null, function () {
                notEqual(events.toString().indexOf("textChanged"), -1, "textChanged is fired.");
                $widget.wijinputmask('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("dropDownOpen", function () {
            var $widget = tests.createInputMask({
                format: "9"
            });
            var items = [
                {
                    label: 'Minimum',
                    value: "123"
                }, 
                {
                    label: 'Middle',
                    value: "456"
                }, 
                {
                    label: 'Maximum',
                    value: "789"
                }
            ];
            $widget.wijinputmask("option", "comboItems", items);
            var events = new Array();
            $widget.wijinputmask({
                dropDownOpen: function (e, data) {
                    events.push("dropDownOpen");
                }
            });
            $widget.wijinputmask("drop");
            notEqual(events.toString().indexOf("dropDownOpen"), -1, "dropDownOpen is fired.");
            $widget.wijinputmask("drop");
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        test("dropDownClose", function () {
            var $widget = tests.createInputMask({
                format: "9"
            });
            var items = [
                {
                    label: 'Minimum',
                    value: "123"
                }, 
                {
                    label: 'Middle',
                    value: "456"
                }, 
                {
                    label: 'Maximum',
                    value: "789"
                }
            ];
            $widget.wijinputmask("option", "comboItems", items);
            var events = new Array();
            $widget.wijinputmask({
                dropDownClose: function (e, data) {
                    events.push("dropDownClose");
                }
            });
            $widget.wijinputmask("drop");
            $widget.wijinputmask("drop");
            notEqual(events.toString().indexOf("dropDownClose"), -1, "dropDownClose is fired.");
            $widget.wijinputmask('destroy');
            $widget.remove();
        });
        //test("keyExit", function () {
        //    var $widget = tests.createInputMask();
        //    var events = new Array();
        //    $widget.wijinputmask({
        //        keyExit: function (e, data) {
        //            events.push("keyExit");
        //        }
        //    });
        //    $widget.wijinputmask("focus");
        //    $widget.simulateKeyStroke("[TAB]", null, function () {
        //        notEqual(events.toString().indexOf("keyExit"), -1, "keyExit is fired.");
        //        $widget.wijinputmask('destroy');
        //        $widget.remove();
        //        start();
        //    });
        //    stop();
        //});
        //test("keyExit", function () {
        //    var $widget = tests.createInputText({
        //        maskFormat: "CCC",
        //        blurOnLastChar: true
        //    });
        //    var events = new Array();
        //    $widget.wijinputmask({
        //        keyExit: function (e, data) {
        //            events.push("keyExit");
        //        }
        //    });
        //    $widget.wijinputmask("focus");
        //    $widget.simulateKeyStroke("[HOME]abc", null, function () {
        //        notEqual(events.toString().indexOf("keyExit"), -1, "keyExit is fired.");
        //        $widget.wijinputmask('destroy');
        //        $widget.remove();
        //        start();
        //    });
        //    stop();
        //});
    })(wijmo.tests || (wijmo.tests = {}));
    var tests = wijmo.tests;
})(wijmo || (wijmo = {}));
