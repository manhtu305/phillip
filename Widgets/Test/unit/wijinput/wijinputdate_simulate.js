var wijmo;
(function (wijmo) {
    (function (tests) {
        var $ = jQuery;
        QUnit.module("wijinputdate: simulate");
        test("typing", function () {
            var $widget = tests.createInputDate();
            $widget.wijinputdate('focus');
            $widget.simulateKeyStroke("[HOME]726[RIGHT]2010", null, function () {
                ok($widget.val() == "7/26/2010", "Date input ok");
                $widget.simulateKeyStroke("[UP][UP][DOWN][DOWN][DOWN]", null, function () {
                    ok($widget.val() == "7/26/2009", "Inc/Dec ok");
                    $widget.wijinputdate('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("tabbing", function () {
            var $widget = tests.createInputDate({
                dateFormat: 'T'
            });
            $widget.wijinputdate('focus');
            $widget.simulateKeyStroke("[TAB][TAB][TAB]", null, function () {
                ok($widget.wijinputdate('option', 'activeField') == 3, "Field OK");
                $widget.wijinputdate('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
    })(wijmo.tests || (wijmo.tests = {}));
    var tests = wijmo.tests;
})(wijmo || (wijmo = {}));
