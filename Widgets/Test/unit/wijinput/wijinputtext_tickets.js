﻿var wijmo;
(function (wijmo) {
    (function (tests) {
        var $ = jQuery;
        QUnit.module("wijinputtext: tickets");
        test("#90287", function () {
            var $widget = tests.createInputText({ disabled: true, showOverflowTip: true, text: "This is a very very long textThis is a very very long text" });
            $widget.simulate('mouseover');
            setTimeout(function() {
                ok($("body").find(".wijmo-wijtooltip").length === 0);
                $widget.wijinputtext('destroy');
                $widget.remove();
                start();
            }, 400);
            stop();
        });

        test("#97277", function () {
            var $widget = tests.createInputText({ text: "aa\raa" }, $("<textarea/>"));
            $widget[0].selectionStart = 4;
            $widget.simulate('mouseup');
            ok($widget[0].selectionStart === 4);
            $widget.wijinputtext('destroy');
            $widget.remove();
        });

        test("#209827", function () {
            var widget = tests.createInputText({
                text: "text",
                textChanged: function (e, args) { msg = msg + "textChanged"; }
            }), msg = '';
            $("#wijmo_input_0").wijinputtext("option", "text", "abc");
            ok(msg === 'textChanged');
            widget.remove();
        });

    })(wijmo.tests || (wijmo.tests = {}));
    var tests = wijmo.tests;
})(wijmo || (wijmo = {}));
