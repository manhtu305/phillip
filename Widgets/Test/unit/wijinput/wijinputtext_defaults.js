var wijinputcoreOptionDefaults = {
    allowSpinLoop: false,
    blurOnLastChar: false,
    blurOnLeftRightKey: 'none',
    culture: '',
    cultureCalendar: '',
    disabled: false,
    dropDownButtonAlign: 'right',
    hideEnter: false,
    imeMode: 'auto',
    invalidClass: 'ui-state-error',
    placeholder: undefined,
    pickers: {
    },
    readonly: false,
    showDropDownButton: false,
    showSpinner: false,
    spinnerAlign: 'verticalRight',
    dropDownOpen: null,
    dropDownClose: null,
    dropDownButtonMouseDown: null,
    dropDownButtonMouseUp: null,
    initializing: null,
    initialized: null,
    invalidInput: null,
    textChanged: null,
    showNullText: false,
    nullText: undefined,
    disableUserInput: false,
    buttonAlign: null,
    showTrigger: undefined,
    triggerMouseDown: null,
    triggerMouseUp: null,
    keyExit: null,
    readingImeStringOutput : null
};
commonWidgetTests('wijinputtext', {
    defaults: $.extend({
    }, wijinputcoreOptionDefaults, {
        autoConvert: true,
        countWrappedLine: false,
        format: '',
        lengthAsByte: false,
        maxLength: 0,
        maxLineCount: 0,
        passwordChar: '',
        text: null,
        highlightText: false,
        ellipsis: 'none',
        ellipsisString: '\u2026',
        showOverflowTip: false
    })
});
