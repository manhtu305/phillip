﻿var wijmo;
(function (wijmo) {
    (function (tests) {
        var $ = jQuery;
        QUnit.module("wijinputnumber: options");
        test("culture", function () {
            var $widget = tests.createInputNumber({
                type: "currency",
                value: 123.45
            });
            ok($widget.wijinputnumber("option", "culture") == "", "culture is default value");
            equal($widget.val(), "$123.45", "culture is default value");
            $widget.wijinputnumber("option", "culture", "ja-JP");
            ok($widget.wijinputnumber("option", "culture") == "ja-JP", "culture is ja-JP");
            equal($widget.val(), "¥123.45", "culture is default value");
            $widget.wijinputnumber("option", "culture", "zh-CN");
            ok($widget.wijinputnumber("option", "culture") == "zh-CN", "culture is zh-CN");
            equal($widget.val(), "¥123.45", "culture is default value");
            $widget.wijinputnumber('destroy');
            $widget.remove();
            var $widget = tests.createInputNumber({
                type: "percent",
                value: 123.45
            });
            ok($widget.wijinputnumber("option", "culture") == "", "culture is default value");
            equal($widget.val(), "123.45 %", "culture is default value");
            $widget.wijinputnumber("option", "culture", "ja-JP");
            ok($widget.wijinputnumber("option", "culture") == "ja-JP", "culture is ja-JP");
            equal($widget.val(), "123.45%", "culture is default value");
            $widget.wijinputnumber("option", "culture", "zh-CN");
            ok($widget.wijinputnumber("option", "culture") == "zh-CN", "culture is zh-CN");
            equal($widget.val(), "123.45%", "culture is default value");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("increment", function () {
            var $widget = tests.createInputNumber({
                value: 123.45
            });
            ok($widget.wijinputnumber("option", "increment") == 1, "increment is default value");
            $widget.wijinputnumber("spinUp");
            equal($widget.val(), "124.45", "increment is default value");
            $widget.wijinputnumber("spinDown");
            equal($widget.val(), "123.45", "increment is default value");
            $widget.wijinputnumber("option", "increment", 3);
            ok($widget.wijinputnumber("option", "increment") == 3, "increment is 3");
            $widget.wijinputnumber("spinUp");
            equal($widget.val(), "126.45", "decimalPlaces is 3");
            $widget.wijinputnumber("spinDown");
            equal($widget.val(), "123.45", "decimalPlaces is 3");
            $widget.wijinputnumber("option", "increment", 0);
            ok($widget.wijinputnumber("option", "increment") == 0, "increment is 0");
            $widget.wijinputnumber("spinUp");
            equal($widget.val(), "123.45", "decimalPlaces is 3");
            $widget.wijinputnumber("spinDown");
            equal($widget.val(), "123.45", "decimalPlaces is 3");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("Spin Down increment by UI", function () {
            var $widget = tests.createInputNumber({
                value: 123.45
            });
            $widget.wijinputnumber("option", "increment", 3);
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[HOME][DOWN]", null, function () {
                equal($widget.wijinputnumber("option", "value"), 120.45, "spinDown in integer field");
                $widget.simulateKeyStroke("[END][DOWN]", null, function () {
                    equal($widget.val(), "120.42", "spinDown in decimal field");
                    $widget.wijinputnumber('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("Spin Up increment by UI", function () {
            var $widget = tests.createInputNumber({
                value: 123.45
            });
            $widget.wijinputnumber("option", "increment", 3);
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[HOME][UP]", null, function () {
                equal($widget.wijinputnumber("option", "value"), 126.45, "spinUp in integer field");
                $widget.simulateKeyStroke("[END][UP]", null, function () {
                    equal($widget.val(), "126.48", "spinUp in decimal field");
                    $widget.wijinputnumber('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("decimalPlaces", function () {
            var $widget = tests.createInputNumber({
                value: 123.45
            });
            ok($widget.wijinputnumber("option", "decimalPlaces") == 2, "decimalPlaces is default value");
            equal($widget.val(), "123.45", "culture is default value");
            $widget.wijinputnumber("option", "decimalPlaces", 3);
            ok($widget.wijinputnumber("option", "decimalPlaces") == 3, "decimalPlaces is 3");
            equal($widget.val(), "123.450", "decimalPlaces is 3");
            $widget.wijinputnumber("option", "decimalPlaces", 0);
            ok($widget.wijinputnumber("option", "decimalPlaces") == 0, "decimalPlaces is 0");
            equal($widget.val(), "123", "decimalPlaces is 0");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("maxValue", function () {
            var $widget = tests.createInputNumber();
            ok($widget.wijinputnumber("option", "maxValue") == 1000000000, "maxValue is default value");
            equal($widget.val(), "0.00", "culture is default value");
            $widget.wijinputnumber("option", "maxValue", 100);
            ok($widget.wijinputnumber("option", "maxValue") == 100, "maxValue is 100");
            $widget.wijinputnumber("option", "value", 120);
            equal($widget.val(), "100.00", "maxValue is 100");
            $widget.wijinputnumber("option", "value", 80);
            equal($widget.val(), "80.00", "maxValue is 100");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("minValue", function () {
            var $widget = tests.createInputNumber();
            ok($widget.wijinputnumber("option", "minValue") == -1000000000, "minValue is default value");
            equal($widget.val(), "0.00", "minValue is default value");
            $widget.wijinputnumber("option", "minValue", -100);
            ok($widget.wijinputnumber("option", "minValue") == -100, "minValue is 100");
            $widget.wijinputnumber("option", "value", -120);
            equal($widget.val(), "-100.00", "maxValue is 100");
            $widget.wijinputnumber("option", "value", -80);
            equal($widget.val(), "-80.00", "maxValue is 100");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("placeholder", function () {
            var $widget = tests.createInputNumber({
                value: null
            });
            ok($widget.wijinputnumber("option", "placeholder") == null, "placeholder is default value");
            $widget.wijinputnumber("option", "placeholder", "");
            ok($widget.wijinputnumber("option", "placeholder") == "", "placeholder is string.empty");
            $widget.wijinputnumber("option", "placeholder", "test");
            ok($widget.wijinputnumber("option", "placeholder") == "test", "placeholder is test");
            equal($widget.wijinputnumber("getText"), "test", "placeholder is test");
            $widget.wijinputnumber("option", "placeholder", "あいうえお");
            ok($widget.wijinputnumber("option", "placeholder") == "あいうえお", "placeholder is あいうえお");
            equal($widget.wijinputnumber("getText"), "あいうえお", "placeholder is あいうえお");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("showGroup", function () {
            var $widget = tests.createInputNumber({
                value: 123456.78
            });
            ok($widget.wijinputnumber("option", "showGroup") == false, "showGroup is default value");
            equal($widget.val(), "123456.78", "culture is default value");
            $widget.wijinputnumber("option", "showGroup", true);
            ok($widget.wijinputnumber("option", "showGroup") == true, "showGroup is true");
            equal($widget.val(), "123,456.78", "decimalPlaces is 3");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("readonly", function () {
            var $widget = tests.createInputNumber();
            ok($widget.wijinputnumber("option", "readonly") == false, "readOnly is default value");
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[HOME]2001", null, function () {
                equal($widget.val(), "2001.00", "readOnly is default value");
                $widget.wijinputnumber("option", "readonly", true);
                ok($widget.wijinputnumber("option", "readonly") == true, "readOnly is true");
                $widget.simulateKeyStroke("[HOME]1999", null, function () {
                    equal($widget.val(), "2001.00", "readOnly is true");
                    $widget.wijinputnumber('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("type", function () {
            var $widget = tests.createInputNumber({
                value: 1200
            });
            ok($widget.val() == '1200.00', "Number value is applied");
            $widget.wijinputnumber('destroy');
            $widget.remove();
            $widget = tests.createInputNumber({
                value: 1200,
                type: 'currency'
            });
            ok($widget.val() == '$1200.00', "Currency value is applied");
            $widget.wijinputnumber('destroy');
            $widget.remove();
            $widget = tests.createInputNumber({
                value: 1200,
                type: 'percent'
            });
            ok($widget.val() == '1200.00 %', "Percent value is applied");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("value", function () {
            var $widget = tests.createInputNumber();
            equal($widget.wijinputnumber("option", "value"), 0, "value is default value");
            $widget.wijinputnumber("option", "value", 123.45);
            equal($widget.wijinputnumber("option", "value"), 123.45, "value is 123.45");
            $widget.wijinputnumber("option", "value", null);
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[HOME]456", null, function () {
                equal($widget.wijinputnumber("option", "value"), 456, "value is 456");
                $widget.wijinputnumber('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("allowSpinLoop", function () {
            var $widget = tests.createInputNumber({
                maxValue: 100,
                minValue: -100,
                value: 100
            });
            ok($widget.wijinputnumber("option", "allowSpinLoop") == false, "allowSpinLoop is default value");
            $widget.wijinputnumber("spinUp");
            equal($widget.val(), "100.00", "culture is default value");
            $widget.wijinputnumber("option", "allowSpinLoop", true);
            ok($widget.wijinputnumber("option", "allowSpinLoop") == true, "allowSpinLoop is true");
            $widget.wijinputnumber("spinUp");
            equal($widget.val(), "-100.00", "allowSpinLoop is true");
            $widget.wijinputnumber("spinDown");
            equal($widget.val(), "100.00", "allowSpinLoop is true");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("allowSpinLoop for UI", function () {
            var $widget = tests.createInputNumber({
                maxValue: 100,
                minValue: -100,
                value: 100
            });
            ok($widget.wijinputnumber("option", "allowSpinLoop") == false, "allowSpinLoop is default value");
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[UP]", null, function () {
                equal($widget.val(), "100.00", "allowSpinLoop is default value");
                $widget.wijinputnumber("option", "allowSpinLoop", true);
                ok($widget.wijinputnumber("option", "allowSpinLoop") == true, "allowSpinLoop is true");
                $widget.simulateKeyStroke("[UP]", null, function () {
                    equal($widget.val(), "-100.00", "allowSpinLoop is true");
                    $widget.simulateKeyStroke("[DOWN]", null, function () {
                        equal($widget.val(), "100.00", "allowSpinLoop is true");
                        $widget.wijinputnumber('destroy');
                        $widget.remove();
                        start();
                    });
                });
            });
            stop();
        });
        test("blurOnLeftRightKey is none", function () {
            var $widget = tests.createInputNumber({
                value: 123
            });
            ok($widget.wijinputnumber("option", "blurOnLeftRightKey") == "none", "blurOnLeftRightKey is default value");
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[HOME][LEFT]", null, function () {
                equal($widget.wijinputnumber('isFocused'), true, "blurOnLeftRightKey is default value");
                $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                    equal($widget.wijinputnumber('isFocused'), true, "blurOnLeftRightKey is default value");
                    $widget.wijinputnumber('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("blurOnLeftRightKey is right", function () {
            var $widget = tests.createInputNumber({
                value: 123
            });
            $widget.wijinputnumber("option", "blurOnLeftRightKey", "right");
            ok($widget.wijinputnumber("option", "blurOnLeftRightKey") == "right", "blurOnLeftRightKey is right");
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[HOME][LEFT]", null, function () {
                equal($widget.wijinputnumber('isFocused'), true, "blurOnLeftRightKey is right");
                $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                    equal($widget.wijinputnumber('isFocused'), false, "blurOnLeftRightKey is right");
                    $widget.wijinputnumber('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("blurOnLeftRightKey is left", function () {
            var $widget = tests.createInputNumber({
                value: 123
            });
            $widget.wijinputnumber("option", "blurOnLeftRightKey", "left");
            ok($widget.wijinputnumber("option", "blurOnLeftRightKey") == "left", "blurOnLeftRightKey is left");
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                equal($widget.wijinputnumber('isFocused'), true, "blurOnLastChar is left");
                $widget.simulateKeyStroke("[HOME][LEFT]", null, function () {
                    equal($widget.wijinputnumber('isFocused'), false, "blurOnLastChar is left");
                    $widget.wijinputnumber('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("blurOnLeftRightKey is both", function () {
            var $widget = tests.createInputNumber({
                value: 123
            });
            $widget.wijinputnumber("option", "blurOnLeftRightKey", "both");
            ok($widget.wijinputnumber("option", "blurOnLeftRightKey") == "both", "blurOnLeftRightKey is default value");
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[HOME][LEFT]", null, function () {
                equal($widget.wijinputnumber('isFocused'), false, "blurOnLastChar is default value");
                $widget.wijinputnumber('focus');
                $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                    equal($widget.wijinputnumber('isFocused'), false, "blurOnLastChar is false");
                    $widget.wijinputnumber('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("currencySymbol", function () {
            var $widget = tests.createInputNumber({
                type: "currency",
                value: 123
            });
            ok($widget.wijinputnumber("option", "currencySymbol") == "$", "currencySymbol is default value");
            equal($widget.val(), "$123.00", "currencySymbol is default value");
            $widget.wijinputnumber("option", "currencySymbol", "¥");
            ok($widget.wijinputnumber("option", "currencySymbol") == "¥", "currencySymbol is ¥");
            equal($widget.val(), "¥123.00", "currencySymbol is ¥");
            $widget.wijinputnumber("option", "currencySymbol", "^");
            ok($widget.wijinputnumber("option", "currencySymbol") == "^", "currencySymbol is ¥");
            equal($widget.val(), "^123.00", "currencySymbol is ¥");
            $widget.wijinputnumber('destroy');
            $widget.remove();
            var $widget = tests.createInputNumber({
                value: 123,
                positivePrefix: "$"
            });
            ok($widget.wijinputnumber("option", "currencySymbol") == "$", "currencySymbol is default value");
            equal($widget.val(), "$123.00", "currencySymbol is default value");
            $widget.wijinputnumber("option", "currencySymbol", "¥");
            ok($widget.wijinputnumber("option", "currencySymbol") == "¥", "currencySymbol is ¥");
            equal($widget.val(), "¥123.00", "currencySymbol is ¥");
            $widget.wijinputnumber("option", "currencySymbol", "^");
            ok($widget.wijinputnumber("option", "currencySymbol") == "^", "currencySymbol is ¥");
            equal($widget.val(), "^123.00", "currencySymbol is ¥");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("negativePrefix", function () {
            var $widget = tests.createInputNumber({
                value: -123
            });
            ok($widget.wijinputnumber("option", "negativePrefix") == "-", "negativePrefix is default value");
            equal($widget.val(), "-123.00", "negativePrefix is default value");
            $widget.wijinputnumber("option", "negativePrefix", "$-");
            ok($widget.wijinputnumber("option", "negativePrefix") == "$-", "negativePrefix is $-");
            equal($widget.val(), "$-123.00", "negativePrefix is $-");
            $widget.wijinputnumber("option", "negativePrefix", "負");
            ok($widget.wijinputnumber("option", "negativePrefix") == "負", "negativePrefix is $-");
            equal($widget.val(), "負123.00", "negativePrefix is $-");
            $widget.wijinputnumber('destroy');
            $widget.remove();
            var $widget = tests.createInputNumber({
                type: "currency",
                value: -123
            });
            ok($widget.wijinputnumber("option", "negativePrefix") == "($", "negativePrefix is default value");
            equal($widget.val(), "($123.00)", "negativePrefix is default value");
            $widget.wijinputnumber("option", "negativePrefix", "$-");
            ok($widget.wijinputnumber("option", "negativePrefix") == "$-", "negativePrefix is $-");
            equal($widget.val(), "$-123.00)", "negativePrefix is $-");
            $widget.wijinputnumber("option", "negativePrefix", "負");
            ok($widget.wijinputnumber("option", "negativePrefix") == "負", "negativePrefix is $-");
            equal($widget.val(), "負123.00)", "negativePrefix is $-");
            $widget.wijinputnumber('destroy');
            $widget.remove();
            var $widget = tests.createInputNumber({
                type: "percent",
                value: -123
            });
            ok($widget.wijinputnumber("option", "negativePrefix") == "-", "negativePrefix is default value");
            equal($widget.val(), "-123.00 %", "negativePrefix is default value");
            $widget.wijinputnumber("option", "negativePrefix", "$-");
            ok($widget.wijinputnumber("option", "negativePrefix") == "$-", "negativePrefix is $-");
            equal($widget.val(), "$-123.00 %", "negativePrefix is default value");
            $widget.wijinputnumber("option", "negativePrefix", "負");
            ok($widget.wijinputnumber("option", "negativePrefix") == "負", "negativePrefix is $-");
            equal($widget.val(), "負123.00 %", "negativePrefix is $-");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("negativeSuffix", function () {
            var $widget = tests.createInputNumber({
                value: -123
            });
            ok($widget.wijinputnumber("option", "negativeSuffix") == "", "negativeSuffix is default value");
            equal($widget.val(), "-123.00", "negativeSuffix is default value");
            $widget.wijinputnumber("option", "negativeSuffix", "-$");
            ok($widget.wijinputnumber("option", "negativeSuffix") == "-$", "negativeSuffix is $-");
            equal($widget.val(), "-123.00-$", "negativeSuffix is $-");
            $widget.wijinputnumber("option", "negativeSuffix", "負");
            ok($widget.wijinputnumber("option", "negativeSuffix") == "負", "negativeSuffix is $-");
            equal($widget.val(), "-123.00負", "negativeSuffix is $-");
            $widget.wijinputnumber('destroy');
            $widget.remove();
            var $widget = tests.createInputNumber({
                type: "currency",
                value: -123
            });
            ok($widget.wijinputnumber("option", "negativeSuffix") == ")", "negativeSuffix is default value");
            equal($widget.val(), "($123.00)", "negativeSuffix is default value");
            $widget.wijinputnumber("option", "negativeSuffix", "-$");
            ok($widget.wijinputnumber("option", "negativeSuffix") == "-$", "negativeSuffix is $-");
            equal($widget.val(), "($123.00-$", "negativeSuffix is $-");
            $widget.wijinputnumber("option", "negativeSuffix", "負");
            ok($widget.wijinputnumber("option", "negativeSuffix") == "負", "negativeSuffix is $-");
            equal($widget.val(), "($123.00負", "negativeSuffix is $-");
            $widget.wijinputnumber('destroy');
            $widget.remove();
            var $widget = tests.createInputNumber({
                type: "percent",
                value: -123
            });
            ok($widget.wijinputnumber("option", "negativeSuffix") == " %", "negativeSuffix is default value");
            equal($widget.val(), "-123.00 %", "negativeSuffix is default value");
            $widget.wijinputnumber("option", "negativeSuffix", "%-");
            ok($widget.wijinputnumber("option", "negativeSuffix") == "%-", "negativeSuffix is $-");
            equal($widget.val(), "-123.00%-", "negativeSuffix is $-");
            $widget.wijinputnumber("option", "negativeSuffix", "%負");
            ok($widget.wijinputnumber("option", "negativeSuffix") == "%負", "negativeSuffix is $-");
            equal($widget.val(), "-123.00%負", "negativeSuffix is $-");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("negativeClass", function () {
            var $widget = tests.createInputNumber();
            ok($widget.wijinputnumber("option", "negativeClass") == "ui-state-negative", "negativeClass is default value");
            $widget.wijinputnumber("option", "negativeClass", "test");
            ok($widget.wijinputnumber("option", "negativeClass") == "test", "negativeClass is test");
            $widget.wijinputnumber("option", "negativeClass", "あいう");
            ok($widget.wijinputnumber("option", "negativeClass") == "あいう", "negativeClass is あいう");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("positivePrefix", function () {
            var $widget = tests.createInputNumber({
                value: 123
            });
            ok($widget.wijinputnumber("option", "positivePrefix") == "", "positivePrefix is default value");
            equal($widget.val(), "123.00", "positivePrefix is default value");
            $widget.wijinputnumber("option", "positivePrefix", "$");
            ok($widget.wijinputnumber("option", "positivePrefix") == "$", "positivePrefix is $");
            equal($widget.val(), "$123.00", "positivePrefix is $");
            $widget.wijinputnumber("option", "positivePrefix", "正");
            ok($widget.wijinputnumber("option", "positivePrefix") == "正", "positivePrefix is $-");
            equal($widget.val(), "正123.00", "positivePrefix is $-");
            $widget.wijinputnumber('destroy');
            $widget.remove();
            var $widget = tests.createInputNumber({
                type: "currency",
                value: 123
            });
            ok($widget.wijinputnumber("option", "positivePrefix") == "$", "positivePrefix is default value");
            equal($widget.val(), "$123.00", "positivePrefix is default value");
            $widget.wijinputnumber("option", "positivePrefix", "$+");
            ok($widget.wijinputnumber("option", "positivePrefix") == "$+", "positivePrefix is $-");
            equal($widget.val(), "$+123.00", "positivePrefix is $-");
            $widget.wijinputnumber("option", "positivePrefix", "正");
            ok($widget.wijinputnumber("option", "positivePrefix") == "正", "positivePrefix is $-");
            equal($widget.val(), "正123.00", "positivePrefix is $-");
            $widget.wijinputnumber('destroy');
            $widget.remove();
            var $widget = tests.createInputNumber({
                type: "percent",
                value: 123
            });
            ok($widget.wijinputnumber("option", "positivePrefix") == "", "positivePrefix is default value");
            equal($widget.val(), "123.00 %", "positivePrefix is default value");
            $widget.wijinputnumber("option", "positivePrefix", "+");
            ok($widget.wijinputnumber("option", "positivePrefix") == "+", "positivePrefix is $-");
            equal($widget.val(), "+123.00 %", "positivePrefix is $-");
            $widget.wijinputnumber("option", "positivePrefix", "正");
            ok($widget.wijinputnumber("option", "positivePrefix") == "正", "positivePrefix is $-");
            equal($widget.val(), "正123.00 %", "positivePrefix is $-");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("positiveSuffix", function () {
            var $widget = tests.createInputNumber({
                value: 123
            });
            ok($widget.wijinputnumber("option", "positiveSuffix") == "", "positiveSuffix is default value");
            equal($widget.val(), "123.00", "positiveSuffix is default value");
            $widget.wijinputnumber("option", "positiveSuffix", "$");
            ok($widget.wijinputnumber("option", "positiveSuffix") == "$", "positiveSuffix is $-");
            equal($widget.val(), "123.00$", "positiveSuffix is $-");
            $widget.wijinputnumber("option", "positiveSuffix", "正");
            ok($widget.wijinputnumber("option", "positiveSuffix") == "正", "positiveSuffix is $-");
            equal($widget.val(), "123.00正", "positiveSuffix is $-");
            $widget.wijinputnumber('destroy');
            $widget.remove();
            var $widget = tests.createInputNumber({
                type: "currency",
                value: 123
            });
            ok($widget.wijinputnumber("option", "positiveSuffix") == "", "positiveSuffix is default value");
            equal($widget.val(), "$123.00", "positiveSuffix is default value");
            $widget.wijinputnumber("option", "positiveSuffix", "+");
            ok($widget.wijinputnumber("option", "positiveSuffix") == "+", "positiveSuffix is $-");
            equal($widget.val(), "$123.00+", "positiveSuffix is $-");
            $widget.wijinputnumber("option", "positiveSuffix", "正");
            ok($widget.wijinputnumber("option", "positiveSuffix") == "正", "positiveSuffix is $-");
            equal($widget.val(), "$123.00正", "positiveSuffix is $-");
            $widget.wijinputnumber('destroy');
            $widget.remove();
            var $widget = tests.createInputNumber({
                type: "percent",
                value: 123
            });
            ok($widget.wijinputnumber("option", "positiveSuffix") == " %", "positiveSuffix is default value");
            equal($widget.val(), "123.00 %", "positiveSuffix is default value");
            $widget.wijinputnumber("option", "positiveSuffix", "%+");
            ok($widget.wijinputnumber("option", "positiveSuffix") == "%+", "positiveSuffix is $-");
            equal($widget.val(), "123.00%+", "positiveSuffix is $-");
            $widget.wijinputnumber("option", "positiveSuffix", "%正");
            ok($widget.wijinputnumber("option", "positiveSuffix") == "%正", "positiveSuffix is $-");
            equal($widget.val(), "123.00%正", "positiveSuffix is $-");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("highlightText1", function () {
            var $widget = wijmo.tests.createInputNumber({
                value: 12345.67
            });
            ok($widget.wijinputnumber("option", "highlightText") == false, "highlightText is default value");
            $widget.wijinputnumber("focus");
            setTimeout(function () {
                equal($widget.wijinputnumber("getSelectedText"), "");
                $widget.remove();
                start();
            }, 200);
            stop();
        });

        test("highlightText2", function () {
            var $widget = wijmo.tests.createInputNumber({
                value: 12345.67
            });
            $widget.wijinputnumber("option", "highlightText", true);
            ok($widget.wijinputnumber("option", "highlightText") == true, "highlightText is default value");
            $widget.wijinputnumber("focus");
            setTimeout(function () {
                equal($widget.wijinputnumber("getSelectedText"), "12345.67");
                $widget.remove();
                start();
            }, 200);
            stop();
        });
        test("Set options in constructor", function () {
            var $widget = tests.createInputNumber({
                allowSpinLoop: true,
                blurOnLastChar: true,
                blurOnLeftRightKey: "both",
                culture: "ja-JP",
                increment: 2,
                currencySymbol: "¥",
                decimalPlaces: 3,
                dropDownButtonAlign: "left",
                maxValue: 9,
                minValue: 3,
                hideEnter: true,
                invalidClass: "test",
                negativePrefix: "負",
                negativeSuffix: "-$",
                negativeClass: "abc",
                positivePrefix: "正",
                positiveSuffix: "+$",
                placeholder: "Please input...",
                showGroup: true,
                readOnly: true,
                type: "currency",
                spinnerAlign: "bothSideUpLeft",
                showTrigger: true,
                showSpinner: true,
                value: 5,
                highlightText: true,
                pickers: {
                    list: [
                        123, 
                        456, 
                        789
                    ],
                    width: 100,
                    height: 100
                }
            });
            ok($widget.wijinputnumber("option", "allowSpinLoop") == true);
            ok($widget.wijinputnumber("option", "blurOnLastChar") == true);
            ok($widget.wijinputnumber("option", "blurOnLeftRightKey") == "both");
            ok($widget.wijinputnumber("option", "culture") == "ja-JP");
            ok($widget.wijinputnumber("option", "increment") == 2);
            ok($widget.wijinputnumber("option", "currencySymbol") == "¥");
            ok($widget.wijinputnumber("option", "dropDownButtonAlign") == "left");
            ok($widget.wijinputnumber("option", "decimalPlaces") == 3);
            ok($widget.wijinputnumber("option", "maxValue") == 9);
            ok($widget.wijinputnumber("option", "minValue") == 3);
            ok($widget.wijinputnumber("option", "hideEnter") == true);
            ok($widget.wijinputnumber("option", "invalidClass") == "test");
            ok($widget.wijinputnumber("option", "negativePrefix") == "負");
            ok($widget.wijinputnumber("option", "negativeSuffix") == "-$");
            ok($widget.wijinputnumber("option", "negativeClass") == "abc");
            ok($widget.wijinputnumber("option", "positivePrefix") == "正");
            ok($widget.wijinputnumber("option", "positiveSuffix") == "+$");
            ok($widget.wijinputnumber("option", "placeholder") == "Please input...");
            ok($widget.wijinputnumber("option", "showGroup") == true);
            ok($widget.wijinputnumber("option", "readOnly") == true);
            ok($widget.wijinputnumber("option", "type") == "currency");
            ok($widget.wijinputnumber("option", "spinnerAlign") == "bothSideUpLeft");
            ok($widget.wijinputnumber("option", "showTrigger") == true);
            ok($widget.wijinputnumber("option", "showSpinner") == true);
            ok($widget.wijinputnumber("option", "value") == 5);
            ok($widget.wijinputnumber("option", "highlightText") == true);
            var pickers = $widget.wijinputnumber("option", "pickers");
            ok(pickers.list.length == 3);
            ok(pickers.width == 100);
            ok(pickers.height == 100);
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        QUnit.module("wijinputnumber: methods");
        test("getPostValue", function () {
            var $widget = tests.createInputNumber({
                value: 123.45
            });
            equal($widget.wijinputnumber("getPostValue"), "123.45", "getPostValue returns value");
            $widget.wijinputnumber("option", "type", "currency");
            equal($widget.wijinputnumber("getPostValue"), "123.45", "getPostValue returns value");
            $widget.wijinputnumber("option", "type", "percent");
            equal($widget.wijinputnumber("getPostValue"), "1.2345000000", "getPostValue returns value");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("getText", function () {
            var $widget = tests.createInputNumber({
                value: 123.45
            });
            equal($widget.wijinputnumber("getText"), "123.45", "getText returns value");
            $widget.wijinputnumber("option", "type", "currency");
            equal($widget.wijinputnumber("getText"), "$123.45", "getText returns value");
            $widget.wijinputnumber("option", "type", "percent");
            equal($widget.wijinputnumber("getText"), "123.45 %", "getText returns value");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("getValue", function () {
            var $widget = tests.createInputNumber({
                value: 123.45
            });
            equal($widget.wijinputnumber("getValue"), 123.45, "getValue returns value");
            $widget.wijinputnumber("option", "type", "currency");
            equal($widget.wijinputnumber("getValue"), 123.45, "getValue returns value");
            $widget.wijinputnumber("option", "type", "percent");
            equal($widget.wijinputnumber("getValue"), 123.45, "getValue returns value");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("isValueNull", function () {
            var $widget = tests.createInputNumber();
            equal($widget.wijinputnumber("isValueNull"), false, "isValueNull returns value");
            $widget.wijinputnumber("option", "value", 123);
            equal($widget.wijinputnumber("isValueNull"), false, "isValueNull returns value");
            $widget.wijinputnumber("option", "value", null);
            equal($widget.wijinputnumber("isValueNull"), true, "isValueNull returns value");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("setText", function () {
            var $widget = tests.createInputNumber();
            $widget.wijinputnumber("setText", "123.45");
            equal($widget.wijinputnumber("option", "value"), 123.45, "getText returns value");
            $widget.wijinputnumber("option", "type", "currency");
            $widget.wijinputnumber("setText", "$456.78");
            equal($widget.wijinputnumber("option", "value"), 456.78, "getText returns value");
            $widget.wijinputnumber("setText", "123.45");
            equal($widget.wijinputnumber("option", "value"), 123.45, "getText returns value");
            $widget.wijinputnumber("option", "type", "percent");
            $widget.wijinputnumber("setText", "456.78 %");
            equal($widget.wijinputnumber("option", "value"), 456.78, "getText returns value");
            $widget.wijinputnumber("setText", "123.45");
            equal($widget.wijinputnumber("option", "value"), 123.45, "getText returns value");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("setValue", function () {
            var $widget = tests.createInputNumber();
            $widget.wijinputnumber("setValue", 123.45);
            equal($widget.wijinputnumber("option", "text"), "123.45", "getText returns value");
            $widget.wijinputnumber("setValue", null);
            equal($widget.wijinputnumber("option", "text"), "", "getText returns value");
            $widget.wijinputnumber("setValue", 0);
            equal($widget.wijinputnumber("option", "text"), "0.00", "getText returns value");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("drop", function () {
            var $widget = tests.createInputNumber();
            var items = [
                {
                    label: 'Minimum',
                    value: "123"
                }, 
                {
                    label: 'Middle',
                    value: "456"
                }, 
                {
                    label: 'Maximum',
                    value: "789"
                }
            ];
            $widget.wijinputnumber("option", "comboItems", items);
            $widget.wijinputnumber("drop");
            $widget.simulateKeyStroke("[DOWN][ENTER]", null, function () {
                equal($widget.val(), "123.00", "Date is dropped down.");
                $widget.wijinputnumber('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("spinUp", function () {
            var $widget = tests.createInputNumber({
                value: 123.45
            });
            $widget.wijinputnumber("spinUp");
            equal($widget.wijinputnumber("option", "value"), 124.45, "spinUp in integer field");
            $widget.wijinputnumber("focus");
            $widget.simulateKeyStroke("[END][TAB]", null, function () {
                $widget.wijinputnumber("spinUp");
                equal($widget.wijinputnumber("option", "value"), 124.46, "spinUp in decimal field");
                $widget.wijinputnumber('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("spinUp by UI", function () {
            var $widget = tests.createInputNumber({
                value: 123.45
            });
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[HOME][UP]", null, function () {
                equal($widget.wijinputnumber("option", "value"), 124.45, "spinUp in integer field");
                $widget.simulateKeyStroke("[END][UP]", null, function () {
                    equal($widget.val(), "124.46", "spinUp in decimal field");
                    $widget.wijinputnumber('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("spinDown", function () {
            var $widget = tests.createInputNumber({
                value: 123.45
            });
            $widget.wijinputnumber("spinDown");
            equal($widget.wijinputnumber("option", "value"), 122.45, "spinDown in integer field");
            $widget.wijinputnumber("focus");
            $widget.simulateKeyStroke("[END][TAB]", null, function () {
                $widget.wijinputnumber("spinDown");
                equal($widget.wijinputnumber("option", "value"), 122.44, "spinDown in decimal field");
                $widget.wijinputnumber('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("spinUp by UI", function () {
            var $widget = tests.createInputNumber({
                value: 123.45
            });
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[HOME][DOWN]", null, function () {
                equal($widget.wijinputnumber("option", "value"), 122.45, "spinDown in integer field");
                $widget.simulateKeyStroke("[END][DOWN]", null, function () {
                    equal($widget.val(), "122.44", "spinDown in decimal field");
                    $widget.wijinputnumber('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("selectText, getSelectedText", function () {
            var $widget = tests.createInputNumber({
                value: 12345.67
            });
            $widget.wijinputnumber("selectText", 1, 3);
            setTimeout(function () {
                equal($widget.wijinputnumber("getSelectedText"), "23", "getSelectedText returns value");
                $widget.remove();
                start();
            }, 200);
            stop();
        });
        QUnit.module("wijinputnumber: events");
        test("invalidInput", function () {
            var $widget = tests.createInputNumber();
            var events = new Array();
            $widget.wijinputnumber({
                invalidInput: function (e, data) {
                    events.push("invalidInput");
                }
            });
            $widget.simulateKeyStroke("[HOME]a", null, function () {
                notEqual(events.toString().indexOf("invalidInput"), -1, "invalidInput is fired.");
                $widget.wijinputnumber('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("valueChanged", function () {
            var $widget = tests.createInputNumber();
            var events = new Array();
            $widget.wijinputnumber({
                valueChanged: function (e, data) {
                    events.push("valueChanged");
                }
            });
            $widget.wijinputnumber("option", "value", 123);
            notEqual(events.toString().indexOf("valueChanged"), -1, "valueChanged is fired.");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("valueChanged by UI", function () {
            var $widget = tests.createInputNumber();
            var events = new Array();
            $widget.wijinputnumber({
                valueChanged: function (e, data) {
                    events.push("valueChanged");
                }
            });
            $widget.simulateKeyStroke("[HOME]1", null, function () {
                notEqual(events.toString().indexOf("valueChanged"), -1, "valueChanged is fired.");
                $widget.wijinputnumber('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("dropDownOpen", function () {
            var $widget = tests.createInputNumber();
            var items = [
                {
                    label: 'Minimum',
                    value: "123"
                }, 
                {
                    label: 'Middle',
                    value: "456"
                }, 
                {
                    label: 'Maximum',
                    value: "789"
                }
            ];
            $widget.wijinputnumber("option", "comboItems", items);
            var events = new Array();
            $widget.wijinputnumber({
                dropDownOpen: function (e, data) {
                    events.push("dropDownOpen");
                }
            });
            $widget.wijinputnumber("drop");
            notEqual(events.toString().indexOf("dropDownOpen"), -1, "dropDownOpen is fired.");
            $widget.wijinputnumber("drop");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("dropDownClose", function () {
            var $widget = tests.createInputNumber();
            var items = [
                {
                    label: 'Minimum',
                    value: "123"
                }, 
                {
                    label: 'Middle',
                    value: "456"
                }, 
                {
                    label: 'Maximum',
                    value: "789"
                }
            ];
            $widget.wijinputnumber("option", "comboItems", items);
            var events = new Array();
            $widget.wijinputnumber({
                dropDownClose: function (e, data) {
                    events.push("dropDownClose");
                }
            });
            $widget.wijinputnumber("drop");
            $widget.wijinputnumber("drop");
            notEqual(events.toString().indexOf("dropDownClose"), -1, "dropDownClose is fired.");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("spinUp", function () {
            var $widget = tests.createInputNumber();
            var events = new Array();
            $widget.wijinputnumber({
                spinUp: function (e, data) {
                    events.push("spinUp");
                }
            });
            $widget.wijinputnumber("spinUp");
            notEqual(events.toString().indexOf("spinUp"), -1, "spinUp is fired.");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("spinUp by UI", function () {
            var $widget = tests.createInputNumber();
            var events = new Array();
            $widget.wijinputnumber({
                spinUp: function (e, data) {
                    events.push("spinUp");
                }
            });
            $widget.simulateKeyStroke("[UP]", null, function () {
                notEqual(events.toString().indexOf("spinUp"), -1, "spinUp is fired.");
                $widget.wijinputnumber('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("spinDown", function () {
            var $widget = tests.createInputNumber();
            var events = new Array();
            $widget.wijinputnumber({
                spinDown: function (e, data) {
                    events.push("spinDown");
                }
            });
            $widget.wijinputnumber("spinDown");
            notEqual(events.toString().indexOf("spinDown"), -1, "spinUp is fired.");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("spinDown by UI", function () {
            var $widget = tests.createInputNumber();
            var events = new Array();
            $widget.wijinputnumber({
                spinDown: function (e, data) {
                    events.push("spinDown");
                }
            });
            $widget.simulateKeyStroke("[DOWN]", null, function () {
                notEqual(events.toString().indexOf("spinDown"), -1, "spinDown is fired.");
                $widget.wijinputnumber('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("triggerMouseDown", function () {
            var $widget = tests.createInputNumber({
                showTrigger: true
            });
            var events = new Array();
            $widget.wijinputnumber({
                triggerMouseDown: function (e, data) {
                    events.push("triggerMouseDown");
                }
            });
            var triggerBtn = $(".wijmo-wijinput-trigger");
            triggerBtn.simulate("mousedown");
            notEqual(events.toString().indexOf("triggerMouseDown"), -1, "triggerMouseDown is fired.");
            $widget.wijinputnumber("drop");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("valueBoundsExceeded", function () {
            var $widget = tests.createInputNumber({
                maxValue: 7,
                minValue: 3
            });
            var events = new Array();
            $widget.wijinputnumber({
                valueBoundsExceeded: function (e, data) {
                    events.push("valueBoundsExceeded");
                }
            });
            $widget.wijinputnumber("option", "value", 9);
            equal(events.toString().indexOf("valueBoundsExceeded"), -1, "valueBoundsExceeded isn't fired.");
            $widget.wijinputnumber("option", "value", 2);
            equal(events.toString().indexOf("valueBoundsExceeded"), -1, "valueBoundsExceeded isn't fired.");
            $widget.wijinputnumber('destroy');
            $widget.remove();
        });
        test("valueBoundsExceeded by UI", function () {
            var $widget = tests.createInputNumber({
                maxValue: 7,
                minValue: 3
            });
            var events = new Array();
            $widget.wijinputnumber({
                valueBoundsExceeded: function (e, data) {
                    events.push("valueBoundsExceeded");
                }
            });
            $widget.simulateKeyStroke("[HOME]9", null, function () {
                notEqual(events.toString().indexOf("valueBoundsExceeded"), -1, "valueBoundsExceeded is fired.");
                $widget.wijinputnumber('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        //test("keyExit", function () {
        //    var $widget = tests.createInputNumber();
        //    var events = new Array();
        //    $widget.wijinputnumber({
        //        keyExit: function (e, data) {
        //            events.push("keyExit");
        //        }
        //    });
        //    $widget.wijinputnumber("focus");
        //    $widget.simulateKeyStroke("[TAB]", null, function () {
        //        notEqual(events.toString().indexOf("keyExit"), -1, "keyExit is fired.");
        //        $widget.wijinputnumber('destroy');
        //        $widget.remove();
        //        start();
        //    });
        //    stop();
        //});
        test("keyExit", function () {
            var $widget = tests.createInputNumber({
                value: 123,
                blurOnLeftRightKey: "both"
            });
            var events = new Array();
            $widget.wijinputnumber({
                keyExit: function (e, data) {
                    events.push("keyExit");
                }
            });
            $widget.wijinputnumber('focus');
            $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                equal($widget.wijinputnumber('isFocused'), false, "blurOnLastChar is default value");
                notEqual(events.toString().indexOf("keyExit"), -1, "keyExit is fired.");
                $widget.wijinputnumber('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
    })(wijmo.tests || (wijmo.tests = {}));
    var tests = wijmo.tests;
})(wijmo || (wijmo = {}));
