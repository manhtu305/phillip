﻿

$(function () {

    var widgetType = "wijinputmask";
    function BuildPropertyPage() {

        var optionsTable = document.createElement("table");
        var tbody = document.createElement("tbody");

        optionsTable.appendChild(tbody);
        var jQueryObj = $("#textbox1");
        BuildOptions(tbody, jQueryObj);

        var methodsTable = document.createElement("table");
        var tbody = document.createElement("tbody");
        methodsTable.appendChild(tbody);
        BuildMethod(tbody, jQueryObj);

        var eventsTable = document.createElement("table");
        var tbody = document.createElement("tbody");
        eventsTable.appendChild(tbody);
        BuildEvent(tbody, jQueryObj);


        optionsTable.id = "divOptions";
        methodsTable.id = "divMethod";
        eventsTable.id = "divEvent";

        optionsTable.className = "tabs";
        methodsTable.className = "tabs";
        eventsTable.className = "tabs";

        var tabContainer = document.createElement("div");
        tabContainer.className = "tabs";
        document.body.appendChild(tabContainer);

        tabContainer.appendChild(BuildTab());
        tabContainer.appendChild(optionsTable);
        tabContainer.appendChild(methodsTable);
        tabContainer.appendChild(eventsTable);
    }

    var comboItemsData = [
    { label: '1980/4/8', value: new Date(1980, 3, 8) },
    { label: '2007/12/25', value: new Date(2007, 11, 25) },
    ];

    var boolType = Boolean;

    var cssLengthType = {};

    var buttonAlignType = { _type: "enum", _data: ["right", "left"] };

    var stringListType = ["234", "232", "2342", "234", "232", "2342", "234", "232", "2342", "234", "232", "2342"];

    var anyType = {};
	
	var pickersType = {};
    var stringType = {};
	

    var callBackType = function () {
        alert("call back");
    };

    var ImeModeType = { _type: "enum", _data: ["auto", "active", "inactive", "disabled"] };

    var ExitOnLeftRightKeyType = { _type: "enum", _data: ["none", "left", "right", "both"] };

    var SpinnerAlignType = { _type: "enum", _data: ["verticalLeft", "verticalRight", "horizontalDownLeft", "horizontalUpLeft"] };

    var TabActionType = { _type: "enum", _data: ["control", "field"] };

    var HighlighTextType = {_type:"enum", _data:["none", "field", "all"]};

    var maskFormatType = { _type: "MaskFormat" };
    var readOnlyType = {};
    var widgetOptions = {
        allowPromptAsInput: boolType,
        dropDownButtonAlign: buttonAlignType,
       // comboHeight: cssLengthType,
        //comboItems: readOnlyType,
      //  comboWidth: cssLengthType,
       // create: readOnlyType,
        culture: stringType,
        disabled: boolType,

        readonly: boolType,
        hideEnter: boolType,
        hidePromptOnLeave: boolType,
        blurOnLastChar: boolType,
        imeMode: ImeModeType,
        blurOnLeftRightKey: ExitOnLeftRightKeyType,
        tabAction: TabActionType,
        highlightText:HighlighTextType,
        //spinnerAlign: SpinnerAlignType,
        invalidClass: anyType,

        placeholder: stringType,
        passwordChar: stringType,
        promptChar: stringType,
        resetOnPrompt: boolType,
        resetOnSpace: boolType,
       // showNullText: boolType,
        showDropDownButton: boolType,
       // showTrigger: boolType,
        skipLiterals: boolType,
        text: stringType,
        autoConvert: boolType,
        maskFormat: maskFormatType,
		pickers : pickersType
    };


    var rangeType = { _min: 1, _max: 100 };

    var maskFormatType = { _type: "MaskFormat" };

    var paramType = function (name, type) {
        this.name = name;
        this.type = type;
    };


    var funcType = function (needOutput, parametersObj) {
        this.needOutput = !!needOutput;
        this.parametersObj = parametersObj ? parametersObj : null;
    };
    var widgetMethods = {
        destroy: new funcType(false, null),
        focus: new funcType(false, null),
        drop: new funcType(false, null),
        getPostValue: new funcType(true, null),
        getText: new funcType(true, null),
        isDestroyed: new funcType(true, null),
        isFocused: new funcType(true, null),
        selectText: new funcType(false, [new paramType('start', 'number'), new paramType('end', 'number')]),
        getSelectedText: new funcType(true, null),
        setText: new funcType(false, [new paramType('text', 'string')]),
        widget: new funcType(true, null)
    };

    var widgetEvents = {
        initialized: "e",
        initializing: "e",
        invalidInput: "e,data",
        textChanged: "e,data",
        dropDownOpen: "e",
        dropDownClose: "e",
        dropDownButtonMouseDown: "e",
        dropDownButtonMouseUp: "e",
        keyExit:"e"
    };

    function BuildTab() {
        var ul = document.createElement("ul");
        var liOption = document.createElement("li");
        var liMethod = document.createElement("li");
        var liEvent = document.createElement("li");

        var optionA = document.createElement("a");
        var methodA = document.createElement("a");
        var eventA = document.createElement("a");

        ul.appendChild(liOption);
        ul.appendChild(liMethod);
        ul.appendChild(liEvent);

        liOption.appendChild(optionA);
        liMethod.appendChild(methodA);
        liEvent.appendChild(eventA);

        optionA.href = "#divOptions";
        optionA.appendChild(document.createTextNode("Options"));

        methodA.href = "#divMethod";
        methodA.appendChild(document.createTextNode("Method"));

        eventA.href = "#divEvent";
        eventA.appendChild(document.createTextNode("Event"));

        return ul;
    };

    function BuildOptions(tbody, jqueryObj) {

        var counter = 0;
        for (var propertyName in widgetOptions) {
            if (counter % 3 === 0 || propertyName == "pickers") {
                var tr = document.createElement("tr");
                tbody.appendChild(tr);
            }

            counter++;
            var td = document.createElement("td");
            td.className = "inputTh";
            //td.style.borderWidth = "2px";
            //td.style.borderStyle = "solid";
            //td.style.borderColor = "lightblue";

            tr.appendChild(td);

            if (propertyName == "create" || propertyName == "comboItems") {
                td.appendChild(document.createTextNode(propertyName));
            }
			else if(propertyName == "pickers")
            {
                var table = $("<table/>")
                    .css({ "width" : "100%" })
                    .appendTo(td);

                var tr2 = $("<tr>").appendTo(table);
                var td3 = $("<td>")
                .css({ "width": "100%" })
                .attr({ "colspan": 2 })
                .appendTo(tr2);
                
                var optionBtn = $("<button>")
                    .addClass("myButton")
                    .css({ "color" : "blue", "width" : "100%" })
                    .appendTo(td3)
                    .append(document.createTextNode(propertyName));

                var tr1 = $("<tr>").appendTo(table);

                var td1 = $("<td>")
                    .css({ "width" : "50%" })
                    .appendTo(tr1);

                var td2 = $("<td>")
                    .css({ "width" : "50%" })
                    .appendTo(tr1);

                var setBtn = $("<button>")
                    .css({ "color":"blue", "width":"100%"})
                    .addClass("myButton")
                    .appendTo(td2)
                    .append(document.createTextNode("Set"))
                    .bind('click', function () {
                        var js = "var picker = " + getPickersString();
						try
						{
							eval(js);
							jqueryObj.wijinputmask("option", propertyName, picker);
						}
						catch(e)
						{
							alert(e.message);
						} 
                        
                    });
                
                var getBtn = $("<button>")
                    .addClass("myButton")
                    .css({ "color": "blue", "width": "100%" })
                    .appendTo(td1)
                    .append(document.createTextNode("Get"))
                    .bind('click', function () {
                        getOptionValue(widgetOptions[propertyName], propertyName, jqueryObj);
                    });
            }
            else {
                var optionBtn = $("<button>");
                optionBtn.addClass("myButton");
                if (IsNewFeature(propertyName)) {
                    optionBtn.css("color", "blue");
                }

                //optionBtn.css("width", "150px");
                optionBtn.append(document.createTextNode(propertyName));
                td.appendChild(optionBtn.get(0));

                addOptionBtnHandler(widgetOptions[propertyName], propertyName, jqueryObj, optionBtn);
            }


            var tdEdit = document.createElement("td");
            tdEdit.className = "inputTd";

            tr.appendChild(tdEdit);
            var editControl = createEditControls(widgetOptions[propertyName], propertyName, jqueryObj);
            if (editControl) {
                if (editControl.tagName.toLowerCase() === "table") {
                    $("input[type='text']", editControl).get(0).id = propertyName + "_Editor";
                    $("input[type='text']", editControl).get(1).id = propertyName + "_Editor2";
                    tdEdit.colSpan = "3";
                } else {
                    editControl.id = propertyName + "_Editor";
					if(propertyName == "pickers"){
						tdEdit.setAttribute("colspan",5);
					}
				}
                tdEdit.appendChild(editControl);
            }
        }



        function addOptionBtnHandler(type, propertyName, obj, btn) {
            btn.bind('click', function () {
                getOptionValue(type, propertyName, obj);
            });
        }

        function getOptionValue(type, propertyName, obj) {
            var editor;
            var value;
			

			
            if (propertyName === "maskFormat") {
                value = obj[widgetType]("option", propertyName);
                if (value instanceof RegExp) {
                    editor = $("#" + propertyName + "_Editor2");
                    $("#" + propertyName + "_Editor").val("");
                } else {
                    $("#" + propertyName + "_Editor2").val("");
                    editor = $("#" + propertyName + "_Editor");
                }

            } else {
                editor = $("#" + propertyName + "_Editor");
            }
			
			if(propertyName == "pickers")
            {
                editor.val(getDefaultPickersString());
				return;
            }
			
            value = obj[widgetType]("option", propertyName);
            if (value === null) {
                value = "null";
            }

            if (value === "") {
                value = "empty";
            }

            if (value.offset) {
                value = value.offset;
            }

            if (value instanceof Date) {
                value = value.getFullYear() + "/" + (value.getMonth() + 1) + "/" + value.getDate() + " " + value.getHours() + ":" + value.getMinutes() + ":" + value.getSeconds();
            }
            if (editor.attr("type") == "checkbox") {
                editor.attr("checked", value === true);
                return;
            }
			

			
            editor.val(value);
        }
    };

    function createEditControls(type, propertyName, obj) {
        if (type === Boolean) {
            var input = $("<input>");
            input.attr("type", "checkbox");
            input.bind("change", function () {
                obj[widgetType]("option", propertyName, this.checked);
            });
            return input.get(0);
        }

        if (type._type === maskFormatType._type) {
            return BuildMaskFormatType(obj);
        }

        if (type._type && type._type === 'enum') {
            var options = $("<select>");
            options.css("width", "150px");
            for (var i = 0; i < type._data.length; i++) {
                var opt = document.createElement("option");
                opt.value = type._data[i];
                opt.appendChild(document.createTextNode(type._data[i]));
                options.get(0).appendChild(opt);
            }
            options.bind("change", function () {
                obj[widgetType]("option", propertyName, this[this.selectedIndex].text);
            });
            return options.get(0);
        }

        if (type === cssLengthType) {
            var input = $("<input>");
            input.attr("type", "range");
            input.attr("min", cssLengthType._min);
            input.attr("max", cssLengthType._max);
            input.bind("change", function () {
                obj[widgetType]("option", propertyName, this.value + "px");
            });
            return input.get(0);
        }
        if (type === rangeType) {
            var input = $("<input>");
            input.bind("change", function () {
                obj[widgetType]("option", propertyName, parseFloat(this.value));
            });
            return input.get(0);
        }
		if(type === pickersType){
			var input = $("<textarea id= 'pickerArea'>")
			        .css({ "width": "100%", "height": "110px" });

			var text = getDefaultPickersString();
			input.val(text);
            return input.get(0);
		}
        if (type === stringType) {
            var input = $("<input>");
            input.attr("type", "text");
            input.bind("change", function () {
                obj[widgetType]("option", propertyName, this.value);
            });
            return input.get(0);
        }
        if (type === anyType) {
            var input = $("<input>");
            input.attr("type", "text");
            input.bind("change", function () {
                obj[widgetType]("option", propertyName, this.value);
            });
            return input.get(0);
        }
        if (type === stringListType) {
            setTimeout(function () {
                obj[widgetType]("option", propertyName, comboItemsData);
            }, 0);
            return null;

        }
        if (type === callBackType) {
            setTimeout(function () {
                obj[widgetType]("option", propertyName, callBackType);
            }, 0);
            return null;

        }

    };

    function BuildMaskFormatType(obj) {
        var table = document.createElement("table");
        var tbody = document.createElement("tbody");
        var tr1 = document.createElement("tr");
        var tr2 = document.createElement("tr");
        var tr3 = document.createElement("tr");
        var tr4 = document.createElement("tr");
        var td1 = document.createElement("td");
        var td2 = document.createElement("td");
        var td3 = document.createElement("td");
        var td4 = document.createElement("td");
        table.appendChild(tbody);
        tbody.appendChild(tr1);
        tbody.appendChild(tr2);
        tbody.appendChild(tr3);
        tbody.appendChild(tr4);
        tr1.appendChild(td1);
        tr2.appendChild(td2);
        tr3.appendChild(td3);
        tr4.appendChild(td4);

        td1.appendChild(document.createTextNode("simple mode"));

        var input = document.createElement("input");
        input.type = "text";
        input.style.width = "300px";

        td1.appendChild(input);

        $(input).bind("change", function () {
            obj.wijinputmask("option", "maskFormat", this.value);
        });



        var keyWords = [
            { keyword: "0", desc: "Digit, required. This element will accept any single digit between 0 and 9." },
            { keyword: "9", desc: "Digit, optional." },
            { keyword: "#", desc: "Digit, optional. Plus '+' and minus '-' signs are allowed." },
            { keyword: "L", desc: "Letter, required. Restricts input to the ASCII letters a-z and A-Z. This mask element is equivalent to [a-zA-Z] in regular expressions." },
            { keyword: "?", desc: "Letter, optional. Restricts input to the ASCII letters a-z and A-Z. This mask element is equivalent to [a-zA-Z]? in regular expressions." },
            { keyword: "&", desc: "Character, required." },
            { keyword: "C", desc: "Character, optional. Any non-control character." },
            { keyword: "A", desc: "Alphanumeric, required." },
            { keyword: "a", desc: "Alphanumeric, optional." },
            { keyword: ".", desc: "Decimal placeholder. The actual display character used will be the decimal placeholder appropriate to the culture option." },
            { keyword: ",", desc: "Thousands placeholder. The actual display character used will be the thousands placeholder appropriate to the culture option." },
            { keyword: ":", desc: "Time separator. The actual display character used will be the time placeholder appropriate to the culture option." },
            { keyword: "/", desc: "Date separator. The actual display character used will be the date placeholder appropriate to the culture option." },
            { keyword: "$", desc: "Currency symbol. The actual character displayed will be the currency symbol appropriate to the culture option." },
            { keyword: "<", desc: "Shift down. Converts all characters that follow to lowercase." },
            { keyword: ">", desc: "Shift up. Converts all characters that follow to uppercase." },
            { keyword: "|", desc: "Disable a previous shift up or shift down." },
            { keyword: "\\", desc: "Escape. Escapes a mask character, turning it into a literal. The escape sequence for a backslash is: \\\\" },
            { isNew: true, keyword: "K", desc: "Matches SBCS Katakana, required." },
            { isNew: true, keyword: "H", desc: "Matches all SBCS characters, required." },
            { isNew: true, keyword: "Ａ", desc: "Matches DBCS Alphanumeric, required." },
            { isNew: true, keyword: "９", desc: "DBCS Digit, required. This element will accept any single DBCS digit between 0 and 9." },
            { isNew: true, keyword: "Ｋ", desc: "DBCS Katakana, required." },
            { isNew: true, keyword: "Ｊ", desc: "Hiragana, required." },
            { isNew: true, keyword: "Ｚ", desc: "All DBCS characters, required." },
            { isNew: true, keyword: "N", desc: "Matches all SBCS big Katakana"},
            { isNew: true, keyword: "Ｎ", desc: "Matches DBCS big Katakana." },
            { isNew: true, keyword: "Ｇ", desc: "Matches DBCS big Hiragana." }
        ];



        var counter = 0;
        for (var j = 0; j < keyWords.length; j++) {
            if (keyWords[j].desc.toLowerCase().indexOf("optional") !== -1) {
                continue;
            }
            counter++;
            var opt = document.createElement("input");
            opt.type = "button";
            opt.value = opt.text = keyWords[j].keyword;
            opt.title = keyWords[j].desc;
            opt.style.width = "28px";
            opt.style.marginLeft = "3px";

            if (keyWords[j].isNew) {
                opt.style.color = "blue";
            }
            $(opt).bind("click", function () {
                var oldMaskValue = obj.wijinputmask("option", "maskFormat");
                if (oldMaskValue instanceof RegExp) {
                    oldMaskValue = "";
                }
                oldMaskValue += this.value;
                input.value = oldMaskValue;
                obj.wijinputmask("option", "maskFormat", oldMaskValue);
            });

            td2.appendChild(opt);

            if (counter % 10 === 0) {
                td2.appendChild(document.createElement("br"));
            }
        }


        var sel = document.createElement("select");
        sel.multiple = "multiple";

        var formatPatterns = ["\\D{3}", "℡ \\D{3}-\\D{4}", "\\D{3}-\\D{4}", "℡ \\D{2,4}-\\D{2,4}-\\D{4}", "\\Ｊ*", "\\Ｋ*"];

        for (var i = 0; i < formatPatterns.length; i++) {

            var opt = document.createElement("option");
            opt.text = formatPatterns[i];
            opt.innerText = opt.text;
            sel.appendChild(opt);
        }

        sel.style.height = "16" * formatPatterns.length + "px";

        $(sel).bind("change", function () {
            obj.wijinputmask("option", "maskFormat", new RegExp(sel[sel.selectedIndex].text));
        });


        var iMformatPattern = document.createElement("input");
        iMformatPattern.type = "text";


        $(iMformatPattern).bind("change", function () {
            obj.wijinputmask("option", "maskFormat", new RegExp(iMformatPattern.value));
        });

        td3.appendChild(document.createTextNode("advanced mode"));
        td3.appendChild(iMformatPattern);

        td4.appendChild(sel);

        return table;
    }


    function IsNewFeature(name) {
        if (name == "blurOnLastChar"
        || name == "blurOnLeftRightKey"
        || name == "imeMode"
        || name == "tabAction"
        || name == "highlightText"
        || name == "spinnerAlign"
        || name == "spinAllowWrap"
        || name == "drop"
        || name == "spinDown"
        || name == "spinUp"
        || name == "dropDownOpen"
        || name == "dropDownClose"
        || name == "dropdownButtonAlign"
        || name === "readonly"
        || name === "placeholder"
        || name === "showDropdownButton"
        || name === "autoConvert"
        || name === "maskFormat"
        || name === "dropDownButtonMouseDown"
        || name === "dropDownButtonMouseUp"
        || name === "keyExit"
        || name === "getSelectedText"
        ) {
            return true;
        }

        return false;
    };

	function getDefaultPickersString()
    {
        var text = "{\r\n" +
                            "   width           :       undefined,\r\n" +
                            "   height          :       undefined,\r\n" +
                            "   list            :   [    '1001.12' ,  '300',  '122.34' ] \r\n" + 
                            "}";

        return text;
    }

    function getPickersString()
    {
        return $("#pickers_Editor").val();
    }
	
    function BuildMethod(tbody, jqueryObj) {
        var counter = 0;
        for (var methodName in widgetMethods) {
            if (counter % 2 === 0) {
                var tr = document.createElement("tr");
                tbody.appendChild(tr);
            }

            counter++;
            var td = document.createElement("td");
            td.className = "inputTh";
            //td.style.backgroundColor = "lightblue";
            //td.style.width = "160px";
            //td.style.textAlign = "center";
            tr.appendChild(td);

            var callBtn = $("<button>");
            callBtn.addClass("myButton");
            if (IsNewFeature(methodName)) {
                callBtn.css("color", "blue");
            }
            //callBtn.css("width", "150px");
            callBtn.append(document.createTextNode(methodName));
            td.appendChild(callBtn.get(0));
            var tdEdit = document.createElement("td");
            tdEdit.className = "inputTd";
            //tdEdit.style.backgroundColor = "lightgreen";
            //tdEdit.style.width = "160px";
            tr.appendChild(tdEdit);
            var editControl = createEditControls(widgetMethods[methodName], methodName);
            if (editControl) {
                tdEdit.appendChild(editControl);
            }

            addCallBtnHandler(widgetMethods[methodName], methodName, jqueryObj, callBtn);
        }

        function createEditControls(type, methodName) {
            var contentDiv = document.createElement("div");
            if (type.parametersObj) {
                for (var index = 0; index < type.parametersObj.length; index++) {
                    var input = document.createElement('input');
                    input.id = methodName + "_param" + index;
                    contentDiv.appendChild(document.createTextNode(type.parametersObj[index].name + ': '));
                    contentDiv.appendChild(input);
                }
            }
            if (type.needOutput) {
                var output = document.createElement("input");
                output.disabled = 'disabled';
                output.id = methodName + "_output";
                contentDiv.appendChild(document.createTextNode('output: '));
                contentDiv.appendChild(output);
            }
            return contentDiv;
        };

        function addCallBtnHandler(type, methodName, obj, btn) {
            btn.bind('click', function () {
                setTimeout(function () {
                    doMethod(type, methodName, obj);
                }, 200);

            });
        }

        function doMethod(type, methodName, obj) {
            var params = [];
            if (type.parametersObj) {
                for (var index = 0; index < type.parametersObj.length; index++) {
                    var paramData = document.getElementById(methodName + "_param" + index).value;
                    if (type.parametersObj[index].type === 'number') {
                        paramData = parseFloat(paramData);
                    }
                    params.push(paramData);
                }
            }

            var result = '';
            switch (params.length) {
                case 0:
                    result = obj[widgetType](methodName);
                    break;
                case 1:
                    result = obj[widgetType](methodName, params[0]);
                    break;
                case 2:
                    result = obj[widgetType](methodName, params[0], params[1]);
                    break;
                case 3:
                    result = obj[widgetType](methodName, params[0], params[1], params[2]);
                    break;
                case 4:
                    result = obj[widgetType](methodName, params[0], params[1], params[2], params[3]);
                    break;
                default:
            }

            if (type.needOutput) {
                var output = document.getElementById(methodName + "_output");
                output.value = result;
            }
        }
    };

    function BuildEvent(tbody, jqueryObj) {
        var counter = 0;
        for (var eventName in widgetEvents) {
            if (counter % 2 === 0) {
                var tr = document.createElement("tr");
                tbody.appendChild(tr);
            }

            counter++;
            var td = document.createElement("td");
            td.className = "inputTh";
            //td.style.backgroundColor = "lightblue";
            //td.style.width = "160px";
            //td.style.textAlign = "center";
            tr.appendChild(td);

            var addBtn = $("<button>");
            addBtn.addClass("myButton");
            if (IsNewFeature(eventName)) {
                addBtn.css("color", "blue");
            }
            //addBtn.css("width", "150px");
            addBtn.append(document.createTextNode(eventName));
            td.appendChild(addBtn.get(0));

            var tdEventArea = document.createElement("td");
            //tdEventArea.style.backgroundColor = "lightgreen";
            //tdEventArea.style.width = "300px";
            //tdEventArea.style.height = "80px";
            tdEventArea.className = "inputTd";
            tr.appendChild(tdEventArea);

            var eventArea = document.createElement("textarea");
            //eventArea.style.width = "200px";
            eventArea.style.height = "65px";
            eventArea.className = "myTextArea";
            eventArea.id = eventName + "Content";

            var eventString = "function(" + widgetEvents[eventName] + ") \n{ \n    console.log(\"" + eventName + "\"); \n}";
            eventArea.appendChild(document.createTextNode(eventString));
            tdEventArea.appendChild(eventArea);

            addAddBtnHandler(widgetEvents[eventName], eventName, jqueryObj, addBtn);
        }


        function addAddBtnHandler(type, methodName, obj, btn) {
            btn.bind('click', function () {
                doEvent(type, methodName, obj);
            });
        }

        function doEvent(type, eventName, obj) {

            var eventFunction = document.getElementById(eventName + "Content").value;
            var fun = CheckFunction(eventFunction);
            obj[widgetType]("option", eventName, fun);
        }


        function Trim(value) {
            if (value == "") {
                return "";
            }

            var beginIndex = 0;
            var endIndex = 0;
            for (var i = 0; i < value.length; i++) {
                if (value.CharAt(i) != " " && value.CharAt(i) != "　") {
                    beginIndex = i;
                    break;
                }
            }

            for (var i = value.length - 1; i >= 0; i--) {
                if (value.CharAt(i) != " " && value.CharAt(i) != "　") {
                    endIndex = i + 1;
                    break;
                }
            }

            try {
                var s = value.Substring(beginIndex, endIndex);
                return s;
            }
            catch (e) {
                return value;
            }


        };

        function CheckFunction(fun) {
            if (fun === undefined || fun === null) {
                return null;
            }

            if (typeof fun == "string") {
                //fun = Trim(fun);
                if (fun.length == 0) {
                    return null;
                }
                try {
                    eval("fun =" + fun);
                }
                catch (e) {
                }
            }

            if (typeof fun == "function") {
                return fun;
            }
            else {
                throw "The value is not a valid function";
            }
        };

    };

    BuildPropertyPage();

    setTimeout(function () {
        $(".tabs").wijtabs();
    }, 0);
});
