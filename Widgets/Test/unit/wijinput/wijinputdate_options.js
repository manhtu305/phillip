﻿var wijmo;
(function (wijmo) {
    (function (tests) {
        var $ = jQuery;
        QUnit.module("wijinputdate: options");
        test("activeField", function () {
            var $widget = tests.createInputDate({
                activeField: 2
            });
            ok($widget.wijinputdate("option", "activeField") == 2, "activeField is 2");
            $widget.wijinputdate("option", "activeField", 0);
            ok($widget.wijinputdate("option", "activeField") == 0, "activeField is 0");
            $widget.wijinputdate("option", "activeField", 1);
            ok($widget.wijinputdate("option", "activeField") == 1, "activeField is 1");
            $widget.wijinputdate("option", "activeField", 2);
            ok($widget.wijinputdate("option", "activeField") == 2, "activeField is 2");
            $widget.wijinputdate("option", "activeField", 3);
            ok($widget.wijinputdate("option", "activeField") == 2, "activeField is 2");
            $widget.wijinputdate("option", "activeField", 10);
            ok($widget.wijinputdate("option", "activeField") == 2, "activeField is " + $widget.wijinputdate("option", "activeField"));
            $widget.wijinputdate("option", "activeField", -1);
            ok($widget.wijinputdate("option", "activeField") == 0, "activeField is 0");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("autoNextField", function () {
            var $widget = tests.createInputDate({
                date: new Date(2001, 0, 1),
                displayFormat: ""
            });
            ok($widget.wijinputdate("option", "autoNextField") == true, "autoNextField is true");
            $widget.simulateKeyStroke("[HOME]351999", null, function () {
                equal($widget.val(), "3/5/1999", "Date input ok");
                $widget.wijinputdate("option", "autoNextField", false);
                ok($widget.wijinputdate("option", "autoNextField") == false, "autoNextField is false");
                $widget.simulateKeyStroke("[HOME]351999", null, function () {
                    equal($widget.val(), "9/5/1999", "Date input ok");
                    $widget.wijinputdate('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("culture", function () {
            var $widget = tests.createInputDate({
                dateFormat: "tt hh:mm:ss",
                date: new Date(2011, 0, 1, 0, 0, 0),
                displayFormat: ""
            });
            ok($widget.wijinputdate("option", "culture") == "", "culture is default value");
            equal($widget.val(), "AM 12:00:00", "culture is default value");
            $widget.wijinputdate("option", "culture", "ja-JP");
            ok($widget.wijinputdate("option", "culture") == "ja-JP", "culture is ja-JP");
            equal($widget.val(), "午前 12:00:00", "culture is ja-JP");
            $widget.wijinputdate("option", "culture", "zh-CN");
            ok($widget.wijinputdate("option", "culture") == "zh-CN", "culture is zh-CN");
            equal($widget.val(), "上午 12:00:00", "culture is zh-CN");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("date, dateformat", function () {
            var $widget = tests.createInputDate({
                date: "8/15/2010",
                displayFormat: ""
            });
            equal($widget.val(), '8/15/2010', "Date is applied");
            $widget.wijinputdate("option", "dateFormat", "D");
            ok($widget.wijinputdate("option", "dateFormat") == "D", "culture is default value");
            equal($widget.val(), 'Sunday, August 15, 2010', "DateFormat is applied");
            $widget.wijinputdate("option", "dateFormat", "ggg ee/MM/dd");
            ok($widget.wijinputdate("option", "dateFormat") == "ggg ee/MM/dd");
            equal($widget.val(), '平成 22/08/15', "DateFormat is applied");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("maxDate", function () {
            var $widget = tests.createInputDate({
                displayFormat: ""
            });
            ok($widget.wijinputdate("option", "maxDate") == null, "maxDate is default value");
            $widget.wijinputdate("option", "maxDate", "12/31/2013");
            ok($widget.wijinputdate("option", "maxDate").toDateString() == "Tue Dec 31 2013", "maxDate is 12/31/2013");
            $widget.wijinputdate("option", "date", "1/1/2014");
            equal($widget.val(), "12/31/2013", "maxDate is 12/31/2013");
            $widget.wijinputdate("option", "date", "12/1/2013");
            equal($widget.val(), "12/1/2013", "maxValue is 12/31/2013");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("minDate", function () {
            var $widget = tests.createInputDate({
                displayFormat: ""
            });
            ok($widget.wijinputdate("option", "minDate") == null, "minValue is default value");
            $widget.wijinputdate("option", "minDate", "1/1/2013");
            equal($widget.wijinputdate("option", "minDate").toDateString(), "Tue Jan 01 2013", "minDate is 1/1/2013");
            $widget.wijinputdate("option", "date", "12/1/2012");
            equal($widget.val(), "1/1/2013", "maxDate is 1/1/2013");
            $widget.wijinputdate("option", "date", "2/1/2013");
            equal($widget.val(), "2/1/2013", "maxDate is 1/1/2013");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("placeholder", function () {
            var $widget = tests.createInputDate({
                date: null
            });
            ok($widget.wijinputdate("option", "placeholder") == null, "placeholder is default value");
            $widget.wijinputdate("option", "placeholder", "");
            ok($widget.wijinputdate("option", "placeholder") == "", "placeholder is test");
            $widget.wijinputdate("option", "placeholder", "test");
            ok($widget.wijinputdate("option", "placeholder") == "test", "placeholder is test");
            equal($widget.wijinputdate("getText"), "test", "placeholder is test");
            $widget.wijinputdate("option", "placeholder", "あいうえお");
            ok($widget.wijinputdate("option", "placeholder") == "あいうえお", "placeholder is あいうえお");
            equal($widget.wijinputdate("getText"), "あいうえお", "placeholder is あいうえお");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("popupPosition", function () {
            var $widget = tests.createInputDate();
            equal($widget.wijinputdate("option", "popupPosition").offset, "0 4", "popupPosition is default value");
            $widget.wijinputdate("option", "popupPosition", {
                offset: '10 10'
            });
            equal($widget.wijinputdate("option", "popupPosition").offset, "10 10", "popupPosition is 10 10");
            $widget.wijinputdate("option", "popupPosition", {
                offset: '-1 -1'
            });
            equal($widget.wijinputdate("option", "popupPosition").offset, "-1 -1", "popupPosition is -1 -1");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("readonly", function () {
            var $widget = tests.createInputDate({
                date: "1/1/2012"
            });
            ok($widget.wijinputdate("option", "readonly") == false, "readOnly is default value");
            $widget.wijinputdate('focus');
            $widget.simulateKeyStroke("2", null, function () {
                equal($widget.val(), "2/1/2012", "readOnly is default value");
                $widget.wijinputdate("option", "readonly", true);
                ok($widget.wijinputdate("option", "readonly") == true, "readOnly is true");
                $widget.simulateKeyStroke("3", null, function () {
                    equal($widget.val(), "2/1/2012", "readOnly is true");
                    $widget.wijinputdate('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("input in date", function () {
            var $widget = tests.createInputDate({
                dateFormat: "yyyy/MM/dd"
            });
            $widget.wijinputdate('focus');
            $widget.simulateKeyStroke("[HOME]20081213", null, function () {
                ok($widget.val() == "2008/12/13", "Date input ok");
                $widget.simulateKeyStroke("[UP][UP][DOWN][DOWN][DOWN]", null, function () {
                    ok($widget.val() == "2008/12/12", "Date spin ok");
                    $widget.wijinputdate('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("allowSpinLoop", function () {
            var $widget = tests.createInputDate({
                maxDate: "12/31/2013",
                minDate: "1/1/2013",
                date: "12/31/2013",
                displayFormat: ""
            });
            ok($widget.wijinputdate("option", "allowSpinLoop") == false, "allowSpinLoop is default value");
            $widget.wijinputdate("spinUp");
            equal($widget.val(), "12/31/2013", "culture is default value");
            $widget.wijinputdate("option", "allowSpinLoop", true);
            ok($widget.wijinputdate("option", "allowSpinLoop") == true, "allowSpinLoop is true");
            $widget.wijinputdate("spinUp");
            equal($widget.val(), "1/1/2013", "allowSpinLoop is true");
            $widget.wijinputdate("spinDown");
            equal($widget.val(), "12/31/2013", "allowSpinLoop is true");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("allowSpinLoop for UI", function () {
            var $widget = tests.createInputDate({
                maxDate: "12/31/2013",
                minDate: "1/1/2013",
                date: "12/31/2013",
                displayFormat: ""
            });
            ok($widget.wijinputdate("option", "allowSpinLoop") == false, "allowSpinLoop is default value");
            $widget.wijinputdate('focus');
            $widget.simulateKeyStroke("[UP]", null, function () {
                equal($widget.val(), "12/31/2013", "allowSpinLoop is default value");
                $widget.wijinputdate("option", "allowSpinLoop", true);
                ok($widget.wijinputdate("option", "allowSpinLoop") == true, "allowSpinLoop is true");
                $widget.simulateKeyStroke("[UP]", null, function () {
                    equal($widget.val(), "1/1/2013", "allowSpinLoop is true");
                    $widget.simulateKeyStroke("[DOWN]", null, function () {
                        equal($widget.val(), "12/31/2013", "allowSpinLoop is true");
                        $widget.wijinputdate('destroy');
                        $widget.remove();
                        start();
                    });
                });
            });
            stop();
        });
        test("amDesignator", function () {
            var $widget = tests.createInputDate({
                dateFormat: "tt hh:mm:ss",
                date: new Date(2012, 0, 1, 1, 0, 0),
                displayFormat: ""
            });
            ok($widget.wijinputdate("option", "amDesignator") == "AM", "amDesignator is default value");
            equal($widget.val(), "AM 01:00:00", "amDesignator is default value");
            $widget.wijinputdate("option", "culture", "ja-JP");
            ok($widget.wijinputdate("option", "amDesignator") == "午前", "amDesignator is 午前");
            equal($widget.val(), "午前 01:00:00", "amDesignator is 午前");
            $widget.wijinputdate("option", "amDesignator", "AA");
            ok($widget.wijinputdate("option", "amDesignator") == "AA", "amDesignator is AA");
            equal($widget.val(), "AA 01:00:00", "amDesignator is AA");
            $widget.wijinputdate("option", "amDesignator", "あい");
            ok($widget.wijinputdate("option", "amDesignator") == "あい", "amDesignator is あい");
            equal($widget.val(), "あい 01:00:00", "amDesignator is あい");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("blurOnLastChar", function () {
            var $widget = tests.createInputDate();
            ok($widget.wijinputdate("option", "blurOnLastChar") == false, "blurOnLastChar is default value");
            $widget.wijinputdate('focus');
            $widget.simulateKeyStroke("[END]2012", null, function () {
                equal($widget.wijinputdate('isFocused'), true, "blurOnLastChar is default value");
                $widget.wijinputdate("option", "date", null);
                $widget.wijinputdate("option", "blurOnLastChar", true);
                ok($widget.wijinputdate("option", "blurOnLastChar") == true, "blurOnLastChar is true");
                $widget.simulateKeyStroke("[END]2014", null, function () {
                    equal($widget.wijinputdate('isFocused'), false, "blurOnLastChar is false");
                    $widget.wijinputdate('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("blurOnLeftRightKey is none", function () {
            var $widget = tests.createInputDate();
            ok($widget.wijinputdate("option", "blurOnLeftRightKey") == "none", "blurOnLeftRightKey is default value");
            $widget.wijinputdate('focus');
            $widget.simulateKeyStroke("[HOME][LEFT]", null, function () {
                equal($widget.wijinputdate('isFocused'), true, "blurOnLeftRightKey is default value");
                $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                    equal($widget.wijinputdate('isFocused'), true, "blurOnLeftRightKey is default value");
                    $widget.wijinputdate('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("blurOnLeftRightKey is right", function () {
            var $widget = tests.createInputDate();
            $widget.wijinputdate("option", "blurOnLeftRightKey", "right");
            ok($widget.wijinputdate("option", "blurOnLeftRightKey") == "right", "blurOnLeftRightKey is right");
            $widget.wijinputdate('focus');
            $widget.simulateKeyStroke("[HOME][LEFT]", null, function () {
                equal($widget.wijinputdate('isFocused'), true, "blurOnLeftRightKey is right");
                $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                    equal($widget.wijinputdate('isFocused'), false, "blurOnLeftRightKey is right");
                    $widget.wijinputdate('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("blurOnLeftRightKey is left", function () {
            var $widget = tests.createInputDate();
            $widget.wijinputdate("option", "blurOnLeftRightKey", "left");
            ok($widget.wijinputdate("option", "blurOnLeftRightKey") == "left", "blurOnLeftRightKey is left");
            $widget.wijinputdate('focus');
            $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                equal($widget.wijinputdate('isFocused'), true, "blurOnLastChar is left");
                $widget.simulateKeyStroke("[HOME][LEFT]", null, function () {
                    equal($widget.wijinputdate('isFocused'), false, "blurOnLastChar is left");
                    $widget.wijinputdate('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("blurOnLeftRightKey is both", function () {
            var $widget = tests.createInputDate();
            $widget.wijinputdate("option", "blurOnLeftRightKey", "both");
            ok($widget.wijinputdate("option", "blurOnLeftRightKey") == "both", "blurOnLeftRightKey is default value");
            $widget.wijinputdate('focus');
            $widget.simulateKeyStroke("[HOME][LEFT]", null, function () {
                equal($widget.wijinputdate('isFocused'), false, "blurOnLastChar is default value");
                $widget.wijinputdate('focus');
                $widget.simulateKeyStroke("[END][RIGHT]", null, function () {
                    equal($widget.wijinputdate('isFocused'), false, "blurOnLastChar is false");
                    $widget.wijinputdate('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("date, displayformat", function () {
            var $widget = tests.createInputDate({
                date: "8/15/2010"
            });
            ok($widget.wijinputdate("option", "displayFormat") == "", "displayformat is default value");
            equal($widget.val(), '8/15/2010', "Date is applied");
            $widget.wijinputdate("option", "displayFormat", "D");
            ok($widget.wijinputdate("option", "displayFormat") == "D", "displayformat is default value");
            equal($widget.val(), 'Sunday, August 15, 2010', "Date is applied");
            $widget.wijinputdate("option", "displayFormat", "d");
            ok($widget.wijinputdate("option", "displayFormat") == "d", "displayformat is default value");
            equal($widget.val(), '8/15/2010', "DateFormat is applied");
            $widget.wijinputdate("option", "displayFormat", "ggg ee/MM/dd");
            ok($widget.wijinputdate("option", "displayFormat") == "ggg ee/MM/dd");
            equal($widget.val(), '平成 22/08/15', "displayformat is applied");
            $widget.wijinputdate("option", "displayFormat", "A yyyy/MM/dd");
            ok($widget.wijinputdate("option", "displayFormat") == "A yyyy/MM/dd");
            equal($widget.val(), 'A.D. 2010/08/15', "displayformat is applied");
            $widget.wijinputdate("option", "displayFormat", "ggg E/MM/dd");
            $widget.wijinputdate("option", "date", new Date(1989, 11, 26));
            ok($widget.wijinputdate("option", "displayFormat") == "ggg E/MM/dd");
            equal($widget.val(), '平成 元/12/26', "displayformat is applied");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("hour12As0", function () {
            var $widget = tests.createInputDate({
                dateFormat: "tt hh:mm:ss",
                date: new Date(2012, 0, 13, 12, 0, 0),
                displayFormat: ""
            });
            ok($widget.wijinputdate("option", "hour12As0") == false, "hour12As0 is default value");
            equal($widget.val(), "PM 12:00:00", "hour12As0 is default value");
            $widget.wijinputdate("option", "hour12As0", true);
            ok($widget.wijinputdate("option", "hour12As0") == true, "hour12As0 is true");
            equal($widget.val(), "PM 00:00:00", "hour12As0 is default value");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("midnightAs0", function () {
            var $widget = tests.createInputDate({
                dateFormat: "HH:mm:ss",
                date: new Date(2012, 0, 13, 0, 0, 0),
                displayFormat: ""
            });
            ok($widget.wijinputdate("option", "midnightAs0") == true, "midnightAs0 is default value");
            equal($widget.val(), "00:00:00", "midnightAs0 is default value");
            $widget.wijinputdate("option", "midnightAs0", false);
            ok($widget.wijinputdate("option", "midnightAs0") == false, "midnightAs0 is false");
            equal($widget.val(), "24:00:00", "midnightAs0 is false");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pickers.calendar", function () {
            var $widget = tests.createInputDate();
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.calendar == undefined, "pickers.calendar is default value");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pickers.calendar.allowQuickPick", function () {
            var $widget = tests.createInputDate({
                pickers: {
                    calendar: {
                        allowQuickPick: true
                    }
                }
            });
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.calendar.allowQuickPick == true, "pickers.calendar.allowQuickPick is default value");
            pickers.calendar.allowQuickPick = false;
            ok(pickers.calendar.allowQuickPick == false, "pickers.calendar.allowQuickPick is false");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pickers.calendar.navButtons", function () {
            var $widget = tests.createInputDate({
                pickers: {
                    calendar: {
                        navButtons: 'default'
                    }
                }
            });
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.calendar.navButtons == 'default', "pickers.calendar.navButtons is default value");
            pickers.calendar.navButtons = 'quick';
            ok(pickers.calendar.navButtons == 'quick', "pickers.calendar.navButtons is quick");
            pickers.calendar.navButtons = 'none';
            ok(pickers.calendar.navButtons == 'none', "pickers.calendar.navButtons is none");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pickers.calendar.nextTooltip", function () {
            var $widget = tests.createInputDate({
                pickers: {
                    calendar: {
                        nextTooltip: 'Next'
                    }
                }
            });
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.calendar.nextTooltip == 'Next', "pickers.calendar.nextTooltip is default value");
            pickers.calendar.nextTooltip = 'abc';
            ok(pickers.calendar.nextTooltip == 'abc', "pickers.calendar.nextTooltip is abc");
            pickers.calendar.nextTooltip = 'あいう';
            ok(pickers.calendar.nextTooltip == 'あいう', "pickers.calendar.nextTooltip is あいう");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pickers.calendar.prevTooltip", function () {
            var $widget = tests.createInputDate({
                pickers: {
                    calendar: {
                        prevTooltip: 'Previous'
                    }
                }
            });
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.calendar.prevTooltip == 'Previous', "pickers.calendar.prevTooltip is default value");
            pickers.calendar.prevTooltip = 'abc';
            ok(pickers.calendar.prevTooltip == 'abc', "pickers.calendar.prevTooltip is abc");
            pickers.calendar.prevTooltip = 'あいう';
            ok(pickers.calendar.prevTooltip == 'あいう', "pickers.calendar.prevTooltip is あいう");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pickers.calendar.showDayPadding", function () {
            var $widget = tests.createInputDate({
                pickers: {
                    calendar: {
                        showDayPadding: true
                    }
                }
            });
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.calendar.showDayPadding == true, "pickers.calendar.showDayPadding is default value");
            pickers.calendar.showDayPadding = false;
            ok(pickers.calendar.showDayPadding == false, "pickers.calendar.showDayPadding is false");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pickers.calendar.showOtherMonthDays", function () {
            var $widget = tests.createInputDate({
                pickers: {
                    calendar: {
                        showOtherMonthDays: true
                    }
                }
            });
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.calendar.showOtherMonthDays == true, "pickers.calendar.showOtherMonthDays is default value");
            pickers.calendar.showOtherMonthDays = false;
            ok(pickers.calendar.showOtherMonthDays == false, "pickers.calendar.showOtherMonthDays is false");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pickers.calendar.showTitle", function () {
            var $widget = tests.createInputDate({
                pickers: {
                    calendar: {
                        showTitle: true
                    }
                }
            });
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.calendar.showTitle == true, "pickers.calendar.showTitle is default value");
            pickers.calendar.showTitle = false;
            ok(pickers.calendar.showTitle == false, "pickers.calendar.showTitle is false");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pickers.calendar.showWeekDays", function () {
            var $widget = tests.createInputDate({
                pickers: {
                    calendar: {
                        showWeekDays: true
                    }
                }
            });
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.calendar.showWeekDays == true, "pickers.calendar.showWeekDays is default value");
            pickers.calendar.showWeekDays = false;
            ok(pickers.calendar.showWeekDays == false, "pickers.calendar.showWeekDays is false");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pickers.calendar.showWeekNumbers", function () {
            var $widget = tests.createInputDate({
                pickers: {
                    calendar: {
                        showWeekNumbers: true
                    }
                }
            });
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.calendar.showWeekNumbers == true, "pickers.calendar.showWeekNumbers is default value");
            pickers.calendar.showWeekNumbers = false;
            ok(pickers.calendar.showWeekNumbers == false, "pickers.calendar.showWeekNumbers is false");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pickers.calendar.titleFormat", function () {
            var $widget = tests.createInputDate({
                pickers: {
                    calendar: {
                        titleFormat: 'MMMM yyyy'
                    }
                }
            });
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.calendar.titleFormat == 'MMMM yyyy', "pickers.calendar.titleFormat is default value");
            pickers.calendar.titleFormat = 'yyyy MM';
            ok(pickers.calendar.titleFormat == 'yyyy MM', "pickers.calendar.titleFormat is yyyy MM");
            pickers.calendar.titleFormat = 'あいう';
            ok(pickers.calendar.titleFormat == 'あいう', "pickers.calendar.titleFormat is あいう");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pickers.calendar.toolTipFormat", function () {
            var $widget = tests.createInputDate({
                pickers: {
                    calendar: {
                        toolTipFormat: 'dddd, MMMM dd, yyyy'
                    }
                }
            });
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.calendar.toolTipFormat == 'dddd, MMMM dd, yyyy', "pickers.calendar.toolTipFormat is default value");
            pickers.calendar.toolTipFormat = 'test';
            ok(pickers.calendar.toolTipFormat == 'test', "pickers.calendar.toolTipFormat is yyyy MMM dd");
            pickers.calendar.toolTipFormat = 'あいう';
            ok(pickers.calendar.toolTipFormat == 'あいう', "pickers.calendar.toolTipFormat is あいう");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pickers.calendar.weekDayFormat", function () {
            var $widget = tests.createInputDate({
                pickers: {
                    calendar: {
                        weekDayFormat: 'short'
                    }
                }
            });
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.calendar.weekDayFormat == 'short', "pickers.calendar.weekDayFormat is default value");
            pickers.calendar.weekDayFormat = 'full';
            ok(pickers.calendar.weekDayFormat == 'full', "pickers.calendar.weekDayFormat is full");
            pickers.calendar.weekDayFormat = 'firstLetter';
            ok(pickers.calendar.weekDayFormat == 'firstLetter', "pickers.calendar.weekDayFormat is firstLetter");
            pickers.calendar.weekDayFormat = 'abbreviated';
            ok(pickers.calendar.weekDayFormat == 'abbreviated', "pickers.calendar.weekDayFormat is abbreviated");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pickers.datePicker", function () {
            var $widget = tests.createInputDate();
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.datePicker == undefined, "pickers.datePicker is default value");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pickers.datePicker.format", function () {
            var $widget = tests.createInputDate({
                pickers: {
                    datePicker: {
                        format: 'MM,dd,yyyy'
                    }
                }
            });
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.datePicker.format == 'MM,dd,yyyy', "pickers.datePicker.format is default value");
            pickers.datePicker.format = 'dd,MMM,yyyy';
            $widget.wijinputdate("option", "pickers", pickers);
            ok(pickers.datePicker.format == 'dd,MMM,yyyy', "pickers.datePicker.format is dddd,MMM,yyyy");
            pickers.datePicker.format = 'yyyy,M,d';
            $widget.wijinputdate("option", "pickers", pickers);
            ok(pickers.datePicker.format == 'yyyy,M,d', "pickers.datePicker.format yyyy,M,d");
            //pickers.datePicker.format = 'M,d';
            //$widget.wijinputdate("option", "pickers", pickers);
            //ok(pickers.datePicker.format == 'MM,dd,yyyy', "pickers.datePicker.format MM,dd,yyyy");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pickers.timePicker", function () {
            var $widget = tests.createInputDate();
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.timePicker == undefined, "pickers.timePicker is default value");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pickers.timePicker.format", function () {
            var $widget = tests.createInputDate({
                pickers: {
                    timePicker: {
                        format: 'hh,mm,tt'
                    }
                }
            });
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.timePicker.format == 'hh,mm,tt', "pickers.datePicker.format is default value");
            pickers.timePicker.format = 't,h,m';
            $widget.wijinputdate("option", "pickers", pickers);
            ok(pickers.timePicker.format == 't,h,m', "pickers.datePicker.format is t,h,m");
            //pickers.timePicker.format = 'h,m';
            //$widget.wijinputdate("option", "pickers", pickers);
            //ok(pickers.timePicker.format == 'hh,mm,tt', "pickers.datePicker.format hh,mm,tt");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("pmDesignator", function () {
            var $widget = tests.createInputDate({
                dateFormat: "tt hh:mm:ss",
                date: new Date(2012, 0, 13, 13, 0, 0),
                displayFormat: ""
            });
            ok($widget.wijinputdate("option", "pmDesignator") == "PM", "pmDesignator is default value");
            equal($widget.val(), "PM 01:00:00", "pmDesignator is default value");
            $widget.wijinputdate("option", "culture", "ja-JP");
            ok($widget.wijinputdate("option", "pmDesignator") == "午後", "amDesignator is 午後");
            equal($widget.val(), "午後 01:00:00", "amDesignator is 午後");
            $widget.wijinputdate("option", "pmDesignator", "PP");
            ok($widget.wijinputdate("option", "pmDesignator") == "PP", "pmDesignator is PP");
            equal($widget.val(), "PP 01:00:00", "pmDesignator is PP");
            $widget.wijinputdate("option", "pmDesignator", "あい");
            ok($widget.wijinputdate("option", "pmDesignator") == "あい", "pmDesignator is あい");
            equal($widget.val(), "あい 01:00:00", "pmDesignator is あい");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("tabAction", function () {
            var $widget = tests.createInputDate();
            ok($widget.wijinputdate("option", "tabAction") == "field", "tabAction is field");
            $widget.wijinputdate('focus');
            $widget.simulateKeyStroke("[HOME][TAB]", null, function () {
                equal($widget.wijinputdate('isFocused'), true, "tabAction is field");
                $widget.wijinputdate("option", "tabAction", "control");
                $widget.wijinputdate('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("highlightText1", function () {
            var $widget = tests.createInputDate({
                date: new Date(2013, 11, 12)
            });
            ok($widget.wijinputdate("option", "highlightText") == "field", "highlightText is default value");
            $widget.wijinputdate("focus");
            setTimeout(function () {
                equal($widget.wijinputdate("getSelectedText"), "12");
                $widget.remove();
                start();
            }, 200);
            stop();
        });
        test("highlightText2", function () {
            var $widget = tests.createInputDate({
                date: new Date(2013, 11, 12)
            });
            $widget.wijinputdate("option", "highlightText", "all");
            ok($widget.wijinputdate("option", "highlightText") == "all", "highlightText is default value");
            $widget.wijinputdate("focus");
            setTimeout(function () {
                equal($widget.wijinputdate("getSelectedText"), "12/12/2013");
                $widget.remove();
                start();
            }, 200);
            stop();
        });
        test("increment", function () {
            var $widget = tests.createInputDate({
                date: "1/1/2011"
            });
            $widget.wijinputdate("spinUp");
            equal($widget.val(), "2/1/2011", "increment is default value");
            $widget.wijinputdate("option", "increment", 2);
            ok($widget.wijinputdate("option", "increment") == 2, "increment is 2");
            $widget.wijinputdate("focus");
            $widget.simulateKeyStroke("[LEFT]", null, function () {
                $widget.wijinputdate("spinUp");
                equal($widget.val(), "4/1/2011", "spinUp in decimal field");
                $widget.wijinputdate("option", "increment", 0);
                ok($widget.wijinputdate("option", "increment") == 0, "increment is 2");
                $widget.simulateKeyStroke("[RIGHT]", null, function () {
                    $widget.wijinputdate("spinUp");
                    equal($widget.val(), "4/1/2011", "spinUp in decimal field");
                    $widget.wijinputdate('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("increment by UI", function () {
            var $widget = tests.createInputDate({
                date: "1/1/2011"
            });
            $widget.wijinputdate('focus');
            $widget.simulateKeyStroke("[UP]", null, function () {
                equal($widget.val(), "2/1/2011", "spinUp in integer field");
                $widget.wijinputdate("option", "increment", 2);
                ok($widget.wijinputdate("option", "increment") == 2, "increment is 2");
                $widget.simulateKeyStroke("[LEFT][UP]", null, function () {
                    equal($widget.val(), "4/1/2011", "spinUp in decimal field");
                    $widget.wijinputdate("option", "increment", 0);
                    ok($widget.wijinputdate("option", "increment") == 0, "increment is 2");
                    $widget.simulateKeyStroke("[RIGHT][UP]", null, function () {
                        equal($widget.val(), "4/1/2011", "spinUp in decimal field");
                        $widget.wijinputdate('destroy');
                        $widget.remove();
                        start();
                    });
                });
            });
            stop();
        });
        test("Set options in constructor", function () {
            var $widget = tests.createInputDate({
                allowSpinLoop: true,
                amDesignator: "AA",
                activeField: 2,
                autoNextField: false,
                blurOnLastChar: true,
                blurOnLeftRightKey: "both",
                culture: "ja-JP",
                dateFormat: "ggg ee/MM/dd",
                displayFormat: "tt hh:mm:ss",
                dropDownButtonAlign: "left",
                hideEnter: true,
                hour12As0: true,
                invalidClass: "test",
                imeMode: "active",
                midnightAs0: false,
                maxDate: "12/31/2012",
                minDate: "1/1/2012",
                pmDesignator: "PP",
                popupPosition: {
                    offset: '10 10'
                },
                placeholder: "Please input...",
                readOnly: true,
                spinnerAlign: "bothSideUpLeft",
                showDropDownButton: true,
                showSpinner: true,
                date: "5/5/2012",
                tabAction: "control",
                highlightText: "control",
                increment: 2,
                pickers: {
                    calendar: {
                        allowQuickPick: false
                    },
                    datePicker: {
                        format: "yyyy,MM,dd"
                    },
                    timePicker: {
                        format: "tt,hh,mm"
                    },
                    list: [
                        new Date(2013, 1, 1), 
                        new Date(2013, 3, 3), 
                        new Date(2013, 4, 5)
                    ],
                    width: 100,
                    height: 100
                }
            });
            ok($widget.wijinputdate("option", "allowSpinLoop") == true);
            ok($widget.wijinputdate("option", "amDesignator") == "AA");
            ok($widget.wijinputdate("option", "activeField") == 2);
            ok($widget.wijinputdate("option", "autoNextField") == false);
            ok($widget.wijinputdate("option", "blurOnLastChar") == true);
            ok($widget.wijinputdate("option", "blurOnLeftRightKey") == "both");
            ok($widget.wijinputdate("option", "culture") == "ja-JP");
            ok($widget.wijinputdate("option", "dateFormat") == "ggg ee/MM/dd");
            ok($widget.wijinputdate("option", "displayFormat") == "tt hh:mm:ss");
            ok($widget.wijinputdate("option", "dropDownButtonAlign") == "left");
            ok($widget.wijinputdate("option", "hideEnter") == true);
            ok($widget.wijinputdate("option", "hour12As0") == true);
            ok($widget.wijinputdate("option", "invalidClass") == "test");
            ok($widget.wijinputdate("option", "imeMode") == "active");
            ok($widget.wijinputdate("option", "midnightAs0") == false);
            ok($widget.wijinputdate("option", "maxDate").toDateString() == "Mon Dec 31 2012");
            ok($widget.wijinputdate("option", "minDate").toDateString() == "Sun Jan 01 2012");
            ok($widget.wijinputdate("option", "pmDesignator") == "PP");
            ok($widget.wijinputdate("option", "popupPosition").offset == "10 10");
            ok($widget.wijinputdate("option", "placeholder") == "Please input...");
            ok($widget.wijinputdate("option", "readOnly") == true);
            ok($widget.wijinputdate("option", "spinnerAlign") == "bothSideUpLeft");
            ok($widget.wijinputdate("option", "showDropDownButton") == true);
            ok($widget.wijinputdate("option", "showSpinner") == true);
            ok($widget.wijinputdate("option", "date").toDateString() == "Sat May 05 2012");
            ok($widget.wijinputdate("option", "tabAction") == "control");
            ok($widget.wijinputdate("option", "highlightText") == "control");
            ok($widget.wijinputdate("option", "increment") == 2);
            var pickers = $widget.wijinputdate("option", "pickers");
            ok(pickers.calendar.allowQuickPick == false);
            ok(pickers.datePicker.format == "yyyy,MM,dd");
            ok(pickers.timePicker.format == "tt,hh,mm");
            ok(pickers.list.length == 3);
            ok(pickers.width == 100);
            ok(pickers.height == 100);
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        QUnit.module("wijinputdate: methods");
        test("getPostValue", function () {
            var $widget = tests.createInputDate({
                date: "1/1/2001",
                displayFormat: "d"
            });
            equal($widget.wijinputdate("getPostValue"), "1/1/2001", "getPostValue returns value");
            $widget.wijinputdate("option", "displayFormat", "D");
            equal($widget.wijinputdate("getPostValue"), "Monday, January 01, 2001", "getPostValue returns value");
            $widget.wijinputdate("option", "displayFormat", "ggg ee/MM/dd");
            equal($widget.wijinputdate("getPostValue"), "平成 13/01/01", "getPostValue returns value");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("getText", function () {
            var $widget = tests.createInputDate({
                date: "1/1/2001",
                displayFormat: "d"
            });
            equal($widget.wijinputdate("getText"), "1/1/2001", "getText returns value");
            $widget.wijinputdate("option", "displayFormat", "D");
            equal($widget.wijinputdate("getText"), "Monday, January 01, 2001", "getText returns value");
            $widget.wijinputdate("option", "displayFormat", "ggg ee/MM/dd");
            equal($widget.wijinputdate("getText"), "平成 13/01/01", "getText returns value");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("isDateNull", function () {
            var $widget = tests.createInputDate({
                displayFormat: ""
            });
            equal($widget.wijinputdate("isDateNull"), false, "isDateNull returns value");
            $widget.wijinputdate("option", "date", "1/1/2001");
            equal($widget.wijinputdate("isDateNull"), false, "isDateNull returns value");
            $widget.wijinputdate("option", "date", null);
            equal($widget.wijinputdate("isDateNull"), true, "isDateNull returns value");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("setText", function () {
            var $widget = tests.createInputDate({
                displayFormat: ""
            });
            $widget.wijinputdate("setText", "1/1/1999");
            equal($widget.wijinputdate().val(), "1/1/1999", "getText returns value");
            $widget.wijinputdate("setText", "Saturday, January 01, 2000");
            equal($widget.wijinputdate().val(), "1/1/2000", "getText returns value");
            $widget.wijinputdate("setText", "");
            equal($widget.wijinputdate().val(), "M/d/yyyy", "getText returns value");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("drop", function () {
            var $widget = tests.createInputDate({
                date: "1/1/2011",
                displayFormat: ""
            });
            $widget.wijinputdate("drop");
            $widget.simulateKeyStroke("[DOWN][ENTER]", null, function () {
                equal($widget.val(), "12/1/2010", "Date is dropped down.");
                $widget.wijinputdate('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("spinUp", function () {
            var $widget = tests.createInputDate({
                date: "1/1/2011",
                displayFormat: ""
            });
            $widget.wijinputdate("spinUp");
            equal($widget.val(), "2/1/2011", "spinUp in integer field");
            $widget.wijinputdate("focus");
            $widget.simulateKeyStroke("[RIGHT]", null, function () {
                $widget.wijinputdate("spinUp");
                equal($widget.val(), "2/2/2011", "spinUp in decimal field");
                $widget.simulateKeyStroke("[RIGHT]", null, function () {
                    $widget.wijinputdate("spinUp");
                    equal($widget.val(), "2/2/2012", "spinUp in decimal field");
                    $widget.wijinputdate('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("spinUp by UI", function () {
            var $widget = tests.createInputDate({
                date: "1/1/2011",
                displayFormat: ""
            });
            $widget.wijinputdate('focus');
            $widget.simulateKeyStroke("[UP]", null, function () {
                equal($widget.val(), "2/1/2011", "spinUp in integer field");
                $widget.simulateKeyStroke("[RIGHT][UP]", null, function () {
                    equal($widget.val(), "2/2/2011", "spinUp in decimal field");
                    $widget.simulateKeyStroke("[RIGHT][UP]", null, function () {
                        equal($widget.val(), "2/2/2012", "spinUp in decimal field");
                        $widget.wijinputdate('destroy');
                        $widget.remove();
                        start();
                    });
                });
            });
            stop();
        });
        test("spinDown", function () {
            var $widget = tests.createInputDate({
                date: "5/5/2011",
                displayFormat: ""
            });
            $widget.wijinputdate("spinDown");
            equal($widget.val(), "4/5/2011", "spinDown in integer field");
            $widget.wijinputdate("focus");
            $widget.simulateKeyStroke("[RIGHT]", null, function () {
                $widget.wijinputdate("spinDown");
                equal($widget.val(), "4/4/2011", "spinUp in decimal field");
                $widget.simulateKeyStroke("[RIGHT]", null, function () {
                    $widget.wijinputdate("spinDown");
                    equal($widget.val(), "4/4/2010", "spinUp in decimal field");
                    $widget.wijinputdate('destroy');
                    $widget.remove();
                    start();
                });
            });
            stop();
        });
        test("spinUp by UI", function () {
            var $widget = tests.createInputDate({
                date: "5/5/2011",
                displayFormat: ""
            });
            $widget.wijinputdate('focus');
            $widget.simulateKeyStroke("[DOWN]", null, function () {
                equal($widget.val(), "4/5/2011", "spinUp in integer field");
                $widget.simulateKeyStroke("[RIGHT][DOWN]", null, function () {
                    equal($widget.val(), "4/4/2011", "spinUp in decimal field");
                    $widget.simulateKeyStroke("[RIGHT][DOWN]", null, function () {
                        equal($widget.val(), "4/4/2010", "spinUp in decimal field");
                        $widget.wijinputdate('destroy');
                        $widget.remove();
                        start();
                    });
                });
            });
            stop();
        });
        test("selectText, getSelectedText", function () {
            var $widget = tests.createInputDate({
                date: "5/5/2011"
            });
            $widget.wijinputdate("selectText", 1, 3);
            setTimeout(function () {
                equal($widget.wijinputdate("getSelectedText"), "/5", "getSelectedText returns value");
                $widget.remove();
                start();
            }, 200);
            stop();
        });
        QUnit.module("wijinputdate: events");
        test("invalidInput", function () {
            var $widget = tests.createInputDate();
            var events = new Array();
            $widget.wijinputdate({
                invalidInput: function (e, data) {
                    events.push("invalidInput");
                }
            });
            $widget.simulateKeyStroke("[HOME]a", null, function () {
                notEqual(events.toString().indexOf("invalidInput"), -1, "invalidInput is fired.");
                $widget.wijinputdate('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("dateChanged", function () {
            var $widget = tests.createInputDate();
            var events = new Array();
            $widget.wijinputdate({
                dateChanged: function (e, data) {
                    events.push("dateChanged");
                }
            });
            $widget.wijinputdate("option", "date", "1/1/1999");
            notEqual(events.toString().indexOf("dateChanged"), -1, "dateChanged is fired.");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("dateChanged by UI", function () {
            var $widget = tests.createInputDate();
            var events = new Array();
            $widget.wijinputdate({
                dateChanged: function (e, data) {
                    events.push("dateChanged");
                }
            });
            $widget.wijinputdate("focus");
            $widget.simulateKeyStroke("[HOME]5", null, function () {
                notEqual(events.toString().indexOf("dateChanged"), -1, "dateChanged is fired.");
                $widget.wijinputdate('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("dropDownOpen", function () {
            var $widget = tests.createInputDate();
            var events = new Array();
            $widget.wijinputdate({
                dropDownOpen: function (e, data) {
                    events.push("dropDownOpen");
                }
            });
            $widget.wijinputdate("drop");
            notEqual(events.toString().indexOf("dropDownOpen"), -1, "dropDownOpen is fired.");
            $widget.wijinputdate("drop");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("dropDownClose", function () {
            var $widget = tests.createInputDate();
            var events = new Array();
            $widget.wijinputdate({
                dropDownClose: function (e, data) {
                    events.push("dropDownClose");
                }
            });
            $widget.wijinputdate("drop");
            $widget.wijinputdate("drop");
            notEqual(events.toString().indexOf("dropDownClose"), -1, "dropDownClose is fired.");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("spinUp", function () {
            var $widget = tests.createInputDate();
            var events = new Array();
            $widget.wijinputdate({
                spinUp: function (e, data) {
                    events.push("spinUp");
                }
            });
            $widget.wijinputdate("spinUp");
            notEqual(events.toString().indexOf("spinUp"), -1, "spinUp is fired.");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("spinUp by UI", function () {
            var $widget = tests.createInputDate();
            var events = new Array();
            $widget.wijinputdate({
                spinUp: function (e, data) {
                    events.push("spinUp");
                }
            });
            $widget.simulateKeyStroke("[UP]", null, function () {
                notEqual(events.toString().indexOf("spinUp"), -1, "spinUp is fired.");
                $widget.wijinputdate('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("spinDown", function () {
            var $widget = tests.createInputDate();
            var events = new Array();
            $widget.wijinputdate({
                spinDown: function (e, data) {
                    events.push("spinDown");
                }
            });
            $widget.wijinputdate("spinDown");
            notEqual(events.toString().indexOf("spinDown"), -1, "spinUp is fired.");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("spinDown by UI", function () {
            var $widget = tests.createInputDate();
            var events = new Array();
            $widget.wijinputdate({
                spinDown: function (e, data) {
                    events.push("spinDown");
                }
            });
            $widget.simulateKeyStroke("[DOWN]", null, function () {
                notEqual(events.toString().indexOf("spinDown"), -1, "spinDown is fired.");
                $widget.wijinputdate('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        test("valueBoundsExceeded", function () {
            var $widget = tests.createInputDate({
                maxDate: "8/15/2013",
                minDate: "2/1/2013",
                displayFormat: ""
            });
            var events = new Array();
            $widget.wijinputdate({
                valueBoundsExceeded: function (e, data) {
                    events.push("valueBoundsExceeded");
                }
            });
            $widget.wijinputdate("option", "date", "8/16/2013");
            equal(events.toString().indexOf("valueBoundsExceeded"), -1, "valueBoundsExceeded isn't fired.");
            $widget.wijinputdate("option", "date", "1/1/2013");
            equal(events.toString().indexOf("valueBoundsExceeded"), -1, "valueBoundsExceeded isn't fired.");
            $widget.wijinputdate('destroy');
            $widget.remove();
        });
        test("valueBoundsExceeded by UI", function () {
            var $widget = tests.createInputDate({
                maxDate: "8/15/2013",
                minDate: "2/1/2013",
                displayFormat: ""
            });
            var events = new Array();
            $widget.wijinputdate({
                valueBoundsExceeded: function (e, data) {
                    events.push("valueBoundsExceeded");
                }
            });
            $widget.simulateKeyStroke("[HOME]9", null, function () {
                notEqual(events.toString().indexOf("valueBoundsExceeded"), -1, "valueBoundsExceeded is fired.");
                $widget.wijinputdate('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
        //test("keyExit", function () {
        //    var $widget = tests.createInputDate();
        //    var events = new Array();
        //    $widget.wijinputdate({
        //        keyExit: function (e, data) {
        //            events.push("keyExit");
        //        }
        //    });
        //    $widget.wijinputdate("focus");
        //    $widget.simulateKeyStroke("[TAB]", null, function () {
        //        notEqual(events.toString().indexOf("keyExit"), -1, "keyExit is fired.");
        //        $widget.wijinputdate('destroy');
        //        $widget.remove();
        //        start();
        //    });
        //    stop();
        //});
        test("keyExit", function () {
            var $widget = tests.createInputDate({
                blurOnLeftRightKey: "both"
            });
            var events = new Array();
            $widget.wijinputdate({
                keyExit: function (e, data) {
                    events.push("keyExit");
                }
            });
            $widget.wijinputdate("focus");
            $widget.simulateKeyStroke("[HOME][LEFT]", null, function () {
                notEqual(events.toString().indexOf("keyExit"), -1, "keyExit is fired.");
                $widget.wijinputdate('destroy');
                $widget.remove();
                start();
            });
            stop();
        });
    })(wijmo.tests || (wijmo.tests = {}));
    var tests = wijmo.tests;
})(wijmo || (wijmo = {}));
