var wijinputcoreOptionDefaults = {
    allowSpinLoop: false,
    blurOnLastChar: false,
    blurOnLeftRightKey: 'none',
    culture: '',
    cultureCalendar: '',
    disabled: false,
    dropDownButtonAlign: 'right',
    hideEnter: false,
    imeMode: 'auto',
    invalidClass: 'ui-state-error',
    pickers: {
    },
    readonly: false,
    showDropDownButton: false,
    showSpinner: false,
    spinnerAlign: 'verticalRight',
    dropDownOpen: null,
    dropDownClose: null,
    dropDownButtonMouseDown: null,
    dropDownButtonMouseUp: null,
    initializing: null,
    initialized: null,
    invalidInput: null,
    textChanged: null,
    showNullText: false,
    nullText: undefined,
    disableUserInput: false,
    buttonAlign: null,
    showTrigger: undefined,
    triggerMouseDown: null,
    triggerMouseUp: null,
    keyExit: null
};
commonWidgetTests('wijinputmask', {
    defaults: $.extend({
    }, wijinputcoreOptionDefaults, {
        mask: '',
        text: null,
        promptChar: '_',
        resetOnPrompt: true,
        resetOnSpace: true,
        allowPromptAsInput: false,
        hidePromptOnLeave: false,
        skipLiterals: true,
        passwordChar: '',
        autoConvert: true,
        tabAction: 'control',
        maskFormat: '',
        highlightText: 'none'
    })
});
