﻿(function ($) {
	module("wijcandlestickchart:tickets");
	test("#52085", function () {
		var candlestickchart = createCandlestickChart({
			seriesList: [{
				label: "Series 1",
				legendEntry: true
			}]
		});
		ok(true, "No exception is thrown.");
		candlestickchart.remove();
	});
	test("#52229", function () {
		var candlestickchart = createCandlestickChart({
			axis: {
				x: {
					annoFormatString:"M"
				}
			},
			seriesList: [{
				label: "MSFT",
				legendEntry: true,
				data: data
			}]
		});
		ok(true, "No exception is thrown.");
		candlestickchart.remove();
	})

	test("#56555", function () {
		var candlestickchart = createCandlestickChart({
			seriesList: [{
				label: "MSFT",
				legendEntry: true,
				data: data
			}],
			hint: {
				enable: false
			}
		});
		ok(true, "No exception is thrown when hint.enable is set to false.");
		candlestickchart.remove();
	});
    test("#52075", function () {
		var candlestickchart = createCandlestickChart({
			seriesList: [{
				label: "Series 1",
				legendEntry: true,
				data: data
			}, {
			    label: "Series 2",
			    legendEntry: true,
			    data: data
			}],
			seriesStyles: [{
                highLow: {
                    fill: "#9999FF"
                },
                raisingClose: {
                    fill: "#FF66CC"
                }
            }]
		});
		ok(candlestickchart.wijcandlestickchart("option", "seriesStyles").length == 12, "The seriesstyles should have 12 style settings even if only one style is setting when creating widget.");
		candlestickchart.remove();
    });

    test("The data series doesn't display when there is only data value in candlestickchart.", function () {
        var data = {
            x: [new Date('12/1/2011')],
            high: [10],
            low: [7.5],
            open: [8],
            close: [8.6]
        }, candlestickchart = createCandlestickChart({
            seriesList: [{
                label: "MSFT",
                legendEntry: true,
                data: data
            }],
            hint: {
                enable: false
            }
        }), xItem = candlestickchart.wijcandlestickchart("option", "seriesList")[0].data.x[0].toString();

        ok(xItem.toString() == new Date('12/1/2011').toString(), "There is only one data value.");
        candlestickchart.remove();
    });

    test("#60703", function () {
        var candlestickchart = createCandlestickChart({
            seriesList: [{
                label: "MSFT",
                legendEntry: true,
                data: data
            }]
        });
        candlestickchart.wijcandlestickchart("option", "type", "hl");
        ok(true, "after set the type option to hl, no exception is thrown.");

        candlestickchart.wijcandlestickchart("option", "type", "ohlc");
        ok(true, "after set the type option to ohlc, no exception is thrown.");

        candlestickchart.wijcandlestickchart("option", "type", "candlestick");
        ok(true, "after set the type option to candlestick, no exception is thrown.");

        candlestickchart.remove();
    });

    test("#71878", function () {
        var seriesEles, trendlinePath,
            candlestickchart = createCandlestickChart({
            seriesList: [{
                label: "Trendline",
                legendEntry: true,
                isTrendline: true,
                data: { x: data.x, y: data.high }
            }],
            mouseOver: function (e, chartObj) {
                if (chartObj !== null) {
                    ok(chartObj.type === "trendLine", 'Hover on the trendline.');
                }
            },
            seriesStyles: [{
                highLow:{
                    stroke: "#FFA41C", "stroke-width": 1, "stroke-opacity": 0.7
                }                
            }],
            seriesHoverStyles: [{
                highLow:{
                    "stroke-width": 5
                }                
            }]
        });

        seriesEles = candlestickchart.data("wijmo-wijcandlestickchart").chartElement.data("fields").seriesEles;
        ok(seriesEles.length === 1 && seriesEles[0].isTrendline, "The trendline is drawn.");
        trendlinePath = seriesEles[0].path;
        ok(trendlinePath.attr("stroke-width") === 1, "SeriesStyle is applied on trendline");
        $(trendlinePath.node).trigger('mouseover');
        ok(trendlinePath.attr("stroke-width") === 5, "HoverSeriesStyle is applied on trendline");
        $(trendlinePath.node).trigger('mouseout');
        ok(trendlinePath.attr("stroke-width") === 1, "HoverSeriesStyle is removed and seriesStyle is applied on trendline");
        candlestickchart.remove();
    });

    test("#72817", function () {
    	var candlestickchart = createCandlestickChart({
    		seriesList: [{
    			data: {
    				high: [, , ], low: [, , ], open: [, , ], close: [, , ], x: [null, null, null]
    			},
    			legendEntry: true
    		}]
    	});

    	ok(true, "The candlestickchart created successfully with setting null to x");
    	candlestickchart.remove();
    });

    test("#71365", function () {
        var candlestickchart = createCandlestickChart({
            seriesStyles: [
                        {
                            highLow: { fill: "red", width: 25 },
                            fallingClose: { fill: "green", width: 80 },
                            risingClose: { fill: "blue", width: 80 }
                        }
            ],
            seriesList: [{
                label: "MSFT",
                legendEntry: true,
                data: {
                    x: [new Date('12/1/2011')],
                    high: [10],
                    low: [7.5],
                    open: [8],
                    close: [8.6]
                }
            }]
        }), candleStickEle = candlestickchart.wijcandlestickchart("getCandlestick", 0);

        ok(candleStickEle !== null, "Candlestick is shown.");
        ok(candleStickEle.high.attr("stroke") === "red", "Candlestick high/low element is filled with red color.");

        candlestickchart.wijcandlestickchart({
            seriesStyles: [
                        {
                            highLow: { stroke: "blue", fill: "red", width: 25 },
                            fallingClose: { fill: "green", width: 80 },
                            risingClose: { fill: "blue", width: 80 }
                        }
            ],
            seriesList: [{
                label: "MSFT",
                legendEntry: true,
                data: {
                    x: [new Date('12/1/2011')],
                    high: [10],
                    low: [7.5],
                    open: [8],
                    close: [8.6]
                }
            }]
        });
        candleStickEle = candlestickchart.wijcandlestickchart("getCandlestick", 0);
        ok(candleStickEle !== null, "Candlestick is shown.");
        ok(candleStickEle.high.attr("stroke") === "blue", "Candlestick high/low element is filled with blue color.");

        candlestickchart.remove();
    });

    test("#65134", function () {
        var candlestickchart = createCandlestickChart({
            type: "ohlc",
            seriesList: [{
                label: "Series 1", legendEntry: true,
                data: {
                    x: [new Date('12/1/2011')],
                    high: [10],
                    low: [7.5],
                    open: [8],
                    close: [8.6]
                }
            }]
        });

        candlestickchart.wijcandlestickchart({
            type: "candlestick",
            painted: function () {
                ok(true, "candlestickchart is redrawn successfully.");
            }
        });
        candlestickchart.remove();
    });

    test("#65149", function () {
        var candlestickchart = createCandlestickChart({
            seriesList: [{
                label: "Series 1",
                legendEntry: true,
                data: {
                    x: [new Date('12/1/2011')],
                    high: [10],
                    low: [7.5],
                    open: [8],
                    close: [8.6]
                }
            }],
            beforePaint: function () {
                return false;
            }
        }), events = ["mousedown", "mouseup", "mousemove", "mouseout"],
        element = candlestickchart.data("wijmo-wijcandlestickchart").chartElement;

        $.each(events, function (i, eName) {
            element.trigger(eName);
        });
        ok(true, "candlestickchart works correctly!");
        candlestickchart.remove();
    });

    test("#65143", function () {
        var candlestickchart = createCandlestickChart({
            seriesList: [{
                label: "Series 1",
                legendEntry: true,
                data: {
                    x: [new Date('12/1/2011')],
                    high: [7],
                    low: [1],
                    open: [3],
                    close: [5]
                }
            }, {
                label: "Series 1",
                legendEntry: true,
                data: {
                    high: [9],
                    low: [3],
                    open: [5],
                    close: [7]
                }
            }]
        });

        ok(true, "candlestickchart works when there are no X values with some series.");
        candlestickchart.remove();
    });

    test("#78520", function () {
        var tempXData1, tempXData2,
            candlestickchart = createCandlestickChart({
			        seriesList: [{
			            label: "Series 1",
			            legendEntry: true,
			            data: {
			                x: [new Date('12/1/2011')],
			                high: [7],
			                low: [1],
			                open: [3],
			                close: [5]
			            }
			        }],
			        "beforePaint": function (e) {
			            tempXData1 = $(e.target).wijcandlestickchart("option", "seriesList")[0].data.x[0];
			        }
			    }), tempSeriesList;

        tempSeriesList = candlestickchart.wijcandlestickchart("option", "seriesList");
        ok(tempSeriesList[0].data.x[0].toString() === new Date('12/1/2011').toString(), "The x value has been recovered!");

        candlestickchart.wijcandlestickchart("option", "beforePaint", function (e) {
            tempXData2 = $(e.target).wijcandlestickchart("option", "seriesList")[0].data.x[0];
        });
        candlestickchart.wijcandlestickchart("option", "seriesList", tempSeriesList);
        ok(tempXData1 === tempXData2, "Get the same x value after set serieslist option.");

        candlestickchart.remove();
    });

    test("#83047", function () {
        var data = [{
            x: new Date("12/5/2011"),
            high: 11,
            low: 4.4,
            open: 11,
            close: 6.2
        }, {
            x: new Date("12/6/2011"),
            high: 14,
            low: 4.2,
            open: 6.2,
            close: 13.8
        }, {
            x: new Date("12/7/2011"),
            high: 16,
            low: 8,
            open: 13.8,
            close: 15
        }],
        candlestickchart = createCandlestickChart({
            dataSource: data,
            axis: {
                y: {
                    textVisible: false
                },
            },
            seriesList: [{
                isTrendline: true,
                data: {
                    x: { bind: "x" },
                    y: { bind: "close" }
                }
            }]
        }), axisLabels, notEmpty = true;

        axisLabels = candlestickchart.find("text.wijchart-axis-label");
        ok(axisLabels.length === 3, "Has painted 3 label text !");
        $.each(axisLabels, function (idx, label) {
            if ($(label).text() === "0NaN") {
                notEmpty = false;
                return false;
            }
        });
        ok(notEmpty, "All axis label has correct value !");

        candlestickchart.remove();
    });

    test("#87958", function () {
        var data = [{
            x: new Date("12/5/2011"),
            high: 11,
            low: 4.4,
            open: 11,
            close: 6.2
        }],
        candlestickchart = createCandlestickChart({
            animation: null,
            dataSource: data,
            seriesList: [{
                data: {
                    x: { bind: "x" },
                    high: { bind: "high" },
                    low: { bind: "low" },
                    open: { bind: "open" },
                    close: { bind: "close" }
                }
            },{
                isTrendline: true,
                data: {
                    x: { bind: "x" },
                    y: { bind: "close" }
                }
            }]
        }), trendLineSeries;

        trendLineSeries = candlestickchart.wijcandlestickchart("option", "seriesList")[1];
        ok(trendLineSeries.data.x[0] === data[0].x, "Data of trendline has recovered !");

        candlestickchart.remove();
    });

    test("#90621", function () {
        var data = [{
            x: new Date("12/5/2011"),
            high: 11,
            low: 4.4,
            open: 11,
            close: 6.2
        }],
        candlestickchart = createCandlestickChart({
            animation: null,
            dataSource: data,
            seriesList: [{
                data: {
                    x: { bind: "x" },
                    high: { bind: "high" },
                    low: { bind: "low" },
                    open: { bind: "open" },
                    close: { bind: "close" }
                }
            }]
        }), xAxisOpt = candlestickchart.wijcandlestickchart("option", "axis").x,
            min = xAxisOpt.min, max = xAxisOpt.max;

        candlestickchart.wijcandlestickchart("option", "axis", { x: {} });
        xAxisOpt = candlestickchart.wijcandlestickchart("option", "axis").x;

        ok(min === xAxisOpt.min && max === xAxisOpt.max, "Get a correct value for axis min and max !");
        candlestickchart.remove();
    });

    test("JS Error issue when data is empty !", function () {
        var candlestickchart = createCandlestickChart({
            animation: null,
            dataSource: data,
            seriesList: [{
                data: {
                    x: [],
                    high: [],
                    low: [],
                    open: [],
                    close: []
                }
            }]
        });

        ok(true, "JS error doesn't occur !");
        candlestickchart.remove();
    });

    test("#112145", function () {
        var xValueLabel, yValueLabel,
            candlestickchart = createCandlestickChart({
            "seriesList": [{
                "data": {
                    "high": [7, 8, 9], "low": [1, 2, 3], "open": [3, 4, 5], "close": [5, 6, 7],
                    "x": [new Date(2015, 00, 01, 00, 00, 00), new Date(2015, 01, 02, 00, 00, 00), new Date(2015, 02, 03, 00, 00, 00)]
                }
            }], "axis": {
                "x": {
                    "visible": true,                    "annoMethod": "valueLabels",                    "valueLabels": [{
                        "value": "2015-01-01T00:00:00:000Z",
                        "gridLineStyle": {
                            "stroke": "#CC00CC", "stroke-width": 2
                        },
                        "gridLine": true,
                        "text": "X1"
                    }]
                },
                "y": {
                    "visible": true,                    "annoMethod": "valueLabels",                    "valueLabels": [{
                        "value": 2,
                        "gridLineStyle": {
                            "stroke": "Fuchsia", "stroke-width": 2
                        }, "gridLine": true,
                        "text": "Y1"
                    }]
                }
            }
        }), axisInfo = candlestickchart.data("wijmo-wijcandlestickchart").axisInfo;

        ok(axisInfo, "axisinfo exists and is not null.");
        ok(axisInfo.x && axisInfo.x.valueLabels && axisInfo.x.valueLabels[0], "Have a valuelabel for x axis.");
        xValueLabel = axisInfo.x.valueLabels[0];
        ok(xValueLabel.gridLine, "The grid line for x valuelabel should be shown.");
        ok(xValueLabel.gridLineStyle && (xValueLabel.gridLineStyle.stroke == "#CC00CC"), "The stroke style for grid line has been set to '#CC00CC'.");
        ok(axisInfo.y && axisInfo.y[0] && axisInfo.y[0].valueLabels[0], "Have a valuelabel for y axis.");
        yValueLabel = axisInfo.y[0].valueLabels[0];
        ok(yValueLabel.gridLine, "The grid line for y valuelabel should be shown.");
        ok(yValueLabel.gridLineStyle && (yValueLabel.gridLineStyle.stroke == "Fuchsia"), "The stroke style for grid line has been set to 'Fuchsia'.");
        candlestickchart.remove();
    });
})(jQuery)