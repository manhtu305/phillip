﻿(function ($) {
	module("wijcandlestickchart:events");

	var events = ["mousedown", "mouseup", "mousemove", "mouseover", "mouseout", "click"],
		eventsName = ["mouseDown", "mouseUp", "mouseMove", "mouseOver", "mouseOut", "click"],
		des = ["mouse down", "mouse up", "mouse move", "mouse over", "mouse out", "click"];

	$.each(events, function (i, name) {
		test(name, function () {
			var candlestickchart = createSimpleCandlestickChart(), opt = {};
			opt[eventsName[i]] = function(e, chartObj){
				if (chartObj !== null) {
					ok(chartObj.index === 0, 'Press the ' + des[i] + ' on the first candlestick.');
					candlestickchart.remove();
					start();
				}
			};
			candlestickchart.wijcandlestickchart(opt);
			setTimeout(function () {
				$(candlestickchart.wijcandlestickchart("getCandlestick", 0).path.node).trigger(name);
			}, 400);
			stop();
		});
	});

	
})(jQuery)