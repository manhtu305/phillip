﻿/*globals commonWidgetTests*/
/*
* wijbarchart_defaults.js
*/
"use strict";
commonWidgetTests('wijcandlestickchart', {
	defaults: {
		disabled: false,
		autoResize: true,
		indicator: {
			visible: false,
			style: {
				stroke: "#000000"
			}
		},
		width: null,
		height: null,
		wijCSS: new wijmo.chart.wijcandlestickchart_css(),
		initSelector: ":jqmData(role='wijcandlestickchart')",
		seriesList: [],
		annotations: [],
		seriesStyles: [
			{ "stroke": "#00cc00", "opacity": 0.9, "stroke-width": 1 },
			{ "stroke": "#0099cc", "opacity": 0.9, "stroke-width": 1 },
			{ "stroke": "#0055cc", "opacity": 0.9, "stroke-width": 1 },
			{ "stroke": "#2200cc", "opacity": 0.9, "stroke-width": 1 },
			{ "stroke": "#8800cc", "opacity": 0.9, "stroke-width": 1 },
			{ "stroke": "#d9007e", "opacity": 0.9, "stroke-width": 1 },
			{ "stroke": "#ff0000", "opacity": 0.9, "stroke-width": 1 },
			{ "stroke": "#ff6600", "opacity": 0.9, "stroke-width": 1 },
			{ "stroke": "#ff9900", "opacity": 0.9, "stroke-width": 1 },
			{ "stroke": "#ffcc00", "opacity": 0.9, "stroke-width": 1 },
			{ "stroke": "#ffff00", "opacity": 0.9, "stroke-width": 1 },
			{ "stroke": "#ace600", "opacity": 0.9, "stroke-width": 1 }
		],
		marginTop: 25,
		marginRight: 25,
		marginBottom: 25,
		marginLeft: 25,
		textStyle: {
			fill: "#888",
			"font-size": 10,
			stroke: "none"
		},

		header: {
			text: "",
			style: {
				fill: "none",
				stroke: "none"
			},
			textStyle: {
				"font-size": 18,
				fill: "#666",
				stroke: "none"
			},
			compass: "north",
			visible: true
		},
		footer: {
			text: "",
			style: {
				fill: "#fff",
				stroke: "none"
			},
			textStyle: {
				fill: "#000",
				stroke: "none"
			},
			compass: "south",
			visible: false
		},
		legend: {
			text: "",
			textMargin: { left: 2, top: 2, right: 2, bottom: 2 },
			style: {
				fill: "none",
				stroke: "none"
			},
			"textWidth": null,
			textStyle: {
				fill: "#333",
				stroke: "none"
			},
			titleStyle: {
				"font-weight": "bold",
				fill: "#000",
				stroke: "none"
			},
			compass: "east",
			orientation: "vertical",
			visible: true,
			reversed: false,
			size: {}
		},
		axis: {
		    x: new wijmo.chart.wijchartcore_options().axis.x,
		    y: new wijmo.chart.wijchartcore_options().axis.y
		},
		hint: {
			enable: true,
			content: null,
			contentStyle: {
				fill: "#d1d1d1",
				"font-size": 12
			},
			title: null,
			titleStyle: {
				fill: "#d1d1d1",
				"font-size": 16
			},
			style: {
				fill: "#000000",
				"stroke-width": 2
			},
			animated: "fade",
			showAnimated: "fade",
			hideAnimated: "fade",
			duration: 120,
			showDuration: 120,
			hideDuration: 120,
			easing: "",
			showEasing: "",
			hideEasing: "",
			showDelay: 0,
			hideDelay: 150,
			compass: "north",
			offsetX: 0,
			offsetY: 0,
			showCallout: true,
			calloutFilled: false,
			calloutFilledStyle: {
				fill: "#000"
			},
			beforeShowing : null,
			closeBehavior : "auto",
			relativeTo: "mouse",
			isContentHtml: false,
		},
		showChartLabels: true,
		chartLabelStyle: {},
		chartLabelFormatString: "",
		chartLabelFormatter: null,
		disableDefaultTextStyle: false,
		shadow: true,
		beforeSeriesChange: null,
		seriesChanged: null,
		beforePaint: null,
		painted: null,

		animation: {
			enabled: true,
			duration: 400,
			easing: '>'
		},
		seriesTransition: {
			enabled: true,
			duration: 400,
			easing: '>'

		},
		seriesHoverStyles: [{
			opacity: 1,
			"stroke-width": 1.5
		}, {
			opacity: 1,
			"stroke-width": 1.5
		}, {
			opacity: 1,
			"stroke-width": 1.5
		}, {
			opacity: 1,
			"stroke-width": 1.5
		}, {
			opacity: 1,
			"stroke-width": 1.5
		}, {
			opacity: 1,
			"stroke-width": 1.5
		}, {
			opacity: 1,
			"stroke-width": 1.5
		}, {
			opacity: 1,
			"stroke-width": 1.5
		}, {
			opacity: 1,
			"stroke-width": 1.5
		}, {
			opacity: 1,
			"stroke-width": 1.5
		}, {
			opacity: 1,
			"stroke-width": 1.5
		}, {
			opacity: 1,
			"stroke-width": 1.5
		}],

		mouseDown: null,
		mouseUp: null,
		mouseOver: null,
		mouseOut: null,
		mouseMove: null,
		click: null,

		culture: "",
		cultureCalendar: "",
		data: null,
		dataSource: null,

		type: "candlestick",
		candlestickFormatter: null
	}
});