﻿(function ($) {
	module("wijcandlestickchart:options");
	test("seriesList", function () {
	    var candlestickchart = createCandlestickChart();
		
		candlestickchart.wijcandlestickchart("option", "seriesList", [{
			label: "MSFT",
			legendEntry: true,
			data: data
		}]);
		
		ok(candlestickchart.data("fields").trackers.length === data.high.length, "the seriesList has been applied.");
		candlestickchart.remove();

		// test empty data
		candlestickchart = createCandlestickChart();
		ok(true, "When the seriesList is empty, No exception thrown.");
		candlestickchart.remove();

		candlestickchart = createCandlestickChart({
			seriesList: [{
				label: "Test",
				legendEntry: true,
				data: {
					high: [],
					low: [],
					open: [],
					close: []
				}
			}]
		});
		ok(true, "when the x value is not set, No exception thrown.");
		candlestickchart.remove();

		candlestickchart = createCandlestickChart({
			seriesList: [{
				label: "Test",
				legendEntry: true,
				data: {
					x: [new Date("1/1/2014"), new Date("2/1/2014")],
					open:[1, 3]
				}
			}]
		});
		ok(true, "when the data is not complete, No exception thrown.");
		candlestickchart.remove();

		candlestickchart = createCandlestickChart({
		    seriesList: [
                        {
                            label: "Series 1", legendEntry: true, data: {
                                x: [new Date('12/1/2011'), new Date('12/2/2011'), new Date("12/5/2011")],
                                high: [10, 12, 11],
                                low: [7.5, 8.6, 4.4],
                                open: [8, 8.6, 11],
                                close: [8.6, 11, 6.2]
                            }
                        },
                        { label: "Series 2", legendEntry: true, data: data }
		    ]
		});
		candlestickchart.wijcandlestickchart({
		    seriesStyles: [
                            {
                                highLow: { fill: "#ff9900", width: 6 },
                                fallingClose: { fill: "#ff9900", height: 2 },
                                raisingClose: { fill: "#ff9900", height: 2 }
                            }
		    ]
		});
		ok(true, "when only one series style is set, no exception thrown.");
		candlestickchart.remove();
	});

	test("seriesStyles", function () {
	    var candlestickchart = createCandlestickChart(),
            seriesStyles, seriesHoverStyles;

	    seriesStyles = candlestickchart.wijcandlestickchart("option", "seriesStyles");
	    seriesHoverStyles = candlestickchart.wijcandlestickchart("option", "seriesHoverStyles");
	    ok(seriesStyles.length > 0, "There are default seriesStyles");
	    ok(seriesHoverStyles.length > 0, "There are default seriesHoverStyles");
	    ok(seriesStyles[0].fallingClose !== undefined && seriesStyles[0].fallingClose !== null, "Default fallingclose style is set in seriesStyles.");
	    ok(seriesStyles[0].risingClose !== undefined && seriesStyles[0].risingClose !== null, "Default risingClose style is set in seriesStyles.");
	    ok(!seriesStyles[0].risingClose.fill, "Default fill for fallingclose style is empty.");
	    ok(seriesHoverStyles[0].fallingClose !== undefined && seriesHoverStyles[0].fallingClose !== null, "Default fallingclose style is set in seriesHoverStyles.");
	    candlestickchart.remove();
	});


})(jQuery);