﻿/*
* wijsplitter_core.js
*/

var el;

function create_wijsplitter(options) {
    var splitter = $('<div id="splitter1" style="width: 300px; height: 300px;"></div>');
    splitter.appendTo("body");
    return splitter.wijsplitter(options);
}
(function ($) {

    module("wijsplitter: core");
    test("create and destroy", function () {
        // test disconected element
        var $widget = create_wijsplitter({});
        ok($widget.hasClass("wijmo-wijsplitter-vertical"),'element css classes created.');
        ok($widget.children().length>0,'created child nodes.');
        ok($widget.children().first().hasClass("wijmo-wijsplitter-wrapper"),'splitter wrapper element created.');
        if($widget.children().first().children())
        {
            ok($widget.children().eq(0).children().has("wijmo-wijsplitter-v-panel1 ui-resizable"),'panel1 element created.');
            ok($widget.children().eq(1).children().has("wijmo-wijsplitter-v-bar ui-widget-header"),'bar element created.');
            ok($widget.children().eq(2).children().has("wijmo-wijsplitter-v-panel2"),'panel2 element created.');
        }
        $widget.stop();
         $widget.wijsplitter("destroy");
         ok(!$widget.hasClass("wijmo-wijsplitter-vertical wijmo-wijsplitter-expanded"),'element css classes removed.');
         ok($widget.children().length == 0, 'child elements removed.');
    });
})(jQuery);