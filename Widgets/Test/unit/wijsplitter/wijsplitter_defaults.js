/*
 * wijsplitter_defaults.js
 */
var wijsplitter_defaults = {
	initSelector: ":jqmData(role='wijsplitter')",
	wijCSS: $.extend({}, $.wijmo.wijCSS, {
		cornerCssPrefix: "ui-corner-",
		arrowCssPrefix: "ui-icon-triangle-1-",
		panelCss: {
			panel1: {
				n: "panel1",
				content: "panel1-content"
			},
			panel2: {
				n: "panel2",
				content: "panel2-content"
			}
		},
		wrapperCss: "wijmo-wijsplitter-wrapper",
		hSplitterCss: "wijmo-wijsplitter-horizontal",
		vSplitterCss: "wijmo-wijsplitter-vertical",
		hSplitterCssPrefix: "wijmo-wijsplitter-h-",
		vSplitterCssPrefix: "wijmo-wijsplitter-v-",
		collapsedCss : "collapsed",
		expandedCss : "expanded",
		resizeHelperCss : "resize-helper",
		barCss : "bar",
		expanderCss : "expander"
	}),
	/// <summary>
    /// Gets or sets the javascript function name that 
    /// would be called at client side when dragging the splitter.
    /// Default: null.
    /// Type: Function.
    /// Code example: 
    /// Supply a callback function to handle the sizing event:
    /// $("#element").wijsplitter({ sizing: function () { } });
    /// Bind to the event by type:
    /// $("#element").bind("wijsplittersizing", function () { });
    /// </summary>
    sizing: null,
    /// <summary>
    /// Gets or sets the javascript function name that 
    /// would be called at client side when finish dragging the splitter.
    /// Default: null.
    /// Type: Function.
    /// Code example: 
    /// Supply a callback function to handle the sized event:
    /// $("#element").wijsplitter({ sized: function () { } });
    /// Bind to the event by type:
    /// $("#element").bind("wijsplittersized", function () { });
    /// </summary>
    sized: null,
    /// <summary>
    /// Gets or sets the javascript function name that 
    /// would be called before panel1 is expanded out.
    /// Default: null.
    /// Type: Function.
    /// </summary>
    expand: null,
    /// <summary>
    /// Gets or sets the javascript function name that 
    /// would be called before panel1 is collapsed.
    /// Default: null.
    /// Type: Function.
    /// </summary>
    collapse: null,
    /// <summary>
    /// Gets or sets the javascript function name that would be called 
    /// when panel1 is expanded out by clicking the collapse/expand image.
    /// Default: null.
    /// Type: Function.
    /// </summary>
    expanded: null,
    /// <summary>
    /// Gets or sets the javascript function name that would be called 
    /// when panel1 is collapsed by clicking the collapse/expand image.
    /// Default: null.
    /// Type: Function.
    /// </summary>
    collapsed: null,
    /// <summary>
    /// A value indicates the z-index of Splitter bar.
    /// Default: -1.
    /// Type: Number.
    /// </summary>
    barZIndex: -1,
    /// <summary>
    /// A value determines whether the expander of Splitter is allowed to be shown.
    /// Default: true.
    /// Type: Boolean.
    /// </summary>
    showExpander: true,
    ///	<summary>
    ///	A value indicates the location of the splitter, in pixels, from the left or top edge of the splitter.
    /// Default: 100.
    /// Type: Number.
    ///	</summary>
    splitterDistance: 100,
    ///	<summary>
    ///	A value indicating the horizontal or vertical orientation of the splitter panels.
    /// Default: 'vertical'.
    /// Type: String.
    ///	</summary>
    orientation: 'vertical',
    ///	<summary>
    ///	A value that indicates whether or not the control is full of document. 
    /// Default: false.
    /// Type: Boolean.
    ///	</summary>
    fullSplit: false,
    ///	<summary>
    ///	A value defines the animation while the bar of splitter is beeing dragged.
    /// Default: {}.
    /// Type: Dictionary.
    ///	</summary>
    resizeSettings: {
        animationOptions: {
            ///	<summary>
            ///	Define how long (in milliseconds) the animation of 
            /// the sliding will run.
            /// Default: 100.
            /// Type: Number.
            ///	</summary>
            duration: 100,
            ///	<summary>
            ///	The easing that is applied to the animation.
            /// Default: 'swing'.
            /// Type: String.
            ///	</summary>
            easing: "swing",
            ///	<summary>
            ///	A value that determines whether use the animation. 
            /// Default: false.
            /// Type: Boolean.
            ///	</summary>
            disabled: false
        },
        ///	<summary>
        ///	A value that determines whether an outline of 
        /// the element is sized.
        /// Default: false.
        /// Type: Boolean.
        ///	</summary>
        ghost: false
    },
    ///	<summary>
    ///	Defines the information for top or left panel of splitter.
    /// Default: {}.
    /// Type: Dictionary.
    ///	</summary>
    panel1: {
        ///	<summary>
        ///	Gets or sets the minimum distance in pixels when resizing the splitter. 
        /// Default: 1.
        /// Type: Number.
        ///	</summary>
        minSize: 1,
        ///	<summary>
        ///	A value determining whether splitter panel is collapsed or expanded. 
        /// Default: false.
        /// Type: Boolean.
        ///	</summary>
        collapsed: false,
        ///	<summary>
        ///	Gets or sets the type of scroll bars to display for splitter panel.
        /// Default: 'auto'.
        /// Type: String.
        ///	</summary>
        scrollBars: "auto"
    },
    ///	<summary>
    ///	Defines the information for bottom or right panel of splitter.
    /// Default: {}.
    /// Type: Dictionary.
    ///	</summary>
    panel2: {
        ///	<summary>
        ///	Gets or sets the minimum distance in pixels when resizing the splitter. 
        /// Default: 1.
        /// Type: Number.
        ///	</summary>
        minSize: 1,
        ///	<summary>
        ///	Gets or sets a value determining whether splitter panel is collapsed or expanded. 
        /// Default: false.
        /// Type: Boolean.
        ///	</summary>
        collapsed: false,
        ///	<summary>
        ///	Gets or sets the type of scroll bars to display for splitter panel.
        /// Default: 'auto'.
        /// Type: String.
        ///	</summary>
        scrollBars: "auto"
    },
    /// <summary>
    /// A value indicating whether wijslider is disabled.
    /// Default: Object.
    /// Type: null.
    /// </summary>
       disabled: false,
       create: null,
       collapsingPanel: "panel1",
       wijMobileCSS: {
           header: "ui-header ui-bar-a",
           content: "ui-body ui-body-b",
           stateDefault: "ui-btn ui-btn-b"
       }
};

    commonWidgetTests('wijsplitter', {
    defaults: wijsplitter_defaults
});
