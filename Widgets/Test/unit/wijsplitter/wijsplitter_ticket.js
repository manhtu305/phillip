﻿(function ($) {
	module("wijsplitter: tickets");

	//#29981
	test("#29981", function () {
		var $widget = create_wijsplitter({ orientation: "vertical" });
		ok($widget.find('.wijmo-wijsplitter-wrapper').width() == $widget.width() * 2, "wrapper width is correct!");
		$widget.remove();
	});

	if (!$.browser.msie) {
		test("#41403", function () {
			var $widget = create_wijsplitter({
				orientation: "vertical", resizeSettings: {
					animationOptions: {
						disabled: false
					}
				}
			}),
				handle = $widget.find(".ui-resizable-e"),
				animating = false;
			$widget.find(".wijmo-wijsplitter-v-panel1").bind("animating", function () {
				animating = true;
			});
			handle.simulate("mouseover").simulate("drag", { dx: 50 });
			setTimeout(function () {
				ok(animating, "when resize animation is enabled, the animating event is fired.");
				$widget.wijsplitter({
					resizeSettings: {
						animationOptions: {
							disabled: true
						}
					}
				});
				animating = false;
				setTimeout(function () {
					handle.simulate("mouseover").simulate("drag", { dx: 50 });
					setTimeout(function () {
						ok(!animating, "when disable the resizer animation, the animating event can't fire.");
						$widget.remove();
						start();
					}, 400);
				}, 400);
			}, 400);
			stop();
		});

		test("#41557", function () {
			var $widget = create_wijsplitter({
				orientation: "vertical"
			}),
				handle = $widget.find(".ui-resizable-e"),
				panel1 = $widget.find(".wijmo-wijsplitter-v-panel1"),
				width = panel1.width();
			handle.simulate("mouseover").simulate("drag", { dx: 50 }).simulate("mouseup");
			setTimeout(function () {
				ok(panel1.width() === width + 50, "the spliter has draged, and the size has changed.");
				$widget.remove();
				start();
			}, 400);
			stop();
		});
	}
})(jQuery);