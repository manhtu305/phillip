/*
* wijsplitter_events.js
*/
(function ($) {
    module("wijsplitter: events");

    test("mouseover, mousedown, mouseup, mouseout", function () {
        expect(6);
        var $widget = create_wijsplitter();

        $('.wijmo-wijsplitter-v-bar').simulate('mouseover');
        ok($('.wijmo-wijsplitter-v-bar').hasClass("ui-state-hover"), "bar mouseover event");

        $('.wijmo-wijsplitter-v-bar').simulate('mouseout');
        ok(!$('.wijmo-wijsplitter-v-bar').hasClass("ui-state-hover"), "bar mouseout event");

        $('.wijmo-wijsplitter-v-expander').simulate('mouseover');
        ok($('.wijmo-wijsplitter-v-expander').hasClass("ui-state-hover"), "expander mouseover event");

        $('.wijmo-wijsplitter-v-bar').simulate('mouseout');
        ok(!$('.wijmo-wijsplitter-v-bar').hasClass("ui-state-hover"), "expander mouseout event");

        $('.wijmo-wijsplitter-v-expander').simulate('mousedown');
        ok($('.wijmo-wijsplitter-v-expander').hasClass("ui-state-active"), "expander mousedown event");

        $('.wijmo-wijsplitter-v-expander').simulate('mouseup');
        ok(!$('.wijmo-wijsplitter-v-expander').hasClass("ui-state-active"), "expander mouseup event");
        
        $widget.remove();
    });
})(jQuery);