﻿/*
* wijsplitter_options.js
*/
(function ($) {

    module("wijsplitter: options");

    test("showExpander", function () {
        var $widget = create_wijsplitter({ showExpander: true });
        ok($widget.find('.wijmo-wijsplitter-v-expander').css('display') == "block", "set showExpander to true");
        $widget.remove();
        var $widget = create_wijsplitter({ showExpander: false });
        ok($widget.find('.wijmo-wijsplitter-v-expander').css('display') == "none", "set showExpander to false");
        $widget.remove();
    });


    test("splitterDistance", function () {
        var $widget = create_wijsplitter({ splitterDistance: 10 });
        ok($widget.find('.wijmo-wijsplitter-v-panel1').css('width') == "10px", "set splitterDistance to 10");
        $widget.remove();

        var $widget = create_wijsplitter({ splitterDistance: 20 });
        ok($widget.find('.wijmo-wijsplitter-v-panel1').css('width') == "20px", "set splitterDistance to 20");
        $widget.remove();

        var $widget = create_wijsplitter({ splitterDistance: 50 });
        ok($widget.find('.wijmo-wijsplitter-v-panel1').css('width') == "50px", "set splitterDistance to 50");
        $widget.remove();

        var $widget = create_wijsplitter({ splitterDistance: 100 });
        ok($widget.find('.wijmo-wijsplitter-v-panel1').css('width') == "100px", "set splitterDistance to 100");
        $widget.remove();
    });


    test("orientation", function () {
        var $widget = create_wijsplitter({ orientation: 'vertical' });
        ok($widget.hasClass('wijmo-wijsplitter-vertical'), "set orientation to vertical");
        $widget.remove();
        var $widget = create_wijsplitter({ orientation: 'horizontal' });
        ok($widget.hasClass('wijmo-wijsplitter-horizontal'), "set orientation to horizontal");
        $widget.remove();
    });


//    test("fullSplit", function () {
//        var $widget = create_wijsplitter({ orientation: 'vertical', fullSplit: true });
//        ok($widget.get(0).style.width == "100%", "set fullSplit to true");
//        $widget.remove();
//        var $widget = create_wijsplitter({ orientation: 'vertical', fullSplit: false });
//        ok($widget.get(0).style.width != "100%", "set fullSplit to false");
//        $widget.remove();
//    });


    test("panel1", function () {
        var $widget = create_wijsplitter({ panel1: { collapsed: true} });
        ok($widget.find('.wijmo-wijsplitter-v-panel1').css('display') == "none", "set panel1 collapsed to true");
        $widget.remove();
        var $widget = create_wijsplitter({ panel1: { collapsed: false} });
        ok($widget.find('.wijmo-wijsplitter-v-panel1').css('display') == "block", "set panel1 collapsed to false");
        $widget.remove();

    });

    test("collapsingPanel", function() {
        var $widget = create_wijsplitter({ collapsingPanel: "panel2" });
        ok($widget.find('.ui-icon-triangle-1-e').length > 0, "set collapsingPanel to panel2");
        $widget.remove();
        var $widget = create_wijsplitter({ collapsingPanel: "panel1" });
        ok($widget.find('.ui-icon-triangle-1-w').length > 0, "set collapsingPanel to panel1");
        $widget.remove();
    });

})(jQuery);