/*
 * wijsuperpanel_core.js
 */
var offsetBefore, offsetAfter,
	heightBefore, heightAfter,
	widthBefore, widthAfter,
	dragged;

function drag(handle, dx, dy) {
	offsetBefore = d.offset();
	heightBefore = d.height();
	widthBefore = d.width();
	//this mouseover is to work around a limitation in resizable
	//TODO: fix resizable so handle doesn"t require mouseover in order to be used
	$(handle, d).simulate("mouseover");
	$(handle, d).simulate("drag", {
		dx: dx || 0,
		dy: dy || 0
	});
	dragged = { dx: dx, dy: dy };
	offsetAfter = d.offset();
	heightAfter = d.height();
	widthAfter = d.width();
}

var touchEnabled = $.support.isTouchEnabled && $.support.isTouchEnabled();

function moved(dx, dy, msg) {
	msg = msg ? msg + "." : "";
	var actual = { left: Math.round(offsetAfter.left), top: Math.round(offsetAfter.top) };
	var expected = { left: Math.round(offsetBefore.left + dx), top: Math.round(offsetBefore.top + dy) };
	same(actual, expected, "dragged[" + dragged.dx + ", " + dragged.dy + "] " + msg);
}

(function($){
	module("wijsuperpanel: core");

	test("create and destroy", function(){
	    // test disconected element
	    var disconected = $("<div>test</div>").wijsuperpanel();
	    disconected.wijsuperpanel("destroy");
	    ok(disconected.text() == "test", "destroy disconected element");	   
	    if (!touchEnabled) {
	        disconected.wijsuperpanel();
	        ok(disconected.wijsuperpanel("paintPanel") == false, "paintpanel returns false when element is disconected and invisible.");
	        // added to body 
	        disconected.appendTo(document.body);
	        ok(disconected.wijsuperpanel("paintPanel") == true, "paintpanel returns true when element is visible.");

	        disconected.wijsuperpanel("destroy");
	    }
		disconected.remove();
	});

}) (jQuery);