/*
* wijsuperpanel_methods.js
*/
(function ($) {

    module("wijsuperpanel: methods");
    if (!touchEnabled) {
        function compareOffset(o1, o2) {
            return Math.abs(o1.left - o2.left) <= 2 && Math.abs(o1.top - o2.top) <= 2;
        }

        test("getContentElement", function () {
            // test disconected element
            var el = $("#superPanel").wijsuperpanel();
            var content = el.wijsuperpanel("getContentElement");
            ok(content.hasClass("wijmo-wijsuperpanel-templateouterwrapper"), "Method 'getContentElement' returns wijmo-wijsuperpanel-templateouterwrapper.");
            el.wijsuperpanel("destroy");
        });

        test("scrollChildIntoView-Menu From Right Bottom", function () {
            var el = $("#superPanel").wijsuperpanel();
            // test disconected element
            var child = $("#Menu");
            el.wijsuperpanel("scrollChildIntoView", child);
            stop();
            setTimeout(function () {
                var cotent = $(".wijmo-wijsuperpanel-contentwrapper", el);
                var contentOffset = cotent.offset();
                contentOffset.left = contentOffset.left + cotent.outerHeight();
                contentOffset.top = contentOffset.top + cotent.outerHeight();
                var child = $("#Menu");
                var childOffset = child.offset();
                childOffset.left = childOffset.left + child.outerHeight();
                childOffset.top = childOffset.top + child.outerHeight();
                ok(compareOffset(contentOffset, childOffset), "Method 'scrollChildIntoView': scroll into view from right bottom.");
                //el.wijsuperpanel("destroy");
                start();
            }, 1500);
        });

        test("scrollChildIntoView-ControlDarkDark From Left Top", function () {
            // test disconected element
            var el = $("#superPanel").wijsuperpanel({
                hScroller: {
                    scrollValue: 100
                },
                vScroller: {
                    scrollValue: 100
                }
            });
            var child = $("#ControlDarkDark");
            el.wijsuperpanel("scrollChildIntoView", child);
            stop();
            setTimeout(function () {
                var cotent = $(".wijmo-wijsuperpanel-contentwrapper", el);
                var contentOffset = cotent.offset();
                var child = $("#ControlDarkDark");
                var childOffset = child.offset();
                ok(compareOffset(contentOffset, childOffset), "Method 'scrollChildIntoView': scroll into view from Left Top.");
                el.wijsuperpanel("destroy");
                start();
            }, 1500);
        });

        test("scrollChildIntoView-Azure From Left Bottom", function () {
            // test disconected element
            var el = $("#superPanel").wijsuperpanel({
                hScroller: {
                    scrollValue: 1000
                },
                vScroller: {
                    scrollValue: 0
                }
            });
            var child = $("#Aquamarine");
            el.wijsuperpanel("scrollChildIntoView", child);
            stop();
            setTimeout(function () {
                var cotent = $(".wijmo-wijsuperpanel-contentwrapper", el);
                var contentOffset = cotent.offset();
                contentOffset.top = contentOffset.top + cotent.outerHeight();
                var child = $("#Aquamarine");
                var childOffset = child.offset();
                childOffset.top = childOffset.top + child.outerHeight();
                ok(compareOffset(contentOffset, childOffset), "Method 'scrollChildIntoView': scroll into view from Left Bottom");
                el.wijsuperpanel("destroy");
                start();
            }, 1500);
        });

        test("scrollChildIntoView-ControlText From Right Top", function () {
            // test disconected element
            var el = $("#superPanel").wijsuperpanel({
                hScroller: {
                    scrollValue: 0
                },
                vScroller: {
                    scrollValue: 100
                }
            });
            var child = $("#ControlText");
            el.wijsuperpanel("scrollChildIntoView", child);
            stop();
            setTimeout(function () {
                var cotent = $(".wijmo-wijsuperpanel-contentwrapper", el);
                var contentOffset = cotent.offset();
                contentOffset.left = contentOffset.left + cotent.outerWidth();
                var child = $("#ControlText");
                var childOffset = child.offset();
                childOffset.left = childOffset.left + child.outerWidth();
                ok(compareOffset(contentOffset, childOffset), "Method 'scrollChildIntoView': scroll into view from Left Bottom");
                el.wijsuperpanel("destroy");
                start();
            }, 1500);
        });

        test("hScrollTo", function () {
            // test disconected element
            var el = $("#superPanel").wijsuperpanel();
            el.wijsuperpanel("hScrollTo", 100);
            setTimeout(function () {
                ok(el.wijsuperpanel("getContentElement").css("left") == "-100px", "Method 'hScrollTo': scrolls to 100px.");
                start();
                el.wijsuperpanel("hScrollTo", 100, true);
                setTimeout(function () {
                    var left = el.wijsuperpanel("getContentElement").width() - $(".elements ul").outerWidth();
                    ok(el.wijsuperpanel("getContentElement").css("left") == left + "px", "Method 'hScrollTo': scrolls to value 100.");
                    el.wijsuperpanel("destroy");
                    start();
                }, 1500);
                stop();
            }, 1500);
            stop();
        });

        test("vScrollTo", function () {
            // test disconected element
            var el = $("#superPanel").wijsuperpanel();
            el.wijsuperpanel("vScrollTo", 100);
            setTimeout(function () {
                ok(el.wijsuperpanel("getContentElement").css("top") == "-100px", "Method 'vScrollTo': scrolls to 100px.");
                start();
                el.wijsuperpanel("vScrollTo", 100, true);
                setTimeout(function () {
                    var top = el.wijsuperpanel("getContentElement").parent().height() - $(".elements ul").outerHeight();
                    ok(el.wijsuperpanel("getContentElement").css("top") == top + "px", "Method 'vScrollTo': scrolls to value 100.");
                    el.wijsuperpanel("destroy");
                    start();
                }, 1500);
                stop();
            }, 1500);
            stop();
        });

        test("refresh, paintPanel", function () {
            var el = $("#superPanel").wijsuperpanel();
            el.find("ul").width(2000);
            el.wijsuperpanel("refresh");

            var data = el.data("wijmo-wijsuperpanel");
            //equal(data._fieldsStore.contentWidth, 2000, "refresh, paintPanel");
            ok(data._fieldsStore.contentWidth > 2000, "refresh, paintPanel");

        });

        test("scrollValueToPx", function () {
            var el = $("#superPanel").wijsuperpanel(), scrollValue, height, width, result;

            // ** Scroll to Px ** //
            el.wijsuperpanel("vScrollTo", 100);
            scrollValue = el.wijsuperpanel("option", "vScroller").scrollValue;
            result = el.wijsuperpanel("scrollValueToPx", scrollValue, "v");
            ok(result === 100, "Vertical ScrollValue to Px conversion success!");
            // ** Scroll to Value(to bottom) ** //
            el.wijsuperpanel("vScrollTo", 100, true);
            result = el.wijsuperpanel("scrollValueToPx", 100, "v");
            height = $(".elements ul").outerHeight() - el.wijsuperpanel("getContentElement").parent().height();
            ok(result === height, "Vertical ScrollValue to Px conversion success!");
            // ** Scroll to Value(to any position) ** //
            el.wijsuperpanel("vScrollTo", 25, true);
            result = el.wijsuperpanel("scrollValueToPx", 25, "v");
            scrollValue = el.wijsuperpanel("scrollPxToValue", result, "v");
            ok(Math.round(scrollValue) === 25, "Vertical ScrollValue to Px conversion success!");

            el.wijsuperpanel("hScrollTo", 100);
            scrollValue = el.wijsuperpanel("option", "hScroller").scrollValue;
            result = el.wijsuperpanel("scrollValueToPx", scrollValue, "h");
            ok(result === 100, "Horizontal ScrollValue to Px conversion success!");

            el.wijsuperpanel("hScrollTo", 100, true);
            result = el.wijsuperpanel("scrollValueToPx", 100, "h");
            width = $(".elements ul").outerWidth() - el.wijsuperpanel("getContentElement").parent().width();
            ok(result === width, "Horizontal ScrollValue to Px conversion success!");

            el.wijsuperpanel("hScrollTo", 25, true);
            result = el.wijsuperpanel("scrollValueToPx", 25, "h");
            scrollValue = el.wijsuperpanel("scrollPxToValue", result, "h");
            ok(Math.round(scrollValue) === 25, "Horizontal ScrollValue to Px conversion success!");

            el.wijsuperpanel("destroy");
        })
    }
})(jQuery);
