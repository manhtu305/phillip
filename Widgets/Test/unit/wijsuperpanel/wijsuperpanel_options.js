/*
 * wijsuperpanel_options.js
 */
(function ($) {
	var contentWrapperClass = ".wijmo-wijsuperpanel-contentwrapper",
		hbarcontainer = ".wijmo-wijsuperpanel-hbarcontainer",
		vbarcontainer = ".wijmo-wijsuperpanel-vbarcontainer",
		draghandleClass = ".wijmo-wijsuperpanel-handle",
		uiDisabled = ".ui-state-disabled",
		buttonLeft = ".wijmo-wijsuperpanel-buttonleft",
		buttonRight = ".wijmo-wijsuperpanel-buttonright",
		buttonTop = ".wijmo-wijsuperpanel-buttontop",
		buttonBottom = ".wijmo-wijsuperpanel-buttonbottom",
		templateWrapper = ".wijmo-wijsuperpanel-templateouterwrapper",
		arrowRight = ".wijmo-wijsuperpanel-hbar-buttonright",
		arrowBottom = ".wijmo-wijsuperpanel-vbar-buttonbottom";

	module("wijsuperpanel:options");
	if (!touchEnabled) {
	    test("allowResize", function () {
	        var handle = ">.ui-resizable-handle",
                fieldKey = "wijsuperpanel-fields";
	        var sp = $("<div></div>").wijsuperpanel();
	        sp.wijsuperpanel("option", "allowResize", true);
	        ok(sp.find(handle).size() == 8 && sp.data(fieldKey).resizer !== null, "Option 'allowResize' is true: resizer is created.");
	        sp.wijsuperpanel("option", "allowResize", false);
	        ok(sp.find(handle).size() == 0 && sp.data(fieldKey).resizer === null, "Option 'allowResize' is false: resizer is null.");
	        sp.wijsuperpanel("destroy");
	    });

	    test("animationOptions", function () {
	        stop();
	        var el = $("#superPanel");
	        el.wijsuperpanel({
	            animationOptions: {
	                complete: function () {
	                    start();
	                    ok(true, "Option 'animationOptions.complete' handler is fired.");
	                    el.wijsuperpanel("destroy");
	                }
	            }
	        });
	        el.wijsuperpanel("vScrollTo", 50);
	    });

	    test("hScroller-scrollBarPosition", function () {
	        var el = $("#superPanel").wijsuperpanel({ hScroller: { scrollBarPosition: "top" } });
	        el.wijsuperpanel("paintPanel");
	        var contentWrapper = $(contentWrapperClass, el);
	        var hScrollbar = $(hbarcontainer, el);
	        var vScrollbar = $(vbarcontainer, el);
	        ok(hScrollbar.outerWidth() === contentWrapper.width() && hScrollbar.css("top") === "0px" && hScrollbar.css("left") == "0px" && vScrollbar.outerHeight() == contentWrapper.height() && vScrollbar.css("right") === "0px" && vScrollbar.css("bottom") == "0px", "Option 'hScroller.scrollBarPosition' is 'top'. Horizontal scroll bar is aligned to left top. Vertical scroll bar is aligned to right bottom.");
	        el.wijsuperpanel({ hScroller: { scrollBarPosition: "bottom" } });
	        el.wijsuperpanel("paintPanel");
	        // bars is re-created, so need to grab again
	        hScrollbar = $(hbarcontainer, el);
	        vScrollbar = $(vbarcontainer, el);
	        ok(hScrollbar.outerWidth() === contentWrapper.width() && hScrollbar.css("bottom") === "0px" && hScrollbar.css("left") == "0px" && vScrollbar.outerHeight() === contentWrapper.height() && vScrollbar.css("right") === "0px" && vScrollbar.css("top") == "0px", "Option 'hScroller.scrollBarPosition' is 'bottom'. Horizontal scroll bar is aligned to left bottom. Vertical scroll bar is aligned to right top.");
	        el.wijsuperpanel("destroy");
	    });

	    test("hScroller-scrollBarVisibility:auto,hidden,visible", function () {
	        var el = $("#superPanel").wijsuperpanel({ hScroller: { scrollBarVisibility: "hidden" } });
	        el.wijsuperpanel("paintPanel");
	        var contentWrapper = $(contentWrapperClass, el);
	        var hScrollbar = $(hbarcontainer, el);
	        var vScrollbar = $(vbarcontainer, el);
	        ok(hScrollbar.size() == 0 && vScrollbar.outerHeight() === contentWrapper.height(), "Option 'hScroller.scrollBarVisibility is 'hidden'. Horizontal scroll bar is invisible while vertical scroll bar is visible.");
	        // to change to auto scroll mode.
	        el.wijsuperpanel({ hScroller: { scrollBarVisibility: "auto" } });
	        el.wijsuperpanel("paintPanel");
	        hScrollbar = $(hbarcontainer, el);
	        vScrollbar = $(vbarcontainer, el);
	        ok(hScrollbar.is(":visible") && hScrollbar.outerWidth() === contentWrapper.width() && vScrollbar.outerHeight() === contentWrapper.height(), "Option 'hScroller.scrollBarVisibility is 'auto'. Both scroll bars are visible and active.");

	        $(".elements", el).css("display", "none");
	        el.wijsuperpanel("paintPanel");
	        hScrollbar = $(hbarcontainer, el);
	        vScrollbar = $(vbarcontainer, el);
	        ok(hScrollbar.size() === 0 && vScrollbar.size() === 0, "Option 'hScroller.scrollBarVisibility is 'auto' and there is no content. Both scroll bars are invisible.");

	        el.wijsuperpanel({ hScroller: { scrollBarVisibility: "visible" } });
	        el.wijsuperpanel("paintPanel");
	        hScrollbar = $(hbarcontainer, el);
	        vScrollbar = $(vbarcontainer, el);
	        ok(!hScrollbar.find(draghandleClass).is(":visible") && hScrollbar.hasClass("wijmo-wijsuperpanel-hbarcontainer-disabled") && hScrollbar.find(uiDisabled).size() === 3, "Option 'hScroller.scrollBarVisibility is 'visible' and there is no content. Horizontal scroll bar is visible but disabled.");
	        $(".elements").css("display", "");
	        el.wijsuperpanel("destroy");
	    });

	    test("hScroller-scrollMode:", function () {
	        var el = $("#superPanel").wijsuperpanel({ hScroller: { scrollMode: "buttons" } });
	        el.wijsuperpanel("paintPanel");
	        var hscrollbar = $(hbarcontainer, el);
	        var vscrollbar = $(vbarcontainer, el);
	        var leftButton = $(buttonLeft, el);
	        var rightButton = $(buttonRight, el);
	        var topButton = $(buttonTop, el);
	        var bottomButton = $(buttonBottom, el);
	        ok(leftButton.is(":visible") && rightButton.is(":visible") && topButton.size() == 0 && bottomButton.size() == 0 && hscrollbar.size() == 0 && vscrollbar.is(":visible"), "Option 'hScroller.scrollMode' is 'buttons'. Horizontal scrollbar is not visible. Horizontal scroll buttons are visible.");

	        el.wijsuperpanel({ hScroller: { scrollMode: "buttonshover" } });
	        el.wijsuperpanel("paintPanel");
	        hscrollbar = $(hbarcontainer, el);
	        vscrollbar = $(vbarcontainer, el);
	        leftButton = $(buttonLeft, el);
	        rightButton = $(buttonRight, el);
	        topButton = $(buttonTop, el);
	        bottomButton = $(buttonBottom, el);
	        ok(leftButton.is(":visible") && rightButton.is(":visible") && topButton.size() == 0 && bottomButton.size() == 0 && hscrollbar.size() == 0 && vscrollbar.is(":visible"), "Option 'hScroller.scrollMode' is 'buttonshover'. Horizontal scrollbar is not visible. Horizontal scroll buttons are visible.");

	        el.wijsuperpanel({ hScroller: { scrollMode: "edge" } });
	        el.wijsuperpanel("paintPanel");
	        hscrollbar = $(hbarcontainer, el);
	        vscrollbar = $(vbarcontainer, el);
	        leftButton = $(buttonLeft, el);
	        rightButton = $(buttonRight, el);
	        topButton = $(buttonTop, el);
	        bottomButton = $(buttonBottom, el);
	        ok(leftButton.size() == 0 && rightButton.size() == 0 && topButton.size() == 0 && bottomButton.size() == 0 && hscrollbar.size() == 0 && vscrollbar.is(":visible"), "Option 'hScroller.scrollMode' is 'edge'. Horizontal scrollbar is invisible. Horizontal scroll buttons are invisible.");

	        el.wijsuperpanel({ hScroller: { scrollMode: "scrollbar,buttons" } });
	        el.wijsuperpanel("paintPanel");
	        hscrollbar = $(hbarcontainer, el);
	        vscrollbar = $(vbarcontainer, el);
	        leftButton = $(buttonLeft, el);
	        rightButton = $(buttonRight, el);
	        topButton = $(buttonTop, el);
	        bottomButton = $(buttonBottom, el);
	        ok(leftButton.is(":visible") && rightButton.is(":visible") && !topButton.is(":visible") && !bottomButton.is(":visible") && hscrollbar.is(":visible") && vscrollbar.is(":visible"), "Option 'hScroller.scrollMode' is 'scrollbar,buttons'. Horizontal scrollbar is visible. Horizontal scroll buttons are visible.");
	        el.wijsuperpanel("destroy");
	    });

	    test("hScroller-scrollValue", function () {
	        var el = $("#superPanel").wijsuperpanel();
	        var content = $(templateWrapper, el);
	        var option = el.wijsuperpanel("option");
	        var userMax = option.hScroller.scrollMax - option.hScroller.scrollLargeChange + 1;
	        el.wijsuperpanel({ hScroller: { scrollValue: userMax } });
	        el.wijsuperpanel("paintPanel");
	        var hDrag = $(hbarcontainer + " " + draghandleClass, el);
	        ok(Math.abs(el.data("wijmo-wijsuperpanel").options.hScroller.scrollValue - userMax) < 0.001 && Math.abs(parseInt(hDrag.css("left")) - 181) < 2 && Math.abs(parseInt(content.css("left")) + 566) < 2, "Option 'hScroller.scrollValue' is set to max. Content wrapper and horizontal drag handle are scrolled to right most position.");

	        el.wijsuperpanel({ hScroller: { scrollValue: 0 } });
	        el.wijsuperpanel("paintPanel");
	        hDrag = $(hbarcontainer + " " + draghandleClass, el);
	        ok(el.data("wijmo-wijsuperpanel").options.hScroller.scrollValue == 0 && hDrag.css("left") == "18px" && content.css("left") == "0px", "Option 'hScroller.scrollValue' is set to 0. Content wrapper and horizontal drag handle are scrolled to left most position.");
	        //el.wijsuperpanel("destroy");
	    });

	    test("hScroller-firstStepChangeFix", function () {
	        var el = $("#superPanel");
	        el.wijsuperpanel({ hScroller: { firstStepChangeFix: 1.5, scrollValue: 0, scrollSmallChange: 1, scrollLargeChange: 10 } });
	        el.wijsuperpanel("paintPanel");
	        stop();
	        var arrow = $(arrowRight);
	        arrow.simulate("mouseover");
	        arrow.simulate("mousedown");
	        arrow.simulate("mouseup");
	        arrow.simulate("mouseout");
	        window.setTimeout(function () {
	            var el = $("#superPanel");
	            var option = el.wijsuperpanel("option");
	            ok(el.data("wijmo-wijsuperpanel").options.hScroller.scrollValue == 2.5, "Option 'hScroller.firstStepChangeFix' is 1.5. Value after first step is fixed to 2.5.");
	            el.wijsuperpanel("destroy");
	            start();
	        }, 400);
	    });

	    test("vScroller-scrollBarPosition", function () {
	        var el = $("#superPanel").wijsuperpanel({ vScroller: { scrollBarPosition: "left" } });
	        el.wijsuperpanel("paintPanel");
	        var contentWrapper = $(contentWrapperClass, el);
	        var hScrollbar = $(hbarcontainer, el);
	        var vScrollbar = $(vbarcontainer, el);
	        ok(vScrollbar.outerHeight() === contentWrapper.height() && vScrollbar.css("left") === "0px" && vScrollbar.css("top") == "0px" && hScrollbar.outerWidth() == contentWrapper.width() && hScrollbar.css("right") === "0px" && hScrollbar.css("bottom") == "0px", "Option 'vScroller.scrollBarPosition' is 'left'. Vertical scroll bar is aligned to left top. Horizontal scroll bar is aligned to right bottom. ");
	        el.wijsuperpanel({ vScroller: { scrollBarPosition: "right" } });
	        el.wijsuperpanel("paintPanel");
	        // bars is re-created, so need to grab again
	        hScrollbar = $(hbarcontainer, el);
	        vScrollbar = $(vbarcontainer, el);
	        ok(vScrollbar.outerHeight() === contentWrapper.height() && vScrollbar.css("right") === "0px" && vScrollbar.css("top") == "0px" && hScrollbar.outerWidth() == contentWrapper.width() && hScrollbar.css("left") === "0px" && hScrollbar.css("bottom") == "0px", "Option 'vScroller.scrollBarPosition' is 'right'. Vertical scroll bar is aligned to right top. Horizontal scroll bar is aligned to left bottom. ");
	        el.wijsuperpanel("destroy");
	    });

	    test("vScroller-scrollBarVisibility:auto,hidden,visible", function () {
	        var el = $("#superPanel").wijsuperpanel({ vScroller: { scrollBarVisibility: "hidden" } });
	        el.wijsuperpanel("paintPanel");
	        var contentWrapper = $(contentWrapperClass, el);
	        var hScrollbar = $(hbarcontainer, el);
	        var vScrollbar = $(vbarcontainer, el);
	        ok(vScrollbar.size() == 0 && hScrollbar.outerWidth() === contentWrapper.width(), "Option 'vScroller.scrollBarVisibility is 'hidden'. Vertical scroll bar is invisible while horizontal scroll bar is visible.");

	        // to change to auto scroll mode.
	        el.wijsuperpanel({ vScroller: { scrollBarVisibility: "auto" } });
	        el.wijsuperpanel("paintPanel");
	        hScrollbar = $(hbarcontainer, el);
	        vScrollbar = $(vbarcontainer, el);
	        ok(vScrollbar.is(":visible") && vScrollbar.outerHeight() === contentWrapper.height() && hScrollbar.outerWidth() === contentWrapper.width(), "Option 'vScroller.scrollBarVisibility is 'auto'. Both scroll bars are visible and active.");

	        $(".elements", el).css("display", "none");
	        el.wijsuperpanel("paintPanel");
	        hScrollbar = $(hbarcontainer, el);
	        vScrollbar = $(vbarcontainer, el);
	        ok(vScrollbar.size() === 0 && hScrollbar.size() === 0, "Option 'vScroller.scrollBarVisibility is 'auto' and there is no content. Both scroll bars are invisible.");

	        el.wijsuperpanel({ vScroller: { scrollBarVisibility: "visible" } });
	        el.wijsuperpanel("paintPanel");
	        hScrollbar = $(hbarcontainer, el);
	        vScrollbar = $(vbarcontainer, el);
	        ok(!vScrollbar.find(draghandleClass).is(":visible") && vScrollbar.hasClass("wijmo-wijsuperpanel-vbarcontainer-disabled") && vScrollbar.find(uiDisabled).size() === 3, "Option 'vScroller.scrollBarVisibility is 'visible' and there is no content. Vertical scroll bar is visible but disabled.");
	        $(".elements").css("display", "");
	        el.wijsuperpanel("destroy");
	    });

	    test("vScroller-scrollMode:", function () {
	        var el = $("#superPanel").wijsuperpanel({ vScroller: { scrollMode: "buttons" } });
	        el.wijsuperpanel("paintPanel");
	        var hscrollbar = $(hbarcontainer, el);
	        var vscrollbar = $(vbarcontainer, el);
	        var leftButton = $(buttonLeft, el);
	        var rightButton = $(buttonRight, el);
	        var topButton = $(buttonTop, el);
	        var bottomButton = $(buttonBottom, el);
	        ok(topButton.is(":visible") && bottomButton.is(":visible") && leftButton.size() == 0 && rightButton.size() == 0 && vscrollbar.size() == 0 && hscrollbar.is(":visible"), "Option 'vScroller.scrollMode' is 'buttons'. Vertical scroll bar is not visible. Vertical scroll buttons are visible.");

	        el.wijsuperpanel({ vScroller: { scrollMode: "buttonshover" } });
	        el.wijsuperpanel("paintPanel");
	        hscrollbar = $(hbarcontainer, el);
	        vscrollbar = $(vbarcontainer, el);
	        leftButton = $(buttonLeft, el);
	        rightButton = $(buttonRight, el);
	        topButton = $(buttonTop, el);
	        bottomButton = $(buttonBottom, el);
	        ok(topButton.is(":visible") && bottomButton.is(":visible") && leftButton.size() == 0 && rightButton.size() == 0 && vscrollbar.size() == 0 && hscrollbar.is(":visible"), "Option 'vScroller.scrollMode' is 'buttonshover'. Vertical scroll bar is not visible. Vertical scroll buttons are visible.");

	        el.wijsuperpanel({ vScroller: { scrollMode: "edge" } });
	        el.wijsuperpanel("paintPanel");
	        hscrollbar = $(hbarcontainer, el);
	        vscrollbar = $(vbarcontainer, el);
	        leftButton = $(buttonLeft, el);
	        rightButton = $(buttonRight, el);
	        topButton = $(buttonTop, el);
	        bottomButton = $(buttonBottom, el);
	        ok(leftButton.size() == 0 && rightButton.size() == 0 && topButton.size() == 0 && bottomButton.size() == 0 && vscrollbar.size() == 0 && hscrollbar.is(":visible"), "Option 'vScroller.scrollMode' is 'edge'. Vertical scrollbar is invisible. Vertical scroll buttons are invisible.");

	        el.wijsuperpanel({ vScroller: { scrollMode: "scrollbar,buttons" } });
	        el.wijsuperpanel("paintPanel");
	        hscrollbar = $(hbarcontainer, el);
	        vscrollbar = $(vbarcontainer, el);
	        leftButton = $(buttonLeft, el);
	        rightButton = $(buttonRight, el);
	        topButton = $(buttonTop, el);
	        bottomButton = $(buttonBottom, el);
	        ok(topButton.is(":visible") && bottomButton.is(":visible") && !leftButton.is(":visible") && !rightButton.is(":visible") && vscrollbar.is(":visible") && hscrollbar.is(":visible"), "Option 'vScroller.scrollMode' is 'scrollbar,buttons'. Vertical scroll bar is visible. Vertical scroll buttons are visible.");
	        el.wijsuperpanel("destroy");
	    });

	    test("vScroller-scrollValue", function () {
	        var el = $("#superPanel").wijsuperpanel();
	        var content = $(templateWrapper, el);
	        var option = el.wijsuperpanel("option");
	        var userMax = option.vScroller.scrollMax - option.vScroller.scrollLargeChange + 1;
	        el.wijsuperpanel({ vScroller: { scrollValue: userMax } });
	        el.wijsuperpanel("paintPanel");
	        var vDrag = $(vbarcontainer + " " + draghandleClass, el);
	        ok(Math.abs(el.data("wijmo-wijsuperpanel").options.vScroller.scrollValue - userMax) < 0.001 &&
            Math.abs(parseInt(vDrag.css("top")) - 247) < 2 &&
            Math.abs(parseInt(content.css("top")) + 3786) < 2,
            "Option 'vScroller.scrollValue' is set to max. Content wrapper and vertical drag handle are scrolled to bottom most position.");
	        el.wijsuperpanel({ vScroller: { scrollValue: 0 } });
	        el.wijsuperpanel("paintPanel");
	        vDrag = $(vbarcontainer + " " + draghandleClass, el);
	        ok(el.data("wijmo-wijsuperpanel").options.vScroller.scrollValue == 0 && vDrag.css("top") == "18px" && content.css("top") == "0px", "Option 'vScroller.scrollValue' is set to 0. Content wrapper and vertical drag handle are scrolled to top most position.");
	        //el.wijsuperpanel("destroy");
	    });

	    test("vScroller-firstStepChangeFix", function () {
	        var el = $("#superPanel");
	        el.wijsuperpanel({ vScroller: { firstStepChangeFix: 1.5, scrollValue: 0, scrollSmallChange: 1, scrollLargeChange: 10 } });
	        el.wijsuperpanel("paintPanel");
	        stop();
	        var arrow = $(arrowBottom);
	        arrow.simulate("mouseover");
	        arrow.simulate("mousedown");
	        arrow.simulate("mouseup");
	        arrow.simulate("mouseout");
	        window.setTimeout(function () {
	            var el = $("#superPanel");
	            var option = el.wijsuperpanel("option");
	            ok(el.data("wijmo-wijsuperpanel").options.vScroller.scrollValue == 2.5, "Option 'vScroller.firstStepChangeFix' is 1.5. Value after first step is fixed to 2.5.");
	            el.wijsuperpanel("destroy");
	            start();
	        }, 400);
	    });

	    test("keyboardSupport", function () {
	        var el = $("#superPanel");
	        el.wijsuperpanel({ keyboardSupport: false, vScroller: { firstStepChangeFix: 1.5, scrollValue: 0, scrollSmallChange: 1, scrollLargeChange: 10 } });
	        var option = el.wijsuperpanel("option");
	        var vbefore = option.vScroller.scrollValue;
	        el.simulate("keydown", { keyCode: $.ui.keyCode.DOWN });
	        option = el.wijsuperpanel("option");
	        var vafter = option.vScroller.scrollValue;
	        ok(vbefore == vafter, "Option 'keyboardSupport' is false. No value change after key down event.");
	        el.wijsuperpanel({ keyboardSupport: true });
	        el.simulate("keydown", { keyCode: $.ui.keyCode.DOWN });
	        window.setTimeout(function () {
	            vafter = el.data("wijmo-wijsuperpanel").options.vScroller.scrollValue;
	            ok(vbefore !== vafter, "Option 'keyboardSupport' is true. Value changed after key down event.");
	            start();
	        }, 1000);
	        stop();
	    });
	}
})(jQuery);
