/*
 * wijsuperpanel_events.js
 */
(function ($) {

	module("wijsuperpanel: events");

	if (!touchEnabled) {

		if (!$.browser.msie) {//can't simulate darg On IEcan't simulate darg on IE after jquery.simulate.js updated!
			test("resized", function () {
				var el = $("#superPanel");
				el.wijsuperpanel({
					allowResize: true, resized: function (e) {
						ok(true, "Event: resized fired.");
						el.wijsuperpanel("destroy");
						start();
					}
				});
				stop();
				$(".ui-resizable-se").simulate("drag", { dx: 30, dy: 30 });
			});

			test("dragstop-h", function () {
				var el = $("#superPanel");
				el.wijsuperpanel({
					dragStop: function (e, data) {
						ok(data.dragHandle == "h", "Event: horizontal dragstop fired.");
						el.wijsuperpanel("destroy");
						start();
					},
					scrollValueChanged: function (e, data) {
						ok(true, "Event: scrollValueChanged fired by dragging h bar.");
					}
				});

				stop();
				$(".wijmo-wijsuperpanel-hbarcontainer .wijmo-wijsuperpanel-handle").simulate("drag", { dx: 30, dy: 0 });
			});

			test("dragstop-v", function () {
				var el = $("#superPanel");
				el.wijsuperpanel({
					dragStop: function (e, data) {
						ok(data.dragHandle == "v", "Event: vertical dragstop fired.");
						el.wijsuperpanel("destroy");
						start();
					},
					scrollValueChanged: function (e, data) {
						ok(true, "Event: scrollValueChanged fired by dragging v bar.");
					}
				});

				stop();
				$(".wijmo-wijsuperpanel-vbarcontainer .wijmo-wijsuperpanel-handle").simulate("drag", { dx: 0, dy: 30 });
			});
		}

		test("scrolling, scroll, scrolled", function () {
			var el = $("#superPanel");
			el.wijsuperpanel({ hScroller: { firstStepChangeFix: 0, scrollValue: 0, smallChange: 1 }, vScroller: { firstStepChangeFix: 0, scrollValue: 0, smallChange: 1 } });
			el.wijsuperpanel("paintPanel");
			el.wijsuperpanel({
				scrolled: function (e, data) {
					ok(true, "Event: scrolled fired.");
					el.wijsuperpanel("destroy");
					start();
				}, scrolling: function (e, data) {
					ok(data.beforePosition.left == 0 && data.beforePosition.top == 0 && data.dir == "v" && data.oldValue == 0 && data.direction == "bottom", "Event: scrolling fired.");
				}, scroll: function (e, data) {
					ok(true, "Event: scroll fired.");
				}
			});
			stop();
			$(".wijmo-wijsuperpanel-vbar-buttonbottom").simulate("mouseover").simulate("mousedown").simulate("mouseup").simulate("mouseout");
		});

		test("scrollValueChanged",function() {
			var el = $("#superPanel"), idx = 0;
			el.wijsuperpanel({ hScroller: { firstStepChangeFix: 0, scrollValue: 0, smallChange: 1 }, vScroller: { firstStepChangeFix: 0, scrollValue: 0, smallChange: 1 } });
			el.wijsuperpanel("paintPanel");
			el.wijsuperpanel({
				scrollValueChanged: function (e, data) {
					idx++;
					if(idx === 1) {
						ok(true, "Event: scrollValueChanged fired by clicking bar.");
					} else if(idx === 2) {
						ok(true, "Event: scrollValueChanged fired by invoking scrollTo method.");
						el.wijsuperpanel("destroy");
						start();
					}
				}
			});
			stop();
			$(".wijmo-wijsuperpanel-vbar-buttonbottom").simulate("mouseover").simulate("click").simulate("mouseout");
			el.wijsuperpanel("scrollTo", 100, 100);
		});

		test("painted", function () {
			var el = $("#superPanel");
			el.wijsuperpanel({
				hScroller: { scrollValue: 0 }, vScroller: { scrollValue: 0 }, painted: function (e) {

					ok(true, "Event: painted fired.");
				}
			});
			el.wijsuperpanel("paintPanel");
			el.wijsuperpanel("destroy");
		});
	}
})(jQuery);
