/*
* wijsuperpanel_tickets.js
*/
(function ($) {
	var htmlText = '';
	htmlText += '<div id="sup1" style="width: 200px; height: 100px;">';
	htmlText += '                    <div class="elements">';
	htmlText += '                        <ul style=" width:200px">';
	htmlText += '                            <li style="background: none repeat scroll 0% 0% activeborder;"><span>ActiveBorder</span>';
	htmlText += '                            </li>';
	htmlText += '                            <li style="background: none repeat scroll 0% 0% activecaption;"><span>ActiveCaption</span>';
	htmlText += '                            </li>';
	htmlText += '                            <li class="li1" style="background: none repeat scroll 0% 0% captiontext;"><span>ActiveCaptionText</span>';
	htmlText += '                            </li>';
	htmlText += '                            <li style="background: none repeat scroll 0% 0% appworkspace;"><span>AppWorkspace</span>';
	htmlText += '                            </li>';
	htmlText += '                        </ul>';
	htmlText += '                    </div>';
	htmlText += '                </div>';
	module("wijsuperpanel: tickets");
	if (!touchEnabled) {
	    test("Bug scrollChildIntoView.", function () {
	        var el = $(htmlText).appendTo("body").wijsuperpanel();

	        el.wijsuperpanel("scrollChildIntoView", $(".li1"));
	        var top1 = parseInt(el.find(".wijmo-wijsuperpanel-templateouterwrapper").css("top"));
	        el.wijsuperpanel("scrollChildIntoView", $(".li1"));
	        var top2 = parseInt(el.find(".wijmo-wijsuperpanel-templateouterwrapper").css("top"));
	        same(top1, top2, "width and height automatically set.");
	        el.remove();
	    });

	    if ($.browser.msie && parseInt($.browser.version) < 9) {
	        test("In IE8, click the scrollbar, the superpanel can't scrolled", function () {
	            var el = $(htmlText).appendTo("body").wijsuperpanel();
	            $(".ui-icon-triangle-1-s").simulate("mouseover").simulate("mousedown");
	            setTimeout(function () {
	                ok($("#sup1").wijsuperpanel("option", "vScroller").scrollValue, "the superpanel scrolled.");
	                $("#sup1").remove();
	                start();
	            }, 1000)
	            stop();
	        });
	    }

	    test("#40479", function () {
	        var el = $(htmlText).appendTo("body").wijsuperpanel({ disabled: true }),
	        handler = el.find(".wijmo-wijsuperpanel-hbarcontainer .wijmo-wijsuperpanel-handle"), oldOffset, newOffset;

	        oldOffset = handler.offset();
	        handler.simulate("drag", { dx: 10 });
	        newOffset = handler.offset();
	        ok(oldOffset.left == newOffset.left, "The scrollbar is disabled");
	        el.remove();
	    });

	    test("Reset scrollValue according to scrollMax", function () {
	        var ele = $(htmlText).appendTo("body").wijsuperpanel(),
	            vScroller = ele.wijsuperpanel("option", "vScroller");
	        ele.wijsuperpanel("vScrollTo", 80, true);
	        vScroller.scrollMax = 50;
	        ele.wijsuperpanel("option", "vScroller", vScroller);
	        ele.wijsuperpanel("refresh");

	        ok(vScroller.scrollValue < 80 && vScroller.scrollValue < 50, "Updated \"scrollValue\" for scroller ! ");
	        ele.remove();
	    });

	    test("Content in superpanel can move when initial even if \"scrollBarVisiblity\" is hidden", function () {
	        var ele = $(htmlText).appendTo("body").wijsuperpanel({
	            animationOptions: null,
	            vScroller: {
	                scrollBarVisibility: "hidden",
	                scrollValue: 20
	            }
	        }), top, result;
	        top = Math.round(ele.find(".wijmo-wijsuperpanel-templateouterwrapper").position().top);
	        result = ele.wijsuperpanel("scrollValueToPx", 20, "v");
	        ok(top + result === 0, "Content in superpanel doesn't move when initialize");
	        ele.wijsuperpanel("vScrollTo", 30, true);
	        top = Math.round(ele.find(".wijmo-wijsuperpanel-templateouterwrapper").position().top);
	        result = ele.wijsuperpanel("scrollValueToPx", 30, "v");
	        ok(top + result === 0, "Content in superpanel doesn't move when use \"scrollTo\" method . ");
	        ele.remove();
	    });

	    test("62949", function () {
	        var ele = $(htmlText).appendTo("body").wijsuperpanel(),
                vScrollBarContainer = ele.find(".wijmo-wijsuperpanel-vbarcontainer"),
	            vScrollBar = vScrollBarContainer.find(".wijmo-wijsuperpanel-handle");
	        ele.wijsuperpanel("vScrollTo", 280, true);
	        setTimeout(function () {
	            ok(vScrollBar.position().top < vScrollBarContainer.innerHeight(), "Scroll bar is in its container!");
	            ele.remove();
	            start();
	        }, 500)
	        stop();
	    });

	    test("Test for the issue that parameter data for \"scrolled\" event does not contain the newValue", function () {
	        var ele = $(htmlText).appendTo("body").wijsuperpanel({
	            scrolled: function (e, data) {
	                var np = data.newValue;
	                ok(data.newValue, "Parameter for \"scrolled\" event can get newValue");
	                ele.remove();
	                start();
	            }
	        });
	        ele.wijsuperpanel("vScrollTo", 20, true);
	        stop();
	    });

	    test("Fixed issue in sample", function () {
	        var markup = "<div style='width: 265px; height: 150px'><ul><li>message messagemessage1</li><li></li><li>message messagemessage2</li><li></li><li>message messagemessage3</li><li></li><li>message messagemessage4</li><li></li><li>message messagemessage5</li></ul></div>",
                contentHeight,
                ele = $(markup).appendTo("body").wijsuperpanel();

	        contentHeight = ele.data("wijmo-wijsuperpanel")._fieldsStore["contentHeight"];
	        ok(contentHeight === ele.find("ul").outerHeight(true), "Got correct height for content ! ");
	        ele.remove();
	    });

	    test("disabled option", function () {
	        var el = $(htmlText).appendTo("body").wijsuperpanel({ disabled: true });
	        ok(el.hasClass("ui-state-disabled"), "The disabled class has added to element.");
	        el.remove();
	    });

	    test("#88570", function () {
	        $.wijmo.wijsuperpanel.autoRereshInterval = 50;

	        var el = $(htmlText).appendTo("body").hide();

	        el.wijsuperpanel({
	            autoRefresh: true
	        });

	        setTimeout(function () {
	            ok(true, "JS error didn't occur !");

	            el.show();
	            setTimeout(function () {
	                ok(el.data("wijmo-wijsuperpanel")._fieldsStore.initialized, "SuperPanel has been created after element shown.");
	                start();
	                el.remove();
	            }, 100);
	        }, 100);
	        stop();

	        $.wijmo.wijsuperpanel.autoRereshInterval = null;
	    });
	} else {
	   
	    test("In IE10 metro mode, the scrollValue in hScroller/vScroller lost effect", function () {
	        var el = $(htmlText).appendTo("body").wijsuperpanel();
	        el.find(".wijmo-wijsuperpanel-statecontainer").prop("scrollTop", 10);
	        setTimeout(function () {
	            ok(el.wijsuperpanel("option", "vScroller").scrollValue > 0, "the scrollValue in vScroller has an value.");
	            el.wijsuperpanel("destroy");
	            el.wijsuperpanel({ vScroller: { scrollValue: 50 } });
	            setTimeout(function () {
	                ok(el.find(".wijmo-wijsuperpanel-statecontainer").prop("scrollTop") > 0, "init the superpanel with scrollValue, the scrollValue is applied to the widget.");
	                el.remove();
	                start();
	            }, 200);
	        }, 100);
	        stop();
	    });
	}
})(jQuery);
