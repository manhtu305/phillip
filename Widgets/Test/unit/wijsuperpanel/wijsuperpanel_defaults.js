/*
* wijsuperpanel_defaults.js
*/
var wijsuperpanel_defaults = {
	/// <summary>
	/// Selector option for auto self initialization. 
	///	This option is internal.
	/// </summary>
	initSelector: ":jqmData(role='wijsuperpanel')",
	wijMobileCSS: {
		header: "ui-header ui-bar-a",
		content: "ui-body-b",
		stateDefault: "ui-btn ui-btn-b",
		stateHover: "ui-btn-down-c",
		stateActive: "ui-btn-down-c"
	},
	wijCSS: $.extend(true,$.wijmo.wijCSS,{
		superpanelHeader: "wijmo-wijsuperpanel-header",
		superpanelFooter: "wijmo-wijsuperpanel-footer",
		superpanelHandle: "",
		superpanelVBarbuttonTop: "",
		superpanelVBarbuttonBottom: "",
		superpanelHBarbuttonLeft: "",
		superpanelHBarbuttonRight: "",
		superpanelHBarContainer: "",
		superpanelVBarContainer: "",
		superpanelButton: "",
		superpanelButtonLeft: "",
		superpanelButtonRight: "",
		superpanelButtonTop: "",
		superpanelButtonBottom: ""
	}),
	/// <summary>
	/// This value determines whether the wijsuperpanel can be resized. 
	/// Default: false.
	/// Type: Boolean.
	/// </summary>
	allowResize: false,
	/// <summary>
	/// This value determines whether wijsuperpanel to automatically refresh 
	/// when content size or wijsuperpanel size are changed.
	/// Default: false.
	/// Type: Boolean.
	/// </summary>
	autoRefresh: false,
	/// <summary>
	/// The animation properties of wijsuperpanel scrolling.
	/// Type: Object.
	/// </summary>
	/// <remarks>
	/// Set this options to null to disable animation.
	/// </remarks>
	animationOptions: {
		/// <summary>
		/// This value determines whether to queue animation operations.
		/// Default: false.
		/// Type: Boolean.
		/// </summary>
		queue: false,
		"disabled": false,
		/// <summary>
		/// This value sets the animation duration of the scrolling animation.
		/// Default: 250.
		/// Type: Number.
		/// </summary>
		duration: 250
	},
	/// <summary>
	/// The hScrollerActivating event handler. 
	/// A function called when horizontal scrollbar is activating.
	/// Default: null
	/// Type: Function
	/// Code example: 
	/// Supply a function as an option.
	/// $("#selector").wijsuperpanel({ hScrollerActivating: function (e, data) { } });
	/// Bind to the event by type: wijtreenodeClick
	/// $("#selector").bind("wijsuperpanelhScrollerActivating", function(e, data) { } );
	/// </summary>
	/// <param name="e" type="Object">
	/// jQuery.Event object.
	/// </param>
	/// <param name="data" type="Object">
	/// The data that relates to this event.
	/// data.direction: the direction of the scrollbar("horizontal" or "vertical").
	/// data.targetBarLen: the height of the horizontal scrollbar.
	/// data.contentLength: the height of the content.
	/// </param>
	hScrollerActivating: null,
	/// <summary>
	/// This option contains horizontal scroller settings.
	/// </summary>
	hScroller: {
		/// <summary>
		/// This value determines the position of the horizontal scroll bar. 
		/// Default: "bottom".
		/// Type: String.
		/// </summary>
		/// <remarks>
		/// Possible options are "bottom" and "top".
		/// "bottom" - The horizontal scroll bar is placed at the bottom of 
		/// the content area.
		/// "top" - The horizontal scroll bar is placed at the top of the 
		///content area.
		/// </remarks>
		scrollBarPosition: "bottom",
		/// <summary>
		/// This value determines the visibility of the horizontal scroll bar.
		/// Default: "auto".
		/// Type: String
		/// </summary>
		/// <remarks>
		/// Possible options are "auto", "visible" and "hidden".
		/// "auto" - Shows the scroll when needed.
		/// "visible" - Scroll bar will always be visible. It"s disabled 
		/// when not needed.
		/// "hidden" - Scroll bar will be hidden.
		/// </remarks>
		scrollBarVisibility: "auto",
		/// <summary>
		/// This value determines the scroll mode of horizontal scrolling. 
		/// Default: "scrollbar".
		/// Type: String.
		/// </summary>
		/// <remarks>
		/// Possible options are "scrollBar", "buttons", "buttonsHover" 
		/// and "edge".
		/// "scrollBar" - Scroll bars are used for scrolling.
		/// "buttons" - Scroll buttons are used for scrolling. 
		/// Scrolling occurs only when scroll buttons are clicked.
		/// "buttonsHover" - Scroll buttons are used for scrolling. 
		/// Scrolling occurs only when scroll buttons are hovered.
		/// "edge" - Scrolling occurs when the mouse is moving to the edge
		/// of the content area.
		/// Scroll modes can be combined with each other. 
		/// For example, scrollMode: "scrollbar,scrollbuttons" will enable 
		/// both a scrollbar and scroll buttons.
		/// </remarks>
		scrollMode: "scrollBar",
		/// <summary>
		/// This value determines the horizontal scrolling position of
		/// wijsuperpanel.
		/// Default: null.
		/// Type: Number.
		/// </summary>
		scrollValue: null,
		/// <summary>
		/// This value sets the maximum value of horizontal scroller.
		/// Default: 100.
		/// Type: Number.
		/// </summary>
		scrollMax: 100,
		/// <summary>
		/// This value sets the minimum value of horizontal scroller.
		/// Default: 0.
		/// Type: Number.
		/// </summary>
		scrollMin: 0,
		/// <summary>
		/// This value sets the large change value of horizontal scroller.
		/// Default: null.
		/// Type: Number.
		/// </summary>
		/// <remarks>
		/// wijsuperpanel will scroll a large change when a user clicks on the 
		/// tracks of scroll bars or presses left or right arrow keys on the 
		/// keyboard with the shift key down.
		/// When scrollLargeChange is null, wijsuperpanel will scroll 
		/// the width of content.
		/// </remarks>
		scrollLargeChange: null,
		/// <summary>
		/// This value sets the small change value of horizontal scroller.
		/// Default: null. 
		/// Type: Number.
		/// </summary>
		/// <remarks>
		/// wijsuperpanel will scroll a small change when a user clicks on 
		/// the arrows of scroll bars, clicks or hovers scroll buttons, 
		/// presses left or right arrow keys on keyboard, 
		/// and hovers on the edge of wijsuperpanel.
		/// When scrollSmallChange is null, wijsuperpanel will scroll half of 
		/// the width of content.
		/// </remarks>
		scrollSmallChange: null,
		/// <summary>
		/// This value sets the minimum length, in pixel, of the horizontal 
		/// scroll bar thumb button.
		/// Default: 6.
		/// Type: Number.
		/// </summary>
		scrollMinDragLength: 6,
		/// <summary>
		/// This object determines the increase button position. 
		/// Default: null.
		/// Type: Object.
		/// </summary>
		/// <remarks>
		/// Please look at the options for jquery.ui.position.js for more info.
		/// </remarks>
		increaseButtonPosition: null,
		/// <summary>
		/// This object determines the decrease button position.
		/// Default: 0.
		/// Type: Object.
		/// </summary>
		decreaseButtonPosition: null,
		/// <summary>
		/// This value sets the width of horizontal hovering edge 
		/// which will trigger the horizontal scrolling.
		/// Default: 20.
		/// Type: Number.
		/// </summary>
		hoverEdgeSpan: 20,
		/// <summary>
		/// The number specifies the value to add to smallchange or largechange
		/// when scrolling the first step(scrolling from scrollMin).
		/// Default: 0.
		/// Type: Number.
		/// </summary>
		firstStepChangeFix: 0

	},
	/// <summary>
	/// A value determins whether wijsuperpanel provides 
	/// keyboard scrolling support.
	/// Default: false.
	/// Type: Boolean.
	/// </summary>
	keyboardSupport: false,
	/// <summary>
	/// This value determines the time interval to call the scrolling
	/// function when doing continuous scrolling.
	/// Default: 100.
	/// Type: Number.
	/// </summary>
	keyDownInterval: 100,
	/// <summary>
	/// This value determines whether wijsuperpanel has mouse wheel support.
	/// Default: true.
	/// Type: Boolean.
	/// </summary>
	/// <remarks>
	/// Mouse wheel plugin is needed to support this feature.
	/// </remarks>
	mouseWheelSupport: true,
	/// <summary>
	/// This value determines whether to fire the mouse wheel event 
	/// when wijsuperpanel is scrolled to the end.
	/// Default: true.
	/// Type: Boolean.
	/// </summary>
	bubbleScrollingEvent: true,
	/// <summary>
	/// This option determines the behavior of resizable widget. 
	/// See JQuery UI resizable options document.
	/// Type: Object.
	/// </summary>
	resizableOptions: {
		handles: "all",
		helper: "ui-widget-content wijmo-wijsuperpanel-helper"
	},
	/// <summary>
	/// Resized event handler. A function gets called when resized event is fired.
	/// Default: null.
	/// Type: Function.
	/// code example:
	/// Supply a callback function to handle the resized event:
	/// $("#element").wijsuperpanel({ resized: funtion() { dosometing } });
	/// Bind to the event by type:
	/// $("#element").bind("wijsuperpanelresized", funtion() { dosometing });
	/// </summary>
	resized: null,
	/// <summary>
	/// This function gets called when thumb buttons of scrollbars dragging stops.
	/// Default: null.
	/// Type: Function.
	/// code example:
	/// Supply a callback function to handle the dragstop event:
	/// $("#element").wijsuperpanel({ dragstop: funtion(e, data) { dosometing } });
	/// Bind to the event by type:
	/// $("#element").bind("wijsuperpaneldragstop", funtion(e, data) { dosometing });
	/// <param name="e" type="EventObj">
	/// EventObj relates to this event.
	/// </param>
	/// <param name="data" type="Object">
	/// The data with this event.
	/// data.dir: data.draghandle is the direction of the scrolling action. 
	/// Possible values: "v"(vertical) and "h"(horizontal).	
	/// </param>
	/// </summary>
	dragStop: null,
	/// <summary>
	/// This function gets called after panel is painted.
	/// Default: null.
	/// Type: Function.
	/// code example:
	/// Supply a callback function to handle the painted event:
	/// $("#element").wijsuperpanel({ painted: funtion() { dosometing } });
	/// Bind to the event by type:
	/// $("#element").bind("wijsuperpanelpainted", funtion() { dosometing });
	/// </summary>
	painted: null,
	/// <summary>
	/// Scrolling event handler. A function called before scrolling occurs.
	/// Default: null.
	/// Type: Function.
	/// code example:
	/// Supply a callback function to handle the scrolling event:
	/// $("#element").wijsuperpanel({ scrolling: funtion(e, data) { dosometing } });
	/// Bind to the event by type:
	/// $("#element").bind("wijsuperpanelscrolling", funtion(e, data) { dosometing });
	/// </summary>
	/// <param name="e" type="Object">
	/// jQuery.Event object.
	/// </param>
	/// <param name="data" type="Object">
	/// The data with this event.
	/// data.oldValue: The scrollValue before scrolling occurs.
	/// data.newValue: The scrollValue after scrolling occurs.
	/// data.dir: The direction of the scrolling action. 
	/// Possible values: "v"(vertical) and "h"(horizontal).
	/// data.beforePosition: The position of content before scrolling occurs.
	/// </param>
	scrolling: null,
	/// <summary>
	/// Scrolled event handler.  A function called after scrolling occurs.
	/// Default: null.
	/// Type: Function.
	/// code example:
	/// Supply a callback function to handle the scrolled event:
	/// $("#element").wijsuperpanel({ scrolled: funtion(e, data) { dosometing } });
	/// Bind to the event by type:
	/// $("#element").bind("wijsuperpanelscrolled", funtion(e, data) { dosometing });
	/// </summary>
	/// <param name="e" type="Object">
	/// jQuery.Event object.
	/// </param>
	/// <param name="data" type="Object">
	/// The data with this event.
	/// data.dir: The direction of the scrolling action. 
	/// Possible values: "v"(vertical) and "h"(horizontal).
	/// data.beforePosition: The position of content before scrolling occurs.
	/// data.afterPosition: The position of content after scrolling occurs.
	/// </param>
	scrolled: null,
	/// <summary>
	/// This value determines whether to show the rounded corner of wijsuperpanel.
	/// Default: true.
	/// Type: Boolean.
	/// </summary>
	showRounder: true,
	/// <summary>
	/// The vScrollerActivating event handler. 
	/// A function called when vertical scrollbar is activating.
	/// Default: null
	/// Type: Function
	/// Code example: 
	/// Supply a function as an option.
	/// $("#selector").wijsuperpanel({ vScrollerActivating: function (e, data) { } });
	/// Bind to the event by type: wijtreenodeClick
	/// $("#selector").bind("wijsuperpanelvScrollerActivating", function(e, data) { } );
	/// </summary>
	/// <param name="e" type="Object">
	/// jQuery.Event object.
	/// </param>
	/// <param name="data" type="Object">
	/// The data that relates to this event.
	/// data.direction: the direction of the scrollbar("horizontal" or "vertical").
	/// data.targetBarLen: the width of the vertical scrollbar.
	/// data.contentLength: the width of the content.
	/// </param>
	vScrollerActivating: null,
	/// <summary>
	/// This option contains vertical scroller settings.
	/// </summary>			
	vScroller: {
		/// <summary>
		/// This value determines the position of vertical scroll bar. 
		/// Default: "right".
		/// Type: String.
		/// </summary>
		/// <remarks>
		/// Possible options are: "left", "right".
		/// "left" - The vertical scroll bar is placed at the 
		/// left side of the content area.
		/// "right" - The vertical scroll bar is placed at the 
		/// right side of the content area.
		/// </remarks>
		scrollBarPosition: "right",
		/// <summary>
		/// This value determines the visibility of the vertical scroll bar.
		/// Default.: "auto". 
		/// Type: String.
		/// </summary>
		/// <remarks>
		/// Possible options are "auto", "visible" and "hidden".
		/// "auto" - Shows the scroll bar when needed.
		/// "visible" - Scroll bar will always be visible. 
		/// It"s disabled when not needed.
		/// "hidden" - Scroll bar will be shown.
		/// </remarks>
		scrollBarVisibility: "auto",
		/// <summary>
		/// This value determines the scroll mode of vertical scrolling. 
		/// Default: "scrollbar".
		/// Type: String.
		/// </summary>
		/// <remarks>
		/// Possible options are: "scrollBar", "buttons", 
		/// "buttonsHover" and "edge".
		/// "scrollBar" - Scroll bars are used for scrolling.
		/// "buttons" - Scroll buttons are used for scrolling. 
		/// Scrolling occurs only when scroll buttons are clicked.
		/// "buttonsHover" - Scroll buttons are used for scrolling. 
		/// Scrolling occurs only when scroll buttons are hovered.
		/// "edge" - Scrolling occurs when the mouse is moving to 
		/// the edge of the content area.
		/// Scroll modes can be combined with each other. 
		/// For example, vScrollMode: "scrollbar,scrollbuttons" will enable 
		/// both a scrollbar and scroll buttons.
		/// </remarks>
		scrollMode: "scrollBar",
		/// <summary>
		/// This value determines the vertical scrolling position of
		/// wijsuperpanel.
		/// Default: null.
		/// Type: Number.
		/// </summary>
		scrollValue: null,
		/// <summary>
		/// This value sets the maximum value of vertical scroller.
		/// Default: 100.
		/// Type: Number.
		/// </summary>
		scrollMax: 100,
		/// <summary>
		/// This value sets the minimum value of vertical scroller.
		/// Default: 0.
		/// Type: Number.
		/// </summary>
		scrollMin: 0,
		/// <summary>
		/// This value sets the large change value of vertical scroller. 
		/// Default: null.
		/// Type: Number.
		/// </summary>
		/// <remarks>
		/// wijsuperpanel will scroll a large change when a user clicks 
		/// on the tracks of scroll bars or presses left or right arrow keys 
		/// on the keyboard with the shift key down.
		/// When scrollLargeChange is null, wijsuperpanel 
		/// will scroll the height of content.
		/// </remarks>
		scrollLargeChange: null,
		/// <summary>
		/// This value sets the small change value of vertical scroller. 
		/// Default: null.
		/// Type: Number.
		/// </summary>
		/// <remarks>
		/// wijsuperpanel will scroll a small change when a user clicks on the 
		/// arrows of scroll bars, clicks or hovers scroll buttons, presses left
		/// or right arrow keys on keyboard, and hovers on the edge of 
		/// wijsuperpanel.
		/// When scrollSmallChange is null, wijsuperpanel will scroll half of 
		/// the height of content.
		/// </remarks>
		scrollSmallChange: null,
		/// <summary>
		/// This value sets the minimum length, in pixel, of the vertical 
		/// scroll bar thumb button.
		/// Default: 6.
		/// Type: Number
		/// </summary>
		scrollMinDragLength: 6,
		/// <summary>
		/// This object determines the increase button position. 
		/// Default: null.
		/// Type: Object.
		/// </summary>
		/// <remarks>
		/// Please look at the options for jquery.ui.position.js for more info.
		/// </remarks>
		increaseButtonPosition: null,
		/// <summary>
		/// This object determines the decrease button position.
		/// Default: 0.
		/// Type: Object.
		/// </summary>
		/// <remarks>
		/// Please look at the options for jquery.ui.position.js for more info.
		/// </remarks>
		decreaseButtonPosition: null,
		/// <summary>
		/// This value sets the width of horizontal hovering edge 
		/// which will trigger the vertical scrolling.
		/// Default: 20.
		/// Type: Number.
		/// </summary>
		hoverEdgeSpan: 20,
		/// <summary>
		/// The value to add to small change or largechange when scrolling 
		/// the first step(scrolling from value 0).
		/// Default: 0.
		/// Type: Number.
		/// </summary>
		firstStepChangeFix: 0
	},
	scroll: null,
	create: null,
	disabled: false,
	customScrolling: false,
	listenContentScroll: false

};


if (touchEnabled) {
	wijsuperpanel_defaults = {
		wijMobileCSS: {
			header: "ui-header ui-bar-a",
			content: "ui-body-b",
			stateDefault: "ui-btn ui-btn-b",
			stateHover: "ui-btn-down-c",
			stateActive: "ui-btn-down-c"
		},
		/// <summary>
		/// Selector option for auto self initialization. 
		///	This option is internal.
		/// </summary>
		initSelector: ":jqmData(role='wijsuperpanel')",
		allowResize: false,
		animationOptions: {
			queue: false,
			disabled: false,
			duration: 250
		},
		hScroller: {
			scrollBarVisibility: "auto",
			scrollValue: null,
			scrollMax: 100,
			firstStepChangeFix: 0,
			scrollMin: 0,
			hoverEdgeSpan: 20
		},
		keyboardSupport: false,
		keyDownInterval: 100,
		mouseWheelSupport: true,
		resizableOptions: {
			handles: "all",
			helper: "ui-widget-content wijmo-wijsuperpanel-helper"
		},
		resized: null,
		painted: null,
		scroll: null,
		showRounder: true,
		vScroller: {
			scrollBarVisibility: "auto",
			scrollValue: null,
			scrollMax: 100,
			scrollMin: 0,
			firstStepChangeFix: 0,
			hoverEdgeSpan: 20
		}
	}
}

$.wijmo.wijsuperpanel.prototype.option = $.Widget.prototype.option;
$.wijmo.wijsuperpanel.prototype._trigger = $.Widget.prototype._trigger;

commonWidgetTests('wijsuperpanel', {
	defaults: wijsuperpanel_defaults
});
