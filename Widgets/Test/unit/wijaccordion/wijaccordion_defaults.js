/*
 * wijaccordion_defaults.js
 */
var wijaccordion_defaults = {
    animated: "slide",
    duration: null,
    event: "click",
    disabled: false,
    create: null,
    expandDirection: "bottom",
    header: "> li > :first-child,> :not(li):even",
    requireOpenedPane: true,
    selectedIndex: 0,
    wijCSS: $.extend(true, {
        wijaccordion: "",
        wijaccordionTop: "",
        wijaccordionBottom: "",
        wijaccordionLeft: "",
        wijaccordionRight: "",
        wijaccordionHeader: "",
        wijaccordionContent: "",
        wijaccordionContentActive: "",
        wijaccordionIcons: ""
    }, $.wijmo.wijCSS),
    wijMobileCSS: {
        "header": "ui-header ui-bar-a",
        "content": "ui-body ui-body-b"
    },
    initSelector: ":jqmData(role='wijaccordion')",
    beforeSelectedIndexChanged: null,
    selectedIndexChanged: null
};

commonWidgetTests("wijaccordion", {
    defaults: wijaccordion_defaults
});
