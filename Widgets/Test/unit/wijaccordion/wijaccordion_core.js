/*
 * wijaccordion_core.js
 */

function create_wijaccordion(options, header, content) {
    return $("<div>" +
                "<div>" + (header ? header : "header") + "</div><div>" + (content ? content : "content") + "</div>" +
                "<div>" + (header ? header : "header") + "</div><div>" + (content ? content : "content") + "</div>" +
                "<div>" + (header ? header : "header") + "</div><div>" + (content ? content : "content") + "</div>" +
            "</div>").appendTo(document.body).wijaccordion(options);
}

(function ($) {

    module("wijaccordion: core");

    test("create and destroy", function () {
        // test disconected element
        var $widget = create_wijaccordion(),
            elementHTML = "<div><div>header</div><div>content</div><div>header</div><div>content</div><div>header</div><div>content</div></div>";

        ok($widget.hasClasses("wijmo-wijaccordion ui-widget wijmo-wijaccordion-icons ui-helper-clearfix wijmo-wijaccordion-bottom"), "element css classes created.");
        ok($widget.find(".wijmo-wijaccordion-header").hasClasses("ui-state-active ui-corner-top"), "header css classes created.");
        $widget.appendTo(document.body);
        ok($widget.find(".wijmo-wijaccordion-content")[0].offsetHeight > 0, "content panel is visible");
        ok($widget.find(".wijmo-wijaccordion-content").eq(0).is(":visible"), "The first panel should be visible");
        ok($widget.find(".wijmo-wijaccordion-content").length === 3, "There are three content panel in widget");
        ok($widget.find(".wijmo-wijaccordion-header").length === 3, "There are three header panel in widget");

        $widget.wijaccordion("destroy");
        ok(!$widget.hasClasses("wijmo-wijaccordion wijmo-wijaccordion ui-widget wijmo-wijaccordion-icons"), "destroy: element css classes removed.");
        ok($widget[0].outerHTML === elementHTML, "Element recovered !");
        $widget.remove();
    });

    // todo: need to test creation of other header patterns. above is just "> :not(li):even".

})(jQuery);