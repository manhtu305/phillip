/*
 * wijaccordion_tickets.js
 */
(function ($) {

    module("wijaccordion: tickets");
    test("rounding classes: ", function () {
        var $widget = create_wijaccordion({ expandDirection: "right" });

        ok($widget.find('.wijmo-wijaccordion-header').hasClass("ui-corner-left"), 'header for right orientation should have ui-corner-left class');
        ok($widget.find('.wijmo-wijaccordion-content').hasClass("ui-corner-right"), 'content for right orientation should have ui-corner-right class');

        $widget.wijaccordion("option", "expandDirection", "left");
        ok($widget.find('.wijmo-wijaccordion-header').hasClass("ui-corner-right"), 'header for left orientation should have ui-corner-right class');
        ok($widget.find('.wijmo-wijaccordion-content').hasClass("ui-corner-left"), 'content for left orientation should have ui-corner-left class');

        $widget.wijaccordion("option", "expandDirection", "top");
        ok($widget.find('.wijmo-wijaccordion-header').hasClass("ui-corner-bottom"), 'header for top orientation should have ui-corner-bottom class');
        ok($widget.find('.wijmo-wijaccordion-content').hasClass("ui-corner-top"), 'content for top orientation should have ui-corner-top class');

        $widget.wijaccordion("option", "expandDirection", "bottom");
        ok($widget.find('.wijmo-wijaccordion-header').hasClass("ui-corner-top"), 'header for bottom orientation should have ui-corner-top class');
        ok($widget.find('.wijmo-wijaccordion-content').hasClass("ui-corner-bottom"), 'content for bottom orientation should have ui-corner-bottom class');
        $widget.remove();
    });

    test("wijmo-wijaccordion-content-active: ", function () {
        var $widget = create_wijaccordion({ expandDirection: "right", animated: false });

        ok($widget.find('.wijmo-wijaccordion-content').eq(0).hasClass("wijmo-wijaccordion-content-active"), 'first accordion pane is active');
        $widget.find(".wijmo-wijaccordion-header").eq(2).simulate('click');
        ok(!$widget.find('.wijmo-wijaccordion-content').eq(0).hasClass("wijmo-wijaccordion-content-active"), 'first accordion pane is inactive');
        ok($widget.find('.wijmo-wijaccordion-content').eq(2).hasClass("wijmo-wijaccordion-content-active"), 'third accordion pane is active');
        $widget.remove();
    });

    test("navigation using up/down left/right keys: ", function () {
        var $widget = create_wijaccordion();

        $widget.find('.wijmo-wijaccordion-header')[0].focus();
        ok($widget.find('.wijmo-wijaccordion-header').eq(0).hasClass("ui-state-focus"), 'first accordion header is focused');
        $widget.find(".wijmo-wijaccordion-header").eq(0).simulate("keydown", { keyCode: $.ui.keyCode.DOWN });
        $widget.find(".wijmo-wijaccordion-header").eq(0).simulate("keydown", { keyCode: $.ui.keyCode.DOWN });
        ok(!$widget.find('.wijmo-wijaccordion-header').eq(0).hasClass("ui-state-focus"), 'first accordion header looses focus');
        ok(!$widget.find('.wijmo-wijaccordion-header').eq(1).hasClass("ui-state-focus"), 'second accordion header is not focused');
        ok($widget.find('.wijmo-wijaccordion-header').eq(2).hasClass("ui-state-focus"), 'third accordion header is focused');
        $widget.find(".wijmo-wijaccordion-header").eq(0).simulate("keydown", { keyCode: $.ui.keyCode.UP });
        ok(!$widget.find('.wijmo-wijaccordion-header').eq(2).hasClass("ui-state-focus"), 'third accordion header looses focus');
        ok($widget.find('.wijmo-wijaccordion-header').eq(1).hasClass("ui-state-focus"), 'second accordion header is focused');
        $widget.remove();
    });

    test("[17869] Unable to select the desire panel after all the panels are open in certain scenarios", function () {
        var $widget = create_wijaccordion({ requireOpenedPane: false });

        $widget.find(".wijmo-wijaccordion-header").eq(1).simulate("click");
        $widget.find(".wijmo-wijaccordion-header").eq(2).simulate("click");
        ok($widget.find('.wijmo-wijaccordion-header').eq(0).hasClass("ui-state-active"), 'first accordion header is active');
        ok($widget.find('.wijmo-wijaccordion-header').eq(1).hasClass("ui-state-active"), 'second accordion header is active');
        ok($widget.find('.wijmo-wijaccordion-header').eq(2).hasClass("ui-state-active"), 'third accordion header is active');
        $widget.wijaccordion("option", "requireOpenedPane", true);
        $widget.find(".wijmo-wijaccordion-header").eq(2).simulate("click");
        ok(!$widget.find('.wijmo-wijaccordion-header').eq(0).hasClass("ui-state-active"), 'first accordion header is not active');
        ok(!$widget.find('.wijmo-wijaccordion-header').eq(1).hasClass("ui-state-active"), 'second accordion header is not active');
        ok($widget.find('.wijmo-wijaccordion-header').eq(2).hasClass("ui-state-active"), 'third accordion header is active');
        $widget.remove();
    });

    test("[25980] When clicking the Header area of C1Accordion, another C1Accordion inside its Content area is collapsed automatically.", function () {
        var $widget1 = create_wijaccordion({}, "outer header", "outer content"), header10 = $widget1.find(".wijmo-wijaccordion-header").eq(0),
            $widget2 = create_wijaccordion({}, "inner header", "inner content"), header20 = $widget2.find(".wijmo-wijaccordion-header").eq(0);

        $widget2.prependTo($widget1.find(".wijmo-wijaccordion-content")[0]);
        ok(header20.hasClass("ui-state-active"), 'second accordion: first header is active');
        header10.simulate("click");
        ok(header20.hasClass("ui-state-active"), 'second accordion: first header still active');
        $widget1.remove();
        $widget2.remove();
    });

    // the test can be run in IE,to avoid the other tests failed the by window resizing,
    // comment the test code, you can run it singly.
    /**
    test("#36827", function () {
        var $widget = create_wijaccordion({
            expandDirection: "left"
        });
        $widget.prependTo(document.body);
        if ($.browser.msie) {
            window.resizeTo(500, 600);
            ok($widget.find(".wijmo-wijaccordion-content").width() < 576, 'second accordion: first header still active');
            
        }
        $widget.remove();
    });*/

    if ($.browser.msie && $.browser.version < 9) {
        test("#42756", function () {
            var $widget = create_wijaccordion();

            $widget.find(".wijmo-wijaccordion-header:last").simulate("click");
            ok(true, "No exception thrown.");
            $widget.remove();
        });
    }

    test("test panel's width after selected index and expandDirection is changed", function () {
        var $widget = create_wijaccordion({ expandDirection: "bottom" }),
            contentEle = $($widget.find(".wijmo-wijaccordion-content")[0]),
            initWidth;

        $widget.wijaccordion("option", "expandDirection", "right");
        initWidth = contentEle.css("width");
        $widget.wijaccordion("option", "selectedIndex", "1");
        $widget.wijaccordion("option", "selectedIndex", "0");

        setTimeout(function () {
            ok(contentEle.css("width") === initWidth, "the width equals default width after expanding and hiding");
            $widget.remove();
            start();
        }, 700);
        stop();
    });

    test("#79862", function () {
        var $widget = create_wijaccordion(),
            contentEle = $($widget.find(".wijmo-wijaccordion-content")[0]),
            initWidth,
            headers = $widget.find(".wijmo-wijaccordion-header"),
            contents = $widget.find(".wijmo-wijaccordion-content"), exceptWidth;
        $widget.wijaccordion("option", "expandDirection", "right");
        headers.eq(1).simulate("click");
        setTimeout(function () {
            $widget.wijaccordion("option", "expandDirection", "top");
            exceptWidth = contents.eq(1).outerWidth();
            ok(headers.eq(1).outerWidth() === exceptWidth, "The width of content is keep consistent");
            $widget.remove();
            start();
        }, 600);

        stop();
    });

    test("#79837", function () {
        jQuery.wijmo.wijaccordion.animations.customSlide = function (options) {
            this.slide(options, {
                easing: options.down ? "easeOutBounce" : "swing",
                duration: 50
            });
        };
        var eleHtml = "<div><h>header</h><div>content</div><h>header</h><div></div><h>header</h><div></div></div>",
            $widget = $(eleHtml).appendTo("body").wijaccordion({
                animated: "customSlide"
            }),
            headers = $widget.find(".wijmo-wijaccordion-header"),
            contents = $widget.find(".wijmo-wijaccordion-content"), exceptHeight;

        headers.eq(1).simulate("click");
        setTimeout(function () {
            headers.eq(2).simulate("click");
            exceptHeight = contents.eq(1).outerHeight();
            setTimeout(function () {
                ok(contents.eq(2).outerHeight() === exceptHeight, "The third content sliders into a correct height !");
                $widget.remove();
                start();
            }, 100);
        }, 100);

        stop();
    });

    test("#39424", function () {
        var $widget = create_wijaccordion({ animated: null, duration: 0 }),
            $contents = $widget.find(".wijmo-wijaccordion-content"),
            indices = [0, 1, 2, 1, 0, 1, 2, 1, 0, 1],
            keys = ["height", "padding-left", "padding-right", "padding-top", "padding-bottom"];

        $widget.wijaccordion("option", "expandDirection", "right");
        $.each(indices, function (i, index) {
            $widget.wijaccordion("option", "selectedIndex", index);
        });
        ok($contents.eq(1).width() > 10, "content div has correct width.");
        ok(keys.every(function (key) {
            return $contents.eq(1).attr("style").indexOf(key) < 0;
        }), "content div has no animation mid-state remains.");
        $widget.remove();
    });

    test("#96934", function () {
        var ele = $("<div style='width: 1000px'><div>header</div><div>content</div><div>header</div><div>content</div><div>header</div><div>content</div></div>").appendTo("body"),
            $widget = ele.wijaccordion({
                expandDirection: "right"
            }),
            content = $widget.find(".wijmo-wijaccordion-content-active"),
            headers = $widget.find(".wijmo-wijaccordion-header"),
            parentWidth = $widget.innerWidth(),
            contentWidth = content.outerWidth(true),
            headersWidth = headers.eq(0).outerWidth(true) * headers.length;

        ok(parentWidth - (contentWidth + headersWidth) <= 2, "The accordion displays its width much correctly !");
        $widget.remove();
    });

})(jQuery);
