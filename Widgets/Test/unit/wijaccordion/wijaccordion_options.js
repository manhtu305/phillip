/*
 * wijaccordion_options.js
 */
(function ($) {

    module("wijaccordion:options");

    test("header", function () {
        var $widget = create_wijaccordion({ animated: false }), headerText = "header";

        ok($widget.find(".wijmo-wijaccordion-header").eq(0).text() === headerText, "First accordion pane's header");
        ok($widget.find(".wijmo-wijaccordion-header").eq(1).text() === headerText, "Second accordion pane's header");
        ok($widget.find(".wijmo-wijaccordion-header").eq(2).text() === headerText, "Third accordion pane's header");

        $widget.wijaccordion({ header: " " });
        ok($widget.find(".wijmo-wijaccordion-header").eq(0).text() !== headerText, "Header is incorrect after setting wrong header in option");
        ok($widget.find(".wijmo-wijaccordion-header").eq(1).text() !== headerText, "Header is incorrect after setting wrong header in option");
        ok($widget.find(".wijmo-wijaccordion-header").eq(2).text() !== headerText, "Header is incorrect after setting wrong header in option");

        $widget.wijaccordion({ header: "> div:even " });
        ok($widget.find(".wijmo-wijaccordion-header").eq(0).text() === headerText, "First accordion pane's header");
        ok($widget.find(".wijmo-wijaccordion-header").eq(1).text() === headerText, "Second accordion pane's header");
        ok($widget.find(".wijmo-wijaccordion-header").eq(2).text() === headerText, "Third accordion pane's header");
        $widget.remove();
    });

    test("event", function () {
        var $widget = create_wijaccordion({ animated: false }), allEvents = "focus dblclick mousedown mouseup mousemove mouseover mouseout keydown keypress keyup",
            eventArray = allEvents.split(" "), index;

        //event's default value is click
        $widget.find(".wijmo-wijaccordion-header").eq(1).simulate('click');
        ok($widget.find('.wijmo-wijaccordion-content').eq(1).is(':visible'), 'accordion pane with index 1 expanded using click event');
        for (index = 0; index < eventArray.length; index++) {
            $widget.find(".wijmo-wijaccordion-header").eq(0).simulate(eventArray[index]);
            ok($widget.find('.wijmo-wijaccordion-content').eq(0).is(':hidden'), eventArray[index] + ' should not fire the header click event');
        }

        $widget.wijaccordion("option", "event", allEvents);
        $widget.find(".wijmo-wijaccordion-header").eq(0).simulate('click');
        ok($widget.find('.wijmo-wijaccordion-content').eq(0).is(':hidden'), 'accordion pane with index 0 still closed because click event no longer toggles panes visibility');
        for (index = 0; index < eventArray.length; index++) {
            $widget.find(".wijmo-wijaccordion-header").eq(index % 2).simulate(eventArray[index]);
            ok($widget.find('.wijmo-wijaccordion-content').eq(index % 2).is(':visible'), eventArray[index] + ' should fire the header click event');
        }
        $widget.remove();
    });

    test("disabled", function () {
        var $widget = create_wijaccordion({ disabled: true });

        ok($widget.hasClass('ui-state-disabled'), 'disabled class added by constructor');
        $widget.wijaccordion({ disabled: false });
        ok(!$widget.hasClass('ui-state-disabled'), 'no disabled classes');
        $widget.wijaccordion("option", "disabled", true);
        ok($widget.hasClass('ui-state-disabled'), 'disabled class added after option change');
        $widget.wijaccordion("option", "disabled", false);
        ok(!$widget.hasClass('ui-state-disabled'), 'disabled class removed after option change');
        $widget.remove();
    });

    test("expandDirection", function () {
        var $widget = create_wijaccordion({ expandDirection: "right" });

        ok($widget.find('.wijmo-wijaccordion-content')[0].offsetLeft > $widget.find('.wijmo-wijaccordion-header')[0].offsetLeft, 'expand direction is right, content located at right');
        $widget.wijaccordion("option", "expandDirection", "left");
        ok($widget.find('.wijmo-wijaccordion-content')[0].offsetLeft < $widget.find('.wijmo-wijaccordion-header')[0].offsetLeft, 'expand direction is left, content located at left');
        $widget.wijaccordion("option", "expandDirection", "top");
        ok($widget.find('.wijmo-wijaccordion-content')[0].offsetTop < $widget.find('.wijmo-wijaccordion-header')[0].offsetTop, 'expand direction is top, content located at top');
        $widget.wijaccordion("option", "expandDirection", "bottom");
        ok($widget.find('.wijmo-wijaccordion-content')[0].offsetTop > $widget.find('.wijmo-wijaccordion-header')[0].offsetTop, 'expand direction is bottom, content located at bottom');
        $widget.remove();
    });

    test("requireOpenedPane", function () {
        var $widget = create_wijaccordion({ requireOpenedPane: false, animated: false });

        $widget.find(".wijmo-wijaccordion-header").eq(1).simulate('click');
        ok($widget.find('.wijmo-wijaccordion-content').eq(0).is(':visible') && $widget.find('.wijmo-wijaccordion-content').eq(1).is(':visible'), 'more than one pane can be opened when requireOpenedPane is false');
        $widget.find(".wijmo-wijaccordion-header").eq(1).simulate('click');
        ok($widget.find('.wijmo-wijaccordion-content').eq(0).is(':visible'), "panel still visible when click other panel");
        ok($widget.find('.wijmo-wijaccordion-content').eq(1).is(':hidden'), "panel should be invisible after clicking it again");
        $widget.find(".wijmo-wijaccordion-header").eq(2).simulate('click');
        ok($widget.find('.wijmo-wijaccordion-content').eq(0).is(':visible'), "panel still visible when click other panel");
        ok($widget.find('.wijmo-wijaccordion-content').eq(2).is(':visible'), "more than one pane can be opened");

        $widget.wijaccordion("option", "requireOpenedPane", true);
        $widget.find(".wijmo-wijaccordion-header").eq(1).simulate('click');
        ok($widget.find('.wijmo-wijaccordion-content').eq(0).is(':hidden'), "other panels should be invisible after clicking a panel");
        ok($widget.find('.wijmo-wijaccordion-content').eq(1).is(':visible'), "The panel should be visible after clicking");
        ok($widget.find('.wijmo-wijaccordion-content').eq(2).is(':hidden'), "other panels should be invisible after clicking a panel");
        $widget.remove();
    });

    test("selectedIndex", function () {
        var $widget = create_wijaccordion({ selectedIndex: 1, animated: false });

        ok($widget.find('.wijmo-wijaccordion-content').eq(0).is(':hidden') && $widget.find('.wijmo-wijaccordion-content').eq(1).is(':visible'), 'selectedIndex is 1');
        $widget.wijaccordion("option", "selectedIndex", "0");
        ok($widget.find('.wijmo-wijaccordion-content').eq(0).is(':visible') && $widget.find('.wijmo-wijaccordion-content').eq(1).is(':hidden'), 'selectedIndex is 0');
        $widget.wijaccordion("option", "selectedIndex", "2");
        ok($widget.find('.wijmo-wijaccordion-content').eq(0).is(':hidden') && $widget.find('.wijmo-wijaccordion-content').eq(1).is(':hidden') && $widget.find('.wijmo-wijaccordion-content').eq(2).is(':visible'), 'selectedIndex is 2');
        $widget.remove();
    });

    test("layoutSetting after horizon expanding and hiding", function () {
        var $widget = create_wijaccordion({ expandDirection: "right", animated: false }),
            contentEle = $widget.find(".wijmo-wijaccordion-content").eq(1),
            initWidth = contentEle.css("width"),
            initPaddingLeft = contentEle.css("paddingLeft"),
            initPaddingRight = contentEle.css("paddingRight");

        $widget.wijaccordion("option", "selectedIndex", "1");
        $widget.wijaccordion("option", "selectedIndex", "2");
        $widget.wijaccordion("option", "selectedIndex", "1");
        ok(contentEle.css("width") === initWidth, "the width equals default width after expanding and hiding");
        ok(contentEle.css("paddingLeft") === initPaddingLeft, "the left padding equals default left padding after expanding and hiding");
        ok(contentEle.css("paddingRight") === initPaddingRight, "the right padding default right padding after expanding and hiding");

        $widget.wijaccordion({ expandDirection: "left" });
        ok(contentEle.css("width") === initWidth, "the width equals default width after expanding and hiding");
        ok(contentEle.css("paddingLeft") === initPaddingLeft, "the left padding equals default left padding after expanding and hiding");
        ok(contentEle.css("paddingRight") === initPaddingRight, "the right padding default right padding after expanding and hiding");
        $widget.remove();
    });

    test("layoutSetting after vertical expanding and hiding", function () {
        var $widget = create_wijaccordion({ expandDirection: "bottom", animated: false }),
            contentEle = $widget.find(".wijmo-wijaccordion-content");

        $widget.wijaccordion("option", "selectedIndex", "1");
        $widget.wijaccordion("option", "selectedIndex", "2");
        $widget.wijaccordion("option", "selectedIndex", "1");
        ok($(contentEle[1]).css("paddingTop") === "16px", "the left padding equals default left padding after expanding and hiding");
        ok($(contentEle[1]).css("paddingBottom") === "16px", "the right padding default right padding after expanding and hiding");

        $widget.wijaccordion({ expandDirection: "top" });
        ok($(contentEle[1]).css("paddingTop") === "16px", "the left padding equals default left padding after expanding and hiding");
        ok($(contentEle[1]).css("paddingBottom") === "16px", "the right padding default right padding after expanding and hiding");
        $widget.remove();
    });

    /*	
    animated
    // Animation easing effect name, set this option to false in order to disable animation (can be set only from widget constructor). (easing effects requires UI Effects Core).
    // You can create own animation as follow:
    // jQuery.wijmo.wijaccordion.animations.custom1 = function (options) {
    //     this.slide(options, {
    //     easing: options.down ? "easeOutBounce" : "swing",
    //     duration: options.down ? 1000 : 200
    //   });
    // }
    //  $("#accordion3").wijaccordion({
    //      expandDirection: "right",
    //      animated: "custom1"
    //  });
    // 
    // options that available for the animation function:
    //  down - value of true indicates that index of the pane to be expanded is higher than index of the pane that must be collapsed.
    //  horizontal - value of true indicates that accordion is in horizontal orientation (when expandDirection is left or right)
    //  rightToLeft - value of true indicates that content element located before header element (for the top and left expand direction)
    //  toShow - jQuery object that contains content elements to be shown
    //  toHide - jQuery object that contains content elements to be hidden
    // Default: "slide"
    */
})(jQuery);
