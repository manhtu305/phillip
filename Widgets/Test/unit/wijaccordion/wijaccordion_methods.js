/*
 * wijaccordion_methods.js
 */
(function ($) {

    module("wijaccordion: methods");

    test("activate", function () {
        var $widget = create_wijaccordion({ animated: false }), activeSuccessfully;

        ok($('.wijmo-wijaccordion-content').eq(1).is(':hidden'), 'accordion pane with index 1 is collapsed');
        activeSuccessfully = $widget.wijaccordion('activate', 1);
        ok(activeSuccessfully, 'active method is invoked successfully');
        ok($('.wijmo-wijaccordion-content').eq(0).is(':hidden'), 'accordion pane with index 0 is collapsed');
        ok($('.wijmo-wijaccordion-content').eq(1).is(':visible'), 'accordion pane with index 1 is expanded');
        ok($('.wijmo-wijaccordion-content').eq(2).is(':hidden'), 'accordion pane with index 2 is collapsed');
        ok($widget.wijaccordion("option", "selectedIndex") === 1, 'accordion pane with index 1 is collapsed');
        activeSuccessfully = $widget.wijaccordion('activate', 2);
        ok(activeSuccessfully, 'active method is invoked successfully');
        ok($('.wijmo-wijaccordion-content').eq(2).is(':visible'), 'accordion pane with index 2 is expanded');
        activeSuccessfully = $widget.wijaccordion('activate', 2);
        ok(!activeSuccessfully, 'active method is not invoked successfully after active an actived panel');
        ok($('.wijmo-wijaccordion-content').eq(2).is(':visible'), 'accordion pane with index 2 is expanded');

        //test other type of parameters
        activeSuccessfully = $widget.wijaccordion('activate', "0");
        ok(activeSuccessfully, 'active method is invoked successfully');
        ok($('.wijmo-wijaccordion-content').eq(0).is(':visible'), 'accordion pane with index 0 is expanded');
        activeSuccessfully = $widget.wijaccordion('activate', $widget.find(".wijmo-wijaccordion-header")[1]);
        ok(activeSuccessfully, 'active method is invoked successfully');
        ok($('.wijmo-wijaccordion-content').eq(1).is(':visible'), 'accordion pane with index 1 is expanded');

        //test with invalid parameters
        activeSuccessfully = $widget.wijaccordion('activate', -1);
        ok(activeSuccessfully, 'active method is invoked successfully');
        ok($widget.wijaccordion("option", "selectedIndex") === -1, 'selectedIndex is invalid value');
        ok($('.wijmo-wijaccordion-content').eq(0).is(':hidden'), 'accordion pane with index 0 is collapsed');
        ok($('.wijmo-wijaccordion-content').eq(1).is(':hidden'), 'accordion pane with index 1 is collapsed');
        ok($('.wijmo-wijaccordion-content').eq(2).is(':hidden'), 'accordion pane with index 2 is collapsed');
        $widget.wijaccordion('activate', 0);
        activeSuccessfully = $widget.wijaccordion('activate', 4);
        ok(activeSuccessfully, 'active method is invoked successfully');
        ok($widget.wijaccordion("option", "selectedIndex") === -1, 'selectedIndex is invalid value');
        ok($('.wijmo-wijaccordion-content').eq(0).is(':hidden'), 'accordion pane with index 0 is collapsed');
        ok($('.wijmo-wijaccordion-content').eq(1).is(':hidden'), 'accordion pane with index 1 is collapsed');
        ok($('.wijmo-wijaccordion-content').eq(2).is(':hidden'), 'accordion pane with index 2 is collapsed');
        $widget.wijaccordion('activate', 0);
        activeSuccessfully = $widget.wijaccordion('activate', $widget.find("#wrongID"));
        ok(activeSuccessfully, 'active method is not invoked successfully');
        ok($widget.wijaccordion("option", "selectedIndex") === -1, 'selectedIndex is invalid value');
        ok($('.wijmo-wijaccordion-content').eq(0).is(':hidden'), 'accordion pane with index 0 is collapsed');
        ok($('.wijmo-wijaccordion-content').eq(1).is(':hidden'), 'accordion pane with index 1 is collapsed');
        ok($('.wijmo-wijaccordion-content').eq(2).is(':hidden'), 'accordion pane with index 2 is collapsed');

        $widget.remove();
    });
})(jQuery);
