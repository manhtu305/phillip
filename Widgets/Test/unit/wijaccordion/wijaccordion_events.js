/*
 * wijaccordion_events.js
 */
(function ($) {

    module("wijaccordion: events");

    test("beforeSelectedIndexChanged and selectedIndexChanged", function () {
        var beforeselectedindexchangedCalled = false, selectedindexchangedCalled = false,
            $widget = create_wijaccordion({
                expanded: false,
                beforeSelectedIndexChanged: function (event, args) {
                    ok(event.type === "wijaccordionbeforeselectedindexchanged", "test event arguments' type");
                    ok(args !== undefined && typeof args === "object", "test args arguments");
                    ok(typeof args.newIndex === "number" && typeof args.prevIndex === "number", "test args arguments' properties");
                    if (args.newIndex == 1) {
                        event.preventDefault();
                    }
                    beforeselectedindexchangedCalled = true;
                },
                selectedIndexChanged: function (event, args) {
                    ok(event.type === "wijaccordionselectedindexchanged", "test event arguments' type");
                    ok(args !== undefined && typeof args === "object", "test args arguments");
                    ok(typeof args.newIndex === "number" && typeof args.prevIndex === "number", "test args arguments' properties");
                    selectedindexchangedCalled = true;
                },
                animated: false
            });

        $widget.wijaccordion('activate', 1);
        ok($widget.find('.wijmo-wijaccordion-content').eq(1).is(':hidden'), 'beforeSelectedIndexChanged cancelled by calling event.preventDefault for index 1');
        ok(!selectedindexchangedCalled, "beforeSelectedIndexChanged cancelled so selectedIndexChanged not fired");

        $widget.wijaccordion('activate', 2);
        ok($widget.find('.wijmo-wijaccordion-content').eq(2).is(':visible'), 'accordion pane with index 2 expanded using active method');
        ok(beforeselectedindexchangedCalled, "beforeSelectedIndexChanged fired");
        ok(selectedindexchangedCalled, "selectedIndexChanged fired");

        beforeselectedindexchangedCalled = false;
        selectedindexchangedCalled = false;
        $widget.wijaccordion({
            beforeSelectedIndexChanged: function () {
                beforeselectedindexchangedCalled = true;
                return false;
            }
        });
        $widget.wijaccordion('activate', 1);
        ok($widget.find('.wijmo-wijaccordion-content').eq(1).is(':hidden'), 'beforeSelectedIndexChanged cancelled by return false so index 1 have not expanded');
        ok($widget.find('.wijmo-wijaccordion-content').eq(2).is(':visible'), 'beforeSelectedIndexChanged cancelled by return false so index 2 still expanded');
        ok(!selectedindexchangedCalled, "beforeSelectedIndexChanged cancelled by return false in beforeSelectedIndexChanged");
        ok(beforeselectedindexchangedCalled, "beforeSelectedIndexChanged raised");
        $widget.remove();
    });

})(jQuery);
