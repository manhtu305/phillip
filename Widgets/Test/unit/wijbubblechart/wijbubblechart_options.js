﻿/*
* wijbubblechart_options.js
*/
"use strict";
(function ($) {
    module("wijbubblechart:options");

    test("chartLabelFormatter && chartLabelFormatString", function () {
        var bubbleChart = createBubblechart({
            seriesList: [{
                label: "China",
                legendEntry: true,
                data: { y: [1], x: [2], y1: [7] }
            }],
            showChartLabels: true,
            animation: {
                enabled: false
            }
        }), chartLabel0;

        chartLabel0 = bubbleChart.data("wijmo-wijbubblechart").chartElement.data("fields").bubbleInfos[0].dcl.attr("text");
        ok(chartLabel0 === "7", "the first chart element label is '7'");

        bubbleChart.wijbubblechart("option", "chartLabelFormatString", "n2");
        chartLabel0 = bubbleChart.data("wijmo-wijbubblechart").chartElement.data("fields").bubbleInfos[0].dcl.attr("text");
        ok(chartLabel0 === "7.00", "the first chart element label is updated according to chart label format string.");

        bubbleChart.wijbubblechart("option", "chartLabelFormatter", function () {
            return "mycustomzied:" + this.value;
        });
        chartLabel0 = bubbleChart.data("wijmo-wijbubblechart").chartElement.data("fields").bubbleInfos[0].dcl.attr("text");
        ok(chartLabel0 === "mycustomzied:7", "the first chart element label is updated according to chartLabelFormatter.");

        bubbleChart.remove();
    });
    
}(jQuery));