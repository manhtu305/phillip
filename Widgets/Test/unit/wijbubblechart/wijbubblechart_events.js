﻿/*globals start, stop, test, ok, module, jQuery, 
simpleOptions, createSimpleBubblechart, createbubblechart*/
/*
* wijbubblechart_events.js
*/
"use strict";
(function ($) {
	var bubblechart,
		newSeriesList = [{
			label: "1800",
			legendEntry: true,
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [100, 31, 635, 203], y1:[5, 10, 8, 7] }
		}, {
			label: "1900",
			legendEntry: true,
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [133, 156, 947, 408], y1:[3, 6, 1, 6] }
		}, {
			label: "2008",
			legendEntry: true,
			data: { x: ['AFRICA', 'AMERICA', 'ASIA', 'EUROPE'], y: [973, 914, 4054, 732], y1:[4, 2, 7, 10] }
		}];

	module("wijbubblechart: events");

	test("mousedown", function () {
		bubblechart = createSimpleBubblechart();
		bubblechart.wijbubblechart({
			mouseDown: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 0, 'Press the mouse down on the first bar.');
					bubblechart.remove();
				}
			}
		});
		stop();
		$(bubblechart.wijbubblechart("getBubble", 0).tracker.node).trigger("mousedown");		
	});

	test("mouseup", function () {
		bubblechart = createSimpleBubblechart();
		bubblechart.wijbubblechart({
			mouseUp: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 1, 'Release the mouse on the first bar.');
					bubblechart.remove();
				}
			}
		});
		stop();
		$(bubblechart.wijbubblechart("getBubble", 1).tracker.node).trigger('mouseup');
	});

	test("mouseover", function () {
		bubblechart = createSimpleBubblechart();
		bubblechart.wijbubblechart({
			mouseOver: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 1, 'Hover the first bar.');
					bubblechart.remove();
				}
			}
		});
		stop();
		$(bubblechart.wijbubblechart("getBubble", 1).tracker.node).trigger('mouseover');
	});

	test("mouseout", function () {
		bubblechart = createSimpleBubblechart();
		bubblechart.wijbubblechart({
			mouseOut: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 2, 'Move the mouse out of the first bar.');
					bubblechart.remove();
				}
			}
		});
		stop();
		$(bubblechart.wijbubblechart("getBubble", 2).tracker.node).trigger('mouseout');
	});

	test("mousemove", function () {
		bubblechart = createSimpleBubblechart();
		bubblechart.wijbubblechart({
			mouseMove: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 2, 'Move the mouse on the first bar.');
					bubblechart.remove();
				}
			}
		});
		stop();
		$(bubblechart.wijbubblechart("getBubble", 2).tracker.node).trigger('mousemove');
	});

	test("click", function () {
		bubblechart = createSimpleBubblechart();
		bubblechart.wijbubblechart({
			click: function (e, chartObj) {
				if (chartObj !== null) {
					start();
					ok(chartObj.index === 2, 'Click on the first bar.');
					bubblechart.remove();
				}
			}
		});
		stop();
		$(bubblechart.wijbubblechart("getBubble", 2).tracker.node).trigger('click');
	});

	test("beforeserieschange", function () {
		bubblechart = createSimpleBubblechart();
		var oldSeriesList = bubblechart.wijbubblechart("option", "seriesList");
		bubblechart.wijbubblechart({
			beforeSeriesChange: function (e, seriesObj) {
				if (seriesObj !== null) {
					start();
					var seriesList = bubblechart.wijbubblechart("option", "seriesList");
					ok(seriesObj.oldSeriesList === oldSeriesList,
							'get the correct old seriesList by seriesObj.oldSeriesList.');
					ok(seriesObj.newSeriesList === newSeriesList,
							'get the correct new seriesList by seriesObj.newSeriesList.');
					ok(seriesList === oldSeriesList,
							"currently seriesList hasn't been changed.");
					bubblechart.remove();
				}
			}
		});
		stop();
		bubblechart.wijbubblechart("option", "seriesList", newSeriesList);
	});

	test("serieschanged", function () {
		bubblechart = createSimpleBubblechart();
		bubblechart.wijbubblechart({
			seriesChanged: function (e, seriesObj) {
				if (seriesObj !== null) {
					start();
					var seriesList = bubblechart.wijbubblechart("option", "seriesList");
					ok(seriesList === newSeriesList,
						"currently seriesList has been changed.");
					bubblechart.remove();
				}
			}
		});
		stop();
		bubblechart.wijbubblechart("option", "seriesList", newSeriesList);
	});

	test("beforepaint, paint", function () {
		var options = $.extend({
			beforePaint: function (e) {
				if (e !== null) {
					ok(true, "The beforepaint event is invoked.");
					return false;
				}
			},
			painted: function (e) {
				ok(false, "The painted event of bar chart1 is invoked.");
			}
		}, true, simpleOptions),
			sector, options2, bubblechart2;
		bubblechart = createBubblechart(options);

		try {
			sector = bubblechart.wijbubblechart("getBubble", 1);
			ok(false, "The bar chart1 shouldn't be painted because of the cancellation.");
		}
		catch (ex) {
			ok(true, "The bar chart1 isn't painted because of the cancellation.");
		}
		bubblechart.remove();

		options2 = $.extend({
			beforePaint: function (e) {
				if (e !== null) {
					ok(true, "The beforepaint event is invoked.");
				}
			},
			painted: function (e) {
				ok(true, "The painted event of bar chart2 is invoked.");
			}
		}, true, simpleOptions);
		bubblechart2 = createBubblechart(options2);
		sector = bubblechart2.wijbubblechart("getBubble", 1);
		ok(sector !== null, 'The bar chart2 is painted.');
		bubblechart2.remove();
	});
} (jQuery));
