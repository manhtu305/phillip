/*globals test, ok, module, jQuery, createSimpleBarchart*/
/*
 * wijbarchart_methods.js
 */
"use strict";
(function ($) {

	module("wijbubblechart: methods");

	test("getBubble", function () {
		var bubblechart = createSimpleBubblechart(),
			sector = bubblechart.wijbubblechart('getBubble', 1);
		
		ok(sector.attrs !== null, 'getBubble:gets the second bar');
		bubblechart.remove();
	});

	test("getCanvas", function () {
		var bubblechart = createSimpleBubblechart(),
			canvas = bubblechart.wijbubblechart('getCanvas');

		ok(canvas.circle !== null, 'getCanvas:gets the canvas for the bar chart');
		bubblechart.remove();
	});

}(jQuery));
