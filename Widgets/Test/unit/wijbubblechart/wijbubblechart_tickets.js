﻿(function ($) {
    module("wijbubblechart: tickets");
    test("data are not display and javascript error is observed when binding bubble chart with DateTime value in X-axis or Y-axis.", function () {
        var bubblechart = createSimpleBubblechart(),
            seriesList = [];

        seriesList.push({ data: { x: [new Date("1/1/2010"), new Date("1/2/2010"), new Date("1/4/2010"), new Date("1/5/2010")], y: [5, 10, 12, 20], y1: [1, 3, 2, 5] } });
        bubblechart.wijbubblechart({
            seriesList: seriesList
        });
        ok(true, 'ok.');

        seriesList = [];
        seriesList.push({ data: { y: [new Date("1/1/2010"), new Date("1/2/2010"), new Date("1/4/2010"), new Date("1/5/2010")], x: [5, 10, 12, 20], y1: [1, 3, 2, 5] } });
        bubblechart.wijbubblechart({
            seriesList: seriesList
        });
        ok(true, 'ok.');
        bubblechart.remove();
    });

    test("adjuest axis issue when the axis-x series data is string array.", function () {
        var bubblechart = createSimpleBubblechart(),
            seriesList = [];

        seriesList.push({ data: { x: ["1/1/2010", "1/2/2010", "1/4/2010", "1/5/2010"], y: [5, 10, 12, 20], y1: [1, 3, 2, 5] } });
        bubblechart.wijbubblechart({
            seriesList: seriesList
        });
        ok(true, 'ok.');
        bubblechart.remove();
    });

    test("when the y1 values are same, the web page will throw a javascript exception.", function () {
        var bubblechart = createSimpleBubblechart(),
            seriesList = [];

        seriesList.push({ data: { x: ["1/1/2010", "1/2/2010", "1/4/2010", "1/5/2010"], y: [5, 10, 12, 20], y1: [1, 1, 1, 1] } });
        bubblechart.wijbubblechart({
            seriesList: seriesList
        });
        ok(true, 'ok.');
        bubblechart.remove();
    });

    test("data check", function () {
        var bubblechart = createSimpleBubblechart(),
            seriesList = [];

        seriesList.push({ data: { x: ["1/1/2010", "1/2/2010", "1/4/2010", "1/5/2010"], y: [5, 10, 12, 20], y1: [1, 1, 1, 1] } });
        bubblechart.wijbubblechart({
            seriesList: seriesList
        });
        ok(true, 'x string, y number, y1 number ok.');

        seriesList = [];
        seriesList.push({
            data: {
                x: ["1/1/2010", "1/2/2010", "1/4/2010", "1/5/2010"],
                y: [new Date("1/1/2010"), new Date("1/2/2010"), new Date("1/4/2010"), new Date("1/6/2010")], y1: [1, 1, 1, 1]
            }
        });
        bubblechart.wijbubblechart({
            seriesList: seriesList
        });
        ok(true, 'x string, y datetime, y1 number ok.');

        seriesList = [];
        seriesList.push({
            data: {
                x: [20, 25, 30, 15],
                y: [5, 10, 12, 20], y1: [1, 1, 1, 1]
            }
        });
        bubblechart.wijbubblechart({
            seriesList: seriesList
        });
        ok(true, 'x number, y number, y1 number ok.');

        seriesList = [];
        seriesList.push({
            data: {
                x: [20, 25, 30, 15],
                y: [new Date("1/1/2010"), new Date("1/2/2010"), new Date("1/4/2010"), new Date("1/6/2010")], y1: [1, 1, 1, 1]
            }
        });
        bubblechart.wijbubblechart({
            seriesList: seriesList
        });
        ok(true, 'x number, y datetime, y1 number ok.');

        seriesList = [];
        seriesList.push({
            data: {
                x: [new Date("1/1/2011"), new Date("1/12/2010"), new Date("1/5/2011"), new Date("1/11/2011")],
                y: [5, 10, 12, 20], y1: [1, 1, 1, 1]
            }
        });
        bubblechart.wijbubblechart({
            seriesList: seriesList
        });
        ok(true, 'x datetime, y number, y1 number ok.');

        seriesList = [];
        seriesList.push({
            data: {
                x: [new Date("1/1/2011"), new Date("1/12/2010"), new Date("1/5/2011"), new Date("1/11/2011")],
                y: [new Date("1/1/2010"), new Date("1/2/2010"), new Date("1/4/2010"), new Date("1/6/2010")], y1: [1, 1, 1, 1]
            }
        });
        bubblechart.wijbubblechart({
            seriesList: seriesList
        });
        ok(true, 'x datetime, y datetime, y1 number ok.');

        bubblechart.remove();
    });

    test("xy data check", function () {
        var bubblechart = createSimpleBubblechart(),
            seriesList = [];

        seriesList.push({ data: { xy: [1, 33, 2, 23, 3, 41, 4, 12], y1: [1, 1, 1, 1] } });
        bubblechart.wijbubblechart({
            seriesList: seriesList
        });
        ok(true, 'x string, y number, y1 number ok.');



        bubblechart.remove();
    });

    test("shadow don't rotation", function () {
        bubblechart = createBubblechart({
            hint: {
                enable: false
            },
            "seriesList": [
                {
                    "data": {
                        "y1": [5, 3, 7], "x": [60, 30, 80], "y": [60, 50, 40]
                    }, "markers": {
                        "symbol": [], "type": "invertedTri"
                    }, "invisibleMarkLabels": null, "label": "Series 1", "legendEntry": true
                }, {
                    "data": {
                        "y1": [7, 4, 6], "x": [70, 80, 30], "y": [30, 50, 90]
                    }, "markers": {
                        "symbol": [], "type": "diamond"
                    }, "invisibleMarkLabels": null, "label": "Series 2", "legendEntry": true
                }
            ], "seriesHoverStyles": [
            {
                "fill": "0-#CCCCFF-#CC33FF", "rotation": 50
            }, {
                "rotation": 30
            }
            ], "culture": "en-US", "footer": {
                "compass": "south", "visible": false
            }, "axis": {
                "x": {
                    "visible": true, "compass": "south", "gridMajor": {
                        "visible": true
                    }, "gridMinor": {
                        "visible": false
                    }
                }, "y": {
                    "visible": false, "labels": {
                        "textAlign": "center"
                    }, "compass": "west", "gridMajor": {
                        "visible": true
                    }, "gridMinor": {
                        "visible": false
                    }
                }
            }
        });
        bubblechart.wijbubblechart({
            mouseOver: function (e, chartObj) {
                if (chartObj !== null) {
                    start();
                    ok(chartObj.bubble.shadow.attr("rotation") === 50, 'shadow is rotated!');
                    bubblechart.remove();

                }
            }
        });
        stop();
        $(bubblechart.wijbubblechart("getBubble", 2).tracker.node).trigger('mouseover');

    });

    test("when set the rotate in seriesStyle, the bubble chart can't apply the styles", function () {
    	var opts = {
    		seriesList: [{
    			label: "West",
    			legendEntry: true,
    			data: { x: [1, 2, 3], y: [1, 1, 1.5], y1: [33.16667, 10, 25.33333] },
    			markers: {
    				type: "tri"
    			}
    		}, {
    			label: "East",
    			legendEntry: true,
    			data: { x: [1, 2, 3], y: [2, 2, 2], y1: [30.5, 5.833333, 13.16667] }

    		}],
    		hint: {
    			enable: false
    		},
    		seriesStyles: [{
    			rotation: 50
    		}],
    		maximumSize: 25
    	},
        bubblechart = createBubblechart(opts);
    	setTimeout(function () {
    		var bubble = bubblechart.wijbubblechart("getBubble", 0);
    		ok(Math.round(bubble.matrix.split().rotate) == 50, "The bubble element is rotated");
    		bubblechart.remove();
    		start();
    	}, 1000)
    	stop();
    });
    test("when tha y1 is not in range -999999999999 to 9999999999999, the radius of bubble will wrong", function () {
    	var opts = {
    		seriesList: [{
    			label: "West",
    			legendEntry: true,
    			data: { x: [1, 2, 3], y: [1, 1, 1.5], y1: [99999999999999, 50000, -9999999999999] },
    			markers: {
    				type: "tri"
    			}
    		}, {
    			label: "East",
    			legendEntry: true,
    			data: { x: [1, 2, 3], y: [2, 2, 2], y1: [100000000000000, 5.833333, -255555555] }

    		}],
    		hint: {
    			enable: false
    		}
    	},
        bubblechart = createBubblechart(opts),
		widget = bubblechart.data("wijmo-wijbubblechart");

    	ok(widget.bubbleAxisAdjust.bubMax === 100000000000000, "the max value is correct");
    	ok(widget.bubbleAxisAdjust.bubMin === -9999999999999, "the min value is correct");
    	bubblechart.remove();
    });

    test("when the page contains two bubble chart, the max value and min value is wrong", function () {
    	var opts = {
    		seriesList: [{
    			label: "West",
    			legendEntry: true,
    			data: { x: [1, 2, 3], y: [1, 1, 1.5], y1: [99999999999999, 50000, -9999999999999] },
    			markers: {
    				type: "tri"
    			}
    		}, {
    			label: "East",
    			legendEntry: true,
    			data: { x: [1, 2, 3], y: [2, 2, 2], y1: [100000000000000, 5.833333, -255555555] }

    		}],
    		hint: {
    			enable: false
    		}
    	},
		opt2 = {
			seriesList: [{
				label: "West",
				legendEntry: true,
				data: { x: [1, 2, 3], y: [1, 1, 1.5], y1: [12000, 50000, -25000] },
				markers: {
					type: "tri"
				}
			}, {
				label: "East",
				legendEntry: true,
				data: { x: [1, 2, 3], y: [2, 2, 2], y1: [65000, 5.833333, -255555555] }

			}],
			hint: {
				enable: false
			}
		},
		bubblechart, bubblechart2, widget1, widget2;
    	setTimeout(function () {
    		bubblechart = createBubblechart(opts);
    	}, 0);
    	setTimeout(function () {
    		bubblechart2 = createBubblechart(opt2);
    	}, 0);
    	setTimeout(function () {
    		widget1 = bubblechart.data("wijmo-wijbubblechart");
			widget2 = bubblechart2.data("wijmo-wijbubblechart");
			ok(widget1.bubbleAxisAdjust.bubMax === 100000000000000, "the max value is correct");
			ok(widget1.bubbleAxisAdjust.bubMin === -9999999999999, "the min value is correct");
			ok(widget2.bubbleAxisAdjust.bubMax === 65000, "the max value is correct");
			ok(widget2.bubbleAxisAdjust.bubMin === -255555555, "the min value is correct");
    		bubblechart.remove();
    		bubblechart2.remove();
    		start();
    	}, 100)
    	stop();
    });

    test("When min/max of axis option is set, autoMin/autoMax should be set to false", function () {
        var bubblechart = createBubblechart({
            axis: {
                x: {
                    max: 10,
                    min: 0
                },
                y: {
                    min: 0,
                    max: 30
                }
            },
            seriesList: [{ data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20], y1: [5, 10, 20, 30] } }]
        });

        var axis = bubblechart.wijbubblechart("option", "axis");
        ok(axis.x.autoMin === false && axis.x.min === 0, "autoMin of x axis is set to false when axis.x.min is set.");
        ok(axis.x.autoMax === false && axis.x.max === 10, "autoMax of x axis is set to false when axis.x.max is set.");
        ok(axis.y.autoMin === false && axis.y.min === 0, "autoMin of y axis is set to false when axis.y.min is set.");
        ok(axis.y.autoMax === false && axis.y.max === 30, "autoMax of y axis is set to false when axis.y.max is set.");
        bubblechart.remove();

		bubblechart = createBubblechart({
            seriesList: [{ data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20], y1: [5, 10, 20, 30] } }]
        });
        axis = bubblechart.wijbubblechart("option", "axis");
        ok(axis.x.autoMin === true, "autoMin of x axis is true.");
        ok(axis.x.autoMax === true, "autoMax of x axis is true.");
        ok(axis.y.autoMin === true, "autoMin of y axis is true.");
        ok(axis.y.autoMax === true, "autoMax of y axis is true.");
		bubblechart.wijbubblechart("option", "axis", {
            x: {
                max: 10,
                min: 0
            },
            y: {
                min: 0,
                max: 30
            }
        });
        axis = bubblechart.wijbubblechart("option", "axis");
        ok(axis.x.autoMin === false && axis.x.min === 0, "autoMin of x axis is set to false when axis.x.min is set.");
        ok(axis.x.autoMax === false && axis.x.max === 10, "autoMax of x axis is set to false when axis.x.max is set.");
        ok(axis.y.autoMin === false && axis.y.min === 0, "autoMin of y axis is set to false when axis.y.min is set.");
        ok(axis.y.autoMax === false && axis.y.max === 30, "autoMax of y axis is set to false when axis.y.max is set.");
        bubblechart.remove();
    });

    test("63015: When \"y1\" is null , the Chart UI (Axis, GridLines, Legend, Labels) is completely removed !", function () {
        var bubblechart = createBubblechart({
            seriesList: [{ data: { x: [1, 2, 3, 4], y: [5, 10, 12, 20] } }]
        });

        ok($("text", bubblechart).length > 0 && $("path", bubblechart).length > 0, "The coordinate has shown ! ");
        bubblechart.remove();
    });

    test("33265", function () {
        var bubblechart = createBubblechart({
            seriesList: [{ data: { x: [1, 2, 3], y: [5, 10, 12], y1: [20, 10, 30] } }],
            animation: {
                enabled: false
            }
        }), circle1, circle2, circle3, children, result;
        circle1 = bubblechart.wijbubblechart("getBubble", 0)[0];
        circle2 = bubblechart.wijbubblechart("getBubble", 1)[0];
        circle3 = bubblechart.wijbubblechart("getBubble", 2)[0];

        children = $(circle1.parentNode.childNodes);

        result = (children.index(circle3) < children.index(circle1)) &&
                 (children.index(circle1) < children.index(circle2));

        ok(result, "Bubbles are rendered in order !");
        bubblechart.remove();
    });

    test("#71878", function () {
        var seriesEles, trendlinePath,
            bubblechart = createBubblechart({
            seriesList: [{
                label: "Trendline",
                legendEntry: true,
                isTrendline: true,
                data: { x: [1, 2, 3], y: [5, 10, 12]}
            }],
            mouseOver: function (e, chartObj) {
                if (chartObj !== null) {
                    ok(chartObj.type === "trendLine", 'Hover on the trendline.');
                }
            },
            seriesStyles: [{
                stroke: "#FFA41C", "stroke-width": 1, "stroke-opacity": 0.7
            }],
            seriesHoverStyles: [{
                "stroke-width": 5
            }]
        });

        seriesEles = bubblechart.data("wijmo-wijbubblechart").chartElement.data("fields").seriesEles;
        ok(seriesEles.length === 1 && seriesEles[0].isTrendline, "The trendline is drawn.");
        trendlinePath = seriesEles[0].path;
        ok(trendlinePath.attr("stroke-width") === 1, "SeriesStyle is applied on trendline");
        $(trendlinePath.node).trigger('mouseover');
        ok(trendlinePath.attr("stroke-width") === 5, "HoverSeriesStyle is applied on trendline");
        $(trendlinePath.node).trigger('mouseout');
        ok(trendlinePath.attr("stroke-width") === 1, "HoverSeriesStyle is removed and seriesStyle is applied on trendline");
        bubblechart.remove();
    });

    test("#65400", function () {
        var bubbleChart = createBubblechart({
            seriesList: [{
                label: "China",
                legendEntry: true,
                data: { y: [73], x: [7931], y1: [1340] }
            }],
            animation: {
                enabled: false
            }
        }), tooltipBubbles;

        bubbleChart.wijbubblechart("option", "showChartLabels", false);
        tooltipBubbles = bubbleChart.data("wijmo-wijbubblechart").tooltipbubbles;
        ok(tooltipBubbles.length === 1, "Chart labels hide !");

        bubbleChart.wijbubblechart("option", "showChartLabels", true);
        tooltipBubbles = bubbleChart.data("wijmo-wijbubblechart").tooltipbubbles;
        ok(tooltipBubbles.length === 2, "Chart labels show !");

        bubbleChart.wijbubblechart("option", "chartLabel", { visible: false });
        tooltipBubbles = bubbleChart.data("wijmo-wijbubblechart").tooltipbubbles;
        ok(tooltipBubbles.length === 1, "Chart labels hide !");

        bubbleChart.wijbubblechart("option", "chartLabel", { visible: true });
        tooltipBubbles = bubbleChart.data("wijmo-wijbubblechart").tooltipbubbles;
        ok(tooltipBubbles.length === 2, "Chart labels show !");

        bubbleChart.remove();
    });

    test("#77157, test render bubble chart", function () {
    	var bubbleChart = createBubblechart({
    		axis: {
    			x: { text: "x" },
    			y: [{ text: "y", compass: "east" }, { text: "y2", compass: "west", max:120 }]
    		},
    		data: {
    			x: ["2012-Q1", "2012-Q2", "2012-Q3", "2012-Q4", "2013-Q1", "2013-Q2"]
    		},
    		seriesList: [{
    			label: "Q1",
    			legendEntry: true,
    			yAxis: 1,
    			data: {
    				y: [58.19, 69.68, 74.48, 70.82, 75.99, 79.40],
    				y1: [81.07, 104.8, 122.5, 144.7, 162.1, 177.9]

    			}
    		}]
    	});

    	ok(true, "The bubble chart of composite chart was created without errors, when the data series was added at the secondary axis");
    	bubbleChart.remove();
    });

    test("#46548", function () {
        var bubbleChart = createBubblechart({
            legend: {
                visible: true, 
                text: "Countries", 
                compass: "east", 
                orientation: "horizontal", 
                size: {
                    width: 100,
                    height: 10
                }
            },
            data: {
                x: ["Machines, engines"]
            },
            seriesList: [{
                label: "China", 
                legendEntry: true, 
                data: {
                    y: [10.48],
                    y1: [11.6]
                }
            }, {
                label: "Japan",
                legendEntry: true,
                data: {
                    y: [8],
                    y1: [5.6]
                }
            }]
        }), canvasWidth = bubbleChart.find("svg").width(),
            legendContainerWidth = bubbleChart.find("rect.wijchart-legend-container").attr("width");

        ok(canvasWidth / 2 >= parseInt(legendContainerWidth), "Legend has located in correct position.");
        bubbleChart.remove();
    });

    test("#92634", function () {
        var dataSource = [{ "XData": "Orange", "Y1Data": 10, "Y2Data": 20 }, ],
            dataSource2 = [],
            bubbleChart = createBubblechart({
                dataSource: dataSource,
                seriesList: [{
                    data: {
                        x: { bind: "XData" },
                        y: { bind: "Y1Data" },
                        y1: { bind: "Y2Data" }
                    }
                }]
            });

        bubbleChart.wijbubblechart("option", "dataSource", dataSource2);
        ok(true, "Without JS Error !");
        bubbleChart.remove();
    });

    test("#93316", function () {
        var titleMargin = 2,
            bubbleChart = createBubblechart({
                header: { text: "Header", visible: true, style: {fill: "#f1f1f1", stroke: "#010101"} }
            }),
            txtOffset = bubbleChart.find(".wijchart-header-text").offset(),
            contOffset = bubbleChart.find(".wijchart-header-container").offset(),
            samePosition = parseInt(txtOffset.left - (contOffset.left + titleMargin)) === 0 && parseInt(txtOffset.top - (contOffset.top + titleMargin)) === 0;

        ok(samePosition, "Header text and container locate in the same place. ");
        bubbleChart.remove();
    });
})(jQuery);

