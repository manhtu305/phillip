﻿/*globals test, ok, jQuery, module, document, $*/
/*
* wijbarchart_core.js create & destroy
*/
"use strict";
function createBubblechart(o) {
	return $('<div id="bubblechartchart" style = "width:400px;height:200px"></div>')
			.appendTo(document.body).wijbubblechart(o);
}

var simpleOptions = {
	seriesList: [{
                    label: "West",
                    legendEntry: true,
                    data: { x: [1,2,3], y: [1, 1, 1.5], y1: [33.16667,10,25.33333] },
                    markers: 'circle'
                },
                {
                    label: "East",
                    legendEntry: true,
                    data: { x: [1,2,3], y: [2, 2, 2], y1: [30.5, 5.833333, 13.16667] },
                    markers: 'circle'
                }
                ],
	hint: {
		enable: false
	},
	maximumSize:25
};

var seriesList1 = [
	{
		label: "West",
		legendEntry: true,
		data: { x: [30, 50, 80], y: [10, 40, 32], y1: [22, 5, 40] },
		markers: 'circle'
	},
    {
        label: "East",
        legendEntry: true,
        data: { x: [30, 50, 80], y: [15, 35, 20], y1: [11, 35, 20] },
        markers: 'circle'
    }
];

var seriesList2 = [
	{
		label: "West",
		legendEntry: true,
		data: { x: [1, 2, 4], y: [-10, 1, 22], y1: [5, 14, 10] },
		markers: 'circle'
	},
    {
        label: "East",
        legendEntry: true,
        data: { x: [1, 2, 4], y: [-15, 5, 17], y1: [12, 8, 6] },
        markers: 'circle'
    }
];

var seriesList3 = [
	{
		label: "West",
		legendEntry: true,
		data: { x: [2006, 2007, 2008, 2009, 2010], y: [1000, -200, 2200, 3200, 5000], y1: [12.5, 5, 2.5, 50, 20] },
		markers: 'circle'
	},
    {
        label: "East",
        legendEntry: true,
        data: { x: [2006, 2007, 2008, 2009, 2010], y: [2500, 1500, -400, 6000, 1050], y1: [37, 13, 29, 4, 64] },
        markers: 'circle'
    }
];


function createSimpleBubblechart() {
	return createBubblechart(simpleOptions);
}

(function ($) {

	module("wijbubblechart: core");

	test("create and destroy", function () {
		var bubblechart = createBubblechart(simpleOptions);

		ok(bubblechart.hasClass('ui-widget wijmo-wijbubblechart'),
			'create:element class created.');

		bubblechart.wijbubblechart('destroy');
		ok(!bubblechart.hasClass('ui-widget wijmo-wijbubblechart'),
			'destroy:element class destroy.');

		bubblechart.remove();
	});

	test("bubble auto axis", function () {
		var opt1 = $.extend({}, simpleOptions, {seriesList: seriesList1});
		var chart1 = createBubblechart(opt1);
		var axisY = chart1.wijbubblechart("option", "axis").y;
		var axisX = chart1.data("wijmo-wijbubblechart").axisInfo.x;
		ok(axisX.min <= 30 && axisX.min >= 20, "axis-x min OK");
		ok(axisX.max > 80 && axisY.max <= 90, "axis-x max OK");
		ok(axisY.min < 10 && axisY.min > 0, "axis-y min OK");
		ok(axisY.max > 40 && axisY.max < 50, "axis-y max OK");
		chart1.remove();

		opt1 = $.extend({}, simpleOptions, {seriesList: seriesList2});
		chart1 = createBubblechart(opt1);
		axisY = chart1.wijbubblechart("option", "axis").y;
		axisX = chart1.data("wijmo-wijbubblechart").axisInfo.x;
		ok(axisX.min < 1 && axisX.min > 0, "axis-x min OK");
		ok(axisX.max > 4 && axisX.max < 5, "axis-x max OK");
		ok(axisY.min < -15 && axisY.min >= -25, "axis-y min OK");
		ok(axisY.max > 22 && axisY.max <= 30, "axis-y max OK");
		chart1.remove();

		opt1 = $.extend({}, simpleOptions, {seriesList: seriesList3});
		chart1 = createBubblechart(opt1);
		axisY = chart1.wijbubblechart("option", "axis").y;
		axisX = chart1.data("wijmo-wijbubblechart").axisInfo.x;
		ok(axisX.min < 2006 && axisX.min > 2005, "axis-x min OK");
		ok(axisX.max > 2010 && axisX.max < 2011, "axis-x max OK");
		ok(axisY.min < -400 && axisY.min >= -1500, "axis-y min OK");
		ok(axisY.max > 6000 && axisY.max <= 7500, "axis-y max OK");
		chart1.remove();
	});

} (jQuery));

