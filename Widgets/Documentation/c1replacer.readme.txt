/*
* c1replacer widget. V1.0
*
* Copyright (c) Componentone Inc.
*
* Descripton:
*	c1replacer widget allows replacing the content of a container with another in animated effects.
*
* Depends:
* 	jquery-1.4.2.js
*	jquery.ui.core.js
*	jquery.ui.widget.js
*
* Optional dependence for effect settings:
*	jquery.effects.core.js	
*	jquery.effects.blind.js
*	jquery.effects.bounce.js
*	jquery.effects.clip.js
*	jquery.effects.drop.js
*	jquery.effects.explode.js
*	jquery.effects.fold.js
*	jquery.effects.highlight.js
*	jquery.effects.pulsate.js
*	jquery.effects.scale.js
*	jquery.effects.shake.js
*	jquery.effects.slide.js
*	jquery.effects.transfer.js
*
* Makeup:
*   <div style="width:xxx;height:xxx">content goes here</div>
*
*/



* Options:

	effect: 'auto'
	// The animation effects, Possible values are: 
	//	none, auto, fade, slideLeft, slideRight, slideTop, slideBottom, slideLeftRight, slideRightLeft
	//	slideTopBottom, slideBottomTop, transferIn, transferOut
	
    duration: 500
	// Teh animation duration
	
	autoWrap: true
	// Determines whether wrapping a new div to the new content automatically.
	
	autoDelete: true
	// Determines whether deleting the old content automatically.
	
	activeContent: undefined
	// The current displayed content.
	
	transferBounds: undefined
	// The bounds to transfer from/to when the effect is transferOut or transferIn
	
	completed:
	// A callback function that will be invoked when the animation is completed.

* Events:
	completed
	// Occurs when the replacing animation is completed.

* Methods:
	replace(content)
	// Replaces the current content with the new content, with specified animation effects.
	
	isReplacing()
	// Determines whether the replacing animation is going on.
	
