* Title
	wijdatasource class

* Description
	A class to provlide tabular data for wijwidgets.
	It is used along with a datareader and an optional proxy.
	Two sample reader and proxy class is in jquery.ui.wijdatasource.js.
 * Depends:


*Options: 
data
// The data to process by wijdatasource.
// Type: Object
// Default: {}
reader
// The reader to use with wijdatasource.
// wijdatasource will call the read method of reader to read from rawdata with an array of fields provided.
// The field contains a name, mapping and defaultValue properties which define the rule of the mapping.
// If no reader is configured with wijdatasource it will directly return the raw data.
// Type: Object
// Default: null
proxy
// The proxy to use with wijdatasource.
// wijdatasource will call the request method of proxy object. 
// In the proxy object, a request could be sent to remote server to obtain data with the ajaxs options object provided.
// Then in the call the reader of wijdatasource to process the raw data.
// Type: Object
// Default: null

*Events
loading
// Function called before loading process starts
// Default: null.
// Type: Function. 
// <param name="datasource" type="wijdatasource">
// wijdatasource object that raises this event.
// </param>
// <param name="data" type="Object">
// data passed in by load method.
// </param>
loaded
// Function called after loading.
// Default: null.
// Type: Function. 
// <param name="datasource" type="wijdatasource">
// wijdatasource object that raises this event.
// </param>
// <param name="data" type="Object">
// data passed in by load method.
// </param>

*Methods
load
// The method to indicate wijdatasource to load data.
// The loading event will be triggered before loading while the loaded event will be triggered after loading.
// <param name="data" type="Object">
// The data to pass to the loading and loaded event handler.
// </param>
// <param name="forceLocalReload" type="Boolean">
// Normally local data is only load for one time,
// if needs to reload the data, try to set forceLocalReload to true.
// </param>

*Properties
items
// The processed items from raw data.  Could be obtained after calling load method.