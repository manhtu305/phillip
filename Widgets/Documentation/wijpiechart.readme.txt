* Title:
	wijpiechart widget.

* Description
	wijpiechart widget allows the user to show customized pie charts.

* Depends:
	raphael.js
	globalize.min.js
	jquery.ui.widget.js
	jquery.wijmo.wijchartcore.js

* HTML Markup
	<div id="piechart">
	</div>
	or
	<table id = "piechart">
		<thead>
			<td></td>
			<th>x1</th>
			<th>x2</th>
		</thead>
		<tbody>
			<tr>
				<th scope="row">seriesName1</th>
				<td>y1</td>
				<td>y2</td>
			</tr>
			<tr>
				<th scope="row">seriesName2</th>
				<td>y1</td>
				<td>y2</td>
			</tr>
		</tbody>
	</table>

* Options:
	radius: null
	// A value indicates the radius used for pie chart.
	// Type: Number.
	// Default: null.
	// Note: If the value is null, then the radius will be calculated by width/height value of pie chart.
	
	innerRadius
	// A value indicates the inner radius used for doughnut charts.
	// Type: Number.
	// Default: null
	
	animation
	/// <summary>
	/// A value that indicates whether to show animation and the duration for the animation.
	/// Default: {enabled:true, duration:400, easing: "easeOutExpo"}.
	/// Type: Object.
	/// Code example:
	///  $("#barchart").wijbarchart({
	///      stacked: true
	///  });
	/// </summary>
	{
		enabled: true,
		/// <summary>
		/// A value that determines whether to show animation.
		/// Default: true.
		/// Type: Boolean.
		/// </summary>

		duration: 400,
		/// <summary>
		/// A value that indicates the duration for the animation.
		/// Default: 400.
		/// Type: Number.
		/// </summary>

		easing: "elastic",
		/// <summary>
		/// A value that indicates the easing for the animation.
		/// Default: "elastic".
		/// Type: string.
		/// </summary>
		
		offset: 10
		/// <summary>
		/// A value that indicates the offset for explode animation.
		/// Default: 10.
		/// Type: Number.
		/// </summary>
	}

	width: null
	// A value indicates the width of wijchart.
	// Type: Number.
	// Default: null.	
	
	height: null
	// A value indicates the height of wijchart.
	// Type: Number.
	// Default: null.
	
	seriesList: []
	// An array collection contains the data to be charted.
	// Type: Array.
	// Default: [].
	//	Code example: seriesList: [{
	//                 label: "Q1",
	//                 legendEntry: true,
	//                 style: { fill: "rgb(255,0,0)", stroke:"none"},
	//                 data: {
	//						x: [1, 2, 3, 4, 5],
	//						y: [12, 21, 9, 29, 30]
	//					},
	//                 offset: 0
	//             }, {
	//         		label: "Q2",
	//            		legendEntry: true,
	//            		style: { fill: "rgb(255,125,0)", stroke: "none" },
	//            		data: {
	//						xy: [1, 21, 2, 10, 3, 19, 4, 31, 5, 20]
	//					},
	//            		offset: 0
	//        		}]
	//				OR
	//				seriesList: [{
	//         		label: "Q1",
	//            		legendEntry: true,
	//            		style: { fill: "rgb(255,125,0)", stroke: "none" },
	//            		data: {
	//						x: ["A", "B", "C", "D", "E"],
	//						y: [12, 21, 9, 29, 30]
	//					},
	//            		offset: 0
	//        		}]
	//				OR
	//				seriesList: [{
	//         		label: "Q1",
	//            		legendEntry: true,
	//            		style: { fill: "rgb(255,125,0)", stroke: "none" },
	//            		data: {
	//						x: [new Date(1978, 0, 1), new Date(1980, 0, 1), new Date(1981, 0, 1), new Date(1982, 0, 1), new Date(1983, 0, 1)],
	//						y: [12, 21, 9, 29, 30]
	//					},
	//            		offset: 0
	//        		}]

	seriesStyles: [{stroke: "#77b3af", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#67908e", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#465d6e", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#5d3f51", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#682e32", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#8c5151", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#ce9262", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#ceb664", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#7fb34f", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#2a7b5f", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#6079cb", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#60a0cb", opacity: 0.9, "stroke-width": "1"}],
	/// An array collection that contains the style to be charted.
	// Type: Array.
	// Default: [{stroke: "#77b3af", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#67908e", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#465d6e", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#5d3f51", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#682e32", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#8c5151", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#ce9262", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#ceb664", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#7fb34f", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#2a7b5f", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#6079cb", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#60a0cb", opacity: 0.9, "stroke-width": "1"}].
	
	seriesHoverStyles: [{opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}],
	// An array collection that contains the style to be charted when hovering the chart element.
	// Type: Array.
	// Default: [{opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}].
	
	marginTop: 25
	// A value indicates the top margin of chart area.
	// Type: Number.
	// Default: 25.
	
	marginRight: 25
	// A value indicates the right margin of chart area.
	// Type: Number.
	// Default: 25.

	marginBottom: 25
	// A value indicates the bottom margin of chart area.
	// Type: Number.
	// Default: 25.

	marginLeft: 25
	// A value indicates the left margin of chart area.
	// Type: Number.
	// Default: 25.
	
	textStyle: {fill: "#888","font-size": "10pt",stroke: "none"},
	// A value that indicates the style of the chart text.
	// Type: Object.
	// Default: {fill:"#888", "font-size": "10pt", stroke:"none"}.

	header:	
	// An object value indicates the header of the chart element.
	// Type: Object.
	// Default: {visible:true, style:{fill:"none", stroke:"none"}, textStyle:{"font-size": "18pt", fill:"#666", stroke:"none"}, compass:"north", orientation:"horizontal"} 
	{
		text: "",
		// A value indicates the text of header.
		// Type: String.
		// Default: "".

		style: {
			fill: "none",
			stroke: "none"
		},
		// A value indicates the style of header.
		// Type: Object.
		// Default: {fill:"none", stroke:"none"}.

		textStyle: {
			"font-size": "18pt",
			fill: "#666",
			stroke: "none"
		},
		// A value indicates the style of header text.
		// Type: Object.
		// Default: {"font-size": "18pt", fill:"#666", stroke:"none"}.
		
		compass: "north",
		// A value indicates the compass of header.
		// Type: String.
		// Default: "north".
		// Notes: options are 'north', 'south', 'east' and 'west'.		
		
		orientation: "horizontal",
		// A value indicates the orientation of header.
		// Type: String.
		// Default: "horizontal".
		// Notes: options are 'horizontal' and 'vertical'.
		
		visible: true
		// A value indicates the visibility of header.
		// Type: Boolean.
		// Default: true.
	}
	
	footer: 
	// An object value indicates the footer of the chart element.
	// Type: Object.
	// Default: {visible:false, style:{fill:"#fff", stroke:"none"}, textStyle:{fille:"#000", stroke:"none"}, compass:"south", orientation:"horizontal"}
	{
		text: "",
		// A value indicates the text of footer.
		// Type: String.
		// Default: "".

		style: {
			fill: "#fff",
			stroke: "none"
		},
		// A value indicates the style of footer.
		// Type: Object.
		// Default: {fill:"#fff", stroke:"none"}.

		textStyle: {
			fill: "#000",
			stroke: "none"
		},
		// A value indicates the style of footer text.
		// Type: Object.
		// Default: {fill:"#000", stroke:"none"}.
		
		compass: "south",
		// A value indicates the compass of footer.
		// Type: String.
		// Default: "south".
		// Notes: options are 'north', 'south', 'east' and 'west'.		
		
		orientation: "horizontal",
		// A value indicates the orientation of footer.
		// Type: String.
		// Default: "horizontal".
		// Notes: options are 'horizontal' and 'vertical'.
		
		visible: false
		// A value indicates the visibility of footer.
		// Type: Boolean.
		// Default: false.
	},
	
	legend: 
	// An object value indicates the legend of the chart element.
	// Type: Object.
	// Default: {text:"", textMargin:{left:2,top:2,right:2,bottom:2},titleStyle:{"font-weight":"bold",fill:"#000",stroke:"none},
	//			visible:true, style:{fill:"#none", stroke:"none"}, textStyle:{fille:"#333", stroke:"none"}, compass:"east", orientation:"vertical"}
	{
		text: "",
		// A value indicates the text of legend.
		// Type: String.
		// Default: "".
		
		textMargin: { left: 2, top: 2, right: 2, bottom: 2 },
		// A value that indicates the text margin of the legend item.
		// Type: Number.
		// Default: {left:2, top:2, right:2, bottom:2}.

		style: {
			fill: "none",
			stroke: "none"
		},
		// A value indicates the style of legend.
		// Type: Object.
		// Default: {fill:"none", stroke:"none"}.

		textStyle: {
			fill: "#333",
			stroke: "none"
		},
		// A value indicates the style of legend text.
		// Type: Object.
		// Default: {fill:"#333", stroke:"none"}.
		
		titleStyle: {"font-weight": "bold",fill: "#000",stroke: "none"},
		// A value that indicates the style of the legend title.
		// Default: {"font-weight": "bold", fill:"#000", stroke:"none"}.
		// Type: Object.
		
		compass: "east",
		// A value indicates the compass of legend.
		// Type: String.
		// Default: "east".
		// Notes: options are 'north', 'south', 'east' and 'west'.		
		
		orientation: "vertical",
		// A value indicates the orientation of legend.
		// Type: String.
		// Default: "vertical".
		// Notes: options are 'horizontal' and 'vertical'.
		
		visible: true
		// A value indicates the visibility of legend.
		// Type: Boolean.
		// Default: true.
	},
		
	hint: 
	// A value is used to indicate whether and what to show on the popped tooltip.
	// Default: {enable:true, content:null, 
	//			contentStyle: {fill: "#d1d1d1","font-size": "16pt"},
	//			title:null, 
	//			titleStyle: {fill: "#d1d1d1","font-size": "16pt"},
	//			style: {fill: "270-#333333-#000000", "stroke-width": "2"},
	//			animated: "fade", showAnimated: "fade", hideAnimated: "fade",
	//			duration: 120, showDuration: 120, hideDuration: 120,
	//			showDelay: 150, hideDelay: 150, easing: "easeOutExpo", 
	//			showEasing: "easeOutExpo", hideEasing: "easeOutExpo",
	//			compass:"north", offsetX: 0, offsetY: 0,  
	//			showCallout: true, calloutFilled: false, 
	//			calloutFilledStyle: {fill: "#000"}}.
	// Type: Function.
	{
		enable: true,
		// A value indicates whether to show the tooltip.
		// Default: true.
		// Type: Boolean.
		
		content: null
		// A value that will be shown in the content part of the tooltip 
		//	or a function which is used to get a value for the tooltip shown.
		// Default: "".
		// Type: String or Function.
		
		contentStyle: {
			fill: "#d1d1d1",
			"font-size": "16pt"
		},
		// A value that indicates the style of content text.
		// Default: {fill: "#d1d1d1","font-size": "16pt"}.
		// Type: Object.			

		title: null,
		// A value that will be shown in the title part of the tooltip 
		//	or a function which is used to get a value for the tooltip shown.
		// Default: null.
		// Type: String or Function.
		
		titleStyle: {
			fill: "#d1d1d1",
			"font-size": "16pt"
		},
		// A value that indicates the style of title text.
		// Default: {fill: "#d1d1d1","font-size": "16pt"}.
		// Type: Object.
		
		style: {
			fill: "270-#333333-#000000",
			"stroke-width": "2"
		},
		// A value that indicates the style of container.
		// Default: {fill: "270-#333333-#000000", "stroke-width": "2"}.
		// Type: Object.

		animated: "fade",
		// A value that indicates the effect during show or hide 
		//	when showAnimated or hideAnimated isn't specified.
		// Default:"fade".
		// Type:String.
		
		showAnimated: "fade",
		// A value that indicates the effect during show.
		// Default:"fade".
		// Type:String.
		
		hideAnimated: "fade",
		// A value that indicates the effect during hide.
		// Default:"fade".
		// Type:String.
		
		duration: 120,
		// A value that indicates the millisecond to show or hide the tooltip
		//	when showDuration or hideDuration isn't specified.
		// Default:120.
		// Type:Number.
		
		showDuration: 120,
		// A value that indicates the millisecond to show the tooltip.
		// Default:120.
		// Type:Number.
		
		hideDuration: 120,
		// A value that indicates the millisecond to hide the tooltip.
		// Default:120.
		// Type:Number.
		
		easing: "easeOutExpo", 
		// A value that indicates the easing during show or hide when
		//	showEasing or hideEasing isn't specified. 
		// Default: "easeOutExpo".
		// Type: String.
		
		showEasing: "easeOutExpo", 
		// A value that indicates the easing during show. 
		// Default: "easeOutExpo".
		// Type: String.
		
		hideEasing: "easeOutExpo",
		// A value that indicates the easing during hide. 
		// Default: "easeOutExpo".
		// Type: String.
		
		showDelay: 150,
		// A value that indicates the millisecond delay to show the tooltip.
		// Default: 150.
		// Type: Number.
		
		hideDelay: 150,		
		// A value that indicates the millisecond delay to hide the tooltip.
		// Default: 150.
		// Type: Number.
		
		compass: "north",
		// A value that indicates the compass of the tooltip.
		// Default: "north".
		// Type: String.
		// Notes: options are 'west', 'east', 'south', 'north', 'southeast', 'southwest', 'northeast', 'northwest'.

		offsetX: 0,
		// A value that indicates the horizontal offset 
		//	of the point to show the tooltip.
		// Default: 0.
		// Type: Number.
		
		offsetY: 0,
		// A value that indicates the vertical offset 
		//	of the point to show the tooltip.
		// Default: 0.
		// Type: Number.
		
		showCallout: true,
		// Determines whether to show the callout element.
		// Default:true.
		// Type:Boolean.
		
		calloutFilled: false,
		// Determines whether to fill the callout.  
		//	If true, then the callout triangle will be filled.
		// Default:false.
		// Type:Boolean.
		
		calloutFilledStyle: {
			fill: "#000"
		}
		// A value that indicates the style of the callout filled.
		// Default: {fill: "#000"}.
		// Type: Object.		
	},
	
	showChartLabels: true,
	// A value indicates whether to show default chart labels.
	// Default: true.
	// Type: Boolean.
	
	chartLabelStyle: {},
	// A value that indicates style of the chart labels.
	// Default: {}.
	// Type: Object.

	disableDefaultTextStyle: false,
	// A value that indicates whether to disable the default text style.
	// Default: false.
	// Type: Boolean.

	shadow: true,
	// A value that indicates whether to show shadow for the chart.
	// Default: false.
	// Type: Boolean.

* Methods:
	getCanvas()
	// Returns the reference to raphael canvas object.
	
	getSector(index)
	// Returns the sector of pie chart according to given index.
			
	redraw(drawIfNeeded)
	// Redraw the chart.
	// <param name="drawIfNeeded" type="Boolean">
	// A value that indicates whether to redraw the chart 
	//	no matter whether the chart is painted.
	// If true, then only when the chart is not created before, 
	// it will be redrawn.  Otherwise, the chart will be forced to redraw.  
	//	The default value is false.
	// </param>

* Events:	
	mousedown: null,
	// Occurs when the user clicks a mouse button.
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#piechart").wijpiechart({mousedown: function(e, data) { } });
	// Bind to the event by type: wijpiechartmousedown
	// $("#piechart").bind("wijpiechartmousedown", function(e, data) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>
	// <param name="data" type="Object">
	// An object that contains all the series infos of the mousedown sector. 
	// data.data: value of the sector.
	// data.index: index of the sector.
	// data.label: label of the sector.
	// data.legendEntry: legend entry of the sector.
	// data.offset: offset of the sector.
	// data.style: style of the sector.
	// type: "pie"
	//	</param>

	mouseup: null,
	// Occurs when the user releases a mouse button while the pointer is over the chart element.
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#piechart").wijpiechart({mouseup: function(e, data) { } });
	// Bind to the event by type: wijpiechartmouseup
	// $("#piechart").bind("wijpiechartmouseup", function(e, data) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>
	// <param name="data" type="Object">
	// An object that contains all the series infos of the mouseup sector. 
	// data.data: value of the sector.
	// data.index: index of the sector.
	// data.label: label of the sector.
	// data.legendEntry: legend entry of the sector.
	// data.offset: offset of the sector.
	// data.style: style of the sector.
	// type: "pie"
	//	</param>

	mouseover: null,
	// Occurs when the user first places the pointer over the chart element.
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#piechart").wijpiechart({mouseover: function(e, data) { } });
	// Bind to the event by type: wijpiechartmouseover
	// $("#piechart").bind("wijpiechartmouseover", function(e, data) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>
	// <param name="data" type="Object">
	// An object that contains all the series infos of the mouseover sector. 
	// data.data: value of the sector.
	// data.index: index of the sector.
	// data.label: label of the sector.
	// data.legendEntry: legend entry of the sector.
	// data.offset: offset of the sector.
	// data.style: style of the sector.
	// type: "pie"
	//	</param>

	mouseout: null,
	// Occurs when the user moves the pointer off of the chart element.
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#piechart").wijpiechart({mouseout: function(e, data) { } });
	// Bind to the event by type: wijpiechartmouseout
	// $("#piechart").bind("wijpiechartmouseout", function(e, data) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>
	// <param name="data" type="Object">
	// An object that contains all the series infos of the mouseout sector. 
	// data.data: value of the sector.
	// data.index: index of the sector.
	// data.label: label of the sector.
	// data.legendEntry: legend entry of the sector.
	// data.offset: offset of the sector.
	// data.style: style of the sector.
	// type: "pie"
	//	</param>

	mousemove: null,
	// Occurs when the user moves the mouse pointer
	// while it is over a chart element.
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#piechart").wijpiechart({mousemove: function(e, data) { } });
	// Bind to the event by type: wijpiechartmousemove
	// $("#piechart").bind("wijpiechartmousemove", function(e, data) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>
	// <param name="data" type="Object">
	// An object that contains all the series infos of the mousemove sector. 
	// data.data: value of the sector.
	// data.index: index of the sector.
	// data.label: label of the sector.
	// data.legendEntry: legend entry of the sector.
	// data.offset: offset of the sector.
	// data.style: style of the sector.
	// type: "pie"
	//	</param>

	click: null,
	// Occurs when the user clicks the chart element. 
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#piechart").wijpiechart({click: function(e, data) { } });
	// Bind to the event by type: wijpiechartclick
	// $("#piechart").bind("wijpiechartclick", function(e, data) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>
	// <param name="data" type="Object">
	// An object that contains all the series infos of the clicked sector. 
	// data.data: value of the sector.
	// data.index: index of the sector.
	// data.label: label of the sector.
	// data.legendEntry: legend entry of the sector.
	// data.offset: offset of the sector.
	// data.style: style of the sector.
	// type: "pie"
	//	</param>

	beforeserieschange: null,
	// Occurs before the series changes.  This event can be cancelled. 
	// "return false;" to cancel the event.
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#piechart").wijpiechart({beforeserieschange: function(e, data) { } });
	// Bind to the event by type: wijpiechartbeforeserieschange
	// $("#piechart").bind("wijpiechartbeforeserieschange", function(e, data) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>
	// <param name="data" type="Object">
	// An object that contains old and new series values.
	// data.oldSeriesList: old series list before change.
	//	data.newSeriesList: new series list that will replace old one.  
	//	</param>

	serieschanged: null,
	// Occurs when the series changes. 
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#piechart").wijpiechart({serieschanged: function(e, data) { } });
	// Bind to the event by type: wijpiechartserieschanged
	// $("#piechart").bind("wijpiechartserieschanged", function(e, data) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>
	// <param name="data" type="Object">
	// An object that contains new series values.  
	//	</param>

	beforepaint: null,
	// Occurs before the canvas is painted.  This event can be cancelled.
	// "return false;" to cancel the event.
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#piechart").wijpiechart({beforepaint: function(e) { } });
	// Bind to the event by type: wijpiechartbeforepaint
	// $("#piechart").bind("wijpiechartbeforepaint", function(e) { } );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>

	painted: null
	// Occurs after the canvas is painted. 
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#piechart").wijpiechart({painted: function(e) { } });
	// Bind to the event by type: wijpiechartpainted
	// $("#piechart").bind("wijpiechartpainted", function(e) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>

