* Title
	wijlist widget

* Description
	wijlist is a list widget could be used by other widget. 

 * Depends:
 *	jquery.ui.core.js
 *	jquery.ui.widget.js
 *	jquery.ui.wijdatasource.js
 *	jquery.ui.wijsuperpanel.js
 * HTML Markup
 <div><ul></ul></div>

Options:
selectionMode
// Selection mode of wijList.  Options are 'single' and 'multiple'.
// Type: String
// Default: 'single'
///	<summary>
///		Whether to autosize wijList.
///	</summary>
autoSize: false,
// Whether to autosize wijList.
// Type: Boolean
// Default: false
maxItemsCount: 5,
// Max item count to display if autoSize if true.
// Type: Number
// Default: 5
dataoptions: null,
// Options of wijdatasource.
// Type: Object
// Default: null
addHoverItemClass: true
// Whether to add ui-state-hover class to list item when mouse enters.
// Type: Boolan
// Default: true

Events: 
resized
// Fired when list is resized.
selected
// Fired when list item is selected.
listloading
// Fired when list item is loading data.
listloaded
// Fired when list item is loaded with data.
focusing
// Fired when mouse enters the item and before any logic in hover event is processed.
focus
// Fired when mouse enters the item and after logic in hover event is processed.
blur
// Fired when mouse leaves the item.
itemrendering
// Fired when before list item is rendered.
itemrendered
// Fired when after list item is rendered.

Methods:
activate(event, item)
// Activate a item in the list.
deactivate
// Deactivate the activated item in the list.
next
// move focus to next item. 
nextPage
// Ture to next Page of list.
previous
// move focus to the previous item. 
previousPage
// Ture to previous Page of list.
first
// Tests the focus is at the first item.
last
// Tests the focus is at the last item.
selectItems
// selectItems by items indices.
unselectItems
// unselectItems by items indices.
renderList
// render the list.
refreshSuperPanel
// refreshes the superpanel around wijlist.