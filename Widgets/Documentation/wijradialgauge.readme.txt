﻿* Title:
	wijradialgauge widget.

* Description
	wijradialgauge widget allows the user to show customized radial gauge..

* Depends:
	raphael.js
	globalize.min.js
	jquery.ui.widget.js
	jquery.wijmo.wijchartcore.js
	jquery.wijmo.wijgauge.js

* HTML Markup
	<div id="barchart">
	</div>

* Options:
	value
	// The value of the gauge.
	// Type: Number
	// Default: 5.
	// Code example: $("#selector").wijradialgauge("option", "value", 20)

	max
	// The max value of the gauge.
	// Type: Number
	// Default: 100.
	// Code example: $("#selector").wijradialgauge("option", "max", 150)
	
	min
	// The min value of the gauge.
	// Type: Number
	// Default: 0.
	// Code example: $("#selector").wijradialgauge("option", "min", 10)

	radius
	// A value that indicates the radius of the radial gauge.
	// Type: Number.
	// Default: "auto".
	// Code example: $("#selector").wijradialgauge("option", "radius", 170).
	
	startAngle
	// A value that indicates the start angle of the radial gauge.
	// Type: Number
	// Default: 0
	// Code example: $("#selector").wijradialgauge("option", "startAngle", -20).

	sweepAngle
	// A value that indicates the sweep angle of the radial gauge.
	// Type: Number.
	// Default: 180.
	// Code example: $("#selector").wijradialgauge("option", "sweepAngle", 200).

	origin
	// A point that indicates the origin of the radial gauge.
	// Type: Object
	// Default: { x: 0.5, y: 0.5 }.
	// Code example: $("#selector").wijradialgauge("option", "origin", { x: 0.5, y: 0.6 }).

	width
	// The width of the gauge.
	// Type: Number.
	// Default: 600.
	// Code example: $("#selector").wijradialgauge("option", "width", 500)

	height
	// The height of the gauge.
	// Type: Number.
	// Default: 600.
	// Code example: $("#selector").wijradialgauge("option", "height", 350)

	tickMajor
	// A value that provides information for the major tick.
	// Type: Object.
	// Default: {position: "inside", style: { fill: "#1E395B", stroke:"none"
	// }, factor: 2, visible: true, marker: "rect", offset: 27, interval: 10}.
	// Note: Sub properties
	// position
	//  // A value that indicates the type of major tick mark.
	//  // Type: "String"
	//  // Default: "inside".
	//  // remarks: Options are 'inside', 'outside' and 'cross'.
	// style
	//	// A value that indicates the style of major tick mark.
	//	// Type: Object.
	//	// Default: {fill: "#1E395B", stroke:"none"}.
	// factor
	//	// A value that indicates an integral factor for major tick mark length.
	//	// Type: Number.
	//	// Default: 2.
	// visible
	//	// A value that indicates whether show the major tick.
	//	// Type: Boolean.
	//	// Default: true.
	// marker
	//	// A value that indicates which marker shape the tick is painted.
	//	// Type: "String"
	//	// Default: "rect".
	//	// remarks: Options are 'rect', 'tri', 'circle', 'invertedTri', 'box', 'cross', 'diamond'.
	// offset
	//	// A value that indicates where to paint the tick on the gauge.
	//	// Type: Number.
	//	// Default: 27.
	// interval
	//	// A value that indicates how much value between two nearest major ticks.
	//	// Type: Number
	//	// Default: 10.
	// code example: $("#selector").wijradialgauge("option", "tickMajor", {interval: 15}).

	tickMinor
	// A value that provides information for the minor tick.
	// Type: Object.
	// Default: {position: "inside", style: { fill: "#1E395B", stroke:"none"
	// }, factor: 1, visible: true, marker: "rect", offset: 30, interval: 5}
	// Note: Sub properties
	// position
	//  // A value that indicates the type of minor tick mark.
	//  // Type: "String"
	//  // Default: "inside".
	//  // remarks: Options are 'inside', 'outside' and 'cross'.
	// style
	//	// A value that indicates the style of minor tick mark.
	//	// Type: Object.
	//	// Default: {fill: "#1E395B", "stroke": "none"}.
	// factor
	//	// A value that indicates an integral factor for minor tick mark length.
	//	// Type: Number.
	//	// Default: 1.
	// visible
	//	// A value that indicates whether show the minor tick.
	//	// Type: Boolean.
	//	// Default: true.
	// marker
	//	// A value that indicates which marker shape the tick is painted.
	//	// Type: "String"
	//	// Default: "rect".
	//	// remarks: Options are 'rect', 'tri', 'circle', 'invertedTri', 'box', 'cross', 'diamond'.
	// offset
	//	// A value that indicates where to paint the tick on the gauge.
	//	// Type: Number.
	//	// Default: 30.
	// interval
	//	// A value that indicates how much value between two nearest minor ticks.
	//	// Type: Number
	//	// Default: 5.
	// code example: $("#selector").wijradialgauge("option", "tickMajor", {interval: 2}).

	pointer
	// A value that includes all settings of the gauge pointer.
	// Type: Object
	// Default: {length: 0.8, style: { fill: "#1E395B", stroke: "#1E395B"},
	// width: 8, offset: 0.15, shape: "tri", visible: true, template: null}.
	// Note: Sub properties
	// length
	//	// An percent of the gauge's width / height.
	//	// Type：Number.
	//	// Default: 0.8.
	// style
	//	// The style of the pointer.
	//	// Type: Object.
	//	// Default: {fill: "#1E395B", stroke: "#1E395B"}.
	// width
	//	// The width of the pointer.
	//	// Type: Number.
	//	// Default: 8.
	// offset 
	//	// A value that indicates the distance the pointer moved.
	//	// Type: Number.
	//	// Default: 0.15.
	// shape
	//	// A value that indicates which shape the pointer shown.
	//	// Type: String.
	//	// Default: 'tri'.
	//	// remarks: Options are 'rect', 'tri'.
	// visible
	//	// A value that indicates whether show the pointer.
	//	// Type: Boolean.
	//	// Default: true.
	// template
	//	// A funtion that indicates how to draw the pointer.
	//	// Type: Funtion.
	//	// Default: null.
	// Code example: $("#selector").wijradialgauge("option", "pointer", {length: 0.85}).

	cap
	// A value that includes all settings of the pointer cap of the gauge.
	// Type: Object.
	// Default: { radius: 15, style: {fill: "#1E395B", stroke: "#1E395B"}, 
	// behindPointer: false, visible: true, template: null }.
	// Node: sub properties
	// radius
	//	// A value that indicates the radius of the cap.
	//	// Type: Number
	//	// Default: 15
	// style
	//	// A value that indicates the style of the cap.
	//	// Type: Object.
	//	// Default: {fill: "#1E395B", stroke: "#1E395B"}.
	// behindPointer
	//	// A value that indicates whether the cap showing behind of the pointer.
	//	// Type: Boolean.
	//	// Default: false.
	// visible
	//	// A value that indicates whether draw the cap.
	//	// Type: Boolean.
	//	// Default: true.
	// template
	//	// A function that indicates how to draw the pointer cap. 
	//	// If user want to customize the cap, they can use this option.
	//	// Type: Function
	//	// Default: null.
	// Code example: Code example: $("#selector").wijradialgauge("option", "cap", {radius: 20}).
	
	islogarithmic
	// A value that indicates whether it uses a logarithmic scale.
	// Type: Boolean.
	// Default: false.
	// Code example: $("#selector").wijradialgauge("option", "islogarithmic", true).

	logarithmicBase
	// A value that indicates the logarithmic base if the islogarithmic option is set to true.
	// Type: Number.
	// Default: 10.
	// Code example: $("#selector").wijradialgauge("option", "logarithmicBase", 10).

	labels
	// A value that includes all settings of the gauge label.
	// Type: Object.
	// Default: {format: "", style: {fill: "#1E395B", "font-size": "12pt",
	// "font-weight": "800"}, visible: true, offset: 30}.
	// Note: Sub properties
	// format
	//	// A value that indicates he format of the label text.
	//	// Type: String.
	//	// Default: "".
	// style
	//	// A value that indicates the style of the gauge label.
	//	// Type: Object.
	//	// Default: {fill: "#1E395B", "font-size": "12pt", "font-weight": "800"}.
	// visible
	//	// A value that indicates whether to show the gauge label.
	//	// Type: Boolean.
	//	// Default: true.
	// offset
	//	// A value that indicates the position of the gauge label.
	//	// Type: Number.
	//	// Default: 30.
	// Code example: $("#selector").wijradialgauge("option", "labels", {visible: false}).

	animation
	// An value that includes all settings of the gauge animation.
	// Type: Object.
	// Default: {enabled: true, duration: 2000, easing: ">"}.
	// Note: Sub properties
	// enabled
	//	// A value that indicates whether play animation.
	//	// Type: Boolean.
	//	// Default: true.
	// duration
	//	// A value that indicates how long time the animation palying.
	//	// Type: Number
	//	// Default: 2000.
	// easing
	//	// A value that indicates which easing the animation played.
	//	// Type: String.
	//	// Default: ">".
	// Code example: $("#selector").wijradialgauge("option", "animation", {enabled: false}).

	face
	// A value that indicates the style of the gauge face.
	// Type: Object.
	// Default: {fill: ""r(0.9, 0.60)#FFFFFF-#D9E3F0"", stroke: "#7BA0CC", "stroke-width": "4"}, template: null}.
	// Node: Sub properties
	// style
	//	// A value that indicates the style of the gauge face.
	//	// Type: Object.
	//	// Default: {fill: ""r(0.9, 0.60)#FFFFFF-#D9E3F0"", stroke: "#7BA0CC", "stroke-width": "4"}.
	// template
	//	// A value that indicates how to draw the gauge face.
	//	// Type: Function.
	//	// Default: null.
	// Code example: $("#selector").wijradialgauge("option", "face", {style: {fill: "#000555"}}).

	marginTop
	// A value that indicates the top margin of the gauge area.
	// Type: Number.
	// Default: 25.
	// Code example: $("#selector").wijradialgauge("option", "marginTop", 200}).

	marginRight
	// A value that indicates the right margin of the gauge area.
	// Type: Number.
	// Default: 25.
	// Code example: $("#selector").wijradialgauge("option", "marginRight", 200}).

	marginBottom
	// A value that indicates the bottom margin of the gauge area.
	// Type: Number.
	// Default: 25.
	// Code example: $("#selector").wijradialgauge("option", "marginBottom", 200}).

	marginLeft
	// A value that indicates the left margin of the gauge area.
	// Type: Number.
	// Default: 25.
	// Code example: $("#selector").wijradialgauge("option", "marginLeft", 200}).

	ranges
	//  An array that includes all settings which indicates how to draw the range in the gauge.
	// Type :Array.
	// Default: [].
	// Node: inside property
	// startWidth
	//	// A value that indicates the width of the range at start side.
	//	// Type: Number.
	//	// Default: 0.
	// endWidth
	//	// A value that indicates the width of the range at end side.
	//	// Type: Number.
	//	// Default: 0.
	// width
	//	// A value that indicates the width of the range if the startWidth and endWidth is not specialize.
	//	// Type: Number.
	//	// Default:0.
	// startValue
	//	// A value that indicates a start position which is the value's point to draw the range.
	//	// Type: Number.
	//	// Default: 0.
	// endValue
	//	// A value that indicates an end position which is the value's point to draw the range.
	//	// Type: Number.
	//	// Default: 0.
	// startDistance
	//	// A value that indicates a relative position the range drawing from.
	//	// Type: Number.
	//	// Default: 0.
	// endDistance
	//	// A value that indicates a relative the range drawing to.
	//	// Type: Number.
	//	// Default: 0.
	// style
	//	// A value that indicates the style of the gauge range.
	//	// Type: Object
	//	// Default:{}
	// Code example: $("#selector").wijradialgauge("option", "ranges", [
	// startWidth: 2, endWidth: 5, startValue: 0, endValue: 10, startDistance: 0.6, endDistance: 0.58,
	// style: { fill: "#7BA0CC", stroke: "#7BA0CC", "stroke-width": "0" }] }).

