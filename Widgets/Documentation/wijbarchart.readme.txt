* Title:
	wijbarchart widget.

* Description
	wijbarchart widget allows the user to show customized pie charts.

* Depends:
	raphael.js
	globalize.min.js
	jquery.ui.widget.js
	jquery.wijmo.wijchartcore.js

* HTML Markup
	<div id="barchart">
	</div>
	or
	<table id = "barchart">
		<thead>
			<td></td>
			<th>x1</th>
			<th>x2</th>
		</thead>
		<tbody>
			<tr>
				<th scope="row">seriesName1</th>
				<td>y1</td>
				<td>y2</td>
			</tr>
			<tr>
				<th scope="row">seriesName2</th>
				<td>y1</td>
				<td>y2</td>
			</tr>
		</tbody>
	</table>

* Options:
	horizontal: true
	// A value that determines whether the bar chart renders horizontal or vertical.
	// Type: Boolean.
	// Default: true.

	stacked: false
	// A value determines whether to show stacked chart.
	// Type: Boolean.
	// Default: false.
	
	is100Percent: false
	// A value determines whether to show stacked and percentage chart.
	// Type: Boolean.
	// Default: false.
	
	clusterOverlap: 0
	// A value indicates the percentage that bar elements in the same cluster overlap.
	// Type: Number.
	// Default: 0
	
	clusterWidth: 85
	// A value indicates the percentage that each bar cluster occupies.
	// Type: Number.
	// Default: 85
	
	clusterRadius: 0,
	// A value that indicates the corner-radius for the bar.
	// Type: Number.
	// Default: 0.

	clusterSpacing: 0,
	// A value that indicates the spacing between the adjacent bars.
	// Type: Number.
	// Default: 0.
	
	animation
	// A value indicates whether to show animation and the duration for the animation.
	// Type: Object.
	// Default: {enabled:true, duration:400, easing: "easeOutExpo"}
	// Note: Sub properties
	//	enabled:true,
	//	// A value determines whether to show animation.
	//	// Type: Boolean.
	//	// Default: true.
	//	duration:400
	//	// A value indicates the duration for the animation.
	//	// Type: Number.
	//	// Default: 400.
	//	easing: "easeOutExpo"
	//	// A value that indicates the easing for the animation.
	//	// Type: string.
	//	// Default: "linear".
	
	width: null
	// A value indicates the width of wijchart.
	// Type: Number.
	// Default: null.	
	
	height: null
	// A value indicates the height of wijchart.
	// Type: Number.
	// Default: null.
	
	seriesList: []
	// An array collection contains the data to be charted.
	// Type: Array.
	// Default: [].
	//	Code example: seriesList: [{
	//                 label: "Q1",
	//                 legendEntry: true,
	//                 style: { fill: "rgb(255,0,0)", stroke:"none"},
	//                 data: {
	//						x: [1, 2, 3, 4, 5],
	//						y: [12, 21, 9, 29, 30]
	//					},
	//                 offset: 0
	//             }, {
	//         		label: "Q2",
	//            		legendEntry: true,
	//            		style: { fill: "rgb(255,125,0)", stroke: "none" },
	//            		data: {
	//						xy: [1, 21, 2, 10, 3, 19, 4, 31, 5, 20]
	//					},
	//            		offset: 0
	//        		}]
	//				OR
	//				seriesList: [{
	//         		label: "Q1",
	//            		legendEntry: true,
	//            		style: { fill: "rgb(255,125,0)", stroke: "none" },
	//            		data: {
	//						x: ["A", "B", "C", "D", "E"],
	//						y: [12, 21, 9, 29, 30]
	//					},
	//            		offset: 0
	//        		}]
	//				OR
	//				seriesList: [{
	//         		label: "Q1",
	//            		legendEntry: true,
	//            		style: { fill: "rgb(255,125,0)", stroke: "none" },
	//            		data: {
	//						x: [new Date(1978, 0, 1), new Date(1980, 0, 1), new Date(1981, 0, 1), new Date(1982, 0, 1), new Date(1983, 0, 1)],
	//						y: [12, 21, 9, 29, 30]
	//					},
	//            		offset: 0
	//        		}]

	seriesStyles: [{stroke: "#77b3af", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#67908e", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#465d6e", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#5d3f51", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#682e32", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#8c5151", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#ce9262", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#ceb664", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#7fb34f", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#2a7b5f", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#6079cb", opacity: 0.9, "stroke-width": "1"}, {
				stroke: "#60a0cb", opacity: 0.9, "stroke-width": "1"}],
	/// An array collection that contains the style to be charted.
	// Type: Array.
	// Default: [{stroke: "#77b3af", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#67908e", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#465d6e", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#5d3f51", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#682e32", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#8c5151", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#ce9262", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#ceb664", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#7fb34f", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#2a7b5f", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#6079cb", opacity: 0.9, "stroke-width": "1"}, {
	//				stroke: "#60a0cb", opacity: 0.9, "stroke-width": "1"}].
	
	seriesHoverStyles: [{opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}, {
				opacity: 1, "stroke-width": "1.5"}],
	// An array collection that contains the style to be charted when hovering the chart element.
	// Type: Array.
	// Default: [{opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}, {
	//				opacity: 1, "stroke-width": "1.5"}].
	
	marginTop: 25
	// A value indicates the top margin of chart area.
	// Type: Number.
	// Default: 25.
	
	marginRight: 25
	// A value indicates the right margin of chart area.
	// Type: Number.
	// Default: 25.

	marginBottom: 25
	// A value indicates the bottom margin of chart area.
	// Type: Number.
	// Default: 25.

	marginLeft: 25
	// A value indicates the left margin of chart area.
	// Type: Number.
	// Default: 25.
	
	textStyle: {fill: "#888","font-size": "10pt",stroke: "none"},
	// A value that indicates the style of the chart text.
	// Type: Object.
	// Default: {fill:"#888", "font-size": "10pt", stroke:"none"}.

	header:	
	// An object value indicates the header of the chart element.
	// Type: Object.
	// Default: {visible:true, style:{fill:"none", stroke:"none"}, textStyle:{"font-size": "18pt", fill:"#666", stroke:"none"}, compass:"north", orientation:"horizontal"} 
	{
		text: "",
		// A value indicates the text of header.
		// Type: String.
		// Default: "".

		style: {
			fill: "none",
			stroke: "none"
		},
		// A value indicates the style of header.
		// Type: Object.
		// Default: {fill:"none", stroke:"none"}.

		textStyle: {
			"font-size": "18pt",
			fill: "#666",
			stroke: "none"
		},
		// A value indicates the style of header text.
		// Type: Object.
		// Default: {"font-size": "18pt", fill:"#666", stroke:"none"}.
		
		compass: "north",
		// A value indicates the compass of header.
		// Type: String.
		// Default: "north".
		// Notes: options are 'north', 'south', 'east' and 'west'.		
		
		orientation: "horizontal",
		// A value indicates the orientation of header.
		// Type: String.
		// Default: "horizontal".
		// Notes: options are 'horizontal' and 'vertical'.
		
		visible: true
		// A value indicates the visibility of header.
		// Type: Boolean.
		// Default: true.
	}
	
	footer: 
	// An object value indicates the footer of the chart element.
	// Type: Object.
	// Default: {visible:false, style:{fill:"#fff", stroke:"none"}, textStyle:{fille:"#000", stroke:"none"}, compass:"south", orientation:"horizontal"}
	{
		text: "",
		// A value indicates the text of footer.
		// Type: String.
		// Default: "".

		style: {
			fill: "#fff",
			stroke: "none"
		},
		// A value indicates the style of footer.
		// Type: Object.
		// Default: {fill:"#fff", stroke:"none"}.

		textStyle: {
			fill: "#000",
			stroke: "none"
		},
		// A value indicates the style of footer text.
		// Type: Object.
		// Default: {fill:"#000", stroke:"none"}.
		
		compass: "south",
		// A value indicates the compass of footer.
		// Type: String.
		// Default: "south".
		// Notes: options are 'north', 'south', 'east' and 'west'.		
		
		orientation: "horizontal",
		// A value indicates the orientation of footer.
		// Type: String.
		// Default: "horizontal".
		// Notes: options are 'horizontal' and 'vertical'.
		
		visible: false
		// A value indicates the visibility of footer.
		// Type: Boolean.
		// Default: false.
	},
	
	legend: 
	// An object value indicates the legend of the chart element.
	// Type: Object.
	// Default: {text:"", textMargin:{left:2,top:2,right:2,bottom:2},titleStyle:{"font-weight":"bold",fill:"#000",stroke:"none},
	//			visible:true, style:{fill:"#none", stroke:"none"}, textStyle:{fille:"#333", stroke:"none"}, compass:"east", orientation:"vertical"}
	{
		text: "",
		// A value indicates the text of legend.
		// Type: String.
		// Default: "".
		
		textMargin: { left: 2, top: 2, right: 2, bottom: 2 },
		// A value that indicates the text margin of the legend item.
		// Type: Number.
		// Default: {left:2, top:2, right:2, bottom:2}.

		style: {
			fill: "none",
			stroke: "none"
		},
		// A value indicates the style of legend.
		// Type: Object.
		// Default: {fill:"none", stroke:"none"}.

		textStyle: {
			fill: "#333",
			stroke: "none"
		},
		// A value indicates the style of legend text.
		// Type: Object.
		// Default: {fill:"#333", stroke:"none"}.
		
		titleStyle: {"font-weight": "bold",fill: "#000",stroke: "none"},
		// A value that indicates the style of the legend title.
		// Default: {"font-weight": "bold", fill:"#000", stroke:"none"}.
		// Type: Object.
		
		compass: "east",
		// A value indicates the compass of legend.
		// Type: String.
		// Default: "east".
		// Notes: options are 'north', 'south', 'east' and 'west'.		
		
		orientation: "vertical",
		// A value indicates the orientation of legend.
		// Type: String.
		// Default: "vertical".
		// Notes: options are 'horizontal' and 'vertical'.
		
		visible: true
		// A value indicates the visibility of legend.
		// Type: Boolean.
		// Default: true.
	},
	
	axis: 		
	// A value indicates the infomations of axis.
	// Type: Object.
	// Default: {x:{alignment:"center",style:{stroke:"#999999","stroke-width":0.5}, visible:true, textVisible:true, 
	//				textStyle:{fill: "#888", "font-size": "15pt", "font-weight": "bold"},
	//				labels: {style: {fill: "#333", "font-size": "11pt"},textAlign: "near", width:null},
	//				compass:"south",autoMin:true,autoMax:true,autoMajor:true,autoMinor:true,
	//				gridMajor:{visible:false, style:{stroke:"#CACACA","stroke-dasharray":"- "}}},
	//				gridMinor:{visible:false, style:{stroke:"#CACACA","stroke-dasharray":"- "}}},
	//				tickMajor:{position:"none",style:{fill:"black"},factor:1},tickMinor:{position:"none",style:{fill:"black"},factor:1},
	//				annoMethod:"values",valueLabels:[]},
	//			  y:{alignment:"center",style:{stroke: "#999999","stroke-width": 0.5}, visible:false, textVisible:true, 
	//				textStyle: {fill: "#888","font-size": "15pt","font-weight": "bold"}, 
	//				labels: {style: {fill: "#333","font-size": "11pt"},textAlign: "center", width:null},
	//				compass:"west",autoMin:true,autoMax:true,autoMajor:true,autoMinor:true,
	//				gridMajor:{visible:true, style:{stroke:"#999999", "stroke-width": "0.5","stroke-dasharray":"none"}}},
	//				gridMinor:{visible:false, style:{stroke:"#CACACA","stroke-dasharray":"- "}}},
	//				tickMajor:{position:"none",style:{fill:"black"},factor:1},tickMinor:{position:"none",style:{fill:"black"},factor:1},
	//				annoMethod:"values",valueLabels:[]}.
	{	
		x: 
		// A value indicates the infomations of X axis.
		// Default: {alignment:"center",style:{stroke:"#999999","stroke-width":0.5}, visible:true, textVisible:true, 
		//			textStyle:{fill: "#888", "font-size": "15pt", "font-weight": "bold"}, 
		//			labels: {style: {fill: "#333", "font-size": "11pt"},textAlign: "near",width:null},
		//			compass:"south",autoMin:true,autoMax:true,autoMajor:true,autoMinor:true,
		//			gridMajor:{visible:false, style:{stroke:"#CACACA","stroke-dasharray":"- "}}},
		//			gridMinor:{visible:false, style:{stroke:"#CACACA","stroke-dasharray":"- "}}},
		//			tickMajor:{position:"none",style:{fill:"black"},factor:1},tickMinor:{position:"none",style:{fill:"black"},factor:1},
		//			annoMethod:"values",valueLabels:[]}.
		// Type: Object.
		{
			alignment: "center",
			// A value that indicates the alignment of the X axis text.
			// Default: "center".
			// Type: String.
			// Notes: options are 'center', 'near', 'far'.
			
			style: {
				stroke: "#999999",
				"stroke-width": 0.5
			},
			// A value that indicates the style of the X axis.
			// Default: {stroke: "#999999", "stroke-width": 0.5}.
			// Type: Object.

			visible: true,
			// A value that indicates the visibility of the X axis.
			// Default: true.
			// Type: Boolean.

			textVisible: true,
			// A value that indicates the visibility of the X axis text.
			// Default: true.
			// Type: Boolean.

			textStyle: {
				fill: "#888",
				"font-size": "15pt",
				"font-weight": "bold"
			},
			// A value that indicates the style of text of the X axis.
			// Default: {fill: "#888","font-size": "15pt","font-weight": "bold"}.
			// Type: Object.

			labels: 
			// A value that provides information for the labels.
			// Default: {style: {fill: "#333","font-size": "11pt"},textAlign: "near",width:null}.
			// Type: Object.
			{
				style: {
					fill: "#333",
					"font-size": "11pt"
				},
				// A value that indicates the style of major text of the X axis.
				// Default: {fill: "#333","font-size": "11pt"}.
				// Type: Object.

				textAlign: "near"
				// A value that indicates the alignment of major text of the X axis.
				// Default: "near".
				// Type: String.
				// Notes: options are 'near', 'center' and 'far'.

				width: null
				// A value that indicates the width of major text of the X axis.
				// Default: null.
				// Type: Number.
			},

			compass: "south",
			// A value indicates the compass of X axis.
			// Default: "south".
			// Type: String.
			// Notes: options are 'north', 'south', 'east' and 'west'.
			
			autoMin: true,		
			// A value indicates whether the axis minimum value is calculated automatically.
			// Default: true.
			// Type: Boolean.
					
			autoMax: true,
			// A value indicates whether the axis maximum value is calculated automatically.
			// Default: true.
			// Type: Boolean.
			
			min: null,			
			// A value indicates the minimum value of the axis.
			// Default: null.
			// Type: Number.
			
			max: null,
			// A value indicates the maximum value of the axis.
			// Default: null.
			// Type: Number.
			
			autoMajor: true,
			// A value indicates whether the major tick mark values are calculated automatically.
			// Default: true.
			// Type: Boolean.
			
			autoMinor: true,
			// A value indicates whether the minor tick mark values are calculated automatically.
			// Default: true.
			// Type: Boolean.
			
			unitMajor: null,
			// A value indicates the units between major tick marks.
			// Default: null.
			// Type: Number.
			
			unitMinor: null,
			// A value indicates the units between minor tick marks.
			// Default: null.
			// Type: Number.
			
			gridMajor: 
			// A value that provides information for the major grid line.
			// Default: {visible:false, style:{stroke:"#CACACA","stroke-dasharray":"- "}}.
			// Type: Object.
			{
				visible: false,
				// A value that indicates the visibility of the major grid line.
				// Default: false.
				// Type: Boolean.

				style: {
					stroke: "#CACACA",
					"stroke-dasharray": "- "
				}
				// A value that indicates the style of the major grid line.
				// Default: {stroke:"#CACACA", "stroke-dasharray": "- "}.
				// Type: Object.
			},

			gridMinor: 
			// A value that provides information for the minor grid line.
			// Default: {visible:false, style:{stroke:"#CACACA","stroke-dasharray":"- "}}.
			// Type: Object.
			{
				visible: false,
				// A value that indicates the visibility of the minor grid line.
				// Default: false.
				// Type: Boolean.

				style: {
					stroke: "#CACACA",
					"stroke-dasharray": "- "
				}
				// A value that indicates the style of the minor grid line.
				// Default: {stroke:"#CACACA", "stroke-dasharray": "- "}.
				// Type: Object.
			},

			tickMajor: 
			// A value that provides information for the major tick.
			// Default: {position:"none", style:{fill:"black"}, factor:1}.
			// Type: Object.
			{
				position: "none",
				// A value that indicates the type of major tick mark.
				// Default: "none".
				// Type: String.
				// Notes: options are 'none', 'inside', 'outside' and 'cross'.

				style: { fill: "black" },
				// A value that indicates the style of major tick mark.
				// Default: {fill: "black"}.
				// Type: Object.

				factor: 1
				// A value that indicates an integral factor for major tick mark length.
				// Default: 1.
				// Type: Number.
			},
			
			tickMinor: 
			// A value that provides information for the minor tick.
			// Default: {position:"none", style:{fill:"black"}, factor:1}.
			// Type: Object.
			{
				position: "none",
				// A value that indicates the type of minor tick mark.
				// Default: "none".
				// Type: String.
				// Notes: options are 'none', 'inside', 'outside' and 'cross'.

				style: { fill: "black" },
				// A value that indicates the style of minor tick mark.
				// Default: {fill: "black"}.
				// Type: Object.

				factor: 1
				// A value that indicates an integral factor for minor tick mark length.
				// Default: 1.
				// Type: Number.
			},

			annoMethod: "values",
			// A value indicates the method of annotation.
			// Default: "values".
			// Type: String.
			// Notes: options are 'values', 'valueLabels'.

			valueLabels: []
			// A value shows a collection of valueLabels for the X axis.
			// Default: [].
			// Type: Array.
		},
		y: 
		// A value indicates the infomations of Y axis.
		// Default: {alignment:"center",style:{stroke: "#999999","stroke-width": 0.5}, visible:false, textVisible:true, 
		//			textStyle: {fill: "#888","font-size": "15pt","font-weight": "bold"}, 
		//			labels: {style: {fill: "#333","font-size": "11pt"},textAlign: "center",width:null},
		//			compass:"west",autoMin:true,autoMax:true,autoMajor:true,autoMinor:true,
		//			gridMajor:{visible:true, style:{stroke:"#999999", "stroke-width": "0.5", "stroke-dasharray":"none"}}},
		//			gridMinor:{visible:false, style:{stroke:"#CACACA","stroke-dasharray":"- "}}}
		//			tickMajor:{position:"none",style:{fill:"black"},factor:1},tickMinor:{position:"none",style:{fill:"black"},factor:1},
		//			annoMethod:"values",valueLabels:[]}
		// Type: Object.
		{
			alignment: "center",
			// A value that indicates the alignment of the Y axis text.
			// Default: "center".
			// Type: String.
			// Notes: options are 'center', 'near', 'far'.

			style: {
				stroke: "#999999",
				"stroke-width": 0.5
			},
			// A value that indicates the style of the Y axis.
			// Default: {stroke:"#999999", "stroke-width": 0.5}.
			// Type: Object.

			visible: false,
			// A value that indicates the visibility of the Y axis.
			// Default: false.
			// Type: Boolean.

			textVisible: true,
			// A value that indicates the visibility of the Y axis text.
			// Default: true.
			// Type: Boolean.

			textStyle: {
				fill: "#888",
				"font-size": "15pt",
				"font-weight": "bold"
			},
			// A value that indicates the style of text of the Y axis.
			// Default: {fill: "#888", "font-size": "15pt", "font-weight": "bold"}.
			// Type: Object.

			labels: 
			// A value that provides information for the labels.
			// Default: {style: {fill: "#333","font-size": "11pt"},textAlign: "center",width:null}.
			// Type: Object.
			{
				style: {
					fill: "#333",
					"font-size": "11pt"
				},
				// A value that indicates the style of major text of the Y axis.
				// Default: {fill: "#333","font-size": "11pt"}.
				// Type: Object.

				textAlign: "center"
				// A value that indicates the alignment of major text of the Y axis.
				// Default: "center".
				// Type: String.
				// Notes: options are 'near', 'center' and 'far'.
								
				width: null
				// A value that indicates the width major text of the Y axis.
				// Default: null.
				// Type: Number.				
			},

			compass: "west",
			// A value indicates the compass of Y axis.
			// Default: "west".
			// Type: String.
			// Notes: options are 'north', 'south', 'east' and 'west'.
			
			autoMin: true,
			// A value indicates whether the axis minimum value is calculated automatically.
			// Default: true.
			// Type: Boolean.

			autoMax: true,
			// A value indicates whether the axis maximum value is calculated automatically.
			// Default: true.
			// Type: Boolean.

			min: null,		
			// A value indicates the minimum value of the axis.
			// Default: null.
			// Type: Number.

			max: null,
			// A value indicates the maximum value of the axis.
			// Default: null.
			// Type: Number.
			
			autoMajor: true,
			// A value indicates whether the major tick mark values are calculated automatically.
			// Default: true.
			// Type: Boolean.

			autoMinor: true,
			// A value indicates whether the minor tick mark values are calculated automatically.
			// Default: true.
			// Type: Boolean.

			unitMajor: null,
			// A value indicates the units between major tick marks.
			// Default: null.
			// Type: Number.

			unitMinor: null,
			// A value indicates the units between minor tick marks.
			// Default: null.
			// Type: Number.

			gridMajor: 
			// A value that provides information for the major grid line.
			// Default: {visible:true, style:{stroke:"#999999", "stroke-width": "0.5","stroke-dasharray":"none"}}.
			// Type: Object.
			{
				visible: true,
				// A value that indicates the visibility of the major grid line.
				// Default: true.
				// Type: Boolean.

				style: {
					stroke: "#999999",
					"stroke-width": "0.5",
					"stroke-dasharray": "none"
				}
				// A value that indicates the style of the major grid line.
				// Default: {stroke:"#999999", "stroke-width": "0.5", "stroke-dasharray": "none"}.
				// Type: Object.
			},

			gridMinor: 
			// A value that provides information for the minor grid line.
			// Default: {visible:false, style:{stroke:"#CACACA","stroke-dasharray":"- "}}.
			// Type: Object.
			{
				visible: false,
				// A value that indicates the visibility of the minor grid line.
				// Default: false.
				// Type: Boolean.

				style: {
					stroke: "#CACACA",
					"stroke-dasharray": "- "
				}
				// A value that indicates the style of the minor grid line.
				// Default: {stroke:"#CACACA", "stroke-dasharray": "- "}.
				// Type: Object.
			},

			tickMajor: 
			// A value that provides information for the major tick.
			// Default: {position:"none", style:{fill:"black"}, factor:1}.
			// Type: Object.
			{
				position: "none",
				// A value that indicates the type of major tick mark.
				// Default: "none".
				// Type: String.
				// Notes: options are 'none', 'inside', 'outside' and 'cross'.

				style: { fill: "black" },
				// A value that indicates the style of major tick mark.
				// Default: {fill: "black"}.
				// Type: Object.

				factor: 1
				// A value that indicates an integral factor for major tick mark length.
				// Default: 1.
				// Type: Number.
			},

			tickMinor: 
			// A value that provides information for the minor tick.
			// Default: {position:"none", style:{fill:"black"}, factor:1}.
			// Type: Object.
			{
				position: "none",
				// A value that indicates the type of minor tick mark.
				// Default: "none".
				// Type: String.
				// Notes: options are 'none', 'inside', 'outside' and 'cross'.

				style: { fill: "black" },
				// A value that indicates the style of minor tick mark.
				// Default: {fill: "black"}.
				// Type: Object.

				factor: 1
				// A value that indicates an integral factor for minor tick mark length.
				// Default: 1.
				// Type: Number.
			},
			
			annoMethod: "values",
			// A value indicates the method of annotation.
			// Default: "values".
			// Type: String.
			// Notes: options are 'values', 'valueLabels'.
			
			valueLabels: []
			// A value shows a collection of valueLabels for the X axis.
			// Default: [].
			// Type: Array.
		}
	},
	
	hint: 
	// A value is used to indicate whether and what to show on the popped tooltip.
	// Default: {enable:true, content:null, 
	//			contentStyle: {fill: "#d1d1d1","font-size": "16pt"},
	//			title:null, 
	//			titleStyle: {fill: "#d1d1d1","font-size": "16pt"},
	//			style: {fill: "270-#333333-#000000", "stroke-width": "2"},
	//			animated: "fade", showAnimated: "fade", hideAnimated: "fade",
	//			duration: 120, showDuration: 120, hideDuration: 120,
	//			showDelay: 150, hideDelay: 150, easing: "", 
	//			showEasing: "", hideEasing: "",
	//			compass:"north", offsetX: 0, offsetY: 0,  
	//			showCallout: true, calloutFilled: false, 
	//			calloutFilledStyle: {fill: "#000"}}.
	// Type: Function.
	{
		enable: true,
		// A value indicates whether to show the tooltip.
		// Default: true.
		// Type: Boolean.
		
		content: null
		// A value that will be shown in the content part of the tooltip 
		//	or a function which is used to get a value for the tooltip shown.
		// Default: "".
		// Type: String or Function.
		
		contentStyle: {
			fill: "#d1d1d1",
			"font-size": "16pt"
		},
		// A value that indicates the style of content text.
		// Default: {fill: "#d1d1d1","font-size": "16pt"}.
		// Type: Object.			

		title: null,
		// A value that will be shown in the title part of the tooltip 
		//	or a function which is used to get a value for the tooltip shown.
		// Default: null.
		// Type: String or Function.
		
		titleStyle: {
			fill: "#d1d1d1",
			"font-size": "16pt"
		},
		// A value that indicates the style of title text.
		// Default: {fill: "#d1d1d1","font-size": "16pt"}.
		// Type: Object.
		
		style: {
			fill: "270-#333333-#000000",
			"stroke-width": "2"
		},
		// A value that indicates the style of container.
		// Default: {fill: "270-#333333-#000000", "stroke-width": "2"}.
		// Type: Object.

		animated: "fade",
		// A value that indicates the effect during show or hide 
		//	when showAnimated or hideAnimated isn't specified.
		// Default:"fade".
		// Type:String.
		
		showAnimated: "fade",
		// A value that indicates the effect during show.
		// Default:"fade".
		// Type:String.
		
		hideAnimated: "fade",
		// A value that indicates the effect during hide.
		// Default:"fade".
		// Type:String.
		
		duration: 120,
		// A value that indicates the millisecond to show or hide the tooltip
		//	when showDuration or hideDuration isn't specified.
		// Default:120.
		// Type:Number.
		
		showDuration: 120,
		// A value that indicates the millisecond to show the tooltip.
		// Default:120.
		// Type:Number.
		
		hideDuration: 120,
		// A value that indicates the millisecond to hide the tooltip.
		// Default:120.
		// Type:Number.
		
		easing: "", 
		// A value that indicates the easing during show or hide when
		//	showEasing or hideEasing isn't specified. 
		// Default: "".
		// Type: String.
		
		showEasing: "", 
		// A value that indicates the easing during show. 
		// Default: "".
		// Type: String.
		
		hideEasing: "",
		// A value that indicates the easing during hide. 
		// Default: "".
		// Type: String.
		
		showDelay: 150,
		// A value that indicates the millisecond delay to show the tooltip.
		// Default: 150.
		// Type: Number.
		
		hideDelay: 150,		
		// A value that indicates the millisecond delay to hide the tooltip.
		// Default: 150.
		// Type: Number.
		
		compass: "north",
		// A value that indicates the compass of the tooltip.
		// Default: "north".
		// Type: String.
		// Notes: options are 'west', 'east', 'south', 'north', 'southeast', 'southwest', 'northeast', 'northwest'.

		offsetX: 0,
		// A value that indicates the horizontal offset 
		//	of the point to show the tooltip.
		// Default: 0.
		// Type: Number.
		
		offsetY: 0,
		// A value that indicates the vertical offset 
		//	of the point to show the tooltip.
		// Default: 0.
		// Type: Number.
		
		showCallout: true,
		// Determines whether to show the callout element.
		// Default:true.
		// Type:Boolean.
		
		calloutFilled: false,
		// Determines whether to fill the callout.  
		//	If true, then the callout triangle will be filled.
		// Default:false.
		// Type:Boolean.
		
		calloutFilledStyle: {
			fill: "#000"
		}
		// A value that indicates the style of the callout filled.
		// Default: {fill: "#000"}.
		// Type: Object.		
	},
	
	showChartLabels: true,
	// A value indicates whether to show default chart labels.
	// Default: true.
	// Type: Boolean.
	
	chartLabelStyle: {},
	// A value that indicates style of the chart labels.
	// Default: {}.
	// Type: Object.

	disableDefaultTextStyle: false,
	// A value that indicates whether to disable the default text style.
	// Default: false.
	// Type: Boolean.

	shadow: true,
	// A value that indicates whether to show shadow for the chart.
	// Default: false.
	// Type: Boolean.

* Methods:
	getCanvas()
	// Returns the reference to raphael canvas object.
	
	getBar(index)
	// Returns the bar of set of the raphael's objects(rects) what represents bars for the series data with given index.
		
	redraw(drawIfNeeded)
	// Redraw the chart.
	// <param name="drawIfNeeded" type="Boolean">
	// A value that indicates whether to redraw the chart 
	//	no matter whether the chart is painted.
	// If true, then only when the chart is not created before, 
	// it will be redrawn.  Otherwise, the chart will be forced to redraw.  
	//	The default value is false.
	// </param>

* Events:	
	mousedown: null,
	// Occurs when the user clicks a mouse button.
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#barchart").wijbarchart({mousedown: function(e, data) { } });
	// Bind to the event by type: wijbarchartmousedown
	// $("#barchart").bind("wijbarchartmousedown", function(e, data) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>
	// <param name="data" type="Object">
	// An object that contains all the series infos of the mousedown bar. 
	// data.bar: the Raphael object of the bar.
	// data.data: data of the series of the bar.
	// data.hoverStyle: hover style of series of the bar.
	// data.index: index of the bar.
	// data.label: label of the series of the bar.
	// data.legendEntry: legend entry of the series of the bar.
	// data.style: style of the series of the bar.
	// data.type: "bar"
	//	</param>

	mouseup: null,
	// Occurs when the user releases a mouse button while the pointer is over the chart element.
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#barchart").wijbarchart({mouseup: function(e, data) { } });
	// Bind to the event by type: wijbarchartmouseup
	// $("#barchart").bind("wijbarchartmouseup", function(e, data) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>
	// <param name="data" type="Object">
	// An object that contains all the series infos of the mouseup bar. 
	// data.bar: the Raphael object of the bar.
	// data.data: data of the series of the bar.
	// data.hoverStyle: hover style of series of the bar.
	// data.index: index of the bar.
	// data.label: label of the series of the bar.
	// data.legendEntry: legend entry of the series of the bar.
	// data.style: style of the series of the bar.
	// data.type: "bar"
	//	</param>

	mouseover: null,
	// Occurs when the user first places the pointer over the chart element.
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#barchart").wijbarchart({mouseover: function(e, data) { } });
	// Bind to the event by type: wijbarchartmouseover
	// $("#barchart").bind("wijbarchartmouseover", function(e, data) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>
	// <param name="data" type="Object">
	// An object that contains all the series infos of the mouseover bar. 
	// data.bar: the Raphael object of the bar.
	// data.data: data of the series of the bar.
	// data.hoverStyle: hover style of series of the bar.
	// data.index: index of the bar.
	// data.label: label of the series of the bar.
	// data.legendEntry: legend entry of the series of the bar.
	// data.style: style of the series of the bar.
	// data.type: "bar"
	//	</param>

	mouseout: null,
	// Occurs when the user moves the pointer off of the chart element.
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#barchart").wijbarchart({mouseout: function(e, data) { } });
	// Bind to the event by type: wijbarchartmouseout
	// $("#barchart").bind("wijbarchartmouseout", function(e, data) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>
	// <param name="data" type="Object">
	// An object that contains all the series infos of the mouseout bar. 
	// data.bar: the Raphael object of the bar.
	// data.data: data of the series of the bar.
	// data.hoverStyle: hover style of series of the bar.
	// data.index: index of the bar.
	// data.label: label of the series of the bar.
	// data.legendEntry: legend entry of the series of the bar.
	// data.style: style of the series of the bar.
	// data.type: "bar"
	//	</param>

	mousemove: null,
	// Occurs when the user moves the mouse pointer
	// while it is over a chart element.
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#barchart").wijbarchart({mousemove: function(e, data) { } });
	// Bind to the event by type: wijbarchartmousemove
	// $("#barchart").bind("wijbarchartmousemove", function(e, data) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>
	// <param name="data" type="Object">
	// An object that contains all the series infos of the mousemove bar. 
	// data.bar: the Raphael object of the bar.
	// data.data: data of the series of the bar.
	// data.hoverStyle: hover style of series of the bar.
	// data.index: index of the bar.
	// data.label: label of the series of the bar.
	// data.legendEntry: legend entry of the series of the bar.
	// data.style: style of the series of the bar.
	// data.type: "bar"
	//	</param>

	click: null,
	// Occurs when the user clicks the chart element. 
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#barchart").wijbarchart({click: function(e, data) { } });
	// Bind to the event by type: wijbarchartclick
	// $("#barchart").bind("wijbarchartclick", function(e, data) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>
	// <param name="data" type="Object">
	// An object that contains all the series infos of the clicked bar.
	// data.bar: the Raphael object of the bar.
	// data.data: data of the series of the bar.
	// data.hoverStyle: hover style of series of the bar.
	// data.index: index of the bar.
	// data.label: label of the series of the bar.
	// data.legendEntry: legend entry of the series of the bar.
	// data.style: style of the series of the bar.
	// data.type: "bar"
	//	</param>

	beforeserieschange: null,
	// Occurs before the series changes.  This event can be cancelled. 
	// "return false;" to cancel the event.
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#barchart").wijbarchart({beforeserieschange: function(e, data) { } });
	// Bind to the event by type: wijbarchartbeforeserieschange
	// $("#barchart").bind("wijbarchartbeforeserieschange", function(e, data) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>
	// <param name="data" type="Object">
	// An object that contains old and new series values.
	// data.oldSeriesList: old series list before change.
	//	data.newSeriesList: new series list that will replace old one.  
	//	</param>

	serieschanged: null,
	// Occurs when the series changes. 
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#barchart").wijbarchart({serieschanged: function(e, data) { } });
	// Bind to the event by type: wijbarchartserieschanged
	// $("#barchart").bind("wijbarchartserieschanged", function(e, data) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>
	// <param name="data" type="Object">
	// An object that contains new series values.  
	//	</param>

	beforepaint: null,
	// Occurs before the canvas is painted.  This event can be cancelled.
	// "return false;" to cancel the event.
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#barchart").wijbarchart({beforepaint: function(e) { } });
	// Bind to the event by type: wijbarchartbeforepaint
	// $("#barchart").bind("wijbarchartbeforepaint", function(e) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>

	painted: null
	// Occurs after the canvas is painted. 
	// Default: null.
	// Type: Function.
	// Code example:
	// Supply a function as an option.
	//  $("#barchart").wijbarchart({painted: function(e) { } });
	// Bind to the event by type: wijbarchartpainted
	// $("#barchart").bind("wijbarchartpainted", function(e) {} );
	// <param name="e" type="eventObj">
	// jQuery.Event object.
	//	</param>
