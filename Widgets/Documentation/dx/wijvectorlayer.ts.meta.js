var wijmo = wijmo || {};

(function () {
wijmo.maps = wijmo.maps || {};

(function () {
/** The ILOD (Level Of Detail) interface associates element visibility with the map scale (zoom and size).
  * @interface ILOD
  * @namespace wijmo.maps
  */
wijmo.maps.ILOD = function () {};
/** <p>When the pixel size of element is less than minSize the element is not visible.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.ILOD.prototype.minSize = null;
/** <p>When the pixel size of element is greater than maxSize the element is not visible.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.ILOD.prototype.maxSize = null;
/** <p>When the map zoom is less than minZoom the element is not visible.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.ILOD.prototype.minZoom = null;
/** <p>When the map zoom is greater than maxZoom the element is not visible.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.ILOD.prototype.maxZoom = null;
/** @class wijvectorlayer
  * @widget 
  * @namespace jQuery.wijmo.maps
  * @extends wijmo.maps.wijlayer
  */
wijmo.maps.wijvectorlayer = function () {};
wijmo.maps.wijvectorlayer.prototype = new wijmo.maps.wijlayer();
/** Force to request the data to get the newest one. */
wijmo.maps.wijvectorlayer.prototype.refresh = function () {};
/** Removes the wijvectorlayer functionality completely. This will return the element back to its pre-init state. */
wijmo.maps.wijvectorlayer.prototype.destroy = function () {};

/** @class */
var wijvectorlayer_options = function () {};
/** <p>The type of the data object. The vector layer supports the following type:
  * &lt;ul&gt;
  * &lt;li&gt;wijJson: the json object with the C1 format. &lt;/li&gt;
  * &lt;li&gt;geoJson: the json object with the GeoJson format.&lt;/li&gt;
  * &lt;/ul&gt;</p>
  * @field 
  * @type {string}
  * @option
  */
wijvectorlayer_options.prototype.dataType = null;
/** <p>The data of the data source.</p>
  * @field 
  * @option 
  * @remarks
  * The data may be the following:
  * &lt;ul&gt;
  * &lt;li&gt;string: the uri of the json data.&lt;/li&gt;
  * &lt;li&gt;function: the function to be called to get the data object.&lt;/li&gt;
  * &lt;li&gt;object: the data object.&lt;/li&gt;
  * &lt;/ul&gt;
  */
wijvectorlayer_options.prototype.data = null;
/** <p class='defaultValue'>Default value: 3</p>
  * <p>Specifies minimal size at which vectors become visible. The default value is 3.</p>
  * @field 
  * @type {number}
  * @option
  */
wijvectorlayer_options.prototype.minSize = 3;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IPlacemark.html'>wijmo.maps.IPlacemark</a></p>
  * <p class='defaultValue'>Default value: null</p>
  * <p>Customize the rendering of placemark.</p>
  * @field 
  * @type {wijmo.maps.IPlacemark}
  * @option
  */
wijvectorlayer_options.prototype.placemark = null;
/** This is a callback function called after the shape of the vector created for rendering.
  * @event 
  * @param {jQuery.Event} event A jQuery Event object.
  * @param {IVectorEventData} data The data contains the vector and shape.
  */
wijvectorlayer_options.prototype.shapeCreated = null;
/** This is a callback function called when the mouse enter the shape.
  * @event 
  * @param {jQuery.Event} event A jQuery Event object.
  * @param {IVectorEventData} data The data contains the vector and shape.
  */
wijvectorlayer_options.prototype.mouseEnter = null;
/** This is a callback function called when the mouse leave the shape.
  * @event 
  * @param {jQuery.Event} event A jQuery Event object.
  * @param {IVectorEventData} data The data contains the vector and shape.
  */
wijvectorlayer_options.prototype.mouseLeave = null;
/** This is a callback function called when click mouse down on the shape.
  * @event 
  * @param {jQuery.Event} event A jQuery Event object.
  * @param {IVectorEventData} data The data contains the vector and shape.
  */
wijvectorlayer_options.prototype.mouseDown = null;
/** This is a callback function called when click mouse up on the shape.
  * @event 
  * @param {jQuery.Event} event A jQuery Event object.
  * @param {IVectorEventData} data The data contains the vector and shape.
  */
wijvectorlayer_options.prototype.mouseUp = null;
/** This is a callback function called when move mouse on the shape.
  * @event 
  * @param {jQuery.Event} event A jQuery Event object.
  * @param {IVectorEventData} data The data contains the vector and shape.
  */
wijvectorlayer_options.prototype.mouseMove = null;
/** This is a callback function called when click mouse on the shape.
  * @event 
  * @param {jQuery.Event} event A jQuery Event object.
  * @param {IVectorEventData} data The data contains the vector and shape.
  */
wijvectorlayer_options.prototype.click = null;
wijmo.maps.wijvectorlayer.prototype.options = $.extend({}, true, wijmo.maps.wijlayer.prototype.options, new wijvectorlayer_options());
$.widget("wijmo.wijvectorlayer", $.wijmo.wijlayer, wijmo.maps.wijvectorlayer.prototype);
/** The vector object construct from the incoming data.
  * @interface IVectorObject
  * @namespace wijmo.maps
  */
wijmo.maps.IVectorObject = function () {};
/** <p>The type of the vector object. &lt;p/&gt;
  * The possible values for wijJson data are:
  * &lt;ul&gt;
  * &lt;li&gt;VectorCollection: The collection of vector items.&lt;/li&gt;
  * &lt;li&gt;Vector: The individual vector item.&lt;/li&gt;
  * &lt;/ul&gt;
  * &lt;p/&gt;
  * The possible values for geoJson data are:
  * &lt;ul&gt;
  * &lt;li&gt;Object: The vector for GeoJSON object.&lt;/li&gt;
  * &lt;li&gt;Geometry: The vector for GeoJSON Geometry object. The base of Point, MultiPoint, LineString, MultiLineString, Polygon, MultiPolygon and GeometryCollection.&lt;/li&gt;
  * &lt;li&gt;Point: The vector for GeoJSON Point object.&lt;/li&gt;
  * &lt;li&gt;MultiPoint: The vector for GeoJSON MultiPoint object.&lt;/li&gt;
  * &lt;li&gt;LineString: The vector for GeoJSON LineString object.&lt;/li&gt;
  * &lt;li&gt;MultiLineString: The vector for GeoJSON MultiLineString object.&lt;/li&gt;
  * &lt;li&gt;Polygon: The vector for GeoJSON Polygon object.&lt;/li&gt;
  * &lt;li&gt;MultiPolygon: The vector for GeoJSON MultiPolygon object.&lt;/li&gt;
  * &lt;li&gt;GeometryCollection: The vector for GeoJSON GeometryCollection object.&lt;/li&gt;
  * &lt;li&gt;Feature: The vector for GeoJSON Feature object.&lt;/li&gt;
  * &lt;li&gt;FeaturesCollectiton: The vector for GeoJSON FeaturesCollection object.&lt;/li&gt;
  * &lt;/ul&gt;</p>
  * @field 
  * @type {string}
  */
wijmo.maps.IVectorObject.prototype.type = null;
/** <p>The original data from the wijJson or geoJson.</p>
  * @field
  */
wijmo.maps.IVectorObject.prototype.data = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IVectorObject.html'>wijmo.maps.IVectorObject</a></p>
  * <p>The parent vector of this vector.</p>
  * @field 
  * @type {wijmo.maps.IVectorObject}
  */
wijmo.maps.IVectorObject.prototype.parent = null;
/** The options of the wijvectorlayer.
  * @class wijvectorlayer_options
  * @namespace wijmo.maps
  * @extends wijmo.maps.wijlayer_options
  */
wijmo.maps.wijvectorlayer_options = function () {};
wijmo.maps.wijvectorlayer_options.prototype = new wijmo.maps.wijlayer_options();
/** Defines the data object (wijJson) of the data for the vector layer.
  * @interface IWijJsonData
  * @namespace wijmo.maps
  */
wijmo.maps.IWijJsonData = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IVector.html'>wijmo.maps.IVector[]</a></p>
  * <p>The array of individual vector data.</p>
  * @field 
  * @type {wijmo.maps.IVector[]}
  */
wijmo.maps.IWijJsonData.prototype.vectors = null;
/** The data for the wijvectorlayer event handler.
  * @interface IVectorEventData
  * @namespace wijmo.maps
  */
wijmo.maps.IVectorEventData = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IVectorObject.html'>wijmo.maps.IVectorObject</a></p>
  * <p>The vector object which wraps the original data from data source.</p>
  * @field 
  * @type {wijmo.maps.IVectorObject}
  */
wijmo.maps.IVectorEventData.prototype.vector = null;
/** <p>The set of Raphael elements which will be drawn.</p>
  * @field 
  * @type {RaphaelSet}
  */
wijmo.maps.IVectorEventData.prototype.shape = null;
/** The event handler for the wijvectorlayer events.
  * @interface IVectorEvent
  * @namespace wijmo.maps
  */
wijmo.maps.IVectorEvent = function () {};
/** Defines the wijmaps vector layer.
  * @interface IVectorLayerOptions
  * @namespace wijmo.maps
  * @extends wijmo.maps.ILayerOptions
  */
wijmo.maps.IVectorLayerOptions = function () {};
/** <p>The type of the data object. The vector layer supports the following type:
  * &lt;ul&gt;
  * &lt;li&gt;wijJson: the json object with the C1 format. &lt;/li&gt;
  * &lt;li&gt;geoJson: the json object with the GeoJson format.&lt;/li&gt;
  * &lt;/ul&gt;</p>
  * @field 
  * @type {string}
  */
wijmo.maps.IVectorLayerOptions.prototype.dataType = null;
/** <p>The data of the data source.</p>
  * @field 
  * @remarks
  * The data may be the following:
  * &lt;ul&gt;
  * &lt;li&gt;string: the uri of the json data.&lt;/li&gt;
  * &lt;li&gt;function: the function to be called to get the data object.&lt;/li&gt;
  * &lt;li&gt;object: the data object.&lt;/li&gt;
  * &lt;/ul&gt;
  */
wijmo.maps.IVectorLayerOptions.prototype.data = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IPlacemark.html'>wijmo.maps.IPlacemark</a></p>
  * <p>Customize the rendering of placemark.</p>
  * @field 
  * @type {wijmo.maps.IPlacemark}
  */
wijmo.maps.IVectorLayerOptions.prototype.placemark = null;
/** <p>Specifies minimal size at which vectors become visible. The default value is 3.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.IVectorLayerOptions.prototype.minSize = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IVectorEvent.html'>wijmo.maps.IVectorEvent</a></p>
  * <p>This is a callback function called after the shape of the vector created for rendering.</p>
  * @field 
  * @type {wijmo.maps.IVectorEvent}
  */
wijmo.maps.IVectorLayerOptions.prototype.shapeCreated = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IVectorEvent.html'>wijmo.maps.IVectorEvent</a></p>
  * <p>This is a callback function called when the mouse enter the shape.</p>
  * @field 
  * @type {wijmo.maps.IVectorEvent}
  */
wijmo.maps.IVectorLayerOptions.prototype.mouseEnter = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IVectorEvent.html'>wijmo.maps.IVectorEvent</a></p>
  * <p>This is a callback function called when the mouse leave the shape.</p>
  * @field 
  * @type {wijmo.maps.IVectorEvent}
  */
wijmo.maps.IVectorLayerOptions.prototype.mouseLeave = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IVectorEvent.html'>wijmo.maps.IVectorEvent</a></p>
  * <p>This is a callback function called when click mouse down on the shape.</p>
  * @field 
  * @type {wijmo.maps.IVectorEvent}
  */
wijmo.maps.IVectorLayerOptions.prototype.mouseDown = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IVectorEvent.html'>wijmo.maps.IVectorEvent</a></p>
  * <p>This is a callback function called when click mouse up on the shape.</p>
  * @field 
  * @type {wijmo.maps.IVectorEvent}
  */
wijmo.maps.IVectorLayerOptions.prototype.mouseUp = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IVectorEvent.html'>wijmo.maps.IVectorEvent</a></p>
  * <p>This is a callback function called when move mouse on the shape.</p>
  * @field 
  * @type {wijmo.maps.IVectorEvent}
  */
wijmo.maps.IVectorLayerOptions.prototype.mouseMove = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IVectorEvent.html'>wijmo.maps.IVectorEvent</a></p>
  * <p>This is a callback function called when click mouse on the shape.</p>
  * @field 
  * @type {wijmo.maps.IVectorEvent}
  */
wijmo.maps.IVectorLayerOptions.prototype.click = null;
/** Determine how to render the placemark.
  * @interface IPlacemark
  * @namespace wijmo.maps
  */
wijmo.maps.IPlacemark = function () {};
/** <p>The image to be drawn for the mark.</p>
  * @field 
  * @type {string}
  */
wijmo.maps.IPlacemark.prototype.image = null;
/** <p>The callback functiton for drawing the mark.</p>
  * @field 
  * @type {function}
  * @remarks
  * The &lt;b&gt;image&lt;/b&gt; is ignored if it is set.
  */
wijmo.maps.IPlacemark.prototype.render = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.ISize.html'>wijmo.maps.ISize</a></p>
  * <p>The size of the mark.</p>
  * @field 
  * @type {wijmo.maps.ISize}
  * @remarks
  * The size of the image is used if not specified.
  */
wijmo.maps.IPlacemark.prototype.size = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IPoint.html'>wijmo.maps.IPoint</a></p>
  * <p>The point within the mark to pin the mark relates to the specific placemark location.</p>
  * @field 
  * @type {wijmo.maps.IPoint}
  * @remarks
  * The (0,0) is used if not specified.
  */
wijmo.maps.IPlacemark.prototype.pinPoint = null;
/** <p>Determine when the label will be visible. 
  * The possible values are:
  * &lt;ul&gt;
  * &lt;li&gt;hidden: Always not draw the label.&lt;/li&gt;
  * &lt;li&gt;autoHide: Does not draw the label if it overlaps with other labels.&lt;/li&gt;
  * &lt;li&gt;visible: Always draw the label.&lt;/li&gt;
  * &lt;/ul&gt;</p>
  * @field 
  * @type {string}
  */
wijmo.maps.IPlacemark.prototype.labelVisibility = null;
/** <p>Specifies the label position relates to the placemark location. 
  * The possible values are:
  * &lt;ul&gt;
  * &lt;li&gt;center: label is centered.&lt;/li&gt;
  * &lt;li&gt;left: label is at the left.&lt;/li&gt;
  * &lt;li&gt;right: label is at the right.&lt;/li&gt;
  * &lt;li&gt;top: label is at the top.&lt;/li&gt;
  * &lt;li&gt;bottom: label is at the bottom.&lt;/li&gt;
  * &lt;/ul&gt;</p>
  * @field 
  * @type {string}
  */
wijmo.maps.IPlacemark.prototype.labelPosition = null;
/** Defines the data for one individual vector item.
  * @interface IVector
  * @namespace wijmo.maps
  */
wijmo.maps.IVector = function () {};
/** <p>The type of the vector.
  * Possible values are:
  * &lt;ul&gt;
  * &lt;li&gt;placemark: Represents a placrmark on the map.&lt;/li&gt;
  * &lt;li&gt;polyline: Represents a polyline on the map.&lt;/li&gt;
  * &lt;li&gt;polygon: Represents a polygon on the map.&lt;/li&gt;
  * &lt;/ul&gt;</p>
  * @field 
  * @type {string}
  */
wijmo.maps.IVector.prototype.type = null;
/** <p>An array of coordinates which represents the vector item, in geographic unit (longitude and latitude).</p>
  * @field 
  * @type {number[]}
  */
wijmo.maps.IVector.prototype.coordinates = null;
/** <p>Optional. The name of the vector item.The default is empty string.</p>
  * @field 
  * @type {string}
  */
wijmo.maps.IVector.prototype.name = null;
/** <p>Optional. The label of the vector item. The default is empty string.</p>
  * @field 
  * @type {string}
  * @remarks
  * For placemark, the label text is from &lt;b&gt;label&lt;/b&gt; and then &lt;b&gt;name&lt;/b&gt;.
  */
wijmo.maps.IVector.prototype.label = null;
/** <p>Optional. The fill color of the shape. The default is "transparent".</p>
  * @field 
  * @type {string}
  */
wijmo.maps.IVector.prototype.fill = null;
/** <p>Optional. The fill opacity of the shape, from 0 to 1.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.IVector.prototype.fillOpacity = null;
/** <p>Opitional. The stroke color of the shape. The default is "#000".</p>
  * @field 
  * @type {string}
  */
wijmo.maps.IVector.prototype.stroke = null;
/** <p>Optional. The stroke opacity of the shape, from 0 to 1.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.IVector.prototype.strokeOpacity = null;
/** <p>Optional. The stroke width of the shape, in screen unit (pixel). The default is 1.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.IVector.prototype.strokeWidth = null;
/** <p>Optional. The arrary to define the stroke dash style. The default is empty string.</p>
  * @field 
  * @type {string}
  * @example
  * [“”, “-”, “.”, “-.”, “-..”, “. ”, “- ”, “--”, “- .”, “--.”, “--..”]
  */
wijmo.maps.IVector.prototype.strokeDashArray = null;
/** <p>Optional. The shape at the end of the line. 
  * Possible values are:
  * &lt;ul&gt;
  * &lt;li&gt;butt&lt;/li&gt;
  * &lt;li&gt;square&lt;/li&gt;
  * &lt;li&gt;round&lt;/li&gt;
  * &lt;/ul&gt;
  * The default is "butt".</p>
  * @field 
  * @type {string}
  */
wijmo.maps.IVector.prototype.strokeLineCap = null;
/** <p>Optional. The shape that joins two lines. 
  * Possible values are:
  * &lt;ul&gt;
  * &lt;li&gt;bevel&lt;/li&gt;
  * &lt;li&gt;round&lt;/li&gt;
  * &lt;li&gt;miter&lt;/li&gt;
  * &lt;/ul&gt;
  * The default is "bevel".</p>
  * @field 
  * @type {string}
  */
wijmo.maps.IVector.prototype.strokeLineJoin = null;
/** <p>The limit on the ratio of the miter length to half the stroke thickness of the shape. 
  * This value is always a positive number that is greater than or equal to 1.
  * The default is 1.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.IVector.prototype.strokeMiterLimit = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.ILOD.html'>wijmo.maps.ILOD</a></p>
  * <p>Optional. The level of details of the vector.</p>
  * @field 
  * @type {wijmo.maps.ILOD}
  */
wijmo.maps.IVector.prototype.lod = null;
typeof wijmo.maps.ILayerOptions != 'undefined' && $.extend(wijmo.maps.IVectorLayerOptions.prototype, wijmo.maps.ILayerOptions.prototype);
})()
})()
