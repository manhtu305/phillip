var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
/** An object that represents selection in the grid. You do not need to create instances of this class.
  * @class selection
  * @namespace wijmo.grid
  */
wijmo.grid.selection = function () {};
/** Gets a read-only collection of the selected cells.
  * @returns {wijmo.grid.cellInfoOrderedCollection} A read-only collection of the selected cells.
  * @example
  * var selectedCells = selectionObj.selectedCells();
  * for (var i = 0, len = selectedCells.length(); i &lt; len; i++) {
  * alert(selectedCells.item(i).value().toString());
  * }
  */
wijmo.grid.selection.prototype.selectedCells = function () {};
/** Gets a read-only collection of the selected rows.
  * @returns {wijmo.grid.selectedRowsAccessor} A read-only collection of the selected rows.
  * @example
  * alert(selectionObj.selectedRows().length());
  */
wijmo.grid.selection.prototype.selectedRows = function () {};
/** Adds a column range to the current selection.
  * Usage:
  * 1. addColumns(0)
  * 2. addColumns(0, 2)
  * @param {number} start The index of the first column to select.
  * @param {number} end The index of the last column to select.
  * @returns {void}
  * @remarks
  * The result depends upon the chosen selection mode in the grid. For example, if current selection mode does not allow multiple selection the previous selection will be removed.
  * @example
  * // Add the first column to the current selection.
  * selectionObj.addColumns(0);
  */
wijmo.grid.selection.prototype.addColumns = function (start, end) {};
/** Adds a cell range to the current selection.
  * @param {wijmo.grid.cellInfoRange} cellRange Cell range to select.
  * @returns {void}
  * @remarks
  * The result depends upon the chosen selection mode in the grid. For example, if current selection mode does not allow multiple selection the previous selection will be removed.
  * @example
  * // Add a cell range (0,0)-(1,1) to the selection
  * selectionObj.addRange(new wijmo.grid.cellInfoRange(0, 0, 1, 1));
  */
wijmo.grid.selection.prototype.addRange = function (cellRange) {};
/** Adds a cell range to the current selection.
  * @param {number} x0 The x-coordinate that represents the top left cell of the range.
  * @param {number} y0 The y-coordinate that represents the top left cell of the range.
  * @param {number} x1 The x-coordinate that represents the bottom right cell of the range.
  * @param {number} y1 The y-coordinate that represents the bottom right cell of the range.
  * @returns {void}
  * @remarks
  * The result depends upon the chosen selection mode in the grid. For example, if current selection mode does not allow multiple selection the previous selection will be removed.
  * @example
  * // Add a cell range (0,0)-(1,1) to the selection
  * selectionObj.addRange(0, 0, 1, 1);
  */
wijmo.grid.selection.prototype.addRange = function (x0, y0, x1, y1) {};
/** Adds a row range to the current selection.
  * Usage:
  * 1. addRows(0)
  * 2. addRows(0, 2)
  * @param {number} start The index of the first row to select.
  * @param {number} end The index of the last row to select.
  * @returns {void}
  * @remarks
  * The result depends upon the chosen selection mode in the grid. For example, if current selection mode does not allow multiple selection the previous selection will be removed.
  * @example
  * // Add the first row to the selection.
  * selectionObj.addRows(0);
  */
wijmo.grid.selection.prototype.addRows = function (start, end) {};
/** Removes a range of cells from the current selection.
  * @param {wijmo.grid.cellInfoRange} cellRange Cell range to remove.
  * @returns {void}
  * @remarks
  * The result depends upon the chosen selection mode in the grid.
  * @example
  * // Remove a cell range (0,0)-(1,1) from the selection
  * selectionObj.removeRange(new wijmo.grid.cellRange(0, 0, 1, 1));
  */
wijmo.grid.selection.prototype.removeRange = function (cellRange) {};
/** Removes a range of cells from the current selection.
  * @param {wijmo.grid.cellInfoRange} cellRange Cell range to remove.
  * @param {number} x0 The x-coordinate that represents the top left cell of the range.
  * @param {number} y0 The y-coordinate that represents the top left cell of the range.
  * @param {number} x1 The x-coordinate that represents the bottom right cell of the range.
  * @param {number} y1 The y-coordinate that represents the bottom right cell of the range.
  * @returns {void}
  * @example
  * // Remove a cell range (0,0)-(1,1) from the selection
  * selectionObj.removeRange(0, 0, 1, 1);
  */
wijmo.grid.selection.prototype.removeRange = function (cellRange, x0, y0, x1, y1) {};
/** Removes a range of columns from the current selection.
  * Usage:
  * 1. removeColumns(0)
  * 2. removeColumns(0, 2)
  * @param {number} start The index of the first column to remove.
  * @param {number} end The index of the last column to remove.
  * @returns {void}
  * @remarks
  * The result depends upon the chosen selection mode in the grid.
  * @example
  * // Remove the first columm from the selection.
  * selectionObj.removeColumns(0);
  */
wijmo.grid.selection.prototype.removeColumns = function (start, end) {};
/** Removes a range of rows from the current selection.
  * Usage:
  * 1. removeRows(0)
  * 2. removeRows(0, 2)
  * @param {number} start The index of the first row to remove.
  * @param {number} end The index of the last row to remove.
  * @returns {void}
  * @remarks
  * The result depends upon the chosen selection mode in the grid.
  * @example
  * // Remove the first row from the selection.
  * selectionObj.removeRows(0);
  */
wijmo.grid.selection.prototype.removeRows = function (start, end) {};
/** Clears the selection.
  * @returns {void}
  * @example
  * // Clear the selection.
  * selectionObj.clear();
  */
wijmo.grid.selection.prototype.clear = function () {};
/** Selects all the cells in a grid.
  * @returns {void}
  * @remarks
  * The result depends upon the chosen selection mode in the grid. For example, if the selection mode is set to "singleCell", then only the top left cell will be selected.
  * @example
  * selectionObj.selectAll();
  */
wijmo.grid.selection.prototype.selectAll = function () {};
/** Begins the update. Any changes won't take effect until endUpdate() is called.
  * @returns {void}
  * @example
  * selectionObj.beginUpdate();
  */
wijmo.grid.selection.prototype.beginUpdate = function () {};
/** Ends the update. The pending changes are executed and the selectionChanged event is raised.
  * @param {JQueryEventObject} e The original event object passed through by wijgrid.
  * @returns {void}
  * @example
  * selectionObj.endUpdate();
  */
wijmo.grid.selection.prototype.endUpdate = function (e) {};
/**  */
wijmo.grid.selection.prototype.doSelection = function () {};
/** @param {wijmo.grid.cellInfo} cellInfo
  * @param {wijmo.grid.IRowInfo} rowInfo
  * @param {wijmo.grid.baseView} view
  * @param {bool} select
  */
wijmo.grid.selection.prototype.selectCell = function (cellInfo, rowInfo, view, select) {};
/** @param {wijmo.grid.cellInfoRange} range
  * @param {bool} add
  */
wijmo.grid.selection.prototype.doRange = function (range, add) {};
/** An ordered read-only collection of wijmo.grid.cellInfo objects
  * @class cellInfoOrderedCollection
  * @namespace wijmo.grid
  */
wijmo.grid.cellInfoOrderedCollection = function () {};
/** Gets an item at the specified index.
  * @param {number} index The zero-based index of the item to get.
  * @returns {wijmo.grid.cellInfo} The wijmo.grid.cellInfo object at the specified index.
  * @example
  * var cellInfoObj = collection.item(0);
  */
wijmo.grid.cellInfoOrderedCollection.prototype.item = function (index) {};
/** Gets the total number of the items in the collection.
  * @returns {number} The total number of the items in the collection.
  * @example
  * var len = collection.length();
  */
wijmo.grid.cellInfoOrderedCollection.prototype.length = function () {};
/** Returns the zero-based index of specified collection item.
  * @param {wijmo.grid.cellInfo} cellInfo A cellInfo object to return the index of.
  * @returns {number} The zero-based index of the specified object, or -1 if the specified object is not a member of the collection.
  * @example
  * var index = collection.indexOf(new wijmo.grid.cellInfo(0, 0));
  */
wijmo.grid.cellInfoOrderedCollection.prototype.indexOf = function (cellInfo) {};
/** Returns the zero-based index of specified collection item.
  * @param {number} cellIndex A zero-based cellIndex component of the wijmo.grid.cellInfo object to return the index of.
  * @param {number} rowIndex A zero-based rowIndex component of the wijmo.grid.cellInfo object to return the index of.
  * @returns {number} The zero-based index of the specified object, or -1 if the specified object is not a member of the collection.
  * @example
  * var index = collection.indexOf(0, 0);
  */
wijmo.grid.cellInfoOrderedCollection.prototype.indexOf = function (cellIndex, rowIndex) {};
/** Provides a read-only access to rows which cells are selected.
  * @class selectedRowsAccessor
  * @namespace wijmo.grid
  */
wijmo.grid.selectedRowsAccessor = function () {};
/** Gets the total number of the items in the collection.
  * @returns {number} The total number of the items in the collection.
  * @example
  * var len = $("#demo").wijgrid("selection").selectedRows().length();
  */
wijmo.grid.selectedRowsAccessor.prototype.length = function () {};
/** Gets an object representing grid's row at the specified index.
  * @param {number} index The zero-based index of the item to get.
  * @returns {wijmo.grid.IRowInfo} An object representing grid's row at the specified index.
  * @example
  * var row = $("#demo").wijgrid("selection").selectedRows().item(0);
  */
wijmo.grid.selectedRowsAccessor.prototype.item = function (index) {};
})()
})()
