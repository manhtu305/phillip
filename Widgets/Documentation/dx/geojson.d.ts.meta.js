var GeoJSON = GeoJSON || {};

(function () {
/** http://geojson.org/geojson-spec.html#geojson-objects
  * @interface GeoJsonObject
  * @namespace GeoJSON
  */
GeoJSON.GeoJsonObject = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
GeoJSON.GeoJsonObject.prototype.type = null;
/** <p>undefined</p>
  * @field 
  * @type {number[]}
  */
GeoJSON.GeoJsonObject.prototype.bbox = null;
/** <p>undefined</p>
  * @field 
  * @type {GeoJSON.CoordinateReferenceSystem}
  */
GeoJSON.GeoJsonObject.prototype.crs = null;
/** http://geojson.org/geojson-spec.html#positions
  * @interface Position
  * @namespace GeoJSON
  */
GeoJSON.Position = function () {};
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
GeoJSON.Position.prototype.length = null;
/** http://geojson.org/geojson-spec.html#geometry-objects
  * @interface GeometryObject
  * @namespace GeoJSON
  * @extends GeoJSON.GeoJsonObject
  */
GeoJSON.GeometryObject = function () {};
/** <p>undefined</p>
  * @field
  */
GeoJSON.GeometryObject.prototype.coordinates = null;
/** http://geojson.org/geojson-spec.html#point
  * @interface Point
  * @namespace GeoJSON
  * @extends GeoJSON.GeometryObject
  */
GeoJSON.Point = function () {};
/** <p>undefined</p>
  * @field 
  * @type {GeoJSON.Position}
  */
GeoJSON.Point.prototype.coordinates = null;
/** http://geojson.org/geojson-spec.html#multipoint
  * @interface MultiPoint
  * @namespace GeoJSON
  * @extends GeoJSON.GeometryObject
  */
GeoJSON.MultiPoint = function () {};
/** <p>undefined</p>
  * @field 
  * @type {GeoJSON.Position[]}
  */
GeoJSON.MultiPoint.prototype.coordinates = null;
/** http://geojson.org/geojson-spec.html#linestring
  * @interface LineString
  * @namespace GeoJSON
  * @extends GeoJSON.GeometryObject
  */
GeoJSON.LineString = function () {};
/** <p>undefined</p>
  * @field 
  * @type {GeoJSON.Position[]}
  */
GeoJSON.LineString.prototype.coordinates = null;
/** http://geojson.org/geojson-spec.html#multilinestring
  * @interface MultiLineString
  * @namespace GeoJSON
  * @extends GeoJSON.GeometryObject
  */
GeoJSON.MultiLineString = function () {};
/** <p>undefined</p>
  * @field 
  * @type {GeoJSON.Position[][]}
  */
GeoJSON.MultiLineString.prototype.coordinates = null;
/** http://geojson.org/geojson-spec.html#polygon
  * @interface Polygon
  * @namespace GeoJSON
  * @extends GeoJSON.GeometryObject
  */
GeoJSON.Polygon = function () {};
/** <p>undefined</p>
  * @field 
  * @type {GeoJSON.Position[][]}
  */
GeoJSON.Polygon.prototype.coordinates = null;
/** http://geojson.org/geojson-spec.html#multipolygon
  * @interface MultiPolygon
  * @namespace GeoJSON
  * @extends GeoJSON.GeometryObject
  */
GeoJSON.MultiPolygon = function () {};
/** <p>undefined</p>
  * @field 
  * @type {GeoJSON.Position[][][]}
  */
GeoJSON.MultiPolygon.prototype.coordinates = null;
/** http://geojson.org/geojson-spec.html#geometry-collection
  * @interface GeometryCollection
  * @namespace GeoJSON
  * @extends GeoJSON.GeoJsonObject
  */
GeoJSON.GeometryCollection = function () {};
/** <p>undefined</p>
  * @field 
  * @type {GeoJSON.GeometryObject[]}
  */
GeoJSON.GeometryCollection.prototype.geometries = null;
/** http://geojson.org/geojson-spec.html#feature-objects
  * @interface Feature
  * @namespace GeoJSON
  * @extends GeoJSON.GeoJsonObject
  */
GeoJSON.Feature = function () {};
/** <p>undefined</p>
  * @field 
  * @type {GeoJSON.GeometryObject}
  */
GeoJSON.Feature.prototype.geometry = null;
/** <p>undefined</p>
  * @field
  */
GeoJSON.Feature.prototype.properties = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
GeoJSON.Feature.prototype.id = null;
/** http://geojson.org/geojson-spec.html#feature-collection-objects
  * @interface FeatureCollection
  * @namespace GeoJSON
  * @extends GeoJSON.GeoJsonObject
  */
GeoJSON.FeatureCollection = function () {};
/** <p>undefined</p>
  * @field 
  * @type {GeoJSON.Feature[]}
  */
GeoJSON.FeatureCollection.prototype.features = null;
/** http://geojson.org/geojson-spec.html#coordinate-reference-system-objects
  * @interface CoordinateReferenceSystem
  * @namespace GeoJSON
  */
GeoJSON.CoordinateReferenceSystem = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
GeoJSON.CoordinateReferenceSystem.prototype.type = null;
/** <p>undefined</p>
  * @field
  */
GeoJSON.CoordinateReferenceSystem.prototype.properties = null;
/** @interface NamedCoordinateReferenceSystem
  * @namespace GeoJSON
  * @extends GeoJSON.CoordinateReferenceSystem
  */
GeoJSON.NamedCoordinateReferenceSystem = function () {};
/** <p>undefined</p>
  * @field
  */
GeoJSON.NamedCoordinateReferenceSystem.prototype.properties = null;
/** @interface LinkedCoordinateReferenceSystem
  * @namespace GeoJSON
  * @extends GeoJSON.CoordinateReferenceSystem
  */
GeoJSON.LinkedCoordinateReferenceSystem = function () {};
/** <p>undefined</p>
  * @field
  */
GeoJSON.LinkedCoordinateReferenceSystem.prototype.properties = null;
typeof GeoJSON.GeoJsonObject != 'undefined' && $.extend(GeoJSON.GeometryObject.prototype, GeoJSON.GeoJsonObject.prototype);
typeof GeoJSON.GeometryObject != 'undefined' && $.extend(GeoJSON.Point.prototype, GeoJSON.GeometryObject.prototype);
typeof GeoJSON.GeometryObject != 'undefined' && $.extend(GeoJSON.MultiPoint.prototype, GeoJSON.GeometryObject.prototype);
typeof GeoJSON.GeometryObject != 'undefined' && $.extend(GeoJSON.LineString.prototype, GeoJSON.GeometryObject.prototype);
typeof GeoJSON.GeometryObject != 'undefined' && $.extend(GeoJSON.MultiLineString.prototype, GeoJSON.GeometryObject.prototype);
typeof GeoJSON.GeometryObject != 'undefined' && $.extend(GeoJSON.Polygon.prototype, GeoJSON.GeometryObject.prototype);
typeof GeoJSON.GeometryObject != 'undefined' && $.extend(GeoJSON.MultiPolygon.prototype, GeoJSON.GeometryObject.prototype);
typeof GeoJSON.GeoJsonObject != 'undefined' && $.extend(GeoJSON.GeometryCollection.prototype, GeoJSON.GeoJsonObject.prototype);
typeof GeoJSON.GeoJsonObject != 'undefined' && $.extend(GeoJSON.Feature.prototype, GeoJSON.GeoJsonObject.prototype);
typeof GeoJSON.GeoJsonObject != 'undefined' && $.extend(GeoJSON.FeatureCollection.prototype, GeoJSON.GeoJsonObject.prototype);
typeof GeoJSON.CoordinateReferenceSystem != 'undefined' && $.extend(GeoJSON.NamedCoordinateReferenceSystem.prototype, GeoJSON.CoordinateReferenceSystem.prototype);
typeof GeoJSON.CoordinateReferenceSystem != 'undefined' && $.extend(GeoJSON.LinkedCoordinateReferenceSystem.prototype, GeoJSON.CoordinateReferenceSystem.prototype);
})()
