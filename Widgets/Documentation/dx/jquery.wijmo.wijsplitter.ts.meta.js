var wijmo = wijmo || {};

(function () {
wijmo.splitter = wijmo.splitter || {};

(function () {
/** @class wijsplitter
  * @widget 
  * @namespace jQuery.wijmo.splitter
  * @extends wijmo.wijmoWidget
  */
wijmo.splitter.wijsplitter = function () {};
wijmo.splitter.wijsplitter.prototype = new wijmo.wijmoWidget();
/** Removes the splitter functionality completely.This will return the element back to its pre - init state.
  * @example
  * $("selector").wijsplitter("destroy");
  */
wijmo.splitter.wijsplitter.prototype.destroy = function () {};
/** Forces the widget to recreate the splitter.
  * @param {bool} size A boolean value to indicate whether the refresh is triggered
  * because the size of widget is changed.
  * @param {bool} state A boolean value to indicate whether the refresh is triggered 
  * because the state of expander is changed(expanded/collapsed).
  */
wijmo.splitter.wijsplitter.prototype.refresh = function (size, state) {};

/** @class */
var wijsplitter_options = function () {};
/** <p class='defaultValue'>Default value: -1</p>
  * <p>A value that indicates the z-index (stack order) of the splitter bar.</p>
  * @field 
  * @type {number}
  * @option
  */
wijsplitter_options.prototype.barZIndex = -1;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A Boolean value that determines whether the expander of the wijsplitter widget is shown.
  * is allowed to be shown.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijsplitter_options.prototype.showExpander = true;
/** <p class='defaultValue'>Default value: 100</p>
  * <p>Gets or sets the location of the splitter, in pixels, from the left or top edge of the SplitContainer.</p>
  * @field 
  * @type {number}
  * @option
  */
wijsplitter_options.prototype.splitterDistance = 100;
/** <p class='defaultValue'>Default value: 'vertical'</p>
  * <p>Gets or sets a value indicating the horizontal or vertical orientation of the SplitContainer panels
  * of the splitter panels.</p>
  * @field 
  * @type {string}
  * @option
  */
wijsplitter_options.prototype.orientation = 'vertical';
/** <p class='defaultValue'>Default value: false</p>
  * <p>Gets or sets a value that indicates whether the widget fills the whole page.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijsplitter_options.prototype.fullSplit = false;
/** <p>Defines the animation while the bar of the splitter is being dragged.</p>
  * @field 
  * @type {object}
  * @option
  */
wijsplitter_options.prototype.resizeSettings = null;
/** <p>Defines the information for the top or left panel of the splitter.</p>
  * @field 
  * @type {object}
  * @option
  */
wijsplitter_options.prototype.panel1 = null;
/** <p>Defines the information for the bottom or right panel of the splitter.</p>
  * @field 
  * @type {object}
  * @option
  */
wijsplitter_options.prototype.panel2 = null;
/** <p class='defaultValue'>Default value: 'panel1'</p>
  * <p>Specifies which panel should be collapsed after clicking the expander of the splitter. Possible values are "panel1" and "panel2".</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $('.selector' ).wijsplitter({collapsingPanel: "panel1"});
  */
wijsplitter_options.prototype.collapsingPanel = 'panel1';
/** Gets or sets the javascript function name that 
  * would be called at the client side when a user is dragging the splitter.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @example
  * Supply a callback function to handle the sizing event:
  * $("#element").wijsplitter({ sizing: function () { } });
  * Bind to the event by type:
  * $("#element").bind("wijsplittersizing", function () { });
  */
wijsplitter_options.prototype.sizing = null;
/** Gets or sets the javascript function name that 
  * would be called at the client side when the user is done dragging the splitter.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @example
  * Supply a callback function to handle the sized event:
  * $("#element").wijsplitter({ sized: function () { } });
  * Bind to the event by type:
  * $("#element").bind("wijsplittersized", function () { });
  */
wijsplitter_options.prototype.sized = null;
/** Gets or sets the javascript function name to be called before panel1 is expanded.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @example
  * Supply a callback function to handle the expand event:
  * $("#element").wijsplitter({ expand: function () { return false; } });
  * Bind to the event by type:
  * $("#element").bind("wijsplitterexpand", function () { return false; });
  */
wijsplitter_options.prototype.expand = null;
/** Gets or sets the javascript function name to be called before panel1 is collapsed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @example
  * Supply a callback function to handle the collapse event:
  * $("#element").wijsplitter({ collapse: function () { return false; } });
  * Bind to the event by type:
  * $("#element").bind("wijsplittercollapse", function () { return false; });
  */
wijsplitter_options.prototype.collapse = null;
/** Gets or sets the javascript function name to be called when panel1 is expanded by clicking the collapse/expand image.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @example
  * Supply a callback function to handle the expanded event:
  * $("#element").wijsplitter({ expanded: function () { } });
  * Bind to the event by type:
  * $("#element").bind("wijsplitterexpanded", function () { });
  */
wijsplitter_options.prototype.expanded = null;
/** Gets or sets the javascript function name to be called when panel1 is collapsed by clicking the collapse/expand image.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @example
  * Supply a callback function to handle the collapsed event:
  * $("#element").wijsplitter({ collapsed: function () { } });
  * Bind to the event by type:
  * $("#element").bind("wijsplittercollapsed", function () { });
  */
wijsplitter_options.prototype.collapsed = null;
wijmo.splitter.wijsplitter.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijsplitter_options());
$.widget("wijmo.wijsplitter", $.wijmo.widget, wijmo.splitter.wijsplitter.prototype);
/** @interface panel_option
  * @namespace wijmo.splitter
  */
wijmo.splitter.panel_option = function () {};
/** <p>Gets or sets the minimum distance in pixels when
  * resizing the splitter.</p>
  * @field 
  * @type {number}
  */
wijmo.splitter.panel_option.prototype.minSize = null;
/** <p>Gets or sets a value determining whether splitter panel is 
  * collapsed or expanded.</p>
  * @field 
  * @type {Boolean}
  */
wijmo.splitter.panel_option.prototype.collapsed = null;
/** <p>Gets or sets the type of scroll bars to display 
  * for splitter panel.</p>
  * @field 
  * @type {string}
  */
wijmo.splitter.panel_option.prototype.scrollBars = null;
})()
})()
