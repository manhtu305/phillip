var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
/** @class wijgrid
  * @widget 
  * @namespace jQuery.wijmo.grid
  * @extends wijmo.wijmoWidget
  */
wijmo.grid.wijgrid = function () {};
wijmo.grid.wijgrid.prototype = new wijmo.wijmoWidget();
/** Returns a one-dimensional array of widgets bound to visible column headers.
  * @returns {Object[]} A one-dimensional array of widgets bound to visible column headers.
  * @remarks
  * wijgrid columns are represented as widgets. This method returns a one-dimensional array of widgets that are bound to visible column headers.
  * The column widget is initiated with values taken from the corresponding item in the wijgrid.options.columns array. However, the options of a column widget instance reference not the original object but a copy created by the widget factory. Due to that, changes to the wijgrid.options.columns options are not automatically propagated to the column widget options and vice versa.
  * To solve this issue, the wijgrid synchronized the column widget option values with the source items. This synchronization occurs inside the ensureControl() method which is automatically called at each action requiring the wijgrid to enter.
  * Still, there is a drawback. For example, a user may want to filter wijgrid data from user code as in this sample:
  * $("#element").wijgrid("option", "columns")[0].filterValue = "newValue";
  * $("#element").wijgrid("ensureControl", true); // make wijgrid re-shape data and re-render.
  * In the sample above, nothing will happen since at synchronization user changes will be ignored.You need to change the filterValue of a column widget. This is what the columns() method is for:
  * $("#element").wijgrid("columns")[0].options.filterValue = "newValue";
  * $("#element").wijgrid("ensureControl", true); // make wijgrid re-shape data and re-render.
  * Here's the best way to change the filterValue:
  * $("#element").wijgrid("columns")[0].option("filterValue", "newValue"); // column widget handles all the needful.
  * @example
  * var colWidgets = $("#element").wijgrid("columns");
  */
wijmo.grid.wijgrid.prototype.columns = function () {};
/** Gets the current cell for the grid.
  * @returns {wijmo.grid.cellInfo} Object that represents current cell of the grid.
  * @example
  * var current = $("#element).wijgrid("currentCell");
  */
wijmo.grid.wijgrid.prototype.currentCell = function () {};
/** Sets the current cell for the grid.
  * @param {wijmo.grid.cellInfo} cellInfo Object that represents a single cell.
  * @returns {wijmo.grid.cellInfo} Object that represents current cell of the grid.
  * @remarks
  * If you wish to hide the current cell, use (-1, -1) for the value.
  * @example
  * $("#element).wijgrid("currentCell", new wijmo.grid.cellInfo(0, 0));
  */
wijmo.grid.wijgrid.prototype.currentCell = function (cellInfo) {};
/** Sets the current cell for the grid.
  * @param {number} cellIndex Zero-based index of the required cell inside the corresponding row.
  * @param {number} rowIndex Zero-based index of the row that contains required cell.
  * @returns {wijmo.grid.cellInfo} Object that represents current cell of the grid.
  * @remarks
  * If you wish to hide the current cell, use (-1, -1) for the value.
  * @example
  * $("#element).wijgrid("currentCell", 0, 0);
  */
wijmo.grid.wijgrid.prototype.currentCell = function (cellIndex, rowIndex) {};
/** Gets an array of underlying data.
  * @returns {object[]} An array of underlying data.
  * @example
  * var data = $("#element").wijgrid("data");
  */
wijmo.grid.wijgrid.prototype.data = function () {};
/** Gets an underlying wijdataview instance.
  * @returns {wijmo.data.IDataView} An underlying wijdataview instance.
  * @example
  * var dataView = $("#element").wijgrid("dataView");
  */
wijmo.grid.wijgrid.prototype.dataView = function () {};
/** Returns an array which consists of hierarchy nodes in wijgrid.
  * @returns {wijmo.grid.hierarchyNode[]}
  */
wijmo.grid.wijgrid.prototype.details = function () {};
/** Re-renders wijgrid.
  * @param {Object} userData Infrastructure, not intended to be used by user.
  * @returns {void}
  * @example
  * $("#element").wijgrid("doRefresh");
  */
wijmo.grid.wijgrid.prototype.doRefresh = function (userData) {};
/** Puts the current cell into edit mode, as long as the editingMode options is set to "cell".
  * @returns {bool} True if the cell is successfully put into edit mode, otherwise false.
  * @example
  * $("#element").wijgrid({}
  *     editingMode: "cell",
  *     currentCellChanged: function (e, args) {
  *         if ($(e.target).wijgrid("option", "isLoaded")) {
  *             window.setTimeout(function () {
  *                 $(e.target).wijgrid("beginEdit");
  *             }, 100);
  *         }
  *     }
  * });
  */
wijmo.grid.wijgrid.prototype.beginEdit = function () {};
/** Finishes editing the current cell.
  * @param {bool} reject An optional parameter indicating whether changes will be rejected (true) or commited (false). The default value is false.
  * @returns {bool} True if the editing was finished successfully, othewise false.
  * @example
  * // endEdit is being called from within the saveChanges function
  * function saveChanges() {
  *     $("#element").wijgrid("endEdit");
  * }
  * functon cancelChanges() {
  *     $("#element").wijgrid("endEdit", true);
  * }
  */
wijmo.grid.wijgrid.prototype.endEdit = function (reject) {};
/** Starts editing of the specified row, can only be used when the editingMode option is set to "row".
  * @param {number} dataItemIndex Determines the data item to edit.
  * @returns {void}
  * @example
  * $("#element").wijgrid("editRow", 0);
  */
wijmo.grid.wijgrid.prototype.editRow = function (dataItemIndex) {};
/** Exports the grid to a specified format. 
  * The export method only works when wijmo.exporter.gridExport's reference is on the page.
  * @param {string|Object} exportSettings 1.The name of the exported file.
  * 2.Settings of exporting, should be conformed to wijmo.exporter.GridExportSetting
  * @param {string} type The type of the exported file.
  * @param {object} settings The export setting.
  * @param {string} serviceUrl The export service url.
  * @remarks
  * Possible exported types are: xls, xlsx, csv, pdf,
  * @example
  * $("#element").wijgrid("exportGrid", "grid", "csv");
  */
wijmo.grid.wijgrid.prototype.exportGrid = function (exportSettings, type, settings, serviceUrl) {};
/** Finishes editing and updates the datasource.
  * @returns {void}
  * @example
  * $("#element").wijgrid("updateRow");
  */
wijmo.grid.wijgrid.prototype.updateRow = function () {};
/** Discards changes and finishes editing of the edited row.
  * @returns {void}
  * @example
  * $("#element").wijgrid("cancelRowEditing");
  */
wijmo.grid.wijgrid.prototype.cancelRowEditing = function () {};
/** Deletes the specified row.
  * @param {number} dataItemIndex Determines the data item to edit.
  * @returns {void}
  * @example
  * $("#element").wijgrid("cancelRowEditing");
  */
wijmo.grid.wijgrid.prototype.deleteRow = function (dataItemIndex) {};
/** Moves the column widget options to the wijgrid options and renders the wijgrid. Use this method when you need to re-render the wijgrid and reload remote data from the datasource.
  * @param {bool} loadData Determines if the wijgrid must load data from a linked data source before rendering.
  * @returns {void}
  * @example
  * // Adds a new row to the viewModel and refreshes the wijgrid
  * var len = viewModel.data().length;
  * viewModel.data.push(new Person({ ID: len, Company: "New Company" + len, Name: "New Name" + len }));
  * $("#element").wijgrid("ensureControl", true);
  */
wijmo.grid.wijgrid.prototype.ensureControl = function (loadData) {};
/** Gets an instance of the wijmo.grid.cellInfo class that represents the grid's specified cell.
  * @param {Object} domCell A HTML DOM Table cell object
  * @returns {wijmo.grid.cellInfo} Object that represents a cell of the grid.
  * @example
  * var cellInfo = $("#element").wijgrid("getCellInfo", domCell);
  */
wijmo.grid.wijgrid.prototype.getCellInfo = function (domCell) {};
/** Returns a one-dimensional array of filter operators which are applicable to the specified data type.
  * @param {string} dataType Specifies the type of data to which you apply the filter operators. Possible values are: "string", "number", "datetime", "currency" and "boolean".
  * @returns {wijmo.grid.IFilterOperator[]} A one-dimensional array of filter operators.
  * @example
  * var operators = $("#element").wijgrid("getFilterOperatorsByDataType", "string");
  */
wijmo.grid.wijgrid.prototype.getFilterOperatorsByDataType = function (dataType) {};
/** Gets the number of pages.
  * @returns {number} The number of pages.
  * @example
  * var pageCount = $("#element").wijgrid("pageCount");
  */
wijmo.grid.wijgrid.prototype.pageCount = function () {};
/** Sets the size of the grid using the width and height parameters.
  * @param {String|Number} width Determines the width of the grid.
  * @param {String|Number} height Determines the height of the grid.
  * @returns {void}
  * @example
  * $("#element").wijgrid("setSize", 200, 200);
  */
wijmo.grid.wijgrid.prototype.setSize = function (width, height) {};
/** Gets an object that manages selection in the grid.
  * @returns {wijmo.grid.selection} Object that manages selection in the grid.
  * @remarks
  * See the description of the wijmo.grid.selection class for more details.
  * @example
  * // Use the row index to add the row to the selection object
  * var selection = $("#element").wijgrid("selection");
  * selection.addRows(2);
  */
wijmo.grid.wijgrid.prototype.selection = function () {};

/** @class */
var wijgrid_options = function () {};
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value indicating whether columns can be moved.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * This option must be set to true in order to drag column headers to the group area.
  * @example
  * // Columns cannot be dragged and moved if this option is set to false
  * $("#element").wijgrid({ allowColMoving: false });
  */
wijgrid_options.prototype.allowColMoving = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether the column width can be increased and decreased by dragging the sizing handle, or the edge of the column header, with the mouse.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * // The sizing handle cannot be dragged and column width cannot be changed if this option is set to false
  * $("#element").wijgrid({ allowColSizing: false });
  */
wijgrid_options.prototype.allowColSizing = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether the user can make changes to cell contents in the grid.
  * This option is obsolete. Use the editingMode option instead.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * // Users cannot change cell contents in the grid if this option is set to false
  * $("#element").wijgrid({ allowEditing: false });
  */
wijgrid_options.prototype.allowEditing = false;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether the user can move the current cell using the arrow keys.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * // Users cannot move the selection using arrow keys if this option is set to false
  * $("#element").wijgrid({ allowKeyboardNavigation: false });
  */
wijgrid_options.prototype.allowKeyboardNavigation = true;
/** <p class='defaultValue'>Default value: 'moveAcross'</p>
  * <p>Determines the action to be performed when the user presses the TAB key.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * This option is invalid when the allowKeyboardNavigation is set to false.
  * Possible values are:
  * "moveAcross": The focus will be kept inside the grid and current selected cell will move cyclically between grid cells when user press TAB or SHIFT+TAB key.
  * "moveAcrossOut": The focus will be able to be moved from the grid to the next focusable element in the tab order when user press TAB key and the current selected cell is the last cell (or press SHIFT+TAB and the current selected cell is the first cell).
  * @example
  * $("#element").wijgrid({ keyActionTab: "moveAcross" });
  */
wijgrid_options.prototype.keyActionTab = 'moveAcross';
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether the grid should display paging buttons. The number of rows on a page is determined by the pageSize option.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * // Grid displays paging buttons when allowPaging is true. The pageSize here sets 5 rows to a page.
  * $("#element").wijgrid({ allowPaging: false, pageSize: 5 });
  */
wijgrid_options.prototype.allowPaging = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether the widget can be sorted by clicking the column header.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * // Sort a column by clicking its header when allowSorting is set to true
  * $("#element").wijgrid({ allowSorting: false });
  */
wijgrid_options.prototype.allowSorting = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that indicates whether virtual scrolling is allowed. Set allowVirtualScrolling to true when using large amounts of data to improve efficiency.
  * Obsoleted, set the scrollingSettings.virtualization.mode property to "rows" instead.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * This option is ignored if the grid uses paging, columns merging or fixed rows. This option cannot be enabled when using dynamic wijdatasource.
  * @example
  * $("#element").wijgrid({ allowVirtualScrolling: false });
  */
wijgrid_options.prototype.allowVirtualScrolling = false;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates calendar's options in grid. It works for calendar in inputdate.</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * Its value is wijcalendar's option, visit 
  * http://wijmo.com/docs/wijmo/#Wijmo~jQuery.fn.-~wijcalendar.html for more details.
  * @example
  * $("#eventscalendar").wijgrid(
  *      { calendar: { prevTooltip: "Previous", nextTooltip: "Next" } });
  */
wijgrid_options.prototype.calendar = null;
/** <p>This function is called each time wijgrid needs to change cell appearence, for example, when the current cell position is changed or cell is selected.
  * Can be used for customization of cell style depending on its state.</p>
  * @field 
  * @type {function}
  * @param {wijmo.grid.ICellStyleFormaterArgs} args The data with this function.
  * @option 
  * @remarks
  * The args.state parameters equal to wijmo.grid.renderState.rendering means that the cell is being created,
  * at this moment you can apply general formatting to it indepentant of any particular state, like "current" or "selected".
  * @example
  * // Make the text of the current cell italic.
  * $("#element").wijgrid({
  *     highlightCurrentCell: true,
  *     cellStyleFormatter: function(args) {
  *         if ((args.row.type &amp; wijmo.grid.rowType.data)) {
  *             if (args.state &amp; wijmo.grid.renderState.current) {
  *                 args.$cell.css("font-style", "italic");
  *             } else {
  *                 args.$cell.css("font-style", "normal");
  *             }
  *         }
  *     }
  * });
  */
wijgrid_options.prototype.cellStyleFormatter = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn[]</a></p>
  * <p class='defaultValue'>Default value: []</p>
  * <p>An array of column options.</p>
  * @field 
  * @type {wijmo.grid.IColumn[]}
  * @option 
  * @example
  * $("#element").wijgrid({ columns: [ { headerText: "column0", allowSort: false }, { headerText: "column1", dataType: "number" } ] });
  */
wijgrid_options.prototype.columns = [];
/** <p class='defaultValue'>Default value: 'merge'</p>
  * <p>Determines behavior for column autogeneration. Possible values are: "none", "append", "merge".</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Possible values are:
  * "none": Column auto-generation is turned off.
  * "append": A column will be generated for each data field and added to the end of the columns collection.
  * "merge": Each column having dataKey option not specified will be automatically bound to the first unreserved data field.For each data field not bound to any column a new column will be generated and added to the end of the columns collection.
  * To prevent automatic binding of a column to a data field set its dataKey option to null.
  * Note: columns autogeneration process affects the options of columns and the columns option itself.
  * @example
  * $("#element").wijgrid({ columnsAutogenerationMode: "merge" });
  */
wijgrid_options.prototype.columnsAutogenerationMode = 'merge';
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the culture ID.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Please see the https://github.com/jquery/globalize for more information.
  * @example
  * // This code sets the culture to English.
  * $("#element").wijgrid({ culture: "en" });
  */
wijgrid_options.prototype.culture = "";
/** <p class='defaultValue'>Default value: ""</p>
  * <p>A value that indicators the culture calendar to format the text.
  * This option must work with culture option.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * // This code sets the culture and calendar to Japanese.
  * $("#element").wijgrid({ culture: "ja-jp", cultureCalendar: "Japanese" });
  */
wijgrid_options.prototype.cultureCalendar = "";
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IFilterOperator.html'>wijmo.grid.IFilterOperator[]</a></p>
  * <p class='defaultValue'>Default value: []</p>
  * <p>An array of custom user filters. Use this option if you want to extend the default set of filter operators with your own. Custom filters will be shown in the filter dropdown.</p>
  * @field 
  * @type {wijmo.grid.IFilterOperator[]}
  * @option 
  * @example
  * var oddFilterOp = {
  * name: "customOperator-Odd",
  * arity: 1,
  * applicableTo: ["number"],
  * operator: function(dataVal) { return (dataVal % 2 !== 0); }
  * }
  * $("#element").wijgrid({ customFilterOperators: [oddFilterOp] });
  */
wijgrid_options.prototype.customFilterOperators = [];
/** <p class='defaultValue'>Default value: null</p>
  * <p>Determines the datasource.
  * Possible datasources include:
  * 	1. A DOM table. This is the default datasource, used if the data option is null. Table must have no cells with rowSpan and colSpan attributes.
  * 	2. A two-dimensional array, such as [[0, "a"], [1, "b"]].
  * 	3. An array of objects, such as [{field0: 0, field1: "a"}, {field0: 1, field1: "b'}].
  * 	4. A wijdatasource.   
  * 	5. A wijdataview.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * // DOM table
  * $("#element").wijgrid();
  * // two-dimensional array
  * $("#element").wijgrid({ data: [[0, "a"], [1, "b"]] });
  */
wijgrid_options.prototype.data = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IDetailSettings.html'>wijmo.grid.IDetailSettings</a></p>
  * <p>Determines the detail grid settings in a hierarchy grid, which is used as a template for populating detail grids in a hierarchical grid.</p>
  * @field 
  * @type {wijmo.grid.IDetailSettings}
  * @option 
  * @remarks
  * Limitations:
  * The following datasources are supported (can be used as a detail.data option): arrays, ArrayDataView, AjaxDataView, ODataView, BreezeDataView.
  * It is not possible to use columns grouping and master-detail hierarchy simultaneously.
  * It is not possible to use virtual scrolling and master-detail hierarchy simultaneously.
  * In a detail grid the filtering ability is disabled for columns which are bounded to detailDataKey fields.
  * @example
  * $("#element").wijgrid({
  *    data: customers,
  *    detail: {
  *       data: orders,
  *       relation: [
  *          { masterDataKey: "ID", detailDataKey: "CUST_ID" }
  *       ]
  *    }
  * });
  */
wijgrid_options.prototype.detail = null;
/** <p class='defaultValue'>Default value: 'auto'</p>
  * <p>Determines an action to bring a cell in the editing mode when the editingMode option is set to "cell". Possible values are: "click", "doubleClick", "auto".</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Possible values are:
  * "click": cell is edited via a single click.
  * "doubleClick": cell is edited via a double click.
  * "auto": action is determined automatically depending upon user environment. If user has a mobile platform then "click" is used, "doubleClick" otherwise.
  * @example
  * $("#element").wijgrid({ editingInitOption: "auto" });
  */
wijgrid_options.prototype.editingInitOption = 'auto';
/** <p class='defaultValue'>Default value: 'none'</p>
  * <p>Determines the editing mode. Possible values are: "none", "row", "cell",</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Possible values are:
  * "none": the editing ability is disabled.
  * "cell": a single cell can be edited via a double click.
  * "row": a whole row can be edited via a command column.
  * @example
  * $("#element").wijgrid({
  *    editingMode: "row",
  *    columns: [{
  *       showEditButton: true
  *    }] 
  * });
  */
wijgrid_options.prototype.editingMode = 'none';
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines if the exact column width, in pixels, is used.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * By default, wijgrid emulates the table element behavior when using a number as the width. This means wijgrid may not have the exact width specified. If exact width is needed, please set the ensureColumnsPxWidth option of wijgrid to true. If this option is set to true, wijgrid will not expand itself to fit the available space.Instead, it will use the width option of each column widget.
  * @example
  * $("#element").wijgrid({ ensureColumnsPxWidth: true });
  */
wijgrid_options.prototype.ensureColumnsPxWidth = false;
/** <p class='defaultValue'>Default value: 'alphabeticalCustomFirst'</p>
  * <p>Determines the order of items in the filter drop-down list.
  * Possible values are: "none", "alphabetical", "alphabeticalCustomFirst" and "alphabeticalEmbeddedFirst"</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Possible values are:
  * "none": Operators follow the order of addition; built-in operators appear before custom ones.
  * "alphabetical": Operators are sorted alphabetically.
  * "alphabeticalCustomFirst": Operators are sorted alphabetically with custom operators appearing before built-in ones.
  * "alphabeticalEmbeddedFirst": Operators are sorted alphabetically with built-in operators appearing before custom operators.
  * "NoFilter" operator is always first.
  * @example
  * $("#element").wijgrid({ filterOperatorsSortMode: "alphabeticalCustomFirst" });
  */
wijgrid_options.prototype.filterOperatorsSortMode = 'alphabeticalCustomFirst';
/** <p>Determines whether the user can change position of the static column or row by dragging the vertical or horizontal freezing handle with the mouse. Possible values are: "none", "columns", "rows", "both".
  * Obsoleted, use the scrollingSettings.freezingMode property instead.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Possible values are:
  * "none": The freezing handle cannot be dragged.
  * "columns": The user can drag the vertical freezing handle to change position of the static column.
  * "rows": The user can drag the horizontal freezing handle to change position of the static row.
  * "both": The user can drag both horizontal and vertical freezing handles.
  * @example
  * $("#element").wijgrid({ freezingMode: "both" });
  */
wijgrid_options.prototype.freezingMode = null;
/** <p class='defaultValue'>Default value: 'Drag a column here to group by that column.'</p>
  * <p>Determines the caption of the group area.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * // Set the groupAreaCaption to a string and the text appears above the grid
  * $("#element").wijgrid({ groupAreaCaption: "Drag a column here to group by that column." });
  */
wijgrid_options.prototype.groupAreaCaption = 'Drag a column here to group by that column.';
/** <p class='defaultValue'>Default value: 10</p>
  * <p>Determines the indentation of the groups, in pixels.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * // Set the groupIndent option to the number of pixels to indent data when grouping.
  * $("#element").wijgrid({ groupIndent: 15 });
  */
wijgrid_options.prototype.groupIndent = 10;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether the position of the current cell is highlighted or not.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijgrid({ highlightCurrentCell: false });
  */
wijgrid_options.prototype.highlightCurrentCell = false;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether hovered row is highlighted or not.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijgrid({ highlightCurrentCell: true });
  */
wijgrid_options.prototype.highlightOnHover = true;
/** <p class='defaultValue'>Default value: 'Loading...'</p>
  * <p>Determines the text to be displayed when the grid is loading.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#element").wijgrid({ loadingText: "Loading..."});
  */
wijgrid_options.prototype.loadingText = 'Loading...';
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Cell values equal to this property value are considered null values. Use this option if you want to change default representation of null values (empty strings) with something else.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Case-sensitive for built-in parsers.
  * @example
  * $("#element").wijgrid({ nullString: "" });
  */
wijgrid_options.prototype.nullString = "";
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Determines the zero-based index of the current page. You can use this to access a specific page, for example, when using the paging feature.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * $("#element").wijgrid({ pageIndex: 0 });
  */
wijgrid_options.prototype.pageIndex = 0;
/** <p class='defaultValue'>Default value: 10</p>
  * <p>Number of rows to place on a single page.
  * The default value is 10.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * // The pageSize here sets 10 rows to a page. The allowPaging option is set to true so paging buttons appear.
  * $("#element").wijgrid({ pageSize: 10 });
  */
wijgrid_options.prototype.pageSize = 10;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IPagerSettings.html'>wijmo.grid.IPagerSettings</a></p>
  * <p>Determines the pager settings for the grid including the mode (page buttons or next/previous buttons), number of page buttons, and position where the buttons appear.</p>
  * @field 
  * @type {wijmo.grid.IPagerSettings}
  * @option 
  * @remarks
  * See the wijpager documentation for more information on pager settings.
  * @example
  * // Display the pager at the top of the wijgrid.
  * $("#element").wijgrid({ pagerSettings: { position: "top" } });
  */
wijgrid_options.prototype.pagerSettings = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value indicating whether DOM cell attributes can be passed within a data value.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * This option allows binding collection of values to data and automatically converting them as attributes of corresponded DOM table cells during rendering.
  * Values should be passed as an array of two items, where first item is a value of the data field, the second item is a list of values:
  * $("#element").wijgrid({
  * data: [
  * [ [1, { "style": "color: red", "class": "myclass" } ], a ]
  * ]
  * });
  * or
  * $("#element").wijgrid({
  * data: [
  * { col0: [1, { "style": "color: red", "class": "myclass" }], col1: "a" }
  * ]
  * });
  * Note: during conversion wijgrid extracts the first item value and makes it data field value, the second item (list of values) is removed:
  * [ { col0: 1, col1: "a" } ]
  * If DOM table is used as a datasource then attributes belonging to the cells in tBody section of the original table will be read and applied to the new cells.
  * rowSpan and colSpan attributes are not allowed.
  * @example
  * // Render the style attribute passed within the data.
  * $("#element").wijgrid({
  *     readAttributesFromData: false });
  *     data: [
  *         [ [1, { "style": "color: red" } ], a ]
  *     ]
  * });
  */
wijgrid_options.prototype.readAttributesFromData = false;
/** <p>Determines the height of a rows when virtual scrolling is used.
  * Obsoleted, use the scrollingSettings.virtualization.rowHeight property instead.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Can be set only during creation
  * @example
  * $("#element").wijgrid({ rowHeight: 20 });
  */
wijgrid_options.prototype.rowHeight = null;
/** <p>Function used for styling rows in wijgrid.</p>
  * @field 
  * @type {function}
  * @param {wijmo.grid.IRowInfo} args The data with this function.
  * @option 
  * @example
  * // Make text of the alternating rows italic.
  * $("#demo").wijgrid({
  *     data: [
  *         [0, "Nancy"], [1, "Susan"], [2, "Alice"], [3, "Kate"]
  *     ],
  *     rowStyleFormatter: function (args) {
  *         if ((args.state &amp; wijmo.grid.renderState.rendering) &amp;&amp; (args.type &amp; wijmo.grid.rowType.dataAlt)) {
  *             args.$rows.find("td").css("font-style", "italic");
  *         }
  *     }
  * });
  */
wijgrid_options.prototype.rowStyleFormatter = null;
/** <p>Determines which scrollbars are active and if they appear automatically based on content size.
  * Possbile values are: "none", "auto", "horizontal", "vertical", "both".
  * Obsoleted, use the scrollingSettings.mode property instead.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Possible values are:
  * "none": Scrolling is not used; the staticRowIndex and staticColumnIndex values are ignored.
  * "auto": Scrollbars appear automatically depending upon content size.
  * "horizontal": The horizontal scrollbar is active.
  * "vertical": The vertical scrollbar is active.
  * "both": Both horizontal and vertical scrollbars are active.
  * @example
  * // The horizontal and vertical scrollbars are active when the scrollMode is set to both.
  * $("#element").wijgrid({ scrollMode: "both" });
  */
wijgrid_options.prototype.scrollMode = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IScrollingSettings.html'>wijmo.grid.IScrollingSettings</a></p>
  * <p>Determines the scrolling settings.</p>
  * @field 
  * @type {wijmo.grid.IScrollingSettings}
  * @option 
  * @example
  * // The horizontal and vertical scrollbars are active when the scrollMode is set to both.
  * $("#element").wijgrid({ scrollingSettings: { mode: "both" } });
  */
wijgrid_options.prototype.scrollingSettings = null;
/** <p class='defaultValue'>Default value: 'singleRow'</p>
  * <p>Determines which cells, range of cells, columns, or rows can be selected at one time.
  * Possible values are: "none", "singleCell", "singleColumn", "singleRow", "singleRange", "multiColumn", "multiRow" and "multiRange".</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Possible values are: 
  * "none": Selection is turned off.
  * "singleCell": Only a single cell can be selected at a time.
  * "singleColumn": Only a single column can be selected at a time.
  * "singleRow": Only a single row can be selected at a time.
  * "singleRange": Only a single range of cells can be selected at a time.
  * "multiColumn": It is possible to select more than one row at the same time using the mouse and the CTRL or SHIFT keys.
  * "multiRow": It is possible to select more than one row at the same time using the mouse and the CTRL or SHIFT keys.
  * "multiRange": It is possible to select more than one cells range at the same time using the mouse and the CTRL or SHIFT keys.
  * @example
  * // Set selectionMode to muliColumn and users can select more than one column using the CTRL or SHIFT keys.
  * $("#element").wijgrid({ selectionMode: "multiColumn" });
  */
wijgrid_options.prototype.selectionMode = 'singleRow';
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value indicating whether the filter row is visible.
  * Filter row is used to display column filtering interface.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * // Set showFilter to true to view the filter row.
  * $("#element").wijgrid({ showFilter: true });
  */
wijgrid_options.prototype.showFilter = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value indicating whether the footer row is visible.
  * Footer row is used for displaying of tfoot section of original table, and to show totals.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * // Set showFooter to true to view the footer row.
  * $("#element").wijgrid({ showFooter: true });
  */
wijgrid_options.prototype.showFooter = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value indicating whether group area is visible.
  * Group area is used to display headers of groupped columns. User can drag columns from/to group area by dragging column headers with mouse, if allowColMoving option is on.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * // Set showGroupArea to true to display the group area.
  * $("#element").wijgrid({ showGroupArea: true });
  */
wijgrid_options.prototype.showGroupArea = false;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value indicating whether a selection will be automatically displayed at the current cell position when the wijgrid is rendered.
  * Set this option to false if you want to prevent wijgrid from selecting the currentCell automatically.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijgrid({ showSelectionOnRender: true });
  */
wijgrid_options.prototype.showSelectionOnRender = true;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value indicating whether the row header is visible.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijgrid({ showRowHeader: true });
  */
wijgrid_options.prototype.showRowHeader = false;
/** <p>Indicates the index of columns that will always be shown on the left when the grid view is scrolled horizontally.
  * Obsoleted, use the scrollingSettings.staticColumnIndex property instead.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Note that all columns before the static column will be automatically marked as static, too.
  * This can only take effect when the scrollMode option is not set to "none".
  * It will be considered "-1" when grouping or row merging is enabled. A "-1" means there is no data column but the row header is static. A zero (0) means one data column and row header are static.
  * @example
  * $("#element").wijgrid({ staticColumnIndex: -1 });
  */
wijgrid_options.prototype.staticColumnIndex = null;
/** <p>Gets or sets the alignment of the static columns area. Possible values are "left", "right".
  * Obsoleted, use the scrollingSettings.staticColumnsAlignment property instead.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * The "right" mode has limited functionality:
  * - The showRowHeader value is ignored.
  * - Changing staticColumnIndex at run-time by dragging the vertical bar is disabled.
  * @example
  * $("#element").wijgrid({ staticColumnsAlignment: "left" });
  */
wijgrid_options.prototype.staticColumnsAlignment = null;
/** <p>Indicates the index of data rows that will always be shown on the top when the wijgrid is scrolled vertically.
  * Obsoleted, use the scrollingSettings.staticRowIndext property instead.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Note, that all rows before the static row will be automatically marked as static, too.
  * This can only take effect when the scrollMode option is not set to "none". This will be considered "-1" when grouping or row merging is enabled.
  * A "-1" means there is no data row but the header row is static.A zero (0) means one data row and the row header are static.
  * @example
  * $("#element").wijgrid({ staticRowIndex: -1 });
  */
wijgrid_options.prototype.staticRowIndex = null;
/** <p class='defaultValue'>Default value: -1</p>
  * <p>Gets or sets the virtual number of items in the wijgrid and enables custom paging.
  * Setting option to a positive value activates custom paging, the number of displayed rows and the total number of pages will be determined by the totalRows and pageSize values.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * In custom paging mode sorting, paging and filtering are not performed automatically.
  * This must be handled manually using the sorted, pageIndexChanged, and filtered events. Load the new portion of data there followed by the ensureControl(true) method call.
  * @example
  * $("#element").wijgrid({ totalRows: -1 });
  */
wijgrid_options.prototype.totalRows = -1;
/** The afterCellEdit event handler is a function called after cell editing is completed.
  * This function can assist you in completing many tasks, such as in making changes once editing is completed; in tracking changes in cells, columns, or rows; or in integrating custom editing functions on the front end.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IAfterCellEditEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ afterCellEdit: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridaftercelledit", function (e, args) {
  * // some code here
  * });
  * @example
  * // Once cell editing is complete, the function calls the destroy method to destroy the wijcombobox widget and the wijinputnumber widget which are used as the custom editors.
  * $("#element").wijgrid({
  *     afterCellEdit: function(e, args) {
  *         switch (args.cell.column().dataKey) {
  *             case "Position":
  *                 args.cell.container().find("input").wijcombobox("destroy");
  *                 break;
  *             case "Acquired":
  *                 args.cell.container().find("input").wijinputnumber("destroy");
  *                 break;
  *         }
  *     }
  * });
  */
wijgrid_options.prototype.afterCellEdit = null;
/** The afterCellUpdate event handler is a function that is called after a cell has been updated. Among other functions, this event allows you to track and store the indices of changed rows or columns.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IAfterCellUpdateEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ afterCellUpdate: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridaftercellupdate", function (e, args) {
  * // some code here
  * });
  * @example
  * // Once the cell has been updated, the information from the underlying data is dumped into the "#log" element.
  * $("#element").wijgrid({
  *     afterCellUpdate: function(e, args) {
  *         $("#log").html(dump($("#demo").wijgrid("data")));
  *     }
  * });
  */
wijgrid_options.prototype.afterCellUpdate = null;
/** The beforeCellEdit event handler is a function that is called before a cell enters edit mode.
  * The beforeCellEdit event handler assists you in appending a widget, data, or other item to a wijgrid's cells before the cells enter edit mode. This event is cancellable if the editigMode options is set to "cell".
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IBeforeCellEditEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ beforeCellEdit: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridbeforecelledit", function (e, args) {
  * // some code here
  * });
  * @example
  * // Allow the user to change the price only if the product hasn't been discontinued:
  * $("#element").wijgrid({
  *     beforeCellEdit: function(e, args) {
  *         return !((args.cell.column().dataKey === "Price") &amp;&amp; args.cell.row().data.Discontinued);
  *     }
  * });
  */
wijgrid_options.prototype.beforeCellEdit = null;
/** The beforeCellUpdate event handler is a function that is called before the cell is updated with new or user-entered data. This event is cancellable if the editingMode options is set to "cell".
  * There are many instances where this event is helpful, such as when you need to check a cell's value before the update occurs or when you need to apply an alert message based on the cell's value.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IBeforeCellUpdateEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ beforeCellUpdate: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridbeforecellupdate", function (e, args) {
  * // some code here
  * });
  * @example
  * // In this sample, you use args.value to check the year that the user enters in the "Acquired" column.
  * // If it's less than 1990 or greater than the current year, then the event handler will return false to cancel updating and show the user an alert message.
  * $("#element").wijgrid({
  *     beforeCellUpdate: function(e, args) {
  *         switch (args.cell.column().dataKey) { 
  *             case "Acquired":
  *                 var $editor = args.cell.container().find("input"),
  *                     value = $editor.wijinputnumber("getValue"),
  *                     curYear = new Date().getFullYear();
  *                 if (value &lt; 1990 || value &gt; curYear) {
  *                     $editor.addClass("ui-state-error");
  *                     alert("value must be between 1990 and " + curYear);
  *                     $editor.focus();
  *                     return false; 
  *                 }
  *                 args.value = value; 
  *                 break;
  *         }
  *     }
  * });
  */
wijgrid_options.prototype.beforeCellUpdate = null;
/** The cellClicked event handler is a function that is called when a cell is clicked. You can use this event to get the information of a clicked cell using the args parameter.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.ICellClickedEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ cellClicked: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcellclicked", function (e, args) {
  * // some code here
  * });
  * @example
  * // The sample uses the cellClicked event to trigger an alert when the cell is clicked.
  * $("#element").wijgrid({
  *     cellClicked: function (e, args) {
  *         alert(args.cell.value());
  *     }
  * });
  */
wijgrid_options.prototype.cellClicked = null;
/** The columnDragging event handler is a function that is called when column dragging has been started, but before the wijgrid handles the operation. This event is cancellable.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IColumnDraggingEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnDragging: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumndragging", function (e, args) {
  * // some code here
  * });
  * @example
  * // Preventing a user from dragging a specific column
  * $("#element").wijgrid({
  *     columnDragging: function (e, args) {
  *         return !(args.drag.dataKey == "ID");
  *     }
  * });
  */
wijgrid_options.prototype.columnDragging = null;
/** The columnDragged event handler is a function that is called when column dragging has been started. You can use this event to find the column being dragged or the dragged column's location.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IColumnDraggedEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnDragged: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumndragged", function (e, args) {
  * // some code here
  * });
  * @example
  * // Supply a callback function to handle the columnDragged event:
  * $("#element").wijgrid({
  *     columnDragged: function (e, args) {
  *         alert("The '" + args.drag.headerText + "' column is being dragged from the '" + args.dragSource + "' location");
  *     }
  * });
  */
wijgrid_options.prototype.columnDragged = null;
/** The columnDropping event handler is a function that is called when a column is dropped into the columns area, but before wijgrid handles the operation. This event is cancellable.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IColumnDroppingEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnDropping: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumndropping", function (e, args) {
  * // some code here
  * });
  * @example
  * // Preventing user from dropping any column before the "ID" column.
  * $("#element").wijgrid({
  *     columnDropping: function (e, args) {
  *         return !(args.drop.dataKey == "ID" &amp;&amp; args.at == "left");
  *     }
  * });
  */
wijgrid_options.prototype.columnDropping = null;
/** The columnDropped event handler is a function that is called when a column has been dropped into the columns area.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IColumnDroppedEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnDropped: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumndropped", function (e, args) {
  * // some code here
  * });
  * @example
  * // Supply a callback function to handle the columnDropped event:
  * $("#element").wijgrid({
  *     columnDropped: function (e, args) {
  *         "The '" + args.drag.headerText + "' column has been dropped onto the '" + args.drop.headerText + "' column at the '" + args.at + "' position"
  *     }
  * });
  */
wijgrid_options.prototype.columnDropped = null;
/** The columnGrouping event handler is a function that is called when a column is dropped into the group area, but before the wijgrid handles the operation. This event is cancellable.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IColumnGroupingEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnGrouping: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumngrouping", function (e, args) {
  * // some code here
  * });
  * @example
  * // Preventing user from grouping the "UnitPrice" column.
  * $("#element").wijgrid({
  *     columnGrouping: function (e, args) {
  *         return !(args.drag.headerText == "UnitPrice");
  *     }
  * });
  */
wijgrid_options.prototype.columnGrouping = null;
/** The columnGrouped event handler is a function that is called when a column has been dropped into the group area.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IColumnGroupedEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnGrouped: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumngrouped", function (e, args) {
  * // some code here
  * });
  * @example
  * // Supply a callback function to handle the columnGrouped event:
  * $("#element").wijgrid({
  *     columnGrouped: function (e, args) {
  *         alert("The '" + args.drag.headerText "' column has been grouped");
  *     }
  * });
  */
wijgrid_options.prototype.columnGrouped = null;
/** The columnResizing event handler is called when a user resizes the column but before the wijgrid handles the operation. This event is cancellable.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IColumnResizingEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnResizing: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumnresizing", function (e, args) {
  * // some code here
  * });
  * @example
  * // Prevent setting the width of "ID" column less than 100 pixels
  * $("#element").wijgrid({
  *     columnResizing: function (e, args) {
  *         if (args.column.dataKey == "ID" &amp;&amp; args.newWidth &lt; 100) {
  *             args.newWidth = 100;
  *         }
  *     }
  * });
  */
wijgrid_options.prototype.columnResizing = null;
/** The columnResized event handler is called when a user has changed a column's size.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IColumnResizedEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnResized: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumnresized", function (e, args) {
  * // some code here
  * });
  * @example
  * // Supply a callback function to handle the columnGrouped event:
  * $("#element").wijgrid({
  *     columnResized: function (e, args) {
  *         alert("The '" + args.column.headerText + "' has been resized");
  *     }
  * });
  */
wijgrid_options.prototype.columnResized = null;
/** The columnUngrouping event handler is called when a column has been removed from the group area but before the wjgrid handles the operation. This event is cancellable.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IColumnUngroupingEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnUngrouping: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumnungrouping", function (e, args) {
  * // some code here
  * });
  * @example
  * // Preventing user from ungrouping the "UnitPrice" column.
  * $("#element").wijgrid({
  *     columnUngrouping: function (e, args) {
  *         return !(args.column.headerText == "UnitPrice");
  *     }
  * });
  */
wijgrid_options.prototype.columnUngrouping = null;
/** The columnUngrouped event handler is called when a column has been removed from the group area.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IColumnUngroupedEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnUngrouped: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumnungrouped", function (e, args) {
  * // some code here
  * });
  * @example
  * // Supply a callback function to handle the columnGrouped event:
  * $("#element").wijgrid({
  *     columnUngrouped: function (e, args) {
  *         alert("The '" + args.column.headerText + "' has been ungrouped");
  *     }
  * });
  */
wijgrid_options.prototype.columnUngrouped = null;
/** The currentCellChanging event handler is called before the cell is changed. You can use this event to get a selected row or column or to get a data row bound to the current cell. This event is cancellable.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.ICurrentCellChangingEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ currentCellChanging: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcurrentcellchanging", function (e, args) {
  * // some code here
  * });
  * @example
  * // Gets the data row bound to the current cell.
  * $("#element").wijgrid({
  *     currentCellChanging: function (e, args) {
  *         var rowObj = $(e.target).wijgrid("currentCell").row();
  *         if (rowObj) {        
  *             var dataItem = rowObj.data; // current data item (before the cell is changed).
  *         }
  *     }
  * });
  */
wijgrid_options.prototype.currentCellChanging = null;
/** The currentCellChanged event handler is called after the current cell is changed.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ currentCellChanged: function (e) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcurrentcellchanged", function (e) {
  * // some code here
  * });
  * @example
  * // Gets the data row bound to the current cell.
  * $("#element").wijgrid({
  *     currentCellChanged: function (e, args) {
  *         var rowObj = $(e.target).wijgrid("currentCell").row();
  *         if (rowObj) {        
  *             var dataItem = rowObj.data; // current data item (after the cell is changed).
  *         }
  *     }
  * });
  */
wijgrid_options.prototype.currentCellChanged = null;
/** The detailCreating event handler is called when wijgrid requires to create a new detail wijgrid.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IDetailCreatingEventArgs} args The data with this event.
  * @remarks
  * Event receives options of a detail grid to create, which were obtained by cloning the detail option of the master grid.
  * User can alter the detail grid options here and provide a specific datasource within the args.options.data option to implement run-time hierarachy.
  */
wijgrid_options.prototype.detailCreating = null;
/** The filterOperatorsListShowing event handler is a function that is called before the filter drop-down list is shown. You can use this event to customize the list of filter operators for your users.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IFilterOperatorsListShowingEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ filterOperatorsListShowing: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridfilteroperatorslistshowing", function (e, args) {
  * // some code here
  * });
  * @example
  * // Limit the filters that will be shown to the "Equals" filter operator
  * $("#element").wijgrid({
  *     filterOperatorsListShowing: function (e, args) {
  *         args.operators = $.grep(args.operators, function(op) {
  *             return op.name === "Equals" || op.name === "NoFilter";
  *         }
  *     }
  * });
  */
wijgrid_options.prototype.filterOperatorsListShowing = null;
/** The filtering event handler is a function that is called before the filtering operation is started. For example, you can use this event to change a filtering condition before a filter will be applied to the data. This event is cancellable.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IFilteringEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ filtering: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridfiltering", function (e, args) {
  * // some code here
  * });
  * @example
  * // Prevents filtering by negative values
  * $("#element").wijgrid({
  *     filtering: function (e, args) {
  *         if (args.column.dataKey == "Price" &amp;&amp; args.value &lt; 0) {
  *             args.value = 0;
  *         }
  *     }
  * });
  */
wijgrid_options.prototype.filtering = null;
/** The filtered event handler is a function that is called after the wijgrid is filtered.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IFilteredEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ filtered: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridfiltered", function (e, args) {
  * // some code here
  * });
  * @example
  * // 
  * $("#element").wijgrid({
  *     filtered: function (e, args) {
  *         alert("The filtered data contains: " + $(this).wijgrid("dataView").count() + " rows");
  *     }
  * });
  */
wijgrid_options.prototype.filtered = null;
/** The groupAggregate event handler is a function that is called when groups are being created and the column object's aggregate option has been set to "custom". This event is useful when you want to calculate custom aggregate values.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IGroupAggregateEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ groupAggregate: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridgroupaggregate", function (e, args) {
  * // some code here
  * });
  * @example
  * // This sample demonstrates using the groupAggregate event handler to calculate an average in a custom aggregate:
  * $("#element").wijgrid({
  *     groupAggregate: function (e, args) {
  *         if (args.column.dataKey == "Price") {
  *             var aggregate = 0;
  *             for (var i = args.groupingStart; i &lt;= args.groupingEnd; i++) {
  *                 aggregate += args.data[i].valueCell(args.column.dataIndex).value;
  *             }
  *             aggregate = aggregate/ (args.groupingEnd - args.groupingStart + 1);
  *             args.text = aggregate;
  *         }
  *     }
  * });
  */
wijgrid_options.prototype.groupAggregate = null;
/** The groupText event handler is a function that is called when groups are being created and the groupInfo option has the groupInfo.headerText or the groupInfo.footerText options set to "custom". This event can be used to customize group headers and group footers.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IGroupTextEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ groupText: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridgrouptext", function (e, args) {
  * // some code here
  * });
  * @example
  * // The following sample sets the groupText event handler to avoid empty cells. The custom formatting applied to group headers left certain cells appearing as if they were empty. This code avoids that:
  * $("#element").wijgrid({
  *     groupText: function (e, args) {
  *         if (!args.groupText) {
  *             args.text = "null";
  *         }
  *     }
  * });
  */
wijgrid_options.prototype.groupText = null;
/** The invalidCellValue event handler is a function called when a cell needs to start updating but the cell value is invalid. So if the value in a wijgrid cell can't be converted to the column target type, the invalidCellValue event will fire.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IInvalidCellValueEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ invalidCellValue: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridinvalidcellvalue", function (e, args) {
  * // some code here
  * });
  * @example
  * // Adds a style to the cell if the value entered is invalid
  * $("#element").wijgrid({
  *     invalidCellValue: function (e, args) {
  *         $(args.cell.container()).addClass("ui-state-error");
  *     }
  * });
  */
wijgrid_options.prototype.invalidCellValue = null;
/** The pageIndexChanging event handler is a function that is called before the page index is changed. This event is cancellable.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IPageIndexChangingEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ pageIndexChanging: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridpageindexchanging", function (e, args) {
  * // some code here
  * });
  * @example
  * // Cancel the event by returning false
  * $("#element").wijgrid({
  *     pageIndexChanging: function (e, args) {
  *         return false;
  *     }
  * });
  */
wijgrid_options.prototype.pageIndexChanging = null;
/** The pageIndexChanged event handler is a function that is called after the page index is changed, such as when you use the numeric buttons to swtich between pages or assign a new value to the pageIndex option.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.IPageIndexChangedEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ pageIndexChanged: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridpageindexchanged", function (e, args) {
  * // some code here
  * });
  * @example
  * // Supply a callback function to handle the pageIndexChanged event:
  * $("#element").wijgrid({
  *     pageIndexChanged: function (e, args) {
  *         alert("The new pageIndex is: " + args.newPageIndex);
  *     }
  * });
  */
wijgrid_options.prototype.pageIndexChanged = null;
/** The selectionChanged event handler is a function that is called after the selection is changed.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.ISelectionChangedEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ selectionChanged: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridselectionchanged", function (e, args) {
  * // some code here
  * });
  * @example
  * // Get the value of the first cell of the selected row.
  * $("#element").wijgrid({
  *     selectionMode: "singleRow",
  *     selectionChanged: function (e, args) {
  *         alert(args.addedCells.item(0).value());
  *     }
  * });
  */
wijgrid_options.prototype.selectionChanged = null;
/** The sorting event handler is a function that is called before the sorting operation is started. This event is cancellable.
  * The allowSorting option must be set to "true" for this event to fire.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.ISortingEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ sorting: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridsorting", function (e, args) {
  * // some code here
  * });
  * @example
  * // Preventing user from sorting the "ID" column.
  * $("#element").wijgrid({
  *     sorting: function (e, args) {
  *         return !(args.column.headerText === "ID");
  *     }
  * });
  */
wijgrid_options.prototype.sorting = null;
/** The sorted event handler is a function that is called after the widget is sorted. The allowSorting option must be set to "true" to allow this event to fire.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {wijmo.grid.ISortedEventArgs} args The data with this event.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ sorted: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridsorted", function (e, args) {
  * // some code here
  * });
  * @example
  * // The following code handles the sorted event and will give you access to the column and the sort direction
  * $("#element").wijgrid({
  *     sorted: function (e, args) {
  *         alert("Column " + args.column.headerText + " sorted in " + args.sortDirection + " order");
  *     }
  * });
  */
wijgrid_options.prototype.sorted = null;
/** The dataLoading event handler is a function that is called when the wijgrid loads a portion of data from the underlying datasource. This can be used for modification of data sent to server if using dynamic remote wijdatasource.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ dataLoading: function (e) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgriddataloading", function (e) {
  * // some code here
  * });
  * @example
  * // This sample allows you to set the session ID when loading a portion of data from the remote wijdatasource:
  * $("#element").wijgrid({
  *     data: new wijdatasource({
  *         proxy: new wijhttpproxy({
  *             // some code here
  *         })
  *     }),
  *     dataLoading: function (e) {
  *         var dataSource = $(this).wijgrid("option", "data");
  *         dataSource.proxy.options.data.sessionID = getSessionID();
  *     }
  * });
  */
wijgrid_options.prototype.dataLoading = null;
/** The dataLoaded event handler is a function that is called when data is loaded.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ dataLoaded: function (e) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgriddataloaded", function (e) {
  * // some code here
  * });
  * @example
  * // Display the number of entries found
  * $("#element").wijgrid({
  *     dataLoaded: function (e) {
  *         alert($(this).wijgrid("dataView").count());
  *     }
  * });
  */
wijgrid_options.prototype.dataLoaded = null;
/** The loading event handler is a function that is called at the beginning of the wijgrid's lifecycle. You can use this event to activate a custom load progress indicator.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ loading: function (e) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridloading", function (e) {
  * // some code here
  * });
  * @example
  * // Creating an indeterminate progressbar during loading
  * $("#element").wijgrid({
  *     loading: function (e) {
  *         $("#progressBar").show().progressbar({ value: false });
  *     }
  * });
  */
wijgrid_options.prototype.loading = null;
/** The loaded event handler is a function that is called at the end the wijgrid's lifecycle when wijgrid is filled with data and rendered. You can use this event to manipulate the grid html content or to finish a custom load indication.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ loaded: function (e) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridloaded", function (e) {
  * // some code here
  * });
  * @example
  * // The loaded event in the sample below ensures that whatever is selected on load is cleared
  * $("#element").wijgrid({
  *     loaded: function (e) {
  *         $(e.target).wijgrid("selection").clear(); // clear selection                    
  *     }
  * });
  */
wijgrid_options.prototype.loaded = null;
/** The rendering event handler is a function that is called when the wijgrid is about to render. Normally you do not need to use this event.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ rendering: function (e) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridrendering", function (e) {
  * // some code here
  * });
  * @example
  * $("#element").wijgrid({
  *     rendering: function (e) {
  *         alert("rendering");
  *     }
  * });
  */
wijgrid_options.prototype.rendering = null;
/** The rendered event handler is a function that is called when the wijgrid is rendered. Normally you do not need to use this event.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ rendered: function (e) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridrendered", function (e) {
  * // some code here
  * });
  * @example
  * $("#element").wijgrid({
  *     rendered: function (e) {
  *         alert("rendered");
  *     }
  * });
  */
wijgrid_options.prototype.rendered = null;
wijmo.grid.wijgrid.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijgrid_options());
$.widget("wijmo.wijgrid", $.wijmo.widget, wijmo.grid.wijgrid.prototype);
})()
})()
