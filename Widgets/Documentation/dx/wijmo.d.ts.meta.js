var JQueryUI = JQueryUI || {};

(function () {
/** @interface UI
  * @namespace JQueryUI
  */
JQueryUI.UI = function () {};
/** @param {HTMLElement} el
  * @param {string} a
  * @returns {bool}
  */
JQueryUI.UI.prototype.hasScroll = function (el, a) {};
/** <p>undefined</p>
  * @field
  */
JQueryUI.UI.prototype.position = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.UI.prototype.plugin = null;
/** @interface KeyCode
  * @namespace JQueryUI
  */
JQueryUI.KeyCode = function () {};
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.INSERT = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.CAPSLOCK = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.CTRL = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.SHIFT = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.ALT = null;
})()
