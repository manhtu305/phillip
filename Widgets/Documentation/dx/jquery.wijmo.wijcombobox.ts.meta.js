var wijmo = wijmo || {};

(function () {
wijmo.combobox = wijmo.combobox || {};

(function () {
/** @class wijcombobox
  * @widget 
  * @namespace jQuery.wijmo.combobox
  * @extends wijmo.wijmoWidget
  */
wijmo.combobox.wijcombobox = function () {};
wijmo.combobox.wijcombobox.prototype = new wijmo.wijmoWidget();
/** Repaints wijcombobox. Returns true if it succeeds; 
  *   otherwise, returns false.
  * @returns {bool} true if it succeeds; otherwise, returns false.
  */
wijmo.combobox.wijcombobox.prototype.repaint = function () {};
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.combobox.wijcombobox.prototype.destroy = function () {};
/** Searches the wijcombobox drop-down list for the specified value.
  * @param {string} value Text to search in the drop-down list.
  * @param {object} eventObj The jquery event object.
  */
wijmo.combobox.wijcombobox.prototype.search = function (value, eventObj) {};
/** Get the select item(s) in combobox.
  * @returns {array} array object or empty array.
  * @remarks
  * when using multiple mode, it will return array object.
  * If no item is selected, it will return null or empty array.
  */
wijmo.combobox.wijcombobox.prototype.getSelectedItems = function () {};
/** Closes drop-down list.
  * @param {?EventObj} event The jquery event object.
  * @param {?boolean} skipAnimation A value indicating whehter to skip animation.
  */
wijmo.combobox.wijcombobox.prototype.close = function (event, skipAnimation) {};

/** @class */
var wijcombobox_options = function () {};
/** <p class='defaultValue'>Default value: null</p>
  * <p>wijdataview to which this wijcombobox is bound.</p>
  * @field 
  * @type {wijdataview}
  * @option 
  * @remarks
  * This option is used if this wijcombobox is bound to a wijdataview.
  * In that case, you can also specify a mapping option to select the properties to bind to,
  * and the data option returns an array of objects containing 
  * value and label property values determined by that mapping.
  */
wijcombobox_options.prototype.dataSource = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that specifies the underlying data source provider of wijcombobox.</p>
  * @field 
  * @type {wijdatasource|Array}
  * @option 
  * @remarks
  * This option could either be a wijdatasource object 
  * or an Object Array containing an item such as 
  * {label: "label text", value: "value"}.
  * @example
  * var testArray = [
  *            {label: 'c++',value: 'c++'}, 
  *            {label: 'java',value: 'java'}, 
  *            {label: 'php',value: 'php'}
  *        ];
  *        $("#tags").wijcombobox({
  *            data: testArray
  *        });
  */
wijcombobox_options.prototype.data = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that specifies the text in the wijcombobox label.</p>
  * @field 
  * @type {object}
  * @option
  */
wijcombobox_options.prototype.labelText = null;
/** <p class='defaultValue'>Default value: 4</p>
  * <p>A value that determines the minimum length of text 
  *   that can be entered in the wijcombobox text box to issue an AJAX request.</p>
  * @field 
  * @type {number}
  * @option
  */
wijcombobox_options.prototype.minLength = 4;
/** <p class='defaultValue'>Default value: 300</p>
  * <p>A value that determines the duration (in milliseconds) of the time
  *   to delay before autocomplete begins after typing stops.</p>
  * @field 
  * @type {number}
  * @option
  */
wijcombobox_options.prototype.delay = 300;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that specifies the animation options for a drop-down list
  *   when it is visible.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * var animationOptions = {
  *           animated: "Drop",
  *           duration: 1000
  *       };
  *       $("#tags").wijcombobox("option", "showingAnimation", animationOptions)
  *         });
  */
wijcombobox_options.prototype.showingAnimation = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that specifies the animation options such as the animation effect and 
  *   duration for the drop-down list when it is hidden.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * var animationOptions = {
  *           animated: "Drop",
  *           duration: 1000
  *       };
  *       $("#tags").wijcombobox("option", "hidingAnimation", animationOptions)
  */
wijcombobox_options.prototype.hidingAnimation = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value that determines whether to show the trigger of wijcombobox.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijcombobox_options.prototype.showTrigger = true;
/** <p class='defaultValue'>Default value: 'right'</p>
  * <p>A value that specifies the position of the drop-down list trigger.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * possible value: right or left
  */
wijcombobox_options.prototype.triggerPosition = 'right';
/** <p class='defaultValue'>Default value: 300</p>
  * <p>A value that specifies the height of the drop-down list.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * If the total height of all items is less than the value of this option,
  * it will use the total height of items as the height of the drop-down list.
  */
wijcombobox_options.prototype.dropdownHeight = 300;
/** <p class='defaultValue'>Default value: 'auto'</p>
  * <p>A value that specifies the width of the drop-down list.</p>
  * @field 
  * @type {number|string}
  * @option 
  * @remarks
  * When this option is set to "auto", the width of the drop-down
  * list is equal to the width of wijcombobox.
  * When this option is set to "fit", the width of the drop-down
  * list will fit the widest item independently.
  */
wijcombobox_options.prototype.dropdownWidth = 'auto';
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that determines whether to select the item when the item gains focus or is activated.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijcombobox_options.prototype.selectOnItemFocus = false;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value determines whether to shorten the drop-down list items 
  *   by matching the text in the textbox after typing.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijcombobox_options.prototype.autoFilter = true;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value that determines whether to start the auto-complete 
  *   function after typing in the text if a match exists.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijcombobox_options.prototype.autoComplete = true;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value that determines whether to highlight the keywords in an item.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * If "abc" is typed in the textbox, 
  * all "abc" matches are highlighted in the drop-down list.
  */
wijcombobox_options.prototype.highlightMatching = true;
/** <p>A value that specifies the position options of the drop-down list.
  *   The default value of the "of" options is the input of wijcombobox.</p>
  * @field 
  * @option 
  * @example
  * var positionOptions = {my:"right", at:"top"};
  *   //specifies the position options of the drop-down list on "right, top" of combobox.
  *   $("#tags").wijcombobox("option", "dropDownListPosition", positionOptions)
  */
wijcombobox_options.prototype.dropDownListPosition = null;
/** <p class='defaultValue'>Default value: []</p>
  * <p>An array that specifies the column collections of wijcombobox.</p>
  * @field 
  * @type {array}
  * @option 
  * @example
  * $("#tags").wijcombobox("option", "columns", [
  *           {name: 'header1', width: 150}, 
  *           {name: 'header2', width: 150}, 
  *           {name: 'header3', width: 150}
  *       ]);
  */
wijcombobox_options.prototype.columns = [];
/** <p class='defaultValue'>Default value: 'single'</p>
  * <p>A value that specifies the selection mode of wijcombobox.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Possible options are: "single" and "multiple".
  */
wijcombobox_options.prototype.selectionMode = 'single';
/** <p class='defaultValue'>Default value: ','</p>
  * <p>A value that specifies the separator for 
  *   the multiple selected items text in the textbox.</p>
  * @field 
  * @type {string}
  * @option
  */
wijcombobox_options.prototype.multipleSelectionSeparator = ',';
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that determines whether to check the input text against
  *   the text of the selected item when the focus blurs.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * If the text does not match any item, input text will restore
  * to text the selected item or empty if no item is selected.
  */
wijcombobox_options.prototype.forceSelectionText = false;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value that determines whether input is editable.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijcombobox_options.prototype.isEditable = true;
/** <p class='defaultValue'>Default value: -1</p>
  * <p>A value that specifies the index of the item to select when using single mode.</p>
  * @field 
  * @type {number|array}
  * @option 
  * @remarks
  * If the selectionMode is "multiple", then this option could be set 
  * to an array of Number which contains the indices of the items to select.
  * If no item is selected, it will return -1.
  * @example
  * //To get the selected item using the selected index:
  *      var selectedIndex = $("# tags ").wijcombobox("option","selectedIndex");  
  *      var selectedItem = $("# tags ").wijcombobox("option","data")[selectedIndex];
  *  // To set the selected item using the selected index:
  *      $("#tags").wijcombobox("option"," selectedIndex", 5);
  */
wijcombobox_options.prototype.selectedIndex = -1;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that specifies the value of the item to select when using single mode.</p>
  * @field 
  * @type {number|string|object}
  * @option 
  * @remarks
  * If no item is selected, it will return null.
  */
wijcombobox_options.prototype.selectedValue = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that specifies the input text of the combobox is in dropdown list or not.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * It's readonly option, if user typed text is not in dropdown list, it returns false; 
  * if user selects a item form the dropdown list or typed text in dropdown list, it returns true.
  */
wijcombobox_options.prototype.inputTextInDropDownList = false;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that specifies the input text of the combobox.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * When set the text by code, it will not affect the selectedIndex and selectedValue.
  */
wijcombobox_options.prototype.text = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value indicating the dropdown element will be append to the body or combobox container.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * If the value is true, the dropdown list will be appended to body element.
  * else it will append to the combobox container.
  */
wijcombobox_options.prototype.ensureDropDownOnBody = true;
/** <p class='defaultValue'>Default value: 6</p>
  * <p>A value added to the width of the original HTML select element to account 
  *   for the scroll bar width of the drop-down list.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Unit for this value is pixel.
  * Because the width of the scroll bar may be different between browsers 
  * if wijcombobox is initialized with the width of the HTML select element, 
  * the text may be hidden by the scroll bar of wijcombobox.
  */
wijcombobox_options.prototype.selectElementWidthFix = 6;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that determines the object that contains the options of wijlist.</p>
  * @field 
  * @type {object}
  * @option
  */
wijcombobox_options.prototype.listOptions = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that determines whether to fit the width of combobox to that of the dropdown.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijcombobox_options.prototype.adjustComboWidth = false;
/** A function called when any item in list is selected.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.combobox.ISelectEventArgs} data Information about an event
  */
wijcombobox_options.prototype.select = null;
/** This event is triggered when the drop-down list is opened.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijcombobox_options.prototype.open = null;
/** This event is triggered when the drop-down list is closed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijcombobox_options.prototype.close = null;
/** This event is triggered when a user searches an item in the drop-down list either 
  *  by typing in the textbox or by calling the search method of wijcombobox.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.combobox.ISearchEventArgs} data Information about an event
  */
wijcombobox_options.prototype.search = null;
/** The event is obsolete event.
  *  A function called when select item is changed.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IselectedEventArgs} data The data with this event.
  */
wijcombobox_options.prototype.changed = null;
/** This event is triggered when the text of the combobox is changed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.combobox.ITextChangedEventArgs} data Information about an event
  */
wijcombobox_options.prototype.textChanged = null;
/** This event is triggered when the selected index of the combobox is changed.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IselectedEventArgs} data The data with this event.
  */
wijcombobox_options.prototype.selectedIndexChanged = null;
/** A function called when the selected index of the comboBox is about to change. 
  *  Cancellable. If return false, the select operation will be canceled.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IselectedEventArgs} data The data with this event.
  */
wijcombobox_options.prototype.selectedIndexChanging = null;
wijmo.combobox.wijcombobox.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijcombobox_options());
$.widget("wijmo.wijcombobox", $.wijmo.widget, wijmo.combobox.wijcombobox.prototype);
/** @interface WijComboboxOptions
  * @namespace wijmo.combobox
  * @extends wijmo.WidgetOptions
  */
wijmo.combobox.WijComboboxOptions = function () {};
/** <p>wijdataview to which this wijcombobox is bound.</p>
  * @field 
  * @type {wijdataview}
  * @remarks
  * This option is used if this wijcombobox is bound to a wijdataview.
  * In that case, you can also specify a mapping option to select the properties to bind to,
  * and the data option returns an array of objects containing 
  * value and label property values determined by that mapping.
  */
wijmo.combobox.WijComboboxOptions.prototype.dataSource = null;
/** <p>A value that specifies the underlying data source provider of wijcombobox.</p>
  * @field 
  * @type {wijdatasource|Array}
  * @remarks
  * This option could either be a wijdatasource object 
  * or an Object Array containing an item such as 
  * {label: "label text", value: "value"}.
  * @example
  * var testArray = [
  *            {label: 'c++',value: 'c++'}, 
  *            {label: 'java',value: 'java'}, 
  *            {label: 'php',value: 'php'}
  *        ];
  *        $("#tags").wijcombobox({
  *            data: testArray
  *        });
  */
wijmo.combobox.WijComboboxOptions.prototype.data = null;
/** <p>A value that specifies the text in the wijcombobox label.</p>
  * @field 
  * @type {string}
  */
wijmo.combobox.WijComboboxOptions.prototype.labelText = null;
/** <p>A value that determines the minimum length of text 
  *   that can be entered in the wijcombobox text box to issue an AJAX request.</p>
  * @field 
  * @type {number}
  */
wijmo.combobox.WijComboboxOptions.prototype.minLength = null;
/** <p>A value that determines the duration (in milliseconds) of the time
  *   to delay before autocomplete begins after typing stops.</p>
  * @field 
  * @type {number}
  */
wijmo.combobox.WijComboboxOptions.prototype.delay = null;
/** <p>A value that specifies the animation options for a drop-down list
  *   when it is visible.</p>
  * @field 
  * @example
  * var animationOptions = {
  *           animated: "Drop",
  *           duration: 1000
  *       };
  *       $("#tags").wijcombobox("option", "showingAnimation", animationOptions)
  *         });
  */
wijmo.combobox.WijComboboxOptions.prototype.showingAnimation = null;
/** <p>A value that specifies the animation options such as the animation effect and 
  *   duration for the drop-down list when it is hidden.</p>
  * @field 
  * @example
  * var animationOptions = {
  *           animated: "Drop",
  *           duration: 1000
  *       };
  *       $("#tags").wijcombobox("option", "hidingAnimation", animationOptions)
  */
wijmo.combobox.WijComboboxOptions.prototype.hidingAnimation = null;
/** <p>A value that determines whether to show the trigger of wijcombobox.</p>
  * @field 
  * @type {boolean}
  */
wijmo.combobox.WijComboboxOptions.prototype.showTrigger = null;
/** <p>A value that specifies the position of the drop-down list trigger.</p>
  * @field 
  * @type {string}
  * @remarks
  * possible value: right or left
  */
wijmo.combobox.WijComboboxOptions.prototype.triggerPosition = null;
/** <p>A value that specifies the height of the drop-down list.</p>
  * @field 
  * @type {number}
  * @remarks
  * If the total height of all items is less than the value of this option,
  * it will use the total height of items as the height of the drop-down list.
  */
wijmo.combobox.WijComboboxOptions.prototype.dropdownHeight = null;
/** <p>A value that specifies the width of the drop-down list.</p>
  * @field 
  * @type {number|string}
  * @remarks
  * When this option is set to "auto", the width of the drop-down
  * list is equal to the width of wijcombobox.
  * When this option is set to "fit", the width of the drop-down
  * list will fit the widest item independently.
  */
wijmo.combobox.WijComboboxOptions.prototype.dropdownWidth = null;
/** <p>A value that determines whether to select the item when the item gains focus or is activated.</p>
  * @field 
  * @type {boolean}
  */
wijmo.combobox.WijComboboxOptions.prototype.selectOnItemFocus = null;
/** <p>A value determines whether to shorten the drop-down list items 
  *   by matching the text in the textbox after typing.</p>
  * @field 
  * @type {boolean}
  */
wijmo.combobox.WijComboboxOptions.prototype.autoFilter = null;
/** <p>A value that determines whether to start the auto-complete 
  *   function after typing in the text if a match exists.</p>
  * @field 
  * @type {boolean}
  */
wijmo.combobox.WijComboboxOptions.prototype.autoComplete = null;
/** <p>A value that determines whether to highlight the keywords in an item.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * If "abc" is typed in the textbox, 
  * all "abc" matches are highlighted in the drop-down list.
  */
wijmo.combobox.WijComboboxOptions.prototype.highlightMatching = null;
/** <p>A value that specifies the position options of the drop-down list.
  *   The default value of the "of" options is the input of wijcombobox.</p>
  * @field 
  * @type {JQueryUI.JQueryPositionOptions}
  * @example
  * var positionOptions = {my:"right", at:"top"};
  *   //specifies the position options of the drop-down list on "right, top" of combobox.
  *   $("#tags").wijcombobox("option", "dropDownListPosition", positionOptions)
  */
wijmo.combobox.WijComboboxOptions.prototype.dropDownListPosition = null;
/** <p>An array that specifies the column collections of wijcombobox.</p>
  * @field 
  * @type {array}
  * @example
  * $("#tags").wijcombobox("option", "columns", [
  *           {name: 'header1', width: 150}, 
  *           {name: 'header2', width: 150}, 
  *           {name: 'header3', width: 150}
  *       ]);
  */
wijmo.combobox.WijComboboxOptions.prototype.columns = null;
/** <p>A value that specifies the selection mode of wijcombobox.</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible options are: "single" and "multiple".
  */
wijmo.combobox.WijComboboxOptions.prototype.selectionMode = null;
/** <p>A value that specifies the separator for 
  *   the multiple selected items text in the textbox.</p>
  * @field 
  * @type {string}
  */
wijmo.combobox.WijComboboxOptions.prototype.multipleSelectionSeparator = null;
/** <p>A value that determines whether to check the input text against
  *   the text of the selected item when the focus blurs.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * If the text does not match any item, input text will restore
  * to text the selected item or empty if no item is selected.
  */
wijmo.combobox.WijComboboxOptions.prototype.forceSelectionText = null;
/** <p>A function called when any item in list is selected.</p>
  * @field 
  * @type {function}
  */
wijmo.combobox.WijComboboxOptions.prototype.select = null;
/** <p>A value that determines whether input is editable.</p>
  * @field 
  * @type {boolean}
  */
wijmo.combobox.WijComboboxOptions.prototype.isEditable = null;
/** <p>A value that specifies the index of the item to select when using single mode.</p>
  * @field 
  * @type {number|array}
  * @remarks
  * If the selectionMode is "multiple", then this option could be set 
  * to an array of Number which contains the indices of the items to select.
  * If no item is selected, it will return -1.
  * @example
  * //To get the selected item using the selected index:
  *      var selectedIndex = $("# tags ").wijcombobox("option","selectedIndex");  
  *      var selectedItem = $("# tags ").wijcombobox("option","data")[selectedIndex];
  *  // To set the selected item using the selected index:
  *      $("#tags").wijcombobox("option"," selectedIndex", 5);
  */
wijmo.combobox.WijComboboxOptions.prototype.selectedIndex = null;
/** <p>A value that specifies the value of the item to select when using single mode.</p>
  * @field 
  * @type {number|string|object}
  * @remarks
  * If no item is selected, it will return null.
  */
wijmo.combobox.WijComboboxOptions.prototype.selectedValue = null;
/** <p>A value that specifies the input text of the combobox is in dropdown list or not.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * It's readonly option, if user typed text is not in dropdown list, it returns false; 
  * if user selects a item form the dropdown list or typed text in dropdown list, it returns true.
  */
wijmo.combobox.WijComboboxOptions.prototype.inputTextInDropDownList = null;
/** <p>A value that specifies the input text of the combobox.</p>
  * @field 
  * @type {string}
  * @remarks
  * When set the text by code, it will not affect the selectedIndex and selectedValue.
  */
wijmo.combobox.WijComboboxOptions.prototype.text = null;
/** <p>A value indicating the dropdown element will be append to the body or combobox container.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * If the value is true, the dropdown list will be appended to body element.
  * else it will append to the combobox container.
  */
wijmo.combobox.WijComboboxOptions.prototype.ensureDropDownOnBody = null;
/** <p>This event is triggered when the drop-down list is opened.</p>
  * @field 
  * @type {function}
  */
wijmo.combobox.WijComboboxOptions.prototype.open = null;
/** <p>This event is triggered when the drop-down list is closed.</p>
  * @field 
  * @type {function}
  */
wijmo.combobox.WijComboboxOptions.prototype.close = null;
/** <p>A value added to the width of the original HTML select element to account 
  *   for the scroll bar width of the drop-down list.</p>
  * @field 
  * @type {number}
  * @remarks
  * Unit for this value is pixel.
  * Because the width of the scroll bar may be different between browsers 
  * if wijcombobox is initialized with the width of the HTML select element, 
  * the text may be hidden by the scroll bar of wijcombobox.
  */
wijmo.combobox.WijComboboxOptions.prototype.selectElementWidthFix = null;
/** <p>A value added to the width of the original HTML select element to account 
  *   for the scroll bar width of the drop-down list.</p>
  * @field 
  * @type {function}
  * @remarks
  * Unit for this value is pixel.
  * Because the width of the scroll bar may be different between browsers 
  * if wijcombobox is initialized with the width of the HTML select element, 
  * the text may be hidden by the scroll bar of wijcombobox.
  */
wijmo.combobox.WijComboboxOptions.prototype.search = null;
/** <p>A value added to the width of the original HTML select element to account 
  *   for the scroll bar width of the drop-down list.</p>
  * @field 
  * @type {function}
  * @remarks
  * Unit for this value is pixel.
  * Because the width of the scroll bar may be different between browsers 
  * if wijcombobox is initialized with the width of the HTML select element, 
  * the text may be hidden by the scroll bar of wijcombobox.
  */
wijmo.combobox.WijComboboxOptions.prototype.changed = null;
/** <p>A value added to the width of the original HTML select element to account 
  *   for the scroll bar width of the drop-down list.</p>
  * @field 
  * @type {function}
  * @remarks
  * Unit for this value is pixel.
  * Because the width of the scroll bar may be different between browsers 
  * if wijcombobox is initialized with the width of the HTML select element, 
  * the text may be hidden by the scroll bar of wijcombobox.
  */
wijmo.combobox.WijComboboxOptions.prototype.textChanged = null;
/** <p>This event is triggered when the selected index of the combobox is changed.</p>
  * @field 
  * @type {function}
  */
wijmo.combobox.WijComboboxOptions.prototype.selectedIndexChanged = null;
/** <p>A function called when the selected index of the comboBox is about to change. 
  *  Cancellable. If return false, the select operation will be canceled.</p>
  * @field 
  * @type {function}
  */
wijmo.combobox.WijComboboxOptions.prototype.selectedIndexChanging = null;
/** <p>A value that determines the object that contains the options of wijlist.</p>
  * @field
  */
wijmo.combobox.WijComboboxOptions.prototype.listOptions = null;
/** <p>A value that determines whether to fit the width of combobox to that of the dropdown.</p>
  * @field 
  * @type {boolean}
  */
wijmo.combobox.WijComboboxOptions.prototype.adjustComboWidth = null;
/** Provides data for the selected event of the wijcombobox.
  * @interface ISelectedEventArgs
  * @namespace wijmo.combobox
  */
wijmo.combobox.ISelectedEventArgs = function () {};
/** <p>The old item</p>
  * @field 
  * @type {Object}
  */
wijmo.combobox.ISelectedEventArgs.prototype.oldItem = null;
/** <p>The new item.</p>
  * @field 
  * @type {Object}
  */
wijmo.combobox.ISelectedEventArgs.prototype.newItem = null;
/** <p>The old index of selected item.</p>
  * @field 
  * @type {number}
  */
wijmo.combobox.ISelectedEventArgs.prototype.oldIndex = null;
/** <p>The new index of selected item.</p>
  * @field 
  * @type {number}
  */
wijmo.combobox.ISelectedEventArgs.prototype.newIndex = null;
/** Contains information about wijcombobox.select event
  * @interface ISelectEventArgs
  * @namespace wijmo.combobox
  */
wijmo.combobox.ISelectEventArgs = function () {};
/** <p>LI element with this item.</p>
  * @field 
  * @type {element}
  */
wijmo.combobox.ISelectEventArgs.prototype.element = null;
/** <p>wijlist instance.</p>
  * @field 
  * @type {object}
  */
wijmo.combobox.ISelectEventArgs.prototype.list = null;
/** <p>Label of item.</p>
  * @field 
  * @type {string}
  */
wijmo.combobox.ISelectEventArgs.prototype.label = null;
/** <p>Value of item.</p>
  * @field 
  * @type {object}
  */
wijmo.combobox.ISelectEventArgs.prototype.value = null;
/** <p>Could be set in handler to override rendered label of item.</p>
  * @field 
  * @type {string}
  */
wijmo.combobox.ISelectEventArgs.prototype.text = null;
/** Contains information about wijcombobox.search event
  * @interface ISearchEventArgs
  * @namespace wijmo.combobox
  */
wijmo.combobox.ISearchEventArgs = function () {};
/** <p>The datasource of wijcombobox.</p>
  * @field 
  * @type {datasource}
  */
wijmo.combobox.ISearchEventArgs.prototype.datasrc = null;
/** <p>The text to search.</p>
  * @field 
  * @type {string}
  */
wijmo.combobox.ISearchEventArgs.prototype.term = null;
/** Contains information about wijcombobox.textChanged event
  * @interface ITextChangedEventArgs
  * @namespace wijmo.combobox
  */
wijmo.combobox.ITextChangedEventArgs = function () {};
/** <p>The old text of combobox.</p>
  * @field 
  * @type {string}
  */
wijmo.combobox.ITextChangedEventArgs.prototype.oldText = null;
/** <p>The new text of combobox.</p>
  * @field 
  * @type {string}
  */
wijmo.combobox.ITextChangedEventArgs.prototype.newText = null;
typeof wijmo.WidgetOptions != 'undefined' && $.extend(wijmo.combobox.WijComboboxOptions.prototype, wijmo.WidgetOptions.prototype);
})()
})()
