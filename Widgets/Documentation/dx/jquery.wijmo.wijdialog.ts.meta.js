var wijmo = wijmo || {};

(function () {
wijmo.dialog = wijmo.dialog || {};

(function () {
/** @class JQueryUIDialog
  * @widget 
  * @namespace jQuery.wijmo.dialog
  * @extends wijmo.JQueryUIWidget
  */
wijmo.dialog.JQueryUIDialog = function () {};
wijmo.dialog.JQueryUIDialog.prototype = new wijmo.JQueryUIWidget();
/** Returns a jQuery object containing the generated wrapper.
  * @returns {JQuery}
  */
wijmo.dialog.JQueryUIDialog.prototype.widget = function () {};
/** Closes the dialog. */
wijmo.dialog.JQueryUIDialog.prototype.close = function () {};
/** Whether the dialog is currently open. */
wijmo.dialog.JQueryUIDialog.prototype.isOpen = function () {};
/** Moves the dialog to the top of the dialog stack. */
wijmo.dialog.JQueryUIDialog.prototype.moveToTop = function () {};
/** Opens the dialog. */
wijmo.dialog.JQueryUIDialog.prototype.open = function () {};

/** @class */
var JQueryUIDialog_options = function () {};
wijmo.dialog.JQueryUIDialog.prototype.options = $.extend({}, true, wijmo.JQueryUIWidget.prototype.options, new JQueryUIDialog_options());
$.widget("wijmo.JQueryUIDialog", $.wijmo.widget, wijmo.dialog.JQueryUIDialog.prototype);
/** @class wijdialog
  * @widget 
  * @namespace jQuery.wijmo.dialog
  * @extends wijmo.dialog.JQueryUIDialog
  */
wijmo.dialog.wijdialog = function () {};
wijmo.dialog.wijdialog.prototype = new wijmo.dialog.JQueryUIDialog();
/** Removes the wijdialog functionality completely. This returns the element to its pre-init state. */
wijmo.dialog.wijdialog.prototype.destroy = function () {};
/** The pin method prevents the wijdialog from being moved. */
wijmo.dialog.wijdialog.prototype.pin = function () {};
/** The refresh method refreshes the iframe content within the wijdialog. */
wijmo.dialog.wijdialog.prototype.refresh = function () {};
/** The toggle method expands or collapses the content of the wijdialog. */
wijmo.dialog.wijdialog.prototype.toggle = function () {};
/** The minimize method minimizes the wijdialog. */
wijmo.dialog.wijdialog.prototype.minimize = function () {};
/** The maximize method maximizes the wijdialog. */
wijmo.dialog.wijdialog.prototype.maximize = function () {};
/** The restore method restores the wijdialog to its normal size from either the minimized or the maximized state. */
wijmo.dialog.wijdialog.prototype.restore = function () {};
/** The getState method gets the state of the dialog widget.
  * @returns {string} Possible values are: minimized, maximized, and normal.
  */
wijmo.dialog.wijdialog.prototype.getState = function () {};
/** The reset method resets dialog properties such as width and height to their default values. */
wijmo.dialog.wijdialog.prototype.reset = function () {};
/** The open method opens an instance of the wijdialog. */
wijmo.dialog.wijdialog.prototype.open = function () {};
/** The close method closes the dialog widget. */
wijmo.dialog.wijdialog.prototype.close = function () {};

/** @class */
var wijdialog_options = function () {};
/** <p class='defaultValue'>Default value: 'body'</p>
  * <p>Which element the dialog (and overlay, if modal) should be appended to.</p>
  * @field 
  * @type {string}
  * @option
  */
wijdialog_options.prototype.appendTo = 'body';
/** <p class='defaultValue'>Default value: true</p>
  * <p>If set to true, the dialog will automatically open upon initialization. If false, the dialog will stay hidden until the open() method is called.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijdialog_options.prototype.autoOpen = true;
/** <p class='defaultValue'>Default value: []</p>
  * <p>Specifies which buttons should be displayed on the dialog. The context of the callback is the dialog element; if you need access to the button, it is available as the target of the event object.</p>
  * @field 
  * @type {obejct|array}
  * @option
  */
wijdialog_options.prototype.buttons = [];
/** <p class='defaultValue'>Default value: true</p>
  * <p>Specifies whether the dialog should close when it has focus and the user presses the esacpe (ESC) key.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijdialog_options.prototype.closeOnEscape = true;
/** <p class='defaultValue'>Default value: 'Close'</p>
  * <p>Specifies the text for the close button. Note that the close text is visibly hidden when using a standard theme.</p>
  * @field 
  * @type {string}
  * @option
  */
wijdialog_options.prototype.closeText = 'Close';
/** <p class='defaultValue'>Default value: ""</p>
  * <p>The specified class name(s) will be added to the dialog, for additional theming.</p>
  * @field 
  * @type {string}
  * @option
  */
wijdialog_options.prototype.dialogClass = "";
/** <p class='defaultValue'>Default value: true</p>
  * <p>If set to true, the dialog will be draggable by the title bar. Requires the jQuery UI Draggable widget to be included.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijdialog_options.prototype.draggable = true;
/** <p class='defaultValue'>Default value: 'auto'</p>
  * <p>The height of the dialog.</p>
  * @field 
  * @type {number|string}
  * @option
  */
wijdialog_options.prototype.height = 'auto';
/** <p class='defaultValue'>Default value: null</p>
  * <p>If and how to animate the hiding of the dialog.</p>
  * @field 
  * @type {number|string|object}
  * @option 
  * @remarks
  * Multiple types supported:
  * Number: The dialog will fade out while animating the height and width for the specified duration.
  * String: The dialog will be hidden using the specified jQuery UI effect. See the list of effects for possible values.
  * Object: If the value is an object, then effect, delay, duration, and easing properties may be provided. The effect property must be the name of a jQuery UI effect. When using a jQuery UI effect that supports additional settings, you may include those settings in the object and they will be passed to the effect. If duration or easing is omitted, then the default values will be used. If delay is omitted, then no delay is used.
  */
wijdialog_options.prototype.hide = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>The maximum height to which the dialog can be resized, in pixels.</p>
  * @field 
  * @type {number}
  * @option
  */
wijdialog_options.prototype.maxHeight = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>The maximum width to which the dialog can be resized, in pixels.</p>
  * @field 
  * @type {number}
  * @option
  */
wijdialog_options.prototype.maxWidth = null;
/** <p class='defaultValue'>Default value: 150</p>
  * <p>The minimum height to which the dialog can be resized, in pixels.</p>
  * @field 
  * @type {number}
  * @option
  */
wijdialog_options.prototype.minHeight = 150;
/** <p class='defaultValue'>Default value: 150</p>
  * <p>The minimum width to which the dialog can be resized, in pixels.</p>
  * @field 
  * @type {number}
  * @option
  */
wijdialog_options.prototype.minWidth = 150;
/** <p class='defaultValue'>Default value: false</p>
  * <p>If set to true, the dialog will have modal behavior; other items on the page will be disabled, i.e., cannot be interacted with. Modal dialogs create an overlay below the dialog but above other page elements.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijdialog_options.prototype.modal = false;
/** <p>Specifies where the dialog should be displayed. The dialog will handle collisions such that as much of the dialog is visible as possible.</p>
  * @field 
  * @type {string|array|object}
  * @option 
  * @remarks
  * Multiple types supported:
  * Object: Identifies the position of the dialog when opened. The of option defaults to the window, but you can specify another element to position against. You can refer to the jQuery UI Position utility for more details about the various options.
  * String: A string representing the position within the viewport. Possible values: "center", "left", "right", "top", "bottom".
  * Array: An array containing an x, y coordinate pair in pixel offset from the top left corner of the viewport or the name of a possible string value.
  */
wijdialog_options.prototype.position = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>If set to true, the dialog will be resizable. Requires the jQuery UI Resizable widget to be included.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijdialog_options.prototype.resizable = true;
/** <p class='defaultValue'>Default value: null</p>
  * <p>If and how to animate the showing of the dialog.</p>
  * @field 
  * @type {number|string|object}
  * @option 
  * @remarks
  * Number: The dialog will fade in while animating the height and width for the specified duration.
  * String: The dialog will be shown using the specified jQuery UI effect. See the list of effects for possible values.
  * Object: If the value is an object, then effect, delay, duration, and easing properties may be provided. The effect property must be the name of a jQuery UI effect. When using a jQuery UI effect that supports additional settings, you may include those settings in the object and they will be passed to the effect. If duration or easing is omitted, then the default values will be used. If delay is omitted, then no delay is used.
  */
wijdialog_options.prototype.show = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Specifies the title of the dialog. If the value is null, the title attribute on the dialog source element will be used.</p>
  * @field 
  * @type {string}
  * @option
  */
wijdialog_options.prototype.title = null;
/** <p class='defaultValue'>Default value: 300</p>
  * <p>The width of the dialog, in pixels.</p>
  * @field 
  * @type {number}
  * @option
  */
wijdialog_options.prototype.width = 300;
/** <p class='defaultValue'>Default value: true</p>
  * <p>undefined</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijdialog_options.prototype.stack = true;
/** <p class='defaultValue'>Default value: 1000</p>
  * <p>This option specifies the starting z-index for the dialog.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * $("selector").wijdialog({zIndex: 2000});
  */
wijdialog_options.prototype.zIndex = 1000;
/** <p>The captionButtons option determines the caption buttons to show on the wijdialog title bar.</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * The default value for this option is: 
  * {
  * pin: {visible: true, click: self.pin, title: "Pin",
  * iconClassOn: "ui-icon-pin-w", iconClassOff:"ui-icon-pin-s"},
  * refresh: {visible: true, click: self.refresh, title: "Refresh",
  * iconClassOn: "ui-icon-refresh"},
  * toggle: {visible: true, click: self.toggle, title: "Toggle"},
  * minimize: {visible: true, click: self.minimize, title: "Minimize",
  * iconClassOn: "ui-icon-minus"},
  * maximize: {visible: true, click: self.maximize, title: "Maximize",
  * iconClassOn: "ui-icon-extlink"},
  * close: {visible: true, click: self.close,  title: "Close",
  * iconClassOn: "ui-icon-close"}
  * };
  * Each button is represented by an object in this object. 
  * property name: The name of the button.
  * visible: A value specifies whether this button is visible.
  * click: The event handler to handle the click event of this button.
  * iconClassOn: Icon for normal state.
  * iconClassOff: Icon after clicking.
  * @example
  * $("selector").wijdialog({captionButtons: {
  * pin: { visible: false },
  * refresh: { visible: false },
  * toggle: { visible: false },
  * minimize: { visible: false },
  * maximize: { visible: false }
  * }
  * });
  */
wijdialog_options.prototype.captionButtons = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>The collapsingAnimation option determines the animation effect that is used when the wijdialog is collapsed.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("selector").wijdialog({collapsingAnimation:
  * { effect: "puff", duration: 300, easing: "easeOutExpo" }
  * });
  */
wijdialog_options.prototype.collapsingAnimation = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>The expandingAnimation option determines the animation effect that is used when the wijdialog is expanded.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("selector").wijdialog({expandingAnimation:
  * { effect: "puff", duration: 300, easing: "easeOutExpo" }
  * });
  */
wijdialog_options.prototype.expandingAnimation = null;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>This option specifies the URL for the iframe element inside wijdialog.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("selector").wijdialog({contentUrl: 'http://www.google.com'});
  */
wijdialog_options.prototype.contentUrl = "";
/** <p class='defaultValue'>Default value: ""</p>
  * <p>The minimizeZoneElementId option specifies the ID of the DOM element to dock to when wijdialog is minimized.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("selector").wijdialog({minimizeZoneElementId: "zoomId"});
  */
wijdialog_options.prototype.minimizeZoneElementId = "";
/** Triggered when a dialog is about to close. If canceled, the dialog will not close.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijdialog_options.prototype.beforeClose = null;
/** Triggered when the dialog is closed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijdialog_options.prototype.close = null;
/** Triggered when the dialog is created.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijdialog_options.prototype.create = null;
/** Triggered while the dialog is being dragged.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijdialog_options.prototype.drag = null;
/** Triggered when the user starts dragging the dialog.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijdialog_options.prototype.dragStart = null;
/** Triggered after the dialog has been dragged.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijdialog_options.prototype.dragStop = null;
/** Triggered when the dialog gains focus.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijdialog_options.prototype.focus = null;
/** Triggered when the dialog is opened.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijdialog_options.prototype.open = null;
/** Triggered while the dialog is being resized.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {ResizeEventArgs} ui Information about an event.
  */
wijdialog_options.prototype.resize = null;
/** Triggered when the user starts resizing the dialog.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {ResizeEventArgs} ui Information about an event.
  */
wijdialog_options.prototype.resizeStart = null;
/** Triggered after the dialog has been resized.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {ResizeEventArgs} ui Information about an event.
  */
wijdialog_options.prototype.resizeStop = null;
/** The buttonCreating event is called before the caption buttons are created. 
  * It can be used to change the array of the buttons or to change, add, or remove buttons from the title bar.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data Buttons array that will be created.
  */
wijdialog_options.prototype.buttonCreating = null;
/** The stateChanged event is called when the dialog state ("maximized", "minimized", "normal") is changed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.dialog.IStateChangedEventArgs} data Information about an event
  */
wijdialog_options.prototype.stateChanged = null;
/** The blur event is called when the dialog widget loses focus.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.dialog.IBlurEventArgs} data Information about an event
  */
wijdialog_options.prototype.blur = null;
wijmo.dialog.wijdialog.prototype.options = $.extend({}, true, wijmo.dialog.JQueryUIDialog.prototype.options, new wijdialog_options());
$.widget("wijmo.wijdialog", $.wijmo.JQueryUIDialog, wijmo.dialog.wijdialog.prototype);
/** @interface ResizeEventArgs
  * @namespace wijmo.dialog
  */
wijmo.dialog.ResizeEventArgs = function () {};
/** <p>The CSS position of the dialog prior to being resized.</p>
  * @field 
  * @type {Object}
  */
wijmo.dialog.ResizeEventArgs.prototype.originalPosition = null;
/** <p>The current CSS position of the dialog.</p>
  * @field 
  * @type {Object}
  */
wijmo.dialog.ResizeEventArgs.prototype.position = null;
/** <p>The size of the dialog prior to being resized.</p>
  * @field 
  * @type {Object}
  */
wijmo.dialog.ResizeEventArgs.prototype.originalSize = null;
/** <p>The current size of the dialog.</p>
  * @field 
  * @type {Object}
  */
wijmo.dialog.ResizeEventArgs.prototype.size = null;
/** Contains information about wijdialog.stateChanged event
  * @interface IStateChangedEventArgs
  * @namespace wijmo.dialog
  */
wijmo.dialog.IStateChangedEventArgs = function () {};
/** <p>The original state of the dialog box.</p>
  * @field 
  * @type {string}
  */
wijmo.dialog.IStateChangedEventArgs.prototype.originalState = null;
/** <p>The current state of the dialog box.</p>
  * @field 
  * @type {string}
  */
wijmo.dialog.IStateChangedEventArgs.prototype.state = null;
/** Contains information about wijdialog.blur event
  * @interface IBlurEventArgs
  * @namespace wijmo.dialog
  */
wijmo.dialog.IBlurEventArgs = function () {};
/** <p>The DOM element of this dialog.</p>
  * @field 
  * @type {DOMElement}
  */
wijmo.dialog.IBlurEventArgs.prototype.el = null;
})()
})()
