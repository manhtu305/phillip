var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
/** @class c1buttonbasefield
  * @namespace wijmo.grid
  * @extends wijmo.grid.c1basefield
  */
wijmo.grid.c1buttonbasefield = function () {};
wijmo.grid.c1buttonbasefield.prototype = new wijmo.grid.c1basefield();
/** @param {wijmo.grid.IColumn} options
  * @returns {bool}
  */
wijmo.grid.c1buttonbasefield.prototype.test = function (options) {};
/** @class c1buttonbasefield_options
  * @namespace wijmo.grid
  * @extends wijmo.grid.c1basefield_options
  */
wijmo.grid.c1buttonbasefield_options = function () {};
wijmo.grid.c1buttonbasefield_options.prototype = new wijmo.grid.c1basefield_options();
})()
})()
var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
/** @class c1buttonfield
  * @namespace wijmo.grid
  * @extends wijmo.grid.c1buttonbasefield
  */
wijmo.grid.c1buttonfield = function () {};
wijmo.grid.c1buttonfield.prototype = new wijmo.grid.c1buttonbasefield();
/** @param {wijmo.grid.IColumn} options
  * @returns {bool}
  */
wijmo.grid.c1buttonfield.prototype.test = function (options) {};
/** @class c1buttonfield_options
  * @namespace wijmo.grid
  * @extends wijmo.grid.c1buttonbasefield_options
  */
wijmo.grid.c1buttonfield_options = function () {};
wijmo.grid.c1buttonfield_options.prototype = new wijmo.grid.c1buttonbasefield_options();
})()
})()
var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
/** @class c1commandbuttonfield
  * @namespace wijmo.grid
  * @extends wijmo.grid.c1buttonbasefield
  */
wijmo.grid.c1commandbuttonfield = function () {};
wijmo.grid.c1commandbuttonfield.prototype = new wijmo.grid.c1buttonbasefield();
/** @param {wijmo.grid.IColumn} options
  * @returns {bool}
  */
wijmo.grid.c1commandbuttonfield.prototype.test = function (options) {};
/** @class c1commandbuttonfield_options
  * @namespace wijmo.grid
  * @extends wijmo.grid.c1buttonbasefield_options
  */
wijmo.grid.c1commandbuttonfield_options = function () {};
wijmo.grid.c1commandbuttonfield_options.prototype = new wijmo.grid.c1buttonbasefield_options();
})()
})()
