var wijmo = wijmo || {};

(function () {
wijmo.filter = wijmo.filter || {};

(function () {
/** @class wijfilter
  * @widget 
  * @namespace jQuery.wijmo.filter
  * @extends wijmo.wijmoWidget
  */
wijmo.filter.wijfilter = function () {};
wijmo.filter.wijfilter.prototype = new wijmo.wijmoWidget();
/** Force wijfilter to raise the close event.
  * @param {bool} settingsChanged Indicates whether the wijfilter settings was changed or not. If true, then the values of the sortDirection, filterValue and filterOperator options will be passed to the event handler within the second parameter.
  */
wijmo.filter.wijfilter.prototype.triggerClose = function (settingsChanged) {};

/** @class */
var wijfilter_options = function () {};
/** <p>An array of data to get filtering values from where each element is a one-dimensional array or object:
  * [[ val0, ..., valN ], ..., [ val0, ..., valN ]]
  * or
  * [{ prop0: val0, ..., propN: valN }, ..., { prop0: val0, ..., propN: valN }]</p>
  * @field 
  * @type {array}
  * @option 
  * @example
  * $("#element").wijfilter({ data: [{ ID: 0, Name: "Name0" }, { ID: 1, Name: "Name1" }] });
  */
wijfilter_options.prototype.data = null;
/** <p>A value used to identify a column in a data array. Can be string if the data option contains an array of objects or an integer if the data option is an array of arrays.</p>
  * @field 
  * @type {String|Number}
  * @option 
  * @example
  * $("#element").wijfilter({ dataKey: "ID" });
  */
wijfilter_options.prototype.dataKey = null;
/** <p class='defaultValue'>Default value: '&lt;null&gt;'</p>
  * <p>A value used to indicate the text to display for a data item when the value of the data item is null.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#element").wijfilter({ nullDisplayText: "&amp;lt;Null&amp;gt;" });
  */
wijfilter_options.prototype.nullDisplayText = '<null>';
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Specifies the title of the dialog.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#element").wijfilter({ title: "" });
  */
wijfilter_options.prototype.title = "";
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value indicating whether sorting buttons are enabled.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijfilter({ enableSortButtons: false });
  */
wijfilter_options.prototype.enableSortButtons = false;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.filter.IFilterOperator.html'>wijmo.filter.IFilterOperator[]</a></p>
  * <p>An array of available filter operators to select.</p>
  * @field 
  * @type {wijmo.filter.IFilterOperator[]}
  * @option 
  * @example
  * $("#element").wijfilter({ availableOperators: [ { name: "lessorequal", displayName: "Less Or Equal" }] });
  */
wijfilter_options.prototype.availableOperators = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value indicating whether header is visible.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijfilter({ showHeader: true });
  */
wijfilter_options.prototype.showHeader = true;
/** <p>Determines the sort direction.
  * Possible values are: "none", "ascending" and "descending".
  * "none": no sorting.
  * "ascending": sort from smallest to largest.
  * "descending": sort from largest to smallest.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#element").wijgrid({ columns: [{ sortDirection: "none" }] });
  */
wijfilter_options.prototype.sortDirection = null;
/** <p class='defaultValue'>Default value: []</p>
  * <p>A value set for filtering.
  * An array where each value is used as the value in the array of filter operators of the filterOperator option.</p>
  * @field 
  * @type {array}
  * @option 
  * @example
  * $("#element").wijfilter({ filterValue: ["a", "b"] });
  */
wijfilter_options.prototype.filterValue = [];
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.filter.IFilterOperatorEx.html'>wijmo.filter.IFilterOperatorEx[]</a></p>
  * <p class='defaultValue'>Default value: []</p>
  * <p>An array of filter operators.</p>
  * @field 
  * @type {wijmo.filter.IFilterOperatorEx[]}
  * @option 
  * @remarks
  * The widget cannot display more than two operators.
  * @example
  * $("#element").wijfilter({ filterOperator: [ { name: "lessorequal" } ] });
  */
wijfilter_options.prototype.filterOperator = [];
/** The close event handler. A function called when dialog is closed.
  * @event 
  * @param {Object} e jQuery.Event object.
  * @param {wijmo.filter.ICloseEventArgs} args The data with this event.
  * @example
  * Supply a callback function to handle the close event:
  * $("#element").wijfilter({ close: function (e, args) { } });
  * Bind to the event by type:
  * $("#element").bind("wijfilterclose", function (e, args) { });
  */
wijfilter_options.prototype.close = null;
wijmo.filter.wijfilter.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijfilter_options());
$.widget("wijmo.wijfilter", $.wijmo.widget, wijmo.filter.wijfilter.prototype);
/** Represents a filter operator
  * @interface IFilterOperator
  * @namespace wijmo.filter
  */
wijmo.filter.IFilterOperator = function () {};
/** <p>The filter name.</p>
  * @field 
  * @type {string}
  */
wijmo.filter.IFilterOperator.prototype.name = null;
/** <p>The name of the filter in the dropdown list (optional).</p>
  * @field 
  * @type {string}
  */
wijmo.filter.IFilterOperator.prototype.displayName = null;
/** Represents a filter operator with the condition property.
  * @interface IFilterOperatorEx
  * @namespace wijmo.filter
  * @extends wijmo.filter.IFilterOperator
  */
wijmo.filter.IFilterOperatorEx = function () {};
/** <p>The logical condition to other operators, "or" or "and".</p>
  * @field 
  * @type {string}
  */
wijmo.filter.IFilterOperatorEx.prototype.condition = null;
/** Provides data for the close event of the wijfilter.
  * @interface ICloseEventArgs
  * @namespace wijmo.filter
  */
wijmo.filter.ICloseEventArgs = function () {};
/** <p>A new sort direction value.</p>
  * @field 
  * @type {string}
  */
wijmo.filter.ICloseEventArgs.prototype.sortDirection = null;
/** <p>A new filter value.</p>
  * @field 
  * @type {array}
  */
wijmo.filter.ICloseEventArgs.prototype.filterValue = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.filter.IFilterOperatorEx.html'>wijmo.filter.IFilterOperatorEx[]</a></p>
  * <p>A new filter operator value.</p>
  * @field 
  * @type {wijmo.filter.IFilterOperatorEx[]}
  */
wijmo.filter.ICloseEventArgs.prototype.filterOperator = null;
typeof wijmo.filter.IFilterOperator != 'undefined' && $.extend(wijmo.filter.IFilterOperatorEx.prototype, wijmo.filter.IFilterOperator.prototype);
})()
})()
