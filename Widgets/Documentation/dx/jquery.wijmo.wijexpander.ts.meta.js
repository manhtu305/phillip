var wijmo = wijmo || {};

(function () {
wijmo.expander = wijmo.expander || {};

(function () {
/** @class wijexpander
  * @widget 
  * @namespace jQuery.wijmo.expander
  * @extends wijmo.wijmoWidget
  */
wijmo.expander.wijexpander = function () {};
wijmo.expander.wijexpander.prototype = new wijmo.wijmoWidget();
/** Removes the wijexpander functionality completely. This returns the element to its pre-init state. */
wijmo.expander.wijexpander.prototype.destroy = function () {};
/** Collapses the content panel.
  * @returns {bool}
  */
wijmo.expander.wijexpander.prototype.collapse = function () {};
/** Expands the content panel.
  * @returns {bool}
  */
wijmo.expander.wijexpander.prototype.expand = function () {};

/** @class */
var wijexpander_options = function () {};
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines if the widget can be collapsed or expanded through user interaction.Set this option to false to disable collapsing and expanding in the widget.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijexpander({ allowExpand: false });
  */
wijexpander_options.prototype.allowExpand = true;
/** <p class='defaultValue'>Default value: 'slide'</p>
  * <p>Determines the animation easing effect; set this option to false in order to disable animation.
  * Note that custom easing effects require the UI Effects Core. Additional options 
  * that are available for the animation function include:
  * expand - value of true indicates that content element must be expanded.
  * horizontal - value of true indicates that expander is horizontally 
  * orientated (when expandDirection is left or right).
  * content - jQuery object that contains content element to be expanded or 
  * 			collapsed.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#expander2").wijexpander({
  *            animated: "custom1"
  *        });
  *        jQuery.wijmo.wijexpander.animations.custom1 = function (options) {
  *            this.slide(options, {
  *                easing: "easeInBounce",
  *                duration: 900
  *            });
  *        }
  */
wijexpander_options.prototype.animated = 'slide';
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the URL to the external content. For example, 
  * http://componentone.com/ for the ComponentOne Web site.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#element").wijexpander({ contentUrl: "http://componentone.com/" });
  */
wijexpander_options.prototype.contentUrl = "";
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines the visibility state of the content panel. If true, the 
  * content element is visible.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijexpander({ expanded: false });
  */
wijexpander_options.prototype.expanded = true;
/** <p class='defaultValue'>Default value: 'bottom'</p>
  * <p>Determines the content expand direction. Available values are top, right, bottom, and left.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#element").wijexpander({ expandDirection: "right" });
  */
wijexpander_options.prototype.expandDirection = 'bottom';
/** Occurs before the content area collapses. 
  * Return false or call event.preventDefault() in order to cancel event and 
  * prevent the content area from collapsing.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijexpander_options.prototype.beforeCollapse = null;
/** Occurs before the content area expands. 
  * Return false or call event.preventDefault() in order to cancel event and 
  * prevent the content area from expanding.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijexpander_options.prototype.beforeExpand = null;
/** Occurs after the content area collapses.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijexpander_options.prototype.afterCollapse = null;
/** Occurs after the content area expands.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijexpander_options.prototype.afterExpand = null;
wijmo.expander.wijexpander.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijexpander_options());
$.widget("wijmo.wijexpander", $.wijmo.widget, wijmo.expander.wijexpander.prototype);
})()
})()
