var wijmo = wijmo || {};

(function () {
wijmo.radio = wijmo.radio || {};

(function () {
/** @class wijradio
  * @widget 
  * @namespace jQuery.wijmo.radio
  * @extends wijmo.wijmoWidget
  */
wijmo.radio.wijradio = function () {};
wijmo.radio.wijradio.prototype = new wijmo.wijmoWidget();
/** Use the refresh method to set the radio button's style. */
wijmo.radio.wijradio.prototype.refresh = function () {};
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.radio.wijradio.prototype.destroy = function () {};

/** @class */
var wijradio_options = function () {};
/** <p class='defaultValue'>Default value: null</p>
  * <p>Causes the radio button to appear in the selected state.</p>
  * @field 
  * @type {object}
  * @option
  */
wijradio_options.prototype.checked = null;
/** A function called when checked state is changed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.radio.IChangedEventArgs} data Information about an event
  */
wijradio_options.prototype.changed = null;
wijmo.radio.wijradio.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijradio_options());
$.widget("wijmo.wijradio", $.wijmo.widget, wijmo.radio.wijradio.prototype);
/** Contains information about wijradio.changed event
  * @interface IChangedEventArgs
  * @namespace wijmo.radio
  */
wijmo.radio.IChangedEventArgs = function () {};
/** <p>The state of the radio button.</p>
  * @field 
  * @type {bool}
  */
wijmo.radio.IChangedEventArgs.prototype.checked = null;
})()
})()
