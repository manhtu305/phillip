var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
/** @class c1field
  * @namespace wijmo.grid
  * @extends wijmo.grid.c1basefield
  */
wijmo.grid.c1field = function () {};
wijmo.grid.c1field.prototype = new wijmo.grid.c1basefield();
/** @param {wijmo.grid.IColumn} column
  * @returns {bool}
  */
wijmo.grid.c1field.prototype.test = function (column) {};
/** @param {wijmo.grid.c1basefield} column
  * @returns {bool}
  */
wijmo.grid.c1field.prototype.isGroupedColumn = function (column) {};
/** @class c1field_options
  * @namespace wijmo.grid
  * @extends wijmo.grid.c1basefield_options
  */
wijmo.grid.c1field_options = function () {};
wijmo.grid.c1field_options.prototype = new wijmo.grid.c1basefield_options();
})()
})()
var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
/** @class c1booleanfield
  * @namespace wijmo.grid
  * @extends wijmo.grid.c1field
  */
wijmo.grid.c1booleanfield = function () {};
wijmo.grid.c1booleanfield.prototype = new wijmo.grid.c1field();
/** @param {wijmo.grid.IColumn} column
  * @returns {bool}
  */
wijmo.grid.c1booleanfield.prototype.test = function (column) {};
})()
})()
