var wijmo = wijmo || {};

(function () {
wijmo.gauge = wijmo.gauge || {};

(function () {
/** @class wijradialgauge
  * @widget 
  * @namespace jQuery.wijmo.gauge
  * @extends wijmo.gauge.wijgauge
  */
wijmo.gauge.wijradialgauge = function () {};
wijmo.gauge.wijradialgauge.prototype = new wijmo.gauge.wijgauge();
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.gauge.wijradialgauge.prototype.destroy = function () {};

/** @class */
var wijradialgauge_options = function () {};
/** <p class='defaultValue'>Default value: 'auto'</p>
  * <p>A value that indicates the radius of the radial gauge in pixels, or use "auto" to have the gauge indicators fill the
  * gauge area.</p>
  * @field 
  * @type {number|string}
  * @option 
  * @remarks
  * Values between 50 and 200 are most useful when you use the default size for the gauge area.
  */
wijradialgauge_options.prototype.radius = 'auto';
/** <p class='defaultValue'>Default value: 0</p>
  * <p>A value that indicates in degrees where to start the lowest number in the numeric labels and tickMarks around the face
  * of the RadialGauge.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * The default value of 0 degrees renders the lowest number to the left of the origin.
  * A value of -90 degrees renders the lowest number below the origin.
  * A value of 90 degrees renders the lowest number above the origin.
  * A value of 180 degrees renders the lowest number to the right of the origin.
  */
wijradialgauge_options.prototype.startAngle = 0;
/** <p class='defaultValue'>Default value: 180</p>
  * <p>A value that indicates in degrees where to render the highest number in the numeric labels and tickMarks in relation
  * to the startAngle.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * // This code example renders the labels and tickmarks from a starting angle of -45 degrees to a sweep angle of 270
  * degrees, so that the numbers render three quarters of the way around the face
  *    $(document).ready(function () {
  *        $("#radialgauge1").wijradialgauge({
  *         value: 90,
  *         startAngle: -45,
  *         sweepAngle: 270
  *     });
  * });
  */
wijradialgauge_options.prototype.sweepAngle = 180;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.gauge.gauge_pointer.html'>wijmo.gauge.gauge_pointer</a></p>
  * <p>A value that includes all settings of the gauge pointer.</p>
  * @field 
  * @type {wijmo.gauge.gauge_pointer}
  * @option 
  * @example
  * // The example above renders the pointer as a purple-outlined blue rectangle of 125% the length of the radius by 10
  * // pixels wide, offset back through the cap by 50% of the length of the radius.
  *    $(document).ready(function () {
  *        $("#radialgauge1").wijradialgauge({
  *         value: 90,
  *         cap: {visible: true},
  *         pointer: {
  *             length: 1.25,
  *             offset: 0.5,
  *             shape: "rect",
  *             style: { fill: "blue", stroke: "purple"},
  *             width: 10
  *         }
  *     });
  * });
  */
wijradialgauge_options.prototype.pointer = null;
/** <p>Sets the starting point for the center of the radial gauge.</p>
  * @field 
  * @option
  */
wijradialgauge_options.prototype.origin = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.gauge.gauge_label.html'>wijmo.gauge.gauge_label</a></p>
  * <p>Sets all of the appearance options of the numeric labels that appear around the edge of the gauge.</p>
  * @field 
  * @type {wijmo.gauge.gauge_label}
  * @option 
  * @example
  * // This example sets the color for the labels to purple, and the font to 14 point, bold, Times New Roman.
  *    $(document).ready(function () {
  *        $("#radialgauge1").wijradialgauge({
  *         value: 90,
  *         labels: {
  *         style: {
  *             fill: "purple",
  *                 "font-size": "14pt",
  *                 "font-weight": "bold",
  *                 "font-family": "Times New Roman"
  *             }
  *         }
  *     });
  * });
  */
wijradialgauge_options.prototype.labels = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.gauge.gauge_tick.html'>wijmo.gauge.gauge_tick</a></p>
  * <p>Sets appearance options for the minor tick marks that appear between  the numeric labels around the face of the gauge,
  * indicating numeric values between the major tick marks.</p>
  * @field 
  * @type {wijmo.gauge.gauge_tick}
  * @option 
  * @example
  * // This example renders the minor tick marks as purple crosses, at an interval of once every 2 numbers
  * $(document).ready(function () {
  *     $("#radialgauge1").wijradialgauge({
  *         value: 90,
  *         tickMinor: {
  *             position: "inside",
  *             style: { fill: "#1E395B", stroke: "purple"},
  *             factor: 2,
  *             marker: 'cross',
  *             visible: true,
  *             offset: 30,
  *             interval: 2
  *         }
  *     });
  * });
  */
wijradialgauge_options.prototype.tickMinor = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.gauge.gauge_tick.html'>wijmo.gauge.gauge_tick</a></p>
  * <p>Sets appearance options for the major tick marks that appear next to the numeric labels around the face of the gauge.</p>
  * @field 
  * @type {wijmo.gauge.gauge_tick}
  * @option 
  * @example
  * // This example renders the major tick marks as slightly larger purple filled diamonds with blue outlines, at an interval
  * // of once every 20 numbers
  * $(document).ready(function () {
  *     $("#radialgauge1").wijradialgauge({
  *         value: 90,
  *         tickMajor: {
  *             position: "inside",
  *             style: { fill: "purple", stroke: "#1E395B"},
  *             factor: 2.5,
  *             marker: 'diamond',
  *             visible: true,
  *             offset: 27,
  *             interval: 20
  *         }
  *     });
  * });
  */
wijradialgauge_options.prototype.tickMajor = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.gauge.radialgauge_cap.html'>wijmo.gauge.radialgauge_cap</a></p>
  * <p>Sets the size, color, and other properties of the circle at the center of the gauge that anchors the pointer.</p>
  * @field 
  * @type {wijmo.gauge.radialgauge_cap}
  * @option 
  * @example
  * // This example creates a rectangular cap that begins 10 pixels to the left of and 10 pixels above the origin, and is 20
  * // pixels wide by 20 pixels tall. The cap is filled with purple and has no outline (stroke).
  * $(document).ready(function () {
  *     $("#radialgauge1").wijradialgauge({
  *         value: 180,
  *         max: 200,
  *         min: 0,
  *         cap: {
  *             template: function (ui) {
  *                 var origin = ui.origin;
  *                 return ui.canvas.rect(origin.x -10, origin.y -10, 20, 20).attr({fill: "purple", stroke: "none"});
  *             }
  *         }
  *     });
  * });
  */
wijradialgauge_options.prototype.cap = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.gauge.gauge_face.html'>wijmo.gauge.gauge_face</a></p>
  * <p>Sets or draws the image or shape to use for the face of the gauge and the background area.</p>
  * @field 
  * @type {wijmo.gauge.gauge_face}
  * @option 
  * @remarks
  * The origin is the center of the gauge, but the image draws from the top left, so we first calculate the starting point of the
  * top left based on the origin, and we calculate the width and height based on the radius of the face. The radius of the face is
  * half of the min of the width and height.
  * Note: The fill property is defined using the Raphael framework. Please see the Raphael Element attr method for more information.
  * The face can be filled with a simple color, or a gradient. The default fill is a radial gradient, indicated by the r in the
  * fill property.
  * @example
  * // This example uses a custom image for the face of the gauge. The argument that we name ui in the example is a JSON object.
  * // This object has a canvas, which is a Raphael paper object, and we use the image method of the Raphael paper that takes five
  * // parameters: source, x, y, width, and height. See the Raphael documentation for more information.
  * $(document).ready(function () {
  *     $("#radialgauge1").wijradialgauge({
  *         value: 90,
  *         radius: 120,
  *         face: {
  *             style: {},
  *             template: function (ui) {
  *                 var url = "images/customGaugeFace.png";
  *                 return ui.canvas.image(url, ui.origin.x -ui.r, ui.origin.y -ui.r, ui.r * 2, ui.r * 2);
  *             }
  *         }
  *     });
  * });
  */
wijradialgauge_options.prototype.face = null;
wijmo.gauge.wijradialgauge.prototype.options = $.extend({}, true, wijmo.gauge.wijgauge.prototype.options, new wijradialgauge_options());
$.widget("wijmo.wijradialgauge", $.wijmo.wijgauge, wijmo.gauge.wijradialgauge.prototype);
/** @interface radialgauge_cap
  * @namespace wijmo.gauge
  */
wijmo.gauge.radialgauge_cap = function () {};
/** <p>A value that indicates the radius of the cap in pixels.</p>
  * @field 
  * @type {number}
  */
wijmo.gauge.radialgauge_cap.prototype.radius = null;
/** <p>A value that contains the fill color and outline color (stroke) of the cap.</p>
  * @field
  */
wijmo.gauge.radialgauge_cap.prototype.style = null;
/** <p>A value that indicates whether the cap shows behind the pointer or in front.</p>
  * @field 
  * @type {boolean}
  */
wijmo.gauge.radialgauge_cap.prototype.behindPointer = null;
/** <p>A value that indicates whether to show the cap.</p>
  * @field 
  * @type {boolean}
  */
wijmo.gauge.radialgauge_cap.prototype.visible = null;
/** <p>A JavaScript callback value that returns a Raphael element (or set) that draws the cap for the pointer.</p>
  * @field 
  * @type {Function}
  * @remarks
  * If you are only using one shape, the function returns a Raphael element. If you define multiple shapes,  have the function
  * create a Raphael set object push all of the Raphael elements to the set, and return the set to wijradialgauge.
  * In order to use the template, you must know how to draw Raphael graphics. For more information, see the Raphael documentation.
  * The cap template's callback contains one argument with two properties:
  * origin -- The starting point from which to draw the center of the cap.
  * This argument is defined by x and y coordinates.
  * canvas -- A Raphael paper object that you can use to draw the custom
  * graphic to use as the cap.
  */
wijmo.gauge.radialgauge_cap.prototype.template = null;
})()
})()
