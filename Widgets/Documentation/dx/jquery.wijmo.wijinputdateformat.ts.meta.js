var wijmo = wijmo || {};

(function () {
wijmo.input = wijmo.input || {};

(function () {
/** @enum DescriptorType
  * @namespace wijmo.input
  */
wijmo.input.DescriptorType = {
/**  */
liternal: 0,
/**  */
OneDigitYear: 1,
/**  */
TwoDigitYear: 2,
/**  */
FourDigitYear: 3,
/**  */
TwoDigitMonth: 4,
/**  */
Month: 5,
/**  */
AbbreviatedMonthNames: 6,
/**  */
MonthNames: 7,
/**  */
EraYear: 8,
/**  */
TwoEraYear: 9,
/**  */
EraName: 10,
/**  */
TwoEraName: 11,
/**  */
ThreeEraName: 12,
/**  */
EraYearBig: 13,
/**  */
AD: 14,
/**  */
TwoDigityDayOfMonth: 15,
/**  */
DayOfMonth: 16,
/**  */
AbbreviatedDayNames: 17,
/**  */
DayNames: 18,
/**  */
h: 19,
/**  */
hh: 20,
/**  */
H: 21,
/**  */
HH: 22,
/**  */
ShortAmPm: 23,
/**  */
AmPm: 24,
/**  */
mm: 25,
/**  */
m: 26,
/**  */
ss: 27,
/**  */
s: 28 
};
/** @interface IDateOptions
  * @namespace wijmo.input
  */
wijmo.input.IDateOptions = function () {};
/** <p>Indicates the culture that the format library will use.</p>
  * @field 
  * @type {string}
  */
wijmo.input.IDateOptions.prototype.culture = null;
/** <p>Indicates the cultureCalendar that the format library will use.</p>
  * @field 
  * @type {string}
  */
wijmo.input.IDateOptions.prototype.cultureCalendar = null;
/** <p>Determines string designator for hours that are "ante meridiem" (before noon).</p>
  * @field 
  * @type {string}
  */
wijmo.input.IDateOptions.prototype.amDesignator = null;
/** <p>Determines the string designator for hours that are "post meridiem" (after noon).</p>
  * @field 
  * @type {string}
  */
wijmo.input.IDateOptions.prototype.pmDesignator = null;
/** <p>A boolean value determines the range of hours that can be entered in the control.</p>
  * @field 
  * @type {boolean}
  */
wijmo.input.IDateOptions.prototype.hour12As0 = null;
/** <p>A boolean value determines whether to express midnight as 24:00.</p>
  * @field 
  * @type {boolean}
  */
wijmo.input.IDateOptions.prototype.midnightAs0 = null;
})()
})()
