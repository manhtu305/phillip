var wijmo = wijmo || {};

(function () {
wijmo.exporter = wijmo.exporter || {};

(function () {
/** @param obj
  * @param {function} replacer
  * @returns {string}
  */
wijmo.exporter.toJSON = function (obj, replacer) {};
/** @param {wijmo.exporter.ExportSetting} setting
  * @param content
  * @returns {void}
  */
wijmo.exporter.exportFile = function (setting, content) {};
/** @param {wijmo.exporter.ExportSetting} setting
  * @param content
  * @param {function} success
  * @returns {void}
  */
wijmo.exporter.exportChunk = function (setting, content, success) {};
/** @param {string} serviceUrl
  * @param {string} content
  * @param {string} token
  */
wijmo.exporter.exportAllPages = function (serviceUrl, content, token) {};
/** Export file type options: Xls, Xlsx, Cvs, Pdf, Png, Jpg,Bmp,Gif and Tiff.
  * @enum ExportFileType
  * @namespace wijmo.exporter
  */
wijmo.exporter.ExportFileType = {
/**  */
Xls: 0,
/**  */
Xlsx: 1,
/**  */
Csv: 2,
/**  */
Pdf: 3,
/**  */
Png: 4,
/**  */
Jpg: 5,
/**  */
Bmp: 6,
/**  */
Gif: 7,
/**  */
Tiff: 8 
};
/** Export mode
  * @enum ExportMethod
  * @namespace wijmo.exporter
  */
wijmo.exporter.ExportMethod = {
/** Sending markup to the service for exporting. */
Content: 0,
/** Sending widget options to the service for exporting. */
Options: 1 
};
/** Export setting.
  * @interface ExportSetting
  * @namespace wijmo.exporter
  */
wijmo.exporter.ExportSetting = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.exporter.ExportFileType.html'>wijmo.exporter.ExportFileType</a></p>
  * <p>Export file type.</p>
  * @field 
  * @type {wijmo.exporter.ExportFileType}
  */
wijmo.exporter.ExportSetting.prototype.exportFileType = null;
/** <p>The name of exported file.</p>
  * @field 
  * @type {string}
  */
wijmo.exporter.ExportSetting.prototype.fileName = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.exporter.PdfSetting.html'>wijmo.exporter.PdfSetting</a></p>
  * <p>Export pdf setting.</p>
  * @field 
  * @type {wijmo.exporter.PdfSetting}
  */
wijmo.exporter.ExportSetting.prototype.pdf = null;
/** <p>The url of export service.</p>
  * @field 
  * @type {string}
  */
wijmo.exporter.ExportSetting.prototype.serviceUrl = null;
/** <p>Customize the response data handling.</p>
  * @field 
  * @type {function}
  */
wijmo.exporter.ExportSetting.prototype.receiver = null;
/** <p>Customize sending the request data to service.</p>
  * @field 
  * @type {function}
  */
wijmo.exporter.ExportSetting.prototype.sender = null;
/** <p>If set to “application/json”, the request data with json format will be sent to service. 
  *   And receiver should be set. Otherwise, data will be set as “x-www-form-urlencoded” type.</p>
  * @field 
  * @type {string}
  */
wijmo.exporter.ExportSetting.prototype.contentType = null;
/** @interface PdfSetting
  * @namespace wijmo.exporter
  */
wijmo.exporter.PdfSetting = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.exporter.PageSize.html'>wijmo.exporter.PageSize</a></p>
  * <p>The page size, the unit is point. It takes effect when paperKind is custom. Used only for grid widget.</p>
  * @field 
  * @type {wijmo.exporter.PageSize}
  */
wijmo.exporter.PdfSetting.prototype.pageSize = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.exporter.Margins.html'>wijmo.exporter.Margins</a></p>
  * <p>The page margins, the unit is point. Used only for grid widget.</p>
  * @field 
  * @type {wijmo.exporter.Margins}
  */
wijmo.exporter.PdfSetting.prototype.margins = null;
/** <p>Auto adjust the content size to make it fit pdf page’s width. Used only for grid widget.</p>
  * @field 
  * @type {boolean}
  */
wijmo.exporter.PdfSetting.prototype.autoFitWidth = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.exporter.PaperKind.html'>wijmo.exporter.PaperKind</a></p>
  * <p>The paperKind of the pdf page. It is corresponding to PaperKind Enumeration. Used only for grid widget.</p>
  * @field 
  * @type {wijmo.exporter.PaperKind}
  */
wijmo.exporter.PdfSetting.prototype.paperKind = null;
/** <p>A boolean value indicates page orientation for the pdf document. Used only for grid widget.</p>
  * @field 
  * @type {boolean}
  */
wijmo.exporter.PdfSetting.prototype.landscape = null;
/** <p>The name of the person that created the pdf document.</p>
  * @field 
  * @type {string}
  */
wijmo.exporter.PdfSetting.prototype.author = null;
/** <p>The name of the application that created the original document.</p>
  * @field 
  * @type {string}
  */
wijmo.exporter.PdfSetting.prototype.creator = null;
/** <p>The keywords associated with the pdf document.</p>
  * @field 
  * @type {string}
  */
wijmo.exporter.PdfSetting.prototype.keywords = null;
/** <p>The name of the application that created the pdf document.</p>
  * @field 
  * @type {string}
  */
wijmo.exporter.PdfSetting.prototype.producer = null;
/** <p>The subject of the pdf document.</p>
  * @field 
  * @type {string}
  */
wijmo.exporter.PdfSetting.prototype.subject = null;
/** <p>The title of the pdf document.</p>
  * @field 
  * @type {string}
  */
wijmo.exporter.PdfSetting.prototype.title = null;
/** <p>A boolean value indicates whether the user can copy contents from the pdf document.</p>
  * @field 
  * @type {boolean}
  */
wijmo.exporter.PdfSetting.prototype.allowCopyContent = null;
/** <p>Customize the response data handling.</p>
  * @field 
  * @type {boolean}
  */
wijmo.exporter.PdfSetting.prototype.allowEditAnnotations = null;
/** <p>A boolean value indicates whether the user can edit the contents of the pdf document.</p>
  * @field 
  * @type {boolean}
  */
wijmo.exporter.PdfSetting.prototype.allowEditContent = null;
/** <p>A boolean value indicates whether the user can print the pdf document.</p>
  * @field 
  * @type {boolean}
  */
wijmo.exporter.PdfSetting.prototype.allowPrint = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.exporter.PdfEncryptionType.html'>wijmo.exporter.PdfEncryptionType</a></p>
  * <p>Specifies the type of encryption used for pdf document. It is corresponding to PdfEncryptionType Enumeration.</p>
  * @field 
  * @type {wijmo.exporter.PdfEncryptionType}
  */
wijmo.exporter.PdfSetting.prototype.encryption = null;
/** <p>Specifies the password required to change permissions for the pdf document.</p>
  * @field 
  * @type {string}
  */
wijmo.exporter.PdfSetting.prototype.ownerPassword = null;
/** <p>Specifies the password required to open the pdf document.</p>
  * @field 
  * @type {string}
  */
wijmo.exporter.PdfSetting.prototype.userPassword = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.exporter.CompressionType.html'>wijmo.exporter.CompressionType</a></p>
  * <p>The compression level to use when saving the pdf document. It is corresponding to CompressionEnum Enumeration.</p>
  * @field 
  * @type {wijmo.exporter.CompressionType}
  */
wijmo.exporter.PdfSetting.prototype.compression = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.exporter.ImageQuality.html'>wijmo.exporter.ImageQuality</a></p>
  * <p>The image quality to use when saving the pdf document. It is corresponding to ImageQualityEnum Enumeration.</p>
  * @field 
  * @type {wijmo.exporter.ImageQuality}
  */
wijmo.exporter.PdfSetting.prototype.imageQuality = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.exporter.FontType.html'>wijmo.exporter.FontType</a></p>
  * <p>It indicates how fonts should be encoded when saving the document. It is corresponding to FontTypeEnum Enumeration.</p>
  * @field 
  * @type {wijmo.exporter.FontType}
  */
wijmo.exporter.PdfSetting.prototype.fontType = null;
/** The font type
  * @enum FontType
  * @namespace wijmo.exporter
  */
wijmo.exporter.FontType = {
/** Use only standard Pdf fonts (Helvetica, Times, Symbol). */
Standard: 0,
/** Use TrueType fonts, no embedding (viewer must have fonts installed). */
TrueType: 1,
/** Use embedded TrueType fonts. */
Embedded: 2 
};
/** The image quality.
  * @enum ImageQuality
  * @namespace wijmo.exporter
  */
wijmo.exporter.ImageQuality = {
/** Low quality, small file size. */
Low: 0,
/** Medium quality, medium file size. */
Medium: 1,
/** High quality, medium/large file size. */
Default: 2,
/** Highest quality, largest file size. */
High: 3 
};
/** The compression type.
  * @enum CompressionType
  * @namespace wijmo.exporter
  */
wijmo.exporter.CompressionType = {
/** High compression, fast save. */
Default: 0,
/** No compression (useful for debugging). */
None: 1,
/** Low compression, fastest save. */
BestSpeed: 2,
/** Highest compression, slowest save. */
BestCompression: 3 
};
/** The Pdf encryption type.
  * @enum PdfEncryptionType
  * @namespace wijmo.exporter
  */
wijmo.exporter.PdfEncryptionType = {
/** Encryption is unavailable due to FIPS compliance (MD5 and AES128 are not FIPS-compliant). */
NotPermit: 0,
/** Standard 40 bit encryption algorithm. */
Standard40: 1,
/** Standard 128 bit encryption algorithm. */
Standard128: 2,
/** AES 128 bit encryption algorithm. */
Aes128: 3 
};
/** The size of page.
  * @interface PageSize
  * @namespace wijmo.exporter
  */
wijmo.exporter.PageSize = function () {};
/** <p>The width of page size.</p>
  * @field 
  * @type {number}
  */
wijmo.exporter.PageSize.prototype.Width = null;
/** <p>The height of page size.</p>
  * @field 
  * @type {number}
  */
wijmo.exporter.PageSize.prototype.Height = null;
/** The margin settings.
  * @interface Margins
  * @namespace wijmo.exporter
  */
wijmo.exporter.Margins = function () {};
/** <p>The top of margin.</p>
  * @field 
  * @type {number}
  */
wijmo.exporter.Margins.prototype.top = null;
/** <p>The left of margin.</p>
  * @field 
  * @type {number}
  */
wijmo.exporter.Margins.prototype.left = null;
/** <p>The bottom of margin.</p>
  * @field 
  * @type {number}
  */
wijmo.exporter.Margins.prototype.bottom = null;
/** <p>The right of margin.</p>
  * @field 
  * @type {number}
  */
wijmo.exporter.Margins.prototype.right = null;
/** Specifies the standard paper sizes.
  * @enum PaperKind
  * @namespace wijmo.exporter
  */
wijmo.exporter.PaperKind = {
/** The paper size is defined by the user. */
Custom: 0,
/** Letter paper (8.5 in. by 11 in.). */
Letter: 1,
/** Letter small paper (8.5 in. by 11 in.). */
LetterSmall: 2,
/** Tabloid paper (11 in. by 17 in.). */
Tabloid: 3,
/** Ledger paper (17 in. by 11 in.). */
Ledger: 4,
/** Legal paper (8.5 in. by 14 in.). */
Legal: 5,
/** Statement paper (5.5 in. by 8.5 in.). */
Statement: 6,
/** Executive paper (7.25 in. by 10.5 in.). */
Executive: 7,
/** A3 paper (297 mm by 420 mm). */
A3: 8,
/** A4 paper (210 mm by 297 mm). */
A4: 9,
/** A4 small paper (210 mm by 297 mm). */
A4Small: 10,
/** A5 paper (148 mm by 210 mm). */
A5: 11,
/** B4 paper (250 mm by 353 mm). */
B4: 12,
/** B5 paper (176 mm by 250 mm). */
B5: 13,
/** Folio paper (8.5 in. by 13 in.). */
Folio: 14,
/** Quarto paper (215 mm by 275 mm). */
Quarto: 15,
/** Standard paper (10 in. by 14 in.). */
Standard10x14: 16,
/** Standard paper (11 in. by 17 in.). */
Standard11x17: 17,
/** Note paper (8.5 in. by 11 in.). */
Note: 18,
/** #9 envelope (3.875 in. by 8.875 in.). */
Number9Envelope: 19,
/** #10 envelope (4.125 in. by 9.5 in.). */
Number10Envelope: 20,
/** #11 envelope (4.5 in. by 10.375 in.). */
Number11Envelope: 21,
/** #12 envelope (4.75 in. by 11 in.). */
Number12Envelope: 22,
/** #14 envelope (5 in. by 11.5 in.). */
Number14Envelope: 23,
/** C paper (17 in. by 22 in.). */
CSheet: 24,
/** D paper (22 in. by 34 in.). */
DSheet: 25,
/** E paper (34 in. by 44 in.). */
ESheet: 26,
/** DL envelope (110 mm by 220 mm). */
DLEnvelope: 27,
/** C5 envelope (162 mm by 229 mm). */
C5Envelope: 28,
/** C3 envelope (324 mm by 458 mm). */
C3Envelope: 29,
/** C4 envelope (229 mm by 324 mm). */
C4Envelope: 30,
/** C6 envelope (114 mm by 162 mm). */
C6Envelope: 31,
/** C65 envelope (114 mm by 229 mm). */
C65Envelope: 32,
/** B4 envelope (250 mm by 353 mm). */
B4Envelope: 33,
/** B5 envelope (176 mm by 250 mm). */
B5Envelope: 34,
/** B6 envelope (176 mm by 125 mm). */
B6Envelope: 35,
/** Italy envelope (110 mm by 230 mm). */
ItalyEnvelope: 36,
/** Monarch envelope (3.875 in. by 7.5 in.). */
MonarchEnvelope: 37,
/** 6 3/4 envelope (3.625 in. by 6.5 in.). */
PersonalEnvelope: 38,
/** US standard fanfold (14.875 in. by 11 in.). */
USStandardFanfold: 39,
/** German standard fanfold (8.5 in. by 12 in.). */
GermanStandardFanfold: 40,
/** German legal fanfold (8.5 in. by 13 in.). */
GermanLegalFanfold: 41,
/** ISO B4 (250 mm by 353 mm). */
IsoB4: 42,
/** Japanese postcard (100 mm by 148 mm). */
JapanesePostcard: 43,
/** Standard paper (9 in. by 11 in.). */
Standard9x11: 44,
/** Standard paper (10 in. by 11 in.). */
Standard10x11: 45,
/** Standard paper (15 in. by 11 in.). */
Standard15x11: 46,
/** Invitation envelope (220 mm by 220 mm). */
InviteEnvelope: 47,
/** Letter extra paper (9.275 in. by 12 in.). This value is specific to the PostScript
  *  driver and is used only by Linotronic printers in order to conserve paper.
  */
LetterExtra: 48,
/** Legal extra paper (9.275 in. by 15 in.). This value is specific to the PostScript
  *  driver and is used only by Linotronic printers in order to conserve paper.
  */
LegalExtra: 49,
/** Tabloid extra paper (11.69 in. by 18 in.). This value is specific to the
  *   PostScript driver and is used only by Linotronic printers in order to conserve
  *   paper.
  */
TabloidExtra: 50,
/** A4 extra paper (236 mm by 322 mm). This value is specific to the PostScript
  *    driver and is used only by Linotronic printers to help save paper.
  */
A4Extra: 51,
/** Letter transverse paper (8.275 in. by 11 in.). */
LetterTransverse: 52,
/** A4 transverse paper (210 mm by 297 mm). */
A4Transverse: 53,
/** Letter extra transverse paper (9.275 in. by 12 in.). */
LetterExtraTransverse: 54,
/** SuperA/SuperA/A4 paper (227 mm by 356 mm). */
APlus: 55,
/** SuperB/SuperB/A3 paper (305 mm by 487 mm). */
BPlus: 56,
/** Letter plus paper (8.5 in. by 12.69 in.). */
LetterPlus: 57,
/** A4 plus paper (210 mm by 330 mm). */
A4Plus: 58,
/** A5 transverse paper (148 mm by 210 mm). */
A5Transverse: 59,
/** JIS B5 transverse paper (182 mm by 257 mm). */
B5Transverse: 60,
/** A3 extra paper (322 mm by 445 mm). */
A3Extra: 61,
/** A5 extra paper (174 mm by 235 mm). */
A5Extra: 62,
/** ISO B5 extra paper (201 mm by 276 mm). */
B5Extra: 63,
/** A2 paper (420 mm by 594 mm). */
A2: 64,
/** A3 transverse paper (297 mm by 420 mm). */
A3Transverse: 65,
/** A3 extra transverse paper (322 mm by 445 mm). */
A3ExtraTransverse: 66,
/** Japanese double postcard (200 mm by 148 mm). Requires Windows 98, Windows
  *    NT 4.0, or later.
  */
JapaneseDoublePostcard: 67,
/** A6 paper (105 mm by 148 mm). Requires Windows 98, Windows NT 4.0, or later. */
A6: 68,
/** Japanese Kaku #2 envelope. Requires Windows 98, Windows NT 4.0, or later. */
JapaneseEnvelopeKakuNumber2: 69,
/** Japanese Kaku #3 envelope. Requires Windows 98, Windows NT 4.0, or later. */
JapaneseEnvelopeKakuNumber3: 70,
/** Japanese Chou #3 envelope. Requires Windows 98, Windows NT 4.0, or later. */
JapaneseEnvelopeChouNumber3: 71,
/** Japanese Chou #4 envelope. Requires Windows 98, Windows NT 4.0, or later. */
JapaneseEnvelopeChouNumber4: 72,
/** Letter rotated paper (11 in. by 8.5 in.). */
LetterRotated: 73,
/** A3 rotated paper (420 mm by 297 mm). */
A3Rotated: 74,
/** A4 rotated paper (297 mm by 210 mm). Requires Windows 98, Windows NT 4.0,
  *   or later.
  */
A4Rotated: 75,
/** A5 rotated paper (210 mm by 148 mm). Requires Windows 98, Windows NT 4.0,
  *   or later.
  */
A5Rotated: 76,
/** JIS B4 rotated paper (364 mm by 257 mm). Requires Windows 98, Windows NT
  *   4.0, or later.
  */
B4JisRotated: 77,
/** JIS B5 rotated paper (257 mm by 182 mm). Requires Windows 98, Windows NT
  *   4.0, or later.
  */
B5JisRotated: 78,
/** Japanese rotated postcard (148 mm by 100 mm). Requires Windows 98, Windows
  *   NT 4.0, or later.
  */
JapanesePostcardRotated: 79,
/** Japanese rotated double postcard (148 mm by 200 mm). Requires Windows 98,
  *  Windows NT 4.0, or later.
  */
JapaneseDoublePostcardRotated: 80,
/** A6 rotated paper (148 mm by 105 mm). Requires Windows 98, Windows NT 4.0,
  *   or later.
  */
A6Rotated: 81,
/** Japanese rotated Kaku #2 envelope. Requires Windows 98, Windows NT 4.0, or
  *   later.
  */
JapaneseEnvelopeKakuNumber2Rotated: 82,
/** Japanese rotated Kaku #3 envelope. Requires Windows 98, Windows NT 4.0, or
  *   later.
  */
JapaneseEnvelopeKakuNumber3Rotated: 83,
/** Japanese rotated Chou #3 envelope. Requires Windows 98, Windows NT 4.0, or
  *   later.
  */
JapaneseEnvelopeChouNumber3Rotated: 84,
/** Japanese rotated Chou #4 envelope. Requires Windows 98, Windows NT 4.0, or
  *   later.
  */
JapaneseEnvelopeChouNumber4Rotated: 85,
/** JIS B6 paper (128 mm by 182 mm). Requires Windows 98, Windows NT 4.0, or
  *   later.
  */
B6Jis: 86,
/** JIS B6 rotated paper (182 mm by 128 mm). Requires Windows 98, Windows NT
  *   4.0, or later.
  */
B6JisRotated: 87,
/** Standard paper (12 in. by 11 in.). Requires Windows 98, Windows NT 4.0, or
  *   later.
  */
Standard12x11: 88,
/** Japanese You #4 envelope. Requires Windows 98, Windows NT 4.0, or later. */
JapaneseEnvelopeYouNumber4: 89,
/** Japanese You #4 rotated envelope. Requires Windows 98, Windows NT 4.0, or
  *   later.
  */
JapaneseEnvelopeYouNumber4Rotated: 90,
/** People's Republic of China 16K paper (146 mm by 215 mm). Requires Windows
  * 			98, Windows NT 4.0, or later.
  */
Prc16K: 91,
/** People's Republic of China 32K paper (97 mm by 151 mm). Requires Windows
  * 			98, Windows NT 4.0, or later.
  */
Prc32K: 92,
/** People's Republic of China 32K big paper (97 mm by 151 mm). Requires Windows
  *   98, Windows NT 4.0, or later.
  */
Prc32KBig: 93,
/** People's Republic of China #1 envelope (102 mm by 165 mm). Requires Windows
  *   98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber1: 94,
/** People's Republic of China #2 envelope (102 mm by 176 mm). Requires Windows
  * 			98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber2: 95,
/** People's Republic of China #3 envelope (125 mm by 176 mm). Requires Windows
  *   98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber3: 96,
/** People's Republic of China #4 envelope (110 mm by 208 mm). Requires Windows
  *   98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber4: 97,
/** People's Republic of China #5 envelope (110 mm by 220 mm). Requires Windows
  * 			98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber5: 98,
/** People's Republic of China #6 envelope (120 mm by 230 mm). Requires Windows
  *   98, Windows NT 4.0, or later.
  * 		PrcEnvelopeNumber6 = 101,
  * 		/** People's Republic of China #7 envelope (160 mm by 230 mm). Requires Windows
  *   98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber7: 99,
/** People's Republic of China #8 envelope (120 mm by 309 mm). Requires Windows
  *   98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber8: 100,
/** People's Republic of China #9 envelope (229 mm by 324 mm). Requires Windows
  *   98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber9: 101,
/** People's Republic of China #10 envelope (324 mm by 458 mm). Requires Windows
  *   98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber10: 102,
/** People's Republic of China 16K rotated paper (146 mm by 215 mm). Requires
  *   Windows 98, Windows NT 4.0, or later.
  */
Prc16KRotated: 103,
/** People's Republic of China 32K rotated paper (97 mm by 151 mm). Requires
  *   Windows 98, Windows NT 4.0, or later.
  */
Prc32KRotated: 104,
/** People's Republic of China 32K big rotated paper (97 mm by 151 mm). Requires
  *   Windows 98, Windows NT 4.0, or later.
  */
Prc32KBigRotated: 105,
/** People's Republic of China #1 rotated envelope (165 mm by 102 mm). Requires
  *    Windows 98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber1Rotated: 106,
/** People's Republic of China #2 rotated envelope (176 mm by 102 mm). Requires
  *  Windows 98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber2Rotated: 107,
/** People's Republic of China #3 rotated envelope (176 mm by 125 mm). Requires
  *   Windows 98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber3Rotated: 108,
/** People's Republic of China #4 rotated envelope (208 mm by 110 mm). Requires
  *   Windows 98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber4Rotated: 109,
/** People's Republic of China Envelope #5 rotated envelope (220 mm by 110 mm).
  *   Requires Windows 98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber5Rotated: 110,
/** People's Republic of China #6 rotated envelope (230 mm by 120 mm). Requires
  *   Windows 98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber6Rotated: 111,
/** People's Republic of China #7 rotated envelope (230 mm by 160 mm). Requires
  * 			Windows 98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber7Rotated: 112,
/** People's Republic of China #8 rotated envelope (309 mm by 120 mm). Requires
  *   Windows 98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber8Rotated: 113,
/** People's Republic of China #9 rotated envelope (324 mm by 229 mm). Requires
  *   Windows 98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber9Rotated: 114,
/** People's Republic of China #10 rotated envelope (458 mm by 324 mm). Requires
  *   Windows 98, Windows NT 4.0, or later.
  */
PrcEnvelopeNumber10Rotated: 115 
};
})()
})()
