var wijmo = wijmo || {};

(function () {
wijmo.wizard = wijmo.wizard || {};

(function () {
/** @class wijwizard
  * @widget 
  * @namespace jQuery.wijmo.wizard
  * @extends wijmo.wijmoWidget
  */
wijmo.wizard.wijwizard = function () {};
wijmo.wizard.wijwizard.prototype = new wijmo.wijmoWidget();
/** Removes the wijwizard functionality completely.
  * This returns the element back to its pre-init state.
  */
wijmo.wizard.wijwizard.prototype.destroy = function () {};
/** The add method adds a new panel.
  * @param {number} index Zero-based position where to insert the new panel.
  * @param {string} title The step title.
  * @param {string} desc The step description.
  * @returns {wijmo.wizard.wijwizard}
  * @example
  * // Add a new panel to be the second step.
  * // It's title is "New Panel", description is "New Panel Description".
  * $("#wizard").wijwizard("add", 1, "New Panel", "New Panel Description");
  */
wijmo.wizard.wijwizard.prototype.add = function (index, title, desc) {};
/** The remove method removes a panel.
  * @param {number} index The zero-based index of the panel to be removed.
  * @returns {wijmo.wizard.wijwizard}
  * @example
  * // Remove the second step.
  * $("#wizard").wijwizard("remove", 1);
  */
wijmo.wizard.wijwizard.prototype.remove = function (index) {};
/** The show method selects an active panel and displays the panel at a specified position.
  * @param {number} index The zero-based index of the panel to be actived.
  * @returns {wijmo.wizard.wijwizard}
  * @example
  * // Show the second step.
  * $("#wizard").wijwizard("show", 1);
  */
wijmo.wizard.wijwizard.prototype.show = function (index) {};
/** The next method moves to the next panel.
  * @returns {bool}
  */
wijmo.wizard.wijwizard.prototype.next = function () {};
/** The back method moves to the previous panel.
  * @returns {bool}
  */
wijmo.wizard.wijwizard.prototype.back = function () {};
/** The load method reload the content of an Ajax panel programmatically.
  * @param {number} index The zero-based index of the panel to be loaded.
  * @returns {wijmo.wizard.wijwizard}
  * @example
  * // Reload the content of second step.
  * $("#wizard").wijwizard("load", 1);
  */
wijmo.wizard.wijwizard.prototype.load = function (index) {};
/** The abort method terminates all running panel ajax requests and animations.
  * @returns {wijmo.wizard.wijwizard}
  */
wijmo.wizard.wijwizard.prototype.abort = function () {};
/** The url method changes the url from which an Ajax (remote) panel will be loaded.
  * @param {number} index The zero-based index of the panel of which its URL is to be updated.
  * @param {string} url A URL the content of the panel is loaded from.
  * @returns {wijmo.wizard.wijwizard}
  * @example
  * // Change the url content of second step.
  * $("#wizard").wijwizard("url", 1, "http://wijmo.com/newurl.html");
  */
wijmo.wizard.wijwizard.prototype.url = function (index, url) {};
/** The count method retrieves the number panels.
  * @returns {number} the pabels's length
  */
wijmo.wizard.wijwizard.prototype.count = function () {};
/** The stop method stops displaying the panels in order automatically. */
wijmo.wizard.wijwizard.prototype.stop = function () {};
/** The play method begins displaying the panels in order automatically. */
wijmo.wizard.wijwizard.prototype.play = function () {};

/** @class */
var wijwizard_options = function () {};
/** <p class='defaultValue'>Default value: 'auto'</p>
  * <p>The navButtons option defines the type of navigation buttons used with the wijwizard.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * The possible values are 'auto', 'common', 'edge' and 'none'.
  */
wijwizard_options.prototype.navButtons = 'auto';
/** <p class='defaultValue'>Default value: false</p>
  * <p>The autoPlay option allows the panels to automatically display in order.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijwizard_options.prototype.autoPlay = false;
/** <p class='defaultValue'>Default value: 3000</p>
  * <p>The delay option determines the time span between displaying panels in autoplay mode.</p>
  * @field 
  * @type {number}
  * @option
  */
wijwizard_options.prototype.delay = 3000;
/** <p class='defaultValue'>Default value: false</p>
  * <p>The loop option allows the wijwizard to begin again from the first panel
  * when reaching the last panel in autoPlay mode.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijwizard_options.prototype.loop = false;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.wizard.ShowOption.html'>wijmo.wizard.ShowOption</a></p>
  * <p>The hideOption option defines the animation effects
  * when hiding the panel content.</p>
  * @field 
  * @type {wijmo.wizard.ShowOption}
  * @option 
  * @example
  * //Set hide animation to blind and duration to 500.
  * $(".selector").wijwizard({
  *     hideOption: {fade: false, blind: true, duration: 500}
  * });
  */
wijwizard_options.prototype.hideOption = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.wizard.ShowOption.html'>wijmo.wizard.ShowOption</a></p>
  * <p>The showOption option defines the animation effects
  * when showing the panel content.</p>
  * @field 
  * @type {wijmo.wizard.ShowOption}
  * @option 
  * @example
  * //Set show animation to blind and duration to 500.
  * $(".selector").wijwizard({
  *     showOption: {fade: false, blind: true, duration: 500}
  * });
  */
wijwizard_options.prototype.showOption = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates additional Ajax options to consider when
  * loading panel content (see $.ajax).</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * Please see following link for more details,
  * http://api.jquery.com/jQuery.ajax/ .
  */
wijwizard_options.prototype.ajaxOptions = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>An option that determines whether to cache emote wijwizard content.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * Cached content is being lazy loaded, 
  * for example only and only once for the panel is displayed. 
  * Note that to prevent the actual Ajax requests from being cached by the browser, 
  * you need to provide an extra cache: false flag to ajaxOptions.
  */
wijwizard_options.prototype.cache = false;
/** <p class='defaultValue'>Default value: null</p>
  * <p>The cookie option is a value that stores the latest active index in a cookie. 
  * The cookie is then used to determine the initially active index
  * if the activeIndex option is not defined.</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * This option requires a cookie plugin. 
  * The object needs to have key/value pairs
  * of the form the cookie plugin expects as options.
  * @example
  * $(".selector").wijwizard({cookie:{expires: 7, path: '/', domain:  'jquery.com';, secure: true }})
  */
wijwizard_options.prototype.cookie = null;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>The stepHeaderTemplate option creates an HTML template 
  * for the step header when a new panel is added with the
  * add method or when creating a panel for a remote panel on the fly.</p>
  * @field 
  * @type {string}
  * @option
  */
wijwizard_options.prototype.stepHeaderTemplate = "";
/** <p class='defaultValue'>Default value: ""</p>
  * <p>The panelTemplate option is an HTML template from which a new panel is created. 
  * The new panel is created by adding a panel with the add method or when creating 
  * a panel from a remote panel on the fly.</p>
  * @field 
  * @type {string}
  * @option
  */
wijwizard_options.prototype.panelTemplate = "";
/** <p class='defaultValue'>Default value: ""</p>
  * <p>The HTML content of this string is shown in a panel
  * while remote content is loading. 
  * Pass the option in empty string to deactivate that behavior.</p>
  * @field 
  * @type {string}
  * @option
  */
wijwizard_options.prototype.spinner = "";
/** <p class='defaultValue'>Default value: 'back'</p>
  * <p>The backBtnText option defines the text for the wizard back button.</p>
  * @field 
  * @type {string}
  * @option
  */
wijwizard_options.prototype.backBtnText = 'back';
/** <p class='defaultValue'>Default value: 'next'</p>
  * <p>The nextBtnText option defines the text for the wijwizard next button.</p>
  * @field 
  * @type {string}
  * @option
  */
wijwizard_options.prototype.nextBtnText = 'next';
/** <p>The activeIndex option defines the current selected index of the wijwizard.</p>
  * @field 
  * @type {number}
  * @option
  */
wijwizard_options.prototype.activeIndex = null;
/** The add event handler is a function called when a panel is added.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijWizardEventArgs} args The data with this event.
  */
wijwizard_options.prototype.add = null;
/** The remove event handler is a function called when a panel is removed.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijWizardEventArgs} args The data with this event.
  */
wijwizard_options.prototype.remove = null;
/** The activeIndexChanged event handler is a function called when the activeIndex is changed.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijWizardEventArgs} args The data with this event.
  */
wijwizard_options.prototype.activeIndexChanged = null;
/** The show event handler is a function called when a panel is shown.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijWizardEventArgs} args The data with this event.
  */
wijwizard_options.prototype.show = null;
/** The load event handler is a function called after the content of a remote panel has been loaded.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijWizardEventArgs} args The data with this event.
  */
wijwizard_options.prototype.load = null;
/** The validating event handler is a function called before moving to next panel. 
  * This event is Cancellable.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijWizardValidatingEventArgs} args The data with this event.
  */
wijwizard_options.prototype.validating = null;
wijmo.wizard.wijwizard.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijwizard_options());
$.widget("wijmo.wijwizard", $.wijmo.widget, wijmo.wizard.wijwizard.prototype);
/** Define the blind , fade and duration animation settings
  * @interface ShowOption
  * @namespace wijmo.wizard
  */
wijmo.wizard.ShowOption = function () {};
/** <p>The blind option determines whether blind animation is applied when showing and hiding the panels.</p>
  * @field 
  * @type {boolean}
  */
wijmo.wizard.ShowOption.prototype.blind = null;
/** <p>The fade option determines whether fading animation is applied when showing and hiding the panels.</p>
  * @field 
  * @type {boolean}
  */
wijmo.wizard.ShowOption.prototype.fade = null;
/** <p>The duration option determines the time span between hiding or showing the panels.</p>
  * @field
  */
wijmo.wizard.ShowOption.prototype.duration = null;
/** wijwizard options definition
  * @interface WijWizardOptions
  * @namespace wijmo.wizard
  * @extends wijmo.WidgetOptions
  */
wijmo.wizard.WijWizardOptions = function () {};
/** <p>The navButtons option defines the type of navigation buttons used with the wijwizard.</p>
  * @field 
  * @type {string}
  * @remarks
  * The possible values are 'auto', 'common', 'edge' and 'none'.
  */
wijmo.wizard.WijWizardOptions.prototype.navButtons = null;
/** <p>The autoPlay option allows the panels to automatically display in order.</p>
  * @field 
  * @type {boolean}
  */
wijmo.wizard.WijWizardOptions.prototype.autoPlay = null;
/** <p>The delay option determines the time span between displaying panels in autoplay mode.</p>
  * @field 
  * @type {number}
  */
wijmo.wizard.WijWizardOptions.prototype.delay = null;
/** <p>The loop option allows the wijwizard to begin again from the first panel
  * when reaching the last panel in autoPlay mode.</p>
  * @field 
  * @type {boolean}
  */
wijmo.wizard.WijWizardOptions.prototype.loop = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.wizard.ShowOption.html'>wijmo.wizard.ShowOption</a></p>
  * <p>The hideOption option defines the animation effects
  * when hiding the panel content.</p>
  * @field 
  * @type {wijmo.wizard.ShowOption}
  * @example
  * //Set hide animation to blind and duration to 500.
  * $(".selector").wijwizard({
  *     hideOption: {fade: false, blind: true, duration: 500}
  * });
  */
wijmo.wizard.WijWizardOptions.prototype.hideOption = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.wizard.ShowOption.html'>wijmo.wizard.ShowOption</a></p>
  * <p>The showOption option defines the animation effects
  * when showing the panel content.</p>
  * @field 
  * @type {wijmo.wizard.ShowOption}
  * @example
  * //Set show animation to blind and duration to 500.
  * $(".selector").wijwizard({
  *     showOption: {fade: false, blind: true, duration: 500}
  * });
  */
wijmo.wizard.WijWizardOptions.prototype.showOption = null;
/** <p>A value that indicates additional Ajax options to consider when
  * loading panel content (see $.ajax).</p>
  * @field 
  * @type {object}
  * @remarks
  * Please see following link for more details,
  * http://api.jquery.com/jQuery.ajax/ .
  */
wijmo.wizard.WijWizardOptions.prototype.ajaxOptions = null;
/** <p>An option that determines whether to cache emote wijwizard content.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * Cached content is being lazy loaded, 
  * for example only and only once for the panel is displayed. 
  * Note that to prevent the actual Ajax requests from being cached by the browser, 
  * you need to provide an extra cache: false flag to ajaxOptions.
  */
wijmo.wizard.WijWizardOptions.prototype.cache = null;
/** <p>The cookie option is a value that stores the latest active index in a cookie. 
  * The cookie is then used to determine the initially active index
  * if the activeIndex option is not defined.</p>
  * @field 
  * @type {object}
  * @remarks
  * This option requires a cookie plugin. 
  * The object needs to have key/value pairs
  * of the form the cookie plugin expects as options.
  * @example
  * $(".selector").wijwizard({cookie:{expires: 7, path: '/', domain:  'jquery.com';, secure: true }})
  */
wijmo.wizard.WijWizardOptions.prototype.cookie = null;
/** <p>The stepHeaderTemplate option creates an HTML template 
  * for the step header when a new panel is added with the
  * add method or when creating a panel for a remote panel on the fly.</p>
  * @field 
  * @type {string}
  */
wijmo.wizard.WijWizardOptions.prototype.stepHeaderTemplate = null;
/** <p>The panelTemplate option is an HTML template from which a new panel is created. 
  * The new panel is created by adding a panel with the add method or when creating 
  * a panel from a remote panel on the fly.</p>
  * @field 
  * @type {string}
  */
wijmo.wizard.WijWizardOptions.prototype.panelTemplate = null;
/** <p>The HTML content of this string is shown in a panel
  * while remote content is loading. 
  * Pass the option in empty string to deactivate that behavior.</p>
  * @field 
  * @type {string}
  */
wijmo.wizard.WijWizardOptions.prototype.spinner = null;
/** <p>The backBtnText option defines the text for the wizard back button.</p>
  * @field 
  * @type {string}
  */
wijmo.wizard.WijWizardOptions.prototype.backBtnText = null;
/** <p>The nextBtnText option defines the text for the wijwizard next button.</p>
  * @field 
  * @type {string}
  */
wijmo.wizard.WijWizardOptions.prototype.nextBtnText = null;
/** <p>The activeIndex option defines the current selected index of the wijwizard.</p>
  * @field 
  * @type {number}
  */
wijmo.wizard.WijWizardOptions.prototype.activeIndex = null;
/** <p>The add event handler is a function called when a panel is added.</p>
  * @field 
  * @type {function}
  */
wijmo.wizard.WijWizardOptions.prototype.add = null;
/** <p>The remove event handler is a function called when a panel is removed.</p>
  * @field 
  * @type {function}
  */
wijmo.wizard.WijWizardOptions.prototype.remove = null;
/** <p>The activeIndexChanged event handler is a function called when the activeIndex is changed.</p>
  * @field 
  * @type {function}
  */
wijmo.wizard.WijWizardOptions.prototype.activeIndexChanged = null;
/** <p>The show event handler is a function called when a panel is shown.</p>
  * @field 
  * @type {function}
  */
wijmo.wizard.WijWizardOptions.prototype.show = null;
/** <p>The load event handler is a function called after the content of a remote panel has been loaded.</p>
  * @field 
  * @type {function}
  */
wijmo.wizard.WijWizardOptions.prototype.load = null;
/** <p>The validating event handler is a function called before moving to next panel. 
  * This event is Cancellable.</p>
  * @field 
  * @type {function}
  */
wijmo.wizard.WijWizardOptions.prototype.validating = null;
/** Define the jQuery Ajax settings
  * @interface JQueryAjaxSettings
  * @namespace wijmo.wizard
  */
wijmo.wizard.JQueryAjaxSettings = function () {};
/** <p>Register a handler to be called when Ajax requests complete.</p>
  * @field 
  * @type {function}
  */
wijmo.wizard.JQueryAjaxSettings.prototype.success = null;
/** <p>Register a handler to be called when Ajax requests complete with an error.</p>
  * @field 
  * @type {function}
  */
wijmo.wizard.JQueryAjaxSettings.prototype.error = null;
/** @interface IWijWizardEventArgs
  * @namespace wijmo.wizard
  */
wijmo.wizard.IWijWizardEventArgs = function () {};
/** <p>The panel element.</p>
  * @field 
  * @type {HTMLElement}
  */
wijmo.wizard.IWijWizardEventArgs.prototype.panel = null;
/** <p>The index of the panel.</p>
  * @field 
  * @type {number}
  */
wijmo.wizard.IWijWizardEventArgs.prototype.index = null;
/** @interface IWijWizardValidatingEventArgs
  * @namespace wijmo.wizard
  */
wijmo.wizard.IWijWizardValidatingEventArgs = function () {};
/** <p>The next panel element.</p>
  * @field 
  * @type {HTMLElement}
  */
wijmo.wizard.IWijWizardValidatingEventArgs.prototype.nextPanel = null;
/** <p>The index of the next panel.</p>
  * @field 
  * @type {number}
  */
wijmo.wizard.IWijWizardValidatingEventArgs.prototype.nextIndex = null;
typeof wijmo.WidgetOptions != 'undefined' && $.extend(wijmo.wizard.WijWizardOptions.prototype, wijmo.WidgetOptions.prototype);
})()
})()
