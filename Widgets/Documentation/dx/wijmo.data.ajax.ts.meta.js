var wijmo = wijmo || {};

(function () {
wijmo.data = wijmo.data || {};

(function () {
/** The ajax result which contains the data and the total items count.
  * @interface IAjaxResult
  * @namespace wijmo.data
  */
wijmo.data.IAjaxResult = function () {};
/** <p>Gets the data from the ajax result.</p>
  * @field 
  * @type {array}
  */
wijmo.data.IAjaxResult.prototype.results = null;
/** <p>Gets the total items count from the ajax result.</p>
  * @field 
  * @type {number}
  */
wijmo.data.IAjaxResult.prototype.totalItemCount = null;
/** @interface IAjaxDataViewOptions
  * @namespace wijmo.data
  * @extends wijmo.data.IRemoteDataViewOptions
  */
wijmo.data.IAjaxDataViewOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQueryAjaxSettings}
  */
wijmo.data.IAjaxDataViewOptions.prototype.ajax = null;
/** <p>undefined</p>
  * @field 
  * @type {function}
  */
wijmo.data.IAjaxDataViewOptions.prototype.onRequest = null;
/** <p>undefined</p>
  * @field 
  * @type {function}
  */
wijmo.data.IAjaxDataViewOptions.prototype.onResponse = null;
/** @class AjaxDataView
  * @namespace wijmo.data
  * @extends wijmo.data.RemoteDataView
  */
wijmo.data.AjaxDataView = function () {};
wijmo.data.AjaxDataView.prototype = new wijmo.data.RemoteDataView();
/**  */
wijmo.data.AjaxDataView.prototype.cancelRefresh = function () {};
/** @class ODataView
  * @namespace wijmo.data
  * @extends wijmo.data.AjaxDataView
  */
wijmo.data.ODataView = function () {};
wijmo.data.ODataView.prototype = new wijmo.data.AjaxDataView();
typeof wijmo.data.IRemoteDataViewOptions != 'undefined' && $.extend(wijmo.data.IAjaxDataViewOptions.prototype, wijmo.data.IRemoteDataViewOptions.prototype);
})()
})()
