var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
/** @class widgetEmulator
  * @namespace wijmo.grid
  */
wijmo.grid.widgetEmulator = function () {};
/** @param {string} key
  * @param value
  */
wijmo.grid.widgetEmulator.prototype.option = function (key, value) {};
/**  */
wijmo.grid.widgetEmulator.prototype.enable = function () {};
/**  */
wijmo.grid.widgetEmulator.prototype.disable = function () {};
/**  */
wijmo.grid.widgetEmulator.prototype.destroy = function () {};
/** @returns {JQuery} */
wijmo.grid.widgetEmulator.prototype.widget = function () {};
})()
})()
var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
/** @interface IC1FieldFactory
  * @namespace wijmo.grid
  */
wijmo.grid.IC1FieldFactory = function () {};
/** @param {wijmo.grid.IC1FieldFactory} factory */
wijmo.grid.registerColumnFactory = function (factory) {};
/** @param {wijmo.grid.wijgrid} grid
  * @param {wijmo.grid.IColumn} column
  * @returns {wijmo.grid.c1basefield}
  */
wijmo.grid.asColumnInstance = function (grid, column) {};
})()
})()
var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
/** @class c1basefield
  * @namespace wijmo.grid
  * @extends wijmo.grid.widgetEmulator
  */
wijmo.grid.c1basefield = function () {};
wijmo.grid.c1basefield.prototype = new wijmo.grid.widgetEmulator();
/** @param {wijmo.grid.IColumn} column
  * @returns {bool}
  */
wijmo.grid.c1basefield.prototype.test = function (column) {};
/** @class c1basefield_options
  * @namespace wijmo.grid
  */
wijmo.grid.c1basefield_options = function () {};
})()
})()
var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
})()
})()
var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
})()
})()
