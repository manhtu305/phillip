var wijmo = wijmo || {};

(function () {
wijmo.slider = wijmo.slider || {};

(function () {
/** @class JQueryUISlider
  * @widget 
  * @namespace jQuery.wijmo.slider
  * @extends wijmo.JQueryUIWidget
  */
wijmo.slider.JQueryUISlider = function () {};
wijmo.slider.JQueryUISlider.prototype = new wijmo.JQueryUIWidget();
/** This option can be used to specify multiple handles.
  * @param {?number} index the first value.
  * @param {?number} val the second value.
  * @remarks
  * If the range option is set to true, the length of values should be 2.
  */
wijmo.slider.JQueryUISlider.prototype.values = function (index, val) {};
/** Determines the value of the slider, if there's only one handle.
  * @param {?number} val the specified value.
  * @remarks
  * If there is more than one handle, determines the value of the first handle.
  */
wijmo.slider.JQueryUISlider.prototype.value = function (val) {};

/** @class */
var JQueryUISlider_options = function () {};
wijmo.slider.JQueryUISlider.prototype.options = $.extend({}, true, wijmo.JQueryUIWidget.prototype.options, new JQueryUISlider_options());
$.widget("wijmo.JQueryUISlider", $.wijmo.widget, wijmo.slider.JQueryUISlider.prototype);
/** @class wijslider
  * @widget 
  * @namespace jQuery.wijmo.slider
  * @extends wijmo.slider.JQueryUISlider
  */
wijmo.slider.wijslider = function () {};
wijmo.slider.wijslider.prototype = new wijmo.slider.JQueryUISlider();
/** This option can be used to specify multiple handles.
  * @param {?number} index the first value.
  * @param {?number} val the second value.
  * @remarks
  * If the range option is set to true, the length of values should be 2.
  */
wijmo.slider.wijslider.prototype.values = function (index, val) {};
/** Determines the value of the slider, if there's only one handle.
  * @param {?number} val the specified value.
  * @remarks
  * If there is more than one handle, determines the value of the first handle.
  */
wijmo.slider.wijslider.prototype.value = function (val) {};
/** Refresh the wijslider widget. */
wijmo.slider.wijslider.prototype.refresh = function () {};
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.slider.wijslider.prototype.destroy = function () {};

/** @class */
var wijslider_options = function () {};
/** <p class='defaultValue'>Default value: true</p>
  * <p>The dragFill option, when set to true, allows the user to drag 
  *  the fill between the thumb buttons on the slider widget.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijslider_options.prototype.dragFill = true;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>The minRange option prevents the two range handles (thumb buttons) 
  *  from being placed on top of one another.</p>
  * @field 
  * @type {number}
  * @option
  */
wijslider_options.prototype.minRange = 0;
/** <p class='defaultValue'>Default value: false</p>
  * <p>The animate option defines the sliding animation that is 
  *  applied to the slider handle when a user clicks outside the handle on the bar.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * This option will accept a string representing one of the three predefined 
  * speeds or a number representing the length of time in 
  * milliseconds that the animation will run. The three predefined speeds are slow, normal, or fast.
  */
wijslider_options.prototype.animate = false;
/** <p class='defaultValue'>Default value: 100</p>
  * <p>The max option defines the maximum value of the slider widget.</p>
  * @field 
  * @type {number}
  * @option
  */
wijslider_options.prototype.max = 100;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>The min option defines the minimum value of the slider widget.</p>
  * @field 
  * @type {number}
  * @option
  */
wijslider_options.prototype.min = 0;
/** <p class='defaultValue'>Default value: 'horizontal'</p>
  * <p>The orientation option determines whether the wijslider is positioned horizontally or vertically.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * If the slider is positioned horizontally, then the min value will be at the left and the max 
  * value will be at the right. If the slider is positioned vertically, then the min value is at the bottom and 
  * the max value is at the top of the widget. By default, the widget's orientation is horizontal.
  */
wijslider_options.prototype.orientation = 'horizontal';
/** <p class='defaultValue'>Default value: false</p>
  * <p>The range option, if set to true, allows the slider to detect if you have two handles.</p>
  * @field 
  * @type {boolean|string}
  * @option 
  * @remarks
  * It will then create a stylable range element between the two handles. 
  * You can also create a stylable range with one handle by using the 'min' and 'max' values. 
  * A min range goes from the slider min value to the range handle. A max range goes from the range 
  * handle to the slider max value.
  */
wijslider_options.prototype.range = false;
/** <p class='defaultValue'>Default value: 1</p>
  * <p>The step option determines the size of each interval between the slider minimum value and the slider maximum value.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * The full specified value range of the slider (from the minimum value to the maximum value) 
  * must be evenly divisible by the step interval.
  */
wijslider_options.prototype.step = 1;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>The value option determines the total value of the slider widget when there is only one range handle.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * If there are two range handles, then the value option determines the value of the first handle.
  */
wijslider_options.prototype.value = 0;
/** <p class='defaultValue'>Default value: null</p>
  * <p>The values option can be used to specify multiple handles.</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * If the range option is set to true, then the 'values' option must be set in order to have two handles.
  */
wijslider_options.prototype.values = null;
/** The buttonMouseOver event is raised when the mouse is over 
  *  the decrement button or the increment button.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IButtonEventArgs} args The data with this event.
  */
wijslider_options.prototype.buttonMouseOver = null;
/** The buttonMouseOut event is raised when the mouse leaves 
  *  the decrement button or the increment button.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IButtonEventArgs} args The data with this event.
  */
wijslider_options.prototype.buttonMouseOut = null;
/** The buttonMouseDown event is raised when the mouse is down
  *  on the decrement button or the increment button.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IButtonEventArgs} args The data with this event.
  */
wijslider_options.prototype.buttonMouseDown = null;
/** The buttonMouseUp event is raised when the mouse is up
  *  on the decrement button or the increment button.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IButtonEventArgs} args The data with this event.
  */
wijslider_options.prototype.buttonMouseUp = null;
/** The buttonClick event is raised when the decrement button
  *  or the increment button is clicked.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IButtonEventArgs} args The data with this event.
  */
wijslider_options.prototype.buttonClick = null;
/** The change event is triggered when the user stops moving the range handle or 
  * when a value is changed programatically.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @remarks
  * This event takes the event and ui arguments. please refer: http://api.jqueryui.com/slider/
  * Use event.originalEvent to detect if the value was changed programatically or through mouse or keyboard interaction.
  */
wijslider_options.prototype.change = null;
/** Triggered on every mouse move during slide.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @remarks
  * please refer: http://api.jqueryui.com/slider/
  */
wijslider_options.prototype.slide = null;
/** The start event is triggered when the user begins to move the slider thumb.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @remarks
  * please refer: http://api.jqueryui.com/slider/
  */
wijslider_options.prototype.start = null;
/** The stop event is triggered when the user stops sliding the slider thumb.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @remarks
  * please refer: http://api.jqueryui.com/slider/
  */
wijslider_options.prototype.stop = null;
wijmo.slider.wijslider.prototype.options = $.extend({}, true, wijmo.slider.JQueryUISlider.prototype.options, new wijslider_options());
$.widget("wijmo.wijslider", $.wijmo.JQueryUISlider, wijmo.slider.wijslider.prototype);
/** Provides data for the button event of the wijslider.
  * @interface IButtonEventArgs
  * @namespace wijmo.slider
  */
wijmo.slider.IButtonEventArgs = function () {};
/** <p>A string value that indicates the type name of button.</p>
  * @field 
  * @type {string}
  */
wijmo.slider.IButtonEventArgs.prototype.buttonType = null;
})()
})()
