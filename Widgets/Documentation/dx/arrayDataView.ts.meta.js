var wijmo = wijmo || {};

(function () {
wijmo.data = wijmo.data || {};

(function () {
/** @class ArrayDataViewBase
  * @namespace wijmo.data
  */
wijmo.data.ArrayDataViewBase = function () {};
/** @ ignore */
wijmo.data.ArrayDataViewBase.prototype.dispose = function () {};
/** Returns the number of items in the current view after client-side filtering/sorting/paging have been applied.
  * @returns {number}
  */
wijmo.data.ArrayDataViewBase.prototype.count = function () {};
/** Returns an element in the view by index.
  * @ param {Number} index The zero-based index of the element to get
  * @ remarks Throws an exception if the index is out of range
  * @param {number} index
  */
wijmo.data.ArrayDataViewBase.prototype.item = function (index) {};
/** Returns the index of the element in current view
  * @ param {object} item The element in current view.
  * @param item
  * @returns {number}
  */
wijmo.data.ArrayDataViewBase.prototype.indexOf = function (item) {};
/** Returns the element array before applying client-side filtering/sorting/paging.
  * @returns {Array}
  * @remarks
  * In the case of a remote data source, usually it is the array returned by a server.
  */
wijmo.data.ArrayDataViewBase.prototype.getSource = function () {};
/** Converts the element array of current view after filter, sort and paging have been applied to Obervable Array. */
wijmo.data.ArrayDataViewBase.prototype.toObservableArray = function () {};
/** Returns properties for elements in the view.
  * @returns {wijmo.data.IPropertyDescriptor[]}
  */
wijmo.data.ArrayDataViewBase.prototype.getProperties = function () {};
/** Returns the current value of the property in the element at the specified index.
  * @ param {Object} itemOrIndex The element with the property value that is to be read 
  * or the zero-based index of the element with the property value that is to be read.
  * @ param {string} property The name of the property to read
  * @ returns The current value of the property in the element
  * @param itemOrIndex
  * @param {string} property
  */
wijmo.data.ArrayDataViewBase.prototype.getProperty = function (itemOrIndex, property) {};
/** Sets the value of the property in the element.
  * @ param {Object} itemOrIndex The element with the property value that is to be read 
  * or the zero-based index of the element with the property value that is to be read.
  * @ param {string} property The name of the property to set
  * @ param newValue The new value
  * @ remarks 
  * If the class implementing IDataView also implements IEditableDataView and the element is being edited, 
  * then the property value change is done within the editing transaction.
  * @param itemOrIndex
  * @param {string} property
  * @param newValue
  * @returns {ArrayDataViewBase}
  */
wijmo.data.ArrayDataViewBase.prototype.setProperty = function (itemOrIndex, property, newValue) {};
/** Registers Subcription of changes for current view.
  * @ param {function} handler Function that will be called when current view is changed.
  * @ param {Object?} context Indicates which calls the handler function.
  * @param {function} handler
  * @param context
  */
wijmo.data.ArrayDataViewBase.prototype.subscribe = function (handler, context) {};
/** Triggers the changes for current view. */
wijmo.data.ArrayDataViewBase.prototype.trigger = function () {};
/** Returns a value that indicates whether the data view supports filtering.
  * @returns {bool}
  */
wijmo.data.ArrayDataViewBase.prototype.canFilter = function () {};
/** Returns a value that indicates whether the data view supports sorting.
  * @returns {bool}
  */
wijmo.data.ArrayDataViewBase.prototype.canSort = function () {};
/** @returns {bool} */
wijmo.data.ArrayDataViewBase.prototype.prevPage = function () {};
/** @returns {bool} */
wijmo.data.ArrayDataViewBase.prototype.nextPage = function () {};
/** Reloads the data view.
  * @ param {IShape} shape
  * Settings for filtering, sorting and/or paging to be applied before loading.
  * In contrast to filter/sort properties in IDataView, 
  * the format of properties in the shape parameter does not have to follow the specs defined in IDataView.
  * For instance, BreezeDataView accepts a Breeze.Predicate as shape.filter.
  * However the IDataView properties must not violate the IDataView specs. If a custom filter format cannot be aligned to the IDataView specs,
  * then the properties values must be functions or null.
  * @ param {boolean} local Prevents server requests and applies filtering/sorting/paging of the source array on the client-side.
  * @ returns {IRefreshResult} A promise object that can be used to receive notification of asynchronous refresh completion or failure.
  * @ remarks
  * Depending on the implementation, data can be loaded from a local or a remote data source. 
  * The implementation transaltes filtering/sorting/paging defined in the IDataView to the data provider specific format.
  * If it is possible, then the data is reshaped on the client-side.
  * The optional shape parameter can be used to change multiple filtering/sorting/paging settings at a time to avoid unnecessary refreshes.
  * During the refresh, the isLoading property value must be true. After a successful refresh, the isLoaded property value must be true.
  * When the method is called, the pending refresh, if any, is canceled.
  * @param {wijmo.data.IShape} shape
  * @param local
  * @returns {JQueryPromise}
  */
wijmo.data.ArrayDataViewBase.prototype.refresh = function (shape, local) {};
/** Cancels the ongoing refresh operation */
wijmo.data.ArrayDataViewBase.prototype.cancelRefresh = function () {};
/** Returns a value indicating whether the element being edited is added by the add(item) methods.
  * @returns {bool}
  */
wijmo.data.ArrayDataViewBase.prototype.isCurrentEditItemNew = function () {};
/** Returns a value that indicates whether the add(item) method is implemented.
  * @returns {bool}
  */
wijmo.data.ArrayDataViewBase.prototype.canAdd = function () {};
/** Adds a new element and marks it as being edited.
  * @ param {Object} item The item to add.
  * @ remarks
  * Adds a new element to the data view, but not to the underlying data source.
  * Commits the element currently being edited, if any.
  * @param {Object} item
  */
wijmo.data.ArrayDataViewBase.prototype.add = function (item) {};
/** Returns a value that indicates whether the addNew() method is implemented.
  * @returns {bool}
  */
wijmo.data.ArrayDataViewBase.prototype.canAddNew = function () {};
/**  */
wijmo.data.ArrayDataViewBase.prototype.addNew = function () {};
/** Starts editing of an element at the index.
  * @ param {Object} element The element to edit. 
  * Defaults to the value of IDataView.currentItem property.
  * @ remarks 
  * Commits the element currently being edited, if any.
  * @param item
  */
wijmo.data.ArrayDataViewBase.prototype.editItem = function (item) {};
/** Returns a value that indicates whether the remove method overloads are implemented.
  * @returns {bool}
  */
wijmo.data.ArrayDataViewBase.prototype.canRemove = function () {};
/** Removes an element at the specified index.
  * @ param {Object} item The element to remove. 
  * Defaults to the value of the IDataView.currentItem property.
  * @ remarks 
  * If the element success removing, it will return true.
  * Otherwise it will return false.
  * @param item
  * @returns {bool}
  */
wijmo.data.ArrayDataViewBase.prototype.remove = function (item) {};
/** Returns a value that indicates whether the current changes can be canceled.
  * @returns {bool}
  */
wijmo.data.ArrayDataViewBase.prototype.canCancelEdit = function () {};
/** Cancels the changes made to the currently editing element since the editing was started.
  * @ remarks After a successful cancelation, the currentEditItem property value must be null.
  */
wijmo.data.ArrayDataViewBase.prototype.cancelEdit = function () {};
/** Returns a value that indicates whether the current changes can be committed
  * @returns {bool}
  */
wijmo.data.ArrayDataViewBase.prototype.canCommitEdit = function () {};
/** Commits the changes made to the currently editing element
  * @ remarks After a successful commit, the currentEditItem property value must be null.
  * If the item being edited was new, it is added to the underlying data source.
  * If the element does not satisfy the filter, then it is removed from the data view, but not from the data source.
  */
wijmo.data.ArrayDataViewBase.prototype.commitEdit = function () {};
/** @class ArrayDataView
  * @namespace wijmo.data
  * @extends wijmo.data.ArrayDataViewBase
  */
wijmo.data.ArrayDataView = function () {};
wijmo.data.ArrayDataView.prototype = new wijmo.data.ArrayDataViewBase();
})()
})()
