var wijmo = wijmo || {};

(function () {
wijmo.input = wijmo.input || {};

(function () {
/** @interface INumberOptions
  * @namespace wijmo.input
  */
wijmo.input.INumberOptions = function () {};
/** <p>Determine the current symbol when number type is currency. The default value is according to culture.</p>
  * @field 
  * @type {string}
  */
wijmo.input.INumberOptions.prototype.currencySymbol = null;
/** <p>Indicates the culture that the format library will use.</p>
  * @field 
  * @type {string}
  */
wijmo.input.INumberOptions.prototype.culture = null;
/** <p>Determine the prefix string used for negative value.
  * The default value is according to culture &amp; type.</p>
  * @field 
  * @type {string}
  */
wijmo.input.INumberOptions.prototype.negativePrefix = null;
/** <p>Determine the suffix string used for negative value. 
  * The default value is according to culture &amp; type.</p>
  * @field 
  * @type {string}
  */
wijmo.input.INumberOptions.prototype.negativeSuffix = null;
/** <p>Determine the prefix string used for positive value. 
  * The default value is according to culture &amp; type.</p>
  * @field 
  * @type {string}
  */
wijmo.input.INumberOptions.prototype.positivePrefix = null;
/** <p>Determine the suffix striing used for positive value. 
  * The default value is according to culture &amp; type.</p>
  * @field 
  * @type {string}
  */
wijmo.input.INumberOptions.prototype.positiveSuffix = null;
/** <p>Indicates the number of decimal places to display.</p>
  * @field 
  * @type {number}
  */
wijmo.input.INumberOptions.prototype.decimalPlaces = null;
/** <p>Indicates whether the thousands group separator will be inserted between each digital group 
  * (number of digits in thousands group depends on the selected Culture).</p>
  * @field 
  * @type {boolean}
  */
wijmo.input.INumberOptions.prototype.showGroup = null;
})()
})()
