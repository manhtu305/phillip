var wijmo = wijmo || {};

(function () {
wijmo.editor = wijmo.editor || {};

(function () {
/** @class wijeditor
  * @widget 
  * @namespace jQuery.wijmo.editor
  * @extends wijmo.wijmoWidget
  */
wijmo.editor.wijeditor = function () {};
wijmo.editor.wijeditor.prototype = new wijmo.wijmoWidget();
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.editor.wijeditor.prototype.destroy = function () {};
/** Adjust the editor layout. */
wijmo.editor.wijeditor.prototype.refresh = function () {};
/** Gets the text displayed in the editor. */
wijmo.editor.wijeditor.prototype.getText = function () {};
/** Sets the text to display in the editor.
  * @param {string} text The text to show in the editor.
  * @remarks
  * You can use the example code in a click function linked to a button, 
  * and replace any existing text in the editor with your text.
  */
wijmo.editor.wijeditor.prototype.setText = function (text) {};

/** @class */
var wijeditor_options = function () {};
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Set to the selector that returns the container to fill when the user 
  *   selects full screen mode.By default, it fills the client's area on the web page.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Once set , you can refer to this string in the ID attribute of the container,
  * for example &lt; div id = "container" &gt; &lt; / div &gt;.
  */
wijeditor_options.prototype.fullScreenContainerSelector = "";
/** <p class='defaultValue'>Default value: false</p>
  * <p>If the compactMode option is true and ribbon is not simple,it will show
  *   compact ribbon although the editor is full width.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijeditor_options.prototype.compactRibbonMode = false;
/** <p class='defaultValue'>Default value: 'wysiwyg'</p>
  * <p>Set the type of editor to show initially.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * It has three options: wysiwyg/code/split.
  * wysiwyg -- A visual designer with toolbar buttons that allow you to format text.
  * code -- A code view of the text that allows you to edit the HTML formatting.
  * split -- A horizontally split editor with the visual designer in the top pane 
  * and the code view in the bottom.In this mode, the user can resize the panes by 
  * dragging the horizontal bar separating them up or down.
  */
wijeditor_options.prototype.editorMode = 'wysiwyg';
/** <p class='defaultValue'>Default value: false</p>
  * <p>Set this option to true to show the editor in full screen mode when the page is first opened.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * The editor fills the client area of the page(or the container specified in the fullScreenContainerSelector option).
  * Note that this only sets the initial mode.The user can still toggle fullScreenMode off
  * using the button at the bottom right of the editor.
  */
wijeditor_options.prototype.fullScreenMode = false;
/** <p class='defaultValue'>Default value: []</p>
  * <p>The fontSizes option specifies the list of font size 
  * which will be shown in the font size drop down list. 
  * Use the option to customize font sizes.</p>
  * @field 
  * @type {array}
  * @option 
  * @remarks
  * Each item includes:
  * tip: the dropdown item’s tip.
  * name: the really fontsize's value that apply.
  * text: the font size text that show on dropdown item.
  * the default value is:
  * [{
  * tip: "VerySmall",
  * name: "xx-small",
  * text: "VerySmall"
  * }, {
  * tip: "Smaller",
  * name: "x-small",
  * text: "Smaller"
  * }, {
  * tip: "Small",
  * name: "small",
  * text: "Small"
  * }, {
  * tip: "Medium",
  * name: "medium",
  * text: "Medium"
  * }, {
  * tip: "Large",
  * name: "large",
  * text: "Large"
  * }, {
  * tip: "Larger",
  * name: "x-large",
  * text: "Larger"
  * }, {
  * tip: "VeryLarge",
  * name: "xx-large",
  * text: "VeryLarge"
  * }]
  * Note: The following Font size’ value is built-in font size, and 
  * please refer https://dvcs.w3.org/hg/editing/raw-file/tip/editing.html#the-fontsize-command
  * 1: x-small
  * 2: small
  * 3: medium
  * 4: large
  * 5: x-large
  * 6: xx-large
  * 7: xxx-large
  * Except these values, user can set the "pt" and "px" value to "name".
  */
wijeditor_options.prototype.fontSizes = [];
/** <p class='defaultValue'>Default value: []</p>
  * <p>The fontNames option specifies the list of font name 
  * which will be shown in the font name drop down list.
  * Use the option to customize font names.</p>
  * @field 
  * @type {array}
  * @option 
  * @remarks
  * Each item includes:
  * tip: the dropdown item’s tip.
  * name: the really fontname's value that apply.
  * text: the font name text that show on dropdown item.
  * the default value is:
  * [{
  * tip: "Arial",
  * name: "fn1",
  * text: "Arial"
  * }, {
  * tip: "Courier New",
  * name: "fn2",
  * text: "Courier New"
  * }, {
  * tip: "Garamond",
  * name: "fn3",
  * text: "Garamond"
  * }, {
  * tip: "Tahoma",
  * name: "fn4",
  * text: "Tahoma"
  * }, {
  * tip: "Times New Roman",
  * name: "fn5",
  * text: "Times New Roman"
  * }, {
  * tip: "Verdana",
  * name: "fn6",
  * text: "Verdana"
  * }, {
  * tip: "Wingdings",
  * name: "fn7",
  * text: "Wingdings"
  * }]
  * Note: The "fn1, fn2,..., fn7" are wijmo built-in font size, indicate the safe web font family, 
  * user can set others font family like css.
  */
wijeditor_options.prototype.fontNames = [];
/** <p class='defaultValue'>Default value: true</p>
  * <p>Select whether to show the path selector in the left side of the footer.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * When you click in the content of the design view, the path selector shows HTML tags 
  * indicating where you are in the source view.If you set showFooter to false, this option is ignored.
  */
wijeditor_options.prototype.showPathSelector = true;
/** <p class='defaultValue'>Default value: 'ribbon'</p>
  * <p>Specify which toolbar to render. Ribbon and simple modes allow you to customize the toolbar.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * For more information on customizing the toolbar, 
  * see editor samples "Customize" and "Customize Simple."
  * ribbon -- Includes the most commonly used tool buttons.
  * simple -- Includes fewer buttons, is much smaller, and takes up less space.
  * bbcode -- Includes buttons used with Bulletin Board Code, 
  * a markup language used to format posts in message boards.
  */
wijeditor_options.prototype.mode = 'ribbon';
/** <p class='defaultValue'>Default value: null</p>
  * <p>Specify an array of commands to display in customizing the simple toolbar.</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * Note that you must specify every command that you want to display, 
  * as these commands are used in place of the default commands.Therefore, 
  * the sample code shown below would only render two commands in the toolbar. 
  * The simple mode commands displayed by default are:
  * "BlockQuote", "Bold", "BulletedList", "InsertCode", "InsertDate", "InsertImage",
  * "Italic", "Link", "NumberedList", "NumberedList"
  * Note: The buildin simple commands support the following commands:
  * "Form","Image","TextArea","Button","TextBox","List","DropDownList",
  * "HiddenField","Radio","CheckBox","JustifyLeft,"JustifyCenter",
  * "JustifyRight","JustifyFull","Border","NumberedList","BulletedList",
  * "Outdent","Indent","BackColor","ForeColor","Bold","Italic","UnderLine",
  * "SubScript","SuperScript","Template","RemoveFormat","InsertBreak",
  * "InsertParagraph","InsertPrint","InsertHR","Undo","Redo","Preview","Cleanup",
  * "Cut","Copy","Paste","SelectAll","Media","InsertSpecialChar","InsertDate","Find",
  * "Inspect","InsertImage","Link","FontName","FontSize","BlockQuote","InsertCode"
  * The default simple mode commands are:
  * ["Bold", "Italic", "Link", "BlockQuote",
  * "StrikeThrough", "InsertDate", "InsertImage",
  * "NumberedList", "BulletedList", "InsertCode"]
  * @example
  * $("#wijeditor").wijeditor({
  *            mode: "simple",
  *            simpleModeCommands: ["Bold","Undo"]
  *         });
  */
wijeditor_options.prototype.simpleModeCommands = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Specify whether to show the footer at the bottom of the editor, 
  *   where users can toggle full screen mode, word wrap, and view mode.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * When you are in design view, the footer also displays the path selector,
  * HTML tags showing your location in source view.
  */
wijeditor_options.prototype.showFooter = true;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether the custom context menu is shown.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * Instead of the standard context menu items shown by the browser,
  * the custom context menu has only three commands:
  * Cut, Copy, and Paste.Note that the context menu is invalid in Safari.
  */
wijeditor_options.prototype.customContextMenu = true;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Set the text that appears in the editor.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Note that this overrides any text that you add in tags
  * inside the textarea object that displays the widget.
  * You can use HTML(or BBCode markup if you set the mode to bbcode) inside the string.
  */
wijeditor_options.prototype.text = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Use the localization option in order to localize text which not depends on culture.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#editor").wijeditor(
  *                 { 
  *                     localization: {
  *                         formButtonTip: "Add Form",
  *                         insertTab: "Insert to Editor"
  *                     }
  *                 });
  */
wijeditor_options.prototype.localization = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determine if wijeditor is in readonly mode</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijeditor_options.prototype.readOnly = false;
/** Occurs when the command button is clicked.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.editor.ICommandButtonClickEventArgs} data Information about an event
  */
wijeditor_options.prototype.commandButtonClick = null;
/** Occurs when the text changed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.editor.ITextChangedEventArgs} data Information about an event
  */
wijeditor_options.prototype.textChanged = null;
wijmo.editor.wijeditor.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijeditor_options());
$.widget("wijmo.wijeditor", $.wijmo.widget, wijmo.editor.wijeditor.prototype);
/** Contains information about wijeditor.commandButtonClick event
  * @interface ICommandButtonClickEventArgs
  * @namespace wijmo.editor
  */
wijmo.editor.ICommandButtonClickEventArgs = function () {};
/** <p>The command name of the button.</p>
  * @field 
  * @type {string}
  */
wijmo.editor.ICommandButtonClickEventArgs.prototype.commandName = null;
/** <p>The parent name of the button which means 
  *                      if the drop down item is clicked, then the name specifies the command name of the drop down button.</p>
  * @field 
  * @type {string}
  */
wijmo.editor.ICommandButtonClickEventArgs.prototype.name = null;
/** Contains information about wijeditor.textChanged event
  * @interface ITextChangedEventArgs
  * @namespace wijmo.editor
  */
wijmo.editor.ITextChangedEventArgs = function () {};
/** <p>The text of the editor.</p>
  * @field 
  * @type {string}
  */
wijmo.editor.ITextChangedEventArgs.prototype.text = null;
})()
})()
