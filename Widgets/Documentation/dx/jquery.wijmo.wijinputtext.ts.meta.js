var wijmo = wijmo || {};

(function () {
wijmo.input = wijmo.input || {};

(function () {
/** @class wijinputtext
  * @widget 
  * @namespace jQuery.wijmo.input
  * @extends wijmo.input.wijinputcore
  */
wijmo.input.wijinputtext = function () {};
wijmo.input.wijinputtext.prototype = new wijmo.input.wijinputcore();
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.input.wijinputtext.prototype.destroy = function () {};
/** Gets the selected text.
  * @returns {string}
  */
wijmo.input.wijinputtext.prototype.getSelectedText = function () {};
/** Selects a range of text in the widget.
  * @param {number} start Start of the range.
  * @param {number} end End of the range.
  * @example
  * // Select first two symbols in a wijinputtext
  * $(".selector").wijinputtext("selectText", 0, 2);
  */
wijmo.input.wijinputtext.prototype.selectText = function (start, end) {};

/** @class */
var wijinputtext_options = function () {};
/** <p class='defaultValue'>Default value: true</p>
  * <p>Gets whether the control automatically converts to the proper format according to the format setting.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijinputtext_options.prototype.autoConvert = true;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether or not the next control in the tab order receives 
  * the focus as soon as the control is filled at the last character.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijinputtext_options.prototype.blurOnLastChar = false;
/** <p class='defaultValue'>Default value: 'none'</p>
  * <p>Gets or set whether the focus automatically moves to the next or previous
  * tab ordering control when pressing the left, right arrow keys.</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputtext_options.prototype.blurOnLeftRightKey = 'none';
/** <p>Gets or sets how to display the Ellipsis string when the control's content is 
  * longer than its Width.</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputtext_options.prototype.ellipsis = null;
/** <p>Gets or sets the Ellipsis string shows in the control.</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputtext_options.prototype.ellipsisString = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Gets or sets whether display the OverflowTip.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijinputtext_options.prototype.showOverflowTip = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Gets or sets whether to highlight the control's Text on receiving input focus.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijinputtext_options.prototype.highlightText = false;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the format string that defines the type of text 
  * allowed for input in the control.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * The following key words are supported.
  * DBCS Keywords:
  * Ａ Upper case DBCS alphabet (A-Z).
  * ａ Lower case DBCS alphabet (a-z).
  * Ｋ DBCS Katakana.
  * ９ DBCS Numbers (0-9).
  * ＃ DBCS numbers and number related symbols (0-9, +-$%\,.).
  * ＠ DBCS symbols.
  * Ｊ Hiragana.
  * Ｚ All DBCS characters without Space.
  * Ｎ Only DBCS large Katakanas.
  * Ｇ Only DBCS large Hiragana.
  * Ｔ Only allow surrogate char.
  * Ｄ All DBCS characters, except for surrogates.
  * Ｓ DBCS space.
  * SBCS Keywords:
  * A Upper case alphabet (A-Z).
  * a Lower case alphabet (a-z).
  * K Katakana.
  * 9 Numbers (0-9).
  * # Numbers and number related symbols (0-9, +-$%\,.).
  * @ Symbols.
  * H All SBCS characters without Space.
  * N Only SBCS large Katakanas.
  * S SBCS Space.
  * ^ Any character not included in the specified format.
  * \ Escape character.
  * For example, format = 'Ａ', then the wijinputtext can only input the upper case DBCS alphabet.
  * If you input the SBCS 'k', then it will be automatic conver to DBCS 'Ｋ' if the autoConvert is true,
  * But if the autoConvert is false, you can't input 'k'.
  */
wijinputtext_options.prototype.format = "";
/** <p class='defaultValue'>Default value: 'auto'</p>
  * <p>Sets the Ime Mode setting of widget.
  * Possible values are: 'auto', 'active', 'inactive', 'disabled'</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputtext_options.prototype.imeMode = 'auto';
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether the maximum length constraint for 
  * input is byte-based or character-based.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijinputtext_options.prototype.lengthAsByte = false;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Determines the maximum length of text that can be input 
  *  in the control.</p>
  * @field 
  * @type {number}
  * @option
  */
wijinputtext_options.prototype.maxLength = 0;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Determines the max count of lines can be input into the Edit control.</p>
  * @field 
  * @type {number}
  * @option
  */
wijinputtext_options.prototype.maxLineCount = 0;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether to treat the wrapped lines as one line or multiple lines
  * when counting the lines count.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijinputtext_options.prototype.countWrappedLine = false;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the password char.</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputtext_options.prototype.passwordChar = "";
/** <p class='defaultValue'>Default value: null</p>
  * <p>Determines the text of the wijinputtext.</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputtext_options.prototype.text = null;
/** The readingImeStringOutput event handler.
  * A function called when the japanese reading ime string is generated.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.input.IReadingImeStringOutputEventArgs} data Information about an event
  */
wijinputtext_options.prototype.readingImeStringOutput = null;
wijmo.input.wijinputtext.prototype.options = $.extend({}, true, wijmo.input.wijinputcore.prototype.options, new wijinputtext_options());
$.widget("wijmo.wijinputtext", $.wijmo.wijinputcore, wijmo.input.wijinputtext.prototype);
/** Contains information about wijinputtext.readingImeStringOutput event
  * @interface IReadingImeStringOutputEventArgs
  * @namespace wijmo.input
  */
wijmo.input.IReadingImeStringOutputEventArgs = function () {};
/** <p>generaged reading ime string.</p>
  * @field 
  * @type {readingString}
  */
wijmo.input.IReadingImeStringOutputEventArgs.prototype.the = null;
})()
})()
