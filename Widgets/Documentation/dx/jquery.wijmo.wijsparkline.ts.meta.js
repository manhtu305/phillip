var wijmo = wijmo || {};

(function () {
wijmo.sparkline = wijmo.sparkline || {};

(function () {
/** @class wijsparkline
  * @widget 
  * @namespace jQuery.wijmo.sparkline
  * @extends wijmo.wijmoWidget
  */
wijmo.sparkline.wijsparkline = function () {};
wijmo.sparkline.wijsparkline.prototype = new wijmo.wijmoWidget();
/** This method redraws the chart. */
wijmo.sparkline.wijsparkline.prototype.redraw = function () {};
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.sparkline.wijsparkline.prototype.destroy = function () {};

/** @class */
var wijsparkline_options = function () {};
/** <p class='defaultValue'>Default value: 'line'</p>
  * <p>Specifies the type of the sparkline widget.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * The value of the type can be 'line', 'area' and 'column'.
  * And the default value is 'line'.
  */
wijsparkline_options.prototype.type = 'line';
/** <p class='defaultValue'>Default value: []</p>
  * <p>Creates an array of series objects that contain data values and labels to display in the chart.</p>
  * @field 
  * @type {array}
  * @option 
  * @remarks
  * The series object contains following options
  * 1) type: Specifies the type of the sparkline widget.  The value for this option can be 'line', 'area' and 'column'.  And the default value is 'Line'.
  * 2) bind: Indicates that which property value is get from the object in the array if the data is set as a object array.
  * 3) seriesStyle: The specific style applies for current series of the sparkline widget for
  */
wijsparkline_options.prototype.seriesList = [];
/** <p class='defaultValue'>Default value: null</p>
  * <p>Sets the width of the sparkline widget in pixels.</p>
  * @field 
  * @type {?number}
  * @option 
  * @remarks
  * Note that this value overrides any value you may set in the &lt;div&gt; element that 
  * you use in the body of the HTML page
  * If you specify a width in the &lt;div&gt; element that is different from this value, 
  * the chart and its border go out of synch.
  */
wijsparkline_options.prototype.width = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Sets the height of the sparkline widget in pixels.</p>
  * @field 
  * @type {?number}
  * @option 
  * @remarks
  * Note that this value overrides any value you may set in the &lt;div&gt; element that 
  * you use in the body of the HTML page. If you specify a height in the &lt;div&gt; element that 
  * is different from this value, the chart and its border go out of synch.
  */
wijsparkline_options.prototype.height = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Sets the array to use as a source for data that you can bind to the sparkline widget.</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * The array can be a simple number array.  The array can be a complex object array also.
  * If it is an object array, use the seriesList object's bind option to specify the data field to use.
  */
wijsparkline_options.prototype.data = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value indicates that which property value is get from the object in the array if the data is set as a object array.</p>
  * @field 
  * @type {object}
  * @option
  */
wijsparkline_options.prototype.bind = null;
/** <p class='defaultValue'>Default value: []</p>
  * <p>Sets an array of style objects to use in rendering sparklines for each series in the sparkline widget.</p>
  * @field 
  * @type {array}
  * @option 
  * @remarks
  * Each style object in the array applies to one series in your seriesList,
  * so you need specify only as many style objects as you have series objects in your seriesList.
  */
wijsparkline_options.prototype.seriesStyles = [];
/** <p>The animation option defines the animation effect and controls other aspects of the widget's animation, 
  * such as duration and easing.</p>
  * @field 
  * @option
  */
wijsparkline_options.prototype.animation = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Axis line as option (off by default) used for identifying negative or positive values.</p>
  * @field 
  * @type {bool}
  * @option 
  * @remarks
  * This option just works with area type sparkline.
  */
wijsparkline_options.prototype.valueAxis = false;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Centers the value axis at the origin option setting value.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * This option just works when valueAxis is set to true.
  */
wijsparkline_options.prototype.origin = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates the minimum value of the sparkline.</p>
  * @field 
  * @type {?number}
  * @option
  */
wijsparkline_options.prototype.min = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates the maximum value of the sparkline.</p>
  * @field 
  * @type {?number}
  * @option
  */
wijsparkline_options.prototype.max = null;
/** <p class='defaultValue'>Default value: 10</p>
  * <p>Set width for each column</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * This option only works for column type sparkline.
  */
wijsparkline_options.prototype.columnWidth = 10;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value which formats the value for tooltip shown.</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * If the tooltipContent option is set, this option won't work.
  */
wijsparkline_options.prototype.tooltipFormat = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A function which is used to get a value for the tooltip shown.</p>
  * @field 
  * @type {function}
  * @option
  */
wijsparkline_options.prototype.tooltipContent = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Indicates whether the event handler of the sparkline widget is enable.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijsparkline_options.prototype.disabled = false;
/** This event fires when the user moves the mouse pointer while it is over a sparkline.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijSparklineEventArgs} args The data with this event.
  */
wijsparkline_options.prototype.mouseMove = null;
/** This event fires when the user clicks the sparkline.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijSparklineEventArgs} args The data with this event.
  */
wijsparkline_options.prototype.click = null;
wijmo.sparkline.wijsparkline.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijsparkline_options());
$.widget("wijmo.wijsparkline", $.wijmo.widget, wijmo.sparkline.wijsparkline.prototype);
/** The interface of WijSparklineOptions.
  * @interface WijSparklineOptions
  * @namespace wijmo.sparkline
  * @extends wijmo.WidgetOptions
  */
wijmo.sparkline.WijSparklineOptions = function () {};
/** <p>Specifies the type of the sparkline widget.</p>
  * @field 
  * @type {string}
  * @remarks
  * The value of the type can be 'line', 'area' and 'column'.
  * And the default value is 'line'.
  */
wijmo.sparkline.WijSparklineOptions.prototype.type = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.sparkline.WijSparklineSeries.html'>wijmo.sparkline.WijSparklineSeries[]</a></p>
  * <p>Creates an array of series objects that contain data values and labels to display in the chart.</p>
  * @field 
  * @type {wijmo.sparkline.WijSparklineSeries[]}
  * @remarks
  * The series object contains following options
  * 1) type: Specifies the type of the sparkline widget.  The value for this option can be 'line', 'area' and 'column'.  And the default value is 'Line'.
  * 2) bind: Indicates that which property value is get from the object in the array if the data is set as a object array.
  * 3) seriesStyle: The specific style applies for current series of the sparkline widget for
  */
wijmo.sparkline.WijSparklineOptions.prototype.seriesList = null;
/** <p>Sets the width of the sparkline widget in pixels.</p>
  * @field 
  * @type {?number}
  * @remarks
  * Note that this value overrides any value you may set in the &lt;div&gt; element that 
  * you use in the body of the HTML page
  * If you specify a width in the &lt;div&gt; element that is different from this value, 
  * the chart and its border go out of synch.
  */
wijmo.sparkline.WijSparklineOptions.prototype.width = null;
/** <p>Sets the height of the sparkline widget in pixels.</p>
  * @field 
  * @type {?number}
  * @remarks
  * Note that this value overrides any value you may set in the &lt;div&gt; element that 
  * you use in the body of the HTML page. If you specify a height in the &lt;div&gt; element that 
  * is different from this value, the chart and its border go out of synch.
  */
wijmo.sparkline.WijSparklineOptions.prototype.height = null;
/** <p>Sets the array to use as a source for data that you can bind to the sparkline widget.</p>
  * @field 
  * @type {array}
  * @remarks
  * The array can be a simple number array.  The array can be a complex object array also.
  * If it is an object array, use the seriesList object's bind option to specify the data field to use.
  */
wijmo.sparkline.WijSparklineOptions.prototype.data = null;
/** <p>A value indicates that which property value is get from the object in the array if the data is set as a object array.</p>
  * @field 
  * @type {string}
  */
wijmo.sparkline.WijSparklineOptions.prototype.bind = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.sparkline.WijSparklineSeriesStyle.html'>wijmo.sparkline.WijSparklineSeriesStyle[]</a></p>
  * <p>Sets an array of style objects to use in rendering sparklines for each series in the sparkline widget.</p>
  * @field 
  * @type {wijmo.sparkline.WijSparklineSeriesStyle[]}
  * @remarks
  * Each style object in the array applies to one series in your seriesList,
  * so you need specify only as many style objects as you have series objects in your seriesList.
  */
wijmo.sparkline.WijSparklineOptions.prototype.seriesStyles = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.sparkline.WijSparklineAnimation.html'>wijmo.sparkline.WijSparklineAnimation</a></p>
  * <p>The animation option defines the animation effect and controls other aspects of the widget's animation, 
  * such as duration and easing.</p>
  * @field 
  * @type {wijmo.sparkline.WijSparklineAnimation}
  */
wijmo.sparkline.WijSparklineOptions.prototype.animation = null;
/** <p>Axis line as option (off by default) used for identifying negative or positive values.</p>
  * @field 
  * @type {bool}
  * @remarks
  * This option just works with area type sparkline.
  */
wijmo.sparkline.WijSparklineOptions.prototype.valueAxis = null;
/** <p>Centers the value axis at the origin option setting value.</p>
  * @field 
  * @type {number}
  * @remarks
  * This option just works when valueAxis is set to true.
  */
wijmo.sparkline.WijSparklineOptions.prototype.origin = null;
/** <p>A value that indicates the minimum value of the sparkline.</p>
  * @field 
  * @type {?number}
  */
wijmo.sparkline.WijSparklineOptions.prototype.min = null;
/** <p>A value that indicates the maximum value of the sparkline.</p>
  * @field 
  * @type {?number}
  */
wijmo.sparkline.WijSparklineOptions.prototype.max = null;
/** <p>Set width for each column</p>
  * @field 
  * @type {number}
  * @remarks
  * This option only works for column type sparkline.
  */
wijmo.sparkline.WijSparklineOptions.prototype.columnWidth = null;
/** <p>A value which formats the value for tooltip shown.</p>
  * @field 
  * @type {string}
  * @remarks
  * If the tooltipContent option is set, this option won't work.
  */
wijmo.sparkline.WijSparklineOptions.prototype.tooltipFormat = null;
/** <p>A function which is used to get a value for the tooltip shown.</p>
  * @field 
  * @type {function}
  */
wijmo.sparkline.WijSparklineOptions.prototype.tooltipContent = null;
/** <p>This event fires when the user moves the mouse pointer while it is over a sparkline.</p>
  * @field 
  * @type {function}
  */
wijmo.sparkline.WijSparklineOptions.prototype.mouseMove = null;
/** <p>This event fires when the user clicks the sparkline.</p>
  * @field 
  * @type {function}
  */
wijmo.sparkline.WijSparklineOptions.prototype.click = null;
/** <p>Indicates whether the event handler of the sparkline widget is enable.</p>
  * @field 
  * @type {boolean}
  */
wijmo.sparkline.WijSparklineOptions.prototype.disabled = null;
typeof wijmo.WidgetOptions != 'undefined' && $.extend(wijmo.sparkline.WijSparklineOptions.prototype, wijmo.WidgetOptions.prototype);
})()
})()
