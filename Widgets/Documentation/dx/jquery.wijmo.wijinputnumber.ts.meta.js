var wijmo = wijmo || {};

(function () {
wijmo.input = wijmo.input || {};

(function () {
/** @class wijinputnumber
  * @widget 
  * @namespace jQuery.wijmo.input
  * @extends wijmo.input.wijinputcore
  */
wijmo.input.wijinputnumber = function () {};
wijmo.input.wijinputnumber.prototype = new wijmo.input.wijinputcore();
/** Set the focus to the widget. */
wijmo.input.wijinputnumber.prototype.focus = function () {};
/** Gets the value. */
wijmo.input.wijinputnumber.prototype.getValue = function () {};
/** Sets the value.
  * @param val
  * @param exact
  * @returns {bool}
  * @example
  * // set value of a wijinputnumber to 10
  * $(".selector").wijinputnumber("setValue", 10, true);
  */
wijmo.input.wijinputnumber.prototype.setValue = function (val, exact) {};
/** Determines whether the value is in null state.
  * @returns {bool}
  */
wijmo.input.wijinputnumber.prototype.isValueNull = function () {};
/** Gets the text value when the container form is posted back to server. */
wijmo.input.wijinputnumber.prototype.getPostValue = function () {};
/** Performs spin up by the specified field and increment value. */
wijmo.input.wijinputnumber.prototype.spinUp = function () {};
/** Performs spin down by the specified field and increment value. */
wijmo.input.wijinputnumber.prototype.spinDown = function () {};

/** @class */
var wijinputnumber_options = function () {};
/** <p>undefined</p>
  * @field 
  * @option
  */
wijinputnumber_options.prototype.wijCSS = null;
/** <p class='defaultValue'>Default value: 'numeric'</p>
  * <p>Determines the type of the number input.
  * Possible values are: 'numeric', 'percent', 'currency'.</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputnumber_options.prototype.type = 'numeric';
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Determines the default numeric value.</p>
  * @field 
  * @type {number}
  * @option
  */
wijinputnumber_options.prototype.value = 0;
/** <p class='defaultValue'>Default value: -1000000000</p>
  * <p>Determines the minimal value that can be entered for 
  * numeric/percent/currency inputs.</p>
  * @field 
  * @type {number}
  * @option
  */
wijinputnumber_options.prototype.minValue = -1000000000;
/** <p class='defaultValue'>Default value: 1000000000</p>
  * <p>Determines the maximum value that can be entered for 
  * numeric/percent/currency inputs.</p>
  * @field 
  * @type {number}
  * @option
  */
wijinputnumber_options.prototype.maxValue = 1000000000;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Indicates whether the thousands group separator will be 
  * inserted between between each digital group 
  * (number of digits in thousands group depends on the selected Culture).</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijinputnumber_options.prototype.showGroup = false;
/** <p class='defaultValue'>Default value: 2</p>
  * <p>Indicates the number of decimal places to display.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Possible values are integer from -2 to 8.
  */
wijinputnumber_options.prototype.decimalPlaces = 2;
/** <p class='defaultValue'>Default value: 1</p>
  * <p>Determines how much to increase/decrease the active field when performing spin on the the active field.</p>
  * @field 
  * @type {number}
  * @option
  */
wijinputnumber_options.prototype.increment = 1;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Determine the current symbol when number type is currency.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * If the currencySymbol's value is null and number's type is currency, wijinputnumber will dispaly the currency symbol from the current culture, for use US culture, it is '$'.  &lt;br /&gt;
  * But user can change this behavior by setting the currencySymbol option, then the '$' will changed to the specified value.
  */
wijinputnumber_options.prototype.currencySymbol = null;
/** <p>Determine the class string that when the input value is negative.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * When the number widget's value is negative, the negativeClass will apply the number input.
  * @example
  * // &lt;style type="text/css" rel="stylesheet"&gt;
  *  //         .negative {
  *  //             background: yellow;
  *  //             color: red !important;
  *  //         }
  *  //&lt;/style&gt;
  * $("#textbox1").wijinputnumber({
  *       negativeClass: 'negative'
  *         
  * });
  */
wijinputnumber_options.prototype.negativeClass = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Determine the prefix string used for negative value.
  * The default value will depend on the wijinputnumber's type option.</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputnumber_options.prototype.negativePrefix = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Determine the suffix string used for negative value.
  * The default value will depend on the wijinputnumber's type option.</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputnumber_options.prototype.negativeSuffix = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Determine the prefix string used for positive value.
  * The default value will depend on the wijinputnumber's type option.</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputnumber_options.prototype.positivePrefix = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Determine the suffix stirng used for positive value.
  * The default value will depend on the wijinputnumber's type option.</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputnumber_options.prototype.positiveSuffix = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Indicates whether to highlight the control's Text on receiving input focus.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijinputnumber_options.prototype.highlightText = false;
/** The valueChanged event handler. 
  * A function called when the value of the input is changed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.input.IValueChangedEventArgs} data Information about an event
  */
wijinputnumber_options.prototype.valueChanged = null;
/** The spinUp event handler.
  * A function called when spin up event fired.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijinputnumber_options.prototype.spinUp = null;
/** The spinDown event handler.
  * A function called when spin down event fired.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijinputnumber_options.prototype.spinDown = null;
/** The valueBoundsExceeded event handler. A function called when 
  * the value of the input exceeds the valid range.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijinputnumber_options.prototype.valueBoundsExceeded = null;
wijmo.input.wijinputnumber.prototype.options = $.extend({}, true, wijmo.input.wijinputcore.prototype.options, new wijinputnumber_options());
$.widget("wijmo.wijinputnumber", $.wijmo.wijinputcore, wijmo.input.wijinputnumber.prototype);
/** Contains information about wijinputnumber.valueChanged event
  * @interface IValueChangedEventArgs
  * @namespace wijmo.input
  */
wijmo.input.IValueChangedEventArgs = function () {};
/** <p>The new value</p>
  * @field
  */
wijmo.input.IValueChangedEventArgs.prototype.value = null;
})()
})()
