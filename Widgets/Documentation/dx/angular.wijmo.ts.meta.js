var wijmo = wijmo || {};

(function () {
wijmo.ng = wijmo.ng || {};

(function () {
/** Describes type structure. If an object, lists its properties. If an array, specifies its element type
  * @interface TypeDef
  * @namespace wijmo.ng
  */
wijmo.ng.TypeDef = function () {};
/** <p>One of the following: string, number, boolean, object, array, date</p>
  * @field 
  * @type {string}
  */
wijmo.ng.TypeDef.prototype.type = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.ng.TypeDef.html'>wijmo.ng.TypeDef</a></p>
  * <p>If type is an array, describes the structure of its elements</p>
  * @field 
  * @type {wijmo.ng.TypeDef}
  */
wijmo.ng.TypeDef.prototype.elementType = null;
/** <p>If type is an object, describes object properties in form of a hash</p>
  * @field
  */
wijmo.ng.TypeDef.prototype.properties = null;
/** Describes type structure of a property.
  * @interface PropertyDefinition
  * @namespace wijmo.ng
  * @extends wijmo.ng.TypeDef
  */
wijmo.ng.PropertyDefinition = function () {};
/** <p>Specifies singular name for the property that describes a collection of items. 
  * For example, singular for property "ranges" is "range".</p>
  * @field 
  * @type {string}
  */
wijmo.ng.PropertyDefinition.prototype.singular = null;
/** <p>Specifies one or more events that are triggered when the value of this property changes</p>
  * @field 
  * @type {string|Array[string]}
  */
wijmo.ng.PropertyDefinition.prototype.changeEvent = null;
/** <p>True if two-way binding is needed for the property. Otherwise, false.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * If binding is two-way, the property name must not be surrounded with curly brackets when specified in markup.
  * If binding is one-way, the property value is cloned on binding
  */
wijmo.ng.PropertyDefinition.prototype.twoWayBinding = null;
wijmo.ng.propPath = wijmo.ng.propPath || {};

(function () {
/** Split a property path to components
  * @param {string} path
  * @returns {string[]}
  */
wijmo.ng.propPath.partition = function (path) {};
/** Read property path value.
  * @param obj
  * @param path
  * @remarks
  * Throws an exception if an intermediate value is null or undefined
  */
wijmo.ng.propPath.get = function (obj, path) {};
/** Set property path value
  * @param obj
  * @param path
  * @param value
  * @remarks
  * Throws an exception if an intermediate value is null or undefined
  */
wijmo.ng.propPath.set = function (obj, path, value) {};
typeof wijmo.ng.TypeDef != 'undefined' && $.extend(wijmo.ng.PropertyDefinition.prototype, wijmo.ng.TypeDef.prototype);
})()
wijmo.ng.ngTools = wijmo.ng.ngTools || {};

(function () {
/** Call a function on the next digest broadcast message of a scope.
  * @param scope
  * @param fn
  */
wijmo.ng.ngTools.onDigest = function (scope, fn) {};
})()
/** Describes a widget
  * @interface WidgetMetadata
  * @namespace wijmo.ng
  */
wijmo.ng.WidgetMetadata = function () {};
/** <p>Name of a base widget</p>
  * @field 
  * @type {string}
  */
wijmo.ng.WidgetMetadata.prototype.inherits = null;
/** <p>A hash of widget events</p>
  * @field
  */
wijmo.ng.WidgetMetadata.prototype.events = null;
/** <p>A hash of widget properties</p>
  * @field
  */
wijmo.ng.WidgetMetadata.prototype.properties = null;
/** A binding between a widget option and scope expression
  * @interface Binding
  * @namespace wijmo.ng
  */
wijmo.ng.Binding = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.ng.TypeDef.html'>wijmo.ng.TypeDef</a></p>
  * <p>Value type</p>
  * @field 
  * @type {wijmo.ng.TypeDef}
  */
wijmo.ng.Binding.prototype.typeDef = null;
/** <p>Scope expression</p>
  * @field 
  * @type {string}
  */
wijmo.ng.Binding.prototype.expression = null;
/** <p>Widget option path. The first component must be an option name</p>
  * @field 
  * @type {string}
  */
wijmo.ng.Binding.prototype.path = null;
/** <p>True if widget option is an event handler</p>
  * @field 
  * @type {boolean}
  */
wijmo.ng.Binding.prototype.isEvent = null;
/** <p>Angular interpolation string, such as "Hello {{firstName}}!"</p>
  * @field 
  * @type {string}
  */
wijmo.ng.Binding.prototype.interpolationString = null;
/** Angular services used by Wijmo directives
  * @interface Services
  * @namespace wijmo.ng
  */
wijmo.ng.Services = function () {};
/** <p>undefined</p>
  * @field 
  * @type {function}
  */
wijmo.ng.Services.prototype.$compile = null;
/** @interface TextEvalEntryHash
  * @namespace wijmo.ng
  */
wijmo.ng.TextEvalEntryHash = function () {};
/** A link function returned by ng.$compile service
  * http://docs.angularjs.org/api/ng.$compile
  * @interface LinkFunction
  * @namespace wijmo.ng
  */
wijmo.ng.LinkFunction = function () {};
wijmo.ng.definitions = wijmo.ng.definitions || {};

(function () {
/** Base class for all Wijmo directives
  * @class DirectiveBase
  * @namespace wijmo.ng.definitions
  */
wijmo.ng.definitions.DirectiveBase = function () {};
/** Merge metadata from different sources: the widget type, its base types and default values
  * @param {string} widgetName
  * @param options
  */
wijmo.ng.definitions.DirectiveBase.prototype.mergeMetadata = function (widgetName, options) {};
/** Creates a new object prototyped from the current one
  * @returns {wijmo.ng.definitions.DirectiveBase}
  * @remarks
  * This method is used for transition from one state to another. 
  * The possible states are: created -&gt; compiled -&gt; linked
  * More than one object may be prototyped from one object, for example, a directive may be linked many times
  * and it means that many linked directives must be created from a compiled one
  */
wijmo.ng.definitions.DirectiveBase.prototype.transition = function () {};
/** Compiles the directive according to the tElem and tAttrs params
  * @param tElem
  * @param tAttrs
  * @param $compile
  * @returns a new object in the compiled state, prototyped from this one
  */
wijmo.ng.definitions.DirectiveBase.prototype.compile = function (tElem, tAttrs, $compile) {};
/** Transition to the linked state
  * @param scope
  * @param elem
  * @param attrs
  */
wijmo.ng.definitions.DirectiveBase.prototype.link = function (scope, elem, attrs) {};
/** @param {string} name
  * @returns {string}
  */
wijmo.ng.definitions.DirectiveBase.prototype.getEventFullName = function (name) {};
/** @param {string} name
  * @param {Function} handler
  */
wijmo.ng.definitions.DirectiveBase.prototype.unbindFromWidget = function (name, handler) {};
/** @param {string} name
  * @param {Function} handler
  */
wijmo.ng.definitions.DirectiveBase.prototype.bindToWidget = function (name, handler) {};
/** Listen to changes of the "data" option.
  * @param {wijmo.ng.Binding} binding
  * @param {function} handler
  */
wijmo.ng.definitions.DirectiveBase.prototype.watchData = function (binding, handler) {};
/** Listen to changes of an option
  * @param {wijmo.ng.Binding} binding
  * @param {function} handler
  */
wijmo.ng.definitions.DirectiveBase.prototype.watchBinding = function (binding, handler) {};
/** Process data-bindings
  * @param creationOptions
  */
wijmo.ng.definitions.DirectiveBase.prototype.wireData = function (creationOptions) {};
/** Set option value
  * @param {string} path an option name, or a path that starts with an option name
  */
wijmo.ng.definitions.DirectiveBase.prototype.setOption = function (path) {};
/** A base class for attribute directives
  * @class AttributeDirective
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.DirectiveBase
  */
wijmo.ng.definitions.AttributeDirective = function () {};
wijmo.ng.definitions.AttributeDirective.prototype = new wijmo.ng.definitions.DirectiveBase();
/** Parse an element tree to options, data-binding entries and inner markup subelements
  * @param elem
  * @returns {Markup}
  */
wijmo.ng.definitions.AttributeDirective.prototype.parseMarkup = function (elem) {};
/** A base class for element directives
  * @class ElementDirective
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.DirectiveBase
  */
wijmo.ng.definitions.ElementDirective = function () {};
wijmo.ng.definitions.ElementDirective.prototype = new wijmo.ng.definitions.DirectiveBase();
/** Creates Markup object. The method may be overridden in a derived class.
  * @param elem
  * @param {wijmo.ng.WidgetMetadata} meta
  * @returns {Markup}
  */
wijmo.ng.definitions.ElementDirective.prototype.createMarkup = function (elem, meta) {};
/** Parse an element tree to options, data-binding entries and inner markup subelements
  * @param elem
  * @returns {Markup}
  */
wijmo.ng.definitions.ElementDirective.prototype.parseMarkup = function (elem) {};
/** @param options */
wijmo.ng.definitions.ElementDirective.prototype.createInstanceCore = function (options) {};
/** @interface GridCellTemplate
  * @namespace wijmo.ng.definitions
  */
wijmo.ng.definitions.GridCellTemplate = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.ng.Subelement.html'>wijmo.ng.Subelement</a></p>
  * <p>undefined</p>
  * @field 
  * @type {wijmo.ng.Subelement}
  */
wijmo.ng.definitions.GridCellTemplate.prototype.view = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.ng.Subelement.html'>wijmo.ng.Subelement</a></p>
  * <p>undefined</p>
  * @field 
  * @type {wijmo.ng.Subelement}
  */
wijmo.ng.definitions.GridCellTemplate.prototype.edit = null;
/** <p>undefined</p>
  * @field 
  * @type {String}
  */
wijmo.ng.definitions.GridCellTemplate.prototype.dataKey = null;
/** wijgrid directive extends ElementDirective by adding cell templates
  * @class wijgrid
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.ElementDirective
  */
wijmo.ng.definitions.wijgrid = function () {};
wijmo.ng.definitions.wijgrid.prototype = new wijmo.ng.definitions.ElementDirective();
/** @param elem
  * @param typeDef
  * @returns {GridMarkup}
  */
wijmo.ng.definitions.wijgrid.prototype.createMarkup = function (elem, typeDef) {};
/**  */
wijmo.ng.definitions.wijgrid.prototype.dataOptionExression = function () {};
/** @param scope
  * @param options
  */
wijmo.ng.definitions.wijgrid.prototype.applyCellTemplates = function (scope, options) {};
/** @param options */
wijmo.ng.definitions.wijgrid.prototype.createInstanceCore = function (options) {};
/** @class wijgridfilter
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.wijgrid
  */
wijmo.ng.definitions.wijgridfilter = function () {};
wijmo.ng.definitions.wijgridfilter.prototype = new wijmo.ng.definitions.wijgrid();
/** @class wijcombobox
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.ElementDirective
  */
wijmo.ng.definitions.wijcombobox = function () {};
wijmo.ng.definitions.wijcombobox.prototype = new wijmo.ng.definitions.ElementDirective();
/** @class wijupload
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.ElementDirective
  */
wijmo.ng.definitions.wijupload = function () {};
wijmo.ng.definitions.wijupload.prototype = new wijmo.ng.definitions.ElementDirective();
/** @class wijinputcore
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.ElementDirective
  */
wijmo.ng.definitions.wijinputcore = function () {};
wijmo.ng.definitions.wijinputcore.prototype = new wijmo.ng.definitions.ElementDirective();
/** @class wijinputdate
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.wijinputcore
  */
wijmo.ng.definitions.wijinputdate = function () {};
wijmo.ng.definitions.wijinputdate.prototype = new wijmo.ng.definitions.wijinputcore();
/** @class wijinputmask
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.wijinputcore
  */
wijmo.ng.definitions.wijinputmask = function () {};
wijmo.ng.definitions.wijinputmask.prototype = new wijmo.ng.definitions.wijinputcore();
/** @class wijinputnumber
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.wijinputcore
  */
wijmo.ng.definitions.wijinputnumber = function () {};
wijmo.ng.definitions.wijinputnumber.prototype = new wijmo.ng.definitions.wijinputcore();
/** @class wijinputtext
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.wijinputcore
  */
wijmo.ng.definitions.wijinputtext = function () {};
wijmo.ng.definitions.wijinputtext.prototype = new wijmo.ng.definitions.wijinputcore();
/** @class wijcheckbox
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.AttributeDirective
  */
wijmo.ng.definitions.wijcheckbox = function () {};
wijmo.ng.definitions.wijcheckbox.prototype = new wijmo.ng.definitions.AttributeDirective();
/** @class wijradio
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.wijcheckbox
  */
wijmo.ng.definitions.wijradio = function () {};
wijmo.ng.definitions.wijradio.prototype = new wijmo.ng.definitions.wijcheckbox();
/** @class wijsplitter
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.ElementDirective
  */
wijmo.ng.definitions.wijsplitter = function () {};
wijmo.ng.definitions.wijsplitter.prototype = new wijmo.ng.definitions.ElementDirective();
/** @class wijexpander
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.ElementDirective
  */
wijmo.ng.definitions.wijexpander = function () {};
wijmo.ng.definitions.wijexpander.prototype = new wijmo.ng.definitions.ElementDirective();
/** @class wijmenu
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.ElementDirective
  */
wijmo.ng.definitions.wijmenu = function () {};
wijmo.ng.definitions.wijmenu.prototype = new wijmo.ng.definitions.ElementDirective();
/** @class wijtree
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.ElementDirective
  */
wijmo.ng.definitions.wijtree = function () {};
wijmo.ng.definitions.wijtree.prototype = new wijmo.ng.definitions.ElementDirective();
/** @class wijchartnavigator
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.ElementDirective
  */
wijmo.ng.definitions.wijchartnavigator = function () {};
wijmo.ng.definitions.wijchartnavigator.prototype = new wijmo.ng.definitions.ElementDirective();
/** @class wijeditor
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.ElementDirective
  */
wijmo.ng.definitions.wijeditor = function () {};
wijmo.ng.definitions.wijeditor.prototype = new wijmo.ng.definitions.ElementDirective();
/** @class wijtabs
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.ElementDirective
  */
wijmo.ng.definitions.wijtabs = function () {};
wijmo.ng.definitions.wijtabs.prototype = new wijmo.ng.definitions.ElementDirective();
/** @param element
  * @param typeDef
  * @returns {TabsMarkup}
  */
wijmo.ng.definitions.wijtabs.prototype.createMarkup = function (element, typeDef) {};
/** @class wijspread
  * @namespace wijmo.ng.definitions
  * @extends wijmo.ng.definitions.ElementDirective
  */
wijmo.ng.definitions.wijspread = function () {};
wijmo.ng.definitions.wijspread.prototype = new wijmo.ng.definitions.ElementDirective();
/** @param {string} path
  * @param value
  */
wijmo.ng.definitions.wijspread.prototype.setOption = function (path, value) {};
/** @param {wijmo.ng.Binding} binding
  * @param {function} handler
  */
wijmo.ng.definitions.wijspread.prototype.watchBinding = function (binding, handler) {};
/** @interface DirectiveClass
  * @namespace wijmo.ng.definitions
  */
wijmo.ng.definitions.DirectiveClass = function () {};
/** Find a directive class in this module for a given widget name
  * @param {string} widgetName
  * @returns {wijmo.ng.definitions.DirectiveClass}
  */
wijmo.ng.definitions.getDirectiveClass = function (widgetName) {};
})()
})()
})()
