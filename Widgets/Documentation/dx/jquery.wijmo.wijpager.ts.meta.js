var wijmo = wijmo || {};

(function () {
wijmo.pager = wijmo.pager || {};

(function () {
/** @class wijpager
  * @widget 
  * @namespace jQuery.wijmo.pager
  * @extends wijmo.wijmoWidget
  */
wijmo.pager.wijpager = function () {};
wijmo.pager.wijpager.prototype = new wijmo.wijmoWidget();

/** @class */
var wijpager_options = function () {};
/** <p>An option that indicates the class of the first-page button. You can set this option with any of the jQuery UI CSS Framework icons.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * // Here's the general way you'll set the option:
  * $("#element").wijpager({
  *    firstPageClass: "ui-icon-seek-first"
  * });
  */
wijpager_options.prototype.firstPageClass = null;
/** <p class='defaultValue'>Default value: 'First'</p>
  * <p>An option that indicates the text to display for the first-page button. The text will display like a tooltip when a user hovers over the first-page button.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * // Here's the general way you'll set the option:
  * $("#element").wijpager({
  *    firstPageText: "Go To First"
  * });
  */
wijpager_options.prototype.firstPageText = 'First';
/** <p>An option that indicates the class of the last-page button. You can set this option with any of the jQuery UI CSS Framework icons.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * // Here's the general way you'll set the option:
  * $("#element").wijpager({
  *    lastPageClass: "ui-icon-seek-end"
  * });
  */
wijpager_options.prototype.lastPageClass = null;
/** <p class='defaultValue'>Default value: 'Last'</p>
  * <p>An option that indicates the text to display for the last-page button. The text will display like a tooltip when a user hovers over the last-page button.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * // Here's the general way you'll set the option:
  * $("#element").wijpager({
  *   lastPageText: "Go To Last"
  * });
  */
wijpager_options.prototype.lastPageText = 'Last';
/** <p class='defaultValue'>Default value: 'numeric'</p>
  * <p>This option determines the pager mode. The possible values for this option are: "nextPrevious", "nextPreviousFirstLast", "numeric", "numericFirstLast".</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Possible values are: "nextPrevious", "nextPreviousFirstLast", "numeric", "numericFirstLast".
  * "nextPrevious": a set of pagination controls consisting of Previous and Next buttons.
  * "nextPreviousFirstLast": a set of pagination controls consisting of Previous, Next, First, and Last buttons.
  * "numeric": a set of pagination controls consisting of numbered link buttons to access pages directly.
  * "numericFirstLast": a set of pagination controls consisting of numbered and First and Last link buttons.
  * @example
  * // Here's the general way you'll set the option: 
  * $("#element").wijpager({
  *    mode: "nextPrevious"
  * });
  */
wijpager_options.prototype.mode = 'numeric';
/** <p>An option that indicates the class of the next-page button. You can set this option with any of the jQuery UI CSS Framework Icons.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * // Here's the general way you'll set the option:
  * $("#element").wijpager({
  *    nextPageClass: "ui-icon-seek-next"
  * });
  */
wijpager_options.prototype.nextPageClass = null;
/** <p class='defaultValue'>Default value: 'Next'</p>
  * <p>An option that indicates the text to display for the next-page button. The text appears like a tooltip when a user hovers over the next-page button.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * // Here's the general way you'll set the option:
  * $("#element").wijpager({
  *    nextPageText: "Go To Next"
  * });
  */
wijpager_options.prototype.nextPageText = 'Next';
/** <p class='defaultValue'>Default value: 10</p>
  * <p>An option that indicates the number of page buttons to display in the pager. You can customize how many page buttons are available for your users to use to page through content.
  * You can use this option in conjunction with the pageCount to customize the pager display.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * // Here's the general way you'll set the option:
  * $("#element").wijpager({
  *    pageButtonCount: 15
  * });
  */
wijpager_options.prototype.pageButtonCount = 10;
/** <p>An option that indicates the class of the previous-page button. You can set this option with any of the jQuery UI CSS Framework Icons.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * // Here's the general way you'll set the option:
  * $("#element").wijpager({
  *    previousPageClass: "ui-icon-seek-prev"
  * });
  */
wijpager_options.prototype.previousPageClass = null;
/** <p class='defaultValue'>Default value: 'Previous'</p>
  * <p>An option that indicates the text to display for the previous-page button. The text appears like a tooltip when a user hovers over the previous-page button.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * // Here's the general way you'll set the option:
  * $("#element").wijpager({
  *    previousPageText: "Go To Previous"
  * });
  */
wijpager_options.prototype.previousPageText = 'Previous';
/** <p class='defaultValue'>Default value: 1</p>
  * <p>An option that indicates the total number of pages. This option allows you to customize the total number of pages that your users will be able to page through. You can use this option in conjunction with the pageButtonCount to customize the pager display.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * // Here's the general way you'll set the option:
  * $("#element").wijpager({
  *    pageCount: 10
  * });
  */
wijpager_options.prototype.pageCount = 1;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>An option that indicates the zero-based index of the current page. By default, your pager will display with the first pager button highlighted since its index is 0.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * // Here's the general way you'll set the option. This will allow your pager widget to display with the 3rd page button highlighted:
  * $("#element").wijpager({
  *    pageIndex: 2
  * });
  */
wijpager_options.prototype.pageIndex = 0;
/** The pageIndexChanging event handler is a function called when page index is changing. This item is cancellable if you return false.
  * @event 
  * @param {object} e The jQuery.Event object.
  * @param {wijmo.grid.IPageIndexChangingEventArgs} args The data associated with this event.
  * @example
  * // Supply a callback function to handle the event:
  * $("#element").wijpager({
  *    pageIndexChanging: function (e, args) {
  *       // Handle the event here.
  *    }
  * });
  * // Bind to the event by type:
  * $("#element").bind("wijpagerpageindexchanging", function (e, args) {
  *    // Handle the event here.
  * });
  * // You can cancel this event be returning false:
  * $("#element").wijpager({
  *    pageIndexChanging: function(e, args) {
  *       return false;
  *    }
  * });
  */
wijpager_options.prototype.pageIndexChanging = null;
/** The pageIndexChanged event handler is a function called when the page index is changed.
  * @event 
  * @param {object} e The jQuery.Event object.
  * @param {wijmo.grid.IPageIndexChangedEventArgs} args The data associated with this event.
  * @example
  * // Supply a callback function to handle the event:
  * $("#element").wijpager({
  *    pageIndexChanged: function (e, args) {
  *       // Handle the event here.
  *    }
  * });
  * // Bind to the event by type:
  * $("#element").bind("wijpagerpageindexchanged", function (e, args) {
  *    // Handle the event here.
  * });
  * // You can also use this event to get the selected page by using the args.newPageIndex parameter:
  * $("#element").wijpager({
  *    pageIndexChanged: function(e, args) {
  *       var selectedPageIndex = args.newPageIndex;
  *    }
  * });
  */
wijpager_options.prototype.pageIndexChanged = null;
wijmo.pager.wijpager.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijpager_options());
$.widget("wijmo.wijpager", $.wijmo.widget, wijmo.pager.wijpager.prototype);
/** Provides data for the pageIndexChanging event of the wijpager.
  * @interface IPageIndexChangingEventArgs
  * @namespace wijmo.pager
  */
wijmo.pager.IPageIndexChangingEventArgs = function () {};
/** <p>The new pageIndex.</p>
  * @field 
  * @type {number}
  */
wijmo.pager.IPageIndexChangingEventArgs.prototype.newPageIndex = null;
/** Provides data for the pageIndexChanged event of the wijpager.
  * @interface IPageIndexChangedEventArgs
  * @namespace wijmo.pager
  */
wijmo.pager.IPageIndexChangedEventArgs = function () {};
/** <p>The new pageIndex.</p>
  * @field 
  * @type {number}
  */
wijmo.pager.IPageIndexChangedEventArgs.prototype.newPageIndex = null;
/** Provides data for the pageIndexChanged event of the wijpager.
  * @interface IPageIndexChanged1EventArgs
  * @namespace wijmo.pager
  */
wijmo.pager.IPageIndexChanged1EventArgs = function () {};
/** <p>The new pageIndex</p>
  * @field 
  * @type {number}
  */
wijmo.pager.IPageIndexChanged1EventArgs.prototype.newPageIndex = null;
/** Represents the wijpager's UI settings.
  * @interface IPagerUISettings
  * @namespace wijmo.pager
  */
wijmo.pager.IPagerUISettings = function () {};
/** <p>An option that indicates the class of the first-page button.</p>
  * @field 
  * @type {string}
  */
wijmo.pager.IPagerUISettings.prototype.firstPageClass = null;
/** <p>An option that indicates the text to display for the first-page button.</p>
  * @field 
  * @type {string}
  */
wijmo.pager.IPagerUISettings.prototype.firstPageText = null;
/** <p>An option that indicates the class of the last-page button.</p>
  * @field 
  * @type {string}
  */
wijmo.pager.IPagerUISettings.prototype.lastPageClass = null;
/** <p>An option that indicates the text to display for the last-page button.</p>
  * @field 
  * @type {string}
  */
wijmo.pager.IPagerUISettings.prototype.lastPageText = null;
/** <p>An option that determines the pager mode. Possible values are: "nextPrevious", "nextPreviousFirstLast", "numeric", "numericFirstLast".</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are:
  * "nextPrevious": a set of pagination controls consisting of Previous and Next buttons.
  * "nextPreviousFirstLast": a set of pagination controls consisting of Previous, Next, First, and Last buttons.
  * "numeric": a set of pagination controls consisting of numbered link buttons to access pages directly.
  * "numericFirstLast": a set of pagination controls consisting of numbered and First and Last link buttons.
  */
wijmo.pager.IPagerUISettings.prototype.mode = null;
/** <p>An option that controls the class of the next-page button.</p>
  * @field 
  * @type {string}
  */
wijmo.pager.IPagerUISettings.prototype.nextPageClass = null;
/** <p>An option that indicates the text to display for the next-page button.</p>
  * @field 
  * @type {string}
  */
wijmo.pager.IPagerUISettings.prototype.nextPageText = null;
/** <p>An option that indicates the number of page buttons to display in the pager.</p>
  * @field 
  * @type {number}
  */
wijmo.pager.IPagerUISettings.prototype.pageButtonCount = null;
/** <p>An option that indicates the class of the previous-page button.</p>
  * @field 
  * @type {string}
  */
wijmo.pager.IPagerUISettings.prototype.previousPageClass = null;
/** <p>An option that indicates the text to display for the previous-page button.</p>
  * @field 
  * @type {string}
  */
wijmo.pager.IPagerUISettings.prototype.previousPageText = null;
})()
})()
