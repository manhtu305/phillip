var wijmo = wijmo || {};

(function () {
wijmo.chart = wijmo.chart || {};

(function () {
/** @class wijchartnavigator
  * @widget 
  * @namespace jQuery.wijmo.chart
  * @extends wijmo.wijmoWidget
  */
wijmo.chart.wijchartnavigator = function () {};
wijmo.chart.wijchartnavigator.prototype = new wijmo.wijmoWidget();
/**  */
wijmo.chart.wijchartnavigator.prototype.destroy = function () {};

/** @class */
var wijchartnavigator_options = function () {};
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Set this option as a jquery selector to bind the target chart(s).</p>
  * @field 
  * @type {string}
  * @option
  */
wijchartnavigator_options.prototype.targetSelector = "";
/** <p>Sets the array to use as a source for data that you can bind to the axes in your seriesList.</p>
  * @field 
  * @option 
  * @remarks
  * Use the data option, and bind values to your X and Y axes.
  */
wijchartnavigator_options.prototype.dataSource = null;
/** <p>Bind a field to each series's data x array</p>
  * @field 
  * @option
  */
wijchartnavigator_options.prototype.data = null;
/** <p class='defaultValue'>Default value: []</p>
  * <p>Creates an array of series objects that contain data values and labels to display in the chart navigator.</p>
  * @field 
  * @type {array}
  * @option
  */
wijchartnavigator_options.prototype.seriesList = [];
/** <p>Sets an array of style objects to use in rendering each series in the chart navigator</p>
  * @field 
  * @type {array}
  * @option 
  * @remarks
  * Each style object in the array applies to one series in your seriesList,
  * so you need specify only as many style objects as you have series objects 
  * in your seriesList.
  */
wijchartnavigator_options.prototype.seriesStyles = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_axis.html'>wijmo.chart.chart_axis</a></p>
  * <p>An object contains all of the information to create the X axis of the chart navigator.</p>
  * @field 
  * @type {wijmo.chart.chart_axis}
  * @option
  */
wijchartnavigator_options.prototype.xAxis = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.navigatorIndicator.html'>wijmo.chart.navigatorIndicator</a></p>
  * <p>Set this option to customize the indicator tooltip.</p>
  * @field 
  * @type {wijmo.chart.navigatorIndicator}
  * @option
  */
wijchartnavigator_options.prototype.indicator = null;
/** <p>This option can be set to initialize the left value of navigator range.</p>
  * @field 
  * @type {number}
  * @option
  */
wijchartnavigator_options.prototype.rangeMin = null;
/** <p>This option can be set to initialize the right value of navigator range.</p>
  * @field 
  * @type {number}
  * @option
  */
wijchartnavigator_options.prototype.rangeMax = null;
/** <p>This option is to determine the size of each changing of navigator range.</p>
  * @field 
  * @type {number}
  * @option
  */
wijchartnavigator_options.prototype.step = null;
/** The updating event is raised when range of navigator is changing.
  * @event 
  * @param {JQueryEventObject} e The jQuery.Event object.
  * @param args The data with this event.
  */
wijchartnavigator_options.prototype.updating = null;
/** The updated event is raised when range of navigator has been changed.
  * @event 
  * @param {JQueryEventObject} e The jQuery.Event object.
  * @param args The data with this event.
  */
wijchartnavigator_options.prototype.updated = null;
wijmo.chart.wijchartnavigator.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijchartnavigator_options());
$.widget("wijmo.wijchartnavigator", $.wijmo.widget, wijmo.chart.wijchartnavigator.prototype);
/** @class wijchartnavigator_css
  * @namespace wijmo.chart
  * @extends wijmo.wijmo_css
  */
wijmo.chart.wijchartnavigator_css = function () {};
wijmo.chart.wijchartnavigator_css.prototype = new wijmo.wijmo_css();
/** @class wijchartnavigator_options
  * @namespace wijmo.chart
  */
wijmo.chart.wijchartnavigator_options = function () {};
})()
})()
