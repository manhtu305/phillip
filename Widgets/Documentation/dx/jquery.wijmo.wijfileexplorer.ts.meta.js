var wijmo = wijmo || {};

(function () {
wijmo.fileexplorer = wijmo.fileexplorer || {};

(function () {
/** @class wijfileexplorer
  * @widget 
  * @namespace jQuery.wijmo.fileexplorer
  * @extends wijmo.wijmoWidget
  */
wijmo.fileexplorer.wijfileexplorer = function () {};
wijmo.fileexplorer.wijfileexplorer.prototype = new wijmo.wijmoWidget();
/**  */
wijmo.fileexplorer.wijfileexplorer.prototype.destroy = function () {};
/** The refresh method refreshes the content within the wijfileexplorer.
  * @param {string} path - Optional. The path to refresh. The current folder is used if the path is not set.
  */
wijmo.fileexplorer.wijfileexplorer.prototype.refresh = function (path) {};

/** @class */
var wijfileexplorer_options = function () {};
/** <p>A value that determines the explorer mode of the wijfileexplorer widget.
  * Possible values are:
  * 'default' and 'fileTree'</p>
  * @field 
  * @type {string}
  * @option
  */
wijfileexplorer_options.prototype.mode = null;
/** <p>A string array that determines the patterns of files that are shown, usually the file extensions.</p>
  * @field 
  * @type {string[]}
  * @option
  */
wijfileexplorer_options.prototype.searchPatterns = null;
/** <p>A value that determines initial path to load data into the wijfileexplorer widget.</p>
  * @field 
  * @type {string}
  * @option
  */
wijfileexplorer_options.prototype.initPath = null;
/** <p>A string array that determines the folder paths to show in the wijfileexplorer widget.</p>
  * @field 
  * @type {string[]}
  * @option
  */
wijfileexplorer_options.prototype.viewPaths = null;
/** <p>A string value that determines the ViewMode of the wijfileexplorer widget. 
  * Possible values are:
  * 'detail' and 'thumbnail'</p>
  * @field 
  * @type {string}
  * @option
  */
wijfileexplorer_options.prototype.viewMode = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that determines whether to allow changing the extension of the file while renaming.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijfileexplorer_options.prototype.allowFileExtensionRename = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that determines whether to allow multiple items selection.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijfileexplorer_options.prototype.allowMultipleSelection = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that determines whether to use paging.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijfileexplorer_options.prototype.allowPaging = false;
/** <p class='defaultValue'>Default value: 10</p>
  * <p>A value that determines the number of items loaded per page when using paging.</p>
  * @field 
  * @type {number}
  * @option
  */
wijfileexplorer_options.prototype.pageSize = 10;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.fileexplorer.IShortcuts.html'>wijmo.fileexplorer.IShortcuts</a></p>
  * <p>A object that determines the shortcuts.</p>
  * @field 
  * @type {wijmo.fileexplorer.IShortcuts}
  * @option
  */
wijfileexplorer_options.prototype.shortcuts = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value that determines whether to allow opening a new window with the file.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijfileexplorer_options.prototype.enableOpenFile = true;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value that determines whether to allow creating new folders.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijfileexplorer_options.prototype.enableCreateNewFolder = true;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value that determines whether to allow copying of files/folders.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijfileexplorer_options.prototype.enableCopy = true;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that determines whether to perform the filtering after the 'Enter' key is pressed.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijfileexplorer_options.prototype.enableFilteringOnEnterPressed = false;
/** <p>A value that indicates the url of current folder.</p>
  * @field 
  * @type {string}
  * @option
  */
wijfileexplorer_options.prototype.currentFolder = null;
/** <p>A value that determines which components will be shown in wijfileexplorer.</p>
  * @field 
  * @type {string}
  * @option
  */
wijfileexplorer_options.prototype.visibleControls = null;
/** <p class='defaultValue'>Default value: 200</p>
  * <p>A value that determines the width of the TreeView.</p>
  * @field 
  * @type {number}
  * @option
  */
wijfileexplorer_options.prototype.treePanelWidth = 200;
/** <p>A value that determines the host uri of current web application.</p>
  * @field 
  * @type {string}
  * @option
  */
wijfileexplorer_options.prototype.hostUri = null;
/** <p>A value that determines the uri of action which executes the file/folder operations.</p>
  * @field 
  * @type {string}
  * @option
  */
wijfileexplorer_options.prototype.actionUri = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that determines whether or not to disable the wijfileexplorer widget.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijfileexplorer_options.prototype.disabled = false;
/** The itemSelected event handler.
  * A function called after the item is selected.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.itemSelected = null;
/** The fileOpened event handler.
  * A function called after the file is opened.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.fileOpened = null;
/** The fileOpening event handler.
  * A function called before the file is opened.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.fileOpening = null;
/** The itemCopying event handler.
  * A function called before the item is copied.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.itemCopying = null;
/** The itemCopied event handler.
  * A function called after the item is copied.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.itemCopied = null;
/** The itemRenaming event handler.
  * A function called before the item is renamed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.itemRenaming = null;
/** The itemRenamed event handler.
  * A function called after the item is renamed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.itemRenamed = null;
/** The itemDeleting event handler.
  * A function called before the item is deleted.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.itemDeleting = null;
/** The itemDeleted event handler.
  * A function called after the item is deleted.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.itemDeleted = null;
/** The errorOccurred event handler.
  * A function called when an error occurs.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.errorOccurred = null;
/** The itemPasting event handler.
  * A function called before the item is pasted.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.itemPasting = null;
/** The itemPasted event handler.
  * A function called after the item is pasted.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.itemPasted = null;
/** The itemMoving event handler.
  * A function called before the item is moved.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.itemMoving = null;
/** The itemMoved event handler.
  * A function called after the item is moved.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.itemMoved = null;
/** The newFolderCreating event handler.
  * A function called before a new folder is created.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.newFolderCreating = null;
/** The newFolderCreated event handler.
  * A function called after a new folder is created.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.newFolderCreated = null;
/** The folderChanged event handler.
  * A function called after current folder is changed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.folderChanged = null;
/** The folderLoaded event handler.
  * A function called after the current folder is loaded.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.folderLoaded = null;
/** The filtering event handler.
  * A function called before filtering.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.filtering = null;
/** The filtered event handler.
  * A function called after filtering.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijfileexplorer_options.prototype.filtered = null;
wijmo.fileexplorer.wijfileexplorer.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijfileexplorer_options());
$.widget("wijmo.wijfileexplorer", $.wijmo.widget, wijmo.fileexplorer.wijfileexplorer.prototype);
/** Specifies the data that the wijfileexplorer sends to the handler which specified by actionUri option.
  * @interface IFileExplorerRequest
  * @namespace wijmo.fileexplorer
  */
wijmo.fileexplorer.IFileExplorerRequest = function () {};
/** <p>The request operation. The value is one of the following:
  * &lt;ul&gt;
  * &lt;li&gt;Paste - Paste a file or folder to a location. &lt;/li&gt;
  * &lt;li&gt;GetItems - Get sub folders and files contained in. &lt;/li&gt;
  * &lt;li&gt;CreateDirectory - Create a new directory. &lt;/li&gt;
  * &lt;li&gt;Delete - Delete files or folders. &lt;/li&gt;
  * &lt;li&gt;Rename - Rename a file or directory. &lt;/li&gt;
  * &lt;li&gt;Move - Move files or folders to a new folder*
  * &lt;li&gt;GetHostUri - Get the uri of the current web application.
  * &lt;/ul&gt;</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IFileExplorerRequest.prototype.commandName = null;
/** <p>The target path to perform the operation. 
  * It has different meaning for different command.
  * &lt;ul&gt;
  * &lt;li&gt; GetItems - Get the sub folders and files of the specified path.&lt;/li&gt;
  * &lt;li&gt; Paste - Copy the specified paths defined in sourcePaths to the path.&lt;/li&gt;
  * &lt;li&gt; CreateDirectory - The path of the new directory to create.&lt;/li&gt;
  * &lt;li&gt; Rename - Rename the specified path defined in sourcePaths to the new path defined in path.&lt;/li&gt;
  * &lt;li&gt; Move - Move the specified paths defined in sourcePaths to the path.&lt;/li&gt;
  * &lt;li&gt; Delete - Invalid.&lt;/li&gt;
  * &lt;li&gt; GetHostUri - Invalid. &lt;/li&gt;
  * &lt;/ul&gt;</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IFileExplorerRequest.prototype.path = null;
/** <p>The source paths used for delete, move or paste command.
  * &lt;ul&gt;
  * &lt;li&gt; Delete - Delete the specified paths.&lt;/li&gt;
  * &lt;li&gt; Paste - Copy the specified paths defined in sourcePaths to the path.&lt;/li&gt;
  * &lt;li&gt; Move - Move the specified paths defined in sourcePaths to the path.&lt;/li&gt;
  * &lt;/ul&gt;</p>
  * @field 
  * @type {string[]}
  */
wijmo.fileexplorer.IFileExplorerRequest.prototype.sourcePaths = null;
/** <p>The patterns of the file to search. Only used for GetItems command.</p>
  * @field 
  * @type {string[]}
  */
wijmo.fileexplorer.IFileExplorerRequest.prototype.searchPatterns = null;
/** <p>The expression of filter while getting items. Only used for GetItems command.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IFileExplorerRequest.prototype.filterExpression = null;
/** <p>The expression of sort while getting items.  Only used for GetItems command.
  * The value is one of the following:
  * &lt;ul&gt;
  * &lt;li&gt;name&lt;/li&gt;
  * &lt;li&gt;size&lt;/li&gt;
  * &lt;/ul&gt;</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IFileExplorerRequest.prototype.sortExpression = null;
/** <p>The direction of sort while getting items. Only used for GetItems command.
  * The value is one of the following:
  * &lt;ul&gt;
  * &lt;li&gt;ascending&lt;/li&gt;
  * &lt;li&gt;descending&lt;/li&gt;
  * &lt;/ul&gt;</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IFileExplorerRequest.prototype.sortDirection = null;
/** <p>The items count of one page if allowPaging option is set. Only used for GetItems command.</p>
  * @field 
  * @type {number}
  */
wijmo.fileexplorer.IFileExplorerRequest.prototype.pageSize = null;
/** <p>The index of the page to get if allowPaging option is set. Only used for GetItems command.</p>
  * @field 
  * @type {number}
  */
wijmo.fileexplorer.IFileExplorerRequest.prototype.pageIndex = null;
/** <p>Gets and sets whether only get the folders. Only used for GetItems command.</p>
  * @field 
  * @type {boolean}
  */
wijmo.fileexplorer.IFileExplorerRequest.prototype.onlyFolder = null;
/** The response data received from the server side specified by actionUri option.
  * @interface IFileExplorerResponse
  * @namespace wijmo.fileexplorer
  */
wijmo.fileexplorer.IFileExplorerResponse = function () {};
/** <p>Specifies whether the action succeed.</p>
  * @field 
  * @type {boolean}
  */
wijmo.fileexplorer.IFileExplorerResponse.prototype.success = null;
/** <p>The index of the page for the current action, if allowPaging option is set.
  * Only for GetItems command.</p>
  * @field 
  * @type {number}
  */
wijmo.fileexplorer.IFileExplorerResponse.prototype.pageIndex = null;
/** <p>The count of the pages for current action, if allowPaging option is set.
  * Only for GetItems command.</p>
  * @field 
  * @type {number}
  */
wijmo.fileexplorer.IFileExplorerResponse.prototype.pageCount = null;
/** <p>The error message if the action failed.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IFileExplorerResponse.prototype.error = null;
/** <p>The uri of the current web applicaiton. 
  * Only for GetHostUri command.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IFileExplorerResponse.prototype.hostUri = null;
/** <p>The collection of item operation results.
  * It has different meaning for different command.
  * For GetItems command, it is an IFileExplorerItem collection which contains sub folders and files.
  * It is an IItemOperationResult collection which contains the success or error informations when call
  * following commands:
  * &lt;ul&gt;
  * &lt;li&gt; Paste&lt;/li&gt;
  * &lt;li&gt; Delete&lt;/li&gt;
  * &lt;li&gt; Move&lt;/li&gt;
  * &lt;/ul&gt;</p>
  * @field 
  * @type {array}
  */
wijmo.fileexplorer.IFileExplorerResponse.prototype.itemOperationResults = null;
/** The result of operating FileExplorerItem while call GetItems, Paste, Delete or Move command.
  * @interface IItemOperationResult
  * @namespace wijmo.fileexplorer
  */
wijmo.fileexplorer.IItemOperationResult = function () {};
/** <p>The path of the item.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IItemOperationResult.prototype.path = null;
/** <p>Whether the operation on the working item succeed.</p>
  * @field 
  * @type {boolean}
  */
wijmo.fileexplorer.IItemOperationResult.prototype.success = null;
/** <p>The error message if the operation on the working item failed.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IItemOperationResult.prototype.error = null;
/** Keyboard shortcuts for wijfileexplorer.
  * @interface IShortcuts
  * @namespace wijmo.fileexplorer
  */
wijmo.fileexplorer.IShortcuts = function () {};
/** <p>Get or set the shortcuts for focusing the FileExplorer.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IShortcuts.prototype.focusFileExplorer = null;
/** <p>Get or set the shortcuts for focusing the TreeView.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IShortcuts.prototype.focusTreeView = null;
/** <p>Get or set the shortcuts for focusing the ToolBar.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IShortcuts.prototype.focusToolBar = null;
/** <p>Get or set the shortcuts for focusing the Grid.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IShortcuts.prototype.focusGrid = null;
/** <p>Get or set the shortcuts for focusing the AddressBar.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IShortcuts.prototype.focusAddressBar = null;
/** <p>Get or set the shortcuts for closing the popup window.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IShortcuts.prototype.popupWindowClose = null;
/** <p>Get or set the shortcuts for focusing the view's pager.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IShortcuts.prototype.focusPager = null;
/** <p>Get or set the shortcuts for opening the ContextMenu.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IShortcuts.prototype.contextMenu = null;
/** <p>Get or set the shortcuts for navigating back.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IShortcuts.prototype.back = null;
/** <p>Get or set the shortcuts for navigating forward.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IShortcuts.prototype.forward = null;
/** <p>Get or set the shortcuts for opening a file or folder.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IShortcuts.prototype.open = null;
/** <p>Get or set the shortcuts for refreshing the control.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IShortcuts.prototype.refresh = null;
/** <p>Get or set the shortcuts for creating a new folder.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IShortcuts.prototype.newFolder = null;
/** <p>Get or set the shortcuts for deleting files or folders.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IShortcuts.prototype["delete"] = null;
/** Describle the sub folder or file of the folder.
  * @interface IFileExplorerItem
  * @namespace wijmo.fileexplorer
  */
wijmo.fileexplorer.IFileExplorerItem = function () {};
/** <p>The path of the item.</p>
  * @field 
  * @type {string}
  */
wijmo.fileexplorer.IFileExplorerItem.prototype.path = null;
/** <p>true, if the item is a folder; otherwise, false.</p>
  * @field 
  * @type {boolean}
  */
wijmo.fileexplorer.IFileExplorerItem.prototype.isFolder = null;
/** <p>true, if the item is a folder and has sub folders; otherwise, false or undefined.</p>
  * @field 
  * @type {boolean}
  */
wijmo.fileexplorer.IFileExplorerItem.prototype.hasSubFolders = null;
/** <p>The size of the file, in bytes. Undefined for sub folder.</p>
  * @field 
  * @type {number}
  */
wijmo.fileexplorer.IFileExplorerItem.prototype.size = null;
/** <p>true, if the item is a folder and has files; otherwise, false or undefined.</p>
  * @field 
  * @type {boolean}
  */
wijmo.fileexplorer.IFileExplorerItem.prototype.hasChildren = null;
/** @class wijfileexplorer_options
  * @namespace wijmo.fileexplorer
  */
wijmo.fileexplorer.wijfileexplorer_options = function () {};
})()
})()
