var wijmo = wijmo || {};

(function () {
wijmo.input = wijmo.input || {};

(function () {
/** @enum FocusType
  * @namespace wijmo.input
  */
wijmo.input.FocusType = {
/**  */
None: 0,
/**  */
Click: 1,
/**  */
ContextMenu: 2,
/**  */
ClientEvent: 3,
/**  */
KeyExit: 4,
/**  */
Default: 5,
/**  */
SpinButton: 6,
/**  */
DropDown: 7,
/**  */
ImeInput: 8,
/**  */
Left: 9,
/**  */
Right: 10,
/**  */
DragDrop: 11 
};
/** Defines the CrLf mode which describes how to process the CrLf char.
  * @enum CrLfMode
  * @namespace wijmo.input
  */
wijmo.input.CrLfMode = {
/** Accepts all CrLf characters in copied, cut, or pasted strings. */
NoControl: 0,
/** Removes all CrLf characters in copied, cut, or pasted strings. */
Filter: 1,
/** Cuts the following strings from the first CrLf character in copied, cut, and pasted strings. */
Cut: 2 
};
/** Specifies how the literal in content is held in the clipboard.
  * @enum ClipContent
  * @namespace wijmo.input
  */
wijmo.input.ClipContent = {
/** Literals are included. */
IncludeLiterals: 0,
/** Literals are excluded. */
ExcludeLiterals: 1 
};
/** @enum EditMode
  * @namespace wijmo.input
  */
wijmo.input.EditMode = {
/**  */
Insert: 0,
/**  */
Overwrite: 1,
/**  */
FixedInsert: 2,
/**  */
FixedOverwrite: 3 
};
/** @enum ShowLiterals
  * @namespace wijmo.input
  */
wijmo.input.ShowLiterals = {
/**  */
Always: 0,
/**  */
PostDisplay: 1,
/**  */
PreDisplay: 2 
};
/** @enum ExitOnLeftRightKey
  * @namespace wijmo.input
  */
wijmo.input.ExitOnLeftRightKey = {
/**  */
None: 0,
/**  */
Left: 1,
/**  */
Right: 2,
/**  */
Both: 3 
};
/** Specifies the type of selection text in control.
  * @enum HighlightText
  * @namespace wijmo.input
  */
wijmo.input.HighlightText = {
/** No selection specified. */
None: 0,
/** Select the specified field. */
Field: 1,
/** Select all the text. */
All: 2 
};
/** @enum DropDownAlign
  * @namespace wijmo.input
  */
wijmo.input.DropDownAlign = {
/**  */
Left: 0,
/**  */
Right: 1 
};
/** @enum ScrollBarMode
  * @namespace wijmo.input
  */
wijmo.input.ScrollBarMode = {
/**  */
Automatic: 0,
/**  */
Fixed: 1 
};
/** @enum ScrollBars
  * @namespace wijmo.input
  */
wijmo.input.ScrollBars = {
/**  */
None: 0,
/**  */
Horizontal: 1,
/**  */
Vertical: 2,
/**  */
Both: 3 
};
/** @enum ControlStatus
  * @namespace wijmo.input
  */
wijmo.input.ControlStatus = {
/**  */
Normal: 0,
/**  */
Hover: 1,
/**  */
Pressed: 2,
/**  */
Focused: 3,
/**  */
Disabled: 4 
};
/** @enum ExitKeys
  * @namespace wijmo.input
  */
wijmo.input.ExitKeys = {
/**  */
Tab: 0,
/**  */
ShiftTab: 1,
/**  */
NextControl: 2,
/**  */
PreviousControl: 3,
/**  */
Right: 4,
/**  */
Left: 5,
/**  */
CtrlRight: 6,
/**  */
CtrlLeft: 7,
/**  */
CharInput: 8 
};
/** @enum TabAction
  * @namespace wijmo.input
  */
wijmo.input.TabAction = {
/**  */
Control: 0,
/**  */
Field: 1 
};
/** @enum Key
  * @namespace wijmo.input
  */
wijmo.input.Key = {
/**  */
BackSpace: 0,
/**  */
Tab: 1,
/**  */
Clear: 2,
/**  */
Enter: 3,
/**  */
Shift: 4,
/**  */
Control: 5,
/**  */
Alt: 6,
/**  */
Pause: 7,
/**  */
Caps_Lock: 8,
/**  */
Escape: 9,
/**  */
Space: 10,
/**  */
PageUp: 11,
/**  */
PageDown: 12,
/**  */
End: 13,
/**  */
Home: 14,
/**  */
Left: 15,
/**  */
Up: 16,
/**  */
Right: 17,
/**  */
Down: 18,
/**  */
Select: 19,
/**  */
Print: 20,
/**  */
Execute: 21,
/**  */
Insert: 22,
/**  */
Delete: 23,
/**  */
Help: 24,
/**  */
equalbraceright: 25,
/**  */
exclamonesuperior: 26,
/**  */
quotedbltwosuperior: 27,
/**  */
sectionthreesuperior: 28,
/**  */
dollar: 29,
/**  */
percent: 30,
/**  */
ampersand: 31,
/**  */
slashbraceleft: 32,
/**  */
parenleftbracketleft: 33,
/**  */
parenrightbracketright: 34,
/**  */
A: 35,
/**  */
B: 36,
/**  */
C: 37,
/**  */
D: 38,
/**  */
E: 39,
/**  */
F: 40,
/**  */
G: 41,
/**  */
H: 42,
/**  */
I: 43,
/**  */
J: 44,
/**  */
K: 45,
/**  */
L: 46,
/**  */
M: 47,
/**  */
N: 48,
/**  */
O: 49,
/**  */
P: 50,
/**  */
Q: 51,
/**  */
R: 52,
/**  */
S: 53,
/**  */
T: 54,
/**  */
U: 55,
/**  */
V: 56,
/**  */
W: 57,
/**  */
X: 58,
/**  */
Y: 59,
/**  */
Z: 60,
/**  */
KP_0: 61,
/**  */
KP_1: 62,
/**  */
KP_2: 63,
/**  */
KP_3: 64,
/**  */
KP_4: 65,
/**  */
KP_5: 66,
/**  */
KP_6: 67,
/**  */
KP_7: 68,
/**  */
KP_8: 69,
/**  */
KP_9: 70,
/**  */
KP_Multiply: 71,
/**  */
KP_Add: 72,
/**  */
KP_Separator: 73,
/**  */
KP_Subtract: 74,
/**  */
KP_Decimal: 75,
/**  */
KP_Divide: 76,
/**  */
F1: 77,
/**  */
F2: 78,
/**  */
F3: 79,
/**  */
F4: 80,
/**  */
F5: 81,
/**  */
F6: 82,
/**  */
F7: 83,
/**  */
F8: 84,
/**  */
F9: 85,
/**  */
F10: 86,
/**  */
F11: 87,
/**  */
F12: 88,
/**  */
F13: 89,
/**  */
F14: 90,
/**  */
F15: 91,
/**  */
F16: 92,
/**  */
F17: 93,
/**  */
F18: 94,
/**  */
F19: 95,
/**  */
F20: 96,
/**  */
F21: 97,
/**  */
F22: 98,
/**  */
F23: 99,
/**  */
F24: 100,
/**  */
Num_Lock: 101,
/**  */
Scroll_Lock: 102 
};
})()
})()
