$(function () {

    if (isTocNavigationEnabled) {

        var arrowContainer = $('<div class="arrow-container"></div>');
        arrowContainer.append('<div class="arrow arrow-right" id="Next"></div>');
        arrowContainer.append('<div class="arrow arrow-left" id="Previous"></div>');
        
        $("#TableOfContents").append(arrowContainer);

        $("#TableOfContents #Next").on("click", function (e) {

            if (!$(this).hasClass("arrow-right-disabled")) {
                navigate("next");
            }
            else {
                e.preventDefault();
                return false;
            }

        });

        $("#TableOfContents #Previous").on("click", function (e) {

            if (!$(this).hasClass("arrow-left-disabled")) {
                navigate("previous");
            }
            else {
                e.preventDefault();
                return false;
            }

        });
    }
    
});

function navigate(messageData) {

    var toc = document.getElementById("webtoc").contentWindow;
    toc.postMessage("navigate|" + messageData, "*");

}

if (isPostMessageEnabled()) {
    addMessageListener(desktopNavigationMessageHandler);
}

function desktopNavigationMessageHandler(event) {
    var message = getMessage(event.data);

    switch (message.messageType) {
        case "update-navigation-buttons":
            var toc = document.getElementById("webtoc").contentWindow;
            toc.postMessage("update-navigation-buttons", "*");
            break;

        case "toggle-toc-previous":
            $("#TableOfContents #Previous").toggleClass("arrow-left-disabled", message.messageData == "true");
            break;

        case "toggle-toc-next":
            $("#TableOfContents #Next").toggleClass("arrow-right-disabled", message.messageData == "true");
            break;
    }
}