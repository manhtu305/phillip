﻿<?xml version="1.0" encoding="utf-8"?>
<Topic Id="9ba5ebdb-9751-4367-bd5e-945936b6a9fb" Status="New Topic" CreatedOn="2014-03-24T21:11:52.3948207Z" ModifiedOn="2015-05-06T07:02:44.5419283Z" PageTypeName="" AutoIndex="true" HelpContextIds="" Name="CandleFormatting" BuildFlags="">
  <Title />
  <ContentsTitle />
  <Notes></Notes>
  <TopicSections>
    <TopicSection Name="BodyText">
      <Content m="2014-03-27T19:51:13.9503887Z">&lt;P&gt;There are a&amp;nbsp;couple of ways to format the candles in a CandlestickChart widget&amp;nbsp;using&amp;nbsp;&lt;A href="3424dd2f-6bc9-4a79-b60a-c031082591c2"&gt;style options&lt;/A&gt;. There are two parts of the API that give you access to the format of the candles: the&amp;nbsp;&lt;A href="Wijmo~wijmo.chart.wijcandlestickchart.options~seriesStyles.html"&gt;seriesStyles&lt;/A&gt; and&amp;nbsp;&lt;A href="Wijmo~wijmo.chart.wijcandlestickchart.options~candlestickFormatter.html"&gt;candlestickFormatter&lt;/A&gt;&amp;nbsp;options. Additionally, you can change the format when the user hovers the cursor over a candle using the&amp;nbsp;&lt;A href="Wijmo~wijmo.chart.wijcandlestickchart.options~seriesHoverStyles.html"&gt;seriesHoverStyles&lt;/A&gt;&amp;nbsp;option. These options give you access to each part of the candle.&lt;/P&gt;
&lt;P&gt;A good use of candle formatting is to create a condition based on whether the stock price for each candle is rising or falling, and set the color of the body of the candle accordingly. You can see this in action in the How To&amp;nbsp;&lt;A href="e35a8e62-c107-4253-963d-9d4a79e832fa" style="auto-update-caption: true"&gt;Conditionally Change Formats&lt;/A&gt;&amp;nbsp;topic.&lt;/P&gt;
&lt;H2&gt;candlestickFormatter&lt;/H2&gt;
&lt;P&gt;A data point in each type of CandlestickChart is composed of a different set of elements. The elements are represented by obj.eles, and the data for the elements&amp;nbsp;are represented by obj.data. Here is a table of the types and the elements that make them up.&lt;/P&gt;
&lt;TABLE&gt;
&lt;TBODY&gt;
&lt;TR&gt;
&lt;TH&gt;Type&lt;/TH&gt;
&lt;TH&gt;Elements (obj.eles)&lt;/TH&gt;
&lt;TH&gt;Data (obj.data)&lt;/TH&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD&gt;candlestick&lt;/TD&gt;
&lt;TD&gt;&lt;STRONG&gt;high &lt;/STRONG&gt;The top of the wick line of the candle.&lt;BR&gt;&lt;STRONG&gt;low&lt;/STRONG&gt; The bottom of the wick line or tail&amp;nbsp;of the candle.&lt;BR&gt;&lt;STRONG&gt;openClose&lt;/STRONG&gt; The top to bottom of the body of the candle.&lt;/TD&gt;
&lt;TD&gt;high&lt;BR&gt;low&lt;BR&gt;open&lt;BR&gt;close&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD&gt;ohlc&lt;/TD&gt;
&lt;TD&gt;&lt;STRONG&gt;open&lt;/STRONG&gt; The horizontal line to the left of the body of the candle.&lt;BR&gt;&lt;STRONG&gt;close&lt;/STRONG&gt; The horizontal line to the right of the body of the candle.&lt;BR&gt;&lt;STRONG&gt;highLow&lt;/STRONG&gt;&amp;nbsp;The top to bottom of the body of the candle.&lt;/TD&gt;
&lt;TD&gt;high&lt;BR&gt;low&lt;BR&gt;open&lt;BR&gt;close&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD&gt;hl&lt;/TD&gt;
&lt;TD&gt;&lt;STRONG&gt;highLow&lt;/STRONG&gt;&amp;nbsp;The top to bottom of the body of the candle.&lt;/TD&gt;
&lt;TD&gt;high&lt;BR&gt;low&lt;/TD&gt;&lt;/TR&gt;&lt;/TBODY&gt;&lt;/TABLE&gt;
&lt;P&gt;Here are code examples with each type of chart.&lt;/P&gt;&lt;?xml:namespace prefix = "innovasys" ns = "http://www.innovasys.com/widgets" /&gt;&lt;innovasys:widget type="Drop down section" layout="block"&gt;&lt;innovasys:widgetproperty layout="inline" name="Heading"&gt;Example&amp;nbsp;code for candlestick type&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;innovasys:widget type="Colorized Example Code" layout="block"&gt;&lt;innovasys:widgetproperty layout="inline" name="Title"&gt;Use in the wijcandlestickchart script inside the document ready function&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="inline" name="LanguageName"&gt;JavaScript&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;PRE&gt;candlestickFormatter: function (obj) {
    var eles = obj.eles,
        data = obj.data,
        open = data.open,
        high = data.high,
        low = data.low,
        close = data.close,
        highEle = eles.high,
        lowEle = eles.low,
        openCloseEle = eles.openClose,
        style = {};

    if (open &amp;gt; close) {
        style.fill = "Red";
        style.stroke = style.fill;
    }
    else {
        style.fill = "Green";
        style.stroke = style.fill;
    }
    openCloseEle.attr(style);
}
&lt;/PRE&gt;&lt;/innovasys:widgetproperty&gt;&lt;/innovasys:widget&gt;&lt;/innovasys:widgetproperty&gt;&lt;/innovasys:widget&gt;&lt;innovasys:widget type="Drop down section" layout="block"&gt;&lt;innovasys:widgetproperty layout="inline" name="Heading"&gt;Example code for ohlc type&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;innovasys:widget type="Colorized Example Code" layout="block"&gt;&lt;innovasys:widgetproperty layout="inline" name="Title"&gt;Use in the wijcandlestickchart script inside the document ready function&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="inline" name="LanguageName"&gt;JavaScript&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;PRE&gt;candlestickFormatter: function (obj) {
    var eles = obj.eles,
        data = obj.data,
        open = data.open,
        close = data.close,
        hlEl = eles.highLow,
        oEl = eles.open,
        cEl = eles.close,
        style = {};

    if (open &amp;gt; close) {
        style.stroke = "rgb(96,189,104)";
    }
    else {
        style.stroke = "rgb(241,88,84)";
    }
    hlEl.attr(style);
    oEl.attr(style);
    cEl.attr(style);
}
&lt;/PRE&gt;&lt;/innovasys:widgetproperty&gt;&lt;/innovasys:widget&gt;&lt;/innovasys:widgetproperty&gt;&lt;/innovasys:widget&gt;&lt;innovasys:widget type="Drop down section" layout="block"&gt;&lt;innovasys:widgetproperty layout="inline" name="Heading"&gt;Example code for hl type&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;innovasys:widget type="Colorized Example Code" layout="block"&gt;&lt;innovasys:widgetproperty layout="inline" name="Title"&gt;Use in the wijcandlestickchart script inside the document ready function&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="inline" name="LanguageName"&gt;JavaScript&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;PRE&gt;candlestickFormatter: function (obj) {
    var eles = obj.eles,
        data = obj.data,
        high = data.high,
        low = data.low,
        hlEle = eles.highLow,
        style = {};

    if (high - low &amp;gt; 5) {
        style.stroke = "rgb(96,189,104)";
    }
    else {
        style.stroke = "rgb(241,88,84)";
    }
    hlEle.attr(style);
}
&lt;/PRE&gt;&lt;/innovasys:widgetproperty&gt;&lt;/innovasys:widget&gt;&lt;/innovasys:widgetproperty&gt;&lt;/innovasys:widget&gt;
&lt;H2&gt;seriesStyles and seriesHoverStyles&lt;/H2&gt;
&lt;P&gt;For each series in the chart, you can use a different set of seriesStyles and seriesHoverStyles. Here are the available styles for each chart type.&lt;/P&gt;
&lt;TABLE&gt;
&lt;TBODY&gt;
&lt;TR&gt;
&lt;TH&gt;Type&lt;/TH&gt;
&lt;TH&gt;Style Names&lt;/TH&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD&gt;candlestick&lt;/TD&gt;
&lt;TD&gt;&lt;STRONG&gt;highLow &lt;/STRONG&gt;The style to use on the top to bottom wick line, or wick and tail,&amp;nbsp;of the candle.&lt;BR&gt;&lt;STRONG&gt;fallingClose &lt;/STRONG&gt;The style to use for the body of the candle when the closing value is lower than the opening value.&lt;BR&gt;&lt;STRONG&gt;risingClose &lt;/STRONG&gt;The style to use for the body of the candle when the closing value is higher than the opening value.&lt;BR&gt;&lt;STRONG&gt;unchangeClose &lt;/STRONG&gt;The style to use for the body of the candle when the closing value is the same as the opening value.&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD&gt;ohlc&lt;/TD&gt;
&lt;TD&gt;&lt;STRONG&gt;open &lt;/STRONG&gt;The style to use for the horizontal line to the left of the body of the candle.&lt;BR&gt;&lt;STRONG&gt;close &lt;/STRONG&gt;The style to use for the horizontal line to the right of the body of the candle.&lt;BR&gt;&lt;STRONG&gt;highLow &lt;/STRONG&gt;The style to use for the body of the candle.&lt;/TD&gt;&lt;/TR&gt;
&lt;TR&gt;
&lt;TD&gt;hl&lt;/TD&gt;
&lt;TD&gt;&lt;STRONG&gt;highLow &lt;/STRONG&gt;The style to use for the body of the candle.&lt;/TD&gt;&lt;/TR&gt;&lt;/TBODY&gt;&lt;/TABLE&gt;
&lt;P&gt;Here are code examples with each type of chart.&lt;/P&gt;&lt;innovasys:widget type="Drop down section" layout="block"&gt;&lt;innovasys:widgetproperty layout="inline" name="Heading"&gt;Example&amp;nbsp;code for candlestick type&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;innovasys:widget type="Colorized Example Code" layout="block"&gt;&lt;innovasys:widgetproperty layout="inline" name="Title"&gt;Use in the wijcandlestickchart script inside the document ready function&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="inline" name="LanguageName"&gt;JavaScript&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;PRE&gt;seriesStyles: [{
    highLow: {fill: "Gray"},
    fallingClose: {fill: "Red"},
    risingClose: {fill: "Green"},
    unchangeClose: {fill: "Yellow"}
}]
&lt;/PRE&gt;&lt;/innovasys:widgetproperty&gt;&lt;/innovasys:widget&gt;&lt;/innovasys:widgetproperty&gt;&lt;/innovasys:widget&gt;&lt;innovasys:widget type="Drop down section" layout="block"&gt;&lt;innovasys:widgetproperty layout="inline" name="Heading"&gt;Example code for ohlc type&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;innovasys:widget type="Colorized Example Code" layout="block"&gt;&lt;innovasys:widgetproperty layout="inline" name="Title"&gt;Use in the wijcandlestickchart script inside the document ready function&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="inline" name="LanguageName"&gt;JavaScript&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;PRE&gt;seriesStyles: [{
    open: {},
    close: {},
    highLow: {fill: "#88bde6"}
}]
&lt;/PRE&gt;&lt;/innovasys:widgetproperty&gt;&lt;/innovasys:widget&gt;&lt;/innovasys:widgetproperty&gt;&lt;/innovasys:widget&gt;&lt;innovasys:widget type="Drop down section" layout="block"&gt;&lt;innovasys:widgetproperty layout="inline" name="Heading"&gt;Example code for hl type&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;innovasys:widget type="Colorized Example Code" layout="block"&gt;&lt;innovasys:widgetproperty layout="inline" name="Title"&gt;Use in the wijcandlestickchart script inside the document ready function&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="inline" name="LanguageName"&gt;JavaScript&lt;/innovasys:widgetproperty&gt; &lt;innovasys:widgetproperty layout="block" name="Content"&gt;&lt;PRE&gt;seriesStyles: [{
    highLow: {fill: "#88bde6"}
}]
&lt;/PRE&gt;&lt;/innovasys:widgetproperty&gt;&lt;/innovasys:widget&gt;&lt;/innovasys:widgetproperty&gt;&lt;/innovasys:widget&gt;</Content>
    </TopicSection>
  </TopicSections>
  <TopicLinks>
    <TopicLink Id="6e34a9ae-2d54-4f71-bb0e-accd6863274d" CategoryId="" TopicLinkCategorySource="TargetItemCategory" Target="2d62b158-0849-4957-8359-ce27724bd46f" BuildFlags="">
      <Title />
      <Category />
    </TopicLink>
    <TopicLink Id="302eacda-39ad-4d54-b15f-aa654662117a" CategoryId="" TopicLinkCategorySource="TargetItemCategory" Target="922c004a-346e-49eb-a79f-b894eb774c41" BuildFlags="">
      <Title />
      <Category />
    </TopicLink>
    <TopicLink Id="62ae6f25-b112-4d4b-bc64-fe680743a977" CategoryId="" TopicLinkCategorySource="TargetItemCategory" Target="5a1032ab-2aa2-4ccc-9a43-823b591a87c7" BuildFlags="">
      <Title />
      <Category />
    </TopicLink>
    <TopicLink Id="8b0bf0d0-5789-48cb-bc5e-644f7abb1c7e" CategoryId="" TopicLinkCategorySource="TargetItemCategory" Target="3424dd2f-6bc9-4a79-b60a-c031082591c2" BuildFlags="">
      <Title />
      <Category />
    </TopicLink>
    <TopicLink Id="bf637472-9aa4-4fbf-a303-0727d50bc3a1" CategoryId="" TopicLinkCategorySource="TargetItemCategory" Target="e35a8e62-c107-4253-963d-9d4a79e832fa" BuildFlags="">
      <Title />
      <Category />
    </TopicLink>
    <TopicLink Id="b6023ef1-c9c8-43bb-9eac-c7fa594d85b7" CategoryId="" TopicLinkCategorySource="TargetItemCategory" Target="3b09f24a-46f1-4ff6-93f4-3a9f99e8567f" BuildFlags="">
      <Title />
      <Category />
    </TopicLink>
    <TopicLink Id="565fda50-6174-4281-be04-fd4b5664eb7d" CategoryId="" TopicLinkCategorySource="TargetItemCategory" Target="03bcce6f-5482-4ce8-aa08-ede0c959f560" BuildFlags="">
      <Title />
      <Category />
    </TopicLink>
    <TopicLink Id="595817e5-74f3-42f3-9868-e23e84fece51" CategoryId="" TopicLinkCategorySource="TargetItemCategory" Target="Wijmo~jQuery.fn.-~wijcandlestickchart" BuildFlags="">
      <Title m="2014-03-27T19:52:45.6260441Z">wijcandlestickchart API</Title>
      <Category />
    </TopicLink>
    <TopicLink Id="3dad73d6-7ba8-454b-87d3-89b0ce963bf1" CategoryId="" TopicLinkCategorySource="TargetItemCategory" Target="Wijmo~wijmo.chart.wijcandlestickchart.options~candlestickFormatter" BuildFlags="">
      <Title m="2014-03-27T19:53:12.2189323Z">candlestickFormatter Option</Title>
      <Category />
    </TopicLink>
    <TopicLink Id="2ff9ae42-9f92-4dd9-8713-b908a774405c" CategoryId="" TopicLinkCategorySource="TargetItemCategory" Target="Wijmo~wijmo.chart.wijcandlestickchart.options~seriesHoverStyles" BuildFlags="">
      <Title m="2014-03-27T19:53:30.5233525Z">seriesHoverStyles Option</Title>
      <Category />
    </TopicLink>
    <TopicLink Id="41532c0e-0978-4454-a1fe-87776c177296" CategoryId="" TopicLinkCategorySource="TargetItemCategory" Target="Wijmo~wijmo.chart.wijcandlestickchart.options~seriesStyles" BuildFlags="">
      <Title m="2014-03-27T19:53:41.823154Z">seriesStyles Option</Title>
      <Category />
    </TopicLink>
  </TopicLinks>
  <TopicKeywords />
  <PropertyDefinitionValues />
  <ExcludedOtherFiles />
</Topic>