﻿if (isPostMessageEnabled()) {
    addMessageListener(navigationMessageHandler);
}

function navigationMessageHandler(event) {
    var message = getMessage(event.data);

    switch (message.messageType) {

        case "navigate":
            if (message.messageData) {
                var anchor = null;

                if (message.messageData == "next") {
                    anchor = moveToNextNode(false);
                }
                else if (message.messageData == "previous") {
                    anchor = moveToPreviousNode(false);
                }

                if (anchor != null && anchor.length) {

                    _ignoreSyncRequest = false;
                    syncToCNode(anchor);

                    // Ignore the next ToC node sync request as it will come from the webframe when the content has finished loading
                    _ignoreSyncRequest = true;

                    if (anchor.attr("href").substring(0, 1) != "#") {
                        var webframe = window.parent.parent;
                        webframe.postMessage("navigate|" + anchor.attr("href"), "*");
                    }
                }
            }
            break;

        case "update-navigation-buttons":
            UpdateNavigationButtons();
            break;
    }
}

function UpdateNavigationButtons() {
        
    window.parent.postMessage("toggle-toc-next|" + isLastNodeSelected(), "*");
    window.parent.postMessage("toggle-toc-previous|" + isFirstNodeSelected(), "*");
    
}
