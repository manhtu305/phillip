function handleErrorUrls() 
{
 try
  {
   var loc = window.parent.location.href;
   if(loc.indexOf("#") != -1)
   {
     var errorCode = loc.substring(loc.indexOf("#")+1);
     if(errorCode)
     {
       var spans=document.getElementsByTagName("span");
       for (i=0; i<spans.length; i++) 
       {
         if(spans[i].className == "nodetext" && spans[i].innerHTML == errorCode)
         {
            var linkToErrorPage = spans[i].parentNode.getAttribute("href");
            var bodyFrame = window.parent.document.getElementById("body");
            bodyFrame.src = linkToErrorPage;
         }
       }
    }       
}
}
catch(e)
{}
}