Before you can open this project in Document! X (or HelpStudio), you must create all of the source js files that it uses from the latest Wijmo source files.
To do this, follow these steps.

1. Get latest from the following folder and all folders below it: $/Rhino/Main/Widgets
2. In the Widgets folder, double-click the genMetajs.cmd file. 

This creates the js files (from the latest source) that the project needs and puts them in the correct place relative to the project. 

Also, you need the custom template files to be in the correct location. 
Please do this BEFORE you open the project. Otherwise, the templates will change to the defaults.
Nodir has also kindly created a bat file that will do that for you.

In the $/Rhino/Main/Widgets/Documentation/dx/Templates READ README folder (checked out on your local machine), run this file:
updateTempaltes.bat

This creates the files and folders needed to use our custom templates.