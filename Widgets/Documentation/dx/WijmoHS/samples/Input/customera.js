window.eras = [
  { name: "明治", abbreviation: "明", symbol: "M", startDate: "1868-09-08", shortcuts: "1,M" }, 
  { name: "大正", abbreviation: "大", symbol: "T", startDate: "1912-07-30", shortcuts: "2,T" }, 
  { name: "昭和", abbreviation: "昭", symbol: "S", startDate: "1926-12-25", shortcuts: "3,S" }, 
  { name: "平成", abbreviation: "平", symbol: "H", startDate: "1989-01-08", shortcuts: "4,H" }
]; 
