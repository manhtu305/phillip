var wijmo = wijmo || {};

(function () {
wijmo.chart = wijmo.chart || {};

(function () {
/** @class wijpiechart
  * @widget 
  * @namespace jQuery.wijmo.chart
  * @extends wijmo.chart.wijchartcore
  */
wijmo.chart.wijpiechart = function () {};
wijmo.chart.wijpiechart.prototype = new wijmo.chart.wijchartcore();
/** Removes the functionality completely. 
  * This will return the element back to its pre-init state.
  */
wijmo.chart.wijpiechart.prototype.destroy = function () {};
/** Returns the sector of the pie chart with the given index.
  * @param {number} index The index of the sector.
  * @returns {Raphael Element} Reference to raphael element object.
  */
wijmo.chart.wijpiechart.prototype.getSector = function (index) {};

/** @class */
var wijpiechart_options = function () {};
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Sets the number of degrees of angle from which to begin painting wedges in the pie. 
  * To see an example of this property in action, check out the following link,
  * http://jsbin.com/ewofiv/1</p>
  * @field 
  * @type {number}
  * @option
  */
wijpiechart_options.prototype.startAngle = 0;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates the radius used for a pie chart.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * If the value is null, then the radius will be calculated 
  * by the width/height value of the pie chart.
  */
wijpiechart_options.prototype.radius = null;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>A value that indicates the inner radius used for doughnut charts.</p>
  * @field 
  * @type {number}
  * @option
  */
wijpiechart_options.prototype.innerRadius = 0;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.piechart_labels.html'>wijmo.chart.piechart_labels</a></p>
  * <p>A value that indicates the chart label elements of chart.</p>
  * @field 
  * @type {wijmo.chart.piechart_labels}
  * @option
  */
wijpiechart_options.prototype.labels = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.piechart_animation.html'>wijmo.chart.piechart_animation</a></p>
  * <p>The animation option  defines the animation effect and controls other aspects of the widget's animation,
  * such as duration and easing.</p>
  * @field 
  * @type {wijmo.chart.piechart_animation}
  * @option
  */
wijpiechart_options.prototype.animation = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_animation.html'>wijmo.chart.chart_animation</a></p>
  * <p>A value that indicates whether to show animation 
  * and the duration for the animation when reload data.</p>
  * @field 
  * @type {wijmo.chart.chart_animation}
  * @option
  */
wijpiechart_options.prototype.seriesTransition = null;
/** <p class='defaultValue'>Default value: []</p>
  * <p>An array collection that contains the data to be charted.</p>
  * @field 
  * @type {array}
  * @option 
  * @remarks
  * The following is the sample data for seriesList option,
  * [{ 
  * label: "Q1",
  * legendEntry: true,
  * data: 12,
  * offset: 0
  * }, {
  * label: "Q2",
  * legendEntry: true,
  * data: 21,
  * offset: 0
  * }, {
  * label: "Q3",
  * legendEntry: true,
  * data: 9,
  * offset: 0
  * }, {
  * label: "Q4",
  * legendEntry: true,
  * data: 29,
  * offset: 10
  * }]
  */
wijpiechart_options.prototype.seriesList = [];
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value that indicates whether the piechart can be rotated and tooltip is always shown 
  * on touchable devices.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijpiechart_options.prototype.enableTouchBehavior = true;
/** <p>Sets the direction in which pie segmets will be drawn - clocwise or counterClockwise.</p>
  * @field 
  * @type {pieChartDirection}
  * @option 
  * @remarks
  * default value is "counterClockwise"
  */
wijpiechart_options.prototype.direction = null;
/** <p>Gets whether Touch behavior is enabled.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijpiechart_options.prototype.isTouchBehaviorEnable = null;
/** Fires when the user clicks a mouse button.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijPieChartEventArgs} args The data with this event.
  */
wijpiechart_options.prototype.mouseDown = null;
/** Fires when the user releases a mouse button
  * while the pointer is over the chart element.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijPieChartEventArgs} args The data with this event.
  */
wijpiechart_options.prototype.mouseUp = null;
/** Fires when the user first places the pointer over the chart element.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijPieChartEventArgs} args The data with this event.
  */
wijpiechart_options.prototype.mouseOver = null;
/** Fires when the user moves the pointer off of the chart element.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijPieChartEventArgs} args The data with this event.
  */
wijpiechart_options.prototype.mouseOut = null;
/** Fires when the user moves the mouse pointer
  * while it is over a chart element.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijPieChartEventArgs} args The data with this event.
  */
wijpiechart_options.prototype.mouseMove = null;
/** Fires when the user clicks the chart element.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijPieChartEventArgs} args The data with this event.
  */
wijpiechart_options.prototype.click = null;
wijmo.chart.wijpiechart.prototype.options = $.extend({}, true, wijmo.chart.wijchartcore.prototype.options, new wijpiechart_options());
$.widget("wijmo.wijpiechart", $.wijmo.wijchartcore, wijmo.chart.wijpiechart.prototype);
/** @class wijpiechart_css
  * @namespace wijmo.chart
  * @extends wijmo.chart.wijchartcore_css
  */
wijmo.chart.wijpiechart_css = function () {};
wijmo.chart.wijpiechart_css.prototype = new wijmo.chart.wijchartcore_css();
/** @interface IWijPieChartEventArgs
  * @namespace wijmo.chart
  */
wijmo.chart.IWijPieChartEventArgs = function () {};
/** <p>Value of the sector.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.IWijPieChartEventArgs.prototype.data = null;
/** <p>Index of the sector.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.IWijPieChartEventArgs.prototype.index = null;
/** <p>Label of the sector.</p>
  * @field 
  * @type {string}
  */
wijmo.chart.IWijPieChartEventArgs.prototype.label = null;
/** <p>Legend entry of the sector.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.IWijPieChartEventArgs.prototype.legendEntry = null;
/** <p>Offset of the sector.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.IWijPieChartEventArgs.prototype.offset = null;
/** <p>Style of the sector.</p>
  * @field
  */
wijmo.chart.IWijPieChartEventArgs.prototype.style = null;
/** <p>Type of the chart, it's value is 'pie'</p>
  * @field 
  * @type {string}
  */
wijmo.chart.IWijPieChartEventArgs.prototype.type = null;
/** @interface piechart_labels
  * @namespace wijmo.chart
  */
wijmo.chart.piechart_labels = function () {};
/** <p>A value that indicates the style of the chart labels.</p>
  * @field
  */
wijmo.chart.piechart_labels.prototype.style = null;
/** <p>A value that indicates the format string of the chart labels.</p>
  * @field 
  * @type {string}
  */
wijmo.chart.piechart_labels.prototype.formatString = null;
/** <p>A value that indicates the formatter of the chart labels.</p>
  * @field 
  * @type {Function}
  */
wijmo.chart.piechart_labels.prototype.formatter = null;
/** <p>A value that indicates the style of the chart labels' connector.</p>
  * @field
  */
wijmo.chart.piechart_labels.prototype.connectorStyle = null;
/** <p>A value that indicates the style of the chart labels.</p>
  * @field 
  * @type {string}
  * @remarks
  * Options are 'inside', 'outside'.
  */
wijmo.chart.piechart_labels.prototype.position = null;
/** <p>A value that indicates the offset of the chart labels.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.piechart_labels.prototype.offset = null;
/** @interface piechart_animation
  * @namespace wijmo.chart
  * @extends wijmo.chart.chart_animation
  */
wijmo.chart.piechart_animation = function () {};
/** <p>A value that indicates the offset for an explode animation.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.piechart_animation.prototype.offset = null;
typeof wijmo.chart.chart_animation != 'undefined' && $.extend(wijmo.chart.piechart_animation.prototype, wijmo.chart.chart_animation.prototype);
})()
})()
