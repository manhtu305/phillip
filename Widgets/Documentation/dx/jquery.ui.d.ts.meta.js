var JQueryUI = JQueryUI || {};

(function () {
/** @interface AccordionOptions
  * @namespace JQueryUI
  */
JQueryUI.AccordionOptions = function () {};
/** <p>undefined</p>
  * @field
  */
JQueryUI.AccordionOptions.prototype.active = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.AccordionOptions.prototype.animate = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.AccordionOptions.prototype.collapsible = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.AccordionOptions.prototype.disabled = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.AccordionOptions.prototype.event = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.AccordionOptions.prototype.header = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.AccordionOptions.prototype.heightStyle = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.AccordionOptions.prototype.icons = null;
/** @interface AccordionUIParams
  * @namespace JQueryUI
  */
JQueryUI.AccordionUIParams = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.AccordionUIParams.prototype.newHeader = null;
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.AccordionUIParams.prototype.oldHeader = null;
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.AccordionUIParams.prototype.newPanel = null;
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.AccordionUIParams.prototype.oldPanel = null;
/** @interface AccordionEvent
  * @namespace JQueryUI
  */
JQueryUI.AccordionEvent = function () {};
/** @interface AccordionEvents
  * @namespace JQueryUI
  */
JQueryUI.AccordionEvents = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.AccordionEvent}
  */
JQueryUI.AccordionEvents.prototype.activate = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.AccordionEvent}
  */
JQueryUI.AccordionEvents.prototype.beforeActivate = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.AccordionEvent}
  */
JQueryUI.AccordionEvents.prototype.create = null;
/** @interface Accordion
  * @namespace JQueryUI
  * @extends JQueryUI.Widget
  * @extends JQueryUI.AccordionOptions
  * @extends JQueryUI.AccordionEvents
  */
JQueryUI.Accordion = function () {};
/** @interface AutocompleteOptions
  * @namespace JQueryUI
  */
JQueryUI.AutocompleteOptions = function () {};
/** <p>undefined</p>
  * @field
  */
JQueryUI.AutocompleteOptions.prototype.appendTo = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.AutocompleteOptions.prototype.autoFocus = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.AutocompleteOptions.prototype.delay = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.AutocompleteOptions.prototype.disabled = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.AutocompleteOptions.prototype.minLength = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.AutocompleteOptions.prototype.position = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.AutocompleteOptions.prototype.source = null;
/** @interface AutocompleteUIParams
  * @namespace JQueryUI
  */
JQueryUI.AutocompleteUIParams = function () {};
/** @interface AutocompleteEvent
  * @namespace JQueryUI
  */
JQueryUI.AutocompleteEvent = function () {};
/** @interface AutocompleteEvents
  * @namespace JQueryUI
  */
JQueryUI.AutocompleteEvents = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.AutocompleteEvent}
  */
JQueryUI.AutocompleteEvents.prototype.change = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.AutocompleteEvent}
  */
JQueryUI.AutocompleteEvents.prototype.close = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.AutocompleteEvent}
  */
JQueryUI.AutocompleteEvents.prototype.create = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.AutocompleteEvent}
  */
JQueryUI.AutocompleteEvents.prototype.focus = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.AutocompleteEvent}
  */
JQueryUI.AutocompleteEvents.prototype.open = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.AutocompleteEvent}
  */
JQueryUI.AutocompleteEvents.prototype.response = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.AutocompleteEvent}
  */
JQueryUI.AutocompleteEvents.prototype.search = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.AutocompleteEvent}
  */
JQueryUI.AutocompleteEvents.prototype.select = null;
/** @interface Autocomplete
  * @namespace JQueryUI
  * @extends JQueryUI.Widget
  * @extends JQueryUI.AutocompleteOptions
  * @extends JQueryUI.AutocompleteEvents
  */
JQueryUI.Autocomplete = function () {};
/** <p>undefined</p>
  * @field 
  * @type {function}
  */
JQueryUI.Autocomplete.prototype.escapeRegex = null;
/** @interface ButtonOptions
  * @namespace JQueryUI
  */
JQueryUI.ButtonOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.ButtonOptions.prototype.disabled = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.ButtonOptions.prototype.icons = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.ButtonOptions.prototype.label = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.ButtonOptions.prototype.text = null;
/** @interface Button
  * @namespace JQueryUI
  * @extends JQueryUI.Widget
  * @extends JQueryUI.ButtonOptions
  */
JQueryUI.Button = function () {};
/** @interface DatepickerOptions
  * @namespace JQueryUI
  */
JQueryUI.DatepickerOptions = function () {};
/** <p>An input element that is to be updated with the selected date from the datepicker. Use the altFormat option to change the format of the date within this field. Leave as blank for no alternate field.</p>
  * @field
  */
JQueryUI.DatepickerOptions.prototype.altField = null;
/** <p>The dateFormat to be used for the altField option. This allows one date format to be shown to the user for selection purposes, while a different format is actually sent behind the scenes. For a full list of the possible formats see the formatDate function</p>
  * @field 
  * @type {string}
  */
JQueryUI.DatepickerOptions.prototype.altFormat = null;
/** <p>The text to display after each date field, e.g., to show the required format.</p>
  * @field 
  * @type {string}
  */
JQueryUI.DatepickerOptions.prototype.appendText = null;
/** <p>Set to true to automatically resize the input field to accommodate dates in the current dateFormat.</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DatepickerOptions.prototype.autoSize = null;
/** <p>A function that takes an input field and current datepicker instance and returns an options object to update the datepicker with. It is called just before the datepicker is displayed.</p>
  * @field 
  * @type {function}
  */
JQueryUI.DatepickerOptions.prototype.beforeShow = null;
/** <p>A function that takes a date as a parameter and must return an array with:
  * [0]: true/false indicating whether or not this date is selectable
  * [1]: a CSS class name to add to the date's cell or "" for the default presentation
  * [2]: an optional popup tooltip for this date
  * The function is called for each day in the datepicker before it is displayed.</p>
  * @field 
  * @type {function}
  */
JQueryUI.DatepickerOptions.prototype.beforeShowDay = null;
/** <p>A URL of an image to use to display the datepicker when the showOn option is set to "button" or "both". If set, the buttonText option becomes the alt value and is not directly displayed.</p>
  * @field 
  * @type {string}
  */
JQueryUI.DatepickerOptions.prototype.buttonImage = null;
/** <p>Whether the button image should be rendered by itself instead of inside a button element. This option is only relevant if the buttonImage option has also been set.</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DatepickerOptions.prototype.buttonImageOnly = null;
/** <p>The text to display on the trigger button. Use in conjunction with the showOn option set to "button" or "both".</p>
  * @field 
  * @type {string}
  */
JQueryUI.DatepickerOptions.prototype.buttonText = null;
/** <p>A function to calculate the week of the year for a given date. The default implementation uses the ISO 8601 definition: weeks start on a Monday; the first week of the year contains the first Thursday of the year.</p>
  * @field 
  * @type {function}
  */
JQueryUI.DatepickerOptions.prototype.calculateWeek = null;
/** <p>Whether the month should be rendered as a dropdown instead of text.</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DatepickerOptions.prototype.changeMonth = null;
/** <p>Whether the year should be rendered as a dropdown instead of text. Use the yearRange option to control which years are made available for selection.</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DatepickerOptions.prototype.changeYear = null;
/** <p>The text to display for the close link. Use the showButtonPanel option to display this button.</p>
  * @field 
  * @type {string}
  */
JQueryUI.DatepickerOptions.prototype.closeText = null;
/** <p>When true, entry in the input field is constrained to those characters allowed by the current dateFormat option.</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DatepickerOptions.prototype.constrainInput = null;
/** <p>The text to display for the current day link. Use the showButtonPanel option to display this button.</p>
  * @field 
  * @type {string}
  */
JQueryUI.DatepickerOptions.prototype.currentText = null;
/** <p>The format for parsed and displayed dates. For a full list of the possible formats see the formatDate function.</p>
  * @field 
  * @type {string}
  */
JQueryUI.DatepickerOptions.prototype.dateFormat = null;
/** <p>The list of long day names, starting from Sunday, for use as requested via the dateFormat option.</p>
  * @field 
  * @type {string[]}
  */
JQueryUI.DatepickerOptions.prototype.dayNames = null;
/** <p>The list of minimised day names, starting from Sunday, for use as column headers within the datepicker.</p>
  * @field 
  * @type {string[]}
  */
JQueryUI.DatepickerOptions.prototype.dayNamesMin = null;
/** <p>The list of abbreviated day names, starting from Sunday, for use as requested via the dateFormat option.</p>
  * @field 
  * @type {string[]}
  */
JQueryUI.DatepickerOptions.prototype.dayNamesShort = null;
/** <p>Set the date to highlight on first opening if the field is blank. Specify either an actual date via a Date object or as a string in the current dateFormat, or a number of days from today (e.g. +7) or a string of values and periods ('y' for years, 'm' for months, 'w' for weeks, 'd' for days, e.g. '+1m +7d'), or null for today.
  * Multiple types supported:
  * Date: A date object containing the default date.
  * Number: A number of days from today. For example 2 represents two days from today and -1 represents yesterday.
  * String: A string in the format defined by the dateFormat option, or a relative date. Relative dates must contain value and period pairs; valid periods are "y" for years, "m" for months, "w" for weeks, and "d" for days. For example, "+1m +7d" represents one month and seven days from today.</p>
  * @field
  */
JQueryUI.DatepickerOptions.prototype.defaultDate = null;
/** <p>Control the speed at which the datepicker appears, it may be a time in milliseconds or a string representing one of the three predefined speeds ("slow", "normal", "fast").</p>
  * @field 
  * @type {string}
  */
JQueryUI.DatepickerOptions.prototype.duration = null;
/** <p>Set the first day of the week: Sunday is 0, Monday is 1, etc.</p>
  * @field 
  * @type {number}
  */
JQueryUI.DatepickerOptions.prototype.firstDay = null;
/** <p>When true, the current day link moves to the currently selected date instead of today.</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DatepickerOptions.prototype.gotoCurrent = null;
/** <p>Normally the previous and next links are disabled when not applicable (see the minDate and maxDate options). You can hide them altogether by setting this attribute to true.</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DatepickerOptions.prototype.hideIfNoPrevNext = null;
/** <p>Whether the current language is drawn from right to left.</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DatepickerOptions.prototype.isRTL = null;
/** <p>The maximum selectable date. When set to null, there is no maximum.
  * Multiple types supported:
  * Date: A date object containing the maximum date.
  * Number: A number of days from today. For example 2 represents two days from today and -1 represents yesterday.
  * String: A string in the format defined by the dateFormat option, or a relative date. Relative dates must contain value and period pairs; valid periods are "y" for years, "m" for months, "w" for weeks, and "d" for days. For example, "+1m +7d" represents one month and seven days from today.</p>
  * @field
  */
JQueryUI.DatepickerOptions.prototype.maxDate = null;
/** <p>The minimum selectable date. When set to null, there is no minimum.
  * Multiple types supported:
  * Date: A date object containing the minimum date.
  * Number: A number of days from today. For example 2 represents two days from today and -1 represents yesterday.
  * String: A string in the format defined by the dateFormat option, or a relative date. Relative dates must contain value and period pairs; valid periods are "y" for years, "m" for months, "w" for weeks, and "d" for days. For example, "+1m +7d" represents one month and seven days from today.</p>
  * @field
  */
JQueryUI.DatepickerOptions.prototype.minDate = null;
/** <p>The list of full month names, for use as requested via the dateFormat option.</p>
  * @field 
  * @type {string[]}
  */
JQueryUI.DatepickerOptions.prototype.monthNames = null;
/** <p>The list of abbreviated month names, as used in the month header on each datepicker and as requested via the dateFormat option.</p>
  * @field 
  * @type {string[]}
  */
JQueryUI.DatepickerOptions.prototype.monthNamesShort = null;
/** <p>Whether the prevText and nextText options should be parsed as dates by the formatDate function, allowing them to display the target month names for example.</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DatepickerOptions.prototype.navigationAsDateFormat = null;
/** <p>The text to display for the next month link. With the standard ThemeRoller styling, this value is replaced by an icon.</p>
  * @field 
  * @type {string}
  */
JQueryUI.DatepickerOptions.prototype.nextText = null;
/** <p>The number of months to show at once.
  * Multiple types supported:
  * Number: The number of months to display in a single row.
  * Array: An array defining the number of rows and columns to display.</p>
  * @field
  */
JQueryUI.DatepickerOptions.prototype.numberOfMonths = null;
/** <p>Called when the datepicker moves to a new month and/or year. The function receives the selected year, month (1-12), and the datepicker instance as parameters. this refers to the associated input field.</p>
  * @field 
  * @type {function}
  */
JQueryUI.DatepickerOptions.prototype.onChangeMonthYear = null;
/** <p>Called when the datepicker is closed, whether or not a date is selected. The function receives the selected date as text ("" if none) and the datepicker instance as parameters. this refers to the associated input field.</p>
  * @field 
  * @type {function}
  */
JQueryUI.DatepickerOptions.prototype.onClose = null;
/** <p>Called when the datepicker is selected. The function receives the selected date as text and the datepicker instance as parameters. this refers to the associated input field.</p>
  * @field 
  * @type {function}
  */
JQueryUI.DatepickerOptions.prototype.onSelect = null;
/** <p>The text to display for the previous month link. With the standard ThemeRoller styling, this value is replaced by an icon.</p>
  * @field 
  * @type {string}
  */
JQueryUI.DatepickerOptions.prototype.prevText = null;
/** <p>Whether days in other months shown before or after the current month are selectable. This only applies if the showOtherMonths option is set to true.</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DatepickerOptions.prototype.selectOtherMonths = null;
/** <p>The cutoff year for determining the century for a date (used in conjunction with dateFormat 'y'). Any dates entered with a year value less than or equal to the cutoff year are considered to be in the current century, while those greater than it are deemed to be in the previous century.
  * Multiple types supported:
  * Number: A value between 0 and 99 indicating the cutoff year.
  * String: A relative number of years from the current year, e.g., "+3" or "-5".</p>
  * @field
  */
JQueryUI.DatepickerOptions.prototype.shortYearCutoff = null;
/** <p>The name of the animation used to show and hide the datepicker. Use "show" (the default), "slideDown", "fadeIn", any of the jQuery UI effects. Set to an empty string to disable animation.</p>
  * @field 
  * @type {string}
  */
JQueryUI.DatepickerOptions.prototype.showAnim = null;
/** <p>Whether to display a button pane underneath the calendar. The button pane contains two buttons, a Today button that links to the current day, and a Done button that closes the datepicker. The buttons' text can be customized using the currentText and closeText options respectively.</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DatepickerOptions.prototype.showButtonPanel = null;
/** <p>When displaying multiple months via the numberOfMonths option, the showCurrentAtPos option defines which position to display the current month in.</p>
  * @field 
  * @type {number}
  */
JQueryUI.DatepickerOptions.prototype.showCurrentAtPos = null;
/** <p>Whether to show the month after the year in the header.</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DatepickerOptions.prototype.showMonthAfterYear = null;
/** <p>When the datepicker should appear. The datepicker can appear when the field receives focus ("focus"), when a button is clicked ("button"), or when either event occurs ("both").</p>
  * @field 
  * @type {string}
  */
JQueryUI.DatepickerOptions.prototype.showOn = null;
/** <p>If using one of the jQuery UI effects for the showAnim option, you can provide additional settings for that animation via this option.</p>
  * @field
  */
JQueryUI.DatepickerOptions.prototype.showOptions = null;
/** <p>Whether to display dates in other months (non-selectable) at the start or end of the current month. To make these days selectable use the selectOtherMonths option.</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DatepickerOptions.prototype.showOtherMonths = null;
/** <p>When true, a column is added to show the week of the year. The calculateWeek option determines how the week of the year is calculated. You may also want to change the firstDay option.</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DatepickerOptions.prototype.showWeek = null;
/** <p>Set how many months to move when clicking the previous/next links.</p>
  * @field 
  * @type {number}
  */
JQueryUI.DatepickerOptions.prototype.stepMonths = null;
/** <p>The text to display for the week of the year column heading. Use the showWeek option to display this column.</p>
  * @field 
  * @type {string}
  */
JQueryUI.DatepickerOptions.prototype.weekHeader = null;
/** <p>The range of years displayed in the year drop-down: either relative to today's year ("-nn:+nn"), relative to the currently selected year ("c-nn:c+nn"), absolute ("nnnn:nnnn"), or combinations of these formats ("nnnn:-nn"). Note that this option only affects what appears in the drop-down, to restrict which dates may be selected use the minDate and/or maxDate options.</p>
  * @field 
  * @type {string}
  */
JQueryUI.DatepickerOptions.prototype.yearRange = null;
/** <p>Additional text to display after the year in the month headers.</p>
  * @field 
  * @type {string}
  */
JQueryUI.DatepickerOptions.prototype.yearSuffix = null;
/** @interface DatepickerFormatDateOptions
  * @namespace JQueryUI
  */
JQueryUI.DatepickerFormatDateOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string[]}
  */
JQueryUI.DatepickerFormatDateOptions.prototype.dayNamesShort = null;
/** <p>undefined</p>
  * @field 
  * @type {string[]}
  */
JQueryUI.DatepickerFormatDateOptions.prototype.dayNames = null;
/** <p>undefined</p>
  * @field 
  * @type {string[]}
  */
JQueryUI.DatepickerFormatDateOptions.prototype.monthNamesShort = null;
/** <p>undefined</p>
  * @field 
  * @type {string[]}
  */
JQueryUI.DatepickerFormatDateOptions.prototype.monthNames = null;
/** @interface Datepicker
  * @namespace JQueryUI
  * @extends JQueryUI.Widget
  * @extends JQueryUI.DatepickerOptions
  */
JQueryUI.Datepicker = function () {};
/** @param {JQueryUI.DatepickerOptions} defaults
  * @returns {void}
  */
JQueryUI.Datepicker.prototype.setDefaults = function (defaults) {};
/** @param {string} format
  * @param {Date} date
  * @param {JQueryUI.DatepickerFormatDateOptions} settings
  * @returns {string}
  */
JQueryUI.Datepicker.prototype.formatDate = function (format, date, settings) {};
/** @param {string} format
  * @param {string} date
  * @param {JQueryUI.DatepickerFormatDateOptions} settings
  * @returns {Date}
  */
JQueryUI.Datepicker.prototype.parseDate = function (format, date, settings) {};
/** @param {Date} date
  * @returns {number}
  */
JQueryUI.Datepicker.prototype.iso8601Week = function (date) {};
/** @param {Date} date
  * @returns {any[]}
  */
JQueryUI.Datepicker.prototype.noWeekends = function (date) {};
/** <p>undefined</p>
  * @field
  */
JQueryUI.Datepicker.prototype.regional = null;
/** @interface DialogOptions
  * @namespace JQueryUI
  */
JQueryUI.DialogOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DialogOptions.prototype.autoOpen = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DialogOptions.prototype.buttons = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DialogOptions.prototype.closeOnEscape = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.DialogOptions.prototype.closeText = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.DialogOptions.prototype.dialogClass = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DialogOptions.prototype.disabled = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DialogOptions.prototype.draggable = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DialogOptions.prototype.height = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.DialogOptions.prototype.maxHeight = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.DialogOptions.prototype.maxWidth = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.DialogOptions.prototype.minHeight = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.DialogOptions.prototype.minWidth = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DialogOptions.prototype.modal = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DialogOptions.prototype.position = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DialogOptions.prototype.resizable = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DialogOptions.prototype.show = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DialogOptions.prototype.stack = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.DialogOptions.prototype.title = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DialogOptions.prototype.width = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.DialogOptions.prototype.zIndex = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DialogEvent}
  */
JQueryUI.DialogOptions.prototype.close = null;
/** @interface DialogUIParams
  * @namespace JQueryUI
  */
JQueryUI.DialogUIParams = function () {};
/** @interface DialogEvent
  * @namespace JQueryUI
  */
JQueryUI.DialogEvent = function () {};
/** @interface DialogEvents
  * @namespace JQueryUI
  */
JQueryUI.DialogEvents = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DialogEvent}
  */
JQueryUI.DialogEvents.prototype.beforeClose = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DialogEvent}
  */
JQueryUI.DialogEvents.prototype.close = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DialogEvent}
  */
JQueryUI.DialogEvents.prototype.create = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DialogEvent}
  */
JQueryUI.DialogEvents.prototype.drag = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DialogEvent}
  */
JQueryUI.DialogEvents.prototype.dragStart = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DialogEvent}
  */
JQueryUI.DialogEvents.prototype.dragStop = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DialogEvent}
  */
JQueryUI.DialogEvents.prototype.focus = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DialogEvent}
  */
JQueryUI.DialogEvents.prototype.open = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DialogEvent}
  */
JQueryUI.DialogEvents.prototype.resize = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DialogEvent}
  */
JQueryUI.DialogEvents.prototype.resizeStart = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DialogEvent}
  */
JQueryUI.DialogEvents.prototype.resizeStop = null;
/** @interface Dialog
  * @namespace JQueryUI
  * @extends JQueryUI.Widget
  * @extends JQueryUI.DialogOptions
  * @extends JQueryUI.DialogEvents
  */
JQueryUI.Dialog = function () {};
/** @interface DraggableEventUIParams
  * @namespace JQueryUI
  */
JQueryUI.DraggableEventUIParams = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.DraggableEventUIParams.prototype.helper = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DraggableEventUIParams.prototype.position = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DraggableEventUIParams.prototype.offset = null;
/** @interface DraggableEvent
  * @namespace JQueryUI
  */
JQueryUI.DraggableEvent = function () {};
/** @interface DraggableOptions
  * @namespace JQueryUI
  */
JQueryUI.DraggableOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DraggableOptions.prototype.disabled = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DraggableOptions.prototype.addClasses = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DraggableOptions.prototype.appendTo = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.DraggableOptions.prototype.axis = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.DraggableOptions.prototype.cancel = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.DraggableOptions.prototype.connectToSortable = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DraggableOptions.prototype.containment = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.DraggableOptions.prototype.cursor = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DraggableOptions.prototype.cursorAt = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.DraggableOptions.prototype.delay = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.DraggableOptions.prototype.distance = null;
/** <p>undefined</p>
  * @field 
  * @type {number[]}
  */
JQueryUI.DraggableOptions.prototype.grid = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DraggableOptions.prototype.handle = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DraggableOptions.prototype.helper = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DraggableOptions.prototype.iframeFix = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.DraggableOptions.prototype.opacity = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DraggableOptions.prototype.refreshPositions = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DraggableOptions.prototype.revert = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.DraggableOptions.prototype.revertDuration = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.DraggableOptions.prototype.scope = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DraggableOptions.prototype.scroll = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.DraggableOptions.prototype.scrollSensitivity = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.DraggableOptions.prototype.scrollSpeed = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DraggableOptions.prototype.snap = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.DraggableOptions.prototype.snapMode = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.DraggableOptions.prototype.snapTolerance = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.DraggableOptions.prototype.stack = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.DraggableOptions.prototype.zIndex = null;
/** @interface DraggableEvents
  * @namespace JQueryUI
  */
JQueryUI.DraggableEvents = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DraggableEvent}
  */
JQueryUI.DraggableEvents.prototype.create = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DraggableEvent}
  */
JQueryUI.DraggableEvents.prototype.start = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DraggableEvent}
  */
JQueryUI.DraggableEvents.prototype.drag = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DraggableEvent}
  */
JQueryUI.DraggableEvents.prototype.stop = null;
/** @interface Draggable
  * @namespace JQueryUI
  * @extends JQueryUI.Widget
  * @extends JQueryUI.DraggableOptions
  * @extends JQueryUI.DraggableEvents
  */
JQueryUI.Draggable = function () {};
/** @interface DroppableEventUIParam
  * @namespace JQueryUI
  */
JQueryUI.DroppableEventUIParam = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.DroppableEventUIParam.prototype.draggable = null;
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.DroppableEventUIParam.prototype.helper = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DroppableEventUIParam.prototype.position = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DroppableEventUIParam.prototype.offset = null;
/** @interface DroppableEvent
  * @namespace JQueryUI
  */
JQueryUI.DroppableEvent = function () {};
/** @interface DroppableOptions
  * @namespace JQueryUI
  */
JQueryUI.DroppableOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DroppableOptions.prototype.disabled = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.DroppableOptions.prototype.accept = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.DroppableOptions.prototype.activeClass = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.DroppableOptions.prototype.greedy = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.DroppableOptions.prototype.hoverClass = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.DroppableOptions.prototype.scope = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.DroppableOptions.prototype.tolerance = null;
/** @interface DroppableEvents
  * @namespace JQueryUI
  */
JQueryUI.DroppableEvents = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DroppableEvent}
  */
JQueryUI.DroppableEvents.prototype.create = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DroppableEvent}
  */
JQueryUI.DroppableEvents.prototype.activate = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DroppableEvent}
  */
JQueryUI.DroppableEvents.prototype.deactivate = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DroppableEvent}
  */
JQueryUI.DroppableEvents.prototype.over = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DroppableEvent}
  */
JQueryUI.DroppableEvents.prototype.out = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.DroppableEvent}
  */
JQueryUI.DroppableEvents.prototype.drop = null;
/** @interface Droppable
  * @namespace JQueryUI
  * @extends JQueryUI.Widget
  * @extends JQueryUI.DroppableOptions
  * @extends JQueryUI.DroppableEvents
  */
JQueryUI.Droppable = function () {};
/** @interface MenuOptions
  * @namespace JQueryUI
  */
JQueryUI.MenuOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.MenuOptions.prototype.disabled = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.MenuOptions.prototype.icons = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.MenuOptions.prototype.menus = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.MenuOptions.prototype.position = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.MenuOptions.prototype.role = null;
/** @interface MenuUIParams
  * @namespace JQueryUI
  */
JQueryUI.MenuUIParams = function () {};
/** @interface MenuEvent
  * @namespace JQueryUI
  */
JQueryUI.MenuEvent = function () {};
/** @interface MenuEvents
  * @namespace JQueryUI
  */
JQueryUI.MenuEvents = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.MenuEvent}
  */
JQueryUI.MenuEvents.prototype.blur = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.MenuEvent}
  */
JQueryUI.MenuEvents.prototype.create = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.MenuEvent}
  */
JQueryUI.MenuEvents.prototype.focus = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.MenuEvent}
  */
JQueryUI.MenuEvents.prototype.select = null;
/** @interface Menu
  * @namespace JQueryUI
  * @extends JQueryUI.Widget
  * @extends JQueryUI.MenuOptions
  * @extends JQueryUI.MenuEvents
  */
JQueryUI.Menu = function () {};
/** @interface ProgressbarOptions
  * @namespace JQueryUI
  */
JQueryUI.ProgressbarOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.ProgressbarOptions.prototype.disabled = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.ProgressbarOptions.prototype.value = null;
/** @interface ProgressbarUIParams
  * @namespace JQueryUI
  */
JQueryUI.ProgressbarUIParams = function () {};
/** @interface ProgressbarEvent
  * @namespace JQueryUI
  */
JQueryUI.ProgressbarEvent = function () {};
/** @interface ProgressbarEvents
  * @namespace JQueryUI
  */
JQueryUI.ProgressbarEvents = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.ProgressbarEvent}
  */
JQueryUI.ProgressbarEvents.prototype.change = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.ProgressbarEvent}
  */
JQueryUI.ProgressbarEvents.prototype.complete = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.ProgressbarEvent}
  */
JQueryUI.ProgressbarEvents.prototype.create = null;
/** @interface Progressbar
  * @namespace JQueryUI
  * @extends JQueryUI.Widget
  * @extends JQueryUI.ProgressbarOptions
  * @extends JQueryUI.ProgressbarEvents
  */
JQueryUI.Progressbar = function () {};
/** @interface ResizableOptions
  * @namespace JQueryUI
  */
JQueryUI.ResizableOptions = function () {};
/** <p>undefined</p>
  * @field
  */
JQueryUI.ResizableOptions.prototype.alsoResize = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.ResizableOptions.prototype.animate = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.ResizableOptions.prototype.animateDuration = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.ResizableOptions.prototype.animateEasing = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.ResizableOptions.prototype.aspectRatio = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.ResizableOptions.prototype.autoHide = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.ResizableOptions.prototype.cancel = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.ResizableOptions.prototype.containment = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.ResizableOptions.prototype.delay = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.ResizableOptions.prototype.disabled = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.ResizableOptions.prototype.distance = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.ResizableOptions.prototype.ghost = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.ResizableOptions.prototype.grid = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.ResizableOptions.prototype.handles = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.ResizableOptions.prototype.helper = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.ResizableOptions.prototype.maxHeight = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.ResizableOptions.prototype.maxWidth = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.ResizableOptions.prototype.minHeight = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.ResizableOptions.prototype.minWidth = null;
/** @interface ResizableUIParams
  * @namespace JQueryUI
  */
JQueryUI.ResizableUIParams = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.ResizableUIParams.prototype.element = null;
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.ResizableUIParams.prototype.helper = null;
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.ResizableUIParams.prototype.originalElement = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.ResizableUIParams.prototype.originalPosition = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.ResizableUIParams.prototype.originalSize = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.ResizableUIParams.prototype.position = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.ResizableUIParams.prototype.size = null;
/** @interface ResizableEvent
  * @namespace JQueryUI
  */
JQueryUI.ResizableEvent = function () {};
/** @interface ResizableEvents
  * @namespace JQueryUI
  */
JQueryUI.ResizableEvents = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.ResizableEvent}
  */
JQueryUI.ResizableEvents.prototype.resize = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.ResizableEvent}
  */
JQueryUI.ResizableEvents.prototype.start = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.ResizableEvent}
  */
JQueryUI.ResizableEvents.prototype.stop = null;
/** @interface Resizable
  * @namespace JQueryUI
  * @extends JQueryUI.Widget
  * @extends JQueryUI.ResizableOptions
  * @extends JQueryUI.ResizableEvents
  */
JQueryUI.Resizable = function () {};
/** @interface SelectableOptions
  * @namespace JQueryUI
  */
JQueryUI.SelectableOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.SelectableOptions.prototype.autoRefresh = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.SelectableOptions.prototype.cancel = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.SelectableOptions.prototype.delay = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.SelectableOptions.prototype.disabled = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.SelectableOptions.prototype.distance = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.SelectableOptions.prototype.filter = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.SelectableOptions.prototype.tolerance = null;
/** @interface SelectableEvents
  * @namespace JQueryUI
  */
JQueryUI.SelectableEvents = function () {};
/** @param {Event} event
  * @param ui
  * @returns {void}
  */
JQueryUI.SelectableEvents.prototype.selected = function (event, ui) {};
/** @param {Event} event
  * @param ui
  * @returns {void}
  */
JQueryUI.SelectableEvents.prototype.selecting = function (event, ui) {};
/** @param {Event} event
  * @param ui
  * @returns {void}
  */
JQueryUI.SelectableEvents.prototype.start = function (event, ui) {};
/** @param {Event} event
  * @param ui
  * @returns {void}
  */
JQueryUI.SelectableEvents.prototype.stop = function (event, ui) {};
/** @param {Event} event
  * @param ui
  * @returns {void}
  */
JQueryUI.SelectableEvents.prototype.unselected = function (event, ui) {};
/** @param {Event} event
  * @param ui
  * @returns {void}
  */
JQueryUI.SelectableEvents.prototype.unselecting = function (event, ui) {};
/** @interface Selectable
  * @namespace JQueryUI
  * @extends JQueryUI.Widget
  * @extends JQueryUI.SelectableOptions
  * @extends JQueryUI.SelectableEvents
  */
JQueryUI.Selectable = function () {};
/** @interface SliderOptions
  * @namespace JQueryUI
  */
JQueryUI.SliderOptions = function () {};
/** <p>undefined</p>
  * @field
  */
JQueryUI.SliderOptions.prototype.animate = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.SliderOptions.prototype.disabled = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.SliderOptions.prototype.max = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.SliderOptions.prototype.min = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.SliderOptions.prototype.orientation = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.SliderOptions.prototype.range = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.SliderOptions.prototype.step = null;
/** @interface SliderUIParams
  * @namespace JQueryUI
  */
JQueryUI.SliderUIParams = function () {};
/** @interface SliderEvent
  * @namespace JQueryUI
  */
JQueryUI.SliderEvent = function () {};
/** @interface SliderEvents
  * @namespace JQueryUI
  */
JQueryUI.SliderEvents = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SliderEvent}
  */
JQueryUI.SliderEvents.prototype.change = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SliderEvent}
  */
JQueryUI.SliderEvents.prototype.create = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SliderEvent}
  */
JQueryUI.SliderEvents.prototype.slide = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SliderEvent}
  */
JQueryUI.SliderEvents.prototype.start = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SliderEvent}
  */
JQueryUI.SliderEvents.prototype.stop = null;
/** @interface Slider
  * @namespace JQueryUI
  * @extends JQueryUI.Widget
  * @extends JQueryUI.SliderOptions
  * @extends JQueryUI.SliderEvents
  */
JQueryUI.Slider = function () {};
/** @interface SortableOptions
  * @namespace JQueryUI
  * @extends JQueryUI.SortableEvents
  */
JQueryUI.SortableOptions = function () {};
/** <p>undefined</p>
  * @field
  */
JQueryUI.SortableOptions.prototype.appendTo = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.SortableOptions.prototype.axis = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.SortableOptions.prototype.cancel = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.SortableOptions.prototype.connectWith = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.SortableOptions.prototype.containment = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.SortableOptions.prototype.cursor = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.SortableOptions.prototype.cursorAt = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.SortableOptions.prototype.delay = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.SortableOptions.prototype.disabled = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.SortableOptions.prototype.distance = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.SortableOptions.prototype.dropOnEmpty = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.SortableOptions.prototype.forceHelperSize = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.SortableOptions.prototype.forcePlaceholderSize = null;
/** <p>undefined</p>
  * @field 
  * @type {number[]}
  */
JQueryUI.SortableOptions.prototype.grid = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.SortableOptions.prototype.handle = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.SortableOptions.prototype.items = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.SortableOptions.prototype.opacity = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.SortableOptions.prototype.placeholder = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.SortableOptions.prototype.revert = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.SortableOptions.prototype.scroll = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.SortableOptions.prototype.scrollSensitivity = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.SortableOptions.prototype.scrollSpeed = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.SortableOptions.prototype.tolerance = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.SortableOptions.prototype.zIndex = null;
/** @interface SortableUIParams
  * @namespace JQueryUI
  */
JQueryUI.SortableUIParams = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.SortableUIParams.prototype.helper = null;
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.SortableUIParams.prototype.item = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.SortableUIParams.prototype.offset = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.SortableUIParams.prototype.position = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.SortableUIParams.prototype.originalPosition = null;
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.SortableUIParams.prototype.sender = null;
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.SortableUIParams.prototype.placeholder = null;
/** @interface SortableEvent
  * @namespace JQueryUI
  */
JQueryUI.SortableEvent = function () {};
/** @interface SortableEvents
  * @namespace JQueryUI
  */
JQueryUI.SortableEvents = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SortableEvent}
  */
JQueryUI.SortableEvents.prototype.activate = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SortableEvent}
  */
JQueryUI.SortableEvents.prototype.beforeStop = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SortableEvent}
  */
JQueryUI.SortableEvents.prototype.change = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SortableEvent}
  */
JQueryUI.SortableEvents.prototype.deactivate = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SortableEvent}
  */
JQueryUI.SortableEvents.prototype.out = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SortableEvent}
  */
JQueryUI.SortableEvents.prototype.over = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SortableEvent}
  */
JQueryUI.SortableEvents.prototype.receive = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SortableEvent}
  */
JQueryUI.SortableEvents.prototype.remove = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SortableEvent}
  */
JQueryUI.SortableEvents.prototype.sort = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SortableEvent}
  */
JQueryUI.SortableEvents.prototype.start = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SortableEvent}
  */
JQueryUI.SortableEvents.prototype.stop = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SortableEvent}
  */
JQueryUI.SortableEvents.prototype.update = null;
/** @interface Sortable
  * @namespace JQueryUI
  * @extends JQueryUI.Widget
  * @extends JQueryUI.SortableOptions
  * @extends JQueryUI.SortableEvents
  */
JQueryUI.Sortable = function () {};
/** @interface SpinnerOptions
  * @namespace JQueryUI
  */
JQueryUI.SpinnerOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.SpinnerOptions.prototype.culture = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.SpinnerOptions.prototype.disabled = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.SpinnerOptions.prototype.icons = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.SpinnerOptions.prototype.incremental = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.SpinnerOptions.prototype.max = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.SpinnerOptions.prototype.min = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.SpinnerOptions.prototype.numberFormat = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.SpinnerOptions.prototype.page = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.SpinnerOptions.prototype.step = null;
/** @interface SpinnerUIParams
  * @namespace JQueryUI
  */
JQueryUI.SpinnerUIParams = function () {};
/** @interface SpinnerEvent
  * @namespace JQueryUI
  */
JQueryUI.SpinnerEvent = function () {};
/** @interface SpinnerEvents
  * @namespace JQueryUI
  */
JQueryUI.SpinnerEvents = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SpinnerEvent}
  */
JQueryUI.SpinnerEvents.prototype.spin = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SpinnerEvent}
  */
JQueryUI.SpinnerEvents.prototype.start = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.SpinnerEvent}
  */
JQueryUI.SpinnerEvents.prototype.stop = null;
/** @interface Spinner
  * @namespace JQueryUI
  * @extends JQueryUI.Widget
  * @extends JQueryUI.SpinnerOptions
  * @extends JQueryUI.SpinnerEvents
  */
JQueryUI.Spinner = function () {};
/** @interface TabsOptions
  * @namespace JQueryUI
  */
JQueryUI.TabsOptions = function () {};
/** <p>undefined</p>
  * @field
  */
JQueryUI.TabsOptions.prototype.active = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.TabsOptions.prototype.collapsible = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.TabsOptions.prototype.disabled = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.TabsOptions.prototype.event = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.TabsOptions.prototype.heightStyle = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.TabsOptions.prototype.hide = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.TabsOptions.prototype.show = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.TabsEvent}
  */
JQueryUI.TabsOptions.prototype.activate = null;
/** @interface TabsUIParams
  * @namespace JQueryUI
  */
JQueryUI.TabsUIParams = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.TabsUIParams.prototype.newTab = null;
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.TabsUIParams.prototype.oldTab = null;
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.TabsUIParams.prototype.newPanel = null;
/** <p>undefined</p>
  * @field 
  * @type {JQuery}
  */
JQueryUI.TabsUIParams.prototype.oldPanel = null;
/** @interface TabsEvent
  * @namespace JQueryUI
  */
JQueryUI.TabsEvent = function () {};
/** @interface TabsEvents
  * @namespace JQueryUI
  */
JQueryUI.TabsEvents = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.TabsEvent}
  */
JQueryUI.TabsEvents.prototype.activate = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.TabsEvent}
  */
JQueryUI.TabsEvents.prototype.beforeActivate = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.TabsEvent}
  */
JQueryUI.TabsEvents.prototype.beforeLoad = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.TabsEvent}
  */
JQueryUI.TabsEvents.prototype.load = null;
/** @interface Tabs
  * @namespace JQueryUI
  * @extends JQueryUI.Widget
  * @extends JQueryUI.TabsOptions
  * @extends JQueryUI.TabsEvents
  */
JQueryUI.Tabs = function () {};
/** @interface TooltipOptions
  * @namespace JQueryUI
  */
JQueryUI.TooltipOptions = function () {};
/** <p>undefined</p>
  * @field
  */
JQueryUI.TooltipOptions.prototype.content = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.TooltipOptions.prototype.disabled = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.TooltipOptions.prototype.hide = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.TooltipOptions.prototype.items = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.TooltipOptions.prototype.position = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.TooltipOptions.prototype.show = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.TooltipOptions.prototype.tooltipClass = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.TooltipOptions.prototype.track = null;
/** @interface TooltipUIParams
  * @namespace JQueryUI
  */
JQueryUI.TooltipUIParams = function () {};
/** @interface TooltipEvent
  * @namespace JQueryUI
  */
JQueryUI.TooltipEvent = function () {};
/** @interface TooltipEvents
  * @namespace JQueryUI
  */
JQueryUI.TooltipEvents = function () {};
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.TooltipEvent}
  */
JQueryUI.TooltipEvents.prototype.close = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.TooltipEvent}
  */
JQueryUI.TooltipEvents.prototype.open = null;
/** @interface Tooltip
  * @namespace JQueryUI
  * @extends JQueryUI.Widget
  * @extends JQueryUI.TooltipOptions
  * @extends JQueryUI.TooltipEvents
  */
JQueryUI.Tooltip = function () {};
/** @interface EffectOptions
  * @namespace JQueryUI
  */
JQueryUI.EffectOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.EffectOptions.prototype.effect = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.EffectOptions.prototype.easing = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.EffectOptions.prototype.duration = null;
/** <p>undefined</p>
  * @field 
  * @type {Function}
  */
JQueryUI.EffectOptions.prototype.complete = null;
/** @interface BlindEffect
  * @namespace JQueryUI
  */
JQueryUI.BlindEffect = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.BlindEffect.prototype.direction = null;
/** @interface BounceEffect
  * @namespace JQueryUI
  */
JQueryUI.BounceEffect = function () {};
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.BounceEffect.prototype.distance = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.BounceEffect.prototype.times = null;
/** @interface ClipEffect
  * @namespace JQueryUI
  */
JQueryUI.ClipEffect = function () {};
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.ClipEffect.prototype.direction = null;
/** @interface DropEffect
  * @namespace JQueryUI
  */
JQueryUI.DropEffect = function () {};
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.DropEffect.prototype.direction = null;
/** @interface ExplodeEffect
  * @namespace JQueryUI
  */
JQueryUI.ExplodeEffect = function () {};
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.ExplodeEffect.prototype.pieces = null;
/** @interface FadeEffect
  * @namespace JQueryUI
  */
JQueryUI.FadeEffect = function () {};
/** @interface FoldEffect
  * @namespace JQueryUI
  */
JQueryUI.FoldEffect = function () {};
/** <p>undefined</p>
  * @field
  */
JQueryUI.FoldEffect.prototype.size = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.FoldEffect.prototype.horizFirst = null;
/** @interface HighlightEffect
  * @namespace JQueryUI
  */
JQueryUI.HighlightEffect = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.HighlightEffect.prototype.color = null;
/** @interface PuffEffect
  * @namespace JQueryUI
  */
JQueryUI.PuffEffect = function () {};
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.PuffEffect.prototype.percent = null;
/** @interface PulsateEffect
  * @namespace JQueryUI
  */
JQueryUI.PulsateEffect = function () {};
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.PulsateEffect.prototype.times = null;
/** @interface ScaleEffect
  * @namespace JQueryUI
  */
JQueryUI.ScaleEffect = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.ScaleEffect.prototype.direction = null;
/** <p>undefined</p>
  * @field 
  * @type {string[]}
  */
JQueryUI.ScaleEffect.prototype.origin = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.ScaleEffect.prototype.percent = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.ScaleEffect.prototype.scale = null;
/** @interface ShakeEffect
  * @namespace JQueryUI
  */
JQueryUI.ShakeEffect = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.ShakeEffect.prototype.direction = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.ShakeEffect.prototype.distance = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.ShakeEffect.prototype.times = null;
/** @interface SizeEffect
  * @namespace JQueryUI
  */
JQueryUI.SizeEffect = function () {};
/** <p>undefined</p>
  * @field
  */
JQueryUI.SizeEffect.prototype.to = null;
/** <p>undefined</p>
  * @field 
  * @type {string[]}
  */
JQueryUI.SizeEffect.prototype.origin = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.SizeEffect.prototype.scale = null;
/** @interface SlideEffect
  * @namespace JQueryUI
  */
JQueryUI.SlideEffect = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.SlideEffect.prototype.direction = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.SlideEffect.prototype.distance = null;
/** @interface TransferEffect
  * @namespace JQueryUI
  */
JQueryUI.TransferEffect = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.TransferEffect.prototype.className = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.TransferEffect.prototype.to = null;
/** @interface JQueryPositionOptions
  * @namespace JQueryUI
  */
JQueryUI.JQueryPositionOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.JQueryPositionOptions.prototype.my = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.JQueryPositionOptions.prototype.at = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.JQueryPositionOptions.prototype.of = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.JQueryPositionOptions.prototype.collision = null;
/** <p>undefined</p>
  * @field 
  * @type {Function}
  */
JQueryUI.JQueryPositionOptions.prototype.using = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.JQueryPositionOptions.prototype.within = null;
/** @interface MouseOptions
  * @namespace JQueryUI
  */
JQueryUI.MouseOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.MouseOptions.prototype.cancel = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.MouseOptions.prototype.delay = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.MouseOptions.prototype.distance = null;
/** @interface KeyCode
  * @namespace JQueryUI
  */
JQueryUI.KeyCode = function () {};
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.BACKSPACE = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.COMMA = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.DELETE = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.DOWN = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.END = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.ENTER = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.ESCAPE = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.HOME = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.LEFT = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.NUMPAD_ADD = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.NUMPAD_DECIMAL = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.NUMPAD_DIVIDE = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.NUMPAD_ENTER = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.NUMPAD_MULTIPLY = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.NUMPAD_SUBTRACT = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.PAGE_DOWN = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.PAGE_UP = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.PERIOD = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.RIGHT = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.SPACE = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.TAB = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
JQueryUI.KeyCode.prototype.UP = null;
/** @interface UI
  * @namespace JQueryUI
  */
JQueryUI.UI = function () {};
/** @param {string} method
  * @returns {JQuery}
  */
JQueryUI.UI.prototype.mouse = function (method) {};
/** @param {JQueryUI.MouseOptions} options
  * @returns {JQuery}
  */
JQueryUI.UI.prototype.mouse = function (options) {};
/** @param {string} optionLiteral
  * @param {string} optionName
  * @param optionValue
  * @returns {JQuery}
  */
JQueryUI.UI.prototype.mouse = function (optionLiteral, optionName, optionValue) {};
/** @param {string} optionLiteral
  * @param optionValue
  */
JQueryUI.UI.prototype.mouse = function (optionLiteral, optionValue) {};
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.Accordion}
  */
JQueryUI.UI.prototype.accordion = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.Autocomplete}
  */
JQueryUI.UI.prototype.autocomplete = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.Button}
  */
JQueryUI.UI.prototype.button = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.Button}
  */
JQueryUI.UI.prototype.buttonset = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.Datepicker}
  */
JQueryUI.UI.prototype.datepicker = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.Dialog}
  */
JQueryUI.UI.prototype.dialog = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.KeyCode}
  */
JQueryUI.UI.prototype.keyCode = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.Menu}
  */
JQueryUI.UI.prototype.menu = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.Progressbar}
  */
JQueryUI.UI.prototype.progressbar = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.Slider}
  */
JQueryUI.UI.prototype.slider = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.Spinner}
  */
JQueryUI.UI.prototype.spinner = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.Tabs}
  */
JQueryUI.UI.prototype.tabs = null;
/** <p>undefined</p>
  * @field 
  * @type {JQueryUI.Tooltip}
  */
JQueryUI.UI.prototype.tooltip = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
JQueryUI.UI.prototype.version = null;
/** @interface WidgetOptions
  * @namespace JQueryUI
  */
JQueryUI.WidgetOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
JQueryUI.WidgetOptions.prototype.disabled = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.WidgetOptions.prototype.hide = null;
/** <p>undefined</p>
  * @field
  */
JQueryUI.WidgetOptions.prototype.show = null;
/** @interface Widget
  * @namespace JQueryUI
  */
JQueryUI.Widget = function () {};
typeof JQueryUI.Widget != 'undefined' && $.extend(JQueryUI.Accordion.prototype, JQueryUI.Widget.prototype);
typeof JQueryUI.AccordionOptions != 'undefined' && $.extend(JQueryUI.Accordion.prototype, JQueryUI.AccordionOptions.prototype);
typeof JQueryUI.AccordionEvents != 'undefined' && $.extend(JQueryUI.Accordion.prototype, JQueryUI.AccordionEvents.prototype);
typeof JQueryUI.Widget != 'undefined' && $.extend(JQueryUI.Autocomplete.prototype, JQueryUI.Widget.prototype);
typeof JQueryUI.AutocompleteOptions != 'undefined' && $.extend(JQueryUI.Autocomplete.prototype, JQueryUI.AutocompleteOptions.prototype);
typeof JQueryUI.AutocompleteEvents != 'undefined' && $.extend(JQueryUI.Autocomplete.prototype, JQueryUI.AutocompleteEvents.prototype);
typeof JQueryUI.Widget != 'undefined' && $.extend(JQueryUI.Button.prototype, JQueryUI.Widget.prototype);
typeof JQueryUI.ButtonOptions != 'undefined' && $.extend(JQueryUI.Button.prototype, JQueryUI.ButtonOptions.prototype);
typeof JQueryUI.Widget != 'undefined' && $.extend(JQueryUI.Datepicker.prototype, JQueryUI.Widget.prototype);
typeof JQueryUI.DatepickerOptions != 'undefined' && $.extend(JQueryUI.Datepicker.prototype, JQueryUI.DatepickerOptions.prototype);
typeof JQueryUI.Widget != 'undefined' && $.extend(JQueryUI.Dialog.prototype, JQueryUI.Widget.prototype);
typeof JQueryUI.DialogOptions != 'undefined' && $.extend(JQueryUI.Dialog.prototype, JQueryUI.DialogOptions.prototype);
typeof JQueryUI.DialogEvents != 'undefined' && $.extend(JQueryUI.Dialog.prototype, JQueryUI.DialogEvents.prototype);
typeof JQueryUI.Widget != 'undefined' && $.extend(JQueryUI.Draggable.prototype, JQueryUI.Widget.prototype);
typeof JQueryUI.DraggableOptions != 'undefined' && $.extend(JQueryUI.Draggable.prototype, JQueryUI.DraggableOptions.prototype);
typeof JQueryUI.DraggableEvents != 'undefined' && $.extend(JQueryUI.Draggable.prototype, JQueryUI.DraggableEvents.prototype);
typeof JQueryUI.Widget != 'undefined' && $.extend(JQueryUI.Droppable.prototype, JQueryUI.Widget.prototype);
typeof JQueryUI.DroppableOptions != 'undefined' && $.extend(JQueryUI.Droppable.prototype, JQueryUI.DroppableOptions.prototype);
typeof JQueryUI.DroppableEvents != 'undefined' && $.extend(JQueryUI.Droppable.prototype, JQueryUI.DroppableEvents.prototype);
typeof JQueryUI.Widget != 'undefined' && $.extend(JQueryUI.Menu.prototype, JQueryUI.Widget.prototype);
typeof JQueryUI.MenuOptions != 'undefined' && $.extend(JQueryUI.Menu.prototype, JQueryUI.MenuOptions.prototype);
typeof JQueryUI.MenuEvents != 'undefined' && $.extend(JQueryUI.Menu.prototype, JQueryUI.MenuEvents.prototype);
typeof JQueryUI.Widget != 'undefined' && $.extend(JQueryUI.Progressbar.prototype, JQueryUI.Widget.prototype);
typeof JQueryUI.ProgressbarOptions != 'undefined' && $.extend(JQueryUI.Progressbar.prototype, JQueryUI.ProgressbarOptions.prototype);
typeof JQueryUI.ProgressbarEvents != 'undefined' && $.extend(JQueryUI.Progressbar.prototype, JQueryUI.ProgressbarEvents.prototype);
typeof JQueryUI.Widget != 'undefined' && $.extend(JQueryUI.Resizable.prototype, JQueryUI.Widget.prototype);
typeof JQueryUI.ResizableOptions != 'undefined' && $.extend(JQueryUI.Resizable.prototype, JQueryUI.ResizableOptions.prototype);
typeof JQueryUI.ResizableEvents != 'undefined' && $.extend(JQueryUI.Resizable.prototype, JQueryUI.ResizableEvents.prototype);
typeof JQueryUI.Widget != 'undefined' && $.extend(JQueryUI.Selectable.prototype, JQueryUI.Widget.prototype);
typeof JQueryUI.SelectableOptions != 'undefined' && $.extend(JQueryUI.Selectable.prototype, JQueryUI.SelectableOptions.prototype);
typeof JQueryUI.SelectableEvents != 'undefined' && $.extend(JQueryUI.Selectable.prototype, JQueryUI.SelectableEvents.prototype);
typeof JQueryUI.Widget != 'undefined' && $.extend(JQueryUI.Slider.prototype, JQueryUI.Widget.prototype);
typeof JQueryUI.SliderOptions != 'undefined' && $.extend(JQueryUI.Slider.prototype, JQueryUI.SliderOptions.prototype);
typeof JQueryUI.SliderEvents != 'undefined' && $.extend(JQueryUI.Slider.prototype, JQueryUI.SliderEvents.prototype);
typeof JQueryUI.SortableEvents != 'undefined' && $.extend(JQueryUI.SortableOptions.prototype, JQueryUI.SortableEvents.prototype);
typeof JQueryUI.Widget != 'undefined' && $.extend(JQueryUI.Sortable.prototype, JQueryUI.Widget.prototype);
typeof JQueryUI.SortableOptions != 'undefined' && $.extend(JQueryUI.Sortable.prototype, JQueryUI.SortableOptions.prototype);
typeof JQueryUI.SortableEvents != 'undefined' && $.extend(JQueryUI.Sortable.prototype, JQueryUI.SortableEvents.prototype);
typeof JQueryUI.Widget != 'undefined' && $.extend(JQueryUI.Spinner.prototype, JQueryUI.Widget.prototype);
typeof JQueryUI.SpinnerOptions != 'undefined' && $.extend(JQueryUI.Spinner.prototype, JQueryUI.SpinnerOptions.prototype);
typeof JQueryUI.SpinnerEvents != 'undefined' && $.extend(JQueryUI.Spinner.prototype, JQueryUI.SpinnerEvents.prototype);
typeof JQueryUI.Widget != 'undefined' && $.extend(JQueryUI.Tabs.prototype, JQueryUI.Widget.prototype);
typeof JQueryUI.TabsOptions != 'undefined' && $.extend(JQueryUI.Tabs.prototype, JQueryUI.TabsOptions.prototype);
typeof JQueryUI.TabsEvents != 'undefined' && $.extend(JQueryUI.Tabs.prototype, JQueryUI.TabsEvents.prototype);
typeof JQueryUI.Widget != 'undefined' && $.extend(JQueryUI.Tooltip.prototype, JQueryUI.Widget.prototype);
typeof JQueryUI.TooltipOptions != 'undefined' && $.extend(JQueryUI.Tooltip.prototype, JQueryUI.TooltipOptions.prototype);
typeof JQueryUI.TooltipEvents != 'undefined' && $.extend(JQueryUI.Tooltip.prototype, JQueryUI.TooltipEvents.prototype);
})()
