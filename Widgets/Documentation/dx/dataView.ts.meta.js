var wijmo = wijmo || {};

(function () {
wijmo.data = wijmo.data || {};

(function () {
/** Contains properties to specify filter, sort and paging.
  * @interface IShape
  * @namespace wijmo.data
  */
wijmo.data.IShape = function () {};
/** <p>The filter to apply to a data view. 
  * See wijmo.data.IDataView.filter for details.</p>
  * @field
  */
wijmo.data.IShape.prototype.filter = null;
/** <p>The sort to apply to a data view. 
  * See wijmo.data.IDataView.sort for details.</p>
  * @field
  */
wijmo.data.IShape.prototype.sort = null;
/** <p>The page index to set in a data view.
  * See wijmo.data.IPagedDataView.pageIndex for details.</p>
  * @field 
  * @type {number}
  */
wijmo.data.IShape.prototype.pageIndex = null;
/** <p>The page size to set in a data view.
  * See wijmo.data.IPagedDataView.pageSize for details.</p>
  * @field 
  * @type {number}
  */
wijmo.data.IShape.prototype.pageSize = null;
/** Defines sorting by one property.
  * @interface ISortDescriptor
  * @namespace wijmo.data
  */
wijmo.data.ISortDescriptor = function () {};
/** <p>Name of the property to sort by.</p>
  * @field 
  * @type {string}
  */
wijmo.data.ISortDescriptor.prototype.property = null;
/** <p>A value indicating whether sorting must be ascending or descending.</p>
  * @field 
  * @type {boolean}
  */
wijmo.data.ISortDescriptor.prototype.asc = null;
/** Defines a filter for one property.
  * @interface IFilterDescriptor
  * @namespace wijmo.data
  */
wijmo.data.IFilterDescriptor = function () {};
/** <p>A name of a built-in operator or an instance of IFilterOperator interface.</p>
  * @field 
  * @remarks
  * Names of built-in operators: 
  * ==, equals
  * !=, notequal
  * doesnotcontain, notcontain
  * &gt;, greater
  * &lt;, less
  * &gt;=, greaterorequal
  * &gt;=, lessorequal
  * isnotempty, notisempty
  * isnotnull, notisnull
  */
wijmo.data.IFilterDescriptor.prototype.operator = null;
/** <p>A filter operand, not used if the operator has an arity of 1.</p>
  * @field
  */
wijmo.data.IFilterDescriptor.prototype.value = null;
/** Defines a filter operator that can be used as a IFilterDescriptor.operator property value.
  * @interface IFilterOperator
  * @namespace wijmo.data
  */
wijmo.data.IFilterOperator = function () {};
/** <p>The number of operands the operator accepts. Support values: 1 and 2.</p>
  * @field 
  * @type {number}
  */
wijmo.data.IFilterOperator.prototype.arity = null;
/** <p>Apply the operator to value(s)</p>
  * @field 
  * @type {function}
  */
wijmo.data.IFilterOperator.prototype.apply = null;
/** <p>Dispaly name of the opreator</p>
  * @field 
  * @type {string}
  */
wijmo.data.IFilterOperator.prototype.displayName = null;
/** Defines a property of an IDataView element.
  * @interface IPropertyDescriptor
  * @namespace wijmo.data
  */
wijmo.data.IPropertyDescriptor = function () {};
/** <p>Property name.</p>
  * @field 
  * @type {string}
  */
wijmo.data.IPropertyDescriptor.prototype.name = null;
/** <p>Property value type.</p>
  * @field 
  * @type {string}
  */
wijmo.data.IPropertyDescriptor.prototype.type = null;
/** A result of IDataView refresh operation, implements a jQuery promise.
  * @interface IRefreshResult
  * @namespace wijmo.data
  * @extends JQueryPromise
  */
wijmo.data.IRefreshResult = function () {};
/** Infrastructure.
  * @interface ICloneable
  * @namespace wijmo.data
  */
wijmo.data.ICloneable = function () {};
/** Provides filtering, sorting and loading of a collection.
  * @interface IDataView
  * @namespace wijmo.data
  * @extends wijmo.IDisposable
  * @extends wijmo.data.ISubscribable
  */
wijmo.data.IDataView = function () {};
/** Returns the number of items in the current view.
  * @returns {number}
  */
wijmo.data.IDataView.prototype.count = function () {};
/** Returns an element in the view by index.
  * @param {number} index The zero-based index of the element to get
  * @remarks
  * Throws an exception if the index is out of range
  */
wijmo.data.IDataView.prototype.item = function (index) {};
/** Returns properties for elements in the view
  * @returns {wijmo.data.IPropertyDescriptor[]}
  */
wijmo.data.IDataView.prototype.getProperties = function () {};
/** Returns the current value of the property in the element at the specified index.
  * @param {number} index The zero-based index of the element with the property value that is to be read.
  * @param {string} property The name of the property to read
  * @returns The current value of the property in the element
  */
wijmo.data.IDataView.prototype.getProperty = function (index, property) {};
/** Returns the current value of the property in the element.
  * @param {Object} item The element with the property value that is to be read.
  * @param {string} property The name of the property to read
  * @returns The current value of the property in the element
  */
wijmo.data.IDataView.prototype.getProperty = function (item, property) {};
/** Sets the value of the property in the element at the specified index.
  * @param {number} index The zero-based index of the element with the property value that is to be set.
  * @param {string} property The name of the property to set
  * @param newValue The new value
  * @returns {void}
  * @remarks
  * If the class implementing IDataView also implements IEditableDataView and the element is being edited, 
  * then the property value change is done within the editing transaction.
  */
wijmo.data.IDataView.prototype.setProperty = function (index, property, newValue) {};
/** Sets the value of the property in the element.
  * @param {Object} item The element with the property value that is to be set.
  * @param {string} property The name of the property to set
  * @param newValue The new value
  * @returns {void}
  * @remarks
  * If the class implementing IDataView also implements IEditableDataView and the element is being edited, 
  * then the property value change is done within the editing transaction.
  */
wijmo.data.IDataView.prototype.setProperty = function (item, property, newValue) {};
/** Returns a value that indicates whether the data view supports filtering
  * @returns {bool}
  */
wijmo.data.IDataView.prototype.canFilter = function () {};
/** Returns a value that indicates whether the data view supports sorting
  * @returns {bool}
  */
wijmo.data.IDataView.prototype.canSort = function () {};
/** Reloads the data view.
  * @param {IShape} shape Settings for filtering, sorting and/or paging to be applied before loading.
  * In contrast to filter/sort properties in IDataView, 
  * the format of properties in the shape parameter does not have to follow the specs defined in IDataView.
  * For instance, BreezeDataView accepts a Breeze.Predicate as shape.filter.
  * However the IDataView properties must not violate the IDataView specs. If a custom filter format cannot be aligned to the IDataView specs,
  * then the properties values must be functions or null.
  * @param {bool} local Prevents server requests and applies filtering/sorting/paging of the source array on the client-side.
  * @returns {IRefreshResult} A promise object that can be used to receive notification of asynchronous refresh completion or failure.
  * @remarks
  * Depending on the implementation, data can be loaded from a local or a remote data source. 
  * The implementation transaltes filtering/sorting/paging defined in the IDataView to the data provider specific format.
  * If it is possible, then the data is reshaped on the client-side.
  * The optional shape parameter can be used to change multiple filtering/sorting/paging settings at a time to avoid unnecessary refreshes.
  * During the refresh, the isLoading property value must be true. After a successful refresh, the isLoaded property value must be true.
  * When the method is called, the pending refresh, if any, is canceled.
  */
wijmo.data.IDataView.prototype.refresh = function (shape, local) {};
/** Cancels the ongoing refresh operation */
wijmo.data.IDataView.prototype.cancelRefresh = function () {};
/** Returns the element array before applying client-side filtering/sorting/paging.
  * @returns {any[]}
  * @remarks
  * In the case of a remote data source, usually it is the array returned by a server.
  */
wijmo.data.IDataView.prototype.getSource = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.data.IMutableObservable.html'>wijmo.data.IMutableObservable</a></p>
  * <p>The filter to be applied to the data view. When assigned, the data view is refreshed.</p>
  * @field 
  * @type {wijmo.data.IMutableObservable}
  * @remarks
  * The following formats of a filter are defined by the IDataView interface:
  * 1) A predicate function. In this case the filtering is always done on the client-side
  * 2) A decomposable property-based filter or its short form described below. Can be used for server-side filtering.
  * Property based filter is a hash where a key matches an element property and a value may be one of these:
  * 1) An instance of FilterDescriptor interface
  * 2) A value, so only elements with the same property values satisfy a predicate
  * Examples:
  * studentView.filter({
  * name: "Alfred",
  * age: { operator: "&gt;", value: "10" }
  * });
  */
wijmo.data.IDataView.prototype.filter = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.data.IMutableObservable.html'>wijmo.data.IMutableObservable</a></p>
  * <p>An observable property that gets or sets the sort to be applied to the data view.
  * When assigned, the data view is refreshed.</p>
  * @field 
  * @type {wijmo.data.IMutableObservable}
  * @remarks
  * The following formats of a sort are defined by the IDataView interface:
  * 1) A string, a comma-separated list of property names with optional " asc" or " desc" suffixes.
  * 2) An array of ISortDescriptor instances
  * 3) A comparison function of type (a, b) =&gt; number. In this case sorting and paging are always done on the client-side
  * Examples:
  * customerView.sort("firstName");
  * productView.sort("unitPrice desc, productName");
  */
wijmo.data.IDataView.prototype.sort = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.data.IObservable.html'>wijmo.data.IObservable</a></p>
  * <p>A value indicating whether the data view is being loaded</p>
  * @field 
  * @type {wijmo.data.IObservable}
  */
wijmo.data.IDataView.prototype.isLoading = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.data.IObservable.html'>wijmo.data.IObservable</a></p>
  * <p>A value indicating whether the data view is loaded</p>
  * @field 
  * @type {wijmo.data.IObservable}
  */
wijmo.data.IDataView.prototype.isLoaded = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.data.IMutableObservable.html'>wijmo.data.IMutableObservable</a></p>
  * <p>The current element index in the data view</p>
  * @field 
  * @type {wijmo.data.IMutableObservable}
  */
wijmo.data.IDataView.prototype.currentPosition = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.data.IMutableObservable.html'>wijmo.data.IMutableObservable</a></p>
  * <p>The current element in the data view</p>
  * @field 
  * @type {wijmo.data.IMutableObservable}
  */
wijmo.data.IDataView.prototype.currentItem = null;
/** Provides paging of a collection.
  * @interface IPagedDataView
  * @namespace wijmo.data
  * @extends wijmo.data.IDataView
  */
wijmo.data.IPagedDataView = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.data.IObservable.html'>wijmo.data.IObservable</a></p>
  * <p>The number of pages.</p>
  * @field 
  * @type {wijmo.data.IObservable}
  */
wijmo.data.IPagedDataView.prototype.pageCount = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.data.IObservable.html'>wijmo.data.IObservable</a></p>
  * <p>The number of elements before paging is applied.</p>
  * @field 
  * @type {wijmo.data.IObservable}
  */
wijmo.data.IPagedDataView.prototype.totalItemCount = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.data.IMutableObservable.html'>wijmo.data.IMutableObservable</a></p>
  * <p>The current page index.</p>
  * @field 
  * @type {wijmo.data.IMutableObservable}
  */
wijmo.data.IPagedDataView.prototype.pageIndex = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.data.IMutableObservable.html'>wijmo.data.IMutableObservable</a></p>
  * <p>The current page index.</p>
  * @field 
  * @type {wijmo.data.IMutableObservable}
  */
wijmo.data.IPagedDataView.prototype.pageSize = null;
/** Provides adding, modifying and removing of elements in a collection.
  * @interface IEditableDataView
  * @namespace wijmo.data
  * @extends wijmo.data.IDataView
  */
wijmo.data.IEditableDataView = function () {};
/** Returns a value indicating whether the element being edited is added by the add(item) or addNew(initialValues) methods.
  * @returns {bool}
  */
wijmo.data.IEditableDataView.prototype.isCurrentEditItemNew = function () {};
/** Returns a value that indicates whether the current changes can be committed
  * @returns {bool}
  */
wijmo.data.IEditableDataView.prototype.canCommitEdit = function () {};
/** Commits the changes made to the currently editing element
  * @returns {void}
  * @remarks
  * After a successful commit, the currentEditItem property value must be null.
  * If the item being edited was new, it is added to the underlying data source.
  * If the element does not satisfy the filter, then it is removed from the data view, but not from the data source.
  */
wijmo.data.IEditableDataView.prototype.commitEdit = function () {};
/** Returns a value that indicates whether the current changes can be canceled.
  * @returns {bool}
  */
wijmo.data.IEditableDataView.prototype.canCancelEdit = function () {};
/** Cancels the changes made to the currently editing element since the editing was started.
  * @returns {void}
  * @remarks
  * After a successful cancelation, the currentEditItem property value must be null.
  */
wijmo.data.IEditableDataView.prototype.cancelEdit = function () {};
/** Starts editing of an element at the index.
  * @param {number} index The zero-based index of the element to edit.
  * @remarks
  * Commits the element currently being edited, if any.
  */
wijmo.data.IEditableDataView.prototype.editItem = function (index) {};
/** Starts editing of the element.
  * @param {Object} element The element to edit. Defaults to the value of IDataView.currentItem property.
  * @remarks
  * Commits the element currently being edited, if any.
  */
wijmo.data.IEditableDataView.prototype.editItem = function (element) {};
/** Returns a value that indicates whether the add(item) method is implemented.
  * @returns {bool}
  */
wijmo.data.IEditableDataView.prototype.canAdd = function () {};
/** Adds a new element and marks it as being edited.
  * @param {Object} item The item to add.
  * @remarks
  * Adds a new element to the data view, but not to the underlying data source.
  * Commits the element currently being edited, if any.
  */
wijmo.data.IEditableDataView.prototype.add = function (item) {};
/** Returns a value that indicates whether the addNew() method is implemented.
  * @returns {bool}
  */
wijmo.data.IEditableDataView.prototype.canAddNew = function () {};
/** Creates, adds a new element and marks it as being edited.
  * @param {Object} initialValue Optional hash of property values for the a element.
  * @remarks
  * Works the same way as add(item), but the new item is created by the data view.
  */
wijmo.data.IEditableDataView.prototype.addNew = function (initialValue) {};
/** Returns a value that indicates whether the remove method overloads are implemented.
  * @returns {bool}
  */
wijmo.data.IEditableDataView.prototype.canRemove = function () {};
/** Removes an element at the specified index.
  * @param {number} index The zero-based index of the element to remove
  * @returns {void}
  */
wijmo.data.IEditableDataView.prototype.remove = function (index) {};
/** Removes an element.
  * @param {Object} item The element to remove. Defaults to the value of the IDataView.currentItem property.
  * @returns {bool} True if an element was removed.
  */
wijmo.data.IEditableDataView.prototype.remove = function (item) {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.data.IObservable.html'>wijmo.data.IObservable</a></p>
  * <p>The element currently being edited.</p>
  * @field 
  * @type {wijmo.data.IObservable}
  * @remarks
  * There may be only one element being edited at time.
  */
wijmo.data.IEditableDataView.prototype.currentEditItem = null;
/** A function that creates an IDataView for a data source if possible.
  * @interface IDataViewFactory
  * @namespace wijmo.data
  */
wijmo.data.IDataViewFactory = function () {};
/** Registers a new IDataView provider.
  * @param {IDataViewFactory} factory A function that creates a IDataView for a data source if possible. Otherwise returns null.
  * @returns An IDisposable that can be used to remove the registration.
  * @remarks
  * Use this method to provide your own IDataView implementation for a specific data source. See wijmo.data.breeze.ts for an example.
  */
wijmo.data.registerDataViewFactory = function (factory) {};
/** Creates an IDataView for a data source.
  * @param src A data source, can be anything that is supported by the registered IDataView providers
  * @returns {wijmo.data.IDataView} An IDataView instance for the data source.
  */
wijmo.data.asDataView = function (src) {};
/** Returns true if the view parameter is a IDataView
  * @param {wijmo.data.IDataView} view
  * @returns {bool}
  */
wijmo.data.isDataView = function (view) {};
typeof JQueryPromise != 'undefined' && $.extend(wijmo.data.IRefreshResult.prototype, JQueryPromise.prototype);
typeof wijmo.IDisposable != 'undefined' && $.extend(wijmo.data.IDataView.prototype, wijmo.IDisposable.prototype);
typeof wijmo.data.ISubscribable != 'undefined' && $.extend(wijmo.data.IDataView.prototype, wijmo.data.ISubscribable.prototype);
typeof wijmo.data.IDataView != 'undefined' && $.extend(wijmo.data.IPagedDataView.prototype, wijmo.data.IDataView.prototype);
typeof wijmo.data.IDataView != 'undefined' && $.extend(wijmo.data.IEditableDataView.prototype, wijmo.data.IDataView.prototype);
})()
})()
