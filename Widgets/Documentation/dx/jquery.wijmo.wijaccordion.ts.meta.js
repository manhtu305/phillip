var wijmo = wijmo || {};

(function () {
wijmo.accordion = wijmo.accordion || {};

(function () {
/** Define the layout setting properties.
  * @interface layoutSetting
  * @namespace wijmo.accordion
  */
wijmo.accordion.layoutSetting = function () {};
/** <p>Gets or sets the top padding of layoutSetting.</p>
  * @field 
  * @type {string}
  */
wijmo.accordion.layoutSetting.prototype.paddingTop = null;
/** <p>Gets or sets the bottom padding of layoutSetting.</p>
  * @field 
  * @type {string}
  */
wijmo.accordion.layoutSetting.prototype.paddingBottom = null;
/** <p>Gets or sets the left padding of layoutSetting.</p>
  * @field 
  * @type {string}
  */
wijmo.accordion.layoutSetting.prototype.paddingLeft = null;
/** <p>Gets or sets the right padding of layoutSetting.</p>
  * @field 
  * @type {string}
  */
wijmo.accordion.layoutSetting.prototype.paddingRight = null;
/** <p>Gets or sets the width of layoutSetting.</p>
  * @field 
  * @type {number}
  */
wijmo.accordion.layoutSetting.prototype.width = null;
/** Define the animation setting properties.
  * @interface animateOptions
  * @namespace wijmo.accordion
  */
wijmo.accordion.animateOptions = function () {};
/** <p>Gets the wijaccordion pane to show.</p>
  * @field 
  * @type {JQuery}
  */
wijmo.accordion.animateOptions.prototype.toShow = null;
/** <p>Gets the wijaccordion pane to hide.</p>
  * @field 
  * @type {JQuery}
  */
wijmo.accordion.animateOptions.prototype.toHide = null;
/** <p>Gets or sets whether animation is horizontal.</p>
  * @field 
  * @type {boolean}
  */
wijmo.accordion.animateOptions.prototype.horizontal = null;
/** <p>Gets or sets the duration for animation.</p>
  * @field 
  * @type {number}
  */
wijmo.accordion.animateOptions.prototype.duration = null;
/** <p>Gets or sets the easing for animation.</p>
  * @field 
  * @type {string}
  * @remarks
  * Valid values are all animations available in jQuery UI.
  */
wijmo.accordion.animateOptions.prototype.easing = null;
/** <p>Gets or sets whether accordion is animated from right to left.</p>
  * @field 
  * @type {boolean}
  */
wijmo.accordion.animateOptions.prototype.rightToLeft = null;
/** <p>Gets whether animated downside.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * It returns true if next index is greater than previous index
  */
wijmo.accordion.animateOptions.prototype.down = null;
/** <p>Gets or sets whether wijaccordion pane is autoHeight.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * When set to true, width is autosized if horizontal and otherwise height is autosized.
  */
wijmo.accordion.animateOptions.prototype.autoHeight = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.accordion.layoutSetting.html'>wijmo.accordion.layoutSetting</a></p>
  * <p>Gets or sets the default layout setting.</p>
  * @field 
  * @type {wijmo.accordion.layoutSetting}
  */
wijmo.accordion.animateOptions.prototype.defaultLayoutSetting = null;
/** @class wijaccordion
  * @widget 
  * @namespace jQuery.wijmo.accordion
  * @extends wijmo.wijmoWidget
  */
wijmo.accordion.wijaccordion = function () {};
wijmo.accordion.wijaccordion.prototype = new wijmo.wijmoWidget();
/** Remove the functionality completely. This will return the element back to its pre-init state.
  * @returns {void}
  */
wijmo.accordion.wijaccordion.prototype.destroy = function () {};
/** Refresh the accordion.
  * @returns {void}
  */
wijmo.accordion.wijaccordion.prototype.refresh = function () {};
/** Activates the accordion content pane at the specified index.
  * @param {number} index The zero-based index of the accordion pane to activate.
  * @returns {bool}
  * @remarks
  * You can use code like in the example below inside your document ready function
  * to activate the specified pane using the click event of a button.
  */
wijmo.accordion.wijaccordion.prototype.activate = function (index) {};
/** @param {string} index
  * @returns {bool}
  */
wijmo.accordion.wijaccordion.prototype.activate = function (index) {};
/** @param {EventTarget} index
  * @returns {bool}
  */
wijmo.accordion.wijaccordion.prototype.activate = function (index) {};
/** @param index
  * @returns {bool}
  */
wijmo.accordion.wijaccordion.prototype.activate = function (index) {};

/** @class */
var wijaccordion_options = function () {};
/** <p class='defaultValue'>Default value: 'slide'</p>
  * <p>Sets the animation easing effect that users experience when they switch
  * between panes.</p>
  * @field 
  * @type {string|function}
  * @option 
  * @remarks
  * Set this option to false in order to disable easing. This results in a plain, abrupt shift 
  * from one pane to the next. You can also create custom easing animations using jQuery UI Easings
  * Options available for the animation function include:
  * down - If true, indicates that the index of the pane should be expanded higher than the index 
  * of the pane that must be collapsed.
  * horizontal - If true, indicates that the accordion have a horizontal 
  * orientation (when the expandDirection is left or right).
  * rightToLeft - If true, indicates that the content element is located 
  * before the header element (top and left expand direction).
  * toShow - jQuery object that contains the content element(s) should be shown.
  * toHide - jQuery object that contains the content element(s) should be hidden.
  * @example
  * //Create your own animation:
  * jQuery.wijmo.wijaccordion.animations.custom1 = function (options) {
  *     this.slide(options, {
  *     easing: options.down ? "easeOutBounce" : "swing",
  *     duration: options.down ? 1000 : 200
  *   });
  * }
  *  $("#accordion3").wijaccordion({
  *      expandDirection: "right",
  *      animated: "custom1"
  *  });
  */
wijaccordion_options.prototype.animated = 'slide';
/** <p class='defaultValue'>Default value: null</p>
  * <p>The animation duration in milliseconds.</p>
  * @field 
  * @type
  * {{number|function}
  * By default, the animation duration value depends on an animation effect specified
  * by the animation option.}
  * @option
  */
wijaccordion_options.prototype.duration = null;
/** <p class='defaultValue'>Default value: 'click'</p>
  * <p>Determines the event that triggers the accordion to change panes.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * To select multiple events, separate them by a space. Supported events include:
  * focus -- The pane opens when you click its header.
  * click (default) -- The pane opens when you click its header.
  * dblclick -- The pane opens when you double-click its header.
  * mousedown -- The pane opens when you press the mouse button over its header.
  * mouseup -- The pane opens when you release the mouse button over its header.
  * mousemove -- The pane opens when you move the mouse pointer into its header.
  * mouseover -- The pane opens when you hover the mouse pointer over its header.
  * mouseout -- The pane opens when the mouse pointer leaves its header.
  * mouseenter -- The pane opens when the mouse pointer enters its header.
  * mouseleave -- The pane opens when the mouse pointer leaves its header.
  * select -- The pane opens when you select its header by clicking and then pressing Enter
  * submit -- The pane opens when you select its header by clicking and then pressing Enter.
  * keydown -- The pane opens when you select its header by clicking and then pressing any key.
  * keypress -- The pane opens when you select its header by clicking and then pressing any key.
  * keyup -- The pane opens when you select its header by clicking and then pressing and releasing any key.
  */
wijaccordion_options.prototype.event = 'click';
/** <p class='defaultValue'>Default value: 'bottom'</p>
  * <p>Determines the direction in which the content area of the control expands.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Available values include: top, right, bottom, and left.
  */
wijaccordion_options.prototype.expandDirection = 'bottom';
/** <p class='defaultValue'>Default value: '&gt; li &gt; :first-child,&gt; :not(li):even'</p>
  * <p>Determines the selector for the header element.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Set this option to put header and content elements inside the HTML tags of your choice.
  * By default, the header is the first child after an &lt;LI&gt; element, and the content is 
  * the second child html markup.
  */
wijaccordion_options.prototype.header = '> li > :first-child,> :not(li):even';
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether clicking a header closes the current pane before opening the new one.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * Setting this value to false causes the headers to act as toggles for opening and
  * closing the panes, leaving all previously clicked panes open until you click them again.
  */
wijaccordion_options.prototype.requireOpenedPane = true;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Gets or sets the zero-based index of the accordion pane to show expanded initially.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * By default, the first pane is expanded. A setting of -1 specifies that no pane
  * is expanded initially, if you also set the requireOpenedPane option to false.
  */
wijaccordion_options.prototype.selectedIndex = 0;
/** Occurs before an active accordion pane change.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.accordion.IBeforeSelectedIndexChangedEventArgs} data Information about an event
  * @remarks
  * Return false or call event.preventDefault() in order to cancel event and
  * prevent the selectedIndex change.
  */
wijaccordion_options.prototype.beforeSelectedIndexChanged = null;
/** Occurs when an active accordion pane changed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.accordion.ISelectedIndexChangedEventArgs} data Information about an event
  */
wijaccordion_options.prototype.selectedIndexChanged = null;
wijmo.accordion.wijaccordion.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijaccordion_options());
$.widget("wijmo.wijaccordion", $.wijmo.widget, wijmo.accordion.wijaccordion.prototype);
/** wijaccordion options definition
  * @interface WijAccordionOptions
  * @namespace wijmo.accordion
  * @extends wijmo.WidgetOptions
  */
wijmo.accordion.WijAccordionOptions = function () {};
/** <p>Sets the animation easing effect that users experience when they switch
  * between panes.</p>
  * @field 
  * @type {string|function}
  * @remarks
  * Set this option to false in order to disable easing. This results in a plain, abrupt shift 
  * from one pane to the next. You can also create custom easing animations using jQuery UI Easings
  * Options available for the animation function include:
  * down - If true, indicates that the index of the pane should be expanded higher than the index 
  * of the pane that must be collapsed.
  * horizontal - If true, indicates that the accordion have a horizontal 
  * orientation (when the expandDirection is left or right).
  * rightToLeft - If true, indicates that the content element is located 
  * before the header element (top and left expand direction).
  * toShow - jQuery object that contains the content element(s) should be shown.
  * toHide - jQuery object that contains the content element(s) should be hidden.
  * @example
  * //Create your own animation:
  * jQuery.wijmo.wijaccordion.animations.custom1 = function (options) {
  *     this.slide(options, {
  *     easing: options.down ? "easeOutBounce" : "swing",
  *     duration: options.down ? 1000 : 200
  *   });
  * }
  *  $("#accordion3").wijaccordion({
  *      expandDirection: "right",
  *      animated: "custom1"
  *  });
  */
wijmo.accordion.WijAccordionOptions.prototype.animated = null;
/** <p>The animation duration in milliseconds.</p>
  * @field 
  * @type
  * {{number|function}
  * By default, the animation duration value depends on an animation effect specified
  * by the animation option.}
  */
wijmo.accordion.WijAccordionOptions.prototype.duration = null;
/** <p>Determines the event that triggers the accordion to change panes.</p>
  * @field 
  * @type {string}
  * @remarks
  * To select multiple events, separate them by a space. Supported events include:
  * focus -- The pane opens when you click its header.
  * click (default) -- The pane opens when you click its header.
  * dblclick -- The pane opens when you double-click its header.
  * mousedown -- The pane opens when you press the mouse button over its header.
  * mouseup -- The pane opens when you release the mouse button over its header.
  * mousemove -- The pane opens when you move the mouse pointer into its header.
  * mouseover -- The pane opens when you hover the mouse pointer over its header.
  * mouseout -- The pane opens when the mouse pointer leaves its header.
  * mouseenter -- The pane opens when the mouse pointer enters its header.
  * mouseleave -- The pane opens when the mouse pointer leaves its header.
  * select -- The pane opens when you select its header by clicking and then pressing Enter
  * submit -- The pane opens when you select its header by clicking and then pressing Enter.
  * keydown -- The pane opens when you select its header by clicking and then pressing any key.
  * keypress -- The pane opens when you select its header by clicking and then pressing any key.
  * keyup -- The pane opens when you select its header by clicking and then pressing and releasing any key.
  */
wijmo.accordion.WijAccordionOptions.prototype.event = null;
/** <p>Determines the direction in which the content area of the control expands.</p>
  * @field 
  * @type {string}
  * @remarks
  * Available values include: top, right, bottom, and left.
  */
wijmo.accordion.WijAccordionOptions.prototype.expandDirection = null;
/** <p>Determines the selector for the header element.</p>
  * @field 
  * @type {string}
  * @remarks
  * Set this option to put header and content elements inside the HTML tags of your choice.
  * By default, the header is the first child after an &lt;LI&gt; element, and the content is 
  * the second child html markup.
  */
wijmo.accordion.WijAccordionOptions.prototype.header = null;
/** <p>Determines whether clicking a header closes the current pane before opening the new one.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * Setting this value to false causes the headers to act as toggles for opening and
  * closing the panes, leaving all previously clicked panes open until you click them again.
  */
wijmo.accordion.WijAccordionOptions.prototype.requireOpenedPane = null;
/** <p>Gets or sets the zero-based index of the accordion pane to show expanded initially.</p>
  * @field 
  * @type {number}
  * @remarks
  * By default, the first pane is expanded. A setting of -1 specifies that no pane
  * is expanded initially, if you also set the requireOpenedPane option to false.
  */
wijmo.accordion.WijAccordionOptions.prototype.selectedIndex = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.accordion.IWijAccordionEvent.html'>wijmo.accordion.IWijAccordionEvent</a></p>
  * <p>Occurs before an active accordion pane change.</p>
  * @field 
  * @type {wijmo.accordion.IWijAccordionEvent}
  * @remarks
  * Return false or call event.preventDefault() in order to cancel event and
  * prevent the selectedIndex change.
  */
wijmo.accordion.WijAccordionOptions.prototype.beforeSelectedIndexChanged = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.accordion.IWijAccordionEvent.html'>wijmo.accordion.IWijAccordionEvent</a></p>
  * <p>Occurs when an active accordion pane changed.</p>
  * @field 
  * @type {wijmo.accordion.IWijAccordionEvent}
  */
wijmo.accordion.WijAccordionOptions.prototype.selectedIndexChanged = null;
/** <p>Gets or sets whether wijaccordion pane is autoHeight.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * When set to true, width is autosized if horizontal and otherwise height is autosized.
  */
wijmo.accordion.WijAccordionOptions.prototype.autoHeight = null;
/** This event argument signature is used in beforeSelectedIndexChanged and selectedIndexChanged event.
  * @interface IWijAccordionEventArgs
  * @namespace wijmo.accordion
  */
wijmo.accordion.IWijAccordionEventArgs = function () {};
/** <p>Gets next wijaccordion pane index.</p>
  * @field 
  * @type {number}
  */
wijmo.accordion.IWijAccordionEventArgs.prototype.newIndex = null;
/** <p>Gets previous wijaccordion pane index.</p>
  * @field 
  * @type {number}
  */
wijmo.accordion.IWijAccordionEventArgs.prototype.prevIndex = null;
/** This event definition signature is used in beforeSelectedIndexChanged and selectedIndexChanged event.
  * @interface IWijAccordionEvent
  * @namespace wijmo.accordion
  */
wijmo.accordion.IWijAccordionEvent = function () {};
/** Contains information about wijaccordion.beforeSelectedIndexChanged event
  * @interface IBeforeSelectedIndexChangedEventArgs
  * @namespace wijmo.accordion
  */
wijmo.accordion.IBeforeSelectedIndexChangedEventArgs = function () {};
/** <p>Index of a pane that will be expanded.</p>
  * @field 
  * @type {number}
  */
wijmo.accordion.IBeforeSelectedIndexChangedEventArgs.prototype.newIndex = null;
/** <p>Index of a pane that will be collapsed.</p>
  * @field 
  * @type {number}
  */
wijmo.accordion.IBeforeSelectedIndexChangedEventArgs.prototype.prevIndex = null;
/** Contains information about wijaccordion.selectedIndexChanged event
  * @interface ISelectedIndexChangedEventArgs
  * @namespace wijmo.accordion
  */
wijmo.accordion.ISelectedIndexChangedEventArgs = function () {};
/** <p>Index of the activated pane.</p>
  * @field 
  * @type {number}
  */
wijmo.accordion.ISelectedIndexChangedEventArgs.prototype.newIndex = null;
/** <p>Index of the collapsed pane.</p>
  * @field 
  * @type {number}
  */
wijmo.accordion.ISelectedIndexChangedEventArgs.prototype.prevIndex = null;
typeof wijmo.WidgetOptions != 'undefined' && $.extend(wijmo.accordion.WijAccordionOptions.prototype, wijmo.WidgetOptions.prototype);
})()
})()
