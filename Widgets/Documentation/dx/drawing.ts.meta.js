var wijmo = wijmo || {};

(function () {
wijmo.maps = wijmo.maps || {};

(function () {
/** Defines a point with horizontal and vertical coordinates.
  * @interface IPoint
  * @namespace wijmo.maps
  */
wijmo.maps.IPoint = function () {};
/** <p>The coordinate in horizontal.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.IPoint.prototype.x = null;
/** <p>The coordinate in vertical.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.IPoint.prototype.y = null;
/** Defines an ordered pair  of number, which specify a width and height.
  * @interface ISize
  * @namespace wijmo.maps
  */
wijmo.maps.ISize = function () {};
/** <p>The width of the size.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.ISize.prototype.width = null;
/** <p>The height of the size.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.ISize.prototype.height = null;
/** Defines a rectangle with location and size.
  * @interface IRectangle
  * @namespace wijmo.maps
  */
wijmo.maps.IRectangle = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IPoint.html'>wijmo.maps.IPoint</a></p>
  * <p>The upper-left point of the rectangle.</p>
  * @field 
  * @type {wijmo.maps.IPoint}
  */
wijmo.maps.IRectangle.prototype.location = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.ISize.html'>wijmo.maps.ISize</a></p>
  * <p>The size of rectangle.</p>
  * @field 
  * @type {wijmo.maps.ISize}
  */
wijmo.maps.IRectangle.prototype.size = null;
})()
})()
