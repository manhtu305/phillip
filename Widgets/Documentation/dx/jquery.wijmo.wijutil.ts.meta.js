var wijmo = wijmo || {};

(function () {
/** @returns {JQueryUI.KeyCode} */
wijmo.getKeyCodeEnum = function () {};
/** @param {string} classNames
  * @returns {string}
  */
wijmo.getCSSSelector = function (classNames) {};
/** @param val
  * @returns {string}
  */
wijmo.htmlEncode = function (val) {};
/** @param val
  * @returns {string}
  */
wijmo.htmlDecode = function (val) {};
})()
var wijmo = wijmo || {};

(function () {
wijmo.input = wijmo.input || {};

(function () {
/** @interface IFormatValidateLib
  * @namespace wijmo.input
  */
wijmo.input.IFormatValidateLib = function () {};
/** Format the specified value into a string as specified format type and options.
  * Internal, wijmo input use the same format library.
  * The format logic will according to the value type.
  * The valid type can be javascript Date type, javascript string type, javascript number.
  * If the value type is Date, the format library will use the logic of the wijinput date.
  *     The formatOrType is a string value, it can be a valid dateFormat value of wijinput date.
  *     The options is object, it can have four properties or null.
  *         amDesignator, Determines string designator for hours that are "ante meridiem" (before noon).
  *         culture, indicates the culture that the format library will use.
  *         hour12As0, a boolean value determines the range of hours that can be entered in the control.
  *         midnightAs0, a boolean value determines whether to express midnight as 24:00.
  *         pmDesignator， Determines the string designator for hours that are "post meridiem" (after noon).
  *     For more information about these options and formatOrType, please see wijinput date.
  *   
  * If the value type is string, the format library will use the logic of the wijinput mask.
  *     The formatOrType is a string, it can be a valid maskFormat value of the wijinput mask. The regex mask format is invalid.
  *     The options is object, it can have two properties
  *         autoConvert, indicate whether automatically converts to the proper format according to the format setting.
  *         culture, indicates the culture that the format library will use.
  *     For more information about these options and formatOrType, please see wijinput mask.
  * If the value type is Number, the format library will use the logic of the wijinput number.
  *     The formatOrType is a string value, it can be a valid type value of wijinput number.
  *     The options is object, it can have eight properties.
  *         currencySymbol, determine the current symbol when number type is currency. The default value is according to cul ture.
  *         culture, indicates the culture that the format library will use.
  *         decimalPlaces, indicates the number of decimal places to display.
  *         negativePrefix, determine the prefix string used for negative value. The default value is according to culture &amp; ty pe. 
  *         negativeSuffix, determine the suffix string used for negative value. The default value is according to culture &amp; ty pe. 
  *         positivePrefix, determine the prefix string used for positive value. The default value is according to culture &amp; ty pe. 
  *         positiveSuffix，determine the suffix striing used for positive value. The default value is according to culture &amp; t yp  e.
  *         showGroup，Indicates whether the thousands group separator will be inserted between each digital group 
  *                    (number of digits in thousands group depends on the selected Culture).
  *     For more information about these options and formatOrType, please see wijinput number.
  *  @example
  *     $.wijinputcore.format(new Date(2013,8,12));   // returns: "9/12/2013"
  *     $.wijinputcore.format(new Date(2013,8,12), "yyyy/MM/dd");  // returns: "2013/09/12"
  *     $.wijinputcore.format(new Date(2013,8,12)，{culture: "ja-JP"});   // returns: "2013/09/12"
  *     $.wijinputcore.format(new Date(2013,8,12,23,22,33), "tt hh:mm:ss", {culture: "ja-JP"});   // returns: "午後 11:22:33"
  *     $.wijinputcore.format(new Date(2013,8,12,24,12,12), "HH:mm:ss", {midnightAs0: true});   // returns: "00:12:12"
  *    
  *     $.wijinputcore.format("1234567", "９９９-９９９９", {autoConvert: true});   // returns: "１２３-４５６７"
  *     $.wijinputcore.format("1234567", "９９９-９９９９", {autoConvert: false});  //  returns: "       "
  *     
  *     $.wijinputcore.format(12345);   // returns: "12345.00"
  *     $.wijinputcore.format(12345, "numeric", {decimalPlaces:3});   // returns: "12345.000"
  *     $.wijinputcore.format(12345, "percent");   // returns: "12345.00 %"
  *     $.wijinputcore.format(12345, "currency");  // returns: "$12345.00"
  *     $.wijinputcore.format(12345, "currency", {culture: "ja-JP"});   // returns: "¥12345.00"
  *     $.wijinputcore.format(12345, "numeric", {positivePrefix: "+", negativePrefix: "-"});   // returns: "+12345.00"
  * @param value
  * @param {string} formatOrType
  * @param options
  * @returns {string}
  */
wijmo.input.IFormatValidateLib.prototype.format = function (value, formatOrType, options) {};
/** Validate the string value according to the specified format string.
  * @param {string} value }, the string value that will be validated.
  * @param {Date} minDate }, the minimum date.
  * @param {Date} maxDate }, the maximum date.
  * @param {string} format }, indicate the date format, for more information about the format keyword, please see wijinput date.
  * @param {string} culture }, indicates the culture that the validate library will use.
  * @returns {bool} , a boolean value indicates whether the string date value is the specified date range.
  * @example
  * $.wijinputcore.validateDate("12/3/2012", new Date(2013,0,1), new Date(2013,11,31));  // returns: false
  * $.wijinputcore.validateDate("平成 25年12月16日", new Date(2013,0,1), new Date(2013,11,31), "ggg ee年MM月dd日");  // returns: false
  */
wijmo.input.IFormatValidateLib.prototype.validateDate = function (value, minDate, maxDate, format, culture) {};
/** Parse the string value to a javascript Date object.
  * @param {string} value }, the string value that will be parsed.
  * @param {string} format }, indicate the date format, for more information about the format keyword, please see wijinput date.
  * @param {string} culture }, indicates the culture that the parse library will use.
  * @returns {Date} return the parsed date object.
  * @example
  * $.wijinputcore.parseDate("9/12/2013");  // returns: new Date(2013,8,12)
  * $.wijinputcore.parseDate("2013/09/12", "yyyy/MM/dd", "ja-JP");  //  returns: new Date(2013,8,12)
  * $.wijinputcore.parseDate("午後 11:22:33", "tt hh:mm:ss", "ja-JP");  //  returns: new Date(new Date().getFullYear(),0,1,23,22,33)
  * $.wijinputcore.parseDate("00:12:12", "HH:mm:ss", "ja-JP");   // returns: new Date(new Date().getFullYear(),0,1,0,12,12)
  */
wijmo.input.IFormatValidateLib.prototype.parseDate = function (value, format, culture) {};
/** Parse the string value to a javascript number object.
  * @param {string} value }, the string value that will be parsed.
  * @param {string} culture }, indicates the culture that the parse library will use.
  * @returns {number} return the parsed number object.
  * @example
  * $.wijinputcore.parseNumber("12,345.00");     // returns: 12345
  * $.wijinputcore.parseNumber("12.345,00","de-DE");  // returns: 12345
  * $.wijinputcore.parseNumber("45.00 %");   // returns: 0.45
  * $.wijinputcore.parseNumber("$12345.67");  // returns: 12345.67
  */
wijmo.input.IFormatValidateLib.prototype.parseNumber = function (value, culture) {};
/** Validate the string value according to the specified format string.
  * @param {string} value }, the string value that will be validated.
  * @param {number} minValue }, the minimum number.
  * @param {number} maxValue }, the maximum number.
  * @param {string} culture }, indicates the culture that the validate library will use.
  * @returns {bool} , a boolean value indicates whether the string number value is the specified number range.
  * @example
  * $.wijinputcore.validateNumber("123,45.6", 100, 100000);   // returns: true
  */
wijmo.input.IFormatValidateLib.prototype.validateNumber = function (value, minValue, maxValue, culture) {};
/** Validate the string value according to the specified format string.
  * @param {string} value }, the string value that will be validated.
  * @param {string} format }, a string value indicates the mask format, for more information about the format keyword, please see wijinput mask. The regex mask format is invalid.
  * @returns {bool} , a boolean value indicates whether the string value is valid for the specified format.
  * @example
  * $.wijinputcore.validateMask("1234567", "999-9999");  // returns: false
  */
wijmo.input.IFormatValidateLib.prototype.validateMask = function (value, format) {};
/** Validate the string value according to the specified format string.
  * This mehtod have the following two function prototype.
  *  boolean $.wijinputcore.validateText(string value, string format);  
  *  boolean $.wijinputcore.validateText(string value, Number minLength, Number maxLength);
  * @param {string} value }, the string value that will be validated.
  * @param format },  a string value indicates the text format, for more information about the format keyword, please see wijinput text.
  * @param minLength }, the length that will be valided.
  * @param maxLength }, the max length that will be valided.
  * @returns {bool} , a boolean value indicates whether the string value is valid for the specified format or length range.
  * @example
  * $.wijinputcore.validateText("123", "9");   // returns: true
  * $.wijinputcore.validateText("あ山９", "Ｚ");   // returns: true
  * $.wijinputcore.validateText("9999999", 4, 7);  //  returns: true
  */
wijmo.input.IFormatValidateLib.prototype.validateText = function (value, format, minLength, maxLength) {};
})()
})()
