var wijmo = wijmo || {};

(function () {
wijmo.tree = wijmo.tree || {};

(function () {
/** @class wijtree
  * @widget 
  * @namespace jQuery.wijmo.tree
  * @extends wijmo.wijmoWidget
  */
wijmo.tree.wijtree = function () {};
wijmo.tree.wijtree.prototype = new wijmo.wijmoWidget();
/** The getSelectedNodes method gets the selected nodes.
  * @returns {any[]} {array}
  * @example
  * $("selector").wijtree("getSelectedNodes");
  */
wijmo.tree.wijtree.prototype.getSelectedNodes = function () {};
/** The getCheckedNodes method gets the nodes which are checked.
  * @returns {any[]} {array}
  * @example
  * $("selector").wijtree("getCheckedNodes");
  */
wijmo.tree.wijtree.prototype.getCheckedNodes = function () {};
/** The destroy method will remove the rating functionality completely and will return the element to its pre-init state.
  * @example
  * $("selector").wijtree("destroy");
  */
wijmo.tree.wijtree.prototype.destroy = function () {};
/** The add method adds a node to the tree widget.
  * @param {string|object} node 1.markup html.such as "&lt;li&gt;&lt;a&gt;node&lt;/a&gt;&lt;/li&gt;" as a node.
  * 2.wijtreenode widget.
  * 3.object options according to the options of wijtreenode.
  * 4.node's text.
  * @param {number} position The position to insert at.
  * @example
  * $("#tree").wijtree("add", "node 1", 1);
  */
wijmo.tree.wijtree.prototype.add = function (node, position) {};
/** The remove method removes the indicated node from the wijtree element.
  * @param {number|object} node which node to be removed
  * 1.wijtreenode element.
  * 2.the zero-based index of which node you determined to remove.
  * @example
  * $("#tree").wijtree("remove", 1);
  */
wijmo.tree.wijtree.prototype.remove = function (node) {};
/** The getNodes method gets an array that contains the root nodes of the current tree.
  * @returns {Array}
  * @example
  * $("#tree").wijtree("getNodes");
  */
wijmo.tree.wijtree.prototype.getNodes = function () {};
/** The findNodeByText method finds a node by the specified node text.
  * @param {string} txt The text of which node you want to find.
  * @returns {wijmo.tree.wijtreenode}
  * @example
  * $("#tree").wijtree("findNodeByText", "node 1");
  */
wijmo.tree.wijtree.prototype.findNodeByText = function (txt) {};

/** @class */
var wijtree_options = function () {};
/** <p class='defaultValue'>Default value: false</p>
  * <p>When the allowDrag option is set to true, the tree nodes can be dragged.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtree_options.prototype.allowDrag = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>When allowDrop is set to true, one tree node can be dropped within another tree node.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtree_options.prototype.allowDrop = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>The allowEdit option allows a user to edit the tree nodes at run time.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtree_options.prototype.allowEdit = false;
/** <p class='defaultValue'>Default value: true</p>
  * <p>The allowSorting option allows the tree nodes to be sorted at run time when the user presses the "s" key.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtree_options.prototype.allowSorting = true;
/** <p class='defaultValue'>Default value: true</p>
  * <p>The allowTriState option allows the tree nodes to exhibit triState behavior. This lets the node checkboxes be checked, unchecked, or indeterminate. This option must be used with the showCheckBoxes option.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtree_options.prototype.allowTriState = true;
/** <p class='defaultValue'>Default value: true</p>
  * <p>The autoCheckNodes option allows the sub-nodes to be checked when the parent nodes are checked. To use this option, showCheckboxes must be set to "true."</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtree_options.prototype.autoCheckNodes = true;
/** <p class='defaultValue'>Default value: false</p>
  * <p>If this option is set to true, 
  * the expanded node will be collapsed if another node is expanded.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtree_options.prototype.autoCollapse = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>If set to true, the select, click, 
  * and check operations are disabled too.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtree_options.prototype.disabled = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>The expandCollapseHoverUsed option allows the tree to expand or collapse when the mouse hovers over the expand/collapse button.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtree_options.prototype.expandCollapseHoverUsed = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>The showCheckBoxes option allows the node Check Box to be shown on the tree nodes.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtree_options.prototype.showCheckBoxes = false;
/** <p class='defaultValue'>Default value: true</p>
  * <p>The showExpandCollapse option determines if the tree is displayed in an expanded or collapsed state. If set to "false," then the wijtree widget will be displayed in the expanded state.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtree_options.prototype.showExpandCollapse = true;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.tree.wijtree_animation.html'>wijmo.tree.wijtree_animation</a></p>
  * <p>The expandAnimation option determines the animation effect, easing, and duration for showing child nodes when the parent node is expanded.</p>
  * @field 
  * @type {wijmo.tree.wijtree_animation}
  * @option
  */
wijtree_options.prototype.expandAnimation = null;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>The expandDelay option controls the length of time in milliseconds to delay before the node is expanded.</p>
  * @field 
  * @type {number}
  * @option
  */
wijtree_options.prototype.expandDelay = 0;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.tree.wijtree_animation.html'>wijmo.tree.wijtree_animation</a></p>
  * <p>The collapseAnimation option determines the animation effect, easing, and duration for hiding child nodes when the parent node is collapsed.</p>
  * @field 
  * @type {wijmo.tree.wijtree_animation}
  * @option
  */
wijtree_options.prototype.collapseAnimation = null;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>This option controls the length of time in milliseconds to delay before the node collapses.</p>
  * @field 
  * @type {number}
  * @option
  */
wijtree_options.prototype.collapseDelay = 0;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Customize the jquery-ui-draggable plugin of wijtree.</p>
  * @field 
  * @type {object}
  * @option
  */
wijtree_options.prototype.draggable = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Customize the jquery-ui-droppable plugin of wijtree.</p>
  * @field 
  * @type {object}
  * @option
  */
wijtree_options.prototype.droppable = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Customizes the helper element to be used to display the position that
  * the node will be inserted to. 
  * If a function is specified, it must return a DOMElement.</p>
  * @field 
  * @type {String|Function}
  * @option
  */
wijtree_options.prototype.dropVisual = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Set the child nodes object array as the datasource of wijtree.</p>
  * @field 
  * @type {Array}
  * @option 
  * @example
  * // Supply a function as an option.
  * $(".selector").wijtree("option","nodes",
  * [{ text:"node1", navigateUrl:"#" }]);
  */
wijtree_options.prototype.nodes = null;
/** The nodeBlur event fired when the node loses focus.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data The node widget that relates to this event.
  */
wijtree_options.prototype.nodeBlur = null;
/** The nodeFocus event fired when the node is focused.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data The node widget that relates to this event.
  */
wijtree_options.prototype.nodeFocus = null;
/** The nodeClick event fires when a tree node is clicked.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data The node widget that relates to this event.
  */
wijtree_options.prototype.nodeClick = null;
/** The nodeCheckChanging event fires before a node is checked.
  * You can cancel this event by returning false.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data The node widget that relates to this event.
  */
wijtree_options.prototype.nodeCheckChanging = null;
/** The nodeCheckChanged event fires when a node is checked.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data The node widget that relates to this event.
  */
wijtree_options.prototype.nodeCheckChanged = null;
/** The nodeCollapsed event fires when a tree node is collapsed.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data The node widget that relates to this event.
  */
wijtree_options.prototype.nodeCollapsed = null;
/** The nodeExpanded event handler.
  * A function called when a node is expanded.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data The node widget that relates to this event.
  */
wijtree_options.prototype.nodeExpanded = null;
/** The nodeDragging event handler.A function called
  * when the node is moved during a drag-and-drop operation.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data The node widget that relates to this event.
  */
wijtree_options.prototype.nodeDragging = null;
/** The nodeDragStarted event fires when a user starts to drag a node.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data The node widget that relates to this event.
  */
wijtree_options.prototype.nodeDragStarted = null;
/** The nodeBeforeDropped event handler is called before a draggable node is dropped in another position. If the event handler returns false, the drop action will be prevented.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data The node widget that relates to this event.
  */
wijtree_options.prototype.nodeBeforeDropped = null;
/** The nodeDropped event is called when an acceptable draggable node is dropped over to another position.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.tree.INodeDroppedEventArgs} data Information about an event
  */
wijtree_options.prototype.nodeDropped = null;
/** The nodeMouseOver event fires when a user places the mouse pointer over a node.
  * @event 
  * @param {object} event jQuery.Event object.
  * @param {object} data The node widget that relates to this event.
  */
wijtree_options.prototype.nodeMouseOver = null;
/** The nodeMouseOut event fires when the user moves the mouse pointer off of a node.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data The node widget that relates to this event.
  */
wijtree_options.prototype.nodeMouseOut = null;
/** The nodeTextChanged event fires when the text of a node changes.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data The node widget that relates to this event.
  */
wijtree_options.prototype.nodeTextChanged = null;
/** The selectedNodeChanged event fires when the selected node changes.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data The node widget that relates to this event.
  */
wijtree_options.prototype.selectedNodeChanged = null;
/** The nodeExpanding event fires before a tree node is expanded.
  * This event can be canceled, if return false
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data The node widget that relates to this event.
  */
wijtree_options.prototype.nodeExpanding = null;
/** The nodeCollapsing event fires before a node collapses.
  * This event can be canceled, if return false
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data The node widget that relates to this event.
  */
wijtree_options.prototype.nodeCollapsing = null;
wijmo.tree.wijtree.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijtree_options());
$.widget("wijmo.wijtree", $.wijmo.widget, wijmo.tree.wijtree.prototype);
/** @class wijtreenode
  * @namespace wijmo.tree
  * @extends wijmo.wijmoWidget
  */
wijmo.tree.wijtreenode = function () {};
wijmo.tree.wijtreenode.prototype = new wijmo.wijmoWidget();
/** Destroy the node widget. */
wijmo.tree.wijtreenode.prototype.destroy = function () {};
/** The add method adds a node to the node.
  * @param {string|object} node 1.markup html.such as "&lt;li&gt;&lt;a&gt;node&lt;/a&gt;&lt;/li&gt;" as a node.
  * 2.wijtreenode element.
  * 3.object options according to the options of wijtreenode.
  * 4. node's text.
  * @param {number} position The position to insert at.
  * @example
  * $("#treenode1").wijtreenode("add", "node 1", 1);
  */
wijmo.tree.wijtreenode.prototype.add = function (node, position) {};
/** The remove method removes the indicated node from this node.
  * @param {string|object} node which node to be removed
  * 1.wijtreenode element.
  * 2.the zero-based index of which node you determined to remove.
  * @example
  * $("#tree").wijtree("remove", 1);
  */
wijmo.tree.wijtreenode.prototype.remove = function (node) {};
/** The getNodes method gets an array that contains the root nodes of the current tree node.
  * @returns {wijmo.tree.wijtreenode[]}
  * @example
  * $("#tree").wijtree("getNodes");
  */
wijmo.tree.wijtreenode.prototype.getNodes = function () {};
/** Sorts the child nodes of the node. */
wijmo.tree.wijtreenode.prototype.sortNodes = function () {};
/** Checks or unchecks the node.
  * @param {bool} value Check or uncheck the node.
  */
wijmo.tree.wijtreenode.prototype.check = function (value) {};
/** Selects or unselects the node.
  * @param {bool} value select or unselect the node.
  */
wijmo.tree.wijtreenode.prototype.select = function (value) {};
/** Get owner which contains the node. */
wijmo.tree.wijtreenode.prototype.getOwner = function () {};
/** Expands the node. */
wijmo.tree.wijtreenode.prototype.expand = function () {};
/** Collapses the node. */
wijmo.tree.wijtreenode.prototype.collapse = function () {};
/** @interface wijtree_animation
  * @namespace wijmo.tree
  */
wijmo.tree.wijtree_animation = function () {};
/** <p>The options of animation.</p>
  * @field
  */
wijmo.tree.wijtree_animation.prototype.animated = null;
/** <p>The animation effect that is used when the wijtree is collapsed. 
  * Please see the Animation Effects topic for a list of the available animation effects.</p>
  * @field 
  * @type {string}
  */
wijmo.tree.wijtree_animation.prototype.effect = null;
/** <p>A value that indicates the easing function that will be applied to the animation. 
  * For more information, please see the Easing topic.</p>
  * @field 
  * @type {string}
  */
wijmo.tree.wijtree_animation.prototype.easing = null;
/** <p>The duration of the animation in milliseconds.</p>
  * @field 
  * @type {number}
  */
wijmo.tree.wijtree_animation.prototype.duration = null;
/** The interface of WijTreeOptions.
  * @interface WijTreeOptions
  * @namespace wijmo.tree
  * @extends wijmo.WidgetOptions
  */
wijmo.tree.WijTreeOptions = function () {};
/** <p>When the allowDrag option is set to true, the tree nodes can be dragged.</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeOptions.prototype.allowDrag = null;
/** <p>When allowDrop is set to true, one tree node can be dropped within another tree node.</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeOptions.prototype.allowDrop = null;
/** <p>The allowEdit option allows a user to edit the tree nodes at run time.</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeOptions.prototype.allowEdit = null;
/** <p>The allowSorting option allows the tree nodes to be sorted at run time when the user presses the "s" key.</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeOptions.prototype.allowSorting = null;
/** <p>The allowTriState option allows the tree nodes to exhibit triState behavior. This lets the node checkboxes be checked, unchecked, or indeterminate. This option must be used with the showCheckBoxes option.</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeOptions.prototype.allowTriState = null;
/** <p>The autoCheckNodes option allows the sub-nodes to be checked when the parent nodes are checked. To use this option, showCheckboxes must be set to "true."</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeOptions.prototype.autoCheckNodes = null;
/** <p>If this option is set to true, 
  * the expanded node will be collapsed if another node is expanded.</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeOptions.prototype.autoCollapse = null;
/** <p>If set to true, the select, click, 
  * and check operations are disabled too.</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeOptions.prototype.disabled = null;
/** <p>The expandCollapseHoverUsed option allows the tree to expand or collapse when the mouse hovers over the expand/collapse button.</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeOptions.prototype.expandCollapseHoverUsed = null;
/** <p>The showCheckBoxes option allows the node Check Box to be shown on the tree nodes.</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeOptions.prototype.showCheckBoxes = null;
/** <p>The showExpandCollapse option determines if the tree is displayed in an expanded or collapsed state. If set to "false," then the wijtree widget will be displayed in the expanded state.</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeOptions.prototype.showExpandCollapse = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.tree.wijtree_animation.html'>wijmo.tree.wijtree_animation</a></p>
  * <p>The expandAnimation option determines the animation effect, easing, and duration for showing child nodes when the parent node is expanded.</p>
  * @field 
  * @type {wijmo.tree.wijtree_animation}
  */
wijmo.tree.WijTreeOptions.prototype.expandAnimation = null;
/** <p>The expandDelay option controls the length of time in milliseconds to delay before the node is expanded.</p>
  * @field 
  * @type {number}
  */
wijmo.tree.WijTreeOptions.prototype.expandDelay = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.tree.wijtree_animation.html'>wijmo.tree.wijtree_animation</a></p>
  * <p>The collapseAnimation option determines the animation effect, easing, and duration for hiding child nodes when the parent node is collapsed.</p>
  * @field 
  * @type {wijmo.tree.wijtree_animation}
  */
wijmo.tree.WijTreeOptions.prototype.collapseAnimation = null;
/** <p>This option controls the length of time in milliseconds to delay before the node collapses.</p>
  * @field 
  * @type {number}
  */
wijmo.tree.WijTreeOptions.prototype.collapseDelay = null;
/** <p>Customize the jquery-ui-draggable plugin of wijtree.</p>
  * @field 
  * @type {JQueryUI.Draggable}
  */
wijmo.tree.WijTreeOptions.prototype.draggable = null;
/** <p>Customize the jquery-ui-droppable plugin of wijtree.</p>
  * @field 
  * @type {JQueryUI.Droppable}
  */
wijmo.tree.WijTreeOptions.prototype.droppable = null;
/** <p>Customizes the helper element to be used to display the position that
  * the node will be inserted to. 
  * If a function is specified, it must return a DOMElement.</p>
  * @field 
  * @type {String|Function}
  */
wijmo.tree.WijTreeOptions.prototype.dropVisual = null;
/** <p>Set the child nodes object array as the datasource of wijtree.</p>
  * @field 
  * @type {Array}
  * @example
  * // Supply a function as an option.
  * $(".selector").wijtree("option","nodes",
  * [{ text:"node1", navigateUrl:"#" }]);
  */
wijmo.tree.WijTreeOptions.prototype.nodes = null;
/** <p>The nodeBlur event fired when the node loses focus.</p>
  * @field 
  * @type {function}
  */
wijmo.tree.WijTreeOptions.prototype.nodeBlur = null;
/** <p>The nodeFocus event fired when the node is focused.</p>
  * @field 
  * @type {function}
  */
wijmo.tree.WijTreeOptions.prototype.nodeFocus = null;
/** <p>The nodeClick event fires when a tree node is clicked.</p>
  * @field 
  * @type {function}
  */
wijmo.tree.WijTreeOptions.prototype.nodeClick = null;
/** <p>The nodeCheckChanging event fires before a node is checked.
  * You can cancel this event by returning false.</p>
  * @field 
  * @type {function}
  */
wijmo.tree.WijTreeOptions.prototype.nodeCheckChanging = null;
/** <p>The nodeCheckChanged event fires when a node is checked.</p>
  * @field 
  * @type {function}
  */
wijmo.tree.WijTreeOptions.prototype.nodeCheckChanged = null;
/** <p>The nodeCollapsed event fires when a tree node is collapsed.</p>
  * @field 
  * @type {function}
  */
wijmo.tree.WijTreeOptions.prototype.nodeCollapsed = null;
/** <p>The nodeExpanded event handler.
  * A function called when a node is expanded.</p>
  * @field 
  * @type {function}
  */
wijmo.tree.WijTreeOptions.prototype.nodeExpanded = null;
/** <p>The nodeDragging event handler.A function called
  * when the node is moved during a drag-and-drop operation.</p>
  * @field 
  * @type {function}
  */
wijmo.tree.WijTreeOptions.prototype.nodeDragging = null;
/** <p>The nodeDragStarted event fires when a user starts to drag a node.</p>
  * @field 
  * @type {function}
  */
wijmo.tree.WijTreeOptions.prototype.nodeDragStarted = null;
/** <p>The nodeBeforeDropped event handler is called before a draggable node is dropped in another position. If the event handler returns false, the drop action will be prevented.</p>
  * @field 
  * @type {function}
  */
wijmo.tree.WijTreeOptions.prototype.nodeBeforeDropped = null;
/** <p>The nodeDropped event is called when an acceptable draggable node is dropped over to another position.</p>
  * @field 
  * @type {function}
  */
wijmo.tree.WijTreeOptions.prototype.nodeDropped = null;
/** <p>The nodeMouseOver event fires when a user places the mouse pointer over a node.</p>
  * @field 
  * @type {function}
  */
wijmo.tree.WijTreeOptions.prototype.nodeMouseOver = null;
/** <p>The nodeMouseOut event fires when the user moves the mouse pointer off of a node.</p>
  * @field 
  * @type {function}
  */
wijmo.tree.WijTreeOptions.prototype.nodeMouseOut = null;
/** <p>The nodeTextChanged event fires when the text of a node changes.</p>
  * @field 
  * @type {function}
  */
wijmo.tree.WijTreeOptions.prototype.nodeTextChanged = null;
/** <p>The selectedNodeChanged event fires when the selected node changes.</p>
  * @field 
  * @type {function}
  */
wijmo.tree.WijTreeOptions.prototype.selectedNodeChanged = null;
/** <p>The nodeExpanding event fires before a tree node is expanded.
  * This event can be canceled, if return false</p>
  * @field 
  * @type {function}
  */
wijmo.tree.WijTreeOptions.prototype.nodeExpanding = null;
/** <p>The nodeCollapsing event fires before a node collapses.
  * This event can be canceled, if return false</p>
  * @field 
  * @type {function}
  */
wijmo.tree.WijTreeOptions.prototype.nodeCollapsing = null;
/** The interface of WijTreeNodeOptions.
  * @interface WijTreeNodeOptions
  * @namespace wijmo.tree
  * @extends wijmo.WidgetOptions
  */
wijmo.tree.WijTreeNodeOptions = function () {};
/** <p>The checked option checks the tree node checkbox when it is set to true. It will uncheck the tree node checkbox when set to false.</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeNodeOptions.prototype.checked = null;
/** <p>The collapsedIconClass option sets the collapsed node icon (based on ui-icon) for the specified nodes.</p>
  * @field 
  * @type {string}
  */
wijmo.tree.WijTreeNodeOptions.prototype.collapsedIconClass = null;
/** <p>The expanded option will expand the tree node if set to "true." It will collapse the tree node if set to "false.".</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeNodeOptions.prototype.expanded = null;
/** <p>The expandedIconClass option sets the expanded node icon (based on ui-icon) for the specified nodes.</p>
  * @field 
  * @type {string}
  */
wijmo.tree.WijTreeNodeOptions.prototype.expandedIconClass = null;
/** <p>The itemIconClass option sets the node icon (based on ui-icon). It will be displayed on both expanded and collapsed nodes when the expandedIconClass and collapsedIconClass options are not specified.</p>
  * @field 
  * @type {string}
  */
wijmo.tree.WijTreeNodeOptions.prototype.itemIconClass = null;
/** <p>The navigateUrl option sets the node's navigate url link.</p>
  * @field 
  * @type {string}
  */
wijmo.tree.WijTreeNodeOptions.prototype.navigateUrl = null;
/** <p>The selected option selects the specified node when set to true, otherwise it unselects the node.</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeNodeOptions.prototype.selected = null;
/** <p>This option sets the node's text.</p>
  * @field 
  * @type {string}
  */
wijmo.tree.WijTreeNodeOptions.prototype.text = null;
/** <p>The toolTip option sets the node's tooltip.</p>
  * @field 
  * @type {string}
  */
wijmo.tree.WijTreeNodeOptions.prototype.toolTip = null;
/** <p>The hasChildren option determines whether the specified node has child nodes. It's always used when you're custom adding child nodes, such as in an async load.</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeNodeOptions.prototype.hasChildren = null;
/** <p>The params option sets the parameter needed to pass when the user is custom loading child nodes.</p>
  * @field
  */
wijmo.tree.WijTreeNodeOptions.prototype.params = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.tree.WijTreeNodeOptions.html'>wijmo.tree.WijTreeNodeOptions[]</a></p>
  * <p>Determines the child nodes of this nodes.</p>
  * @field 
  * @type {wijmo.tree.WijTreeNodeOptions[]}
  */
wijmo.tree.WijTreeNodeOptions.prototype.nodes = null;
/** <p>Determines the node's checkState.
  * It can be set to "checked", "unchecked", or "indeterminate".</p>
  * @field 
  * @type {string}
  */
wijmo.tree.WijTreeNodeOptions.prototype.checkState = null;
/** <p>Determines the index of next node.</p>
  * @field 
  * @type {number}
  */
wijmo.tree.WijTreeNodeOptions.prototype.nIndex = null;
/** <p>The htmlGenerated option determines whether the node be generated from html.</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeNodeOptions.prototype.htmlGenerated = null;
/** <p>The isAddedNodeWithOptions option determines whether the node be created by options.</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeNodeOptions.prototype.isAddedNodeWithOptions = null;
/** <p>Determines whether to enable node dragging.
  * Default value is null which means whether to enable node dragging depends on the setting of wijtree's allowDrag option.</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeNodeOptions.prototype.allowDrag = null;
/** <p>Determines whether to enable node dropping.
  * Default value is null which means whether to enable node dropping depends on the setting of wijtree's allowDrop option.</p>
  * @field 
  * @type {boolean}
  */
wijmo.tree.WijTreeNodeOptions.prototype.allowDrop = null;
/** The interface of Position.
  * @interface Position
  * @namespace wijmo.tree
  */
wijmo.tree.Position = function () {};
/** <p>The x value of position.</p>
  * @field 
  * @type {number}
  */
wijmo.tree.Position.prototype.x = null;
/** <p>The y value of position.</p>
  * @field 
  * @type {number}
  */
wijmo.tree.Position.prototype.y = null;
/** The interface of Rect.
  * @interface Rect
  * @namespace wijmo.tree
  */
wijmo.tree.Rect = function () {};
/** <p>The height value of rect.</p>
  * @field 
  * @type {number}
  */
wijmo.tree.Rect.prototype.h = null;
/** <p>The width value of rect.</p>
  * @field 
  * @type {number}
  */
wijmo.tree.Rect.prototype.w = null;
/** <p>The left value of rect.</p>
  * @field 
  * @type {number}
  */
wijmo.tree.Rect.prototype.l = null;
/** <p>The top value of rect.</p>
  * @field 
  * @type {number}
  */
wijmo.tree.Rect.prototype.t = null;
/** Contains information about wijtree.nodeDropped event
  * @interface INodeDroppedEventArgs
  * @namespace wijmo.tree
  */
wijmo.tree.INodeDroppedEventArgs = function () {};
/** <p>The source parent of current draggable node before it be dragged, a jQuery object.</p>
  * @field 
  * @type {jQuery}
  */
wijmo.tree.INodeDroppedEventArgs.prototype.sourceParent = null;
/** <p>The Index of dragged node in source parent.</p>
  * @field 
  * @type {number}
  */
wijmo.tree.INodeDroppedEventArgs.prototype.sIndex = null;
/** <p>The target parent of current draggable node after it be dropped, a jQuery object.</p>
  * @field 
  * @type {jQuery}
  */
wijmo.tree.INodeDroppedEventArgs.prototype.targetParent = null;
/** <p>The Index of dragged node in target parent.</p>
  * @field 
  * @type {number}
  */
wijmo.tree.INodeDroppedEventArgs.prototype.tIndex = null;
/** <p>The current draggable node.</p>
  * @field 
  * @type {jQuery}
  */
wijmo.tree.INodeDroppedEventArgs.prototype.draggable = null;
/** <p>The current absolute position of the draggable helper.</p>
  * @field 
  * @type {object}
  */
wijmo.tree.INodeDroppedEventArgs.prototype.offset = null;
/** <p>The current position of the draggable helper.</p>
  * @field 
  * @type {object}
  */
wijmo.tree.INodeDroppedEventArgs.prototype.position = null;
typeof wijmo.WidgetOptions != 'undefined' && $.extend(wijmo.tree.WijTreeOptions.prototype, wijmo.WidgetOptions.prototype);
typeof wijmo.WidgetOptions != 'undefined' && $.extend(wijmo.tree.WijTreeNodeOptions.prototype, wijmo.WidgetOptions.prototype);
})()
})()
