var wijmo = wijmo || {};

(function () {
wijmo.popup = wijmo.popup || {};

(function () {
/** @class wijpopup
  * @widget 
  * @namespace jQuery.wijmo.popup
  * @extends wijmo.JQueryUIWidget
  */
wijmo.popup.wijpopup = function () {};
wijmo.popup.wijpopup.prototype = new wijmo.JQueryUIWidget();
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.popup.wijpopup.prototype.destroy = function () {};
/** Determines whether the element is visible.
  * @returns {bool}
  */
wijmo.popup.wijpopup.prototype.isVisible = function () {};
/** Popups the element.
  *  Position is an optional argument, it is the options object used in jquery.ui.position.
  * @param {?object} position An optional argument, it is the options object used in jquery.ui.position.
  */
wijmo.popup.wijpopup.prototype.show = function (position) {};
/** Popups the element at specified absolute position related to document.
  * @param {number} x The x coordinate at which to show the popup.
  * @param {number} y The y coordinate at which to show the popup.
  * @example
  * // set the popup position is "100, 100" that related to document.
  *  $(".selector").wijpopup('showAt', 100, 100);
  */
wijmo.popup.wijpopup.prototype.showAt = function (x, y) {};
/** Hides the element. */
wijmo.popup.wijpopup.prototype.hide = function () {};

/** @class */
var wijpopup_options = function () {};
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines if the element's parent element is the outermost element.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * If true, the element's parent element will be changed to 
  * the body or outermost form element.
  */
wijpopup_options.prototype.ensureOutermost = false;
/** <p class='defaultValue'>Default value: 'show'</p>
  * <p>Specifies the effect to be used when the popup is shown.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * This allows you to use a different effect when you show the popup 
  * than when you hide the popup.(See also hideEffect.) Possible values 
  * include 'blind', 'clip', 'drop', 'fade', 'fold', 'slide', and 'pulsate'.
  * See the jQuery UI / Effects page for more information.
  */
wijpopup_options.prototype.showEffect = 'show';
/** <p>Specifies the object/hash including specific options for the show effect.</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * See the option parameters used by the jQuery UI show effect for more information.
  * @example
  * // set the show effect's direction.
  *       $(".selector").wijpopup({ showOptions: {direction: 'up' });
  */
wijpopup_options.prototype.showOptions = null;
/** <p class='defaultValue'>Default value: 300</p>
  * <p>A value that indicates the number of milliseconds it takes for the 
  *  indicated animation effect to completely show the popup.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * This allows you to use a different number of milliseconds when you
  * show the popup than when you hide the popup.(See also hideDuration.)
  */
wijpopup_options.prototype.showDuration = 300;
/** <p class='defaultValue'>Default value: 'hide'</p>
  * <p>Specifies the effect to be used when the popup is hidden.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * This allows you to use a different effect when you show the popup than 
  * when you hide the popup.(See also showEffect.) 
  * Possible values include 'blind', 'clip', 'drop', 'fade', 'fold', 'slide', 
  * and 'pulsate'.See the jQuery UI / Effects page for more information.
  */
wijpopup_options.prototype.hideEffect = 'hide';
/** <p>Specifies the object/hash including specific options for the hide effect.</p>
  * @field 
  * @option 
  * @remarks
  * See the option parameters used by the jQuery UI hide effect for more information.
  * @example
  * // set the hide effect's direction.
  *       $(".selector").wijpopup({ hideOptions: {direction: 'up' });
  */
wijpopup_options.prototype.hideOptions = null;
/** <p class='defaultValue'>Default value: 100</p>
  * <p>A value that indicates the number of milliseconds it takes for the 
  *  indicated animation effect to completely hide the popup.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * This allows you to use a different number of milliseconds when
  * you show the popup than when you hide the popup.(See also showDuration.)
  */
wijpopup_options.prototype.hideDuration = 100;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether to automatically hide the popup when clicking outside the element.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * If true, the popup will be automatically hidden when another element is selected.
  * If false (default), the popup will remain visible until hidden with the hide method.
  */
wijpopup_options.prototype.autoHide = false;
/** <p>Options for positioning the element, please see jquery.ui.position for possible options.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * // positioning the element, located on "#TextBox1" and up offset is 4
  *  $(".selector").wijpopup({ position:{ of: $('#TextBox1'), offset: '0 4' }});
  */
wijpopup_options.prototype.position = null;
/** The showing event handler. 
  * A function called before the element is shown. Cancellable.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @remarks
  * This is a cancelable event.You can set data.cancel = true to cancel the element to be shown.
  */
wijpopup_options.prototype.showing = null;
/** The shown event handler. A function called after the element is shown.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijpopup_options.prototype.shown = null;
/** The hiding event handler. 
  * A function called before the element is hidden. Cancellable.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.popup.IHidingEventArgs} data Information about an event
  * @remarks
  * This is a cancelable event. You can set data.cancel = true to cancel the element to be hidden.
  */
wijpopup_options.prototype.hiding = null;
/** The hidden event handler. A function called after the element is hidden.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijpopup_options.prototype.hidden = null;
/** The posChanged event handler. 
  *   A function called when the position of the element is changed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijpopup_options.prototype.posChanged = null;
wijmo.popup.wijpopup.prototype.options = $.extend({}, true, wijmo.JQueryUIWidget.prototype.options, new wijpopup_options());
$.widget("wijmo.wijpopup", $.wijmo.widget, wijmo.popup.wijpopup.prototype);
/** Contains information about wijpopup.hiding event
  * @interface IHidingEventArgs
  * @namespace wijmo.popup
  */
wijmo.popup.IHidingEventArgs = function () {};
/** <p>Cancel the element to be hidden if true.</p>
  * @field 
  * @type {bool}
  */
wijmo.popup.IHidingEventArgs.prototype.cancel = null;
})()
})()
