var wijmo = wijmo || {};

(function () {
wijmo.superpanel = wijmo.superpanel || {};

(function () {
/** @interface superpanel_vscroller
  * @namespace wijmo.superpanel
  */
wijmo.superpanel.superpanel_vscroller = function () {};
/** <p>This value determines the position of the vertical scroll bar.</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible options are: "left", "right".
  * "left" - The vertical scroll bar is placed at the 
  * left side of the content area.
  * "right" - The vertical scroll bar is placed at the 
  * right side of the content area.
  */
wijmo.superpanel.superpanel_vscroller.prototype.scrollBarPosition = null;
/** <p>This value determines the visibility of the vertical scroll bar.
  * Default.: "auto".</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible options are "auto", "visible" and "hidden".
  * "auto" - Shows the scroll bar when needed.
  * "visible" - Scroll bar will always be visible. 
  * It"s disabled when not needed.
  * "hidden" - Scroll bar will be shown.
  */
wijmo.superpanel.superpanel_vscroller.prototype.scrollBarVisibility = null;
/** <p>This value determines the scroll mode of vertical scrolling.</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible options are: "scrollBar", "buttons", 
  * "buttonsHover" and "edge".
  * "scrollBar" - Scroll bars are used for scrolling.
  * "buttons" - Scroll buttons are used for scrolling. 
  * Scrolling occurs only when scroll buttons are clicked.
  * "buttonsHover" - Scroll buttons are used for scrolling. 
  * Scrolling occurs only when scroll buttons are hovered.
  * "edge" - Scrolling occurs when the mouse is moving to 
  * the edge of the content area.
  * Scroll modes can be combined with each other. 
  * For example, vScrollMode: "scrollbar,scrollbuttons" will enable 
  * both a scrollbar and scroll buttons.
  */
wijmo.superpanel.superpanel_vscroller.prototype.scrollMode = null;
/** <p>This number determines the vertical scrolling position of
  * wijsuperpanel.</p>
  * @field 
  * @type {number}
  */
wijmo.superpanel.superpanel_vscroller.prototype.scrollValue = null;
/** <p>This number sets the maximum value of the vertical scroller.</p>
  * @field 
  * @type {number}
  */
wijmo.superpanel.superpanel_vscroller.prototype.scrollMax = null;
/** <p>This number sets the minimum value of the vertical scroller.</p>
  * @field 
  * @type {number}
  */
wijmo.superpanel.superpanel_vscroller.prototype.scrollMin = null;
/** <p>This value sets the large change value of the vertical scroller.</p>
  * @field 
  * @type {number}
  * @remarks
  * wijsuperpanel will scroll a large change when a user clicks 
  * on the tracks of scroll bars or presses left or right arrow keys 
  * on the keyboard with the shift key down.
  * When scrollLargeChange is null, wijsuperpanel 
  * will scroll the height of content.
  */
wijmo.superpanel.superpanel_vscroller.prototype.scrollLargeChange = null;
/** <p>This value sets the small change value of the vertical scroller.</p>
  * @field 
  * @type {number}
  * @remarks
  * wijsuperpanel will scroll a small change when a user clicks on the 
  * arrows of scroll bars, clicks or hovers scroll buttons, presses left
  * or right arrow keys on keyboard, and hovers on the edge of 
  * wijsuperpanel.
  * When scrollSmallChange is null, wijsuperpanel will scroll half of 
  * the height of content.
  */
wijmo.superpanel.superpanel_vscroller.prototype.scrollSmallChange = null;
/** <p>This value sets the minimum length, in pixel, of the vertical 
  * scroll bar thumb button.</p>
  * @field 
  * @type {number}
  */
wijmo.superpanel.superpanel_vscroller.prototype.scrollMinDragLength = null;
/** <p>This object determines the increase button position.</p>
  * @field 
  * @type {Object}
  * @remarks
  * Please look at the options for jquery.ui.position.js for more info.
  */
wijmo.superpanel.superpanel_vscroller.prototype.increaseButtonPosition = null;
/** <p>This object determines the decrease button position.</p>
  * @field 
  * @type {Object}
  * @remarks
  * Please look at the options for jquery.ui.position.js for more info.
  */
wijmo.superpanel.superpanel_vscroller.prototype.decreaseButtonPosition = null;
/** <p>This value sets the width of the horizontal hovering edge which will trigger the vertical scrolling.</p>
  * @field 
  * @type {number}
  */
wijmo.superpanel.superpanel_vscroller.prototype.hoverEdgeSpan = null;
/** <p>This number specifies the value to add to smallchange or largechange when scrolling the first step (scrolling from scrollMin).</p>
  * @field 
  * @type {number}
  */
wijmo.superpanel.superpanel_vscroller.prototype.firstStepChangeFix = null;
/** @interface superpanel_hscroller
  * @namespace wijmo.superpanel
  */
wijmo.superpanel.superpanel_hscroller = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.superpanel.superpanel_hscroller.prototype.scrollBarPosition = null;
/** <p>This value determines the visibility of the horizontal scroll bar.</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible options are "auto", "visible" and "hidden".
  * "auto" - Shows the scroll when needed.
  * "visible" - The Scroll bar is always visible. It is disabled 
  * when not needed.
  * "hidden" - The Scroll bar is hidden.
  */
wijmo.superpanel.superpanel_hscroller.prototype.scrollBarVisibility = null;
/** <p>This value determines the scroll mode of horizontal scrolling.</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible options are "scrollBar", "buttons", "buttonsHover" 
  * and "edge".
  * "scrollBar" - Scroll bars are used for scrolling.
  * "buttons" - Scroll buttons are used for scrolling. 
  * Scrolling occurs only when scroll buttons are clicked.
  * "buttonsHover" - Scroll buttons are used for scrolling. 
  * Scrolling occurs only when scroll buttons are hovered.
  * "edge" - Scrolling occurs when the mouse is moving to the edge
  * of the content area.
  * Scroll modes can be combined with each other. 
  * For example, scrollMode: "scrollbar,scrollbuttons" will enable 
  * both a scrollbar and scroll buttons.
  */
wijmo.superpanel.superpanel_hscroller.prototype.scrollMode = null;
/** <p>This value determines the horizontal scrolling position of
  * wijsuperpanel.</p>
  * @field 
  * @type {number}
  */
wijmo.superpanel.superpanel_hscroller.prototype.scrollValue = null;
/** <p>This value sets the maximum value of the horizontal scroller.</p>
  * @field 
  * @type {number}
  */
wijmo.superpanel.superpanel_hscroller.prototype.scrollMax = null;
/** <p>This value sets the minimum value of the horizontal scroller.</p>
  * @field 
  * @type {number}
  */
wijmo.superpanel.superpanel_hscroller.prototype.scrollMin = null;
/** <p>This value sets the large change value of the horizontal scroller.</p>
  * @field 
  * @type {number}
  * @remarks
  * Wijsuperpanel will scroll a large change when a user clicks on the 
  * tracks of scroll bars or presses left or right arrow keys on the 
  * keyboard with the shift key down.
  * When scrollLargeChange is null, wijsuperpanel will scroll 
  * the width of content.
  */
wijmo.superpanel.superpanel_hscroller.prototype.scrollLargeChange = null;
/** <p>This value sets the small change value of the horizontal scroller.</p>
  * @field 
  * @type {number}
  * @remarks
  * Wijsuperpanel will scroll a small change when a user clicks on 
  * the arrows of scroll bars, clicks or hovers scroll buttons, 
  * presses left or right arrow keys on keyboard, 
  * and hovers on the edge of wijsuperpanel.
  * When scrollSmallChange is null, wijsuperpanel will scroll half of 
  * the width of content.
  */
wijmo.superpanel.superpanel_hscroller.prototype.scrollSmallChange = null;
/** <p>This value sets the minimum length, in pixel, of the horizontal 
  * scroll bar thumb button.</p>
  * @field 
  * @type {number}
  */
wijmo.superpanel.superpanel_hscroller.prototype.scrollMinDragLength = null;
/** <p>This is an object that determines the increase button position.</p>
  * @field 
  * @type {Object}
  * @remarks
  * Please look at the options for jquery.ui.position.js for more info.
  */
wijmo.superpanel.superpanel_hscroller.prototype.increaseButtonPosition = null;
/** <p>This is an object that determines the decrease button position.</p>
  * @field 
  * @type {Object}
  */
wijmo.superpanel.superpanel_hscroller.prototype.decreaseButtonPosition = null;
/** <p>This value sets the width, in pixels, of the horizontal hovering edge which will trigger the horizontal scrolling.</p>
  * @field 
  * @type {number}
  */
wijmo.superpanel.superpanel_hscroller.prototype.hoverEdgeSpan = null;
/** <p>This number specifies the value to add to smallchange or largechange when scrolling the first step (scrolling from scrollMin).</p>
  * @field 
  * @type {number}
  */
wijmo.superpanel.superpanel_hscroller.prototype.firstStepChangeFix = null;
/** @class wijsuperpanel
  * @widget 
  * @namespace jQuery.wijmo.superpanel
  * @extends wijmo.wijmoWidget
  */
wijmo.superpanel.wijsuperpanel = function () {};
wijmo.superpanel.wijsuperpanel.prototype = new wijmo.wijmoWidget();
/** Destroys wijsuperpanel widget and reset the DOM element. */
wijmo.superpanel.wijsuperpanel.prototype.destroy = function () {};
/** Determine whether scoll the child DOM element to view 
  * need to scroll the scroll bar
  * @param {DOMElement} child The child to scroll to.
  */
wijmo.superpanel.wijsuperpanel.prototype.needToScroll = function (child) {};
/** Scroll children DOM element to view.
  * @param {DOMElement} child The child to scroll to.
  */
wijmo.superpanel.wijsuperpanel.prototype.scrollChildIntoView = function (child) {};
/** Scroll to horizontal position.
  * @param {number} x The position to scroll to.
  * @param {bool} isScrollValue A value that indicates whether x is value or pixel.
  */
wijmo.superpanel.wijsuperpanel.prototype.hScrollTo = function (x, isScrollValue) {};
/** Scroll to vertical position.
  * @param {number} y The position to scroll to.
  * @param {bool} isScrollValue A value that indicates whether y is value or pixel.
  */
wijmo.superpanel.wijsuperpanel.prototype.vScrollTo = function (y, isScrollValue) {};
/** Convert pixel to scroll value.
  * For example, wijsuperpanel scrolled 50px 
  * which is value 1 after conversion.
  * @param {number} px Length of scrolling.
  * @param {string} dir Scrolling direction. Options are: "h" and "v".
  */
wijmo.superpanel.wijsuperpanel.prototype.scrollPxToValue = function (px, dir) {};
/** Convert scroll value to pixel.
  * For example, scroll value is 1  
  * which makes wijsuperpanel scrolled 50px after conversion.
  * @param {number} scroll value.
  * @param {string} dir Scrolling direction. Options are: "h" and "v".
  */
wijmo.superpanel.wijsuperpanel.prototype.scrollValueToPx = function (scroll, dir) {};
/** Scroll to the specified position.
  * which is value 1 after conversion.
  * @param {number} x Horizontal position to scroll to.
  * @param {number} y Vertical position to scroll to.
  * @param {bool} isScrollValue A value that indicates whether x, y are value or pixel.
  */
wijmo.superpanel.wijsuperpanel.prototype.scrollTo = function (x, y, isScrollValue) {};
/** Refreshes wijsuperpanel. 
  * Needs to be called after content being changed.
  * @returns {bool} Returns true if it is successful, else returns false.
  */
wijmo.superpanel.wijsuperpanel.prototype.refresh = function () {};
/** Refreshes wijsuperpanel. 
  * Needs to be called after content being changed.
  * @param unfocus
  * @returns {bool} Returns true if it is successful, else returns false.
  */
wijmo.superpanel.wijsuperpanel.prototype.paintPanel = function (unfocus) {};
/** Gets the content element of wijsuperpanel.
  * @returns {jQuery}
  * @example
  * $("selector").wijsuperpanel("getContentElement");
  */
wijmo.superpanel.wijsuperpanel.prototype.getContentElement = function () {};

/** @class */
var wijsuperpanel_options = function () {};
/** <p class='defaultValue'>Default value: false</p>
  * <p>The value determines whether the wijsuperpanel can be resized.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijsuperpanel_options.prototype.allowResize = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>This value determines whether wijsuperpanel is automatically refreshed when the content size or wijsuperpanel size are changed.
  * Set this value to true if you load images in the wijsuperpanel without specifying their sizes.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijsuperpanel_options.prototype.autoRefresh = false;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.superpanel.superpanel_animation.html'>wijmo.superpanel.superpanel_animation</a></p>
  * <p>The animationOptions function determines whether or not the animation is shown. If true, it defines the animation effect and controls other aspects of the widget's animation, such as duration, queue, and easing.</p>
  * @field 
  * @type {wijmo.superpanel.superpanel_animation}
  * @option 
  * @remarks
  * Set this options to null to disable animation.
  * @example
  * $('#superPanel').wijsuperpanel({
  *     animationOptions: {
  *         disabled: false,
  *         duration: 1000,
  *         easing: easeInQuad
  *     }
  *   });
  */
wijsuperpanel_options.prototype.animationOptions = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.superpanel.superpanel_hscroller.html'>wijmo.superpanel.superpanel_hscroller</a></p>
  * <p>This option contains horizontal scroller settings.</p>
  * @field 
  * @type {wijmo.superpanel.superpanel_hscroller}
  * @option
  */
wijsuperpanel_options.prototype.hScroller = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>This value determines whether wijsuperpanel provides keyboard scrolling support.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijsuperpanel_options.prototype.keyboardSupport = false;
/** <p class='defaultValue'>Default value: 100</p>
  * <p>This value determines the time interval to call the scrolling function when doing continuous scrolling.</p>
  * @field 
  * @type {number}
  * @option
  */
wijsuperpanel_options.prototype.keyDownInterval = 100;
/** <p class='defaultValue'>Default value: true</p>
  * <p>This value determines whether wijsuperpanel has mouse wheel support.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * Mouse wheel plugin is needed to support this feature.
  */
wijsuperpanel_options.prototype.mouseWheelSupport = true;
/** <p class='defaultValue'>Default value: true</p>
  * <p>This value determines whether to fire the mouse wheel event when wijsuperpanel is scrolled to the end.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijsuperpanel_options.prototype.bubbleScrollingEvent = true;
/** <p>This option determines the behavior of the resizable widget. See the JQuery UI resizable options document for more information.</p>
  * @field 
  * @type {object}
  * @option
  */
wijsuperpanel_options.prototype.resizableOptions = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>This value determines whether to show the rounded corner of wijsuperpanel.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijsuperpanel_options.prototype.showRounder = true;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.superpanel.superpanel_vscroller.html'>wijmo.superpanel.superpanel_vscroller</a></p>
  * <p>This option contains vertical scroller settings.</p>
  * @field 
  * @type {wijmo.superpanel.superpanel_vscroller}
  * @option
  */
wijsuperpanel_options.prototype.vScroller = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines if use custom scrolling.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijsuperpanel_options.prototype.customScrolling = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines if the native scroll events should be listened.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijsuperpanel_options.prototype.listenContentScroll = false;
/** The hScrollerActivating event handler. 
  * A function called when horizontal scrollbar is activating.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.superpanel.IHScrollerActivatingEventArgs} data Information about an event
  */
wijsuperpanel_options.prototype.hScrollerActivating = null;
/** Resized event handler. This function gets called when the resized event is fired.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijsuperpanel_options.prototype.resized = null;
/** This function gets called when the user stops dragging the thumb buttons of the scrollbars.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.superpanel.IDragStopEventArgs} data Information about an event
  */
wijsuperpanel_options.prototype.dragStop = null;
/** This function gets called after panel is painted.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijsuperpanel_options.prototype.painted = null;
/** Scrolling event handler. A function called before scrolling occurs.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.superpanel.IScrollingEventArgs} data Information about an event
  */
wijsuperpanel_options.prototype.scrolling = null;
/** Scroll event handler. This function is called before scrolling occurs.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.superpanel.IScrollEventArgs} data Information about an event
  */
wijsuperpanel_options.prototype.scroll = null;
/** Scrolled event handler. This function gets called after scrolling occurs.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.superpanel.IScrolledEventArgs} data Information about an event
  */
wijsuperpanel_options.prototype.scrolled = null;
/** A function called when the vertical scrollbar is activating.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.superpanel.IVScrollerActivatingEventArgs} data Information about an event
  */
wijsuperpanel_options.prototype.vScrollerActivating = null;
wijmo.superpanel.wijsuperpanel.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijsuperpanel_options());
$.widget("wijmo.wijsuperpanel", $.wijmo.widget, wijmo.superpanel.wijsuperpanel.prototype);
/** @interface superpanel_animation
  * @namespace wijmo.superpanel
  */
wijmo.superpanel.superpanel_animation = function () {};
/** <p>This value determines whether to queue animation operations.</p>
  * @field 
  * @type {boolean}
  */
wijmo.superpanel.superpanel_animation.prototype.queue = null;
/** <p>This value determines whether to disable animation operations.</p>
  * @field 
  * @type {boolean}
  */
wijmo.superpanel.superpanel_animation.prototype.disabled = null;
/** <p>This value sets the animation duration of the scrolling animation.</p>
  * @field 
  * @type {number}
  */
wijmo.superpanel.superpanel_animation.prototype.duration = null;
/** <p>Sets the type of animation easing effect that users experience as the panel is scrolled. 
  * You can create custom easing animations using jQuery UI Easings.</p>
  * @field 
  * @type {string}
  */
wijmo.superpanel.superpanel_animation.prototype.easing = null;
/** Contains information about wijsuperpanel.hScrollerActivating event
  * @interface IHScrollerActivatingEventArgs
  * @namespace wijmo.superpanel
  */
wijmo.superpanel.IHScrollerActivatingEventArgs = function () {};
/** <p>The direction of the scrolling action.
  * Possible values: "v"(vertical) and "h"(horizontal).</p>
  * @field 
  * @type {string}
  */
wijmo.superpanel.IHScrollerActivatingEventArgs.prototype.direction = null;
/** <p>The width of the horizontal scrollbar.</p>
  * @field 
  * @type {object}
  */
wijmo.superpanel.IHScrollerActivatingEventArgs.prototype.targetBarLen = null;
/** <p>The width of the content.</p>
  * @field 
  * @type {object}
  */
wijmo.superpanel.IHScrollerActivatingEventArgs.prototype.contentLength = null;
/** Contains information about wijsuperpanel.dragStop event
  * @interface IDragStopEventArgs
  * @namespace wijmo.superpanel
  */
wijmo.superpanel.IDragStopEventArgs = function () {};
/** <p>The direction of the scrolling action.
  * Possible values: "v"(vertical) and "h"(horizontal).</p>
  * @field 
  * @type {string}
  */
wijmo.superpanel.IDragStopEventArgs.prototype.dir = null;
/** Contains information about wijsuperpanel.scrolling event
  * @interface IScrollingEventArgs
  * @namespace wijmo.superpanel
  */
wijmo.superpanel.IScrollingEventArgs = function () {};
/** <p>The direction of the scrolling action.
  * Possible values: "v"(vertical) and "h"(horizontal).</p>
  * @field 
  * @type {string}
  */
wijmo.superpanel.IScrollingEventArgs.prototype.dir = null;
/** <p>The scrollValue before scrolling occurs.</p>
  * @field 
  * @type {number}
  */
wijmo.superpanel.IScrollingEventArgs.prototype.oldValue = null;
/** <p>The scrollValue after scrolling occurs.</p>
  * @field 
  * @type {number}
  */
wijmo.superpanel.IScrollingEventArgs.prototype.newValue = null;
/** <p>The position of content before scrolling occurs.</p>
  * @field 
  * @type {object}
  */
wijmo.superpanel.IScrollingEventArgs.prototype.beforePosition = null;
/** Contains information about wijsuperpanel.scroll event
  * @interface IScrollEventArgs
  * @namespace wijmo.superpanel
  */
wijmo.superpanel.IScrollEventArgs = function () {};
/** <p>The direction of the scrolling action.
  * Possible values: "v"(vertical) and "h"(horizontal).</p>
  * @field 
  * @type {string}
  */
wijmo.superpanel.IScrollEventArgs.prototype.dir = null;
/** <p>TThe options of the animation which scrolling uses.</p>
  * @field 
  * @type {object}
  */
wijmo.superpanel.IScrollEventArgs.prototype.animationOptions = null;
/** <p>The position of content after scrolling occurs.</p>
  * @field 
  * @type {object}
  */
wijmo.superpanel.IScrollEventArgs.prototype.position = null;
/** Contains information about wijsuperpanel.scrolled event
  * @interface IScrolledEventArgs
  * @namespace wijmo.superpanel
  */
wijmo.superpanel.IScrolledEventArgs = function () {};
/** <p>The direction of the scrolling action.
  * Possible values: "v"(vertical) and "h"(horizontal).</p>
  * @field 
  * @type {string}
  */
wijmo.superpanel.IScrolledEventArgs.prototype.dir = null;
/** <p>The position of content before scrolling occurs.</p>
  * @field 
  * @type {object}
  */
wijmo.superpanel.IScrolledEventArgs.prototype.beforePosition = null;
/** <p>The position of content after scrolling occurs.</p>
  * @field 
  * @type {object}
  */
wijmo.superpanel.IScrolledEventArgs.prototype.afterPosition = null;
/** Contains information about wijsuperpanel.vScrollerActivating event
  * @interface IVScrollerActivatingEventArgs
  * @namespace wijmo.superpanel
  */
wijmo.superpanel.IVScrollerActivatingEventArgs = function () {};
/** <p>The direction of the scrolling action.
  * Possible values: "v"(vertical) and "h"(horizontal).</p>
  * @field 
  * @type {string}
  */
wijmo.superpanel.IVScrollerActivatingEventArgs.prototype.direction = null;
/** <p>The width of the vertical scrollbar.</p>
  * @field 
  * @type {object}
  */
wijmo.superpanel.IVScrollerActivatingEventArgs.prototype.targetBarLen = null;
/** <p>The width of the content.</p>
  * @field 
  * @type {object}
  */
wijmo.superpanel.IVScrollerActivatingEventArgs.prototype.contentLength = null;
})()
})()
