var wijmo = wijmo || {};

(function () {
wijmo.gauge = wijmo.gauge || {};

(function () {
/** @class wijgauge
  * @widget 
  * @namespace jQuery.wijmo.gauge
  * @extends wijmo.wijmoWidget
  */
wijmo.gauge.wijgauge = function () {};
wijmo.gauge.wijgauge.prototype = new wijmo.wijmoWidget();
/** Redraw the gauge. */
wijmo.gauge.wijgauge.prototype.redraw = function () {};
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.gauge.wijgauge.prototype.destroy = function () {};
/** Returns a reference to the Raphael canvas object.
  * @returns {Raphael Element} Reference to raphael canvas object.
  */
wijmo.gauge.wijgauge.prototype.getCanvas = function () {};

/** @class */
var wijgauge_options = function () {};
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Sets the value of the gauge, indicated by the pointer.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * This value must fall between the min and max values that you set.
  */
wijgauge_options.prototype.value = 0;
/** <p class='defaultValue'>Default value: 100</p>
  * <p>Sets the maximum value of the gauge.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Use this option along with min to set the numeric scale of values that are shown on the gauge. This setting limits
  * your valid values for other options, such as value and ranges.
  */
wijgauge_options.prototype.max = 100;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Sets the minimum value of the gauge.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Use this option along with min to set the numeric scale of values that are shown on the gauge. This setting limits
  * your valid values for other options, such as value and ranges.
  */
wijgauge_options.prototype.min = 0;
/** <p class='defaultValue'>Default value: 600</p>
  * <p>Sets the width of the gauge area in pixels.</p>
  * @field 
  * @type {number}
  * @option
  */
wijgauge_options.prototype.width = 600;
/** <p class='defaultValue'>Default value: 400</p>
  * <p>Sets the height of the gauge area in pixels.</p>
  * @field 
  * @type {number}
  * @option
  */
wijgauge_options.prototype.height = 400;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value that indicates whether to redraw the gauge automatically when resizing the gauge element.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijgauge_options.prototype.autoResize = true;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.gauge.gauge_tick.html'>wijmo.gauge.gauge_tick</a></p>
  * <p>Sets appearance options for the major tick marks that appear next to the numeric labels around the face of the gauge.</p>
  * @field 
  * @type {wijmo.gauge.gauge_tick}
  * @option 
  * @example
  * $(document).ready(function () {
  *        $("#radialgauge1").wijradialgauge({
  *         value: 90,
  *         tickMajor: {
  *         position: "inside",
  *         style: { fill: "purple", stroke: "#1E395B"},
  *         factor: 2.5,
  *         marker: 'diamond',
  *         visible: true,
  *         offset: 27,
  *         interval: 20
  *         }
  *     });
  * });
  */
wijgauge_options.prototype.tickMajor = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.gauge.gauge_tick.html'>wijmo.gauge.gauge_tick</a></p>
  * <p>A value that provides information for the minor tick.</p>
  * @field 
  * @type {wijmo.gauge.gauge_tick}
  * @option 
  * @example
  * //This example renders the minor tick marks as purple crosses, at an interval of once every 2 numbers
  *    $(document).ready(function () {
  *        $("#radialgauge1").wijradialgauge({
  *     value: 90,
  *     tickMinor: {
  *     position: "inside",
  *     style: { fill: "#1E395B", stroke: "purple"},
  *     factor: 2,
  *     marker: 'cross',
  *     visible: true,
  *     offset: 30,
  *     interval: 2
  *     }
  *     });
  *     });
  */
wijgauge_options.prototype.tickMinor = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.gauge.gauge_pointer.html'>wijmo.gauge.gauge_pointer</a></p>
  * <p>A value that includes all settings of the gauge pointer.</p>
  * @field 
  * @type {wijmo.gauge.gauge_pointer}
  * @option 
  * @example
  * // The example above renders the pointer as a purple-outlined blue rectangle of 125% the length of the radius
  * // by 10 pixels wide, offset back through the cap by 50% of the length of the radius
  *    $(document).ready(function () {
  *        $("#radialgauge1").wijradialgauge({
  *     value: 90,
  *     cap: {visible: true},
  *     pointer: {
  *     length: 1.25,
  *     offset: 0.5,
  *     shape: "rect",
  *     style: { fill: "blue", stroke: "purple"},
  *     width: 10
  *     }
  *     });
  *     });
  */
wijgauge_options.prototype.pointer = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>The islogarithmic option, inherited from the jquery.wijmo.wijgauge.js base class, indicates whether to use logarithmic
  * scaling for gauge label numeric values.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * This adds space between tick marks that corresponds to the percentage of change between those numbers rather than
  * absolute arithmetic values. You would want to use logarithmic scaling if you were displaying really high numbers,
  * because it goes higher much more quickly. A linear scale is more difficult to use if you are displaying really high
  * numbers. Note: By default, Wijmo uses a logarithmic base of 10, the common logarithm. See logarithmicBase for
  * information on changing this value.
  * @example
  * // The following code creates a gauge with the number labels and tick marks arranged as in the following image.
  *    $(document).ready(function () {
  *        $("#radialgauge1").wijradialgauge({
  *     value: 90,
  *     islogarithmic: true
  *     });
  *     });
  */
wijgauge_options.prototype.islogarithmic = false;
/** <p class='defaultValue'>Default value: 10</p>
  * <p>The logarithmicBase option, inherited from the jquery.wijmo.wijgauge.js base class, indicates the logarithmic
  * base to use if the islogarithmic option is set to true. The logarithmic base is the number to raise to produce the exponent.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * For example, with the default base 10, a logarithm of 3 produces 1000, or 10 to the power of 3, or 10³ = 10 x 10 x 10 = 1000.
  * If you change the base to 2, a logarithm of 3 produces 8, or 2³ = 2 x 2 x 2 = 8. You can use the natural
  * logarithm (using a base of ≈ 2.718) by specifying the value Math.e.
  */
wijgauge_options.prototype.logarithmicBase = 10;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.gauge.gauge_label.html'>wijmo.gauge.gauge_label</a></p>
  * <p>Sets all of the appearance options of the numeric labels that appear around the edge of the gauge.</p>
  * @field 
  * @type {wijmo.gauge.gauge_label}
  * @option 
  * @example
  * // This example sets the color for the labels to purple, and the font to 14 point, bold, Times New Roman.
  * $(document).ready(function () {
  *    $("#radialgauge1").wijradialgauge({
  *     value: 90,
  *     labels: {
  *     style: {
  *         fill: "purple",
  *             "font-size": "14pt",
  *             "font-weight": "bold",
  *             "font-family": "Times New Roman"
  *         }
  *     }
  * });
  * });
  */
wijgauge_options.prototype.labels = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.gauge.gauge_animation.html'>wijmo.gauge.gauge_animation</a></p>
  * <p>Defines the animation effect, controlling aspects such as duration and easing.</p>
  * @field 
  * @type {wijmo.gauge.gauge_animation}
  * @option
  */
wijgauge_options.prototype.animation = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.gauge.gauge_face.html'>wijmo.gauge.gauge_face</a></p>
  * <p>Sets or draws the image or shape to use for the face of the gauge and the background area.</p>
  * @field 
  * @type {wijmo.gauge.gauge_face}
  * @option 
  * @remarks
  * The origin is the center of the gauge, but the image draws from the top left, so we first calculate the starting point
  * of the top left based on the origin, and we calculate the width and height based on the radius of the face. The radius
  * of the face is half of the min of the width and height.
  * Note: The fill property is defined using the Raphael framework. Please see the Raphael Element attr method for more
  * information. The face can be filled with a simple color, or a gradient. The default fill is a radial gradient,
  * indicated by the r in the fill property.
  * @example
  * // This example uses a custom image for the face of the gauge. The argument that we name ui in the example is a JSON
  * // object. This object has a canvas, which is a Raphael paper object, and we use the image method of the Raphael
  * // paper that takes five parameters: source, x, y, width, and height. See the Raphael documentation for more information.
  * // We also set the radius to 120 pixels to render it inside the white area of the image.
  * $(document).ready(function () {
  * $("#radialgauge1").wijradialgauge({
  *     value: 90,
  *     radius: 120,
  *     face: {
  *         style: {},
  *         template: function (ui) {
  *             var url = "images/customGaugeFace.png";
  *             return ui.canvas.image(url, ui.origin.x -ui.r, ui.origin.y -ui.r, ui.r * 2, ui.r * 2);
  *         }
  *     }
  * });
  * });
  */
wijgauge_options.prototype.face = null;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Sets a value in pixels that indicates where to render the top edge of the gauge face.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Note: If the radius of the gauge is too large to fit within the rectangle defined by the width and height less the
  * specified margins, and the radius is set to the default value of "auto," Wijmo automatically resizes the gauge to fit.
  */
wijgauge_options.prototype.marginTop = 0;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Sets a value in pixels that indicates where to render the right edge of the gauge face.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Note: If the radius of the gauge is too large to fit within the rectangle defined by the width and height less
  * the specified margins, and the radius is set to the default value of "auto," Wijmo automatically resizes the gauge to fit.
  */
wijgauge_options.prototype.marginRight = 0;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Sets a value in pixels that indicates where to render the bottom edge of the gauge face.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Note: If the radius of the gauge is too large to fit within the rectangle defined by the width and height less the
  * specified margins, and the radius is set to the default value of "auto," Wijmo automatically resizes the gauge to fit.
  */
wijgauge_options.prototype.marginBottom = 0;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Sets a value in pixels that indicates where to render the left edge of the gauge face.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Note: If the radius of the gauge is too large to fit within the rectangle defined by the width and height less the
  * specified margins, and the radius is set to the default value of "auto," Wijmo automatically resizes the gauge to fit.
  */
wijgauge_options.prototype.marginLeft = 0;
/** <p class='defaultValue'>Default value: []</p>
  * <p>Allows you to create an array of ranges to highlight where values fall within the gauge.</p>
  * @field 
  * @type {array}
  * @option 
  * @remarks
  * For example, a red range, a yellow range, and a green range. Each range is drawn in the form of a curved bar.
  * You can control every aspect of each range with the settings detailed below.
  * Options available for each range include:
  * startWidth – Sets the thickness of the left side of the range bar in pixels.
  * endWidth – Sets the thickness of the right side of the range bar in pixels.
  * startValue – Sets the value at which to begin drawing the range bar.This value must fall between the max and min values you set for the gauge.
  * endValue – Sets the value at which to end the range bar.This value must fall between the max and min values you set for the gauge.
  * startDistance – Sets the distance from the center of the gauge to draw the left side of the range bar.
  * endDistance – Sets the distance from the center of the gauge to draw the right side of the range bar.
  * style – Sets the colors to use in drawing the range bar with the following options:
  */
wijgauge_options.prototype.ranges = [];
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether to render the gauge in reverse order, with the numbering going from highest to lowest.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijgauge_options.prototype.isInverted = false;
/** Fires before the value changes, this event can be called.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IGaugeEventArgs} data Information about an event
  */
wijgauge_options.prototype.beforeValueChanged = null;
/** Fires before the value changes, this event can be called.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IGaugeEventArgs} data Information about an event
  */
wijgauge_options.prototype.valueChanged = null;
/** Fires before the canvas is painted. This event can be cancelled. "return false;" to cancel the event.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijgauge_options.prototype.painted = null;
wijmo.gauge.wijgauge.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijgauge_options());
$.widget("wijmo.wijgauge", $.wijmo.widget, wijmo.gauge.wijgauge.prototype);
/** @interface gauge_tick
  * @namespace wijmo.gauge
  */
wijmo.gauge.gauge_tick = function () {};
/** <p>A value that indicates the position of the major tick marks in relation to the edge of the face</p>
  * @field 
  * @type {String}
  * @remarks
  * Valid Values:
  * "inside" -- Draws the major tick marks inside the edge of the face.
  * "outside" -- Draws the major tick marks outside the edge of the face.
  * "cross" -- Draws the major tick marks centered on the edge of the face.
  */
wijmo.gauge.gauge_tick.prototype.position = null;
/** <p>A value that indicates the fill color and outline (stroke) of the major tick mark.</p>
  * @field
  */
wijmo.gauge.gauge_tick.prototype.style = null;
/** <p>A value that indicates how long to draw the major tick marks as a factor of the default length of the minor tick marks.</p>
  * @field 
  * @type {number}
  */
wijmo.gauge.gauge_tick.prototype.factor = null;
/** <p>A value that indicates whether to show the major tick mark.</p>
  * @field 
  * @type {boolean}
  */
wijmo.gauge.gauge_tick.prototype.visible = null;
/** <p>A value that indicates the shape to use in drawing major tick marks.</p>
  * @field 
  * @type {string}
  * @remarks
  * Options are 'rect', 'tri', 'circle', 'invertedTri', 'box', 'cross', 'diamond'.
  */
wijmo.gauge.gauge_tick.prototype.marker = null;
/** <p>A value that indicates the distance in pixels from the edge of the face to draw the major tick marks. 
  * The numeric labels are drawn a few pixels</p>
  * @field 
  * @type {number}
  */
wijmo.gauge.gauge_tick.prototype.offset = null;
/** <p>A value that indicates the frequency of the major tick marks and their numeric labels.</p>
  * @field 
  * @type {number}
  * @remarks
  * A setting of 1 renders a major tick mark for every number between the min and max.This setting is useful when 
  * the spread between min and max is small. The default setting of 10 renders a major tick mark once every 10 numbers
  * between the min and the max. A setting of 100 renders a major tick mark once every 100 numbers between the min
  * and max. This setting is useful when the spread between min and max is very large.
  */
wijmo.gauge.gauge_tick.prototype.interval = null;
/** @interface gauge_pointer
  * @namespace wijmo.gauge
  */
wijmo.gauge.gauge_pointer = function () {};
/** <p>Sets the length of the pointer as a percentage of the radius(or width/height in lineargauge) of the gauge.
  * You can set the length to be greater than the radius(or width/height).</p>
  * @field 
  * @type {number}
  */
wijmo.gauge.gauge_pointer.prototype.length = null;
/** <p>Sets the fill and outline (stroke) colors of the pointer.</p>
  * @field
  */
wijmo.gauge.gauge_pointer.prototype.style = null;
/** <p>Sets the width of the pointer in pixels.</p>
  * @field 
  * @type {number}
  */
wijmo.gauge.gauge_pointer.prototype.width = null;
/** <p>Sets the percentage of the pointer that is shoved backward through the cap.</p>
  * @field 
  * @type {number}
  */
wijmo.gauge.gauge_pointer.prototype.offset = null;
/** <p>Sets the shape in which to render the pointer: triangular or rectangular.</p>
  * @field 
  * @type {string}
  * @remarks
  * Options are 'rect', 'tri'.
  */
wijmo.gauge.gauge_pointer.prototype.shape = null;
/** <p>A value that indicates whether to show the pointer.</p>
  * @field 
  * @type {boolean}
  */
wijmo.gauge.gauge_pointer.prototype.visible = null;
/** <p>A JavaScript callback value that returns a Raphael element that draws the pointer.</p>
  * @field 
  * @type {Function}
  * @remarks
  * Use this option to customize the pointer. In order to use the template, you must know how to draw Raphael
  * graphics For more information,see the Raphael documentation.
  * In radial gauge：
  * The pointer template's callback contains two arguments:
  * startLocation -- The starting point from which to draw the pointer. This argument is defined by x and y coordinates.
  * pointerInfo -- A JSON object that extends the gauge's pointer options:
  * offset -- Sets the percentage of the pointer that is shoved backward through the origin.
  * length -- Sets the absolute value in pixels of the length of the pointer.
  * gaugeBBox -- An object that sets the bounding box of the gauge, as defined by x and y coordinates and width and height options.
  */
wijmo.gauge.gauge_pointer.prototype.template = null;
/** @interface gauge_label
  * @namespace wijmo.gauge
  */
wijmo.gauge.gauge_label = function () {};
/** <p>A value that indicates the globalized format to use for the labels.</p>
  * @field 
  * @type {string}
  * @remarks
  * For more information on using jQuery globalize with Wijmo 
  * Note: If the value is a function rather than a string, the function formats the value and returns it to the gauge.
  */
wijmo.gauge.gauge_label.prototype.format = null;
/** <p>A value that indicates the color, weight, and size of the numeric labels.</p>
  * @field
  */
wijmo.gauge.gauge_label.prototype.style = null;
/** <p>A value that indicates whether to show the numeric labels.</p>
  * @field 
  * @type {boolean}
  */
wijmo.gauge.gauge_label.prototype.visible = null;
/** <p>A value in pixels that indicates the distance of the numeric labels from the outer reach of the pointer.</p>
  * @field 
  * @type {number}
  * @remarks
  * A value of 50 pixels renders the labels outside the circular gauge area, cutting off the numbers along
  * the top. A value of 0 pixels renders the labels just inside the tick marks.
  */
wijmo.gauge.gauge_label.prototype.offset = null;
/** @interface gauge_animation
  * @namespace wijmo.gauge
  */
wijmo.gauge.gauge_animation = function () {};
/** <p>A value that determines whether to show animation.</p>
  * @field 
  * @type {boolean}
  */
wijmo.gauge.gauge_animation.prototype.enabled = null;
/** <p>The duration option defines the length of the animation effect in milliseconds.</p>
  * @field 
  * @type {number}
  */
wijmo.gauge.gauge_animation.prototype.duration = null;
/** <p>The easing option uses Raphael easing formulas to add effects to the animation, such as allowing an item to
  * bounce realistically.</p>
  * @field 
  * @type {string}
  * @remarks
  * Valid Values (see http://raphaeljs.com/easing.html for easing demos):
  */
wijmo.gauge.gauge_animation.prototype.easing = null;
/** @interface gauge_face
  * @namespace wijmo.gauge
  */
wijmo.gauge.gauge_face = function () {};
/** <p>A value that indicates the fill color (or gradient), and the outline color and width of the gauge face.</p>
  * @field
  */
wijmo.gauge.gauge_face.prototype.style = null;
/** <p>A JavaScript callback value that returns a Raphael element (or set) that draws the gauge face.</p>
  * @field 
  * @type {Function}
  * @remarks
  * If you are only using one shape, the function returns a Raphael element. If you define multiple shapes,
  * have the function create a Raphael set object, push all of the Raphael elements to the set, and return the
  * set to wijgauge. In order to use the template, you must know how to draw Raphael graphics.
  * For more information, see the Raphael documentation.
  * In radial gauge
  * The face template's callback contains one argument with the following parameters:
  * origin -- The starting point from which to draw the center of the face.This argument is defined by x and y coordinates.
  * canvas -- A Raphael paper object that you can use to draw the custom graphic to use as the face.
  */
wijmo.gauge.gauge_face.prototype.template = null;
/** @interface IGaugeEventArgs
  * @namespace wijmo.gauge
  */
wijmo.gauge.IGaugeEventArgs = function () {};
/** <p>The gauge's old value.</p>
  * @field 
  * @type {number}
  */
wijmo.gauge.IGaugeEventArgs.prototype.oldValue = null;
/** <p>The value to be seted.</p>
  * @field 
  * @type {number}
  */
wijmo.gauge.IGaugeEventArgs.prototype.newValue = null;
})()
})()
