var wijmo = wijmo || {};

(function () {
wijmo.input = wijmo.input || {};

(function () {
/** @class wijinputmask
  * @widget 
  * @namespace jQuery.wijmo.input
  * @extends wijmo.input.wijinputcore
  */
wijmo.input.wijinputmask = function () {};
wijmo.input.wijinputmask.prototype = new wijmo.input.wijinputcore();
/** Sets the text displayed in the input box.
  * @param text
  * @example
  * // This example sets text of a wijinputcore to "Hello"
  * $(".selector").wijinputcore("setText", "Hello");
  */
wijmo.input.wijinputmask.prototype.setText = function (text) {};
/** Gets the text displayed in the input box.
  * @returns {string}
  */
wijmo.input.wijinputmask.prototype.getText = function () {};
/** Selects a range of text in the widget.
  * @param {number} start Start of the range.
  * @param {number} end End of the range.
  * @example
  * // Select first two symbols in a wijinputmask
  * $(".selector").wijinputmask("selectText", 0, 2);
  */
wijmo.input.wijinputmask.prototype.selectText = function (start, end) {};
/** Gets the selected text. */
wijmo.input.wijinputmask.prototype.getSelectedText = function () {};
/** Gets the text value when the container 
  * form is posted back to server.
  */
wijmo.input.wijinputmask.prototype.getPostValue = function () {};
/** Determines whether the widget has the focus.
  * @returns {bool}
  */
wijmo.input.wijinputmask.prototype.isFocused = function () {};

/** @class */
var wijinputmask_options = function () {};
/** <p>undefined</p>
  * @field 
  * @option
  */
wijinputmask_options.prototype.wijCSS = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Gets whether the control automatically converts to the proper format according to the format setting.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * // In this example, when input the lower case alphabet, it will be convert to upper case alphabet.
  * $(".selector").wijinputmask({    
  *   maskFormat: /\A{3}-\A{4}/,
  *   autoConvert:true
  * });
  */
wijinputmask_options.prototype.autoConvert = true;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Determines the default text.</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputmask_options.prototype.text = null;
/** <p class='defaultValue'>Default value: 'none'</p>
  * <p>Determines whether to highlight the control's text on receiving input focus.
  * Possible values are : "none", "field", "all";
  * The default value is "none";</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputmask_options.prototype.highlightText = 'none';
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the input mask to use at run time.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * &lt;pre&gt;
  * The property can be a string composed of one or more of the masking elements as shown in the following table,
  * 0  --------- Digit, required. This element will accept any single digit between 0 and 9.&lt;br /&gt;
  * 9  --------- Digit or space, optional. 
  * #  --------- Digit or space, optional. Plus (+) and minus (-) signs are allowed.      
  * L  --------- Letter, required. Restricts input to the ASCII letters a-z and A-Z.      
  * This mask element is equivalent to [a-zA-Z] in regular expressions.      
  * ?  --------- Letter, optional. Restricts input to the ASCII letters a-z and A-Z.      
  * This mask element is equivalent to [a-zA-Z]? in regular expressions.     
  * &amp;amp;  --------- Character, required.&lt;br /&gt;
  * C  --------- Character, optional. Any non-control character.&lt;br /&gt;
  * A  --------- Alphanumeric, optional. 
  * a  --------- Alphanumeric, optional. 
  * .  --------- Decimal placeholder. The actual display character used will be the decimal placeholder appropriate to the culture option.&lt;br /&gt;
  * ,  --------- Thousands placeholder. The actual display character used will be the thousands placeholder appropriate to the culture option. 
  * :  --------- Time separator. The actual display character used will be the time placeholder appropriate to the culture option. 
  * /  --------- Date separator. The actual display character used will be the date placeholder appropriate to the culture option. 
  * $  --------- Currency symbol. The actual character displayed will be the currency symbol appropriate to the culture option. 
  * &amp;lt;  --------- Shift down. Converts all characters that follow to lowercase. 
  * &amp;gt;  --------- Shift up. Converts all characters that follow to uppercase. 
  * |  --------- Disable a previous shift up or shift down. 
  * H  --------- All SBCS characters. 
  * K  --------- SBCS Katakana. 
  * ９ --------- DBCS Digit. 
  * Ｋ --------- DBCS Katakana. 
  * Ｊ --------- Hiragana. 
  * Ｚ --------- All DBCS characters.  
  * N  --------- All SBCS big Katakana.
  * Ｎ --------- Matches DBCS big Katakana.
  * Ｇ --------- Matches DBCS big Hiragana.
  * \\ --------- Escape. Escapes a mask character, turning it into a literal. The escape sequence for a backslash is: \\\\  
  * All other characters, Literals. All non-mask elements appear as themselves within wijinputmask. 
  * Literals always occupy a static position in the mask at run time, and cannot be moved or deleted by the user. 
  * The following table shows example masks.
  * 00/00/0000            A date (day, numeric month, year) in international date format. The "/" character is a logical date separator, and will appear to the user as the date separator appropriate to the application's current culture. 
  * 00-&amp;gt;L&amp;lt;LL-0000         A date (day, month abbreviation, and year) in United States format in which the three-letter month abbreviation is displayed with an initial uppercase letter followed by two lowercase letters. 
  * (999)-000-0000        United States phone number, area code optional. If users do not want to enter the optional characters, they can either enter spaces or place the mouse pointer directly at the position in the mask represented by the first 0.  
  * $999,999.00           A currency value in the range of 0 to 999999. The currency, thousandth, and decimal characters will be replaced at run time with their culture-specific equivalents.  
  * For example: 
  * $(".selector").wijinputmask({   
  * maskFormat: "(999)-000-0000 " 
  * }); 
  * Value of maskFormat can also be a regex value, attentation, only the following key word supported by the regex value.&lt;br /&gt;
  * \A   ----------- Matches any upper case alphabet [A-Z]. 
  * \a   ----------- Matches any lower case alphabet [a-z]. 
  * \D   ----------- Matches any decimal digit. Same as [0-9]. 
  * \W   ----------- Matches any word character. It is same as [a-zA-Z_0-9]. 
  * \K   ----------- Matches SBCS Katakana. 
  * \H   ----------- Matches all SBCS characters. 
  * \Ａ  ----------- Matches any upper case DBCS alphabet [Ａ-Ｚ]. 
  * \ａ  ----------- Matches any lower case DBCS alphabet [ａ-ｚ]. 
  * \Ｄ  ----------- Matches any DBCS decimal digit. Same as [０-９]. 
  * \Ｗ  ----------- Matches any DBCS word character. It is same as [ａ-ｚＡ-Ｚ＿０-９]. 
  * \Ｋ  ----------- DBCS Katakana 
  * \Ｊ  ----------- Hiragana 
  * \Ｚ  ----------- All DBCS characters. 
  * \N   ----------- Matches all SBCS big Katakana.
  * \Ｎ  ----------- Matches DBCS big Katakana.
  * \Ｇ  ----------- Matches DBCS big Hiragana.
  * \Ｔ  ----------- Matchese surrogate char.
  * [^] Used to express an exclude subset. 
  * - Used to define a contiguous character range. 
  * {} Specifies that the pattern. 
  * * The short expression of {0,}. 
  * + The short expression of {1,}. 
  * ? The short expression of {0,1}. 
  * The following shows some example use Regex value. 
  * \D{3}-\D{4}                      Zip Code (012-3456) 
  * ℡ \D{2,4}-\D{2,4}-\D{4}/        Phone number (℡ 012-345-6789) 
  * \D{2,4}-\D{2,4}-\D{4}            Phone number (012-345-6789) 
  * Attentation, because maskFormat requrie that it uses a regex value, so assign the value as follows example. 
  * &lt;/pre&gt;
  * @example
  * &lt;pre&gt;
  * $(".selector").wijinputmask({
  * maskFormat: /\D{3}-\D{4}/
  * }); &lt;br /&gt;
  * $(".selector").wijinputmask({
  * maskFormat: new RegExp("\\D{3}-\\D{4}")
  * });
  * &lt;/pre&gt;
  */
wijinputmask_options.prototype.maskFormat = "";
/** <p class='defaultValue'>Default value: '_'</p>
  * <p>Determines the character that appears when the widget has focus but no input has been entered.</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputmask_options.prototype.promptChar = '_';
/** <p class='defaultValue'>Default value: false</p>
  * <p>Indicates whether the prompt characters in the input mask are hidden when the input loses focus.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * This property doesn't take effect when maskFormat property is set to a Regex value.
  */
wijinputmask_options.prototype.hidePromptOnLeave = false;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines how an input character that matches 
  * the prompt character should be handled.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * This property doesn't take effect when maskFormat property is set to a Regex value.
  */
wijinputmask_options.prototype.resetOnPrompt = true;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Indicates whether promptChar can be entered as valid data by the user.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * This property doesn't take effect when maskFormat property is set to a Regex value.
  */
wijinputmask_options.prototype.allowPromptAsInput = false;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the character to be substituted for the actual input characters.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * This property doesn't take effect when maskFormat property is set to a Regex value.
  */
wijinputmask_options.prototype.passwordChar = "";
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines how a space input character should be handled.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * This property doesn't take effect when maskFormat property is set to a Regex value.
  */
wijinputmask_options.prototype.resetOnSpace = true;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Indicates whether the user is allowed to re-enter literal values.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * This property doesn't take effect when maskFormat property is set to a Regex value.
  */
wijinputmask_options.prototype.skipLiterals = true;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether or not the next control in the tab order receives 
  * 			the focus as soon as the control is filled at the last character.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijinputmask_options.prototype.blurOnLastChar = false;
/** <p class='defaultValue'>Default value: 'control'</p>
  * <p>Determines whether the focus will move to the next filed or the next control when press the tab key. 
  * Possible values are "control", "field".</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * The caret moves to the next field when this property is "field".
  * If the caret is in the last field, the input focus leaves this control when pressing the TAB key. 
  * If the value is "control", the behavior is similar to the standard control.
  */
wijinputmask_options.prototype.tabAction = 'control';
/** <p class='defaultValue'>Default value: 'auto'</p>
  * <p>Determines the input method setting of widget.
  * Possible values are: 'auto', 'active', 'inactive', 'disabled'</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * This property only take effect on IE and firefox browser.
  */
wijinputmask_options.prototype.imeMode = 'auto';
wijmo.input.wijinputmask.prototype.options = $.extend({}, true, wijmo.input.wijinputcore.prototype.options, new wijinputmask_options());
$.widget("wijmo.wijinputmask", $.wijmo.wijinputcore, wijmo.input.wijinputmask.prototype);
})()
})()
