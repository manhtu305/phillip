var wijmo = wijmo || {};

(function () {
wijmo.exporter = wijmo.exporter || {};

(function () {
/** Settings for eventscalendar exporting
  * @interface IEventsCalendarExportSettings
  * @namespace wijmo.exporter
  * @extends wijmo.exporter.ExportSetting
  */
wijmo.exporter.IEventsCalendarExportSettings = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.exporter.ExportMethod.html'>wijmo.exporter.ExportMethod</a></p>
  * <p>It has "Content" and "Options" mode.</p>
  * @field 
  * @type {wijmo.exporter.ExportMethod}
  */
wijmo.exporter.IEventsCalendarExportSettings.prototype.method = null;
/** <p>The theme name which is applied to exported eventscalendar</p>
  * @field 
  * @type {string}
  */
wijmo.exporter.IEventsCalendarExportSettings.prototype.theme = null;
typeof wijmo.exporter.ExportSetting != 'undefined' && $.extend(wijmo.exporter.IEventsCalendarExportSettings.prototype, wijmo.exporter.ExportSetting.prototype);
})()
})()
