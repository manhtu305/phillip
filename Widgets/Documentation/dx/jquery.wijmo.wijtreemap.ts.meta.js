var wijmo = wijmo || {};

(function () {
wijmo.treemap = wijmo.treemap || {};

(function () {
/** @class wijtreemap
  * @widget 
  * @namespace jQuery.wijmo.treemap
  * @extends wijmo.wijmoWidget
  */
wijmo.treemap.wijtreemap = function () {};
wijmo.treemap.wijtreemap.prototype = new wijmo.wijmoWidget();
/** This method refreshes the treemap. */
wijmo.treemap.wijtreemap.prototype.refresh = function () {};
/** The destroy method will remove the treemap functionality completely and will return the element to its pre-init state.
  * @example
  * $("selector").wijtreemap("destroy");
  */
wijmo.treemap.wijtreemap.prototype.destroy = function () {};

/** @class */
var wijtreemap_options = function () {};
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates the width of the treemap in pixels.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Note that this value overrides any value you may set in the &lt;div&gt; element that 
  * you use in the body of the HTML page.
  */
wijtreemap_options.prototype.width = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates the height of the treemap in pixels.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Note that this value overrides any value you may set in the &lt;div&gt; element that 
  * you use in the body of the HTML page.
  */
wijtreemap_options.prototype.height = null;
/** <p class='defaultValue'>Default value: 'squarified'</p>
  * <p>A value that indicates the type of treemap to be displayed.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Options are 'squarified', 'horizontal' and 'vertical'.
  */
wijtreemap_options.prototype.type = 'squarified';
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates the array to use as a source that you can bind to treemap.</p>
  * @field 
  * @type {array}
  * @option 
  * @remarks
  * Use the valueBinding, labelBinding, colorBinding option, and bind values to treemap.
  */
wijtreemap_options.prototype.data = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates the field to each item's value. Default value field is 'value' if valueBinding is not set.</p>
  * @field 
  * @type {string}
  * @option
  */
wijtreemap_options.prototype.valueBinding = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates the field to each item's label  Default label field is 'label' if labelBinding is not set.</p>
  * @field 
  * @type {string}
  * @option
  */
wijtreemap_options.prototype.labelBinding = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates the field to each item's color  Default color field is 'color' if colorBinding is not set.</p>
  * @field 
  * @type {string}
  * @option
  */
wijtreemap_options.prototype.colorBinding = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value that indicates whether to show the label.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtreemap_options.prototype.showLabel = true;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates a function which is used to format the label of treemap item.</p>
  * @field 
  * @type {Function}
  * @option
  */
wijtreemap_options.prototype.labelFormatter = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that indicates whether to show the tooltip.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtreemap_options.prototype.showTooltip = false;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates options of tooltip.</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * Its value is wijtooltip's option, visit
  * http://wijmo.com/docs/wijmo/#Wijmo~jQuery.fn.-~wijtooltip.html for more details.
  */
wijtreemap_options.prototype.tooltipOptions = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value that indicates whether to show the title.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtreemap_options.prototype.showTitle = true;
/** <p class='defaultValue'>Default value: 20</p>
  * <p>A value that indicates the height of the title.</p>
  * @field 
  * @type {number}
  * @option
  */
wijtreemap_options.prototype.titleHeight = 20;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates a function which is used to format the title of treemap item.</p>
  * @field 
  * @type {Function}
  * @option
  */
wijtreemap_options.prototype.titleFormatter = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates the color of min value.</p>
  * @field 
  * @type {string}
  * @option
  */
wijtreemap_options.prototype.minColor = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates min value. 
  * If this option is not set, treemap will calculate min value automatically.</p>
  * @field 
  * @type {number}
  * @option
  */
wijtreemap_options.prototype.minColorValue = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates the color of mid value.</p>
  * @field 
  * @type {string}
  * @option
  */
wijtreemap_options.prototype.midColor = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates mid value.
  * If this option is not set, treemap will calculate mid value automatically.</p>
  * @field 
  * @type {number}
  * @option
  */
wijtreemap_options.prototype.midColorValue = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates the color of max value.</p>
  * @field 
  * @type {string}
  * @option
  */
wijtreemap_options.prototype.maxColor = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates max value.
  * If this option is not set, treemap will calculate max value automatically.</p>
  * @field 
  * @type {number}
  * @option
  */
wijtreemap_options.prototype.maxColorValue = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that indicates whether to show back button after treemap drilled down.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtreemap_options.prototype.showBackButtons = false;
/** This event fires before item is painting. User can create customize item in this event.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {Object} data Data of item to be painted, set value to data.htmlTemplate to customize treemap item.
  */
wijtreemap_options.prototype.itemPainting = null;
/** This event fires after item is painted.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijtreemap_options.prototype.itemPainted = null;
/** This event fires before treemap is painting.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {JQuery} args The element of wijtreemap.
  */
wijtreemap_options.prototype.painting = null;
/** This event fires after treemap is painted.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijtreemap_options.prototype.painted = null;
/** This event fires before drill down. This event can be cancelled, use "return false;" to cancel the event.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.treemap.IDrillingDownEventArgs} data Information about an event
  */
wijtreemap_options.prototype.drillingDown = null;
/** This event fires after drill down.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {Object} data The data of item that is drilled down.
  */
wijtreemap_options.prototype.drilledDown = null;
/** This event fires before roll up. This event can be cancelled, use "return false;" to cancel the event.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.treemap.IRollingUpEventArgs} data Information about an event
  */
wijtreemap_options.prototype.rollingUp = null;
/** This event fires after roll up.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {Object} data The data of item that is rolled up.
  */
wijtreemap_options.prototype.rolledUp = null;
wijmo.treemap.wijtreemap.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijtreemap_options());
$.widget("wijmo.wijtreemap", $.wijmo.widget, wijmo.treemap.wijtreemap.prototype);
/** Contains information about wijtreemap.drillingDown event
  * @interface IDrillingDownEventArgs
  * @namespace wijmo.treemap
  */
wijmo.treemap.IDrillingDownEventArgs = function () {};
/** <p>The data of clicked item.</p>
  * @field
  */
wijmo.treemap.IDrillingDownEventArgs.prototype.clickItemData = null;
/** <p>The data of current item.</p>
  * @field
  */
wijmo.treemap.IDrillingDownEventArgs.prototype.currentData = null;
/** <p>The data of target item to be drilled down.</p>
  * @field
  */
wijmo.treemap.IDrillingDownEventArgs.prototype.targetData = null;
/** Contains information about wijtreemap.rollingUp event
  * @interface IRollingUpEventArgs
  * @namespace wijmo.treemap
  */
wijmo.treemap.IRollingUpEventArgs = function () {};
/** <p>The data of clicked item.</p>
  * @field
  */
wijmo.treemap.IRollingUpEventArgs.prototype.clickItemData = null;
/** <p>The data of current item.</p>
  * @field
  */
wijmo.treemap.IRollingUpEventArgs.prototype.currentData = null;
/** <p>The data of target item to be rolled up.</p>
  * @field
  */
wijmo.treemap.IRollingUpEventArgs.prototype.targetData = null;
})()
})()
