var wijmo = wijmo || {};

(function () {
wijmo.carousel = wijmo.carousel || {};

(function () {
/** @class wijcarousel
  * @widget 
  * @namespace jQuery.wijmo.carousel
  * @extends wijmo.wijmoWidget
  */
wijmo.carousel.wijcarousel = function () {};
wijmo.carousel.wijcarousel.prototype = new wijmo.wijmoWidget();
/** Removes the wijcarousel functionality completely. This returns the element to its pre-init state. */
wijmo.carousel.wijcarousel.prototype.destroy = function () {};
/** Refresh the carousel layout.Reset the layout, scrolled. */
wijmo.carousel.wijcarousel.prototype.refresh = function () {};
/** Starts automatically displaying each of the images in order. */
wijmo.carousel.wijcarousel.prototype.play = function () {};
/** Stops automatically displaying the images in order. */
wijmo.carousel.wijcarousel.prototype.pause = function () {};
/** Shows the next picture. */
wijmo.carousel.wijcarousel.prototype.next = function () {};
/** Shows the previous picture. */
wijmo.carousel.wijcarousel.prototype.previous = function () {};
/** Scrolls to the picture at the specified index.
  * @param {number} index The zero-based index of the picture to which to scroll.
  * @example
  * $("#element").wijcarousel("scrollTo", 2);
  */
wijmo.carousel.wijcarousel.prototype.scrollTo = function (index) {};
/** Add a custom item with specified index.
  * @param {string|jQuery} ui The node content or innerHTML.
  * @param {number} index Specified the postion to insert at.
  */
wijmo.carousel.wijcarousel.prototype.add = function (ui, index) {};
/** Remove the item at specified index.
  * @param {number} index Specified which item should be removed.
  */
wijmo.carousel.wijcarousel.prototype.remove = function (index) {};

/** @class */
var wijcarousel_options = function () {};
/** <p class='defaultValue'>Default value: []</p>
  * <p>An object collection that contains the data of the carousel.</p>
  * @field 
  * @type {array}
  * @option 
  * @example
  * :
  * $("#element").wijcarousel( { data: [{
  *     imageUrl: "../thumb/image1.jpg",
  *     linkUrl: "../images/image1.jpg",
  *     content: "",
  *     caption: "&lt;span&gt;Word Caption 1&lt;/span&gt;"
  * },{
  *     imageUrl: "../thumb/image2.jpg",
  *     linkUrl: "../images/image2.jpg",
  *     content: "",
  *     caption: "&lt;span&gt;Word Caption 2&lt;/span&gt;"
  * }] } );
  */
wijcarousel_options.prototype.data = [];
/** <p class='defaultValue'>Default value: false</p>
  * <p>Allows pictures to be played automatically.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijcarousel( { auto: true } );
  */
wijcarousel_options.prototype.auto = false;
/** <p class='defaultValue'>Default value: 5000</p>
  * <p>Determines the time span between two pictures showing in autoplay mode.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * $("#element").wijcarousel( { interval: 3000 } );
  */
wijcarousel_options.prototype.interval = 5000;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines if the timer of the carousel should be shown. 
  * If true, the timer appears by default at the top of the carousel with a play/pause button allowing carousel items to be automatically cycled through at run time.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijcarousel( { showTimer: true } );
  */
wijcarousel_options.prototype.showTimer = false;
/** <p class='defaultValue'>Default value: 'inside'</p>
  * <p>Determines the position value for next button and previous button. 
  * Possible values are: "inside" and "outside".</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#element").wijcarousel( { buttonPosition: "outside" } );
  */
wijcarousel_options.prototype.buttonPosition = 'inside';
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether to show the pager element. 
  * By default, if showPager is set to true, the pager will appear at the bottom right of the widget and allows run time carousel item navigation. 
  * You can customize the location and appearance of the pager by using the pagerPosition and pagerType options.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijcarousel( { showPager: true } );
  */
wijcarousel_options.prototype.showPager = false;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.carousel.btn_class_option.html'>wijmo.carousel.btn_class_option</a></p>
  * <p>Determines the class of custom previous button.
  * Includes the following sub-options "defaultClass", "hoverClass", "disableClass".</p>
  * @field 
  * @type {wijmo.carousel.btn_class_option}
  * @option 
  * @example
  * $("#element").wijcarousel( { prevBtnClass: {
  * }});
  */
wijcarousel_options.prototype.prevBtnClass = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.carousel.btn_class_option.html'>wijmo.carousel.btn_class_option</a></p>
  * <p>Determines the class of custom previous button.
  * Includes the following sub-options "defaultClass", "hoverClass", "disableClass".</p>
  * @field 
  * @type {wijmo.carousel.btn_class_option}
  * @option 
  * @example
  * $("#element").wijcarousel( { nextBtnClass: {
  * }});
  */
wijcarousel_options.prototype.nextBtnClass = null;
/** <p class='defaultValue'>Default value: 'numbers'</p>
  * <p>Determines the type of the pager in the carousel. 
  * Possible values are: "slider", "numbers", "dots", "thumbnails".
  * For a live example, see the Carousel Paging page in the Explore sample.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#element").wijcarousel( { pagerType: "numbers" } );
  */
wijcarousel_options.prototype.pagerType = 'numbers';
/** <p>Determines the thumbnails list for a pager when pagerType is "thumbnails".</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#element").wijcarousel( { thumbnails: [] } );
  */
wijcarousel_options.prototype.thumbnails = null;
/** <p>A value that indicates the position settings for the pager.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#element").wijcarousel( { 
  *     pagerPosition: {
  *         my: 'left bottom', 
  *         at: 'right top', 
  *         offset: '0 0'} 
  * });
  */
wijcarousel_options.prototype.pagerPosition = null;
/** <p class='defaultValue'>Default value: 'horizontal'</p>
  * <p>Determines the orientation of the pager. 
  * Possible values are: "vertical" &amp; "horizontal"</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#element").wijcarousel( { orientation: "vertical" } );
  */
wijcarousel_options.prototype.orientation = 'horizontal';
/** <p class='defaultValue'>Default value: 'horizontal'</p>
  * <p>Determines the orientation of the slider. 
  * Possible values are: "vertical" &amp; "horizontal"</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#element").wijcarousel( { sliderOrientation: "vertical" } );
  */
wijcarousel_options.prototype.sliderOrientation = 'horizontal';
/** <p class='defaultValue'>Default value: true</p>
  * <p>Allows the carousel to loop back to the beginning.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijcarousel( { loop: true } );
  */
wijcarousel_options.prototype.loop = true;
/** <p>The animation option determines whether and how images are scroll in the carousel. 
  * It defines the animation effect and controls other aspects of the widget's animation, such as duration and easing. 
  * Set the disable attribute to true in order to disable the animation effect.
  * For a live example, see the Carousel Animation page in the Explore sample.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#element").wijcarousel( { 
  *     animation { 
  *         queue: true,
  *         disable: false,
  *         duration: true,
  *         easing: "easeOutCubic"
  *     }
  * } );
  */
wijcarousel_options.prototype.animation = null;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Determines the custom start position of the image list in wijcarousel.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * $("#element").wijcarousel( { start: 2 } );
  */
wijcarousel_options.prototype.start = 0;
/** <p class='defaultValue'>Default value: 1</p>
  * <p>Determines how many images should be shown in the view area.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * $("#element").wijcarousel( { display: 2 } );
  */
wijcarousel_options.prototype.display = 1;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines if we should preview the last and next images.
  * loop == false , orintation == "horizontal",display == 1.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijcarousel( { preview: false } );
  */
wijcarousel_options.prototype.preview = false;
/** <p class='defaultValue'>Default value: 1</p>
  * <p>Determines how many images will be scrolled 
  * when you click the Next/Previous button.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * $("#element").wijcarousel( { step: 2 } );
  */
wijcarousel_options.prototype.step = 1;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether the custom control should be shown.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijcarousel( { showControls: true } );
  */
wijcarousel_options.prototype.showControls = false;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the innerHtml of the custom control.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#element").wijcarousel( { control: "&lt;div&gt;Blah&lt;/div&gt;" } );
  */
wijcarousel_options.prototype.control = "";
/** <p>A value that indicates the position settings for the custom control.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#element").wijcarousel( { 
  *     controlPosition: {
  *         my: 'left bottom', 
  *         at: 'right top  ', 
  *         offset: '0 0'
  *     } 
  * });
  */
wijcarousel_options.prototype.controlPosition = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether the caption of items should be shown.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijcarousel( { showCaption: true } );
  */
wijcarousel_options.prototype.showCaption = true;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether the controls should be shown after created
  * or when hovering on the dom element.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijcarousel( { showControlsOnHover: true } );
  */
wijcarousel_options.prototype.showControlsOnHover = false;
/** This is the itemClick event handler.
  * It is a function called when the image is clicked.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.carousel.IItemClickEventArgs} data Information about an event
  */
wijcarousel_options.prototype.itemClick = null;
/** This is the beforeScroll event handler.
  * It is a function called before scrolling to another image.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.carousel.IBeforeScrollEventArgs} data Information about an event
  */
wijcarousel_options.prototype.beforeScroll = null;
/** This is the afterScroll event handler.
  * It is a function called after scrolling to another image.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.carousel.IAfterScrollEventArgs} data Information about an event
  */
wijcarousel_options.prototype.afterScroll = null;
/** This is the loadCallback event handler.
  * It is a function called after creating the dom element.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data The node widget that relates to this event.
  */
wijcarousel_options.prototype.loadCallback = null;
wijmo.carousel.wijcarousel.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijcarousel_options());
$.widget("wijmo.wijcarousel", $.wijmo.widget, wijmo.carousel.wijcarousel.prototype);
/** @interface btn_class_option
  * @namespace wijmo.carousel
  */
wijmo.carousel.btn_class_option = function () {};
/** <p>The defalut CSS class of wijcarousel</p>
  * @field 
  * @type {string}
  */
wijmo.carousel.btn_class_option.prototype.defaultClass = null;
/** <p>The hover-state CSS class of wijcarousel</p>
  * @field 
  * @type {string}
  */
wijmo.carousel.btn_class_option.prototype.hoverClass = null;
/** <p>The disabled-state CSS class of wijcarousel</p>
  * @field 
  * @type {string}
  */
wijmo.carousel.btn_class_option.prototype.disableClass = null;
/** @interface thumbnails_option
  * @namespace wijmo.carousel
  */
wijmo.carousel.thumbnails_option = function () {};
/** <p>The event hanlder of "mouseover"</p>
  * @field 
  * @type {Function}
  */
wijmo.carousel.thumbnails_option.prototype.mouseover = null;
/** <p>The event hanlder of "mouseout"</p>
  * @field 
  * @type {Function}
  */
wijmo.carousel.thumbnails_option.prototype.mouseout = null;
/** <p>The event hanlder of "mousedown"</p>
  * @field 
  * @type {Function}
  */
wijmo.carousel.thumbnails_option.prototype.mousedown = null;
/** <p>The event hanlder of "mouseup"</p>
  * @field 
  * @type {Function}
  */
wijmo.carousel.thumbnails_option.prototype.mouseup = null;
/** <p>The event hanlder of "click"</p>
  * @field 
  * @type {Function}
  */
wijmo.carousel.thumbnails_option.prototype.click = null;
/** <p>The width of thumbnail image</p>
  * @field 
  * @type {number}
  */
wijmo.carousel.thumbnails_option.prototype.imageWidth = null;
/** <p>The height of thumbnail image</p>
  * @field 
  * @type {number}
  */
wijmo.carousel.thumbnails_option.prototype.imageHeight = null;
/** <p>The list of images</p>
  * @field 
  * @type {Array}
  */
wijmo.carousel.thumbnails_option.prototype.images = null;
/** @interface wijcarousel_animation
  * @namespace wijmo.carousel
  */
wijmo.carousel.wijcarousel_animation = function () {};
/** <p>This value determines whether to queue animation operations.</p>
  * @field 
  * @type {boolean}
  */
wijmo.carousel.wijcarousel_animation.prototype.queue = null;
/** <p>A value that determines whether to show animation. Set this option to true in order to disable easing.</p>
  * @field 
  * @type {boolean}
  */
wijmo.carousel.wijcarousel_animation.prototype.disable = null;
/** <p>The duration option defines the length of the scrolling animation effect in milliseconds.</p>
  * @field 
  * @type {number}
  */
wijmo.carousel.wijcarousel_animation.prototype.duration = null;
/** <p>Sets the type of animation easing effect that users experience when the wijcarousel is scrolled to another image. For example, the wijcarousel can bounce several times as it loads.
  * Valid Values:
  * easeInCubic - Cubic easing in.Begins at zero velocity and then accelerates.
  * easeOutCubic - Cubic easing in and out.Begins at full velocity and then decelerates to zero.
  * easeInOutCubic - Begins at zero velocity, accelerates until halfway, and then decelerates to zero velocity again.
  * easeInBack - Begins slowly and then accelerates.
  * easeOutBack - Begins quickly and then decelerates.
  * easeOutElastic - Begins at full velocity and then decelerates to zero.
  * easeOutBounce - Begins quickly and then decelerates.The number of bounces is related to the duration, longer durations produce more bounces.
  * Default: "linear".
  * Type: string.</p>
  * @field 
  * @type {string}
  */
wijmo.carousel.wijcarousel_animation.prototype.easing = null;
/** Contains information about wijcarousel.itemClick event
  * @interface IItemClickEventArgs
  * @namespace wijmo.carousel
  */
wijmo.carousel.IItemClickEventArgs = function () {};
/** <p>This is the index of the clicked image.</p>
  * @field 
  * @type {number}
  */
wijmo.carousel.IItemClickEventArgs.prototype.index = null;
/** <p>This is the dom element of this item.</p>
  * @field 
  * @type {DOMElement}
  */
wijmo.carousel.IItemClickEventArgs.prototype.el = null;
/** Contains information about wijcarousel.beforeScroll event
  * @interface IBeforeScrollEventArgs
  * @namespace wijmo.carousel
  */
wijmo.carousel.IBeforeScrollEventArgs = function () {};
/** <p>This is the index of the clicked image.</p>
  * @field 
  * @type {number}
  */
wijmo.carousel.IBeforeScrollEventArgs.prototype.index = null;
/** <p>The index of the image that will scrolled to.</p>
  * @field 
  * @type {DOMElement}
  */
wijmo.carousel.IBeforeScrollEventArgs.prototype.to = null;
/** Contains information about wijcarousel.afterScroll event
  * @interface IAfterScrollEventArgs
  * @namespace wijmo.carousel
  */
wijmo.carousel.IAfterScrollEventArgs = function () {};
/** <p>The index of the last image.</p>
  * @field 
  * @type {number}
  */
wijmo.carousel.IAfterScrollEventArgs.prototype.index = null;
/** <p>The index of the current image.</p>
  * @field 
  * @type {DOMElement}
  */
wijmo.carousel.IAfterScrollEventArgs.prototype.to = null;
})()
})()
