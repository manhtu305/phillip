var wijmo = wijmo || {};

(function () {
wijmo.data = wijmo.data || {};

(function () {
/** @interface IBreezeDataViewOptions
  * @namespace wijmo.data
  * @extends wijmo.data.IRemoteDataViewOptions
  */
wijmo.data.IBreezeDataViewOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
wijmo.data.IBreezeDataViewOptions.prototype.inlineCount = null;
/** @class BreezeDataView
  * @namespace wijmo.data
  * @extends wijmo.data.RemoteDataView
  */
wijmo.data.BreezeDataView = function () {};
wijmo.data.BreezeDataView.prototype = new wijmo.data.RemoteDataView();
/** @returns {wijmo.data.IPropertyDescriptor[]} */
wijmo.data.BreezeDataView.prototype.getProperties = function () {};
/**  */
wijmo.data.BreezeDataView.prototype.cancelRefresh = function () {};
/** @returns {bool} */
wijmo.data.BreezeDataView.prototype.canAddNew = function () {};
/** @param initialValues */
wijmo.data.BreezeDataView.prototype.addNew = function (initialValues) {};
/** @param entity */
wijmo.data.BreezeDataView.prototype.add = function (entity) {};
/**  */
wijmo.data.BreezeDataView.prototype.commitEdit = function () {};
/**  */
wijmo.data.BreezeDataView.prototype.cancelEdit = function () {};
typeof wijmo.data.IRemoteDataViewOptions != 'undefined' && $.extend(wijmo.data.IBreezeDataViewOptions.prototype, wijmo.data.IRemoteDataViewOptions.prototype);
})()
})()
