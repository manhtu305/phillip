var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
/** Represents an information about a row of the wijgrid.
  * @interface IRowInfo
  * @namespace wijmo.grid
  */
wijmo.grid.IRowInfo = function () {};
/** <p>Associated data.</p>
  * @field
  */
wijmo.grid.IRowInfo.prototype.data = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.rowType.html'>wijmo.grid.rowType</a></p>
  * <p>Type of the row</p>
  * @field 
  * @type {wijmo.grid.rowType}
  */
wijmo.grid.IRowInfo.prototype.type = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.renderState.html'>wijmo.grid.renderState</a></p>
  * <p>State of the row. Only the wijmo.grid.renderState.rendering value is useful in a row context.</p>
  * @field 
  * @type {wijmo.grid.renderState}
  */
wijmo.grid.IRowInfo.prototype.state = null;
/** <p>Data row index, the index of the data row inside the TBODY section of the wijgrid.</p>
  * @field 
  * @type {number}
  */
wijmo.grid.IRowInfo.prototype.dataRowIndex = null;
/** <p>Data item index.</p>
  * @field 
  * @type {number}
  */
wijmo.grid.IRowInfo.prototype.dataItemIndex = null;
/** <p>The value that is being grouped. Available for groupHeader and groupFooter rows only.</p>
  * @field
  */
wijmo.grid.IRowInfo.prototype.groupByValue = null;
/** <p>jQuery object that represents associteted HTML Table rows.</p>
  * @field 
  * @type {JQuery}
  */
wijmo.grid.IRowInfo.prototype.$rows = null;
/** Provides data for the cellStyleFormatter option of the wijgrid.
  * @interface ICellStyleFormaterArgs
  * @namespace wijmo.grid
  */
wijmo.grid.ICellStyleFormaterArgs = function () {};
/** <p>jQuery object that represents cell to format.</p>
  * @field 
  * @type {JQuery}
  */
wijmo.grid.ICellStyleFormaterArgs.prototype.$cell = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn</a></p>
  * <p>Options of the column to which the cell belongs.</p>
  * @field 
  * @type {wijmo.grid.IColumn}
  */
wijmo.grid.ICellStyleFormaterArgs.prototype.column = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.renderState.html'>wijmo.grid.renderState</a></p>
  * <p>State of a cell to format.</p>
  * @field 
  * @type {wijmo.grid.renderState}
  */
wijmo.grid.ICellStyleFormaterArgs.prototype.state = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IRowInfo.html'>wijmo.grid.IRowInfo</a></p>
  * <p>Information about associated row.</p>
  * @field 
  * @type {wijmo.grid.IRowInfo}
  */
wijmo.grid.ICellStyleFormaterArgs.prototype.row = null;
/** Data converter that is able to translate values from a string representation to column data type and back.
  * @interface IDataParser
  * @namespace wijmo.grid
  */
wijmo.grid.IDataParser = function () {};
/** <p>Converts the value into typed value.</p>
  * @field 
  * @type {function}
  */
wijmo.grid.IDataParser.prototype.parse = null;
/** <p>Converts the value into its string representation.</p>
  * @field 
  * @type {function}
  */
wijmo.grid.IDataParser.prototype.toStr = null;
/** Represents a filter operator.
  * @interface IFilterOperator
  * @namespace wijmo.grid
  */
wijmo.grid.IFilterOperator = function () {};
/** <p>An array of datatypes to which the filter can be applied. Possible values for elements of the array are "string", "number", "datetime", "currency" and "boolean".</p>
  * @field 
  * @type {string[]}
  */
wijmo.grid.IFilterOperator.prototype.applicableTo = null;
/** <p>Operator name.</p>
  * @field 
  * @type {string}
  */
wijmo.grid.IFilterOperator.prototype.name = null;
/** <p>The display name of the operator to show in the filter dropdown.</p>
  * @field 
  * @type {string}
  */
wijmo.grid.IFilterOperator.prototype.displayName = null;
/** <p>The number of filter operands. Can be either 1 or 2.</p>
  * @field 
  * @type {number}
  */
wijmo.grid.IFilterOperator.prototype.arity = null;
/** <p>Comparison operator; the number of accepted parameters depends on the arity. The first parameter is a data value, the second parameter is a filter value.</p>
  * @field 
  * @type {function}
  */
wijmo.grid.IFilterOperator.prototype.operator = null;
/** Represents a pager settings.
  * @interface IPagerSettings
  * @namespace wijmo.grid
  * @extends wijmo.pager.IPagerUISettings
  */
wijmo.grid.IPagerSettings = function () {};
/** <p>Determines the position of the pager. Possible values are: "bottom", "top", "topAndBottom".</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are:
  * "bottom": Pager appear below the control.
  * "top": Pager appear above the control.
  * "topAndBottom": Pager appear above and below the control.
  */
wijmo.grid.IPagerSettings.prototype.position = null;
/** Determines virtualization settings.
  * @interface IVirtualizationSettings
  * @namespace wijmo.grid
  */
wijmo.grid.IVirtualizationSettings = function () {};
/** <p>Determines virtual scrolling mode.
  * Possbile values are: "none", "rows", "columns", "both".</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are:
  * "none": Virtual scrolling is not used.
  * "rows": Rows are rendered on demand.
  * "columns": Columns are rendered on demand.
  * "both": Both "rows" and "columns" modes are used.
  * @example
  * $("#element").wijgrid({
  *   scrollingSettings: {
  *       virtualizationSettings: {
  *          mode: "rows"
  *       }                    
  *   }
  * })
  */
wijmo.grid.IVirtualizationSettings.prototype.mode = null;
/** <p>Determines the height of a rows when rows virtual scrolling is used.</p>
  * @field 
  * @type {number}
  * @remarks
  * Can be set only during creation
  * @example
  * $("#element").wijgrid({
  *   scrollingSettings: {
  *       virtualizationSettings: {
  *          rowHeight: 20
  *       }                    
  *   }
  * })
  */
wijmo.grid.IVirtualizationSettings.prototype.rowHeight = null;
/** <p>Determines the default width of a columns when columns virtual scrolling is used.</p>
  * @field 
  * @type {number}
  * @remarks
  * Can be set only during creation
  * @example
  * $("#element").wijgrid({
  *   scrollingSettings: {
  *       virtualizationSettings: {
  *          columnWidth: 100
  *       }                    
  *   }
  * })
  */
wijmo.grid.IVirtualizationSettings.prototype.columnWidth = null;
/** Determines the scrolling settings.
  * @interface IScrollingSettings
  * @namespace wijmo.grid
  */
wijmo.grid.IScrollingSettings = function () {};
/** <p>Determines which scrollbars are active and if they appear automatically based on content size.
  * Possbile values are: "none", "auto", "horizontal", "vertical", "both".</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are:
  * "none": Scrolling is not used; the staticRowIndex and staticColumnIndex values are ignored.
  * "auto": Scrollbars appear automatically depending upon content size.
  * "horizontal": The horizontal scrollbar is active.
  * "vertical": The vertical scrollbar is active.
  * "both": Both horizontal and vertical scrollbars are active.
  * @example
  * // The horizontal and vertical scrollbars are active when the scrollMode is set to both.
  * $("#element").wijgrid({ scrollingSettings: { mode: "both" } });
  */
wijmo.grid.IScrollingSettings.prototype.mode = null;
/** <p>Determines whether the user can change position of the static column or row by dragging the vertical or horizontal freezing handle with the mouse. Possible values are: "none", "columns", "rows", "both".</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are:
  * "none": The freezing handle cannot be dragged.
  * "columns": The user can drag the vertical freezing handle to change position of the static column.
  * "rows": The user can drag the horizontal freezing handle to change position of the static row.
  * "both": The user can drag both horizontal and vertical freezing handles.
  * @example
  * $("#element").wijgrid({ scrollingSettings: { freezingMode: "both" }});
  */
wijmo.grid.IScrollingSettings.prototype.freezingMode = null;
/** <p>Indicates the index of columns that will always be shown on the left when the grid view is scrolled horizontally.
  * Note that all columns before the static column will be automatically marked as static, too.
  * This can only take effect when the scrollingSettings.mode option is not set to "none".
  * It will be considered "-1" when grouping or row merging is enabled. A "-1" means there is no data column but the row header is static. A zero (0) means one data column and row header are static.</p>
  * @field 
  * @type {number}
  * @example
  * $("#element").wijgrid({ scrollingSettings: { staticColumnIndex: -1 } });
  */
wijmo.grid.IScrollingSettings.prototype.staticColumnIndex = null;
/** <p>Gets or sets the alignment of the static columns area. Possible values are "left", "right".</p>
  * @field 
  * @type {string}
  * @remarks
  * The "right" mode has limited functionality:
  * - The showRowHeader value is ignored.
  * - Changing staticColumnIndex at run-time by dragging the vertical bar is disabled.
  * @example
  * $("#element").wijgrid({ scrollingSettings: { staticColumnsAlignment: "left" } });
  */
wijmo.grid.IScrollingSettings.prototype.staticColumnsAlignment = null;
/** <p>Indicates the index of data rows that will always be shown on the top when the wijgrid is scrolled vertically.
  * Note, that all rows before the static row will be automatically marked as static, too.
  * This can only take effect when the scrollingSettings.mode option is not set to "none". This will be considered "-1" when grouping or row merging is enabled.
  * A "-1" means there is no data row but the header row is static.A zero (0) means one data row and the row header are static.</p>
  * @field 
  * @type {number}
  * @example
  * $("#element").wijgrid({ scrollingSettings: { staticRowIndex: -1 } });
  */
wijmo.grid.IScrollingSettings.prototype.staticRowIndex = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IVirtualizationSettings.html'>wijmo.grid.IVirtualizationSettings</a></p>
  * <p>Determines virtual scrolling settings. Use it when using large amounts of data to improve efficiency.</p>
  * @field 
  * @type {wijmo.grid.IVirtualizationSettings}
  * @remarks
  * Virtual scrolling has some limitations. See the virtualizationSettings.mode property for more details.
  * @example
  * $("#element").wijgrid({
  *   scrollingSettings: {
  *       virtualizationSettings: {
  *          mode: "vertical"
  *       }                    
  *   }
  * });
  */
wijmo.grid.IScrollingSettings.prototype.virtualizationSettings = null;
/** Used to customize the appearance and position of groups.
  * @interface IGroupInfo
  * @namespace wijmo.grid
  */
wijmo.grid.IGroupInfo = function () {};
/** <p>Determines whether the grid should insert group header and/or group footer rows for this column.  Possible values are: "none", "header", "footer", "headerAndFooter".</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are:
  * "none": disables grouping for the column.
  * "header": inserts header rows.
  * "footer": inserts footer rows.
  * "headerAndFooter": inserts header and footer rows.
  * @example
  * $("#element").wijgrid({ columns: [{ groupInfo: { position: "header" }}] });
  */
wijmo.grid.IGroupInfo.prototype.position = null;
/** <p>Determines whether the user will be able to collapse and expand the groups by clicking on the group headers, and also determines whether groups will be initially collapsed or expanded.
  * Possible values are: "none", "startCollapsed", "startExpanded".</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are:
  * "none": disables collapsing and expanding.
  * "startCollapsed": groups are initially collapsed.
  * "startExpanded": groups are initially expanded.
  * @example
  * $("#element").wijgrid({ columns: [{ groupInfo: { outlineMode: "startExpanded" }}] });
  */
wijmo.grid.IGroupInfo.prototype.outlineMode = null;
/** <p>A value indicating whether groupings containing a single row are grouped.</p>
  * @field 
  * @type {boolean}
  * @example
  * $("#element").wijgrid({ columns: [{ groupInfo: { groupSingleRow: true }}] });
  */
wijmo.grid.IGroupInfo.prototype.groupSingleRow = null;
/** <p>Determines the CSS used to show collapsed nodes on the grid.</p>
  * @field 
  * @type {string}
  * @example
  * $("#element").wijgrid({ columns: [{ groupInfo: { collapsedImageClass: "myClass" }}] });
  */
wijmo.grid.IGroupInfo.prototype.collapsedImageClass = null;
/** <p>Determines the CSS used to show expanded nodes on the grid.</p>
  * @field 
  * @type {string}
  * @example
  * $("#element").wijgrid({ columns: [{ groupInfo: { expandedImageClass: "myClass" }}] });
  */
wijmo.grid.IGroupInfo.prototype.expandedImageClass = null;
/** <p>Determines the text that is displayed in the group header rows.</p>
  * @field 
  * @type {string}
  * @remarks
  * The text may include up to three placeholders:
  * "{0}" is replaced with the value being grouped on.
  * "{1}" is replaced with the group's column header.
  * "{2}" is replaced with the aggregate
  * The text may be set to "custom". Doing so causes the grid groupText event to be raised when processing a grouped header.
  * @example
  * $("#element").wijgrid({ columns: [{ groupInfo: { headerText: "{1}: {0}" }}] });
  */
wijmo.grid.IGroupInfo.prototype.headerText = null;
/** <p>Determines the text that is displayed in the group footer rows.</p>
  * @field 
  * @type {string}
  * @remarks
  * The text may include up to three placeholders:
  * "{0}" is replaced with the value being grouped on.
  * "{1}" is replaced with the group's column header.
  * "{2}" is replaced with the aggregate
  * The text may be set to "custom". Doing so causes the grid groupText event to be raised when processing a grouped footer.
  * @example
  * $("#element").wijgrid({ columns: [{ groupInfo: { footerText: "{1}: {0}" }}] });
  */
wijmo.grid.IGroupInfo.prototype.footerText = null;
/** Provides data for the cellFormater option of the column.
  * @interface IC1BaseFieldCellFormatterArgs
  * @namespace wijmo.grid
  */
wijmo.grid.IC1BaseFieldCellFormatterArgs = function () {};
/** <p>jQuery object that represents cell container to format.</p>
  * @field 
  * @type {JQuery}
  */
wijmo.grid.IC1BaseFieldCellFormatterArgs.prototype.$container = null;
/** <p>Callback function which is invoked after applying default formatting.</p>
  * @field
  */
wijmo.grid.IC1BaseFieldCellFormatterArgs.prototype.afterDefaultCallback = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn</a></p>
  * <p>Options of the formatted column.</p>
  * @field 
  * @type {wijmo.grid.IColumn}
  */
wijmo.grid.IC1BaseFieldCellFormatterArgs.prototype.column = null;
/** <p>Formatted value of the cell.</p>
  * @field
  */
wijmo.grid.IC1BaseFieldCellFormatterArgs.prototype.formattedValue = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IRowInfo.html'>wijmo.grid.IRowInfo</a></p>
  * <p>Information about associated row.</p>
  * @field 
  * @type {wijmo.grid.IRowInfo}
  */
wijmo.grid.IC1BaseFieldCellFormatterArgs.prototype.row = null;
/** Represents options of the base class for all fields.
  * @interface IC1BaseFieldOptions
  * @namespace wijmo.grid
  * @extends wijmo.grid.IColumnInternalOptions
  */
wijmo.grid.IC1BaseFieldOptions = function () {};
/** <p>A value indicating whether the column can be moved.</p>
  * @field 
  * @type {boolean}
  * @example
  * $("#element").wijgrid({ columns: [ { allowMoving: true } ] });
  */
wijmo.grid.IC1BaseFieldOptions.prototype.allowMoving = null;
/** <p>A value indicating whether the column can be sized.</p>
  * @field 
  * @type {boolean}
  * @example
  * $("#element").wijgrid({ columns: [ { allowSizing: true } ] });
  */
wijmo.grid.IC1BaseFieldOptions.prototype.allowSizing = null;
/** <p>Function used for changing content, style and attributes of the column cells.</p>
  * @field 
  * @type
  * {{Function}
  * Important: cellFormatter should not alter content of header and filter row cells container.}
  * @example
  * // Add an image which URL is obtained from the "Url" data field to the column cells.
  * $("#demo").wijgrid({
  *     data: [
  *         { ID: 0, Url: "/images/0.jpg" },
  *         { ID: 1, Url: "/images/1.jpg" }
  *     ],
  *     columns: [
  *         {},
  *         {
  *             cellFormatter: function (args) {
  *                 if (args.row.type &amp; wijmo.grid.rowType.data) {
  *                     args.$container
  *                         .empty()
  *                         .append($("&lt;img /&gt;")
  *                             .attr("src", args.row.data.Url));
  *                     return true;
  *                 }
  *             }
  *         }
  *     ]
  * });
  */
wijmo.grid.IC1BaseFieldOptions.prototype.cellFormatter = null;
/** <p>Determines whether to use number type column width as the real width of the column.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * If this option is set to true, wijgrid will use the width option of the column widget.
  * If this option is undefined, wijgrid will refer to the ensureColumnsPxWidth option.
  * @example
  * $("#element").wijgrid({ columns: [{ ensurePxWidth: true }]});
  */
wijmo.grid.IC1BaseFieldOptions.prototype.ensurePxWidth = null;
/** <p>Gets or sets the footer text.
  * The text may include a placeholder: "{0}" is replaced with the aggregate.</p>
  * @field 
  * @type {string}
  * @remarks
  * If the value is undefined the footer text will be determined automatically depending on the type of the datasource:
  * DOM table - text in the footer cell.
  * @example
  * $("#element").wijgrid({ columns: [{ footerText: "footer" }]});
  */
wijmo.grid.IC1BaseFieldOptions.prototype.footerText = null;
/** <p>Gets or sets the header text.</p>
  * @field 
  * @type {string}
  * @remarks
  * If the value is undefined the header text will be determined automatically depending on the type of the datasource:
  * DOM table - text in the header cell.
  * Array of objects - dataKey (name of the field associated with column).
  * Two-dimensional array - dataKey (index of the field associated with column).
  * @example
  * $("#element").wijgrid({ columns: [ { headerText: "column0" } ] });
  */
wijmo.grid.IC1BaseFieldOptions.prototype.headerText = null;
/** <p>Gets or sets the text alignment of data cells. Possible values are "left", "right", "canter".</p>
  * @field 
  * @type {string}
  * @example
  * $("#element").wijgrid({ columns: [{ textAligment: "right" }]});
  */
wijmo.grid.IC1BaseFieldOptions.prototype.textAlignment = null;
/** <p>A value indicating whether column is visible.</p>
  * @field 
  * @type {boolean}
  * @example
  * $("#element").wijgrid({ columns: [{ visible: true }]});
  */
wijmo.grid.IC1BaseFieldOptions.prototype.visible = null;
/** <p>Determines the width of the column.</p>
  * @field 
  * @type {String|Number}
  * @remarks
  * The option could either be a number of string.
  * Use number to specify width in pixel, use string to specify width in percentage.
  * By default, wijgrid emulates the table element behavior when using number as width. This means wijgrid may not have the exact width specified. If exact width is needed, please set ensureColumnsPxWidth option of wijgrid to true.
  * @example
  * $("#element").wijgrid({ columns: [ { width: 150 } ] });
  * $("#element").wijgrid({ columns: [ { width: "10%" } ]});
  */
wijmo.grid.IC1BaseFieldOptions.prototype.width = null;
/** Represents options of a class for for all data-bound fields.
  * @interface IC1FieldOptions
  * @namespace wijmo.grid
  * @extends wijmo.grid.IC1BaseFieldOptions
  */
wijmo.grid.IC1FieldOptions = function () {};
/** <p>Causes the grid to calculate aggregate values on the column and place them in the column footer cell or group header and footer rows.
  * Possible values are: "none", "count", "sum", "average", "min", "max", "std", "stdPop", "var", "varPop" and "custom".</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are:
  * "none": no aggregate is calculated or displayed.
  * "count": count of non-empty values.
  * "sum": sum of numerical values.
  * "average": average of the numerical values.
  * "min": minimum value (numerical, string, or date).
  * "max": maximum value (numerical, string, or date).
  * "std": standard deviation (using formula for Sample, n-1).
  * "stdPop": standard deviation (using formula for Population, n).
  * "var": variance (using formula for Sample, n-1).
  * "varPop": variance (using formula for Population, n).
  * "custom": custom value (causing grid to throw groupAggregate event).
  * If the showFooter option is off or grid does not contain any groups, setting the "aggregate" option has no effect.
  * @example
  * $("#element").wijgrid({ columns: [{ aggregate: "count" }]});
  */
wijmo.grid.IC1FieldOptions.prototype.aggregate = null;
/** <p>A value indicating whether column can be sorted.</p>
  * @field 
  * @type {boolean}
  * @example
  * $("#element").wijgrid({ columns: [{ allowSort: true }] });
  */
wijmo.grid.IC1FieldOptions.prototype.allowSort = null;
/** <p>A value indicating the key of the data field associated with a column.
  * If an array of objects is used as a datasource for wijgrid, this should be string value,
  * otherwise this should be an integer determining an index of the field in the datasource.</p>
  * @field 
  * @type {String|Number}
  * @example
  * $("#element").wijgrid({ columns: [{ dataKey: "ProductID" }]});
  */
wijmo.grid.IC1FieldOptions.prototype.dataKey = null;
/** <p>Column data type. Defines the rules, according to which column value will be formatted, defines editors types and allowed filter operators.
  * Does not change the type of source data, besides the case when data source is HTMLTable.
  * Possible values are: "string", "number", "datetime", "currency" and "boolean".</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are:
  * "string": if using built-in parser any values are acceptable; "&amp;nbsp;" considered as an empty string, nullString as null.
  * "number": if using built-in parser only numeric values are acceptable, also "&amp;nbsp;", "" and nullString which are considered as null. Any other value throws an exception.
  * "datetime": if using built-in parser only date-time values are acceptable, also "&amp;nbsp;", "" and nullString which are considered as null. Any other value throws an exception.
  * "currency": if using built-in parser only numeric and currency values are acceptable, also "&amp;nbsp;", "" and nullString which are considered as null. Any other value throws an exception.
  * "boolean": if using built-in parser only "true" and "false" (case-insensitive) values are acceptable, also "&amp;nbsp;", "" and nullString which are considered as null. Any other value throws an exception.
  * @example
  * $("#element").wijgrid({ columns: [{ dataType: "string" }]});
  */
wijmo.grid.IC1FieldOptions.prototype.dataType = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IDataParser.html'>wijmo.grid.IDataParser</a></p>
  * <p>Data converter that is able to translate values from a string representation to column data type and back.</p>
  * @field 
  * @type {wijmo.grid.IDataParser}
  * @remarks
  * If undefined, than the built-in parser for supported datatypes will be used.
  * @example
  * var myBoolParser = {
  *     parseDOM: function (value, culture, format, nullString) {
  *         return this.parse(value.innerHTML, culture, format, nullString);
  *     },
  *     parse: function (value, culture, format, nullString) {
  *         if (typeof (value) === "boolean")  return value;
  *         if (!value || (value === "&amp;nbsp;") || (value === nullString)) {
  *             return null;
  *         }
  *         switch (value.toLowerCase()) {
  *             case "on": return true;
  *             case "off": return false;
  *         }
  *         return NaN;
  *     },
  *     toStr: function (value, culture, format, nullString) {
  *         if (value === null)  return nullString;
  *             return (value) ? "on" : "off";
  *         }
  *     }
  * }
  * $("#element").wijgrid({ columns: [ { dataType: "boolean", dataParser: myBoolParser } ] });
  */
wijmo.grid.IC1FieldOptions.prototype.dataParser = null;
/** <p>A pattern used for formatting and parsing column values.</p>
  * @field 
  * @type {string}
  * @remarks
  * The default value is undefined ("n" pattern will be used for "number" dataType, "d" for "datetime", "c" for "currency").
  * Please see the https://github.com/jquery/globalize for a full explanation and additional values.
  * @example
  * $("#element").wijgrid({
  *     columns: [
  *         { dataType: "currency" }, 
  *         { dataType: "number" }, 
  *         { dataType: "number", dataFormatString: "p0" }
  *     ]
  * });
  */
wijmo.grid.IC1FieldOptions.prototype.dataFormatString = null;
/** <p>A value indicating whether data values are HTML-encoded before they are displayed in a cell.</p>
  * @field 
  * @type {boolean}
  * @example
  * $("#element").wijgrid({
  *     data: [
  *         [0, "&lt;b&gt;value&lt;/b&gt;"],
  *         [1, "&amp;amp;"],
  *     ],
  *     columns: [
  *         { headerText: "ID" }, 
  *         { headerText: "Value", encodeHtml: true }
  *     ]
  * });
  */
wijmo.grid.IC1FieldOptions.prototype.encodeHtml = null;
/** <p>An operations set for filtering. Must be either one of the embedded operators or custom filter operator.
  * Operator names are case insensitive.</p>
  * @field 
  * @remarks
  * Embedded filter operators include:
  * "NoFilter": no filter.
  * "Contains": applicable to "string" data type.
  * "NotContain": applicable to "string" data type.
  * "BeginsWith": applicable to "string" data type.
  * "EndsWith": applicable to "string" data type.
  * "Equals": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
  * "NotEqual": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
  * "Greater": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
  * "Less": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
  * "GreaterOrEqual": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
  * "LessOrEqual": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
  * "IsEmpty": applicable to "string".
  * "NotIsEmpty": applicable to "string".
  * "IsNull": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
  * "NotIsNull": applicable to "string", "number", "datetime", "currency" and "boolean" data types.
  * Full option value is:
  * [filterOperartor1, ..., filterOperatorN]
  * where each filter item is an object of the following kind:
  * { name: &lt;operatorName&gt;, condition: "or"|"and" }
  * where:
  * name: filter operator name.
  * condition: logical condition to other operators, "or" is by default.
  * Example:
  * filterOperator: [ { name: "Equals" }, { name: "NotEqual", condition: "and" } ]
  * It is possible to use shorthand notation, the following statements are equivalent:
  * filterOperator: [ { name: "Equals" }, { name: "BeginsWith" } ]
  * filterOperator: [ "Equals", "BeginsWith" ]
  * In the case of a single operator option name may contain only filter operator name, the following statements are equivalent:
  * filterOperator: [ { name: "Equals" } ]
  * filterOperator: [ "Equals" ]
  * filterOperator: "Equals"
  * Note: wijgrid built-in filter editors do not support multiple filter operators.
  * @example
  * $("#element").wijgrid({ columns: [{ dataType: "number", filterOperator: "Equals", filterValue: 0 }]});
  */
wijmo.grid.IC1FieldOptions.prototype.filterOperator = null;
/** <p>A value set for filtering.</p>
  * @field 
  * @remarks
  * Full option value is:
  * [filterValue1, ..., filterValueN]
  * where each item is a filter value for the corresponding filter operator. Example:
  * filterValue: [0, "a", "b"]
  * Built-in filter operators support array of values as an argument. Example:
  * filterOperator: ["Equals", "BeginsWith"]
  * filterValue: [[0, 1, 2], "a"]
  * As a result of filtering all the records having 0, 1, 2, or starting with "a" will be fetched.
  * Shorthand notation allows omitting square brackets, the following statements are equivalent:
  * filterValue: ["a"]
  * filterValue: [["a"]]
  * filterValue: "a"
  * Note: wijgrid built-in filter editors do not support multiple filter values.
  * @example
  * $("#element").wijgrid({ columns: [{ dataType: "number", filterOperator: "Equals", filterValue: 0 }]});
  */
wijmo.grid.IC1FieldOptions.prototype.filterValue = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IGroupInfo.html'>wijmo.grid.IGroupInfo</a></p>
  * <p>Used to customize the appearance and position of groups.</p>
  * @field 
  * @type {wijmo.grid.IGroupInfo}
  * @example
  * $("#element").wijgrid({ columns: [{ groupInfo: { position: "header" }}]});
  */
wijmo.grid.IC1FieldOptions.prototype.groupInfo = null;
/** <p>Controls the state of the input method editor for text fields.
  * Possible values are: "auto", "active", "inactive", "disabled".
  * Please refer to https://developer.mozilla.org/en-US/docs/Web/CSS/ime-mode for more info.</p>
  * @field 
  * @type {string}
  * @example
  * $("#element").wijgrid({ columns: [{ imeMode: "auto" }]});
  */
wijmo.grid.IC1FieldOptions.prototype.imeMode = null;
/** <p>Determines the type of html editor for filter and cells.
  * Possible values are: "number", "date", "datetime", "datetime-local", "month", "time", "text".</p>
  * @field 
  * @type {string}
  * @remarks
  * If the value is set then input type element is used with "type" attribute set to the value. If the value is not set then:
  * - in desktop environment a "text" input element is used as the editor.
  * - in mobile environment a "number" input element is used for columns having "number" and "currency" dataType; for columns where dataType = "datetime" a "datetime" input element is used, otherwise a "text" input element is shown.
  * @example
  * $("#element").wijgrid({ columns: [{ inputType: "text" }]});
  */
wijmo.grid.IC1FieldOptions.prototype.inputType = null;
/** <p>A value indicating whether the cells in the column can be edited.</p>
  * @field 
  * @type {boolean}
  * @example
  * $("#element").wijgrid({ columns: [ { readOnly: false } ] });
  */
wijmo.grid.IC1FieldOptions.prototype.readOnly = null;
/** <p>Determines whether rows are merged. Possible values are: "none", "free" and "restricted".</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are:
  * "none": no row merging.
  * "free": allows row with identical text to merge.
  * "restricted": keeps rows with identical text from merging if rows in the previous column are merged.
  * @example
  * $("#element").wijgrid({ columns: [{ rowMerge: "none" }]});
  */
wijmo.grid.IC1FieldOptions.prototype.rowMerge = null;
/** <p>A value indicating whether filter editor will be shown in the filter row.</p>
  * @field 
  * @type {boolean}
  * @example
  * $("#element").wijgrid({ columns: [{ showFilter: true }]});
  */
wijmo.grid.IC1FieldOptions.prototype.showFilter = null;
/** <p>Determines the sort direction. Possible values are: "none", "ascending" and "descending".</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are:
  * "none": no sorting.
  * "ascending": sort from smallest to largest.
  * "descending": sort from largest to smallest.
  * @example
  * $("#element").wijgrid({ columns: [{ sortDirection: "none" }]});
  */
wijmo.grid.IC1FieldOptions.prototype.sortDirection = null;
/** <p>A value indicating whether null value is allowed during editing.</p>
  * @field 
  * @type {boolean}
  * @example
  * $("#element").wijgrid({ columns: [{ valueRequired: false }]});
  */
wijmo.grid.IC1FieldOptions.prototype.valueRequired = null;
/** Represents options of a class which is used to create multilevel column headers.
  * @interface IC1BandFieldOptions
  * @namespace wijmo.grid
  * @extends wijmo.grid.IC1BaseFieldOptions
  */
wijmo.grid.IC1BandFieldOptions = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn[]</a></p>
  * <p>Gets a array of objects representing the band columns.</p>
  * @field 
  * @type {wijmo.grid.IColumn[]}
  * @example
  * $("#element").wijgrid({
  *   columns: [{
  *      headerText: "Band",
  *      columns: [
  *         { headerText: "ID" },
  *         { headerText: "Name" }
  *      ]
  *   }]
  * });
  */
wijmo.grid.IC1BandFieldOptions.prototype.columns = null;
/** Represents options of the base class for all button fields.
  * @interface IC1ButtonBaseFieldOptions
  * @namespace wijmo.grid
  * @extends wijmo.grid.IC1BaseFieldOptions
  */
wijmo.grid.IC1ButtonBaseFieldOptions = function () {};
/** <p>Gets or sets the type of the button in the column. Possible values are "link", "button", "imageButton", "image".</p>
  * @field 
  * @type {string}
  * @example
  * $("#element").wijgrid({ columns: [{ buttonType: "button" }]});
  */
wijmo.grid.IC1ButtonBaseFieldOptions.prototype.buttonType = null;
/** Represents options of a field that displays a command button.
  * @interface IC1ButtonFieldOptions
  * @namespace wijmo.grid
  * @extends wijmo.grid.IC1ButtonBaseFieldOptions
  */
wijmo.grid.IC1ButtonFieldOptions = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.ICommandButton.html'>wijmo.grid.ICommandButton</a></p>
  * <p>Represents options of a command button.</p>
  * @field 
  * @type {wijmo.grid.ICommandButton}
  * @example
  * $("#element").wijgrid({
  *    columns: [{
  *       buttonType: "link",
  *       command: {
  *          text: "myCommand",
  *          click: function (e, args) { 
  *             alert("clicked!");
  *          }
  *       }
  *    }]
  * });
  */
wijmo.grid.IC1ButtonFieldOptions.prototype.command = null;
/** Represents options of a field that displays command buttons to perform editing and deleting operations.
  * @interface IC1CommandButtonFieldOptions
  * @namespace wijmo.grid
  * @extends wijmo.grid.IC1ButtonBaseFieldOptions
  */
wijmo.grid.IC1CommandButtonFieldOptions = function () {};
/** <p>Gets or sets a value indicating whether a Delete button is displayed in a command column.</p>
  * @field 
  * @type {boolean}
  * @example
  * $("#element").wijgrid({
  *    columns: [{
  *       showDeleteButton: true
  *    }]
  * });
  */
wijmo.grid.IC1CommandButtonFieldOptions.prototype.showDeleteButton = null;
/** <p>Gets or sets a value indicating whether an Edit button is displayed in a command column.</p>
  * @field 
  * @type {boolean}
  * @example
  * $("#element").wijgrid({
  *    columns: [{
  *       showEditButton: true
  *    }]
  * });
  */
wijmo.grid.IC1CommandButtonFieldOptions.prototype.showEditButton = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.ICommandButton.html'>wijmo.grid.ICommandButton</a></p>
  * <p>Represents options of a Cancel command button.</p>
  * @field 
  * @type {wijmo.grid.ICommandButton}
  * @example
  * $("#element").wijgrid({
  *    columns: [{
  *       cancelCommand: {
  *          text: "Cancel!"
  *       }
  *    }]
  * });
  */
wijmo.grid.IC1CommandButtonFieldOptions.prototype.cancelCommand = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.ICommandButton.html'>wijmo.grid.ICommandButton</a></p>
  * <p>Represents options of a Delete command button.</p>
  * @field 
  * @type {wijmo.grid.ICommandButton}
  * @example
  * $("#element").wijgrid({
  *    columns: [{
  *       deleteCommand: {
  *          text: "Delete!"
  *       }
  *    }]
  * });
  */
wijmo.grid.IC1CommandButtonFieldOptions.prototype.deleteCommand = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.ICommandButton.html'>wijmo.grid.ICommandButton</a></p>
  * <p>Represents options of an Edit command button.</p>
  * @field 
  * @type {wijmo.grid.ICommandButton}
  * @example
  * $("#element").wijgrid({
  *    columns: [{
  *       editCommand: {
  *          text: "Edit!"
  *       }
  *    }]
  * });
  */
wijmo.grid.IC1CommandButtonFieldOptions.prototype.editCommand = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.ICommandButton.html'>wijmo.grid.ICommandButton</a></p>
  * <p>Represents options of an Update commnd button.</p>
  * @field 
  * @type {wijmo.grid.ICommandButton}
  * @example
  * $("#element").wijgrid({
  *    columns: [{
  *       updateCommand: {
  *          text: "Update!"
  *       }
  *    }]
  * });
  */
wijmo.grid.IC1CommandButtonFieldOptions.prototype.updateCommand = null;
/** Represents options of the command button. Allows button look customization and track mouse click on it.
  * @interface ICommandButton
  * @namespace wijmo.grid
  */
wijmo.grid.ICommandButton = function () {};
/** <p>Determines the text that is displayed in the button.</p>
  * @field 
  * @type {string}
  * @example
  * $("#element").wijgrid({
  *    columns: [{
  *       command: {
  *          text: "MyButton"
  *       }
  *    }]
  * });
  */
wijmo.grid.ICommandButton.prototype.text = null;
/** <p>Determines the data field to bind a button text to it.</p>
  * @field 
  * @type {string}
  * @example
  * $("#element").wijgrid({
  *    columns: [{
  *       command: {
  *          text: "{0}",
  *          textDataKey: "ID"
  *       }
  *    }]
  * });
  */
wijmo.grid.ICommandButton.prototype.textDataKey = null;
/** <p>The click event handler is a function that is called when a button is clicked. This event is cancellable.</p>
  * @field 
  * @type {function}
  * @example
  * $("#element").wijgrid({
  *    columns: [{
  *       deleteCommand: {
  *          click: function(e, args) {
  *             return confirm("Are you sure?");
  *          }
  *       }
  *    }]
  * });
  */
wijmo.grid.ICommandButton.prototype.click = null;
/** <p>Determines the CSS class used to display an image button.</p>
  * @field 
  * @type {string}
  * @remarks
  * Can be used only if the buttonType option of a button field is set to "imageButton" or "image".
  * @example
  * $("#element").wijgrid({
  *    columns: [{
  *       buttonType: "imageButton",
  *       command: {
  *          iconClass: "ui-icon-info",
  *          click: function(e, args) {
  *             alert(args.row.data.ID);
  *          }
  *       }
  *    }]
  * });
  */
wijmo.grid.ICommandButton.prototype.iconClass = null;
/** Combines all of the available options
  * @interface IColumn
  * @namespace wijmo.grid
  * @extends wijmo.grid.IColumnInternalOptions
  * @extends wijmo.grid.IC1BaseFieldOptions
  * @extends wijmo.grid.IC1FieldOptions
  * @extends wijmo.grid.IC1BandFieldOptions
  * @extends wijmo.grid.IC1ButtonBaseFieldOptions
  * @extends wijmo.grid.IC1ButtonFieldOptions
  * @extends wijmo.grid.IC1CommandButtonFieldOptions
  */
wijmo.grid.IColumn = function () {};
/** Provides data for the click event of a command buttons.
  * @interface IButtonCommandEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.IButtonCommandEventArgs = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn</a></p>
  * <p>The associated column.</p>
  * @field 
  * @type {wijmo.grid.IColumn}
  */
wijmo.grid.IButtonCommandEventArgs.prototype.column = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IRowInfo.html'>wijmo.grid.IRowInfo</a></p>
  * <p>Information about the associated row.</p>
  * @field 
  * @type {wijmo.grid.IRowInfo}
  */
wijmo.grid.IButtonCommandEventArgs.prototype.row = null;
/** Provides data for the editing events of the wijgrid.
  * @interface IBaseCellEditEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.IBaseCellEditEventArgs = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.cellInfo.html'>wijmo.grid.cellInfo</a></p>
  * <p>Gets the edited cell's information.</p>
  * @field 
  * @type {wijmo.grid.cellInfo}
  */
wijmo.grid.IBaseCellEditEventArgs.prototype.cell = null;
/** Provides data for the beforeCellEdit event of the wijgrid.
  * @interface IBeforeCellEditEventArgs
  * @namespace wijmo.grid
  * @extends wijmo.grid.IBaseCellEditEventArgs
  */
wijmo.grid.IBeforeCellEditEventArgs = function () {};
/** <p>The event initiated cell editing.</p>
  * @field 
  * @type {JQueryEventObject}
  */
wijmo.grid.IBeforeCellEditEventArgs.prototype.event = null;
/** <p>Gets or sets a value that determines how cell editing is initiated: manually or automatically.
  * If the cell contain custom controls or if you wish to provide custom editing on the front end, then the property must be set to "true".
  * The default value is "false", which means that the wijgrid will handle editing automatically.</p>
  * @field 
  * @type {boolean}
  */
wijmo.grid.IBeforeCellEditEventArgs.prototype.handled = null;
/** Provides data for the afterCellEdit event of the wijgrid.
  * @interface IAfterCellEditEventArgs
  * @namespace wijmo.grid
  * @extends wijmo.grid.IBaseCellEditEventArgs
  */
wijmo.grid.IAfterCellEditEventArgs = function () {};
/** <p>The event that initiates the cell update.</p>
  * @field 
  * @type {JQueryEventObject}
  */
wijmo.grid.IAfterCellEditEventArgs.prototype.event = null;
/** <p>Gets or sets a value that determines how cell editing is finalized: automatically or manually.
  * The default value is "false", which means that the wijgrid will try to finalize cell editing automatically by applying the column's cell formatter to the cell.
  * If this behavior is not suitable for you, then this property must be set to "true".</p>
  * @field 
  * @type {boolean}
  */
wijmo.grid.IAfterCellEditEventArgs.prototype.handled = null;
/** Provides data for the beforeCellUpdate event of the wijgrid.
  * @interface IBeforeCellUpdateEventArgs
  * @namespace wijmo.grid
  * @extends wijmo.grid.IBaseCellEditEventArgs
  */
wijmo.grid.IBeforeCellUpdateEventArgs = function () {};
/** <p>Gets the new cell value. If the property value is not changed, then the widget will try to extract the new cell value automatically.
  * If you provide custom editing on the front end, then they new cell value must be returned in this property.</p>
  * @field
  */
wijmo.grid.IBeforeCellUpdateEventArgs.prototype.value = null;
/** Provides data for the afterCellUpdate event of the wijgrid.
  * @interface IAfterCellUpdateEventArgs
  * @namespace wijmo.grid
  * @extends wijmo.grid.IBaseCellEditEventArgs
  */
wijmo.grid.IAfterCellUpdateEventArgs = function () {};
/** Provides data for the invalidCellValue event of the wijgrid.
  * @interface IInvalidCellValueEventArgs
  * @namespace wijmo.grid
  * @extends wijmo.grid.IBaseCellEditEventArgs
  */
wijmo.grid.IInvalidCellValueEventArgs = function () {};
/** <p>Gets the current value</p>
  * @field
  */
wijmo.grid.IInvalidCellValueEventArgs.prototype.value = null;
/** Provides data for the cellClicked event of the wijgrid.
  * @interface ICellClickedEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.ICellClickedEventArgs = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.cellInfo.html'>wijmo.grid.cellInfo</a></p>
  * <p>Gets the clicked cell's information.</p>
  * @field 
  * @type {wijmo.grid.cellInfo}
  */
wijmo.grid.ICellClickedEventArgs.prototype.cell = null;
/** Provides data for the columnDragging event of the wijgrid.
  * @interface IColumnDraggingEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.IColumnDraggingEventArgs = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn</a></p>
  * <p>The column being dragged.</p>
  * @field 
  * @type {wijmo.grid.IColumn}
  */
wijmo.grid.IColumnDraggingEventArgs.prototype.drag = null;
/** <p>The location of the column that is being dragged. The possible values are "groupArea" or "columns."</p>
  * @field 
  * @type {string}
  */
wijmo.grid.IColumnDraggingEventArgs.prototype.dragSource = null;
/** Provides data for the columnDragged event of the wijgrid.
  * @interface IColumnDraggedEventArgs
  * @namespace wijmo.grid
  * @extends wijmo.grid.IColumnDraggingEventArgs
  */
wijmo.grid.IColumnDraggedEventArgs = function () {};
/** Provides data for the columnDropping event of the wijgrid.
  * @interface IColumnDroppingEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.IColumnDroppingEventArgs = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn</a></p>
  * <p>The column being dragged.</p>
  * @field 
  * @type {wijmo.grid.IColumn}
  */
wijmo.grid.IColumnDroppingEventArgs.prototype.drag = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn</a></p>
  * <p>The column on which the drag source is dropped: the drop target.</p>
  * @field 
  * @type {wijmo.grid.IColumn}
  */
wijmo.grid.IColumnDroppingEventArgs.prototype.drop = null;
/** <p>The position at which the dragged column is dropped relative to the drop target. The possible values are "left", "right", or "center".</p>
  * @field 
  * @type {string}
  */
wijmo.grid.IColumnDroppingEventArgs.prototype.at = null;
/** Provides data for the columnDropped event of the wijgrid.
  * @interface IColumnDroppedEventArgs
  * @namespace wijmo.grid
  * @extends wijmo.grid.IColumnDroppingEventArgs
  */
wijmo.grid.IColumnDroppedEventArgs = function () {};
/** Provides data for the columnGrouping event of the wijgrid.
  * @interface IColumnGroupingEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.IColumnGroupingEventArgs = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn</a></p>
  * <p>The column being dragged: the drag source.</p>
  * @field 
  * @type {wijmo.grid.IColumn}
  */
wijmo.grid.IColumnGroupingEventArgs.prototype.drag = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn</a></p>
  * <p>The drop target, the place where the dragged column is dropped. This parameter is null if you are dropping a column into an empty group area.</p>
  * @field 
  * @type {wijmo.grid.IColumn}
  */
wijmo.grid.IColumnGroupingEventArgs.prototype.drop = null;
/** <p>The location of the dragged column. The possible values are "groupArea" and "columns".</p>
  * @field 
  * @type {string}
  */
wijmo.grid.IColumnGroupingEventArgs.prototype.dragSource = null;
/** <p>The location of the dropped column. The possible values are "groupArea" and "columns".</p>
  * @field 
  * @type {string}
  */
wijmo.grid.IColumnGroupingEventArgs.prototype.dropSource = null;
/** <p>The position at which the dragged column is dropped relative to the drop target. The possible values are "left", "right", or "center". The value is "left" if you're dropping the column into an empty group area.</p>
  * @field 
  * @type {string}
  */
wijmo.grid.IColumnGroupingEventArgs.prototype.at = null;
/** Provides data for the columnGrouped event of the wijgrid.
  * @interface IColumnGroupedEventArgs
  * @namespace wijmo.grid
  * @extends wijmo.grid.IColumnGroupingEventArgs
  */
wijmo.grid.IColumnGroupedEventArgs = function () {};
/** Provides data for the columnResizing event of the wijgrid.
  * @interface IColumnResizingEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.IColumnResizingEventArgs = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn</a></p>
  * <p>The column that is being resized.</p>
  * @field 
  * @type {wijmo.grid.IColumn}
  */
wijmo.grid.IColumnResizingEventArgs.prototype.column = null;
/** <p>The width of the column before it is resized.</p>
  * @field
  */
wijmo.grid.IColumnResizingEventArgs.prototype.oldWidth = null;
/** <p>args.newWidth: The new width being set for the column.</p>
  * @field
  */
wijmo.grid.IColumnResizingEventArgs.prototype.newWidth = null;
/** Provides data for the columnResized event of the wijgrid.
  * @interface IColumnResizedEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.IColumnResizedEventArgs = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn</a></p>
  * <p>The column that is being resized.</p>
  * @field 
  * @type {wijmo.grid.IColumn}
  */
wijmo.grid.IColumnResizedEventArgs.prototype.column = null;
/** Provides data for the columnUngrouping event of the wijgrid.
  * @interface IColumnUngroupingEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.IColumnUngroupingEventArgs = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn</a></p>
  * <p>The column being ungrouped</p>
  * @field 
  * @type {wijmo.grid.IColumn}
  */
wijmo.grid.IColumnUngroupingEventArgs.prototype.column = null;
/** Provides data for the columnUngrouped event of the wijgrid.
  * @interface IColumnUngroupedEventArgs
  * @namespace wijmo.grid
  * @extends wijmo.grid.IColumnUngroupingEventArgs
  */
wijmo.grid.IColumnUngroupedEventArgs = function () {};
/** Provides data for the currentCellChanging event of the wijgrid.
  * @interface ICurrentCellChangingEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.ICurrentCellChangingEventArgs = function () {};
/** <p>The new cell index.</p>
  * @field 
  * @type {number}
  */
wijmo.grid.ICurrentCellChangingEventArgs.prototype.cellIndex = null;
/** <p>The new row index.</p>
  * @field 
  * @type {number}
  */
wijmo.grid.ICurrentCellChangingEventArgs.prototype.rowIndex = null;
/** <p>The old cell index.</p>
  * @field 
  * @type {number}
  */
wijmo.grid.ICurrentCellChangingEventArgs.prototype.oldCellIndex = null;
/** <p>The old row index.</p>
  * @field 
  * @type {number}
  */
wijmo.grid.ICurrentCellChangingEventArgs.prototype.oldRowIndex = null;
/** Provides data for the detailCreating event of the wijgrid.
  * @interface IDetailCreatingEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.IDetailCreatingEventArgs = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IWijgridOptions.html'>wijmo.grid.IWijgridOptions</a></p>
  * <p>Defines create options of the detail grid.</p>
  * @field 
  * @type {wijmo.grid.IWijgridOptions}
  */
wijmo.grid.IDetailCreatingEventArgs.prototype.options = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IDataKeyArray.html'>wijmo.grid.IDataKeyArray</a></p>
  * <p>Returns related master key.</p>
  * @field 
  * @type {wijmo.grid.IDataKeyArray}
  */
wijmo.grid.IDetailCreatingEventArgs.prototype.masterKey = null;
/** Provides data for the filterOperatorsListShowing event of the wijgrid.
  * @interface IFilterOperatorsListShowingEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.IFilterOperatorsListShowingEventArgs = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn</a></p>
  * <p>The associated column.</p>
  * @field 
  * @type {wijmo.grid.IColumn}
  */
wijmo.grid.IFilterOperatorsListShowingEventArgs.prototype.column = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IFilterOperator.html'>wijmo.grid.IFilterOperator[]</a></p>
  * <p>An array of filter operators.</p>
  * @field 
  * @type {wijmo.grid.IFilterOperator[]}
  */
wijmo.grid.IFilterOperatorsListShowingEventArgs.prototype.operators = null;
/** Provides data for the filtering event of the wijgrid.
  * @interface IFilteringEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.IFilteringEventArgs = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn</a></p>
  * <p>The column that is being filtered.</p>
  * @field 
  * @type {wijmo.grid.IColumn}
  */
wijmo.grid.IFilteringEventArgs.prototype.column = null;
/** <p>The new filter operator name.</p>
  * @field 
  * @type {string}
  */
wijmo.grid.IFilteringEventArgs.prototype.operator = null;
/** <p>The new filter value.</p>
  * @field
  */
wijmo.grid.IFilteringEventArgs.prototype.value = null;
/** Provides data for the filtered event of the wijgrid.
  * @interface IFilteredEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.IFilteredEventArgs = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn</a></p>
  * <p>The column that is being filtered.</p>
  * @field 
  * @type {wijmo.grid.IColumn}
  */
wijmo.grid.IFilteredEventArgs.prototype.column = null;
/** Provides data for the groupAggregate event of the wijgrid.
  * @interface IGroupAggregateEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.IGroupAggregateEventArgs = function () {};
/** <p>The data object.</p>
  * @field
  */
wijmo.grid.IGroupAggregateEventArgs.prototype.data = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn</a></p>
  * <p>The column that is being grouped.</p>
  * @field 
  * @type {wijmo.grid.IColumn}
  */
wijmo.grid.IGroupAggregateEventArgs.prototype.column = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn</a></p>
  * <p>The column initiated grouping.</p>
  * @field 
  * @type {wijmo.grid.IColumn}
  */
wijmo.grid.IGroupAggregateEventArgs.prototype.groupByColumn = null;
/** <p>The text that is being grouped.</p>
  * @field 
  * @type {string}
  */
wijmo.grid.IGroupAggregateEventArgs.prototype.groupText = null;
/** <p>The text that will be displayed in the group header or group footer.</p>
  * @field 
  * @type {string}
  */
wijmo.grid.IGroupAggregateEventArgs.prototype.text = null;
/** <p>The first index for the data being grouped.</p>
  * @field 
  * @type {number}
  */
wijmo.grid.IGroupAggregateEventArgs.prototype.groupingStart = null;
/** <p>The last index for the data being grouped.</p>
  * @field 
  * @type {number}
  */
wijmo.grid.IGroupAggregateEventArgs.prototype.groupingEnd = null;
/** <p>Specifies whether or not the row you are grouping is a group header.</p>
  * @field 
  * @type {boolean}
  */
wijmo.grid.IGroupAggregateEventArgs.prototype.isGroupHeader = null;
/** Provides data for the groupText event of the wijgrid.
  * @interface IGroupTextEventArgs
  * @namespace wijmo.grid
  * @extends wijmo.grid.IGroupAggregateEventArgs
  */
wijmo.grid.IGroupTextEventArgs = function () {};
/** <p>The aggregate value.</p>
  * @field
  */
wijmo.grid.IGroupTextEventArgs.prototype.aggregate = null;
/** Provides data for the pageIndexChanging event of the wijgrid.
  * @interface IPageIndexChangingEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.IPageIndexChangingEventArgs = function () {};
/** <p>The new page index.</p>
  * @field 
  * @type {number}
  */
wijmo.grid.IPageIndexChangingEventArgs.prototype.newPageIndex = null;
/** Provides data for the pageIndexChanged event of the wijgrid.
  * @interface IPageIndexChangedEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.IPageIndexChangedEventArgs = function () {};
/** Provides data for the selectionChanged event of the wijgrid.
  * @interface ISelectionChangedEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.ISelectionChangedEventArgs = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.cellInfoOrderedCollection.html'>wijmo.grid.cellInfoOrderedCollection</a></p>
  * <p>The cells that have been added to the selection.</p>
  * @field 
  * @type {wijmo.grid.cellInfoOrderedCollection}
  */
wijmo.grid.ISelectionChangedEventArgs.prototype.addedCells = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.cellInfoOrderedCollection.html'>wijmo.grid.cellInfoOrderedCollection</a></p>
  * <p>The cells that have been removed from the selection.</p>
  * @field 
  * @type {wijmo.grid.cellInfoOrderedCollection}
  */
wijmo.grid.ISelectionChangedEventArgs.prototype.removedCells = null;
/** Provides data for the sorting event of the wijgrid.
  * @interface ISortingEventArgs
  * @namespace wijmo.grid
  */
wijmo.grid.ISortingEventArgs = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn</a></p>
  * <p>The column that is being sorted.</p>
  * @field 
  * @type {wijmo.grid.IColumn}
  */
wijmo.grid.ISortingEventArgs.prototype.column = null;
/** <p>The new sort direction.</p>
  * @field 
  * @type {string}
  */
wijmo.grid.ISortingEventArgs.prototype.sortDirection = null;
/** <p>Combines args.column.dataKey and args.sortDirection in a shorthand notation to represent a sorting command: "&amp;lt;dataKey&amp;gt; &amp;lt;asc|desc&amp;gt;".</p>
  * @field 
  * @type {string}
  */
wijmo.grid.ISortingEventArgs.prototype.sortCommand = null;
/** Provides data for the sorted event of the wijgrid.
  * @interface ISortedEventArgs
  * @namespace wijmo.grid
  * @extends wijmo.grid.ISortingEventArgs
  */
wijmo.grid.ISortedEventArgs = function () {};
/** Represents options of the wijgrid.
  * @interface IWijgridOptions
  * @namespace wijmo.grid
  */
wijmo.grid.IWijgridOptions = function () {};
/** <p>A value indicating whether columns can be moved.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * This option must be set to true in order to drag column headers to the group area.
  * @example
  * // Columns cannot be dragged and moved if this option is set to false
  * $("#element").wijgrid({ allowColMoving: false });
  */
wijmo.grid.IWijgridOptions.prototype.allowColMoving = null;
/** <p>Determines whether the column width can be increased and decreased by dragging the sizing handle, or the edge of the column header, with the mouse.</p>
  * @field 
  * @type {boolean}
  * @example
  * // The sizing handle cannot be dragged and column width cannot be changed if this option is set to false
  * $("#element").wijgrid({ allowColSizing: false });
  */
wijmo.grid.IWijgridOptions.prototype.allowColSizing = null;
/** <p>Determines whether the user can make changes to cell contents in the grid.
  * This option is obsolete. Use the editingMode option instead.</p>
  * @field 
  * @type {boolean}
  * @example
  * // Users cannot change cell contents in the grid if this option is set to false
  * $("#element").wijgrid({ allowEditing: false });
  */
wijmo.grid.IWijgridOptions.prototype.allowEditing = null;
/** <p>Determines whether the user can move the current cell using the arrow keys.</p>
  * @field 
  * @type {boolean}
  * @example
  * // Users cannot move the selection using arrow keys if this option is set to false
  * $("#element").wijgrid({ allowKeyboardNavigation: false });
  */
wijmo.grid.IWijgridOptions.prototype.allowKeyboardNavigation = null;
/** <p>Determines the action to be performed when the user presses the TAB key.</p>
  * @field 
  * @type {string}
  * @remarks
  * This option is invalid when the allowKeyboardNavigation is set to false.
  * Possible values are:
  * "moveAcross": The focus will be kept inside the grid and current selected cell will move cyclically between grid cells when user press TAB or SHIFT+TAB key.
  * "moveAcrossOut": The focus will be able to be moved from the grid to the next focusable element in the tab order when user press TAB key and the current selected cell is the last cell (or press SHIFT+TAB and the current selected cell is the first cell).
  * @example
  * $("#element").wijgrid({ keyActionTab: "moveAcross" });
  */
wijmo.grid.IWijgridOptions.prototype.keyActionTab = null;
/** <p>Determines whether the grid should display paging buttons. The number of rows on a page is determined by the pageSize option.</p>
  * @field 
  * @type {boolean}
  * @example
  * // Grid displays paging buttons when allowPaging is true. The pageSize here sets 5 rows to a page.
  * $("#element").wijgrid({ allowPaging: false, pageSize: 5 });
  */
wijmo.grid.IWijgridOptions.prototype.allowPaging = null;
/** <p>Determines whether the widget can be sorted by clicking the column header.</p>
  * @field 
  * @type {boolean}
  * @example
  * // Sort a column by clicking its header when allowSorting is set to true
  * $("#element").wijgrid({ allowSorting: false });
  */
wijmo.grid.IWijgridOptions.prototype.allowSorting = null;
/** <p>A value that indicates whether virtual scrolling is allowed. Set allowVirtualScrolling to true when using large amounts of data to improve efficiency.
  * Obsoleted, set the scrollingSettings.virtualization.mode property to "rows" instead.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * This option is ignored if the grid uses paging, columns merging or fixed rows. This option cannot be enabled when using dynamic wijdatasource.
  * @example
  * $("#element").wijgrid({ allowVirtualScrolling: false });
  */
wijmo.grid.IWijgridOptions.prototype.allowVirtualScrolling = null;
/** <p>This function is called each time wijgrid needs to change cell appearence, for example, when the current cell position is changed or cell is selected.
  * Can be used for customization of cell style depending on its state.</p>
  * @field 
  * @type {function}
  * @remarks
  * The args.state parameters equal to wijmo.grid.renderState.rendering means that the cell is being created,
  * at this moment you can apply general formatting to it indepentant of any particular state, like "current" or "selected".
  * @example
  * // Make the text of the current cell italic.
  * $("#element").wijgrid({
  *     highlightCurrentCell: true,
  *     cellStyleFormatter: function(args) {
  *         if ((args.row.type &amp; wijmo.grid.rowType.data)) {
  *             if (args.state &amp; wijmo.grid.renderState.current) {
  *                 args.$cell.css("font-style", "italic");
  *             } else {
  *                 args.$cell.css("font-style", "normal");
  *             }
  *         }
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.cellStyleFormatter = null;
/** <p>A value that indicates calendar's options in grid. It works for calendar in inputdate.</p>
  * @field 
  * @type {object}
  * @remarks
  * Its value is wijcalendar's option, visit 
  * http://wijmo.com/docs/wijmo/#Wijmo~jQuery.fn.-~wijcalendar.html for more details.
  * @example
  * $("#eventscalendar").wijgrid(
  *      { calendar: { prevTooltip: "Previous", nextTooltip: "Next" } });
  */
wijmo.grid.IWijgridOptions.prototype.calendar = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IColumn.html'>wijmo.grid.IColumn[]</a></p>
  * <p>An array of column options.</p>
  * @field 
  * @type {wijmo.grid.IColumn[]}
  * @example
  * $("#element").wijgrid({ columns: [ { headerText: "column0", allowSort: false }, { headerText: "column1", dataType: "number" } ] });
  */
wijmo.grid.IWijgridOptions.prototype.columns = null;
/** <p>Determines behavior for column autogeneration. Possible values are: "none", "append", "merge".</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are:
  * "none": Column auto-generation is turned off.
  * "append": A column will be generated for each data field and added to the end of the columns collection.
  * "merge": Each column having dataKey option not specified will be automatically bound to the first unreserved data field.For each data field not bound to any column a new column will be generated and added to the end of the columns collection.
  * To prevent automatic binding of a column to a data field set its dataKey option to null.
  * Note: columns autogeneration process affects the options of columns and the columns option itself.
  * @example
  * $("#element").wijgrid({ columnsAutogenerationMode: "merge" });
  */
wijmo.grid.IWijgridOptions.prototype.columnsAutogenerationMode = null;
/** <p>Determines the culture ID.</p>
  * @field 
  * @type {string}
  * @remarks
  * Please see the https://github.com/jquery/globalize for more information.
  * @example
  * // This code sets the culture to English.
  * $("#element").wijgrid({ culture: "en" });
  */
wijmo.grid.IWijgridOptions.prototype.culture = null;
/** <p>A value that indicators the culture calendar to format the text.
  * This option must work with culture option.</p>
  * @field 
  * @type {string}
  * @example
  * // This code sets the culture and calendar to Japanese.
  * $("#element").wijgrid({ culture: "ja-jp", cultureCalendar: "Japanese" });
  */
wijmo.grid.IWijgridOptions.prototype.cultureCalendar = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IFilterOperator.html'>wijmo.grid.IFilterOperator[]</a></p>
  * <p>An array of custom user filters. Use this option if you want to extend the default set of filter operators with your own. Custom filters will be shown in the filter dropdown.</p>
  * @field 
  * @type {wijmo.grid.IFilterOperator[]}
  * @example
  * var oddFilterOp = {
  * name: "customOperator-Odd",
  * arity: 1,
  * applicableTo: ["number"],
  * operator: function(dataVal) { return (dataVal % 2 !== 0); }
  * }
  * $("#element").wijgrid({ customFilterOperators: [oddFilterOp] });
  */
wijmo.grid.IWijgridOptions.prototype.customFilterOperators = null;
/** <p>Determines the datasource.
  * Possible datasources include:
  * 	1. A DOM table. This is the default datasource, used if the data option is null. Table must have no cells with rowSpan and colSpan attributes.
  * 	2. A two-dimensional array, such as [[0, "a"], [1, "b"]].
  * 	3. An array of objects, such as [{field0: 0, field1: "a"}, {field0: 1, field1: "b'}].
  * 	4. A wijdatasource.   
  * 	5. A wijdataview.</p>
  * @field 
  * @example
  * // DOM table
  * $("#element").wijgrid();
  * // two-dimensional array
  * $("#element").wijgrid({ data: [[0, "a"], [1, "b"]] });
  */
wijmo.grid.IWijgridOptions.prototype.data = null;
/** <p>Determines an action to bring a cell in the editing mode when the editingMode option is set to "cell". Possible values are: "click", "doubleClick", "auto".</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are:
  * "click": cell is edited via a single click.
  * "doubleClick": cell is edited via a double click.
  * "auto": action is determined automatically depending upon user environment. If user has a mobile platform then "click" is used, "doubleClick" otherwise.
  * @example
  * $("#element").wijgrid({ editingInitOption: "auto" });
  */
wijmo.grid.IWijgridOptions.prototype.editingInitOption = null;
/** <p>Determines the editing mode. Possible values are: "none", "row", "cell",</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are:
  * "none": the editing ability is disabled.
  * "cell": a single cell can be edited via a double click.
  * "row": a whole row can be edited via a command column.
  * @example
  * $("#element").wijgrid({
  *    editingMode: "row",
  *    columns: [{
  *       showEditButton: true
  *    }] 
  * });
  */
wijmo.grid.IWijgridOptions.prototype.editingMode = null;
/** <p>Determines if the exact column width, in pixels, is used.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * By default, wijgrid emulates the table element behavior when using a number as the width. This means wijgrid may not have the exact width specified. If exact width is needed, please set the ensureColumnsPxWidth option of wijgrid to true. If this option is set to true, wijgrid will not expand itself to fit the available space.Instead, it will use the width option of each column widget.
  * @example
  * $("#element").wijgrid({ ensureColumnsPxWidth: true });
  */
wijmo.grid.IWijgridOptions.prototype.ensureColumnsPxWidth = null;
/** <p>Determines the order of items in the filter drop-down list.
  * Possible values are: "none", "alphabetical", "alphabeticalCustomFirst" and "alphabeticalEmbeddedFirst"</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are:
  * "none": Operators follow the order of addition; built-in operators appear before custom ones.
  * "alphabetical": Operators are sorted alphabetically.
  * "alphabeticalCustomFirst": Operators are sorted alphabetically with custom operators appearing before built-in ones.
  * "alphabeticalEmbeddedFirst": Operators are sorted alphabetically with built-in operators appearing before custom operators.
  * "NoFilter" operator is always first.
  * @example
  * $("#element").wijgrid({ filterOperatorsSortMode: "alphabeticalCustomFirst" });
  */
wijmo.grid.IWijgridOptions.prototype.filterOperatorsSortMode = null;
/** <p>Determines whether the user can change position of the static column or row by dragging the vertical or horizontal freezing handle with the mouse. Possible values are: "none", "columns", "rows", "both".
  * Obsoleted, use the scrollingSettings.freezingMode property instead.</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are:
  * "none": The freezing handle cannot be dragged.
  * "columns": The user can drag the vertical freezing handle to change position of the static column.
  * "rows": The user can drag the horizontal freezing handle to change position of the static row.
  * "both": The user can drag both horizontal and vertical freezing handles.
  * @example
  * $("#element").wijgrid({ freezingMode: "both" });
  */
wijmo.grid.IWijgridOptions.prototype.freezingMode = null;
/** <p>Determines the caption of the group area.</p>
  * @field 
  * @type {string}
  * @example
  * // Set the groupAreaCaption to a string and the text appears above the grid
  * $("#element").wijgrid({ groupAreaCaption: "Drag a column here to group by that column." });
  */
wijmo.grid.IWijgridOptions.prototype.groupAreaCaption = null;
/** <p>Determines the indentation of the groups, in pixels.</p>
  * @field 
  * @type {number}
  * @example
  * // Set the groupIndent option to the number of pixels to indent data when grouping.
  * $("#element").wijgrid({ groupIndent: 15 });
  */
wijmo.grid.IWijgridOptions.prototype.groupIndent = null;
/** <p>Determines whether the position of the current cell is highlighted or not.</p>
  * @field 
  * @type {boolean}
  * @example
  * $("#element").wijgrid({ highlightCurrentCell: false });
  */
wijmo.grid.IWijgridOptions.prototype.highlightCurrentCell = null;
/** <p>Determines whether hovered row is highlighted or not.</p>
  * @field 
  * @type {boolean}
  * @example
  * $("#element").wijgrid({ highlightOnHover: true });
  */
wijmo.grid.IWijgridOptions.prototype.highlightOnHover = null;
/** <p>Determines the text to be displayed when the grid is loading.</p>
  * @field 
  * @type {string}
  * @example
  * $("#element").wijgrid({ loadingText: "Loading..."});
  */
wijmo.grid.IWijgridOptions.prototype.loadingText = null;
/** <p>Cell values equal to this property value are considered null values. Use this option if you want to change default representation of null values (empty strings) with something else.</p>
  * @field 
  * @type {string}
  * @remarks
  * Case-sensitive for built-in parsers.
  * @example
  * $("#element").wijgrid({ nullString: "" });
  */
wijmo.grid.IWijgridOptions.prototype.nullString = null;
/** <p>Determines the zero-based index of the current page. You can use this to access a specific page, for example, when using the paging feature.</p>
  * @field 
  * @type {number}
  * @example
  * $("#element").wijgrid({ pageIndex: 0 });
  */
wijmo.grid.IWijgridOptions.prototype.pageIndex = null;
/** <p>Number of rows to place on a single page.
  * The default value is 10.</p>
  * @field 
  * @type {number}
  * @example
  * // The pageSize here sets 10 rows to a page. The allowPaging option is set to true so paging buttons appear.
  * $("#element").wijgrid({ pageSize: 10 });
  */
wijmo.grid.IWijgridOptions.prototype.pageSize = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IPagerSettings.html'>wijmo.grid.IPagerSettings</a></p>
  * <p>Determines the pager settings for the grid including the mode (page buttons or next/previous buttons), number of page buttons, and position where the buttons appear.</p>
  * @field 
  * @type {wijmo.grid.IPagerSettings}
  * @remarks
  * See the wijpager documentation for more information on pager settings.
  * @example
  * // Display the pager at the top of the wijgrid.
  * $("#element").wijgrid({ pagerSettings: { position: "top" } });
  */
wijmo.grid.IWijgridOptions.prototype.pagerSettings = null;
/** <p>A value indicating whether DOM cell attributes can be passed within a data value.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * This option allows binding collection of values to data and automatically converting them as attributes of corresponded DOM table cells during rendering.
  * Values should be passed as an array of two items, where first item is a value of the data field, the second item is a list of values:
  * $("#element").wijgrid({
  * data: [
  * [ [1, { "style": "color: red", "class": "myclass" } ], a ]
  * ]
  * });
  * or
  * $("#element").wijgrid({
  * data: [
  * { col0: [1, { "style": "color: red", "class": "myclass" }], col1: "a" }
  * ]
  * });
  * Note: during conversion wijgrid extracts the first item value and makes it data field value, the second item (list of values) is removed:
  * [ { col0: 1, col1: "a" } ]
  * If DOM table is used as a datasource then attributes belonging to the cells in tBody section of the original table will be read and applied to the new cells.
  * rowSpan and colSpan attributes are not allowed.
  * @example
  * // Render the style attribute passed within the data.
  * $("#element").wijgrid({
  *     readAttributesFromData: false });
  *     data: [
  *         [ [1, { "style": "color: red" } ], a ]
  *     ]
  * });
  */
wijmo.grid.IWijgridOptions.prototype.readAttributesFromData = null;
/** <p>Determines the height of a rows when virtual scrolling is used.
  * Obsoleted, use the scrollingSettings.virtualization.rowHeight property instead.</p>
  * @field 
  * @type {number}
  * @remarks
  * Can be set only during creation
  * @example
  * $("#element").wijgrid({ rowHeight: 20 });
  */
wijmo.grid.IWijgridOptions.prototype.rowHeight = null;
/** <p>Function used for styling rows in wijgrid.</p>
  * @field 
  * @type {function}
  * @example
  * // Make text of the alternating rows italic.
  * $("#demo").wijgrid({
  *     data: [
  *         [0, "Nancy"], [1, "Susan"], [2, "Alice"], [3, "Kate"]
  *     ],
  *     rowStyleFormatter: function (args) {
  *         if ((args.state &amp; wijmo.grid.renderState.rendering) &amp;&amp; (args.type &amp; wijmo.grid.rowType.dataAlt)) {
  *             args.$rows.find("td").css("font-style", "italic");
  *         }
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.rowStyleFormatter = null;
/** <p>Determines which scrollbars are active and if they appear automatically based on content size.
  * Possbile values are: "none", "auto", "horizontal", "vertical", "both".
  * Obsoleted, use the scrollingSettings.mode property instead.</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are:
  * "none": Scrolling is not used; the staticRowIndex and staticColumnIndex values are ignored.
  * "auto": Scrollbars appear automatically depending upon content size.
  * "horizontal": The horizontal scrollbar is active.
  * "vertical": The vertical scrollbar is active.
  * "both": Both horizontal and vertical scrollbars are active.
  * @example
  * // The horizontal and vertical scrollbars are active when the scrollMode is set to both.
  * $("#element").wijgrid({ scrollMode: "both" });
  */
wijmo.grid.IWijgridOptions.prototype.scrollMode = null;
/** <p>Determines which cells, range of cells, columns, or rows can be selected at one time.
  * Possible values are: "none", "singleCell", "singleColumn", "singleRow", "singleRange", "multiColumn", "multiRow" and "multiRange".</p>
  * @field 
  * @type {string}
  * @remarks
  * Possible values are: 
  * "none": Selection is turned off.
  * "singleCell": Only a single cell can be selected at a time.
  * "singleColumn": Only a single column can be selected at a time.
  * "singleRow": Only a single row can be selected at a time.
  * "singleRange": Only a single range of cells can be selected at a time.
  * "multiColumn": It is possible to select more than one row at the same time using the mouse and the CTRL or SHIFT keys.
  * "multiRow": It is possible to select more than one row at the same time using the mouse and the CTRL or SHIFT keys.
  * "multiRange": It is possible to select more than one cells range at the same time using the mouse and the CTRL or SHIFT keys.
  * @example
  * // Set selectionMode to muliColumn and users can select more than one column using the CTRL or SHIFT keys.
  * $("#element").wijgrid({ selectionMode: "multiColumn" });
  */
wijmo.grid.IWijgridOptions.prototype.selectionMode = null;
/** <p>A value indicating whether the filter row is visible.
  * Filter row is used to display column filtering interface.</p>
  * @field 
  * @type {boolean}
  * @example
  * // Set showFilter to true to view the filter row.
  * $("#element").wijgrid({ showFilter: true });
  */
wijmo.grid.IWijgridOptions.prototype.showFilter = null;
/** <p>A value indicating whether the footer row is visible.
  * Footer row is used for displaying of tfoot section of original table, and to show totals.</p>
  * @field 
  * @type {boolean}
  * @example
  * // Set showFooter to true to view the footer row.
  * $("#element").wijgrid({ showFooter: true });
  */
wijmo.grid.IWijgridOptions.prototype.showFooter = null;
/** <p>A value indicating whether group area is visible.
  * Group area is used to display headers of groupped columns. User can drag columns from/to group area by dragging column headers with mouse, if allowColMoving option is on.</p>
  * @field 
  * @type {boolean}
  * @example
  * // Set showGroupArea to true to display the group area.
  * $("#element").wijgrid({ showGroupArea: true });
  */
wijmo.grid.IWijgridOptions.prototype.showGroupArea = null;
/** <p>A value indicating whether a selection will be automatically displayed at the current cell position when the wijgrid is rendered.
  * Set this option to false if you want to prevent wijgrid from selecting the currentCell automatically.</p>
  * @field 
  * @type {boolean}
  * @example
  * $("#element").wijgrid({ showSelectionOnRender: true });
  */
wijmo.grid.IWijgridOptions.prototype.showSelectionOnRender = null;
/** <p>A value indicating whether the row header is visible.</p>
  * @field 
  * @type {boolean}
  * @example
  * $("#element").wijgrid({ showRowHeader: true });
  */
wijmo.grid.IWijgridOptions.prototype.showRowHeader = null;
/** <p>Indicates the index of columns that will always be shown on the left when the grid view is scrolled horizontally.
  * Obsoleted, use the scrollingSettings.staticColumnIndex property instead.</p>
  * @field 
  * @type {number}
  * @remarks
  * Note that all columns before the static column will be automatically marked as static, too.
  * This can only take effect when the scrollMode option is not set to "none".
  * It will be considered "-1" when grouping or row merging is enabled. A "-1" means there is no data column but the row header is static. A zero (0) means one data column and row header are static.
  * @example
  * $("#element").wijgrid({ staticColumnIndex: -1 });
  */
wijmo.grid.IWijgridOptions.prototype.staticColumnIndex = null;
/** <p>Gets or sets the alignment of the static columns area. Possible values are "left", "right".
  * Obsoleted, use the scrollingSettings.staticColumnsAlignment property instead.</p>
  * @field 
  * @type {string}
  * @remarks
  * The "right" mode has limited functionality:
  * - The showRowHeader value is ignored.
  * - Changing staticColumnIndex at run-time by dragging the vertical bar is disabled.
  * @example
  * $("#element").wijgrid({ staticColumnsAlignment: "left" });
  */
wijmo.grid.IWijgridOptions.prototype.staticColumnsAlignment = null;
/** <p>Indicates the index of data rows that will always be shown on the top when the wijgrid is scrolled vertically.
  * Obsoleted, use the scrollingSettings.staticRowIndext property instead.</p>
  * @field 
  * @type {number}
  * @remarks
  * Note, that all rows before the static row will be automatically marked as static, too.
  * This can only take effect when the scrollMode option is not set to "none". This will be considered "-1" when grouping or row merging is enabled.
  * A "-1" means there is no data row but the header row is static.A zero (0) means one data row and the row header are static.
  * @example
  * $("#element").wijgrid({ staticRowIndex: -1 });
  */
wijmo.grid.IWijgridOptions.prototype.staticRowIndex = null;
/** <p>Gets or sets the virtual number of items in the wijgrid and enables custom paging.
  * Setting option to a positive value activates custom paging, the number of displayed rows and the total number of pages will be determined by the totalRows and pageSize values.</p>
  * @field 
  * @type {number}
  * @remarks
  * In custom paging mode sorting, paging and filtering are not performed automatically.
  * This must be handled manually using the sorted, pageIndexChanged, and filtered events. Load the new portion of data there followed by the ensureControl(true) method call.
  * @example
  * $("#element").wijgrid({ totalRows: -1 });
  */
wijmo.grid.IWijgridOptions.prototype.totalRows = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IScrollingSettings.html'>wijmo.grid.IScrollingSettings</a></p>
  * <p>Determines the scrolling settings.</p>
  * @field 
  * @type {wijmo.grid.IScrollingSettings}
  * @example
  * // The horizontal and vertical scrollbars are active when the scrollMode is set to both.
  * $("#element").wijgrid({ scrollingSettings: { mode: "both" } });
  */
wijmo.grid.IWijgridOptions.prototype.scrollingSettings = null;
/** <p>The afterCellEdit event handler is a function called after cell editing is completed.
  * This function can assist you in completing many tasks, such as in making changes once editing is completed; in tracking changes in cells, columns, or rows; or in integrating custom editing functions on the front end.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ afterCellEdit: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridaftercelledit", function (e, args) {
  * // some code here
  * });
  * @example
  * // Once cell editing is complete, the function calls the destroy method to destroy the wijcombobox widget and the wijinputnumber widget which are used as the custom editors.
  * $("#element").wijgrid({
  *     afterCellEdit: function(e, args) {
  *         switch (args.cell.column().dataKey) {
  *             case "Position":
  *                 args.cell.container().find("input").wijcombobox("destroy");
  *                 break;
  *             case "Acquired":
  *                 args.cell.container().find("input").wijinputnumber("destroy");
  *                 break;
  *         }
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.afterCellEdit = null;
/** <p>The afterCellUpdate event handler is a function that is called after a cell has been updated. Among other functions, this event allows you to track and store the indices of changed rows or columns.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ afterCellUpdate: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridaftercellupdate", function (e, args) {
  * // some code here
  * });
  * @example
  * // Once the cell has been updated, the information from the underlying data is dumped into the "#log" element.
  * $("#element").wijgrid({
  *     afterCellUpdate: function(e, args) {
  *         $("#log").html(dump($("#demo").wijgrid("data")));
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.afterCellUpdate = null;
/** <p>The beforeCellEdit event handler is a function that is called before a cell enters edit mode.
  * The beforeCellEdit event handler assists you in appending a widget, data, or other item to a wijgrid's cells before the cells enter edit mode. This event is cancellable if the editigMode options is set to "cell".</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ beforeCellEdit: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridbeforecelledit", function (e, args) {
  * // some code here
  * });
  * @example
  * // Allow the user to change the price only if the product hasn't been discontinued:
  * $("#element").wijgrid({
  *     beforeCellEdit: function(e, args) {
  *         return !((args.cell.column().dataKey === "Price") &amp;&amp; args.cell.row().data.Discontinued);
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.beforeCellEdit = null;
/** <p>The beforeCellUpdate event handler is a function that is called before the cell is updated with new or user-entered data. This event is cancellable if the editingMode options is set to "cell".
  * There are many instances where this event is helpful, such as when you need to check a cell's value before the update occurs or when you need to apply an alert message based on the cell's value.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ beforeCellUpdate: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridbeforecellupdate", function (e, args) {
  * // some code here
  * });
  * @example
  * // In this sample, you use args.value to check the year that the user enters in the "Acquired" column.
  * // If it's less than 1990 or greater than the current year, then the event handler will return false to cancel updating and show the user an alert message.
  * $("#element").wijgrid({
  *     beforeCellUpdate: function(e, args) {
  *         switch (args.cell.column().dataKey) { 
  *             case "Acquired":
  *                 var $editor = args.cell.container().find("input"),
  *                     value = $editor.wijinputnumber("getValue"),
  *                     curYear = new Date().getFullYear();
  *                 if (value &lt; 1990 || value &gt; curYear) {
  *                     $editor.addClass("ui-state-error");
  *                     alert("value must be between 1990 and " + curYear);
  *                     $editor.focus();
  *                     return false; 
  *                 }
  *                 args.value = value; 
  *                 break;
  *         }
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.beforeCellUpdate = null;
/** <p>The cellClicked event handler is a function that is called when a cell is clicked. You can use this event to get the information of a clicked cell using the args parameter.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ cellClicked: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcellclicked", function (e, args) {
  * // some code here
  * });
  * @example
  * // The sample uses the cellClicked event to trigger an alert when the cell is clicked.
  * $("#element").wijgrid({
  *     cellClicked: function (e, args) {
  *         alert(args.cell.value());
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.cellClicked = null;
/** <p>The columnDragging event handler is a function that is called when column dragging has been started, but before the wijgrid handles the operation. This event is cancellable.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnDragging: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumndragging", function (e, args) {
  * // some code here
  * });
  * @example
  * // Preventing a user from dragging a specific column
  * $("#element").wijgrid({
  *     columnDragging: function (e, args) {
  *         return !(args.drag.dataKey == "ID");
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.columnDragging = null;
/** <p>The columnDragged event handler is a function that is called when column dragging has been started. You can use this event to find the column being dragged or the dragged column's location.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnDragged: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumndragged", function (e, args) {
  * // some code here
  * });
  * @example
  * // Supply a callback function to handle the columnDragged event:
  * $("#element").wijgrid({
  *     columnDragged: function (e, args) {
  *         alert("The '" + args.drag.headerText + "' column is being dragged from the '" + args.dragSource + "' location");
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.columnDragged = null;
/** <p>The columnDropping event handler is a function that is called when a column is dropped into the columns area, but before wijgrid handles the operation. This event is cancellable.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnDropping: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumndropping", function (e, args) {
  * // some code here
  * });
  * @example
  * // Preventing user from dropping any column before the "ID" column.
  * $("#element").wijgrid({
  *     columnDropping: function (e, args) {
  *         return !(args.drop.dataKey == "ID" &amp;&amp; args.at == "left");
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.columnDropping = null;
/** <p>The columnDropped event handler is a function that is called when a column has been dropped into the columns area.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnDropped: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumndropped", function (e, args) {
  * // some code here
  * });
  * @example
  * // Supply a callback function to handle the columnDropped event:
  * $("#element").wijgrid({
  *     columnDropped: function (e, args) {
  *         "The '" + args.drag.headerText + "' column has been dropped onto the '" + args.drop.headerText + "' column at the '" + args.at + "' position"
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.columnDropped = null;
/** <p>The columnGrouping event handler is a function that is called when a column is dropped into the group area, but before the wijgrid handles the operation. This event is cancellable.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnGrouping: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumngrouping", function (e, args) {
  * // some code here
  * });
  * @example
  * // Preventing user from grouping the "UnitPrice" column.
  * $("#element").wijgrid({
  *     columnGrouping: function (e, args) {
  *         return !(args.drag.headerText == "UnitPrice");
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.columnGrouping = null;
/** <p>The columnGrouped event handler is a function that is called when a column has been dropped into the group area.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnGrouped: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumngrouped", function (e, args) {
  * // some code here
  * });
  * @example
  * // Supply a callback function to handle the columnGrouped event:
  * $("#element").wijgrid({
  *     columnGrouped: function (e, args) {
  *         alert("The '" + args.drag.headerText "' column has been grouped");
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.columnGrouped = null;
/** <p>The columnResizing event handler is called when a user resizes the column but before the wijgrid handles the operation. This event is cancellable.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnResizing: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumnresizing", function (e, args) {
  * // some code here
  * });
  * @example
  * // Prevent setting the width of "ID" column less than 100 pixels
  * $("#element").wijgrid({
  *     columnResizing: function (e, args) {
  *         if (args.column.dataKey == "ID" &amp;&amp; args.newWidth &lt; 100) {
  *             args.newWidth = 100;
  *         }
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.columnResizing = null;
/** <p>The columnResized event handler is called when a user has changed a column's size.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnResized: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumnresized", function (e, args) {
  * // some code here
  * });
  * @example
  * // Supply a callback function to handle the columnGrouped event:
  * $("#element").wijgrid({
  *     columnResized: function (e, args) {
  *         alert("The '" + args.column.headerText + "' has been resized");
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.columnResized = null;
/** <p>The columnUngrouping event handler is called when a column has been removed from the group area but before the wjgrid handles the operation. This event is cancellable.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnUngrouping: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumnungrouping", function (e, args) {
  * // some code here
  * });
  * @example
  * // Preventing user from ungrouping the "UnitPrice" column.
  * $("#element").wijgrid({
  *     columnUngrouping: function (e, args) {
  *         return !(args.column.headerText == "UnitPrice");
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.columnUngrouping = null;
/** <p>The columnUngrouped event handler is called when a column has been removed from the group area.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ columnUngrouped: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcolumnungrouped", function (e, args) {
  * // some code here
  * });
  * @example
  * // Supply a callback function to handle the columnGrouped event:
  * $("#element").wijgrid({
  *     columnUngrouped: function (e, args) {
  *         alert("The '" + args.column.headerText + "' has been ungrouped");
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.columnUngrouped = null;
/** <p>The currentCellChanging event handler is called before the cell is changed. You can use this event to get a selected row or column or to get a data row bound to the current cell. This event is cancellable.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ currentCellChanging: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcurrentcellchanging", function (e, args) {
  * // some code here
  * });
  * @example
  * // Gets the data row bound to the current cell.
  * $("#element").wijgrid({
  *     currentCellChanging: function (e, args) {
  *         var rowObj = $(e.target).wijgrid("currentCell").row();
  *         if (rowObj) {        
  *             var dataItem = rowObj.data; // current data item (before the cell is changed).
  *         }
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.currentCellChanging = null;
/** <p>The currentCellChanged event handler is called after the current cell is changed.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ currentCellChanged: function (e) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridcurrentcellchanged", function (e) {
  * // some code here
  * });
  * @example
  * // Gets the data row bound to the current cell.
  * $("#element").wijgrid({
  *     currentCellChanged: function (e, args) {
  *         var rowObj = $(e.target).wijgrid("currentCell").row();
  *         if (rowObj) {        
  *             var dataItem = rowObj.data; // current data item (after the cell is changed).
  *         }
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.currentCellChanged = null;
/** <p>The detailCreating event handler is called when wijgrid requires to create a new detail wijgrid.</p>
  * @field 
  * @type {function}
  * @remarks
  * Event receives options of a detail grid to create, which were obtained by cloning the detail option of the master grid.
  * User can alter the detail grid options here and provide a specific datasource within the args.options.data option to implement run-time hierarachy.
  */
wijmo.grid.IWijgridOptions.prototype.detailCreating = null;
/** <p>The filterOperatorsListShowing event handler is a function that is called before the filter drop-down list is shown. You can use this event to customize the list of filter operators for your users.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ filterOperatorsListShowing: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridfilteroperatorslistshowing", function (e, args) {
  * // some code here
  * });
  * @example
  * // Limit the filters that will be shown to the "Equals" filter operator
  * $("#element").wijgrid({
  *     filterOperatorsListShowing: function (e, args) {
  *         args.operators = $.grep(args.operators, function(op) {
  *             return op.name === "Equals" || op.name === "NoFilter";
  *         }
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.filterOperatorsListShowing = null;
/** <p>The filtering event handler is a function that is called before the filtering operation is started. For example, you can use this event to change a filtering condition before a filter will be applied to the data. This event is cancellable.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ filtering: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridfiltering", function (e, args) {
  * // some code here
  * });
  * @example
  * // Prevents filtering by negative values
  * $("#element").wijgrid({
  *     filtering: function (e, args) {
  *         if (args.column.dataKey == "Price" &amp;&amp; args.value &lt; 0) {
  *             args.value = 0;
  *         }
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.filtering = null;
/** <p>The filtered event handler is a function that is called after the wijgrid is filtered.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ filtered: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridfiltered", function (e, args) {
  * // some code here
  * });
  * @example
  * // 
  * $("#element").wijgrid({
  *     filtered: function (e, args) {
  *         alert("The filtered data contains: " + $(this).wijgrid("dataView").count() + " rows");
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.filtered = null;
/** <p>The groupAggregate event handler is a function that is called when groups are being created and the column object's aggregate option has been set to "custom". This event is useful when you want to calculate custom aggregate values.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ groupAggregate: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridgroupaggregate", function (e, args) {
  * // some code here
  * });
  * @example
  * // This sample demonstrates using the groupAggregate event handler to calculate an average in a custom aggregate:
  * $("#element").wijgrid({
  *     groupAggregate: function (e, args) {
  *         if (args.column.dataKey == "Price") {
  *             var aggregate = 0;
  *             for (var i = args.groupingStart; i &lt;= args.groupingEnd; i++) {
  *                 aggregate += args.data[i].valueCell(args.column.dataIndex).value;
  *             }
  *             aggregate = aggregate/ (args.groupingEnd - args.groupingStart + 1);
  *             args.text = aggregate;
  *         }
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.groupAggregate = null;
/** <p>The groupText event handler is a function that is called when groups are being created and the groupInfo option has the groupInfo.headerText or the groupInfo.footerText options set to "custom". This event can be used to customize group headers and group footers.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ groupText: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridgrouptext", function (e, args) {
  * // some code here
  * });
  * @example
  * // The following sample sets the groupText event handler to avoid empty cells. The custom formatting applied to group headers left certain cells appearing as if they were empty. This code avoids that:
  * $("#element").wijgrid({
  *     groupText: function (e, args) {
  *         if (!args.groupText) {
  *             args.text = "null";
  *         }
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.groupText = null;
/** <p>The invalidCellValue event handler is a function called when a cell needs to start updating but the cell value is invalid. So if the value in a wijgrid cell can't be converted to the column target type, the invalidCellValue event will fire.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ invalidCellValue: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridinvalidcellvalue", function (e, args) {
  * // some code here
  * });
  * @example
  * // Adds a style to the cell if the value entered is invalid
  * $("#element").wijgrid({
  *     invalidCellValue: function (e, args) {
  *         $(args.cell.container()).addClass("ui-state-error");
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.invalidCellValue = null;
/** <p>The pageIndexChanging event handler is a function that is called before the page index is changed. This event is cancellable.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ pageIndexChanging: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridpageindexchanging", function (e, args) {
  * // some code here
  * });
  * @example
  * // Cancel the event by returning false
  * $("#element").wijgrid({
  *     pageIndexChanging: function (e, args) {
  *         return false;
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.pageIndexChanging = null;
/** <p>The pageIndexChanged event handler is a function that is called after the page index is changed, such as when you use the numeric buttons to swtich between pages or assign a new value to the pageIndex option.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ pageIndexChanged: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridpageindexchanged", function (e, args) {
  * // some code here
  * });
  * @example
  * // Supply a callback function to handle the pageIndexChanged event:
  * $("#element").wijgrid({
  *     pageIndexChanged: function (e, args) {
  *         alert("The new pageIndex is: " + args.newPageIndex);
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.pageIndexChanged = null;
/** <p>The selectionChanged event handler is a function that is called after the selection is changed.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ selectionChanged: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridselectionchanged", function (e, args) {
  * // some code here
  * });
  * @example
  * // Get the value of the first cell of the selected row.
  * $("#element").wijgrid({
  *     selectionMode: "singleRow",
  *     selectionChanged: function (e, args) {
  *         alert(args.addedCells.item(0).value());
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.selectionChanged = null;
/** <p>The sorting event handler is a function that is called before the sorting operation is started. This event is cancellable.
  * The allowSorting option must be set to "true" for this event to fire.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ sorting: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridsorting", function (e, args) {
  * // some code here
  * });
  * @example
  * // Preventing user from sorting the "ID" column.
  * $("#element").wijgrid({
  *     sorting: function (e, args) {
  *         return !(args.column.headerText === "ID");
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.sorting = null;
/** <p>The sorted event handler is a function that is called after the widget is sorted. The allowSorting option must be set to "true" to allow this event to fire.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ sorted: function (e, args) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridsorted", function (e, args) {
  * // some code here
  * });
  * @example
  * // The following code handles the sorted event and will give you access to the column and the sort direction
  * $("#element").wijgrid({
  *     sorted: function (e, args) {
  *         alert("Column " + args.column.headerText + " sorted in " + args.sortDirection + " order");
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.sorted = null;
/** <p>The dataLoading event handler is a function that is called when the wijgrid loads a portion of data from the underlying datasource. This can be used for modification of data sent to server if using dynamic remote wijdatasource.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ dataLoading: function (e) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgriddataloading", function (e) {
  * // some code here
  * });
  * @example
  * // This sample allows you to set the session ID when loading a portion of data from the remote wijdatasource:
  * $("#element").wijgrid({
  *     data: new wijdatasource({
  *         proxy: new wijhttpproxy({
  *             // some code here
  *         })
  *     }),
  *     dataLoading: function (e) {
  *         var dataSource = $(this).wijgrid("option", "data");
  *         dataSource.proxy.options.data.sessionID = getSessionID();
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.dataLoading = null;
/** <p>The dataLoaded event handler is a function that is called when data is loaded.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ dataLoaded: function (e) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgriddataloaded", function (e) {
  * // some code here
  * });
  * @example
  * // Display the number of entries found
  * $("#element").wijgrid({
  *     dataLoaded: function (e) {
  *         alert($(this).wijgrid("dataView").count());
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.dataLoaded = null;
/** <p>The loading event handler is a function that is called at the beginning of the wijgrid's lifecycle. You can use this event to activate a custom load progress indicator.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ loading: function (e) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridloading", function (e) {
  * // some code here
  * });
  * @example
  * // Creating an indeterminate progressbar during loading
  * $("#element").wijgrid({
  *     loading: function (e) {
  *         $("#progressBar").show().progressbar({ value: false });
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.loading = null;
/** <p>The loaded event handler is a function that is called at the end the wijgrid's lifecycle when wijgrid is filled with data and rendered. You can use this event to manipulate the grid html content or to finish a custom load indication.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ loaded: function (e) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridloaded", function (e) {
  * // some code here
  * });
  * @example
  * // The loaded event in the sample below ensures that whatever is selected on load is cleared
  * $("#element").wijgrid({
  *     loaded: function (e) {
  *         $(e.target).wijgrid("selection").clear(); // clear selection                    
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.loaded = null;
/** <p>The rendering event handler is a function that is called when the wijgrid is about to render. Normally you do not need to use this event.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ rendering: function (e) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridrendering", function (e) {
  * // some code here
  * });
  * @example
  * $("#element").wijgrid({
  *     rendering: function (e) {
  *         alert("rendering");
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.rendering = null;
/** <p>The rendered event handler is a function that is called when the wijgrid is rendered. Normally you do not need to use this event.</p>
  * @field 
  * @type {function}
  * @remarks
  * You can bind to the event either by type or by name.
  * Bind to the event by name:
  * $("#element").wijgrid({ rendered: function (e) {
  * // some code here
  * }});
  * Bind to the event by type:
  * $("#element").bind("wijgridrendered", function (e) {
  * // some code here
  * });
  * @example
  * $("#element").wijgrid({
  *     rendered: function (e) {
  *         alert("rendered");
  *     }
  * });
  */
wijmo.grid.IWijgridOptions.prototype.rendered = null;
/** Determines a master-detail relation in a hierarchy grid.
  * @interface IMasterDetailRelation
  * @namespace wijmo.grid
  */
wijmo.grid.IMasterDetailRelation = function () {};
/** <p>Determines the name of the master key field.</p>
  * @field 
  * @type {string}
  */
wijmo.grid.IMasterDetailRelation.prototype.detailDataKey = null;
/** <p>Determines the name of the detail key field.</p>
  * @field 
  * @type {string}
  */
wijmo.grid.IMasterDetailRelation.prototype.masterDataKey = null;
/** Represents options of the detail grid.
  * @interface IDetailSettings
  * @namespace wijmo.grid
  * @extends wijmo.grid.IWijgridOptions
  */
wijmo.grid.IDetailSettings = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.IMasterDetailRelation.html'>wijmo.grid.IMasterDetailRelation[]</a></p>
  * <p>Determines an array of IMasterDetailRelation objects that represent master-detail relations in a hierarchical grid.</p>
  * @field 
  * @type {wijmo.grid.IMasterDetailRelation[]}
  */
wijmo.grid.IDetailSettings.prototype.relation = null;
/** <p>Determines whether the hierarchy will be expanded by default or not.</p>
  * @field 
  * @type {boolean}
  */
wijmo.grid.IDetailSettings.prototype.startExpanded = null;
/** <p>Determines the height of the detail grid.</p>
  * @field
  */
wijmo.grid.IDetailSettings.prototype.height = null;
/** <p>Determines the width of the detail grid.</p>
  * @field
  */
wijmo.grid.IDetailSettings.prototype.width = null;
/** Represents a collection of field values of a data item in a datasource.
  * @interface IDataKeyArray
  * @namespace wijmo.grid
  */
wijmo.grid.IDataKeyArray = function () {};
typeof wijmo.pager.IPagerUISettings != 'undefined' && $.extend(wijmo.grid.IPagerSettings.prototype, wijmo.pager.IPagerUISettings.prototype);
typeof wijmo.grid.IColumnInternalOptions != 'undefined' && $.extend(wijmo.grid.IC1BaseFieldOptions.prototype, wijmo.grid.IColumnInternalOptions.prototype);
typeof wijmo.grid.IC1BaseFieldOptions != 'undefined' && $.extend(wijmo.grid.IC1FieldOptions.prototype, wijmo.grid.IC1BaseFieldOptions.prototype);
typeof wijmo.grid.IC1BaseFieldOptions != 'undefined' && $.extend(wijmo.grid.IC1BandFieldOptions.prototype, wijmo.grid.IC1BaseFieldOptions.prototype);
typeof wijmo.grid.IC1BaseFieldOptions != 'undefined' && $.extend(wijmo.grid.IC1ButtonBaseFieldOptions.prototype, wijmo.grid.IC1BaseFieldOptions.prototype);
typeof wijmo.grid.IC1ButtonBaseFieldOptions != 'undefined' && $.extend(wijmo.grid.IC1ButtonFieldOptions.prototype, wijmo.grid.IC1ButtonBaseFieldOptions.prototype);
typeof wijmo.grid.IC1ButtonBaseFieldOptions != 'undefined' && $.extend(wijmo.grid.IC1CommandButtonFieldOptions.prototype, wijmo.grid.IC1ButtonBaseFieldOptions.prototype);
typeof wijmo.grid.IColumnInternalOptions != 'undefined' && $.extend(wijmo.grid.IColumn.prototype, wijmo.grid.IColumnInternalOptions.prototype);
typeof wijmo.grid.IC1BaseFieldOptions != 'undefined' && $.extend(wijmo.grid.IColumn.prototype, wijmo.grid.IC1BaseFieldOptions.prototype);
typeof wijmo.grid.IC1FieldOptions != 'undefined' && $.extend(wijmo.grid.IColumn.prototype, wijmo.grid.IC1FieldOptions.prototype);
typeof wijmo.grid.IC1BandFieldOptions != 'undefined' && $.extend(wijmo.grid.IColumn.prototype, wijmo.grid.IC1BandFieldOptions.prototype);
typeof wijmo.grid.IC1ButtonBaseFieldOptions != 'undefined' && $.extend(wijmo.grid.IColumn.prototype, wijmo.grid.IC1ButtonBaseFieldOptions.prototype);
typeof wijmo.grid.IC1ButtonFieldOptions != 'undefined' && $.extend(wijmo.grid.IColumn.prototype, wijmo.grid.IC1ButtonFieldOptions.prototype);
typeof wijmo.grid.IC1CommandButtonFieldOptions != 'undefined' && $.extend(wijmo.grid.IColumn.prototype, wijmo.grid.IC1CommandButtonFieldOptions.prototype);
typeof wijmo.grid.IBaseCellEditEventArgs != 'undefined' && $.extend(wijmo.grid.IBeforeCellEditEventArgs.prototype, wijmo.grid.IBaseCellEditEventArgs.prototype);
typeof wijmo.grid.IBaseCellEditEventArgs != 'undefined' && $.extend(wijmo.grid.IAfterCellEditEventArgs.prototype, wijmo.grid.IBaseCellEditEventArgs.prototype);
typeof wijmo.grid.IBaseCellEditEventArgs != 'undefined' && $.extend(wijmo.grid.IBeforeCellUpdateEventArgs.prototype, wijmo.grid.IBaseCellEditEventArgs.prototype);
typeof wijmo.grid.IBaseCellEditEventArgs != 'undefined' && $.extend(wijmo.grid.IAfterCellUpdateEventArgs.prototype, wijmo.grid.IBaseCellEditEventArgs.prototype);
typeof wijmo.grid.IBaseCellEditEventArgs != 'undefined' && $.extend(wijmo.grid.IInvalidCellValueEventArgs.prototype, wijmo.grid.IBaseCellEditEventArgs.prototype);
typeof wijmo.grid.IColumnDraggingEventArgs != 'undefined' && $.extend(wijmo.grid.IColumnDraggedEventArgs.prototype, wijmo.grid.IColumnDraggingEventArgs.prototype);
typeof wijmo.grid.IColumnDroppingEventArgs != 'undefined' && $.extend(wijmo.grid.IColumnDroppedEventArgs.prototype, wijmo.grid.IColumnDroppingEventArgs.prototype);
typeof wijmo.grid.IColumnGroupingEventArgs != 'undefined' && $.extend(wijmo.grid.IColumnGroupedEventArgs.prototype, wijmo.grid.IColumnGroupingEventArgs.prototype);
typeof wijmo.grid.IColumnUngroupingEventArgs != 'undefined' && $.extend(wijmo.grid.IColumnUngroupedEventArgs.prototype, wijmo.grid.IColumnUngroupingEventArgs.prototype);
typeof wijmo.grid.IGroupAggregateEventArgs != 'undefined' && $.extend(wijmo.grid.IGroupTextEventArgs.prototype, wijmo.grid.IGroupAggregateEventArgs.prototype);
typeof wijmo.grid.ISortingEventArgs != 'undefined' && $.extend(wijmo.grid.ISortedEventArgs.prototype, wijmo.grid.ISortingEventArgs.prototype);
typeof wijmo.grid.IWijgridOptions != 'undefined' && $.extend(wijmo.grid.IDetailSettings.prototype, wijmo.grid.IWijgridOptions.prototype);
})()
})()
