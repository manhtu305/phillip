var wijmo = wijmo || {};

(function () {
wijmo.rating = wijmo.rating || {};

(function () {
/** @class wijrating
  * @widget 
  * @namespace jQuery.wijmo.rating
  * @extends wijmo.wijmoWidget
  */
wijmo.rating.wijrating = function () {};
wijmo.rating.wijrating.prototype = new wijmo.wijmoWidget();
/** The destroy() method will remove the rating functionality completely 
  * and will return the element to its pre-init state.
  */
wijmo.rating.wijrating.prototype.destroy = function () {};

/** @class */
var wijrating_options = function () {};
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that determines whether or not to disable the rating widget.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijrating_options.prototype.disabled = false;
/** <p class='defaultValue'>Default value: 5</p>
  * <p>A value that determines the number of stars to display.</p>
  * @field 
  * @type {number}
  * @option
  */
wijrating_options.prototype.count = 5;
/** <p class='defaultValue'>Default value: 1</p>
  * <p>An option that determines the number of sections into which each star will be split.</p>
  * @field 
  * @type {number}
  * @option
  */
wijrating_options.prototype.split = 1;
/** <p class='defaultValue'>Default value: 5</p>
  * <p>An option that determines the total value of the rating widget.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * For example, in a rating widget with 5 stars the count is 5. A split of 2 means that each star is split into two parts. 
  * If the totalValue of the stars is 100, then each part of the split star has a value of 10 and each whole star has a value of 20. 
  * The step can be represented by this equation: 100/(5 * 2) = 10 and the value of one star can be represented by this equation: 10*(1 * 2) = 20.
  */
wijrating_options.prototype.totalValue = 5;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>An option that determines the rated value of the rating widget.</p>
  * @field 
  * @type {number}
  * @option
  */
wijrating_options.prototype.value = 0;
/** <p class='defaultValue'>Default value: null</p>
  * <p>An option that defines the minimum value that can be rated using the rating widget.</p>
  * @field 
  * @type {number}
  * @option
  */
wijrating_options.prototype.min = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>An option that defines the maximum value that can be rated using the rating widget.</p>
  * @field 
  * @type {number}
  * @option
  */
wijrating_options.prototype.max = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.rating.wijrating_resetButton.html'>wijmo.rating.wijrating_resetButton</a></p>
  * <p>The resetButton option determines the properties of the widget's reset button.</p>
  * @field 
  * @type {wijmo.rating.wijrating_resetButton}
  * @option 
  * @remarks
  * The reset button is used to reset the rated value to 0.
  * If the rating widget is disabled, the reset button will be hidden.
  */
wijrating_options.prototype.resetButton = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.rating.wijrating_hint.html'>wijmo.rating.wijrating_hint</a></p>
  * <p>A value that controls the hint information shown when hovering over the rating star.</p>
  * @field 
  * @type {wijmo.rating.wijrating_hint}
  * @option
  */
wijrating_options.prototype.hint = null;
/** <p class='defaultValue'>Default value: 'horizontal'</p>
  * <p>The orientation option determines the orientation of the rating widget.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Options are "horizontal" and "vertical".
  */
wijrating_options.prototype.orientation = 'horizontal';
/** <p class='defaultValue'>Default value: 'normal'</p>
  * <p>The direction option determines the direction in which items are rated.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Options are "normal" and "reversed". The "normal" represents rating 
  * from left to right or top to bottom.
  */
wijrating_options.prototype.direction = 'normal';
/** <p class='defaultValue'>Default value: 'continuous'</p>
  * <p>The ratingMode option determines how the widget performs the rating function. 
  * The widget can rate things continuously or singly.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Options are "continuous" and "single". The "single" option represents 
  * that only one star can be rated, while "continuous" represents that 
  * all the stars from first to the rated one will be rated.
  */
wijrating_options.prototype.ratingMode = 'continuous';
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.rating.wijrating_icons.html'>wijmo.rating.wijrating_icons</a></p>
  * <p>A value that indicates the settings for customized rating icons.</p>
  * @field 
  * @type {wijmo.rating.wijrating_icons}
  * @option
  */
wijrating_options.prototype.icons = null;
/** <p class='defaultValue'>Default value: 16</p>
  * <p>An option that determines the width of the icon. All icons should have the same width.</p>
  * @field 
  * @type {number}
  * @option
  */
wijrating_options.prototype.iconWidth = 16;
/** <p class='defaultValue'>Default value: 16</p>
  * <p>An option that determines the height of the icon. All icons should have the same height.</p>
  * @field 
  * @type {number}
  * @option
  */
wijrating_options.prototype.iconHeight = 16;
/** <p class='defaultValue'>Default value: null</p>
  * <p>An option that controls aspects of the widget's animation, 
  * such as the animation effect  and easing.</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * animation.animated defines the animation effect for the rating widget.
  * animation.duration defines the length of the animation effect in milliseconds.
  * animation.easing defines the easing effect of an animation.
  * animation.delay defines the length of the delay in milliseconds.
  */
wijrating_options.prototype.animation = null;
/** The rating event fires before widget rating. 
  * You can use this event if you'd like to track the rated values.
  * You can cancel this event by returning false.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.rating.IRatingEventArgs} data Information about an event
  */
wijrating_options.prototype.rating = null;
/** The rated event fires after the widget is rated.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.rating.IRatedEventArgs} data Information about an event
  */
wijrating_options.prototype.rated = null;
/** The reset event fires when the reset button is clicked.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijrating_options.prototype.reset = null;
/** The hover event fires when a user hovers over a rating icon.
  * You can use this event to trigger something, such as an alert,
  * when a user hovers over one of the rating icons.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.rating.IHoverEventArgs} data Information about an event
  */
wijrating_options.prototype.hover = null;
wijmo.rating.wijrating.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijrating_options());
$.widget("wijmo.wijrating", $.wijmo.widget, wijmo.rating.wijrating.prototype);
/** @interface wijrating_resetButton
  * @namespace wijmo.rating
  */
wijmo.rating.wijrating_resetButton = function () {};
/** <p>An option that determines whether to show the reset button.</p>
  * @field 
  * @type {boolean}
  */
wijmo.rating.wijrating_resetButton.prototype.disabled = null;
/** <p>An option that controls the text shown when hovering over the button.</p>
  * @field 
  * @type {string}
  */
wijmo.rating.wijrating_resetButton.prototype.hint = null;
/** <p>The position sub-property defines the resetButton's position in relation to the rating widget.</p>
  * @field 
  * @type {string}
  * @remarks
  * Options are "leftOrTop", "rightOrBottom".
  */
wijmo.rating.wijrating_resetButton.prototype.position = null;
/** <p>The value that controls the customized class added to the reset button.</p>
  * @field 
  * @type {string}
  */
wijmo.rating.wijrating_resetButton.prototype.customizedClass = null;
/** <p>An option that controls the customized class added to the reset button when a user hovers over it.</p>
  * @field 
  * @type {string}
  */
wijmo.rating.wijrating_resetButton.prototype.customizedHoverClass = null;
/** @interface wijrating_hint
  * @namespace wijmo.rating
  */
wijmo.rating.wijrating_hint = function () {};
/** <p>An option that determines whether or not to show the hint.</p>
  * @field 
  * @type {boolean}
  */
wijmo.rating.wijrating_hint.prototype.disabled = null;
/** <p>An option that determines the values that will be shown when a star is hovered over.</p>
  * @field 
  * @type {array}
  * @remarks
  * If the content is null and disabled is false, then the hint will 
  * show the value of each star.
  */
wijmo.rating.wijrating_hint.prototype.content = null;
/** @interface wijrating_icons
  * @namespace wijmo.rating
  */
wijmo.rating.wijrating_icons = function () {};
/** <p>A string or an array value that indicates the urls of icons.</p>
  * @field 
  * @type {string|array}
  * @remarks
  * If the value is a string, then all the star will apply the iconsClass.
  * If the value is an array, then each star will apply the related 
  * iconsClass value by index.
  */
wijmo.rating.wijrating_icons.prototype.iconsClass = null;
/** <p>A string or an array value indicates the urls of hover icons.</p>
  * @field 
  * @type {string|array}
  * @remarks
  * If the value is a string, then all the star will apply the iconsClass when hovered over.
  * If the value is an array, then each star will apply the 
  * related iconsClass value by index when hovered over.
  */
wijmo.rating.wijrating_icons.prototype.hoverIconsClass = null;
/** <p>A string or an array value indicates the urls of rated icons.</p>
  * @field 
  * @type {string|array}
  * @remarks
  * If the value is a string, then all the rated star will apply the iconsClass.
  * If the value is an array, then each rated star will apply the related 
  * iconsClass value by index.
  */
wijmo.rating.wijrating_icons.prototype.ratedIconsClass = null;
/** Contains information about wijrating.rating event
  * @interface IRatingEventArgs
  * @namespace wijmo.rating
  */
wijmo.rating.IRatingEventArgs = function () {};
/** <p>The previously rated value.</p>
  * @field 
  * @type {number}
  */
wijmo.rating.IRatingEventArgs.prototype.oldValue = null;
/** <p>The new value that is to be rated.</p>
  * @field 
  * @type {number}
  */
wijmo.rating.IRatingEventArgs.prototype.newValue = null;
/** <p>The rated object.</p>
  * @field
  */
wijmo.rating.IRatingEventArgs.prototype.target = null;
/** Contains information about wijrating.rated event
  * @interface IRatedEventArgs
  * @namespace wijmo.rating
  */
wijmo.rating.IRatedEventArgs = function () {};
/** <p>The rated value.</p>
  * @field 
  * @type {number}
  */
wijmo.rating.IRatedEventArgs.prototype.value = null;
/** <p>The rated object.</p>
  * @field
  */
wijmo.rating.IRatedEventArgs.prototype.target = null;
/** Contains information about wijrating.hover event
  * @interface IHoverEventArgs
  * @namespace wijmo.rating
  */
wijmo.rating.IHoverEventArgs = function () {};
/** <p>The value of hovered object.</p>
  * @field 
  * @type {number}
  */
wijmo.rating.IHoverEventArgs.prototype.value = null;
/** <p>The hovered object.</p>
  * @field
  */
wijmo.rating.IHoverEventArgs.prototype.target = null;
})()
})()
