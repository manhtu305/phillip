var wijmo = wijmo || {};

(function () {
wijmo.flipcard = wijmo.flipcard || {};

(function () {
/** @class wijflipcard
  * @widget 
  * @namespace jQuery.wijmo.flipcard
  * @extends wijmo.wijmoWidget
  */
wijmo.flipcard.wijflipcard = function () {};
wijmo.flipcard.wijflipcard.prototype = new wijmo.wijmoWidget();
/**  */
wijmo.flipcard.wijflipcard.prototype.destroy = function () {};

/** @class */
var wijflipcard_options = function () {};
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that determines whether or not to disable the wijflipcard widget.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijflipcard_options.prototype.disabled = false;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates the width of the wijflipcard widget.</p>
  * @field 
  * @type {object}
  * @option
  */
wijflipcard_options.prototype.width = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates the height of the wijflipcard widget.</p>
  * @field 
  * @type {object}
  * @option
  */
wijflipcard_options.prototype.height = null;
/** <p class='defaultValue'>Default value: 'click'</p>
  * <p>A value that indicates the event used to flip between two panels.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * The value can be 'click', 'mouseenter'
  */
wijflipcard_options.prototype.triggerEvent = 'click';
/** <p>The flip animation options, define direction, duration etc.</p>
  * @field 
  * @type {object}
  * @option
  */
wijflipcard_options.prototype.animation = null;
/** This event is triggered when flip animation start.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IFlippingEventArgs} data The data with this event.
  */
wijflipcard_options.prototype.flipping = null;
/** This event is triggered when flip animation end.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IFlippedEventArgs} data The data with this event.
  */
wijflipcard_options.prototype.flipped = null;
wijmo.flipcard.wijflipcard.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijflipcard_options());
$.widget("wijmo.wijflipcard", $.wijmo.widget, wijmo.flipcard.wijflipcard.prototype);
/** Provides data for the flipping event of the wijflipcard.
  * @interface IFlippingEventArgs
  * @namespace wijmo.flipcard
  */
wijmo.flipcard.IFlippingEventArgs = function () {};
/** <p>The front panel.</p>
  * @field 
  * @type {JQuery}
  */
wijmo.flipcard.IFlippingEventArgs.prototype.frontPanel = null;
/** <p>The back panel.</p>
  * @field 
  * @type {JQuery}
  */
wijmo.flipcard.IFlippingEventArgs.prototype.backPanel = null;
/** Provides data for the flipped event of the wijflipcard.
  * @interface IFlippedEventArgs
  * @namespace wijmo.flipcard
  */
wijmo.flipcard.IFlippedEventArgs = function () {};
/** <p>The front panel.</p>
  * @field 
  * @type {JQuery}
  */
wijmo.flipcard.IFlippedEventArgs.prototype.frontPanel = null;
/** <p>The back panel.</p>
  * @field 
  * @type {JQuery}
  */
wijmo.flipcard.IFlippedEventArgs.prototype.backPanel = null;
/** <p>Indicates the flipped is flipped or not, if the back panel is showing, the value is true; otherwise, it's false</p>
  * @field 
  * @type {boolean}
  */
wijmo.flipcard.IFlippedEventArgs.prototype.isFlipped = null;
})()
})()
