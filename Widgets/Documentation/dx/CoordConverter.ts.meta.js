var wijmo = wijmo || {};

(function () {
wijmo.maps = wijmo.maps || {};

(function () {
/** The converter to convert the coordinates in geographic unit (longitude and latitude), logic unit(percentage) and screen unit (pixel).
  * @interface ICoordConverter
  * @namespace wijmo.maps
  */
wijmo.maps.ICoordConverter = function () {};
/** Convert the point coordinates from screen unit (pixel) to geographic unit (longitude and latitude).
  * @param {IPoint} position The point to convert, in screen unit.
  * @returns {IPoint} The new point after converted, in geographic unit.
  */
wijmo.maps.ICoordConverter.prototype.viewToGeographic = function (position) {};
/** Convert the point coordinates from geographic unit (longitude and latitude) to screen unit (pixel).
  * @param {IPoint} position The point to convert, in geographic unit.
  * @returns {IPoint} The new point after converted, in screen unit.
  */
wijmo.maps.ICoordConverter.prototype.geographicToView = function (position) {};
/** Convert the point coordinates from screen unit (pixel) to logic unit (percentage).
  * @param {IPoint} position The point to convert, in screen unit.
  * @returns {IPoint} The new point after converted, in logic unit.
  */
wijmo.maps.ICoordConverter.prototype.viewToLogic = function (position) {};
/** Convert the point coordinates from logic unit (percentage) to screen unit (pixel).
  * @param {IPoint} position The point to convert, in loogic unit.
  * @returns {IPoint} The new point after converted, in screen unit.
  */
wijmo.maps.ICoordConverter.prototype.logicToView = function (position) {};
/** Convert the point coordinates from geographic unit (longitude and latitude) to logic unit (percentage).
  * @param {IPoint} position The point to convert, in geographic unit.
  * @returns {IPoint} The new point after converted, in logic unit.
  */
wijmo.maps.ICoordConverter.prototype.geographicToLogic = function (position) {};
/** Convert the point coordinates from logic unit (percentage) to geographic unit (longitude and latitude).
  * @param {IPoint} position The point to convert, in logic unit.
  * @returns {IPoint} The new point after converted, in geographic unit.
  */
wijmo.maps.ICoordConverter.prototype.logicToGeographic = function (position) {};
/** Gets the size of the map viewport.
  * 		@returns {ISize} The size of the map viewport, in screen unit (pixel).
  * @returns {wijmo.maps.ISize}
  */
wijmo.maps.ICoordConverter.prototype.getViewSize = function () {};
/** Gets the size of the map viewport.
  * returns {ISize} The size of the map viewport, in logic unit (percentage).
  * @returns {wijmo.maps.ISize}
  */
wijmo.maps.ICoordConverter.prototype.getLogicSize = function () {};
/** Gets the center of the map viewport.
  * returns {IPoint} The center of the map view port, in screen unit (pixel).
  * @returns {wijmo.maps.IPoint}
  */
wijmo.maps.ICoordConverter.prototype.getViewCenter = function () {};
/** Gets the center of the map viewport.
  * returns {IPoint} The center of the map view port, in logic unit (percentage).
  * @returns {wijmo.maps.IPoint}
  */
wijmo.maps.ICoordConverter.prototype.getLogicCenter = function () {};
/** Gets the size of the full map in current zoom level.
  * @returns {ISize} The size of the full map, in screen unit (pixel).
  */
wijmo.maps.ICoordConverter.prototype.getFullMapSize = function () {};
/** Calculate the distance between two points.
  * @param {IPoint} lonLat1 The coordinate of first point, in geographic unit.
  * @param {IPoint} longLat2 The coordinate of second point, in geographic unit.
  * @returns {number} The distance between to points, in meters.
  */
wijmo.maps.ICoordConverter.prototype.distance = function (lonLat1, longLat2) {};
/** <p>The jQuery object that represents the wijmaps element.</p>
  * @field 
  * @type {JQuery}
  */
wijmo.maps.ICoordConverter.prototype.element = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IPoint.html'>wijmo.maps.IPoint</a></p>
  * <p>The current center of the map, in geographic unit.</p>
  * @field 
  * @type {wijmo.maps.IPoint}
  */
wijmo.maps.ICoordConverter.prototype.center = null;
/** <p>The current zoom of the map.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.ICoordConverter.prototype.zoom = null;
/** <p>The width of the tile, in pixel.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.ICoordConverter.prototype.tileWidth = null;
/** <p>The height of the tile, in pixel.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.ICoordConverter.prototype.tileHeight = null;
})()
})()
