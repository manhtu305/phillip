var wijmo = wijmo || {};

(function () {
wijmo.gauge = wijmo.gauge || {};

(function () {
/** @class wijlineargauge
  * @widget 
  * @namespace jQuery.wijmo.gauge
  * @extends wijmo.gauge.wijgauge
  */
wijmo.gauge.wijlineargauge = function () {};
wijmo.gauge.wijlineargauge.prototype = new wijmo.gauge.wijgauge();
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.gauge.wijlineargauge.prototype.destroy = function () {};

/** @class */
var wijlineargauge_options = function () {};
/** <p class='defaultValue'>Default value: 'horizontal'</p>
  * <p>Sets the orientation of the gauge, with a setting of horizontal showing values across the gauge from left to right, and a
  * setting of vertical showing values along the gauge from top to bottom.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * // This sample shows how to create vertical linear gauge.
  *    $(document).ready(function () {
  *        $("#lineargauge1").wijlineargauge({
  *         value: 31,
  *         orientation: "vertical"
  *     });
  * });
  */
wijlineargauge_options.prototype.orientation = 'horizontal';
/** <p class='defaultValue'>Default value: 0.1</p>
  * <p>Sets the starting location of the X axis as a percentage of the width of the gauge.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Note: By default, the xAxisLength option is set to draw the X axis 80% of the width the gauge, so if you want to center
  * the axis within the gauge, you must also adjust that option.
  */
wijlineargauge_options.prototype.xAxisLocation = 0.1;
/** <p class='defaultValue'>Default value: 0.8</p>
  * <p>Sets the length of the X axis as a percentage of the width of the gauge.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Note: By default, the xAxisLocation option is set to begin the X axis 10% of the way across from the left edge of the
  * gauge, so if you want to use a higher ratio for the xAxisLength, you must also adjust that option.
  */
wijlineargauge_options.prototype.xAxisLength = 0.8;
/** <p class='defaultValue'>Default value: 0.5</p>
  * <p>Sets the base vertical location of the pointer, tick marks and labels on the X axis as a percentage of the height of the gauge.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Note that there is no actual Y axis in the LinearGauge. By default, it is centered in the gauge.A setting of 0.8 moves the
  * pointer, tick marks, and labels toward the bottom edge of the gauge.
  */
wijlineargauge_options.prototype.yAxisLocation = 0.5;
/** <p class='defaultValue'>Default value: 'auto'</p>
  * <p>Sets the width of the gauge area in pixels.</p>
  * @field 
  * @type {number|string}
  * @option
  */
wijlineargauge_options.prototype.width = 'auto';
/** <p class='defaultValue'>Default value: 'auto'</p>
  * <p>Sets the height of the gauge area in pixels.</p>
  * @field 
  * @type {number|string}
  * @option
  */
wijlineargauge_options.prototype.height = 'auto';
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.gauge.gauge_pointer.html'>wijmo.gauge.gauge_pointer</a></p>
  * <p>Creates an object that includes all settings of the gauge pointer.</p>
  * @field 
  * @type {wijmo.gauge.gauge_pointer}
  * @option
  */
wijlineargauge_options.prototype.pointer = null;
/** <p class='defaultValue'>Default value: 5</p>
  * <p>It is a value in pixels that indicates where to render the left edge of the gauge markers, it may help to also change
  * the width option.</p>
  * @field 
  * @type {number}
  * @option
  */
wijlineargauge_options.prototype.marginLeft = 5;
/** <p class='defaultValue'>Default value: 5</p>
  * <p>It is a value in pixels that indicates where to render the top edge of the gauge face.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * In order to change the margin settings by more than a few pixels without clipping the gauge face edges, it may help
  * to also change the height property.
  */
wijlineargauge_options.prototype.marginTop = 5;
/** <p class='defaultValue'>Default value: 5</p>
  * <p>It is a value in pixels that indicates where to render the right edge of the gauge face.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * In order to change the margin settings without compressing the gauge markers, it may help to also change the width option.
  */
wijlineargauge_options.prototype.marginRight = 5;
/** <p class='defaultValue'>Default value: 5</p>
  * <p>It is a value in pixels that indicates where to render the bottom edge of the gauge face.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * In order to change the margin settings by more than a few pixels without clipping the gauge face edges, it may help to
  * also change the height property
  */
wijlineargauge_options.prototype.marginBottom = 5;
wijmo.gauge.wijlineargauge.prototype.options = $.extend({}, true, wijmo.gauge.wijgauge.prototype.options, new wijlineargauge_options());
$.widget("wijmo.wijlineargauge", $.wijmo.wijgauge, wijmo.gauge.wijlineargauge.prototype);
})()
})()
