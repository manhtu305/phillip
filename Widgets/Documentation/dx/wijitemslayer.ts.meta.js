var wijmo = wijmo || {};

(function () {
wijmo.maps = wijmo.maps || {};

(function () {
/** Defines slice info on specific zoom. Used by virtual layer.
  * @interface IMapSlice
  * @namespace wijmo.maps
  */
wijmo.maps.IMapSlice = function () {};
/** <p>The number of latitude divisions for this slice.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.IMapSlice.prototype.latitudeSlices = null;
/** <p>The number of longitude divisions for this slice.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.IMapSlice.prototype.longitudeSlices = null;
/** <p>The minimum zoom of this slice.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.IMapSlice.prototype.zoom = null;
/** @class wijitemslayer
  * @widget 
  * @namespace jQuery.wijmo.maps
  * @extends wijmo.maps.wijlayer
  */
wijmo.maps.wijitemslayer = function () {};
wijmo.maps.wijitemslayer.prototype = new wijmo.maps.wijlayer();
/** Force to request the data to get the newest one. */
wijmo.maps.wijitemslayer.prototype.refresh = function () {};
/** Removes the wijitemslayer functionality completely. This will return the element back to its pre-init state. */
wijmo.maps.wijitemslayer.prototype.destroy = function () {};

/** @class */
var wijitemslayer_options = function () {};
/** The function to be called when request the data.
  * @event 
  * @param {RaphaelPaper} paper The Raphael paper to draw the items.
  * @remarks
  * &lt;p&gt;The items layer request the items to be drawn on the first update. 
  * If want to refresh the items, call refresh() method if necessary.&lt;/p&gt;
  * &lt;p&gt;The items will be arranged to the position based on it's location.&lt;/p&gt;
  */
wijitemslayer_options.prototype.request = null;
wijmo.maps.wijitemslayer.prototype.options = $.extend({}, true, wijmo.maps.wijlayer.prototype.options, new wijitemslayer_options());
$.widget("wijmo.wijitemslayer", $.wijmo.wijlayer, wijmo.maps.wijitemslayer.prototype);
/** Defines the wijmaps items layer.
  * @interface IItemsLayerOptions
  * @namespace wijmo.maps
  * @extends wijmo.maps.ILayerOptions
  */
wijmo.maps.IItemsLayerOptions = function () {};
/** <p>The function to be called when request the data.</p>
  * @field 
  * @type {function}
  * @remarks
  * &lt;p&gt;The items layer request the items to be drawn on the first update. 
  * If want to refresh the items, call refresh() method if necessary.&lt;/p&gt;
  * &lt;p&gt;The items will be arranged to the position based on it's location.&lt;/p&gt;
  */
wijmo.maps.IItemsLayerOptions.prototype.request = null;
/** Defines the object of the request result for the items layer.
  * @interface IItemsLayerRequestResult
  * @namespace wijmo.maps
  */
wijmo.maps.IItemsLayerRequestResult = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IItemsLayerItem.html'>wijmo.maps.IItemsLayerItem[]</a></p>
  * <p>The array of individual item to be drawn.</p>
  * @field 
  * @type {wijmo.maps.IItemsLayerItem[]}
  */
wijmo.maps.IItemsLayerRequestResult.prototype.items = null;
/** Defines the object of the requested individual item for the items layer.
  * @interface IItemsLayerItem
  * @namespace wijmo.maps
  */
wijmo.maps.IItemsLayerItem = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IPoint.html'>wijmo.maps.IPoint</a></p>
  * <p>The location of the item, in geographic unit.</p>
  * @field 
  * @type {wijmo.maps.IPoint}
  */
wijmo.maps.IItemsLayerItem.prototype.location = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IPoint.html'>wijmo.maps.IPoint</a></p>
  * <p>The point within the item to pin the element on the location. The (0,0) is used if not specified.</p>
  * @field 
  * @type {wijmo.maps.IPoint}
  */
wijmo.maps.IItemsLayerItem.prototype.pinpoint = null;
/** <p>The set of Rapahel elements to be drawn.</p>
  * @field 
  * @type {RaphaelSet}
  */
wijmo.maps.IItemsLayerItem.prototype.elements = null;
/** The options of the wijitemslayer.
  * @class wijitemslayer_options
  * @namespace wijmo.maps
  * @extends wijmo.maps.wijlayer_options
  */
wijmo.maps.wijitemslayer_options = function () {};
wijmo.maps.wijitemslayer_options.prototype = new wijmo.maps.wijlayer_options();
typeof wijmo.maps.ILayerOptions != 'undefined' && $.extend(wijmo.maps.IItemsLayerOptions.prototype, wijmo.maps.ILayerOptions.prototype);
})()
})()
