var wijmo = wijmo || {};

(function () {
wijmo.exporter = wijmo.exporter || {};

(function () {
/** The setting of grid export.
  * @interface GridExportSetting
  * @namespace wijmo.exporter
  * @extends wijmo.exporter.ExportSetting
  */
wijmo.exporter.GridExportSetting = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.exporter.GridPdfSetting.html'>wijmo.exporter.GridPdfSetting</a></p>
  * <p>The setting of grid pdf export.</p>
  * @field 
  * @type {wijmo.exporter.GridPdfSetting}
  */
wijmo.exporter.GridExportSetting.prototype.pdf = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.exporter.ExcelSetting.html'>wijmo.exporter.ExcelSetting</a></p>
  * <p>The setting of grid excel export.</p>
  * @field 
  * @type {wijmo.exporter.ExcelSetting}
  */
wijmo.exporter.GridExportSetting.prototype.excel = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.grid.wijgrid.html'>wijmo.grid.wijgrid</a></p>
  * <p>The wijgrid that need to export.</p>
  * @field 
  * @type {wijmo.grid.wijgrid}
  */
wijmo.exporter.GridExportSetting.prototype.grid = null;
/** <p>A boolean value indicates whether only export the data in current page 
  * 		when grid uses the paging. If not set, it works as true.</p>
  * @field 
  * @type {boolean}
  */
wijmo.exporter.GridExportSetting.prototype.onlyCurrentPage = null;
/** <p>The row count when paging and exporting all pages.</p>
  * @field 
  * @type {number}
  */
wijmo.exporter.GridExportSetting.prototype.rowCountPerRequest = null;
/** The setting of grid pdf export.
  * @interface GridPdfSetting
  * @namespace wijmo.exporter
  * @extends wijmo.exporter.PdfSetting
  */
wijmo.exporter.GridPdfSetting = function () {};
/** <p>A boolean value indicates whether repeat the grid header in each page. Used only for grid widget.</p>
  * @field 
  * @type {boolean}
  */
wijmo.exporter.GridPdfSetting.prototype.repeatHeader = null;
/** The setting of grid excel export.
  * @interface ExcelSetting
  * @namespace wijmo.exporter
  */
wijmo.exporter.ExcelSetting = function () {};
/** <p>A boolean value indicates whether row’s height is auto adjusted per the content.</p>
  * @field 
  * @type {boolean}
  */
wijmo.exporter.ExcelSetting.prototype.autoRowHeight = null;
/** <p>Specifies the name of the person, company, or application that created this excel file.</p>
  * @field 
  * @type {string}
  */
wijmo.exporter.ExcelSetting.prototype.author = null;
/** <p>A boolean value whether to show the gridlines in the sheet.</p>
  * @field 
  * @type {boolean}
  */
wijmo.exporter.ExcelSetting.prototype.showGridLines = null;
typeof wijmo.exporter.ExportSetting != 'undefined' && $.extend(wijmo.exporter.GridExportSetting.prototype, wijmo.exporter.ExportSetting.prototype);
typeof wijmo.exporter.PdfSetting != 'undefined' && $.extend(wijmo.exporter.GridPdfSetting.prototype, wijmo.exporter.PdfSetting.prototype);
})()
})()
