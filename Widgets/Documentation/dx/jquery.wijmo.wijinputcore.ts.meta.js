var wijmo = wijmo || {};

(function () {
wijmo.input = wijmo.input || {};

(function () {
/** @class wijinputcore
  * @widget 
  * @namespace jQuery.wijmo.input
  * @extends wijmo.wijmoWidget
  */
wijmo.input.wijinputcore = function () {};
wijmo.input.wijinputcore.prototype = new wijmo.wijmoWidget();
/** Destroy the widget. */
wijmo.input.wijinputcore.prototype.destroy = function () {};
/** Open the dropdown list. */
wijmo.input.wijinputcore.prototype.drop = function () {};
/** Get a boolean value indicates that whether the widget has been destroyed.
  * @returns {bool}
  */
wijmo.input.wijinputcore.prototype.isDestroyed = function () {};
/** Gets element this widget is associated.
  * @returns {JQuery}
  */
wijmo.input.wijinputcore.prototype.widget = function () {};
/** Gets the text displayed in the input box.
  * @returns {string}
  */
wijmo.input.wijinputcore.prototype.getText = function () {};
/** Sets the text displayed in the input box.
  * @param value
  * @example
  * // This example sets text of a wijinputcore to "Hello"
  * $(".selector").wijinputcore("setText", "Hello");
  */
wijmo.input.wijinputcore.prototype.setText = function (value) {};
/** Gets the text value when the container form is posted back to server. */
wijmo.input.wijinputcore.prototype.getPostValue = function () {};
/** Selects a range of text in the widget.
  * @param {number} start Start of the range.
  * @param {number} end End of the range.
  * @example
  * // Select first two symbols in a wijinputcore
  * $(".selector").wijinputdate("selectText", 0, 2);
  */
wijmo.input.wijinputcore.prototype.selectText = function (start, end) {};
/** Set the focus to the widget. */
wijmo.input.wijinputcore.prototype.focus = function () {};
/** Determines whether the widget has the focus.
  * @returns {bool}
  */
wijmo.input.wijinputcore.prototype.isFocused = function () {};
/** Gets the selected text. */
wijmo.input.wijinputcore.prototype.getSelectedText = function () {};

/** @class */
var wijinputcore_options = function () {};
/** <p>undefined</p>
  * @field 
  * @option
  */
wijinputcore_options.prototype.wijCSS = null;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the input method setting of widget.
  * Possible values are: 'auto', 'active', 'inactive', 'disabled'</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * This property only take effect on IE and firefox browser.
  */
wijinputcore_options.prototype.imeMode = "";
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the culture used to show values in the wijinput widget.</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputcore_options.prototype.culture = "";
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Assigns the string value of the culture calendar that appears on the calendar.
  *   This option must work with culture option.</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputcore_options.prototype.cultureCalendar = "";
/** <p>The CSS class applied to the widget when an invalid value is entered.</p>
  * @field 
  * @option 
  * @remarks
  * For some property of the css, such as the color, because wijmo has set default style,
  * and it may be has a higher priority, so custom need to user a higher priority than the defualt.
  * @example
  * // This example sets the invalidClass option to "invalid".
  * .wijmo-wijinput.invalid {
  * color: red !important;
  * background-color: green !important;
  * font-size: xx-large;
  * } 
  * $(".selector").wijinputcore("option", "invalidClass" "invalid");
  */
wijinputcore_options.prototype.invalidClass = null;
/** <p>Determines the text displayed when the widget is blank and contains no initial text.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * when the option's value is empty, the empty value will display, when the value is null, then the placeholder will not show.
  */
wijinputcore_options.prototype.placeholder = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>If true, then the browser response is disabled when the ENTER key is pressed.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijinputcore_options.prototype.hideEnter = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether a user can enter a value in the wijinput widget. 
  * If readonly is true, user can't input value to the wijinput widget by ui operation, such as spin, pick value from pickers.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijinputcore_options.prototype.readonly = false;
/** <p class='defaultValue'>Default value: 'right'</p>
  * <p>Determines the side, left or right, where the dropdown button appear.
  * Possible values are: 'left', 'right'</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputcore_options.prototype.dropDownButtonAlign = 'right';
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether dropdown button is displayed.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijinputcore_options.prototype.showDropDownButton = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether spinner button is displayed.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijinputcore_options.prototype.showSpinner = false;
/** <p class='defaultValue'>Default value: 'none'</p>
  * <p>Determines whether the focus automatically moves to the next or previous
  * tab ordering control when pressing the left, right arrow keys.
  * Possible values are "none", "left", "right", "both". 
  * The default value is "none".</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputcore_options.prototype.blurOnLeftRightKey = 'none';
/** <p class='defaultValue'>Default value: 'verticalRight'</p>
  * <p>Determines the side, left or right, where the spinner button appear.
  * Possible values are: 'vertialLeft', 'verticalRight', 'horizontalDownLeft', 'horizontalUpLeft'.
  * The default value is 'verticalRight'.</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputcore_options.prototype.spinnerAlign = 'verticalRight';
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether the spin behavior can wrap when reaching a maximum or minimum limit.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijinputcore_options.prototype.allowSpinLoop = false;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.input.PickerClass.html'>wijmo.input.PickerClass</a></p>
  * <p>An object contains the settings for the dropdown list.</p>
  * @field 
  * @type {wijmo.input.PickerClass}
  * @option 
  * @example
  * $(".selector").wijinputmask({
  *      pickers: {
  *          list: [
  *              { label: 'item1', value: 1 },
  *              { label: 'item2', value: 2 }
  *          ],
  *          width: 100,
  *          height: 130
  *      }
  *  });
  */
wijinputcore_options.prototype.pickers = null;
/** The dropdownOpen event handler. 
  * A function called before the widget's dropdown opened.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijinputcore_options.prototype.dropDownOpen = null;
/** The dropdownClose event handler. 
  * A function called before the widget's dropdown closed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijinputcore_options.prototype.dropDownClose = null;
/** The initializing event handler. 
  * A function called before the widget is initialized.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijinputcore_options.prototype.initializing = null;
/** The initialized event handler. 
  * A function called after the widget is initialized.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijinputcore_options.prototype.initialized = null;
/** The dropdownButtonMouseDown event handler. A function called 
  * when the mouse is pressed down on the dropdown button.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijinputcore_options.prototype.dropDownButtonMouseDown = null;
/** The dropdownButtonMouseUp event handler. A function called 
  * when the mouse is released on the dropdown button.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijinputcore_options.prototype.dropDownButtonMouseUp = null;
/** Fired when the widget text is changed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.input.ITextChangedEventArgs} data Information about an event
  */
wijinputcore_options.prototype.textChanged = null;
/** The invalidInput event handler. A function called 
  * when invalid charactor is typed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.input.IInvalidInputEventArgs} data Information about an event
  */
wijinputcore_options.prototype.invalidInput = null;
/** Fired when the widget lost focus and caused by the keyboard behavior.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijinputcore_options.prototype.keyExit = null;
wijmo.input.wijinputcore.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijinputcore_options());
$.widget("wijmo.wijinputcore", $.wijmo.widget, wijmo.input.wijinputcore.prototype);
/** @interface PickerClass
  * @namespace wijmo.input
  */
wijmo.input.PickerClass = function () {};
/** <p>Determines the width of the dropdown window.</p>
  * @field 
  * @type {number}
  * @remarks
  * If the value isn't set, the width will be auto, depends on the contents of the pickers.
  */
wijmo.input.PickerClass.prototype.width = null;
/** <p>Determines the height of the dropdown window.</p>
  * @field 
  * @type {number}
  * @remarks
  * If the value isn't set, the height will be auto, depends on the contents of the pickers.
  */
wijmo.input.PickerClass.prototype.height = null;
/** <p>Contains an array of data items used to populate the dropdown list.</p>
  * @field 
  * @remarks
  * The array item can be "string", or a an oject contains two property "label" and "value";
  * The label property is string, used to display on the dropdown list.
  * The value proeprty is string object, used to assign the value to the input widget.
  */
wijmo.input.PickerClass.prototype.list = null;
/** @enum MouseButton
  * @namespace wijmo.input
  */
wijmo.input.MouseButton = {
/**  */
Default: 0,
/**  */
Left: 1,
/**  */
Middle: 2,
/**  */
Right: 3 
};
/** Contains information about wijinputcore.textChanged event
  * @interface ITextChangedEventArgs
  * @namespace wijmo.input
  */
wijmo.input.ITextChangedEventArgs = function () {};
/** <p>The new text.</p>
  * @field 
  * @type {string}
  */
wijmo.input.ITextChangedEventArgs.prototype.text = null;
/** Contains information about wijinputcore.invalidInput event
  * @interface IInvalidInputEventArgs
  * @namespace wijmo.input
  */
wijmo.input.IInvalidInputEventArgs = function () {};
/** <p>The newly input character.</p>
  * @field 
  * @type {string}
  */
wijmo.input.IInvalidInputEventArgs.prototype.char = null;
/** <p>The widget object itself.</p>
  * @field
  */
wijmo.input.IInvalidInputEventArgs.prototype.widget = null;
})()
})()
