var wijmo = wijmo || {};

(function () {
wijmo.menu = wijmo.menu || {};

(function () {
/** @class wijmenu
  * @widget 
  * @namespace jQuery.wijmo.menu
  * @extends wijmo.wijmoWidget
  */
wijmo.menu.wijmenu = function () {};
wijmo.menu.wijmenu.prototype = new wijmo.wijmoWidget();
/** The destroy method removes the wijmenu functionality completely
  * and returns the element to its pre-init state.
  */
wijmo.menu.wijmenu.prototype.destroy = function () {};
/** This method activates a menu item by deactivating the current item,
  * scrolling the new item into view, and, if necessary,making it the active item,
  * and triggering a focus event.
  * @param {event} event The javascript event.
  * @param item A menu item to activate.
  * @example
  * //Actives a menu item with "sub-item" class.
  * $(".selector").wijmenu("activate", null, $(".sub-item"));
  */
wijmo.menu.wijmenu.prototype.activate = function (event, item) {};
/** The deactivate method clears the current selection. 
  * This method is useful when reopening a menu which you previously selected an item.
  * If you don't call this method then an item which you selected before allowing the menu to close 
  * will remain highlighted when the menu is reopened.
  * @param {event} event The javascript event.
  */
wijmo.menu.wijmenu.prototype.deactivate = function (event) {};
/** The next method gets the next selectable item. 
  * The first item is selected if no item is active or the last one is active. 
  * It returns null if none is selectable.
  * @param {event} event The javascript event.
  */
wijmo.menu.wijmenu.prototype.next = function (event) {};
/** Get the previous selectable item. 
  * It selects the last item if no item is active or if the first item is active. 
  * It returns null if no previous item is selectable.
  * @param {event} event The javascript event.
  */
wijmo.menu.wijmenu.prototype.previous = function (event) {};
/** The first method defines the first menu item as the active item.
  * @returns {bool}
  */
wijmo.menu.wijmenu.prototype.first = function () {};
/** The last method defines the last menu item as the active item.
  * @returns {bool}
  */
wijmo.menu.wijmenu.prototype.last = function () {};
/** This method is similar to the "next" method, 
  * but it jumps a whole page to the next page instead of to the next selectable item.
  * You can call this method when you are using an iPod-style menu.
  * @param {event} event The javascript event.
  */
wijmo.menu.wijmenu.prototype.nextPage = function (event) {};
/** This method is similar to the "previous" method, 
  * but it jumps a whole page to the previous page.
  * You can call this method when you're using an iPod-style menu.
  * @param {event} event The javascript event.
  */
wijmo.menu.wijmenu.prototype.previousPage = function (event) {};
/** This method selects the active item which triggers the select event for that item.
  * This event is useful for custom keyboard handling.
  * @param {event} event The javascript event.
  */
wijmo.menu.wijmenu.prototype.select = function (event) {};
/** The setItemDisabled method allows the user to disable a specific menu item.
  * @param selector Indicates the item to be disabled. The parameter's type is jQuery selector.
  * @param {bool} disabled If the value for this parameter is true,  then the menu item will be disabled. The parameter's type is Boolean.
  * @example
  * //Disables a menuitem with "sub-item" class.
  * $(".selector").wijmenu("setItemDisabled", $(".sub-item"), true);
  */
wijmo.menu.wijmenu.prototype.setItemDisabled = function (selector, disabled) {};
/** The method is used to refresh the menu when DOM operations add or replace a menu item. 
  * For example, if you add a new menu item through "menu.append," 
  * then you can use the refresh method to make sure that the new menu item appears in the menu.
  * @example
  * //Adds a new item and refresh menu.
  * menu.append("&lt;li&gt;&lt;a href='#'&gt;new item&lt;/a&gt;&lt;/li&gt;").wijmenu("refresh");
  */
wijmo.menu.wijmenu.prototype.refresh = function () {};
/** The hideAllMenus method hides all menu items currently showing.
  * @param e
  */
wijmo.menu.wijmenu.prototype.hideAllMenus = function (e) {};
/** Adds a child menuItem to the menuItem.
  * @param menuitem The menuItem to be added
  * 1.markup html.such as "&lt;a&gt;menuItem&lt;/a&gt;" as a menuItem.
  * 2.object options according to the options of wijmenuItem.
  * @param {number} position the position to insert at
  */
wijmo.menu.wijmenu.prototype.add = function (menuitem, position) {};
/** Gets the collection of child items.
  * @returns {array} the menu items.
  */
wijmo.menu.wijmenu.prototype.getItems = function () {};
/** Remove an item from the menu.
  * @param {number} index The index of menuitem to be removed
  * @example
  * //Removes the second menuitem.
  * $("#menu").wijmenu("remove", 1);
  */
wijmo.menu.wijmenu.prototype.remove = function (index) {};

/** @class */
var wijmenu_options = function () {};
/** <p class='defaultValue'>Default value: ""</p>
  * <p>The trigger option handles the open event for the menu or submenu. 
  * This option must be used with the triggerEvent option.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * If the trigger is set to a menu item(the &lt;li&gt; element),
  * then the submenu appears when the item is clicked if the triggerEvent 
  * is set to click. If the trigger is set to an element outside of the menu, 
  * then the menu opens when the element is clicked if the triggerEvent is 
  * set to click as a contextmenu.
  * @example
  * //Set trigger to make menu as input's contextmenu.
  * $(".selector").wijmenu("option", "trigger", "input")
  */
wijmenu_options.prototype.trigger = "";
/** <p class='defaultValue'>Default value: 'click'</p>
  * <p>The triggerEvent option specifies the mouse event used to show the menu or submenu.
  * triggerEvent must be used with the trigger option.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * The value can be seted to 'click', 'mouseenter', 'dbclick', 'rtclick'
  */
wijmenu_options.prototype.triggerEvent = 'click';
/** <p class='defaultValue'>Default value: 'default'</p>
  * <p>The submenuTriggerEvent option specifies the mouse event used to show the submenu.
  * If submenuTriggerEvent is 'default', it will get value from triggerEvent option.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * The value can be seted to 'default', 'click', 'mouseenter', 'dbclick', 'rtclick'.
  */
wijmenu_options.prototype.submenuTriggerEvent = 'default';
/** <p>The position option specifies the location and orientation of the menu relative to the button
  * or link used to open it.</p>
  * @field 
  * @option 
  * @remarks
  * Configuration for the Position Utility,Of option
  * is excluded, it is always configured by the widget.
  * Collision also controls collision detection automatically too.
  * It uses jQuery position plugin, see the following link for more details,
  * http://api.jqueryui.com/position/
  */
wijmenu_options.prototype.position = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.menu.wijmenu_animation.html'>wijmo.menu.wijmenu_animation</a></p>
  * <p>The animation option determines the menu animation if the showAnimation
  * and hideAnimation properties are not individually specified.
  * For a description of the animation effects and the easing effects, please see the Animation page.</p>
  * @field 
  * @type {wijmo.menu.wijmenu_animation}
  * @option 
  * @remarks
  * This option uses the standard animation setting syntax 
  * from jQuery.UI.
  */
wijmenu_options.prototype.animation = null;
/** <p>This option determines the animation effect, the duration, and the easing effect used to hide the submenus. 
  * You can also use this option to set any animation-specific options, such as direction.</p>
  * @field 
  * @option 
  * @remarks
  * This option uses the standard animation setting syntax from jQuery.UI.
  */
wijmenu_options.prototype.showAnimation = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.menu.wijmenu_animation.html'>wijmo.menu.wijmenu_animation</a></p>
  * <p>Determines the animation used to hide submenus.</p>
  * @field 
  * @type {wijmo.menu.wijmenu_animation}
  * @option 
  * @remarks
  * This option uses the standard animation setting syntax 
  * from jQuery.UI.
  */
wijmenu_options.prototype.hideAnimation = null;
/** <p class='defaultValue'>Default value: 400</p>
  * <p>This option determines how many milliseconds to delay 
  * before showing the submenu in a fly-out menu.</p>
  * @field 
  * @type {number}
  * @option
  */
wijmenu_options.prototype.showDelay = 400;
/** <p class='defaultValue'>Default value: 400</p>
  * <p>This option determines how many milliseconds to delay 
  * before hiding the submenu in a fly-out menu.</p>
  * @field 
  * @type {number}
  * @option
  */
wijmenu_options.prototype.hideDelay = 400;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.menu.wijmenu_slidingAnimation.html'>wijmo.menu.wijmenu_slidingAnimation</a></p>
  * <p>This option defines the animation used to slide a submenu when in sliding mode.</p>
  * @field 
  * @type {wijmo.menu.wijmenu_slidingAnimation}
  * @option
  */
wijmenu_options.prototype.slidingAnimation = null;
/** <p class='defaultValue'>Default value: 'flyout'</p>
  * <p>The mode option defines the behavior of the menu, 
  * whether it is a pop-up menu or an iPod-style navigation list.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * The possible values are "flyout" or "sliding".
  * If you want to show your submenu in the same container by sliding animation as in an iPod-style menu, 
  * use the "sliding" mode.
  */
wijmenu_options.prototype.mode = 'flyout';
/** <p class='defaultValue'>Default value: null</p>
  * <p>This option specifies a hash value that sets to superpanel options 
  * when a superpanel is created.  It is used to set appearances and behaviors
  * when a scrollbar is shown in sliding mode.</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * It uses wijsuperpanel, see the following link for more details,
  * http://wijmo.com/wiki/index.php/Superpanel .
  */
wijmenu_options.prototype.superPanelOptions = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>When you set the checkable option to true and click on a menu item, 
  * the clicked item's state will be selected. 
  * A "ui-state-active" class will be added to the item so that the item behavior and the item appearance are different.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijmenu_options.prototype.checkable = false;
/** <p class='defaultValue'>Default value: 'horizontal'</p>
  * <p>This option controls the root menu's orientation. All submenus are vertical 
  * regardless of the orientation of the root menu.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Possible values are "horizontal" or "vertical". 
  * You can see the horizontal menu in theMarkup and Scripting section.
  */
wijmenu_options.prototype.orientation = 'horizontal';
/** <p class='defaultValue'>Default value: 'ltr'</p>
  * <p>A value that indicates menu's direction.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * The value should be "ltr" or "rtl".
  */
wijmenu_options.prototype.direction = 'ltr';
/** <p class='defaultValue'>Default value: 200</p>
  * <p>This option determines the iPod-style menu's maximum height.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * This option can only be used in an iPod-style menu.
  * When the menu contains more items than can be displayed within the allowed height, 
  * the menu will show a scrollbar.
  */
wijmenu_options.prototype.maxHeight = 200;
/** <p class='defaultValue'>Default value: true</p>
  * <p>This option determines whether the iPod-style menu shows a back link or a breadcrumb header
  * in the menu.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijmenu_options.prototype.backLink = true;
/** <p class='defaultValue'>Default value: 'Back'</p>
  * <p>This option sets the text of the back link.</p>
  * @field 
  * @type {string}
  * @option
  */
wijmenu_options.prototype.backLinkText = 'Back';
/** <p class='defaultValue'>Default value: 'All'</p>
  * <p>A value that sets the text of the top link.</p>
  * @field 
  * @type {string}
  * @option
  */
wijmenu_options.prototype.topLinkText = 'All';
/** <p class='defaultValue'>Default value: 'Choose an option'</p>
  * <p>A value that sets the top breadcrumb's default text.</p>
  * @field 
  * @type {string}
  * @option
  */
wijmenu_options.prototype.crumbDefaultText = 'Choose an option';
/** <p class='defaultValue'>Default value: null</p>
  * <p>The options of child items</p>
  * @field 
  * @type {array}
  * @option
  */
wijmenu_options.prototype.items = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value indicating the submenu element will be append to the body or menu container.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * If the value is true, the submenu will be appended to body element.
  * else it will append to the menu container.
  */
wijmenu_options.prototype.ensureSubmenuOnBody = false;
/** The select event is triggered when a menu item is selected.
  * This event can be triggered by theselect method to allow for custom keyboard handling.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.menu.ISelectEventArgs} data Information about an event
  */
wijmenu_options.prototype.select = null;
/** The focus event is triggered either on mouse hover or 
  * when the keyboard cursor keys are used for navigation.
  * In general, an item or element that has focus is highlighted in some way. 
  * When an item or element has focus it is the first to receive keyboard-related events.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.menu.IFocusEventArgs} data Information about an event
  */
wijmenu_options.prototype.focus = null;
/** The blur event is triggered when a menu item loses focus. 
  * You can use this event to track which item has lost focus. 
  * An item can lose focus through keyboard commands, such as when the Tab key is pressed, 
  * or when a user clicks elsewhere on the page.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.menu.IBlurEventArgs} data Information about an event
  */
wijmenu_options.prototype.blur = null;
/** The showing event is triggered before the submenu is shown. 
  * showing is a useful event if you want to perform a function before the submenu is shown 
  * or if you want to block the submenu from showing by returning false.
  * @event 
  * @param event This is the jQuery.Event object.
  * @param item This is the event object that relates to the submenu's parent item.
  */
wijmenu_options.prototype.showing = null;
/** The shown event is triggered after the menu or submenu is shown.
  * @event 
  * @param event This is the jQuery.Event object.
  * @param item The wijmenu widget object when the menu has been displayed, 
  * or the wijmenuitem widget when the submenu has been displayed.
  */
wijmenu_options.prototype.shown = null;
/** The hidding event is triggered before hidding the menu or submenu.
  * This event can be cancelled with "return false";
  * @event 
  * @param event This is the jQuery.Event object.
  * @param item This is the wijmenu widget object when hiding the menu 
  * or a wijmenuitem when hiding the submenu.
  */
wijmenu_options.prototype.hidding = null;
/** The hidden event is triggered after the menu or submenu is hidden.
  * @event 
  * @param event This is the jQuery.Event object.
  * @param item The wijmenu widget object when a menu is hidden 
  * or a wijmenuitem object when the submenu is hidden.
  */
wijmenu_options.prototype.hidden = null;
wijmo.menu.wijmenu.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijmenu_options());
$.widget("wijmo.wijmenu", $.wijmo.widget, wijmo.menu.wijmenu.prototype);
/** @interface wijmenu_animation
  * @namespace wijmo.menu
  */
wijmo.menu.wijmenu_animation = function () {};
/** <p>The animation effect you apply to the menu. 
  * This animation effect will control how the submenu is shown. 
  * Please see the Animation Effects topic for a list of the available animation effects.</p>
  * @field 
  * @type {string}
  */
wijmo.menu.wijmenu_animation.prototype.animated = null;
/** <p>The duration of the animation in milliseconds.</p>
  * @field
  */
wijmo.menu.wijmenu_animation.prototype.duration = null;
/** <p>A value that indicates the easing function that will be applied to the animation. 
  * For more information, please see the Easing topic.</p>
  * @field 
  * @type {string}
  */
wijmo.menu.wijmenu_animation.prototype.easing = null;
/** <p>A value that indicates animation-specific settings.</p>
  * @field 
  * @example
  * //Set animation direction.
  * $(".selector").wijmenu("option", "animation", {
  *     animated:"slide", 
  *     option: { direction: "right" }, 
  *     duration: 400, 
  *     easing: null
  * });
  */
wijmo.menu.wijmenu_animation.prototype.option = null;
/** @interface wijmenu_slidingAnimation
  * @namespace wijmo.menu
  */
wijmo.menu.wijmenu_slidingAnimation = function () {};
/** <p>The duration of the animation in milliseconds.</p>
  * @field
  */
wijmo.menu.wijmenu_slidingAnimation.prototype.duration = null;
/** <p>A value that indicates the easing function that will be applied to the animation. 
  * For more information, please see the Easing topic.</p>
  * @field 
  * @type {string}
  */
wijmo.menu.wijmenu_slidingAnimation.prototype.easing = null;
/** Contains information about wijmenu.select event
  * @interface ISelectEventArgs
  * @namespace wijmo.menu
  */
wijmo.menu.ISelectEventArgs = function () {};
/** <p>item.item is the active item of the menu.</p>
  * @field
  */
wijmo.menu.ISelectEventArgs.prototype.item = null;
/** Contains information about wijmenu.focus event
  * @interface IFocusEventArgs
  * @namespace wijmo.menu
  */
wijmo.menu.IFocusEventArgs = function () {};
/** <p>item.item is the item which is focused.</p>
  * @field
  */
wijmo.menu.IFocusEventArgs.prototype.item = null;
/** Contains information about wijmenu.blur event
  * @interface IBlurEventArgs
  * @namespace wijmo.menu
  */
wijmo.menu.IBlurEventArgs = function () {};
/** <p>item.item is the item which is blured.</p>
  * @field
  */
wijmo.menu.IBlurEventArgs.prototype.item = null;
})()
})()
