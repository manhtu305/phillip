var wijmo = wijmo || {};

(function () {
wijmo.evcal = wijmo.evcal || {};

(function () {
/** @class wijevcal
  * @widget 
  * @namespace jQuery.wijmo.evcal
  * @extends wijmo.wijmoWidget
  */
wijmo.evcal.wijevcal = function () {};
wijmo.evcal.wijevcal.prototype = new wijmo.wijmoWidget();
/** Get the localized string by key
  * @param {string} key The key of the localized string.
  * @param {string} defaultValue The default value of the localized string.
  * @returns {string} The localized string.
  */
wijmo.evcal.wijevcal.prototype.localizeString = function (key, defaultValue) {};
/** hide the loading label */
wijmo.evcal.wijevcal.prototype.hideLoadingLabel = function () {};
/** Removes the wijevcal functionality completely. This returns the element to its pre-init state. */
wijmo.evcal.wijevcal.prototype.destroy = function () {};
/** Exports the EventsCalendar in a graphic format. 
  * The export method only works when wijmo.exporter.eventscalendarExport's reference is on the page.
  * @param {string|Object} exportSettings 1.The name of the exported file.
  * 2.Settings of exporting, should be conformed to wijmo.exporter.IEventsCalendarExportSettings
  * @param {?string} type The type of the exported file.
  * @param {?string} serviceUrl The export service url.
  * @param {?string} pdfSettings The setting of pdf.
  * @param {?string} exportMethod with different mode,
  * 1. "Content" Sending EventsCalendar markup to the service for exporting.
  * 2. "Options" Sending EventsCalendar widget options to the service for exporting.
  * @remarks
  * Default exported file type is png, possible types are: jpg, png, gif, bmp, tiff, pdf.
  * @example
  * $("#evcal").wijevcal("exportEventsCalendar", "eventscalendar", "png");
  */
wijmo.evcal.wijevcal.prototype.exportEventsCalendar = function (exportSettings, type, serviceUrl, pdfSettings, exportMethod) {};
/** Deletes the existing calendar from the current data source.
  * @param {object} o Calendar id, name or calendar object.
  * @param {function} successCallback Function that will be called when calendar is deleted.
  * @param {function} errorCallback Function that will be called when calendar can not be deleted.(e.g. due to data source or memory problems).
  * @returns {bool}
  * @example
  * $("#wijevcal").wijevcal("deleteCalendar", "My calendar");
  */
wijmo.evcal.wijevcal.prototype.deleteCalendar = function (o, successCallback, errorCallback) {};
/** Adds a new calendar.
  * @param {object} o Calendar object.
  * Calendar object fields:
  * id - String, unique calendar id, this field generated automatically;
  * name - String, calendar name;
  * location - String, location field;
  * description - String, calendar description;
  * color - String, calendar color;
  * tag - String, this field can be used to store custom information.
  * @param {function} successCallback Function that will be called when calendar is added.
  * @param {function} errorCallback Function that will be called when calendar can not be added.(e.g. due to data source or memory problems).
  * @returns {bool}
  * @example
  * $("#wijevcal").wijevcal("addCalendar", { 
  * name: "My calendar", 
  * location: "Home", 
  * description: "Some description",
  * color: "lime" 
  * });
  */
wijmo.evcal.wijevcal.prototype.addCalendar = function (o, successCallback, errorCallback) {};
/** Updates the existing calendar.
  * @param {object} o Calendar object.
  * Calendar object fields:
  * id - String, unique calendar id, this field generated automatically;
  * name - String, calendar name;
  * location - String, location field;
  * description - String, calendar description;
  * color - String, calendar color;
  * tag - String, this field can be used to store custom information.
  * @param {function} successCallback Function that will be called when calendar is updated.
  * @param {function} errorCallback Function that will be called when calendar can not be updated.(e.g. due to data source or memory problems).
  * @returns {bool}
  * @example
  * $("#wijevcal").wijevcal("updateCalendar", { 
  * name: "My calendar", 
  * location: "Home", 
  * description: "Some description",
  * color: "lime" 
  * });
  */
wijmo.evcal.wijevcal.prototype.updateCalendar = function (o, successCallback, errorCallback) {};
/** Adds a new event.
  * @param {object} o Event object.
  * Event object fields:
  * id - String, unique event id, this field generated automatically;
  * calendar - String, calendar id to which the event belongs;
  * subject - String, event title;
  * location - String, event location;
  * start - Date, start date/time;
  * end - Date, end date/time;
  * description - String, event description;
  * color - String, event color;
  * allday - Boolean, indicates all day event
  * tag - String, this field can be used to store custom information.
  * @param {function} successCallback Function that will be called when event is added.
  * @param {function} errorCallback Function that will be called when event can not be added.(e.g. due to data source or memory problems).
  * @returns {bool}
  * @example
  * $("#wijevcal").wijevcal("addEvent", { 
  *       start: new Date(2011, 4, 2, 0, 32), 
  *       end: new Date(2011, 4, 2, 0, 50), 
  *       subject: "Subject" });
  */
wijmo.evcal.wijevcal.prototype.addEvent = function (o, successCallback, errorCallback) {};
/** Updates the existing event.
  * @param {object} o Event object.
  * Event object fields:
  * id - String, unique event id, this field generated automatically;
  * calendar - String, calendar id to which the event belongs;
  * subject - String, event title;
  * location - String, event location;
  * start - Date, start date/time;
  * end - Date, end date/time;
  * description - String, event description;
  * color - String, event color;
  * allday - Boolean, indicates all day event
  * tag - String, this field can be used to store custom information.
  * @param {function} successCallback Function that will be called when event is updated.
  * @param {function} errorCallback Function that will be called when event can not be updated.(e.g. due to data source or memory problems).
  * @returns {bool}
  * @example
  * $("#wijevcal").wijevcal("updateEvent", { 
  *       start: new Date(2011, 4, 2, 0, 32), 
  *       end: new Date(2011, 4, 2, 0, 50), 
  *       subject: "Subject" });
  */
wijmo.evcal.wijevcal.prototype.updateEvent = function (o, successCallback, errorCallback) {};
/** Retrieves the array which contains 
  * the full list of Event objects in the specified time interval. 
  * Note, this method will create instances of the Event
  * object for recurring events.
  * @param {Date} start The Date value which specifies 
  * the start date and time of the interval.
  * @param {Date} end The Date value which specifies 
  * the end date and time of the interval.
  * @returns {array} events array
  */
wijmo.evcal.wijevcal.prototype.getOccurrences = function (start, end) {};
/** Deletes the event.
  * @param {number} id Event object or event id.
  * @param {function} successCallback Function that will be called when event is deleted.
  * @param {function} errorCallback Function that will be called when event can not be deleted.(e.g. due to data source or memory problems).
  * @returns {bool}
  * @example
  * $("#wijevcal").wijevcal("deleteEvent", eventId);
  */
wijmo.evcal.wijevcal.prototype.deleteEvent = function (id, successCallback, errorCallback) {};
/** Use beginUpdate and endUpdate when making a large number of changes 
  * to widget options.
  * @example
  * $("#wijevcal").wijevcal("beginUpdate");
  * $("#wijevcal").wijevcal("option", "timeInterval", 10);
  *   $("#wijevcal").wijevcal("option", "timeRulerInterval", 20);
  * $("#wijevcal").wijevcal("endUpdate");
  */
wijmo.evcal.wijevcal.prototype.beginUpdate = function () {};
/** Use beginUpdate and endUpdate when making a large number of changes 
  * to widget options.
  * @example
  * $("#wijevcal").wijevcal("beginUpdate");
  * $("#wijevcal").wijevcal("option", "timeInterval", 10);
  * $("#wijevcal").wijevcal("option", "timeRulerInterval", 20);
  * $("#wijevcal").wijevcal("endUpdate");
  */
wijmo.evcal.wijevcal.prototype.endUpdate = function () {};
/** Navigates to the event given by the parameter id.
  * @param {object} id Event object or event id.
  * @example
  * $("#wijevcal").wijevcal("goToEvent", 
  *   "apptid_dynid1ts1320322142549");
  */
wijmo.evcal.wijevcal.prototype.goToEvent = function (id) {};
/** Tests to see if event duration is more or equals to one day.
  * @param {object} id Event object or event id.
  * @returns {bool} if event duration is more or equals to one day, return true
  * @example
  * var isAllDay = $("#wijevcal").wijevcal("isAllDayEvent", 
  *   "apptid_dynid1ts1320322142549");
  */
wijmo.evcal.wijevcal.prototype.isAllDayEvent = function (id) {};
/** Navigates to the date given by parameter dt.
  * @param {Date} dt Javascript date.
  * @example
  * $("#wijevcal").wijevcal("goToDate", new Date());
  */
wijmo.evcal.wijevcal.prototype.goToDate = function (dt) {};
/** Scrolls view to time given by parameter time.
  * @param {Date} time Javascript date.
  * @example
  * $("#wijevcal").wijevcal("goToTime", new Date());
  */
wijmo.evcal.wijevcal.prototype.goToTime = function (time) {};
/** Navigates to today's date.
  * @example
  * $("#wijevcal").wijevcal("goToday");
  */
wijmo.evcal.wijevcal.prototype.goToday = function () {};
/** Navigates to the previous date.
  * @example
  * $("#wijevcal").wijevcal("goLeft");
  */
wijmo.evcal.wijevcal.prototype.goLeft = function () {};
/** Navigates to the next date.
  * @example
  * $("#wijevcal").wijevcal("goRight");
  */
wijmo.evcal.wijevcal.prototype.goRight = function () {};
/** Invalidates the entire surface of the control 
  * and causes the control to be redrawn.
  */
wijmo.evcal.wijevcal.prototype.refresh = function () {};
/** Invalidates the entire surface of the control 
  * and causes the control to be redrawn.
  */
wijmo.evcal.wijevcal.prototype.invalidate = function () {};
/** Call this method in order to display built-in "edit calendar" dialog box.
  * @param {object} calendar Calendar object or calendar name.
  * Calendar object fields:
  * id - String, unique calendar id, this field generated automatically;
  * name - String, calendar name;
  * location - String, location field;
  * description - String, calendar description;
  * color - String, calendar color;
  * tag - String, this field can be used to store custom information.
  * @example
  * Display dialog for a new calendar:
  * $("#wijevcal").wijevcal("showEditCalendarDialog", null);
  */
wijmo.evcal.wijevcal.prototype.showEditCalendarDialog = function (calendar) {};
/** Call this method in order to display built-in "edit event" dialog box.
  * @param {object} calendar Event object.
  * Event object fields:
  * id - String, unique event id, this field generated automatically;
  * calendar - String, calendar id to which the event belongs;
  * subject - String, event title;
  * location - String, event location;
  * start - Date, start date/time;
  * end - Date, end date/time;
  * description - String, event description;
  * color - String, event color;
  * allday - Boolean, indicates all day event
  * tag - String, this field can be used to store custom information.
  * @param {object} offsetElement Optional. 
  * DOM element which will be used to calculate dialog position.
  * @example
  * Display dialog for a new event:
  * $("#wijevcal").wijevcal("showEditEventDialog", null);
  */
wijmo.evcal.wijevcal.prototype.showEditEventDialog = function (calendar, offsetElement) {};
/** Find event object by id
  * @param id
  * @returns {object} events data.
  */
wijmo.evcal.wijevcal.prototype.findEventById = function (id) {};
/** Sends a log message to built-in log console. 
  * Note: n order to use this method, you must set the enableLogs option to true.
  * @param {string} msg Log message.
  * @param {string} className Optional. CSS class name that will be applied to the destination message.
  * Few predefined classes are available: 
  * "error", "warning", "information", "status"
  */
wijmo.evcal.wijevcal.prototype.log = function (msg, className) {};
/** Changes status label text.
  * @param {string} txt The new status text.
  */
wijmo.evcal.wijevcal.prototype.status = function (txt) {};

/** @class */
var wijevcal_options = function () {};
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the culture to be used, for example, "de-DE" is German.
  * Date and time formatting depends on the culture option.</p>
  * @field 
  * @type {string}
  * @option
  */
wijevcal_options.prototype.culture = "";
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the culture calendar to be used. This option must work with culture option.</p>
  * @field 
  * @type {string}
  * @option
  */
wijevcal_options.prototype.cultureCalendar = "";
/** <p class='defaultValue'>Default value: null</p>
  * <p>Use the localization option in order to localize
  * text which not depends on culture option.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal(
  *   {
  *       localization: {
  *       buttonToday: "Go today",
  *       buttonListView: "Agenda"
  *   }
  * });
  */
wijevcal_options.prototype.localization = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Set this option to true if you want to prevent users to edit events data.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#report").wijevcal({ 
  *   readOnly: true
  * });
  */
wijevcal_options.prototype.readOnly = false;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the URL of the web service which will be used to store information about events.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#report").wijevcal({ 
  *   webServiceUrl: "http://mysite/c1evcalservice.ashx" 
  * });
  */
wijevcal_options.prototype.webServiceUrl = "";
/** <p class='defaultValue'>Default value: null</p>
  * <p>The colors option specifies the name of the colors that will be shown in the color name drop-down list.
  * "blue", "cornflowerblue", "yellow", "bronze"]</p>
  * @field 
  * @type {Array}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal(
  *   { colors: ["cornflowerblue", "yellow"]);
  */
wijevcal_options.prototype.colors = null;
/** <p>Data storage methods. Use this option in order to implement custom
  * data storage layer.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal(
  *   { dataStorage: {
  *       addEvent: function(obj, successCallback, errorCallback) {
  *       },
  *       updateEvent: function(obj, successCallback, errorCallback) {
  *       },
  *       deleteEvent: function(obj, successCallback, errorCallback) {
  *       },
  *       loadEvents: function(visibleCalendars, 
  *           successCallback, errorCallback) {
  *       },
  *       addCalendar: function(obj, successCallback, errorCallback) {
  *       },
  *       updateCalendar: function(obj, successCallback, errorCallback) {
  *       },
  *       deleteCalendar: function(obj, successCallback, errorCallback) {
  *       },
  *       loadCalendars: function(successCallback, errorCallback) {
  *       }
  *   });
  */
wijevcal_options.prototype.dataStorage = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A dataview object to bind to events data</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * var dv = $.wijmo.wijdataview({....});
  * $("#eventscalendar").wijevcal({
  *   dataSource: dv
  * })
  */
wijevcal_options.prototype.dataSource = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>The event objects array.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal(
  *       { eventsData: [{id: "appt1", 
  *           start: new Date(2011, 4, 6, 17, 30), 
  *           end: new Date(2011, 4, 6, 17, 35) }] });
  */
wijevcal_options.prototype.eventsData = null;
/** <p class='defaultValue'>Default value: []</p>
  * <p>The event objects array. This option is read-only.
  * This option is deprecated:
  * please, use eventsData option, instead.</p>
  * @field 
  * @type {array}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal(
  *           { eventsData: [{id: "appt1", 
  *               start: new Date(2011, 4, 6, 17, 30), 
  *               end: new Date(2011, 4, 6, 17, 35) }] });
  */
wijevcal_options.prototype.appointments = [];
/** <p class='defaultValue'>Default value: []</p>
  * <p>Available calendar objects array. 
  * This option is read-only. 
  * Use addCalendar/updateCalendar/deleteCalendar methods in order 
  * to add/edit or delete a calendar.</p>
  * @field 
  * @type {array}
  * @option 
  * @example
  * var calendars = $("#eventscalendar")
  *       .wijevcal("option", "calendars");
  */
wijevcal_options.prototype.calendars = [];
/** <p class='defaultValue'>Default value: false</p>
  * <p>Specifies whether the events calendar is disabled.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal("option", 
  *       "disabled", true);
  */
wijevcal_options.prototype.disabled = false;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>The calendar dialog box template.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal(
  *       { editCalendarTemplate: "html content");
  */
wijevcal_options.prototype.editCalendarTemplate = "";
/** <p class='defaultValue'>Default value: false</p>
  * <p>Enables a built-in log console.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal({ enableLogs: true });
  */
wijevcal_options.prototype.enableLogs = false;
/** <p class='defaultValue'>Default value: '{2}'</p>
  * <p>Format of the title text for the event.
  * Format arguments:
  * {0} = Start, {1} = End, {2} = Subject, {3} = Location, {4} = Icons,
  * {5} = Description.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal({ 
  *           eventTitleFormat: "{0:h:mmtt}-{1:h:mmtt} {4} {2}" });
  */
wijevcal_options.prototype.eventTitleFormat = '{2}';
/** <p>The title text format that will be shown under the header bar.
  * {0} = start date. {1} = end date.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * Specify common title format:
  *   $("#eventscalendar").wijevcal(
  *       { 
  *           titleFormat: "First date: {0:d} Last date: {1:d}" 
  *       } 
  *   );
  * Specify separate format for the each view:
  *   $("#eventscalendar").wijevcal(
  *       { 
  *           titleFormat:  { 
  *           //function customFormatFunc will be called 
  *           //in order to format string: 
  *           day: customFormatFunc,
  *           week: "Week {0:d} : {1:d}",
  *           month: "{0:yyyy, MMMM}",
  *           list: "Events until {1:d}",
  *           custom: "{0:d} - {1:d}"
  *       }
  *   }
  * );
  */
wijevcal_options.prototype.titleFormat = null;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>The first day of the week (from 0 to 6). 
  * Sunday is 0, Monday is 1, and so on.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal(
  * { firstDayOfWeek: 1 });
  */
wijevcal_options.prototype.firstDayOfWeek = 0;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Indicates whether the header bar will be visible.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijevcal({ headerBarVisible: false });
  */
wijevcal_options.prototype.headerBarVisible = true;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Indicates whether the bottom navigation bar will be visible.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijevcal({ navigationBarVisible: false });
  */
wijevcal_options.prototype.navigationBarVisible = true;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Indicates whether the right pane will be visible. 
  * By default the right pane are empty. 
  * You can use this pane in order to provide additional custom UI.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijevcal({ rightPaneVisible: false });
  */
wijevcal_options.prototype.rightPaneVisible = false;
/** <p class='defaultValue'>Default value: null</p>
  * <p>The selected date.</p>
  * @field 
  * @type {Date}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal(
  *       { selectedDate: new Date(2015, 11, 21) });
  */
wijevcal_options.prototype.selectedDate = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>The selected dates.</p>
  * @field 
  * @type {Date}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal(
  * { selectedDates: [new Date(2012, 11, 21), new Date(2015, 11, 21)] });
  */
wijevcal_options.prototype.selectedDates = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Indicates whether the status bar will be visible.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").c1reportviewer({ statusBarVisible: false });
  */
wijevcal_options.prototype.statusBarVisible = false;
/** <p class='defaultValue'>Default value: 30</p>
  * <p>The time interval in minutes for the Day view.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal(
  *       { timeInterval: 10 });
  */
wijevcal_options.prototype.timeInterval = 30;
/** <p class='defaultValue'>Default value: 15</p>
  * <p>The Day view time interval row height in pixels.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal(
  *       { timeIntervalHeight: 30 });
  */
wijevcal_options.prototype.timeIntervalHeight = 15;
/** <p class='defaultValue'>Default value: 60</p>
  * <p>Time ruler interval for the Day view (in minutes).</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal(
  *       { timeRulerInterval: 60 });
  */
wijevcal_options.prototype.timeRulerInterval = 60;
/** <p class='defaultValue'>Default value: '{0:h tt}'</p>
  * <p>Time ruler format for the Day view.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Format argument:
  * {0} = Current ruler time.
  * @example
  * $("#eventscalendar").wijevcal(
  *   { timeRulerFormat: "{0:t}" });
  */
wijevcal_options.prototype.timeRulerFormat = '{0:h tt}';
/** <p class='defaultValue'>Default value: '{0:d }'</p>
  * <p>Format of the text for the day cell header(month view).
  * Format argument:
  *  {0} = Day date.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal(
  *   { dayHeaderFormat: "{0}" });
  */
wijevcal_options.prototype.dayHeaderFormat = '{0:d }';
/** <p class='defaultValue'>Default value: '{0:ddd d}'</p>
  * <p>Format of the text for the first cell header in the first row of the month view.
  * Format argument:
  *  {0} = Day date.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal(
  *       { firstRowDayHeaderFormat: "{0}" });
  */
wijevcal_options.prototype.firstRowDayHeaderFormat = '{0:ddd d}';
/** <p>Format of the text for the day header in the day view. Format argument: {0} = Day date.</p>
  * @field 
  * @option 
  * @remarks
  * Format argument:
  * {0} = Day date.
  * day: "all-day events",
  * week: "{0:d dddd}",
  * list: "{0:d dddd}",
  * custom: "{0:d dddd}"
  * }
  * @example
  * $("#eventscalendar").wijevcal(
  *       { dayViewHeaderFormat: "{0: d}" });
  */
wijevcal_options.prototype.dayViewHeaderFormat = null;
/** <p class='defaultValue'>Default value: 'day'</p>
  * <p>The active view type. Possible values are: day, week, month, list. If it is a custom view, the viewType should be its name.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal(
  *   { viewType: "month" });
  */
wijevcal_options.prototype.viewType = 'day';
/** <p class='defaultValue'>Default value: []</p>
  * <p>Array of the view types which need to be shown.</p>
  * @field 
  * @type
  * {{Array}
  * possible items in the array is "day", "week", "month", "list". 
  * If add a custom view, item should be an Object:
  * object fields:
  * name - String, The unique name of custom view which is displayed on the toolbar;
  * unit - String, the time unit of custom view possible values are "day", "week", "month", "year";
  * count - number, the count of time span, depends on the unit;
  * {
  * name: "2 Days",
  * unit: "day",
  * count: 2
  * }}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal("option", 
  * "views", ["day", "week", "month", "list", {name: "2 Days", unit: "day", count: 2}]
  * );
  */
wijevcal_options.prototype.views = [];
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value indicating the event dialog element will be append to the body or eventcalendar container.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * If the value is true, the dialog will be appended to body element.
  * else it will append to the eventcalendar container.
  * @example
  * $("#eventscalendar").wijevcal(
  *      { ensureEventDialogOnBody: true });
  */
wijevcal_options.prototype.ensureEventDialogOnBody = false;
/** <p class='defaultValue'>Default value: []</p>
  * <p>Array of the calendar names which need to be shown.</p>
  * @field 
  * @type {Array}
  * @option 
  * @example
  * $("#eventscalendar").wijevcal("option", 
  *       "visibleCalendars", ["My Calendar"]);
  */
wijevcal_options.prototype.visibleCalendars = [];
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates calendar's options in evets calendar.</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * Its value is wijcalendar's option, visit 
  * http://wijmo.com/docs/wijmo/#Wijmo~jQuery.fn.-~wijcalendar.html for more details.
  * @example
  * $("#eventscalendar").wijevcal(
  *      { calendar: { prevTooltip: "Previous", nextTooltip: "Next" } });
  */
wijevcal_options.prototype.calendar = null;
/** Occurs when calendars option has been changed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.evcal.ICalendarsChangedEventArgs} data Information about an event
  */
wijevcal_options.prototype.calendarsChanged = null;
/** Occurs when events calendar is constructed and events
  * data is loaded from an external or local data source.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijevcal_options.prototype.initialized = null;
/** Occurs when selectedDates option has been changed.
  * Event type: wijevcalselecteddateschanged
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.evcal.ISelectedDatesChangedEventArgs} data Information about an event
  */
wijevcal_options.prototype.selectedDatesChanged = null;
/** Occurs when viewType option has been changed.
  * @event 
  * @param {string} viewType The new viewType value.
  */
wijevcal_options.prototype.viewTypeChanged = null;
/** Occurs when event markup is creating.
  * @event 
  * @param {Object} data The data with this event. data object fields:
  * id - String, unique event id, this field generated automatically;
  * calendar - String, calendar id to which the event belongs;
  * subject - String, event title;
  * location - String, event location;
  * start - Date, start date/time;
  * end - Date, end date/time;
  * description - String, event description;
  * color - String, event color;
  * allday - Boolean, indicates all day event
  * tag - String, this field can be used to store custom information.
  * @param {string} eventTemplate The template of events displayed on the view.
  */
wijevcal_options.prototype.eventCreating = null;
/** Occurs before the built-in event dialog is shown.
  * Return false or call event.preventDefault() in order to cancel event and
  * prevent the built-in dialog to be shown.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.evcal.IBeforeEditEventDialogShowEventArgs} data Information about an event
  */
wijevcal_options.prototype.beforeEditEventDialogShow = null;
/** Occurs before the add event action.
  * Return false or call event.preventDefault() in order to cancel event and
  * prevent the add action.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.evcal.IBeforeAddEventEventArgs} data Information about an event
  */
wijevcal_options.prototype.beforeAddEvent = null;
/** Occurs before the update event action.
  * Return false or call event.preventDefault() in order to cancel event and
  * prevent the update action.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.evcal.IBeforeUpdateEventEventArgs} data Information about an event
  */
wijevcal_options.prototype.beforeUpdateEvent = null;
/** Occurs before the delete action.
  * Return false or call event.preventDefault() in order to cancel event and
  * prevent the delete action.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.evcal.IBeforeDeleteEventEventArgs} data Information about an event
  */
wijevcal_options.prototype.beforeDeleteEvent = null;
/** Occurs before the add calendar action.
  * Return false or call event.preventDefault() in order to cancel event and
  * prevent the add action.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.evcal.IBeforeAddCalendarEventArgs} data Information about an event
  */
wijevcal_options.prototype.beforeAddCalendar = null;
/** Occurs before the update calendar action.
  * Return false or call event.preventDefault() in order to cancel event and
  * prevent the update action.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.evcal.IBeforeUpdateCalendarEventArgs} data Information about an event
  */
wijevcal_options.prototype.beforeUpdateCalendar = null;
/** Occurs before the delete calendar action.
  * Return false or call event.preventDefault() in order to cancel event and
  * prevent the delete action.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.evcal.IBeforeDeleteCalendarEventArgs} data Information about an event
  */
wijevcal_options.prototype.beforeDeleteCalendar = null;
/** Occurs when the eventsData option is changed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.evcal.IEventsDataChangedEventArgs} data Information about an event
  */
wijevcal_options.prototype.eventsDataChanged = null;
wijmo.evcal.wijevcal.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijevcal_options());
$.widget("wijmo.wijevcal", $.wijmo.widget, wijmo.evcal.wijevcal.prototype);
/** Contains information about wijevcal.calendarsChanged event
  * @interface ICalendarsChangedEventArgs
  * @namespace wijmo.evcal
  */
wijmo.evcal.ICalendarsChangedEventArgs = function () {};
/** <p>the new calendars option value.</p>
  * @field 
  * @type {object}
  */
wijmo.evcal.ICalendarsChangedEventArgs.prototype.calendars = null;
/** Contains information about wijevcal.selectedDatesChanged event
  * @interface ISelectedDatesChangedEventArgs
  * @namespace wijmo.evcal
  */
wijmo.evcal.ISelectedDatesChangedEventArgs = function () {};
/** <p>the new selectedDates value.</p>
  * @field 
  * @type {object}
  */
wijmo.evcal.ISelectedDatesChangedEventArgs.prototype.selectedDates = null;
/** Contains information about wijevcal.beforeEditEventDialogShow event
  * @interface IBeforeEditEventDialogShowEventArgs
  * @namespace wijmo.evcal
  */
wijmo.evcal.IBeforeEditEventDialogShowEventArgs = function () {};
/** <p>This is the event data.</p>
  * @field 
  * @type {object}
  */
wijmo.evcal.IBeforeEditEventDialogShowEventArgs.prototype.data = null;
/** <p>This is target offset DOM element which can be used for popup callout.</p>
  * @field 
  * @type {DOMElement}
  */
wijmo.evcal.IBeforeEditEventDialogShowEventArgs.prototype.targetCell = null;
/** Contains information about wijevcal.beforeAddEvent event
  * @interface IBeforeAddEventEventArgs
  * @namespace wijmo.evcal
  */
wijmo.evcal.IBeforeAddEventEventArgs = function () {};
/** <p>This is the new event data that should be added to a data source.</p>
  * @field 
  * @type {object}
  */
wijmo.evcal.IBeforeAddEventEventArgs.prototype.data = null;
/** Contains information about wijevcal.beforeUpdateEvent event
  * @interface IBeforeUpdateEventEventArgs
  * @namespace wijmo.evcal
  */
wijmo.evcal.IBeforeUpdateEventEventArgs = function () {};
/** <p>This is the new event data that should be added to a data source.</p>
  * @field 
  * @type {object}
  */
wijmo.evcal.IBeforeUpdateEventEventArgs.prototype.data = null;
/** <p>This is previous event data.</p>
  * @field 
  * @type {object}
  */
wijmo.evcal.IBeforeUpdateEventEventArgs.prototype.prevData = null;
/** Contains information about wijevcal.beforeDeleteEvent event
  * @interface IBeforeDeleteEventEventArgs
  * @namespace wijmo.evcal
  */
wijmo.evcal.IBeforeDeleteEventEventArgs = function () {};
/** <p>This is the event object that should be deleted.</p>
  * @field 
  * @type {object}
  */
wijmo.evcal.IBeforeDeleteEventEventArgs.prototype.data = null;
/** Contains information about wijevcal.beforeAddCalendar event
  * @interface IBeforeAddCalendarEventArgs
  * @namespace wijmo.evcal
  */
wijmo.evcal.IBeforeAddCalendarEventArgs = function () {};
/** <p>This is  the new calendar data that should be added to a data source.</p>
  * @field 
  * @type {object}
  */
wijmo.evcal.IBeforeAddCalendarEventArgs.prototype.data = null;
/** Contains information about wijevcal.beforeUpdateCalendar event
  * @interface IBeforeUpdateCalendarEventArgs
  * @namespace wijmo.evcal
  */
wijmo.evcal.IBeforeUpdateCalendarEventArgs = function () {};
/** <p>This is the new event data that should be added to a data source.</p>
  * @field 
  * @type {object}
  */
wijmo.evcal.IBeforeUpdateCalendarEventArgs.prototype.data = null;
/** <p>This is previous event data.</p>
  * @field 
  * @type {object}
  */
wijmo.evcal.IBeforeUpdateCalendarEventArgs.prototype.prevData = null;
/** Contains information about wijevcal.beforeDeleteCalendar event
  * @interface IBeforeDeleteCalendarEventArgs
  * @namespace wijmo.evcal
  */
wijmo.evcal.IBeforeDeleteCalendarEventArgs = function () {};
/** <p>This is the calendar data that should be deleted from a data source.</p>
  * @field 
  * @type {object}
  */
wijmo.evcal.IBeforeDeleteCalendarEventArgs.prototype.data = null;
/** Contains information about wijevcal.eventsDataChanged event
  * @interface IEventsDataChangedEventArgs
  * @namespace wijmo.evcal
  */
wijmo.evcal.IEventsDataChangedEventArgs = function () {};
/** <p>This is array of the event objects.</p>
  * @field 
  * @type {object}
  */
wijmo.evcal.IEventsDataChangedEventArgs.prototype.eventsData = null;
})()
})()
