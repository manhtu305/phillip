var wijmo = wijmo || {};

(function () {
wijmo.textbox = wijmo.textbox || {};

(function () {
/** @class wijtextbox
  * @widget 
  * @namespace jQuery.wijmo.textbox
  * @extends wijmo.wijmoWidget
  */
wijmo.textbox.wijtextbox = function () {};
wijmo.textbox.wijtextbox.prototype = new wijmo.wijmoWidget();
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.textbox.wijtextbox.prototype.destroy = function () {};

/** @class */
var wijtextbox_options = function () {};
wijmo.textbox.wijtextbox.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijtextbox_options());
$.widget("wijmo.wijtextbox", $.wijmo.widget, wijmo.textbox.wijtextbox.prototype);
})()
})()
