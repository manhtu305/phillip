var Q = Q || {};

(function () {
/** @interface IPromise
  * @namespace Q
  */
Q.IPromise = function () {};
/** @param {function} onFulfill
  * @param {function} onReject
  * @returns {IPromise}
  */
Q.IPromise.prototype.then = function (onFulfill, onReject) {};
/** @param {function} onFulfill
  * @param {function} onReject
  * @returns {IPromise}
  */
Q.IPromise.prototype.then = function (onFulfill, onReject) {};
/** @param {function} onFulfill
  * @param {function} onReject
  * @returns {IPromise}
  */
Q.IPromise.prototype.then = function (onFulfill, onReject) {};
/** @param {function} onFulfill
  * @param {function} onReject
  * @returns {IPromise}
  */
Q.IPromise.prototype.then = function (onFulfill, onReject) {};
/** @interface Deferred
  * @namespace Q
  */
Q.Deferred = function () {};
/** @param {T in Q.Deferred<T>} value
  * @returns {void}
  */
Q.Deferred.prototype.resolve = function (value) {};
/** @param reason
  * @returns {void}
  */
Q.Deferred.prototype.reject = function (reason) {};
/** @param value
  * @returns {void}
  */
Q.Deferred.prototype.notify = function (value) {};
/** @returns {function} */
Q.Deferred.prototype.makeNodeResolver = function () {};
/** <p>undefined</p>
  * @field 
  * @type {Promise}
  */
Q.Deferred.prototype.promise = null;
/** @interface Promise
  * @namespace Q
  */
Q.Promise = function () {};
/** Like a finally clause, allows you to observe either the fulfillment or rejection of a promise, but to do so without modifying the final value. This is useful for collecting resources regardless of whether a job succeeded, like closing a database connection, shutting a server down, or deleting an unneeded key from an object.
  * finally returns a promise, which will become resolved with the same fulfillment value or rejection reason as promise. However, if callback returns a promise, the resolution of the returned promise will be delayed until the promise returned from callback is finished.
  * @param {function} finallyCallback
  * @returns {Promise}
  */
Q.Promise.prototype.fin = function (finallyCallback) {};
/** Like a finally clause, allows you to observe either the fulfillment or rejection of a promise, but to do so without modifying the final value. This is useful for collecting resources regardless of whether a job succeeded, like closing a database connection, shutting a server down, or deleting an unneeded key from an object.
  * finally returns a promise, which will become resolved with the same fulfillment value or rejection reason as promise. However, if callback returns a promise, the resolution of the returned promise will be delayed until the promise returned from callback is finished.
  * @param {function} finallyCallback
  * @returns {Promise}
  */
Q.Promise.prototype.finally = function (finallyCallback) {};
/** The then method from the Promises/A+ specification, with an additional progress handler.
  * @param {function} onFulfill
  * @param {function} onReject
  * @param {Function} onProgress
  * @returns {Promise}
  */
Q.Promise.prototype.then = function (onFulfill, onReject, onProgress) {};
/** The then method from the Promises/A+ specification, with an additional progress handler.
  * @param {function} onFulfill
  * @param {function} onReject
  * @param {Function} onProgress
  * @returns {Promise}
  */
Q.Promise.prototype.then = function (onFulfill, onReject, onProgress) {};
/** The then method from the Promises/A+ specification, with an additional progress handler.
  * @param {function} onFulfill
  * @param {function} onReject
  * @param {Function} onProgress
  * @returns {Promise}
  */
Q.Promise.prototype.then = function (onFulfill, onReject, onProgress) {};
/** The then method from the Promises/A+ specification, with an additional progress handler.
  * @param {function} onFulfill
  * @param {function} onReject
  * @param {Function} onProgress
  * @returns {Promise}
  */
Q.Promise.prototype.then = function (onFulfill, onReject, onProgress) {};
/** Like then, but "spreads" the array into a variadic fulfillment handler. If any of the promises in the array are rejected, instead calls onRejected with the first rejected promise's rejection reason.
  * This is especially useful in conjunction with all
  * @param {Function} onFulfilled
  * @param {Function} onRejected
  * @returns {Promise}
  */
Q.Promise.prototype.spread = function (onFulfilled, onRejected) {};
/** @param {function} onRejected
  * @returns {Promise}
  */
Q.Promise.prototype.fail = function (onRejected) {};
/** @param {function} onRejected
  * @returns {Promise}
  */
Q.Promise.prototype.fail = function (onRejected) {};
/** A sugar method, equivalent to promise.then(undefined, onRejected).
  * @param {function} onRejected
  * @returns {Promise}
  */
Q.Promise.prototype.catch = function (onRejected) {};
/** A sugar method, equivalent to promise.then(undefined, onRejected).
  * @param {function} onRejected
  * @returns {Promise}
  */
Q.Promise.prototype.catch = function (onRejected) {};
/** A sugar method, equivalent to promise.then(undefined, undefined, onProgress).
  * @param {function} onProgress
  * @returns {Promise}
  */
Q.Promise.prototype.progress = function (onProgress) {};
/** Much like then, but with different behavior around unhandled rejection. If there is an unhandled rejection, either because promise is rejected and no onRejected callback was provided, or because onFulfilled or onRejected threw an error or returned a rejected promise, the resulting rejection reason is thrown as an exception in a future turn of the event loop.
  * This method should be used to terminate chains of promises that will not be passed elsewhere. Since exceptions thrown in then callbacks are consumed and transformed into rejections, exceptions at the end of the chain are easy to accidentally, silently ignore. By arranging for the exception to be thrown in a future turn of the event loop, so that it won't be caught, it causes an onerror event on the browser window, or an uncaughtException event on Node.js's process object.
  * Exceptions thrown by done will have long stack traces, if Q.longStackSupport is set to true. If Q.onerror is set, exceptions will be delivered there instead of thrown in a future turn.
  * The Golden Rule of done vs. then usage is: either return your promise to someone else, or if the chain ends with you, call done to terminate it.
  * @param {function} onFulfilled
  * @param {function} onRejected
  * @param {function} onProgress
  * @returns {void}
  */
Q.Promise.prototype.done = function (onFulfilled, onRejected, onProgress) {};
/** Returns a promise to get the named property of an object. Essentially equivalent to
  * promise.then(function (o) {
  *     return o[propertyName];
  * });
  * @param {string} propertyName
  * @returns {Promise}
  */
Q.Promise.prototype.get = function (propertyName) {};
/** @param {string} propertyName
  * @param value
  * @returns {Promise}
  */
Q.Promise.prototype.set = function (propertyName, value) {};
/** @param {string} propertyName
  * @returns {Promise}
  */
Q.Promise.prototype["delete"] = function (propertyName) {};
/** Returns a promise for the result of calling the named method of an object with the given array of arguments. The object itself is this in the function, just like a synchronous method call. Essentially equivalent to
  * promise.then(function (o) {
  *     return o[methodName].apply(o, args);
  * });
  * @param {string} methodName
  * @param {any[]} args
  * @returns {Promise}
  */
Q.Promise.prototype.post = function (methodName, args) {};
/** Returns a promise for the result of calling the named method of an object with the given variadic arguments. The object itself is this in the function, just like a synchronous method call.
  * @param {string} methodName
  * @param {any[]} args
  * @returns {Promise}
  */
Q.Promise.prototype.invoke = function (methodName, args) {};
/** @param {any[]} args
  * @returns {Promise}
  */
Q.Promise.prototype.fapply = function (args) {};
/** @param {any[]} args
  * @returns {Promise}
  */
Q.Promise.prototype.fcall = function (args) {};
/** Returns a promise for an array of the property names of an object. Essentially equivalent to
  * promise.then(function (o) {
  *     return Object.keys(o);
  * });
  * @returns {Promise}
  */
Q.Promise.prototype.keys = function () {};
/** A sugar method, equivalent to promise.then(function () { return value; }).
  * @param {U} value
  * @returns {Promise}
  */
Q.Promise.prototype.thenResolve = function (value) {};
/** A sugar method, equivalent to promise.then(function () { throw reason; }).
  * @param reason
  * @returns {Promise}
  */
Q.Promise.prototype.thenReject = function (reason) {};
/** @param {number} ms
  * @param {string} message
  * @returns {Promise}
  */
Q.Promise.prototype.timeout = function (ms, message) {};
/** Returns a promise that will have the same result as promise, but will only be fulfilled or rejected after at least ms milliseconds have passed.
  * @param {number} ms
  * @returns {Promise}
  */
Q.Promise.prototype.delay = function (ms) {};
/** Returns whether a given promise is in the fulfilled state. When the static version is used on non-promises, the result is always true.
  * @returns {bool}
  */
Q.Promise.prototype.isFulfilled = function () {};
/** Returns whether a given promise is in the rejected state. When the static version is used on non-promises, the result is always false.
  * @returns {bool}
  */
Q.Promise.prototype.isRejected = function () {};
/** Returns whether a given promise is in the pending state. When the static version is used on non-promises, the result is always false.
  * @returns {bool}
  */
Q.Promise.prototype.isPending = function () {};
/**  */
Q.Promise.prototype.valueOf = function () {};
/** Returns a "state snapshot" object, which will be in one of three forms:
  * - { state: "pending" }
  * - { state: "fulfilled", value: &lt;fulfllment value&gt; }
  * - { state: "rejected", reason: &lt;rejection reason&gt; }
  * @returns {PromiseState}
  */
Q.Promise.prototype.inspect = function () {};
/** @interface PromiseState
  * @namespace Q
  */
Q.PromiseState = function () {};
/** <p>"fulfilled", "rejected", "pending"</p>
  * @field 
  * @type {string}
  */
Q.PromiseState.prototype.state = null;
/** <p>undefined</p>
  * @field 
  * @type {T in Q.PromiseState<T>}
  */
Q.PromiseState.prototype.value = null;
/** <p>undefined</p>
  * @field
  */
Q.PromiseState.prototype.reason = null;
/** @param {IPromise} value
  * @returns {Promise}
  */
Q.when = function (value) {};
/** @param {T} value
  * @returns {Promise}
  */
Q.when = function (value) {};
/** @param {T} value
  * @param {function} onFulfilled
  * @returns {Promise}
  */
Q.when = function (value, onFulfilled) {};
/** @param {T} value
  * @param {function} onFulfilled
  * @returns {Promise}
  */
Q.when = function (value, onFulfilled) {};
/** @param {IPromise} value
  * @param {function} onFulfilled
  * @param {function} onRejected
  * @param {function} onProgress
  * @returns {Promise}
  */
Q.when = function (value, onFulfilled, onRejected, onProgress) {};
/** @param {IPromise} value
  * @param {function} onFulfilled
  * @param {function} onRejected
  * @param {function} onProgress
  * @returns {Promise}
  */
Q.when = function (value, onFulfilled, onRejected, onProgress) {};
/** @param {IPromise} value
  * @param {function} onFulfilled
  * @param {function} onRejected
  * @param {function} onProgress
  * @returns {Promise}
  */
Q.when = function (value, onFulfilled, onRejected, onProgress) {};
/** @param {IPromise} value
  * @param {function} onFulfilled
  * @param {function} onRejected
  * @param {function} onProgress
  * @returns {Promise}
  */
Q.when = function (value, onFulfilled, onRejected, onProgress) {};
/** @param {function} method
  * @param {any[]} args
  * @returns {function}
  */
Q.fbind = function (method, args) {};
/** @param {function} method
  * @param {any[]} args
  * @returns {function}
  */
Q.fbind = function (method, args) {};
/** @param {function} method
  * @param {any[]} args
  * @returns {Promise}
  */
Q.fcall = function (method, args) {};
/** @param obj
  * @param {string} functionName
  * @param {any[]} args
  * @returns {Promise}
  */
Q.send = function (obj, functionName, args) {};
/** @param obj
  * @param {string} functionName
  * @param {any[]} args
  * @returns {Promise}
  */
Q.invoke = function (obj, functionName, args) {};
/** @param obj
  * @param {string} functionName
  * @param {any[]} args
  * @returns {Promise}
  */
Q.mcall = function (obj, functionName, args) {};
/** @param {Function} nodeFunction
  * @param {any[]} args
  * @returns {function}
  */
Q.denodeify = function (nodeFunction, args) {};
/** @param {Function} nodeFunction
  * @param {any[]} args
  * @returns {function}
  */
Q.nfbind = function (nodeFunction, args) {};
/** @param {Function} nodeFunction
  * @param {any[]} args
  * @returns {Promise}
  */
Q.nfcall = function (nodeFunction, args) {};
/** @param {Function} nodeFunction
  * @param {any[]} args
  * @returns {Promise}
  */
Q.nfapply = function (nodeFunction, args) {};
/** @param nodeModule
  * @param {string} functionName
  * @param {any[]} args
  * @returns {Promise}
  */
Q.ninvoke = function (nodeModule, functionName, args) {};
/** @param nodeModule
  * @param {string} functionName
  * @param {any[]} args
  * @returns {Promise}
  */
Q.nsend = function (nodeModule, functionName, args) {};
/** @param nodeModule
  * @param {string} functionName
  * @param {any[]} args
  * @returns {Promise}
  */
Q.nmcall = function (nodeModule, functionName, args) {};
/** Returns a promise that is fulfilled with an array containing the fulfillment value of each promise, or is rejected with the same rejection reason as the first promise to be rejected.
  * @param {IPromise[]} promises
  * @returns {Promise}
  */
Q.all = function (promises) {};
/** Returns a promise that is fulfilled with an array containing the fulfillment value of each promise, or is rejected with the same rejection reason as the first promise to be rejected.
  * @param {any[]} promises
  * @returns {Promise}
  */
Q.all = function (promises) {};
/** Returns a promise that is fulfilled with an array of promise state snapshots, but only after all the original promises have settled, i.e. become either fulfilled or rejected.
  * @param {IPromise[]} promises
  * @returns {Promise}
  */
Q.allSettled = function (promises) {};
/** Returns a promise that is fulfilled with an array of promise state snapshots, but only after all the original promises have settled, i.e. become either fulfilled or rejected.
  * @param {any[]} promises
  * @returns {Promise}
  */
Q.allSettled = function (promises) {};
/** @param {IPromise[]} promises
  * @returns {Promise}
  */
Q.allResolved = function (promises) {};
/** @param {any[]} promises
  * @returns {Promise}
  */
Q.allResolved = function (promises) {};
/** Like then, but "spreads" the array into a variadic fulfillment handler. If any of the promises in the array are rejected, instead calls onRejected with the first rejected promise's rejection reason. 
  * This is especially useful in conjunction with all.
  * @param {any[]} promises
  * @param {function} onFulfilled
  * @param {function} onRejected
  * @returns {Promise}
  */
Q.spread = function (promises, onFulfilled, onRejected) {};
/** Like then, but "spreads" the array into a variadic fulfillment handler. If any of the promises in the array are rejected, instead calls onRejected with the first rejected promise's rejection reason. 
  * This is especially useful in conjunction with all.
  * @param {any[]} promises
  * @param {function} onFulfilled
  * @param {function} onRejected
  * @returns {Promise}
  */
Q.spread = function (promises, onFulfilled, onRejected) {};
/** Like then, but "spreads" the array into a variadic fulfillment handler. If any of the promises in the array are rejected, instead calls onRejected with the first rejected promise's rejection reason. 
  * This is especially useful in conjunction with all.
  * @param {any[]} promises
  * @param {function} onFulfilled
  * @param {function} onRejected
  * @returns {Promise}
  */
Q.spread = function (promises, onFulfilled, onRejected) {};
/** Like then, but "spreads" the array into a variadic fulfillment handler. If any of the promises in the array are rejected, instead calls onRejected with the first rejected promise's rejection reason. 
  * This is especially useful in conjunction with all.
  * @param {any[]} promises
  * @param {function} onFulfilled
  * @param {function} onRejected
  * @returns {Promise}
  */
Q.spread = function (promises, onFulfilled, onRejected) {};
/** Like then, but "spreads" the array into a variadic fulfillment handler. If any of the promises in the array are rejected, instead calls onRejected with the first rejected promise's rejection reason. 
  * This is especially useful in conjunction with all.
  * @param {IPromise[]} promises
  * @param {function} onFulfilled
  * @param {function} onRejected
  * @returns {Promise}
  */
Q.spread = function (promises, onFulfilled, onRejected) {};
/** Like then, but "spreads" the array into a variadic fulfillment handler. If any of the promises in the array are rejected, instead calls onRejected with the first rejected promise's rejection reason. 
  * This is especially useful in conjunction with all.
  * @param {IPromise[]} promises
  * @param {function} onFulfilled
  * @param {function} onRejected
  * @returns {Promise}
  */
Q.spread = function (promises, onFulfilled, onRejected) {};
/** Like then, but "spreads" the array into a variadic fulfillment handler. If any of the promises in the array are rejected, instead calls onRejected with the first rejected promise's rejection reason. 
  * This is especially useful in conjunction with all.
  * @param {IPromise[]} promises
  * @param {function} onFulfilled
  * @param {function} onRejected
  * @returns {Promise}
  */
Q.spread = function (promises, onFulfilled, onRejected) {};
/** Like then, but "spreads" the array into a variadic fulfillment handler. If any of the promises in the array are rejected, instead calls onRejected with the first rejected promise's rejection reason. 
  * This is especially useful in conjunction with all.
  * @param {IPromise[]} promises
  * @param {function} onFulfilled
  * @param {function} onRejected
  * @returns {Promise}
  */
Q.spread = function (promises, onFulfilled, onRejected) {};
/** Returns a promise that will have the same result as promise, except that if promise is not fulfilled or rejected before ms milliseconds, the returned promise will be rejected with an Error with the given message. If message is not supplied, the message will be "Timed out after " + ms + " ms".
  * @param {Promise} promise
  * @param {number} ms
  * @param {string} message
  * @returns {Promise}
  */
Q.timeout = function (promise, ms, message) {};
/** Returns a promise that will have the same result as promise, but will only be fulfilled or rejected after at least ms milliseconds have passed.
  * @param {Promise} promise
  * @param {number} ms
  * @returns {Promise}
  */
Q.delay = function (promise, ms) {};
/** Returns a promise that will have the same result as promise, but will only be fulfilled or rejected after at least ms milliseconds have passed.
  * @param {T} value
  * @param {number} ms
  * @returns {Promise}
  */
Q.delay = function (value, ms) {};
/** Returns whether a given promise is in the fulfilled state. When the static version is used on non-promises, the result is always true.
  * @param {Promise} promise
  * @returns {bool}
  */
Q.isFulfilled = function (promise) {};
/** Returns whether a given promise is in the rejected state. When the static version is used on non-promises, the result is always false.
  * @param {Promise} promise
  * @returns {bool}
  */
Q.isRejected = function (promise) {};
/** Returns whether a given promise is in the pending state. When the static version is used on non-promises, the result is always false.
  * @param {Promise} promise
  * @returns {bool}
  */
Q.isPending = function (promise) {};
/** Returns a "deferred" object with a:
  * promise property
  * resolve(value) method
  * reject(reason) method
  * notify(value) method
  * makeNodeResolver() method
  * @returns {Deferred}
  */
Q.defer = function () {};
/** Returns a promise that is rejected with reason.
  * @param reason
  * @returns {Promise}
  */
Q.reject = function (reason) {};
/** @param {function} resolver
  * @returns {Promise}
  */
Q.promise = function (resolver) {};
/** @param {function} resolver
  * @returns {Promise}
  */
Q.promise = function (resolver) {};
/** Creates a new version of func that accepts any combination of promise and non-promise values, converting them to their fulfillment values before calling the original func. The returned version also always returns a promise: if func does a return or throw, then Q.promised(func) will return fulfilled or rejected promise, respectively.
  * This can be useful for creating functions that accept either promises or non-promise values, and for ensuring that the function always returns a promise even in the face of unintentional thrown exceptions.
  * @param {function} callback
  * @returns {function}
  */
Q.promised = function (callback) {};
/** Returns whether the given value is a Q promise.
  * @param object
  * @returns {bool}
  */
Q.isPromise = function (object) {};
/** Returns whether the given value is a promise (i.e. it's an object with a then function).
  * @param object
  * @returns {bool}
  */
Q.isPromiseAlike = function (object) {};
/** Returns whether a given promise is in the pending state. When the static version is used on non-promises, the result is always false.
  * @param object
  * @returns {bool}
  */
Q.isPending = function (object) {};
/** This is an experimental tool for converting a generator function into a deferred function. This has the potential of reducing nested callbacks in engines that support yield.
  * @param generatorFunction
  * @returns {function}
  */
Q.async = function (generatorFunction) {};
/** @param {Function} callback
  * @returns {void}
  */
Q.nextTick = function (callback) {};
/** Calling resolve with a pending promise causes promise to wait on the passed promise, becoming fulfilled with its fulfillment value or rejected with its rejection reason (or staying pending forever, if the passed promise does).
  * Calling resolve with a rejected promise causes promise to be rejected with the passed promise's rejection reason.
  * Calling resolve with a fulfilled promise causes promise to be fulfilled with the passed promise's fulfillment value.
  * Calling resolve with a non-promise value causes promise to be fulfilled with that value.
  * @param {IPromise} object
  * @returns {Promise}
  */
Q.resolve = function (object) {};
/** Calling resolve with a pending promise causes promise to wait on the passed promise, becoming fulfilled with its fulfillment value or rejected with its rejection reason (or staying pending forever, if the passed promise does).
  * Calling resolve with a rejected promise causes promise to be rejected with the passed promise's rejection reason.
  * Calling resolve with a fulfilled promise causes promise to be fulfilled with the passed promise's fulfillment value.
  * Calling resolve with a non-promise value causes promise to be fulfilled with that value.
  * @param {T} object
  * @returns {Promise}
  */
Q.resolve = function (object) {};
})()
