var wijmo = wijmo || {};

(function () {
wijmo.datepager = wijmo.datepager || {};

(function () {
/** @class wijdatepager
  * @widget 
  * @namespace jQuery.wijmo.datepager
  * @extends wijmo.wijmoWidget
  */
wijmo.datepager.wijdatepager = function () {};
wijmo.datepager.wijdatepager.prototype = new wijmo.wijmoWidget();
/** Destroys the widget and resets the DOM element. */
wijmo.datepager.wijdatepager.prototype.destroy = function () {};
/** Refreshes the widget layout. */
wijmo.datepager.wijdatepager.prototype.refresh = function () {};
/** Redraws the widget layout. */
wijmo.datepager.wijdatepager.prototype.invalidate = function () {};
/** Selects the previous date.
  * @param {Object} ev The event of firing the select previous date.
  * @returns {bool}
  */
wijmo.datepager.wijdatepager.prototype.goLeft = function (ev) {};
/** Selects the next date. */
wijmo.datepager.wijdatepager.prototype.goRight = function () {};

/** @class */
var wijdatepager_options = function () {};
/** <p>undefined</p>
  * @field 
  * @option
  */
wijdatepager_options.prototype.wijCSS = null;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Culture name, e.g. "de-DE".</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * // This markup sets the culture to German:
  * $("#element").wijdatepager( { culture: "de-DE" } );
  */
wijdatepager_options.prototype.culture = "";
/** <p class='defaultValue'>Default value: ""</p>
  * <p>A value that indicators the culture calendar to format the text.
  *  This option must work with culture option.</p>
  * @field 
  * @type {string}
  * @option
  */
wijdatepager_options.prototype.cultureCalendar = "";
/** <p class='defaultValue'>Default value: null</p>
  * <p>Use the localization option in order to provide custom localization.</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * Default:
  * {
  * dayViewTooltipFormat: "{0:dddd, MMMM d, yyyy}",
  * weekViewTooltipFormat: "{0:MMMM d} - {1:d, yyyy}",
  * weekViewTooltip2MonthesFormat: "{0:MMMM d} - {1:MMMM d, yyyy}",			
  * monthViewTooltipFormat: "{0:MMMM yyyy}",
  * customViewDayUnitTooltip2MonthesFormat: "{0:MMMM d} - {1:MMMM d, yyyy}",
  * customViewSingleDayTooltipFormat: "{0:MMMM d yyyy}",
  * customViewDayTooltipFormat: "{0:MMMM d} - {1:d, yyyy}",
  * customViewMonthTooltip2YearsFormat: "{0:MMMM yyyy} - {1:MMMM yyyy}",
  * customViewSingleMonthTooltipFormat: "{0:MMMM yyyy}",
  * customViewMonthTooltipFormat: "{0:MMMM} - {1:MMMM, yyyy}",
  * customViewSingleYearTooltipFormat: "{0:yyyy}",
  * customViewYearTooltipFormat: "{0:yyyy} - {1:yyyy}",
  * customViewUnitDayLabelFormat: "{0:MMM dd}-{1:dd}",
  * customViewUnitSingleDayLabelFormat: "{0:MMM dd}",
  * customViewUnitDayLabelFormat2Months: "{0:MMM dd}-{1:MMM dd}",
  * customViewUnitMonthLabelFormat: "{0:MMM}-{1:MMM, yyyy}",
  * customViewUnitSingleMonthLabelFormat: "{0:MMM}",
  * customViewUnitMonthLabelFormat2Years: "{0:MMM yyyy}-{1:MMM yyyy}",
  * customViewUnitSingleYearLabelFormat: "{0:yyyy}",
  * customViewUnitYearLabelFormat: "{0:yyyy}-{1:yyyy}",
  * dayViewLabelFormat": "{0:d }",
  * dayViewMonthLabelFormat: "{0:MM}",
  * weekViewLabelFormat: "{0:MMM dd}-{1:dd}",
  * monthViewLabelFormat: "{0:MMM}",
  * monthViewYearLabelFormat: "{0:yyyy}"
  * }
  * @example
  * $("#datepager").wijdatepager(
  *                 { 
  *                     localization: {
  *                         weekViewTooltip2MonthesFormat: "{0:MMMM d} - {1:MMMM d}",
  *                         dayViewTooltipFormat: "{0:dddd, MMMM d}"
  *                     }
  *                 });
  */
wijdatepager_options.prototype.localization = null;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>The first day of the week (from 0 to 6).  Sunday is 0, Monday is 1, and so on.</p>
  * @field 
  * @type {number}
  * @option
  */
wijdatepager_options.prototype.firstDayOfWeek = 0;
/** <p class='defaultValue'>Default value: 6</p>
  * <p>The count of date group displayed in the pager.</p>
  * @field 
  * @type {Number}
  * @option
  */
wijdatepager_options.prototype.maxDateGroup = 6;
/** <p class='defaultValue'>Default value: null</p>
  * <p>The selected date.</p>
  * @field 
  * @type {Date}
  * @option
  */
wijdatepager_options.prototype.selectedDate = null;
/** <p class='defaultValue'>Default value: 'day'</p>
  * <p>The active view type.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Possible values are: day, week, month, custom.
  */
wijdatepager_options.prototype.viewType = 'day';
/** <p>The options of custom view.</p>
  * @field 
  * @type {Object}
  * @option 
  * @remarks
  * The options of the custom view. options object fields:
  * unit - String, the time unit of custom view possible values are "day", "week", "month", "year";
  * count - number, the count of time span, depends on the unit;
  * Default:
  * {
  * unit: "day", 
  * count: 1
  * }
  * @example
  * $("#datepager").wijdatepager({ 
  *         customViewOptions: {
  *             unit: "day",
  *             count: 2
  *         }
  *     });
  */
wijdatepager_options.prototype.customViewOptions = null;
/** <p class='defaultValue'>Default value: 'right'</p>
  * <p>Gets or sets the text for the 'next' button's ToolTip.</p>
  * @field 
  * @type {string}
  * @option
  */
wijdatepager_options.prototype.nextTooltip = 'right';
/** <p class='defaultValue'>Default value: 'left'</p>
  * <p>Gets or sets the text for the 'previous' button's ToolTip.</p>
  * @field 
  * @type {string}
  * @option
  */
wijdatepager_options.prototype.prevTooltip = 'left';
/** Occurs when the selectedDate option has been changed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.datepager.ISelectedDateChangedEventArgs} data Information about an event
  */
wijdatepager_options.prototype.selectedDateChanged = null;
wijmo.datepager.wijdatepager.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijdatepager_options());
$.widget("wijmo.wijdatepager", $.wijmo.widget, wijmo.datepager.wijdatepager.prototype);
/** Contains information about wijdatepager.selectedDateChanged event
  * @interface ISelectedDateChangedEventArgs
  * @namespace wijmo.datepager
  */
wijmo.datepager.ISelectedDateChangedEventArgs = function () {};
/** <p>The new selectedDate option value.</p>
  * @field
  */
wijmo.datepager.ISelectedDateChangedEventArgs.prototype.selectedDate = null;
})()
})()
