var wijmo = wijmo || {};

(function () {
wijmo.data = wijmo.data || {};

(function () {
wijmo.data.filtering = wijmo.data.filtering || {};

(function () {
wijmo.data.filtering.extended = wijmo.data.filtering.extended || {};

(function () {
/** @interface IPropertyPredicate
  * @namespace wijmo.data.filtering.extended
  * @extends wijmo.data.IFilterDescriptor
  */
wijmo.data.filtering.extended.IPropertyPredicate = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.data.filtering.extended.IPropertyPredicate.prototype.property = null;
typeof wijmo.data.IFilterDescriptor != 'undefined' && $.extend(wijmo.data.filtering.extended.IPropertyPredicate.prototype, wijmo.data.IFilterDescriptor.prototype);
})()
})()
})()
})()
