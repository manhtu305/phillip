var wijmo = wijmo || {};

(function () {
wijmo.tooltip = wijmo.tooltip || {};

(function () {
/** @class wijtooltip
  * @widget 
  * @namespace jQuery.wijmo.tooltip
  * @extends wijmo.wijmoWidget
  */
wijmo.tooltip.wijtooltip = function () {};
wijmo.tooltip.wijtooltip.prototype = new wijmo.wijmoWidget();
/** Removes the wijtooltip functionality completely.
  * This returns the element back to its pre-init state.
  */
wijmo.tooltip.wijtooltip.prototype.destroy = function () {};
/** Returns the wijtooltip element.
  * @returns {JQuery}
  */
wijmo.tooltip.wijtooltip.prototype.widget = function () {};
/** Shows the tooltip */
wijmo.tooltip.wijtooltip.prototype.show = function () {};
/** Shows the tooltip at the specified position
  * @param {object} point A point value that indicates the position that tooltip will be shown.
  * @example
  * //Shows the tooltip at point {x: 100, y: 120}.
  * $("#tooltip").wijtooltip("showAt", {x:100, y:120});
  */
wijmo.tooltip.wijtooltip.prototype.showAt = function (point) {};
/** Hides the tooltip. */
wijmo.tooltip.wijtooltip.prototype.hide = function () {};

/** @class */
var wijtooltip_options = function () {};
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Sets the tooltip's content.</p>
  * @field 
  * @type {string|function}
  * @option 
  * @remarks
  * The value can be a string, html code, or a function. 
  * If it is a function, then the content will be 
  * the function's return value.
  * @example
  * //Set tooltip's content to "my content".
  * $(".selector").wijtooltip("option", "content", "my content").
  */
wijtooltip_options.prototype.content = "";
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Specifies a value that sets the tooltip's title.</p>
  * @field 
  * @type {string|function}
  * @option 
  * @remarks
  * The value can be a string, html code, or a function. 
  * If it is a function, then the title will be the function's return value.
  * @example
  * //Set tooltip's title to "my title".
  * $(".selector").wijtooltip("option", "title", "my title");
  */
wijtooltip_options.prototype.title = "";
/** <p class='defaultValue'>Default value: 'auto'</p>
  * <p>Determines how to close the tooltip. Behaviors include auto or sticky.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Options: "auto", "none" and "sticky".
  */
wijtooltip_options.prototype.closeBehavior = 'auto';
/** <p class='defaultValue'>Default value: false</p>
  * <p>If true, then the tooltip moves with the mouse.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtooltip_options.prototype.mouseTrailing = false;
/** <p class='defaultValue'>Default value: 'hover'</p>
  * <p>Sets the event that will cause the tooltip to appear.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Options: "hover", "click", "focus", "rightClick", "custom".
  */
wijtooltip_options.prototype.triggers = 'hover';
/** <p>Sets the tooltip's position mode in relation to the 'relativeTo', 
  * 'offsetX', and 'offsetY' properties.</p>
  * @field 
  * @option 
  * @remarks
  * See jQuery ui position for more details
  * http://api.jqueryui.com/position/ .
  */
wijtooltip_options.prototype.position = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether to show the callout element.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtooltip_options.prototype.showCallout = true;
/** <p>Sets the showAnimation and hideAnimation options if they are  not specified individually.</p>
  * @field 
  * @option 
  * @remarks
  * This should be an object value. Possible values include:
  * 'animated', 'duration', and 'easing'. You can create custom easing
  * animations using jQuery UI Easings.
  * This property works with jQuery animation.
  */
wijtooltip_options.prototype.animation = null;
/** <p>Determines the animation effect that will be shown.</p>
  * @field 
  * @option 
  * @remarks
  * This should be an object value. Possible values include:
  * 'animated', 'duration', and 'easing'. You can create custom easing
  * animations using jQuery UI Easings.
  * This property works with jQuery animation.
  */
wijtooltip_options.prototype.showAnimation = null;
/** <p>Determines whether the animation effect can be seen.</p>
  * @field 
  * @option 
  * @remarks
  * This should be an object value. Possible values include:
  * 'animated', 'duration', and 'easing'. You can create custom easing
  * animations using jQuery UI Easings.
  * This property works with jQuery animation.
  */
wijtooltip_options.prototype.hideAnimation = null;
/** <p class='defaultValue'>Default value: 150</p>
  * <p>Determines the length of the delay before the tooltip appears.</p>
  * @field 
  * @type {number}
  * @option
  */
wijtooltip_options.prototype.showDelay = 150;
/** <p class='defaultValue'>Default value: 150</p>
  * <p>Determines the length of the delay before the tooltip disappears.</p>
  * @field 
  * @type {number}
  * @option
  */
wijtooltip_options.prototype.hideDelay = 150;
/** <p>Sets the callout's offset changing animation.</p>
  * @field 
  * @option 
  * @remarks
  * This should be an object value. Possible values include:
  * 'disabled', 'duration', and 'easing'.
  */
wijtooltip_options.prototype.calloutAnimation = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines the callout's class style. 
  * If true, then the callout triangle is filled.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtooltip_options.prototype.calloutFilled = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that indicates whether to show the modal tooltip.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtooltip_options.prototype.modal = false;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Determines which group the tooltip belongs to.</p>
  * @field 
  * @type {string}
  * @option
  */
wijtooltip_options.prototype.group = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A function that defines a callback when AJAX is uesd to set the
  * content property.</p>
  * @field 
  * @type {function}
  * @option 
  * @remarks
  * In AJAX's complete callback method, the user set the callback 
  * data to the content option.
  */
wijtooltip_options.prototype.ajaxCallback = null;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>A value that indicates whether to set user-defined class.</p>
  * @field 
  * @type {string}
  * @option
  */
wijtooltip_options.prototype.cssClass = "";
/** <p class='defaultValue'>Default value: null</p>
  * <p>Determines the width of the tooltip.</p>
  * @field 
  * @type {object}
  * @option
  */
wijtooltip_options.prototype.controlwidth = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Determines the height of the tooltip.</p>
  * @field 
  * @type {object}
  * @option
  */
wijtooltip_options.prototype.controlheight = null;
/** Trigegred before showing the tooltip. 
  * Use return false; to cancel the event and stop showing the tooltip.
  * @event 
  * @param event jQuery.Event object
  * @param ui The wijtooltip widget.
  */
wijtooltip_options.prototype.showing = null;
/** Triggered once the tooltip has shown.
  * @event 
  * @param event jQuery.Event object
  * @param ui The wijtooltip widget.
  */
wijtooltip_options.prototype.shown = null;
/** Triggered before hiding the tooltip.If data.cancel is 
  * set to true, then the tooltip is no longer hidden
  * @event 
  * @param event jQuery.Event object
  * @param ui The wijtooltip widget.
  */
wijtooltip_options.prototype.hiding = null;
/** Triggered once the tooltip is hidden.
  * @event 
  * @param event jQuery.Event object
  * @param ui The wijtooltip widget.
  */
wijtooltip_options.prototype.hidden = null;
wijmo.tooltip.wijtooltip.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijtooltip_options());
$.widget("wijmo.wijtooltip", $.wijmo.widget, wijmo.tooltip.wijtooltip.prototype);
})()
})()
