var wijmo = wijmo || {};

(function () {
wijmo.maps = wijmo.maps || {};

(function () {
/** @class wijmaps
  * @widget 
  * @namespace jQuery.wijmo.maps
  * @extends wijmo.wijmoWidget
  */
wijmo.maps.wijmaps = function () {};
wijmo.maps.wijmaps.prototype = new wijmo.wijmoWidget();
/** Refresh the map layers. Usually used when the items of the layer data changed. */
wijmo.maps.wijmaps.prototype.refreshLayers = function () {};
/** Convert the point from screen unit (pixel) to  geographic unit (longitude and latitude).
  * @param {IPoint} position The point to convert.
  * @returns {wijmo.maps.IPoint}
  */
wijmo.maps.wijmaps.prototype.viewToGeographic = function (position) {};
/** Convert the point from geographic unit (longitude and latitude) to screen unit (pixel).
  * @param {IPoint} position The point to convert.
  * @returns {wijmo.maps.IPoint}
  */
wijmo.maps.wijmaps.prototype.geographicToView = function (position) {};
/** Convert the point from screen unit (pixel) to logic unit (percentage).
  * @param {IPoint} position The point to convert.
  * @returns {wijmo.maps.IPoint}
  */
wijmo.maps.wijmaps.prototype.viewToLogic = function (position) {};
/** Convert the point from logic unit (percentage) to screen unit (pixel).
  * @param {IPoint} position The point to convert.
  * @returns {wijmo.maps.IPoint}
  */
wijmo.maps.wijmaps.prototype.logicToView = function (position) {};
/** Calculate the distance between two points.
  * @param {Point} lonLat1 The coordinate of first point, in geographic unit.
  * @param {Point} longLat2 The coordinate of second point, in geographic unit.
  * @returns {number} The distance between to points, in meters.
  */
wijmo.maps.wijmaps.prototype.distance = function (lonLat1, longLat2) {};
/** Removes the wijmaps functionality completely. This will return the element back to its pre-init state. */
wijmo.maps.wijmaps.prototype.destroy = function () {};

/** @class */
var wijmaps_options = function () {};
/** <p class='defaultValue'>Default value: 'bingMapsRoadSource'</p>
  * <p>The source of the wijmaps tile layer. Wijmaps provides some built in sources:
  * &lt;ul&gt;
  * &lt;li&gt;bingMapsRoadSource&lt;/li&gt;
  * &lt;li&gt;bingMapsAerialSource&lt;/li&gt;
  * &lt;li&gt;bingMapsHybridSource&lt;/li&gt;
  * &lt;/ul&gt;
  * Set to null if want to hide the tile layer.&lt;br&gt;
  * Set to a IMultiScaleTileSource object if want to use another map tile server.</p>
  * @field 
  * @type {string}
  * @option
  */
wijmaps_options.prototype.source = 'bingMapsRoadSource';
/** <p class='defaultValue'>Default value: ""</p>
  * <p>The bing mpas key of the bing maps source.</p>
  * @field 
  * @type {string}
  * @option
  */
wijmaps_options.prototype.bingMapsKey = "";
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IPoint.html'>wijmo.maps.IPoint</a></p>
  * <p>The center of wijmaps view port, in geographic unit.</p>
  * @field 
  * @type {wijmo.maps.IPoint}
  * @option
  */
wijmaps_options.prototype.center = null;
/** <p class='defaultValue'>Default value: 1</p>
  * <p>The current zoom of the wijmaps.</p>
  * @field 
  * @type {number}
  * @option
  */
wijmaps_options.prototype.zoom = 1;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determine whether show the tools layer.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijmaps_options.prototype.showTools = true;
/** <p>The target zoom of the wijmaps to scale.</p>
  * @field 
  * @type {number}
  * @option
  */
wijmaps_options.prototype.targetZoom = null;
/** <p class='defaultValue'>Default value: 0.3</p>
  * <p>The speed for scaling the wijmaps from current zoom to target zoom.</p>
  * @field 
  * @type {number}
  * @option
  */
wijmaps_options.prototype.targetZoomSpeed = 0.3;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IPoint.html'>wijmo.maps.IPoint</a></p>
  * <p>The targer center of the wijmaps to translate.</p>
  * @field 
  * @type {wijmo.maps.IPoint}
  * @option
  */
wijmaps_options.prototype.targetCenter = null;
/** <p class='defaultValue'>Default value: 0.3</p>
  * <p>The speed for translate the wijmaps from current center to target center.</p>
  * @field 
  * @type {number}
  * @option
  */
wijmaps_options.prototype.targetCenterSpeed = 0.3;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.ILayerOptions.html'>wijmo.maps.ILayerOptions[]</a></p>
  * <p class='defaultValue'>Default value: []</p>
  * <p>Defines the array of layers append to the wijmaps.</p>
  * @field 
  * @type {wijmo.maps.ILayerOptions[]}
  * @option
  */
wijmaps_options.prototype.layers = [];
/** This is a callback function called after the zoom of the wijmaps changed.
  * @event 
  * @param {jQuery.Event} event A jQuery Event object.
  * @param {IZoomChangedEventData} data The data contains zoom relates to this event.
  */
wijmaps_options.prototype.zoomChanged = null;
/** This is a callback function called after the target zoom of the wijmaps changed.
  * @event 
  * @param {jQuery.Event} event A jQuery Event object.
  * @param {ITargetZoomChangedEventData} data The data contains targetZoom relates to this event.
  */
wijmaps_options.prototype.targetZoomChanged = null;
/** This is a callback function called after the center of the wijmaps changed.
  * @event 
  * @param {jQuery.Event} event A jQuery Event object.
  * @param {ICenterChangedEventData} data The data contains center relates to this event.
  */
wijmaps_options.prototype.centerChanged = null;
/** This is a callback function called after the target center of the wijmaps changed.
  * @event 
  * @param {jQuery.Event} event A jQuery Event object.
  * @param {ITargetCenterChangedEventData} data The data contains targetCenter relates to this event.
  */
wijmaps_options.prototype.targetCenterChanged = null;
wijmo.maps.wijmaps.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijmaps_options());
$.widget("wijmo.wijmaps", $.wijmo.widget, wijmo.maps.wijmaps.prototype);
/** The options of the wijmaps widget.
  * @class wijmaps_options
  * @namespace wijmo.maps
  */
wijmo.maps.wijmaps_options = function () {};
/** The data relates to the zoomChanged event.
  * @interface IZoomChangedEventData
  * @namespace wijmo.maps
  */
wijmo.maps.IZoomChangedEventData = function () {};
/** <p>The zoom changed.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.IZoomChangedEventData.prototype.zoom = null;
/** The data relates to the targetZoomChanged event.
  * @interface ITargetZoomChangedEventData
  * @namespace wijmo.maps
  */
wijmo.maps.ITargetZoomChangedEventData = function () {};
/** <p>The target zoom changed.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.ITargetZoomChangedEventData.prototype.targetZoom = null;
/** The data relates to the centerChanged event.
  * @interface ICenterChangedEventData
  * @namespace wijmo.maps
  */
wijmo.maps.ICenterChangedEventData = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IPoint.html'>wijmo.maps.IPoint</a></p>
  * <p>The center changed.</p>
  * @field 
  * @type {wijmo.maps.IPoint}
  */
wijmo.maps.ICenterChangedEventData.prototype.center = null;
/** The data relates to the targetCenterChanged event.
  * @interface ITargetCenterChangedEventData
  * @namespace wijmo.maps
  */
wijmo.maps.ITargetCenterChangedEventData = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IPoint.html'>wijmo.maps.IPoint</a></p>
  * <p>The target center changed.</p>
  * @field 
  * @type {wijmo.maps.IPoint}
  */
wijmo.maps.ITargetCenterChangedEventData.prototype.targetCenter = null;
})()
})()
