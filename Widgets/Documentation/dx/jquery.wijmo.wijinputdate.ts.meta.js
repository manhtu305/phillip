var wijmo = wijmo || {};

(function () {
wijmo.input = wijmo.input || {};

(function () {
/** @class wijinputdate
  * @widget 
  * @namespace jQuery.wijmo.input
  * @extends wijmo.input.wijinputcore
  */
wijmo.input.wijinputdate = function () {};
wijmo.input.wijinputdate.prototype = new wijmo.input.wijinputcore();
/** Gets the text value when the container form is posted back to server. */
wijmo.input.wijinputdate.prototype.getPostValue = function () {};
/** Performs spin up by the active field and increment value. */
wijmo.input.wijinputdate.prototype.spinUp = function () {};
/** Performs spin down by the active field and increment value. */
wijmo.input.wijinputdate.prototype.spinDown = function () {};
/** Open the dropdown window. */
wijmo.input.wijinputdate.prototype.drop = function () {};
/** Set the focus to the widget. */
wijmo.input.wijinputdate.prototype.focus = function () {};
/** Determines whether the date is a null value.
  * @returns {bool}
  */
wijmo.input.wijinputdate.prototype.isDateNull = function () {};
/** Selects a range of text in the widget.
  * @param {number} start Start of the range.
  * @param {number} end End of the range.
  * @example
  * // Select first two symbols in a wijinputdate
  * $(".selector").wijinputdate("selectText", 0, 2);
  */
wijmo.input.wijinputdate.prototype.selectText = function (start, end) {};

/** @class */
var wijinputdate_options = function () {};
/** <p>undefined</p>
  * @field 
  * @option
  */
wijinputdate_options.prototype.wijCSS = null;
/** <p>Determines the initial date value shown for the wijdateinput widget.</p>
  * @field 
  * @type {Date}
  * @option
  */
wijinputdate_options.prototype.date = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Determines the earliest, or minimum, date that can be entered.</p>
  * @field 
  * @type {Date}
  * @option
  */
wijinputdate_options.prototype.minDate = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Determines the latest, or maximum date, that can be entered.</p>
  * @field 
  * @type {Date}
  * @option
  */
wijinputdate_options.prototype.maxDate = null;
/** <p class='defaultValue'>Default value: 'd'</p>
  * <p>The format pattern to display the date value when control got focus.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * wijinputdate supports two types of formats:  &lt;br /&gt;
  * Standard Format and Custom Format. &lt;br /&gt;
  * &lt;br /&gt;
  * A standard date and time format string uses a single format specifier  &lt;br /&gt;
  * to define the text representation of a date and time value. &lt;br /&gt;
  * &lt;br /&gt;
  * Possible values for Standard Format are: &lt;br /&gt;
  * "d": ShortDatePattern &lt;br /&gt;
  * "D": LongDatePattern &lt;br /&gt;
  * "f": Full date and time (long date and short time)  &lt;br /&gt;
  * "F": FullDateTimePattern  &lt;br /&gt;
  * "g": General (short date and short time)  &lt;br /&gt;
  * "G": General (short date and long time)  &lt;br /&gt;
  * "m": MonthDayPattern  &lt;br /&gt;
  * "M": monthDayPattern  &lt;br /&gt;
  * "r": RFC1123Pattern   &lt;br /&gt;
  * "R": RFC1123Pattern   &lt;br /&gt;
  * "s": SortableDateTimePattern   &lt;br /&gt;
  * "t": shortTimePattern   &lt;br /&gt;
  * "T": LongTimePattern   &lt;br /&gt;
  * "u": UniversalSortableDateTimePattern  &lt;br /&gt;
  * "U": Full date and time (long date and long time) using universal time  &lt;br /&gt;
  * "y": YearMonthPattern   &lt;br /&gt;
  * "Y": yearMonthPattern   &lt;br /&gt;
  * Any date and time format string that contains more than one character,  &lt;br/&gt;
  * including white space, is interpreted as a custom date and time format 
  * string. For example:    &lt;br/&gt;
  * "mmm-dd-yyyy", "mmmm d, yyyy", "mm/dd/yyyy", "d-mmm-yyyy",  
  * "ddd, mmmm dd, yyyy" etc.   &lt;br/&gt;
  * Below are the custom date and time format specifiers:  &lt;br/&gt;
  * &lt;br/&gt;
  * "d": The day of the month, from 1 through 31.   &lt;br /&gt;
  * "dd": The day of the month, from 01 through 31.  &lt;br /&gt;
  * "ddd": The abbreviated name of the day of the week.  &lt;br /&gt;
  * "dddd": The full name of the day of the week.   &lt;br /&gt;
  * "m": The minute, from 0 through 59.   &lt;br /&gt;
  * "mm": The minute, from 00 through 59.  &lt;br /&gt;
  * "M": The month, from 1 through 12.  &lt;br /&gt;
  * "MM": The month, from 01 through 12.  &lt;br /&gt;
  * "MMM": The abbreviated name of the month.  &lt;br /&gt;
  * "MMMM": The full name of the month.  &lt;br /&gt;
  * "y": The year, from 0 to 99.   &lt;br /&gt;
  * "yy": The year, from 00 to 99   &lt;br /&gt;
  * "yyy": The year, with a minimum of three digits.  &lt;br /&gt;
  * "yyyy": The year as a four-digit number   &lt;br /&gt;
  * "h": The hour, using a 12-hour clock from 1 to 12.   &lt;br /&gt;
  * "hh": The hour, using a 12-hour clock from 01 to 12.   &lt;br /&gt;
  * "H": The hour, using a 24-hour clock from 0 to 23.   &lt;br /&gt;
  * "HH": The hour, using a 24-hour clock from 00 to 23.  &lt;br /&gt;
  * "s": The second, from 0 through 59.   &lt;br /&gt;
  * "ss": The second, from 00 through 59.   &lt;br /&gt;
  * "t": The first character of the AM/PM designator.   &lt;br /&gt;
  * "tt": The AM/PM designator.    &lt;br /&gt;
  */
wijinputdate_options.prototype.dateFormat = 'd';
/** <p class='defaultValue'>Default value: ""</p>
  * <p>The format pattern to display the date value when control lost focus.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * wijinputdate supports two types of formats:   &lt;br /&gt;
  * Standard Format and Custom Format.   &lt;br /&gt;
  * &lt;br /&gt;
  * A standard date and time format string uses a single format specifier &lt;br /&gt;
  * to define the text representation of a date and time value.   &lt;br /&gt;
  * Possible values for Standard Format are:  &lt;br /&gt;
  * "d": ShortDatePattern  &lt;br /&gt;
  * "D": LongDatePattern  &lt;br /&gt;
  * "f": Full date and time (long date and short time) &lt;br /&gt;
  * "F": FullDateTimePattern  &lt;br /&gt;
  * "g": General (short date and short time)  &lt;br /&gt;
  * "G": General (short date and long time)  &lt;br /&gt;
  * "m": MonthDayPattern   &lt;br /&gt;
  * "M": monthDayPattern  &lt;br /&gt;
  * "r": RFC1123Pattern   &lt;br /&gt;
  * "R": RFC1123Pattern   &lt;br /&gt;
  * "s": SortableDateTimePattern  &lt;br /&gt;
  * "t": shortTimePattern  &lt;br /&gt;
  * "T": LongTimePattern   &lt;br /&gt;
  * "u": UniversalSortableDateTimePattern  &lt;br /&gt;
  * "U": Full date and time (long date and long time) using universal time   &lt;br /&gt;
  * "y": YearMonthPattern  &lt;br /&gt;
  * "Y": yearMonthPattern  &lt;br /&gt;
  * Any date and time format string that contains more than one character, &lt;br /&gt;
  * including white space, is interpreted as a custom date and time format   &lt;br /&gt;
  * string. For example: &lt;br /&gt;
  * "mmm-dd-yyyy", "mmmm d, yyyy", "mm/dd/yyyy", "d-mmm-yyyy",  
  * "ddd, mmmm dd, yyyy" etc.  &lt;br /&gt;
  * Below are the custom date and time format specifiers:  &lt;br /&gt;
  * "d": The day of the month, from 1 through 31.   &lt;br /&gt;
  * "dd": The day of the month, from 01 through 31.   &lt;br /&gt;
  * "ddd": The abbreviated name of the day of the week.  &lt;br /&gt;
  * "dddd": The full name of the day of the week.  &lt;br /&gt;
  * "m": The minute, from 0 through 59.   &lt;br /&gt;
  * "mm": The minute, from 00 through 59.  &lt;br /&gt;
  * "M": The month, from 1 through 12.  &lt;br /&gt;
  * "MM": The month, from 01 through 12.   &lt;br /&gt;
  * "MMM": The abbreviated name of the month.  &lt;br /&gt;
  * "MMMM": The full name of the month. &lt;br /&gt;
  * "y": The year, from 0 to 99.  &lt;br /&gt;
  * "yy": The year, from 00 to 99  &lt;br /&gt;
  * "yyy": The year, with a minimum of three digits.  &lt;br /&gt;
  * "yyyy": The year as a four-digit number  &lt;br /&gt;
  * "h": The hour, using a 12-hour clock from 1 to 12.  &lt;br /&gt;
  * "hh": The hour, using a 12-hour clock from 01 to 12.  &lt;br /&gt;
  * "H": The hour, using a 24-hour clock from 0 to 23.   &lt;br /&gt;
  * "HH": The hour, using a 24-hour clock from 00 to 23.  &lt;br /&gt;
  * "s": The second, from 0 through 59.   &lt;br /&gt;
  * "ss": The second, from 00 through 59.  &lt;br /&gt;
  * "t": The first character of the AM/PM designator.  &lt;br /&gt;
  * "tt": The AM/PM designator.    &lt;br /&gt;
  * "E": Display the nengo year as a single digit number when possible (first year use Japanese name).  &lt;br /&gt;
  */
wijinputdate_options.prototype.displayFormat = "";
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines string designator for hours that are "ante meridiem" (before noon).</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * The Text set in the amDesignator option is displayed in the position occupied by the keywords "tt" and "t" 
  * in the dateFormat or dipslayFormat.  &lt;br /&gt;
  * If the custom pattern includes the format pattern "tt" and the time is before noon,  
  * the value of amDesignator is displayed in place of the "tt" in the dateFormat or displayFormat pattern.   &lt;br /&gt;
  * If the custom pattern includes the format pattern "t", only the first character of amDesignator is displayed.   &lt;br /&gt;
  * If setting "tt" in format and not setting amDesignator/pmDesignator options, 
  * it will show the string getting from the specified culture.    &lt;br /&gt;
  * If not set, it will show "午前"/"午後" in Japanese culture and "AM"/"PM" in English culture.   &lt;br /&gt;
  * If setting "t" in format and not setting amDesignator/pmDesignator options, it will show the string getting from current culture.  &lt;br /&gt;
  * If not set, it will display "午" in Japanese culture and "A"/"P" in English culture.  &lt;br /&gt;
  */
wijinputdate_options.prototype.amDesignator = "";
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the string designator for hours that are "post meridiem" (after noon).</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * The Text set in the amDesignator option is displayed in the position occupied by the keywords "tt" and "t" 
  * in the dateFormat or dipslayFormat.  &lt;br /&gt;
  * If the custom pattern includes the format pattern "tt" and the time is after noon,  
  * the value of pmDesignator is displayed in place of the "tt" in the dateFormat or displayFormat pattern.   &lt;br /&gt;
  * If the custom pattern includes the format pattern "t", only the first character of pmDesignator is displayed.   &lt;br /&gt;
  * If setting "tt" in format and not setting amDesignator/pmDesignator options, 
  * it will show the string getting from the specified culture.    &lt;br /&gt;
  * If not set, it will show "午前"/"午後" in Japanese culture and "AM"/"PM" in English culture.   &lt;br /&gt;
  * If setting "t" in format and not setting amDesignator/pmDesignator options, it will show the string getting from current culture.  &lt;br /&gt;
  * If not set, it will display "午" in Japanese culture and "A"/"P" in English culture.  &lt;br /&gt;
  */
wijinputdate_options.prototype.pmDesignator = "";
/** <p class='defaultValue'>Default value: true</p>
  * <p>A boolean value determines whether to express midnight as 24:00.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijinputdate_options.prototype.midnightAs0 = true;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A boolean value determines the range of hours that can be entered in the control.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * If set this property to false,
  * the control sets the range for the hour field from 1 - 12.  If set to true
  * the range is set to 0 - 11.
  */
wijinputdate_options.prototype.hour12As0 = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determins whether or not the next control in the tab order receives 
  * the focus as soon as the control is filled at the last character.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijinputdate_options.prototype.blurOnLastChar = false;
/** <p class='defaultValue'>Default value: 'none'</p>
  * <p>Gets or set whether the focus automatically moves to the next or previous
  * tab ordering control when pressing the left, right arrow keys.</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputdate_options.prototype.blurOnLeftRightKey = 'none';
/** <p class='defaultValue'>Default value: 'field'</p>
  * <p>Gets or sets whether the tab key moves the focus between controls or between 
  * fields within the control, possible values is "field", "control".</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * The caret moves to the next field when this property is "field". 
  * If the caret is in the last field, the input focus leaves this control when pressing the TAB key. 
  * If the value is "control", the behavior is similar to the standard control.
  */
wijinputdate_options.prototype.tabAction = 'field';
/** <p class='defaultValue'>Default value: 1950</p>
  * <p>Determines how the control should interpret 2-digits year inputted in year field.
  * when the "smartInputMode" option is set to true.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * For example, if "startYear" is set to 1950 (the default value), and "smartInputMode" is true.   &lt;br /&gt;
  * Enter 2-digit year value which is greater than 50 [e.g., 88]  &lt;br /&gt;
  * ‘1988’ displays in year part.  &lt;br /&gt;
  * Enter 2-digit year value which is less than 50 [e.g., 12]  &lt;br /&gt;
  * ‘2012’ displays in year part.  &lt;br /&gt;
  */
wijinputdate_options.prototype.startYear = 1950;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the character that appears when the widget has focus but no input has been entered.</p>
  * @field 
  * @type {string}
  * @option
  */
wijinputdate_options.prototype.promptChar = "";
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether the control should interpret 2-digits year inputted in year field.  
  * using the value provided in the "startYear" option.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * For example, when "smartInputMode" is false (the default value), and "startYear" is 1950. &lt;br /&gt;
  * Enter 2-digit year value which is greater than 50 [e.g., 88]  &lt;br /&gt;
  * ‘0088’ displays in year part.    &lt;br /&gt;
  * Enter 2-digit year value which is less than 50 [e.g., 12]  &lt;br /&gt;
  * ‘0012’ displays in year part.  &lt;br /&gt;
  * Set "smartInputMode" to true.  &lt;br /&gt;
  * Enter 2-digit year value which is greater than 50 [e.g., 88]  &lt;br /&gt;
  * ‘1988’ displays in year part when smartInputMode is true.   &lt;br /&gt;
  * Enter 2-digit year value which is less than 50 [e.g., 12]  &lt;br /&gt;
  * ‘2012’ displays in year part. &lt;br /&gt;
  */
wijinputdate_options.prototype.smartInputMode = false;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Determines the active field index.</p>
  * @field 
  * @type {number}
  * @option
  */
wijinputdate_options.prototype.activeField = 0;
/** <p class='defaultValue'>Default value: 800</p>
  * <p>Determines the time span, in milliseconds, 
  * between two input intentions.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * when press a keyboard, and the widget will delay a time and then handle 
  * the next keyboard press. Use this option to control the speed of the key press.
  */
wijinputdate_options.prototype.keyDelay = 800;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether to automatically moves to the next field.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * For example, if user want input the '2012-9-20' in inputdate widget, 
  * if this option's value is true, when user type '2012' in textbox,  
  * it will auto focus in next field, user can type '9' in second field, 
  * if this option's value is false, user want to type '9' in second field, 
  * they should focus the second field by manual.
  */
wijinputdate_options.prototype.autoNextField = true;
/** <p>Detemines the popup position of a calendar. 
  * See jQuery.ui.position for position options.</p>
  * @field 
  * @option 
  * @example
  * // In the following example, the Y offset between the popup position and wijinputdate is 10 pixel.
  * $("#textbox1").wijinputdate({
  *     popupPosition: { offset: '0 10' },
  *     comboItems: [{ label: "first Day", value: new Date(2013, 1, 1) },
  *         { label: "second day", value: new Date(2013, 3, 3) },
  *         { label: "third day", value: new Date(2013, 4, 5) }]
  * });
  */
wijinputdate_options.prototype.popupPosition = null;
/** <p class='defaultValue'>Default value: 'field'</p>
  * <p>Gets or sets whether to highlight the control's Text on receiving input focus. 
  * possible values is "field", "all" .</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#textbox1").wijinputdate({
  *     highlightText: "field"
  * });
  */
wijinputdate_options.prototype.highlightText = 'field';
/** <p class='defaultValue'>Default value: 1</p>
  * <p>Determines how much to increase/decrease the active field when performing spin on the the active field.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * $("#textbox1").wijinputdate({
  *     increment: 2
  * });
  */
wijinputdate_options.prototype.increment = 1;
/** <p class='defaultValue'>Default value: 'auto'</p>
  * <p>Determines the input method setting of widget.
  * Possible values are: 'auto', 'active', 'inactive', 'disabled'</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * This property only take effect on IE and firefox browser.
  */
wijinputdate_options.prototype.imeMode = 'auto';
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether dropdown button is displayed.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijinputdate_options.prototype.showDropDownButton = true;
/** <p>Determines whether dropdown button is displayed.</p>
  * @field 
  * @option
  */
wijinputdate_options.prototype.showTrigger = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.input.DatePickersClass.html'>wijmo.input.DatePickersClass</a></p>
  * <p>An object contains the settings for the dropdown list.</p>
  * @field 
  * @type {wijmo.input.DatePickersClass}
  * @option 
  * @example
  * //  The following code show the dropdown calendar, dropdown list, 
  * //  and set the dorpdown window's width to 100px, height to 30px;
  * $(".selector").wijinputdate({
  *     pickers: {
  *         calendar: {},
  *         list: [{ label: 'item1', value: 1 }],
  *         width: 100,
  *         height: 30
  *     }
  * });
  * // The following code show the dropdown date picker and dropdown time picker,
  * // also it sets the time picker's format ot "hh hh:mm".
  * $(".selector").wijinputdate({
  *     pickers: {
  *         datePicker: {},
  *         timePicker: { format: "tt hh:mm" }
  *     }
  * });
  * // The following code shows the drpdown date picker and dropdown time picker.
  * // also it sets the date picker's format to "yyyy/MMMM/dd".
  * $(".selector").wijinputdate({
  *     pickers: {
  *         datePicker: { format: "yyyy/MMMM/dd" },
  *         timePicker: {}
  *     }
  * });
  */
wijinputdate_options.prototype.pickers = null;
/** The afterSlide event handler. 
  * A function called after the calendar view slided to another month.
  * Cancellable.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijinputdate_options.prototype.afterSlide = null;
/** The dateChanged event handler. 
  * A function called when the date of the input is changed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.input.IDateChangedEventArgs} data Information about an event
  */
wijinputdate_options.prototype.dateChanged = null;
/** The spinUp event handler.
  * A function called when spin up event fired.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijinputdate_options.prototype.spinUp = null;
/** The spinDown event handler.
  * A function called when spin down event fired.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijinputdate_options.prototype.spinDown = null;
/** The valueBoundsExceeded event hander.
  * A function called when the valueBoundExceeded event fired.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijinputdate_options.prototype.valueBoundsExceeded = null;
wijmo.input.wijinputdate.prototype.options = $.extend({}, true, wijmo.input.wijinputcore.prototype.options, new wijinputdate_options());
$.widget("wijmo.wijinputdate", $.wijmo.wijinputcore, wijmo.input.wijinputdate.prototype);
/** @interface DatePickersClass
  * @namespace wijmo.input
  */
wijmo.input.DatePickersClass = function () {};
/** <p>Determines the width of the dropdown window.</p>
  * @field 
  * @type {number}
  */
wijmo.input.DatePickersClass.prototype.width = null;
/** <p>Determines the height of the dropdown window.</p>
  * @field 
  * @type {number}
  */
wijmo.input.DatePickersClass.prototype.height = null;
/** <p>Contains an array of data items used to populate the dropdown list.
  * The array item can be "string", or a an oject contains two property "label" and "value";
  * The label property is string, used to display on the dropdown list.
  * The value proeprty is date object, used to assign the value to the wijinputdate widget.</p>
  * @field
  */
wijmo.input.DatePickersClass.prototype.list = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.input.ICalendar.html'>wijmo.input.ICalendar</a></p>
  * <p>Determines the settings of the calendar.
  * If this property's value is null, the calendar doesn't show.
  * But if there is no picker show, then the calendar will still show.</p>
  * @field 
  * @type {wijmo.input.ICalendar}
  */
wijmo.input.DatePickersClass.prototype.calendar = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.input.IPicker.html'>wijmo.input.IPicker</a></p>
  * <p>Determines the settings of the date picker.
  * If this property's value is null, the date picker doesn't show.</p>
  * @field 
  * @type {wijmo.input.IPicker}
  */
wijmo.input.DatePickersClass.prototype.datePicker = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.input.IPicker.html'>wijmo.input.IPicker</a></p>
  * <p>Determines the settings of the time picker.
  * If this property's value is null, the time picker doesn't show.</p>
  * @field 
  * @type {wijmo.input.IPicker}
  */
wijmo.input.DatePickersClass.prototype.timePicker = null;
/** @interface ICalendar
  * @namespace wijmo.input
  */
wijmo.input.ICalendar = function () {};
/** <p>Determines whether users can change the view to month/year/decade while clicking on the calendar title.</p>
  * @field 
  * @type {boolean}
  */
wijmo.input.ICalendar.prototype.allowQuickPick = null;
/** <p>Determines the display type of navigation buttons.
  * Possible values are "default", "quick", "none".
  * The default value is "default".</p>
  * @field 
  * @type {string}
  */
wijmo.input.ICalendar.prototype.navButtons = null;
/** <p>Determines the text for the 'next' button's ToolTip.</p>
  * @field 
  * @type {string}
  */
wijmo.input.ICalendar.prototype.nextTooltip = null;
/** <p>Determines the text for the 'previous' button's ToolTip.</p>
  * @field 
  * @type {string}
  */
wijmo.input.ICalendar.prototype.prevTooltip = null;
/** <p>Determines whether to add zeroes to days with only one digit.</p>
  * @field 
  * @type {boolean}
  */
wijmo.input.ICalendar.prototype.showDayPadding = null;
/** <p>Determines whether to display the days of the next and/or previous month.</p>
  * @field 
  * @type {boolean}
  */
wijmo.input.ICalendar.prototype.showOtherMonthDays = null;
/** <p>Determines whether to display the calendar's title.</p>
  * @field 
  * @type {boolean}
  */
wijmo.input.ICalendar.prototype.showTitle = null;
/** <p>Determines whether to display week days.</p>
  * @field 
  * @type {boolean}
  */
wijmo.input.ICalendar.prototype.showWeekDays = null;
/** <p>Determines whether to display week numbers.</p>
  * @field 
  * @type {boolean}
  */
wijmo.input.ICalendar.prototype.showWeekNumbers = null;
/** <p>Determines the format for the title text.</p>
  * @field 
  * @type {string}
  */
wijmo.input.ICalendar.prototype.titleFormat = null;
/** <p>Determines the format for the ToolTip.</p>
  * @field 
  * @type {string}
  */
wijmo.input.ICalendar.prototype.toolTipFormat = null;
/** <p>Determines the format for the week day.</p>
  * @field 
  * @type {string}
  */
wijmo.input.ICalendar.prototype.weekDayFormat = null;
/** @interface IPicker
  * @namespace wijmo.input
  */
wijmo.input.IPicker = function () {};
/** <p>Determiens the order and the text display for the picker.
  * The default value is according to culture.</p>
  * @field 
  * @type {string}
  * @remarks
  * As default, the rolls in the picker display according to culture,
  * for example, display as year, month, day in ja culture, and month, day, year in en culture.  &lt;br /&gt;
  * And, the text display on a field depends on the culture too.   &lt;br /&gt;
  * Year part's range is 100 years, it will be (current year - 50) to (current year + 50).
  * User can also customize the order and text of the rolls use the format options.
  * The picker's format must include 3 parts, for datePicker, it is year, month and day, and for timepicker, it is ampm, hour and minute. &lt;br /&gt;
  * Each part has possible values, if the format is invalid (don't include 3 parts or format is invalid),
  * the picker will display as default according to the culture.  &lt;br /&gt;
  * Currently, the following standard are supported.  &lt;br /&gt;
  * "yyyy" :  Display the year as a 4-digit number (0100 - 9999).   &lt;br /&gt;
  * "M" :  Display the month as a number without a leading zero (1 - 12).  &lt;br /&gt;
  * "MM" : Display the month as a number with a leading zero (01 - 12).  &lt;br /&gt;
  * "MMM" : Display the month defined by the DateTimeFormatInfo.AbbreviatedMonthNames() property associated with the current thread or by a specified format provider.  &lt;br /&gt;
  * "MMMM" : Display the month defined by the DateTimeFormatInfo.MonthNames() property associated with the current thread or by a specified format provider.   &lt;br /&gt;
  * "d" : Display the day as a number without a leading zero (1 - 31).  &lt;br /&gt;
  * "dd" : Display the day as a number with a leading zero (01 - 31).  &lt;br /&gt;
  * "t" : Displays the first character of the A.M./P.M. designator.  &lt;br /&gt;
  * "tt" : Displays the A.M./P.M. designator.  &lt;br /&gt;
  * "h" : Display the hour as a number without leading zeros (0 - 11) in 12 hour mode.  &lt;br /&gt;
  * "hh" : Display the hour as a number with leading zeros (00 - 11) in 12 hour mode.  &lt;br /&gt;
  * "m" : Display the minute as a number without leading zeros (0 - 59).  &lt;br /&gt;
  * "mm" : Display the minute as a number with leading zeros (00 - 59).  &lt;br /&gt;
  * @example
  * $(".selector").wijinputdate({
  * pickers: {
  *   datePicker: {format: "yyyy,MMMM,dd"}, 
  *   timePicker: {format: "tt,hh,mm"}
  * });
  */
wijmo.input.IPicker.prototype.format = null;
/** Contains information about wijinputdate.dateChanged event
  * @interface IDateChangedEventArgs
  * @namespace wijmo.input
  */
wijmo.input.IDateChangedEventArgs = function () {};
/** <p>The data with this event.</p>
  * @field 
  * @type {Date}
  */
wijmo.input.IDateChangedEventArgs.prototype.date = null;
})()
})()
