var wijmo = wijmo || {};

(function () {
wijmo.upload = wijmo.upload || {};

(function () {
/** @class wijupload
  * @widget 
  * @namespace jQuery.wijmo.upload
  * @extends wijmo.wijmoWidget
  */
wijmo.upload.wijupload = function () {};
wijmo.upload.wijupload.prototype = new wijmo.wijmoWidget();
/** Removes the wijupload functionality completely.This will return the element back to its pre - init state.
  * @example
  * $(" selector ").wijupload("destroy");
  */
wijmo.upload.wijupload.prototype.destroy = function () {};
/** Returns the.wijmo - wijupload element.
  * @returns {JQuery}
  * @example
  * $(" selector ").wijupload("widget");
  */
wijmo.upload.wijupload.prototype.widget = function () {};

/** @class */
var wijupload_options = function () {};
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Specifies the URL path of the server-side handler that handles the post request, validates file size and type, renames files, and saves the file to the server disk.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * For php: 
  * $(".selector").wijupload("option", "action", "../upload.php")
  * For asp.net: 
  * $(".selector").wijupload("option", "action", "../handlers/uploadHandler.ashx")
  */
wijupload_options.prototype.action = "";
/** <p class='defaultValue'>Default value: false</p>
  * <p>The value indicates whether to upload the file as soon as it is selected in the "Choose File to Upload" dialog box.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijupload_options.prototype.autoSubmit = false;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Specifies the maximum number of files that can be uploaded.</p>
  * @field 
  * @type {number}
  * @option
  */
wijupload_options.prototype.maximumFiles = 0;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether multiple selection is supported.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijupload_options.prototype.multiple = true;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Specifies the accept attribute of upload. This is an attribute of the file input.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * It is a filter that allows the "Choose File to Upload" dialog box to show the file list with the specified type.
  * Possible values:
  * audio/* - All sound files are accepted.
  * video/* - All video files are accepted.
  * image/* - All image files are accepted.
  * MIME_type - A valid MIME type with no parameters.
  * @example
  * $(".selector").wijupload("accept", "image/*")
  */
wijupload_options.prototype.accept = "";
/** <p class='defaultValue'>Default value: false</p>
  * <p>upload with SWFupload.swf,
  * this option is used for multiple-select in IE.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijupload_options.prototype.enableSWFUploadOnIE = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>upload with SWFupload.swf in all browsers.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijupload_options.prototype.enableSWFUpload = false;
/** Fires when user selects a file. This event can be cancelled.
  * "return false;" to cancel the event.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data An object that contains the input file.
  */
wijupload_options.prototype.change = null;
/** Fires before the file is uploaded.  This event can be cancelled. 
  * "return false;" to cancel the event.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data An object that contains the input file.
  */
wijupload_options.prototype.upload = null;
/** Fires when the uploadAll button is clicked. This event can be cancelled. 
  * "return false;" to cancel the event.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijupload_options.prototype.totalUpload = null;
/** Fires when a file is uploading.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data An object that contains the file info, loadedSize and totalSize.
  */
wijupload_options.prototype.progress = null;
/** Fires when the uploadAll button is clicked and file upload is complete.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.upload.ITotalProgressEventArgs} data Information about an event
  */
wijupload_options.prototype.totalProgress = null;
/** Fires when file upload is complete.
  * @event 
  * @param {jQuery.Event} e jQuery Event object
  * @param {object} data An object that contains the file info.
  */
wijupload_options.prototype.complete = null;
/** Fires when the uploadAll button is clicked and file upload is complete.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijupload_options.prototype.totalComplete = null;
wijmo.upload.wijupload.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijupload_options());
$.widget("wijmo.wijupload", $.wijmo.widget, wijmo.upload.wijupload.prototype);
/** Contains information about wijupload.totalProgress event
  * @interface ITotalProgressEventArgs
  * @namespace wijmo.upload
  */
wijmo.upload.ITotalProgressEventArgs = function () {};
/** <p>The size of loaded files.</p>
  * @field 
  * @type {number}
  */
wijmo.upload.ITotalProgressEventArgs.prototype.loadedSize = null;
/** <p>The size of total files.</p>
  * @field 
  * @type {number}
  */
wijmo.upload.ITotalProgressEventArgs.prototype.totalSize = null;
})()
})()
