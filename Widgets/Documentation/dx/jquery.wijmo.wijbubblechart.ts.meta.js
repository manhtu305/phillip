var wijmo = wijmo || {};

(function () {
wijmo.chart = wijmo.chart || {};

(function () {
/** @class wijbubblechart
  * @widget 
  * @namespace jQuery.wijmo.chart
  * @extends wijmo.chart.wijchartcore
  */
wijmo.chart.wijbubblechart = function () {};
wijmo.chart.wijbubblechart.prototype = new wijmo.chart.wijchartcore();
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.chart.wijbubblechart.prototype.destroy = function () {};
/** Returns the bubble which has a Raphael's object that represents bubbles for the series data with the given index.
  * @param {number} index The index of the bubble.
  * @returns {Raphael Element} The bubble object.
  */
wijmo.chart.wijbubblechart.prototype.getBubble = function (index) {};

/** @class */
var wijbubblechart_options = function () {};
/** <p class='defaultValue'>Default value: 5</p>
  * <p>The minimum bubble size represents the percentage of the diameter (or area) of the plot area.</p>
  * @field 
  * @type {number}
  * @option
  */
wijbubblechart_options.prototype.minimumSize = 5;
/** <p class='defaultValue'>Default value: 20</p>
  * <p>The maximum bubble size represents the percentage of the diameter (or area) of the plot area.</p>
  * @field 
  * @type {number}
  * @option
  */
wijbubblechart_options.prototype.maximumSize = 20;
/** <p class='defaultValue'>Default value: 'diameter'</p>
  * <p>A value that indicates how to calculate the bubble size.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Valid Values: "area" and "diameter"
  * area: Render the bubble's area based on the y1 value.
  * diameter: Render the bubble's diameter based on the y1 value.
  */
wijbubblechart_options.prototype.sizingMethod = 'diameter';
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_animation.html'>wijmo.chart.chart_animation</a></p>
  * <p>The animation option defines the animation effect and controls other aspects of the widget's animation, 
  * such as duration and easing.</p>
  * @field 
  * @type {wijmo.chart.chart_animation}
  * @option
  */
wijbubblechart_options.prototype.animation = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_animation.html'>wijmo.chart.chart_animation</a></p>
  * <p>The seriesTransition option is used to animate series in the chart when just their values change. This is
  * helpful for visually showing changes in data for the same series.</p>
  * @field 
  * @type {wijmo.chart.chart_animation}
  * @option
  */
wijbubblechart_options.prototype.seriesTransition = null;
/** <p class='defaultValue'>Default value: []</p>
  * <p>Sets the data for the chart to display.</p>
  * @field 
  * @type {array}
  * @option 
  * @example
  * $("#bubblechart").wijbubblechart({
  * seriesList: [{
  * label: "Q1",
  * legendEntry: true,
  * data: {
  * x: [1, 2, 3, 4, 5],
  * y: [12, 21, 9, 29, 30],
  * y1:[3, 5, 1, 6, 2]
  * }
  * }, {
  * label: "Q2",
  * legendEntry: true,
  * data: {
  * xy: [1, 21, 2, 10, 3, 19, 4, 31, 5, 20],
  * y1:[3, 5, 1, 6, 2]
  * }
  * }]
  * OR
  * seriesList: [{
  * label: "Q1",
  * legendEntry: true,
  * data: {
  * x: ["A", "B", "C", "D", "E"],
  * y: [12, 21, 9, 29, 30],
  * y1:[3, 5, 1, 6, 2]
  * }
  * }]
  * OR
  * seriesList: [{
  * label: "Q1",
  * legendEntry: true,
  * data: {
  * x: [new Date(1978, 0, 1), new Date(1980, 0, 1),
  * new Date(1981, 0, 1), new Date(1982, 0, 1),
  * new Date(1983, 0, 1)],
  * y: [12, 21, 9, 29, 30],
  * y1:[3, 5, 1, 6, 2]
  * }
  * }]
  * });
  */
wijbubblechart_options.prototype.seriesList = [];
/** <p class='defaultValue'>Default value: []</p>
  * <p>An array collection that contains the style applied to the chart elements. For more information on the available
  * style parameters.</p>
  * @field 
  * @type {array}
  * @option 
  * @example
  * $("#bubblechart").wijbubblechart("option", "seriesHoverStyles", {
  * seriesHoverStyles: [
  *     {fill: "rgb(255,0,0)", stroke:"none"}, 
  *     { fill: "rgb(255,125,0)", stroke: "none" }
  * ]});
  */
wijbubblechart_options.prototype.seriesHoverStyles = [];
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.bubblechart_chartlabel.html'>wijmo.chart.bubblechart_chartlabel</a></p>
  * <p>Creates a chartLabel object that defines all of the settings used to draw a label for each bubble in the chart.</p>
  * @field 
  * @type {wijmo.chart.bubblechart_chartlabel}
  * @option 
  * @example
  * // This code creates a chart with a label outside (and to the south) of each bubble with the numbers formatted 
  * // as percentages with no decimal spaces, in purple size 14 font
  * $(document).ready(function () {
  * $("#wijbubblechart").wijbubblechart({
  *     axis: {
  *         y: { text: "Number of Products" },
  *         x: { text: "Sales", annoFormatString: "C0" }
  *     },
  *     chartLabel: {
  *         chartLabelFormatString: "P0",
  *         compass: "south",
  *         position: "outside",
  *         style: { fill: "purple", "font-size": 14 }
  *     },
  *     legend: { visible: false },
  *     seriesList: [
  *        {
  *            label: "Company A Market Share",
  *            data: { y: [14], x: [12200], y1: [.15] }
  *        }, {
  *            label: "Company B Market Share",
  *            data: { y: [20], x: [60000], y1: [.23] }
  *        }, {
  *            label: "Company C Market Share",
  *            data: { y: [18], x: [24400], y1: [.1] }
  *        }]
  * });
  * });
  */
wijbubblechart_options.prototype.chartLabel = null;
/** Fires when the user clicks a mouse button.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IBubbleChartEventArgs} data Information about an event
  */
wijbubblechart_options.prototype.mouseDown = null;
/** Fires when the user releases a mouse button while the pointer is over the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IBubbleChartEventArgs} data Information about an event
  */
wijbubblechart_options.prototype.mouseUp = null;
/** Fires when the user first places the pointer over the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IBubbleChartEventArgs} data Information about an event
  */
wijbubblechart_options.prototype.mouseOver = null;
/** Fires when the user moves the pointer off of the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IBubbleChartEventArgs} data Information about an event
  */
wijbubblechart_options.prototype.mouseOut = null;
/** Fires when the user moves the mouse pointer while it is over a chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IBubbleChartEventArgs} data Information about an event
  */
wijbubblechart_options.prototype.mouseMove = null;
/** Fires when the user clicks the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IBubbleChartEventArgs} data Information about an event
  */
wijbubblechart_options.prototype.click = null;
wijmo.chart.wijbubblechart.prototype.options = $.extend({}, true, wijmo.chart.wijchartcore.prototype.options, new wijbubblechart_options());
$.widget("wijmo.wijbubblechart", $.wijmo.wijchartcore, wijmo.chart.wijbubblechart.prototype);
/** @class wijbubblechart_css
  * @namespace wijmo.chart
  * @extends wijmo.chart.wijchartcore_css
  */
wijmo.chart.wijbubblechart_css = function () {};
wijmo.chart.wijbubblechart_css.prototype = new wijmo.chart.wijchartcore_css();
/** @interface bubblechart_chartlabel
  * @namespace wijmo.chart
  */
wijmo.chart.bubblechart_chartlabel = function () {};
/** <p>A value that indicates whether to draw a label inside our outside of each bubble in the chart.</p>
  * @field 
  * @type {string}
  * @remarks
  * If set to "outside," the compass attribute sets where to draw the label.
  * The value should be "inside" or "outside".
  */
wijmo.chart.bubblechart_chartlabel.prototype.position = null;
/** <p>A value that indicates the compass position at which to draw a chart label next to each bubble 
  * when you set the position option for the label to "outside." If the position is set to "inside," 
  * this attribute is ignored.</p>
  * @field 
  * @type {string}
  * @remarks
  * The value should be "north", "east", "west" or "south"
  */
wijmo.chart.bubblechart_chartlabel.prototype.compass = null;
/** <p>The visible option indicates whether to draw a label on each bubble in the chart.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.bubblechart_chartlabel.prototype.visible = null;
/** <p>A value that indicates the style parameters to apply to the labels on each bubble in the chart.</p>
  * @field 
  * @remarks
  * Note: If you do not set any value for this style, the fallback style is chartLabelStyle.
  * The style is defined in Raphael here is the documentation: http://raphaeljs.com/reference.html#Element.attr. 
  * The style is the “attr” method’s parameters.
  */
wijmo.chart.bubblechart_chartlabel.prototype.style = null;
/** <p>Sets the numeric format of the chart labels that show the value of each bubble.</p>
  * @field 
  * @type {string}
  * @remarks
  * You can use Standard Numeric Format Strings. You can also change the style of these labels using 
  * chartLabelStyle, or to hide them
  */
wijmo.chart.bubblechart_chartlabel.prototype.chartLabelFormatString = null;
/** @interface IBubbleChartEventArgs
  * @namespace wijmo.chart
  */
wijmo.chart.IBubbleChartEventArgs = function () {};
/** <p>The Raphael object of the bubble.</p>
  * @field 
  * @type {RaphaelElement}
  */
wijmo.chart.IBubbleChartEventArgs.prototype.bubble = null;
/** <p>data of the series of the bubble.</p>
  * @field 
  * @type {Object}
  */
wijmo.chart.IBubbleChartEventArgs.prototype.data = null;
/** <p>hover style of series of the bubble.</p>
  * @field 
  * @type {Object}
  */
wijmo.chart.IBubbleChartEventArgs.prototype.hoverStyle = null;
/** <p>index of the bubble.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.IBubbleChartEventArgs.prototype.index = null;
/** <p>label of the series of the bubble.</p>
  * @field 
  * @type {string}
  */
wijmo.chart.IBubbleChartEventArgs.prototype.label = null;
/** <p>legend entry of the series of the bubble.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.IBubbleChartEventArgs.prototype.legendEntry = null;
/** <p>style of the series of the bubble.</p>
  * @field 
  * @type {Object}
  */
wijmo.chart.IBubbleChartEventArgs.prototype.style = null;
/** <p>"bubble"</p>
  * @field 
  * @type {string}
  */
wijmo.chart.IBubbleChartEventArgs.prototype.type = null;
/** <p>The x value of the bubble.</p>
  * @field 
  * @type {string|number|Date}
  */
wijmo.chart.IBubbleChartEventArgs.prototype.x = null;
/** <p>The y value of the bubble.</p>
  * @field 
  * @type {number|Date}
  */
wijmo.chart.IBubbleChartEventArgs.prototype.y = null;
/** <p>The y1 value of the bubble.</p>
  * @field 
  * @type {number|Date}
  */
wijmo.chart.IBubbleChartEventArgs.prototype.y1 = null;
})()
})()
