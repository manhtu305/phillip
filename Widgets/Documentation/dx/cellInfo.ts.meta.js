var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
/** An object that represents a single cell.
  * @class cellInfo
  * @namespace wijmo.grid
  */
wijmo.grid.cellInfo = function () {};
/** Gets the zero-based index of the cell in the row which it corresponds to.
  * @returns {number} The zero-based index of the cell in the row which it corresponds to.
  * @example
  * var index = cellInfoObj.cellIndex();
  */
wijmo.grid.cellInfo.prototype.cellIndex = function () {};
/** Gets the associated column object.
  * @returns {wijmo.grid.IColumn} The associated column object.
  * @example
  * var column = cellInfoObj.column();
  */
wijmo.grid.cellInfo.prototype.column = function () {};
/** Returns the jQuery object containing a cell content.
  * @returns {Object} The jQuery object containing a cell content.
  * @example
  * var $container = cellInfoObj.container();
  */
wijmo.grid.cellInfo.prototype.container = function () {};
/** Compares the current object with an object you have specified and indicates whether they are identical
  * @param {wijmo.grid.cellInfo} value The object to compare
  * @returns {bool} True if the objects are identical, otherwise false.
  * @example
  * var isEqual = cellInfoObj1.isEqual(cellInfoObj2);
  */
wijmo.grid.cellInfo.prototype.isEqual = function (value) {};
/** Gets the accociated row's information.
  * @returns {wijmo.grid.IRowInfo} Information about associated row.
  * @example
  * var row = cellInfoObj.row();
  */
wijmo.grid.cellInfo.prototype.row = function () {};
/** Gets the zero-based index of the row containing the cell.
  * @returns {number} The zero-based index of the row containing the cell.
  * @example
  * var index = cellInfoObj.rowIndex();
  */
wijmo.grid.cellInfo.prototype.rowIndex = function () {};
/** Returns the table cell element corresponding to this object.
  * @returns {HTMLTableCellElement} The table cell element corresponding to this object.
  * @example
  * var domCell = cellInfoObj.tableCell();
  */
wijmo.grid.cellInfo.prototype.tableCell = function () {};
/** Gets the underlying data value.
  * @returns {Object} The underlying data value.
  * @example
  * var value = cellInfoObj.value();
  */
wijmo.grid.cellInfo.prototype.value = function () {};
/** Sets the underlying data vakue.
  * @param {Object} value The value to set.
  * @returns {void}
  * @remarks
  * An "invalid value" exception will be thrown by the setter if the value does not correspond to associated column data type.
  * @example
  * cellInfoObj.value("value");
  */
wijmo.grid.cellInfo.prototype.value = function (value) {};
/** An object that specifies a range of cells determined by two cells.
  * @class cellInfoRange
  * @namespace wijmo.grid
  */
wijmo.grid.cellInfoRange = function () {};
/** Gets the object that represents the bottom right cell of the range.
  * @returns {wijmo.grid.cellInfo} The object that represents the bottom right cell of the range.
  * @example
  * var cellInfoObj = range.bottomRight();
  */
wijmo.grid.cellInfoRange.prototype.bottomRight = function () {};
/** Compares the current range with a specified range and indicates whether they are identical.
  * @param {wijmo.grid.cellInfoRange} range Range to compare.
  * @returns {bool} True if the ranges are identical, otherwise false.
  * @example
  * var isEqual = range1.isEqual(range2);
  */
wijmo.grid.cellInfoRange.prototype.isEqual = function (range) {};
/** Gets the object that represents the top left cell of the range.
  * @returns {wijmo.grid.cellInfo} The object that represents the top left cell of the range.
  * @example
  * var cellInfoObj = range.topLeft();
  */
wijmo.grid.cellInfoRange.prototype.topLeft = function () {};
})()
})()
