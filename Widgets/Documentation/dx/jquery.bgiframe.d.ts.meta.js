var BgiFrame = BgiFrame || {};

(function () {
/** @interface ISettings
  * @namespace BgiFrame
  */
BgiFrame.ISettings = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
BgiFrame.ISettings.prototype.top = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
BgiFrame.ISettings.prototype.left = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
BgiFrame.ISettings.prototype.width = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
BgiFrame.ISettings.prototype.height = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
BgiFrame.ISettings.prototype.opacity = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
BgiFrame.ISettings.prototype.src = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
BgiFrame.ISettings.prototype.conditional = null;
/** @interface IBgiframe
  * @namespace BgiFrame
  */
BgiFrame.IBgiframe = function () {};
/** @returns {HTMLElement} */
BgiFrame.IBgiframe.prototype.createIframe = function () {};
/** @param {HTMLElement} element
  * @returns {void}
  */
BgiFrame.IBgiframe.prototype.fire = function (element) {};
/** @param {HTMLElement} element
  * @returns {HTMLElement}
  */
BgiFrame.IBgiframe.prototype.getIframe = function (element) {};
/** @param n
  * @returns {string}
  */
BgiFrame.IBgiframe.prototype.prop = function (n) {};
/** <p>undefined</p>
  * @field 
  * @type {BgiFrame.ISettings}
  */
BgiFrame.IBgiframe.prototype.s = null;
})()
