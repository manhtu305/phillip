var wijmo = wijmo || {};

(function () {
wijmo.checkbox = wijmo.checkbox || {};

(function () {
/** @class wijcheckbox
  * @widget 
  * @namespace jQuery.wijmo.checkbox
  * @extends wijmo.wijmoWidget
  */
wijmo.checkbox.wijcheckbox = function () {};
wijmo.checkbox.wijcheckbox.prototype = new wijmo.wijmoWidget();
/** Use the refresh method to set the checkbox element's style.
  * @param {object} e The event that fires the refresh the checkbox.
  */
wijmo.checkbox.wijcheckbox.prototype.refresh = function (e) {};
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.checkbox.wijcheckbox.prototype.destroy = function () {};

/** @class */
var wijcheckbox_options = function () {};
/** <p class='defaultValue'>Default value: null</p>
  * <p>Causes the checkbox to appear with a checkmark.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijcheckbox_options.prototype.checked = null;
/** A function that is called when the checked state changes.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.checkbox.IChangedEventArgs} data Information about an event
  */
wijcheckbox_options.prototype.changed = null;
wijmo.checkbox.wijcheckbox.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijcheckbox_options());
$.widget("wijmo.wijcheckbox", $.wijmo.widget, wijmo.checkbox.wijcheckbox.prototype);
/** Contains information about wijcheckbox.changed event
  * @interface IChangedEventArgs
  * @namespace wijmo.checkbox
  */
wijmo.checkbox.IChangedEventArgs = function () {};
/** <p>the state of checkbox.</p>
  * @field 
  * @type {bool}
  */
wijmo.checkbox.IChangedEventArgs.prototype.checked = null;
})()
})()
