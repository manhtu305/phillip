var wijmo = wijmo || {};

(function () {
wijmo.data = wijmo.data || {};

(function () {
/** @interface ISubscription
  * @namespace wijmo.data
  * @extends wijmo.IDisposable
  */
wijmo.data.ISubscription = function () {};
/** @interface ISubscribable
  * @namespace wijmo.data
  */
wijmo.data.ISubscribable = function () {};
/** @param {function} handler
  * @param context
  * @returns {wijmo.data.ISubscription}
  */
wijmo.data.ISubscribable.prototype.subscribe = function (handler, context) {};
/** @param {wijmo.data.ISubscribable} subscribable
  * @returns {bool}
  */
wijmo.data.isSubscriptable = function (subscribable) {};
/** @interface IObservable
  * @namespace wijmo.data
  * @extends wijmo.data.ISubscribable
  */
wijmo.data.IObservable = function () {};
/** @interface IMutableObservable
  * @namespace wijmo.data
  * @extends wijmo.data.IObservable
  */
wijmo.data.IMutableObservable = function () {};
/** @returns {wijmo.data.IObservable} */
wijmo.data.IMutableObservable.prototype.read = function () {};
/** @interface INumericMutableObservable
  * @namespace wijmo.data
  * @extends wijmo.data.IMutableObservable
  */
wijmo.data.INumericMutableObservable = function () {};
/** @returns {number} */
wijmo.data.INumericMutableObservable.prototype.inc = function () {};
/** @returns {number} */
wijmo.data.INumericMutableObservable.prototype.dec = function () {};
/** @returns {wijmo.data.IObservable} */
wijmo.data.INumericMutableObservable.prototype.read = function () {};
/** @param value
  * @returns {wijmo.data.IMutableObservable}
  */
wijmo.data.observable = function (value) {};
/** @param {wijmo.data.IObservable} observable
  * @returns {bool}
  */
wijmo.data.isObservable = function (observable) {};
typeof wijmo.IDisposable != 'undefined' && $.extend(wijmo.data.ISubscription.prototype, wijmo.IDisposable.prototype);
typeof wijmo.data.ISubscribable != 'undefined' && $.extend(wijmo.data.IObservable.prototype, wijmo.data.ISubscribable.prototype);
typeof wijmo.data.IObservable != 'undefined' && $.extend(wijmo.data.IMutableObservable.prototype, wijmo.data.IObservable.prototype);
typeof wijmo.data.IMutableObservable != 'undefined' && $.extend(wijmo.data.INumericMutableObservable.prototype, wijmo.data.IMutableObservable.prototype);
})()
})()
