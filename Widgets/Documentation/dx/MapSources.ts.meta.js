var wijmo = wijmo || {};

(function () {
wijmo.maps = wijmo.maps || {};

(function () {
/** Defines an object that represents the tile source of the map.
  * @interface IMultiScaleTileSource
  * @namespace wijmo.maps
  */
wijmo.maps.IMultiScaleTileSource = function () {};
/** The callback function to get the url of the specific tile.
  * @param {number} zoom The current zoom level.
  * @param {number} x The column index of the tile, in 0 base. The amount of columns is 2^zoom.
  * @param {number} y The row index of the tile, in 0 base. The amount of rows is 2^zoom.
  * @returns {string}
  */
wijmo.maps.IMultiScaleTileSource.prototype.getUrl = function (zoom, x, y) {};
/** <p>The width of the tile, in pixel. The default is 256.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.IMultiScaleTileSource.prototype.tileWidth = null;
/** <p>The height of the tile, in pixel. The default is 256.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.IMultiScaleTileSource.prototype.tileHeight = null;
/** <p>The minmum zoom level supports by this tile source. The default is 1.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.IMultiScaleTileSource.prototype.minZoom = null;
/** <p>The maximum zoom level supports by this tile source. The default is 19.</p>
  * @field 
  * @type {number}
  */
wijmo.maps.IMultiScaleTileSource.prototype.maxZoom = null;
})()
})()
