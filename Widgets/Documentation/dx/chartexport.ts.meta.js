var wijmo = wijmo || {};

(function () {
wijmo.exporter = wijmo.exporter || {};

(function () {
/** @enum ChartExportMethod
  * @namespace wijmo.exporter
  */
wijmo.exporter.ChartExportMethod = {
/**  */
Content: 0,
/**  */
Options: 1 
};
/** @interface ChartExportSetting
  * @namespace wijmo.exporter
  * @extends wijmo.exporter.ExportSetting
  */
wijmo.exporter.ChartExportSetting = function () {};
/** <p>The export width setting.</p>
  * @field 
  * @type {number}
  */
wijmo.exporter.ChartExportSetting.prototype.width = null;
/** <p>The export height setting.</p>
  * @field 
  * @type {number}
  */
wijmo.exporter.ChartExportSetting.prototype.height = null;
/** <p>The name of wijchart</p>
  * @field 
  * @type {string}
  */
wijmo.exporter.ChartExportSetting.prototype.widgetName = null;
/** <p>The options of wijchart,used for chart export options mode.</p>
  * @field
  */
wijmo.exporter.ChartExportSetting.prototype.widgetOptions = null;
/** <p>Chart widget markups, used for chart export content mode.</p>
  * @field 
  * @type {string}
  */
wijmo.exporter.ChartExportSetting.prototype.HTMLContent = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.wijchartcore.html'>wijmo.chart.wijchartcore</a></p>
  * <p>The wijchart object.</p>
  * @field 
  * @type {wijmo.chart.wijchartcore}
  */
wijmo.exporter.ChartExportSetting.prototype.chart = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.exporter.ChartExportMethod.html'>wijmo.exporter.ChartExportMethod</a></p>
  * <p>It has "Content" and "Options" mode.</p>
  * @field 
  * @type {wijmo.exporter.ChartExportMethod}
  */
wijmo.exporter.ChartExportSetting.prototype.method = null;
typeof wijmo.exporter.ExportSetting != 'undefined' && $.extend(wijmo.exporter.ChartExportSetting.prototype, wijmo.exporter.ExportSetting.prototype);
})()
})()
