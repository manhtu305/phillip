var wijmo = wijmo || {};

(function () {
wijmo.tabs = wijmo.tabs || {};

(function () {
/** @class wijtabs
  * @widget 
  * @namespace jQuery.wijmo.tabs
  * @extends wijmo.wijmoWidget
  */
wijmo.tabs.wijtabs = function () {};
wijmo.tabs.wijtabs.prototype = new wijmo.wijmoWidget();
/** The destroy() method will remove the wijtabs functionality completely 
  * and will return the element to its pre-init state.
  */
wijmo.tabs.wijtabs.prototype.destroy = function () {};
/** Terminate all running tab ajax requests and animations.
  * @returns {wijtabs}
  */
wijmo.tabs.wijtabs.prototype.abort = function () {};
/** Selects a tab; for example, a clicked tab.
  * @param {number|string} index The zero-based index of the tab to be selected or
  * the id selector of the panel the tab is associated with.
  * @returns {wijtabs}
  * @example
  * //Select the second tab.
  * $("#element").wijtabs('select', 1);
  */
wijmo.tabs.wijtabs.prototype.select = function (index) {};
/** Reload the content of an Ajax tab programmatically. 
  * This method always loads the tab content from the remote location,
  * even if cache is set to true.
  * @param {number} index The zero-based index of the tab to be reloaded.
  * @returns {wijtabs}
  * @example
  * //Reload the second tab.
  * $("#element").wijtabs('load', 1);
  */
wijmo.tabs.wijtabs.prototype.load = function (index) {};
/** Add a new tab.
  * @param {string} url A URL consisting of a fragment identifier 
  * only to create an in-page tab or a full url 
  * (relative or absolute, no cross-domain support) to 
  * turn the new tab into an Ajax (remote) tab.
  * @param {string} label The tab label.
  * @param {number} index Zero-based position where to insert the new tab.
  * @returns {wijtabs}
  * @example
  * //Add a new tab to be a second tab.
  * $("#element").wijtabs('add', "http://wijmo.com/newTab", "NewTab", 1);
  */
wijmo.tabs.wijtabs.prototype.add = function (url, label, index) {};
/** Removes a tab.
  * @param {number} index The zero-based index of the tab to be removed.
  * @returns {wijtabs}
  * @example
  * //Removes the second tab
  * $("#element").wijtabs('remove', 1);
  */
wijmo.tabs.wijtabs.prototype.remove = function (index) {};
/** Enable a disabled tab.
  * @param {number} index The zero-based index of the tab to be enabled.
  * @returns {wijtabs}
  * @example
  * //Enables the second tab
  * $("#element").wijtabs('enableTab', 1);
  */
wijmo.tabs.wijtabs.prototype.enableTab = function (index) {};
/** Disabled a tab.
  * @param {number} index The zero-based index of the tab to be disabled.
  * @returns {wijtabs}
  * @example
  * //Disables the second tab
  * $("#element").wijtabs('disableTab', 1);
  */
wijmo.tabs.wijtabs.prototype.disableTab = function (index) {};
/** Changes the url from which an Ajax (remote) tab will be loaded.
  * @param {number} index The zero-based index of the tab of which its URL is to be updated.
  * @param {string} url A URL the content of the tab is loaded from.
  * @returns {wijtabs}
  * @remarks
  * The specified URL will be used for subsequent loads. 
  * Note that you can not only change the URL for an existing remote tab
  * with this method, but you can also turn an in-page tab into a remote tab.
  * @example
  * //Changes the second tab to a new tab url.
  * $("#element").wijtabs('url', 1, "http://wijmo.com/newTabUrl")
  */
wijmo.tabs.wijtabs.prototype.url = function (index, url) {};
/** Retrieve the number of tabs of the first matched tab pane. */
wijmo.tabs.wijtabs.prototype.length = function () {};

/** @class */
var wijtabs_options = function () {};
/** <p class='defaultValue'>Default value: 'top'</p>
  * <p>Determines the tabs' alignment in respect to the content.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Possible values are: 'top', 'bottom', 'left' and 'right'.
  */
wijtabs_options.prototype.alignment = 'top';
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether the tab can be dragged to a new position.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * This option only works when jQuery.ui.sortable is available
  * which means that it doesn't work in mobile mode.
  */
wijtabs_options.prototype.sortable = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether to wrap to the next line or enable scrolling 
  * when the number of tabs exceeds the specified width.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtabs_options.prototype.scrollable = false;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Additional Ajax options to consider when loading tab content (see $.ajax).</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * Please see following link for more details,
  * http://api.jquery.com/jQuery.ajax/ .
  */
wijtabs_options.prototype.ajaxOptions = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether or not to cache the remote tabs content,
  * for example, to load content only once or with every click.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * Note that to prevent the actual Ajax requests from being cached
  * by the browser, you need to provide an extra cache: 
  * false flag to ajaxOptions.
  */
wijtabs_options.prototype.cache = false;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Stores the latest selected tab in a cookie. 
  * The cookie is then used to determine the initially selected tab
  * if the selected option is not defined. 
  * This option requires a cookie plugin. The object needs to have key/value pairs
  * of the form the cookie plugin expects as options.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * //Set cookie to wijtabs.
  * $('.selector').wijtabs({cookie: { 
  * expires: 7, path: '/', domain: 'jquery.com', secure: true }});
  */
wijtabs_options.prototype.cookie = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether a tab can be collapsed by a user.
  * When this is set to true, an already selected tab
  * will be collapsed upon reselection.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijtabs_options.prototype.collapsible = false;
/** <p class='defaultValue'>Default value: null</p>
  * <p>This is an animation option for hiding the tab's panel content.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * //Set hide animation to blind/fade and duration to 200.
  * $('.selector').wijtabs({
  * hideOption: { blind: true, fade: true, duration: 200}});
  */
wijtabs_options.prototype.hideOption = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>This is an animation option for showing the tab's panel content.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * //Set show animation to blind/fade and duration to 200.
  * $('.selector').wijtabs({
  * showOption: { blind: true, fade: true, duration: 200}});
  */
wijtabs_options.prototype.showOption = null;
/** <p class='defaultValue'>Default value: []</p>
  * <p>An array containing the position of the tabs (zero-based index)
  * that should be disabled upon initialization.</p>
  * @field 
  * @type {array}
  * @option
  */
wijtabs_options.prototype.disabledIndexes = [];
/** <p class='defaultValue'>Default value: 'click'</p>
  * <p>The type of event to be used for selecting a tab.</p>
  * @field 
  * @type {string}
  * @option
  */
wijtabs_options.prototype.event = 'click';
/** <p class='defaultValue'>Default value: 'ui-tabs-'</p>
  * <p>If the remote tab, its anchor element that is, has no title attribute
  * to generate an id from, 
  * an id/fragment identifier is created from this prefix and a unique id
  * returned by $.data(el), for example "ui-tabs-54".</p>
  * @field 
  * @type {string}
  * @option
  */
wijtabs_options.prototype.idPrefix = 'ui-tabs-';
/** <p class='defaultValue'>Default value: ""</p>
  * <p>This is the HTML template from which a new tab panel is created in case
  * a tab is added via the add method or 
  * if a panel for a remote tab is created on the fly.</p>
  * @field 
  * @type {string}
  * @option
  */
wijtabs_options.prototype.panelTemplate = "";
/** <p class='defaultValue'>Default value: ""</p>
  * <p>The HTML content of this string is shown in a tab title
  * while remote content is loading. 
  * Pass in an empty string to deactivate that behavior. 
  * A span element must be present in the A tag of the title
  * for the spinner content to be visible.</p>
  * @field 
  * @type {string}
  * @option
  */
wijtabs_options.prototype.spinner = "";
/** <p class='defaultValue'>Default value: ""</p>
  * <p>HTML template from which a new tab is created and added. 
  * The placeholders #{href} and #{label} are replaced with the url
  * and tab label that are passed as arguments to the add method.</p>
  * @field 
  * @type {string}
  * @option
  */
wijtabs_options.prototype.tabTemplate = "";
/** The add event handler. A function called when a tab is added.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijTabsEventArgs} args The data with this event.
  */
wijtabs_options.prototype.add = null;
/** The remove event handler. A function called when a tab is removed.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijTabsEventArgs} args The data with this event.
  */
wijtabs_options.prototype.remove = null;
/** The select event handler. A function called when clicking a tab.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijTabsEventArgs} args The data with this event.
  */
wijtabs_options.prototype.select = null;
/** The beforeShow event handler. A function called before a tab is shown.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijTabsEventArgs} args The data with this event.
  */
wijtabs_options.prototype.beforeShow = null;
/** The show event handler. A function called when a tab is shown.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijTabsEventArgs} args The data with this event.
  */
wijtabs_options.prototype.show = null;
/** The load event handler. 
  * A function called after the content of a remote tab has been loaded.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijTabsEventArgs} args The data with this event.
  */
wijtabs_options.prototype.load = null;
/** The disable event handler. A function called when a tab is disabled.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijTabsEventArgs} args The data with this event.
  */
wijtabs_options.prototype.disable = null;
/** The enable event handler. A function called when a tab is enabled.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijTabsEventArgs} args The data with this event.
  */
wijtabs_options.prototype.enable = null;
wijmo.tabs.wijtabs.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijtabs_options());
$.widget("wijmo.wijtabs", $.wijmo.widget, wijmo.tabs.wijtabs.prototype);
/** @interface IWijTabsEventArgs
  * @namespace wijmo.tabs
  */
wijmo.tabs.IWijTabsEventArgs = function () {};
/** <p>The tab element.</p>
  * @field 
  * @type {JQuery}
  */
wijmo.tabs.IWijTabsEventArgs.prototype.tab = null;
/** <p>The panel element.</p>
  * @field 
  * @type {JQuery}
  */
wijmo.tabs.IWijTabsEventArgs.prototype.panel = null;
/** <p>The index of the panel.</p>
  * @field 
  * @type {number}
  */
wijmo.tabs.IWijTabsEventArgs.prototype.index = null;
})()
})()
