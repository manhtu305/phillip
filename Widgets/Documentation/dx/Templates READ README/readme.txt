Extract this zip file into the following folder on your machine BEFORE you open the help project.

C:\Documents and Settings\All Users\Application Data\innovasys\DocumentX2013\templates\DX.JAVASCRIPT

If the DX.JAVASCRIPT folder doesn't exist, create it.

If you already opened the project and got the missing template error, it reset to the default template, so if you saved changes, you'll need to select this template in the project. To select this template:

1. In the Project Explorer, expand the Build Profiles node.
2. Double-click the Wijmo build profile to open it.
3. On the Wijmo tab that appears, under Appearance, select Templates.
3. On the Templates page, under Javascript Documentation, drop down the Template drop-down list and select Wijmo 2012 Style.