SET TF="%VS110COMNTOOLS%..\IDE\TF.exe"
%TF% get -recursive .

echo A | unzip "Wijmo 2012 Style (Javascript documentation).zip" -d c:\ProgramData\Innovasys\DocumentX2013\templates\DX.JAVASCRIPT
echo A | unzip "Wijmo 2012 Style (Browser Help).zip" -d c:\ProgramData\Innovasys\DocumentX2013\templates\BROWSERHELP
echo A | unzip "Wijmo 2012 Style (Topics).zip" -d c:\ProgramData\Innovasys\DocumentX2013\templates\TOPICS
