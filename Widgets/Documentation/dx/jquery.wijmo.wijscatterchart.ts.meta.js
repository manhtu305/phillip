var wijmo = wijmo || {};

(function () {
wijmo.chart = wijmo.chart || {};

(function () {
/** @class wijscatterchart
  * @widget 
  * @namespace jQuery.wijmo.chart
  * @extends wijmo.chart.wijchartcore
  */
wijmo.chart.wijscatterchart = function () {};
wijmo.chart.wijscatterchart.prototype = new wijmo.chart.wijchartcore();
/** Remove the functionality completely. This will return the element back to its pre-init state */
wijmo.chart.wijscatterchart.prototype.destroy = function () {};
/** Returns the scatter element with the given series index and scatter index.
  * @param {number} seriesIndex The index of the series
  * @param {number} scatterIndex The index of the scatter element
  * @returns
  * {Raphael Element} if scatterIndex is not specified, return a list of scatters of specified seriesIndex, 
  * else return the specified scatter element
  */
wijmo.chart.wijscatterchart.prototype.getScatter = function (seriesIndex, scatterIndex) {};

/** @class */
var wijscatterchart_options = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_animation.html'>wijmo.chart.chart_animation</a></p>
  * <p>An option that controls aspects of the widget's animation, such as duration and easing.</p>
  * @field 
  * @type {wijmo.chart.chart_animation}
  * @option
  */
wijscatterchart_options.prototype.animation = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that indicates whether to show default chart labels.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijscatterchart_options.prototype.showChartLabels = false;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_animation.html'>wijmo.chart.chart_animation</a></p>
  * <p>The seriesTransition option is used to animate series in the chart when just their values change.</p>
  * @field 
  * @type {wijmo.chart.chart_animation}
  * @option 
  * @remarks
  * This is helpful for visually showing changes in data for the same series.
  */
wijscatterchart_options.prototype.seriesTransition = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value that indicates whether to zoom in on the marker on hover.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijscatterchart_options.prototype.zoomOnHover = true;
/** Occurs when the user clicks a mouse button.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IScatterChartEventArgs} data Information about an event
  */
wijscatterchart_options.prototype.mouseDown = null;
/** Fires when the user releases a mouse button while the pointer is over the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IScatterChartEventArgs} data Information about an event
  */
wijscatterchart_options.prototype.mouseUp = null;
/** Fires when the user first places the pointer over the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IScatterChartEventArgs} data Information about an event
  */
wijscatterchart_options.prototype.mouseOver = null;
/** Fires when the user moves the pointer off of the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IScatterChartEventArgs} data Information about an event
  */
wijscatterchart_options.prototype.mouseOut = null;
/** Fires when the user moves the mouse pointer while it is over a chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IScatterChartEventArgs} data Information about an event
  */
wijscatterchart_options.prototype.mouseMove = null;
/** Fires when the user clicks the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IScatterChartEventArgs} data Information about an event
  */
wijscatterchart_options.prototype.click = null;
wijmo.chart.wijscatterchart.prototype.options = $.extend({}, true, wijmo.chart.wijchartcore.prototype.options, new wijscatterchart_options());
$.widget("wijmo.wijscatterchart", $.wijmo.wijchartcore, wijmo.chart.wijscatterchart.prototype);
/** @class wijscatterchart_css
  * @namespace wijmo.chart
  * @extends wijmo.chart.wijchartcore_css
  */
wijmo.chart.wijscatterchart_css = function () {};
wijmo.chart.wijscatterchart_css.prototype = new wijmo.chart.wijchartcore_css();
/** Contains information about wijscatterchart event.
  * @interface IScatterChartEventArgs
  * @namespace wijmo.chart
  */
wijmo.chart.IScatterChartEventArgs = function () {};
/** <p>The Raphael object of the marker.</p>
  * @field 
  * @type {Object}
  */
wijmo.chart.IScatterChartEventArgs.prototype.marker = null;
/** <p>The data of the series of the marker.</p>
  * @field 
  * @type {Object}
  */
wijmo.chart.IScatterChartEventArgs.prototype.data = null;
/** <p>The hover style of the series of the marker.</p>
  * @field 
  * @type {Object}
  */
wijmo.chart.IScatterChartEventArgs.prototype.hoverStyle = null;
/** <p>scatter</p>
  * @field 
  * @type {string}
  */
wijmo.chart.IScatterChartEventArgs.prototype.type = null;
/** <p>The label of the series of the marker.</p>
  * @field 
  * @type {string}
  */
wijmo.chart.IScatterChartEventArgs.prototype.label = null;
/** <p>index of the marker.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.IScatterChartEventArgs.prototype.index = null;
/** <p>The legend entry of the series of the marker.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.IScatterChartEventArgs.prototype.legendEntry = null;
/** <p>The style of the series of the marker.</p>
  * @field 
  * @type {Object}
  */
wijmo.chart.IScatterChartEventArgs.prototype.style = null;
/** <p>value x of the marker.</p>
  * @field 
  * @type {string|number|Date}
  */
wijmo.chart.IScatterChartEventArgs.prototype.x = null;
/** <p>value y of the marker.</p>
  * @field 
  * @type {number|Date}
  */
wijmo.chart.IScatterChartEventArgs.prototype.y = null;
})()
})()
