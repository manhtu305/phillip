var wijmo = wijmo || {};

(function () {
wijmo.dropdown = wijmo.dropdown || {};

(function () {
/** @class wijdropdown
  * @widget 
  * @namespace jQuery.wijmo.dropdown
  * @extends wijmo.wijmoWidget
  */
wijmo.dropdown.wijdropdown = function () {};
wijmo.dropdown.wijdropdown.prototype = new wijmo.wijmoWidget();
/** Use the refresh method to set the drop-down element's style. */
wijmo.dropdown.wijdropdown.prototype.refresh = function () {};
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.dropdown.wijdropdown.prototype.destroy = function () {};

/** @class */
var wijdropdown_options = function () {};
/** <p class='defaultValue'>Default value: 1000</p>
  * <p>A value indicates the z-index of wijdropdown.</p>
  * @field 
  * @type {number}
  * @option
  */
wijdropdown_options.prototype.zIndex = 1000;
/** <p>A value that specifies the animation options for a drop-down list
  *  when it is visible.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * var animationOptions = {
  * animated: "Drop", duration: 1000 };
  * $("#tags").wijdropdown("option", "showingAnimation", animationOptions)
  */
wijdropdown_options.prototype.showingAnimation = null;
/** <p>A value that specifies the animation options such as the animation effect and
  * duration for the drop-down list when it is hidden.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * var animationOptions = {
  * animated: "Drop", duration: 1000 };
  * $("#tags").wijdropdown("option", "hidingAnimation", animationOptions)
  */
wijdropdown_options.prototype.hidingAnimation = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value indicating the dropdown element will be append to the body or dropdown container.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * If the value is true, the dropdown list will be appended to body element.
  * else it will append to the dropdown container.
  */
wijdropdown_options.prototype.ensureDropDownOnBody = false;
wijmo.dropdown.wijdropdown.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijdropdown_options());
$.widget("wijmo.wijdropdown", $.wijmo.widget, wijmo.dropdown.wijdropdown.prototype);
})()
})()
