var wijmo = wijmo || {};

(function () {
wijmo.ribbon = wijmo.ribbon || {};

(function () {
/** @class wijribbon
  * @widget 
  * @namespace jQuery.wijmo.ribbon
  * @extends wijmo.wijmoWidget
  */
wijmo.ribbon.wijribbon = function () {};
wijmo.ribbon.wijribbon.prototype = new wijmo.wijmoWidget();
/** Sets a ribbon tab page as visible or not.
  * @param {string} id The id of the tab page.
  * @param {bool} visible The visible state of the tab page.
  */
wijmo.ribbon.wijribbon.prototype.setTabPageVisible = function (id, visible) {};
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.ribbon.wijribbon.prototype.destroy = function () {};
/** The method sets the chosen button as enabled or disabled according to the command name.
  * @param {string} commandName The name of the command.
  * @param {bool} disabled The disabled state of the button, true or false.
  */
wijmo.ribbon.wijribbon.prototype.setButtonDisabled = function (commandName, disabled) {};
/** The method sets the ribbon buttons as enabled or disabled according to the command name.
  * @param {object} commands An object that contains commands infos that need to change state,
  * key is command name, value is button disabled state, true or false.
  */
wijmo.ribbon.wijribbon.prototype.setButtonsDisabled = function (commands) {};
/** The method sets sets the buttons as checked or not checked.
  * @param {object} commands An object that contains commands infos that need to change state,
  * key is command name, value is button checked state, true or false.
  */
wijmo.ribbon.wijribbon.prototype.setButtonsChecked = function (commands) {};
/** The method used to push the custom button to button collection.
  * @param {string} cmdName The command of the button.
  * @param {Object} eleObj The object of the button information.
  */
wijmo.ribbon.wijribbon.prototype.registerButtonInformation = function (cmdName, eleObj) {};
/** The custom button trigger ribbon click
  * @param {e} e the event information.
  * @param {obj} eleObj The data information.
  */
wijmo.ribbon.wijribbon.prototype.ribbonClick = function (e, eleObj) {};
/** Sets a ribbon button as checked or not checked.
  * @param {string} commandName The command name of the button.
  * @param {bool} checked The checked state of the button.
  * @param {string} name The parent name of the button.
  */
wijmo.ribbon.wijribbon.prototype.setButtonChecked = function (commandName, checked, name) {};

/** @class */
var wijribbon_options = function () {};
/** <p class='defaultValue'>Default value: false</p>
  * <p>If the compactMode option is true and ribbon is not simple,it will show
  *   compact mode.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijribbon_options.prototype.compactMode = false;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Using this option to create customize ribbon: ribbon tabs, groups, buttons.
  *   Can define the ribbon data like this:
  * var ribbonData = [];
  *    // format tab definition
  *     ribbonData[0] = {
  *         id: "format",
  *         text: "format",
  *         groups:[
  *         // Actions group definition
  *         {
  *             id:"Actions",
  *             text: "Actions",
  *             css: "wijmo-wijribbon-actions",
  *             // group's buttons definition
  *             buttons:[
  *                 //big button definition: save
  *                 { name: "save", tip: 'Save', text: 'Save Button', css: "wijmo-wijribbon-save", buttonType: "bigButton", action: function (e) { alert("Save"); } },
  *                //redo and undo in one set
  *                 [
  *                   { name: "undo", tip: 'Undo', css: "wijmo-wijribbon-undo", buttonType: "button" },
  *                   { name: "redo", tip: 'Redo', css: "wijmo-wijribbon-redo", buttonType: "button" }
  *                 ],
  *                  //preview and cleanup in one set
  *                 [
  *                   { name: "preview", tip: 'Preview', css: "wijmo-wijribbon-preview" },
  *                   { name: "cleanup", tip: 'Clean up', css: "wijmo-wijribbon-cleanup" }
  *                 ] 
  *             ]
  *        }];
  *  Now the supported button's type is: "button", "buttonWithText", "bigButton", "checkbox", "dropdownButton",  "radio", "splitButton".
  *  User can define button's text, tooltip, class and click function; if some buttons in one group, put them in array.
  *  Some details refer the customizeribbonbydata.html and customizesimpleribbonbydata.html.</p>
  * @field 
  * @type {object}
  * @option
  */
wijribbon_options.prototype.data = null;
/** The wijRibbonClick event is a function that is called 
  * when the ribbon command button is clicked.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.ribbon.IClickEventArgs} data Information about an event
  */
wijribbon_options.prototype.click = null;
wijmo.ribbon.wijribbon.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijribbon_options());
$.widget("wijmo.wijribbon", $.wijmo.widget, wijmo.ribbon.wijribbon.prototype);
/** Contains information about wijribbon.click event
  * @interface IClickEventArgs
  * @namespace wijmo.ribbon
  */
wijmo.ribbon.IClickEventArgs = function () {};
/** <p>the command name of the button.</p>
  * @field 
  * @type {string}
  */
wijmo.ribbon.IClickEventArgs.prototype.commandName = null;
/** <p>the parent name of the button which means if the drop down item is clicked, 
  *                     then the name specifies the command name of the drop down button.</p>
  * @field 
  * @type {string}
  */
wijmo.ribbon.IClickEventArgs.prototype.name = null;
})()
})()
