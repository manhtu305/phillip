var wijmo = wijmo || {};

(function () {
wijmo.chart = wijmo.chart || {};

(function () {
/** @class TrendlineRender
  * @namespace wijmo.chart
  */
wijmo.chart.TrendlineRender = function () {};
/**  */
wijmo.chart.TrendlineRender.prototype.render = function () {};
/** @param clipRect
  * @param path
  * @param cBounds
  */
wijmo.chart.TrendlineRender.prototype.setClipRect = function (clipRect, path, cBounds) {};
/** @param animationEnable
  * @param duration
  * @param easing
  * @param seTrans
  * @param cBounds
  * @param paths
  * @param fieldsAniPathAttr
  * @param axis
  * @param exVal
  */
wijmo.chart.TrendlineRender.prototype.playAnimation = function (animationEnable, duration, easing, seTrans, cBounds, paths, fieldsAniPathAttr, axis, exVal) {};
/** @param duration
  * @param easing
  * @param cBounds
  * @param path
  * @param clipRect
  * @param width
  * @param height
  */
wijmo.chart.TrendlineRender.prototype.playPathAnimation = function (duration, easing, cBounds, path, clipRect, width, height) {};
/** @param fitType
  * @returns {bool}
  */
wijmo.chart.TrendlineRender.prototype.validateType = function (fitType) {};
/** @param cBounds
  * @param series
  * @param seriesHoverStyle
  * @param axis
  * @param hole
  * @param fieldsAniPathAttr
  * @param animation
  * @param seriesTransition
  * @param index
  * @param seriesStyle
  * @param canvas
  * @param paths
  * @param shadowPaths
  * @param animationSet
  * @param aniPathsAttr
  * @param wijCSS
  * @param seriesEles
  * @param inverted
  * @param {bool} shadow
  */
wijmo.chart.TrendlineRender.prototype.renderSingleTrendLine = function (series, seriesStyle, seriesHoverStyle, axis, hole, fieldsAniPathAttr, animation, seriesTransition, index, cBounds, canvas, paths, shadowPaths, animationSet, aniPathsAttr, wijCSS, seriesEles, inverted, shadow) {};
/** @param options
  * @param aniPathsAttr
  * @param fieldsAniPathAttr
  * @param paths
  * @param shadowPaths
  * @param animationSet
  * @param linesStyle
  * @param seriesEles
  */
wijmo.chart.TrendlineRender.prototype.renderTrendLineChart = function (options, aniPathsAttr, fieldsAniPathAttr, paths, shadowPaths, animationSet, linesStyle, seriesEles) {};
/** @param aniPathsAttr
  * @param canvas
  * @param lineSeries
  * @param paths
  * @param shadowPaths
  * @param lineHoverStyle
  * @param animationSet
  * @param pathArr
  * @param bounds
  * @param initAniPath
  * @param lineStyle
  * @param pathIdx
  * @param wijCSS
  * @param axis
  * @param inverted
  * @param {bool} shadow
  */
wijmo.chart.TrendlineRender.prototype.renderPath = function (canvas, bounds, lineSeries, paths, shadowPaths, lineHoverStyle, animationSet, pathArr, aniPathsAttr, initAniPath, lineStyle, pathIdx, wijCSS, axis, inverted, shadow) {};
/** @param val
  * @param dir
  * @param axis
  * @param bounds
  * @param inverted
  */
wijmo.chart.TrendlineRender.prototype.calculatePoint = function (val, dir, axis, bounds, inverted) {};
/** @param lastYPoint
  * @param cBounds
  * @param pathArr
  * @param needAnimated
  * @param firstYPoint
  * @param initAniPath
  * @param valX
  * @param valY
  * @param axis
  * @param pointIdx
  * @param inverted
  */
wijmo.chart.TrendlineRender.prototype.renderPoint = function (cBounds, initAniPath, pathArr, needAnimated, firstYPoint, lastYPoint, valX, valY, axis, pointIdx, inverted) {};
/** @param mouseOut
  * @param element
  * @param mouseDown
  * @param mouseUp
  * @param mouseOver
  * @param widgetName
  * @param mouseMove
  * @param click
  * @param disabled
  * @param wijCSS
  * @param hasProcessed
  */
wijmo.chart.TrendlineRender.prototype.bindLiveEvents = function (element, widgetName, mouseDown, mouseUp, mouseOver, mouseOut, mouseMove, click, disabled, wijCSS, hasProcessed) {};
/** @param element
  * @param widgetName
  * @param wijCSS
  */
wijmo.chart.TrendlineRender.prototype.unbindLiveEvents = function (element, widgetName, wijCSS) {};
/** @param canvas
  * @param x
  * @param width
  * @param height
  * @param style
  * @param y
  * @param legendSize
  * @param legendIcons
  * @param legendIndex
  * @param seriesIndex
  * @param legendCss
  */
wijmo.chart.TrendlineRender.prototype.paintLegendIcon = function (x, y, width, height, style, canvas, legendSize, legendIcons, legendIndex, seriesIndex, legendCss) {};
/** @param eles */
wijmo.chart.TrendlineRender.prototype.showSerieEles = function (eles) {};
/** @param eles */
wijmo.chart.TrendlineRender.prototype.hideSerieEles = function (eles) {};
/** @param series */
wijmo.chart.TrendlineRender.prototype.getSeriesChartType = function (series) {};
/** @class ChartTooltip
  * @namespace wijmo.chart
  */
wijmo.chart.ChartTooltip = function () {};
/**  */
wijmo.chart.ChartTooltip.prototype.init = function () {};
/** @param all
  * @param key
  * @param obj
  */
wijmo.chart.ChartTooltip.prototype.replacer = function (all, key, obj) {};
/** @param str
  * @param obj
  * @returns {string}
  */
wijmo.chart.ChartTooltip.prototype.fill = function (str, obj) {};
/** @param {bool} hideImmediately */
wijmo.chart.ChartTooltip.prototype.hide = function (hideImmediately) {};
/** @param {wijmo.Point} point
  * @param e
  */
wijmo.chart.ChartTooltip.prototype.showAt = function (point, e) {};
/** @param {number} offset */
wijmo.chart.ChartTooltip.prototype.resetCalloutOffset = function (offset) {};
/**  */
wijmo.chart.ChartTooltip.prototype.destroy = function () {};
/**  */
wijmo.chart.ChartTooltip.prototype.getOptions = function () {};
/** @param targets */
wijmo.chart.ChartTooltip.prototype.setTargets = function (targets) {};
/** @param selector */
wijmo.chart.ChartTooltip.prototype.setSelector = function (selector) {};
/** @param opts */
wijmo.chart.ChartTooltip.prototype.setOptions = function (opts) {};
/** @class wijchartcore
  * @widget 
  * @namespace jQuery.wijmo.chart
  * @extends wijmo.wijmoWidget
  */
wijmo.chart.wijchartcore = function () {};
wijmo.chart.wijchartcore.prototype = new wijmo.wijmoWidget();
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.chart.wijchartcore.prototype.destroy = function () {};
/** Returns a reference to the Raphael canvas object.
  * @returns {Raphael} Reference to raphael canvas object.
  * @example
  * $("#chartcore").wijchartcore("getCanvas")
  */
wijmo.chart.wijchartcore.prototype.getCanvas = function () {};
/** Exports the chart in a graphic format. 
  * The export method only works when wijmo.exporter.chartExport's reference is on the page.
  * @param {string|Object} exportSettings 1.The name of the exported file.
  * 2.Settings of exporting, should be conformed to wijmo.exporter.ChartExportSetting
  * @param {?string} type The type of the exported file.
  * @param {?string} pdfSettings The setting of pdf.
  * @param {?string} serviceUrl The export service url.
  * @param {?string} exportMethod with different mode,
  * 1. "Content" Sending chart markup to the service for exporting. It requires IE9 or high version installed on the service host. It has better performance than Options mode.
  * 2. "Options" Sending chart widget options to the service for exporting.
  * @remarks
  * Default exported file type is png, possible types are: jpg, png, gif, bmp, tiff, pdf.
  * @example
  * $("#chartcore").wijchartcore("exportChart", "chart", "png");
  */
wijmo.chart.wijchartcore.prototype.exportChart = function (exportSettings, type, pdfSettings, serviceUrl, exportMethod) {};
/** Add series point to the series list.
  * @param {number} seriesIndex The index of the series that the point will be inserted to.
  * @param {object} point The point that will be inserted to.
  * @param {bool} shift A value that indicates whether to shift the first point.
  */
wijmo.chart.wijchartcore.prototype.addSeriesPoint = function (seriesIndex, point, shift) {};
/** Suspend automatic updates to the chart while reseting the options. */
wijmo.chart.wijchartcore.prototype.beginUpdate = function () {};
/** Restore automatic updates to the chart after the options has been reset. */
wijmo.chart.wijchartcore.prototype.endUpdate = function () {};
/** This method redraws the chart.
  * @param {?Boolean} drawIfNeeded A value that indicates whether to redraw the chart regardless of whether
  * the chart already exists. If true, then the chart is redrawn only if it was not already created. If false, 
  * then the chart is redrawn, even if it already exists.
  */
wijmo.chart.wijchartcore.prototype.redraw = function (drawIfNeeded) {};

/** @class */
var wijchartcore_options = function () {};
/** <p class='defaultValue'>Default value: []</p>
  * <p>Creates an array of AnnotationBase objects that contain the settings of the annotations to display in the chart.</p>
  * @field 
  * @type {array}
  * @option
  */
wijchartcore_options.prototype.annotations = [];
/** <p class='defaultValue'>Default value: null</p>
  * <p>Sets the width of the chart in pixels.</p>
  * @field 
  * @type {?number}
  * @option 
  * @remarks
  * Note that this value overrides any value you may set in the &lt;div&gt; element that 
  * you use in the body of the HTML page
  * If you specify a width in the &lt;div&gt; element that is different from this value, 
  * the chart and its border go out of synch.
  */
wijchartcore_options.prototype.width = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Sets the height of the barchart in pixels.</p>
  * @field 
  * @type {?number}
  * @option 
  * @remarks
  * Note that this value overrides any value you may set in the &lt;div&gt; element that 
  * you use in the body of the HTML page. If you specify a height in the &lt;div&gt; element that 
  * is different from this value, the chart and its border go out of synch.
  */
wijchartcore_options.prototype.height = null;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>A value that indicator the culture to format the chart text.</p>
  * @field 
  * @type {string}
  * @option
  */
wijchartcore_options.prototype.culture = "";
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Assigns the string value of the culture calendar that appears on the calendar.
  * This option must work with culture option.</p>
  * @field 
  * @type {string}
  * @option
  */
wijchartcore_options.prototype.cultureCalendar = "";
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value that indicates whether to redraw the chart automatically when resizing the chart element.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijchartcore_options.prototype.autoResize = true;
/** <p class='defaultValue'>Default value: []</p>
  * <p>Creates an array of series objects that contain data values and labels to display in the chart.</p>
  * @field 
  * @type {array}
  * @option
  */
wijchartcore_options.prototype.seriesList = [];
/** <p class='defaultValue'>Default value: []</p>
  * <p>Sets an array of style objects to use in rendering the bars for each series in the chart.</p>
  * @field 
  * @type {RaphaelStyle[]}
  * @option 
  * @remarks
  * Each style object in the array applies to one series in your seriesList,
  * so you need specify only as many style objects as you have series objects 
  * in your seriesList. The style is also used in the legend entry for the series
  * in your seriesList. The style is also used in the legend entry for the series
  */
wijchartcore_options.prototype.seriesStyles = [];
/** <p class='defaultValue'>Default value: []</p>
  * <p>Sets an array of styles to use in rendering bars in the chart when you hover over them.</p>
  * @field 
  * @type {array}
  * @option 
  * @remarks
  * Each style object in the array applies to a series in your seriesList,so you need 
  * specify only as many style objects as you have series objects in your seriesList
  */
wijchartcore_options.prototype.seriesHoverStyles = [];
/** <p class='defaultValue'>Default value: 25</p>
  * <p>Sets the amount of space in pixels between the chart area and the top edge of the &lt;div&gt; that defines the widget</p>
  * @field 
  * @type {number}
  * @option
  */
wijchartcore_options.prototype.marginTop = 25;
/** <p class='defaultValue'>Default value: 25</p>
  * <p>Sets the amount of space in pixels between the chart area and the right edge of the &lt;div&gt; that defines the widget</p>
  * @field 
  * @type {number}
  * @option
  */
wijchartcore_options.prototype.marginRight = 25;
/** <p class='defaultValue'>Default value: 25</p>
  * <p>Sets the amount of space in pixels between the chart area and the bottom edge of the &lt;div&gt; that defines the widget.</p>
  * @field 
  * @type {number}
  * @option
  */
wijchartcore_options.prototype.marginBottom = 25;
/** <p class='defaultValue'>Default value: 25</p>
  * <p>Sets the amount of space in pixels between the chart area and the left edge of the &lt;div&gt; that defines the widget</p>
  * @field 
  * @type {number}
  * @option
  */
wijchartcore_options.prototype.marginLeft = 25;
/** <p>Creates an object to use for the fallback style of any chart text that does not 
  * have other style options set.</p>
  * @field 
  * @option 
  * @remarks
  * Each type of text in the chart has a different set of styles applied by default, but if those styles are set to null,
  * or if a particular style option is not set by default, the chart falls back on any style options you set in this option. 
  * Styles for specific types of chart text that may use this option as a fallback style are set in the following options:
  * axis x labels style
  * axis x textStyle
  * axis y labels style
  * axis y textStyle
  * chartLabelStyle
  * footer textStyle
  * header textStyle
  * hint contentStyle
  * hint titleStyle
  * legend textStyle
  * legend titleStyle
  * The style is defined in Raphael here is the documentation: http://raphaeljs.com/reference.html#Element.attr.
  * The style is the “attr” method’s parameters.
  */
wijchartcore_options.prototype.textStyle = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_title.html'>wijmo.chart.chart_title</a></p>
  * <p>Sets up the object to use as the header of the barchart.</p>
  * @field 
  * @type {wijmo.chart.chart_title}
  * @option
  */
wijchartcore_options.prototype.header = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_title.html'>wijmo.chart.chart_title</a></p>
  * <p>Sets up the object to use as the footer of the barchart.</p>
  * @field 
  * @type {wijmo.chart.chart_title}
  * @option
  */
wijchartcore_options.prototype.footer = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_legend.html'>wijmo.chart.chart_legend</a></p>
  * <p>Creates a legend object to display with the chart.</p>
  * @field 
  * @type {wijmo.chart.chart_legend}
  * @option 
  * @remarks
  * By default, each series that you create in the seriesList is represented by a color in the legend,
  * using the seriesList label that you specify. If you do not specify a label,
  * it is labeled "undefined." If you do not want a series to appear in the legend, 
  * you can set the seriesList legendEntry attribute to false.
  * By default, users can click a legend entry to toggle the data series it 
  * represents in the chart.See Clickable Legend for code that allows you to disable this function.
  * @example
  * // This code creates a chart with a legend that is positioned below the chart (south), 
  * // with series labels and colors in a row (horizontal), a grey outline and lighter
  * // grey fill (style), has a title that reads "Legend" (text), has 5 pixels of
  * // space around the text (textMargin), has series labels in a black 12-point font
  * // (textStyle), and has a 14-point font title (titleStyle)
  *    $(document).ready(function () {
  *        $("#wijbarchart").wijbarchart({
  *         legend: {
  *         compass: "south",
  *         orientation: "horizontal",
  *         style: {fill: "gainsboro", stroke: "grey"},
  *         text: "Legend",
  *         textMargin: {left: 5, top: 5, right: 5, bottom: 5 },
  *         textStyle: {fill: "black", "font-size": 12},
  *         titleStyle: {"font-size": 14}
  *         },
  *         seriesList: [{
  *         label: "US",
  *         data: {
  *         x: ['PS3', 'XBOX360', 'Wii'],
  *         y: [12.35, 21.50, 30.56]
  *         }
  *         },
  *         {
  *         label: "Japan",
  *         data: {
  *         x: ['PS3', 'XBOX360', 'Wii'],
  *         y: [4.58, 1.23, 9.67]
  *         }
  *         },
  *         {
  *         label: "Other",
  *         data: {
  *         x: ['PS3', 'XBOX360', 'Wii'],
  *         y: [31.59, 37.14, 65.32]
  *         }
  *         }],
  *     });
  * });
  */
wijchartcore_options.prototype.legend = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_axes.html'>wijmo.chart.chart_axes</a></p>
  * <p>A value that contains all of the information to create the X and Y axes of the chart</p>
  * @field 
  * @type {wijmo.chart.chart_axes}
  * @option
  */
wijchartcore_options.prototype.axis = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_hint.html'>wijmo.chart.chart_hint</a></p>
  * <p>Creates an object to use as the tooltip, or hint, when the mouse is over a chart element.</p>
  * @field 
  * @type {wijmo.chart.chart_hint}
  * @option 
  * @remarks
  * By default, it displays the value of the seriesList label and Y data for the chart
  */
wijchartcore_options.prototype.hint = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_indicator.html'>wijmo.chart.chart_indicator</a></p>
  * <p>Sets up an object that can display an indicator line running horizontally/vertically through the center
  * of each chart element in the chart when the user clicks the chart element.</p>
  * @field 
  * @type {wijmo.chart.chart_indicator}
  * @option
  */
wijchartcore_options.prototype.indicator = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value that indicates whether to show default chart labels.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijchartcore_options.prototype.showChartLabels = true;
/** <p>Sets all of the style options of the chart labels that show the value of each chart element.</p>
  * @field 
  * @option 
  * @remarks
  * The style is defined in Raphael here is the documentation: http://raphaeljs.com/reference.html#Element.attr. 
  * The style is the “attr” method’s parameters.
  */
wijchartcore_options.prototype.chartLabelStyle = null;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Sets the numeric format of the chart labels that show the value of each chart element. You can use Standard
  * Numeric Format Strings.</p>
  * @field 
  * @type {string}
  * @option
  */
wijchartcore_options.prototype.chartLabelFormatString = "";
/** <p class='defaultValue'>Default value: null</p>
  * <p>A value that indicates a function which is used to obtain the content part of the chart label for each chart element.</p>
  * @field 
  * @type {function}
  * @option
  */
wijchartcore_options.prototype.chartLabelFormatter = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Sets a value indicating whether you can set the font-family of the text using a class instead of options.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * Note: This applies only to the font-family in the current version.
  */
wijchartcore_options.prototype.disableDefaultTextStyle = false;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value that indicates whether to show a shadow around the edge of the chart.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijchartcore_options.prototype.shadow = true;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Sets the array to use as a source for data that you can bind to the axes in your seriesList.</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * Use the data option, and bind values to your X and Y axes. For more information please see the datasource demo:
  * http://wijmo.com/demo/explore/?widget=BarChartsample=Array%20as%20datasource.
  */
wijchartcore_options.prototype.dataSource = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Bind a field to each series's data x array</p>
  * @field 
  * @type {object}
  * @option
  */
wijchartcore_options.prototype.data = null;
/** <p>Gets or sets whether series objects are stacked.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * default value is "false"
  */
wijchartcore_options.prototype.stacked = null;
/** <p>Gets or sets whether each value contributes to the total with the relative size of each series representing its contribution to the total.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * default value is "false"
  */
wijchartcore_options.prototype.is100Percent = null;
/** <p>Gets or sets whether control is disabled.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * default value is "false"
  */
wijchartcore_options.prototype.disabled = null;
/** <p>Gets or sets whether series are displayed horizontally.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * default value is "true"
  */
wijchartcore_options.prototype.horizontal = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_animation.html'>wijmo.chart.chart_animation</a></p>
  * <p>Creates the animation object to use when the seriesList data changes.</p>
  * @field 
  * @type {wijmo.chart.chart_animation}
  * @option 
  * @remarks
  * This allows you to visually show changes in data for the same series.
  * Note: This animation does not appear when you first load or reload the page--it only occurs when data changes.
  * @example
  * // This code creates a chart with random data that regenerates when you click the button created in the
  * second code snippet below
  *  $(document).ready(function () {
  * $("#wijbarchart").wijbarchart({
  * seriesList: [createRandomSeriesList("2013")],
  * seriesTransition: {
  *     duration: 800,
  *     easing: "easeOutBounce"
  * }
  * });
  * } );
  * function reload() {
  *     $("#wijbarchart").wijbarchart("option", "seriesList", [createRandomSeriesList("2013")]);
  * }
  * function createRandomSeriesList(label) {
  *     var data = [],
  *         randomDataValuesCount = 12,
  *         labels = ["January", "February", "March", "April", "May", "June",
  *             "July", "August", "September", "October", "November", "December"],
  *         idx;
  *     for (idx = 0; idx &lt; randomDataValuesCount; idx++) {
  *         data.push(Math.round(Math.random() * 100));
  *     }
  *     return {
  *         label: label,
  *         legendEntry: false,
  *         data: { x: labels, y: data }
  *     };
  * }
  */
wijchartcore_options.prototype.seriesTransition = null;
/** This event fires before the series changes. This event can be cancelled. "return false;" to cancel the event.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.chart.IBeforeSeriesChangeEventArgs} data Information about an event
  */
wijchartcore_options.prototype.beforeSeriesChange = null;
/** This event fires when the series changes.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.chart.ISeriesChangedEventArgs} data Information about an event
  */
wijchartcore_options.prototype.seriesChanged = null;
/** This event fires before the canvas is painted. This event can be cancelled. "return false;" to cancel the event.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijchartcore_options.prototype.beforePaint = null;
/** This event fires after the canvas is painted.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijchartcore_options.prototype.painted = null;
wijmo.chart.wijchartcore.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijchartcore_options());
$.widget("wijmo.wijchartcore", $.wijmo.widget, wijmo.chart.wijchartcore.prototype);
/** @class wijchartcore_options
  * @namespace wijmo.chart
  */
wijmo.chart.wijchartcore_options = function () {};
/** @class wijchartcore_css
  * @namespace wijmo.chart
  * @extends wijmo.wijmo_css
  */
wijmo.chart.wijchartcore_css = function () {};
wijmo.chart.wijchartcore_css.prototype = new wijmo.wijmo_css();
/** @interface chart_title
  * @namespace wijmo.chart
  */
wijmo.chart.chart_title = function () {};
/** <p>A value that indicates the text to display in the header.</p>
  * @field 
  * @type {string}
  * @remarks
  * If you leave this as an empty string, no header renders, regardless of any other header attributes.
  */
wijmo.chart.chart_title.prototype.text = null;
/** <p>A value that indicates all of the style attributes to use in rendering the header.</p>
  * @field 
  * @remarks
  * The style is defined in Raphael here is the documentation: http://raphaeljs.com/reference.html#Element.attr
  * The style is the “attr” method’s parameters.
  */
wijmo.chart.chart_title.prototype.style = null;
/** <p>A value that indicates the style of the header text.</p>
  * @field 
  * @remarks
  * Note: Any style options set in the fallback textStyle option are used for any
  * style options that are not set explicitly (or set by default) in this option.
  */
wijmo.chart.chart_title.prototype.textStyle = null;
/** <p>A value that indicates the position of the header in relation to the chart.</p>
  * @field 
  * @type {string}
  * @remarks
  * Valid Values:
  * "north" Renders the header above the chart.
  * "south" Renders the header below the chart.
  * "east" Renders the header to the right of the chart, with the text rotated 90 degrees.
  * "west" Renders the header to the left of the chart, with the text rotated 270 degrees.
  */
wijmo.chart.chart_title.prototype.compass = null;
/** <p>A value that indicates the visibility of the header.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.chart_title.prototype.visible = null;
/** @interface chart_legend
  * @namespace wijmo.chart
  */
wijmo.chart.chart_legend = function () {};
/** <p>A value that indicates the text to use as the title at the top of the legend.</p>
  * @field 
  * @type {string}
  * @remarks
  * Set style properties on this text using the titleStyle attribute.
  */
wijmo.chart.chart_legend.prototype.text = null;
/** <p>A value in pixels that indicates the amount of space to leave around the text inside the legend.</p>
  * @field
  */
wijmo.chart.chart_legend.prototype.textMargin = null;
/** <p>A value that indicates the background color (fill) and border (stroke) of the legend.
  * The style is defined in Raphael here is the documentation: http://raphaeljs.com/reference.html#Element.attr. 
  * The style is the 'attr' method's parameters.</p>
  * @field
  */
wijmo.chart.chart_legend.prototype.style = null;
/** <p>A value that indicates the width of the legend text.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.chart_legend.prototype.textWidth = null;
/** <p>A value that indicates the style of the series label text. The text values come from the seriesList labels.</p>
  * @field 
  * @remarks
  * Note: Any style options set in the fallback textStyle option are used for
  * any style options that are not set explicitly (or set by default) in this option.
  * The style is defined in Raphael here is the documentation: http://raphaeljs.com/reference.html#Element.attr. 
  * The style is the 'attr' method's parameters.
  */
wijmo.chart.chart_legend.prototype.textStyle = null;
/** <p>A value that indicates the style of the legend title. The text for the title is set in the text 
  * attribute of the legend.</p>
  * @field 
  * @remarks
  * Note: Any style options set in the fallback textStyle option are used for any
  * style options that are not set explicitly (or set by default) in this option.
  * The style is defined in Raphael here is the documentation: http://raphaeljs.com/reference.html#Element.attr. 
  * The style is the 'attr' method's parameters.
  */
wijmo.chart.chart_legend.prototype.titleStyle = null;
/** <p>A value that indicates the compass position of the legend.</p>
  * @field 
  * @type {string}
  * @remarks
  * Valid Values: "north", "south", "east", "west"
  */
wijmo.chart.chart_legend.prototype.compass = null;
/** <p>A value that indicates the orientation of the legend.</p>
  * @field 
  * @type {string}
  * @remarks
  * Vertical orientation generally works better with an east or west compass setting for the 
  * legend, while horizontal is more useful with a north or south compass setting and a small
  * number of series in the seriesList.
  * Valid Values:
  * "horizontal" Displays series labels and colors in a row.
  * "vertical" Displays series labels and colors in a column.
  */
wijmo.chart.chart_legend.prototype.orientation = null;
/** <p>A value that indicates whether to show the legend. Set this value to false to hide the legend.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.chart_legend.prototype.visible = null;
/** <p>A value that indicates whether to reversed the order of legend list. Set this value to true to reversed the order of legend list.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.chart_legend.prototype.reversed = null;
/** <p>A value that indicates the size of the legend.</p>
  * @field
  */
wijmo.chart.chart_legend.prototype.size = null;
/** @interface chart_indicator
  * @namespace wijmo.chart
  */
wijmo.chart.chart_indicator = function () {};
/** <p>A value that indicates whether to show indicator lines when the user clicks a chart element in the chart.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.chart_indicator.prototype.visible = null;
/** <p>A value that contains the fill color and outline color (stroke) of the indicator lines.</p>
  * @field 
  * @remarks
  * Note that when you set the stroke color of the indicator line, it extends this color to the outline
  * of the #hint:hint for the duration of the click.
  */
wijmo.chart.chart_indicator.prototype.style = null;
/** @interface chart_hint
  * @namespace wijmo.chart
  */
wijmo.chart.chart_hint = function () {};
/** <p>A value that indicates whether to show the tooltip.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * Set this value to false to hide hints even when the mouse is over a chart element.
  */
wijmo.chart.chart_hint.prototype.enable = null;
/** <p>A value that appears in the content part of the tooltip or a function which is
  * used to get a value for the tooltip shown.</p>
  * @field 
  * @remarks
  * If you do not set the content here, the hint displays content in the following order:
  * The hint option's title attribute
  * The seriesList option's label attribute, which shows "undefined" if you do not provide a label value
  * The seriesList option's data y attribute
  * You can format the numeric Y data in this attribute using a function
  */
wijmo.chart.chart_hint.prototype.content = null;
/** <p>A value that indicates the style of content text.</p>
  * @field 
  * @remarks
  * Note: Any style options set in the fallback textStyle option are used for any style
  * options that are not set explicitly (or set by default) in this option.
  * The style is defined in Raphael here is the documentation: http://raphaeljs.com/reference.html#Element.attr.
  * The style is the 'attr' method's parameters.
  */
wijmo.chart.chart_hint.prototype.contentStyle = null;
/** <p>A text value (or a function returning a text value) to display in the hint on a line
  * above the content.</p>
  * @field
  */
wijmo.chart.chart_hint.prototype.title = null;
/** <p>A value that indicates the style to use for the hint title text.</p>
  * @field 
  * @remarks
  * The style is defined in Raphael here is the documentation: http://raphaeljs.com/reference.html#Element.attr. 
  * he style is the 'attr' method's parameters.
  */
wijmo.chart.chart_hint.prototype.titleStyle = null;
/** <p>A value that indicates the fill color or gradient and outline thickness and color
  * (stroke) of the rectangle used to display the hint.</p>
  * @field 
  * @remarks
  * The style is defined in Raphael here is the documentation: http://raphaeljs.com/reference.html#Element.attr.
  * The style is the 'attr' method's parameters.
  */
wijmo.chart.chart_hint.prototype.style = null;
/** <p>A value that indicates the effect to use when showing or hiding the hint when showAnimated 
  * or hideAnimated is not specified.</p>
  * @field 
  * @type {string}
  * @remarks
  * The only animation style included is "fade." If you want a different one, you can create a custom animation
  */
wijmo.chart.chart_hint.prototype.animated = null;
/** <p>A value that indicates the animation effect to use when the hint appears after you mouse into the chart element.</p>
  * @field 
  * @type {string}
  * @remarks
  * This allows you to use a different effect when you mouse out of a bar than when you mouse in. (See also 
  * hideAnimated.) If you set this value to null, the animated property supplies the animation effect to use. 
  * The only animation style included is "fade." If you want a different one, you can create a custom animation
  */
wijmo.chart.chart_hint.prototype.showAnimated = null;
/** <p>A value that indicates the animation effect to use when the hint goes away after you mouse out of the chart element.</p>
  * @field 
  * @type {string}
  * @remarks
  * This allows you to use a different effect when you mouse into a chart elemrnt than when you mouse out.
  * (See also showAnimated.) If you set this value to null, the animated property supplies the animation 
  * effect to use. The only animation style included is "fade." If you want a different one, you can create 
  * a custom animation
  */
wijmo.chart.chart_hint.prototype.hideAnimated = null;
/** <p>A value that indicates the number of milliseconds it takes to show or hide the hint when you mouse over
  * or mouse out of a bar with the showDuration or hideDuration attribute set to null.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.chart_hint.prototype.duration = null;
/** <p>A value that indicates the number of milliseconds it takes for the indicated animation effect to completely
  * show the tooltip.</p>
  * @field 
  * @type {number}
  * @remarks
  * This allows you to use a different number of milliseconds when you mouse out of a bar than when you mouse in.
  */
wijmo.chart.chart_hint.prototype.showDuration = null;
/** <p>A value that indicates the number of milliseconds it takes for the indicated animation effect to completely
  * hide the tooltip.</p>
  * @field 
  * @type {number}
  * @remarks
  * This allows you to use a different number of milliseconds when you mouse into a bar than when you mouse out.
  */
wijmo.chart.chart_hint.prototype.hideDuration = null;
/** <p>A value that indicates the easing animation used to show or hide the hint when you mouse over or mouse out
  * of a bar with the showEasing or hideEasing attribute set to null.</p>
  * @field 
  * @type {string}
  * @remarks
  * The easing is defined in Raphael, the documentation is:http://raphaeljs.com/reference.html#Raphael.easing_formulas
  */
wijmo.chart.chart_hint.prototype.easing = null;
/** <p>A value that indicates the easing effect to use when the hint appears after you mouse into the chart element.</p>
  * @field 
  * @type {string}
  * @remarks
  * This allows you to use a different effect when you mouse out of a bar than when you mouse in. (See also 
  * hideEasing.) If you set this value to null, the easing property supplies the easing effect to use.
  */
wijmo.chart.chart_hint.prototype.showEasing = null;
/** <p>A value that indicates the easing effect to use when the hint goes away after you mouse out of the chart element.</p>
  * @field 
  * @type {string}
  * @remarks
  * This allows you to use a different effect when you mouse into a bar than when you mouse out. (See also
  * showEasing.) If you set this value to null, the easing property supplies the easing effect to use.
  */
wijmo.chart.chart_hint.prototype.hideEasing = null;
/** <p>A value that indicates the number of milliseconds to wait before showing the hint after the mouse moves
  * into the chart element</p>
  * @field 
  * @type {number}
  * @remarks
  * This allows you to use a different number of milliseconds when you mouse out of a bar than when you mouse in.
  */
wijmo.chart.chart_hint.prototype.showDelay = null;
/** <p>A value that indicates the number of milliseconds to wait before hiding the hint after the mouse leaves
  * the chart element</p>
  * @field 
  * @type {number}
  * @remarks
  * This allows you to use a different number of milliseconds when you mouse into a bar than when you mouse out.
  */
wijmo.chart.chart_hint.prototype.hideDelay = null;
/** <p>A value that indicates the compass position of the callout (the small triangle that points from the main
  * hint box to the bar it describes) in relation to the hint box.</p>
  * @field 
  * @type {string}
  * @remarks
  * Valid Values: west, east, south, southeast, southwest, northeast and northwest
  */
wijmo.chart.chart_hint.prototype.compass = null;
/** <p>A value that indicates the horizontal distance in pixels from the mouse pointer to the callout triangle
  * of the hint.</p>
  * @field 
  * @type {number}
  * @remarks
  * The position of the callout triangle depends on the compass setting of the hint callout.
  */
wijmo.chart.chart_hint.prototype.offsetX = null;
/** <p>A value that indicates the vertical distance in pixels from the mouse pointer to the callout triangle
  * of the hint.</p>
  * @field 
  * @type {number}
  * @remarks
  * The position of the callout triangle depends on the compass setting of the hint callout.
  */
wijmo.chart.chart_hint.prototype.offsetY = null;
/** <p>Determines whether to show the callout element, the small triangle that points from the main hint box
  * to the bar it describes.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * To change the appearance of the callout, see these other hint attributes: calloutFilled,calloutFilledStyle,
  * and compass.
  */
wijmo.chart.chart_hint.prototype.showCallout = null;
/** <p>Determines whether to fill the callout (the small triangle that points from the main hint box to the bar
  * it describes).</p>
  * @field 
  * @type {boolean}
  * @remarks
  * If you set it to true, the callout triangle uses the colors you specify in the calloutFilledStyle
  * attribute. Otherwise, it takes on the colors of the style attribute of the hint.
  */
wijmo.chart.chart_hint.prototype.calloutFilled = null;
/** <p>A value that indicates the style to use in rendering the callout (the small triangle that points from the
  * main hint box to the bar it describes).</p>
  * @field 
  * @remarks
  * In order for this attribute of the callout to take effect, you must also set the calloutFilled attribute
  * to true. Otherwise, it takes on the colors of the style attribute of the hint.
  * The style is defined in Raphael here is the documentation: http://raphaeljs.com/reference.html#Element.attr.
  * The style is the “attr” method’s parameters.
  */
wijmo.chart.chart_hint.prototype.calloutFilledStyle = null;
/** <p>Determines how to close the tooltip. Behaviors include auto or sticky.</p>
  * @field 
  * @remarks
  * Options: "auto", "none" and "sticky".
  */
wijmo.chart.chart_hint.prototype.closeBehavior = null;
/** <p>Determines tooltip should related to mouse or element.</p>
  * @field 
  * @remarks
  * Options: "mouse" and "element".
  */
wijmo.chart.chart_hint.prototype.relativeTo = null;
/** <p>Determine whether to use a tooltip in html style.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * If you want display the tooltip with html style, set this property as true. 
  * Note that: This property must work with wijtooltip, if wijmo.wijtooltip is not included,
  * the tooltip will display as SVG whatever this property is.
  */
wijmo.chart.chart_hint.prototype.isContentHtml = null;
/** @interface chart_axes
  * @namespace wijmo.chart
  */
wijmo.chart.chart_axes = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_axis.html'>wijmo.chart.chart_axis</a></p>
  * <p>An object containing all of the information to create the X axis of the chart.</p>
  * @field 
  * @type {wijmo.chart.chart_axis}
  */
wijmo.chart.chart_axes.prototype.x = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_axis.html'>wijmo.chart.chart_axis</a></p>
  * <p>An object containing all of the information to create the Y axis of the chart.</p>
  * @field 
  * @type {wijmo.chart.chart_axis}
  */
wijmo.chart.chart_axes.prototype.y = null;
/** @interface chart_axis
  * @namespace wijmo.chart
  */
wijmo.chart.chart_axis = function () {};
/** <p>Sets the alignment of the X axis title text.</p>
  * @field 
  * @type {string}
  * @remarks
  * Options are 'center', 'near', 'far'.
  */
wijmo.chart.chart_axis.prototype.alignment = null;
/** <p>A value that indicates the style of the X axis.</p>
  * @field 
  * @remarks
  * The style is defined in Raphael here is the documentation:http://raphaeljs.com/reference.html#Element.attr.
  * The style is the “attr” method’s parameters.
  */
wijmo.chart.chart_axis.prototype.style = null;
/** <p>A value that indicates the visibility of the X axis.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.chart_axis.prototype.visible = null;
/** <p>A value that indicates the visibility of the X axis text.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.chart_axis.prototype.textVisible = null;
/** <p>Sets the text to use for the X axis title.</p>
  * @field 
  * @type {string}
  */
wijmo.chart.chart_axis.prototype.text = null;
/** <p>A value that indicates the style of text of the X axis.</p>
  * @field 
  * @remarks
  * Note: Any style options set in the fallback textStyle option are used for
  * any style options that are not set explicitly (or set by default) in this option.
  * The style is defined in Raphael here is the documentation: http://raphaeljs.com/reference.html#Element.attr. 
  * The style is the “attr” method’s parameters.
  */
wijmo.chart.chart_axis.prototype.textStyle = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_axis_label.html'>wijmo.chart.chart_axis_label</a></p>
  * <p>A value that provides information for the labels.</p>
  * @field 
  * @type {wijmo.chart.chart_axis_label}
  */
wijmo.chart.chart_axis.prototype.labels = null;
/** <p>A value that indicates where to draw the X axis using the four points of a compass.</p>
  * @field 
  * @type {string}
  * @remarks
  * "north" Draws the axis along the top edge of the chart.
  * "south" Draws the axis along the bottom edge of the chart.
  * "east" Draws the axis along the right edge of the chart.
  * "west" Draws the axis along the left edge of the chart.
  */
wijmo.chart.chart_axis.prototype.compass = null;
/** <p>A value that indicates whether to calculate the axis minimum value automatically.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * If you set this value to false, in order to show major tick marks on the axis, you must 
  * specify a value for the min option.
  */
wijmo.chart.chart_axis.prototype.autoMin = null;
/** <p>A value that indicates whether to calculate the axis maximum value automatically.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * If you set this value to false, in order to show major tick marks on the axis, you must specify a 
  * value for the max option.
  */
wijmo.chart.chart_axis.prototype.autoMax = null;
/** <p>A value that indicates the minimum value of the axis.</p>
  * @field 
  * @type {?number}
  */
wijmo.chart.chart_axis.prototype.min = null;
/** <p>A value that indicates the maximum value of the axis.</p>
  * @field 
  * @type {?number}
  */
wijmo.chart.chart_axis.prototype.max = null;
/** <p>A value that indicates the origin value of the X axis.</p>
  * @field 
  * @type {?number}
  */
wijmo.chart.chart_axis.prototype.origin = null;
/** <p>A value that indicates whether to calculate the major tick mark values automatically.</p>
  * @field 
  * @type {boolean}
  * @remarks
  * If you set this value to false, in order to show major tick marks on the axis, you must 
  * specify a value for the unitMajor option.
  */
wijmo.chart.chart_axis.prototype.autoMajor = null;
/** <p>A value that indicates whether to calculate the minor tick mark values automatically</p>
  * @field 
  * @type {boolean}
  * @remarks
  * If you set this value to false, in order to show minor tick marks on the axis, you must 
  * specify a value for the unitMinor option.
  */
wijmo.chart.chart_axis.prototype.autoMinor = null;
/** <p>A value that indicates the units between major tick marks.</p>
  * @field 
  * @type {?number}
  */
wijmo.chart.chart_axis.prototype.unitMajor = null;
/** <p>A value that indicates the units between minor tick marks.</p>
  * @field 
  * @type {?number}
  */
wijmo.chart.chart_axis.prototype.unitMinor = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_axis_grid.html'>wijmo.chart.chart_axis_grid</a></p>
  * <p>A value that provides information for the major grid line.</p>
  * @field 
  * @type {wijmo.chart.chart_axis_grid}
  */
wijmo.chart.chart_axis.prototype.gridMajor = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_axis_grid.html'>wijmo.chart.chart_axis_grid</a></p>
  * <p>A value that provides information for the minor grid line.</p>
  * @field 
  * @type {wijmo.chart.chart_axis_grid}
  */
wijmo.chart.chart_axis.prototype.gridMinor = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_axis_tick.html'>wijmo.chart.chart_axis_tick</a></p>
  * <p>Creates an object with all of the settings to use in drawing tick marks that appear next to the numeric 
  * labels for major values along the X axis.</p>
  * @field 
  * @type {wijmo.chart.chart_axis_tick}
  */
wijmo.chart.chart_axis.prototype.tickMajor = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_axis_tick.html'>wijmo.chart.chart_axis_tick</a></p>
  * <p>A value that provides information for the minor tick.</p>
  * @field 
  * @type {wijmo.chart.chart_axis_tick}
  */
wijmo.chart.chart_axis.prototype.tickMinor = null;
/** <p>The annoMethod option indicates how to label values along the axis.</p>
  * @field 
  * @type {string}
  * @remarks
  * Valid Values:
  * "values" Uses numeric seriesList data values to annotate the axis.
  * "valueLabels" Uses the array of string values that you provide in the valueLabels option to 
  * annotate the axis.
  */
wijmo.chart.chart_axis.prototype.annoMethod = null;
/** <p>The annoFormatString option uses Standard Numeric Format Strings to provide formatting for numeric
  * values in axis labels.</p>
  * @field 
  * @type {string}
  */
wijmo.chart.chart_axis.prototype.annoFormatString = null;
/** <p>The valueLabels option shows a collection of valueLabels for the X axis.</p>
  * @field 
  * @type {array}
  * @remarks
  * If the annoMethod is "values", this option will lost effect, If the annoMethod is "valueLabels", 
  * the axis's label will set to this option's value.
  */
wijmo.chart.chart_axis.prototype.valueLabels = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
wijmo.chart.chart_axis.prototype.length = null;
/** @interface chart_axis_label
  * @namespace wijmo.chart
  */
wijmo.chart.chart_axis_label = function () {};
/** <p>A value that indicates the style of major text of the X axis.</p>
  * @field 
  * @remarks
  * Note: Any style options set in the fallback textStyle option are used for any style 
  * options that are not set explicitly (or set by default) in this option.
  * The style is defined in Raphael here is the documentation: http://raphaeljs.com/reference.html#Element.attr. 
  * The style is the “attr” method’s parameters.
  */
wijmo.chart.chart_axis_label.prototype.style = null;
/** <p>A value that indicates the alignment of major text of the X axis.</p>
  * @field 
  * @type {string}
  * @remarks
  * Options are 'near', 'center' and 'far'.
  */
wijmo.chart.chart_axis_label.prototype.textAlign = null;
/** <p>A value that indicates the width of major text of the X axis.</p>
  * @field 
  * @type {number}
  * @remarks
  * If the value is null, then the width will be calculated automatically.
  */
wijmo.chart.chart_axis_label.prototype.width = null;
/** @interface chart_axis_grid
  * @namespace wijmo.chart
  */
wijmo.chart.chart_axis_grid = function () {};
/** <p>A value that indicates the visibility of the major/minor grid line.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.chart_axis_grid.prototype.visible = null;
/** <p>A value that indicates the style of the major grid line</p>
  * @field 
  * @remarks
  * The style is defined in Raphael here is the documentation: http://raphaeljs.com/reference.html#Element.attr.
  * The style is the “attr” method’s parameters.
  */
wijmo.chart.chart_axis_grid.prototype.style = null;
/** @interface chart_axis_tick
  * @namespace wijmo.chart
  */
wijmo.chart.chart_axis_tick = function () {};
/** <p>A value that indicates the position of the major tick mark in relation to the axis.</p>
  * @field 
  * @type {string}
  * @remarks
  * Valid Values: none, inside, outside, cross
  */
wijmo.chart.chart_axis_tick.prototype.position = null;
/** <p>A value that indicates the style of major tick mark</p>
  * @field 
  * @remarks
  * The style is defined in Raphael here is the documentation: http://raphaeljs.com/reference.html#Element.attr. 
  * The style is the “attr” method’s parameters.
  */
wijmo.chart.chart_axis_tick.prototype.style = null;
/** <p>A value that indicates an integral factor for major tick mark length.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.chart_axis_tick.prototype.factor = null;
/** @interface chart_animation
  * @namespace wijmo.chart
  */
wijmo.chart.chart_animation = function () {};
/** <p>A value that determines whether to show animation. Set this option to false in order to disable easing.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.chart_animation.prototype.enabled = null;
/** <p>The duration option defines the length of the animation effect in milliseconds.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.chart_animation.prototype.duration = null;
/** <p>Sets the type of animation easing effect that users experience when the wijbarchart series loads on the page</p>
  * @field 
  * @type {string}
  * @remarks
  * For example, the wijbarchart series can bounce several times as it loads.
  * The easing is defined in Raphael, the documentation is: http://raphaeljs.com/reference.html#Raphael.easing_formulas
  */
wijmo.chart.chart_animation.prototype.easing = null;
/** Contains information about wijchartcore.beforeSeriesChange event
  * @interface IBeforeSeriesChangeEventArgs
  * @namespace wijmo.chart
  */
wijmo.chart.IBeforeSeriesChangeEventArgs = function () {};
/** <p>The old series list before changes.</p>
  * @field 
  * @type {array}
  */
wijmo.chart.IBeforeSeriesChangeEventArgs.prototype.oldSeriesList = null;
/** <p>The new series list that will replace the old one.</p>
  * @field 
  * @type {array}
  */
wijmo.chart.IBeforeSeriesChangeEventArgs.prototype.newSeriesList = null;
/** Contains information about wijchartcore.seriesChanged event
  * @interface ISeriesChangedEventArgs
  * @namespace wijmo.chart
  */
wijmo.chart.ISeriesChangedEventArgs = function () {};
/** <p>An object that contains new series values.</p>
  * @field 
  * @type {object}
  */
wijmo.chart.ISeriesChangedEventArgs.prototype.data = null;
})()
})()
