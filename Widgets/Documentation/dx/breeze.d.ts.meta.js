var breeze = breeze || {};

(function () {
breeze.core = breeze.core || {};

(function () {
/** @interface ErrorCallback
  * @namespace breeze.core
  */
breeze.core.ErrorCallback = function () {};
/** @interface IEnum
  * @namespace breeze.core
  */
breeze.core.IEnum = function () {};
/** @param object
  * @returns {bool}
  */
breeze.core.IEnum.prototype.contains = function (object) {};
/** @param {string} name
  * @returns {breeze.core.EnumSymbol}
  */
breeze.core.IEnum.prototype.fromName = function (name) {};
/** @returns {string[]} */
breeze.core.IEnum.prototype.getNames = function () {};
/** @returns {breeze.core.EnumSymbol[]} */
breeze.core.IEnum.prototype.getSymbols = function () {};
/** @param {Object} obj
  * @param {function} kvfn
  * @returns {void}
  */
breeze.core.objectForEach = function (obj, kvfn) {};
/** @param {Object} target
  * @param {Object} source
  * @returns {Object}
  */
breeze.core.extend = function (target, source) {};
/** @param {string} propertyName
  * @param value
  * @returns {function}
  */
breeze.core.propEq = function (propertyName, value) {};
/** @param {string} propertyName
  * @returns {function}
  */
breeze.core.pluck = function (propertyName) {};
/** @param {any[]} a1
  * @param {any[]} a2
  * @param {function} equalsFn
  * @returns {bool}
  */
breeze.core.arrayEquals = function (a1, a2, equalsFn) {};
/** @param {any[]} a1
  * @param {function} predicate
  */
breeze.core.arrayFirst = function (a1, predicate) {};
/** @param {any[]} a1
  * @param {function} predicate
  * @returns {number}
  */
breeze.core.arrayIndexOf = function (a1, predicate) {};
/** @param {any[]} array
  * @param item
  * @param {bool} shouldRemoveMultiple
  */
breeze.core.arrayRemoveItem = function (array, item, shouldRemoveMultiple) {};
/** @param {any[]} array
  * @param {function} predicate
  * @param {bool} shouldRemoveMultiple
  */
breeze.core.arrayRemoveItem = function (array, predicate, shouldRemoveMultiple) {};
/** @param {any[]} a1
  * @param {any[]} a2
  * @param {function} callback
  * @returns {any[]}
  */
breeze.core.arrayZip = function (a1, a2, callback) {};
/** @param {string} libnames
  * @param {string} errMessage
  * @returns {Object}
  */
breeze.core.requireLib = function (libnames, errMessage) {};
/** @param {Object} obj
  * @param {string} property
  * @param tempValue
  * @param {function} fn
  */
breeze.core.using = function (obj, property, tempValue, fn) {};
/** @param {Function} fn */
breeze.core.memoize = function (fn) {};
/** @returns {string} */
breeze.core.getUuid = function () {};
/** @param {string} duration
  * @returns {number}
  */
breeze.core.durationToSeconds = function (duration) {};
/** @param o
  * @returns {bool}
  */
breeze.core.isDate = function (o) {};
/** @param o
  * @returns {bool}
  */
breeze.core.isGuid = function (o) {};
/** @param o
  * @returns {bool}
  */
breeze.core.isDuration = function (o) {};
/** @param o
  * @returns {bool}
  */
breeze.core.isFunction = function (o) {};
/** @param o
  * @returns {bool}
  */
breeze.core.isEmpty = function (o) {};
/** @param o
  * @returns {bool}
  */
breeze.core.isNumeric = function (o) {};
/** @param {string} str
  * @param {string} prefix
  * @returns {bool}
  */
breeze.core.stringStartsWith = function (str, prefix) {};
/** @param {string} str
  * @param {string} suffix
  * @returns {bool}
  */
breeze.core.stringEndsWith = function (str, suffix) {};
/** @param {string} format
  * @param {any[]} args
  * @returns {string}
  */
breeze.core.formatString = function (format, args) {};
})()
})()
var breeze = breeze || {};

(function () {
/** @interface Entity
  * @namespace breeze
  */
breeze.Entity = function () {};
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityAspect}
  */
breeze.Entity.prototype.entityAspect = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityType}
  */
breeze.Entity.prototype.entityType = null;
/** @interface ComplexObject
  * @namespace breeze
  */
breeze.ComplexObject = function () {};
/** <p>undefined</p>
  * @field 
  * @type {breeze.ComplexAspect}
  */
breeze.ComplexObject.prototype.complexAspect = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.ComplexType}
  */
breeze.ComplexObject.prototype.complexType = null;
/** @interface IProperty
  * @namespace breeze
  */
breeze.IProperty = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.IProperty.prototype.name = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.IStructuralType}
  */
breeze.IProperty.prototype.parentType = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.Validator[]}
  */
breeze.IProperty.prototype.validators = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
breeze.IProperty.prototype.isDataProperty = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
breeze.IProperty.prototype.isNavigationProperty = null;
/** @interface IStructuralType
  * @namespace breeze
  */
breeze.IStructuralType = function () {};
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataProperty[]}
  */
breeze.IStructuralType.prototype.complexProperties = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataProperty[]}
  */
breeze.IStructuralType.prototype.dataProperties = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.IStructuralType.prototype.name = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.IStructuralType.prototype.namespace = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.IStructuralType.prototype.shortName = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataProperty[]}
  */
breeze.IStructuralType.prototype.unmappedProperties = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.Validator[]}
  */
breeze.IStructuralType.prototype.validators = null;
/** @interface DataPropertyOptions
  * @namespace breeze
  */
breeze.DataPropertyOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.DataPropertyOptions.prototype.complexTypeName = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.DataPropertyOptions.prototype.concurrencyMode = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataTypeSymbol}
  */
breeze.DataPropertyOptions.prototype.dataType = null;
/** <p>undefined</p>
  * @field
  */
breeze.DataPropertyOptions.prototype.defaultValue = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
breeze.DataPropertyOptions.prototype.fixedLength = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
breeze.DataPropertyOptions.prototype.isNullable = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
breeze.DataPropertyOptions.prototype.isPartOfKey = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
breeze.DataPropertyOptions.prototype.isUnmapped = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
breeze.DataPropertyOptions.prototype.maxLength = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.DataPropertyOptions.prototype.name = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.DataPropertyOptions.prototype.nameOnServer = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.Validator[]}
  */
breeze.DataPropertyOptions.prototype.validators = null;
/** @interface DataServiceOptions
  * @namespace breeze
  */
breeze.DataServiceOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.DataServiceOptions.prototype.serviceName = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.DataServiceOptions.prototype.adapterName = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
breeze.DataServiceOptions.prototype.hasServerMetadata = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.JsonResultsAdapter}
  */
breeze.DataServiceOptions.prototype.jsonResultsAdapter = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
breeze.DataServiceOptions.prototype.useJsonp = null;
/** @interface QueryContext
  * @namespace breeze
  */
breeze.QueryContext = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.QueryContext.prototype.url = null;
/** <p>undefined</p>
  * @field
  */
breeze.QueryContext.prototype.query = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityManager}
  */
breeze.QueryContext.prototype.entityManager = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataService}
  */
breeze.QueryContext.prototype.dataService = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.QueryOptions}
  */
breeze.QueryContext.prototype.queryOptions = null;
/** @interface NodeContext
  * @namespace breeze
  */
breeze.NodeContext = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.NodeContext.prototype.nodeType = null;
/** @interface DataType
  * @namespace breeze
  * @extends breeze.core.IEnum
  */
breeze.DataType = function () {};
/** @param {string} typeName
  * @returns {breeze.DataTypeSymbol}
  */
breeze.DataType.prototype.toDataType = function (typeName) {};
/** @param date
  * @returns {Date}
  */
breeze.DataType.prototype.parseDateFromServer = function (date) {};
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataTypeSymbol}
  */
breeze.DataType.prototype.Binary = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataTypeSymbol}
  */
breeze.DataType.prototype.Boolean = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataTypeSymbol}
  */
breeze.DataType.prototype.Byte = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataTypeSymbol}
  */
breeze.DataType.prototype.DateTime = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataTypeSymbol}
  */
breeze.DataType.prototype.DateTimeOffset = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataTypeSymbol}
  */
breeze.DataType.prototype.Decimal = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataTypeSymbol}
  */
breeze.DataType.prototype.Double = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataTypeSymbol}
  */
breeze.DataType.prototype.Guid = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataTypeSymbol}
  */
breeze.DataType.prototype.Int16 = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataTypeSymbol}
  */
breeze.DataType.prototype.Int32 = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataTypeSymbol}
  */
breeze.DataType.prototype.Int64 = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataTypeSymbol}
  */
breeze.DataType.prototype.Single = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataTypeSymbol}
  */
breeze.DataType.prototype.String = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataTypeSymbol}
  */
breeze.DataType.prototype.Time = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataTypeSymbol}
  */
breeze.DataType.prototype.Undefined = null;
/** <p>undefined</p>
  * @field
  */
breeze.DataType.prototype.defaultValue = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
breeze.DataType.prototype.isNumeric = null;
/** @interface EntityAction
  * @namespace breeze
  * @extends breeze.core.IEnum
  */
breeze.EntityAction = function () {};
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityActionSymbol}
  */
breeze.EntityAction.prototype.AcceptChanges = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityActionSymbol}
  */
breeze.EntityAction.prototype.Attach = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityActionSymbol}
  */
breeze.EntityAction.prototype.AttachOnImport = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityActionSymbol}
  */
breeze.EntityAction.prototype.AttachOnQuery = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityActionSymbol}
  */
breeze.EntityAction.prototype.Clear = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityActionSymbol}
  */
breeze.EntityAction.prototype.Detach = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityActionSymbol}
  */
breeze.EntityAction.prototype.EntityStateChange = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityActionSymbol}
  */
breeze.EntityAction.prototype.MergeOnImport = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityActionSymbol}
  */
breeze.EntityAction.prototype.MergeOnSave = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityActionSymbol}
  */
breeze.EntityAction.prototype.MergeOnQuery = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityActionSymbol}
  */
breeze.EntityAction.prototype.PropertyChange = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityActionSymbol}
  */
breeze.EntityAction.prototype.RejectChanges = null;
/** @interface EntityByKeyResult
  * @namespace breeze
  */
breeze.EntityByKeyResult = function () {};
/** <p>undefined</p>
  * @field 
  * @type {breeze.Entity}
  */
breeze.EntityByKeyResult.prototype.entity = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityKey}
  */
breeze.EntityByKeyResult.prototype.entityKey = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
breeze.EntityByKeyResult.prototype.fromCache = null;
/** @interface EntityManagerOptions
  * @namespace breeze
  */
breeze.EntityManagerOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.EntityManagerOptions.prototype.serviceName = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataService}
  */
breeze.EntityManagerOptions.prototype.dataService = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.MetadataStore}
  */
breeze.EntityManagerOptions.prototype.metadataStore = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.QueryOptions}
  */
breeze.EntityManagerOptions.prototype.queryOptions = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.SaveOptions}
  */
breeze.EntityManagerOptions.prototype.saveOptions = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.ValidationOptions}
  */
breeze.EntityManagerOptions.prototype.validationOptions = null;
/** <p>undefined</p>
  * @field 
  * @type {Function}
  */
breeze.EntityManagerOptions.prototype.keyGeneratorCtor = null;
/** @interface EntityManagerProperties
  * @namespace breeze
  */
breeze.EntityManagerProperties = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.EntityManagerProperties.prototype.serviceName = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataService}
  */
breeze.EntityManagerProperties.prototype.dataService = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.QueryOptions}
  */
breeze.EntityManagerProperties.prototype.queryOptions = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.SaveOptions}
  */
breeze.EntityManagerProperties.prototype.saveOptions = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.ValidationOptions}
  */
breeze.EntityManagerProperties.prototype.validationOptions = null;
/** <p>undefined</p>
  * @field 
  * @type {Function}
  */
breeze.EntityManagerProperties.prototype.keyGeneratorCtor = null;
/** @interface ExecuteQuerySuccessCallback
  * @namespace breeze
  */
breeze.ExecuteQuerySuccessCallback = function () {};
/** @interface ExecuteQueryErrorCallback
  * @namespace breeze
  */
breeze.ExecuteQueryErrorCallback = function () {};
/** @interface SaveChangesSuccessCallback
  * @namespace breeze
  */
breeze.SaveChangesSuccessCallback = function () {};
/** @interface SaveChangesErrorCallback
  * @namespace breeze
  */
breeze.SaveChangesErrorCallback = function () {};
/** @interface OrderByClause
  * @namespace breeze
  */
breeze.OrderByClause = function () {};
/** @interface EntityState
  * @namespace breeze
  * @extends breeze.core.IEnum
  */
breeze.EntityState = function () {};
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityStateSymbol}
  */
breeze.EntityState.prototype.Added = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityStateSymbol}
  */
breeze.EntityState.prototype.Deleted = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityStateSymbol}
  */
breeze.EntityState.prototype.Detached = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityStateSymbol}
  */
breeze.EntityState.prototype.Modified = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityStateSymbol}
  */
breeze.EntityState.prototype.Unchanged = null;
/** @interface EntityTypeOptions
  * @namespace breeze
  */
breeze.EntityTypeOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.EntityTypeOptions.prototype.shortName = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.EntityTypeOptions.prototype.namespace = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.AutoGeneratedKeyType}
  */
breeze.EntityTypeOptions.prototype.autoGeneratedKeyType = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.EntityTypeOptions.prototype.defaultResourceName = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataProperty[]}
  */
breeze.EntityTypeOptions.prototype.dataProperties = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.NavigationProperty[]}
  */
breeze.EntityTypeOptions.prototype.navigationProperties = null;
/** @interface EntityTypeProperties
  * @namespace breeze
  */
breeze.EntityTypeProperties = function () {};
/** <p>undefined</p>
  * @field 
  * @type {breeze.AutoGeneratedKeyType}
  */
breeze.EntityTypeProperties.prototype.autoGeneratedKeyType = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.EntityTypeProperties.prototype.defaultResourceName = null;
/** @interface FetchStrategy
  * @namespace breeze
  * @extends breeze.core.IEnum
  */
breeze.FetchStrategy = function () {};
/** <p>undefined</p>
  * @field 
  * @type {breeze.StrategySymbol}
  */
breeze.FetchStrategy.prototype.FromLocalCache = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.StrategySymbol}
  */
breeze.FetchStrategy.prototype.FromServer = null;
/** @interface FilterQueryOp
  * @namespace breeze
  * @extends breeze.core.IEnum
  */
breeze.FilterQueryOp = function () {};
/** <p>undefined</p>
  * @field 
  * @type {breeze.FilterQueryOpSymbol}
  */
breeze.FilterQueryOp.prototype.Contains = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.FilterQueryOpSymbol}
  */
breeze.FilterQueryOp.prototype.EndsWith = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.FilterQueryOpSymbol}
  */
breeze.FilterQueryOp.prototype.Equals = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.FilterQueryOpSymbol}
  */
breeze.FilterQueryOp.prototype.GreaterThan = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.FilterQueryOpSymbol}
  */
breeze.FilterQueryOp.prototype.GreaterThanOrEqual = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.FilterQueryOpSymbol}
  */
breeze.FilterQueryOp.prototype.IsTypeOf = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.FilterQueryOpSymbol}
  */
breeze.FilterQueryOp.prototype.LessThan = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.FilterQueryOpSymbol}
  */
breeze.FilterQueryOp.prototype.LessThanOrEqual = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.FilterQueryOpSymbol}
  */
breeze.FilterQueryOp.prototype.NotEquals = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.FilterQueryOpSymbol}
  */
breeze.FilterQueryOp.prototype.StartsWith = null;
/** @interface MergeStrategy
  * @namespace breeze
  * @extends breeze.core.IEnum
  */
breeze.MergeStrategy = function () {};
/** <p>undefined</p>
  * @field 
  * @type {breeze.StrategySymbol}
  */
breeze.MergeStrategy.prototype.OverwriteChanges = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.StrategySymbol}
  */
breeze.MergeStrategy.prototype.PreserveChanges = null;
/** @interface MetadataStoreOptions
  * @namespace breeze
  */
breeze.MetadataStoreOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {breeze.NamingConvention}
  */
breeze.MetadataStoreOptions.prototype.namingConvention = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.LocalQueryComparisonOptions}
  */
breeze.MetadataStoreOptions.prototype.localQueryComparisonOptions = null;
/** @interface NamingConventionOptions
  * @namespace breeze
  */
breeze.NamingConventionOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {function}
  */
breeze.NamingConventionOptions.prototype.serverPropertyNameToClient = null;
/** <p>undefined</p>
  * @field 
  * @type {function}
  */
breeze.NamingConventionOptions.prototype.clientPropertyNameToServer = null;
/** @interface NavigationPropertyOptions
  * @namespace breeze
  */
breeze.NavigationPropertyOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.NavigationPropertyOptions.prototype.name = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.NavigationPropertyOptions.prototype.nameOnServer = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.NavigationPropertyOptions.prototype.entityTypeName = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
breeze.NavigationPropertyOptions.prototype.isScalar = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.NavigationPropertyOptions.prototype.associationName = null;
/** <p>undefined</p>
  * @field 
  * @type {string[]}
  */
breeze.NavigationPropertyOptions.prototype.foreignKeyNames = null;
/** <p>undefined</p>
  * @field 
  * @type {string[]}
  */
breeze.NavigationPropertyOptions.prototype.foreignKeyNamesOnServer = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.Validator[]}
  */
breeze.NavigationPropertyOptions.prototype.validators = null;
/** @interface PredicateMethod
  * @namespace breeze
  */
breeze.PredicateMethod = function () {};
/** @interface QueryOptionsConfiguration
  * @namespace breeze
  */
breeze.QueryOptionsConfiguration = function () {};
/** <p>undefined</p>
  * @field 
  * @type {breeze.StrategySymbol}
  */
breeze.QueryOptionsConfiguration.prototype.fetchStrategy = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.StrategySymbol}
  */
breeze.QueryOptionsConfiguration.prototype.mergeStrategy = null;
/** @interface QueryResult
  * @namespace breeze
  */
breeze.QueryResult = function () {};
/** <p>undefined</p>
  * @field 
  * @type {breeze.Entity[]}
  */
breeze.QueryResult.prototype.results = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityQuery}
  */
breeze.QueryResult.prototype.query = null;
/** <p>undefined</p>
  * @field 
  * @type {XMLHttpRequest}
  */
breeze.QueryResult.prototype.XHR = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.EntityManager}
  */
breeze.QueryResult.prototype.entityManager = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
breeze.QueryResult.prototype.inlineCount = null;
/** @interface SaveOptionsConfiguration
  * @namespace breeze
  */
breeze.SaveOptionsConfiguration = function () {};
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
breeze.SaveOptionsConfiguration.prototype.allowConcurrentSaves = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.SaveOptionsConfiguration.prototype.resourceName = null;
/** <p>undefined</p>
  * @field 
  * @type {breeze.DataService}
  */
breeze.SaveOptionsConfiguration.prototype.dataService = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.SaveOptionsConfiguration.prototype.tag = null;
/** @interface SaveResult
  * @namespace breeze
  */
breeze.SaveResult = function () {};
/** <p>undefined</p>
  * @field 
  * @type {breeze.Entity[]}
  */
breeze.SaveResult.prototype.entities = null;
/** <p>undefined</p>
  * @field
  */
breeze.SaveResult.prototype.keyMappings = null;
/** <p>undefined</p>
  * @field 
  * @type {XMLHttpRequest}
  */
breeze.SaveResult.prototype.XHR = null;
/** @interface ValidationOptionsConfiguration
  * @namespace breeze
  */
breeze.ValidationOptionsConfiguration = function () {};
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
breeze.ValidationOptionsConfiguration.prototype.validateOnAttach = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
breeze.ValidationOptionsConfiguration.prototype.validateOnSave = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
breeze.ValidationOptionsConfiguration.prototype.validateOnQuery = null;
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
breeze.ValidationOptionsConfiguration.prototype.validateOnPropertyChange = null;
/** @interface ValidatorFunction
  * @namespace breeze
  */
breeze.ValidatorFunction = function () {};
/** @interface ValidatorFunctionContext
  * @namespace breeze
  */
breeze.ValidatorFunctionContext = function () {};
/** <p>undefined</p>
  * @field
  */
breeze.ValidatorFunctionContext.prototype.value = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.ValidatorFunctionContext.prototype.validatorName = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.ValidatorFunctionContext.prototype.displayName = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.ValidatorFunctionContext.prototype.messageTemplate = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
breeze.ValidatorFunctionContext.prototype.message = null;
typeof breeze.core.IEnum != 'undefined' && $.extend(breeze.DataType.prototype, breeze.core.IEnum.prototype);
typeof breeze.core.IEnum != 'undefined' && $.extend(breeze.EntityAction.prototype, breeze.core.IEnum.prototype);
typeof breeze.core.IEnum != 'undefined' && $.extend(breeze.EntityState.prototype, breeze.core.IEnum.prototype);
typeof breeze.core.IEnum != 'undefined' && $.extend(breeze.FetchStrategy.prototype, breeze.core.IEnum.prototype);
typeof breeze.core.IEnum != 'undefined' && $.extend(breeze.FilterQueryOp.prototype, breeze.core.IEnum.prototype);
typeof breeze.core.IEnum != 'undefined' && $.extend(breeze.MergeStrategy.prototype, breeze.core.IEnum.prototype);
})()
var breeze = breeze || {};

(function () {
breeze.config = breeze.config || {};

(function () {
/** @param {string} interfaceName
  * @param {string} adapterName
  * @returns {Object}
  */
breeze.config.getAdapter = function (interfaceName, adapterName) {};
/** @param {string} interfaceName
  * @param {string} adapterName
  * @returns {Object}
  */
breeze.config.getAdapterInstance = function (interfaceName, adapterName) {};
/** @param {string} interfaceName
  * @param {string} adapterName
  * @param {bool} isDefault
  * @returns {void}
  */
breeze.config.initializeAdapterInstance = function (interfaceName, adapterName, isDefault) {};
/** @param {Object} config
  * @returns {void}
  */
breeze.config.initializeAdapterInstances = function (config) {};
/** @param {string} interfaceName
  * @param {Function} adapterCtor
  * @returns {void}
  */
breeze.config.registerAdapter = function (interfaceName, adapterCtor) {};
/** @param {Function} fn
  * @param {string} fnName
  * @returns {void}
  */
breeze.config.registerFunction = function (fn, fnName) {};
/** @param {Function} ctor
  * @param {string} typeName
  * @returns {void}
  */
breeze.config.registerType = function (ctor, typeName) {};
})()
})()
