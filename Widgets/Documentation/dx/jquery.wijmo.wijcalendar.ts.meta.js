var wijmo = wijmo || {};

(function () {
wijmo.calendar = wijmo.calendar || {};

(function () {
/** @class wijcalendar
  * @widget 
  * @namespace jQuery.wijmo.calendar
  * @extends wijmo.wijmoWidget
  */
wijmo.calendar.wijcalendar = function () {};
wijmo.calendar.wijcalendar.prototype = new wijmo.wijmoWidget();
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.calendar.wijcalendar.prototype.destroy = function () {};
/** Refreshes the calendar. */
wijmo.calendar.wijcalendar.prototype.refresh = function () {};
/** Refereshes a single date on the calendar.
  * @param {Date} date The date to be refreshed.
  */
wijmo.calendar.wijcalendar.prototype.refreshDate = function (date) {};
/** Gets the valid display date. */
wijmo.calendar.wijcalendar.prototype.getDisplayDate = function () {};
/** Gets the currently selected date. */
wijmo.calendar.wijcalendar.prototype.getSelectedDate = function () {};
/** Selects the specified date.
  * @param {Date} date The date to be selected.
  * @returns {bool}
  */
wijmo.calendar.wijcalendar.prototype.selectDate = function (date) {};
/** Clears any selection from the specified date.
  * @param {Date} date The date to be removed from the selectedDates collection.
  * @returns {bool}
  */
wijmo.calendar.wijcalendar.prototype.unSelectDate = function (date) {};
/** Clears any selections from dates on the calendar, removing them from the selectedDates collection. */
wijmo.calendar.wijcalendar.prototype.unSelectAll = function () {};
/** Determines whether the calendar is in the pop-up state. */
wijmo.calendar.wijcalendar.prototype.isPopupShowing = function () {};
/** Pops up the calendar at specifies position.
  * @param {Object} position A jQuery Position plugin that indicates the position in which to pop up the calendar.
  * Please see "http://jqueryui.com/demos/position/" for details of the parameter.
  */
wijmo.calendar.wijcalendar.prototype.popup = function (position) {};
/** Pops up the calendar at the specified X and Y coordinates in the document.
  * @param {number} x X offset.
  * @param {number} y Y offset.
  */
wijmo.calendar.wijcalendar.prototype.popupAt = function (x, y) {};
/** Closes the calendar if it is in the pop-up state. */
wijmo.calendar.wijcalendar.prototype.close = function () {};

/** @class */
var wijcalendar_options = function () {};
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Assigns the string value of the culture ID that appears on the calendar 
  *   for the weekday and title names.</p>
  * @field 
  * @type {string}
  * @option
  */
wijcalendar_options.prototype.culture = "";
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Assigns the string value of the culture calendar that appears on the calendar.
  *   This option must work with culture option.</p>
  * @field 
  * @type {string}
  * @option
  */
wijcalendar_options.prototype.cultureCalendar = "";
/** <p class='defaultValue'>Default value: 'yyyy'</p>
  * <p>Gets or sets the format for the title text in the month view.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * The default value of this option is "yyyy".
  */
wijcalendar_options.prototype.monthViewTitleFormat = 'yyyy';
/** <p class='defaultValue'>Default value: 1</p>
  * <p>Gets or sets the number of calendar months in horizontal direction.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * By setting the monthCols property, calendar months will be added horizontally to the widget.      
  * The default value of this option is "1", which displays one calendar month at a time.
  */
wijcalendar_options.prototype.monthCols = 1;
/** <p class='defaultValue'>Default value: 1</p>
  * <p>Gets or sets the number of calendar months in vertical direction.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * By setting the monthRows property, calendar months will be added vertically to the widget.
  * The default value of this option is "1", which displays one calendar month at a time.
  */
wijcalendar_options.prototype.monthRows = 1;
/** <p class='defaultValue'>Default value: 'MMMM yyyy'</p>
  * <p>Gets or sets the format for the title text.</p>
  * @field 
  * @type {string}
  * @option
  */
wijcalendar_options.prototype.titleFormat = 'MMMM yyyy';
/** <p class='defaultValue'>Default value: true</p>
  * <p>A boolean property that determines whether to display the calendar's title.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijcalendar_options.prototype.showTitle = true;
/** <p>Gets or sets the display date for the first month view. 
  *   You can specify the date via a Date object.</p>
  * @field 
  * @type {Date}
  * @option
  */
wijcalendar_options.prototype.displayDate = null;
/** <p class='defaultValue'>Default value: 6</p>
  * <p>Gets or sets the number of day rows that appear in the calendar. 
  *  This is useful if you want to view more or less calendar days on the calendar month.</p>
  * @field 
  * @type {number}
  * @option
  */
wijcalendar_options.prototype.dayRows = 6;
/** <p class='defaultValue'>Default value: 7</p>
  * <p>Gets or sets the number of day columns that appear in the calendar.
  *  This is useful if you want to view more or less calendar days on the calendar month.</p>
  * @field 
  * @type {number}
  * @option
  */
wijcalendar_options.prototype.dayCols = 7;
/** <p class='defaultValue'>Default value: 'day'</p>
  * <p>An option that determines the initial view type of the calendar.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Possible values are: "day", "month" , "year" or "decade".
  * The default value of this option is "day".
  * The initialView option only takes effect when initializing the wijcalendar.
  */
wijcalendar_options.prototype.initialView = 'day';
/** <p class='defaultValue'>Default value: 'short'</p>
  * <p>Gets or sets the format for the week day.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Possible values are: "short", "full", "firstLetter" or "abbreviated".
  */
wijcalendar_options.prototype.weekDayFormat = 'short';
/** <p class='defaultValue'>Default value: true</p>
  * <p>A boolean property that determines whether to display week days.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijcalendar_options.prototype.showWeekDays = true;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether to display week numbers.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * When enabled, the week numbers appear vertically on the left side of the calendar. 
  * The week numbers represent a week number for each week in the calendar month. 
  * In the calendar year there are a total of 52 weeks so the weeknumbers will range from 1 to 52.
  */
wijcalendar_options.prototype.showWeekNumbers = false;
/** <p class='defaultValue'>Default value: 'firstDay'</p>
  * <p>Defines different rules for determining the first week of the year.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Possible values are: "firstDay", "firstFullWeek" or "firstFourDayWeek"
  * @example
  * $("#calendar1").wijcalendar(
  *            { calendarWeekRule: 'firstFourDayWeek'}
  *   );
  */
wijcalendar_options.prototype.calendarWeekRule = 'firstDay';
/** <p>Determines the minimum date to display. You can specify the minDate via a Date object.</p>
  * @field 
  * @type {Date}
  * @option 
  * @remarks
  * The default value is new Date(1900, 0, 1).
  */
wijcalendar_options.prototype.minDate = null;
/** <p>Determines the maximum date to display. You can specify the maxDate via a Date object.</p>
  * @field 
  * @type {Date}
  * @option 
  * @remarks
  * The default value is new Date(1900, 0, 1).
  */
wijcalendar_options.prototype.maxDate = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether to display the days of the next and/or previous month.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijcalendar_options.prototype.showOtherMonthDays = true;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether to add zeroes to days with only one digit</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * for example, "1" would become "01" if this property were set to "true"
  */
wijcalendar_options.prototype.showDayPadding = false;
/** <p>Gets or sets the date selection mode on the calendar control that
  *  specifies whether the user can select a single day, a week, or an entire month.</p>
  * @field 
  * @option 
  * @remarks
  * Possible fields in hash are: day, days, weekDay, weekNumber, month.
  * @example
  * $("#calendar1").wijcalendar(
  *             { selectionMode: {day:true, weekDay:true}}
  *   );
  */
wijcalendar_options.prototype.selectionMode = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Allows users to preview the next and previous months by 
  *  hovering over the previousPreview and nextPreview buttons.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijcalendar_options.prototype.allowPreview = false;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether users can change the view to month/year/decade
  *  while clicking on the calendar title.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijcalendar_options.prototype.allowQuickPick = true;
/** <p class='defaultValue'>Default value: 'dddd, MMMM dd, yyyy'</p>
  * <p>Gets or sets the format for the ToolTip.</p>
  * @field 
  * @type {string}
  * @option
  */
wijcalendar_options.prototype.toolTipFormat = 'dddd, MMMM dd, yyyy';
/** <p class='defaultValue'>Default value: 'Previous'</p>
  * <p>Gets or sets the text for the 'previous' button's ToolTip.</p>
  * @field 
  * @type {string}
  * @option
  */
wijcalendar_options.prototype.prevTooltip = 'Previous';
/** <p class='defaultValue'>Default value: 'Next'</p>
  * <p>Gets or sets the text for the 'next' button's ToolTip.</p>
  * @field 
  * @type {string}
  * @option
  */
wijcalendar_options.prototype.nextTooltip = 'Next';
/** <p class='defaultValue'>Default value: 'Quick Previous'</p>
  * <p>Gets or sets the "quick previous" button's ToolTip.</p>
  * @field 
  * @type {string}
  * @option
  */
wijcalendar_options.prototype.quickPrevTooltip = 'Quick Previous';
/** <p class='defaultValue'>Default value: 'Quick Next'</p>
  * <p>Gets or sets the "quick next" button's ToolTip.</p>
  * @field 
  * @type {string}
  * @option
  */
wijcalendar_options.prototype.quickNextTooltip = 'Quick Next';
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Gets or sets the "previous preview" button's ToolTip.</p>
  * @field 
  * @type {string}
  * @option
  */
wijcalendar_options.prototype.prevPreviewTooltip = "";
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Gets or sets the "next preview" button's ToolTip.</p>
  * @field 
  * @type {string}
  * @option
  */
wijcalendar_options.prototype.nextPreviewTooltip = "";
/** <p class='defaultValue'>Default value: 'default'</p>
  * <p>Determines the display type of navigation buttons.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Possible values are: "default", "quick" or "none"
  */
wijcalendar_options.prototype.navButtons = 'default';
/** <p class='defaultValue'>Default value: 12</p>
  * <p>Determines the increase/decrease steps when clicking the quick navigation button.</p>
  * @field 
  * @type {number}
  * @option
  */
wijcalendar_options.prototype.quickNavStep = 12;
/** <p class='defaultValue'>Default value: 'horizontal'</p>
  * <p>Determines the month slide direction.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Possible values are: horizontal or vertical
  */
wijcalendar_options.prototype.direction = 'horizontal';
/** <p class='defaultValue'>Default value: 250</p>
  * <p>Gets or sets the animation duration in milliseconds.</p>
  * @field 
  * @type {number}
  * @option
  */
wijcalendar_options.prototype.duration = 250;
/** <p class='defaultValue'>Default value: 'easeInQuad'</p>
  * <p>Sets the type of animation easing effect that users experience 
  *   when they click the previous or next buttons on the wijcalendar.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * For example, if the easing is set to "easeInBounce" the calendar 
  * bounces back and forth several times and then slides to the previous 
  * or next calendar month.
  * You can create custom easing animations using jQuery UI Easings.
  */
wijcalendar_options.prototype.easing = 'easeInQuad';
/** <p class='defaultValue'>Default value: false</p>
  * <p>A boolean property that determines whether the wijcalendar widget 
  *  is a pop-up calendar.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * this is useful, for example, 
  * if you're integrating the calendar with an input control to create a date picker.
  */
wijcalendar_options.prototype.popupMode = false;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A boolean property that determines whether to autohide
  *   the calendar in pop-up mode when clicking outside of the calendar.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijcalendar_options.prototype.autoHide = true;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Function used for customizing the content, style and attributes of a day cell.</p>
  * @field 
  * @type {function}
  * @option 
  * @remarks
  * the function include following parameter:
  * $daycell:jQuery jQuery object that represents table cell of the date to be customized.
  * date: Date Date of the cell. 
  * hover: boolean Whether mouse is over the day cell. 
  * preview: object Whether rendering in preview container.
  * returns true if day cell content has been changed
  * and default cell content will not be applied.
  */
wijcalendar_options.prototype.customizeDate = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A callback function used to customizing the title text on month view.</p>
  * @field 
  * @type {function}
  * @option 
  * @remarks
  * the function include following parameter:
  * date: Date The display date of the month.
  * format: string The title format. Determined by the options.titleFormat.
  */
wijcalendar_options.prototype.title = null;
/** The beforeSlide event handler. 
  * A function called before the calendar view slides to another month. 
  * Cancellable.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijcalendar_options.prototype.beforeSlide = null;
/** The afterSlide event handler. 
  * A function called after the calendar view slided to another month.
  * Cancellable.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijcalendar_options.prototype.afterSlide = null;
/** The beforeSelect event handler. 
  * A function called before user selects a day by mouse. Cancellable.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {ISelectedDate} args The data with this event.
  * @example
  * $("#calendar1").wijcalendar({
  *       beforeSelect: function (e, data) 
  *       {
  *              var selDates = $("#calendar1").wijcalendar('option', 'selectedDates'),
  *              selected = false,
  *              list;
  *              $.each(selDates, function (i, d)
  *                  {
  *                    if (data.date.getFullYear() === d.getFullYear() &amp;&amp;
  *                        data.date.getMonth() === d.getMonth() &amp;&amp;                        data.date.getDate() === d.getDate())
  *                                   {
  *                            selected = true;
  *                            return false;
  *                         }
  *              );
  *  
  *           if (selected) 
  *           {
  *                    $("#calendar1").wijcalendar('unSelectDate', data.date);
  *                       } else
  *                                  {
  *                            $("#calendar1").wijcalendar('selectDate', data.date);
  *                       }
  *            list = $("#msg").empty();
  *                        selDates = $("#calendar1").wijcalendar('option', 'selectedDates');
  *                       $.each(selDates, function (i, d) 
  *                       {
  *                 var li = $("&lt;li/&gt;");
  *                 li.text(d.getFullYear() + "/" + (d.getMonth() + 1) + "/" + d.getDate());
  *                list.append(li);
  *             });
  *                return false;
  *             }
  *                    });
  */
wijcalendar_options.prototype.beforeSelect = null;
/** The afterSelect event handler. 
  * A function called after user selects a day by mouse.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {ISelectedDate} args The data with this event.
  */
wijcalendar_options.prototype.afterSelect = null;
/** The selectedDatesChanged event handler. 
  * A function called after the selectedDates collection changed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.calendar.ISelectedDatesChangedEventArgs} data Information about an event
  * @example
  * $("#calendar").wijcalendar(
  *   {
  *   popupMode: true,
  *   selectedDatesChanged: function () {
  *   var selDate = $(this).wijcalendar("getSelectedDate");
  *   var selectDate = new Date(selDate);
  *   if (!!selDate) $("#popdate").val(selectDate.getMonth() + 1 + "/" + selectDate.getDate() + "/" + selectDate.getFullYear());
  *   }
  */
wijcalendar_options.prototype.selectedDatesChanged = null;
wijmo.calendar.wijcalendar.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijcalendar_options());
$.widget("wijmo.wijcalendar", $.wijmo.widget, wijmo.calendar.wijcalendar.prototype);
/** Provides data for the select event of the wijcalendar.
  * @interface ISelectedDate
  * @namespace wijmo.calendar
  */
wijmo.calendar.ISelectedDate = function () {};
/** <p>The selected date.</p>
  * @field 
  * @type {Date}
  */
wijmo.calendar.ISelectedDate.prototype.date = null;
/** Contains information about wijcalendar.selectedDatesChanged event
  * @interface ISelectedDatesChangedEventArgs
  * @namespace wijmo.calendar
  */
wijmo.calendar.ISelectedDatesChangedEventArgs = function () {};
/** <p>The array with all selected date object.</p>
  * @field 
  * @type {Date}
  */
wijmo.calendar.ISelectedDatesChangedEventArgs.prototype.dates = null;
})()
})()
