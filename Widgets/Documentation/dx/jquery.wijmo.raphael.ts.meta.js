var wijmo = wijmo || {};

(function () {
/** @interface Point
  * @namespace wijmo
  */
wijmo.Point = function () {};
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
wijmo.Point.prototype.x = null;
/** <p>undefined</p>
  * @field 
  * @type {number}
  */
wijmo.Point.prototype.y = null;
})()
