var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
/** @class c1bandfield
  * @namespace wijmo.grid
  * @extends wijmo.grid.c1basefield
  */
wijmo.grid.c1bandfield = function () {};
wijmo.grid.c1bandfield.prototype = new wijmo.grid.c1basefield();
/** @class c1bandfield_options
  * @namespace wijmo.grid
  * @extends wijmo.grid.c1basefield_options
  */
wijmo.grid.c1bandfield_options = function () {};
wijmo.grid.c1bandfield_options.prototype = new wijmo.grid.c1basefield_options();
})()
})()
