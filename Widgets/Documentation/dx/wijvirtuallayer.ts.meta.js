var wijmo = wijmo || {};

(function () {
wijmo.maps = wijmo.maps || {};

(function () {
/** @class wijvirtuallayer
  * @widget 
  * @namespace jQuery.wijmo.maps
  * @extends wijmo.maps.wijlayer
  */
wijmo.maps.wijvirtuallayer = function () {};
wijmo.maps.wijvirtuallayer.prototype = new wijmo.maps.wijlayer();
/** Force to request the data to get the newest one. */
wijmo.maps.wijvirtuallayer.prototype.refresh = function () {};
/** Removes the wijvirtuallayer functionality completely. This will return the element back to its pre-init state. */
wijmo.maps.wijvirtuallayer.prototype.destroy = function () {};

/** @class */
var wijvirtuallayer_options = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IMapSlice.html'>wijmo.maps.IMapSlice[]</a></p>
  * <p class='defaultValue'>Default value: []</p>
  * <p>Specifies how the map is partitioned for virtualization.</p>
  * @field 
  * @type {wijmo.maps.IMapSlice[]}
  * @option 
  * @remarks
  * Each slice defines a set of regions this virtual layer will get items form when needed.
  * The minimum zoom of a slice is the value value of it's &lt;b&gt;zoom&lt;/b&gt; property, 
  * while the maximum zoom is the value from the &lt;b&gt;zoom&lt;/b&gt; property of the next slice.
  * Each slice divides the map in a uniform grid of regions according to the current projection.
  */
wijvirtuallayer_options.prototype.slices = [];
/** The function to be called when request the data.
  * @event 
  * @param {RaphaelPaper} paper The Raphael paper to draw the items.
  * @param {number} minZoom The min zoom suits the current request slice.
  * @param {number} maxZoom The max zoom suits the current request slice.
  * @param {IPoint} lowerLeft The coordinates of the lower left point of the current request slice, in geographic unit.
  * @param {IPoint} upperRight The coordinates of the upper right point of the current request slice, in geographic unit.
  * @remarks
  * &lt;p&gt;The virtual layer request the items to be drawn when the slice need be redrawn.
  * If want to refresh the items, call refresh() method if necessary.&lt;/p&gt;
  * &lt;p&gt;The items will be arranged to the position based on it's location.&lt;/p&gt;
  */
wijvirtuallayer_options.prototype.request = null;
wijmo.maps.wijvirtuallayer.prototype.options = $.extend({}, true, wijmo.maps.wijlayer.prototype.options, new wijvirtuallayer_options());
$.widget("wijmo.wijvirtuallayer", $.wijmo.wijlayer, wijmo.maps.wijvirtuallayer.prototype);
/** Defines the optoins of the wijmaps virtual layer.
  * @interface IVirualLayerOptions
  * @namespace wijmo.maps
  * @extends wijmo.maps.ILayerOptions
  */
wijmo.maps.IVirualLayerOptions = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IMapSlice.html'>wijmo.maps.IMapSlice[]</a></p>
  * <p>Specifies how the map is partitioned for virtualization.</p>
  * @field 
  * @type {wijmo.maps.IMapSlice[]}
  * @remarks
  * Each slice defines a set of regions this virtual layer will get items form when needed.
  * The minimum zoom of a slice is the value value of it's &lt;b&gt;zoom&lt;/b&gt; property, 
  * while the maximum zoom is the value from the &lt;b&gt;zoom&lt;/b&gt; property of the next slice.
  * Each slice divides the map in a uniform grid of regions according to the current projection.
  */
wijmo.maps.IVirualLayerOptions.prototype.slices = null;
/** <p>The function to be called when request the data.</p>
  * @field 
  * @type {function}
  * @remarks
  * &lt;p&gt;The virtual layer request the items to be drawn when the slice need be redrawn.
  * If want to refresh the items, call refresh() method if necessary.&lt;/p&gt;
  * &lt;p&gt;The items will be arranged to the position based on it's location.&lt;/p&gt;
  */
wijmo.maps.IVirualLayerOptions.prototype.request = null;
/** Defines the object of the request result for the virtual layer.
  * @interface IVirtualLayerRequestResult
  * @namespace wijmo.maps
  */
wijmo.maps.IVirtualLayerRequestResult = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IVirtualLayerItem.html'>wijmo.maps.IVirtualLayerItem[]</a></p>
  * <p>The array of individual item to be drawn.</p>
  * @field 
  * @type {wijmo.maps.IVirtualLayerItem[]}
  */
wijmo.maps.IVirtualLayerRequestResult.prototype.items = null;
/** Defines the object of the requested individual item for the virtual layer.
  * @interface IVirtualLayerItem
  * @namespace wijmo.maps
  * @extends wijmo.maps.IItemsLayerItem
  */
wijmo.maps.IVirtualLayerItem = function () {};
/** Represents the options for the wijvirtuallayer.
  * @class wijvirtuallayer_options
  * @namespace wijmo.maps
  * @extends wijmo.maps.wijlayer_options
  */
wijmo.maps.wijvirtuallayer_options = function () {};
wijmo.maps.wijvirtuallayer_options.prototype = new wijmo.maps.wijlayer_options();
typeof wijmo.maps.ILayerOptions != 'undefined' && $.extend(wijmo.maps.IVirualLayerOptions.prototype, wijmo.maps.ILayerOptions.prototype);
typeof wijmo.maps.IItemsLayerItem != 'undefined' && $.extend(wijmo.maps.IVirtualLayerItem.prototype, wijmo.maps.IItemsLayerItem.prototype);
})()
})()
