var wijmo = wijmo || {};

(function () {
wijmo.chart = wijmo.chart || {};

(function () {
/** @class wijbarchart
  * @widget 
  * @namespace jQuery.wijmo.chart
  * @extends wijmo.chart.wijchartcore
  */
wijmo.chart.wijbarchart = function () {};
wijmo.chart.wijbarchart.prototype = new wijmo.chart.wijchartcore();
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.chart.wijbarchart.prototype.destroy = function () {};
/** This method returns the bar, which has a set of Raphaël objects (rects) that represent bars for the
  * series data, from the specified index.
  * @param {number} index The zero-based index of the bar to return.
  * @returns {Raphael element} Bar object.
  */
wijmo.chart.wijbarchart.prototype.getBar = function (index) {};

/** @class */
var wijbarchart_options = function () {};
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value that determines whether the bar chart renders horizontally or vertically.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * If set to false, the numeric Y axis renders to the left, and the X axis labels render below the bars.
  * @example
  * // set the bar as column chart.
  *         $("#wijbarchart").wijbarchart({
  *     horizontal: false,
  *     seriesList: [{
  *         legendEntry: false,
  *         data: {
  *             x: ['Ford', 'GM', 'Chrysler', 'Toyota', 'Nissan', 'Honda'],
  *             y: [.05, .04, .21, .27, .1, .24]
  *         }
  *     }]
  * });
  */
wijbarchart_options.prototype.horizontal = true;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Sets a value that determines whether to stack bars in the chart to show how each value in a series
  * contributes to the total.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * If you want each bar to fill up 100 percet of the chart area, you can also set the is100Percent option to true.
  * See Clustering Data for more information on the concept of using the same X values with multiple Y series.
  * @example
  * // set the chart to stacked bar chart.
  *  $("#wijbarchart").wijbarchart({
  *     stacked: true,
  *     seriesList: [{
  *         label: "US",
  *         data: { x: ['PS3', 'XBOX360', 'Wii'], y: [12.35, 21.50, 30.56] }
  *     }, {
  *         label: "Japan",
  *         data: { x: ['PS3', 'XBOX360', 'Wii'], y: [4.58, 1.23, 9.67] }
  *     }, {
  *         label: "Other",
  *         data: { x: ['PS3', 'XBOX360', 'Wii'], y: [31.59, 37.14, 65.32] }
  *     }]
  * });
  */
wijbarchart_options.prototype.stacked = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Sets a value that determines whether to present stacked bars as a total value of 100 percent, illustrating
  * how each value contributes to the total.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @remarks
  * See Clustering Data for more information on the concept of using the same X values with multiple Y series.
  * Note: This value has no effect if you do not set the stacked option to true.
  * @example
  * // This code results in bars with data for three countries stacked to fill 100% of the chart area, with each
  * // series in a different color representing its percentage of the total.
  * $("#wijbarchart").wijbarchart({
  *     stacked: true,
  *     is100Percent: true,
  *     seriesList: [{
  *         label: "US",
  *         legendEntry: true,
  *         data: { x: ['PS3', 'XBOX360', 'Wii'], y: [12.35, 21.50, 30.56] }
  *     }, {
  *         label: "Japan",
  *         legendEntry: true,
  *         data: { x: ['PS3', 'XBOX360', 'Wii'], y: [4.58, 1.23, 9.67] }
  *     }, {
  *         label: "Other",
  *         legendEntry: true,
  *         data: { x: ['PS3', 'XBOX360', 'Wii'], y: [31.59, 37.14, 65.32] }
  *     }],
  * });
  */
wijbarchart_options.prototype.is100Percent = false;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Sets the amount of each bar to render over the edge of the next bar in the same cluster,as a percentage of
  * the bar width.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Note: A cluster occurs when you have two or more series in your seriesList that have the same x data,
  * but have different y data and different labels
  */
wijbarchart_options.prototype.clusterOverlap = 0;
/** <p class='defaultValue'>Default value: 85</p>
  * <p>Sets the percentage of each cluster's allocated plot area that the bars in each cluster occupy.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * By default, the bars occupy 85% of the cluster's plot area, leaving a small gap between clusters. A setting
  * of 100% removes the gap, or you can make the gap more dramatic with a setting of 50%. 
  * This setting may affect your clusterSpacing option setting.
  */
wijbarchart_options.prototype.clusterWidth = 85;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Sets the number of pixels by which to round the corner-radius for the bars in the chart.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * The amount of rounding this produces depends on the size of the bar.For example, with a clusterRadius
  * of 3, a small bar might look like a ball on the end, while a very large bar might only show a slight
  * rounding of the corners.
  */
wijbarchart_options.prototype.clusterRadius = 0;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Sets the amount of space in pixels between the bars in each cluster.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * This space may also be affected by the clusterOverlap and clusterWidth option settings.
  */
wijbarchart_options.prototype.clusterSpacing = 0;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_animation.html'>wijmo.chart.chart_animation</a></p>
  * <p>The animation option determines whether and how the animation is shown.</p>
  * @field 
  * @type {wijmo.chart.chart_animation}
  * @option 
  * @remarks
  * It defines the animation effect and controls other aspects of the widget's animation, such as
  * duration and easing. Set this option to false in order to disable the animation effect.
  */
wijbarchart_options.prototype.animation = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_animation.html'>wijmo.chart.chart_animation</a></p>
  * <p>Creates the animation object to use when the seriesList data changes.</p>
  * @field 
  * @type {wijmo.chart.chart_animation}
  * @option 
  * @remarks
  * This allows you to visually show changes in data for the same series.
  * Note: This animation does not appear when you first load or reload the page--it only occurs when data changes.
  * @example
  * // This code creates a chart with random data that regenerates when you click the button created in the
  * second code snippet below
  *  $(document).ready(function () {
  * $("#wijbarchart").wijbarchart({
  * seriesList: [createRandomSeriesList("2013")],
  * seriesTransition: {
  *     duration: 800,
  *     easing: "easeOutBounce"
  * }
  * });
  * } );
  * function reload() {
  *     $("#wijbarchart").wijbarchart("option", "seriesList", [createRandomSeriesList("2013")]);
  * }
  * function createRandomSeriesList(label) {
  *     var data = [],
  *         randomDataValuesCount = 12,
  *         labels = ["January", "February", "March", "April", "May", "June",
  *             "July", "August", "September", "October", "November", "December"],
  *         idx;
  *     for (idx = 0; idx &lt; randomDataValuesCount; idx++) {
  *         data.push(Math.round(Math.random() * 100));
  *     }
  *     return {
  *         label: label,
  *         legendEntry: false,
  *         data: { x: labels, y: data }
  *     };
  * }
  */
wijbarchart_options.prototype.seriesTransition = null;
/** This event fires when the user clicks a mouse button.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IBarChartEventArgs} data Information about an event
  */
wijbarchart_options.prototype.mouseDown = null;
/** Fires when the user releases a mouse button while the pointer is over the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IBarChartEventArgs} data Information about an event
  */
wijbarchart_options.prototype.mouseUp = null;
/** Fires when the user first places the pointer over the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IBarChartEventArgs} data Information about an event
  */
wijbarchart_options.prototype.mouseOver = null;
/** Fires when the user moves the pointer off of the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IBarChartEventArgs} data Information about an event
  */
wijbarchart_options.prototype.mouseOut = null;
/** Fires when the user moves the mouse pointer while it is over a chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IBarChartEventArgs} data Information about an event
  */
wijbarchart_options.prototype.mouseMove = null;
/** Fires when the user clicks the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IBarChartEventArgs} data Information about an event
  */
wijbarchart_options.prototype.click = null;
wijmo.chart.wijbarchart.prototype.options = $.extend({}, true, wijmo.chart.wijchartcore.prototype.options, new wijbarchart_options());
$.widget("wijmo.wijbarchart", $.wijmo.wijchartcore, wijmo.chart.wijbarchart.prototype);
/** @class wijbarchart_css
  * @namespace wijmo.chart
  * @extends wijmo.chart.wijchartcore_css
  */
wijmo.chart.wijbarchart_css = function () {};
wijmo.chart.wijbarchart_css.prototype = new wijmo.chart.wijchartcore_css();
/** @interface IBarChartEventArgs
  * @namespace wijmo.chart
  */
wijmo.chart.IBarChartEventArgs = function () {};
/** <p>The Raphael object of the bar.</p>
  * @field 
  * @type {RaphaelElement}
  */
wijmo.chart.IBarChartEventArgs.prototype.bar = null;
/** <p>Data of the series of the bar.</p>
  * @field 
  * @type {Object}
  */
wijmo.chart.IBarChartEventArgs.prototype.data = null;
/** <p>hover style of series of the bar.</p>
  * @field 
  * @type {Object}
  */
wijmo.chart.IBarChartEventArgs.prototype.hoverStyle = null;
/** <p>index of the bar.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.IBarChartEventArgs.prototype.index = null;
/** <p>label of the series of the bar.</p>
  * @field 
  * @type {string}
  */
wijmo.chart.IBarChartEventArgs.prototype.label = null;
/** <p>legend entry of the series of the bar.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.IBarChartEventArgs.prototype.legendEntry = null;
/** <p>style of the series of the bar.</p>
  * @field 
  * @type {Object}
  */
wijmo.chart.IBarChartEventArgs.prototype.style = null;
/** <p>"bar"</p>
  * @field 
  * @type {string}
  */
wijmo.chart.IBarChartEventArgs.prototype.type = null;
/** <p>The x value of the bar.</p>
  * @field 
  * @type {number|string|datetime}
  */
wijmo.chart.IBarChartEventArgs.prototype.x = null;
/** <p>The y valuue of the bar.</p>
  * @field 
  * @type {number|datetime}
  */
wijmo.chart.IBarChartEventArgs.prototype.y = null;
})()
})()
