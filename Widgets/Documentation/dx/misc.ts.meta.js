var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
/** Specifies the type of a row in the grid.
  * @enum rowType
  * @namespace wijmo.grid
  */
wijmo.grid.rowType = {
/** The header row. */
header: 0,
/** Data row. */
data: 1,
/** Alternating data row (used only as modifier of the rowType.data, not as an independent value). */
dataAlt: 2,
/** Filter row. */
filter: 3,
/** Group header row. */
groupHeader: 4,
/** Group footer row. */
groupFooter: 5,
/** Footer row. */
footer: 6,
/**  */
emptyDataRow: 7,
/** Hierarchy header row  (used only as modifier of the rowType.data, not as an independent value). */
dataHeader: 8,
/**  */
dataDetail: 9,
/** Hierararchy detail row. */
detail: 10 
};
/** Determines an object render state. This enumeration can be used with the cellStyleFormatter and rowStyleFormatter options to get a formatted object state.
  * @enum renderState
  * @namespace wijmo.grid
  */
wijmo.grid.renderState = {
/** This is the normal state. The object is rendered and not hovered, selected, or one of the elements determining the current position of the wijgrid. */
none: 0,
/** The object is being rendered. In the cellStyleFormatter, the rendered object is a table cell. In the rowStyleFormatter, the object is a table row. */
rendering: 1,
/** The object is one of the elements determining the current position of the wijgrid. */
current: 2,
/** The object is hovered over. */
hovered: 3,
/** The object is selected. */
selected: 4,
/**  */
editing: 5 
};
/** @param {HTMLTableElement} table
  * @returns {void}
  */
wijmo.grid.emptyTable = function (table) {};
})()
})()
var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
})()
})()
