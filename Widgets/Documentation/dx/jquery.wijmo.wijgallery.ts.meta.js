var wijmo = wijmo || {};

(function () {
wijmo.gallery = wijmo.gallery || {};

(function () {
/** @class wijgallery
  * @widget 
  * @namespace jQuery.wijmo.gallery
  * @extends wijmo.wijmoWidget
  */
wijmo.gallery.wijgallery = function () {};
wijmo.gallery.wijgallery.prototype = new wijmo.wijmoWidget();
/** Returns a count of the number of items in the gallery.
  * @returns {number} The number of items in the gallery
  */
wijmo.gallery.wijgallery.prototype.count = function () {};
/** Removes the wijgallery functionality completely. This returns the element to its pre-init state. */
wijmo.gallery.wijgallery.prototype.destroy = function () {};
/** Shows the picture at the specified index.
  * @param {number} index The zero-based index of the picture to show.
  * @example
  * $("#element").wijgallery("show", 1);
  */
wijmo.gallery.wijgallery.prototype.show = function (index) {};
/** Shows the next picture. */
wijmo.gallery.wijgallery.prototype.next = function () {};
/** Shows the previous picture. */
wijmo.gallery.wijgallery.prototype.previous = function () {};
/** Starts automatically displaying each of the images in order. */
wijmo.gallery.wijgallery.prototype.play = function () {};
/** Stops automatically displaying the images in order. */
wijmo.gallery.wijgallery.prototype.pause = function () {};
/** Adds a custom item with specified index.
  * The first parameter is the new item to add,
  * it should be a jQuery Element or HTML string. 
  * The second parameter is the index of item to add , 
  * If  no index specified the item will be added at the last of item collection.
  * @param {string|jQuery} ui The node content or innerHTML.
  * @param {number} index Specified the postion to insert at.
  * @example
  * $("#element").wijgallery("add", "&lt;li&gt;&lt;img..&gt;&lt;/li&gt;", index);
  */
wijmo.gallery.wijgallery.prototype.add = function (ui, index) {};
/** Removes the item at specified index. 
  * The parameter is the index of item to add , 
  * If no index specified the last item will be removed.
  * @param {number} index Specified which item should be removed.
  * @example
  * $("#element").wijgallery("remove", index);
  */
wijmo.gallery.wijgallery.prototype.remove = function (index) {};

/** @class */
var wijgallery_options = function () {};
/** <p class='defaultValue'>Default value: false</p>
  * <p>Allows pictures to be played automatically.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijgallery( { autoPlay: true } );
  */
wijgallery_options.prototype.autoPlay = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>If set to true, the thumbnails will auto 
  * scrolled after you select the image.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijgallery( { scrollWithSelection: true } );
  */
wijgallery_options.prototype.scrollWithSelection = false;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines if the timer bar should be shown.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijgallery( { showTimer: false } );
  */
wijgallery_options.prototype.showTimer = true;
/** <p class='defaultValue'>Default value: 5000</p>
  * <p>Determines the time span between 2 pictures showing in autoplay mode.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * $("#element").wijgallery( { interval: 3000 } );
  */
wijgallery_options.prototype.interval = 5000;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether the caption of items should be shown.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijgallery( { showCaption: true } );
  */
wijgallery_options.prototype.showCaption = true;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether to show captions for the thumbnails in the gallery.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijgallery( { showThumbnailCaptions: true } );
  */
wijgallery_options.prototype.showThumbnailCaptions = false;
/** <p class='defaultValue'>Default value: []</p>
  * <p>An object collection that contains the data of the gallery.</p>
  * @field 
  * @type {array}
  * @option 
  * @example
  * $("#element").wijgallery( { data: [{
  *     url: "../images/image1.jpg",
  *     thumbUrl: "../thumb/image1.jpg",
  *     caption: "&lt;span&gt;Word Caption 1&lt;/span&gt;"
  * },{
  *     url: "../images/image2.jpg",
  *     thumbUrl: "../thumb/image2.jpg",
  *     caption: "&lt;span&gt;Word Caption 2&lt;/span&gt;"
  * }] } );
  */
wijgallery_options.prototype.data = [];
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether the custom control should be shown.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijgallery( { showControls: true } );
  */
wijgallery_options.prototype.showControls = false;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the innerHTML of the custom control.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#element").wijgallery( { control: "&lt;div&gt;Blah&lt;/div&gt;" } );
  */
wijgallery_options.prototype.control = "";
/** <p>A value that indicates the position settings for the custom control.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#element").wijgallery( { 
  *     controlPosition: {
  *         my: 'left bottom', 
  *         at: 'right top', 
  *         offset: '0 0'} 
  * });
  */
wijgallery_options.prototype.controlPosition = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether the controls should be shown after the dom element is created or hovered on.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijgallery( { 
  *     showCounter: false 
  * });
  */
wijgallery_options.prototype.showCounter = true;
/** <p class='defaultValue'>Default value: '[i] of [n]'</p>
  * <p>Determines the text format of counter.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * '[i]' and '[n]' are built-in parameters represents 
  * the current page index and the number of pages.
  * @example
  * $("#id").wijgallery({
  *     counter: '[i]/[n]'
  * });
  */
wijgallery_options.prototype.counter = '[i] of [n]';
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines if the pager should be shown.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijgallery( { 
  *     showPager: false 
  * } );
  */
wijgallery_options.prototype.showPager = false;
/** <p>Determines the position of the pager.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#element").wijgallery( { 
  *     pagingPosition: { my:{ },at:{ } } ;
  * } );
  */
wijgallery_options.prototype.pagingPosition = null;
/** <p class='defaultValue'>Default value: 'horizontal'</p>
  * <p>Determines the orientation of the thumbnails. Possible values are: "vertical" and "horizontal".</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#element").wijgallery( { 
  *     thumbnailOrientation: "vertical" 
  * } );
  */
wijgallery_options.prototype.thumbnailOrientation = 'horizontal';
/** <p class='defaultValue'>Default value: 'after'</p>
  * <p>Determines the direction of the thumbnails. Possible values are: "before" and "after".</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#element").wijgallery( { 
  *     thumbnailOrientation: "before" 
  * } );
  */
wijgallery_options.prototype.thumbnailDirection = 'after';
/** <p>A value that determines the settings of the animation effect to be used when the wijgallery is scrolling.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#element").wijgallery( { 
  *     transitions: {
  *         animated: "slide",
  *         duration: 1000
  *     }
  * } );
  */
wijgallery_options.prototype.transitions = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether the controls should be shown after the dom element is created or hovered on.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#element").wijgallery( { showControlsOnHover: true } );
  */
wijgallery_options.prototype.showControlsOnHover = true;
/** <p class='defaultValue'>Default value: 5</p>
  * <p>Determines how many thumbnails should be displayed.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * $("#element").wijgallery( { thumbsDisplay: 6 } );
  */
wijgallery_options.prototype.thumbsDisplay = 5;
/** <p class='defaultValue'>Default value: 100</p>
  * <p>Determines the length of the thumbnails.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * $("#element").wijgallery( { thumbsLength: 6 } );
  */
wijgallery_options.prototype.thumbsLength = 100;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether to turn on the movie controls in movie player.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#id").wijgallery({
  *      showMovieControls: false
  *  });
  */
wijgallery_options.prototype.showMovieControls = false;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether to turn on the autoplay option in movie player.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#id").wijgallery({
  *      autoPlayMovies: false
  *  });
  */
wijgallery_options.prototype.autoPlayMovies = true;
/** <p>A hash object that contains parameters for flash object.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#id").wijgallery({
  *      flashParams: { allowfullscreen: false }
  *  });
  */
wijgallery_options.prototype.flashParams = null;
/** <p>A hash object that contains variants for flash object.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#id").wijgallery({  flashVars: { width:300,  height:400 } });
  */
wijgallery_options.prototype.flashVars = null;
/** <p class='defaultValue'>Default value: '9.0.115'</p>
  * <p>Version of flash object.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#id").wijgallery({  flashVersion: "8.0" })
  */
wijgallery_options.prototype.flashVersion = '9.0.115';
/** <p class='defaultValue'>Default value: 'player\\\\player.swf'</p>
  * <p>The relative path and name of the flash vedio player.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#id").wijgallery({  flvPlayer: "player\\player2.swf " });
  */
wijgallery_options.prototype.flvPlayer = 'player\\\\player.swf';
/** <p class='defaultValue'>Default value: 'player\\\\expressInstall.swf'</p>
  * <p>The relative path and name of the flash installation guide.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#id").wijgallery({  flashInstall: " player\expressInstall2.swf " });
  */
wijgallery_options.prototype.flashInstall = 'player\\\\expressInstall.swf';
/** <p class='defaultValue'>Default value: 'img'</p>
  * <p>Determines the display mode of the gallery.
  * Possible values: "img", "iframe", "swf", "flv"</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#id").wijgallery({  mode: "swf" });
  */
wijgallery_options.prototype.mode = 'img';
/** This is the beforeTransition event handler. It is a function called before transitioning to another image.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.gallery.IBeforeTransitionEventArgs} data Information about an event
  */
wijgallery_options.prototype.beforeTransition = null;
/** The afterTransition event handler. A function called after the transition is over.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.gallery.IAfterTransitionEventArgs} data Information about an event
  */
wijgallery_options.prototype.afterTransition = null;
/** The loadCallback event handler. A function called after the dom element is created.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {object} data The node widget that relates to this event.
  */
wijgallery_options.prototype.loadCallback = null;
wijmo.gallery.wijgallery.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijgallery_options());
$.widget("wijmo.wijgallery", $.wijmo.widget, wijmo.gallery.wijgallery.prototype);
/** Contains information about wijgallery.beforeTransition event
  * @interface IBeforeTransitionEventArgs
  * @namespace wijmo.gallery
  */
wijmo.gallery.IBeforeTransitionEventArgs = function () {};
/** <p>The index of the current image.</p>
  * @field 
  * @type {number}
  */
wijmo.gallery.IBeforeTransitionEventArgs.prototype.index = null;
/** <p>The index of the image that will scrolled to.</p>
  * @field 
  * @type {number}
  */
wijmo.gallery.IBeforeTransitionEventArgs.prototype.to = null;
/** Contains information about wijgallery.afterTransition event
  * @interface IAfterTransitionEventArgs
  * @namespace wijmo.gallery
  */
wijmo.gallery.IAfterTransitionEventArgs = function () {};
/** <p>The index of the current image.</p>
  * @field 
  * @type {number}
  */
wijmo.gallery.IAfterTransitionEventArgs.prototype.index = null;
/** <p>The index of the image that will scrolled to.</p>
  * @field 
  * @type {number}
  */
wijmo.gallery.IAfterTransitionEventArgs.prototype.to = null;
})()
})()
