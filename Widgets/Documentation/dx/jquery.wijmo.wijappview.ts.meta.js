var wijmo = wijmo || {};

(function () {
wijmo.appview = wijmo.appview || {};

(function () {
/** @class wijappview
  * @widget 
  * @namespace jQuery.wijmo.appview
  * @extends wijmo.wijmoWidget
  */
wijmo.appview.wijappview = function () {};
wijmo.appview.wijappview.prototype = new wijmo.wijmoWidget();
/**  */
wijmo.appview.wijappview.prototype.destroy = function () {};
/** Load an appview page
  * @param {string} url URL of the page to load
  * @param {wijmo.appview.ILoadSettings} options Load settings to override options.loadSettings
  * @returns {JQueryPromise}
  */
wijmo.appview.wijappview.prototype.loadPage = function (url, options) {};
/** Current active appview page DOM element
  * @returns {JQuery}
  */
wijmo.appview.wijappview.prototype.activePage = function () {};
/** Change current appview page
  * @param toPage A URL or an appview page DOM element to switch to
  * @param {wijmo.appview.ILoadSettings} options Load settings to be used if 'toPage' is a URL
  * @remarks
  * If 'toPage' is a URL, then changePage() loads the page first and calls itself recursively
  */
wijmo.appview.wijappview.prototype.changePage = function (toPage, options) {};

/** @class */
var wijappview_options = function () {};
/** <p class='defaultValue'>Default value: 'appviewpage'</p>
  * <p>Name of a parameter in a URL that specifies current appview page address</p>
  * @field 
  * @type {string}
  * @option
  */
wijappview_options.prototype.urlParamName = 'appviewpage';
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.appview.ILoadSettings.html'>wijmo.appview.ILoadSettings</a></p>
  * <p>Settings used to load appview pages</p>
  * @field 
  * @type {wijmo.appview.ILoadSettings}
  * @option
  */
wijappview_options.prototype.loadSettings = null;
/** <p>undefined</p>
  * @field 
  * @option
  */
wijappview_options.prototype.wijCSS = null;
/** Fires before a page is loaded
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IPageLoadEventArgs} args Information about an event
  */
wijappview_options.prototype.pagebeforeload = null;
/** Fires after a page is loaded
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IPageLoadEventArgs} args Information about an event
  */
wijappview_options.prototype.pageload = null;
/** Fires when a page load is failed
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IPageLoadEventArgs} args Information about an event
  */
wijappview_options.prototype.pageloadfailed = null;
/** Fires before the current page is changed
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IPageChangeEventArgs} args Information about an event
  */
wijappview_options.prototype.pagebeforechange = null;
/** Fires after a page is changed
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IPageChangeEventArgs} args Information about an event
  */
wijappview_options.prototype.pagechange = null;
/** Fires when a page change is failed
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {IPageChangeEventArgs} args Information about an event
  */
wijappview_options.prototype.pagechangefailed = null;
wijmo.appview.wijappview.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijappview_options());
$.widget("wijmo.wijappview", $.wijmo.widget, wijmo.appview.wijappview.prototype);
/** Settings used to load appview pages
  * @interface ILoadSettings
  * @namespace wijmo.appview
  */
wijmo.appview.ILoadSettings = function () {};
/** <p>HTML method (GET, POST, etc) to be used when requesting an appview page</p>
  * @field 
  * @type {string}
  */
wijmo.appview.ILoadSettings.prototype.type = null;
/** <p>A hash of request parameters. Corresponds to the 'data' parameter of jQuery.ajax</p>
  * @field
  */
wijmo.appview.ILoadSettings.prototype.data = null;
/** <p>Specifies whether to show a rotating wheel during loading</p>
  * @field 
  * @type {boolean}
  */
wijmo.appview.ILoadSettings.prototype.showLoadMsg = null;
/** Base class for appview event args
  * @interface IAppViewEventArgs
  * @namespace wijmo.appview
  */
wijmo.appview.IAppViewEventArgs = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.appview.ILoadSettings.html'>wijmo.appview.ILoadSettings</a></p>
  * <p>Settings used for loading pages</p>
  * @field 
  * @type {wijmo.appview.ILoadSettings}
  */
wijmo.appview.IAppViewEventArgs.prototype.options = null;
/** Appview Page load-related event info
  * @interface IPageLoadEventArgs
  * @namespace wijmo.appview
  * @extends wijmo.appview.IAppViewEventArgs
  */
wijmo.appview.IPageLoadEventArgs = function () {};
/** <p>The url of the loading page</p>
  * @field 
  * @type {string}
  */
wijmo.appview.IPageLoadEventArgs.prototype.url = null;
/** <p>The absolute url of the loading page</p>
  * @field 
  * @type {string}
  */
wijmo.appview.IPageLoadEventArgs.prototype.absUrl = null;
/** <p>An XHR object used for loading</p>
  * @field 
  * @type {JQueryXHR}
  */
wijmo.appview.IPageLoadEventArgs.prototype.xhr = null;
/** <p>Text status of the HTTP resonse</p>
  * @field 
  * @type {string}
  */
wijmo.appview.IPageLoadEventArgs.prototype.textStatus = null;
/** <p>The loaded page. If multiple pages are loaded, the main one</p>
  * @field 
  * @type {JQuery}
  */
wijmo.appview.IPageLoadEventArgs.prototype.page = null;
/** <p>All loaded pages</p>
  * @field 
  * @type {JQuery}
  */
wijmo.appview.IPageLoadEventArgs.prototype.allPages = null;
/** <p>An error that occurred during loading</p>
  * @field 
  * @type {string}
  */
wijmo.appview.IPageLoadEventArgs.prototype.errorThrown = null;
/** Appview page change-related event info
  * @interface IPageChangeEventArgs
  * @namespace wijmo.appview
  * @extends wijmo.appview.IAppViewEventArgs
  */
wijmo.appview.IPageChangeEventArgs = function () {};
/** <p>The target page</p>
  * @field 
  * @type {JQuery|string}
  */
wijmo.appview.IPageChangeEventArgs.prototype.toPage = null;
/** @class wijappview_options
  * @namespace wijmo.appview
  */
wijmo.appview.wijappview_options = function () {};
typeof wijmo.appview.IAppViewEventArgs != 'undefined' && $.extend(wijmo.appview.IPageLoadEventArgs.prototype, wijmo.appview.IAppViewEventArgs.prototype);
typeof wijmo.appview.IAppViewEventArgs != 'undefined' && $.extend(wijmo.appview.IPageChangeEventArgs.prototype, wijmo.appview.IAppViewEventArgs.prototype);
})()
})()
