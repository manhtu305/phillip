var wijmo = wijmo || {};

(function () {
wijmo.input = wijmo.input || {};

(function () {
/** @interface IMaskOptions
  * @namespace wijmo.input
  */
wijmo.input.IMaskOptions = function () {};
/** <p>Indicate whether automatically converts to the proper format according to the format setting.</p>
  * @field 
  * @type {boolean}
  */
wijmo.input.IMaskOptions.prototype.autoConvert = null;
/** <p>Indicates the culture that the format library will use.</p>
  * @field 
  * @type {string}
  */
wijmo.input.IMaskOptions.prototype.culture = null;
})()
})()
