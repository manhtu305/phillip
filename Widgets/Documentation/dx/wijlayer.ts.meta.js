var wijmo = wijmo || {};

(function () {
wijmo.maps = wijmo.maps || {};

(function () {
/** @class wijlayer
  * @widget 
  * @namespace jQuery.wijmo.maps
  * @extends wijmo.wijmoWidget
  */
wijmo.maps.wijlayer = function () {};
wijmo.maps.wijlayer.prototype = new wijmo.wijmoWidget();
/** Update the layer, redraw the layer if needed. It's called when the center and zoom changed. */
wijmo.maps.wijlayer.prototype.update = function () {};
/** Called when the wijmaps begins to update the layer status. */
wijmo.maps.wijlayer.prototype.beginUpdate = function () {};
/** Called when the wijmaps ends update the layer status. The layer will be updated after this function called. */
wijmo.maps.wijlayer.prototype.endUpdate = function () {};
/** Refresh the layer. Usually used if want to apply the new data to the layer. */
wijmo.maps.wijlayer.prototype.refresh = function () {};

/** @class */
var wijlayer_options = function () {};
/** <p>Indicates whether the layer is disabled or not.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijlayer_options.prototype.disabled = null;
/** <p>The type of the wijlayer. Wijmaps supports built-in vector, items and virtual layer. 
  * For custom layer, the type is "custom".</p>
  * @field 
  * @type {string}
  * @option
  */
wijlayer_options.prototype.type = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IPoint.html'>wijmo.maps.IPoint</a></p>
  * <p>The current center of the layer, in geographic unit.</p>
  * @field 
  * @type {wijmo.maps.IPoint}
  * @option
  */
wijlayer_options.prototype.center = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IPoint.html'>wijmo.maps.IPoint</a></p>
  * <p>The target center of the layer, in geographic unit.</p>
  * @field 
  * @type {wijmo.maps.IPoint}
  * @option
  */
wijlayer_options.prototype.targetCenter = null;
/** <p>The current zoom of the layer.</p>
  * @field 
  * @type {number}
  * @option
  */
wijlayer_options.prototype.zoom = null;
/** <p>The target zoom of the layer.</p>
  * @field 
  * @type {number}
  * @option
  */
wijlayer_options.prototype.targetZoom = null;
/** <p>Specifies whether the map is on zooming.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijlayer_options.prototype.isZooming = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.ICoordConverter.html'>wijmo.maps.ICoordConverter</a></p>
  * <p>The converter used to convert the coordinates in geograhic unit (longitude and latitude), logic unit(percentage) and screen unit (pixel).</p>
  * @field 
  * @type {wijmo.maps.ICoordConverter}
  * @option
  */
wijlayer_options.prototype.converter = null;
wijmo.maps.wijlayer.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijlayer_options());
$.widget("wijmo.wijlayer", $.wijmo.widget, wijmo.maps.wijlayer.prototype);
/** The layer which can append to the wijmaps.
  * @interface IWijlayer
  * @namespace wijmo.maps
  */
wijmo.maps.IWijlayer = function () {};
/** <p>Called when the wijmaps begins to update the layer status.</p>
  * @field 
  * @type {function}
  */
wijmo.maps.IWijlayer.prototype.beginUpdate = null;
/** <p>Called when the wijmaps ends update the layer status. The layer will be updated after this function called.</p>
  * @field 
  * @type {function}
  */
wijmo.maps.IWijlayer.prototype.endUpdate = null;
/** <p>Update the layer, redraw the layer if needed. It's called when the center and zoom changed.</p>
  * @field 
  * @type {function}
  */
wijmo.maps.IWijlayer.prototype.update = null;
/** <p>Refresh the layer. Usually used if want to apply the new data to the layer.</p>
  * @field 
  * @type {function}
  */
wijmo.maps.IWijlayer.prototype.refresh = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.wijlayer_options.html'>wijmo.maps.wijlayer_options</a></p>
  * <p>The options of the wijmaps layer.</p>
  * @field 
  * @type {wijmo.maps.wijlayer_options}
  */
wijmo.maps.IWijlayer.prototype.options = null;
/** Defines the wijmaps layer.
  * @interface ILayerOptions
  * @namespace wijmo.maps
  */
wijmo.maps.ILayerOptions = function () {};
/** <p>The type of the wijmaps layer. Possible values are:
  * &lt;ul&gt;
  * &lt;li&gt;vector: The vector layer. Used to draw a shape or placemark. &lt;/li&gt;
  * &lt;li&gt;items: The items layer. Draw user defined items in the map. &lt;/li&gt;
  * &lt;li&gt;virtual: The virual layer. Similar to items layer. Can draw different items in diffrent zoom.&lt;/li&gt;
  * &lt;li&gt;custom: The user customized layer. &lt;/li&gt;
  * &lt;/ul&gt;</p>
  * @field 
  * @type {string}
  */
wijmo.maps.ILayerOptions.prototype.type = null;
/** Defines the custom wijmaps layer.
  * @interface ICustomLayerOptions
  * @namespace wijmo.maps
  * @extends wijmo.maps.ILayerOptions
  */
wijmo.maps.ICustomLayerOptions = function () {};
/** <p>The name of the widget which is used to render the custom wijmaps layer.</p>
  * @field 
  * @type {string}
  * @remarks
  * The widget need implements the IWijlayer interface.
  */
wijmo.maps.ICustomLayerOptions.prototype.widgetName = null;
/** The options of the wijlayer.
  * @class wijlayer_options
  * @namespace wijmo.maps
  */
wijmo.maps.wijlayer_options = function () {};
typeof wijmo.maps.ILayerOptions != 'undefined' && $.extend(wijmo.maps.ICustomLayerOptions.prototype, wijmo.maps.ILayerOptions.prototype);
})()
})()
