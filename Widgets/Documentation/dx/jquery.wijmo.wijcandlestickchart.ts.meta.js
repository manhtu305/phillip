var wijmo = wijmo || {};

(function () {
wijmo.chart = wijmo.chart || {};

(function () {
/** @class wijcandlestickchart
  * @widget 
  * @namespace jQuery.wijmo.chart
  * @extends wijmo.chart.wijchartcore
  */
wijmo.chart.wijcandlestickchart = function () {};
wijmo.chart.wijcandlestickchart.prototype = new wijmo.chart.wijchartcore();
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.chart.wijcandlestickchart.prototype.destroy = function () {};
/** This method returns the candlestick elements, which has a set of Raphaël objects  that represent candlestick elements for the
  * series data, from the specified index.
  * @param {number} index The zero-based index of the candlestick to return.
  * @returns {Object} candlestick object which contains Raphael elements.
  */
wijmo.chart.wijcandlestickchart.prototype.getCandlestick = function (index) {};

/** @class */
var wijcandlestickchart_options = function () {};
/** <p>A value that indicates how to draw the candlestick element. Possible value is ohlc, hl, candlestick</p>
  * @field 
  * @option
  */
wijcandlestickchart_options.prototype.type = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_animation.html'>wijmo.chart.chart_animation</a></p>
  * <p>The animation option defines the animation effect and controls other aspects of the widget's animation, 
  * such as duration and easing.</p>
  * @field 
  * @type {wijmo.chart.chart_animation}
  * @option
  */
wijcandlestickchart_options.prototype.animation = null;
/** <p class='defaultValue'>Default value: null</p>
  * <p>A callback to format the candlestick element.</p>
  * @field 
  * @type {Function}
  * @option
  */
wijcandlestickchart_options.prototype.candlestickFormatter = null;
/** <p>Creates an object to use as the tooltip, or hint, when the mouse is over a chart element.</p>
  * @field 
  * @option
  */
wijcandlestickchart_options.prototype.hint = null;
/** This event fires when the user clicks a mouse button.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {ICandlestickChartEventArgs} data Information about an event
  */
wijcandlestickchart_options.prototype.mouseDown = null;
/** Fires when the user releases a mouse button while the pointer is over the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {ICandlestickChartEventArgs} data Information about an event
  */
wijcandlestickchart_options.prototype.mouseUp = null;
/** Fires when the user first places the pointer over the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {ICandlestickChartEventArgs} data Information about an event
  */
wijcandlestickchart_options.prototype.mouseOver = null;
/** Fires when the user moves the pointer off of the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {ICandlestickChartEventArgs} data Information about an event
  */
wijcandlestickchart_options.prototype.mouseOut = null;
/** Fires when the user moves the mouse pointer while it is over a chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {ICandlestickChartEventArgs} data Information about an event
  */
wijcandlestickchart_options.prototype.mouseMove = null;
/** Fires when the user clicks the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {ICandlestickChartEventArgs} data Information about an event
  */
wijcandlestickchart_options.prototype.click = null;
wijmo.chart.wijcandlestickchart.prototype.options = $.extend({}, true, wijmo.chart.wijchartcore.prototype.options, new wijcandlestickchart_options());
$.widget("wijmo.wijcandlestickchart", $.wijmo.wijchartcore, wijmo.chart.wijcandlestickchart.prototype);
/** @class wijcandlestickchart_css
  * @namespace wijmo.chart
  * @extends wijmo.chart.wijchartcore_css
  */
wijmo.chart.wijcandlestickchart_css = function () {};
wijmo.chart.wijcandlestickchart_css.prototype = new wijmo.chart.wijchartcore_css();
/** @interface ICandlestickChartEventArgs
  * @namespace wijmo.chart
  */
wijmo.chart.ICandlestickChartEventArgs = function () {};
/** <p>Type of the target. It's value is "candlestick".</p>
  * @field 
  * @type {string}
  */
wijmo.chart.ICandlestickChartEventArgs.prototype.type = null;
/** <p>Candlestick type of the target,  It's value is "hl", "ohlc", "candlestick".</p>
  * @field 
  * @type {string}
  */
wijmo.chart.ICandlestickChartEventArgs.prototype.candlestickType = null;
/** <p>Label of the candlestick.</p>
  * @field 
  * @type {string}
  */
wijmo.chart.ICandlestickChartEventArgs.prototype.label = null;
/** <p>Data of the series of the candlestick.</p>
  * @field
  */
wijmo.chart.ICandlestickChartEventArgs.prototype.data = null;
/** <p>Legend entry of the candlestick.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.ICandlestickChartEventArgs.prototype.legendEntry = null;
/** <p>The high value of the candlestick data.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.ICandlestickChartEventArgs.prototype.high = null;
/** <p>The low value of the candlestick data.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.ICandlestickChartEventArgs.prototype.low = null;
/** <p>The open value of the candlestick data.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.ICandlestickChartEventArgs.prototype.open = null;
/** <p>The close value of the candlestick data.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.ICandlestickChartEventArgs.prototype.close = null;
/** <p>The time value of the candlestick data.</p>
  * @field 
  * @type {Date}
  */
wijmo.chart.ICandlestickChartEventArgs.prototype.x = null;
/** <p>The data index in the series data.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.ICandlestickChartEventArgs.prototype.index = null;
/** <p>The style of this target candlestick elements.</p>
  * @field 
  * @type {Object}
  */
wijmo.chart.ICandlestickChartEventArgs.prototype.style = null;
/** <p>The hover style of this target candlestick elements.</p>
  * @field 
  * @type {Object}
  */
wijmo.chart.ICandlestickChartEventArgs.prototype.hoverStyle = null;
})()
})()
