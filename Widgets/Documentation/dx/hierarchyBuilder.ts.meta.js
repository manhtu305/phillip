var wijmo = wijmo || {};

(function () {
wijmo.grid = wijmo.grid || {};

(function () {
/** Provides an access to hierarchy nodes in form of tree.
  * @class hierarchyNode
  * @namespace wijmo.grid
  */
wijmo.grid.hierarchyNode = function () {};
/** Returns children nodes.
  * @returns {wijmo.grid.hierarchyNode[]} An array of children nodes if node is expanded, otherwise an empty array.
  */
wijmo.grid.hierarchyNode.prototype.details = function () {};
/** Collapses the node.
  * @returns {void}
  */
wijmo.grid.hierarchyNode.prototype.collapse = function () {};
/** Expands the node.
  * @returns {void}
  */
wijmo.grid.hierarchyNode.prototype.expand = function () {};
/** Determines whether node is expanded or not.
  * @returns {bool} True if node is collapsed, otherwise false.
  */
wijmo.grid.hierarchyNode.prototype.isExpanded = function () {};
/** Returns a wijgrid instance object which represents the detail grid related to node.
  * @returns {wijmo.grid.wijgrid} A wijgrid instance object which represents the detail grid related to node if node is expanded, otherwise null.
  */
wijmo.grid.hierarchyNode.prototype.grid = function () {};
/** Returns master key related to node.
  * @returns {wijmo.grid.IDataKeyArray} Master key related to node.
  */
wijmo.grid.hierarchyNode.prototype.masterKey = function () {};
})()
})()
