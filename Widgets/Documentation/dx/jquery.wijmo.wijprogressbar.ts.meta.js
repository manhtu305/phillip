var wijmo = wijmo || {};

(function () {
wijmo.progressbar = wijmo.progressbar || {};

(function () {
/** @class JQueryUIProgress
  * @widget 
  * @namespace jQuery.wijmo.progressbar
  * @extends wijmo.JQueryUIWidget
  */
wijmo.progressbar.JQueryUIProgress = function () {};
wijmo.progressbar.JQueryUIProgress.prototype = new wijmo.JQueryUIWidget();
/** @param newValue */
wijmo.progressbar.JQueryUIProgress.prototype.value = function (newValue) {};

/** @class */
var JQueryUIProgress_options = function () {};
wijmo.progressbar.JQueryUIProgress.prototype.options = $.extend({}, true, wijmo.JQueryUIWidget.prototype.options, new JQueryUIProgress_options());
$.widget("wijmo.JQueryUIProgress", $.wijmo.widget, wijmo.progressbar.JQueryUIProgress.prototype);
/** @class wijprogressbar
  * @widget 
  * @namespace jQuery.wijmo.progressbar
  * @extends wijmo.progressbar.JQueryUIProgress
  */
wijmo.progressbar.wijprogressbar = function () {};
wijmo.progressbar.wijprogressbar.prototype = new wijmo.progressbar.JQueryUIProgress();
/** Sets the current value of the progressbar.
  * @param {?number} newValue The value to set.
  */
wijmo.progressbar.wijprogressbar.prototype.value = function (newValue) {};
/** Removes the wijprogressbar functionality completely. This returns the element to its pre-init state. */
wijmo.progressbar.wijprogressbar.prototype.destroy = function () {};

/** @class */
var wijprogressbar_options = function () {};
/** <p class='defaultValue'>Default value: 'center'</p>
  * <p>The label's alignment on the progress bar. The value should be "east",
  * "west", "center", "north", "south" or "running".</p>
  * @field 
  * @type {string}
  * @option
  */
wijprogressbar_options.prototype.labelAlign = 'center';
/** <p class='defaultValue'>Default value: 100</p>
  * <p>The value of the progress bar,the type should be numeric.</p>
  * @field 
  * @type {number}
  * @option
  */
wijprogressbar_options.prototype.maxValue = 100;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>The minimum value of the progress bar,the type should be numeric.</p>
  * @field 
  * @type {number}
  * @option
  */
wijprogressbar_options.prototype.minValue = 0;
/** <p class='defaultValue'>Default value: 'east'</p>
  * <p>The fill direction of the progress bar.the value should be "east", 
  * "west", "north" or "south".</p>
  * @field 
  * @type {string}
  * @option
  */
wijprogressbar_options.prototype.fillDirection = 'east';
/** <p class='defaultValue'>Default value: '{1}%'</p>
  * <p>Sets the format of the label text.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * The available formats are as follows:
  * {0} or {ProgressValue} express the current progress Value.
  * {1} or {PercentProgress} express the current percent of the progress bar.
  * {2} or {RemainingProgress} express the remaining progress of the 
  * progress bar.
  * {3} or {PercentageRemaining} express the remaining percent of 
  * the progress bar.
  * {4} or {Min} express the min Vlaue of the progress bar.
  * {5} or {Max} express the max Value of the progress bar.
  */
wijprogressbar_options.prototype.labelFormatString = '{1}%';
/** <p class='defaultValue'>Default value: '{1}%'</p>
  * <p>Set the format of the ToolTip of the progress bar,the expression of the 
  * format like the labelFormatString.
  * ,'{1}%').</p>
  * @field 
  * @type {string}
  * @option
  */
wijprogressbar_options.prototype.toolTipFormatString = '{1}%';
/** <p class='defaultValue'>Default value: 1</p>
  * <p>The increment of the progress bar's indicator.</p>
  * @field 
  * @type {number}
  * @option
  */
wijprogressbar_options.prototype.indicatorIncrement = 1;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>The Url of the image's indicator.</p>
  * @field 
  * @type {string}
  * @option
  */
wijprogressbar_options.prototype.indicatorImage = "";
/** <p class='defaultValue'>Default value: 0</p>
  * <p>Gets or sets the delay time of the progress bar's animation in milliseconds. The default value of this option is "0".</p>
  * @field 
  * @type {number}
  * @option
  */
wijprogressbar_options.prototype.animationDelay = 0;
/** <p>The options parameter of the jQuery's animation.</p>
  * @field 
  * @type {object}
  * @option
  */
wijprogressbar_options.prototype.animationOptions = null;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>The value of the progressbar.</p>
  * @field 
  * @type {number}
  * @option
  */
wijprogressbar_options.prototype.value = 0;
/** Fire upon running the progress. The parameter is an object: {oldValue:xxx,newValue:xxx}. Returning "false" will stop the progress.
  * Return false to cancel the event.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.progressbar.IProgressChangingEventArgs} data Information about an event
  */
wijprogressbar_options.prototype.progressChanging = null;
/** Fires before running the progress. The parameters are an object: {oldValue:xxx,newValue:xxx}. Returning "false" prevents the running progress.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.progressbar.IBeforeProgressChangingEventArgs} data Information about an event
  */
wijprogressbar_options.prototype.beforeProgressChanging = null;
/** Fires when the progress changes. The parameter is an object: {oldValue:xxx,newValue:xxx}.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.progressbar.IProgressChangedEventArgs} data Information about an event
  */
wijprogressbar_options.prototype.progressChanged = null;
wijmo.progressbar.wijprogressbar.prototype.options = $.extend({}, true, wijmo.progressbar.JQueryUIProgress.prototype.options, new wijprogressbar_options());
$.widget("wijmo.wijprogressbar", $.wijmo.JQueryUIProgress, wijmo.progressbar.wijprogressbar.prototype);
/** Contains information about wijprogressbar.progressChanging event
  * @interface IProgressChangingEventArgs
  * @namespace wijmo.progressbar
  */
wijmo.progressbar.IProgressChangingEventArgs = function () {};
/** <p>The old value of the progressbar.</p>
  * @field 
  * @type {number}
  */
wijmo.progressbar.IProgressChangingEventArgs.prototype.oldValue = null;
/** <p>The new value of the progressbar.</p>
  * @field 
  * @type {number}
  */
wijmo.progressbar.IProgressChangingEventArgs.prototype.newValue = null;
/** Contains information about wijprogressbar.beforeProgressChanging event
  * @interface IBeforeProgressChangingEventArgs
  * @namespace wijmo.progressbar
  */
wijmo.progressbar.IBeforeProgressChangingEventArgs = function () {};
/** <p>The old value of the progressbar.</p>
  * @field 
  * @type {number}
  */
wijmo.progressbar.IBeforeProgressChangingEventArgs.prototype.oldValue = null;
/** <p>The new value of the progressbar.</p>
  * @field 
  * @type {number}
  */
wijmo.progressbar.IBeforeProgressChangingEventArgs.prototype.newValue = null;
/** Contains information about wijprogressbar.progressChanged event
  * @interface IProgressChangedEventArgs
  * @namespace wijmo.progressbar
  */
wijmo.progressbar.IProgressChangedEventArgs = function () {};
/** <p>The old value of the progressbar.</p>
  * @field 
  * @type {number}
  */
wijmo.progressbar.IProgressChangedEventArgs.prototype.oldValue = null;
/** <p>The new value of the progressbar.</p>
  * @field 
  * @type {number}
  */
wijmo.progressbar.IProgressChangedEventArgs.prototype.newValue = null;
})()
})()
