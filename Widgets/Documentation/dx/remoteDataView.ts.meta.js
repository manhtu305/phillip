var wijmo = wijmo || {};

(function () {
wijmo.data = wijmo.data || {};

(function () {
/** @interface IRemoteDataViewOptions
  * @namespace wijmo.data
  * @extends wijmo.data.IShape
  */
wijmo.data.IRemoteDataViewOptions = function () {};
/** <p>undefined</p>
  * @field 
  * @type {boolean}
  */
wijmo.data.IRemoteDataViewOptions.prototype.localPaging = null;
/** @class RemoteDataView
  * @namespace wijmo.data
  * @extends wijmo.data.ArrayDataViewBase
  */
wijmo.data.RemoteDataView = function () {};
wijmo.data.RemoteDataView.prototype = new wijmo.data.ArrayDataViewBase();
typeof wijmo.data.IShape != 'undefined' && $.extend(wijmo.data.IRemoteDataViewOptions.prototype, wijmo.data.IShape.prototype);
})()
})()
