var wijmo = wijmo || {};

(function () {
wijmo.listview = wijmo.listview || {};

(function () {
/** wijlistview is inherited from jQuery Mobile listview, so all its options and methods apply:
  * http://api.jquerymobile.com/listview/
  * @class wijlistview
  * @widget 
  * @namespace jQuery.wijmo.listview
  * @extends wijmo.wijmoWidget
  */
wijmo.listview.wijlistview = function () {};
wijmo.listview.wijlistview.prototype = new wijmo.wijmoWidget();

/** @class */
var wijlistview_options = function () {};
wijmo.listview.wijlistview.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijlistview_options());
$.widget("wijmo.wijlistview", $.wijmo.widget, wijmo.listview.wijlistview.prototype);
})()
})()
