var wijmo = wijmo || {};

(function () {
wijmo.list = wijmo.list || {};

(function () {
/** @class wijlist
  * @widget 
  * @namespace jQuery.wijmo.list
  * @extends wijmo.wijmoWidget
  */
wijmo.list.wijlist = function () {};
wijmo.list.wijlist.prototype = new wijmo.wijmoWidget();
/** The removeAll method removes all items from the wijlist. */
wijmo.list.wijlist.prototype.removeAll = function () {};
/** The addItem method adds the specified item to the list by index.
  * @param {object} item Indicates the listItem to add.
  * @param {number} index Index of the added item.
  * @remarks
  * If the index is undefined, then the item becomes the last list item.
  */
wijmo.list.wijlist.prototype.addItem = function (item, index) {};
/** The removeItem method removes the specified item from the wijlist.
  * @param {object} item Indicates the item to be removed.
  */
wijmo.list.wijlist.prototype.removeItem = function (item) {};
/** The indexOf method returns the index of the specified list item.
  * @param {object} item Indicates the specified item.
  * @returns {number} the index of first matched specified item.
  */
wijmo.list.wijlist.prototype.indexOf = function (item) {};
/** Allows the user to find the index of first matched list item by item's label.
  * @param {string} label Indicates the specified item's label that used to search.
  * @returns {number} the index of first matched list item.
  * @remarks
  * If there is no matched list item, it will return -1.
  */
wijmo.list.wijlist.prototype.findIndexByLabel = function (label) {};
/** The removeItemAt method removes the specified list item by index from the wijlist widget.
  * @param {number} index The zero-based index of the list item to remove.
  */
wijmo.list.wijlist.prototype.removeItemAt = function (index) {};
/** The method sets the items for the wijlist to render.
  * @param {array} items Items to be rendered by the wijlist.
  */
wijmo.list.wijlist.prototype.setItems = function (items) {};
/** The popItem method removes the last item from the wijlist widget. */
wijmo.list.wijlist.prototype.popItem = function () {};
/** The method gets the jQuery object reference of the &lt;ul&gt; element of the wijlist widget.
  * @returns {object} the ul JQuery reference.
  */
wijmo.list.wijlist.prototype.getList = function () {};
/** Get the select item(s).
  * @returns {array|Object} items array for multiple selection mode, item object for single selection mode.
  * @remarks
  * It will return item object in single selection mode, and items array in multiple selection mode.
  * If no item is selected, it will return null in single selection mode, and empty array in multiple selection mode.
  */
wijmo.list.wijlist.prototype.getSelectedItems = function () {};
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.list.wijlist.prototype.destroy = function () {};
/** The activate method activates an item in the wijlist and allows the list to scroll to the item.
  * @param {object} event The event object that activates the item.
  * @param {object} item The listItem to activate.
  * @param {bool} scrollTo Indicates whether to scroll the activated item into view.
  */
wijmo.list.wijlist.prototype.activate = function (event, item, scrollTo) {};
/** The deactivate method deactivates the activated item in the wijlist widget. */
wijmo.list.wijlist.prototype.deactivate = function () {};
/** The next method moves focus to the next list item.
  * @param {object} event Event will raise activation.
  */
wijmo.list.wijlist.prototype.next = function (event) {};
/** The nextPage method turns to the next page of the list. */
wijmo.list.wijlist.prototype.nextPage = function () {};
/** The previous method moves focus to the previous list item.
  * @param {object} event Event will raise activation.
  */
wijmo.list.wijlist.prototype.previous = function (event) {};
/** The previous method moves focus to the previous list item. */
wijmo.list.wijlist.prototype.previousPage = function () {};
/** The first method tests whether the focus is at the first list item.
  * @returns {bool}
  */
wijmo.list.wijlist.prototype.first = function () {};
/** The last method tests whether the last list item has focus.
  * @returns {bool}
  */
wijmo.list.wijlist.prototype.last = function () {};
/** The getItems method allows the user to find list items by index or by value.
  * @param {array|number|string} indices the indices of the items.
  * @param {bool} byIndex Indicates the indices parameter is indices or values of items. 
  * If true, it's used as the index/indices of the item(s) to get.
  * If false, it's used as the value/values of the item(s) to get.
  * @returns {object} the item at the specified index or with the specified value.
  */
wijmo.list.wijlist.prototype.getItems = function (indices, byIndex) {};
/** Selects item(s) in the list by item index/indices or value(s).
  * @param {array|number|string} indices the indices of the items.
  * @param {bool} triggerSelected Whether to trigger selected event of list.
  * @param {bool} byIndex Indicates the indices parameter is indices or values of items. 
  * If true, it's used as the index/indices of the item(s) to get.
  * If false, it's used as the value/values of the item(s) to get.
  */
wijmo.list.wijlist.prototype.selectItems = function (indices, triggerSelected, byIndex) {};
/** The unselectItems method clears selections from the indicated list items.
  * @param {array} indices The zero-based index numbers of items to clear.
  */
wijmo.list.wijlist.prototype.unselectItems = function (indices) {};
/** The renderList method renders the wijlist widget on the client browser when list items change. */
wijmo.list.wijlist.prototype.renderList = function () {};
/** The refreshSuperPanel method refreshes the SuperPanel around 
  *  the wijlist to reflect a change in the wijlist content.
  * @returns {bool}
  */
wijmo.list.wijlist.prototype.refreshSuperPanel = function () {};

/** @class */
var wijlist_options = function () {};
/** <p class='defaultValue'>Default value: null</p>
  * <p>This option is the wijdataview to which the wijlist is bound.</p>
  * @field 
  * @type {wijdataview}
  * @option 
  * @remarks
  * This option is used if this wijlist is bound to a wijdataview.
  * In that case, you can also specify a mapping option to 
  * select the properties to bind to,
  * and the listItems option returns an array of objects containing 
  * value and label property values determined by that mapping.
  */
wijlist_options.prototype.dataSource = null;
/** <p class='defaultValue'>Default value: []</p>
  * <p>An array that specifies the listItem collections of wijlist.</p>
  * @field 
  * @type {array}
  * @option
  */
wijlist_options.prototype.listItems = [];
/** <p class='defaultValue'>Default value: 'single'</p>
  * <p>A value indicates the list items can be single-selected or multi-selected</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Options are "single" and "multiple". This option should not be set 
  * again after initialization.
  */
wijlist_options.prototype.selectionMode = 'single';
/** <p class='defaultValue'>Default value: -1</p>
  * <p>A value that specifies the index of the item to select when using single mode.</p>
  * @field 
  * @type {number|array}
  * @option 
  * @remarks
  * If the selectionMode is "multiple", then this option could be set 
  * to an array of Number which contains the indices of the items to select.
  * If no item is selected, it will return -1 or [](in multiple mode).
  */
wijlist_options.prototype.selectedIndex = -1;
/** <p class='defaultValue'>Default value: false</p>
  * <p>The autoSize determines whether or not the wijlist will be automatically sized.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijlist_options.prototype.autoSize = false;
/** <p class='defaultValue'>Default value: 5</p>
  * <p>A value specifies the maximum number of items that will be displayed 
  * if the autoSize option is also set to true.</p>
  * @field 
  * @type {number}
  * @option
  */
wijlist_options.prototype.maxItemsCount = 5;
/** <p class='defaultValue'>Default value: true</p>
  * <p>The addHoverItemClass option determines whether the "ui-state-hover" class 
  *  is applied to a list item on mouse over.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijlist_options.prototype.addHoverItemClass = true;
/** <p class='defaultValue'>Default value: null</p>
  * <p>The superPanelOptions option indicates the customized options of wijsuperpanel 
  *  when the wijsuperpanel is created.</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * superpanel is the list container.For detailed options please refer to the Superpanel widget
  */
wijlist_options.prototype.superPanelOptions = null;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value indicates whether wijlist is disabled.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijlist_options.prototype.disabled = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value determines the highlight state when the mouse leaves an item.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijlist_options.prototype.keepHightlightOnMouseLeave = false;
/** Select event handler of wijlist.
  *  A function will be called when any item in the list is selected.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.list.ISelectedEventArgs} data Information about an event
  */
wijlist_options.prototype.selected = null;
/** The focusing event is fired when the mouse enters the list item and 
  * before the hover event logic is processed.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IItemEventArgs} args The data with this event.
  */
wijlist_options.prototype.focusing = null;
/** The focus event is fired when the mouse enters the list item and 
  * after the hover event logic is processed.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IItemEventArgs} args The data with this event.
  */
wijlist_options.prototype.focus = null;
/** The blur event is fired when the mouse leaves the item.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IItemEventArgs} args The data with this event.
  */
wijlist_options.prototype.blur = null;
/** The itemRendering event is fired before a list item is rendered.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IItemEventArgs} args The data with this event.
  */
wijlist_options.prototype.itemRendering = null;
/** The itemRendered event is fired after a list item is rendered.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IItemEventArgs} args The data with this event.
  */
wijlist_options.prototype.itemRendered = null;
/** The listRendered event is fired after the list is rendered.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.list.IListRenderedEventArgs} data Information about an event
  */
wijlist_options.prototype.listRendered = null;
/** The added event is fired after adding item in addItem method.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {wijmo.list.IAddedEventArgs} data Information about an event
  */
wijlist_options.prototype.added = null;
wijmo.list.wijlist.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijlist_options());
$.widget("wijmo.wijlist", $.wijmo.widget, wijmo.list.wijlist.prototype);
/** Provides data for the item operation event of the wijlist.
  * @interface IItemEventArgs
  * @namespace wijmo.list
  */
wijmo.list.IItemEventArgs = function () {};
/** <p>The &lt; LI &gt; element with this item.</p>
  * @field 
  * @type {Object}
  */
wijmo.list.IItemEventArgs.prototype.element = null;
/** <p>The wijlist instance.</p>
  * @field 
  * @type {Object}
  */
wijmo.list.IItemEventArgs.prototype.list = null;
/** <p>The label of the item.</p>
  * @field 
  * @type {String}
  */
wijmo.list.IItemEventArgs.prototype.label = null;
/** <p>The value of the item.</p>
  * @field 
  * @type {Object}
  */
wijmo.list.IItemEventArgs.prototype.value = null;
/** <p>The text could be set in handler to override rendered label of item.</p>
  * @field 
  * @type {String}
  */
wijmo.list.IItemEventArgs.prototype.text = null;
/** Contains information about wijlist.selected event
  * @interface ISelectedEventArgs
  * @namespace wijmo.list
  */
wijmo.list.ISelectedEventArgs = function () {};
/** <p>By data.item to obtain the selected item.</p>
  * @field 
  * @type {objcect}
  */
wijmo.list.ISelectedEventArgs.prototype.item = null;
/** Contains information about wijlist.listRendered event
  * @interface IListRenderedEventArgs
  * @namespace wijmo.list
  */
wijmo.list.IListRenderedEventArgs = function () {};
/** <p>The list to be rendered.</p>
  * @field 
  * @type {object}
  */
wijmo.list.IListRenderedEventArgs.prototype.list = null;
/** Contains information about wijlist.added event
  * @interface IAddedEventArgs
  * @namespace wijmo.list
  */
wijmo.list.IAddedEventArgs = function () {};
/** <p>By added item and index.</p>
  * @field 
  * @type {object}
  */
wijmo.list.IAddedEventArgs.prototype.item = null;
})()
})()
