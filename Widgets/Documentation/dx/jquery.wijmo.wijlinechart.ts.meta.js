var wijmo = wijmo || {};

(function () {
wijmo.chart = wijmo.chart || {};

(function () {
/** @class wijlinechart
  * @widget 
  * @namespace jQuery.wijmo.chart
  * @extends wijmo.chart.wijchartcore
  */
wijmo.chart.wijlinechart = function () {};
wijmo.chart.wijlinechart.prototype = new wijmo.chart.wijchartcore();
/** Remove the functionality completely. 
  * This will return the element back to its pre-init state.
  */
wijmo.chart.wijlinechart.prototype.destroy = function () {};
/** Returns reference to raphael's path object for the line data with given index.
  * @param {number} lineIndex The index of the series data for which to return lines.
  * @returns {Raphael Element} Reference to raphael element object.
  * @example
  * //Get the first line.
  * $("#linechart").wijlinechart("getLinePath", 0);
  */
wijmo.chart.wijlinechart.prototype.getLinePath = function (lineIndex) {};
/** Returns reference to set of the raphael's objects
  * what represents markers for the line data with given index.
  * @param {number} lineIndex The index of the series data for which to return markers.
  * @returns {Raphael Element} Reference to raphael element object.
  * @example
  * //Get the markers of the first line.
  * $("#linechart").wijlinechart("getLineMarkers", 0);
  */
wijmo.chart.wijlinechart.prototype.getLineMarkers = function (lineIndex) {};

/** @class */
var wijlinechart_options = function () {};
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that determines whether to show a stacked chart.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijlinechart_options.prototype.stacked = false;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Gets or sets the data hole value.</p>
  * @field 
  * @type {number}
  * @option 
  * @remarks
  * Data holes are used as placeholders for data points 
  * that indicate data is normally present but not in this case.
  */
wijlinechart_options.prototype.hole = null;
/** <p class='defaultValue'>Default value: 'line'</p>
  * <p>An option that indicates the type of chart to be displayed.</p>
  * @field 
  * @type {string}
  * @option 
  * @remarks
  * Options are 'line' and 'area'.
  */
wijlinechart_options.prototype.type = 'line';
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.linechart_animation.html'>wijmo.chart.linechart_animation</a></p>
  * <p>The animation option defines the animation effect and controls other aspects of the widget's animation, 
  * such as duration and easing.</p>
  * @field 
  * @type {wijmo.chart.linechart_animation}
  * @option
  */
wijlinechart_options.prototype.animation = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_animation.html'>wijmo.chart.chart_animation</a></p>
  * <p>The seriesTransition option is used to animate series in the chart when just their values change.
  * This is helpful for visually showing changes in data for the same series.</p>
  * @field 
  * @type {wijmo.chart.chart_animation}
  * @option
  */
wijlinechart_options.prototype.seriesTransition = null;
/** This event fires when the user clicks a mouse button.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijLineChartEventArgs} args The data with this event.
  */
wijlinechart_options.prototype.mouseDown = null;
/** This event fires when the user releases a mouse button while the pointer is over the chart element.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijLineChartEventArgs} args The data with this event.
  */
wijlinechart_options.prototype.mouseUp = null;
/** This event fires when the user first places the pointer over the chart element.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijLineChartEventArgs} args The data with this event.
  */
wijlinechart_options.prototype.mouseOver = null;
/** This event fires when the user moves the pointer off of the chart element.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijLineChartEventArgs} args The data with this event.
  */
wijlinechart_options.prototype.mouseOut = null;
/** This event fires when the user moves the mouse pointer while it is over a chart element.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijLineChartEventArgs} args The data with this event.
  */
wijlinechart_options.prototype.mouseMove = null;
/** This event fires when the user clicks the chart element.
  * @event 
  * @param {Object} e The jQuery.Event object.
  * @param {IWijLineChartEventArgs} args The data with this event.
  */
wijlinechart_options.prototype.click = null;
wijmo.chart.wijlinechart.prototype.options = $.extend({}, true, wijmo.chart.wijchartcore.prototype.options, new wijlinechart_options());
$.widget("wijmo.wijlinechart", $.wijmo.wijchartcore, wijmo.chart.wijlinechart.prototype);
/** @class wijlinechart_css
  * @namespace wijmo.chart
  * @extends wijmo.chart.wijchartcore_css
  */
wijmo.chart.wijlinechart_css = function () {};
wijmo.chart.wijlinechart_css.prototype = new wijmo.chart.wijchartcore_css();
/** @interface IWijLineChartEventArgs
  * @namespace wijmo.chart
  */
wijmo.chart.IWijLineChartEventArgs = function () {};
/** <p>Type of the target. Its value is "line" or "marker".</p>
  * @field 
  * @type {string}
  */
wijmo.chart.IWijLineChartEventArgs.prototype.type = null;
/** <p>Index of the marker or line.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.IWijLineChartEventArgs.prototype.index = null;
/** <p>Indicates whether the marker is symbol. Works when type is "marker".</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.IWijLineChartEventArgs.prototype.isSymbol = null;
/** <p>The line infos of the marker. Works when type is "marker".</p>
  * @field
  */
wijmo.chart.IWijLineChartEventArgs.prototype.lineSeries = null;
/** <p>The Raphael object of the marker. Works when type is "marker".</p>
  * @field
  */
wijmo.chart.IWijLineChartEventArgs.prototype.marker = null;
/** <p>Data of the series of the line.</p>
  * @field
  */
wijmo.chart.IWijLineChartEventArgs.prototype.data = null;
/** <p>Fit type of the line.</p>
  * @field 
  * @type {string}
  */
wijmo.chart.IWijLineChartEventArgs.prototype.fitType = null;
/** <p>Label of the line.</p>
  * @field 
  * @type {string}
  */
wijmo.chart.IWijLineChartEventArgs.prototype.label = null;
/** <p>Legend entry of the line.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.IWijLineChartEventArgs.prototype.legendEntry = null;
/** <p>Collection of the markers of the line.</p>
  * @field
  */
wijmo.chart.IWijLineChartEventArgs.prototype.lineMarkers = null;
/** <p>Style of the line.</p>
  * @field
  */
wijmo.chart.IWijLineChartEventArgs.prototype.lineStyle = null;
/** <p>Marker type and visibility of the line.</p>
  * @field
  */
wijmo.chart.IWijLineChartEventArgs.prototype.markers = null;
/** <p>The Raphael object of the line.</p>
  * @field
  */
wijmo.chart.IWijLineChartEventArgs.prototype.path = null;
/** <p>Visibility of the line.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.IWijLineChartEventArgs.prototype.visible = null;
/** @interface linechart_animation
  * @namespace wijmo.chart
  * @extends wijmo.chart.chart_animation
  */
wijmo.chart.linechart_animation = function () {};
/** <p>A value that determines the effect for the animation.</p>
  * @field 
  * @type {string}
  * @remarks
  * Options are 'horizontal' and 'vertical'.
  */
wijmo.chart.linechart_animation.prototype.direction = null;
typeof wijmo.chart.chart_animation != 'undefined' && $.extend(wijmo.chart.linechart_animation.prototype, wijmo.chart.chart_animation.prototype);
})()
})()
