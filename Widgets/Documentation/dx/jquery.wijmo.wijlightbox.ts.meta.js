var wijmo = wijmo || {};

(function () {
wijmo.lightbox = wijmo.lightbox || {};

(function () {
/** @class wijlightbox
  * @widget 
  * @namespace jQuery.wijmo.lightbox
  * @extends wijmo.wijmoWidget
  */
wijmo.lightbox.wijlightbox = function () {};
wijmo.lightbox.wijlightbox.prototype = new wijmo.wijmoWidget();
/** Destroys wijlightbox widget and reset the DOM element. */
wijmo.lightbox.wijlightbox.prototype.destroy = function () {};
/** Shows the content in specified index.
  * @param {number} index The zero-based index of the picture to show.
  * @returns {wijlightbox}
  * @example
  * $("#id").wijlightbox('show');
  */
wijmo.lightbox.wijlightbox.prototype.show = function (index) {};
/** Moves to the next panel.
  * @returns {bool}
  */
wijmo.lightbox.wijlightbox.prototype.next = function () {};
/** Moves to the previous panel.
  * @returns {bool}
  */
wijmo.lightbox.wijlightbox.prototype.back = function () {};
/** Adjust the position of lightbox according to the "position" option set it originally to a new location. .
  * It is usually called after window resized.
  */
wijmo.lightbox.wijlightbox.prototype.adjustPosition = function () {};
/** Determines whether the lightbox is currently displaying images automatically.See the play method for more information.
  * @returns {bool} A value that indicate whether it is playing.
  */
wijmo.lightbox.wijlightbox.prototype.isPlaying = function () {};
/** Starts displaying the images in order automatically.
  * @returns {bool}
  */
wijmo.lightbox.wijlightbox.prototype.play = function () {};
/** Stops the slide playing mode. */
wijmo.lightbox.wijlightbox.prototype.stop = function () {};

/** @class */
var wijlightbox_options = function () {};
/** <p class='defaultValue'>Default value: 'overlay'</p>
  * <p>Determines the position of text description.
  * Possible values are:
  * 'inside', 'outside', 'overlay', 'titleOverlay' and 'none'</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      textPosition: 'titleOverlay'
  *  });
  */
wijlightbox_options.prototype.textPosition = 'overlay';
/** <p class='defaultValue'>Default value: 600</p>
  * <p>Determines the maximum width of the content.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      width: 800
  *  });
  */
wijlightbox_options.prototype.width = 600;
/** <p class='defaultValue'>Default value: 400</p>
  * <p>Determines the maximum height of the content.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      height: 500
  *  });
  */
wijlightbox_options.prototype.height = 400;
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value determines whether to auto-size to 
  * keep the original width/height ratio of content.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      autoSize: false
  *  });
  */
wijlightbox_options.prototype.autoSize = true;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the name of player to host the content.
  * Possible values are: 
  * 'inline', 'iframe', 'img', 'swf', 'flv', 'wmp', 'qt', 'wijvideo'</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      player: 'img'
  *  });
  */
wijlightbox_options.prototype.player = "";
/** <p class='defaultValue'>Default value: null</p>
  * <p>Determines the array of data items.</p>
  * @field 
  * @type {Array}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      groupItems: []
  *  });
  */
wijlightbox_options.prototype.groupItems = null;
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the root url for each item.</p>
  * @field 
  * @type {string}
  * @option
  */
wijlightbox_options.prototype.rootUrl = "";
/** <p class='defaultValue'>Default value: ""</p>
  * <p>Determines the visibility of the control buttons.
  * Possible values are: 'play', 'stop' separated by '|'.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      ctrlButtons: 'play|stop'
  *  });
  */
wijlightbox_options.prototype.ctrlButtons = "";
/** <p class='defaultValue'>Default value: 'close'</p>
  * <p>Determines the visibility of the dialog buttons.
  * Possible values are: 'close', 'fullSize' separated by '|'.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      dialogButtons: 'close|fullSize'
  *  });
  */
wijlightbox_options.prototype.dialogButtons = 'close';
/** <p class='defaultValue'>Default value: 'default'</p>
  * <p>Determines the type counter style.
  * Possible values are: 'default', 'sequence'</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      counterType: 'sequence'
  *  });
  */
wijlightbox_options.prototype.counterType = 'default';
/** <p class='defaultValue'>Default value: '[i] of [n]'</p>
  * <p>Determines the text format of counter.
  * '[i]' and '[n]' are built-in parameters represents 
  * the current page index and the number of pages.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      counterFormat: '[i]/[n]'
  *  });
  */
wijlightbox_options.prototype.counterFormat = '[i] of [n]';
/** <p class='defaultValue'>Default value: 10</p>
  * <p>Determines the maximum number of 
  * digit buttons in sequence counter type.</p>
  * @field 
  * @type {number}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      counterLimit: 5
  *  });
  */
wijlightbox_options.prototype.counterLimit = 10;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether to display the counter.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      showCounter: false
  *  });
  */
wijlightbox_options.prototype.showCounter = true;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether to display the navigation buttons.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      showNavButtons: false
  *  });
  */
wijlightbox_options.prototype.showNavButtons = true;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether to display the time bar.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      showTimer: true
  *  });
  */
wijlightbox_options.prototype.showTimer = false;
/** <p class='defaultValue'>Default value: 'inside'</p>
  * <p>Determines the position of control buttons.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      controlsPosition: 'outside'
  *  });
  */
wijlightbox_options.prototype.controlsPosition = 'inside';
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether to display the control buttons only when hovering the mouse over the content.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      showControlsOnHover: false
  *  });
  */
wijlightbox_options.prototype.showControlsOnHover = true;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether to pause the auto-play when clicking the content.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      clickPause: true
  *  });
  */
wijlightbox_options.prototype.clickPause = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether to allow keyboard navigation.</p>
  * @field 
  * @type {boolean}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      keyNav: true
  *  });
  */
wijlightbox_options.prototype.keyNav = false;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether the window is modal.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijlightbox_options.prototype.modal = false;
/** <p>Determines the pop-up position of content window.
  * Please see jquery.ui.position for possible options.</p>
  * @field 
  * @type {object}
  * @option
  */
wijlightbox_options.prototype.position = null;
/** <p class='defaultValue'>Default value: 1000</p>
  * <p>Determines the z-index of popup container.</p>
  * @field 
  * @type {number}
  * @option
  */
wijlightbox_options.prototype.zIndex = 1000;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether to close the pop-up window 
  * when a user presses the ESC key.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijlightbox_options.prototype.closeOnEscape = true;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether to close the pop-up window 
  * when user clicks on the outside of content.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijlightbox_options.prototype.closeOnOuterClick = true;
/** <p class='defaultValue'>Default value: false</p>
  * <p>Determines whether pages are automatically displayed in order.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijlightbox_options.prototype.autoPlay = false;
/** <p class='defaultValue'>Default value: 2000</p>
  * <p>Determines the time span in milliseconds 
  * between panels in autoplay mode.</p>
  * @field 
  * @type {number}
  * @option
  */
wijlightbox_options.prototype.delay = 2000;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether start from the first page 
  * when reaching the end in autoplay mode.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijlightbox_options.prototype.loop = true;
/** <p class='defaultValue'>Default value: null</p>
  * <p>Store the latest active index in a cookie. 
  * The cookie is then used to determine the initially active index</p>
  * @field 
  * @type {object}
  * @option 
  * @remarks
  * if the activeIndex option is not defined. 
  * This requires cookie plugin. 
  * The object needs to have key/value pairs of 
  * the form the cookie plugin expects as options.
  * e.g. { expires: 7, path: '/', domain: 'jquery.com', secure: true }
  */
wijlightbox_options.prototype.cookie = null;
/** <p>Determines the animation style when switching between pages.
  * Possible values are 'slide', 'fade', 'none'</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      transAnimation: {
  *         animated: slide
  *     }
  *  });
  */
wijlightbox_options.prototype.transAnimation = null;
/** <p class='defaultValue'>Default value: 'horizontal'</p>
  * <p>Determines the slide direction.
  * Possible values are: 'horizontal', 'vertical'</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      slideDirection: 'vertical'
  *  });
  */
wijlightbox_options.prototype.slideDirection = 'horizontal';
/** <p>Determines the animation style when resizing.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      resizeAnimation: { animated: 'wh'}
  *  });
  */
wijlightbox_options.prototype.resizeAnimation = null;
/** <p>Determines the animation style when showing the text.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      textShowOption: { 
  *         duration: 500,
  *         easing: 'linear'
  *     }
  *  });
  */
wijlightbox_options.prototype.textShowOption = null;
/** <p>Determines the animation style when hidding the text.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      textHideOption: { 
  *         duration: 500,
  *         easing: 'linear'
  *     }
  *  });
  */
wijlightbox_options.prototype.textHideOption = null;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether to turn on the movie controls in movie player.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijlightbox_options.prototype.showMovieControls = true;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether to turn on the autoplay option in movie player.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijlightbox_options.prototype.autoPlayMovies = true;
/** <p>Determines a hash object that contains parameters for a flash object.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      flashParams: {
  *          autostart: true,
  *          allowscriptaccess: 'always'
  *      }
  *  });
  */
wijlightbox_options.prototype.flashParams = null;
/** <p>Determines a hash object that contains variants for a flash object.</p>
  * @field 
  * @type {object}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      flashVars: {
  *          backcolor: "0x000000",
  *          frontcolor: "0xCCCCCC"
  *      }
  *  });
  */
wijlightbox_options.prototype.flashVars = null;
/** <p class='defaultValue'>Default value: '9.0.115'</p>
  * <p>Version of Flash object.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      flashVersion: "9.0.115"
  *  });
  */
wijlightbox_options.prototype.flashVersion = '9.0.115';
/** <p class='defaultValue'>Default value: 'player\\\\player.swf'</p>
  * <p>Determines the relative path and name of the flash vedio player.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      flvPlayer: "player\\myplayer.swf"
  *  });
  */
wijlightbox_options.prototype.flvPlayer = 'player\\\\player.swf';
/** <p class='defaultValue'>Default value: 'player\\\\expressInstall.swf'</p>
  * <p>Determines the relative path and name of the flash installation guide.</p>
  * @field 
  * @type {string}
  * @option 
  * @example
  * $("#id").wijlightbox({
  *      flashInstall: "player\\installFlash.swf"
  *  });
  */
wijlightbox_options.prototype.flashInstall = 'player\\\\expressInstall.swf';
/** The beforeShow event handler. 
  * A function called before a page's content is shown.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijlightbox_options.prototype.beforeShow = null;
/** The show event handler. 
  * A function called after a page's content is shown.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijlightbox_options.prototype.show = null;
/** The open event handler. 
  * A function called after the popped up container is opened.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijlightbox_options.prototype.open = null;
/** The showbeforeClose event handler. 
  * A function called before the popped up container is closed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijlightbox_options.prototype.beforeClose = null;
/** The close event handler. 
  * A function called after the popped up container is closed.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  */
wijlightbox_options.prototype.close = null;
wijmo.lightbox.wijlightbox.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijlightbox_options());
$.widget("wijmo.wijlightbox", $.wijmo.widget, wijmo.lightbox.wijlightbox.prototype);
/** @interface flashParams_option
  * @namespace wijmo.lightbox
  */
wijmo.lightbox.flashParams_option = function () {};
/** <p>The backgroud color of the flash content</p>
  * @field 
  * @type {string}
  */
wijmo.lightbox.flashParams_option.prototype.bgcolor = null;
/** <p>Determines if allow full-screen mode.</p>
  * @field 
  * @type {boolean}
  */
wijmo.lightbox.flashParams_option.prototype.allowfullscreen = null;
})()
})()
