var wijmo = wijmo || {};

(function () {
wijmo.chart = wijmo.chart || {};

(function () {
/** @class wijcompositechart
  * @widget 
  * @namespace jQuery.wijmo.chart
  * @extends wijmo.chart.wijchartcore
  */
wijmo.chart.wijcompositechart = function () {};
wijmo.chart.wijcompositechart.prototype = new wijmo.chart.wijchartcore();
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.chart.wijcompositechart.prototype.destroy = function () {};
/** Returns the raphael element with the given type and index.
  * @param {string} type The type of the chart element.
  * @param {number} index The index of the element.
  * @param {number} seriesIndex The index of the series.
  * @returns {Raphael Element} Returns the specified raphael object.
  */
wijmo.chart.wijcompositechart.prototype.getElement = function (type, index, seriesIndex) {};

/** @class */
var wijcompositechart_options = function () {};
/** <p class='defaultValue'>Default value: false</p>
  * <p>A value that determines whether to show a stacked chart.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijcompositechart_options.prototype.stacked = false;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>A value that indicates the percentage of bar elements in the same cluster overlap.</p>
  * @field 
  * @type {number}
  * @option
  */
wijcompositechart_options.prototype.clusterOverlap = 0;
/** <p class='defaultValue'>Default value: 85</p>
  * <p>A value that indicates the percentage of the plot area that each bar cluster occupies.</p>
  * @field 
  * @type {number}
  * @option
  */
wijcompositechart_options.prototype.clusterWidth = 85;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>A value that indicates the corner-radius for the bar.</p>
  * @field 
  * @type {number}
  * @option
  */
wijcompositechart_options.prototype.clusterRadius = 0;
/** <p class='defaultValue'>Default value: 0</p>
  * <p>A value that indicates the spacing between the adjacent bars.</p>
  * @field 
  * @type {number}
  * @option
  */
wijcompositechart_options.prototype.clusterSpacing = 0;
/** <p class='defaultValue'>Default value: []</p>
  * <p>An array collection that contains the data that will be displayed by the chart."</p>
  * @field 
  * @type {array}
  * @option 
  * @example
  * $("#compositechart").wijcompositechart({
  *             seriesList: [{
  *                 type: "bar",
  *                 label: "Q1",
  *                 legendEntry: true,
  *                 data: {
  *                     x: [1, 2, 3, 4, 5],
  *                     y: [12, 21, 9, 29, 30]
  *                 }}, {
  *                 type: "bar",
  *                 label: "Q2",
  *                 legendEntry: true,
  *                 data: {
  *                     xy: [1, 21, 2, 10, 3, 19, 4, 31, 5, 20]
  *                 }}, {
  *                 type: "line",
  *                 label: "Q3",
  *                 legendEntry: true,
  *                 data: {
  *                     x: [1, 2, 3, 4, 5],
  *                     y: [12, 21, 9, 29, 30]
  *                 }}, {
  *                 type: "pie",
  *                 label: "title for pie chart",
  *                 legendEntry: false,
  *                 data: [{
  *                     label: "Q4",
  *                     data: 12,
  *                     offset: 15
  *                 }, {
  *                     label: "Q5",
  *                     data: 21,
  *                     offset: 0
  *                 }, {
  *                     label: "Q5",
  *                     data: 21,
  *                     offset: 0
  *                 }],
  *                 center: {
  *                     x: 150,
  *                     y: 150
  *                 },
  *                 radius: 100
  *                 }
  *             }]
  *             OR
  *             seriesList: [{
  *                 type: "bar"
  *                 label: "Q1",
  *                 legendEntry: true,
  *                 data: {
  *                     x: ["A", "B", "C", "D", "E"],
  *                     y: [12, 21, 9, 29, 30]
  *                 }
  *             }, {
  *                 type: "line"
  *                 label: "Q1",
  *                 legendEntry: true,
  *                 data: {
  *                     x: ["A", "B", "C", "D", "E"],
  *                     y: [12, 21, 9, 29, 30]
  *                 }
  *             }
  *             ]
  *             OR
  *             seriesList: [{
  *                 type: "bar",
  *                 label: "Q1",
  *                 legendEntry: true,
  *                 data: {
  *                     x: [new Date(1978, 0, 1), new Date(1980, 0, 1), 
  *                         new Date(1981, 0, 1), new Date(1982, 0, 1), 
  *                         new Date(1983, 0, 1)],
  *                     y: [12, 21, 9, 29, 30]
  *                 }
  *             }, {
  *                 type: "bar",
  *                 label: "Q2",
  *                 legendEntry: true,
  *                 data: {
  *                     x: [new Date(1978, 0, 1), new Date(1980, 0, 1), 
  *                         new Date(1981, 0, 1), new Date(1982, 0, 1), 
  *                         new Date(1983, 0, 1)],
  *                     y: [10, 25, 5, 25, 35]
  *                 }
  *             }]
  *  });
  */
wijcompositechart_options.prototype.seriesList = [];
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_animation.html'>wijmo.chart.chart_animation</a></p>
  * <p>The animation option defines the animation effect and controls other aspects of the widget's animation, 
  * such as duration and easing.</p>
  * @field 
  * @type {wijmo.chart.chart_animation}
  * @option
  */
wijcompositechart_options.prototype.animation = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.chart.chart_animation.html'>wijmo.chart.chart_animation</a></p>
  * <p>The seriesTransition option is used to animate series in the chart when just their values change.</p>
  * @field 
  * @type {wijmo.chart.chart_animation}
  * @option 
  * @remarks
  * This is helpful for visually showing changes in data for the same series.
  * Note: When programmatically updating the seriesList with a different number of series in the array make sure
  * to disable seriesTransition like the following:
  * seriesTransition: { enabled: false}
  */
wijcompositechart_options.prototype.seriesTransition = null;
/** <p>Gets or sets the hole value.</p>
  * @field 
  * @option
  */
wijcompositechart_options.prototype.hole = null;
/** <p>The maximum bubble size represents the percentage of the diameter (or area) of the plot area.</p>
  * @field 
  * @option
  */
wijcompositechart_options.prototype.maximumSize = null;
/** <p>The minimum bubble size represents the percentage of the diameter (or area) of the plot area.</p>
  * @field 
  * @option
  */
wijcompositechart_options.prototype.minimumSize = null;
/** <p>A value that indicates how to calculate the bubble size.</p>
  * @field 
  * @option 
  * @remarks
  * Valid Values: "area" and "diameter"
  * area: Render the bubble's area based on the y1 value.
  * diameter: Render the bubble's diameter based on the y1 value.
  */
wijcompositechart_options.prototype.sizingMethod = null;
/** <p>A value that indicates whether to zoom in on the marker on hover.</p>
  * @field 
  * @option
  */
wijcompositechart_options.prototype.zoomOnHover = null;
/** Occurs when the user clicks a mouse button.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {ICompositeChartEventArgs} data Information about an event
  */
wijcompositechart_options.prototype.mouseDown = null;
/** Occurs when the user releases a mouse button while the pointer is over the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {ICompositeChartEventArgs} data Information about an event
  */
wijcompositechart_options.prototype.mouseUp = null;
/** Occurs when the user first places the pointer over the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {ICompositeChartEventArgs} data Information about an event
  */
wijcompositechart_options.prototype.mouseOver = null;
/** Occurs when the user moves the pointer off of the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {ICompositeChartEventArgs} data Information about an event
  */
wijcompositechart_options.prototype.mouseOut = null;
/** Occurs when the user moves the mouse pointer while it is over a chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {ICompositeChartEventArgs} data Information about an event
  */
wijcompositechart_options.prototype.mouseMove = null;
/** Occurs when the user clicks the chart element.
  * @event 
  * @param {jQuery.Event} e Standard jQuery event object
  * @param {ICompositeChartEventArgs} data Information about an event
  */
wijcompositechart_options.prototype.click = null;
wijmo.chart.wijcompositechart.prototype.options = $.extend({}, true, wijmo.chart.wijchartcore.prototype.options, new wijcompositechart_options());
$.widget("wijmo.wijcompositechart", $.wijmo.wijchartcore, wijmo.chart.wijcompositechart.prototype);
/** @class wijcompositechart_css
  * @namespace wijmo.chart
  * @extends wijmo.chart.wijchartcore_css
  */
wijmo.chart.wijcompositechart_css = function () {};
wijmo.chart.wijcompositechart_css.prototype = new wijmo.chart.wijchartcore_css();
/** @interface ICompositeChartEventArgs
  * @namespace wijmo.chart
  */
wijmo.chart.ICompositeChartEventArgs = function () {};
/** <p>The chart type of the element.</p>
  * @field 
  * @type {string}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.type = null;
/** <p>The Raphael object of the bar. Only effect when the type is bar.</p>
  * @field 
  * @type {RaphaelElement}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.bar = null;
/** <p>data of the series of the element.</p>
  * @field 
  * @type {Object}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.data = null;
/** <p>hover style of series of the element.</p>
  * @field 
  * @type {Object}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.hoverStyle = null;
/** <p>index of the element.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.index = null;
/** <p>style of the series of the element.</p>
  * @field 
  * @type {Object}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.style = null;
/** <p>label of the series of the element.</p>
  * @field 
  * @type {string}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.label = null;
/** <p>legend entry of the series of the element.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.legendEntry = null;
/** <p>fit type of the line. Only available when the type is line.</p>
  * @field 
  * @type {string}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.fitType = null;
/** <p>collection of the markers of the line. Only available when the type is line.</p>
  * @field 
  * @type {array}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.lineMarkers = null;
/** <p>style of the line. Only available when the type is line.</p>
  * @field 
  * @type {Object}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.lineStyle = null;
/** <p>marker type and visibility of the line. Only available when the type is line.</p>
  * @field 
  * @type {string}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.markers = null;
/** <p>the Raphael object of the line. Only available when the type is line.</p>
  * @field 
  * @type {RaphaelElement}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.path = null;
/** <p>visibility of the element.</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.visible = null;
/** <p>indicates whether the marker is symbol. Only available when the type is marker</p>
  * @field 
  * @type {boolean}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.isSymbol = null;
/** <p>the line infos of the marker. Only available when the type is marker</p>
  * @field 
  * @type {Object}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.lineSeries = null;
/** <p>the Raphael object of the marker. Only available when the type is marker</p>
  * @field 
  * @type {RaphaelElement}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.marker = null;
/** <p>offset of the sector. Only available when the type is pie.</p>
  * @field 
  * @type {number}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.offset = null;
/** <p>value x of the marker. Only available when the type is scatter</p>
  * @field 
  * @type {string|number|Date}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.x = null;
/** <p>value y of the marker. Only available when the type is scatter.</p>
  * @field 
  * @type {number|Date}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.y = null;
/** <p>The high value of the candlestick data. Only available when the type is ohlc/hl/candlestick</p>
  * @field 
  * @type {number}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.high = null;
/** <p>The low value of the candlestick data. Only available when the type is ohlc/hl/candlestick</p>
  * @field 
  * @type {number}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.low = null;
/** <p>The open value of the candlestick data. Only available when the type is ohlc/hl/candlestick</p>
  * @field 
  * @type {number}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.open = null;
/** <p>The close value of the candlestick data. Only available when the type is ohlc/hl/candlestick</p>
  * @field 
  * @type {number}
  */
wijmo.chart.ICompositeChartEventArgs.prototype.close = null;
})()
})()
