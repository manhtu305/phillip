var wijmo = wijmo || {};

(function () {
/** @class JQueryUIWidget
  * @namespace wijmo
  */
wijmo.JQueryUIWidget = function () {};
/** Removes the dialog functionality completely. This will return the element back to its pre-init state. */
wijmo.JQueryUIWidget.prototype.destroy = function () {};
/** Returns a jQuery object containing the original element or other relevant generated element.
  * @returns {JQuery}
  */
wijmo.JQueryUIWidget.prototype.widget = function () {};
/** @class JQueryMobileWidget
  * @namespace wijmo
  * @extends wijmo.JQueryUIWidget
  */
wijmo.JQueryMobileWidget = function () {};
wijmo.JQueryMobileWidget.prototype = new wijmo.JQueryUIWidget();
/** @interface WijmoCSS
  * @namespace wijmo
  */
wijmo.WijmoCSS = function () {};
/** <p>undefined</p>
  * @field 
  * @type {Function}
  */
wijmo.WijmoCSS.prototype.getState = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.widget = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.overlay = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.content = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.header = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.stateDisabled = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.stateFocus = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.stateActive = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.stateDefault = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.stateHighlight = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.stateHover = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.stateChecked = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.stateError = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.icon = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconCheck = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconRadioOn = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconRadioOff = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconClose = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconArrow4Diag = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconNewWin = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconVGripSolid = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconHGripSolid = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconPlay = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconPause = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconStop = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconArrowUp = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconArrowRight = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconArrowDown = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconArrowLeft = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconArrowRightDown = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconArrowThickDown = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconArrowThickUp = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconCaratUp = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconCaratRight = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconCaratDown = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconCaratLeft = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconClock = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconPencil = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconSeekFirst = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconSeekEnd = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconSeekNext = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconSeekPrev = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconPrint = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconDisk = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconSeekStart = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconFullScreen = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconContinousView = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconZoomIn = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconZoomOut = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconBookmark = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconSearch = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconImage = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconArrowReturn = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.iconHome = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.inputSpinnerLeft = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.inputSpinnerRight = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.inputTriggerLeft = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.inputTriggerRight = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.inputSpinnerTriggerLeft = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.inputSpinnerTriggerRight = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.cornerAll = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.cornerLeft = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.cornerRight = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.cornerBottom = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.cornerBL = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.cornerBR = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.cornerTop = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.cornerTL = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.cornerTR = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.helperClearFix = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.helperReset = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.helperHidden = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.priorityPrimary = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.prioritySecondary = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.button = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.buttonText = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.buttonTextOnly = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.tabs = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.tabsTop = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.tabsBottom = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.tabsLeft = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.tabsRight = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.tabsLoading = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.tabsActive = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.tabsPanel = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.tabsNav = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.tabsHide = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.tabsCollapsible = null;
/** <p>undefined</p>
  * @field 
  * @type {string}
  */
wijmo.WijmoCSS.prototype.activeMenuitem = null;
/** The options of base wijmo widget.
  * @interface WidgetOptions
  * @namespace wijmo
  */
wijmo.WidgetOptions = function () {};
/** <p>Determines whether the widget is disabled.</p>
  * @field 
  * @type {boolean}
  */
wijmo.WidgetOptions.prototype.disabled = null;
/** @class wijmoWidget
  * @namespace wijmo
  * @extends wijmo.JQueryMobileWidget
  */
wijmo.wijmoWidget = function () {};
wijmo.wijmoWidget.prototype = new wijmo.JQueryMobileWidget();
/**  */
wijmo.wijmoWidget.prototype.destroy = function () {};
/** @class wijmo_css
  * @namespace wijmo
  */
wijmo.wijmo_css = function () {};
})()
