var wijmo = wijmo || {};

(function () {
wijmo.maps = wijmo.maps || {};

(function () {
/** @class wijmultiscaleimage
  * @widget 
  * @namespace jQuery.wijmo.maps
  * @extends wijmo.wijmoWidget
  */
wijmo.maps.wijmultiscaleimage = function () {};
wijmo.maps.wijmultiscaleimage.prototype = new wijmo.wijmoWidget();
/** Removes the wijmultiscaleimage functionality completely. This will return the element back to its pre-init state. */
wijmo.maps.wijmultiscaleimage.prototype.destroy = function () {};

/** @class */
var wijmultiscaleimage_options = function () {};
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IMultiScaleTileSource.html'>wijmo.maps.IMultiScaleTileSource</a></p>
  * <p>The source of the wijmaps tile layer. Wijmaps provides some built in sources:
  * &lt;ul&gt;
  * &lt;li&gt;wijmo.maps.MapSources.bingMapsRoadSource&lt;/li&gt;
  * &lt;li&gt;wijmo.maps.MapSources.bingMapsAerialSource&lt;/li&gt;
  * &lt;li&gt;wijmo.maps.MapSources.bingMapsHybridSource&lt;/li&gt;
  * &lt;/ul&gt;
  * Set to null if want to hide the tile layer.&lt;br&gt;
  * Set to a new object if want to use another map tile server.</p>
  * @field 
  * @type {wijmo.maps.IMultiScaleTileSource}
  * @option
  */
wijmultiscaleimage_options.prototype.source = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.maps.IPoint.html'>wijmo.maps.IPoint</a></p>
  * <p>The current center of the layer, in geographic unit.</p>
  * @field 
  * @type {wijmo.maps.IPoint}
  * @option
  */
wijmultiscaleimage_options.prototype.center = null;
/** <p class='defaultValue'>Default value: 3</p>
  * <p>The current zoom level of the layer.</p>
  * @field 
  * @type {number}
  * @option
  */
wijmultiscaleimage_options.prototype.zoom = 3;
wijmo.maps.wijmultiscaleimage.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijmultiscaleimage_options());
$.widget("wijmo.wijmultiscaleimage", $.wijmo.widget, wijmo.maps.wijmultiscaleimage.prototype);
/** The options of wijmultiscaleimage.
  * @class wijmultiscaleimage_options
  * @namespace wijmo.maps
  */
wijmo.maps.wijmultiscaleimage_options = function () {};
})()
})()
