var wijmo = wijmo || {};

(function () {
wijmo.chart = wijmo.chart || {};

(function () {
/** @class wijrangebarchart
  * @namespace wijmo.chart
  * @extends wijmo.chart.wijchartcore
  */
wijmo.chart.wijrangebarchart = function () {};
wijmo.chart.wijrangebarchart.prototype = new wijmo.chart.wijchartcore();
/** Remove the functionality completely. This will return the element back to its pre-init state. */
wijmo.chart.wijrangebarchart.prototype.destroy = function () {};
/** @class RangeBarChartRender
  * @namespace wijmo.chart
  */
wijmo.chart.RangeBarChartRender = function () {};
/**  */
wijmo.chart.RangeBarChartRender.prototype.render = function () {};
/**  */
wijmo.chart.RangeBarChartRender.prototype.paintGanttItems = function () {};
/** @param task
  * @param bounds
  */
wijmo.chart.RangeBarChartRender.prototype.paintParentGanttItem = function (task, bounds) {};
/** @param task
  * @param bounds
  */
wijmo.chart.RangeBarChartRender.prototype.paintChildGanttItem = function (task, bounds) {};
/** @param currentTaskBounds
  * @param linkTaskBounds
  */
wijmo.chart.RangeBarChartRender.prototype.paintFinishToStartTasksLinking = function (currentTaskBounds, linkTaskBounds) {};
/** @param currentTaskBounds
  * @param linkTaskBounds
  */
wijmo.chart.RangeBarChartRender.prototype.paintStartToStartTasksLinking = function (currentTaskBounds, linkTaskBounds) {};
/** @param currentTaskBounds
  * @param linkTaskBounds
  */
wijmo.chart.RangeBarChartRender.prototype.paintFinishToFinishTasksLinking = function (currentTaskBounds, linkTaskBounds) {};
/** @param currentTaskBounds
  * @param linkTaskBounds
  */
wijmo.chart.RangeBarChartRender.prototype.paintStartToFinishTasksLinking = function (currentTaskBounds, linkTaskBounds) {};
/** @param startPoint
  * @param endPoint
  * @param pathPoints
  */
wijmo.chart.RangeBarChartRender.prototype.paintTasksLinking = function (startPoint, endPoint, pathPoints) {};
/** @param linkTaskId */
wijmo.chart.RangeBarChartRender.prototype.getLinkTaskBounds = function (linkTaskId) {};
/** @returns {any[]} */
wijmo.chart.RangeBarChartRender.prototype.getBoundsForTasks = function () {};
/** @param startDate
  * @param endDate
  * @returns {number}
  */
wijmo.chart.RangeBarChartRender.prototype.calcDateDiff = function (startDate, endDate) {};
/** @class wijrangebarchart_css
  * @namespace wijmo.chart
  * @extends wijmo.chart.wijchartcore_css
  */
wijmo.chart.wijrangebarchart_css = function () {};
wijmo.chart.wijrangebarchart_css.prototype = new wijmo.chart.wijchartcore_css();
})()
})()
