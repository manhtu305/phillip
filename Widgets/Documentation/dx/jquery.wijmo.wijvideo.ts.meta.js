var wijmo = wijmo || {};

(function () {
wijmo.video = wijmo.video || {};

(function () {
/** @class wijvideo
  * @widget 
  * @namespace jQuery.wijmo.video
  * @extends wijmo.wijmoWidget
  */
wijmo.video.wijvideo = function () {};
wijmo.video.wijvideo.prototype = new wijmo.wijmoWidget();
/** Remove the functionality completely. This will return the element back to its pre-init state.
  * @returns {void}
  */
wijmo.video.wijvideo.prototype.destroy = function () {};
/** Play the video.
  * @returns {void}
  */
wijmo.video.wijvideo.prototype.play = function () {};
/** Pause the video.
  * @returns {void}
  */
wijmo.video.wijvideo.prototype.pause = function () {};
/** Gets the video width in pixel.
  * @returns {number}
  */
wijmo.video.wijvideo.prototype.getWidth = function () {};
/** Sets the video width in pixel.
  * @param {number} width Width value in pixel.
  * @returns {void}
  * @example
  * // Sets the video width to 600 pixel.
  * $("#element").wijvideo("setWidth", 600);
  */
wijmo.video.wijvideo.prototype.setWidth = function (width) {};
/** Gets the video height in pixel.
  * @returns {number}
  */
wijmo.video.wijvideo.prototype.getHeight = function () {};
/** Sets the video height in pixel.
  * @param {number} height Height value in pixel.
  * @returns {void}
  * @example
  * // Sets the video height to 400 pixel.
  * $("#element").wijvideo("setHeight", 400);
  */
wijmo.video.wijvideo.prototype.setHeight = function (height) {};

/** @class */
var wijvideo_options = function () {};
/** <p class='defaultValue'>Default value: true</p>
  * <p>A value that indicates whether to show the full screen button.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijvideo_options.prototype.fullScreenButtonVisible = true;
/** <p class='defaultValue'>Default value: true</p>
  * <p>Determines whether to display the controls only when hovering the mouse to the video.</p>
  * @field 
  * @type {boolean}
  * @option
  */
wijvideo_options.prototype.showControlsOnHover = true;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.video.WijVideoLocalization.html'>wijmo.video.WijVideoLocalization</a></p>
  * <p class='defaultValue'>Default value: null</p>
  * <p>Use the localization option in order to localize text which not depends on culture.</p>
  * @field 
  * @type {wijmo.video.WijVideoLocalization}
  * @option 
  * @remarks
  * The default localization: {
  * volumeToolTip: "Volume",
  * fullScreenToolTip: "Full Screen"
  * }
  * @example
  * $("#video").wijvideo(
  *         { 
  *             localization: {
  *             volumeToolTip: "newVolume",
  *             fullScreenToolTip: "newFullScreen"
  *         }
  *     });
  */
wijvideo_options.prototype.localization = null;
wijmo.video.wijvideo.prototype.options = $.extend({}, true, wijmo.wijmoWidget.prototype.options, new wijvideo_options());
$.widget("wijmo.wijvideo", $.wijmo.widget, wijmo.video.wijvideo.prototype);
/** The options of wijvideo widget.
  * @interface WijVideoOptions
  * @namespace wijmo.video
  * @extends wijmo.WidgetOptions
  */
wijmo.video.WijVideoOptions = function () {};
/** <p>A value that indicates whether to show the full screen button.</p>
  * @field 
  * @type {boolean}
  */
wijmo.video.WijVideoOptions.prototype.fullScreenButtonVisible = null;
/** <p>Determines whether to display the controls only when hovering the mouse to the video.</p>
  * @field 
  * @type {boolean}
  */
wijmo.video.WijVideoOptions.prototype.showControlsOnHover = null;
/** <p class='widgetType'>Type: <a href='Wijmo~wijmo.video.WijVideoLocalization.html'>wijmo.video.WijVideoLocalization</a></p>
  * <p>Use the localization option in order to localize text which not depends on culture.</p>
  * @field 
  * @type {wijmo.video.WijVideoLocalization}
  * @remarks
  * The default localization: {
  * volumeToolTip: "Volume",
  * fullScreenToolTip: "Full Screen"
  * }
  * @example
  * $("#video").wijvideo(
  *         { 
  *             localization: {
  *             volumeToolTip: "newVolume",
  *             fullScreenToolTip: "newFullScreen"
  *         }
  *     });
  */
wijmo.video.WijVideoOptions.prototype.localization = null;
/** Represents the localization settings.
  * @interface WijVideoLocalization
  * @namespace wijmo.video
  */
wijmo.video.WijVideoLocalization = function () {};
/** <p>The tooltip text for the volumn button.</p>
  * @field 
  * @type {string}
  */
wijmo.video.WijVideoLocalization.prototype.volumeToolTip = null;
/** <p>The tooltip text for the fullscreen button.</p>
  * @field 
  * @type {string}
  */
wijmo.video.WijVideoLocalization.prototype.fullScreenToolTip = null;
typeof wijmo.WidgetOptions != 'undefined' && $.extend(wijmo.video.WijVideoOptions.prototype, wijmo.WidgetOptions.prototype);
})()
})()
