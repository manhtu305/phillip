import os
from os import path
import shutil

def copyTree(src, dest):
    if not path.exists(dest):
        os.makedirs(dest)

    for name in os.listdir(src):
        srcPath = path.join(src, name)
        destPath = path.join(dest, name)
        if path.isfile(srcPath):
            shutil.copy(srcPath, destPath)
        else:
            copyTree(srcPath, destPath)

def copySubfolders(src, dest):
    for name in os.listdir(src):
        srcFolder = path.join(src, name)
        if not path.isdir(srcFolder): continue
        targetFolder = path.join(dest, name)
        copyTree(srcFolder, targetFolder)

def zipDir(dir, zip):
    for root, dirs, files in os.walk(dir):
        for file in files:
            fullPath = os.path.join(root, file)
            zip.write(fullPath, path.relpath(fullPath, dir))

class DownloadBuilderError(Exception):
    def __init__(self, message): # real signature unknown
        self.message = message
        pass
