import os
import re
import json
import queue

srcPath = os.path.join(os.path.dirname(__file__), 'wijmo-all')

excludedFiles = ['jquery.wijmo.open.all', 'jquery.wijmo.complete.all']

def getFileKey(name):
    name = name.lower()\
        .replace('-', '.')\
        .replace('.min', '')\
        .replace('.pre', '')\
        .replace('.js', '')

    name = re.sub('.\d[\d\.]+', '', name)

    widgetPrefix = 'jquery.wijmo.'
    pluginPrefix = 'jquery.plugin.'
    pluginPrefixLong = 'jquery.plugin.wij'

    if name.startswith('jquery.ui') or name.startswith('jquery.effects.'):
        return 'jquery.ui'
    elif name == 'knockoutjs':
        return 'knockout'
    elif name.startswith(widgetPrefix):
        return name[len(widgetPrefix):]
    elif name.startswith(pluginPrefixLong):
        return name[len(pluginPrefix):]

    return name

class ScriptFile:
    def __init__(self, fullName):
        self.fullName = fullName
        self.dependencies = []
        self.key = getFileKey(os.path.basename(self.fullName))

    def read(self):
        with open(self.fullName, encoding='utf-8') as file:
            return file.read()

    def dependsOn(self, dep):
        checked = set()
        def dependsOn0(who):
            if who in checked:
                return False
            result = any([d == dep or dependsOn0(d) for d in who.dependencies])
            if not result:
                checked.add(who)
            return result

        return dependsOn0(self)

    def allDependencies(self):
        result = set()
        toCheck = queue.Queue()
        toCheck.add(self)

        while not toCheck.empty():
            s = toCheck.get()
            result.add(s)
            for d in s.dependencies:
                if d not in result:
                    toCheck.put(d)

        result.remove(self)

    def smallestDependencySet(self):
        return [d for d in self.dependencies if not any([d2.dependsOn(d) for d2 in self.dependencies])]

class ScriptSet:
    scripts = {}
    externalBundle = []
    bundles = { 'external': externalBundle }

    def findScripts(self):

        def scanDir(dir):
            for filename in os.listdir(dir):
                if os.path.splitext(filename)[1] == '.js':
                    script = ScriptFile(os.path.join(dir, filename))
                    if script.key in excludedFiles:
                        continue
                    if script.key in self.scripts:
                        print('Duplicate script. Key: %s, name: %s' % (script.key, script.fullName))
                    self.scripts[script.key] = script
                    yield script

        def scanBundle(name, dir):
            dir = os.path.join(srcPath, dir)
            devBundle = os.path.join(dir, 'development-bundle')
            self.bundles[name] = list(scanDir(os.path.join(devBundle, 'wijmo')))
            self.externalBundle.extend(scanDir(os.path.join(devBundle, 'external')))

        scanBundle('open', 'Wijmo-Open')
        scanBundle('complete', 'Wijmo-Complete')

    def detectDependencies(self):
        self.scripts['jquery.ui'].dependencies.append(self.scripts['jquery'])
        jsFileRgx = re.compile('(?<=\s)\w[\w\.\-]*\.js')
        for script in self.scripts.values():
            deps = set()
            for m in set(jsFileRgx.finditer(script.read())):
                key = getFileKey(m.group())
                if key == script.key or key in deps: continue
                deps.add(key)
                dep = self.scripts.get(key)
                if not dep:
                    print('Unresolved dependency: %s (key: %s)' % (m.group(), key))
                    continue

                script.dependencies.append(dep)

    def convertToJson(self):
        result = {}
        for name, scripts in self.bundles.items():
            bundleJson = {}
            result[name] = bundleJson
            for s in scripts:
                bundleJson[s.key] = [d.key for d in s.smallestDependencySet()]
        return result

scripts = ScriptSet()
scripts.findScripts()
scripts.detectDependencies()

with open('widgets.test.json', 'w') as jsonOut:
    json.dump(scripts.convertToJson(), jsonOut, indent=4)