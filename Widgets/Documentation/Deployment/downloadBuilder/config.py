from os import path
import json
import

from bundle import *
from component import *

class Configuration:
    def __init__(self):
        self.bundles = BundleSet()
        self.mainDir = path.normpath(path.dirname(__file__))




    def _loadBundles(self):
        open = WijmoBundle('open', path.join(self.srcPath, 'Wijmo-Open'))
        complete = WijmoBundle('complete', path.join(self.srcPath, 'Wijmo-Complete'))
        self._loadBundle(open)
        self._loadBundle(complete)
        self._loadBundle(ExternalBundle([open, complete]))

    def load(self):
        cfgFile = path.join(self.mainDir, 'wijDownloadBuilder.config')
        self._root = ET.parse(cfgFile).getroot()

        widgetsFilename = path.join(self.mainDir, 'widgets.json')
        with open(widgetsFilename) as widgetsFile:
            self._widgetCfg = json.load(widgetsFile)

        self.dropFilePath = self._root.attrib['dropFile']
        if not path.isabs(self.dropFilePath):
            self.dropFilePath = path.join(self.mainDir, self.dropFilePath)
        self.dropFilePath = path.normpath(self.dropFilePath)

        self._loadBundles()




