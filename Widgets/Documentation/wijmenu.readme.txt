* Title
	wijmenu widget

* Description
	Create muti-level menus with animation effects,images and check box items,interactive item scrolling,and more using the wijmenu widget. You can even create pop-up menu for context help within your application.
    The wijmenu widget is created by the jquery.ui.wijmenu.js library.
* Depends:
    jQuery widget often have dependencies on external files,such as javascript and css files,in order to use the jQuery widget and interactions.The wijmenu widget requires the following dependencies:
	jquery.ui.core.js
	jquery.ui.widget.js
	jquery.wijmo.wijutil.js
	jquery.ui.position.js
    jquery.wijmo.superpanel.js
	jquery.ui.effects.core.js

* HTML Markup
    <ul id=¡±elementid¡±>
		<li><a>menuitem1</a></li>
    </ul>


* Options:
    trigger
    // Specifies the event to show the menu.
    // Type:String.
    // Default: "".
    // Remark:If set to the menu item(the li element) then when it is clicked (if the triggerEvent set to 'click')
    // show submenu.  If set to a element out of the menu, click(if the triggerEvent set to 'click') it, open the menu.
    // Code Example: $(".selector").wijmenu("option", "trigger", "#selector").

    triggerEvent
    // Specifies the event to show the menu.
    // Type: String.
    // Default: "click".
    // Remark: The value can be seted to 'click', 'mouseenter', 'dbclick', 'rtclick'.
    // Code Example: $(".selector").wijmenu("option", "triggerEvent", "click").

    position
    // Location and Orientation of the menu,relative to the button/link userd to open it. Configuration for the
    // Position Utility,Of option excluded(always configured by widget).  Collision also controls collision 
    // detection automatically too.
    // Type: Object.
    // Default: {}.
    // Code Example: $(".selector").wijmenu("option", "position", {my: "top right", at: "buttom left"}).
	
	animated
    // Sets showAnimated and hideAnimated if not specified individually.
    // Type: String.
    // Default: "slide".
    // Remark: User's standard animation setting syntax from other widgets.
    // Code Example: $(".selector").wijmenu("option", "animated", "silde").
	
    showAnimated
    // Determines the animation used during show.
    // Type: String.
    //Default: "slide".
    // Remark: This option uses the standard animation setting syntax from other widgets.
    // Code Example: $(".selector").wijmenu("option", "showAnimated", "silde").

    hideAnimated
    // Determines the animation used during hide.
    // Type: String.
    // Default: "slide".
    // Remark: User's standard animation setting syntax from other widgets.
    // Code Example: $(".selector").wijmenu("option", "hideAnimated", "silde").

    duration
    // Determines the speed to show/hide the menu in milliseconds.  Sets showDuration and hideDuration if they are not specified.
    // Type: Number.
    // Default: 400.
    // Code Example: $(".selector").wijmenu("option", "duration", 400).

    showDuration
    // Determines the speed to show the menu in milliseconds.
    // Type: Number.
    // Default: 400.
    // Code Example: $(".selector").wijmenu("option", "showDuration", 400).

    hideDuration
    // Determines the speed to hide the menu in milliseconds.
    // Type: Number.
    // Default: 400.
    // Code Example: $(".selector").wijmenu("option", "hideDuration", 400).

    mode
    // Defines the behavior of the submenu whether it is a popup menu or an iPod-style navigation list.
	// Type: String.
    // Default: "flyout".
    // Remark: The value should be "flyout" or "sliding".
    // Code Example: $(".selector").wijmenu("option", "mode", "flyout").

    superPanelOptions
    // This option specifies a hash value that sets to superpanel options when a superpanel is created.
    // Type: Object.
    // Default: null.
    // Code Example: $(".selector").wijmenu("option", "superPanelOptions", null).

    checkable
    // Defines whether the item can be checked.
    // Type: Boolean.
    // Default: false.
    // Code Example: $(".selector").wijmenu("option", "checkable", true).

    orientation
    // Controls the root menus orientation. All submenus are vertical regardless of the orientation of the root menu.
    // Type: String.
    // Default: "horizontal".
    // Remark: The value should be "horizontal" or "vertical".
    // Code Example: $(".selector").wijmenu("option", "orientation", "horizontal").
	
    maxHeight
    // Determines the i-Pod-style menu's maximum height.
    // Type: Number.
    // Default: 200.
    // Remark: This option only used in i-pod style menu.  When the menu's heiget largger than this value, menu show scroll bar.
    // Code Example: $(".selector").wijmenu("option", "maxHeight", 200).

    backLink
    // Determines whether the i-Pod menu shows a back link or a breadcrumb header in the menu.
    // Type: Boolean.
    // Default: true.
    // Code Example: $(".selector").wijmenu("option", "backLink", true).

    backLinkText
    // Sets the text of the back link.
    // Type: String.
    // Default: "Back".
    // Code Example: $(".selector").wijmenu("option", "backLink", "Back").

    topLinkText
    // Sets the text of the top link.
    // Type: String.
    // Default: "All".
    // Code Example: $(".selector").wijmenu("option", "topLinkText", "All").
	
    crumbDefaultText
    // Sets the top breadcrumb's default Text.
    // Type: String.
    // Default: "Choose an option".
    // Code Example: $(".selector").wijmenu("option", "crumbDefaultText", "Choose an option").

*methods
	destroy()
	// Removes the wijmenu functionality completely. This returns the element back to its pre-init state.
	
    activate
    // Actives an menu item by deactivating the current item, scrolling the new one into view, if necessary,
    // making it the active item, and triggering a focus event.

    deactivate
    // Clears the current selection.This method is useful when reopening a menu which previously had an item selected.

    next
    // Selects the next item based on the active one. Selects the first item if none is active or if the last one is active.

    previous
    // Selects the previous item based on the active one. Selects the last item if none is active or if the first one is active.

    first
    // Determines whether the active item is the first menu item.

    last
    // Determines whether the active item is the last menu item.

    nextPage
    // This event is similar to the next event, but it jumps a whole page.

    previousPage
    // This event is silimlar to the previous event, but it jumps a whole page.

    select
    // Selects the active item,triggering the select event for that item. This event is useful for custom keyboard handling.

    refresh
    // Renders all non-menu-items as menuitems,called once by _create.  Call this method whenever adding or replaceing 
    // items in the menu via DOM operations,for example,via menu.append
	
    hideAllMenus
    // hide all the visible submenus. 

	
*Events
	focus.wijmenu
    // Triggered when a menu item gets the focus, either when the mouse is used to hover over it(on hover) or 
    // when the cursor keys are used on the keyboard(navigation with cursor key) focus.
    // Remarks: The ui.item refers to a jQuery object containing the focused menu item (a li element).

    blur.wijmenu
    // Triggered a menu item looses focus.

    select.wijmenu
    // Triggered when a menu item was is selected
    // Remarks: The ui.item refers to a jQuery object containing the selected menu item (a li element).

	

