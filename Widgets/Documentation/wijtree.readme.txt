* Title
	wijtree widget

* Description
	wijtree is a jQuery widget to build a tree base on user's options.

* Depends:
	jquery-1.4.2.js
	jquery.ui.core.js
	jquery.ui.widget.js
	jquery.effects.core.js
	jquery.ui.draggable.js
	jquery.ui.droppable.js

* HTML Markup
	<ul id="id">
		<li><a>tree Node 1</a></li>
	</ul>

* Options:

	allowDrag
	// Allow tree nodes to be dragged
	// Type: Boolean
	// Default:false
	
	allowDrop
	// Allow tree to be dropped with tree nodes
	// Type: Boolean
	// Default:false
	
	allowEdit
	// Allow tree nodes to be edited at run time.
	// Type: Boolean
	// Default:false
	
	allowSorting
	// Allow tree nodes to be sorted at run time.
	// Type: Boolean  
	// Default:true
	
	allowTriState
	// Allow triState of checkBox
	// Type: Boolean
	// Default:true
	
	autoCheckNodes
	// Allow sub nodes to be checked on parent node check.
	// Type: Boolean
	// Default true
	
	autoCollapse
	// If this option is set to true, expanded node will be collapsed if other node is expanded.
	// Type: Boolean
	// Default:false	
	
	disabled
	// If it is set to true,the select ,click ,check operations are disabled too.
	// Type: Boolean
	// Default:false
		
	displayVisible
	// Set the visibility of tree widget
	// Type: Boolean
	// Default:true
	
	expandCollapseHoverUsed
	// if this option is set to true, the tree will be expand/Collapse when the mouse hover on the expand/Collapse button
	// Type: Boolean
	// Default:false

	showCheckBoxes
	// Allow the CheckBox to be shown on tree nodes
	// Type: Boolean
	// Default:false
	
	showExpandCollapse
	// Allow tree nodes to be expanded or collapsed
	// Type: Boolean
	// Default:true
	
	expandAnimation
	//  Animation options for showing the child nodes when the parent node expanded.
	//  Type: Object
	//  Default: null
	
	expandDelay
	// The duration of the time to delay before node expanded
	// Type: Number
	// Default:0
	
	collapseAnimation
	// Animation options for hiding the child nodes when the parent node collapsed.
	// Type: Object
	// Default: null
	
	collapseDelay
	// The duration of the time to delay before node collapsed
	// Type: Number
	// Default:0

	
	
*methods
	destroy()
	// Destroy the widget
	
	getSelectedNode()
	// Get the selected nodes
	
	add(node,index)
	// Add a node to this element,the first params can be as follows:
	// 1.markup html.such as '<li><a>node</a></li>' as a node.
	// 2.wijtreenode widget.
	// 3.object options according to the options of wijtreenode.
	// the second param is a optional param.which is add to a position of the tree 
	
	remove(node)
	// Remove a node to this element,the first params can be as follows:
	// 1.wijtreenode widget.
	// 2.the index of which node you determined to remove.

	findNodeByText(text)
	//Find node by the node text
	
*Events

	nodeBlur.wijtree
	// When node is blur ,trigger the event
	
	nodeClick.wijtree
	// When node is clicked,trigger the event
	
	nodeCheckChanged.wijtree
	// When node is checked ,trigger the event
	
	nodeCollapsed.wijtree
	// When node is collapsed,trigger the event
	
	nodeExpanded.wijtree
	// When node is expanded,trigger the event
	
	nodeDragging.wijtree
	// When the mouse is moved during the dragging,trigger the event
	
	nodeDragStarted.wijtree
	// When dragging starts,trigger the event
	
	nodeDroped.wijtree
	// When an accepted draggable node is dropped 'over' ,trigger the event
	
	nodeMouseOver.wijtree
	// When mouse over the node,trigger the event
	
	nodeMouseOut.wijtree
	// When mouse out the node,trigger the event
	
	nodeTextChanged.wijtree
	// When the text of node changed,trigger the event
	
	selectedNodeChanged.wijtree
	// When the seleced node change,trigger the event
	

*Title
	wijtreenode widget
	
*Description
	this is a internal widget.children of wijtree

*HTML markup
	<li>
		<a>tree Node 1</a>
		<ul>..</ul>
	</li>
	
*Options
	checked
	// Check this node when it set to true,else uncheck the node
	// Type: Boolean
	// Default:false
	
	displayVisible
	// Set the visibility of the node
	// Type: Boolean
	// Default:true
	
	disabled
	// If it is set to true,the select ,click ,check operations are disabled too.
	// Type: Boolean
	// Default:false
	
	expanded
	// Set the node is expanded or collapsed
	// Type: Boolean
	// Default:false
	
	collapsedImageUrl
	// Set the collapsed image url path of the node
	// Type: String
	// Default:''
	
	expandedImageUrl
	// Set the expanded image url path of the node
	// Type: String
	// Default:''
	
	imageUrl
	// Set the image url path of the node
	// Type: String
	// Default:''
	
	navigateUrl
	// Set the navigate url link of the node
	// Type: String
	// Default:''
	
	selected
	// Select this node when it set to true,else unselect the node
	// Type: Boolean
	// Default:false
	
	text
	// Set the node text
	// Type: String
	// Default:''
	
	toolTip
	// Set the node toolTip
	// Type: String
	// Default:''	
	
*Methods
	
	add(node,index)
	// Add a node to this element,the first params can be as follows:
	// 1.Markup html.such as '<li><a>node</a></li>' as a node.
	// 2.wijtreenode widget.
	// 3.Object options according to the options of wijtreenode.
	// the second param is a optional param.which is add to a position of the tree 
	
	remove(node)
	// Add a node to this element,the first params can be as follows:
	// 1.wijtreenode widget.
	// 2.The index of which node you determined to remove.
	
	destroy
	// Destroy this widget
	
	check(value)
	// Check or uncheck the node
	
    select
    // Select or unselect the node
    
    expand
    // Expand the node
    
    collapse
    // Collapse the node
	
