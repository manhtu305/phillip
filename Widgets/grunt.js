/// <reference path="./Wijmo/External/declarations/node.d.ts" />
/** This scripts builds Wijmo, inlcuding
* compilation of TypeScript files, expanding variables in templates (e.g. version number),
* fixing encoding issues, injecting copyright header, generating scripts suitable for AMD,
* copying files to distribution folder, etc.
*
* Since Wijmo is split into two packages (Wijmo-Open and Wijmo-Pro),
* a part of script that processes package-speicific files is located in the Wijmo\Build\package.ts.
* Besides package.ts, there are other scripts in the Build folder used by this script.
*/
var fs = require("fs");
var path = require("path");
var packageModule = require("./Wijmo/Build/package");
var util = require("./Wijmo/Build/util");
var ctl = require("./Wijmo/Build/controls");
var amd = require("./Wijmo/Build/amd");
var exporter = require("./Wijmo/Build/exporter");

var extend = require("node.extend"), thisModule = eval("module"), dist = "dist", distPro = dist + "/wijmo-pro", distOpen = dist + "/wijmo-open";

thisModule.exports = function (grunt) {
    grunt.loadNpmTasks("grunt-css");

    // load scripts form the Wijmo/Build/tasks folder
    grunt.loadTasks("Wijmo/Build/tasks");

    /** Returns CSS concatenated from all widget folders, with a copyright header */
    function concatAllCss() {
        function readCss(pack) {
            var css = fs.readFileSync(pack.combinedCssFile(), "utf-8");
            css = css.replace(/\s+/g, " ");
            return css;
        }
        var css = wijmoPro.copyright + "\r\n";
        css += readCss(wijmoOpen) + "\r\n";
        css += readCss(wijmoPro);
        fs.writeFileSync(distPro + "/css/" + allCssCombinedFilename, css, "utf-8");
    }

    /** Copies a built wijmo-pro folder and converts it to wijmo-open */
    function convertProToOpen() {
        /** Returns a list of widget file names */
        function findWidgets(pack) {
            return pack.internalScripts.map(function (s) {
                var fullname = path.basename(s.path).replace(/\.js$/, "");
                var short = fullname.replace(/(^jquery\.\w+\.wij|^wij)/i, "");
                return {
                    full: fullname,
                    short: short,
                    shortRgx: new RegExp(short, "i")
                };
            });
        }

        var openWidgets = findWidgets(wijmoOpen);
        var containerDirs = ["wijmo", "samples/widgetexplorer/samples"];

        /** Returns True if a file must be kept in the converted wijmo-open */
        function shouldBeInOpen(filename) {
            filename = filename.substr(distPro.length + 1);

            // exclude certain files
            var baseName = path.basename(filename);
            if (baseName.match(/export/i))
                return false;
            if (baseName.match(/wijmo-pro/i))
                return false;
            if (baseName.match(/COMMERCIAL-LICENSE|breeze|raphael/i))
                return false;
            if (filename === "index.html")
                return false;
            if (filename.match(/css\/images/i))
                return false;
            if (filename.match(/odata|wijmo\.data/i))
                return false;
            if (filename.match(/amd/i))
                return false;

            var parts = filename.split("/").map(function (s) {
                return s.toLowerCase();
            });
            if (parts[0] == "samples") {
                if (parts[1] == "widgetexplorermobile")
                    return false;
                if (parts[1] == "using-knockout-grid")
                    return false;
                if (parts[1] == "widgetexplorer") {
                    if (parts[2] != "samples")
                        return false;
                    if (!openWidgets.some(function (w) {
                        return parts[3].match(w.shortRgx) == null;
                    }))
                        return false;
                }
            } else if (parts[0] == "wijmo") {
                if (parts[1] === "data")
                    return false;
                if (parts[1].match(/^wij/) && !openWidgets.some(function (w) {
                    return parts[1] === "wij" + w.short;
                }))
                    return false;
            }
            return true;
        }

        // Copy all files that should be copied from wijmo-pro to wijmo-open
        grunt.file.expandFiles(distPro + "/**/*.*").filter(shouldBeInOpen).forEach(function (filename) {
            var targetFilename = filename.replace(distPro, distOpen);
            grunt.file.copy(filename, targetFilename);
        });
    }

    var pkg = grunt.file.readJSON("package.json"), wijmoOpen = new packageModule.WijmoPackage(grunt, pkg, "open", distPro), wijmoPro = new packageModule.WijmoPackage(grunt, pkg, "pro", distPro), dataScripts = grunt.file.expand("Wijmo/data/src/*.ts"), gridScripts = grunt.file.expand(["Wijmo/wijgrid/Grid.ts/Grid/*.ts", "Wijmo/wijgrid/Grid.ts/data/*.ts"]), mapScripts = grunt.file.expand("Wijmo/wijmaps/maps.ts/*.ts"), allTsScripts = wijmoOpen.scripts.concat(wijmoPro.scripts).map(function (s) {
        return s.ts;
    }).concat(gridScripts).concat(dataScripts).concat(mapScripts), packages = [wijmoOpen, wijmoPro], allCssCombinedFilename = "jquery.wijmo-pro.all." + pkg.version + ".min.css", cfg = {
        pkg: pkg
    };

    // merge package-specific configs to single cfg variable
    packages.forEach(function (p) {
        extend(true, cfg, p.getConfig());
    });
    extend(true, cfg, {
        uglify: {
            codegen: {
                ascii_only: true
            }
        },
        // generate .ts.meta files for all TypeScript scripts
        metagen: {
            src: allTsScripts
        },
        //htmlgen: {
        //    src: "Wijmo/**/*.ts.meta"
        //},
        // generate d.ts file for definition from .ts.meta files
        defgen: {
            wijmo: {
                src: "Wijmo/**/*.ts.meta",
                dest: "wijmo.d.ts"
            }
        },
        // generate .js files for Document X from .ts.meta files
        jsgen: {
            wijmo: {
                src: "Wijmo/**/*.ts.meta",
                dest: "Documentation/dx"
            }
        },
        copy: {
            // copy images used in css
            "css-images": {
                src: ["Wijmo/*/images/**/*", "Wijmo/*/images/*"],
                strip: /(Images|Wijmo\/\w+)[\/\\]/gi,
                dest: distPro + "/css/images"
            },
            // copy themes to dist
            themes: {
                src: "Themes/**/*",
                dest: distPro
            },
            // copy sample to dist
            samples: {
                src: "Samples/**/*",
                // exclude dev files, projects, object/binaries and other stuff
                except: [
                    "*-dev.html", "WidgetExplorer*.csproj", /(obj|bin)\/(debug|release)\//i,
                    "*.vspscc", "*.bak", "*.user", "*.pdb"],
                processTemplate: ["*.html", "*.js", "*.ts"],
                processFilter: "*.html",
                process: function (contents, filename) {
                    if (filename.match(/WidgetExplorer\/samples/)) {
                        // replace sampleBootstrapper link with jquery-wijmo.css, combined css file, require.js and Widget Explorer's require.wijmo.config.js
                        var rootFolder = "../../../../";
                        var newLinks = '<link href="../../../../Themes/rocket/jquery-wijmo.css" rel="stylesheet" type="text/css" />\r\n' + '    <link href="' + rootFolder + "css/" + allCssCombinedFilename + '" rel="stylesheet" type="text/css" />\r\n' + '    <script src="../../js/require.js" type="text/javascript"></script>\r\n' + '    <script src="../../js/require.wijmo.config.js" type="text/javascript"></script>';
                        contents = contents.replace(/<script[^>]+sampleBootstrapper\.js.+?<\/script>/, newLinks);
                    } else if (filename.match(/require\.wijmo\.config\.js/)) {
                        // process require.wijmo.config.js as a template
                        contents = grunt.template.process(contents);
                    }

                    return contents;
                },
                dest: distPro
            },
            // copy misc files to dist
            "wijmo-rest": {
                src: ["change.log", "index.html", "COMMERCIAL-LICENSE.txt", "Wijmo/external/**/*.js"],
                dest: distPro
            },
            // copy definition file to dist
            "wijmo-definition": {
                src: ["wijmo.d.ts"],
                dest: distPro + "/Wijmo/definition"
            }
        },
        concat: {
            // combine all wijgrid .js files (after ts compilation)
            grid: {
                src: ["copyright.txt"].concat("wijgrid.js sketchTable.js c1basefield.js c1field.js c1band.js c1groupedfield.js c1commandbutton.js c1buttonfield.js bands_traversing.js settingsManager.js dataViewWrapper.js grouper.js groupHelper.js merger.js misc.js filterOperators.js htmlTableAccessor.js cellInfo.js baseView.js flatView.js fixedView.js selection.js uiSelection.js rowAccessor.js cellEditorHelper.js cellFormatterHelper.js uiResizer.js uiDragndrop.js cellStyleFormatterHelper.js rowStyleFormatterHelper.js tally.js uiFrozener.js columnsGenerator.js uiVirtualScroller.js renderBoundsCollection.js hierarchyBuilder.js".split(" ").map(function (f) {
                    return "Grid/" + f;
                })).concat("converters.js wijmo.data.filtering.extended.js dataViewAdapter.js koDataView.js".split(" ").map(function (s) {
                    return "data/" + s;
                })).map(function (f) {
                    return "Wijmo/wijgrid/Grid.ts/" + f;
                }),
                dest: "Wijmo/wijgrid/jquery.wijmo.wijgrid.js"
            },
            maps: {
                src: ["Utils", "Drawing", "ClippingEngine", "MapProjection", "CoordConverter", "MapSources", "wijlayer", "wijvectorlayer", "wijvirtuallayer", "wijitemslayer", "wijtoolslayer", "wijmultiscaleimage", "wijmaps"].map(function (s) {
                    return "Wijmo/wijmaps/maps.ts/" + s + ".js";
                }),
                dest: "Wijmo/wijmaps/jquery.wijmo.wijmaps.js"
            }
        },
        // tsc stands for TypeScript Compiler. This task compiles .ts files
        tsc: {
            // compile all .ts files
            wijmo: {
                src: allTsScripts
            },
            // data is compiled separately, because multiple .ts files are compiled to a single .js file
            data: {
                src: "Wijmo/data/src/*.ts",
                out: "Wijmo/data/wijmo.data.js"
            }
        },
        // LINT all .ts files. See tasks\tslint.ts
        tslint: {
            src: allTsScripts
        },
        exe: {
            // run unit tests
            phantom: {
                cwd: "Test/unit",
                path: "runphantom.cmd"
            }
        }
    });

    extend(true, cfg, ctl.getConfig());

    // generate version.txt
    grunt.registerTask("versionFile", function () {
        fs.writeFileSync(dist + "/version.txt", pkg.version);
    });

    // register combined tasks
    grunt.registerTask("concat:css-all", concatAllCss);
    grunt.registerTask("css", "concat:css-open concat:css-pro concat:css-all interop-css-open copy:css-images");
    grunt.registerTask("js", "tsc concat:grid concat:maps js-open js-pro");
    grunt.registerTask("js-min", "tsc concat:grid concat:maps");
    grunt.registerTask("wijmo", "js css");
    grunt.registerTask("wijmo-min", "js-min css");
    grunt.registerTask("def", "metagen defgen copy:wijmo-definition");
    grunt.registerTask("min", "mineach js-min-open js-min-pro");
    grunt.registerTask("wijmo-release", "tsc::force wijmo min");
    grunt.registerTask("wijmo-release-min", "tsc::force wijmo-min min");
    grunt.registerTask("copy:wijmo", "copy:wijmo-open copy:wijmo-pro copy:wijmo-rest");
    grunt.registerTask("clean", function () {
        return util.rm(dist);
    });
    grunt.registerTask("bundle-pro", "wijmo-release copy:wijmo amd-js copy:themes copy:samples");
    grunt.registerTask("bundle-pro-min", "wijmo-release-min copy:wijmo amd-js copy:themes copy:samples");
    grunt.registerTask("convert-pro-to-open", convertProToOpen);
    grunt.registerTask("bundle-open", "bundle-pro convert-pro-to-open");
    grunt.registerTask("bundle", "clean bundle-pro convert-pro-to-open versionFile def");
    grunt.registerTask("test", "exe:phantom");
    grunt.registerTask("tested-bundle", "test bundle");
    grunt.registerTask("removeWijmoTempFolder", function () {
        return util.rm(distPro + "/Wijmo");
    });
    grunt.registerTask("bundle-min", "clean bundle-pro-min convert-pro-to-open versionFile removeWijmoTempFolder def");

    // register amd-specific tasks
    amd.registerTasks(grunt, distPro);

    // --- at this point configuration is completed ---
    // init grunt
    grunt.initConfig(cfg);
    packages.forEach(function (p) {
        return p.init(grunt);
    });

    // register tasks used in C1.Wijmo.Controls.* and C1.Wijmo.Extenders.* dll build
    ["Controls", "Extenders"].forEach(function (ns) {
        ctl.registerTasks(grunt, ns, wijmoOpen, wijmoPro);
    });

    exporter.registerTasks(grunt, wijmoOpen, wijmoPro);

    // the default task in "build"
    grunt.registerTask("default", "build");
};
