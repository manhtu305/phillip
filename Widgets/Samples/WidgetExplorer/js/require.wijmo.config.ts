/// <reference path="..\..\..\Wijmo\external\declarations\require.d.ts" />

module wijmo {
    var isExplorerHtml = document.location.pathname.match(/explore\.htm/),
        amdJs = "../../amd-js",
        isOnline = document.location.host.match(/\.com/),
        widgetExplorerDir = "../Samples/WidgetExplorer",
        jsDir = widgetExplorerDir + "/js",
        externalDir = "http://cdn.wijmo.com/external";

    if (isOnline) {
        amdJs = "http://cdn.wijmo.com/amd-js/<%= pkg.version %>";
        jsDir = "http://demos.componentone.com/wijmo/WidgetExplorer/js";
    }
    else if (!isExplorerHtml) {
        amdJs = "../../" + amdJs;
    }

    requirejs.config({
        baseUrl: amdJs,
        paths: {
            "jquery": "jquery-<%= pkg.dependencies.jquery %>.min",
            "jquery-ui": "jquery-ui-<%= pkg.dependencies['jquery-ui'] %>.custom.min",
            "jquery.ui": "jquery-ui",
            "globalize": "globalize.min",
            "knockout": "knockout-<%= pkg.dependencies.knockout %>",
            "swfobject": "swfobject",
            "swfupload": "swfupload",
            "jquery.bgiframe": "jquery.bgiframe",
            "jquery.cookie": "jquery.cookie",
            "breeze": "breeze.debug",
            "jquery.mousewheel": "jquery.mousewheel.min",
            "amplify": "amplify.store.min",

            "js": jsDir,

            "amplify.core": externalDir + "/amplify.core.min",
            "amplify.store": externalDir + "/amplify.store.min",
            "jquery.cookie": externalDir + "/jquery.cookie",
            "jquery.tmpl": externalDir + "/jquery.tmpl.min"
        },
        shim: {
            chartexport: [
                "wijmo.wijcompositechart"
            ],
            breeze: ["q", "jquery"],
            knockout: ["jquery"],
            "globalize.cultures": ["globalize"],

            "amplify.store": ["amplify.core"],
            "amplify": ["jquery", "amplify.store"],

            "jquery.cookie": ["jquery"],
            "jquery.tmpl": ["jquery"] 
        }
    });
}