﻿Imports System.Web.Http.Controllers
Imports System.Web.Http.Routing

Public Class CustomDirectRouteProvider
    Inherits DefaultDirectRouteProvider

    Protected Overrides Function GetActionRouteFactories(actionDescriptor As HttpActionDescriptor) As IReadOnlyList(Of IDirectRouteFactory)
        ' inherit route attributes decorated on base class controller's actions
        Return actionDescriptor.GetCustomAttributes(Of IDirectRouteFactory)(True)
    End Function

    Protected Overrides Function GetRoutePrefix(controllerDescriptor As HttpControllerDescriptor) As String
        Dim prefix = MyBase.GetRoutePrefix(controllerDescriptor)
        If String.IsNullOrEmpty(prefix) Then
            Dim prefixAttr = controllerDescriptor.GetCustomAttributes(Of IRoutePrefix)(True).FirstOrDefault()
            If prefixAttr IsNot Nothing Then
                Return prefixAttr.Prefix
            End If
        End If

        Return prefix
    End Function

End Class
