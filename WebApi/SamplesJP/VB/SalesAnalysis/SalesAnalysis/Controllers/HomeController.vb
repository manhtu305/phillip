﻿Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.Owin

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Private _userManager As ApplicationUserManager

    Public ReadOnly Property UserManager() As ApplicationUserManager
        Get
            If (_userManager Is Nothing) Then
                _userManager = HttpContext.GetOwinContext().GetUserManager(Of ApplicationUserManager)()
            End If
            Return _userManager
        End Get
    End Property

    Function Index() As ActionResult
        ViewData("Title") = "Welcome - Sales Analysis"
        Return View()
    End Function

    <Authorize>
    Public Function Analysis() As ActionResult
        ViewBag.Title = "Sales Analysis"

        Dim userId = User.Identity.GetUserId()
        Dim role = UserManager.GetRolesAsync(userId).Result.FirstOrDefault()
        ViewBag.Email = User.Identity.Name
        ViewBag.Role = role

        Dim tables = ViewPermission.All.Where(Function(i) i.Value.Roles.Any(Function(r) User.IsInRole(r))).[Select](Function(i) i.Key).ToList()
        ViewBag.Tables = tables
        Return View()
    End Function

End Class
