﻿@ModelType Boolean
@Code
    Dim screenCss = If(Model, "narrow-screen", "wide-screen")
End Code
<ul Class="site-nav @screenCss">
  <li>
        <a href = "https://www.grapecity.co.jp/developer/controls/aspnet-mvc/webapi" target="_blank">製品情報</a>
  </li>
  <li>
        <a href = "https://www.grapecity.co.jp/developer/support" target="_blank">サポート</a>
  </li>
  <li>
        <a href = "https://www.grapecity.co.jp/developer/purchase" target="_blank">ご購入</a>
  </li>
  <li Class="bold">
    <a href = "https://www.grapecity.co.jp/developer/download#net" target="_blank">トライアル版</a>
  </li>
@If (ViewBag.Email IsNot Nothing) Then
  @<li class="signout-btn">
    <a href="javascript:void(0)" onclick="logout()" title="@ViewBag.Email (@ViewBag.Role)">Sign out</a>
  </li>
End If
</ul>
