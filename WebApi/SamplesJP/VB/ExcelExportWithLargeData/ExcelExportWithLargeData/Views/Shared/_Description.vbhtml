﻿<div class="description">
    <p>
        Because browsers and JavaScript limitations, <a href="http://wijmo.com/5/docs/topic/wijmo.grid.FlexGrid.Class.html">FlexGird</a>
        control with large data(more than 50,000 rows) cannot be converted to Excel file by
        <a href="http://wijmo.com/5/docs/topic/wijmo.grid.xlsx.FlexGridXlsxConverter.Class.html">FlexGridXlsxConverter</a>
        or <a href="http://helpcentral.componentone.com/nethelp/C1WebAPI/webframe.html#C1%20Web%20API%20Client%20Javascript.html">ComponentOne Web API Client</a>.
    </p>
    <p>
        This sample demonstrates how to solve this issue. The steps:
    </p>
    <ol>
        <li>Create a MVC application with Web API framework.</li>
        <li>Prepare a large data (E.g.: 200,000 rows).</li>
        <li>Install <a href="http://helpcentral.componentone.com/nethelp/C1WebAPI/Installation.html">ComponentOne Web API Excel Services</a> NuGet packages and <a href="http://helpcentral.componentone.com/nethelp/C1WebAPI/Licensing.html">add license</a>.</li>
        <li>Create an xlsx file as the <a href="@Url.Content("~/api/storage/files/OrdersTemplate.xlsx")">template</a>.</li>
        <li><a href="http://helpcentral.componentone.com/nethelp/C1WebAPI/Configuring.NETCollections.html">Configure data provider</a> in Startup.</li>
        <li><a href="http://helpcentral.componentone.com/nethelp/C1WebAPI/ConfiguringStorage.html">Configure storage provider</a> in Startup.</li>
        <li>Add C1.Web.Mvc(in <a href="http://www.componentone.com/Studio/Platform/MVC">ComponentOne MVC Edition</a>) as reference and <a href="http://helpcentral.componentone.com/nethelp/c1mvchelpers/UsingVSTemplate.html">some configuration</a>.</li>
        <li>Add a FlexGrid to show all the large data in view page.</li>
        <li>
            Create a button for generating the Excel file from Excel Services, the url is like:<br />
            <i>~/api/excel?FileName=data&TemplateFileName=files%5COrdersTemplate.xlsx&dataname=orders</i><br />
            Note, max rows count in xls format is 65,000. So this sample just export the xlsx and csv format.
        </li>
    </ol>
</div>