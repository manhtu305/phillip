﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
#if NETCORE3
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.AspNetCore.Http.Features;
#endif
namespace C1.AspNetCore.Api
{
    public static class C1Extensions
    {
#if NETCORE3
        public static void AddC1Api(this IServiceCollection services)
        {
            services.Configure<KestrelServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });
            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            services.Configure<FormOptions>(options => {
                options.ValueCountLimit = 1024;
                options.KeyLengthLimit = 1024 * 8;
                options.ValueLengthLimit = 1024 * 1000000;
            });

            services.AddMvc().AddNewtonsoftJson();
        }

        public static void UseC1Api(this IApplicationBuilder app)
        {
            app.UseCors(builder =>
            {
                builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
            });

            app.Use(async (context, next) =>
            {
                context.Features.Get<IHttpMaxRequestBodySizeFeature>()
                    .MaxRequestBodySize = null;
                await next.Invoke();
            });

        }
#endif
    }
}
