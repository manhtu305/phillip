﻿using System;
using System.Collections.Generic;
using System.Text;

namespace C1.AspNetCore.Api
{
  public sealed class ImageFormat
  {
    //private static ImageFormat memoryBMP = new ImageFormat(new Guid("{b96b3caa-0728-11d3-9d7b-0000f81ef32e}"));
    private static ImageFormat bmp = new ImageFormat(new Guid("{b96b3cab-0728-11d3-9d7b-0000f81ef32e}"));
    //private static ImageFormat emf = new ImageFormat(new Guid("{b96b3cac-0728-11d3-9d7b-0000f81ef32e}"));
    //private static ImageFormat wmf = new ImageFormat(new Guid("{b96b3cad-0728-11d3-9d7b-0000f81ef32e}"));
    private static ImageFormat jpeg = new ImageFormat(new Guid("{b96b3cae-0728-11d3-9d7b-0000f81ef32e}"));
    private static ImageFormat png = new ImageFormat(new Guid("{b96b3caf-0728-11d3-9d7b-0000f81ef32e}"));
    private static ImageFormat gif = new ImageFormat(new Guid("{b96b3cb0-0728-11d3-9d7b-0000f81ef32e}"));
    private static ImageFormat tiff = new ImageFormat(new Guid("{b96b3cb1-0728-11d3-9d7b-0000f81ef32e}"));
    //private static ImageFormat exif = new ImageFormat(new Guid("{b96b3cb2-0728-11d3-9d7b-0000f81ef32e}"));
    //private static ImageFormat photoCD = new ImageFormat(new Guid("{b96b3cb3-0728-11d3-9d7b-0000f81ef32e}"));
    //private static ImageFormat flashPIX = new ImageFormat(new Guid("{b96b3cb4-0728-11d3-9d7b-0000f81ef32e}"));
    //private static ImageFormat icon = new ImageFormat(new Guid("{b96b3cb5-0728-11d3-9d7b-0000f81ef32e}"));


    private Guid guid;

    /// <summary>
    ///    Initializes a new instance of the <see cref='System.Drawing.Imaging.ImageFormat'/> class with the specified GUID.
    /// </summary>
    public ImageFormat(Guid guid)
    {
      this.guid = guid;
    }

    /// <summary>
    ///    Specifies a global unique identifier (GUID)
    ///    that represents this <see cref='System.Drawing.Imaging.ImageFormat'/>.
    /// </summary>
    public Guid Guid
    {
      get { return guid; }
    }

    ///// <summary>
    /////    Specifies a memory bitmap image format.
    ///// </summary>
    //public static ImageFormat MemoryBmp
    //{
    //  get { return memoryBMP; }
    //}

    /// <summary>
    ///    Specifies the bitmap image format.
    /// </summary>
    public static ImageFormat Bmp
    {
      get { return bmp; }
    }

    ///// <summary>
    /////    Specifies the enhanced Windows metafile
    /////    image format.
    ///// </summary>
    //public static ImageFormat Emf
    //{
    //  get { return emf; }
    //}

    ///// <summary>
    /////    Specifies the Windows metafile image
    /////    format.
    ///// </summary>
    //public static ImageFormat Wmf
    //{
    //  get { return wmf; }
    //}

    /// <summary>
    ///    Specifies the GIF image format.
    /// </summary>
    public static ImageFormat Gif
    {
      get { return gif; }
    }

    /// <summary>
    ///    Specifies the JPEG image format.
    /// </summary>
    public static ImageFormat Jpeg
    {
      get { return jpeg; }
    }

    /// <summary>
    ///    <para>
    ///       Specifies the W3C PNG image format.
    ///    </para>
    /// </summary>
    public static ImageFormat Png
    {
      get { return png; }
    }

    /// <summary>
    ///    Specifies the Tag Image File
    ///    Format (TIFF) image format.
    /// </summary>
    public static ImageFormat Tiff
    {
      get { return tiff; }
    }

    ///// <summary>
    /////    Specifies the Exchangable Image Format
    /////    (EXIF).
    ///// </summary>
    //public static ImageFormat Exif
    //{
    //  get { return exif; }
    //}

    ///// <summary>
    /////    <para>
    /////       Specifies the Windows icon image format.
    /////    </para>
    ///// </summary>
    //public static ImageFormat Icon
    //{
    //  get { return icon; }
    //}

    /// <include file='doc\ImageFormat.uex' path='docs/doc[@for="ImageFormat.Equals"]/*' />
    /// <summary>
    ///    Returns a value indicating whether the
    ///    specified object is an <see cref='System.Drawing.Imaging.ImageFormat'/> equivalent to this <see cref='System.Drawing.Imaging.ImageFormat'/>.
    /// </summary>
    public override bool Equals(object o)
    {
      ImageFormat format = o as ImageFormat;
      if (format == null)
        return false;
      return this.guid == format.guid;
    }

    /// <include file='doc\ImageFormat.uex' path='docs/doc[@for="ImageFormat.GetHashCode"]/*' />
    /// <summary>
    ///    <para>
    ///       Returns a hash code.
    ///    </para>
    /// </summary>
    public override int GetHashCode()
    {
      return this.guid.GetHashCode();
    }

    /// <summary>
    ///    Converts this <see cref='System.Drawing.Imaging.ImageFormat'/> to a human-readable string.
    /// </summary>
    public override string ToString()
    {
      //if (this == memoryBMP) return "MemoryBMP";
      if (this == bmp) return "Bmp";
      //if (this == emf) return "Emf";
      //if (this == wmf) return "Wmf";
      if (this == gif) return "Gif";
      if (this == jpeg) return "Jpeg";
      if (this == png) return "Png";
      if (this == tiff) return "Tiff";
      //if (this == exif) return "Exif";
      //if (this == icon) return "Icon";
      return "[ImageFormat: " + guid + "]";
    }
  }
}
