﻿using C1.Web.Api.BarCode;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace C1.AspNetCore.Api.UnitTest.BarCode
{

    [TestClass]
    public class BarCodeTest
    {
        private static BarCodeController _controller = new BarCodeControllerImpl();
        private static BarCodeRequest _req = new BarCodeRequest();
        private static IActionResult _res;

        [TestMethod]
        public void Sumary()
        {
            var arrTest = this.GetType().GetMethods().Where(x => x.Name.Contains("Test"));
            foreach (var x in arrTest) x.Invoke(this, null);
            arrTest = null;
        }

        [TestMethod]
        public void Test0()
        {
            _req.Text = "123456789";
            _res = _controller.GetByJson(JsonConvert.SerializeObject(_req));
        }

        [TestMethod]
        public void Test1()
        {
            _req.CodeType = GrapeCity.Documents.Barcode.CodeType.Ansi39;
            _res = _controller.GetByJson(JsonConvert.SerializeObject(_req));
        }

        [TestMethod]
        public void Test2()
        {
            _req.BarWidth = 300;
            _req.BarHeight = 200;
            _res = _controller.GetByJson(JsonConvert.SerializeObject(_req));
        }

        [TestMethod]
        public void Test3()
        {
            _req.Font.Family = "Times New Roman";
            _req.Font.Size = 20;
            _req.Font.Bold = true;
            _req.Font.Italic = true;
            _req.Font.Underline = true;
            _res = _controller.GetByJson(JsonConvert.SerializeObject(_req));
        }

        [TestMethod]
        public void Test4()
        {
            _req.AdditionalNumber = "000";
            _res = _controller.GetByJson(JsonConvert.SerializeObject(_req));
        }

        [TestMethod]
        public void Test5()
        {
            _req.CaptionPosition = GrapeCity.Documents.Barcode.BarCodeCaptionPosition.Below;
            _res = _controller.GetByJson(JsonConvert.SerializeObject(_req));
        }

        [TestMethod]
        public void Test6()
        {
            _req.CaptionAlignment = GrapeCity.Documents.Text.TextAlignment.Center;
            _res = _controller.GetByJson(JsonConvert.SerializeObject(_req));
        }

        [TestMethod]
        public void Test7()
        {
            _req.BarDirection = GrapeCity.Documents.Barcode.BarCodeDirection.TopToBottom;
            _res = _controller.GetByJson(JsonConvert.SerializeObject(_req));
        }

        [TestMethod]
        public void Test8()
        {
            _req.ModuleSize = 2;
            _res = _controller.GetByJson(JsonConvert.SerializeObject(_req));
        }

        [TestMethod]
        public void Test9()
        {
            _req.CaptionGrouping = true;
            _res = _controller.GetByJson(JsonConvert.SerializeObject(_req));
        }

        [TestMethod]
        public void Test10()
        {
            _req.BackColor = System.Drawing.Color.Black;
            _req.ForeColor = System.Drawing.Color.White;
            _res = _controller.GetByJson(JsonConvert.SerializeObject(_req));
        }

        [TestMethod]
        public void Test11()
        {
            _req.DPIs.Horizontal = _req.DPIs.Vertical = 120.0f;
            _res = _controller.GetByJson(JsonConvert.SerializeObject(_req));
        }

        [TestMethod]
        public void Test12()
        {
            _req.ImgAlign.Horizontal = GrapeCity.Documents.Drawing.ImageAlignHorz.Center;
            _req.ImgAlign.Vertical = GrapeCity.Documents.Drawing.ImageAlignVert.Center;
            _res = _controller.GetByJson(JsonConvert.SerializeObject(_req));
        }

        [TestMethod]
        public void Test13()
        {
            _req.FileName = "Test13";
            _req.Type = Web.Api.ExportFileType.Pdf;
            _res = _controller.GetByJson(JsonConvert.SerializeObject(_req));
        }
    }

    internal class BarCodeControllerImpl : BarCodeController {
    }
}
