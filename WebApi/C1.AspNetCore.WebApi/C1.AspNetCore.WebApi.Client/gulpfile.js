'use strict';
const clean = require('gulp-clean');
const gulp = require('gulp');
const fs = require('fs');

gulp.task('clean', async (done) => {
    if (await fs.existsSync('./node_modules') && await fs.existsSync('./dist'))
        return gulp.src('./dist', {
                read: false
            })
            .pipe(clean());
    done();
});

gulp.task("build", async (done) => {
    try {
        done();
    } catch (err) {
        done();
    }
});