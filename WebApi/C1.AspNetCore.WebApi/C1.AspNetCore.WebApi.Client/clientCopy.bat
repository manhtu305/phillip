set ProjectDir=%1

cd /D %ProjectDir%

pushd ..\
set fromDirectory=%CD%\C1.AspNetCore.WebApi.Client\dist\visitor
set toDirectory=%CD%\C1.AspNetCore.WebApi.Visitor\wwwroot\visitor\
popd

@echo Begin Copy from %fromDirectory% to %toDirectory%
XCOPY /e /y %fromDirectory%\*.js %toDirectory%