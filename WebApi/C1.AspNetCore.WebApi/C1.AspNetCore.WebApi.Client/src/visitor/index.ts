module c1.webapi {
    export class Visitor {
        private _window: Window;
        private _document: Document;

        private _navigator: Navigator;

        private _visitorId: string = "";

        constructor() {
            this._window = window;
            this._document = document;
            this._navigator = navigator;
        }

        /**
         * Get all the information of the visitor including ip, geo, locale, session, browser, os, device..
         */
        public getData(): IVisitor {
            return this.createVisitorData();
        }

        /**
       * Get all the information of the visitor including ip, geo, locale, session, browser, os, device..
       */
        public getDataAsync(dataHandler: (completedVisitorData: IVisitor) => void) {
            const visitorData: IVisitor = this.createVisitorData();
            const fingerprint = new BrowserFingerprint(VisitorConfig.API_HOST.url);
            const scanner = new BrowserInfoService();

            visitorData.browserInfo = scanner.scan();
            this.getGeolocationAsync(data => {
                visitorData.geo = data;
                fingerprint
                    .getFingerPrintId((id: string) => {
                        visitorData.visitorId = id;
                        dataHandler && dataHandler(visitorData);
                    }, visitorData);
            });
        }

        private createVisitorData() {
            return {
                visitorId: this._visitorId,
                ip: this.getIpAddress(),
                geo: this.getGeolocation(),
                locale: this.getLocale(),
                session: this.getCurrentSession(),
                firstSession: this.getFirstSession(),
                browser: this.getWebBrowserInformation(),
                os: this.getOperatingSystem(),
                device: this.getDevice()
            }
        }

        /**
         * Get information of the visitor's device.
         */
        public getDevice(): IDevice {
            let viewportWidth, viewportHeight;
            try { viewportWidth = this._window.innerWidth || this._document.documentElement.clientWidth || this._document.body.clientWidth; } catch (e) { viewportWidth = 0; }
            try { viewportHeight = this._window.innerHeight || this._document.documentElement.clientHeight || this._document.body.clientHeight; } catch (e) { viewportHeight = 0; }

            const parser = new UserAgentParser(this._navigator.userAgent);
            const device = parser.getDevice();
            const deviceName = device.model ? device.model : "";

            let deviceInfo: IDevice = <IDevice>{
                name: deviceName,
                version: "",
                screen: {
                    resolution: `${window.screen.width}x${window.screen.height}`,
                    width: window.screen.width,
                    height: window.screen.height
                },
                viewport: {
                    resolution: `${viewportWidth}x${viewportHeight}`,
                    width: viewportWidth,
                    height: viewportHeight
                },
                isTablet: !!this._navigator.userAgent.match(/(iPad|SCH-I800|xoom|kindle)/i),
                isMobile: !!this._navigator.userAgent.match(/(iPhone|iPod|blackberry|android|htc|lg|midp|mmp|mobile|nokia|opera mini|palm|pocket|psp|sgh|smartphone|symbian|treo mini|Playstation Portable|SonyEricsson|Samsung|MobileExplorer|PalmSource|Benq|Windows Phone|Windows Mobile|IEMobile|Windows CE|Nintendo Wii)/i)
            }
            return deviceInfo;
        }

        /**
         * Get opering system of the client. return {@link IOperatingSystem} object.
         */
        public getOperatingSystem(): IOperatingSystem {
            // system
            let os = "", nAgt = this._navigator.userAgent, nVer = this._navigator.appVersion;
            var clientStrings = [
                { s: 'Windows 10', r: /(Windows 10.0|Windows NT 10.0)/ },
                { s: 'Windows 8.1', r: /(Windows 8.1|Windows NT 6.3)/ },
                { s: 'Windows 8', r: /(Windows 8|Windows NT 6.2)/ },
                { s: 'Windows 7', r: /(Windows 7|Windows NT 6.1)/ },
                { s: 'Windows Vista', r: /Windows NT 6.0/ },
                { s: 'Windows Server 2003', r: /Windows NT 5.2/ },
                { s: 'Windows XP', r: /(Windows NT 5.1|Windows XP)/ },
                { s: 'Windows 2000', r: /(Windows NT 5.0|Windows 2000)/ },
                { s: 'Windows ME', r: /(Win 9x 4.90|Windows ME)/ },
                { s: 'Windows 98', r: /(Windows 98|Win98)/ },
                { s: 'Windows 95', r: /(Windows 95|Win95|Windows_95)/ },
                { s: 'Windows NT 4.0', r: /(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/ },
                { s: 'Windows CE', r: /Windows CE/ },
                { s: 'Windows 3.11', r: /Win16/ },
                { s: 'Android', r: /Android/ },
                { s: 'Open BSD', r: /OpenBSD/ },
                { s: 'Sun OS', r: /SunOS/ },
                { s: 'Chrome OS', r: /CrOS/ },
                { s: 'Linux', r: /(Linux|X11(?!.*CrOS))/ },
                { s: 'iOS', r: /(iPhone|iPad|iPod)/ },
                { s: 'Mac OS X', r: /Mac OS X/ },
                { s: 'Mac OS', r: /(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/ },
                { s: 'QNX', r: /QNX/ },
                { s: 'UNIX', r: /UNIX/ },
                { s: 'BeOS', r: /BeOS/ },
                { s: 'OS/2', r: /OS\/2/ },
                { s: 'Search Bot', r: /(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/ }
            ];
            for (var id in clientStrings) {
                var cs = clientStrings[id];
                if (cs.r.test(nAgt)) {
                    os = cs.s;
                    break;
                }
            }

            let osVersion = "";
            if (/Windows/.test(os)) {
                osVersion = /Windows (.*)/.exec(os)[1];
                os = 'Windows';
            }

            switch (os) {
                case 'Mac OS X':
                    osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                    break;

                case 'Android':
                    osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                    break;

                case 'iOS':
                    let regx = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
                    osVersion = regx[1] + '.' + regx[2] + '.' + (regx[3] ? regx[3] : 0);
                    break;
            }
            let operatingSystem: IOperatingSystem = {
                name: os,
                version: osVersion,
                platform: this._navigator.userAgent.match(/x86_64|Win64|WOW64|x86-64|x64\;|AMD64|amd64/) || ((<any>this._navigator).cpuClass === 'x64') ? 'x64' : 'x86'
            };
            return operatingSystem;
        }

        /**
         * Detect browser information of the visitor's device.
         */
        public getWebBrowserInformation(): IWebBrowser {
            // browser
            let userAgent = navigator.userAgent;
            let browser = navigator.appName;
            let version = '' + parseFloat(navigator.appVersion);
            let majorVersion = parseInt(navigator.appVersion, 10);
            let nameOffset, verOffset, ix;

            // Opera
            if ((verOffset = userAgent.indexOf('Opera')) != -1) {
                browser = 'Opera';
                version = userAgent.substring(verOffset + 6);
                if ((verOffset = userAgent.indexOf('Version')) != -1) {
                    version = userAgent.substring(verOffset + 8);
                }
            }
            // Opera Next
            if ((verOffset = userAgent.indexOf('OPR')) != -1) {
                browser = 'Opera';
                version = userAgent.substring(verOffset + 4);
            }
            // Edge
            else if ((verOffset = userAgent.indexOf('Edge')) != -1) {
                browser = 'Microsoft Edge';
                version = userAgent.substring(verOffset + 5);
            }
            // Edge
            else if ((verOffset = userAgent.indexOf('Edg/')) != -1) {
                browser = 'Microsoft Edge';
                version = userAgent.substring(verOffset + 4);
            }
            // MSIE
            else if ((verOffset = userAgent.indexOf('MSIE')) != -1) {
                browser = 'Microsoft Internet Explorer';
                version = userAgent.substring(verOffset + 5);
            }
            // Chrome
            else if ((verOffset = userAgent.indexOf('Chrome')) != -1) {
                browser = 'Chrome';
                version = userAgent.substring(verOffset + 7);
            }
            // Chrome in iOS
            else if ((verOffset = userAgent.indexOf('CriOS')) != -1) {
                browser = 'Chrome';
                version = userAgent.substring(verOffset + 6);
            }
            // Safari
            else if ((verOffset = userAgent.indexOf('Safari')) != -1) {
                browser = 'Safari';
                version = userAgent.substring(verOffset + 7);
                if ((verOffset = userAgent.indexOf('Version')) != -1) {
                    version = userAgent.substring(verOffset + 8);
                }
            }
            // Firefox
            else if ((verOffset = userAgent.indexOf('Firefox')) != -1) {
                browser = 'Firefox';
                version = userAgent.substring(verOffset + 8);
            }
            // MSIE 11+
            else if (userAgent.indexOf('Trident/') != -1) {
                browser = 'Microsoft Internet Explorer';
                version = userAgent.substring(userAgent.indexOf('rv:') + 3);
            }
            // Other browsers
            else if ((nameOffset = userAgent.lastIndexOf(' ') + 1) < (verOffset = userAgent.lastIndexOf('/'))) {
                browser = userAgent.substring(nameOffset, verOffset);
                version = userAgent.substring(verOffset + 1);
                if (browser.toLowerCase() == browser.toUpperCase()) {
                    browser = navigator.appName;
                }
            }

            // trim the version string
            if ((ix = version.indexOf(';')) != -1) version = version.substring(0, ix);
            if ((ix = version.indexOf(' ')) != -1) version = version.substring(0, ix);
            if ((ix = version.indexOf(')')) != -1) version = version.substring(0, ix);

            majorVersion = parseInt('' + version, 10);
            if (isNaN(majorVersion)) {
                version = '' + parseFloat(navigator.appVersion);
                majorVersion = parseInt(navigator.appVersion, 10);
            }

            return {
                name: browser,
                version: version,
                majorVersion: majorVersion
            }
        }

        /**
         * Get first session information when visitor landed on the website.
         */
        public getFirstSession(): IFirstSession {
            return SessionUtils.getSession(VisitorConfig.SESSION_COOKIE_NAME, VisitorConfig.SESSION_TIMEOUT * 24 * 60 * 60 * 1000);
        }

        /**
         * Get current session information of the visitor.
         */
        public getCurrentSession(): ICurrentSession {
            return SessionUtils.getSession();
        }

        /**
         * Detect the locale of the visitor's device.
         */
        public getLocale(): ILocale {
            let lang = ((
                this._navigator.language ||
                (<any>this._navigator)['browserLanguage'] ||
                (<any>this._navigator)['systemLanguage'] ||
                (<any>this._navigator)['userLanguage']
            ) || '').split("-");
            if (lang.length == 2) {
                return { countryCode: lang[1].toLowerCase(), languageCode: lang[0].toLowerCase() };
            } else if (lang) {
                return { languageCode: lang[0].toLowerCase(), countryCode: null };
            } else { return { languageCode: null, countryCode: null }; }
        }

        /**
         * Get the ip address of the visitor.
         */
        public getIpAddress(): IIPAddress {
            let ip: IIPAddress = {
                address: VisitorConfig.SERVER_DATA ? VisitorConfig.SERVER_DATA.ipAddress : ""
            }
            return ip;
        }

        /**
         * Detect geolocation information of the vistor.
         */
        public getGeolocation(): IGeoLocation {
            let geo: IGeoLocation = {
                countryCode: "",
                countryName: "",
                cityName: "",
                postalCode: "",
                regionName: "",
                timeZone: "",
                latitude: 0,
                longitude: 0
            };
            if (VisitorConfig.SERVER_DATA && VisitorConfig.SERVER_DATA.geoLocation) {
                geo = VisitorConfig.SERVER_DATA.geoLocation;
            }
            return geo;
        }

        /**
         * Detect geolocation using google map api.
         */
        public getGeolocationAsync(callback: Function) {

            let geo = this.getGeolocation();

            if (!VisitorConfig.GOOGLEMAP_API_KEY) {
                if (callback) callback(geo);
                return;
            }

            let ggService = new GoogleMapService(VisitorConfig.GOOGLEMAP_API_KEY);
            ggService.getGeolocation(data => {
                if (callback) callback(data);
            }, err => { });
        }
    }

    if (!window['visitor']) {
        window['visitor'] = new Visitor();
    }
}

