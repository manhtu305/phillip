module c1.webapi {

    export class CookieUtils {

        /**
         * Get Cookie value stored in the website.
         * @param cookie_name Name of the stored cookie.  
         * @param result The output represented cookie value.
         */
        public static getCookie(cookie_name: string, result?: any) {
            return (result = new RegExp('(?:^|; )' + encodeURIComponent(cookie_name) + '=([^;]*)').exec(document.cookie)) ? decodeURIComponent(result[1]) : null;
        }

        /**
         * Get json object that represented cookie data. Remove the version information from the cookie.
         * @param cookie_name name of the cookie stored in the browser.
         */
        public static unpackCookie(cookie_name: string) {
            let obj;
            try { obj = JSON.parse(CookieUtils.getCookie(cookie_name)); } catch (e) { }
            if (obj && obj.version == VisitorConfig.API_VERSION) {
                delete obj.version; return obj;
            }
        }

        /**
         * Add api version into cookie data.
         * @param obj 
         */
        public static packCookie(obj: any) {
            if (obj) {
                obj.version = VisitorConfig.API_VERSION;
                var ret = JSON.stringify(obj);
                delete obj.version;
                return ret;
            }
        }

        /**
         * Set the cookie data
         * @param cname cookie name
         * @param value data stored by the cookie
         * @param expires number in millisecond when the cookie will be expired.
         * @param options additional option when saving the cookie.
         */
        public static setCookie(cname: string, value: any, expires: any, options?: any) {
            if (!cname) { return null; }
            if (!options) { options = {}; }
            if (value === null || value === undefined) { expires = -1; }
            if (expires) { options.expires = (new Date().getTime()) + expires; }
            return (document.cookie = [
                encodeURIComponent(cname), '=',
                encodeURIComponent(String(value)),
                options.expires ? '; expires=' + new Date(options.expires).toUTCString() : '',
                '; path=' + (options.path ? options.path : '/'),
                options.domain ? '; domain=' + options.domain : '',
                (window.location && window.location.protocol === 'https:') ? '; secure' : ''
            ].join(''));
        }

    }
}