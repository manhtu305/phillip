module c1.webapi {
    export class SessionUtils {

        /**
         * Get reference information form the location of the website
         * @param url_str the location of the website.
         */
        public static parseUrl(url_str: string) {

            if (!url_str) {

                return {
                    host: '',
                    path: '',
                    protocol: '',
                    port: undefined,
                    search: '',
                    query: {}
                }
            }

            let a = document.createElement("a"), query = {};
            a.href = url_str; var query_str = a.search.substr(1);
            if (query_str != '') {
                var pairs = query_str.split("&"), i = 0,
                    length = pairs.length, parts;
                for (; i < length; i++) {
                    parts = pairs[i].split("=");
                    if (parts.length === 2) {
                        (<any>query)[parts[0]] = decodeURI(parts[1]);
                    }
                }
            }
            return {
                host: a.host,
                path: a.pathname,
                protocol: a.protocol,
                port: a.port === '' ? 80 : a.port,
                search: a.search,
                query: query
            }
        }

        /**
         * Get session information by the given cookie. If there isn't session, create a new one and save it to machine.
         * @param cookie cookie data of the session
         * @param expires number in millisecond when the cookie will be expired.
         */
        public static getSession(cookie?: any, expires?: any) {
            let session = CookieUtils.unpackCookie(cookie);
            if (session == null) {
                session = {
                    visits: 1,
                    start: new Date(),
                    last_visit: new Date(),
                    url: window.location.href,
                    path: window.location.pathname,
                    referrer: document.referrer,
                    referrer_info: this.parseUrl(document.referrer),
                    search: { engine: null, query: null }
                };
                let search_engines = [
                    { name: "Google", host: "google", query: "q" },
                    { name: "Bing", host: "bing.com", query: "q" },
                    { name: "Yahoo", host: "search.yahoo", query: "p" },
                    { name: "AOL", host: "search.aol", query: "q" },
                    { name: "Ask", host: "ask.com", query: "q" },
                    { name: "Baidu", host: "baidu.com", query: "wd" }
                ], length = search_engines.length,
                    engine, match, i = 0,
                    fallbacks = 'q query term p wd query text'.split(' ');
                for (i = 0; i < length; i++) {
                    engine = search_engines[i];
                    if (session.referrer_info.host.indexOf(engine.host) !== -1) {
                        session.search.engine = engine.name;
                        session.search.query = session.referrer_info.query[engine.query];
                        session.search.terms = session.search.query ? session.search.query.split(" ") : null;
                        break;
                    }
                }

                if (session.search.engine === null && session.referrer_info.search.length > 1) {
                    for (i = 0; i < fallbacks.length; i++) {
                        var terms = session.referrer_info.query[fallbacks[i]];
                        if (terms) {
                            session.search.engine = "Unknown";
                            session.search.query = terms;
                            session.search.terms = terms.split(" ");
                            break;
                        }
                    }
                }
            } else {
                session.start = new Date(session.start);
                session.prev_visit = new Date(session.last_visit);
                session.last_visit = new Date();
                session.visits++;
                session.time_since_last_visit = session.last_visit.getTime() - session.prev_visit.getTime();
            }

            CookieUtils.setCookie(cookie, CookieUtils.packCookie(session), expires);

            return session;
        }
    }
}