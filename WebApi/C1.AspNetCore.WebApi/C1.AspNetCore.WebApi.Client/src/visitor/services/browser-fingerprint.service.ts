module c1.webapi {
    export class BrowserFingerprint {

        private _host: string = '';

        constructor(apiHostUrl: string) {
            this._host = this.removeEndSlashIfNeeded(apiHostUrl);
        }

        private removeEndSlashIfNeeded(apiHostUrl: string) {
            if (apiHostUrl.indexOf("/", apiHostUrl.length - 1) !== -1) {                
                return apiHostUrl.substring(0, apiHostUrl.length - 2);
            }

            return apiHostUrl;
        }                

        public getFingerPrintId(idReceivedHandler: (id: string) => void, visitorData: IVisitor) {            
            const request = new XMLHttpRequest();

            request.onreadystatechange = () => {
                if (request.readyState !== 4) return;

                if (request.status >= 200 && request.status < 300) {
                    idReceivedHandler(request.responseText);
                } else {
                    throw request.statusText;
                }
            };

            request.open('POST', `${this._host}/api/visitor/browserfingerprintid`);
            request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            request.send(JSON.stringify(visitorData));
        }
    }
}