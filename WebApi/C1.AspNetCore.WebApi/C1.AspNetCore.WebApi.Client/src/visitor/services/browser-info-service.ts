module c1.webapi {

    export class BrowserInfoService {

        private _navigator: Navigator;

        public constructor() {
            this._navigator = window.navigator;
        }

        public scan(): IBrowserInfo {

            const components = <IBrowserInfo>{};

            components.webdriver = this._navigator.webdriver;
            components.language = this._navigator.language;
            components.colorDepth = window.screen.colorDepth;
            components.deviceMemory = (<any>this._navigator).deviceMemory;
            components.pixelRatio = window.devicePixelRatio;
            components.hardwareConcurrency = this._navigator.hardwareConcurrency;
            components.sessionStorage = !!window.sessionStorage;
            components.localStorage = !!window.localStorage;
            components.indexedDb = !!window.indexedDB;
            components.addBehavior = !!(<any>document.body).addBehavior;
            components.openDatabase = !!(<any>window).openDatabase;
            components.cpuClass = (<any>this._navigator).cpuClass;
            components.platform = this._navigator.platform;
            components.doNotTrack = this._navigator.doNotTrack;
            components.plugins = this._navigator.plugins;
            components.adBlock = this.getAdBlock();
            components.touchSupport = this.getTouchSupport();
            components.fonts = new FontDetector().getFonts();
            components.canvas = new CanvasDetector().getCanvas();
            components.webglVendorAndRenderer = this.getWebglVendorAndRenderer();

            return components;
        }

        getWebglCanvas(): any {
            var canvas = document.createElement('canvas');
            var gl = null;
            try {
                gl = canvas.getContext('webgl') || canvas.getContext('experimental-webgl');
            } catch (e) { /* squelch */ }
            if (!gl) { gl = null }
            return gl;
        }

        loseWebglContext(context): any {
            var loseContextExtension = context.getExtension('WEBGL_lose_context')
            if (loseContextExtension != null) {
                loseContextExtension.loseContext();
            }
        }

        getWebglVendorAndRenderer(): any {
            try {
                var glContext = this.getWebglCanvas();
                var extensionDebugRendererInfo = glContext.getExtension('WEBGL_debug_renderer_info');
                var params = glContext.getParameter(extensionDebugRendererInfo.UNMASKED_VENDOR_WEBGL) +
                    '~' + glContext.getParameter(extensionDebugRendererInfo.UNMASKED_RENDERER_WEBGL);
                this.loseWebglContext(glContext);
                return params;
            } catch (e) {
                return null;
            }
        }

        getAdBlock(): boolean {
            const ads = document.createElement('div');
            ads.innerHTML = '&nbsp;';
            ads.className = 'adsbox';
            let result = false;
            try {

                document.body.appendChild(ads);
                result = (<Element & { offsetHeight: number }>document.getElementsByClassName('adsbox')[0]).offsetHeight === 0;
                document.body.removeChild(ads);
            } catch (e) {
                result = false;
            }

            return result;
        }

        getTouchSupport(): Array<any> {
            let maxTouchPoints = 0;
            let touchEvent: boolean;

            if (typeof navigator.maxTouchPoints !== 'undefined') {
                maxTouchPoints = navigator.maxTouchPoints;
            } else if (typeof navigator.msMaxTouchPoints !== 'undefined') {
                maxTouchPoints = navigator.msMaxTouchPoints;
            }
            try {
                document.createEvent('TouchEvent');
                touchEvent = true;
            } catch (_) {
                touchEvent = false;
            }
            const touchStart = 'ontouchstart' in window;
            return [maxTouchPoints, touchEvent, touchStart];
        }
    }
}