module c1.webapi {
 
    export interface AddressComponent {
        long_name: string;
        short_name: string;
        types: Array<string>;
    }

    export interface Address {
        cityName?: string;
        countryName?: string;
        countryCode?: string;
        postalCode?: string;
    }

    export class GoogleAddressParser {
        private address: Address = {};

        constructor(private address_components: Array<AddressComponent>) {
            this.parseAddress();
        }

        private parseAddress() {
            if (!Array.isArray(this.address_components)) {
                throw Error('Address Components is not an array');
            }

            if (!this.address_components.length) {
                throw Error('Address Components is empty');
            }

            for (let i = 0; i < this.address_components.length; i++) {
                const component: AddressComponent = this.address_components[i];

                if (this.isState(component) && !this.address.cityName) {
                    this.address.cityName = component.long_name;
                }

                if (this.isCity(component)) {
                    this.address.cityName = component.long_name;
                }

                if (this.isCountry(component)) {
                    this.address.countryName = component.long_name;
                    this.address.countryCode = component.short_name;
                }

                if (this.isPostalCode(component)) {
                    this.address.postalCode = component.long_name;
                }
            }
        }

        private isCity(component): boolean {
            return component.types.indexOf('locality') != -1;
        }

        private isState(component): boolean {
            return component.types.indexOf('administrative_area_level_1') != -1;
        }

        private isCountry(component): boolean {
            return component.types.indexOf('country') != -1;
        }

        private isPostalCode(component): boolean {
            return component.types.indexOf('postal_code') != -1;
        }

        result(): Address {
            return this.address;
        }
    }

    export class GoogleMapService {

        private API_KEY: string = "";

        constructor(apikey?: string) {
            if (apikey) this.API_KEY = apikey;
        }

        _requireApiKey() {
            if (!this.API_KEY) throw new Error("Missing a valid API key");
        }

        private _getLatlng(resolve: Function, reject: Function) {
            let request = new XMLHttpRequest();

            // Setup our listener to process compeleted requests
            request.onreadystatechange = () => {

                // Only run if the request is complete
                if (request.readyState !== 4) return;

                // Process the response
                if (request.status >= 200 && request.status < 300) {
                    // If successful
                    resolve(JSON.parse(request.responseText));
                } else {
                    // If failed
                    reject({
                        status: request.status,
                        statusText: request.statusText
                    });
                }

            };

            // Setup our HTTP request
            request.open('POST', `https://www.googleapis.com/geolocation/v1/geolocate?key=${this.API_KEY}`, true);

            // Send the request
            request.send();

        }

        private _getAddress(data: { lat: number, lng: number }, resolve: Function, reject: Function) {
            let request = new XMLHttpRequest();
            // Setup our listener to process compeleted requests
            request.onreadystatechange = () => {

                // Only run if the request is complete
                if (request.readyState !== 4) return;

                // Process the response
                if (request.status >= 200 && request.status < 300) {
                    // If successful
                    resolve(JSON.parse(request.responseText));
                } else {
                    // If failed
                    reject({
                        status: request.status,
                        statusText: request.statusText
                    });
                }
            };

            // Setup our HTTP request
            request.open('GET', `https://maps.googleapis.com/maps/api/geocode/json?latlng=${data.lat},${data.lng}&key=${this.API_KEY}`, true);

            // Send the request
            request.send();
        }
        public getGeolocation(resolve: Function, reject: Function) {
            let geo: IGeoLocation = {
                countryCode: "",
                countryName: "",
                cityName: "",
                postalCode: "",
                regionName: "",
                timeZone: "",
                latitude: 0,
                longitude: 0
            };

            this._getLatlng(data => {
                if (!data || !data['location']) return reject();
                geo.latitude = data['location'].lat;
                geo.longitude = data['location'].lng;
                this._getAddress(data['location'], res => {
                    let parser = new GoogleAddressParser(res["results"][0].address_components).result();
                    geo = { ...geo, ...parser };
                    return resolve(geo);
                }, err => { 
                    reject(err);
                });
            }, err => {
                reject(err);
            });
        }
    }
}