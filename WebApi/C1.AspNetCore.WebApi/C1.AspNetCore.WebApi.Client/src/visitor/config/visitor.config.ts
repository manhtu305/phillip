module c1.webapi {
    /**
     * Configuration for the Visitor API
     */
    export class VisitorConfig {
        /**
         * The number version of the visitor api.
         */
        public static API_VERSION: string = "1.0.0";
        /**
        * The number version of the visitor api.
        */
        public static GOOGLEMAP_API_KEY: string = "";
        /**
         * The number of days the session cookie will expired. 
         */
        public static SESSION_TIMEOUT: number = 32;
        /**
         * The cookie name of the session will be stored
         */
        public static SESSION_COOKIE_NAME: string = "visitor_first_session";
        /**
        * The number version of the visitor api.
        */
        public static SERVER_DATA: any = null;
        /**
         * The host url of the visitor api.
         */
        public static API_HOST: { url: string } = { url: '' };
    }
}