module c1.webapi {
    export interface IIPAddress {
        /**
         * IP address of the visitor's Internet connection
         */
        address: string;
    }
}