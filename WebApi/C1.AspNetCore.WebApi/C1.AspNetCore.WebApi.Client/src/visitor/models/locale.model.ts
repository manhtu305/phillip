
module c1.webapi {
    export interface ILocale {
        /**
         * Two-letter ISO-639-1 code for the browser's default/preferred language.
         */
        languageCode: string;
        /**
         * Two-letter ISO-3166 country code that the browser's default language is localized for.
         */
        countryCode: string;
    }
}