module c1.webapi {
    export interface ILatlng {
        /**
         * Latitude of the city where the IP address is located.
         */
        latitude: number;
        /**
         * Longitude of the city where the IP address is located.
         */
        longitude: number;
    }

    export interface IGeoLocation {
        /**
         * Two-letter ISO 3166 code for the visitor's country.
         */
        countryCode: string;
        /**
         * Full name of the visitor's country.
         */
        countryName: string;
        /**
         * Full name of the visitor's city or town.
         */
        cityName: string;
        /**
         * The visitor's zip/postal code.
         */
        postalCode: string;
        /**
         * Two-letter code for the visitor's region.
         */
        regionName: string;
        /**
         * The timezone of the visitor's location.
         * */
        timeZone: string;
        /**
         * Latitude of the city where the IP address is located.
         */
        latitude: number;
        /**
         * Longitude of the city where the IP address is located.
         */
        longitude: number;
    }
}
