module c1.webapi {
    export interface IDevice {
        /**
         * Family name of the mobile device used by the visitor.
         */
        name: string;
        /**
         * Version/edition of the visitor's mobile device.
         */
        version: string;
        /**
         * Screen information of the visitor's device
         */
        screen: IDeviceScreen;
        /**
        * The viewport of the visitor's device
        */
        viewport: IDeviceScreen;
        /**
        * Value represent the visitor's device is table or not
        */
        isTablet: boolean;
        /**
        * Value represent the visitor's device is mobile or not
        */
        isMobile: boolean;
    }

    export interface IDeviceScreen {
        /**
         * Screen resolution of the visitor's device.
         */
        resolution: string;
        /**
         * Screen width of the user's device in pixels.
         */
        width: number;
        /**
         * 	Screen height of the user's device in pixels.
         */
        height: number;
    }
}