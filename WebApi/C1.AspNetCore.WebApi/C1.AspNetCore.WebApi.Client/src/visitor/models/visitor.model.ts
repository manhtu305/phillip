
module c1.webapi {
    export interface IVisitor {

        /**
        * The unique id of the visitor
        */
        visitorId: string;
        /**
         * The ip address information of the visitor.
         */
        ip: IIPAddress;
        /**
         * The geolocation information of the visitor.
         */
        geo: IGeoLocation;
        /**
         * The locale using by the visitor's device.
         */
        locale: ILocale;
        /**
         * The current session information when the visitor visiting the website.
         */
        session: ICurrentSession;
        /**
         * The first session that the visitor landed to the website.
         */
        firstSession: IFirstSession;
        /**
         * The browser information of the visitor.
         */
        browser: IWebBrowser;
        /**
         * The operating system of the visitor's device.
         */
        os: IOperatingSystem;
        /**
         * The device used by the visitor when visiting the website.
         */
        device: IDevice;

        /**
       * all of the components collect from navigator and window
       */
        browserInfo?: IBrowserInfo;
    }
}