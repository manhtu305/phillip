
module c1.webapi {
    export interface IOperatingSystem {
        /**
         * 	Family name of the operating system used by the visitor.
         */
        name: string;
        /**
         * Version/edition of the visitor's operating system.
         */
        version: string;
        /**
         * platform(x64|x86) of the visitor's operating system.
         */
        platform: string;
    }
}