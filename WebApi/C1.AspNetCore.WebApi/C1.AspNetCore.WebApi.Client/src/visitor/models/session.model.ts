
module c1.webapi {
    export interface ISession {
        /**
         * Number of times the visitor has visited the site.
         */
        visits: number;
        /**
         * 	Date and time when the visitor landed on the website.
         */
        start: string;
        /**
         * Last Date and time the visitor visited the website.
         */
        last_visit: string;
        /**
         * URL of the first page the visitor landed on when arriving at the website.
         */
        url: string;
        /**
         * URL of the website that referred the visitor.
         */
        path: string;
        /**
         * The domain of the website that referred the visitor.
         */
        referrer: string;
        /**
         * The information referred the visitor.
         */
        referrer_infor: any;
        /**
         * The query of the website where visitor visiting.
         */
        search: any;
    }


    export interface IFirstSession extends ISession {

    }

    export interface ICurrentSession extends ISession {
        /**
           * Date and time of the last visited on the website.
           */
        prev_visit: string;
        /**
         * Number of milliseconds since last visit to the current session.
         */
        time_since_last_visit: string;
    }
}