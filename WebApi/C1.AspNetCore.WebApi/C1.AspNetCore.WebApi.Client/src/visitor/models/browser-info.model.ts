module c1.webapi {

    export interface IBrowserCanvasInfo {

        canvas?: any;

        webgl?: any;

        webglVendorAndRenderer?: any;

    }

    export interface IBrowserAudioInfo {

        audio?: number;

    }

    export interface ILiedInformation {

        hasLiedLanguages?: boolean;

        hasLiedResolution?: boolean;

        hasLiedOs?: boolean;

        hasLiedBrowser?: boolean;
    }

    export interface IBrowserFontInfo {

        fonts?: any;

        fontsFlash?: any;
    }

    export interface IBrowserInfo extends IBrowserCanvasInfo, IBrowserAudioInfo, IBrowserFontInfo, ILiedInformation {

        webdriver?: boolean;

        language?: string;

        colorDepth?: number;

        deviceMemory?: number;

        pixelRatio?: number;

        hardwareConcurrency?: number;

        sessionStorage?: boolean;

        localStorage?: boolean;

        indexedDb?: boolean;

        addBehavior?: boolean;

        openDatabase?: boolean;

        cpuClass?: string;

        platform?: string;

        doNotTrack?: string;

        plugins?: any;

        adBlock?: boolean;

        touchSupport?: any;

        enumerateDevices?: string;
    }
}