
module c1.webapi {
    export interface IWebBrowser {
        /**
         * Display name of the browser used by the visitor.
         */
        name: string;
        /**
         * The browser's full version number.
         */
        version: string;
        /**
         * The browser's major version number.
         */
        majorVersion: number;
    }
}