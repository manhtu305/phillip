module c1.webapi {


    /*!
     * UAParser.js v0.7.21
     * Lightweight JavaScript-based User-Agent string parser
     * https://github.com/faisalman/ua-parser-js
     *
     * Copyright © 2012-2019 Faisal Salman <f@faisalman.com>
     * Licensed under MIT License
     */



    class UserAgentConstant {

        public static LIBVERSION = '0.7.21';
        public static EMPTY = '';
        public static UNKNOWN = '?';
        public static FUNC_TYPE = 'function';
        public static UNDEF_TYPE = 'undefined';
        public static OBJ_TYPE = 'object';
        public static STR_TYPE = 'string';
        public static MAJOR = 'major';
        public static MODEL = 'model';
        public static NAME = 'name';
        public static TYPE = 'type';
        public static VENDOR = 'vendor';
        public static VERSION = 'version';
        public static ARCHITECTURE = 'architecture';
        public static CONSOLE = 'console';
        public static MOBILE = 'mobile';
        public static TABLET = 'tablet';
        public static SMARTTV = 'smarttv';
        public static WEARABLE = 'wearable';
        public static EMBEDDED = 'embedded';
    }


    class UserAgentRegex {


        private static has(str1: string | any, str2: string): boolean {
            if (typeof str1 === UserAgentConstant.STR_TYPE) {
                return str2.toLowerCase().indexOf(str1.toLowerCase()) !== -1;
            } else {
                return false;
            }
        };

        private major(version: string | any): string {
            return typeof (version) === UserAgentConstant.STR_TYPE ? version.replace(/[^\d\.]/g, '').split(".")[0] : undefined;
        }

        private static trim(str: string): string {
            return str.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
        }

        private static toLower(str: string): string {
            return str.toLowerCase();
        }

        private static deviceMaps = {

            device: {
                amazon: {
                    model: {
                        'Fire Phone': ['SD', 'KF']
                    }
                },
                sprint: {
                    model: {
                        'Evo Shift 4G': '7373KT'
                    },
                    vendor: {
                        'HTC': 'APA',
                        'Sprint': 'Sprint'
                    }
                }
            }
        };

        public search(device: any, ua: string, rexes: any) {

            let i = 0;
            let j = undefined;
            let k = undefined;
            let p = undefined;
            let q = undefined;
            let matches = undefined;
            let match = undefined;

            while (i < rexes.length && !matches) {

                let regex = rexes[i],       // even sequence (0,2,4,..)
                    props = rexes[i + 1];   // odd sequence (1,3,5,..)
                j = k = 0;

                // try matching uastring with regexes
                while (j < regex.length && !matches) {

                    matches = regex[j++].exec(ua);

                    if (!!matches) {
                        for (p = 0; p < props.length; p++) {
                            match = matches[++k];
                            q = props[p];
                            // check if given property is actually array
                            if (typeof q === UserAgentConstant.OBJ_TYPE && q.length > 0) {
                                if (q.length === 2) {
                                    if (typeof q[1] == UserAgentConstant.FUNC_TYPE) {
                                        // assign modified match
                                        device[q[0]] = q[1].call(device, match);
                                    } else {
                                        // assign given value, ignore regex match
                                        device[q[0]] = q[1];
                                    }
                                } else if (q.length === 3) {
                                    // check whether function or regex
                                    if (typeof q[1] === UserAgentConstant.FUNC_TYPE && !(q[1].exec && q[1].test)) {
                                        // call function (usually string mapper)
                                        device[q[0]] = match ? q[1].call(device, match, q[2]) : undefined;
                                    } else {
                                        // sanitize match using given regex
                                        device[q[0]] = match ? match.replace(q[1], q[2]) : undefined;
                                    }
                                } else if (q.length === 4) {
                                    device[q[0]] = match ? q[3].call(device, match.replace(q[1], q[2])) : undefined;
                                }
                            } else {
                                device[q] = match ? match : undefined;
                            }
                        }
                    }
                }
                i += 2;
            }
        };

        private static lookup(key: string, map: any): string | undefined {

            for (let i in map) {
                // check if array
                if (typeof map[i] === UserAgentConstant.OBJ_TYPE && map[i].length > 0) {
                    for (let j = 0; j < map[i].length; j++) {
                        if (UserAgentRegex.has(map[i][j], key)) {
                            return (i === UserAgentConstant.UNKNOWN) ? undefined : i;
                        }
                    }
                } else if (UserAgentRegex.has(map[i], key)) {
                    return (i === UserAgentConstant.UNKNOWN) ? undefined : i;
                }
            }
            return key;
        }

        public static regexDictionary = {
            device: [[

                /\((ipad|playbook);[\w\s\),;-]+(rim|apple)/i                        // iPad/PlayBook
            ], [UserAgentConstant.MODEL, UserAgentConstant.VENDOR, [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /applecoremedia\/[\w\.]+ \((ipad)/                                  // iPad
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Apple'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /(apple\s{0,1}tv)/i                                                 // Apple TV
            ], [[UserAgentConstant.MODEL, 'Apple TV'], [UserAgentConstant.VENDOR, 'Apple'], [UserAgentConstant.TYPE, UserAgentConstant.SMARTTV]], [

                /(archos)\s(gamepad2?)/i,                                           // Archos
                /(hp).+(touchpad)/i,                                                // HP TouchPad
                /(hp).+(tablet)/i,                                                  // HP Tablet
                /(kindle)\/([\w\.]+)/i,                                             // Kindle
                /\s(nook)[\w\s]+build\/(\w+)/i,                                     // Nook
                /(dell)\s(strea[kpr\s\d]*[\dko])/i                                  // Dell Streak
            ], [UserAgentConstant.VENDOR, UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /(kf[A-z]+)\sbuild\/.+silk\//i                                      // Kindle Fire HD
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Amazon'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [
                /(sd|kf)[0349hijorstuw]+\sbuild\/.+silk\//i                         // Fire Phone
            ], [[UserAgentConstant.MODEL, UserAgentRegex.lookup, UserAgentRegex.deviceMaps.device.amazon.model], [UserAgentConstant.VENDOR, 'Amazon'], [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [
                /android.+aft([bms])\sbuild/i                                       // Fire TV
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Amazon'], [UserAgentConstant.TYPE, UserAgentConstant.SMARTTV]], [

                /\((ip[honed|\s\w*]+);.+(apple)/i                                   // iPod/iPhone
            ], [UserAgentConstant.MODEL, UserAgentConstant.VENDOR, [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [
                /\((ip[honed|\s\w*]+);/i                                            // iPod/iPhone
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Apple'], [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /(blackberry)[\s-]?(\w+)/i,                                         // BlackBerry
                /(blackberry|benq|palm(?=\-)|sonyericsson|acer|asus|dell|meizu|motorola|polytron)[\s_-]?([\w-]*)/i,
                // BenQ/Palm/Sony-Ericsson/Acer/Asus/Dell/Meizu/Motorola/Polytron
                /(hp)\s([\w\s]+\w)/i,                                               // HP iPAQ
                /(asus)-?(\w+)/i                                                    // Asus
            ], [UserAgentConstant.VENDOR, UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [
                /\(bb10;\s(\w+)/i                                                   // BlackBerry 10
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'BlackBerry'], [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [
                // Asus Tablets
                /android.+(transfo[prime\s]{4,10}\s\w+|eeepc|slider\s\w+|nexus 7|padfone|p00c)/i
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Asus'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /(sony)\s(tablet\s[ps])\sbuild\//i,                                  // Sony
                /(sony)?(?:sgp.+)\sbuild\//i
            ], [[UserAgentConstant.VENDOR, 'Sony'], [UserAgentConstant.MODEL, 'Xperia Tablet'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [
                /android.+\s([c-g]\d{4}|so[-l]\w+)(?=\sbuild\/|\).+chrome\/(?![1-6]{0,1}\d\.))/i
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Sony'], [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /\s(ouya)\s/i,                                                      // Ouya
                /(nintendo)\s([wids3u]+)/i                                          // Nintendo
            ], [UserAgentConstant.VENDOR, UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.CONSOLE]], [

                /android.+;\s(shield)\sbuild/i                                      // Nvidia
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Nvidia'], [UserAgentConstant.TYPE, UserAgentConstant.CONSOLE]], [

                /(playstation\s[34portablevi]+)/i                                   // Playstation
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Sony'], [UserAgentConstant.TYPE, UserAgentConstant.CONSOLE]], [

                /(sprint\s(\w+))/i                                                  // Sprint Phones
            ], [[UserAgentConstant.VENDOR, UserAgentRegex.lookup, UserAgentRegex.deviceMaps.device.sprint.vendor], [UserAgentConstant.MODEL, UserAgentRegex.lookup, UserAgentRegex.deviceMaps.device.sprint.model], [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /(htc)[;_\s-]+([\w\s]+(?=\)|\sbuild)|\w+)/i,                        // HTC
                /(zte)-(\w*)/i,                                                     // ZTE
                /(alcatel|geeksphone|nexian|panasonic|(?=;\s)sony)[_\s-]?([\w-]*)/i
                // Alcatel/GeeksPhone/Nexian/Panasonic/Sony
            ], [UserAgentConstant.VENDOR, [UserAgentConstant.MODEL, /_/g, ' '], [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /(nexus\s9)/i                                                       // HTC Nexus 9
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'HTC'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /d\/huawei([\w\s-]+)[;\)]/i,
                /(nexus\s6p|vog-l29|ane-lx1|eml-l29)/i                              // Huawei
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Huawei'], [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /android.+(bah2?-a?[lw]\d{2})/i                                     // Huawei MediaPad
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Huawei'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /(microsoft);\s(lumia[\s\w]+)/i                                     // Microsoft Lumia
            ], [UserAgentConstant.VENDOR, UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /[\s\(;](xbox(?:\sone)?)[\s\);]/i                                   // Microsoft Xbox
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Microsoft'], [UserAgentConstant.TYPE, UserAgentConstant.CONSOLE]], [
                /(kin\.[onetw]{3})/i                                                // Microsoft Kin
            ], [[UserAgentConstant.MODEL, /\./g, ' '], [UserAgentConstant.VENDOR, 'Microsoft'], [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                // Motorola
                /\s(milestone|droid(?:[2-4x]|\s(?:bionic|x2|pro|razr))?:?(\s4g)?)[\w\s]+build\//i,
                /mot[\s-]?(\w*)/i,
                /(XT\d{3,4}) build\//i,
                /(nexus\s6)/i
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Motorola'], [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [
                /android.+\s(mz60\d|xoom[\s2]{0,2})\sbuild\//i
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Motorola'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /hbbtv\/\d+\.\d+\.\d+\s+\([\w\s]*;\s*(\w[^;]*);([^;]*)/i            // HbbTV devices
            ], [[UserAgentConstant.VENDOR, UserAgentRegex.trim], [UserAgentConstant.MODEL, UserAgentRegex.trim], [UserAgentConstant.TYPE, UserAgentConstant.SMARTTV]], [

                /hbbtv.+maple;(\d+)/i
            ], [[UserAgentConstant.MODEL, /^/, 'SmartTV'], [UserAgentConstant.VENDOR, 'Samsung'], [UserAgentConstant.TYPE, UserAgentConstant.SMARTTV]], [

                /\(dtv[\);].+(aquos)/i                                              // Sharp
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Sharp'], [UserAgentConstant.TYPE, UserAgentConstant.SMARTTV]], [

                /android.+((sch-i[89]0\d|shw-m380s|gt-p\d{4}|gt-n\d+|sgh-t8[56]9|nexus 10))/i,
                /((SM-T\w+))/i
            ], [[UserAgentConstant.VENDOR, 'Samsung'], UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [                  // Samsung
                /smart-tv.+(samsung)/i
            ], [UserAgentConstant.VENDOR, [UserAgentConstant.TYPE, UserAgentConstant.SMARTTV], UserAgentConstant.MODEL], [
                /((s[cgp]h-\w+|gt-\w+|galaxy\snexus|sm-\w[\w\d]+))/i,
                /(sam[sung]*)[\s-]*(\w+-?[\w-]*)/i,
                /sec-((sgh\w+))/i
            ], [[UserAgentConstant.VENDOR, 'Samsung'], UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /sie-(\w*)/i                                                        // Siemens
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Siemens'], [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /(maemo|nokia).*(n900|lumia\s\d+)/i,                                // Nokia
                /(nokia)[\s_-]?([\w-]*)/i
            ], [[UserAgentConstant.VENDOR, 'Nokia'], UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /android[x\d\.\s;]+\s([ab][1-7]\-?[0178a]\d\d?)/i                   // Acer
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Acer'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+([vl]k\-?\d{3})\s+build/i                                 // LG Tablet
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'LG'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [
                /android\s3\.[\s\w;-]{10}(lg?)-([06cv9]{3,4})/i                     // LG Tablet
            ], [[UserAgentConstant.VENDOR, 'LG'], UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [
                /(lg) netcast\.tv/i                                                 // LG SmartTV
            ], [UserAgentConstant.VENDOR, UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.SMARTTV]], [
                /(nexus\s[45])/i,                                                   // LG
                /lg[e;\s\/-]+(\w*)/i,
                /android.+lg(\-?[\d\w]+)\s+build/i
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'LG'], [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /(lenovo)\s?(s(?:5000|6000)(?:[\w-]+)|tab(?:[\s\w]+))/i             // Lenovo tablets
            ], [UserAgentConstant.VENDOR, UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [
                /android.+(ideatab[a-z0-9\-\s]+)/i                                  // Lenovo
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Lenovo'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [
                /(lenovo)[_\s-]?([\w-]+)/i
            ], [UserAgentConstant.VENDOR, UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /linux;.+((jolla));/i                                               // Jolla
            ], [UserAgentConstant.VENDOR, UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /((pebble))app\/[\d\.]+\s/i                                         // Pebble
            ], [UserAgentConstant.VENDOR, UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.WEARABLE]], [

                /android.+;\s(oppo)\s?([\w\s]+)\sbuild/i                            // OPPO
            ], [UserAgentConstant.VENDOR, UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /crkey/i                                                            // Google Chromecast
            ], [[UserAgentConstant.MODEL, 'Chromecast'], [UserAgentConstant.VENDOR, 'Google'], [UserAgentConstant.TYPE, UserAgentConstant.SMARTTV]], [

                /android.+;\s(glass)\s\d/i                                          // Google Glass
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Google'], [UserAgentConstant.TYPE, UserAgentConstant.WEARABLE]], [

                /android.+;\s(pixel c)[\s)]/i                                       // Google Pixel C
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Google'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+;\s(pixel( [23])?( xl)?)[\s)]/i                              // Google Pixel
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Google'], [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /android.+;\s(\w+)\s+build\/hm\1/i,                                 // Xiaomi Hongmi 'numeric' models
                /android.+(hm[\s\-_]*note?[\s_]*(?:\d\w)?)\s+build/i,               // Xiaomi Hongmi
                /android.+(mi[\s\-_]*(?:a\d|one|one[\s_]plus|note lte)?[\s_]*(?:\d?\w?)[\s_]*(?:plus)?)\s+build/i,
                // Xiaomi Mi
                /android.+(redmi[\s\-_]*(?:note)?(?:[\s_]*[\w\s]+))\s+build/i       // Redmi Phones
            ], [[UserAgentConstant.MODEL, /_/g, ' '], [UserAgentConstant.VENDOR, 'Xiaomi'], [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [
                /android.+(mi[\s\-_]*(?:pad)(?:[\s_]*[\w\s]+))\s+build/i            // Mi Pad tablets
            ], [[UserAgentConstant.MODEL, /_/g, ' '], [UserAgentConstant.VENDOR, 'Xiaomi'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [
                /android.+;\s(m[1-5]\snote)\sbuild/i                                // Meizu
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Meizu'], [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [
                /(mz)-([\w-]{2,})/i
            ], [[UserAgentConstant.VENDOR, 'Meizu'], UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /android.+a000(1)\s+build/i,                                        // OnePlus
                /android.+oneplus\s(a\d{4})[\s)]/i
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'OnePlus'], [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /android.+[;\/]\s*(RCT[\d\w]+)\s+build/i                            // RCA Tablets
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'RCA'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+[;\/\s]+(Venue[\d\s]{2,7})\s+build/i                      // Dell Venue Tablets
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Dell'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+[;\/]\s*(Q[T|M][\d\w]+)\s+build/i                         // Verizon Tablet
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Verizon'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+[;\/]\s+(Barnes[&\s]+Noble\s+|BN[RT])(V?.*)\s+build/i     // Barnes & Noble Tablet
            ], [[UserAgentConstant.VENDOR, 'Barnes & Noble'], UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+[;\/]\s+(TM\d{3}.*\b)\s+build/i                           // Barnes & Noble Tablet
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'NuVision'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+;\s(k88)\sbuild/i                                         // ZTE K Series Tablet
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'ZTE'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+[;\/]\s*(gen\d{3})\s+build.*49h/i                         // Swiss GEN Mobile
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Swiss'], [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /android.+[;\/]\s*(zur\d{3})\s+build/i                              // Swiss ZUR Tablet
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Swiss'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+[;\/]\s*((Zeki)?TB.*\b)\s+build/i                         // Zeki Tablets
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Zeki'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /(android).+[;\/]\s+([YR]\d{2})\s+build/i,
                /android.+[;\/]\s+(Dragon[\-\s]+Touch\s+|DT)(\w{5})\sbuild/i        // Dragon Touch Tablet
            ], [[UserAgentConstant.VENDOR, 'Dragon Touch'], UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+[;\/]\s*(NS-?\w{0,9})\sbuild/i                            // Insignia Tablets
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Insignia'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+[;\/]\s*((NX|Next)-?\w{0,9})\s+build/i                    // NextBook Tablets
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'NextBook'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+[;\/]\s*(Xtreme\_)?(V(1[045]|2[015]|30|40|60|7[05]|90))\s+build/i
            ], [[UserAgentConstant.VENDOR, 'Voice'], UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [                    // Voice Xtreme Phones

                /android.+[;\/]\s*(LVTEL\-)?(V1[12])\s+build/i                     // LvTel Phones
            ], [[UserAgentConstant.VENDOR, 'LvTel'], UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [

                /android.+;\s(PH-1)\s/i
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Essential'], [UserAgentConstant.TYPE, UserAgentConstant.MOBILE]], [                // Essential PH-1

                /android.+[;\/]\s*(V(100MD|700NA|7011|917G).*\b)\s+build/i          // Envizen Tablets
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Envizen'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+[;\/]\s*(Le[\s\-]+Pan)[\s\-]+(\w{1,9})\s+build/i          // Le Pan Tablets
            ], [UserAgentConstant.VENDOR, UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+[;\/]\s*(Trio[\s\-]*.*)\s+build/i                         // MachSpeed Tablets
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'MachSpeed'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+[;\/]\s*(Trinity)[\-\s]*(T\d{3})\s+build/i                // Trinity Tablets
            ], [UserAgentConstant.VENDOR, UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+[;\/]\s*TU_(1491)\s+build/i                               // Rotor Tablets
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Rotor'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+(KS(.+))\s+build/i                                        // Amazon Kindle Tablets
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Amazon'], [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /android.+(Gigaset)[\s\-]+(Q\w{1,9})\s+build/i                      // Gigaset Tablets
            ], [UserAgentConstant.VENDOR, UserAgentConstant.MODEL, [UserAgentConstant.TYPE, UserAgentConstant.TABLET]], [

                /\s(tablet|tab)[;\/]/i,                                             // Unidentifiable Tablet
                /\s(mobile)(?:[;\/]|\ssafari)/i                                     // Unidentifiable Mobile
            ], [[UserAgentConstant.TYPE, UserAgentRegex.toLower], UserAgentConstant.VENDOR, UserAgentConstant.MODEL], [

                /[\s\/\(](smart-?tv)[;\)]/i                                         // SmartTV
            ], [[UserAgentConstant.TYPE, UserAgentConstant.SMARTTV]], [

                /(android[\w\.\s\-]{0,9});.+build/i                                 // Generic Android Device
            ], [UserAgentConstant.MODEL, [UserAgentConstant.VENDOR, 'Generic']]
            ]
        };


    }

    export interface ActualDevice {
        vendor: string;
        model: string;
        type: string;
    }

    export class UserAgentParser {

        private _ua: string;

        constructor(ua: string) {
            this._ua = ua;
        }

        /**
        * Get current device information         
        */
        public getDevice(): ActualDevice {

            const regex = new UserAgentRegex();

            let device: ActualDevice = { vendor: undefined, model: undefined, type: undefined };

            regex.search(device, this._ua, UserAgentRegex.regexDictionary.device);

            return device;
        };

    }
}