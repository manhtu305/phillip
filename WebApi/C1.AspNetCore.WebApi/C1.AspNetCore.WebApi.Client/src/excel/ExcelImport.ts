/// <reference path="FlexGridLoader.ts" />

// TODO: complete this.
module wijmo.grid.excel {
    "use strict";

    export interface IGcWorkbook {
        activeSheetIndex: number;
        defaultThemeVersion: number;
        namedStyles: IGcCellStyle[];
        names: string[];
        sheetCount: number;
        sheets: Record<string, IGcWorksheet>;
        tabStripRatio: number;
        version: string;

    }

    export interface IGcCellStyle {
        backColor: string;
        font: string;
        foreColor: string;
        hAlign: GcHorizontalAlignment;
        textDecoration: GcTextDercorationType;
        locked: boolean;
        name: string;
        shrinkToFit: boolean;
        textIndent: number;
        vAlign: GcVerticalAlignment;
        wordWrap: boolean;
        formatter: string;
        borderTop: IGcCellBorderStyle;
        borderRight: IGcCellBorderStyle;
        borderBottom: IGcCellBorderStyle;
        borderLeft: IGcCellBorderStyle;
    }

    export interface IGcRange {
        row: number;
        col: number;
        rowCount: number;
        colCount: number;
    }
    export interface IGcColumnSetting {
        size: number;
    }

    export interface IGcRowSetting {
        size: number;
    }

    export interface IGcRowOutlines {
        direction: number;
        itemsData: IGcRowOutline[];
    }

    export interface IGcRowOutline {
        count: number;
        index: number;
        info: IGcRowOutlineInfo;
    }

    export interface IGcRowOutlineInfo {
        collapsed: boolean;
        level: number;
    }
    export interface IGcWorksheet {
        name: string;
        index: number;
        rowCount: number;
        columnCount: number;
        allowCellOverflow: boolean;
        defaults: IGcCellSetting;
        activeRow: number;
        activeCol: number;
        selections: {};
        names: [],
        data: IGcWorksheetData;
        validations: [];
        namedStyles: IGcCellStyle[],
        spans: IGcRange[];
        columns: IGcColumnSetting[];
        rows: IGcRowSetting[];
        rowOutlines: IGcRowOutlines;
        sparklineGroups: [];
        theme: IGcWorksheetTheme;
        showRowOutline: boolean;
        showColumnOutline: boolean;
        frozenRowCount: number;
        frozenColCount: number;
        zoomFactor: number;
        floatingObject: [];
        charts: [];
        printInfo: IGcPrintInfo
    }

    export interface IGcPrintInfo {}

    export interface IGcWorksheetTheme {}

    export interface IGcCellRichText {
        text: string;
        style?: {
            font: string,
            foreColor: string,
            vertAlign: number
        };
    }

    export interface IGcWorksheetData {
        defaultDataNode: {
            style: IGcCellStyle;
        }
        dataTable: Record<string, Record<string, IGcCell>>;
        columnDataArray: [];
        rowDataArray: [];
    }

    export interface IGcCell {
        formula: string;
        value: any;
        style: string;
    }

    export interface IGcCellSetting {
        colHeaderRowHeight: number;
        rowHeaderColWidth: number;
        rowHeight: number;
        colWidth: number
    }

    export interface IGcCellBorderStyle {
        color: string;
        style: GcBorderStyle;
    }
    export enum GcTextDercorationType {
        None = 0,
        Underline = 1,
        LineThrough = 2,
        Overline = 3,
        // This enum was not defined by SpreadJS but return by GcExcel
        UnderlineDouble = 8
    }

    export enum GcBorderStyle {
        //
        // Summary:
        //     Specifies no border.
        None = 0,
        //
        // Summary:
        //     Specifies a hairline border.
        Hair = 1,
        //
        // Summary:
        //     Specifies a dash dot dot border.
        DashDotDot = 2,
        //
        // Summary:
        //     Specifies a dash dot border.
        DashDot = 3,
        //
        // Summary:
        //     Specifies a dotted border.
        Dotted = 4,
        //
        // Summary:
        //     Specifies a dashed border.
        Dashed = 5,
        //
        // Summary:
        //     Specifies a thin border.
        Thin = 6,
        //
        // Summary:
        //     Specifies a medium dash dot dot border.
        MediumDashDotDot = 7,
        //
        // Summary:
        //     Specifies a slant dash dot border.
        SlantDashDot = 8,
        //
        // Summary:
        //     Specifies a medium dash dot border.
        MediumDashDot = 9,
        //
        // Summary:
        //     Specifies a medium dashed.
        MediumDashed = 10,
        //
        // Summary:
        //     Specifies a medium border.
        Medium = 11,
        //
        // Summary:
        //     Specifies a thick line border.
        Thick = 12,
        //
        // Summary:
        //     Specifies a double line.
        Double = 13
    }

    export enum GcHorizontalAlignment {

        //
        // Summary:
        //     Spedifies left justification.
        Left = 0,
        //
        // Summary:
        //     Spedifies that the text should be centered.
        Center = 1,
        //
        // Summary:
        //     Spedifies right justification.
        Right = 2,
        //
        // Summary:
        //     Specifies that the text will be align by default.
        General = 3,
    }

    export enum GcVerticalAlignment {
        //
        // Summary:
        //     Specifies that text is placed at the top.
        Top = 0,
        //
        // Summary:
        //     Specifies that text is vertically centered.
        Center = 1,
        //
        // Summary:
        //     Specifies that text is placed at the bottom.
        Bottom = 2,
        //
        // Summary:
        //     Specifies that rotated text is vertically justified.
        Justify = 3,
        //
        // Summary:
        //     Specifies that rotated text should wrap and be aligned to form straight edges
        //     on the left and right, including the last line.
        Distributed = 4
    }
}


module wijmo.grid {
    "use strict";

    function IsFlexSheet(grid: any): boolean {
        return "sheets" in grid;
    }


    /*
       * The importer for FlexGrid/FlexSheet.
       */
    export class ExcelImporter {

        /*
         * Request the server to import.
         * 
         * @param control The control to import.
         * @param serviceUrl The service address.
         * @param file The file to import.
         * @param includeColumnHeaders Whether to treat the first row as header.
         */
        requestImport(control: FlexGrid, serviceUrl: string, file: File, includeColumnHeaders: boolean = false) {
            var self = this,
                postData = new FormData(),
                flexSheet: any,
                workbook;
            postData.append("file", file);
            AjaxHelper.post(serviceUrl, data => {                
                self._ImportXlsxToFlexGrid(control, data, (IsFlexSheet(control) ? false : includeColumnHeaders));
            }, postData, function () {
                var xhr = <XMLHttpRequest>this;
                var error = new Error(xhr.responseText || "Unknown error occurs.");
                error.name = xhr.statusText || error.name;
                throw error;
            });
        }

        private _ImportXlsxToFlexGrid(control: FlexGrid, data: any, includeColumnHeaders: boolean = false, includeCellStyles: boolean = true) {
            wijmo.grid.excel.ExcelToFlexGridLoader.Load(control, data, {
                includeCellStyles: includeCellStyles,
                includeColumnHeaders: includeColumnHeaders
            });
        }
    }
}