/// <reference path="../../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.xlsx.d.ts" />

module wijmo.grid.excel {
    /**
     * Custom merge manager for FlexGrid.
     */
    export class FlexGridMergeManager extends wijmo.grid.MergeManager {
        mergedRanges: IGcRange[] = [];
        rowHeaderCount: number = 0;
        constructor(flexGrid, mergedRanges: IGcRange[], numberRowHeader: number) {
            super(flexGrid);
            this.mergedRanges = mergedRanges;
            this.rowHeaderCount = numberRowHeader;
        }
        getMergedRange(panel, r, c, clip = true) {
            if (panel && panel.cellType != 1) {
                return;
            }
            r += this.rowHeaderCount;
            for (let range of this.mergedRanges) {
                if (r >= range.row && r < (range.row + range.rowCount) && c >= range.col && c < (range.col + range.colCount)) {
                    return new wijmo.grid.CellRange(range.row - this.rowHeaderCount, range.col, range.row + range.rowCount - 1 - this.rowHeaderCount, range.col + range.colCount - 1);
                }
            }
            return null;
        }
    }

}