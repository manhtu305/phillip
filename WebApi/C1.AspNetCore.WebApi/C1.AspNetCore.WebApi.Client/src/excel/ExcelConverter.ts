/// <reference path="../../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.xlsx.d.ts" />
/// <reference path="ExcelImport.ts" />

module wijmo.grid.excel {

  export class ExcelStyleConverter {

    /** Convert size unit from Excel to FlexGrid, xcel has default size is 11.5pt while Flexgrid is 15px */
    public static PointToPixel(point: number, round: boolean = false): number {
      //if (round) {
      //    return Math.floor(point * 15 / 11.5);
      //}
      //return point * 15 / 11.5;

      // For now, we don't keep aspect ratio between excel and grid, so it is temporary ignore this ratio
      return point;
    }

    /**
     * Parse ODate string to Javascript date object.
     * 
     * @param OADateString String represented Date cell type
     */
    public static OADateStringToJSDate(OADateString: string): Date {
      if (!OADateString || OADateString.indexOf("OADate") == -1) return null;
      return new Date((parseInt(OADateString.replace("/OADate(", "").replace(")/", "")) - (25567 + 2)) * 86400 * 1000);
    }


    /**
     * Convert from excel fromat to wijmo format
     * 
     * @param value Excel value object
     * @param excelFormat Excel format
     */
    public static ExcelFormatToWijmoFormat(value: any, excelFormat: string): string {
      if (value === undefined || value === null || isNaN(value)) {
        return undefined;
      }
      let format = "";
      if (wijmo.isDate(value)) {
        format = wijmo.xlsx.Workbook.fromXlsxFormat(excelFormat)[0];
      } else {
        if (wijmo.isNumber(value) && (!excelFormat || excelFormat === 'General')) {
          format = wijmo.isInt(value) ? 'd' : 'f2';
        } else if (wijmo.isNumber(value) || value === '') {
          format = wijmo.xlsx.Workbook.fromXlsxFormat(excelFormat)[0];
        } else {
          format = excelFormat;
        }
      }

      return format;
    }

    /**
     * Convert excel LineStyle to Css Borderstyle
     * @param lineStyle From SSJSON schema, empty:0,thin:1,medium:2,dashed:3,dotted:4,thick:5,double:6,hair:7,mediumDashed:8,dashDot:9,mediumDashDot:10,dashDotDot:11,mediumDashDotDot:12,slantedDashDot:13. 
     */
    public static ExcelLineStyleToCssBorderStyle(lineStyle: number) {
      let val = '';
      switch (lineStyle) {
        case 0:
          val = "none";
          break;
        case 1:
        case 2:
        case 5:
          val = "thin"
          break;
        case 3:
          val = "dashed"
          break;
        case 4:
          val = "dotted"
          break;
        case 6:
          val = "double"
          break;
        default:
          val = "thin";
      }
      return val;
    }


    private static _GetValueDataType(value: any): DataType {
      if (value == null
        || isNaN(value)) {
        return null;
      }

      return getType(value);
    }

    public static ExcelValueToHTML(cellStyle: IGcCellStyle, cellValue: any) {
      if (cellValue && cellStyle && cellStyle.formatter) {
        return Globalize.format(cellValue, cellStyle.formatter);
      }
      return cellValue;
    }


    /**
     * Convert excel cell style to CSS
     * 
     * @param cellStyle Excel cell style
     * @param cellValue Excel cell value
     * @param cssStyle Style of the HTML element represented for this cell on the Grid control.
     */
    public static ExcelStyleToCssStyle(cellStyle: IGcCellStyle, cellValue: any, cssStyle?: CSSStyleDeclaration) {
      if (!cellStyle) return null;

      if (!cssStyle) {
        let fakeContainer = document.createElement("div");
        cssStyle = fakeContainer.style;
      }

      // Parse font
      cssStyle.font = '';
      if (cellStyle.font) {
        cssStyle.font = cellStyle.font;
      }

      cssStyle.color = '';
      // Parse fore color
      if (cellStyle.foreColor) {
        cssStyle.color = cellStyle.foreColor;
      }

      cssStyle.background = '';
      // Parse background color
      if (cellStyle.backColor) {
        cssStyle.background = cellStyle.backColor;
      }

      //#region BorderParser
      // // Border style is not working properly with FlexGrid
      // if (cellStyle.borderBottom && cellStyle.borderBottom.color) {
      //     cssStyle.borderBottomColor = cellStyle.borderBottom.color;
      //     cssStyle.borderBottomStyle = GcUtils._ExcelLineStyleToCssBorderStyle(cellStyle.borderBottom.style);
      // }
      // if (cellStyle.borderTop && cellStyle.borderTop.color) {
      //     cssStyle.borderTopColor = cellStyle.borderTop.color;
      //     cssStyle.borderTopStyle = GcUtils._ExcelLineStyleToCssBorderStyle(cellStyle.borderTop.style);
      // }
      // if (cellStyle.borderLeft && cellStyle.borderLeft.color) {
      //     cssStyle.borderLeftColor = cellStyle.borderLeft.color;
      //     cssStyle.borderLeftStyle = GcUtils._ExcelLineStyleToCssBorderStyle(cellStyle.borderLeft.style);
      // }
      // if (cellStyle.borderRight && cellStyle.borderRight.color) {
      //     cssStyle.borderRightColor = cellStyle.borderRight.color;
      //     cssStyle.borderRightStyle = GcUtils._ExcelLineStyleToCssBorderStyle(cellStyle.borderRight.style);
      // }
      //#endregion

      // Parse horizontal align
      cssStyle.display = "flex";
      switch (cellStyle.hAlign) {
        case GcHorizontalAlignment.Left:
          cssStyle.justifyContent = "flex-start";
          break;
        case GcHorizontalAlignment.Center:
          cssStyle.justifyContent = "center";
          break;
        case GcHorizontalAlignment.Right:
          cssStyle.justifyContent = "flex-end";
          break;
        case GcHorizontalAlignment.General:
          if (cellValue != null && !isNaN(cellValue)) {
            switch (this._GetValueDataType(cellValue)) {
              case DataType.Date:
                cssStyle.justifyContent = "flex-end";
                break;
              case DataType.Number:
                cssStyle.justifyContent = "flex-end";
                break;
              case DataType.Boolean:
                cssStyle.justifyContent = "center";
                break;
              default:
                if (cellStyle.formatter && cellStyle.formatter.search(/[n,c,p]/i) === 0) {
                  cssStyle.justifyContent = "flex-end";
                } else {
                  cssStyle.justifyContent = "flex-start";
                }
                break;
            }
          }
          break;
        default:
          cssStyle.justifyContent = "";
          break;

      }

      switch (cellStyle.vAlign) {
        case GcVerticalAlignment.Top:
          cssStyle.alignItems = "flex-start";
          break;
        case GcVerticalAlignment.Center:
          cssStyle.alignItems = "center";
          break;
        case GcVerticalAlignment.Bottom:
          cssStyle.alignItems = "flex-end";
          break;
        default:
          cssStyle.alignItems = "center";
          break;
      }

      // Parse word wrap
      cssStyle.whiteSpace = "";
      if (cellStyle.wordWrap) {
        cssStyle.whiteSpace = "normal";
      }

      return cssStyle;
    }
  }

}