﻿/// <reference path="../../../../../Shared/WijmoMVC/dist/controls/wijmo.d.ts" />
/// <reference path="../../../../../Shared/WijmoMVC/dist/controls/wijmo.xlsx.d.ts" />
/// <reference path="../../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.d.ts" />
/// <reference path="../../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.detail.d.ts" />
/// <reference path="../../../../../Shared/WijmoMVC/dist/controls/wijmo.input.d.ts" />
/// <reference path="../../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.xlsx.d.ts" />
/// <reference path="../../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.multirow.d.ts" />
/// <reference path="../../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.transposed.d.ts" />
/// <reference path="../../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.filter.d.ts" />
/// <reference path="../../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.sheet.d.ts" />
/*
 * ComponentOne ASP.NET Web API
 * ExcelExport.js
 * 
 */

/*
 * Control-independent Excel export code.
 */
module wijmo {
    "use strict";

    /*
     * Excel export options.
     */
    export class ExcelExportSettings implements IExportSettings {
        private _type: ExportFileType;
        private _fileName: string;
        private _workbook: any;
        private _includeColumnHeaders: boolean;
        private _onlyCurrentPage: boolean;

        get type(): ExportFileType {
            return this._type;
        }
        set type(value: ExportFileType) {
            this._type = asEnum(value, ExportFileType, false);
        }

        get fileName(): string {
            return this._fileName;
        }
        set fileName(value: string) {
            this._fileName = asString(value, false);
        }

        get workbook(): wijmo.xlsx.IWorkbook {
            return this._workbook;
        }
        set workbook(value: wijmo.xlsx.IWorkbook) {
            this._workbook = value;
        }

        get includeColumnHeaders(): boolean {
            return this._includeColumnHeaders;
        }

        set includeColumnHeaders(value: boolean) {
            this._includeColumnHeaders = wijmo.asBoolean(value, false);
        }

        get onlyCurrentPage(): boolean {
            return this._onlyCurrentPage;
        }

        set onlyCurrentPage(value: boolean) {
            this._onlyCurrentPage = wijmo.asBoolean(value, false);
        }
    }

    /*
     * Control-independent Excel exporter.
     */
    export class ExcelExporter {

        /*
         * Request the server to export.
         * 
         * @param commonObj The common format to export.
         * @param serviceUrl The service address.
         * @param options The export settings.
         */
        requestExport(commonObj: wijmo.xlsx.IWorkbook, serviceUrl: string, options: ExcelExportSettings): void {
            var settings: ExcelExportSettings = new ExcelExportSettings(),
                postData: any;

            // verify url is string
            serviceUrl = asString(serviceUrl, false);

            // Excel common format data
            settings.workbook = commonObj;
            this._processWorkbook(settings.workbook);

            // copy user settings
            wijmo.copy(settings, options);

            // make sure to use getSerializableObject to avoid underscores
            postData = getSerializableObject(settings);

            // send request
            wijmo.postFormRequest(serviceUrl, postData);
        }

        _processWorkbook(workbook: wijmo.xlsx.IWorkbook) {
            workbook.sheets && workbook.sheets.forEach(sheet => {
                sheet.columns && sheet.columns.forEach(col => {
                    col.width && (col.width = col.width.toFixed(0));
                });
                sheet.rows && sheet.rows.forEach(row => {
                    row.cells && row.cells.forEach(cell => {
                        if (!cell) {
                            return;
                        }

                        if (cell.colSpan === 1) {
                            delete cell.colSpan;
                        }
                        if (cell.rowSpan === 1) {
                            delete cell.rowSpan;
                        }
                        if (wijmo.isDate(cell.value)) {
                            cell.value = wijmo.Globalize.formatDate(cell.value, "yyyy-MM-dd HH:mm:ss").replace(" ", "T");
                            cell.isDate = true;
                        }
                    });
                });
            });
        }
    }
}

/*
 * FlexGrid/FlexSheet-related Excel export code.
 */
module wijmo.grid {
    "use strict";

    function isFlexSheet(grid: any): boolean {
        return "sheets" in grid;
    }

    /*
     * Excel exporter for FlexGrid/FlexSheet.
     */
    export class ExcelExporter extends wijmo.ExcelExporter {

        /*
         * Request the server to export.
         * 
         * @param control The control to export.
         * @param serviceUrl The service address.
         * @param options The export settings.
         */
        requestExport(control: any, serviceUrl: string, options: ExcelExportSettings): void {
            var cv = control.collectionView,
                workbook: any,
                pageSize = cv && cv["pageSize"];

            if (!(control instanceof FlexGrid)) { // FlexGrid or FlexSheet
                return;
            }

            if (pageSize && !options.onlyCurrentPage) {
                control.collectionView["pageSize"] = 0;
            }

            // Excel data
            if (isFlexSheet(control)) {
                workbook = (<wijmo.grid.sheet.FlexSheet>control).saveToWorkbookOM();
            } else {
                workbook = xlsx.FlexGridXlsxConverter.save(control, {
                    includeColumnHeaders: !!(options.includeColumnHeaders || options["includeColumnHeader"]),
                    includeCellStyles: true
                })._serialize();
            }


            if (pageSize && !options.onlyCurrentPage) {
                control.collectionView["pageSize"] = pageSize;
            }

            super.requestExport(workbook, serviceUrl, options);
        }
    }
}

// TODO: complete this.
module wijmo.grid.excel {
    "use strict";

    export interface IWorkbook {
    }

    export interface IWorksheet {
    }
}

/*
 * MVC specific code.
 */
module c1.mvc.grid {
    "use strict";

    /*
     * Excel exporter for MVC FlexGrid.
     */
    export class ExcelExporter extends wijmo.grid.ExcelExporter {

        /*
         * Request the server to export.
         * 
         * @param control The control to export.
         * @param serviceUrl The service address.
         * @param options The export settings.
         * @param setMaskGrid A function which is used to set the properties of the mask grid for export.
         */
        requestExport(control: wijmo.grid.FlexGrid, serviceUrl: string, options: wijmo.ExcelExportSettings, setMaskGrid?: Function): void {
            var self = this, container: HTMLElement,
                grid: wijmo.grid.FlexGrid, ele: HTMLDivElement,
                cv: wijmo.collections.ICollectionView = control.collectionView,
                superExport = super.requestExport;

            if (cv && cv["getAllData"] && cv["_disableServerRead"] === false &&
                ((!options["onlyCurrentPage"] && cv["pageSize"]) || (cv["_isDynamicalLoadingEnabled"] && cv["_isDynamicalLoadingEnabled"]()))) {
                cv["getAllData"](function (res) {
                    ele = document.createElement("div");
                    ele.style.display = "none";
                    container = control.hostElement.parentElement;
                    container.appendChild(ele);
                    grid = new wijmo.grid.FlexGrid(ele);
                    grid.autoGenerateColumns = false;
                    grid.itemsSource = res.items;
                    self._setFlexGrid(control, grid);
                    if (setMaskGrid) setMaskGrid(grid);
                    delete options["onlyCurrentPage"];
                    superExport.call(self, grid, serviceUrl, options);
                    container.removeChild(ele);
                });
            } else {
                superExport.call(self, control, serviceUrl, options);
            }
        }

        _setFlexGrid(originalGrid: wijmo.grid.FlexGrid, desGrid: wijmo.grid.FlexGrid) {
            var index, col, oriColumns = originalGrid.columns, oriGroupDescriptions = originalGrid.collectionView.groupDescriptions;
            for (index = 0; index < oriColumns.length; index++) {
                col = new wijmo.grid.Column();
                this.copyColumn(col, oriColumns[index]);
                desGrid.columns.push(col);
            }
            for (index = 0; index < oriGroupDescriptions.length; index++) {
                desGrid.collectionView.groupDescriptions.push(oriGroupDescriptions[index]);
            }
        }

        copyColumn(desCol: wijmo.grid.Column, oriCol: wijmo.grid.Column) {
            var key, pd;
            for (key in oriCol) {
                if (wijmo._isPropertyDeprecated(key)) {
                    continue;
                }

                //Copy the actual size instead of automatic size
                if (key == 'width' && wijmo.grid.Column._parseStarSize(oriCol.width) != null) {
                    desCol.width = oriCol.size;
                    continue;
                }

                pd = Object.getOwnPropertyDescriptor(Object.getPrototypeOf(oriCol), key);

                var proto = (<any>oriCol).__proto__;
                while (!pd && proto && !(proto.isPrototypeOf(Object))) {
                    pd = Object.getOwnPropertyDescriptor(Object.getPrototypeOf(proto), key);

                    if (pd) {
                        continue;
                    }

                    proto = proto.__proto__;
                }

                if (pd && pd.set && pd.get) {
                    desCol[key] = oriCol[key];
                }
            }
        }
    }
}