/// <reference path="../../../../../Shared/WijmoMVC/dist/controls/wijmo.grid.xlsx.d.ts" />
/// <reference path="ExcelImport.ts" />
/// <reference path="ExcelConverter.ts" />

module wijmo.grid.excel {

    export class ExcelToFlexGridLoader {
        /**
         * Import Excel file to FlexGrid/FlexSheet control
         * 
         * @param grid wijmo FlexGrid or FlexSheet control
         * @param workbook GcExcel JSONData which following SSJSON Schema
         * @param options import setting
         */
        public static Load(grid: FlexGrid, workbook: IGcWorkbook, options: wijmo.grid.xlsx.IFlexGridXlsxOptions) {
            
            options = options || {};

            // Reset grid data.
            grid.itemsSource = null;
            grid.columns.clear();
            grid.columnHeaders.rows.clear();
            grid.columnHeaders.columns.clear();
            grid.rows.clear();
            grid.frozenColumns = 0;
            grid.frozenRows = 0;
            grid.itemFormatter = null;
            grid.mergeManager = null;


            // Find ExcelSheet parse to FlexGrid
            let currentSheet: IGcWorksheet;
            let sheetIndex = options.sheetIndex != null && !isNaN(options.sheetIndex) ? options.sheetIndex : 0;
            if (sheetIndex < 0 || sheetIndex >= workbook.sheetCount) {
                throw 'The sheet index option is out of the sheet range of current workbook.';
            }
            if (options.sheetName != null && workbook.sheets[options.sheetName]) {
                currentSheet = workbook.sheets[options.sheetName];
            }
            currentSheet = currentSheet || workbook.sheets[Object.keys(workbook.sheets)[sheetIndex]];
            if (currentSheet.rowCount == 0 || currentSheet.columnCount == 0) return;

            // Convert excel format to wijmo format
            if (currentSheet && currentSheet.namedStyles) {
                for (let style of currentSheet.namedStyles) {
                    if (style && style.formatter) {
                        style.formatter = wijmo.xlsx.Workbook.fromXlsxFormat(style.formatter)[0];
                    }
                }
            }

            //#region FlexGrid setting from Excel Sheet setting
            grid.rows.defaultSize = ExcelStyleConverter.PointToPixel(currentSheet.defaults.rowHeight, true);
            grid.columns.defaultSize = ExcelStyleConverter.PointToPixel(currentSheet.defaults.colWidth, true);
           
            let includeColumnHeaders: boolean = (options.includeColumnHeaders != null ? options.includeColumnHeaders : true) && currentSheet.rows.length > 0,
                numberRowsHeader: number = includeColumnHeaders ? 1 : 0,
                columnSettings = currentSheet.columns,
                rowSettings = currentSheet.rows,
                rowCount: number = currentSheet.rowCount,
                columnCount: number = currentSheet.columnCount;


            // FrozenRows & FrozenColumns
            if (isNumber(currentSheet.frozenColCount) && !isNaN(currentSheet.frozenColCount)) {
                grid.frozenColumns = currentSheet.frozenColCount;
            }
            if (isNumber(currentSheet.frozenRowCount) && !isNaN(currentSheet.frozenRowCount)) {
                grid.frozenRows = includeColumnHeaders && currentSheet.frozenRowCount > 0 ? currentSheet.frozenRowCount - 1 : currentSheet.frozenRowCount;
            }
            //#endregion

            for (let c = 0; c < columnCount; c++) {
                grid.columns.push(new Column());

                let colWidth = -1;
                if (!!columnSettings[c]) {
                    if (!isNaN(+columnSettings[c].size)) {
                        colWidth = ExcelStyleConverter.PointToPixel(+columnSettings[c].size, true);
                    }
                }
                if (colWidth == -1) {
                    colWidth = grid.columns.defaultSize;
                }
                if (colWidth != -1) {
                    grid.columns[c].size = ExcelStyleConverter.PointToPixel(colWidth, true);
                }
            }
            var sheetData = currentSheet.data.dataTable;

            // Add column header
            for (let r = 0; r < numberRowsHeader; r++) {
                grid.columnHeaders.rows.push(new Row());
                for (let c = 0; c < currentSheet.columnCount; c++) {
                    let cellData = ExcelToFlexGridLoader._GetCellData(sheetData, r, c);
                    if (cellData) {
                        grid.columnHeaders.setCellData(r, c, ExcelToFlexGridLoader._ParseCellValue(cellData));
                    }
                }
            }

            // Column header always has at least 1 row even if includeColumnHeaders = false;
            if (!includeColumnHeaders) {
                grid.columnHeaders.rows.push(new Row());
            }

            for (let r = numberRowsHeader; r < rowCount; r++) {
                let groupRowInfo: IGcRowOutlineInfo = null;
                if (currentSheet.rowOutlines && currentSheet.rowOutlines.itemsData) {
                    for (let rowOutline of currentSheet.rowOutlines.itemsData) {
                        if (rowOutline && rowOutline.index == r + 1) {
                            groupRowInfo = rowOutline.info;
                            break;
                        }
                    }
                }

                if (groupRowInfo) {
                    let groupRow = new GroupRow();
                   // groupRow.isCollapsed = groupRowInfo.collapsed == null ? false : groupRowInfo.collapsed;
                    groupRow.level = 1 + (isNaN(groupRowInfo.level) ? 0 : groupRowInfo.level);
                    grid.rows.push(groupRow);
                } else {
                    let row = new Row();
                    grid.rows.push(row);
                }

                let rowHeight = -1;
                if (!!rowSettings[r]) {
                    if (!isNaN(+rowSettings[r].size)) {
                        rowHeight = ExcelStyleConverter.PointToPixel(+rowSettings[r].size, true);
                    }
                }
                if (rowHeight == -1) {
                    rowHeight = grid.rows.defaultSize;
                }
                if (rowHeight != -1) {
                    grid.rows[r - numberRowsHeader].size = ExcelStyleConverter.PointToPixel(rowHeight, true);
                }

                for (let c = 0; c < currentSheet.columnCount; c++) {
                    let cellData = ExcelToFlexGridLoader._GetCellData(sheetData, r, c);
                    let cellStyle = ExcelToFlexGridLoader._GetCellStyle(currentSheet, cellData);
                    if (cellData) {
                        grid.setCellData(r - numberRowsHeader, c, ExcelToFlexGridLoader._ParseCellValue(cellData));
                    }
                }
            }

            grid.itemFormatter = (panel, r, c, cell) => {
                this.FormatCell(currentSheet, sheetData, numberRowsHeader, panel, r, c, cell);
            };

            // Parse Merge range
            if (currentSheet && currentSheet.spans.length > 0) {
                grid.allowMerging = wijmo.grid.AllowMerging.Cells;
                grid.mergeManager = new FlexGridMergeManager(grid, currentSheet.spans, numberRowsHeader);
            }
        }

        /**
         * Format excel cell to GridCell with CSS style support.
         *
         * @param sheet 
         * @param sheetData 
         * @param numberRowsHeader number of row in the header
         * @param panel The GridPanel contains this cell
         * @param r Row index
         * @param c Column index 
         * @param cell The HTML Element represented for this cell
         */
        public static FormatCell(sheet : IGcWorksheet, sheetData, numberRowsHeader : number, panel : GridPanel, r : number, c : number, cell : any) {
            if (panel && panel.cellType != 1) { return; }
            let cellData = ExcelToFlexGridLoader._GetCellData(sheetData, r + numberRowsHeader, c);
            let cellInPanelValue = panel.getCellData(r, c, false);
            let cellStyle = ExcelToFlexGridLoader._GetCellStyle(sheet, cellData);
            if (!cellStyle) cellStyle = ExcelToFlexGridLoader._GetDefaultCellStyle(sheet);

            ExcelStyleConverter.ExcelStyleToCssStyle(cellStyle, cellInPanelValue, cell.style);
            let htmlContent = ExcelStyleConverter.ExcelValueToHTML(cellStyle, cellInPanelValue);
            if (htmlContent) {
                cell.innerHTML = htmlContent;
            }
        }
        

        private static _GetDefaultCellStyle(gcWorksheet: IGcWorksheet): IGcCellStyle {
            if (gcWorksheet && gcWorksheet.data && gcWorksheet.data.defaultDataNode && gcWorksheet.data.defaultDataNode.style) {
                return gcWorksheet.data.defaultDataNode.style;
            }
            return null;
        }

        private static _GetCellStyle(gcWorksheet: IGcWorksheet, cell: IGcCell): IGcCellStyle {
            if (!gcWorksheet || !cell) return;
            if (cell.style) {
                for (let cellStyle of gcWorksheet.namedStyles) {
                    if (cellStyle.name == cell.style) {
                        return cellStyle;
                    }
                }
            }
            return null;
        }

        private static _GetCellData(data: Record<string, Record<string, IGcCell>>, rowIndex: number, columnIndex: number): IGcCell {
            let r = rowIndex.toString(), c = columnIndex.toString();
            if (data && data.hasOwnProperty(r) && data[r] && data[r].hasOwnProperty(c)) {
                return data[r][c];
            }
            return null;
        }

        private static _ParseCellValue(cell: IGcCell) {
            if (cell.formula && cell.value && isObject(cell.value)) {
                if (cell.value.hasOwnProperty("_error")) {
                    return cell.value._error;
                }
            }
            else if (cell.value && isObject(cell.value) && cell.value.hasOwnProperty("richText")) {
                let txts = <IGcCellRichText[]>cell.value.richText;
                if (txts) {
                    let val = "";
                    for (let richText of txts) {
                        let style = "";
                        if (richText.style) {
                            if (richText.style["font"]) {
                                style += ` font : ${richText.style["font"]};`;
                            }
                            if (richText.style["foreColor"]) {
                                style += ` color : ${richText.style["foreColor"]};`;
                            }
                        }
                        let span = `<span style="${style}">${richText.text}</span>`
                        val += span;
                    }
                    return val;
                }
            }
            else if (cell.value && wijmo.isString(cell.value) && cell.value.indexOf("OADate") != -1) {
                let date = ExcelStyleConverter.OADateStringToJSDate(cell.value);
                if (date) cell.value = date;                
            }
            return cell.value;
        }
    }
}