﻿/// <reference path="../../../../../Shared/WijmoMVC/dist/controls/wijmo.d.ts" />

/*
 * ComponentOne ASP.NET Web API
 * Base.js
 * 
 */

/* 
 *  serialization methods 
 */
module wijmo {
    "use strict";

    export function _isPropertyDeprecated(propertyName: string): boolean {
        return propertyName === 'disabled' || propertyName === 'required';
    }

    /* 
     * returns all serializable properties for a given Control.
     * serializable is defined as having a getter, setter, and doesn"t
     * begin with an underscore character.
     */
    function getSerializableProperties(obj: Object): string[] {
        var arr: string[] = [],
            names: string[], name: string,
            i: number, len: number,
            pd: PropertyDescriptor;

        for (; obj !== null; obj = Object.getPrototypeOf(obj)) {
            names = Object.getOwnPropertyNames(obj);
            len = names.length;

            for (i = 0; i < len; i++) {
                name = names[i].trim();
                if (name[0] === "_" || _isPropertyDeprecated(name)) { continue; }

                pd = Object.getOwnPropertyDescriptor(obj, name);
                if (pd && pd.set && pd.get) {
                    arr.push(name);
                }
            }
        }

        return arr;
    }

    /*
     * returns a representation of a given Control that can be serialized to JSON.
     */
    export function getSerializableObject(obj: Object, include?: string[], ignore?: string[]): any {
        // ensure that an array or null was passed
        include = (asArray(include) || getSerializableProperties(obj));
        ignore = asArray(ignore);

        var retval: Object = {},
            i: number,
            len = include.length,
            prop: string;

        for (i = 0; i < len; i++) {
            prop = include[i];

            // avoid underscore - private
            if (prop[0] === "_") { continue; }

            // #119933 "false" or "null" can be meaningful
            if (!(prop in obj)) { continue; }

            // ignore certain properties
            if (ignore && !isUndefined(ignore) && ignore.some((value: string): boolean => prop === value)) { continue; }

            retval[prop] = obj[prop];
        }

        return retval;
    }

    var _requestIframe: HTMLIFrameElement;
    /*
     * post request by form
     */
    export function postFormRequest(url: string, data: any, setting: any = null) {
        if (_requestIframe) {
            _clearIframe(_requestIframe);
            _requestIframe.parentNode.removeChild(_requestIframe);
            _requestIframe = null;
        }

        _requestIframe = document.createElement('iframe');
        _requestIframe.style.display = 'none';
        _requestIframe.src = 'about:blank';
        _requestIframe.onload = ev => {
            _updateFrameContent(_requestIframe, url, data, setting);
        };
        document.body.appendChild(_requestIframe);

    }

    function _getiframeDocument(iframe): Document {
        var iframeDoc = iframe.contentWindow || iframe.contentDocument;
        if (iframeDoc.document) {
            iframeDoc = iframeDoc.document;
        }
        return <Document>iframeDoc;
    }

    function _updateFrameContent(iframe: HTMLIFrameElement, url: string, data: any, setting: any = null) {
        var content = _htmlSpecialCharsEntityEncode(wijmo.serialize(data));
        var formInnerHtml = '<input type="hidden" name="data" value="' + content + '" />';
        content = _htmlSpecialCharsEntityEncode(wijmo.serialize(setting));
        if (setting !== null)
            formInnerHtml += '<input type="hidden" name="setting" value="' + content + '" />';
        var formDoc = _getiframeDocument(iframe);
        var form: HTMLFormElement;

        formDoc.write("<html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8'></head>"
            + "<body><form method='POST' accept-charset='utf-8' action='" + url + "'>" + formInnerHtml
            + "</form></body></html>");
        form = <HTMLFormElement>(formDoc.querySelector("form"));
        form.submit();
        setTimeout(() => {
            _clearIframe(iframe);
        }, 0);
    }

    function _clearIframe(iframe: HTMLIFrameElement) {
        iframe.onload = null;
        iframe.innerHTML = '';
        //iframe.src = 'about:blank';
    }

    var _htmlSpecialCharsRegEx = /[<>&\r\n"']/gm;
    var _htmlSpecialCharsPlaceHolders = {
        '<': 'lt;',
        '>': 'gt;',
        '&': 'amp;',
        '\r': "#13;",
        '\n': "#10;",
        '"': 'quot;',
        "'": 'apos;' /*single quotes just to be safe*/
    };

    function _htmlSpecialCharsEntityEncode(str) {
        return str.replace(_htmlSpecialCharsRegEx, function (match) {
            return '&' + _htmlSpecialCharsPlaceHolders[match];
        });
    }

    /*
     * compare an object with a standard object and remove default properties.
     */
    function removeDefaultProperties(obj: Object, std: Object) {
        if (!std) {
            return;
        }
        for (var prop in obj) {
            if (isObject(obj[prop]) && std[prop]) {
                removeDefaultProperties(obj[prop], std[prop]);
            } else {
                if (obj[prop] === std[prop]) {
                    delete obj[prop];
                }
            }
        }
    }

    // private method - probably not necessary
    // #119933 "false" or "null" can be meaningful
    // Although getSerializableProperties() deals with property names beginning with "_", it is not recursive into children so there are still cases missed that lead to circular reference error. So add defensive measures here.
    var _serializeReplacer = (key: string, value: any): any => key[0] === "_" ? undefined : value;

    // static method
    // returns JSON representation of object
    export var serialize = (obj: Object, std?: Object): string => {
        if (std) {
            removeDefaultProperties(obj, std);
        }
        return JSON.stringify(obj, _serializeReplacer);
    }
}

/*
 * models for image export 
 */
module wijmo {
    "use strict";

    /*
     * Enum that represents the type of files that can be exported.
     */
    export enum ExportFileType {
        Xls,
        Xlsx,
        Csv,
        Pdf,
        Png,
        Jpg,
        Bmp,
        Gif,
        Tiff
    }

    /*
     * Interface representing common export settings for all controls.
     */
    export interface IExportSettings {
        /*
         * The export file format
         */
        type: ExportFileType;
        /*
         * The export file name without extension
         */
        fileName: string;
    }
}