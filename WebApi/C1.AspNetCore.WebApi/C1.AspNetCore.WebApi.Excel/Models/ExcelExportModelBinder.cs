﻿using C1.Web.Api;
using C1.Web.Api.Excel;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;

namespace C1.AspNetCore.Api.Excel
{
  public class ExcelExportModelBinder : BodyModelBinder<ExcelExportSource>
  {
    private const string SettingField = "setting";
    private const string DataField = "data";

    public override async Task BindModelAsync(ModelBindingContext bindingContext)
    {
      var request = bindingContext.HttpContext.Request;
      var form = await request.ReadFormAsync();
      var value = string.Empty;
      if (form != null)
      {
        if (form[DataField] != StringValues.Empty)
          value = form[DataField].FirstOrDefault();
      }
      ExcelExportSource model = JsonConvert.DeserializeObject<ExcelExportSource>(value);
      GcWorkbook wb = new GcWorkbook();
      wb.FromJSON(value);
      model.Workbook = wb.GetWorkbook();
      bindingContext.Result = ((model != null) ? ModelBindingResult.Success(model) :
          ModelBindingResult.Failed());
    }
  }
}
