﻿using GrapeCity.Documents.Excel;
using System;

namespace C1.AspNetCore.Api.Excel.ExcelIO
{
  public class ExcelUtils
  {
    /// <summary>
    /// Get Excel Save File Format from fileType string
    /// </summary>
    /// <param name="fileType">The file extension.</param>
    /// <returns>A FileOpenFormat object.</returns>        
    public static SaveFileFormat ConvertStringToExcelSaveFileFormat(string fileType)
    {
      if (string.Compare(fileType.Trim('.'), "csv", StringComparison.OrdinalIgnoreCase) == 0) return SaveFileFormat.Csv;
      else if (string.Compare(fileType.Trim('.'), "pdf", StringComparison.OrdinalIgnoreCase) == 0) return SaveFileFormat.Pdf;
      else if (string.Compare(fileType.Trim('.'), "xlsm", StringComparison.OrdinalIgnoreCase) == 0) return SaveFileFormat.Xlsm;
      return SaveFileFormat.Xlsx;
    }

    /// <summary>
    /// Get Excel Open File Format from fileType string
    /// </summary>
    /// <param name="fileType">The file extension.</param>
    /// <returns>A FileOpenFormat object.</returns>        
    public static OpenFileFormat ConvertStringToExcelOpenFileFormat(string fileType)
    {
      if (string.Compare(fileType.Trim('.'), "csv", StringComparison.OrdinalIgnoreCase) == 0) return OpenFileFormat.Csv;
      else if (string.Compare(fileType.Trim('.'), "xlsm", StringComparison.OrdinalIgnoreCase) == 0) return OpenFileFormat.Xlsm;
      return OpenFileFormat.Xlsx;
    }

    public static bool IsValidFileType(string fileType)
    {
      if (string.Compare(fileType.Trim('.'), "csv", StringComparison.OrdinalIgnoreCase) == 0) return true;
      if (string.Compare(fileType.Trim('.'), "pdf", StringComparison.OrdinalIgnoreCase) == 0) return true;
      if (string.Compare(fileType.Trim('.'), "xlsx", StringComparison.OrdinalIgnoreCase) == 0) return true;
      if (string.Compare(fileType.Trim('.'), "xlsm", StringComparison.OrdinalIgnoreCase) == 0) return true;
      return false;
    }

  }
}
