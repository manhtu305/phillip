﻿using System;
using System.Collections.Generic;
using System.IO;
using C1.AspNetCore.Api.Excel.ExcelIO;
using GrapeCity.Documents.Excel;

namespace C1.Web.Api.Excel
{
  /// <summary>
  /// Implementation of IExcelHost using C1.Excel.
  /// </summary>
  public class GcExcelHost : IExcelHost
  {

    /// <summary>
    /// Read data from Excel file stream.
    /// </summary>
    /// <param name="file">The file.</param>
    /// <param name="fileType">The file extension.</param>
    /// <returns>A Workbook object.</returns>        
    IWorkbook IExcelHost.Read(Stream file, string fileType)
    {
      var workbook = new Workbook();
      if (file != null)
      {
        if (ExcelUtils.IsValidFileType(fileType))
        {
          workbook.Open(file, ExcelUtils.ConvertStringToExcelOpenFileFormat(fileType));
        }
        else
        {
          workbook.Open(file);
        }
      }
      return workbook;
    }

    /// <summary>
    /// Write data to Excel file stream.
    /// </summary>
    /// <param name="workbook">A Workbook object.</param>
    /// <param name="file">The file.</param>
    /// <param name="fileType">The file extension.</param>
    void IExcelHost.Write(IWorkbook workbook, Stream file, string fileType)
    {
      if (file != null)
      {
        if (ExcelUtils.IsValidFileType(fileType))
        {
          workbook.Save(file, ExcelUtils.ConvertStringToExcelSaveFileFormat(fileType));
        }
        else
        {
          workbook.Save(file);
        }
      }
    }
    /// <summary>
    /// New empty workbook
    /// </summary>
    /// <returns>A Workbook object.</returns>
    IWorkbook IExcelHost.NewWorkbook()
    {
      return new Workbook();
    }
  }
}
