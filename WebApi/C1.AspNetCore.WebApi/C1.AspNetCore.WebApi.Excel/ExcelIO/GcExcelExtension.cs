﻿using C1.Web.Api.Localization;
using GrapeCity.Documents.Excel;
using System;
using System.Collections.Generic;
using System.Text;

namespace C1.Web.Api.Excel
{
  internal static class GcExcelExtension
  {
    /// <summary>
    /// Write evaluation info to the workbook.
    /// Map with implementation in \c1.aspnetcore.api\src\c1.aspnetcore.api.excel\utils.cs
    /// </summary>
    /// <param name="workbook"></param>
    public static void WriteEvaluationInfo(this IWorkbook workbook)
    {
      var evaluationMessage = Resources.EvaluationMessage;
      foreach (var c1Sheet in workbook.Worksheets)
      {
        var count = 0;
        if(c1Sheet.GetUsedRange() != null) count = c1Sheet.GetUsedRange().RowCount;
        c1Sheet.Range[count, 0].Value = evaluationMessage ;
        c1Sheet.Range[count, 0].HorizontalAlignment = HorizontalAlignment.Left;
      }
    }
  }
}
