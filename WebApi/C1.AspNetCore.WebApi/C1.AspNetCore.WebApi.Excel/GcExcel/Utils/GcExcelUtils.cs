using GrapeCity.Documents.Excel;
using System;
using System.ComponentModel;
using System.Drawing;

namespace C1.AspNetCore.Api.Excel
{
  internal class GcExcelUtils
    {
        /**
        * Convert html color to .Net color, htmlColor can be rgb, rgba, hex
         */
        public static Color ColorFromHtml(string htmlColor)
        {
            if (string.IsNullOrEmpty(htmlColor)) return Color.Empty;

            if (htmlColor.TrimStart().StartsWith("rgba(", StringComparison.OrdinalIgnoreCase))
            {
                var startIndex = htmlColor.IndexOf('(');
                var endIndex = htmlColor.IndexOf(')');
                if (endIndex == -1) return Color.Empty;

                var valuesString = htmlColor.Substring(startIndex + 1, endIndex - startIndex - 1);
                var values = valuesString.Split(',');
                var r = Convert.ToInt32(values[0]);
                var g = Convert.ToInt32(values[1]);
                var b = Convert.ToInt32(values[2]);
                var a = (int)Math.Round(Convert.ToDouble(values[3]) * 255);
                return Color.FromArgb(a, r, g, b);
            }
            else if (htmlColor.TrimStart().StartsWith("rgb(", StringComparison.OrdinalIgnoreCase))
            {
                var startIndex = htmlColor.IndexOf('(');
                var endIndex = htmlColor.IndexOf(')');
                if (endIndex == -1) return Color.Empty;

                var valuesString = htmlColor.Substring(startIndex + 1, endIndex - startIndex - 1);
                var values = valuesString.Split(',');
                var r = Convert.ToInt32(values[0]);
                var g = Convert.ToInt32(values[1]);
                var b = Convert.ToInt32(values[2]);
                return Color.FromArgb(255, r, g, b);
            }


            Color c = Color.Empty;

            // empty color
            if ((htmlColor == null) || (htmlColor.Length == 0))
                return c;

            // #RRGGBB or #RGB
            if ((htmlColor[0] == '#') &&
                ((htmlColor.Length == 7) || (htmlColor.Length == 4)))
            {

                if (htmlColor.Length == 7)
                {
                    c = Color.FromArgb(Convert.ToInt32(htmlColor.Substring(1, 2), 16),
                                       Convert.ToInt32(htmlColor.Substring(3, 2), 16),
                                       Convert.ToInt32(htmlColor.Substring(5, 2), 16));
                }
                else
                {
                    string r = Char.ToString(htmlColor[1]);
                    string g = Char.ToString(htmlColor[2]);
                    string b = Char.ToString(htmlColor[3]);

                    c = Color.FromArgb(Convert.ToInt32(r + r, 16),
                                       Convert.ToInt32(g + g, 16),
                                       Convert.ToInt32(b + b, 16));
                }
            }

            // special case. Html requires LightGrey, but .NET uses LightGray
            if (c.IsEmpty && String.Equals(htmlColor, "LightGrey", StringComparison.OrdinalIgnoreCase))
            {
                c = Color.LightGray;
            }

            // resort to type converter which will handle named colors
            if (c.IsEmpty)
            {
                c = (Color)TypeDescriptor.GetConverter(typeof(Color)).ConvertFromString(htmlColor);
            }

            return c;
        }

        /**
        * Convert Wijmo FlexGrid border style to excel border style 
        */
        public static BorderLineStyle ConvertFlexGridBorderStyleToExcelBorderStyle(FlexGridBorderStyle flexGridBorderStyle)
        {
            BorderLineStyle style = BorderLineStyle.Thin;

            switch (flexGridBorderStyle)
            {
                case FlexGridBorderStyle.Dashed:
                    style = BorderLineStyle.Dashed;
                    break;
                case FlexGridBorderStyle.Dotted:
                    style = BorderLineStyle.Dotted;
                    break;
                case FlexGridBorderStyle.Double:
                    style = BorderLineStyle.Double;
                    break;
                case FlexGridBorderStyle.Hair:
                    style = BorderLineStyle.Hair;
                    break;
                case FlexGridBorderStyle.Medium:
                    style = BorderLineStyle.Medium;
                    break;
                case FlexGridBorderStyle.MediumDashDotDotted:
                    style = BorderLineStyle.MediumDashDot;
                    break;
                case FlexGridBorderStyle.MediumDashDotted:
                    style = BorderLineStyle.MediumDashDotDot;
                    break;
                case FlexGridBorderStyle.MediumDashed:
                    style = BorderLineStyle.MediumDashed;
                    break;
                case FlexGridBorderStyle.None:
                    style = BorderLineStyle.None;
                    break;
                case FlexGridBorderStyle.SlantedMediumDashDotted:
                    style = BorderLineStyle.SlantDashDot;
                    break;
                case FlexGridBorderStyle.Thick:
                    style = BorderLineStyle.Thick;
                    break;
                case FlexGridBorderStyle.Thin:
                    style = BorderLineStyle.Thin;
                    break;
                case FlexGridBorderStyle.ThinDashDotDotted:
                    style = BorderLineStyle.DashDotDot;
                    break;
                case FlexGridBorderStyle.ThinDashDotted:
                    style = BorderLineStyle.DashDot;
                    break;                
            }
            return style;
        }

    }
}