using System;
using GrapeCity.Documents.Excel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace C1.AspNetCore.Api.Excel
{

  internal class GcWorkbook
  {
    private Workbook _Workbook = null;
    private WorkbookModel _WorkbookModel = null;
    private WorkbookExportSetting _WorkbookExportSetting = null;

    public GcWorkbook()
    {
      _Workbook = new Workbook();
    }
    private Workbook Workbook
    {
      set
      {
        this._Workbook = value;
      }
      get
      {
        return this._Workbook;
      }
    }

    private WorkbookModel Model
    {
      set
      {
        this._WorkbookModel = value;
      }
      get
      {
        return this._WorkbookModel;
      }
    }

    private WorkbookExportSetting ExportSetting
    {
      set
      {
        this._WorkbookExportSetting = value;
      }
      get
      {
        return this._WorkbookExportSetting;
      }
    }
    /**Convert JSON string to workbook
    * @param jsonStr: FlexGrid xlsx data exported from wijmo.FlexGrid.XlsxConverter
     */
    public void FromJSON(string jsonStr)
    {
      JObject json = JObject.Parse(jsonStr);

      if (json == null) return;

      this.ExportSetting = JsonConvert.DeserializeObject<WorkbookExportSetting>(jsonStr);
      this.Model = JsonConvert.DeserializeObject<WorkbookModel>(json.GetValue("workbook").ToString());

      for (int sheetIndex = 0; sheetIndex < Model.sheets.Length; sheetIndex++)
      {
        WorksheetModel worksheetModel = Model.sheets[sheetIndex];
        IWorksheet worksheet = (Workbook.Worksheets.Count == 1) ? Workbook.Worksheets[0] : Workbook.Worksheets.Add();

        // Worksheet settings
        worksheet.Name = String.Format("Sheet{0}", _Workbook.Worksheets.Count);
        worksheet.Outline.SummaryRow = SummaryRow.Above;
        if (!ExportSetting.noFreezing && worksheetModel.frozenPane != null)
        {
          worksheet.FreezePanes(worksheetModel.frozenPane.rows, worksheetModel.frozenPane.columns);
        }

        for (int rowIndex = 0; rowIndex < worksheetModel.rows.Length; rowIndex++)
        {
          var rowModel = worksheetModel.rows[rowIndex];

          // Parse row settings
          worksheet.Rows[rowIndex].OutlineLevel = rowModel.groupLevel + 1;
          if(ExportSetting.visibleOnly) worksheet.Rows[rowIndex].Hidden = !rowModel.visible;

          // Parse row cells
          int colIndex = rowModel.cells.Length;
          while (colIndex-- > 0)
          {
            _ParseCell(worksheet, worksheetModel, rowIndex, colIndex);
          }
        }

        for (int colIndex = 0; colIndex < worksheetModel.columns.Length; colIndex++)
        {
          if (ExportSetting.visibleOnly) worksheet.Columns[colIndex].Hidden = !worksheetModel.columns[colIndex].visible;
        }
      }
    }
    private void _ParseCell(IWorksheet worksheet, WorksheetModel worksheetModel, int rowIndex, int colIndex)
    {
      RowModel rowModel = worksheetModel.rows[rowIndex];
      ColumnModel colModel = worksheetModel.columns[colIndex];
      CellModel cellModel = rowModel.cells[colIndex];
      IRange cell = worksheet.Range[rowIndex, colIndex];

      // Note: cell format date need to set after cell's value was set.
      cell.RowHeightInPixel = rowModel.height;
      cell.ColumnWidthInPixel = colModel.width;


      cell.HorizontalAlignment = (HorizontalAlignment)cellModel.style.hAlign;
      // Note: In Excel, date cell with format as General, horizontal alignment will be right as default, but in FlexGrid it was left. So we need to take care of this case.
      if (ExportSetting.asDisplayed && cellModel.isDate && cell.HorizontalAlignment == HorizontalAlignment.General)
      {
        cell.HorizontalAlignment = HorizontalAlignment.Left;
      }
      
      // Note: FlexGrid does not had vertical alignment property. By default, the data will display center of the cell.
      cell.VerticalAlignment = ExportSetting.asDisplayed ? VerticalAlignment.Center : VerticalAlignment.Top;
      cell.Value = cellModel.isDate ? DateTime.Parse(cellModel.value.ToString()) : cellModel.value;
      cell.NumberFormat = cellModel.style.format;

      _ParseCellStyle(cell, cellModel.style);

      if (cellModel.colSpan > 1)
      {
        worksheet.Range[rowIndex, colIndex, 1, cellModel.colSpan].Merge();
      }
    }
    private void _ParseCellStyle(IRange cell, CellStyleModel style)
    {
      _ParseFontStyle(cell.Font, style.font);
      _ParseFillStyle(cell, style.fill);
      _ParseBordersStyle(cell, style.borders);
    }
    private void _ParseFontStyle(IFont font, FontModel model)
    {
      if (font == null || model == null) return;
      if (model.bold) font.Bold = true;
      if (model.italic) font.Italic = true;
      if (model.underline) font.Underline = UnderlineType.Single;
      var color = GcExcelUtils.ColorFromHtml(model.color);
      if (color != null)
      {
        font.Color = color;
      }
      // Note: Default font size in excel is 11.5px, and the flexgrid is 15px
      font.Size = model.size * 11.5 / 15;
      if (model.family != null && model.family.Length > 0)
      {
        font.Name = model.family;
      }
    }
    private void _ParseFillStyle(IRange cell, FillModel model)
    {
      if (cell == null || model == null) return;
      var color = GcExcelUtils.ColorFromHtml(model.color);
      if (color != null) cell.Interior.Color = color;
    }
    private void _ParseBordersStyle(IRange cell, BordersModel model)
    {
      if (cell == null || model == null) return;

      if (model.left != null)
      {
        var color = _ParseBorderColor(model.left.color);
        if (color != null) cell.Borders[BordersIndex.EdgeLeft].Color = color;
        cell.Borders[BordersIndex.EdgeLeft].LineStyle = GcExcelUtils.ConvertFlexGridBorderStyleToExcelBorderStyle((FlexGridBorderStyle)model.left.style);
      }

      if (model.top != null)
      {
        var color = _ParseBorderColor(model.top.color);
        if (color != null) cell.Borders[BordersIndex.EdgeTop].Color = color;
        cell.Borders[BordersIndex.EdgeTop].LineStyle = GcExcelUtils.ConvertFlexGridBorderStyleToExcelBorderStyle((FlexGridBorderStyle)model.top.style);
      }

      if (model.right != null)
      {
        var color = _ParseBorderColor(model.right.color);
        if (color != null) cell.Borders[BordersIndex.EdgeRight].Color = color;
        cell.Borders[BordersIndex.EdgeRight].LineStyle = GcExcelUtils.ConvertFlexGridBorderStyleToExcelBorderStyle((FlexGridBorderStyle)model.right.style);
      }

      if (model.bottom != null)
      {
        var color = _ParseBorderColor(model.bottom.color);
        if (color != null) cell.Borders[BordersIndex.EdgeBottom].Color = color;
        cell.Borders[BordersIndex.EdgeBottom].LineStyle = GcExcelUtils.ConvertFlexGridBorderStyleToExcelBorderStyle((FlexGridBorderStyle)model.bottom.style);
      }
    }

    // Note: Border Color does not support alpha, so if the given color has transparency we convert it to default color
    private System.Drawing.Color _ParseBorderColor(string color)
    {
      if (color.StartsWith("rgba"))
      {
        var values = color.Replace(")", "").Split(',');
        if (values.Length == 4 && Convert.ToDouble(values[3]) < 1)
        {
          color = "#CCCCCC";
        }
      }
      return GcExcelUtils.ColorFromHtml(color);
    }

    /**
    *  Return the workbook
    */
    public Workbook GetWorkbook()
    {
      return _Workbook;
    }

  }


}