using Newtonsoft.Json;
using System.ComponentModel;

namespace C1.AspNetCore.Api.Excel
{
  internal class BorderModel
  {
    public string color { get; set; }
    public int style { get; set; }
  }

  internal class BordersModel
  {
    public BorderModel right { get; set; }

    public BorderModel bottom { get; set; }

    public BorderModel left { get; set; }

    public BorderModel top { get; set; }
  }

  internal class FillModel
  {
    public string color { get; set; }
  }
  internal class FontModel
  {

    public bool bold { get; set; }

    public bool italic { get; set; }

    public bool underline { get; set; }

    public string color { get; set; }

    public string family { get; set; }

    public double size { get; set; }
  }
  internal class CellStyleModel
  {

    public FillModel fill { get; set; }

    public FontModel font { get; set; }

    public BordersModel borders { get; set; }

    public string format { get; set; }

    public int hAlign { get; set; }

    public int indent { get; set; }
  }
  internal class ColumnModel
  {

    public bool autoWidth { get; set; }

    public bool visible { get; set; }

    public int width { get; set; }

    public CellStyleModel style { get; set; }
  }

  internal class CellModel
  {

    public CellStyleModel style { get; set; }

    public object value { get; set; }

    public string formula { get; set; }

    public bool isDate { get; set; }

    public int colSpan { get; set; }

    public int rowSpan { get; set; }
  }
  internal class RowModel
  {

    public CellModel[] cells { get; set; }

    public int groupLevel { get; set; }

    public int height { get; set; }

    public bool visible { get; set; }
  }
  internal class FrozenPaneModel
  {

    public int rows { get; set; }

    public int columns { get; set; }
  }
  internal class WorksheetModel
  {

    public bool visible { get; set; }

    public ColumnModel[] columns { get; set; }

    public RowModel[] rows { get; set; }

    public FrozenPaneModel frozenPane { get; set; }
  }

  internal class WorkbookModel
  {
    public WorksheetModel[] sheets { get; set; }
  }

  internal class WorkbookExportSetting
  {

    /**Setting file type for export. Default value is 1 - xlsx. @see{ExportFileType} for more information. */
    [DefaultValue(1)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
    public int type { get; set; }

    /**Filename to be exported*/
    public string fileName { get; set; }

    /**Setting column headers of FlexGrid should be export or not*/
    public bool includeColumnHeaders { get; set; }

    /**Determine whether export only current page or all page in FlexGrid*/
    public bool onlyCurrentPage { get; set; }
    
    /**Setting freezing row/column when export FlexGrid. Default value was false.*/
    [DefaultValue(false)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
    public bool noFreezing { get; set; }

    /**Setting Whether FlexGrid need to be exported as displayed or not. Default value was true.*/
    [DefaultValue(true)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
    public bool asDisplayed { get; set; }

    /**Setting Whether FlexGrid need to be export visible cell/row/column or not. Default value was true.*/
    [DefaultValue(true)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate)]
    public bool visibleOnly { get; set; }
  }

  /**
  * Wijmo FlexGrid predefined border style
  */
  internal enum FlexGridBorderStyle
  {
    /** No border */
    None = 0,
    /** Thin border */
    Thin = 1,
    /** Medium border */
    Medium = 2,
    /** Dashed border */
    Dashed = 3,
    /** Dotted border */
    Dotted = 4,
    /** Thick line border */
    Thick = 5,
    /** Double line border */
    Double = 6,
    /** Hair line border */
    Hair = 7,
    /** Medium dashed border */
    MediumDashed = 8,
    /** Thin dash dotted border */
    ThinDashDotted = 9,
    /** Medium dash dotted border */
    MediumDashDotted = 10,
    /** Thin dash dot dotted border */
    ThinDashDotDotted = 11,
    /** Medium dash dot dotted border */
    MediumDashDotDotted = 12,
    /** Slanted medium dash dotted border */
    SlantedMediumDashDotted = 13
  }
}