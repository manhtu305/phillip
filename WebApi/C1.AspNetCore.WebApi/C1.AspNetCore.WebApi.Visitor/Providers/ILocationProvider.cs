﻿using C1.Web.Api.Visitor.Models;

namespace C1.Web.Api.Visitor.Providers
{
  /// <summary>
  /// A provider help to detect the visitor location.
  /// </summary>
  public interface ILocationProvider
  {
    /// <summary>
    /// Getting Geographical location based on an IP Address
    /// </summary>
    /// <param name="ipAddress">A string represent an IP Address</param>
    /// <returns><see cref="GeoLocation"/></returns>
    GeoLocation LocationFromIPAddress(string ipAddress);

    /// <summary>
    /// Getting Geographical location based on latitude and longitude point.
    /// </summary>
    /// <param name="latitude">latitude location</param>
    /// <param name="longitude">longitude location</param>
    /// <returns><see cref="GeoLocation"/></returns>
    GeoLocation LocationFromLatlng(double latitude, double longitude);

    /// <summary>
    /// Get location provider type
    /// </summary>
    /// <returns></returns>
    LocationProviderType Type();

  }

  /// <summary>
  /// Location Provider Type
  /// </summary>
  public enum LocationProviderType
  {
    /// <summary>
    /// Use Visitor API without location provider
    /// </summary>
    None = 0,
    /// <summary>
    /// Use Visitor API with IP2Location Database
    /// </summary>
    Ip2Location = 1,
    /// <summary>
    /// Use Visitor API with Google Map API
    /// </summary>
    GoogleMapAPI = 2,
    /// <summary>
    /// Use Visitor API with Custom Location Provider
    /// </summary>
    Custom = 3
  }
}
