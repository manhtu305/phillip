﻿using C1.Web.Api.Visitor.Models;
using C1.Web.Api.Visitor.Utils;
using System;
using System.Linq;

namespace C1.Web.Api.Visitor.Providers
{
  /// <summary>
  /// A location provider help to detect the Visitor Location based on IP Address.
  /// </summary>
  public class Ip2LocationProvider : ILocationProvider
  {
    private readonly LocationContext dbContext;
    /// <summary>
    /// Default constructor.
    /// </summary>
    /// <param name="_db">A <see cref="LocationContext"/> dependency using with EFCore </param>
    public Ip2LocationProvider(LocationContext _db)
    {
      this.dbContext = _db;
    }

    /// <summary>
    /// Getting geographical location from an IP Address. It will automatically detect the given IP Address is IPv4 or IPv6. If the given IP is in localhost, there's no location can be found.
    /// </summary>
    /// <param name="ipAddress">a <see cref="long"/> number represent an IP Address</param>
    /// <returns>return a <see cref="GeoLocation"/> object</returns>
    public GeoLocation LocationFromIPAddress(string ipAddress)
    {
      if (IpAddressUtils.IsPrivate(ipAddress))
      {
        return null;
      }
      else if (IpAddressUtils.IsIpAddressV4(ipAddress))
      {
        return this.LocationFromIPV4Number(IpAddressUtils.IPV4ToNumber(ipAddress));
      }
      else if (IpAddressUtils.IsIpAddressV6(ipAddress))
      {
        return this.LocationFromIPV6Number((decimal)IpAddressUtils.IPV6ToNumber(ipAddress));
      }
      return null;
    }

    /// <summary>
    /// Getting geographical location from an IPv4 number.
    /// </summary>
    /// <param name="ipNumber">a <see cref="long"/> number represent an IP Address</param>
    /// <returns>return a <see cref="GeoLocation"/> object</returns>
    public GeoLocation LocationFromIPV4Number(double ipNumber)
    {
      return dbContext.ip2location_db11
        .Where(e => ipNumber < e.IPTo && ipNumber > e.IPFrom)
        .OrderBy(p => p.IPTo)
        .Take(1)
        .FirstOrDefault();
    }

    /// <summary>
    /// Getting geographical location from an IPv6 number.
    /// </summary>
    /// <param name="ipNumber">a <see cref="long"/> number represent an IP Address</param>
    /// <returns>return a <see cref="GeoLocation"/> object</returns>
    public GeoLocation LocationFromIPV6Number(decimal ipNumber)
    {
      return dbContext
        .ip2location_db11_ipv6.Where(e => ipNumber < e.IPTo && ipNumber > e.IPFrom)
        .OrderBy(p => p.IPTo)
        .Take(1)
        .FirstOrDefault();
    }

    /// <summary>
    /// Getting geographical localtion from latitude and longitude point.
    /// </summary>
    /// <param name="latitude">latitude location of the visitor</param>
    /// <param name="longitude">longitude location of the visitor</param>
    /// <returns></returns>
    public GeoLocation LocationFromLatlng(double latitude, double longitude)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Get location provider type
    /// </summary>
    /// <returns><see cref="LocationProviderType"/></returns>
    public LocationProviderType Type()
    {
      return LocationProviderType.Ip2Location;
    }
  }
}
