﻿using C1.Web.Api.Visitor.Models;
using System;

namespace C1.Web.Api.Visitor.Providers
{
  /// <summary>
  /// A location provider help to detect the Visitor Location using Google MAP API.
  /// </summary>
  public class GoogleMapLocationProvider : ILocationProvider
  {
    /// <summary>
    /// An API Key to working with Google MAP API.
    /// </summary>
    public string ApiKey { get; set; }

    /// <summary>
    /// Default constructor.
    /// </summary>
    /// <param name="key">A Google MAP API Key </param>
    public GoogleMapLocationProvider(string key = "")
    {
      this.ApiKey = key;
    }

    /// <summary>
    /// Getting Geographical location based on an IP Address
    /// </summary>
    /// <param name="ipAddress">A string represent an IP Address</param>
    /// <returns><see cref="GeoLocation"/></returns>
    public GeoLocation LocationFromIPAddress(string ipAddress)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Getting Geographical location based on latitude and longitude point.
    /// </summary>
    /// <param name="latitude">latitude location</param>
    /// <param name="longitude">longitude location</param>
    /// <returns><see cref="GeoLocation"/></returns>
    public GeoLocation LocationFromLatlng(double latitude, double longitude)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// Get location provider type
    /// </summary>
    /// <returns><see cref="LocationProviderType"/></returns>
    public LocationProviderType Type()
    {
      return LocationProviderType.GoogleMapAPI;
    }
  }
}
