﻿namespace C1.Web.Api.Visitor.Models
{
    /// <summary>
    /// The Device information comes from client
    /// </summary>
    public class VisitorDevice : StringValueExtractor, IStringValueGetter
    {
        /// <summary>
        /// Client device name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Client device version
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Client screen information
        /// </summary>
        public VisitorScreen VisitorScreen { get; set; }

        /// <summary>
        /// Check client is tablet or not
        /// </summary>
        public bool IsTablet { get; set; }

        /// <summary>
        /// Check client is mobile or not
        /// </summary>
        public bool IsMobile { get; set; }

        /// <summary>
        /// GetCombiningStringValue of client device
        /// </summary>
        /// <returns></returns>
        public string GetCombiningStringValue()
        {
            return base.GetStringValue();
        }
    }
}