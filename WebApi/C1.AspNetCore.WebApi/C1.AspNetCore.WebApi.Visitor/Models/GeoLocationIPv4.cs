﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace C1.Web.Api.Visitor.Models
{
  /// <summary>
  /// The geographical location model of the visitor.
  /// </summary>
  public class GeoLocationIPv4 : GeoLocation
  {
    /// <summary>
    /// A begin ip range of the City
    /// </summary>
    [Column("ip_from")]
    public double IPFrom { get; set; }

    /// <summary>
    /// An end ip range of the City
    /// </summary>
    [Key]
    [Required]
    [Column("ip_to")]
    public double IPTo { get; set; }

  }
}
