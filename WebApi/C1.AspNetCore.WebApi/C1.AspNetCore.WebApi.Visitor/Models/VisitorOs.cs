﻿namespace C1.Web.Api.Visitor.Models
{
    /// <summary>
    /// The Os information object comes from the client
    /// </summary>
    public class VisitorOs : StringValueExtractor, IStringValueGetter
    {

        /// <summary>
        /// Os name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// OS Version
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Platform name
        /// </summary>
        public string Platform { get; set; }

        /// <summary>
        /// GetCombiningStringValue of the client Os value
        /// </summary>
        /// <returns></returns>
        public string GetCombiningStringValue()
        {
            return base.GetStringValue();
        }
    }
}