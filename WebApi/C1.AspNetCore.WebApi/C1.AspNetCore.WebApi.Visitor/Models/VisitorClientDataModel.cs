﻿using System.Linq;
using System.Text;

namespace C1.Web.Api.Visitor.Models
{
    /// <summary>
    /// The object wrapped all of client browser information
    /// </summary>
    public class VisitorClientDataModel : StringValueExtractor, IStringValueGetter
    {
        /// <summary>
        /// The visitor ID
        /// </summary>
        public string VisitorId { get; set; }

        /// <summary>
        /// The IP Address of visitor
        /// </summary>
        public Ip Ip { get; set; }

        /// <summary>
        /// Visitor Locale
        /// </summary>
        public VisitorLocale Locale { get; set; }

        /// <summary>
        /// Visitor browser information
        /// </summary>
        public VisitorBrowser Browser { get; set; }

        /// <summary>
        /// Visitor OS information
        /// </summary>
        public VisitorOs Os { get; set; }

        /// <summary>
        /// Visitor Device information
        /// </summary>
        public VisitorDevice Device { get; set; }

        /// <summary>
        /// Visitor browser info
        /// </summary>
        public VisitorClientBrowserInfo BrowserInfo { get; set; }

        /// <summary>
        /// GetCombiningStringValue of Visitor
        /// </summary>
        /// <returns></returns>
        public string GetCombiningStringValue()
        {
            var properties = this.GetType().GetProperties();

            var stringBuilder = new StringBuilder();

            foreach (var propertyInfo in properties)
            {
                if (NoneStableProperties.Contains(propertyInfo.Name)) continue;

                var value = propertyInfo.GetValue(this);

                if (value == null) continue;

                if (value is IStringValueGetter getter)
                {
                    stringBuilder.Append(getter.GetCombiningStringValue());
                }
                else
                {
                    stringBuilder.Append(value);
                }
            }

            return stringBuilder.ToString();
        }
    }
}