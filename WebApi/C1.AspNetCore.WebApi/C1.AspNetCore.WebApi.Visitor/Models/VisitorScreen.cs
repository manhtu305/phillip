﻿namespace C1.Web.Api.Visitor.Models
{
    /// <summary>
    /// The screen information object comes from client
    /// </summary>
    public class VisitorScreen : StringValueExtractor, IStringValueGetter
    {
        /// <summary>
        /// The screen resolution
        /// </summary>
        public string Resolution { get; set; }

        /// <summary>
        /// The width of screen
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// The height of screen
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// GetCombiningStringValue of the client VisitorScreen
        /// </summary>
        /// <returns></returns>
        public string GetCombiningStringValue()
        {
            return base.GetStringValue();
        }
    }
}