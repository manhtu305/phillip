﻿namespace C1.Web.Api.Visitor.Models
{
    /// <summary>
    /// The Client Geo Location
    /// </summary>
    public class VisitorGeoInfo : StringValueExtractor, IStringValueGetter
    {
        /// <summary>
        /// The collected CountryCode from visitor client side
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// The collected CountryName from visitor client side
        /// </summary>
        public string CountryName { get; set; }

        /// <summary>
        /// The collected CityName from visitor client side
        /// </summary>
        public string CityName { get; set; }

        /// <summary>
        /// The collected PostalCode from visitor client side
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// The collected RegionName from visitor client side
        /// </summary>
        public string RegionName { get; set; }

        /// <summary>
        /// The collected TimeZone from visitor client side
        /// </summary>
        public string TimeZone { get; set; }

        /// <summary>
        /// The collected Latitude from visitor client side
        /// </summary>
        public int Latitude { get; set; }

        /// <summary>
        /// The collected Longitude from visitor client side
        /// </summary>
        public int Longitude { get; set; }


        /// <summary>
        /// GetCombiningStringValue Of Geo
        /// </summary>
        /// <returns></returns>
        public string GetCombiningStringValue()
        {
            return base.GetStringValue();
        }
    }
}