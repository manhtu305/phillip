﻿using Microsoft.EntityFrameworkCore;

namespace C1.Web.Api.Visitor.Models
{
  /// <summary>
  /// A Database Context represend Entity Framework Core Database Context to work with IP2Location database.
  /// </summary>
  public class LocationContext : DbContext
  {
    /// <summary>
    /// db11 database set for IPv4
    /// </summary>
    public DbSet<GeoLocationIPv4> ip2location_db11 { get; set; }

    /// <summary>
    /// db11 database set for IPv6
    /// </summary>
    public DbSet<GeoLocationIPv6> ip2location_db11_ipv6 { get; set; }

    /// <summary>
    /// A default constructor
    /// </summary>
    /// <param name="options">options to configuration the LocationContext</param>
    public LocationContext(DbContextOptions<LocationContext> options) : base(options) { }

  }
}
