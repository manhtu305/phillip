﻿using System;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace C1.Web.Api.Visitor.Models
{

    /// <summary>
    /// 
    /// </summary>
    public interface IStringValueGetter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        string GetCombiningStringValue();
    }

    /// <summary>
    /// IP that is collected from client
    /// </summary>
    public class Ip : IStringValueGetter
    {
        /// <summary>
        /// The client Ip Address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// GetCombiningStringValue Of Client Ip Object
        /// </summary>
        /// <returns></returns>
        public string GetCombiningStringValue()
        {
            return Address;
        }
    }

    /// <summary>
    /// The base class help the concreting classes extract the string value
    /// </summary>
    public abstract class StringValueExtractor
    {
        /// <summary>
        /// All of none stable value comes from client will be listing here
        /// </summary>
        protected readonly string[] NoneStableProperties =
           { "visitorId", "firstSession", "session", "viewport", "latitude", "longitude" };

        /// <summary>
        /// Generic GetStringValue 
        /// </summary>
        /// <returns></returns>
        protected string GetStringValue()
        {
            var properties = this.GetType().GetProperties();

            var strValue = "";

            foreach (var property in properties)
            {

                if (NoneStableProperties.Contains(property.Name)) continue;

                var value = property.GetValue(this);

                if (value == null) continue;


                strValue += value.ToString();
            }

            return strValue;

        }

        /// <summary>
        /// Generic GetStringValue 
        /// </summary>
        /// <returns></returns>
        protected string GetStringValue(params string[] excludeProperties)
        {
            var properties = this.GetType().GetProperties();

            var strValue = "";

            foreach (var property in properties)
            {
                if (NoneStableProperties.Contains(property.Name)) continue;

                if (excludeProperties.Contains(property.Name)) continue;

                var value = property.GetValue(this);

                if (value == null) continue;

                strValue += value.ToString();
            }

            return strValue;
        }
    }
}
