
using C1.Web.Api.Visitor.Providers;

namespace C1.Web.Api.Visitor.Models
{
  /// <summary>
  /// All the information of the visitor that can be detected in server side including ip, geo..
  /// </summary>
  public class VisitorData
  {
    /// <summary>
    /// The ip address information of the visitor.
    /// </summary>
    public string IpAddress { get; set; }

    /// <summary>
    /// The geographical location of the visitor.
    /// </summary>
    public GeoLocation GeoLocation { get; set; }

    /// <summary>
    /// The location provider using by this Visitor API.
    /// </summary>
    public LocationProviderType LocationProviderType { get; set; }

  }
}