﻿using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace C1.Web.Api.Visitor.Models
{
    /// <summary>
    /// The client browser information object
    /// </summary>
    public class VisitorClientBrowserInfo : StringValueExtractor, IStringValueGetter
    {
        /// <summary>
        /// Client Browser language name
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// The color depth of client browser
        /// </summary>
        public float? ColorDepth { get; set; }

        /// <summary>
        /// The memory counting in GB of client.
        /// </summary>
        public float? DeviceMemory { get; set; }

        /// <summary>
        /// The screen ratio
        /// </summary>
        public float? PixelRatio { get; set; }

        /// <summary>
        /// A Number indicating the number of logical processor cores.
        /// </summary>
        public float? HardwareConcurrency { get; set; }

        /// <summary>
        /// Is using SessionStorage or not
        /// </summary> 
        public bool? SessionStorage { get; set; }

        /// <summary>
        /// Is using LocalStorage or not
        /// </summary>
        public bool? LocalStorage { get; set; }

        /// <summary>
        /// Is using IndexedDb or not
        /// </summary>
        public bool? IndexedDb { get; set; }

        /// <summary>
        /// Is using IndexedDb or not
        /// </summary>
        public bool? AddBehavior { get; set; }

        /// <summary>
        /// Is using OpenDatabase or not
        /// </summary>
        public bool? OpenDatabase { get; set; }

        /// <summary>
        /// The browser platform name
        /// </summary>
        public string Platform { get; set; }        

        /// <summary>
        /// List of Plugins client is using
        /// </summary>
        public dynamic Plugins { get; set; }

        /// <summary>
        /// Is using AdBlock or not
        /// </summary>
        public bool? AdBlock { get; set; }

        /// <summary>
        /// TouchSupport
        /// </summary>
        public List<object> TouchSupport { get; set; }

        /// <summary>
        /// Fonts
        /// </summary>
        public List<object> Fonts { get; set; }

        /// <summary>
        /// Canvas
        /// </summary>
        public List<object> Canvas { get; set; }

        /// <summary>
        /// WebglVendorAndRenderer
        /// </summary>
        public string WebglVendorAndRenderer { get; set; }

        /// <summary>
        /// GetCombiningStringValue
        /// </summary>
        /// <returns></returns>
        public string GetCombiningStringValue()
        {
            StringBuilder stringBuilder = new StringBuilder(
                GetStringValue("Plugins", "TouchSupport", "Fonts", "Canvas")
            );

            if (Plugins != null)
            {
                stringBuilder.Append(Plugins.ToString());
            }

            if (TouchSupport != null)
            {
                stringBuilder.Append(string.Join("", TouchSupport));
            }

            if (Fonts != null)
            {
                stringBuilder.Append(string.Join("", Fonts));
            }

            if (Canvas != null)
            {
                stringBuilder.Append(string.Join("", Canvas));
            }


            return stringBuilder.ToString();
        }
    }
}