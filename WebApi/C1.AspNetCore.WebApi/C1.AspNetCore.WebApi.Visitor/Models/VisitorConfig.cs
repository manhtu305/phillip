﻿using C1.Web.Api.Visitor.Providers;

namespace C1.Web.Api.Visitor.Models
{
  /// <summary>
  /// Configuration to use Visitor API
  /// </summary>
  public class VisitorConfig
  {
    /// <summary>
    /// A provider help to detect visitor Geographical Location.
    /// </summary>
    public ILocationProvider LocationProvider
    {
      get;
      set;
    }

    /// <summary>
    /// A resource path to visitor client script.
    /// </summary>
    public string ClientScriptResourcePath
    {
      get;
      set;
    }

    /// <summary>
    /// A template to pass visitor data from server side to client script.
    /// </summary>
    public string ServerScriptsTemplate
    {
      get;
      set;
    }
    /// <summary>
    /// A template to pass configuration to use google map API in the client side.
    /// </summary>
    public string ServerScriptsGoogleMapTemplate
    {
      get;
      set;
    }

    /// <summary>
    /// A callback after the script was loaded.
    /// </summary>
    public string InitCallbackTemplate
    {
      get;
      set;
    }

    /// <summary>
    /// A Default constructor
    /// </summary>
    public VisitorConfig()
    {
      ClientScriptResourcePath = "wwwroot.visitor.index.min.js";
      ServerScriptsTemplate = "c1.webapi.VisitorConfig.SERVER_DATA = {0};c1.webapi.VisitorConfig.API_HOST = {1};";
      ServerScriptsGoogleMapTemplate = "c1.webapi.VisitorConfig.SERVER_DATA = {0};c1.webapi.VisitorConfig.GOOGLEMAP_API_KEY = '{1}';c1.webapi.VisitorConfig.API_HOST = {2};";
      InitCallbackTemplate = "window.visitor.getDataAsync({0});";
    }
  }
}
