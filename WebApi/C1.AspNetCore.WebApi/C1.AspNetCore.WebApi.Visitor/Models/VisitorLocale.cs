﻿namespace C1.Web.Api.Visitor.Models
{
    /// <summary>
    /// Locale Location object comes from client
    /// </summary>
    public class VisitorLocale : StringValueExtractor, IStringValueGetter
    {
        /// <summary>
        /// The collected LanguageCode from visitor client side
        /// </summary>
        public string LanguageCode { get; set; }

        /// <summary>
        /// The collected CountryCode from visitor client side
        /// </summary>
        public string CountryCode { get; set; }


        /// <summary>
        /// GetCombiningStringValue of the client Locale
        /// </summary>
        /// <returns></returns>
        public string GetCombiningStringValue()
        {
            return base.GetStringValue();
        }
    }
}