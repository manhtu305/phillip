﻿using System.ComponentModel.DataAnnotations.Schema;

namespace C1.Web.Api.Visitor.Models
{
  /// <summary>
  /// The geographical location model of the visitor.
  /// </summary>
  public class GeoLocation
  {
    /// <summary>
    /// Two-letter ISO 3166 code for the visitor's country.
    /// </summary>
    [Column("country_code")]
    public string CountryCode { get; set; }

    /// <summary>
    /// Full name of the visitor's country.
    /// </summary>   
    [Column("country_name")]
    public string CountryName { get; set; }

    /// <summary>
    /// Two-letter code for the visitor's region
    /// </summary>
    [Column("region_name")]
    public string RegionName { get; set; }

    /// <summary>
    /// Full name of the visitor's city or town.
    /// </summary>
    [Column("city_name")]
    public string CityName { get; set; }

    /// <summary>
    /// Latitude of the city where the IP address is located.
    /// </summary>
    [Column("latitude")]
    public double Latitude { get; set; }

    /// <summary>
    /// Longitude of the city where the IP address is located.
    /// </summary>
    [Column("longitude")]
    public double Longitude { get; set; }

    /// <summary>
    /// The visitor's zip/postal code.
    /// </summary>
    [Column("zip_code")]
    public string ZipCode { get; set; }

    /// <summary>
    /// The timezone of the visitor's location.
    /// </summary>
    [Column("time_zone")]
    public string TimeZone { get; set; }

  }
}
