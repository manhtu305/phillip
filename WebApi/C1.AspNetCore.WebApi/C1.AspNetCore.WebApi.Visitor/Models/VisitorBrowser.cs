﻿namespace C1.Web.Api.Visitor.Models
{
    /// <summary>
    ///  The Browser Object comes from client
    /// </summary>
    public class VisitorBrowser : StringValueExtractor, IStringValueGetter
    {
        /// <summary>
        /// The collected Name of browser from visitor client side
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// The collected browser version from visitor client side
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// The collected browser major version from visitor client side
        /// </summary>
        public int MajorVersion { get; set; }

        /// <summary>
        /// GetCombiningStringValue Of Browser
        /// </summary>
        /// <returns></returns>
        public string GetCombiningStringValue()
        {
            return base.GetStringValue();
        }
    }
}