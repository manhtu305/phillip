﻿using System.Text;

namespace C1.Web.Api.Visitor.Utils
{
    internal static class MurMurExtension
    {
        public static uint CharCodeAt(this string val, int index)
        {
            var code = val[index];
            byte[] buff = Encoding.ASCII.GetBytes(code.ToString());
            return buff[0];
        }

        public static string ToHexString(this uint val)
        {
            return val.ToString("x8");
        }

        public static string SliceBack(this string val, int length)
        {
            var startIndex = val.Length - 1 + length;
            var outString = val.Substring(startIndex, -length);
            return outString;
        }
    }
}