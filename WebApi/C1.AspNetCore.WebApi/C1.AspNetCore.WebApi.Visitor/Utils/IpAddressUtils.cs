﻿using System;
using System.Net;
using System.Numerics;

namespace C1.Web.Api.Visitor.Utils
{
  /// <summary>
  /// An Utils class help to working with IP Address.
  /// </summary>
  public class IpAddressUtils
  {
    /// <summary>
    /// Check if given IP Address is from local machine or not.
    /// </summary>
    /// <param name="ipAddress"></param>
    /// <returns><see cref="bool"/> </returns>
    public static bool IsPrivate(string ipAddress)
    {
      try
      { // get host IP addresses
        IPAddress[] hostIPs = Dns.GetHostAddresses(ipAddress);
        // get local IP addresses
        IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());

        // test if any host IP equals to any local IP or to localhost
        foreach (IPAddress hostIP in hostIPs)
        {
          // is localhost
          if (IPAddress.IsLoopback(hostIP)) return true;
          // is local address
          foreach (IPAddress localIP in localIPs)
          {
            if (hostIP.Equals(localIP)) return true;
          }
        }
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
      }
      return false;
    }
    /// <summary>
    /// Convert an IPv4 Address string to integer
    /// This method copied from https://lite.ip2location.com/faqs
    /// </summary>
    /// <param name="ipv4"></param>
    /// <returns></returns>
    public static double IPV4ToNumber(string ipv4)
    {
      int i;
      string[] arrDec;
      double num = 0;
      if (ipv4 == "")
      {
        return 0;
      }
      else
      {
        arrDec = ipv4.Split(".");
        for (i = arrDec.Length - 1; i >= 0; i--)
        {
          num += ((int.Parse(arrDec[i]) % 256) * Math.Pow(256, (3 - i)));
        }
        return num;
      }
    }


    /// <summary>
    /// Convert ip address string into long number.
    /// This method copied from https://lite.ip2location.com/faqs
    /// </summary>
    /// <param name="ipv6"></param>
    /// <returns></returns>
    public static BigInteger IPV6ToNumber(string ipv6)
    {
      System.Net.IPAddress address;
      System.Numerics.BigInteger ipnum;

      if (System.Net.IPAddress.TryParse(ipv6, out address))
      {
        byte[] addrBytes = address.GetAddressBytes();

        if (System.BitConverter.IsLittleEndian)
        {
          System.Collections.Generic.List<byte> byteList = new System.Collections.Generic.List<byte>(addrBytes);
          byteList.Reverse();
          addrBytes = byteList.ToArray();
        }

        if (addrBytes.Length > 8)
        {
          //IPv6
          ipnum = System.BitConverter.ToUInt64(addrBytes, 8);
          ipnum <<= 64;
          ipnum += System.BitConverter.ToUInt64(addrBytes, 0);
        }
        else
        {
          //IPv4
          ipnum = System.BitConverter.ToUInt32(addrBytes, 0);
        }
        return ipnum;
      }
      return 0;
    }

    /// <summary>
    /// Conver an IP Address from string to integer.
    /// </summary>
    /// <param name="ipAddress"></param>
    /// <returns></returns>
    public static bool IsIpAddressV4(string ipAddress)
    {
      IPAddress address;
      if (IPAddress.TryParse(ipAddress, out address))
      {
        return address != null && address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork;
      }
      return false;
    }

    /// <summary>
    /// Check whether given IP Address is IPv6 or not.
    /// </summary>
    /// <param name="ipAddress"></param>
    /// <returns></returns>
    public static bool IsIpAddressV6(string ipAddress)
    {
      IPAddress address;
      if (IPAddress.TryParse(ipAddress, out address))
      {
        return address != null && address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6;
      }
      return false;
    }

    /// <summary>
    /// Check whether given IP Address is a valid IP Address or not.
    /// </summary>
    /// <param name="ipAddress"></param>
    /// <returns></returns>
    public static bool IsIpAddress(string ipAddress)
    {
      return IsIpAddressV4(ipAddress) || IsIpAddressV6(ipAddress);
    }
  }
}
