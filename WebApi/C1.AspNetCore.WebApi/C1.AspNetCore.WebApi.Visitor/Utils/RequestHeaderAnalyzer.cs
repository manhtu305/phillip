﻿using System;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace C1.Web.Api.Visitor.Utils
{
    /// <summary>
    /// Request Header Analyzer
    /// </summary>
    public class RequestHeaderAnalyzer
    {
        /// <summary>
        /// Analyze Request Header 
        /// </summary>
        /// <returns>The combined string of request header stable info</returns>
        public static string Analyze(IHeaderDictionary headerDictionary)
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.Append(GetHeaderValue(headerDictionary, "Accept"));
            stringBuilder.Append(GetHeaderValue(headerDictionary, "Accept-Encoding"));
            stringBuilder.Append(GetHeaderValue(headerDictionary, "Accept-Language"));

            return stringBuilder.ToString();
        }

        private static string GetHeaderValue(IHeaderDictionary headerDictionary, string key)
        {
            headerDictionary.TryGetValue(key, out var accept);

            if (accept.Any())
            {
                return string.Join("", accept);
            }

            return "";
        }
    }
}