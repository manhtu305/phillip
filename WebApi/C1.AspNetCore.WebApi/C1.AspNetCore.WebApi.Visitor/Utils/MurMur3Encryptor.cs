﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Internal;
using Newtonsoft.Json;

namespace C1.Web.Api.Visitor.Utils
{
    /// <summary>
    /// 
    /// </summary>
    public class MurMur3Encryptor
    {

        //
        // Given two 64bit uints (as an array of two 32bit uints) returns the two
        // added together as a 64bit uint (as an array of two 32bit uints).
        //
        private static uint[] Add(uint[] m, uint[] n)
        {

            m = new uint[] { m[0] >> 16, m[0] & 0xffff, m[1] >> 16, m[1] & 0xffff };
            n = new uint[] { n[0] >> 16, n[0] & 0xffff, n[1] >> 16, n[1] & 0xffff };
            var o = new uint[] { 0, 0, 0, 0 };
            o[3] += m[3] + n[3];
            o[2] += o[3] >> 16;
            o[3] &= 0xffff;
            o[2] += m[2] + n[2];
            o[1] += o[2] >> 16;
            o[2] &= 0xffff;
            o[1] += m[1] + n[1];
            o[0] += o[1] >> 16;
            o[1] &= 0xffff;
            o[0] += m[0] + n[0];
            o[0] &= 0xffff;

            return new[] { (o[0] << 16) | o[1], (o[2] << 16) | o[3] };
        }

        //
        // Given two 64bit uints (as an array of two 32bit uints) returns the two
        // multiplied together as a 64bit uint (as an array of two 32bit uints).
        //
        private static uint[] Multiply(uint[] m, uint[] n)
        {
            m = new uint[] { m[0] >> 16, m[0] & 0xffff, m[1] >> 16, m[1] & 0xffff };
            n = new uint[] { n[0] >> 16, n[0] & 0xffff, n[1] >> 16, n[1] & 0xffff };
            var o = new uint[] { 0, 0, 0, 0 };
            o[3] += m[3] * n[3];
            o[2] += o[3] >> 16;
            o[3] &= 0xffff;
            o[2] += m[2] * n[3];
            o[1] += o[2] >> 16;
            o[2] &= 0xffff;
            o[2] += m[3] * n[2];
            o[1] += o[2] >> 16;
            o[2] &= 0xffff;
            o[1] += m[1] * n[3];
            o[0] += o[1] >> 16;
            o[1] &= 0xffff;
            o[1] += m[2] * n[2];
            o[0] += o[1] >> 16;
            o[1] &= 0xffff;
            o[1] += m[3] * n[1];
            o[0] += o[1] >> 16;
            o[1] &= 0xffff;
            o[0] += (m[0] * n[3]) + (m[1] * n[2]) + (m[2] * n[1]) + (m[3] * n[0]);
            o[0] &= 0xffff;

            return new uint[] { (o[0] << 16) | o[1], (o[2] << 16) | o[3] };
        }
        //
        // Given a 64bit uint (as an array of two 32bit uints) and an uint
        // representing a number of bit positions, returns the 64bit uint (as an
        // array of two 32bit uints) rotated left by that number of positions.
        // 
        private static uint[] Rotl(uint[] m, int n)
        {
            n %= 64;
            if (n == 32)
            {
                return new uint[] { m[1], m[0] };
            }
            else if (n < 32)
            {
                return new uint[] { (m[0] << n) | (m[1] >> (32 - n)), (m[1] << n) | (m[0] >> (32 - n)) };
            }
            else
            {
                n -= 32;
                return new uint[] { (m[1] << n) | (m[0] >> (32 - n)), (m[0] << n) | (m[1] >> (32 - n)) };
            }
        }
        //
        // Given a 64bit uint (as an array of two 32bit uints) and an uint
        // representing a number of bit positions, returns the 64bit uint (as an
        // array of two 32bit uints) shifted left by that number of positions.
        //
        private static uint[] LeftShift(uint[] m, int n)
        {
            n %= 64;
            if (n == 0)
            {
                return m;
            }
            else if (n < 32)
            {
                return new uint[] { (m[0] << n) | (m[1] >> (32 - n)), m[1] << n };
            }
            else
            {
                return new uint[] { m[1] << (n - 32), 0 };
            }
        }
        //
        // Given two 64bit uints (as an array of two 32bit uints) returns the two
        // xored together as a 64bit uint (as an array of two 32bit uints).
        //
        private static uint[] Xor(uint[] m, uint[] n)
        {

            return new uint[] { m[0] ^ n[0], m[1] ^ n[1] };

        }
        //
        // Given a block, returns murmurHash3's final x64 mix of that block.
        // (`[0, h[0] >> 1]` is a 33 bit unsigned right shift. This is the
        // only place where we need to right shift 64bit uints.)
        //
        private static uint[] Fmix(uint[] h)
        {
            h = Xor(h, new uint[] { 0, h[0] >> 1 });
            h = Multiply(h, new uint[] { 0xff51afd7, 0xed558ccd });
            h = Xor(h, new uint[] { 0, h[0] >> 1 });
            h = Multiply(h, new uint[] { 0xc4ceb9fe, 0x1a85ec53 });
            h = Xor(h, new uint[] { 0, h[0] >> 1 });
            return h;
        }

        //
        // Given a string and an optional seed as an uint, returns a 128 bit
        // hash using the x64 flavor of MurmurHash3, as an unsigned hex.
        //
        /// <summary>
        /// Encrypt the input base on MurMur3 algorithm 
        /// </summary>
        /// <param name="input">The input string that you want to encrypt</param>
        /// <returns></returns>
        public static string Encrypt(string input)
        {
            var remainder = input.Length % 16;
            var uints = input.Length - remainder;
            var h1 = new uint[] { 0, 31 };
            var h2 = new uint[] { 0, 31 };
            var k1 = new uint[] { 0, 0 };
            var k2 = new uint[] { 0, 0 };
            var c1 = new uint[] { 0x87c37b91, 0x114253d5 };
            var c2 = new uint[] { 0x4cf5ad43, 0x2745937f };


            int i = 0;

            for (i = 0; i < uints; i = i + 16)
            {
                k1 = new uint[] { ((input.CharCodeAt(i + 4) & 0xff)) | ((input.CharCodeAt(i + 5) & 0xff) << 8) | ((input.CharCodeAt(i + 6) & 0xff) << 16) | ((input.CharCodeAt(i + 7) & 0xff) << 24), ((input.CharCodeAt(i) & 0xff)) | ((input.CharCodeAt(i + 1) & 0xff) << 8) | ((input.CharCodeAt(i + 2) & 0xff) << 16) | ((input.CharCodeAt(i + 3) & 0xff) << 24) };
                k2 = new uint[] { ((input.CharCodeAt(i + 12) & 0xff)) | ((input.CharCodeAt(i + 13) & 0xff) << 8) | ((input.CharCodeAt(i + 14) & 0xff) << 16) | ((input.CharCodeAt(i + 15) & 0xff) << 24), ((input.CharCodeAt(i + 8) & 0xff)) | ((input.CharCodeAt(i + 9) & 0xff) << 8) | ((input.CharCodeAt(i + 10) & 0xff) << 16) | ((input.CharCodeAt(i + 11) & 0xff) << 24) };
                k1 = Multiply(k1, c1);
                k1 = Rotl(k1, (31));
                k1 = Multiply(k1, c2);
                h1 = Xor(h1, k1);
                h1 = Rotl(h1, 27);
                h1 = Add(h1, h2);
                h1 = Add(Multiply(h1, new uint[] { 0, 5 }), new uint[] { 0, 0x52dce729 });
                k2 = Multiply(k2, c2);
                k2 = Rotl(k2, 33);
                k2 = Multiply(k2, c1);
                h2 = Xor(h2, k2);
                h2 = Rotl(h2, 31);
                h2 = Add(h2, h1);
                h2 = Add(Multiply(h2, new uint[] { 0, 5 }), new uint[] { 0, 0x38495ab5 });
            }

            k1 = new uint[] { 0, 0 };
            k2 = new uint[] { 0, 0 };

            switch (remainder)
            {
                case 15:
                    k2 = Xor(k2, LeftShift(new uint[] { 0, (uint)input.CharCodeAt(i + 14) }, 48));
                    break;
                case 14:
                    k2 = Xor(k2, LeftShift(new uint[] { 0, (uint)input.CharCodeAt(i + 13) }, 40));
                    break;
                case 13:
                    k2 = Xor(k2, LeftShift(new uint[] { 0, (uint)input.CharCodeAt(i + 12) }, 32));
                    break;
                case 12:
                    k2 = Xor(k2, LeftShift(new uint[] { 0, (uint)input.CharCodeAt(i + 11) }, 24));

                    break;
                case 11:
                    k2 = Xor(k2, LeftShift(new uint[] { 0, (uint)input.CharCodeAt(i + 10) }, 16));
                    break;
                case 10:
                    k2 = Xor(k2, LeftShift(new uint[] { 0, (uint)input.CharCodeAt(i + 9) }, 8));
                    break;
                case 9:
                    k2 = Xor(k2, new uint[] { 0, input.CharCodeAt(i + 8) });
                    k2 = Multiply(k2, c2);
                    k2 = Rotl(k2, (int)33);
                    k2 = Multiply(k2, c1);
                    h2 = Xor(h2, k2);
                    break;
                case 8:
                    k1 = Xor(k1, LeftShift(new uint[] { 0, (uint)input.CharCodeAt(i + 7) }, (int)56));
                    break;
                case 7:
                    k1 = Xor(k1, LeftShift(new uint[] { 0, (uint)input.CharCodeAt(i + 6) }, (int)48));

                    break;
                case 6:
                    k1 = Xor(k1, LeftShift(new uint[] { 0, (uint)input.CharCodeAt(i + 5) }, (int)40));

                    break;
                case 5:
                    k1 = Xor(k1, LeftShift(new uint[] { 0, (uint)input.CharCodeAt(i + 4) }, (int)32));

                    break;
                case 4:
                    k1 = Xor(k1, LeftShift(new uint[] { 0, (uint)input.CharCodeAt(i + 3) }, (int)24));

                    break;
                case 3:
                    k1 = Xor(k1, LeftShift(new uint[] { 0, (uint)input.CharCodeAt(i + 2) }, (int)16));
                    break;
                case 2:
                    k1 = Xor(k1, LeftShift(new uint[] { 0, (uint)input.CharCodeAt(i + 1) }, (int)8));
                    break;
                case 1:
                    k1 = Xor(k1, new uint[] { 0, input.CharCodeAt(i) });
                    k1 = Multiply(k1, c1);
                    k1 = Rotl(k1, 31);
                    k1 = Multiply(k1, c2);
                    h1 = Xor(h1, k1);
                    break;
            }
            h1 = Xor(h1, new uint[] { 0, (uint)input.Length });
            h2 = Xor(h2, new uint[] { 0, (uint)input.Length });
            h1 = Add(h1, h2);
            h2 = Add(h2, h1);
            h1 = Fmix(h1);
            h2 = Fmix(h2);
            h1 = Add(h1, h2);
            h2 = Add(h2, h1);

            return ("00000000" + (h1[0] >> 0).ToHexString()).SliceBack(-8) +
                   ("00000000" + (h1[1] >> 0).ToHexString()).SliceBack(-8) +
                   ("00000000" + (h2[0] >> 0).ToHexString()).SliceBack(-8) +
                   ("00000000" + (h2[1] >> 0).ToHexString()).SliceBack(-8);
        }
    }
}