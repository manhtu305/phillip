using System;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using System.Text.Json;
using C1.Web.Api.Visitor.Models;
using Microsoft.AspNetCore.Http.Features;
using C1.Web.Api.Visitor.Providers;
using Microsoft.AspNetCore.Http;
using C1.Util.Licensing;
using C1.Web.Api.Visitor.Utils;
using Microsoft.AspNetCore.Http.Extensions;
using Newtonsoft.Json.Linq;

namespace C1.Web.Api.Visitor
{
    /// <summary>
    /// Controller for Visitor Web API.
    /// </summary>
    public class VisitorController : ControllerBase
    {
        private readonly bool _needRenderEvalInfo;

        /// <summary>
        /// A default constructor for Visitor Web API.
        /// </summary>
        /// <param name="config">A configuration to use the Visitor Web API</param>
        public VisitorController(VisitorConfig config)
        {
            this.Config = config;
            this._needRenderEvalInfo = LicenseHelper.NeedRenderEvalInfo<LicenseDetector>();
        }

        /// <summary>
        /// Configuration to use the Visitor Web API
        /// </summary>
        public VisitorConfig Config
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the Visitor client script.
        /// </summary>
        /// <param name="callback">Callback method when the script was loaded.</param>
        /// <returns></returns>
        [HttpGet("api/visitor/visitor-client.min.js")]
        public ActionResult GetVisitorClientScript([FromQuery] string callback)
        {
            _LoadVisitorClientScript();
            var clientScript = _VisitorScript;
            clientScript += _VisitorData(HttpContext);
            clientScript += _RegisterCallback(callback);
            byte[] existingData = Encoding.UTF8.GetBytes(clientScript);
            return File(existingData, "application/javascript", "visitor-client.min.js");
        }

        /// <summary>
        /// Get the fingerprint id for the visitor
        /// </summary>
        /// <param name="requestModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/visitor/browserfingerprintid")]
        public ActionResult RequestBrowserFingerprintId([FromBody] VisitorClientDataModel requestModel)
        {
            if (requestModel == null) return Ok("");

            try
            {
                var headerCombinedString = RequestHeaderAnalyzer.Analyze(Request.Headers);
                var stringValue = requestModel.GetCombiningStringValue();

                return Ok(MurMur3Encryptor.Encrypt(stringValue + headerCombinedString));
            }
            catch (Exception e)
            {
                // prevent client app gets crashed.
                return Ok("");
            }

        }

        private string _RegisterCallback(string callback)
        {
            if (!String.IsNullOrEmpty(callback))
            {
                return String.Format(Config.InitCallbackTemplate, callback);
            }
            return "";
        }

        private string _VisitorData(HttpContext context)
        {
            VisitorData data = new VisitorData();
            data.LocationProviderType = LocationProviderType.None;
            data.IpAddress = context.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress?.ToString();
            if (Config.LocationProvider != null)
            {
                data.LocationProviderType = Config.LocationProvider.Type();
                if (Config.LocationProvider is Ip2LocationProvider)
                {
                    data.GeoLocation = Config.LocationProvider.LocationFromIPAddress(data.IpAddress);
                    return String.Format(Config.ServerScriptsTemplate, _Serialize(data), CreateHostInfo(context));
                }
                else if (Config.LocationProvider is GoogleMapLocationProvider)
                {
                    return String.Format(Config.ServerScriptsGoogleMapTemplate, _Serialize(data), ((GoogleMapLocationProvider)this.Config.LocationProvider).ApiKey, CreateHostInfo(context));
                }
            }
            return String.Format(Config.ServerScriptsTemplate, _Serialize(data), CreateHostInfo(context));
        }

        private string CreateHostInfo(HttpContext context)
        {

            var replacePath = "/api/visitor/visitor-client.min.js";

            var path = context.Request.GetDisplayUrl();

            var index = path.IndexOf(replacePath, StringComparison.Ordinal);

            var actualPath = path.Substring(0, index);

            return _Serialize(new { url = actualPath });

        }

        private string _Serialize(Object obj)
        {
            if (obj == null) return String.Empty;
            return JsonSerializer.Serialize(obj, new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true
            });
        }

        private string _VisitorScript
        {
            get;
            set;
        }

        private void _LoadVisitorClientScript()
        {
            if (!String.IsNullOrEmpty(this._VisitorScript)) return;
            try
            {
                var assembly = Assembly.GetExecutingAssembly();
                var resourceName = String.Join(".", assembly.GetName().Name, this.Config.ClientScriptResourcePath);
                var res = assembly.GetManifestResourceNames();
                using (Stream stream = assembly.GetManifestResourceStream(resourceName))
                using (StreamReader reader = new StreamReader(stream))
                {
                    this._VisitorScript = reader.ReadToEnd();
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }

    public class Test
    {
        public string VisitorId { get; set; }
    }
}