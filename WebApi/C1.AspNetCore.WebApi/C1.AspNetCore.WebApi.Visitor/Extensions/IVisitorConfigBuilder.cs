﻿using C1.Web.Api.Visitor.Providers;

namespace C1.Web.Api.Visitor
{
  /// <summary>
  /// A Builder help to create VisitorConfig object.
  /// </summary>
  public interface IVisitorConfigBuilder
  {
    /// <summary>
    /// Setup Visitor Web API using IP2Location database.
    /// </summary>
    /// <param name="connectionString">A connection to IP2Location database</param>
    void UseIp2Location(string connectionString);

    /// <summary>
    /// Setup Visitor Web API using Google Map API.
    /// </summary>
    /// <param name="apikey">an API key using Google Map services.</param>
    void UseGoogleMapLocation(string apikey);

    /// <summary>
    /// Using Visitor Web API with a custom location provider.
    /// </summary>
    /// <param name="provider">The provider help to detect Visitor's geolocation</param>
    void UseCustomLocationProvider(ILocationProvider provider);

    /// <summary>
    /// A Default configuration to use Visitor API without geolocation detector.
    /// </summary>
    void UseWithoutLocationProvider();

  }
}
