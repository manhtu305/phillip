using C1.Web.Api.Visitor.Models;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace C1.Web.Api.Visitor
{
  /// <summary>
  /// An extension help to configure Visitor API. 
  /// </summary>
  public static class VisitorExtension
  {
    /// <summary>
    /// Configure to use Visitor API
    /// </summary>
    /// <param name="services">Specifies the contract for a collection of service descriptors. <see cref="IServiceCollection"/></param>
    /// <param name="configAction">An action to configure the geolocation provider using by Visitor API.</param>
    public static void ConfigureVisitor(this IServiceCollection services, Action<IVisitorConfigBuilder> configAction)
    {
      var configBuilder = new VisitorConfigBuilder();
      configAction(configBuilder);
      services.AddSingleton<VisitorConfig>(new VisitorConfig()
      {
        LocationProvider = configBuilder.LocationProvider()
      });
    }

  }
}