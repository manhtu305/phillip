﻿using C1.Web.Api.Visitor.Models;
using C1.Web.Api.Visitor.Providers;
using Microsoft.EntityFrameworkCore;
using System;

namespace C1.Web.Api.Visitor
{
  /// <summary>
  /// A Builder help to create VisitorConfig object.
  /// </summary>
  public class VisitorConfigBuilder : IVisitorConfigBuilder
  {
    ILocationProvider locationProvider;

    /// <summary>
    /// Setup Visitor Web API using IP2Location database.
    /// </summary>
    /// <param name="connectionString">A connection to IP2Location database</param>
    public void UseIp2Location(string connectionString)
    {
      if (String.IsNullOrEmpty(connectionString))
      {
        throw new ArgumentNullException("connectionString");
      }
      var optionsBuilder = new DbContextOptionsBuilder<LocationContext>();
      optionsBuilder.UseSqlServer(connectionString);
      var context = new LocationContext(optionsBuilder.Options);
      this.locationProvider = new Ip2LocationProvider(context);
    }

    /// <summary>
    /// Setup Visitor Web API using Google Map API.
    /// </summary>
    /// <param name="apikey">an API key using Google Map services.</param>
    public void UseGoogleMapLocation(string apikey)
    {
      if (String.IsNullOrEmpty(apikey))
      {
        throw new ArgumentNullException("apikey");
      }
      this.locationProvider = new GoogleMapLocationProvider(apikey);
    }

    /// <summary>
    /// Using Visitor Web API with a custom location provider.
    /// </summary>
    /// <param name="provider">The provider help to detect Visitor's geolocation</param>
    public void UseCustomLocationProvider(ILocationProvider provider)
    {
      this.locationProvider = provider;
    }

    /// <summary>
    /// A Default configuration to use Visitor API without geolocation detector.
    /// </summary>
    public void UseWithoutLocationProvider()
    {
      this.locationProvider = null;
    }

    /// <summary>
    /// Gets LocationProvider for Visitor API.
    /// </summary>
    /// <returns>An ILocationProvider</returns>
    public ILocationProvider LocationProvider()
    {
      return this.locationProvider;
    }
  
  }
}
