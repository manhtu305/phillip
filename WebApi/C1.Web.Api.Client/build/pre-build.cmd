@pushd "%~dp0"
@echo off
setlocal enabledelayedexpansion
set fileEx=.min
@if '%1'=='Debug' (
    set fileEx=
)
set srcPath=..\..\..\Shared\WijmoMVC\dist
set destPath=..\..\C1.AspNetCore.Api\src\C1.AspNetCore.Api.Image\Resources

@echo copy wijmo5 js/css...

@if exist %destPath%\css\wijmo.min.css (
    attrib -r -s -h %destPath%\css\wijmo.min.css
    del %destPath%\css\wijmo.min.css
)
copy %srcPath%\styles\wijmo%fileEx%.css %destPath%\css\wijmo.min.css

@if exist %destPath%\js\wijmo.min.js (
    attrib -r -s -h %destPath%\js\wijmo.min.js
    del %destPath%\js\wijmo.min.js
)
copy %srcPath%\controls\wijmo%fileEx%.js %destPath%\js\wijmo.min.js

@if exist %destPath%\js\wijmo.chart.min.js (
    attrib -r -s -h %destPath%\js\wijmo.chart.min.js
    del %destPath%\js\wijmo.chart.min.js
)
copy %srcPath%\controls\wijmo.chart%fileEx%.js %destPath%\js\wijmo.chart.min.js

@if exist %destPath%\js\wijmo.chart.analytics.min.js (
    attrib -r -s -h %destPath%\js\wijmo.chart.analytics.min.js
    del %destPath%\js\wijmo.chart.analytics.min.js
)
copy %srcPath%\controls\wijmo.chart.analytics%fileEx%.js %destPath%\js\wijmo.chart.analytics.min.js

@if exist %destPath%\js\wijmo.chart.hierarchical.min.js (
    attrib -r -s -h %destPath%\js\wijmo.chart.hierarchical.min.js
    del %destPath%\js\wijmo.chart.hierarchical.min.js
)
copy %srcPath%\controls\wijmo.chart.hierarchical%fileEx%.js %destPath%\js\wijmo.chart.hierarchical.min.js

@if exist %destPath%\js\wijmo.chart.radar.min.js (
    attrib -r -s -h %destPath%\js\wijmo.chart.radar.min.js
    del %destPath%\js\wijmo.chart.radar.min.js
)
copy %srcPath%\controls\wijmo.chart.radar%fileEx%.js %destPath%\js\wijmo.chart.radar.min.js

@if exist %destPath%\js\wijmo.gauge.min.js (
    attrib -r -s -h %destPath%\js\wijmo.gauge.min.js
    del %destPath%\js\wijmo.gauge.min.js
)
copy %srcPath%\controls\wijmo.gauge%fileEx%.js %destPath%\js\wijmo.gauge.min.js

@echo copy wijmo5 dist...

@popd
goto :EOF
