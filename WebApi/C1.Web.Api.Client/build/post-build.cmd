@pushd "%~dp0"
@echo off
setlocal enabledelayedexpansion

set outputfile=webapiclient
set outputFolder=..\bin
if not exist %outputFolder% md %outputFolder%

@echo build %outputfile%...

@if exist %outputFolder%\%outputfile%.min.js (
    attrib -r -s -h %outputFolder%\%outputfile%.min.js
    del %outputFolder%\%outputfile%.min.js
)
@if exist %outputFolder%\%outputfile%.js (
    attrib -r -s -h %outputFolder%\%outputfile%.js
    del %outputFolder%\%outputfile%.js
)
copy ..\wijmo.io\AjaxHelper.min.js/b + ..\wijmo.io\Base.min.js/b + ..\wijmo.io\ImageExport.min.js/b + ..\wijmo.io\ExcelExport.min.js/b %outputFolder%\%outputfile%.min.js
copy ..\wijmo.io\AjaxHelper.js/b + ..\wijmo.io\Base.js/b + ..\wijmo.io\ImageExport.js/b + ..\wijmo.io\ExcelExport.js/b %outputFolder%\%outputfile%.js

@popd
goto :EOF
