﻿/// <reference path="../../../Shared/WijmoMVC/dist/controls/wijmo.d.ts" />
/// <reference path="../../../Shared/WijmoMVC/dist/controls/wijmo.chart.d.ts" />
/// <reference path="../../../Shared/WijmoMVC/dist/controls/wijmo.chart.analytics.d.ts" />
/// <reference path="../../../Shared/WijmoMVC/dist/controls/wijmo.chart.radar.d.ts" />
/// <reference path="../../../Shared/WijmoMVC/dist/controls/wijmo.chart.hierarchical.d.ts" />
/// <reference path="../../../Shared/WijmoMVC/dist/controls/wijmo.gauge.d.ts" />

/*
 * ComponentOne ASP.NET Web API
 * ImageExport.js
 * 
 */

/*
 * models for image export 
 */
module wijmo {
    "use strict";

    /*
     * Enum representing the types of controls that can be exported to an image
     */
    export enum ImageControlType {
        FlexChart,
        FlexPie,
        LinearGauge,
        RadialGauge,
        BulletGraph,
        Sunburst,
        FlexRadar,
        TreeMap,
    }

    /*
     * Interface representing common chart export settings
     */
    export interface IImageExportSettings extends IExportSettings {
        /*
         * State of the control needed for export.
         * This could be a control state, random settings, etc. as a string 
         * - basically whatever is needed for exporting.
         */
        state: string;
        /*
         * The vertical size of the image
         */
        height?: number;
        /*
         * The horizontal size of the image
         */
        width?: number;
        /*
         * The type of the control to export
         */
        controlType?: ImageControlType;
    }

    /*
     * Concrete class representing the chart's export settings.
     * Note that this or a object literal implementing this interface can be used for export method.
     */
    export class ImageExportSettings implements IImageExportSettings {
        private _type: ExportFileType;
        private _controlType: ImageControlType;
        private _fileName: string;
        private _height: number;
        private _width: number;
        private _state: string;

        get type(): ExportFileType {
            return this._type;
        }
        set type(value: ExportFileType) {
            this._type = asEnum(value, ExportFileType, false);
        }

        get controlType(): ImageControlType {
            return this._controlType;
        }
        set controlType(value: ImageControlType) {
            this._controlType = asEnum(value, ImageControlType, false);
        }

        get fileName(): string {
            return this._fileName;
        }
        set fileName(value: string) {
            this._fileName = asString(value, false);
        }

        get height(): number {
            return this._height;
        }
        set height(value: number) {
            this._height = asNumber(value, false);
        }

        get width(): number {
            return this._width;
        }
        set width(value: number) {
            this._width = asNumber(value, false);
        }

        get state(): string {
            return this._state;
        }
        set state(value: string) {
            this._state = asString(value, false);
        }
    }

    /*
     * Image exporter base
     */
    export class ImageExporter {

        /*
         * Request the server to export image.
         * 
         * @param control The control to export.
         * @param serviceUrl The service address.
         * @param options The export settings.
         */
        requestExport(control: wijmo.Control, serviceUrl: string, options: IImageExportSettings): void {
            var settings: IImageExportSettings = new ImageExportSettings(),
                postData: any,
                rect: ClientRect;

            // init service url
            serviceUrl = asString(serviceUrl, false);

            // set state as JSON string
            settings.state = wijmo.serialize(this._getSerializableControl(control));

            // set ImageControlType
            settings.controlType = this._getImageControlType(control);

            // init height/width if not explicitly set
            rect = control.hostElement.getBoundingClientRect();
            settings.height = (asNumber(settings.height, true) || rect.height);
            settings.width = (asNumber(settings.width, true) || rect.width);

            // copy user settings
            wijmo.copy(settings, options);

            // make sure to use getSerializableObject to avoid underscores
            postData = getSerializableObject(settings);

            // send request
            wijmo.postFormRequest(serviceUrl, postData);
        }

        _getSerializableControl(control: wijmo.Control): Object {
            var retval = getSerializableObject(control, null, ["itemFormatter","renderEngine"]);
            return retval;
        }

        _getImageControlType(control: wijmo.Control): ImageControlType {
            if (control instanceof wijmo.chart.radar.FlexRadar)
                return ImageControlType.FlexRadar;

            if (control instanceof wijmo.chart.hierarchical.Sunburst)
                return ImageControlType.Sunburst;

            if (control instanceof wijmo.chart.FlexChart)
                return ImageControlType.FlexChart;

            if (control instanceof wijmo.chart.FlexPie)
                return ImageControlType.FlexPie;

            if (control instanceof wijmo.gauge.BulletGraph)
                return ImageControlType.BulletGraph;

            if (control instanceof wijmo.gauge.LinearGauge)
                return ImageControlType.LinearGauge;

            if (control instanceof wijmo.gauge.RadialGauge)
                return ImageControlType.RadialGauge;

            if (control instanceof wijmo.chart.hierarchical.TreeMap)
                return ImageControlType.TreeMap;

            throw "This type control isn't supported.";
        }
    }
}

/*
 * charts 
 */
module wijmo.chart {
    "use strict";

    /*
     * Chart exporter
     */
    export class ImageExporter extends wijmo.ImageExporter {

        _getSerializableControl(control: FlexChartBase): Object {
            var retval: any = super._getSerializableControl(control),
                i: number = 0, series: any;

            retval.itemsSource = ImageExporter._collectionViewToArray(control);

            // legend and dataLabel
            retval.legend = getSerializableObject(control.legend);
            retval.dataLabel = getSerializableObject(control["dataLabel"]);

            if (control instanceof FlexChartCore) {
                // individual axes
                retval.axisX = getSerializableObject(control.axisX);
                retval.axisX.itemsSource = ImageExporter._collectionViewToArray(control.axisX);
                retval.axisY = getSerializableObject(control.axisY);
                retval.axisY.itemsSource = ImageExporter._collectionViewToArray(control.axisY);

                // series collection
                retval.series = [];
                for (i = 0; i < control.series.length; i++) {
                    series = control.series[i];
                    var retSeries = getSerializableObject(series, undefined, ["axisX", "axisY"]);
                    if (series.axisX) retSeries["axisX"] = getSerializableObject(series.axisX);
                    if (series.axisY) retSeries["axisY"] = getSerializableObject(series.axisY);
                    retSeries.innerType = ImageExporter._getSeriesType(series);
                    if (series instanceof wijmo.chart.analytics.YFunctionSeries) {
                        if (series.func) retSeries.func = series.func.toString();
                    } else if (series instanceof wijmo.chart.analytics.ParametricFunctionSeries) {
                        if (series.xFunc) retSeries.xFunc = series.xFunc.toString();
                        if (series.yFunc) retSeries.yFunc = series.yFunc.toString();
                    }
                    retval.series.push(retSeries);
                    retval.series[i].itemsSource = ImageExporter._collectionViewToArray(series);
                }

                // selection
                if (control.selection) {
                    retval.selection = getSerializableObject(control.selection);
                }
            }

            return retval;
        }

        static _getSeriesType(series: SeriesBase): string {
            if (series instanceof wijmo.chart.analytics.Waterfall) {
                return 'wijmo.chart.analytics.Waterfall';
            }

            if (series instanceof wijmo.chart.analytics.ErrorBar) {
                return 'wijmo.chart.analytics.ErrorBar';
            }

            if (series instanceof wijmo.chart.analytics.BoxWhisker) {
                return 'wijmo.chart.analytics.BoxWhisker';
            }

            if (series instanceof wijmo.chart.analytics.YFunctionSeries) {
                return 'wijmo.chart.analytics.YFunctionSeries';
            }

            if (series instanceof wijmo.chart.analytics.ParametricFunctionSeries) {
                return 'wijmo.chart.analytics.ParametricFunctionSeries';
            }

            if (series instanceof wijmo.chart.analytics.MovingAverage) {
                return 'wijmo.chart.analytics.MovingAverage';
            }

            if (series instanceof wijmo.chart.analytics.TrendLine) {
                return 'wijmo.chart.analytics.TrendLine';
            }

            if (series instanceof wijmo.chart.radar.FlexRadarSeries) {
                return 'wijmo.chart.radar.FlexRadarSeries';
            }

            return void (0);
        }

        static _collectionViewToArray(control: any): any {
            var itemsSource, cv;
            if (control == null || control.itemsSource == null) {
                return undefined;
            }

            // reserved for some object which only has itemsSource without collectionView.
            if (typeof (control.collectionView) === 'undefined') {
                itemsSource = control.itemsSource;
                cv = wijmo.tryCast(itemsSource, 'ICollectionView');
                if (cv != null) {
                    itemsSource = cv.sourceCollection;
                }
            } else {
                itemsSource = control.collectionView == null ? null : control.collectionView.sourceCollection;
            }

            if (itemsSource instanceof wijmo.collections.ObservableArray) {
                itemsSource = (<wijmo.collections.ObservableArray>itemsSource).slice(0);
            }

            return itemsSource;
        }
    }
}

/*
 * gauges
 */
module wijmo.gauge {
    "use strict";

    /*
     * Gauge exporter
     */
    export class ImageExporter extends wijmo.ImageExporter {

        _getSerializableControl(control: Gauge): Object {
            var retval: any = super._getSerializableControl(control),
                range: Range,
                i: number = 0;

            if (control instanceof BulletGraph) {
                // Do NOT export ranges of BulletGraph. They are generated by options.
            } else { // LinearGauge and RadialGauge
                // ranges collection
                retval.ranges = [];
                for (i = 0; i < control.ranges.length; i++) {
                    range = <Range>getSerializableObject(control.ranges[i]);

                    // WY@20150506
                    // wijmo.angular.gauge has a bug to fill "name" with irrelevant data, which causes error.
                    // If wijmo team fix this one day, the following should be removed.
                    range.name = null;

                    retval.ranges.push(range);
                }
            }

            // face and pointer
            retval.face = getSerializableObject(control.face);
            retval.pointer = getSerializableObject(control.pointer);

            return retval;
        }
    }
}