﻿using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace $safeprojectname$
{
	public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
            .UseKestrel()
            .UseIISIntegration()
            .UseStartup<Startup>()
            .UseApplicationInsights()
            .UseContentRoot(Directory.GetCurrentDirectory())
            .Build();

            host.Run();
        }
    }
}
