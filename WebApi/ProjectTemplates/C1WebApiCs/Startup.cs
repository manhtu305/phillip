﻿using System;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Collections.Generic;$if$ ($selfhost$ == True)
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;$else$
using System.Threading.Tasks;$endif$
using Owin;
using Microsoft.Owin;
using Microsoft.Owin.Cors;$if$ ($imageservices$ == True)
using C1.Web.Api.Image;$endif$$if$ ($barcodeservices$ == True)
using C1.Web.Api.BarCode;$endif$$if$ ($dataengineservices$ == True)
using C1.Web.Api.DataEngine;$endif$$if$ ($excelservices$ == True)
using C1.Web.Api.Excel;$endif$$if$ ($reportservices$ == True)
using C1.Web.Api.Report;$endif$$if$ ($pdfservices$ == True)
using C1.Web.Api.Pdf;$endif$$if$ ($cloudservices$ == True)
using C1.Web.Api.Storage;$endif$

[assembly: OwinStartup(typeof($safeprojectname$.Startup))]

namespace $safeprojectname$
{
    /// <summary>
    /// Owin startup class.
    /// </summary>
    public class Startup
    {
        private readonly HttpConfiguration config = $if$ ($selfhost$ == True)new HttpConfiguration()$else$GlobalConfiguration.Configuration$endif$;

    public void Configuration(IAppBuilder app)
        {$if$ ($selfhost$ == True)
            app.UseErrorPage();
            app.UseFileServer(new FileServerOptions()
            {
                RequestPath = PathString.Empty,
                FileSystem = new PhysicalFileSystem(@".\wwwroot"),
            });$else$
            // Tracing support
            //config.EnableSystemDiagnosticsTracing();$endif$

            // CORS supports - does not apply to <form>
            app.UseCors(CorsOptions.AllowAll);

            // Web Api
            RegisterRoutes(config);
            app.UseWebApi(config);
$if$ ($selfhost$ == True)
            app.UseWelcomePage();$else$
            // Anything unhandled
            app.Run(context =>
            {
                context.Response.Redirect(context.Request.PathBase + "/default.html");
                return Task.FromResult(0);
            });$endif$
        }

        private static void RegisterRoutes(HttpConfiguration config)
        {
            // Use APIs defined in the C1 library
            config.Services.Replace(typeof(IAssembliesResolver), new CustomAssembliesResolver());

            // Attribute Routes
            config.MapHttpAttributeRoutes();

            // default route
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }

    internal class CustomAssembliesResolver : DefaultAssembliesResolver
    {
        public override ICollection<System.Reflection.Assembly> GetAssemblies()
        {
            var assemblies = base.GetAssemblies();
            var customControllersAssembly = typeof(ApiController).Assembly;
$if$ ($cloudservices$ == True)
            customControllersAssembly = typeof(StorageController).Assembly;
            if (!assemblies.Contains(customControllersAssembly))
            {
                assemblies.Add(customControllersAssembly);
        }
$endif$$if$ ($reportservices$ == True)
            customControllersAssembly = typeof(ReportController).Assembly;
            if (!assemblies.Contains(customControllersAssembly))
            {
                assemblies.Add(customControllersAssembly);
            }
$endif$$if$ ($imageservices$ == True)
            customControllersAssembly = typeof(ImageController).Assembly;
            if (!assemblies.Contains(customControllersAssembly))
            {
                assemblies.Add(customControllersAssembly);
            }
$endif$$if$ ($barcodeservices$ == True)
            customControllersAssembly = typeof(BarCodeController).Assembly;
            if (!assemblies.Contains(customControllersAssembly))
            {
                assemblies.Add(customControllersAssembly);
            }
$endif$$if$ ($excelservices$ == True)
            customControllersAssembly = typeof(ExcelController).Assembly;
            if (!assemblies.Contains(customControllersAssembly))
            {
                assemblies.Add(customControllersAssembly);
            }
$endif$$if$ ($dataengineservices$ == True)
            customControllersAssembly = typeof(DataEngineController).Assembly;
            if (!assemblies.Contains(customControllersAssembly))
            {
                assemblies.Add(customControllersAssembly);
            }
$endif$$if$ ($pdfservices$ == True)
            customControllersAssembly = typeof(PdfController).Assembly;
            if (!assemblies.Contains(customControllersAssembly))
            {
                assemblies.Add(customControllersAssembly);
            }
$endif$
            return assemblies;
        }
    }
}