﻿$if$ ($selfhost$ == True)using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Owin.Hosting;

namespace $safeprojectname$
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://*:$serverport$/";
            using (WebApp.Start<Startup>(url))
            {
                System.Diagnostics.Process.Start(url.Replace("*", "localhost")); // Launch the browser.
                Console.WriteLine("ComponentOne Web services launched at {0}.", url);
                Console.Write("Press Enter to exit...");
                Console.ReadLine();
            }
        }
    }
}$endif$