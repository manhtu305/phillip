﻿Imports System.Web.Http
Imports System.Web.Http.Dispatcher
Imports Owin
Imports Microsoft.Owin
Imports Microsoft.Owin.Cors$if$ ($selfhost$ != True)
Imports System.Threading.Tasks$else$
Imports Microsoft.Owin.StaticFiles
Imports Microsoft.Owin.FileSystems$endif$$if$ ($imageservices$ == True)
Imports C1.Web.Api.Image$endif$$if$ ($barcodeservices$ == True)
Imports C1.Web.Api.BarCode$endif$$if$ ($dataengineservices$ == True)
Imports C1.Web.Api.DataEngine$endif$$if$ ($excelservices$ == True)
Imports C1.Web.Api.Excel$endif$$if$ ($reportservices$ == True)
Imports C1.Web.Api.Report$endif$$if$ ($pdfservices$ == True)
Imports C1.Web.Api.Pdf$endif$$if$ ($cloudservices$ == True)
Imports C1.Web.Api.Storage$endif$

<Assembly: OwinStartup(GetType(Startup))>

''' <summary>
''' Owin startup class.
''' </summary>
''' <remarks></remarks>
Public Class Startup

    Private ReadOnly config As HttpConfiguration = $if$ ($selfhost$ == True)New HttpConfiguration$else$GlobalConfiguration.Configuration$endif$

    Public Sub Configuration(ByVal app As IAppBuilder)$if$ ($selfhost$ == True)
        app.UseErrorPage()
        app.UseFileServer(New FileServerOptions With {
            .RequestPath = PathString.Empty,
            .FileSystem = New PhysicalFileSystem(".\wwwroot")})$endif$
        'CORS support
        app.UseCors(CorsOptions.AllowAll)

        'Web Api
        RegisterRoutes(config)
        app.UseWebApi(config)
$if$ ($selfhost$ == True)
        app.UseWelcomePage()$else$
        'Anything unhandled
        app.Run(Function(context)
                    context.Response.Redirect(context.Request.PathBase.ToString() + "/default.html")
                    Return Task.FromResult(0)
                End Function)$endif$
    End Sub

    Private Sub RegisterRoutes(ByVal config As HttpConfiguration)
        'Use APIs defined in the C1 library
        config.Services.Replace(GetType(IAssembliesResolver), New CustomAssembliesResolver)

        'Attribute routes
        config.MapHttpAttributeRoutes()

        'Default route
        config.Routes.MapHttpRoute(
            name:="DefaultApi",
            routeTemplate:="api/{controller}/{id}",
            defaults:=New With {.id = RouteParameter.Optional}
        )
    End Sub
End Class

Friend Class CustomAssembliesResolver
    Inherits DefaultAssembliesResolver

    Public Overrides Function GetAssemblies() As ICollection(Of Reflection.Assembly)
        Dim assemblies = MyBase.GetAssemblies()
        Dim customControllersAssembly = GetType(ApiController).Assembly

$if$ ($cloudservices$ == True)
        customControllersAssembly = GetType(StorageController).Assembly
        If Not assemblies.Contains(customControllersAssembly) Then
            assemblies.Add(customControllersAssembly)
        End If
$endif$$if$ ($reportservices$ == True)
        customControllersAssembly = GetType(ReportController).Assembly
        If Not assemblies.Contains(customControllersAssembly) Then
            assemblies.Add(customControllersAssembly)
        End If
$endif$$if$ ($imageservices$ == True)
        customControllersAssembly = GetType(ImageController).Assembly
        If Not assemblies.Contains(customControllersAssembly) Then
            assemblies.Add(customControllersAssembly)
        End If
$endif$$if$ ($barcodeservices$ == True)
        customControllersAssembly = GetType(BarCodeController).Assembly
        If Not assemblies.Contains(customControllersAssembly) Then
            assemblies.Add(customControllersAssembly)
        End If
$endif$$if$ ($excelservices$ == True)
        customControllersAssembly = GetType(ExcelController).Assembly
        If Not assemblies.Contains(customControllersAssembly) Then
            assemblies.Add(customControllersAssembly)
        End If
$endif$$if$ ($dataengineservices$ == True)
        customControllersAssembly = GetType(DataEngineController).Assembly
        If Not assemblies.Contains(customControllersAssembly) Then
            assemblies.Add(customControllersAssembly)
        End If
$endif$$if$ ($pdfservices$ == True)
        customControllersAssembly = GetType(PdfController).Assembly
        If Not assemblies.Contains(customControllersAssembly) Then
            assemblies.Add(customControllersAssembly)
        End If
$endif$
        Return assemblies
    End Function
End Class
