﻿Imports Microsoft.Owin.Hosting

Module Program

    Sub Main()
        Dim url = "http://*:$serverport$/"
        Using WebApp.Start(Of Startup)(url)
            Process.Start(url.Replace("*", "localhost"))
            Console.WriteLine("ComponentOne Web service launched at {0}.", url)
            Console.Write("Press Enter to exit...")
            Console.ReadLine()
        End Using
    End Sub

End Module
