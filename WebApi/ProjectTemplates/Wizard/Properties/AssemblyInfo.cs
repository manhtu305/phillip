using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("C1.Web.Api.TemplateWizard")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("GrapeCity, Inc.")]
[assembly: AssemblyProduct("C1.Web.Api.TemplateWizard")]
[assembly: AssemblyCopyright("Copyright © GrapeCity, Inc.  All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("bb3d56c7-8120-48b7-88f4-d33926cccf65")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion(C1.Web.Api.TemplateWizard.AssemblyInfo.Version)]
[assembly: AssemblyFileVersion(C1.Web.Api.TemplateWizard.AssemblyInfo.Version)]

namespace C1.Web.Api.TemplateWizard
{
    internal static class AssemblyInfo
    {
        public const string VersionSuffix = ".20202.44444";
        public const string Version = "4.5" + VersionSuffix;
        public const string C1WinFormsVersion = "4.5.20202.449";
#if GRAPECITY
        public const string C1WinFormsDEVersion = "5.0.20202.155";
#else
        public const string C1WinFormsDEVersion = "5.0.20202.154";
#endif
  }
}
