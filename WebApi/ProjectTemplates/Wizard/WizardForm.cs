﻿using C1.Web.Api.TemplateWizard.Models;
using System;
using System.Windows.Forms;

namespace C1.Web.Api.TemplateWizard
{
    public partial class WizardForm : Form
    {
        private const string AspNetCoreVersionPrefix = "ASP.NET Core ";
        private static readonly string[] AspNetCoreVersions = new[] { "1.0", "1.1" };

        public WizardForm()
        {
            InitializeComponent();

            foreach (var version in AspNetCoreVersions)
            {
                cbCoreVersion.Items.Add(AspNetCoreVersionPrefix + version);
            }
        }

        public bool ShowDialogWithSettings(ProjectSettings model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            UpdateUI(model);
            if (ShowDialog() != DialogResult.OK)
            {
                return false;
            }

            UpdateModel(model);
            return true;
        }

        private void UpdateModel(ProjectSettings model)
        {
            model.SelfHost = ckbSelfHost.Checked;
            model.ExcelServices = ckbExcel.Checked;
            model.ImageServices = ckbImage.Checked;
            model.ReportServices = ckbReport.Checked;
            model.DataEngineServices = ckbDataEngine.Checked;
            model.BarCodeServices = ckbBarCode.Checked;
            model.CloudServices = ckbCloud.Checked;
            model.PdfServices = ckbPdf.Checked;
            model.AspNetCoreVersion = AspNetCoreVersions[cbCoreVersion.SelectedIndex];
        }

        private void UpdateUI(ProjectSettings model)
        {
            panelSelfHost.Visible = !model.IsAspNetCore;
            ckbSelfHost.Checked = model.SelfHost;
            ckbExcel.Checked = model.ExcelServices;
            ckbImage.Checked = model.ImageServices;
            ckbReport.Checked = model.ReportServices;
            ckbDataEngine.Checked = model.DataEngineServices;
            ckbBarCode.Checked = model.BarCodeServices;
	    ckbCloud.Checked = model.CloudServices;
            ckbPdf.Checked = model.PdfServices;
            cbCoreVersion.Visible = model.IsAspNetCore && model.VsVersion >= new Version("15.0");
            cbCoreVersion.SelectedIndex = Array.IndexOf(AspNetCoreVersions, model.AspNetCoreVersion);
        }
	}
}
