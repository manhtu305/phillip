﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.XPath;
using C1.Web.Api.TemplateWizard.Models;
using EnvDTE;
using EnvDTE80;

namespace C1.Web.Api.TemplateWizard
{
    internal abstract class WizardHelper
    {
#region WizardHelper Class
        public const string WebFrameworksRegistry = "$webframeworksregistry$";

        public const string WebApiVersionKey = "$webapiversion$";

        public const string AspCorsVersionKey = "$aspcorsversion$";

        public const string OwinVersionKey = "$owinversion$";

        protected const string C1WebApiPkgRegistry = "ComponentOne WebApi";

        protected const string _uselessFolder = "_c1_useless\\";

        protected const string WinSuffix = "$winsuffix$";

        protected const string WebSuffix = "$websuffix$";

        private IEnumerable<Package> _barCodePackages;

        private IEnumerable<Package> _imagePackages;

        private IEnumerable<Package> _excelPackages;

        private IEnumerable<Package> _dataEnginePackages;

        private IEnumerable<Package> _reportPackages;

        private IEnumerable<Package> _cloudPackages;

        private IEnumerable<Package> _pdfPackages;

        protected WizardHelper(DTE dte, ProjectSettings projectSettings, Dictionary<string, string> replacements)
        {
            Dte = dte;
            ProjectSettings = projectSettings;
            Replacements = replacements;
        }

        public DTE Dte { get; set; }

        public ProjectSettings ProjectSettings
        {
            get;
            private set;
        }

        public string CustomizedVsTemplate
        {
            get;
            private set;
        }

        public string VsTemplate
        {
            get;
            private set;
        }

        public Dictionary<string, string> Replacements
        {
            get;
            private set;
        }

        protected virtual bool SupportOfflinePackages
        {
            get
            {
                return true;
            }
        }

        protected abstract string C1WebApiPkgPrefix { get; }

        protected abstract string C1WebApiPkgMainVersion { get; }

        protected abstract string C1WebApiPkgMinorVersion { get; }

        protected virtual IEnumerable<Package> ExcelPackages
        {
            get
            {
                return _excelPackages ?? (_excelPackages = new Package[] {
                    CreateWebApiPackage("Excel")
                });
            }
        }

        protected virtual IEnumerable<Package> CloudPackages
        {
            get
            {
                return _cloudPackages ?? (_cloudPackages = new Package[] {
                                        CreateWebApiPackage("Cloud")
                                });
            }
        }

        protected virtual IEnumerable<Package> ReportPackages
        {
            get
            {
                return _reportPackages ?? (_reportPackages = new Package[] {
                    CreateWebApiPackage("Report")
                });
            }
        }

        protected virtual IEnumerable<Package> ImagePackages
        {
            get
            {
                return _imagePackages ?? (_imagePackages = new Package[] {
                    CreateWebApiPackage("Image")
                });
            }
        }

        protected virtual IEnumerable<Package> DataEnginePackages
        {
            get
            {
                return _dataEnginePackages ?? (_dataEnginePackages = new Package[] {
                    CreateWebApiPackage("DataEngine")
                });
            }
        }

        protected virtual IEnumerable<Package> BarCodePackages
        {
            get
            {
                return _barCodePackages ?? (_barCodePackages = new Package[] {
                    CreateWebApiPackage("BarCode")
                });
            }
        }

        protected virtual IEnumerable<Package> PdfPackages
        {
            get
            {
                return _pdfPackages ?? (_pdfPackages = new Package[] {
                    CreateWebApiPackage("Pdf")
                });
            }
        }

        protected Package CreateWebApiPackage(string moduleName)
        {
            const string _c1PkgSuffix =
#if GRAPECITY
            ".ja";
#else
            "";
#endif
            return new Package(C1WebApiPkgPrefix + "." + moduleName + _c1PkgSuffix,
                C1WebApiPkgRegistry, C1WebApiPkgMainVersion +"." + C1WebApiPkgMinorVersion + AssemblyInfo.VersionSuffix);
        }

        public virtual void RunStarted(object[] customParams)
        {
            VsTemplate = customParams[0] as string;
            if (SupportOfflinePackages)
            {
                CustomizedVsTemplate = Path.Combine(Path.GetDirectoryName(VsTemplate), Guid.NewGuid() + ".vstemplate");                
                var doc = XDocument.Load(VsTemplate);
                var packages = PrepareExtraPackages();
                AddExtraPackages(doc, packages);
                UpdateWebFrameworksRegistry(doc);
                UpdateWebApiVersion(doc);
                doc.Save(CustomizedVsTemplate);

                customParams[0] = CustomizedVsTemplate;
            }

            AdjustReplacements();
        }

        private void UpdateWebFrameworksRegistry(XDocument doc)
        {
            var registryNode = doc.XPathSelectElement(
                string.Format(@"/*[local-name()='VSTemplate']/*[local-name()='WizardData']/*[@keyName='{0}']",
                WebFrameworksRegistry));
            if (registryNode == null)
            {
                return;
            }

            // VS2017(VS15.0) installs web frameworks nuget packages in WebStackVS15 repositry.
            var webFrameworksRegistry = ProjectSettings.VsVersion < new Version("15.0") ?
                "AspNetWebFrameworksAndTools5" : "WebStackVS" + ProjectSettings.VsVersion.Major.ToString();
            registryNode.SetAttributeValue("keyName", webFrameworksRegistry);
        }

        //<package id="Microsoft.AspNet.WebApi.Client" version="5.2.3" skipAssemblyReferences="false" />
        //should dynamic change version depend on visual studio
        private void UpdateWebApiVersion(XDocument doc)
        {
            var nodes = doc.XPathSelectElements(
                string.Format(@"/*[local-name()='VSTemplate']/*[local-name()='WizardData']/*[local-name()='packages']/*[@version='{0}']",
                WebApiVersionKey));
            bool isVs2019AndAbove = ProjectSettings.VsVersion >= new Version("16.0");

            if (nodes != null)
            {
                //from VS2019(VS16.0) using package webapi 5.2.7, lower than vs version use 5.2.3
                string version = isVs2019AndAbove ? "5.2.7" : "5.2.3";

                foreach (XElement node in nodes)
                {
                    node.SetAttributeValue("version", version);
                }
            }

            nodes = doc.XPathSelectElements(
                string.Format(@"/*[local-name()='VSTemplate']/*[local-name()='WizardData']/*[local-name()='packages']/*[@version='{0}']",
                AspCorsVersionKey));
            if (nodes != null)
            {
                //from VS2019(VS16.0) using package aspnet.cors 5.2.7, lower than vs version use 5.0.0
                string version = isVs2019AndAbove ? "5.2.7" : "5.0.0";

                foreach (XElement node in nodes)
                {
                    node.SetAttributeValue("version", version);
                }
            }

            nodes = doc.XPathSelectElements(
                 string.Format(@"/*[local-name()='VSTemplate']/*[local-name()='WizardData']/*[local-name()='packages']/*[@version='{0}']",
                 OwinVersionKey));
            if (nodes != null)
            {
				bool isTargetFramework45 = new Version(GetFrameworkVersion()) == new Version("4.5");
				//from VS2019(VS16.0) using package aspnet.cors 4.0.0, lower than vs version use 3.0.1
				// Microsoft.Owin 4.0.0 require target framework version 4.5.1+
				string version = (isVs2019AndAbove && !isTargetFramework45) ? "4.0.0" : "3.0.1";

                foreach (XElement node in nodes)
                {
                    node.SetAttributeValue("version", version);
                }
            }

            nodes = doc.XPathSelectElements(
                 string.Format(@"/*[local-name()='VSTemplate']/*[local-name()='WizardData']/*[local-name()='packages']/*[@id='{0}']",
#if GRAPECITY
                 "C1.Web.Api.ja"
#else
                 "C1.Web.Api"
#endif
                 ));
            if (nodes != null)
            {
                string minorVersion = C1WebApiPkgMinorVersion;
                if (minorVersion != "5") {
                    foreach (XElement node in nodes)
                    {
                        foreach (XAttribute at in node.Attributes())
                        {
                            if (at.Value.StartsWith("4.5."))
                            {
                                at.SetValue(at.Value.Replace("4.5.", "4." + minorVersion + "."));
                            }
                        }
                    }
                }
            }
        }

        protected virtual void AdjustReplacements()
        {
            Utils.AddReplacements(Replacements, ProjectSettings);

            if (ProjectSettings.AspNetCoreVersion == "3.0")
            {
                Replacements["$targetframeworkname$"] = "netcoreapp3.0";
            }
            else if (ProjectSettings.AspNetCoreVersion == "2.2")
            {
                Replacements["$targetframeworkname$"] = "netcoreapp2.2";
            }
            else
            {
                Replacements["$targetframeworkname$"] = GetFrameworkName();
            }

            Replacements["$serverport$"] = (new Random()).Next(1000, 0xFFFF).ToString();
            
#if GRAPECITY
            Replacements[WinSuffix] = ".Ja";
            Replacements[WebSuffix] = ".ja";
#else
            Replacements[WinSuffix] = "";
            Replacements[WebSuffix] = "";
#endif
        }

        private string GetFrameworkName()
        {
            return "net" + GetFrameworkVersion().Replace(".", "");
        }

        protected virtual string GetFrameworkVersion()
        {
            return Replacements["$targetframeworkversion$"];
        }

        public virtual void FinishedGenerating(Project project)
        {
            var projectPath = Path.GetDirectoryName(project.FullName);
            var uselessPath = Path.Combine(projectPath, _uselessFolder);
            if (Directory.Exists(uselessPath))
            {
                Directory.Delete(uselessPath, true);
            }
        }

        private void AddExtraPackages(XDocument doc, IEnumerable<Package> packages)
        {
            var repositories = new Dictionary<string, IDictionary<string, Package>>();
            foreach (var package in packages)
            {
                AddPackageToDictionary(repositories, package);
            }

            foreach (var registry in repositories.Keys)
            {
                AddPackages(doc, registry, repositories[registry].Values);
            }
        }

        private void AddPackages(XDocument doc, string registry, IEnumerable<Package> packages)
        {
            var registryNode = doc.XPathSelectElement(string.Format(@"/*[local-name()='VSTemplate']/*[local-name()='WizardData']/*[@keyName='{0}']", registry));
            foreach (var package in packages)
            {
                var packageNode = registryNode.XPathSelectElement(string.Format(@"*[@id='{0}']", package.Id));
                if (packageNode == null)
                {
                    packageNode = new XElement("package");
                    registryNode.Add(packageNode);
                }

                packageNode.SetAttributeValue("id", package.Id);
                packageNode.SetAttributeValue("version", package.Version);
                packageNode.SetAttributeValue("skipAssemblyReferences", "false");
            }
        }

        protected virtual IEnumerable<Package> PrepareExtraPackages()
        {
            var packages = new List<Package>();
            if (ProjectSettings.ExcelServices)
            {
                packages.AddRange(ExcelPackages);
            }

            if (ProjectSettings.ImageServices)
            {
                packages.AddRange(ImagePackages);
            }

            if (ProjectSettings.BarCodeServices)
            {
                packages.AddRange(BarCodePackages);
            }

            if (ProjectSettings.ReportServices)
            {
                packages.AddRange(ReportPackages);
            }

            if (ProjectSettings.DataEngineServices)
            {
                packages.AddRange(DataEnginePackages);
            }

            if (ProjectSettings.PdfServices)
            {
                packages.AddRange(PdfPackages);
            }

            if (ProjectSettings.CloudServices)
            {
                packages.AddRange(CloudPackages);
            }

            return packages;
        }

        private static void AddPackageToDictionary(IDictionary<string, IDictionary<string, Package>> repositories, Package package)
        {
            IDictionary<string, Package> packages;
            if (!repositories.TryGetValue(package.Registry, out packages))
            {
                packages = new Dictionary<string, Package>();
                repositories.Add(package.Registry, packages);
            }

            Package existedPackage;
            if (packages.TryGetValue(package.Id, out existedPackage))
            {
                if (new Version(existedPackage.Version) >= new Version(package.Version))
                {
                    return;
                }

                packages.Remove(package.Id);
            }

            packages.Add(package.Id, package);
        }

        public virtual void RunFinished()
        {
            if (!string.IsNullOrEmpty(CustomizedVsTemplate) && File.Exists(CustomizedVsTemplate))
            {
                File.Delete(CustomizedVsTemplate);
            }
        }

        public static WizardHelper Create(DTE dte, ProjectSettings projectSettings, Dictionary<string, string> replacements)
        {
            return projectSettings.IsAspNetCore ? (WizardHelper)new AspNetCoreWizardHelper(dte, projectSettings, replacements)
                : new AspNetWizardHelper(dte, projectSettings, replacements);
        }

#endregion

#region AspNetWizardHelper Class
        private class AspNetWizardHelper : WizardHelper
        {
            private IEnumerable<Package> _aspNetExcelPackages;
            private IEnumerable<Package> _aspNetReportPackages;
            private IEnumerable<Package> _aspNetDataEnginePackages;
            private IEnumerable<Package> _aspNetPdfPackages;

            private static readonly IEnumerable<Package> _webHostPackages = new Package[] {
                new Package("Microsoft.AspNet.WebApi.WebHost", WebFrameworksRegistry, "$webapiversion$"),
                new Package("Microsoft.Owin.Host.SystemWeb", WebFrameworksRegistry, "$owinversion$")
            };

            private static readonly IEnumerable<Package> _selfHostPackages = new Package[] {
                new Package("Microsoft.AspNet.WebApi.OwinSelfHost", WebFrameworksRegistry, "$webapiversion$")
            };

            public AspNetWizardHelper(DTE dte, ProjectSettings projectSettings, Dictionary<string, string> replacements)
                : base(dte, projectSettings, replacements)
            {
                if (projectSettings.IsAspNetCore)
                {
                    throw new NotSupportedException("Don't support to create an AspNetWizardHelper instance for ASP.NET Core project.");
                }
            }

            protected override string C1WebApiPkgMainVersion
            {
                get
                {
                    return "4";
                }
            }

            private string minorVersion = "";
            protected override string C1WebApiPkgMinorVersion
            {
                get
                {
                    if (minorVersion == "")
                    {
                        var versionStr = Replacements["$targetframeworkversion$"];
                        var version = new Version(versionStr);
                        var minVersion = new Version("4.5.2");
                        minorVersion = version < minVersion ? "0" : "5";
                    }
                    return minorVersion;
                }
            }

            private bool isNet452AndAbove()
            {
                return C1WebApiPkgMinorVersion == "5";
            }

            protected override string C1WebApiPkgPrefix
            {
                get
                {
                    return "C1.Web.Api";
                }
            }

            private string winFormsVersion = "";
            internal string WinFormsVersion
            {
                get
                {
                    if (winFormsVersion == "")
                    {
                        Version ver = new Version(AssemblyInfo.C1WinFormsVersion);
                        winFormsVersion = string.Format("4.{0}.{1}.{2}", C1WebApiPkgMinorVersion, ver.Build, ver.Revision);
                    }
                    return winFormsVersion;
                }
            }           

            protected override IEnumerable<Package> ExcelPackages
            {
                get
                {
                    if (_aspNetExcelPackages == null)
                    {
                        var aspNetExcelPackages = isNet452AndAbove() ? 
                            new List<Package> {}
                            : new List<Package> {new Package("C1.Excel", C1WebApiPkgRegistry, WinFormsVersion) };
                        aspNetExcelPackages.AddRange(base.ExcelPackages);
                        _aspNetExcelPackages = aspNetExcelPackages;
                    }

                    return _aspNetExcelPackages;
                }
            }

            protected override IEnumerable<Package> ReportPackages
            {
                get
                {
                    if (_aspNetReportPackages == null)
                    {
                        var aspNetReportPackages = isNet452AndAbove() ?
                            new List<Package> {
                                CreateWebApiPackage("Document")
                            }
                            : new List<Package> {
                                new Package("C1.Excel", C1WebApiPkgRegistry, WinFormsVersion),
                                new Package("C1.Document", C1WebApiPkgRegistry, WinFormsVersion),
                                new Package("C1.FlexReport", C1WebApiPkgRegistry, WinFormsVersion),
                                CreateWebApiPackage("Document")
                            };
                        aspNetReportPackages.AddRange(base.ReportPackages);
                        _aspNetReportPackages = aspNetReportPackages;
                    }

                    return _aspNetReportPackages;
                }
            }

            protected override IEnumerable<Package> DataEnginePackages
            {
                get
                {
                    if (_aspNetDataEnginePackages == null)
                    {

                        var aspNetDataEnginePackages = isNet452AndAbove() ? new List<Package>()
                            : new List<Package> { new Package("C1.DataEngine", C1WebApiPkgRegistry, WinFormsVersion)};
                         
                        aspNetDataEnginePackages.AddRange(base.DataEnginePackages);
                        _aspNetDataEnginePackages = aspNetDataEnginePackages;
                    }

                    return _aspNetDataEnginePackages;
                }
            }

            protected override IEnumerable<Package> PdfPackages
            {
                get
                {
                    if (_aspNetPdfPackages == null)
                    {
                        var aspNetPdfPackages = isNet452AndAbove() ?
                            new List<Package> {
                                CreateWebApiPackage("Document")
                            }
                            : new List<Package> {
                                new Package("C1.Document", C1WebApiPkgRegistry, WinFormsVersion),
                                CreateWebApiPackage("Document")
                            };
                        aspNetPdfPackages.AddRange(base.PdfPackages);
                        _aspNetPdfPackages = aspNetPdfPackages;
                    }

                    return _aspNetPdfPackages;
                }
            }

            protected override IEnumerable<Package> PrepareExtraPackages()
            {
                var packages = new List<Package>();
                packages.AddRange(base.PrepareExtraPackages());

                if (ProjectSettings.SelfHost)
                {
                    packages.AddRange(_selfHostPackages);
                }
                else
                {
                    packages.AddRange(_webHostPackages);
                }

                return packages;
            }

            protected override void AdjustReplacements()
            {
                base.AdjustReplacements();

                var wwwrootFolder = ProjectSettings.SelfHost ? "wwwroot\\" : string.Empty;
                Replacements["$wwwrootfolder$"] = wwwrootFolder;

                var selfHostOnlyFolder = ProjectSettings.SelfHost ? string.Empty : _uselessFolder;
                Replacements["$selfhostonlyfolder$"] = selfHostOnlyFolder;

                var webHostOnlyFolder = !ProjectSettings.SelfHost ? string.Empty : _uselessFolder;
                Replacements["$webhostonlyfolder$"] = webHostOnlyFolder;

                var licxFolder = ProjectSettings.LicxRequired ? (ProjectSettings.IsVb ? "My Project\\" : "Properties\\") : _uselessFolder;
                Replacements["$licxfolder$"] = licxFolder;
                string dataEngineSuffix = "";
                string dataEngineVer = AssemblyInfo.C1WinFormsVersion;

                string dataEngineFolder = "net452";
                string apiFWFolder = "net45";
                if (isNet452AndAbove())
                {
                    Replacements["$excelrelatedpkgrequired$"] = (ProjectSettings.ExcelServices || ProjectSettings.ReportServices).ToString();

                    Replacements["$barcoderelatedpkgrequired$"] = (ProjectSettings.PdfServices || ProjectSettings.BarCodeServices || ProjectSettings.ReportServices).ToString();

                    Replacements["$pdfpkgrequired$"] = (ProjectSettings.PdfServices || ProjectSettings.ReportServices).ToString();

                    Replacements["$winzippkgrequired$"] = (ProjectSettings.PdfServices || ProjectSettings.ExcelServices || ProjectSettings.ReportServices).ToString();
                    Replacements["$reportpkgrequired$"] = ProjectSettings.ReportServices.ToString();
                    Replacements["$dataenginepkgrequired$"] = ProjectSettings.DataEngineServices.ToString();
                    //apiFWFolder = "net452";
                    dataEngineVer = AssemblyInfo.C1WinFormsDEVersion;
                }
                else //FW <4.5.2 will use old packages
                {
                    Replacements["$excelrelatedpkgrequired$"] = "False";
                    Replacements["$barcoderelatedpkgrequired$"] = "False";
                    Replacements["$pdfpkgrequired$"] = "False";
                    Replacements["$winzippkgrequired$"] = "False";
                    Replacements["$reportpkgrequired$"] = "False";
                    Replacements["$dataenginepkgrequired$"] = "False";
                    dataEngineSuffix = ".4";
                    dataEngineFolder = "net40";
                }
                Replacements["$dataengineversion$"] = dataEngineSuffix;
                Replacements["$dataenginefolder$"] = dataEngineFolder;
                Replacements["$winversion$"] = WinFormsVersion;
                Replacements["$windeversion$"] = dataEngineVer;
                Replacements["$webversion$"] = C1WebApiPkgMainVersion + "." + C1WebApiPkgMinorVersion + AssemblyInfo.VersionSuffix;
                Replacements["$fwname$"] = apiFWFolder;
                Replacements["$abovevs2019$"] = ProjectSettings.VsVersion >= new Version("16.0") ? "True": "False";

            }
        }
#endregion

#region AspNetCoreWizardHelper Class
        private class AspNetCoreWizardHelper : WizardHelper
        {
            private class DotnetSdk
            {
                public DotnetSdk(string name, Version version)
                {
                    FullVersion = name;
                    Version = version;
                }
                public string FullVersion { get; set; }
                public Version Version { get; set; }
            }

            private string _globalJsonPath;

            public AspNetCoreWizardHelper(DTE dte, ProjectSettings projectSettings, Dictionary<string, string> replacements)
                : base(dte, projectSettings, replacements)
            {
                if (!projectSettings.IsAspNetCore)
                {
                    throw new NotSupportedException("Don't support to create an AspNetCoreWizardHelper instance for ASP.NET project.");
                }
            }

            protected override bool SupportOfflinePackages
            {
                get
                {
                    // Currently, VS doesn't support it for .NET Core project.
                    return false;
                }
            }

            protected override string C1WebApiPkgMinorVersion
            {
                get
                {
                    return "0";
                }
            }
            protected override string C1WebApiPkgMainVersion
            {
                get
                {
                    return "1";
                }
            }

            protected override string C1WebApiPkgPrefix
            {
                get
                {
                    return "C1.AspNetCore.Api";
                }
            }

            protected override void AdjustReplacements()
            {
                Replacements["$targetframeworkversion$"] = GetFrameworkVersion();
                base.AdjustReplacements();
            }

            protected override string GetFrameworkVersion()
            {
                var versionStr = Replacements["$targetframeworkversion$"];
                var version = new Version(versionStr);
                var minVersion = new Version("4.5.2");
                return version < minVersion ? minVersion.ToString() : versionStr;
            }

            public override void RunStarted(object[] customParams)
            {
                base.RunStarted(customParams);
                ProcessGlobalJson();
            }

            public override void RunFinished()
            {
                base.RunFinished();

#if !GRAPECITY
                try
                {
                    Dte.ExecuteCommand("Tools.GrapeCityLicenseManager");
                }
                catch { }
#endif
            }

            public override void FinishedGenerating(Project project)
            {
                base.FinishedGenerating(project);

                if (!string.IsNullOrEmpty(_globalJsonPath))
                {
                    var solution = project.DTE.Solution as Solution2;
                    var solutionItems = GetSolutionItemsProject(solution);
                    var globalJsonItem = solutionItems.ProjectItems.AddFromFile(_globalJsonPath);
                    if (globalJsonItem != null && globalJsonItem.IsOpen)
                    {
                        var window = globalJsonItem.Open();
                        window.Close();
                    }
                }
            }

            private void ProcessGlobalJson()
            {
                var isVs2015 = ProjectSettings.VsVersion == new Version("14.0");
                if (!isVs2015) return;

                string solutionDirectory, projectName, destinationDirectory, exclusiveProject, __exclusiveProject;
                Replacements.TryGetValue("$solutiondirectory$", out solutionDirectory);
                Replacements.TryGetValue("$destinationdirectory$", out destinationDirectory);
                Replacements.TryGetValue("$projectname$", out projectName);
                Replacements.TryGetValue("$exclusiveproject$", out exclusiveProject);
                Replacements.TryGetValue("$__exclusiveproject$", out __exclusiveProject);

                var createDirectoryForSolution = solutionDirectory != null && destinationDirectory != null && projectName != null
                    && string.Equals(Path.Combine(solutionDirectory, projectName), destinationDirectory, StringComparison.OrdinalIgnoreCase);
                var createNewSolution = __exclusiveProject == "True" || exclusiveProject == "True";
                if (!createDirectoryForSolution || !createNewSolution) return;

                var globalJsonPath = Path.Combine(solutionDirectory, "global.json");
                if (File.Exists(globalJsonPath)) return;

                var sdkVersion = GetGlobalJsonSdkVersion();
                if (string.IsNullOrEmpty(sdkVersion)) return;

                var content = GetGlobalJsonContent();
                content = content.Replace("DOTNET_SDK_VERSION", sdkVersion);
                File.WriteAllText(globalJsonPath, content);
                _globalJsonPath = globalJsonPath;
            }

            private Project GetSolutionItemsProject(Solution2 solution)
            {
                try
                {
                    return solution.Item(Constants.vsProjectKindSolutionItems);
                }
                catch (ArgumentException)
                {
                    return solution.AddSolutionFolder("Solution Items");
                }
            }

            private string GetGlobalJsonContent()
            {
                using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("C1.Web.Api.TemplateWizard.Resources.global.json"))
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }

            private string GetGlobalJsonSdkVersion()
            {
                var sdkFolder = @"c:\Program Files\dotnet\sdk\";
                if (!Directory.Exists(sdkFolder)) return null;

                var sdks = new List<DotnetSdk>();
                foreach (var folder in Directory.GetDirectories(sdkFolder))
                {
                    var name = Path.GetFileName(folder);
                    var index = name.IndexOf('-');
                    var versionString = index > 0 ? name.Substring(0, index) : name;
                    Version version;
                    if (Version.TryParse(versionString, out version))
                    {
                        sdks.Add(new DotnetSdk(name, version));
                    }
                }

                if (sdks.Count == 0) return null;

                var sdk100Version = new Version(1, 0, 0);
                var maxSdkVersion = sdks.Max(sdk => sdk.Version);
                if (maxSdkVersion <= sdk100Version) return null;

                var sdk100s = sdks.Where(sdk => sdk.Version <= sdk100Version);
                if (!sdk100s.Any()) return null;

                return sdk100s.Max(sdk => sdk.FullVersion);
            }
        }
#endregion
    }
}