﻿using System;
using System.Collections.Generic;
using EnvDTE;
using Microsoft.VisualStudio.TemplateWizard;
using System.Reflection;
using System.IO;
using Microsoft.VisualStudio.Shell;
using IServiceProvider = Microsoft.VisualStudio.OLE.Interop.IServiceProvider;
using Microsoft.VisualStudio.Shell.Interop;

namespace C1.Web.Api.TemplateWizard
{
    public class Wizard : IWizard
    {
        private ServiceProvider _serviceProvider;
        private WizardHelper _helper;
        private IWizard _nuget = (IWizard)Assembly.Load("NuGet.VisualStudio.Interop, Version=1.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a").CreateInstance("NuGet.VisualStudio.TemplateWizard");

        // This method is called before opening any item that 
        // has the OpenInEditor attribute.
        public void BeforeOpeningFile(ProjectItem projectItem)
        {
            _nuget.BeforeOpeningFile(projectItem);
        }

        public void ProjectFinishedGenerating(Project project)
        {
            // Fix bug 218118
            // Save solution file at upper level folder of project file.
            var solutionPath = project.DTE.Solution.FullName;
            var createDirectoryForSolution = !string.IsNullOrEmpty(solutionPath);
            if (!createDirectoryForSolution)
            {
                var projPath = Path.GetDirectoryName(project.FullName);
                var projName = Path.GetFileNameWithoutExtension(project.FullName);
                solutionPath = Path.Combine(Path.GetDirectoryName(projPath), projName + ".sln");
                project.DTE.Solution.SaveAs(solutionPath);
            }

            _nuget.ProjectFinishedGenerating(project);
            if (_helper != null)
            {
                _helper.FinishedGenerating(project);

                var vs2015AndNew = _helper.ProjectSettings.VsVersion >= new Version("14.0");
                var isCore = _helper.ProjectSettings.IsAspNetCore;
                if (vs2015AndNew && !isCore && !createDirectoryForSolution)
                {
                    var solution4 = _serviceProvider.GetService(typeof(SVsSolution)) as IVsSolution4;
                    var solution5 = _serviceProvider.GetService(typeof(SVsSolution)) as IVsSolution5;
                    if (solution4 != null && solution5 != null)
                    {
                        var guid = solution5.GetGuidOfProjectFile(project.FullName);
                        solution4.UnloadProject(guid, (uint)_VSProjectUnloadStatus.UNLOADSTATUS_UnloadedByUser);
                        solution4.ReloadProject(guid);
                    }
                }
            }
        }

        // This method is only called for item templates,
        // not for project templates.
        public void ProjectItemFinishedGenerating(ProjectItem projectItem)
        {
            _nuget.ProjectItemFinishedGenerating(projectItem);
        }

        // This method is called after the project is created.
        public void RunFinished()
        {
            _nuget.RunFinished();
            if (_helper != null)
            {
                _helper.RunFinished();
            }
        }

        public void RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
        {
            _serviceProvider = new ServiceProvider((IServiceProvider)automationObject);
            var settings = Utils.GenerateSettings(automationObject, replacementsDictionary);

            string hideDialogStr;
            var hideDialog = replacementsDictionary.TryGetValue("$hidedialog$", out hideDialogStr) &&
                hideDialogStr.Equals("True", StringComparison.OrdinalIgnoreCase);
            if (!hideDialog)
            {
                var result = Utils.ShowDialog(settings);
                if (!result.HasValue || !result.Value)
                {
                    throw new WizardCancelledException();
                }
            }

            _helper = WizardHelper.Create((DTE)automationObject, settings, replacementsDictionary);
            _helper.RunStarted(customParams);

            _nuget.RunStarted(automationObject, replacementsDictionary, runKind, customParams);
        }

        // This method is only called for item templates,
        // not for project templates.
        public bool ShouldAddProjectItem(string filePath)
        {
            return true;
        }
    }
}
