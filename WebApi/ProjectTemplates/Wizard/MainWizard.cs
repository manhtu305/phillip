﻿using Microsoft.VisualStudio.TemplateWizard;
using System.Collections.Generic;
using System.Text;
using EnvDTE;
using System.IO;

namespace C1.Web.Api.TemplateWizard
{
    public class MainWizard : IWizard
    {
        private DTE _dte;
        private string _solutionPath;
        private string _templatePath;
        private string _projectName;
        private string _destinationPath;

        public void BeforeOpeningFile(ProjectItem projectItem)
        {
        }

        public void ProjectFinishedGenerating(Project project)
        {
        }

        public void ProjectItemFinishedGenerating(ProjectItem projectItem)
        {
        }

        public void RunFinished()
        {
            _dte.Solution.AddFromTemplate(_templatePath, _destinationPath, _projectName);
        }

        public void RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams)
        {
            var settings = Utils.GenerateSettings(automationObject, replacementsDictionary);
            var result = Utils.ShowDialog(settings);
            if (!result.HasValue || !result.Value)
            {
                throw new WizardCancelledException();
            }

            _dte = (DTE)automationObject;
            replacementsDictionary.TryGetValue("$safeprojectname$", out _projectName);
            replacementsDictionary.TryGetValue("$solutiondirectory$", out _solutionPath);
            replacementsDictionary.TryGetValue("$destinationdirectory$", out _destinationPath);
            _solutionPath = Path.Combine(_solutionPath, _projectName + ".sln");
            ResolveVsTemplate(customParams, settings.AspNetCoreVersion);
            var replacements = new Dictionary<string, string>();
            //should pass $targetframeworkversion$ to exact template, otherwise will get incorrect version.
            replacements.Add("$targetframeworkversion$", replacementsDictionary["$targetframeworkversion$"]);
            replacements.Add("$hidedialog$", "True");

            string exclusiveProject = "False";
            replacementsDictionary.TryGetValue("$exclusiveproject$", out exclusiveProject);
            replacements.Add("$__exclusiveproject$", exclusiveProject);

            Utils.AddReplacements(replacements, settings);
            AddTemplateParameters(replacements);
        }

        public bool ShouldAddProjectItem(string filePath)
        {
            return true;
        }

        /// <summary>
        /// Template should follow this rule,
        /// Base template name: AspNetCoreAppCs.
        /// Core 1.0 template name: AspNetCore10AppCs.
        /// Core 2.0 template name: AspNetCore20AppCs.
        /// When searching template, replace the 'Core' part to 'Core10' or 'Core20'.
        /// </summary>
        private void ResolveVsTemplate(object[] customParams, string aspNetCoreVersion)
        {
            var vsTemplate = customParams[0] as string;
            var rootPath = Path.GetDirectoryName(vsTemplate);
            FileInfo fileInfo = new FileInfo(vsTemplate);
            var templateName = Path.GetFileNameWithoutExtension(vsTemplate);
            var coreVersion = "";
            if (aspNetCoreVersion != "2.2")
            {
              coreVersion = string.Format("{0}0", aspNetCoreVersion[0]);
            }
            else
            {
              coreVersion = "22";
            }
      templateName = templateName.Replace("Core", "Core" + coreVersion) + ".vstemplate";

            _templatePath = SearchTemplate(fileInfo.Directory.Parent.FullName, templateName);
            if (string.IsNullOrEmpty(_templatePath))
            {
                throw new FileNotFoundException(string.Format("Template {0} not found.", templateName));
            }
        }

        private static string SearchTemplate(string targetDir, string fileName)
        {
            foreach (string d in Directory.GetDirectories(targetDir))
            {
                var filePath = Path.Combine(d, fileName);
                if (File.Exists(filePath))
                {
                    return filePath;
                }
            }
            return string.Empty;
        }

        private void AddTemplateParameters(Dictionary<string, string> replacementsDictionary)
        {
            var builder = new StringBuilder();
            foreach (var key in replacementsDictionary.Keys)
            {
                builder.Append(string.Format("|{0}={1}", key, replacementsDictionary[key]));
            }
            _templatePath += builder.ToString();
        }
    }
}
