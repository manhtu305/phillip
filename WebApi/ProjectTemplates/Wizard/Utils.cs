﻿using C1.Web.Api.TemplateWizard.Models;
using C1.Web.Api.TemplateWizard.UI;
using C1Wizard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace C1.Web.Api.TemplateWizard
{
    internal static class Utils
    {
        public static ProjectSettings GenerateSettings(object automationObject, Dictionary<string, string> replacementsDictionary)
        {
            var settings = ProjectSettings.Create(replacementsDictionary);
            settings.VsVersion= new Version(((dynamic)automationObject).Version.ToString());
            return settings;
        }

        public static bool? ShowDialog(ProjectSettings settings)
        {
            var dlg = new WizardDialog();
            dlg.Title = Localization.Resources.WizardTitle;
            var content = new ApiSettings();
            content.DataContext = settings;
            dlg.DialogContent = content;
            return dlg.ShowDialog();
        }

        public static void AddReplacements(IDictionary<string, string> replacements, object model)
        {
            if (replacements == null)
            {
                throw new ArgumentNullException("replacements");
            }

            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            var properties = model.GetType().GetProperties().Where(p => !p.GetCustomAttributes<ReplacementIgnoreAttribute>().Any());
            foreach (var prop in properties)
            {
                var value = prop.GetValue(model);
                if (value != null)
                {
                    replacements[string.Format("${0}$", prop.Name.ToLower())] = value.ToString();
                }
            }
        }
    }
}
