﻿namespace C1.Web.Api.TemplateWizard
{
    partial class WizardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WizardForm));
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.titlePic = new System.Windows.Forms.PictureBox();
			this.lblTitle = new System.Windows.Forms.Label();
			this.lbLineBottom = new System.Windows.Forms.Label();
			this.panelPage = new System.Windows.Forms.FlowLayoutPanel();
			this.panelSettings = new System.Windows.Forms.Panel();
			this.cbCoreVersion = new System.Windows.Forms.ComboBox();
			this.panelServices = new System.Windows.Forms.Panel();
			this.ckbCloud = new System.Windows.Forms.CheckBox();
			this.ckbPdf = new System.Windows.Forms.CheckBox();
			this.ckbImage = new System.Windows.Forms.CheckBox();
			this.ckbDataEngine = new System.Windows.Forms.CheckBox();
			this.ckbBarCode = new System.Windows.Forms.CheckBox();
			this.ckbReport = new System.Windows.Forms.CheckBox();
			this.ckbExcel = new System.Windows.Forms.CheckBox();
			this.lbServices = new System.Windows.Forms.Label();
			this.lbLineServices = new System.Windows.Forms.Label();
			this.panelSelfHost = new System.Windows.Forms.Panel();
			this.lbLineSelfHost = new System.Windows.Forms.Label();
			this.ckbSelfHost = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.titlePic)).BeginInit();
			this.panelPage.SuspendLayout();
			this.panelSettings.SuspendLayout();
			this.panelServices.SuspendLayout();
			this.panelSelfHost.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnOk
			// 
			resources.ApplyResources(this.btnOk, "btnOk");
			this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.btnOk.Name = "btnOk";
			this.btnOk.UseVisualStyleBackColor = true;
			// 
			// btnCancel
			// 
			resources.ApplyResources(this.btnCancel, "btnCancel");
			this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			// 
			// titlePic
			// 
			resources.ApplyResources(this.titlePic, "titlePic");
			this.titlePic.Name = "titlePic";
			this.titlePic.TabStop = false;
			// 
			// lblTitle
			// 
			resources.ApplyResources(this.lblTitle, "lblTitle");
			this.lblTitle.Name = "lblTitle";
			// 
			// lbLineBottom
			// 
			resources.ApplyResources(this.lbLineBottom, "lbLineBottom");
			this.lbLineBottom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lbLineBottom.Name = "lbLineBottom";
			// 
			// panelPage
			// 
			resources.ApplyResources(this.panelPage, "panelPage");
			this.panelPage.Controls.Add(this.titlePic);
			this.panelPage.Controls.Add(this.panelSettings);
			this.panelPage.Controls.Add(this.cbCoreVersion);
			this.panelPage.Controls.Add(this.panelServices);
			this.panelPage.Controls.Add(this.panelSelfHost);
			this.panelPage.Name = "panelPage";
			// 
			// panelSettings
			// 
			this.panelSettings.Controls.Add(this.lblTitle);
			resources.ApplyResources(this.panelSettings, "panelSettings");
			this.panelSettings.Name = "panelSettings";
			// 
			// cbCoreVersion
			// 
			this.cbCoreVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbCoreVersion.FormattingEnabled = true;
			resources.ApplyResources(this.cbCoreVersion, "cbCoreVersion");
			this.cbCoreVersion.Name = "cbCoreVersion";
			// 
			// panelServices
			// 
			resources.ApplyResources(this.panelServices, "panelServices");
			this.panelServices.Controls.Add(this.ckbCloud);
			this.panelServices.Controls.Add(this.ckbPdf);
			this.panelServices.Controls.Add(this.ckbImage);
			this.panelServices.Controls.Add(this.ckbDataEngine);
			this.panelServices.Controls.Add(this.ckbBarCode);
			this.panelServices.Controls.Add(this.ckbReport);
			this.panelServices.Controls.Add(this.ckbExcel);
			this.panelServices.Controls.Add(this.lbServices);
			this.panelServices.Controls.Add(this.lbLineServices);
			this.panelServices.Name = "panelServices";
			// 
			// ckbCloud
			// 
			resources.ApplyResources(this.ckbCloud, "ckbCloud");
			this.ckbCloud.Name = "ckbCloud";
			this.ckbCloud.UseVisualStyleBackColor = true;
			// 
			// ckbPdf
			// 
			resources.ApplyResources(this.ckbPdf, "ckbPdf");
			this.ckbPdf.Name = "ckbPdf";
			this.ckbPdf.UseVisualStyleBackColor = true;
			// 
			// ckbImage
			// 
			resources.ApplyResources(this.ckbImage, "ckbImage");
			this.ckbImage.Name = "ckbImage";
			this.ckbImage.UseVisualStyleBackColor = true;
			// 
			// ckbDataEngine
			// 
			resources.ApplyResources(this.ckbDataEngine, "ckbDataEngine");
			this.ckbDataEngine.Name = "ckbDataEngine";
			this.ckbDataEngine.UseVisualStyleBackColor = true;
			// 
			// ckbBarCode
			// 
			resources.ApplyResources(this.ckbBarCode, "ckbBarCode");
			this.ckbBarCode.Name = "ckbBarCode";
			this.ckbBarCode.UseVisualStyleBackColor = true;
			// 
			// ckbReport
			// 
			resources.ApplyResources(this.ckbReport, "ckbReport");
			this.ckbReport.Name = "ckbReport";
			this.ckbReport.UseVisualStyleBackColor = true;
			// 
			// ckbExcel
			// 
			resources.ApplyResources(this.ckbExcel, "ckbExcel");
			this.ckbExcel.Name = "ckbExcel";
			this.ckbExcel.UseVisualStyleBackColor = true;
			// 
			// lbServices
			// 
			resources.ApplyResources(this.lbServices, "lbServices");
			this.lbServices.Name = "lbServices";
			// 
			// lbLineServices
			// 
			this.lbLineServices.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			resources.ApplyResources(this.lbLineServices, "lbLineServices");
			this.lbLineServices.Name = "lbLineServices";
			// 
			// panelSelfHost
			// 
			resources.ApplyResources(this.panelSelfHost, "panelSelfHost");
			this.panelSelfHost.Controls.Add(this.lbLineSelfHost);
			this.panelSelfHost.Controls.Add(this.ckbSelfHost);
			this.panelSelfHost.Name = "panelSelfHost";
			// 
			// lbLineSelfHost
			// 
			this.lbLineSelfHost.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			resources.ApplyResources(this.lbLineSelfHost, "lbLineSelfHost");
			this.lbLineSelfHost.Name = "lbLineSelfHost";
			// 
			// ckbSelfHost
			// 
			resources.ApplyResources(this.ckbSelfHost, "ckbSelfHost");
			this.ckbSelfHost.Name = "ckbSelfHost";
			this.ckbSelfHost.UseVisualStyleBackColor = true;
			// 
			// WizardForm
			// 
			resources.ApplyResources(this, "$this");
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.panelPage);
			this.Controls.Add(this.lbLineBottom);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.btnOk);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "WizardForm";
			this.ShowInTaskbar = false;
			((System.ComponentModel.ISupportInitialize)(this.titlePic)).EndInit();
			this.panelPage.ResumeLayout(false);
			this.panelPage.PerformLayout();
			this.panelSettings.ResumeLayout(false);
			this.panelSettings.PerformLayout();
			this.panelServices.ResumeLayout(false);
			this.panelServices.PerformLayout();
			this.panelSelfHost.ResumeLayout(false);
			this.panelSelfHost.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.PictureBox titlePic;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lbLineBottom;
        private System.Windows.Forms.FlowLayoutPanel panelPage;
        private System.Windows.Forms.Panel panelSettings;
        private System.Windows.Forms.Panel panelServices;
        private System.Windows.Forms.CheckBox ckbImage;
        private System.Windows.Forms.CheckBox ckbDataEngine;
        private System.Windows.Forms.CheckBox ckbBarCode;
	private System.Windows.Forms.CheckBox ckbCloud;
	private System.Windows.Forms.CheckBox ckbReport;
        private System.Windows.Forms.CheckBox ckbExcel;
        private System.Windows.Forms.Label lbServices;
        private System.Windows.Forms.Label lbLineServices;
        private System.Windows.Forms.Panel panelSelfHost;
        private System.Windows.Forms.Label lbLineSelfHost;
        private System.Windows.Forms.CheckBox ckbSelfHost;
        private System.Windows.Forms.CheckBox ckbPdf;
        private System.Windows.Forms.ComboBox cbCoreVersion;
	}
}