﻿using System;

namespace C1.Web.Api.TemplateWizard
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public sealed class ReplacementIgnoreAttribute : Attribute
    {
    }
}
