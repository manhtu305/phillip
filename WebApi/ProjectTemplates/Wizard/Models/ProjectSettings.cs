﻿using C1.Web.Api.TemplateWizard.Properties;
using System;
using System.Collections.Generic;

namespace C1.Web.Api.TemplateWizard.Models
{
    public class ProjectSettings
    {
        private bool _selfHost;
        private static IList<CoreVersion> _coreVersions;

        public ProjectSettings()
        {
        }

        internal static ProjectSettings Create(Dictionary<string, string> replacementsDictionary)
        {
            var settings = new ProjectSettings();
            settings.IsAspNetCore = GetReplacementsBoolValue(replacementsDictionary, "IsAspNetCore");
            settings.IsVb = GetReplacementsBoolValue(replacementsDictionary, "IsVb");
            settings.AspNetCoreVersion = GetReplacementsStringValue(replacementsDictionary, "AspNetCoreVersion", Settings.Default.AspNetCoreVersion);
            settings.SelfHost = GetReplacementsBoolValue(replacementsDictionary, "SelfHost");
            settings.ExcelServices = GetReplacementsBoolValue(replacementsDictionary, "ExcelServices", true);
            settings.BarCodeServices = GetReplacementsBoolValue(replacementsDictionary, "BarCodeServices", true);
            settings.ReportServices = GetReplacementsBoolValue(replacementsDictionary, "ReportServices", true);
            settings.DataEngineServices = GetReplacementsBoolValue(replacementsDictionary, "DataEngineServices", true);
            settings.PdfServices = GetReplacementsBoolValue(replacementsDictionary, "PdfServices", true);
            settings.CloudServices = GetReplacementsBoolValue(replacementsDictionary, "CloudServices", false);
            settings.ImageServices = GetReplacementsBoolValue(replacementsDictionary, "ImageServices");

            return settings;
        }

        private static bool GetReplacementsBoolValue(Dictionary<string, string> replacementsDictionary, string name)
        {
            return GetReplacementsBoolValue(replacementsDictionary, name, false);
        }

        private static bool GetReplacementsBoolValue(Dictionary<string, string> replacementsDictionary, string name, bool defaultValue)
        {
            string valueStr;
            string key = string.Format("${0}$", name.ToLower());
            if (replacementsDictionary.TryGetValue(key, out valueStr))
            {
                return valueStr.Equals("True", StringComparison.OrdinalIgnoreCase);
            }
            return defaultValue;
        }

        private static string GetReplacementsStringValue(Dictionary<string, string> replacementsDictionary, string name)
        {
            return GetReplacementsStringValue(replacementsDictionary, name, null);
        }

        private static string GetReplacementsStringValue(Dictionary<string, string> replacementsDictionary, string name, string defaultValue)
        {
            string valueStr;
            string key = string.Format("${0}$", name.ToLower());
            if (replacementsDictionary.TryGetValue(key, out valueStr))
            {
                return valueStr;
            }
            return defaultValue;
        }

        [ReplacementIgnoreAttribute]
        public bool ShowSelfHosted
        {
            get { return !IsAspNetCore; }
        }

        [ReplacementIgnoreAttribute]
        public bool ShowCoreVersion
        {
            get
            {
                return IsAspNetCore && VsVersion >= new Version("15.0");
            }
        }

        [ReplacementIgnoreAttribute]
        public IEnumerable<CoreVersion> CoreVersions
        {
            get
            {
                if (_coreVersions == null)
                {
                    _coreVersions = new List<CoreVersion>();
                    //_coreVersions.Add(new CoreVersion("1.0"));
                    //_coreVersions.Add(new CoreVersion("1.1"));
                    if (VsVersion >= new Version("15.0"))
                    {
                        _coreVersions.Add(new CoreVersion("2.0"));
                        _coreVersions.Add(new CoreVersion("2.1"));
                    }
#if GRAPECITY
                    AspNetCoreVersion = "2.0";
#else
                    if (VsVersion >= new Version("15.0"))
                    {
                        _coreVersions.Add(new CoreVersion("2.2"));
                    }
                    if (VsVersion >= new Version("16.0"))
                    {
                        _coreVersions.Add(new CoreVersion("3.0"));
                    }
#endif
                }

                return _coreVersions;
            }
        }

        // True means JPN version.
        public bool IsGrapeCity
        {
            get
            {
#if GRAPECITY
                return true;
#else
                return false;
#endif
            }
        }

        public string PkgSuffix
        {
            get
            {
#if GRAPECITY
                return ".ja";
#else
                return "";
#endif
            }
        }

        public bool IsVb
        {
            get;
            private set;
        }

        public bool IsAspNetCore
        {
            get;
            private set;
        }

        public string AspNetCoreVersion
        {
            get { return Settings.Default.AspNetCoreVersion; }
            set
            {
                Settings.Default.AspNetCoreVersion = value;
                Settings.Default.Save();
            }
        }

        public bool SelfHost
        {
            get
            {
                return !IsAspNetCore && _selfHost;
            }
            set
            {
                _selfHost = value;
            }
        }

        public bool ExcelServices
        {
            get;
            set;
        }

        public bool ImageServices
        {
            get;
            set;
        }

        public bool CloudServices
        {
            get;
            set;
        }

        public bool BarCodeServices
        {
            get;
            set;
        }

        public bool ReportServices
        {
            get;
            set;
        }

        public bool DataEngineServices
        {
            get;
            set;
        }

        public bool PdfServices
        {
            get;
            set;
        }

        public bool BasicLicenseRequired
        {
            get
            {
                return ImageServices || ExcelServices || BarCodeServices;
            }
        }

        public bool LicxRequired
        {
            get
            {
                return BasicLicenseRequired || ReportServices || DataEngineServices || PdfServices || CloudServices;
            }
        }

        public Version VsVersion { get; set; }
    }

    public class CoreVersion
    {
        private const string AspNetCoreVersionPrefix = "ASP.NET Core ";

        public CoreVersion(string version)
        {
            Version = version;
        }

        public string Version { get; private set; }
        public string FullVersion { get { return AspNetCoreVersionPrefix + Version; } }
    }
}
