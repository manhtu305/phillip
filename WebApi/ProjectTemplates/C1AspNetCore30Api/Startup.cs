﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Threading;
using C1.AspNetCore.Api;
$if$ ($cloudservices$ == True)
using Google.Apis.Drive.v3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Util.Store;
$endif$
$if$ ($selfhost$ == True)
using Owin;
$endif$
namespace $safeprojectname$
{
	public class Startup
{
    public Startup(IWebHostEnvironment env, IConfiguration config, ILoggerFactory loggerFactory)
    {
        Environment = env;
        var builder = new ConfigurationBuilder()
            .SetBasePath(env.ContentRootPath)
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
            .AddEnvironmentVariables();
        Configuration = builder.Build();
    }

    public static IWebHostEnvironment Environment { get; set; }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

        // Add framework services.
        services.AddMvc(option => { option.EnableEndpointRouting = false; });

        // CORS support
        services.AddCors(options =>
          {
              options.AddPolicy("AllowAll", builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
          });

        services.Configure<FormOptions>(options => options.ValueLengthLimit = int.MaxValue);
        services.AddC1Api();
  }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
    {
        app.UseC1Api();
        app.UseStaticFiles();

        // do not change the name of defaultCulture
        var defaultCulture = "en-US";
        IList<CultureInfo> supportedCultures = new List<CultureInfo>
            {
                new CultureInfo(defaultCulture)
            };
        app.UseRequestLocalization(new RequestLocalizationOptions
        {
            DefaultRequestCulture = new RequestCulture(defaultCulture),
            SupportedCultures = supportedCultures,
            SupportedUICultures = supportedCultures
        });

        app.UseMvc();

        app.UseStorageProviders()
        .AddDiskStorage("ExcelRoot", Path.Combine(env.WebRootPath, "ExcelRoot"));

		$if$ ($cloudservices$ == True)
			string[] scopes = { DriveService.Scope.Drive };
        string applicationName = "C1WebApi";
        // please uncomment this line when you want to test Onedrive service.
        // Onedrive storage
        //app.UseStorageProviders().AddOneDriveStorage("OneDrive", Configuration["AppSettings:OneDriveAccessToken"]);

        // please uncomment this line when you want to test GoogleDrive service.
        // Google storage
        //app.UseStorageProviders().AddGoogleDriveStorage("GoogleDrive", GetUserCredential(scopes, env), applicationName);
        
        // Dropbox storage
        app.UseStorageProviders().AddDropBoxStorage("DropBox", Configuration["AppSettings:DropBoxStorageAccessToken"], applicationName);

		$endif$
	}
$if$ ($cloudservices$ == True)
	//private UserCredential GetUserCredential(string[] scopes, IWebHostEnvironment env)
 //   {
 //       var contentRootPath = env.ContentRootPath;
 //       UserCredential credential;
 //       using (var fileStream =
 //           new FileStream(Path.Combine(contentRootPath, "credentials.json"), FileMode.Open, FileAccess.Read))
 //       {
 //           // The file token.json stores the user's access and refresh tokens, and is created
 //           // automatically when the authorization flow completes for the first time.
 //           string credPath = "token.json";
 //           credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
 //               GoogleClientSecrets.Load(fileStream).Secrets,
 //               scopes,
 //               "user",
 //               CancellationToken.None,
 //               new FileDataStore(Path.Combine(contentRootPath, credPath), true)).Result;
 //       }
 //       return credential;
 //   }
    $endif$

    $if$ ($dataengineservices$ == True)
		private string GetConnectionString(IWebHostEnvironment env)
    {
        var configConnectionString = Configuration["Data:DefaultConnection:ConnectionString"];
        configConnectionString = configConnectionString.Replace("|DataDirectory|", Path.Combine(env.WebRootPath, "App_Data"));
        return configConnectionString;
    }
	$endif$
	}
}
