﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using SHttpMethod = System.Net.Http.HttpMethod;

namespace C1.Web.Api.Tests.PathRoute
{
    [TestClass]
    public class PathRouteTests: BaseRoutingTests
    {
        [TestMethod]
        public void TestGet()
        {
            const string res = "/files/report.flxr/name/$fake/a/b";
            var request = new HttpRequestMessage(SHttpMethod.Get,
                "http://www.fakesite.com/api/fakepath" + res);

            var routeTester = new RouteTester(HttpConfig, request);

            Assert.AreEqual(typeof(FakePathController), routeTester.GetControllerType());
            Assert.AreEqual("Get", routeTester.GetActionName());

            var values = routeTester.SubRoutes.ToList();
            CollectionAssert.Contains(values, new KeyValuePair<string, object> ("path", "files/report.flxr/name"));
            CollectionAssert.Contains(values, new KeyValuePair<string, object> ("id", "a"));
            CollectionAssert.Contains(values, new KeyValuePair<string, object> ("value", "b"));
        }

        [TestMethod]
        public void TestPost()
        {
            const string res = "/files/report.flxr/name/$fake/a/param/b";
            var request = new HttpRequestMessage(SHttpMethod.Post,
                "http://www.fakesite.com/api/fakepath" + res);

            var routeTester = new RouteTester(HttpConfig, request);

            Assert.AreEqual(typeof(FakePathController), routeTester.GetControllerType());
            Assert.AreEqual("Post", routeTester.GetActionName());

            var values = routeTester.SubRoutes.ToList();
            CollectionAssert.Contains(values, new KeyValuePair<string, object>("path", "files/report.flxr/name"));
            CollectionAssert.Contains(values, new KeyValuePair<string, object>("id", "a"));
            CollectionAssert.Contains(values, new KeyValuePair<string, object>("value", "b"));
        }

        [TestMethod]
        public void TestOnlyPath()
        {
            const string res = "/files/report.flxr/name";
            var request = new HttpRequestMessage(SHttpMethod.Get,
                "http://www.fakesite.com/api/fakepath" + res);

            var routeTester = new RouteTester(HttpConfig, request);

            Assert.AreEqual(typeof(FakePathController), routeTester.GetControllerType());
            Assert.AreEqual("GetPath", routeTester.GetActionName());

            var values = routeTester.SubRoutes.ToList();
            CollectionAssert.Contains(values, new KeyValuePair<string, object>("path", "files/report.flxr/name"));
        }

        [TestMethod]
        public void TestGetPathContent()
        {
            const string res = "/files/report.flxr/name/$content";
            var request = new HttpRequestMessage(SHttpMethod.Get,
                "http://www.fakesite.com/api/fakepath" + res);

            var routeTester = new RouteTester(HttpConfig, request);

            Assert.AreEqual(typeof(FakePathController), routeTester.GetControllerType());
            Assert.AreEqual("GetPathContent", routeTester.GetActionName());

            var values = routeTester.SubRoutes.ToList();
            CollectionAssert.Contains(values, new KeyValuePair<string, object>("path", "files/report.flxr/name"));
        }
    }

    [RoutePrefix("api/fakepath")]
    public class FakePathController: ApiController
    {
        [HttpGet]
        [PathRoute("{*path}/$fake/{id}/{value}")]
        public string Get(string path, string id, string value)
        {
            return string.Format("path: {0}, id: {0}, value: {1}.", path, id, value);
        }

        [HttpPost]
        [PathRoute("{*path}/$fake/{id}/param/{value}")]
        public string Post(string path, string id, string value)
        {
            return string.Format("path: {0}, id: {1}, value: {2}.", path, id, value);
        }

        [HttpGet]
        [PathRoute("{*path}", Order = 1)]
        public string GetPath(string path)
        {
            return string.Format("path: {0}.", path);
        }

        [HttpGet]
        [PathRoute("{*path}/$content")]
        public string GetPathContent(string path)
        {
            return string.Format("path: {0}.", path);
        }
    }
}
