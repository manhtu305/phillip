﻿//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Http;
//using C1.Web.Api.Report;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using SHttpMethod = System.Net.Http.HttpMethod;

//namespace C1.Web.Api.Tests.Report
//{
//    [TestClass]
//    public class ReportRoutingTests: BaseRoutingTests
//    {
//        [TestMethod]
//        public void Get()
//        {
//            const string idAndCmd = "files/report.flxr/name/run";
//            var request = new HttpRequestMessage(SHttpMethod.Get,
//                "http://www.fakesite.com/api/report/" + idAndCmd);

//            var routeTester = new RouteTester(HttpConfig, request);

//            Assert.AreEqual(typeof(ReportController), routeTester.GetControllerType());
//            Assert.AreEqual("Get", routeTester.GetActionName());

//            CollectionAssert.AreEqual(routeTester.SubRoutes.ToList(),
//                new Dictionary<string, object> { { "idAndCmd", idAndCmd } });
//        }
//    }
//}
