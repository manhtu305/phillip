﻿//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Http;
//using C1.Web.Api.Report;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using SHttpMethod = System.Net.Http.HttpMethod;

//namespace C1.Web.Api.Tests.Report
//{
//    [TestClass]
//    public class ReportsRoutingTests : BaseRoutingTests
//    {
//        [TestMethod]
//        public void Get()
//        {
//            const string path = "files/report.flxr";
//            var request = new HttpRequestMessage(SHttpMethod.Get,
//                "http://www.fakesite.com/api/reports/" + path);

//            var routeTester = new RouteTester(HttpConfig, request);

//            Assert.AreEqual(typeof(ReportsController), routeTester.GetControllerType());
//            Assert.AreEqual("Get", routeTester.GetActionName());

//            CollectionAssert.AreEqual(routeTester.SubRoutes.ToList(),
//                new Dictionary<string, object> { { "path", path } });
//        }
//    }
//}
