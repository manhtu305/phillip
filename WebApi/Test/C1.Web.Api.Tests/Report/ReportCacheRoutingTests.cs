﻿//using System.Collections.Generic;
//using System.Linq;
//using System.Net.Http;
//using C1.Web.Api.Report;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using MSTestHacks;
//using SHttpMethod = System.Net.Http.HttpMethod;

//namespace C1.Web.Api.Tests.Report
//{
//    [TestClass]
//    public class ReportCacheRoutingTests: BaseRoutingTests
//    {
//        [TestMethod]
//        public void Get()
//        {
//            const string id = "adbcefg";
//            var request = new HttpRequestMessage(SHttpMethod.Get,
//                string.Format("http://www.fakesite.com/api/reportcache/{0}", id));

//            var routeTester = new RouteTester(HttpConfig, request);

//            Assert.AreEqual(typeof(ReportCacheController), routeTester.GetControllerType());
//            Assert.AreEqual("Get", routeTester.GetActionName());

//            CollectionAssert.AreEqual(routeTester.SubRoutes.ToList(),
//                new Dictionary<string, object> { { "id", id } });
//        }

//        [TestMethod]
//        public void Parameters()
//        {
//            const string id = "adbcefg";
//            var request = new HttpRequestMessage(SHttpMethod.Get,
//                string.Format("http://www.fakesite.com/api/reportcache/{0}/parameters", id));

//            var routeTester = new RouteTester(HttpConfig, request);

//            Assert.AreEqual(typeof(ReportCacheController), routeTester.GetControllerType());
//            Assert.AreEqual("Parameters", routeTester.GetActionName());

//            CollectionAssert.AreEqual(routeTester.SubRoutes.ToList(),
//                new Dictionary<string, object> { { "id", id } });
//        }

//        [TestMethod]
//        public void Clear()
//        {
//            const string id = "adbcefg";
//            var request = new HttpRequestMessage(SHttpMethod.Get,
//                string.Format("http://www.fakesite.com/api/reportcache/{0}/clear", id));

//            var routeTester = new RouteTester(HttpConfig, request);

//            Assert.AreEqual(typeof(ReportCacheController), routeTester.GetControllerType());
//            Assert.AreEqual("Clear", routeTester.GetActionName());

//            CollectionAssert.AreEqual(routeTester.SubRoutes.ToList(),
//                new Dictionary<string, object> { { "id", id } });
//        }

//        [TestMethod]
//        public void Stop()
//        {
//            const string id = "adbcefg";
//            var request = new HttpRequestMessage(SHttpMethod.Get,
//                string.Format("http://www.fakesite.com/api/reportcache/{0}/stop", id));

//            var routeTester = new RouteTester(HttpConfig, request);

//            Assert.AreEqual(typeof(ReportCacheController), routeTester.GetControllerType());
//            Assert.AreEqual("Stop", routeTester.GetActionName());

//            CollectionAssert.AreEqual(routeTester.SubRoutes.ToList(),
//                new Dictionary<string, object> { { "id", id } });
//        }

//        [TestMethod]
//        public void Outline()
//        {
//            const string id = "adbcefg";
//            var request = new HttpRequestMessage(SHttpMethod.Get,
//                string.Format("http://www.fakesite.com/api/reportcache/{0}/outline", id));

//            var routeTester = new RouteTester(HttpConfig, request);

//            Assert.AreEqual(typeof(ReportCacheController), routeTester.GetControllerType());
//            Assert.AreEqual("Outline", routeTester.GetActionName());

//            CollectionAssert.AreEqual(routeTester.SubRoutes.ToList(),
//                new Dictionary<string, object> { { "id", id } });
//        }

//        [TestMethod]
//        public void OutlineLabel()
//        {
//            const string id = "adbcefg";
//            const string value = "123";
//            var request = new HttpRequestMessage(SHttpMethod.Get,
//                string.Format("http://www.fakesite.com/api/reportcache/{0}/outlinelabel/{1}", id, value));

//            var routeTester = new RouteTester(HttpConfig, request);

//            Assert.AreEqual(typeof(ReportCacheController), routeTester.GetControllerType());
//            Assert.AreEqual("OutlineLabel", routeTester.GetActionName());

//            CollectionAssert.AreEqual(routeTester.SubRoutes.ToList(),
//                new Dictionary<string, object> { { "id", id }, {"value", value} });
//        }

//        [TestMethod]
//        public void Bookmark()
//        {
//            const string id = "adbcefg";
//            const string value = "123";
//            var request = new HttpRequestMessage(SHttpMethod.Get,
//                string.Format("http://www.fakesite.com/api/reportcache/{0}/bookmark/{1}", id, value));

//            var routeTester = new RouteTester(HttpConfig, request);

//            Assert.AreEqual(typeof(ReportCacheController), routeTester.GetControllerType());
//            Assert.AreEqual("Bookmark", routeTester.GetActionName());

//            CollectionAssert.AreEqual(routeTester.SubRoutes.ToList(),
//                new Dictionary<string, object> { { "id", id }, { "value", value } });
//        }

//        [TestMethod]
//        public void Export()
//        {
//            const string id = "adbcefg";
//            var request = new HttpRequestMessage(SHttpMethod.Get,
//                string.Format("http://www.fakesite.com/api/reportcache/{0}/export?format=html", id));

//            var routeTester = new RouteTester(HttpConfig, request);

//            Assert.AreEqual(typeof(ReportCacheController), routeTester.GetControllerType());
//            Assert.AreEqual("Export", routeTester.GetActionName());

//            CollectionAssert.AreEqual(routeTester.SubRoutes.ToList(),
//                new Dictionary<string, object> { { "id", id } });
//        }

//        [TestMethod]
//        [DataSource("C1.Web.Api.Tests.Report.ReportCacheRoutingTests.SearchDataList")]
//        public void Search()
//        {
//            var data =TestContext.GetRuntimeDataSourceObject<SearchData>();
//            var request = new HttpRequestMessage(SHttpMethod.Get,
//                string.Format("http://www.fakesite.com/api/reportcache/{0}/search/{1}{2}", 
//                data.Id, data.Value, data.QueryString));

//            var routeTester = new RouteTester(HttpConfig, request);

//            Assert.AreEqual(typeof (ReportCacheController), routeTester.GetControllerType());
//            Assert.AreEqual("Search", routeTester.GetActionName());

//            CollectionAssert.AreEqual(routeTester.SubRoutes.ToList(),
//                new Dictionary<string, object>
//                {
//                    {"id", data.Id},
//                    {"value", data.Value}
//                });

//            CollectionAssert.AreEqual(routeTester.Queries.ToList(), data.ExpectedQueries.ToList());
//        }

//        private static readonly IEnumerable<SearchData> _searchDataList = new List<SearchData>
//        {
//            new SearchData(),
//            new SearchData{
//                QueryString = "?matchCase=true", 
//                ExpectedQueries = new Dictionary<string, string>{{"matchCase", "true"}}
//            },
//            new SearchData{
//                QueryString = "?matchCase=false", 
//                ExpectedQueries = new Dictionary<string, string>{{"matchCase", "false"}}
//            },
//            new SearchData{
//                QueryString = "?wholeword=true", 
//                ExpectedQueries = new Dictionary<string, string>{{"wholeword", "true"}}
//            },
//            new SearchData{
//                QueryString = "?wholeword=false", 
//                ExpectedQueries = new Dictionary<string, string>{{"wholeword", "false"}}
//            },
//            new SearchData{
//                QueryString = "?matchCase=true&wholeword=false", 
//                ExpectedQueries = new Dictionary<string, string>{{"matchCase", "true"}, {"wholeword", "false"}}
//            },
//            new SearchData{
//                QueryString = "?wholeword=true&matchCase=false", 
//                ExpectedQueries = new Dictionary<string, string>{{"wholeword", "true"}, {"matchCase", "false"}}
//            },
//        };

//        public IEnumerable<SearchData> SearchDataList
//        {
//            get
//            {
//                return _searchDataList;
//            }
//        } 

//        public class SearchData
//        {
//            public SearchData()
//            {
//                Id = "abcdefg";
//                Value = "123";
//                ExpectedQueries = new Dictionary<string, string>();
//            }

//            public string Id { get; set; }

//            public string Value { get; set; }

//            public string QueryString { get; set; }

//            public IDictionary<string, string> ExpectedQueries { get; set; }
//        }
//    }
//}
