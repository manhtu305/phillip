﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using C1.C1Excel;
using C1.Web.Api.Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace C1.Web.Api.Tests
{
    public static class TestHelper
    {
        private static readonly object _rootDirLocker = new object();
        private static string _rootDir;
        private const string _expectedResultsDirName = "TestExpectedResults";
        private const string _samplesDirName = "TestSamples";

        public static string GetRootDir(this TestContext context)
        {
            if (_rootDir != null) return _rootDir;
            lock (_rootDirLocker)
            {
                if (_rootDir != null) return _rootDir;

                var testDir = context.TestRunDirectory;
                var rootDir = Path.Combine(testDir, "..", "..");
                var expectedFilesDir = Path.Combine(rootDir, _expectedResultsDirName);
                if (Directory.Exists(expectedFilesDir))
                {
                    // In tfs build:
                    return _rootDir = rootDir;
                }

                // In dev machine:
                return _rootDir = Path.Combine(rootDir, "..", "Test");
            }
        }

        public static string GetSamplesDir(this TestContext context, string samplePath = null)
        {
            var path = Path.Combine(context.GetRootDir(), _samplesDirName);
            return string.IsNullOrEmpty(samplePath) ? path : Path.Combine(path, samplePath);
        }

        public static string GetExpectedResultsDir(this TestContext context, string resultPath = null)
        {
            var path = Path.Combine(context.GetRootDir(), _expectedResultsDirName);
            return string.IsNullOrEmpty(resultPath) ? path : Path.Combine(path, resultPath);
        }

        public static C1XLBook CreateC1XLBook(IEnumerable data = null)
        {
            if (data == null)
            {
                data = InitData();
            }

            var c1XLbook = Utils.NewC1XLBook();
            c1XLbook.Author = "test";

            var c1Sheet = c1XLbook.Sheets[0];
            var rowIndex = 0;
            const int headerIndex = 0;
            const int dataStartIndex = 1;
            foreach (var item in data)
            {
                var columnIndex = 0;
                foreach (var pi in item.GetType().GetProperties())
                {
                    if (rowIndex == 0)
                    {
                        //Add header
                        var headerCell = c1Sheet[headerIndex, columnIndex];
                        headerCell.Value = pi.Name;
                    }

                    var cell = c1Sheet[rowIndex + dataStartIndex, columnIndex];
                    cell.Value = pi.GetValue(item);
                    columnIndex++;
                }

                rowIndex++;
            }

            return c1XLbook;
        }

        public static void CheckWorkbook(TestContext context, Workbook currentWorkbook, string name)
        {
            Assert.IsNotNull(currentWorkbook);

            var expectedFile = Path.Combine(context.GetExpectedResultsDir(), name + ".xml");

            var currentFile = Path.Combine(context.TestResultsDirectory, context.TestName);
            currentWorkbook.ToC1XLBook().Save(currentFile + ".xlsx");
            var current = Serialize(currentWorkbook);
            var currentResult = currentFile + ".xml";
            File.WriteAllText(currentResult, current);

            CompareFile(expectedFile, currentResult);
        }

        public static void CompareFile(string expectedFile, string currentFile)
        {
            var current = File.ReadAllText(currentFile);
            var expected = File.ReadAllText(expectedFile);
            Assert.AreEqual(expected, current, string.Format("{0} is different from the expected file: {1}.", currentFile, expectedFile));
        }

        public static void CompareFile(string expectedFile, Stream currentFile)
        {
            var current = StreamToString(currentFile);
            var expected = File.ReadAllText(expectedFile);
            Assert.AreEqual(expected, current, string.Format("{0} is different from the expected file: {1}.", "Current file", expectedFile));
        }

        public static string StreamToString(Stream stream)
        {
            stream.Position = 0;
            using (var reader = new StreamReader(stream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }

        public static string Serialize<T>(T thisT)
        {
            var xs = new XmlSerializer(thisT.GetType());
            using (var writer = new StringWriter())
            {
                xs.Serialize(writer, thisT);
                return writer.ToString();
            }
        }

        #region TestDataItem
        public static IEnumerable InitData(int count = 10)
        {
            var data = new List<object>();
            for (int i = 0; i < count; i++)
            {
                data.Add(new TestDataItem
                {
                    Id = i,
                    Text = GetStringItem(i),
                    Boolean = i % 2 == 0,
                    Double = GetDoubleItem(i),
                    //DateTime = GetDateTimeItem(i)
                });
            }

            return data;
        }

        private static double GetDoubleItem(int i)
        {
            return i * i / 100d;
        }

        private static string GetStringItem(int i)
        {
            string prefix;
            switch (i % 3)
            {
                case 2:
                    prefix = "B";
                    break;
                case 1:
                    prefix = "C";
                    break;
                default:
                    prefix = "A";
                    break;
            }

            return prefix + i;
        }

        private static DateTime GetDateTimeItem(int i)
        {
            var baseDateTime = new DateTime(2015, 1, 2, 3, 4, 5);
            switch (i % 3)
            {
                case 2:
                    return baseDateTime.AddMonths(i);
                case 1:
                    return baseDateTime.AddDays(i);
                default:
                    return baseDateTime.AddHours(i);
            }
        }
        public class TestDataItem
        {
            public int Id { get; set; }
            public double Double { get; set; }
            public string Text { get; set; }
            public bool Boolean { get; set; }
            //Has bug
            //public DateTime DateTime { get; set; }
        }

        #endregion TestDataItem
    }
}
