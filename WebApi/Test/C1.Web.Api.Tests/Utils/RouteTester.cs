﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;

namespace C1.Web.Api.Tests
{
    public class RouteTester
    {
        private bool _actionSelected;
        private readonly HttpRequestMessage _request;
        private readonly IHttpControllerSelector _controllerSelector;
        private readonly HttpControllerContext _controllerContext;

        public RouteTester(HttpConfiguration conf, HttpRequestMessage req)
        {
            conf.EnsureInitialized();
            _request = req;
            RouteData = conf.Routes.GetRouteData(_request);
            _request.Properties[HttpPropertyKeys.HttpRouteDataKey] = RouteData;
            _controllerSelector = new DefaultHttpControllerSelector(conf);
            _controllerContext = new HttpControllerContext(conf, RouteData, _request);
        }

        public IHttpRouteData RouteData
        {
            get;
            private set;
        }

        public IEnumerable<KeyValuePair<string, string>> Queries
        {
            get
            {
                EnsureSelectAction();
                if (!_request.Properties.ContainsKey(HttpPropertyKeys.RequestQueryNameValuePairsKey))
                {
                    return new List<KeyValuePair<string, string>>();
                }

                return _request.Properties[HttpPropertyKeys.RequestQueryNameValuePairsKey] as IEnumerable<KeyValuePair<string, string>>;
            }
        }

        private void EnsureSelectAction()
        {
            if(_actionSelected)
                return;

            GetActionName();
        }

        public IEnumerable<KeyValuePair<string, object>> SubRoutes
        {
            get
            {
                return RouteData.GetSubRoutes().SelectMany(s => s.Values);
            }
        }

        public string GetActionName()
        {
            if (_controllerContext.ControllerDescriptor == null)
                GetControllerType();

            var actionSelector = new ApiControllerActionSelector();
            var descriptor = actionSelector.SelectAction(_controllerContext);

            _actionSelected = true;

            return descriptor.ActionName;
        }

        public Type GetControllerType()
        {
            var descriptor = _controllerSelector.SelectController(_request);
            _controllerContext.ControllerDescriptor = descriptor;
            return descriptor.ControllerType;
        }
    }
}
