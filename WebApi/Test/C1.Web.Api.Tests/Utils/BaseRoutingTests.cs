﻿using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MSTestHacks;

namespace C1.Web.Api.Tests
{
    public abstract class BaseRoutingTests : TestBase
    {
        [TestInitialize]
        public void TestInitialize()
        {
            HttpConfig = new HttpConfiguration { IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always };
            HttpConfig.MapHttpAttributeRoutes();
            HttpConfig.Routes.MapHttpRoute(name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new {id = RouteParameter.Optional});
        }

        protected HttpConfiguration HttpConfig
        {
            get;
            private set;
        }
    }
}
