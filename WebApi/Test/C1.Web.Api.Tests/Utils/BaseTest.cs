﻿using C1.Web.Api.Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace C1.Web.Api.Tests
{
    public abstract class BaseTest
    {
        protected void CheckWorkbook(Workbook workbook, string expectedFile)
        {
            TestHelper.CheckWorkbook(TestContext, workbook, expectedFile);
        }

        public TestContext TestContext { get; set; }
    }
}
