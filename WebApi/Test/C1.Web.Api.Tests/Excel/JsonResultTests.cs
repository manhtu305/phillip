﻿using System.Web.Http.Results;
using C1.Web.Api.Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace C1.Web.Api.Tests.Excel
{
    [TestClass]
    public class JsonResultTests : ExcelBaseTests
    {
        [TestMethod]
        public void XmlToJson()
        {
            //TODO
        }

        [TestMethod]
        public void CsvToJson()
        {
            var controller = new ExcelController();
            var csvStream = CsvSample;
            csvStream.Position = 0;

            var request = new ExcelRequest
            {
                Type = ExportFileType.Json,
                WorkbookFile = new FormFile(csvStream, "csv")
            };

            var postResult = controller.Post(request);
            var result = postResult as JsonResult<object>;

            Assert.IsNotNull(result);

            var workbook = result.Content as Workbook;
            Assert.IsNotNull(workbook);

            CheckWorkbook(workbook, "CsvToJson");
        }

        [TestMethod]
        public void XlsToJson()
        {
            var controller = new ExcelController();
            var sampleXlsStream = XlsSample;
            sampleXlsStream.Position = 0;

            var request = new ExcelRequest
            {
                Type = ExportFileType.Json,
                WorkbookFile = new FormFile(sampleXlsStream, "xls")
            };

            var result = controller.Post(request) as JsonResult<object>;

            Assert.IsNotNull(result);

            var workbook = result.Content as Workbook;
            CheckWorkbook(workbook, "XlsToJson");
        }

        [TestMethod]
        public void XlsxToJson()
        {
            var controller = new ExcelController();
            var sampleXlsxStream = XlsxSample;
            sampleXlsxStream.Position = 0;

            var request = new ExcelRequest
            {
                Type = ExportFileType.Json,
                WorkbookFile = new FormFile(sampleXlsxStream, "xlsx")
            };

            var result = controller.Post(request) as JsonResult<object>;

            Assert.IsNotNull(result);

            var workbook = result.Content as Workbook;
            CheckWorkbook(workbook, "XlsxToJson");
        }

        [TestMethod]
        public void XmlDataToJson()
        {
            var controller = new ExcelController();
            var request = new ExcelRequest
            {
                Type = ExportFileType.Json,
                DataFile = new FormFile(XmlDataStream, "xml")
            };

            var result = controller.Post(request) as JsonResult<object>;

            Assert.IsNotNull(result);

            var workbook = result.Content as Workbook;
            CheckWorkbook(workbook, "XmlDataToJson");
        }

        [TestMethod]
        public void JsonDataToJson()
        {
            var controller = new ExcelController();
            var request = new ExcelRequest
            {
                Type = ExportFileType.Json,
                Data = JsonData
            };

            var result = controller.Post(request) as JsonResult<object>;

            Assert.IsNotNull(result);

            var workbook = result.Content as Workbook;
            CheckWorkbook(workbook, "JsonDataToJson");
        }
    }
}
