﻿using System.Collections;
using System.IO;
using C1.C1Excel;
using C1.Web.Api.Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace C1.Web.Api.Tests.Excel
{
    public abstract class ExcelBaseTests: BaseTest
    {

        protected Stream XlsxSample
        {
            get;
            set;
        }

        protected Stream XmlDataStream
        {
            get;
            set;
        }

        protected Stream XlsSample
        {
            get;
            set;
        }

        protected C1XLBook C1XLbookSample
        {
            get;
            set;
        }

        protected Stream CsvSample
        {
            get;
            set;
        }

        protected Workbook WorkbookSample
        {
            get;
            set;
        }

        protected IEnumerable JsonData
        {
            get;
            set;
        }

        [TestInitialize]
        public void TestInitialize()
        {
            var c1XLbook = Utils.NewC1XLBook(TestContext.GetSamplesDir("10Rows.xlsx"));
            C1XLbookSample = c1XLbook;
            C1JsonHelper.Serialize(C1XLbookSample.ToWorkbook(), TestContext.GetSamplesDir("10RowsWorkbook.json"));
            WorkbookSample = C1JsonHelper.Deserialize<Workbook>(File.ReadAllText(TestContext.GetSamplesDir("10RowsWorkbook.json")));

            var stream = new MemoryStream();
            c1XLbook.Save(stream, FileFormat.Biff8);
            XlsSample = stream;

            var csvStream = new MemoryStream();
            c1XLbook.SaveEx(csvStream, "csv");
            CsvSample = csvStream;

            JsonData = C1JsonHelper.Deserialize(File.ReadAllText(TestContext.GetSamplesDir("JsonData.json"))) as IEnumerable;
            XmlDataStream = File.OpenRead(TestContext.GetSamplesDir("10RowsData.xml"));

            var xStream = new MemoryStream();
            c1XLbook.Save(xStream, FileFormat.OpenXml);
            XlsxSample = xStream;
        }

        [TestCleanup]
        public void TestCleanup()
        {
            if (XlsxSample != null)
            {
                XlsxSample.Dispose();
                XlsxSample = null;
            }

            if (C1XLbookSample != null)
            {
                C1XLbookSample.Dispose();
                C1XLbookSample = null;
            }

            if (XlsSample != null)
            {
                XlsSample.Dispose();
                XlsSample = null;
            }

            if (CsvSample != null)
            {
                CsvSample.Dispose();
                CsvSample = null;
            }

            if (XmlDataStream != null)
            {
                XmlDataStream.Dispose();
                XmlDataStream = null;
            }
        }
    }
}
