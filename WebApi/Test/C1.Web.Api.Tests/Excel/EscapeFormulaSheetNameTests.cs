﻿using System.Collections.Generic;
using System.Diagnostics;
using C1.Web.Api.Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace C1.Web.Api.Tests.Excel
{
    [TestClass]
    public class EscapeFormulaSheetNameTests
    {
        [TestMethod]
        public void TestEscapeFormulaSheetName()
        {
            foreach (var item in Data)
            {
                Assert.AreEqual(item.Value, ExcelMerger.EscapeFormulaSheetName(item.Key));
            }
        }

        private static readonly IEnumerable<KeyValuePair<string, string>> Data = new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string>(@"sheet1", @"sheet1"),
            new KeyValuePair<string, string>(@"sheet 1", @"'sheet 1'"),
            new KeyValuePair<string, string>(@"$1", @"'$1'"),
            new KeyValuePair<string, string>(@"sheet_1", @"sheet_1"),
            new KeyValuePair<string, string>(@"A1", @"'A1'"),
            new KeyValuePair<string, string>(@"a1048576", @"'a1048576'"),
            new KeyValuePair<string, string>(@"b1048577", @"b1048577"),
            new KeyValuePair<string, string>(@"a'a", @"'a''a'"),
            new KeyValuePair<string, string>(@"a""a", @"'a""a'"),
        };
    }
}
