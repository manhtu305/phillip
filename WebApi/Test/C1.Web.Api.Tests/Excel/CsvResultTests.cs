﻿using System.IO;
using C1.Web.Api.Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace C1.Web.Api.Tests.Excel
{
    [TestClass]
    public class CsvResultTests : ExcelBaseTests
    {
        [TestMethod]
        public void JsonToCsv()
        {
            var controller = new ExcelController();

            var request = new ExcelRequest
            {
                Type = ExportFileType.Csv,
                Workbook = WorkbookSample
            };

            var result = controller.Get(request) as FileResult;
            CheckCsv(result, "JsonToCsv");
        }

        [TestMethod]
        public void XlsxToCsv()
        {
            var controller = new ExcelController();
            var sampleXlsxStream = XlsxSample;
            sampleXlsxStream.Position = 0;

            var request = new ExcelRequest
            {
                Type = ExportFileType.Csv,
                WorkbookFile = new FormFile(sampleXlsxStream, "xlsx")
            };

            var result = controller.Get(request) as FileResult;
            CheckCsv(result, "XlsxToCsv");
        }

        [TestMethod]
        public void XlsToCsv()
        {
            var controller = new ExcelController();
            var sampleXlsStream = XlsSample;
            sampleXlsStream.Position = 0;

            var request = new ExcelRequest
            {
                Type = ExportFileType.Csv,
                WorkbookFile = new FormFile(sampleXlsStream, "xls")
            };

            var result = controller.Get(request) as FileResult;
            CheckCsv(result, "XlsToCsv");
        }

        private void CheckCsv(FileResult result, string name)
        {
            Assert.IsNotNull(result);

            var resultFile = Path.Combine(TestContext.TestResultsDirectory, name + ".csv");
            using (Stream stream = result.StreamGetter())
            using (var fileStream = File.Create(resultFile))
            {
                stream.Seek(0, SeekOrigin.Begin);
                stream.CopyTo(fileStream);
            }

            TestHelper.CompareFile(TestContext.GetExpectedResultsDir(name) + ".csv", resultFile);
        }

        [TestMethod]
        public void XmlDataToCsv()
        {
            var controller = new ExcelController();
            var request = new ExcelRequest
            {
                Type = ExportFileType.Csv,
                DataFile = new FormFile(XmlDataStream, "xml")
            };

            var result = controller.Get(request) as FileResult;
            CheckCsv(result, "XmlDataToCsv");
        }

        [TestMethod]
        public void JsonDataToCsv()
        {
            var controller = new ExcelController();
            var request = new ExcelRequest
            {
                Type = ExportFileType.Csv,
                Data = JsonData
            };

            var result = controller.Get(request) as FileResult;
            CheckCsv(result, "JsonDataToCsv");
        }
    }
}
