﻿using System.IO;
using C1.Web.Api.Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace C1.Web.Api.Tests.Excel
{
    [TestClass]
    public class XlsResultTests : ExcelBaseTests
    {
        [TestMethod]
        public void JsonToXls()
        {
            var controller = new ExcelController();

            var request = new ExcelRequest
            {
                Type = ExportFileType.Xls,
                Workbook = WorkbookSample
            };

            var result = controller.Get(request) as FileResult;
            CheckExcelFileResult(result, "JsonToXls");
        }

        [TestMethod]
        public void CsvToXls()
        {
            var controller = new ExcelController();
            var csvStream = CsvSample;
            csvStream.Position = 0;

            var request = new ExcelRequest
            {
                Type = ExportFileType.Xls,
                WorkbookFile = new FormFile(csvStream, "csv")
            };

            var result = controller.Get(request) as FileResult;
            CheckExcelFileResult(result, "CsvToXls");
        }

        [TestMethod]
        public void XlsxToXls()
        {
            var controller = new ExcelController();
            var sampleXlsxStream = XlsxSample;
            sampleXlsxStream.Position = 0;

            var request = new ExcelRequest
            {
                Type = ExportFileType.Xls,
                WorkbookFile = new FormFile(sampleXlsxStream, "xlsx")
            };

            var result = controller.Get(request) as FileResult;
            CheckExcelFileResult(result, "XlsxToXls");
        }

        private void CheckExcelFileResult(FileResult result, string testName)
        {
            Assert.IsNotNull(result);

            using (Stream stream = result.StreamGetter())
            {
                stream.Position = 0;
                using (var c1XLBook = Utils.NewC1XLBook(stream, "xls"))
                {
                    var workbook = c1XLBook.ToWorkbook();
                    CheckWorkbook(workbook, testName);
                }
            }
        }

        [TestMethod]
        public void DataToXls()
        {
            var controller = new ExcelController();
            var request = new ExcelRequest
            {
                Type = ExportFileType.Xls,
                Data = JsonData
            };

            var result = controller.Get(request) as FileResult;
            CheckExcelFileResult(result, "DataToXls");
        }

        [TestMethod]
        public void XmlDataToXls()
        {
            var controller = new ExcelController();
            var request = new ExcelRequest
            {
                Type = ExportFileType.Xls,
                DataFile = new FormFile(XmlDataStream, "xml")
            };

            var result = controller.Get(request) as FileResult;
            CheckExcelFileResult(result, "XmlDataToXls");
        }

        [TestMethod]
        public void JsonDataToXls()
        {
            var controller = new ExcelController();
            var request = new ExcelRequest
            {
                Type = ExportFileType.Xls,
                Data = JsonData
            };

            var result = controller.Get(request) as FileResult;
            CheckExcelFileResult(result, "JsonDataToXls");
        }
    }
}
