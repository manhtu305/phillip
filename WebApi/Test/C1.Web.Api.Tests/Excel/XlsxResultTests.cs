﻿using System.IO;
using C1.Web.Api.Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace C1.Web.Api.Tests.Excel
{
    [TestClass]
    public class XlsxResultTests : ExcelBaseTests
    {
        [TestMethod]
        public void JsonToXlsx()
        {
            var controller = new ExcelController();

            var request = new ExcelRequest
            {
                Type = ExportFileType.Xlsx,
                Workbook = WorkbookSample
            };

            var result = controller.Get(request) as FileResult;
            CheckExcelFileResult(result, "JsonToXlsx");
        }

        [TestMethod]
        public void CsvToXlsx()
        {
            var controller = new ExcelController();
            var csvStream = CsvSample;
            csvStream.Position = 0;

            var request = new ExcelRequest
            {
                Type = ExportFileType.Xlsx,
                WorkbookFile = new FormFile(csvStream, "csv")
            };

            var result = controller.Get(request) as FileResult;
            CheckExcelFileResult(result, "CsvToXlsx");
        }

        [TestMethod]
        public void XlsToXlsx()
        {
            var controller = new ExcelController();
            var sampleXlsStream = XlsSample;
            sampleXlsStream.Position = 0;

            var request = new ExcelRequest
            {
                Type = ExportFileType.Xlsx,
                WorkbookFile = new FormFile(sampleXlsStream, "xls")
            };

            var result = controller.Get(request) as FileResult;
            CheckExcelFileResult(result, "XlsToXlsx");
        }

        private void CheckExcelFileResult(FileResult result, string testName)
        {
            Assert.IsNotNull(result);

            using (Stream stream = result.StreamGetter())
            {
                stream.Position = 0;
                using (var c1XLBook = Utils.NewC1XLBook(stream, "xlsx"))
                {
                    var workbook = c1XLBook.ToWorkbook();
                    CheckWorkbook(workbook, testName);
                }
            }
        }

        [TestMethod]
        public void XmlDataToXlsx()
        {
            var controller = new ExcelController();
            var request = new ExcelRequest
            {
                Type = ExportFileType.Xlsx,
                DataFile = new FormFile(XmlDataStream, "xml")
            };

            var result = controller.Get(request) as FileResult;
            CheckExcelFileResult(result, "XmlDataToXlsx");
        }

        [TestMethod]
        public void JsonDataToXlsx()
        {
            var controller = new ExcelController();
            var request = new ExcelRequest
            {
                Type = ExportFileType.Xlsx,
                Data = JsonData
            };

            var result = controller.Get(request) as FileResult;
            CheckExcelFileResult(result, "JsonDataToXlsx");
        }

    }
}
