﻿using System.Net.Http;
using C1.Web.Api.Excel;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SHttpMethod = System.Net.Http.HttpMethod;

namespace C1.Web.Api.Tests.Excel
{
    [TestClass]
    public class RoutingTests : BaseRoutingTests
    {
        [TestMethod]
        public void GetRouting()
        {
            var request = new HttpRequestMessage(SHttpMethod.Get, "http://www.fakesite.com/api/excel?FileFormat=xls");

            var routeTester = new RouteTester(HttpConfig, request);

            Assert.AreEqual(typeof(ExcelController), routeTester.GetControllerType());
            Assert.AreEqual("Get", routeTester.GetActionName());
        }

        [TestMethod]
        public void PostRouting()
        {
            var request = new HttpRequestMessage(SHttpMethod.Post, "http://www.fakesite.com/api/excel");

            var routeTester = new RouteTester(HttpConfig, request);

            Assert.AreEqual(typeof(ExcelController), routeTester.GetControllerType());
            Assert.AreEqual("Post", routeTester.GetActionName());
        }

    }
}
