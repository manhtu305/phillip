﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyTested.AspNetCore.Mvc;
using C1.Web.Api;

namespace C1.AspNetCore.Api.Tests
{
    [TestClass]
    public class PathRouteTests
    {
        private const string _api = "http://www.fakesite.com/api/fakepath/";

        [TestInitialize]
        public void Startup()
        {
            MyMvc.StartsFrom<TestStartup>();
        }


        [TestMethod]
        public void TestGet()
        {
            const string path = "files/report.flxr/name";
            const string id = "a";
            const string value = "b";
            const string url = _api + path  + "/$fake/" + id + "/" + value;

            MyMvc.Routing().ShouldMap(request => request
                .WithLocation(url)
                .WithMethod("Get"))
                .To<FakePathController>(c=>c.Get(path, id, value));
        }

        [TestMethod]
        public void TestPost()
        {
            const string path = "files/report.flxr/name";
            const string id = "a";
            const string value = "b";
            const string url = _api + path + "/$fake/" + id + "/param/" + value;

            MyMvc.Routing().ShouldMap(request => request
                .WithLocation(url)
                .WithMethod("Post"))
                .To<FakePathController>(c => c.Post(path, id, value));
        }

        [TestMethod]
        public void TestOnlyPath()
        {
            const string path = "files/report.flxr/name";
            const string url = _api + path;

            MyMvc.Routing().ShouldMap(request => request
                .WithLocation(url)
                .WithMethod("Get"))
                .To<FakePathController>(c => c.GetPath(path));
        }
    }

    [Route("api/fakepath")]
    public class FakePathController: Controller
    {
        [HttpGet]
        [PathRoute("{*path}/$fake/{id}/{value}")]
        public string Get(string path, string id, string value)
        {
            return string.Format("path: {0}, id: {0}, value: {1}.", path, id, value);
        }

        [HttpPost]
        [PathRoute("{*path}/$fake/{id}/param/{value}")]
        public string Post(string path, string id, string value)
        {
            return string.Format("path: {0}, id: {1}, value: {2}.", path, id, value);
        }

        [HttpGet]
        [PathRoute("{*path}")]
        public string GetPath(string path)
        {
            return string.Format("path: {0}.", path);
        }
    }
}
