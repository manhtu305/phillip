﻿using System;
using System.IO;
using System.Collections;
using C1.DataBoundExcel;
using C1.Web.Api.Storage;
using C1.Web.Api.Data;
#if NETCORE
using GrapeCity.Documents.Excel;
#else 
using C1.C1Excel;
#endif
using C1.Util.Licensing;
#if NETCORE
#elif !ASPNETCORE
using IActionResult = System.Web.Http.IHttpActionResult;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace C1.Web.Api.Excel
{
  internal class ExcelGenerator : ExcelBase
  {

    private class GeneratorSource : ExportSource
    {
      public Workbook Workbook
      {
        get;
        set;
      }

      public FileStorage Excel
      {
        get;
        set;
      }

      public IEnumerable Data
      {
        get;
        set;
      }

      public FileStorage Template
      {
        get;
        set;
      }
    }

    private readonly GeneratorSource _source = new GeneratorSource();
    private readonly bool _needRenderEvalInfo;

    public ExcelGenerator(ExcelRequest re) : base(re)
    {
      _needRenderEvalInfo = LicenseHelper.NeedRenderEvalInfo<LicenseDetector>();
      InitSource(re);
    }

    private void InitSource(ExcelRequest request)
    {
      _source.FileName = request.FileName;
      _source.Type = request.Type;
      InitWorkbook(request);
      InitTemplate(request);
      InitData(request);
    }

    private void InitData(ExcelRequest request)
    {
      if (request.Data != null)
      {
        _source.Data = request.Data;
        return;
      }

      if (!string.IsNullOrEmpty(request.DataName))
      {
        _source.Data = DataProviderManager.Read(request.DataName, request.RequestParams);
        return;
      }

      if (request.DataFile != null || !string.IsNullOrEmpty(request.DataFileName))
      {
        var dataStream = request.DataFile != null ? request.DataFile.GetStream() :
            StorageProviderManager.GetFileStorage(request.DataFileName, request.RequestParams).Read();
        _source.Data = C1XmlHelper.GetData(dataStream);
      }
    }

    private void InitTemplate(ExcelRequest request)
    {
      if (request.TemplateFile != null || !string.IsNullOrEmpty(request.TemplateFileName))
      {
        Func<Stream> templateStreamGetter;
        string templateFormat;
        if (request.TemplateFile != null)
        {
          templateStreamGetter = () => request.TemplateFile.GetStream();
          templateFormat = request.TemplateFile.Extension;
        }
        else
        {
          var extensionIndex = request.TemplateFileName.LastIndexOf('.');
          templateFormat = extensionIndex > -1
              ? request.TemplateFileName.Substring(extensionIndex + 1)
              : "xls";
          templateStreamGetter = StorageProviderManager.GetFileStorage(request.TemplateFileName, request.RequestParams).Read;
        }

        _source.Template = new FileStorage(templateStreamGetter, templateFormat);
      }
    }

    private void InitWorkbook(ExcelRequest request)
    {
      if (request.Workbook != null)
      {
        _source.Workbook = request.Workbook;
        return;
      }

      if (request.WorkbookFile != null || !string.IsNullOrEmpty(request.WorkbookFileName))
      {
        Func<Stream> workbookStreamGetter;
        string fileFormat;
        if (request.WorkbookFile != null)
        {
          workbookStreamGetter = () => request.WorkbookFile.GetStream();
          fileFormat = request.WorkbookFile.Extension;
        }
        else
        {
          var extensionIndex = request.WorkbookFileName.LastIndexOf('.');
          fileFormat = extensionIndex > -1
              ? request.WorkbookFileName.Substring(extensionIndex + 1)
              : "xls";
          workbookStreamGetter = StorageProviderManager.GetFileStorage(request.WorkbookFileName, request.RequestParams).Read;
        }

        if (string.Equals(fileFormat, "json"))
        {
          _source.Workbook = C1JsonHelper.Deserialize<Workbook>(workbookStreamGetter());
        }
        else if (string.Equals(fileFormat, "xml"))
        {
          _source.Workbook = C1XmlHelper.Deserialize<Workbook>(workbookStreamGetter());
        }
        else
        {
          _source.Excel = new FileStorage(workbookStreamGetter, fileFormat);
        }
      }
    }
#if !NETCORE
    protected override C1XLBook GetC1Book()
    {
      C1XLBook c1Book;
      if (_source.Workbook != null)
      {
        c1Book = _source.Workbook.ToC1XLBook();
      }

      else if (_source.Excel != null)
      {
        c1Book = Utils.NewC1XLBook(_source.Excel.Read(), _source.Excel.Extension);
      }
      else
      {
        var dataBoundExcel = new DataBoundExcel.DataBoundExcel();
        c1Book = dataBoundExcel.ExportC1Excel(GetDataBoundSource());
      }

      if (c1Book.Sheets.Count == 0)
      {
        c1Book.Sheets.Add();
      }

      ExportC1ExcelEvalInfo(c1Book);
      return c1Book;
    }
#endif

#if NETCORE
    protected override IWorkbook GetWorkbook()
    {
      IWorkbook workbook = null;
      if (_source.Workbook != null)
      {
        workbook = _source.Workbook;
      }
      else if (_source.Excel != null)
      {
        IExcelHost excelHost = ExcelFactory.CreateExcelHost();
        workbook = excelHost.Read(_source.Excel.Read(), _source.Excel.Extension);
      }
      else
      {
        var dataBoundExcel = new DataBoundExcel.DataBoundExcel();
        workbook = dataBoundExcel.ExportWorkbook(GetDataBoundSource());
      }

      if (workbook.Worksheets.Count == 0)
      {
        workbook.Worksheets.Add();
      }

      ExportEvalInfo(workbook);
      return workbook;
    }

    private void ExportEvalInfo(IWorkbook workbook)
    {
      if (workbook != null && _needRenderEvalInfo)
      {
        workbook.WriteEvaluationInfo();
      }
    }

    private DataBoundExcelSource GetDataBoundSource()
    {
      return new DataBoundExcelSource(_source.Data, _source.Template);
    }

    protected override Stream GetFileStream()
    {
      var workbook = GetWorkbook();
      var outputStream = new MemoryStream();
      workbook.Save(outputStream);
      return outputStream;
    }

#else

    protected override Workbook GetWorkbook()
    {
      Workbook workbook;
      if (_source.Workbook != null)
      {
        workbook = _source.Workbook;
      }
      else if (_source.Excel != null)
      {
        IExcelHost excelHost = new C1ExcelHost();
        workbook = excelHost.Read(_source.Excel.Read(), _source.Excel.Extension);
      }
      else
      {
        var dataBoundExcel = new DataBoundExcel.DataBoundExcel();
        workbook = dataBoundExcel.ExportWorkbook(GetDataBoundSource());
      }

      ExportEvalInfo(workbook);
      return workbook;
    }

    private void ExportC1ExcelEvalInfo(C1XLBook workbook)
    {
      if (_needRenderEvalInfo)
      {
        Utils.WriteC1ExcelEvalInfo(workbook);
      }
    }
    private void ExportEvalInfo(Workbook workbook)
    {
      if (_needRenderEvalInfo)
      {
        Utils.WriteEvalInfo(workbook);
      }
    }

    private DataBoundExcelSource GetDataBoundSource()
    {
      return new DataBoundExcelSource(_source.Data, _source.Template);
    }
#endif

  }

}
