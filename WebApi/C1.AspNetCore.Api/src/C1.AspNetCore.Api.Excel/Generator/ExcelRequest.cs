﻿using System.Collections;
#if !NETCORE
using System.ComponentModel;
#else
using GrapeCity.Documents.Excel;
#endif

namespace C1.Web.Api.Excel
{
  /// <summary>
  /// The request data of generating the excel.
  /// </summary>
  public class ExcelRequest : ExportSource
  {
    /// <summary>
    /// Createa an ExcelRequest instance.
    /// </summary>
    public ExcelRequest()
    {
      Type = ExportFileType.Xlsx;
    }

    /// <summary>
    /// Gets or sets the data name that data provider recognizes.
    /// </summary>
    public string DataName
    {
      get;
      set;
    }

    /// <summary>
    /// Gets or sets the template file name that storage manager can recognize.
    /// </summary>
#if !NETCORE
    [EditorBrowsable(EditorBrowsableState.Never)] // will support it in feature
#endif
    public string TemplateFileName
    {
      get;
      set;
    }

    /// <summary>
    /// Gets or sets the template file which is posted from client side.
    /// </summary>
#if !NETCORE
    [EditorBrowsable(EditorBrowsableState.Never)] // will support it in feature
#endif
    public FormFile TemplateFile
    {
      get;
      set;
    }

    /// <summary>
    /// Gets or sets the xml data file name that storage manager can recognize.
    /// </summary>
    public string DataFileName
    {
      get;
      set;
    }

    /// <summary>
    /// Gets or sets the xml data file stream which is posted from client side.
    /// </summary>
    public FormFile DataFile
    {
      get;
      set;
    }


    /// <summary>
    /// Gets or sets the data which is posted from client side.
    /// </summary>
    public IEnumerable Data
    {
      get;
      set;
    }

    /// <summary>
    /// Gets or sets the workbook which is posted from client side.
    /// </summary>
    public Workbook Workbook
    {
      get;
      set;
    }

    /// <summary>
    /// Gets or sets the Excel file which is posted from client side.
    /// </summary>
    public FormFile WorkbookFile
    {
      get;
      set;
    }

    /// <summary>
    /// Gets or sets the workbook file name that storage manager can recognize.
    /// </summary>
    public string WorkbookFileName
    {
      get;
      set;
    }
  }
}
