﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
#if NETCORE
using GrapeCity.Documents.Excel;
#else 
using C1.C1Excel;
#endif
using C1.Web.Api.Storage;
using C1.Util.Licensing;

namespace C1.Web.Api.Excel
{
  internal class ExcelMerger : ExcelBase
  {
    private readonly bool _needRenderEvalInfo;
    private readonly HashSet<string> _existedSheetNames = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
    private readonly MergeRequest _mergeRequest;
    private readonly static Regex WordPattern = new Regex(@"\b\w+\b");
    private const string FormulaEscapeChar = "'";
    private const string EscapedFormulaEscapeChar = "''";
    private readonly static Regex CellRefNamePattern = new Regex(@"^[a-zA-Z](\d{1,7})$");

    public ExcelMerger(MergeRequest re) : base(re)
    {
      _needRenderEvalInfo = LicenseHelper.NeedRenderEvalInfo<LicenseDetector>();
      _mergeRequest = re;
    }
#if NETCORE
    private IWorkbook Merge()
    {
      if (_mergeRequest == null) return null;

      // Get Files To Merge
      var files = _GetFiles();

      // Convert from files to Workbook
      var sourceExcels = files.Select(f =>
      {
        IExcelHost host = ExcelFactory.CreateExcelHost();
        return host.Read(f.Read(), f.Extension);
      });

      IWorkbook workbook = null;
      
      foreach (var sourceExcel in sourceExcels)
      {
        if (sourceExcel == null) continue;

        if (workbook == null)
        {
          workbook = sourceExcel;
          continue;
        }
          
        foreach (var worksheet in sourceExcel.Worksheets)
        {
          worksheet.Copy(workbook);
        }
      }

      return workbook;
    }

    private IEnumerable<FileStorage> _GetFiles()
    {
      if (_mergeRequest.FilesToMerge != null && _mergeRequest.FilesToMerge.Length > 0)
      {
        return _mergeRequest.FilesToMerge.Select(f => new FileStorage(f.GetStream(), f.Extension));
      }

      if (_mergeRequest.FileNamesToMerge != null && _mergeRequest.FileNamesToMerge.Length > 0)
      {
        return _mergeRequest.FileNamesToMerge
          .Select(n =>
          {
            var storage = StorageProviderManager.GetFileStorage(n, _mergeRequest.RequestParams);
            return new FileStorage(storage.Read, Path.GetExtension(n));
          });
      }
      return new FileStorage[] { };
    }
    protected override IWorkbook GetWorkbook()
    {
      return Merge();
    }
#else
    private C1XLBook Merge()
    {
      var files = GetFiles();
      var sourceExcels = files.Select(f =>
      {
        var excel = Utils.NewC1XLBook(f.Read(), f.Extension);
        return excel;
      });

      var workbook = Utils.NewC1XLBook();
      workbook.Sheets.Clear();
      _existedSheetNames.Clear();
      foreach (var sourceExcel in sourceExcels)
      {
        var xlSheets = sourceExcel.Sheets.Cast<XLSheet>().ToList();
        var changedNames = ResolveSheetNames(xlSheets);
        ResolveHyperLinks(changedNames.Keys.ToList(), changedNames.Values.ToList(), xlSheets);
        ResolveFormulas(changedNames, xlSheets);

        foreach (var sourceSheet in xlSheets)
        {
          workbook.Sheets.Add(sourceSheet.Clone());
          _existedSheetNames.Add(sourceSheet.Name);
        }
      }

      if (workbook.Sheets.Count == 0)
      {
        workbook.Sheets.Add();
      }

      ExportEvalInfo(workbook);
      return workbook;
    }

    private static void ResolveFormulas(IDictionary<string, string> changedNames, List<XLSheet> sheets)
    {
      if (!changedNames.Any())
      {
        return;
      }

      const string sheetRefPattern = "({0})([!:])";
      const string sheetRefReplacement = "{0}$2";
      const string wordBoundary = "\\b";
      const string regexReplaceChar = "$";
      const string escapedRegexReplaceChar = "$$";
      var sheetRefReplacements = changedNames.Select(i =>
      {
        var oldName = Regex.Escape(EscapeFormulaSheetName(i.Key));
        var refPattern = oldName.StartsWith(FormulaEscapeChar) ? sheetRefPattern : wordBoundary + sheetRefPattern;
        var replacement = EscapeFormulaSheetName(i.Value).Replace(regexReplaceChar, escapedRegexReplaceChar);
        return new KeyValuePair<Regex, string>(
                  new Regex(string.Format(refPattern, oldName)),
                  string.Format(sheetRefReplacement, replacement)
                  );
      }).ToList();

      foreach (var sheet in sheets)
      {
        var rowCount = sheet.Rows.Count;
        var colCount = sheet.Columns.Count;
        for (var rowIndex = 0; rowIndex < rowCount; rowIndex++)
        {
          for (var colIndex = 0; colIndex < colCount; colIndex++)
          {
            var cell = sheet[rowIndex, colIndex];
            var formula = cell.Formula;
            if (!string.IsNullOrEmpty(formula))
            {
              cell.Formula = ResolveFormula(sheetRefReplacements, formula);
            }
          }
        }
      }
    }

    internal static string EscapeFormulaSheetName(string name)
    {
      Debug.Assert(!string.IsNullOrEmpty(name), "name should not be empty.");

      var matches = WordPattern.Matches(name);
      // name only contains word seperator chars, like: "$", "$$"
      // Or name contains more than one word
      // Or name contains one word with word seperator chars, like: "$sheet1", "sheet1$$"
      if (matches.Count != 1 || !string.Equals(matches[0].Value, name) || IsCellRefName(name))
      {
        return string.Format("{1}{0}{1}", name.Replace(FormulaEscapeChar, EscapedFormulaEscapeChar),
            FormulaEscapeChar);
      }

      return name;
    }

    //Check if the name is a cell reference name, like A1.
    private static bool IsCellRefName(string name)
    {
      var match = CellRefNamePattern.Match(name);
      //Group #0 is the name self, #1 is the number
      if (!match.Success || match.Groups.Count != 2)
      {
        return false;
      }

      var numberGroup = match.Groups[1];
      int columnNumber;
      if (!int.TryParse(numberGroup.Value, out columnNumber))
      {
        return false;
      }

      const int columnNumberMin = 1;
      const int columnNumberMax = 1048576;
      return columnNumber >= columnNumberMin && columnNumber <= columnNumberMax;
    }

    private static string ResolveFormula(IEnumerable<KeyValuePair<Regex, string>> replacements, string formula)
    {
      var result = formula;
      foreach (var replacement in replacements)
      {
        result = replacement.Key.Replace(result, replacement.Value);
      }

      return result;
    }


    /// <summary>
    /// Change the sheet name which is referenced by cell's hyperlink
    /// </summary>
    private static void ResolveHyperLinks(List<string> oldNames, List<string> newNames, List<XLSheet> sheets)
    {
      foreach (var sheet in sheets)
      {
        for (int rowIndex = 0; rowIndex < sheet.Rows.Count; rowIndex++)
        {
          for (int colIndex = 0; colIndex < sheet.Columns.Count; colIndex++)
          {
            var cell = sheet[rowIndex, colIndex];
            var link = cell.Hyperlink;
            if (!string.IsNullOrEmpty(link))
            {
              cell.Hyperlink = ReplaceStringWithChangedName(oldNames, newNames, link);
            }
          }
        }
      }
    }

    private static string ReplaceStringWithChangedName(List<string> oldNames, List<string> newNames, string originString)
    {
      const string cellReferenceOperator = "!";
      string result = originString;
      for (int index = 0; index < oldNames.Count; index++)
      {
        result = string.Empty;
        var oldName = oldNames[index] + cellReferenceOperator;
        var newName = newNames[index] + cellReferenceOperator;
        // check if sheet name is included in "".
        while (originString.Contains(oldName) && (originString.Substring(0, originString.IndexOf(oldName)).Split('\"').Length % 2 == 1))
        {
          result += originString.Substring(0, originString.IndexOf(oldName)) + newName;
          originString = originString.Substring(originString.IndexOf(oldName) + oldName.Length);
        }
        result += originString;
        originString = result;
      }
      return result;
    }

    private void ExportEvalInfo(C1XLBook workbook)
    {
      if (_needRenderEvalInfo)
      {
        Utils.WriteC1ExcelEvalInfo(workbook);
      }
    }

    private IEnumerable<FileStorage> GetFiles()
    {
      if (_mergeRequest.FilesToMerge != null && _mergeRequest.FilesToMerge.Any())
      {
        return _mergeRequest.FilesToMerge.Select(f => new FileStorage(f.GetStream(), f.Extension));
      }

      if (_mergeRequest.FileNamesToMerge != null && _mergeRequest.FileNamesToMerge.Any())
      {
        return _mergeRequest.FileNamesToMerge.Select(n =>
        {
          var storage = StorageProviderManager.GetFileStorage(n, _mergeRequest.RequestParams);
          return new FileStorage(storage.Read, Path.GetExtension(n));
        });
      }

      return new FileStorage[] { };
    }

    private IDictionary<string, string> ResolveSheetNames(List<XLSheet> currentSheetList)
    {
      var changed = true;
      var currentNames = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
      var changedNames = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
      foreach (var currentSheet in currentSheetList)
      {
        if (changed)
        {
          currentNames.Clear();
          foreach (var name in currentSheetList.Select(s => s.Name))
          {
            currentNames.Add(name);
          }
          changed = false;
        }

        var currentName = currentSheet.Name;
        var changedName = currentName;
        var suffix = 1;
        while (_existedSheetNames.Contains(changedName) || (changed && currentNames.Contains(changedName)))
        {
          changedName = string.Format("{0}_{1}", currentName, suffix++);
          changed = true;
        }

        if (!changed)
        {
          continue;
        }

        var sheet =
            currentSheetList.FirstOrDefault(s => string.Equals(currentName, s.Name, StringComparison.OrdinalIgnoreCase));
        if (sheet != null)
        {
          sheet.Name = changedName;
          changedNames.Add(currentName, changedName);
        }
      }

      return changedNames;
    }

    protected override C1XLBook GetC1Book()
    {
      return Merge();
    }

    protected override Workbook GetWorkbook()
    {
      return GetC1Book().ToWorkbook();
    }
#endif
  }
}
