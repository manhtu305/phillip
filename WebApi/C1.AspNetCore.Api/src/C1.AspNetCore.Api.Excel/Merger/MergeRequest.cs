﻿namespace C1.Web.Api.Excel
{
  /// <summary>
  /// The request data of merging excels.
  /// </summary>
  public class MergeRequest : ExportSource
  {
    /// <summary>
    /// Createa a MergeRequest instance.
    /// </summary>
    public MergeRequest()
    {
      Type = ExportFileType.Xlsx;
    }

    /// <summary>
    /// Gets or sets a collection of file name that are used to merge, and the names can be recognized by storage manager.
    /// </summary>
    public string[] FileNamesToMerge
    {
      get;
      set;
    }

    /// <summary>
    /// Gets or sets a collection of Excel files which is posted from client side for merging.
    /// </summary>
    public FormFile[] FilesToMerge
    {
      get;
      set;
    }
  }
}