﻿using System.IO;
#if NETCORE
using GrapeCity.Documents.Excel;
using Newtonsoft.Json;
#else 
using C1.C1Excel;
#endif
using System;
#if !ASPNETCORE && !NETCORE
using IActionResult = System.Web.Http.IHttpActionResult;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace C1.Web.Api.Excel
{
  internal abstract class ExcelBase : Processor
  {
    private readonly ExportSource _source;

    protected ExcelBase(ExportSource request)
    {
      _source = request;
    }

    protected override bool IsRequestObjectResult
    {
      get
      {
        return _source.Type == ExportFileType.Xml || _source.Type == ExportFileType.Json;
      }
    }

    protected override string FileName
    {
      get
      {
        return string.IsNullOrEmpty(_source.FileName) ? base.FileName : _source.FileName;
      }
    }

    protected override IActionResult GetFileResult()
    {
      // [301886] Save excel should execute outside FileResult.ExecuteResultAsync(), 
      // otherwise if export contains new System.Windows.Form.Control(), the process will suspend in Core 2.0.
      var stream = GetFileStream();
      return new FileResult(() => stream, FullFileName);
    }


#if NETCORE
    protected override Stream GetFileStream()
    {
      var workbook = GetWorkbook();
      var outputStream = new MemoryStream();
      workbook.Save(outputStream);
      return outputStream;
    }
#else
    protected override Stream GetFileStream()
    {
      var c1Book = GetC1Book();
      var outputStream = new MemoryStream();
      c1Book.SaveEx(outputStream, FileExtension);
      return outputStream;
    }
#endif


    protected override string FileExtension
    {
      get
      {
        return _source.Type.ToFileExtension().Substring(1);
      }
    }

#if NETCORE
    protected override IObjectFormatter ObjectFormatter
    {
      get
      {
        return string.Equals(FileExtension, "xml", StringComparison.OrdinalIgnoreCase)
            ? XmlFormatter.Instance
            : base.ObjectFormatter;
      }
    }

    protected override object GetObject()
    {
      if (_source.Type == ExportFileType.Json)
      {
        return JsonConvert.DeserializeObject(GetWorkbook().ToJson());
      }
      else if (_source.Type == ExportFileType.Xml)
      {
        return JsonConvert.DeserializeObject(GetWorkbook().ToJson());
      }
      return GetWorkbook();
    }

    protected abstract IWorkbook GetWorkbook();
#else
    protected override IObjectFormatter ObjectFormatter
    {
      get
      {
        return string.Equals(FileExtension, "xml", StringComparison.OrdinalIgnoreCase)
            ? XmlFormatter.Instance
            : base.ObjectFormatter;
      }
    }

    protected override object GetObject()
    {
      return GetWorkbook();
    }

    protected abstract C1XLBook GetC1Book();

    protected abstract Workbook GetWorkbook();
#endif
  }
}