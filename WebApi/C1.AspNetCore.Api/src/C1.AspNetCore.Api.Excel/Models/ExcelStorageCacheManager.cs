﻿using System.Collections.Specialized;
#if NETCORE
using GrapeCity.Documents.Excel;
#endif
namespace C1.Web.Api.Excel
{
#if NETCORE
  internal class ExcelStorageCacheManager : EditableStorageDocumentCacheManager<StorageExcel, IWorkbook>
#else
  internal class ExcelStorageCacheManager : EditableStorageDocumentCacheManager<StorageExcel, C1Excel.C1XLBook>
#endif  
  {
    public readonly static ExcelStorageCacheManager Instance = new ExcelStorageCacheManager();

    private ExcelStorageCacheManager() { }

    protected override StorageExcel CreateStorageDocument(string path, NameValueCollection args = null)
    {
      return new StorageExcel(path, args);
    }
  }
}
