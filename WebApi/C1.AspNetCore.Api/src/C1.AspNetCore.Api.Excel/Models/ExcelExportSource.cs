﻿#if NETCORE
using GrapeCity.Documents.Excel;
#endif

namespace C1.Web.Api.Excel
{
  /// <summary>
  /// The exchange data model used for sending Excel export requests.
  /// </summary>
  public class ExcelExportSource : ExportSource, IExporterSource
  {
    /// <summary>
    /// The workbook to export.
    /// </summary>
    public Workbook Workbook { get; set; }

    IExporter IExporterSource.CreateExporter()
    {
      return new ExcelExporter();
    }
  }
}