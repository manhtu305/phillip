﻿#if NETCORE
using GrapeCity.Documents.Excel;
#else 
using C1.C1Excel;
#endif
using C1.Web.Api.Storage;
using System.Collections.Specialized;
using System.IO;

namespace C1.Web.Api.Excel
{
#if NETCORE
  internal class StorageExcel : IEditableStorageDocument<IWorkbook>
#else
  internal class StorageExcel : IEditableStorageDocument<C1XLBook>
#endif
  {
    public StorageExcel(string path, NameValueCollection args = null)
    {
      Path = path.Replace('\\', '/').Trim();
      File = StorageProviderManager.GetFileStorage(path, args);
      Document = LoadDocument(File);
    }
#if NETCORE
    private IWorkbook LoadDocument(IFileStorage file)
    {
      using (var stream = file.Read())
      {
        return ExcelFactory.CreateExcelHost().Read(stream, System.IO.Path.GetExtension(file.Name));
      }
    }
#else
    private C1XLBook LoadDocument(IFileStorage file)
    {
      using (var stream = file.Read())
      {
        return Utils.NewC1XLBook(stream, System.IO.Path.GetExtension(file.Name));
      }
    }
#endif



    public string Path
    {
      get;
      private set;
    }

    public IFileStorage File
    {
      get;
      private set;
    }

#if NETCORE
    public IWorkbook Document
#else
    public C1XLBook Document
#endif
    {
      get;
      private set;
    }

    public bool IsDirty
    {
      get;
      set;
    }

    public void SaveChanges()
    {
      if (!IsDirty)
      {
        return;
      }

      lock (Document)
      {
        using (var stream = new MemoryStream())
        {
#if NETCORE
          Document.Save(stream);
#else
          Document.SaveEx(stream, System.IO.Path.GetExtension(File.Name));
#endif
          stream.Position = 0;
          File.Write(stream);
        }
      }
    }

    public void Dispose()
    {
      if (Document != null)
      {
        SaveChanges();
#if !NETCORE
        Document.Dispose();
#endif
        Document = null;
      }
    }
  }
}
