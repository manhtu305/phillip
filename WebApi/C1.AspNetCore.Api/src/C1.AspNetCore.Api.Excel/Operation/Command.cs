﻿namespace C1.Web.Api.Excel.Operation
{
  [SmartAssembly.Attributes.DoNotObfuscateType]
  internal enum Command
  {
    Split,
    Find,
    Replace,
    Rows,
    Columns
  }
}
