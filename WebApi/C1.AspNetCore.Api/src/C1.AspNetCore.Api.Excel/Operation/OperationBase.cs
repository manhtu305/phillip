﻿#if NETCORE
using GrapeCity.Documents.Excel;
#else 
using C1.C1Excel;
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace C1.Web.Api.Excel.Operation
{
  internal abstract class OperationBase
  {
    protected OperationContext Context { get; private set; }

    protected abstract bool IsReadOnly { get; }

    public OperationBase(OperationContext context)
    {
      Context = context;
      Context.ExcelDocument = ExcelStorageCacheManager.Instance.GetDocument(Context.ExcelPath, context.RequestParams);
    }

    protected T GetParameter<T>(string key)
    {
      var value = GetParameterValue(key);
      if (value != null && value.Length > 0 && !string.IsNullOrEmpty(value[0]))
      {
        return (T)ChangeType(value[0], typeof(T));
      }
      throw new ArgumentNullException(key);
    }

    protected T GetParameter<T>(string key, T defaultValue)
    {
      var value = GetParameterValue(key);
      if (value != null && value.Length > 0 && !string.IsNullOrEmpty(value[0]))
      {
        return (T)ChangeType(value[0], typeof(T));
      }
      return defaultValue;
    }

    protected T[] GetParameter<T>(string key, T[] defaultValue)
    {
      var value = GetParameterValue(key);
      if (value != null && value.Length > 0)
      {
        return value.Select(v => (T)ChangeType(v, typeof(T))).ToArray();
      }
      return defaultValue;
    }

    private string[] GetParameterValue(string key)
    {
      if (Context.Parameters == null)
      {
        return null;
      }
      return Context.Parameters.FirstOrDefault(p => p.Key.Equals(key, StringComparison.OrdinalIgnoreCase)).Value;
    }

    internal static object ChangeType(string value, Type type)
    {
      if (type == typeof(string))
      {
        return value;
      }

      var underlyingType = type;
      if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
      {
        if (string.IsNullOrEmpty(value))
        {
          return null;
        }
        underlyingType = Nullable.GetUnderlyingType(type);
      }

      if (underlyingType.IsEnum)
      {
        return Enum.Parse(underlyingType, value, true);
      }

      return Convert.ChangeType(value, underlyingType);
    }

    internal OperationResult GetResult()
    {
      var result = new OperationResult();

      try
      {
        lock (Context.Excel)
        {
          result.Data = Execute();
        }

        if (!IsReadOnly)
        {
          Context.ExcelDocument.IsDirty = true;
        }

        result.Success = true;
      }
      catch (Exception e)
      {
        result.Success = false;
        result.Message = e.Message;
      }

      return result;
    }

    protected abstract object Execute();
  }

  internal abstract class ReadOnlyOperationBase : OperationBase
  {
    public ReadOnlyOperationBase(OperationContext context)
        : base(context)
    {
    }

    protected override bool IsReadOnly
    {
      get
      {
        return true;
      }
    }
  }

  internal abstract class WritableOperationBase : OperationBase
  {
    public WritableOperationBase(OperationContext context)
        : base(context)
    {
    }

    protected override bool IsReadOnly
    {
      get
      {
        return false;
      }
    }
  }

  internal abstract class WritableSheetOperationBase : WritableOperationBase
  {
    public WritableSheetOperationBase(OperationContext context)
        : base(context)
    {
    }
#if NETCORE
    internal IWorksheet Sheet
    {
      get
      {
        if (!string.IsNullOrEmpty(Context.SheetName))
        {
          return Context.Excel.Worksheets[Context.SheetName];
        }
        else
        {
          throw new ArgumentNullException("sheetname");
        }
      }
    }
#else
    internal XLSheet Sheet
    {
      get
      {
        if (!string.IsNullOrEmpty(Context.SheetName))
        {
          return Context.Excel.Sheets[Context.SheetName];
        }
        else
        {
          throw new ArgumentNullException("sheetname");
        }
      }
    }
#endif


  }

  internal abstract class RowColOperationBase : WritableSheetOperationBase
  {
    internal int[] Indexes { get; private set; }

    internal RowColOperationBase(OperationContext context)
        : base(context)
    {
      Indexes = GetIndexes();
    }

    private int[] GetIndexes()
    {
      if (!string.IsNullOrEmpty(Context.Key))
      {
        var items = new List<int>();

        Context.Key.Split(',').Where(k => !string.IsNullOrEmpty(k.Trim())).ToList().ForEach(k =>
        {
          if (k.Contains('-'))
          {
            var range = k.Split('-').Where(i => !string.IsNullOrEmpty(i.Trim())).ToList();
            if (range.Count() == 2)
            {
              for (var i = int.Parse(range[0]); i <= int.Parse(range[1]); i++)
              {
                items.Add(i);
              }
              return;
            }
            throw new FormatException();
          }
          items.Add(int.Parse(k));
        });

        return items.ToArray();
      }

      throw new ArgumentNullException("indexes");
    }
  }

  internal abstract class RowColUpdateOperationBase<T> : RowColOperationBase
  {
    protected static readonly PropertyInfo[] Properties = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty);

    internal RowColUpdateOperationBase(OperationContext context)
        : base(context)
    {
    }

    protected virtual void SetPropertyValue(T obj, string propertyName, string value)
    {
      var propertyInfo = Properties.FirstOrDefault(p => p.Name.Equals(propertyName, StringComparison.OrdinalIgnoreCase));
      if (propertyInfo != null)
      {
        var propertyType = propertyInfo.PropertyType;
        if (propertyType.Name == "String" || propertyType.GetInterface("IEnumerable", false) == null)
        {
          propertyInfo.SetValue(obj, ChangeType(value, propertyType));
          return;
        }
      }
      throw new NotSupportedException();
    }

    protected override object Execute()
    {
      foreach (var item in GetItems(Indexes))
      {
        foreach (var param in Context.Parameters)
        {
          var stringValue = param.Value != null && param.Value.Length > 0 ? param.Value[0] : null;
          SetPropertyValue(item, param.Key, stringValue);
        }
      }
      return null;
    }

    protected abstract IEnumerable<T> GetItems(int[] indexes);
  }
}
