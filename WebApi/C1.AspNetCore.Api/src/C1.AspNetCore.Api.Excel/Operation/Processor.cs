﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using C1.Util.Licensing;
using C1.Web.Api.Localization;
using System.Collections.Specialized;
#if !ASPNETCORE && !NETCORE
using IActionResult = System.Web.Http.IHttpActionResult;
using Controller = System.Web.Http.ApiController;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace C1.Web.Api.Excel.Operation
{
  internal class Processor
  {
    private HttpMethod _method;
    private string _excelPath;
    private string _sheetName;
    private Command _command;
    private string _key;
    private IDictionary<string, string[]> _parameters;
    internal const char PATH_SEPARATOR = '/';
    private Lazy<NameValueCollection> _requestParams;

    private readonly static JsonSerializerSettings JsonSettings = new JsonSerializerSettings
    {
      ContractResolver = new CamelCasePropertyNamesContractResolver(),
      DefaultValueHandling = DefaultValueHandling.Include,
      NullValueHandling = NullValueHandling.Ignore,
      DateFormatString = "yyyy/MM/dd HH:mm:ss",
    };

    public Processor(HttpMethod method, string pathAndCmd, IDictionary<string, string[]> parameters)
    {
      _method = method;
      _parameters = parameters;
      SetPathAndCmd(pathAndCmd);
    }

    [SmartAssembly.Attributes.DoNotObfuscate]
    internal void SetRequestParamsGetter(Func<NameValueCollection> getter)
    {
      _requestParams = new Lazy<NameValueCollection>(getter);
    }

    private void SetPathAndCmd(string pathAndCmd)
    {
      var index = -1;
      var pathAndSheet = string.Empty;

      foreach (var cmd in (Command[])Enum.GetValues(typeof(Command)))
      {
        index = pathAndCmd.LastIndexOf(PATH_SEPARATOR + cmd.ToString(), StringComparison.OrdinalIgnoreCase);
        if (index != -1)
        {
          _command = cmd;
          pathAndSheet = pathAndCmd.Substring(0, index);
          break;
        }
      }

      if (index == -1)
      {
        throw new NotSupportedException();
      }

      string path;
      string sheetName;
      SeparatePathByLast(pathAndSheet, out path, out sheetName);

      var lowercasePath = path.ToLower();
      if (!string.IsNullOrEmpty(sheetName) && (lowercasePath.EndsWith(".xls") || lowercasePath.EndsWith(".xlsx") || lowercasePath.EndsWith(".csv")))
      {
        _excelPath = path;
        _sheetName = sheetName;
      }
      else
      {
        _excelPath = pathAndSheet;
      }

      var keyIndex = index + (PATH_SEPARATOR + _command.ToString()).Length;
      if (keyIndex < pathAndCmd.Length)
      {
        var keys = pathAndCmd.Substring(keyIndex);
        if (keys.StartsWith(PATH_SEPARATOR.ToString()))
        {
          _key = keys.Substring(1);
        }
      }
    }

    private static void SeparatePathByLast(string value, out string former, out string last)
    {
      value = value ?? string.Empty;
      var lastNodeStart = value.LastIndexOf(PATH_SEPARATOR);
      former = lastNodeStart == -1 ? value : value.Substring(0, lastNodeStart);
      last = (lastNodeStart == -1 || lastNodeStart == value.Length - 1) ?
          string.Empty : value.Substring(lastNodeStart + 1);
    }

    internal IActionResult GetResult(Controller controller)
    {
      if (LicenseHelper.IsValidLicense<LicenseDetector>())
      {
        return JsonFormatter.Json(CreateOperater().GetResult(), JsonSettings, controller);
      }
      return null;
    }

    private OperationBase CreateOperater()
    {
      var context = new OperationContext(_excelPath, _sheetName, _key, _parameters);

      switch (_command)
      {
        case Command.Split: return new Split(context);
        case Command.Find: return new Find(context, _requestParams);
        case Command.Replace: return new Replace(context, _requestParams);
        case Command.Rows:
          switch (_method)
          {
            case HttpMethod.Post: return new AddRows(context);
            case HttpMethod.Put: return new UpdateRows(context);
            case HttpMethod.Delete: return new RemoveRows(context);
          }
          break;
        case Command.Columns:
          switch (_method)
          {
            case HttpMethod.Post: return new AddColumns(context);
            case HttpMethod.Put: return new UpdateColumns(context);
            case HttpMethod.Delete: return new RemoveColumns(context);
          }
          break;
      }

      throw new NotSupportedException();
    }
  }
}
