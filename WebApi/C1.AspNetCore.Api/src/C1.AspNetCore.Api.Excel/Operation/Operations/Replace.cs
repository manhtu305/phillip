﻿using System;
using System.Collections.Specialized;
using System.Text.RegularExpressions;

namespace C1.Web.Api.Excel.Operation
{
  internal class ReplaceResult
  {
    public int ReplacedCellCount { get; set; }
  }

  internal class Replace : Find
  {
    private string NewText { get; set; }

    protected override bool IsReadOnly
    {
      get
      {
        return false;
      }
    }

    internal Replace(OperationContext context, Lazy<NameValueCollection> requestParameters)
        : base(context, requestParameters)
    {
      NewText = GetParameter("newtext", string.Empty);
      if (string.IsNullOrEmpty(NewText) && requestParameters != null)
      {
        var newtext = requestParameters.Value["newtext"];
        if (newtext != null)
        {
          NewText = newtext;
        }
      }
    }

    protected override object Execute()
    {
      var result = new ReplaceResult();

      if (string.IsNullOrEmpty(Text) || Text.Equals(NewText))
      {
        return result;
      }

      result.ReplacedCellCount = ReplaceText();
      return result;
    }

    private int ReplaceText()
    {
      var findResults = FindText();

      findResults.ForEach(r =>
      {
#if NETCORE
        var cell = Context.Excel.Worksheets[r.SheetName].Cells[r.RowIndex, r.ColumnIndex];
#else
        var cell = Context.Excel.Sheets[r.SheetName][r.RowIndex, r.ColumnIndex];
#endif
        var newCellText = Regex.Replace(cell.Text, Regex.Escape(Text), NewText, MatchCase ? RegexOptions.None : RegexOptions.IgnoreCase);
        try
        {
          cell.Value = ChangeType(newCellText, cell.Value.GetType());
        }
        catch
        {
          cell.Value = newCellText;
        }
      });

      return findResults.Count;
    }
  }
}
