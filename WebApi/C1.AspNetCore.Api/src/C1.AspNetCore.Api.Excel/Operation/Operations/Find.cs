﻿
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
#if NETCORE
using GrapeCity.Documents.Excel;
namespace C1.Web.Api.Excel.Operation
{
  internal class FindResult
  {
    public string SheetName { get; set; }
    public int RowIndex { get; set; }
    public int ColumnIndex { get; set; }
    public int StartIndex { get; set; }
  }

  internal class Find : ReadOnlyOperationBase
  {
    protected string Text { get; private set; }

    protected bool MatchCase { get; private set; }

    protected bool WholeCell { get; private set; }

    internal Find(OperationContext context, Lazy<NameValueCollection> requestParameters)
        : base(context)
    {
      Text = GetParameter<string>("text", string.Empty);
      if (string.IsNullOrEmpty(Text) && requestParameters != null)
      {
        var text = requestParameters.Value["text"];
        if (text != null)
        {
          Text = text;
        }
      }
      MatchCase = GetParameter("matchcase", false);
      WholeCell = GetParameter("wholecell", false);
    }

    protected override object Execute()
    {
      return FindText();
    }

    protected List<FindResult> FindText()
    {
      var findResults = new List<FindResult>();

      if (string.IsNullOrEmpty(Context.SheetName))
      {
        foreach (IWorksheet sheet in Context.Excel.Worksheets)
        {
          FindInSheet(sheet, findResults);
        }
      }
      else
      {
        FindInSheet(Context.Excel.Worksheets[Context.SheetName], findResults);
      }

      return findResults;
    }

    private void FindInSheet(IWorksheet sheet, List<FindResult> findResult)
    {
      IRange usedRange = sheet.UsedRange;
      for (int rowIndex = 0; rowIndex < usedRange.Rows.Count; rowIndex++)
      {
        for (int colIndex = 0; colIndex < usedRange.Columns.Count; colIndex++)
        {
          var startIndex = FindInCell(sheet.Cells[rowIndex, colIndex], MatchCase, WholeCell);
          if (startIndex != -1)
          {
            findResult.Add(new FindResult()
            {
              SheetName = sheet.Name,
              RowIndex = rowIndex,
              ColumnIndex = colIndex,
              StartIndex = startIndex
            });
          }
        }
      }
    }

    private int FindInCell(IRange cell, bool matchCase, bool wholeCell)
    {
      if (cell == null || cell.Value == null || string.IsNullOrEmpty(cell.Text))
      {
        return -1;
      }

      if (!wholeCell)
      {
        return matchCase ? cell.Text.IndexOf(Text) : cell.Text.IndexOf(Text, StringComparison.OrdinalIgnoreCase);
      }

      if (matchCase ? Text.Equals(cell.Text) : Text.Equals(cell.Text, StringComparison.OrdinalIgnoreCase))
      {
        return 0;
      }
      return -1;
    }
  }
}

#else
using C1.C1Excel;
namespace C1.Web.Api.Excel.Operation
{
  internal class FindResult
  {
    public string SheetName { get; set; }
    public int RowIndex { get; set; }
    public int ColumnIndex { get; set; }
    public int StartIndex { get; set; }
  }

  internal class Find : ReadOnlyOperationBase
  {
    protected string Text { get; private set; }

    protected bool MatchCase { get; private set; }

    protected bool WholeCell { get; private set; }

    internal Find(OperationContext context, Lazy<NameValueCollection> requestParameters)
        : base(context)
    {
      Text = GetParameter<string>("text", string.Empty);
      if (string.IsNullOrEmpty(Text) && requestParameters != null)
      {
        var text = requestParameters.Value["text"];
        if (text != null)
        {
          Text = text;
        }
      }
      MatchCase = GetParameter("matchcase", false);
      WholeCell = GetParameter("wholecell", false);
    }

    protected override object Execute()
    {
      return FindText();
    }

    protected List<FindResult> FindText()
    {
      var findResults = new List<FindResult>();

      if (string.IsNullOrEmpty(Context.SheetName))
      {
        foreach (XLSheet sheet in Context.Excel.Sheets)
        {
          FindInSheet(sheet, findResults);
        }
      }
      else
      {
        FindInSheet(Context.Excel.Sheets[Context.SheetName], findResults);
      }

      return findResults;
    }

    private void FindInSheet(XLSheet sheet, List<FindResult> findResult)
    {
      for (int rowIndex = 0; rowIndex < sheet.Rows.Count; rowIndex++)
      {
        for (int colIndex = 0; colIndex < sheet.Columns.Count; colIndex++)
        {
          var startIndex = FindInCell(sheet.GetCell(rowIndex, colIndex), MatchCase, WholeCell);
          if (startIndex != -1)
          {
            findResult.Add(new FindResult()
            {
              SheetName = sheet.Name,
              RowIndex = rowIndex,
              ColumnIndex = colIndex,
              StartIndex = startIndex
            });
          }
        }
      }
    }

    private int FindInCell(XLCell cell, bool matchCase, bool wholeCell)
    {
      if (cell == null || cell.Value == null || string.IsNullOrEmpty(cell.Text))
      {
        return -1;
      }

      if (!wholeCell)
      {
        return matchCase ? cell.Text.IndexOf(Text) : cell.Text.IndexOf(Text, StringComparison.OrdinalIgnoreCase);
      }

      if (matchCase ? Text.Equals(cell.Text) : Text.Equals(cell.Text, StringComparison.OrdinalIgnoreCase))
      {
        return 0;
      }
      return -1;
    }
  }
}
#endif