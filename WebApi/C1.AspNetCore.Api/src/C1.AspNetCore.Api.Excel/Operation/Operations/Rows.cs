﻿
using System.Linq;
using System.Collections.Generic;
using System;

#if NETCORE
using GrapeCity.Documents.Excel;
namespace C1.Web.Api.Excel.Operation
{
  internal class AddRows : RowColOperationBase
  {
    internal int RowCount { get; private set; }

    internal AddRows(OperationContext context)
        : base(context)
    {
      RowCount = GetParameter("count", 1);
    }

    protected override object Execute()
    {
      for (var i = 0; i < RowCount; i++)
      {
        Sheet.Rows[Indexes[0]].Insert();
      }
      return null;
    }
  }

  internal class RemoveRows : RowColOperationBase
  {
    internal RemoveRows(OperationContext context)
        : base(context)
    {
    }

    protected override object Execute()
    {
      foreach (var row in Indexes.Select(i => Sheet.Rows[i]).ToList())
      {
        row.Delete();
      }
      return null;
    }
  }

  internal class UpdateRows : RowColUpdateOperationBase<IRange>
  {
    internal UpdateRows(OperationContext context)
        : base(context)
    {
    }

    protected override void SetPropertyValue(IRange obj, string propertyName, string value)
    {
      switch (propertyName.ToLower())
      {
        case "visible":
        case "outlinelevel":
          base.SetPropertyValue(obj, propertyName, value);
          break;
        default:
          throw new NotSupportedException();
      }
    }

    protected override IEnumerable<IRange> GetItems(int[] indexes)
    {
      return indexes.Select(i => Sheet.Rows[i]).ToList();
    }
  }
}

#else

using C1.C1Excel;
namespace C1.Web.Api.Excel.Operation
{
  internal class AddRows : RowColOperationBase
  {
    internal int RowCount { get; private set; }

    internal AddRows(OperationContext context)
        : base(context)
    {
      RowCount = GetParameter("count", 1);
    }

    protected override object Execute()
    {
      for (var i = 0; i < RowCount; i++)
      {
        Sheet.Rows.Insert(Indexes[0]);
      }
      return null;
    }
  }

  internal class RemoveRows : RowColOperationBase
  {
    internal RemoveRows(OperationContext context)
        : base(context)
    {
    }

    protected override object Execute()
    {
      foreach (var row in Indexes.Select(i => Sheet.Rows[i]).ToList())
      {
        Sheet.Rows.Remove(row);
      }
      return null;
    }
  }

  internal class UpdateRows : RowColUpdateOperationBase<XLRow>
  {
    internal UpdateRows(OperationContext context)
        : base(context)
    {
    }

    protected override void SetPropertyValue(XLRow obj, string propertyName, string value)
    {
      switch (propertyName.ToLower())
      {
        case "visible":
        case "outlinelevel":
          base.SetPropertyValue(obj, propertyName, value); break;
        default:
          throw new NotSupportedException();
      }
    }

    protected override IEnumerable<XLRow> GetItems(int[] indexes)
    {
      return indexes.Select(i => Sheet.Rows[i]).ToList();
    }
  }
}
#endif
