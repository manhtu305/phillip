﻿using C1.Web.Api.Storage;
using C1.Web.Api.Localization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

#if NETCORE
using GrapeCity.Documents.Excel;
namespace C1.Web.Api.Excel.Operation
{
  internal class Split : ReadOnlyOperationBase
  {
    private static readonly Regex SeqNumRegex = new Regex(@"(?<=\()\d+(?=\)$)");

    private string DefaultExtension { get; set; }

    private string OutputPath { get; set; }

    private string[] DefaultOutputNames { get; set; }

    private string[] OutputNames { get; set; }

    private IWorksheet[] VisibleSheets
    {
      get
      {
        return Context.Excel.Worksheets.Where(s => s.Visible == Visibility.Visible).ToArray();
      }
    }

    internal Split(OperationContext context)
        : base(context)
    {
      var defaultOutputPath = Context.ExcelPath.Substring(0, Context.ExcelPath.LastIndexOf(Processor.PATH_SEPARATOR));
      OutputPath = GetParameter("outputpath", defaultOutputPath).TrimEnd(Processor.PATH_SEPARATOR);
      OutputNames = GetParameter("outputnames", new string[0]);
      DefaultExtension = Path.GetExtension(Context.ExcelPath);
      DefaultOutputNames = VisibleSheets.Select(s => GetSafeFileName(string.Format("{0}_{1}", Path.GetFileNameWithoutExtension(Context.ExcelPath), s.Name))).ToArray();
    }

    private string GetSafeFileName(string fileName)
    {
      var safeFileName = fileName;
      foreach (var chr in Path.GetInvalidFileNameChars())
      {
        safeFileName = safeFileName.Replace(chr, '_');
      }
      return safeFileName;
    }

    protected override object Execute()
    {
      var isCustomName = OutputNames != null && OutputNames.Length > 0;
      return SplitExcel(isCustomName ? OutputNames : DefaultOutputNames, isCustomName);
    }

    private List<string> SplitExcel(string[] names, bool isCustomName)
    {
      var outPutPaths = new List<string>();

      for (var i = 0; i < names.Length; i++)
      {
        if (i >= VisibleSheets.Length)
        {
          break;
        }

        var sheet = VisibleSheets[i];


        using (var stream = new MemoryStream())
        {
          string fileName;
          string fullPath;
          var extension = DefaultExtension;
          if (isCustomName)
          {
            fileName = names[i];
            fullPath = OutputPath + Processor.PATH_SEPARATOR + fileName;
            extension = Path.GetExtension(fileName);
            if (StorageProviderManager.GetFileStorage(fullPath, Context.RequestParams).Exists)
            {
              throw new Exception(string.Format(Resources.FileExists, fullPath));
            }
          }
          else
          {
            fileName = GetUniqueFileName(OutputPath, names[i], DefaultExtension);
            fullPath = OutputPath + Processor.PATH_SEPARATOR + fileName + DefaultExtension;
          }
          // FIX ME : for now (Jul 2019), GCExcel does not support this functionality
          //sheet.Save(stream, ExcelUtils.ConvertStringToExcelSaveFileFormat(extension));
          _SaveSheetToOutputStream(stream, sheet);
          stream.Position = 0;
          StorageProviderManager.GetFileStorage(fullPath, Context.RequestParams).Write(stream);

          outPutPaths.Add(fullPath);
        }
      }


      return outPutPaths;
    }

    private void _SaveSheetToOutputStream(Stream stream, IWorksheet sheet)
    {
      if (sheet == null) return;
      IWorkbook workbook = ExcelFactory.CreateExcelHost().NewWorkbook();
      // Copy theme from origin workbook to desination workbook
      if (sheet.Workbook != null) workbook.Theme = sheet.Workbook.Theme;
      // Rename this worksheet to avoid duplicate sheet name;
      workbook.Worksheets[0].Name = sheet.Name + "_temp";
      sheet.CopyBefore(workbook.Worksheets[0]);
      // Remove default worksheet added when creating workbook      
      workbook.Worksheets[1].Delete();
      workbook.Save(stream);
    }


    private string GetUniqueFileName(string path, string fileName, string extension)
    {
      var uniqueFileName = fileName;

      while (FileExists(path + Processor.PATH_SEPARATOR + uniqueFileName + extension))
      {
        uniqueFileName = GetNewName(uniqueFileName);
      }

      return uniqueFileName;
    }

    private bool FileExists(string fullName)
    {
      return StorageProviderManager.GetFileStorage(fullName, Context.RequestParams).Exists;
    }

    private string GetNewName(string oldName)
    {
      if (SeqNumRegex.IsMatch(oldName))
      {
        return SeqNumRegex.Replace(oldName, m => (int.Parse(m.Value) + 1).ToString());
      }

      return oldName + "(1)";
    }
  }
}

#else


using C1.C1Excel;
namespace C1.Web.Api.Excel.Operation
{
  internal class Split : ReadOnlyOperationBase
  {
    private static readonly Regex SeqNumRegex = new Regex(@"(?<=\()\d+(?=\)$)");

    private string DefaultExtension { get; set; }

    private string OutputPath { get; set; }

    private string[] DefaultOutputNames { get; set; }

    private string[] OutputNames { get; set; }

    private XLSheet[] VisibleSheets
    {
      get
      {
        return Context.Excel.Sheets.Cast<XLSheet>().Where(s => s.Visible).ToArray();
      }
    }

    internal Split(OperationContext context)
        : base(context)
    {
      var defaultOutputPath = Context.ExcelPath.Substring(0, Context.ExcelPath.LastIndexOf(Processor.PATH_SEPARATOR));
      OutputPath = GetParameter("outputpath", defaultOutputPath).TrimEnd(Processor.PATH_SEPARATOR);
      OutputNames = GetParameter("outputnames", new string[0]);
      DefaultExtension = Path.GetExtension(Context.ExcelPath);
      DefaultOutputNames = VisibleSheets.Select(s => GetSafeFileName(string.Format("{0}_{1}", Path.GetFileNameWithoutExtension(Context.ExcelPath), s.Name))).ToArray();
    }

    private string GetSafeFileName(string fileName)
    {
      var safeFileName = fileName;
      foreach (var chr in Path.GetInvalidFileNameChars())
      {
        safeFileName = safeFileName.Replace(chr, '_');
      }
      return safeFileName;
    }

    protected override object Execute()
    {
      var isCustomName = OutputNames != null && OutputNames.Length > 0;
      return SplitExcel(isCustomName ? OutputNames : DefaultOutputNames, isCustomName);
    }

    private List<string> SplitExcel(string[] names, bool isCustomName)
    {
      var outPutPaths = new List<string>();

      for (var i = 0; i < names.Length; i++)
      {
        if (i >= VisibleSheets.Length)
        {
          break;
        }

        var sheet = VisibleSheets[i];
        using (var excel = Utils.NewC1XLBook())
        {
          excel.Sheets.Clear();
          excel.Sheets.Add(sheet.Clone());

          using (var stream = new MemoryStream())
          {
            string fileName;
            string fullPath;
            var extension = DefaultExtension;
            if (isCustomName)
            {
              fileName = names[i];
              fullPath = OutputPath + Processor.PATH_SEPARATOR + fileName;
              extension = Path.GetExtension(fileName);
              if (StorageProviderManager.GetFileStorage(fullPath, Context.RequestParams).Exists)
              {
                throw new Exception(string.Format(Resources.FileExists, fullPath));
              }
            }
            else
            {
              fileName = GetUniqueFileName(OutputPath, names[i], DefaultExtension);
              fullPath = OutputPath + Processor.PATH_SEPARATOR + fileName + DefaultExtension;
            }

            excel.SaveEx(stream, extension);
            stream.Position = 0;
            StorageProviderManager.GetFileStorage(fullPath, Context.RequestParams).Write(stream);
            outPutPaths.Add(fullPath);
          }
        }
      }

      return outPutPaths;
    }

    private string GetUniqueFileName(string path, string fileName, string extension)
    {
      var uniqueFileName = fileName;

      while (FileExists(path + Processor.PATH_SEPARATOR + uniqueFileName + extension))
      {
        uniqueFileName = GetNewName(uniqueFileName);
      }

      return uniqueFileName;
    }

    private bool FileExists(string fullName)
    {
      return StorageProviderManager.GetFileStorage(fullName, Context.RequestParams).Exists;
    }

    private string GetNewName(string oldName)
    {
      if (SeqNumRegex.IsMatch(oldName))
      {
        return SeqNumRegex.Replace(oldName, m => (int.Parse(m.Value) + 1).ToString());
      }

      return oldName + "(1)";
    }
  }
}
#endif
