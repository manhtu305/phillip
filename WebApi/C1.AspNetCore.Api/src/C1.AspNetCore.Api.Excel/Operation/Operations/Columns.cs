﻿using System;
using System.Collections.Generic;
using System.Linq;
#if NETCORE
using GrapeCity.Documents.Excel;
namespace C1.Web.Api.Excel.Operation
{
  internal class AddColumns : RowColOperationBase
  {
    internal int ColumnCount { get; private set; }

    internal AddColumns(OperationContext context)
        : base(context)
    {
      ColumnCount = GetParameter("count", 1);
    }

    protected override object Execute()
    {
      for (var i = 0; i < ColumnCount; i++)
      {
        Sheet.Columns[Indexes[0]].Insert();
      }
      return null;
    }
  }

  internal class RemoveColumns : RowColOperationBase
  {
    internal RemoveColumns(OperationContext context)
        : base(context)
    {
    }

    protected override object Execute()
    {
      foreach (var col in Indexes.Select(i => Sheet.Columns[i]).ToList())
      {
        col.Delete();
      }
      return null;
    }
  }

  internal class UpdateColumns : RowColUpdateOperationBase<IRange>
  {
    internal UpdateColumns(OperationContext context)
        : base(context)
    {
    }

    protected override void SetPropertyValue(IRange obj, string propertyName, string value)
    {
      switch (propertyName.ToLower())
      {
        case "value":
        case "visible":
        case "outlinelevel":
          base.SetPropertyValue(obj, propertyName, value); break;
        default:
          throw new NotSupportedException();
      }
    }

    protected override IEnumerable<IRange> GetItems(int[] indexes)
    {
      return indexes.Select(i => Sheet.Columns[i]).ToList();
    }
  }
}

#else 
using C1.C1Excel;
namespace C1.Web.Api.Excel.Operation
{
  internal class AddColumns : RowColOperationBase
  {
    internal int ColumnCount { get; private set; }

    internal AddColumns(OperationContext context)
        : base(context)
    {
      ColumnCount = GetParameter("count", 1);
    }

    protected override object Execute()
    {
      for (var i = 0; i < ColumnCount; i++)
      {
        Sheet.Columns.Insert(Indexes[0]);
      }
      return null;
    }
  }

  internal class RemoveColumns : RowColOperationBase
  {
    internal RemoveColumns(OperationContext context)
        : base(context)
    {
    }

    protected override object Execute()
    {
      foreach (var col in Indexes.Select(i => Sheet.Columns[i]).ToList())
      {
        Sheet.Columns.Remove(col);
      }
      return null;
    }
  }

  internal class UpdateColumns : RowColUpdateOperationBase<XLColumn>
  {
    internal UpdateColumns(OperationContext context)
        : base(context)
    {
    }

    protected override void SetPropertyValue(XLColumn obj, string propertyName, string value)
    {
      switch (propertyName.ToLower())
      {
        case "visible":
        case "outlinelevel":
          base.SetPropertyValue(obj, propertyName, value); break;
        default:
          throw new NotSupportedException();
      }
    }

    protected override IEnumerable<XLColumn> GetItems(int[] indexes)
    {
      return indexes.Select(i => Sheet.Columns[i]).ToList();
    }
  }
}
#endif
