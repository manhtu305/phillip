﻿#if NETCORE
using GrapeCity.Documents.Excel;
#else 
using C1.C1Excel;
#endif
using System;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace C1.Web.Api.Excel.Operation
{
  internal class OperationContext
  {
    private readonly Lazy<NameValueCollection> _requestParams;
    public StorageExcel ExcelDocument { get; set; }
#if NETCORE
    public IWorkbook Excel { get { return ExcelDocument == null ? null : ExcelDocument.Document; } }
#else
    public C1XLBook Excel { get { return ExcelDocument == null ? null : ExcelDocument.Document; } }
#endif

    public string ExcelPath { get; private set; }
    public string SheetName { get; private set; }
    public string Key { get; private set; }
    public IDictionary<string, string[]> Parameters { get; private set; }
    public NameValueCollection RequestParams
    {
      get
      {
        return _requestParams.Value;
      }
    }

    public OperationContext(string excelPath, string sheetName, string key, IDictionary<string, string[]> parameters)
    {
      ExcelPath = excelPath;
      SheetName = sheetName;
      Key = key;
      Parameters = parameters;
      _requestParams = new Lazy<NameValueCollection>(() =>
      {
        if (Parameters == null)
        {
          return null;
        }

        var rawParams = RequestParamsExtensions.Create();
        foreach (var item in Parameters)
        {
          rawParams.Add(Parameters);
        }

        return rawParams;
      });
    }
  }
}
