﻿using C1.Web.Api.Excel;
using System;
using System.Collections.Generic;
using System.Text;
#if NETCORE
using GrapeCity.Documents.Excel;
#endif

namespace C1.Web.Api.Excel
{
  internal class ExcelFactory
  {
    public static IExcelHost CreateExcelHost()
    {
      IExcelHost host = null;
#if NETCORE
      host = new GcExcelHost();      
#else
      host = new C1ExcelHost();
#endif
      return host;
    }
  }
}
