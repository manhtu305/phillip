﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using C1.Util.Licensing;
using C1.Web.Api.Localization;

#if ASPNETCORE
#elif NETCORE
using GrapeCity.Documents.Excel;
#else
using System.ComponentModel;
#endif

namespace C1.Web.Api.Excel
{
  /// <summary>
  /// Exporter that is responsible for converting an ExcelExportSource to an Excel file.
  /// </summary>
#if !ASPNETCORE && !NETCORE
    [LicenseProvider] //This option can be removed. Just for 2015v2 compatibility.
#endif
  public class ExcelExporter : IExporter<ExcelExportSource>
  {
    private readonly bool _needRenderEvalInfo;

    /// <summary>
    /// Create an ExcelExporter instance.
    /// </summary>
    public ExcelExporter()
    {
      _needRenderEvalInfo = LicenseHelper.NeedRenderEvalInfo<LicenseDetector>();
    }

    /// <summary>
    /// Execute the export.
    /// </summary>
    /// <param name="exportSource">The export options.</param>
    /// <param name="outputStream">The export result.</param>
    /// <returns>The task.</returns>
    public Task ExportAsync(ExcelExportSource exportSource, Stream outputStream)
    {
      ExportEvalInfo(exportSource.Workbook);
      IExcelHost host = ExcelFactory.CreateExcelHost();
      host.Write(exportSource.Workbook, outputStream, exportSource.Type.ToFileExtension());
      return Task.FromResult(0);
    }

    Task IExporter.ExportAsync(object exportSource, Stream outputStream)
    {
      return this.ExportAsync((ExcelExportSource)exportSource, outputStream);
    }

    private void ExportEvalInfo(Workbook workbook)
    {
      if (_needRenderEvalInfo)
      {
#if NETCORE
        workbook.WriteEvaluationInfo();
#else
        Utils.WriteEvalInfo(workbook);
#endif
      }
    }
  }
}