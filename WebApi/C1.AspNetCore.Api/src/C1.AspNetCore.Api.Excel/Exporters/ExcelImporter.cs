﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using C1.Util.Licensing;
using C1.Web.Api.Localization;

#if ASPNETCORE
#elif NETCORE
using GrapeCity.Documents.Excel;
#else
using System.ComponentModel;
#endif

namespace C1.Web.Api.Excel
{
  /// <summary>
  /// Importer that is responsible for converting an uploaded file to an Excel JSON.
  /// </summary>
#if !ASPNETCORE && !NETCORE
    [LicenseProvider] //This option can be removed. Just for 2015v2 compatibility.
#endif

#if NETCORE
  public class ExcelImporter : IImporter<ImportSource, IWorkbook>
#else
  public class ExcelImporter : IImporter<ImportSource, Workbook>
#endif
  {
    private readonly bool _needRenderEvalInfo;

    /// <summary>
    /// Create an ExcelImporter instance.
    /// </summary>
    public ExcelImporter()
    {
      _needRenderEvalInfo = LicenseHelper.NeedRenderEvalInfo<LicenseDetector>();
    }

    /// <summary>
    /// Execute the import.
    /// </summary>
    /// <param name="source">The import options.</param>
    /// <returns>The task.</returns>
#if NETCORE
    public Task<IWorkbook> ImportAsync(ImportSource source)
#else
    public Task<Workbook> ImportAsync(ImportSource source)
#endif
    {
      var extentsion = source.FileName.Trim('\\', '\"').Split('.').Last();
      IExcelHost host = ExcelFactory.CreateExcelHost();
      var stream = source.GetFileStream();
      var workbook = host.Read(stream, extentsion);
      ExportEvalInfo(workbook);
      return Task.FromResult(workbook);
    }

#if NETCORE
    private void ExportEvalInfo(IWorkbook workbook)
#else
    private void ExportEvalInfo(Workbook workbook)
#endif

    {
      if (_needRenderEvalInfo)
      {
#if NETCORE
        workbook.WriteEvaluationInfo();
#else
        Utils.WriteEvalInfo(workbook);
#endif
      }
    }
  }
}