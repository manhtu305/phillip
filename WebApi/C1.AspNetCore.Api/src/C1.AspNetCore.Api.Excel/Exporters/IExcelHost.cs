﻿using System.Threading.Tasks;
using System.IO;
#if NETCORE
using GrapeCity.Documents.Excel;
#endif

namespace C1.Web.Api.Excel
{
  /// <summary>
  /// Interface for converting Excel file stream to JSON format.
  /// </summary>
  public interface IExcelHost
  {
#if NETCORE
    /// <summary>
    /// Read data from Excel file stream.
    /// </summary>
    /// <param name="file">The file.</param>
    /// <param name="fileType">The file extension.</param>
    /// <returns>A Workbook object.</returns>
    IWorkbook Read(Stream file, string fileType);
    /// <summary>
    /// Write data to Excel file stream.
    /// </summary>
    /// <param name="workbook">A Workbook object.</param>
    /// <param name="file">The file.</param>
    /// <param name="fileType">The file extension.</param>
    void Write(IWorkbook workbook, Stream file, string fileType);
    /// <summary>
    /// New empty workbook
    /// </summary>
    /// <returns>A Workbook object.</returns>
    IWorkbook NewWorkbook();
#else

    /// <summary>
    /// Read data from Excel file stream.
    /// </summary>
    /// <param name="file">The file.</param>
    /// <param name="fileType">The file extension.</param>
    /// <returns>A Workbook object.</returns>
    Workbook Read(Stream file, string fileType);
    /// <summary>
    /// Write data to Excel file stream.
    /// </summary>
    /// <param name="workbook">A Workbook object.</param>
    /// <param name="file">The file.</param>
    /// <param name="fileType">The file extension.</param>
    void Write(Workbook workbook, Stream file, string fileType);
#endif
  }
}
