﻿using System.Linq;
using C1.C1Excel;
using C1.Web.Api.Localization;
using System;
using System.IO;

namespace C1.Web.Api.Excel
{
    internal static class Utils
    {
        public static void WriteC1ExcelEvalInfo(C1XLBook workbook)
        {
            var evaluationMessage = Resources.EvaluationMessage;
            foreach (var c1Sheet in workbook.Sheets.Cast<XLSheet>())
            {
                var count = c1Sheet.Rows.Cast<XLRow>().Count();
                var newCell = c1Sheet[count, 0];
                newCell.Value = evaluationMessage;
                newCell.Style = new XLStyle(workbook) { AlignHorz = XLAlignHorzEnum.Left };
            }
        }

        public static void WriteEvalInfo(Workbook workbook)
        {
            var evaluationMessage = Resources.EvaluationMessage;
            foreach (var sheet in workbook.Sheets)
            {
                var row = new Row();
                var cell = new Cell {Value = evaluationMessage, Style = new Style {HAlign = HAlignType.Left}};
                row.Cells.Add(cell);
                sheet.Rows.Add(row);
            }
        }

        public static C1XLBook NewC1XLBook(string filePath)
        {
            using (var file = File.OpenRead(filePath))
            {
                var extension = Path.GetExtension(filePath);
                return NewC1XLBook(file, extension);
            }
        }

        public static C1XLBook NewC1XLBook(Stream file = null, string fileType = null)
        {
            var c1Book = new C1XLBook();
            if (file != null)
            {
                var format = GetC1FileFormat(fileType);
                if (format == FileFormat.Csv)
                {
                    c1Book.Sheets[0].LoadCsv(file);
                }
                else
                {
                    c1Book.Load(file, format);
                }
            }

            return c1Book;
        }

        public static FileFormat GetC1FileFormat(string extension)
        {
            extension = extension.Trim('.');
            FileFormat format;
            if (string.Compare(extension, "xlsx", StringComparison.OrdinalIgnoreCase) == 0)
            {
                format = FileFormat.OpenXml;
            }
            else if (string.Compare(extension, "csv", StringComparison.OrdinalIgnoreCase) == 0)
            {
                format = FileFormat.Csv;
            }
            else
            {
                format = FileFormat.Biff8;
            }
            return format;
        }

        public static void SaveEx(this C1XLBook c1Book, Stream outputStream, string extension)
        {
            var format = GetC1FileFormat(extension);
            if (format != FileFormat.Csv)
            {
                c1Book.Save(outputStream, format);
                return;
            }

            // #119940 Cannot save csv to stream
            var path = GetTempFilePath();
            c1Book.Sheets[0].SaveCsv(path);
            var bytes = File.ReadAllBytes(path);
            outputStream.Write(bytes, 0, bytes.Length);
            try
            {
                File.Delete(path);
            }
            catch { }
        }

        private static string GetTempFilePath()
        {
            return Path.Combine(Path.GetTempPath(), "c1_" + Path.GetRandomFileName() + ".csv");
        }
    }
}
