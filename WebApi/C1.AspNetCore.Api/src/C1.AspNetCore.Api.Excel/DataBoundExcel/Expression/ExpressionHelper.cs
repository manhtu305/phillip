﻿using System.Collections.Generic;

namespace C1.DataBoundExcel.Expression
{
    internal static class ExpressionHelper
    {
        private static List<ExpressionProcessor> _expressionProcessors;

        private static List<ExpressionProcessor> ExpressionProcessors
        {
            get
            {
                if (_expressionProcessors == null)
                {
                    _expressionProcessors = new List<ExpressionProcessor>();
                    _expressionProcessors.Add(new TextExpressionProcessor());
                    _expressionProcessors.Add(new FormulaExpressionProcessor());
                }
                return _expressionProcessors;
            }
        }

        internal static bool TryParse(string text, out ExpressionProcessor expression)
        {
            if (string.IsNullOrEmpty(text))
            {
                expression = null;
                return false;
            }

            expression = ExpressionProcessors.Find(p => p.IsMatch(text));
            return expression != null;
        }
    }
}
