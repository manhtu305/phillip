﻿using C1.DataBoundExcel.DataContext;

namespace C1.DataBoundExcel.Expression
{
    internal class FormulaExpressionProcessor : ExpressionProcessor
    {
        private const string MATCH_PATTERN = @"\(\([^\(^\)]+\)\)";

        protected override string MatchPattern
        {
            get
            {
                return MATCH_PATTERN;
            }
        }

        protected override string GetFieldName(string text)
        {
            return text.Trim('(', ')');
        }

        internal override string GetValue(string fieldName, BaseDataContext dataContext)
        {
            return base.GetValue(fieldName, dataContext).Replace("\"", "\"\"");
        }
    }
}
