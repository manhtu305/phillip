﻿using C1.DataBoundExcel.DataContext;
using System.Text.RegularExpressions;

namespace C1.DataBoundExcel.Expression
{
    internal class TextExpressionProcessor : ExpressionProcessor
    {
        private const string MATCH_PATTERN = @"\[[^\[^\]]+\]";

        protected override string MatchPattern
        {
            get
            {
                return MATCH_PATTERN;
            }
        }

        protected override string GetFieldName(string text)
        {
            return text.Trim('[', ']');
        }

        internal override object Execute(string text, BaseDataContext dataContext)
        {
            if (Regex.IsMatch(text, string.Format("^{0}$", MATCH_PATTERN)))
            {
                var fieldName = GetFieldName(text);
                return dataContext.GetValue(fieldName);
            }

            return base.Execute(text, dataContext);
        }
    }
}
