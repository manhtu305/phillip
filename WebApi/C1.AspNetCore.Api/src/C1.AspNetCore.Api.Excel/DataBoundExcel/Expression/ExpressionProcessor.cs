﻿using C1.DataBoundExcel.DataContext;
using System.Text.RegularExpressions;

namespace C1.DataBoundExcel.Expression
{
    internal abstract class ExpressionProcessor
    {
        protected abstract string MatchPattern { get; }

        protected abstract string GetFieldName(string text);

        internal virtual bool IsMatch(string text)
        {
            return Regex.IsMatch(text, MatchPattern);
        }

        internal virtual string GetValue(string fieldName, BaseDataContext dataContext)
        {
            var val = dataContext.GetValue(fieldName);
            return val == null ? string.Empty : val.ToString();
        }

        internal virtual object Execute(string text, BaseDataContext dataContext)
        {
            if (IsMatch(text))
            {
                return Regex.Replace(text, MatchPattern, m =>
                {
                    var fieldName = GetFieldName(m.Value);
                    return GetValue(fieldName, dataContext);
                });
            }

            return text;
        }
    }
}
