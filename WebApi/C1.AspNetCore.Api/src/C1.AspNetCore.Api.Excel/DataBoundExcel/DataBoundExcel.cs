﻿#if NETCORE
using GrapeCity.Documents.Excel;
#else 
using C1.C1Excel;
#endif
using C1.DataBoundExcel.DataContext;
using C1.DataBoundExcel.Template;
using C1.Web.Api;
using C1.Web.Api.Excel;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace C1.DataBoundExcel
{
  internal class DataBoundExcel : IExporter<DataBoundExcelSource>
  {
    Task IExporter.ExportAsync(object source, Stream outputStream)
    {
      return ExportAsync((DataBoundExcelSource)source, outputStream);
    }

    public Task ExportAsync(DataBoundExcelSource source, Stream outputStream)
    {
      var excel = ExportC1Excel(source);
      excel.Save(outputStream);
      return Task.FromResult(0);
    }


#if NETCORE
    public IWorkbook ExportWorkbook(DataBoundExcelSource source)
    {
      return ExportC1Excel(source);
    }

    public IWorkbook ExportC1Excel(DataBoundExcelSource source)
    {
      var fixedData = (source.Data ?? new object[0]).Cast<object>().ToList().AsReadOnly();
      var conlumnInfo = DataHelper.GetColumnInfos(fixedData.FirstOrDefault());
      var workbook = CreateRawWorkbook(conlumnInfo, source.Template);
      var templateInfo = new TemplateInfo(workbook);
      templateInfo.Apply(fixedData, conlumnInfo);
      return workbook;
    }

    private static IWorkbook CreateRawWorkbook(IEnumerable<DataColumnInfo> columnInfos, FileStorage template)
    {
      if (template == null)
      {
        return TemplateHelper.GenerateTemplate(columnInfos);
      }
      return ExcelFactory.CreateExcelHost().Read(template.Read(), template.Extension);
    }
#else
    public Workbook ExportWorkbook(DataBoundExcelSource source)
    {
      var excel = ExportC1Excel(source);
      return excel.ToWorkbook();
    }

    public C1XLBook ExportC1Excel(DataBoundExcelSource source)
    {
      var fixedData = (source.Data ?? new object[0]).Cast<object>().ToList().AsReadOnly();
      var conlumnInfo = DataHelper.GetColumnInfos(fixedData.FirstOrDefault());
      var workbook = CreateRawWorkbook(conlumnInfo, source.Template);
      var templateInfo = new TemplateInfo(workbook);
      templateInfo.Apply(fixedData, conlumnInfo);
      return workbook;
    }

    private static C1XLBook CreateRawWorkbook(IEnumerable<DataColumnInfo> columnInfos, FileStorage template)
    {
      if (template == null)
      {
        return TemplateHelper.GenerateTemplate(columnInfos);
      }

      var workbook = Utils.NewC1XLBook();
      workbook.Load(template.Read(), Utils.GetC1FileFormat(template.Extension));
      return workbook;
    }
#endif
    }

}
