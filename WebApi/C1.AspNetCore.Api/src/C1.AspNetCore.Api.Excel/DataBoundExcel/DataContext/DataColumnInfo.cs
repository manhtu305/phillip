﻿using System;

namespace C1.DataBoundExcel.DataContext
{
  internal class DataColumnInfo
  {
    public DataColumnInfo(string fieldName, Type dataType, Func<object, object> getter)
    {
      FieldName = fieldName;
      DataType = dataType;
      Getter = getter;
    }

    public string FieldName
    {
      get;
      private set;
    }

    public Type DataType
    {
      get;
      private set;
    }

    public Func<object, object> Getter
    {
      get;
      private set;
    }
  }
}
