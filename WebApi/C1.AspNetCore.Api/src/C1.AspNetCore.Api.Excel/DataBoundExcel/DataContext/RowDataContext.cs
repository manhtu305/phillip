﻿namespace C1.DataBoundExcel.DataContext
{
  internal class RowDataContext : BaseDataContext
  {
    private readonly TableDataContext _table;

    public RowDataContext(object data, TableDataContext parent)
        : base(data, parent)
    {
      _table = parent;
    }

    public TableDataContext Table
    {
      get { return _table; }
    }

    public override object GetValue(string fieldName)
    {
      return Table.GetItemValue(Data, fieldName);
    }
  }
}
