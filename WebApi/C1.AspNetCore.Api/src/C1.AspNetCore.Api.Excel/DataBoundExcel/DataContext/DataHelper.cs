﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace C1.DataBoundExcel.DataContext
{
  internal static class DataHelper
  {
    public static IEnumerable<DataColumnInfo> GetColumnInfos(object item)
    {
      if (item == null)
      {
        return new List<DataColumnInfo>();
      }

      var type = item.GetType();
      var typeCode = Type.GetTypeCode(type);

      //When the item self is the column data
      if (typeCode != TypeCode.Object)
      {
        const string itemName = "Item";
        return new List<DataColumnInfo> { new DataColumnInfo(itemName, type, o => o) };
      }

      var customTypeDescriptor = item as ICustomTypeDescriptor;
      if (customTypeDescriptor != null)
      {
        return customTypeDescriptor.GetProperties()
                .Cast<PropertyDescriptor>()
                .Select(pd => GetColumnInfo(pd, item));
      }

      var keyValuePairs = item as IEnumerable<KeyValuePair<string, object>>;
      if (keyValuePairs != null)
      {
        var columnInfos = new List<DataColumnInfo>();
        foreach (var property in keyValuePairs)
        {
          var key = property.Key;
          var value = property.Value;
          var valueType = value == null ? typeof(object) : value.GetType();
          Func<object, object> getter = o =>
          {
            var d = o as IDictionary<string, object>;
            return d == null ? null : d[key];
          };
          Type resolvedValueType;
          var resolvedGetter = ResolveGetter(getter, valueType, value, out resolvedValueType);
          columnInfos.Add(new DataColumnInfo(key, resolvedValueType, resolvedGetter));
        }

        return columnInfos;
      }

      return type.GetProperties().Concat<MemberInfo>(type.GetFields()).Select(i => GetColumnInfo(i, item));
    }

    private static DataColumnInfo GetColumnInfo(object descriptor, object owner)
    {
      string name = null;
      Type valueType = null;
      Func<object, object> getter = null;
      var pd = descriptor as PropertyDescriptor;
      if (pd != null)
      {
        name = pd.Name;
        valueType = pd.PropertyType;
        getter = pd.GetValue;
      }
      else
      {
        var pi = descriptor as PropertyInfo;
        if (pi != null)
        {
          name = pi.Name;
          valueType = pi.PropertyType;
          getter = pi.GetValue;
        }
        else
        {
          var fi = descriptor as FieldInfo;
          if (fi != null)
          {
            name = fi.Name;
            valueType = fi.FieldType;
            getter = fi.GetValue;
          }
        }
      }

      if (string.IsNullOrEmpty(name))
      {
        return null;
      }

      Type resolvedValueType;
      var resolvedGetter = ResolveGetter(getter, valueType, owner, out resolvedValueType);
      return new DataColumnInfo(name, resolvedValueType, resolvedGetter);
    }

    private static Func<object, object> ResolveGetter(Func<object, object> getter,
        Type valueType, object owner, out Type resolvedValueType)
    {
      Func<object, object> resolvedDBNullGetter = o =>
      {
        var val = getter(o);
        return val is DBNull ? null : val;
      };

      resolvedValueType = valueType;
      var typeCode = Type.GetTypeCode(valueType);
      if (typeCode != TypeCode.Object)
      {
        return resolvedDBNullGetter;
      }

      var value = resolvedDBNullGetter(owner);
      var convertible = value as IConvertible;
      if (convertible == null)
      {
        return resolvedDBNullGetter;
      }

      typeCode = convertible.GetTypeCode();
      resolvedValueType = typeCode.ToType();
      var useResolvedConvert = (typeCode == TypeCode.Empty || typeCode == TypeCode.DBNull);
      return o =>
      {
        var currentValue = resolvedDBNullGetter(o);
        if (useResolvedConvert)
        {
          return ResolvedConvertibleConvert(currentValue);
        }

        try
        {
          return ConvertibleConvert(currentValue, typeCode);
        }
        catch (Exception)
        {
          useResolvedConvert = true;
          return ResolvedConvertibleConvert(currentValue);
        }
      };
    }

    private static object ConvertibleConvert(object value, TypeCode typeCode)
    {
      return Convert.ChangeType(value, typeCode);
    }

    private static object ResolvedConvertibleConvert(object value)
    {
      var currentConvertible = value as IConvertible;
      if (currentConvertible == null)
      {
        return null;
      }

      var typeCode = currentConvertible.GetTypeCode();
      if (typeCode == TypeCode.Empty || typeCode == TypeCode.DBNull)
      {
        return null;
      }

      try
      {
        return ConvertibleConvert(value, typeCode);
      }
      catch (Exception)
      {
        return null;
      }
    }

    public static Type ToType(this TypeCode code)
    {
      switch (code)
      {
        case TypeCode.Boolean:
          return typeof(bool);

        case TypeCode.Byte:
          return typeof(byte);

        case TypeCode.Char:
          return typeof(char);

        case TypeCode.DateTime:
          return typeof(DateTime);

        case TypeCode.DBNull:
          return typeof(DBNull);

        case TypeCode.Decimal:
          return typeof(decimal);

        case TypeCode.Double:
          return typeof(double);

        case TypeCode.Int16:
          return typeof(short);

        case TypeCode.Int32:
          return typeof(int);

        case TypeCode.Int64:
          return typeof(long);

        case TypeCode.Object:
          return typeof(object);

        case TypeCode.SByte:
          return typeof(sbyte);

        case TypeCode.Single:
          return typeof(Single);

        case TypeCode.String:
          return typeof(string);

        case TypeCode.UInt16:
          return typeof(UInt16);

        case TypeCode.UInt32:
          return typeof(UInt32);

        case TypeCode.UInt64:
          return typeof(UInt64);

        default:
          return typeof(object);
      }
    }
  }
}
