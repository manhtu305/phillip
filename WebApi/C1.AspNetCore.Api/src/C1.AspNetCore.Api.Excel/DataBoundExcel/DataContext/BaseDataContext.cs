﻿namespace C1.DataBoundExcel.DataContext
{
    internal abstract class BaseDataContext
    {
        protected BaseDataContext(object data, BaseDataContext parent = null)
        {
            Data = data;
            Parent = parent;
        }

        public virtual BaseDataContext Parent
        {
            get;
            private set;
        }

        public virtual object Data
        {
            get;
            private set;
        }

        public abstract object GetValue(string fieldName);
    }
}
