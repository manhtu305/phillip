﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace C1.DataBoundExcel.DataContext
{
  internal class TableDataContext : BaseDataContext
  {
    private readonly IDictionary<string, Func<object, object>> _getters =
        new Dictionary<string, Func<object, object>>(StringComparer.OrdinalIgnoreCase);

    private int _rowPosition = -1;
    private readonly IList<object> _rows;

    public TableDataContext(IEnumerable data, IEnumerable<DataColumnInfo> columnInfos, BaseDataContext parent = null) : base(data, parent)
    {
      _rows = (data == null ? new List<object>() : data.Cast<object>().ToList()).AsReadOnly();
      var item = _rows.FirstOrDefault();
      if (item == null || item == DBNull.Value)
      {
        return;
      }

      if (columnInfos != null)
      {
        columnInfos.ToList().ForEach(d => _getters.Add(d.FieldName, d.Getter));
      }
    }

    public object GetItemValue(object item, string fieldName)
    {
      Func<object, object> getter;
      return _getters.TryGetValue(fieldName, out getter) ? getter(item) : null;
    }

    public override object GetValue(string fieldName)
    {
      var list = Data as IEnumerable;
      Func<object, object> getter;
      if (list == null || !_getters.TryGetValue(fieldName, out getter))
      {
        return null;
      }

      return list.Cast<object>().Select(i => getter);
    }

    public bool NextRow(out object row)
    {
      if (++_rowPosition >= _rows.Count)
      {
        _rowPosition = -1;
        row = null;
        return false;
      }

      row = _rows[_rowPosition];
      return true;
    }
  }
}
