﻿#if NETCORE
using GrapeCity.Documents.Excel;
#else 
using C1.C1Excel;
#endif
using C1.DataBoundExcel.DataContext;
using C1.DataBoundExcel.Expression;
using C1.Web.Api.Excel;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace C1.DataBoundExcel.Template
{
  internal static class TemplateHelper
  {
#if NETCORE
    internal static IWorkbook GenerateTemplate(IEnumerable<DataColumnInfo> columnInfos)
    {
      var workbook = ExcelFactory.CreateExcelHost().NewWorkbook();
      if (columnInfos == null || !columnInfos.Any())
      {
        return workbook;
      }

      var sheet = workbook.Worksheets[0];
      const int rowHeaderIndex = 0;
      const int templateRowIndex = 1;
      var columnIndex = 0;
      foreach (var columnInfo in columnInfos)
      {
        var headerCell = sheet.Cells[rowHeaderIndex, columnIndex];
        headerCell.Value = columnInfo.FieldName;
        var cell = sheet.Cells[templateRowIndex, columnIndex];
        cell.Value = GenerateFieldExpression(columnInfo.FieldName);
        columnIndex++;
      }

      return workbook;
    }

    private static string GenerateFieldExpression(string name)
    {
      return string.Format("[{0}]", name);
    }

    internal static Rectangle GetTemplateRange(IWorksheet sheet)
    {
      if (sheet.UsedRange == null) return Rectangle.Empty;

      for (var rowIndex = 0; rowIndex < sheet.UsedRange.Rows.Count; rowIndex++)
      {
        for (var colIndex = 0; colIndex < sheet.UsedRange.Columns.Count; colIndex++)
        {
          if (IsTemplateCell(sheet.Cells[rowIndex, colIndex]))
          {
            var location = new Point(colIndex, rowIndex);
            return new Rectangle(0, location.Y, sheet.UsedRange.Columns.Count, GetTemplateHeight(location, sheet));
          }
        }
      }

      return Rectangle.Empty;
    }

    private static int GetTemplateHeight(Point location, IWorksheet sheet)
    {
      var templateHeight = 1;
      for (var rowIndex = location.Y + 1; rowIndex < sheet.UsedRange.Rows.Count; rowIndex++)
      {
        var hasTemplateCell = false;
        for (var colIndex = 0; colIndex < sheet.UsedRange.Columns.Count; colIndex++)
        {
          if (IsTemplateCell(sheet.Cells[rowIndex, colIndex]))
          {
            hasTemplateCell = true;
            break;
          }
        }

        if (hasTemplateCell)
        {
          templateHeight = rowIndex - location.Y + 1;
        }
      }

      return templateHeight;
    }

    internal static bool IsFormulaCell(IRange cell)
    {
      return !string.IsNullOrEmpty(cell.Formula);
    }

    internal static bool IsEmptyCell(IRange cell)
    {
      return cell == null || !IsFormulaCell(cell) && cell.Value == null;
    }

    internal static bool IsTemplateCell(IRange cell)
    {
      if (IsEmptyCell(cell))
      {
        return false;
      }

      ExpressionProcessor exp;
      return (IsFormulaCell(cell) && ExpressionHelper.TryParse(cell.Formula, out exp))
          || (!IsFormulaCell(cell) && ExpressionHelper.TryParse(cell.Text, out exp));
    }
#else
    internal static C1XLBook GenerateTemplate(IEnumerable<DataColumnInfo> columnInfos)
        {
            var workbook = Utils.NewC1XLBook();
            if (columnInfos == null || !columnInfos.Any())
            {
                return workbook;
            }

            var sheet = workbook.Sheets[0];
            const int rowHeaderIndex = 0;
            const int templateRowIndex = 1;
            var columnIndex = 0;
            foreach (var columnInfo in columnInfos)
            {
                var headerCell = sheet[rowHeaderIndex, columnIndex];
                headerCell.Value = columnInfo.FieldName;
                var cell = sheet[templateRowIndex, columnIndex];
                cell.Value = GenerateFieldExpression(columnInfo.FieldName);
                columnIndex++;
            }

            return workbook;
        }

        private static string GenerateFieldExpression(string name)
        {
            return string.Format("[{0}]", name);
        }

        internal static Rectangle GetTemplateRange(XLSheet sheet)
        {
            for (var rowIndex = 0; rowIndex < sheet.Rows.Count; rowIndex++)
            {
                for (var colIndex = 0; colIndex < sheet.Columns.Count; colIndex++)
                {
                    if (IsTemplateCell(sheet.GetCell(rowIndex, colIndex)))
                    {
                        var location = new Point(colIndex, rowIndex);
                        return new Rectangle(0, location.Y, sheet.Columns.Count, GetTemplateHeight(location, sheet));
                    }
                }
            }

            return Rectangle.Empty;
        }

        private static int GetTemplateHeight(Point location, XLSheet sheet)
        {
            var templateHeight = 1;
            for (var rowIndex = location.Y + 1; rowIndex < sheet.Rows.Count; rowIndex++)
            {
                var hasTemplateCell = false;
                for (var colIndex = 0; colIndex < sheet.Columns.Count; colIndex++)
                {
                    if (IsTemplateCell(sheet.GetCell(rowIndex, colIndex)))
                    {
                        hasTemplateCell = true;
                        break;
                    }
                }

                if (hasTemplateCell)
                {
                    templateHeight = rowIndex - location.Y + 1;
                }
            }

            return templateHeight;
        }

        internal static bool IsFormulaCell(XLCell cell)
        {
            return !string.IsNullOrEmpty(cell.Formula);
        }

        internal static bool IsEmptyCell(XLCell cell)
        {
            return cell == null || !IsFormulaCell(cell) && cell.Value == null;
        }

        internal static bool IsTemplateCell(XLCell cell)
        {
            if (IsEmptyCell(cell))
            {
                return false;
            }

            ExpressionProcessor exp;
            return (IsFormulaCell(cell) && ExpressionHelper.TryParse(cell.Formula, out exp))
                || (!IsFormulaCell(cell) && ExpressionHelper.TryParse(cell.Text, out exp));
        }
#endif
  }
}
