﻿#if NETCORE
using GrapeCity.Documents.Excel;
#else 
using C1.C1Excel;
#endif
using C1.DataBoundExcel.DataContext;
using C1.DataBoundExcel.Expression;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace C1.DataBoundExcel.Template
{
  internal class TemplateInfo
  {
    //The General Date Long Time ("G") Format Specifier.
    private const string GeneralDateTimeFormat = "G";
#if NETCORE
    // FIX ME
    //private static readonly string ExcelGeneralDateTimeFormat =  XLStyle.FormatDotNetToXL(GeneralDateTimeFormat);
    private readonly IWorkbook _workbook;
    public TemplateInfo(IWorkbook workbook)
    {
      _workbook = workbook;
    }
    public void Apply(IList<object> data, IEnumerable<DataColumnInfo> columnInfos)
    {
      foreach (IWorksheet sheet in _workbook.Worksheets)
      {
        var templateRange = TemplateHelper.GetTemplateRange(sheet);
        if (templateRange.Height > 0)
        {
          templateRange = RepeatTemplate(sheet, templateRange, data.Count);
          ApplyTemplate(sheet, templateRange, data, columnInfos);
        }
      }
    }

    private Rectangle RepeatTemplate(IWorksheet sheet, Rectangle templateRange, int dataLength)
    {
      if (dataLength > templateRange.Height)
      {
        for (var i = 0; i < dataLength - templateRange.Height; i++)
        {
          sheet.Rows[templateRange.Top + 1].Insert();
        }

        for (var rowIndex = templateRange.Top + 1; rowIndex < templateRange.Bottom; rowIndex++)
        {
          CopyRow(rowIndex + dataLength - templateRange.Height, rowIndex, templateRange, sheet);
        }

        for (var rowIndex = templateRange.Bottom; rowIndex < templateRange.Top + dataLength; rowIndex++)
        {
          CopyRow(((rowIndex - templateRange.Top) % templateRange.Height) + templateRange.Top, rowIndex, templateRange, sheet);
        }
      }

      return new Rectangle(templateRange.Location, new Size(templateRange.Width, dataLength));
    }

    private void ApplyTemplate(IWorksheet sheet, Rectangle templateRange, IList<object> data, IEnumerable<DataColumnInfo> columnInfos)
    {
      var tableDataContext = new TableDataContext(data, columnInfos);
      for (var rowIndex = templateRange.Top; rowIndex < templateRange.Bottom; rowIndex++)
      {
        var rowDataContext = new RowDataContext(data[rowIndex - templateRange.Top], tableDataContext);
        for (var colIndex = templateRange.Left; colIndex < templateRange.Right; colIndex++)
        {
          ApplyCellTemplate(sheet, rowDataContext, sheet.Cells[rowIndex, colIndex]);
        }
      }
    }

    private void ApplyCellTemplate(IWorksheet sheet, RowDataContext rowDataContext, IRange cell)
    {
      if (!TemplateHelper.IsEmptyCell(cell))
      {
        ExpressionProcessor expression;
        if (TemplateHelper.IsFormulaCell(cell))
        {
          if (ExpressionHelper.TryParse(cell.Formula, out expression))
          {
            cell.Formula = expression.Execute(cell.Formula, rowDataContext).ToString();
          }
        }
        else
        {
          if (ExpressionHelper.TryParse(cell.Text, out expression))
          {
            cell.Value = expression.Execute(cell.Text, rowDataContext);

            if (cell.Value is DateTime)
            {
              // FIX ME
              //var style = cell.Style != null ? cell.Style.Clone() : new XLStyle(sheet.Book);
              //style.Format = string.IsNullOrEmpty(style.Format) ? ExcelGeneralDateTimeFormat : style.Format;
              //cell.Style = style;
            }
          }
        }
      }
    }

    private void CopyRow(int sourceRowIndex, int targetRowIndex, Rectangle templateRange, IWorksheet sheet)
    {
      for (var colIndex = templateRange.Left; colIndex < templateRange.Right; colIndex++)
      {
        ResolveMergedCells(sourceRowIndex, targetRowIndex, colIndex, sheet);

        var sourceCell = sheet.Cells[sourceRowIndex, colIndex];
        var targetCell = sheet.Cells[targetRowIndex, colIndex];

        if (sourceCell == null && targetCell == null)
        {
          continue;
        }
        else if (sourceCell == null)
        {
          sourceCell = sheet.Cells[sourceRowIndex, colIndex];
        }
        else
        {
          targetCell = sheet.Cells[targetRowIndex, colIndex];
        }

        sourceCell.Copy(targetCell);
      }

    }

    private void ResolveMergedCells(int sourceRowIndex, int targetRowIndex, int colIndex, IWorksheet sheet)
    {
      // FIX ME
      //var newCellRanges = new List<XLCellRange>();
      //foreach (XLCellRange cellRange in sheet.MergedCells)
      //{
      //  if (cellRange.RowFrom == cellRange.RowTo && cellRange.Contains(sheet, sourceRowIndex, colIndex) && !cellRange.Contains(sheet, targetRowIndex, colIndex))
      //  {
      //    var newCellRange = new XLCellRange(sheet, targetRowIndex, targetRowIndex, cellRange.ColumnFrom, cellRange.ColumnTo);
      //    newCellRanges.Add(newCellRange);
      //  }
      //}

      //newCellRanges.ForEach(r => sheet.MergedCells.Add(r));
    }
  
#else
    private static readonly string ExcelGeneralDateTimeFormat = XLStyle.FormatDotNetToXL(GeneralDateTimeFormat);
    private readonly C1XLBook _workbook;
    public TemplateInfo(C1XLBook workbook)
    {
      _workbook = workbook;
    }



    public void Apply(IList<object> data, IEnumerable<DataColumnInfo> columnInfos)
    {
      foreach (XLSheet sheet in _workbook.Sheets)
      {
        var templateRange = TemplateHelper.GetTemplateRange(sheet);
        if (templateRange.Height > 0)
        {
          templateRange = RepeatTemplate(sheet, templateRange, data.Count);
          ApplyTemplate(sheet, templateRange, data, columnInfos);
        }
      }
    }

    private Rectangle RepeatTemplate(XLSheet sheet, Rectangle templateRange, int dataLength)
    {
      if (dataLength > templateRange.Height)
      {
        for (var i = 0; i < dataLength - templateRange.Height; i++)
        {
          sheet.Rows.Insert(templateRange.Top + 1);
        }

        for (var rowIndex = templateRange.Top + 1; rowIndex < templateRange.Bottom; rowIndex++)
        {
          CopyRow(rowIndex + dataLength - templateRange.Height, rowIndex, templateRange, sheet);
        }

        for (var rowIndex = templateRange.Bottom; rowIndex < templateRange.Top + dataLength; rowIndex++)
        {
          CopyRow(((rowIndex - templateRange.Top) % templateRange.Height) + templateRange.Top, rowIndex, templateRange, sheet);
        }
      }

      return new Rectangle(templateRange.Location, new Size(templateRange.Width, dataLength));
    }

    private void ApplyTemplate(XLSheet sheet, Rectangle templateRange, IList<object> data, IEnumerable<DataColumnInfo> columnInfos)
    {
      var tableDataContext = new TableDataContext(data, columnInfos);
      for (var rowIndex = templateRange.Top; rowIndex < templateRange.Bottom; rowIndex++)
      {
        var rowDataContext = new RowDataContext(data[rowIndex - templateRange.Top], tableDataContext);
        for (var colIndex = templateRange.Left; colIndex < templateRange.Right; colIndex++)
        {
          ApplyCellTemplate(sheet, rowDataContext, sheet.GetCell(rowIndex, colIndex));
        }
      }
    }

    private void ApplyCellTemplate(XLSheet sheet, RowDataContext rowDataContext, XLCell cell)
    {
      if (!TemplateHelper.IsEmptyCell(cell))
      {
        ExpressionProcessor expression;
        if (TemplateHelper.IsFormulaCell(cell))
        {
          if (ExpressionHelper.TryParse(cell.Formula, out expression))
          {
            cell.Formula = expression.Execute(cell.Formula, rowDataContext).ToString();
          }
        }
        else
        {
          if (ExpressionHelper.TryParse(cell.Text, out expression))
          {
            cell.Value = expression.Execute(cell.Text, rowDataContext);

            if (cell.Value is DateTime)
            {
              var style = cell.Style != null ? cell.Style.Clone() : new XLStyle(sheet.Book);
              style.Format = string.IsNullOrEmpty(style.Format) ? ExcelGeneralDateTimeFormat : style.Format;
              cell.Style = style;
            }
          }
        }
      }
    }

    private void CopyRow(int sourceRowIndex, int targetRowIndex, Rectangle templateRange, XLSheet sheet)
    {
      for (var colIndex = templateRange.Left; colIndex < templateRange.Right; colIndex++)
      {
        ResolveMergedCells(sourceRowIndex, targetRowIndex, colIndex, sheet);

        var sourceCell = sheet.GetCell(sourceRowIndex, colIndex);
        var targetCell = sheet.GetCell(targetRowIndex, colIndex);

        if (sourceCell == null && targetCell == null)
        {
          continue;
        }
        else if (sourceCell == null)
        {
          sourceCell = sheet[sourceRowIndex, colIndex];
        }
        else
        {
          targetCell = sheet[targetRowIndex, colIndex];
        }

        var style = sourceCell.Style == null ? null : sourceCell.Style.Clone();
        targetCell.SetValue(sourceCell.Value, style);

        targetCell.Hyperlink = sourceCell.Hyperlink;

        if (!string.IsNullOrEmpty(sourceCell.Formula))
        {
          sheet.CopyFormula(sourceRowIndex, colIndex, targetRowIndex, colIndex);
        }
        else
        {
          targetCell.Formula = string.Empty;
        }
      }

      sheet.Rows[targetRowIndex].Collapsed = sheet.Rows[sourceRowIndex].Collapsed;
      sheet.Rows[targetRowIndex].Height = sheet.Rows[sourceRowIndex].Height;
      sheet.Rows[targetRowIndex].IsCustomHeight = sheet.Rows[sourceRowIndex].IsCustomHeight;
      sheet.Rows[targetRowIndex].OutlineLevel = sheet.Rows[sourceRowIndex].OutlineLevel;
      sheet.Rows[targetRowIndex].PageBreak = sheet.Rows[sourceRowIndex].PageBreak;
      sheet.Rows[targetRowIndex].Visible = sheet.Rows[sourceRowIndex].Visible;
      sheet.Rows[targetRowIndex].Style = sheet.Rows[sourceRowIndex].Style == null ? null : sheet.Rows[sourceRowIndex].Style.Clone();
    }

    private void ResolveMergedCells(int sourceRowIndex, int targetRowIndex, int colIndex, XLSheet sheet)
    {
      var newCellRanges = new List<XLCellRange>();
      foreach (XLCellRange cellRange in sheet.MergedCells)
      {
        if (cellRange.RowFrom == cellRange.RowTo && cellRange.Contains(sheet, sourceRowIndex, colIndex) && !cellRange.Contains(sheet, targetRowIndex, colIndex))
        {
          var newCellRange = new XLCellRange(sheet, targetRowIndex, targetRowIndex, cellRange.ColumnFrom, cellRange.ColumnTo);
          newCellRanges.Add(newCellRange);
        }
      }

      newCellRanges.ForEach(r => sheet.MergedCells.Add(r));
    }
#endif
    }
}
