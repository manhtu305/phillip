﻿using C1.Web.Api;
using System.Collections;

namespace C1.DataBoundExcel
{
  internal class DataBoundExcelSource : ExportSource
  {
    public DataBoundExcelSource(IEnumerable data, FileStorage template = null)
    {
      Data = data;
      Template = template;
    }

    public IEnumerable Data
    {
      get;
      private set;
    }

    public FileStorage Template
    {
      get;
      private set;
    }
  }
}
