﻿using System;
#if NETCORE
using C1.AspNetCore.Api.Excel;
#endif

#if !ASPNETCORE && !NETCORE
using System.Web.Http.ModelBinding;
#else
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
#endif

namespace C1.Web.Api.Excel
{
    /// <summary>
    /// An attribute to apply to action parameters for model binding for Excel export.
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = true)]
    public class ExcelExportModelBinderAttribute : ModelBinderAttribute
    {
        /// <summary>
        /// Initialize a new instance of the ExcelExportModelBinderAttribute class.
        /// </summary>
        public ExcelExportModelBinderAttribute()
            : base()
        {
#if NETCORE
            BinderType = typeof(ExcelExportModelBinder);
#else
            BinderType = typeof(BodyModelBinder<ExcelExportSource>);
#endif

#if ASPNETCORE || NETCORE
            // Model validation will take long time because there are lots of data in the model.
            // Set BindingSource.IsFromRequest to false, it will skip model validation.
            BindingSource = new BindingSource(BindingSource.Id, BindingSource.DisplayName, BindingSource.IsGreedy, false);
#endif
        }
    }
}