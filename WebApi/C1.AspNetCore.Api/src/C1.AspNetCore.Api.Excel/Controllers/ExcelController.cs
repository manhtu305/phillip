﻿using System.Collections.Generic;
using System.IO;
#if NETCORE
using GrapeCity.Documents.Excel;
#endif
#if !ASPNETCORE && !NETCORE
using System;
using System.Net.Http;
using System.Web.Http;
using Controller = System.Web.Http.ApiController;
using IActionResult = System.Web.Http.IHttpActionResult;
using FromQuery = C1.Web.Api.FromUriExAttribute;
using System.Web.Http.ModelBinding;
#else
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;
#endif

namespace C1.Web.Api.Excel
{
  /// <summary>
  /// Controller for Excel Web API.
  /// </summary>
  public class ExcelController : Controller
  {

    public ExcelController() : base()
    {
#if NETCORE
     Workbook.SetLicenseKey("GrapeCity-Internal-License,691549758164986#A0ijOklkI1pjIEJCLi4TPnd5dIZDWRVWTGVkWaRHeWBXSZ3GdxUWMkhnMMdHTL34KS9UZqlHbYxGWvNXbGdFUmV7L7EDZxZVOFNmNjx4Y9QTNmllWGllb05kS4gjWBlUarEmTDZkI0IyUiwiMzEjM5UzM8ITM0IicfJye&Qf35VfikDS7IjI0IyQiwiIyYFI4VmTuACblNGeFBicvZGI49WZtV7YvREIDdkI0IiTis7W0ICZyBlIsIyN5UTMyADIyIDNwkTMwIjI0ICdyNkIsIyYulGI9RXaDVGchJ7RiojIh94QiwiI6gTO4YTM8UzN9QTNxnjKi");
#endif
    }
#if ASPNETCORE || NETCORE
    private new ContentResult Json(object data, JsonSerializerSettings settings)
    {
      var json = JsonConvert.SerializeObject(data, settings);
      return Content(json, "application/json");
    }
#endif

#if !NETCORE
    /// <summary>
    /// Gets the result containing the requested excel according to the specified request.
    /// </summary>
    /// <param name="re">The request</param>
    /// <returns>The result containing the requested excel</returns>
    [HttpGet]
    [Route("api/excel")]
    public virtual IActionResult Get([FromQuery]ExcelRequest re)
    {
      return Generate(re);
    }

    /// <summary>
    /// Process the GET request.
    /// </summary>
    /// <param name="pathAndCmd">The text for path and command.</param>
    /// <param name="parameters">The request parameters.</param>
    /// <returns>The response result.</returns>
    [HttpGet]
    [Route("api/excel/{*pathAndCmd}")]
    public virtual IActionResult GetOperation(string pathAndCmd, [FromQuery]IDictionary<string, string[]> parameters)
    {
      var processor = new Operation.Processor(HttpMethod.Get, pathAndCmd, parameters);
      processor.SetRequestParamsGetter(() => Request.GetParams());
      return processor.GetResult(this);
    }

    /// <summary>
    /// Process the PUT request.
    /// </summary>
    /// <param name="pathAndCmd">The text for path and command.</param>
    /// <param name="parameters">The request parameters.</param>
    /// <returns>The response result.</returns>
    [HttpPut]
    [Route("api/excel/{*pathAndCmd}")]
    public virtual IActionResult PutOperation(string pathAndCmd, [FromForm]IDictionary<string, string[]> parameters)
    {
      var processor = new Operation.Processor(HttpMethod.Put, pathAndCmd, parameters);
      return processor.GetResult(this);
    }

    /// <summary>
    /// Process the POST request.
    /// </summary>
    /// <param name="pathAndCmd">The text for path and command.</param>
    /// <param name="parameters">The request parameters.</param>
    /// <returns>The response result.</returns>
    [HttpPost]
    [Route("api/excel/{*pathAndCmd}")]
    public virtual IActionResult PostOperation(string pathAndCmd, [FromForm]IDictionary<string, string[]> parameters)
    {
      var processor = new Operation.Processor(HttpMethod.Post, pathAndCmd, parameters);
      return processor.GetResult(this);
    }

    /// <summary>
    /// Process the DELETE request.
    /// </summary>
    /// <param name="pathAndCmd">The text for path and command.</param>
    /// <param name="parameters">The request parameters.</param>
    /// <returns>The response result.</returns>
    [HttpDelete]
    [Route("api/excel/{*pathAndCmd}")]
    public virtual IActionResult DeleteOperation(string pathAndCmd, [FromQuery]IDictionary<string, string[]> parameters)
    {
      var processor = new Operation.Processor(HttpMethod.Delete, pathAndCmd, parameters);
      return processor.GetResult(this);
    }

    /// <summary>
    /// Gets the result containing the requested excel according to the posted request.
    /// </summary>
    /// <param name="re">The request</param>
    /// <returns>The result containing the requested excel</returns>
    [HttpPost]
    [Route("api/excel")]
    public virtual IActionResult Post([ModelBinder(BinderType = typeof(RequestModelBinder<ExcelRequest>))]ExcelRequest re)
    {
      return Generate(re);      
    }

    private IActionResult Generate(ExcelRequest re)
    {
      re = re ?? new ExcelRequest();
      re.SetRequestParamsGetter(() => Request.GetParams());
      var excelGenerator = new ExcelGenerator(re);
      return excelGenerator.GetResult(this);
    }

    /// <summary>
    /// Gets the result of merging multiple excels according to the specified request.
    /// </summary>
    /// <param name="re">The request</param>
    /// <returns>The result containing the requested excel</returns>
    [HttpGet]
    [Route("api/excel/merge")]
    public virtual IActionResult Merge([FromQuery]MergeRequest re)
    {
      return MergeCore(re);
    }

    /// <summary>
    /// Gets the result of merging multiple excels according to the posted request.
    /// </summary>
    /// <param name="re">The request</param>
    /// <returns>The result containing the requested excel</returns>
    [HttpPost]
    [Route("api/excel/merge")]
    public virtual IActionResult MergePost([ModelBinder(BinderType = typeof(RequestModelBinder<MergeRequest>))]MergeRequest re)
    {
      return MergeCore(re);
    }

    private IActionResult MergeCore(MergeRequest re)
    {
      re.SetRequestParamsGetter(() => Request.GetParams());
      var merger = new ExcelMerger(re);
      return merger.GetResult(this);
    }

    /// <summary>
    /// Import from a file into a json text with the Excel common format.
    /// </summary>
    /// <param name="source">The import source.</param>
    /// <returns>A json text with the Excel common format.</returns>
    [HttpPost]
    [Route("api/import/excel")]
    public virtual IActionResult Import([ImportModelBinder] ImportSource source)
    {
      if (source != null && source.FileName != null)
      {
        var importer = new ExcelImporter();
        var task = importer.ImportAsync(source);
        task.Wait();
        return Json(task.Result, Workbook.JsonSettings);
      }
      return BadRequest();
    }

    /// <summary>
    /// Export to an excel file.
    /// </summary>
    /// <param name="source">The data used for the Excel export requests.</param>
    /// <returns>An excel file.</returns>
    [HttpPost]
    [Route("api/export/excel")]
    public virtual IActionResult Export([ExcelExportModelBinder] ExcelExportSource source)
    {
        return new ExporterResult(source);
    }

#if DEBUG && !ASPNETCORE
        [HttpGet]
        [Route("excel/test")]
        public IActionResult Test()
        {
            var outputStream = new System.IO.MemoryStream();
            var fileName = "test.xls";
            var workbook = GetTestWorkbook();

            IExcelHost host = new C1ExcelHost();
            host.Write(workbook, outputStream, "xls");
            outputStream.Position = 0;
            return ResponseMessage(new HttpResponseMessage
            {
                Content = new AttachmentContent(outputStream, fileName)
            });
        }

        private Workbook GetTestWorkbook()
        {
            var workbook = new Workbook();
            var worksheet = new Worksheet();
            worksheet.Name = "My sheet 1";
            Array.ForEach(new[] { 20, 120, 80 }, x => worksheet.Columns.Add(new Column { Width = x.ToString() }));
            worksheet.Rows.Add(new Row("ID", "Name", "Gender") { Height = 30 });
            worksheet.Rows.Add(new Row("1", "Ann", "F"));
            worksheet.Rows.Add(new Row("2", "Bob", "M"));
            worksheet.Rows.Add(new Row("3", "Clerk", "M"));
            workbook.Sheets.Add(worksheet);
            return workbook;
        }
#endif
#endif

#if NETCORE

    /// <summary>
    /// Import from a file into a json text with the Excel common format.
    /// </summary>
    /// <param name="source">The import source.</param>
    /// <returns>A json text with the Excel common format.</returns>
    [HttpPost]
    [Route("api/import/excel")]
    public virtual IActionResult Import([ImportModelBinder] ImportSource source)
    {
      if (source != null && source.FileName != null)
      {
        var importer = new ExcelImporter();
        var task = importer.ImportAsync(source);
        task.Wait();
        var json = task.Result.ToJson();

        return Content(json, "application/json");
      }
      return BadRequest();
    }

    /// <summary>
    /// Export to an excel file.
    /// </summary>
    /// <param name="source">The data used for the Excel export requests.</param>
    /// <returns>An excel file.</returns>
    [HttpPost]
    [Route("api/export/excel")]
    public virtual IActionResult Export([ExcelExportModelBinder] ExcelExportSource source)
    {
      return new ExporterResult(source);
    }

    /// <summary>
    /// Process the GET request.
    /// </summary>
    /// <param name="pathAndCmd">The text for path and command.</param>
    /// <param name="parameters">The request parameters.</param>
    /// <returns>The response result.</returns>
    [HttpGet]
    [Route("api/excel/{*pathAndCmd}")]
    public virtual IActionResult GetOperation(string pathAndCmd, [FromQuery]IDictionary<string, string[]> parameters)
    {
      var processor = new C1.Web.Api.Excel.Operation.Processor(HttpMethod.Get, pathAndCmd, parameters);
      processor.SetRequestParamsGetter(() => Request.GetParams());
      return processor.GetResult(this);
    }

    /// <summary>
    /// Process the POST request.
    /// </summary>
    /// <param name="pathAndCmd">The text for path and command.</param>
    /// <param name="parameters">The request parameters.</param>
    /// <returns>The response result.</returns>
    [HttpPost]
    [Route("api/excel/{*pathAndCmd}")]
    public virtual IActionResult PostOperation(string pathAndCmd, [FromForm]IDictionary<string, string[]> parameters)
    {
      var processor = new C1.Web.Api.Excel.Operation.Processor(HttpMethod.Post, pathAndCmd, parameters);
      return processor.GetResult(this);
    }

    /// <summary>
    /// Process the PUT request.
    /// </summary>
    /// <param name="pathAndCmd">The text for path and command.</param>
    /// <param name="parameters">The request parameters.</param>
    /// <returns>The response result.</returns>
    [HttpPut]
    [Route("api/excel/{*pathAndCmd}")]
    public virtual IActionResult PutOperation(string pathAndCmd, [FromForm]IDictionary<string, string[]> parameters)
    {
      var processor = new C1.Web.Api.Excel.Operation.Processor(HttpMethod.Put, pathAndCmd, parameters);
      return processor.GetResult(this);
    }

    /// <summary>
    /// Process the DELETE request.
    /// </summary>
    /// <param name="pathAndCmd">The text for path and command.</param>
    /// <param name="parameters">The request parameters.</param>
    /// <returns>The response result.</returns>
    [HttpDelete]
    [Route("api/excel/{*pathAndCmd}")]
    public virtual IActionResult DeleteOperation(string pathAndCmd, [FromQuery]IDictionary<string, string[]> parameters)
    {
      var processor = new C1.Web.Api.Excel.Operation.Processor(HttpMethod.Delete, pathAndCmd, parameters);
      return processor.GetResult(this);
    }
    
    /// <summary>
    /// Gets the result containing the requested excel according to the specified request.
    /// </summary>
    /// <param name="re">The request</param>
    /// <returns>The result containing the requested excel</returns>
    [HttpGet]
    [Route("api/excel")]
    public virtual IActionResult Get([FromQuery]ExcelRequest re)
    {
      return Generate(re);
    }

    // GET api/values
    [HttpGet("api/excel/test")]
    public ActionResult<string> Get()
    {
      return "Welcome to ExcelAPI";
    }

    /// <summary>
    /// Gets the result containing the requested excel according to the posted request.
    /// </summary>
    /// <param name="re">The request</param>
    /// <returns>The result containing the requested excel</returns>
    [HttpPost]
    [Route("api/excel")]
    public virtual IActionResult Post([ModelBinder(BinderType = typeof(RequestModelBinder<ExcelRequest>))]ExcelRequest re)
    {
      return Generate(re);
    }

    /// <summary>
    /// Gets the result of merging multiple excels according to the specified request.
    /// </summary>
    /// <param name="re">The request</param>
    /// <returns>The result containing the requested excel</returns>
    [HttpGet]
    [Route("api/excel/merge")]
    public virtual IActionResult Merge([FromQuery]MergeRequest re)
    {
      return MergeCore(re);
    }

    /// <summary>
    /// Gets the result of merging multiple excels according to the posted request.
    /// </summary>
    /// <param name="re">The request</param>
    /// <returns>The result containing the requested excel</returns>
    [HttpPost]
    [Route("api/excel/merge")]
    public virtual IActionResult MergePost([ModelBinder(BinderType = typeof(RequestModelBinder<MergeRequest>))]MergeRequest re)
    {
      return MergeCore(re);
    }

    private IActionResult MergeCore(MergeRequest re)
    {
      re.SetRequestParamsGetter(() => Request.GetParams());
      var merger = new ExcelMerger(re);
      return merger.GetResult(this);
    }

    private IActionResult Generate(ExcelRequest re)
    {
      re = re ?? new ExcelRequest();
      re.SetRequestParamsGetter(() => Request.GetParams());
      var excelGenerator = new ExcelGenerator(re);
      IActionResult result = excelGenerator.GetResult(this);

      var downloadFileName = string.IsNullOrEmpty(re.FileName) ? nameof(result) : re.FileName;

      Response.Headers.Add("Content-Disposition", $"attachment; filename=" + System.Uri.EscapeUriString(downloadFileName) + re.Type.ToFileExtension());
      return result;
    }

#endif
  }


}