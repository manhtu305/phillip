﻿using System.Linq;
using System.Reflection;

namespace C1.Web.Api.WebDriver
{
    internal class By
    {
        private static readonly TypeInfo ByType = PhantomJSDriver.WebDriverAssembly.DefinedTypes
            .First(t => t.FullName == "OpenQA.Selenium.By");
        private static readonly MethodInfo CssSelectorMethod = ByType.GetMethod("CssSelector", BindingFlags.Public | BindingFlags.Static);

        public static object CssSelector(string cssSelectorToFind)
        {
            return CssSelectorMethod.Invoke(null, new object[] { cssSelectorToFind });
        }
    }
}
