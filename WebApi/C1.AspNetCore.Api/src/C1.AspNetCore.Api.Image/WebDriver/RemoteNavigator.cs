﻿namespace C1.Web.Api.WebDriver
{
    internal class RemoteNavigator
    {
        private readonly object _internalNavigation;

        public RemoteNavigator(object internalNavigation)
        {
            _internalNavigation = internalNavigation;
        }

        public void GoToUrl(string url)
        {
            var goToUrlMethod = _internalNavigation.GetType().GetMethod("GoToUrl", new [] {typeof (string)});
            goToUrlMethod.Invoke(_internalNavigation, new object[] {url});
        }
    }
}
