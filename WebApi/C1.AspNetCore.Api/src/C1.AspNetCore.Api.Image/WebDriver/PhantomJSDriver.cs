﻿using System;
using System.Linq;
using System.Reflection;

namespace C1.Web.Api.WebDriver
{
    internal class PhantomJSDriver : IDisposable
    {
        // WebDriver and WebDriver.Support assemblies are not signed. But all C1 assemblies need be signed,
        // so we have to load these 2 assemblies dynamically.
        public static readonly Assembly WebDriverAssembly = Assembly.Load("WebDriver");
        public static readonly Assembly WebDriverSupportAssembly = Assembly.Load("WebDriver.Support");

        private const string PhantomJSDriverName = "OpenQA.Selenium.PhantomJS.PhantomJSDriver";
        private static readonly TypeInfo PhantomJSDriverType = WebDriverAssembly.DefinedTypes
            .First(t => t.FullName == PhantomJSDriverName);
        private static readonly MethodInfo NavigateMethod = PhantomJSDriverType.GetMethod("Navigate", BindingFlags.Public | BindingFlags.Instance);
        private static readonly MethodInfo FindElementMethod = PhantomJSDriverType.GetMethod("FindElement", BindingFlags.Public | BindingFlags.Instance);

        private readonly object _internalDriver;

        public PhantomJSDriver()
        {
            _internalDriver = WebDriverAssembly.CreateInstance(PhantomJSDriverName);
        }

        public object InternalDriver
        {
            get
            {
                return _internalDriver;
            }
        }

        public RemoteNavigator Navigate()
        {
            return new RemoteNavigator(NavigateMethod.Invoke(_internalDriver, null));
        }

        public dynamic FindElement(object by)
        {
            return FindElementMethod.Invoke(_internalDriver, new [] { by });
        }

        public void Dispose()
        {
            var disposable = _internalDriver as IDisposable;
            if (disposable != null)
            {
                disposable.Dispose();
            }
        }
    }
}
