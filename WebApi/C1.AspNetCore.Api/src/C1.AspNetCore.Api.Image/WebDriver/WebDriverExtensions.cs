﻿using System.Linq;
using System.Reflection;

namespace C1.Web.Api.WebDriver
{
    internal static class WebDriverExtensions
    {
        private static readonly TypeInfo WebDriverExtensionsType = PhantomJSDriver.WebDriverSupportAssembly.DefinedTypes
            .First(t => t.FullName == "OpenQA.Selenium.Support.Extensions.WebDriverExtensions");
        private static readonly MethodInfo TakeScreenshotMethod = WebDriverExtensionsType
            .GetMethod("TakeScreenshot", BindingFlags.Public | BindingFlags.Static);

        public static dynamic TakeScreenshot(this PhantomJSDriver driver)
        {
            return TakeScreenshotMethod.Invoke(null, new [] {driver.InternalDriver});
        }
    }
}
