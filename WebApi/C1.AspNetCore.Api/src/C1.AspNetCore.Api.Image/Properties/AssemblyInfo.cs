﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;

#if !ASPNETCORE
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("C1.Web.Api.Image")]
[assembly: AssemblyDescription("ComponentOne ASP.NET Chart and Gauge Image Export Services")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("GrapeCity, Inc.")]
[assembly: AssemblyProduct("C1.Web.Api.Image")]
[assembly: AssemblyCopyright("Copyright © GrapeCity, Inc.  All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("E068AA40-57B6-4CAB-A325-B8A4D6F8E1AE")]
#endif

#if UNITTEST
[assembly: InternalsVisibleTo(AssemblyInfo.NamePrefix + ".Tests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c1bd0a0680e736d0bea3881ff835b56a8cb56c82ed17edc4714140bed28d7d504ba64753dcf227a35b1bab1a43bd83d1fc916fdf98e6daf5aa19f216831b11334657352c4c848ea47cde573158d1c86ec7efbced83cb62ae2550bd0fd0da2cf48593567ceb65d1e02f6d96ebebdbb83e39db493bb2ca1c301eebca0cb8e5abe4")]
#endif

[assembly: AssemblyVersion(AssemblyInfo.Version)]
[assembly: AssemblyFileVersion(AssemblyInfo.Version)]