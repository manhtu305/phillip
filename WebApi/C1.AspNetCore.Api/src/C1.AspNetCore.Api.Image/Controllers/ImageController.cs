﻿#if !ASPNETCORE
using System.Net.Http;
using System.Web.Http;
using Controller = System.Web.Http.ApiController;
using IActionResult = System.Web.Http.IHttpActionResult;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace C1.Web.Api.Image
{
    /// <summary>
    /// Controller for image export.
    /// </summary>
    public class ImageController : Controller
    {
        /// <summary>
        /// Exports to an image.
        /// </summary>
        /// <param name="exportSource">The exchange data model used for sending image export requests.</param>
        /// <returns>An image.</returns>
        [HttpPost]
        [Route("api/export/image")]
        public virtual IActionResult Export([ImageExportModelBinder] ImageExportSource exportSource)
        {
            return new ExporterResult(exportSource);
        }
    }
}