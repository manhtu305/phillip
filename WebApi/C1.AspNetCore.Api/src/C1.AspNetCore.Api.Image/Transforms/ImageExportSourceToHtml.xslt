﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">

  <xsl:output method="html" indent="no" encoding="utf-8" omit-xml-declaration="yes" />
  <xsl:template match="/">
    <xsl:variable name="height" select="ImageExportSource/Height/text()" />
    <xsl:variable name="width" select="ImageExportSource/Width/text()" />
    <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE html&gt;</xsl:text>
    <html>
      <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link rel="stylesheet" href="css/wijmo.min.css" />
        <style>
          #wj-wm {
            display: none;
          }

          rect:not(.wj-data-label-border)[stroke=null] {
            stroke-width: 0;
          }

          .wj-flexchart {
              -webkit-box-sizing: border-box;
              -moz-box-sizing: border-box;
              box-sizing: border-box;
          }
        </style>
        <script src="js/wijmo.min.js" type="text/javascript"></script>
        <script src="js/wijmo.chart.min.js" type="text/javascript"></script>
        <script src="js/wijmo.chart.analytics.min.js" type="text/javascript"></script>
        <script src="js/wijmo.chart.hierarchical.min.js" type="text/javascript"></script>
        <script src="js/wijmo.chart.radar.min.js" type="text/javascript"></script>
        <script src="js/wijmo.gauge.min.js" type="text/javascript"></script>
        <script src="js/c1.wijmo.js" type="text/javascript"></script>
      </head>
      <body style="background: white; margin: 0; padding: 0;">
        <div id="control" style="height: {$height}px; width: {$width}px;"></div>
        <script type="text/javascript">
          (function() {
            try {
              var control = new <xsl:value-of select="ImageExportSource/ControlConstructor/text()" />('#control');
              var jsonObj = <xsl:value-of select="ImageExportSource/State/text()" />;
              jsonObj = c1.parseJSON(JSON.stringify(jsonObj)) || {};
              var series = jsonObj['series'], extraSeries = [];
              if(series){
                var newSeries = [];
                for(var i = 0, l = series.length; i &lt; l; i++){
                  var s = series[i];
                  if(!s) continue;
                  if (s['axisX']) {
                    s['axisX'] = wijmo.copy(new wijmo.chart.Axis(), s['axisX']);
                  }
                  if (s['axisY']) {
                    s['axisY'] = wijmo.copy(new wijmo.chart.Axis(), s['axisY']);
                  }
                  var innerType = s.innerType;
                  if(innerType){
                    delete s.innerType;
                    var cstr = c1._findFunction(innerType);
                    if(cstr) {
                      var extraS = new cstr();
                      if(extraS instanceof wijmo.chart.analytics.YFunctionSeries){
                        if(s.func) s.func = c1._toFunction(s.func);
                      } else if(extraS instanceof wijmo.chart.analytics.ParametricFunctionSeries){
                        if(s.xFunc) s.xFunc = c1._toFunction(s.xFunc);
                        if(s.yFunc) s.yFunc = c1._toFunction(s.yFunc);
                      }
                      var extraItem = {index: i, value: wijmo.copy(extraS, s)};
                      extraSeries.push(extraItem);
                      continue;
                    }
                  }
                  newSeries.push(s);
                }
                jsonObj['series'] = newSeries;
              }
              control.initialize(jsonObj);
              control.deferUpdate(function(){
                extraSeries.forEach(function(item){control.series.splice(item.index, 0, item.value);});
              });
            } catch (e) {
              document.getElementById('control').innerHTML = e;
            }
          })();
        </script>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>