
// wijmo.copy() may be used in private _copy(), like in chart.series. 
// So adding a new copy2() something will not work.
// The only possible way is to replace the original copy().
wijmo.copy = function extend(dst, src) {
    for (var key in src) {
        if (key in dst) { // check, but not throw.
            var value = src[key];
            if (!dst._copy || !dst._copy(key, value)) { // allow overrides
                if (dst[key] instanceof Event && wijmo.isFunction(value)) {
                    dst[key].addHandler(value); // add event handler
                }
                else if (wijmo.isObject(value) && dst[key]) {
                    extend(dst[key], value); // copy sub-objects
                }
                else {
                    dst[key] = value; // assign values
                }
            }
        }
    }
    return dst;
};

var c1 = c1 || {};

c1.parseJSON = function (text) {
    return JSON.parse(text, function (key, value) {
        if (/(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})/.test(value)) {
            return new Date(value);
        }
        return value;
    });
}

c1._findFunction = function(name) {
    if (!name) return void (0);
    var result = window;
    name.split('.').forEach(function (levelName) { return result = result[levelName]; });
    return result;
}

c1._toFunction = function (text) {
    if (!text) return void (0);
    try {
        return eval('(' + text + ')');
    } catch (e) {
        return void (0);
    }
}