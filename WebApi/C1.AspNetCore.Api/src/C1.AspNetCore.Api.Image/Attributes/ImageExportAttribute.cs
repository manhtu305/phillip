﻿using System;

#if !ASPNETCORE
using System.Web.Http.ModelBinding;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace C1.Web.Api.Image
{
    /// <summary>
    /// An attribute to apply to action parameters for model binding for image export.
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = true)]
    public class ImageExportModelBinderAttribute : ModelBinderAttribute
    {
        /// <summary>
        /// Initialize a new instance of the ImageExportModelBinderAttribute class.
        /// </summary>
        public ImageExportModelBinderAttribute()
            : base()
        {
            BinderType = typeof(BodyModelBinder<ImageExportSource>);
        }
    }
}