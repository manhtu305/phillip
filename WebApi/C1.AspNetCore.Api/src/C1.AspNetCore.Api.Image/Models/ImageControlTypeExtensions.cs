﻿using System.Collections.Generic;

namespace C1.Web.Api.Image
{
    internal static class ImageControlTypeExtensions
    {
        private static readonly Dictionary<ImageControlType, string> ctorMap = new Dictionary<ImageControlType, string>
        {
            { ImageControlType.FlexChart, "wijmo.chart.FlexChart" },
            { ImageControlType.FlexPie, "wijmo.chart.FlexPie" },
            { ImageControlType.LinearGauge, "wijmo.gauge.LinearGauge" },
            { ImageControlType.RadialGauge, "wijmo.gauge.RadialGauge" },
            { ImageControlType.BulletGraph, "wijmo.gauge.BulletGraph" },
            { ImageControlType.Sunburst, "wijmo.chart.hierarchical.Sunburst" },
            { ImageControlType.FlexRadar, "wijmo.chart.radar.FlexRadar" },
            { ImageControlType.TreeMap, "wijmo.chart.hierarchical.TreeMap" }
        };

        /// <summary>
        /// Converts ImageControlType enum to a fully qualified Wijmo constructor.
        /// </summary>
        internal static string ToConstuctor(this ImageControlType type)
        {
            return ctorMap[type];
        }
    }
}