﻿using System;
using System.Xml.Serialization;

namespace C1.Web.Api.Image
{
    /// <summary>
    /// The exchange data model used for sending image export requests.
    /// </summary>
    [Serializable]
    public class ImageExportSource : ExportSource, IExporterSource
    {
        private float height;
        private float width;

        /// <summary>
        /// The vertical size of the image.
        /// </summary>
        public float Height
        {
            get { return height <= 0 ? 600 : height; }
            set { height = value; }
        }

        /// <summary>
        /// The horizontal size of the image.
        /// </summary>
        public float Width
        {
            get { return width <= 0 ? 800 : width; }
            set { width = value; }
        }

        /// <summary>
        /// The serialized options.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// The name of the control.
        /// </summary>
        [XmlIgnore]
        public ImageControlType ControlType { get; set; }

        /// <summary>
        /// The full name of the control.
        /// </summary>
        public string ControlConstructor
        {
            get { return ControlType.ToConstuctor(); }
            set { /* don't do anything - fine for our prototype */ }
        }

        IExporter IExporterSource.CreateExporter()
        {
            return new ImageExporter();
        }
    }
}