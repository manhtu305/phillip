﻿namespace C1.Web.Api.Image
{
    /// <summary>
    /// Enum representing the type of controls that can be exported to an image.
    /// </summary>
    public enum ImageControlType
    {
        /// <summary>
        /// FlexChart control.
        /// </summary>
        FlexChart,
        /// <summary>
        /// FlexPie control.
        /// </summary>
        FlexPie,
        /// <summary>
        /// LinearGauge control.
        /// </summary>
        LinearGauge,
        /// <summary>
        /// RadialGauge control.
        /// </summary>
        RadialGauge,
        /// <summary>
        /// BulletGraph control.
        /// </summary>
        BulletGraph,
        /// <summary>
        /// Sunburst control.
        /// </summary>
        Sunburst,
        /// <summary>
        /// FlexRadar control.
        /// </summary>
        FlexRadar,
        /// <summary>
        /// TreeMap control.
        /// </summary>
        TreeMap
    }
}