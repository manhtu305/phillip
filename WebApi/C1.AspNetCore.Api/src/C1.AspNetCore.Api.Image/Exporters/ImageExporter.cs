﻿using C1.Util.Licensing;
using System;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using C1.Web.Api.WebDriver;
#if !ASPNETCORE
using System.ComponentModel;
#endif

namespace C1.Web.Api.Image
{
    internal static class WebResources
    {
        private const string prefix = AssemblyInfo.NamePrefix + ".Image.Resources.";

        private static string[] dirs = { "css", "js" };
        private static string[] files = 
        { 
            "css/wijmo.min.css",
            "js/wijmo.min.js",
            "js/wijmo.chart.min.js",
            "js/wijmo.chart.analytics.min.js",
            "js/wijmo.chart.hierarchical.min.js",
            "js/wijmo.chart.radar.min.js",
            "js/wijmo.gauge.min.js",
            "js/c1.wijmo.js"
        };

        public static string PrepareResources()
        {
            var path = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
            foreach (var dir in dirs)
            {
                var dirPath = Path.Combine(path, dir);
                if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }
            }
            foreach (var file in files)
            {
                var filePath = Path.Combine(path, file);
                using (var fs = File.Create(filePath))
                {
                    GetResource(GetResourceName(file)).CopyTo(fs);
                }
            }

            return path;
        }

        private static string GetResourceName(string id)
        {
            return prefix + id.Replace("/", ".");
        }

        private static Stream GetResource(string name)
        {
            return System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(name);
        }
    }

    /// <summary>
    /// Exporter that is responsible for converting an ImageExportSource to an image.
    /// </summary>
#if !ASPNETCORE
    [LicenseProvider]
#endif
    public class ImageExporter : IExporter<ImageExportSource>
    {
        private const string controlId = "control";
        private readonly bool _needRenderEvalInfo;
        private static string _webResourcesPath;

        /// <summary>
        /// Create an ImageExporter instance.
        /// </summary>
        public ImageExporter()
        {
            _needRenderEvalInfo = LicenseHelper.NeedRenderEvalInfo<LicenseDetector>();
        }

        private string GetTempFilePath()
        {
            return Path.Combine(_webResourcesPath, string.Format("{0}.html", Path.GetRandomFileName()));
        }

        private async Task CreateFileAsync(string path, string contents)
        {
            using (var stream = File.Create(path))
            using (var writer = new StreamWriter(stream))
            {
                await writer.WriteAsync(contents);
            }
        }

        private static bool TryDeleteFile(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                return true;
            }
            catch 
            {
                return false;
            }
        }

        // TODO: use TaskCompletionSource to wait until ready (a certain mark appears in the page)
        private async Task<bool> ResourcesReady(PhantomJSDriver driver)
        {
            await Task.Delay(100);
            return true;
        }

        /// <summary>
        /// Execute the export.
        /// </summary>
        /// <param name="exportSource">The export options.</param>
        /// <param name="outputStream">The export result.</param>
        /// <returns>The task.</returns>
        public async Task ExportAsync(ImageExportSource exportSource, Stream outputStream)
        {
            if (string.IsNullOrEmpty(_webResourcesPath) || !Directory.Exists(_webResourcesPath))
            {
                _webResourcesPath = WebResources.PrepareResources();
            }

            // get file path in local storage for temp file
            var tempFilePath = GetTempFilePath();

            // get html representation of the Source object
            var html = exportSource.ToHtml();

            try
            {
                // create a temp file
                await CreateFileAsync(tempFilePath, html);
#if DEBUG
                System.Diagnostics.Process.Start(tempFilePath);
#endif
                using (var driver = new PhantomJSDriver())
                {
                    var tempUri = new Uri(tempFilePath);
                    driver.Navigate().GoToUrl(tempUri.AbsoluteUri);

                    if (await ResourcesReady(driver))
                    {
                        var control = driver.FindElement(By.CssSelector(string.Format("#{0}", controlId)));
                        var screenshot = driver.TakeScreenshot();
                        var outBytes = screenshot.AsByteArray;

                        using (var stream = new MemoryStream(outBytes))
                        using (var bitmap = new Bitmap(stream))
                        {
                            // get position and dimensions of the control
                            var area = new Rectangle(control.Location, control.Size);

                            // focus on control in the image via cloning
                            using (var clone = bitmap.Clone(area, bitmap.PixelFormat))
                            {
                                DrawEvalInfo(clone);
                                clone.Save(outputStream, exportSource.Type.ToImageFormat());
                            }
                        }
                    }
                }
            }
            finally
            {
#if DEBUG
#else
                TryDeleteFile(tempFilePath);
#endif
            }
        }

        private void DrawEvalInfo(Bitmap image)
        {
            if (!_needRenderEvalInfo)
            {
                return;
            }

            LicenseHelper.DrawEvalInfo(image);
        }

        Task IExporter.ExportAsync(object exportSource, Stream outputStream)
        {
            return this.ExportAsync((ImageExportSource)exportSource, outputStream);
        }
    }
}