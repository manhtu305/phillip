﻿using System.IO;
using System.Xml;
using System.Xml.Xsl;

namespace C1.Web.Api.Image
{
    /// <summary>
    /// Generate the HTML used to take screenshot by XSLT.
    /// </summary>
    internal static class ImageCaptureSourceGenerator
    {
        private const string xsltRes = AssemblyInfo.NamePrefix + ".Image.Transforms.ImageExportSourceToHtml.xslt";

        private static XslCompiledTransform _transform;

        static ImageCaptureSourceGenerator()
        {
            _transform = new XslCompiledTransform();
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            using (var reader = XmlReader.Create(assembly.GetManifestResourceStream(xsltRes)))
            {
                _transform.Load(reader);
            }
        }

        /// <summary>
        /// Creates an HTML representation of the ImageInputSource object.
        /// </summary>
        public static string ToHtml(this ImageExportSource source)
        {
            var stream = C1XmlHelper.SerializeToStream(source, null, new XmlWriterSettings
            {
                OmitXmlDeclaration = true
            });

            if (stream == null)
            {
                return string.Empty;
            }

            stream.Position = 0;
            using (var result = new StringWriter())
            using (var reader = XmlReader.Create(stream))
            {
                _transform.Transform(reader, null, result);
                return result.ToString();
            }
        }
    }
}