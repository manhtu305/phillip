﻿using C1.Web.Api.Storage;
using C1.Web.Api.Storage.CloudStorage;
using Google.Apis.Auth.OAuth2;


#if ASPNETCORE || NETCORE
namespace Microsoft.AspNetCore.Builder
#else
namespace Owin
#endif
{
  /// <summary>
  /// The extensions of <see cref="StorageProviderManager"/>.
  /// </summary>
  public static class CloudStorageProviderManagerExtensions
  {
#if !NETCORE
    /// <summary>	
    /// Add a <see cref="AWSStorageProvider"/> to <see cref="StorageProviderManager"/> with the specified key and some parameters.
    /// </summary>
    /// <param name="manager">The <see cref="AWSStorageProvider"></see></param>
    /// <param name="key">The key value must be AWS.</param>
    /// <param name="accessKey">Your AWS access key.</param>
    /// <param name="secretKey">Your AWS secret key.</param>
    /// <param name="bucketName">Your AWS bucket name.</param>
    /// <param name="region">Your region, eg : us-east-1.</param>
    /// <returns></returns>
    public static StorageProviderManager AddAWSStorage(this StorageProviderManager manager, string key, string accessKey, string secretKey, string bucketName, string region)
    {
      manager.Add(key, new AWSStorageProvider(accessKey, secretKey, bucketName, region));
      return manager;
    }

    /// <summary>
    /// Add a <see cref="AzureStorageProvider"/> to <see cref="StorageProviderManager"/> with the specified key and some parameters.
    /// </summary>
    /// <param name="manager">The <see cref="AzureStorageProvider"></see></param>
    /// <param name="key">The key value must be Azure.</param>
    /// <param name="connectionString">Your Azure connection string.</param>
    /// <returns></returns>
    public static StorageProviderManager AddAzureStorage(this StorageProviderManager manager, string key, string connectionString)
    {
      manager.Add(key, new AzureStorageProvider(connectionString));
      return manager;
    }
#endif
    /// <summary>
    /// Add a <see cref="DropBoxStorageProvider"/> to <see cref="StorageProviderManager"/> with the specified key and some parameters.
    /// </summary>
    /// <param name="manager">The <see cref="DropBoxStorageProvider"></see></param>
    /// <param name="key">The key value must be DropBox.</param>
    /// <param name="accesstoken">Your access token.</param>
    /// <param name="applicationName">Your application name.</param>
    /// <returns></returns>
    public static StorageProviderManager AddDropBoxStorage(this StorageProviderManager manager, string key, string accesstoken, string applicationName)
    {
      manager.Add(key, new DropBoxStorageProvider(accesstoken, applicationName));
      return manager;
    }

    /// <summary>
    /// Add a <see cref="DropBoxStorageProvider"/> to <see cref="StorageProviderManager"/> with the specified key and some parameters.
    /// </summary>
    /// <param name="manager">The <see cref="DropBoxStorageProvider"></see></param>
    /// <param name="key">The key value must be DropBox.</param>
    /// <param name="credential">Credential information is created by your token.json file.</param>
    /// <param name="applicationName">Your application name.</param>
    /// <returns></returns>
    public static StorageProviderManager AddGoogleDriveStorage(this StorageProviderManager manager, string key, UserCredential credential, string applicationName)
    {
      manager.Add(key, new GoogleDriveStorageProvider(credential, applicationName));
      return manager;
    }

    /// <summary>
    /// Add a <see cref="OneDriveStorageProvider"/> to <see cref="StorageProviderManager"/> with the specified key and some parameters.
    /// </summary>
    /// <param name="manager">The <see cref="OneDriveStorageProvider"></see></param>
    /// <param name="key">The key value must be OneDrive.</param>
    /// <param name="accessToken">Your access token</param>
    /// Please read CLOUD API document to know how to get accessToken.
    /// <returns></returns>
    public static StorageProviderManager AddOneDriveStorage(this StorageProviderManager manager, string key, string accessToken)
    {
      manager.Add(key, new OneDriveStorageProvider(accessToken));
      return manager;
    }
  }
}
