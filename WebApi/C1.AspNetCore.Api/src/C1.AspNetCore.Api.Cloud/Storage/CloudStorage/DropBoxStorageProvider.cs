﻿using C1.Web.Api.Storage;
using System;
using System.Collections.Specialized;
using Dropbox.Api;

namespace C1.Web.Api.Storage.CloudStorage
{
	/// <summary>
	/// The DropBox cloud storage provider.
	/// </summary>
	public class DropBoxStorageProvider : IStorageProvider
	{
		/// <summary>
		/// The DropBox cloud access token.
		/// </summary>
		public string AccessToken;
		/// <summary>
		/// The DropBox cloud application name.
		/// </summary>
		public string ApplicationName;

		/// <summary>
		/// Create a <see cref="DropBoxStorageProvider"/> to <see cref="StorageProviderManager"/> with the specified key and some parameters.
		/// </summary>
		/// <param name="accesstoken">Your access token.</param>
		/// <param name="applicationName">Your application name.</param>
		public DropBoxStorageProvider(string accesstoken, string applicationName = "C1WebApi")
			: base()
		{
			AccessToken = accesstoken;
			ApplicationName = applicationName;
		}

		public IDirectoryStorage GetDirectoryStorage(string name, NameValueCollection args = null)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Gets the <see cref="DropBoxStorage"/> with specified name and arguments.
		/// </summary>
		/// <param name="name">The name of the <see cref="DropBoxStorage"/>.</param>
		/// <param name="args">The arguments</param>
		/// <returns>The <see cref="DropBoxStorage"/>.</returns>
		public IFileStorage GetFileStorage(string name, NameValueCollection args = null)
		{
			DropboxClientConfig clientConfig = new DropboxClientConfig(ApplicationName, 1);

			return new DropBoxStorage(name, AccessToken, clientConfig);
		}
	}
}