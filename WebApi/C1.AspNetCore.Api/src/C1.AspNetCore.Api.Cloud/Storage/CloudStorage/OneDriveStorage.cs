﻿using C1.Web.Api.Storage.Legacy;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace C1.Web.Api.Storage.CloudStorage
{
    /// <summary>
    /// OneDrive cloud storage
    /// </summary>
    public class OneDriveStorage : ICloudStorage
    {
        private string OneDriveDefaultUrl = "https://api.onedrive.com/v1.0/drive/root:/";
        /// <summary>
        /// File Name
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// File Path
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// File Path
        /// </summary>
        public string AccessToken { get; set; }
        private string Target { get; set; }

        /// <summary>
        /// Constructor OneDrive
        /// </summary>
        /// <param name="name">File Name</param>
        /// <param name="filePath">File Path</param>
        /// <param name="accessToken">Access token of OneDrive</param>
        [Obsolete]
        public OneDriveStorage(string name, string filePath, string accessToken)
        {
            Name = name;
            Path = filePath;
            AccessToken = accessToken;
        }

        /// <summary>
        /// Constructor OneDrive
        /// </summary>
        /// <param name="name">File Name</param>
        /// <param name="accessToken">Access token of OneDrive</param>
        public OneDriveStorage(string name, string accessToken)
        {
            name = name ?? string.Empty;
            string fileName, containerName, target = "";
            var filepath = name.Replace("\\", "/").TrimStart('/');
            if (name.Contains("targetpath"))
            {
                Utilities.SeparateName(name, out fileName, out containerName, out target);
                name = containerName + "/" + fileName;
                filepath = containerName + "/" + fileName.Replace("\\", "/").TrimStart('/');
            }

            Name = name;
            Path = filepath;
            Target = target;
            AccessToken = accessToken;
        }

        /// <summary>
        /// Exists property
        /// </summary>
        public bool Exists
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// ReadOnly property
        /// </summary>

        public bool ReadOnly
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// Gets the file with the specified path.
        /// </summary>
        /// <returns>MemoryStream</returns>
        public Stream Read()
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(BuildPathUrl(false));
            webRequest.KeepAlive = false;
            using (var webResponse = webRequest.GetResponse())
            using (var responseStream = webResponse.GetResponseStream())
            using (var reader = new BinaryReader(responseStream))
            {
                var buffer = reader.ReadBytes((int)webResponse.ContentLength);
                return new MemoryStream(buffer);
            }
        }
        /// <summary>
        /// Uploads file with the specified path.
        /// </summary>
        /// <param name="stream"></param>
        public void Write(Stream stream)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(BuildPathUrl(false));
            WriteFile(stream, webRequest);
        }

        private void Write(Stream stream, bool removeFile = false)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(BuildPathUrl(false, removeFile));
            WriteFile(stream, webRequest);
        }

        private void WriteFile(Stream stream, HttpWebRequest webRequest)
        {

            webRequest.KeepAlive = false;
            //set the put method
            webRequest.Method = "PUT";

            webRequest.ContentLength = stream.Length;
            Stream reqStream = webRequest.GetRequestStream();

            stream.Position = 0;

            byte[] buffer = new byte[1024 * 1024];
            int bytesRead = 0;

            while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) != 0)
            {
                reqStream.Write(buffer, 0, bytesRead);
                WebResponse response = webRequest.GetResponse();
            }
        }

        /// <summary>
        /// Deletes the file with the specified path.
        /// </summary>
        public void Delete()
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(BuildPathUrl(true));
            webRequest.Method = "DELETE";
            webRequest.KeepAlive = false;
            using (var httpResponse = (HttpWebResponse)webRequest.GetResponse()) { };
        }

        private void Delete(bool removeFile = false)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(BuildPathUrl(true, removeFile));
            webRequest.Method = "DELETE";
            webRequest.KeepAlive = false;
            using (var httpResponse = (HttpWebResponse)webRequest.GetResponse()) { };
        }

        private string BuildPathUrl(bool isDeleteRequest, bool removeFile = false)
        {
            string target;
            if (removeFile)
                target = Target;
            else
                target = Path;
            string content = ":/content";
            if (isDeleteRequest) content = "";
            return OneDriveDefaultUrl + target + content + "?access_token=" + AccessToken;
        }

        /// <summary>
        /// Gets all files and folders within the specified path.
        /// </summary>
        /// <returns>files and folders within the specified path</returns>
        public List<ListResult> List()
        {

            var url = OneDriveDefaultUrl + Path + ":/children?access_token=" + AccessToken;
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.KeepAlive = false;
            webRequest.Method = "GET";
            string result = "";
            var resultList = new List<ListResult>();

            using (var webResponse = webRequest.GetResponse())
            using (var responseStream = new StreamReader(webResponse.GetResponseStream()))
            {
                result = responseStream.ReadToEnd();
                var jsonListItem = JsonConvert.DeserializeObject<RootObject>(result);

                foreach (var value in jsonListItem.value)
                {
                    var item = new ListResult();
                    item.Name = value.name;
                    if (value.folder == null)
                    {
                        item.Type = ResultType.File;
                        item.Size = Utilities.ByteToKB((long)value.size);
                        item.ModifiedDate = value.lastModifiedDateTime.ToString("MM/dd/yyyy HH:mm:ss");
                    }
                    else
                    {
                        item.Type = ResultType.Folder;
                    }
                    resultList.Add(item);
                }
            }
            return resultList;
        }

        /// <summary>
        /// Move files from current path to destination path
        /// </summary>
        public void MoveFile()
        {
            var fileStream = Read();
            if (fileStream.Length != 0)
            {
                try
                {
                    Write(fileStream, true);
                    Delete();
                }
                catch (Exception e)
                {
                    Delete(true);
                }
            }
        }

        /// <summary>
        /// Create new folder with the specified path
        /// </summary>
        public void CreateFolder()
        {
            string path, name;
            getPathAndName(Path, out path, out name);
            var url = OneDriveDefaultUrl + path + ":/children?access_token=" + AccessToken;
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"name\":\"" + name + "\",\"folder\" : { \"childCount\" : 0 }}";
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
            }
        }

        private void getPathAndName(string input, out string path, out string name)
        {
            var nodes = input.Split('/').ToArray();
            name = nodes.Last();
            var arr = nodes.Take(nodes.Count() - 1).ToArray();
            path = string.Join("/", arr.ToArray());
        }
    }
}
