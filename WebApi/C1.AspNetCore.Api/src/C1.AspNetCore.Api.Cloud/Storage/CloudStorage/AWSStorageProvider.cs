﻿#if !NETCORE3 && !NETCORE
using System;
using System.Collections.Specialized;
using Amazon;
using Amazon.S3;

namespace C1.Web.Api.Storage.CloudStorage
{
    /// <summary>
    /// The AWS cloud storage provider.
    /// </summary>
    public class AWSStorageProvider : IStorageProvider
    {
        private string BucketName { get; set; }
        private string AccessKey { get; set; }
        private string SecretKey { get; set; }
        private AmazonS3Config Config { get; set; }

        /// <summary>
        /// Create a <see cref="AWSStorageProvider"/> to <see cref="StorageProviderManager"/> with the specified key and some parameters.
        /// </summary>
        /// <param name="accessKey">Your AWS access key.</param>
        /// <param name="secretKey">Your AWS secret key.</param>
        /// <param name="bucketName">Your AWS bucket name.</param>
        /// <param name="region">Your region, eg : us-east-1.</param>
        public AWSStorageProvider(string accessKey, string secretKey, string bucketName, string region)
            : base()
        {
            BucketName = bucketName;
            //-- Set the region or service url in the config
            AmazonS3Config config = new AmazonS3Config();
            config.RegionEndpoint = RegionEndpoint.GetBySystemName(region);
            AccessKey = accessKey;
            Config = config;
            SecretKey = secretKey;
        }

        public IDirectoryStorage GetDirectoryStorage(string name, NameValueCollection args = null)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the <see cref="AWSStorage"/> with specified name and arguments.
        /// </summary>
        /// <param name="name">The name of the <see cref="AWSStorage"/>.</param>
        /// <param name="args">The arguments</param>
        /// <returns>The <see cref="AWSStorage"/>.</returns>
        public IFileStorage GetFileStorage(string name, NameValueCollection args = null)
        {
            return new AWSStorage(name, BucketName, AccessKey, SecretKey, Config);
        }
    }
}

#endif