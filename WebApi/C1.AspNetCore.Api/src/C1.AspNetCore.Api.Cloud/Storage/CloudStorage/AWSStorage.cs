﻿#if !NETCORE3 && !NETCORE
using System;
using System.IO;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using C1.Web.Api.Storage.Legacy;
using Amazon.S3.IO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace C1.Web.Api.Storage.CloudStorage
{
    /// <summary>
    /// AWS cloud storage
    /// </summary>
    public class AWSStorage : ICloudStorage
    {
        string DummyFileName = "pleaseIgnoreMeImDummyFile.txt";
        /// <summary>
        /// File Name
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// File Path
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// BucketName of AWS
        /// </summary>
        private string BucketName { get; set; }
        /// <summary>
        /// AmazonS3Client instance
        /// </summary>
        private AmazonS3Client S3Client { get; set; }

        private String AccessKey { get; set; }
        private String SecretKey { get; set; }
        private string Target { get; set; }
        private AmazonS3Config Config { get; set; }

        /// <summary>
        /// Constructor AWSStorage
        /// </summary>
        /// <param name="name">File Name</param>
        /// <param name="bucketName">BucketName of AWS</param>
        /// <param name="s3Client">AmazonS3Client instance</param>
        [Obsolete("")]
        public AWSStorage(string name, string bucketName, AmazonS3Client s3Client)
        {
            Name = name;
            BucketName = bucketName;
            S3Client = s3Client;
        }

        /// <summary>
        /// Constructor AWSStorage
        /// </summary>
        /// <param name="name">Paths</param>
        /// <param name="bucketName">BucketName of AWS</param>
        /// <param name="accessKey">AmazonS3access key</param>
        /// <param name="secretKey">AmazonS3 secret key</param>
        public AWSStorage(string name, string bucketName, string accessKey, string secretKey, AmazonS3Config config)
        {
            string fileName, containerName, target;
            Utilities.SeparateName(name, out fileName, out containerName, out target);
            Name = fileName;
            BucketName = bucketName;
            AccessKey = accessKey;
            SecretKey = secretKey;
            Config = config;
            Target = target;
            S3Client = new AmazonS3Client(AccessKey, SecretKey, Config);
        }

        /// <summary>
        /// Exists property
        /// </summary>
        public bool Exists
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// ReadOnly property
        /// </summary>
        public bool ReadOnly
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// Gets the file with the specified path.
        /// </summary>
        /// <returns>MemoryStream</returns>
        public Stream Read()
        {
            return ReadStreamAsync().ConfigureAwait(false).GetAwaiter().GetResult();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private async System.Threading.Tasks.Task<Stream> ReadStreamAsync()
        {
            var stream = new MemoryStream();
            GetObjectRequest request = new GetObjectRequest();
            request.BucketName = BucketName;
            request.Key = Name;

#if !ASPNETCORE
			using (GetObjectResponse response = S3Client.GetObject(request))
			{
				await response.ResponseStream.CopyToAsync(stream).ConfigureAwait(false);
			}
#else
            using (GetObjectResponse response = await S3Client.GetObjectAsync(request))
            {
                await response.ResponseStream.CopyToAsync(stream).ConfigureAwait(false);
            }
#endif
            return stream;
        }

        /// <summary>
        /// Uploads file with the specified path.
        /// </summary>
        /// <param name="stream"></param>
        public void Write(Stream stream)
        {
            UploadAsync(stream).Wait();
        }

        private void Write(Stream stream, bool removeFile = false)
        {
            UploadAsync(stream, removeFile).Wait();
        }

        private async System.Threading.Tasks.Task UploadAsync(Stream stream, bool removeFile = false)
        {
            string target;
            if (removeFile)
                target = Target;
            else
                target = Name;
            stream.Position = 0;
            using (stream) // Load the data into memory stream from a data source other than a file
            {
                using (var transferUtility = new TransferUtility(S3Client))
                {
                    await transferUtility.UploadAsync(stream, BucketName, target).ConfigureAwait(false);
                }
            }
        }

        private async System.Threading.Tasks.Task DeleteAsync(bool removeFile = false)
        {
            var deleteObjectRequest = new DeleteObjectRequest
            {
                BucketName = BucketName,
                Key = removeFile ? Target : Name
            };
            await S3Client.DeleteObjectAsync(deleteObjectRequest).ConfigureAwait(false);
            S3DirectoryInfo directoryToDelete = new S3DirectoryInfo(S3Client, BucketName, deleteObjectRequest.Key);
            directoryToDelete.Delete(true); // true will delete recursively in folder inside
            
            }

        /// <summary>
        /// Deletes the file with the specified path.
        /// </summary>
        public void Delete()
        {
            DeleteAsync().Wait();
        }

        private void Delete(bool removeFile = false)
        {
            DeleteAsync(removeFile).Wait();
        }

        /// <summary>
        /// Gets all files and folders within the specified path.
        /// </summary>
        /// <returns>files and folders within the specified path</returns>
        public List<ListResult> List()
        {
            S3DirectoryInfo dir = new S3DirectoryInfo(S3Client, BucketName, Name.Replace("/", "\\"));
            var resultList = new List<ListResult>();
            foreach (IS3FileSystemInfo file in dir.GetFileSystemInfos())
            {
                var item = new ListResult();
                item.Name = file.Name;
                if (file.Type == FileSystemType.File)
                {
                    item.Type = ResultType.File;
                    item.ModifiedDate = file.LastWriteTimeUtc.ToString("MM/dd/yyyy HH:mm:ss");
                    var infor = (S3FileInfo)file;
                    item.Size = Utilities.ByteToKB(infor.Length);
                }
                else
                {
                    item.Type = ResultType.Folder;
                }
                if (item.Name != DummyFileName)
                    resultList.Add(item);
            }
            return resultList;
        }

        private async Task ListingObjectsAsync()
        {
            try
            {
                ListObjectsV2Request request = new ListObjectsV2Request
                {
                    BucketName = BucketName,
                    MaxKeys = 10000
                };
                ListObjectsV2Response response;
                do
                {
                    response = await S3Client.ListObjectsV2Async(request);

                    // Process the response.
                    foreach (S3Object entry in response.S3Objects)
                    {
                        Console.WriteLine("key = {0} size = {1}",
                                entry.Key, entry.Size);
                    }
                    Console.WriteLine("Next Continuation Token: {0}", response.NextContinuationToken);
                    request.ContinuationToken = response.NextContinuationToken;
                } while (response.IsTruncated);
            }
            catch (AmazonS3Exception amazonS3Exception)
            {
                Console.WriteLine("S3 error occurred. Exception: " + amazonS3Exception.ToString());
                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.ToString());
                Console.ReadKey();
            }
        }
        /// <summary>
        /// Move files from current path to destination path
        /// </summary>
        public void MoveFile()
        {
            var fileStream = Read();
            if (fileStream.Length != 0)
            {
                try
                {
                    Write(fileStream, true);
                    S3Client = new AmazonS3Client(AccessKey, SecretKey, Config);
                    Delete();
                }
                catch (Exception e)
                {
                    Delete(true);
                }
            }
        }

        /// <summary>
        /// Create new folder with the specified path
        /// </summary>
        public void CreateFolder()
        {
            CreateFolderAsync().Wait();
        }

        private async System.Threading.Tasks.Task CreateFolderAsync()
        {
            using (var transferUtility = new TransferUtility(S3Client))
            {
                await transferUtility.UploadAsync(new MemoryStream(), BucketName, Name + "/" + DummyFileName).ConfigureAwait(false);
            }
        }
    }
}

#endif