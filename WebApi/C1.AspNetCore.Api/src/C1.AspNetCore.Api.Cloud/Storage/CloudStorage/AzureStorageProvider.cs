﻿#if !NETCORE3 && !NETCORE
using System;
using System.Collections.Specialized;

namespace C1.Web.Api.Storage.CloudStorage
{
	/// <summary>
	/// The Azure cloud storage provider.
	/// </summary>
	public class AzureStorageProvider : IStorageProvider
	{
		/// <summary>
		/// Your Azure connection string.
		/// </summary>
		internal string ConnectionString { get; set; }

		/// <summary>
		/// Create a <see cref="AzureStorageProvider"/> to <see cref="StorageProviderManager"/> with the specified key and some parameters.
		/// </summary>
		/// <param name="connectionString">Your Azure connection string.</param>
		public AzureStorageProvider(string connectionString)
			  : base()
		{
			ConnectionString = connectionString;
		}

		public IDirectoryStorage GetDirectoryStorage(string name, NameValueCollection args = null)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Gets the <see cref="AzureStorage"/> with specified name and arguments.
		/// </summary>
		/// <param name="name">The name of the <see cref="AzureStorage"/>.</param>
		/// <param name="args">The arguments</param>
		/// <returns>The <see cref="AzureStorage"/>.</returns>
		public IFileStorage GetFileStorage(string name, NameValueCollection args = null)
		{
			return new AzureStorage(name, ConnectionString);
		}
	}
}
#endif