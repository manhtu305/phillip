﻿#if !NETCORE3 && !NETCORE
using System.IO;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Web;
using System;
using System.Collections.Generic;
using C1.Web.Api.Storage.Legacy;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.File;

namespace C1.Web.Api.Storage.CloudStorage
{
  /// <summary>
  /// AzureStorage cloud storage
  /// </summary>
  public class AzureStorage : ICloudStorage
  {
    // Maximum 10000 for loop to improve performance
    const int MaxEvent = 10000;
    const string DummyFileName = "pleaseIgnoreMeIfYouSee.txt";
    /// <summary>
    /// File Name
    /// </summary>
    public string Name { get; private set; }
    /// <summary>
    /// File Path
    /// </summary>
    public string Path { get; set; }

    private string Target { get; set; }
    private string ConnectionString { get; set; }
    private CloudStorageAccount CloudStorageAccount { get; set; }
    private CloudBlobClient BlobClient { get; set; }
    private CloudBlobContainer CloudBlobContainer { get; set; }
    private CloudBlockBlob BlockBlob { get; set; }

    /// <summary>
    /// Constructor AzureStorage
    /// </summary>
    /// <param name="name">File Name</param>
    /// <param name="path">File Path</param>
    /// <param name="connectionString">Connection string of Azure cloud</param>
    [Obsolete]
    public AzureStorage(string name, string path, string connectionString)
    {
      Name = name;
      Path = path;
      CloudStorageAccount = CloudStorageAccount.Parse(connectionString);
      BlobClient = CloudStorageAccount.CreateCloudBlobClient();
      CloudBlobContainer = BlobClient.GetContainerReference(Path);
      BlockBlob = CloudBlobContainer.GetBlockBlobReference(Name);
    }

    /// <summary>
    /// Constructor AzureStorage
    /// </summary>
    /// <param name="name">Paths</param>
    /// <param name="connectionString">Connection string of Azure cloud</param>
    public AzureStorage(string name, string connectionString)
    {
      string fileName, containerName, target;
      Utilities.SeparateName(name, out fileName, out containerName, out target);
      Name = fileName;
      Path = containerName;
      Target = target;
      ConnectionString = connectionString;

      CloudStorageAccount = CloudStorageAccount.Parse(connectionString);
      BlobClient = CloudStorageAccount.CreateCloudBlobClient();
      CloudBlobContainer = BlobClient.GetContainerReference(Path);
      BlockBlob = CloudBlobContainer.GetBlockBlobReference(Name);
    }

    private CloudBlockBlob CreateNewInstance(string pathTarget)
    {
      return CloudBlobContainer.GetBlockBlobReference(pathTarget);
    }

    /// <summary>
    /// Exists property
    /// </summary>
    public bool Exists
    {
      get
      {
        return ExistsAsync().ConfigureAwait(false).GetAwaiter().GetResult();
      }
    }

    /// <summary>
    /// ReadOnly property
    /// </summary>
    public bool ReadOnly
    {
      get
      {
        return true;
      }
    }

    /// <summary>
    /// Gets the file with the specified path.
    /// </summary>
    /// <returns>MemoryStream</returns>
    public Stream Read()
    {
            var MemoryStream = new MemoryStream();
            BlockBlob.DownloadToStream(MemoryStream);
            return MemoryStream;
    }

    private async System.Threading.Tasks.Task<CloudBlobContainer> CreateIfNotExistsAsync()
    {
      if (await CloudBlobContainer.CreateIfNotExistsAsync().ConfigureAwait(false))
      {
        await CloudBlobContainer.SetPermissionsAsync(
            new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob })
            .ConfigureAwait(false);
      }
      return CloudBlobContainer;
    }

    private async System.Threading.Tasks.Task UploadFromByteArrayAsync(Stream stream)
    {
      stream.Position = 0;
      await this.BlockBlob.UploadFromByteArrayAsync(ReadFully(stream, this.BlockBlob.StreamWriteSizeInBytes),
                       0, (int)stream.Length).ConfigureAwait(false);
    }

    private async System.Threading.Tasks.Task<bool> ExistsAsync()
    {
      return await BlockBlob.ExistsAsync().ConfigureAwait(false);
    }

    private async System.Threading.Tasks.Task CopyAsync(CloudBlockBlob blobCopy, CloudBlockBlob blobTarget)
    {
      using (var sourceStream = await blobTarget.OpenReadAsync().ConfigureAwait(false))
      {
        await blobCopy.UploadFromByteArrayAsync(ReadFully(sourceStream, blobCopy.StreamWriteSizeInBytes),
                     0, (int)sourceStream.Length).ConfigureAwait(false);
      }
    }

    private async System.Threading.Tasks.Task DeleteIfExistsAsync()
    {
      await BlockBlob.DeleteIfExistsAsync().ConfigureAwait(false);
#if !NETCORE
      foreach (IListBlobItem item in CloudBlobContainer.ListBlobs(Name + "/"))
      {
        if (item.GetType() == typeof(CloudBlobDirectory))
        {
          DeleteDirectoryRecursive((CloudBlobDirectory)item);
        }
        else
        {
          CloudBlockBlob file = (CloudBlockBlob)item;
          file.Delete();
        }
      }
#else
        var count = 0;

            BlobContinuationToken dirToken = null;
            do
            {
                var dirResult = await CloudBlobContainer.ListBlobsSegmentedAsync(Name + "/", dirToken);
                dirToken = dirResult.ContinuationToken;
                foreach (var item in dirResult.Results)
                {
                    if (item is CloudBlobDirectory)
                    {
                        DeleteDirectoryRecursiveAsync((CloudBlobDirectory)item);
                    }
                    else
                    {
                        CloudBlockBlob file = (CloudBlockBlob)item;
                        await file.DeleteAsync();
                    }
                    count++;
                    if (count >= MaxEvent)
                        break;
                }
            } while (dirToken != null && count < MaxEvent);
#endif
    }
#if !NETCORE
    private static void DeleteDirectoryRecursive(CloudBlobDirectory directory)
    {
      foreach (IListBlobItem item in directory.ListBlobs())
      {
        if (item.GetType() == typeof(CloudBlockBlob))
        {
          CloudBlockBlob file = (CloudBlockBlob)item;
          file.Delete();
        }

        else if (item.GetType() == typeof(CloudBlobDirectory))
        {
          CloudBlobDirectory dir = (CloudBlobDirectory)item;
          DeleteDirectoryRecursive(dir);
        }
      }
    }
#else
    private async void DeleteDirectoryRecursiveAsync(CloudBlobDirectory directory)
    {
      var count = 0;

      BlobContinuationToken dirToken = null;
      do
      {
        var dirResult = await CloudBlobContainer.ListBlobsSegmentedAsync(Name + "/", dirToken);
        dirToken = dirResult.ContinuationToken;
        foreach (var item in dirResult.Results)
        {
          if (item is CloudBlobDirectory)
          {
            CloudBlobDirectory dir = (CloudBlobDirectory)item;
            DeleteDirectoryRecursiveAsync(dir);
          }
          else
          {
            CloudBlockBlob file = (CloudBlockBlob)item;
            await file.DeleteAsync();
          }
          count++;
          if (count >= MaxEvent)
            break;
        }
      } while (dirToken != null && count < MaxEvent);
    }
#endif


    /// <summary>
    /// Uploads file with the specified path.
    /// </summary>
    /// <param name="stream"></param>
    public void Write(Stream stream)
    {
      WriteBlob(stream);
    }

    private void WriteBlob(Stream stream)
    {
      BlockBlob.Properties.ContentType = Utilities.GetMimeMapping(Name);
      UploadFromByteArrayAsync(stream).ConfigureAwait(false).GetAwaiter();
    }

    static byte[] ReadFully(Stream input, int size)
    {
      byte[] buffer = new byte[size];
      using (MemoryStream ms = new MemoryStream())
      {
        int read;
        while ((read = input.Read(buffer, 0, size)) > 0)
        {
          ms.Write(buffer, 0, read);
        }
        return ms.ToArray();
      }
    }

    /// <summary>
    /// Deletes the file with the specified path.
    /// </summary>
    public void Delete()
    {
      DeleteIfExistsAsync().ConfigureAwait(false).GetAwaiter();
    }

    private void Delete(bool moveFile = false)
    {
      if (moveFile)
        BlockBlob = CloudBlobContainer.GetBlockBlobReference(Path);
      DeleteIfExistsAsync().ConfigureAwait(false).GetAwaiter();
    }

    /// <summary>
    /// Gets all files and folders within the specified path.
    /// </summary>
    /// <returns>files and folders within the specified path</returns>
    public List<ListResult> List()
    {
      var resultList = new List<ListResult>();
#if !NETCORE
      var blobs = CloudBlobContainer.ListBlobs(Name + "/");

      foreach (var blob in blobs)
      {
        var item = new ListResult();
        if (blob.GetType() == typeof(CloudBlobDirectory))
        {
          CloudBlobDirectory directory = (CloudBlobDirectory)blob;
          item.Name = directory.Prefix.ToString().Substring(Name.Length).Replace("/", "");
          item.Type = ResultType.Folder;
        }
        else
        {
          CloudBlob cloudBlob = (CloudBlob)blob;
          cloudBlob.FetchAttributes();
          item.Size = (ulong)cloudBlob.Properties.Length;
          item.ModifiedDate = cloudBlob.Properties.LastModified==null?"n/a":cloudBlob.Properties.LastModified.Value.ToString("MM/dd/yyyy HH:mm:ss");
          item.Name = cloudBlob.Name.Substring(Name.Length + 1);
          item.Type = ResultType.File;
        }
        if (item.Name != DummyFileName)
          resultList.Add(item);
      }
#else
			resultList = ReadTrafficReportsAsync(CloudBlobContainer, MaxEvent, resultList).ConfigureAwait(false).GetAwaiter().GetResult();
#endif
      return resultList;
    }

    private async Task<List<ListResult>> ReadTrafficReportsAsync(CloudBlobContainer container, int maxEvents, List<ListResult> resultList)
    {
      var count = 0;

      BlobContinuationToken dirToken = null;
      do
      {
        var dirResult = await container.ListBlobsSegmentedAsync(Name + "/", dirToken);
        dirToken = dirResult.ContinuationToken;
        foreach (var blob in dirResult.Results)
        {
          var item = new ListResult();
          if (blob is CloudBlobDirectory)
          {
            CloudBlobDirectory directory = (CloudBlobDirectory)blob;
            item.Name = directory.Prefix.ToString().Substring(Name.Length).Replace("/", "");
            item.Type = ResultType.Folder;
          }
          else
          {
            CloudBlob cloudBlob = (CloudBlob)blob;
            cloudBlob.FetchAttributes();
            item.Size = Utilities.ByteToKB(cloudBlob.Properties.Length);
            item.ModifiedDate = cloudBlob.Properties.LastModified == null ? "n/a" : cloudBlob.Properties.LastModified.Value.ToString("MM/dd/yyyy HH:mm:ss");
            item.Name = cloudBlob.Name.Substring(Name.Length + 1);
            item.Type = ResultType.File;
          }
          if (item.Name != DummyFileName)
            resultList.Add(item);
          count++;
          if (count >= maxEvents)
            break;
        }
      } while (dirToken != null && count < maxEvents);
      return resultList;
    }

    /// <summary>
    /// Move files from current path to destination path
    /// </summary>
    public void MoveFile()
    {
      try
      {
        CloudBlockBlob blobCopy = CloudBlobContainer.GetBlockBlobReference(Target);
        BlockBlob.Properties.ContentType = Utilities.GetMimeMapping(Name);
        CopyAsync(blobCopy, this.BlockBlob).ConfigureAwait(false).GetAwaiter();
        Delete();
      }
      catch (Exception e)
      {
        Delete(true);
      }
    }

    /// <summary>
    /// Create new folder with the specified path
    /// </summary>
    public void CreateFolder()
    {
      BlockBlob = CloudBlobContainer.GetBlockBlobReference(Name + "/" + DummyFileName);
#if !NETCORE
      BlockBlob.UploadFromByteArray(new byte[0], 0, 0);
#else
            BlockBlob.UploadFromByteArrayAsync(new byte[0], 0, 0);
#endif
    }
  }
}
#endif