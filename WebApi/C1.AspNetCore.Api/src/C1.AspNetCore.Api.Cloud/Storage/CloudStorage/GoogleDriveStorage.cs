﻿using Google.Apis.Drive.v3;
using System.IO;
using System.Collections.Generic;
using Google.Apis.Drive.v3.Data;
using System.Linq;
using C1.Web.Api.Storage.Legacy;
using System;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;

namespace C1.Web.Api.Storage.CloudStorage
{
    /// <summary>
    /// Google cloud storage
    /// </summary>
    public class GoogleDriveStorage : ICloudStorage
    {
        /// <summary>
        /// File Name
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Google itemId of file
        /// </summary>
        public string ItemId { get; set; }
        /// <summary>
        /// File Path
        /// </summary>
        public string Path { get; set; }

        private string Target { get; set; }
        private UserCredential Credential { get; set; }
        private string ApplicationName { get; set; }

        private DriveService DriveService { get; set; }

        /// <summary>
        /// Constructor Google
        /// </summary>
        /// <param name="name">File Name</param>
        /// <param name="path">File Path</param>
        /// <param name="itemId">Google itemId of file</param>
        /// <param name="driveService">DriveService instance</param>
        [Obsolete]
        public GoogleDriveStorage(string name, string path, string itemId, DriveService driveService)
        {
            Name = name;
            Path = path;
            ItemId = itemId;
            DriveService = driveService;
        }

        /// <summary>
        /// Constructor Google
        /// </summary>
        /// <param name="name">Paths</param>
        /// <param name="credential">Credential information</param>
        /// <param name="applicationName">Name of application</param>
        public GoogleDriveStorage(string name, UserCredential credential, string applicationName)
        {
            string fileName, containerName, itemID, target;
            SeparateName(name, out fileName, out containerName, out itemID, out target);

            Name = fileName;
            Path = containerName;
            ItemId = itemID;
            Target = target;
            Credential = credential;
            ApplicationName = applicationName;
            DriveService = CreateNewClientInstance();
        }

        private void SeparateName(string name, out string fileName, out string containerName, out string itemID, out string target)
        {
            itemID = null;
            if (name.Contains("targetpath"))
            {
                var targetNode = name.Split('?').ToArray();
                target = targetNode[1].Replace("targetpath=", "");
                name = targetNode.FirstOrDefault() + "?" + targetNode.Skip(2).FirstOrDefault();
            }
            else
            {
                target = "";
            }

            var nodes = name.Split('/').ToArray();
            fileName = nodes.Last();
            containerName = name.Replace(fileName, "");
            if (containerName.Last() == '/') containerName = containerName.Substring(0, containerName.Length - 1);
            if (target != "")
            {
                var arr = target.Split('/').ToArray();
                arr = arr.Take(arr.Count() - 1).ToArray();
                target = string.Join("/", arr.ToArray());

                if (target.Last() == '/') target = target.Substring(0, target.Length - 1);
            }
            if (fileName.IndexOf('?') != -1)
            {
                itemID = fileName.Split('?').LastOrDefault();

                itemID = itemID.Substring("itemID?".Length);

                fileName = fileName.Split('?').FirstOrDefault();
            }
        }

        private DriveService CreateNewClientInstance()
        {
            return new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = Credential,
                ApplicationName = ApplicationName,
            });
        }

        /// <summary>
        /// Exists property
        /// </summary>
        public bool Exists
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// ReadOnly property
        /// </summary>
        public bool ReadOnly
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// Gets the file with the specified path.
        /// </summary>
        /// <returns>MemoryStream</returns>
        public Stream Read()
        {
            var request = DriveService.Files.Get(FindIdByName(Name, false));
            var stream = new MemoryStream();
            request.Download(stream);

            return stream;
        }

        /// <summary>
        /// Uploads file with the specified path.
        /// </summary>
        /// <param name="stream"></param>
        public void Write(Stream stream)
        {
            WriteStream(stream, Path);
        }

        private void Write(Stream stream, bool removeFile = false)
        {
            if (removeFile)
                WriteStream(stream, Target, removeFile);
            else
                WriteStream(stream, Path);
        }

        private void WriteStream(Stream stream, string target, bool removeFile = false)
        {
            var mimeType = Utilities.GetMimeMapping(Name);
            Google.Apis.Drive.v3.Data.File body = new Google.Apis.Drive.v3.Data.File();
            body.Name = Name;
            body.Description = "My description"; // your file description
            body.MimeType = mimeType;

            var parentlist = new List<string>();
            parentlist.Add(FindIdByName(target, true, removeFile)); // your folder-id here (folder name: WebAPI)
            body.Parents = parentlist;
            stream.Position = 0;
            FilesResource.CreateMediaUpload request = DriveService.Files.Create(body, stream, mimeType);
            request.Upload();
        }

        private string FindIdByName(string name, bool isFolderSearch = false, bool removeFile = false)
        {
            var Id = "";
            if (!string.IsNullOrEmpty(ItemId) && !removeFile)
            {
                return ItemId;
            }

            if (name.Contains("/"))
            {
                name = name.Split('/').Last();
            }
            string pageToken = null;

            do
            {
                var request = DriveService.Files.List();
                if (isFolderSearch)
                    request.Q = "mimeType='application/vnd.google-apps.folder'";
                request.Spaces = "drive";
                request.Fields = "nextPageToken, files(id, name)";
                request.PageToken = pageToken;

                var result = request.Execute();

                foreach (var file in result.Files)
                {
                    if (file.Name == name)
                    {
                        Id = file.Id;
                        pageToken = result.NextPageToken;
                        break;
                    }
                }
                pageToken = result.NextPageToken;
            } while (pageToken != null);
            return Id;
        }
        /// <summary>
        /// Deletes the file with the specified path.
        /// </summary>
        public void Delete()
        {
            DriveService.Files.Delete(FindIdByName(Name)).Execute();
        }

        private void Delete(bool removeFile = false)
        {
            string target;
            if (removeFile)
                target = Target;
            else
                target = Name;
            DriveService.Files.Delete(FindIdByName(target)).Execute();
        }

        /// <summary>
        /// Gets all files and folders within the specified path.
        /// </summary>
        /// <returns>files and folders within the specified path</returns>
        public List<ListResult> List()
        {
            string search = string.Format("'{0}' in parents", FindIdByName(Name, true));

            var request = DriveService.Files.List();

            request.Q = search;
            request.Spaces = "drive";
            request.Fields = "nextPageToken, files(id, name, mimeType, size, modifiedTime)";
            FileList filesFeed = new FileList();
            filesFeed = request.Execute();

            var resultList = new List<ListResult>();
            IList<Google.Apis.Drive.v3.Data.File> files = new List<Google.Apis.Drive.v3.Data.File>();

            while (filesFeed.Files != null)
            {
                foreach (Google.Apis.Drive.v3.Data.File item in filesFeed.Files)
                {
                    var result = new ListResult();
                    result.Name = item.Name;
                    result.ItemID = item.Id;
                    result.Type = item.MimeType == "application/vnd.google-apps.folder" ? ResultType.Folder : ResultType.File;
                    result.Size = Utilities.ByteToKB(item.Size==null?0:item.Size.Value);
                    result.ModifiedDate = item.ModifiedTime==null?"n/a":item.ModifiedTime.Value.ToString("MM/dd/yyyy HH:mm:ss");
                    resultList.Add(result);
                }
                if (filesFeed.NextPageToken == null)
                {
                    break;
                }
                request.PageToken = filesFeed.NextPageToken;
                filesFeed = request.Execute();
            }
            return resultList;
        }

        /// <summary>
        /// Move files from current path to destination path
        /// </summary>
        public void MoveFile()
        {
            var fileStream = Read();
            if (fileStream.Length != 0)
            {
                try
                {
                    DriveService = CreateNewClientInstance();
                    Write(fileStream, true);
                    DriveService = CreateNewClientInstance();
                    Delete();
                }
                catch (Exception e)
                {
                    Delete(true);
                }
            }
        }

        /// <summary>
        /// Create new folder with the specified path
        /// </summary>
        public void CreateFolder()
        {
            Google.Apis.Drive.v3.Data.File fileMetadata = new Google.Apis.Drive.v3.Data.File();
            fileMetadata.Name = Name;
            fileMetadata.Description = "My description"; // your file description
            fileMetadata.MimeType = "application/vnd.google-apps.folder";

            var parentlist = new List<string>();
            parentlist.Add(FindIdByName(Target, true)); // your folder-id here (folder name: WebAPI)
            fileMetadata.Parents = parentlist;

            FilesResource.CreateRequest request = DriveService.Files.Create(fileMetadata);
            request.Execute();
        }
    }
}