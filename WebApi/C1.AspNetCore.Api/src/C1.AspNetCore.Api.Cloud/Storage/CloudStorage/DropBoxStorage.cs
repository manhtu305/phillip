﻿using System;
using System.IO;
using Dropbox.Api;
using Dropbox.Api.Files;
using System.Linq;
using System.Collections.Generic;
using C1.Web.Api.Storage.Legacy;

namespace C1.Web.Api.Storage.CloudStorage
{
    /// <summary>
    /// DropBox cloud storage
    /// </summary>
    public class DropBoxStorage : ICloudStorage
    {
        /// <summary>
        /// File Name
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// File Path
        /// </summary>
        public string Path { get; set; }
        private string Target { get; set; }
        private string AccessToken { get; set; }
        private DropboxClientConfig ClientConfig { get; set; }
        private DropboxClient DBClient;

        /// <summary>
        /// Constructor DropBox
        /// </summary>
        /// <param name="name">File Name</param>
        /// <param name="path">File Path</param>
        /// <param name="dbClient">DropboxClient instance</param>
        [Obsolete]
        public DropBoxStorage(string name, string path, DropboxClient dbClient)
        {
            Name = name;
            Path = path;
            DBClient = dbClient;
        }

        /// <summary>
        /// Constructor DropBox
        /// </summary>
        /// <param name="name">Paths</param>
        /// <param name="accessToken">Dropbox access token</param>
        /// <param name="clientConfig">Dropbox config</param>
        public DropBoxStorage(string name, string accessToken, DropboxClientConfig clientConfig)
        {
            string fileName, containerName, target;
            Utilities.SeparateName(name, out fileName, out containerName, out target);
            Name = fileName;
            Path = containerName;
            Target = target;
            AccessToken = accessToken;
            ClientConfig = clientConfig;
            DBClient = new DropboxClient(AccessToken, ClientConfig);
        }

        /// <summary>
        /// Exists property
        /// </summary>
        public bool Exists
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// ReadOnly property
        /// </summary>
        public bool ReadOnly
        {
            get
            {
                return true;
            }
        }
        /// <summary>
        /// Gets the file with the specified path.
        /// </summary>
        /// <returns>MemoryStream</returns>
        public Stream Read()
        {
            return ReadStreamAsync().ConfigureAwait(false).GetAwaiter().GetResult();
        }

        private async System.Threading.Tasks.Task<Stream> ReadStreamAsync()
        {
            var stream = new MemoryStream();

            using (var response = await DBClient.Files.DownloadAsync("/" + Name).ConfigureAwait(false))
            {
                (await response.GetContentAsStreamAsync().ConfigureAwait(false)).CopyTo(stream);
            }

            return stream;
        }

        private async System.Threading.Tasks.Task UploadAsync(Stream stream, bool moveFile = false)
        {
            string target = moveFile ? Target : Name;
            stream.Position = 0;
            using (DBClient)
            {
              await DBClient.Files.UploadAsync("/" + target,
                  Dropbox.Api.Files.WriteMode.Overwrite.Instance,
                  false, null, false, null, false, stream).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Uploads file with the specified path.
        /// </summary>
        /// <param name="stream"></param>
        public void Write(Stream stream)
        {
            UploadAsync(stream).Wait();
        }

        private void Write(Stream stream, bool moveFile = false)
        {
            UploadAsync(stream, moveFile).Wait();
        }

        private async System.Threading.Tasks.Task DeleteAsync(bool moveFile = false)
        {
            string target = moveFile ? Target : Name;
            using (DBClient)
            {
                await DBClient.Files.DeleteV2Async("/" + target).ConfigureAwait(false);
            }
        }
        /// <summary>
        /// Deletes the file with the specified path.
        /// </summary>
        public void Delete()
        {
            DeleteAsync().Wait();
        }

        private void Delete(bool moveFile = false)
        {
            DeleteAsync(moveFile).Wait();
        }

        /// <summary>
        /// Gets all files and folders within the specified path.
        /// </summary>
        /// <returns>files and folders within the specified path</returns>
        public List<ListResult> List()
        {
            var list = ListFileAsync().ConfigureAwait(false).GetAwaiter().GetResult();
            return list;
        }

        private async System.Threading.Tasks.Task<List<ListResult>> ListFileAsync()
        {
 
        var list = new ListFolderResult();

        using (DBClient)
        {
          list = await DBClient.Files.ListFolderAsync("/" + Name).ConfigureAwait(false);
        }
        var resultList = new List<ListResult>();
        foreach (var entry in list.Entries)
        {
          var item = new ListResult();
          item.Name = entry.Name;
                if (entry.IsFolder)
                {
                    item.Type = ResultType.Folder;
                }
                else
                {
                    item.Type = ResultType.File;
                    var file = entry.AsFile;
                    item.Size = Utilities.ByteToKB((long)file.Size);
                    item.ModifiedDate = file.ServerModified.ToString("MM/dd/yyyy HH:mm:ss");
                }
          resultList.Add(item);
        }
        return resultList;
    }

        /// <summary>
        /// Move files from current path to destination path
        /// </summary>
        public void MoveFile()
        {
            var fileStream = Read();
            if (fileStream.Length != 0)
            {
                try
                {
                    DBClient = new DropboxClient(AccessToken, ClientConfig);
                    Write(fileStream, true);
                    DBClient = new DropboxClient(AccessToken, ClientConfig);
                    Delete();
                }
                catch (Exception e)
                {
                    Delete(true);
                }
            }
        }

        /// <summary>
        /// Create new folder with the specified path
        /// </summary>
        public void CreateFolder()
        {
            var folderMetadata = DBClient.Files.EndCreateFolderBatch(DBClient.Files.BeginCreateFolderBatch(new List<string>() { "/" + Name }));
        }
    }
}