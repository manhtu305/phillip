﻿using System;
using System.Collections.Specialized;
using Google.Apis.Drive.v3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using System.Linq;

namespace C1.Web.Api.Storage.CloudStorage
{
	/// <summary>
	/// The GoogleDrive cloud storage provider.
	/// </summary>
	public class GoogleDriveStorageProvider : IStorageProvider
	{
		public DriveService DriveService { get; set; }

		private UserCredential Credential { get; set; }

		private string ApplicationName { get; set; }

		/// <summary>
		/// Create a <see cref="GoogleDriveStorageProvider"/> to <see cref="StorageProviderManager"/> with the specified key and some parameters.
		/// </summary>
		/// <param name="credential">Credential infor is created by your token.json file.</param>
		/// <param name="applicationName">Your application name.</param>
		public GoogleDriveStorageProvider(UserCredential credential, string applicationName = "WebAPI")
			: base()
		{
			Credential = credential;
			ApplicationName = applicationName;
		}

		public IDirectoryStorage GetDirectoryStorage(string name, NameValueCollection args = null)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Gets the <see cref="GoogleDriveStorage"/> with specified name and arguments.
		/// </summary>
		/// <param name="name">The name of the <see cref="GoogleDriveStorage"/>.</param>
		/// <param name="args">The arguments</param>
		/// <returns>The <see cref="GoogleDriveStorage"/>.</returns>
		public IFileStorage GetFileStorage(string name, NameValueCollection args = null)
		{
			return new GoogleDriveStorage(name, Credential, ApplicationName);
		}


	}
}