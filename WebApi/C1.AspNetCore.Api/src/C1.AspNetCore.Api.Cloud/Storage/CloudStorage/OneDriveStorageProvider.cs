﻿using System;
using System.Collections.Specialized;

namespace C1.Web.Api.Storage.CloudStorage
{
	/// <summary>
	/// The OneDrive cloud storage provider.
	/// </summary>
	public class OneDriveStorageProvider : IStorageProvider
	{
		public string AccessToken;

		/// <summary>
		/// Create a <see cref="OneDriveStorageProvider"/> to <see cref="StorageProviderManager"/> with the specified key and some parameters.
		/// </summary>
		/// <param name="accessToken">Your access token</param>
		public OneDriveStorageProvider(string accessToken)
			: base()
		{
			AccessToken = accessToken;
		}

		/// <summary>
		/// Gets the <see cref="OneDriveStorage"/> with specified name and arguments.
		/// </summary>
		/// <param name="name">The name of the <see cref="OneDriveStorage"/>.</param>
		/// <param name="args">The arguments</param>
		/// <returns>The <see cref="OneDriveStorage"/>.</returns>
		public IFileStorage GetFileStorage(string name, NameValueCollection args = null)
		{
			return new OneDriveStorage(name, AccessToken);
		}

		public IDirectoryStorage GetDirectoryStorage(string name, NameValueCollection args = null)
		{
			throw new NotImplementedException();
		}
	}
}
