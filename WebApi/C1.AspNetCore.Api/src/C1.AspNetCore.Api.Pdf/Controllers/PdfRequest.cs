﻿using C1.Web.Api.Document;
using C1.Web.Api.Document.Models;
using C1.Web.Api.Pdf.Models;
using System.Collections.Generic;

namespace C1.Web.Api.Pdf
{
    /// <summary>
    /// Defines pdf request.
    /// </summary>
    internal class PdfRequestContext : DocumentRequestContext<PdfDocumentSource>
    {
        private Cache<PdfDocumentSource> _cache = null;

        public PdfRequestContext(string path)
        {
            Path = path;
        }

        /// <summary>
        /// Gets or sets the path of the pdf file.
        /// </summary>
        internal string Path
        {
            get;
            private set;
        }

        private Cache<PdfDocumentSource> Cache
        {
            get
            {
                return _cache ?? (_cache = PdfCacheManager.Instance.GetOrCreate(Path, RequestParams));
            }
        }

        protected override PdfDocumentSource GetDocument()
        {
            return Cache.Content;
        }

        public ExecutionInfo GetPdfExecutionInfo()
        {
            var info= new ExecutionInfo(Cache, Cache.Content);
            return info;
        }

        public DocumentStatus GetStatus()
        {
            return new DocumentStatus(Cache, Cache.Content);
        }

        public IDocumentFeatures GetFeatures()
        {
            return Cache.Content.GetFeatures();
        }

        public IEnumerable<SearchResult> Search(int startPageIndex, SearchScope scope, SearchOptions options)
        {
            return Cache.Content.Search(startPageIndex, scope, options);
        }
    }
}
