﻿using System.Collections.Generic;
using C1.Web.Api.Document;
using System;
using System.Linq;
using C1.Web.Api.Document.Models;
#if !ASPNETCORE
using System.Web.Http;
using System.Net.Http;
using IActionResult = System.Web.Http.IHttpActionResult;
using FromQuery = C1.Web.Api.FromUriExAttribute;
using System.Web.Http.ModelBinding;
#else
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RoutePrefixAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;
#endif

namespace C1.Web.Api.Pdf
{
  /// <summary>
  /// Pdf controller
  /// </summary>
  [RoutePrefix("api/pdf")]
  public class PdfController : DocumentController
  {
    /// <summary>
    /// Gets the export formats supported by the specified pdf file.
    /// </summary>
    /// <param name="pdfPath">The full path of the pdf file.</param>
    /// <returns>An <see cref="IActionResult"/> type object with content of collection of <see cref="Document.Models.ExportDescription"/> type object.</returns>
    [HttpGet]
    [PathRoute("{*pdfPath}/$pdf/supportedformats")]
    public virtual IActionResult GetPdfSupportedFormats(string pdfPath)
    {
      var context = new PdfRequestContext(pdfPath);

      return ProcessPdfAction(context, () =>
      {
        var content = context.GetSupportedFormats();
        return Ok(content);
      });
    }

    /// <summary>
    /// Exports the specified pdf to the specified export filter with options.
    /// </summary>
    /// <param name="pdfPath">The full path of the pdf file.</param>
    /// <param name="exportOptions">The export options.</param>
    /// <param name="exportFileName">The optional exported file name.</param>
    /// <returns>An <see cref="IActionResult"/> type object with content of exported file stream.</returns>
    [HttpGet]
    [PathRoute("{*pdfPath}/$pdf/export")]
    public virtual IActionResult ExportPdf_Get(string pdfPath, [FromQuery]IDictionary<string, string> exportOptions, string exportFileName = null)
    {
      return ExportPdf(pdfPath, exportOptions, exportFileName);
    }

    /// <summary>
    /// Exports the specified pdf to the specified export filter with options.
    /// </summary>
    /// <param name="pdfPath">The full path of the pdf.</param>
    /// <param name="exportOptions">The export options.</param>
    /// <param name="exportFileName">The optional exported file name.</param>
    /// <returns>An <see cref="IActionResult"/> type object with content of exported file stream.</returns>
    [HttpPost]
    [PathRoute("{*pdfPath}/$pdf/export")]
    public virtual IActionResult ExportPdf_Post(string pdfPath, [FromFormEx]IDictionary<string, string> exportOptions, [FromFormEx]string exportFileName = null)
    {
      return ExportPdf(pdfPath, exportOptions, exportFileName);
    }

    /// <summary>
    /// Exports the specified pdf to the specified export filter with options.
    /// </summary>
    /// <param name="pdfPath">The full path of the pdf.</param>
    /// <param name="exportOptions">The export options.</param>
    /// <param name="exportFileName">The optional exported file name.</param>
    /// <returns>An <see cref="IActionResult"/> type object with content of exported file stream.</returns>
    protected virtual IActionResult ExportPdf(string pdfPath, IDictionary<string, string> exportOptions, string exportFileName)
    {
      var context = new PdfRequestContext(pdfPath);
      if (exportOptions != null)
      {
        exportOptions = exportOptions.ToDictionary(p => p.Key, p => p.Value, StringComparer.OrdinalIgnoreCase);
      }

      return ProcessPdfAction(context, () =>
      {
        var options = context.GetExportFilterOptions(exportOptions);
        var filter = context.GetExportFilter(options);
        return File(() =>
              {
                return context.Export(filter, options);
              }, context.GetExportFullFileName(exportFileName, options));
      });
    }

    /// <summary>
    /// Gets the info of the specified pdf document source.
    /// </summary>
    /// <param name="pdfPath">The full path of the pdf file.</param>
    /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Document.Models.ExecutionInfo"/> type object.</returns>
    [HttpGet]
    [PathRoute("{*pdfPath}/$pdf")]
    public virtual IActionResult GetPdfInfo(string pdfPath)
    {
      RemoveCacheIfNedded(Request, pdfPath);

      var context = new PdfRequestContext(pdfPath);

      return ProcessPdfAction(context, () =>
      {
        var info = context.GetPdfExecutionInfo();
        info.StatusLocation = Location<string>(() => GetStatus);
        info.FeaturesLocation = Location<string>(() => GetPdfFeatures);
        return Ok(info);
      });
    }
#if !ASPNETCORE
    private void RemoveCacheIfNedded(HttpRequestMessage re, string pdfPath)
    {
      if (re.Headers.Contains("disable_cache"))
      {
        bool force = false;
        bool.TryParse(re.Headers.GetValues("disable_cache").First(), out force);
        if (force) PdfCacheManager.Instance.Remove(pdfPath);
      }
    }
#else
    private void RemoveCacheIfNedded(HttpRequest re, string pdfPath)
    {
        bool disableCache = false;        
        bool.TryParse(re?.Headers["disable_cache"], out disableCache);      
        if (disableCache) PdfCacheManager.Instance.Remove(pdfPath);
    }
#endif


    /// <summary>
    /// Gets the status of the pdf document source with specified file path.
    /// </summary>
    /// <param name="pdfPath">The full path of the pdf file.</param>
    /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Document.Models.DocumentStatus"/> type object.</returns>
    [HttpGet]
    [PathRoute("{*pdfPath}/$pdf/status")]
    public virtual IActionResult GetStatus(string pdfPath)
    {
      var context = new PdfRequestContext(pdfPath);

      return ProcessAction(() =>
      {
        var status = context.GetStatus();
        return Ok(status);
      });
    }

    /// <summary>
    /// Gets the features supported by the pdf document source instance with specified path.
    /// </summary>
    /// <param name="pdfPath">The full path of the pdf file.</param>
    /// <returns>An <see cref="IActionResult"/> type object with content of collection of <see cref="Document.Models.IDocumentFeatures"/> type object.</returns>
    [HttpGet]
    [PathRoute("{*pdfPath}/$pdf/features")]
    public virtual IActionResult GetPdfFeatures(string pdfPath)
    {
      var context = new PdfRequestContext(pdfPath);

      return ProcessAction(() =>
      {
        var content = context.GetFeatures();
        return Ok(content);
      });
    }

    /// <summary>
    /// Gets the search result in the pdf document source instance with specified path.
    /// </summary>
    /// <param name="pdfPath">The full path of the pdf file.</param>
    /// <param name="text">The text used to be searched.</param>
    /// <param name="matchCase">A boolean value indicates if search the value with case sensitive.</param>
    /// <param name="wholeWord">A boolean value indicates if search the value with matching a whole word.</param>
    /// <param name="startPageIndex">0-based index of the first page to search.</param>
    /// <param name="scope">The search scope.</param>
    /// <returns>An <see cref="IActionResult"/> type object with content of collection of <see cref="Document.Models.SearchResult"/> type object.</returns>
    [HttpGet]
    [PathRoute("{*pdfPath}/$pdf/search")]
    public virtual IActionResult GetSearchResults(string pdfPath, [ModelBinder(BinderType = typeof(StringModelBinder))]string text, bool matchCase = false, bool wholeWord = false,
        int startPageIndex = 0, SearchScope scope = SearchScope.WholeDocument)
    {
      var context = new PdfRequestContext(pdfPath);

      return ProcessAction(() =>
      {
        var options = new Document.Models.SearchOptions
        {
          Text = Uri.UnescapeDataString(text),
          MatchCase = matchCase,
          WholeWord = wholeWord
        };

        var content = context.Search(startPageIndex, scope, options);
        return Ok(content);
      });
    }

    private IActionResult ProcessPdfAction(PdfRequestContext context, Func<IActionResult> func)
    {
      context.SetRequestParamsGetter(() => Request.GetParams());
      return ProcessAction(func);
    }
  }
}
