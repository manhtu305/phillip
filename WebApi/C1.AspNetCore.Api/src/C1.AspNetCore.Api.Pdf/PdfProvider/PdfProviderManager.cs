﻿using C1.Web.Api.Configuration;
using C1.Web.Api.Document;
using C1.Web.Api.Pdf.Models;
using System.Collections.Specialized;

namespace C1.Web.Api.Pdf
{
    /// <summary>
    /// The manager for pdf providers.
    /// </summary>
    public sealed class PdfProviderManager : Manager<PdfProvider>
    {
        private readonly static PdfProviderManager _current = new PdfProviderManager();

        private PdfProviderManager()
        {
        }

        /// <summary>
        /// Gets the current pdf provider manager.
        /// </summary>
        public static PdfProviderManager Current
        {
            get { return _current; }
        }

        #region get provider

        /// <summary>
        /// Creates a <see cref="PdfDocumentSource"/> for the specified path.
        /// </summary>
        /// <param name="fullPath">The full path of the pdf.</param>
        /// <param name="args">The collection of HTTP query string variables.</param>
        /// <returns>A <see cref="PdfDocumentSource"/> object created for the specified path.</returns>
        /// <remarks>The fullPath argument starts with the provider key.</remarks>
        internal PdfDocumentSource CreatePdf(string fullPath, NameValueCollection args)
        {
            string key, path;
            var provider = GetProvider(fullPath, out key, out path);
            return provider == null ? null : provider.CreatePdf(key, path, args);
        }

        /// <summary>
        /// Gets the provider for the specified path.
        /// </summary>
        /// <param name="fullPath">The full path of the pdf.</param>
        /// <param name="key">The key of the provider.</param>
        /// <param name="path">The relative path of the pdf.</param>
        /// <returns>The <see cref="PdfProvider"/> for the specified path.</returns>
        /// <remarks>
        /// The first part of the fullPath is taken as the key.
        /// If there is no provider registed with the key, return null.
        /// </remarks>
        internal PdfProvider GetProvider(string fullPath, out string key, out string path)
        {
            // take first part as the key
            PathUtil.SeparatePathByFirst(fullPath, out key, out path);
            // has the provider in itself
            if (Contains(key))
                return Get(key);
            return null;
        }

        #endregion
    }
}
