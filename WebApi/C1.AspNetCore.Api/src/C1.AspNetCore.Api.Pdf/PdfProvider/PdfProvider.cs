﻿using C1.Web.Api.Pdf.Models;
using C1.Win.C1Document;
using System.Collections.Specialized;

namespace C1.Web.Api.Pdf
{
    /// <summary>
    /// Represents the provider for pdf.
    /// </summary>
    public abstract class PdfProvider
    {
        /// <summary>
        /// Creates the pdfDocument for the specified pdf path.
        /// </summary>
        /// <param name="path">The relative path of the pdf.</param>
        /// <param name="args">The collection of HTTP query string variables.</param>
        /// <returns>A <see cref="C1PdfDocumentSource"/> created for the specified path.</returns>
        /// <remarks>
        /// If overiding this method, you can create the pdfDocument by yourself, or modify the pdfDocument created by base.
        /// </remarks>
        protected abstract C1PdfDocumentSource CreatePdfDocument(string path, NameValueCollection args = null);

        /// <summary>
        /// Creates the pdf for the specified pdf path.
        /// </summary>
        /// <param name="providerName">The key used to register the provider.</param>
        /// <param name="path">The relative path of the pdf.</param>
        /// <param name="args">The collection of HTTP query string variables.</param>
        /// <returns>A <see cref="PdfDocumentSource"/> created for the specified path.</returns>
        internal PdfDocumentSource CreatePdf(string providerName, string path, NameValueCollection args)
        {
            // get the document source.
            var document = CreatePdfDocument(path, args);

            var pdfDoc = document as C1PdfDocumentSource;
            if (pdfDoc != null)
            {
                return new PdfDocumentSource(pdfDoc, providerName + "/" + path, args);
            }

            return null;
        }
    }

}
