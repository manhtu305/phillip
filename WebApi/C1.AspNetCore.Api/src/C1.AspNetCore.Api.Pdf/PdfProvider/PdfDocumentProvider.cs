﻿using System;
using System.Collections.Specialized;
using C1.Win.C1Document;
using C1.Web.Api.Storage;
using C1.Web.Api.Document;

namespace C1.Web.Api.Pdf
{
    /// <summary>
    /// Represents the provider for FlexReport.
    /// </summary>
    public class PdfDocumentProvider : PdfProvider
    {
        /// <summary>
        /// Initializes a new instance of <see cref="PdfDocumentProvider"/> instance.
        /// </summary>
        /// <param name="storage">The <see cref="IStorageProvider"/> used to access the FlexReport file.</param>
        public PdfDocumentProvider(IStorageProvider storage)
        {
            if (storage == null)
                throw new ArgumentNullException("storage");
            
            Storage = storage;
        }

        /// <summary>
        /// Gets the <see cref="IStorageProvider"/> used to access the FlexReport file.
        /// </summary>
        public IStorageProvider Storage { get; private set; }

        /// <summary>
        /// Creates the document for the specified pdf path.
        /// </summary>
        /// <param name="path">The relative path of the pdf.</param>
        /// <param name="args">The collection of HTTP query string variables.</param>
        /// <returns>A <see cref="C1PdfDocumentSource"/> created for the specified path.</returns>
        protected override C1PdfDocumentSource CreatePdfDocument(string path, NameValueCollection args)
        {
            if (Storage == null) return null;

            // get the file storage
            string filePath, last;
            PathUtil.SeparatePathByFirst(path, out filePath, out last);
            var fileStorage = Storage.GetFileStorage(filePath, args) as IFileStorage;

            if (fileStorage == null || !fileStorage.Exists) return null;

            // load pdf from stream
            var pdfDocument = new C1PdfDocumentSource();
            using (var stream = fileStorage.Read())
            {
                pdfDocument.LoadFromStream(stream);
            }

            return pdfDocument;
        }
    }
}
