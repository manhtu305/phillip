﻿using System.Collections.Specialized;
using C1.Web.Api.Pdf.Models;
using C1.Win.C1Document;

namespace C1.Web.Api.Pdf
{
    internal class PdfCacheManager : StorageDocumentCacheManager<PdfDocumentSource, C1PdfDocumentSource>
    {
        public static readonly PdfCacheManager Instance = new PdfCacheManager();

        private PdfCacheManager() { }

        protected override PdfDocumentSource CreateStorageDocument(string path, NameValueCollection args = null)
        {
            var doc = PdfProviderManager.Current.CreatePdf(path, args);
            if (doc == null)
                doc = new PdfDocumentSource(null, path, args);
            doc.Load();
            return doc;
        }
    }
}
