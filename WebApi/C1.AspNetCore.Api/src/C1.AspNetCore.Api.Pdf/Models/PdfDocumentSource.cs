﻿using C1.Win.C1Document;
using C1.Web.Api.Document.Models;
using C1.Web.Api.Storage;
using System.Collections.Specialized;
using System;
using System.Linq;

namespace C1.Web.Api.Pdf.Models
{
    internal class PdfDocumentFeatures : IDocumentFeatures
    {
        public bool NonPaginated { get { return false; } }

        public bool Paginated { get { return true; } }

        public bool TextSearchInPaginatedMode { get { return true; } }

        public bool PageSettings { get { return false; } }
    }

    internal class PdfDocumentSource : Document.Models.Document, IStorageDocument<C1PdfDocumentSource>
    {
        private static readonly IDocumentFeatures _features = new PdfDocumentFeatures();
        private IFileStorage _file;
        private NameValueCollection _fileArgs;
        private readonly C1PdfDocumentSource _pdf;

        public PdfDocumentSource(C1PdfDocumentSource pdf, string path, NameValueCollection fileArgs) : base(path)
        {
            _pdf = pdf;
            _fileArgs = fileArgs;
        }

        internal override Type LicenseDetectorType
        {
            get
            {
                return typeof(LicenseDetector);
            }
        }

        public C1PdfDocumentSource Document
        {
            get
            {
                return DocumentSource as C1PdfDocumentSource;
            }
        }

        public IFileStorage File
        {
            get
            {
                if (_file == null)
                {
                    IFileStorage file = null;
                    try
                    {
                        file = StorageProviderManager.GetFileStorage(Path, _fileArgs);
                    }
                    catch (InvalidOperationException)
                    {
                        throw new NotFoundException();
                    }

                    if (file == null || !file.Exists)
                    {
                        throw new NotFoundException();
                    }
                    _file = file;
                }

                return _file;
            }
        }

        public override bool HasOutlines
        {
            get
            {
                // pdf service doesn't support get outline for now.
                // So hide outlines from document source.
                return false;
            }
        }

        internal override IDocumentFeatures GetFeatures()
        {
            return _features;
        }

        protected override void InternalLoad()
        {
            C1PdfDocumentSource pdfDocument;
            if (_pdf != null)
                pdfDocument = _pdf;
            else
            {
                pdfDocument = new C1PdfDocumentSource();
                using (var stream = File.Read())
                {
                    pdfDocument.LoadFromStream(stream);
                }
            }
            pdfDocument.DocumentLocation = Path;
            DocumentSource = pdfDocument;
        }
    }
}
