﻿using C1.Web.Api.Pdf;
using System;

#if ASPNETCORE
using IAppBuilder = Microsoft.AspNetCore.Builder.IApplicationBuilder;
namespace Microsoft.AspNetCore.Builder
#else
namespace Owin
#endif
{
    /// <summary>
    /// Extension methods for <see cref="IAppBuilder"/>.
    /// </summary>
    public static class AppBuilderUseExtensions
    {
        /// <summary>
        /// Gets the current <see cref="PdfProviderManager"/> instance.
        /// </summary>
        /// <param name="app">The app builder.</param>
        /// <returns>The current <see cref="PdfProviderManager"/>.</returns>
        public static PdfProviderManager UsePdfProviders(this IAppBuilder app)
        {
            return PdfProviderManager.Current;
        }
    }
}
