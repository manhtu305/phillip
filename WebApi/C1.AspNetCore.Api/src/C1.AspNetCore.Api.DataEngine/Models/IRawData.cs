﻿using System.Collections;

namespace C1.Web.Api.DataEngine.Models
{
    /// <summary>
    /// Defines the interface for the returned raw data.
    /// </summary>
    public interface IRawData
    {
        /// <summary>
        /// The raw data.
        /// </summary>
        IEnumerable Value { get; set; }

        /// <summary>
        /// The total count of all the raw data.
        /// </summary>
        long TotalCount { get; set; }
    }
}
