﻿namespace C1.Web.Api.DataEngine.Models
{
    /// <summary>
    /// Enumeration with value types.
    /// </summary>
    public enum DataType
    {
        /// <summary>
        /// Anything except string, number, boolean, date, time and array.
        /// </summary>
        Object,
        /// <summary>
        /// A text.
        /// </summary>
        String,
        /// <summary>
        /// A number.
        /// </summary>
        Number,
        /// <summary>
        /// A boolean.
        /// </summary>
        Boolean,
        /// <summary>
        /// A date or time.
        /// </summary>
        Date,
        /// <summary>
        /// An array.
        /// </summary>
        Array
    }
}
