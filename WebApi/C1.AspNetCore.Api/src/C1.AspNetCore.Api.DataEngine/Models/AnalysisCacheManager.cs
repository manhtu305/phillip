﻿using C1.Web.Api.DataEngine.Models;

namespace C1.Web.Api.DataEngine
{
    internal class AnalysisCacheManager : CacheManager<Analysis>
    {
        public readonly static AnalysisCacheManager Instance = new AnalysisCacheManager();

        private AnalysisCacheManager() { }

        protected override string GenerateId(Analysis content)
        {
            return content.Token;
        }
    }
}
