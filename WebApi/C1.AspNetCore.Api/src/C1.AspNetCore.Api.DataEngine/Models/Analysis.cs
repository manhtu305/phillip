﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System;
using System.Collections;
using C1.Web.Api.DataEngine.Localization;
using C1.Web.Api.DataEngine.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using C1.Web.Api.DataEngine.Serialization;
using C1.Web.Api.DataEngine.Utils;
using System.Reflection;

namespace C1.Web.Api.DataEngine.Models
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal class Analysis
    {
        private Task<Dictionary<object[], object[]>> _task;
        private CancellationTokenSource _cancellationTokenSource;
        private IEnumerable _result;
        private Status _status;

        [JsonConverter(typeof(DataConverter))]
        public IEnumerable Result
        {
            get
            {
                return _result;
            }
            set
            {
                _result = value;
            }
        }

        public string Token { get; set; }

        public Status Status
        {
            get
            {
                return _status ?? (_status = new Status());
            }
        }

        private Dictionary<string, object> View { get; set; }
        internal string DataSourceKey { get; private set; }
        internal Exception Exception { get; set; }

        public Analysis(string dataSourceKey, Dictionary<string, object> view)
        {
            Token = Guid.NewGuid().ToString();
            View = view;
            _cancellationTokenSource = new CancellationTokenSource();
            DataSourceKey = dataSourceKey;
            _task = FlexPivotEngineProviderManager.Current.Get(dataSourceKey).GetAggregatedData(view, _cancellationTokenSource, Status.UpdateProgress);
        }

        internal void TryToGetResult()
        {
            switch (Status.ExecutingStatus)
            {
                case ExecutingStatus.Completed:
                    break;
                case ExecutingStatus.Cleared:
                    throw new Exception(Resources.OperationCancelled);
                case ExecutingStatus.Executing:
                    throw new Exception(Resources.OperationBeingExcuted);
                case ExecutingStatus.Exception:
                default:
                    throw Exception ?? new Exception();
            }
        }

        public void Clear()
        {
            if (_cancellationTokenSource != null && _cancellationTokenSource.IsCancellationRequested)
            {
                _cancellationTokenSource.Cancel();
            }
            AnalysisCacheManager.Instance.Remove(Token);
        }

        private void ProcessResult()
        {
            var data = _task.Result;
            var maxCount = FlexPivotEngineProviderManager.AnalysisResultMaxCount;
            // honor MaxCount.
            if (maxCount > 0 && data.Count > maxCount)
            {
                Status.Update(0, ExecutingStatus.Exception);
                _result = null;
                Exception = new Exception(string.Format(Resources.AggregatedResultOutOfRange, maxCount, "AnalysisResultMaxCount"));
            }
            else
            {
                Status.Update(100, ExecutingStatus.Completed);
                _result = GetFormattedData(data, View);
            }
        }

        internal void Refresh()
        {
            switch (_task.Status)
            {
                case TaskStatus.RanToCompletion:
                    ProcessResult();
                    break;
                case TaskStatus.Canceled:
                    Status.Update(0, ExecutingStatus.Cleared);
                    break;
                case TaskStatus.Faulted:
                    Status.Update(0, ExecutingStatus.Exception);
                    Exception = ErrorHelper.GetInnerException(_task.Exception);
                    break;
                default:
                    Status.ExecutingStatus = ExecutingStatus.Executing;
                    break;
            }
        }

        private static IDictionary<string, object> GetFieldProperties(object field)
        {
            var props = field as IDictionary<string, object>;
            if (props == null)
            {
                props = new Dictionary<string, object>();
                IEnumerator enumerator = (field as IEnumerable).GetEnumerator();
                while (enumerator.MoveNext())
                {
                    var src = enumerator.Current;
                    var type = src.GetType();
                    string name = (string)type.GetProperty("Name").GetValue(src);
                    object value = type.GetProperty("Value").GetValue(src);
                    props.Add(name, GetFieldPropertyValue(value));
                }
            }
            return props;
        }

        private static object GetFieldPropertyValue(object value)
        {
            PropertyInfo info = value.GetType().GetProperty("Value");
            if (info == null)
            {
                var array = new ArrayList();
                Type[] parameters = { typeof(System.Type) };
                Type[] args = { typeof(System.Object) };
                IEnumerator enumerator = (value as IEnumerable).GetEnumerator();
                while (enumerator.MoveNext())
                {
                    var src = enumerator.Current;
                    var type = src.GetType();
                    var method = type.GetMethod("ToObject", parameters);
                    var element = method.Invoke(src, args);
                    array.Add(element);
                }
                return array.ToArray();
            }
            return info.GetValue(value);
        }

        private static ArrayList GetFieldItems(Dictionary<string, object> view, string key)
        {
            var tempObjs = GetFieldProperties(view[key])["items"];
            ArrayList ArrayEx = new ArrayList();

            IEnumerable enumerable = tempObjs as IEnumerable;
            if (enumerable != null)
            {
                foreach (object element in enumerable)
                {
                    ArrayEx.Add(element);
                }
            }

            return ArrayEx.Count != 0 ? ArrayEx : new ArrayList();
        }

        private static IList GetFormattedData(Dictionary<object[], object[]> resultData, Dictionary<string, object> view)
        {
            var rowItems = GetFieldItems(view, "rowFields");
            var colItems = GetFieldItems(view, "columnFields");
            var valItems = GetFieldItems(view, "valueFields");
            var result = new List<Dictionary<string, object>>();
            foreach (var dataItem in resultData)
            {
                var item = new Dictionary<string, object>();
                var index = 0;
                foreach (var row in rowItems)
                {
                    item.Add(row.ToString(), dataItem.Key[index++]);
                }
                foreach (var col in colItems)
                {
                    item.Add(col.ToString(), dataItem.Key[index++]);
                }
                index = 0;
                foreach (var val in valItems)
                {
                    item.Add(val.ToString(), dataItem.Value[index++]);
                }
                result.Add(item);
            }
            return result;
        }
    }
}
