﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace C1.Web.Api.DataEngine.Models
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal class Status
    {
        [JsonConverter(typeof(StringEnumConverter))]
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Include)]
        public ExecutingStatus ExecutingStatus { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Include)]
        public float Progress { get; set; }

        internal void UpdateProgress(int progress)
        {
            Progress = progress;
        }

        internal void Update(int progress, ExecutingStatus status)
        {
            UpdateProgress(progress);
            ExecutingStatus = status;
        }

    }
}
