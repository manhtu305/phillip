﻿using C1.Web.Api.DataEngine.Serialization;
using Newtonsoft.Json;
using System.Collections;

namespace C1.Web.Api.DataEngine.Models
{
    internal class RawData : IRawData
    {
        [JsonConverter(typeof(DataConverter))]
        public IEnumerable Value { get; set; }
        public long TotalCount { get; set; }
    }
}
