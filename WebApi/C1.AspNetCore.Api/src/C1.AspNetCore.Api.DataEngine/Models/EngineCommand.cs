﻿namespace C1.Web.Api.DataEngine.Models
{
    internal enum EngineCommand
    {
        Fields,
        Analyses,
        Detail,
        RawData,
        Status,
        ResultData,
        Analysis,
        UniqueValues,
        Clear
    }
}
