﻿namespace C1.Web.Api.DataEngine.Models
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal enum ExecutingStatus
    {
        /// <summary>
        /// The operation is not executed. It is default.
        /// </summary>
        NotSet,
        /// <summary>
        /// The analysis is still executing.
        /// </summary>
        Executing,
        /// <summary>
        /// Some errors occur during executing the analysis.
        /// </summary>
        Exception,
        /// <summary>
        /// The analysis is finished.
        /// </summary>
        Completed,
        /// <summary>
        /// The analysis is removed.
        /// </summary>
        Cleared
    }
}
