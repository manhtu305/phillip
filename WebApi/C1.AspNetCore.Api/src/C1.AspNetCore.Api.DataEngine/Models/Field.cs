﻿using C1.Web.Api.DataEngine.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
#if !NETCORE
using DimT = C1.FlexPivot.Internal.DimensionType;
#endif

namespace C1.Web.Api.DataEngine.Models
{
    /// <summary>
    /// The field class.
    /// </summary>
    public class Field
    {
        private IList<Field> _subFields;

#region Properties
        /// <summary>
        /// Gets or sets a text to be displayed in the user interface.
        /// </summary>
        public string Header { get; set; }

        /// <summary>
        /// Gets or sets the name of the source column which the field is bound to.
        /// </summary>
        /// <remarks>
        /// It only works when connecting DataEngine or DataSource.
        /// </remarks>
        public string Binding { get; set; }

        /// <summary>
        /// Gets or sets the data type of the field.
        /// </summary>
        /// <remarks>
        /// It only works when connecting DataEngine or DataSource.
        /// </remarks>
        public DataType? DataType { get; set; }
#if !NETCORE
        /// <summary>
        /// Gets or sets the dimension type of the field.
        /// </summary>
        /// <remarks>
        /// It works when connecting a cube data.
        /// </remarks>
        public DimT? DimensionType { get; set; }
#endif
        /// <summary>
        /// Gets the child fields.
        /// </summary>
        /// <remarks>
        /// It only works when connecting a cube data.
        /// </remarks>
        public IList<Field> SubFields
        {
            get { return _subFields ?? (_subFields = new List<Field>()); }
        }

        /// <summary>
        /// Gets or sets the lookup values for this field.
        /// </summary>
        public Dictionary<string, object> Lookup { get; set; }
#endregion Properties

#region Methods
        /// <summary>
        /// Specifies whether the SubFields property should be serialized.
        /// </summary>
        /// <returns>If true, the property should be serialized. Otherwise, it should not be serialized.</returns>
        public bool ShouldSerializeSubFields()
        {
            return SubFields.Count > 0;
        }

        /// <summary>
        /// Convert the field generated from the DataEngine data and the in-memory data to a <see cref="C1.Web.Api.DataEngine.Models.Field"/> instance.
        /// </summary>
        /// <param name="oField">The field data.</param>
        /// <returns>A <see cref="C1.Web.Api.DataEngine.Models.Field"/> instance.</returns>
        internal static Field ConvertFromRDField(object oField)
        {
            var field = new Field();
            var pi = ReflectionHelper.GetPropertyInfo("Header", oField);
            if(pi != null)
            {
                field.Header = pi.GetValue(oField) as string;
            }
            pi = ReflectionHelper.GetPropertyInfo("Binding", oField);
            if (pi != null)
            {
                field.Binding = pi.GetValue(oField) as string;
            }
            pi = ReflectionHelper.GetPropertyInfo("DataType", oField);
            if (pi != null)
            {
                var value = pi.GetValue(oField) as Type;
                if (value == null)
                {
                    field.DataType = null;
                }
                else
                {
                    field.DataType = value.ConvertToDataType();
                }
            }
            return field;
        }
#if !NETCORE
        internal static DataType GetDataTypeFrom(DimT? dimensionType)
        {
            if(dimensionType == null)
            {
                return Models.DataType.Object;
            }

            switch (dimensionType.Value)
            {
                case DimT.Attribute:
                case DimT.Hierarchy:
                    return Models.DataType.String;
                case DimT.Measure:
                case DimT.Kpi:
                    return Models.DataType.Number;
                default:
                    return Models.DataType.Object;
            }
        }
        /// <summary>
        /// Converts the field generated from the cube data to a <see cref="Field"/> object.
        /// </summary>
        /// <param name="oField">The field data.</param>
        /// <returns>A <see cref="Field"/> instance</returns>
        internal static Field ConvertFromCubeField(object oField)
        {
            var field = ConvertFromRDField(oField);
            var pi = ReflectionHelper.GetPropertyInfo("DimensionType", oField);
            if (pi != null)
            {
                var value = pi.GetValue(oField);
                if(value == null)
                {
                    field.DimensionType = null;
                }
                else
                {
                    field.DimensionType = (DimT?)(int)(pi.GetValue(oField));
                }
            }

            field.DataType = field.DataType ?? GetDataTypeFrom(field.DimensionType);

            IEnumerable subFields = null;
            pi = ReflectionHelper.GetPropertyInfo("SubFields", oField);
            if (pi != null)
            {
                subFields = pi.GetValue(oField) as IEnumerable;
            }

            if (subFields != null)
            {
                foreach (var subField in subFields)
                {
                    field.SubFields.Add(ConvertFromCubeField(subField));
                }
            }

            return field;
        }
        
#endif
        #endregion Methods
    }
}
