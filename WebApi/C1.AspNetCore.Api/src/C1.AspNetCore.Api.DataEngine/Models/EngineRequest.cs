﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
#if !NETCORE
using System.Web.Script.Serialization;
#endif

namespace C1.Web.Api.DataEngine.Models
{
    /// <summary>
    /// Defines the class for the request of the cell detail data.
    /// </summary>
    public class DetailRequest: EngineRequestWithPaging
    {
        /// <summary>
        /// Gets or sets a <see cref="System.Object"/> array, which is used to specify some cell.
        /// </summary>
        /// <remarks>
        /// Firstly, you need add the values of the fields in rowFields in order into keys.
        /// Then add the values of the fields in columnFields in order.
        /// If some field value is null, JUST add null into array and DO NOT remove it.
        /// ou should keep the count of the keys array same as the count of the fields in rowFields and columnFields. 
        /// </remarks>
        public object[] Keys { get; set; }
    }

    /// <summary>
    /// Defines the class for the engine request with paging.
    /// </summary>
    public class EngineRequestWithPaging: EngineRequest
    {
        private int _skip = 0;
        private int _top = -1;

        /// <summary>
        /// Get or sets a <see cref="System.Int32"/>, which is used to exclude the first n entities. n is specified by this value.
        /// </summary>
        public int Skip
        {
            get { return _skip; }
            set { _skip = Math.Max(0, value); }
        }

        /// <summary>
        /// Gets or sets a <see cref="System.Int32"/>, which indicates the count of the data requested.
        /// </summary>
        public int Top
        {
            get { return _top; }
            set { _top = Math.Max(-1, value); }
        }

        /// <summary>
        /// Create a <see cref="EngineRequestWithPaging"/> instance according to the specified <see cref="EngineRequest"/> instance.
        /// </summary>
        /// <param name="er">The <see cref="EngineRequest"/> instance.</param>
        public EngineRequestWithPaging(EngineRequest er)
            :this()
        {
            View = er.View;
        }

        /// <summary>
        /// Create a <see cref="EngineRequestWithPaging"/> instance.
        /// </summary>
        public EngineRequestWithPaging() : base()
        {
        }
    }

    /// <summary>
    /// Defines the class for the engine request.
    /// </summary>
    public class EngineRequest
    {
#region Fields
        private const char PATH_SEPARATOR = '/';
#endregion Fields

#region Properties
        /// <summary>
        /// Gets or sets the analysis token.
        /// </summary>
        internal string AnalysisToken { get; set; }

        /// <summary>
        /// Gets or sets a <see cref="System.String"/>, which indicates the view definition.
        /// </summary>
        [JsonConverter(typeof(ViewConverter))]
        public Dictionary<string, object> View { get; set; }

        /// <summary>
        /// Specifies the field which you want to get the unique values.
        /// </summary>
        internal string FieldName { get; set; }

        internal EngineCommand Command { get; set; }

        internal string DataSourceKey { get; set; }

#endregion Properties
    }

    internal class ViewConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            if (objectType.IsAssignableFrom(typeof(string)))
            {
                return true;
            }
            return false;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
#if !NETCORE
            var jss = new JavaScriptSerializer();

            return jss.Deserialize<Dictionary<string, object>>(reader.Value.ToString());
#else
            return JsonConvert.DeserializeObject<Dictionary<string, object>>(reader.Value.ToString());
#endif
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }
    }
}
