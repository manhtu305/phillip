﻿using Newtonsoft.Json;

namespace C1.Web.Api.DataEngine.Serialization
{
    internal static class JsonSerializationHelper
    {
        internal static JsonSerializerSettings DataJsonSettings = new JsonSerializerSettings
        {
            DefaultValueHandling = DefaultValueHandling.Include,
            NullValueHandling = NullValueHandling.Include
        };
    }
}
