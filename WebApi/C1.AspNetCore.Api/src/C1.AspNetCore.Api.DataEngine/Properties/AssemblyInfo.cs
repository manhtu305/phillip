﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using C1.Util.Licensing;

// licensing support
// Studio Ultimate
[assembly: C1ProductInfo("SU", "757CCC59-F365-4325-A676-0674C656B7A2")]
// Studio Enterprise
[assembly: C1ProductInfo("SE", "724e8a91-af12-4a3b-9aeb-ef89612e692e")]

#if !ASPNETCORE
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("C1.Web.Api.DataEngine")]
[assembly: AssemblyDescription("ComponentOne ASP.NET DataEngine Services")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("GrapeCity, Inc.")]
[assembly: AssemblyProduct("C1.Web.Api.DataEngine")]
[assembly: AssemblyCopyright("Copyright © GrapeCity, Inc.  All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("850E08D8-012A-4BD2-BC42-5970B4F2D003")]
#endif

[assembly: AssemblyVersion(AssemblyInfo.Version)]
[assembly: AssemblyFileVersion(AssemblyInfo.Version)]

#if UNITTEST
[assembly: InternalsVisibleTo(AssemblyInfo.NamePrefix + ".Tests, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c1bd0a0680e736d0bea3881ff835b56a8cb56c82ed17edc4714140bed28d7d504ba64753dcf227a35b1bab1a43bd83d1fc916fdf98e6daf5aa19f216831b11334657352c4c848ea47cde573158d1c86ec7efbced83cb62ae2550bd0fd0da2cf48593567ceb65d1e02f6d96ebebdbb83e39db493bb2ca1c301eebca0cb8e5abe4")]
#endif