﻿#if !ASPNETCORE && !NETCORE
using System.Web.Http;
using Controller = System.Web.Http.ApiController;
using IActionResult = System.Web.Http.IHttpActionResult;
using FromQuery = C1.Web.Api.FromUriExAttribute;
#else
using Microsoft.AspNetCore.Mvc;
using RoutePrefix = Microsoft.AspNetCore.Mvc.RouteAttribute;
#endif
using C1.Web.Api.DataEngine.Models;
using Newtonsoft.Json;

namespace C1.Web.Api.DataEngine
{
    /// <summary>
    /// The data engine controller.
    /// </summary>
    [RoutePrefix("api/dataengine")]
    public partial class DataEngineController : C1ApiController
    {
        /// <summary>
        /// Gets all the fields information defined in the data.
        /// </summary>
        /// <param name="dataSourceKey">The data source key specifies the data.</param>
        /// <returns>The field list and the total data count.</returns>
        [HttpGet]
        [Route("{dataSourceKey}/fields")]
        public virtual IActionResult GetFields(string dataSourceKey)
        {
            return GetDataSourceProcessorResult(dataSourceKey, EngineCommand.Fields);
        }

        /// <summary>
        /// Gets the raw data.
        /// </summary>
        /// <param name="dataSourceKey">The data source key specifies the data.</param>
        /// <param name="skip">A number value which is used to exclude the first n entities. n is specified by this value</param>
        /// <param name="top">A number value which indicates the count of the data requested.</param>
        /// <returns>The raw data and the total count of all the raw data.</returns>
        [HttpGet]
        [Route("{dataSourceKey}")]
        public virtual IActionResult GetRawData(string dataSourceKey, int? skip = null, int? top = null)
        {
            var requestWithPaging = new EngineRequestWithPaging();
            if (skip.HasValue)
            {
                requestWithPaging.Skip = skip.Value;
            }
            if (top.HasValue)
            {
                requestWithPaging.Top = top.Value;
            }
            return GetDataSourceProcessorResult(dataSourceKey, EngineCommand.RawData, requestWithPaging);
        }

        /// <summary>
        /// Analyzes the data.
        /// </summary>
        /// <param name="dataSourceKey">The data source key specifies the data.</param>
        /// <param name="er">The engine request.</param>
        /// <returns>The analysis result information.</returns>
        [HttpPost]
        [Route("{dataSourceKey}/analyses")]
        public virtual IActionResult AnalyzeData(string dataSourceKey, [FromBody]EngineRequest er)
        {
            return GetDataSourceProcessorResult(dataSourceKey, EngineCommand.Analyses, er);
        }

        /// <summary>
        /// Gets a list of objects in the raw data source that define the content 
        /// of a specific cell in the output table.
        /// </summary>
        /// <param name="dataSourceKey">The data source key specifies the data.</param>
        /// <param name="er">The request for the cell detail raw data.</param>
        /// <returns>
        /// The raw data which the cell includes and the total count of the cell raw data.
        /// </returns>
        /// <remarks>Now only the DataEngine data and the in-memory data support this action. The cube data doesn't support it.</remarks>
        [HttpPost]
        [Route("{dataSourceKey}")]
        public virtual IActionResult GetDetail(string dataSourceKey, [FromBody]DetailRequest er)
        {
            return GetDataSourceProcessorResult(dataSourceKey, EngineCommand.Detail, er ?? new EngineRequestWithPaging());
        }

        /// <summary>
        /// Gets the unique values of a field.
        /// </summary>
        /// <param name="dataSourceKey">The data source key specifies the data.</param>
        /// <param name="fieldname">The key of the field which unique values you want to get.</param>
        /// <param name="er">The engine request.</param>
        /// <returns>The unique value list.</returns>
        /// <remarks>Now only the DataEngine data and the in-memory data support this action. The cube data doesn't support it.</remarks>
        [HttpPost]
        [Route("{dataSourceKey}/fields/{fieldname}/uniquevalues")]
        public virtual IActionResult GetUniqueValues(string dataSourceKey, string fieldname, [FromBody]EngineRequest er)
        {
            var request = er ?? new EngineRequest();
            request.FieldName = fieldname;
            return GetDataSourceProcessorResult(dataSourceKey, EngineCommand.UniqueValues, request);
        }
    }
}
