﻿using C1.Web.Api.DataEngine.Models;
using C1.Util.Licensing;
#if !ASPNETCORE && !NETCORE
using IActionResult = System.Web.Http.IHttpActionResult;
using System.Web.Http.Results;
using Controller = System.Web.Http.ApiController;
using System.Text;
#else
#endif

namespace C1.Web.Api.DataEngine
{
    public partial class DataEngineController
    {
        private EngineRequest _getRequestForProcessor(string dataSourceKey, EngineCommand cmd, EngineRequest request = null)
        {
            LicenseHelper.NeedRenderEvalInfo<LicenseDetector>();
            var engineRequest = GetRequest(request, cmd);
            engineRequest.DataSourceKey = dataSourceKey;
            return engineRequest;
        }

        private EngineRequest GetRequest(EngineRequest request, EngineCommand cmd)
        {
            EngineRequest er;
            switch (cmd)
            {
                case EngineCommand.Detail:
                    er = request ?? new DetailRequest();
                    break;
                case EngineCommand.RawData:
                    er = request ?? new EngineRequestWithPaging();
                    break;
                case EngineCommand.Analyses:
                case EngineCommand.Analysis:
                case EngineCommand.Clear:
                case EngineCommand.Fields:
                case EngineCommand.ResultData:
                case EngineCommand.Status:
                case EngineCommand.UniqueValues:
                default:
                    er = request ?? new EngineRequest();
                    break;
            }
            er.Command = cmd;
            return er;
        }

    }
}
