﻿using C1.Web.Api.DataEngine.Data;
using C1.Web.Api.DataEngine.Localization;
using C1.Web.Api.DataEngine.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
#if !ASPNETCORE && !NETCORE
using IActionResult = System.Web.Http.IHttpActionResult;
using Controller = System.Web.Http.ApiController;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace C1.Web.Api.DataEngine
{
    public partial class DataEngineController
    {
        private IActionResult GetDataSourceProcessorResult(string dataSourceKey, EngineCommand cmd, EngineRequest er = null)
        {
            var request = _getRequestForDataEngineProcessor(dataSourceKey, cmd, er);
            return _getResultFromDataEngineProcessor(request);
        }

        private EngineRequest _getRequestForDataEngineProcessor(string dataSourceKey, EngineCommand cmd, EngineRequest request = null)
        {
            return _getRequestForProcessor(dataSourceKey, cmd, request);
        }

        #region Methods
        #region Overrided Methods
        private IActionResult _getResultFromDataEngineProcessor(EngineRequest request)
        {
            if (!FlexPivotEngineProviderManager.Current.Contains(request.DataSourceKey))
            {
                return NotFound();
            }

            switch (request.Command)
            {
                case EngineCommand.Fields:
                    return GetFields(request);
                case EngineCommand.RawData:
                    return GetRawData(request);
                case EngineCommand.Analyses:
                    return Analyze(request);
                case EngineCommand.Detail:
                    return GetDetail(request);
                case EngineCommand.UniqueValues:
                    return GetUniqueValues(request);
            }

            return NotFound();
        }
#endregion Overrided Methods

#region Utilities
        internal static IEnumerable GetPageData(IEnumerable data, int skip, int top)
        {
            if (data == null)
            {
                return null;
            }

            if (skip < 0)
            {
                skip = 0;
            }

            var pageData = data.OfType<object>().Skip(skip);
            if (top >= 0)
            {
                pageData = pageData.Take(top);
            }

            return pageData;
        }
#endregion Utilities

#region Actions
#region Fields
        private IActionResult GetFields(EngineRequest request)
        {
            var value = FlexPivotEngineProviderManager.Current.Get(request.DataSourceKey).Fields;
            return Ok(value);
        }
#endregion Fields

#region RawData
        private IActionResult GetRawData(EngineRequest request)
        {
            var requestWithPaging = request as EngineRequestWithPaging;
            var rawData = FlexPivotEngineProviderManager.Current.Get(requestWithPaging.DataSourceKey).RawData;
            var value = new RawData
            {
                Value = GetPageData(rawData.Value, requestWithPaging.Skip, requestWithPaging.Top),
                TotalCount = rawData.TotalCount
            };
            return Ok(value);
        }
#endregion RawData

#region Analyze
        private IActionResult Analyze(EngineRequest request)
        {
            var analysis = AnalysisCacheManager.Instance.Create(new Analysis(request.DataSourceKey, request.View)).Content;
            return Created(analysis, "Token");
        }
#endregion Analyze

#region Details
        private IActionResult GetDetail(EngineRequest request)
        {
            var detailRequest = request as DetailRequest;
            object[] keys = detailRequest != null ? detailRequest.Keys : null;
            if (request.View == null || keys == null)
            {
                throw new Exception(Resources.ViewNotNull);
            }
            var detailData = FlexPivotEngineProviderManager.Current.Get(detailRequest.DataSourceKey).GetDetail(detailRequest.View, keys);
            var value = new RawData
            {
                Value = GetPageData(detailData.Value, detailRequest.Skip, detailRequest.Top),
                TotalCount = detailData.TotalCount
            };
            return Ok(value);
        }
#endregion Details

#region UniqueValues
        private IActionResult GetUniqueValues(EngineRequest request)
        {
            var value = FlexPivotEngineProviderManager.Current.Get(request.DataSourceKey).GetUniqueValues(request.View, request.FieldName);
            return Ok(value);
        }
#endregion UniqueValues
#endregion Actions
#endregion Methods
    }
}
