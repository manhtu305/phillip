﻿#if !ASPNETCORE && !NETCORE
using System.Web.Http;
using IActionResult = System.Web.Http.IHttpActionResult;
#else
using Microsoft.AspNetCore.Mvc;
#endif
using C1.Web.Api.DataEngine.Models;

namespace C1.Web.Api.DataEngine
{
    public partial class DataEngineController
    {
        /// <summary>
        /// Gets the analysis result data.
        /// </summary>
        /// <param name="dataSourceKey">The data source key specifies the data.</param>
        /// <param name="token">The key specifies the analysis instance.</param>
        /// <returns>The analysis result data.</returns>
        [HttpGet]
        [Route("{dataSourceKey}/analyses/{token}/result")]
        public virtual IActionResult GetAnalysisResult(string dataSourceKey, string token)
        {
            return GetAnalysisProcessorResult(dataSourceKey, token, EngineCommand.ResultData);
        }

        /// <summary>
        /// Gets the analysis status.
        /// </summary>
        /// <param name="dataSourceKey">The data source key specifies the data.</param>
        /// <param name="token">The key specifies the analysis instance.</param>
        /// <returns>The status.</returns>
        [HttpGet]
        [Route("{dataSourceKey}/analyses/{token}/status")]
#if ASPNETCORE || NETCORE
        [ResponseCache(NoStore = true)]
#else
        [CacheControl(NoStore = true, NoCache = true)]
#endif
        public virtual IActionResult GetAnalysisStatus(string dataSourceKey, string token)
        {
            return GetAnalysisProcessorResult(dataSourceKey, token, EngineCommand.Status);
        }

        /// <summary>
        /// Gets the analysis information.
        /// </summary>
        /// <param name="dataSourceKey">The data source key specifies the data.</param>
        /// <param name="token">The key specifies the analysis instance.</param>
        /// <returns>The analysis result information.</returns>
        [HttpGet]
        [Route("{dataSourceKey}/analyses/{token}")]
        public virtual IActionResult GetAnalysis(string dataSourceKey, string token)
        {
            return GetAnalysisProcessorResult(dataSourceKey, token, EngineCommand.Analysis);
        }

        /// <summary>
        /// Remove the analysis.
        /// </summary>
        /// <param name="dataSourceKey">The data source key specifies the data.</param>
        /// <param name="token">The key specifies the analysis instance.</param>
        /// <returns>The response for this request.</returns>
        [HttpDelete]
        [Route("{dataSourceKey}/analyses/{token}")]
        public virtual IActionResult RemoveAnalysis(string dataSourceKey, string token)
        {
            return GetAnalysisProcessorResult(dataSourceKey, token, EngineCommand.Clear);
        }
    }
}
