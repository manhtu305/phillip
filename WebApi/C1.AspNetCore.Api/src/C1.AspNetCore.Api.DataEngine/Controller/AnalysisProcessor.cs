﻿using C1.Web.Api.DataEngine.Models;
using C1.Web.Api.DataEngine.Utils;
using System;
using C1.Web.Api.DataEngine.Serialization;
#if !ASPNETCORE && !NETCORE
using IActionResult = System.Web.Http.IHttpActionResult;
using Controller = System.Web.Http.ApiController;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace C1.Web.Api.DataEngine
{
    public partial class DataEngineController
    {
        private IActionResult GetAnalysisProcessorResult(string dataSourceKey, string token, EngineCommand cmd)
        {
            var request = _getRequestForAnalysisProcessor(dataSourceKey, token, cmd);
            return _getResultFromAnalysisProcessor(request);
        }

        private EngineRequest _getRequestForAnalysisProcessor(string dataSourceKey, string token, EngineCommand cmd, EngineRequest request = null)
        {
            var engineRequest = _getRequestForProcessor(dataSourceKey, cmd, request);
            engineRequest.AnalysisToken = token;
            return engineRequest;
        }

        #region Methods
        private IActionResult _getResultFromAnalysisProcessor(EngineRequest request)
        {
            if (!AnalysisCacheManager.Instance.Contains(request.AnalysisToken))
            {
                return NotFound();
            }
            var cache = AnalysisCacheManager.Instance.Get(request.AnalysisToken);
            if(request.DataSourceKey != cache.Content.DataSourceKey)
            {
                return NotFound();
            }
            if (cache != null)
            {
                cache.Content.Refresh();
            }

            switch (request.Command)
            {
                case EngineCommand.Status:
                    return GetStatus(cache);
                case EngineCommand.ResultData:
                    return GetResult(cache);
                case EngineCommand.Analysis:
                    return GetAnalysis(cache);
                case EngineCommand.Clear:
                    return Clear(cache);
            }
            return NotFound();
        }

        #region Actions
        #region Status
        private IActionResult GetStatus(Cache<Analysis> cache)
        {
            return Ok(cache.Content.Status);
        }
        #endregion Status

        #region ResultData
        private IActionResult GetResult(Cache<Analysis> cache)
        {
            cache.Content.TryToGetResult();
            SetJsonSettings(JsonSerializationHelper.DataJsonSettings);
            return Ok(cache.Content.Result);
        }
        #endregion ResultData

        #region Analysis
        private IActionResult GetAnalysis(Cache<Analysis> cache)
        {
            return Ok(cache.Content);
        }
        #endregion Analysis

        #region Clear
        private IActionResult Clear(Cache<Analysis> cache)
        {
            cache.Content.Clear();
            return NoContent();
        }
        #endregion Clear
        #endregion Actions
        #endregion Methods
    }
}
