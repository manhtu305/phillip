﻿using C1.Web.Api.DataEngine.Data;
using System.IO;
using System;
#if ASPNETCORE || NETCORE
using IAppBuilder = Microsoft.AspNetCore.Builder.IApplicationBuilder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
namespace Microsoft.AspNetCore.Builder
#else
namespace Owin
#endif
{
    /// <summary>
    /// Extension methods for <see cref="IAppBuilder"/>.
    /// </summary>
    public static class AppBuilderUseExtensions
    {
        /// <summary>
        /// Use <see cref="FlexPivotEngineProviderManager"/>.
        /// </summary>
        /// <returns>The <see cref="FlexPivotEngineProviderManager"/>.</returns>
        public static FlexPivotEngineProviderManager UseDataEngineProviders(this IAppBuilder app)
        {
            // Sets the default workspace path.
            var manager = FlexPivotEngineProviderManager.Current;
            if (FlexPivotEngineProviderManager.DefaultWorkspacePath == null)
            {
                manager.SetDefaultWorkspacePath(GetDefaultWorkspacePath(app));
            }

            return manager;
        }

        /// <summary>
        /// Sets the max count of the analysis result data.
        /// </summary>
        [Obsolete("This method has been deprecated. Use IAppBuilder.UseDataEngineProviders().SetDataEngineAnalysisResultMaxCount(int maxCount) instead.")]
        public static void SetDataEngineAnalysisResultMaxCount(this IAppBuilder app, int maxCount)
        {
            FlexPivotEngineProviderManager.Current.SetDataEngineAnalysisResultMaxCount(maxCount);
        }

        /// <summary>
        /// Sets the default workspace path for the DataEngine data.
        /// </summary>
        [Obsolete("This method has been deprecated. Use IAppBuilder.UseDataEngineProviders().SetDefaultWorkspacePath(string path) instead.")]
        public static void SetDefaultWorkspacePath(this IAppBuilder app, string path)
        {
            FlexPivotEngineProviderManager.Current.SetDefaultWorkspacePath(path);
        }

        private static string GetDefaultWorkspacePath(IAppBuilder app)
        {
#if ASPNETCORE || NETCORE
            var env = app.ApplicationServices.GetService<IHostingEnvironment>();
            var rootPath = env != null ? env.WebRootPath : string.Empty;
#else
            var rootPath = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
#endif

            return Path.Combine(rootPath, "Data");
        }
    }
}
