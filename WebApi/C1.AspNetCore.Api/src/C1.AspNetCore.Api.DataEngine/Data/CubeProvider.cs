﻿using C1.FlexPivot;
using C1.Web.Api.DataEngine.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace C1.Web.Api.DataEngine.Data
{
    internal class CubeProvider : IFlexPivotEngineProvider
    {
        private readonly string _connectionString;
        private readonly string _cubeName;

        public CubeProvider(string connectionString, string cubeName)
        {
            _connectionString = connectionString;
            _cubeName = cubeName;
        }

        #region IFlexPivotEngineProvider
        public IEnumerable<Field> Fields
        {
            get
            {
                return FlexPivotEngineProviderManager.GetFieldsFromCubeMeta(Task.Run(() => C1FlexPivotEngine.GetMetadataCube(_connectionString, _cubeName, CancellationToken.None)).Result);
            }
        }

        public IRawData RawData
        {
            get
            {
                throw Utils.ErrorHelper.GetCommandNotSupportedException(EngineCommand.RawData);
            }
        }

        public Task<Dictionary<object[], object[]>> GetAggregatedData(Dictionary<string, object> view, CancellationTokenSource cancelSource, Action<int> progress)
        {
            return Task.Run(() => C1FlexPivotEngine.ExecCube(_connectionString, _cubeName, view, cancelSource.Token, new ProgressDelegate(progress)));
        }

        public IRawData GetDetail(Dictionary<string, object> view, object[] key)
        {
            throw Utils.ErrorHelper.GetCommandNotSupportedException(EngineCommand.Detail);
        }

        public IEnumerable GetUniqueValues(Dictionary<string, object> view, string fieldName)
        {
            throw Utils.ErrorHelper.GetCommandNotSupportedException(EngineCommand.UniqueValues);
        }
        #endregion IAggregatedDataProvider
    }
}
