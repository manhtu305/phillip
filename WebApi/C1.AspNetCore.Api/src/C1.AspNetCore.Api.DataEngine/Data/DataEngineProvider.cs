﻿using C1.FlexPivot;
using C1.Web.Api.DataEngine.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Threading;
using System.Threading.Tasks;

namespace C1.Web.Api.DataEngine.Data
{
    internal class DataEngineProvider : IFlexPivotEngineProvider
    {
        private readonly string _worksapce;
        private readonly string _tableName;

        public DataEngineProvider(string wrokspace, string tableName)
        {
            _worksapce = wrokspace;
            _tableName = tableName;
        }

        #region IFlexPivotEngineProvider
        public IEnumerable<Field> Fields
        {
            get
            {
                return FlexPivotEngineProviderManager.GetFieldsFromRDMeta(Task.Run(() => C1FlexPivotEngine.GetMetadata(_worksapce, _tableName, CancellationToken.None)).Result);
            }
        }

        public IRawData RawData
        {
            get
            {
                var rawData = Task.Run(() => C1FlexPivotEngine.GetRawData(_worksapce, _tableName, CancellationToken.None)).Result;
                var totalCount = rawData.Count;
                return new RawData
                {
                    Value = GetFormattedRawData(rawData),
                    TotalCount = totalCount
                };
            }
        }

        public Task<Dictionary<object[], object[]>> GetAggregatedData(Dictionary<string, object> view, CancellationTokenSource cancelSource, Action<int> progress)
        {
            return Task.Run(() => C1FlexPivotEngine.Exec(_worksapce, _tableName, view, cancelSource.Token, new ProgressDelegate(progress)));
        }

        public IRawData GetDetail(Dictionary<string, object> view, object[] key)
        {
            var detailData = Task.Run(() => C1FlexPivotEngine.GetDetails(_worksapce, _tableName, view, key, CancellationToken.None)).Result;
            var totalCount = detailData.Count;
            return new RawData
            {
                Value = GetFormattedRawData(detailData),
                TotalCount = totalCount
            };
        }

        public IEnumerable GetUniqueValues(Dictionary<string, object> view, string fieldName)
        {
            return Task.Run(() => C1FlexPivotEngine.GetUniqueValues(_worksapce, _tableName, view, fieldName, CancellationToken.None)).Result;
        }

        private static List<Dictionary<string, object>> GetFormattedRawData(IList rawData)
        {
            var resultData = new List<Dictionary<string, object>>();
            foreach (var rdi in rawData)
            {
                var item = ConvertIDataRecord(rdi);
                if (item != null)
                {
                    resultData.Add(item);
                }
            }
            return resultData;
        }

        private static Dictionary<string, object> ConvertIDataRecord(object row)
        {
            var idr = row as IDataRecord;
            if (idr != null)
            {
                var fieldCount = idr.FieldCount;
                var result = new Dictionary<string, object>();
                for (var i = 0; i < fieldCount; i++)
                {
                    result.Add(idr.GetName(i), idr.GetValue(i));
                }
                return result;
            }
            return null;
        }
        #endregion IFlexPivotEngineProvider
    }
}
