﻿using C1.Web.Api.DataEngine.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace C1.Web.Api.DataEngine.Data
{
    /// <summary>
    /// The flex pivot engine provider interface.
    /// </summary>
    public interface IFlexPivotEngineProvider
    {
        /// <summary>
        /// Gets the field collection defined in the data source.
        /// </summary>
        IEnumerable<Field> Fields { get; }

        /// <summary>
        /// Get the entire raw source data.
        /// </summary>
        IRawData RawData { get; }

        /// <summary>
        /// Gets the unqiue values of the specified field.
        /// </summary>
        /// <param name="view">View definition used for performing aggregation.</param>
        /// <param name="fieldName">The name of the field to provide unique values for.</param>
        /// <returns>Collection of objects with properties Name (string) and Type (System.Type)</returns>
        /// <remarks>
        /// The user should be aware that getting value list can take considerable time if number of rows in the source data is very large.
        /// </remarks>
        IEnumerable GetUniqueValues(Dictionary<string, object> view, string fieldName);

        /// <summary>
        /// Gets the source data rows used in obtaining a given aggregated value.
        /// </summary>
        /// <param name="view">View definition used for performing aggregation.</param>
        /// <param name="key">Key values specifying the aggregated value (subtotal)./></param>
        /// <returns>List of source data objects that contributed to the given aggregation value.</returns>
        /// <remarks>
        /// It is recommended to use paging providing source data to the clients, because a DataEngine table can have a very large number of rows and aggregating a subtotal can involve a considerable part of all rows.
        /// </remarks>
        IRawData GetDetail(Dictionary<string, object> view, object[] key);

        /// <summary>
        /// Gets the aggregated results according to the given view in an asynchronous execution
        /// </summary>
        /// <param name="view">View definition used for performing aggregation.</param>
        /// <param name="cancelSource">The cancellation token source.</param>
        /// <param name="progress">A callback function to report analysis progress.</param>
        /// <returns>View execution (aggregation) result</returns>
        Task<Dictionary<object[], object[]>> GetAggregatedData(Dictionary<string, object> view, CancellationTokenSource cancelSource, Action<int> progress);
    }
}
