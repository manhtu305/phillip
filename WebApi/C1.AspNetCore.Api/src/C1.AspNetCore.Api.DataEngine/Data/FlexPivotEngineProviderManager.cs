﻿using C1.DataEngine;
using C1.Web.Api.Configuration;
using C1.Web.Api.DataEngine.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
#if ASPNETCORE || NETCORE
using IAppBuilder = Microsoft.AspNetCore.Builder.IApplicationBuilder;
#else
using Owin;
#endif

namespace C1.Web.Api.DataEngine.Data
{
    /// <summary>
    /// The aggregated data provider manager.
    /// </summary>
    public sealed class FlexPivotEngineProviderManager : Manager<IFlexPivotEngineProvider>
    {
        #region Fields
        private static FlexPivotEngineProviderManager _current = new FlexPivotEngineProviderManager();
        internal static int AnalysisResultMaxCount = -1;
        internal static string DefaultWorkspacePath = null;
        #endregion Fields

        #region Properties
        internal static FlexPivotEngineProviderManager Current
        {
            get
            {
                return _current;
            }
        }
        #endregion Properties

        #region Ctors
        private FlexPivotEngineProviderManager()
        {
        }
        #endregion Ctors

#region Methods

#if !NETCORE
        /// <summary>
        /// Adds a cube data.
        /// </summary>
        /// <param name="name">The unique value used to stand for the data.</param>
        /// <param name="connectionString">The connection string for the cube data.</param>
        /// <param name="cubeName">Specify the cube name in the cube data.</param>
        /// <returns>The flex pivot engine provider manager.</returns>
        public FlexPivotEngineProviderManager AddCube(string name, string connectionString, string cubeName)
        {
            Items.Add(name, new CubeProvider(connectionString, cubeName));
            return this;
        }
#endif
        /// <summary>
        /// Adds a DataEngine data which data files already exist.
        /// </summary>
        /// <param name="name">The unique value used to stand for the data source.</param>
        /// <param name="workspace">
        /// Path in the server's file system where DataEngine data files locate.
        /// </param>
        /// <param name="tableName">
        /// The name of the DataEngine table.
        /// </param>
        /// <returns>The flex pivot engine provider manager.</returns>
        public FlexPivotEngineProviderManager AddDataEngine(string name, string workspace,
            string tableName)
        {
            var tName = GetTableName(name, tableName);
            var wsPath = GetWorkspacePath(workspace);
            Items.Add(name, new DataEngineProvider(wsPath, tName));
            return this;
        }

        /// <summary>
        /// Adds a DataEngine data from a database.
        /// </summary>
        /// <param name="name">The unique value used to stand for the data source.</param>
        /// <param name="command">ADO.NET command object for retrieving data from the database. The connection in the command should be open.</param>
        /// <param name="connection">An open ADO.NET connection object for connecting to the database.</param>
        /// <param name="workspace">
        /// Path in the server's file system where DataEngine data is saved in files.
        /// If it is set to null or not set, the default workspace is used.
        /// The default workspace path is "application base path + /Data".
        /// You can change it via the <see cref="IAppBuilder"/>.SetDefaultWorkspacePath(string path) method.
        /// </param>
        /// <param name="tableName">
        /// The name of the DataEngine table.
        /// If it is set to null or not set, the value of the name parameter is used.
        /// </param>
        /// <returns>The flex pivot engine provider manager.</returns>
        public FlexPivotEngineProviderManager AddDataEngine(string name, DbCommand command,
            DbConnection connection = null, string workspace = null, string tableName = null)
        {
            var tName = GetTableName(name, tableName);
            var wsPath = GetWorkspacePath(workspace);

            using (var w = new Workspace())
            {
                w.Init(wsPath);
                DbConnector connector = new DbConnector(w, connection);
                connector.Command = command;
                connector.GetData(tName);
                w.Save();
            }
            return AddDataEngine(name, wsPath, tName);
        }

        private string GetTableName(string key, string tableName = null)
        {
            return tableName ?? key;
        }

        internal string GetWorkspacePath(string workspace = null)
        {
            return workspace ?? DefaultWorkspacePath;
        }

        /// <summary>
        /// Adds a DataEngine data from an arbitrary IEnumerable.
        /// </summary>
        /// <param name="name">The unique value used to stand for the data source.</param>
        /// <param name="dataGetter">A function to get an arbitrary IEnumerable.</param>
        /// <param name="workspace">
        /// Path in the server's file system where DataEngine data is saved in files.
        /// If it is set to null or not set, the default workspace is used.
        /// The default workspace path is "application base path + /Data".
        /// You can change it via the <see cref="IAppBuilder"/>.SetDefaultWorkspacePath(string path) method.
        /// </param>
        /// <param name="tableName">
        /// The name of the DataEngine table.
        /// If it is set to null or not set, the value of the name parameter is used.
        /// </param>
        /// <returns>The flex pivot engine provider manager.</returns>
        public FlexPivotEngineProviderManager AddDataEngine<T>(string name, Func<IEnumerable<T>> dataGetter,
            string workspace = null, string tableName = null)
        {
            var tName = GetTableName(name, tableName);
            var wsPath = GetWorkspacePath(workspace);

            using (var w = new Workspace())
            {
                w.Init(wsPath);
                var connector = new ObjectConnector<T>(w, dataGetter());
                connector.GetData(tName);
                w.Save();
            }
            return AddDataEngine(name, wsPath, tName);
        }

        /// <summary>
        /// Adds a DataSource data from an arbitrary IEnumerable.
        /// </summary>
        /// <param name="name">The unique value used to stand for the data.</param>
        /// <param name="dataSourceGetter">A function used to return the arbitrary IEnumerable.</param>
        /// <returns>The flex pivot engine provider manager.</returns>
        public FlexPivotEngineProviderManager AddDataSource(string name, Func<IEnumerable> dataSourceGetter)
        {
            Items.Add(name, new DataSourceProvider(dataSourceGetter));
            return this;
        }

        /// <summary>
        /// Adds a DataSource data from an arbitrary IEnumerable.
        /// </summary>
        /// <param name="name">The unique value used to stands for the data.</param>
        /// <param name="dataSource">The arbitrary IEnumerable.</param>
        /// <returns>The flex pivot engine provider manager.</returns>
        public FlexPivotEngineProviderManager AddDataSource(string name, IEnumerable dataSource)
        {
            return AddDataSource(name, () => dataSource);
        }

        /// <summary>
        /// Gets the fields from the meta of the DataEngine data and the DataSource data.
        /// </summary>
        /// <param name="metaData">The meta data.</param>
        /// <returns>A field collection.</returns>
        internal static IEnumerable<Field> GetFieldsFromRDMeta(Dictionary<string, object> metaData)
        {
            return (metaData["fields"] as IList).OfType<object>().Select(oField => Field.ConvertFromRDField(oField)).ToList();
        }
#if !NETCORE
        /// <summary>
        /// Gets the cube fields from the meta of the cube data.
        /// </summary>
        /// <param name="metaData">The meta data.</param>
        /// <returns>A field collection.</returns>
        internal static IEnumerable<Field> GetFieldsFromCubeMeta(Dictionary<string, object> metaData)
        {
            var fields = new List<Field>();
            var allFields = metaData["fields"] as IList;
            foreach (var mField in allFields)
            {
                fields.Add(Field.ConvertFromCubeField(mField));
            }
            return fields;
        }
#endif
        /// <summary>
        /// Disposes the specified workspace and cancel all related tasks.
        /// </summary>
        /// <param name="path">The path of the workspace to be disposed.</param>
        /// <remarks>
        /// It only works when analyzing a DataEngine data.
        /// If the path is not set, the default workspace will be disposed.
        /// Please be careful and ensure you DO want to dispose the workspace when you will call this method.
        /// It will cancel all running tasks (if any exists) with the specified workspace and remove locks from files.
        /// So after calling this method, you will be able to delete them.
        /// </remarks>
        public void DisposeWorkspace(string path = null)
        {
            var wsPath = Current.GetWorkspacePath(path);
            FlexPivot.C1FlexPivotEngine.DisposeWorkspace(wsPath);
        }

        /// <summary>
        /// Sets the max count of the analysis result data.
        /// </summary>
        /// <param name="maxCount">The max count.</param>
        /// <remarks>
        /// When the count of the analysis result data exceeds maxCount, an exception would be thrown.
        /// When maxCount is not set, the count is not limited.
        /// Normally an integer which is not lower than zero can be set to maxCount.
        /// If a negative integer is set to maxCount, the count is not limited.
        /// </remarks>
        public void SetDataEngineAnalysisResultMaxCount(int maxCount)
        {
            if (maxCount < 0)
            {
                maxCount = -1;
            }
            AnalysisResultMaxCount = maxCount;
        }

        /// <summary>
        /// Sets the default workspace path for the DataEngine data.
        /// </summary>
        /// <param name="path">The path of the default workspace.</param>
        public void SetDefaultWorkspacePath(string path)
        {
            DefaultWorkspacePath = path;
        }
#endregion Methods
    }
}
