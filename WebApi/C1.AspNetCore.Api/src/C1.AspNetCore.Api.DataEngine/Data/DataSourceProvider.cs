﻿using C1.FlexPivot;
using C1.Web.Api.DataEngine.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using System.Linq;

namespace C1.Web.Api.DataEngine.Data
{
    internal class DataSourceProvider : IFlexPivotEngineProvider
    {
        private readonly Func<IEnumerable> _dataSourceReader;

        public DataSourceProvider(Func<IEnumerable> dataSourceReader)
        {
            _dataSourceReader = dataSourceReader;
        }

        private IEnumerable DataSource
        {
            get { return _dataSourceReader != null ? _dataSourceReader() : null; }
        }

        #region IFlexPivotEngineProvider
        public IEnumerable<Field> Fields
        {
            get
            {
                return FlexPivotEngineProviderManager.GetFieldsFromRDMeta(Task.Run(() => C1FlexPivotEngine.GetMetadata(DataSource, CancellationToken.None)).Result);
            }
        }

        public IRawData RawData
        {
            get
            {
                return new RawData
                {
                    Value = DataSource,
                    TotalCount = DataSource.Cast<object>().Count()
                };
            }
        }

        public Task<Dictionary<object[], object[]>> GetAggregatedData(Dictionary<string, object> view, CancellationTokenSource cancelSource, Action<int> progress)
        {
            return Task.Run(() => C1FlexPivotEngine.Exec(DataSource, view, cancelSource.Token, new ProgressDelegate(progress)));
        }

        public IRawData GetDetail(Dictionary<string, object> view, object[] key)
        {
            var detailData = Task.Run(() => C1FlexPivotEngine.GetDetails(DataSource, view, key, CancellationToken.None)).Result;
            var totalCount = detailData.Count;
            return new RawData
            {
                Value = GetFormattedRawData(detailData),
                TotalCount = totalCount
            };
        }

        private static IEnumerable<Dictionary<string, object>> GetFormattedRawData(IList rawData)
        {
            DataView dataView = rawData as DataView;
            if(dataView != null)
            {
                var dataColumns = dataView.Table.Columns;
                var columnCount = dataColumns.Count;
                var result = new List<Dictionary<string, object>>();

                foreach(DataRowView drv in dataView)
                {
                    var rowItem = new Dictionary<string, object>();
                    for (var i = 0; i < columnCount; i++)
                    {
                        rowItem.Add(dataColumns[i].ColumnName, drv[i]);
                    }
                    result.Add(rowItem);
                }

                return result;
            }
            return null;
        }

        public IEnumerable GetUniqueValues(Dictionary<string, object> view, string fieldName)
        {
            return Task.Run(() => C1FlexPivotEngine.GetUniqueValues(DataSource, view, fieldName, CancellationToken.None)).Result;
        }
        #endregion IFlexPivotEngineProvider
    }
}
