﻿using C1.Web.Api.DataEngine.Localization;
using C1.Web.Api.DataEngine.Models;
using System;

namespace C1.Web.Api.DataEngine.Utils
{
    internal static class ErrorHelper
    {
        internal static NotSupportedException GetCommandNotSupportedException(EngineCommand cmd)
        {
            return new NotSupportedException(string.Format(Resources.CommandNotSupported, cmd.ToString()));
        }

        internal static Exception GetInnerException(Exception e)
        {
            while(e != null && e.InnerException != null)
            {
                e = e.InnerException;
            }

            return e;
        }
    }
}
