﻿using System.Reflection;

namespace C1.Web.Api.DataEngine.Utils
{
    internal static class ReflectionHelper
    {
        public static PropertyInfo GetPropertyInfo(string proName, object parentValue)
        {
            if(parentValue != null)
            {
                return parentValue.GetType().GetProperty(proName);
            }
            return null;
        }

    }
}
