﻿using C1.Web.Api.DataEngine.Models;
using System;
using System.Collections;
#if ASPNETCORE
using System.Reflection;
#endif

namespace C1.Web.Api.DataEngine.Utils
{
    internal static class TypeEx
    {
        internal static void ValidNullParameter(object para, string paraName, string msg = null)
        {
            if (para == null)
            {
                throw new ArgumentNullException(paraName, msg);
            }
        }

        public static DataType ConvertToDataType(this Type type)
        {
            var tempType = GetUnderlyingType(type);
            if (tempType == typeof(bool))
            {
                return DataType.Boolean;
            }
            else if (tempType == typeof(DateTime) || tempType == typeof(DateTimeOffset))
            {
                return DataType.Date;
            }
            else if (tempType == typeof(string)
                || tempType == typeof(Guid)
                || tempType == typeof(char))
            {
                return DataType.String;
            }
            else if (typeof(IEnumerable).IsAssignableFrom(tempType))
            {
                return DataType.Array;
            }
            else if (IsNumeric(tempType))
            {
                return DataType.Number;
            }
            return DataType.Object;
        }

        public static bool IsNumeric(Type type)
        {
            // handle regular types
            switch (Type.GetTypeCode(type))
            {
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                    return true;
            }

            // done
            return false;
        }

        private static Type GetUnderlyingType(Type type)
        {
            var tempType = type;
            if (type.IsNullableType())
            {
                tempType = Nullable.GetUnderlyingType(type);
            }
            return tempType;
        }

        public static bool IsPrimitive(this Type type)
        {
#if ASPNETCORE
            return type.GetTypeInfo().IsPrimitive;
#else
            return type.IsPrimitive;
#endif
        }

        public static bool IsValueType(this Type type)
        {
#if ASPNETCORE
            return type.GetTypeInfo().IsValueType;
#else
            return type.IsValueType;
#endif
        }

        public static bool IsGenericType(this Type type)
        {
#if ASPNETCORE
            return type.GetTypeInfo().IsGenericType;
#else
            return type.IsGenericType;
#endif
        }

        public static bool IsNullableType(this Type t)
        {
            ValidNullParameter(t, "t");
            if (t.IsValueType())
            {
                return (t.IsGenericType() && t.GetGenericTypeDefinition() == typeof(Nullable<>));
            }

            return false;
        }
    }
}
