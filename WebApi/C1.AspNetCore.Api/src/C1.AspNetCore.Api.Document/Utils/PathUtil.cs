﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace C1.Web.Api.Document
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal static class PathUtil
    {
        internal const char PATH_SEPARATOR = '/';

        public static void SeparatePathByLast(string value, out string former, out string last)
        {
            value = value ?? string.Empty;
            var lastNodeStart = value.LastIndexOf(PATH_SEPARATOR);
            former = lastNodeStart == -1 ? string.Empty : value.Substring(0, lastNodeStart);
            last = lastNodeStart == -1 ? value : value.Substring(lastNodeStart + 1);
        }

        public static void SeparatePathByFirst(string value, out string former, out string last)
        {
            value = value ?? string.Empty;
            var nodeStart = value.IndexOf(PATH_SEPARATOR);
            former = nodeStart == -1 ? value : value.Substring(0, nodeStart);
            last = (nodeStart == -1 || nodeStart == value.Length - 1) ? string.Empty : value.Substring(nodeStart + 1);
        }

        public static string Combine(string path1, string path2)
        {
            if (string.IsNullOrEmpty(path1)) return path2;
            if (string.IsNullOrEmpty(path2)) return path1;

            return path1.TrimEnd(PATH_SEPARATOR) + PATH_SEPARATOR + path2.TrimStart(PATH_SEPARATOR);
        }
    }
}
