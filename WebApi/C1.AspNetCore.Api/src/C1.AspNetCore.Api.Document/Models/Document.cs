﻿using C1.Util.Licensing;
using C1.Web.Api.Document.Localization;
#if !NETCORE
using C1.C1Zip;
using C1.Win.C1Document;
using C1.Win.C1Document.Export;
#endif
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace C1.Web.Api.Document.Models
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal abstract class Document : IDisposable
    {
        private const string CustomActionPrefix = "CA:";

        // The units of measurement used to define coordinates and sizes of following objects:
        // 1) The BookmarkPosition.PageBounds property which will be used by the result of:
        // 1.1) GetBookmark() method.
        // 1.2) ExecuteCustomAction() method.
        // 1.3) GetOutlines() method.
        // 2) The SearchResult.BoundsList property which will be used by the result of Search() method.
        private const UnitTypeEnum MeasurementUnits = UnitTypeEnum.Twip;

        private C1DocumentSource _documentSource;
        IAsyncOperationWithProgress<C1BookmarkPosition, double> _customActionProcess;
        private List<string> _errorList;
        private PageSettings _pageSettings;
        private PageSettings _originalPageSettings;
        private string _exportTempFolder;
        private readonly bool _needRenderEvalInfo;

        protected Document(string path)
        {
            _needRenderEvalInfo = LicenseHelper.NeedRenderEvalInfo(LicenseDetectorType);
            Path = path;
        }

        internal event EventHandler<EventArgs> DocumentUpdating;

        protected void OnDocumentUpdating()
        {
            if (DocumentUpdating != null)
                DocumentUpdating.Invoke(this, EventArgs.Empty);
        }

#region properties
        internal virtual Type LicenseDetectorType
        {
            get
            {
                return typeof(LicenseDetector);
            }
        }

        public bool NeedRenderEvalInfo { get { return _needRenderEvalInfo; } }
        public virtual string Name { get { return DocumentSource.DocumentName; } }
        public string Path { get; set; }
        public DateTime LoadedDateTime { get; private set; }
        public virtual int PageCount { get { return DocumentSource.PageCount; } }
        public double Progress { get; protected set; }
        public string Status { get; protected set; }
        public virtual bool HasOutlines { get { return DocumentSource.Document != null && DocumentSource.Document.Outlines != null && DocumentSource.Document.Outlines.Count > 0; } }

        public List<string> ErrorList
        {
            get { return _errorList ?? (_errorList = new List<string>()); }
            set { _errorList = value; }
        }

        public virtual string DocumentId { get { return Path; } }
        public virtual C1DocumentSource DocumentSource
        {
            get { return _documentSource; }
            protected set
            {
                if (_documentSource != null)
                {
                    _documentSource.ExecuteActionCompleted -= OnExecuteCustomActionCompleted;
                }

                _documentSource = value;

                if (_documentSource != null)
                {
                    _documentSource.ExecuteActionCompleted += OnExecuteCustomActionCompleted;
                }
            }
        }
        public virtual PageSettings PageSettings
        {
            get { return _pageSettings; }
        }

        internal void InitPageSettings(PageSettings settings)
        {
            if (settings != null) UpdatePageSettings(settings);
            _originalPageSettings = ToPageSettings(DocumentSource.PageSettings, Paginated);
        }

        internal virtual void SetPageSettings(PageSettings value)
        {
            var realValue = _originalPageSettings.Clone();
            CopyPageSettings(realValue, value);
            UpdatePageSettings(realValue);
        }

        internal virtual void UpdatePageSettings(PageSettings value)
        {
            // set layout mode
            if (value.Paginated.HasValue)
            {
                Paginated = value.Paginated.Value;
            }

            var winPageSettings = DocumentSource.PageSettings.Clone();
            ApplyToWinPageSettings(value, winPageSettings);
            if (!DocumentSource.PageSettings.IsLayoutEqual(winPageSettings))
            {
                DocumentSource.PageSettings = winPageSettings;
                OnDocumentUpdating();
            }

            _pageSettings = ToPageSettings(winPageSettings, Paginated);
        }

        internal virtual bool Paginated
        {
            get { return DocumentSource.Paginated; }
            set
            {
                if (DocumentSource.Paginated == value) return;

                if (IsPaginatedSupported(value))
                {
                    DocumentSource.Paginated = value;
                }
                else
                {
                    ErrorList.Add(value ? Resources.PaginatedNotSupported : Resources.NonPaginatedNotSupported);
                }
            }
        }

        private bool IsPaginatedSupported(bool paginated)
        {
            return paginated ? GetFeatures().Paginated : GetFeatures().NonPaginated;
        }

        internal string ExportTempFolder
        {
            get
            {
                if (string.IsNullOrEmpty(_exportTempFolder))
                    _exportTempFolder = System.IO.Path.Combine(System.IO.Path.GetTempPath(), Guid.NewGuid().ToString());

                return _exportTempFolder;
            }
        }
#endregion

        internal virtual Stream GetExportedStream(ExportFilter exportFilter, ExportFilterOptions exportFilterOptions)
        {
            // on SingleFile = false, html filter exports the images into images folder.
            var htmlFilter = exportFilter as HtmlFilterBase;
            if (htmlFilter != null && !htmlFilter.SingleFile)
            {
                var zipFile = ZipFolder(htmlFilter.FileName);
                return File.OpenRead(zipFile);
            }

            var files = new FileInfo(exportFilter.FileName).Directory.GetFiles();
            if (files.Length > 0)
            {
                return File.OpenRead(files[0].FullName);
            }

            throw new FileNotFoundException(Resources.ExportedFileNotFound, exportFilter.FileName);
        }

        protected string ZipFolder(string filePath)
        {
            var tempFolder = System.IO.Path.Combine(ExportTempFolder, Guid.NewGuid().ToString());
            Directory.CreateDirectory(tempFolder);
            var dir = System.IO.Path.GetDirectoryName(filePath);
            var zipFileName = System.IO.Path.GetFileNameWithoutExtension(filePath) + ".zip";
            var zipFilePath = System.IO.Path.Combine(tempFolder, zipFileName);

            // TODO: need use another zip lib
#if !NETCORE
            var zip = new C1ZipFile(zipFilePath);
            zip.Entries.AddFolder(dir);
            zip.Close();
#endif
            return zipFilePath;
        }
#region method
        protected abstract void InternalLoad();
        internal virtual void InternalExport(ExportFilter exportFilter)
        {
            DocumentSource.Export(exportFilter);
        }

        internal virtual void Load()
        {
            InternalLoad();

            _pageSettings = ToPageSettings(DocumentSource.PageSettings, Paginated);
            Status = ExecutingStatus.Loaded;
            LoadedDateTime = DateTime.Now;
        }

        internal virtual Stream Export(ExportFilter exportFilter, ExportFilterOptions exportFilterOptions)
        {
            lock (DocumentSource)
            {
                InternalExport(exportFilter);
            }

            return GetExportedStream(exportFilter, exportFilterOptions);
        }

        internal virtual void Stop()
        {
            if (_customActionProcess != null)
            {
                _customActionProcess.Cancel();
            }

            Status = ExecutingStatus.Stopped;
        }

        internal virtual IEnumerable<ExportDescription> GetSupportedFormats()
        {
            return DocumentSource.SupportedExportProviders.Select(p => p.ToExportDescriptor(this));
        }

        internal virtual ExportDescription GetSupportedFormatByName(string name)
        {
            var format = DocumentSource.SupportedExportProviders.FirstOrDefault(p => string.Equals(p.DefaultExtension, name, StringComparison.OrdinalIgnoreCase));
            return format == null ? null : format.ToExportDescriptor(this);
        }

        internal virtual ExportFilterOptions GetExportFilterOptions(IDictionary<string, string> options)
        {
            string format;
            if (options != null && options.TryGetValue("format", out format))
            {
                switch (format.ToLower())
                {
                    case "pdf":
                        return new PdfExportFilterOptions(options);
                    case "html":
                        return new HtmlExportFilterOptions(options);
                    case "rtf":
                        return new RtfExportFilterOptions(options);
                    case "docx":
                        return new DocxExportFilterOptions(options);
                    case "xls":
                        return new XlsExportFilterOptions(options);
                    case "xlsx":
                        return new XlsxExportFilterOptions(options);
                    case "tiff":
                        return new TiffExportFilterOptions(options);
                    case "bmp":
                        return new BmpExportFilterOptions(options);
                    case "png":
                        return new PngExportFilterOptions(options);
                    case "jpg":
                        return new JpegExportFilterOptions(options);
                    case "gif":
                        return new GifExportFilterOptions(options);
                    case "zip":
                        return new MetaExportFilterOptions(options);
                    default:
                        throw new NotAcceptableException();
                }
            }

            return new HtmlExportFilterOptions(options);
        }

        internal abstract IDocumentFeatures GetFeatures();

        private Rect[] GetBounds(Rect start, Rect end, Rect pageBound)
        {
            var rects = new List<Rect>();
            var lineHeight = end.Y - start.Y;

            if (lineHeight < start.Height)
            {
                double minX = Math.Min(start.X, end.X);
                double minY = Math.Min(start.Y, end.Y);
                double maxRight = Math.Max(start.X + start.Width, end.X + end.Width);
                double maxBottom = Math.Max(start.Y + start.Height, end.Y + end.Height);
                rects.Add(new Rect(minX, minY, maxRight - minX, maxBottom - minY));
            }
            else if (lineHeight < start.Height * 2)
            {
                rects.Add(new Rect(start.X, start.Y, pageBound.X + pageBound.Width - start.X, start.Height));
                rects.Add(new Rect(pageBound.X, end.Y, end.X + end.Width - pageBound.X, end.Height));
            }
            else
            {
                rects.Add(new Rect(start.X, start.Y, pageBound.X + pageBound.Width - start.X, start.Height));
                rects.Add(new Rect(pageBound.X, start.Y + start.Height, pageBound.Width, end.Y - start.Y - start.Height));
                rects.Add(new Rect(pageBound.X, end.Y, end.X + end.Width - pageBound.X, end.Height));
            }

            return rects.ToArray();
        }

        private SearchResult GetSearchResult(C1FoundPosition foundPosition)
        {
            if (foundPosition == null)
            {
                return null;
            }

            var startPos = foundPosition.GetStart();
            var endPos = foundPosition.GetEnd();

            if (startPos == null || endPos == null)
            {
                return null;
            }

            var startBound = startPos.PageCoords.GetBounds();
            var endBound = endPos.PageCoords.GetBounds();
            var fragment = startPos.RenderFragment;

            var bounds = fragment.Bounds;
            var pageBound = fragment.GetPageBounds();
            var contentBounds = fragment.GetContentBounds();

            var boundsList = GetBounds(
                    new Rect(startBound.X, startBound.Y, startBound.Width, startBound.Height),
                    new Rect(endBound.X, endBound.Y, endBound.Width, endBound.Height),
                    new Rect(pageBound.X + contentBounds.X - bounds.X, pageBound.Y + contentBounds.Y - bounds.Y, contentBounds.Width, contentBounds.Height));
            if (DocumentSource.Document.MeasurementUnits != MeasurementUnits)
            {
                boundsList = boundsList.Select(b => ConvertUnit(b, DocumentSource.Document.MeasurementUnits, MeasurementUnits)).ToArray();
            }

            return new SearchResult()
            {
                NearText = foundPosition.NearText,
                PositionInNearText = foundPosition.PositionInNearText,
                BoundsList = boundsList,
                PageIndex = foundPosition.GetPage().PageIndex
            };
        }

        private Rect ConvertUnit(Rect rect, UnitTypeEnum fromUnitType, UnitTypeEnum toUnitType)
        {
            return new Rect(Unit.Convert(rect.X, fromUnitType, toUnitType),
                Unit.Convert(rect.Y, fromUnitType, toUnitType),
                Unit.Convert(rect.Width, fromUnitType, toUnitType),
                Unit.Convert(rect.Height, fromUnitType, toUnitType));
        }

        internal virtual List<SearchResult> Search(int startPageIndex, SearchScope scope, SearchOptions searchOptions)
        {
            if ((Status == ExecutingStatus.Loaded
                || Status == ExecutingStatus.Completed
                || Status == ExecutingStatus.Stopped)
                && !string.IsNullOrEmpty(searchOptions.Text))
            {
                if (startPageIndex < 0 || startPageIndex >= PageCount)
                {
                    return null;
                }

                var results = new List<SearchResult>();
                var textSearchManager = new C1TextSearchManager(DocumentSource);
                C1FoundPosition firstPosition = textSearchManager.FindStart(startPageIndex, GetWinSearchScope(scope), new C1FindTextParams(searchOptions.Text, searchOptions.WholeWord, searchOptions.MatchCase));
                C1FoundPosition foundPosition = firstPosition;

                if (scope == SearchScope.FirstOccurrence)
                {
                    if(foundPosition != null)
                    {
                        results.Add(GetSearchResult(foundPosition));
                    }
                }
                else
                { 
                    while (foundPosition != null)
                    {
                        if (scope == SearchScope.EndOfDocument && foundPosition.GetPage().PageIndex < startPageIndex)
                        {
                            break;
                        }

                        results.Add(GetSearchResult(foundPosition));
                        foundPosition = textSearchManager.FindNext(foundPosition);
                        if (foundPosition == firstPosition)
                        {
                            break;
                        }
                    }
                }

                return results;
            }

            return null;
        }

        internal virtual IEnumerable<DocumentPosition> GetBookmarks()
        {
            throw new NotImplementedException();
        }

        internal virtual DocumentPosition GetBookmark(string bookmark)
        {
            if (!string.IsNullOrEmpty(bookmark))
            {
                var position = DocumentSource.GetLinkTargetPosition(new C1LinkTarget(null, bookmark));
                return ConvertBookmarkPosition(position);
            }
            return null;
        }

        private DocumentPosition ConvertBookmarkPosition(C1BookmarkPosition position)
        {
            if (position == null) return null;

            var bookmarkPosition = new DocumentPosition();
            bookmarkPosition.PageIndex = position.PageIndex;
            var bounds = new Rect(position.PageBounds.X, position.PageBounds.Y, position.PageBounds.Width, position.PageBounds.Height);
            if(DocumentSource.Document.MeasurementUnits != MeasurementUnits)
            {
                bounds = ConvertUnit(bounds, DocumentSource.Document.MeasurementUnits, MeasurementUnits);
            }
            bookmarkPosition.PageBounds = bounds;
            return bookmarkPosition;
        }

        internal void ExecuteCustomAction(string actionString)
        {
            if (!actionString.StartsWith(CustomActionPrefix, StringComparison.InvariantCultureIgnoreCase))
                actionString = CustomActionPrefix + actionString;

            var linkTarget = C1LinkTargetBase.Parse(actionString);
            if(linkTarget==null || linkTarget.DocumentAction == null)
            {
                throw new NotFoundException();
            }

            ErrorList.Clear();

            var action = linkTarget.DocumentAction;
            OnCustomActionStarting(action);

            lock (DocumentSource)
            {
                _customActionProcess = DocumentSource.ExecuteActionAsyncEx(action);
            }

            if (_customActionProcess.Status == AsyncStatus.Completed)
            {
                Status = ExecutingStatus.Completed;
            }
            else
            {
                _customActionProcess.Completed += _customActionProcess_Completed;
                _customActionProcess.Progress += _customActionProcess_Progress;
            }

            OnDocumentUpdating();
        }

        private void OnExecuteCustomActionCompleted(object sender, ExecuteActionCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                throw e.Error;
            }

            if (!e.Cancelled)
            {
                OnCustomActionCompleted(e.Action, ConvertBookmarkPosition(e.Position));
            }
        }

        protected virtual void OnCustomActionStarting(C1DocumentAction action)
        {
            // set status to Rendering
        }

        protected virtual void OnCustomActionCompleted(C1DocumentAction action, DocumentPosition position)
        {
            // store the position
        }

        private void _customActionProcess_Progress(IAsyncOperationWithProgress<C1BookmarkPosition, double> asyncInfo, double progressInfo)
        {
            Progress = progressInfo;
            if (progressInfo == 1)
            {
                Status = ExecutingStatus.Completed;
            }
        }

        private void _customActionProcess_Completed(IAsyncOperationWithProgress<C1BookmarkPosition, double> asyncInfo, AsyncStatus asyncStatus)
        {
            Status = ExecutingStatus.Completed;
        }

        private OutlineNode GetOutlineNode(C1.Win.C1Document.OutlineNode c1OutlineNode)
        {
            var node = new OutlineNode();
            node.Caption = c1OutlineNode.Caption;
            node.Level = c1OutlineNode.Level;

            var linkTarget = c1OutlineNode.LinkTarget as C1LinkTarget;
            if (linkTarget != null)
            {
                node.Target = linkTarget.Bookmark;
            }

            if (c1OutlineNode.HasChildren)
            {
                node.Children = new List<OutlineNode>();
                c1OutlineNode.Children.ToList().ForEach(c =>
                {
                    node.Children.Add(GetOutlineNode(c));
                });
            }
            return node;
        }
        
        internal virtual List<OutlineNode> GetOutlines()
        {
            if (HasOutlines && Status == ExecutingStatus.Completed)
            {
                return DocumentSource.Document.Outlines.Select(o => GetOutlineNode(o)).ToList();
            }
            else
            {
                return null;
            }
        }

        public virtual void Dispose()
        {
            Stop();
            if (DocumentSource != null)
            {
                DocumentSource.Dispose();
            }

            try
            {
                if (Directory.Exists(ExportTempFolder))
                {
                    Directory.Delete(ExportTempFolder, true);
                }
            }
            catch { }
        }
#endregion

#region static

        private static void CopyPageSettings(PageSettings value, PageSettings src)
        {
            if (src.Paginated.HasValue) value.Paginated = src.Paginated;
            if (src.PaperSize.HasValue) value.PaperSize = src.PaperSize;
            if (src.Width != null) value.Width = src.Width;
            if (src.Height != null) value.Height = src.Height;
            if (src.LeftMargin != null) value.LeftMargin = src.LeftMargin;
            if (src.TopMargin != null) value.TopMargin = src.TopMargin;
            if (src.RightMargin != null) value.RightMargin = src.RightMargin;
            if (src.BottomMargin != null) value.BottomMargin = src.BottomMargin;
            if (src.Landscape != null) value.Landscape = src.Landscape;
        }

        private static void ApplyToWinPageSettings(PageSettings value, C1PageSettings winPageSettings)
        {
            if (value.PaperSize.HasValue)
            {
                winPageSettings.PaperSize = value.PaperSize.Value;
            }

            var paperSize = winPageSettings.PaperSize;

            if (paperSize == System.Drawing.Printing.PaperKind.Custom)
            {
                if (!string.IsNullOrEmpty(value.Height))
                {
                    winPageSettings.Height = new Unit(value.Height);
                }

                if (!string.IsNullOrEmpty(value.Width))
                {
                    winPageSettings.Width = new Unit(value.Width);
                }
            }

            if (!string.IsNullOrEmpty(value.BottomMargin))
            {
                winPageSettings.BottomMargin = new Unit(value.BottomMargin);
            }

            if (!string.IsNullOrEmpty(value.TopMargin))
            {
                winPageSettings.TopMargin = new Unit(value.TopMargin);
            }

            if (!string.IsNullOrEmpty(value.RightMargin))
            {
                winPageSettings.RightMargin = new Unit(value.RightMargin);
            }

            if (!string.IsNullOrEmpty(value.LeftMargin))
            {
                winPageSettings.LeftMargin = new Unit(value.LeftMargin);
            }

            if (value.Landscape.HasValue)
            {
                // In C1.Win.C1Document.C1PageSettings.Landscape setter,
                // if Landscape changes, it will swap the values between:
                // _width and _height
                // _leftMargin and _topMargin
                // _rightMargin and _bottomMargin
                // So we will swap them before setting Landscape.
                if (value.Landscape.Value != winPageSettings.Landscape)
                {
                    if (paperSize == System.Drawing.Printing.PaperKind.Custom)
                    {
                        var width = winPageSettings.Width;
                        winPageSettings.Width = winPageSettings.Height;
                        winPageSettings.Height = width;
                    }

                    var leftMargin = winPageSettings.LeftMargin;
                    winPageSettings.LeftMargin = winPageSettings.TopMargin;
                    winPageSettings.TopMargin = leftMargin;

                    var rightMargin = winPageSettings.RightMargin;
                    winPageSettings.RightMargin = winPageSettings.BottomMargin;
                    winPageSettings.BottomMargin = rightMargin;
                }

                winPageSettings.Landscape = value.Landscape.Value;
            }
        }

        private static PageSettings ToPageSettings(C1PageSettings value, bool paginated)
        {
            return new PageSettings
            {
                Paginated = paginated,
                Width = value.Width.ToString(),
                Height = value.Height.ToString(),
                BottomMargin = value.BottomMargin.ToString(),
                TopMargin = value.TopMargin.ToString(),
                LeftMargin = value.LeftMargin.ToString(),
                RightMargin = value.RightMargin.ToString(),
                Landscape = value.Landscape,
                PaperSize = value.PaperSize
            };
        }

        private static C1.Win.C1Document.SearchScope GetWinSearchScope(SearchScope scope)
        {
            switch (scope)
            {
                case SearchScope.FirstOccurrence:
                    return Win.C1Document.SearchScope.FirstOccurrence;
                case SearchScope.SinglePage:
                    return Win.C1Document.SearchScope.SinglePage;
                //case SearchScope.WholeDocument:
                //case SearchScope.EndOfDocument:
                default:
                    return Win.C1Document.SearchScope.WholeDocument;
            }
        }
#endregion
    }
}
