﻿using System.Drawing.Printing;

namespace C1.Web.Api.Document.Models
{
    /// <summary>
    /// Defines the document page settings.
    /// </summary>
    public class PageSettings
    {
        /// <summary>
        /// Gets or sets a value indicating whether the content should be represented as set of fixed sized pages.
        /// </summary>
        public bool? Paginated
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the height of the paper.
        /// </summary>
        public string Height
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the width of the paper.
        /// </summary>
        public string Width
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the top margin.
        /// </summary>
        public string TopMargin
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the left margin.
        /// </summary>
        public string LeftMargin
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the right margin.
        /// </summary>
        public string RightMargin
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the bottom margin.
        /// </summary>
        public string BottomMargin
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use landscape orientation.
        /// </summary>
        public bool? Landscape
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the paper kind.
        /// </summary>
        public PaperKind? PaperSize
        {
            get;
            set;
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal PageSettings Clone()
        {
            return new PageSettings
            {
                Paginated = Paginated,
                PaperSize = PaperSize,
                Width = Width,
                Height = Height,
                LeftMargin = LeftMargin,
                TopMargin = TopMargin,
                RightMargin = RightMargin,
                BottomMargin = BottomMargin,
                Landscape = Landscape
            };
        }
    }
}
