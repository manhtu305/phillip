﻿namespace C1.Web.Api.Document.Models
{
    /// <summary>
    /// Represents the result of search.
    /// </summary>
    public class SearchResult
    {
        /// <summary>
        /// Gets the adjacent text of the result.
        /// </summary>
        public string NearText { get; internal set; }
        /// <summary>
        /// Gets the position of the search text in <see cref="NearText"/> of the result.
        /// </summary>
        public int PositionInNearText { get; internal set; }
        /// <summary>
        /// Gets the list of <see cref="Rect"/>. The mearurement units is twip.
        /// </summary>
        public Rect[] BoundsList { get; internal set; }
        /// <summary>
        /// Gets the page index o the result.
        /// </summary>
        public int PageIndex { get; internal set; }
    }
}
