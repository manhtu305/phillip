﻿namespace C1.Web.Api.Document.Models
{
    /// <summary>
    /// Specifies text search scope.
    /// </summary>
    public enum SearchScope
    {
        /// <summary>
        /// Searches the whole document.
        /// </summary>
        WholeDocument,
        /// <summary>
        /// Searches to the end of the document.
        /// </summary>
        EndOfDocument,
        /// <summary>
        /// Searches till the first occurrence is found.
        /// </summary>
        FirstOccurrence,
        /// <summary>
        /// Searches within the specified page only.
        /// </summary>
        SinglePage,
        
    }
}
