﻿namespace C1.Web.Api.Document.Models
{
    /// <summary>
    /// Represents the features supported by the document.
    /// </summary>
    public interface IDocumentFeatures
    {
        /// <summary>
        /// Gets a boolean value indicating whether it supports paginated mode of document rendering.
        /// </summary>
        bool Paginated { get; }
        /// <summary>
        /// Gets a boolean value indicating whether it support non-paginated mode of document rendering.
        /// </summary>
        bool NonPaginated { get; }
        /// <summary>
        /// Gets a boolean value indicating whether it supports text searching in paginated mode.
        /// </summary>
        bool TextSearchInPaginatedMode { get; }
        /// <summary>
        /// Gets a boolean value indicating whether it supports setting page settings.
        /// </summary>
        bool PageSettings { get; }
    }
}
