﻿namespace C1.Web.Api.Document.Models
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal class SearchOptions
    {
        public string Text { get; set; }
        public bool MatchCase { get; set; }
        public bool WholeWord { get; set; }
    }
}
