﻿using System.Collections.Generic;
using C1.Win.C1Document.Export;
using C1.Win.C1Document;
using Newtonsoft.Json;

namespace C1.Web.Api.Document.Models
{
    internal static class ExportDescriptionExtensions
    {
        internal static ExportDescription ToExportDescriptor(this ExportProvider provider, Document document)
        {
            var desc = new ExportDescription();
            desc.Name = provider.FormatName;
            desc.Format = provider.DefaultExtension;

            var nonPaginatedSupported = document.DocumentSource.Features.SupportsNonPaginated;

            switch (desc.Format)
            {
                case "pdf":
                    SetPdfOptionDescriptions(desc.OptionDescriptions, provider); break;
                case "html":
                    SetHtmlOptionDescriptions(desc.OptionDescriptions, provider, nonPaginatedSupported); break;
                case "rtf":
                    SetRtfOptionDescriptions(desc.OptionDescriptions, provider, nonPaginatedSupported); break;
                case "docx":
                    SetDocxOptionDescriptions(desc.OptionDescriptions, provider, nonPaginatedSupported); break;
                case "xls":
                case "xlsx":
                    SetExcelOptionDescriptions(desc.OptionDescriptions, provider); break;
                case "zip":
                    SetMetaOptionDescriptions(desc.OptionDescriptions, provider); break;
                case "tiff":
                    SetTiffOptionDescriptions(desc.OptionDescriptions, provider); break;
                case "bmp":
                case "png":
                case "jpg":
                case "gif":
                    SetImageOptionDescriptions(desc.OptionDescriptions, provider); break;
            }
            return desc;
        }

        private static void SetPdfOptionDescriptions(List<ExportOptionDescription> opts, ExportProvider provider)
        {
            opts.Add(new ExportOptionDescription("pdfACompatible", "bool", true));
            opts.Add(new ExportOptionDescription("embedFonts", "bool", true));
            opts.Add(new ExportOptionDescription("useCompression", "bool", true));
            opts.Add(new ExportOptionDescription("useOutlines", "bool", true));

            //PdfSecurityOptions
            opts.Add(new ExportOptionDescription("allowCopyContent", "bool", true, "documentRestrictions"));
            opts.Add(new ExportOptionDescription("allowEditAnnotations", "bool", true, "documentRestrictions"));
            opts.Add(new ExportOptionDescription("allowEditContent", "bool", true, "documentRestrictions"));
            opts.Add(new ExportOptionDescription("allowPrint", "bool", true, "documentRestrictions"));
            opts.Add(new ExportOptionDescription("ownerPassword", "string", string.Empty, "passwordSecurity"));
            opts.Add(new ExportOptionDescription("userPassword", "string", string.Empty, "passwordSecurity"));
            opts.Add(new ExportOptionDescription("encryptionType", "string", "Standard40", "passwordSecurity", new List<string> { "NotPermit", "Standard40", "Standard128", "Aes128" }));

            opts.AddOutputRangeDescription();
            opts.AddDocumentInfoDescription(provider);
        }

        private static void SetHtmlOptionDescriptions(List<ExportOptionDescription> opts, ExportProvider provider, bool nonPaginatedSupported)
        {
            if (nonPaginatedSupported)
            {
                opts.AddPagedDescription();
            }
            opts.AddOutputRangeDescription();

            opts.Add(new ExportOptionDescription("showNavigator", "bool", false));
            opts.Add(new ExportOptionDescription("navigatorPosition", "string", "Left", "", new[] { "Left", "Right" }));
            opts.Add(new ExportOptionDescription("singleFile", "bool", true));
        }

        private static void SetRtfOptionDescriptions(List<ExportOptionDescription> opts, ExportProvider provider, bool nonPaginatedSupported)
        {
            if (nonPaginatedSupported)
            {
                opts.AddPagedDescription();
            }
            opts.AddOutputRangeDescription();
            opts.Add(new ExportOptionDescription("shapesWord2007Compatible", "bool", true));
        }

        private static void SetDocxOptionDescriptions(List<ExportOptionDescription> opts, ExportProvider provider, bool nonPaginatedSupported)
        {
            if (nonPaginatedSupported)
            {
                opts.AddPagedDescription();
            }
            opts.AddOutputRangeDescription();
            opts.AddDocumentInfoDescription(provider);
        }

        private static void SetExcelOptionDescriptions(List<ExportOptionDescription> opts, ExportProvider provider)
        {
            opts.Add(new ExportOptionDescription("tolerance", "int", 0));
            opts.Add(new ExportOptionDescription("pictureLayer", "bool", false));
            opts.Add(new ExportOptionDescription("sheetName", "string", "Sheet1"));
            opts.AddDocumentInfoDescription(provider);
        }

        private static void SetMetaOptionDescriptions(List<ExportOptionDescription> opts, ExportProvider provider)
        {
            opts.Add(new ExportOptionDescription("metafileType", "string", "EmfPlusDual", "", new List<string> { "EmfOnly", "EmfPlusOnly", "EmfPlusDual" }));
            opts.AddOutputRangeDescription();
        }

        private static void SetTiffOptionDescriptions(List<ExportOptionDescription> opts, ExportProvider provider)
        {
            opts.Add(new ExportOptionDescription("monochrome", "bool", true));
            SetImageOptionDescriptions(opts, provider);
        }

        private static void SetImageOptionDescriptions(List<ExportOptionDescription> opts, ExportProvider provider)
        {
            opts.Add(new ExportOptionDescription("resolution", "float", 300f));
            opts.AddOutputRangeDescription();
        }

        private static void AddPagedDescription(this List<ExportOptionDescription> descriptions)
        {
            descriptions.Add(new ExportOptionDescription("paged", "bool", true));
        }

        private static void AddOutputRangeDescription(this List<ExportOptionDescription> descriptions)
        {
            descriptions.Add(new ExportOptionDescription("outputRange", "string", string.Empty, "outputRange"));
            descriptions.Add(new ExportOptionDescription("outputRangeInverted", "bool", false, "outputRange"));
        }

        private static void AddDocumentInfoDescription(this List<ExportOptionDescription> descriptions, ExportProvider provider)
        {
            if (provider.SupportedDocumentInfoFields == C1.Win.C1Document.DocumentInfoFields.None)
                return;

            var DocumentInfoFields = System.Enum.GetValues(typeof(DocumentInfoFields));
            foreach (DocumentInfoFields field in DocumentInfoFields)
            {
                if (provider.SupportedDocumentInfoFields.HasFlag(field) && field != Win.C1Document.DocumentInfoFields.None)
                {
                    var fieldName = System.Enum.GetName(typeof(DocumentInfoFields), field).ToCamelCase();
                    descriptions.Add(new ExportOptionDescription(fieldName, "string", string.Empty, "documentInfo"));
                }
            }
        }

        private static string ToCamelCase(this string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                return str.Substring(0, 1).ToLower() + str.Substring(1);
            }

            return str;
        }
    }

    /// <summary>
    /// Represents the description of the supported export format.
    /// </summary>
    public class ExportDescription
    {
        private List<ExportOptionDescription> _optionDescriptions;

        /// <summary>
        /// Gets the short description of the export format.
        /// </summary>
        public string Name
        {
            get;
            [SmartAssembly.Attributes.DoNotObfuscate]
            internal set;
        }
        /// <summary>
        /// Gets the default file extension for the export format.
        /// </summary>
        public string Format
        {
            get;
            [SmartAssembly.Attributes.DoNotObfuscate]
            internal set;
        }
        /// <summary>
        /// Gets the list of descriptions of options for the export format.
        /// </summary>
        public List<ExportOptionDescription> OptionDescriptions
        {
            get
            {
                return _optionDescriptions ?? (_optionDescriptions = new List<ExportOptionDescription>());
            }
        }
    }

    /// <summary>
    /// Represents the description of export option.
    /// </summary>
    public class ExportOptionDescription
    {
        /// <summary>
        /// Gets the name of the options.
        /// </summary>
        public string Name { get; private set; }
        /// <summary>
        /// Gets the data type of the options.
        /// </summary>
        public string Type { get; private set; }
        /// <summary>
        /// Gets a boolean value indicating whether the value of the options can be null.
        /// </summary>
        public bool Nullable { get; private set; }
        /// <summary>
        /// Gets the default value of the option.
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Include)]
        public object DefaultValue { get; private set; }
        /// <summary>
        /// Gets the list of strings for supported values of the option.
        /// </summary>
        public IEnumerable<string> AllowedValues { get; private set; }
        /// <summary>
        /// Gets the group which the options belongs to.
        /// </summary>
        public string Group { get; private set; }

        /// <summary>
        /// Initializes a new instance of <see cref="ExportOptionDescription"/> object.
        /// </summary>
        /// <param name="name">The name of the option.</param>
        /// <param name="type">The data type of the option.</param>
        /// <param name="defaultValue">The defualt value of the option.</param>
        /// <param name="group">The group which the option belongs to.</param>
        /// <param name="allowedValues">Thee supported values of the option.</param>
        /// <param name="nullable">Whether the value of the option can be null.</param>
        public ExportOptionDescription(
            string name,
            string type = "string",
            object defaultValue = null,
            string group = "",
            IEnumerable<string> allowedValues = null,
            bool nullable = false)
        {
            Name = name;
            Type = type;
            DefaultValue = defaultValue;
            AllowedValues = allowedValues;
            Nullable = nullable;
            Group = group;
        }
    }
}
