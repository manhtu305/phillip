﻿using SmartAssembly.Attributes;
using System.Collections.Generic;

namespace C1.Web.Api.Document.Models
{
    /// <summary>
    /// Represents the location and size of a rectangle.
    /// </summary>
    public class Rect
    {
        /// <summary>
        /// Initializes a new instance of <see cref="Rect"/> object.
        /// </summary>
        /// <param name="x">The x-coordinate of the upper-left corner of the rect.</param>
        /// <param name="y">The y-coordinate of the upper-left corner of the rect.</param>
        /// <param name="width">The width of the rect.</param>
        /// <param name="height">The height of the rect.</param>
        public Rect(double x, double y, double width, double height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Gets the x-coordinate of the upper-left corner of the rect.
        /// </summary>
        public double X { get; private set; }
        /// <summary>
        /// Gets the y-coordinate of the upper-left corner of the rect.
        /// </summary>
        public double Y { get; private set; }
        /// <summary>
        /// Gets the width of the rect.
        /// </summary>
        public double Width { get; private set; }
        /// <summary>
        /// Gets the height of the rect.
        /// </summary>
        public double Height { get; private set; }
    }

    /// <summary>
    /// Represents the position in the document.
    /// </summary>
    public class DocumentPosition
    {
        /// <summary>
        /// Gets the bound of the position.
        /// </summary>
        /// <remarks>
        /// The measurement units is twip.
        /// </remarks>
        public Rect PageBounds { get; internal set; }
        /// <summary>
        /// Gets the page index of the position.
        /// </summary>
        public int PageIndex { get; internal set; }
    }

    /// <summary>
    /// Represents the node of outline in the outlines tree.
    /// </summary>
    public class OutlineNode
    {
        /// <summary>
        /// Gets the caption of the outline.
        /// </summary>
        public string Caption { get; internal set; }
        /// <summary>
        /// Gets the children list of the outline.
        /// </summary>
        public List<OutlineNode> Children { get; internal set; }
        /// <summary>
        /// Gets the level of the outline.
        /// </summary>
        public int Level { get; internal set; }
        /// <summary>
        /// Gets the bookmark string of the linked target for the outline.
        /// </summary>
        public string Target { get; internal set; }
    }
}
