﻿namespace C1.Web.Api.Document.Models
{
    internal static class ExecutingStatus
    {
        public const string Loaded = "Loaded";
        public const string Completed = "Completed";
        public const string Stopped = "Stopped";
    }
}
