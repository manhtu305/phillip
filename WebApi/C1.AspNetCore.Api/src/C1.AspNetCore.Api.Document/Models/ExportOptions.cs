﻿using C1.C1Pdf;
using C1.Web.Api.Document.Localization;
using C1.Win.C1Document;
using C1.Win.C1Document.Export;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;

namespace C1.Web.Api.Document.Models
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal abstract class ExportFilterOptions
    {
        private delegate bool ParserFunc<T>(string s, out T value);

        internal IDictionary<string, string> Options { get; set; }

        public abstract string Extension
        {
            get;
        }

        public virtual string FilterExtension
        {
            get { return Extension; }
        }

        public ExportFilterOptions(IDictionary<string, string> options)
        {
            Options = options;
        }

        public virtual ExportFilter ToExportFilter(string tempFolder)
        {
            var exportId = Guid.NewGuid().ToString();
            var tempPath = Path.Combine(tempFolder, exportId);
            Directory.CreateDirectory(tempPath);

            var filter = CreateExportFilter();
            filter.ShowOptions = false;
            filter.FileName = Path.Combine(tempPath, "document" + "." + FilterExtension);

            return filter;
        }

        protected abstract ExportFilter CreateExportFilter();

        private T GetValue<T>(string optionName, ParserFunc<T> parser, T defaultValue)
        {
            if (Options.ContainsKey(optionName))
            {
                T val;
                if(parser(Options[optionName], out val))
                {
                    return val;
                }
            }

            return defaultValue;
        }

        private T? GetNullableValue<T>(string optionName, ParserFunc<T> parser, T? defaultValue) where T:struct
        {
            if (Options.ContainsKey(optionName))
            {
                T val;
                if (parser(Options[optionName], out val))
                {
                    return val;
                }
            }

            return defaultValue;
        }

        protected bool GetBooleanValue(string optionName, bool defaultValue)
        {
            return GetValue<bool>(optionName, bool.TryParse, defaultValue);
        }

        protected bool? GetNullableBooleanValue(string optionName, bool? defaultValue)
        {
            return GetNullableValue<bool>(optionName, bool.TryParse, defaultValue);
        }

        protected int GetIntValue(string optionName, int defaultValue)
        {
            return GetValue<int>(optionName, int.TryParse, defaultValue);
        }

        protected int? GetNullableIntValue(string optionName, int? defaultValue)
        {
            return GetNullableValue<int>(optionName, int.TryParse, defaultValue);
        }

        protected float GetFloatValue(string optionName, float defaultValue)
        {
            return GetValue<float>(optionName, float.TryParse, defaultValue);
        }

        protected string GetStringValue(string optionName, string defaultValue)
        {
            if (Options.ContainsKey(optionName))
            {
                return Options[optionName];
            }

            return defaultValue;
        }

        protected DateTime GetDateTimeValue(string optionName, DateTime defaultValue)
        {
            return GetValue<DateTime>(optionName, DateTime.TryParse, defaultValue);
        }

        protected T GetEnumValue<T>(string optionName, T defaultValue) where T : struct
        {
            if (Options.ContainsKey(optionName))
            {
                T val;
                if (Enum.TryParse<T>(Options[optionName], out val))
                {
                    return val;
                }
            }

            return defaultValue;
        }

        protected Unit GetUnit(string optionName, Unit defaultValue)
        {
            return GetValue<Unit>(optionName, Unit.TryParse, defaultValue);
        }

        protected Unit? GetNullableUnit(string optionName, Unit? defaultValue)
        {
            return GetNullableValue<Unit>(optionName, Unit.TryParse, defaultValue);
        }

        protected OutputRange GetOutputRange(OutputRange defaultValue, OutputRange invertedDefaultValue)
        {
            var pages = GetStringValue("outputRange", string.Empty);
            var inverted = GetBooleanValue("outputRangeInverted", false);

            if (string.IsNullOrWhiteSpace(pages)
                // bug 306974, for SSRS report, only PageRange or PageList can be specified for specific format,
                // is this case, client should let user to enter the pages and validate it base on the allowed type.
                // for now, we accept the "PageRange" and "PageList" as "All".
                || string.Equals(pages, "PageRange", StringComparison.OrdinalIgnoreCase)
                || string.Equals(pages, "PageList", StringComparison.OrdinalIgnoreCase))
            {
                return inverted ? invertedDefaultValue : defaultValue;
            }

            //This is only for try, 
            //because public try parse method doesn't support inverted property.
            OutputRange tempRange;
            if (!OutputRange.TryParse(pages, out tempRange))
            {
                throw new StatusCodeException(422, string.Format(Resources.PageRangeSyntaxError, pages));
                
            }

            if(tempRange.Type == OutputRangeType.All)
            {
                return inverted ? OutputRange.All : OutputRange.AllInverted;
            }

            return new OutputRange(pages, inverted);
        }

        protected DocumentInfo GetDocumentInfo()
        {
            var docInfo = new DocumentInfo();
            docInfo.Author = GetStringValue("author", "");
            docInfo.Comment = GetStringValue("comment", "");
            docInfo.Company = GetStringValue("company", "");
            docInfo.CreationTime = GetDateTimeValue("creationTime", DateTime.Now);
            docInfo.Creator = GetStringValue("creator", "");
            docInfo.Keywords = GetStringValue("keywords", "").Split(',');
            docInfo.Manager = GetStringValue("manager", "");
            docInfo.Operator = GetStringValue("operator", "");
            docInfo.Producer = GetStringValue("producer", "");
            docInfo.RevisionTime = GetDateTimeValue("revisionTime", DateTime.Now);
            docInfo.Subject = GetStringValue("subject", "");
            docInfo.Title = GetStringValue("title", "");
            return docInfo;
        }
    }

    internal class HtmlExportFilterOptions : ExportFilterOptions
    {
        public override string Extension
        {
            get
            {
                return SingleFile ? "html" : "zip";
            }
        }

        public override string FilterExtension
        {
            get
            {
                return "html";
            }
        }

        public HtmlExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        private bool SingleFile
        {
            get { return GetBooleanValue("singleFile", true); }
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new HtmlFilter
            {
                Paged = GetBooleanValue("paged", true),
                Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted),
                ShowNavigator = GetBooleanValue("showNavigator", false),
                SingleFile = GetBooleanValue("singleFile", true),
                NavigatorPosition = GetEnumValue<HtmlFilter.NavigatorPositions>("navigatorPosition", HtmlFilter.NavigatorPositions.Left),
            };

            return filter;
        }
    }

    internal class PdfExportFilterOptions : ExportFilterOptions
    {
        public override string Extension
        {
            get
            {
                return "pdf";
            }
        }

        public PdfExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new PdfFilter();
            filter.EmbedFonts = GetBooleanValue("embedFonts", true);
            filter.PdfACompatible = GetBooleanValue("pdfACompatible", true);
            filter.UseCompression = GetBooleanValue("useCompression", true);
            filter.UseOutlines = GetBooleanValue("useOutlines", true);

            //PdfSecurityOptions
            filter.PdfSecurityOptions.AllowCopyContent = GetBooleanValue("allowCopyContent", true);
            filter.PdfSecurityOptions.AllowEditAnnotations = GetBooleanValue("allowEditAnnotations", true);
            filter.PdfSecurityOptions.AllowEditContent = GetBooleanValue("allowEditContent", true);
            filter.PdfSecurityOptions.AllowPrint = GetBooleanValue("allowPrint", true);
            filter.PdfSecurityOptions.OwnerPassword = GetStringValue("ownerPassword", "");
            filter.PdfSecurityOptions.UserPassword = GetStringValue("userPassword", "");
            filter.PdfSecurityOptions.EncryptionType = GetEnumValue("encryptionType", PdfEncryptionType.Standard40);

            filter.Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted);
            filter.DocumentInfo = GetDocumentInfo();
            return filter;
        }
    }

    internal class RtfExportFilterOptions : ExportFilterOptions
    {
        public override string Extension
        {
            get
            {
                return "rtf";
            }
        }

        public RtfExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new RtfFilter();
            filter.OpenXml = false;
            filter.Paged = GetBooleanValue("paged", true);
            filter.Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted);
            filter.ShapesWord2007Compatible = GetBooleanValue("shapesWord2007Compatible", true);
            return filter;
        }
    }

    internal class DocxExportFilterOptions : RtfExportFilterOptions
    {
        public DocxExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        public override string Extension
        {
            get
            {
                return "docx";
            }
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = base.CreateExportFilter() as RtfFilter;
            filter.OpenXml = true;
            filter.DocumentInfo = GetDocumentInfo();
            return filter;
        }
    }

    internal class XlsExportFilterOptions : ExportFilterOptions
    {
        public override string Extension
        {
            get
            {
                return "xls";
            }
        }

        public XlsExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new XlsFilter();
            filter.OpenXml = false;
            filter.Tolerance = GetIntValue("tolerance", 0);
            filter.PictureLayer = GetBooleanValue("pictureLayer", false);
            filter.SheetName = GetStringValue("sheetName", "Sheet1");
            filter.DocumentInfo = GetDocumentInfo();
            return filter;
        }
    }

    internal class XlsxExportFilterOptions : XlsExportFilterOptions
    {
        public override string Extension
        {
            get
            {
                return "xlsx";
            }
        }

        public XlsxExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = base.CreateExportFilter() as XlsFilter;
            filter.OpenXml = true;
            return filter;
        }
    }

    internal class TiffExportFilterOptions : ExportFilterOptions
    {
        public override string Extension
        {
            get
            {
                return "tiff";
            }
        }

        public TiffExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new TiffFilter();
            filter.Monochrome = GetBooleanValue("monochrome", true);
            filter.Resolution = GetFloatValue("resolution", 300);
            filter.Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted);
            return filter;
        }
    }

    internal abstract class ImageExportFilterOptions : ExportFilterOptions
    {
        private OutputRange OutputRange
        {
            get
            {
                return GetOutputRange(null, null);
            }
        }

        private bool MultiFile
        {
            get
            {
                int page;
                return !(OutputRange != null && int.TryParse(OutputRange.ToString(), out page));
            }
        }

        protected abstract string ImageExtension { get; }

        public override string Extension
        {
            get
            {
                return MultiFile ? "zip" : ImageExtension;
            }
        }

        public ImageExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        public override ExportFilter ToExportFilter(string tempFolder)
        {
            var filter = base.ToExportFilter(tempFolder);
            filter.UseZipForMultipleFiles = MultiFile;
            return filter;
        }
    }

    internal class BmpExportFilterOptions : ImageExportFilterOptions
    {
        protected override string ImageExtension
        {
            get
            {
                return "bmp";
            }
        }

        public BmpExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new BmpFilter();
            filter.Resolution = GetFloatValue("resolution", 300);
            filter.Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted);
            return filter;
        }
    }

    internal class PngExportFilterOptions : ImageExportFilterOptions
    {
        protected override string ImageExtension
        {
            get
            {
                return "png";
            }
        }

        public PngExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new PngFilter();
            filter.Resolution = GetFloatValue("resolution", 300);
            filter.Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted);
            return filter;
        }
    }

    internal class JpegExportFilterOptions : ImageExportFilterOptions
    {
        protected override string ImageExtension
        {
            get
            {
                return "jpg";
            }
        }

        public JpegExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new JpegFilter();
            filter.Resolution = GetFloatValue("resolution", 300);
            filter.Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted);
            return filter;
        }
    }

    internal class GifExportFilterOptions : ImageExportFilterOptions
    {
        protected override string ImageExtension
        {
            get
            {
                return "gif";
            }
        }

        public GifExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new GifFilter();
            filter.Resolution = GetFloatValue("resolution", 300);
            filter.Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted);
            return filter;
        }
    }

    internal class MetaExportFilterOptions : ImageExportFilterOptions
    {
        protected override string ImageExtension
        {
            get
            {
                return "emf";
            }
        }

        public MetaExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new MetafileFilter();
            filter.MetafileType = GetEnumValue("metafileType", EmfType.EmfPlusDual);
            filter.Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted);
            return filter;
        }
    }
}
