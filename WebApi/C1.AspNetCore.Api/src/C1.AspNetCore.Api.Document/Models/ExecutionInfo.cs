﻿using System;

namespace C1.Web.Api.Document.Models
{
    /// <summary>
    /// Represents the info of the document execution.
    /// </summary>
    public class ExecutionInfo
    {
        internal ExecutionInfo(ICache cache, Document doc, bool detailed = true)
        {
            LoadedDateTime = cache.CreatedTime;
            ExpiredDateTime = cache.ExpiredTime;
            Path = doc.Path;
            if (detailed)
            {
                PageSettings = doc.PageSettings;
                Features = doc.GetFeatures();
                Status = new DocumentStatus(cache, doc);
            }
        }

        /// <summary>
        /// Gets the loaded date time of the document.
        /// </summary>
        public DateTime LoadedDateTime { get; private set; }
        /// <summary>
        /// Gets the expired date time of the cache.
        /// </summary>
        public DateTime ExpiredDateTime { get; private set; }
        /// <summary>
        /// Gets the path of the document.
        /// </summary>
        public string Path { get; private set; }
        /// <summary>
        /// Gets the page settings of the document.
        /// </summary>
        public PageSettings PageSettings { get; private set; }
        /// <summary>
        /// Gets the features supported by the document.
        /// </summary>
        public IDocumentFeatures Features { get; private set; }
        /// <summary>
        /// Gets the status of document.
        /// </summary>
        public virtual DocumentStatus Status { get; private set; }
        /// <summary>
        /// Gets the location to get outlines.
        /// </summary>
        public string OutlinesLocation
        {
            get;
            [SmartAssembly.Attributes.DoNotObfuscate]
            internal set;
        }
        /// <summary>
        /// Gets the location to get status.
        /// </summary>
        public string StatusLocation
        {
            get;
            [SmartAssembly.Attributes.DoNotObfuscate]
            internal set;
        }
        /// <summary>
        /// Gets the location to get page settings.
        /// </summary>
        public string PageSettingsLocation
        {
            get;
            [SmartAssembly.Attributes.DoNotObfuscate]
            internal set;
        }
        /// <summary>
        /// Gets the location to get features.
        /// </summary>
        public string FeaturesLocation
        {
            get;
            [SmartAssembly.Attributes.DoNotObfuscate]
            internal set;
        }
        /// <summary>
        /// Gets the location to getting supported formats.
        /// </summary>
        public string SupportedFormatsLocation
        {
            get;
            [SmartAssembly.Attributes.DoNotObfuscate]
            internal set;
        }
    }
}
