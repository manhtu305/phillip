﻿using System;

namespace C1.Web.Api.Document.Models
{
    /// <summary>
    /// Represents the status of the document execution.
    /// </summary>
    public class DocumentStatus
    {
        internal DocumentStatus(ICache cache, Document doc)
        {
            ExpiredDateTime = cache.ExpiredTime;
            Status = doc.Status;
            ErrorList = doc.ErrorList.ToArray();
            Progress = doc.Progress;
            PageCount = doc.PageCount;
            HasOutlines = doc.HasOutlines;
        }

        /// <summary>
        /// Gets the expired date time of the cache.
        /// </summary>
        public DateTime ExpiredDateTime { get; private set; }
        /// <summary>
        /// Gets a boolean value indicating whether the document has outlines.
        /// </summary>
        public bool HasOutlines { get; private set; }

        /// <summary>
        /// Gets the execution status of the document.
        /// </summary>
        public string Status
        {
            get;
            [SmartAssembly.Attributes.DoNotObfuscate]
            internal set;
        }
        /// <summary>
        /// Gets the collection of error message string.
        /// </summary>
        public string[] ErrorList { get; private set; }
        /// <summary>
        /// Gets the progress of the execution.
        /// </summary>
        public double Progress { get; private set; }
        /// <summary>
        /// Gets the rendering completed page count.
        /// </summary>
        public int PageCount { get; private set; }
    }
}
