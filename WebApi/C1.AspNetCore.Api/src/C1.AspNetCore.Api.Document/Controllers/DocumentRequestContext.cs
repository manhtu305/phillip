﻿using C1.Web.Api.Document.Localization;
using C1.Web.Api.Document.Models;
using C1.Win.C1Document;
using C1.Win.C1Document.Export;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;

namespace C1.Web.Api.Document
{
    /// <summary>
    /// Defines document request.
    /// </summary>
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal abstract class DocumentRequestContext<TDocument>
        where TDocument:Models.Document
    {
        private const string DEFAULT_FILE_NAME = "result";
        private TDocument _document;
        private Lazy<NameValueCollection> _requestParams;

        #region settings
        protected NameValueCollection RequestParams
        {
            get
            {
                return _requestParams == null ? null : _requestParams.Value;
            }
        }

        public void SetRequestParamsGetter(Func<NameValueCollection> getter)
        {
            _requestParams = new Lazy<NameValueCollection>(getter);
        }
        #endregion

        #region document
        protected virtual TDocument Document
        {
            get
            {
                return _document ?? (_document = GetDocument());
            }
        }

        protected abstract TDocument GetDocument();

        #endregion

        #region actions

        public ExportFilterOptions GetExportFilterOptions(IDictionary<string, string> exportOptions)
        {
            if (exportOptions == null)
            {
                exportOptions = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            }

            if (!exportOptions.ContainsKey("format"))
            {
                exportOptions["format"] = "html";
            }

            return Document.GetExportFilterOptions(exportOptions);
        }

        public ExportFilter GetExportFilter(ExportFilterOptions options)
        {
            var filter = options.ToExportFilter(Document.ExportTempFolder);
            ValidateOutputRange(filter.Range);
            return filter;
        }

        private void ValidateOutputRange(OutputRange range)
        {
            if (range.Type != OutputRangeType.All)
            {
                if (range.GetMaxPage(1) == 0)
                {
                    throw new StatusCodeException(422, string.Format(Resources.PageRangeInvalidRange));
                }
            }

            const int start = 1;
            var end = Document.PageCount;

            // the document is not generated.
            if (end <= 0) return;

            var hasValidPage = false;
            for (var i = start; i <= end; i++)
            {
                if (range.Contains(i))
                {
                    hasValidPage = true;
                    break;
                }
            }

            if (!hasValidPage)
            {
                throw new StatusCodeException(422, string.Format(Resources.PageRangeOutOfRange, range.ToString(), end));
            }
        }

        public virtual Stream Export(ExportFilter exportFilter, ExportFilterOptions exportFilterOptions)
        {
            return Document.Export(exportFilter, exportFilterOptions);
        }

        public IEnumerable<ExportDescription> GetSupportedFormats()
        {
            return Document.GetSupportedFormats();
        }

        public ExportDescription GetSupportedFormatByName(string name)
        {
            var format = Document.GetSupportedFormatByName(name);
            if (format == null)
            {
                throw new NotFoundException();
            }
            return format;
        }

        public string GetExportFullFileName(string fileName, ExportFilterOptions options)
        {
            return string.Format("{0}.{1}", GetExportFileName(fileName), GetExportFileExtension(options));
        }

        private string GetExportFileName(string fileName)
        {
            if (!string.IsNullOrEmpty(fileName))
            {
                return fileName;
            }

            var documentName = Document.Name;
            if (!string.IsNullOrEmpty(documentName))
            {
                return documentName;
            }

            return DEFAULT_FILE_NAME;
        }

        private string GetExportFileExtension(ExportFilterOptions options)
        {
            return options.Extension;
        }

        #endregion
    }
}
