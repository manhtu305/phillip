﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace C1.Web.Api.Document
{
    /// <summary>
    /// The abstract controller for processing document.
    /// </summary>
    public abstract class DocumentController : C1ApiController
    {
        private static JsonSerializerSettings _defaultJsonSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            DefaultValueHandling = DefaultValueHandling.Include,
            NullValueHandling = NullValueHandling.Ignore,
            DateFormatString = "yyyy/MM/dd HH:mm:ss",
        };

        /// <summary>
        /// Initializes a new instance of <see cref="DocumentController"/> object.
        /// </summary>
        public DocumentController()
        {
            SetJsonSettings(_defaultJsonSettings);
        }
    }
}
