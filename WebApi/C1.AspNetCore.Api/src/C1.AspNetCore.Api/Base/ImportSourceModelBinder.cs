﻿using Newtonsoft.Json;
using System.Threading.Tasks;
using System;
using System.Text;

#if !ASPNETCORE && !NETCORE
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
#else
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Net.Http.Headers;
#endif

namespace C1.Web.Api
{
    /// <summary>
    /// ModelBinder for uploaded file as import source.
    /// </summary>
    public class ImportSourceModelBinder : IModelBinder
    {
#if !ASPNETCORE && !NETCORE
        /// <summary>
        /// Binds the model to a value by using the specified controller context and binding context.
        /// </summary>
        /// <param name="actionContext">The action context.</param>
        /// <param name="bindingContext">The binding context.</param>
        /// <returns>true if model binding is successful; otherwise, false.</returns>
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var task = BindModelAsync(actionContext, bindingContext);
            task.Wait();
            return task.Result;
        }

        private async Task<bool> BindModelAsync(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var stream = await actionContext.Request.Content.ReadAsStreamAsync();
            var parser = new MultipartFormDataParser(stream);
            if (parser.Success)
            {
                var result = new ImportSource(parser.Filename,
                    new System.IO.MemoryStream(parser.FileContents));
                bindingContext.Model = result;
                return true;
            }
            return false;
        }
#else
        /// <summary>
        /// Binds the model to a value by using the binding context.
        /// </summary>
        /// <param name="bindingContext">The binding context.</param>
        /// <returns>The binding result.</returns>
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var form = bindingContext.HttpContext.Request.Form;

            if (form.Files != null && form.Files.Count > 0)
            {
                var file = form.Files[0];
                var result = new ImportSource(file.FileName, file.OpenReadStream());
                bindingContext.Result = ModelBindingResult.Success(result);
            }
            else
            {
                bindingContext.Result = ModelBindingResult.Failed();
            }

            return Task.FromResult(0);
        }
#endif
    }

    /// <summary>
    /// An attribute to apply to action parameters for model binding for import.
    /// </summary>
    [AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = true)]
    public class ImportModelBinderAttribute : ModelBinderAttribute
    {
        /// <summary>
        /// The constructor of ImportModelBinderAttribute class.
        /// </summary>
        public ImportModelBinderAttribute()
            : base()
        {
            BinderType = typeof(ImportSourceModelBinder);
        }
    }
}