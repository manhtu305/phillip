﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace C1.Web.Api
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal static class C1XmlHelper
    {
        private static void SerializeCore(object value, XmlWriter writer, Type[] extraTypes = null)
        {
            if (value != null)
            {
              // Add support serialize a JObject
              if (value is JObject)
              {
                XmlDocument doc = JsonConvert.DeserializeXmlNode(value.ToString(), "root");
                doc.WriteTo(writer);
              }
              else
              {
                var xmlSerializer = new XmlSerializer(value.GetType(), extraTypes);
                xmlSerializer.Serialize(writer, value);
              }
            }
        }

        public static void Serialize(object value, Stream output, Type[] extraTypes = null, XmlWriterSettings writerSettings = null)
        {
            using (var xmlWriter = XmlWriter.Create(output, writerSettings))
            {
                SerializeCore(value, xmlWriter, extraTypes);
            }
        }

        public static Stream SerializeToStream(object value, Type[] extraTypes = null, XmlWriterSettings writerSettings = null)
        {
            var stream = new MemoryStream();
            Serialize(value, stream, extraTypes, writerSettings);
            return stream;
        }

        public static void SerializeToFile(object value, string path, Type[] extraTypes = null, XmlWriterSettings writerSettings = null)
        {
            using (var stream = new FileStream(path, FileMode.OpenOrCreate))
            {
                Serialize(value, stream, extraTypes);
            }
        }

        public static T Deserialize<T>(Stream stream)
        {
            using (var reader = XmlReader.Create(stream))
            {
                var ser = new XmlSerializer(typeof (T));
                return (T) ser.Deserialize(reader);
            }
        }

        internal static IEnumerable GetData(string path)
        {
            return GetData(XDocument.Load(path).Root);
        }

        internal static IEnumerable GetData(XElement xElement)
        {
            var obj = new ExpandoObject();
            XmlToDynamicParse(obj, xElement);
            return FindFirstCollection(obj) ?? UseFirstChildAsCollection(obj);
        }

        private static IEnumerable UseFirstChildAsCollection(ExpandoObject obj)
        {
            var root = obj.FirstOrDefault().Value as IDictionary<string, object>;
            if (root == null)
            {
                return null;
            }

            var child = root.FirstOrDefault().Value;
            return child == null ? null : new [] { child };
        }

        private static IEnumerable FindFirstCollection(object obj)
        {
            if (obj is IDictionary<string, object>)
            {
                foreach (var item in (IDictionary<string, object>) obj)
                {
                    var result = FindFirstCollection(item.Value);
                    if (result != null)
                    {
                        return result;
                    }
                }
            }
            else if (obj is IEnumerable && !(obj is string))
            {
                return (IEnumerable) obj;
            }

            return null;
        }

        internal static IEnumerable GetData(Stream dataStream)
        {
            dataStream.Position = 0;
            return GetData(XDocument.Load(dataStream).Root);
        }

        public static void XmlToDynamicParse(dynamic parent, XElement node)
        {
            if (node.HasElements)
            {
                if (node.Elements(node.Elements().First().Name.LocalName).Count() > 1)
                {
                    //list
                    var item = new ExpandoObject();
                    var list = new List<dynamic>();
                    foreach (var element in node.Elements())
                    {
                        XmlToDynamicParse(list, element);
                    }

                    AddProperty(item, node.Elements().First().Name.LocalName, list);
                    AddProperty(parent, node.Name.ToString(), item);
                }
                else
                {
                    var item = new ExpandoObject();

                    foreach (var attribute in node.Attributes())
                    {
                        AddProperty(item, attribute.Name.ToString(), attribute.Value.Trim());
                    }

                    //element
                    foreach (var element in node.Elements())
                    {
                        XmlToDynamicParse(item, element);
                    }

                    AddProperty(parent, node.Name.ToString(), item);
                }
            }
            else
            {
                AddProperty(parent, node.Name.ToString(), node.Value.Trim());
            }
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        private static void AddProperty(dynamic parent, string name, object value)
        {
            if (parent is List<dynamic>)
            {
                (parent as List<dynamic>).Add(value);
            }
            else
            {
                (parent as IDictionary<String, object>)[name] = value;
            }
        }

    }
}