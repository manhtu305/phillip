﻿using System;
using System.IO;

namespace C1.Web.Api
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal class FileStorage
    {
        private readonly Func<Stream> _reader;
        public FileStorage(Func<Stream> reader, string extension)
        {
            _reader = reader;
            Extension = extension;
        }

        public FileStorage(Stream stream, string extension) : this(() => stream, extension)
        {
        }

        public string Extension
        {
            get;
            private set;
        }

        public virtual Stream Read()
        {
            var stream = _reader();
            if(stream != null)
            {
                stream.Position = 0;
            }

            return stream;
        }
    }
}
