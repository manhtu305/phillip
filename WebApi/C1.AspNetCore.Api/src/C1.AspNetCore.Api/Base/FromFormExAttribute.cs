﻿using System;
using SmartAssembly.Attributes;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Specialized;
#if ASPNETCORE || NETCORE
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Http;
#else
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
#endif

namespace C1.Web.Api
{
    /// <summary>
    /// Defines a <see cref="ModelBinderAttribute"/> object to deserialize the values from form.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Parameter, AllowMultiple = false, Inherited = true)]
    public class FromFormExAttribute : ModelBinderAttribute
    {
        /// <summary>
        /// Initialize a <see cref="FromFormExAttribute"/> instance.
        /// </summary>
        /// <param name="checkPrefix">A bool value indicates whether with prefix or without.</param>
        public FromFormExAttribute(bool checkPrefix = false)
        {
            BinderType = checkPrefix ? typeof(FromFormExModelBinderWithPrefix) : typeof(FromFormExModelBinder);
        }
    }

#if ASPNETCORE || NETCORE
    internal class FormDataWrapper
    {
        private IFormCollection _data;

        public FormDataWrapper(IFormCollection formData)
        {
            _data = formData;
        }

        public IEnumerable<string> Keys
        {
            get { return _data.Keys; }
        }

        public bool ContainsKey(string key)
        {
            return _data.ContainsKey(key);
        }

        public string Get(string key)
        {
            if (!ContainsKey(key)) return null;

            var values = _data[key];
            return string.Join(",", values);
        }

        public string[] GetValues(string key)
        {
            if (!ContainsKey(key)) return null;

            var values = _data[key];
            return values.ToArray();
        }
    }
#else
    internal class FormDataWrapper
    {
        private NameValueCollection _data;

        public FormDataWrapper(NameValueCollection formData)
        {
            _data = formData;
        }

        public IEnumerable<string> Keys
        {
            get { return _data.AllKeys; }
        }

        public bool ContainsKey(string key)
        {
            // dont use _data._data.AllKeys.Contains(key)
            // it uses case sensitive comparer

            return _data[key] != null;
        }

        public string Get(string key)
        {
            return _data.Get(key);
        }

        public string[] GetValues(string key)
        {
            return _data.GetValues(key);
        }
    }
#endif

    internal abstract class FromFormExModelBinderBase : IModelBinder
    {
#if ASPNETCORE || NETCORE
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var request = bindingContext.HttpContext.Request;
            if (!request.ContentLength.HasValue || request.ContentLength.Value == 0)
            {
                return Task.FromResult(0);
            }

            var formData = request.Form;
            if (formData != null)
            {
                bindingContext.Result = ModelBindingResult.Success(GetModelValue(bindingContext, new FormDataWrapper(formData)));
            }

            return Task.FromResult(0);
        }
#else
        private static string FormDataKey = "0448F271-A8C4-48ED-9753-A97C4F9511EE";
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            object data;
            if (!actionContext.Request.Properties.TryGetValue(FormDataKey, out data))
            {
                // read the form data
                var task = actionContext.Request.Content.ReadAsFormDataAsync();
                task.Wait();
                data = task.Result;
                actionContext.Request.Properties[FormDataKey] = data;

                // seek the content stream
                var taskReadStream = actionContext.Request.Content.ReadAsStreamAsync();
                taskReadStream.Wait();
                var stream = taskReadStream.Result;
                if (stream.CanSeek)
                {
                    stream.Seek(0, System.IO.SeekOrigin.Begin);
                }
            }

            var formData = data as NameValueCollection;
            if (formData == null) return false;

            bindingContext.Model = GetModelValue(bindingContext, new FormDataWrapper(formData));
            return true;
        }
#endif

        protected abstract object GetModelValue(ModelBindingContext bindingContext, FormDataWrapper formData);

        protected object GetDictionary(FormDataWrapper formData, string prefix, Type modelType)
        {
            if (!modelType.IsGenericType || modelType.Name != "IDictionary`2")
            {
                return null;
            }

            bool hasPrefix = !string.IsNullOrEmpty(prefix);
            var keys = hasPrefix
                ? formData.Keys.Where(k => k.StartsWith(prefix + ".", StringComparison.OrdinalIgnoreCase))
                : formData.Keys;
            var index = hasPrefix ? prefix.Length + 1 : 0;

            var genericTypeArgs = modelType.GenericTypeArguments;
            if (genericTypeArgs.Length == 2 && genericTypeArgs[1] == typeof(string))
            {
                return keys.ToDictionary(k => k.Substring(index), k=>formData.Get(k), StringComparer.OrdinalIgnoreCase);
            }
            else
            {
                return keys.ToDictionary(k => k.Substring(index), k=> formData.GetValues(k), StringComparer.OrdinalIgnoreCase);
            }
        }

        protected object GetObject(FormDataWrapper formData, string prefix, Type modelType)
        {
            bool hasPrefix = !string.IsNullOrEmpty(prefix);
            var propertyPrefix = hasPrefix ? prefix + "." : "";

            try
            {
                var model = Activator.CreateInstance(modelType);
                foreach (var property in modelType.GetProperties())
                {
                    if (!property.CanWrite) continue;

                    var key = propertyPrefix + property.Name;
                    if (!formData.ContainsKey(key)) continue;

                    var pType = property.PropertyType;
                    if (pType.IsArray)
                    {
                        property.SetValue(model, GetArray(formData.GetValues(key), pType.GetElementType()));
                        continue;
                    }

                    object pvalue;
                    if (GetSerializedObj(formData.Get(key), pType, out pvalue))
                    {
                        property.SetValue(model, pvalue);
                    }
                }

                return model;
            }
            catch
            {
                return null;
            }
        }

        internal static Array GetArray(object value, Type elementType)
        {
            if (value == null) return null;

            var list = value as IList<object> ?? new List<object> { value };
            var count = list.Count;
            var array = Array.CreateInstance(elementType, count);

            for (var i = 0; i < count; i++)
            {
                object elementValue;
                if (GetSerializedObj(list[i], elementType, out elementValue))
                {
                    array.SetValue(elementValue, i);
                }
            }

            return array;
        }

        internal static bool GetSerializedObj(object value, Type type, out object obj)
        {
            var underlyingType = Nullable.GetUnderlyingType(type);
            if (underlyingType != null)
            {
                if (GetSerializedObj(value, underlyingType, out obj))
                {
                    return true;
                }

                return false;
            }

            obj = null;
            var strValue = value as string;
            if (type.IsEnum)
            {
                try
                {
                    obj = Enum.Parse(type, strValue, true);
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }

            try
            {
                obj = Convert.ChangeType(value, type);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

    internal class FromFormExModelBinderWithPrefix : FromFormExModelBinderBase
    {
        protected override object GetModelValue(ModelBindingContext bindingContext, FormDataWrapper formData)
        {
            var name = bindingContext.ModelName;
            if (formData == null || !ContainsPrefix(formData, name))
            {
                return null;
            }

            object model = GetDictionary(formData, name, bindingContext.ModelType)
                ?? GetObject(formData, name, bindingContext.ModelType);

            return model;
        }

        private static bool ContainsPrefix(FormDataWrapper data, string prefix)
        {
            var s = prefix + ".";
            foreach (string key in data.Keys)
            {
                if (key.StartsWith(s, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }
    }

    internal class FromFormExModelBinder : FromFormExModelBinderBase
    {
        protected override object GetModelValue(ModelBindingContext bindingContext, FormDataWrapper formData)
        {
            if (formData == null) return null;

            object model = GetDictionary(formData, "", bindingContext.ModelType)
                ?? GetSimpleValue(formData, bindingContext.ModelName, bindingContext.ModelType)
                ?? GetObject(formData, "", bindingContext.ModelType);

            return model;
        }

        private object GetSimpleValue(FormDataWrapper formData, string name, Type valueType)
        {
            if (!formData.ContainsKey(name)) return null;

            if (valueType.IsArray)
            {
                return GetArray(formData.GetValues(name), valueType.GetElementType());
            }

            object value;
            if(GetSerializedObj(formData.Get(name), valueType, out value))
            {
                return value;
            }

            return null;
        }
    }
}
