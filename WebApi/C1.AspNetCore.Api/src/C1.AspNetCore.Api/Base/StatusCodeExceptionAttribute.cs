﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
#if NETCORE || ASPNETCORE
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
#else
using System.Net;
using System.Web.Http.Filters;
using System.Net.Http;
#endif

namespace C1.Web.Api
{
    /// <summary>
    /// Filter the <see cref="StatusCodeException"/> and return the response with specific status code.
    /// </summary>
    /// <remarks>
    /// In ASPNET, it can filter the exception on executing the action and result (IHttpActionResult.ExecuteAsync).
    /// In ASPNETCORE, it can only filter the exception on excuting the action.
    /// </remarks>
    internal class StatusCodeExceptionFilterAttribute : ExceptionFilterAttribute
    {
#if !ASPNETCORE && !NETCORE
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            var sce = actionExecutedContext.Exception as StatusCodeException;
            if (sce != null)
            {
                var response = actionExecutedContext.Request.CreateErrorResponse((HttpStatusCode)sce.StatusCode, sce.Message);
                actionExecutedContext.Response = response;
            }
            else
            {
                base.OnException(actionExecutedContext);
            }
        }
#else
        public override void OnException(ExceptionContext context)
        {
            var sce = context.Exception as StatusCodeException;
            if (sce != null)
            {
                var result = new ObjectResult(sce.Message);
                result.StatusCode = sce.StatusCode;
                context.Result = result;
            }
            else
            {
                base.OnException(context);
            }
        }
#endif
    }
}
