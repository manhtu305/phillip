﻿using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace C1.Web.Api
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal class C1JsonHelper
    {
        public static T Deserialize<T>(Stream stream)
        {
            return Deserialize<T>(StreamToString(stream));
        }

        public static T Deserialize<T>(string content)
        {
            using (var textReader = new StringReader(content))
            using (var reader = new JsonTextReader(textReader))
            {
                return new JsonSerializer().Deserialize<T>(reader);
            }
        }
        public static object Deserialize(string content, Type type = null)
        {
            using (var textReader = new StringReader(content))
            using (var reader = new JsonTextReader(textReader))
            {
                type = type ?? typeof (object);
                return new JsonSerializer().Deserialize(reader, type);
            }
        }

        public static Stream Serialize<T>(T obj)
        {
            var stream = new MemoryStream();
            using (var textWriter = new StreamWriter(stream))
            using (var writer = new JsonTextWriter(textWriter))
            {
                new JsonSerializer().Serialize(writer, obj);
            }

            stream.Position = 0;
            return stream;
        }

        public static void Serialize<T>(T obj, string path)
        {
            using (var stream = File.Create(path))
            using (var textWriter = new StreamWriter(stream))
            using (var writer = new JsonTextWriter(textWriter))
            {
                new JsonSerializer().Serialize(writer, obj);
            }
        }

        public static string StreamToString(Stream stream)
        {
            stream.Position = 0;
            using (var reader = new StreamReader(stream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }
    }
}