﻿#if !ASPNETCORE && !NETCORE
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace C1.Web.Api
{
    /// <summary>
    /// Provides HTTP content to be downloaded as attachments.
    /// </summary>
    public class AttachmentContent : StreamContent
    {
        private const string contentDisposition = "attachment";

        /// <summary>
        /// Creates an AttachmentContent instance.
        /// </summary>
        /// <param name="stream">the content</param>
        /// <param name="fileName">the download name</param>
        public AttachmentContent(Stream stream, string fileName)
            : base(stream)
        {
            Headers.ContentDisposition = new ContentDispositionHeaderValue(contentDisposition)
            {
                FileName = WebUtility.UrlEncode(fileName),
                FileNameStar = fileName     // for non-ASCII character support
            };
        }
    }
}
#endif