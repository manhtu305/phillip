﻿using System.IO;
using System.Threading.Tasks;

namespace C1.Web.Api
{
    /// <summary>
    /// Common interface for all exporters to share.
    /// </summary>
    public interface IExporter
    {
        /// <summary>
        /// Execute exporting task asynchronously.
        /// </summary>
        /// <param name="exportSource">The export source.</param>
        /// <param name="outputStream">The output stream.</param>
        /// <returns></returns>
        Task ExportAsync(object exportSource, Stream outputStream);
    }

    /// <summary>
    /// Common generic interface for all exporters to share.
    /// </summary>
    /// <typeparam name="T">Type of ExportSource.</typeparam>
    public interface IExporter<T> : IExporter
        where T : ExportSource
    {
        /// <summary>
        /// Execute exporting task asynchronously.
        /// </summary>
        /// <param name="exportSource">The export source.</param>
        /// <param name="outputStream">The output stream.</param>
        /// <returns>The task.</returns>
        Task ExportAsync(T exportSource, Stream outputStream);
    }

    /// <summary>
    /// Common generic interface for all importers to share.
    /// </summary>
    /// <typeparam name="TSource">Type of ImportSource.</typeparam>
    /// <typeparam name="TResult">Type of import result.</typeparam>
    public interface IImporter<TSource, TResult>
        where TSource : ImportSource
    {
        /// <summary>
        /// Execute importing task asynchronously.
        /// </summary>
        /// <param name="importSource">The import source.</param>
        /// <returns>The task.</returns>
        Task<TResult> ImportAsync(TSource importSource);
    }
}