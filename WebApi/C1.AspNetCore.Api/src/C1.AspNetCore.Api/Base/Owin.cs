﻿using System;

namespace C1.Web.Api
{
    /// <summary>
    /// Owin utility functions.
    /// </summary>
    public static class Owin
    {
        // http://stackoverflow.com/questions/24571258/how-do-you-resolve-a-virtual-path-to-a-file-under-an-owin-host
        /// <summary>
        /// Gets the root directory of the application.
        /// </summary>
        /// <returns>The root directory.</returns>
        public static string GetApplicationRoot()
        {
#if NETCORE
      return AppDomain.CurrentDomain.BaseDirectory;
#else
            return AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
#endif
        }

        /// <summary>
        /// Gets the physical directory of a path.
        /// </summary>
        /// <param name="path">A path.</param>
        /// <returns>The physical directory.</returns>
        public static string MapPath(string path)
        {
            return System.IO.Path.Combine(GetApplicationRoot(), path.Replace("~/", ""));
        }
    }
}