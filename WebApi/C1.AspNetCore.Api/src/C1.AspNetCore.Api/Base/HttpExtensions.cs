﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Globalization;
#if ASPNETCORE || NETCORE
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
#else
using System.Net.Http;
using HttpRequest = System.Net.Http.HttpRequestMessage;
#endif
namespace C1.Web.Api
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal static class HttpExtensions
    {
        /// <summary>
        /// Gets the culture from Accept-Language header.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>The valid culture with highest quality.</returns>
        /// <remarks>
        /// The Accept-Language header is passed from client, like "en,zh-CN;q=0.8,zh;q=0.6".
        /// </remarks>
        public static CultureInfo GetAcceptCulture(this HttpRequest request)
        {
#if ASPNETCORE || NETCORE
            StringValues acceptLanguage;
            if (!request.Headers.TryGetValue("Accept-Language", out acceptLanguage)) return null;

            var languages = acceptLanguage.SelectMany(
                val => string.IsNullOrEmpty(val)
                    ? Enumerable.Empty<LanguageWithQuality>()
                    : val.Trim().Split(',').Select(lan => LanguageWithQuality.Parse(lan.Trim()))
                );
#else
            var acceptLanguage = request.Headers.AcceptLanguage;
            if (acceptLanguage == null || acceptLanguage.Count == 0) return null;

            var languages = acceptLanguage.Select(lan => new LanguageWithQuality(lan.Value, lan.Quality));
#endif

            CultureInfo culture = null;
            foreach (var lan in languages.OrderByDescending(lan=>lan.Quality))
            {
                try
                {
                    culture = new CultureInfo(lan.Name);
                    break;
                }
                catch
                {
                    continue;
                }
            }

            return culture;
        }

        private class LanguageWithQuality
        {
            public string Name { get; private set; }
            public double Quality { get; private set; }

            public LanguageWithQuality(string name, double? quality)
            {
                Name = name;
                Quality = quality.HasValue ? quality.Value : 1;
            }

            /// <summary>
            /// Parse the language string with quality.
            /// </summary>
            public static LanguageWithQuality Parse(string s)
            {
                var index = s.IndexOf(';');
                if (index > -1)
                {
                    // like "zh-CN;q=0.8".
                    var name = s.Substring(0, index).Trim();
                    var qualityString = s.Substring(index + 1).Trim();
                    if(qualityString.StartsWith("q=", StringComparison.InvariantCultureIgnoreCase))
                    {
                        qualityString = qualityString.Substring(2);
                    }

                    var quality = 0d;
                    double.TryParse(qualityString, out quality);
                    return new LanguageWithQuality(name, quality);
                }
                else
                {
                    // like "en", has highest quality.
                    return new LanguageWithQuality(s, 1d);
                }
            }

            public override string ToString()
            {
                return string.Format("{0};{1}", Name, Quality);
            }
        }
    }
}
