﻿using Newtonsoft.Json;
using System.Threading.Tasks;
using System;
using System.Text;
using System.Linq;

#if !ASPNETCORE && !NETCORE
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
#else
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Primitives;
#endif

namespace C1.Web.Api
{
    /// <summary>
    /// ModelBinder that reads information from the Request.Body. Please be aware that
    /// it is only safe to read the Request's Body one time.
    /// </summary>
    public class BodyModelBinder<T> : IModelBinder
    {
        private const string dataField = "data";  // name of the hidden field containing JSON data

#if !ASPNETCORE && !NETCORE
        /// <summary>
        /// Binds the model to a value by using the specified controller context and binding context.
        /// </summary>
        /// <param name="actionContext">The action context.</param>
        /// <param name="bindingContext">The binding context.</param>
        /// <returns>true if model binding is successful; otherwise, false.</returns>
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var task = BindModelAsync(actionContext, bindingContext);
            task.Wait();
            return task.Result;
        }

        private async Task<bool> BindModelAsync(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            string value;
            if (actionContext.Request.Content.IsFormData())
            {
                var formData = await actionContext.Request.Content.ReadAsFormDataAsync();
                value = formData.Get(dataField);
            }
            else
            {
                var content = actionContext.Request.Content;
                value = await content.ReadAsStringAsync();
            }
            bindingContext.Model = JsonConvert.DeserializeObject<T>(value);
            return bindingContext.Model != null;
        }
#else
        /// <summary>
        /// Binds the model to a value by using the binding context.
        /// </summary>
        /// <param name="bindingContext">The binding context.</param>
        /// <returns>The binding result.</returns>
        public virtual async Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var request = bindingContext.HttpContext.Request;
            var form = await request.ReadFormAsync();
            var value = string.Empty;

            if (bindingContext.ModelType == typeof(T) && form != null &&
                form[dataField] != StringValues.Empty)
            {
                value = form[dataField].FirstOrDefault();
            }
            else
            {
                var body = request.Body;
                var bytes = new byte[body.Length];
                body.Position = 0;
                await body.ReadAsync(bytes, 0, bytes.Length);
                value = Encoding.UTF8.GetString(bytes);
            }

            var model = JsonConvert.DeserializeObject<T>(value);
            bindingContext.Result = ((model != null) ? ModelBindingResult.Success( model) :
                ModelBindingResult.Failed());
        }
#endif
    }
}