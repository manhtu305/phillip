﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
#if ASPNETCORE || NETCORE
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
#else
using System.Net.Http;
using HttpRequest = System.Net.Http.HttpRequestMessage;
using RouteData = System.Web.Http.Routing.IHttpRouteData;
#endif

namespace C1.Web.Api
{
#if ASPNETCORE || NETCORE
    using Microsoft.AspNetCore.Mvc.ActionConstraints;
    using Microsoft.AspNetCore.Mvc.Routing;

    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal class PathRouteAttribute : Attribute, IActionConstraint, IRouteTemplateProvider
    {
        private int? _order;
        private readonly string _routeTemplate;
        private string _routeRegex;
        private IReadOnlyList<string> _routeValueNames;

        public PathRouteAttribute(string template)
        {
            Template = template;
            _routeTemplate = PathRouteHelper.RouteTemplate;
        }

        public string Name
        {
            get;
            set;
        }

        public int Order
        {
            get
            {
                return _order.HasValue ? _order.Value : 0;
            }
            set
            {
                _order = value;
            }
        }

        public string[] ExcludingKeywords { get; set; }

        public string Template
        {
            get;
        }

        string IRouteTemplateProvider.Template
        {
            get
            {
                return _routeTemplate;
            }
        }

        int? IRouteTemplateProvider.Order
        {
            get
            {
                return _order;
            }
        }

        public bool Accept(ActionConstraintContext context)
        {
            var values = context.RouteContext.RouteData.Values;
            _routeRegex = _routeRegex ?? PathRouteHelper.ToRegex(Template, out _routeValueNames);
            return PathRouteHelper.Match(values, _routeRegex, _routeValueNames, ExcludingKeywords);
        }
    }

#else

    using System.Net.Http;
    using System.Web.Http;
    using System.Web.Http.Routing;

    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal class PathRouteAttribute : Attribute, IDirectRouteFactory, IHttpRouteInfoProvider
    {
        private readonly RouteAttribute _impl;
        private readonly string _template;

        public PathRouteAttribute(string template)
        {
            _template = template;
            _impl = new RouteAttribute(PathRouteHelper.RouteTemplate);
        }

        public string Template
        {
            get
            {
                return _template;
            }
        }

        public string Name
        {
            get
            {
                return _impl.Name;
            }
        }

        public string[] ExcludingKeywords { get; set; }

        string IHttpRouteInfoProvider.Template
        {
            get
            {
                return _impl.Template;
            }
        }

        public int Order
        {
            get
            {
                return _impl.Order;
            }
            set
            {
                _impl.Order = value;
            }
        }

        public RouteEntry CreateRoute(DirectRouteFactoryContext context)
        {
            var route = (_impl as IDirectRouteFactory).CreateRoute(context);
            var pathRoute = new PathHttpRoute(route.Route, Template, ExcludingKeywords);
            var entry = new RouteEntry(route.Name, pathRoute);
            return entry;
        }

        private class PathHttpRoute : IHttpRoute
        {
            private readonly IHttpRoute _inner;
            private readonly string _template;
            private string _routeRegex;
            private IReadOnlyList<string> _routeValueNames;
            private string[] _excludes;

            public PathHttpRoute(IHttpRoute inner, string template, string[] excludes)
            {
                _inner = inner;
                _template = template;
                _excludes = excludes;
            }

            public IDictionary<string, object> Constraints
            {
                get
                {
                    return _inner.Constraints;
                }
            }

            public IDictionary<string, object> DataTokens
            {
                get
                {
                    return _inner.DataTokens;
                }
            }

            public IDictionary<string, object> Defaults
            {
                get
                {
                    return _inner.Defaults;
                }
            }

            public HttpMessageHandler Handler
            {
                get
                {
                    return _inner.Handler;
                }
            }

            public string RouteTemplate
            {
                get
                {
                    return _inner.RouteTemplate;
                }
            }

            public IHttpRouteData GetRouteData(string virtualPathRoot, HttpRequestMessage request)
            {
                var data = _inner.GetRouteData(virtualPathRoot, request);
                if(data == null)
                {
                    return null;
                }

                var values = data.Values;
                _routeRegex = _routeRegex ?? PathRouteHelper.ToRegex(_template, out _routeValueNames);
                return PathRouteHelper.Match(values, _routeRegex, _routeValueNames, _excludes) ? data : null;
            }

            public IHttpVirtualPathData GetVirtualPath(HttpRequestMessage request, IDictionary<string, object> values)
            {
                return _inner.GetVirtualPath(request, values);
            }
        }
    }
#endif

    internal static class PathRouteHelper
    {
        private const string _regexStart = "^";
        private const string _regexEnd = "$";
        private const string _pathSeparator = "/";
        private const string _routeValueKey = "__c1res";
        private const string _wildcard = "*";
        private const string _routeValueStart = "{";
        private const string _routeValueEnd = "}";
        private const string _wildcardRouteValueStart = _routeValueStart + _wildcard;
        public const string RouteTemplate = _wildcardRouteValueStart + _routeValueKey + _routeValueEnd;

        public static string ToRegex(string pathTemplate, out IReadOnlyList<string> names)
        {
            if (string.IsNullOrEmpty(pathTemplate))
            {
                names = new string[] { };
                return string.Empty;
            }

            var nameList = new List<string>();
            var parts = pathTemplate.Split(_pathSeparator.ToCharArray());
            var pattern = _regexStart;
            var lastIsWildcard = false;
            for (int i = 0, length = parts.Length; i < length; i++)
            {
                var part = parts[i];
                if (part.StartsWith(_routeValueStart) && part.EndsWith(_routeValueEnd))
                {
                    if (lastIsWildcard)
                    {
                        throw new InvalidOperationException(string.Format("Cannot use '{0}' as path route.", pathTemplate));
                    }

                    var partName = part.Substring(1, part.Length - _routeValueStart.Length - _routeValueEnd.Length);
                    if (part.StartsWith(_wildcardRouteValueStart))
                    {
                        partName = partName.Substring(_wildcard.Length);
                        pattern += "(?<" + partName + ">.*)";
                        nameList.Add(partName);
                        lastIsWildcard = true;
                    }
                    else
                    {
                        pattern += @"/(?<" + partName + ">[^/]+)";
                        nameList.Add(partName);
                        lastIsWildcard = false;
                    }

                    continue;
                }

                if (lastIsWildcard)
                {
                    // not end with this literal part
                    pattern += @"(?<!/" + Regex.Escape(part) + ")";
                    lastIsWildcard = false;
                }

                pattern += Regex.Escape(@"/" + part);
            }

            names = nameList.AsReadOnly();
            return pattern + _regexEnd;
        }

        public static string ResolveTemplate(string pathTemplate, TemplateDataResolver resolver)
        {
            if (string.IsNullOrEmpty(pathTemplate))
            {
                return string.Empty;
            }

            var parts = pathTemplate.Split(_pathSeparator.ToCharArray());
            for (int i = 0, length = parts.Length; i < length; i++)
            {
                var part = parts[i];
                if (part.StartsWith(_routeValueStart) && part.EndsWith(_routeValueEnd))
                {
                    var partName = part.Substring(1, part.Length - _routeValueStart.Length - _routeValueEnd.Length);
                    if (part.StartsWith(_wildcardRouteValueStart))
                    {
                        partName = partName.Substring(_wildcard.Length);
                    }

                    var value = resolver.Resolve(partName);
                    if (value != null)
                    {
                        parts[i] = value.ToString();
                    }
                }
            }

            return string.Join("/", parts);
        }

        public static bool Match(IDictionary<string, object> values, string regex, IReadOnlyList<string> names, string[] excludes)
        {
            if (values == null || !values.ContainsKey(_routeValueKey)
                || string.IsNullOrEmpty(regex) || names == null || !names.Any())
            {
                return false;
            }

            var res = (string)values[_routeValueKey] ?? string.Empty;

            if (excludes != null)
            {
                var resWithSlash = res.EndsWith("/") ? res : res + "/";
                if (!resWithSlash.StartsWith("/")) resWithSlash = "/" + resWithSlash;

                if (excludes.Any(s => resWithSlash.IndexOf("/" + s + "/", StringComparison.InvariantCultureIgnoreCase) > -1))
                {
                    return false;
                }
            }

            var result = Regex.Match(res, regex, RegexOptions.IgnoreCase);
            if (!result.Success)
            {
                return false;
            }

            foreach (var name in names)
            {
                var group = result.Groups[name];
                values[name] = group == null ? null : group.Value;
            }

            return true;
        }
    }

    internal class TemplateDataResolver
    {
        private IDictionary<string, object> _values;
        private RouteData _routeData;
        private HttpRequest _request;

        public TemplateDataResolver(IDictionary<string, object> values, RouteData routeData, HttpRequest request)
        {
            _values = values;
            _routeData = routeData;
            _request = request;
        }

        public object Resolve(string key)
        {
            object value;
            if (_values != null)
            {
                var dic = new Dictionary<string, object>(_values, StringComparer.OrdinalIgnoreCase);
                if (dic.TryGetValue(key, out value))
                {
                    return value;
                }
            }

            if (_routeData != null && _routeData.Values.TryGetValue(key, out value))
            {
                return value;
            }

            if (_request != null)
            {
                var parameters = _request.GetParams();
                return parameters[key]; ;
            }

            return null;
        }
    }
}
