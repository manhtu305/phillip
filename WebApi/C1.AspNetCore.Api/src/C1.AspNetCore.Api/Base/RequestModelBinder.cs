﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Collections;
#if !ASPNETCORE && !NETCORE
using C1.Web.Api.Localization;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using System.Net.Http;
using System.Threading;
#else
using System.Reflection;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Net.Http.Headers;
using Microsoft.Extensions.Primitives;
#endif

namespace C1.Web.Api
{
    /// <summary>
    /// The model binder for the parameter of C1 Web API action.
    /// </summary>
    /// <typeparam name="T">The type of the models</typeparam>
    public class RequestModelBinder<T> : IModelBinder
    {
#if !ASPNETCORE && !NETCORE
        /// <summary>
        /// Binds the model to a value by using the specified controller context and binding context.
        /// </summary>
        /// 
        /// <returns>
        /// true if model binding is successful; otherwise, false.
        /// </returns>
        /// <param name="actionContext">The action context.</param><param name="bindingContext">The binding context.</param>
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var task = BindModelAsync(actionContext, bindingContext);
            task.Wait();
            return task.Result;
        }

        private static async Task<bool> BindModelAsync(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var request = actionContext.Request;
            var values = new Dictionary<string, object>();
            if (actionContext.Request.Content.IsFormData())
            {
                await GetFormDataValues(actionContext, values);
            }
            else if (actionContext.Request.Content.IsMimeMultipartContent())
            {
                GetMultipartFormDataValues(request, values);
            }
            else
            {
                throw new InvalidOperationException(Resources.CannotReadRequestData);
            }

            bindingContext.Model = CreateModel(values);
            return bindingContext.Model != null;
        }

        private static void GetMultipartFormDataValues(HttpRequestMessage request, IDictionary<string, object> values)
        {
            var path = GetRandomFolder();
            var provider = new MultipartFormDataStreamProvider(path);

            //Web API 2's ReadAsMultipartAsync() is problematic. It often hangs and never returns.
            //http://stackoverflow.com/questions/15201255/request-content-readasmultipartasync-never-returns
            Task.Factory.StartNew(
                () => request.Content.ReadAsMultipartAsync(provider).Wait(),
                CancellationToken.None,
                TaskCreationOptions.LongRunning, // guarantees separate thread
                TaskScheduler.Default).Wait();

            foreach (string key in provider.FormData)
            {
                AddFormValue(values, key, provider.FormData[key]);
            }

            foreach (var file in provider.FileData)
            {
                var cd = file.Headers.ContentDisposition;
                if (cd == null)
                {
                    continue;
                }

                string name;
                string extension;
                if (!GetContentDispositionInfo(cd.FileName, cd.Name, out name, out extension))
                {
                    continue;
                }

                var stream = CloneStream(File.OpenRead(file.LocalFileName));
                AddFormValue(values, name, new FormFile(stream, extension));
            }

            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }
        }

        private static async Task GetFormDataValues(HttpActionContext actionContext, IDictionary<string, object> values)
        {
            var formData = await actionContext.Request.Content.ReadAsFormDataAsync();
            formData.Cast<string>().ToList().ForEach(s => AddFormValue(values, s, formData[s]));
        }

        private static string GetRandomFolder()
        {
            var path = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }
#else
        /// <summary>
        /// Async function to bind to a particular model.
        /// </summary>
        /// <param name="bindingContext">The binding context which has the object to be bound.</param>
        /// <returns>
        /// A Task which on completion returns a 
        /// Microsoft.AspNetCore.Mvc.ModelBinding.ModelBindingResult which represents
        /// the result of the model binding process.
        /// </returns>
        /// <remarks>
        /// A null return value means that this model binder was not able to handle the request.
        /// Returning null ensures that subsequent model binders are run. If a non null value
        /// indicates that the model binder was able to handle the request.
        /// </remarks>
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var values = new Dictionary<string, object>();
            var form  = bindingContext.HttpContext.Request.Form;
            foreach(var key in form.Keys)
            {
                var stringValues = form[key];
                if(stringValues == StringValues.Empty)
                {
                    continue;
                }

                foreach (var stringValue in stringValues)
                {
                    AddFormValue(values, key, stringValue);
                }
            }

            foreach(var file in form.Files)
            {
                // Asp.Net Core 1.0 and 1.1 use string as the parameter of TryParse method.
                // But Asp.Net Core 2.0 use StringSegment as the parameter. Use reflection to resolve version confict.
                var type = typeof(ContentDispositionHeaderValue);
                var methodInfo = type.GetMethod("TryParse");
                object[] args;
                ContentDispositionHeaderValue contentDisposition = null;
                var firstParam = methodInfo.GetParameters()[0];
                if (firstParam.ParameterType == typeof(StringSegment))
                {
                    args = new object[] { new StringSegment(file.ContentDisposition), contentDisposition };
                }
                else
                {
                    args = new object[] { file.ContentDisposition, contentDisposition };
                }

                var result = (bool)methodInfo.Invoke(null, args);
                contentDisposition = (ContentDispositionHeaderValue)args[1];
                if (!result)
                {
                    continue;
                }

                var fileNamePropertyInfo = type.GetProperty("FileName");
                var fileNamePropertyValue = fileNamePropertyInfo.GetValue(contentDisposition);
                var namePropertyInfo = type.GetProperty("Name");
                var namePropertyValue = namePropertyInfo.GetValue(contentDisposition);
                string name;
                string extension;
                if (!GetContentDispositionInfo(fileNamePropertyValue.ToString(), namePropertyValue.ToString(), out name, out extension))
                {
                    continue;
                }

                AddFormValue(values, name, new FormFile(CloneStream(file.OpenReadStream()), extension));
            }

            bindingContext.Result = ModelBindingResult.Success(CreateModel(values));
            return Task.FromResult(0);
        }

#endif

        private static Stream CloneStream(Stream origin)
        {
            using (origin)
            {
                var memoryStream = new MemoryStream();
                try
                {
                    origin.CopyTo(memoryStream);
                    return memoryStream;
                }
                catch (Exception)
                {
                    memoryStream.Dispose();
                    return null;
                }
            }
        }

        private static bool GetContentDispositionInfo(string fileName, string name, out string validName, out string extension)
        {
            validName = null;
            extension = null;

            fileName = (fileName ?? string.Empty).Trim('"');
            if (string.IsNullOrEmpty(fileName))
            {
                return false;
            }

            validName = (name ?? string.Empty).Trim('"');
            if (string.IsNullOrEmpty(fileName))
            {
                return false;
            }

            extension = Path.GetExtension(fileName);
            return true;
        }

        private static bool GetSerializedObj(object value, Type type, out object obj)
        {
            obj = null;
            var pTypeCode = Type.GetTypeCode(type);
            var strValue = value as string;
            switch (pTypeCode)
            {
                case TypeCode.Object:
                    if (typeof(Stream) == type && value is Stream)
                    {
                        obj = value;
                        return true;
                    }

                    if (!string.IsNullOrEmpty(strValue))
                    {
                        if (type == typeof(IEnumerable))
                        {
                            type = typeof(object);
                        }

                        obj = C1JsonHelper.Deserialize(strValue, type);
                        return true;
                    }

                    return false;
                case TypeCode.Empty:
                    return false;
                default:
                    if (type.IsEnum)
                    {
                        try
                        {
                            obj = Enum.Parse(type, strValue, true);
                            return true;
                        }
                        catch (ArgumentNullException)
                        {
                        }
                        catch (ArgumentException)
                        {
                        }
                        catch (OverflowException)
                        {
                        }
                        return false;
                    }

                    try
                    {
                        obj = Convert.ChangeType(value, type);
                        return true;
                    }
                    catch (InvalidCastException)
                    {
                    }
                    catch (FormatException)
                    {
                    }
                    catch (OverflowException)
                    {
                    }
                    catch (ArgumentNullException)
                    {
                    }
                    return false;
            }
        }

        private static T CreateModel(IEnumerable<KeyValuePair<string, object>> values)
        {
            var type = typeof(T);
            var model = Activator.CreateInstance<T>();
            var properties = type.GetProperties();
            foreach (var item in values)
            {
                var name = item.Key;
                var value = item.Value;
                //Only process properties for request model
                var property = properties.FirstOrDefault(f => string.Equals(f.Name, name, StringComparison.OrdinalIgnoreCase));
                if (property != null)
                {
                    var pType = property.PropertyType;
                    if (pType.IsArray)
                    {
                        property.SetValue(model, GetArray(value, pType.GetElementType()));
                        continue;
                    }

                    if (pType == typeof(FormFile))
                    {
                        property.SetValue(model, value as FormFile);
                        continue;
                    }

                    object obj;
                    if (GetSerializedObj(value, pType, out obj))
                    {
                        property.SetValue(model, obj);
                    }
                }
            }

            return model;
        }

        private static string FirstLetterToUpper(string str)
        {
            if (str == null)
                return null;

            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);

            return str.ToUpper();
        }

        private static void AddFormValue(IDictionary<string, object> values, string key, object value)
        {
            key = FirstLetterToUpper(key);
            object existValue;
            if (values.TryGetValue(key, out existValue))
            {
                var list = existValue as List<object> ?? new List<object> { existValue };
                list.Add(value);
                value = list;
            }

            values[key] = value;
        }

        private static Array GetArray(object value, Type elementType)
        {
            if (value == null)
            {
                return null;
            }

            if (value.GetType().IsArray)
            {
                return value as Array;
            }

            var list = value as IList<object> ?? new List<object> { value };
            var count = list.Count;
            var array = Array.CreateInstance(elementType, count);
            for (var i = 0; i < count; i++)
            {
                array.SetValue(list[i], i);
            }

            return array;
        }
    }
}