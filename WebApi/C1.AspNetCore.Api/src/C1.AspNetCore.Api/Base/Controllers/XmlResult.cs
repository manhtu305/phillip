﻿using System.IO;
#if !ASPNETCORE && !NETCORE
using ActionResult = System.Web.Http.IHttpActionResult;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using System;
using System.Net;
using System.Net.Http.Headers;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace C1.Web.Api
{
    internal class XmlResult : ActionResult
    {
        private const string ContentType = "text/xml";

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlResult"/> class.
        /// </summary>
        /// <param name="value">The object to serialize to XML.</param>
        public XmlResult(object value)
        {
            Value = value;
        }

        /// <summary>
        /// Gets the object to be serialized to XML.
        /// </summary>
        public object Value
        {
            get;
            private set;
        }

        private void Serialize(Stream stream)
        {
            C1XmlHelper.Serialize(Value, stream);
        }

#if !ASPNETCORE && !NETCORE
        /// <summary>
        /// Creates an <see cref="HttpResponseMessage"/> asynchronously.
        /// </summary>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        /// <returns>A task that, when completed, contains the <see cref="HttpResponseMessage"/>.</returns>
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Execute());
        }

        private HttpResponseMessage Execute()
        {
            // Simulate from JsonResult in Microsoft.AspNetCore.WebApi.Core.5.2.3\lib\net45\System.Web.Http.dll
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
                ArraySegment<byte> arraySegment;
                using (var memoryStream = new MemoryStream())
                {
                    Serialize(memoryStream);
                    arraySegment = new ArraySegment<byte>(memoryStream.GetBuffer(), 0, (int)memoryStream.Length);
                }

                httpResponseMessage.Content = new ByteArrayContent(arraySegment.Array, arraySegment.Offset, arraySegment.Count);
                httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue(ContentType);
            }
            catch
            {
                httpResponseMessage.Dispose();
                throw;
            }
            return httpResponseMessage;
        }

#else

        /// <summary>
        /// Serialises the object that was passed into the constructor to XML and writes the corresponding XML to the result stream.
        /// </summary>
        /// <param name="context">The controller context for the current request.</param>
        public override void ExecuteResult(ActionContext context)
        {
            context.HttpContext.Response.ContentType = ContentType;
            Serialize(context.HttpContext.Response.Body);
        }

#endif
    }
}