﻿#if !ASPNETCORE && !NETCORE
using System.Text;
using System.Web.Http.Results;
using Controller = System.Web.Http.ApiController;
using IActionResult = System.Web.Http.IHttpActionResult;
using System.Web.Http;
using System.Web.Http.Routing;
using System.Net.Http.Formatting;
using System.Net.Http;
using System.Net;
using RoutePrefixAttribute = System.Web.Http.RoutePrefixAttribute;
#else
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using RoutePrefixAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;
#endif
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace C1.Web.Api
{
    /// <summary>
    /// Defines the base class for the api controller.
    /// </summary>
    [StatusCodeExceptionFilter]
    public abstract class C1ApiController: Controller
    {
        private static bool IsNET45 = Type.GetType("System.Reflection.ReflectionContext", false) != null;
        private const string DEFAULT_FILE_NAME = "result";
#if !ASPNETCORE && !NETCORE
        private IEnumerable<MediaTypeFormatter> _formatters;
        /// <summary>
        /// Gets the media-type formatters for this instance.
        /// </summary>
        protected virtual IEnumerable<MediaTypeFormatter> Formatters
        {
            get
            {
                if (_formatters == null)
                {
                    _formatters = new List<MediaTypeFormatter>()
                    {
                        new JsonMediaTypeFormatter()
                        {
                            SerializerSettings = _defaultJsonSettings
                        }
                    };
                }
                return _formatters;
            }
        }
#else
        private FormatterCollection<IOutputFormatter> _formatters;
        /// <summary>
        /// Gets the media-type formatters for this instance.
        /// </summary>
        protected virtual FormatterCollection<IOutputFormatter> Formatters
        {
            get
            {
                if (_formatters == null)
                {
                    _formatters = new FormatterCollection<IOutputFormatter>(new List<IOutputFormatter>()
                    {
#if NETCORE3
                        new NewtonsoftJsonOutputFormatter(_defaultJsonSettings,System.Buffers.ArrayPool<char>.Shared, new MvcOptions())
#else
                        new JsonOutputFormatter(_defaultJsonSettings, System.Buffers.ArrayPool<char>.Shared)
#endif
                    });
                }
                return _formatters;
            }
        }
#endif
        private static readonly JsonSerializerSettings _defaultJsonSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            DefaultValueHandling = DefaultValueHandling.Ignore,
            NullValueHandling = NullValueHandling.Ignore,
            DateFormatString = "yyyy/MM/dd HH:mm:ss",
        };

        #region location

        /// <summary>
        /// Returns an absolute url for the specified action.
        /// </summary>
        /// <param name="expression">The expression which returns the function of calling the action method.</param>
        /// <param name="values">The dictionary that contains the parameters for the action.</param>
        /// <returns>The absolute url for the specified action.</returns>
        /// <example>
        /// Location(()=>this.Action);
        /// </example>
        protected string Location(Expression<Func<Func<IActionResult>>> expression, IDictionary<string, object> values = null)
        {
            return Location(expression.Body as UnaryExpression, values);
        }

        /// <summary>
        /// Returns an absolute url for the specified action.
        /// </summary>
        /// <param name="expression">The expression which returns the function of calling the action method.</param>
        /// <param name="values">The dictionary that contains the parameters for the action.</param>
        /// <returns>The absolute url for the specified action.</returns>
        /// <example>
        /// Location&lt;string&gt;(()=>this.Action);
        /// </example>
        protected string Location<T1>(Expression<Func<Func<T1, IActionResult>>> expression, IDictionary<string, object> values = null)
        {
            return Location(expression.Body as UnaryExpression, values);
        }

        /// <summary>
        /// Returns an absolute url for the specified action.
        /// </summary>
        /// <param name="expression">The expression which returns the function of calling the action method.</param>
        /// <param name="values">The dictionary that contains the parameters for the action.</param>
        /// <returns>The absolute url for the specified action.</returns>
        /// <example>
        /// Location&lt;string, string&gt;(()=>this.Action);
        /// </example>
        protected string Location<T1, T2>(Expression<Func<Func<T1, T2, IActionResult>>> expression, IDictionary<string, object> values = null)
        {
            return Location(expression.Body as UnaryExpression, values);
        }

        /// <summary>
        /// Returns an absolute url for the specified action.
        /// </summary>
        /// <param name="expression">The expression which returns the function of calling the action method.</param>
        /// <param name="values">The dictionary that contains the parameters for the action.</param>
        /// <returns>The absolute url for the specified action.</returns>
        /// <example>
        /// Location&lt;string, string, string&gt;(()=>this.Action);
        /// </example>
        protected string Location<T1, T2, T3>(Expression<Func<Func<T1, T2, T3, IActionResult>>> expression, IDictionary<string, object> values = null)
        {
            return Location(expression.Body as UnaryExpression, values);
        }

        /// <summary>
        /// Returns an absolute url for the specified action.
        /// </summary>
        /// <param name="expression">The expression which returns the function of calling the action method.</param>
        /// <param name="values">The dictionary that contains the parameters for the action.</param>
        /// <returns>The absolute url for the specified action.</returns>
        /// <example>
        /// Location&lt;string, string, string, string&gt;(()=>this.Action);
        /// </example>
        protected string Location<T1, T2, T3, T4>(Expression<Func<Func<T1, T2, T3, T4, IActionResult>>> expression, IDictionary<string, object> values = null)
        {
            return Location(expression.Body as UnaryExpression, values);
        }

        /// <summary>
        /// Returns an absolute url for the specified action.
        /// </summary>
        /// <param name="expression">The expression which returns the function of calling the action method.</param>
        /// <param name="values">The dictionary that contains the parameters for the action.</param>
        /// <returns>The absolute url for the specified action.</returns>
        /// <example>
        /// Location&lt;string, string, string, string, string&gt;(()=>this.Action);
        /// </example>
        protected string Location<T1, T2, T3, T4, T5>(Expression<Func<Func<T1, T2, T3, T4, T5, IActionResult>>> expression, IDictionary<string, object> values = null)
        {
            return Location(expression.Body as UnaryExpression, values);
        }

        private string Location(UnaryExpression unaryExpression, IDictionary<string, object> values = null)
        {
            // refer http://stackoverflow.com/questions/8225302/get-the-name-of-a-method-using-an-expression

            if (unaryExpression == null) return null;

            var methodCallExpression = unaryExpression.Operand as MethodCallExpression;
            if (methodCallExpression == null) return null;

            MethodInfo methodInfo;
            if (IsNET45)
            {
                var methodCallObject = (ConstantExpression)methodCallExpression.Object;
                methodInfo = methodCallObject.Value as MethodInfo;
            }
            else
            {
                var methodInfoExpression = (ConstantExpression)methodCallExpression.Arguments.Last();
                methodInfo = methodInfoExpression.Value as MethodInfo;
            }

            if (methodInfo == null) return null;

            return Location(methodInfo, values);
        }

        /// <summary>
        /// Returns an absolute url for the specified action.
        /// </summary>
        /// <param name="actionName">The name of the action.</param>
        /// <param name="values">The dictionary that contains the parameters for the action.</param>
        /// <returns>The absolute url for the specified action.</returns>
        protected string Location(string actionName, IDictionary<string, object> values = null)
        {
            var flags = BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;
            var actionMethod = GetType().GetMethod(actionName, flags);
            if (actionMethod == null)
            {
                throw new ArgumentException(string.Format(Localization.Resources.MissingMethodInClass, actionName, GetType().FullName));
            }

            return Location(actionMethod, values);
        }

        /// <summary>
        /// Returns an absolute url for the specified action.
        /// </summary>
        /// <param name="methodInfo">The method for the action.</param>
        /// <param name="values">The dictionary that contains the parameters for the action.</param>
        /// <returns>The absolute url for the specified action.</returns>
        protected string Location(MethodInfo methodInfo, IDictionary<string, object> values = null)
        {
            var routePrefix = methodInfo.DeclaringType.GetCustomAttributes(typeof(RoutePrefixAttribute), true).FirstOrDefault() as RoutePrefixAttribute;
            if (routePrefix == null)
            {
                throw new NotSupportedException(string.Format(Localization.Resources.MissingRoutePrefix, methodInfo.DeclaringType.FullName));
            }

            var pathRoute = methodInfo.GetCustomAttributes(typeof(PathRouteAttribute), true).FirstOrDefault() as PathRouteAttribute;
            if (pathRoute == null)
            {
                throw new NotSupportedException(string.Format(Localization.Resources.MissingPathRoute, methodInfo.Name));
            }

#if !ASPNETCORE && !NETCORE
            var routeData = RequestContext.RouteData;
            var prefix = routePrefix.Prefix;
#else
            var routeData = RouteData;
            var prefix = routePrefix.Template;
#endif
            var resolver = new TemplateDataResolver(values, routeData, Request);
            var path = PathRouteHelper.ResolveTemplate(pathRoute.Template, resolver);

            // for non-core, the content(~) returns full absolute path, starting with the scheme
            var location = Url.Content(string.Format("~/{0}/{1}", prefix, path));
#if ASPNETCORE || NETCORE
            // for core, the content(~) returns the absolute path, including the virtual path (PathBase),
            // but does not include the scheme and host.
            location = string.Format("{0}://{1}{2}", Request.Scheme, Request.Host, location);
#endif
            return location;
        }

        #endregion

        #region process action

        /// <summary>
        /// Process the action by catching specific excepitions.
        /// </summary>
        /// <remarks>
        /// The following exceptions will be handled:
        /// <list type="bullet">
        /// <item>
        /// <term><see cref="NotFoundException"/></term>
        /// <description>Returns 404.</description>
        /// </item>
        /// <item>
        /// <term><see cref="NotAcceptableException"/></term>
        /// <description>Returns 406.</description>
        /// </item>
        /// </list>
        /// </remarks>
        /// <param name="func">The function to process the action.</param>
        /// <returns>The action result.</returns>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal IActionResult ProcessAction(Func<IActionResult> func)
        {
            try
            {
                return func();
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (NotAcceptableException)
            {
                return NotAcceptable();
            }
        }

#endregion

        /// <summary>
        /// Set Json formatter serializer settings.
        /// </summary>
        /// <param name="jsonSettings">The serializer settings of Json formatter</param>
        protected void SetJsonSettings(JsonSerializerSettings jsonSettings)
        {
            foreach (var formatter in Formatters)
            {
                var jsonFormatter = formatter as
#if !ASPNETCORE && !NETCORE
                    JsonMediaTypeFormatter;
#elif NETCORE3
                    NewtonsoftJsonOutputFormatter;
#else
                    JsonOutputFormatter;
#endif
                if (jsonFormatter != null)
                {
#if !ASPNETCORE && !NETCORE
                    jsonFormatter.SerializerSettings = jsonSettings;
#elif NETCORE3
                    Formatters.Remove(jsonFormatter);
                    Formatters.Add(new NewtonsoftJsonOutputFormatter(jsonSettings, System.Buffers.ArrayPool<char>.Shared, new MvcOptions()));
#else
                    Formatters.Remove(jsonFormatter);
                    Formatters.Add(new JsonOutputFormatter(jsonSettings, System.Buffers.ArrayPool<char>.Shared));
#endif
                    return;
                }
            }
        }

#if !ASPNETCORE && !NETCORE
        //For json serialization.
        private static readonly Encoding _jsonSerializerEncoding = new UTF8Encoding(false, true);

        /// <summary>
        /// Creates a <see cref="T:System.Web.Http.NegotiatedContentResult`1" /> with the specified values.
        /// </summary>
        /// <typeparam name="T">The type of content in the entity body.</typeparam>
        /// <param name="statusCode">The HTTP status code for the response message.</param>
        /// <param name="value">The content value to negotiate and format in the entity body.</param>
        /// <returns>A <see cref="T:System.Web.Http.NegotiatedContentResult`1" /> with the specified values.</returns>
        protected override NegotiatedContentResult<T> Content<T>(HttpStatusCode statusCode, T value)
        {
            var conneg = ControllerContext.Configuration.Services.GetContentNegotiator();
            return new NegotiatedContentResult<T>(statusCode, value, conneg, Request, Formatters);
        }

        /// <summary>
        /// Creates a System.Web.Http.Results.CreatedNegotiatedContentResult`1 (201 Created) with the specified values.
        /// </summary>
        /// <typeparam name="T">The type of content in the entity body.</typeparam>
        /// <param name="location">The location at which the content has been created.</param>
        /// <param name="content">The content value to negotiate and format in the entity body.</param>
        /// <returns>A System.Web.Http.Results.CreatedNegotiatedContentResult`1 with the specified values.</returns>
        protected override CreatedNegotiatedContentResult<T> Created<T>(Uri location, T content)
        {
            var conneg = ControllerContext.Configuration.Services.GetContentNegotiator();
            return new CreatedNegotiatedContentResult<T>(location, content, conneg, Request, Formatters);
        }

        /// <summary>
        /// Creates a System.Web.Http.Results.CreatedAtRouteNegotiatedContentResult`1 (201 Created) with the specified values.
        /// </summary>
        /// <typeparam name="T">The type of content in the entity body.</typeparam>
        /// <param name="routeName">The name of the route to use for generating the URL.</param>
        /// <param name="routeValues">The route data to use for generating the URL.</param>
        /// <param name="content">The content value to negotiate and format in the entity body.</param>
        /// <returns>A System.Web.Http.Results.CreatedAtRouteNegotiatedContentResult`1 with the specified values.</returns>
        protected override CreatedAtRouteNegotiatedContentResult<T> CreatedAtRoute<T>(string routeName, IDictionary<string, object> routeValues, T content)
        {
            var urlFactory = Url ?? new UrlHelper(Request);
            var conneg = ControllerContext.Configuration.Services.GetContentNegotiator();
            return new CreatedAtRouteNegotiatedContentResult<T>(routeName, routeValues, content, urlFactory, conneg, Request, Formatters);
        }

        /// <summary>
        /// Creates a System.Web.Http.Results.JsonResult`1 (200 OK) with the specified values.
        /// </summary>
        /// <typeparam name="T">The type of content in the entity body.</typeparam>
        /// <param name="content">The content value to serialize in the entity body.</param>
        /// <param name="serializerSettings">The serializer settings.</param>
        /// <param name="encoding">The content encoding.</param>
        /// <returns>A System.Web.Http.Results.JsonResult`1 with the specified values.</returns>
        protected override JsonResult<T> Json<T>(T content, JsonSerializerSettings serializerSettings, Encoding encoding)
        {
            JsonSerializerSettings settings = null;
            foreach (var formatter in Formatters)
            {
                var jsonFormatter = formatter as JsonMediaTypeFormatter;
                if (jsonFormatter != null)
                {
                    settings = jsonFormatter.SerializerSettings;
                }
            }

            return new JsonResult<T>(content, settings ?? new JsonSerializerSettings(), _jsonSerializerEncoding, this);
        }

        /// <summary>
        /// Creates an System.Web.Http.Results.OkNegotiatedContentResult`1 with the specified values.
        /// </summary>
        /// <typeparam name="T">The type of content in the entity body.</typeparam>
        /// <param name="content">The content value to negotiate and format in the entity body.</param>
        /// <returns>An System.Web.Http.Results.OkNegotiatedContentResult`1 with the specified values.</returns>
        protected override OkNegotiatedContentResult<T> Ok<T>(T content)
        {
            var conneg = ControllerContext.Configuration.Services.GetContentNegotiator();
            return new OkNegotiatedContentResult<T>(content, conneg, Request, Formatters);
        }

        /// <summary>
        /// Creates an <see cref="IActionResult"/> result that produces a NoContent (204) response.
        /// </summary>
        /// <returns>A NoContent result for the response.</returns>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal IActionResult NoContent()
        {
            return StatusCode(System.Net.HttpStatusCode.NoContent);
        }

#else
        /// <summary>
        /// Creates an Microsoft.AspNetCore.Mvc.BadRequestObjectResult that produces a Bad
        /// Request (400) response.
        /// </summary>
        /// <returns>The created Microsoft.AspNetCore.Mvc.BadRequestObjectResult for the response.</returns>
        public override BadRequestObjectResult BadRequest(ModelStateDictionary modelState)
        {
            var result = base.BadRequest(modelState);
            result.Formatters = Formatters;
            return result;
        }

        /// <summary>
        /// Creates an Microsoft.AspNetCore.Mvc.BadRequestObjectResult that produces a Bad
        /// Request (400) response.
        /// </summary>
        /// <returns>The created Microsoft.AspNetCore.Mvc.BadRequestObjectResult for the response.</returns>
        public override BadRequestObjectResult BadRequest(object error)
        {
            var result = base.BadRequest(error);
            result.Formatters = Formatters;
            return result;
        }

        /// <summary>
        /// Creates a Microsoft.AspNetCore.Mvc.CreatedResult object that produces a Created
        /// (201) response.
        /// </summary>
        /// <param name="uri">The URI at which the content has been created.</param>
        /// <param name="value">The content value to format in the entity body.</param>
        /// <returns>The created Microsoft.AspNetCore.Mvc.CreatedResult for the response.</returns>
        public override CreatedResult Created(string uri, object value)
        {
            var result= base.Created(uri, value);
            result.Formatters = Formatters;
            return result;
        }

        /// <summary>
        /// Creates a Microsoft.AspNetCore.Mvc.CreatedResult object that produces a Created
        /// (201) response.
        /// </summary>
        /// <param name="uri">The URI at which the content has been created.</param>
        /// <param name="value">The content value to format in the entity body.</param>
        /// <returns>The created Microsoft.AspNetCore.Mvc.CreatedResult for the response.</returns>
        public override CreatedResult Created(Uri uri, object value)
        {
            var result = base.Created(uri, value);
            result.Formatters = Formatters;
            return result;
        }

        /// <summary>
        /// Creates a Microsoft.AspNetCore.Mvc.CreatedAtActionResult object that produces
        /// a Created (201) response.
        /// </summary>
        /// <param name="actionName">The name of the action to use for generating the URL.</param>
        /// <param name="controllerName">The name of the controller to use for generating the URL.</param>
        /// <param name="routeValues">The route data to use for generating the URL.</param>
        /// <param name="value">The content value to format in the entity body.</param>
        /// <returns>The created Microsoft.AspNetCore.Mvc.CreatedAtActionResult for the response.</returns>
        public override CreatedAtActionResult CreatedAtAction(string actionName, string controllerName, object routeValues, object value)
        {
            var result = base.CreatedAtAction(actionName, controllerName, routeValues, value);
            result.Formatters = Formatters;
            return result;
        }

        /// <summary>
        /// Creates a Microsoft.AspNetCore.Mvc.CreatedAtRouteResult object that produces
        /// a Created (201) response.
        /// </summary>
        /// <param name="routeName">The name of the route to use for generating the URL.</param>
        /// <param name="routeValues">The route data to use for generating the URL.</param>
        /// <param name="value">The content value to format in the entity body.</param>
        /// <returns>The created Microsoft.AspNetCore.Mvc.CreatedAtRouteResult for the response.</returns>
        public override CreatedAtRouteResult CreatedAtRoute(string routeName, object routeValues, object value)
        {
            var result = base.CreatedAtRoute(routeName, routeValues, value);
            result.Formatters = Formatters;
            return result;
        }

        /// <summary>
        /// Creates an Microsoft.AspNetCore.Mvc.NotFoundObjectResult that produces a Not
        /// Found (404) response.
        /// </summary>
        /// <returns>The created Microsoft.AspNetCore.Mvc.NotFoundObjectResult for the response.</returns>
        public override NotFoundObjectResult NotFound(object value)
        {
            var result = base.NotFound(value);
            result.Formatters = Formatters;
            return result;
        }

        /// <summary>
        /// Creates an Microsoft.AspNetCore.Mvc.OkObjectResult object that produces an OK
        /// (200) response.
        /// </summary>
        /// <param name="value">The content value to format in the entity body.</param>
        /// <returns>The created Microsoft.AspNetCore.Mvc.OkObjectResult for the response.</returns>
        public override OkObjectResult Ok(object value)
        {
            var result = base.Ok(value);
            result.Formatters = Formatters;
            return result;
        }

        /// <summary>
        /// Creates a Microsoft.AspNetCore.Mvc.ObjectResult object by specifying a statusCode
        /// and value
        /// </summary>
        /// <param name="statusCode">The status code to set on the response.</param>
        /// <param name="value">The value to set on the Microsoft.AspNetCore.Mvc.ObjectResult.</param>
        /// <returns>The created Microsoft.AspNetCore.Mvc.ObjectResult object for the response.</returns>
        public override ObjectResult StatusCode(int statusCode, object value)
        {
            var result = base.StatusCode(statusCode, value);
            result.Formatters = Formatters;
            return result;
        }

#region 500
        /// <summary>
        /// Creates an <see cref="IActionResult"/> result that produces a NotFind (500) response.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>A NotFind result for the response.</returns>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal virtual IActionResult InternalServerError(Exception exception)
        {
            return StatusCode(500, exception);
        }

        /// <summary>
        /// Creates an <see cref="IActionResult"/> result that produces a NotFind (500) response.
        /// </summary>
        /// <returns>A NotFind result for the response.</returns>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal virtual IActionResult InternalServerError()
        {
            return StatusCode(500);
        }
#endregion 500
#endif

#region 200
#region File
        /// <summary>
        /// Returns a file in the specified fileStream with the file name and the file extension.
        /// </summary>
        /// <param name="streamGetter">The function to get a <see cref="Stream"/> with the contents of the file.</param>
        /// <param name="fileName">The file name.</param>
        /// <param name="fileExtension">The file extension.</param>
        /// <returns>A file result for the response.</returns>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal virtual IActionResult File(Func<Stream> streamGetter, string fileName, string fileExtension)
        {
            return File(streamGetter, string.Format("{0}.{1}", fileName ?? DEFAULT_FILE_NAME, fileExtension));
        }

        /// <summary>
        /// Returns a file in the specified fileStream with the full file name.
        /// </summary>
        /// <param name="streamGetter">The function to get a <see cref="Stream"/> with the contents of the file.</param>
        /// <param name="fullFileName">The file full name.</param>
        /// <returns>A file result for the response.</returns>
        [SmartAssembly.Attributes.DoNotObfuscate]
        protected virtual IActionResult File(Func<Stream> streamGetter, string fullFileName)
        {
            return new FileResult(streamGetter, fullFileName);
        }
#endregion File
#endregion 200

#region 201
        internal IActionResult Created(object entity, string entityIdName = "Id")
        {
            var pId = entity.GetType().GetProperty(entityIdName);
            if (pId != null)
            {
                var id = pId.GetValue(entity).ToString();
                return Created(GetEntityUrl(id, this), entity);
            }
            throw new NotSupportedException();
        }

        private static string GetEntityUrl(string entityId, Controller controller)
        {
#if ASPNETCORE || NETCORE
            var context = controller.HttpContext;
            return string.Format("{0}://{1}{2}{3}", context.Request.Scheme, context.Request.Host, context.Request.Path, entityId);
#else
            return string.Format("{0}/{1}", controller.Request.RequestUri.AbsolutePath, entityId);
#endif
        }
#endregion 201

#region 202

        /// <summary>
        /// Creates an <see cref="IActionResult"/> result that produces an Accepted (202) response.
        /// </summary>
        /// <param name="statusLocation">Specifies the status location in the result.</param>
        /// <returns>The Accepted result for the response.</returns>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal virtual IActionResult Accept(string statusLocation)
        {
#if !ASPNETCORE && !NETCORE
            var response = new HttpResponseMessage();
            response.StatusCode = System.Net.HttpStatusCode.Accepted;
#else
            var response = Response;
#endif
            response.Headers.Add("StatusLocation", System.Uri.EscapeUriString(statusLocation));
#if !ASPNETCORE && !NETCORE
            return ResponseMessage(response);
#else
            return new StatusCodeResult(202);
#endif
        }
#endregion 202

#region 406
        /// <summary>
        /// Creates an <see cref="IActionResult"/> result that produces a NotAcceptable (406) response.
        /// </summary>
        /// <returns>A NotAcceptable result for the response.</returns>
        [SmartAssembly.Attributes.DoNotObfuscate]
        internal IActionResult NotAcceptable()
        {
#if !ASPNETCORE && !NETCORE
            return StatusCode(HttpStatusCode.NotAcceptable);
#else
            return StatusCode(406);
#endif
        }
#endregion 406
    }
}
