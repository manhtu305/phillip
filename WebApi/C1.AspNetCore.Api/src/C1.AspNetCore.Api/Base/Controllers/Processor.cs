﻿using System.IO;
#if !ASPNETCORE && !NETCORE
using IActionResult = System.Web.Http.IHttpActionResult;
using Controller = System.Web.Http.ApiController;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace C1.Web.Api
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal abstract class Processor
    {
        private const string DEFAULT_FILE_NAME = "result";

        protected abstract bool IsRequestObjectResult { get; }

        public IActionResult GetResult(Controller controller)
        {
            return IsRequestObjectResult ? GetObjectResult(controller) : GetFileResult();
        }

        protected virtual IActionResult GetFileResult()
        {
            return new FileResult(GetFileStream, FullFileName);
        }

        protected abstract Stream GetFileStream();

        protected virtual string FullFileName
        {
            get
            {
                return string.Format("{0}.{1}", FileName, FileExtension);
            }
        }

        protected virtual string FileName
        {
            get
            {
                return DEFAULT_FILE_NAME;
            }
        }

        protected abstract string FileExtension { get; }

        protected virtual IObjectFormatter ObjectFormatter
        {
            get
            {
                return JsonFormatter.Instance;
            }
        }

        protected virtual IActionResult GetObjectResult(Controller controller)
        {
            return ObjectFormatter.Format(GetObject(), controller);
        }

        protected abstract object GetObject();
    }
}
