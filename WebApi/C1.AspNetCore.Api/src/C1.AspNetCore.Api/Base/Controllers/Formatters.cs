﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
#if !ASPNETCORE && !NETCORE
using IActionResult = System.Web.Http.IHttpActionResult;
using System.Web.Http.Results;
using Controller = System.Web.Http.ApiController;
using System.Text;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace C1.Web.Api
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal interface IObjectFormatter
    {
        IActionResult Format(object model, Controller controller);
    }

    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal class XmlFormatter : IObjectFormatter
    {
        public readonly static XmlFormatter Instance = new XmlFormatter();

        public IActionResult Format(object model, Controller controller)
        {
            return Xml(model);
        }

        public static XmlResult Xml(object obj)
        {
            return new XmlResult(obj);
        }
    }

    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal class JsonFormatter : IObjectFormatter
    {

        private readonly static JsonSerializerSettings _defaultJsonSettings;

        public readonly static JsonFormatter Instance;

        public readonly static JsonFormatter IncludeDefaultValueInstance;

        private readonly JsonSerializerSettings _jsonSettings;

        static JsonFormatter()
        {
            _defaultJsonSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                DefaultValueHandling = DefaultValueHandling.Ignore,
                NullValueHandling = NullValueHandling.Ignore,
                DateFormatString = "yyyy/MM/dd HH:mm:ss",
            };

            Instance = new JsonFormatter(_defaultJsonSettings);

            IncludeDefaultValueInstance = new JsonFormatter(new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                DefaultValueHandling = DefaultValueHandling.Include,
                NullValueHandling = NullValueHandling.Ignore,
                DateFormatString = "yyyy/MM/dd HH:mm:ss",
            });
        }

        public JsonFormatter(JsonSerializerSettings jsonSettings = null)
        {
            _jsonSettings = jsonSettings ?? _defaultJsonSettings;
        }

        public IActionResult Format(object model, Controller controller)
        {
            return Json(model, _jsonSettings, controller);
        }

        public static IActionResult Json(object obj, Controller controller)
        {
            return Json(obj, _defaultJsonSettings, controller);
        }

        public static IActionResult Json(object obj, JsonSerializerSettings jsonSettings, Controller controller)
        {
#if !ASPNETCORE && !NETCORE
            return new JsonResult<object>(obj, jsonSettings, new UTF8Encoding(false, true), controller);
#else
            return new JsonResult(obj, jsonSettings);
#endif
        }

#if !ASPNETCORE && !NETCORE
        public static IActionResult Json<T>(T obj, Controller controller)
        {
            return Json<T>(obj, _defaultJsonSettings, controller);
        }

        public static IActionResult Json<T>(T obj, JsonSerializerSettings jsonSettings, Controller controller)
        {
            return new JsonResult<T>(obj, jsonSettings, new UTF8Encoding(false, true), controller);
        }
#endif
    }
}