﻿using System.IO;
using System.Threading.Tasks;
using System;
#if !ASPNETCORE && !NETCORE
using IActionResult = System.Web.Http.IHttpActionResult;
using System.Threading;
using System.Net.Http;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace C1.Web.Api
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal class FileResult : IActionResult
    {
        private readonly Func<Stream> _streamGetter;
        private readonly string _fileName;

        public FileResult(Func<Stream> streamGetter, string fileName)
        {
            _streamGetter = streamGetter;
            _fileName = fileName;
        }

#if UNITTEST
        internal Func<Stream> StreamGetter
        {
            get
            {
                return _streamGetter;
            }
        }
#endif

#if !ASPNETCORE && !NETCORE
        /// <summary>
        /// Creates an <see cref="HttpResponseMessage"/> asynchronously.
        /// </summary>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        /// <returns>A task that, when completed, contains the <see cref="HttpResponseMessage"/>.</returns>
        public async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
#else
        public async Task ExecuteResultAsync(ActionContext context)
#endif
        {
            var task = Task.Run(() =>
            {
                var outputStream = _streamGetter();
                if (outputStream.CanSeek)
                {
                    outputStream.Position = 0;
                }

                string contentType;
                var fileExtension = Path.GetExtension(_fileName).Remove(0, 1);//remove '.'
#if !ASPNETCORE && !NETCORE
                var response = new HttpResponseMessage
                {
                    Content = new AttachmentContent(outputStream, _fileName)
                };

                if (ExportFileTypeExtensions.TryGetMediaType(fileExtension, out contentType))
                {
                    response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(contentType);
                }

                return response;
            });
            return await task;
#else
                context.HttpContext.Response.Headers["Content-Disposition"] = $"attachment; filename=\"{System.Uri.EscapeUriString(_fileName)}\"";
                if (ExportFileTypeExtensions.TryGetMediaType(fileExtension, out contentType))
                {
                    context.HttpContext.Response.ContentType = contentType;
                }
                outputStream.CopyTo(context.HttpContext.Response.Body);
            });
            await task;
#endif
        }
    }
}