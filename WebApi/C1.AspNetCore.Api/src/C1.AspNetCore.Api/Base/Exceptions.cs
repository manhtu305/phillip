﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace C1.Web.Api
{
    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class NotFoundException : Exception
    {
        public NotFoundException()
        {
        }

        public NotFoundException(string message) : base(message)
        {
        }
    }

    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class NotAcceptableException : Exception
    {
        public NotAcceptableException()
        {
        }

        public NotAcceptableException(string message) : base(message)
        {
        }
    }

    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class StatusCodeException : Exception
    {
        public StatusCodeException(int statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
        }

        public int StatusCode { get; private set; }
    }
}
