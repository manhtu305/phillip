﻿namespace C1.Web.Api
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal enum HttpMethod
    {
        Get,
        Head,
        Post,
        Put,
        Delete,
        Connect,
        Options,
        Trace,
        Patch
    }
}
