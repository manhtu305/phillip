﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
#if ASPNETCORE || NETCORE
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
#else
using System.Net.Http;
using HttpRequest = System.Net.Http.HttpRequestMessage;
#endif

namespace C1.Web.Api
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal static class RequestParamsExtensions
    {
        public static NameValueCollection Create()
        {
            return new NameValueCollection(StringComparer.OrdinalIgnoreCase);
        }

        public static NameValueCollection GetParams(this HttpRequest request)
        {
            var requestParams = Create();
            if (request == null)
            {
                return requestParams;
            }

#if ASPNETCORE || NETCORE
            if (request.HasFormContentType && request.Form != null)
            {
                requestParams.Add(request.Form);
            }

            if (request.Query != null)
            {
                requestParams.Add(request.Query);
            }
#else
            requestParams.Add(request.GetQueryNameValuePairs());
            if (request.Content != null && request.Content.IsFormData())
            {
                var task = request.Content.ReadAsFormDataAsync();
                task.Wait();
                if (task.Result != null) requestParams.Add(task.Result);
            }
#endif

            return requestParams;
        }

#if ASPNETCORE || NETCORE
        public static void Add(this NameValueCollection source, IEnumerable<KeyValuePair<string, StringValues>> other)
        {
            Add(source, other.Select(i => new KeyValuePair<string, IEnumerable<string>>(i.Key, i.Value)));
        }
#endif

        public static void Add(this NameValueCollection source, IEnumerable<KeyValuePair<string, string[]>> other)
        {
            Add(source, other.Select(i => new KeyValuePair<string, IEnumerable<string>>(i.Key, i.Value)));
        }

        public static void Add(this NameValueCollection source, IEnumerable<KeyValuePair<string, string>> other)
        {
            Add(source, other.Select(i => new KeyValuePair<string, IEnumerable<string>>(i.Key, new string[] { i.Value })));
        }

        public static void Add(this NameValueCollection source, IEnumerable<KeyValuePair<string, IEnumerable<string>>> other)
        {
            other.ToList().ForEach(item =>
            {
                var values = item.Value == null ? new string[] { } : item.Value.ToArray();
                if (!values.Any())
                {
                    source.Add(item.Key, null);
                }

                foreach (var value in values)
                {
                    source.Add(item.Key, value);
                }
            });
        }
    }
}
