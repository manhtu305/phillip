﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
#if !ASPNETCORE && !NETCORE
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
#else
using Microsoft.AspNetCore.Mvc.ModelBinding;
#endif

namespace C1.Web.Api
{
    /// <summary>
    /// Model binder for return raw string value.
    /// </summary>
    /// <remarks>
    /// For the default model binder, the empty string will be converted to null.
    /// This model binder will keep the raw string value, including empty string.
    /// </remarks>
    public class StringModelBinder : IModelBinder
    {

#if !ASPNETCORE && !NETCORE
        /// <summary>
        /// Binds the model to a value by using the specified controller context and binding context.
        /// </summary>
        /// <param name="actionContext">The action context.</param>
        /// <param name="bindingContext">The binding context.</param>
        /// <returns>true if model binding is successful; otherwise, false.</returns>
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            if(bindingContext.ModelType != typeof(string))
            {
                return false;
            }

            string val = bindingContext.ValueProvider.GetValue(bindingContext.ModelName).AttemptedValue;
            bindingContext.Model = val;
            return true;
        }
#else
        /// <summary>
        /// Binds the model to a value by using the binding context.
        /// </summary>
        /// <param name="bindingContext">The binding context.</param>
        /// <returns>The binding result.</returns>
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType == typeof(string))
            {
                var val = bindingContext.ValueProvider.GetValue(bindingContext.ModelName).FirstValue;
                bindingContext.Result = ModelBindingResult.Success(val);
            }

            return Task.FromResult(0);
        }
#endif
    }
}
