﻿using System.IO;
using System.Threading.Tasks;
#if !ASPNETCORE && !NETCORE
using System.Threading;
using System.Net.Http;
using System.Web.Http;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace C1.Web.Api
{
#if !ASPNETCORE && !NETCORE
    /// <summary>
    /// The IHttpActionResult for exporting.
    /// </summary>
    public class ExporterResult : IHttpActionResult
#else
    /// <summary>
    /// The IActionResult for exporting.
    /// </summary>
    public class ExporterResult : IActionResult
#endif
    {
        private const string DefaultFileName = "result";

        /// <summary>
        /// Gets the exporting model.
        /// </summary>
        public ExportSource ExportSource { get; private set; }

        /// <summary>
        /// Creates the ExporterResult instance.
        /// </summary>
        /// <param name="source">The exporting model.</param>
        public ExporterResult(ExportSource source)
        {
            ExportSource = source;
        }

#if !ASPNETCORE && !NETCORE
        /// <summary>
        /// Creates an <see cref="HttpResponseMessage"/> asynchronously.
        /// </summary>
        /// <param name="cancellationToken">The token to monitor for cancellation requests.</param>
        /// <returns>A task that, when completed, contains the <see cref="HttpResponseMessage"/>.</returns>
        public async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
#else
        public async Task ExecuteResultAsync(ActionContext context)
#endif
        {
            var outputStream = new MemoryStream(); // do NOT dispose.
            await ExportCoreAsync(outputStream);

            var fileName = (string.IsNullOrWhiteSpace(ExportSource.FileName) ? DefaultFileName : ExportSource.FileName.Trim())
                + ExportSource.Type.ToFileExtension();
            if (outputStream.CanSeek)
            {
                outputStream.Position = 0;
            }

#if !ASPNETCORE && !NETCORE
            return new HttpResponseMessage
            {
                Content = new AttachmentContent(outputStream, fileName)
            };
#else
            context.HttpContext.Response.Headers["Content-Disposition"] = $"attachment; filename={System.Uri.EscapeUriString(fileName)}";
            outputStream.CopyTo(context.HttpContext.Response.Body);
#endif
        }

        internal virtual async Task ExportCoreAsync(Stream outputStream)
        {
            var exporterSource = ExportSource as IExporterSource;
            if (exporterSource != null)
            {
                var exporter = exporterSource.CreateExporter();
                await exporter.ExportAsync(ExportSource, outputStream);
            }
        }
    }
}