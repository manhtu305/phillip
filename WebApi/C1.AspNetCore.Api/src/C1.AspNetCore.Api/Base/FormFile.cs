﻿using System.IO;

namespace C1.Web.Api
{
    /// <summary>
    /// This class defines the file from form post.
    /// </summary>
    public class FormFile
    {

        private readonly Stream _stream;

        /// <summary>
        /// Create a FormFile with specified stream and file extension.
        /// </summary>
        /// <param name="stream">The file stream</param>
        /// <param name="extension">The file extension</param>
        public FormFile(Stream stream, string extension)
        {
            _stream = stream;
            Extension = extension;
        }

        /// <summary>
        /// Gets the file stream.
        /// </summary>
        /// <returns>The file stream</returns>
        public Stream GetStream()
        {
            return _stream;
        }

        /// <summary>
        /// Gets the file extension.
        /// </summary>
        public string Extension
        {
            get;
            private set;
        }
    }
}
