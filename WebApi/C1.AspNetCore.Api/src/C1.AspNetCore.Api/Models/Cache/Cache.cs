﻿using System;

namespace C1.Web.Api
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal interface ICache
    {
        DateTime CreatedTime { get; }
        DateTime ExpiredTime { get; }
    }

    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal class Cache<T> : ICache, IDisposable where T: class
    {
        private const int _defaultDurationSeconds = 600;
        public T Content { get; private set; }
        public bool IsDisposed { get; private set; }
        public string Id { get; private set; }
        public DateTime CreatedTime { get; private set; }
        public DateTime LastActivedTime { get; private set; }
        public DateTime ExpiredTime { get; private set; }
        public TimeSpan Duration { get; private set; }
        public bool IsExpired
        {
            get
            {
                return ExpiredTime <= DateTime.Now;
            }
        }

        public Cache(string id, T content, int? durationSeconds = null)
            : this(id, content, durationSeconds.HasValue ? new TimeSpan(0, 0, durationSeconds.Value) : new TimeSpan(0, 0, _defaultDurationSeconds))
        {
        }

        public Cache(string id, T content, TimeSpan duration)
        {
            Id = id;
            Content = content;
            Duration = duration;
            CreatedTime = DateTime.Now;
            Active();
        }

        public void Active()
        {
            LastActivedTime = DateTime.Now;
            ExpiredTime = LastActivedTime + Duration;
        }

        public void Dispose()
        {
            var disposable = Content as IDisposable;
            if(disposable != null)
            {
                disposable.Dispose();
            }

            Content = null;
            IsDisposed = true;
        }
    }
}
