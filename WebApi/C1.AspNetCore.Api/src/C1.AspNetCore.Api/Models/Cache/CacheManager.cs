﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;

namespace C1.Web.Api
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal class CacheManager<T> where T : class
    {
        private static TimeSpan _defaultCacheDuration = new TimeSpan(0, 10, 0);
        private static int _processInterval = 1000;
        private IDictionary<string, Cache<T>> _caches = new Dictionary<string, Cache<T>>(StringComparer.OrdinalIgnoreCase);
        private readonly Thread _cacheProcessThread;
        private readonly CacheIntervalTask _cacheIntervalTask;
        public readonly object SyncObject = new object();

        public CacheManager()
        {
            CacheDuration = _defaultCacheDuration;
            _cacheIntervalTask = CreateCacheIntervalTask();
            _cacheProcessThread = new Thread(_cacheIntervalTask.Run);
        }

        protected virtual CacheIntervalTask CreateCacheIntervalTask()
        {
            return new CacheIntervalTask(this);
        }

        public void Refresh()
        {
            _cacheIntervalTask.Process();
        }

        public IDictionary<string, Cache<T>> Caches
        {
            get
            {
                return _caches;
            }
        }

        public TimeSpan CacheDuration
        {
            get;
            set;
        }

        protected Cache<T> InnerCreate(T content)
        {
            if (_cacheProcessThread.ThreadState != ThreadState.Running)
            {
                try
                {
                    _cacheProcessThread.Start();
                }
                catch { }
            }

            var id = GenerateId(content);
            var cache = new Cache<T>(id, content, _defaultCacheDuration);
            Caches.Add(id, cache);
            return cache;
        }

        protected virtual string GenerateId(T content)
        {
            return Guid.NewGuid().ToString();
        }

        public Cache<T> Create(T content)
        {
            lock (SyncObject)
            {
                return InnerCreate(content);
            }
        }

        public Cache<T> Get(string id)
        {
            Refresh();
            lock (SyncObject)
            {
                var cache = Caches[id];
                cache.Active();
                return cache;
            }
        }

        public IEnumerable<Cache<T>> Get(Func<string, Cache<T>, bool> selector)
        {
            Refresh();
            lock (SyncObject)
            {
                return Caches.Where(p => selector(p.Key, p.Value)).Select(p => p.Value);
            }
        }

        protected bool InnerTryGet(string id, out Cache<T> cache)
        {
            if (Caches.TryGetValue(id, out cache))
            {
                cache.Active();
                return true;
            }

            cache = null;
            return false;
        }

        public bool TryGet(string id, out Cache<T> cache)
        {
            Refresh();
            lock (SyncObject)
            {
                return InnerTryGet(id, out cache);
            }
        }

        public bool Contains(string id)
        {
            Refresh();
            lock (SyncObject)
            {
                return Caches.ContainsKey(id);
            }
        }

        public void Remove(string id)
        {
            Refresh();
            lock (SyncObject)
            {
                InnerRemove(id);
            }
        }
        private void InnerRemove(string id)
        {
            Cache<T> cache;
            if (Caches.TryGetValue(id, out cache))
            {
                cache.Dispose();
                Caches.Remove(id);
            }
        }

        protected class CacheIntervalTask
        {
            public CacheIntervalTask(CacheManager<T> manager)
            {
                Manager = manager;
            }

            protected CacheManager<T> Manager
            {
                get;
                private set;
            }

            public void Run()
            {
                while (true)
                {
                    try
                    {
                        Process();
                    }
                    catch { }
                    Thread.Sleep(_processInterval);
                }
            }

            public virtual void Process()
            {
                lock (Manager.SyncObject)
                {
                    foreach (var id in Manager.Caches.Keys.ToList())
                    {
                        if (Manager.Caches[id].IsExpired)
                        {
                            Manager.InnerRemove(id);
                        }
                    }
                }
            }
        }
    }
}
