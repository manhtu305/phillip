﻿using System.Collections.Generic;
using System.Collections.Specialized;

namespace C1.Web.Api
{

    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal interface ISyncCacheManager
    {
        void Remove(string id);
    }

    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal interface IEditableCacheManager : ISyncCacheManager
    {
        bool SaveDocument(string path);
    }

    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal static class StorageDocumentCacheHelper
    {
        public readonly static IList<IEditableCacheManager> EditableStorageDocCacheManagers
            = new List<IEditableCacheManager>();
        public readonly static IList<ISyncCacheManager> StorageDocCacheManagers
            = new List<ISyncCacheManager>();

        public static void SaveDocument(string path)
        {
            foreach (var manager in EditableStorageDocCacheManagers)
            {
                if (manager.SaveDocument(path))
                {
                    break;
                }
            }
        }

        public static void RemoveCache(string path)
        {
            foreach (var manager in StorageDocCacheManagers)
            {
                manager.Remove(path);
            }
        }
    }


    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal abstract class EditableStorageDocumentCacheManager<T, V> : StorageDocumentCacheManager<T, V>, IEditableCacheManager
        where T : class, IEditableStorageDocument<V> where V : class
    {
        protected EditableStorageDocumentCacheManager() : base()
        {
            StorageDocumentCacheHelper.EditableStorageDocCacheManagers.Add(this);
        }

        public bool SaveDocument(string path)
        {
            lock (SyncObject)
            {
                Cache<T> cache;
                T content;
                if (InnerTryGet(path, out cache))
                {
                    content = cache.Content;
                    content.SaveChanges();
                    return true;
                }

                return false;
            }
        }

        protected override CacheIntervalTask CreateCacheIntervalTask()
        {
            return new DocCacheIntervalTask(this);
        }

        protected class DocCacheIntervalTask : CacheIntervalTask
        {
            public DocCacheIntervalTask(CacheManager<T> manager) : base(manager)
            {
            }

            public override void Process()
            {
                var manager = Manager as CacheManager<T>;
                lock (manager.SyncObject)
                {
                    foreach (var cache in manager.Caches)
                    {
                        cache.Value.Content.SaveChanges();
                    }
                }

                base.Process();
            }
        }
    }

    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal abstract class StorageDocumentCacheManager<T, V> : CacheManager<T>, ISyncCacheManager
    where T : class, IStorageDocument<V> where V : class
    {
        protected StorageDocumentCacheManager()
        {
            StorageDocumentCacheHelper.StorageDocCacheManagers.Add(this);
        }

        protected abstract T CreateStorageDocument(string path, NameValueCollection args = null);

        public T GetDocument(string path, NameValueCollection args = null)
        {
            return GetOrCreate(path, args).Content;
        }

        public Cache<T> GetOrCreate(string path, NameValueCollection args = null)
        {
            Refresh();
            lock (SyncObject)
            {
                Cache<T> cache;
                if (InnerTryGet(path, out cache))
                {
                    return cache;
                }

                var docStorage = CreateStorageDocument(path, args);
                return InnerCreate(docStorage);
            }
        }

        protected override string GenerateId(T content)
        {
            return content.Path;
        }
    }
}