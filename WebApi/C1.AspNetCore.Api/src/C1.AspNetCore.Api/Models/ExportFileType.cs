﻿namespace C1.Web.Api
{
    /// <summary>
    /// Supported export file types
    /// </summary>
    public enum ExportFileType
    {
        /// <summary>
        /// Biff8 (Office Excel 97-2003 format)
        /// </summary>
        Xls,
        /// <summary>
        /// OpenXML (Office Excel 2007+ format)
        /// </summary>
        Xlsx,
        /// <summary>
        /// Comma Separated Values
        /// </summary>
        Csv,
        /// <summary>
        /// Adobe PDF (Portable Document Format)
        /// </summary>
        Pdf,
        /// <summary>
        /// Portable Network Graphics
        /// </summary>
        Png,
        /// <summary>
        /// Joint Photographic Experts Group
        /// </summary>
        Jpeg,
        /// <summary>
        /// Bitmap
        /// </summary>
        Bmp,
        /// <summary>
        /// Graphic Interchange Format
        /// </summary>
        Gif,
        /// <summary>
        /// Tagged Image File Format
        /// </summary>
        Tiff,
        /// <summary>
        /// Json format
        /// </summary>
        Json,
        /// <summary>
        /// Xml format
        /// </summary>
        Xml,
    }
}