﻿using System.IO;

namespace C1.Web.Api
{
    /// <summary>
    /// The wrapper of uploaded file as import source.
    /// </summary>
    public class ImportSource
    {
        /// <summary>
        /// The file name from content dispostion.
        /// </summary>
        public string FileName { get; set; }

        private Stream _fileStream;

        /// <summary>
        /// Create an ImportSource instance.
        /// </summary>
        /// <param name="fileName">The import file name.</param>
        /// <param name="fileStream">The import file stream.</param>
        public ImportSource(string fileName, Stream fileStream)
        {
            this.FileName = fileName;
            this._fileStream = fileStream;
        }

        /// <summary>
        /// Gets the file content stream.
        /// </summary>
        /// <returns></returns>
        public Stream GetFileStream()
        {
            return _fileStream;
        }
    }
}