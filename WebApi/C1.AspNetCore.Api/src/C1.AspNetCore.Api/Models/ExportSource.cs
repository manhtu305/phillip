﻿using System;
using System.Collections.Specialized;

namespace C1.Web.Api
{
    /// <summary>
    /// The exchange data model used for sending export requests.
    /// </summary>
    public class ExportSource
    {
        private string fileName;
        private Lazy<NameValueCollection> _requestParams;

        /// <summary>
        /// Gets or sets the exported file name.
        /// </summary>
        public string FileName
        {
            get { return fileName; }
            set { fileName = value != null ? value.Trim() : null; } // value?.Trim() in C# 6.0
        }

        /// <summary>
        /// Gets or sets the exported file type.
        /// </summary>
        public ExportFileType Type
        {
            get;
            set;
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal NameValueCollection RequestParams
        {
            [SmartAssembly.Attributes.DoNotObfuscate]
            get
            {
                return _requestParams == null ? null : _requestParams.Value;
            }
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal void SetRequestParamsGetter(Func<NameValueCollection> getter)
        {
            _requestParams = new Lazy<NameValueCollection>(getter);
        }
    }

    /// <summary>
    /// The source for exporter.
    /// </summary>
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal interface IExporterSource
    {
        /// <summary>
        /// Create the exporter of this source.
        /// </summary>
        /// <returns>The exporter.</returns>
        IExporter CreateExporter();
    }
}