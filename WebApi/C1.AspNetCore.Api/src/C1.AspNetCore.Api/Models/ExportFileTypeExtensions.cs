﻿#if NETCORE
using C1.AspNetCore.Api;
#else
using System.Drawing.Imaging;
#endif
using System;
using System.Collections.Generic;

namespace C1.Web.Api
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal static class ExportFileTypeExtensions
    {
        // enum to file extension mapping
        private static readonly Dictionary<ExportFileType, string> fileExtensionMaps = new Dictionary<ExportFileType, string>
        {
            { ExportFileType.Xlsx, ".xlsx" },
            { ExportFileType.Xls, ".xls" },
            { ExportFileType.Csv, ".csv" },
            { ExportFileType.Pdf, ".pdf" },
            { ExportFileType.Jpeg, ".jpg" },
            { ExportFileType.Png, ".png" },
            { ExportFileType.Bmp, ".bmp" },
            { ExportFileType.Gif, ".gif" },
            { ExportFileType.Tiff, ".tiff" },
            { ExportFileType.Xml, ".xml" },
            { ExportFileType.Json, ".json" }
        };

        // file extension to mime type mapping
        private static readonly Dictionary<string, string> mimeMap = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
        {
            { "xls", "application/vnd.ms-excel" },
            { "xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" },
            { "csv", "text/csv" },
            { "pdf", "application/pdf" },
            { "png","image/png" },
            { "jpeg", "image/jpeg" },
            { "bmp", "image/bmp" },
            { "gif", "image/gif" },
            { "tiff", "image/tiff" },
            { "html","text/html" },
            { "rtf", "text/plain" },
            { "doc", "application/vnd.ms-word" },
            { "docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document" },
            { "jpg", "image/jpeg" },
            { "emf", "application/x-emf" },
            { "zip", "application/zip" },
            { "mhtml", "message/rfc822" }
        };

        // map a subset of ExportFileTypes to ImageFormat
        private static readonly Dictionary<ExportFileType, ImageFormat> imageFormatMap = new Dictionary<ExportFileType, ImageFormat>
        {
            { ExportFileType.Png, ImageFormat.Png },
            { ExportFileType.Jpeg, ImageFormat.Jpeg },
            { ExportFileType.Bmp, ImageFormat.Bmp },
            { ExportFileType.Gif, ImageFormat.Gif },
            { ExportFileType.Tiff, ImageFormat.Tiff }
        };

        /// <summary>
        /// Gets file extension for ExportFileType enum
        /// </summary>
        public static string ToFileExtension(this ExportFileType type)
        {
            return fileExtensionMaps[type];
        }

        /// <summary>
        /// Gets mime type for ExportFileType enum
        /// </summary>
        public static string ToMediaType(this ExportFileType type)
        {
            return mimeMap[type.ToString()];
        }

        /// <summary>
        /// Gets mime type for file extension
        /// </summary>
        public static bool TryGetMediaType(string fileExtension, out string mediaType)
        {
            return mimeMap.TryGetValue(fileExtension, out mediaType);
        }

        /// <summary>
        /// Returns BCL ImageType for a given ExportFileType.
        /// </summary>
        public static ImageFormat ToImageFormat(this ExportFileType type)
        {
            ImageFormat format;
            bool found = imageFormatMap.TryGetValue(type, out format);
            return found ? format : ImageFormat.Png;
        }
    }
}