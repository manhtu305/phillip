﻿using C1.Web.Api.Storage;
using System;

namespace C1.Web.Api
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal interface IStorageDocument<T> : IDisposable where T : class
    {
        string Path { get; }
        IFileStorage File { get; }
        T Document { get; }
    }

    [SmartAssembly.Attributes.DoNotObfuscateType]
    internal interface IEditableStorageDocument<T> : IStorageDocument<T> where T : class
    {
        void SaveChanges();
        bool IsDirty { get; set; }
    }
}
