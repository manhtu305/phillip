﻿namespace C1.Web.Api
{
    /// <summary>
    /// The license manager.
    /// </summary>
    public static class LicenseManager
    {
        /// <summary>
        /// The run time license key.
        /// </summary>
        public static string Key;
    }
}
