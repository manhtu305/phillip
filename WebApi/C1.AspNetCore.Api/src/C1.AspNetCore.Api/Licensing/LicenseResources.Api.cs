﻿using C1.Web.Api.Localization;
using System.Globalization;
using System.Resources;

namespace C1.Util.Licensing
{
    internal static partial class LicenseResources
    {
        public static ResourceManager ResourceManager
        {
            get
            {
                return Resources.ResourceManager;
            }
        }

        public static CultureInfo Culture
        {
            get
            {
                return Resources.Culture;
            }
        }
    }
}
