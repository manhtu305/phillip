﻿using System;
using System.Reflection;
using C1.Web.Api.Localization;
#if !NETCORE
using System.Drawing;
using System.Drawing.Text;
#endif

namespace C1.Util.Licensing
{

    [SmartAssembly.Attributes.DoNotObfuscate]
    internal static partial class LicenseHelper
    {
#if !NETCORE
        private static readonly Color EvalInfoColor = Color.LightGray;

        [SmartAssembly.Attributes.DoNotObfuscate]
        public static void DrawEvalInfo(Image image)
        {
            using (var graphics = Graphics.FromImage(image))
            using (var font = CreateEvalInfoFont())
            using (var stringFromat = CreateEvalInfoStringFormat())
            using (var brush = new SolidBrush(EvalInfoColor))
            {
                graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
                graphics.DrawString(Resources.EvaluationMessage, font, brush, new RectangleF(0, 0, image.Width, image.Height), stringFromat);
            }
        }

        private static Font CreateEvalInfoFont()
        {
            return new Font("Arial", 20, FontStyle.Italic | FontStyle.Underline);
        }

        private static StringFormat CreateEvalInfoStringFormat()
        {
            var stringFromat = StringFormat.GenericDefault.Clone() as StringFormat;
            stringFromat.Alignment = StringAlignment.Center;
            stringFromat.LineAlignment = StringAlignment.Center;
            return stringFromat;
        }
#else
      // TODO Need check this license implementation
#endif
        //Method to help get Assembly of a type, difference between core and noncore
        [SmartAssembly.Attributes.DoNotObfuscate]
        public static Assembly Assembly(this Type type)
        {
#if ASPNETCORE || NETCORE
            return type.GetTypeInfo().Assembly;
#else
            return type.Assembly;
#endif
        }
    }
}
