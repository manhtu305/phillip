﻿namespace C1.Web.Api.Storage
{
    /// <summary>
    /// The base class of disk storage.
    /// </summary>
    [SmartAssembly.Attributes.DoNotObfuscate]
    internal abstract class BaseDiskStorage: BaseStorage
    {
        /// <summary>
        /// Create a BaseDiskStorage by specified name and the disk path.
        /// </summary>
        /// <param name="name">The name</param>
        /// <param name="path">The disk path</param>
        protected BaseDiskStorage(string name, string path)
            : base(name)
        {
            Path = path;
        }

        /// <summary>
        /// Gets the disk path.
        /// </summary>
        public string Path
        {
            [SmartAssembly.Attributes.DoNotObfuscate]
            get;
            private set;
        }
    }
}
