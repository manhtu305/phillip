﻿using System.Collections.Specialized;
using System.IO;

namespace C1.Web.Api.Storage
{
    /// <summary>
    /// The disk storage provider.
    /// </summary>
    public class DiskStorageProvider : IStorageProvider
    {
        /// <summary>
        /// Creates a <see cref="DiskStorageProvider"/> with disk path which is used as the root path.
        /// </summary>
        /// <param name="root">The disk path which is used as the root path.</param>
        public DiskStorageProvider(string root)
        {
            Root = root;
        }

        /// <summary>
        /// Gets the disk path which is used as the root path.
        /// </summary>
        public string Root
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the <see cref="IDirectoryStorage"/> with specified name and arguments.
        /// </summary>
        /// <param name="name">The name of the <see cref="IDirectoryStorage"/>.</param>
        /// <param name="args">The arguments</param>
        /// <returns>The <see cref="IDirectoryStorage"/>.</returns>
        public IDirectoryStorage GetDirectoryStorage(string name, NameValueCollection args = null)
        {
            name = StorageProviderManager.ResolveName(name);
            var path = GetFullPath(name);
            return new DirectoryDiskStorage(name, path);
        }

        /// <summary>
        /// Gets the <see cref="IFileStorage"/> with specified name and arguments.
        /// </summary>
        /// <param name="name">The name of the <see cref="IFileStorage"/>.</param>
        /// <param name="args">The arguments</param>
        /// <returns>The <see cref="IFileStorage"/>.</returns>
        public IFileStorage GetFileStorage(string name, NameValueCollection args = null)
        {
            name = StorageProviderManager.ResolveName(name);
            var path = GetFullPath(name);
            return new FileDiskStorage(name, path);
        }

        private string GetFullPath(string name)
        {
            return Path.Combine(Root, name);
        }
    }
}
