﻿using System.IO;
using System.Linq;

namespace C1.Web.Api.Storage
{
    internal class DirectoryDiskStorage : BaseDiskStorage, IDirectoryStorage
    {
        public DirectoryDiskStorage(string name, string path) : base(name, path)
        {
        }

        public override bool Exists
        {
            get
            {
                return Directory.Exists(Path);
            }
        }

        public void Delete(bool recursive = false)
        {
            Directory.Delete(Path, recursive);
        }

        public void Create()
        {
            Directory.CreateDirectory(Path);
        }

        public string[] GetDirectories(bool recursive = false, string searchPattern = null)
        {
            searchPattern = string.IsNullOrEmpty(searchPattern) ? "*" : searchPattern;
            return Directory.GetDirectories(Path, searchPattern, recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
                .Select(ResolveName).ToArray();
        }

        public string[] GetFiles(bool recursive = false, string searchPattern = null)
        {
            return Directory.GetFiles(Path, searchPattern, recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly)
                .Select(ResolveName).ToArray();
        }

        private string ResolveName(string path)
        {
            return StorageProviderManager.ResolveName(path.Substring(Path.Length));
        }
    }
}
