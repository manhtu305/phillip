﻿using System;
using System.Collections.Generic;
using System.IO;
using C1.Web.Api.Storage;
using System.Linq;

namespace C1.Web.Api.Storage
{
#pragma warning disable CS0618 // Type or member is obsolete
    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class FileDiskStorage : BaseDiskStorage, IFileStorage, C1.Storage.IFileStorage
#pragma warning restore CS0618 // Type or member is obsolete
    {
        private new string Path { get; set; }
        private string Target { get; set; }
        string FilePath { get; set; }
        public FileDiskStorage(string name, string path) : base(name, path)
        {
            string filePath = "";
            string target = "";
            string root = path;

            if (!string.IsNullOrEmpty(name))
            {
                root = path.Replace(name, "");
            }

            if (name.Contains("targetpath"))
            {
                SeparateName(name, out filePath, out target);
            }
            else
            {
                filePath = path;
            }

            Path = System.IO.Path.Combine(root, filePath);
            Target = System.IO.Path.Combine(root, target);
        }

        private void SeparateName(string name, out string path, out string target)
        {
            if (name.Contains("?"))
            {
                var targetNode = name.Split('?').ToArray();
                target = targetNode.LastOrDefault().Replace("targetpath=", "");
                if (target.Trim().FirstOrDefault() == '/')
                    target = target.Substring(1);
                name = targetNode.FirstOrDefault();
            }
            else
            {
                target = "";
            }
            path = name;
        }

        public override bool Exists
        {
            get
            {
                return System.IO.File.Exists(Path);
            }
        }

        public bool ReadOnly
        {
            get
            {
                return System.IO.File.GetAttributes(Path).HasFlag(FileAttributes.ReadOnly);
            }
        }

        public Stream Read()
        {
            var stream = new MemoryStream();
            using (var fs = System.IO.File.OpenRead(Path))
            {
                fs.CopyTo(stream);
            }

            stream.Position = 0;
            return stream;
        }

        public void Write(Stream stream)
        {
            var directory = new FileInfo(Path).Directory;
            if (!directory.Exists)
            {
                directory.Create();
            }

            var tempPathAndName = string.Empty;
            if (Exists)
            {
                var finfo = new FileInfo(Path);
                tempPathAndName = System.IO.Path.Combine(finfo.DirectoryName, System.IO.Path.GetRandomFileName());
                finfo.MoveTo(tempPathAndName);
            }

            try
            {
                using (var fs = new FileStream(Path, FileMode.Create, FileAccess.Write))
                {
                    stream.CopyTo(fs);
                }

                if (!string.IsNullOrEmpty(tempPathAndName))
                {
                    System.IO.File.Delete(tempPathAndName);
                }
            }
            catch
            {
                if (!string.IsNullOrEmpty(tempPathAndName))
                {
                    Delete();
                    new FileInfo(tempPathAndName).MoveTo(Path);
                }
            }
        }

        public void Delete()
        {
            if (Exists)
            {
                System.IO.File.Delete(Path);
            }
        }

        /// <summary>
        /// Create new folder with the specified path
        /// </summary>
        public void CreateFolder()
        {
            Directory.CreateDirectory(Path);
        }

        /// <summary>
        /// Gets all files and folders within the specified path.
        /// </summary>
        /// <returns>files and folders within the specified path</returns>
        public List<ListResult> List()
        {
            var resultList = new List<ListResult>();
            foreach (var dir in Directory.GetDirectories(Path).Select(ResolveName).ToArray())
            {
                var item = new ListResult();
                item.Name = dir;
                item.Type = ResultType.Folder;
                resultList.Add(item);
            }
            foreach (var file in Directory.GetFiles(Path).Select(ResolveName).ToArray())
            {
                var item = new ListResult();
                item.Name = file;
                FileInfo fi = new FileInfo(System.IO.Path.Combine(Path, file));
                if (fi != null)
                {
                    item.Size = ByteToKB(fi.Length);
                    item.ModifiedDate = fi.LastWriteTimeUtc == null ? "n/a" : fi.LastWriteTimeUtc.ToString("MM/dd/yyyy HH:mm:ss");
                }
                item.Type = ResultType.File;
                resultList.Add(item);
            }
            return resultList;
        }
        private double ByteToKB(long _byte)
        {
            if (_byte == 0) return 0;

            return Math.Floor((double)_byte / 1024);
        }

        /// <summary>
        /// Move files from current path to destination path
        /// </summary>
        public void MoveFile()
        {
            string tempPath = Path;
            var fileStream = Read();
            if (fileStream.Length != 0)
            {
                try
                {
                    Path = Target;
                    Write(fileStream);
                    Path = tempPath;
                    Delete();
                }
                catch (Exception e)
                {
                    Path = Target;
                    Delete();
                }
            }
        }

        private string ResolveName(string path)
        {
            return StorageProviderManager.ResolveName(path.Substring(Path.Length));
        }
    }
}
