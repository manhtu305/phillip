﻿using C1.Web.Api.Storage;
using System.Collections.Generic;
using System.IO;

namespace C1.Web.Api.Storage
{
    /// <summary>
    /// The file storage interface.
    /// </summary>
    public interface IFileStorage : IStorage
    {
        /// <summary>
        /// Determines whether the specified file is read only.
        /// </summary>
        bool ReadOnly { get; }

        /// <summary>
        /// Read the file stream.
        /// </summary>
        /// <returns></returns>
        Stream Read();
        /// <summary>
        /// Write a stream to file, if the file already exists, it is overwritten.
        /// </summary>
        /// <param name="stream">The file stream</param>
        void Write(Stream stream);

        /// <summary>
        /// Delete the file from storage.
        /// </summary>
        void Delete();
    }
}
