﻿using System;
using System.ComponentModel;

namespace C1.Storage
{
    /// <summary>
    /// The file storage interface.
    /// </summary>
    [Obsolete("Please use C1.Web.Api.Storage.IFileStorage instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public interface IFileStorage : Web.Api.Storage.IFileStorage, IStorage
    {
    }
}
