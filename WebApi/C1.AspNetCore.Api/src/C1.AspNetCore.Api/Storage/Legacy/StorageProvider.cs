﻿using C1.Configuration;
using C1.Web.Api.Localization;
using System;
using System.Collections.Specialized;
using System.ComponentModel;

namespace C1.Storage
{
    /// <summary>
    /// The base class of the storage provider.
    /// </summary>
    [Obsolete("Please use C1.Web.Api.Storage.IStorageProvider instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public abstract class StorageProvider : ConditionalProvider<IStorage>
    {
        /// <summary>
        /// Gets the file storage with specified name and arguments.
        /// </summary>
        /// <param name="name">The name of the file storage</param>
        /// <param name="args">The arguments</param>
        /// <returns>The file storage</returns>
        public IFileStorage GetFileStorage(string name, NameValueCollection args = null)
        {
            var file = GetObject(name, args) as IFileStorage;
            if (file == null)
            {
                throw new InvalidOperationException(string.Format(Resources.FileNotFound, name));
            }

            return file;
        }
    }
}
