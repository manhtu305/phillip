﻿using System;
using System.ComponentModel;

namespace C1.Storage
{
    /// <summary>
    /// The storage interface.
    /// </summary>
    [Obsolete("Please use C1.Web.Api.Storage.IStorage instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public interface IStorage : Web.Api.Storage.IStorage
    {
    }
}
