﻿using C1.Configuration;
using C1.Web.Api.Localization;
using System;
using System.ComponentModel;
using System.Collections.Specialized;

namespace C1.Storage
{
    /// <summary>
    /// The storage provider manager.
    /// </summary>
    [Obsolete("Please use C1.Web.Api.Storage.StorageProviderManager instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public class StorageProviderManager : ConditionalProviderManager<IStorage, StorageProvider>
    {
        private static StorageProviderManager _current = new StorageProviderManager();

        /// <summary>
        /// Gets or sets the current storage provider manager.
        /// </summary>
        public static StorageProviderManager Current
        {
            get
            {
                return _current;
            }
            set
            {
                _current = value;
            }
        }

        /// <summary>
        /// Gets the storage with specified name and arguments.
        /// </summary>
        /// <param name="name">The name of storage</param>
        /// <param name="args">The arguments</param>
        /// <returns>The storage</returns>
        public static IStorage GetStorage(string name, NameValueCollection args = null)
        {
            return Current.GetObject(name, args);
        }

        /// <summary>
        /// Gets the file storage with specified name and arguments.
        /// </summary>
        /// <param name="name">The name of file storage</param>
        /// <param name="args">The arguments</param>
        /// <returns>The file storage</returns>
        public static IFileStorage GetFileStorage(string name, NameValueCollection args = null)
        {
            var file = GetStorage(name, args) as IFileStorage;
            if (file == null)
            {
                throw new InvalidOperationException(string.Format(Resources.FileNotFound, name));
            }

            return file;
        }
    }
}