﻿using C1.Web.Api.Localization;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.ComponentModel;

namespace C1.Storage
{
    /// <summary>
    /// The disk storage provider.
    /// </summary>
    [Obsolete("Please use C1.Web.Api.Storage.DiskStorageProvider instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public class DiskStorageProvider : StorageProvider
    {
        private readonly IDictionary<string, string> _roots =
            new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Gets a boolean indicates whether current storage provider recognizes the name.
        /// </summary>
        /// <param name="name">The name of the object</param>
        /// <returns>A boolean indicates whether current provider recognizes the name.</returns>
        public override bool Supports(string name)
        {
            return Roots.Any(p => Match(p.Key, name));
        }

        private static bool Match(string root, string name)
        {
            return root == string.Empty || name.StartsWith(root, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Gets the storage with the specified name and arguments.
        /// </summary>
        /// <param name="name">The name of storage</param>
        /// <param name="args">The arguments</param>
        /// <returns>The storage</returns>
        public override IStorage GetObject(string name, NameValueCollection args)
        {
            var pair = Roots.FirstOrDefault(p => Match(p.Key, name));
            if (string.IsNullOrEmpty(pair.Value) || pair.Key == null)
            {
                throw new InvalidOperationException(string.Format(Resources.StorageNameNotFound, name));
            }

            var nameWithoutRoot = name.Substring(pair.Key.Length).TrimStart(new char[] { '/', '\\' });
            var path = Path.Combine(pair.Value, nameWithoutRoot);

            return new Web.Api.Storage.FileDiskStorage(name, path);
        }

        /// <summary>
        /// Gets the roots with names and paths.
        /// </summary>
        public IDictionary<string, string> Roots
        {
            get
            {
                return _roots;
            }
        }
    }
}
