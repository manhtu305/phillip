﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C1.Web.Api.Storage.Legacy
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICloudStorage : IFileStorage
    {
        /// <summary>
        /// List all files and folders.
        /// </summary>
        /// <returns></returns>
        List<ListResult> List();
        /// <summary>
        /// Move files from current path to destination path
        /// </summary>
        void MoveFile();
        /// <summary>
        /// Create new folder with the specified path
        /// </summary>
        void CreateFolder();
    }
}
