﻿namespace C1.Web.Api.Storage
{
    /// <summary>
    /// The storage interface.
    /// </summary>
    public interface IStorage
    {
        /// <summary>
        /// Gets the name of storage.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Determines whether the specified file exists.
        /// </summary>
        bool Exists { get; }
    }
}
