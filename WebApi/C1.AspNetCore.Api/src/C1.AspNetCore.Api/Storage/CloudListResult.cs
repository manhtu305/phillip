﻿
using System;
using System.Collections.Generic;

namespace C1.Web.Api.Storage
{
    /// <summary>
    /// Class for DeserializeObject
    /// </summary>
    public class ListResult
    {
        /// <summary>
        /// File ID
        /// </summary>
        public string ItemID { get; set; }
        /// <summary>
        /// File type
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// File type
        /// </summary>
        public ResultType Type { get; set; }
        /// <summary>
        /// File Size
        /// </summary>
        public double Size { get; set; }
        /// <summary>
        /// Last Modified Date
        /// </summary>
        public string ModifiedDate { get; set; }
    }

    /// <summary>
    /// Class for DeserializeObject
    /// </summary>
    public enum ResultType
    {
        /// <summary>
        /// Folder
        /// </summary>
        Folder = 0,
        /// <summary>
        /// File
        /// </summary>
        File = 1
    }

    /// <summary>
    /// Class for DeserializeObject
    /// </summary>
    public class Folder
    {
        /// <summary>
        /// Folder type
        /// </summary>
        public string folderType { get; set; }
    }

    /// <summary>
    /// Class for DeserializeObject
    /// </summary>
    public class Value
    {
        /// <summary>
        /// Folder Name
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// Folder reference
        /// </summary>
        public Folder folder { get; set; }

        /// <summary>
        ///  Size
        /// </summary>
        public ulong size { get; set; }
        /// <summary>
        ///  lastModifiedDateTime
        /// </summary>
        public DateTime lastModifiedDateTime { get; set; }
    }

    /// <summary>
    /// Class for DeserializeObject
    /// </summary>
    public class RootObject
    {
        /// <summary>
        /// List of Value class
        /// </summary>
        public List<Value> value { get; set; }
    }
}
