﻿namespace C1.Web.Api.Storage
{
    /// <summary>
    /// The directory storage interface.
    /// </summary>
    public interface IDirectoryStorage : IStorage
    {
        /// <summary>
        /// Gets files in this directory.
        /// </summary>
        string[] GetFiles(bool recursive = false, string searchPattern = null);

        /// <summary>
        /// Gets directories in this directory.
        /// </summary>
        /// <returns></returns>
        string[] GetDirectories(bool recursive = false, string searchPattern = null);

        /// <summary>
        /// Creates the directory.
        /// </summary>
        void Create();

        /// <summary>
        /// Delete the directory from storage.
        /// </summary>
        void Delete(bool recursive = false);
    }
}
