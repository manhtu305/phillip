﻿using C1.Web.Api.Storage;

#if ASPNETCORE || NETCORE
namespace Microsoft.AspNetCore.Builder
#else
namespace Owin
#endif
{
    /// <summary>
    /// The extensions of <see cref="StorageProviderManager"/>.
    /// </summary>
    public static class StorageProviderManagerExtensions
    {
        /// <summary>
        /// Add a <see cref="DiskStorageProvider"/> to <see cref="StorageProviderManager"/> with the specified key and the disk path.
        /// </summary>
        /// <param name="manager">The <see cref="StorageProviderManager"/>.</param>
        /// <param name="key">The key.</param>
        /// <param name="diskPath">The disk path.</param>
        /// <returns></returns>
        public static StorageProviderManager AddDiskStorage(this StorageProviderManager manager, string key, string diskPath)
        {
            manager.Add(key, new DiskStorageProvider(diskPath));
            return manager;
        }
    }
}
