﻿using C1.Web.Api.Configuration;
using System.Collections.Specialized;
using System.Linq;

namespace C1.Web.Api.Storage
{
    /// <summary>
    /// This manager mantains multiple <see cref="IStorageProvider"/>.
    /// </summary>
    /// <remarks>
    /// If add an <see cref="IStorageProvider"/> with empty string key, this 
    /// <see cref="IStorageProvider"/> will be treated as the default provider.
    /// When no macthed provider are found, the default provider will be used to 
    /// get the storage.
    /// </remarks>
    public sealed class StorageProviderManager : Manager<IStorageProvider>
    {
        private static readonly StorageProviderManager _current = new StorageProviderManager();

        private StorageProviderManager()
        {
        }

        /// <summary>
        /// Gets the <see cref="StorageProviderManager"/> instance.
        /// </summary>
        public static StorageProviderManager Current
        {
            get
            {
                return _current;
            }
        }

        private static void SeparatePath(string fullPath, out string providerKey, out string storageName)
        {
            var resolvedName = ResolveName(fullPath);
            var nodes = resolvedName.Split('/').ToArray();
            providerKey = nodes.FirstOrDefault();
            storageName = string.Join("/", nodes.Skip(1).ToArray());
        }

        internal static string ResolveName(string name)
        {
            var resolvedName = name ?? string.Empty;
            return resolvedName.Replace('\\', '/').Trim('/');
        }

        /// <summary>
        /// Gets the <see cref="IStorageProvider"/> with the specified path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="key">The key of the <see cref="IStorageProvider"/>.</param>
        /// <param name="storageName">The storage name which doesn't include the key of the <see cref="IStorageProvider"/>.</param>
        /// <returns>The matched <see cref="IStorageProvider"/>.</returns>
        public IStorageProvider GetProvider(string path, out string key, out string storageName)
        {
            SeparatePath(path, out key, out storageName);
            IStorageProvider provider;
            if (!TryGet(key, out provider))
            {
                storageName = path;
                key = string.Empty;
                TryGet(key, out provider);
            }

            return provider;
        }

        /// <summary>
        /// Gets the <see cref="IDirectoryStorage"/> with specified path and arguments.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="args">The arguments.</param>
        /// <returns>The <see cref="IDirectoryStorage"/>.</returns>
        public static IDirectoryStorage GetDirectoryStorage(string path, NameValueCollection args = null)
        {
            string key, storageName;
            var provider = Current.GetProvider(path, out key, out storageName);
            if (provider != null)
            {
                return provider.GetDirectoryStorage(storageName, args);
            }

            return null;
        }

        /// <summary>
        /// Gets the <see cref="IFileStorage"/> with specified path and arguments.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="args">The arguments.</param>
        /// <returns>The <see cref="IFileStorage"/>.</returns>
        public static IFileStorage GetFileStorage(string path, NameValueCollection args = null)
        {
            string key, storageName;
            var provider = Current.GetProvider(path, out key, out storageName);
            if (provider != null)
            {
                return provider.GetFileStorage(storageName, args);
            }

#pragma warning disable 618 // Type or member is obsolete
            var legacyManager = C1.Storage.StorageProviderManager.Current;
            if (legacyManager != null)
            {
                return C1.Storage.StorageProviderManager.GetStorage(path, args) as IFileStorage;
            }
#pragma warning restore 618 // Type or member is obsolete

            return null;
        }
    }
}
