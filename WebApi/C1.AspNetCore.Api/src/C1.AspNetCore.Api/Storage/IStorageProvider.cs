﻿using System.Collections.Specialized;

namespace C1.Web.Api.Storage
{
    /// <summary>
    /// The base class of the storage provider.
    /// </summary>
    public interface IStorageProvider
    {
        /// <summary>
        /// Gets the <see cref="IFileStorage"/> with specified name and arguments.
        /// </summary>
        /// <param name="name">The name of the <see cref="IFileStorage"/>.</param>
        /// <param name="args">The arguments</param>
        /// <returns>The <see cref="IFileStorage"/>.</returns>
        IFileStorage GetFileStorage(string name, NameValueCollection args = null);

        /// <summary>
        /// Gets the <see cref="IDirectoryStorage"/> with specified name and arguments.
        /// </summary>
        /// <param name="name">The name of the <see cref="IDirectoryStorage"/>.</param>
        /// <param name="args">The arguments</param>
        /// <returns>The <see cref="IDirectoryStorage"/>.</returns>
        IDirectoryStorage GetDirectoryStorage(string name, NameValueCollection args = null);

    }
}
