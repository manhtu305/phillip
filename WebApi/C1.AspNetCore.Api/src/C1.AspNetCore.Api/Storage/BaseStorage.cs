﻿namespace C1.Web.Api.Storage
{
    /// <summary>
    /// The base class of storage.
    /// </summary>
    internal abstract class BaseStorage: IStorage
    {
        /// <summary>
        /// Create a BaseStorage with name.
        /// </summary>
        /// <param name="name">The name</param>
        protected BaseStorage(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        public string Name
        {
            get;
            private set;
        }

        public abstract bool Exists { get; }
    }
}
