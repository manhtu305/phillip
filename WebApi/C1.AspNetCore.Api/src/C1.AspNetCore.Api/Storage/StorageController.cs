﻿using System.IO;
using C1.Web.Api.Storage.Legacy;
using System.Collections.Generic;
#if !ASPNETCORE && !NETCORE
using System.Web.Http;
using Controller = System.Web.Http.ApiController;
using IActionResult = System.Web.Http.IHttpActionResult;
using System.Web.Http.ModelBinding;
#else
using Microsoft.AspNetCore.Mvc;
#endif

namespace C1.Web.Api.Storage
{
    /// <summary>
    /// Controller for Storage Web API.
    /// </summary>
    public class StorageController : Controller
    {
        /// <summary>
        /// Gets the file with the specified path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="subpath">The file name</param>
        /// <returns>The result which contains the file.</returns>
        [HttpGet]
        [Route("api/storage/{*path}")]
        public IActionResult GetFile(string path, string subpath = null)
        {
            path = path + subpath;
            StorageDocumentCacheHelper.SaveDocument(path);
            return new FileResult(() => StorageProviderManager.GetFileStorage(path, Request.GetParams()).Read(), Path.GetFileName(path));
        }

        /// <summary>
        /// Gets all files and folders within the specified path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="subpath">The file name</param>
        /// <returns>The result which contains the files and folders at json type.</returns>
        [HttpGet]
        [Route("api/storage/List/{*path}")]
        public IActionResult GetListFileName(string path, string subpath = null)
        {
            path = path + subpath;
            StorageDocumentCacheHelper.SaveDocument(path);
            IFileStorage result = StorageProviderManager.GetFileStorage(path, Request.GetParams());

            ICloudStorage cloudResult = result as ICloudStorage;

            if (cloudResult == null)
            {
                FileDiskStorage diskResult = result as FileDiskStorage;
                if (diskResult != null)
                    return Json(diskResult.List());
            } 
            else
                return Json(cloudResult.List());
            return Json(new List<ListResult>());
        }

        /// <summary>
        /// Uploads file with the specified path.
        /// </summary>
        /// <param name="path">The path which the uploaded file is put.</param>
        /// <param name="subpath">The file name</param>
        /// <param name="re">The upload request.</param>
        /// <returns>The result.</returns>
        [HttpPut]
        [HttpPost]
        [Route("api/storage/{*path}")]
        public IActionResult UploadFile(string path, [ModelBinder(BinderType = typeof(RequestModelBinder<UploadRequest>))]UploadRequest re, string subpath = null)
        {
            path = path + subpath;
            StorageDocumentCacheHelper.RemoveCache(path);
            using (var stream = re.File.GetStream())
            {
                stream.Position = 0;
                StorageProviderManager.GetFileStorage(path, Request.GetParams()).Write(stream);
            }
            return Json(new { Success = true });
        }

        /// <summary>
        /// Deletes the file with the specified path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="subpath">The file name</param>
        /// <returns>The result.</returns>
        [HttpDelete]
        [Route("api/storage/{*path}")]
        public IActionResult DeleteFile(string path, string subpath = null)
        {
            path = path + subpath;
            StorageDocumentCacheHelper.RemoveCache(path);
            StorageProviderManager.GetFileStorage(path, Request.GetParams()).Delete();
            return Json(new { Success = true });
        }

        /// <summary>
        /// Move files from current path to destination path
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="subpath">The file name</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/storage/Move/{*path}")]
        public IActionResult MoveFile(string path, string subpath = null)
        {
            path = path + subpath;
            StorageDocumentCacheHelper.SaveDocument(path);
            var result = StorageProviderManager.GetFileStorage(path, Request.GetParams());
            ICloudStorage cloudResult = result as ICloudStorage;

            if (cloudResult == null)
            {
                FileDiskStorage diskResult = result as FileDiskStorage;
                if (diskResult != null)
                    diskResult.MoveFile();
            }
            else
                cloudResult.MoveFile();
            return Json(new { Success = true });
        }

        /// <summary>
        /// Create new folder with path
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="subpath">The sub path.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/storage/Folder/{*path}")]
        public IActionResult CreateFolder(string path, string subpath = null)
        {
            path = path + subpath;
            StorageDocumentCacheHelper.SaveDocument(path);

            var result = StorageProviderManager.GetFileStorage(path, Request.GetParams());
            ICloudStorage cloudResult = result as ICloudStorage;

            if (cloudResult == null)
            {
                FileDiskStorage diskResult = result as FileDiskStorage;
                if (diskResult != null)
                    diskResult.CreateFolder();
            }
            else
                cloudResult.CreateFolder();
            return Json(new { Success = true });
        }
    }
}