﻿namespace C1.Web.Api.Storage
{
    /// <summary>
    /// The request data of uploading file to storage.
    /// </summary>
    public class UploadRequest
    {
        /// <summary>
        /// Gets or sets the file which is posted from client side.
        /// </summary>
        public FormFile File
        {
            get;
            set;
        }
    }
}
