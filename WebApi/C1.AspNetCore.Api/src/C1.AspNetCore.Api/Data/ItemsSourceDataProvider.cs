﻿using System;
using System.Collections;
using System.Collections.Specialized;

namespace C1.Web.Api.Data
{
    /// <summary>
    /// The data provider which can provider items as the data source.
    /// </summary>
    public class ItemsSourceDataProvider : IDataProvider
    {
        private readonly Func<IEnumerable> _reader;

        /// <summary>
        /// Creates an <see cref="ItemsSourceDataProvider"/> with data reader.
        /// </summary>
        /// <param name="reader">A delegate indicates the data reader.</param>
        public ItemsSourceDataProvider(Func<IEnumerable> reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            _reader = reader;
        }

        /// <summary>
        /// Creates an <see cref="ItemsSourceDataProvider"/> with items.
        /// </summary>
        /// <param name="items">The items.</param>
        public ItemsSourceDataProvider(IEnumerable items) : this(new Func<IEnumerable>(() => items))
        {
        }

        /// <summary>
        /// Read data.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns>The data.</returns>
        public IEnumerable Read(NameValueCollection args = null)
        {
            return _reader();
        }
    }
}
