﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;

namespace C1.Data
{
    /// <summary>
    /// The dataset provider.
    /// </summary>
    [Obsolete("Please use C1.Web.Api.Data.ItemsSourceProvider instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public class DataSetProvider: DataProvider
    {
        private readonly IDictionary<string, Func<IEnumerable>> _dataSets =
            new Dictionary<string, Func<IEnumerable>>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Gets a boolean indicates whether current provider recognizes the name.
        /// </summary>
        /// <param name="name">The name of the object</param>
        /// <returns>A boolean indicates whether current provider recognizes the name.</returns>
        public override bool Supports(string name)
        {
            return DataSets.ContainsKey(name);
        }

        /// <summary>
        /// Gets the IEnumerable data with the specified name and arguments.
        /// </summary>
        /// <param name="name">The name</param>
        /// <param name="args">The arguments</param>
        /// <returns>The IEnumerable data</returns>
        public override IEnumerable GetObject(string name, NameValueCollection args)
        {
            Func<IEnumerable> dataSetGetter;
            return _dataSets.TryGetValue(name, out dataSetGetter) ? dataSetGetter() : null;
        }

        /// <summary>
        /// The collection of dataset.
        /// </summary>
        public IDictionary<string, Func<IEnumerable>> DataSets
        {
            get
            {
                return _dataSets;
            }
        }
    }
}
