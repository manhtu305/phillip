﻿using System.Collections;
using C1.Configuration;
using System;
using System.ComponentModel;

namespace C1.Data
{
    /// <summary>
    /// The base class of data provider.
    /// </summary>
    [Obsolete("Please use C1.Web.Api.Data.IDataProvider inerface instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public abstract class DataProvider: ConditionalProvider<IEnumerable>
    {
    }
}
