﻿using C1.Configuration;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;

namespace C1.Data
{
    /// <summary>
    /// The manager of data provider.
    /// </summary>
    [Obsolete("Please use C1.Web.Api.Data.DataProviderManager instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public class DataProviderManager : ConditionalProviderManager<IEnumerable, DataProvider>
    {
        private static DataProviderManager _current = new DataProviderManager();

        /// <summary>
        /// Gets or sets the current DataProviderManager.
        /// </summary>
        public static DataProviderManager Current
        {
            get
            {
                return _current;
            }
            set
            {
                _current = value;
            }
        }

        /// <summary>
        /// Gets the data by specified name and arguments.
        /// </summary>
        /// <param name="name">The name</param>
        /// <param name="args">The arguments</param>
        /// <returns>The data</returns>
        public static IEnumerable GetData(string name, NameValueCollection args = null)
        {
            return Current.GetObject(name, args);
        }
    }
}