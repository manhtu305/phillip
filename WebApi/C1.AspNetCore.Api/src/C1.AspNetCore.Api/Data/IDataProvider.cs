﻿using System.Collections;
using System.Collections.Specialized;

namespace C1.Web.Api.Data
{
    /// <summary>
    /// The base class of data provider.
    /// </summary>
    public interface IDataProvider
    {
        /// <summary>
        /// Read data.
        /// </summary>
        /// <param name="args">The arguments.</param>
        /// <returns>The data.</returns>
        IEnumerable Read(NameValueCollection args = null);
    }
}
