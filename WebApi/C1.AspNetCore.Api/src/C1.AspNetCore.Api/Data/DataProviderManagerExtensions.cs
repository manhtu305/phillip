﻿using C1.Web.Api.Data;
using System;
using System.Collections;

#if ASPNETCORE || NETCORE
namespace Microsoft.AspNetCore.Builder
#else
namespace Owin
#endif
{
    /// <summary>
    /// The extensions of <see cref="DataProviderManager"/>.
    /// </summary>
    public static class DataProviderManagerExtensions
    {
        /// <summary>
        /// Add an <see cref="ItemsSourceDataProvider"/> with key and items.
        /// </summary>
        /// <param name="manager">The <see cref="DataProviderManager"/>.</param>
        /// <param name="key">The key.</param>
        /// <param name="items">The data source.</param>
        /// <returns>The <see cref="DataProviderManager"/>.</returns>
        public static DataProviderManager AddItemsSource(this DataProviderManager manager, string key, IEnumerable items)
        {
            manager.Add(key, new ItemsSourceDataProvider(items));
            return manager;
        }

        /// <summary>
        /// Add an <see cref="ItemsSourceDataProvider"/> with key and items getter.
        /// </summary>
        /// <param name="manager">The <see cref="DataProviderManager"/>.</param>
        /// <param name="key">The key.</param>
        /// <param name="itemsGetter">The delegate of getting data source.</param>
        /// <returns>The <see cref="DataProviderManager"/>.</returns>
        public static DataProviderManager AddItemsSource(this DataProviderManager manager, string key, Func<IEnumerable> itemsGetter)
        {
            manager.Add(key, new ItemsSourceDataProvider(itemsGetter));
            return manager;
        }
    }
}
