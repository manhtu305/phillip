﻿using C1.Web.Api.Configuration;
using System.Collections;
using System.Collections.Specialized;

namespace C1.Web.Api.Data
{
    /// <summary>
    /// The manager of data provider.
    /// </summary>
    public sealed class DataProviderManager : Manager<IDataProvider>
    {
        private readonly static DataProviderManager _current = new DataProviderManager();

        private DataProviderManager()
        {
        }

        /// <summary>
        /// Gets or sets the current DataProviderManager.
        /// </summary>
        public static DataProviderManager Current
        {
            get
            {
                return _current;
            }
        }

        /// <summary>
        /// Gets the data by specified name and arguments.
        /// </summary>
        /// <param name="name">The name</param>
        /// <param name="args">The arguments</param>
        /// <returns>The data</returns>
        public static IEnumerable Read(string name, NameValueCollection args = null)
        {
            IDataProvider provider;
            if (Current.TryGet(name, out provider))
            {
                return provider.Read(args);
            }

#pragma warning disable 618 // Type or member is obsolete
            var legacy = C1.Data.DataProviderManager.Current;
            if (legacy != null)
            {
                return C1.Data.DataProviderManager.GetData(name, args);
            }
#pragma warning restore 618 // Type or member is obsolete

            return null;
        }
    }
}