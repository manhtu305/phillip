﻿using System;
using System.Collections;
using System.ComponentModel;

#if ASPNETCORE || NETCORE
using IAppBuilder = Microsoft.AspNetCore.Builder.IApplicationBuilder;
namespace Microsoft.AspNetCore.Builder
#else
namespace Owin
#endif
{
    /// <summary>
    /// Extension methods for <see cref="IAppBuilder"/>.
    /// </summary>
    public static partial class AppBuilderUseExtensions
    {
        /// <summary>
        /// Add a StroageProvider to current StorageProviderManager.
        /// </summary>
        /// <param name="app">The app builder</param>
        /// <param name="storageProvider">The StorageProvider</param>
        /// <returns>The app builder</returns>
        [Obsolete("Please use UseStorageProvider instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static IAppBuilder AddStorageProvider(this IAppBuilder app, C1.Storage.StorageProvider storageProvider)
        {
            C1.Storage.StorageProviderManager.Current.Providers.Add(storageProvider);
            return app;
        }

        /// <summary>
        /// Add a DataProvider to current DataProviderManager.
        /// </summary>
        /// <param name="app">The app builder</param>
        /// <param name="dataProvider">The DataProvider</param>
        /// <returns>The app builder</returns>
        [Obsolete("Please use UseDataProvider instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static IAppBuilder AddDataProvider(this IAppBuilder app, C1.Data.DataProvider dataProvider)
        {
            C1.Data.DataProviderManager.Current.Providers.Add(dataProvider);
            return app;
        }

        /// <summary>
        /// Add DiskStorage to current StorageProviderManager.
        /// </summary>
        /// <param name="app">The <see cref="IAppBuilder"/></param>
        /// <param name="rootName">The name of the root</param>
        /// <param name="rootPath">The full path of the root</param>
        /// <returns>The <see cref="IAppBuilder"/></returns>
        [Obsolete("Please use UseStorageProvider instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static IAppBuilder AddDiskStorage(this IAppBuilder app, string rootName, string rootPath)
        {
            app.UseStorageProviders().AddDiskStorage(rootName, rootPath);
            return app;
        }

        /// <summary>
        /// Add DataSet to current DataProviderManager.
        /// </summary>
        /// <param name="app">The <see cref="IAppBuilder"/></param>
        /// <param name="name">The name of the dataset</param>
        /// <param name="dataSet">The dataset</param>
        /// <returns>The <see cref="IAppBuilder"/></returns>
        [Obsolete("Please use UseDataProvider instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static IAppBuilder AddDataSet(this IAppBuilder app, string name, IEnumerable dataSet)
        {
            app.UseDataProviders().AddItemsSource(name, dataSet);
            return app;
        }

        /// <summary>
        /// Add DataSet to current DataProviderManager.
        /// </summary>
        /// <param name="app">The <see cref="IAppBuilder"/></param>
        /// <param name="name">The name of the dataset</param>
        /// <param name="dataSetGetter">The dataset getter</param>
        /// <returns>The <see cref="IAppBuilder"/></returns>
        [Obsolete("Please use UseDataProvider instead.")]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public static IAppBuilder AddDataSet(this IAppBuilder app, string name, Func<IEnumerable> dataSetGetter)
        {
            app.UseDataProviders().AddItemsSource(name, dataSetGetter);
            return app;
        }
    }
}