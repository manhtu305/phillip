﻿using C1.Web.Api.Data;
using C1.Web.Api.Storage;

#if ASPNETCORE || NETCORE
using IAppBuilder = Microsoft.AspNetCore.Builder.IApplicationBuilder;
namespace Microsoft.AspNetCore.Builder
#else
namespace Owin
#endif
{
    /// <summary>
    /// Extension methods for <see cref="IAppBuilder"/>.
    /// </summary>
    public static partial class AppBuilderUseExtensions
    {
        /// <summary>
        /// Uses <see cref="DataProviderManager"/>.
        /// </summary>
        /// <param name="app">The application builder.</param>
        /// <returns>The <see cref="DataProviderManager"/>.</returns>
        public static DataProviderManager UseDataProviders(this IAppBuilder app)
        {
            return DataProviderManager.Current;
        }

        /// <summary>
        /// Uses <see cref="StorageProviderManager"/>.
        /// </summary>
        /// <param name="app">The application builder.</param>
        /// <returns>The <see cref="StorageProviderManager"/>.</returns>
        public static StorageProviderManager UseStorageProviders(this IAppBuilder app)
        {
            return StorageProviderManager.Current;
        }
    }
}
