﻿using System;
using System.Collections.Generic;

namespace C1.Web.Api.Configuration
{
    /// <summary>
    /// The base Manager class. It is used to manage the specified type instances.
    /// </summary>
    /// <typeparam name="T">The type of the instance which is managed.</typeparam>
    public class Manager<T>
    {
        private readonly Dictionary<string, T> _items = new Dictionary<string, T>(StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// Gets the map of the items which are maintained.
        /// </summary>
        public IDictionary<string, T> Items
        {
            get
            {
                return _items;
            }
        }

        /// <summary>
        /// Gets a boolean value which indicates whether the key exists in the manager.
        /// </summary>
        /// <param name="key">The item key.</param>
        /// <returns>A boolean value which indicates whether the key exists in the manager.</returns>
        public bool Contains(string key)
        {
            return Items.ContainsKey(key);
        }

        /// <summary>
        /// Gets the instance which is mapped to the specified key.
        /// </summary>
        /// <param name="key">The item key.</param>
        /// <returns>The item.</returns>
        public T Get(string key)
        {
            return Items[key];
        }

        /// <summary>
        /// Try to get the instance which is mapped to the specified key.
        /// </summary>
        /// <param name="key">The item key.</param>
        /// <param name="value">The item</param>
        /// <returns>true if contains an element with the specified key; otherwise, false.</returns>
        public bool TryGet(string key, out T value)
        {
            value = default(T);
            return Items.TryGetValue(key, out value);
        }

        /// <summary>
        /// Add item with the key to the manager.
        /// </summary>
        /// <param name="key">The item key.</param>
        /// <param name="item">The item.</param>
        public void Add(string key, T item)
        {
            Items.Add(key, item);
        }

        /// <summary>
        /// Remove the item with the specified key.
        /// </summary>
        /// <param name="key">The item key.</param>
        public void Remove(string key)
        {
            Items.Remove(key);
        }
    }
}
