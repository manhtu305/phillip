﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;

namespace C1.Configuration
{
    /// <summary>
    /// Defines the class for providing the specified type object conditionally.
    /// </summary>
    /// <typeparam name="T">The type of the object that current provides</typeparam>
    [Obsolete("This class is not used any more.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public abstract class ConditionalProvider<T>
    {
        /// <summary>
        /// Gets a boolean indicates whether current provider recognizes the name.
        /// </summary>
        /// <param name="name">The name of the object</param>
        /// <returns>A boolean indicates whether current provider recognizes the name.</returns>
        public abstract bool Supports(string name);

        /// <summary>
        /// Gets the object with the specified name and arguments.
        /// </summary>
        /// <param name="name">The name</param>
        /// <param name="args">The arguments</param>
        /// <returns>The object</returns>
        public abstract T GetObject(string name, NameValueCollection args);

        /// <summary>
        /// Gets the object with the specified name.
        /// </summary>
        /// <param name="name">The name</param>
        /// <returns>The object</returns>
        public T GetObject(string name)
        {
            return GetObject(name, null);
        }
    }
}
