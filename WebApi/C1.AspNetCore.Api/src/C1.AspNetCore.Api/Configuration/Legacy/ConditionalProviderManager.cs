﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.ComponentModel;

namespace C1.Configuration
{
    /// <summary>
    /// The manager of ConditionalProvider.
    /// </summary>
    /// <typeparam name="T">The type of object</typeparam>
    /// <typeparam name="TP">The type of ConditionalProvider</typeparam>
    [Obsolete("Please use C1.Web.Api.Configuration.Manager<T> class instead.")]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public class ConditionalProviderManager<T, TP> where TP:ConditionalProvider<T>
    {
        private readonly IList<TP> _providers = new List<TP>();

        /// <summary>
        /// Gets the object with the specified name and arguments.
        /// </summary>
        /// <param name="name">The name</param>
        /// <param name="args">The arguments</param>
        /// <returns>The object</returns>
        public virtual T GetObject(string name, NameValueCollection args)
        {
            var provider = GetSupportedProvider(name);
            if (provider == null)
            {
                throw new InvalidOperationException(
                    string.Format("Cannot find the file provider which supports to get the file named {0}.", name));
            }

            return provider.GetObject(name, args);
        }

        /// <summary>
        /// Gets the object with the specified name.
        /// </summary>
        /// <param name="name">The name</param>
        /// <returns>The object</returns>
        public virtual T GetObject(string name)
        {
            return GetObject(name, null);
        }

        private TP GetSupportedProvider(string name)
        {
            return Providers.FirstOrDefault(p => p.Supports(name));
        }

        /// <summary>
        /// Gets the providers.
        /// </summary>
        public IList<TP> Providers
        {
            get
            {
                return _providers;
            }
        }
    }
}