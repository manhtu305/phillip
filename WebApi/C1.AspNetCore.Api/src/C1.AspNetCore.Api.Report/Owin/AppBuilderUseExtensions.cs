﻿using C1.Web.Api.Report;
using System;

#if ASPNETCORE
using IAppBuilder = Microsoft.AspNetCore.Builder.IApplicationBuilder;
namespace Microsoft.AspNetCore.Builder
#else
namespace Owin
#endif
{
    /// <summary>
    /// Extension methods for <see cref="IAppBuilder"/>.
    /// </summary>
    public static class AppBuilderUseExtensions
    {
        /// <summary>
        /// Set a duration for the report cache.
        /// </summary>
        /// <param name="app">The app builder.</param>
        /// <param name="seconds">The duration.</param>
        /// <returns>The app builder.</returns>
        public static IAppBuilder SetReportCacheDuration(this IAppBuilder app, int seconds)
        {
            return app.SetReportCacheDuration(new TimeSpan(0, 0, seconds));
        }

        /// <summary>
        /// Set a duration for the report cache.
        /// </summary>
        /// <param name="app">The app builder.</param>
        /// <param name="duration">The duration.</param>
        /// <returns>The app builder.</returns>
        public static IAppBuilder SetReportCacheDuration(this IAppBuilder app, TimeSpan duration)
        {
            ReportCacheManager.Instance.CacheDuration = duration;
            return app;
        }

        /// <summary>
        /// Gets the current <see cref="ReportProviderManager"/> instance.
        /// </summary>
        /// <param name="app">The app builder.</param>
        /// <returns>The current <see cref="ReportProviderManager"/>.</returns>
        public static ReportProviderManager UseReportProviders(this IAppBuilder app)
        {
            return ReportProviderManager.Current;
        }
    }
}
