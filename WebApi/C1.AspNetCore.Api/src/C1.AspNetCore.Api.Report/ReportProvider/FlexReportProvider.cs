﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using C1.Web.Api.Report.Models;
using C1.Win.C1Document;
using C1.Web.Api.Storage;
using C1.Win.FlexReport;
using C1.Web.Api.Document;

namespace C1.Web.Api.Report
{
    /// <summary>
    /// Represents the provider for FlexReport.
    /// </summary>
    public class FlexReportProvider : ReportProvider
    {
        /// <summary>
        /// Initializes a new instance of <see cref="FlexReportProvider"/> instance.
        /// </summary>
        /// <param name="storage">The <see cref="IStorageProvider"/> used to access the FlexReport file.</param>
        public FlexReportProvider(IStorageProvider storage)
        {
            if (storage == null)
                throw new ArgumentNullException("storage");
            
            Storage = storage;
        }

        /// <summary>
        /// Gets the <see cref="IStorageProvider"/> used to access the FlexReport file.
        /// </summary>
        public IStorageProvider Storage { get; private set; }

        /// <summary>
        /// Creates the document for the specified report path.
        /// </summary>
        /// <param name="path">The relative path of the report.</param>
        /// <param name="args">The collection of HTTP query string variables.</param>
        /// <returns>A <see cref="C1FlexReport"/> created for the specified path.</returns>
        protected override C1DocumentSource CreateDocument(string path, NameValueCollection args)
        {
            if (Storage == null) return null;

            // get the file storage
            string filePath, reportName;
            PathUtil.SeparatePathByLast(path, out filePath, out reportName);
            var fileStorage = Storage.GetFileStorage(filePath, args) as IFileStorage;

            if (fileStorage == null || !fileStorage.Exists) return null;

            // load report from file
            var report = new C1FlexReport();
            using (var stream = fileStorage.Read())
            {
                report.Load(stream, reportName);
            }

            return report;
        }

        /// <summary>
        /// Gets catalog info of the specified path.
        /// </summary>
        /// <param name="providerName">The key used to register the provider.</param>
        /// <param name="path">The relative path of the folder or report.</param>
        /// <param name="recursive">Whether to return the entire tree of child items below the specified item.</param>
        /// <param name="args">The collection of HTTP query string variables.</param>
        /// <returns>A <see cref="CatalogItem"/> object.</returns>
        public override CatalogItem GetCatalogItem(string providerName, string path, bool recursive, NameValueCollection args)
        {
            if (Storage == null) return null;

            path = path.TrimEnd('/');

            return GetFolderNode(providerName, path, recursive, args) ??
                GetFileNode(providerName, path, args) ??
                GetReportNode(providerName, path, args);
        }

        private CatalogItem GetFolderNode(string providerName, string path, bool recursive, NameValueCollection args)
        {
            var folder = Storage.GetDirectoryStorage(path, args);
            if (folder == null || !folder.Exists) return null;

            var fullRootPath = PathUtil.Combine(providerName, path);
            string parentFolder, folderName;
            PathUtil.SeparatePathByLast(path, out parentFolder, out folderName);

            var root = new CatalogItem();
            root.Type = CatalogItemType.Folder;
            root.Name = folderName;
            root.Path = fullRootPath;
            var nodes = root.Items;

            var subFolders = folder.GetDirectories();
            foreach (var subFolder in subFolders)
            {
                CatalogItem folderNode;
                if (recursive)
                {
                    folderNode = GetFolderNode(providerName, subFolder, recursive, args);
                }
                else
                {
                    folderNode = new CatalogItem
                    {
                        Name = subFolder,
                        Path = PathUtil.Combine(fullRootPath, subFolder),
                        Type = CatalogItemType.Folder
                    };
                }

                nodes.Add(folderNode);
            }

            var files = folder.GetFiles(false, "*.flxr").Union(folder.GetFiles(false, "*.xml"));
            foreach (var file in files)
            {
                var fileNode = GetFileNode(providerName, PathUtil.Combine(path, file), args);
                if (fileNode != null)
                {
                    nodes.Add(fileNode);
                }
            }

            return root;
        }

        private CatalogItem GetFileNode(string providerName, string path, NameValueCollection args)
        {
            var file = Storage.GetFileStorage(path, args);
            if (file == null || !file.Exists) return null;

            using (var stream = file.Read())
            {
                var reportNames = C1FlexReport.GetReportList(stream);
                if (reportNames == null || reportNames.Length == 0) return null;

                string folderPath, fileName;
                PathUtil.SeparatePathByLast(file.Name, out folderPath, out fileName);
                var filePath = PathUtil.Combine(providerName, file.Name);
                var fileNode = new CatalogItem { Name = fileName, Path = filePath, Type = CatalogItemType.File };

                foreach (var reportName in reportNames)
                {
                    var reportPath = PathUtil.Combine(filePath, reportName);
                    var node = new CatalogItem { Name = reportName, Path = reportPath, Type = CatalogItemType.Report };
                    fileNode.Items.Add(node);
                }

                return fileNode;
            }
        }

        private CatalogItem GetReportNode(string providerName, string path, NameValueCollection args)
        {
            string filePath, reportName;
            PathUtil.SeparatePathByLast(path, out filePath, out reportName);

            var fileNode = GetFileNode(providerName, filePath, args);
            if (fileNode == null) return null;

            return fileNode.Items.FirstOrDefault(item => string.Equals(item.Name, reportName, StringComparison.OrdinalIgnoreCase));
        }
    }
}
