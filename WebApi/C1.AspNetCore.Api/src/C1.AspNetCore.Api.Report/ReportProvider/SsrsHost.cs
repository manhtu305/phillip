﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace C1.Web.Api.Report
{
    /// <summary>
    /// Defines SSRS authentication types.
    /// </summary>
    /// <remarks>
    /// These values correspond to <b>Authentication</b> section in <b>rsreportserver.config</b> file.
    /// </remarks>
    public enum AuthenticationType
    {
        /// <summary>
        /// Use this setting if SSRS supports following authentication types:
        /// <b>RSWindowsNegotiate</b>, <b>RSWindowsKerberos</b>.
        /// </summary>
        Negotiate,
        /// <summary>
        /// Use this setting if SSRS supports: <b>RSWindowsNTLM</b>.
        /// </summary>
        Ntlm,
        /// <summary>
        /// Use this setting if SSRS supports: <b>RSWindowsBasic</b>.
        /// </summary>
        Basic,
    }

    /// <summary>
    /// Represents the SSRS host.
    /// </summary>
    public class SsrsHost
    {
        private AuthenticationType _authenticationType = AuthenticationType.Ntlm;
        private int _timeout = 600000;

        /// <summary>
        /// Initializes a new instance of <see cref="SsrsHost"/>.
        /// </summary>
        /// <param name="webServiceUrl">The url of the SSRS web service.</param>
        public SsrsHost(string webServiceUrl)
        {
            WebServiceUrl = webServiceUrl;
        }

        /// <summary>
        /// Gets the url of the SSRS web service.
        /// </summary>
        public string WebServiceUrl { get; private set; }

        /// <summary>
        /// Gets or sets the type of authentication used in SSRS connection.
        /// </summary>
        public AuthenticationType AuthenticationType
        {
            get { return _authenticationType; }
            set { _authenticationType = value; }
        }

        /// <summary>
        /// Gets or sets credential used in SSRS connection.
        /// </summary>
        public NetworkCredential Credential { get; set; }

        /// <summary>
        /// Gets or sets the number of milliseconds to wait for server communications.
        /// </summary>
        public int Timeout
        {
            get { return _timeout; }
            set { _timeout = value; }
        }
    }
}
