﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using C1.Win.C1Document;
using C1.Web.Api.Report.Models;

namespace C1.Web.Api.Report
{
    /// <summary>
    /// Represents the custom provider which allows to customize the creation of document.
    /// </summary>
    /// <remarks>
    /// Use this provider to create the document by yourself.
    /// </remarks>
    public class CustomReportProvider : ReportProvider
    {
        private readonly Func<string, NameValueCollection, C1DocumentSource> _documentCreater;
        private readonly Func<string, string, bool, NameValueCollection, Models.CatalogItem> _getCatalogInfoFunc;

        /// <summary>
        /// Initializes a new instance of <see cref="CustomReportProvider"/> object.
        /// </summary>
        /// <param name="documentCreater">
        /// The function to create the document for the specified path.
        /// The first parameter of the function represents the relative path of the document.
        /// The second parameter of the function represents the collection of HTTP query string variables.
        /// </param>
        /// <param name="getCatalogInfoFunc">
        /// The function to get the catalog info of the specified folder or report.
        /// The first parameter of the function reprents the key used to register the provider.
        /// The second parameter of the function represents the relative path of the folder or report.
        /// The third parameter of the function represents whether to return the entire tree of child items below the specified item.
        /// The fourth parameter of the function represents the collection of HTTP query string variables.
        /// </param>
        public CustomReportProvider(Func<string, NameValueCollection, C1DocumentSource> documentCreater, 
            Func<string, string, bool, NameValueCollection, CatalogItem> getCatalogInfoFunc = null)
        {
            _documentCreater = documentCreater;
            _getCatalogInfoFunc = getCatalogInfoFunc;
        }

        /// <summary>
        /// Creates the document for the specified report path.
        /// </summary>
        /// <param name="path">The relative path of the report.</param>
        /// <param name="args">The collection of HTTP query string variables.</param>
        /// <returns>A <see cref="C1DocumentSource"/> created for the specified path.</returns>
        protected override C1DocumentSource CreateDocument(string path, NameValueCollection args)
        {
            return _documentCreater == null ? null : _documentCreater(path, args);
        }

        /// <summary>
        /// Gets catalog info of the specified path.
        /// </summary>
        /// <param name="providerName">The key used to register the provider.</param>
        /// <param name="path">The relative path of the folder or report.</param>
        /// <param name="recursive">Whether to return the entire tree of child items below the specified item.</param>
        /// <param name="args">The collection of HTTP query string variables.</param>
        /// <returns>A <see cref="CatalogItem"/> object.</returns>
        public override CatalogItem GetCatalogItem(string providerName, string path, bool recursive, NameValueCollection args)
        {
            if (_getCatalogInfoFunc == null) return null;
            return _getCatalogInfoFunc(providerName, path, recursive, args);
        }
    }
}
