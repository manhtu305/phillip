﻿using C1.Web.Api.Configuration;
using C1.Web.Api.Document;
using C1.Web.Api.Storage;
using System;
using System.Collections.Specialized;
using System.Net;

namespace C1.Web.Api.Report
{
    /// <summary>
    /// The manager for report providers.
    /// </summary>
    public sealed class ReportProviderManager : Manager<ReportProvider>
    {
        private readonly static ReportProviderManager _current = new ReportProviderManager();

        private ReportProviderManager()
        {
        }

        /// <summary>
        /// Gets the current report provider manager.
        /// </summary>
        public static ReportProviderManager Current
        {
            get { return _current; }
        }

        #region add provider

        /// <summary>
        /// Add <see cref="SsrsHost"/> for SSRS report to current <see cref="ReportProviderManager"/>.
        /// </summary>
        /// <param name="key">The provider key act as the alias name of the SSRS host.</param>
        /// <param name="host">The <see cref="SsrsHost"/>.</param>
        /// <returns>The <see cref="ReportProviderManager"/>.</returns>
        public ReportProviderManager AddSsrsReportHost(string key, SsrsHost host)
        {
            Add(key, new SsrsReportProvider(host));
            return this;
        }

        /// <summary>
        /// Add an SSRS host for SSRS report to current <see cref="ReportProviderManager"/>.
        /// </summary>
        /// <param name="key">The provider key act as the alias name of the SSRS host.</param>
        /// <param name="serviceUrl">The url of the SSRS web service.</param>
        /// <param name="credential">The credential used in SSRS connection.</param>
        /// <param name="authenticationType">The type of authentication used in SSRS connection.</param>
        /// <param name="timeout">The number of milliseconds to wait for server communications.</param>
        /// <returns>The <see cref="ReportProviderManager"/>.</returns>
        public ReportProviderManager AddSsrsReportHost(string key, string serviceUrl,
            NetworkCredential credential = null, AuthenticationType authenticationType = AuthenticationType.Ntlm,
            int timeout = 600000)
        {
            var host = new SsrsHost(serviceUrl) { AuthenticationType = authenticationType, Credential = credential, Timeout = timeout };
            return AddSsrsReportHost(key, host);
        }

        /// <summary>
        /// Add an <see cref="IStorageProvider"/> for FlexReport to current <see cref="ReportProviderManager"/>.
        /// </summary>
        /// <param name="key">The provider key.</param>
        /// <param name="storage">The <see cref="IStorageProvider"/>.</param>
        /// <returns>The <see cref="ReportProviderManager"/>.</returns>
        public ReportProviderManager AddFlexReportStorage(string key, IStorageProvider storage)
        {
            var provider = new FlexReportProvider(storage);
            Add(key, provider);
            return this;
        }

        /// <summary>
        /// Add a disk storage for FlexReport to current <see cref="ReportProviderManager"/>.
        /// </summary>
        /// <param name="key">The provider key.</param>
        /// <param name="rootPath">The full path of the root.</param>
        /// <returns>The <see cref="ReportProviderManager"/>.</returns>
        public ReportProviderManager AddFlexReportDiskStorage(string key, string rootPath)
        {
            var storage = new DiskStorageProvider(rootPath);
            return AddFlexReportStorage(key, storage);
        }

        #endregion

        #region get provider

        /// <summary>
        /// Creates a <see cref="Models.Report"/> for the specified path.
        /// </summary>
        /// <param name="fullPath">The full path of the report.</param>
        /// <param name="args">The collection of HTTP query string variables.</param>
        /// <returns>A <see cref="Models.Report"/> object created for the specified path.</returns>
        /// <remarks>
        /// The fullPath argument starts with the provider key.
        /// </remarks>
        internal Models.Report CreateReport(string fullPath, NameValueCollection args)
        {
            string key, path;
            var provider = GetProvider(fullPath, out key, out path);
            return provider == null ? null : provider.CreateReport(key, path, args);
        }

        /// <summary>
        /// Gets the provider for the specified path.
        /// </summary>
        /// <param name="fullPath">The full path of the report.</param>
        /// <param name="key">The key of the provider.</param>
        /// <param name="path">The relative path of the report.</param>
        /// <returns>The <see cref="ReportProvider"/> for the specified path.</returns>
        /// <remarks>
        /// The first part of the fullPath is taken as the key. 
        /// If there is no provider registed with the key. Will try to find the provider which is registerd with empty string.
        /// If there is no provider found in the current <see cref="ReportProviderManager"/>, 
        /// will try to find the <see cref="IStorageProvider"/> in the current <see cref="StorageProviderManager"/>.
        /// </remarks>
        internal ReportProvider GetProvider(string fullPath, out string key, out string path)
        {
            // take first part as the key
            PathUtil.SeparatePathByFirst(fullPath, out key, out path);
            if(!Contains(key) && Contains(""))
            {
                key = "";
                path = fullPath;
            }

            // has the provider in itself
            if (Contains(key))
            {
                return Get(key);
            }

            // try to find in storage provider
            var storageProvider = StorageProviderManager.Current.GetProvider(fullPath, out key, out path);
            return storageProvider == null ? null : new FlexReportProvider(storageProvider);
        }

        #endregion
    }
}
