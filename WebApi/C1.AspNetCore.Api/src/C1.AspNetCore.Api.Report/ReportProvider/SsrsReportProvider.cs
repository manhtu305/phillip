﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using C1.Web.Api.Report.Models;
using RS4 = C1.Win.ImportServices.ReportingService4;
using System.Threading;
using C1.Win.C1Document;
using C1.Web.Api.Document;

namespace C1.Web.Api.Report
{
    /// <summary>
    /// Represents the provider for SSRS report.
    /// </summary>
    public class SsrsReportProvider : ReportProvider
    {
        /// <summary>
        /// Initializes a new instance of <see cref="SsrsReportProvider"/>.
        /// </summary>
        /// <param name="host">The <see cref="SsrsHost"/> used to access the SSRS.</param>
        public SsrsReportProvider(SsrsHost host)
        {
            if (host == null)
                throw new ArgumentNullException("host");

            Host = host;
        }

        /// <summary>
        /// Gets the <see cref="SsrsHost"/> used to access the SSRS.
        /// </summary>
        public SsrsHost Host { get; private set; }

        /// <summary>
        /// Creates the document for the specified report path.
        /// </summary>
        /// <param name="path">The relative path of the report.</param>
        /// <param name="args">The collection of HTTP query string variables.</param>
        /// <returns>A <see cref="C1DocumentSource"/> created for the specified path.</returns>
        /// <remarks>
        /// The path argument is not starts with "/". Should add "/" at the beginning of the path, on connecting with the SSRS.
        /// </remarks>
        protected override C1DocumentSource CreateDocument(string path, NameValueCollection args)
        {
            if (Host == null) return null;

            // the path should starts with "/".
            var reportPath = path.StartsWith("/") ? path : "/" + path;

            try
            {
                var doc = new C1SSRSDocumentSource();
                doc.ConnectionOptions.AuthenticationType = (C1.Win.C1Ssrs.AuthenticationType)Host.AuthenticationType;
                doc.ConnectionOptions.Credential = Host.Credential;
                doc.ConnectionOptions.Timeout = Host.Timeout;
                doc.DocumentLocation = new SSRSReportLocation(Host.WebServiceUrl, reportPath);

                return doc;
            }
            catch(Exception ex)
            {
                if (IsFaultException(ex))
                {
                    return null;
                }

                throw ex;
            }
        }

        /// <summary>
        /// Gets catalog info of the specified path.
        /// </summary>
        /// <param name="providerName">The key used to register the provider.</param>
        /// <param name="path">The relative path of the folder or report.</param>
        /// <param name="recursive">Whether to return the entire tree of child items below the specified item.</param>
        /// <param name="args">The collection of HTTP query string variables.</param>
        /// <returns>A <see cref="CatalogItem"/> object.</returns>
        /// <remarks>
        /// The path argument is not starts with "/". Should add "/" at the beginning of the path, on connecting with the SSRS.
        /// </remarks>
        public override CatalogItem GetCatalogItem(string providerName, string path, bool recursive, NameValueCollection args)
        {
            if (Host == null) return null;

            // the path should starts with "/".
            var folderPath = path.TrimEnd('/');
            if (!folderPath.StartsWith("/"))
            {
                folderPath = "/" + folderPath;
            }

            try
            {
                using (var rs = new C1.Win.C1Ssrs.ServerConnection())
                {
                    rs.SetConnectionOptions(Host.WebServiceUrl, new Win.C1Ssrs.ConnectionOptions(Host.Credential));
                    var itemType = rs.GetItemType(folderPath, CancellationToken.None);
                    switch (itemType)
                    {
                        case RS4.ItemTypeEnum.Folder:
                            return GetFolderNode(providerName, folderPath, recursive);
                        case RS4.ItemTypeEnum.Report:
                            return GetReportNode(providerName, folderPath);
                        default:
                            return null;
                    }
                }
            }
            catch (Exception ex)
            {
                // invalid path
                if (IsFaultException(ex))
                {
                    return null;
                }

                throw ex;
            }
        }

        private CatalogItem GetReportNode(string providerName, string reportPath)
        {
            // try to load the report
            var doc = new C1SSRSDocumentSource();
            doc.ConnectionOptions.AuthenticationType = (C1.Win.C1Ssrs.AuthenticationType)Host.AuthenticationType;
            doc.ConnectionOptions.Credential = Host.Credential;
            doc.DocumentLocation = new SSRSReportLocation(Host.WebServiceUrl, reportPath);
            doc.ValidateParameters();

            // with no exceptions
            string folderName, reportName;
            PathUtil.SeparatePathByLast(reportPath, out folderName, out reportName);

            var node = new CatalogItem();
            node.Type = CatalogItemType.Report;
            node.Name = reportName;
            node.Path = PathUtil.Combine(providerName, reportPath);
            return node;
        }

        private CatalogItem GetFolderNode(string providerName, string folderPath, bool recursive)
        {
            using (var rs = new C1.Win.C1Ssrs.ServerConnection())
            {
                rs.SetConnectionOptions(Host.WebServiceUrl, new Win.C1Ssrs.ConnectionOptions(Host.Credential));

                var items = rs.ListChildren(folderPath, recursive, CancellationToken.None);
                var allItems = from item in items
                               where !item.Hidden && (item.Type == RS4.ItemTypeEnum.Folder || item.Type == RS4.ItemTypeEnum.Report)
                               orderby item.Type, item.Path, item.Name
                               select item;

                string parentFolder, folderName;
                PathUtil.SeparatePathByLast(folderPath, out parentFolder, out folderName);

                var root = new CatalogItem();
                root.Type = CatalogItemType.Folder;
                root.Name = folderName;
                root.Path = PathUtil.Combine(providerName, folderPath);

                var nodes = root.Items;
                var level = (folderPath == "/") ? 1 : GetPathLevel(folderPath) + 1;
                AddItems(nodes, allItems, providerName, level);

                return root;
            }
        }

        private void AddItems(IList<CatalogItem> nodes, IEnumerable<RS4.CatalogItem> allItems, string providerName, int level = 1)
        {
            foreach(var levelItem in allItems.Where(item=>GetPathLevel(item.Path)==level))
            {
                var path = PathUtil.Combine(providerName, levelItem.Path);

                if (levelItem.Type == RS4.ItemTypeEnum.Folder)
                {
                    // remove confiction of "/A" and "/AAA"
                    var itemPath = levelItem.Path + "/";
                    var node = new CatalogItem { Name = levelItem.Name, Path = path, Type = CatalogItemType.Folder };
                    nodes.Add(node);

                    var hasChildren = allItems.Any(item => item.Type == RS4.ItemTypeEnum.Report && item.Path.StartsWith(itemPath));
                    if (hasChildren)
                    {
                        var subItems = allItems.Where(item => item.Path.StartsWith(itemPath));
                        AddItems(node.Items, subItems, providerName, level + 1);
                    }
                }
                else
                {
                    var node = new CatalogItem { Name = levelItem.Name, Path = path, Type = CatalogItemType.Report };
                    nodes.Add(node);
                }
            }
        }

        private static int GetPathLevel(string path)
        {
            return path.Count(c => c == '/');
        }

        internal static bool IsFaultException(Exception ex)
        {
#if !ASPNETCORE
            return ex is System.ServiceModel.FaultException;
#else
            return ex.GetType().FullName == "System.ServiceModel.FaultException";
#endif
        }
    }
}
