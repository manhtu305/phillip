﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using C1.Web.Api.Report.Models;
using C1.Win.C1Document;
using System.Collections.Specialized;
using C1.Web.Api.Document;

namespace C1.Web.Api.Report
{
    /// <summary>
    /// Represents the provider for report.
    /// </summary>
    public abstract class ReportProvider
    {
        /// <summary>
        /// Gets catalog info of the specified path.
        /// </summary>
        /// <param name="providerName">The key used to register the provider.</param>
        /// <param name="path">The relative path of the folder or report.</param>
        /// <param name="recursive">Whether to return the entire tree of child items below the specified item.</param>
        /// <param name="args">The collection of HTTP query string variables.</param>
        /// <returns>A <see cref="CatalogItem"/> object.</returns>
        public abstract CatalogItem GetCatalogItem(string providerName, string path, bool recursive, NameValueCollection args = null);

        /// <summary>
        /// Creates the document for the specified report path.
        /// </summary>
        /// <param name="path">The relative path of the report.</param>
        /// <param name="args">The collection of HTTP query string variables.</param>
        /// <returns>A <see cref="C1DocumentSource"/> created for the specified path.</returns>
        /// <remarks>
        /// If overiding this method, you can create the document by yourself, or modify the document created by base.
        /// </remarks>
        protected abstract C1DocumentSource CreateDocument(string path, NameValueCollection args);

        /// <summary>
        /// Creates the report for the specified report path.
        /// </summary>
        /// <param name="providerName">The key used to register the provider.</param>
        /// <param name="path">The relative path of the report.</param>
        /// <param name="args">The collection of HTTP query string variables.</param>
        /// <returns>A <see cref="Models.Report"/> created for the specified path.</returns>
        internal Models.Report CreateReport(string providerName, string path, NameValueCollection args)
        {
            // get the document source.
            var document = CreateDocument(path, args);

            // it's a flex report.
            var flexReportDoc = document as C1.Win.FlexReport.C1FlexReport;
            if (flexReportDoc != null)
            {
                return new FlexReport(flexReportDoc, PathUtil.Combine(providerName, path));
            }

            // it's an SSRS report.
            var ssrsReportDoc = document as C1.Win.C1Document.C1SSRSDocumentSource;
            if (ssrsReportDoc != null)
            {
                return new SsrsReport(ssrsReportDoc, PathUtil.Combine(providerName, path));
            }

            return null;
        }
    }
}
