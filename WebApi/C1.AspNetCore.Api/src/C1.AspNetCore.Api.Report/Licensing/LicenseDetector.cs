﻿using C1.Util.Licensing;
using System.ComponentModel;

namespace C1.Web.Api.Report
{
    /// <summary>
    /// Define a class for detecting C1.Web.Api.Report license.
    /// </summary>
    [LicenseProvider]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public sealed class LicenseDetector : BaseLicenseDetector
    {
        /// <summary>
        /// The constructor.
        /// </summary>
        public LicenseDetector()
        {
        }
    }

#if ASPNETCORE

    internal class LicenseProviderAttribute : BaseLicenseProviderAttribute
    {
        public override string RunTimeKey
        {
            get
            {
                return LicenseManager.Key;
            }
        }
    }

    /// <summary>
    /// The license manager.
    /// </summary>
    public static class LicenseManager
    {
        /// <summary>
        /// The run time license key.
        /// </summary>
        public static string Key;
    }

#endif
}
