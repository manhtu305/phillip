﻿using C1.Web.Api.Document.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace C1.Web.Api.Report.Models
{
    /// <summary>
    /// Represents the information of the report.
    /// </summary>
    public class ReportInfo
    {
        internal ReportInfo(Report report)
        {
            HasParameters = report.HasParameters;
            Path = report.Path;
            PageSettings = report.PageSettings;
        }

        /// <summary>
        /// Gets the path of the report.
        /// </summary>
        public string Path { get; private set; }
        /// <summary>
        /// Gets a boolean value indicating whhether the report has parameters.
        /// </summary>
        public bool HasParameters { get; private set; }
        /// <summary>
        /// Gets the default page settings of the report.
        /// </summary>
        public PageSettings PageSettings { get; private set; }
        /// <summary>
        /// Gets the location to getting parameters.
        /// </summary>
        public string ParametersLocation { get; internal set; }
        /// <summary>
        /// Gets the location to getting page settings.
        /// </summary>
        public string PageSettingsLocation { get; internal set; }
        /// <summary>
        /// Gets the location to getting supported formats.
        /// </summary>
        public string SupportedFormatsLocation { get; internal set; }
        /// <summary>
        /// Gets the location to create new report instance.
        /// </summary>
        public string CreationLocation { get; internal set; }
    }

    /// <summary>
    /// Represents the information of the report instance.
    /// </summary>
    public class ReportExecutionInfo : ExecutionInfo
    {
        private ReportStatus _status;
        internal ReportExecutionInfo(Cache<Report> cache, Report report, bool detailed = true) : base(cache, report, detailed)
        {
            Id = cache.Id;
            HasParameters = report.HasParameters;
            if (detailed)
            {
                _status = new ReportStatus(cache, report);
            }
        }

        /// <summary>
        /// Gets the cache id for the report instance.
        /// </summary>
        public string Id { get; private set; }
        /// <summary>
        /// Gets a boolean value indicating whether the report has parameters.
        /// </summary>
        public bool HasParameters { get; private set; }
        /// <summary>
        /// Gets the location to get parameters.
        /// </summary>
        public string ParametersLocation { get; internal set; }
        /// <summary>
        /// Gets the status of document.
        /// </summary>
        public override DocumentStatus Status
        {
            get
            {
                return _status;
            }
        }
    }
}
