﻿using C1.Web.Api.Storage;
using C1.Util.Licensing;
using C1.Web.Api.Document.Models;
using C1.Web.Api.Report.Localization;
using C1.Win.C1Document;
using C1.Win.C1Document.Export;
using C1.Win.FlexReport;
using System.Drawing;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace C1.Web.Api.Report.Models
{
    internal class FlexReportFeatures : IDocumentFeatures
    {
        public bool NonPaginated { get { return false; } }

        public bool Paginated { get { return true; } }

        public bool TextSearchInPaginatedMode { get { return true; } }

        public bool PageSettings { get { return true; } }
    }

    internal class FlexReport : Report
    {
        private readonly static IDocumentFeatures _features = new FlexReportFeatures();
        private readonly C1FlexReport _flexReport;

        public FlexReport(C1FlexReport report, string path)
            : base(path)
        {
            if (report == null)
                throw new ArgumentNullException("report");

            _flexReport = report;
            DocumentSource = report;
        }

        internal override IDocumentFeatures GetFeatures()
        {
            return _features;
        }

        internal override void SetLanguage(CultureInfo language)
        {
            if(_flexReport.Culture == null)
            {
                _flexReport.Culture = language;
            }
        }

        protected override void InternalLoad()
        {
            _flexReport.ReportError += _flexReport_ReportError;
            _flexReport.SecurityError += _flexReport_SecurityError;

            if (NeedRenderEvalInfo)
            {
                DrawEvalInfo(_flexReport);
            }
        }

        private void _flexReport_SecurityError(object sender, SecurityErrorEventArgs e)
        {
            ErrorList.Add(string.Format(Resources.SecurityError, e.Path, e.FailedAttemptCount, e.Retry));
        }

        private void _flexReport_ReportError(object sender, ReportErrorEventArgs e)
        {
            ErrorList.Add(e.Exception.Message);
        }

        private static void DrawEvalInfo(C1FlexReport report)
        {
            var width = report.PageSettings.Width.Convert(UnitTypeEnum.Dip);
            width = width - Unit.Convert(report.Layout.MarginLeft, UnitTypeEnum.Twip, UnitTypeEnum.Dip) -
                    Unit.Convert(report.Layout.MarginRight, UnitTypeEnum.Twip, UnitTypeEnum.Dip);

            var height = report.PageSettings.Height.Convert(UnitTypeEnum.Dip);
            height = height - Unit.Convert(report.Layout.MarginTop, UnitTypeEnum.Twip, UnitTypeEnum.Dip) -
                     Unit.Convert(report.Layout.MarginBottom, UnitTypeEnum.Twip, UnitTypeEnum.Dip);

            var image = new Bitmap((int)width, (int)height);
            LicenseHelper.DrawEvalInfo(image);
            report.Overlay.Picture = image;
        }

        internal override IAsyncActionWithProgress<double> InternalRenderAsync()
        {
            return _flexReport.RenderAsyncEx();
        }
    }
}
