﻿using C1.Web.Api.Document.Models;
using C1.Web.Api.Report.Localization;
using C1.Win.C1Document;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace C1.Web.Api.Report.Models
{
    internal abstract class Report : Document.Models.Document
    {
        private const string CustomActionPrefix = "CA:";
        private IAsyncActionWithProgress<double> _renderingProcess;
        //IAsyncOperationWithProgress<C1BookmarkPosition, double> _customActionProcess;
        private IList<Parameter> _originalParameters;
        private DocumentPosition _initialPosition;

        public Report(string path)
            : base(path)
        {
        }

        internal override Type LicenseDetectorType
        {
            get
            {
                return typeof(LicenseDetector);
            }
        }

        public bool HasParameters
        {
            get
            {
                return DocumentSource != null
                    && DocumentSource.Parameters != null
                    && DocumentSource.Parameters.Count > 0;
            }
        }

        public DocumentPosition InitialPosition
        {
            get { return _initialPosition; }
        }

        internal abstract void SetLanguage(CultureInfo language);

        internal void SetParameterByName(string name, string[] value)
        {
            var parameter = DocumentSource.Parameters.FirstOrDefault(p => string.Equals(p.Name, name, StringComparison.OrdinalIgnoreCase));
            if (parameter == null)
            {
                throw new NotFoundException();
            }

            var values = new Dictionary<string, string[]>(StringComparer.OrdinalIgnoreCase);
            values[name] = value;
            UpdateParameters(values);
        }

        internal void InitParameters(IDictionary<string, string[]> parameters)
        {
            if(parameters!=null && parameters.Count > 0)
            {
                UpdateParameters(parameters);
            }

            var originalParameters = GetParameters();
            if (originalParameters != null)
            {
                _originalParameters = originalParameters.ToList();
            }
        }

        internal void SetParameters(IDictionary<string, string[]> parameters)
        {
            UpdateParameters(parameters, false, true);
        }

        internal void UpdateParameters(IDictionary<string, string[]> parameters, bool force = false, bool useDefault = false)
        {
            if (!HasParameters)
            {
                return;
            }

            var errors = new Dictionary<string, string>();
            if (parameters != null && DocumentSource.Parameters.Count > 0)
            {
                foreach (var p in DocumentSource.Parameters)
                {
                    if (!parameters.ContainsKey(p.Name))
                    {
                        if (useDefault && _originalParameters != null)
                        {
                            var originParameter = _originalParameters.FirstOrDefault(op => string.Equals(op.Name, p.Name, StringComparison.OrdinalIgnoreCase));
                            if (originParameter != null)
                            {
                                p.Value = originParameter.Value;
                            }
                        }

                        continue;
                    }

                    object value;
                    if (TryParseParameter(parameters[p.Name], p.DataType, p.Nullable, p.MultiValue, out value))
                    {
                        p.Value = value;
                        continue;
                    }

                    if (force)
                    {
                        p.Value = parameters[p.Name];
                        continue;
                    }

                    errors[p.Name] = Resources.InvalidParameters;
                }
            }

            var validateResult = DocumentSource.ValidateParameters();
            if (validateResult != null)
            {
                validateResult.ForEach(e =>
                {
                    errors[e.Parameter.Name] = e.ErrorMessage;
                });
            }

            var newParameters = GetParameters().ToList();
            foreach (var p in newParameters)
            {
                string error;
                if (errors.TryGetValue(p.Name, out error))
                {
                    p.Error = error;
                }
            }

            // force is true if need reset parameters on trying resolve parameter errors.
            // we need try to resolve parameter errors only once; or, it will be endless loop. (TFS:300215)
            if (!force)
            {
                TryResolveParameterErrors(newParameters);
            }

            OnDocumentUpdating();
        }

        private void TryResolveParameterErrors(IEnumerable<Parameter> parameters)
        {
            var needResetParameters = new Dictionary<string, string[]>();
            foreach (var p in parameters)
            {
                if (string.IsNullOrEmpty(p.Error) || p.Value == null || p.AllowedValues == null
                    || !p.AllowedValues.Any())
                {
                    continue;
                }

                var allowedValues = p.AllowedValues.Select(a => a.Value).ToList();
                if (!p.MultiValue)
                {
                    if (!allowedValues.Contains(p.Value))
                    {
                        needResetParameters.Add(p.Name, null);
                    }

                    continue;
                }

                var values = p.Value as IEnumerable;
                if (values == null)
                {
                    continue;
                }

                foreach (var value in values)
                {
                    if (!allowedValues.Contains(value))
                    {
                        needResetParameters.Add(p.Name, null);
                        break;
                    }
                }
            }

            if (needResetParameters.Any())
            {
                UpdateParameters(needResetParameters, true);
            }
        }

        private static bool TryParseParameter(string[] value, ParameterType type, bool nullable, bool multiValue, out object result)
        {
            result = null;
            if (value != null && value.Length > 0)
            {
                if (multiValue)
                {
                    var results = new object[value.Length];
                    for (var i = 0; i < value.Length; i++)
                    {
                        if (!TryParseString(value[i], type, nullable, out results[i]))
                        {
                            return false;
                        }
                    }
                    result = results;
                    return true;
                }
                else
                {
                    return TryParseString(value[0], type, nullable, out result);
                }
            }
            else
            {
                return nullable;
            }
        }

        private static bool TryParseString(string value, ParameterType type, bool nullable, out object result)
        {
            result = null;
            if (string.IsNullOrEmpty(value) && nullable)
            {
                return true;
            }

            switch (type)
            {
                case ParameterType.Boolean:
                    bool boolVal;
                    if (bool.TryParse(value, out boolVal))
                    {
                        result = boolVal;
                        return true;
                    }
                    break;
                case ParameterType.Float:
                    float floatVal;
                    if (float.TryParse(value, out floatVal))
                    {
                        result = floatVal;
                        return true;
                    }
                    break;
                case ParameterType.Integer:
                    int intVal;
                    if (int.TryParse(value, out intVal))
                    {
                        result = intVal;
                        return true;
                    }
                    break;
                case ParameterType.Date:
                case ParameterType.Time:
                case ParameterType.DateTime:
                    DateTime dateTimeVal;
                    if (DateTime.TryParse(value, out dateTimeVal))
                    {
                        result = dateTimeVal;
                        return true;
                    }
                    break;
                case ParameterType.String:
                    result = value;
                    return true;
            }

            return false;
        }

        internal Parameter GetParameterByName(string name)
        {
            if (!HasParameters)
            {
                return null;
            }

            TryValidateParameters();
            System.Threading.Thread.Sleep(100);

            var parameter = DocumentSource.Parameters.FirstOrDefault(p => string.Equals(p.Name, name, StringComparison.OrdinalIgnoreCase));
            return parameter == null ? null : CreateParameter(parameter);
        }

        internal IEnumerable<Parameter> GetParameters()
        {
            if (!HasParameters)
            {
                return null;
            }

            TryValidateParameters();

            System.Threading.Thread.Sleep(100);

            return DocumentSource.Parameters.Select(CreateParameter);
        }

        private Parameter CreateParameter(C1.Win.C1Document.Parameter p)
        {
            var para = new Parameter();
            para.Name = p.Name;
            para.DataType = p.DataType;
            para.Nullable = p.Nullable;
            para.Value = p.Value;
            para.Hidden = p.Hidden;
            para.MultiValue = p.MultiValue;
            para.Prompt = p.Prompt;
            para.AllowBlank = p.AllowBlank;
            para.MaxLength = p.MaxLength;

            if (p.AllowedValues != null)
            {
                para.AllowedValues = new List<KeyValuePair<string, object>>();
                p.AllowedValues.ToList().ForEach(v =>
                {
                    para.AllowedValues.Add(new KeyValuePair<string, object>(v.Label, v.Value));
                });
            }

            return para;
        }

        private bool TryValidateParameters()
        {
            try
            {
                DocumentSource.ValidateParameters();
                foreach(var parameter in DocumentSource.Parameters)
                {
                    if(parameter.Value==null && !parameter.Nullable && parameter.AllowBlank && parameter.DataType == ParameterType.String)
                    {
                        parameter.Value = "";
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        internal void Render()
        {
            if (DocumentSource == null) return;

            ErrorList.Clear();

            lock (DocumentSource)
            {
                _renderingProcess = InternalRenderAsync();
            }

            if (_renderingProcess.Status == AsyncStatus.Completed)
            {
                Status = ExecutingStatus.Completed;
            }
            else
            {
                Status = ExecutingStatus.Rendering;
                _renderingProcess.Completed += _renderingProcess_Completed;
                _renderingProcess.Progress += _renderingProcess_Progress;
            }
        }

        internal abstract IAsyncActionWithProgress<double> InternalRenderAsync();

        private void _renderingProcess_Progress(IAsyncActionWithProgress<double> asyncInfo, double progressInfo)
        {
            Progress = progressInfo;
        }

        private void _renderingProcess_Completed(IAsyncActionWithProgress<double> asyncInfo, AsyncStatus asyncStatus)
        {
            Status = ExecutingStatus.Completed;
        }

        protected override void OnCustomActionStarting(C1DocumentAction action)
        {
            _initialPosition = null;
            Status = ExecutingStatus.Rendering;
        }

        protected override void OnCustomActionCompleted(C1DocumentAction action, DocumentPosition position)
        {
            base.OnCustomActionCompleted(action, position);
            _initialPosition = position;
        }

        internal override void Stop()
        {
            if (Status == ExecutingStatus.Rendering)
            {
                _renderingProcess.Cancel();
                base.Stop();
            }
        }

        internal override List<SearchResult> Search(int startPageIndex, C1.Web.Api.Document.Models.SearchScope scope, SearchOptions searchOptions)
        {
            if (Status == ExecutingStatus.Completed || Status == ExecutingStatus.Stopped)
            {
                return base.Search(startPageIndex, scope, searchOptions);
            }

            return null;
        }
    }
}
