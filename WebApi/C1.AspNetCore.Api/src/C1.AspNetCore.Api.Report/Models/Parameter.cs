﻿using C1.Win.C1Document;
using System.Collections.Generic;

namespace C1.Web.Api.Report.Models
{
    /// <summary>
    /// Represents the user-defined parameter.
    /// </summary>
    public class Parameter
    {
        /// <summary>
        /// Gets the name of the parameter.
        /// </summary>
        public string Name { get; internal set; }
        /// <summary>
        /// Gets the type of the parameter.
        /// </summary>
        public ParameterType DataType { get; internal set; }
        /// <summary>
        /// Gets a boolean value indicating whether the value of the parameter can be null.
        /// </summary>
        /// <remarks>
        /// Cannot be true if this is a multi-value parameter.
        /// </remarks>
        public bool Nullable { get; internal set; }
        /// <summary>
        /// Gets a list of supported values for the parameter.
        /// </summary>
        public List<KeyValuePair<string, object>> AllowedValues { get; internal set; }
        /// <summary>
        /// Gets the value of the parameter.
        /// </summary>
        /// <remarks>
        /// Value can be specified as array if it's a multi-value parameter.
        /// In this case, all items should have same type.
        /// </remarks>
        public object Value { get; internal set; }
        /// <summary>
        /// Gets a boolean value indicating whether the parameter should not be displayed to the user.
        /// </summary>
        public bool Hidden { get; internal set; }
        /// <summary>
        /// Gets a boolean value indicating whether this is a multi-value parameter.
        /// </summary>
        public bool MultiValue { get; internal set; }
        /// <summary>
        /// Gets the prompt to display when asking for parameter value.
        /// </summary>
        public string Prompt { get; internal set; }
        /// <summary>
        /// Gets the error which occurs after the parameter is set.
        /// </summary>
        public string Error { get; internal set; }
        /// <summary>
        /// Gets a boolean value indicating whether the value of this parameter can be the empty string.
        /// </summary>
        /// <remarks>
        /// Ignored for non-string parameters.
        /// </remarks>
        public bool AllowBlank { get; internal set; }
        /// <summary>
        /// Gets the maximum length of a string parameter.
        /// </summary>
        /// <remarks>0 means unlimited length. Ignored for non-string parameters.</remarks>
        public int MaxLength { get; internal set; }
    }
}