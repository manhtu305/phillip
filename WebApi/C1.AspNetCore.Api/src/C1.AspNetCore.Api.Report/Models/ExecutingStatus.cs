﻿namespace C1.Web.Api.Report.Models
{
    /// <summary>
    /// The status of the executing report.
    /// </summary>
    internal static class ExecutingStatus
    {
        /// <summary>
        /// The report is Loaded.
        /// </summary>
        public const string Loaded = "Loaded";
        /// <summary>
        /// The report is rendering.
        /// </summary>
        public const string Rendering = "Rendering";
        /// <summary>
        /// The report is rendered.
        /// </summary>
        public const string Completed = "Completed";
        /// <summary>
        /// The report rendering is stopped.
        /// </summary>
        public const string Stopped = "Stopped";
        /// <summary>
        /// The execution is not found.
        /// </summary>
        public const string NotFound = "NotFound";
    }
}
