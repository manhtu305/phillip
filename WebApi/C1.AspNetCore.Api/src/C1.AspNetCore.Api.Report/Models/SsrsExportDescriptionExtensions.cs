﻿using C1.Web.Api.Document.Models;
using C1.Win.C1Document;
using C1.Win.C1Document.Export;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace C1.Web.Api.Report.Models
{
    internal static class SsrsExportDescriptionExtensions
    {
        private static readonly Unit c_DefWidth = new Unit(8.5, UnitTypeEnum.Inch);
        private static readonly Unit c_DefHeight = new Unit(11, UnitTypeEnum.Inch);
        private static readonly Unit c_DefMargin = new Unit(1, UnitTypeEnum.Inch);

        internal static ExportDescription ToSsrsExportDescriptor(this ExportProvider provider)
        {
            var desc = new ExportDescription();
            desc.Name = provider.FormatName;
            desc.Format = provider.DefaultExtension;

            switch (desc.Format)
            {
                case "html":
                    SetHtmlOptionDescriptions(desc.OptionDescriptions, provider);
                    break;
                case "pdf":
                    SetPdfOptionDescriptions(desc.OptionDescriptions, provider);
                    break;
                case "doc":
                case "docx":
                    SetWordOptionDescriptions(desc.OptionDescriptions, provider);
                    break;
                case "xls":
                case "xlsx":
                    SetExcelOptionDescriptions(desc.OptionDescriptions, provider);
                    break;
                case "mhtml":
                    SetMhtmlOptionDescriptions(desc.OptionDescriptions, provider);
                    break;
                case "csv":
                    SetCsvOptionDescriptions(desc.OptionDescriptions, provider);
                    break;
                case "png":
                    SetPngOptionDescriptions(desc.OptionDescriptions, provider);
                    break;
                case "tiff":
                case "bmp":
                case "emf":
                case "gif":
                case "jpeg":
                    SetImageOptionDescriptions(desc.OptionDescriptions, provider);
                    break;
            }

            return desc;
        }

        private static void SetWordOptionDescriptions(List<ExportOptionDescription> opts, ExportProvider provider)
        {
            // outputRange: All
            opts.Add(new ExportOptionDescription("autoFit", "string", "Default", "", new[] { "False", "True", "Never", "Default" }));
            opts.Add(new ExportOptionDescription("expandToggles", "bool", false));
            opts.Add(new ExportOptionDescription("fixedPageWidth", "bool", false));
            opts.Add(new ExportOptionDescription("omitHyperlinks", "bool", false));
            opts.Add(new ExportOptionDescription("omitDrillthroughs", "bool", false));
        }

        private static void SetExcelOptionDescriptions(List<ExportOptionDescription> opts, ExportProvider provider)
        {
            // outputRange: All
            opts.Add(new ExportOptionDescription("omitDocumentMap", "bool", false));
            opts.Add(new ExportOptionDescription("omitFormulas", "bool", false));
            opts.Add(new ExportOptionDescription("simplePageHeaders", "bool", false));
        }

        private static void SetMhtmlOptionDescriptions(List<ExportOptionDescription> opts, ExportProvider provider)
        {
            // outputRange: All
            opts.Add(new ExportOptionDescription("outlookCompat", "bool", true));
        }

        private static void SetCsvOptionDescriptions(List<ExportOptionDescription> opts, ExportProvider provider)
        {
            // outputRange: All
            opts.Add(new ExportOptionDescription("encoding", "string", "UTF8", "", new[] { "UTF8", "UTF7", "ASCII", "Unicode" }));
            opts.Add(new ExportOptionDescription("excelMode", "bool", true));
            opts.Add(new ExportOptionDescription("fieldDelimiter", "string", ","));
            opts.Add(new ExportOptionDescription("fileExtension", "string", ".csv"));
            opts.Add(new ExportOptionDescription("noHeader", "bool", false));
            opts.Add(new ExportOptionDescription("qualifier", "string", "\""));
            opts.Add(new ExportOptionDescription("recordDelimiter", "string", "\r\n"));
            opts.Add(new ExportOptionDescription("supressLineBreaks", "bool", false));
            opts.Add(new ExportOptionDescription("useFormattedValues", "bool", null, "", null, true));
        }

        private static void SetPngOptionDescriptions(List<ExportOptionDescription> opts, ExportProvider provider)
        {
            // false means exporting from the cached pdf file, used to export thumbnails
            opts.Add(new ExportOptionDescription("serverSize", "bool", true));

            // outputRange: All, PageList, PageRange
            opts.AddOutputRangeDescription(OutputRangeTypes.All | OutputRangeTypes.PageList | OutputRangeTypes.PageRange);

            #region serverSide=true
            // valid when serverSide=true
            SetPaginatedOptionDescriptions(opts, provider);
            #endregion

            #region serverSide=false
            // valid when serverSide=false
            opts.Add(new ExportOptionDescription("resolution", "float", 300f));
            #endregion
        }

        private static void SetImageOptionDescriptions(List<ExportOptionDescription> opts, ExportProvider provider)
        {
            // outputRange: All, PageList, PageRange
            opts.AddOutputRangeDescription(OutputRangeTypes.All | OutputRangeTypes.PageList | OutputRangeTypes.PageRange);

            SetPaginatedOptionDescriptions(opts, provider);
        }

        private static void SetPdfOptionDescriptions(List<ExportOptionDescription> opts, ExportProvider provider)
        {
            opts.Add(new ExportOptionDescription("serverSide", "bool", true));

            #region serverSide=true
            // valid when serverSide=true
            // outputRange: All, PageRange
            opts.AddOutputRangeDescription(OutputRangeTypes.All | OutputRangeTypes.PageRange);
            opts.Add(new ExportOptionDescription("humanReadablePdf", "bool", false));
            SetPaginatedOptionDescriptions(opts, provider);
            #endregion

            #region serverSide=false
            // valid when serverSide=false
            // outputRange: All, PageRange, PageList
            //opts.AddOutputRangeDescription(OutputRangeTypes.All | OutputRangeTypes.PageRange | OutputRangeTypes.PageList);
            opts.Add(new ExportOptionDescription("pdfACompatible", "bool", true));
            opts.Add(new ExportOptionDescription("embedFonts", "bool", true));
            opts.Add(new ExportOptionDescription("useCompression", "bool", true));
            //PdfSecurityOptions
            opts.Add(new ExportOptionDescription("allowCopyContent", "bool", true, "documentRestrictions"));
            opts.Add(new ExportOptionDescription("allowEditAnnotations", "bool", true, "documentRestrictions"));
            opts.Add(new ExportOptionDescription("allowEditContent", "bool", true, "documentRestrictions"));
            opts.Add(new ExportOptionDescription("allowPrint", "bool", true, "documentRestrictions"));
            opts.Add(new ExportOptionDescription("ownerPassword", "string", string.Empty, "passwordSecurity"));
            opts.Add(new ExportOptionDescription("userPassword", "string", string.Empty, "passwordSecurity"));
            opts.Add(new ExportOptionDescription("encryptionType", "string", "Standard40", "passwordSecurity", new List<string> { "NotPermit", "Standard40", "Standard128", "Aes128" }));
            // documentInfo
            opts.AddDocumentInfoDescription(provider);
            #endregion
        }

        private static void SetHtmlOptionDescriptions(List<ExportOptionDescription> opts, ExportProvider provider)
        {
            // outputRange: All, PageRange, PageList
            opts.AddOutputRangeDescription(OutputRangeTypes.All | OutputRangeTypes.PageRange | OutputRangeTypes.PageRange);

            opts.AddPagedDescription();

            opts.Add(new ExportOptionDescription("showNavigator", "bool", false));
            opts.Add(new ExportOptionDescription("navigatorPosition", "string", "Left", "", new[] { "Left", "Right" }));
            opts.Add(new ExportOptionDescription("singleFile", "bool", true));
        }

        private static void SetPaginatedOptionDescriptions(List<ExportOptionDescription> opts, ExportProvider provider)
        {
            opts.Add(new ExportOptionDescription("columns", "int", null, "", null, true));
            opts.Add(new ExportOptionDescription("columnSpacing", "unit", null, "", null, true));

            // page settings
            // hidden option, false means override the default pagesettings with following options
            //opts.Add(new ExportOptionDescription("_useDefaultPageSettings", "bool", "true"));
            opts.Add(new ExportOptionDescription("pageWidth", "unit", c_DefWidth));
            opts.Add(new ExportOptionDescription("pageHeight", "unit", c_DefHeight));
            opts.Add(new ExportOptionDescription("marginLeft", "unit", c_DefMargin));
            opts.Add(new ExportOptionDescription("marginTop", "unit", c_DefMargin));
            opts.Add(new ExportOptionDescription("marginRight", "unit", c_DefMargin));
            opts.Add(new ExportOptionDescription("marginBottom", "unit", c_DefMargin));
        }

        private static void AddPagedDescription(this List<ExportOptionDescription> opts)
        {
            opts.Add(new ExportOptionDescription("paged", "bool", false));
        }

        private static void AddOutputRangeDescription(this List<ExportOptionDescription> opts, OutputRangeTypes rangeTypes)
        {
            var allowValues = new List<string>();
            if (rangeTypes.HasFlag(OutputRangeTypes.All))
            {
                allowValues.Add("All");
            }
            if (rangeTypes.HasFlag(OutputRangeTypes.PageRange))
            {
                allowValues.Add("PageRange");
            }
            if (rangeTypes.HasFlag(OutputRangeTypes.PageList))
            {
                allowValues.Add("PageList");
            }

            opts.Add(new ExportOptionDescription("outputRange", "string", "All", "outputRange", allowValues));
            opts.Add(new ExportOptionDescription("outputRangeInverted", "bool", false, "outputRange"));
        }

        private static void AddDocumentInfoDescription(this List<ExportOptionDescription> opts, ExportProvider provider)
        {
            if (provider.SupportedDocumentInfoFields == C1.Win.C1Document.DocumentInfoFields.None)
                return;

            var DocumentInfoFields = System.Enum.GetValues(typeof(DocumentInfoFields));
            foreach (DocumentInfoFields field in DocumentInfoFields)
            {
                if (provider.SupportedDocumentInfoFields.HasFlag(field) && field != Win.C1Document.DocumentInfoFields.None)
                {
                    var fieldName = System.Enum.GetName(typeof(DocumentInfoFields), field).ToCamelCase();
                    opts.Add(new ExportOptionDescription(fieldName, "string", string.Empty, "documentInfo"));
                }
            }
        }

        private static string ToCamelCase(this string str)
        {
            if (!string.IsNullOrEmpty(str))
            {
                return str.Substring(0, 1).ToLower() + str.Substring(1);
            }

            return str;
        }

        [Flags]
        private enum OutputRangeTypes
        {
            All = 1,
            PageList = 2,
            PageRange = 4
        }
    }
}
