﻿using C1.Web.Api.Document.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using C1.Win.C1Document.Export;
using C1.Win.C1Document.Export.Ssrs;
using C1.Win.C1Document;
using C1.C1Pdf;

namespace C1.Web.Api.Report.Models
{
    internal class SsrsHtmlExportFilterOptions : ExportFilterOptions
    {
        public SsrsHtmlExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        public override string Extension
        {
            get
            {
                return SingleFile ? "html" : "zip";
            }
        }

        public override string FilterExtension
        {
            get
            {
                return "html";
            }
        }

        private bool SingleFile
        {
            get { return GetBooleanValue("singleFile", true); }
        }

        protected override ExportFilter CreateExportFilter()
        {
            // in non-paginated mode, use the SvgExporter to export the C1Page to SVG.
            // in paginated mode, use the HtmlFilter to export C1PdfDocumentSource to SVG.
            var paged = GetBooleanValue("paged", false);
            var filter = paged ? (HtmlFilterBase)new HtmlFilter() : new SvgExporter();

            filter.Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted);
            filter.ShowNavigator = GetBooleanValue("showNavigator", false);
            filter.SingleFile = GetBooleanValue("singleFile", true);
            filter.NavigatorPosition = GetEnumValue<HtmlFilter.NavigatorPositions>("navigatorPosition", HtmlFilter.NavigatorPositions.Left);

            return filter;
        }
    }

    internal class SsrsMhtmlExportFilterOptions : ExportFilterOptions
    {
        public SsrsMhtmlExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        public override string Extension
        {
            get
            {
                return "mhtml";
            }
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new MhtmlExporter();
            filter.OutlookCompat = GetBooleanValue("outlookCompat", true);
            return filter;
        }
    }

    internal class SsrsCsvExportFilterOptions : ExportFilterOptions
    {
        public SsrsCsvExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        public override string Extension
        {
            get
            {
                return "csv";
            }
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new CsvExporter();

            filter.Encoding = GetEnumValue<CsvExporterEncoding>("encoding", CsvExporterEncoding.UTF8);
            filter.ExcelMode = GetBooleanValue("excelMode", true);
            filter.FieldDelimiter = GetStringValue("fieldDelimiter", ",");
            filter.FileExtension = GetStringValue("fileExtension", ".csv");
            filter.NoHeader = GetBooleanValue("noHeader", false);
            filter.Qualifier = GetStringValue("qualifier", "\"");
            filter.RecordDelimiter = GetStringValue("recordDelimiter", "\r\n");
            filter.SuppressLineBreaks = GetBooleanValue("supressLineBreaks", false);
            filter.UseFormattedValues = GetNullableBooleanValue("useFormattedValues", null);

            return filter;
        }
    }

    internal abstract class SsrsWordExportFilterOptions : ExportFilterOptions
    {
        public SsrsWordExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        protected void ApplyWordOptions(WordExporter filter)
        {
            filter.AutoFit = GetEnumValue<WordTableAutoFitMode>("autoFit", WordTableAutoFitMode.Default);
            filter.ExpandToggles = GetBooleanValue("expandToggles", false);
            filter.FixedPageWidth = GetBooleanValue("fixedPageWidth", false);
            filter.OmitHyperlinks = GetBooleanValue("omitHyperlinks", false);
            filter.OmitDrillthroughs = GetBooleanValue("omitDrillthroughs", false);
        }
    }

    internal class SsrsDocExportFilterOptions : SsrsWordExportFilterOptions
    {
        public SsrsDocExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        public override string Extension
        {
            get
            {
                return "doc";
            }
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new WordExporter();
            filter.OpenXmlDocument = false;
            ApplyWordOptions(filter);
            return filter;
        }
    }

    internal class SsrsDocxExportFilterOptions : SsrsWordExportFilterOptions
    {
        public SsrsDocxExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        public override string Extension
        {
            get
            {
                return "docx";
            }
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new WordExporter();
            filter.OpenXmlDocument = true;
            ApplyWordOptions(filter);
            return filter;
        }
    }

    internal abstract class SsrsExcelExportFilterOptions : ExportFilterOptions
    {
        public SsrsExcelExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        protected void ApplyExcelOptions(ExcelExporter filter)
        {
            filter.OmitDocumentMap = GetBooleanValue("omitDocumentMap", false);
            filter.OmitFormulas = GetBooleanValue("omitFormulas", false);
            filter.SimplePageHeaders = GetBooleanValue("simplePageHeaders", false);
        }
    }

    internal class SsrsXlsExportFilterOptions : SsrsExcelExportFilterOptions
    {
        public SsrsXlsExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        public override string Extension
        {
            get
            {
                return "xls";
            }
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new ExcelExporter
            {
                OmitDocumentMap = GetBooleanValue("omitDocumentMap", false),
                OmitFormulas = GetBooleanValue("omitFormulas", false),
                SimplePageHeaders = GetBooleanValue("simplePageHeaders", false)
            };
            filter.OpenXmlDocument = false;
            return filter;
        }
    }

    internal class SsrsXlsxExportFilterOptions : SsrsExcelExportFilterOptions
    {
        public SsrsXlsxExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        public override string Extension
        {
            get
            {
                return "xlsx";
            }
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new ExcelExporter();
            filter.OpenXmlDocument = true;
            return filter;
        }
    }

    internal abstract class SsrsPaginatedExportFilterOptions : ExportFilterOptions
    {
        public SsrsPaginatedExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        internal void ApplyPaginatedOptions(PaginatedExporter filter)
        {
            // defualt null means use default settings in report.
            filter.Columns = GetNullableIntValue("columns", null);

            // default null means use default settings in report.
            filter.ColumnSpacing = GetNullableUnit("columnSpacing", null);

            // page settings
            // use hidden _useDefaultPageSettings property to group all pagesettings properties
            if (!GetBooleanValue("_useDefaultPageSettings", true))
            {
                var pageSettings = new C1PageSettings();
                pageSettings.Width = GetUnit("pageWidth", new Unit(8.5, UnitTypeEnum.Inch));
                pageSettings.Height = GetUnit("pageHeight", new Unit(11, UnitTypeEnum.Inch));
                pageSettings.LeftMargin = GetUnit("marginLeft", new Unit(1, UnitTypeEnum.Inch));
                pageSettings.TopMargin = GetUnit("marginTop", new Unit(1, UnitTypeEnum.Inch));
                pageSettings.RightMargin = GetUnit("marginRight", new Unit(1, UnitTypeEnum.Inch));
                pageSettings.BottomMargin = GetUnit("marginBottom", new Unit(1, UnitTypeEnum.Inch));

                // default is null, means using default pagesettings defined in report.
                filter.PageSettings = pageSettings;
            }
        }
    }

    internal abstract class SsrsImageExportFilterOptions : SsrsPaginatedExportFilterOptions
    {
        public SsrsImageExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        private OutputRange OutputRange
        {
            get
            {
                return GetOutputRange(null, null);
            }
        }

        private bool MultiFile
        {
            get
            {
                int page;
                return !(OutputRange != null && int.TryParse(OutputRange.ToString(), out page));
            }
        }

        internal abstract string ImageExtension { get; }

        public override string Extension
        {
            get
            {
                return MultiFile ? "zip" : ImageExtension;
            }
        }

        public override ExportFilter ToExportFilter(string tempFolder)
        {
            var filter = base.ToExportFilter(tempFolder);
            filter.UseZipForMultipleFiles = MultiFile;
            return filter;
        }
    }

    internal class SsrsPngExportFilterOptions : SsrsImageExportFilterOptions
    {
        public SsrsPngExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        internal override string ImageExtension
        {
            get
            {
                return "png";
            }
        }

        protected override ExportFilter CreateExportFilter()
        {
            // serverSide=true: export via SSRS server.
            // serverSide=false: export via local C1PdfDocumentSource for paginated mode. Thumbnails using this way.
            return CreateSsrsExportFilter() ?? CreateCommonExportFilter();
        }

        private ExportFilter CreateSsrsExportFilter()
        {
            var serverSide = GetBooleanValue("serverSide", true);
            if (!serverSide) return null;

            var filter = new PngExporter();
            filter.Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted);
            ApplyPaginatedOptions(filter);
            return filter;
        }

        private ExportFilter CreateCommonExportFilter()
        {
            var filter = new PngFilter();
            filter.Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted);
            filter.Resolution = GetFloatValue("resolution", 300f);
            return filter;
        }
    }

    internal class SsrsTiffExportFilterOptions : SsrsPaginatedExportFilterOptions
    {
        public SsrsTiffExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        public override string Extension
        {
            get
            {
                return "tiff";
            }
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new TiffExporter();
            filter.Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted);
            ApplyPaginatedOptions(filter);
            return filter;
        }
    }

    internal class SsrsBmpExportFilterOptions : SsrsImageExportFilterOptions
    {
        public SsrsBmpExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        internal override string ImageExtension
        {
            get
            {
                return "bmp";
            }
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new BmpExporter();
            filter.Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted);
            ApplyPaginatedOptions(filter);
            return filter;
        }
    }

    internal class SsrsGifExportFilterOptions : SsrsImageExportFilterOptions
    {
        public SsrsGifExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        internal override string ImageExtension
        {
            get
            {
                return "gif";
            }
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new GifExporter();
            filter.Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted);
            ApplyPaginatedOptions(filter);
            return filter;
        }
    }

    internal class SsrsJpegExportFilterOptions : SsrsImageExportFilterOptions
    {
        public SsrsJpegExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        internal override string ImageExtension
        {
            get
            {
                return "jpeg";
            }
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new JpegExporter();
            filter.Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted);
            ApplyPaginatedOptions(filter);
            return filter;
        }
    }

    internal class SsrsEmfExportFilterOptions : SsrsImageExportFilterOptions
    {
        public SsrsEmfExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        internal override string ImageExtension
        {
            get
            {
                return "emf";
            }
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new EmfExporter();
            filter.Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted);
            ApplyPaginatedOptions(filter);
            return filter;
        }
    }

    internal class SsrsPdfExportFilterOptions : SsrsPaginatedExportFilterOptions
    {
        public SsrsPdfExportFilterOptions(IDictionary<string, string> options)
            : base(options)
        {
        }

        public override string Extension
        {
            get
            {
                return "pdf";
            }
        }

        protected override ExportFilter CreateExportFilter()
        {
            var filter = new C1.Win.C1Document.Export.Ssrs.PdfExporter();

            filter.ServerSide = GetBooleanValue("serverSide", true);
            filter.Range = GetOutputRange(OutputRange.All, OutputRange.AllInverted);

            if (filter.ServerSide)
            {
                filter.HumanReadablePDF = GetBooleanValue("humanReadablePdf", false);
                ApplyPaginatedOptions(filter);
            }
            else
            {
                filter.EmbedFonts = GetBooleanValue("embedFonts", false);
                filter.PdfACompatible = GetBooleanValue("pdfACompatible", true);
                filter.UseCompression = GetBooleanValue("useCompression", true);

                //PdfSecurityOptions
                filter.PdfSecurityOptions.AllowCopyContent = GetBooleanValue("allowCopyContent", true);
                filter.PdfSecurityOptions.AllowEditAnnotations = GetBooleanValue("allowEditAnnotations", true);
                filter.PdfSecurityOptions.AllowEditContent = GetBooleanValue("allowEditContent", true);
                filter.PdfSecurityOptions.AllowPrint = GetBooleanValue("allowPrint", true);
                filter.PdfSecurityOptions.OwnerPassword = GetStringValue("ownerPassword", string.Empty);
                filter.PdfSecurityOptions.UserPassword = GetStringValue("userPassword", string.Empty);
                filter.PdfSecurityOptions.EncryptionType = GetEnumValue<C1.C1Pdf.PdfEncryptionType>("encryptionType", PdfEncryptionType.Standard40);

                //documentInfo
                filter.DocumentInfo = GetDocumentInfo();
            }

            return filter;
        }
    }
}
