﻿using C1.Web.Api.Document.Models;
using System;

namespace C1.Web.Api.Report.Models
{
    /// <summary>
    /// Represents the status of the report instance.
    /// </summary>
    public class ReportStatus : DocumentStatus
    {
        internal ReportStatus(ICache cache, Report report) : base(cache, report)
        {
            InitialPosition = report.InitialPosition;
        }

        /// <summary>
        /// Gets the initialized position should be navigated to after the report is rendered.
        /// </summary>
        public DocumentPosition InitialPosition { get; private set; }
    }
}