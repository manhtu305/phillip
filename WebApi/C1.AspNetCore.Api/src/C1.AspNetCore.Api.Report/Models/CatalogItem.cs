﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace C1.Web.Api.Report.Models
{
    /// <summary>
    /// Represents the report catalog item.
    /// </summary>
    public class CatalogItem
    {
        private IList<CatalogItem> _items;

        /// <summary>
        /// Gets or sets the short name of the item.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the full path of the item.
        /// </summary>
        /// <remarks>
        /// The path is does not starts with "/". It's a full path which beginning with the registered report provider key.
        /// </remarks>
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets the type of the item.
        /// </summary>
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Include)]
        public CatalogItemType Type { get; set; }

        /// <summary>
        /// Gets the collection of child items.
        /// </summary>
        public IList<CatalogItem> Items
        {
            get
            {
                return _items ?? (_items = new List<CatalogItem>());
            }
        }
    }

    /// <summary>
    /// Indicates the type of the report catalog item.
    /// </summary>
    public enum CatalogItemType
    {
        /// <summary>
        /// It's a folder.
        /// </summary>
        Folder,
        /// <summary>
        /// It's a report definition file.
        /// </summary>
        File,
        /// <summary>
        /// It's an SSRS report or the name of the FlexReport defined in the report definition file.
        /// </summary>
        Report,
        /// <summary>
        /// Unknown type.
        /// </summary>
        Unknown = 100
    }
}
