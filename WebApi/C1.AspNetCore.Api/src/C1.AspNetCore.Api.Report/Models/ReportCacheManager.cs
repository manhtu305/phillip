﻿namespace C1.Web.Api.Report
{
    internal class ReportCacheManager : CacheManager<Models.Report>
    {
        public static readonly ReportCacheManager Instance = new ReportCacheManager();

        private ReportCacheManager() { }
    }
}
