﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using C1.Win.C1Document;
using C1.Win.C1Document.Export;
using C1.Web.Api.Report.Localization;
using C1.Web.Api.Document.Models;
using SsrsExporter = C1.Win.C1Document.Export.Ssrs.Exporter;
using C1.Win.C1Document.Export.Ssrs;
using System.Globalization;
using System.IO;

namespace C1.Web.Api.Report.Models
{
    /// <summary>
    /// The document for SSRS report.
    /// </summary>
    /// <remarks>
    /// It manages the paginated in different ways.
    /// non-paginated mode: Use C1SsrsDocumentSource as the document source.
    /// paginated mode: Use the exported pdf file as the docuemnt source. 
    /// (The paginated C1SsrsDocumentSource does not support svg exporting.)
    /// </remarks>
    internal class SsrsReportFeatures : IDocumentFeatures
    {
        public bool NonPaginated { get { return true; } }

        public bool Paginated { get { return true; } }

        public bool TextSearchInPaginatedMode { get { return false; } }

        public bool PageSettings { get { return true; } }
    }

    internal class SsrsReport : Report
    {
        private readonly static IDocumentFeatures _features = new SsrsReportFeatures();
        private readonly C1SSRSDocumentSource _ssrsReport;
        private readonly PaginatedSsrsReport _paginatedDocument;
        private bool _paginated;
        private readonly object _bookmarkLockObj = new object();

        public SsrsReport(C1SSRSDocumentSource report, string path):
            base(path)
        {
            if (report == null)
                throw new ArgumentNullException("document");

            _ssrsReport = report;
            DocumentSource = report;
            _paginated = report.Paginated;
            _paginatedDocument = new PaginatedSsrsReport(this, path);
        }

        public override string Name
        {
            get
            {
                var name = base.Name;
                var index = name.LastIndexOf('/');
                if (index > -1)
                {
                    name = name.Substring(index + 1);
                }
                return name;
            }
        }

        internal override IDocumentFeatures GetFeatures()
        {
            return _features;
        }

        internal override void SetLanguage(CultureInfo language)
        {
            _ssrsReport.Language = language;
        }

        internal override IEnumerable<ExportDescription> GetSupportedFormats()
        {
            // The SvgExporter is not included by C1SSRSDocumentSource
            return DocumentSource.SupportedExportProviders
                .Union(new[] { C1SSRSDocumentSource.SvgExportProvider })
                .Select(p => p.ToSsrsExportDescriptor());
        }

        internal override ExportDescription GetSupportedFormatByName(string name)
        {
            var format = DocumentSource.SupportedExportProviders
                .Union(new[] { C1SSRSDocumentSource.SvgExportProvider })
                .FirstOrDefault(p => string.Equals(p.DefaultExtension, name, StringComparison.OrdinalIgnoreCase));
            return format == null ? null : format.ToSsrsExportDescriptor();
        }

        #region Paginated Related

        internal override bool Paginated
        {
            get { return _paginated; }
            set
            {
                // does not set C1SSRSDocumentSource.Paginated property,
                // will manage the paginated mode by PaginatedSsrsReport class.
                _paginated = value;
            }
        }

        public override bool HasOutlines
        {
            get
            {
                // hope C1PdfDocumentSource supports outlines, but now not supporting.
                return _paginated? _paginatedDocument.HasOutlines: base.HasOutlines;
            }
        }

        public override int PageCount
        {
            get
            {
                return _paginated ? _paginatedDocument.PageCount : base.PageCount;
            }
        }

        internal override void InternalExport(ExportFilter exportFilter)
        {
            // the export filter can be:
            // 1. SsrsExporter, export vis SSRS
            // 2. SvgExporter, export non-paginated C1Page to SVG
            // 3. HtmlFilter, export paginated C1PdfDocumentSource to SVG

            if (exportFilter is SsrsExporter || exportFilter is SvgExporter)
                base.InternalExport(exportFilter);
            else
                _paginatedDocument.InternalExport(exportFilter);
        }

        internal override List<SearchResult> Search(int startPageIndex, C1.Web.Api.Document.Models.SearchScope scope, SearchOptions searchOptions)
        {
            // hope C1PdfDocumentSource supports searching, but now not supporting.
            return _paginated ? _paginatedDocument.Search(startPageIndex, scope, searchOptions) : base.Search(startPageIndex, scope, searchOptions);
        }

        internal override DocumentPosition GetBookmark(string bookmark)
        {
            if (_paginated)
            {
                // hope C1PdfDocumentSource supports bookmark, but now not supporting.
                return _paginatedDocument.GetBookmark(bookmark);
            }
            else
            {
                // C1SSRSDocumentSource.InternalGetPage() does not support multi-threads.
                lock (_bookmarkLockObj)
                {
                    return base.GetBookmark(bookmark);
                }
            }
        }

        internal override List<Document.Models.OutlineNode> GetOutlines()
        {
            // hope C1PdfDocumentSource supports outlines, but now not supporting.
            return _paginated ? _paginatedDocument.GetOutlines() : base.GetOutlines();
        }

        #endregion

        internal override ExportFilterOptions GetExportFilterOptions(IDictionary<string, string> options)
        {
            string format;
            if (options != null && options.TryGetValue("format", out format))
            {
                switch (format.ToLower())
                {
                    case "pdf":
                        return new SsrsPdfExportFilterOptions(options);
                    case "html":
                        return new SsrsHtmlExportFilterOptions(options);
                    case "doc":
                        return new SsrsDocExportFilterOptions(options);
                    case "docx":
                        return new SsrsDocxExportFilterOptions(options);
                    case "xls":
                        return new SsrsXlsExportFilterOptions(options);
                    case "xlsx":
                        return new SsrsXlsxExportFilterOptions(options);
                    case "tiff":
                        return new SsrsTiffExportFilterOptions(options);
                    case "bmp":
                        return new SsrsBmpExportFilterOptions(options);
                    case "png":
                        return new SsrsPngExportFilterOptions(options);
                    case "jpeg":
                        return new SsrsJpegExportFilterOptions(options);
                    case "gif":
                        return new SsrsGifExportFilterOptions(options);
                    case "emf":
                        return new SsrsEmfExportFilterOptions(options);
                    case "csv":
                        return new SsrsCsvExportFilterOptions(options);
                    case "mhtml":
                        return new SsrsMhtmlExportFilterOptions(options);
                    default:
                        throw new NotAcceptableException();
                }
            }

            return new SsrsHtmlExportFilterOptions(options);
        }

        internal override Stream GetExportedStream(ExportFilter exportFilter, ExportFilterOptions exportFilterOptions)
        {
            // Not same as FlexReport image exporter, for SSRS image exporter, the exported file is not zipped 
            // if there is only one page, even if the UseZipForMultipleFiles is set to true.
            var imageExporter = exportFilter as ImageExporter;
            if (imageExporter != null && imageExporter.UseZipForMultipleFiles && !imageExporter.MultiFile)
            {
                // change the file name extension from .zip to .bmp/.png/.***
                var imageExportOptions = exportFilterOptions as SsrsImageExportFilterOptions;
                var imageFileName = System.IO.Path.ChangeExtension(imageExporter.FileName, imageExportOptions.ImageExtension);
                File.Move(imageExporter.FileName, imageFileName);

                // zip the image file
                var zipFile = ZipFolder(imageFileName);
                return File.OpenRead(zipFile);
            }

            return base.GetExportedStream(exportFilter, exportFilterOptions);
        }

        protected override void InternalLoad()
        {
            _ssrsReport.SecurityError += _ssrsReport_SecurityError;

            try
            {
                // to force gettings the info of parameters and page settings.
                _ssrsReport.ValidateParameters();
            }
            catch (Exception ex)
            {
                // invalid document location throws fault exception.
                if (SsrsReportProvider.IsFaultException(ex))
                {
                    throw new NotFoundException();
                }

                throw ex;
            }
        }

        private void _ssrsReport_SecurityError(object sender, SecurityErrorEventArgs e)
        {
            ErrorList.Add(string.Format(Resources.SecurityError, e.Path, e.FailedAttemptCount, e.Retry));
        }

        internal override IAsyncActionWithProgress<double> InternalRenderAsync()
        {
            return _paginated ? _paginatedDocument.InternalRenderAsync() : _ssrsReport.GenerateAsyncEx();
        }

        internal override void Stop()
        {
            base.Stop();
            _ssrsReport.CancelAsync();
            _paginatedDocument.Stop();
        }

        public override void Dispose()
        {
            base.Dispose();
            _paginatedDocument.Dispose();
        }
    }

    /// <summary>
    /// The document used to manage the PDF content for paginated SSRS report.
    /// </summary>
    internal class PaginatedSsrsReport : Document.Models.Document
    {
        private readonly object _lockObj = new object();
        private readonly SsrsReport _ssrsReport;
        private C1DocumentSource _pdfDocument;
        private bool _dirty = true;

        public PaginatedSsrsReport(SsrsReport ssrsReport, string path)
            : base(path)
        {
            if (ssrsReport == null)
                throw new ArgumentNullException("ssrsReport");

            _ssrsReport = ssrsReport;
            _ssrsReport.DocumentUpdating += (s, e) => SetDirty(true);

            _pdfDocument = new C1PdfDocumentSource();
        }

        internal override Type LicenseDetectorType
        {
            get
            {
                return typeof(LicenseDetector);
            }
        }

        public override C1DocumentSource DocumentSource
        {
            get
            {
                return _pdfDocument;
            }
            protected set
            {
                _pdfDocument = value;
            }
        }

        internal override IDocumentFeatures GetFeatures()
        {
            throw new NotSupportedException();
        }

        protected override void InternalLoad()
        {
            // nothing to do for pdf doucument souce.
        }

        internal IAsyncActionWithProgress<double> InternalRenderAsync()
        {
            if (!_dirty)
            {
                return new CompletedAction();
            }

            var filter = CreateExportFilter();
            var result = _ssrsReport.DocumentSource.ExportAsyncEx(filter);
            result.Completed += (info, status) =>
            {
                lock (_lockObj)
                {
                    SetDocumentSource(filter);
                }
            };
            return result;
        }

        private void InternalRender()
        {
            if (_dirty)
            {
                lock (_lockObj)
                {
                    if (_dirty)
                    {
                        var filter = CreateExportFilter();
                        _ssrsReport.DocumentSource.Export(filter);
                        SetDocumentSource(filter);
                    }
                }
            }
        }

        private void SetDocumentSource(ExportFilter filter)
        {
            var oldDocument = _pdfDocument;
            var doc = new C1PdfDocumentSource();
            doc.LoadFromFile(filter.FileName);
            _pdfDocument = doc;
            SetDirty(false);

            if (oldDocument != null)
            {
                oldDocument.Dispose();
                oldDocument = null;
            }
        }

        internal override void InternalExport(ExportFilter exportFilter)
        {
            InternalRender();
            base.InternalExport(exportFilter);
        }

        private void SetDirty(bool dirty)
        {
            _dirty = dirty;
        }

        private ExportFilter CreateExportFilter()
        {
            var settings = new Dictionary<string, string>();
            var exportOptions = new SsrsPdfExportFilterOptions(settings);
            return exportOptions.ToExportFilter(ExportTempFolder);
        }
    }

    /// <summary>
    /// Represents an action which is completed.
    /// </summary>
    internal class CompletedAction : IAsyncActionWithProgress<double>
    {
        private AsyncActionProgressHandler<double> _actionProgress;
        private AsyncActionWithProgressCompletedHandler<double> _actionCompleted;

        #region IAsyncInfo implementation

        void IAsyncInfo.Cancel()
        {
            // do nothing
        }

        void IAsyncInfo.Close()
        {
            // do nothing
        }

        AsyncStatus IAsyncInfo.Status
        {
            get
            {
                return AsyncStatus.Completed;
            }
        }

        Exception IAsyncInfo.ErrorCode
        {
            get
            {
                return null;
            }
        }

        uint IAsyncInfo.Id
        {
            get
            {
                return 0;
            }
        }

        #endregion

        #region IAsyncActionWithProgress<double> implementation

        void IAsyncActionWithProgress<double>.GetResults()
        {
            // do nothing
        }

        AsyncActionProgressHandler<double> IAsyncActionWithProgress<double>.Progress
        {
            get { return _actionProgress; }
            set { _actionProgress = value; }
        }

        AsyncActionWithProgressCompletedHandler<double> IAsyncActionWithProgress<double>.Completed
        {
            get { return _actionCompleted; }
            set { _actionCompleted = value; }
        }

        #endregion
    }
}
