﻿#if !ASPNETCORE
using System.Web.Http;
using IActionResult = System.Web.Http.IHttpActionResult;
using FromQuery = C1.Web.Api.FromUriExAttribute;
using System.Web.Http.ModelBinding;
#else
using Microsoft.AspNetCore.Mvc;
using RoutePrefixAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;
#endif
using System.Collections.Generic;
using C1.Web.Api.Document.Models;
using System;
using System.Linq;
using C1.Web.Api.Document;

namespace C1.Web.Api.Report
{
    /// <summary>
    /// The report controller.
    /// </summary>
    [RoutePrefix("api/report")]
    public class ReportController : DocumentController
    {
        #region reports

        /// <summary>
        /// Gets the catalog info of the specified path.
        /// </summary>
        /// <param name="path">The folder path or report path.</param>
        /// <param name="recursive">Whether to return the entire tree of child items below the specified item.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Models.CatalogItem"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*path}", ExcludingKeywords = new[] { "$report", "$instances" }, Order = 1)]
        public virtual IActionResult GetCatalogInfo(string path, bool recursive = false)
        {
            var context = new ReportsCatalogRequestContext(Request);
            return ProcessAction(() =>
            {
                var content = context.GetCatalogInfo(path, recursive);
                if (content == null)
                {
                    throw new NotFoundException();
                }

                return Ok(content);
            });
        }

#endregion

#region report

        /// <summary>
        /// Gets the info of the specified report.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Models.ReportInfo"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$report")]
        public virtual IActionResult GetReport(string reportPath)
        {
            var context = new ReportRequestContext(reportPath);

            return ProcessReportAction(context, () =>
            {
                var content = context.GetReportInfo();
                content.PageSettingsLocation = Location<string>(() => GetReportPageSettings);
                content.ParametersLocation = Location<string>(() => GetReportParameters);
                content.SupportedFormatsLocation = Location<string>(() => GetReportSupportedFormats);
                content.CreationLocation = Location("CreateInstance");
                return Ok(content);
            });
        }

        /// <summary>
        /// Gets the default page settings defined in the specified report.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="PageSettings"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$report/pagesettings")]
        public virtual IActionResult GetReportPageSettings(string reportPath)
        {
            var context = new ReportRequestContext(reportPath);

            return ProcessReportAction(context, () =>
            {
                var content = context.GetPageSettings();
                return Ok(content);
            });
        }

        /// <summary>
        /// Gets the parameters defined in the specified report.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of collection of <see cref="Models.Parameter"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$report/parameters")]
        public virtual IActionResult GetReportParameters(string reportPath)
        {
            var context = new ReportRequestContext(reportPath);

            return ProcessReportAction(context, () =>
            {
                var content = context.GetParameters();
                return Ok(content);
            });
        }

        /// <summary>
        /// Gets the parameter with specifed name in the specified report.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="parameterName">The name of the parameter.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Models.Parameter"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$report/parameters/{parameterName}")]
        public virtual IActionResult GetReportParameterByName(string reportPath, string parameterName)
        {
            var context = new ReportRequestContext(reportPath);

            return ProcessReportAction(context, () =>
            {
                var content = context.GetParameterByName(parameterName);
                return Ok(content);
            });
        }

        /// <summary>
        /// Gets the export formats supported by the specified report.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of collection of <see cref="Document.Models.ExportDescription"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$report/supportedformats")]
        public virtual IActionResult GetReportSupportedFormats(string reportPath)
        {
            var context = new ReportRequestContext(reportPath);

            return ProcessReportAction(context, () =>
            {
                var content = context.GetSupportedFormats();
                return Ok(content);
            });
        }

        /// <summary>
        /// Gets the export format with specified name which is supported by the specified report.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="formatName">The name of the export format.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Document.Models.ExportDescription"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$report/supportedformats/{formatName}")]
        public virtual IActionResult GetReportSupportedFormatByName(string reportPath, string formatName)
        {
            var context = new ReportRequestContext(reportPath);

            return ProcessReportAction(context, () =>
            {
                var content = context.GetSupportedFormatByName(formatName);
                return Ok(content);
            });
        }

        /// <summary>
        /// Exports the specified report to the specified export filter with options and the specified page settings or/and parameters.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="exportOptions">The export options.</param>
        /// <param name="exportFileName">The optional exported file name.</param>
        /// <param name="parameters">The optional parameters for rendering.</param>
        /// <param name="pageSettings">The optional page settings for rendering.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of exported file stream.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$report/export")]
        public virtual IActionResult ExportReport_Get(string reportPath, [FromQuery]IDictionary<string, string> exportOptions, string exportFileName = null, [FromQuery]IDictionary<string, string[]> parameters = null, [FromQuery]PageSettings pageSettings = null)
        {
            return ExportReport(reportPath, exportOptions, exportFileName, parameters, pageSettings);
        }

        /// <summary>
        /// Exports the specified report to the specified export filter with options and the specified page settings or/and parameters.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="exportOptions">The export options.</param>
        /// <param name="exportFileName">The optional exported file name.</param>
        /// <param name="parameters">The optional parameters for rendering.</param>
        /// <param name="pageSettings">The optional page settings for rendering.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of exported file stream.</returns>
        [HttpPost]
        [PathRoute("{*reportPath}/$report/export")]
        public virtual IActionResult ExportReport_Post(string reportPath, [FromFormEx(true)]IDictionary<string, string> exportOptions, [FromFormEx] string exportFileName = null, [FromFormEx(true)]IDictionary<string, string[]> parameters = null, [FromFormEx(true)]PageSettings pageSettings = null)
        {
            return ExportReport(reportPath, exportOptions, exportFileName, parameters, pageSettings);
        }

        /// <summary>
        /// Exports the specified report to the specified export filter with options and the specified page settings or/and parameters.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="exportOptions">The export options.</param>
        /// <param name="exportFileName">The optional exported file name.</param>
        /// <param name="parameters">The optional parameters for rendering.</param>
        /// <param name="pageSettings">The optional page settings for rendering.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of exported file stream.</returns>
        protected virtual IActionResult ExportReport(string reportPath, IDictionary<string, string> exportOptions, string exportFileName, IDictionary<string, string[]> parameters, PageSettings pageSettings)
        {
            var context = new ReportRequestContext(reportPath)
            {
                Parameters = parameters,
                PageSettings = pageSettings
            };

            if (exportOptions != null)
            {
                exportOptions = exportOptions.ToDictionary(p => p.Key, p => p.Value, StringComparer.OrdinalIgnoreCase);
            }

            return ProcessReportAction(context, () =>
            {
                var options = context.GetExportFilterOptions(exportOptions);
                var filter = context.GetExportFilter(options);
                // Export should execute outside FileResult.ExecuteResultAsync(), 
                // otherwise if export contains new System.Windows.Form.Control(), the process will suspend in Core 2.0.
                var steam = context.Export(filter, options);

                return File(() =>
                {
                    return steam;
                }, context.GetExportFullFileName(exportFileName, options));
            });
        }

#endregion

#region report cache

        /// <summary>
        /// Gets all report instances created by the specified report.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of collection of <see cref="Models.ReportExecutionInfo"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$instances")]
        public virtual IActionResult GetInstances(string reportPath)
        {
            var context = new ReportInstanceRequestContext(reportPath, null);

            return ProcessAction(() =>
            {
                var content = context.GetInstances().ToArray();
                if (!content.Any())
                {
                    var reportContext = new ReportRequestContext(reportPath);
                    using (var report = reportContext.TryLoadReport())
                    {
                        if (report == null)
                        {
                            return NotFound();
                        }
                    }
                }
                else
                {
                    foreach(var info in content)
                    {
                        SetLocations(info);
                    }
                }

                return Ok(content);
            });
        }

        /// <summary>
        /// Creates new report instance from the specified report.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="parameters">The optional parameters.</param>
        /// <param name="pageSettings">The optional page settings.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Models.ReportExecutionInfo"/> type object, for the new created report instance.</returns>
        [HttpPost]
        [PathRoute("{*reportPath}/$instances")]
        public virtual IActionResult CreateInstance(string reportPath, [FromFormEx(true)]IDictionary<string, string[]> parameters = null, [FromFormEx(true)]PageSettings pageSettings = null)
        {
            var context = new ReportRequestContext(reportPath)
            {
                Parameters = parameters,
                PageSettings = pageSettings
            };

            return ProcessReportAction(context, () =>
            {
                var cache = context.CreateInstance();

                var values = new Dictionary<string, object>();
                values.Add("instanceId", cache.Id);
                var url = Location<string, string>(() => GetInstanceById, values);
                var content = GetExecutionInfo(reportPath, cache.Id);
                return Created(new Uri(url), content);
            });
        }

        /// <summary>
        /// Gets the info of the report instance with the specified instance id.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The id of the instance.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Models.ReportExecutionInfo"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$instances/{instanceId}")]
        public virtual IActionResult GetInstanceById(string reportPath, string instanceId)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(() =>
            {
                var content = GetExecutionInfo(reportPath, instanceId);
                return Ok(content);
            });
        }

        private Models.ReportExecutionInfo GetExecutionInfo(string reportPath, string instanceId)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);
            var content = context.GetInstance();
            SetLocations(content);
            return content;
        }

        private void SetLocations(Models.ReportExecutionInfo info)
        {
            var values = new Dictionary<string, object>();
            values["instanceId"] = info.Id;

            info.StatusLocation = Location<string, string>(() => GetInstanceStatus, values);
            info.OutlinesLocation = Location<string, string>(() => GetInstanceOutlines, values);
            info.ParametersLocation = Location<string, string>(() => GetInstanceParameters, values);
            info.FeaturesLocation = Location<string, string>(() => GetInstanceFeatures, values);
            info.PageSettingsLocation = Location<string, string>(() => GetInstancePageSettings, values);
            info.SupportedFormatsLocation = Location<string, string>(() => GetInstanceSupportedFormats, values);
        }

        /// <summary>
        /// Deletes the report instance with the specifed instance id.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <returns>No content.</returns>
        [HttpDelete]
        [PathRoute("{*reportPath}/$instances/{instanceId}")]
        public virtual IActionResult DeleteInstanceById(string reportPath, string instanceId)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(() =>
            {
                context.RemoveInstance();
                return NoContent();
            });
        }

        /// <summary>
        /// Renders the report instance with the specified instance id.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <param name="parameters">The optional parameters.</param>
        /// <param name="pageSettings">The optional page settings.</param>
        /// <param name="actionString">The optional action string.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Models.ReportStatus"/> type object.</returns>
        [HttpPost]
        [PathRoute("{*reportPath}/$instances/{instanceId}/render")]
        public virtual IActionResult RenderInstance(string reportPath, string instanceId, [FromFormEx(true)]IDictionary<string, string[]> parameters = null, [FromFormEx(true)]PageSettings pageSettings = null, [FromFormEx]string actionString = null)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId)
            {
                ActionString = actionString,
                Parameters = parameters,
                PageSettings = pageSettings
            };

            return ProcessAction(() =>
            {
                context.Render();
                var status = context.GetStatus();
                if(status.Status == Models.ExecutingStatus.Completed)
                {
                    return Ok(status);
                }
                else
                {
                    var url = Location<string, string>(() => GetInstanceStatus);
                    return Accept(url);
                }
            });
        }

        /// <summary>
        /// Gets the status of the report instance with specified instance id.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Models.ReportStatus"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$instances/{instanceId}/status")]
        public virtual IActionResult GetInstanceStatus(string reportPath, string instanceId)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(() =>
            {
                var status = context.GetStatus();
                return Ok(status);
            });
        }

        /// <summary>
        /// Stops the rendering of the report instance.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Models.ReportStatus"/> type object.</returns>
        [HttpPost]
        [PathRoute("{*reportPath}/$instances/{instanceId}/stop")]
        public virtual IActionResult StopInstanceRendering(string reportPath, string instanceId)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(() =>
            {
                context.Stop();
                var status = context.GetStatus();
                return Ok(status);
            });
        }

        /// <summary>
        /// Gets the parameters in the report instance with specified instance id.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of collection of <see cref="Models.Parameter"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$instances/{instanceId}/parameters")]
        public virtual IActionResult GetInstanceParameters(string reportPath, string instanceId)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(()=>
            {
                var content = context.GetParameters();
                return Ok(content);
            });
        }

        /// <summary>
        /// Updates all parameter values in the report instance with specified instance id.
        /// </summary>
        /// <remarks>
        /// Use the default value if the parameter value is not specified.
        /// </remarks>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <param name="parameters">The report parameters.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of collection of <see cref="Models.Parameter"/> type object, for the new parameters with validation.</returns>
        [HttpPut]
        [PathRoute("{*reportPath}/$instances/{instanceId}/parameters")]
        public virtual IActionResult SetInstanceParameters(string reportPath, string instanceId, [FromForm]IDictionary<string, string[]> parameters)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(()=>
            {
                context.SetParameters(parameters);
                var content = context.GetParameters();
                return Ok(content);
            });
        }

        /// <summary>
        /// Updates specified parameter values in the report instance with specified instance id.
        /// </summary>
        /// <remarks>
        /// Keep the current value if the parameter value is not specified.
        /// </remarks>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <param name="parameters">The report parameters.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of collection of <see cref="Models.Parameter"/> type object, for the new parameters with validation.</returns>
        [HttpPatch]
        [PathRoute("{*reportPath}/$instances/{instanceId}/parameters")]
        public virtual IActionResult UpdateInstanceParameters(string reportPath, string instanceId, [FromForm]IDictionary<string, string[]> parameters)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(() =>
            {
                context.UpdateParameters(parameters);
                var content = context.GetParameters();
                return Ok(content);
            });
        }

        /// <summary>
        /// Gets the parameter with specified name in the report instance with specified instance id.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <param name="parameterName">The name of the parameter.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Models.Parameter"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$instances/{instanceId}/parameters/{parameterName}")]
        public virtual IActionResult GetInstanceParameterByName(string reportPath, string instanceId, string parameterName)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(() =>
            {
                var content = context.GetParameterByName(parameterName);
                return Ok(content);
            });
        }

        /// <summary>
        /// Updates the value of parameter with specified name in the report instance with specified instance if.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <param name="parameterName">The name of the parameter.</param>
        /// <param name="value">The value of the parameter.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Models.Parameter"/> type object, for the new parameter with validation.</returns>
        [HttpPut]
        [PathRoute("{*reportPath}/$instances/{instanceId}/parameters/{parameterName}")]
        public virtual IActionResult SetInstanceParameterByName(string reportPath, string instanceId, string parameterName, [FromFormEx] string[] value)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(() =>
            {
                context.SetParameterByName(parameterName, value);
                var content = context.GetParameterByName(parameterName);
                return Ok(content);
            });
        }

        /// <summary>
        /// Gets the current page settings in the report instance with specified instance id.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Document.Models.PageSettings"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$instances/{instanceId}/pagesettings")]
        public virtual IActionResult GetInstancePageSettings(string reportPath, string instanceId)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(() =>
            {
                var content = context.GetPageSettings();
                return Ok(content);
            });
        }

        /// <summary>
        /// Updates all page settings properties in the report instance with specified instance id.
        /// </summary>
        /// <remarks>
        /// Use the default value if one property is not specified.
        /// </remarks>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <param name="pageSettings">The page settings.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Document.Models.PageSettings"/> type object, for the new updated page settings.</returns>
        [HttpPut]
        [PathRoute("{*reportPath}/$instances/{instanceId}/pagesettings")]
        public virtual IActionResult SetInstancePageSettings(string reportPath, string instanceId, Document.Models.PageSettings pageSettings)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(() =>
            {
                context.SetPageSettings(pageSettings);
                var content = context.GetPageSettings();
                return Ok(content);
            });
        }

        /// <summary>
        /// Updates the specified page settings properties in the report instancce with specified instance id.
        /// </summary>
        /// <remarks>
        /// Keep the current value if one property is not specified.
        /// </remarks>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <param name="pageSettings">The page settings.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Document.Models.PageSettings"/> type object, for the new updated page settings.</returns>
        [HttpPatch]
        [PathRoute("{*reportPath}/$instances/{instanceId}/pagesettings")]
        public virtual IActionResult UpdateInstancePageSettings(string reportPath, string instanceId, Document.Models.PageSettings pageSettings)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(() =>
            {
                context.UpdatePageSettings(pageSettings);
                var content = context.GetPageSettings();
                return Ok(content);
            });
        }

        /// <summary>
        /// Gets all outlines in the report instance with specified instance id.
        /// </summary>
        /// <remarks>
        /// Please ensure the report instance is rendered.
        /// </remarks>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of collection of <see cref="Document.Models.OutlineNode"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$instances/{instanceId}/outlines")]
        public virtual IActionResult GetInstanceOutlines(string reportPath, string instanceId)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(() =>
            {
                var content = context.GetOutlines();
                return Ok(content);
            });
        }

        /// <summary>
        /// Not supported. Gets all bookmarks in the report instance with specified instance id.
        /// </summary>
        /// <remarks>
        /// It's not supported by this api.
        /// </remarks>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of collection of <see cref="Document.Models.DocumentPosition"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$instances/{instanceId}/bookmarks")]
        public virtual IActionResult GetInstanceBookmarks(string reportPath, string instanceId)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(() =>
            {
                var content = context.GetBookmarks();
                return Ok(content);
            });
        }

        /// <summary>
        /// Gets the bookmark with specified name in the report instance with specified instance id.
        /// </summary>
        /// <remarks>
        /// Please ensure the report instance is rendered.
        /// </remarks>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <param name="bookmarkName">The name of the bookmark.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Document.Models.DocumentPosition"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$instances/{instanceId}/bookmarks/{bookmarkName}")]
        public virtual IActionResult GetInstanceBookmarkByName(string reportPath, string instanceId, string bookmarkName)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(() =>
            {
                var content = context.GetBookmarkByName(bookmarkName);
                return Ok(content);
            });
        }

        /// <summary>
        /// Gets the search result in the report instance with specified instance id.
        /// </summary>
        /// <remarks>
        /// Please ensure the report instance is rendered.
        /// </remarks>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <param name="text">The text used to be searched.</param>
        /// <param name="matchCase">A boolean value indicates if search the value with case sensitive.</param>
        /// <param name="wholeWord">A boolean value indicates if search the value with matching a whole word.</param>
        /// <param name="startPageIndex">0-based index of the first page to search.</param>
        /// <param name="scope">The search scope.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of collection of <see cref="Document.Models.SearchResult"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$instances/{instanceId}/search")]
        public virtual IActionResult GetInstanceSearchResults(string reportPath, string instanceId, 
            [ModelBinder(BinderType = typeof(StringModelBinder))]string text, bool matchCase = false, bool wholeWord = false, 
            int startPageIndex = 0, SearchScope scope = SearchScope.WholeDocument)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(() =>
            {
                var options = new Document.Models.SearchOptions
                {
                    Text = Uri.UnescapeDataString(text),
                    MatchCase = matchCase,
                    WholeWord = wholeWord
                };

                var content = context.Search(startPageIndex, scope, options);
                return Ok(content);
            });
        }

        /// <summary>
        /// Exports the report instance with specified instance id into report filter with options.
        /// </summary>
        /// <remarks>
        /// Please ensure the report instance is rendered.
        /// </remarks>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <param name="options">The export options.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of exported file stream.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$instances/{instanceId}/export")]
        public virtual IActionResult ExportInstance_Get(string reportPath, string instanceId, [FromQuery]IDictionary<string, string> options)
        {
            return ExportInstance(reportPath, instanceId, options);
        }

        /// <summary>
        /// Exports the report instance with specified instance id into report filter with options.
        /// </summary>
        /// <remarks>
        /// Please ensure the report instance is rendered.
        /// </remarks>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <param name="options">The export options.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of exported file stream.</returns>
        [HttpPost]
        [PathRoute("{*reportPath}/$instances/{instanceId}/export")]
        public virtual IActionResult ExportInstance_Post(string reportPath, string instanceId, [FromFormEx]IDictionary<string, string> options)
        {
            return ExportInstance(reportPath, instanceId, options);
        }

        /// <summary>
        /// Exports the report instance with specified instance id into report filter with options.
        /// </summary>
        /// <remarks>
        /// Please ensure the report instance is rendered.
        /// </remarks>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <param name="exportOptions">The export options.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of exported file stream.</returns>
        protected virtual IActionResult ExportInstance(string reportPath, string instanceId, IDictionary<string, string> exportOptions)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);
            if (exportOptions != null)
            {
                exportOptions = exportOptions.ToDictionary(p => p.Key, p=>p.Value, StringComparer.OrdinalIgnoreCase);
            }

            return ProcessAction(()=>
            {
               string fileName = null;
                if (exportOptions != null)
                {
                    foreach (var item in exportOptions)
                    {
                        if (string.Equals(item.Key, "ExportFileName", StringComparison.OrdinalIgnoreCase))
                        {
                            fileName = item.Value;
                            break;
                        }
                    }
                }
                var options = context.GetExportFilterOptions(exportOptions);
                var filter = context.GetExportFilter(options);
                // Export should execute outside FileResult.ExecuteResultAsync(), 
                // otherwise if export contains new System.Windows.Form.Control(), the process will suspend in Core 2.0.
                var stream = context.Export(filter, options);

                return File(()=>
                {
                    return stream;
                }, context.GetExportFullFileName(fileName, options));
            });
        }

        /// <summary>
        /// Gets the export formats supported by the report instance with specified instance id.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of collection of <see cref="Document.Models.ExportDescription"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$instances/{instanceId}/supportedformats")]
        public virtual IActionResult GetInstanceSupportedFormats(string reportPath, string instanceId)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(()=>
            {
                var content = context.GetSupportedFormats();
                return Ok(content);
            });
        }

        /// <summary>
        /// Gets the export format with specified name supported by the report instance with specified instance id.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <param name="formatName">The name of the export format.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of <see cref="Document.Models.ExportDescription"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$instances/{instanceId}/supportedformats/{formatName}")]
        public virtual IActionResult GetInstanceSupportedFormatByName(string reportPath, string instanceId, string formatName)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(() =>
            {
                var content = context.GetSupportedFormatByName(formatName);
                return Ok(content);
            });
        }

        /// <summary>
        /// Gets the features supported by the report instance with specified instance id.
        /// </summary>
        /// <param name="reportPath">The full path of the report.</param>
        /// <param name="instanceId">The report instance id.</param>
        /// <returns>An <see cref="IActionResult"/> type object with content of collection of <see cref="Document.Models.IDocumentFeatures"/> type object.</returns>
        [HttpGet]
        [PathRoute("{*reportPath}/$instances/{instanceId}/features")]
        public virtual IActionResult GetInstanceFeatures(string reportPath, string instanceId)
        {
            var context = new ReportInstanceRequestContext(reportPath, instanceId);

            return ProcessAction(() =>
            {
                var content = context.GetFeatures();
                return Ok(content);
            });
        }

#endregion

#region processing

        private IActionResult ProcessReportAction(ReportRequestContext context, Func<IActionResult> func)
        {
            context.Language = Request.GetAcceptCulture();
            context.SetRequestParamsGetter(() => Request.GetParams());
            return ProcessAction(func);
        }

#endregion
    }
}