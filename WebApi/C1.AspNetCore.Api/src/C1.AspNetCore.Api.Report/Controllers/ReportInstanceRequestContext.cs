﻿#if !ASPNETCORE

#else
using Microsoft.AspNetCore.Mvc;
#endif
using System;
using System.Collections.Generic;
using C1.Web.Api.Document.Models;
using C1.Web.Api.Report.Models;
using System.Linq;

namespace C1.Web.Api.Report
{
    internal class ReportInstanceRequestContext : BaseReportRequestContext
    {
        private Cache<Models.Report> _cache;

        public ReportInstanceRequestContext(string path, string id):base(path)
        {
            Id = id;
        }

        #region settings

        /// <summary>
        /// Gets the id of the report instance.
        /// </summary>
        public string Id { get; private set; }

        /// <summary>
        /// Gets or sets the action string.
        /// </summary>
        public string ActionString { get; set; }

        #endregion

        #region document

        protected override Models.Report GetDocument()
        {
            return Cache.Content;
        }

        private Cache<Models.Report> Cache
        {
            get
            {
                if (_cache == null)
                {
                    if (!ReportCacheManager.Instance.TryGet(Id, out _cache))
                    {
                        throw new NotFoundException(string.Format(Localization.Resources.InvalidInstannceId, Id));
                    }

                    if (!string.Equals(_cache.Content.Path, Path, StringComparison.OrdinalIgnoreCase))
                    {
                        throw new NotFoundException(string.Format(Localization.Resources.InstanceIdNotMatchReportPath, Id, Path));
                    }
                }

                return _cache;
            }
        }

        #endregion

        #region actions

        public IEnumerable<ReportExecutionInfo> GetInstances()
        {
            return ReportCacheManager.Instance
                .Get((k, c) => string.Equals(c.Content.Path, Path, StringComparison.OrdinalIgnoreCase))
                .Select(c => new ReportExecutionInfo(c, c.Content, false));
        }

        public ReportExecutionInfo GetInstance()
        {
            return new ReportExecutionInfo(Cache, Cache.Content, true);
        }

        public void RemoveInstance()
        {
            ReportCacheManager.Instance.Remove(Id);
        }

        public ReportStatus GetStatus(Cache<Models.Report> cache = null)
        {
            cache = cache ?? Cache;
            if (cache == null || cache.Content == null)
            {
                throw new NotFoundException();
            }

            return new ReportStatus(cache, cache.Content);
        }

        public void Render()
        {
            if (PageSettings != null)
            {
                Document.UpdatePageSettings(PageSettings);
            }

            if (Parameters != null)
            {
                Document.UpdateParameters(Parameters);
            }

            if (!string.IsNullOrEmpty(ActionString))
            {
                // execute custom action will render the document internally.
                Document.ExecuteCustomAction(ActionString);
            }
            else
            {
                Document.Render();
            }
        }

        public void Stop()
        {
            Document.Stop();
        }

        public IEnumerable<SearchResult> Search(int startPageIndex, SearchScope scope, SearchOptions options)
        {
            return Document.Search(startPageIndex, scope, options);
        }

        public void SetParameters(IDictionary<string, string[]> values)
        {
            Document.SetParameters(values);
        }

        public void UpdateParameters(IDictionary<string, string[]> values)
        {
            Document.UpdateParameters(values);
        }

        public void SetParameterByName(string name, string[] value)
        {
            Document.SetParameterByName(name, value);
        }

        public PageSettings GetPageSettings()
        {
            return Document.PageSettings;
        }

        public void SetPageSettings(PageSettings settings)
        {
            Document.SetPageSettings(settings);
        }

        public void UpdatePageSettings(PageSettings settings)
        {
            Document.UpdatePageSettings(settings);
        }

        public IEnumerable<OutlineNode> GetOutlines()
        {
            return Document.GetOutlines();
        }

        public IEnumerable<DocumentPosition> GetBookmarks()
        {
            return Document.GetBookmarks();
        }

        public DocumentPosition GetBookmarkByName(string name)
        {
            return Document.GetBookmark(name);
        }

        public IDocumentFeatures GetFeatures()
        {
            return Document.GetFeatures();
        }

        #endregion
    }
}