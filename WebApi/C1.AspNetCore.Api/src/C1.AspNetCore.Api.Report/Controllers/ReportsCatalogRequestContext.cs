﻿#if !ASPNETCORE
using System.Web.Http;
using HttpRequest = System.Net.Http.HttpRequestMessage;
#else
using Microsoft.AspNetCore.Http;
#endif
using System.Linq;
using C1.Web.Api.Storage;
using C1.Web.Api.Document;
using C1.Web.Api.Report.Models;
using System;
using System.IO;

namespace C1.Web.Api.Report
{
    internal class ReportsCatalogRequestContext
    {
        public HttpRequest _request;

        public ReportsCatalogRequestContext(HttpRequest request)
        {
            _request = request;
        }

        #region settings

        private HttpRequest Request
        {
            get { return _request; }
        }

        #endregion

        #region actions

        /// <summary>
        /// Gets the catalog info for the specified path.
        /// </summary>
        /// <param name="path">The folder path or report path.</param>
        /// <param name="recursive">Whether gets all items.</param>
        /// <returns>The catalog info for the specified path.</returns>
        public Models.CatalogItem GetCatalogInfo(string path, bool recursive = false)
        {
            string providerName, folderPath;
            var provider = ReportProviderManager.Current.GetProvider(path, out providerName, out folderPath);
            Models.CatalogItem item = null;
            if (provider != null)
            {
                item = provider.GetCatalogItem(providerName, folderPath, recursive, Request.GetParams());
            }
            else if (string.IsNullOrEmpty(path))
            {
                item = GetAllItems(recursive);
            }

            return item;
        }

        private Models.CatalogItem GetAllItems(bool recursive)
        {
            var root = new Models.CatalogItem();
            root.Type = Models.CatalogItemType.Folder;
            root.Path = string.Empty;
            root.Name = string.Empty;
            var allItems = root.Items;

            var requestParams = Request.GetParams();
            var keys = ReportProviderManager.Current.Items.Keys.Union(StorageProviderManager.Current.Items.Keys);
            foreach (var key in keys)
            {
                string providerName, folderPath;
                var provider = ReportProviderManager.Current.GetProvider(key, out providerName, out folderPath);
                if (provider != null)
                {
                    try
                    {
                        var item = provider.GetCatalogItem(providerName, folderPath, recursive, requestParams);
                        if (item != null)
                        {
                            allItems.Add(item);
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }
            }

            return root;
        }

        #endregion
    }
}
