﻿using System;
using System.ComponentModel;
using System.Globalization;
using C1.Web.Api.Report.Models;
using System.Collections.Generic;
using C1.Web.Api.Document.Models;
using C1.Web.Api.Document;

namespace C1.Web.Api.Report
{
    /// <summary>
    /// Defines base report request.
    /// </summary>
    internal abstract class BaseReportRequestContext : DocumentRequestContext<Models.Report>
    {
        public BaseReportRequestContext(string path)
        {
            Path = path;
        }

        #region settings

        /// <summary>
        /// Gets or sets the full path of the report.
        /// </summary>
        public string Path
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the parameters of the report.
        /// </summary>
#if !ASPNETCORE
        [TypeConverter(typeof(DictionaryQueryConverter))]
#endif
        public IDictionary<string, string[]> Parameters
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the page settings of the report.
        /// </summary>
        public PageSettings PageSettings
        {
            get;
            set;
        }

        #endregion

        #region actions

        /// <summary>
        /// Gets all parameters in the report.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Parameter> GetParameters()
        {
            return Document.GetParameters();
        }

        /// <summary>
        /// Gets the parameter with specified name in the report.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Parameter GetParameterByName(string name)
        {
            var parameter = Document.GetParameterByName(name);
            if (parameter == null)
            {
                throw new NotFoundException();
            }
            return parameter;
        }

        #endregion
    }

#if !ASPNETCORE
    internal class DictionaryQueryConverter:TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            return base.ConvertFrom(context, culture, value);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return false;
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            throw new NotSupportedException(Localization.Resources.ConvertNotSupport);
        }
    }
#endif
}
