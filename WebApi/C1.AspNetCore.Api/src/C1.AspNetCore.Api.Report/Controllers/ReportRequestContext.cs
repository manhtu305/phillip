﻿using C1.Web.Api.Document;
using C1.Web.Api.Document.Models;
using C1.Web.Api.Report.Models;
using System;
using System.Globalization;

namespace C1.Web.Api.Report
{
    /// <summary>
    /// The report request.
    /// </summary>
    internal class ReportRequestContext : BaseReportRequestContext
    {
        public ReportRequestContext(string path) : base(path)
        {
        }

        #region settings

        /// <summary>
        /// Gets or sets the accept language.
        /// </summary>
        public CultureInfo Language
        {
            get;
            set;
        }

        #endregion

        #region document

        protected override Models.Report GetDocument()
        {
            var report = TryLoadReport();

            report.SetLanguage(Language);
            report.Load();

            report.InitPageSettings(PageSettings);
            report.InitParameters(Parameters);

            return report;
        }

        #endregion

        #region actions

        public ReportInfo GetReportInfo()
        {
            return new ReportInfo(Document);
        }

        public Cache<Models.Report> CreateInstance()
        {
            return ReportCacheManager.Instance.Create(Document);
        }

        public PageSettings GetPageSettings()
        {
            return Document.PageSettings;
        }

        public Models.Report TryLoadReport()
        {
            var report = ReportProviderManager.Current.CreateReport(Path, RequestParams);
            if (report == null)
            {
                throw new NotFoundException(string.Format(Localization.Resources.InvalidReportPath, Path));
            };

            return report;
        }

        #endregion
    }
}
