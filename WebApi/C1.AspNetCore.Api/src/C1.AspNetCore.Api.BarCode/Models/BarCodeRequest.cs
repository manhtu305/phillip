﻿#if NETCORE
using GrapeCity.Documents.Barcode;
using GrapeCity.Documents.Drawing;
using GrapeCity.Documents.Text;
#endif

using System.Drawing;


namespace C1.Web.Api.BarCode
{
    /// <summary>
    /// The request data of generating the barcode.
    /// </summary>
    public class BarCodeRequest : ExportSource
    {
        #region Properties
        /// <summary>
        /// Gets or sets the value that is encoded as a barcode image.
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// The type of encoding to use when generating the barcode image.
        /// </summary>
        public CodeType CodeType { get; set; }
        /// <summary>
        /// The width of the bars in screen pixels. If set to 0, the width is calculated automatically.
        /// </summary>
        public int BarWidth { get; set; }
        /// <summary>
        /// The height of the bars in screen pixels.
        /// </summary>
        public int BarHeight { get; set; }
        /// <summary>
        /// Gets or sets the font of the text.
        /// </summary>
        public Font Font { get; set; }
        /// <summary>
        /// Additional number for supplemental barcodes
        /// </summary>
        public string AdditionalNumber { get; set; }
        /// <summary>
        /// Barcode caption position relative to the barcode symbol
        /// </summary>
        public BarCodeCaptionPosition CaptionPosition { get; set; }
        /// <summary>
        /// Barcode caption alignment relative to the barcode symbol
        /// </summary>
        public TextAlignment CaptionAlignment { get; set; }
        /// <summary>
        /// Direction of the barcode.
        /// </summary>
        public BarCodeDirection BarDirection { get; set; }
        /// <summary>
        /// Width of the narrow bar in screen pixels. The default value is one screen pixel wide (1/96").
        /// </summary>
        public int ModuleSize { get; set; }
        /// <summary>
        /// Draw separated caption for certain barcodes (EAN 8/13, UPC-A/E).
        /// </summary>
        public bool CaptionGrouping { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether a checksum of the barcode will be computed and included in the barcode when applicable.
        /// </summary>
        public bool CheckSumEnabled { get; set; }
        /// <summary>
        /// QRCode options.
        /// </summary>
        public QRCodeOptions QRCodeOptions { get; set; }
        /// <summary>
        /// PDF417 options.
        /// </summary>
        public PDF417Options PDF417Options { get; set; }
        /// <summary>
        /// Code49 options.
        /// </summary>
        public Code49Options Code49Options { get; set; }
        /// <summary>
        /// RSSExpandedStacked options.
        /// </summary>
        public RssExpandedStackedOptions RssExpandedStackedOptions { get; set; }
        /// <summary>
        /// MicroPDF417 options.
        /// </summary>
        public MicroPDF417Options MicroPDF417Options { get; set; }
        /// <summary>
        /// GS1Composite options.
        /// </summary>
        public GS1CompositeOptions GS1CompositeOptions { get; set; }
        /// <summary>
        /// DataMatrix options.
        /// </summary>
        public DataMatrixOptions DataMatrixOptions { get; set; }

        /// <summary>
        /// Gets or sets the background color for the barcode image.
        /// </summary>
        public Color BackColor { get; set; }
        /// <summary>
        /// Gets or sets the foreground color for the barcode image.
        /// </summary>
        public Color ForeColor { get; set; }

#if NETCORE
        /// <summary>
        /// DPI values of vertical and horizontal
        /// </summary>
        public DPIS DPIs { get; set; }

        /// <summary>
        /// image align of vertical and horizontal
        /// </summary>
        public ImageAlignment ImgAlign;

        public static readonly int DefaultWidth = 170;
        public static readonly int DefaultHeight = 40;
#else
        /// <summary>
        /// A quiet zone is an area of blank space on either side of a barcode that tells the scanner where the symbology starts and stops.
        /// </summary>
        public QuietZone QuietZone { get; set; }
        /// <summary>
        /// Fixed number of digits of values of barcode.
        /// </summary>
        public int TextFixedLength { get; set; }
        /// <summary>
        /// Code25Intlv options.
        /// </summary>
        public Code25intlvOptions Code25intlvOptions { get; set; }
        /// <summary>
        /// Update EAN128FNC1  options.
        /// </summary>
        public Ean128Fnc1Options Ean128Fnc1Options { get; set; }
#endif

        #endregion

        /// <summary>
        /// Creates a new instance of the <see cref="BarCodeRequest"/>.
        /// </summary>
        public BarCodeRequest()
        {
            FileName = "barcode";
            Type = ExportFileType.Png;
            CodeType = CodeType.Code39;

            Font = new Font() { Family = "Courier New", Size = 8 };
            AdditionalNumber = string.Empty;
            CaptionPosition = BarCodeCaptionPosition.None;
            CaptionAlignment = TextAlignment.Center;
            BarDirection = BarCodeDirection.LeftToRight;
            ModuleSize = 1;
            CaptionGrouping = true;
            CheckSumEnabled = true;
            QRCodeOptions = new QRCodeOptions();
            PDF417Options = new PDF417Options();
            Code49Options = new Code49Options();
            RssExpandedStackedOptions = new RssExpandedStackedOptions();
            MicroPDF417Options = new MicroPDF417Options();
            GS1CompositeOptions = new GS1CompositeOptions();
            DataMatrixOptions = new DataMatrixOptions();
            BackColor = Color.White;
            ForeColor = Color.Black;
#if NETCORE
            BarWidth = DefaultWidth;
            BarHeight = DefaultHeight;
            DPIs = new DPIS();
            ImgAlign = new ImageAlignment();
#else
            BarWidth = 0;
            BarHeight = 40;
            QuietZone = new QuietZone();
            TextFixedLength = 0;
            Code25intlvOptions = new Code25intlvOptions();
            Ean128Fnc1Options = new Ean128Fnc1Options();
#endif
        }
    }

    /// <summary>
    /// Font styles.
    /// </summary>
    public class Font
    {
        /// <summary>
        /// Font family name.
        /// </summary>
        public string Family { get; set; }
        /// <summary>
        /// Font size.
        /// </summary>
        public float Size { get; set; }
        /// <summary>
        /// Whether to make the text bold.
        /// </summary>
        public bool Bold { get; set; }
        /// <summary>
        /// Whether to make the text italic.
        /// </summary>
        public bool Italic { get; set; }
        /// <summary>
        /// Whether to show underline.
        /// </summary>
        public bool Underline { get; set; }
        /// <summary>
        /// Whether to show strikethrough.
        /// </summary>
        public bool Strikeout { get; set; }
    }

#if NETCORE
    /// <summary>
    /// Settings the horizontal and vertical dpi of the image.
    /// </summary>
    public class DPIS
    {
        /// <summary>
        /// Horizontal dpi of the image
        /// </summary>
        public float Horizontal { get; set; }

        /// <summary>
        /// Vertical dpi of the image
        /// </summary>
        public float Vertical { get; set; }

        /// <summary>
        /// it will set the default DPIs to 96.0
        /// </summary>
        public DPIS()
        {
            Horizontal = Vertical = 96.0f;
        }
    }

    public class ImageAlignment
    {
        public ImageAlignHorz Horizontal { get; set; }
        public ImageAlignVert Vertical { get; set; }

        public ImageAlignment()
        {
            Horizontal = ImageAlignHorz.Left;
            Vertical = ImageAlignVert.Top;
        }
    }
#endif

}
