﻿namespace C1.Web.Api.BarCode
{
    /// <summary>
    /// Type of code, or symbology, the barcode control will use to generate the barcode.
    /// </summary>
    public enum CodeType : short
    {
        /// <summary>
        /// None.
        /// </summary>
        None = 0,

        // regular barcodes
        /// <summary>
        /// ANSI 3 of 9 (Code 39) uses upper case, numbers, - , * $ / + %.
        /// </summary>
        Ansi39 = 1,
        /// <summary>
        /// ANSI Extended 3 of 9 (Extended Code 39) uses the complete ASCII character set.
        /// </summary>
        Ansi39x = 2,
        /// <summary>
        /// Code 2 of 5 uses only numbers.
        /// </summary>
        Code_2_of_5 = 3,
        /// <summary>
        /// Interleaved 2 of 5 uses only numbers.
        /// </summary>
        Code25intlv = 4,
        /// <summary>
        /// Matrix 2 of 5 is a higher density barcode consisting of 3 black bars and 2 white bars. 
        /// Matrix_2_of_5 uses only numbers.
        /// </summary>
        Matrix_2_of_5 = 5,
        /// <summary>
        /// Code 39 uses numbers,  % * $ /. , - +, and upper case.
        /// </summary>
        Code39 = 6,
        /// <summary>
        /// Extended Code 39 uses the complete ASCII character set.
        /// </summary>
        Code39x = 7,
        /// <summary>
        /// Code 128 A uses control characters, numbers, punctuation, and upper case.
        /// </summary>
        Code_128_A = 8,
        /// <summary>
        /// Code 128 B uses punctuation, numbers, upper case and lower case.
        /// </summary>
        Code_128_B = 9,
        /// <summary>
        /// Code 128 C uses only numbers.
        /// </summary>
        Code_128_C = 10,
        /// <summary>
        /// Code 128 Auto uses the complete ASCII character set.  
        /// Automatically selects between Code 128 A, B and C to give the smallest barcode.
        /// </summary>
        Code_128auto = 11,
        /// <summary>
        /// Code 93 uses uppercase, % $ * / , + -,  and numbers.
        /// </summary>
        Code_93 = 12,
        /// <summary>
        /// Extended Code 93 uses the complete ASCII character set.
        /// </summary>
        Code93x = 13,
        /// <summary>
        /// MSI Code uses only numbers.
        /// </summary>
        MSI = 14,
        /// <summary>
        /// PostNet uses only numbers with a check digit.
        /// </summary>
        PostNet = 15,
        /// <summary>
        /// Codabar uses A B C D + - : , / and numbers.
        /// </summary>
        Codabar = 16,
        /// <summary>
        /// EAN-8 uses only numbers (7 numbers and a check digit).
        /// </summary>
        EAN_8 = 17,
        /// <summary>
        /// EAN-13 uses only numbers (12 numbers and a check digit). 
        /// If there are only 12 numbers in the string, it calculates a checksum and adds it to the thirteenth position. 
        /// If there are 13, it validates the checksum and throws an error if it is incorrect.
        /// </summary>
        EAN_13 = 18,
        /// <summary>
        /// UPC-A uses only numbers (11 numbers and a check digit).
        /// </summary>
        UPC_A = 19,
        /// <summary>
        /// UPC-E0 uses only numbers.
        /// Used for zero-compression UPC symbols.  
        /// For the Caption property, you may enter either a six-digit UPC-E code or a complete 11-digit (includes code type, which must be 0 (zero)) UPC-A code.  
        /// If an 11-digit code is entered, the Barcode control will convert it to a six-digit UPC-E code, if possible. 
        /// If it is not possible to convert from the 11-digit code to the six-digit code, nothing is displayed.
        /// </summary>
        UPC_E0 = 20,
        /// <summary>
        /// UPC-E1 uses only numbers.  Used typically for shelf labeling in the retail environment.  
        /// The length of the input string for U.P.C. E1 is six numeric characters.
        /// </summary>
        UPC_E1 = 21,
        /// <summary>
        /// Royal Mail RM4SCC uses only letters and numbers (with a check digit).  
        /// This is the barcode used by the Royal Mail in the United Kingdom.
        /// </summary>
        RM4SCC = 22,
        /// <summary>
        /// UCC/EAN –128 uses the complete ASCII character Set.  
        /// This is a special version of Code 128 used in HIBC applications.
        /// </summary>
        UCCEAN128 = 23,
        // GC barcodes
        /// <summary>
        /// QRCode is a 2D symbology that is capable of handling numeric, alphanumeric and byte data as well as Japanese kanji and kana characters. 
        /// This symbology can encode up to 7,366 characters.
        /// </summary>
        QRCode = 24,
        /// <summary>
        /// Code 49 is a 2D high-density stacked barcode. Encodes the complete ASCII character set. 
        /// </summary>
        Code49 = 25,
        /// <summary>
        /// This is the barcode used by the Japanese Postal system. 
        /// Encodes alpha and numeric characters consisting of 18 digits including a 7-digit postal code number, optionally followed by block and house number information. 
        /// The data to be encoded can include hyphens.
        /// </summary>
        JapanesePostal = 26,
        /// <summary>
        /// Pdf417 is a popular high-density 2-dimensional symbology that encodes up to 1108 bytes of information. 
        /// This barcode consists of a stacked set of smaller barcodes. 
        /// Encodes the full ASCII character set. 
        /// Capable of encoding as many as 2725 data characters.
        /// </summary>
        Pdf417 = 27,
        /// <summary>
        /// EAN128FNC1 is a UCC/EAN-128 (EAN128) type barcode that allows you to insert FNC1 character at any place and adjust the bar size etc, which is not available in UCC/EAN-128.
        /// To insert FNC1 character, set “\n” for C#, or “vbLf” for VB to Text property at runtime.
        /// </summary>
        EAN128FNC1 = 28, // japanese implementation of Ean 128 bar code,
        /// <summary>
        /// RSS14 is a Reduced Space Symbology that encodes Composite Component (CC) extended EAN and UPC information in less space. 
        /// This version is a 14-digit EAN.UCC item identification for use with omnidirectional point-of-sale scanners.
        /// </summary>
        RSS14 = 29, // RSS barcodes
        /// <summary>
        /// RSS14Truncated is a Reduced Space Symbology that encodes Composite Component (CC) extended EAN and UPC information in less space. 
        /// This version is a 14-digit EAN.UCC item identification plus Indicator digits for use on small items, not for point-of-sale scanners.
        /// </summary>
        RSS14Truncated = 30,
        /// <summary>
        /// RSS14Stacked is a Reduced Space Symbology that encodes Composite Component (CC) extended EAN and UPC information in less space. 
        /// This version is the same as RSS14Truncated, but stacked in two rows when RSS14Truncated is too wide.
        /// </summary>
        RSS14Stacked = 31,
        /// <summary>
        /// RSS14StackedOmnidirectional is a Reduced Space Symbology that encodes Composite Component (CC) extended EAN and UPC information in less space. 
        /// This version is the same as RSS14, but stacked in two rows when RSS14 is too wide.
        /// </summary>
        RSS14StackedOmnidirectional = 32,
        /// <summary>
        /// RSSExpanded is a Reduced Space Symbology that encodes Composite Component (CC) extended EAN and UPC information in less space. 
        /// This version is a 14-digit EAN.UCC item identification plus AI element strings (expiration date, weight, etc.) for use with omnidirectional point-of-sale scanners.
        /// </summary>
        RSSExpanded = 33,
        /// <summary>
        /// RSSExpandedStacked is a Reduced Space Symbology that encodes Composite Component (CC) extended EAN and UPC information in less space. 
        /// This version is the same as RSSExpanded, but stacked in two rows when RSSExpanded is too wide.
        /// </summary>
        RSSExpandedStacked = 34,
        /// <summary>
        /// RSS Limited is a Reduced Space Symbology that encodes Composite Component (CC) extended EAN and UPC information in less space. 
        /// This version is a 14-digit EAN.UCC item identification with indicator digits of 0 or 1 in a small symbol that is not scanned by point-of-sale scanners.
        /// </summary>
        RSSLimited = 35,
        /// <summary>
        /// Data Matrix is a high density, two-dimensional barcode with square modules arranged in a square or rectangular matrix pattern.
        /// </summary>
        DataMatrix = 36,
        /// <summary>
        /// MicroPDF417 is two-dimensional (2D), multi-row symbology, derived from PDF417. 
        /// Micro-PDF417 is designed for applications that need to encode data in a two-dimensional (2D) symbol (up to 150 bytes, 250 alphanumeric characters, or 366 numeric digits) with the minimal symbol size.
        /// </summary>
        MicroPDF417 = 37,
        // new regular barcodes
        /// <summary>
        /// Intelligent Mail, formerly known as the 4-State Customer Barcode, is a 65-bar code used for domestic mail in the U.S.
        /// </summary>
        IntelligentMail = 64,
    }

    /// <summary>
    /// Specifies the barcode caption position relative to the barcode symbol.
    /// </summary>
    public enum BarCodeCaptionPosition
    {
        /// <summary>
        /// Caption is not printed.
        /// </summary>
        None = 0,
        /// <summary>
        /// Caption is printed above the barcode symbol.
        /// </summary>
        Above = 1,
        /// <summary>
        /// Caption is printed below the barcode symbol.
        /// </summary>
        Below = 2,
    }

    /// <summary>
    /// Specifies how an object or text in a control is horizontally aligned relative
    /// to an element of the control.
    /// </summary>
    public enum TextAlignment
    {
        /// <summary>
        /// Text is aligned to the left edge of barcode.
        /// </summary>
        Left,
        /// <summary>
        /// Text is centered within the barcode.
        /// </summary>
        Center,
        /// <summary>
        /// Text is aligned to the right edge of the barcode.
        /// </summary>
        Right
    }

    /// <summary>
    /// Specifies the print direction of the barcode symbol.
    /// </summary>
    public enum BarCodeDirection
    {
        /// <summary>
        /// The barcode symbol is printed left to right (default).
        /// </summary>
        LeftToRight = 0,
        /// <summary>
        /// The barcode symbol is printed right to left.
        /// </summary>
        RightToLeft = 1,
        /// <summary>
        /// The barcode symbol is printed top to bottom.
        /// </summary>
        TopToBottom = 2,
        /// <summary>
        /// The barcode symbol is printed bottom to top.
        /// </summary>
        BottomToTop = 3
    }

    /// <summary>
    /// Specifies the QRCode's model.
    /// </summary>
    public enum QRCodeModel
    {
        /// <summary>
        /// Model1.
        /// </summary>
        Model1 = 1,
        /// <summary>
        /// Model2.
        /// </summary>
        Model2 = 2
    }

    /// <summary>
    /// Specifies the QRCode's error correction level.
    /// </summary>
    public enum QRCodeErrorLevel
    {
        /// <summary>
        /// Low
        /// </summary>
        Low = 0,
        /// <summary>
        /// Medium
        /// </summary>
        Medium = 1,
        /// <summary>
        /// Quality
        /// </summary>
        Quality = 2,
        /// <summary>
        /// High
        /// </summary>
        High = 3,
    }

    /// <summary>
    /// Specifies the QRCode's mask pattern reference.
    /// </summary>
    public enum QRCodeMask
    {
        /// <summary>
        /// Auto configuration.
        /// </summary>
        Auto = -1,
        /// <summary>
        /// Pattern 000.
        /// </summary>
        Mask000 = 0,
        /// <summary>
        /// Pattern 001.
        /// </summary>
        Mask001 = 1,
        /// <summary>
        /// Pattern 010.
        /// </summary>
        Mask010 = 2,
        /// <summary>
        /// Pattern 011.
        /// </summary>
        Mask011 = 3,
        /// <summary>
        /// Pattern 100.
        /// </summary>
        Mask100 = 4,
        /// <summary>
        /// Pattern 101.
        /// </summary>
        Mask101 = 5,
        /// <summary>
        /// Pattern 110.
        /// </summary>
        Mask110 = 6,
        /// <summary>
        /// Pattern 111.
        /// </summary>
        Mask111 = 7,
    };

    /// <summary>
    /// Specifies PDF417's barcode type.
    /// </summary>
    public enum PDF417Type
    {
        /// <summary>
        /// Standard type.
        /// </summary>
        Normal = 0,
        /// <summary>
        /// Compact type (right indicator is neither displayed nor printed).
        /// </summary>
        Simple = 1,
    };

    /// <summary>
    ///  MicroPDF417 encoding compaction mode.
    /// </summary>
    public enum MicroPDF417SymbolCompactionMode
    {
        /// <summary>
        /// Auto, The compaction mode will be selected automatically to achieve a better compression efficiency.
        /// </summary>
        Auto,
        /// <summary>
        /// Text Compaction Mode.
        /// </summary>
        TextCompactionMode,
        /// <summary>
        /// Numeric Compaction Mode.
        /// </summary>
        NumericCompactionMode,
        /// <summary>
        /// Byte Compaction Mode.
        /// </summary>
        ByteCompactionMode
    }

    /// <summary>
    ///  MicroPDF417 encoding compaction mode.
    /// </summary>
    public enum MicroPDF417SymbolVersion
    {
        /// <summary>
        /// Symbol Size Auto
        /// The minimum symbol size with column prior that fits the data will be selected automatically.
        /// </summary>
        ColumnPriorAuto,
        /// <summary>
        /// Symbol Size Auto
        /// The minimum symbol size with row prior that fits the data will be selected automatically.
        /// </summary>
        RowPriorAuto,
        /// <summary>
        /// Version 1X11,Represents 1 column and 11 row.
        /// </summary>
        Version1X11,
        /// <summary>
        /// Version 1X14,Represents 1 column and 14 row.
        /// </summary>
        Version1X14,
        /// <summary>
        /// Version 1X17,Represents 1 column and 17 row.
        /// </summary>
        Version1X17,
        /// <summary>
        /// Version 1X20,Represents 1 column and 20 row.
        /// </summary>
        Version1X20,
        /// <summary>
        /// Version 1X24,Represents 1 column and 24 row.
        /// </summary>
        Version1X24,
        /// <summary>
        /// Version 1X28,Represents 1 column and 28 row.
        /// </summary>
        Version1X28,
        /// <summary>
        /// Version 2X8,Represents 2 column and 8 row.
        /// </summary>
        Version2X8,
        /// <summary>
        /// Version 2X11,Represents 2 column and 11 row.
        /// </summary>
        Version2X11,
        /// <summary>
        /// Version 2X14,Represents 2 column and 14 row.
        /// </summary>
        Version2X14,
        /// <summary>
        /// Version 2X17,Represents 2 column and 17 row.
        /// </summary>
        Version2X17,
        /// <summary>
        /// Version 2X20,Represents 2 column and 20 row.
        /// </summary>
        Version2X20,
        /// <summary>
        /// Version 2X23,Represents 2 column and 23 row.
        /// </summary>
        Version2X23,
        /// <summary>
        /// Version 2X26,Represents 2 column and 26 row.
        /// </summary>
        Version2X26,
        /// <summary>
        /// Version 3X6,Represents 3 column and 6 row.
        /// </summary>
        Version3X6,
        /// <summary>
        /// Version 3X8,Represents 3 column and 8 row.
        /// </summary>
        Version3X8,
        /// <summary>
        /// Version 3X10,Represents 3 column and 10 row.
        /// </summary>
        Version3X10,
        /// <summary>
        /// Version 3X12,Represents 3 column and 12 row.
        /// </summary>
        Version3X12,
        /// <summary>
        /// Version 3X15,Represents 3 column and 15 row.
        /// </summary>
        Version3X15,
        /// <summary>
        /// Version 3X20,Represents 3 column and 20 row.
        /// </summary>
        Version3X20,
        /// <summary>
        /// Version 3X26,Represents 3 column and 26 row.
        /// </summary>
        Version3X26,
        /// <summary>
        /// Version 3X32,Represents 3 column and 32 row.
        /// </summary>
        Version3X32,
        /// <summary>
        /// Version 3X38,Represents 3 column and 38 row.
        /// </summary>
        Version3X38,
        /// <summary>
        /// Version 3X44,Represents 3 column and 44 row.
        /// </summary>
        Version3X44,
        /// <summary>
        /// Version 4X4,Represents 4 column and 4 row.
        /// </summary>
        Version4X4,
        /// <summary>
        /// Version 4X6,Represents 4 column and 6 row.
        /// </summary>
        Version4X6,
        /// <summary>
        /// Version 4X8,Represents 4 column and 8 row.
        /// </summary>
        Version4X8,
        /// <summary>
        /// Version 4X10,Represents 4 column and 10 row.
        /// </summary>
        Version4X10,
        /// <summary>
        /// Version 4X12,Represents 4 column and 12 row.
        /// </summary>
        Version4X12,
        /// <summary>
        /// Version 4X15,Represents 4 column and 15 row.
        /// </summary>
        Version4X15,
        /// <summary>
        /// Version 4X20,Represents 4 column and 20 row.
        /// </summary>
        Version4X20,
        /// <summary>
        /// Version 4X26,Represents 4 column and 26 row.
        /// </summary>
        Version4X26,
        /// <summary>
        /// Version 4X32,Represents 4 column and 32 row.
        /// </summary>
        Version4X32,
        /// <summary>
        /// Version 4X38,Represents 4 column and 38 row.
        /// </summary>
        Version4X38,
        /// <summary>
        /// Version 4X44,Represents 4 column and 44 row.
        /// </summary>
        Version4X44,
    }

    /// <summary>
    /// Composite symbol type.
    /// </summary>
    public enum GS1CompositeType
    {
        /// <summary>
        /// Does not composite the GS1 barcode with a depenent barcode.
        /// </summary>
        None,
        /// <summary>
        /// Composite the GS1 barcode with a dependent CC-A barcode.
        /// </summary>
        CCA
    }

    /// <summary>
    /// The ECC mode enumeration.
    /// </summary>
    public enum DataMatrixEccMode
    {
        /// <summary>
        /// ECC000. This mode encodes data with only CRC checksum, no additional error checking bits.
        /// Data encoded with this ECC mode usually requiers the smallest symbol size.
        /// </summary>
        ECC000,
        /// <summary>
        /// ECC050.
        /// </summary>
        ECC050,
        /// <summary>
        /// ECC080.
        /// </summary>
        ECC080,
        /// <summary>
        /// ECC100.
        /// </summary>
        ECC100,
        /// <summary>
        /// ECC140.
        /// </summary>
        ECC140,
        /// <summary>
        /// ECC200. This is the suggested ECC mode.
        /// Data encoded with this ECC mode will be protedted Reed-Solomon error checking and correcting algorithm.
        /// </summary>
        ECC200
    }

    /// <summary>
    ///   The symbol size enumeration for ECC200 mode.
    /// </summary>
    public enum DataMatrixEcc200SymbolSize
    {
        /// <summary>
        /// SquareAuto.
        /// The minimum square symbol size that fits the data will be selected automatically.
        /// </summary>
        SquareAuto = -2,
        /// <summary>
        /// RectangularAuto.
        /// The minimum rectangular symbol size that fits the data will be selected automatically.
        /// </summary>
        RectangularAuto = -1,
        /// <summary>
        /// Square10. Represents 10 by 10 square symbol.
        /// </summary>
        Square10 = 0,
        /// <summary>
        /// Square12. Represents 12 by 12 square symbol.
        /// </summary>
        Square12,
        /// <summary>
        /// Square14. Represents 14 by 14 square symbol.
        /// </summary>
        Square14,
        /// <summary>
        /// Square16. Represents 16 by 16 square symbol.
        /// </summary>
        Square16,
        /// <summary>
        /// Square18. Represents 18 by 18 square symbol.
        /// </summary>
        Square18,
        /// <summary>
        /// Square20. Represents 20 by 20 square symbol.
        /// </summary>
        Square20,
        /// <summary>
        /// Square22. Represents 22 by 22 square symbol.
        /// </summary>
        Square22,
        /// <summary>
        /// Square24. Represents 24 by 24 square symbol.
        /// </summary>
        Square24,
        /// <summary>
        /// Square26. Represents 26 by 26 square symbol.
        /// </summary>
        Square26,
        /// <summary>
        /// Square32. Represents 32 by 32 square symbol.
        /// </summary>
        Square32,
        /// <summary>
        /// Square36. Represents 36 by 36 square symbol.
        /// </summary>
        Square36,
        /// <summary>
        /// Square40. Represents 40 by 40 square symbol.
        /// </summary>
        Square40,
        /// <summary>
        /// Square44. Represents 44 by 44 square symbol.
        /// </summary>
        Square44,
        /// <summary>
        /// Square48. Represents 48 by 48 square symbol.
        /// </summary>
        Square48,
        /// <summary>
        /// Square52. Represents 52 by 52 square symbol.
        /// </summary>
        Square52,
        /// <summary>
        /// Square64. Represents 64 by 64 square symbol.
        /// </summary>
        Square64,
        /// <summary>
        /// Square72. Represents 72 by 72 square symbol.
        /// </summary>
        Square72,
        /// <summary>
        /// Square80. Represents 80 by 80 square symbol.
        /// </summary>
        Square80,
        /// <summary>
        /// Square88. Represents 88 by 88 square symbol.
        /// </summary>
        Square88,
        /// <summary>
        /// Square96. Represents 96 by 96 square symbol.
        /// </summary>
        Square96,
        /// <summary>
        /// Square104. Represents 104 by 104 square symbol.
        /// </summary>
        Square104,
        /// <summary>
        /// Square120. Represents 120 by 120 square symbol.
        /// </summary>
        Square120,
        /// <summary>
        /// Square132. Represents 132 by 132 square symbol.
        /// </summary>
        Square132,
        /// <summary>
        /// Square144. Represents 144 by 144 square symbol.
        /// </summary>
        Square144,
        /// <summary>
        /// Rectangular8x18. Represents 8 by 18 rectangular symbol.
        /// </summary>
        Rectangular8x18,
        /// <summary>
        /// Rectangular8x32. Represents 8 by 32 rectangular symbol.
        /// </summary>
        Rectangular8x32,
        /// <summary>
        /// Rectangular12x26. Represents 12 by 26 rectangular symbol.
        /// </summary>
        Rectangular12x26,
        /// <summary>
        /// Rectangular12x36. Represents 12 by 36 rectangular symbol.
        /// </summary>
        Rectangular12x36,
        /// <summary>
        /// Rectangular16x36. Represents 16 by 36 rectangular symbol.
        /// </summary>
        Rectangular16x36,
        /// <summary>
        /// Rectangular16x48. Represents 16 by 48 rectangular symbol.
        /// </summary>
        Rectangular16x48
    }

    /// <summary>
    ///   The enumeration of encoding char set under ECC200 mode.
    /// </summary>
    public enum DataMatrixEcc200EncodingMode
    {
        /// <summary>
        /// Auto. The encoding char set will be selected automatically to achieve a better compression efficiency.
        /// </summary>
        Auto = -1,
        /// <summary>
        /// ASCII.
        /// </summary>
        ASCII = 0,
        /// <summary>
        /// C40.
        /// </summary>
        C40,
        /// <summary>
        /// Text.
        /// </summary>
        Text,
        /// <summary>
        /// X12. Only 40 X12 characters are supported by this char set.
        /// </summary>
        X12,
        /// <summary>
        /// EDIFACT. Only 63 EDIFACT characters are supported by this char set.
        /// </summary>
        EDIFACT,
        /// <summary>
        /// Base256.
        /// </summary>
        Base256
    }

    /// <summary>
    ///   The symbol size enumeration for ECC000-140 mode.
    /// </summary>
    public enum DataMatrixEcc000_140SymbolSize
    {
        /// <summary>
        /// Auto.
        /// The smallest symbol size which fits the data will be selected automatically.
        /// </summary>
        Auto = -1,
        /// <summary>
        /// Square9. Represents 9 by 9 square symbol.
        /// </summary>
        Square9 = 0,
        /// <summary>
        /// Square11. Represents 11 by 11 square symbol.
        /// </summary>
        Square11,
        /// <summary>
        /// Square13. Represents 13 by 13 square symbol.
        /// </summary>
        Square13,
        /// <summary>
        /// Square15. Represents 15 by 15 square symbol.
        /// </summary>
        Square15,
        /// <summary>
        /// Square17. Represents 17 by 17 square symbol.
        /// </summary>
        Square17,
        /// <summary>
        /// Square19. Represents 19 by 19 square symbol.
        /// </summary>
        Square19,
        /// <summary>
        /// Square21. Represents 21 by 21 square symbol.
        /// </summary>
        Square21,
        /// <summary>
        /// Square23. Represents 23 by 23 square symbol.
        /// </summary>
        Square23,
        /// <summary>
        /// Square25. Represents 25 by 25 square symbol.
        /// </summary>
        Square25,
        /// <summary>
        /// Square27. Represents 27 by 27 square symbol.
        /// </summary>
        Square27,
        /// <summary>
        /// Square29. Represents 29 by 29 square symbol.
        /// </summary>
        Square29,
        /// <summary>
        /// Square31. Represents 31 by 31 square symbol.
        /// </summary>
        Square31,
        /// <summary>
        /// Square33. Represents 33 by 33 square symbol.
        /// </summary>
        Square33,
        /// <summary>
        /// Square35. Represents 35 by 35 square symbol.
        /// </summary>
        Square35,
        /// <summary>
        /// Square37 Represents 37 by 37 square symbol.
        /// </summary>
        Square37,
        /// <summary>
        /// Square39. Represents 39 by 39 square symbol.
        /// </summary>
        Square39,
        /// <summary>
        /// Square41. Represents 41 by 41 square symbol.
        /// </summary>
        Square41,
        /// <summary>
        /// Square43. Represents 43 by 43 square symbol.
        /// </summary>
        Square43,
        /// <summary>
        /// Square45. Represents 45 by 45 square symbol.
        /// </summary>
        Square45,
        /// <summary>
        /// Square47. Represents 47 by 47 square symbol.
        /// </summary>
        Square47,
        /// <summary>
        /// Square49. Represents 49 by 49 square symbol.
        /// </summary>
        Square49
    }
}
