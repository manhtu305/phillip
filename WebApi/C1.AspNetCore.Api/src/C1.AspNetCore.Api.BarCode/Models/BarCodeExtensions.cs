﻿using System;
using System.Linq;
using System.Text;

namespace C1.Web.Api.BarCode
{
    internal static class BarCodeEnumExtensions
    {
        private static ToEnum ConvertToEnum<ToEnum, FromEnum>(FromEnum input)
        {
            if (typeof(FromEnum).IsEnum && typeof(ToEnum).IsEnum)
            {
                var enumString = Enum.GetName(typeof(FromEnum), input);
                if (Enum.IsDefined(typeof(ToEnum), enumString))
                {
                    return (ToEnum)Enum.Parse(typeof(ToEnum), enumString);
                }
            }
            return default(ToEnum);
        }

        internal static C1.BarCode.CodeType ToBarCodeEnum(this CodeType codeType)
        {
            return ConvertToEnum<C1.BarCode.CodeType, CodeType>(codeType);
        }

        internal static C1.BarCode.TextAlignment ToBarCodeEnum(this TextAlignment alignment)
        {
            return ConvertToEnum<C1.BarCode.TextAlignment, TextAlignment>(alignment);
        }

        internal static C1.BarCode.BarCodeCaptionPosition ToBarCodeEnum(this BarCodeCaptionPosition captionPosition)
        {
            return ConvertToEnum<C1.BarCode.BarCodeCaptionPosition, BarCodeCaptionPosition>(captionPosition);
        }

        internal static C1.BarCode.BarCodeDirection ToBarCodeEnum(this BarCodeDirection direction)
        {
            return ConvertToEnum<C1.BarCode.BarCodeDirection, BarCodeDirection>(direction);
        }

        internal static C1.BarCode.QRCodeErrorLevel ToBarCodeEnum(this QRCodeErrorLevel errorLevel)
        {
            return ConvertToEnum<C1.BarCode.QRCodeErrorLevel, QRCodeErrorLevel>(errorLevel);
        }

        internal static C1.BarCode.QRCodeMask ToBarCodeEnum(this QRCodeMask mask)
        {
            return ConvertToEnum<C1.BarCode.QRCodeMask, QRCodeMask>(mask);
        }

        internal static C1.BarCode.QRCodeModel ToBarCodeEnum(this QRCodeModel model)
        {
            return ConvertToEnum<C1.BarCode.QRCodeModel, QRCodeModel>(model);
        }
    }

    internal static class BarCodeOptionsExtensions
    {
        private static ToOptions ConvertToBarCodeOptions<ToOptions, FromOptions>(FromOptions opt) where ToOptions : class, new() where FromOptions : class
        {
            var toOptions = new ToOptions();
            var targetProps = typeof(ToOptions).GetProperties();

            foreach (var prop in typeof(FromOptions).GetProperties())
            {
                if (targetProps.Select(p => { return p.Name; }).Contains(prop.Name))
                {
                    try
                    {
                        var targetProp = targetProps.FirstOrDefault(p => { return p.Name == prop.Name; });

                        if (prop.PropertyType.IsEnum)
                        {
                            var val = prop.GetValue(opt);
                            var enumName = Enum.GetName(prop.PropertyType, val);
                            targetProp.SetValue(toOptions, Enum.Parse(targetProp.PropertyType, enumName));
                        }
                        else
                        {
                            var val = prop.GetValue(opt);
                            targetProp.SetValue(toOptions, val);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException is C1.BarCode.BarCodeException)
                        {
                            throw ex.InnerException;
                        }
                        else
                        {
                            throw ex;
                        }
                    }
                }
            }
            return toOptions;
        }

        internal static C1.BarCode.QuietZone ToBarCodeOptions(this QuietZone options)
        {
            return ConvertToBarCodeOptions<C1.BarCode.QuietZone, QuietZone>(options);
        }

        internal static C1.BarCode.QRCodeOptions ToBarCodeOptions(this QRCodeOptions options)
        {
            var barCodeOptions = ConvertToBarCodeOptions<C1.BarCode.QRCodeOptions, QRCodeOptions>(options);
            barCodeOptions.Encoding = Encoding.GetEncoding(options.CodePage);
            return barCodeOptions;
        }

        internal static C1.BarCode.PDF417Options ToBarCodeOptions(this PDF417Options options)
        {
            return ConvertToBarCodeOptions<C1.BarCode.PDF417Options, PDF417Options>(options);
        }

        internal static C1.BarCode.Code49Options ToBarCodeOptions(this Code49Options options)
        {
            return ConvertToBarCodeOptions<C1.BarCode.Code49Options, Code49Options>(options);
        }

        internal static C1.BarCode.RssExpandedStackedOptions ToBarCodeOptions(this RssExpandedStackedOptions options)
        {
            return ConvertToBarCodeOptions<C1.BarCode.RssExpandedStackedOptions, RssExpandedStackedOptions>(options);
        }

        internal static C1.BarCode.MicroPDF417Options ToBarCodeOptions(this MicroPDF417Options options)
        {
            return ConvertToBarCodeOptions<C1.BarCode.MicroPDF417Options, MicroPDF417Options>(options);
        }

        internal static C1.BarCode.Code25intlvOptions ToBarCodeOptions(this Code25intlvOptions options)
        {
            return ConvertToBarCodeOptions<C1.BarCode.Code25intlvOptions, Code25intlvOptions>(options);
        }

        internal static C1.BarCode.GS1CompositeOptions ToBarCodeOptions(this GS1CompositeOptions options)
        {
            return ConvertToBarCodeOptions<C1.BarCode.GS1CompositeOptions, GS1CompositeOptions>(options);
        }

        internal static C1.BarCode.Ean128Fnc1Options ToBarCodeOptions(this Ean128Fnc1Options options)
        {
            return ConvertToBarCodeOptions<C1.BarCode.Ean128Fnc1Options, Ean128Fnc1Options>(options);
        }

        internal static C1.BarCode.DataMatrixOptions ToBarCodeOptions(this DataMatrixOptions options)
        {
            return ConvertToBarCodeOptions<C1.BarCode.DataMatrixOptions, DataMatrixOptions>(options);
        }
    }
}
