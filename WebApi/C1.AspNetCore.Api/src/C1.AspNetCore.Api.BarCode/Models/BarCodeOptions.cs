﻿using System.Drawing;

namespace C1.Web.Api.BarCode
{
    /// <summary>
    /// A quiet zone is an area of blank space on either side of a barcode that tells the scanner where the symbology starts and stops.
    /// </summary>
    public class QuietZone
    {
        /// <summary>
        /// Gets or sets the left value of the barcode's QuietZone.
        /// </summary>
        public double Left { get; set; }

        /// <summary>
        /// Gets or sets the right value of the barcode's QuietZone.
        /// </summary>
        public double Right { get; set; }

        /// <summary>
        /// Gets or sets the top value of the barcode's QuietZone.
        /// </summary>
        public double Top { get; set; }

        /// <summary>
        /// Gets or sets the bottom value of the barcode's QuietZone.
        /// </summary>
        public double Bottom { get; set; }

        /// <summary>
        /// Initializes a new instance of QuietZone class.
        /// </summary>
        public QuietZone()
        {
            Left = 0;
            Right = 0;
            Top = 0;
            Bottom = 0;
        }
    }

    /// <summary>
    /// The options of QRCode
    /// </summary>
    public class QRCodeOptions
    {
        private const QRCodeModel c_DefModel = QRCodeModel.Model2;
        private const QRCodeErrorLevel c_DefErrorLevel = QRCodeErrorLevel.Low;
        private const int c_DefVersion = -1;
        private const QRCodeMask c_DefMask = QRCodeMask.Auto;
        private const bool c_DefConnection = false;
        private const int c_DefConnectionNumber = 0;
        private const int c_DefCodePage = 65001;

        /// <summary>
        /// Gets or sets the error correction level for the barcode.
        /// </summary>
        public QRCodeErrorLevel ErrorLevel { get; set; }
        /// <summary>
        /// Gets or sets the QRCode model.
        /// </summary>
        public QRCodeModel Model { get; set; }
        /// <summary>
        /// Gets or sets the QRCode version.
        /// </summary>
        /// <remarks>
        /// Specify any value between 1 and 14 when the Model property is set to Model1 
        /// and 1 to 40 for Model2. 
        /// When -1 is specified, the version most suited to the value is automatically determined
        /// </remarks>
        public int Version { get; set; }
        /// <summary>
        /// Gets or sets the pattern used for the barcode masking.
        /// </summary>
        public QRCodeMask Mask { get; set; }
        /// <summary>
        /// Gets or sets whether connection is used for the barcode.
        /// </summary>
        public bool Connection { get; set; }
        /// <summary>
        /// Gets or sets the connection number for the barcode.
        /// </summary>
        public int ConnectionNumber { get; set; }
        /// <summary>
        /// Gets or sets the CodePage of the barcode data.
        /// </summary>
        public int CodePage { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="QRCodeOptions"/> class.
        /// </summary>
        public QRCodeOptions()
        {
            Model = c_DefModel;
            ErrorLevel = c_DefErrorLevel;
            Version = c_DefVersion;
            Mask = c_DefMask;
            Connection = c_DefConnection;
            ConnectionNumber = c_DefConnectionNumber;
            CodePage = c_DefCodePage;
        }
    }

    /// <summary>
    /// The options of PDF417.
    /// </summary>
    public class PDF417Options
    {
        private const int c_DefColumn = -1;
        private const int c_DefRow = -1;
        private const int c_DefErrorLevel = -1;
        private const PDF417Type c_DefType = PDF417Type.Normal;

        /// <summary>
        /// Initializes a new instance of the <see cref="PDF417Options"/> class.
        /// </summary>
        public PDF417Options()
        {
            Column = c_DefColumn;
            Row = c_DefRow;
            ErrorLevel = c_DefErrorLevel;
            Type = c_DefType;
        }

        /// <summary>
        /// Gets or sets the error correction level for the barcode.
        /// </summary>
        public int ErrorLevel { get; set; }

        /// <summary>
        /// Gets or sets columns number for the barcode.
        /// </summary>
        public int Column { get; set; }

        /// <summary>
        /// Gets or sets row numbers for the barcode.
        /// </summary>
        public int Row { get; set; }

        /// <summary>
        /// Gets or set the PDF417 barcode's type.
        /// </summary>
        public PDF417Type Type { get; set; }
    }

    /// <summary>
    /// The options of Code49.
    /// </summary>
    public class Code49Options
    {
        private const bool c_DefGrouping = false;
        private const int c_DefGroup = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="Code49Options"/> class.
        /// </summary>
        public Code49Options()
        {
            Grouping = c_DefGrouping;
            Group = c_DefGroup;
        }

        /// <summary>
        /// Gets or sets a value indicating whether grouping is set for the barcode.
        /// </summary>
        public bool Grouping { get; set; }

        /// <summary>
        /// Gets or sets group numbers.
        /// </summary>
        public int Group { get; set; }
    }

    /// <summary>
    /// The options of RSSExpandedStacked
    /// </summary>
    public class RssExpandedStackedOptions
    {
        private const int c_DefRowCount = 2;

        /// <summary>
        /// Initializes a new instance of the <see cref="RssExpandedStackedOptions"/> class.
        /// </summary>
        public RssExpandedStackedOptions()
        {
            RowCount = c_DefRowCount;
        }

        /// <summary>
        /// Gets or sets the number of stacked rows.
        /// </summary>
        public int RowCount { get; set; }
    }

    /// <summary>
    /// The options of MicroPDF417
    /// </summary>
    public class MicroPDF417Options
    {
        private const MicroPDF417SymbolCompactionMode c_DefCompactionMode = MicroPDF417SymbolCompactionMode.Auto;
        private const MicroPDF417SymbolVersion c_DefVersion = MicroPDF417SymbolVersion.ColumnPriorAuto;
        private const int c_DefSegmentIndex = 0;
        private const int c_DefSegmentCount = 0;
        private const int c_DefFileID = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="MicroPDF417Options"/> class.
        /// </summary>
        public MicroPDF417Options()
        {
            SegmentCount = c_DefSegmentCount;
            SegmentIndex = c_DefSegmentIndex;
            FileID = c_DefFileID;
            CompactionMode = c_DefCompactionMode;
            Version = c_DefVersion;
        }

        /// <summary>
        /// Gets or sets the CompactionMode.
        /// </summary>
        public MicroPDF417SymbolCompactionMode CompactionMode { get; set; }

        /// <summary>
        /// Gets or sets the symbol size.
        /// </summary>
        public MicroPDF417SymbolVersion Version { get; set; }

        /// <summary>
        /// Gets or sets the segment index of structured append symbol.
        /// </summary>
        /// <remarks>
        /// Valid value is from 0 to 99998, and less than SegmentCount.
        /// </remarks>
        public int SegmentIndex { get; set; }

        /// <summary>
        /// Gets or sets the segment count of structured append symbol.
        /// </summary>
        /// <remarks>
        /// Valid value is from 0 to 99999.
        /// </remarks>
        public int SegmentCount { get; set; }

        /// <summary>
        /// Gets or sets the file id of structured append symbol.
        /// </summary>
        /// <remarks>
        /// Valid value is from 0 to 899.
        /// </remarks>
        public int FileID { get; set; }
    }

    /// <summary>
    /// The options of Code25intlv.
    /// </summary>
    public class Code25intlvOptions
    {
        private const bool c_DefBearBar = false;
        private const int c_DefLineStrokeThickness = 1;
        private static readonly Color c_DefLineStroke = Color.Black;

        /// <summary>
        /// Initializes a new instance of the <see cref="Code25intlvOptions"/> class.
        /// </summary>
        public Code25intlvOptions()
        {
            LineStrokeThickness = c_DefLineStrokeThickness;
            BearBar = c_DefBearBar;
            LineStroke = c_DefLineStroke;
        }

        /// <summary>
        /// Whether or not to display  bearer bar to ITF barcode.
        /// </summary>
        public bool BearBar { get; set; }

        /// <summary>
        /// The line width of bearer bar.
        /// </summary>
        public int LineStrokeThickness { get; set; }

        /// <summary>
        /// The color of bearer bar.
        /// </summary>
        public Color LineStroke { get; set; }
    }

    /// <summary>
    /// The options of GS1Composite
    /// </summary>
    public class GS1CompositeOptions
    {
        private const GS1CompositeType c_DefType = GS1CompositeType.None;

        /// <summary>
        /// Initializes a new instance of the <see cref="GS1CompositeOptions" /> class.
        /// </summary>
        public GS1CompositeOptions()
        {
            Type = c_DefType;
            Value = string.Empty;
        }

        /// <summary>
        /// Gets or sets the composite symbol type. Default value is None.
        /// </summary>
        public GS1CompositeType Type { get; set; }

        /// <summary>
        /// Gets or sets the CCA or CCB character data.
        /// </summary>
        public string Value { get; set; }
    }

    /// <summary>
    /// The options of Code128
    /// </summary>
    public class Ean128Fnc1Options
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Ean128Fnc1Options"/> class.
        /// </summary>
        public Ean128Fnc1Options()
        {
            Dpi = 0;
            BarAdjust = 0;
            ModuleSize = 0;
        }

        /// <summary>
        /// Gets or sets the resolution of the printer.
        /// </summary>
        public int Dpi { get; set; }

        /// <summary>
        /// Gets or sets the adjustment size by dot.
        /// </summary>
        public int BarAdjust { get; set; }

        /// <summary>
        /// Gets or sets the horizontal size of the barcode module.
        /// </summary>
        public int ModuleSize { get; set; }
    }

    /// <summary>
    /// The options of DataMatrix.
    /// </summary>
    public class DataMatrixOptions
    {
        private const DataMatrixEccMode c_DefEccMode = DataMatrixEccMode.ECC200;
        private const DataMatrixEcc200SymbolSize c_DefEcc200SymbolSize = DataMatrixEcc200SymbolSize.SquareAuto;
        private const DataMatrixEcc200EncodingMode c_DefEcc200EncodingMode = DataMatrixEcc200EncodingMode.Auto;
        private const DataMatrixEcc000_140SymbolSize c_DefEcc000_140SymbolSize = DataMatrixEcc000_140SymbolSize.Auto;
        private const bool c_DefStructuredAppend = false;
        private const int c_DefStructureNumber = 0;
        private const byte c_DefFileIdentifier = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="DataMatrixOptions"/> class.
        /// </summary>
        public DataMatrixOptions()
        {
            EccMode = c_DefEccMode;
            Ecc200SymbolSize = c_DefEcc200SymbolSize;
            Ecc200EncodingMode = c_DefEcc200EncodingMode;
            Ecc000_140SymbolSize = c_DefEcc000_140SymbolSize;
            StructuredAppend = c_DefStructuredAppend;
            StructureNumber = c_DefStructureNumber;
            FileIdentifier = c_DefFileIdentifier;
        }

        /// <summary>
        /// Gets or sets the ECC mode.
        /// </summary>
        /// <value>
        /// The ECC mode.
        /// </value>
        public DataMatrixEccMode EccMode { get; set; }

        /// <summary>
        /// Gets or sets the size of the ECC200 symbol.
        /// </summary>
        /// <value>
        /// The size of the ECC200 symbol.
        /// </value>
        public DataMatrixEcc200SymbolSize Ecc200SymbolSize { get; set; }

        /// <summary>
        /// Gets or sets the ECC200 encoding mode.
        /// </summary>
        /// <value>
        /// The ECC200 encoding mode.
        /// </value>
        public DataMatrixEcc200EncodingMode Ecc200EncodingMode { get; set; }

        /// <summary>
        /// Gets or sets the size of the ECC000-140 symbol.
        /// </summary>
        /// <value>
        /// The size of the ECC000-140 symbol.
        /// </value>
        public DataMatrixEcc000_140SymbolSize Ecc000_140SymbolSize { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the current symbol is part of structured append symbols.
        /// </summary>
        /// <remarks>
        /// Structured append is only supported by ECC200.
        /// </remarks>
        /// <value>
        /// <c>true</c> if the current symbol is part of structured append symbols; otherwise, <c>false</c>.
        /// </value>
        public bool StructuredAppend { get; set; }

        /// <summary>
        /// Gets or sets the structure number of current symbol within the structuerd append symbols.
        /// </summary>
        /// <remarks>
        /// The structure number will only be used when ECC mode is <see cref="T:DataMatrixEccMode.ECC200"/>.
        /// </remarks>
        /// <value>
        /// The structure number of current symbol within the structured append symbols.
        /// The structure number starts from 0, and should be less than both the symbol amount and the max symbol count(16).
        /// </value>
        public int StructureNumber { get; set; }

        /// <summary>
        /// Gets or sets the file identifier of a related group of structured append symbols.
        /// </summary>
        /// <remarks>
        /// The valid file indentifier value should be within [1,254],
        /// set file identifier to 0 lets the file identifier of the symbols calculated automatically.
        /// </remarks>
        /// <value>
        /// The file identifier.
        /// </value>
        public byte FileIdentifier { get; set; }
    }
}
