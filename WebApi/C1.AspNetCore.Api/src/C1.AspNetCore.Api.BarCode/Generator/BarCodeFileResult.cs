﻿#if NETCORE
using GrapeCity.Documents.Imaging;
#endif
using System.IO;
using System.Threading.Tasks;

namespace C1.Web.Api.BarCode
{
    internal class BarCodeFileResult : ExporterResult
    {
#if NETCORE
        private GcBitmap _image;
        public BarCodeFileResult(ExportSource source, GcBitmap image) : base(source)
        {
            _image = image;
        }
#else
        private System.Drawing.Image _image;
        public BarCodeFileResult(ExportSource source, System.Drawing.Image image) : base(source)
        {
            _image = image;
        }
#endif

        internal override Task ExportCoreAsync(Stream outputStream)
        {
#if NETCORE
            switch (ExportSource.Type)
            {
                case ExportFileType.Png:
                    _image.SaveAsPng(outputStream);
                    break;
                case ExportFileType.Jpeg:
                    _image.SaveAsJpeg(outputStream);
                    break;
                case ExportFileType.Bmp:
                    _image.SaveAsBmp(outputStream);
                    break;
                case ExportFileType.Gif:
                    _image.SaveAsGif(outputStream);
                    break;
                case ExportFileType.Tiff:
                    _image.SaveAsTiff(outputStream);
                    break;
                default:
                    _image.SaveAsPng(outputStream);// Keep same behavior as WebAPI classic
                    break;
            }
#else
            _image.Save(outputStream, ExportSource.Type.ToImageFormat());
#endif
            return Task.FromResult(0);
        }
    }
}
