﻿using C1.Util.Licensing;

#if !ASPNETCORE && !NETCORE
using IActionResult = System.Web.Http.IHttpActionResult;
using Controller = System.Web.Http.ApiController;
using System.ComponentModel;
#else
using Microsoft.AspNetCore.Mvc;
using System;
#if NETCORE
using GrapeCity.Documents.Imaging;
using GrapeCity.Documents.Barcode;
using GrapeCity.Documents.Text;
#endif
#endif
using System.Drawing;

namespace C1.Web.Api.BarCode
{
    /// <summary>
    /// Generator the barcode image 
    /// </summary>
    public class BarCodeGenerator
    {
        private BarCodeRequest _request;
        private readonly bool _needRenderEvalInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="BarCodeGenerator"/> class.
        /// </summary>
        public BarCodeGenerator()
            : this(null)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BarCodeGenerator"/> class.
        /// </summary>
        /// <param name="request">The request data of generating the barcode.</param>
        public BarCodeGenerator(BarCodeRequest request)
        {
            _needRenderEvalInfo = LicenseHelper.NeedRenderEvalInfo<LicenseDetector>();
            _request = request;
        }

        /// <summary>
        /// Gets the barcode image according to the specified request.
        /// </summary>
        public IActionResult GetResult(Controller controller)
        {
#if NETCORE
            GcBitmap
#else
            System.Drawing.Image
#endif
            image = GetBarCodeImage(_request);
            return new BarCodeFileResult(_request, image);
        }

#if NETCORE

        private SizeF CreateDefaultBarcodeSize()
        {
            return new SizeF(170, 40);
        }

        private GcBitmap DrawBarcodeEvalInfo(GcBarcode barcode)
        {
            int barcodeWidth = (_request.BarWidth <= 0) ? BarCodeRequest.DefaultWidth : _request.BarWidth;
            int barcodeHeight = (_request.BarHeight <= 0) ? BarCodeRequest.DefaultHeight : _request.BarHeight;
            GcBitmap bmp = new GcBitmap(2 * barcodeWidth, 2 * barcodeHeight, false, _request.DPIs.Horizontal, _request.DPIs.Vertical);
            using (GcBitmapGraphics g = bmp.CreateGraphics(_request.BackColor))
            {
                // Do not render caption
                SizeF bcSize = g.MeasureBarcode(barcode);

                if (bcSize.IsEmpty)
                {
                    bcSize = CreateDefaultBarcodeSize();
                }

                string evaluationMessage = "ComponentOne";
                SizeF emSize;
                RectangleF bcRect;

                if (barcode.Options.CaptionPosition == BarCodeCaptionPosition.Above)
                {
                    bcRect = new RectangleF(new PointF(0, 0), bcSize);
                    g.DrawBarcode(barcode, bcRect);

                    emSize = g.MeasureString(evaluationMessage, barcode.TextFormat, bcSize.Width - 10);
                    g.DrawString(evaluationMessage, barcode.TextFormat, new PointF(5, bcSize.Height));
                }

                else
                {
                    emSize = g.MeasureString(evaluationMessage, barcode.TextFormat, bcSize.Width - 10);
                    g.DrawString(evaluationMessage, barcode.TextFormat, new PointF(5, 0));

                    bcRect = new RectangleF(new PointF(0, barcodeHeight / 2 - emSize.Height / 2), bcSize);
                    g.DrawBarcode(barcode, bcRect);

                }

                return bmp.Clip(new Rectangle(0, 0, (int)(Math.Ceiling(bcSize.Width)), (int)(Math.Ceiling(bcSize.Height + emSize.Height))));
            }

        }

        private GcBitmap DrawBarCode(GcBarcode barcode)
        {
            int barcodeWidth = (_request.BarWidth <= 0) ? BarCodeRequest.DefaultWidth : _request.BarWidth;
            int barcodeHeight = (_request.BarHeight <= 0) ? BarCodeRequest.DefaultHeight : _request.BarHeight;
            GcBitmap bmp = new GcBitmap(2 * barcodeWidth, 2 * barcodeHeight, false, _request.DPIs.Horizontal, _request.DPIs.Vertical);
            using (GcBitmapGraphics g = bmp.CreateGraphics(_request.BackColor))
            {
                SizeF bcSize = g.MeasureBarcode(barcode);
                RectangleF bcRect = new RectangleF(new PointF(0, 0), bcSize);
                g.DrawBarcode(barcode, bcRect);
                return bmp.Clip(new Rectangle(0, 0, (int)(Math.Ceiling(bcSize.Width)), (int)(Math.Ceiling(bcSize.Height))));
            }
        }
        private GcBitmap DrawEmptyBarCode(GcBarcode barcode)
        {
            int barcodeWidth = (_request.BarWidth <= 0) ? BarCodeRequest.DefaultWidth : _request.BarWidth;
            int barcodeHeight = (_request.BarHeight <= 0) ? BarCodeRequest.DefaultHeight : _request.BarHeight;
            GcBitmap bmp = new GcBitmap(2 * barcodeWidth, 2 * barcodeHeight, false, _request.DPIs.Horizontal, _request.DPIs.Vertical);
            using (GcBitmapGraphics g = bmp.CreateGraphics(_request.BackColor))
            {
                barcodeWidth = 1;
                if (!String.IsNullOrEmpty(barcode.Text) && barcode.Options.CaptionPosition != BarCodeCaptionPosition.None)
                {
                    SizeF textSize = g.MeasureString(barcode.Text, barcode.TextFormat);
                    barcodeWidth = (int)(Math.Ceiling(textSize.Width));
                    barcodeHeight = (int)(Math.Ceiling(barcodeHeight + textSize.Height));
                }
                return bmp.Resize(barcodeWidth, barcodeHeight);
            }
        }

        private GcBitmap DrawBarCodeException(GcBarcode barcode, BarCodeException exception)
        {
            int barcodeWidth = (_request.BarWidth <= 0) ? BarCodeRequest.DefaultWidth : _request.BarWidth;
            int barcodeHeight = (_request.BarHeight <= 0) ? BarCodeRequest.DefaultHeight : _request.BarHeight;
            if (_request.BackColor != Color.Red)
            {
                // ErrorMessage is RED, but if BackColor is RED we set error message color as ForColor
                barcode.TextFormat.ForeColor = Color.Red;
            }
            GcBitmap bmp = new GcBitmap(barcodeWidth, barcodeHeight, false, _request.DPIs.Horizontal, _request.DPIs.Vertical);
            using (GcBitmapGraphics g = bmp.CreateGraphics(_request.BackColor))
            {
                SizeF emSize = g.MeasureString(exception.Message, barcode.TextFormat, barcodeWidth - 10);
                g.DrawString(exception.Message, barcode.TextFormat, new PointF(5, barcodeHeight / 2 - emSize.Height / 2));
                return bmp;
            }
        }

        private GcBitmap GetBarCodeImage(BarCodeRequest request)
        {
            GcBarcode bc = new GcBarcode();
            bc.Text = request.Text;
            bc.CodeType = request.CodeType;
            bc.Options.SizeOptions.BarHeight = request.BarHeight;
            bc.Options.SupplementNumber = request.AdditionalNumber;
            bc.Options.TextAlign = request.CaptionAlignment;
            bc.Options.CaptionPosition = request.CaptionPosition;
            bc.Options.BarCodeDirection = request.BarDirection;
            bc.Options.SizeOptions.ModuleSize = request.ModuleSize;
            bc.Options.CaptionGrouping = request.CaptionGrouping;
            bc.Options.CheckSumEnabled = request.CheckSumEnabled;
            bc.Options.QRCode = request.QRCodeOptions;
            bc.Options.PDF417 = request.PDF417Options;
            bc.Options.Code49 = request.Code49Options;
            bc.Options.RssExpandedStacked = request.RssExpandedStackedOptions;
            bc.Options.MicroPDF417 = request.MicroPDF417Options;
            bc.Options.GS1Composite = request.GS1CompositeOptions;
            bc.Options.DataMatrix = request.DataMatrixOptions;
            bc.HorizontalAlignment = request.ImgAlign.Horizontal;
            bc.VerticalAlignment = request.ImgAlign.Vertical;

            //bc.Options.QuietZone = request.QuietZone.ToBarCodeOptions();// NOT supported
            //c1BarCode.TextFixedLength = request.TextFixedLength;
            //bc.Options.Code25intlv = request.Code25intlvOptions;// NOT supported
            //bc.Options.Ean128Fnc1 = request.Ean128Fnc1Options;// NOT supported

            FontStyle fontStyle = FontStyle.Regular;
            if (request.Font.Bold)
            {
                fontStyle |= FontStyle.Bold;
            }
            if (request.Font.Italic)
            {
                fontStyle |= FontStyle.Italic;
            }

            TextFormat textFormat = new TextFormat();
            textFormat.ForeColor = request.ForeColor;
            textFormat.FontSize = request.Font.Size;
            textFormat.FontStyle = fontStyle;
            textFormat.Strikethrough = request.Font.Strikeout;
            textFormat.Underline = request.Font.Underline;
            textFormat.FontName = request.Font.Family;
            bc.TextFormat = textFormat;

            try
            {
                if (_needRenderEvalInfo)
                {
                    return DrawBarcodeEvalInfo(bc);
                }

                if (request.CodeType == CodeType.None || String.IsNullOrEmpty(_request.Text))
                {
                    return DrawEmptyBarCode(bc);
                }

                return DrawBarCode(bc);
            }
            catch (BarCodeException exception)
            {
                return DrawBarCodeException(bc, exception);
            }
        }

#else
    private System.Drawing.Image GetBarCodeImage(BarCodeRequest request)
        {
            var c1BarCode = new C1.Win.BarCode.C1BarCode();
            c1BarCode.NeedRenderEvalInfo = _needRenderEvalInfo;
            try
            {
                c1BarCode.Text = request.Text;
                c1BarCode.CodeType = request.CodeType.ToBarCodeEnum();
                c1BarCode.BarWidth = request.BarWidth;
                c1BarCode.BarHeight = request.BarHeight;
                c1BarCode.BackColor = request.BackColor;
                c1BarCode.ForeColor = request.ForeColor;
                c1BarCode.AdditionalNumber = request.AdditionalNumber;
                c1BarCode.CaptionAlignment = request.CaptionAlignment.ToBarCodeEnum();
                c1BarCode.CaptionPosition = request.CaptionPosition.ToBarCodeEnum();
                c1BarCode.BarDirection = request.BarDirection.ToBarCodeEnum();
                c1BarCode.QuietZone = request.QuietZone.ToBarCodeOptions();
                c1BarCode.ModuleSize = request.ModuleSize;
                c1BarCode.TextFixedLength = request.TextFixedLength;
                c1BarCode.CaptionGrouping = request.CaptionGrouping;
                c1BarCode.CheckSumEnabled = request.CheckSumEnabled;
                c1BarCode.QRCodeOptions = request.QRCodeOptions.ToBarCodeOptions();
                c1BarCode.PDF417Options = request.PDF417Options.ToBarCodeOptions();
                c1BarCode.Code49Options = request.Code49Options.ToBarCodeOptions();
                c1BarCode.RssExpandedStackedOptions = request.RssExpandedStackedOptions.ToBarCodeOptions();
                c1BarCode.MicroPDF417Options = request.MicroPDF417Options.ToBarCodeOptions();
                c1BarCode.Code25intlvOptions = request.Code25intlvOptions.ToBarCodeOptions();
                c1BarCode.GS1CompositeOptions = request.GS1CompositeOptions.ToBarCodeOptions();
                c1BarCode.Ean128Fnc1Options = request.Ean128Fnc1Options.ToBarCodeOptions();
                c1BarCode.DataMatrixOptions = request.DataMatrixOptions.ToBarCodeOptions();

                var fontStyle = FontStyle.Regular;
                if (request.Font.Bold)
                {
                    fontStyle |= FontStyle.Bold;
                }
                if (request.Font.Italic)
                {
                    fontStyle |= FontStyle.Italic;
                }
                if (request.Font.Underline)
                {
                    fontStyle |= FontStyle.Underline;
                }
                if (request.Font.Strikeout)
                {
                    fontStyle |= FontStyle.Strikeout;
                }
                c1BarCode.Font = new System.Drawing.Font(request.Font.Family, request.Font.Size, fontStyle);

                return c1BarCode.Image;
            }
            catch (C1.BarCode.BarCodeException e)
            {
                return c1BarCode.RenderExceptionImage(e.Message);
            }
        }
#endif
    }
}
