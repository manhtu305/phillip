﻿using System;

namespace C1.BarCode
{
    /// <summary>
    /// C1DescriptionAttribute replaces the DescriptionAttribute
    /// and uses the StringsManager and Strings classes to
    /// return the localized Attribute string.
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    internal class C1DescriptionAttribute : Attribute
    {
        private Type _stringsType;
        private string _key;
        private string _description;

        #region Constructors
        public C1DescriptionAttribute(
            string key,
            string description)
            : this(null, key, description)
        {
        }

        public C1DescriptionAttribute(
            Type stringsType,
            string key,
            string description)
        {
            _stringsType = stringsType;
            _key = key;
            _description = description;
        }
        #endregion

        #region Public properties
        public string Description
        {
            get
            {
                return _description;
            }
        }
        #endregion
    }
}

namespace C1.BarCode.SmartAssembly.Attributes
{
    internal class DoNotObfuscateTypeAttribute : Attribute
    {
    }
}
