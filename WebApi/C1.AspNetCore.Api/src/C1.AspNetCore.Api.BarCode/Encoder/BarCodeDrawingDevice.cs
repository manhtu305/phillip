﻿using C1.BarCode;
using C1.Win.Interop;
using System.Drawing;

namespace C1.Win.BarCode
{
    /// <summary>
    /// Implement IDrawingDevice interface to render barcode.
    /// </summary>
    internal class BarCodeDrawingDevice : DrawingDeviceBase, IDrawingDevice
    {
        #region fields

        private Graphics _graphics;
        private Font _font = new Font(FontFamily.GenericSansSerif, 20);
        private Color _backColor = Color.White;
        private Color _foreColor = Color.Black;
        private BarCodeDirection _direction = BarCodeDirection.LeftToRight;
        private TextAlignment _horizontalAlignment;
        #endregion

        #region ctors

        public BarCodeDrawingDevice(Graphics graphics, Font font, TextAlignment horizontalAlignment)
            : base(BarCodeDirection.LeftToRight, Rect.Empty)
        {
            _graphics = graphics;
            _font = font;
            _horizontalAlignment = horizontalAlignment;
            //_graphics.Clear(_backColor);
        }

        public BarCodeDrawingDevice(Graphics graphics, BarCodeDirection direction, Rect bounds, Color backColor,
            Color foreColor, Font font, TextAlignment horizontalAlignment)
            : base(direction, bounds)
        {
            _graphics = graphics;
            _direction = direction;
            _backColor = backColor;
            _foreColor = foreColor;
            _font = font;
            _horizontalAlignment = horizontalAlignment;
            graphics.Transform = Transform;
        }

        #endregion

        #region public methods

        /// <summary>
        /// Calculate the size of text.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public C1.Win.Interop.Size MeasureString(string text)
        {
            // <<IP>> make it in WinForms way
            SizeF sf = _graphics.MeasureString(text, _font);
            return new Interop.Size(sf.Width, sf.Height);
        }

        /// <summary>
        /// Draw rectangle.
        /// </summary>
        /// <param name="bounds"></param>
        public void FillRectangle(Rect bounds)
        {
            using (var brush = new SolidBrush(_foreColor))
            {
                _graphics.FillRectangle(brush, (float)bounds.TopLeft.X, (float)bounds.TopLeft.Y, (float)bounds.Size.Width, (float)bounds.Size.Height);
            }
        }

        /// <summary>
        /// Draw rectangle.
        /// </summary>
        /// <param name="bounds"></param>
        /// <param name="strokeThickness"></param>
        /// <param name="stroke"></param>
        public void DrawRectangle(Rect bounds, int strokeThickness, Color stroke)
        {
            _graphics.Clear(Color.Blue);
        }

        /// <summary>
        /// Draw string.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="point"></param>
        public void DrawString(string text, C1.Win.Interop.Point point)
        {
            using (var foreBrush = new SolidBrush(_foreColor)) // <<IP>> dispose brush as soon as we don't need it
            {
                if (_horizontalAlignment == TextAlignment.Right)
                {
                    var stringSize = MeasureString(text);
                    _graphics.DrawString(text, _font, foreBrush, (float)point.X + (float)stringSize.Width, (float)point.Y,
                        new StringFormat(StringFormatFlags.NoClip){ LineAlignment = StringAlignment.Near, Alignment = StringAlignment.Far});
                }
                else
                    _graphics.DrawString(text, _font, foreBrush, (float)point.X, (float)point.Y);
            }
        }

        #endregion

        #region public properties
        public Font Font
        {
            get { return _font; }
        }

        public Color ForeColor
        {
            get { return _foreColor; }
        }

        public Color FillColor
        {
            get { return _backColor; }
        }

        #endregion
    }
}
