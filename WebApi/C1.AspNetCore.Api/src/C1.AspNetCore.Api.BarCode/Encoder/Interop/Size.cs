using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;

namespace C1.Win.Interop
{
    /// <summary>
    /// Stores an ordered pair of <b>double</b> values, typically the width and height of a rectangle.
    /// </summary>
    public struct Size
    {
        private static Size s_Empty = new Size();
        private double _width;
        private double _height;

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Size"/> structure.
        /// </summary>
        /// <param name="width">The horizontal component of the <see cref="Size"/>.</param>
        /// <param name="height">The vertical component of the <see cref="Size"/>.</param>
        public Size(double width, double height)
        {
            _width = width;
            _height = height;
        }
        #endregion

        #region Public
        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        public override bool Equals(object obj)
        {
            if (!(obj is Size))
                return false;
            Size s = (Size)obj;
            return _width == s._width && _height == s._height;
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        public override int GetHashCode()
        {
            return (int)_width ^ (int)_height;
        }

        /// <summary>
        /// Converts the current <see cref="Size"/> structure to a human-readable string representing it.
        /// </summary>
        /// <returns>The string representing the current <see cref="Size"/>.</returns>
        public override string ToString()
        {
            return string.Format("{{Width={0}, Height={1}}}", _width, _height);
        }

        /// <summary>
        /// Converts the current <see cref="Size"/> structure to a <see cref="Point"/> by rounding the 
        /// width and height to the next higher integer values. 
        /// </summary>
        /// <returns>The <see cref="Size"/> structure this method converts to.</returns>
        public System.Drawing.Size Ceiling()
        {
            return new System.Drawing.Size((int)Math.Ceiling(_width), (int)Math.Ceiling(_height));
        }

        /// <summary>
        /// Converts the current <see cref="Size"/> structure to a <see cref="Point"/> by truncating the 
        /// width and height to integer values. 
        /// </summary>
        /// <returns>The <see cref="Size"/> structure this method converts to.</returns>
        public System.Drawing.Size Truncate()
        {
            return new System.Drawing.Size((int)_width, (int)_height);
        }
        #endregion

        #region Public static
        /// <summary>
        /// Converts a <see cref="System.Drawing.Size"/> to a <see cref="Size"/>.
        /// </summary>
        /// <param name="value">The <see cref="Size"/> to convert.</param>
        /// <returns>The converted <see cref="Size"/>.</returns>
        public static implicit operator Size(System.Drawing.Size value)
        {
            return new Size(value.Width, value.Height);
        }

        /// <summary>
        /// Converts a <see cref="SizeF"/> to a <see cref="Size"/>.
        /// </summary>
        /// <param name="value">The <see cref="SizeF"/> to convert.</param>
        /// <returns>The converted <see cref="Size"/>.</returns>
        public static implicit operator Size(SizeF value)
        {
            return new Size(value.Width, value.Height);
        }

        /// <summary>
        /// Converts a <see cref="Size"/> to a <see cref="Point"/>.
        /// </summary>
        /// <param name="value">The <see cref="Size"/> to convert.</param>
        /// <returns>The converted <see cref="Point"/>.</returns>
        public static explicit operator Point(Size value)
        {
            return new Point(value.Width, value.Height);
        }

        /// <summary>
        /// Converts a string to a <see cref="Size"/> structure.
        /// </summary>
        /// <param name="s">The string to convert.</param>
        /// <param name="result">OUT: the created <see cref="Size"/> structure.</param>
        /// <param name="throwException">Indicates whether an exception should be thrown if the string cannot be converted.</param>
        /// <returns><b>true</b> if no error occurred, <b>false</b> otherwise (if <paramref name="throwException"/> is <b>false</b>).</returns>
        public static bool Parse(string s, out Size result, bool throwException)
        {
            result = Size.Empty;
            string[] elems = s.Split(',');
            if (elems.Length != 2)
            {
                if (throwException)
                    throw new Exception(string.Format(Strings.InvalidString, s, typeof(Size).Name));
                return false;
            }

            if (double.TryParse(elems[0], NumberStyles.Any, CultureInfo.InvariantCulture, out result._width) &&
                double.TryParse(elems[0], NumberStyles.Any, CultureInfo.InvariantCulture, out result._height))
                return true;

            if (throwException)
                throw new Exception(string.Format(Strings.InvalidString, s, typeof(Size).Name));
            return false;
        }

        /// <summary>
        /// Adds two <see cref="Size"/> structures.
        /// </summary>
        /// <param name="sz1">The first <see cref="Size"/> that is added.</param>
        /// <param name="sz2">The second <see cref="Size"/> that is added.</param>
        /// <returns>A <see cref="Size"/> representing the result of the addition.</returns>
        public static Size Add(Size sz1, Size sz2)
        {
            return new Size(sz1.Width + sz2.Width, sz1.Height + sz2.Height);
        }

        /// <summary>
        /// Subtracts one <see cref="Size"/> structure from another.
        /// </summary>
        /// <param name="sz1">The <see cref="Size"/> that is subtracted from.</param>
        /// <param name="sz2">The <see cref="Size"/> that is subtracted.</param>
        /// <returns>A <see cref="Size"/> representing the result of the subtraction.</returns>
        public static Size Substract(Size sz1, Size sz2)
        {
            return new Size(sz1.Width - sz2.Width, sz1.Height - sz2.Height);
        }

        /// <summary>
        /// Subtracts one <see cref="Size"/> structure from another.
        /// </summary>
        /// <param name="sz1">The <see cref="Size"/> that is subtracted from.</param>
        /// <param name="sz2">The <see cref="Size"/> that is subtracted.</param>
        /// <returns>A <see cref="Size"/> representing the result of the subtraction.</returns>
        public static Size operator -(Size sz1, Size sz2)
        {
            return Size.Substract(sz1, sz2);
        }

        /// <summary>
        /// Adds two <see cref="Size"/> structures.
        /// </summary>
        /// <param name="sz1">The first <see cref="Size"/> that is added.</param>
        /// <param name="sz2">The second <see cref="Size"/> that is added.</param>
        /// <returns>A <see cref="Size"/> representing the result of the addition.</returns>
        public static Size operator +(Size sz1, Size sz2)
        {
            return Size.Add(sz1, sz2);
        }
        #endregion

        #region Operators
        /// <summary>
        /// Indicates whether two <see cref="Size"/> structures are equal.
        /// </summary>
        public static bool operator ==(Size x, Size y)
        {
            return x._width == y._width && x._height == y._height;
        }

        /// <summary>
        /// Indicates whether two <see cref="Size"/> structures are not equal.
        /// </summary>
        public static bool operator !=(Size x, Size y)
        {
            return x._width != y._width || x._height != y._height;
        }
        #endregion

        #region Public properties
        /// <summary>
        /// Gets or sets the horizontal component of the current <see cref="Size"/>.
        /// </summary>
        public double Width
        {
            get { return _width; }
            set { _width = value; }
        }

        /// <summary>
        /// Gets or sets the vertical component of the current <see cref="Size"/>.
        /// </summary>
        public double Height
        {
            get { return _height; }
            set { _height = value; }
        }

        /// <summary>
        /// Gets a value indicating whether of the current <see cref="Size"/> has zero <see cref="Width"/> and <see cref="Height"/>.
        /// </summary>
        [Browsable(false)]
        public bool IsEmpty
        {
            get { return _width == 0 && _height == 0; }
        }
        #endregion

        #region Public static properties
        /// <summary>
        /// Represents an empty instance of the <see cref="Size"/> structure.
        /// </summary>
        public static Size Empty
        {
            get { return s_Empty; }
        }
        #endregion
    }
}
