﻿using System;
using System.Drawing;
using System.Globalization;

namespace C1.Win.Interop
{
    /// <summary>
    /// Stores a set of four <b>double</b> values that represent the location and size of a rectangle.
    /// </summary>
    public struct Rect
    {
        private static Rect s_Empty = new Rect();
        private double _x;
        private double _y;
        private double _width;
        private double _height;

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Rect"/> structure.
        /// </summary>
        /// <param name="location">The location (coordinates of the top left corner) of the rectangle.</param>
        /// <param name="size">The size of the rectangle.</param>
        public Rect(Point location, Size size)
        {
            _x = location.X;
            _y = location.Y;
            _width = size.Width;
            _height = size.Height;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Rect"/> structure.
        /// </summary>
        /// <param name="x">The X coordinate of the left side of the rectangle.</param>
        /// <param name="y">The Y coordinate of the top side of the rectangle.</param>
        /// <param name="width">The width of the rectangle.</param>
        /// <param name="height">The height of the rectangle.</param>
        public Rect(double x, double y, double width, double height)
        {
            _x = x;
            _y = y;
            _width = width;
            _height = height;
        }
        #endregion

        #region Public
        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        public override bool Equals(object obj)
        {
            if (!(obj is Rect))
                return false;
            return this.Equals((Rect)obj);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        public override int GetHashCode()
        {
            return (int)_x ^ (int)_y ^ (int)_width ^ (int)_height;
        }

        /// <summary>
        /// Deflates the current <see cref="Rect"/> by the specified amounts provided as <see cref="Thickness"/>.
        /// </summary>
        public void Deflate(Thickness th)
        {
            _x += th.Left;
            _y += th.Top;
            _width -= th.Left + th.Right;
            _height -= th.Top + th.Bottom;
        }

        /// <summary>
        /// Converts the current <see cref="Rect"/> structure to a human-readable string representing it.
        /// </summary>
        /// <returns>The string representing the current <see cref="Rect"/>.</returns>
        public override string ToString()
        {
            return string.Format("{{X={0}, Y={1}, Width={2}, Height={3}}}", _x, _y, _width, _height);
        }

        /// <summary>
        /// Tests whether the specified point is contained within the current <see cref="Rect"/>.
        /// </summary>
        /// <param name="pt">The <see cref="Point"/> to test.</param>
        /// <returns><b>true</b> if <paramref name="pt"/> is contained within the current <see cref="Rect"/> structure,
        /// <b>false</b> otherwise.</returns>
        public bool Contains(Point pt)
        {
            return this.Contains(pt.X, pt.Y);
        }

        /// <summary>
        /// Tests whether the specified rectangle is entirely contained within the current <see cref="Rect"/>.
        /// </summary>
        /// <param name="rc">The <see cref="Rect"/> to test.</param>
        /// <returns><b>true</b> if <paramref name="rc"/> is completely contained within the current <see cref="Rect"/> structure,
        /// <b>false</b> otherwise.</returns>
        public bool Contains(Rect rc)
        {
            return X <= rc.X && rc.Right <= this.Right && Y <= rc.Y && rc.Bottom <= this.Bottom;
        }

        /// <summary>
        /// Tests whether a point specified by its coordinates is contained within the current <see cref="Rect"/>.
        /// </summary>
        /// <param name="x">The X coordinate of the point to test.</param>
        /// <param name="y">The Y coordinate of the point to test.</param>
        /// <returns><b>true</b> if the specified point is contained within the current <see cref="Rect"/> structure,
        /// <b>false</b> otherwise.</returns>
        public bool Contains(double x, double y)
        {
            return x >= _x && x < _x + _width && y >= _y && y < _y + _height;
        }

        /// <summary>
        /// Inflates the current <see cref="Rect"/> by the specified amounts on four sides.
        /// </summary>
        /// <param name="left">The amount to add on the left.</param>
        /// <param name="top">The amount to add at the top.</param>
        /// <param name="right">The amount to add on the right.</param>
        /// <param name="bottom">The amount to add on the bottom.</param>
        public void Inflate(double left, double top, double right, double bottom)
        {
            _x -= left;
            _y -= top;
            _width += left + right;
            _height += top + bottom;
        }

        /// <summary>
        /// Inflates the current <see cref="Rect"/> by a specified <see cref="Size"/>.
        /// </summary>
        /// <param name="size">The <see cref="Size"/> to inflate the current rectangle by.</param>
        public void Inflate(Size size)
        {
            Inflate(size.Width, size.Height);
        }

        /// <summary>
        /// Inflates the current <see cref="Rect"/> by specified horizontal and vertical amounts.
        /// </summary>
        /// <param name="x">The amount to add on the left and right of the current <see cref="Rect"/>.</param>
        /// <param name="y">The amount to add at the top and bottom of the current <see cref="Rect"/>.</param>
        public void Inflate(double x, double y)
        {
            _x -= x;
            _y -= y;
            _width += x * 2;
            _height += y * 2;
        }

        /// <summary>
        /// Replaces the current <see cref="Rect"/> structure with the intersection
        /// of itself and another <see cref="Rect"/>.
        /// </summary>
        /// <param name="rect">The <see cref="Rect"/> to intersect the current one with.</param>
        public void Intersect(Rect rect)
        {
            double right = Math.Min(Right, rect.Right);
            _x = Math.Max(_x, rect._x);
            double bottom = Math.Min(Bottom, rect.Bottom);
            _y = Math.Max(_y, rect._y);
            if (right >= _x && bottom >= _y)
            {
                _width = right - _x;
                _height = bottom - _y;
            }
            else
            {
                _x = 0;
                _y = 0;
                _width = 0;
                _height = 0;
            }
        }

        /// <summary>
        /// Tests whether the current <see cref="Rect"/> intersects with another.
        /// </summary>
        /// <param name="rc">The <see cref="Rect"/> to test.</param>
        /// <returns><b>true</b> if the current <see cref="Rect"/> intersects with <paramref name="rc"/>, <b>false</b> otherwise.</returns>
        public bool IntersectsWith(Rect rc)
        {
            return rc.X < Right && X < rc.Right && rc.Y < Bottom && Y < rc.Bottom;
        }

        /// <summary>
        /// Offsets the location of the current <see cref="Rect"/> by specified horizontal and vertical amounts.
        /// </summary>
        /// <param name="x">The horizontal offset to apply.</param>
        /// <param name="y">The vertical offset to apply.</param>
        public void Offset(double x, double y)
        {
            _x += x;
            _y += y;
        }

        /// <summary>
        /// Offsets the location of the current <see cref="Rect"/> by horizontal and vertical amounts specified by a <see cref="Point"/>.
        /// </summary>
        /// <param name="pt">The <see cref="Point"/> specifying the horizontal and vertical offsets to apply.</param>
        public void Offset(Point pt)
        {
            Offset(pt.X, pt.Y);
        }

        /// <summary>
        /// Converts the current <see cref="Rect"/> to a <see cref="RectangleF"/>.
        /// </summary>
        /// <returns>The converted <see cref="RectangleF"/> structure.</returns>
        public RectangleF ToRectangleF()
        {
            return new RectangleF((float)_x, (float)_y, (float)_width, (float)_height);
        }

        /// <summary>
        /// Converts the current <see cref="Rect"/> to a <see cref="Rectangle"/>.
        /// </summary>
        /// <returns>The converted <see cref="Rectangle"/> structure.</returns>
        public Rectangle ToRectangle()
        {
            return new Rectangle((int)Math.Round(_x), (int)Math.Round(_y), (int)Math.Round(_width), (int)Math.Round(_height));
        }
        #endregion

        #region Public static
        /// <summary>
        /// Indicates whether two <see cref="Rect"/> structures are equal.
        /// </summary>
        public static bool operator ==(Rect rect1, Rect rect2)
        {
            return ((((rect1.X == rect2.X) && (rect1.Y == rect2.Y)) && (rect1.Width == rect2.Width)) && (rect1.Height == rect2.Height));
        }

        /// <summary>
        /// Indicates whether two <see cref="Rect"/> structures are not equal.
        /// </summary>
        public static bool operator !=(Rect rect1, Rect rect2)
        {
            return !(rect1 == rect2);
        }

        /// <summary>
        /// Converts a <see cref="Rectangle"/> to a <see cref="Rect"/>.
        /// </summary>
        /// <param name="value">The <see cref="Rectangle"/> to convert.</param>
        /// <returns>The converted <see cref="Rect"/> structure.</returns>
        public static implicit operator Rect(Rectangle value)
        {
            return new Rect(value.X, value.Y, value.Width, value.Height);
        }

        /// <summary>
        /// Converts a <see cref="RectangleF"/> to a <see cref="Rect"/>.
        /// </summary>
        /// <param name="value">The <see cref="RectangleF"/> to convert.</param>
        /// <returns>The converted <see cref="Rect"/> structure.</returns>
        public static implicit operator Rect(RectangleF value)
        {
            return new Rect(value.X, value.Y, value.Width, value.Height);
        }

        /// <summary>
        /// Converts a string to a <see cref="Rect"/> structure.
        /// </summary>
        /// <param name="s">The string to convert.</param>
        /// <param name="result">OUT: the created <see cref="Rect"/> structure.</param>
        /// <param name="throwException">Indicates whether an exception should be thrown if the string cannot be converted.</param>
        /// <returns><b>true</b> if no error occurred, <b>false</b> otherwise (if <paramref name="throwException"/> is <b>false</b>).</returns>
        public static bool Parse(string s, out Rect result, bool throwException)
        {
            result = Rect.Empty;
            string[] elems = s.Split(',');
            if (elems.Length != 4)
            {
                if (throwException)
                    throw new Exception(string.Format(Strings.InvalidString, s, typeof(Rect).Name));
                return false;
            }

            if (double.TryParse(elems[0], NumberStyles.Any, CultureInfo.InvariantCulture, out result._x) &&
                double.TryParse(elems[1], NumberStyles.Any, CultureInfo.InvariantCulture, out result._y) &&
                double.TryParse(elems[2], NumberStyles.Any, CultureInfo.InvariantCulture, out result._width) &&
                double.TryParse(elems[3], NumberStyles.Any, CultureInfo.InvariantCulture, out result._height))
                return true;

            if (throwException)
                throw new Exception(string.Format(Strings.InvalidString, s, typeof(Rect).Name));
            return false;
        }

        /// <summary>
        /// Creates a <see cref="Rect"/> structure based on the coordinates
        /// of its top-left and bottom-right corners.
        /// </summary>
        /// <param name="left">The X coordinate of the upper-left corner of the rectangular region.</param>
        /// <param name="top">The Y coordinate of the upper-left corner of the rectangular region.</param>
        /// <param name="right">The X coordinate of the lower-right corner of the rectangular region.</param>
        /// <param name="bottom">The Y coordinate of the lower-right corner of the rectangular region.</param>
        /// <returns>The newly created <see cref="Rect"/> structure.</returns>
        public static Rect FromLTRB(double left, double top, double right, double bottom)
        {
            return new Rect(
                left,
                top,
                right - left,
                bottom - top);
        }

        /// <summary>
        /// Creates a new <see cref="Rect"/> structure from another <b>RectangleD</b>,
        /// inflated by the specified amounts vertically and horizontally.
        /// </summary>
        /// <param name="rc">The <see cref="Rect"/> a copy of which is created and inflated.</param>
        /// <param name="x">The amount by which to inflate the resulting rectangle horizontally.</param>
        /// <param name="y">The amount by which to inflate the resulting rectangle vertically.</param>
        /// <returns>The newly created <see cref="Rect"/> structure.</returns>
        public static Rect Inflate(Rect rc, double x, double y)
        {
            Rect result = rc;
            result.Inflate(x, y);
            return result;
        }

        /// <summary>
        /// Creates a new <see cref="Rect"/> structure representing the intersection
        /// of two <b>RectangleD</b> structures.
        /// (The intersection may be empty, in which case <see cref="Rect.Empty"/> is returned.)
        /// </summary>
        /// <param name="a">The first <see cref="Rect"/> to intersect.</param>
        /// <param name="b">The second <see cref="Rect"/> to intersect.</param>
        /// <returns>The newly created <see cref="Rect"/> structure.</returns>
        public static Rect Intersect(Rect a, Rect b)
        {
            Rect result = a;
            result.Intersect(b);
            return result;
        }

        /// <summary>
        /// Creates a new <see cref="Rect"/> structure representing the smallest possible
        /// rectangle containing the two specified rectangles.
        /// </summary>
        /// <param name="a">The first <see cref="Rect"/> to combine.</param>
        /// <param name="b">The second <see cref="Rect"/> to combine.</param>
        /// <returns>The newly created <see cref="Rect"/> structure.</returns>
        public static Rect Union(Rect a, Rect b)
        {
            double single1 = Math.Min(a.X, b.X);
            double single2 = Math.Max(a.X + a.Width, b.X + b.Width);
            double single3 = Math.Min(a.Y, b.Y);
            double single4 = Math.Max(a.Y + a.Height, b.Y + b.Height);
            return new Rect(single1, single3, single2 - single1, single4 - single3);
        }
        #endregion

        #region Public properties
        /// <summary>
        /// Gets or sets the X coordinate of the top left corner of the current <see cref="Rect"/> structure.
        /// </summary>
        public double X
        {
            get { return _x; }
            set { _x = value; }
        }

        /// <summary>
        /// Gets or sets the Y coordinate of the top left corner of the current <see cref="Rect"/> structure.
        /// </summary>
        public double Y
        {
            get { return _y; }
            set { _y = value; }
        }

        /// <summary>
        /// Gets or sets the width of the current <see cref="Rect"/> structure.
        /// </summary>
        public double Width
        {
            get { return _width; }
            set { _width = value; }
        }

        /// <summary>
        /// Gets or sets the height of the current <see cref="Rect"/> structure.
        /// </summary>
        public double Height
        {
            get { return _height; }
            set { _height = value; }
        }

        /// <summary>
        /// Gets the X coordinate of the left edge of the current <see cref="Rect"/> structure.
        /// </summary>
        public double Left
        {
            get { return _x; }
        }

        /// <summary>
        /// Gets the Y coordinate of the top edge of the current <see cref="Rect"/> structure.
        /// </summary>
        public double Top
        {
            get { return _y; }
        }

        /// <summary>
        /// Gets the X coordinate of the right edge of the current <see cref="Rect"/> structure.
        /// </summary>
        public double Right
        {
            get { return _x + _width; }
        }

        /// <summary>
        /// Gets the Y coordinate of the bottom edge of the current <see cref="Rect"/> structure.
        /// </summary>
        public double Bottom
        {
            get { return _y + _height; }
        }

        /// <summary>
        /// Gets or sets the coordinates of the top left corner of the current <see cref="Rect"/> structure.
        /// </summary>
        public Point Location
        {
            get { return new Point(_x, _y); }
            set
            {
                _x = value.X;
                _y = value.Y;
            }
        }

        /// <summary>
        /// Gets or sets the size of the current <see cref="Rect"/> structure.
        /// </summary>
        public Size Size
        {
            get { return new Size(_width, _height); }
            set
            {
                _width = value.Width;
                _height = value.Height;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="Width"/> and <see cref="Height"/>
        /// properties of the current <see cref="Rect"/> structure have zero values.
        /// </summary>
        public bool IsEmpty
        {
            get { return _width <= 0 || _height <= 0; }
        }

        /// <summary>
        /// Gets the coordinates of the top left corner of the current <see cref="Rect"/> structure.
        /// </summary>
        public Point TopLeft
        {
            get { return new Point(_x, _y); }
        }

        /// <summary>
        /// Gets the coordinates of the top right corner of the current <see cref="Rect"/> structure.
        /// </summary>
        public Point TopRight
        {
            get { return new Point(_x + _width, _y); }
        }

        /// <summary>
        /// Gets the coordinates of the bottom right corner of the current <see cref="Rect"/> structure.
        /// </summary>
        public Point BottomRight
        {
            get { return new Point(_x + _width, _y + _height); }
        }

        /// <summary>
        /// Gets the coordinates of the bottom left corner of the current <see cref="Rect"/> structure.
        /// </summary>
        public Point BottomLeft
        {
            get { return new Point(_x, _y + _height); }
        }
        #endregion

        #region Public static properties
        /// <summary>
        /// Represents an empty instance of the <see cref="Rect"/> structure.
        /// </summary>
        public static Rect Empty
        {
            get { return s_Empty; }
        }
        #endregion
    }
}
