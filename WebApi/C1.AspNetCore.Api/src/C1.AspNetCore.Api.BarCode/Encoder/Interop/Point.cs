using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;

namespace C1.Win.Interop
{
    /// <summary>
    /// Represents an ordered pair of <b>double</b> X and Y coordinates that defines a point 
    /// in a two-dimensional plane.
    /// </summary>
    public struct Point
    {
        private static Point s_Empty = new Point();
        private double _x;
        private double _y;

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Point"/> structure.
        /// </summary>
        /// <param name="x">The horizontal position of the point.</param>
        /// <param name="y">The vertical position of the point.</param>
        public Point(double x, double y)
        {
            _x = x;
            _y = y;
        }
        #endregion

        #region Public
        /// <summary>
        /// Indicates whether this instance and a specified object are equal.
        /// </summary>
        public override bool Equals(object obj)
        {
            if (!(obj is Point))
                return false;
            Point p = (Point)obj;
            return _x == p._x && _y == p._y;
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        public override int GetHashCode()
        {
            return (int)_x ^ (int)_y;
        }

        /// <summary>
        /// Converts the current <see cref="Point"/> to a human readable string. 
        /// </summary>
        /// <returns>A string that represents the current <see cref="Point"/> structure.</returns>
        public override string ToString()
        {
            return string.Format("{{X={0}, Y={1}}}", _x, _y);
        }

        /// <summary>
        /// Converts the current <see cref="Point"/> structure to a <see cref="System.Drawing.Point"/> by rounding the 
        /// <b>double</b> coordinates to the next higher integer values. 
        /// </summary>
        /// <returns>The <see cref="Point"/> structure this method converts to.</returns>
        public System.Drawing.Point Ceiling()
        {
            return new System.Drawing.Point((int)Math.Ceiling(_x), (int)Math.Ceiling(_y));
        }

        /// <summary>
        /// Converts the current <see cref="Point"/> structure to a <see cref="System.Drawing.Point"/> by truncating
        /// the values of the Point object. 
        /// </summary>
        /// <returns>The <see cref="Point"/> structure this method converts to.</returns>
        public System.Drawing.Point Truncate()
        {
            return new System.Drawing.Point((int)_x, (int)_y);
        }

        /// <summary>
        /// Offsets the current <see cref="Point"/> structure by the specified values.
        /// </summary>
        /// <param name="dx">The horizontal offset.</param>
        /// <param name="dy">The vertical offset.</param>
        public void Offset(double dx, double dy)
        {
            _x += dx;
            _y += dy;
        }

        /// <summary>
        /// Converts the current <see cref="Point"/> structure to a <see cref="PointF"/>.
        /// </summary>
        /// <returns>The <see cref="PointF"/> structure this method converts to.</returns>
        public PointF ToPointF()
        {
            return new PointF((float)_x, (float)_y);
        }
        #endregion

        #region Public static
        /// <summary>
        /// Converts a string to a <see cref="Point"/> structure.
        /// </summary>
        /// <param name="s">The string to convert.</param>
        /// <param name="result">OUT: the created <see cref="Point"/> structure.</param>
        /// <param name="throwException">Indicates whether an exception should be thrown if the string cannot be converted.</param>
        /// <returns><b>true</b> if no error occurred, <b>false</b> otherwise (if <paramref name="throwException"/> is <b>false</b>).</returns>
        public static bool Parse(string s, out Point result, bool throwException)
        {
            result = Point.Empty;
            string[] elems = s.Split(',');
            if (elems.Length != 2)
            {
                if (throwException)
                    throw new Exception(string.Format(Strings.InvalidString, s, typeof(Point).Name));
                return false;
            }

            if (double.TryParse(elems[0], NumberStyles.Any, CultureInfo.InvariantCulture, out result._x) &&
                double.TryParse(elems[0], NumberStyles.Any, CultureInfo.InvariantCulture, out result._y))
                return true;

            if (throwException)
                throw new Exception(string.Format(Strings.InvalidString, s, typeof(Point).Name));
            return false;
        }

        /// <summary>
        /// Adds a <see cref="Size"/> to a <see cref="Point"/>.
        /// </summary>
        /// <param name="pt">The <see cref="Point"/> that is added.</param>
        /// <param name="sz">The <see cref="Size"/> that is added.</param>
        /// <returns>A <see cref="Point"/> representing the result of the addition.</returns>
        public static Point Add(Point pt, Size sz)
        {
            return new Point(pt.X + sz.Width, pt.Y + sz.Height);
        }

        /// <summary>
        /// Subtracts a <see cref="Size"/> from a <see cref="Point"/>.
        /// </summary>
        /// <param name="pt">The <see cref="Point"/> that is subtracted from.</param>
        /// <param name="sz">The <see cref="Size"/> that is subtracted.</param>
        /// <returns>A <see cref="Point"/> representing the result of the subtraction.</returns>
        public static Point Substract(Point pt, Size sz)
        {
            return new Point(pt.X - sz.Width, pt.Y - sz.Height);
        }

        /// <summary>
        /// Converts a <see cref="System.Drawing.Point"/> to a <see cref="Point"/>.
        /// </summary>
        /// <param name="value">The <see cref="Point"/> to convert.</param>
        /// <returns>The converted <see cref="Point"/>.</returns>
        public static implicit operator Point(System.Drawing.Point value)
        {
            return new Point(value.X, value.Y);
        }

        /// <summary>
        /// Converts a <see cref="PointF"/> to a <see cref="Point"/>.
        /// </summary>
        /// <param name="value">The <see cref="PointF"/> to convert.</param>
        /// <returns>The converted <see cref="Point"/>.</returns>
        public static implicit operator Point(PointF value)
        {
            return new Point(value.X, value.Y);
        }

        /// <summary>
        /// Converts a <see cref="Point"/> to a <see cref="Size"/>.
        /// </summary>
        /// <param name="value">The <see cref="Point"/> to convert.</param>
        /// <returns>The converted <see cref="Size"/>.</returns>
        public static explicit operator Size(Point value)
        {
            return new Size(value.X, value.Y);
        }

        /// <summary>
        /// Adds a <see cref="Size"/> to a <see cref="Point"/>.
        /// </summary>
        /// <param name="pt">The <see cref="Point"/> that is added.</param>
        /// <param name="sz">The <see cref="Size"/> that is added.</param>
        /// <returns>A <see cref="Point"/> representing the result of the addition.</returns>
        public static Point operator +(Point pt, Size sz)
        {
            return Point.Add(pt, sz);
        }

        /// <summary>
        /// Subtracts a <see cref="Size"/> from a <see cref="Point"/>.
        /// </summary>
        /// <param name="pt">The <see cref="Point"/> that is subtracted from.</param>
        /// <param name="sz">The <see cref="Size"/> that is subtracted.</param>
        /// <returns>A <see cref="Point"/> representing the result of the subtraction.</returns>
        public static Point operator -(Point pt, Size sz)
        {
            return Point.Substract(pt, sz);
        }
        #endregion

        #region Operators
        /// <summary>
        /// Indicates whether two <see cref="Point"/> structures are equal.
        /// </summary>
        public static bool operator ==(Point x, Point y)
        {
            return x._x == y._x && x._y == y._y;
        }

        /// <summary>
        /// Indicates whether two <see cref="Point"/> structures are not equal.
        /// </summary>
        public static bool operator !=(Point x, Point y)
        {
            return x._x != y._x || x._y != y._y;
        }
        #endregion

        #region Public properties
        /// <summary>
        /// Gets or sets the X coordinate of the current <see cref="Point"/>.
        /// </summary>
        public double X
        {
            get { return _x; }
            set { _x = value; }
        }

        /// <summary>
        /// Gets or sets the Y coordinate of the current <see cref="Point"/>.
        /// </summary>
        public double Y
        {
            get { return _y; }
            set { _y = value; }
        }

        /// <summary>
        /// Gets a value indicating whether the current <see cref="Point"/> is empty.
        /// </summary>
        [Browsable(false)]
        public bool IsEmpty
        {
            get { return _x == 0 && _y == 0; }
        }
        #endregion

        #region Public static properties
        /// <summary>
        /// Represents an empty instance of the <see cref="Point"/> structure.
        /// </summary>
        public static Point Empty
        {
            get { return s_Empty; }
        }
        #endregion
    }
}
