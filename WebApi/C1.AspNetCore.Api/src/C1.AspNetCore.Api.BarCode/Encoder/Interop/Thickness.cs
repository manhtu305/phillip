namespace C1.Win.Interop
{
    /// <summary>
    /// Describes the thickness of a frame around a rectangle. Four double
    /// values describe the <see cref="Thickness.Left"/>, <see cref="Thickness.Top"/>,
    /// <see cref="Thickness.Right"/>, and <see cref="Thickness.Bottom"/> sides
    /// of the rectangle, respectively.
    /// </summary>
    public struct Thickness
    {
        private double _left;
        private double _top;
        private double _right;
        private double _bottom;

        #region Constructors
        /// <summary>
        /// Initializes a <see cref="Thickness"/> structure that has the specified
        /// uniform length on each side.
        /// </summary>
        /// <param name="uniformLength">The uniform length applied to all four sides of the bounding rectangle.</param>
        public Thickness(double uniformLength)
        {
            _left = uniformLength;
            _top = uniformLength;
            _right = uniformLength;
            _bottom = uniformLength;
        }

        /// <summary>
        /// Initializes a Windows.UI.Xaml.Thickness structure that has specific lengths
        /// (supplied as a System.Double) applied to each side of the rectangle.
        /// </summary>
        /// <param name="left">The thickness for the left side of the rectangle.</param>
        /// <param name="top">The thickness for the upper side of the rectangle.</param>
        /// <param name="right">The thickness for the right side of the rectangle.</param>
        /// <param name="bottom">The thickness for the lower side of the rectangle.</param>
        public Thickness(double left, double top, double right, double bottom)
        {
            _left = left;
            _top = top;
            _right = right;
            _bottom = bottom;
        }
        #endregion

        #region Public static
        /// <summary>
        /// Compares two <see cref="Thickness"/> structures for inequality.
        /// </summary>
        /// <param name="t1">The first structure to compare.</param>
        /// <param name="t2">The other structure to compare.</param>
        /// <returns>true if the two instances of Windows.UI.Xaml.Thickness are not equal; otherwise, false.</returns>
        public static bool operator !=(Thickness t1, Thickness t2)
        {
            return !(t1 == t2);
        }

        /// <summary>
        /// Compares the value of two <see cref="Thickness"/> structures for equality.
        /// </summary>
        /// <param name="t1">The first structure to compare.</param>
        /// <param name="t2">The other structure to compare.</param>
        /// <returns>true if the two instances of Windows.UI.Xaml.Thickness are equal; otherwise, false.</returns>
        public static bool operator ==(Thickness t1, Thickness t2)
        {
            return 
                t1._left == t2._left &&
                t1._top == t2._top &&
                t1._right == t2._right &&
                t1._bottom == t2._bottom;
        }
        #endregion

        #region Public properties
        /// <summary>
        /// Gets or sets the width, of the lower side of the bounding rectangle.
        /// </summary>
        public double Bottom
        {
            get { return _bottom; }
            set { _bottom = value; }
        }

        /// <summary>
        /// Gets or sets the width, of the left side of the bounding rectangle.
        /// </summary>
        public double Left
        {
            get { return _left; }
            set { _left = value; }
        }

        /// <summary>
        /// Gets or sets the width, of the right side of the bounding rectangle.
        /// </summary>
        public double Right
        {
            get { return _right; }
            set { _right = value; }
        }

        /// <summary>
        /// Gets or sets the width, of the upper side of the bounding rectangle.
        /// </summary>
        public double Top
        {
            get { return _top; }
            set { _top = value; }
        }
        #endregion

        #region Public 
        /// <summary>
        /// Compares this Windows.UI.Xaml.Thickness structure to another System.Object
        /// for equality.
        /// </summary>
        /// <param name="obj">The object to compare.</param>
        /// <returns>true if the two objects are equal; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Thickness))
                return false;
            return Equals((Thickness)obj);
        }

        /// <summary>
        /// Compares this Windows.UI.Xaml.Thickness structure to another <see cref="Thickness"/>
        /// structure for equality.
        /// </summary>
        /// <param name="thickness">An instance of <see cref="Thickness"/> to compare for equality.</param>
        /// <returns>true if the two instances of Windows.UI.Xaml.Thickness are equal; otherwise, false.</returns>
        public bool Equals(Thickness thickness)
        {
            return this == thickness;
        }

        /// <summary>
        /// Returns the hash code of the structure.
        /// </summary>
        /// <returns>A hash code for this instance of <see cref="Thickness"/>.</returns>
        public override int GetHashCode()
        {
            return (int)_left ^ (int)_top ^ (int)_right ^ (int)_bottom;
        }

        /// <summary>
        /// Returns the string representation of the <see cref="Thickness"/> structure.
        /// </summary>
        /// <returns>A string that represents the <see cref="Thickness"/> value.</returns>
        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}, {3}", _left, _top, _right, _bottom);
        }
        #endregion
    }
}
