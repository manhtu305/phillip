using System.Globalization;
using System.Reflection;
using System.Resources;

namespace C1.Win
{
    [Obfuscation(Exclude = true, ApplyToMembers = true)]
    internal static class Strings
    {
        public static string InvalidString
        {
            get
            {
                return BarCode.Strings.GetCustomString("InvalidString", "String [{0}] does not represent an instance of type [{1}].");
            }
        }
    }
}

namespace C1.Win.BarCode
{
    /// <summary>
    /// Static class containing UI strings used by the designer.
    /// </summary>
    internal static class Strings
    {
        #region Infrastructure
        private static ResourceManager s_ResourceManager;

        public static ResourceManager ResourceManager
        {
            get
            {
                if (object.ReferenceEquals(s_ResourceManager, null))
                {
                    var resourceManager = new ResourceManager(
#if ASPNETCORE
                        "C1.AspNetCore.Api.BarCode.Resources",

#else
                        "C1.Web.Api.BarCode.Encoder.XBarcode.Resources.Resources",

#endif
                        typeof(Strings).Assembly);

                    s_ResourceManager = resourceManager;
                }

                return s_ResourceManager;
            }
            set { s_ResourceManager = value; }
        }

        public static CultureInfo UICulture
        {
#if LOCTEST
            get { return new CultureInfo("ja"); }
#else
#if DEBUG && GRAPECITY
            get { return new CultureInfo("ja"); }
#else
            // 2012/10/26
            // As result of C1 developers discussion now CurrentUICulture used
            // instead of CurrentCulture to get culture for localized strings
#if !LOCALIZE_TO_CURRENT_CULTURE
            get { return CultureInfo.CurrentUICulture; }
#else
            get { return CultureInfo.CurrentCulture; }
#endif
#endif
#endif
        }
        #endregion

        public static string GetCustomString(string code, string defaultValue)
        {
            var customString = ResourceManager.GetString(code, UICulture);
            if (string.IsNullOrEmpty(customString))
            {
                return defaultValue;
            }
            return customString;
        }

        public static class Errors
        {
            public static string BarNarrowMustBePositive
            {
                get { return GetCustomString("BarNarrowMustBePositive", "BarNarrow must be > 0."); }
            }
            public static string BarWideMustBePositive
            {
                get { return GetCustomString("BarWideMustBePositive", "BarWide must be > 0."); }
            }
            public static string BarHeightMustBePositive
            {
                get { return GetCustomString("BarHeightMustBePositive", "BarHeight must be > 0."); }
            }
            public static string InvalidSymbolSize
            {
                get { return GetCustomString("InvalidSymbolSize", "Invalid symbol size (should be between 2 and 10)."); }
            }
            public static string GetError(string code, string defaultValue)
            {
                return GetCustomString(code, defaultValue);
            }
        }
    }
}