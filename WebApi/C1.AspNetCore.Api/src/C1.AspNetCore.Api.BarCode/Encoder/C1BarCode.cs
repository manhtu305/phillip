﻿using C1.BarCode;
using C1.Win.Interop;
using System;
using System.Drawing;

namespace C1.Win.BarCode
{
    /// <summary>
    /// Encodes text value into barcode image
    /// </summary>
    internal class C1BarCode : INotify
    {
        private const int DEFAULT_WIDTH = 150;
        private const int DEFAULT_HEIGHT = 150;
        private const int BAR_HEIGHT = 40;
        private const string TEXT = "";
        private const int NARROW = 1;
        private const int SPACING_PIXEL = 1;

        #region Private Fields
        private string _evalInfoText = "ComponentOne";
        private Font _evalInfoFont = new Font("Calibri", 9);
        private Color _evalInfoColor = Color.Gray;

        private BarCodeCaptionPosition _textPosition = BarCodeCaptionPosition.None;
        private TextAlignment _textAlignment = TextAlignment.Center;
        private CodeType _codeType;
        private BarCodeDirection _barDirection;
        private QuietZone _quietZone;
        private int _moduleSize;
        private int _barWidth;
        private int _barHeight;
        private int _textFixedLength;
        private bool _checkSumEnabled;
        private bool _captionGrouping;

        private BarEngine _engine;
        private BarEngine _barEngineDependent;

        private bool _dirty = true;
        private Image _barcodeImage;
        #endregion

        #region Properties

        #region Text
        /// <summary>
        /// Gets or sets the value that is encoded as a barcode image.
        /// </summary>
        public string Text { get; set; }
        #endregion

        #region CodeType
        /// <summary>
        /// The type of encoding to use when generating the barcode image.
        /// </summary>
        public CodeType CodeType
        {
            get { return _codeType; }
            set
            {
                _codeType = value;
                OnOptionsChanged();
                Refresh();
            }
        }
        #endregion

        #region BarWidth
        /// <summary>
        /// The width of the bars in screen pixels.
        /// </summary>
        public int BarWidth
        {
            get { return _barWidth; }
            set
            {
                if (value > 0 && value != _barWidth)
                {
                    _barWidth = value;
                    Refresh();
                }
            }
        }
        #endregion

        #region BarHeight
        /// <summary>
        /// The height of the bars in screen pixels.
        /// </summary>
        public int BarHeight
        {
            get { return _barHeight; }
            set
            {
                if (_barHeight != value)
                {
                    if (value <= 0)
                    {
                        throw new BarCodeException(Strings.Errors.BarHeightMustBePositive);
                    }
                    _barHeight = value;
                    Refresh();
                }
            }
        }
        #endregion

        #region BackColor
        /// <summary>
        /// Gets or sets the background color for the barcode image.
        /// </summary>
        public Color BackColor { get; set; }
        #endregion

        #region ForeColor
        /// <summary>
        /// Gets or sets the foreground color for the barcode image.
        /// </summary>
        public Color ForeColor { get; set; }
        #endregion

        #region Font
        /// <summary>
        /// Gets or sets the font of the text.
        /// </summary>
        public Font Font { get; set; }
        #endregion

        #region AdditionalNumber
        /// <summary>
        /// Additional number for supplemental barcodes
        /// </summary>
        public string AdditionalNumber { get; set; }
        #endregion

        #region CaptionPosition
        /// <summary>
        /// Barcode caption position relative to the barcode symbol
        /// </summary>
        public BarCodeCaptionPosition CaptionPosition
        {
            get { return _textPosition; }
            set
            {
                _textPosition = value;
                Refresh();
            }
        }
        #endregion

        #region CaptionAlignment
        /// <summary>
        /// Barcode caption alignment relative to the barcode symbol
        /// </summary>
        public TextAlignment CaptionAlignment
        {
            get { return _textAlignment; }
            set
            {
                _textAlignment = value;
                Refresh();
            }
        }
        #endregion

        #region BarDirection

        /// <summary>
        /// Direction of the barcode.
        /// </summary>
        public BarCodeDirection BarDirection
        {
            get { return _barDirection; }
            set
            {
                _barDirection = value;
                Refresh();
            }
        }
        #endregion

        #region QuietZone
        /// <summary>
        /// A quiet zone is an area of blank space on either side of a barcode that tells the scanner where the symbology starts and stops.
        /// </summary>
        public QuietZone QuietZone
        {
            get
            {
                if (_quietZone == null)
                {
                    QuietZone = new QuietZone(0, 0, 0, 0);
                }
                return _quietZone;
            }
            set
            {
                if (_quietZone != value)
                {
                    if (_quietZone != null)
                        _quietZone.SetNotify(null);

                    _quietZone = value ?? new QuietZone(0, 0, 0, 0);
                    _quietZone.SetNotify(this);
                    OnOptionsChanged();
                    Refresh();
                }
            }
        }

        #endregion QuietZone

        #region ModuleSize
        /// <summary>
        /// Width of the narrow bar in screen pixels. The default value is one screen pixel wide (1/96").
        /// </summary>
        public int ModuleSize
        {
            get
            {
                return _moduleSize;
            }
            set
            {
                if (_moduleSize != value)
                {
                    if (value <= 0)
                    {
                        throw new BarCodeException(Strings.Errors.BarNarrowMustBePositive);
                    }
                    _moduleSize = value;
                    Refresh();
                }
            }
        }
        #endregion

        #region TextFixedLength
        /// <summary>
        /// Fixed number of digits of values of barcode.
        /// </summary>
        public int TextFixedLength
        {
            get { return _textFixedLength; }
            set
            {
                if (_textFixedLength != value)
                {
                    _textFixedLength = value;
                    Refresh();
                }
            }
        }
        #endregion

        #region CaptionGrouping
        /// <summary>
        /// Draw separated caption for certain barcodes (EAN 8/13, UPC-A/E).
        /// </summary>
        public bool CaptionGrouping
        {
            get { return _captionGrouping; }
            set
            {
                if (_captionGrouping == value)
                    return;

                _captionGrouping = value;
                Refresh();
            }
        }
        #endregion

        #region CheckSumEnabled
        /// <summary>
        /// Gets or sets a value indicating whether a checksum of the barcode will be computed and included in the barcode when applicable.
        /// </summary>
        public bool CheckSumEnabled
        {
            get { return _checkSumEnabled; }
            set
            {
                if (_checkSumEnabled == value)
                    return;

                _checkSumEnabled = value;
                Refresh();
            }
        }
        #endregion

        #region Image
        /// <summary>
        /// Returns bitmap image of control
        /// </summary>
        public Image Image
        {
            get
            {
                if (_dirty || _barcodeImage == null)
                {
                    _barcodeImage = GetBarCodeImage();
                }
                return _barcodeImage;
            }
        }
        #endregion

        #region CodeTypeOptions

        #region QRCodeOptions
        QRCodeOptions _qrCodeOptions;
        /// <summary>
        /// QRCode options.
        /// </summary>
        public QRCodeOptions QRCodeOptions
        {
            get
            {
                // <<IP>> lazy initialization. Do it in the single place. Whenever you need this property, it will be initialized here.
                if (_qrCodeOptions == null)
                {
                    QRCodeOptions = new QRCodeOptions();
                }
                return _qrCodeOptions;
            }
            set
            {
                if (_qrCodeOptions != value)
                {
                    if (_qrCodeOptions != null)
                        _qrCodeOptions.SetNotify(null);
                    _qrCodeOptions = value ?? new QRCodeOptions();
                    _qrCodeOptions.SetNotify(this);
                    _qrCodeOptions.UpdateBarEngine(_engine);
                    Refresh();
                }
            }
        }
        #endregion

        #region PDF417Options
        PDF417Options _pdf417Options;
        /// <summary>
        /// PDF417 options.
        /// </summary>
        public PDF417Options PDF417Options
        {
            get
            {
                // <<IP>> lazy initialization. Do it in the single place
                if (_pdf417Options == null)
                {
                    PDF417Options = new PDF417Options();
                }
                return _pdf417Options;
            }
            set
            {
                if (_pdf417Options != value)
                {
                    if (_pdf417Options != null)
                        _pdf417Options.SetNotify(null);
                    _pdf417Options = value ?? new PDF417Options();
                    _pdf417Options.SetNotify(this);
                    _pdf417Options.UpdateBarEngine(_engine);
                    Refresh();
                }
            }
        }
        #endregion

        #region Code49Options
        Code49Options _code49Options;
        /// <summary>
        /// Code49 options.
        /// </summary>
        public Code49Options Code49Options
        {
            get
            {
                // <<IP>> lazy initialization. Do it in the single place
                if (_code49Options == null)
                {
                    Code49Options = new Code49Options();
                }
                return _code49Options;
            }
            set
            {
                if (_code49Options != value)
                {
                    if (_code49Options != null)
                        _code49Options.SetNotify(null);
                    _code49Options = value ?? new Code49Options();
                    _code49Options.SetNotify(this);
                    _code49Options.UpdateBarEngine(_engine);
                    Refresh();
                }
            }
        }
        #endregion

        #region RssExpandedStackedOptions
        RssExpandedStackedOptions _rssExpandedStackedOptions;
        /// <summary>
        /// RSSExpandedStacked options.
        /// </summary>
        public RssExpandedStackedOptions RssExpandedStackedOptions
        {
            get
            {
                if (_rssExpandedStackedOptions == null)
                {
                    RssExpandedStackedOptions = new RssExpandedStackedOptions();
                }
                return _rssExpandedStackedOptions;
            }
            set
            {
                if (_rssExpandedStackedOptions != value)
                {
                    if (_rssExpandedStackedOptions != null)
                        _rssExpandedStackedOptions.SetNotify(null);
                    _rssExpandedStackedOptions = value ?? new RssExpandedStackedOptions();
                    _rssExpandedStackedOptions.SetNotify(this);
                    _rssExpandedStackedOptions.UpdateBarEngine(_engine);
                    Refresh();
                }
            }
        }
        #endregion

        #region MicroPDF417Options
        MicroPDF417Options _microPDF417Options;
        /// <summary>
        /// MicroPDF417 options.
        /// </summary>
        public MicroPDF417Options MicroPDF417Options
        {
            get
            {
                if (_microPDF417Options == null)
                {
                    MicroPDF417Options = new MicroPDF417Options();
                }
                return _microPDF417Options;
            }
            set
            {
                if (_microPDF417Options != value)
                {
                    if (_microPDF417Options != null)
                        _microPDF417Options.SetNotify(null);
                    _microPDF417Options = value ?? new MicroPDF417Options();
                    _microPDF417Options.SetNotify(this);
                    _microPDF417Options.UpdateBarEngine(_engine);
                    Refresh();
                }
            }
        }
        #endregion

        #region Code25intlvOptions
        Code25intlvOptions _code25intlvOptions;
        /// <summary>
        /// Code25Intlv options.
        /// </summary>
        public Code25intlvOptions Code25intlvOptions
        {
            get
            {
                if (_code25intlvOptions == null)
                {
                    Code25intlvOptions = new Code25intlvOptions();
                }
                return _code25intlvOptions;
            }
            set
            {
                if (_code25intlvOptions != value)
                {
                    if (_code25intlvOptions != null)
                        _code25intlvOptions.SetNotify(null);
                    _code25intlvOptions = value ?? new Code25intlvOptions();
                    _code25intlvOptions.SetNotify(this);
                    _code25intlvOptions.UpdateBarEngine(_engine);
                    Refresh();
                }
            }
        }
        #endregion

        #region GS1CompositeOptions
        GS1CompositeOptions _gs1CompositeOptions;
        /// <summary>
        /// GS1Composite options.
        /// </summary>
        public GS1CompositeOptions GS1CompositeOptions
        {
            get
            {
                if (_gs1CompositeOptions == null)
                {
                    GS1CompositeOptions = new GS1CompositeOptions();
                }
                return _gs1CompositeOptions;
            }
            set
            {
                if (_gs1CompositeOptions != value)
                {
                    if (_gs1CompositeOptions != null)
                        _gs1CompositeOptions.SetNotify(null);
                    _gs1CompositeOptions = value ?? new GS1CompositeOptions();
                    _gs1CompositeOptions.SetNotify(this);
                    _gs1CompositeOptions.UpdateBarEngine(_engine);
                    Refresh();
                }
            }
        }
        #endregion

        #region Ean128Fnc1Options
        Ean128Fnc1Options _ean128Fnc1Options;
        /// <summary>
        /// Update EAN128FNC1  options.
        /// </summary>
        public Ean128Fnc1Options Ean128Fnc1Options
        {
            get
            {
                if (_ean128Fnc1Options == null)
                {
                    Ean128Fnc1Options = new Ean128Fnc1Options();
                }
                return _ean128Fnc1Options;
            }
            set
            {
                if (_ean128Fnc1Options != value)
                {
                    if (_ean128Fnc1Options != null)
                        _ean128Fnc1Options.SetNotify(null);
                    _ean128Fnc1Options = value ?? new Ean128Fnc1Options();
                    _ean128Fnc1Options.SetNotify(this);
                    _ean128Fnc1Options.UpdateBarEngine(_engine);
                    Refresh();
                }
            }
        }
        #endregion

        #region DataMatrixOptions
        DataMatrixOptions _dataMatrixOptions;
        /// <summary>
        /// DataMatrix options.
        /// </summary>
        public DataMatrixOptions DataMatrixOptions
        {
            get
            {
                if (_dataMatrixOptions == null)
                {
                    DataMatrixOptions = new DataMatrixOptions();
                }
                return _dataMatrixOptions;
            }
            set
            {
                if (_dataMatrixOptions != value)
                {
                    if (_dataMatrixOptions != null)
                        _dataMatrixOptions.SetNotify(null);
                    _dataMatrixOptions = value ?? new DataMatrixOptions();
                    _dataMatrixOptions.SetNotify(this);
                    _dataMatrixOptions.UpdateBarEngine(_engine);
                    Refresh();
                }
            }
        }
        #endregion

        #endregion

        internal bool NeedRenderEvalInfo { get; set; }
        internal string EvalInfoText
        {
            get
            {
                return _evalInfoText;
            }
            set
            {
                _evalInfoText = value;
            }
        }
        internal Font EvalInfoFont
        {
            get
            {
                return _evalInfoFont;
            }
            set
            {
                _evalInfoFont = value;
            }
        }
        internal Color EvalInfoColor
        {
            get
            {
                return _evalInfoColor;
            }
            set
            {
                _evalInfoColor = value;
            }
        }

        #endregion

        private Image GetBarCodeImage()
        {
            Rect bounds;
            var evalInfoSize = new Interop.Size();

            using (var g = Graphics.FromImage(new Bitmap(1, 1)))
            {
                bounds = CalculateOutBounds(g);

                if (NeedRenderEvalInfo)
                {
                    evalInfoSize = MeasureString(g, EvalInfoFont, EvalInfoText);
                    if (CaptionPosition == BarCodeCaptionPosition.Below)
                    {
                        bounds.Offset(0, evalInfoSize.Height);
                    }
                }
            }

            var image = new Bitmap((int)(bounds.Width + QuietZone.Left + QuietZone.Right), (int)(bounds.Height + QuietZone.Top + QuietZone.Bottom + evalInfoSize.Height));
            using (var g = Graphics.FromImage(image))
            {
                try
                {
                    g.Clear(BackColor);
                    BarEngine barEngineMain = GetBarEngine();
                    if (barEngineMain == null || string.IsNullOrEmpty(Text))
                        return image;

                    if (IsCompositeBarcode())
                    {
                        BarEngine barEngineDependent = GetBarEngineDependent();
                        BarEngine[] engines = new BarEngine[] { barEngineDependent, barEngineMain };
                        string[] values = new string[] { GS1CompositeOptions.Value, Text };

                        //get IDrawingDevice to measure height of caption
                        var drawingDevice = CreateDrawingDevice(g, ref bounds);
                        double captionHeight = drawingDevice.MeasureString(Text).Height;
                        // caculate bounds of main barcode and dependent barcode
                        Rect[] boundsEngines;
                        double fixedModuleSize;

                        BarEngine.CalculateCompositeBarcodeSize(drawingDevice, bounds, CaptionPosition, (float)captionHeight, BarCodeDirection.LeftToRight, engines, values, false, out boundsEngines, out fixedModuleSize);
                        // render each barcode with the fixed module size, because:
                        // 1) dependent CCA/CCB cannot render with auto module size
                        // 2) make sure two barcodes use the same module size
                        barEngineDependent.FixedModuleSize = fixedModuleSize;
                        barEngineMain.FixedModuleSize = fixedModuleSize;

                        // attach caption to corresponding bar engine
                        switch (CaptionPosition)
                        {
                            case BarCodeCaptionPosition.Above:
                                barEngineDependent.CaptionPos = BarCodeCaptionPosition.Above;
                                barEngineDependent.TextAlign = barEngineMain.TextAlign;
                                // the caption of the composite barcode comes from the main bar engine.
                                barEngineDependent.CompositeCaptionLabel = barEngineMain.GetCaption(Text);
                                barEngineMain.CaptionPos = BarCodeCaptionPosition.None;
                                break;
                            case BarCodeCaptionPosition.Below:
                                barEngineDependent.CaptionPos = BarCodeCaptionPosition.None;
                                barEngineMain.CaptionPos = BarCodeCaptionPosition.Below;
                                break;
                            default:
                                break;
                        }

                        // render each barcode
                        Rect boundsMain = boundsEngines[1];
                        Rect boundsDependent = boundsEngines[0];
                        RenderBarEngine(barEngineMain, boundsMain, Text, true, g);
                        RenderBarEngine(barEngineDependent, boundsDependent, GS1CompositeOptions.Value, true, g);
                    }
                    else
                    {
                        RenderBarEngine(barEngineMain, bounds, Text, false, g);
                    }

                    if (NeedRenderEvalInfo)
                    {
                        RenderEvalInfo(g, image, evalInfoSize);
                    }

                    return RotateBarcode(image);
                }
                catch (BarCodeException ex)
                {
                    RenderException(g, ex.Message, bounds);
                }

                return image;
            }
        }

        private void RenderEvalInfo(Graphics g, Image image, Interop.Size evalInfoSize)
        {
            using (var brush = new SolidBrush(EvalInfoColor))
            {
                g.DrawString(EvalInfoText, EvalInfoFont, brush, 0, CaptionPosition == BarCodeCaptionPosition.Below ? 0 : (float)(image.Height - evalInfoSize.Height));
            }
        }

        private Image RotateBarcode(Bitmap image)
        {
            if (BarDirection == BarCodeDirection.LeftToRight) {
                return image;
            }

            Bitmap tempImage;
            if (IsVertical)
            {
                tempImage = new Bitmap(image.Height, image.Width);
            }
            else
            {
                tempImage = new Bitmap(image.Width, image.Height);
            }

            using (var g = Graphics.FromImage(tempImage))
            {
                switch (BarDirection) {
                    case BarCodeDirection.RightToLeft:
                        g.RotateTransform(180);
                        g.TranslateTransform(-tempImage.Width, -tempImage.Height);
                        break;
                    case BarCodeDirection.TopToBottom:
                        g.RotateTransform(90);
                        g.TranslateTransform(0, -tempImage.Width);
                        break;
                    case BarCodeDirection.BottomToTop:
                        g.TranslateTransform(0, tempImage.Height);
                        g.RotateTransform(-90);
                        break;
                }
                g.DrawImage(image, 0, 0);
            }
            image.Dispose();
            return tempImage;
        }

        internal Bitmap RenderExceptionImage(string message)
        {
            var img = new Bitmap(BarWidth > 0 ? BarWidth : DEFAULT_WIDTH, BarHeight > 0 ? BarHeight : BAR_HEIGHT);
            using (var g = Graphics.FromImage(img))
            {
                RenderException(g, message, new Rect(0, 0, img.Width, img.Height));
            }
            return img;
        }

        private void RenderException(Graphics g, string message, Rect bounds)
        {
            g.Clear(Color.White);
            using (var foreBrush = new SolidBrush(Color.Red))
            {
                g.DrawString(message, new Font(FontFamily.GenericSansSerif, 8), foreBrush, bounds.ToRectangleF());
            }
        }

        private bool IsCompositeBarcode()
        {
            if (GS1CompositeOptions == null
                || GS1CompositeOptions.Type == GS1CompositeType.None
                || string.IsNullOrEmpty(GS1CompositeOptions.Value))
                return false;

            switch (CodeType)
            {
                case CodeType.RSSLimited:
                case CodeType.RSS14Stacked:
                    return true;
                default:
                    return false;
            }
        }

        private IDrawingDevice CreateDrawingDevice(Graphics graphics, ref Rect bounds)
        {
            var dc = new BarCodeDrawingDevice(graphics, BarCodeDirection.LeftToRight, bounds, BackColor, ForeColor, Font, CaptionAlignment);
            return dc;
        }

        private void RenderBarEngine(BarEngine barEngine, Rect bounds, string barcodeData, bool isComposite, Graphics graphics)
        {
            Rect mainBarsBounds;
            Rect supplementBarsBounds;
            var containsSupplement = AllowsSupplementBarcode(CodeType) && !string.IsNullOrEmpty(AdditionalNumber);
            if (containsSupplement)
            {
                SplitBarcodesBounds(Text, AdditionalNumber, bounds, out mainBarsBounds, out supplementBarsBounds);
                barEngine.CaptionPos = CaptionPosition;
                barEngine.CodeType = AdditionalNumber.Length == 2 ? BarStyle.cbsSupplement2 : BarStyle.cbsSupplement5;
                var sdc = CreateDrawingDevice(graphics, ref supplementBarsBounds);
                // Draw supplement bars.
                barEngine.DrawEx(sdc, supplementBarsBounds, AdditionalNumber);
                barEngine.CaptionPos = CaptionPosition;
                barEngine.CodeType = (BarStyle)CodeType;
            }
            else
            {
                mainBarsBounds = bounds;
            }
            //mainBarsBounds = isComposite ? mainBarsBounds : Rotate(mainBarsBounds);
            var dc = CreateDrawingDevice(graphics, ref mainBarsBounds);
            // Draw main bars
            barEngine.DrawEx(dc, mainBarsBounds, barcodeData);
        }

        /// <summary>
        /// Returns true if a <see cref="CodeType"/> can have a supplement barcode, otherwise returns false.
        /// </summary>
        private bool AllowsSupplementBarcode(CodeType symbology)
        {
            switch (symbology)
            {
                case CodeType.EAN_13:
                case CodeType.EAN_8:
                case CodeType.UPC_A:
                case CodeType.UPC_E0:
                case CodeType.UPC_E1:
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Determines the optimal size for 2 barcodes (main and supplement) in a report item 
        /// content area.
        /// </summary>
        private void SplitBarcodesBounds(
            string barcodeData,
            string supplementBarcodeData,
            Rect barcodeBounds,
            out Rect mainBounds,
            out Rect supplementBounds)
        {
            mainBounds = barcodeBounds;
            supplementBounds = barcodeBounds;
            var totalBarsWidth = barcodeBounds.Width - SPACING_PIXEL;
            if (totalBarsWidth < 0)
            {
                mainBounds = new Rect(0, 0, 0, 0);
                supplementBounds = new Rect(0, 0, 0, 0);
                return;
            }

            var mainPercent = (float)barcodeData.Length / (barcodeData.Length + supplementBarcodeData.Length);
            var mainBarsWidth = totalBarsWidth * mainPercent;
            var supplementBarsWidth = totalBarsWidth - mainBarsWidth;

            mainBounds.Width = mainBarsWidth;
            supplementBounds.Width = supplementBarsWidth;
            supplementBounds.X += mainBounds.Right + SPACING_PIXEL;
        }

        private BarEngine GetBarEngine()
        {
            _engine = _engine ?? new BarEngine();

            _engine.IsAutoModuleSize = false;
            _engine.FixedModuleSize = BarEngine.DefaultFixedModuleSize;
            _engine.QuietZone = _quietZone;
            _engine.FixLength = _textFixedLength;
            _engine.CodeType = (BarStyle)_codeType;
            _engine.CaptionPos = _textPosition;
            _engine.TextAlign = _textAlignment;

            //switch (_textAlignment)
            //{
            //    case HorizontalAlignment.Left: _engine.TextAlign = C1.BarCode.TextAlignment.Left; break;
            //    case HorizontalAlignment.Right: _engine.TextAlign = C1.BarCode.TextAlignment.Right; break;
            //    case HorizontalAlignment.Center: _engine.TextAlign = C1.BarCode.TextAlignment.Center; break;
            //};
            _engine.FixLength = _textFixedLength;
            _engine.CheckSumEnabled = _checkSumEnabled;
            _engine.CaptionGrouping = _captionGrouping;
            _engine.BarHeight = _barHeight;

            bool isComposite = IsCompositeBarcode();
            _engine.GS1CompositeType = isComposite ? GS1CompositeType.CCA : GS1CompositeType.None;
            _engine.IsAutoModuleSize = !isComposite;
            _engine.BarWidth = ModuleSize;
            _engine.NWRatio = GetNWRatio(_codeType);

            return _engine;
        }

        private float GetNWRatio(CodeType codeType)
        {
            switch (codeType)
            {
                case C1.BarCode.CodeType.Matrix_2_of_5: return 2.5f;
                case C1.BarCode.CodeType.Ansi39:
                case C1.BarCode.CodeType.Ansi39x: return 3;
                default: return 2;
            }
        }

        /// <summary>
        /// Gets the engine of dependent barcode for the composite barcode.
        /// </summary>
        /// <returns></returns>
        private BarEngine GetBarEngineDependent()
        {
            _barEngineDependent = _barEngineDependent ?? new BarEngine();
            switch (GS1CompositeOptions.Type)
            {
                case GS1CompositeType.CCA:
                    _barEngineDependent.CodeType = BarStyle.cbsCCA;
                    break;
                case GS1CompositeType.CCB:
                    _barEngineDependent.CodeType = BarStyle.cbsCCB;
                    break;
            }
            _barEngineDependent.GS1CompositeColumn = GS1CompositeOptions.GetCompositeColumn(CodeType);
            _barEngineDependent.IsAutoModuleSize = false;
            _barEngineDependent.FixedModuleSize = BarEngine.DefaultFixedModuleSize;

            return _barEngineDependent;
        }

        private void Refresh()
        {
            _dirty = true;
        }

        #region ** Constructor

        /// <summary>
        /// Initializes a new instance of the <see cref="C1BarCode"/> class.
        /// </summary>
        public C1BarCode()
        {
            _barHeight = BAR_HEIGHT;
            _barDirection = BarCodeDirection.LeftToRight;
            _codeType = CodeType.Code39;
            _textPosition = BarCodeCaptionPosition.None;
            _textAlignment = TextAlignment.Center;
            _moduleSize = NARROW;
            _checkSumEnabled = true;
            _captionGrouping = true;
            _engine = GetBarEngine();
            Text = TEXT;
            BackColor = Color.White;
            ForeColor = Color.Black;
            Font = new Font(FontFamily.GenericSansSerif, 12);
        }
        #endregion

        private bool IsVertical
        {
            get
            {
                return BarDirection == BarCodeDirection.BottomToTop
                       || BarDirection == BarCodeDirection.TopToBottom;
            }
        }

        private Rect CalculateOutBounds(Graphics g)
        {
            var captionSize = new Interop.Size();
            if (CaptionPosition != BarCodeCaptionPosition.None)
            {
                captionSize = MeasureString(g, Font, Text);
            }
            var adjustBounds = GetAdjustBounds(g, new Rect(0, 0, DEFAULT_WIDTH, BAR_HEIGHT));
            var tempWidth = BarWidth > 0 ? BarWidth : Math.Max(adjustBounds.Width, captionSize.Width);
            var tempHeight = CodeType == C1.BarCode.CodeType.QRCode ? tempWidth : BarHeight > 0 ? BarHeight : adjustBounds.Height > 0 ? adjustBounds.Height : BAR_HEIGHT;

            return new Rect(QuietZone.Left, QuietZone.Top, tempWidth, tempHeight + captionSize.Height);
        }

        private Rect GetAdjustBounds(Graphics g, Rect originalBounds)
        {
            var _defaultSizeValue = new C1.Win.Interop.Size(DEFAULT_HEIGHT, DEFAULT_HEIGHT);
            C1.Win.Interop.Size newSize = new C1.Win.Interop.Size();
            double barWidth = 0;
            double barHeight = 0;
            if (!IsCompositeBarcode())
            {
                BarEngine barEngine = GetBarEngine();
                BarCodeDrawingDevice dd = new BarCodeDrawingDevice(g, Font, CaptionAlignment);
                var containsSupplement = AllowsSupplementBarcode(CodeType) && !string.IsNullOrEmpty(AdditionalNumber);
                if (containsSupplement)
                {
                    barEngine.CaptionPos = CaptionPosition;
                    barEngine.CodeType = AdditionalNumber.Length == 2 ? BarStyle.cbsSupplement2 : BarStyle.cbsSupplement5;
                    newSize = barEngine.CalculateSize(dd, AdditionalNumber, _defaultSizeValue);
                    barEngine.CaptionPos = CaptionPosition;
                    barEngine.CodeType = (BarStyle)CodeType;
                }
                var mainBarSize = CodeType == CodeType.None ? new C1.Win.Interop.Size()
                    : barEngine.CalculateSize(dd, Text, _defaultSizeValue);
                var spacingPixels = 1;
                newSize = new C1.Win.Interop.Size(mainBarSize.Width + newSize.Width + spacingPixels, mainBarSize.Height + newSize.Height);
            }
            else
            {
                BarEngine barEngineDependent = GetBarEngineDependent();
                BarEngine barEngineMain = GetBarEngine();
                BarEngine[] engines = new BarEngine[] { barEngineDependent, barEngineMain };
                string[] values = new string[] { GS1CompositeOptions.Value, Text };
                Rect[] boundsEngines;
                double fixedModuleSize;
                BarCodeDrawingDevice dd = new BarCodeDrawingDevice(g, Font, CaptionAlignment);
                newSize = BarEngine.CalculateCompositeBarcodeSize(dd, new Rect(0, 0, _defaultSizeValue.Width, _defaultSizeValue.Height), BarCodeCaptionPosition.None, 0, BarCodeDirection.LeftToRight, engines, values, true, out boundsEngines, out fixedModuleSize);
            }

            if (!double.IsPositiveInfinity(newSize.Width) &&
                !double.IsNegativeInfinity(newSize.Width))
            {
                barWidth = newSize.Width;
            }
            else
            {
                barWidth = originalBounds.Width;
            }

            if (!double.IsPositiveInfinity(newSize.Height) &&
                !double.IsNegativeInfinity(newSize.Height))
            {
                barHeight = newSize.Height;
            }
            else
            {
                barHeight = originalBounds.Height;
            }

            return new Rect(originalBounds.X, originalBounds.Y, barWidth, barHeight);
        }

        private Interop.Size MeasureString(Graphics g, Font font, string text)
        {
            var captionSize = new Interop.Size(0, 0);
            IDrawingDevice device = new BarCodeDrawingDevice(g, font, CaptionAlignment);
            captionSize = device.MeasureString(text);
            return captionSize;
        }

        /// <summary>
        /// Update options of barcode.
        /// </summary>
        private void OnOptionsChanged()
        {
            switch (CodeType)
            {
                case CodeType.QRCode:
                    QRCodeOptions.UpdateBarEngine(_engine);
                    break;
                case CodeType.Pdf417:
                    PDF417Options.UpdateBarEngine(_engine);
                    break;
                case CodeType.Code49:
                    Code49Options.UpdateBarEngine(_engine);
                    break;
                case CodeType.RSSExpandedStacked:
                    RssExpandedStackedOptions.UpdateBarEngine(_engine);
                    break;
                case CodeType.MicroPDF417:
                    MicroPDF417Options.UpdateBarEngine(_engine);
                    break;
                case CodeType.Code25intlv:
                    Code25intlvOptions.UpdateBarEngine(_engine);
                    break;
                case CodeType.RSSLimited:
                case CodeType.RSS14Stacked:
                    GS1CompositeOptions.UpdateBarEngine(_engine);
                    break;
                case CodeType.DataMatrix:
                    DataMatrixOptions.UpdateBarEngine(_engine);
                    break;
                case CodeType.EAN128FNC1:
                    Ean128Fnc1Options.UpdateBarEngine(_engine);
                    break;
            }
        }

        #region Members of INotify interface
        void INotify.ChangeValue(object sender)
        {
            OnOptionsChanged();
            Refresh();
        }
        #endregion
    }
}
