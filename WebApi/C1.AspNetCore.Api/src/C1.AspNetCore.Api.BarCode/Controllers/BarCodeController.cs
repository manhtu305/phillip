﻿#if !ASPNETCORE && !NETCORE
using System.Web.Http;
using Controller = System.Web.Http.ApiController;
using FromQuery = System.Web.Http.FromUriAttribute;
using IActionResult = System.Web.Http.IHttpActionResult;
#else
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
#endif

#if NETCORE
using System.Runtime.CompilerServices;
using GrapeCity.Documents.Imaging;
#endif

namespace C1.Web.Api.BarCode
{
  /// <summary>
  /// Controller for BarCode Web API.
  /// </summary>
  public class BarCodeController : Controller
  {
    /// <summary>
    /// A default constructor for BarCode Web API.
    /// </summary>
#if NETCORE
    [MethodImpl(MethodImplOptions.NoOptimization)]
    public BarCodeController() : base()
    {
      GcBitmap.SetLicenseKey("GrapeCity-Internal-License,324419947258618#A0IIZOiQWSisnOiQkIsISP3cnRI5EWU5mb7lnYW9GNvFUbvhjbvI4Z8IWWvh4ROFkeyEEewsGO0llWmpHcFFDUJRDdmRWdFdzbBVTTqF5S9gnTK5WRh3ESLdzQmVTVqlzRxBHSP9UWiojITJCL4MjNzUDNzQzN0IicfJye#4Xfd5nINJUT5IiOiMkIsIiM6BCdl9kLgcmbpdWYtlEIy3mZgQnbl5Wdj3GRgM4RiojIOJyebpjIkJHUiwiIxQzNxIDMgIjM4ATOxAjMiojI4J7QiwiIj9WagkHdpNUZwFmcHJiOiEmTDJCLigTM6gTNycDN9kTMJQ3M");      
    }
    /// <summary>
    /// Gets the barcode image according to the specified request.
    /// </summary>
    /// <param name="setting">The json text contains setting of barcode</param>
    /// <returns>The barcode image</returns>

    [HttpGet("api/barcode/{setting}")]
    public virtual IActionResult GetByJson(string setting)
    {
      IActionResult result = null;
      try
      {
        BarCodeRequest barCodeRequest = JsonConvert.DeserializeObject<BarCodeRequest>(setting);
        var barcodeGenerator = new BarCodeGenerator(barCodeRequest);
        result = barcodeGenerator.GetResult(this);
      }
      catch (System.Exception ex)
      {
        result = BadRequest(ex.Message);
      }
      return result;
    }
#endif
  /// <summary>
  /// Gets the barcode image according to the specified request.
  /// </summary>
  /// <param name="re">The request parameters</param>
  /// <returns>The barcode image</returns>
  [HttpGet]
    [Route("api/barcode")]
    public virtual IActionResult Get([FromQuery]BarCodeRequest re)
    {
      var barcodeGenerator = new BarCodeGenerator(re);
      return barcodeGenerator.GetResult(this);
    }

  }
}
