@echo off
setlocal enabledelayedexpansion
cd "%~dp0"
@echo "%~dp0"
if not exist %1GrapeCity\zip.exe (
xcopy zip.exe %1GrapeCity\
 @echo zip.exe is copied
 )
if not exist %1GrapeCity\unzip.exe (
xcopy unzip.exe %1GrapeCity\
@echo unzip.exe is copied
)

if not "%2"=="GrapeCity" goto :EOF
@echo "%1"

set curver=1.0.20202.44444

for /r "%1" %%i in (GrapeCity\*%curver%.nupkg) do (
  call :BuildPack %%i %1
)

if exist %1GrapeCity\zip.exe del /F /Q %1GrapeCity\zip.exe
if exist %1GrapeCity\unzip.exe del /F /Q %1GrapeCity\unzip.exe
if exist %1GrapeCity rd %1GrapeCity

goto :EOF

:BuildPack
set pack=%~n1
set pack=!pack:ja=!
if not "%pack%"=="%~n1" goto :EOF

@echo getting package name ...
set pack=!pack:.nupkg=!
set pack=!pack:.%curver%=!
@echo package name: %pack%

set oldId=%pack%
set newId=%pack%.ja

@echo initializing ...
set newName=%~n1.nupkg
set newName=!newName:%oldId%=%newId%!

set temp=%~n1
set temp=!temp:%oldId%=%newId%!
set temp=%~dp1%temp%

if exist "%temp%.nupkg" del /f /q "%temp%.nupkg"

if exist "%temp%" rd /s /q "%temp%"
md "%temp%"

if exist "%1.zip" del /f /q "%1.zip"
copy "%1" "%1.zip" >nul

call :createScript

@echo unzip "%~nx1" ...
call %2GrapeCity\unzip.exe "%1.zip" -d "%temp%"

@echo replace to ja ...
"%~dp0replace.vbs" "%temp%\%oldId%.nuspec" %oldId% %newId%
"%~dp0replace.vbs" "%temp%\%oldId%.nuspec" "<language>en-US</language>" "<language>ja-JP</language>"
"%~dp0replace.vbs" "%temp%\%oldId%.nuspec" "<authors>GrapeCity, Inc.</authors>" "<authors>GrapeCity.JP</authors>"
"%~dp0replace.vbs" "%temp%\%oldId%.nuspec" "<owners>GrapeCity, Inc.</owners>" "<owners>GrapeCity.JP</owners>"
"%~dp0replace.vbs" "%temp%\%oldId%.nuspec" "<copyright>(c) GrapeCity, Inc.. All rights reserved.</copyright>" "<copyright>(c) GrapeCity inc. All rights reserved.</copyright>"
"%~dp0replace.vbs" "%temp%\%oldId%.nuspec" "https://www.grapecity.com/en/componentone" "http://www.grapecity.co.jp/developer"
"%~dp0replace.vbs" "%temp%\%oldId%.nuspec" "https://gccontent.blob.core.windows.net/gccontent/logos/c1-hex-logo.png" "https://download.grapecity.com/logo/c1logo_nuget.png"
"%~dp0replace.vbs" "%temp%\%oldId%.nuspec" "<requireLicenseAcceptance>false</requireLicenseAcceptance>" "<requireLicenseAcceptance>false</requireLicenseAcceptance>%%0d%%0a    <licenseUrl>http://www.grapecity.co.jp/developer/support/license</licenseUrl>"
if not "%oldId%"=="C1.AspNetCore.Api" "%~dp0replace.vbs" "%temp%\%oldId%.nuspec" %%22C1.AspNetCore.Api%%22 %%22C1.AspNetCore.Api.ja%%22
if not "%oldId%"=="C1.AspNetCore.Api" "%~dp0replace.vbs" "%temp%\%oldId%.nuspec" %%22C1.AspNetCore.Api.Document%%22 %%22C1.AspNetCore.Api.Document.ja%%22
if not "%oldId%"=="C1.AspNetCore.Api" "%~dp0replace.vbs" "%temp%\%oldId%.nuspec" %%22C1.DataEngine%%22 %%22C1.DataEngine.Ja%%22
if not "%oldId%"=="C1.AspNetCore.Api" "%~dp0replace.vbs" "%temp%\%oldId%.nuspec" %%22C1.Win.C1Document%%22 %%22C1.Win.C1Document.Ja%%22
if not "%oldId%"=="C1.AspNetCore.Api" "%~dp0replace.vbs" "%temp%\%oldId%.nuspec" %%22C1.C1Excel%%22 %%22C1.C1Excel.Ja%%22
if not "%oldId%"=="C1.AspNetCore.Api" "%~dp0replace.vbs" "%temp%\%oldId%.nuspec" %%22C1.Win.FlexReport%%22 %%22C1.Win.FlexReport.Ja%%22
"%~dp0replace.vbs" "%temp%\_rels\.rels" %oldId% %newId%
ren "%temp%\%oldId%.nuspec" "%newId%.nuspec"

@echo create %newName% ...
cd %temp%
@echo %cd%
call %2GrapeCity\zip.exe -r "%temp%.zip" *
cd ..
if exist "%~dp1%newName%" del /F /Q "%~dp1%newName%"
ren "%temp%.zip" "%newName%"

@echo clearing ...
if exist %temp% rd /s /q %temp%
if exist "%1" del /F /Q "%1"
if exist "%1.zip" del /F /Q "%1.zip"
if exist replace.vbs del /F /Q replace.vbs

@echo.
@echo done.
goto :EOF


:createScript
    
    if exist replace.vbs attrib -R -S -H replace.vbs
    if exist replace.vbs del /F /Q replace.vbs
    @echo On Error Resume Next                                      > replace.vbs
    @echo filePath = wscript.arguments(0)                           >> replace.vbs
    @echo oldTxt = URLDecode(wscript.arguments(1))                  >> replace.vbs
    @echo newTxt = URLDecode(wscript.arguments(2))                  >> replace.vbs
    @echo set fso = CreateObject("Scripting.FileSystemObject")      >> replace.vbs
    @echo Set readerStream = CreateObject("Adodb.Stream")           >> replace.vbs
    @echo readerStream.Type = 2                                     >> replace.vbs
    @echo readerStream.charset = "utf-8"                            >> replace.vbs
    @echo readerStream.Open                                         >> replace.vbs
    @echo readerStream.LoadFromFile filePath                        >> replace.vbs
    @echo text = readerStream.readtext                              >> replace.vbs
    @echo readerStream.close                                        >> replace.vbs
    @echo If InStr(text, oldTxt) ^>0 Then                           >> replace.vbs
    @echo     text = Replace(text, oldTxt, newTxt, 1, -1, 0)        >> replace.vbs
    @echo     fso.getfile(filePath).attributes = 0                  >> replace.vbs
    @echo     fso.deleteFile(filePath)                              >> replace.vbs
    @echo     Set writerStream = CreateObject("Adodb.Stream")       >> replace.vbs
    @echo     writerStream.Type = 2                                 >> replace.vbs
    @echo     writerStream.charset = "utf-8"                        >> replace.vbs
    @echo     writerStream.Open                                     >> replace.vbs
    @echo     writerStream.WriteText text                           >> replace.vbs
    @echo     writerStream.SaveToFile filePath                      >> replace.vbs
    @echo     writerStream.close                                    >> replace.vbs
    @echo End If                                                    >> replace.vbs
    @echo set fso = Nothing                                         >> replace.vbs
    @echo Function URLDecode(strURL)                                >> replace.vbs
    @echo   Dim I                                                   >> replace.vbs
    @echo   If InStr(strURL, "%%") = 0 Then                         >> replace.vbs
    @echo     URLDecode = strURL                                    >> replace.vbs
    @echo     Exit Function                                         >> replace.vbs
    @echo   End If                                                  >> replace.vbs
    @echo   For I = 1 To Len(strURL)                                >> replace.vbs
    @echo     If Mid(strURL, I, 1) = "%%" Then                      >> replace.vbs
    @echo       If Eval("&H" ^& Mid(strURL, I + 1, 2)) ^> 127 Then  >> replace.vbs
    @echo         URLDecode = URLDecode ^& Chr(Eval("&H" ^& Mid(strURL, I + 1, 2) ^& Mid(strURL, I + 4, 2)))    >> replace.vbs
    @echo         I = I + 5                                         >> replace.vbs
    @echo       Else                                                >> replace.vbs
    @echo         URLDecode = URLDecode ^& Chr(Eval("&H" ^& Mid(strURL, I + 1, 2)))        >> replace.vbs
    @echo         I = I + 2                                         >> replace.vbs
    @echo       End If                                              >> replace.vbs
    @echo     Else                                                  >> replace.vbs
    @echo       URLDecode = URLDecode ^& Mid(strURL, I, 1)          >> replace.vbs
    @echo     End If                                                >> replace.vbs
    @echo   Next                                                    >> replace.vbs
    @echo End Function                                              >> replace.vbs
goto :EOF
