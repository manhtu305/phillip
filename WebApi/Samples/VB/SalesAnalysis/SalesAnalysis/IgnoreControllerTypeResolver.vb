﻿Imports System.Web.Http.Controllers
Imports System.Web.Http.Dispatcher

Friend Class IgnoreControllerTypeResolver
    Inherits DefaultHttpControllerTypeResolver

    Public Sub New(controllerTypes As IEnumerable(Of Type))
        MyBase.New(Function(t) IsControllerType(t, controllerTypes))
    End Sub

    Private Shared Function IsControllerType(t As Type, controllerTypes As IEnumerable(Of Type)) As Boolean
        If t IsNot Nothing AndAlso t.IsClass AndAlso (t.IsVisible AndAlso Not t.IsAbstract) AndAlso GetType(IHttpController).IsAssignableFrom(t) AndAlso HasValidControllerName(t) Then
            Return Not controllerTypes.Contains(t)
        End If

        Return False
    End Function

    Private Shared Function HasValidControllerName(controllerType As Type) As Boolean
        Dim str As String = DefaultHttpControllerSelector.ControllerSuffix
        If controllerType.Name.Length > str.Length Then
            Return controllerType.Name.EndsWith(str, StringComparison.OrdinalIgnoreCase)
        End If
        Return False
    End Function

End Class
