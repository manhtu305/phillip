﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <title>@ViewBag.Title</title>
    <link rel="apple-touch-icon" sizes="180x180" href="~/Content/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="~/Content/favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="~/Content/favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="~/Content/favicon/manifest.json">
    <link rel="mask-icon" href="~/Content/favicon/safari-pinned-tab.svg" color="#503b75">
    
    @Html.C1().Styles()
    @Styles.Render("~/Content/css")
    @Scripts.Render("~/bundles/modernizr")
    @Html.C1().Scripts().Olap()
</head>
<body>
    <div class="hide">
        @Html.Partial("_SiteNav", True)
    </div>
    <header>
        <div class="hamburger-nav-btn narrow-screen"><span class="icon-bars"></span></div>
        <a class="logo-container" href="https://www.grapecity.com/en/webapi" target="_blank">
            <i class="gcicon-product-c1"></i>@Html.Raw(Resources.Resource.Layout_ComponentOne)
        </a>
        @If (ViewBag.Email IsNot Nothing) Then
            @<div Class="wide-screen user-panel">
                <span Class="glyphicon glyphicon-user"></span> <span>@ViewBag.Email (@ViewBag.Role)</span><br />
                <a Class="signout-btn" href="javascript:void(0)" onclick="logout()">@Html.Raw(Resources.Resource.SiteNav_SignOut)</a>
            </div>
        End If
        <div Class="task-bar">
            <span Class="semi-bold">@ViewBag.Title</span>
            @Html.Partial("_SiteNav", False)
        </div>
    </header>
    @RenderBody()
    <footer>
        <a href="http://www.grapecity.com/">
            <i class="gcicon-logo-gc-horiz"></i>
        </a>
        <p>
            © @DateTime.Now.Year @Html.Raw(Resources.Resource.Layout_License1)<br />
            @Html.Raw(Resources.Resource.Layout_License2)Layout_License2)
        </p>
        @Code
            Dim currentUrl = Request.Url
            Dim urlStr = currentUrl.OriginalString.Substring(0, currentUrl.OriginalString.Length - (If(currentUrl.Query Is Nothing, 0, currentUrl.Query.Length)))
            @<a href="http://www.facebook.com/sharer.php?u=@urlStr" target="_blank">
                <img src="~/Content/css/images/icons/32/picons36.png" alt="facebook" />
            </a>
            @<a href="http://twitter.com/share?text=Have you seen this? C1Studio Web API&url=@urlStr" target="_blank">
                <img src="~/Content/css/images/icons/32/picons33.png" alt="Twitter" />
            </a>
        End Code
    </footer>
    <div id="msgPopup">
        <div Class="modal-body">@Html.Raw(Resources.Resource.Layout_Loading)</div>
        <div Class="modal-footer">
            <button Class="btn" onclick="closeMsgPopup()">@Html.Raw(Resources.Resource.Layout_Close)</button>
        </div>
    </div>
    @Html.C1().Popup("#msgPopup").Modal(True).HideTrigger(PopupTrigger.None)

    @Scripts.Render("~/bundles/jquery")
    @Scripts.Render("~/bundles/bootstrap")
    @Scripts.Render("~/bundles/app")
    <Script>
        var homeUrl = '@Url.Action("Index", "Home")';
    </Script>
    @RenderSection("scripts", required:=False)
</body>
</html>
