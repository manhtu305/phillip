﻿Imports System.Web.Http
Imports System.Web.Http.Dispatcher
Imports Microsoft.Owin.Security.OAuth

Public Module WebApiConfig
    Public Sub Register(config As HttpConfiguration)
        ' Web API configuration and services
        ' Configure Web API to use only bearer token authentication.
        config.SuppressDefaultHostAuthentication()
        config.Filters.Add(New HostAuthenticationFilter(OAuthDefaults.AuthenticationType))

        ' Web API routes
        config.MapHttpAttributeRoutes(New CustomDirectRouteProvider())

        config.Routes.MapHttpRoute(
          name:="DefaultApi",
          routeTemplate:="api/{controller}/{id}",
          defaults:=New With {.id = RouteParameter.Optional}
        )

        config.Services.Replace(GetType(IHttpControllerTypeResolver),
            New IgnoreControllerTypeResolver(New Type() {GetType(C1.Web.Api.DataEngine.DataEngineController)}))
    End Sub
End Module
