﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WebAPIExplorer.Data;
using Windows.UI.Xaml.Media.Imaging;
using System.Resources;
using System.Reflection;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace WebAPIExplorer
{
    /// <summary>
    /// Generate Barcode page 
    /// </summary>
    public sealed partial class GenerateBarcodePage : Page
    {
        private Manage MngObj = new Manage();
        public GenerateBarcodePage()
        {
            this.InitializeComponent();
            SetGenerateBarcodePage();
        }

        private void SetGenerateBarcodePage()
        {
            TxtBlkGenBarcodeTitle.Text = MngObj.ResLoader.GetString("GenBarcodeTitle");
            TxtBlkGenBarcodeDesc.Text = MngObj.ResLoader.GetString("DescHeader");
            TxtBlkGenBarcodeDescDetails.Text = MngObj.ResLoader.GetString("GenBarcodeDescDetails");
            TxtBlkGenBarcodeParamsHeader.Text = MngObj.ResLoader.GetString("ParamHeader");
            BtnGenBarcode.Content = MngObj.ResLoader.GetString("BtnGenBarcode");

            TxtBlkGenBarcodeType.Text = "Type";
            TxtBlkGenBarcodeTypeDesc.Text = MngObj.ResLoader.GetString("GenBarcodeTypeDesc");
            CmbGenBarcodeImgType.DisplayMemberPath = "Text";
            CmbGenBarcodeImgType.SelectedValuePath = "Value";
            CmbGenBarcodeImgType.ItemsSource = MngObj.GetBC_ImgTypeDDList();
            CmbGenBarcodeImgType.SelectedIndex = 0;

            TxtBlkGenBarcodeText.Text = "Text";
            TxtBlkGenBarcodeTextDesc.Text = MngObj.ResLoader.GetString("GenBarcodeTextDesc");

            TxtBlkGenBarcodeCodeType.Text = "CodeType";
            TxtBlkGenBarcodeCodeTypeDesc.Text = MngObj.ResLoader.GetString("GenBarcodeCodeTypeDesc");
            CmbGenBarcodeCodeType.DisplayMemberPath = "Text";
            CmbGenBarcodeCodeType.SelectedValuePath = "Value";
            CmbGenBarcodeCodeType.ItemsSource = MngObj.GetBC_CodeTypeDDList();
            CmbGenBarcodeCodeType.SelectedValue = "Code39";

            TxtBlkGenBarcodeBackColor.Text = "BackColor";
            TxtBlkGenBarcodeBackColorDesc.Text = MngObj.ResLoader.GetString("GenBarcodeBackColorDesc");
            CmbGenBarcodeBackColor.DisplayMemberPath = "Text";
            CmbGenBarcodeBackColor.SelectedValuePath = "Value";
            CmbGenBarcodeBackColor.ItemsSource = MngObj.GetBC_ColorDDList();
            CmbGenBarcodeBackColor.SelectedIndex = 1;

            TxtBlkGenBarcodeForeColor.Text = "ForeColor";
            TxtBlkGenBarcodeForeColorDesc.Text = MngObj.ResLoader.GetString("GenBarcodeForeColorDesc");
            CmbGenBarcodeForeColor.DisplayMemberPath = "Text";
            CmbGenBarcodeForeColor.SelectedValuePath = "Value";
            CmbGenBarcodeForeColor.ItemsSource = MngObj.GetBC_ColorDDList();
            CmbGenBarcodeForeColor.SelectedIndex = 2;

            TxtBlkGenBarcodeCaptionPosition.Text = "CaptionPosition";
            TxtBlkGenBarcodeCaptionPositionDesc.Text = MngObj.ResLoader.GetString("GenBarcodeCaptionPositionDesc");
            CmbGenBarcodeCaptionPosition.DisplayMemberPath = "Text";
            CmbGenBarcodeCaptionPosition.SelectedValuePath = "Value";
            CmbGenBarcodeCaptionPosition.ItemsSource = MngObj.GetBC_CaptionPositionDDList();
            CmbGenBarcodeCaptionPosition.SelectedIndex = 0;

            TxtBlkGenBarcodeCaptionAlign.Text = "CaptionAlignment";
            TxtBlkGenBarcodeCaptionAlignDesc.Text = MngObj.ResLoader.GetString("GenBarcodeCaptionAlignDesc");
            CmbGenBarcodeCaptionAlign.DisplayMemberPath = "Text";
            CmbGenBarcodeCaptionAlign.SelectedValuePath = "Value";
            CmbGenBarcodeCaptionAlign.ItemsSource = MngObj.GetBC_CaptionAlignmentDDList();
            CmbGenBarcodeCaptionAlign.SelectedIndex = 0;

            TxtBlkGenBarcodeCheckSumEnabled.Text = "CheckSumEnabled";
            TxtBlkGenBarcodeCheckSumEnabledDesc.Text = MngObj.ResLoader.GetString("GenBarcodeCheckSumEnabledDesc");
            CBGenBarcodeCheckSumEnabled.IsChecked = true;

            //QRCode Settings
            TxtBlkGenBarcodeParamsQRCodeSettingsHeader.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeParamsQRCodeSettingsHeader");
            TxtBlkGenBarcodeQRCodeModel.Text = "Model";
            TxtBlkGenBarcodeQRCodeModelDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeQRCodeModelDesc");
            CmbGenBarcodeQRCodeModel.DisplayMemberPath = "Text";
            CmbGenBarcodeQRCodeModel.SelectedValuePath = "Value";
            CmbGenBarcodeQRCodeModel.ItemsSource = MngObj.GetBC_QRCodeModelDDList();
            CmbGenBarcodeQRCodeModel.SelectedIndex = 0;
            TxtBlkGenBarcodeQRCodeErrLevel.Text = "ErrorLevel";
            CmbGenBarcodeQRCodeErrLevel.DisplayMemberPath = "Text";
            CmbGenBarcodeQRCodeErrLevel.SelectedValuePath = "Value";
            CmbGenBarcodeQRCodeErrLevel.ItemsSource = MngObj.GetBC_QRCodeErrorLevelDDList();
            CmbGenBarcodeQRCodeErrLevel.SelectedIndex = 0;
            TxtBlkGenBarcodeQRCodeErrLevelDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeQRCodeErrLevelDesc");
            TxtBlkGenBarcodeQRCodeVersion.Text = "Version";
            TxtBoxGenBarcodeQRCodeVersion.Text = "-1";
            TxtBlkGenBarcodeQRCodeVersionDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeQRCodeVersionDesc");
            TxtBlkGenBarcodeQRCodeMask.Text = "Mask";
            CmbGenBarcodeQRCodeMask.DisplayMemberPath = "Text";
            CmbGenBarcodeQRCodeMask.SelectedValuePath = "Value";
            CmbGenBarcodeQRCodeMask.ItemsSource = MngObj.GetBC_QRCodeMaskDDList();
            CmbGenBarcodeQRCodeMask.SelectedIndex = 0;
            TxtBlkGenBarcodeQRCodeMaskDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeQRCodeMaskDesc");
            TxtBlkGenBarcodeQRCodeConnectionNumber.Text = "ConnectionNumber";
            TxtBoxGenBarcodeQRCodeConnectionNumber.Text = "0";
            TxtBlkGenBarcodeQRCodeConnectionNumberDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeQRCodeConnectionNumberDesc");
            TxtBlkGenBarcodeQRCodeConnection.Text = "Connection";
            TxtBlkGenBarcodeQRCodeConnectionDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeQRCodeConnectionDesc");

            //PDF417 Settings
            TxtBlkGenBarcodeParamsPDF417SettingsHeader.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeParamsPDF417SettingsHeader");
            TxtBlkGenBarcodePDF417Column.Text = "Column";
            TxtBlkGenBarcodePDF417ColumnDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodePDF417ColumnDesc");
            TxtBlkGenBarcodePDF417Row.Text = "Row";
            TxtBlkGenBarcodePDF417RowDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodePDF417RowDesc");
            TxtBlkGenBarcodePDF417ErrLevel.Text = "ErrorLevel";
            TxtBlkGenBarcodePDF417ErrLevelDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodePDF417ErrLevelDesc");
            TxtBlkGenBarcodePDF417Type.Text = "Type";
            TxtBlkGenBarcodePDF417TypeDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodePDF417TypeDesc");
            CmbGenBarcodePDF417Type.DisplayMemberPath = "Text";
            CmbGenBarcodePDF417Type.SelectedValuePath = "Value";
            CmbGenBarcodePDF417Type.ItemsSource = MngObj.GetBC_PDF417TypeDDList();
            CmbGenBarcodePDF417Type.SelectedIndex = 0;

            //Code49 Settings
            TxtBlkGenBarcodeParamsCode49SettingsHeader.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeParamsCode49SettingsHeader");
            TxtBlkGenBarcodeCode49Group.Text = "Group";
            TxtBlkGenBarcodeCode49GroupDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeCode49GroupDesc");
            TxtBlkGenBarcodeCode49Grouping.Text = "Grouping";
            TxtBlkGenBarcodeCode49GroupingDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeCode49GroupingDesc");

            //RssExpandedStacked Settings
            TxtBlkGenBarcodeParamsRssExpandedStackedSettingsHeader.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeParamsRssExpandedStackedSettingsHeader");
            TxtBlkGenBarcodeRssExpandedStackedRowCount.Text = "RowCount";
            TxtBlkGenBarcodeRssExpandedStackedRowCountDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeRssExpandedStackedRowCountDesc");


            //MicroPDF417 Settings
            TxtBlkGenBarcodeParamsMicroPDF417SettingsHeader.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeParamsMicroPDF417SettingsHeader");
            TxtBlkGenBarcodeMicroPDF417CompactionMode.Text = "CompactionMode";
            CmbGenBarcodeMicroPDF417CompactionMode.DisplayMemberPath = "Text";
            CmbGenBarcodeMicroPDF417CompactionMode.SelectedValuePath = "Value";
            CmbGenBarcodeMicroPDF417CompactionMode.ItemsSource = MngObj.GetBC_MicroPDF417CompactionDDList();
            CmbGenBarcodeMicroPDF417CompactionMode.SelectedIndex = 0;
            TxtBlkGenBarcodeMicroPDF417CompactionModeDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeMicroPDF417CompactionModeDesc");
            TxtBlkGenBarcodeMicroPDF417FileID.Text = "FileID";
            TxtBlkGenBarcodeMicroPDF417FileIDDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeMicroPDF417FileIDDesc");
            TxtBlkGenBarcodeMicroPDF417SegmentCount.Text = "SegmentCount";
            TxtBlkGenBarcodeMicroPDF417SegmentCountDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeMicroPDF417SegmentCountDesc");
            TxtBlkGenBarcodeMicroPDF417SegmentIndex.Text = "SegmentIndex";
            TxtBlkGenBarcodeMicroPDF417SegmentIndexDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeMicroPDF417SegmentIndexDesc");
            TxtBlkGenBarcodeMicroPDF417Version.Text = "Version";
            CmbGenBarcodeMicroPDF417Version.DisplayMemberPath = "Text";
            CmbGenBarcodeMicroPDF417Version.SelectedValuePath = "Value";
            CmbGenBarcodeMicroPDF417Version.ItemsSource = MngObj.GetBC_MicroPDF417VersionDDList();
            CmbGenBarcodeMicroPDF417Version.SelectedIndex = 0;
            TxtBlkGenBarcodeMicroPDF417VersionDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeMicroPDF417VersionDesc");

            //Code25intlv Settings
            TxtBlkGenBarcodeParamsCode25intlvSettingsHeader.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeParamsCode25intlvSettingsHeader");
            TxtBlkGenBarcodeCode25intlvLineStroke.Text = "LineStroke";
            CmbGenBarcodeCode25intlvLineStroke.DisplayMemberPath = "Text";
            CmbGenBarcodeCode25intlvLineStroke.DisplayMemberPath = "Value";
            CmbGenBarcodeCode25intlvLineStroke.ItemsSource = MngObj.GetBC_ColorDDList();
            CmbGenBarcodeCode25intlvLineStroke.SelectedIndex = 0;
            TxtBlkGenBarcodeCode25intlvLineStrokeDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeCode25intlvLineStrokeDesc");
            TxtBlkGenBarcodeCode25intlvBearBar.Text = "BearBar";
            TxtBlkGenBarcodeCode25intlvBearBarDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeCode25intlvBearBarDesc");
            TxtBlkGenBarcodeCode25intlvLineStrokeThickness.Text = "LineStrokeThickness";
            TxtBlkGenBarcodeCode25intlvLineStrokeThicknessDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeCode25intlvLineStrokeThicknessDesc");

            //EAN128FNC1 Settings
            TxtBlkGenBarcodeParamsEAN128FNC1SettingsHeader.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeParamsEAN128FNC1SettingsHeader");
            TxtBlkGenBarcodeEAN128FNC1BarAdjust.Text = "BarAdjust";
            TxtBlkGenBarcodeEAN128FNC1BarAdjustDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeEAN128FNC1BarAdjustDesc");
            TxtBlkGenBarcodeEAN128FNC1Dpi.Text = "Dpi";
            TxtBlkGenBarcodeEAN128FNC1DpiDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeEAN128FNC1DpiDesc");
            TxtBlkGenBarcodeEAN128FNC1ModuleSize.Text = "ModuleSize";
            TxtBlkGenBarcodeEAN128FNC1ModuleSizeDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeEAN128FNC1ModuleSizeDesc");

            //DataMatrix Settings
            TxtBlkGenBarcodeParamsDataMatrixSettingsHeader.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeParamsDataMatrixSettingsHeader");
            TxtBlkGenBarcodeDataMatrixEccMode.Text = "EccMode";
            CmbGenBarcodeDataMatrixEccMode.DisplayMemberPath = "Text";
            CmbGenBarcodeDataMatrixEccMode.SelectedValuePath = "Value";
            CmbGenBarcodeDataMatrixEccMode.ItemsSource = MngObj.GetBC_DataMatrixEccModeDDList();
            CmbGenBarcodeDataMatrixEccMode.SelectedIndex = 0;
            TxtBlkGenBarcodeDataMatrixEccModeDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeDataMatrixEccModeDesc");
            TxtBlkGenBarcodeDataMatrixEcc200SymbolSize.Text = "Ecc200SymbolSize";
            CmbGenBarcodeDataMatrixEcc200SymbolSize.DisplayMemberPath = "Text";
            CmbGenBarcodeDataMatrixEcc200SymbolSize.SelectedValuePath = "Value";
            CmbGenBarcodeDataMatrixEcc200SymbolSize.ItemsSource = MngObj.GetBC_DataMatrixEcc200SymbolSizeDDList();
            CmbGenBarcodeDataMatrixEcc200SymbolSize.SelectedIndex = 0;
            TxtBlkGenBarcodeDataMatrixEcc200SymbolSizeDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeDataMatrixEcc200SymbolSizeDesc");
            TxtBlkGenBarcodeDataMatrixEcc200EncodingMode.Text = "Ecc200EncodingMode";
            CmbGenBarcodeDataMatrixEcc200EncodingMode.DisplayMemberPath = "Text";
            CmbGenBarcodeDataMatrixEcc200EncodingMode.SelectedValuePath = "Value";
            CmbGenBarcodeDataMatrixEcc200EncodingMode.ItemsSource = MngObj.GetBC_DataMatrixEcc200EncodingModeDDList();
            CmbGenBarcodeDataMatrixEcc200EncodingMode.SelectedIndex = 0;
            TxtBlkGenBarcodeDataMatrixEcc200EncodingModeDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeDataMatrixEcc200EncodingModeDesc");
            TxtBlkGenBarcodeDataMatrixEcc000_140SymbolSize.Text = "Ecc000_140SymbolSize";
            TxtBlkGenBarcodeDataMatrixEcc000_140SymbolSizeDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeDataMatrixEcc000_140SymbolSizeDesc");
            TxtBlkGenBarcodeDataMatrixStructureNumber.Text = "StructureNumber";
            TxtBlkGenBarcodeDataMatrixStructureNumberDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeDataMatrixStructureNumberDesc");
            TxtBlkGenBarcodeDataMatrixStructuredAppend.Text = "StructuredAppend";
            TxtBlkGenBarcodeDataMatrixStructuredAppendDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeDataMatrixStructuredAppendDesc");
            TxtBlkGenBarcodeDataMatrixFileIdentifier.Text = "FileIdentifier";
            TxtBlkGenBarcodeDataMatrixFileIdentifierDesc.Text = MngObj.ResLoader.GetString("TxtBlkGenBarcodeDataMatrixFileIdentifierDesc");

            //Result
            TxtBlkGenBarcodeResultRequestUrlTitle.Text = MngObj.ResLoader.GetString("ResultRequestUrlTitle");
            TxtBlkGenBarcodeResultBarcode.Text = MngObj.ResLoader.GetString("GenBarcodeResultBarcode");

        }

        private void BtnGenBarcode_Click(object sender, RoutedEventArgs e)
        {
            TxtBlkGenBarcodeResultMsg.Text = "";
            try
            {
                string FullURL = App.APIBaseUri + "api/barcode?";
                FullURL += "Text=" + TxtBoxGenBarcodeText.Text;
                FullURL += "&Type=" + CmbGenBarcodeImgType.SelectedValue.ToString();
                if (CmbGenBarcodeCodeType.SelectedValue.ToString() != "None")
                    FullURL += "&CodeType=" + CmbGenBarcodeCodeType.SelectedValue.ToString();
                FullURL += "&BackColor=" + CmbGenBarcodeBackColor.SelectedValue.ToString();
                FullURL += "&ForeColor=" + CmbGenBarcodeForeColor.SelectedValue.ToString();
                if (CmbGenBarcodeCaptionPosition.SelectedValue.ToString() != "None")
                    FullURL += "&CaptionPosition=" + CmbGenBarcodeCaptionPosition.SelectedValue.ToString();
                FullURL += "&CaptionAlignment=" + CmbGenBarcodeCaptionAlign.SelectedValue.ToString();
                FullURL += "&barcodeCheckSumEnabled=" + CBGenBarcodeCheckSumEnabled.IsChecked.ToString();

                if (CmbGenBarcodeCodeType.SelectedValue.ToString() == "QRCode")
                {
                    FullURL += "&QRCodeOptions.Model=" + CmbGenBarcodeQRCodeModel.SelectedValue.ToString();
                    FullURL += "&QRCodeOptions.ErrorLevel=" + CmbGenBarcodeQRCodeErrLevel.SelectedValue.ToString();
                    FullURL += "&QRCodeOptions.Version=" + TxtBoxGenBarcodeQRCodeVersion.Text.Trim();
                    FullURL += "&QRCodeOptions.Mask=" + CmbGenBarcodeQRCodeMask.SelectedValue.ToString();
                    FullURL += "&QRCodeOptions.ConnectionNumber=" + TxtBoxGenBarcodeQRCodeConnectionNumber.Text.Trim();
                    FullURL += "&QRCodeOptions.Connection=" + CBoxGenBarcodeQRCodeConnection.IsChecked.ToString();
                }
                if (CmbGenBarcodeCodeType.SelectedValue.ToString() == "Pdf417")
                {
                    FullURL += "&Pdf417Options.Column=" + TxtBoxGenBarcodePDF417Column.Text.Trim();
                    FullURL += "&Pdf417Options.Row=" + TxtBoxGenBarcodePDF417Row.Text.Trim();
                    FullURL += "&Pdf417Options.ErrorLevel=" + TxtBoxGenBarcodePDF417ErrLevel.Text.Trim();
                    FullURL += "&Pdf417Options.Type=" + CmbGenBarcodePDF417Type.SelectedValue.ToString();
                }
                if (CmbGenBarcodeCodeType.SelectedValue.ToString() == "Code49")
                {
                    FullURL += "&Code49Options.Group=" + TxtBoxGenBarcodeCode49Group.Text.Trim();
                    FullURL += "&Code49Options.Grouping=" + CBoxGenBarcodeCode49Grouping.IsChecked.ToString();
                }
                if (CmbGenBarcodeCodeType.SelectedValue.ToString() == "RssExpandedStacked")
                {
                    FullURL += "&RssExpandedStackedOptions.RowCount=" + TxtBoxGenBarcodeRssExpandedStackedRowCount.Text.Trim();
                }
                if (CmbGenBarcodeCodeType.SelectedValue.ToString() == "MicroPDF417")
                {
                    FullURL += "&MicroPDF417Options.CompactionMode=" + CmbGenBarcodeMicroPDF417CompactionMode.SelectedValue.ToString();
                    FullURL += "&MicroPDF417Options.FileID=" + TxtBoxGenBarcodeMicroPDF417FileID.Text.Trim();
                    FullURL += "&MicroPDF417Options.SegmentCount=" + TxtBoxGenBarcodeMicroPDF417SegmentCount.Text.Trim();
                    FullURL += "&MicroPDF417Options.SegmentIndex=" + TxtBoxGenBarcodeMicroPDF417SegmentIndex.Text.Trim();
                    FullURL += "&MicroPDF417Options.Version=" + CmbGenBarcodeMicroPDF417Version.SelectedValue.ToString();
                }
                if (CmbGenBarcodeCodeType.SelectedValue.ToString() == "Code25intlv")
                {
                    FullURL += "&Code25intlvOptions.LineStroke=" + CmbGenBarcodeCode25intlvLineStroke.SelectedValue.ToString();
                    FullURL += "&Code25intlvOptions.BearBar=" + CBoxGenBarcodeCode25intlvBearBar.IsChecked.ToString();
                    FullURL += "&Code25intlvOptions.LineStrokeThickness=" + TxtBoxGenBarcodeCode25intlvLineStrokeThickness.Text.Trim();
                }
                if (CmbGenBarcodeCodeType.SelectedValue.ToString() == "EAN128FNC1")
                {
                    FullURL += "&Ean128Fnc1Options.BarAdjust=" + TxtBoxGenBarcodeEAN128FNC1BarAdjust.Text.Trim();
                    FullURL += "&Ean128Fnc1Options.Dpi=" + TxtBoxGenBarcodeEAN128FNC1Dpi.Text.Trim();
                    FullURL += "&Ean128Fnc1Options.ModuleSize=" + TxtBoxGenBarcodeEAN128FNC1ModuleSize.Text.Trim();
                }
                if (CmbGenBarcodeCodeType.SelectedValue.ToString() == "DataMatrix")
                {
                    FullURL += "&DataMatrixOptions.EccMode=" + CmbGenBarcodeDataMatrixEccMode.SelectedValue.ToString();
                    FullURL += "&DataMatrixOptions.Ecc200SymbolSize=" + CmbGenBarcodeDataMatrixEcc000_140SymbolSize.SelectedValue.ToString();
                    FullURL += "&DataMatrixOptions.Ecc200EncodingMode=" + CmbGenBarcodeDataMatrixEcc200EncodingMode.SelectedValue.ToString();
                    FullURL += "&DataMatrixOptions.Ecc000_140SymbolSize=" + CmbGenBarcodeDataMatrixEcc000_140SymbolSize.SelectedValue.ToString();
                    FullURL += "&DataMatrixOptions.StructureNumber=" + TxtBoxGenBarcodeDataMatrixStructureNumber.Text.Trim();
                    FullURL += "&DataMatrixOptions.StructuredAppend=" + CBoxGenBarcodeDataMatrixStructuredAppend.IsChecked.ToString();
                    FullURL += "&DataMatrixOptions.FileIdentifier=" + TxtBoxGenBarcodeDataMatrixFileIdentifier.Text.Trim();
                }

                TxtBlkGenBarcodeResultRequestUrl.Text = FullURL;
                BitmapImage Img1 = new BitmapImage(new Uri(FullURL));
                ImgGenBarcodeResultBarcode.Source = Img1;
                StkPnlBarcodeResult.Visibility = Visibility.Visible;
            }
            catch(Exception ex)
            {
                TxtBlkGenBarcodeResultMsg.Text = MngObj.ResLoader.GetString("ErrorMsg") + ex.Message;
            }
        }

        private void CmbGenBarcodeCodeType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            StkPnlBarcodeParamQRCode.Visibility = Visibility.Collapsed;
            StkPnlBarcodeParamPDF417.Visibility = Visibility.Collapsed;
            StkPnlBarcodeParamCode49.Visibility = Visibility.Collapsed;
            StkPnlBarcodeParamRssExpandedStacked.Visibility = Visibility.Collapsed;
            StkPnlBarcodeParamMicroPDF417.Visibility = Visibility.Collapsed;
            StkPnlBarcodeParamCode25intlv.Visibility = Visibility.Collapsed;
            StkPnlBarcodeParamEAN128FNC1.Visibility = Visibility.Collapsed;
            StkPnlBarcodeParamDataMatrix.Visibility = Visibility.Collapsed;

            if (CmbGenBarcodeCodeType.SelectedValue.ToString() == "QRCode")
            {
                StkPnlBarcodeParamQRCode.Visibility = Visibility.Visible;

            }
            else if (CmbGenBarcodeCodeType.SelectedValue.ToString() == "PDF417")
            {
                StkPnlBarcodeParamPDF417.Visibility = Visibility.Visible;
            }
            else if (CmbGenBarcodeCodeType.SelectedValue.ToString() == "Code49")
            {
                StkPnlBarcodeParamCode49.Visibility = Visibility.Visible;
            }
            else if (CmbGenBarcodeCodeType.SelectedValue.ToString() == "RssExpandedStacked")
            {
                StkPnlBarcodeParamRssExpandedStacked.Visibility = Visibility.Visible;
            }
            else if (CmbGenBarcodeCodeType.SelectedValue.ToString() == "MicroPDF417")
            {
                StkPnlBarcodeParamMicroPDF417.Visibility = Visibility.Visible;
            }
            else if (CmbGenBarcodeCodeType.SelectedValue.ToString() == "Code25intlv")
            {
                StkPnlBarcodeParamCode25intlv.Visibility = Visibility.Visible;
            }
            else if (CmbGenBarcodeCodeType.SelectedValue.ToString() == "EAN128FNC1")
            {
                StkPnlBarcodeParamEAN128FNC1.Visibility = Visibility.Visible;
            }
            else if (CmbGenBarcodeCodeType.SelectedValue.ToString() == "DataMatrix")
            {
                StkPnlBarcodeParamDataMatrix.Visibility = Visibility.Visible;
            }
        }

        private void StkPnlGenBarcodeGET1_Link_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (StkPnlGenBarcodeGET1_Content.Visibility == Visibility.Collapsed)
                StkPnlGenBarcodeGET1_Content.Visibility = Visibility.Visible;
            else
                StkPnlGenBarcodeGET1_Content.Visibility = Visibility.Collapsed;
        }
    }
}
