﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPIExplorer.Data
{
    public class DDLModel
    {
        public DDLModel() { }
        public string Text { get; set; }
        public string Value { get; set; }
    }
}
