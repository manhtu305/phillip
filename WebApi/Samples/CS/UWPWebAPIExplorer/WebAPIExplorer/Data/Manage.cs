﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;

namespace WebAPIExplorer.Data
{
    public class Manage
    {
        //Constructor();
        public Manage()
        {
        }

        public ResourceLoader ResLoader = new ResourceLoader("WebAPIExplorerResource");

        public string GetResponseSchema()
        {
            return System.IO.File.ReadAllText(@"Assets\ResponseSchema.txt");
        }

        public string GetJSONData()
        {
            return System.IO.File.ReadAllText(@"Assets\JSONData.txt");
        }

        public IEnumerable<DDLModel> GetItemsDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "Items: 5", Value = "5" });
            RetValue.Add(new DDLModel() { Text = "Items: 50", Value = "50" });
            RetValue.Add(new DDLModel() { Text = "Items: 500", Value = "500" });
            RetValue.Add(new DDLModel() { Text = "Items: 5000", Value = "5000" });
            return RetValue;
        }

        public IEnumerable<DDLModel> GetColumnVisDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "Column Visibility: Show", Value = "Show" });
            RetValue.Add(new DDLModel() { Text = "Column Visibility: Hide", Value = "Hide" });
            return RetValue;
        }

        public IEnumerable<DDLModel> GetExportFormatDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "Export Format: XLSX", Value = "XLSX" });
            RetValue.Add(new DDLModel() { Text = "Export Format: XLS", Value = "XLS" });
            RetValue.Add(new DDLModel() { Text = "Export Format: CSV", Value = "CSV" });
            return RetValue;
        }

        //To Get List of Barcode Image Types
        public IEnumerable<DDLModel> GetBC_ImgTypeDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "jpeg", Value = "Jpeg" });
            RetValue.Add(new DDLModel() { Text = "Bmp", Value = "Bmp" });
            RetValue.Add(new DDLModel() { Text = "Gif", Value = "Gif" });
            RetValue.Add(new DDLModel() { Text = "Tiff", Value = "Tiff" });
            return RetValue;
        }

        //To Get List of Barcode Types
        public IEnumerable<DDLModel> GetBC_CodeTypeDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "None", Value = "None" });
            RetValue.Add(new DDLModel() { Text = "Ansi39", Value = "Ansi39" });
            RetValue.Add(new DDLModel() { Text = "Ansi39x", Value = "Ansi39x" });
            RetValue.Add(new DDLModel() { Text = "Code_2_of_5", Value = "Code_2_of_5" });
            RetValue.Add(new DDLModel() { Text = "Code25intlv", Value = "Code25intlv" });
            RetValue.Add(new DDLModel() { Text = "Matrix_2_of_5", Value = "Matrix_2_of_5s" });
            RetValue.Add(new DDLModel() { Text = "Code39", Value = "Code39" });
            RetValue.Add(new DDLModel() { Text = "Code_128_A", Value = "Code_128_A" });
            RetValue.Add(new DDLModel() { Text = "Code_128_B", Value = "Code_128_B" });
            RetValue.Add(new DDLModel() { Text = "Code_128_C", Value = "Code_128_C" });
            RetValue.Add(new DDLModel() { Text = "Code_128auto", Value = "Code_128auto" });
            RetValue.Add(new DDLModel() { Text = "Code_93", Value = "Code_93" });
            RetValue.Add(new DDLModel() { Text = "Code93x", Value = "Code93x" });
            RetValue.Add(new DDLModel() { Text = "MSI", Value = "MSI" });
            RetValue.Add(new DDLModel() { Text = "PostNet", Value = "PostNet" });
            RetValue.Add(new DDLModel() { Text = "Codabar", Value = "Codabar" });
            RetValue.Add(new DDLModel() { Text = "EAN_8", Value = "EAN_8" });
            RetValue.Add(new DDLModel() { Text = "EAN_13", Value = "EAN_13" });
            RetValue.Add(new DDLModel() { Text = "UPC_A", Value = "UPC_A" });
            RetValue.Add(new DDLModel() { Text = "UPC_E0", Value = "UPC_E0" });
            RetValue.Add(new DDLModel() { Text = "UPC_E1", Value = "UPC_E1" });
            RetValue.Add(new DDLModel() { Text = "RM4SCC", Value = "RM4SCC" });
            RetValue.Add(new DDLModel() { Text = "UCCEAN128", Value = "UCCEAN128" });
            RetValue.Add(new DDLModel() { Text = "QRCode", Value = "QRCode" });
            RetValue.Add(new DDLModel() { Text = "Code49", Value = "Code49" });
            RetValue.Add(new DDLModel() { Text = "JapanesePostal", Value = "JapanesePostal" });
            RetValue.Add(new DDLModel() { Text = "Pdf417", Value = "Pdf417" });
            RetValue.Add(new DDLModel() { Text = "EAN128FNC1", Value = "EAN128FNC1" });
            RetValue.Add(new DDLModel() { Text = "RSS14", Value = "RSS14" });
            RetValue.Add(new DDLModel() { Text = "RSS14Truncated", Value = "RSS14Truncated" });
            RetValue.Add(new DDLModel() { Text = "RSS14Stacked", Value = "RSS14Stacked" });
            RetValue.Add(new DDLModel() { Text = "RSS14StackedOmnidirectional", Value = "RSS14StackedOmnidirectional" });
            RetValue.Add(new DDLModel() { Text = "RSSExpanded", Value = "RSSExpanded" });
            RetValue.Add(new DDLModel() { Text = "RSSExpandedStacked", Value = "RSSExpandedStacked" });
            RetValue.Add(new DDLModel() { Text = "RSSLimited", Value = "RSSLimited" });
            RetValue.Add(new DDLModel() { Text = "DataMatrix", Value = "DataMatrix" });
            RetValue.Add(new DDLModel() { Text = "MicroPDF417", Value = "MicroPDF417" });
            RetValue.Add(new DDLModel() { Text = "IntelligentMail", Value = "IntelligentMail" });
            return RetValue;
        }

        //To Get List of Barcode's Backcolor/Forecolor
        public IEnumerable<DDLModel> GetBC_ColorDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "Transparent", Value = "Transparent" });
            RetValue.Add(new DDLModel() { Text = "White", Value = "White" });
            RetValue.Add(new DDLModel() { Text = "Black", Value = "Black" });
            RetValue.Add(new DDLModel() { Text = "Red", Value = "Red" });
            RetValue.Add(new DDLModel() { Text = "Green", Value = "Green" });
            RetValue.Add(new DDLModel() { Text = "Blue", Value = "Blue" });
            RetValue.Add(new DDLModel() { Text = "Yellow", Value = "Yellow" });
            RetValue.Add(new DDLModel() { Text = "Orange", Value = "Orange" });
            return RetValue;
        }

        //To Get List of Barcode's Caption Position
        public IEnumerable<DDLModel> GetBC_CaptionPositionDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "None", Value = "None" });
            RetValue.Add(new DDLModel() { Text = "Above", Value = "Above" });
            RetValue.Add(new DDLModel() { Text = "Below", Value = "Below" });
            return RetValue;
        }

        //To Get List of Barcode's Caption Alignment
        public IEnumerable<DDLModel> GetBC_CaptionAlignmentDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "Left", Value = "Left" });
            RetValue.Add(new DDLModel() { Text = "Center", Value = "Center" });
            RetValue.Add(new DDLModel() { Text = "Right", Value = "Right" });
            return RetValue;
        }

        //To Get List of Barcode's QRCode Model
        public IEnumerable<DDLModel> GetBC_QRCodeModelDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "Model1", Value = "Model1" });
            RetValue.Add(new DDLModel() { Text = "Model2", Value = "Model2" });
            return RetValue;
        }

        //To Get List of Barcode's QRCode ErrorLevel
        public IEnumerable<DDLModel> GetBC_QRCodeErrorLevelDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "Low", Value = "Low" });
            RetValue.Add(new DDLModel() { Text = "Medium", Value = "Medium" });
            RetValue.Add(new DDLModel() { Text = "Quality", Value = "Quality" });
            RetValue.Add(new DDLModel() { Text = "High", Value = "High" });
            return RetValue;
        }

        //To Get List of Barcode's QRCode Mask
        public IEnumerable<DDLModel> GetBC_QRCodeMaskDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "Auto", Value = "Auto" });
            RetValue.Add(new DDLModel() { Text = "Mask000", Value = "Mask000" });
            RetValue.Add(new DDLModel() { Text = "Mask001", Value = "Mask001" });
            RetValue.Add(new DDLModel() { Text = "Mask010", Value = "Mask010" });
            RetValue.Add(new DDLModel() { Text = "Mask011", Value = "Mask011" });
            RetValue.Add(new DDLModel() { Text = "Mask100", Value = "Mask100" });
            RetValue.Add(new DDLModel() { Text = "Mask101", Value = "Mask101" });
            RetValue.Add(new DDLModel() { Text = "Mask110", Value = "Mask110" });
            RetValue.Add(new DDLModel() { Text = "Mask111", Value = "Mask111" });
            return RetValue;
        }

        //To Get List of Barcode's PDF417 Type
        public IEnumerable<DDLModel> GetBC_PDF417TypeDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "Normal", Value = "Normal" });
            RetValue.Add(new DDLModel() { Text = "Simple", Value = "Simple" });
            return RetValue;
        }

        //To Get List of Barcode's MicroPDF417 Compaction
        public IEnumerable<DDLModel> GetBC_MicroPDF417CompactionDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "Auto", Value = "Auto" });
            RetValue.Add(new DDLModel() { Text = "TextCompactionMode", Value = "TextCompactionMode" });
            RetValue.Add(new DDLModel() { Text = "NumericCompactionMode", Value = "NumericCompactionMode" });
            RetValue.Add(new DDLModel() { Text = "ByteCompactionMode", Value = "ByteCompactionMode" });            
            return RetValue;
        }

        //To Get List of Barcode's MicroPDF417 Version
        public IEnumerable<DDLModel> GetBC_MicroPDF417VersionDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "ColumnPriorAuto", Value = "ColumnPriorAuto" });
            RetValue.Add(new DDLModel() { Text = "RowPriorAuto", Value = "RowPriorAuto" });
            RetValue.Add(new DDLModel() { Text = "Version1X11", Value = "Version1X11" });
            RetValue.Add(new DDLModel() { Text = "Version1X14", Value = "Version1X14" });
            RetValue.Add(new DDLModel() { Text = "Version1X17", Value = "Version1X17" });
            RetValue.Add(new DDLModel() { Text = "Version1X20", Value = "Version1X20" });
            RetValue.Add(new DDLModel() { Text = "Version1X24", Value = "Version1X24" });
            RetValue.Add(new DDLModel() { Text = "Version1X28", Value = "Version1X28" });
            RetValue.Add(new DDLModel() { Text = "Version2X8", Value = "Version2X8" });
            RetValue.Add(new DDLModel() { Text = "Version2X14", Value = "Version2X14" });
            RetValue.Add(new DDLModel() { Text = "Version2X17", Value = "Version2X17" });
            RetValue.Add(new DDLModel() { Text = "Version2X20", Value = "Version2X20" });
            RetValue.Add(new DDLModel() { Text = "Version2X23", Value = "Version2X23" });
            RetValue.Add(new DDLModel() { Text = "Version2X26", Value = "Version2X26" });
            RetValue.Add(new DDLModel() { Text = "Version3X6", Value = "Version3X6" });
            RetValue.Add(new DDLModel() { Text = "Version3X8", Value = "Version3X8" });
            RetValue.Add(new DDLModel() { Text = "Version3X10", Value = "Version3X10" });
            RetValue.Add(new DDLModel() { Text = "Version3X12", Value = "Version3X12" });
            RetValue.Add(new DDLModel() { Text = "Version3X15", Value = "Version3X15" });
            RetValue.Add(new DDLModel() { Text = "Version3X20", Value = "Version3X20" });
            RetValue.Add(new DDLModel() { Text = "Version3X26", Value = "Version3X26" });
            RetValue.Add(new DDLModel() { Text = "Version3X32", Value = "Version3X32" });
            RetValue.Add(new DDLModel() { Text = "Version3X38", Value = "Version3X38" });
            RetValue.Add(new DDLModel() { Text = "Version3X44", Value = "Version3X44" });
            RetValue.Add(new DDLModel() { Text = "Version4X4", Value = "Version4X4" });
            RetValue.Add(new DDLModel() { Text = "Version4X6", Value = "Version4X6" });
            RetValue.Add(new DDLModel() { Text = "Version4X8", Value = "Version4X8" });
            RetValue.Add(new DDLModel() { Text = "Version4X10", Value = "Version4X10" });
            RetValue.Add(new DDLModel() { Text = "Version4X12", Value = "Version4X12" });
            RetValue.Add(new DDLModel() { Text = "Version4X15", Value = "Version4X15" });
            RetValue.Add(new DDLModel() { Text = "Version4X20", Value = "Version4X20" });
            RetValue.Add(new DDLModel() { Text = "Version4X26", Value = "Version4X26" });
            RetValue.Add(new DDLModel() { Text = "Version4X32", Value = "Version4X32" });
            RetValue.Add(new DDLModel() { Text = "Version4X38", Value = "Version4X38" });
            RetValue.Add(new DDLModel() { Text = "Version4X44", Value = "Version4X44" });            
            return RetValue;
        }

        //To Get List of Barcode's DataMatrix EccMode
        public IEnumerable<DDLModel> GetBC_DataMatrixEccModeDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "ECC000", Value = "ECC000" });
            RetValue.Add(new DDLModel() { Text = "ECC050", Value = "ECC050" });
            RetValue.Add(new DDLModel() { Text = "ECC080", Value = "ECC080" });
            RetValue.Add(new DDLModel() { Text = "ECC100", Value = "ECC100" });
            RetValue.Add(new DDLModel() { Text = "ECC140", Value = "ECC140" });
            RetValue.Add(new DDLModel() { Text = "ECC200", Value = "ECC200" });
            return RetValue;
        }

        //To Get List of Barcode's DataMatrix Ecc200SymbolSize
        public IEnumerable<DDLModel> GetBC_DataMatrixEcc200SymbolSizeDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "SquareAuto", Value = "SquareAuto" });
            RetValue.Add(new DDLModel() { Text = "RectangularAuto", Value = "RectangularAuto" });
            RetValue.Add(new DDLModel() { Text = "Square10", Value = "Square10" });
            RetValue.Add(new DDLModel() { Text = "Square12", Value = "Square12" });
            RetValue.Add(new DDLModel() { Text = "Square14", Value = "Square14" });
            RetValue.Add(new DDLModel() { Text = "Square16", Value = "Square16" });
            RetValue.Add(new DDLModel() { Text = "Square18", Value = "Square18" });
            RetValue.Add(new DDLModel() { Text = "Square20", Value = "Square20" });
            RetValue.Add(new DDLModel() { Text = "Square22", Value = "Square22" });
            RetValue.Add(new DDLModel() { Text = "Square24", Value = "Square24" });
            RetValue.Add(new DDLModel() { Text = "Square26", Value = "Square26" });
            RetValue.Add(new DDLModel() { Text = "Square32", Value = "Square32" });
            RetValue.Add(new DDLModel() { Text = "Square36", Value = "Square36" });
            RetValue.Add(new DDLModel() { Text = "Square40", Value = "Square40" });
            RetValue.Add(new DDLModel() { Text = "Square44", Value = "Square44" });
            RetValue.Add(new DDLModel() { Text = "Square48", Value = "Square48" });
            RetValue.Add(new DDLModel() { Text = "Square52", Value = "Square52" });
            RetValue.Add(new DDLModel() { Text = "Square64", Value = "Square64" });
            RetValue.Add(new DDLModel() { Text = "Square72", Value = "Square72" });
            RetValue.Add(new DDLModel() { Text = "Square80", Value = "Square80" });
            RetValue.Add(new DDLModel() { Text = "Square88", Value = "Square88" });
            RetValue.Add(new DDLModel() { Text = "Square96", Value = "Square96" });
            RetValue.Add(new DDLModel() { Text = "Square104", Value = "Square104" });
            RetValue.Add(new DDLModel() { Text = "Square120", Value = "Square120" });
            RetValue.Add(new DDLModel() { Text = "Square132", Value = "Square132" });
            RetValue.Add(new DDLModel() { Text = "Square144", Value = "Square144" });
            RetValue.Add(new DDLModel() { Text = "Rectangular8x18", Value = "Rectangular8x18" });
            RetValue.Add(new DDLModel() { Text = "Rectangular8x32", Value = "Rectangular8x32" });
            RetValue.Add(new DDLModel() { Text = "Rectangular12x26", Value = "Rectangular12x26" });
            RetValue.Add(new DDLModel() { Text = "Rectangular12x36", Value = "Rectangular12x36" });
            RetValue.Add(new DDLModel() { Text = "Rectangular16x36", Value = "Rectangular16x36" });
            RetValue.Add(new DDLModel() { Text = "Rectangular16x48", Value = "Rectangular16x48" });
            return RetValue;
        }

        //To Get List of Barcode's DataMatrix Ecc200EncodingMode
        public IEnumerable<DDLModel> GetBC_DataMatrixEcc200EncodingModeDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "Auto", Value = "Auto" });
            RetValue.Add(new DDLModel() { Text = "ASCII", Value = "ASCII" });
            RetValue.Add(new DDLModel() { Text = "C40", Value = "C40" });
            RetValue.Add(new DDLModel() { Text = "Text", Value = "Text" });
            RetValue.Add(new DDLModel() { Text = "X12", Value = "X12" });
            RetValue.Add(new DDLModel() { Text = "EDIFACT", Value = "EDIFACT" });
            RetValue.Add(new DDLModel() { Text = "Base256", Value = "Base256" });
            return RetValue;
        }


        //To Get List of Barcode's DataMatrix Ecc000_140SymbolSize
        public IEnumerable<DDLModel> GetBC_DataMatrixEcc000_140SymbolSizeDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "Auto", Value = "Auto" });
            RetValue.Add(new DDLModel() { Text = "Square9", Value = "Square9" });
            RetValue.Add(new DDLModel() { Text = "Square11", Value = "Square11" });
            RetValue.Add(new DDLModel() { Text = "Square13", Value = "Square13" });
            RetValue.Add(new DDLModel() { Text = "Square15", Value = "Square15" });
            RetValue.Add(new DDLModel() { Text = "Square17", Value = "Square17" });
            RetValue.Add(new DDLModel() { Text = "Square19", Value = "Square19" });
            RetValue.Add(new DDLModel() { Text = "Square21", Value = "Square21" });
            RetValue.Add(new DDLModel() { Text = "Square23", Value = "Square23" });
            RetValue.Add(new DDLModel() { Text = "Square25", Value = "Square25" });
            RetValue.Add(new DDLModel() { Text = "Square27", Value = "Square27" });
            RetValue.Add(new DDLModel() { Text = "Square29", Value = "Square29" });
            RetValue.Add(new DDLModel() { Text = "Square31", Value = "Square31" });
            RetValue.Add(new DDLModel() { Text = "Square33", Value = "Square33" });
            RetValue.Add(new DDLModel() { Text = "Square35", Value = "Square35" });
            RetValue.Add(new DDLModel() { Text = "Square37", Value = "Square37" });
            RetValue.Add(new DDLModel() { Text = "Square39", Value = "Square39" });
            RetValue.Add(new DDLModel() { Text = "Square41", Value = "Square41" });
            RetValue.Add(new DDLModel() { Text = "Square43", Value = "Square43" });
            RetValue.Add(new DDLModel() { Text = "Square45", Value = "Square45" });
            RetValue.Add(new DDLModel() { Text = "Square47", Value = "Square47" });
            RetValue.Add(new DDLModel() { Text = "Square49", Value = "Square49" });
            return RetValue;
        }

        //To Get List of Excel Types
        public IEnumerable<DDLModel> GetExcel_TypeDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "json", Value = "json" });
            RetValue.Add(new DDLModel() { Text = "xlsx", Value = "xlsx" });
            RetValue.Add(new DDLModel() { Text = "xls", Value = "xls" });
            RetValue.Add(new DDLModel() { Text = "csv", Value = "csv" });
            RetValue.Add(new DDLModel() { Text = "xml", Value = "xml" });
            return RetValue;
        }

        //To Get List of Excel DataName
        public IEnumerable<DDLModel> GetExcel_DataNameDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "Select a DataSource", Value = "" });
            RetValue.Add(new DDLModel() { Text = "Sales", Value = "Sales" });
            RetValue.Add(new DDLModel() { Text = "Orders", Value = "Orders" });
            RetValue.Add(new DDLModel() { Text = "Products", Value = "Nwind/Products" });
            RetValue.Add(new DDLModel() { Text = "Shippers", Value = "Nwind/Shippers" });
            return RetValue;
        }

        //To Get List of Excel DataFileName
        public IEnumerable<DDLModel> GetExcel_DataFileNameDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "Select a XML file name", Value = "" });
            RetValue.Add(new DDLModel() { Text = "10rowsdata.xml", Value = "ExcelRoot/10rowsdata.xml" });
            RetValue.Add(new DDLModel() { Text = "OneDrive/Documents/10rowsdata.xml", Value = "OneDrive/Documents/10rowsdata.xml" });
            return RetValue;
        }

        //To Get List of Excel DataFileName
        public IEnumerable<DDLModel> GetExcel_WorkBookFileNameDDList()
        {
            List<DDLModel> RetValue = new List<DDLModel>();
            RetValue.Add(new DDLModel() { Text = "Select a Excel file name", Value = "" });
            RetValue.Add(new DDLModel() { Text = "align.xls", Value = "ExcelRoot/align.xls" });
            RetValue.Add(new DDLModel() { Text = "FlexGrid.xlsx", Value = "ExcelRoot/FlexGrid.xlsx" });
            RetValue.Add(new DDLModel() { Text = "GAS.xls", Value = "ExcelRoot/GAS.xls" });
            RetValue.Add(new DDLModel() { Text = "Grouping.xlsx", Value = "ExcelRoot/Grouping.xlsx" });
            RetValue.Add(new DDLModel() { Text = "Hierarchical.xlsx", Value = "ExcelRoot/Hierarchical.xlsx" });
            RetValue.Add(new DDLModel() { Text = "Houston.xlsx", Value = "ExcelRoot/Houston.xlsx" });
            RetValue.Add(new DDLModel() { Text = "Palette.xls", Value = "ExcelRoot/Palette.xls" });
            RetValue.Add(new DDLModel() { Text = "ExcelWithFormula.xls", Value = "ExcelRoot/ExcelWithFormula.xls" });
            RetValue.Add(new DDLModel() { Text = "OneDrive/Documents/align.xls", Value = "OneDrive/Documents/align.xls" });
            RetValue.Add(new DDLModel() { Text = "OneDrive/Documents/FlexGrid.xlsx", Value = "OneDrive/Documents/FlexGrid.xlsx" });
            RetValue.Add(new DDLModel() { Text = "OneDrive/Documents/GAS.xls", Value = "OneDrive/Documents/GAS.xls" });
            RetValue.Add(new DDLModel() { Text = "OneDrive/Documents/Grouping.xlsx", Value = "OneDrive/Documents/Grouping.xlsx" });
            RetValue.Add(new DDLModel() { Text = "OneDrive/Documents/Hierarchical.xlsx", Value = "OneDrive/Documents/Hierarchical.xlsx" });
            RetValue.Add(new DDLModel() { Text = "OneDrive/Documents/Houston.xlsx", Value = "OneDrive/Documents/Houston.xlsx" });
            RetValue.Add(new DDLModel() { Text = "OneDrive/Documents/Palette.xls", Value = "OneDrive/Documents/Palette.xls" });
            RetValue.Add(new DDLModel() { Text = "OneDrive/Documents/ExcelWithFormula.xls", Value = "OneDrive/Documents/ExcelWithFormula.xls" });
            return RetValue;
        }

        

    }
}