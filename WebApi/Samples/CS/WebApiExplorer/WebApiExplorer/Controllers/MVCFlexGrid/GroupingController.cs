﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApiExplorer.Models;

namespace WebApiExplorer.Controllers
{
    public partial class MVCFlexGridController : Controller
    {
        private readonly GridExportImportOptions _flexGridGroupModel = new GridExportImportOptions
        {
            NeedExport = true,
            NeedImport = false,
            OnlyCurrentPage = false,
            IncludeColumnHeaders = true
        };

        public ActionResult Grouping()
        {
            ViewBag.DemoSettingsModel = new ClientSettingsModel
            {
                Settings = new Dictionary<string, object[]>
                {
                    {"GroupBy", new object[]{"ShipCountry", "ShipCity", "ShipCountry and ShipCity", "Freight", "ShippedDate","None"}}
                }
            };
            ViewBag.Options = _flexGridGroupModel;
            var nwind = new C1NWindEntities();
            return View(nwind.Orders);
        }

    }
}
