﻿using C1.Web.Mvc.Chart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebApiExplorer.Models;

namespace WebApiExplorer.Controllers
{
    public partial class MVCFlexChartController : Controller
    {
        private List<PopulationByCountry> _populationByCountry = PopulationByCountry.GetData();
        public ActionResult ErrorBar()
        {
            ViewBag.Options = _flexChartModel;
            var width = new object[] { 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1 };
            var boolValues = new object[] { false, true };
            ViewBag.DemoSettingsModel = new ClientSettingsModel
            {
                Settings = new Dictionary<string, object[]>
                {
                    {"Direction", Enum.GetValues(typeof(ErrorBarDirection)).Cast<object>().ToArray()},
                    {"ErrorAmount", Enum.GetValues(typeof(ErrorAmount)).Cast<object>().ToArray()},
                    {"Value", new object[]{50, 100, 150, 200}},
                    {"EndStyle", Enum.GetValues(typeof(ErrorBarEndStyle)).Cast<object>().ToArray()}
                }
            };

            return View(_populationByCountry);
        }
    }
}
