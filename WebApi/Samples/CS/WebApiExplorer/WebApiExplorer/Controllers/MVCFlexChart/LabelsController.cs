﻿using C1.Web.Mvc.Chart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApiExplorer.Models;

namespace WebApiExplorer.Controllers
{
    public partial class MVCFlexChartController : Controller
    {
        private static IDictionary<string, object[]> CreateLabelSettings()
        {
            var settings = new Dictionary<string, object[]>
            {
                {"ChartType", new object[]{"Column", "Bar", "Scatter", "Line", "LineSymbols", "Area", "Spline", "SplineSymbols", "SplineArea"}},
                {"DataLabel.Position", new object[]{LabelPosition.Top, LabelPosition.Right, LabelPosition.Bottom, LabelPosition.Left, LabelPosition.None}},
                {"DataLabel.Border", new object[]{false, true}},
            };

            return settings;
        }

        public ActionResult Labels()
        {
            var model = new ClientSettingsModel
            {
                Settings = CreateLabelSettings()
            };
            ViewBag.Options = _flexChartModel;
            return View(model);
        }
    }
}
