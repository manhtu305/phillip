﻿using C1.Web.Api.Storage;
using System.IO;
using System.Net;
using System;

namespace WebApi
{
    internal class HTTPStorage : IFileStorage
    {
        public string Name { get; private set; }
        public string Path { get; set; }

        public HTTPStorage(string name, string path)
        {
            Name = name;
            Path = path;
        }

        public bool Exists
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public bool ReadOnly
        {
            get
            {
                return true;
            }
        }

        public Stream Read()
        {
            var webRequest = WebRequest.Create(Path) as HttpWebRequest;
            webRequest.KeepAlive = false;
            using (var webResponse = webRequest.GetResponse())
            using (var responseStream = webResponse.GetResponseStream())
            using (var reader = new BinaryReader(responseStream))
            {
                var buffer = reader.ReadBytes((int)webResponse.ContentLength);
                return new MemoryStream(buffer);
            }
        }

        public void Write(Stream stream)
        {
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }
    }
}
