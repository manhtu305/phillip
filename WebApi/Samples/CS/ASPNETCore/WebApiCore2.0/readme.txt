﻿AspNetCore WebApiCore2.0 Sample.
-------------------------------------------------------------------
The WebApiCore2.0 sample demonstrates how to make a webapi server to provide all services that ComponentOne WebApi products supports.

The sample is a service application which provides all these services:
- Excel: generate/import/export etc.
- Barcode: generate barcode.
- DataEngine: analyze the raw data to show the aggregate result, show detailed row data etc.
- Visitor: Gathering visitor information like ipaddress, geolocation, language,...

In the sample, we used visitor api which require a local database included ip2location database.
Please add a new database to your local sql server and provide a valid connection string inside appsettings.json file.
You can find the database and setup guide from https://lite.ip2location.com/database/ip-country-region-city-latitude-longitude-zipcode-timezone
- database in use: db11
- server type: sql
- ip version: ipv4


