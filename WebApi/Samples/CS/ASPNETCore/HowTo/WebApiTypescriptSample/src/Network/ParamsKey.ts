export class ParamsKey {
    public static STATUS: string = "status";
    public static MESSAGE: string = "message";
    public static CONTENT: string = "content";

}