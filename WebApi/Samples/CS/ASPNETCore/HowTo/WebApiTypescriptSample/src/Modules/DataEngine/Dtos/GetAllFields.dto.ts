import { BaseDto } from "../../../Base/Base.Dto";

export class GetAllFieldsDto extends BaseDto {

    /**The data source to get the fields information. */
    public DataSource: string;

}