﻿using System;
using System.Net.Http.Headers;
using System.Web.Http.Filters;

namespace C1.Web.Api
{
    [SmartAssembly.Attributes.DoNotObfuscateType]
    [AttributeUsage(AttributeTargets.Method)]
    internal class CacheControlAttribute : ActionFilterAttribute
    {
        public int MaxAge { get; set; }
        public bool NoCache { get; set; }
        public bool NoStore { get; set; }

        public CacheControlAttribute()
        {
            MaxAge = 3600;
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);

            if (actionExecutedContext.Response != null)
            {
                var cache = actionExecutedContext.Response.Headers.CacheControl;
                if (cache == null)
                {
                    actionExecutedContext.Response.Headers.CacheControl = cache = new CacheControlHeaderValue();
                }

                if (NoStore || NoCache)
                {
                    cache.NoStore = NoStore;
                    cache.NoCache = NoCache;
                }
                else
                {
                    cache.MaxAge = new System.TimeSpan(MaxAge);
                }
            }
        }
    }
}
