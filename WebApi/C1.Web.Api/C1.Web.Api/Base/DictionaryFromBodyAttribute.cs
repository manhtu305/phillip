﻿using System;
using System.Web.Http.ModelBinding;

namespace C1.Web.Api
{
    /// <summary>
    /// Defines a <see cref="ModelBinderAttribute"/> object to deserialize the values from form.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Parameter, AllowMultiple = false, Inherited = true)]
    public class FromFormAttribute : ModelBinderAttribute
    {
        /// <summary>
        /// Initializes a new instance of <see cref="FromFormAttribute"/> class.
        /// </summary>
        public FromFormAttribute()
        {
            BinderType = typeof (DictionaryModelBinder);
        }
    }
}
