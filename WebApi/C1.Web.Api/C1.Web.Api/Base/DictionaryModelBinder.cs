﻿using System.Linq;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;

namespace C1.Web.Api
{
    internal class DictionaryModelBinder : IModelBinder
    {
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var task = actionContext.Request.Content.ReadAsFormDataAsync();
            task.Wait();

            var formData = task.Result;
            var model = formData == null? null : formData.AllKeys.ToDictionary(k => k, formData.GetValues);
            bindingContext.Model = model;
            return true;
        }
    }
}
