﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using System.Web.Http.ValueProviders;
using System.Web.Http.ValueProviders.Providers;

namespace C1.Web.Api
{
    /// <summary>
    /// Defines a <see cref="ModelBinderAttribute"/> class to deserialize the values from the query.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Parameter, AllowMultiple = false, Inherited = true)]
    public class FromUriExAttribute : ModelBinderAttribute
    {
        /// <summary>
        /// Gets the value providers that will be fed to the model binder.
        /// </summary>
        /// <param name="configuration">The <see cref="HttpConfiguration"/> configuration object.</param>
        /// <returns>A collection of <see cref="ValueProviderFactory"/> instances.</returns>
        public override IEnumerable<ValueProviderFactory> GetValueProviderFactories(HttpConfiguration configuration)
        {
            if (configuration == null)
                throw new ArgumentNullException("configuration");

            var services = configuration.Services;
            var factoryEx = services.GetValueProviderFactories()
                .OfType<UriValueProviderFactoryEx>().FirstOrDefault();

            if (factoryEx == null)
            {
                factoryEx = RegisterUriValueProviderFactoryEx(services);
            }

            return factoryEx == null ? new ValueProviderFactory[] {} : new ValueProviderFactory[] {factoryEx};
        }

        private static UriValueProviderFactoryEx RegisterUriValueProviderFactoryEx(ServicesContainer services)
        {
            UriValueProviderFactoryEx factoryEx;
            lock (services)
            {
                factoryEx = services.GetValueProviderFactories()
                    .OfType<UriValueProviderFactoryEx>().FirstOrDefault();
                if (factoryEx == null)
                {
                    var factory = services.GetValueProviderFactories()
                        .OfType<IUriValueProviderFactory>().FirstOrDefault() as ValueProviderFactory;
                    if (factory != null)
                    {
                        factoryEx = new UriValueProviderFactoryEx(factory);
                        services.Add(typeof (ValueProviderFactory), factoryEx);
                    }
                }
            }

            return factoryEx;
        }

        [SmartAssembly.Attributes.DoNotObfuscateType]
        internal class UriValueProviderFactoryEx : ValueProviderFactory
        {
            private readonly ValueProviderFactory _valueProviderFactory;
            private const string ID = "{4324CA3A-1767-474B-9228-81CA2A6C4CFC}";

            public UriValueProviderFactoryEx(ValueProviderFactory valueProviderFactory)
            {
                _valueProviderFactory = valueProviderFactory;
            }

            public override IValueProvider GetValueProvider(HttpActionContext actionContext)
            {
                if (actionContext == null)
                    throw new ArgumentNullException("actionContext");

                var properties = actionContext.Request.Properties;
                ValueProviderEx stringValueProvider;
                if (!TryGetValueFromDictionary(properties, ID, out stringValueProvider))
                {
                    stringValueProvider = new ValueProviderEx(_valueProviderFactory.GetValueProvider(actionContext));
                    properties[ID] = stringValueProvider;
                }

                return stringValueProvider;
            }

            private static bool TryGetValueFromDictionary<T>(IDictionary<string, object> collection, string key,
                out T value)
            {
                object obj;
                if (collection.TryGetValue(key, out obj) && obj is T)
                {
                    value = (T) obj;
                    return true;
                }

                value = default (T);
                return false;
            }
        }

        [SmartAssembly.Attributes.DoNotObfuscateType]
        internal class ValueProviderEx : IValueProvider
        {
            private readonly IValueProvider _valueProvider;
            private readonly NameValuePairsValueProvider _nameValuePairsValueProvider;

            private const string DicNameGroupName = "name";
            private const string DicItemIndexGroupName = "index";
            private const string DicItemFieldGroupName = "field";
            private const string DicPrefixPattern =
                @"(?<" + DicNameGroupName + @">.*)\[(?<" + DicItemIndexGroupName + @">\d*)\]((\.(?<" +
                DicItemFieldGroupName + @">key|value))|)";
            private readonly static Regex DictionaryPrefixRegex = new Regex(DicPrefixPattern, RegexOptions.IgnoreCase);

            public ValueProviderEx(IValueProvider valueProvider)
            {
                _valueProvider = valueProvider;
                _nameValuePairsValueProvider = _valueProvider as NameValuePairsValueProvider;
            }

            public bool ContainsPrefix(string prefix)
            {
                if (_valueProvider.ContainsPrefix(prefix))
                {
                    return true;
                }

                return ContainsDictionaryPrefix(prefix);
            }

            private bool ContainsDictionaryPrefix(string prefix)
            {
                string name;
                int index;
                return TryGetDicItem(prefix, out name, out index);
            }

            private bool TryGetDicItem(string key, out string name, out int index)
            {
                DicField fieldName;
                ValueProviderResult value;
                return TryGetDicItem(key, out name, out index, true, out fieldName, out value);
            }

            private bool TryGetDicItem(string key, out string name, out int index,
                bool requiredValue, out DicField fieldName, out ValueProviderResult value)
            {
                name = null;
                index = -1;
                fieldName = DicField.None;
                value = null;

                if (_nameValuePairsValueProvider == null)
                {
                    return false;
                }

                var match = DictionaryPrefixRegex.Matches(key).Cast<Match>().FirstOrDefault();
                if (match == null)
                {
                    return false;
                }

                if (!int.TryParse(match.Groups[DicItemIndexGroupName].Value, out index) || index < 0)
                {
                    return false;
                }

                name = match.Groups[DicNameGroupName].Value;
                var items = _nameValuePairsValueProvider.GetKeysFromPrefix(name);
                var count = items.Count;
                if (index >= count)
                {
                    return false;
                }

                if (!requiredValue)
                {
                    return true;
                }

                if (!Enum.TryParse(match.Groups[DicItemFieldGroupName].Value, true, out fieldName) || fieldName == DicField.None)
                {
                    return false;
                }

                var item = items.ElementAt(index);
                var itemKey = item.Key;
                if (fieldName == DicField.Key)
                {
                    value = new ValueProviderResult(itemKey,itemKey, CultureInfo.InvariantCulture);
                }
                else
                {
                    var queryName = string.IsNullOrEmpty(name) ? itemKey : string.Format("{0}.{1}", name, itemKey);
                    value = GetValue(queryName);
                }

                return true;
            }

            public ValueProviderResult GetValue(string key)
            {
                var result = _valueProvider.GetValue(key);
                ValueProviderResult dicItemFieldResult;
                if (result == null && TryGetDicItemFieldValue(key, out dicItemFieldResult))
                {
                    return dicItemFieldResult;
                }

                return result;
            }

            private bool TryGetDicItemFieldValue(string key, out ValueProviderResult result)
            {
                string name;
                int index;
                DicField fieldName;
                return TryGetDicItem(key, out name, out index, true, out fieldName, out result);
            }
        }

        [SmartAssembly.Attributes.DoNotObfuscateType]
        internal enum DicField
        {
            None,
            Key,
            Value
        }
    }
}
