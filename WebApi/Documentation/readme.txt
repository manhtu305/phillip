===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20202.267/4.5.20202.267)		Drop Date: 08/06/2020
===================================================================================================
Samples
    [456104] 'LicenseException' error is displayed when using some report services although valid license key is activated in license activation.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20202.266/4.5.20202.266)		Drop Date: 07/28/2020
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20202.265/4.5.20202.265)		Drop Date: 07/23/2020
===================================================================================================
All
    Upgrade to use C1.DataEngine 154/155.

Samples
    [444975] GCDTLicenses.xml file is shown with error sign in JP core CloudFileExplorer.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20202.264/4.5.20202.264)		Drop Date: 07/14/2020
===================================================================================================

Visitors
    [445259] 'Visitor ID' values are incorrectly displayed at some scenario.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20202.263/4.5.20202.263)		Drop Date: 07/13/2020
===================================================================================================
All
    Upgrade to use C1.DataEngine 153.

Licenses
    [444310] 'A valid license can't generate for xxx' error is observed in Viewer control when creating NonCore project with FlexViewer, Olap, DataEngine if no/invalid key in GCLM and valid key is activated in LicenseActivation.
    [444725] 'Internal server error' is occurred for all api services by unchecking 'Self host application' checkbox when GCLM has SE license key added.

Visitors
    [445042]  Any visitor data can't show and 'Visitor:304 Uncaught TypeError: Cannot read property 'getDataAsync' of undefined at Visitor:304' error is observed at Visitor service.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20202.262/4.5.20202.262)		Drop Date: 07/10/2020
===================================================================================================
All
    Upgrade Winforms dlls from 20202.428 to 20202.438.

ProjectTemplates
    [444623] Request to remove core VSIX that support to VS 15 at both EN/JP.

WebAPI
    [404635][Cloud Service] Corrupted file gets downloaded from CloudStorage .

Licenses
    [444729] 'FileNotFoundException: Could not load file or assembly 'GrapeCity.Documents.xxxx' error are observed after running WebAPI Core project with NetCore 2.2 version.
    [444732] License error is occurred for all services at WebAPI Core project with NetCore 3.0 version although SE Trial key or SE key is activated in GCLM.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20202.261/4.5.20202.261)		Drop Date: 07/07/2020
===================================================================================================
All
    [425665] Update JP xmlDocument.

WebAPI
    [443539] 'TypeError: underfined is not a constructor (evaluating 'e.beginRender()')' error is observed after exporting image of FlexChart and FlexRadar MVC and Wijmo controls.

ProjectTemplate
    [441671] 'Unable to find version 4.5.20202.428 of packages C1.DataEngine' error is observed when creating the MVC NonCore project by checking with C1 WebAPI DataEngine Library.

Licenses
    [436651] License Error occur when create non-core MVC template with WebApi using certain scenario although Sutdio Enterprise key is activated in GCLM.
    [443589] Evaluation message is displayed when Trial License key of New License(GCLM) is activated if C1 AspNet MVC (non-core) project is created with WebAPI DataEngine service.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20202.260/4.5.20202.260)		Drop Date: 07/02/2020
===================================================================================================
Licenses
    [436651] License Error occur when create non-core MVC template with WebApi using certain scenario although Sutdio Enterprise key is activated in GCLM.
    [443589] Evaluation message is displayed when Trial License key of New License(GCLM) is activated if C1 AspNet MVC (non-core) project is created with WebAPI DataEngine service.
    [443384] Different behavior are observed between Trial license is expired and removed the Trial license when system date is changed to + 30 days .

ProjectTemplate
    [442337] 'Microsoft.AnalysisServices.AdomdClient' dlls is included twice at 'Samples\VB\HowTo\OLAP\Olap101' of JP and conflict version is observed at WebAPI noncore project (CS/VB).
    [443556] Unsupported service checkboxes are included at 'Project Settings' of WebApi Core project by choosing NetCore2.2 or 3.0 version.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20202.259/4.5.20202.259)		Drop Date: 06/27/2020
===================================================================================================
All
    [425660] [PrepairCC] JP localization.
    [441839] Modified Date and Size are not displayed correctly in 'LocalStorage'.
	
Licenses
    [442012] License error do not occurs and it works as license although license.licx file or '331cf6cd-b73c-429f-ba79-fa2f85eebd68.gclicx' file or both are deleted in project.
    [441598] Unlike with EN, 'Invalid key activated' message is observed when creating MVC or WebAPI core project with GCLM to NoKey at JP environment.
    [434141] Trial License key of New License(GCLM) do not work as expected.

Sample
    [442488] Remove support netcore1.0.
    [442607] Correct reference of C1.DataEngine.Ja.

ProjectTemplates
    [442356] Remove support netcore1.0.
    [442105] WinForm nuget cannot be restored and error is displayed when create and build the project using WebAPI Core JP template.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20202.258/4.5.20202.258)		Drop Date: 06/23/2020
===================================================================================================
ProjectTemplates
    [441670] All vsix files under 'ProjectTemplates' folder of 4.0 build version can't install and 'Install Failed' error is displayed.

BarCode
    [438478] Generated barcode result is not the same in trial state when using  NetCore 2.1 or lower version and NetCore 2.2 or above version service link.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20202.257/4.5.20202.257)		Drop Date: 06/17/2020
===================================================================================================
All
    Upgrade Winforms dlls from 20201.424 to 20202.428.

License
    [434484] Unlike the ENG environment, GCLM dialog box doesn't popup when build the project although C1 nugets are added into project in JP environment.

Sample
    [440614] Unlike with CS\ExcelExportWithLargeData sample, 'Project_Readme.html' is included at 'VB\ExcelExportWithLargeData' file although this html file don't use at sample.

Pdf
    [438346] Change the PDF returned from the WebAPI dynamically.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20202.256/4.5.20202.256)		Drop Date: 06/07/2020
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20202.255/4.5.20202.255)		Drop Date: 06/07/2020
===================================================================================================
Barcode
    [438478] Generated barcode result is not the same in trial state when using NetCore 2.1 or lower version and NetCore 2.2 or above version service link.
    [422911] Console error is occurred when generating barcode with blank text by using .Net Core 2.2 or above version.

WebAPI
    [433091] Console error is occurred when exporting Excel file with any format by checking Excel Service only.
    [438953] [Regression] '500: Internal Server Error You don't have permission to access this folder.
Samples
    [438736] [WebApiJavascriptSample] WebApi Demo service link is incorrectly displayed at 'CS\ASPNETCore\HowTo\WebApiJavascriptSample'.
    [438517] Request to add description at 'Visitor\UniqueVisitorId' tag of 'CS\ASPNETCore\WebApiExplorer'sample.
    [403579] Url link is not working properly and 404 error is found in sample.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20202.254/4.5.20202.254)		Drop Date: 05/26/2020
===================================================================================================
Visitor
    [426527] Do implement for BrowserFingerprinting.
    [426522] Try to use canvas to improve accurate of the result.
    [431005] Should add a page or demo about BrowserFingerPrinting.
    [429727] Improve BrowserFingerprinting with some server side detech/analyze.
    [431855] Web browser name is incorrectly displayed when running at Edge browser.
    [432784] 'referrer_info : host, path, protocol, port' value are incorrectly displayed at IE.

ProjectTemplates
    [421355] 'The referenced component 'C1.DataEngine$dataengineversion$' could not be found.' warning message is observed after building the non-core webapi project.

    [420296] Error messages are observed when building the non-core webapi project.
    [418725] WinForm dll '4.5.2' are included instead of '4.0' when create the projects using Non Core templates(MVC/WebApi) by choosing '.NET Framework 4.5/4.5.1'.


Barcode
    [422911] Console error is occurred when generating barcode with blank text by using .Net Core 2.2 or above version.

WebAPI
    [415485] Excel file cannot generate and 'HTTP ERROR 500' is occurred if 'SalesTemplate.xlsx' is used.
    [434820] Console error is occurred when generating excel with 'Json' or 'Xml' file type if file name is blank.
    [420366] Imported file data can't observed when importing file which is exported from net core 2.2 or above version service link.

Report
    [430525] Export setting do not work properly when it is exported to 'Microsoft Excel' format.

Samples
    [418632] 'CloudFileExplorer' sample cannot restore automatically and 'WebApi/MVC' dll are needed to add manually.
    [420584] Incorrect WebApi dll version is used in 'WebApi(Non Core JP)' sample.
    [417441] [CloudFileExplorer] Request to remove the values of 'OneDriveAccessToken' and 'DropBoxStorageAccessToken' keys in 'Web.config' file.
    [420445] 500 internal server error is occurred when generating excel with blank file name.
    [420421] WebApiExplorer Syntax error is occurred when using visitor service with core 3.0 at IE browser.
    [407536] JP localization.
    [414896] WebApiExplorer Can not open page when deploy this sample to IIS.
    [403579] [ExcelExportWithLargeData] Url link is not working properly and 404 error is found in sample.
    [406382] [WebApiExplorer] Inconsistent text is displayed in 'Data Source' drop down of 'DataEngine > Services > GET (GET/api/dataengine/{datasource}/fields)' in 'WebApiExplorer(Core)' sample.
    [421513] [ReportViewer][Query] Multiple issues are observed when exporting the report after unchecked the paged checkbox in some export setting in some flex reports.
    [433341] Console error displays when click on 'Export' button in 'Paging/Virtual Scrolling/Grouping/Remote Data Bind' pages of 'WebApiExplorer' samples.
    [435316] Unlike with the 'Conditional Styling' view page of WebApiExplorer (Non-Core).
    [431977] Excel file path is wrong in 'CS\ExcelToJSON\ExcelToJSON\Views\Home' page.

Pdf
    [415384] Unable to load modified PDF with the same name.

FileManager
    [418701][WebApi][CloudFileExplorer][Sample] 'Uncaught TypeError: Cannot read property....../Error: Unable to get property......' errors are occurred when click on 'MoveUp' and 'Search' buttons.

Wijmo5Mvc
    [351224] Chart controls occupy full width of DashboradLayout control in Firefox browser if no width and height are set in control and Tile.

Pdf
    [430441] Some special characters, space and umlaut characters cannot be searched using search panel of viewer control if flexreport/SSRS report and pdf is used.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.253/4.5.20201.253)		Drop Date: 05/13/2020
===================================================================================================
All
    Upgrade Winforms dlls from 20201.416 to 20201.424.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.252/4.5.20201.252)		Drop Date: 05/10/2020
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.251/4.5.20201.251)		Drop Date: 04/29/2020
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.250/4.5.20201.250)		Drop Date: 04/24/2020
===================================================================================================
Samples
    [431977] Excel file path is wrong in 'CS\ExcelToJSON\ExcelToJSON\Views\Home' page.

Pdf
    [430441] Some special characters, space and umlaut characters cannot be searched using search panel of viewer control if flexreport/SSRS report and pdf is used.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.249/4.5.20201.249)		Drop Date: 04/17/2020
===================================================================================================
Report
    [430441] Some special characters, space and umlaut characters (e.g. �) cannot be searched using search panel of viewer control if flexreport/SSRS report and pdf is used.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.248/4.5.20201.248)		Drop Date: 04/08/2020
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.247/4.5.20201.247)		Drop Date: 04/08/2020
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.246/4.5.20201.246)		Drop Date: 04/03/2020
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.245/4.5.20201.245)		Drop Date: 03/31/2020
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.244/4.5.20201.244)		Drop Date: 03/12/2020
===================================================================================================
ProjectTemplates
    [373811] 'Could not load file or assembly 'System.Web.Cors, Version=5.0.0.0

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.243/4.5.20201.243)		Drop Date: 03/12/2020
===================================================================================================
ProjectTemplates
    [373811] 'Could not load file or assembly 'System.Web.Cors, Version=5.0.0.0

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.242/4.5.20201.242)		Drop Date: 03/12/2020
===================================================================================================
Samples
    [424567] [WebApiExplorer] Add license info to sample to use IP2location database.

Visitor
    [423708] [License] Visitor API is working correctly as expected with every keys.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.241/4.5.20201.241)		Drop Date: 03/10/2020
===================================================================================================
FlexReport
    [424138] Evaluation dialog gets appeared when running the ReportViewer referring the reports within project.

Visitor
    [423708] [License] Visitor API is working correctly as expected with every keys. 

ProjectTemplate
    [421957] WF dlls can not be restored properly and error occur when create WebAPI 4.0/4.5 and MVC 4.5 project with WF dlls.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.240/4.5.20201.240)		Drop Date: 03/06/2020
===================================================================================================
Samples
    [423837] [WebApiExplorer] Update Visitor Description in the sample.

ProjectTemplate
    [423905] 'Unable to resolve type 'C1.Web.Api.Cloud.LicenseDetector, C1.Web.Api.Cloud' error is observed when checking Cloud service with other services in non-core project 4.0 dll version.
    [423967] WF errors are observed when creating WebApi Core/Non-core and AspNet MVC Core(.NetFramework) projects at JP.
    [421957] WF dlls can not be restored properly and error occur when create WebAPI 4.0/4.5 and MVC 4.5 project with WF dlls.
    [424092] License.licx file is missing in project when only cloud service is created with webapi non-core template.

Visitor
    [423708] [License] Visitor API is working correctly as expected with every keys. 
    [423240] [iPad] Web browser and (Mobile) Device information are incorrectly displayed when running at iPad's Chrome browser. 

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.239/4.5.20201.239)		Drop Date: 03/04/2020
===================================================================================================
All
    Upgrade Winforms dlls from 20193.398 to 20201.416.

Sample
    [422272] Some of the words in WebApi sample are needed to localize under 'GrapeCity Logo'.

ProjectTemplate
    [403793] Add project cloud to project template of noncore.
    [421957] WF dlls can not be restored properly and error occur when create WebAPI 4.0/4.5 and MVC 4.5 project with WF dlls.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.238/4.5.20201.238)		Drop Date: 02/27/2020
===================================================================================================
All
    [422165] Remove '-beta' of webAPI 3.0.

ProjectTemplates
    [421946] 'StorageProviderManager' does not contain a definition for 'AddAzureStorage'.xxxxx error is observed when creating project with NetCore2.2 version and 'Cloud Service' checkbox.

Sample
    [421691] Error is not displayed and 'Response Schema' is 'success: true' when list the file although DropBoxStorageAccessTocken key value is not added in 'Web.config' of service sample.
    [WebApiCore2.0][Regression] 500 internal server error is occurred at barcode when using WebApiCore2.0 sample service link.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.237/4.5.20201.237)		Drop Date: 02/27/2020
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20201.236/4.5.20201.236)		Drop Date: 02/20/2020
===================================================================================================
All
    Merge from Main to Dev for start 2020v1.
    [409572] Add Create Folder API to Cloud dll.
    [409573] Add Delete Folder API to Cloud dll.
    [409586] Add temperate Delete/Create folder for testing purpose for delete/create folder API of cloud dll.
    [415690] Add download feature to sample of FileManager.
    [415094] Create UI for move file API on sample.
    [415088] [API][Cloud] After move file using MoveFile API of cloud, new file is 0 byte.

Visitor
    [409585] Clean code of the prototype.
    [408623] Create first prototype for visitor api.
    [409592] Submit the prototype of the visitor api to TFS.
    [417364] Any data can't display at page and console error is occurred when using visitor service with core 3.0.
    [416025] Request to show error message '404 - File or directory not found.' instead of file downloading in WebApiWPFSample when using webapi service link which is not deployed yet.

Samples
    [412957] Add samples to WebApiCore2.0 and WebApiExplorer project.
    [412990][WebAPI] Verify using of Zlib library.
    [413486][CloudFileExplorer] Console error is occurred when clicking 'Delete' button of FileManager control sample project.
    [415213] Exported barcode name is inconsistently displayed when generating barcode with TypeScriptSample.
    [417584] Request to update as new features for 'AllowSorting' and 'SelectionMode' properties setting in FlexGrid sample page.
    [417043] CaptionAlignment values are incorrectly displayed at 'WebApiTypescriptSample' and 'WebApiConsoleSample'.
    [402679] [WebApiJavascriptSample][IE] Exported file name is incorrectly displayed when generating excel with number format file name.
    [414896] WebApiExplorer Can not open page when deploy this sample to IIS.
    [407536] JP localization.
    [414896] WebApiExplorer Can not open page when deploy this sample to IIS.

Pdf
    [415384] Unable to load modified PDF with the same name.

BarCode
    [414976] Width of barcode is very small when codeType is choose as 'None' if webapi core 2.2 and above is used as service.
    [414415] Console error is occurred when calling WebAPI Core 2.2 or above version service link more than 10 times.

ProjectTemplates
    [421355] 'The referenced component 'C1.DataEngine$dataengineversion$' could not be found.' warning message is observed after building the non-core webapi project.

FileManager
    [419962] Add sample for FileManager control.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.235/4.5.20193.235)		Drop Date: 01/10/2020
===================================================================================================
All
    [416166] WF dlls '4.0.20193.393' are included instead of '4.5.20193.398' in '4.5 Core' samples and projects created by Core templates.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.234/4.5.20193.234)		Drop Date: 01/08/2020
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.233/4.5.20193.233)		Drop Date: 01/07/2020
===================================================================================================
All
    Upgrade DataEngine.Standard dll from 121 to 123.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.232/4.5.20193.232)		Drop Date: 01/01/2020
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.231/4.5.20193.231)		Drop Date: 12/27/2019
===================================================================================================
All
    Upgrade Winforms dlls from 393 to 398.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.230/4.5.20193.230)		Drop Date: 12/24/2019
===================================================================================================
BarCode
    [414396] 'None' value of 'CodeType' property is not working as expected when C1 WebAPI Core project 2.2 and above versions are used as service link.

Excel
    [414392] Excel file cannot generate and 'HTTP ERROR 500' is occurred if 'Table from Nwind Database(Products/Shippers)' is used in 'DataName' when webapi core 2.2 and above is used as service.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.229/4.5.20193.229)		Drop Date: 12/18/2019
===================================================================================================

Sample
    [413386][WebApiExplorer] Info for 'DropBox' services is displayed at initialize state in 'DELETE' section of 'File Management(Storage)'.
    [413385][WebApiExplorer] 'CaptionAlignment' is not working properly as expected when using C1 WebAPI Core project 2.2 and above version service link at BarCode service.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.228/4.5.20193.228)		Drop Date: 12/17/2019
===================================================================================================
Samples
    [413111] WebApiJavascriptSample (503) server unavailable error is observed when generating excel with OneDrive data file.
    [413139] Error is occurred when running NonCore\WebApiExplorer 4.5 sample version.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.227/4.5.20193.227)		Drop Date: 12/12/2019
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.226/4.5.20193.226)		Drop Date: 12/12/2019
===================================================================================================
Samples
    [407686] Add readme files for new clientside samples.
    [407314][File Management] Excel file can't upload/delete/list properly choosing path (Azure, AWS, GoogleDrive, DropBox).
	
===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.225/4.5.20193.225)		Drop Date: 11/28/2019
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.224/4.5.20193.224)		Drop Date: 11/26/2019
===================================================================================================

Samples
    [404863] Error occur after click Fech Data in 'ExcelToJson' sample.
    [408313] [Wijmo5 MVC][WebAPI][Sample][Regression] 500 internal server error is occurred at Pdf section when 'WebApi' sample link is used in 'WebApiExplorer' sample.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.223/4.5.20193.223)		Drop Date: 11/22/2019
===================================================================================================
All
    [402163] C1 icon cannot display properly for API dll in NuGet Package Management with NetCore 3.0.

AspNetCore
    [404635] [WebAPI] [Cloud Service] Corrupted file gets downloaded from CloudStorage.

ProjectTemplates
    [408002] Remove Azure cloud register from startup.cs file of 3.0 api core template.
    [407296] [Wijmo 5 MVC][WebAPI][ProjectTemplate] Package Installation Error occur after create project with dll which is used WF dlls in non-core project.

Samples
    [404863] Error occur after click Fech Data in 'ExcelToJson' sample.
    [407314][File Management] Excel file can't upload/delete/list properly choosing path (Azure, AWS, GoogleDrive, DropBox).

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.222/4.5.20193.222)		Drop Date: 11/15/2019
===================================================================================================

All
    [404635] [Cloud Service] Corrupted file gets downloaded from CloudStorage.
    [406332] 500 internal server error is occurred when using C1 WebAPI Core project 2.2 version service link in 'WebAPIExplorer' sample.

Samples
    [406390][WebApiCore2.0] Info for cloud services are not added in 'WebApiCore2.0' sample.
    [406378] [WebApiCore2.0] Errors occur when load 'SSAS (Adventure Works)' data if WebApiCore2.0 sample or WebAPI template with core 2.2 is used as service.
	[404863] Error occur after click Fech Data in 'ExcelToJson' sample.
    [404388] WinForm DLL version is used incorrect version (4.0.20192.382/4.5.20192.382) instead of (4.0.20193.387/4.5.20193.387).

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.221/4.5.20193.221)		Drop Date: 11/12/2019
===================================================================================================
All
    Upgrade Winforms dlls from 387 to 393.
    [406129] Generate XML file for project core.

Samples
    [404742] WF dlls are missing in some product sample project and can't restore of 4.5 dll version.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.220/4.5.20193.220)		Drop Date: 11/08/2019
===================================================================================================
All
    [404635] [Cloud Service] Corrupted file gets downloaded from CloudStorage.

SamplesJP
    [404388] WinForm DLL version is used incorrect version (4.0.20192.382/4.5.20192.382) instead of (4.0.20193.387/4.5.20193.387).
    [402163] C1 icon cannot display properly for API dll in NuGet Package Management with NetCore 3.0.

Samples
    [404433] Displayed logos are different between 'WebApi' samples and 'C1 WebApi' project templates used samples.
    [404442] '500 internal server error' is occurred when using C1 WebAPI Core project service link in 'WebAPIExplorer' sample.

ProjectTemplates
    [404433] Displayed logos are different between 'WebApi' samples and 'C1 WebApi' project templates used samples.
	[404877] ArgumentException error is occurred when running the C1Template prj (.Net Framework) with NetCore 2.2 version.
	
===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.219/4.5.20193.219)		Drop Date: 11/08/2019
===================================================================================================

All
    [404635] [Cloud Service] Corrupted file gets downloaded from CloudStorage.

SamplesJP
    [404388] WinForm DLL version is used incorrect version (4.0.20192.382/4.5.20192.382) instead of (4.0.20193.387/4.5.20193.387).
    [402163] C1 icon cannot display properly for API dll in NuGet Package Management with NetCore 3.0.

Samples
    [404433] Displayed logos are different between 'WebApi' samples and 'C1 WebApi' project templates used samples.
    [404442] '500 internal server error' is occurred when using C1 WebAPI Core project service link in 'WebAPIExplorer' sample.

ProjectTemplates
    [404433] Displayed logos are different between 'WebApi' samples and 'C1 WebApi' project templates used samples.
	[404877] ArgumentException error is occurred when running the C1Template prj (.Net Framework) with NetCore 2.2 version.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.218/4.5.20193.218)		Drop Date: 11/06/2019
===================================================================================================
Samples
    [404048] Export excel to Json on WebApiExplorer throw exeption.

Samples
    [403469] 'C1.Web.Mvc.LicenseDetectorxxx' error occurs when run the 'ExcelToJson' sample.
    [404323][CloudFileExplorer] Revert to previous version of cloud sample.
    [402523] Demo link are incorrectly displayed at some of WebApi sample prj with NetCore 2.0.

ProjectTemplates
    [403492] Errors are occurred when creating the C1Template prj (.Net Framework) with NetCore 2.2 and 3.0.
    [404454] Warning message is displayed when restore the project if template is created with .net core 3.0.

Samples
    [403469] 'C1.Web.Mvc.LicenseDetectorxxx' error occurs when run the 'ExcelToJson' sample.

ProjectTemplates
    [403492] Errors are occurred when creating the C1Template prj (.Net Framework) with NetCore 2.2 and 3.0.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.217/4.5.20193.217)		Drop Date: 11/02/2019
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20193.216/4.5.20193.216)		Drop Date: 11/01/2019
===================================================================================================
All
    Upgrade Winforms dlls from 382 to 387.
    Merge from Dev to Main for CF 2019v3.

Sample
    [403463] Request to add title in readme file of 'ExcelToJson' sample like other samples.

ProjectTemplates
    [403494] Error occurs when create webapi core template using .net core 3.0 without Cloud service.
    [403469] 'C1.Web.Mvc.LicenseDetectorxxx' error occurs when run the 'ExcelToJson' sample.
    [403492] Errors are occurred when creating the C1Template prj (.Net Framework) with NetCore 2.2 and 3.0.
    [403522][Regression] Some of services checkboxes are missing in Project Settings of WebAPI 2.2 template.

BarCode
    [403495] Displayed BarCode design is incorrect displayed when .net core 3.0 (WebApiCore2.0 sample) is used as server link.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.215/4.5.20192.215)		Drop Date: 09/25/2019
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.214/4.5.20192.214)		Drop Date: 09/19/2019
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.213/4.5.20192.213)		Drop Date: 09/10/2019
===================================================================================================
Samples
    [397404] WebApi(Core) sample cannot run and error is occurred when build the sample.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.212/4.5.20192.212)		Drop Date: 09/06/2019
===================================================================================================
All
    Upgrade Winforms dlls from 375 to 382.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.211/4.5.20192.211)		Drop Date: 09/05/2019
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.210/4.5.20192.210)		Drop Date: 09/03/2019
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.209/4.5.20192.209)		Drop Date: 08/29/2019
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.208/4.5.20192.208)		Drop Date: 08/29/2019
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.207/4.5.20192.207)		Drop Date: 08/29/2019
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.206/4.5.20192.206)		Drop Date: 08/28/2019
===================================================================================================
Samples
    [395525]  ASPNETCore sample is included in WebApi 4.0 version samples.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.205/4.5.20192.205)		Drop Date: 08/20/2019
===================================================================================================
ProjectTemplates
    [373811] 'Could not load file or assembly 'System.Web.Cors, Version=5.0.0.0,...................' error is occurred when create and run the project using WebApi 2.2 templates(CS/VB) without checking 'As Self-Hosted Application' checkbox.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.204/4.5.20192.204)		Drop Date: 07/29/2019
===================================================================================================
SampleJP
    [391708] Error occurs when run the 'SalesAnalysis' sample.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.203/4.5.20192.203)		Drop Date: 07/25/2019
===================================================================================================
SampleJP
    [391712] Cannot login and 500 Internal server error occurs when click on Login button.
    [391707] Cannot restore and error occurs when 'SalesAnalysis'(VB) is opened from 4.5 sample dropped.
    [391708] Error occurs when run the 'SalesAnalysis' sample.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.202/4.5.20192.202)		Drop Date: 07/24/2019
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.201/4.5.20192.201)		Drop Date: 07/09/2019
===================================================================================================
Sample
    [386446] [Regression] Sample restore is failed and errors are occurred in 'Startup.cs' and 'HTTPStorage.cs' pages of 'WebApi' and 'WebApi(ASPNETCore)' samples.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.200/4.5.20192.200)		Drop Date: 07/09/2019
===================================================================================================
All
    No changes, just version upgrade.
===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.199/4.5.20192.199)		Drop Date: 07/08/2019
===================================================================================================
All
    [384175] Update JP XML document for Cloud dll.

Sample
    [386446] [WebApi][Regression] Sample restore is failed and errors are occurred in 'Startup.cs' and 'HTTPStorage.cs' pages of 'WebApi' and 'WebApi(ASPNETCore)' samples.


===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.198/4.5.20192.198)		Drop Date: 07/07/2019
===================================================================================================
All
    [386456] [License][Regression] In Trial state, Generated from WebAPI Core(self/IIS) server link is not working properly and 500 Internal Server error occurs

Project Templates
    [301896] Cannot export the excel file which has large data.
    [385516] Error is occurred when build the project that is used WebApi core template.

Sample
    [387521] WebApi template service link is not correct in 'Web.config' file of WebApiExplorer with 4.5 version.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.197/4.5.20192.197)		Drop Date: 07/06/2019
===================================================================================================
All
    No changes, just version upgrade.

===================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.196/4.5.20192.196)		Drop Date: 07/04/2019
===================================================================================================
All
    Upgrade Winforms dlls to 375.

Sample
    [387249] [CloudFileExplorer] Console error 'Uncaught TypeError : xxxxx' is occurred when clicking 'Refresh Folder' button at non-selecting or any selecting Cloud Services.

========================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.195)		Drop Date: 07/01/2019
========================================================================================================
All
    [386922] Error occurs in CreateDocument function when sample is restored which is opened from 4.5 version.
    [386657] Could not load file or assembly error occurs for WF DataEngine dll when the non-core samples in 4.5 version.    
    [386452] In Trial state, Water Mark doesn't display in exported file of BarCode with any format.

Sample
    [386446] [WebApi][Regression] Sample restore is failed and errors are occurred in 'Startup.cs' and 'HTTPStorage.cs' pages of 'WebApi' and 'WebApi(ASPNETCore)' samples.
    [386931] WF versions display incorrectly(i.e display 4ll instead of 4.5.2) at 'ExcelExportWithLargeData' sample page with 4.5 dll version.

========================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.194)		Drop Date: 07/01/2019
========================================================================================================
All
    No changes, just version upgrade.

========================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.193)		Drop Date: 07/01/2019
========================================================================================================
All
    No changes, just version upgrade.

========================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.192)		Drop Date: 06/28/2019
========================================================================================================
WebAPI
    [385804] Separate cloud storage to other dll.
    [371709] Making cloud storage api's as an builtin feature of WebApi storage. 
    [377732] Making cloud storage List Api.

ProjectTemplates    
    [385516] Error is occurred when build the project that is used WebApi core template.
    [373811] 'Could not load file or assembly 'System.Web.Cors, Version=5.0.0.0,...................' error is occurred when create and run the project using WebApi 2.2 templates(CS/VB).

Sample
    [384926] Add new api 'List' into sample WebAPIExplorer.
    [380455] Making separate sample the introduce builtin cloud storage Api.
========================================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.191/4.5.20192.191)		Drop Date: 06/24/2019
========================================================================================================
All
    [385203] Some of the WF nuget version is used as 4.0 in WebAPI Bin folder of 4.5 version.

BarCode
    [384529] 500 Internal Server Error is occurred and cannot generate BarCode.

Samples
    [382313] Add missing json file into sample projects.

ProjectTemplates
    [384016] Add missing tags for WebAPI project templates.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.190)		Drop Date: 06/18/2019
=====================================================================================
All
    Upgrade WinForms dlls to 372.

Project Templates
    [366164] [WebApi][ExcelService] '413' (Request Entity Too Large) error is displayed when use 'https' as service link and export large excel files.
    [382278][VS2019] Cannot install and 'The install of xxxx was not succesful xxxxxx' message is displayed when installed C1ProjectType template in VS 2019.

Samples
    [356544] [WebApi][SalesReport][Sample] Request to display user's role above 'Sign Out' button after Login.
    [371709] [WebAPI] Making cloud storage api's as an builtin feature of WebApi storage.

WebApi
    [384047] [Wijmo 5 MVC][ReportViewer][Regression] FlexReports and SSRS Reports can't load and 500 internal server error is occurred
	
=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.189)		Drop Date: 06/18/2019
=====================================================================================
All
    Upgrade WinForms dlls to 372.

Project Templates
    [366164] [WebApi][ExcelService] '413' (Request Entity Too Large) error is displayed when use 'https' as service link and export large excel files.
    [382278][VS2019] Cannot install and 'The install of xxxx was not succesful xxxxxx' message is displayed when installed C1ProjectType template in VS 2019.

Samples
    [356544] [WebApi][SalesReport][Sample] Request to display user's role above 'Sign Out' button after Login.
    [371709] [WebAPI] Making cloud storage api's as an builtin feature of WebApi storage.

WebApi
    [384047] [Wijmo 5 MVC][ReportViewer][Regression] FlexReports and SSRS Reports can't load and 500 internal server error is occurred

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.188)		Drop Date: 06/12/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.187)		Drop Date: 06/12/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20192.186)		Drop Date: 06/10/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.185)		Drop Date: 05/21/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.184)		Drop Date: 05/15/2019
=====================================================================================

    [378411] [Wijmo5 MVC][WebAPI][Sample][MVCExplorer][WebApiExplorer][Regression] 'String was not recognized as a valid Boolean.' error is occurred in some samples when changed the value from setting's dropdown

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.183)		Drop Date: 05/08/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.182)		Drop Date: 05/07/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.181)		Drop Date: 05/07/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.180)		Drop Date: 05/03/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.179)		Drop Date: 04/08/2019
=====================================================================================
All
    Upgrade winform build 359 to 362.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.178)		Drop Date: 04/07/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.177)		Drop Date: 04/04/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.176)		Drop Date: 04/03/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.175)		Drop Date: 04/03/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.174)		Drop Date: 04/02/2019
=====================================================================================
    [364960] Update JP License agreement.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.173)		Drop Date: 03/26/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.172)		Drop Date: 03/22/2019
=====================================================================================
    [355179] 2018v3 JP Sample issues.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.171)		Drop Date: 03/14/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.170)		Drop Date: 03/11/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.169)		Drop Date: 03/11/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.168)		Drop Date: 03/08/2019
=====================================================================================

Samples
   [369337] [Storage][File Management] Excel file can't upload/delete and 500 internal server error is occurred when choose the path 'DropBox/C1WebApi/test.xlsx.
   [369345] [Storage] Uploaded excel file cannot open and 'Excel cannot open the file..............' alert box is appeared when upload the file using the path 'GoogleDrive/WebAPI/test.xlsx.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.167)		Drop Date: 03/08/2019
=====================================================================================
ProjectTemplates
    [368871] Package Installation Error occur when PdfViewer/ReportViewer Item Template is added in VS 2019.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.166)		Drop Date: 03/06/2019
=====================================================================================
ProjectTemplates
    [368871] Package Installation Error occur when PdfViewer/ReportViewer Item Template is added in VS 2019.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.165)		Drop Date: 03/05/2019
=====================================================================================
All
    Upgrade winform build 358 to 359.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.164)		Drop Date: 03/01/2019
=====================================================================================
All
    Upgrade winform build 357 to 358.

Samples
    [365718] Removes unused resource files in project SalesAnalysis VB.
    [365667] [SalesReport][Sample][Regression] 'ComponentOne' icon and 'Sign out' button are distorted.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.163)		Drop Date: 02/22/2019
=====================================================================================
ProjectTemplates
    [367468] 'OK' and 'Cancel' buttons are displayed inconsistently in 'ComponentOne ASP.NET Web API Application Wizard' form when create the projects using WebAPI 2.2 or core template.

Samples
    [365718] Fix bug "Resource is not declared" in Sales Analysis samples.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.162)		Drop Date: 02/17/2019
=====================================================================================
All
    [365126] Update JP License agreement.
    Upgrade winform build 343 to 357.

ProjectTemplates
    [365763] 'Package Installation Error' occurs when create the WebApi 2.2 templates in VS 2019 Preview 2.1.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.161)		Drop Date: 01/31/2019
=====================================================================================
All
    [364960] Update JP License agreement.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20191.160)		Drop Date: 01/31/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.159)		Drop Date: 01/10/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.158)		Drop Date: 01/08/2019
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.157)		Drop Date: 12/27/2018
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.156)		Drop Date: 12/21/2018
=====================================================================================
Samples
    [358238] Request to update latest build version in demo link of WebApiExplorer sample for both JP and ENG sample dropped.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.155)		Drop Date: 12/19/2018
=====================================================================================
All
    Upgrade winform build 338 to 343.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.154)		Drop Date: 12/12/2018
=====================================================================================
[All]
    [357115] [Export] [ImageService][ExcelService] Images, some of the excel files cannot be exported in Chrome browser with version (71.0.3578.80).

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.153)		Drop Date: 12/07/2018
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.152)		Drop Date: 12/05/2018
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.151)		Drop Date: 11/27/2018
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.150)		Drop Date: 11/13/2018
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.149)		Drop Date: 11/12/2018
=====================================================================================
Project Template
    [352889] Templates which is saved in 'Web' tag should be displayed in 'Visual Basic' and 'Visual C#' tags.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.148)		Drop Date: 11/09/2018
=====================================================================================
Project Template
    [350489] missing some projects listed in C# or VB category.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.147)		Drop Date: 11/07/2018
=====================================================================================
All
    Upgrade winform build 336 to 338.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.146)		Drop Date: 11/05/2018
=====================================================================================
All
    Upgrade winform build 333 to 336.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.145)		Drop Date: 11/01/2018
=====================================================================================
All
    Upgrade winform build 333 to 334.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.144)		Drop Date: 10/26/2018
=====================================================================================
All
    Upgrade winform build 326 to 333.

Samples
    [348824] In Core project, the report cannot be rendered if the report path contains Unicode characters.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.143)		Drop Date: 10/18/2018
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.142)		Drop Date: 10/12/2018
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.141)		Drop Date: 10/08/2018
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20183.140)		Drop Date: 10/05/2018
=====================================================================================
All
    Upgrade winform build 314 to 326.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20182.139)		Drop Date: 08/22/2018
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20182.138)		Drop Date: 08/15/2018
=====================================================================================
Samples
    [336860] Vertical Scroll bar is displayed in side menu in some samples.
    [336978] 'Uncaught TypeError: Cannot read property 'requestHeaders' of null' error occurs after logging in.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20182.137)		Drop Date: 08/01/2018
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20182.136)		Drop Date: 07/06/2018
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20182.135)		Drop Date: 07/04/2018
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20182.134)		Drop Date: 07/02/2018
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20182.133)		Drop Date: 07/02/2018
=====================================================================================
All
    Upgrade winform build 313 to 314.

Project Templates
    [330673] Update the year range to "1978-2018" in the license readme for ProjectTemplates.

Samples
    [330246] Icon is not Asian market C1 icon in WebApiExplorer sample.
    [330362] Update supported cultures in CORE samples.
    [330695] Update the link in the mvc and api samples.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20182.132)		Drop Date: 06/27/2018
=====================================================================================
All
    Upgrade winform build 312 to 313.
    [329318] Apply JP xml documents.

Samples
    [329574] Update GTM in samples.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20182.131)		Drop Date: 06/21/2018
=====================================================================================
All
    Upgrade winform build 310 to 312.

Samples
    [327651] 'description is a duplicate attribute name.xxxxxxx' error occur when run the WebApiExplorer sample.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20182.130)		Drop Date: 06/15/2018
=====================================================================================
Samples
    [326789] Apply JP Resources to some samples.

Project Templates
    [326950] Error occurs when rebuild the Core/Framework(Core 2.1) project.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20182.129)		Drop Date: 06/08/2018
=====================================================================================
All
    Upgrade winform build 309 to 310.

Project Templates
    [325929] Update the version number for Core 2.1.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20182.128)		Drop Date: 05/29/2018
=====================================================================================
Samples
    [324479] MVC dll version is not latest and used '4.0.20181.55555' in 'WebApiExplorer' sample.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20182.127)		Drop Date: 05/25/2018
=====================================================================================
All
    Merged from Development to Main branch.
    Upgrade winform build 296 to 309.
    [315864] Use JP xml docments for JP build.

Samples
    [319855] Error occurs when rebuild the webapi 2.2 sample in JP environment.
    [313252] Add JP resources in WebAPI sample.
    [313253] Add JP resources in WebApiExplorer sample.
    [316974] [WebApiExplorerSample] "ID" column is missing in "Overview" sample when minimize the browser or zoom size of browser is very large.
    [317105] [WebApiExplorer]Javascript error occurs after navigate to "Olap" sample under "MVC Controls" of DataEngine service.

Excel Services
    [320046] Unlike the other special characters, console error occurs when find space(' ') in Find/Replace sample.
    [319981] Error occurs when generate the excel file with xml format.

Project Templates
    [321551] Add missing metadata in vsix for EN build.
    [316504] Request to get focus on 'OK' button in project wizard form.
    [320679] Replace C1 with ComponentOne in JP build.
    [316504] Request to get focus on 'OK' button in project wizard form.
    [315963] Add CORE 2.1 in all MVC and API project templates.

Report Services
    [316431] Support AllowBlank and MaxLength for string parameter.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20181.126)		Drop Date: 03/15/2018
=====================================================================================
Samples:
    [312904]Cannot restore the webapi dlls in sample.

Excel Services
    [313312] Add C1.C1Zip.4 assembly into the C1.Excel package.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20181.125)		Drop Date: 03/07/2018
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20181.124)		Drop Date: 03/02/2018
=====================================================================================
Excel
    [301896] Cannot export the excel file which has large data.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20181.123)		Drop Date: 02/24/2018
=====================================================================================
All
    Upgrade winform build 293 to 296.

BarCode
    [132022]AdditionalNumber doesn't place in a correct location in some condition.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20181.122)		Drop Date: 02/09/2018
=====================================================================================
All
    Upgrade winform build 292 to 293.

Report
    [289812]Parameters get reset on pressing tab key after setting next parameter.

Samples
    [301896]Cannot export the excel file which has large data.

DataEngine
    [305464]DataEngine will throw an exception when setting some filters on the fields in both Columns and Values.
    [305999]Support the filter for DateTime field and numeric field in Dimension.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20181.121)		Drop Date: 02/02/2018
=====================================================================================
Excel
    [305106] Error occurs after import the attached excel file which is merge all cols/rows in excel sheet.

Report
    [306974]Error occurs when export the report if chose 'PageRange' in Page range dropdown.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20181.120)		Drop Date: 01/26/2018
=====================================================================================
All
    Upgrade winform build 286 to 292.
    Upgrade major version 20173 to 20181.

Document
    [261316]Some inconsistance behavior are observed if "0" is set in outputRange of export option.
    [256860]Case insensitive for export options with GET method.
    [300177]Search result cannot be displayed properly when 'searchscope' is 'end of the document' and 'startpageIndex' are set.
    [300182]Error occurs when search the data if start page index is larger than total page count of report/pdf file.
    [261316]Some inconsistance behavior are observed if "0" is set in outputRange of export option.
    [256860]Case insensitive for export options with GET method.
    [297554]Support search scope and start page in Report and PDF services.

Pdf
    [242900]Should return 404 when pdf path is wrong.

Report
    [301842]Error occurs when export the report files.
    [300215]Incorrect behavior is observed when choosing item from 'Category' dropdown of 'Product Line Sales' report.
    [297499]Expose Timeout for connection options of SSRS document.
    [296663]Unable to set back Control value to Null.

Excel
    [301886]"Fail Network Error" error occurs when merge the excel files when total file size is large if webapi 3 project is open with Core20.2017 solution.

Samples
    [301788]Update the documentation link in samples.
    [301797]Javascript error occurs after navigate to "Olap" sample under "Wijmo 5 Controls" of DataEngine service in WebApiExplorer sample.
    [271804]Add fields when changing dataset in WebApiExplorer's Olap sample.

Project Templates
    [299775]Run license manager after creating core projects via templates.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20173.119)		Drop Date: 11/30/2017
=====================================================================================
All
    [299014]Update nuget package icon url for JP build.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20173.118)		Drop Date: 11/30/2017
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20173.117)		Drop Date: 11/29/2017
=====================================================================================
All
    Update authors of vsix/nuget packages to "GrapeCity, Inc.".
    Upgrade winform build 285 to 286.

Project Templates
    [298539]Version number of templates are displayed incorrectly in some vsix templates.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20173.116)		Drop Date: 11/26/2017
=====================================================================================
All
    Upgrade winform build 282 to 285.
    [298430]Update icons for 2017v3 JP release.

Project Templates
    [298176] 'MoreInfoURL' value is missing and License agreement box is different with other vsix template in C1ProjectType.
    [298361] Different behavior are observed in EULA between vsix template and word file"(GC_EULA_DEV_TOOLS_(07-26-17)_JPN)".

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20173.115)		Drop Date: 11/21/2017
=====================================================================================
All
    [298035]Update links for 2017v3 JP release.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20173.114)		Drop Date: 11/02/2017
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20173.113)		Drop Date: 10/31/2017
=====================================================================================
Samples
    [294334]Update link of C1 home page in WebApi sample.
    [294599]Update the link address of C1 logo in all samples.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20173.112)		Drop Date: 10/29/2017
=====================================================================================
All
    Upgrade winform build 281 to 282.

Report Services
    [286896]Cannot export excel format(xls,xlsx) when excel file size is over 75 KB and the sample is opened with 'FlexViewerExplorer.Core20.2017.sln'.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20173.111)		Drop Date: 10/23/2017
=====================================================================================
All
    Upgrade winform build 277 to 281.

Report Services
    [286896]Cannot export excel format(xls,xlsx) when excel file size is over 75 KB and the sample is opened with 'FlexViewerExplorer.Core20.2017.sln'.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20173.110)		Drop Date: 10/19/2017
=====================================================================================
WebApiExplorer
    [261833]Request to change the button to disable state if report does not load in "Report Instance" some sample pages.
    [251983]FlexChart shows some confusing labels.

Samples
    [291317]404 Not Found error occurs for some icons when deploy Explorer samples on Azure.

Project Templates
    [291534]Logo of MVC classic is not updated and C1 logo cannot be displayed in all templates of AspNET MVC and WebApi.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20173.109)		Drop Date: 10/13/2017
=====================================================================================
Image
    [290681]Request to support exporting for TreeMap control.

All
    [290382]Update c1 icons.

Excel Services
    [281237]Error occurs after post the file using "WebApi.Core20.2017.sln" as a server link

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20173.108)		Drop Date: 09/29/2017
=====================================================================================
All
    Upgrade winform build 271 to 277.
    Upgrade major version 20172 to 20173.

DataEngine
    [286003]Add meaningful exception information.
    [287655]Update JP resources.
    [287642]Add a public method to dispose workspace.

WebApiExplorer
    [288279]Javascript error occurs when generate the excel file more than one time using JSON type.
    [288804]Export formats are not localized in Get/Post Method of PDF/Report service in JP environment.
    [281284]Date format is displayed instead of Supplier ID in exported file of product data set if exported format is excel fromat(xls, xlsx, csv).

Samples
    [274327]Hide SSRS credential from startup.cs.
    [285289]Add google analytics tags in all web demo samples.
    [273477]Warning displays in error list after rebuilding the "Samples-->VB-->SalesAnalysis" sample in VS2015.
    [288729]Update new icons and links in all Web API samples.
    [265176]Cannot show a complete appearance of file uploading button.

Project Templates
    [274106]Update Web API project template (Standard) for Core 2.0.
    [274105]Update Web API project template wizard for Core 2.0.
    [280546]Rename Web API standard project template.
    [270791]It pops up 404 error when running webapi template.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20172.107)		Drop Date: 08/25/2017
=====================================================================================
Samples
    [274104]Update Core WebApi sample for Core 2.0.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20172.106)		Drop Date: 08/20/2017
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20172.105)		Drop Date: 06/30/2017
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20172.104)		Drop Date: 06/29/2017
=====================================================================================
All
    Upgrade winform build 268 to 271.

Samples
    [270779]Exclude the UWPWebAPIExplorer sample.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20172.103)		Drop Date: 06/26/2017
=====================================================================================
Samples
    [269697]The license in "CS-->ASPNETCore-->WebApi" is invaild.
    [268986]Update readme.txt for some samples.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20172.102)		Drop Date: 06/24/2017
=====================================================================================
All
    Upgrade winform build 267 to 268.

Report
    [264701]]Can not unzip file for exporting  CompressedMetafiles format of report with setting Export Option.

Samples
    [269284]Rename WebApi3 sample to ASPNETCore\WebApi.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20172.101)		Drop Date: 06/20/2017
=====================================================================================
All
    Upgrade winform build 266 to 267.

WebApiExplorer
    [268796]Update WebApiExplorer sample base on new actionString format.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20172.100)		Drop Date: 06/18/2017
=====================================================================================
All
    Upgrade winform build 263 to 266.

Project Templates
    [266514]"Global.json" is opened after creating "ASP.NET Core Application" in VS2015.
    [264301]Cannot start WebApi project that I create it without selecting "Create directory for solution".

WebApiExplorer
    [266829]Should change code "C1XapOptimizer.pdf" to "DefaultDocument.pdf" in WebApiExplorer sample.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20172.99)		Drop Date: 06/12/2017
=====================================================================================
Project Templates
    [257826]Error dialog displays after creating Core application by VS2015 which environment contains VS2017.

WebApiExplorer
    [251979]Client settings doesn't take effect on Mvc Control.
    [264476]No data displays in the "WebApiExplorer-->Excel-->Import/Export Services-->Wijmo5Controls-->FlexGrid" sample.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20172.98)		Drop Date: 06/08/2017
=====================================================================================
All
    Move C1.Win.Bitmap.4.dll from C1.Document package to C1.Excel package.

Samples
    [264394]Update the pdf file in WebApi sample with the DefaultDocument.pdf.
    [264307]Error displays in the browser after exporting the FlexGrid to Excel in "ExcelExportWithLargeData" sample.
    [261294]"ReadMe" and "Screenshot" files are missing in "ExcelExportWithLargeData" sample for VB project.
    Update reference of C1.Win.Bitmap.4.dll. (SalesReport, WebApi, ExcelExportWithLargeData)

WebApiExplorer
    [264476]No data displays in the "WebApiExplorer-->Excel-->Import/Export Services-->Wijmo5Controls-->FlexGrid" sample.
    [264478]]The value for "DataLabelPosition" in the "WebApiExplorer-->Image-->Export Services-->MVC Controls-->FlexPie" sample is not correct.
    [261321]Error occurs after export the PDF file with Compressed metafiles format.
    Remove Compressed metafiles/Open XML Excel/Microsoft Excel/Open XML Word options in Pdf export formats.
    [265135]The fields for SSAS can't be removed after changing the "DataSet" to other value in "WebApiExplorer-->DataEngine-->Samples for Services-->Olap".

Pdf Services
    [261624]Error occurs after load the attached pdf file("Annualreport.pdf").

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20172.97)		Drop Date: 06/03/2017
=====================================================================================
All
    [257506]Jp resource is not recognized for "C1 Web API Application for ASP.NET Core" project in JP Environment.

Project Templates
    [262948]Adjust the layout of wizard.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20172.96)		Drop Date: 05/26/2017
=====================================================================================

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20172.95)		Drop Date: 05/26/2017
=====================================================================================
All
    Upgrade major version 20171 to 20172.
    Upgrade winform build 250 to 263.
    [256865]JS error displays after change the export type to "OpenXml"/"RTF" in FlexViewerExplorer.
    [250694]Inconsistent information is exported for the Document properties of 'Open XML Word' and 'Microsoft Excel' formats.
    [250930]'Inverted' export format cannot be set properly in any supporting format types.

Project Templates
    Make small changes for supporting VS2017.
    [218118]User is unable to recover some references if he doesn't select "Create directory for solution".
    [258956]Update API  project templates with new structure.

Pdf Services
    [247140]Add search api for Pdf services.
    [250375]Hightlight of search item is not correctly.
    [250371]Add offical license verifying.
    [245429]File Name cannot display correctly after export the Pdf file.
    Hide paged export options from Docx/Rtf/Html for Pdf.
    [253589]License dialog for 'C1.C1Excel' displays after exporting pdf file to excel file.

Report Services
    [242892]Error displays after exporting SSRS to html with "Paged" checked in "FlexViewerExplorer" sample.

Image Services
    Make image services support  and series in wijmo.chart.analytics module.
Excel Services Client
    [146397]The column width in the exported excel is different from the column width in FlexGrid in some condition.

Excel Services
    [242855]The responses of accessing api/excel are different between Web API 2.2 and Core.

Samples
    Create ExcelExportWithLargeData sample.
    [251980]The TickMarks of chart in the exported image file is different from it in browser.
    [149092]Move database from mvc/webapi samples to common database folder.
    [247181]Update WebApiExplorer sample with new features of pdf.
    Make small changes for supporting VS2017.
    Add FlexRadar, Sunburst and wijmo.chart.analytics series samples in WebApiExplorer Image services catalog.
    Add C1.Win.Bitmap.4.dll as the reference of WebApi sample.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20171.94)		Drop Date: 05/09/2017
=====================================================================================
All
    Upgrade winform build 260.
    [256865]JS error displays after change the export type to "OpenXml"/"RTF" in FlexViewerExplorer.

Pdf
    [253589]License dialog for 'C1.C1Excel' displays after exporting pdf file to excel file.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20171.93)		Drop Date: 05/05/2017
=====================================================================================
All
    Upgrade winform build 259.

WebApi
    Hide paged export options from Docx/Rtf/Html for Pdf.

Pdf
    [245429]File Name cannot display correctly after export the Pdf file.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20171.92)		Drop Date: 04/18/2017
=====================================================================================
WebApi
    [250694]Inconsistent information is exported for the Document properties of 'Open XML Word' and 'Microsoft Excel' formats.
    [250930]'Inverted' export format cannot be set properly in any supporting format types.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20171.91)		Drop Date: 03/01/2017
=====================================================================================
All
    Upgrade winform build 248.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20171.90)		Drop Date: 02/24/2017
=====================================================================================
Report
    [242890]Error displays after exporting Report/PDF by using post method in WebApi2 application.

DataEngine
    [240384]The detail row show no date if the fields contains "Date" in the OlapExplorer sample.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20171.89)		Drop Date: 02/23/2017
=====================================================================================
Sample
    [242724]Failed to build the Webapi3 sample of VS2017.

Project Template
    [242582]Update web api VS2017 ASP.NET CORE project template.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20171.88)		Drop Date: 02/21/2017
=====================================================================================
Sample
    [241972]"Excel" service and "Barcode" servie doesn't work on sample project.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20171.87)		Drop Date: 02/21/2017
=====================================================================================
WebApi
    Make the samples show the service introduction as required.

Core
    [240382]It can only download a HTML file when we try to export a report for different file format.
    [240375]Server side cannot respond "Find" request of "Excel Service" in WebApiExplorer.

WebApi3 Sample
    [239322]Error displays in the "MVC application-->Property-->Build" which is created by VS2017.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20171.86)		Drop Date: 02/17/2017
=====================================================================================
WebApiExplorer
    Improved report web api descriptions.

Client
    Make small change because the obsoloted wijmo.xlsx.IWorkSheet.cols member is removed.

Core
    Make BarCode, Excel and Image services to be virtual so that they can be overrided.

WebApi
    Make the sample show the service introduction as required.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20171.85)		Drop Date: 02/15/2017
=====================================================================================
Samples
    [238504]Request to update icon for DataEngine in sample.

SSRS
    [238764]Report does not load after clicked on "Print Layout icon" of SSRS report.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20171.84)		Drop Date: 02/13/2017
=====================================================================================
Report
    [236477]Javascript error is observed when enter only space in search box.

Samples
    [238540]In both CS and VB sample of "SalesAnalysis", Javascript error occurs after clicked on login button.
    [236218]Fix the reopen bug.
    [238533]Fields name which is displayed in PivotPanel are cut-off.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20171.83)		Drop Date: 02/10/2017
=====================================================================================
Pdf
    [236615]Some inconsistent behaviors found in Pdf sample.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20171.82)		Drop Date: 02/10/2017
=====================================================================================
DataEngine
    [237645]Add license for DataEngine web api.
    [237964]Failed to run Olap sample.

WebApi
    [238037]Use ServerConnection.GetItemType() to detect the SSRS catalog item type.
    [238072]Implement loading pdf from stream of Pdf Services.

Client
    [236433]Some inconsistance behavior are observed after exporting the data in some page.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20171.81)		Drop Date: 1/25/2017
=====================================================================================
Document
    Add 'Group' property for supported format response data ExportOptionDescription.
    Change type of ExportOptionDescription.DefaultValue from string to object.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.79)		Drop Date: 1/6/2017
=====================================================================================
Samples
    [213915]Errors display in error list after rebuilding the VB/SalesAnalysis sample in VS2013.
    [232854]User meets with error message when he generate excel file from demo database.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.78)		Drop Date: 12/29/2016
=====================================================================================
Samples
    [222596]Implement Web API security sample of Web API 2.2 VB.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.77)		Drop Date: 12/28/2016
=====================================================================================
Project Templates
    [227823]Error displays in the response data after request URL of cube which service is created by "VS2012+VB+Self-host" in JP build.

Samples
    [222594]Implement Web API security sample of Web API 2.2 C#.
    [227350]Update the WebAPIExplorer sample as per the new spec of report/pdf/dataengine.

DataEngine
    Remove useless resources.
    [230227]The trial license of winform shows in webapi sample.

Report
    [230972]"500 Internet error" displays after set the wrong path of SSRS to the service of WebApi.
    [230267]Report does not load and console error is observed when clicked the print layout button.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.76)		Drop Date: 12/23/2016
=====================================================================================
WebApiExplorer
    [224977]There isn't sample of "ViewDefinition" for "SSAS(Adventure Wroks)" in "WebApiExplorer" sample.

DataEngine
    [222616]The returned "status" value of DataEngine URL is always "Executing" after setting the wrong "ViewDefinition".
    [226710]Web browser throws error message when user send "get status" request for a token.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.75)		Drop Date: 12/22/2016
=====================================================================================
Samples
    [224964]Internal error displays after rendering Olap control in browser.

DataEngine
    [222616]Update the request for Detail in DataEngine web api.
    [228296]Roll back to beta licensefor DataEngine web api.
    [227352]Refactor the dataengine according to the new response.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.74)		Drop Date: 12/09/2016
=====================================================================================
Samples
    [227097]Update WebApi3 sample to support vs2017.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.73)		Drop Date: 12/08/2016
=====================================================================================
Project Templates
    [225009]Webapi core template does not be localizated in VS2017.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.72)		Drop Date: 12/05/2016
=====================================================================================
DataEngine
    [222616]Refactor DataEngine WebAPI url format.
    [222634]Allow the user to customize the max count of the aggregated data.
    [222632]Add total items count for getting detail cell data and all the rawdata.
    [222641]Update the codes to support customized route.
    [222719]Support progress when getting the aggregated data.
    [222637]Update the WebAPIExplorer samples.

Project Templates
    [222599]Implement Web API project templates for VS2017 JP.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.71)		Drop Date: 11/10/2016
=====================================================================================
Project Templates
    Make En ASP.NET Core project templates for VS2017.
    [220025]Can not run the DataEngine service correctly created by vstemplate.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.70)		Drop Date: 11/09/2016
=====================================================================================
Project Templates
    [218970]In both VB and CS, Webapi2.2 project cannot create and package installation error is displayed in VS2017 RC.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.69)		Drop Date: 11/02/2016
=====================================================================================
All
    Upgrade winform assemblies from 211 to 212.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.68)		Drop Date: 10/31/2016
=====================================================================================
ProjectTemplates
    [216317]Should not generate C1.Web.Api.LicenseDetector when only check Report services in wizard dialog.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.67)		Drop Date: 10/27/2016
=====================================================================================
Samples
    [210460]Need the PDF icon in WebApi Explorer sample.

DataEngine
    [209031]Some parameters in "analysis" request will invite underlying error.
    [213305]Internet error "A task may only be disposed if it is in a completion state" in the browser if move the fields to frequently.
    [208858]Dataenginetoken's status is always "Executing" when you send "analysis" request without any data.

Report
    [215124]Suggest to update the export file name to SSRS report name.

Pdf and Report
    [215535]Fix C1.AspNetCore.Api.Pdf and C1.AspNetCore.Api.Report ja nupkg issue.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.66)		Drop Date: 10/25/2016
=====================================================================================
WebApiExplorer
    [213817]PivotPanel and PivotGrid are overlapped in IE 11.
    [214561]Issue of sample project: user fails to send "detail" request.
    [214607]Issue of sample project: user fails to send "uniquevalues" request.

Pdf
    Fix Pdf services(Web API 2.2) license issue.
    [214455]Evaluation version message of "C1.Win.C1Document" pops up after rendering PDFViewer in Core application.

Report
    [214701]Failed to open the export file with zip format in SSRS report.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.65)		Drop Date: 10/21/2016
=====================================================================================
License
    Implement 2016v3 licenses of pdf, report and data engine services.

Samples
    [211512]Some files are duplicate in WebApi sample

Document
    [214162]It takes a long time to display the outline of SSRS in ReportViewer.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.64)		Drop Date: 10/20/2016
=====================================================================================
Pdf
    [210462]Does not been localizated in webapi template in JP.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.63)		Drop Date: 10/16/2016
=====================================================================================
Report
    [192891]Add pageSettings in the parameters of export action of report controller.
    [210670]The request of "OutLines" returns "false" while the report contains outline.
    [211747]Throw exception of cache id not found.
    [213249]Clicking Hyperlink does not navigate correctly in "Sales_by_Region" report.

Sample
    [211512]Some files are duplicate in WebApi sample.
    [212564]Update WebApiExplorer to show all report APIs including SSRS.

Olap
    Update the codes according to the review.
    Update the WebApiExplorer sample for the DataEngine Web Api.
    [213063]Can't display the detail data after double clicking the column of PivotGrid if the use the "AddDataSource" method of provider as service.
    [212578]Add the pages to show the apis of the DataEngine web api in WebApiExplorer sample.
    Update the cancel api for token.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20163.62)		Drop Date: 09/29/2016
=====================================================================================
Excel
    Fix the issue that merged cells can't be exported correctly.

Report
    Implement SSRS support in Report WebAPI.

Pdf
    Implement Pdf WebAPI.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20162.61)		Drop Date: 08/29/2016
=====================================================================================
Report
    [204903]Setting parameter has problem in Cascading Parameters report.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20162.60)		Drop Date: 08/25/2016
=====================================================================================
Samples
    Update all report samples as per WinForm's.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20162.59)		Drop Date: 08/15/2016
=====================================================================================
Excel
    [203232]Can't generate any data source to Json format.
    [203254]Cannot export Excel in virtual scrolling page.

Report
    [203563]Error occurs when setting PageSettings without parameters.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20162.57)		Drop Date: 08/09/2016
=====================================================================================
All
    [200950]Refactor WebAPI.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20162.56)		Drop Date: 07/05/2016
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20162.55)		Drop Date: 07/05/2016
=====================================================================================
License
    [189888]The feature "Split/Find/Replace/Add/Delete" of WebApi work correctly without license.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20162.54)		Drop Date: 07/04/2016
=====================================================================================
All
    No changes, just version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20162.53)		Drop Date: 06/30/2016
=====================================================================================
All
    Implement asp.net core 1.0 support in webapi.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20162.52)		Drop Date: 06/29/2016
=====================================================================================
WebApiExplorer
    [194439]Need the dataEngine icon on the sample.

License
    [197333]Modify the license check code to support License Generator tool.
    [189888]The feature "Split/Find/Replace/Add/Delete" of WebApi work correctly without license.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20162.51)		Drop Date: 06/24/2016
=====================================================================================
DataEngine
    Add jp resources.
    [195563]The fields don't display after using the DataSet data.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20162.50)		Drop Date: 06/23/2016
=====================================================================================
DataEngine
    [195160]The browser will crash after running "DataEngine" sample in some case.

WebApiExplorer
    [194915]Suggest data matchs the template in sample.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20162.49)		Drop Date: 06/22/2016
=====================================================================================
WebApiExplorer
    [194446]Move down takes no effect and need to confirm the "Move do Beginning".

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20162.48)		Drop Date: 06/21/2016
=====================================================================================
WebApiExplorer
    [194438]Sugget to remain the page of storage after uploading file succesfully.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20162.47)		Drop Date: 06/17/2016
=====================================================================================
Excel
    [190011]Server side returns error message when it deal with spacing.
    [190020]User is unable to delete any text when it use Replace function of Excel Service.
    [190797]Operating ReadOnly excel takes no effect but returns successful message.
    [190816]Need to remove OneDriver function in excel sample.

All
    [193676]Selecting one parameter does not tick another parameter option in Core.

Report
    [193874]Some reports can't render in FlexViewerExplorer sample in Chrome.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20162.46)		Drop Date: 06/16/2016
=====================================================================================
OlapEngine
    Add c1OlapEngine sample.

Excel
    [190649]Can not split excel correctly if existing hidden sheet.
    [190774]The first data record cannot display correct data by correct format.
    [190884]The result cannot hide all related rows.
    [190868]Row height setting doesn't take effect on result.
    [190613]Excel Template doesn't work well in a certain case.
    [190817]It is better to change return value from "replaceCount" to "replacecellCount" of Replace Text.
    [190697]Excel Service can only merges the first data line in result.
    [189888]The feature "Split/Find/Replace/Add/Delete" of WebApi work correctly without license.
    [189931]Error displays after hide/unhide the rows/columns by using WebApi3.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20162.45)		Drop Date: 06/13/2016
=====================================================================================
Report
    [192894]Changing shifting from portrait to landscape does not show correctly in certain report.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20162.44)		Drop Date: 06/12/2016
=====================================================================================
All
    Rename C1.Web.Api to C1.AspNetCore.Api for ASP.NET Core platform.

Report
    [192112]Rename bounds to boundsList in result of report search api.
    [192113]Refactor the result structure of report parameter setting api.
    Change the expired date of Report service to 2017.3.15.
    [143673]Optimize parameters and clear action of reportcache controller.
    [142795]Can not search the text with some symbol.
    [143769]Optimize search and bookmark action of reportcache controller.

VSTemplate
    [192410]The information of "WebApiTemplate" isn't right.

Excel
    Implement Generate Excel From Given Template And Data.
    Implement split operation.
    Implement add/delete row/column operation.
    Implement find/replace operation.
    Implement Group/Ungroup column/row of excel file in storage.
    Implement Hide/Unhide column/row of excel file in storage.
    Implement Add posted Excel file to storage.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20161.43)		Drop Date: 06/02/2016
=====================================================================================
VSTemplate
    [191168]Compile error displays in "Program.cs" after rebuild the application which create by WebApi3 template.

WebApi3
    [191157]Error displays after importing excel to flexgrid by WebApi3.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20161.42)		Drop Date: 05/30/2016
=====================================================================================
All
    Upgrade to asp.net core RC2.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20161.41)		Drop Date: 05/06/2016
=====================================================================================
All
    Upgrade winform build to 147.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20161.40)		Drop Date: 05/04/2016
=====================================================================================
VSTemplate
    [153061]WebApi project needs phantomjs on JPN build.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20161.39)		Drop Date: 04/29/2016
=====================================================================================
All
    Upgrade winform build to 145.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20161.38)		Drop Date: 04/25/2016
=====================================================================================
WebApi3
    [148944]Missing "C1.Web.Api" reference in project.json file.

VS Template
    Don't add PhantomJS to the WebAPI application when using VS template.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20161.37)		Drop Date: 03/04/2016
=====================================================================================
All
    Upgrade winform build to 139.

Excel
    [147757]File Name cannot display correctly in generate excel file.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20161.36)		Drop Date: 02/27/2016
=====================================================================================
WebApiExplorer
    [145948]Issue of IIS deployment.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20161.35)		Drop Date: 02/22/2016
=====================================================================================
Excel
    [134452]Unlike previous buid, the column's width are too narrow when exporting from FlexGrid to excel.
    [145907]Warning displays in the "WebApiExplorer-->Views-->MVCFlexgrid-->Index.cshtml" sample.

Report
    [142860]Suggest to use report name when exporting to any type document.
    [142792]Highlight text does not match search text in some search result.

WebApiExplorer
    [145790]Error [The "FlexGridXlsxConverter.fromWorkbookOM" method is deprecated xxx] occurs in generating excel which is set "Type" as "JSON" configured on server and posted from client.
    [146144]Can't export flexgrid to excel by using "WebApi3" as service after updating the reference to JP.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20161.34)		Drop Date: 02/17/2016
=====================================================================================
Excel
    [142873]Javascript error is observe after importing an excel file in "Excel Service" sample page.
    [144010]The column header is exported to excel file after exporting FlexGrid to excel with setting "IncludeComlumnHeader" to false.

WebApiExplorer
    [144520]Request to change text "Select a report file" as bookmark.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20161.33)		Drop Date: 02/16/2016
=====================================================================================
Excel
    [145012]Exported excel file can't be exported and error has occurred after generating excel file from XML file available in storage and posted from client.
    [144010]The column header is exported to excel file after exporting FlexGrid to excel with setting "IncludeComlumnHeader" to false.

Report
    [144246]Fix the issues with some reports can't be loaded correctly to flexviewer.
    [143227]Sometimes Page Thumbnails doesn't work well.

WebApiExplorer
    [144520]Request to change text "Select a report file" as bookmark.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20161.32)		Drop Date: 02/06/2016
=====================================================================================
Report
    [144657]Web browser shows error message when user switch to anyone report.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20161.31)		Drop Date: 02/03/2016
=====================================================================================
Report
    Upgrade winform flexreport to 133.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20161.30)		Drop Date: 02/01/2016
=====================================================================================
Report
    Update C1.Web.Api.Report(remove reflection codes).

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20161.29)		Drop Date: 01/29/2016
=====================================================================================
All
    [140071]Do obfuscation on webapi project.

Report
    Implement the FlexReport WebAPI.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20153.27)		Drop Date: 12/3/2015
=====================================================================================
Core
    [139041]HTTP access error occurs for Excel Service.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20153.26)		Drop Date: 12/2/2015
=====================================================================================
    Add asp.net5 RC1 support.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20153.25)		Drop Date: 11/2/2015
=====================================================================================
Excel
    [133102]Exported excel is corrupted when merge an excel file which contains Hyperlinks with another excel file.
    [133140]Multiple issues are observed when import excel which contains DateTime format column.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20153.24)		Drop Date: 10/30/2015
=====================================================================================
Excel
    [135760]Some inconsistence behaviour are observed in generate excel when convert the file with various format using 'json' type.
    [136183]JS error will throw out after generating/merging xml/json format in get mode by using WebAPI3 sample as service in Chrome/FF.
    [135960]Javascript error occurs when exported the FlexGrid's grouping data in WebApiExplorer.
    [135756]Error occur when generate/merge excel using 'ExcelWithFormula.xls' file from 'OneDrive' with 'xml' type.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20153.23)		Drop Date: 10/28/2015
=====================================================================================
Excel
    [133880]WebApi cannot deal with NULL value from database when we set "Type" as "JSON" or "XML".
    [135711]Data cell's forecolor is same as its background.
    [135569]Error occur when convert to different format in Generate Excel of Get in some excel file which contain grouping.

Image
    [135199]"UnauthorizedAccessException" is thrown out when exporting image and CSV by IIS deployed WebAPI 3 service for second time in unfresh machine.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20153.22)		Drop Date: 10/22/2015
=====================================================================================
Excel
    [133966]The value contains "%%data type%%" after generating/merging excel to json/xml data.
    [134803]Fix generating DateTime type value in excel file.
    [131304]Exported image for flexchart display as blank when binding FlexChart with database.
    [133990]WebAPI lost attribute "VAlign" for some certain configurations in excel when we set "Type" as "JSON" or "XML".

Image
    [131371]One series is lost in exported image when exporting FlexChart which is set 'Stacked100%' in multiple Axes.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20153.21)		Drop Date: 10/19/2015
=====================================================================================
Excel
    [134224]In merge api, sheet name is wrong in the formula when name is repeated and contains special char.
    [133017]Multiple issue are observed when import the XML Data file which has an one row data
    [133016]ArgumentException is observe after generate to xml and json type when generate excel file from JSON data that contain null value
    [133982]WebApi shows error message after merging/generating excel which contains merged cell to Json/Xml type in some case.
    [134696]Update the implementation of size(including fontsize, width, height) according to the common json format

JP VSTemplate
    [131482] Unlike previous build 18, 'C1.Web.Api' is observed instead of 'C1.Web.Api.ja' in the 'project.json' file athough installed the 'JPN' template in JP enviornment.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20153.20)		Drop Date: 10/10/2015
=====================================================================================
Excel
    Implement excel merging service.
    Implement excel generating service(no template support).
    [131448]Fix the issue that special excel file can't be imported into flexsheet.

Barcode service
    [127866]Implement barcode service.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.19)		Drop Date: 09/16/2015
=====================================================================================
Excel
    [124173][WebAPI-ExcelImpor-Date] In IE11 and FireFox, the date value cannot be imported to FlexGrid.
All
    Add asp.net5 beta7 support.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.18)		Drop Date: 07/10/2015
=====================================================================================
Excel
    Update c1excel from 66 to 67 version.

WebApiExplorer
    [124062]Need remove column's visible settings in "Remote Data Bind" page of MVCFlexGrid.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.17)		Drop Date: 07/09/2015
=====================================================================================
WebApiExplorer
    Update the data for flexGrid in order to make the value of downloads lower than 1000.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.16)		Drop Date: 07/09/2015
=====================================================================================
WebApiExplorer
    [123769]The Country column is too narrow in some pages of Wijmo5FlexGrid.
    [123713]bug-[WebAPI-Sample]The value of  "Label Position" should be "Top" in "Wijmo5FlexChart-->Labels" sample.

Excel
    [123726][WebAPI-ExcelExport-FlexGrid] The exported font size is not correct if FlexGrid's content is exported to excel file.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.15)		Drop Date: 07/07/2015
=====================================================================================
Image
    [121569][WebAPI-ImageExport-FlexChart]Can't export the chart to image file after binding new fields to axisX by using Bind(string bindingFields, string readActionUrl).

Excel
    [122696][WebAPI-ExcelExport-Format] Format with precision settings cannot be exported to excel file.
    Change to do the "px to pt" conversion in ExcelConverter.js and fix font size inconsistency.
    Fix Date change issue when importing.
    [123611][WebAPI-ExcelImport] Cannot import excel file whose name has JP/CN character.
    [123614][WebAPI-ExcelImport-Filter] Hidden row also can be imported if excel file with filter settings is imported to FlexGrid.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.14)		Drop Date: 07/03/2015
=====================================================================================
    None, just a version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.13)		Drop Date: 07/03/2015
=====================================================================================
    None, just a version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.12)		Drop Date: 07/03/2015
=====================================================================================
Excel
    [120519][WebAPI-ExcelExport-FlexGrid] JS error occurs when exporting Footer Grid.
    [122718][WebAPI-ExcelExport-NumberValue] In the exported excel file, the cell with number type's value has green icon at the top-left corner.
    [123328]Update JP resource for vs template extension.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.11)		Drop Date: 07/03/2015
=====================================================================================
Excel
    Handle CSS fontFamily string.
    Fix an issue that empty rows at the end of a sheet lead to wrong format parsing when importing.
    Enhance Date serialization.
    [WebApi][WebApiExplorer][FlexGrid] The exported excel file currency signs are display incorrectly when exporting the FlexGrid.

WebApiExplorer
    [123019]bug-[WebAPI-Sample] After reducing the width of browser, the triangle icon displays outside of the drop-down list which is used to choose sample.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.10)		Drop Date: 07/02/2015
=====================================================================================
    None, just a version upgrade.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.9)		Drop Date: 07/01/2015
=====================================================================================
VS Template
    Update Wizard project to create PhantomJS link/copy.

Core
    [122204][WebAPI-License-Selfhost] Some issues occurs when exporting/importing by self-host WebAPI service with expired/invalid/no license.
    [122214][WebAPI-Expired/InvalidLicense-IIS] "Invalid character" js error occurs when importing the file to FlexGrid by WebAPI service with expired/invalid license.

WebApiExplorer
    [122866]bug-[WebAPI-Sample]There are three issues about WebApi Sample.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.8)		Drop Date: 06/30/2015
=====================================================================================
Excel
    [121854][WebAPI][FlexGrid] "OnlyCurrentPage" option is not working when "DisableServerRead" is set to true in FlexGrid.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.7)		Drop Date: 06/30/2015
=====================================================================================
VS Template
    Fix PhantomJS links in VS templates for going back to online NuGet packages.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.6)		Drop Date: 06/28/2015
=====================================================================================
Core
    Remove package dependency of PhantomJS in ASP.NET 5.
    [122153]bug-[WebAPI-JP-Resource] JP resource need be added to WebAPI JP build.
    [122173]bug-[WebAPI-License] The exported "Evaluation version" info should be aligned to left in the exported excel file.
    [122144]The license exception messages for Jp version in ASP.NET 5.
    [121088]bug-[WebAPI-ExcelExport-VS2015] C1Excel license dialog shows when exporting excel file.
    [120505]bug-[WebAPI-ExcelExport-FlexGrid] When export FlexGrid with new row to excel file, redundant row with column name is exported.

VS Template
    [116749]Web port of projects is same if creating multiple projects by the same WebApi VS template.

Image
    Fix the chart export issue with date axis.

Excel
    [121507][WebAPI-FlexGrid-Style] FlexGrid cannot be exported to excel file correctly after exporting the imported excel file.
    [121981][WebAPI-ExcelImport-FlexGrid] Data disappears after scrolling operation on FlexGrid with imported data.
    [121509][WebAPI-ExcelImport-FlexGrid]RowHeight is not correct after importing operation.
    [121516][WebAPI-FlexGrid-DataType]Imported/exported data type is lost in FlexGrid.
    [120999][WebAPI][WebApiExplorer][FlexGrid]The value in filter dialog and column are mismatch after "Export" button is click in "Remote DataBind"
    [120512][WebAPI-ExcelExport-FlexGrid] Column-width with "*" cannot be exported correctly.
    [119975][WebAPI-ExcelExport-FlexSheet] Font settings of FlexSheet cannot be exported to excel file.
    [120409][WebAPI-FlexSheet-Style] FlexSheet's style cannot be exported/imported.

WebApiExplorer
    [121443]Add import option to the Mvc FlexGrid in WebApiExplorer sample.
    [121456][WebAPI-Import-Sample] In WebApiExplorer sample, same file name cannot be imported to FlexGrid continuously.
    [121553][WebAPI-ExcelExport-Sample] Need add menu to select exporting file type for exporting.
    [121002][WebAPI][WebApiExplorer][LinearGuage]The exported image is display incorrectly for Linear guage when setting "Direction" to "Up" or "Down".
    [120993][WebAPI][WebApiExplorer]Javascipt Error is observe after clicking "Export" button in "Filtering" sample page in FlexGrid
    [120953][WebAPI][WebApiExplorer][FlexGrid]Unlike Wijmo5MVCExplorer, the rows count is reset to "500" after sorting a column in FlexGrid although user select only "5"

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.5)		Drop Date: 06/24/2015
=====================================================================================
Excel
    [121534][WebAPI-ExcelExport-FlexGrid-Style] In FlexGrid, group's style settings cannot be exported to excel file.

WebApiExplorer
    [121803]bug-[WebAPI-Sample] Exporter class of MVC's FlexGrid need be changed.
    [120860]Update the "Conditional Styling" sample to avoid the wijmo5 issue.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.4)		Drop Date: 06/23/2015
=====================================================================================
Excel
    Add licensing to Excel import.
    [120995][WebAPI-SelfHost-VS2015] Cannot use self-host WebAPI service of VS2015 to export/import file.

License
    [119590] License in Web API of ASP.NET 5
    [119589] License in Web API 2.2

Client
    Refactor ExcelExport.js.

WebApiExplorer
    [120729][WebAPI-ImageExport-BulletGraph]The range of BulletGraph disappears in the exported image file.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.3)		Drop Date: 06/18/2015
=====================================================================================
Core
    [121218]Fix the issue that Selenium can't be restored correctly.

Image
    Overwrite wijmo.copy() to avoid potential property conflicts.

Excel
    [120483][WebAPI-ExcelImport-FlexGrid] An error is thrown when import collapsed group data.
    [121043][WebAPI-IIS-Import-VS2015] Excel file cannot be imported to FlexGrid when using deployed VS2015 WebAPI on IIS.

WebApiExplorer
    [120852]Remove "Financial Chart" sample from WebApiExplorer.
    [120875]Remove "Merge Cells" sample from WebApiExplorer.
    [120858]Update the webapi sample, to resolve the warning.
    [120838]Update the VirtualScrolling sample, the "PageSize" property shouldn't be setting to "10".

VSTemplate
    Add CORS support for ASP.NET 5.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.2)		Drop Date: 06/15/2015
=====================================================================================
Image
    [120431][WebAPI-ImageExport-FlexChart]Can't export the chart to image file after selecting the chart by series/points mode.

Client
    Refactor the WebApiClient.js.

=====================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition (4.0.20152.1)		Drop Date: 06/10/2015
=====================================================================================
    WebApi first drop.

==================================================================================
ComponentOne Studio for ASP.NET WebAPI Edition	GrapeCity, Inc - www.componentone.com
==================================================================================

A set of modern UI controls built upon latest cutting edge technologies like HTML5, CSS, ECMA5 without making compromises to support legacy browsers. Inside ComponentOne ASP.NET MVC Edition you will find fast and lightweight controls ranging from data management to data visualization, project templates, and professionally designed themes. Shift you site into overdrive with ComponentOne ASP.NET MVC Edition.
