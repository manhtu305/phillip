@echo off
setlocal enabledelayedexpansion

set oldver=.0.20171.93
set newver=.0.20171.94
set c1WinOldver=4.0.20171.260
set c1WinNewver=4.0.20172.263

set tfPath="C:\Program Files (x86)\Microsoft Visual Studio 11.0\Common7\IDE\tf.exe"

@if '%1'=='' (
    if exist %tfPath% %tfPath% edit "%~dpnx0"
)

@if not '%1'=='' (
    @echo process: %1
    if exist %tfPath% %tfPath% edit %1
    if exist %1 attrib -R -S -H %1
    %~dp0replace.vbs %1 %oldver% %newver%
    %~dp0replace.vbs %1 %c1WinOldver% %c1WinNewver%
    goto :EOF
)

call :createScript

forfiles /p C1.AspNetCore.Api\JPPackageBuilder /s /m updatePackage.cmd /c "cmd /c %0 @path"
forfiles /p ..\Shared\Libs /s /m *.nuspec /c "cmd /c %0 @path"
forfiles /p C1.AspNetCore.Api\src\C1.AspNetCore.Api\Properties /s /m AssemblyInfo.cs /c "cmd /c %0 @path"
forfiles /p C1.AspNetCore.Api /s /m project.json /c "cmd /c %0 @path"
forfiles /p C1.Web.Api /s /m C1.Web.Api*.nuspec /c "cmd /c %0 @path"

forfiles /p Samples /s /m packages.config /c "cmd /c %0 @path"
forfiles /p Samples\CS /s /m project.json /c "cmd /c %0 @path"
forfiles /p Samples\CS /s /m *.csproj /c "cmd /c %0 @path"
forfiles /p Samples\VB /s /m *.vbproj /c "cmd /c %0 @path"
forfiles /p Samples\CS\WebApiExplorer\WebApiExplorer /s /m Web.config /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\CS\ASPNETCore\FlexSheetExplorer\FlexSheetExplorer /s /m appsettings.json /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\CS\FlexSheetExplorer\FlexSheetExplorer /s /m Web.config /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\CS\FlexViewerExplorer\FlexViewerExplorer /s /m FlexViewerExplorer.csproj /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\CS\FlexViewerExplorer\FlexViewerExplorer /s /m packages.config /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\CS\ASPNETCore\FlexViewerExplorer\src\FlexViewerExplorer /s /m FlexViewerExplorer.csproj /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\CS\ASPNETCore\FlexViewerExplorer\src\FlexViewerExplorer /s /m project.json /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\CS\OlapExplorer\OlapExplorer /s /m OlapExplorer.csproj /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\CS\OlapExplorer\OlapExplorer /s /m packages.config /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\CS\ASPNETCore\OlapExplorer\src\OlapExplorer /s /m OlapExplorer.csproj /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\CS\ASPNETCore\OlapExplorer\src\OlapExplorer /s /m project.json /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\CS\HowTo\PdfViewer\PdfViewer /s /m PdfViewer.csproj /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\CS\HowTo\PdfViewer\PdfViewer /s /m packages.config /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\CS\HowTo\ReportViewer\CustomReportProvider\CustomReportProvider /s /m CustomReportProvider.csproj /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\CS\HowTo\ReportViewer\CustomReportProvider\CustomReportProvider /s /m packages.config /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\CS\HowTo\ReportViewer\ReportViewer101\ReportViewer101\Controllers /s /m HomeController.cs /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\CS\HowTo\OLAP\Olap101\Olap101 /s /m Olap101.csproj /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\CS\HowTo\OLAP\Olap101\Olap101 /s /m packages.config /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\VB\HowTo\PdfViewer\PdfViewer /s /m PdfViewer.vbproj /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\VB\HowTo\PdfViewer\PdfViewer /s /m packages.config /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\VB\HowTo\OLAP\Olap101 /s /m packages.config /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\VB\HowTo\OLAP\Olap101 /s /m Olap101.vbproj /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\VB\HowTo\ReportViewer\CustomReportProvider\CustomReportProvider /s /m CustomReportProvider.vbproj /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\Samples\VB\HowTo\ReportViewer\CustomReportProvider\CustomReportProvider /s /m packages.config /c "cmd /c %0 @path"

forfiles /p ProjectTemplates /s /m source.extension.vsixmanifest /c "cmd /c %0 @path"
forfiles /p ProjectTemplates /s /m *Template.csproj /c "cmd /c %0 @path"
forfiles /p ProjectTemplates /s /m *Template.vbproj /c "cmd /c %0 @path"
forfiles /p ProjectTemplates /s /m project_json /c "cmd /c %0 @path"
forfiles /p ProjectTemplates /s /m packages_config /c "cmd /c %0 @path"
forfiles /p ProjectTemplates /s /m AssemblyInfo.cs /c "cmd /c %0 @path"
forfiles /p ProjectTemplates /s /m *.vstemplate /c "cmd /c %0 @path"
forfiles /p ProjectTemplates /s /m *.csproj /c "cmd /c %0 @path"

forfiles /p ..\Wijmo5Mvc\ItemTemplates /s /m *.vstemplate /c "cmd /c %0 @path"
forfiles /p ..\Wijmo5Mvc\ItemTemplates /s /m AssemblyInfo.cs /c "cmd /c %0 @path"

cmd /c %0 Zipper.xml
cmd /c %0 Zipper_JPN.xml

if exist replace.vbs del /F /Q replace.vbs
@echo done.
pause
goto :EOF


:createScript
    if exist replace.vbs attrib -R -S -H replace.vbs
    if exist replace.vbs del /F /Q replace.vbs
    @echo filePath = wscript.arguments(0)                           > replace.vbs
    @echo oldver = wscript.arguments(1)                             >> replace.vbs
    @echo newver = wscript.arguments(2)                             >> replace.vbs
    @echo set fso = CreateObject("Scripting.FileSystemObject")      >> replace.vbs
    @echo Set readerStream = CreateObject("Adodb.Stream")           >> replace.vbs
    @echo readerStream.Type = 2                                     >> replace.vbs
    @echo readerStream.charset = "utf-8"                            >> replace.vbs
    @echo readerStream.Open                                         >> replace.vbs
    @echo readerStream.LoadFromFile filePath                        >> replace.vbs
    @echo text = readerStream.readtext                              >> replace.vbs
    @echo readerStream.close                                        >> replace.vbs
    @echo If InStr(text, oldver) ^>0 Then                           >> replace.vbs
    @echo     text = Replace(text, oldver, newver, 1, -1, 0)        >> replace.vbs
    @echo     fso.getfile(filePath).attributes = 0                  >> replace.vbs
    @echo     fso.deleteFile(filePath)                              >> replace.vbs
    @echo     Set writerStream = CreateObject("Adodb.Stream")       >> replace.vbs
    @echo     writerStream.Type = 2                                 >> replace.vbs
    @echo     writerStream.charset = "utf-8"                        >> replace.vbs
    @echo     writerStream.Open                                     >> replace.vbs
    @echo     writerStream.WriteText text                           >> replace.vbs
    @echo     writerStream.SaveToFile filePath                      >> replace.vbs
    @echo     writerStream.close                                    >> replace.vbs
    @echo End If                                                    >> replace.vbs
    @echo set fso = Nothing                                         >> replace.vbs
goto :EOF