﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;


// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace WebAPIExplorer
{
    public sealed partial class UCMenu : UserControl
    {
        public UCMenu()
        {
            this.InitializeComponent();
        }

        public SplitView SplitView { get; set; }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            SplitView.IsPaneOpen = !SplitView.IsPaneOpen;
        }

        private void BtnSMenuGenerateExcelText_Click(object sender, RoutedEventArgs e)
        {
            ((Frame)Window.Current.Content).Navigate(typeof(GenerateExcelPage));
        }

        private void BtnSMenuMergeExcelText_Click(object sender, RoutedEventArgs e)
        {
            ((Frame)Window.Current.Content).Navigate(typeof(MergeExcelPage));
        }

        private void BtnSMenuGenbarcodeText_Click(object sender, RoutedEventArgs e)
        {
            ((Frame)Window.Current.Content).Navigate(typeof(GenerateBarcodePage));
        }

        private void BtnMainLogo_Click(object sender, RoutedEventArgs e)
        {
            ((Frame)Window.Current.Content).Navigate(typeof(MainPage));
        }

        private void StkPnlExcelLink_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (StkPnlExcelContent.Visibility == Visibility.Visible)
            {                
                ImgMenuExcel.Source = new BitmapImage(new Uri(this.BaseUri, "/Assets/icon_Collapsed.png"));
                StkPnlExcelContent.Visibility = Visibility.Collapsed;
            }
            else //if (StkPnlExcelContent.Visibility == Visibility.Collapsed)
            {
                ImgMenuExcel.Source = new BitmapImage(new Uri(this.BaseUri, "/Assets/icon_Expanded.png"));
                StkPnlExcelContent.Visibility = Visibility.Visible;
            }
        }

        private void StkPnlBarcodeLink_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (StkPnlBarcodeContent.Visibility == Visibility.Visible)
            {
                ImgMenuBarcode.Source = new BitmapImage(new Uri(this.BaseUri, "/Assets/icon_Collapsed.png"));
                StkPnlBarcodeContent.Visibility = Visibility.Collapsed;
            }
            else //if (StkPnlBarcodeContent.Visibility == Visibility.Collapsed)
            {
                ImgMenuBarcode.Source = new BitmapImage(new Uri(this.BaseUri, "/Assets/icon_Expanded.png"));
                StkPnlBarcodeContent.Visibility = Visibility.Visible;
            }

        }
    }
}
