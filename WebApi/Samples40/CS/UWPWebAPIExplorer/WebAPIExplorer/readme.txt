﻿ASP.NET WebAPI UWPWebAPIExplorer sample
--------------------------------------------------------------------------
Explore the WebAPI features offered by ComponentOne WebAPI services to Generate Excel files, Merge Excel files and Generate Barcode. 

This sample application explores how to generate barcode from various range of barcode types of C1WebAPI.
This application explores the usage of C1WebAPI services which provides GET and POST methods to 
Generate Excel files from dataset\collection, from XML, from different formats, from JSON.
This sample also describes how to merge two excel files by using C1WebAPI services.
