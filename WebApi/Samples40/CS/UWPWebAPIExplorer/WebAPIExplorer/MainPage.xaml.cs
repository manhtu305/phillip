﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using WebAPIExplorer.Data;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Foundation;
using Windows.Networking.BackgroundTransfer;
using Windows.Storage;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace WebAPIExplorer
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private Manage Mng = new Manage();
        public MainPage()
        {
            this.InitializeComponent();
            SetHomePage();            
        }       

        private void SetHomePage()
        {
            TxtBlkHomeBodyText.Text = Mng.ResLoader.GetString("HomeBodyText");
            BtnOnlineHelp.Content = Mng.ResLoader.GetString("BtnOnlineHelp");
        }        

        private void BtnOnlineHelp_Click(object sender, RoutedEventArgs e)
        {
            LaunchOnlineHelp();
        }

        async void LaunchOnlineHelp()
        {            
            string uriToLaunch = @"http://help.grapecity.com/componentone/NetHelp/c1webapi/webframe.html";
            var uri = new Uri(uriToLaunch);

            var success = await Windows.System.Launcher.LaunchUriAsync(uri);

            if (success)
            {
                // URI launched
            }
            else
            {
                // URI launch failed
            }
        }

    }
}
