﻿using System;
using System.IO;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using WebAPIExplorer.Data;
using System.Net.Http;
using System.Threading.Tasks;
using Windows.Storage.Pickers;
using System.Collections.Generic;
using Windows.Storage;
using Windows.Storage.Provider;
using System.Reflection;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace WebAPIExplorer
{
    /// <summary>
    /// Generate Excel page 
    /// </summary>
    public sealed partial class GenerateExcelPage : Page
    {
        private string GenExcelPOSTXMLFile = null, GenExcelPOSTConvertFormatFile = null;
        Manage MngObj = new Manage();

        public GenerateExcelPage()
        {
            this.InitializeComponent();
            SetGenerateExcelPage();
        }

        private void SetGenerateExcelPage()
        {
            TxtBlkGenExcelTitle.Text = MngObj.ResLoader.GetString("GenExcelTitle");
            SetGenExcelGETData();
            SetGenExcelGETXML();
            SetGenExcelGETConvertFormat();
            SetGenExcelPOSTXML();
            SetGenExcelPOSTJSON();
            SetGenExcelPOSTConvertFormat();
        }

        private void SetGenExcelGETData()
        {
            TxtBlkGenExcelGETDataGETPOSTText.Text = MngObj.ResLoader.GetString("GenExcelGETDataGETPOSTText");
            TxtBlkGenExcelGETDataDesc.Text = MngObj.ResLoader.GetString("DescHeader");
            TxtBlkGenExcelGETDataDescDetails.Text = MngObj.ResLoader.GetString("GenExcelGETDataDescDetails");
            BtnGenExcelGETData.Content = MngObj.ResLoader.GetString("BtnGenExcel");

            TxtBlkGenExcelGETDataParamsHeader.Text = MngObj.ResLoader.GetString("ParamHeader");
            TxtBlkGenExcelGETDataFileName.Text = "FileName";
            TxtBlkGenExcelGETDataFileNameDesc.Text = MngObj.ResLoader.GetString("GenExcelGETDataFileNameDesc");
            TxtBlkGenExcelGETDataType.Text = "Type";
            CmbGenExcelGETDataType.DisplayMemberPath = "Text";
            CmbGenExcelGETDataType.SelectedValuePath = "Value";
            CmbGenExcelGETDataType.ItemsSource = MngObj.GetExcel_TypeDDList();
            CmbGenExcelGETDataType.SelectedValue = "xlsx";
            TxtBlkGenExcelGETDataTypeDesc.Text = MngObj.ResLoader.GetString("GenExcelGETDataTypeDesc");
            TxtBlkGenExcelGETDataDataName.Text = "DataName";
            CmbGenExcelGETDataDataName.DisplayMemberPath = "Text";
            CmbGenExcelGETDataDataName.SelectedValuePath = "Value";
            CmbGenExcelGETDataDataName.ItemsSource = MngObj.GetExcel_DataNameDDList();
            CmbGenExcelGETDataDataName.SelectedIndex = 0;
            TxtBlkGenExcelGETDataDataNameDesc.Text = MngObj.ResLoader.GetString("GenExcelGETDataDataNameDesc");

            //Response Schema
            TxtBlkGenExcelGETDataResponseSchemaHeader.Text = MngObj.ResLoader.GetString("ResponseSchemaHeader");
            TxtBoxGenExcelGETDataResponseSchema.Text = MngObj.GetResponseSchema();

            //Result
            TxtBlkGenExcelGETDataResultRequestUrlTitle.Text = MngObj.ResLoader.GetString("ResultRequestUrlTitle");
            TxtBlkGenExcelGETDataResultJsonTitle.Text = MngObj.ResLoader.GetString("ResultJsonTitle");
            TxtBlkGenExcelGETDataResultxmlTitle.Text = MngObj.ResLoader.GetString("ResultxmlTitle");
        }

        private async void BtnGenExcelGETData_Click(object sender, RoutedEventArgs e)
        {
            TxtBlkGenExcelGETDataResultMsg.Text = "";
            string FullURL = App.APIBaseUri + "api/excel?";
            FullURL += "FileName=" + TxtBoxGenExcelGETDataFileName.Text.Trim();
            FullURL += "&Type=" + CmbGenExcelGETDataType.SelectedValue.ToString();
            if (CmbGenExcelGETDataDataName.SelectedIndex > 0)
                FullURL += "&DataName=" + CmbGenExcelGETDataDataName.SelectedValue.ToString();

            TxtBlkGenExcelGETDataResultRequestUrl.Text = FullURL;
            StkPnlGenExcelGETDataResultJson.Visibility = Visibility.Collapsed;
            StkPnlGenExcelGETDataResultxml.Visibility = Visibility.Collapsed;

            using (var client = new HttpClient())
            {
                var response = client.GetAsync(FullURL).Result;
                if (!response.IsSuccessStatusCode)
                {
                    TxtBlkGenExcelGETDataResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgInvalidResponse");
                }
                else if (CmbGenExcelGETDataType.SelectedValue.ToString() != "json" && CmbGenExcelGETDataType.SelectedValue.ToString() != "xml")
                {
                    //download excel file
                    FileSavePicker savePicker = new FileSavePicker();
                    savePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
                    savePicker.FileTypeChoices.Add("Excel File", new List<string>() { "." + CmbGenExcelGETDataType.SelectedValue.ToString() });
                    savePicker.SuggestedFileName = string.IsNullOrEmpty(TxtBoxGenExcelGETDataFileName.Text) ? "excel" : TxtBoxGenExcelGETDataFileName.Text.Trim();
                    StorageFile file = await savePicker.PickSaveFileAsync();
                    if (file != null)
                    {
                        CachedFileManager.DeferUpdates(file);
                        await FileIO.WriteBytesAsync(file, await response.Content.ReadAsByteArrayAsync());
                        FileUpdateStatus status = await CachedFileManager.CompleteUpdatesAsync(file);
                        if (status == FileUpdateStatus.Complete)
                            TxtBlkGenExcelGETDataResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgFileLocation") + file.Path;
                        else
                            TxtBlkGenExcelGETDataResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgFileNotDownloaded");
                    }
                }
                else
                {
                    if (CmbGenExcelGETDataType.SelectedValue.ToString() == "json")
                    {
                        string ResultList = response.Content.ReadAsStringAsync().Result;
                        TxtBoxGenExcelGETDataResultJson.Text = response.Content.ReadAsStringAsync().Result.ToString();
                        StkPnlGenExcelGETDataResultJson.Visibility = Visibility.Visible;
                    }
                    else if (CmbGenExcelGETDataType.SelectedValue.ToString() == "xml")
                    {
                        TxtBoxGenExcelGETDataResultxml.Text = response.Content.ReadAsStringAsync().Result.ToString();
                        StkPnlGenExcelGETDataResultxml.Visibility = Visibility.Visible;
                    }
                }
            }
            StkPnlGenExcelGETDataResult.Visibility = Visibility.Visible;
        }

        private void StkPnlGenExcelGET1_Link_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (StkPnlGenExcelGET1_Content.Visibility == Visibility.Collapsed)
                StkPnlGenExcelGET1_Content.Visibility = Visibility.Visible;
            else
                StkPnlGenExcelGET1_Content.Visibility = Visibility.Collapsed;
        }

        private void SetGenExcelGETXML()
        {
            TxtBlkGenExcelGETXMLGETPOSTText.Text = MngObj.ResLoader.GetString("GenExcelGETXMLGETPOSTText");
            TxtBlkGenExcelGETXMLDesc.Text = MngObj.ResLoader.GetString("DescHeader");
            TxtBlkGenExcelGETXMLDescDetails.Text = MngObj.ResLoader.GetString("GenExcelGETXMLDescDetails");
            BtnGenExcelGETXML.Content = MngObj.ResLoader.GetString("BtnGenExcel");

            TxtBlkGenExcelGETXMLParamsHeader.Text = MngObj.ResLoader.GetString("ParamHeader");
            TxtBlkGenExcelGETXMLFileName.Text = "FileName";
            TxtBlkGenExcelGETXMLFileNameDesc.Text = MngObj.ResLoader.GetString("GenExcelGETXMLFileNameDesc");
            TxtBlkGenExcelGETXMLType.Text = "Type";
            CmbGenExcelGETXMLType.DisplayMemberPath = "Text";
            CmbGenExcelGETXMLType.SelectedValuePath = "Value";
            CmbGenExcelGETXMLType.ItemsSource = MngObj.GetExcel_TypeDDList();
            CmbGenExcelGETXMLType.SelectedValue = "xlsx";
            TxtBlkGenExcelGETXMLTypeDesc.Text = MngObj.ResLoader.GetString("GenExcelGETXMLTypeDesc");
            TxtBlkGenExcelGETXMLDataFileName.Text = "DataFileName";
            CmbGenExcelGETXMLDataFileName.DisplayMemberPath = "Text";
            CmbGenExcelGETXMLDataFileName.SelectedValuePath = "Value";
            CmbGenExcelGETXMLDataFileName.ItemsSource = MngObj.GetExcel_DataFileNameDDList();
            CmbGenExcelGETXMLDataFileName.SelectedIndex = 0;
            TxtBlkGenExcelGETXMLDataFileNameDesc.Text = MngObj.ResLoader.GetString("GenExcelGETXMLDataFileNameDesc");
            TxtBlkGenExcelGETXMLDataFileNameDesc2.Text = "<orders>";
            TxtBlkGenExcelGETXMLDataFileNameDesc2.Text += "\n    <order id=\"1\">";
            TxtBlkGenExcelGETXMLDataFileNameDesc2.Text += "\n        <price>1000</price>";
            TxtBlkGenExcelGETXMLDataFileNameDesc2.Text += "\n   </order>";
            TxtBlkGenExcelGETXMLDataFileNameDesc2.Text += "\n   ......";
            TxtBlkGenExcelGETXMLDataFileNameDesc2.Text += "\n</ orders > ";

            //Response Schema
            TxtBlkGenExcelGETXMLResponseSchemaHeader.Text = MngObj.ResLoader.GetString("ResponseSchemaHeader");
            TxtBoxGenExcelGETXMLResponseSchema.Text = MngObj.GetResponseSchema();

            //Result
            TxtBlkGenExcelGETXMLResultRequestUrlTitle.Text = MngObj.ResLoader.GetString("ResultRequestUrlTitle");
            TxtBlkGenExcelGETXMLResultJsonTitle.Text = MngObj.ResLoader.GetString("ResultJsonTitle");
            TxtBlkGenExcelGETXMLResultxmlTitle.Text = MngObj.ResLoader.GetString("ResultxmlTitle");
        }

        private async void BtnGenExcelGETXML_Click(object sender, RoutedEventArgs e)
        {
            TxtBlkGenExcelGETXMLResultMsg.Text = "";
            string FullURL = App.APIBaseUri + "api/excel?";
            FullURL += "FileName=" + TxtBoxGenExcelGETXMLFileName.Text.Trim();
            FullURL += "&Type=" + CmbGenExcelGETXMLType.SelectedValue.ToString();
            if (CmbGenExcelGETXMLDataFileName.SelectedIndex > 0)
                FullURL += "&DataFileName=" + CmbGenExcelGETXMLDataFileName.SelectedValue.ToString();

            TxtBlkGenExcelGETXMLResultRequestUrl.Text = FullURL;
            StkPnlGenExcelGETXMLResultJson.Visibility = Visibility.Collapsed;
            StkPnlGenExcelGETXMLResultxml.Visibility = Visibility.Collapsed;

            using (var client = new HttpClient())
            {
                var response = client.GetAsync(FullURL).Result;
                if (!response.IsSuccessStatusCode)
                {
                    TxtBlkGenExcelGETXMLResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgInvalidResponse");
                }
                else if (CmbGenExcelGETXMLType.SelectedValue.ToString() != "json" && CmbGenExcelGETXMLType.SelectedValue.ToString() != "xml")
                {
                    //download excel file
                    FileSavePicker savePicker = new FileSavePicker();
                    savePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
                    savePicker.FileTypeChoices.Add("Excel File", new List<string>() { "." + CmbGenExcelGETXMLType.SelectedValue.ToString() });
                    savePicker.SuggestedFileName = string.IsNullOrEmpty(TxtBoxGenExcelGETXMLFileName.Text) ? "excel" : TxtBoxGenExcelGETXMLFileName.Text.Trim();
                    StorageFile file = await savePicker.PickSaveFileAsync();
                    if (file != null)
                    {
                        CachedFileManager.DeferUpdates(file);
                        await FileIO.WriteBytesAsync(file, await response.Content.ReadAsByteArrayAsync());
                        FileUpdateStatus status = await CachedFileManager.CompleteUpdatesAsync(file);
                        if (status == FileUpdateStatus.Complete)
                            TxtBlkGenExcelGETXMLResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgFileLocation") + file.Path;
                        else
                            TxtBlkGenExcelGETXMLResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgFileNotDownloaded");
                    }
                }
                else
                {
                    if (CmbGenExcelGETXMLType.SelectedValue.ToString() == "json")
                    {
                        string ResultList = response.Content.ReadAsStringAsync().Result;
                        TxtBoxGenExcelGETXMLResultJson.Text = response.Content.ReadAsStringAsync().Result.ToString();
                        StkPnlGenExcelGETXMLResultJson.Visibility = Visibility.Visible;
                    }
                    else if (CmbGenExcelGETXMLType.SelectedValue.ToString() == "xml")
                    {
                        TxtBoxGenExcelGETXMLResultxml.Text = response.Content.ReadAsStringAsync().Result.ToString();
                        StkPnlGenExcelGETXMLResultxml.Visibility = Visibility.Visible;
                    }
                }

            }

            StkPnlGenExcelGETXMLResult.Visibility = Visibility.Visible;
        }

        private void StkPnlGenExcelGET2_Link_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (StkPnlGenExcelGET2_Content.Visibility == Visibility.Collapsed)
                StkPnlGenExcelGET2_Content.Visibility = Visibility.Visible;
            else
                StkPnlGenExcelGET2_Content.Visibility = Visibility.Collapsed;
        }

        private void SetGenExcelGETConvertFormat()
        {
            TxtBlkGenExcelGETConvertFormatGETPOSTText.Text = MngObj.ResLoader.GetString("GenExcelGETConvertFormatGETPOSTText");
            TxtBlkGenExcelGETConvertFormatDesc.Text = MngObj.ResLoader.GetString("DescHeader");
            TxtBlkGenExcelGETConvertFormatDescDetails.Text = MngObj.ResLoader.GetString("GenExcelGETConvertFormatDescDetails");
            BtnGenExcelGETConvertFormat.Content = MngObj.ResLoader.GetString("BtnGenExcel");

            TxtBlkGenExcelGETConvertFormatParamsHeader.Text = MngObj.ResLoader.GetString("ParamHeader");
            TxtBlkGenExcelGETConvertFormatFileName.Text = "FileName";
            TxtBlkGenExcelGETConvertFormatFileNameDesc.Text = MngObj.ResLoader.GetString("GenExcelGETConvertFormatFileNameDesc");
            TxtBlkGenExcelGETConvertFormatType.Text = "Type";
            CmbGenExcelGETConvertFormatType.DisplayMemberPath = "Text";
            CmbGenExcelGETConvertFormatType.SelectedValuePath = "Value";
            CmbGenExcelGETConvertFormatType.ItemsSource = MngObj.GetExcel_TypeDDList();
            CmbGenExcelGETConvertFormatType.SelectedValue = "xlsx";
            TxtBlkGenExcelGETConvertFormatTypeDesc.Text = MngObj.ResLoader.GetString("GenExcelGETConvertFormatTypeDesc");
            TxtBlkGenExcelGETConvertFormatWorkBookFileName.Text = "WorkBookFileName";
            CmbGenExcelGETConvertFormatWorkBookFileName.DisplayMemberPath = "Text";
            CmbGenExcelGETConvertFormatWorkBookFileName.SelectedValuePath = "Value";
            CmbGenExcelGETConvertFormatWorkBookFileName.ItemsSource = MngObj.GetExcel_WorkBookFileNameDDList();
            CmbGenExcelGETConvertFormatWorkBookFileName.SelectedIndex = 0;
            TxtBlkGenExcelGETConvertFormatWorkBookFileNameDesc.Text = MngObj.ResLoader.GetString("GenExcelGETConvertFormatWorkBookFileNameDesc");

            //Response Schema            
            TxtBlkGenExcelGETConvertFormatResponseSchemaHeader.Text = MngObj.ResLoader.GetString("ResponseSchemaHeader");
            TxtBoxGenExcelGETConvertFormatResponseSchema.Text = MngObj.GetResponseSchema();

            //Result
            TxtBlkGenExcelGETConvertFormatResultRequestUrlTitle.Text = MngObj.ResLoader.GetString("ResultRequestUrlTitle");
            TxtBlkGenExcelGETConvertFormatResultJsonTitle.Text = MngObj.ResLoader.GetString("ResultJsonTitle");
            TxtBlkGenExcelGETConvertFormatResultxmlTitle.Text = MngObj.ResLoader.GetString("ResultxmlTitle");
        }

        private async void BtnGenExcelGETConvertFormat_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TxtBlkGenExcelGETConvertFormatResultMsg.Text = "";
                string FullURL = App.APIBaseUri + "api/excel?";
                FullURL += "FileName=" + TxtBoxGenExcelGETConvertFormatFileName.Text.Trim();
                FullURL += "&Type=" + CmbGenExcelGETConvertFormatType.SelectedValue.ToString();
                if (CmbGenExcelGETConvertFormatWorkBookFileName.SelectedIndex > 0)
                    FullURL += "&WorkBookFileName=" + CmbGenExcelGETConvertFormatWorkBookFileName.SelectedValue.ToString();

                TxtBlkGenExcelGETConvertFormatResultRequestUrl.Text = FullURL;
                StkPnlGenExcelGETConvertFormatResultJson.Visibility = Visibility.Collapsed;
                StkPnlGenExcelGETConvertFormatResultxml.Visibility = Visibility.Collapsed;

                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(FullURL).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        TxtBlkGenExcelGETConvertFormatResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgInvalidResponse");
                    }
                    else if (CmbGenExcelGETConvertFormatType.SelectedValue.ToString() != "json" && CmbGenExcelGETConvertFormatType.SelectedValue.ToString() != "xml")
                    {
                        //download excel file
                        FileSavePicker savePicker = new FileSavePicker();
                        savePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
                        savePicker.FileTypeChoices.Add("Excel File", new List<string>() { "." + CmbGenExcelGETConvertFormatType.SelectedValue.ToString() });
                        savePicker.SuggestedFileName = string.IsNullOrEmpty(TxtBoxGenExcelGETConvertFormatFileName.Text) ? "excel" : TxtBoxGenExcelGETConvertFormatFileName.Text.Trim();
                        StorageFile file = await savePicker.PickSaveFileAsync();
                        if (file != null)
                        {
                            CachedFileManager.DeferUpdates(file);
                            await FileIO.WriteBytesAsync(file, await response.Content.ReadAsByteArrayAsync());
                            FileUpdateStatus status = await CachedFileManager.CompleteUpdatesAsync(file);
                            if (status == FileUpdateStatus.Complete)
                                TxtBlkGenExcelGETConvertFormatResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgFileLocation") + file.Path;
                            else
                                TxtBlkGenExcelGETConvertFormatResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgFileNotDownloaded");
                        }
                    }
                    else
                    {
                        //show data in json
                        if (CmbGenExcelGETConvertFormatType.SelectedValue.ToString() == "json")
                        {
                            string ResultList = response.Content.ReadAsStringAsync().Result;
                            TxtBoxGenExcelGETConvertFormatResultJson.Text = response.Content.ReadAsStringAsync().Result.ToString();
                            StkPnlGenExcelGETConvertFormatResultJson.Visibility = Visibility.Visible;
                        }
                        //show data in xml
                        else if (CmbGenExcelGETConvertFormatType.SelectedValue.ToString() == "xml")
                        {
                            TxtBoxGenExcelGETConvertFormatResultxml.Text = response.Content.ReadAsStringAsync().Result.ToString();
                            StkPnlGenExcelGETConvertFormatResultxml.Visibility = Visibility.Visible;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                TxtBlkGenExcelGETConvertFormatResultMsg.Text = MngObj.ResLoader.GetString("ErrorMsg") + ex.Message;
            }
            StkPnlGenExcelGETConvertFormatResult.Visibility = Visibility.Visible;
        }

        private void StkPnlGenExcelGET3_Link_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (StkPnlGenExcelGET3_Content.Visibility == Visibility.Collapsed)
                StkPnlGenExcelGET3_Content.Visibility = Visibility.Visible;
            else
                StkPnlGenExcelGET3_Content.Visibility = Visibility.Collapsed;
        }

        private void SetGenExcelPOSTXML()
        {
            try
            {
                Manage MngObj = new Manage();
                TxtBlkGenExcelPOSTXMLGETPOSTText.Text = MngObj.ResLoader.GetString("GenExcelPOSTXMLGETPOSTText");
                TxtBlkGenExcelPOSTXMLDesc.Text = MngObj.ResLoader.GetString("DescHeader");
                TxtBlkGenExcelPOSTXMLDescDetails.Text = MngObj.ResLoader.GetString("GenExcelPOSTXMLDescDetails");
                BtnGenExcelPOSTXML.Content = MngObj.ResLoader.GetString("BtnGenExcel");


                TxtBlkGenExcelPOSTXMLParamsHeader.Text = MngObj.ResLoader.GetString("ParamHeader");
                TxtBlkGenExcelPOSTXMLFileName.Text = "FileName";
                TxtBlkGenExcelPOSTXMLFileNameDesc.Text = MngObj.ResLoader.GetString("GenExcelPOSTXMLFileNameDesc");
                TxtBlkGenExcelPOSTXMLType.Text = "Type";
                CmbGenExcelPOSTXMLType.DisplayMemberPath = "Text";
                CmbGenExcelPOSTXMLType.SelectedValuePath = "Value";
                CmbGenExcelPOSTXMLType.ItemsSource = MngObj.GetExcel_TypeDDList();
                CmbGenExcelPOSTXMLType.SelectedValue = "xlsx";
                TxtBlkGenExcelPOSTXMLTypeDesc.Text = MngObj.ResLoader.GetString("GenExcelPOSTXMLTypeDesc");
                TxtBlkGenExcelPOSTXMLDataFile.Text = "DataFile";
                TxtBlkCtrlGenExcelPOSTXMLDataFile.Text = MngObj.ResLoader.GetString("ResultMsgNoFileChoosenDefault");
                TxtBlkGenExcelPOSTXMLDataFileDesc.Text = MngObj.ResLoader.GetString("GenExcelPOSTXMLDataFileDesc");
                TxtBlkGenExcelPOSTXMLDataFileDesc2.Text = "<orders>";
                TxtBlkGenExcelPOSTXMLDataFileDesc2.Text += "\n    <order id=\"1\">";
                TxtBlkGenExcelPOSTXMLDataFileDesc2.Text += "\n        <price>1000</price>";
                TxtBlkGenExcelPOSTXMLDataFileDesc2.Text += "\n   </order>";
                TxtBlkGenExcelPOSTXMLDataFileDesc2.Text += "\n   ......";
                TxtBlkGenExcelPOSTXMLDataFileDesc2.Text += "\n</ orders > ";

                //Response Schema
                TxtBlkGenExcelPOSTXMLResponseSchemaHeader.Text = MngObj.ResLoader.GetString("ResponseSchemaHeader");
                TxtBoxGenExcelPOSTXMLResponseSchema.Text = MngObj.GetResponseSchema();

                //Result
                TxtBlkGenExcelPOSTXMLResultRequestUrlTitle.Text = MngObj.ResLoader.GetString("ResultRequestUrlTitle");
                TxtBlkGenExcelPOSTXMLResultJsonTitle.Text = MngObj.ResLoader.GetString("ResultJsonTitle");
                TxtBlkGenExcelPOSTXMLResultxmlTitle.Text = MngObj.ResLoader.GetString("ResultxmlTitle");
            }
            catch (Exception ex)
            {
            }
        }

        private void BtnGenExcelPOSTXML_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TxtBlkGenExcelPOSTXMLResultMsg.Text = "";
                string FullURL = App.APIBaseUri + "api/excel";

                TxtBlkGenExcelPOSTXMLResultRequestUrl.Text = FullURL;
                StkPnlGenExcelPOSTXMLResultJson.Visibility = Visibility.Collapsed;
                StkPnlGenExcelPOSTXMLResultxml.Visibility = Visibility.Collapsed;

                string _filePath = GenExcelPOSTXMLFile;
                if (string.IsNullOrEmpty(_filePath))
                {
                    TxtBlkGenExcelPOSTXMLResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgInvalidResponse");
                }
                else {
                    Assembly asm = typeof(MainPage).GetTypeInfo().Assembly;
                    using (var client = new HttpClient())
                    using (var formData = new MultipartFormDataContent())
                    using (var fileStream = asm.GetManifestResourceStream("WebAPIExplorer.Resources." + Path.GetFileName(_filePath)))
                    {
                        var fileName = string.IsNullOrEmpty(TxtBoxGenExcelPOSTXMLFileName.Text) ? "excel" : TxtBoxGenExcelPOSTXMLFileName.Text.Trim();
                        var fileFormat = string.IsNullOrEmpty(CmbGenExcelPOSTXMLType.SelectedValue.ToString()) ? "xlsx" : CmbGenExcelPOSTXMLType.SelectedValue.ToString();
                        formData.Add(new StringContent(fileName), "FileName");
                        formData.Add(new StringContent(fileFormat), "Type");
                        formData.Add(new StreamContent(fileStream), "DataFile", Path.GetFileName(_filePath));
                        var response = client.PostAsync(FullURL, formData).Result;
                        GenExcelPOSTXMLFile = null;
                        TxtBlkCtrlGenExcelPOSTXMLDataFile.Text = MngObj.ResLoader.GetString("ResultMsgNoFileChoosenDefault");

                        if (!response.IsSuccessStatusCode)
                        {
                            TxtBlkGenExcelPOSTXMLResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgInvalidResponse");
                        }
                        else if (CmbGenExcelPOSTXMLType.SelectedValue.ToString() != "json" && CmbGenExcelPOSTXMLType.SelectedValue.ToString() != "xml")
                        {
                            ////download excel file
                            DownloadFileGenExcelPOSTXML(response);
                        }
                        else
                        {
                            //show data in json
                            if (CmbGenExcelPOSTXMLType.SelectedValue.ToString() == "json")
                            {
                                string ResultList = response.Content.ReadAsStringAsync().Result;
                                TxtBoxGenExcelPOSTXMLResultJson.Text = response.Content.ReadAsStringAsync().Result.ToString();
                                StkPnlGenExcelPOSTXMLResultJson.Visibility = Visibility.Visible;
                            }
                            //show data in xml
                            else if (CmbGenExcelPOSTXMLType.SelectedValue.ToString() == "xml")
                            {
                                TxtBoxGenExcelPOSTXMLResultxml.Text = response.Content.ReadAsStringAsync().Result.ToString();
                                StkPnlGenExcelPOSTXMLResultxml.Visibility = Visibility.Visible;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                TxtBlkGenExcelPOSTXMLResultMsg.Text = MngObj.ResLoader.GetString("ErrorMsg") + ex.Message;
            }
            StkPnlGenExcelPOSTXMLResult.Visibility = Visibility.Visible;
        }

        private async void DownloadFileGenExcelPOSTXML(HttpResponseMessage response)
        {
            //download excel file
            FileSavePicker savePicker = new FileSavePicker();
            savePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            savePicker.FileTypeChoices.Add("Excel File", new List<string>() { "." + CmbGenExcelPOSTXMLType.SelectedValue.ToString() });
            savePicker.SuggestedFileName = string.IsNullOrEmpty(TxtBoxGenExcelPOSTXMLFileName.Text) ? "excel" : TxtBoxGenExcelPOSTXMLFileName.Text.Trim();
            StorageFile file = await savePicker.PickSaveFileAsync();
            if (file != null)
            {
                CachedFileManager.DeferUpdates(file);
                await FileIO.WriteBytesAsync(file, await response.Content.ReadAsByteArrayAsync());
                FileUpdateStatus status = await CachedFileManager.CompleteUpdatesAsync(file);
                if (status == FileUpdateStatus.Complete)
                    TxtBlkGenExcelPOSTXMLResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgFileLocation") + file.Path;
                else
                    TxtBlkGenExcelPOSTXMLResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgFileNotDownloaded");
            }
        }

        private void StkPnlGenExcelGET4_Link_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (StkPnlGenExcelGET4_Content.Visibility == Visibility.Collapsed)
                StkPnlGenExcelGET4_Content.Visibility = Visibility.Visible;
            else
                StkPnlGenExcelGET4_Content.Visibility = Visibility.Collapsed;
        }
        private async void TxtBlkCtrlGenExcelPOSTXMLDataFile_Tapped(object sender, TappedRoutedEventArgs e)
        {
            GenExcelPOSTXMLFile = null;
            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.List;
            picker.FileTypeFilter.Add(".xml");

            Windows.Storage.StorageFile file = await picker.PickSingleFileAsync();
            if (file != null)
            {
                GenExcelPOSTXMLFile = file.Path;
                TxtBlkCtrlGenExcelPOSTXMLDataFile.Text = file.Name.Length > 30 ? ".." + file.Name.Substring(file.Name.Length - 30, 30) : file.Name;
            }
        }

        private void SetGenExcelPOSTJSON()
        {
            try
            {
                Manage MngObj = new Manage();
                TxtBlkGenExcelPOSTJSONGETPOSTText.Text = MngObj.ResLoader.GetString("GenExcelPOSTJSONGETPOSTText");
                TxtBlkGenExcelPOSTJSONDesc.Text = MngObj.ResLoader.GetString("DescHeader");
                TxtBlkGenExcelPOSTJSONDescDetails.Text = MngObj.ResLoader.GetString("GenExcelPOSTJSONDescDetails");
                BtnGenExcelPOSTJSON.Content = MngObj.ResLoader.GetString("BtnGenExcel");

                TxtBlkGenExcelPOSTJSONParamsHeader.Text = MngObj.ResLoader.GetString("ParamHeader");
                TxtBlkGenExcelPOSTJSONFileName.Text = "FileName";
                TxtBlkGenExcelPOSTJSONFileNameDesc.Text = MngObj.ResLoader.GetString("GenExcelPOSTJSONFileNameDesc");
                TxtBlkGenExcelPOSTJSONType.Text = "Type";
                CmbGenExcelPOSTJSONType.DisplayMemberPath = "Text";
                CmbGenExcelPOSTJSONType.SelectedValuePath = "Value";
                CmbGenExcelPOSTJSONType.ItemsSource = MngObj.GetExcel_TypeDDList();
                CmbGenExcelPOSTJSONType.SelectedValue = "xlsx";
                TxtBlkGenExcelPOSTJSONTypeDesc.Text = MngObj.ResLoader.GetString("GenExcelPOSTJSONTypeDesc");
                TxtBlkGenExcelPOSTJSONData.Text = "Data";
                TxtBoxGenExcelPOSTJSONData.Text = MngObj.GetJSONData();
                TxtBlkGenExcelPOSTJSONDataDesc.Text = MngObj.ResLoader.GetString("GenExcelPOSTJSONDataDesc");

                //Response Schema
                TxtBlkGenExcelPOSTJSONResponseSchemaHeader.Text = MngObj.ResLoader.GetString("ResponseSchemaHeader");
                TxtBoxGenExcelPOSTJSONResponseSchema.Text = MngObj.GetResponseSchema();

                //Result
                TxtBlkGenExcelPOSTJSONResultRequestUrlTitle.Text = MngObj.ResLoader.GetString("ResultRequestUrlTitle");
                TxtBlkGenExcelPOSTJSONResultJsonTitle.Text = MngObj.ResLoader.GetString("ResultJsonTitle");
                TxtBlkGenExcelPOSTJSONResultxmlTitle.Text = MngObj.ResLoader.GetString("ResultxmlTitle");
            }
            catch (Exception ex)
            {
            }
        }

        private async void BtnGenExcelPOSTJSON_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TxtBlkGenExcelPOSTJSONResultMsg.Text = "";
                string FullURL = App.APIBaseUri + "api/excel";

                TxtBlkGenExcelPOSTJSONResultRequestUrl.Text = FullURL;
                StkPnlGenExcelPOSTJSONResultJson.Visibility = Visibility.Collapsed;
                StkPnlGenExcelPOSTJSONResultxml.Visibility = Visibility.Collapsed;

                using (var client = new HttpClient())
                using (var formData = new MultipartFormDataContent())
                {
                    var fileName = string.IsNullOrEmpty(TxtBoxGenExcelPOSTJSONFileName.Text) ? "excel" : TxtBoxGenExcelPOSTJSONFileName.Text.Trim();
                    var fileFormat = string.IsNullOrEmpty(CmbGenExcelPOSTJSONType.SelectedValue.ToString()) ? "xlsx" : CmbGenExcelPOSTJSONType.SelectedValue.ToString();
                    var data = TxtBoxGenExcelPOSTJSONData.Text.Trim();
                    formData.Add(new StringContent(fileName), "FileName");
                    formData.Add(new StringContent(fileFormat), "Type");
                    formData.Add(new StringContent(data), "Data");
                    var response = client.PostAsync(FullURL, formData).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        TxtBlkGenExcelPOSTJSONResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgInvalidResponse");
                    }
                    else if (CmbGenExcelPOSTJSONType.SelectedValue.ToString() != "json" && CmbGenExcelPOSTJSONType.SelectedValue.ToString() != "xml")
                    {
                        //download excel file
                        FileSavePicker savePicker = new FileSavePicker();
                        savePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
                        savePicker.FileTypeChoices.Add("Excel File", new List<string>() { "." + CmbGenExcelPOSTJSONType.SelectedValue.ToString() });
                        savePicker.SuggestedFileName = string.IsNullOrEmpty(TxtBoxGenExcelPOSTJSONFileName.Text) ? "excel" : TxtBoxGenExcelPOSTJSONFileName.Text.Trim();
                        StorageFile file = await savePicker.PickSaveFileAsync();
                        if (file != null)
                        {
                            CachedFileManager.DeferUpdates(file);
                            await FileIO.WriteBytesAsync(file, await response.Content.ReadAsByteArrayAsync());
                            FileUpdateStatus status = await CachedFileManager.CompleteUpdatesAsync(file);
                            if (status == FileUpdateStatus.Complete)
                                TxtBlkGenExcelPOSTJSONResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgFileLocation") + file.Path;
                            else
                                TxtBlkGenExcelPOSTJSONResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgFileNotDownloaded");
                        }
                    }
                    else
                    {
                        //show data in json
                        if (CmbGenExcelPOSTJSONType.SelectedValue.ToString() == "json")
                        {
                            string ResultList = response.Content.ReadAsStringAsync().Result;
                            TxtBoxGenExcelPOSTJSONResultJson.Text = response.Content.ReadAsStringAsync().Result.ToString();
                            StkPnlGenExcelPOSTJSONResultJson.Visibility = Visibility.Visible;
                        }
                        //show data in xml
                        else if (CmbGenExcelPOSTJSONType.SelectedValue.ToString() == "xml")
                        {
                            TxtBoxGenExcelPOSTJSONResultxml.Text = response.Content.ReadAsStringAsync().Result.ToString();
                            StkPnlGenExcelPOSTJSONResultxml.Visibility = Visibility.Visible;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                TxtBlkGenExcelPOSTJSONResultMsg.Text = MngObj.ResLoader.GetString("ErrorMsg") + ex.Message;
            }
            StkPnlGenExcelPOSTJSONResult.Visibility = Visibility.Visible;
        }

        private void StkPnlGenExcelGET5_Link_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (StkPnlGenExcelGET5_Content.Visibility == Visibility.Collapsed)
                StkPnlGenExcelGET5_Content.Visibility = Visibility.Visible;
            else
                StkPnlGenExcelGET5_Content.Visibility = Visibility.Collapsed;
        }


        private void SetGenExcelPOSTConvertFormat()
        {
            try
            {
                Manage MngObj = new Manage();
                TxtBlkGenExcelPOSTConvertFormatGETPOSTText.Text = MngObj.ResLoader.GetString("GenExcelPOSTConvertFormatGETPOSTText");
                TxtBlkGenExcelPOSTConvertFormatDesc.Text = MngObj.ResLoader.GetString("DescHeader");
                TxtBlkGenExcelPOSTConvertFormatDescDetails.Text = MngObj.ResLoader.GetString("GenExcelPOSTConvertFormatDescDetails");
                BtnGenExcelPOSTConvertFormat.Content = MngObj.ResLoader.GetString("BtnGenExcel");

                TxtBlkGenExcelPOSTConvertFormatParamsHeader.Text = MngObj.ResLoader.GetString("ParamHeader");
                TxtBlkGenExcelPOSTConvertFormatFileName.Text = "FileName";
                TxtBlkGenExcelPOSTConvertFormatFileNameDesc.Text = MngObj.ResLoader.GetString("GenExcelPOSTConvertFormatFileNameDesc");
                TxtBlkGenExcelPOSTConvertFormatType.Text = "Type";
                CmbGenExcelPOSTConvertFormatType.DisplayMemberPath = "Text";
                CmbGenExcelPOSTConvertFormatType.SelectedValuePath = "Value";
                CmbGenExcelPOSTConvertFormatType.ItemsSource = MngObj.GetExcel_TypeDDList();
                CmbGenExcelPOSTConvertFormatType.SelectedValue = "xlsx";
                TxtBlkGenExcelPOSTConvertFormatTypeDesc.Text = MngObj.ResLoader.GetString("GenExcelPOSTConvertFormatTypeDesc");
                TxtBlkGenExcelPOSTConvertFormatWorkbookFile.Text = "WorkbookFile";
                TxtBlkCtrlGenExcelPOSTConvertFormatWorkbookFile.Text = MngObj.ResLoader.GetString("ResultMsgNoFileChoosenDefault");
                TxtBlkGenExcelPOSTConvertFormatWorkbookFileDesc.Text = MngObj.ResLoader.GetString("GenExcelPOSTConvertFormatWorkbookFileDesc");

                //Response Schema
                TxtBlkGenExcelPOSTConvertFormatResponseSchemaHeader.Text = MngObj.ResLoader.GetString("ResponseSchemaHeader");
                TxtBoxGenExcelPOSTConvertFormatResponseSchema.Text = MngObj.GetResponseSchema();

                //Result
                TxtBlkGenExcelPOSTConvertFormatResultRequestUrlTitle.Text = MngObj.ResLoader.GetString("ResultRequestUrlTitle");
                TxtBlkGenExcelPOSTConvertFormatResultJsonTitle.Text = MngObj.ResLoader.GetString("ResultJsonTitle");
                TxtBlkGenExcelPOSTConvertFormatResultxmlTitle.Text = MngObj.ResLoader.GetString("ResultxmlTitle");
            }
            catch (Exception ex)
            {
            }
        }

        private void BtnGenExcelPOSTConvertFormat_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TxtBlkGenExcelPOSTConvertFormatResultMsg.Text = "";
                string FullURL = App.APIBaseUri + "api/excel";

                TxtBlkGenExcelPOSTConvertFormatResultRequestUrl.Text = FullURL;
                StkPnlGenExcelPOSTConvertFormatResultJson.Visibility = Visibility.Collapsed;
                StkPnlGenExcelPOSTConvertFormatResultxml.Visibility = Visibility.Collapsed;

                string _filePath = GenExcelPOSTConvertFormatFile;
                if (string.IsNullOrEmpty(_filePath))
                {
                    TxtBlkGenExcelPOSTConvertFormatResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgInvalidResponse");
                    return;
                }

                //Task.Run(() =>
                //{
                //    Task.Yield();
                Assembly asm = typeof(MainPage).GetTypeInfo().Assembly;
                using (var client = new HttpClient())
                using (var formData = new MultipartFormDataContent())
                using (var fileStream = asm.GetManifestResourceStream("WebAPIExplorer.Resources." + Path.GetFileName(_filePath)))//using (var fileStream = File.OpenRead(_filePath))
                {
                    var fileName = string.IsNullOrEmpty(TxtBoxGenExcelPOSTConvertFormatFileName.Text) ? "excel" : TxtBoxGenExcelPOSTConvertFormatFileName.Text.Trim();
                    var fileFormat = string.IsNullOrEmpty(CmbGenExcelPOSTConvertFormatType.SelectedValue.ToString()) ? "xlsx" : CmbGenExcelPOSTConvertFormatType.SelectedValue.ToString();
                    formData.Add(new StringContent(fileName), "FileName");
                    formData.Add(new StringContent(fileFormat), "Type");
                    formData.Add(new StreamContent(fileStream), "WorkbookFile", Path.GetFileName(_filePath));
                    var response = client.PostAsync(FullURL, formData).Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        TxtBlkGenExcelPOSTConvertFormatResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgInvalidResponse");
                    }
                    else if (CmbGenExcelPOSTConvertFormatType.SelectedValue.ToString() != "json" && CmbGenExcelPOSTConvertFormatType.SelectedValue.ToString() != "xml")
                    {
                        //download excel file
                        DownloadFileGenExcelPOSTConvertFormat(response);
                    }
                    else
                    {
                        //show data in json
                        if (CmbGenExcelPOSTConvertFormatType.SelectedValue.ToString() == "json")
                        {
                            string ResultList = response.Content.ReadAsStringAsync().Result;
                            TxtBoxGenExcelPOSTConvertFormatResultJson.Text = response.Content.ReadAsStringAsync().Result.ToString();
                            StkPnlGenExcelPOSTConvertFormatResultJson.Visibility = Visibility.Visible;
                        }
                        //show data in xml
                        else if (CmbGenExcelPOSTConvertFormatType.SelectedValue.ToString() == "xml")
                        {
                            TxtBoxGenExcelPOSTConvertFormatResultxml.Text = response.Content.ReadAsStringAsync().Result.ToString();
                            StkPnlGenExcelPOSTConvertFormatResultxml.Visibility = Visibility.Visible;
                        }
                    }
                }
                //});
            }
            catch (Exception ex)
            {
                TxtBlkGenExcelPOSTConvertFormatResultMsg.Text = MngObj.ResLoader.GetString("ErrorMsg") + ex.Message;
            }
            StkPnlGenExcelPOSTConvertFormatResult.Visibility = Visibility.Visible;
        }

        private async void DownloadFileGenExcelPOSTConvertFormat(HttpResponseMessage response)
        {
            //download excel file
            FileSavePicker savePicker = new FileSavePicker();
            savePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            savePicker.FileTypeChoices.Add("Excel File", new List<string>() { "." + CmbGenExcelPOSTConvertFormatType.SelectedValue.ToString() });
            savePicker.SuggestedFileName = string.IsNullOrEmpty(TxtBoxGenExcelPOSTConvertFormatFileName.Text) ? "excel" : TxtBoxGenExcelPOSTConvertFormatFileName.Text.Trim();
            StorageFile file = await savePicker.PickSaveFileAsync();
            if (file != null)
            {
                CachedFileManager.DeferUpdates(file);
                await FileIO.WriteBytesAsync(file, await response.Content.ReadAsByteArrayAsync());
                FileUpdateStatus status = await CachedFileManager.CompleteUpdatesAsync(file);
                if (status == FileUpdateStatus.Complete)
                    TxtBlkGenExcelPOSTConvertFormatResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgFileLocation") + file.Path;
                else
                    TxtBlkGenExcelPOSTConvertFormatResultMsg.Text = MngObj.ResLoader.GetString("ResultMsgFileNotDownloaded");
            }
        }

        private void StkPnlGenExcelGET6_Link_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (StkPnlGenExcelGET6_Content.Visibility == Visibility.Collapsed)
                StkPnlGenExcelGET6_Content.Visibility = Visibility.Visible;
            else
                StkPnlGenExcelGET6_Content.Visibility = Visibility.Collapsed;
        }

        private async void TxtBlkCtrlGenExcelPOSTConvertFormatWorkbookFile_Tapped(object sender, TappedRoutedEventArgs e)
        {
            GenExcelPOSTConvertFormatFile = null;
            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.List;
            picker.FileTypeFilter.Add(".xls");
            picker.FileTypeFilter.Add(".xlsx");
            picker.FileTypeFilter.Add(".csv");

            Windows.Storage.StorageFile file = await picker.PickSingleFileAsync();
            if (file != null)
            {
                GenExcelPOSTConvertFormatFile = file.Path;
                TxtBlkCtrlGenExcelPOSTConvertFormatWorkbookFile.Text = file.Name.Length > 30 ? ".." + file.Name.Substring(file.Name.Length - 30, 30) : file.Name;
            }
        }

    }
}


