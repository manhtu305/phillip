ASP.NET Core Web API Sample.
-------------------------------------------------------------------
The WebAPI sample demonstrates how to make a webapi server to provide all services that ComponentOne Web API products supports.

The sample is a service application which provides all these services:
-Excel: generate/import/export etc.
-Barcode: generate barcode.
-Image: export flexchart/gauge to image.
-Report: generate a report from given report defintion file and datasource, export the report to other formats etc.
-DataEngine: analyze the raw data to show the aggregate result, show detailed row data etc.
-Pdf: load a pdf from given path, export the pdf to other formats etc.

<product>Data Engine Services for ASP.NET Web API;ASP.NET</product>
<product>PDF Services for ASP.NET Web API;ASP.NET</product>
<product>Report Services for ASP.NET Web API;ASP.NET</product>
<product>Excel Services for ASP.NET Web API;ASP.NET</product>
<product>Image Services for ASP.NET Web API;ASP.NET</product>
<product>Barcode Services for ASP.NET Web API;ASP.NET</product>