ASP.NET WebAPI Explorer Sample.
-------------------------------------------------------------------
The WebAPI Explorer sample demonstrates Rest APIs' and Import\Export features of data management and visualization controls.

Generate Excel files, export data management or visualization controls to the format of your choice. With these powerful Web APIs, 
you can generate excel files from different data sources, merge excel files, create barcodes on the fly or import and export excel sheets with FlexGrid. 
The possibilities are limited only by the developer's imagination.
