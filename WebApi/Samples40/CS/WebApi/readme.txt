﻿ASP.NET WebAPI Sample.
-------------------------------------------------------------------
The WebAPI sample demonstrates how to make a webapi server to provide all services that ComponentOne WebAPI products supports.

The sample is a service application which provides all these services:
-Excel: generate/import/export etc.
-Barcode: generate barcode.
-Image: export flexchart/gauge to image.
-Report: generate a report from given report defintion file and datasource, export the report to other formats etc.
-DataEngine: analyze the raw data to show the aggregate result, show detailed row data etc.
-Pdf: load a pdf from given path, export the pdf to other formats etc.

For cloud service storage function demo, please note that :

- Azure :

The demo uses the file path as "Azure/demostoragepdfcontainer/test.xlsx"

You need to use your account to update the Azure connection string in the sample by following these steps:

step 1: login to your Azure account.

step 2: create a container and generate connection string for this container.

step 3: use the connection string created in step 2 to put into AzureStorageConnectionString key value in the Web.config.

step 4: open WebApiExplorer project and navigate to Storage api section.

step 5: change "demostoragepdfcontainer" to your container name.

step 6: change "test.xlsx" to your file name you want to post.

 

- AWS: AWS/test.xlsx

step 1: you need prepare value for following 3 keys in the Web.config.

AccessTocken
SecretKey
BucketName
step 2: open WebApiExplorer project and navigate to Storage api section.

step 3: change "test.xlsx" to your file name you want to post.

 

DropBox : DropBox/C1WebApi/test.xlsx

step 1: login to your DropBox acount.

step 2: create an App and generate Access Token for this app.

step 3: use Access Token  created at step 2 to put into DropBoxStorageAccessTocken key value in the Web.config.

step 4: open WebApiExplorer project and navigate to Storage api section.

step 5: change "C1WebApi" to your apps name.

step 6: change "test.xlsx" to your file name you want to post.

 

GoogleDrive: GoogleDrive/WebAPI/test.xlsx

step 1: login to your GoogleDrive acount.

step 2: create an App and generate credentials.json file for this app.

step 3: usecredentials.json  created at step 2 to put into WebApi folder.

step 4: open WebApiExplorer project and navigate to Storage api section.

step 5: change "WebAPI" to your apps name.

step 6: change "test.xlsx" to your file name you want to post.