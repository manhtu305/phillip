﻿Imports System.Data.SqlClient
Imports Owin

Partial Public Class Startup
    Public Sub Configuration(app As IAppBuilder)
        ConfigureAuth(app)
        Using connection = New SqlConnection(ConfigurationManager.ConnectionStrings("DefaultConnection").ConnectionString)
            connection.Open()
            app.UseDataEngineProviders() _
            .AddDataEngine("Orders", New SqlCommand("SELECT Orders.Amount, Orders.Province, Orders.Region, Orders.ShipMode, Orders.ShippingCost, Orders.OrderPriority, Orders.OrderQuantity, Orders.Discount, Customers.Name as CustomerName, Employees.Name as EmployeeName From Orders Left join Customers on Orders.CustomerId = Customers.Id Left join Employees on Orders.EmployeeId = Employees.Id"), connection) _
            .AddDataEngine("Invoices", New SqlCommand("SELECT Invoices.Amount, Customers.Name as CustomerName, Employees.Name as EmployeeName From Invoices Left join Customers on Invoices.CustomerId = Customers.Id Left join Employees on Invoices.EmployeeId = Employees.Id"), connection) _
            .AddDataEngine("Products", New SqlCommand("select Name,Price,Category,SubCategory,QuantityInStock,QuantityInOrder from Products"), connection) _
            .AddDataEngine("Expenses", New SqlCommand("SELECT Expenses.Amount, Expenses.Type, Expenses.Status, Employees.Name as EmployeeName From Expenses Left join Employees on Expenses.EmployeeId = Employees.Id"), connection)
        End Using
    End Sub
End Class
