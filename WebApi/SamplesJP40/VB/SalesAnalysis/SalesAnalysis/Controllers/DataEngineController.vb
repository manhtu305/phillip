﻿Imports C1.Web.Api.DataEngine.Models
Imports System.Web.Http
Imports System.Web.Http.Results

<Authorize>
Public Class DataEngineController
    Inherits C1.Web.Api.DataEngine.DataEngineController
    Public Overrides Function GetFields(dataSourceKey As String) As IHttpActionResult
        Dim result = MyBase.GetFields(dataSourceKey)
        Dim contentResult = TryCast(result, OkNegotiatedContentResult(Of IEnumerable(Of Field)))
        If contentResult Is Nothing Then
            Return result
        End If

        Dim content = TryCast(contentResult.Content, IEnumerable(Of Field))
        content = ViewPermission.VerifyFields(dataSourceKey, content, RequestContext.Principal)
        Return Ok(content)
    End Function
End Class