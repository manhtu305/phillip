﻿Public Class LoginUser
    Private Shared ReadOnly _definedUsers As New Lazy(Of IReadOnlyList(Of LoginUser))(AddressOf InitDefinedUsers)

    Private Shared Function InitDefinedUsers() As IReadOnlyList(Of LoginUser)
        Return New List(Of LoginUser)() From {
            New LoginUser("test@test.com", "C1_test", "Admin"),
            New LoginUser("nancy@test.com", "C1_test", "Sales Representative"),
            New LoginUser("steven@test.com", "C1_test", "Sales Manager")
        }.AsReadOnly()
    End Function

    Public Shared ReadOnly Property DefinedUsers() As IReadOnlyList(Of LoginUser)
        Get
            Return _definedUsers.Value
        End Get
    End Property

    Public Sub New(email__1 As String, password__2 As String, role__3 As String)
        Email = email__1
        Password = password__2
        Role = role__3
    End Sub

    Public Property Email() As String
    Public Property Password() As String
    Public Property Role() As String
    Public ReadOnly Property Display() As String
        Get
            Return String.Format("{0}({1})", Email, Role)
        End Get
    End Property
End Class