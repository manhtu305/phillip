﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <title>@ViewBag.Title</title>
    <link rel="icon" href="~/Content/images/favicon.png" type="image/png" />
    <link rel="shortcut icon" href="~/Content/images/favicon.ico" type="image/x-icon" />
    @Html.C1().Styles()
    @Styles.Render("~/Content/css")
    @Scripts.Render("~/bundles/modernizr")
    @Html.C1().Scripts().Olap()
</head>
<body>
    <div class="hide">
        @Html.Partial("_SiteNav", True)
    </div>
    <header>
        <div class="hamburger-nav-btn narrow-screen"><span class="icon-bars"></span></div>
        <a href="https://www.grapecity.co.jp/developer/componentone-studio" class="logo-container" target="_blank">
            <img src="~/Content/images/c1icon.png" width="46" height="46" alt="ComponentOne Webサイトに移動" />
            <span class="icon-c1text"></span>
        </a>
        @If (ViewBag.Email IsNot Nothing) Then
            @<div Class="wide-screen user-panel">
                <span Class="glyphicon glyphicon-user"></span> <span>@ViewBag.Email (@ViewBag.Role)</span><br />
                <a Class="signout-btn" href="javascript:void(0)" onclick="logout()">Sign out</a>
            </div>
        End If
        <div Class="task-bar">
            <span Class="semi-bold">@ViewBag.Title</span>
            @Html.Partial("_SiteNav", False)
        </div>
    </header>
    @RenderBody()
    <footer>
        <a href="https://www.grapecity.co.jp/developer" class="c1logo">
            <img src="~/Content/css/images/grapecityLogo.png" />
        </a>
        <p>
            © @DateTime.Now.Year GrapeCity, Inc. All Rights Reserved.<br />
            ここで示されているすべての製品および会社名は、それぞれの所有者の商標である場合があります。
        </p>
        @Code
            Dim currentUrl = Request.Url
            Dim urlStr = currentUrl.OriginalString.Substring(0, currentUrl.OriginalString.Length - (If(currentUrl.Query Is Nothing, 0, currentUrl.Query.Length)))
            @<a href="https://www.facebook.com/GrapeCity.dev.JP" target="_blank">
                <img src="~/Content/css/images/icons/32/picons36.png" alt="facebook" />
            </a>
            @<a href="https://twitter.com/GrapeCity_dev" target="_blank">
                <img src="~/Content/css/images/icons/32/picons33.png" alt="Twitter" />
            </a>
        End Code
    </footer>
    <div id="msgPopup">
        <div Class="modal-body">Loading...</div>
        <div Class="modal-footer">
            <button Class="btn" onclick="closeMsgPopup()">Close</button>
        </div>
    </div>
    @Html.C1().Popup("#msgPopup").Modal(True).HideTrigger(PopupTrigger.None)

    @Scripts.Render("~/bundles/jquery")
    @Scripts.Render("~/bundles/bootstrap")
    @Scripts.Render("~/bundles/app")
    <Script>
        var homeUrl = '@Url.Action("Index", "Home")';
    </Script>
    @RenderSection("scripts", required:=False)

    <script type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-20295802-2', 'auto');
        ga('send', 'pageview');
    </script>
</body>
</html>
