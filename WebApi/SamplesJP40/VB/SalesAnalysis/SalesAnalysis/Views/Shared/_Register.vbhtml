﻿@ModelType RegisterBindingModel
@Using (Html.BeginForm())
@Html.LabelFor(Function(m) m.Email)
@Html.EditorFor(Function(m) m.Email, New With {Key .htmlAttributes = New With {Key .[class] = "form-control"}})
@Html.ValidationMessageFor(Function(m) m.Email, Nothing, New With {Key .[class] = "text-danger"}, "div")

@Html.LabelFor(Function(m) m.Password)
@Html.EditorFor(Function(m) m.Password, New With {Key .htmlAttributes = New With {Key .[class] = "form-control"}})
@Html.ValidationMessageFor(Function(m) m.Password, Nothing, New With {Key .[class] = "text-danger"}, "div")

@Html.LabelFor(Function(m) m.ConfirmPassword)
@Html.EditorFor(Function(m) m.ConfirmPassword, New With {Key .htmlAttributes = New With {Key .[class] = "form-control"}})
@Html.ValidationMessageFor(Function(m) m.ConfirmPassword, Nothing, New With {Key .[class] = "text-danger"}, "div")

@Html.LabelFor(Function(m) m.Role)
@Html.DropDownListFor(Function(m) m.Role, Model.Roles.[Select](Function(r) New SelectListItem() With {.Text = r, .Value = r}), New With {Key .[class] = "form-control"})
@<br />
@<input type = "button" value="Register" onclick="registerEx()" Class="form-control" />
End Using
