[サンプル名]SalesReport
[カテゴリ]共通
------------------------------------------------------------------------
[概要]
このサンプルは、Web APIレポートサービスで認証を制御する方法を示しています。

このサンプルは、Web APIレポートサービスで認証を制御する方法を示しています。
すべてのユーザーに役割が割り当てられています。役割の違いによって権限が異なります。役割にフォルダへのアクセス権がない場合、httpステータスコード401の認証エラーが発生します。
