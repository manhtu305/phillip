﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;

namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	/// <summary>
	/// Represents the PieChartAnimation class which contains all of the settings for the animation.
	/// </summary>
	public class PieChartAnimation: ChartAnimation
	{

		/// <summary>
		/// Initializes a new instance of the PieChartAnimation class.
		/// </summary>
		public PieChartAnimation()
			: base()
		{
		}

		/// <summary>
		/// A value that indicates the offset for explode animation.
		/// </summary>
		[C1Description("C1Chart.PieChartAnimation.Offset")]
		[NotifyParentProperty(true)]
		[DefaultValue(10)]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Offset
		{
			get
			{
				return this.GetPropertyValue<int>("Offset", 10);
			}
			set
			{
				this.SetPropertyValue<int>("Offset", value);
			}
		}

		/// <summary>
		/// Serializes the current animation object to a string value.
		/// </summary>
		/// <returns>
		/// Returns the string value that serialized from the current animation object.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override string GetOptionValue()
		{
			string optionValue = base.GetOptionValue();
			if (this.Offset != 10)
			{
				if (!String.IsNullOrEmpty(optionValue))
				{
					optionValue += ",";
				}
				optionValue += " offset: " + this.Offset;
			}
			return optionValue;
		}

		#region Serialize
		/// <summary>
		/// Returns a boolean value informing the serializer whether the PieChartAnimation
		/// has changed from its default value.
		/// </summary>
		/// <returns>
		/// Returns a boolean value to determine whether this PieChartAnimation should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		internal protected override bool ShouldSerialize()
		{
			if (Offset != 10)
				return true;
			return base.ShouldSerialize();
		}
		#endregion
	}
}
