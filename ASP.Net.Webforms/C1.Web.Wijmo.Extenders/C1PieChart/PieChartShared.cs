﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	[WidgetDependencies(
        typeof(WijPieChart)
#if !EXTENDER
, "extensions.c1piechart.js",
ResourcesConst.C1WRAPPER_PRO
#endif
)]
#if EXTENDER
	public partial class C1PieChartExtender : C1ChartCoreExtender<PieChartSeries, PieChartAnimation>
#else    
	public partial class C1PieChart : C1ChartCore<PieChartSeries, PieChartAnimation, C1PieChartBinding>
#endif
	{

		private void InitChart()
		{
			this.SeriesTransition = new ChartAnimation();
			this.SeriesTransition.Duration = 1000;
			//this.SeriesTransition.Easing = "bounce";
			this.SeriesTransition.Easing = ChartEasing.EaseOutBounce;
		}

		/// <summary>
		/// A value that indicates the start angle of a pie chart when painting sectors.
		/// </summary>
		[C1Description("C1Chart.PieChart.StartAngle")]
		[WidgetOption]
		[DefaultValue(0)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int StartAngle
		{
			get
			{
				return this.GetPropertyValue<int>("StartAngle", 0);
			}
			set
			{
				this.SetPropertyValue<int>("StartAngle", value);
			}
		}

		/// <summary>
		/// A value indicates the radius used for a pie chart.
		/// If the value is -1, then the radius will be calculated 
		/// by the width/height value of the pie chart.
		/// </summary>
		[C1Description("C1Chart.PieChart.Radius")]
		[WidgetOption]
		[DefaultValue(-1)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Radius
		{
			get
			{
				return this.GetPropertyValue<int>("Radius", -1);
			}
			set
			{
				this.SetPropertyValue<int>("Radius", value);
			}
		}

        /// <summary>
        /// A value indicates the direction used for a pie chart.
        /// 'counterClockwise' is default value. The second available value is 'clockwise'.
        /// </summary>
        [C1Description("C1Chart.PieChart.Direction")]
        [WidgetOption]
        [DefaultValue(PieChartDirection.CounterClockwise)]
        [C1Category("Category.Appearance")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public PieChartDirection Direction
        {
            get
            {
                return this.GetPropertyValue<PieChartDirection>("Direction", PieChartDirection.CounterClockwise);
            }
            set
            {
                this.SetPropertyValue<PieChartDirection>("Direction", value);
            }
        }

		/// <summary>
		/// A value that indicates whether to show the effect and duration of the animation when reloading the data.
		/// </summary>
		[C1Description("C1Chart.PieChart.SeriesTransition")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public ChartAnimation SeriesTransition { get; set; }

		/// <summary>
		/// A value indicates the inner radius used for doughnut charts.
		/// </summary>
		[C1Description("C1Chart.PieChart.InnerRadius")]
		[WidgetOption]
		[DefaultValue(0)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int InnerRadius
		{
			get
			{
				return this.GetPropertyValue<int>("InnerRadius", 0);
			}
			set
			{
				this.SetPropertyValue<int>("InnerRadius", value);
			}
		}

		/// <summary>
		/// A value that indicates whether the piechart can be rotated and tooltip is always shown on touchable devices.
		/// </summary>
		[C1Description("C1Chart.PieChart.EnableTouchBehavior")]
		[WidgetOption]
		[DefaultValue(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool EnableTouchBehavior
		{
			get
			{
				return this.GetPropertyValue<bool>("EnableTouchBehavior", true);
			}
			set
			{
				this.SetPropertyValue<bool>("EnableTouchBehavior", value);
			}
		}

		//pie chart's label is a string, so ChartLabelFormatString doesn't work for piechart.
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override string ChartLabelFormatString
		{
			get
			{
				return base.ChartLabelFormatString;
			}
			set
			{
				base.ChartLabelFormatString = value;
			}
		}

		#region Serialize

		/// <summary>
		/// Determine whether the SeriesTransition property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns false if SeriesTransition has default values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeSeriesTransition()
		{
			if (!this.SeriesTransition.Enabled)
				return true;
			if (this.SeriesTransition.Duration != 1000)
				return true;
			//if (this.SeriesTransition.Easing != "bounce")
			if (this.SeriesTransition.Easing != ChartEasing.EaseOutBounce)
				return true;
			return false;
		}

		#endregion
	}
}
