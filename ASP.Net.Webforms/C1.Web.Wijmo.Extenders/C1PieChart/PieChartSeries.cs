﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{

	/// <summary>
	/// Represents the PieChartSeries class which contains all of the settings for the chart series.
	/// </summary>
	public class PieChartSeries : ChartSeries
	{

		/// <summary>
		/// Initializes a new instance of the PieChartSeries class.
		/// </summary>
		public PieChartSeries()
			:base()
		{
		}

		/// <summary>
		/// A value that indicates the offset of the chart series.
		/// </summary>
		[C1Description("C1Chart.PieChartSeries.Offset")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(0)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Offset
		{
			get
			{
				return this.GetPropertyValue<int>("Offset", 0);
			}
			set
			{
				this.SetPropertyValue<int>("Offset", value);
			}
		}

		/// <summary>
		/// A value that indicates the data of the chart series.
		/// </summary>
		[C1Description("C1Chart.ChartSeries.Data")]
		[NotifyParentProperty(true)]
		[DefaultValue(0.0)]
		//[WidgetOption]
		[Json(true, false, 0.0)]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public double Data
		{
			get
			{
				return this.GetPropertyValue<double>("Data", 0.0);
			}
			set
			{
				this.SetPropertyValue<double>("Data", value);
			}
		}


#if !EXTENDER
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override string[] HintContents
		{
			get
			{
				return base.HintContents;
			}
		}

		/// <summary>
		/// A value that indicates the hint content.
		/// </summary>
		[C1Description("C1Chart.PieChartHintContent")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[C1Category("Category.Data")]
		[Layout(LayoutType.Data)]
		public string HintContent
		{
			get
			{
				return this.GetPropertyValue<string>("HintContent", "");
			}
			set
			{
				this.SetPropertyValue<string>("HintContent", value);
			}
		}
#endif

        /// <summary>
        /// Hide this property for PieChart, currently PieChart doesn't support trendline.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override bool IsTrendline
        {
            get
            {
                return base.IsTrendline;
            }
        }

        /// <summary>
        /// Hide this property for PieChart, currently PieChart doesn't support trendline.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override TrendlineChartSeries TrendlineSeries
        {
            get
            {
                return base.TrendlineSeries;
            }
        }

		/// <summary>
		/// Returns a boolean value informing the serializer whether the PieChartSeries
		/// has changed from its default value.
		/// </summary>
		/// <returns>
		/// Returns a boolean value to determine whether this PieChartSeries should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal override bool ShouldSerialize()
		{
			//always return true.the Offset property should be serialized even its value is 0.
			return true;
			//if (!Visible)
			//    return true;
			//if (!String.IsNullOrEmpty(Label))
			//    return true;
			//if (!LegendEntry)
			//    return true;
			//if (Data != 0.0)
			//    return true;
			//return false;
		}

        /// <summary>
        /// Indicate whehter need to serialize TrendlineSeries, PieChart doesn't support Trendline.
        /// </summary>
        /// <returns></returns>
        public override bool ShouldSerializeTrendlineSeries()
        {
            return false;
        }
	}
}
