﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

[assembly: TagPrefix("C1.Web.Wijmo.Extenders.Menu", "Menu")]
namespace C1.Web.Wijmo.Extenders.C1Menu
{
	/// <summary>
	/// Menu extender
	/// </summary>
	[ToolboxItem(true)]
	[TargetControlType(typeof(Panel))]
	[ToolboxBitmap(typeof(C1MenuExtender), "Menu.png")]
	[LicenseProvider]
	public partial class C1MenuExtender : WidgetExtenderControlBase
	{
		#region ** fields
		private bool _productLicensed = false;
		#endregion

		#region ** constructor
		public C1MenuExtender()
		{
			VerifyLicense();

			this.Position = new PositionSettings();
			this.Animation = new Animation();
			this.ShowAnimation = new Animation();
			this.ShowAnimation.Animated = new AnimatedOption() { Effect = "slide" };
			this.HideAnimation = new Animation();
			this.HideAnimation.Animated = new AnimatedOption() { Effect = "fade" };
			this.SlidingAnimation = new SlideAnimation();
		}

		internal virtual void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1MenuExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}
		#endregion

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		internal override bool IsWijMobileExtender
		{
			get
			{
				return false;
			}
		}

	}
}
