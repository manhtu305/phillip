﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Menu
#else
namespace C1.Web.Wijmo.Controls.C1Menu
#endif
{

	#region ** TriggerEvent enum
	/// <summary>
	/// If menu is flyout mode, this value decide which event handle to show the menu and submenu.
	/// </summary>
	public enum TriggerEvent
	{
		/// <summary>
		/// Click event
		/// </summary>
		Click = 0,
		/// <summary>
		/// Mouseover and mouseout event
		/// </summary>
		Mouseenter = 1,
		/// <summary>
		/// Double click event
		/// </summary>
		Dblclick = 2,
		/// <summary>
		/// Right click event.
		/// </summary>
		Rtclick = 4
	}
	#endregion end of ** TriggerEvent enum

    #region
    /// <summary>
    /// If menu is flyout mode, this value specially decide which event handle to show the submenu.
    /// </summary>
    public enum SubmenuTriggerEvent
    {
        /// <summary>
        /// Follow the TriggerEvent property.
        /// </summary>
        Default = 0,
        /// <summary>
        /// Click event
        /// </summary>
        Click = 1,
        /// <summary>
        /// Mouseover and mouseout event
        /// </summary>
        Mouseenter = 2,
        /// <summary>
        /// Double click event
        /// </summary>
        Dblclick = 4,
        /// <summary>
        /// Right click event.
        /// </summary>
        Rtclick = 8
    }
    #endregion

    #region ** MenuMode enum
    /// <summary>
	/// Decides which mode the menu displays.
	/// </summary>
	public enum MenuMode
	{
		/// <summary>
		/// Show the menu as flyout style.
		/// </summary>
		Flyout = 0,
		/// <summary>
		/// Show the meun as i-pod style
		/// </summary>
		Sliding = 1
	}
	#endregion end of ** MenuMode enum

#if !EXTENDER
	/// <summary>
	/// Decides which side the image displays on menu item.
	/// </summary>
	public enum ImagePosition
	{
		/// <summary>
		/// Show the image on the left side of the menu item.
		/// </summary>
		Left = 0,
		/// <summary>
		/// Show the image on the right side of the menu item.
		/// </summary>
		Right = 1
	}
#endif
}
