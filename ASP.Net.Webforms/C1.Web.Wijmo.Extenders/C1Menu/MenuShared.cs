﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Menu", "wijmo")] 
namespace C1.Web.Wijmo.Extenders.C1Menu
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Menu", "wijmo")] 
namespace C1.Web.Wijmo.Controls.C1Menu
#endif
{

    [WidgetDependencies(
        typeof(WijMenu)
#if !EXTENDER
, "extensions.c1menu.js",
ResourcesConst.C1WRAPPER_OPEN
#endif
)]
#if EXTENDER
	public partial class C1MenuExtender
#else
	public partial class C1Menu
#endif
	{

		#region ** fields
		private PositionSettings _positionSettings;
		private Animation _animation;
		private Animation _showAnimation;
		private Animation _hideAnimation;
		private SlideAnimation _slidingAnimation;
		#endregion

		#region ** Options
		/// <summary>
		/// Specifies the event used to show the menu.
		/// </summary>
		[C1Description("C1Menu.Trigger")]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Behavior")]
		#if !EXTENDER
		[Layout(LayoutType.Behavior)]
		#endif
		public string Trigger
		{
			get
			{
				return GetPropertyValue("Trigger", "");
			}
			set
			{
				SetPropertyValue("Trigger", value);
			}
		}

		/// <summary>
        /// Set the event used to show the menu and submenu( If "SubmenuTriggerEvent" is "Default").
		/// </summary>
		[C1Description("C1Menu.TriggerEvent")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DefaultValue(TriggerEvent.Click)]
		#if !EXTENDER
		[Layout(LayoutType.Behavior)]
		#endif
		public TriggerEvent TriggerEvent
		{
			get
			{
				return GetPropertyValue("TriggerEvent", TriggerEvent.Click);
			}
			set
			{
				SetPropertyValue("TriggerEvent", value);
			}
		}

        /// <summary>
        /// Specifies the event used to show the submenu.
        /// </summary>
        [C1Description("C1Menu.SubmenuTriggerEvent")]
        [WidgetOption]
        [C1Category("Category.Behavior")]
        [DefaultValue(SubmenuTriggerEvent.Default)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public SubmenuTriggerEvent SubmenuTriggerEvent
        {
            get
            {
                return GetPropertyValue("SubmenuTriggerEvent", SubmenuTriggerEvent.Default);
            }
            set
            {
                SetPropertyValue("SubmenuTriggerEvent", value);
            }
        }

		/// <summary>
		/// Specifies the location and orientation of the menu relative to the button or link used to open it. 
		/// Configuration for the Position Utility Of option is excluded; 
		/// it is always configured by the widget. Collision also controls collision detection automatically.
		/// </summary>
		[C1Description("C1Menu.Position")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		#if !EXTENDER
		[Layout(LayoutType.Appearance)]
		#endif
		public PositionSettings Position
		{
			get
			{
				if (_positionSettings == null)
				{
					_positionSettings = new PositionSettings();
				}
				return _positionSettings;
			}
			set
			{
				_positionSettings = value;
			}
		}

		/// <summary>
		/// Defines the animation to show or hide the submenu in flyout mode. 
		/// If showAnimation or hideAnimation is not specified use this property.
		/// </summary>
		[C1Description("C1Menu.Animation")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		#if !EXTENDER
		[Layout(LayoutType.Behavior)]
		#endif
		public Animation Animation
		{
			get
			{
				if (_animation == null)
				{
					_animation = new Animation();
				}
				return _animation;
			}
			set
			{
				_animation = value;
			}
		}

		/// <summary>
		/// Defines the show animation to show submenu in flyout mode.
		/// </summary>
		[C1Description("C1Menu.ShowAnimation")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		#if !EXTENDER
		[Layout(LayoutType.Behavior)]
		#endif
		public Animation ShowAnimation
		{
			get
			{
				if (_showAnimation == null)
				{
					_showAnimation = new Animation();
				}
				return _showAnimation;
			}
			set
			{
				_showAnimation = value;
			}
		}

		/// <summary>
		/// Defines the animation to hide submenu in flyout mode.
		/// </summary>
		[C1Description("C1Menu.HideAnimation")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		#if !EXTENDER
		[Layout(LayoutType.Behavior)]
		#endif
		public Animation HideAnimation
		{
			get
			{
				if (_hideAnimation == null)
				{
					_hideAnimation = new Animation();
				}
				return _hideAnimation;
			}
			set
			{
				_hideAnimation = value;
			}
		}

		/// <summary>
		/// Defines the sliding animation in slide mode.
		/// </summary>
		[C1Description("C1Menu.SlidingAnimation")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		#if !EXTENDER
		[Layout(LayoutType.Behavior)]
		#endif
		public SlideAnimation SlidingAnimation
		{
			get
			{
				if (_slidingAnimation == null)
				{
					_slidingAnimation = new SlideAnimation();
				}
				return _slidingAnimation;
			}
			set
			{
				_slidingAnimation = value;
			}
		}

		/// <summary>
		/// Defines the behavior of the submenu whether it is a popup menu or an iPod-style navigation list.
		/// </summary>
		[C1Description("C1Menu.Mode")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		[DefaultValue(MenuMode.Flyout)]
		#if !EXTENDER
		[Layout(LayoutType.Appearance)]
		#endif
		public MenuMode Mode
		{
			get
			{
				return GetPropertyValue("Mode", MenuMode.Flyout);
			}
			set
			{
				SetPropertyValue("Mode", value);
			}
		}

		// to do superpanel options.

		/// <summary>
		/// Controls the root menu's orientation. All submenus are vertical regardless of the 
		/// orientation of the root menu.
		/// </summary>
		[C1Description("C1Menu.Orientation")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
		[DefaultValue(Orientation.Horizontal)]
		#if !EXTENDER
		[Layout(LayoutType.Appearance)]
		#endif
		public Orientation Orientation
		{
			get
			{
				return GetPropertyValue("Orientation", Orientation.Horizontal);
			}
			set
			{
				SetPropertyValue("Orientation", value);
			}
		}
		/// <summary>
		/// A value that indicates menu's direction.
		/// </summary>
		[C1Description("C1Menu.Direction")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
		[DefaultValue(Direction.Ltr)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public Direction Direction
		{
			get
			{
				return GetPropertyValue("Direction", Direction.Ltr);
			}
			set
			{
				SetPropertyValue("Direction", value);
			}
		}

		/// <summary>
		/// Determines the iPod-style menu's maximum height.
		/// </summary>
		[C1Description("C1Menu.MaxHeight")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
		[DefaultValue(200)]
		#if !EXTENDER
		[Layout(LayoutType.Appearance)]
		#endif
		public int MaxHeight
		{
			get
			{
				return GetPropertyValue("MaxHeight", 200);
			}
			set
			{
				if (MaxHeight < 0)
				{
					throw new ArgumentOutOfRangeException("Value", " 'Value' should not be less than 0.");
				}
				SetPropertyValue("MaxHeight", value);
			}
		}

		/// <summary>
		/// Determines whether the iPod menu shows a back link or a breadcrumb header in the menu.
		/// </summary>
		[C1Description("C1Menu.BackLink")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
		[DefaultValue(true)]
		#if !EXTENDER
		[Layout(LayoutType.Appearance)]
		#endif
		public bool BackLink
		{
			get
			{
				return GetPropertyValue("BackLink", true);
			}
			set
			{
				SetPropertyValue("BackLink", value);
			}
		}

		/// <summary>
		/// Gets and sets the text of the back link.
		/// </summary>
		[C1Description("C1Menu.BackLinkText")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
		[DefaultValue("Back")]
		#if !EXTENDER
		[Layout(LayoutType.Appearance)]
		#endif
		public string BackLinkText
		{
			get
			{
				return GetPropertyValue("BackLinkText", C1Localizer.GetString("C1Menu.BackLinkDefaultText"));
				//return GetPropertyValue("BackLinkText", "Back");
			}
			set
			{
				SetPropertyValue("BackLinkText", value);
			}
		}

		/// <summary>
		/// Gets and sets the text of the top link.
		/// </summary>
		[C1Description("C1Menu.TopLinkText")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
		[DefaultValue("All")]
		#if !EXTENDER
		[Layout(LayoutType.Appearance)]
		#endif
		public string TopLinkText
		{
			get
			{
				return GetPropertyValue("TopLinkText", C1Localizer.GetString("C1Menu.TopLinkDefaultText"));
				//return GetPropertyValue("TopLinkText", "All");
			}
			set
			{
				SetPropertyValue("TopLinkText", value);
			}
		}

		/// <summary>
		/// Gets and sets the top breadcrumb's default text.
		/// </summary>
		[C1Description("C1Menu.CrumbDefaultText")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		[DefaultValue("Choose an option")]
		#if !EXTENDER
		[Layout(LayoutType.Appearance)]
		#endif
		public string CrumbDefaultText
		{
			get
			{
                return GetPropertyValue("CrumbDefaultText", C1Localizer.GetString("C1Menu.CrumbDefaultDafaultText"));
				//return GetPropertyValue("CrumbDefaultText", "Choose an option");
			}
			set
			{
				SetPropertyValue("CrumbDefaultText", value);
			}
		}

		/// <summary>
		/// Gets or sets delay time to show submenu. 
		/// </summary>
		[C1Description("C1Menu.ShowDelay")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DefaultValue(400)]
		#if !EXTENDER
		[Layout(LayoutType.Behavior)]
		#endif
		public int ShowDelay 
		{
			get
			{
				return GetPropertyValue<int>("ShowDelay", 400);
			}
			set
			{
				SetPropertyValue<int>("ShowDelay", value);
			}
		}

		/// <summary>
		/// Gets or sets delay time to hide submenu.
		/// </summary>
		[C1Description("C1Menu.HideDelay")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DefaultValue(400)]
		#if !EXTENDER
		[Layout(LayoutType.Behavior)]
		#endif
		public int HideDelay 
		{
			get
			{
				return GetPropertyValue<int>("HideDelay", 400);
			}
			set
			{
				SetPropertyValue<int>("HideDelay", value);
			}
		}

		/// <summary>
        /// Gets or sets the item can be checked
		/// </summary>
        [C1Description("C1Menu.Checkable")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		#if !EXTENDER
		[Layout(LayoutType.Behavior)]
		#endif
		public bool Checkable
		{
			get
			{
                return GetPropertyValue<bool>("Checkable", false);
			}
			set
			{
                SetPropertyValue<bool>("Checkable", value);
			}
		}

		/// <summary>
		/// A value indicating the submenu will be append to the body or menu container.
		/// If the value is true, the submenu will be appended to body element,
		/// else it will append to the menu container.
		/// Default: false.
		/// Type: Boolean.
		/// </summary>
		[WidgetOption]
		[C1Description("C1Menu.EnsureSubmenuOnBody")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool EnsureSubmenuOnBody
		{
			get
			{
				return GetPropertyValue("EnsureSubmenuOnBody", false);
			}
			set
			{
				SetPropertyValue("EnsureSubmenuOnBody", value);
			}
		}

		#region ** client events

		/// <summary>
		/// Triggered when a menu item gets the focus, either when the mouse is used to 
		/// hover over it (on hover) or when the cursor keys are used on the 
		/// keyboard (navigation with cursor key) focus.
		/// </summary>
		[C1Description("C1Menu.OnClientFocus")]
        [C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e")]
		[DefaultValue("")]
		[WidgetOptionName("focus")]
		#if !EXTENDER
		[Layout(LayoutType.Behavior)]
		#endif
		public string OnClientFocus
		{
			get
			{
				return GetPropertyValue("OnClientFocus", "");
			}
			set
			{
				SetPropertyValue("OnClientFocus", value);
			}
		}

		/// <summary>
		/// Triggered when a menu item loses focus.
		/// </summary>
		[C1Description("C1Menu.OnClientBlur")]
        [C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e")]
		[DefaultValue("")]
		[WidgetOptionName("blur")]
		#if !EXTENDER
		[Layout(LayoutType.Behavior)]
		#endif
		public string OnClientBlur
		{
			get
			{
				return GetPropertyValue("OnClientBlur", "");
			}
			set
			{
				SetPropertyValue("OnClientBlur", value);
			}
		}

		/// <summary>
		/// Triggered when a menu item is selected.
		/// </summary>
		[C1Description("C1Menu.OnClientSelect")]
        [C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e")]
		[DefaultValue("")]
		[WidgetOptionName("select")]
		#if !EXTENDER
		[Layout(LayoutType.Behavior)]
		#endif
		public string OnClientSelect
		{
			get
			{
				return GetPropertyValue("OnClientSelect", "");
			}
			set
			{
				SetPropertyValue("OnClientSelect", value);
			}
		}
        /// <summary>
        /// Triggered before showing a menu or submenu.
        /// </summary>
        [C1Description("C1Menu.OnClientShowing")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("e, item")]
        [DefaultValue("")]
        [WidgetOptionName("showing")]
        #if !EXTENDER
        [Layout(LayoutType.Behavior)]
        #endif
        public string OnClientShowing
        {
            get
            {
                return GetPropertyValue("OnClientShowing", "");
            }
            set
            {
                SetPropertyValue("OnClientShowing", value);
            }
        }
        /// <summary>
        /// Triggered once a menu or submenu has shown.
        /// </summary>
        [C1Description("C1Menu.OnClientShown")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("e, item")]
        [DefaultValue("")]
        [WidgetOptionName("shown")]
        #if !EXTENDER
        [Layout(LayoutType.Behavior)]
        #endif
        public string OnClientShown
        {
            get
            {
                return GetPropertyValue("OnClientShown", "");
            }
            set
            {
                SetPropertyValue("OnClientShown", value);
            }
        }
        /// <summary>
        /// Triggered before hidding a menu or submenu.
        /// </summary>
        [C1Description("C1Menu.OnClientHidding")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("e, item")]
        [DefaultValue("")]
        [WidgetOptionName("hidding")]
        #if !EXTENDER
        [Layout(LayoutType.Behavior)]
        #endif
        public string OnClientHidding
        {
            get
            {
                return GetPropertyValue("OnClientHidding", "");
            }
            set
            {
                SetPropertyValue("OnClientHidding", value);
            }
        }
        /// <summary>
        /// Triggered once a menu or submenu has hidden.
        /// </summary>
        [C1Description("C1Menu.OnClientHidden")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("e, item")]
        [DefaultValue("")]
        [WidgetOptionName("hidden")]
        #if !EXTENDER
        [Layout(LayoutType.Behavior)]
        #endif
        public string OnClientHidden
        {
            get
            {
                return GetPropertyValue("OnClientHidden", "");
            }
            set
            {
                SetPropertyValue("OnClientHidden", value);
            }
        }
		#endregion

		#endregion

		#region ** methods for serialization
		/// <summary>
		/// Determine whether the Animation property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeAnimation()
		{
			return this.Animation.Animated.Disabled != false
				|| this.Animation.Animated.Effect != "slide"
				|| this.Animation.Duration != 400
				|| this.Animation.Option.Count != 0
				|| this.Animation.Easing != Easing.Swing;
		}

		/// <summary>
		/// Determines whether the HideAnimation property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Return true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeHideAnimation()
		{
			return this.HideAnimation.Animated.Disabled != false
				|| this.HideAnimation.Animated.Effect != "fade"
				|| this.HideAnimation.Duration != 400
				|| this.HideAnimation.Easing!= Easing.Swing
				|| this.HideAnimation.Option.Count != 0;
		}

		/// <summary>
		/// Determines whether the ShowAnimation property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Return true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeShowAnimation()
		{
			return this.ShowAnimation.Animated.Disabled != false
				|| this.ShowAnimation.Animated.Effect != "slide"
				|| this.ShowAnimation.Duration != 400
				|| this.ShowAnimation.Easing!= Easing.Swing
				|| this.ShowAnimation.Option.Count != 0;
		}

		/// <summary>
		/// Determines whether the SlidingAnimation property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Return true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeSlidingAnimation()
		{
			return this.SlidingAnimation.Disabled != false
				|| this.SlidingAnimation.Duration != 400
				|| this.SlidingAnimation.Easing != Easing.Swing;
		}

		/// <summary>
		/// Determines whether the Position property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Return true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializePosition()
		{
			return this.Position.At.Left != HPosition.Left
				|| this.Position.At.Top != VPosition.Top
				|| this.Position.My.Left != HPosition.Left
				|| this.Position.My.Top != VPosition.Top
				|| this.Position.Collision.Left != CollisionMode.Flip
				|| this.Position.Collision.Top != CollisionMode.Flip
				|| this.Position.Offset.Left != 0
				|| this.Position.Offset.Top != 0;
		}

		#endregion end of ** methods for serialization.
	}
}
