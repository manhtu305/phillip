﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{

	#region ** HPosition enumerator
	/// <summary>
	/// For jQuery ui position utility's "my" and "at" option.  
	/// The value contains two words combined by blank character.  This is the first word. 
	/// </summary>
	public enum HPosition
	{
		/// <summary>
		/// Left side
		/// </summary>
		Left = 0,
		/// <summary>
		/// Center side
		/// </summary>
		Center = 1,
		/// <summary>
		/// Right side
		/// </summary>
		Right = 2
	}
	#endregion end of ** HPosition enumerator.

	#region ** VPosition enumerator
	/// <summary>
	/// For jQuery ui position utility's "my" and "at" option.  
	/// The value contains two words combined by blank character.  This is the second word.   
	/// </summary>
	public enum VPosition
	{
		/// <summary>
		/// Top side
		/// </summary>
		Top = 0,
		/// <summary>
		/// Center side
		/// </summary>
		Center = 1,
		/// <summary>
		/// Bottom side
		/// </summary>
		Bottom = 2
	}
	#endregion end of ** VPosition enumerator.

	#region ** CollisionMode enumerator
	/// <summary>
	/// When the positioned element overflows the window in some direction, 
	/// move it to an alternative position
	/// </summary>
	public enum CollisionMode
	{
		/// <summary>
		/// To the opposite side and the collision detection is run again to see if it will fit. 
		/// If it won't fit in either position, the center option should be used as a fall back.
		/// </summary>
		Flip = 0,
		/// <summary>
		/// Not do collision detection.
		/// </summary>
		None = 1,
		/// <summary>
		/// So the element keeps in the desired direction, but is re-positioned so it fits.
		/// </summary>
		Fit = 2
	}
	#endregion end of ** CollisionMode enumerator

	#region ** Compass enumerator
	/// <summary>
	/// The Compass enumerator is used to define the compass.
	/// </summary>
	[Flags]
	public enum Compass
	{
		/// <summary>
		/// North side
		/// </summary>
		N = 1,
		/// <summary>
		/// East side
		/// </summary>
		E = 2,
		/// <summary>
		/// South side
		/// </summary>
		S = 4,
		/// <summary>
		/// West side
		/// </summary>
		W = 8,
		/// <summary>
		/// NorthEast side
		/// </summary>
		NE = 16,
		/// <summary>
		/// SouthEast side
		/// </summary>
		SE = 32,
		/// <summary>
		/// SouthWest side
		/// </summary>
		SW = 64,
		/// <summary>
		/// NorthWest side
		/// </summary>
		NW = 128,
		/// <summary>
		/// All sides
		/// </summary>
		All = 255,
	}
	#endregion end of ** Compass enumerator.

	#region ** Orientation enum
	/// <summary>
	/// Orientation type
	/// </summary>
	public enum Orientation
	{
		/// <summary>
		/// Horizontal
		/// </summary>
		Horizontal = 0,
		/// <summary>
		/// Vertical
		/// </summary>
		Vertical = 1
	}

	public enum Direction
	{
		/// <summary>
		/// Left to right.
		/// </summary>
		Ltr = 0,
		/// <summary>
		/// Right to left.
		/// </summary>
		Rtl = 1,
	}
	#endregion end of ** Orientation enum


	/// <summary>
	/// Specifies the transition for objects being animated.
	/// </summary>
	public enum Easing
	{
		/// <summary>
		/// Linear
		/// </summary>
		Linear = 1,
		/// <summary>
		/// Swing
		/// </summary>
		Swing = 2,
		/// <summary>
		/// EaseInQuad
		/// </summary>
		EaseInQuad = 3,
		/// <summary>
		/// EaseOutQuad
		/// </summary>
		EaseOutQuad = 4,
		/// <summary>
		/// EaseInOutQuad
		/// </summary>
		EaseInOutQuad = 5,
		/// <summary>
		/// EaseInCubic
		/// </summary>
		EaseInCubic = 6,
		/// <summary>
		/// EaseOutCubic
		/// </summary>
		EaseOutCubic = 7,
		/// <summary>
		/// EaseInOutCubic
		/// </summary>
		EaseInOutCubic = 8,
		/// <summary>
		/// EaseInQuart
		/// </summary>
		EaseInQuart = 9,
		/// <summary>
		/// EaseOutQuart
		/// </summary>
		EaseOutQuart = 10,
		/// <summary>
		/// EaseInOutQuart
		/// </summary>
		EaseInOutQuart = 11,
		/// <summary>
		/// EaseInQuint
		/// </summary>
		EaseInQuint = 12,
		/// <summary>
		/// EaseOutQuint
		/// </summary>
		EaseOutQuint = 13,
		/// <summary>
		/// EaseInOutQuint
		/// </summary>
		EaseInOutQuint = 14,
		/// <summary>
		/// EaseInSine
		/// </summary>
		EaseInSine = 15,
		/// <summary>
		/// EaseOutSine
		/// </summary>
		EaseOutSine = 16,
		/// <summary>
		/// EaseInOutSine
		/// </summary>
		EaseInOutSine = 17,
		/// <summary>
		/// EaseInExpo
		/// </summary>
		EaseInExpo = 18,
		/// <summary>
		/// EaseOutExpo
		/// </summary>
		EaseOutExpo = 19,
		/// <summary>
		/// EaseInOutExpo
		/// </summary>
		EaseInOutExpo = 20,
		/// <summary>
		/// EaseInCirc
		/// </summary>
		EaseInCirc = 21,
		/// <summary>
		/// EaseOutCirc
		/// </summary>
		EaseOutCirc = 22,
		/// <summary>
		/// EaseInOutCirc
		/// </summary>
		EaseInOutCirc = 23,
		/// <summary>
		/// EaseInElastic
		/// </summary>
		EaseInElastic = 24,
		/// <summary>
		/// EaseOutElastic
		/// </summary>
		EaseOutElastic = 25,
		/// <summary>
		/// EaseInOutElastic
		/// </summary>
		EaseInOutElastic = 26,
		/// <summary>
		/// EaseInBack
		/// </summary>
		EaseInBack = 27,
		/// <summary>
		/// EaseOutBack
		/// </summary>
		EaseOutBack = 28,
		/// <summary>
		/// EaseInOutBack
		/// </summary>
		EaseInOutBack = 29,
		/// <summary>
		/// EaseInBounce
		/// </summary>
		EaseInBounce = 30,
		/// <summary>
		/// EaseOutBounce
		/// </summary>
		EaseOutBounce = 31,
		/// <summary>
		/// EaseInOutBounce
		/// </summary>
		EaseInOutBounce=32
	}

	/// <summary>
	/// SelectionMode.
	/// </summary>
	public enum SelectionMode
	{
		/// <summary>
		/// Single
		/// </summary>
		Single = 0,
		/// <summary>
		/// Multiple
		/// </summary>
		Multiple,
	}

	/// <summary>
	/// Ajax request type.
	/// </summary>
	public enum RequestType
	{
		/// <summary>
		/// GET
		/// </summary>
		Get = 0,
		/// <summary>
		/// POST
		/// </summary>
		Post,
	}
}
