using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Web.UI;

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
#if ASP_NET4
[assembly: AssemblyVersion("4.0.20123.4")]
#elif ASP_NET35
[assembly: AssemblyVersion("3.5.20123.4")]
#else
[assembly: AssemblyVersion("2.0.20123.4")]
#endif
