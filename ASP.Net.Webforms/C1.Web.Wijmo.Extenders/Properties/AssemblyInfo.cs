using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Web.UI;
using System.Security;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

#if GRAPECITY
#if ASP_NET4
[assembly: AssemblyTitle("ComponentOne Wijmo Extenders for ASP.NET (CLR 4.0) JPN")]
[assembly: AssemblyDescription("ComponentOne Wijmo Extenders for ASP.NET (CLR 4.0) JPN")]
[assembly: AssemblyProduct("ComponentOne Studio for ASP.NET AJAX (CLR 4.0) JPN")]
#elif ASP_NET35
[assembly: AssemblyTitle("ComponentOne Wijmo Extenders for ASP.NET (CLR 3.5) JPN")]
[assembly: AssemblyDescription("ComponentOne Wijmo Extenders for ASP.NET (CLR 3.5) JPN")]
[assembly: AssemblyProduct("ComponentOne Studio for ASP.NET AJAX (CLR 3.5) JPN")]
#else
[assembly: AssemblyTitle("ComponentOne Wijmo Extenders for ASP.NET (CLR 2.0) JPN")]
[assembly: AssemblyDescription("ComponentOne Wijmo Extenders for ASP.NET (CLR 2.0) JPN")]
[assembly: AssemblyProduct("ComponentOne Studio for ASP.NET AJAX (CLR 2.0) JPN")]
#endif
#else
#if ASP_NET4
[assembly: AssemblyTitle("ComponentOne Extenders for ASP.NET Wijmo (CLR 4.0)")]
[assembly: AssemblyDescription("ComponentOne Extenders for ASP.NET Wijmo (CLR 4.0)")]
[assembly: AssemblyProduct("ComponentOne Studio for ASP.NET Wijmo (CLR 4.0)")]
#elif ASP_NET35
[assembly: AssemblyTitle("ComponentOne Extenders for ASP.NET Wijmo (CLR 3.5)")]
[assembly: AssemblyDescription("ComponentOne Extenders for ASP.NET Wijmo (CLR 3.5)")]
[assembly: AssemblyProduct("ComponentOne Studio for ASP.NET Wijmo (CLR 3.5)")]
#else
[assembly: AssemblyTitle("ComponentOne Extenders for ASP.NET Wijmo (CLR 2.0)")]
[assembly: AssemblyDescription("ComponentOne Extenders for ASP.NET Wijmo (CLR 2.0)")]
[assembly: AssemblyProduct("ComponentOne Studio for ASP.NET Wijmo (CLR 2.0)")]
#endif
#endif

[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("GrapeCity, Inc.")]
[assembly: AssemblyCopyright("Copyright © GrapeCity, Inc.  All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// CLS compliance:
//[assembly: System.CLSCompliantAttribute(true)] //todo:?

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("dab2d3e2-21e4-4e63-b39f-eb4a1c3b4f2c")]

#if ASP_NET4
[assembly: SecurityRules(SecurityRuleSet.Level1)] 
#endif

[assembly: System.Drawing.BitmapSuffixInSameAssembly()]
[assembly: AllowPartiallyTrustedCallers()]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyName("")]

[assembly: AssemblyVersion(C1.Wijmo.Licensing.VersionConst.VerString)]

// --- todo: update product information

// licensing support
[assembly: C1.Util.Licensing.C1ProductInfo("82", "9A0EC944-F0FC-4780-B14A-CEED103C2E05")]
// Studio Enterprise
[assembly: C1.Util.Licensing.C1ProductInfo("SE", "724e8a91-af12-4a3b-9aeb-ef89612e692e")]
// Studio for ASP.NET
[assembly: C1.Util.Licensing.C1ProductInfo("S9", "08f7d405-7096-4b5f-a288-f749b8c83e6a")]
// OLAP for ASP.NET
[assembly: C1.Util.Licensing.C1ProductInfo("96", "23793597-d4b0-423b-ae00-a06178993ffd")]
// Studio Ultimate
[assembly: C1.Util.Licensing.C1ProductInfo("SU", "757CCC59-F365-4325-A676-0674C656B7A2")]
// Studio for Web
[assembly: C1.Util.Licensing.C1ProductInfo("SW", "D5B51125-3A50-41A7-85F3-9F8CD6F639E9")]
// ASP.NET WebForms
[assembly: C1.Util.Licensing.C1ProductInfo("AW", "35B13E10-48BC-4D43-B7BE-697B033A9B47")]

#if BETA
[assembly: C1.Win.C1BetaExpiryDate("6/15/2011")]
#endif

#if false
#if ASP_NET45
[assembly:InternalsVisibleTo("C1.Web.Wijmo.Design.45, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c1bd0a0680e736d0bea3881ff835b56a8cb56c82ed17edc4714140bed28d7d504ba64753dcf227a35b1bab1a43bd83d1fc916fdf98e6daf5aa19f216831b11334657352c4c848ea47cde573158d1c86ec7efbced83cb62ae2550bd0fd0da2cf48593567ceb65d1e02f6d96ebebdbb83e39db493bb2ca1c301eebca0cb8e5abe4")]
#elif ASP_NET4
[assembly:InternalsVisibleTo("C1.Web.Wijmo.Design.4, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c1bd0a0680e736d0bea3881ff835b56a8cb56c82ed17edc4714140bed28d7d504ba64753dcf227a35b1bab1a43bd83d1fc916fdf98e6daf5aa19f216831b11334657352c4c848ea47cde573158d1c86ec7efbced83cb62ae2550bd0fd0da2cf48593567ceb65d1e02f6d96ebebdbb83e39db493bb2ca1c301eebca0cb8e5abe4")]
#elif ASP_NET35
[assembly: InternalsVisibleTo("C1.Web.Wijmo.Design.3, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c1bd0a0680e736d0bea3881ff835b56a8cb56c82ed17edc4714140bed28d7d504ba64753dcf227a35b1bab1a43bd83d1fc916fdf98e6daf5aa19f216831b11334657352c4c848ea47cde573158d1c86ec7efbced83cb62ae2550bd0fd0da2cf48593567ceb65d1e02f6d96ebebdbb83e39db493bb2ca1c301eebca0cb8e5abe4")]
#else
[assembly:InternalsVisibleTo("C1.Web.Wijmo.Design.2, PublicKey=0024000004800000940000000602000000240000525341310004000001000100c1bd0a0680e736d0bea3881ff835b56a8cb56c82ed17edc4714140bed28d7d504ba64753dcf227a35b1bab1a43bd83d1fc916fdf98e6daf5aa19f216831b11334657352c4c848ea47cde573158d1c86ec7efbced83cb62ae2550bd0fd0da2cf48593567ceb65d1e02f6d96ebebdbb83e39db493bb2ca1c301eebca0cb8e5abe4")]
#endif
#endif

[assembly: TagPrefix("C1.Web.Wijmo.Extenders", "Wijmo")]
