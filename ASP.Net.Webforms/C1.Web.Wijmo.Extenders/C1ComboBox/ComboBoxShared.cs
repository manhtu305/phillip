﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.WebControls;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1ComboBox", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1ComboBox
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1ComboBox", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1ComboBox
#endif
{
    [WidgetDependencies(
        typeof(WijCombobox)
#if !EXTENDER
, "extensions.c1combobox.js",
ResourcesConst.C1WRAPPER_PRO
#endif
)]
#if EXTENDER
	public partial class C1ComboBoxExtender
#else
    public partial class C1ComboBox
#endif
    {
        #region ** fields

        private PositionSettings _dropDownListPosition = null;
        private Animation _showingAnimation = null;
        private Animation _hidingAnimation = null;

        #endregion

        #region ** options

        /// <summary>
        /// A value that specifies the text in the wijcombobox label.
        /// Default: null.
        /// Type: String.
        /// </summary>
        [C1Description("C1ComboBox.LabelText")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
        [DefaultValue("")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public string LabelText
        {
            get
            {
                return GetPropertyValue("LabelText", "");
            }
            set
            {
                SetPropertyValue("LabelText", value);
            }
        }

        /// <summary>
        /// A value that determines the minimum length of text 
        /// that can be entered in the wijcombobox text box to issue an AJAX request.
        /// Default: 4.
        /// Type: Number.
        /// </summary>
        [WidgetOption]
        [C1Description("C1ComboBox.MinLength")]
        [C1Category("Category.Behavior")]
        [DefaultValue(4)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int MinLength
        {
            get
            {
                return GetPropertyValue("MinLength", 4);
            }
            set
            {
                SetPropertyValue("MinLength", value);
            }
        }

        /// <summary>
        /// A value that determines the duration (in milliseconds) of the time 
        /// to delay before autocomplete begins after typing stops.
        /// Default: 300.
        /// Type: Number.
        /// </summary>
        [WidgetOption]
        [C1Description("C1ComboBox.Delay")]
        [C1Category("Category.Behavior")]
        [DefaultValue(300)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int Delay
        {
            get
            {
                return GetPropertyValue("Delay", 300);
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("Value", " 'Delay' should not be less than 0.");
                }
                SetPropertyValue("Delay", value);
            }
        }

        /// <summary>
        /// A value that determines whether to show the trigger of wijcombobox.
        /// Default: true.
        /// Type: Boolean.
        /// </summary>
        [WidgetOption]
        [C1Description("C1ComboBox.ShowTrigger")]
        [C1Category("Category.Appearance")]
        [DefaultValue(true)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public bool ShowTrigger
        {
            get
            {
                return GetPropertyValue("ShowTrigger", true);
            }
            set
            {
                SetPropertyValue("ShowTrigger", value);
            }
        }

        /// <summary>
        /// A value that specifies the position of the drop-down list trigger.
        /// Default: "right".
        /// Type: String.
        /// </summary>
        [WidgetOption]
        [C1Description("C1ComboBox.TriggerPosition")]
        [C1Category("Category.Appearance")]
        [DefaultValue(TriggerPosition.Right)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public TriggerPosition TriggerPosition
        {
            get
            {
                return GetPropertyValue("TriggerPosition", TriggerPosition.Right);
            }
            set
            {
                SetPropertyValue("TriggerPosition", value);
            }
        }

        /// <summary>
        /// A value that specifies the height of the drop-down list.
        /// Default: 300.
        /// Type: Number.
        /// </summary>
        /// <remarks>
        /// If the total height of all items is less than the value of this option,
        /// it will use the total height of items as the height of the drop-down list.
        /// </remarks>
        [WidgetOption]
        [C1Description("C1ComboBox.DropdownHeight")]
        [C1Category("Category.Appearance")]
        [DefaultValue(300)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public int DropdownHeight
        {
            get
            {
                return GetPropertyValue("DropdownHeight", 300);
            }
            set
            {
                SetPropertyValue("DropdownHeight", value);
            }
        }

        /// <summary>
        /// A value that specifies the width of the drop-down list.
        /// Default: "-1".
        /// Type: Number.
        /// </summary>
        /// <remarks>
        /// When this option is set to "-1", the width of the drop-down
        /// list is equal to the width of wijcombobox.
        /// </remarks>
        [WidgetOption]
        [C1Description("C1ComboBox.DropdownWidth")]
        [C1Category("Category.Appearance")]
        [DefaultValue(-1)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public int DropdownWidth
        {
            get
            {
                return GetPropertyValue("DropdownWidth", -1);
            }
            set
            {
                SetPropertyValue("DropdownWidth", value);
            }
        }

        /// <summary>
        /// A value that determines whether to select the item 
        /// when the item gains focus or is activated.
        /// Default: false.
        /// Type: Boolean.
        /// </summary>
        [WidgetOption]
        [C1Description("C1ComboBox.SelectOnItemFocus")]
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool SelectOnItemFocus
        {
            get
            {
                return GetPropertyValue("SelectOnItemFocus", false);
            }
            set
            {
                SetPropertyValue("SelectOnItemFocus", value);
            }
        }

        /// <summary>
        /// A value determines whether to shorten the drop-down list items 
        /// by matching the text in the textbox after typing.
        /// Default: true.
        /// Type: Boolean.
        /// </summary>
        [WidgetOption]
        [C1Description("C1ComboBox.AutoFilter")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool AutoFilter
        {
            get
            {
                return GetPropertyValue("AutoFilter", true);
            }
            set
            {
                SetPropertyValue("AutoFilter", value);
            }
        }

        /// <summary>
        /// A value indicating the dropdown element will be append to the body or combobox container.
        /// If the value is true, the dropdown list will be appended to body element,
        /// else it will append to the combobox container.
        /// Default: true.
        /// Type: Boolean.
        /// </summary>
        [WidgetOption]
        [C1Description("C1ComboBox.EnsureDropDownOnBody")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool EnsureDropDownOnBody
        {
            get
            {
                return GetPropertyValue("EnsureDropDownOnBody", true);
            }
            set
            {
                SetPropertyValue("EnsureDropDownOnBody", value);
            }
        }

        /// <summary>
        /// A value that determines whether to start the auto-complete 
        /// function after typing in the text if a match exists.
        /// Default: true.
        /// Type: Boolean.
        /// </summary>
        [WidgetOption]
        [C1Description("C1ComboBox.AutoComplete")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool AutoComplete
        {
            get
            {
                return GetPropertyValue("AutoComplete", true);
            }
            set
            {
                SetPropertyValue("AutoComplete", value);
            }
        }

        /// <summary>
        /// A value that determines whether to highlight the keywords in an item. 
        /// If "abc" is typed in the textbox, 
        /// all "abc" matches are highlighted in the drop-down list.
        /// Default: true.
        /// Type: Boolean.
        /// </summary>
        [WidgetOption]
        [C1Description("C1ComboBox.HighlightMatching")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool HighlightMatching
        {
            get
            {
                return GetPropertyValue("HighlightMatching", true);
            }
            set
            {
                SetPropertyValue("HighlightMatching", value);
            }
        }

        /// <summary>
        /// A value that specifies the position options of the drop-down list
        /// </summary>
        [WidgetOption]
        [C1Description("C1ComboBox.DropDownListPosition")]
        [C1Category("Category.Behavior")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public PositionSettings DropDownListPosition
        {
            get
            {
                if (_dropDownListPosition == null)
                {
                    _dropDownListPosition = new PositionSettings();
                }
                return _dropDownListPosition;
            }
            set
            {
                _dropDownListPosition = value;
            }
        }

        /// <summary>
        /// A value that specifies the selection mode of wijcombobox.
        /// Default: "Single".
        /// Type: String.
        /// <remarks>
        /// Possible options are: "single" and "multiple".
        /// </remars>
        /// </summary>
        [WidgetOption]
        [C1Description("C1ComboBox.SelectionMode")]
        [C1Category("Category.Behavior")]
        [DefaultValue(SelectionMode.Single)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public SelectionMode SelectionMode
        {
            get
            {
                return GetPropertyValue("SelectionMode", SelectionMode.Single);
            }
            set
            {
                SetPropertyValue("SelectionMode", value);
            }
        }

        /// <summary>
        /// A value that specifies the separator for 
        /// the multiple selected items text in the textbox.
        /// Default: ",".
        /// Type: String.
        /// </summary>
        [WidgetOption]
        [C1Description("C1ComboBox.MultipleSelectionSeparator")]
        [C1Category("Category.Behavior")]
        [DefaultValue(",")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string MultipleSelectionSeparator
        {
            get
            {
                return GetPropertyValue("MultipleSelectionSeparator", ",");
            }
            set
            {
                SetPropertyValue("MultipleSelectionSeparator", value);
            }
        }

        /// <summary>
        /// A value that determines whether to check the input text against 
        /// the text of the selected item when the focus blurs. 
        /// Default: false.
        /// Type: Boolean.
        /// </summary>
        /// <remarks>
        /// If the text does not match any item, input text will restore 
        /// to text the selected item or empty if no item is selected.  
        /// </remarks>
        [WidgetOption]
        [C1Description("C1ComboBox.ForceSelectionText")]
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool ForceSelectionText
        {
            get
            {
                return GetPropertyValue("ForceSelectionText", false);
            }
            set
            {
                SetPropertyValue("ForceSelectionText", value);
            }
        }

        /// <summary>
        /// A value that determines whether input is editable.
        /// Default: true.
        /// Type: Boolean.
        /// </summary>
        [WidgetOption]
        [C1Description("C1ComboBox.IsEditable")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool IsEditable
        {
            get
            {
                return GetPropertyValue("IsEditable", true);
            }
            set
            {
                SetPropertyValue("IsEditable", value);
            }
        }

        /**
        //update for issue 20253 by wh at 2012/3/1 
        /// <summary>
        /// A value that specifies the index of the item to select when using single mode.
        /// If the selectionMode is "multiple", then this option could be set 
        /// to an array of Number which contains the indices of the items to select.
        /// Default: -1.
        /// Type: Number/Array.
        /// </summary>
        /// <remarks>
        /// If no item is selected, it will return -1.
        /// </remarks>
        [WidgetOption]
        [C1Description("C1ComboBox.SelectedIndex")]		
        [C1Category("Category.Behavior")]
        [DefaultValue(-1)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int SelectedIndex
        {
            get
            {
                return GetPropertyValue("SelectedIndex", -1);
            }
            set
            {
                SetPropertyValue("SelectedIndex", value);
            }
        }
        //end for issue 20253 by wh at 2012/3/1
         */

        /// <summary>
        /// A value added to the width of the target select element 
        /// to account for the scroll bar width of superpanel.
        /// Default: 6.
        /// Type: Number.
        /// <remarks>
        /// Unit for this value is pixel.
        /// Because the width of the scroll bar may be different between browsers 
        /// if wijcombobox is initialized with the width of the HTML select element, 
        /// the text may be hidden by the scroll bar of wijcombobox. 
        /// </remarks>
        /// </summary>
        [WidgetOption]
        [C1Description("C1ComboBox.SelectElementWidthFix")]
        [DefaultValue(6)]
        [C1Category("Category.Behavior")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int SelectElementWidthFix
        {
            get
            {
                return GetPropertyValue("SelectElementWidthFix", 6);
            }
            set
            {
                SetPropertyValue("SelectElementWidthFix", value);
            }
        }

        /// <summary>
        /// A value that specifies the animation options for a drop-down list
        /// when it is visible.
        /// Default: null.
        /// Type: Object.
        /// </summary>
        [WidgetOption]
        [C1Description("C1ComboBox.ShowingAnimation")]
        [C1Category("Category.Behavior")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public Animation ShowingAnimation
        {
            get
            {
                if (_showingAnimation == null)
                {
                    _showingAnimation = new Animation();
                }
                return _showingAnimation;
            }
            set
            {
                _showingAnimation = value;
            }
        }

        /// <summary>
        /// A value specifies the animation options for the drop-down list
        /// when it is hidden.
        /// Default: null.
        /// Type: Object.
        /// </summary>
        [WidgetOption]
        [C1Description("C1ComboBox.HidingAnimation")]
        [C1Category("Category.Behavior")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public Animation HidingAnimation
        {
            get
            {
                if (_hidingAnimation == null)
                {
                    _hidingAnimation = new Animation();
                }
                return _hidingAnimation;
            }
            set
            {
                _hidingAnimation = value;
            }
        }

        #endregion

        #region ** methods for serialization

        /// <summary>
        /// Determines whether the ShowingAnimation should be serialized to client side.
        /// </summary>
        /// <returns>
        /// Return true if any subproperty is changed, otherwise return false.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeShowingAnimation()
        {
            return this.ShowingAnimation.Animated.Disabled != false
                || this.ShowingAnimation.Animated.Effect != "slide"
                || this.ShowingAnimation.Duration != 400
                || this.ShowingAnimation.Easing != Easing.Swing
                || this.ShowingAnimation.Option.Count != 0;
        }

        /// <summary>
        /// Determines whether the HidingAnimation property should be serialized to client side.
        /// </summary>
        /// <returns>
        /// Return true if any subproperty is changed, otherwise return false.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeHidingAnimation()
        {
            return this.HidingAnimation.Animated.Disabled != false
                || this.HidingAnimation.Animated.Effect != "slide"
                || this.HidingAnimation.Duration != 400
                || this.HidingAnimation.Easing != Easing.Swing
                || this.HidingAnimation.Option.Count != 0;
        }

        /// <summary>
        /// Determines whether the DropDownListPosition property should be serialized to client side.
        /// </summary>
        /// <returns>
        /// Return true if any subproperty is changed, otherwise return false.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeDropDownListPosition()
        {
            return this.DropDownListPosition.At.Left != HPosition.Left
                || this.DropDownListPosition.At.Top != VPosition.Top
                || this.DropDownListPosition.My.Left != HPosition.Left
                || this.DropDownListPosition.My.Top != VPosition.Top
                || this.DropDownListPosition.Collision.Left != CollisionMode.Flip
                || this.DropDownListPosition.Collision.Top != CollisionMode.Flip
                || this.DropDownListPosition.Offset.Left != 0
                || this.DropDownListPosition.Offset.Top != 0;
        }

        #endregion

        #region ** client events
        /// <summary>
        /// A function called when any item in list is selected.
        /// </summary>
        [C1Description("C1ComboBox.OnClientSelect")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent]
        [WidgetOptionName("select")]
        [DefaultValue("")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientSelect
        {
            get
            {
                return GetPropertyValue("OnClientSelect", "");
            }
            set
            {
                SetPropertyValue("OnClientSelect", value);
            }
        }

        /// <summary>
        /// A function called when drop-down list is opened.
        /// </summary>
        [C1Description("C1ComboBox.OnClientOpen")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent]
        [WidgetOptionName("open")]
        [DefaultValue("")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientOpen
        {
            get
            {
                return GetPropertyValue("OnClientOpen", "");
            }
            set
            {
                SetPropertyValue("OnClientOpen", value);
            }
        }

        /// <summary>
        /// A function called when drop-down list is closed.
        /// </summary>
        [C1Description("C1ComboBox.OnClientClose")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent]
        [WidgetOptionName("close")]
        [DefaultValue("")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientClose
        {
            get
            {
                return GetPropertyValue("OnClientClose", "");
            }
            set
            {
                SetPropertyValue("OnClientClose", value);
            }
        }

        /// <summary>
        /// A function called before searching the list.
        /// </summary>
        [C1Description("C1ComboBox.OnClientSearch")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent]
        [WidgetOptionName("search")]
        [DefaultValue("")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientSearch
        {
            get
            {
                return GetPropertyValue("OnClientSearch", "");
            }
            set
            {
                SetPropertyValue("OnClientSearch", value);
            }
        }

        /// <summary>
        /// A function called when select item is changed.
        /// </summary>
        ///[Obsolete("Use the OnClientSelectedIndexChanged property instead.")]
        [C1Description("C1ComboBox.OnClientChanged")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent]
        [WidgetOptionName("changed")]
        [DefaultValue("")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientChanged
        {
            get
            {
                return GetPropertyValue("OnClientChanged", "");
            }
            set
            {
                SetPropertyValue("OnClientChanged", value);
            }
        }

        /// <summary>
        /// A function called after the selected index of the C1ComboBox has changed.
        /// </summary>
        [C1Description("C1ComboBox.OnClientSelectedIndexChanged")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent]
        [WidgetOptionName("selectedIndexChanged")]
        [DefaultValue("")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientSelectedIndexChanged
        {
            get
            {
                return GetPropertyValue("OnClientSelectedIndexChanged", "");
            }
            set
            {
                SetPropertyValue("OnClientSelectedIndexChanged", value);
            }
        }

        /// <summary>
        /// A function called when the selected index of the C1ComboBox is about to change.
        /// </summary>
        [C1Description("C1ComboBox.OnClientSelectedIndexChanging")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent]
        [WidgetOptionName("selectedIndexChanging")]
        [DefaultValue("")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientSelectedIndexChanging
        {
            get
            {
                return GetPropertyValue("OnClientSelectedIndexChanging", "");
            }
            set
            {
                SetPropertyValue("OnClientSelectedIndexChanging", value);
            }
        }

        /// <summary>
        /// A function called when when the Text property is changed.
        /// </summary>
        [C1Description("C1ComboBox.OnClientTextChanged")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent]
        [WidgetOptionName("textChanged")]
        [DefaultValue("")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientTextChanged
        {
            get
            {
                return GetPropertyValue("OnClientTextChanged", "");
            }
            set
            {
                SetPropertyValue("OnClientTextChanged", value);
            }
        }
        #endregion end of ** client events.
    }
}
