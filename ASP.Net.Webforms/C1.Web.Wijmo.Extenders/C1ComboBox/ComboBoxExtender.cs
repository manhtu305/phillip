﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Extenders.Converters;
using C1.Web.Wijmo.Extenders.Localization;

[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1ComboBox", "wijmo")]

namespace C1.Web.Wijmo.Extenders.C1ComboBox
{

	/// <summary>
	/// ComboBox for the expander widget.
	/// </summary>
	[TargetControlType(typeof(Panel))]
	[TargetControlType(typeof(DropDownList))]
	[ToolboxBitmap(typeof(C1ComboBoxExtender), "ComboBox.png")]
	[LicenseProviderAttribute()]
    [ToolboxItem(true)]
    public partial class C1ComboBoxExtender : WidgetExtenderControlBase
	{
		private List<C1ComboBoxColumn> _coloumns = null;
		private bool _productLicensed = false;

		public C1ComboBoxExtender()
		{
			VerifyLicense();
			this.ShowingAnimation = new Animation();
			this.HidingAnimation = new Animation();
			this.DropDownListPosition = new PositionSettings();
			this.ListOptions = new C1List.C1ListExtender();
			this.Data = new DataSourceOption();
		}

		internal override bool IsWijMobileExtender
		{
			get
			{
				return false;
			}
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1ComboBoxExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		#region ** Options

		/// <summary>
		/// An array that specifies the column collections of wijcombobox.
		/// Example: columns: [{name: "header1", width: 150},
		///                    {name: "header2", width: 150},
		///                    {name: "header3", width: 150}]
		/// Default: [].
		/// Type: Array.
		/// </summary>
		[WidgetOption]
		[C1Description("C1ComboBox.Columns")]
		//[Category("Options")]
		[C1Category("Category.Data")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public List<C1ComboBoxColumn> Columns
		{
			get
			{
				if (_coloumns == null)
				{
					_coloumns = new List<C1ComboBoxColumn>();
				}
				return _coloumns;
			}
		}

		/// <summary>
		/// A value that specifies the underlying data source provider of wijcombobox
		/// </summary>
		[C1Description("C1ComboBox.Data")]
		[WidgetOption]
		//[Category("Options")]
		[C1Category("Category.Data")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		public DataSourceOption Data
		{
			get;
			set;
		}

		/// <summary>
		/// The object contains the options of wijlist
		/// </summary>
		[WidgetOption]
		[C1Description("C1ComboBox.ListOptions")]
		//[Category("Options")]
		[C1Category("Category.Appearance")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		public C1List.C1ListExtender ListOptions
		{
			get
			{
				return GetPropertyValue("ListOptions", new C1List.C1ListExtender());
			}
			set
			{
				SetPropertyValue("ListOptions", value);
			}
		}

		//update for 20174: get selectedvalue option by wh at 2012/2/19
		///<summary>
		/// Gets or sets the SelectedValue of <see cref="C1ComboBox"/>.
		///</summary>
		/// <example>
		/// <code lang="C#">
		/// C1ComboBox c = new C1ComboBox();
		/// c.Items.Add(new C1ComboBoxItem("test1"));
		/// c.Items.Add(new C1ComboBoxItem("test2"));
		/// // select the test2 item.
		/// c.SelectedValue = "test2";
		/// </code>
		/// </example>
		[WidgetOption]
		[C1Description("C1ComboBox.SelectedValue")]
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[C1Category("Category.Behavior")]
		[DefaultValue(null)]
		public virtual string SelectedValue
		{
			get
			{
				int selectedIndex = this.SelectedIndex;
				if (this.Data != null && this.Data.Items.Count > 0 && 
					selectedIndex >= 0)
				{
					return this.Data.Items[selectedIndex].Value;
				}
                return null;
			}
			set
			{
				if (this.Data.Items.Count != 0)
				{
					int index = FindItemIndexByValue(value);
					this.SelectedIndex = index;
				}
			}
		}
		//end for get selectedValue option

		//update for issue 20253 by wh at 2012/3/1
		/// <summary>
		/// A value that specifies the index of the item to select when using single mode.
		/// If the selectionMode is "multiple", then this option could be set 
		/// to an array of Number which contains the indices of the items to select.
		/// Default: -1.
		/// Type: Number/Array.
		/// </summary>
		/// <remarks>
		/// If no item is selected, it will return -1.
		/// </remarks>
		[WidgetOption]
		[C1Description("C1ComboBox.SelectedIndex")]
		[C1Category("Category.Behavior")]
		[DefaultValue(-1)]
		public int SelectedIndex
		{
			get
			{
				return GetPropertyValue("SelectedIndex", -1);
			}
			set
			{
				SetPropertyValue("SelectedIndex", value);
			}
		}
		//end for issue 20253 by wh at 2012/3/1
		#endregion  

		#region methods for serialization
		/// <summary>
		/// Determines whether the Data property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Return true if any subproperty is changed, otherwise return false.
		/// </returns>
		public bool ShouldSerializeData()
		{
			return this.Data.Items.Count != 0
				|| !string.IsNullOrEmpty(this.Data.DataSourceID);
		}

		/// <summary>
		/// Determines whether the Columns should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Return true if anysubproperty is changed, otherwise return false.
		/// </returns>
		public bool ShouldSerializeColumns()
		{
			return this.Columns.Count != 0;
		}

		/// <summary>
		/// Determines whether the ListOptions property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Return true if any subproperty is changed, otherwise return false.
		/// </returns>
		public bool ShouldSerializeListOptions()
		{
			return this.ListOptions.ListItems.Count != 0;
		}
		#endregion

		#region help method
		private int FindItemIndexByValue(string value)
		{
			if (this.Data != null)
			{
				List<C1ComboBoxItem> items = this.Data.Items;
				if (items != null && items.Count > 0)
				{
					for (int i = 0; i < items.Count; i++)
					{
						if (value != null && value.Equals(items[i].Value))
						{
							return i;
						}
					}
				}
			} 
			return -1;
		}
		#endregion
	}

	/// <summary>
	/// Represents a combobox Data property in the <see cref="C1ComboBoxExtender"/> extender.
	/// </summary>
	public class DataSourceOption : ICustomOptionType
	{
		private List<C1ComboBoxItem> _items;
		/// <summary>
		/// An Array data for combobox
		/// </summary>
		[Description("combobox item array")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[NotifyParentProperty(true)]
		public List<C1ComboBoxItem> Items
		{
			get
			{
				if (_items == null)
				{
					_items = new List<C1ComboBoxItem>();
				}
				return _items;
			}
		}

		[NotifyParentProperty(true)]
		public string DataSourceID
		{
			get;
			set;
		}

		#region ICustomOptionType Members

		string ICustomOptionType.SerializeValue()
		{
			if (string.IsNullOrEmpty(this.DataSourceID))
			{
				return WidgetObjectBuilder.SerializeOption(this.Items);
			}
			else
			{
				return this.DataSourceID;
			}
		}

		bool ICustomOptionType.IncludeQuotes
		{
			get
			{
				return false;
			}
		}

		#endregion
	}

}
