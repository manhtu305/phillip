﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Extenders.Localization;

namespace C1.Web.Wijmo.Extenders.C1ComboBox
{
	[DefaultProperty("Label")]
	public partial class C1ComboBoxItem
	{

		///<summary>
		/// Initializes a new instance of the <see cref="ComboBoxItem"/> class.
		///</summary>
		///<param name="text"><see cref="ComboBoxItem"/> Text.</param>
		///<param name="value"><see cref="ComboBoxItem"/> Value.</param>
		public C1ComboBoxItem(string text, string value)
		{
			Label = text;
			Value = value;
		}

        public C1ComboBoxItem()
        { 
            
        }

		/// <summary>
		/// item's label.
		/// </summary>
		[WidgetOption]
		[C1Description("C1ComboBox.C1ComboBoxItem.Label")]
		public string Label
		{
			get;
			set;
		}
	}
}
