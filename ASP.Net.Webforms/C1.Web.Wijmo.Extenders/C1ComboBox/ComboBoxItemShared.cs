﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;

namespace C1.Web.Wijmo.Extenders.C1ComboBox
{
	/// <summary>
	/// Represents a combobox items in the <see cref="C1ComboBoxExtender"/> extender. 
	/// </summary>
	public partial class C1ComboBoxItem : Settings
#else
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1ComboBox
{
	/// <summary>
	/// Represents a combobox items in the <see cref="C1ComboBox"/> control. 
	/// </summary>
	public partial class C1ComboBoxItem
#endif
	{
		private Hashtable _properties = new Hashtable();

		#region ** properties
		/// <summary>
		/// item's value
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1ComboBox.C1ComboBoxItem.Value")]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public string Value
		{
			get
			{
				return _properties["Value"] == null ? "" : _properties["Value"].ToString();
			}
			set
			{
				_properties["Value"] = value;
			}
		}

		/// <summary>
		/// Item's cells.
		/// </summary>
		[WidgetOption]
		[C1Description("C1ComboBox.C1ComboBoxItem.Cells")]
		[TypeConverter(typeof(StringArrayConverter))]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public string[] Cells
		{
			get
			{
				return _properties["Cells"] == null ? new string[0] : _properties["Cells"] as string[];
			}
			set
			{
				_properties["Cells"] = value;
			}
		}

		/// <summary>
		/// Determines the item is selected.
		/// </summary>
		[DefaultValue(false)]
		[C1Description("C1ComboBox.C1ComboBoxItem.Selected")]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Selected
		{
			get
			{
				return _properties["Selected"] == null ? false : (bool)_properties["Selected"];
			}
			set
			{
				_properties["Selected"] = value;
			}


		}
		#endregion end of ** properties.

		#region ** methods for serialization
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeCells()
		{
			if (this.Cells == null)
			{
				return false;
			}
			else
			{
				return this.Cells.Length > 0;
			}
		}
		#endregion
	}
}
