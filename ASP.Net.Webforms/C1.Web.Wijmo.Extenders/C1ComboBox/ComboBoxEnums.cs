﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1ComboBox
{
#else
namespace C1.Web.Wijmo.Controls.C1ComboBox
{
	/// <summary>
	/// Specifies the means of populating items for combobox at Callback mode.
	/// </summary>
	public enum CallbackDataPopulate
	{
		/// <summary>
		/// Populate ComboBoxItem to combobox in ItemPopulate event.
		/// </summary>
		ItemPopulate = 0,
		/// <summary>
		/// Set datasource to combobox in CallbackDataBind event.
		/// </summary>
		DataBind = 1,
	}

#endif
	/// <summary>
	/// Specifies the position of the drop-down list trigger
	/// </summary>
	public enum TriggerPosition
	{

		/// <summary>
		/// Left side
		/// </summary>
		Left=0,
		/// <summary>
		/// RightSide
		/// </summary>
		Right=1
	}
}
