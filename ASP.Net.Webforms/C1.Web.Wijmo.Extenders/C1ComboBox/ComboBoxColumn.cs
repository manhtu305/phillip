﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1ComboBox
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1ComboBox
#endif
{
#if EXTENDER
	/// <summary>
	/// Represents a combobox column in the <see cref="C1ComboBoxExtender"/> extender. 
	/// </summary>
	public class C1ComboBoxColumn : Settings
	{ 
#else
	/// <summary>
	/// Represents a combobox column in the <see cref="C1ComboBox"/> control. 
	/// </summary>
	public class C1ComboBoxColumn : Settings
	{
		/// <summary>
		/// Gets or sets the field in the data source from which to load cells value.
		/// </summary>
		[DefaultValue("")]
        [WidgetOption]
        [Layout(LayoutType.Data)]
		[C1Description("C1ComboBox.C1ComboBoxBindColumn.FieldName")] 
		public string DataField
		{
			get
			{
				return this.GetPropertyValue<string>("DataField", "");
			}
			set
			{
				this.SetPropertyValue<string>("DataField", value);
			}
		}

		/// <summary>
		/// The formatting applied to the cell field. For example,"{0:d}".
		/// </summary>
		[DefaultValue("")]
		[Layout(LayoutType.Data)]
		[C1Description("C1ComboBox.C1ComboBoxBindColumn.DataFormatString")]
		public string DataFormatString
		{
			get
			{
				return this.GetPropertyValue<string>("DataFormatString", "");
			}
			set
			{
				this.SetPropertyValue<string>("DataFormatString", value);
			}
		}
#endif
		/// <summary>
		/// The Name Property of Column for Wijmo ComboBox.
		/// </summary>
		[DefaultValue("")]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		[C1Description("C1ComboBox.C1ComboBoxBindColumn.Name")]
		public string Name
		{
			get
			{
				return this.GetPropertyValue<string>("Name", "");
			}
			set
			{
				this.SetPropertyValue<string>("Name", value);
			}
		}

		/// <summary>
		/// The Width Property of Column for Wijmo ComboBox.
		/// </summary>
		[WidgetOption]
		[DefaultValue(-1)]
#if !EXTENDER
		[Layout(LayoutType.Sizes)]
#endif
		[C1Description("C1ComboBox.C1ComboBoxBindColumn.Width")]
		public int Width
		{
			get
			{
				return this.GetPropertyValue<int>("Width", -1);
			}
			set
			{
				this.SetPropertyValue<int>("Width", value);
			}
		}
	}
}
