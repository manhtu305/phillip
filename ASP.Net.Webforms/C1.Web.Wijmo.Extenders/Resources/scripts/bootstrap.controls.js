﻿if ($.fn && $.fn.button && $.fn.button.noConflict) {
	$.fn.button.noConflict();
}

if ($.wijmo.wijreportviewer) {
	$.extend($.wijmo.wijreportviewer.prototype.options.wijCSS, $.wijmo.widget.prototype.options.wijCSS, {
		iconBookmark: "ui-icon-bookmark glyphicon glyphicon-bookmark",
		iconSearch: "ui-icon-search glyphicon glyphicon-search",
		iconImage: "ui-icon-image glyphicon glyphicon-picture",
		iconPrint: "ui-icon-print glyphicon glyphicon-print",
		iconDisk: "ui-icon-disk glyphicon glyphicon-floppy-disk",
		iconFullScreen: "ui-icon-newwin glyphicon glyphicon-fullscreen",
		iconContinousView: "ui-icon-carat-2-n-s glyphicon glyphicon-resize-vertical",
		iconZoomIn: "ui-icon-zoomin glyphicon glyphicon-zoom-in",
		iconZoomOut: "ui-icon-zoomout glyphicon glyphicon-zoom-out"
	});
}