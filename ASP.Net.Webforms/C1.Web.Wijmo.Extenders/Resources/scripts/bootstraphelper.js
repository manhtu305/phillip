﻿var widgetNameMapper = {
	c1inputcurrency: "wijinputnumber",
	c1inputnumeric: "wijinputnumber",
	c1inputpercent: "wijinputnumber",
	c1treeview: "wijtree",
	c1treeviewnode:"wijtreenode",
	c1gridview: "wijgrid"
}

if ($.wijmo.wijreportviewer) {
	$.extend($.wijmo.wijreportviewer.prototype.options.wijCSS, $.wijmo.widget.prototype.options.wijCSS, {
		iconBookmark: "ui-icon-bookmark glyphicon glyphicon-bookmark",
		iconSearch: "ui-icon-search glyphicon glyphicon-search",
		iconImage: "ui-icon-image glyphicon glyphicon-picture",
		iconPrint: "ui-icon-print glyphicon glyphicon-print",
		iconDisk: "ui-icon-disk glyphicon glyphicon-floppy-disk",
		iconFullScreen: "ui-icon-newwin glyphicon glyphicon-fullscreen",
		iconContinousView: "ui-icon-carat-2-n-s glyphicon glyphicon-resize-vertical",
		iconZoomIn: "ui-icon-zoomin glyphicon glyphicon-zoom-in",
		iconZoomOut: "ui-icon-zoomout glyphicon glyphicon-zoom-out"
	});
}

$.each($.wijmo, function (key, value) {
    if (key.toString().indexOf("c1") !== -1) {
        var widget = widgetNameMapper[key] ? $.wijmo[widgetNameMapper[key]] : $.wijmo[key.replace("c1", "wij")];

        if (widget) {
            $.extend(value.prototype.options.wijCSS, widget.prototype.options.wijCSS);
        }
    }
});