﻿/// <reference path="../../../../widgets/wijmo/External/declarations/jquery.d.ts"/>
(function ($) {


	$(document).bind("mobileinit", function () {
		$.mobile.ajaxEnabled = false;
	});

	//trigger visibility event for wijmo widgets.
	$(document).bind('pageshow', function () {
		if ($(this).wijTriggerVisibility) {
			$(this).wijTriggerVisibility();
		}
	});

} (jQuery));