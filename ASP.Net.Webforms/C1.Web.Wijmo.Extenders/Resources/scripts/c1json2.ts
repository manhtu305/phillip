﻿declare var __JSONC1;
// Copyright (c) 2008 ComponentOne L.L.C., All rights reserved.
// [MODIFIED json2 (http://www.json.org/json2.js)] -->
// LAST UPDATED: 2009.05.27 
// Syntax for dates:
// "YYYY-MM-DDThh:mm:ss:mssZ"

// Create a __JSONC1 object only if one does not already exist. We create the
// methods in a closure to avoid creating global variables.

if (!this.__JSONC1) {
	__JSONC1 = {};
}
(function () {

	function f(n, k) {
		// Format integers to have at least k digits.
		n = n.toString();
		while (n.length < k) {
			n = '0' + n;
		}
		return n;
	}

	/*
		Use IE8 native JSON support if any:
		JSON.parse(source, reviver)
		JSON.stringify(value, replacer, space)
        
	*/

	//"YYYY-MM-DDThh:mm:ss:mssZ", precision is taken into account
	//Example:2009-02-18T10:24:10:893Z
	// regular expession to test if given string is date: 
	// ^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?):(\d{3})Z$
	Date.prototype.c1ToJSON = function (key) {
		return "" + f(this.getFullYear(), 4) + '-' +
			f(this.getMonth() + 1, 2) + '-' +
			f(this.getDate(), 2) + 'T' +
			f(this.getHours(), 2) + ':' +
			f(this.getMinutes(), 2) + ':' +
			f(this.getSeconds(), 2) + ":" +
			f(this.getMilliseconds(), 3) +
			'Z' + "";
	};


	if (typeof Date.prototype.toJSON !== 'function') {
		Date.prototype.toJSON = Date.prototype.c1ToJSON;
		String.prototype.toJSON =
		Number.prototype.toJSON =
		Boolean.prototype.toJSON = function (key) {
			return this.valueOf();
		};
	}

	var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
		escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
		gap,
		indent,
		meta = {    // table of character substitutions
			'\b': '\\b',
			'\t': '\\t',
			'\n': '\\n',
			'\f': '\\f',
			'\r': '\\r',
			'"': '\\"',
			'\\': '\\\\'
		},
		rep;


	function quote(string) {

		// If the string contains no control characters, no quote characters, and no
		// backslash characters, then we can safely slap some quotes around it.
		// Otherwise we must also replace the offending characters with safe escape
		// sequences.

		escapable.lastIndex = 0;
		return escapable.test(string) ?
			'"' + string.replace(escapable, function (a) {
				var c = meta[a];
				return typeof c === 'string' ? c :
					'\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
			}) + '"' :
			'"' + string + '"';
	}


	function str(key, holder) {

		// Produce a string from holder[key].

		var i,          // The loop counter.
			k,          // The member key.
			v,          // The member value.
			length,
			mind = gap,
			partial,
			value = holder[key];

		// If the value has a toJSON method, call it to obtain a replacement value.

		if (value && typeof value === 'object' &&
			typeof value.c1ToJSON === 'function') {
			value = value.c1ToJSON(key);
		}
		else if (value && typeof value === 'object' &&
			typeof value.toJSON === 'function') {
			value = value.toJSON(key);
		}

		// If we were called with a replacer function, then call the replacer to
		// obtain a replacement value.

		if (typeof rep === 'function') {
			value = rep.call(holder, key, value);
		}

		// What happens next depends on the value's type.

		switch (typeof value) {
			case 'string':
				return quote(value);

			case 'number':

				// numbers must be finite. Encode non-finite numbers as null.

				return isFinite(value) ? String(value) : 'null';

			case 'boolean':
			case 'null':

				// If the value is a boolean or null, convert it to a string. Note:
				// typeof null does not produce 'null'. The case is included here in
				// the remote chance that this gets fixed someday.

				return String(value);

			// If the type is 'object', we might be dealing with an object or an array or
			// null.

			case 'object':

				// Due to a specification blunder in ECMAScript, typeof null is 'object',
				// so watch out for that case.

				if (!value) {
					return 'null';
				}

				// Make an array to hold the partial results of stringifying this object value.

				gap += indent;
				partial = [];

				// Is the value an array?

				if (Object.prototype.toString.apply(value) === '[object Array]') {

					// The value is an array. Stringify every element. Use null as a placeholder
					// for non-json values.

					length = value.length;
					for (i = 0; i < length; i += 1) {
						partial[i] = str(i, value) || 'null';
					}

					// Join all of the elements together, separated with commas, and wrap them in
					// brackets.

					v = partial.length === 0 ? '[]' :
					gap ? '[\n' + gap +
					partial.join(',\n' + gap) + '\n' +
					mind + ']' :
					'[' + partial.join(',') + ']';
					gap = mind;
					return v;
				}

				// If the replacer is an array, use it to select the members to be stringified.

				if (rep && typeof rep === 'object') {
					length = rep.length;
					for (i = 0; i < length; i += 1) {
						k = rep[i];
						if (typeof k === 'string') {
							v = str(k, value);
							if (v) {
								partial.push(quote(k) + (gap ? ': ' : ':') + v);
							}
						}
					}
				} else {

					// Otherwise, iterate through all of the keys in the object.

					for (k in value) {
						if (Object.hasOwnProperty.call(value, k)) {
							if (jQuery.type(value[k]) === "object") {
								if (!jQuery.isPlainObject(value[k])) {
									continue;
								}
							}
							v = str(k, value);
							if (v) {
								partial.push(quote(k) + (gap ? ': ' : ':') + v);
							}
						}
					}
				}

				// Join all of the member texts together, separated with commas,
				// and wrap them in braces.

				v = partial.length === 0 ? '{}' :
				gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' +
				mind + '}' : '{' + partial.join(',') + '}';
				gap = mind;
				return v;
		}
	}

	// If the object does not yet have a stringify method, give it one.

	if (typeof __JSONC1.stringify !== 'function') {
		__JSONC1.stringify = function (value, replacer, space) {

			/* Use browser's built-in JSON parser: */
			/*
			// Commented because of problems with replacer parameter in FireFox 3.5
			if(window.JSON && (typeof JSON.stringify == 'function')) {
				return JSON.stringify(value, function(key, value) {
					  if( this[key] != null && (typeof this[key].c1ToJSON == 'function') ) {
						value = this[key].c1ToJSON();
					  }
					  if (replacer && typeof replacer == 'function') {
						return replacer.call(this, key, value);
					  }
					  return value;
					}, space)
			}
			*/

			// The stringify method takes a value and an optional replacer, and an optional
			// space parameter, and returns a json text. The replacer can be a function
			// that can replace values, or an array of strings that will select the keys.
			// A default replacer method can be provided. Use of the space parameter can
			// produce text that is more easily readable.

			var i;
			gap = '';
			indent = '';

			// If the space parameter is a number, make an indent string containing that
			// many spaces.

			if (typeof space === 'number') {
				for (i = 0; i < space; i += 1) {
					indent += ' ';
				}

				// If the space parameter is a string, it will be used as the indent string.

			} else if (typeof space === 'string') {
				indent = space;
			}

			// If there is a replacer, it must be a function or an array.
			// Otherwise, throw an error.

			rep = replacer;
			if (replacer && typeof replacer !== 'function' &&
				(typeof replacer !== 'object' ||
				typeof replacer.length !== 'number')) {
				throw new Error('json stringify');
			}

			// Make a fake root object containing our value under the key of ''.
			// Return the result of stringifying the value.

			return str('', { '': value });
		};
	}

	if (typeof __JSONC1.parse !== 'function') {
		__JSONC1.parse = function (text, customReviver) {

			var reviver = function (key, value) {
				var a;
				if (typeof value === 'string') {
					a = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?):(\d{3})Z$/.exec(value);
					if (a) {
						var dt1 = new Date(+a[1], +a[2] - 1, +a[3], +a[4],
							+a[5], +a[6], +a[7]);
						dt1.setFullYear(+a[1]);
						return dt1;
					}
				}
				if (customReviver && typeof customReviver == 'function') {
					return customReviver.call(this, key, value);
				}
				return value;
			};

			/* Use browser's built-in parser: */
			if (window.JSON && (typeof JSON.parse == 'function')) {
				return JSON.parse(text, reviver);
			}

			// The parse method takes a text and an optional reviver function, and returns
			// a JavaScript value if the text is a valid json text.

			var j;

			function walk(holder, key) {

				// The walk method is used to recursively walk the resulting structure so
				// that modifications can be made.

				var k, v, value = holder[key];
				if (value && typeof value === 'object') {
					for (k in value) {
						if (Object.hasOwnProperty.call(value, k)) {
							v = walk(value, k);
							if (v !== undefined) {
								value[k] = v;
							} else {
								delete value[k];
							}
						}
					}
				}
				return reviver.call(holder, key, value);
			}


			// Parsing happens in four stages. In the first stage, we replace certain
			// Unicode characters with escape sequences. JavaScript handles many characters
			// incorrectly, either silently deleting them, or treating them as line endings.

			cx.lastIndex = 0;
			if (cx.test(text)) {
				text = text.replace(cx, function (a) {
					return '\\u' +
						('0000' + a.charCodeAt(0).toString(16)).slice(-4);
				});
			}

			// In the second stage, we run the text against regular expressions that look
			// for non-json patterns. We are especially concerned with '()' and 'new'
			// because they can cause invocation, and '=' because it can cause mutation.
			// But just to be safe, we want to reject all unexpected forms.

			// We split the second stage into 4 regexp operations in order to work around
			// crippling inefficiencies in IE's and Safari's regexp engines. First we
			// replace the json backslash pairs with '@' (a non-json character). Second, we
			// replace all simple value tokens with ']' characters. Third, we delete all
			// open brackets that follow a colon or comma or that begin the text. Finally,
			// we look to see that the remaining characters are only whitespace or ']' or
			// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

			if (/^[\],:{}\s]*$/.
				test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').
					replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
					replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

				// In the third stage we use the eval function to compile the text into a
				// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
				// in JavaScript: it can begin a block or an object literal. We wrap the text
				// in parens to eliminate the ambiguity.

				j = eval('(' + text + ')');

				// In the optional fourth stage, we recursively walk the new structure, passing
				// each name/value pair to a reviver function for possible transformation.

				return typeof reviver === 'function' ?
					walk({ '': j }, '') : j;
			}

			// If the text is not json parseable, then a SyntaxError is thrown.

			throw new SyntaxError('json parse');
		};
	}
})();

// <-- end of [MODIFIED json2]

// returns monthly week number (value from 1 to 5)
// startDayOfWeek , default is 0 - Sun, 1 - Mon, ... , 6 - Sat
if (!Date.prototype.getMonthlyWeekNumber) {
	Date.prototype.getMonthlyWeekNumber = function () {
		var selfDay = this.getDay(), weekNum = 0,
			startDt = new Date(this.getFullYear(), this.getMonth(), 1),
			curDt = new Date(this.getFullYear(), this.getMonth(), this.getDate());
		while (startDt <= curDt) {
			weekNum++;
			curDt = new Date(curDt.getFullYear(), curDt.getMonth(),
				curDt.getDate() - 7);
		}
		return weekNum;
		/*
		var prefixes = [1, 2, 3, 4, 5],
		weekNum = (prefixes[0 | this.getDate() / 7]);
		return weekNum;*/
	}
}


interface Date {
	getMonthlyWeekNumber;
	c1ToJSON;
}

interface String {
	toJSON;
}

interface Number {
	toJSON;
}

interface Boolean {
	toJSON;
}

interface Window {
	JSON;
}