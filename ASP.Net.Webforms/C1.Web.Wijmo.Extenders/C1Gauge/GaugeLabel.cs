﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if !EXTENDER
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.C1Chart;
namespace C1.Web.Wijmo.Controls.C1Gauge
#else
using C1.Web.Wijmo.Extenders.Localization;
	using C1.Web.Wijmo.Extenders.C1Chart;
namespace C1.Web.Wijmo.Extenders.C1Gauge
#endif
{
	/// <summary>
    /// Represents the GaugeLabel class which contains all settings for the gauge's label option.
	/// </summary>
	public class GaugeLabel: Settings, IJsonEmptiable
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GaugeLabel"/> class.
		/// </summary>
		public GaugeLabel()
		{
			this.LabelStyle = new ChartStyle();
		}

		#region ** Options
		/// <summary>
		/// Gets or sets the label's format.
		/// </summary>
		[WidgetOption]
		[C1Description("GaugeLabel.Format")]
		[C1Category("Category.Behavior")]
		[DefaultValue("")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string Format
		{
			get
			{
				return GetPropertyValue<string>("Format", "");
			}
			set
			{
				SetPropertyValue<string>("Format", value);
			}
		}

		/// <summary>
		/// Gets or sets the label's style.
		/// </summary>
		[C1Description("GaugeLabel.Style")]
		[C1Category("Category.Style")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[WidgetOptionName("style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle LabelStyle
		{
			get;
			set;
		}

		/// <summary>
		/// A value that indicates whether to show the label.
		/// </summary>
		[WidgetOption]
		[C1Description("GaugeLabel.Visible")]
		[C1Category("Category.Appearance")]
		[DefaultValue(true)]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public bool Visible
		{
			get
			{
				return GetPropertyValue<bool>("Visible", true);
			}
			set
			{
				SetPropertyValue<bool>("Visible", value);
			}
		}

		/// <summary>
		/// Gets or sets the offset of the label.
		/// </summary>
		[WidgetOption]
		[C1Description("GaugeLabel.Offset")]
		[C1Category("Category.Behavior")]
		[DefaultValue(0.0)]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double Offset
		{
			get
			{
				return GetPropertyValue<double>("Offset", 0);
			}
			set
			{
				SetPropertyValue<double>("Offset", value);
			}
		}
		#endregion

		bool IJsonEmptiable.IsEmpty
		{
			get 
			{
				return !ShouldSerialize();
			}
		}

		internal bool ShouldSerialize()
		{
			return !string.IsNullOrEmpty(Format)
				|| LabelStyle.ShouldSerialize()
				|| Visible != true
				|| Offset != 0;
		}
	}
}
