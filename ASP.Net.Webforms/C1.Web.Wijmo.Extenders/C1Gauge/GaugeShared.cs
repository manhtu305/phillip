﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Web.UI;

#if !EXTENDER
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.C1Chart;
namespace C1.Web.Wijmo.Controls.C1Gauge
#else
using C1.Web.Wijmo.Extenders.Localization;
using C1.Web.Wijmo.Extenders.C1Chart;
namespace C1.Web.Wijmo.Extenders.C1Gauge
#endif
{
    [WidgetDependencies(typeof(WijGauge))]
#if !EXTENDER
	public partial class C1Gauge
#else
	public partial class C1GaugeExtender
#endif	
	{

		#region ** fields
		private List<GaugelRange> ranges;
		#endregion

		#region ** Options
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public override string Theme
		{
			get
			{
				return string.Empty;
			}
		}
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public override string ThemeSwatch
		{
			get
			{
				return base.ThemeSwatch;
			}
			set
			{
				base.ThemeSwatch = value;
			}
		}

		/// <summary>
		/// A value that indicates whether to redraw the gauge automatically when resizing the gauge element.
		/// </summary>
		[C1Description("C1Gauge.AutoResize")]
		[WidgetOption]
		[DefaultValue(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool AutoResize
		{
			get
			{
				return GetPropertyValue<bool>("AutoResize", true);
			}
			set
			{
				SetPropertyValue<bool>("AutoResize", value);
			}
		}

		/// <summary>
		/// gets/sets the value of the gauge.
		/// </summary>
		[WidgetOption]
		[DefaultValue(0.0)]
		[C1Description("C1Gauge.Value")]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double Value
		{
			get 
			{
				return GetPropertyValue<double>("Value", 0);
			}
			set
			{
				SetPropertyValue<double>("Value", value);
			}
		}

		/// <summary>
		/// Gets/sets the max value of the gauge.
		/// </summary>
		[WidgetOption]
		[C1Description("C1Gauge.Max")]
		[C1Category("Category.Behavior")]
		[DefaultValue(100.0)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double Max
		{
			get
			{
				return GetPropertyValue<double>("Max", 100);
			}
			set
			{
				SetPropertyValue<double>("Max", value);
			}
		}

		/// <summary>
		/// Gets or set the min value of the gauge.
		/// </summary>
		[WidgetOption]
		[C1Description("C1Gauge.Min")]
		[DefaultValue(0.0)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double Min
		{
			get 
			{
				return GetPropertyValue<double>("Min", 0);
			}
			set
			{
				SetPropertyValue<double>("Min", value);
			}
		}

		/// <summary>
		/// A value that gets or sets the margin left of the gauge.
		/// </summary>
		[WidgetOption]
		[C1Description("C1Gauge.MarginLeft")]
		[DefaultValue(5)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int MarginLeft
		{
			get
			{
				return GetPropertyValue<int>("MarginLeft", 5);
			}
			set
			{
				SetPropertyValue<int>("MarginLeft", value);
			}
		}

		/// <summary>
		/// A value that gets or sets the margin right of the gauge.
		/// </summary>
		[WidgetOption]
		[C1Description("C1Gauge.MarginRight")]
		[DefaultValue(5)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int MarginRight
		{
			get
			{
				return GetPropertyValue<int>("MarginRight", 5);
			}
			set
			{
				SetPropertyValue<int>("MarginRight", value);
			}
		}

		/// <summary>
		/// A value that gets or sets the margin top of the gauge.
		/// </summary>
		[WidgetOption]
		[C1Description("C1Gauge.MarginTop")]
		[DefaultValue(5)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int MarginTop
		{
			get
			{
				return GetPropertyValue<int>("MarginTop", 5);
			}
			set
			{
				SetPropertyValue<int>("MarginTop", value);
			}
		}

		/// <summary>
		/// A value that gets or sets margin bottom of the gauge.
		/// </summary>
		[WidgetOption]
		[C1Description("C1Gauge.MarginBottom")]
		[DefaultValue(5)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int MarginBottom
		{
			get
			{
				return GetPropertyValue<int>("MarginBottom", 5);
			}
			set
			{
				SetPropertyValue<int>("MarginBottom", value);
			}
		}

		/// <summary>
		/// Gets or sets the tickMajor of the gauge.
		/// </summary>
		[WidgetOption]
		[C1Description("C1Gauge.TickMajor")]
		[C1Category("Category.Behavior")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public GaugeTick TickMajor
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the tickMinor of the gauge.
		/// </summary>
		[WidgetOption]
		[C1Description("C1Gauge.TickMinor")]
		[C1Category("Category.Behavior")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public GaugeTick TickMinor
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or set the pointer of the gauge.
		/// </summary>
		[WidgetOption]
		[C1Description("C1Gauge.Pointer")]
		[C1Category("Category.Behavior")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public GaugePointer Pointer
		{
			get;
			set;
		}

		/// <summary>
		/// A value that indicates whether the gauge value is logarithmic mode.
		/// </summary>
		[WidgetOption]
		[C1Description("C1Gauge.Islogarithmic")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Islogarithmic
		{
			get
			{
				return GetPropertyValue<bool>("Islogarithmic", false);
			}
			set
			{
				SetPropertyValue<bool>("Islogarithmic", value);
			}
		}

		/// <summary>
		/// Gets or sets the LogarithmicBase of the gauge.
		/// </summary>
		[DefaultValue(10)]
		[C1Description("C1Gauge.LogarithmicBase")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int LogarithmicBase
		{ 
			get
			{
				return GetPropertyValue<int>("LogarithmicBase", 10);
			}
			set
			{
				SetPropertyValue<int>("LogarithmicBase", value);
			}
		}

		/// <summary>
		/// A value that indicate how to show the labels.
		/// </summary>
		[WidgetOption]
		[C1Description("C1Gauge.Labels")]
		[C1Category("Category.Behavior")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public GaugeLabel Labels
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the gauge's animation.
		/// </summary>
		[WidgetOption]
		[C1Description("C1Gauge.Animation")]
		[C1Category("Category.Behavior")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public C1Chart.ChartAnimation Animation
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the gauge's face.
		/// </summary>
		[WidgetOption]
		[C1Description("C1Gauge.Face")]
		[C1Category("Category.Appearance")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public GaugeFace Face
		{
			get;
			set;
		}


		/// <summary>
		/// Gets or sets the ranges of the radialGauge.
		/// </summary>
		[WidgetOption]
		[C1Description("C1RadialGauge.Ranges")]
		[C1Category("Category.Behavior")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
		[CollectionItemType(typeof(GaugelRange))]
#endif
		public List<GaugelRange> Ranges
		{
			get
			{
				if (ranges == null)
				{
					ranges = new List<GaugelRange>();
				}
				return ranges;
			}
		}
		#endregion

		#region ** Methods
		/// <summary>
		/// Initializes the properties of the class.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected virtual void InitProperties()
		{
			this.TickMajor = new GaugeTick();
			this.TickMajor.Factor = 2;
			this.TickMajor.Interval = 10;
			this.TickMajor.TickStyle = new ChartStyle();

			this.TickMinor = new GaugeTick();
			this.TickMinor.Factor = 1;
			this.TickMinor.Interval = 5;
			this.TickMinor.Visible = false;
			this.TickMinor.TickStyle = new ChartStyle();

			this.Animation = new ChartAnimation();
			this.Animation.Duration = 2000;
			this.Animation.Easing = ChartEasing.EaseOutBack;
			this.Animation.Enabled = true;

			this.Labels = new GaugeLabel();
			this.Face = new GaugeFace();
			//this.InnerStates[""]
		}
		#endregion

		#region ** Seriliaze
		/// <summary>
		/// Determine whether the TickMajor property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns false if TickMajor has default values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual bool ShouldSerializeTickMajor()
		{
			return TickMajor.Factor != 2
				|| TickMajor.Interval != 10
				|| TickMajor.Position != Position.Center
				|| TickMajor.Offset != 0
				|| TickMajor.Marker != Marker.Rect
				|| TickMajor.ShouldSerializeStyle()
				|| TickMajor.Visible != true; 

		}

		/// <summary>
		/// Determine whether the TickMinor property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns false if TickMinor has default values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual bool ShouldSerializeTickMinor()
		{
			return TickMinor.Factor != 1
				|| TickMinor.Interval != 5
				|| TickMinor.Position != Position.Center
				|| TickMinor.Offset != 0
				|| TickMinor.Marker != Marker.Rect
				|| TickMinor.ShouldSerializeStyle()
				|| TickMinor.Visible != false;
		}

		/// <summary>
		/// Determine whether the Animation property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns false if Animation has default values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeAnimation()
		{
#if !EXTENDER
			if (this.IsDesignMode)
			{
				//fix for tfs issue 24774
				//animation should always be seriesed(set animation disabled) at design time.
				return true;
			}
#endif	
			return Animation.Enabled != true
				|| Animation.Duration != 2000
				|| Animation.Easing != ChartEasing.EaseOutBack;
		}

		/// <summary>
		/// Determine whether the Ranges property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if Ranges has values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeRanges()
		{
			return Ranges.Count > 0;
		}

		#endregion
	}
}
