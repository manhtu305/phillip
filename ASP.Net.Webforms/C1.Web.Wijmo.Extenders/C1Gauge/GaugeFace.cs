﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;

#if !EXTENDER
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.C1Chart;
namespace C1.Web.Wijmo.Controls.C1Gauge
#else
using C1.Web.Wijmo.Extenders.Localization;
using C1.Web.Wijmo.Extenders.C1Chart;
namespace C1.Web.Wijmo.Extenders.C1Gauge
#endif
{
	/// <summary>
    /// Represents the GaugeFace class which contains all settings for the gauge's face option.
	/// </summary>
	public class GaugeFace: Settings, IJsonEmptiable
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GaugeFace"/> class.
		/// </summary>
		public GaugeFace() 
		{
			this.FaceStyle = new ChartStyle();
		}

		/// <summary>
		/// Gets or sets the style of the face.
		/// </summary>
		[C1Description("GaugeFace.Style")]
		[C1Category("Category.Style")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[WidgetOptionName("style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle FaceStyle
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the face's template
		/// </summary>
		[WidgetEvent]
		[C1Description("GaugeFace.Template")]
		[C1Category("Category.Behavior")]
		[DefaultValue("")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string Template
		{
			get
			{
				return GetPropertyValue<string>("Template", string.Empty);
			}
			set
			{
				SetPropertyValue<string>("Template", value);
			}
		}

		internal bool ShouldSeralize()
		{
			return this.FaceStyle.ShouldSerialize()
				|| !string.IsNullOrEmpty(this.Template);
		}

		bool IJsonEmptiable.IsEmpty
		{
			get 
			{
				return !this.ShouldSeralize();
			}
		}
	}
}
