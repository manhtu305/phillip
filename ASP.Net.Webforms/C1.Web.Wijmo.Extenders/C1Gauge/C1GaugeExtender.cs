﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

namespace C1.Web.Wijmo.Extenders.C1Gauge
{
	[TargetControlType(typeof(Control))]
	[ToolboxItem(false)]
	public partial class C1GaugeExtender: WidgetExtenderControlBase
	{
		public C1GaugeExtender():base()
		{
			this.InitProperties();
		}

		#region ** Options
		/// <summary>
		/// Gets or sets the width of the gauge.
		/// </summary>
		[WidgetOption]
		[DefaultValue(800)]
		[Category("Options")]
		public virtual int Width
		{
			get
			{
				return GetPropertyValue<int>("Width", 800);
			}
			set
			{
				SetPropertyValue<int>("Width", value);
			}
		}

		/// <summary>
		/// Gets or sets the height of the range.
		/// </summary>
		[WidgetOption]
		[DefaultValue(600)]
		[Category("Options")]
		public virtual int Height
		{
			get
			{
				return GetPropertyValue<int>("Height", 600);
			}
			set
			{
				SetPropertyValue<int>("Height", value);
			}
		}
		#endregion
	}
}
