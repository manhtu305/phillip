﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if !EXTENDER
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.C1Chart;
namespace C1.Web.Wijmo.Controls.C1Gauge
#else
using C1.Web.Wijmo.Extenders.Localization;
using C1.Web.Wijmo.Extenders.C1Chart;
namespace C1.Web.Wijmo.Extenders.C1Gauge
#endif
{
	/// <summary>
	/// Represents the GaugeTick class which contains all settings for the gauge's tickMinor/tickMajor option.
	/// </summary>
	public class GaugeTick: Settings
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GaugeTick"/> class.
		/// </summary>
		public GaugeTick()
		{
			this.TickStyle = new C1Chart.ChartStyle();
		}

		#region ** Options
		/// <summary>
		/// Gets or sets the position of the gauge.
		/// </summary>
		[WidgetOption]
		[C1Description("GaugeTick.Position")]
		[C1Category("Category.Appearance")]
		[DefaultValue(Position.Center)]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public Position Position
		{
			get
			{
				return GetPropertyValue<Position>("Position", Position.Center);
			}
			set
			{
				SetPropertyValue<Position>("Position", value);
			}
		}

		/// <summary>
		/// Gets or sets the tick's style.
		/// </summary>
		[C1Description("GaugeTick.Style")]
		[C1Category("Category.Style")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[WidgetOptionName("style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public C1Chart.ChartStyle TickStyle
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the tick's factor 
		/// </summary>
		[WidgetOption]
		[C1Description("GaugeTick.Factor")]
		[C1Category("Category.Behavior")]
		[DefaultValue(1.0)]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double Factor
		{
			get
			{
				return GetPropertyValue<double>("Factor", 1);
			}
			set
			{
				SetPropertyValue<double>("Factor", value);
			}
		}

		/// <summary>
		/// A value that indicate whether to show the tick.
		/// </summary>
		[WidgetOption]
		[C1Description("GaugeTick.Visible")]
		[C1Category("Category.Behavior")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Visible
		{
			get
			{
				return GetPropertyValue("Visible", true);
			}
			set
			{
				SetPropertyValue<bool>("Visible", value);
			}
		}

		/// <summary>
		/// Gets or sets the tick's marker shape.
		/// </summary>
		[WidgetOption]
		[C1Description("GaugeTick.Marker")]
		[C1Category("Category.Behavior")]
		[DefaultValue(Marker.Rect)]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Marker Marker
		{
			get
			{
				return GetPropertyValue<Marker>("Marker", Marker.Rect);
			}
			set
			{
				SetPropertyValue<Marker>("Marker", value);
			}
		}

		/// <summary>
		/// Gets or sets the offset of the tick.
		/// </summary>
		[WidgetOption]
		[C1Description("GaugeTick.Offset")]
		[C1Category("Category.Behavior")]
		[Json(true)]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double Offset
		{
			get
			{
				return GetPropertyValue<double>("Offset", 0);
			}
			set
			{
				SetPropertyValue<double>("Offset", value);
			}
		}

		/// <summary>
		/// Gets or sets the interval of the tick.
		/// </summary>
		[WidgetOption]
		[C1Description("GaugeTick.Interval")]
		[C1Category("Category.Behavior")]
		[DefaultValue(10)]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double Interval
		{
			get
			{
				return GetPropertyValue<double>("Interval", 10);
			}
			set
			{
				SetPropertyValue<double>("Interval", value);
			}
		}
		#endregion

		#region ** Serialize
		/// <summary>
		/// Determine whether the Style property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns false if Style has default values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeStyle()
		{
			return TickStyle.ShouldSerialize();
		}
		#endregion
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the GaugeTick.Position property.
	/// </summary>
	public enum Position
	{
		/// <summary>
		/// At inside
		/// </summary>
		Inside = 1,
		/// <summary>
		/// At center
		/// </summary>
		Center = 2,
		/// <summary>
		/// At outside
		/// </summary>
		Outside = 3
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the GaugeTick.Marker property.
	/// </summary>
	public enum Marker
	{
		/// <summary>
		/// rectangle
		/// </summary>
		Rect = 1,
		/// <summary>
		/// circle
		/// </summary>
		Circle = 2,
		/// <summary>
		/// triangle
		/// </summary>
		Tri = 3,
		/// <summary>
		/// inverted triangle
		/// </summary>
		InvertedTri = 4,
		/// <summary>
		/// square
		/// </summary>
		Box = 5,
		/// <summary>
		/// diamond
		/// </summary>
		Diamond = 6,
		/// <summary>
		/// crossed horizontal and vertical lines
		/// </summary>
		Cross = 7
	}
}
