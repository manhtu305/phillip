﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if !EXTENDER
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.C1Chart;
namespace C1.Web.Wijmo.Controls.C1Gauge
#else
using C1.Web.Wijmo.Extenders.Localization;
using C1.Web.Wijmo.Extenders.C1Chart;
namespace C1.Web.Wijmo.Extenders.C1Gauge
#endif
{
	/// <summary>
    /// Represents the C1RadialGaugePointerCap class which contains all settings for the gauge's range option.
	/// </summary>
	public class GaugelRange: Settings
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GaugelRange"/> class.
		/// </summary>
		public GaugelRange()
		{
			this.RangeStyle = new ChartStyle();
		}

		/// <summary>
		/// Gets or sets the start width of the range.
		/// </summary>
		[WidgetOption]
		[C1Description("RadialRange.StartWidth")]
		[C1Category("Category.Behavior")]
		[DefaultValue(0)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double StartWidth
		{
			get
			{
				return GetPropertyValue<double>("StartWidth", 0);
			}
			set
			{
				SetPropertyValue<double>("StartWidth", value);
			}
		}

		/// <summary>
		/// Gets or sets the end width of the range.
		/// </summary>
		[WidgetOption]
		[C1Description("RadialRange.EndWidth")]
		[C1Category("Category.Behavior")]
		[DefaultValue(0)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double EndWidth
		{
			get
			{
				return GetPropertyValue<double>("EndWidth", 0);
			}
			set
			{
				SetPropertyValue<double>("EndWidth", value);
			}
		}

		/// <summary>
		/// Gets or sets the width of the range.
		/// </summary>
		[WidgetOption]
		[C1Description("RadialRange.Width")]
		[C1Category("Category.Behavior")]
		[DefaultValue(0.0)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double Width
		{
			get
			{
				return GetPropertyValue<double>("Width", 0.1);
			}
			set
			{
				SetPropertyValue<double>("Width", value);
			}
		}

		/// <summary>
		/// Gets or sets the start value of the range.
		/// </summary>
		[WidgetOption]
		[C1Description("RadialRange.StartValue")]
		[C1Category("Category.Behavior")]
		[DefaultValue(0)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double StartValue
		{
			get
			{
				return GetPropertyValue<double>("StartValue", 0);
			}
			set
			{
				SetPropertyValue<double>("StartValue", value);
			}
		}

		/// <summary>
		/// Gets or sets the end value of the range.
		/// </summary>
		[WidgetOption]
		[C1Description("RadialRange.EndValue")]
		[C1Category("Category.Behavior")]
		[DefaultValue(0)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double EndValue
		{
			get
			{
				return GetPropertyValue<double>("EndValue", 0);
			}
			set
			{
				SetPropertyValue<double>("EndValue", value);
			}
		}

		/// <summary>
		/// Gets or sets the start location of the range.
		/// </summary>
		[WidgetOption]
		[C1Description("RadialRange.StartLocation")]
		[C1Category("Category.Behavior")]
		[DefaultValue(1.0)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double StartDistance
		{
			get
			{
				return GetPropertyValue<double>("StartDistance", 1.0);
			}
			set
			{
				SetPropertyValue<double>("StartDistance", value);
			}
		}

		/// <summary>
		/// Gets or sets the end location of the range.
		/// </summary>
		[WidgetOption]
		[C1Description("RadialRange.EndLocation")]
		[C1Category("Category.Behavior")]
		[DefaultValue(1.0)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double EndDistance
		{
			get
			{
				return GetPropertyValue<double>("EndDistance", 1.0);
			}
			set
			{
				SetPropertyValue<double>("EndDistance", value);
			}
		}

		/// <summary>
		/// Gets or sets the style of the range.
		/// </summary>
		[C1Description("RadialRange.Style")]
		[C1Category("Category.Style")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[WidgetOptionName("style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle RangeStyle
		{
			get;
			set;
		}

		/// <summary>
		/// Determine whether the Style property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns false if Style has default values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeStyle()
		{
			return RangeStyle.ShouldSerialize();
		}

	}
}
