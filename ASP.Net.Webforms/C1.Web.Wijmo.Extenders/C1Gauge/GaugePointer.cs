﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if !EXTENDER
using C1.Web.Wijmo.Controls.C1Chart;
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Gauge
#else
using C1.Web.Wijmo.Extenders.C1Chart;
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Gauge
#endif
{
	/// <summary>
    /// Represents the GaugePointer class which contains all settings for the gauge's pointer option.
	/// </summary>
	public class GaugePointer:Settings, IJsonEmptiable
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="GaugePointer"/> class.
		/// </summary>
		public GaugePointer()
		{
			this.PointerStyle = new ChartStyle();
		}

		#region ** Options
		/// <summary>
		/// Gets or sets the length of the pointer
		/// </summary>
		[DefaultValue(1)]
		[C1Description("GaugePointer.Length")]
		[C1Category("Category.Misc")]
		[WidgetOption]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Sizes)]
#endif
		public virtual double Length
		{
			get
			{
				return GetPropertyValue<double>("Length", 1);
			}
			set
			{
				SetPropertyValue<double>("Length", value);
			}
		}

		/// <summary>
		/// Gets or sets the shape of the pointer
		/// </summary>
		[WidgetOption]
		[NotifyParentProperty(true)]
		[DefaultValue(Shape.Tri)] 
		[C1Description("GaugePointer.Shape")]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public Shape Shape
		{
			get
			{
				return GetPropertyValue<Shape>("Shape", Shape.Tri);
			}
			set
			{
				SetPropertyValue<Shape>("Shape", value);
			}
		}

		/// <summary>
		/// Gets or sets the style of the pointer.
		/// </summary>
		[C1Description("GaugePointer.Style")]
		[C1Category("Category.Style")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[WidgetOptionName("style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle PointerStyle
		{
			get;
			set;
		}

		/// <summary>
		/// A value that indicates the pointer's width.
		/// </summary>
		[WidgetOption]
		[C1Description("GaugePointer.Width")]
		[C1Category("Category.Misc")]
		[DefaultValue(4)]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Sizes)]
#endif
		public double Width
		{
			get
			{
				return GetPropertyValue<double>("Width", 4);
			}
			set
			{
				SetPropertyValue<double>("Width", value);
			}
		}

		/// <summary>
		/// Gets or sets the offset of the pointer.
		/// </summary>
		[WidgetOption]
		[C1Description("GaugePointer.Offset")]
		[C1Category("Category.Behavior")]
		[DefaultValue(0)]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double Offset
		{
			get
			{
				return GetPropertyValue<double>("Offset", 0);
			}
			set
			{
				SetPropertyValue<double>("Offset", value);
			}
		}

		/// <summary>
		/// A value indicates whether to show the pointer.
		/// </summary>
		[WidgetOption]
		[C1Description("GaugePointer.Visible")]
		[C1Category("Category.Appearance")]
		[DefaultValue(true)]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public bool Visible
		{
			get
			{
				return GetPropertyValue<bool>("Visible", true);
			}
			set
			{
				SetPropertyValue<bool>("Visible", value);
			}
		}

		/// <summary>
		/// Gets or sets the template of the pointer.
		/// </summary>
		[WidgetEvent]
		[C1Description("GaugePointer.Template")]
		[C1Category("Category.Behavior")]
		[DefaultValue("")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string Template
		{
			get
			{
				return GetPropertyValue<string>("Template", "");
			}
			set
			{
				SetPropertyValue<string>("Template", value);
			}
		}
		#endregion
		
		bool IJsonEmptiable.IsEmpty
		{
			get 
			{
				return !ShouldSerialize();
			}
		}

		internal bool ShouldSerialize()
		{
			return Length != 1
						|| PointerStyle.ShouldSerialize()
						|| Width != 4
						|| Offset != 0
						|| Visible == false
						|| !string.IsNullOrEmpty(Template);
		}
	}

	/// <summary>
	/// Define the shape for pointer.
	/// </summary>
	public enum Shape
	{ 
		/// <summary>
		/// define rectangle
		/// </summary>
		Rect = 0,
		/// <summary>
		/// define triangle
		/// </summary>
		Tri = 1,
		/// <summary>
		/// Other shapes.
		/// </summary>
		Other = 2
	}
}
