﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1SuperPanel
#else
namespace C1.Web.Wijmo.Controls.C1SuperPanel
#endif
{

	/// <summary>
	/// Base class for scroller.
	/// </summary>
	public class BaseScroller : Settings
	{

		#region ** fields
		private PositionSettings _increaseButtonPosition = null;
		private PositionSettings _decreaseButtonPosition = null;
		#endregion end of ** fields.

		#region ** constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="BaseScroller"/> class.
		/// </summary>
		public BaseScroller()
		{
		}
		#endregion end of ** constructors.

		#region ** properties
		/// <summary>
		/// This value sets the small change value of scroller. 
		/// </summary>
		[DefaultValue((double)1)]
		[C1Description("C1SuperPanel.C1ScrollerBase.SmallChange")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public double ScrollSmallChange
		{
			get
			{
				double smallChange = this.GetPropertyValue<double>("ScrollSmallChange", 1);
				return Math.Min(smallChange, this.ScrollLargeChange);
			}
			set
			{
				if (this.ScrollSmallChange != value)
				{
					CheckLessThenZero(value);
					this.SetPropertyValue<double>("ScrollSmallChange", value);
				}
			}
		}

		/// <summary>
		/// This value sets the large change value of scroller.
		/// </summary>
		[DefaultValue((double)10)]
		[C1Description("C1SuperPanel.C1ScrollerBase.LargeChange")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public double ScrollLargeChange
		{
			get
			{
				double largeChange = this.GetPropertyValue<double>("ScrollLargeChange", 10);
				return Math.Min(largeChange, (this.ScrollMax - this.ScrollMin) + 1);
			}
			set
			{
				if (this.ScrollLargeChange != value)
				{
					CheckLessThenZero(value);
					this.SetPropertyValue<double>("ScrollLargeChange", value);
				}
			}
		}

		/// <summary>
		/// This value determines the scrolling position of <seealso cref="C1SuperPanelExtender"/>.
		/// </summary>
		[DefaultValue((double)0)]
		[C1Description("C1SuperPanel.C1ScrollerBase.Value")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public double ScrollValue
		{
			get
			{
				double value = this.GetPropertyValue<double>("ScrollValue", 0);

				if (value > this.ScrollMax)
				{
					return this.ScrollMax;
				}

				if (value < this.ScrollMin)
				{
					return this.ScrollMin;
				}

				return value;
			}
			set
			{
				CheckLessThenZero(value);

				if (value > this.ScrollMax || value < this.ScrollMin)
				{
					throw new ArgumentOutOfRangeException("Value", " 'Value' should be between 'minimum' and 'maximum'.");
				}

				this.SetPropertyValue<double>("ScrollValue", value);
			}
		}

		/// <summary>
		/// This value sets the maximum value of scroller.
		/// </summary>
		[DefaultValue((double)200)]
		[C1Description("C1SuperPanel.C1ScrollerBase.Maximum")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public double ScrollMax
		{
			get
			{
				return this.GetPropertyValue<double>("ScrollMax", 200);
			}
			set
			{
				if (this.ScrollMax != value)
				{
					if (this.ScrollMin > value)
					{
						this.SetPropertyValue<double>("ScrollMin", value);
					}

					if (value < this.ScrollValue)
					{
						this.SetPropertyValue<double>("ScrollValue", value);
					}

					this.SetPropertyValue<double>("ScrollMax", value);
				}
			}
		}

		/// <summary>
		/// This value sets the minimum value of scroller.
		/// </summary>
		[DefaultValue((double)0)]
		[C1Description("C1SuperPanel.C1ScrollerBase.Minimum")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public double ScrollMin
		{
			get
			{
				return this.GetPropertyValue<double>("ScrollMin", 0);
			}
			set
			{
				if (this.ScrollMin != value)
				{
					if (this.ScrollMax < value)
					{
						this.SetPropertyValue<double>("ScrollMax", value);
					}

					if (value > this.ScrollValue)
					{
						this.SetPropertyValue<double>("ScrollValue", value);
					}

					this.SetPropertyValue<double>("ScrollMin", value);
				}
			}
		}

		/// <summary>
		/// This value determines the scroll mode of scrolling.
		/// </summary>
		[DefaultValue(ScrollMode.ScrollBar)]
		[C1Description("C1SuperPanel.C1ScrollerBase.Mode")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public ScrollMode ScrollMode
		{
			get
			{
				return this.GetPropertyValue<ScrollMode>("ScrollMode", ScrollMode.ScrollBar);
			}
			set
			{
				this.SetPropertyValue<ScrollMode>("ScrollMode", value);
			}
		}

		/// <summary>
		/// This value determines the visibility of the scroll bar.
		/// </summary>
		[DefaultValue(ScrollBarVisibility.Auto)]
		[C1Description("C1SuperPanel.C1ScrollerBase.Visibility")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public ScrollBarVisibility ScrollBarVisibility
		{
			get
			{
				return this.GetPropertyValue<ScrollBarVisibility>("ScrollBarVisibility", ScrollBarVisibility.Auto);
			}
			set
			{
				this.SetPropertyValue<ScrollBarVisibility>("ScrollBarVisibility", value);
			}
		}

		/// <summary>
		/// This object determines the increase button position.
		/// </summary>
		[C1Description("C1SuperPanel.C1ScrollerBase.IncreaseButtonPosition")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		public PositionSettings IncreaseButtonPosition
		{
			get
			{
				if (this._increaseButtonPosition == null)
				{
					this._increaseButtonPosition = new PositionSettings();
				}

				return this._increaseButtonPosition;
			}
			set
			{
				this._increaseButtonPosition = value;
			}
		}

		/// <summary>
		/// This object determines the decrease button position.
		/// </summary>
		[C1Description("C1SuperPanel.C1ScrollerBase.DecreaseButtonPosition")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		public PositionSettings DecreaseButtonPosition
		{
			get
			{
				if (this._decreaseButtonPosition == null)
				{
					this._decreaseButtonPosition = new PositionSettings();
				}

				return this._decreaseButtonPosition;
			}
			set
			{
				this._decreaseButtonPosition = value;
			}
		}

		/// <summary>
		/// This value sets the minimum length, in pixel, of the scroll bar thumb button.
		/// </summary>
		[DefaultValue(6)]
		[C1Description("C1SuperPanel.C1ScrollerBase.MinDragLength")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public int ScrollMinDragLength
		{
			get
			{
				return GetPropertyValue<int>("ScrollMinDragLength", 6);
			}
			set
			{
				SetPropertyValue<int>("ScrollMinDragLength", value);
			}
		}

		/// <summary>
		/// This value sets the width of hovering edge 
		/// which will trigger the scrolling.
		/// </summary>
		[DefaultValue(20)]
		[C1Description("C1SuperPanel.C1ScrollerBase.HoverEdgeSpan")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public int HoverEdgeSpan
		{
			get
			{
				return GetPropertyValue<int>("HoverEdgeSpan", 20);
			}
			set
			{
				SetPropertyValue<int>("HoverEdgeSpan", value);
			}
		}

		/// <summary>
		/// The value to add to small change or large change when scrolling
		/// the first step(scrolling from value 0).
		/// </summary>
		[DefaultValue(0)]
		[C1Description("C1SuperPanel.C1ScrollerBase.FirstStepChangeFix")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public int FirstStepChangeFix
		{
			get
			{
				return GetPropertyValue<int>("FirstStepChangeFix", 0);
			}
			set
			{
				SetPropertyValue<int>("FirstStepChangeFix", value);
			}
		}
		#endregion end of ** properties.

		#region ** methods
		private static void CheckLessThenZero(double value)
		{
			if (value < 0)
			{
				throw new ArgumentOutOfRangeException("Value", " 'Value' should not be less than 0.");
			}
		}
		#endregion end of ** methods.
	}
}