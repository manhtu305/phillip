﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Web;
using System.ComponentModel;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Extenders.C1SuperPanel
{

	/// <summary>
	/// Represents a WijSuperPanel control.
	/// </summary>
	[TargetControlType(typeof(Panel))]
	[ToolboxItem(true)]
	[ToolboxBitmap(typeof(C1SuperPanelExtender), "SuperPanel.png")]
	[LicenseProviderAttribute()]
	public partial class C1SuperPanelExtender : WidgetExtenderControlBase
	{

		private bool _productLicensed = false;
		public C1SuperPanelExtender()
		{
			VerifyLicense();
			this.AnimationOptions = new SlideAnimation();
			this.ResizableOptions = new ResizeSettings();
			this.ResizableOptions.Handles = Compass.All;
			this.ResizableOptions.Helper = CONST_RESIZE_HELPER;
			this.HScroller = new HScroller();
			this.VScroller = new VScroller();
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1SuperPanelExtender), this, false);
#if GRAPECITY
            _productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}
	}
}
