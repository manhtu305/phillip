﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1SuperPanel
#else
namespace C1.Web.Wijmo.Controls.C1SuperPanel
#endif
{

	#region ** The HScrollBarPosition enumeration
	/// <summary>
	/// Contains position option for horizontal scrollbar.
	/// </summary>
	public enum HScrollBarPosition
	{
		/// <summary>
		/// Places horizontal scrollbar at left side.
		/// </summary>
		Top = 0,
		/// <summary>
		/// Places horizontal scrollbar at right side.
		/// </summary>
		Bottom = 1
	} 
	#endregion end of ** The HScrollBarPosition enumeration.

	#region ** The VScrollBarPosition enumeration
	/// <summary>
	/// Contains position option for vertical scrollbar.
	/// </summary>
	public enum VScrollBarPosition
	{
		/// <summary>
		/// Places vertical scrollbar at left side.
		/// </summary>
		Left = 0,
		/// <summary>
		/// Places vertical scrollbar at right side.
		/// </summary>
		Right = 1
	} 
	#endregion end of ** The VScrollBarPosition enumeration.

	#region ** The ScrollBarVisibility enumeration
	/// <summary>
	/// Contains three visibility options for scroll bar or scroll button. 
	/// </summary>
	public enum ScrollBarVisibility
	{
		/// <summary>
		/// Shows the scroll when needed.
		/// </summary>
		Auto = 0,
		/// <summary>
		/// Scroll bar will always be visible.  It's disabled when not needed.
		/// </summary>
		Visible = 1,
		/// <summary>
		/// Scroll bar will be hidden.
		/// </summary>
		Hidden = 2
	} 
	#endregion end of ** The ScrollBarVisibility enumeration.

	#region ** The ScrollMode enumeration
	/// <summary>
	/// Contains scroll mode that could be used in <see cref="C1SuperPanelExtender"/>.
	/// </summary>
	[Flags]
	[Editor("C1.Web.Wijmo.Design.UITypeEditors.C1FlagEnumEditor", typeof(System.Drawing.Design.UITypeEditor))]
	public enum ScrollMode
	{
		/// <summary>
		/// Scroll bars are used for scrolling.
		/// </summary>
		ScrollBar = 1,
		/// <summary>
		/// Scroll buttons are used for scrolling.
		/// </summary>
		Buttons = 2,
		/// <summary>
		/// Scroll buttons are used for scrolling.
		/// </summary>
		ButtonsHover = 4,
		/// <summary>
		/// Scrolling occurs when the mouse is moving to the edge
		/// of the content area.
		/// </summary>
		Edge = 8
	} 
	#endregion end of ** The ScrollMode enumeration.
}
