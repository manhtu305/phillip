﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1SuperPanel
#else
namespace C1.Web.Wijmo.Controls.C1SuperPanel
#endif
{

	#region ** HScroller class
	/// <summary>
	/// Represents the HScroller class which contains all settings for the horizontal scrollbar.
	/// </summary>
	public class HScroller : BaseScroller
	{

		/// <summary>
		/// Initialize a new instance of <see cref="HScroller"/>.
		/// </summary>
		public HScroller()
		{
			this.ScrollBarPosition = HScrollBarPosition.Bottom;
		}

		/// <summary>
		/// This value determines the position of the horizontal scroll bar.
		/// </summary>
		[DefaultValue(HScrollBarPosition.Bottom)]
		[C1Description("C1SuperPanel.HScroller.ScrollbarLocation")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public HScrollBarPosition ScrollBarPosition
		{
            get
            {
                return GetPropertyValue<HScrollBarPosition>("ScrollBarPosition", HScrollBarPosition.Bottom);
            }
            set
            {
                SetPropertyValue<HScrollBarPosition>("ScrollBarPosition", value);
            }
		}
	} 
	#endregion end of ** HScroller class.

	#region ** VScroller class
	/// <summary>
	/// Represents the VScroller class which contains all settings for the vertical scrollbar.
	/// </summary>
	public class VScroller : BaseScroller
	{

		/// <summary>
		/// Initialize a new instance of <see cref="VScroller"/>.
		/// </summary>
		public VScroller()
		{
			this.ScrollBarPosition = VScrollBarPosition.Right;
		}

		/// <summary>
		/// This value determines the position of the vertical scroll bar.
		/// </summary>
		[DefaultValue(VScrollBarPosition.Right)]
		[C1Description("C1SuperPanel.VScroller.ScrollbarLocation")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public VScrollBarPosition ScrollBarPosition
		{
            get
            {
                return GetPropertyValue<VScrollBarPosition>("ScrollBarPosition", VScrollBarPosition.Right);
            }
            set
            {
                SetPropertyValue<VScrollBarPosition>("ScrollBarPosition", value);
            }
		}
	} 
	#endregion end of ** VScroller class.
}
