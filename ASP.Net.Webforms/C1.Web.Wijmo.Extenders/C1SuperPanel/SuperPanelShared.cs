﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using C1.Web.Wijmo;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1SuperPanel", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1SuperPanel
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1SuperPanel", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1SuperPanel
#endif
{
    [WidgetDependencies(
        typeof(WijSuperPanel)
#if !EXTENDER
, "extensions.c1superpanel.js",
ResourcesConst.C1WRAPPER_OPEN
#endif
)]
#if EXTENDER
	public partial class C1SuperPanelExtender
#else
    public partial class C1SuperPanel
#endif
    {

        #region ** fields
        private SlideAnimation _animationOptions = null;
        private HScroller _hScroller = null;
        private VScroller _vScroller = null;
        private ResizeSettings _resizeSettings = null;
        private const string CONST_RESIZE_HELPER = "ui-widget-content wijmo-wijsuperpanel-helper";
        #endregion end of ** fields.

        #region ** properties
        /// <summary>
        /// This value determines whether the wijsuperpanel can be resized.
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [C1Description("C1SuperPanel.AllowResize")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public bool AllowResize
        {
            get
            {
                return this.GetPropertyValue("AllowResize", false);
            }
            set
            {
                this.SetPropertyValue("AllowResize", value);
            }
        }

        /// <summary>
        /// This value determines whether wijsuperpanel to automatically refresh 
        /// when content size or wijsuperpanel size are changed.
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [C1Description("C1SuperPanel.AutoRefresh")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public bool AutoRefresh
        {
            get
            {
                return this.GetPropertyValue("AutoRefresh", false);
            }
            set
            {
                this.SetPropertyValue("AutoRefresh", value);
            }
        }

        /// <summary>
        /// The animation properties of wijsuperpanel scrolling.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1SuperPanel.AnimationOptions")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public SlideAnimation AnimationOptions
        {
            get
            {
                if (this._animationOptions == null)
                {
                    this._animationOptions = new SlideAnimation();
                }

                return this._animationOptions;
            }
            set
            {
                this._animationOptions = value;
            }
        }

        /// <summary>
        /// This option contains horizontal scroller settings.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1SuperPanel.HScroller")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public HScroller HScroller
        {
            get
            {
                if (this._hScroller == null)
                {
                    this._hScroller = new HScroller();
                }

                return this._hScroller;
            }
            set
            {
                this._hScroller = value;
            }
        }

        /// <summary>
        /// This option contains vertical scroller settings.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1SuperPanel.VScroller")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public VScroller VScroller
        {
            get
            {
                if (this._vScroller == null)
                {
                    this._vScroller = new VScroller();
                }

                return this._vScroller;
            }
            set
            {
                this._vScroller = value;
            }
        }

        /// <summary>
        /// A value that determines whether wijsuperpanel provides keyboard scrolling support.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1SuperPanel.KeyboardSupport")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool KeyboardSupport
        {
            get
            {
                return this.GetPropertyValue("KeyboardSupport", false);
            }
            set
            {
                this.SetPropertyValue("KeyboardSupport", value);
            }
        }

        /// <summary>
        /// This value determines the time interval to call the scrolling
        /// function when doing continuous scrolling.
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(100)]
        [C1Description("C1SuperPanel.KeyDownInterval")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int KeyDownInterval
        {
            get
            {
                return this.GetPropertyValue("KeyDownInterval", 100);
            }
            set
            {
                this.SetPropertyValue("KeyDownInterval", value);
            }
        }

        /// <summary>
        /// This value determines whether wijsuperpanel has mouse wheel support.
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [C1Description("C1SuperPanel.MouseWheelSupport")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool MouseWheelSupport
        {
            get
            {
                return this.GetPropertyValue("MouseWheelSupport", true);
            }
            set
            {
                this.SetPropertyValue("MouseWheelSupport", value);
            }
        }

        /// <summary>
        /// This value determines whether to fire the mouse wheel event
        /// when <seealso cref="C1SuperPanelExtender"/> is scrolled to the end.
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [C1Description("C1SuperPanel.BubbleScrollingEvent")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Misc)]
#endif
        public bool BubbleScrollingEvent
        {
            get
            {
                return this.GetPropertyValue("BubbleScrollingEvent", true);
            }
            set
            {
                this.SetPropertyValue("BubbleScrollingEvent", value);
            }
        }

        /// <summary>
        /// This option determines the behavior of resizable widget.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1SuperPanel.ResizableOptions")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public ResizeSettings ResizableOptions
        {
            get
            {
                if (this._resizeSettings == null)
                {
                    this._resizeSettings = new ResizeSettings();
                }

                return this._resizeSettings;
            }
            set
            {
                this._resizeSettings = value;
            }
        }

        /// <summary>
        /// This value determines whether to show the rounded corner of wijsuperpanel.
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [C1Description("C1SuperPanel.ShowRounder")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public bool ShowRounder
        {
            get
            {
                return this.GetPropertyValue("ShowRounder", true);
            }
            set
            {
                this.SetPropertyValue("ShowRounder", value);
            }
        }
        #endregion end of ** properties.

        #region ** client events
        /// <summary>
        /// The name of the function which will be called 
        /// when thumb buttons of scrollbars dragging stops.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [WidgetOptionName("dragStop")]
        [C1Description("C1SuperPanel.OnClientDragStop")]
        [WidgetEvent]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientDragStop
        {
            get
            {
                return this.GetPropertyValue("OnClientDragStop", "");
            }
            set
            {
                this.SetPropertyValue("OnClientDragStop", value);
            }
        }

        /// <summary>
        /// The name of the function which will be called after panel is painted.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [WidgetOptionName("painted")]
        [C1Description("C1SuperPanel.OnClientPainted")]
        [WidgetEvent]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientPainted
        {
            get
            {
                return this.GetPropertyValue("OnClientPainted", "");
            }
            set
            {
                this.SetPropertyValue("OnClientPainted", value);
            }
        }

        /// <summary>
        /// The name of the function which will be called when resized event is fired.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [WidgetOptionName("resized")]
        [C1Description("C1SuperPanel.OnClientResized")]
        [WidgetEvent]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientResized
        {
            get
            {
                return this.GetPropertyValue("OnClientResized", "");
            }
            set
            {
                this.SetPropertyValue("OnClientResized", value);
            }
        }

        /// <summary>
        /// The name of the function which will be called before scrolling occurs.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [WidgetOptionName("scrolling")]
        [C1Description("C1SuperPanel.OnClientScrolling")]
        [WidgetEvent("e, data")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientScrolling
        {
            get
            {
                return this.GetPropertyValue("OnClientScrolling", "");
            }
            set
            {
                this.SetPropertyValue("OnClientScrolling", value);
            }
        }

        /// <summary>
        /// The name of the function which will be called after scrolling occurs.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [WidgetOptionName("scrolled")]
        [C1Description("C1SuperPanel.OnClientScrolled")]
        [WidgetEvent("e, data")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientScrolled
        {
            get
            {
                return this.GetPropertyValue("OnClientScrolled", "");
            }
            set
            {
                this.SetPropertyValue("OnClientScrolled", value);
            }
        }
        #endregion end of ** client events.

        #region ** methods
        #region ** methods for serialization
        /// <summary>
        /// Determine whether the AnimationOptions property should be serialized to client side.
        /// </summary>
        /// <returns>
        /// Returns true if any subproperty is changed, otherwise return false.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeAnimationOptions()
        {
            return this.AnimationOptions.Disabled
                || this.AnimationOptions.Duration != 400
                || this.AnimationOptions.Easing != Easing.Swing;
        }

        /// <summary>
        /// Determine whether the HScroller property should be serialized to client side.
        /// </summary>
        /// <returns>
        /// Returns true if any subproperty is changed, otherwise return false.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeHScroller()
        {
            return this.HScroller.FirstStepChangeFix != 0
                || this.HScroller.HoverEdgeSpan != 20
                || ((IJsonEmptiable)this.HScroller.IncreaseButtonPosition).IsEmpty
                || ((IJsonEmptiable)this.HScroller.DecreaseButtonPosition).IsEmpty
                || this.HScroller.ScrollBarPosition != HScrollBarPosition.Bottom
                || this.HScroller.ScrollBarVisibility != ScrollBarVisibility.Auto
                || this.HScroller.ScrollLargeChange != (double)10
                || this.HScroller.ScrollMax != (double)200
                || this.HScroller.ScrollMin != (double)0
                || this.HScroller.ScrollMinDragLength != 6
                || this.HScroller.ScrollMode != ScrollMode.ScrollBar
                || this.HScroller.ScrollSmallChange != (double)1
                || this.HScroller.ScrollValue != (double)0;
        }

        /// <summary>
        /// Determine whether the VScroller property should be serialized to client side.
        /// </summary>
        /// <returns>
        /// Returns true if any subproperty is changed, otherwise return false.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeVScroller()
        {
            return this.VScroller.FirstStepChangeFix != 0
                || this.VScroller.HoverEdgeSpan != 20
                || ((IJsonEmptiable)this.VScroller.IncreaseButtonPosition).IsEmpty
                || ((IJsonEmptiable)this.VScroller.DecreaseButtonPosition).IsEmpty
                || this.VScroller.ScrollBarPosition != VScrollBarPosition.Right
                || this.VScroller.ScrollBarVisibility != ScrollBarVisibility.Auto
                || this.VScroller.ScrollLargeChange != (double)10
                || this.VScroller.ScrollMax != (double)200
                || this.VScroller.ScrollMin != (double)0
                || this.VScroller.ScrollMinDragLength != 6
                || this.VScroller.ScrollMode != ScrollMode.ScrollBar
                || this.VScroller.ScrollSmallChange != (double)1
                || this.VScroller.ScrollValue != (double)0;
        }

        /// <summary>
        /// Determine whether the ResizableOptions property should be serialized to client side.
        /// </summary>
        /// <returns>
        /// Returns true if any subproperty is changed, otherwise return false.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeResizableOptions()
        {
            return this.ResizableOptions.Animated
                || this.ResizableOptions.AnimateDuration != 600
                || this.ResizableOptions.AnimateEasing != Easing.Swing
                || this.ResizableOptions.AspectRatio != (float)1
                || this.ResizableOptions.AutoHide
                || this.ResizableOptions.Ghost
                || this.ResizableOptions.Handles != Compass.All
                || this.ResizableOptions.Helper != CONST_RESIZE_HELPER
                || this.ResizableOptions.MaxHeight != 0
                || this.ResizableOptions.MaxWidth != 0
                || this.ResizableOptions.MinHeight != 10
                || this.ResizableOptions.MinWidth != 10;
        }
        #endregion end of ** methods for serialization.
        #endregion end of ** methods.
    }
}