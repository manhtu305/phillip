﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Reflection;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.Licensing
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.Licensing
#endif
{
    class LicCheck
    {
        /// <summary>
        /// Generates the appropriate alert for the control at runtime.  Call this method in the OnPreRender()
        /// override.
        /// </summary>
        /// <param name="isProductLicensed">True if the control has a valid license.</param>
        /// <param name="page">The page.</param>
        /// <param name="control">The control.</param>
        internal static void OnPreRenderCheckLicense(bool isProductLicensed, Page page, System.Web.UI.Control control)
        {
            if (!page.IsPostBack && !page.IsCallback && !isProductLicensed)
                page.ClientScript.RegisterClientScriptBlock(control.GetType(), "Beta" + control.ClientID, GetEvalMessageAlertCode(control), true);
                //page.ClientScript.RegisterClientScriptBlock(control.GetType(), "Beta" + control.ClientID, GetEvalMessageAlertCode(control.ID), true);
        }

        /// <summary>
        /// Gets the eval message alert code.
        /// </summary>
        /// <returns></returns>
        //internal static string GetEvalMessageAlertCode(string id)
        internal static string GetEvalMessageAlertCode(System.Web.UI.Control control)
        {
#if BETA
            return "/*c1eval*>*/alert(\"ComponentOne Studio for ASP.NET (Beta). Copyright 2001-2009, GrapeCity, Inc. All rights reserved.\");/*<*c1eval*/";
#else

            string msg = string.Format(C1Localizer.GetString("Licensing.Eval"), control.GetType().Name);
            //string msg = string.Format(C1Localizer.GetString("Licensing.Eval"), id);
            return "/*c1eval*>*/alert(\"" + msg + "\");/*<*c1eval*/";       
#endif

        }

        /// <summary>
        /// Call this method from Render override.
        /// </summary>
        /// <remarks>This method preliminary used by control developers.</remarks>
        /// <param name="writer">The System.Web.UI.HtmlTextWriter object that receives the control content.</param>
        internal static void RenderLicenseWebComment(HtmlTextWriter writer)
        {
            bool isDesignMode = (System.Web.HttpContext.Current == null);
            const string urlRoot = "http://www.componentone.com/";

            if (!isDesignMode)
            {
                string comment = string.Empty;
                try
                {
                    string[] parts = System.Reflection.Assembly.GetExecutingAssembly()
                        .FullName.Split(",=".ToCharArray());
                    comment = "<!-- " + parts[0] + ", " + parts[2] + "  " + urlRoot + " -->";
                }
                catch 
                {
                    comment = "<!-- " + urlRoot + " -->";
                }

                writer.Write(comment);
                writer.WriteLine();
            }
            //fix: do not render new line for design time
        }


    }
}
