﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Tabs
#else
namespace C1.Web.Wijmo.Controls.C1Tabs
#endif
{
    /// <summary>
    /// Represents the tabs' alignment in respect to the content.
    /// </summary>
    public enum Alignment
    {
        Top = 0,
        Right = 1,
        Bottom = 2,
        Left = 3
    }
}
