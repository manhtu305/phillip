﻿using System.ComponentModel;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;


#if EXTENDER
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Tabs", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Tabs
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Tabs", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1Tabs
#endif
{
#if EXTENDER 
	using C1.Web.Wijmo.Extenders.Localization;
    using C1.Web.Wijmo.Extenders.Converters;
#else
    using C1.Web.Wijmo.Controls.Localization;
    using C1.Web.Wijmo.Controls.Converters;
#endif

    using C1.Web.Wijmo;

    [WidgetDependencies(
        typeof(WijTabs)
#if !EXTENDER
, "extensions.c1tabs.js",
ResourcesConst.C1WRAPPER_OPEN
#endif
)]
#if EXTENDER
	public partial class C1TabsExtender
#else
    public partial class C1Tabs
#endif
    {
        #region ** fields

        private BlindOption _showOption = null;
        private BlindOption _hideOption = null;
        private AjaxSettings _ajaxOptions = null;

        #endregion

        #region ** options

        ///	<summary>
        ///	Determines the tabs' alignment in respect to the content.
        ///	</summary>
        [WidgetOption]
        [DefaultValue(Alignment.Top)]
        [C1Description("C1Tabs.Alignment")]
        public Alignment Alignment
        {
            get
            {
                return GetPropertyValue<Alignment>("Alignment", Alignment.Top);
            }
            set
            {
                SetPropertyValue<Alignment>("Alignment", value);
            }
        }

        ///	<summary>
        ///	Gets or sets the selected index.
        ///	</summary>
        [C1Description("C1Tabs.Selected")]
        [WidgetOption]
        [DefaultValue(0)]
        public int Selected
        {
            get
            {
                return GetPropertyValue<int>("Selected", 0);
            }
            set
            {
                SetPropertyValue<int>("Selected", value);
            }
        }

        /// <summary>
        /// Determines whether the tab can be dragged to a new position.
        /// </summary>
        [C1Description("C1Tabs.Sortable")]
        [WidgetOption]
        [DefaultValue(false)]
        public bool Sortable
        {
            get
            {
                return GetPropertyValue<bool>("Sortable", false);
            }
            set
            {
                SetPropertyValue<bool>("Sortable", value);
            }
        }

        /// <summary>
        /// Determines whether to wrap to the next line or scrolling is enabled when the tabs exceed the specified width.
        /// </summary>
        [C1Description("C1Tabs.Scrollable")]
        [WidgetOption]
        [DefaultValue(false)]
        public bool Scrollable
        {
            get
            {
                return GetPropertyValue<bool>("Scrollable", false);
            }
            set
            {
                SetPropertyValue<bool>("Scrollable", value);
            }
        }

        /// <summary>
        /// Additional Ajax options to consider when loading tab content.
        /// </summary>
        [NotifyParentProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption()]
        [C1Description("C1Tabs.AjaxOptions")]
        public AjaxSettings AjaxOptions
        {
            get
            {
                if (_ajaxOptions == null) _ajaxOptions = new AjaxSettings();

                return GetPropertyValue<AjaxSettings>("AjaxOptions", _ajaxOptions);
            }
            set
            {
                SetPropertyValue<AjaxSettings>("AjaxOptions", value);
            }
        }

        /// <summary>
        /// Whether or not to cache remote tabs content.
        /// </summary>
        [C1Description("C1Tabs.Cache")]
        [WidgetOption]
        [DefaultValue(false)]
        public bool Cache
        {
            get
            {
                return GetPropertyValue<bool>("Cache", false);
            }
            set
            {
                SetPropertyValue<bool>("Cache", value);
            }
        }

        /// <summary>
        /// Determines whether a tab can be collapsed by a user.
        /// </summary>
        [C1Description("C1Tabs.Collapsible")]
        [WidgetOption]
        [DefaultValue(false)]
        public bool Collapsible
        {
            get
            {
                return GetPropertyValue<bool>("Collapsible", false);
            }
            set
            {
                SetPropertyValue<bool>("Collapsible", value);
            }
        }

        /// <summary>
        /// This is an animation option for showing the tabs panel content. 
        /// </summary>
        [C1Description("C1Tabs.ShowOption")]
        [NotifyParentProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption()]
        public BlindOption ShowOption
        {
            get
            {
                if (_showOption == null) _showOption = new BlindOption();

                return GetPropertyValue<BlindOption>("ShowOption", _showOption);
            }
            set
            {
                SetPropertyValue<BlindOption>("ShowOption", value);
            }
        }

        /// <summary>
        /// This is an animation option for hiding the tabs panel content. 
        /// </summary>
        [C1Description("C1Tabs.HideOption")]
        [NotifyParentProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption()]
        public BlindOption HideOption
        {
            get
            {
                if (_hideOption == null) _hideOption = new BlindOption();

                return GetPropertyValue<BlindOption>("HideOption", _hideOption);
            }
            set
            {
                SetPropertyValue<BlindOption>("HideOption", value);
            }
        }

        /// <summary>
        /// The type of event to be used for selecting a tab.
        /// </summary>
        [C1Description("C1Tabs.Event")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue("click")]
        public string Event
        {
            get
            {
                return GetPropertyValue<String>("Event", "click");
            }
            set
            {
                SetPropertyValue<String>("Event", value);
            }
        }

#if EXTENDER

		 ///	<summary>
		///	HTML template from which a new tab panel is created in case of adding a tab with the add method or 
		///	when creating a panel for a remote tab on the fly.
		///	</summary>
        [C1Description("C1Tabs.PanelTemplate")]
		[WidgetOption]
		[DefaultValue("<div></div>")]
		public string PanelTemplate
		{
			get
			{
				return GetPropertyValue<String>("PanelTemplate", "<div></div>");
			}
			set
			{
				SetPropertyValue<String>("PanelTemplate", value);
			}
		}

		///	<summary>
		///	HTML template from which a new tab is created and added. 
		///	The placeholders #{href} and #{label} are replaced with the url and tab label that are passed as 
		///	arguments to the add method.
		///	</summary>
        [C1Description("C1Tabs.TabTemplate")]
		[WidgetOption]
		[DefaultValue("<li><a href=\"#{href}\"><span>#{label}</span></a></li>")]
		public string TabTemplate
		{
			get
			{
				return GetPropertyValue<String>("TabTemplate", "<li><a href=\"#{href}\"><span>#{label}</span></a></li>");
			}
			set
			{
				SetPropertyValue<String>("TabTemplate", value);
			}
		}

		///	<summary>
		///	The HTML content of this string is shown in a tab title while remote content is loading. 
		///	Pass in empty string to deactivate that behavior. 
		///	An span element must be present in the A tag of the title, for the spinner content to be visible.
		///	</summary>
        [C1Description("C1Tabs.Spinner")]
		[WidgetOption]
		[DefaultValue("<em>Loading&#8230;</em>")]
		public string Spinner
		{
			get
			{
				return GetPropertyValue<String>("Spinner", "<em>Loading&#8230;</em>");
			}
			set
			{
				SetPropertyValue<String>("Spinner", value);
			}
		}

#endif

        #endregion

        #region ** events

        /// <summary>
        /// A function called when a tab is added.
        /// </summary>
        [C1Description("C1Tabs.OnClientAdd")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetOption]
        [WidgetEvent]
        [DefaultValue("")]
        public string OnClientAdd
        {
            get
            {
                return GetPropertyValue<String>("OnClientAdd", "");
            }
            set
            {
                SetPropertyValue<String>("OnClientAdd", value);
            }
        }

        /// <summary>
        /// A function called when a tab is removed.
        /// </summary>
        [C1Description("C1Tabs.OnClientRemove")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetOption]
        [WidgetEvent]
        [DefaultValue("")]
        public string OnClientRemove
        {
            get
            {
                return GetPropertyValue<String>("OnClientRemove", "");
            }
            set
            {
                SetPropertyValue<String>("OnClientRemove", value);
            }
        }

        /// <summary>
        /// A function called when a tab is disabled.
        /// </summary>
        [C1Description("C1Tabs.OnClientDisable")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetOption]
        [WidgetEvent]
        [DefaultValue("")]
        public string OnClientDisable
        {
            get
            {
                return GetPropertyValue<String>("OnClientDisable", "");
            }
            set
            {
                SetPropertyValue<String>("OnClientDisable", value);
            }
        }

        /// <summary>
        /// A function called when a tab is enabled.
        /// </summary>
        [C1Description("C1Tabs.OnClientEnable")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetOption]
        [WidgetEvent]
        [DefaultValue("")]
        public string OnClientEnable
        {
            get
            {
                return GetPropertyValue<String>("OnClientEnable", "");
            }
            set
            {
                SetPropertyValue<String>("OnClientEnable", value);
            }
        }

        /// <summary>
        /// A function called after the content of a remote tab has been loaded.
        /// </summary>
        [C1Description("C1Tabs.OnClientLoad")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetOption]
        [WidgetEvent]
        [DefaultValue("")]
        public string OnClientLoad
        {
            get
            {
                return GetPropertyValue<String>("OnClientLoad", "");
            }
            set
            {
                SetPropertyValue<String>("OnClientLoad", value);
            }
        }

        /// <summary>
        /// A function called when clicking a tab.
        /// </summary>
        [C1Description("C1Tabs.OnClientSelect")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetOption]
        [WidgetEvent]
        [DefaultValue("")]
        public string OnClientSelect
        {
            get
            {
                return GetPropertyValue<String>("OnClientSelect", "");
            }
            set
            {
                SetPropertyValue<String>("OnClientSelect", value);
            }
        }

        /// <summary>
        /// A function called when a tab is shown.
        /// </summary>
        [C1Description("C1Tabs.OnClientShow")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetOption]
        [WidgetEvent]
        [DefaultValue("")]
        public string OnClientShow
        {
            get
            {
                return GetPropertyValue<String>("OnClientShow", "");
            }
            set
            {
                SetPropertyValue<String>("OnClientShow", value);
            }
        }

        #endregion

        #region ** protected override


        #endregion
    }
}