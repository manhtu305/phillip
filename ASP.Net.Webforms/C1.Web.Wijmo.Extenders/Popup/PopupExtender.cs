﻿using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using C1.Web.Wijmo;


namespace C1.Web.Wijmo.Extenders.Popup
{

	/// <summary>
	/// Popup Extender.
	/// </summary>
    [TargetControlType(typeof(Panel))]
    [ToolboxItem(true)]
	public class PopupExtender : WidgetExtenderControlBase
    {
        #region ** fields

        private PositionSettings _position = null;

        #endregion

        #region ** options


		[WidgetOption]
		[DefaultValue(false)]
        public bool EnsureOutermost
		{
			get
			{
                return GetPropertyValue("EnsureOutermost", false);
			}
			set
			{
                SetPropertyValue("EnsureOutermost", value);
			}
		}

		[WidgetOption]
        [DefaultValue(ShowEffect.Show)]
        public ShowEffect ShowEffect
		{
			get
			{
                return GetPropertyValue("ShowEffect", ShowEffect.Show);
			}
			set
			{
                SetPropertyValue("ShowEffect", value);
			}
		}

        [WidgetOption]
        [DefaultValue(300)]
        public int ShowDuration
        {
            get
            {
                return GetPropertyValue("ShowDuration", 300);
            }
            set
            {
                SetPropertyValue("ShowDuration", value);
            }
        }

        [WidgetOption]
        [DefaultValue(HideEffect.Hide)]
        public HideEffect HideEffect
        {
            get
            {
                return GetPropertyValue("HideEffect", HideEffect.Hide);
            }
            set
            {
                SetPropertyValue("HideEffect", value);
            }
        }

        [WidgetOption]
        [DefaultValue(100)]
        public int HideDuration
        {
            get
            {
                return GetPropertyValue("HideDuration", 100);
            }
            set
            {
                SetPropertyValue("HideDuration", value);
            }
        }

        [WidgetOption]
        [DefaultValue(false)]
        public bool AutoHide
        {
            get
            {
                return GetPropertyValue("AutoHide", false);
            }
            set
            {
                SetPropertyValue("AutoHide", value);
            }
        }

        //[NotifyParentProperty(true)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        //[PersistenceMode(PersistenceMode.InnerProperty)]
        //[TypeConverter(typeof(ExpandableObjectConverter))]
        //[WidgetOption()]
        //public PositionSettings Position
        //{
        //    get
        //    {
        //        if (_position == null)
        //        {
        //            _position = new PositionSettings();
        //            _position.At.Left = 
        //        }

        //        return GetPropertyValue<PositionSettings>("Position", _position);
        //    }
        //    set
        //    {
        //        SetPropertyValue("Position", value);
        //    }
        //}

		
		#endregion
    }
}

