﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Extenders.Popup
{
    public enum ShowEffect
    {
        Show = 0,
        Blind = 1, 
        Clip = 2, 
        Drop = 3, 
        Fade = 4, 
        Fold = 5, 
        Slide = 6, 
        Pulsate = 7
    }

    public enum HideEffect
    {
        Hide = 0,
        Blind = 1,
        Clip = 2,
        Drop = 3,
        Fade = 4,
        Fold = 5,
        Slide = 6,
        Pulsate = 7
    }
}
