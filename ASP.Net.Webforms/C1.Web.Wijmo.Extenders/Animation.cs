﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Design;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Text;
using System.Reflection;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{

	#region ** Animation class
	/// <summary>
	/// Define the options for the jQuery animation.
	/// </summary>
	public class Animation : Settings
	{

		private Dictionary<string, string> _options;
		private AnimatedOption _animated;
		/// <summary>
		/// Animation constructor.
		/// </summary>
		public Animation()
		{
			this.Animated = new AnimatedOption();
			this.Duration = 400;
			this.Easing = Easing.Swing;
			this.Option = new Dictionary<string, string>();
		}

		/// <summary>
		/// Determines the effects to animate. 
		/// If user want disabled the animation, can set the Animation.disabled = true.
		/// If user want set an animation effect, such as "slide", "fade", 
		/// user set the animation.effect = "slide"
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[C1Description("Animation.Animated")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public AnimatedOption Animated
		{
			get
			{
				if (_animated == null)
				{
					_animated = new AnimatedOption();
				}
				return _animated;
			}
			set
			{
				_animated = value;
			}
		}

		/// <summary>
		/// Defines the amount of time the animation is displayed.
		/// </summary>
		[DefaultValue(400)]
		[C1Description("Animation.Duration")]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int Duration
		{
			get
			{
				return base.GetPropertyValue<int>("Duration", 400);
			}
			set
			{
				SetPropertyValue<int>("Duration", value);
			}
		}

		/// <summary>
		/// Determines the easing effect to be applied to the animation. 
		/// There are over 30 easing effects, including Linear, Swing, EaseInBounce, EaseOutBounce and many more.
		/// </summary>
		[DefaultValue(Easing.Swing)]
		[C1Description("Animation.Easing")]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Easing Easing
		{
			get
			{
				return GetPropertyValue<Easing>("Easing", Easing.Swing);
			}
			set
			{
				SetPropertyValue<Easing>("Easing", value);
			}
		}

		/// <summary>
		/// Defines the option for some effects.
		/// If user set the animated to "slide", in jQuery.Effect plugin, 
		/// the slide effect contains some arguments such as direction, distance
		/// </summary>
		[NotifyParentProperty(true)]
		[C1Description("Animation.Option")]
		[PersistenceMode(PersistenceMode.Attribute)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
		[TypeConverter(typeof(StringDictionaryConverter))]
#if ASP_NET4
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.StringDictionaryEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.StringDictionaryEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#endif
#endif
		public Dictionary<string, string> Option
		{
			get
			{
				if (_options == null)
				{
					_options = new Dictionary<string, string>();
				}
				return _options;
			}
			set
			{
				_options = value;
			}
		}

		#region ** methods for serialization\
		/// <summary>
		/// Determine whether the Animation property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeAnimated()
		{
			return !this.Animated.Disabled
				|| this.Animated.Effect == "slide";
		}

		/// <summary>
		/// Determine whether the Animation property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeOptions() 
		{
			return this.Option.Count > 0;
		}
		#endregion
	}
	#endregion end of ** Animation class

	#region ** AnimatedOption class
	/// <summary>
	/// Represents the AnimatedOption class which contains all of the settings for the Animation's animated option.
	/// </summary>
	public class AnimatedOption : Settings, ICustomOptionType, IJsonEmptiable
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="AnimatedOption"/> class.
		/// </summary>
		public AnimatedOption()
		{
			this.Effect = "slide";
		}

		/// <summary>
		/// Determines whether the animation is used.
		/// </summary>
		[DefaultValue(false)]
		[C1Description("Animation.Disabled")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Disabled
		{
			get
			{
				return GetPropertyValue<bool>("Disabled", false);
			}
			set
			{
				SetPropertyValue<bool>("Disabled", value);
			}
		}

		/// <summary>
		/// Define which effect of the animation. It describe the jquery.effect plugin.
		/// </summary>
		[DefaultValue("slide")]
		[C1Description("Animation.Effect")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string Effect
		{
			get
			{
				return GetPropertyValue<string>("Effect", "slide");
			}
			set
			{
				SetPropertyValue<string>("Effect", value);
			}
		}

		#region ** implement ICustomOptionType interface
		/// <summary>
		///Serialize the animated value.
		/// </summary>
		/// <returns></returns>
		string ICustomOptionType.SerializeValue()
		{
			if (this.Disabled)
			{
				return "false";
			}
			else
			{
				return this.Effect;
			}
		}

#if !EXTENDER
		//TODO:
		void ICustomOptionType.DeSerializeValue(object state)
		{
			if (state is bool)
			{
				this.Disabled = (bool)state;
			}
			else if (state is string)
			{
				this.Effect = (string)state;
			}
		}
#endif

		bool ICustomOptionType.IncludeQuotes
		{
			get
			{
				if (this.Disabled)
				{
					return false;
				}

				return true;
			}
		}

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			if (this.Disabled)
			{
				return C1Localizer.GetString("Animation.AnimationDisabled", "Animation disabled.");
			}
			else {
				return string.Format(C1Localizer.GetString("Animation.EffectNameFormat",  "Effect: {0}"), Effect);
			}
		}

		#endregion end of ** implement ICustomOptionType interface

		/// <summary>
		/// Get whether this class should serialize.
		/// </summary>
		/// <returns></returns>
		protected internal bool ShouldSerialize()
		{
			return Disabled || Effect != "slide";
		}

		bool IJsonEmptiable.IsEmpty
		{
			get { return !this.ShouldSerialize(); }
		}
	}
	#endregion end of ** AnimatedOption class


	/// <summary>
	/// Define the animation options for Tabs and Wizard.
	/// </summary>
	[PersistChildren(false), ParseChildren(true)]
	public class BlindOption: Settings 
	{
		/// <summary>
		/// Determines whether blind animation is used.
		/// </summary>
		[C1Description("BlindOption.Blind")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(false)]
		public bool Blind
		{
			get
			{
				return GetPropertyValue<bool>("Blind", false);
			}
			set
			{
				SetPropertyValue<bool>("Blind", value);
			}
		}

		/// <summary>
		/// Determines whether fade animation is used.
		/// </summary>
		[C1Description("BlindOption.Fade")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(true)]
		public bool Fade
		{
			get
			{
				return GetPropertyValue<bool>("Fade", true);
			}
			set
			{
				SetPropertyValue<bool>("Fade", value);
			}
		}

		/// <summary>
		/// The duration for blind or fade animation.
		/// </summary>
		[C1Description("BlindOption.Duration")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(400)]
		public int Duration
		{
			get
			{
				return GetPropertyValue<int>("Duration", 400);
			}
			set
			{
				SetPropertyValue<int>("Duration", value);
			}
		}
	}

#if !EXTENDER
	/// <summary>
	///  Provides a unified way of converting types of values to other types
	/// </summary>
	public class StringDictionaryConverter : TypeConverter
	{
		/// <summary>
		/// Returns whether this converter can convert an object of the given type to the type of this converter, using the specified context.
		/// </summary>
		/// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
		/// <param name="sourceType">A System.Type that represents the type you want to convert from.</param>
		/// <returns>true if this converter can perform the conversion; otherwise, false.</returns>
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return ((sourceType == typeof(string)) || base.CanConvertFrom(context, sourceType));
		}

		/// <summary>
		/// Returns whether this converter can convert the object to the specified type, using the specified context.
		/// </summary>
		/// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
		/// <param name="destinationType">A System.Type that represents the type you want to convert to.</param>
		/// <returns>true if this converter can perform the conversion; otherwise, false.</returns>
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return ((destinationType == typeof(InstanceDescriptor)) || base.CanConvertTo(context, destinationType));
		}

		/// <summary>
		/// Converts the given object to the type of this converter, using the specified context and culture information.
		/// </summary>
		/// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
		/// <param name="culture">The System.Globalization.CultureInfo to use as the current culture.</param>
		/// <param name="value">The System.Object to convert.</param>
		/// <returns>An System.Object that represents the converted value.</returns>
		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			// format:
			// key1=value1;key2=value2
			if (value is string)
			{
				Dictionary<string, string> dictionary = new Dictionary<string, string>();

				string[] dictionaryItems = ((string)value).Split(';');
				foreach (string dictionaryItem in dictionaryItems)
				{
					string[] keyValuePair = dictionaryItem.Split(':');
					if (keyValuePair.Length == 2)
						dictionary.Add(keyValuePair[0].Trim(), keyValuePair[1].Trim());
				}
				return dictionary;
			}
			return base.ConvertFrom(context, culture, value);
		}

		/// <summary>
		/// Converts the given value object to the specified type, using the specified context and culture information.
		/// </summary>
		/// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
		/// <param name="culture">A System.Globalization.CultureInfo. If null is passed, the current culture is assumed.</param>
		/// <param name="value">The System.Object to convert.</param>
		/// <param name="destinationType">The System.Type to convert the value parameter to.</param>
		/// <returns>An System.Object that represents the converted value.</returns>
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == null)
				throw new ArgumentNullException("destinationType");
			if ((destinationType == typeof(string)) && (value is Dictionary<string, string>))
			{
				StringBuilder sb = new StringBuilder();
				foreach (KeyValuePair<string, string> dictionaryItem in (Dictionary<string, string>)value)
				{
					sb.AppendFormat("{0}:{1};", dictionaryItem.Key, dictionaryItem.Value);
				}
				return sb.ToString();
			}
			if (destinationType == typeof(InstanceDescriptor))
			{
				//Dictionary<string, string> isc = new Dictionary<string, string>();

				//ConstructorInfo ci = typeof(Dictionary<string, string>).GetConstructor(new Type[] { typeof(IDictionary<string, string>) });
				//IDictionary<string, string> dict = value as Dictionary<string, string>;
				//if (dict == null)
				//{
				//    dict = new Dictionary<string, string>() { };
				//}
				//InstanceDescriptor descriptor = new InstanceDescriptor(ci, new object[] { dict });

				//return descriptor;

				ConstructorInfo ci = typeof(Dictionary<string, string>).GetConstructor(System.Type.EmptyTypes);

				return new InstanceDescriptor(ci, null, false);
			}
			return base.ConvertTo(context, culture, value, destinationType);
		}

	}
#endif
}
