﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	[WidgetDependencies(
        typeof(WijBarChart)
#if !EXTENDER
        , "extensions.c1barchart.js",
		ResourcesConst.C1WRAPPER_PRO
#endif
    )]
#if EXTENDER
	public partial class C1BarChartExtender : C1ChartCoreExtender<BarChartSeries,  ChartAnimation>
#else
	public partial class C1BarChart : C1ChartCore<BarChartSeries, ChartAnimation, C1ChartBinding>
#endif
	{

		private void InitChart()
		{
			this.SeriesTransition = new ChartAnimation();
			this.SeriesTransition.Duration = 400;
			//this.SeriesTransition.Easing = ">";
			this.SeriesTransition.Easing = ChartEasing.EaseInCubic;
		}

		/// <summary>
		/// A value that determines whether the bar chart renders horizontal or vertical.
		/// </summary>
		[DefaultValue(true)]
		[C1Description("C1Chart.BarChart.Horizontal")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public bool Horizontal 
		{
			get
			{
				return this.GetPropertyValue<bool>("Horizontal", true);
			} 
			set
			{
				this.SetPropertyValue<bool>("Horizontal", value);
			}
		}

		/// <summary>
		/// A value that determines whether to show a stacked chart.
		/// </summary>
		[DefaultValue(false)]
		[C1Description("C1Chart.BarChart.Stacked")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Stacked
		{
			get
			{
				return this.GetPropertyValue<bool>("Stacked", false);
			}
			set
			{
				this.SetPropertyValue<bool>("Stacked", value);
			}
		}

		/// <summary>
		/// A value that determines whether to show a stacked and percentage chart.
		/// </summary>
		[DefaultValue(false)]
		[C1Description("C1Chart.BarChart.Is100Percent")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Is100Percent
		{
			get
			{
				return this.GetPropertyValue<bool>("Is100Percent", false);
			}
			set
			{
				this.SetPropertyValue<bool>("Is100Percent", value);
			}
		}

		/// <summary>
		/// A value that indicates the percentage of bar elements in the same cluster overlap.
		/// </summary>
		[DefaultValue(0)]
		[C1Description("C1Chart.BarChart.ClusterOverlap")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int ClusterOverlap
		{
			get
			{
				return this.GetPropertyValue<int>("ClusterOverlap", 0);
			}
			set
			{
				this.SetPropertyValue<int>("ClusterOverlap", value);
			}
		}

		/// <summary>
		/// A value that indicates the percentage of the plot area that each bar cluster occupies.
		/// </summary>
		[DefaultValue(85)]
		[C1Description("C1Chart.BarChart.ClusterWidth")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Sizes)]
#endif
		public int ClusterWidth
		{
			get
			{
				return this.GetPropertyValue<int>("ClusterWidth", 85);
			}
			set
			{
				this.SetPropertyValue<int>("ClusterWidth", value);
			}
		}

		/// <summary>
		/// A value that indicates the corner-radius for the bar.
		/// </summary>
		[DefaultValue(0)]
		[C1Description("C1Chart.BarChart.ClusterRadius")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int ClusterRadius
		{
			get
			{
				return this.GetPropertyValue<int>("ClusterRadius", 0);
			}
			set
			{
				this.SetPropertyValue<int>("ClusterRadius", value);
			}
		}

		/// <summary>
		/// A value that indicates the spacing between the adjacent bars.
		/// </summary>
		[DefaultValue(0)]
		[C1Description("C1Chart.BarChart.ClusterSpacing")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int ClusterSpacing
		{
			get
			{
				return this.GetPropertyValue<int>("ClusterSpacing", 0);
			}
			set
			{
				this.SetPropertyValue<int>("ClusterSpacing", value);
			}
		}


		/// <summary>
		/// A value that indicates whether to show the effect and duration of the animation when reloading the data.
		/// </summary>
		[C1Description("C1Chart.BarChart.SeriesTransition")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public ChartAnimation SeriesTransition { get; set; }


		//2011.6.28 add comments to fix bug 15868.
		/// <summary>
		/// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered in the browser window.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			//axis.compass is reset at client side.
			/*
			if (Horizontal)
			{
				if (this.Axis.X.Compass == ChartCompass.North || this.Axis.X.Compass == ChartCompass.South)
				{
					this.Axis.X.Compass = ChartCompass.West;
				}
				if (this.Axis.Y.Compass == ChartCompass.East || this.Axis.Y.Compass == ChartCompass.West)
				{
					this.Axis.Y.Compass = ChartCompass.South;
				}
			}
			else
			{
				if (this.Axis.X.Compass == ChartCompass.West || this.Axis.X.Compass == ChartCompass.East)
				{
					this.Axis.X.Compass = ChartCompass.South;
				}
				if (this.Axis.Y.Compass == ChartCompass.South || this.Axis.Y.Compass == ChartCompass.North)
				{
					this.Axis.Y.Compass = ChartCompass.West;
				}
			}
			 * */

			base.Render(writer);
		}
		//end comments

		// added in 2013-10-31, fixing the OADateTime issue, move this method from C1BarChart to this file.
		/// <summary>
		/// Gets the series data of the specified series.
		/// </summary>
		/// <param name="series">
		/// The specified series.
		/// </param>
		/// <returns>
		/// Returns the series data which retrieved from the specified series.
		/// </returns>
		protected override ChartSeriesData GetSeriesData(BarChartSeries series)
		{
            if (series.IsTrendline)
                return base.GetSeriesData(series);

			return series.Data;
		}

		#region Serialize

		/// <summary>
		/// Determine whether the SeriesTransition property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns false if SeriesTransition has default values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeSeriesTransition()
		{
			if (!this.SeriesTransition.Enabled)
				return true;
			if (this.SeriesTransition.Duration != 400)
				return true;
			//if (this.SeriesTransition.Easing != ">")
			if (this.SeriesTransition.Easing != ChartEasing.EaseInCubic)
				return true;
			return false;
		}

		#endregion
	}
}
