﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	/// <summary>
	/// Represents the BarChartSeries class which contains all of the settings for the chart series.
	/// </summary>
	public class BarChartSeries : ChartSeries
	{

		/// <summary>
		/// Initializes a new instance of the BarChartSeries class.
		/// </summary>
		public BarChartSeries()
			: base()
		{
			this.Data = new ChartSeriesData();
		}

		/// <summary>
		/// A value that indicates the data of the chart series.
		/// </summary>
		[C1Description("C1Chart.ChartSeries.Data")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public ChartSeriesData Data { get; set; }

		/// <summary>
		/// Returns a boolean value that indicates whether the bar chart series
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a boolean value to determine whether this series should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal override bool ShouldSerialize()
		{
			var shouldSerialize = base.ShouldSerialize();
			if (shouldSerialize)
				return true;
			if (Data.ShouldSerialize())
				return true;
			return false;
		}
	}
}
