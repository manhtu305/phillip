﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Globalization;
using System.Drawing;

#if EXTENDER 
namespace C1.Web.Wijmo.Extenders.Converters
#else
namespace C1.Web.Wijmo.Controls.Converters
#endif
{
    /// <summary>
    ///  Converts a predefined color name or an RGB color value to and 
    ///  from a System.Drawing.Color object.
    /// </summary>
    public class C1WebColorConverter : WebColorConverter
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public C1WebColorConverter()
            : base()
        {

        }

        #region --- public override ---

        /// <summary>
        /// ConvertFrom override.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="culture"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            try
            {
                return base.ConvertFrom(context, culture, value);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// ConvertTo override
        /// </summary>
        /// <param name="context"></param>
        /// <param name="culture"></param>
        /// <param name="value"></param>
        /// <param name="destinationType"></param>
        /// <returns></returns>
        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            try
            {
                return base.ConvertTo(context, culture, value, destinationType);
            }
            catch (Exception)
            {
                return null;
            }
        }
        #endregion

        #region --- public static ---

	    /// <summary>
	    /// Converts Color type to string representation.
	    /// </summary>
	    /// <param name="c">Color to convert.</param>
	    /// <returns></returns>
	    public static string ToStringHexColor(Color c)
	    {
		    if (c != Color.Empty)
		    {
			    if (!c.IsSystemColor && c.IsNamedColor)
			    {
				    string ret = c.Name;
				    ret = ret.Replace("LightGray", "LightGrey");
				    return ret;
			    }

			    return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
		    }
		    return "";
	    }

	    /// <summary>
        /// Convert string to Color type.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static Color ToColor(string s)
        {
            if (s != null)
                s = s.Replace("LightGrey", "LightGray");
            return (Color)new WebColorConverter().ConvertFromString(s);
        }

        #endregion
    }
}
