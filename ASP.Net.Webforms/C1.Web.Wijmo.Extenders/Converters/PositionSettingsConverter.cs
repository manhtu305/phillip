﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

#if EXTENDER 
namespace C1.Web.Wijmo.Extenders.Converters
#else
namespace C1.Web.Wijmo.Controls.Converters
#endif
{
	internal class PositionSettingsConverter:TypeConverter
	{

		public static string ToString(PositionSettings pSettings)
		{
			if (pSettings == null)
			{
				return "";
			}
			StringBuilder sb = new StringBuilder();
			sb.Append("{");
			if (pSettings.My.ToString() != "Left, Top")
			{
				sb.AppendFormat("my: '{0} {1}', ", pSettings.My.Left, pSettings.My.Top);
			}
			if (pSettings.At.ToString() != "Left, Top")
			{
				sb.AppendFormat("at: '{0} {1}', ", pSettings.At.Left, pSettings.At.Top);
			}
			if (pSettings.Offset.ToString() != "0, 0")
			{
				sb.AppendFormat("offset: '{0} {1}', ", pSettings.Offset.Left, pSettings.Offset.Top);
			}
			if (pSettings.Collision.ToString() != "Flip, Flip")
			{
				sb.AppendFormat("collision: '{0} {1}', ", pSettings.Collision.Left, pSettings.Collision.Top);
			}
			if (sb.Length > 2)
			{
				sb.Remove(sb.Length - 2, 2);
			}
			sb.Append("}");

			return sb.ToString().ToLower() ;
		}
	}
}
