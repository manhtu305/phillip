﻿using System;
using System.ComponentModel;

#if EXTENDER 
namespace C1.Web.Wijmo.Extenders.Converters
#else
namespace C1.Web.Wijmo.Controls.Converters
#endif
{
    using System.Globalization;
    using System.Text.RegularExpressions;

    /// <summary>
    /// When enumeration is converted to a string puts enumeration members into quote symbols.
    /// </summary>
    internal class QuotedEnumConverter : EnumConverter
    {
        private static readonly Regex _reComma = new Regex(@",\s*", RegexOptions.Compiled);
        private static readonly Regex _reQuote = new Regex(@"\""", RegexOptions.Compiled);

        public QuotedEnumConverter(Type type) : base(type)
        {
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value.GetType() == typeof(string))
            {
                return base.ConvertFrom(context, culture, _reQuote.Replace((string)value, string.Empty));
            }

            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                return string.Format("\"{0}\"",
                    _reComma.Replace((string)base.ConvertTo(context, culture, value, destinationType), "\", \"").ToLower());
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
