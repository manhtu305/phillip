﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Collections;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Text.RegularExpressions;


#if EXTENDER 
namespace C1.Web.Wijmo.Extenders.Converters
#else
namespace C1.Web.Wijmo.Controls.Converters
#endif
{
	/// <summary>
	/// The double array converter.
	/// </summary>
    public class DoubleArrayConverter : BaseArrayConverter<double, DoubleConverter>
    {
    }

	/// <summary>
	/// The datetime array converter.
	/// </summary>
    public class DateTimeArrayConverter : BaseArrayConverter<DateTime, DateTimeConverter>
    {
    }

	/// <summary>
	/// The Array converter
	/// </summary>
	/// <typeparam name="T">The array type</typeparam>
	/// <typeparam name="TConverter">The converter of the array element.</typeparam>
    public class BaseArrayConverter<T, TConverter> : TypeConverter
        where TConverter : TypeConverter
    {

		/// <summary>
		/// A value that indicate whether the array can convert from sourcetype
		/// </summary>
		/// <param name="context"></param>
		/// <param name="sourceType"></param>
		/// <returns></returns>
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string);
        }

		/// <summary>
		/// Convert to array from the object value.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="culture"></param>
		/// <param name="value"></param>
		/// <returns></returns>
        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value is string)
            {
                string s = ((string)value).Trim();
                List<T> items = new List<T>();

                if (s.Length == 0)
                {
                    return new T[0];
                }

                string[] values = s.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string item in values)
                {
                    TConverter converter = System.Activator.CreateInstance<TConverter>();
                    items.Add((T)converter.ConvertFrom(context, culture, item));
                }

                return items.ToArray();
            }

            return base.ConvertFrom(context, culture, value);
        }

		/// <summary>
		/// Convert the array to string
		/// </summary>
		/// <param name="context"></param>
		/// <param name="culture"></param>
		/// <param name="value"></param>
		/// <param name="destinationType"></param>
		/// <returns></returns>
        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (value is T[] && destinationType == typeof(string))
            {
                T[] items = value as T[];
                StringBuilder sb = new StringBuilder();

                if (items.Length == 0)
                {
                    return string.Empty;
                }

                foreach (object item in items)
                {
                    TConverter converter = System.Activator.CreateInstance<TConverter>();
                    sb.AppendFormat("{0}, ", converter.ConvertTo(context, culture, item, destinationType));
                }

                sb.Remove(sb.Length - 2, 2);
                return sb.ToString();
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }
    } 
}
