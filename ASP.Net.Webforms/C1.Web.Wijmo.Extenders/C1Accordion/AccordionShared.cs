﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using C1.Web.Wijmo;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Accordion", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Accordion
#else
using C1.Web.Wijmo.Controls.Localization;
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Accordion", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1Accordion
#endif
{
	[WidgetDependencies(
        typeof(WijAccordion)
#if !EXTENDER
		, "extensions.c1accordion.js",
		ResourcesConst.C1WRAPPER_OPEN
#endif		
	)]
	#if EXTENDER 	
	public partial class C1AccordionExtender
#else
	public partial class C1Accordion
	#endif
	{

		#region ** fields

		AnimatedOption _Animated;

		#endregion

		#region ** options
		/// <summary>
		/// Determines the animation easing effect; set this option to false in 
		/// order to disable animation.
		/// Additional options 
		/// that are available for the animation function include:
		/// expand - value of true indicates that content element must be expanded.
		/// horizontal - value of true indicates that expander is horizontally 
		///	orientated (when expandDirection is left or right).
		/// content - jQuery object that contains content element to be expanded or 
		///				collapsed.
		/// </summary>
		[C1Description("C1Accordion.Animated", "Determines the animation easing effect.")]
        [C1Category("Category.Behavior")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption()]
		public AnimatedOption Animated
		{
			get
			{
				if (_Animated == null)
					_Animated = new AnimatedOption();
				return _Animated;
			}
		}

		/// <summary>
		/// Determines the event that triggers the accordion.
		/// </summary>
		[C1Description("C1Accordion.Event", "Determines the event that triggers the accordion.")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue("click")]
		public string Event
		{
			get
			{
				return GetPropertyValue("Event", "click");
			}
			set
			{
				SetPropertyValue("Event", value);
			}
		}

		/// <summary>
		/// Determines the content expand direction. Available values are top, right,
		///	bottom, and left. 
		/// </summary>
		[C1Description("C1Accordion.ExpandDirection", "Determines the content expand direction. Available values are top, right, bottom, and left.")]
        [C1Category("Category.Appearance")]		
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption]
		[DefaultValue(ExpandDirection.Bottom)]
		public ExpandDirection ExpandDirection
		{
			get
			{
				return GetPropertyValue("ExpandDirection", ExpandDirection.Bottom);
			}
			set
			{
				SetPropertyValue("ExpandDirection", value);
			}
		}

		/// <summary>
		/// Selector for the header element. By using this option you can put 
		///	header/content elements inside LI tags or into any other more complex 
		///	html markup.
		/// </summary>
		[C1Description("C1Accordion.Header", "Selector for the header element. By using this option you can put header/content elements inside LI tags or into any other more complex html markup.")]
        [C1Category("Category.Appearance")]
		[WidgetOption]
		[DefaultValue("> li > :first-child,> :not(li):even")]
		public string Header
		{
			get
			{
				return GetPropertyValue("Header", "> li > :first-child,> :not(li):even");
			}
			set
			{
				SetPropertyValue("Header", value);
			}
		}

		/// <summary>
		/// Determines whether clicking the header will close the currently opened 
		///	pane (leaving all the accordion's panes closed).
		/// </summary>
		[C1Description("C1Accordion.RequireOpenedPane", "Determines whether clicking the header will close the currently opened pane (leaving all the accordion's panes closed).")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool RequireOpenedPane
		{
			get
			{
				return GetPropertyValue("RequireOpenedPane", true);
			}
			set
			{
				SetPropertyValue("RequireOpenedPane", value);
			}
		}

		/// <summary>
		/// Gets or sets the index of the currently expanded accordion pane.
		/// </summary>
		[C1Description("C1Accordion.SelectedIndex", "Index of the currently expanded accordion pane.")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(0)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int SelectedIndex
		{
			get
			{
				return GetPropertyValue("SelectedIndex", 0);
			}
			set
			{
				SetPropertyValue("SelectedIndex", value);
			}
		}

		#region ** client events

		/// <summary>
		/// Occurs before an active accordion pane changes. The event is cancelable.
		/// </summary>
		[C1Description("C1Accordion.OnClientBeforeSelectedIndexChanged", "Occurs before an active accordion pane changes. The event is cancelable.")]
        [C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, args")]
		[DefaultValue("")]
		public string OnClientBeforeSelectedIndexChanged
		{
			get
			{
				return GetPropertyValue("OnClientBeforeSelectedIndexChanged", "");
			}
			set
			{
				SetPropertyValue("OnClientBeforeSelectedIndexChanged", value);
			}
		}

		/// <summary>
		/// Occurs when an active accordion pane changed.
		/// </summary>
		[C1Description("C1Accordion.OnClientSelectedIndexChanged", "Occurs when an active accordion pane changed.")]
        [C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, args")]
		[DefaultValue("")]
		public string OnClientSelectedIndexChanged
		{
			get
			{
				return GetPropertyValue("OnClientSelectedIndexChanged", "");
			}
			set
			{
				SetPropertyValue("OnClientSelectedIndexChanged", value);
			}
		}

		#endregion

		#endregion
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the ExpandDirection 
	/// property in the C1Accordion class. This enumeration specifies different 
	/// expand directions (top, right, bottom, and left) for the C1Accordion control.
	/// </summary>
	public enum ExpandDirection
	{

		/// <summary>
		/// Top.
		/// </summary>
		Top = 0,
		/// <summary>
		/// Right.
		/// </summary>
		Right = 1,
		/// <summary>
		/// Bottom.
		/// </summary>
		Bottom = 2,
		/// <summary>
		/// Left.
		/// </summary>
		Left = 3
	}
}
