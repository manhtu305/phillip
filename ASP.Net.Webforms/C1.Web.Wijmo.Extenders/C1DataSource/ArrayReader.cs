﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Collections;

namespace C1.Web.Wijmo.Extenders.C1DataSource
{
	/// <summary>
	/// Represents a data reader in the <see cref="C1DataSource"/> extender. 
	/// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class ArrayReader : ICustomOptionType
    {

        private List<ReaderField> _fields;

        /// <summary>
        /// The list of the field to restructure the data
        /// </summary>
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public List<ReaderField> Fields
        {
            get
            {
                if (this._fields == null)
                {
                    this._fields = new List<ReaderField>();
                }
                return this._fields;
            }
        }

        internal bool ShouldSerialize()
        {
            return this.ShouldSerializeFields();
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeFields()
        {
            foreach (ReaderField field in this.Fields)
            {
                if (field.ShouldSerialize())
                {
                    return true;
                }
            }
            return false;
        }

        internal void Reset()
        {
            this.ResetFields();
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ResetFields()
        {
            this.Fields.Clear();
        }

        #region ICustomOptionType Members

        string ICustomOptionType.SerializeValue()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("new wijarrayreader(");
            builder.Append(WidgetObjectBuilder.SerializeOption(this.Fields));
            builder.Append(")");
            return builder.ToString();
        }

        bool ICustomOptionType.IncludeQuotes
        {
            get
            {
                return false;
            }
        }

        #endregion
    }
}
