﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.Design;

namespace C1.Web.Wijmo.Extenders.C1DataSource
{
    internal class C1DataSourceDesigner : ControlDesigner
    {
        public override string GetDesignTimeHtml()
        {
            return this.CreatePlaceHolderDesignTimeHtml();
        }
    }
}
