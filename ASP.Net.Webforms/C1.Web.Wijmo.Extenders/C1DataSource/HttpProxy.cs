﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

namespace C1.Web.Wijmo.Extenders.C1DataSource
{
	/// <summary>
	/// Represents the http proxy of the <see cref="C1DataSource"/> control. 
	/// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class HttpProxy : ICustomOptionType
    {
        private List<ProxyField> _options;

        /// <summary>
        /// The Ajax Options used to get the data
        /// </summary>
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        public List<ProxyField> Options
        {
            get
            {
                if (this._options == null)
                {
                    this._options = new List<ProxyField>();
                }
                return this._options;
            }
        }

        internal bool ShouldSerialize()
        {
            return this.ShouldSerializeOptions();
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeOptions()
        {
            foreach (ProxyField field in this.Options)
            {
                if (field.ShouldSerialize())
                {
                    return true;
                }
            }
            return false;
        }

        internal void Reset()
        {
            this.ResetOptions();
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ResetOptions()
        {
            this.Options.Clear();
        }

        #region ICustomOptionType Members

        string ICustomOptionType.SerializeValue()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("new wijhttpproxy(");
            //builder.Append(WidgetObjectBuilder.SerializeOption(this.Options));
            builder.Append("{");
            foreach (ProxyField field in this.Options)
            {
                if (field.ShouldSerialize())
                {
                    builder.Append(field.Name);
                    builder.Append(": ");
                    if (field.Value.NeedQuotes)
                    {
                        builder.Append("\"");
                    }
                    builder.Append(field.Value.ValueString);
                    if (field.Value.NeedQuotes)
                    {
                        builder.Append("\"");
                    }
                }
                builder.Append(", ");
            }
            builder.Remove(builder.Length - 2, 2);
            builder.Append("}");
            builder.Append(")");
            return builder.ToString();
        }

        bool ICustomOptionType.IncludeQuotes
        {
            get
            {
                return false;
            }
        }

        #endregion
    }
}
