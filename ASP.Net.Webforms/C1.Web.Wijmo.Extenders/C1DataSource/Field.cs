﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

namespace C1.Web.Wijmo.Extenders.C1DataSource
{
	/// <summary>
	/// Represents the field list in the <see cref="ArrayReader"/>. 
	/// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class ReaderField
    {
        public ReaderField()
        {
            this.DefaultValue = new ObjectValue();
            this.Mapping = new Mapping();
        }

        /// <summary>
        /// The name of the field used to restructure the data
        /// </summary>
        [DefaultValue(null)]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// The default value of the field to restructure the data
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public ObjectValue DefaultValue
        {
            get;
            set;
        }

        /// <summary>
        /// The mapping relation of the field to restructure the data
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public Mapping Mapping
        {
            get;
            set;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeDefaultValue()
        {
            return this.DefaultValue.ShouldSerialize();
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeMapping()
        {
            return this.Mapping.ShouldSerialize();
        }

        internal bool ShouldSerialize()
        {
            return !string.IsNullOrEmpty(this.Name);
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ResetDefaultValue()
        {
            this.DefaultValue.Reset();
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ResetMapping()
        {
            this.Mapping.Reset();
        }
    }

    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class ProxyField
    {
        public ProxyField()
        {
            this.Value = new ObjectValue();
        }

        /// <summary>
        /// The name of the option to be set.
        /// </summary>
        [DefaultValue(null)]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// The value of the option to be set.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public ObjectValue Value
        {
            get;
            set;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeValue()
        {
            return this.Value.ShouldSerialize();
        }

        internal bool ShouldSerialize()
        {
            return !string.IsNullOrEmpty(this.Name) && this.ShouldSerializeValue();
        }

        public void ResetValue()
        {
            this.Value.Reset();
        }
    }

    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Mapping : ICustomOptionType
    {
        #region Properties

        public Mapping()
        {
            this.Index = -1;
        }

        /// <summary>
        /// The number used to map the data
        /// </summary>
        [NotifyParentProperty(true)]
        [DefaultValue(-1)]
        public int Index
        {
            get;
            set;
        }

        /// <summary>
        /// The name used to map the data
        /// </summary>
        [NotifyParentProperty(true)]
        [DefaultValue(null)]
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// The function used to map the data
        /// </summary>
        [NotifyParentProperty(true)]
        [DefaultValue(null)]
        public string Function
        {
            get;
            set;
        }

        #endregion

        #region ICustomOptionType Members

        string ICustomOptionType.SerializeValue()
        {
            if (this.Index != -1)
            {
                return this.Index.ToString();
            }
            else if (!string.IsNullOrEmpty(this.Name))
            {
                return this.Name;
            }
            else
            {
                return this.Function;
            }
        }

        bool ICustomOptionType.IncludeQuotes
        {
            get
            {
                if (this.Index != -1)
                {
                    return false;
                }
                else if (!string.IsNullOrEmpty(this.Name))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        #endregion

        #region Methods

        internal bool ShouldSerialize()
        {
            return this.Index != -1
                || !string.IsNullOrEmpty(this.Name)
                || !string.IsNullOrEmpty(this.Function);
        }

        internal void Reset()
        {
            this.Index = -1;
            this.Name = null;
            this.Function = null;
        }

        #endregion
    }
}
