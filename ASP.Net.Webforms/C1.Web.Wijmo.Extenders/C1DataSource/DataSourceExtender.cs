﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.ComponentModel;
using System.Web.UI;

namespace C1.Web.Wijmo.Extenders.C1DataSource
{
	/// <summary>
	/// The extender of the wijdatasource.
	/// </summary>
	[ToolboxItem(false)]
	[WidgetDependencies("wijmo.jquery.wijmo.wijdatasource.js", ResourcesConst.WIJMO_PRO_JS)]
	public class C1DataSourceExtender : WidgetExtenderControlBase
	{
		public C1DataSourceExtender()
		{
			this.Reader = new ArrayReader();
			this.Proxy = new HttpProxy();
		}

		/// <summary>
		/// The data to process using the wijdatasource class.
		/// The value can be the reference of the data or the data itself in client side.
		/// </summary>
		[DefaultValue(null)]
		[WidgetEvent]
		public string Data
		{
			get;
			set;
		}

		/// <summary>
		/// The wijdatasource widget instance name in client side.
		/// </summary>
		public string InstanceName
		{
			get;
			set;
		}

		/// <summary>
		/// The reader to use with wijdatasource. The wijdatasource class will call the
		/// read method of reader to read from rawdata with an array of fields provided.
		/// The field contains a name, mapping and defaultValue properties which define
		/// the rule of the mapping.
		/// If no reader is configured with wijdatasource it will directly return the
		/// raw data.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
		public ArrayReader Reader
		{
			get;
			set;
		}
		/// <summary>
		/// The proxy to use with wijdatasource. The wijdatasource class will call
		/// the proxy object's request method.  
		/// In the proxy object, you can send a request to a remote server to
		/// obtain data with the ajaxs options object provided. 
		/// Then you can use the wijdatasource reader to process the raw data in the call.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
		public HttpProxy Proxy
		{
			get;
			set;
		}
		
		/// <summary>
		/// Function called before loading process starts
		/// The value can be the reference of the function or the function itself in client side.
		/// </summary>
		/// <param name="datasource" type="wijdatasource">
		/// wijdatasource object that raises this event.
		/// </param>
		/// <param name="data" type="Object">
		/// data passed in by load method.
		/// </param>
		[DefaultValue(null)]
		[WidgetEvent]
		public string OnClientLoading
		{
			get;
			set;
		}
		/// <summary>
		/// Function called after loading.
		/// The value can be the reference of the function or the function itself in client side.
		/// </summary>
		/// <param name="datasource" type="wijdatasource">
		/// wijdatasource object that raises this event.
		/// </param>
		/// <param name="data" type="Object">
		/// data passed in by load method.
		/// </param>
		[DefaultValue(null)]
		[WidgetEvent]
		public string OnClientLoaded
		{
			get;
			set;
		}

		#region Methods

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeProxy()
		{
			return this.Proxy.ShouldSerialize();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeReader()
		{
			return this.Reader.ShouldSerialize();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public void ResetProxy()
		{
			this.Proxy.Reset();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public void ResetReader()
		{
			this.Reader.Reset();
		}

		protected override void Render(HtmlTextWriter writer)
		{
			StringBuilder builder = new StringBuilder();
			//builder.Append("$(\"#" + this.ClientID + "\").data(\"wijdatasource\", new wijdatasource(");
			builder.Append("var " + this.InstanceName + " = new wijdatasource(");
			builder.Append(WidgetObjectBuilder.SerializeOption(this));
			builder.Append(");");
			//builder.Append("));");
			ScriptManager.RegisterStartupScript(this, this.GetType(), "c" + this.ClientID, builder.ToString(), true);
		}

		#endregion

	}
}
