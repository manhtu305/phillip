﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Web.UI;
using System.Collections;

[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1DataSource", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1DataSource
{
	/// <summary>
	/// A control that is the container of the datasource extender.
	/// </summary>
	[Designer(typeof(C1DataSourceDesigner))]
	[ToolboxBitmap(typeof(C1DataSource), "DataSource.png")]
	public class C1DataSource : TargetControlBase
	{

		private C1DataSourceExtender _extender;

		protected override WidgetExtenderControlBase Extender
		{
			get
			{
				if (_extender == null)
				{
					_extender = new C1DataSourceExtender();
					_extender.InstanceName = ID;
					_extender.ID = ID + "_DataSourceExtender";
					_extender.TargetControlID = ID;
				}
				return _extender;
			}
		}

		private C1DataSourceExtender DataSourceExtender
		{
			get
			{
				return this.Extender as C1DataSourceExtender;
			}
		}

		/// <summary>
		/// The data to process using the wijdatasource class.
		/// The value can be the reference of the data or the data itself in client side.
		/// </summary>
		[DefaultValue(null)]
		[WidgetEvent]
		public string Data
		{
			get
			{
				return this.DataSourceExtender.Data;
			}
			set
			{
				this.DataSourceExtender.Data = value;
			}
		}

		/// <summary>
		/// The reader to use with wijdatasource. The wijdatasource class will call the
		/// read method of reader to read from rawdata with an array of fields provided.
		/// The field contains a name, mapping and defaultValue properties which define
		/// the rule of the mapping.
		/// If no reader is configured with wijdatasource it will directly return the
		/// raw data.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
		public ArrayReader Reader
		{
			get
			{
				return this.DataSourceExtender.Reader;
			}
			set
			{
				this.DataSourceExtender.Reader = value;
			}
		}
		/// <summary>
		/// The proxy to use with wijdatasource. The wijdatasource class will call
		/// the proxy object's request method.  
		/// In the proxy object, you can send a request to a remote server to
		/// obtain data with the ajaxs options object provided. 
		/// Then you can use the wijdatasource reader to process the raw data in the call.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
		public HttpProxy Proxy
		{
			get
			{
				return this.DataSourceExtender.Proxy;
			}
			set
			{
				this.DataSourceExtender.Proxy = value;
			}
		}
		
		/// <summary>
		/// Function called before loading process starts
		/// The value can be the reference of the function or the function itself in client side.
		/// </summary>
		/// <param name="datasource" type="wijdatasource">
		/// wijdatasource object that raises this event.
		/// </param>
		/// <param name="data" type="Object">
		/// data passed in by load method.
		/// </param>
		[DefaultValue(null)]
		[WidgetEvent]
		public string OnClientLoading
		{
			get
			{
				return this.DataSourceExtender.OnClientLoading;
			}
			set
			{
				this.DataSourceExtender.OnClientLoading = value;
			}
		}
		/// <summary>
		/// Function called after loading.
		/// The value can be the reference of the function or the function itself in client side.
		/// </summary>
		/// <param name="datasource" type="wijdatasource">
		/// wijdatasource object that raises this event.
		/// </param>
		/// <param name="data" type="Object">
		/// data passed in by load method.
		/// </param>
		[DefaultValue(null)]
		[WidgetEvent]
		public string OnClientLoaded
		{
			get
			{
				return this.DataSourceExtender.OnClientLoaded;
			}
			set
			{
				this.DataSourceExtender.OnClientLoaded = value;
			}
		}

		#region Methods

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeProxy()
		{
			return this.DataSourceExtender.ShouldSerializeProxy();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeReader()
		{
			return this.DataSourceExtender.ShouldSerializeReader();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public void ResetProxy()
		{
			this.DataSourceExtender.ResetProxy();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public void ResetReader()
		{
			this.DataSourceExtender.ResetReader();
		}

		protected override void Render(HtmlTextWriter writer)
		{
			this.RenderChildren(writer);
		}

		#endregion

	}
}
