﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Gallery
{
#else
namespace C1.Web.Wijmo.Controls.C1Gallery
{
#endif
	/// <summary>
    /// Specifies the position of the thumbnail image navigation buttons.
	/// </summary>
	public enum ThumbsPosition
	{

		/// <summary>
		/// Inside of frame
		/// </summary>
		After = 0,
		/// <summary>
		/// Outside of frame
		/// </summary>
		Before = 1
	}

	/// <summary>
	/// Specifies the display mode of gallery.
	/// </summary>
	public enum DisplayMode
	{ 
		/// <summary>
		/// Image mode.
		/// </summary>
		Img = 0,

		/// <summary>
		/// Iframe mode.
		/// </summary>
		Iframe = 1,

		/// <summary>
		/// Swf mode.
		/// </summary>
		Swf = 2,

		/// <summary>
		/// Flv mode.
		/// </summary>
		Flv = 3
	}
	
}
