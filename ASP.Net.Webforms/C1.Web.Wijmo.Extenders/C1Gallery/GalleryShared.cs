﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using C1.Web.Wijmo;


#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Gallery", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Gallery
#else
using C1.Web.Wijmo.Controls.Localization;

[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Gallery", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1Gallery
#endif
{
    [WidgetDependencies(
        typeof(WijGallery)
#if !EXTENDER
        , typeof(C1Carousel.C1Carousel)
        , "extensions.c1gallery.js",
		ResourcesConst.C1WRAPPER_PRO
#endif
)]
#if EXTENDER
	public partial class C1GalleryExtender
#else
    public partial class C1Gallery
#endif
    {

        private Animation _transitions;
        private PositionSettings _pagerPosition;
        private PositionSettings _controlPosition;

        #region ** options.

        /// <summary>
        /// When AutoPlay is true the gallery images will be played automatically (similarly to a slideshow).
        /// </summary>
        [C1Description("C1Gallery.AutoPlay", "When AutoPlay is true the gallery images will be played automatically (similarly to a slideshow).")]
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [WidgetOptionName("autoPlay")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool AutoPlay
        {
            get
            {
                return GetPropertyValue("AutoPlay", false);
            }
            set
            {
                SetPropertyValue("AutoPlay", value);
            }
        }

        /// <summary>
        /// Determines if the thumbnail images scroll automatically when images are selected.
        /// </summary>
        [C1Description("C1Gallery.ScrollWithSelection", "Determines if the thumbnail images scroll automatically when images are selected.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool ScrollWithSelection
        {
            get
            {
                return GetPropertyValue("ScrollWithSelection", false);
            }
            set
            {
                SetPropertyValue("ScrollWithSelection", value);
            }
        }

        /// <summary>
        /// Determines if the gallery timer should be shown.
        /// </summary>
        [C1Description("C1Gallery.ShowTimer", "Determines if the gallery timer should be shown.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool ShowTimer
        {
            get
            {
                return GetPropertyValue("ShowTimer", true);
            }
            set
            {
                SetPropertyValue("ShowTimer", value);
            }
        }

        /// <summary>
        /// Determines the time span, in miliseconds, between two pictures being shown in AutoPlay mode.
        /// </summary>
        [C1Description("C1Gallery.Interval", "Determines the time span, in miliseconds, between two pictures being shown in AutoPlay mode.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(5000)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int Interval
        {
            get
            {
                return GetPropertyValue("Interval", 5000);
            }
            set
            {
                SetPropertyValue("Interval", value);
            }
        }

        /// <summary>
        /// Determines if the item caption should be shown.
        /// </summary>
        [C1Description("C1Gallery.ShowCaption", "Determines if the item caption should be shown.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool ShowCaption
        {
            get
            {
                return GetPropertyValue("ShowCaption", true);
            }
            set
            {
                SetPropertyValue("ShowCaption", value);
            }
        }

        /// <summary>
        /// Determines whether the caption of thumbnails should be shown.
        /// </summary>
        [C1Description("C1Gallery.ShowThumbnailCaptions", "Determines whether the caption of thumbnails should be shown.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool ShowThumbnailCaptions
        {
            get
            {
                return GetPropertyValue("ShowThumbnailCaptions", false);
            }
            set
            {
                SetPropertyValue("ShowThumbnailCaptions", value);
            }
        }

        /// <summary>
        /// Determines if custom controls should be shown.
        /// </summary>
        [C1Description("C1Gallery.ShowControls", " Determines if custom controls should be shown.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool ShowControls
        {
            get
            {
                return GetPropertyValue("ShowControls", true);
            }
            set
            {
                SetPropertyValue("ShowControls", value);
            }
        }

        /// <summary>
        /// Determines if the counter is shown.
        /// </summary>
        [C1Description("C1Gallery.ShowCounter", " Determines if the counter is shown.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool ShowCounter
        {
            get
            {
                return GetPropertyValue("ShowCounter", true);
            }
            set
            {
                SetPropertyValue("ShowCounter", value);
            }
        }

        /// <summary>
        /// Determines if the pager should be shown.
        /// </summary>
        [C1Description("C1Gallery.ShowPager", "Determines if the pager should be shown.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool ShowPager
        {
            get
            {
                return GetPropertyValue("ShowPager", false);
            }
            set
            {
                SetPropertyValue("ShowPager", value);
            }
        }

        /// <summary>
        /// Sets the position of the pager in gallery to the 'relativeTo', 'offsetX', 
        /// and 'offsetY' properties. For example, here is the jQuery ui position's 
        /// position:{my:'top left',at:'right bottom',offset:}.
        /// </summary>
        [C1Description("C1Gallery.PagerPosition", "Sets the position of the pager in gallery.")]
        [C1Category("Category.Layout")]
        [WidgetOption]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public PositionSettings PagerPosition
        {
            get
            {
                if (this._pagerPosition == null)
                {
                    this._pagerPosition = new PositionSettings();
                }
                return this._pagerPosition;
            }
            set
            {
                this._pagerPosition = value;
            }
        }

        /// <summary>
        /// Sets the position of the controls in gallery to the 'relativeTo', 'offsetX', 
        /// and 'offsetY' properties. For example, here is the jQuery ui position's 
        /// position:{my:'top left',at:'right bottom',offset:}.
        /// </summary>
        [C1Description("C1Gallery.ControlPosition", "Sets the position of the controls in gallery.")]
        [C1Category("Category.Layout")]
        [WidgetOption]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public PositionSettings ControlPosition
        {
            get
            {
                if (this._controlPosition == null)
                {
                    this._controlPosition = new PositionSettings();
                }
                return this._controlPosition;
            }
            set
            {
                this._controlPosition = value;
            }
        }

        /// <summary>
        /// Determines whether the controls should be shown after created or when users hover over the DOM element.
        /// </summary>
        [C1Description("C1Gallery.ShowControlsOnHover", "Determines whether the controls should be shown after created or when users hover over the DOM element.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool ShowControlsOnHover
        {
            get
            {
                return GetPropertyValue("ShowControlsOnHover", true);
            }
            set
            {
                SetPropertyValue("ShowControlsOnHover", value);
            }
        }

        /// <summary>
        /// Gets or sets the counter's format string.
        /// </summary>
        [C1Description("C1Gallery.Counter", " Gets or sets the counter's format string.")]
        [C1Category("Category.Appearance")]
        [DefaultValue("[i] of [n]")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string Counter
        {
            get
            {
                return GetPropertyValue("Counter",
                    C1Localizer.GetString("C1Multimedia.CounterDefaultValue", "[i] of [n]"));
                //JP "[i] / [n]" ,others "[i] of [n]");
            }
            set
            {
                SetPropertyValue("Counter", value);
            }
        }

        /// <summary>
        /// Gets or sets the content found between the opening and closing tags of the specified HTML server control.
        /// </summary>
        [C1Description("C1Gallery.Control", "Determines the innerHtml of the custom control.")]
        [C1Category("Category.Appearance")]
        [DefaultValue("")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string Control
        {
            get
            {
                return GetPropertyValue("Control", "");
            }
            set
            {
                SetPropertyValue("Control", value);
            }
        }

        /// <summary>
        /// Controls the gallery's orientation. All images are vertical regardless of the 
        /// orientation of the gallery.
        /// </summary>
        [C1Description("C1Gallery.ThumbnailOrientation", "Controls the gallery's orientation")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
        [DefaultValue(Orientation.Horizontal)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public Orientation ThumbnailOrientation
        {
            get
            {
                return GetPropertyValue("ThumbnailOrientation", Orientation.Horizontal);
            }
            set
            {
                SetPropertyValue("ThumbnailOrientation", value);
            }
        }


        /// <summary>
        /// Determines the animation effect settings to be used when the gallery is scrolled.
        /// </summary>
        [C1Description("C1Gallery.Transitions", "Determines the animation effect settings to be used when the gallery is scrolled.")]
        [C1Category("Category.Behavior")]
        [NotifyParentProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public Animation Transitions
        {
            get
            {
                if (this._transitions == null)
                {
                    this._transitions = new Animation();
                }
                return this._transitions;
            }
            set
            {
                this._transitions = value;
            }
        }

        /// <summary>
        /// Controls the thumbnail images' location. Possible values are "before" and " after".
        /// </summary>
        [C1Description("C1Gallery.ThumbnailDirection", "Controls the thumbnail images' location. Possible values are \"before\" and \" after\".")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
        [DefaultValue(Orientation.Horizontal)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public ThumbsPosition ThumbnailDirection
        {
            get
            {
                return GetPropertyValue("ThumbnailDirection", ThumbsPosition.After);
            }
            set
            {
                SetPropertyValue("ThumbnailDirection", value);
            }
        }

        /// <summary>
        /// Determines the number of thumbnail images to display.
        /// </summary>
        [C1Description("C1Gallery.ThumbsDisplay", "Determines the number of thumbnail images to display.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(5)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int ThumbsDisplay
        {
            get
            {
                return GetPropertyValue("ThumbsDisplay", 5);
            }
            set
            {
                SetPropertyValue("ThumbsDisplay", value);
            }
        }

        /// <summary>
        /// Determines the height/width of thumbnail images.
        /// </summary>
        [C1Description("C1Gallery.ThumbsLength", "Determines the height/width of thumbnail images.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(100)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int ThumbsLength
        {
            get
            {
                return GetPropertyValue("ThumbsLength", 100);
            }
            set
            {
                SetPropertyValue("ThumbsLength", value);
            }
        }

        /// <summary>
        /// Determines the gallery's display mode.
        /// </summary>
        [C1Description("C1Gallery.Mode", "Determines the gallery's display mode.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(DisplayMode.Img)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public DisplayMode Mode
        {
            get
            {
                return GetPropertyValue("Mode", DisplayMode.Img);
            }
            set
            {
                SetPropertyValue("Mode", value);
            }
        }

        /// <summary>
        /// Determines if movie controls are enabled in the movie player.
        /// </summary>
        [C1Description("C1Gallery.ShowMovieControls", "Determines if movie controls are enabled in the movie player.")]
        [WidgetOption]
        [DefaultValue(true)]
        public bool ShowMovieControls
        {
            get
            {
                return GetPropertyValue<bool>("ShowMovieControls", true);
            }
            set
            {
                SetPropertyValue<bool>("ShowMovieControls", value);
            }
        }

        /// <summary>
        /// Determines whether to turn on the autoplay option in the movie player.
        /// </summary>
        [C1Description("C1Gallery.AutoPlayMovies", "Determines whether to turn on the autoplay option in the movie player.")]
        [WidgetOption]
        [DefaultValue(true)]
        public bool AutoPlayMovies
        {
            get
            {
                return GetPropertyValue<bool>("AutoPlayMovies", true);
            }
            set
            {
                SetPropertyValue<bool>("AutoPlayMovies", value);
            }
        }

        /// <summary>
        /// Gets or sets the relative path and name of the flash video player.
        /// </summary>
        [C1Description("C1Gallery.FlvPlayer", "Gets or sets the relative path and name of the flash video player.")]
        [WidgetOption]
        [DefaultValue("player/player.swf")]
        public string FlvPlayer
        {
            get
            {
                return GetPropertyValue<string>("FlvPlayer", "player\\player.swf");
            }
            set
            {
                SetPropertyValue<string>("FlvPlayer", value);
            }
        }

        /// <summary>
        /// Gets or sets the relative path and name of the flash installation guide.
        /// </summary>
        [C1Description("C1Gallery.FlashInstall", "Gets or sets the relative path and name of the flash installation guide.")]
        [WidgetOption]
        [DefaultValue("player/expressInstall.swf")]
        public string FlashInstall
        {
            get
            {
                return GetPropertyValue<string>("FlashInstall", "player\\expressInstall.swf");
            }
            set
            {
                SetPropertyValue<string>("FlashInstall", value);
            }
        }

        #endregion

        #region ** client side events

        /// <summary>
        /// The name of the function which will be called before transition to another image.
        /// </summary>
        [C1Description("C1Gallery.OnClientBeforeTransition", "The name of the function which will be called when before transition to another image")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("ev, index")]
        [WidgetOptionName("beforeTransition")]
        [DefaultValue("")]
        public string OnClientBeforeTransition
        {
            get
            {
                return GetPropertyValue("OnClientBeforeTransition", "");
            }
            set
            {
                SetPropertyValue("OnClientBeforeTransition", value);
            }
        }

        /// <summary>
        /// The name of the function which will be called after transition to another image.
        /// </summary>
        [C1Description("C1Gallery.OnClientAfterTransition", "The name of the function which will be called after transition to another image.")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("ev, index")]
        [WidgetOptionName("afterTransition")]
        [DefaultValue("")]
        public string OnClientAfterTransition
        {
            get
            {
                return GetPropertyValue("OnClientAfterTransition", "");
            }
            set
            {
                SetPropertyValue("OnClientAfterTransition", value);
            }
        }

        /// <summary>
        /// The name of the function which will be called after created the DOM element.
        /// </summary>
        [C1Description("C1Gallery.OnClientLoadCallback", "The name of the function which will be called after created the DOM element.")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("ev, index")]
        [WidgetOptionName("loadCallback")]
        [DefaultValue("")]
        public string OnClientLoadCallback
        {
            get
            {
                return GetPropertyValue("OnClientLoadCallback", "");
            }
            set
            {
                SetPropertyValue("OnClientLoadCallback", value);
            }
        }
        #endregion

        #region ** methods for serialization


        #endregion end of ** methods for serialization.
    }
}
