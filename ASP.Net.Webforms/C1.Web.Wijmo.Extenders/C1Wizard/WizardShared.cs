﻿using System.ComponentModel;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;


#if EXTENDER
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Wizard", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Wizard
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Wizard", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1Wizard
#endif
{
#if EXTENDER 
	using C1.Web.Wijmo.Extenders.Localization;
    using C1.Web.Wijmo.Extenders.Converters;
#else
    using C1.Web.Wijmo.Controls.Localization;
    using C1.Web.Wijmo.Controls.Converters;
#endif

	using C1.Web.Wijmo;

	[WidgetDependencies(
		typeof(WijWizard)
#if !EXTENDER
		, "extensions.c1wizard.js",
		ResourcesConst.C1WRAPPER_PRO
#endif		
	)]
#if EXTENDER
	public partial class C1WizardExtender
#else
	public partial class C1Wizard
#endif
	{
		#region ** fields

		private BlindOption _showOption = null;
		private BlindOption _hideOption = null;
		private AjaxSettings _ajaxOptions = null;

		#endregion

		#region ** options

		///	<summary>
		///	Gets or sets the active index.
		///	</summary>
        [C1Description("C1Wizard.ActiveIndex")]
		[WidgetOption]
		[DefaultValue(0)]
		public int ActiveIndex
		{
			get
			{
				return GetPropertyValue<int>("ActiveIndex", 0);
			}
			set
			{
                if (this.ActiveIndex != value)
                {
                    SetPropertyValue<int>("ActiveIndex", value);
#if !EXTENDER
                    OnActiveIndexChanged(new EventArgs());
#endif
                }
			}
		}


		/// <summary>
		/// Determines the type of navigation buttons used with the wijwizard. 
		/// </summary>
        [C1Description("C1Wizard.NavButtons")]
		[WidgetOption]
		[DefaultValue(NavButtonType.Auto)]
		public NavButtonType NavButtons
		{
			get
			{
				return GetPropertyValue<NavButtonType>("NavButtons", NavButtonType.Auto);
			}
			set
			{
				SetPropertyValue<NavButtonType>("NavButtons", value);
			}
		}

		/// <summary>
		/// Determines whether panels are automatically displayed in order.
		/// </summary>
        [C1Description("C1Wizard.AutoPlay")]
		[WidgetOption]
		[DefaultValue(false)]
		public bool AutoPlay
		{
			get
			{
				return GetPropertyValue<bool>("AutoPlay", false);
			}
			set
			{
				SetPropertyValue<bool>("AutoPlay", value);
			}
		}

		/// <summary>
		/// Determines the time span between panels in autoplay mode. 
		/// </summary>
        [C1Description("C1Wizard.Delay")]
		[WidgetOption]
		[DefaultValue(3000)]
		public int Delay
		{
			get
			{
				return GetPropertyValue<int>("Delay", 300);
			}
			set
			{
				SetPropertyValue<int>("Delay", value);
			}
		}

		/// <summary>
		/// Determines whether start from the first panel when reaching the end in autoplay mode.
		/// </summary>
        [C1Description("C1Wizard.Loop")]
		[WidgetOption]
		[DefaultValue(false)]
		public bool Loop
		{
			get
			{
				return GetPropertyValue<bool>("Loop", false);
			}
			set
			{
				SetPropertyValue<bool>("Loop", value);
			}
		}

		/// <summary>
		/// The animation option for hiding the panel content.
		/// </summary>
        [C1Description("C1Wizard.ShowOption")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption()]
		public BlindOption ShowOption
		{
			get
			{
				if (_showOption == null) _showOption = new BlindOption();

				return GetPropertyValue<BlindOption>("ShowOption", _showOption);
			}
			set
			{
				SetPropertyValue<BlindOption>("ShowOption", value);
			}
		}


		/// <summary>
		/// The animation option for showing the panel content. 
		/// </summary>
        [C1Description("C1Wizard.HideOption")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption()]
		public BlindOption HideOption
		{
			get
			{
				if (_hideOption == null) _hideOption = new BlindOption();

				return GetPropertyValue<BlindOption>("HideOption", _hideOption);
			}
			set
			{
				SetPropertyValue<BlindOption>("HideOption", value);
			}
		}

		/// <summary>
		/// Additional Ajax options to consider when loading panel content.
		/// </summary>
        [C1Description("C1Wizard.AjaxOptions")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption()]
		public AjaxSettings AjaxOptions
		{
			get
			{
				if (_ajaxOptions == null) _ajaxOptions = new AjaxSettings();

				return GetPropertyValue<AjaxSettings>("AjaxOptions", _ajaxOptions);
			}
			set
			{
				SetPropertyValue<AjaxSettings>("AjaxOptions", value);
			}
		}

		/// <summary>
		/// Whether or not to cache remote wijwizard content.
		/// </summary>
        [C1Description("C1Wizard.Cache")]
		[WidgetOption]
		[DefaultValue(false)]
		public bool Cache
		{
			get
			{
				return GetPropertyValue<bool>("Cache", false);
			}
			set
			{
				SetPropertyValue<bool>("Cache", value);
			}
		}

		/// <summary>
		/// HTML template for step header when a new panel is added with the add method or
		///	when creating a panel for a remote panel on the fly.
		/// </summary>
        [C1Description("C1Wizard.StepHeaderTemplate")]
#if EXTENDER
		[WidgetOption]
#endif
		[DefaultValue(@"<h1>#{title}</h1>#{desc}")]
		public string StepHeaderTemplate
		{
			get
			{
				return GetPropertyValue<string>("StepHeaderTemplate", @"<h1>#{title}</h1>#{desc}");
			}
			set
			{
				if (String.IsNullOrEmpty(value)) return;
				SetPropertyValue<string>("StepHeaderTemplate", value);
			}
		}

		/// <summary>
		/// HTML template from which a new panel is created by adding a panel with the add method or
		///	when creating a panel for a remote panel on the fly.
		/// </summary>
        [C1Description("C1Wizard.PanelTemplate")]
#if EXTENDER
		[WidgetOption]
#endif
		[DefaultValue(@"<div></div>")]
		public string PanelTemplate
		{
			get
			{
				return GetPropertyValue<string>("PanelTemplate", @"<div></div>");
			}
			set
			{
				if (String.IsNullOrEmpty(value)) return;
				SetPropertyValue<string>("PanelTemplate", value);
			}
		}

		///	<summary>
		///	The HTML content of this string is shown in a panel while remote content is loading. 
		///	Pass in empty string to deactivate that behavior. 
		///	</summary>
        [C1Description("C1Wizard.Spinner")]
#if EXTENDER
		[WidgetOption]
#endif
		[DefaultValue(@"<em>Loading&#8230;</em>")]
		public string Spinner
		{
			get
			{
				return GetPropertyValue<string>("Spinner", @"<em>Loading&#8230;</em>");
			}
			set
			{
				if (String.IsNullOrEmpty(value)) return;
				SetPropertyValue<string>("Spinner", value);
			}
		}

		/// <summary>
		/// A value that indicates the text of back button. 
		/// </summary>
		[C1Description("C1Wizard.BackBtnTextDescription")]
		[WidgetOption]
		[DefaultValue("back")]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string BackBtnText
		{
			get
			{
				return GetPropertyValue<string>("BackBtnText", C1Localizer.GetString("C1Wizard.BackBtnText"));
			}
			set
			{
				SetPropertyValue<string>("BackBtnText", value);
			}
		}

		/// <summary>
		/// A value that indicates the text of next button. 
		/// </summary>
		[C1Description("C1Wizard.NextBtnTextDescription")]
		[WidgetOption]
		[DefaultValue("next")]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string NextBtnText
		{
			get
			{
				return GetPropertyValue<string>("NextBtnText", C1Localizer.GetString("C1Wizard.NextBtnText"));
			}
			set
			{
				SetPropertyValue<string>("NextBtnText", value);
			}
		}


		#endregion

		#region ** client events

		/// <summary>
		/// This event is triggered after the content of a remote panel has been loaded.
		/// </summary>
        [C1Description("C1Wizard.OnClientLoad")]
        [C1Category("Category.ClientSideEvents")]
		[WidgetOption]
		[WidgetEvent]
		[DefaultValue("")]
		public string OnClientLoad
		{
			get
			{
				return GetPropertyValue("OnClientLoad", "");
			}
			set
			{
				SetPropertyValue("OnClientLoad", value);
			}
		}

		/// <summary>
		/// This event is triggered when a panel is added.
		/// </summary>
        [C1Description("C1Wizard.OnClientAdd")]
        [C1Category("Category.ClientSideEvents")]
		[WidgetOption]
		[WidgetEvent]
		[DefaultValue("")]
		public string OnClientAdd
		{
			get
			{
				return GetPropertyValue("OnClientAdd", "");
			}
			set
			{
				SetPropertyValue("OnClientAdd", value);
			}
		}

		/// <summary>
		/// This event is triggered when a panel is removed.
		/// </summary>
        [C1Description("C1Wizard.OnClientRemove")]
        [C1Category("Category.ClientSideEvents")]
		[WidgetOption]
		[WidgetEvent]
		[DefaultValue("")]
		public string OnClientRemove
		{
			get
			{
				return GetPropertyValue("OnClientRemove", "");
			}
			set
			{
				SetPropertyValue("OnClientRemove", value);
			}
		}


		/// <summary>
		/// This event is triggered when a panel is shown.
		/// </summary>
        [C1Description("C1Wizard.OnClientShow")]
        [C1Category("Category.ClientSideEvents")]
		[WidgetOption]
		[WidgetEvent]
		[DefaultValue("")]
		public string OnClientShow
		{
			get
			{
				return GetPropertyValue("OnClientShow", "");
			}
			set
			{
				SetPropertyValue("OnClientShow", value);
			}
		}

		///	<summary>
		///	This event is triggered before moving to next panel.
		///	</summary>
        [C1Description("C1Wizard.OnClientValidating")]
        [C1Category("Category.ClientSideEvents")]
		[WidgetOption]
		[WidgetEvent]
		[DefaultValue("")]
		public string OnClientValidating
		{
			get
			{
				return GetPropertyValue("OnClientValidating", "");
			}
			set
			{
				SetPropertyValue("OnClientValidating", value);
			}
		}

		#endregion
	}
}
