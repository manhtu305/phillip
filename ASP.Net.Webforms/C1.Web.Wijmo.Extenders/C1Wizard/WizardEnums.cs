﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Wizard
#else
namespace C1.Web.Wijmo.Controls.C1Wizard
#endif
{
    /// <summary>
    /// Represents the type of navigation button used with the wijwizard. 
    /// </summary>
    public enum NavButtonType
    {
        Auto = 0,
        Common = 1,
        Edge = 2,
        None = 3
    }
}
