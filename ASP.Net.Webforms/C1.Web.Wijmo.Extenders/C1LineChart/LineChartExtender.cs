﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Web.UI;

namespace C1.Web.Wijmo.Extenders.C1Chart
{
	/// <summary>
	/// Extender for line chart widget.
	/// </summary>
	[ToolboxBitmap(typeof(C1LineChartExtender), "LineChart.png")]
	[ToolboxItem(true)]
	[LicenseProviderAttribute()]
	public partial class C1LineChartExtender : C1ChartCoreExtender<LineChartSeries, LineChartAnimation>
	{
		private bool _productLicensed = false;

		/// <summary>
		/// Initializes a new instance of the C1LineChartExtender class.
		/// </summary>
		public C1LineChartExtender()
			: base()
		{
			VerifyLicense();
			this.InitChart();
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1LineChartExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}
	}
}
