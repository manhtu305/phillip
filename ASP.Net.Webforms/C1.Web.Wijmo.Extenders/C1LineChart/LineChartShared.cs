﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
    [WidgetDependencies(
        typeof(WijLineChart)
#if !EXTENDER
, "extensions.c1linechart.js",
ResourcesConst.C1WRAPPER_PRO
#endif
)]
#if EXTENDER
	public partial class C1LineChartExtender : C1ChartCoreExtender<LineChartSeries, LineChartAnimation>
#else    
	public partial class C1LineChart : C1ChartCore<LineChartSeries, LineChartAnimation, C1ChartBinding>
#endif
	{

		private void InitChart()
		{
			this.Legend.Size.Width = 30;
			this.Legend.Size.Height = 3;
			this.Animation.Duration = 2000;
			this.SeriesTransition = new ChartAnimation();
			this.SeriesTransition.Duration = 2000;
			//this.SeriesTransition.Easing = ">";
			this.SeriesTransition.Easing = ChartEasing.EaseInCubic;
			Hole = new ChartHole();
		}

		// added in 2013-10-31, fixing the OADateTime issue, move this method from C1LineChart to this file.
		/// <summary>
		/// Gets the series data of the specified series.
		/// </summary>
		/// <param name="series">
		/// The specified series.
		/// </param>
		/// <returns>
		/// Returns the series data which retrieved from the specified series.
		/// </returns>
		protected override ChartSeriesData GetSeriesData(LineChartSeries series)
		{
            if (series.IsTrendline)
                return base.GetSeriesData(series);

			return series.Data;
		}

		/// <summary>
		/// A value that indicates the type of the chart.
		/// </summary>
		[C1Description("C1Chart.LineChartType")]
		[WidgetOption]
		[DefaultValue(LineChartType.Line)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public LineChartType Type
		{
			get
			{
				return GetPropertyValue<LineChartType>("Type", LineChartType.Line);
			}
			set
			{
				SetPropertyValue<LineChartType>("Type", value);
			}
		}

		/// <summary>
		/// A value that indicates whether to show the effect and duration of the animation when reloading the data.
		/// </summary>
		[C1Description("C1Chart.LineChart.SeriesTransition")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public ChartAnimation SeriesTransition { get; set; }

//        /// <summary>
//        /// A value that indicates whether to zoom in on the line and marker on hover.
//        /// </summary>
//        [Description("A value that indicates whether to zoom in on the line and marker on hover.")]
//        [WidgetOption]
//        [DefaultValue(true)]
//#if !EXTENDER
//        [Layout(LayoutType.Behavior)]
//#endif
//        public bool ZoomOnHover
//        {
//            get
//            {
//                return this.GetPropertyValue<bool>("ZoomOnHover", true);
//            }
//            set
//            {
//                this.SetPropertyValue<bool>("ZoomOnHover", value);
//            }
//        }

		/// <summary>
		/// Gets or sets the data hole value.
		/// </summary>
		[C1Description("C1Chart.LineChartHole")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public ChartHole Hole { get; set; }

		#region Serialize

		/// <summary>
		/// Determine whether the SeriesTransition property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns false if SeriesTransition has default values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeSeriesTransition()
		{
#if !EXTENDER
			if (this.IsDesignMode)
			{
				return true;
			}
#endif
			if (!this.SeriesTransition.Enabled)
				return true;
			if (this.SeriesTransition.Duration != 2000)
				return true;
			//if (this.SeriesTransition.Easing != ">")
			if (this.SeriesTransition.Easing != ChartEasing.EaseInCubic)
				return true;
			return false;
		}

		#endregion
	}


	/// <summary>
	/// Represents the ChartHole class. ChartHole is used as placeholders for data points 
	/// that indicate data is normally present but not in this case.
	/// </summary>
	public class ChartHole : Settings, ICustomOptionType, IJsonEmptiable
	{
		/// <summary>
		/// Creates a new instance of the ChartHole class.
		/// </summary>
		public ChartHole()
		{
		}

		/// <summary>
		/// A value that indicates the numeric hole value.
		/// </summary>
		[NotifyParentProperty(true)]
		[C1Description("C1Chart.ChartHole.NumericHole")]
		[DefaultValue(null)]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public double? NumericHole
		{
			get
			{
				return GetPropertyValue<double?>("NumericHole", null);
			}
			set
			{
				SetPropertyValue<double?>("NumericHole", value);
			}
		}

		/// <summary>
		/// A value that indicates the date hole value.
		/// </summary>
		[NotifyParentProperty(true)]
		[C1Description("C1Chart.ChartHole.DateHole")]
		[DefaultValue(null)]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public DateTime? DateHole
		{
			get
			{
				return GetPropertyValue<DateTime?>("DateHole", null);
			}
			set
			{
				SetPropertyValue<DateTime?>("DateHole", value);
			}
		}

		#region Serialize
		internal bool ShouldSerialize()
		{
			if (NumericHole.HasValue)
				return true;
			if (DateHole != null)
				return true;
			return false;
		}

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		#endregion

		#region ICustomOptionType Members

#if !EXTENDER
		void ICustomOptionType.DeSerializeValue(object state)
		{
			if (state is DateTime)
			{
				DateHole = state as DateTime?;
			}
			else if (state is double)
			{
				NumericHole = state as double?;
			}
			else if (state is int)
			{
				NumericHole = (double?)(int)state;
			}
		}
#endif

		string ICustomOptionType.SerializeValue()
		{
			if (NumericHole.HasValue)
			{
				return NumericHole.Value.ToString();
			}
			else if (DateHole != null)
			{
				//return JsonHelper.ObjectToString(DateHole, null);
				return JsonHelper.WidgetToString(DateHole, null);
			}
			return "";
		}

		bool ICustomOptionType.IncludeQuotes
		{
			get { return false; }
		}

		#endregion
	}
}
