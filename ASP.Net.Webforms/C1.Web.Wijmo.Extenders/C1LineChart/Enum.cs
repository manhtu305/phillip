﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{

	/// <summary>
	/// Use the members of this enumeration to set the value of the LineChart.Type property.
	/// </summary>
	public enum LineChartType
	{
		/// <summary>
		/// Draws a line chart.
		/// </summary>
		Line = 0,
		/// <summary>
		/// Draws an area chart.
		/// </summary>
		Area = 1
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the LineChart.FitType property.
	/// </summary>
	public enum LineChartFitType
	{
		/// <summary>
		/// Straight lines connect data points. 
		/// </summary>
		Line = 0,
		/// <summary>
		/// Line passes smoothly through each data point;
		/// there are no sharp corners and no abrupt changes in the tightness of the curve.
		/// </summary>
		Spline = 1,
		/// <summary>
		/// Line does not pass through all data points, but the points act as magnets,
		/// pulling the curve in certain directions and influencing the way the curve bends.
		/// </summary>
		Bezier = 2
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the LineChart.Direction property.
	/// </summary>
	public enum LineChartDirection
	{
		/// <summary>
		/// Draws the line horizontally for the LineChart.
		/// </summary>
		Horizontal = 0,
		/// <summary>
		/// Draws the line vertically for the LIneChart.
		/// </summary>
		Vertical = 1
	}
}
