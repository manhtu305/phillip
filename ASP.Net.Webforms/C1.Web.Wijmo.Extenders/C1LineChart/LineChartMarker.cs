﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.Design;
using System.Drawing.Design;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	#region LineChartMarker

	/// <summary>
	/// Represents the LineChartMarker class which contains all of the settings for the line chart marker.
	/// </summary>
	public class LineChartMarker : Settings, IJsonEmptiable
	{
		List<LineChartMarkerSymbol> _symbols = null;

		/// <summary>
		/// Initializes a new instance of the LineChartMarker class.
		/// </summary>
		public LineChartMarker()
		{
		}

		#region Properties

		/// <summary>
		/// A value that indicates the visibility of the marker.
		/// </summary>
		[C1Description("C1Chart.LineChartMarker.Visible")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(false)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Visible
		{
			get
			{
				return this.GetPropertyValue<bool>("Visible", false);
			}
			set
			{
				this.SetPropertyValue<bool>("Visible", value);
			}
		}

		/// <summary>
		/// A value that indicates the type of the marker.
		/// </summary>
		[C1Description("C1Chart.LineChartMarker.Type")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(MarkerType.Circle)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public MarkerType Type
		{
			get
			{
				return this.GetPropertyValue<MarkerType>("Type", MarkerType.Circle);
			}
			set
			{
				this.SetPropertyValue<MarkerType>("Type", value);
			}
		}

		/// <summary>
		/// A value that indicates the symbol of the marker.
		/// </summary>
		[C1Description("C1Chart.LineChartMarker.Symbol")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public List<LineChartMarkerSymbol> Symbol
		{
			get
			{
				if (_symbols == null)
				{
					_symbols = new List<LineChartMarkerSymbol>();
				}
				return _symbols;
			}
		}

		#endregion

		#region Serialize

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		/// <summary>
		/// Determine whether the Symbol property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeSymbol()
		{
			return Symbol.Count > 0;
		}

		internal bool ShouldSerialize()
		{
			if (Visible)
				return true;
			if (Type != MarkerType.Circle)
				return true;
			if (Symbol.Count > 0)
				return true;
			return false;
		}
		#endregion
	}
	#endregion

	#region LineChartMarkerSymbol

	/// <summary>
	/// Represents the LineChartMarkerSymbol class which contains all of the settings for the symbol of marker.
	/// </summary>
	public class LineChartMarkerSymbol: Settings, IJsonEmptiable
	{

		/// <summary>
		/// Initializes a new instance of the LineChartMarkerSymbol class.
		/// </summary>
		public LineChartMarkerSymbol()
		{
			this.Index = -1;
			this.Width = -1;
			this.Height = -1;
			this.Url = "";
		}

		#region Properties

		/// <summary>
		/// A value that indicates the index of the marker to show the symbol.
		/// </summary>
		[C1Description("C1Chart.LineChartMarkerSymbol.Index")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(-1)]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public int Index
		{ 
			get
			{
				return this.GetPropertyValue<int>("Index", -1);
			}
			set
			{
				this.SetPropertyValue<int>("Index", value);
			}
		}

		/// <summary>
		/// A value that indicates the url of the symbol.
		/// </summary>
		[C1Description("C1Chart.LineChartMarkerSymbol.Url")]
		[Editor(typeof(ImageUrlEditor), typeof(UITypeEditor))]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string Url
		{ 
			get
			{
				return this.GetPropertyValue<string>("Url", "");
			}
			set
			{
				this.SetPropertyValue<string>("Url", value);
			}
		}

		/// <summary>
		/// A value that indicates the width of the symbol.
		/// </summary>
		[C1Description("C1Chart.LineChartMarkerSymbol.Width")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(-1)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Sizes)]
#endif
		public int Width
		{ 
			get
			{
				return this.GetPropertyValue<int>("Width", -1);
			}
			set
			{
				this.SetPropertyValue<int>("Width", value);
			}
		}

		/// <summary>
		/// A value that indicates the height of the symbol.
		/// </summary>
		[C1Description("C1Chart.LineChartMarkerSymbol.Height")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(-1)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Sizes)]
#endif
		public int Height
		{ 
			get
			{
				return this.GetPropertyValue<int>("Height", -1);
			}
			set
			{
				this.SetPropertyValue<int>("Height", value);
			}
		}

		#endregion

		#region Serialize

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal bool ShouldSerialize()
		{
			if (Index != -1)
				return true;
			if (!String.IsNullOrEmpty(Url))
				return true;
			if (Width != -1)
				return true;
			if (Height != -1)
				return true;
			return false;
		}
		#endregion
	}
	#endregion
}
