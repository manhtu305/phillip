﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	/// <summary>
	/// Represents the LineChartAnimation class which contains all of the settings for the animation.
	/// </summary>
	public class LineChartAnimation : ChartAnimation
	{

		/// <summary>
		/// Initializes a new instance of the LineChartAnimation class.
		/// </summary>
		public LineChartAnimation()
			: base()
		{
		}

		/// <summary>
		/// Gets or sets the direction of the animated line in the Line Chart.
		/// </summary>
		[C1Description("C1Chart.LineChartAnimation.Direction")]
		[NotifyParentProperty(true)]
		[DefaultValue(LineChartDirection.Horizontal)]
		[WidgetOption]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public LineChartDirection Direction
		{
			get
			{
				return this.GetPropertyValue<LineChartDirection>("Direction", LineChartDirection.Horizontal);
			}
			set
			{
				this.SetPropertyValue<LineChartDirection>("Direction", value);
			}
		}

		/// <summary>
		/// Serializes the current animation object to a string value.
		/// </summary>
		/// <returns>
		/// Returns the string value that serialized from the current animation object.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override string GetOptionValue()
		{
			string optionValue = base.GetOptionValue();
			if (this.Direction != LineChartDirection.Horizontal)
			{
				if (!String.IsNullOrEmpty(optionValue))
				{
					optionValue += ",";
				}
				optionValue += " direction: '" + this.Direction + "'";
			}
			return optionValue;
		}

		#region Serialize
		/// <summary>
		/// Returns a boolean value that indicates whether the bar chart series
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a boolean value to determine whether this series should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		internal protected override bool ShouldSerialize()
		{
			if (Direction != LineChartDirection.Horizontal)
				return true;
			return base.ShouldSerialize();
		}
		#endregion
	}
}
