﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else

using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	/// <summary>
	/// Represents the LineChartSeries class which contains all of the settings for the chart series.
	/// </summary>
	public class LineChartSeries : ChartSeries
	{

		/// <summary>
		/// Initializes a new instance of the LineChartSeries class.
		/// </summary>
		public LineChartSeries()
			:base()
		{
			this.Markers = new LineChartMarker();
			this.MarkerStyle = new ChartStyle();
			this.Data = new ChartSeriesData();
		}

		/// <summary>
		/// A value that indicates the display type of the line.
		/// </summary>
		[C1Description("C1Chart.LineChartSeries.Display")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(ChartSeriesDisplay.Show)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public ChartSeriesDisplay Display
		{
			get
			{
				return this.GetPropertyValue<ChartSeriesDisplay>("Display", ChartSeriesDisplay.Show);
			}
			set
			{
				this.SetPropertyValue<ChartSeriesDisplay>("Display", value);
			}
		}

		/// <summary>
		/// A value that indicates the data of the chart series.
		/// </summary>
		[C1Description("C1Chart.ChartSeries.Data")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public ChartSeriesData Data { get; set; }

		/// <summary>
		/// A value that indicates the fit type of the line.
		/// </summary>
		[C1Description("C1Chart.LineChartSeries.FitType")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(LineChartFitType.Line)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public LineChartFitType FitType
		{
			get
			{
				return this.GetPropertyValue<LineChartFitType>("FitType", LineChartFitType.Line);
			}
			set
			{
				this.SetPropertyValue<LineChartFitType>("FitType", value);
			}
		}

		/// <summary>
		/// A value that indicates the markers of the line.
		/// </summary>
		[C1Description("C1Chart.LineChartSeries.Markers")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public LineChartMarker Markers { get; set; }

		/// <summary>
		/// A value that indicates the style of the markers.
		/// </summary>
		[C1Description("C1Chart.LineChartSeries.MarkerStyle")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle MarkerStyle { get; set; }

		#region Serialize
		/// <summary>
		/// Returns a boolean value informing the serializer whether the chart series has changed from its default value.
		/// </summary>
		/// <returns>
		/// Returns a boolean value to indicate whether the series should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal override bool ShouldSerialize()
		{
			var shouldSerialize = base.ShouldSerialize();
			if (shouldSerialize)
				return true;
			if (FitType != LineChartFitType.Line)
				return true;
			if (Markers.ShouldSerialize())
				return true;
			if (MarkerStyle.ShouldSerialize())
				return true;
			if (Data.ShouldSerialize())
				return true;
			return false;
		}
		#endregion

	} 
}
