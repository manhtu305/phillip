﻿using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using C1.Web.Wijmo;



namespace C1.Web.Wijmo.Extenders.C1Input
{
	using C1.Web.Wijmo.Extenders.C1ComboBox;

	/// <summary>
	/// Base class for C1InputMaskExtender, C1InputDateExtender, C1InputNumberExtender, C1InputCurrencyExtender and C1InputPercentExtender.
	/// </summary>
	[TargetControlType(typeof(TextBox))]
	public abstract partial class C1InputExtenderBase : WidgetExtenderControlBase
	{


		internal override bool IsWijMobileExtender
		{
			get
			{
				return false;
			}
		}
	}
}

