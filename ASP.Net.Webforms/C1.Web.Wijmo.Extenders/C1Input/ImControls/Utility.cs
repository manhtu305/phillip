﻿using System;
using System.Drawing;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Web.UI.WebControls;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{
    internal static class Utility
    {
        public static string MaskValChar = "\ufeff";
        internal static string Hold = "g-C2";//(char)0XFF;
        internal static string Sep1 = Utility.Hold + ";";
        internal static string Sep2 = Utility.Hold + ",";

        /// <summary>
        /// Gets the length of the text, especially for processing the JIS0213.
        /// </summary>
        /// <param name="text">Text</param>
        /// <returns>Number of characters in the text</returns>
        internal static int GetStringLength(string text)
        {
            StringInfo siText = new StringInfo(text);
            return siText.LengthInTextElements;
        }

        /// <summary>
        /// Retrieves a substring from this instance. 
        /// The substring starts at a specified character position and has a specified length.
        /// </summary>
        /// <param name="text">Text</param>
        /// <param name="startIndex">Index of the start of the substring</param>
        /// <param name="length">Number of characters in the substring</param>
        /// <returns>A System.String equivalent to the substring of length length that begins
        /// at startIndex in this instance, or System.String.Empty if startIndex is equal
        /// to the length of this instance and length is zero.        /// 
        /// </returns>
        internal static string Substring(string text, int startIndex, int length)
        {
            if (text.Length == 0)
            {
                return text;
            }
            StringInfo siText = new StringInfo(text);
            if (startIndex + length > siText.LengthInTextElements)
            {
                return siText.SubstringByTextElements(startIndex);
            }
            return siText.SubstringByTextElements(startIndex, length);
        }

        /// <summary>
        /// Retrieves a substring from this instance. The substring starts at a specified character position.
        /// </summary>
        /// <param name="text">Text</param>
        /// <param name="startIndex">Starting character position of a substring in this instance</param>
        /// <returns>
        /// A System.String object equivalent to the substring that begins at startIndex
        /// in this instance, or System.String.Empty if startIndex is equal to the length of this instance.
        /// </returns>
        internal static string Substring(string text, int startIndex)
        {
            StringInfo siText = new StringInfo(text);
            if (startIndex >= siText.LengthInTextElements)
            {
                return "";
            }

            return siText.SubstringByTextElements(startIndex);
        }

        /// <summary>
        /// Reports the index of the first occurrence of the specified Unicode character in this string. 
        /// The search starts at a specified character position.
        /// </summary>
        /// <param name="text">Text</param>
        /// <param name="value">Unicode character to seek</param>
        /// <param name="startIndex">Search starting position</param>
        /// <returns>
        /// The index position of value if that character is found, or -1 if it is not.
        /// </returns>
        internal static int IndexOf(string text, char value, int startIndex)
        {
            int iOldIndex = text.IndexOf(value, startIndex);
            if (iOldIndex == -1)
            {
                return -1;
            }
            string strLeftText = text.Substring(0, iOldIndex);
            StringInfo siLeftText = new StringInfo(strLeftText);
            iOldIndex = iOldIndex - (strLeftText.Length - siLeftText.LengthInTextElements);
            return iOldIndex;
        }

        /// <summary>
        /// Reports the index of the first occurrence of the specified System.String in this instance.
        /// </summary>
        /// <param name="text">Text</param>
        /// <param name="value">System.String to seek</param>
        /// <returns>
        /// The index position of value if that string is found, or -1 if it is not.
        /// If value is System.String.Empty, the return value is 0.
        /// </returns>
        internal static int IndexOf(string text, string value)
        {
            int iOldIndex = text.IndexOf(value);
            if (iOldIndex == -1)
            {
                return -1;
            }
            string strLeftText = text.Substring(0, iOldIndex);
            StringInfo siLeftText = new StringInfo(strLeftText);
            iOldIndex = iOldIndex - (strLeftText.Length - siLeftText.LengthInTextElements);
            return iOldIndex;
        }

        /// <summary>
        /// Reports the index of the first occurrence of the specified Unicode character in this string.
        /// </summary>
        /// <param name="text">Text</param>
        /// <param name="value">Unicode character to seek</param>
        /// <returns>The index position of value if that character is found, or -1 if it is not.</returns>
        internal static int IndexOf(string text, char value)
        {
            int iOldIndex = text.IndexOf(value);
            if (iOldIndex == -1)
            {
                return -1;
            }
            string strLeftText = text.Substring(0, iOldIndex);
            StringInfo siLeftText = new StringInfo(strLeftText);
            iOldIndex = iOldIndex - (strLeftText.Length - siLeftText.LengthInTextElements);
            return iOldIndex;
        }

        /// <summary>
        /// Reports the index position of the last occurrence of a specified System.String within this instance.
        /// </summary>
        /// <param name="text">Text</param>
        /// <param name="value">System.String to seek</param>
        /// <returns>
        /// The index position of value if that string is found, or -1 if it is not.
        /// If value is System.String.Empty, the return value is the last index position in value.
        /// </returns>
        internal static int LastIndexOf(string text, string value)
        {
            int iOldIndex = text.LastIndexOf(value);
            if (iOldIndex == -1)
            {
                return -1;
            }
            string strLeftText = text.Substring(0, iOldIndex);
            StringInfo siLeftText = new StringInfo(strLeftText);
            iOldIndex = iOldIndex - (strLeftText.Length - siLeftText.LengthInTextElements);
            return iOldIndex;
        }

        public static int Unit2Pixel(Unit unit)
        {
            double value = unit.Value;

            switch (unit.Type)
            {
                case UnitType.Pica:
                    value = (12 * value) / 72;
                    break;
                case UnitType.Point:
                    value /= 72;
                    break;
                case UnitType.Cm:
                    value *= 0.3937;
                    break;
                case UnitType.Mm:
                    value *= 0.03937;
                    break;
                case UnitType.Inch:
                    value *= 1;
                    break;
                case UnitType.Pixel:
                    return Convert.ToInt32(value);
                default:
                    return -1;
            }

            int dpi = DPI;

            return Convert.ToInt32(value * dpi);
        }

        internal static int DPI
        {
            get
            {
                //Add comments by Ryan Wu at 12:32 Jun. 10 2007.
                //For fix the bug#8207.(Sometimes will throw the DC exception when operate List control on Win2k machine).
                //System.IntPtr hDC = GetDC(IntPtr.Zero);
                //int dpi = GetDeviceCaps(hDC, LOGPIXELSX);
                //ReleaseDC(IntPtr.Zero, hDC);
                //return dpi;
                try
                {
                    // change by Sean Huang at 2010.03.04 -->
                    // does not have reflection permission on Medium trusted level. -->
                    //System.IntPtr hDC = GetDC(IntPtr.Zero);
                    //int dpi = GetDeviceCaps(hDC, LOGPIXELSX);
                    //ReleaseDC(IntPtr.Zero, hDC);
                    //return dpi;
                    return GetDPIInternal();
                    // end of Sean Huang <--
                }
                catch
                {
                    return 96;
                }
            }
        }

        private static int GetDPIInternal()
        {
            System.IntPtr hDC = GetDC(IntPtr.Zero);
            int dpi = GetDeviceCaps(hDC, LOGPIXELSX);
            ReleaseDC(IntPtr.Zero, hDC);
            return dpi;
        }

        internal static double GetFontSizeUnitValue(FontUnit size)
        {
            switch (size.ToString())
            {
                case "Smaller":
                    return Utility.Unit2Pixel(new Unit(CONST_FONT_SMALLER, UnitType.Point));
                case "Larger":
                    return Utility.Unit2Pixel(new Unit(CONST_FONT_LARGER, UnitType.Point));
                case "XX-Small":
                    return Utility.Unit2Pixel(new Unit(CONST_FONT_XXSMALL, UnitType.Point));
                case "X-Small":
                    return Utility.Unit2Pixel(new Unit(CONST_FONT_XSMALL, UnitType.Point));
                case "Small":
                    return Utility.Unit2Pixel(new Unit(CONST_FONT_SMALL, UnitType.Point));
                case "Medium":
                    return Utility.Unit2Pixel(new Unit(CONST_FONT_MEDIUM, UnitType.Point));
                case "Large":
                    return Utility.Unit2Pixel(new Unit(CONST_FONT_LARGE, UnitType.Point));
                case "X-Large":
                    return Utility.Unit2Pixel(new Unit(CONST_FONT_XLARGE, UnitType.Point));
                case "XX-Large":
                    return Utility.Unit2Pixel(new Unit(CONST_FONT_XXLARGE, UnitType.Point));
                default:
                    return Utility.Unit2Pixel(size.Unit);
            }
        }

        public static Size MeasureStringSingleLine(String text, FontInfo font)
        {
            FontStyle fontStyle = FontStyle.Regular;

            if (font.Bold)
            {
                fontStyle |= FontStyle.Bold;
            }

            if (font.Italic)
            {
                fontStyle |= FontStyle.Italic;
            }

            if (font.Strikeout)
            {
                fontStyle |= FontStyle.Strikeout;
            }

            if (font.Underline)
            {
                fontStyle |= FontStyle.Underline;
            }

            GraphicsUnit gUnit = GraphicsUnit.Pixel;

            //switch (font.Size.Unit.Type)
            //{
            //    case UnitType.Point:
            //        gUnit = GraphicsUnit.Point;
            //        break;
            //    case UnitType.Pixel:
            //        gUnit = GraphicsUnit.Pixel;
            //        break;
            //    case UnitType.Inch:
            //        gUnit = GraphicsUnit.Inch;
            //        break;
            //    case UnitType.Cm:
            //    case UnitType.Mm:
            //        gUnit = GraphicsUnit.Millimeter;
            //        break;
            //    default:
            //        break;
            //}

            double dSize = Utility.GetFontSizeUnitValue(font.Size);

            if (font.Size.IsEmpty)
            {
                dSize = 10;
                gUnit = GraphicsUnit.Point;
            }

            if (font.Size.Unit.Type == UnitType.Cm)
            {
                dSize *= 10;
            }

            Font winFont = null;

            if (font.Name.Equals(String.Empty))
            {
                //Add comments by Ryan Wu at 17:08 May. 15 2007.
                //For fix the bug#8170.
                ////Add comments by Ryan Wu at 17:08 May. 15 2007.
                ////For support Globalization Font.
                ////winFont = new Font("MS UI Gothic", float.Parse(dSize.ToString()), fontStyle, gUnit);
                //winFont = new Font(SR.GetString("Globalization.DefaultFontName"), float.Parse(dSize.ToString()), fontStyle, gUnit);
                ////end by Ryan Wu.
                winFont = new Font(Utility.DefaultFontName, float.Parse(dSize.ToString()), fontStyle, gUnit);
                //end by Ryan Wu.
            }
            else
            {
                try
                {
                    winFont = new Font(font.Name, float.Parse(dSize.ToString()), fontStyle, gUnit);
                }
                catch (Exception)
                {
                    fontStyle |= FontStyle.Italic;

                    // change by Sean Huang at 2008.11.14, for bug 10442 -->
                    //winFont = new drawing.Font(font.Name, float.Parse(dSize.ToString()), fontStyle, gUnit);
                    try
                    {
                        winFont = new Font(font.Name, float.Parse(dSize.ToString()), fontStyle, gUnit);
                    }
                    catch (Exception ex)
                    {
                        //modified by sj
                        //winFont = new Font(font.Name, float.Parse(dSize.ToString()), FontStyle.Bold, gUnit);
                        fontStyle ^= FontStyle.Italic;
                        fontStyle |= FontStyle.Bold;
                        winFont = new Font(font.Name, float.Parse(dSize.ToString()), fontStyle, gUnit);
                        //end by sj
                    }
                    // end of Sean Huang <--
                }
            }

            try
            {
                return Utility.MeasureStringSingleLine(text, winFont);
            }
            catch
            {
                // by Sean Huang at 2010.03.04
                // does not have permission on Medium trusted level.
                return Size.Empty;
            }
        }

        public static bool IsFullTrust()
        {
            if (_isFullTrust.HasValue)
            {
                return _isFullTrust.Value;
            }
            try
            {
                new PermissionSet(PermissionState.Unrestricted).Demand();
                _isFullTrust = true;
            }
            catch (SecurityException)
            {
                _isFullTrust = false;
            }
            return _isFullTrust.Value;
        }
        public static bool IsGraphicErroOnAzure()
        {
            if (_isGrapicErrorOnAzure.HasValue)
            {
                return _isGrapicErrorOnAzure.Value;
            }
            try
            {
                Font font = new Font("Arial", 8);
                int height = font.Height;
                _isGrapicErrorOnAzure = false;
            }
            catch (OutOfMemoryException)
            {
                _isGrapicErrorOnAzure = true;
            }
            return _isGrapicErrorOnAzure.Value;
        }

        public static Size MeasureStringSingleLine(String text, Font font)
        {
            //Add comments by Yang at 13:50 Dec. 9th 2010
            //For fixing the bugs for Azure and Medium Trust
            bool isGraphicError = Utility.IsGraphicErroOnAzure() || !Utility.IsFullTrust();
            if (isGraphicError)
            {
                return Utility.MeasureStringSingleLineForAzureError(text, font);
            }
            return Utility.MeasureStringSingleLineForTrust(text, font);
        }

        //Add comments by Yang at 13:50 Dec. 9th 2010
        //For fixing the bugs for Azure and Medium Trust
        private static Size MeasureStringSingleLineForTrust(String text, Font font)
        {
            Size textSize = Size.Empty;

            if (text == null || text.Length == 0)
            {
                return textSize;
            }
            try
            {
                IntPtr hFont = Utility.GetIntPtrFromFont(font);
                LOGFONT logFont = new LOGFONT();
                Utility.GetObject(hFont, Marshal.SizeOf(typeof(LOGFONT)), ref logFont);
                IntPtr hLogFont = Utility.CreateFontIndirect(ref logFont);
                Utility.DeleteObject(hFont);
                IntPtr hdc = GetDC(IntPtr.Zero);
                IntPtr oldFont = Utility.SelectObject(hdc, hLogFont);
                textSize = Utility.NativeGetTextExtent(hdc, text);
                Utility.SelectObject(hdc, oldFont);
                Utility.DeleteObject(hLogFont);
                ReleaseDC(IntPtr.Zero, hFont);
                ReleaseDC(IntPtr.Zero, hLogFont);
                ReleaseDC(IntPtr.Zero, hdc);
                ReleaseDC(IntPtr.Zero, oldFont);
            }
            catch
            {
            }

            return textSize;
        }

        private static IntPtr GetIntPtrFromFont(Font font)
        {
            Font nFont = font == null ? System.Windows.Forms.Control.DefaultFont : font;
            return font.ToHfont();
        }

        private static Size MeasureStringSingleLineForAzureError(String text, Font font)
        {
            if (_refBitmap == null)
            {
                _refBitmap = new Bitmap(1, 1);
            }
            using (Graphics graphics = Graphics.FromImage(_refBitmap))
            {
                return graphics.MeasureString(text, font).ToSize();
            }
        }

        #region Win32 API.
        #region Nested LOGFONT Struct
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct LOGFONT
        {
            public int lfHeight;
            public int lfWidth;
            public int lfEscapement;
            public int lfOrientation;
            public int lfWeight;
            public byte lfItalic;
            public byte lfUnderline;
            public byte lfStrikeOut;
            public byte lfCharSet;
            public byte lfOutPrecision;
            public byte lfClipPrecision;
            public byte lfQuality;
            public byte lfPitchAndFamily;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string lfFaceName;

            /// <summary>
            /// Adds this methods in order to reduce the warning count of Font32 class.
            /// </summary>
            public bool Equals(LOGFONT logFont)
            {
                return (this.lfCharSet == logFont.lfCharSet &&
                    this.lfClipPrecision == logFont.lfClipPrecision &&
                    this.lfEscapement == logFont.lfEscapement &&
                    this.lfFaceName == logFont.lfFaceName &&
                    this.lfHeight == logFont.lfHeight &&
                    this.lfItalic == logFont.lfItalic &&
                    this.lfOrientation == logFont.lfOrientation &&
                    this.lfOutPrecision == logFont.lfOutPrecision &&
                    this.lfPitchAndFamily == logFont.lfPitchAndFamily &&
                    this.lfQuality == logFont.lfQuality &&
                    this.lfStrikeOut == logFont.lfStrikeOut &&
                    this.lfUnderline == logFont.lfUnderline &&
                    this.lfWeight == logFont.lfWeight &&
                    this.lfWidth == logFont.lfWidth);
            }
        }
        #endregion end of Nested LOGFONT Struct.

        /// <summary>The GetDC Function.</summary>
        [DllImport("user32.dll")]
        public static extern System.IntPtr GetDC(System.IntPtr hWnd);

        /// <summary>The ReleaseDC Function.</summary>
        [DllImport("user32.dll")]
        public static extern int ReleaseDC(System.IntPtr hWnd, System.IntPtr hDC);

        [DllImport("gdi32.dll")]
        public static extern int GetDeviceCaps(IntPtr hdc, int nIndex);

        [DllImport("gdi32.dll", CharSet = CharSet.Unicode)]
        public static extern IntPtr CreateFontIndirect(ref LOGFONT font);

        [DllImportAttribute("gdi32.dll", CharSet = CharSet.Unicode)]
        public static extern int GetObject(IntPtr hObject, int nSize, ref LOGFONT logFont);

        private static Size NativeGetTextExtent(IntPtr hdc, String s)
        {
            Size sz = new Size(0, 0);
            Utility.GetTextExtentPoint32(hdc, s, s.Length, ref sz);

            return sz;//new Size(sz.cx, sz.cy);
        }

        [DllImport("gdi32.dll", CharSet = CharSet.Unicode)]
        public static extern bool GetTextExtentPoint32(IntPtr hdc, string text, int length, [In, Out] ref Size size);

        /// <summary>The DeleteObject Function.</summary>
        [DllImportAttribute("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr gdiObject);

        /// <summary>The SelectObject Function.</summary>
        [DllImportAttribute("gdi32.dll")]
        public static extern IntPtr SelectObject(IntPtr hDC, IntPtr gdiObject);
        #endregion

        private const double CONST_FONT_SMALLER = 10;
        private const double CONST_FONT_LARGER = 13.5;
        private const double CONST_FONT_XXSMALL = 7;
        private const double CONST_FONT_XSMALL = 7.5;
        private const double CONST_FONT_SMALL = 10;
        private const double CONST_FONT_MEDIUM = 12;
        private const double CONST_FONT_LARGE = 13.5;
        private const double CONST_FONT_XLARGE = 17;
        private const double CONST_FONT_XXLARGE = 24;
        private const int LOGPIXELSX = 88;
        private static string DefaultFontName = "Arial";
        private static Bitmap _refBitmap = null;
        private static bool? _isFullTrust;
        private static bool? _isGrapicErrorOnAzure;
    }
}
