﻿using System;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif
using System.Collections;
using System.Web.UI;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{
    internal class MaskControl
    {
        private MaskFormat _maskFormat;
        internal string _text = string.Empty;
        internal string _value = string.Empty;
        private IDictionary _viewState = new StateBag();
        private bool _autoConvert;

        public bool AutoConvert
        {
            get
            {
                return this._autoConvert;
            }
            set
            {
                this._autoConvert = value;
            }
        }

        public char PromptChar
        {
            get
            {
                if (this._maskFormat.Fields != null)
                {
                    return this._maskFormat.Fields.PromptChar;
                }
                else
                {
                    return '_';
                }
            }
            set
            {
                if (this._maskFormat.Fields != null)
                {
                    this._maskFormat.Fields.PromptChar = value;
                }
            }
        }

        public string Value
        {
            get
            {
                if (this._maskFormat.Fields != null && this._maskFormat.Fields.Count > 0)
                {
                    return this._maskFormat.Fields.Value;
                }
                else
                {
                    return this._value;
                }
            }
            set
            {
                if (this._value == value)
                {
                    return;
                }

                string oldValue = this._value;
                String oldText = this._text;

                if (this._maskFormat.Fields == null
                    || (this._maskFormat.Fields != null && this._maskFormat.Fields.Count == 0))
                {
                    this._value = value;
                    this._text = value;
                }
                else
                {
                    try
                    {
                        if (this._maskFormat.Fields.SetValue(value))
                        {
                            this._value = this._maskFormat.Fields.Value;
                            this._text = this._maskFormat.Fields.Text;
                        }

                    }
                    catch
                    {
                        throw new ArgumentException(string.Format(C1Localizer.GetString("C1Input.Common.InvalidParameter"), "Value"));
                    }
                }

                ViewState["FieldText"] = this.GetFieldText();
            }
        }

        /// <summary>
        /// Gets text from the field.
        /// </summary>
        internal string GetFieldText()
        {
            if (this.Format.Fields != null && this.Format.Fields.Count > 0)
            {
                return this._maskFormat.Fields.GetFieldText();
            }
            else
            {
                return this._text;
            }
        }

        /// <summary>
        /// Gets whether the value is null.
        /// </summary>
        internal bool ValueIsNull
        {
            get
            {
                return this.Value == string.Empty;
            }
        }

        public string TextWidthLiterals
        {
            get
            {
                //For null fields null,return "";
                //For null have fields, return Format.Null.
                if (this.ValueIsNull && this._maskFormat.Null != string.Empty)
                {
                    if (this._maskFormat.Fields != null && this._maskFormat.Fields.Count > 0)
                    {
                        //return this._maskFormat.Null;
                        return this._maskFormat.NullExpression;
                    }
                }

                if (this._maskFormat.Fields != null && this._maskFormat.Fields.Count > 0)
                {
                    return this._maskFormat.Fields.TextWidthLiterals;
                }
                else
                {
                    return this._text;
                }
            }
        }

        public string TextWidthPromptAndLiterals
        {
            get
            {
                return this.Text;
            }
        }

        public string TextWidthPrompts
        {
            get
            {
                //For null fields null,return "";
                //For null have fields, return Format.Null.
                if (this.ValueIsNull && this._maskFormat.Null != string.Empty)
                {
                    if (this._maskFormat.Fields != null && this._maskFormat.Fields.Count > 0)
                    {
                        //return this._maskFormat.Null;
                        return this._maskFormat.NullExpression;
                    }
                }

                if (this._maskFormat.Fields != null && this._maskFormat.Fields.Count > 0)
                {
                    return this._maskFormat.Fields.TextWidthPrompts;
                }
                else
                {
                    return this._text;
                }
            }
        }


        public string Text
        {
            get
            {
                //For null fields null,return "";
                //For null have fields, return Format.Null.
                if (this.ValueIsNull && this._maskFormat.Null != string.Empty)
                {
                    if (this._maskFormat.Fields != null && this._maskFormat.Fields.Count > 0)
                    {
                        //return this._maskFormat.Null;
                        return this._maskFormat.NullExpression;
                    }
                }

                if (this._maskFormat.Fields != null && this._maskFormat.Fields.Count > 0)
                {
                    return this._maskFormat.Fields.Text;
                }
                else
                {
                    return this._text;
                }
            }
            set
            {
                if (this._text != value)
                {
                    string oldValue = this._value;
                    String oldText = this._text;

                    try
                    {
                        if (this._maskFormat.Fields == null
                            || (this._maskFormat.Fields != null && this._maskFormat.Fields.Count == 0))
                        {
                            //Text not equal value whenit has format, if set Value, Text is changed 
                            this._value = value;
                            this._text = value;
                            //end by Kevin

                        }
                        //if the value equals format.null, value of mask should be set to ""
                        //but no pattern should consider the value as valid input
                        else if (this._maskFormat.Null == value)
                        {
                            if (this._maskFormat.Fields.SetText("", true))
                            {
                                this._value = this._maskFormat.Fields.Value;
                                this._text = this._maskFormat.Fields.Text;
                            }
                        }
                        else if (this._maskFormat.Fields.SetText(value, true))
                        {
                            //Text not equal value whenit has format, if set Value, Text is changed 
                            this._value = this._maskFormat.Fields.Value;
                            this._text = this._maskFormat.Fields.Text;
                        }

                        //Text not equal value whenit has format, if set Value, Text is changed 
                        ViewState["FieldText"] = this.GetFieldText();
                    }
                    catch
                    {
                        throw new ArgumentException(string.Format( C1Localizer.GetString("C1Input.Common.InvalidParameter"), "Text"));
                    }
                }
            }
        }

        public IDictionary ViewState
        {
            get
            {
                return this._viewState;
            }
        }

        /// <summary>
        /// Gets or sets the format.
        /// </summary>
        public MaskFormat Format
        {
            get
            {
                return this._maskFormat;
            }
            set
            {
                try
                {
                    if (this._maskFormat != null && this._maskFormat.Equals(value))
                    {
                        return;
                    }

                    if (value == null || value.ToString() == "")
                    {
                        return;

                    }
                    value = new MaskFormat(this as IMaskFormat, value.Pattern, value.Null, value.NonFocusNull);
                    bool isPatternChanged = true;

                    if (this._maskFormat != null)
                    {
                        if (this._maskFormat.Pattern == value.Pattern)
                        {
                            isPatternChanged = false;
                        }
                    }

                    this._maskFormat = value;

                    //clear value when changing pattern 
                    if (!isPatternChanged)
                    {
                        //recover old value into fields
                        if (this._maskFormat.Fields != null && this._maskFormat.Fields.Count > 0 && ViewState["FieldText"] != null)
                        {
                            this._maskFormat.Fields.SetFieldText((string)ViewState["FieldText"]);
                        }
                    }
                    else
                    {
                        this.Text = string.Empty;
                    }

                    if (this._maskFormat != null)
                    {
                        this._maskFormat.PropertyChanged += this.MaskFormatPropertyChanged;
                    }

                    this._maskFormat.SetDirty();
                }
                catch
                {
                }
            }
        }

        private void MaskFormatPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
        }


        public string EmptyString
        {
            get
            {
                if (this._maskFormat.Fields != null)
                {
                    return this._maskFormat.Fields.GetFillingString(this._maskFormat.PromptChar);
                }
                else
                {
                    return "";
                }
            }
        }
    }
}
