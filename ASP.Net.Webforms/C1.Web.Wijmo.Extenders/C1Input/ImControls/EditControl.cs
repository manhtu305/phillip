﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{
    internal class EditControl
    {
        private string _format = "";
        private TextFilter _filter;
        private bool _lengthAsByte;
        private bool _autoConvert;
        private int _maxLength;
        private int _maxLineCount;
        private string _text;
        private bool _wordWrap;
        private bool _countWrappedLine;
        private bool _multiLine;

#if EXTENDER
        private C1InputTextExtender _owner;
#else
        private C1InputText _owner;
#endif
        
        private bool _designMode;
        private FontInfo _font;

        
#if EXTENDER
       public EditControl(C1InputTextExtender owner)
#else
        public EditControl(C1InputText owner)
#endif
        {
            this._owner = owner;
        }

#if EXTENDER
        public C1InputTextExtender Owner
#else
        public C1InputText Owner
#endif
        {
            get
            {
                return this._owner;
            }
        }

        public FontInfo Font
        {
            get
            {
                return this._font;
            }
        }

        public bool DesignMode
        {
            get
            {
                return this._designMode;
            }
            set
            {
                this._designMode = value;
            }
        }

        public bool LengthAsByte
        {
            get
            {
                return this._lengthAsByte;
            }
            set
            {
                if (value != this._lengthAsByte)
                {
                    this._lengthAsByte = value;
                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(this.Owner);
                    properties["Text"].SetValue(this.Owner, this.GetCheckedLength(this.Text, this.MaxLength, this._lengthAsByte));
                }
            }
        }

        public int MaxLength
        {
            get
            {
                return this._maxLength;
            }
            set
            {
                if (value != this._maxLength)
                {
                    if (value >= 0)
                    {
                        this._maxLength = value;
                        PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(this.Owner);
                        properties["Text"].SetValue(this.Owner, this.GetCheckedLength(this.Text, this._maxLength, this.LengthAsByte));
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException("MaxLength", C1Localizer.GetString("Exception.Common.NegativeValue"));
                    }
                }
            }
        }

        public bool AutoConvert
        {
            get
            {
                return this._autoConvert;
            }
            set
            {
                if (value != this._autoConvert)
                {
                    this._autoConvert = value;
                    this._filter = new TextFilter(this.Format, this.AutoConvert);

                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(this.Owner);
                    properties["Text"].SetValue(this.Owner, this.GetCheckedLength(this._filter.FilterTextByFormat(this.Text), this.MaxLength, this.LengthAsByte));

                }
            }
        }

        public string Format
        {
            get
            {
                return _format;
            }
            set
            {
                try
                {
                    if (this._format != value)
                    {
                        this._filter = new TextFilter(value, this.AutoConvert);
                        this._format = value;
                        PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(this.Owner);
                        properties["Text"].SetValue(this.Owner, this.GetCheckedLength(this._filter.FilterTextByFormat(this.Text), this.MaxLength, this.LengthAsByte));
                    }
                }
                catch (System.Exception)
                {
                    throw new ArgumentException(string.Format(C1Localizer.GetString("C1Input.Common.InvalidParameter"), "Format"));
                }
            }
        }

        public int MaxLineCount
        {
            get
            {
                return this._maxLineCount;
            }
            set
            {
                if (value != this._maxLineCount)
                {
                    if (value < 0)
                    {
                        throw new ArgumentOutOfRangeException("MaxLineCount", C1Localizer.GetString("Exception.Common.NegativeValue"));
                    }
                    this._maxLineCount = value;

                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(this.Owner);
                    properties["Text"].SetValue(this.Owner, this.GetCheckedLine(this.Text, value, this.CountWrappedLine, this.WordWrap, this.MultiLine, this.Font));
                }
            }
        }

        public bool MultiLine
        {
            get
            {
                return this._multiLine;
            }
            set
            {
                if (value != this._multiLine)
                {
                    this._multiLine = value;
                    if (value == false)
                    {
                        PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(this.Owner);
                        properties["Text"].SetValue(this.Owner, this.Text.Replace("\r\n", string.Empty));
                    }
                }
            }
        }

        public bool CountWrappedLine
        {
            get
            {
                return this._countWrappedLine;
            }
            set
            {
                if (value != this._countWrappedLine)
                {
                    this._countWrappedLine = value;
                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(this.Owner);
                    properties["Text"].SetValue(this.Owner,
                        this.GetCheckedLine(this.Text, this.MaxLineCount, value, this.WordWrap, this.MultiLine,
                            this.Font));

                }
            }
        }

        public bool WordWrap
        {
            get
            {
                return this._wordWrap;
            }
            set
            {
                if (value != this._wordWrap)
                {
                    PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(this.Owner);
                    properties["Text"].SetValue(this.Owner, this.GetCheckedLine(this.Text, this.MaxLineCount, this.CountWrappedLine, value, this.MultiLine, this.Font));
                }
            }
        }

        public string Text
        {
            get
            {
                return this._text;
            }
            set
            {
                this._filter = new TextFilter(this.Format, this.AutoConvert);
                string oldText = this.Text;

                string tempText = this.GetCheckedLength(this._filter.FilterTextByFormat(value), this.MaxLength, this.LengthAsByte);
                this._text = this.GetCheckedLine(tempText, this.MaxLineCount, this.CountWrappedLine, this.WordWrap, this.MultiLine, this.Font);
            }
        }

        private string GetCheckedLine(string text, int maxLine, bool isCountWrappedLine, bool isWordwrap, bool multiLine, FontInfo font)
        {
            // TODO:
            //bool bIsIE =  (String.Compare(Utility.GetBrowser(this.DesignMode, this.Page), "IE", true) == 0);
            bool bIsIE = true;
            if (!multiLine || maxLine == 0 || !bIsIE)
            {
                return text;
            }
            bool _isCountWrappedLine = isCountWrappedLine && isWordwrap;

            FontInfo fNewFont = new System.Web.UI.WebControls.Style().Font;
            fNewFont.CopyFrom(font);
            // TODO:
            //if (font.Name.Equals(string.Empty))
            //{
            //    fNewFont.Name = this.DefaultInputFontName;
            //}
            int lineCount = this.GetLineCount(text, _isCountWrappedLine, fNewFont);
            if (lineCount <= maxLine)
            {
                return text;
            }
            else
            {
                int start = 0;
                int end = text.Length;
                do
                {
                    int mid = (start + end) / 2;
                    string temptext = text.Substring(0, mid);
                    lineCount = this.GetLineCount(temptext, _isCountWrappedLine, fNewFont);
                    if (lineCount > maxLine)
                    {
                        end = mid;
                    }
                    else if (lineCount < maxLine)
                    {
                        start = mid;
                    }
                    else if ((lineCount == maxLine) && (end > start + 1))
                    {
                        start = mid;
                    }
                    else if ((lineCount == maxLine) && (end == start + 1))
                    {
                        break;
                    }
                }
                while (start < end);

                return text.Substring(0, start);
            }
        }

        private int GetLineCount(string text, bool isCountWrappedLine, FontInfo font)
        {
            int count = 0;

            if (text == "")
            {
                return 1;
            }

            if (!isCountWrappedLine)
            {
                string temptext = text;
                int tempcount = temptext.Length;
                if (temptext.Contains("\r\n"))
                {
                    temptext = temptext.Replace("\r\n", "");
                }
                count += (tempcount - temptext.Length) / 2;

                tempcount = temptext.Length;
                if (temptext.Contains("\r"))
                {
                    temptext = temptext.Replace("\r", "");
                }
                count += tempcount - temptext.Length;

                tempcount = temptext.Length;
                if (temptext.Contains("\n"))
                {
                    temptext = temptext.Replace("\n", "");
                }
                count += tempcount - temptext.Length;
                count += 1;
            }
            else
            {
                string[] arrLines = text.Split(new string[] { "\r\n" }, StringSplitOptions.None);
                for (int i = 0; i < arrLines.Length; i++)
                {
                    count += this.SplitTextWithR(arrLines[i], font);
                }

            }
            return count;
        }

        private int SplitTextWithR(string text, FontInfo font)
        {
            if (text == "")
            {
                return 1;
            }

            string[] arrLines = text.Split(new char[] { '\r' });
            int count = 0;
            for (int i = 0; i < arrLines.Length; i++)
            {
                count += this.SplitTextWithN(arrLines[i], font);
            }
            return count;
        }

        private int SplitTextWithN(string text, FontInfo font)
        {
            if (text == "")
            {
                return 1;
            }

            string[] arrLines = text.Split(new char[] { '\n' });
            int count = 0;
            for (int i = 0; i < arrLines.Length; i++)
            {
                count += this.GetSingleLineTextLine(arrLines[i], font);
            }
            return count;
        }

        private int GetWrapperWidth()
        {
#if EXTENDER
            // TOOD: 
            return 0;
#else
            int width = (int)this._owner.Width.Value;
            if (width == 0) width = C1InputBase.DesignTimeLayout.WrapperWidth;

            return width;
#endif
        }

        private int GetSingleLineTextLine(string text, FontInfo font)
        {
            if (text == "")
            {
                return 1;
            }

            int textWidth = Utility.MeasureStringSingleLine(text, font).Width;
            int inputboxWidth = this.GetWrapperWidth();

            int lineCount = textWidth / inputboxWidth;
            int remainCount = textWidth % inputboxWidth;

            if (lineCount == 0)
            {
                return 1;
            }

            if (remainCount == 0)
            {
                return lineCount;
            }

            return ++lineCount;
        }

        internal string GetCheckedLength(string text, int maxLength, bool lengthAsByte)
        {
            if (maxLength == 0)
            {
                return text;
            }
            else
            {
                string checkedText = "";
                if (lengthAsByte)
                {
                    StringInfo siText = new StringInfo(text);
                    int iLength = siText.LengthInTextElements;
                    int length = 0;
                    for (int i = 0; i < iLength; i++)
                    {
                        string strChar = siText.SubstringByTextElements(i, 1);
                        if (strChar.Length > 1)
                        {
                            length += strChar.Length * 2;
                        }
                        else if (Char.IsSurrogate(strChar, 0))
                        {
                            length = length + 4;
                        }
                        else if (CharEx.IsFullWidth(strChar[0]))
                        {
                            length += 2;
                        }
                        else
                        {
                            length++;
                        }


                        if (length <= maxLength)
                        {
                            checkedText += strChar;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                else
                {
                    if (Utility.GetStringLength(text) <= maxLength)
                    {
                        checkedText = text;
                    }
                    else
                    {
                        checkedText = Utility.Substring(text, 0, maxLength);
                    }
                }

                int temp = Utility.GetStringLength(checkedText);
                if (temp != 0 && Utility.Substring(checkedText, temp - 1, 1) == "\r")
                {
                    checkedText = Utility.Substring(checkedText, 0, temp - 1);
                }
                return checkedText;
            }
        }
    }
}
