﻿#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{

    #region Field class
    /// <summary>
    /// Represents the Field.
    /// </summary>
    internal abstract class Field
    {
        #region Members
        private FieldCollection _owner;
        private string _text = "";
        private string _oldText = "";
        private int _trueLength;
        #endregion end of Members.

        #region Abstract Properties
        /// <summary>
        /// Gets the minimum length.
        /// </summary>
        public abstract int MinLength
        {
            get;
        }

        /// <summary>
        /// Gets the maximum length.
        /// </summary>
        public abstract int MaxLength
        {
            get;
        }
        #endregion end of Abstract properties.

        #region Property
        /// <summary>
        /// Gets or sets the prompt character of the field, for filling field text
        /// when the text length is less than the minimum length of this field.
        /// </summary>
        public char PromptChar
        {
            get
            {
                if (this.Owner != null)
                {
                    return this.Owner.PromptChar;
                }
                return '_';
            }
        }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public virtual string Text
        {
            get
            {
                return this._text;
            }
            set
            {
                this._text = value;
            }
        }

        /// <summary>
        /// Gets or sets the old text.
        /// </summary>
        public virtual string OldText
        {
            get
            {
                return this._oldText;
            }
            set
            {
                this._oldText = value;
            }
        }

        /// <summary>
        /// Gets the length of the field.
        /// </summary>
        public int Length
        {
            get
            {
                return Utility.GetStringLength(this.Text);
            }
        }

        /// <summary>
        /// Gets the true length of the field.
        /// </summary>
        /// <remarks>Ellipse the effection of filling prompt char.</remarks>
        public int TrueLength
        {
            get
            {
                return this._trueLength;
            }
            set
            {
                this._trueLength = value;
            }
        }

        /// <summary>
        /// Gets the true length of this field when ShowLiterals is not always.
        /// </summary>
        public virtual int TrueBitsLength
        {
            get
            {
                return this._trueLength;
            }
        }

        /// <summary>
        /// Gets or sets the parent of this Field.
        /// </summary>
        protected FieldCollection Owner
        {
            get
            {
                return this._owner;
            }
        }
        #endregion end of Properties.

        #region Methods
        /// <summary>
        /// Assign the owner to the field.
        /// </summary>
        /// <param name="owner">The owner object</param>
        internal void InternalAssignOwner(FieldCollection owner)
        {
            this._owner = owner;
        }

        /// <summary>
        /// Clears these field.
        /// </summary>
        public virtual void Clear() { }

        /// <summary>
        /// Inserts text at the specified position.
        /// </summary>
        /// <param name="start">Starting position to insert text</param>
        /// <param name="text">Text string to insert</param>
        /// <param name="existInvalid">Returns whether there are invalid characters</param>
        public virtual void Insert(ref int start, ref string text, out bool existInvalid)
        {
            existInvalid = false;
        }

        /// <summary>
        /// Replaces the selected text with a new text string.
        /// </summary>
        /// <param name="start">Starting position of the selected text</param>
        /// <param name="length">Length of the selected Text</param>
        /// <param name="text">Text string to be replaced</param>
        /// <param name="existInvalid">Returns whether there are invalid characters</param>
        public virtual void Replace(ref int start, int length, ref string text, out bool existInvalid)
        {
            existInvalid = false;
        }

        /// <summary>
        /// Deletes the selected text.
        /// </summary>
        /// <param name="start">Starting position of the selected text</param>
        /// <param name="length">Length of the selected Text</param>
        public virtual void Delete(ref int start, int length)
        {
        }

        /// <summary>
        /// Set the specified text to the field.
        /// </summary>
        /// <param name="text">Text to set</param>
        /// <param name="existInvalid">Returns whether there are invalid characters</param>
        public virtual void SetText(ref string text, out bool existInvalid)
        {
            existInvalid = false;
        }

        /// <summary>
        /// Set the specified text to the field for mask control.
        /// </summary>
        /// <param name="text">Text to set</param>
        /// <param name="existInvalid">Returns whether there are invalid characters</param>
        /// <param name="markIsText">Mark whether it is set text(true) or set value(false)</param>
        public virtual void SetText(ref string text, out bool existInvalid, bool markIsText)
        {
            existInvalid = false;
        }

        /// <summary>
        /// Gets the field text of the field object.
        /// </summary>
        /// <returns>Field text string of the field object</returns>
        public virtual string GetFieldText()
        {
            return string.Empty;
        }
        #endregion end of Methods.
    }
    #endregion end of Field class.

    #region PromptField class
    /// <summary>
    /// Repersents the field where the prompt string is shown.
    /// </summary>
    internal class PromptField : Field
    {
        #region Members
        private string _promptText = string.Empty;
        #endregion end of Members.

        #region Properties
        /// <summary>
        /// Indicates the prompt string.
        /// </summary>
        protected string PromptText
        {
            get
            {
                return this._promptText;
            }
            set
            {
                this._promptText = value;
            }
        }

        /// <summary>
        /// Gets the minimum length.
        /// </summary>
        public override int MinLength
        {
            get
            {
                return this._promptText.Length;
            }
        }

        /// <summary>
        /// Gets the maximum length.
        /// </summary>
        public override int MaxLength
        {
            get
            {
                return this._promptText.Length;
            }
        }

        /// <summary>
        /// Gets or sets Text.
        /// </summary>
        public override string Text
        {
            get
            {
                return this._promptText;
            }
        }

        /// <summary>
        /// Gets whether the field's Length is field's MaxLength.
        /// </summary>
        public bool IsOver
        {
            get
            {
                return true;
            }
        }
        #endregion end of Properties.

        #region Constructor
        /// <summary>
        /// Creates a prompt field with the specified text.
        /// </summary>
        /// <param name="text">Specified text</param>
        public PromptField(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                this._promptText = text;
            }
        }
        #endregion end of Constructor.

        #region Methods
        /// <summary>
        /// Gets the field text of the field object.
        /// </summary>
        /// <returns>The field text string of the field object.</returns>
        public override string GetFieldText()
        {
            return this._promptText;
        }

        /// <summary>
        /// Gets an empty expression string.
        /// </summary>
        /// <param name="promptChar">The prompt character</param>
        /// <param name="fillTail">Whether the field tail is trimed</param>
        /// <returns>Empty expression string</returns>
        public string GetFillingString(char promptChar, bool fillTail)
        {
            return this.Text;
        }

        /// <summary>
        /// Sets text to this field.
        /// </summary>
        /// <param name="text">The text to set</param>
        /// <param name="existInvalid">Returns whether there are invalid characters</param>
        public override void SetText(ref string text, out bool existInvalid)
        {
            if (text == this._promptText)
            {
                existInvalid = false;
                text = text.Substring(this._promptText.Length);
            }
            else
            {
                existInvalid = true;
            }
        }

        /// <summary>
        /// Sets text to this field.
        /// </summary>
        /// <param name="text">The text to set</param>
        /// <param name="existInvalid">Returns whether there are invalid characters</param>
        /// <param name="markIsText">Mark whether is set text or set value</param>
        public override void SetText(ref string text, out bool existInvalid, bool markIsText)
        {
            if (text == this._promptText)
            {
                existInvalid = false;
                text = text.Substring(this._promptText.Length);
            }
            else
            {
                existInvalid = true;
            }
        }
        #endregion end of Methods.
    }
    #endregion end of PromptField class.
}
