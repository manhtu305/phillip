﻿using System;
using System.Collections.Generic;
using System.Text;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{
    /// <summary>
    /// Represents the filter that checks the validity of text in the text editor.
    /// </summary>
    internal class TextFilter : FilterField.CharacterFilter
    {
        #region static field

        private const char SYMBOL_Emoji = 'Ｅ';
        private const char SYMBOL_IVS = 'Ｖ';

        /// <summary>
        /// Indicates all keyword characters in format.
        /// </summary>
        static protected List<char> _keywordChars = new List<char>(new char[] { 
            '\xff21',	// Ａ
            'A',	    // A
            '\xff41',	// ａ
            'a',	    // a
            '\xff2b',	// Ｋ
            'K',	    // K
            '\xff19',	// ９
            '9',	    // 9
            '\xff03',	// ＃
            '#',	    // #
            '\xff20',	// ＠
            '@',	    // @
            '\xff22',	// Ｂ
            'B',	    // B
            '\xff38',	// Ｘ
            'X',	    // X
            '\xff2a',	// Ｊ
            '\xff3a',	// Ｚ
            'H',	    // H
            '\xff2e',	// Ｎ
            'N',	    // N
            '\xff27',	// DBCS G
            '\xff33',   // DBCS S
            'S',	    // S
            '\xff24',	//  //DBCS D
            '\xff34',	// Ｔ
            '\xff2D',	// DBCS M
            '\xff29',	// DBCS I
            SYMBOL_Emoji,
            SYMBOL_IVS
        });
        #endregion


        #region The CharType enumeration
        /// <summary>
        /// Indicates the character type.
        /// </summary>
        [Flags]
        private enum CharType : uint
        {
            DBCS_UpperAlphabet = 0x80000002,// 1000 0000 0000 0000,0000 0000 0000 0010
            SBCS_UpperAlphabet = 0x40000001,// 0100 0000 0000 0000,0000 0000 0000 0001

            DBCS_LowerAlphabet = 0x80000008,// 1000 0000 0000 0000,0000 0000 0000 1000
            SBCS_LowerAlphabet = 0x40000004,// 0100 0000 0000 0000,0000 0000 0000 0100

            DBCS_Number = 0x80000020,// 1000 0000 0000 0000,0000 0000 0010 0000
            SBCS_Number = 0x40000010,// 0100 0000 0000 0000,0000 0000 0001 0000

            DBCS_Binary = 0x80000080,// 1000 0000 0000 0000,0000 0000 1000 0000
            SBCS_Binary = 0x40000040,// 0100 0000 0000 0000,0000 0000 0100 0000

            DBCS_Hexadecimal = 0x80000200,// 1000 0000 0000 0000,0000 0010 0000 0000
            SBCS_Hexadecimal = 0x40000100,// 0100 0000 0000 0000,0000 0001 0000 0000

            DBCS_Symbol = 0x80000800,// 1000 0000 0000 0000,0000 1000 0000 0000
            SBCS_Symbol = 0x40000400,// 0100 0000 0000 0000,0000 0100 0000 0000

            DBCS_NumberSymbol = 0x80002000,// 1000 0000 0000 0000,0010 0000 0000 0000
            SBCS_NumberSymbol = 0x40001000,// 0100 0000 0000 0000,0001 0000 0000 0000

            DBCS_Katakana = 0x80008000,// 1000 0000 0000 0000,1000 0000 0000 0000
            SBCS_Katakana = 0x40004000,// 0100 0000 0000 0000,0100 0000 0000 0000

            DBCS_Space = 0x80020000,// 1000 0000 0000 0010,0000 0000 0000 0000
            SBCS_Space = 0x40010000,// 0100 0000 0000 0001,0000 0000 0000 0000

            TwoBytes = 0x80080000,// 1000 0000 0000 1000,0000 0000 0000 0000
            FourBytes = 0x80040000,// 1000 0000 0000 0100,0000 0000 0000 0000

            DBCS_ShiftJIS = 0x80100000,// 1000 0000 0001 0000,0000 0000 0000 0000
            DBCS_JISX0208 = 0x80200000,// 1000 0000 0010 0000,0000 0000 0000 0000

            Emoji = 0x00400000,          // 0000 0000 0100 0000,0000 0000 0000 0000
            IVS = 0x00800000,            // 0000 0000 1000 0000,0000 0000 0000 0000

            //add new format here

            DBCS_Hiragana = 0x81000000,// 1000 0001 0000 0000,0000 0000 0000 0000

            Upper_SBCS_Katakana = 0x48000000,// 0100 1000 0000 0000,0000 0000 0000 0000
            Upper_DBCS_Katakana = 0x90000000,// 1001 0000 0000 0000,0000 0000 0000 0000
            Upper_DBCS_Hiragana = 0xA0000000,// 1010 0000 0000 0000,0000 0000 0000 0000

            All = 0xC6000000,// 1100 0110 0000 0000,0000 0000 0000 0000
            DBCS_All = 0x84000000,// 1000 0100 0000 0000,0000 0000 0000 0000
            SBCS_All = 0x42000000,// 0100 0010 0000 0000,0000 0000 0000 0000

            DBCS = 0x80000000,// 1000 0000 0000 0000,0000 0000 0000 0000
            SBCS = 0x40000000,// 0100 0000 0000 0000,0000 0000 0000 0000
        }
        #endregion

        #region Members
        /// <summary>
        /// Specifies the format string.
        /// </summary>
        private string _format = string.Empty;
        /// <summary>
        /// Specifies the _excludeFormat string.
        /// </summary>
        private string _excludeFormat = string.Empty;
        /// <summary>
        /// Specifies the _includeFormat string.
        /// </summary>
        private string _includeFormat = string.Empty;
        /// <summary>
        /// Specifies the _excludeNormalChar string.
        /// </summary>
        private string _excludeNormalChar = string.Empty;
        /// <summary>
        /// Specifies the _includeNormalChar string.
        /// </summary>
        private string _includeNormalChar = string.Empty;
        /// <summary>
        /// Specifies which type is allowed.
        /// </summary>
        private CharType _allowTypes = 0;
        /// <summary>
        /// Specifies which type is exclude.
        /// </summary>
        private CharType _excludeTypes = 0;
        /// <summary>
        /// Specifies whether it is automatically convert.
        /// </summary>
        private bool _autoConvert = true;
        /// <summary>
        /// Specifies whether the input is valid.
        /// </summary>
        private bool _isInputValid = true;

       // private bool _ivsAssociated = false;
        #endregion

        #region __symbolArray
        /// <summary>
        /// Specifies the symbol collection which @ and ＠ stands for.
        /// </summary>
        private char[] _symbolArray = new char[]{
													//'\u3000',	//IDEOGRAPHIC SPACE
													'\u3001',	//IDEOGRAPHIC COMMA
													'\u3002',	//IDEOGRAPHIC FULL STOP
													'\uFF0C',	//FULLWIDTH COMMA
													'\uFF0E',	//FULLWIDTH FULL STOP
													'\u30FB',	//KATAKANA MIDDLE DOT
													'\uFF1A',	//FULLWIDTH COLON
													'\uFF1B',	//FULLWIDTH SEMICOLON
													'\uFF1F',	//FULLWIDTH QUESTION MARK
													'\uFF01',	//FULLWIDTH EXCLAMATION MARK
													'\u309B',	//KATAKANA-HIRAGANA VOICED SOUND MARK
													'\u309C',	//KATAKANA-HIRAGANA SEMI-VOICED SOUND MARK
													'\u00B4',	//ACUTE ACCENT
													'\uFF40',	//FULLWIDTH GRAVE ACCENT
													'\u00A8',	//DIAERESIS
													'\uFF3E',	//FULLWIDTH CIRCUMFLEX ACCENT
													'\uFFE3',	//FULLWIDTH MACRON 
													'\uFF3F',	//FULLWIDTH LOW LINE
													'\u30FD',	//KATAKANA ITERATION MARK
													'\u30FE',	//KATAKANA VOICED ITERATION MARK
													'\u309D',	//HIRAGANA ITERATION MARK
													'\u309E',	//HIRAGANA VOICED ITERATION MARK
													'\u3003',	//DITTO MARK
													'\u4EDD',	//<cjk>
													'\u3005',	//IDEOGRAPHIC ITERATION MARK
													'\u3006',	//IDEOGRAPHIC CLOSING MARK
													'\u3007',	//IDEOGRAPHIC NUMBER ZERO
													'\u30FC',	//KATAKANA-HIRAGANA PROLONGED SOUND MARK
													'\u2014',	//EM DASH	Windows: U+2015
													'\u2010',	//HYPHEN
													'\uFF0F',	//FULLWIDTH SOLIDUS
													'\u005C',	//REVERSE SOLIDUS	Fullwidth: U+FF3C
													'\u301C',	//WAVE DASH	Windows: U+FF5E
													'\u2016',	//DOUBLE VERTICAL LINE	Windows: U+2225
													'\uFF5C',	//FULLWIDTH VERTICAL LINE
													'\u2026',	//HORIZONTAL ELLIPSIS
													'\u2025',	//TWO DOT LEADER
													'\u2018',	//LEFT SINGLE QUOTATION MARK
													'\u2019',	//RIGHT SINGLE QUOTATION MARK
													'\u201C',	//LEFT DOUBLE QUOTATION MARK
													'\u201D',	//RIGHT DOUBLE QUOTATION MARK
													'\uFF08',	//FULLWIDTH LEFT PARENTHESIS
													'\uFF09',	//FULLWIDTH RIGHT PARENTHESIS
													'\u3014',	//LEFT TORTOISE SHELL BRACKET
													'\u3015',	//RIGHT TORTOISE SHELL BRACKET
													'\uFF3B',	//FULLWIDTH LEFT SQUARE BRACKET
													'\uFF3D',	//FULLWIDTH RIGHT SQUARE BRACKET
													'\uFF5B',	//FULLWIDTH LEFT CURLY BRACKET
													'\uFF5D',	//FULLWIDTH RIGHT CURLY BRACKET
													'\u3008',	//LEFT ANGLE BRACKET
													'\u3009',	//RIGHT ANGLE BRACKET
													'\u300A',	//LEFT DOUBLE ANGLE BRACKET
													'\u300B',	//RIGHT DOUBLE ANGLE BRACKET
													'\u300C',	//LEFT CORNER BRACKET
													'\u300D',	//RIGHT CORNER BRACKET
													'\u300E',	//LEFT WHITE CORNER BRACKET
													'\u300F',	//RIGHT WHITE CORNER BRACKET
													'\u3010',	//LEFT BLACK LENTICULAR BRACKET
													'\u3011',	//RIGHT BLACK LENTICULAR BRACKET
													'\uFF0B',	//FULLWIDTH PLUS SIGN
													'\u2212',	//MINUS SIGN	Windows: U+FF0D
													'\u00B1',	//PLUS-MINUS SIGN
													'\u00D7',	//MULTIPLICATION SIGN
													'\u00F7',	//DIVISION SIGN
													'\uFF1D',	//FULLWIDTH EQUALS SIGN
													'\u2260',	//NOT EQUAL TO
													'\uFF1C',	//FULLWIDTH LESS-THAN SIGN
													'\uFF1E',	//FULLWIDTH GREATER-THAN SIGN
													'\u2266',	//LESS-THAN OVER EQUAL TO
													'\u2267',	//GREATER-THAN OVER EQUAL TO
													'\u221E',	//INFINITY
													'\u2234',	//THEREFORE
													'\u2642',	//MALE SIGN
													'\u2640',	//FEMALE SIGN
													'\u00B0',	//DEGREE SIGN
													'\u2032',	//PRIME
													'\u2033',	//DOUBLE PRIME
													'\u2103',	//DEGREE CELSIUS
													'\uFFE5',	//FULLWIDTH YEN SIGN
													'\uFF04',	//FULLWIDTH DOLLAR SIGN
													'\u00A2',	//CENT SIGN	Windows: U+FFE0
													'\u00A3',	//POUND SIGN	Windows: U+FFE1
													'\uFF05',	//FULLWIDTH PERCENT SIGN
													'\uFF03',	//FULLWIDTH NUMBER SIGN
													'\uFF06',	//FULLWIDTH AMPERSAND
													'\uFF0A',	//FULLWIDTH ASTERISK
													'\uFF20',	//FULLWIDTH COMMERCIAL AT
													'\u00A7',	//SECTION SIGN
													'\u2606',	//WHITE STAR
													'\u2605',	//BLACK STAR
													'\u25CB',	//WHITE CIRCLE
													'\u25CF',	//BLACK CIRCLE
													'\u25CE',	//BULLSEYE
													'\u25C7',	//WHITE DIAMOND
													'\u25C6',	//BLACK DIAMOND
													'\u25A1',	//WHITE SQUARE
													'\u25A0',	//BLACK SQUARE
													'\u25B3',	//WHITE UP-POINTING TRIANGLE
													'\u25B2',	//BLACK UP-POINTING TRIANGLE
													'\u25BD',	//WHITE DOWN-POINTING TRIANGLE
													'\u25BC',	//BLACK DOWN-POINTING TRIANGLE
													'\u203B',	//REFERENCE MARK
													'\u3012',	//POSTAL MARK
													'\u2192',	//RIGHTWARDS ARROW
													'\u2190',	//LEFTWARDS ARROW
													'\u2191',	//UPWARDS ARROW
													'\u2193',	//DOWNWARDS ARROW
													'\u3013',	//GETA MARK
													'\uFF07',	//FULLWIDTH APOSTROPHE
													'\uFF02',	//FULLWIDTH QUOTATION MARK	[2000]
													'\uFF0D',	//FULLWIDTH HYPHEN-MINUS	[2000]
													'\u007E',	//TILDE	[2000]	Fullwidth: U+FF5E
													'\u3033',	//VERTICAL KANA REPEAT MARK UPPER HALF	[2000]
													'\u3034',	//VERTICAL KANA REPEAT WITH VOICED SOUND MARK UPPER HALF	[2000]
													'\u3035',	//VERTICAL KANA REPEAT MARK LOWER HALF	[2000]
													'\u303B',	//VERTICAL IDEOGRAPHIC ITERATION MARK	[2000]	[Unicode3.2]
													'\u303C',	//MASU MARK	[2000]	[Unicode3.2]
													'\u30FF',	//KATAKANA DIGRAPH KOTO	[2000]	[Unicode3.2]
													'\u309F',	//HIRAGANA DIGRAPH YORI	[2000]	[Unicode3.2]
													'\u2208',	//ELEMENT OF	[1983]
													'\u220B',	//CONTAINS AS MEMBER	[1983]
													'\u2286',	//SUBSET OF OR EQUAL TO	[1983]
													'\u2287',	//SUPERSET OF OR EQUAL TO	[1983]
													'\u2282',	//SUBSET OF	[1983]
													'\u2283',	//SUPERSET OF	[1983]
													'\u222A',	//UNION	[1983]
													'\u2229',	//INTERSECTION	[1983]
													'\u2284',	//NOT A SUBSET OF	[2000]
													'\u2285',	//NOT A SUPERSET OF	[2000]
													'\u228A',	//SUBSET OF WITH NOT EQUAL TO	[2000]
													'\u228B',	//SUPERSET OF WITH NOT EQUAL TO	[2000]
													'\u2209',	//NOT AN ELEMENT OF	[2000]
													'\u2205',	//EMPTY SET	[2000]
													'\u2305',	//PROJECTIVE	[2000]
													'\u2306',	//PERSPECTIVE	[2000]
													'\u2227',	//LOGICAL AND	[1983]
													'\u2228',	//LOGICAL OR	[1983]
													'\u00AC',	//NOT SIGN	[1983]	Windows: U+FFE2
													'\u21D2',	//RIGHTWARDS DOUBLE ARROW	[1983]
													'\u21D4',	//LEFT RIGHT DOUBLE ARROW	[1983]
													'\u2200',	//FOR ALL	[1983]
													'\u2203',	//THERE EXISTS	[1983]
													'\u2295',	//CIRCLED PLUS	[2000]
													'\u2296',	//CIRCLED MINUS	[2000]
													'\u2297',	//CIRCLED TIMES	[2000]
													'\u2225',	//PARALLEL TO	[2000]
													'\u2226',	//NOT PARALLEL TO	[2000]
													'\u2985',	//LEFT WHITE PARENTHESIS	[2000]	[Unicode3.2]
													'\u2986',	//RIGHT WHITE PARENTHESIS	[2000]	[Unicode3.2]
													'\u3018',	//LEFT WHITE TORTOISE SHELL BRACKET	[2000]
													'\u3019',	//RIGHT WHITE TORTOISE SHELL BRACKET	[2000]
													'\u3016',	//LEFT WHITE LENTICULAR BRACKET	[2000]
													'\u3017',	//RIGHT WHITE LENTICULAR BRACKET	[2000]
													'\u2220',	//ANGLE	[1983]
													'\u22A5',	//UP TACK	[1983]
													'\u2312',	//ARC	[1983]
													'\u2202',	//PARTIAL DIFFERENTIAL	[1983]
													'\u2207',	//NABLA	[1983]
													'\u2261',	//IDENTICAL TO	[1983]
													'\u2252',	//APPROXIMATELY EQUAL TO OR THE IMAGE OF	[1983]
													'\u226A',	//MUCH LESS-THAN	[1983]
													'\u226B',	//MUCH GREATER-THAN	[1983]
													'\u221A',	//SQUARE ROOT	[1983]
													'\u223D',	//REVERSED TILDE 	[1983]
													'\u221D',	//PROPORTIONAL TO	[1983]
													'\u2235',	//BECAUSE	[1983]
													'\u222B',	//INTEGRAL	[1983]
													'\u222C',	//DOUBLE INTEGRAL	[1983]
													'\u2262',	//NOT IDENTICAL TO	[2000]
													'\u2243',	//ASYMPTOTICALLY EQUAL TO	[2000]
													'\u2245',	//APPROXIMATELY EQUAL TO	[2000]
													'\u2248',	//ALMOST EQUAL TO	[2000]
													'\u2276',	//LESS-THAN OR GREATER-THAN	[2000]
													'\u2277',	//GREATER-THAN OR LESS-THAN	[2000]
													'\u2194',	//LEFT RIGHT ARROW	[2000]
													'\u212B',	//ANGSTROM SIGN	[1983]
													'\u2030',	//PER MILLE SIGN	[1983]
													'\u266F',	//MUSIC SHARP SIGN	[1983]
													'\u266D',	//MUSIC FLAT SIGN	[1983]
													'\u266A',	//EIGHTH NOTE	[1983]
													'\u2020',	//DAGGER	[1983]
													'\u2021',	//DOUBLE DAGGER	[1983]
													'\u00B6',	//PILCROW SIGN	[1983]
													'\u266E',	//MUSIC NATURAL SIGN	[2000]
													'\u266B',	//BEAMED EIGHTH NOTES	[2000]
													'\u266C',	//BEAMED SIXTEENTH NOTES	[2000]
													'\u2669',	//QUARTER NOTE	[2000]
													'\u25EF',	//LARGE CIRCLE	[1983]
													'\uFF3C',   // '＼'
													'\uFF5E',   // '～'
													'\uFFE0',   // 'E
													'\uFFE1',   // 'E
													'\uFFE2',   // '￢'
													'\u2015',   // '―'
													'\u0021',   // '! '
													'\u0022',   // '" '
													'\u0023',   // '# '
													'\u0024',   // '$ '
													'\u0025',   // '% '
													'\u0026',   // '& '
													'\u0027',   // '' '
													'\u0028',   // '( '
													'\u0029',   // ') '
													'\u002A',   // '* '
													'\u002B',   // '+ '
													'\u002C',   // ', '
													'\u002D',   // '- '
													'\u002E',   // '. '
													'\u002F',   // '/ '
													'\u003A',   // ': '
													'\u003B',   // '; '
													'\u003C',   // '<'
													'\u003D',   // '= '
													'\u003E',   // '>'
													'\u003F',   // '? '
													'\u0040',   // '@ '
													'\u005B',   // '[ '
													'\u005D',   // '] '
													'\u005E',   // '^ '
													'\u005F',   // '_ '
													'\u0060',   // '` '
													'\u007B',   // '{ '
													'\u007C',   // '| '
													'\u007D',   // '} '
													'\uFF61',   // '?'
													'\uFF62',   // Halfwidth Left Corner Bracket
													'\uFF63',   // Halfwidth Right Corner Bracket
													'\uFF64',   // 'E
													'\uFF65',    // '￥ '
													'\u00A1',
													'\u00A4',
													'\u00A5'
												};
        #endregion

        #region Constructor
        /// <summary>
        /// Creates a new text filter with default values.
        /// </summary>
        public TextFilter()
        {

        }

        /// <summary>
        /// Creates a new text filter with the specified parameters.
        /// </summary>
        /// <param name="format">Format</param>
        /// <param name="autoConvert">Whether to automatically convert</param>
        ///// <param name="allowSpace">How to allow space</param>
        //public TextFilter(string format, bool autoConvert, AllowSpace allowSpace)
        public TextFilter(string format, bool autoConvert)
        {
            this.Format = format;
            this.AutoConvert = autoConvert;
        }

        #endregion

        #region Properties
        /// <summary>
        /// Gets whether the input is valid.
        /// </summary>
        public bool IsInputValid
        {
            get
            {
                return this._isInputValid;
            }
        }

        /// <summary>
        /// Gets or sets whether the control automatically converts to the proper format according to the format setting.
        /// </summary>
        public bool AutoConvert
        {
            get
            {
                return this._autoConvert;
            }
            set
            {
                this._autoConvert = value;
                this.IsAutoConvert = value;
            }
        }

        /// <summary>
        /// Gets or sets the format string.
        /// </summary>
        public string Format
        {
            get { return this._format; }
            set
            {
                if (this.Format != value)
                {
                    this.ParseFormat(value);
                    this._format = value;
                }
            }
        }

        internal string ExcludeFormat
        {
            get { return this._excludeFormat; }
        }

        internal string IncludeFormat
        {
            get { return this._includeFormat; }
        }

        internal string ExcludeNormalChar
        {
            get { return this._excludeNormalChar; }
        }

        internal string IncludeNormalChar
        {
            get { return this._includeNormalChar; }
        }

        #endregion

        #region Methods

        #region Public Methods
        /// <summary>
        /// Gets whether the filter allow DBCS.
        /// </summary>
        public bool AllowDBCS
        {
            get { return !(((this._allowTypes & CharType.DBCS_All) != 0) ^ this.Include); }
        }

        /// <summary>
        /// Gets whether the filter allow SBCS.
        /// </summary>
        public bool AllowSBCS
        {
            get { return !(((this._allowTypes & CharType.SBCS_All) != 0) ^ this.Include); }
        }

        /// <summary>
        /// Creates a shallow copy of the TextFilter.
        /// </summary>
        public override object Clone()
        {
            TextFilter data = new TextFilter();

            data.AutoConvert = this.AutoConvert;
            data.Format = this.Format;

            return data;
        }

        private void ResetFormatData()
        {
            this._allowTypes = 0;
            this._excludeTypes = 0;
            this._excludeFormat = string.Empty;
            this._includeFormat = string.Empty;
            this._includeNormalChar = string.Empty;
            this._excludeNormalChar = string.Empty;
        }

        private static bool IsNormalChar(char c)
        {
            if (c == '\x5C' || c == '\x5E' || c == '\x3000' || c == '\x20')
            {
                return false;
            }

            for (int i = 0; i < _keywordChars.Count; i++)
            {
                if (c == _keywordChars[i])
                {
                    return false;
                }
            }

            return true;
        }

        private void GetParseFormat(string format, ref CharType charTypes, ref string normalChar)
        {
            if (string.IsNullOrEmpty(format))
            {
                charTypes = CharType.All;
                return;
            }

            if (format == "^")
            {
                charTypes = CharType.All;
                return;
            }

            normalChar = string.Empty;
            for (int i = 0; i < format.Length; i++)
            {
                switch (format[i])
                {
                    case '^':
                        if (i == 0)
                            this.Include = false;
                        else
                            throw new ArgumentException();
                        break;
                    case '\\':
                        if (i == format.Length - 1)
                        {
                            throw new ArgumentException(C1Localizer.GetString("C1Input.TextFilter.FormatInvalid"));
                        }

                        if (IsNormalChar(format[i + 1]))
                        {
                            throw new ArgumentException(C1Localizer.GetString("C1Input.TextFilter.FormatInvalid"));
                        }

                        normalChar = normalChar + format[i + 1];
                        i++;
                        break;
                    case '\xff21':	// DBCS A + SBCS A
                        if ((charTypes & CharType.DBCS_UpperAlphabet) == CharType.DBCS_UpperAlphabet)
                        {
                        }
                        else
                            charTypes |= CharType.DBCS_UpperAlphabet;
                        break;
                    case 'A':
                        if ((charTypes & CharType.SBCS_UpperAlphabet) == CharType.SBCS_UpperAlphabet)
                        {
                        }
                        else
                            charTypes |= CharType.SBCS_UpperAlphabet;
                        break;
                    case '\xff41':	// DBCS a + SBCS a
                        if ((charTypes & CharType.DBCS_LowerAlphabet) == CharType.DBCS_LowerAlphabet)
                        {
                        }
                        else
                            charTypes |= CharType.DBCS_LowerAlphabet;
                        break;
                    case 'a':
                        if ((charTypes & CharType.SBCS_LowerAlphabet) == CharType.SBCS_LowerAlphabet)
                        {
                        }
                        else
                            charTypes |= CharType.SBCS_LowerAlphabet;
                        break;
                    case '\xff2b':	// DBCS K + SBCS K
                        if ((charTypes & CharType.DBCS_Katakana) == CharType.DBCS_Katakana)
                        {
                        }
                        else
                            charTypes |= CharType.DBCS_Katakana;
                        break;
                    case 'K':
                        if ((charTypes & CharType.SBCS_Katakana) == CharType.SBCS_Katakana)
                        {
                        }
                        else
                            charTypes |= CharType.SBCS_Katakana;
                        break;
                    case '\xff19':	// DBCS 9 + SBCS 9
                        if ((charTypes & CharType.DBCS_Number) == CharType.DBCS_Number)
                        {
                        }
                        else
                            charTypes |= CharType.DBCS_Number;
                        break;
                    case '9':
                        if ((charTypes & CharType.SBCS_Number) == CharType.SBCS_Number)
                        {
                        }
                        else
                            charTypes |= CharType.SBCS_Number;
                        break;
                    case '\xff03':	// DBCS # + SBCS #
                        if ((charTypes & CharType.DBCS_NumberSymbol) == CharType.DBCS_NumberSymbol)
                        {
                        }
                        else
                            charTypes |= CharType.DBCS_NumberSymbol;
                        break;
                    case '#':
                        if ((charTypes & CharType.SBCS_NumberSymbol) == CharType.SBCS_NumberSymbol)
                        {
                        }
                        else
                            charTypes |= CharType.SBCS_NumberSymbol;
                        break;
                    case '\xff20':	// DBCS @ + SBCS @
                        if ((charTypes & CharType.DBCS_Symbol) == CharType.DBCS_Symbol)
                        {
                        }
                        else
                            charTypes |= CharType.DBCS_Symbol;
                        break;
                    case '@':
                        if ((charTypes & CharType.SBCS_Symbol) == CharType.SBCS_Symbol)
                        {
                        }
                        else
                            charTypes |= CharType.SBCS_Symbol;
                        break;
                    case '\xff22':	// DBCS B + SBCS B
                        if ((charTypes & CharType.DBCS_Binary) == CharType.DBCS_Binary)
                        {
                        }
                        else
                            charTypes |= CharType.DBCS_Binary;
                        break;
                    case 'B':
                        if ((charTypes & CharType.SBCS_Binary) == CharType.SBCS_Binary)
                        {
                        }
                        else
                            charTypes |= CharType.SBCS_Binary;
                        break;
                    case '\xff38':	// DBCS X + SBCS X
                        if ((charTypes & CharType.DBCS_Hexadecimal) == CharType.DBCS_Hexadecimal)
                        {
                        }
                        else
                            charTypes |= CharType.DBCS_Hexadecimal;
                        break;
                    case 'X':
                        if ((charTypes & CharType.SBCS_Hexadecimal) == CharType.SBCS_Hexadecimal)
                        {
                        }
                        else
                            charTypes |= CharType.SBCS_Hexadecimal;
                        break;
                    case '\xff2a':	// DBCS J
                        if ((charTypes & CharType.DBCS_Hiragana) == CharType.DBCS_Hiragana)
                        {
                        }
                        else
                            charTypes |= CharType.DBCS_Hiragana;
                        break;
                    case '\xff3a':	// DBCS Z
                        if ((charTypes & CharType.DBCS_All) == CharType.DBCS_All)
                        {
                        }
                        else
                            charTypes |= CharType.DBCS_All;
                        break;
                    case 'H':		// SBCS H
                        if ((charTypes & CharType.SBCS_All) == CharType.SBCS_All)
                        {
                        }
                        else
                            charTypes |= CharType.SBCS_All;
                        break;
                    case 'N':
                        if ((charTypes & CharType.Upper_SBCS_Katakana) == CharType.Upper_SBCS_Katakana)
                        {
                        }
                        else
                            charTypes |= CharType.Upper_SBCS_Katakana;
                        break;
                    case '\xff2e':      // DBCS N
                        if ((charTypes & CharType.Upper_DBCS_Katakana) == CharType.Upper_DBCS_Katakana)
                        {
                        }
                        else
                            charTypes |= CharType.Upper_DBCS_Katakana;
                        break;
                    case '\xff27':      // DBCS G
                        if ((charTypes & CharType.Upper_DBCS_Hiragana) == CharType.Upper_DBCS_Hiragana)
                        {
                        }
                        else
                            charTypes |= CharType.Upper_DBCS_Hiragana;
                        break;
                    case '\xff33':      //DBCS S
                        if ((charTypes & CharType.DBCS_Space) == CharType.DBCS_Space)
                        {
                        }
                        else
                            charTypes |= CharType.DBCS_Space;
                        break;
                    case 'S':           //SBCS S
                        if ((charTypes & CharType.SBCS_Space) == CharType.SBCS_Space)
                        {
                        }
                        else
                            charTypes |= CharType.SBCS_Space;
                        break;
                    case '\xff2D':           // DBCS M
                        if ((charTypes & CharType.DBCS_ShiftJIS) == CharType.DBCS_ShiftJIS)
                        {
                        }
                        else
                            charTypes |= CharType.DBCS_ShiftJIS;
                        break;
                    case '\xff29':           // DBCS I
                        if ((charTypes & CharType.DBCS_JISX0208) == CharType.DBCS_JISX0208)
                        {
                        }
                        else
                            charTypes |= CharType.DBCS_JISX0208;
                        break;
                    case '\xff34':      //DBCS T
                        if ((charTypes & CharType.FourBytes) == CharType.FourBytes)
                        {
                        }
                        else
                            charTypes |= CharType.FourBytes;
                        break;
                    case '\xff24':      //DBCS D
                        if ((charTypes & CharType.TwoBytes) == CharType.TwoBytes)
                        {
                        }
                        else
                            charTypes |= CharType.TwoBytes;
                        break;
                    case SYMBOL_Emoji:
                        charTypes |= CharType.Emoji;
                        break;
                    case SYMBOL_IVS:
                        charTypes |= CharType.IVS;
                        break;
                    default:
                        normalChar = normalChar + format[i];
                        break;
                }
            }
        }

        /// <summary>
        /// Parses the format string and get a filter to check character.
        /// </summary>
        /// <param name="format">The format string</param>
        public virtual void ParseFormat(string format)
        {
            if (string.IsNullOrEmpty(format))
            {
                ResetFormatData();
                return;
            }

            if (format == "^")
            {
                ResetFormatData();
                this._excludeTypes = CharType.All;
                this._excludeFormat = "^";
                return;
            }

            SplitFormat(format, ref _includeFormat, ref _excludeFormat);

            GetParseFormat(_includeFormat, ref _allowTypes, ref _includeNormalChar);
            GetParseFormat(_excludeFormat, ref _excludeTypes, ref _excludeNormalChar);
        }

        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>Whether the character is valid</returns>
        public override bool IsValid(char c)
        {
            if (!String.IsNullOrEmpty(_excludeFormat))
            {
                if (!IsExcludeValid(c))
                {
                    if (!String.IsNullOrEmpty(_includeFormat))
                    {
                        return IsIncludeValid(c);
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(_includeFormat))
                {
                    return IsIncludeValid(c);
                }
            }

            return true;
        }


        public override bool IsValid(string strChar)
        {
            if (!String.IsNullOrEmpty(_excludeFormat))
            {
                if (!IsExcludeValid(strChar))
                {
                    if (!String.IsNullOrEmpty(_includeFormat))
                    {
                        return IsIncludeValid(strChar);
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(_includeFormat))
                {
                    return IsIncludeValid(strChar);
                }
            }

            return true;
        }

        private bool IsIncludeValid(char c)
        {
            return IsValidProcess(c, _allowTypes, _includeNormalChar);
        }

        private bool IsIncludeValid(string c)
        {
            return IsValidProcess(c, _allowTypes, _includeNormalChar);
        }

        private bool IsExcludeValid(char c)
        {
            return IsValidProcess(c, _excludeTypes, _excludeNormalChar);
        }

        private bool IsExcludeValid(string c)
        {
            return IsValidProcess(c, _excludeTypes, _excludeNormalChar);
        }

        private bool IsValidProcess(char c, CharType charType, string normalChar)
        {
            if (normalChar.Contains(c.ToString()))
            {
                return true;
            }

            if (c == '\x09' || c == '\x0D' || c == '\x0A')
            {
                return true;
            }

            if (charType == 0)
            {
                return false;
            }

            // Check the character type.
            bool isValid = false;

            if (charType == CharType.All)
            {
                return true;
            }

            if ((charType & CharType.DBCS_ShiftJIS) == CharType.DBCS_ShiftJIS && this.IsShiftJIS(c))
            {
                return true;
            }

            if (CharEx.IsFullWidth(c))
            {
                if ((charType & CharType.DBCS) != CharType.DBCS)
                    return false;

                if ((charType & CharType.DBCS_All) == CharType.DBCS_All && c != '\u3000')
                    isValid = true;
                else if ((charType & CharType.DBCS_LowerAlphabet) == CharType.DBCS_LowerAlphabet && this.IsLower(c))
                    isValid = true;
                else if ((charType & CharType.DBCS_UpperAlphabet) == CharType.DBCS_UpperAlphabet && this.IsUpper(c))
                    isValid = true;
                else if ((charType & CharType.DBCS_Number) == CharType.DBCS_Number && this.IsNumber(c))
                    isValid = true;
                else if ((charType & CharType.DBCS_Binary) == CharType.DBCS_Binary && this.IsBinary(c))
                    isValid = true;
                else if ((charType & CharType.DBCS_Hexadecimal) == CharType.DBCS_Hexadecimal && this.IsHex(c))
                    isValid = true;
                else if ((charType & CharType.DBCS_Symbol) == CharType.DBCS_Symbol && this.IsSymbol(c))
                    isValid = true;
                else if ((charType & CharType.DBCS_NumberSymbol) == CharType.DBCS_NumberSymbol && this.IsNumberSymbol(c))
                    isValid = true;
                else if ((charType & CharType.DBCS_Katakana) == CharType.DBCS_Katakana && this.IsKatakana(c))
                {
                    bool isMixedJPChar = this.IsHiragana(c);
                    if (isMixedJPChar)
                    {
                        if (this.Include)
                        {
                            isValid = true;
                        }
                        else
                        {
                            isValid = ((charType & CharType.DBCS_Hiragana) == CharType.DBCS_Hiragana);
                        }
                    }
                    else
                    {
                        isValid = true;
                    }
                }
                else if ((charType & CharType.DBCS_Hiragana) == CharType.DBCS_Hiragana && this.IsHiragana(c))
                {
                    bool isMixedJPChar = this.IsKatakana(c);
                    if (isMixedJPChar)
                    {
                        if (this.Include)
                        {
                            isValid = true;
                        }
                        else
                        {
                            isValid = ((charType & CharType.DBCS_Katakana) == CharType.DBCS_Katakana);
                        }
                    }
                    else
                    {
                        isValid = true;
                    }
                }
                else if ((charType & CharType.Upper_DBCS_Katakana) == CharType.Upper_DBCS_Katakana && this.IsKatakana(c) && this.IsUpperKana(c))
                    isValid = true;
                else if ((charType & CharType.Upper_DBCS_Hiragana) == CharType.Upper_DBCS_Hiragana && this.IsHiragana(c) && this.IsUpperKana(c))
                    isValid = true;
                else if ((charType & CharType.DBCS_Space) == CharType.DBCS_Space && this.IsFormatSpace(c))
                    isValid = true;
                else if ((charType & CharType.FourBytes) == CharType.FourBytes && this.IsFourBytes(c))
                    isValid = true;
                else if ((charType & CharType.TwoBytes) == CharType.TwoBytes && !this.IsFourBytes(c) && c != '\u3000')
                    isValid = true;
                else if ((charType & CharType.DBCS_JISX0208) == CharType.DBCS_JISX0208 && this.IsJISX0208(c))
                    isValid = true;
            }
            else
            {
                if ((charType & CharType.SBCS) != CharType.SBCS)
                    return false;
                if ((charType & CharType.SBCS_All) == CharType.SBCS_All && c != '\x20')
                    isValid = true;
                else if ((charType & CharType.SBCS_LowerAlphabet) == CharType.SBCS_LowerAlphabet && this.IsLower(c))
                    isValid = true;
                else if ((charType & CharType.SBCS_UpperAlphabet) == CharType.SBCS_UpperAlphabet && this.IsUpper(c))
                    isValid = true;
                else if ((charType & CharType.SBCS_Number) == CharType.SBCS_Number && this.IsNumber(c))
                    isValid = true;
                else if ((charType & CharType.SBCS_Binary) == CharType.SBCS_Binary && this.IsBinary(c))
                    isValid = true;
                else if ((charType & CharType.SBCS_Hexadecimal) == CharType.SBCS_Hexadecimal && this.IsHex(c))
                    isValid = true;
                else if ((charType & CharType.SBCS_Symbol) == CharType.SBCS_Symbol && this.IsSymbol(c))
                    isValid = true;
                else if ((charType & CharType.SBCS_NumberSymbol) == CharType.SBCS_NumberSymbol && this.IsNumberSymbol(c))
                    isValid = true;
                else if ((charType & CharType.SBCS_Katakana) == CharType.SBCS_Katakana && this.IsKatakana(c))
                    isValid = true;
                else if ((charType & CharType.Upper_SBCS_Katakana) == CharType.Upper_SBCS_Katakana && this.IsKatakana(c) && this.IsUpperKana(c))
                    isValid = true;
                else if ((charType & CharType.SBCS_Space) == CharType.SBCS_Space && this.IsFormatSpace(c))
                    isValid = true;
            }

            return isValid;
        }

        private bool IsValidProcess(string c, CharType charType, string normalChar)
        {
            if (normalChar.Contains(c))
            {
                return true;
            }

            if (charType == 0)
            {
                return false;
            }

            if (charType == CharType.All)
            {
                return true;
            }

            if ((charType & CharType.Emoji) == CharType.Emoji && this.IsEmoji(c))
            {
                return true;
            }

            if ((charType & CharType.IVS) == CharType.IVS && this.IsIVSChar(c))
            {
                return true;
            }

            if ((charType & CharType.FourBytes) == CharType.FourBytes && c.Length >= 2)
            {
                for (int i = 0; i < c.Length; i++)
                {
                    if (char.IsSurrogate(c[i]))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Formats the specified text using the filter.
        /// </summary>
        /// <param name="value">The specified text</param>
        /// <returns>The formated result</returns>
        public string FilterTextByFormat(string value)
        {
            if (value == null)
                return "";

            StringBuilder newText = new StringBuilder();
            int len = value.Length;

            for (int i = 0; i < len; )
            {
                string result = this.Check(value, ref i);
                if (result != string.Empty)
                {
                    newText.Append(result);
                }
                else
                {
                    string temp = value.Substring(i);
                    temp = Utility.Substring(temp, 0, 1);
                    this._isInputValid = false;
                    i += temp.Length;
                }
            }
            return newText.ToString();
        }

        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="text">String to be checked</param>
        /// <param name="index">Character index in the string</param>
        /// <returns>Converted string</returns>
        public override string Check(string text, ref int index)
        {
#if DEBUG
            if (string.IsNullOrEmpty(text) || index < 0 || index >= text.Length)
                throw new ArgumentNullException();
#endif
            string tempCheckingStr = text.Substring(index);
            string checkingStr = Utility.Substring(tempCheckingStr, 0, 1);
            bool isValid = checkingStr.Length == 1 ? this.IsValid(text[index]) : this.IsValid(checkingStr);

            if (isValid)
            {
                text = checkingStr;
                index += checkingStr.Length;
                return text;
            }

            if (this.IsAutoConvert && (checkingStr.Length == 1 || checkingStr.Length > 2))
            {
                return this.Convert(text, ref index);
            }

            return string.Empty;
        }

        #endregion end of Public Methods

        #region Private Methods

        private void SplitFormat(string format, ref string includeFormat, ref string excludeFormat)
        {
            if (string.IsNullOrEmpty(format))
            {
                return;
            }

            int caretCount = 0;

            StringBuilder includeFormatBuilder = new StringBuilder();
            StringBuilder excludeFormatBuilder = new StringBuilder();

            for (int i = 0; i < format.Length; i++)
            {
                char currentChar = format[i];

                if (currentChar == '\x5C') //   '\\'
                {
                    if (i != format.Length - 1)
                    {
                        if (caretCount == 0)
                        {
                            includeFormatBuilder.Append(currentChar);
                            includeFormatBuilder.Append(format[i + 1]);
                        }
                        else
                        {
                            excludeFormatBuilder.Append(currentChar);
                            excludeFormatBuilder.Append(format[i + 1]);
                        }
                        i++;
                    }
                    else
                    {
                        // such as @"^\".
                        throw new ArgumentException(C1Localizer.GetString("C1Input.TextFilter.FormatInvalid"));
                    }
                }
                else if (currentChar == '\x5E') // '^'
                {
                    caretCount++;

                    if (caretCount > 1)
                    {
                        throw new ArgumentException(C1Localizer.GetString("C1Input.TextFilter.FormatInvalid"));
                    }
                }
                else
                {
                    if (caretCount == 0)
                    {
                        includeFormatBuilder.Append(currentChar);
                    }
                    else
                    {
                        excludeFormatBuilder.Append(currentChar);
                    }
                }
            }

            // exist exclude format
            //
            if (caretCount == 1)
            {
                excludeFormatBuilder.Insert(0, '\x5E'); // '^'
            }

            includeFormat = includeFormatBuilder.ToString();
            excludeFormat = excludeFormatBuilder.ToString();
        }

        private bool IsFormatSpace(char c)
        {
            if (c == '\x20' || c == '\u3000')
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsFourBytes(char c)
        {
            return Char.IsSurrogate(c);
        }

        private bool IsShiftJIS(char c)
        {
            return CharEx.IsShiftJIS(c);
        }

        private bool IsJISX0208(char c)
        {
            return CharEx.IsJISX0208(c);
        }

        private bool IsUpper(char c)
        {
            return CharEx.IsUpper(c);
        }

        private bool IsLower(char c)
        {
            return CharEx.IsLower(c);
        }

        private bool IsNumber(char c)
        {
            return CharEx.IsDigit(c);
        }

        private bool IsBinary(char c)
        {
            c = CharEx.IsFullWidth(c) ? CharEx.ToHalfWidth(c) : c;
            return (c == '0' || c == '1');
        }

        private bool IsHex(char c)
        {
            c = CharEx.IsFullWidth(c) ? CharEx.ToHalfWidth(c) : c;
            return (c == 'A' || c == 'B' || c == 'C' || c == 'D' || c == 'E' || c == 'F' ||
                c == 'a' || c == 'b' || c == 'c' || c == 'd' || c == 'e' || c == 'f' ||
                CharEx.IsDigit(c));
        }

        private bool IsSymbol(char c)
        {
            for (int i = 0; i < this._symbolArray.Length; i++)
            {
                if (c == this._symbolArray[i])
                {
                    return true;
                }
            }
            return false;
        }

        private bool IsNumberSymbol(char c)
        {
            c = CharEx.IsFullWidth(c) ? CharEx.ToHalfWidth(c) : c;
            return (CharEx.IsDigit(c) || c == '+' || c == '-' || c == '$' || c == '%' || c == '\\' || c == ',' || c == '.');
        }

        private bool IsKatakana(char c)
        {
            return CharEx.IsKatakana(c);
        }

        private bool IsHiragana(char c)
        {
            return CharEx.IsHiragana(c);
        }

        private bool IsDBCS(char c)
        {
            return CharEx.IsFullWidth(c);
        }

        private bool IsSBCS(char c)
        {
            return !CharEx.IsFullWidth(c);
        }

        private bool IsIVSChar(string str)
        {
            return false;
            //int isvElementLength;
            //return IVSCharHelper.IsIVSElement(str, 0, out isvElementLength);
        }

        private bool IsEmoji(string str)
        {
            return false;
            //int emojiCharLength;
            //return EmojiHelper.IsEmoji(str, 0, out emojiCharLength);
        }

        /// <summary>
        /// Determines whether the specified character is a upper case Katakana character.
        /// </summary>
        /// <param name="c"><b>Char</b> that contains the character to be checked</param>
        /// <returns>
        /// If the character is a upper case katakana character, return <b>true</b>, otherwise, return <b>false</b>
        /// </returns>
        private bool IsUpperKana(char c)
        {
            return CharEx.IsUpperKana(c);
        }
        #endregion end of Private Methods

        #region Protected Methods
        /// <summary>
        /// Converts the character at the specified index.
        /// </summary>
        /// <param name="text">String to be checked</param>
        /// <param name="index">Character index in the string</param>
        /// <returns>Converted string</returns>
        protected override string Convert(string text, ref int index)
        {
            char c = text[index];

            bool isValid = false;

            #region Convert between ivs and normal char.
            {
                //int charLength = 1;
                //string tempStr = text.Substring(index);
                //string ivs = Utility.Substring(tempStr, 0, 1);
                //string result = IVSCharHelper.ConvertedWithIVS(ivs, 0, ref charLength);
                //if (result.Length > 0)
                //{
                //    isValid = result.Length == 1 ? this.IsValid(result[0]) : this.IsValid(result);
                //    if (isValid)
                //    {
                //        index += ivs.Length;
                //        return result;
                //    }
                //}
            }

            #region Convert between upper and lower alphabet automatically.
            if (CharEx.IsAlphabet(c))
            {
                char r = CharEx.IsLower(c) ? char.ToUpper(c) : char.ToLower(c);
                isValid = this.IsValid(r);
                if (isValid)
                {
                    index++;
                    return new string(r, 1);
                }
                c = CharEx.IsFullWidth(c) ? CharEx.ToHalfWidth(c) : CharEx.ToFullWidth(c);
                isValid = this.IsValid(c);
                if (isValid)
                {
                    index++;
                    return new string(c, 1);
                }
                r = CharEx.IsFullWidth(r) ? CharEx.ToHalfWidth(r) : CharEx.ToFullWidth(r);
                isValid = this.IsValid(r);
                if (isValid)
                {
                    index++;
                    return new string(r, 1);
                }
                return string.Empty;
            }
            #endregion
            #region Convert from Hiragana to other styles automatically.
            if (CharEx.IsHiragana(c))
            {
                // Large < - > Small                
                if (CharEx.IsLowerKana(c))
                {
                    char u = CharEx.ToUpperKana(c);
                    isValid = this.IsValid(u);
                    if (isValid)
                    {
                        index++;
                        return new string(u, 1);
                    }
                }
                else if (CharEx.HasLowerKana(c))
                {
                    char l = CharEx.ToLowerKana(c);
                    isValid = this.IsValid(l);
                    if (isValid)
                    {
                        index++;
                        return new string(l, 1);
                    }
                }
                // Hiragana to DBCS Katakana
                char r = CharEx.ToKatakana(c);
                isValid = this.IsValid(r);
                if (isValid)
                {
                    index++;
                    return new string(r, 1);
                }

                if (CharEx.IsLowerKana(r))
                {
                    char u = CharEx.ToUpperKana(r);
                    isValid = this.IsValid(u);
                    if (isValid)
                    {
                        index++;
                        return new string(u, 1);
                    }
                }
                else if (CharEx.HasLowerKana(r))
                {
                    char l = CharEx.ToLowerKana(r);
                    isValid = this.IsValid(l);
                    if (isValid)
                    {
                        index++;
                        return new string(l, 1);
                    }
                }
                // Hiragana to SBCS Katakana
                char[] chars = CharEx.ToHalfWidthEx(r);
                isValid = this.IsValid(chars[0]);
                if (isValid)
                {
                    index++;
                    return new string(chars);
                }

                if (CharEx.IsLowerKana(chars[0]))
                {
                    chars[0] = CharEx.ToUpperKana(chars[0]);
                    isValid = this.IsValid(chars[0]);
                    if (isValid)
                    {
                        index++;
                        return new string(chars);
                    }
                }
                else if (CharEx.HasLowerKana(chars[0]))
                {
                    chars[0] = CharEx.ToLowerKana(chars[0]);
                    isValid = this.IsValid(chars[0]);
                    if (isValid)
                    {
                        index++;
                        return new string(chars);
                    }
                }
                return string.Empty;
            }
            #endregion
            #region Convert from Katakana to Hiragana (or DBCS <-> SBCS)automatically.
            if (CharEx.IsKatakana(c))
            {
                // Large < - > Small                
                if (CharEx.IsLowerKana(c))
                {
                    char u = CharEx.ToUpperKana(c);
                    isValid = this.IsValid(u);
                    if (isValid)
                    {
                        index++;
                        return new string(u, 1);
                    }
                }
                else if (CharEx.HasLowerKana(c))
                {
                    char l = CharEx.ToLowerKana(c);
                    isValid = this.IsValid(l);
                    if (isValid)
                    {
                        index++;
                        return new string(l, 1);
                    }
                }
                // DBCS < - > SBCS
                bool processedAll = false;
                char r = c;
                if (CharEx.IsFullWidth(c))
                {
                    char[] newChars = CharEx.ToHalfWidthEx(c);

                    if (newChars.Length > 0)
                    {
                        isValid = this.IsValid(newChars[0]);
                        if (isValid)
                        {
                            index++;
                            return new string(newChars);
                        }
                    }
                    if (CharEx.IsLowerKana(newChars[0]))
                    {
                        newChars[0] = CharEx.ToUpperKana(newChars[0]);
                        isValid = this.IsValid(newChars[0]);
                        if (isValid)
                        {
                            index++;
                            return new string(newChars);
                        }
                    }
                    else if (CharEx.HasLowerKana(newChars[0]))
                    {
                        newChars[0] = CharEx.ToLowerKana(newChars[0]);
                        isValid = this.IsValid(newChars[0]);
                        if (isValid)
                        {
                            index++;
                            return new string(newChars);
                        }
                    }
                }
                else
                {
                    if ((index + 1) < text.Length)
                        r = CharEx.ToFullWidth(out processedAll, new char[] { c, text[index + 1] });
                    else
                        r = CharEx.ToFullWidth(c);

                    if (!CharEx.IsKatakana(r)) // ***********
                        return string.Empty;

                    isValid = this.IsValid(r);
                    if (isValid)
                    {
                        index++;
                        if (processedAll)
                            index++;
                        return new string(r, 1);
                    }

                    if (CharEx.IsLowerKana(r))
                    {
                        char u = CharEx.ToUpperKana(r);
                        isValid = this.IsValid(u);
                        if (isValid)
                        {
                            index++;
                            return new string(u, 1);
                        }
                    }
                    else if (CharEx.HasLowerKana(r))
                    {
                        char l = CharEx.ToLowerKana(r);
                        isValid = this.IsValid(l);
                        if (isValid)
                        {
                            index++;
                            return new string(l, 1);
                        }
                    }

                }

                r = CharEx.ToHiragana(r);
                isValid = this.IsValid(r);
                if (isValid)
                {
                    index++;
                    if (processedAll)
                        index++;
                    if (r == '\u3094')
                    {
                        if (processedAll)
                        {
                            return (new string('\u3046', 1) + new string('\u309B', 1));
                        }
                        else
                        {
                            index--;
                            return string.Empty;
                        }
                    }
                    return new string(r, 1);
                }
                if (CharEx.IsLowerKana(r))
                {
                    char u = CharEx.ToUpperKana(r);
                    isValid = this.IsValid(u);
                    if (isValid)
                    {
                        index++;
                        return new string(u, 1);
                    }
                }
                else if (CharEx.HasLowerKana(r))
                {
                    char l = CharEx.ToLowerKana(r);
                    isValid = this.IsValid(l);
                    if (isValid)
                    {
                        index++;
                        return new string(l, 1);
                    }
                }
            }
            #endregion
            #region Convert between DBCS and SBCS automatically.
            c = CharEx.IsFullWidth(c) ? CharEx.ToHalfWidth(c) : CharEx.ToFullWidth(c);

            isValid = this.IsValid(c);
            if (isValid)
            {
                index++;
                return new string(c, 1);
            }
            #endregion
            return string.Empty;
        }

        #endregion end of Protected Methods

        #endregion end of Methods
    }
    #endregion
}

