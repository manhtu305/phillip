﻿using System;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{
    internal class FieldCollection : System.Collections.CollectionBase
    {
        #region Members
        private object _owner;
        private char _promptChar = '_';
        #endregion end of Members.

        #region Constructor
        /// <summary>
        /// Creates a new collection of fields with default values.
        /// </summary>
        public FieldCollection()
        {
        }

        /// <summary>
        /// Creates a new collection of fields for the specified owner control.
        /// </summary>
        /// <param name="owner">Owner control</param>
        public FieldCollection(object owner)
        {
            this._owner = owner;
        }
        #endregion end of Constructor.

        #region Properties
        /// <summary>
        /// Gets or sets the field data, which hold the field collection.
        /// </summary>
        internal protected virtual object Owner
        {
            get
            {
                return this._owner;
            }
            set
            {
                this._owner = value;
            }
        }

        /// <summary>
        /// Gets or sets the prompt character of the field, for filling field text
        /// when the text length is less than the minimum length of this field.
        /// </summary>
        public virtual char PromptChar
        {
            get
            {
                return this._promptChar;
            }
            set
            {
                if (value == (char)0)
                {
                    value = ' ';
                }

                this._promptChar = value;
            }
        }

        /// <summary>
        /// Gets the length, in number of characters.
        /// </summary>
        public int Length
        {
            get
            {
                int length = 0;
                for (int i = 0; i < this.Count; i++)
                {
                    Field field = this[i];
                    length += field.Length > field.MinLength ? field.Length : field.MinLength;
                }

                return length;
            }
        }

        /// <summary>
        /// Gets the text string.
        /// </summary>
        public virtual string Text
        {
            get
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Gets the field object.
        /// </summary>
        public Field this[int index]
        {
            get
            {
                return (Field)base.InnerList[index];
            }
        }

        #endregion end of Properties.

        #region Methods
        /// <summary>
        /// Adds a field to the collection of fields.
        /// </summary>
        /// <param name="field">Field to add to FieldCollection</param>
        /// <returns>Index of the added field</returns>
        public int Add(Field field)
        {
            if (field == null)
            {
                throw new ArgumentNullException("field");
            }

            field.InternalAssignOwner(this);

            return base.InnerList.Add(field);
        }

        /// <summary>
        /// Gets the filling string with the specified fill character.
        /// </summary>
        /// <param name="fillChar">Character to fill the string</param>
        /// <returns>Full string with literals and fill chararacter</returns>
        /// <remarks>This method gets the text while nothing is input</remarks>
        public virtual string GetFillingString(char fillChar)
        {
            return String.Empty;
        }

        /// <summary>
        /// Gets the field index according to the character index.
        /// </summary>
        /// <param name="position">Character index</param>
        /// <param name="offset">Offset, the character index in the field</param>
        /// <returns>Index of the field</returns>
        public virtual int GetFieldIndex(int position, out int offset)
        {
#if DEBUG
            if (position < 0 || position > this.Length)
            {
                throw new ArgumentOutOfRangeException("position");
            }
#endif
            int length = 0;

            for (int i = 0; i < this.Count; i++)
            {
                Field field = this[i];
                int fieldLength = field.Length > field.MinLength ? field.Length : field.MinLength;

                if ((position == length && fieldLength == 0)
                    || (position >= length && position < length + fieldLength))
                {
                    offset = position - length;
                    return i;
                }

                length += fieldLength;
            }

            // It is in end of fields.
            if (position == length)
            {
                Field field = this[this.Count - 1];
                offset = field.Length > field.MinLength ? field.Length : field.MinLength;
                return this.Count - 1;
            }

            throw new ArgumentOutOfRangeException("position");
        }

        /// <summary>
        /// Sets the text to this field.
        /// </summary>
        /// <param name="text">Text string</param>
        /// <param name="includePrompt">Whether to include a prompt</param>
        public virtual void SetText(string text, bool includePrompt)
        {
        }
        #endregion end of Method.
    }
}
