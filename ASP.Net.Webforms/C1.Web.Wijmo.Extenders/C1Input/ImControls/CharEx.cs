﻿using System;
using System.Globalization;
using System.Text;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{
    #region CharType Enumuration
    /// <summary>
    /// Defines the category of a Unicode character.  
    /// </summary>
    [Flags]
    internal enum CharType
    {
        /// <summary>
        /// Indicates that the character is not of a particular category.
        /// </summary>
        OtherChar = 0x0000,
        /// <summary>
        /// Indicates that the character is a control code.
        /// </summary>
        Control = 0x0001,
        /// <summary>
        /// Indicates that the character is a numeric digit.
        /// </summary>
        Numeric = 0x0002,
        /// <summary>
        /// Indicates that the character is a mathematical symbol.
        /// </summary>
        MathSymbol = 0x0003,
        /// <summary>
        /// Indicates that the character is a symbol.
        /// </summary>
        Symbol = 0x0004,
        /// <summary>
        /// Indicates that the character is a punctuation. ( Open &amp; Close ).
        /// </summary>
        Punctuation = 0x0005,
        /// <summary>
        /// Indicates that the character is a space character.
        /// </summary>
        Space = 0x0006,
        /// <summary>
        /// Indicates that the character is an upper case letter.
        /// </summary>
        UpperCase = 0x0007,
        /// <summary>
        /// Indicates that the character is a lower case letter.
        /// </summary>
        LowerCase = 0x0008,
        /// <summary>
        /// Indicates that the character is a Japanese Katakana character.
        /// </summary>
        Katakana = 0x0009,
        /// <summary>
        /// Indicates that the character is a Japanese Hiragana character.
        /// </summary>
        Hiragana = 0x000a,
        /// <summary>
        /// Indicates that the character is a CJK punctuation.
        /// </summary>
        FarEastPunctation = 0x000b,
        /// <summary>
        /// Indicates that the character is a Hangal character.
        /// </summary>
        Hangul = 0x000c,
        /// <summary>
        /// Indicates that the character is of full width.
        /// </summary>
        FullWidth = 0x8000,
    }
    #endregion end of CharType Enumuration.

    /// <summary>
    /// Represents a Unicode character and extention to the standard
    /// .NET Char type. The purpose for this is to provide better 
    /// CJK (Chinese, Japanese, Korean ) character support.
    /// </summary>
    /// <remarks>
    ///	The CharEx type represents Unicode character (multibyte) with 
    ///	values ranging from '\u0000' through '\uffff'.
    /// </remarks>
    internal struct CharEx : IComparable
    {
        #region Members
        #region Constants
        #region Full/HalfWidth characters (different cultures)
        //		private const int ANSISTART			 = 0x0000;
        private const int ANSIEND = 0x00ff;
        //		private const int BOTHWIDTHSTART	 = 0xff00;
        //		private const int BOTHWIDTHEND		 = 0xffef;
        //		private const int FULLALPHASTART	 = 0xff01;
        //		private const int FULLUPPERSTART	 = 0xff21;
        //		private const int FULLUPPEREND		 = 0xff3a;
        //		private const int FULLALPHAEND		 = 0xff5e;
        //		private const int CJKHALFSYMBOLSTART = 0xff61;
        //		private const int CJKHALFSYMBOLEND	 = 0xff64;
        private const int KANAHALFSTART = 0xff65;
        //		private const int KANAHALFEND		 = 0xff9f;
        //		private const int HANGULHALFSTART	 = 0xffa0;
        //		private const int HANGULHALFEND		 = 0xffdc;
        //		private const int FULLSYMBOLSTART	 = 0xffe0;
        //		private const int FULLSYMBOLEND		 = 0xffe6;
        //		private const int HALFPUNCTSTART	 = 0xffe8;
        //		private const int HALFPUNCTEND		 = 0xffee;
        #endregion end of Full/HalfWidth characters (different cultures).

        #region Voiced characters (Japanese)
        private const char KATAKANA_VOICED = '\uff9e';
        private const char KATAKANA_SEMIVOICED = '\uff9f';
        #endregion end of Voiced characters (Japanese).

        #region Others
        public const char Tab = '\u0009';
        public const char Space = '\u0020';
        #endregion end of Others.
        #endregion end of Constants.

        #region Static Members(tables)
        #region Character Groups
        /// <summary>
        /// Character groups (character codes) based on Unicode 3.1.
        /// </summary>
        private static readonly char[] Charstarts =
				{
					'\u0000',			// Basic Latin
					'\u0080',			// Latin 1 Supplement
					'\u0100',			// Latin Extended - A
					'\u0180',			// Latin Extended - B
					'\u0250',			// IPA extensions
					'\u02b0',			// Spacing Modifier Letters
					'\u0300',			// Combining Diacritical Marks
					'\u0370',			// Greek
					'\u0400',			// Cyrillic
					'\u0530',			// Armenian
					'\u0590',			// Hebrew
					'\u0600',			// Arabic
					'\u0700',			// Syriac
					'\u0780',			// Thaana
					'\u0900',			// Devanagari
					'\u0980',			// Bengali
					'\u0a00',			// Gurmukhi
					'\u0a80',			// Gujarati
					'\u0b00',			// Oriya
					'\u0b80',			// Tamil
					'\u0c00',			// Telugu
					'\u0c80',			// Kannada
					'\u0d00',			// Malayalam
					'\u0d80',			// Sinhala
					'\u0e00',			// Thai
					'\u0e80',			// Lao
					'\u0f00',			// Tibetan
					'\u1000',			// Myanmar
					'\u10a0',			// Georgian
					'\u1100',			// Hangal Jamo
					'\u1200',			// Ethiopic
					'\u13a0',			// Cherokee
					'\u1400',			// Unified Canadian Aboriginal Syllabic
					'\u1680',			// Ogham
					'\u16a0',			// Runic
					'\u1780',			// Khmer
					'\u1800',			// Mongolian
					'\u1e00',			// Latin Extended Additional
					'\u1f00',			// Greek Extended
					'\u2000',			// General Punctuation
					'\u2070',			// Superscripts and Subscripts
					'\u20a0',			// Currency Symbols
					'\u20d0',			// Combining Marks for Symbols
					'\u2100',			// Letter like Symbols
					'\u2150',			// Number Forms
					'\u2190',			// Arrows
					'\u2200',			// Mathematical operators
					'\u2300',			// Miscellaneous Technical
					'\u2400',			// Control Pictures
					'\u2440',			// Optical Character Recognition
					'\u2460',			// Enclosed AlphaNumerics
					'\u2500',			// Box drawing
					'\u2580',			// Block Elements
					'\u25a0',			// Geometric Shapes
					'\u2600',			// Miscellaneous Symbols
					'\u2700',			// Dingbats
					'\u2800',			// Braille Patterns
					'\u2e80',			// CJK Radicals Supplement
					'\u2f00',			// Kangxi Radicals
					'\u2ff0',			// Ideographic Description Characters
					'\u3000',			// CJK Symbols and Punctuations
					'\u3040',			// Hiragana
					'\u30a0',			// Katakana
					'\u3100',			// Bopomofo
					'\u3130',			// Hangal Compatibility Jamo
					'\u3190',			// Kanbun
					'\u31a0',			// Bopomofo Extended
					'\u3200',			// Enclosed CJK Letters and Months
					'\u3300',			// CJK Compatiblity
					'\u3400',			// CJK Unified Ideographs Extension
					'\u4e00',			// CJK Undified Ideographs
					'\ua000',			// Yi Syllables
					'\ua490',			// Yi Radicals
					'\uac00',			// Hangul Syllables
					'\uf900',			// CJK Compatible Ideographs
					'\ufb00',			// Alphabetic Presentation Forms
					'\ufb50',			// Arabic Presentation Forms A
					'\ufe20',			// Combining Half Marks
					'\ufe30',			// CJK Compatiblity Form
					'\ufe50',			// Small Form Variants
					'\ufe70',			// Arabi Presentation Forms B
					'\uff00',			// Halfwidth and Fullwidth Forms
					'\ufff0'            // Specials
				};
        #endregion end of Character Groups.

        #region Character Block Categories
        /// <summary>
        /// Character blocks categorized base on the Unicode standard.
        /// </summary>
        internal enum Blocks
        {
            BASIC_LATIN,
            LATIN_1_SUPPLEMENT,
            LATIN_EXTENDED_A,
            LATIN_EXTENDED_B,
            IPA_EXTENSIONS,
            SPACING_MODIFIER_LETTERS,
            COMBINING_DIACRITICAL_MARKS,
            GREEK,
            CYRILLIC,
            ARMENIAN,
            HEBREW,
            ARABIC,
            SYRIAC,
            THAANA,
            DEVANAGARI,
            BENGALI,
            GURMUKHI,
            GUJARATI,
            ORIYA,
            TAMIL,
            TELUGU,
            KANNADA,
            MALAYALAM,
            SINHALA,
            THAI,
            LAO,
            TIBETAN,
            MYANMAR,
            GEORGIAN,
            HANGUL_JAMO,
            ETHIOPIC,
            CHEROKEE,
            UNIFIED_CANADIAN_ABORIGINAL_SYLLABIC,
            OGHAM,
            RUNIC,
            KUMER,
            MONGOLIAN,
            LATIN_EXTENDED_ADDITIONAL,
            GREEK_EXTENDED,
            GENERAL_PUNCTUATION,
            SUPERSCRIPTS_AND_SUBSCRIPTS,
            CURRENCY_SYMBOLS,
            COMBINING_MARKS_FOR_SYMBOLS,
            LETTERLIKE_SYMBOLS,
            NUMBER_FORMS,
            ARROWS,
            MATHEMATICAL_OPERATORS,
            MISCELLANEOUS_TECHNICAL,
            CONTROL_PICTURES,
            OPTICAL_CHARACTER_RECOGNITION,
            ENCLOSED_ALPHANUMERICS,
            BOX_DRAWING,
            BLOCK_ELEMENTS,
            GEOMETRIC_SHAPES,
            MISCELLANEOUS_SYMBOLS,
            DINGBATS,
            BRAILLE_PATTERNS,
            CJK_RADICALS_SUPPLEMENT,
            KANGXI_RADICALS,
            IDEOGRAPHIC_DESCRIPTION_CHARACTERS,
            CJK_SYMBOLS_AND_PUNCTUATION,
            HIRAGANA,
            KATAKANA,
            BOPOMOFO,
            HANGUL_COMPATIBILITY_JAMO,
            KANBUN,
            BOPOMOFO_EXTENDED,
            ENCLOSED_CJK_LETTERS_AND_MONTHS,
            CJK_COMPATIBILITY,
            CJK_UNIFIED_IDEOGRAPHS_EXTENSION,
            CJK_UNIFIED_IDEOGRAPHS,
            YI_SYLLABLES,
            YI_RADICALS,
            HANGUL_SYLLABLES,
            CJK_COMPATIBILITY_IDEOGRAPHS,
            ALPHABETIC_PRESENTATION_FORMS,
            ARABIC_PRESENTATION_FORMS_A,
            COMBINING_HALF_MARKS,
            CJK_COMPATIBILITY_FORMS,
            SMALL_FORM_VARIANTS,
            ARABIC_PRESENTATION_FORMS_B,
            HALFWIDTH_AND_FULLWIDTH_FORMS,
            SPECIALS,
        };
        #endregion end of Character Block Categories.

        #region Multi width character block mapping table
        /// <summary>
        /// Table of multi-width character blocks.
        /// </summary>
        private static readonly char[] Fullhalfblocks = 
			{
				'\uff01',			// Symbols
				'\uff10',			// Numbers
				'\uff1a',			// Symbols
				'\uff21',			// Uppercase
				'\uff3b',			// Symbols
				'\uff41',			// Lowercase
				'\uff5b',			// Symbols
				'\uff61',			// CJK Halfwidth Punctuation
				'\uff65',			// Halfwidth Katakana
				'\uffa0',			// Halfwidth Hangal
				'\uffe0',			// Fullwidth symbol variants
				'\uffe8'            // Halfwidth symbol variants
			};

        /// <summary>
        /// Type of multi-width characters.
        /// </summary>
        private static readonly CharType[] Mwtable = 
			{
				CharType.Symbol | CharType.FullWidth,
				CharType.Numeric | CharType.FullWidth,
				CharType.Symbol | CharType.FullWidth,
				CharType.UpperCase | CharType.FullWidth,
				CharType.Symbol | CharType.FullWidth,
				CharType.LowerCase | CharType.FullWidth,
				CharType.Symbol | CharType.FullWidth,
				CharType.FarEastPunctation,
				CharType.Katakana,
				CharType.Hangul,
				CharType.Symbol | CharType.FullWidth,
				CharType.Symbol
			};
        #endregion end of Multi width character block mapping table.

        #region Half width Katakana map table
        /// <summary>
        /// Mapping table of full width Katakana.
        /// </summary>
        private static readonly char[] Halfkana = 
		{
			'\u30fb', '\u30f2',									// ., small o
			'\u30a1', '\u30a3', '\u30a5', '\u30a7', '\u30a9',	// small a
			'\u30e3', '\u30e5', '\u30e7',						// small ya
			'\u30c3', '\u30fc',									// small tu, -
			'\u30a2', '\u30a4', '\u30a6', '\u30a8', '\u30aa',	// a
			'\u30ab', '\u30ad', '\u30af', '\u30b1', '\u30b3',	// ka
			'\u30b5', '\u30b7', '\u30b9', '\u30bb', '\u30bd',	// sa
			'\u30bf', '\u30c1', '\u30c4', '\u30c6', '\u30c8',	// ta
			'\u30ca', '\u30cb', '\u30cc', '\u30cd', '\u30ce',	// na
			'\u30cf', '\u30d2', '\u30d5', '\u30d8', '\u30db',	// ha
			'\u30de', '\u30df', '\u30e0', '\u30e1', '\u30e2',	// ma
			'\u30e4', '\u30e6', '\u30e8',						// ya
			'\u30e9', '\u30ea', '\u30eb', '\u30ec', '\u30ed',	// ra
			'\u30ef', '\u30f3',									// wa
			'\u3099', '\u309a'                                  // daku-on
		};
        #endregion end of Half width Katakana map table.

        #region Full width Katakana map table
        /// <summary>
        /// Mapping table of half-width Katakana.
        /// </summary>
        private static readonly char[] Fullkana = 
		{
			'\uff67', '\uff71', '\uff68', '\uff72', '\uff69', '\uff73',		//a
			'\uff6a', '\uff74', '\uff6b', '\uff75',
			'\uff76', '\uff76', '\uff77', '\uff77', '\uff78', '\uff78',		// ka
			'\uff79', '\uff79', '\uff7a', '\uff7a',
			'\uff7b', '\uff7b', '\uff7c', '\uff7c', '\uff7d', '\uff7d',		// sa
			'\uff7e', '\uff7e', '\uff7f', '\uff7f',
			'\uff80', '\uff80', '\uff81', '\uff81', '\uff6f', '\uff82',		// ta
			'\uff82', '\uff83', '\uff83', '\uff84', '\uff84',
			'\uff85', '\uff86', '\uff87', '\uff88', '\uff89',				// na
			'\uff8a', '\uff8a', '\uff8a', '\uff8b', '\uff8b', '\uff8b',		// ha
			'\uff8c', '\uff8c', '\uff8c', '\uff8d', '\uff8d', '\uff8d',
			'\uff8e', '\uff8e', '\uff8e',
			'\uff8f', '\uff90', '\uff91', '\uff92', '\uff93',				// ma
			'\uff6c', '\uff94', '\uff6d', '\uff95', '\uff6e', '\uff96',		// ya
			'\uff97', '\uff98', '\uff99', '\uff9a', '\uff9b',				// ra
			'\uff9c', '\uff9c', '\uff68', '\uff6a', '\uff66', '\uff9d',		// wa,... un
			'\uff73', '\uff76', '\uff79', '\uff9c', '\uff68', '\uff6a',
			'\uff66', 
			'\uff65', '\uff70',
			'\u30fd',	//KATAKANA ITERATION MARK
			'\u309d',	//KATAKANA VOICED ITERATION MARK
			'\u30ff'    //KATAKANA DIGRAPH KOTO
		};
        #endregion end of Full width Katakana map table.

        #region Voiced (accent) map table (Japanese)
        /// <summary>
        /// Mapping table for accents for the Japanese language.
        /// </summary>
        private static readonly int[] Accentkana = 
				{
					0, 0, 0, 0, 0, 0, 0, 0, 0, 0,						// a
					-1, 1, -1, 1, -1, 1, -1, 1, -1, 1,					// ka
					-1, 1, -1, 1, -1, 1, -1, 1, -1, 1,					// sa
					-1, 1, -1, 1, 0, -1, 1, -1, 1, -1, 1,				// ta
					0, 0, 0, 0, 0,										// na
					-3, 1, 2, -3, 1, 2, -3, 1, 2, -3, 1, 2, -3, 1, 2,	// ha
					0, 0, 0, 0, 0,										// ma
					0, 0, 0, 0, 0, 0,									// ya
					0, 0, 0, 0, 0,										// ra
					0, 0, 0, 0, 0, 0,									// wa,...un
					1, 0, 0, 1, 1, 1,
					1,
					0, 0,
					0, 0, 0
				};
        #endregion end of Voiced (accent) map table (Japanese).

        #region Special quotations for FarEast
        private static readonly char[] FeQuotes = { '\u2018', '\u2019', '\u201c', '\u201d' };
        #endregion end of Special quotations for FarEast.

        #region Katakana & Hiragana mixed characters (Japanese)
        private static readonly char[] JpnMixed = { '\u30fc' };
        #endregion end of Katakana & Hiragana mixed characters (Japanese).

        #region Full / Half width special character map table
        private static readonly char[] JpnSpecialFull =
				{
					'\u3000',		// Space
					'\u3001',		// Comma
					'\u3002',		// Stop (Period)
					'\u300c',		// Left bracket
					'\u300d',		// Right bracket
					'\u201c',		// Double Quotes
					'\u201d',		//     "
					'\u2018',		// Single Quotes
					'\u2019',		//	   "
					'\u309b',		// JPN Voiced (heavy)
					'\u309c',		// JPN Voiced (light)
					'\uffe5'		// yen mark !
				};

        private static readonly char[] JpnSpecialHalf = 
				{
					'\u0020',		// Space
					'\uff64',		// Comma
					'\uff61',		// Stop (Period)
					'\uff62',		// Left bracket
					'\uff63',		// Right bracket
					'\u0022',		// Double Quotes
					'\u0022',		//     "
					'\u0027',		// Single Quotes
					'\u0027',		//	   "
					'\uff9e',		// JPN Voiced (heavy)
					'\uff9f',		// JPN Voiced (light)
					'\u00a5'        // yen mark !
				};
        #endregion end of Full / Half width special character map table

        #region Mapping between capitalized kana...

        private const string LowerKana = "\u3041\u3043\u3045\u3047\u3049\u3063\u3083\u3085\u3087\u308e" + "\u30a1\u30a3\u30a5\u30a7\u30a9\u30c3\u30e3\u30e5\u30e7\u30ee" + "\uff67\uff68\uff69\uff6a\uff6b\uff6c\uff6d\uff6e\uff6f\u30F5\u30F6\u3095\u3096";

        private const string UpperKana = "\u3042\u3044\u3046\u3048\u304a\u3064\u3084\u3086\u3088\u308f" + "\u30a2\u30a4\u30a6\u30a8\u30aa\u30c4\u30e4\u30e6\u30e8\u30ef" + "\uff71\uff72\uff73\uff74\uff75\uff94\uff95\uff96\uff82\u30AB\u30B1\u304B\u3051";

        #endregion
        #endregion end of Static Members(tables).

        #region Static Min & Max values
        /// <summary>
        /// Represents the smallest possible value of a Char. 
        /// This field is constant.
        /// </summary>
        public static char MinValue = '\u0000';

        /// <summary>
        /// Represents the largest possible value of a Char. 
        /// This field is constant.
        /// </summary>
        public static char MaxValue = '\uffff';
        #endregion end of Static Min & Max values.

        #region Full & Half Width Symbol
        private  static readonly char[] FullWidthSymbolArray = new char[]{
												//'\u3000',	//IDEOGRAPHIC SPACE
												'\u3001',	//IDEOGRAPHIC COMMA
												'\u3002',	//IDEOGRAPHIC FULL STOP
												'\uFF0C',	//FULLWIDTH COMMA
												'\uFF0E',	//FULLWIDTH FULL STOP
												'\u30FB',	//KATAKANA MIDDLE DOT
												'\uFF1A',	//FULLWIDTH COLON
												'\uFF1B',	//FULLWIDTH SEMICOLON
												'\uFF1F',	//FULLWIDTH QUESTION MARK
												'\uFF01',	//FULLWIDTH EXCLAMATION MARK
												'\u309B',	//KATAKANA-HIRAGANA VOICED SOUND MARK
												'\u309C',	//KATAKANA-HIRAGANA SEMI-VOICED SOUND MARK
												'\u00B4',	//ACUTE ACCENT
												'\uFF40',	//FULLWIDTH GRAVE ACCENT
												'\u00A8',	//DIAERESIS
												'\uFF3E',	//FULLWIDTH CIRCUMFLEX ACCENT
												'\uFFE3',	//FULLWIDTH MACRON 
												'\uFF3F',	//FULLWIDTH LOW LINE
												'\u30FD',	//KATAKANA ITERATION MARK
												'\u30FE',	//KATAKANA VOICED ITERATION MARK
												'\u309D',	//HIRAGANA ITERATION MARK
												'\u309E',	//HIRAGANA VOICED ITERATION MARK
												'\u3003',	//DITTO MARK
												'\u4EDD',	//<cjk>
												'\u3005',	//IDEOGRAPHIC ITERATION MARK
												'\u3006',	//IDEOGRAPHIC CLOSING MARK
												'\u3007',	//IDEOGRAPHIC NUMBER ZERO
												'\u30FC',	//KATAKANA-HIRAGANA PROLONGED SOUND MARK
												'\u2014',	//EM DASH	Windows: U+2015
												'\u2010',	//HYPHEN
												'\uFF0F',	//FULLWIDTH SOLIDUS
												//'\u005C',	//REVERSE SOLIDUS	Fullwidth: U+FF3C
												'\u301C',	//WAVE DASH	Windows: U+FF5E
												'\u2016',	//DOUBLE VERTICAL LINE	Windows: U+2225
												'\uFF5C',	//FULLWIDTH VERTICAL LINE
												'\u2026',	//HORIZONTAL ELLIPSIS
												'\u2025',	//TWO DOT LEADER
												'\u2018',	//LEFT SINGLE QUOTATION MARK
												'\u2019',	//RIGHT SINGLE QUOTATION MARK
												'\u201C',	//LEFT DOUBLE QUOTATION MARK
												'\u201D',	//RIGHT DOUBLE QUOTATION MARK
												'\uFF08',	//FULLWIDTH LEFT PARENTHESIS
												'\uFF09',	//FULLWIDTH RIGHT PARENTHESIS
												'\u3014',	//LEFT TORTOISE SHELL BRACKET
												'\u3015',	//RIGHT TORTOISE SHELL BRACKET
												'\uFF3B',	//FULLWIDTH LEFT SQUARE BRACKET
												'\uFF3D',	//FULLWIDTH RIGHT SQUARE BRACKET
												'\uFF5B',	//FULLWIDTH LEFT CURLY BRACKET
												'\uFF5D',	//FULLWIDTH RIGHT CURLY BRACKET
												'\u3008',	//LEFT ANGLE BRACKET
												'\u3009',	//RIGHT ANGLE BRACKET
												'\u300A',	//LEFT DOUBLE ANGLE BRACKET
												'\u300B',	//RIGHT DOUBLE ANGLE BRACKET
												'\u300C',	//LEFT CORNER BRACKET
												'\u300D',	//RIGHT CORNER BRACKET
												'\u300E',	//LEFT WHITE CORNER BRACKET
												'\u300F',	//RIGHT WHITE CORNER BRACKET
												'\u3010',	//LEFT BLACK LENTICULAR BRACKET
												'\u3011',	//RIGHT BLACK LENTICULAR BRACKET
												'\uFF0B',	//FULLWIDTH PLUS SIGN
												'\u2212',	//MINUS SIGN	Windows: U+FF0D
												'\u00B1',	//PLUS-MINUS SIGN
												'\u00D7',	//MULTIPLICATION SIGN
												'\u00F7',	//DIVISION SIGN
												'\uFF1D',	//FULLWIDTH EQUALS SIGN
												'\u2260',	//NOT EQUAL TO
												'\uFF1C',	//FULLWIDTH LESS-THAN SIGN
												'\uFF1E',	//FULLWIDTH GREATER-THAN SIGN
												'\u2266',	//LESS-THAN OVER EQUAL TO
												'\u2267',	//GREATER-THAN OVER EQUAL TO
												'\u221E',	//INFINITY
												'\u2234',	//THEREFORE
												'\u2642',	//MALE SIGN
												'\u2640',	//FEMALE SIGN
												'\u00B0',	//DEGREE SIGN
												'\u2032',	//PRIME
												'\u2033',	//DOUBLE PRIME
												'\u2103',	//DEGREE CELSIUS
												'\uFFE5',	//FULLWIDTH YEN SIGN
												'\uFF04',	//FULLWIDTH DOLLAR SIGN
												'\u00A2',	//CENT SIGN	Windows: U+FFE0
												'\u00A3',	//POUND SIGN	Windows: U+FFE1
												'\uFF05',	//FULLWIDTH PERCENT SIGN
												'\uFF03',	//FULLWIDTH NUMBER SIGN
												'\uFF06',	//FULLWIDTH AMPERSAND
												'\uFF0A',	//FULLWIDTH ASTERISK
												'\uFF20',	//FULLWIDTH COMMERCIAL AT
												'\u00A7',	//SECTION SIGN
												'\u2606',	//WHITE STAR
												'\u2605',	//BLACK STAR
												'\u25CB',	//WHITE CIRCLE
												'\u25CF',	//BLACK CIRCLE
												'\u25CE',	//BULLSEYE
												'\u25C7',	//WHITE DIAMOND
												'\u25C6',	//BLACK DIAMOND
												'\u25A1',	//WHITE SQUARE
												'\u25A0',	//BLACK SQUARE
												'\u25B3',	//WHITE UP-POINTING TRIANGLE
												'\u25B2',	//BLACK UP-POINTING TRIANGLE
												'\u25BD',	//WHITE DOWN-POINTING TRIANGLE
												'\u25BC',	//BLACK DOWN-POINTING TRIANGLE
												'\u203B',	//REFERENCE MARK
												'\u3012',	//POSTAL MARK
												'\u2192',	//RIGHTWARDS ARROW
												'\u2190',	//LEFTWARDS ARROW
												'\u2191',	//UPWARDS ARROW
												'\u2193',	//DOWNWARDS ARROW
												'\u3013',	//GETA MARK
												'\uFF07',	//FULLWIDTH APOSTROPHE
												'\uFF02',	//FULLWIDTH QUOTATION MARK	[2000]
												'\uFF0D',	//FULLWIDTH HYPHEN-MINUS	[2000]
												//'\u007E',	//TILDE	[2000]	Fullwidth: U+FF5E
												'\u3033',	//VERTICAL KANA REPEAT MARK UPPER HALF	[2000]
												'\u3034',	//VERTICAL KANA REPEAT WITH VOICED SOUND MARK UPPER HALF	[2000]
												'\u3035',	//VERTICAL KANA REPEAT MARK LOWER HALF	[2000]
												'\u303B',	//VERTICAL IDEOGRAPHIC ITERATION MARK	[2000]	[Unicode3.2]
												'\u303C',	//MASU MARK	[2000]	[Unicode3.2]
												'\u30FF',	//KATAKANA DIGRAPH KOTO	[2000]	[Unicode3.2]
												'\u309F',	//HIRAGANA DIGRAPH YORI	[2000]	[Unicode3.2]
												'\u2208',	//ELEMENT OF	[1983]
												'\u220B',	//CONTAINS AS MEMBER	[1983]
												'\u2286',	//SUBSET OF OR EQUAL TO	[1983]
												'\u2287',	//SUPERSET OF OR EQUAL TO	[1983]
												'\u2282',	//SUBSET OF	[1983]
												'\u2283',	//SUPERSET OF	[1983]
												'\u222A',	//UNION	[1983]
												'\u2229',	//INTERSECTION	[1983]
												'\u2284',	//NOT A SUBSET OF	[2000]
												'\u2285',	//NOT A SUPERSET OF	[2000]
												'\u228A',	//SUBSET OF WITH NOT EQUAL TO	[2000]
												'\u228B',	//SUPERSET OF WITH NOT EQUAL TO	[2000]
												'\u2209',	//NOT AN ELEMENT OF	[2000]
												'\u2205',	//EMPTY SET	[2000]
												'\u2305',	//PROJECTIVE	[2000]
												'\u2306',	//PERSPECTIVE	[2000]
												'\u2227',	//LOGICAL AND	[1983]
												'\u2228',	//LOGICAL OR	[1983]
												'\u00AC',	//NOT SIGN	[1983]	Windows: U+FFE2
												'\u21D2',	//RIGHTWARDS DOUBLE ARROW	[1983]
												'\u21D4',	//LEFT RIGHT DOUBLE ARROW	[1983]
												'\u2200',	//FOR ALL	[1983]
												'\u2203',	//THERE EXISTS	[1983]
												'\u2295',	//CIRCLED PLUS	[2000]
												'\u2296',	//CIRCLED MINUS	[2000]
												'\u2297',	//CIRCLED TIMES	[2000]
												'\u2225',	//PARALLEL TO	[2000]
												'\u2226',	//NOT PARALLEL TO	[2000]
												'\u2985',	//LEFT WHITE PARENTHESIS	[2000]	[Unicode3.2]
												'\u2986',	//RIGHT WHITE PARENTHESIS	[2000]	[Unicode3.2]
												'\u3018',	//LEFT WHITE TORTOISE SHELL BRACKET	[2000]
												'\u3019',	//RIGHT WHITE TORTOISE SHELL BRACKET	[2000]
												'\u3016',	//LEFT WHITE LENTICULAR BRACKET	[2000]
												'\u3017',	//RIGHT WHITE LENTICULAR BRACKET	[2000]
												'\u2220',	//ANGLE	[1983]
												'\u22A5',	//UP TACK	[1983]
												'\u2312',	//ARC	[1983]
												'\u2202',	//PARTIAL DIFFERENTIAL	[1983]
												'\u2207',	//NABLA	[1983]
												'\u2261',	//IDENTICAL TO	[1983]
												'\u2252',	//APPROXIMATELY EQUAL TO OR THE IMAGE OF	[1983]
												'\u226A',	//MUCH LESS-THAN	[1983]
												'\u226B',	//MUCH GREATER-THAN	[1983]
												'\u221A',	//SQUARE ROOT	[1983]
												'\u223D',	//REVERSED TILDE 	[1983]
												'\u221D',	//PROPORTIONAL TO	[1983]
												'\u2235',	//BECAUSE	[1983]
												'\u222B',	//INTEGRAL	[1983]
												'\u222C',	//DOUBLE INTEGRAL	[1983]
												'\u2262',	//NOT IDENTICAL TO	[2000]
												'\u2243',	//ASYMPTOTICALLY EQUAL TO	[2000]
												'\u2245',	//APPROXIMATELY EQUAL TO	[2000]
												'\u2248',	//ALMOST EQUAL TO	[2000]
												'\u2276',	//LESS-THAN OR GREATER-THAN	[2000]
												'\u2277',	//GREATER-THAN OR LESS-THAN	[2000]
												'\u2194',	//LEFT RIGHT ARROW	[2000]
												'\u212B',	//ANGSTROM SIGN	[1983]
												'\u2030',	//PER MILLE SIGN	[1983]
												'\u266F',	//MUSIC SHARP SIGN	[1983]
												'\u266D',	//MUSIC FLAT SIGN	[1983]
												'\u266A',	//EIGHTH NOTE	[1983]
												'\u2020',	//DAGGER	[1983]
												'\u2021',	//DOUBLE DAGGER	[1983]
												'\u00B6',	//PILCROW SIGN	[1983]
												'\u266E',	//MUSIC NATURAL SIGN	[2000]
												'\u266B',	//BEAMED EIGHTH NOTES	[2000]
												'\u266C',	//BEAMED SIXTEENTH NOTES	[2000]
												'\u2669',	//QUARTER NOTE	[2000]
												'\u25EF',	//LARGE CIRCLE	[1983]
												// Added by shen yuan at 2005-10-26
												'\uFF3C',   // '＼'
												'\uFF5E',   // '～'
												'\uFFE0',   // 'E
												'\uFFE1',   // 'E
												'\uFFE2',   // '￢'
												'\u2015'    // '―'
												// End.
											};


        private  static char[] HalfWidthSymbolArray = new char[]{
													'\u005C',	//REVERSE SOLIDUS	Fullwidth: U+FF3C
													'\u007E',	//TILDE	[2000]	Fullwidth: U+FF5E
													//'\u00AC',	//NOT SIGN	[1983]	Windows: U+FFE2
													//'\u00B6'	//PILCROW SIGN	[1983]
													// Added by shen yuan at 2005-10-26
													'\u0021',   // '! '
													'\u0022',   // '" '
													'\u0023',   // '# '
													'\u0024',   // '$ '
													'\u0025',   // '% '
													'\u0026',   // '& '
													'\u0027',   // '' '
													'\u0028',   // '( '
													'\u0029',   // ') '
													'\u002A',   // '* '
													'\u002B',   // '+ '
													'\u002C',   // ', '
													'\u002D',   // '- '
													'\u002E',   // '. '
													'\u002F',   // '/ '
													'\u003A',   // ': '
													'\u003B',   // '; '
													'\u003C',   // '<'
													'\u003D',   // '= '
													'\u003E',   // '>'
													'\u003F',   // '? '
													'\u0040',   // '@ '
													'\u005B',   // '[ '
													'\u005D',   // '] '
													'\u005E',   // '^ '
													'\u005F',   // '_ '
													'\u0060',   // '` '
													'\u007B',   // '{ '
													'\u007C',   // '| '
													'\u007D',   // '} '
													'\uFF61',   // '? '
													'\uFF62',   // Halfwidth Left Corner Bracket
													'\uFF63',   // Halfwidth Right Corner Bracket
													'\uFF64',   // 'E
													'\uFF65',    // '￥ '
													'\u00A1',
													'\u00A4',
													'\u00A5'
													// End.
													};
        #endregion


        #region Instance Members
        /// <summary>
        /// Store for the instance unicode character.
        /// </summary>
        internal readonly char MValue;
        #endregion end of Instance members.
        #endregion end of Members.

        #region Construction
        /// <summary>
        /// Intializes a new instance of the CharEx class to a 
        /// specified Char structure.
        /// </summary>
        /// <param name="c">A character structure</param>
        /// <remarks>
        /// This constructor creates a <b>CharEx</b> and uses the specified 
        /// character (Char struct)as it's base.
        /// </remarks>
        public CharEx(char c)
        {
            MValue = c;
        }
        #endregion end of Construction.

        #region Methods
        #region Interface Implementation -(Public)
        #region IComparable
        /// <summary>
        /// Compares this instance to a specified object and returns an indication of 
        /// their relative values.
        /// </summary>
        /// <param name="value">An object to compare, or a null reference</param>
        /// <returns>
        /// A signed number indicating the relative values of this instance and value.
        /// </returns>
        public int CompareTo(object value)
        {
            return MValue.CompareTo(value);
        }
        #endregion end of IComparable.
        #endregion end of Interface Implementation -(Public).

        #region Conversion Operators
        /// <summary>
        /// The assignment operator converting a Char structure to a CharEx structure.
        /// </summary>
        /// <param name="c">A Char value</param>
        /// <returns>
        /// A CharEx class representing the same character as c.
        ///</returns>
        public static implicit operator CharEx(char c)
        {
            return new CharEx(c);
        }
        #endregion end of Conversion Operators.

        #region Object Methods -(Public)
        #region Character Type checking
        /// <summary>
        /// Retrieves the type of character.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// A CharType representing the type of character of c.
        /// </returns>
        public static CharType GetCharType(char c)
        {
            CharType ctype = CharType.OtherChar;

            UnicodeCategory uc = Char.GetUnicodeCategory(c);
            Blocks block = BelongTo(c);

            switch (uc)
            {
                case UnicodeCategory.Control:
                    ctype = CharType.Control;
                    break;
                case UnicodeCategory.SpaceSeparator:
                    return CharType.Space | (c == '\u3000' ? CharType.FullWidth : 0);
                case UnicodeCategory.UppercaseLetter:
                    ctype = CharType.UpperCase;
                    break;
                case UnicodeCategory.LowercaseLetter:
                    ctype = CharType.LowerCase;
                    break;
                case UnicodeCategory.DecimalDigitNumber:
                    ctype = CharType.Numeric;
                    break;
                case UnicodeCategory.MathSymbol:
                    ctype = CharType.MathSymbol;
                    break;
                case UnicodeCategory.CurrencySymbol:
                    ctype = CharType.Symbol;
                    break;
                case UnicodeCategory.OpenPunctuation:
                case UnicodeCategory.ClosePunctuation:
                case UnicodeCategory.InitialQuotePunctuation:
                case UnicodeCategory.FinalQuotePunctuation:
                case UnicodeCategory.DashPunctuation:
                    ctype = CharType.Punctuation;
                    break;
                case UnicodeCategory.ModifierSymbol:
                case UnicodeCategory.NonSpacingMark:
                case UnicodeCategory.ModifierLetter:
                case UnicodeCategory.OtherLetter:
                case UnicodeCategory.OtherNotAssigned:
                case UnicodeCategory.ConnectorPunctuation:
                    switch (block)
                    {
                        case Blocks.HALFWIDTH_AND_FULLWIDTH_FORMS:
                            return MultiWidthDetails(c);
                        case Blocks.KATAKANA:
                            return CharType.Katakana | CharType.FullWidth;
                        case Blocks.HIRAGANA:
                            return CharType.Hiragana | CharType.FullWidth;
                    }
                    break;
                case UnicodeCategory.OtherSymbol:
                case UnicodeCategory.OtherPunctuation:
                    ctype = CharType.Symbol;
                    break;
            }

            if (IsFarEastBlock(block, c))
            {
                ctype |= CharType.FullWidth;
            }

            return ctype;
        }

        /// <summary>
        /// Determines whether the specified character is of the specified type.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <param name="type">A CharType type</param>
        /// <returns>
        /// true if specified c and type matches the type of the
        /// character; otherwise false.
        /// </returns>
        public static bool IsCharOfType(char c, CharType type)
        {
            return GetCharType(c) == type;
        }

        /// <summary>
        /// Determines whether the specified Unicode character is 
        /// categorized as a multi-width character.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a multi-width character; otherwise, false.
        /// </returns>
        public static bool IsMultiWidth(char c)
        {
            Blocks block = BelongTo(c);

            return (block == Blocks.KATAKANA ||
                    block == Blocks.CJK_SYMBOLS_AND_PUNCTUATION ||
                    block == Blocks.HALFWIDTH_AND_FULLWIDTH_FORMS ||
                    (block == Blocks.BASIC_LATIN && c >= '\u0020'));
        }

        /// <summary>
        /// Determines whether the character at the specified position 
        /// in a specified string is categorized as a multi-width character.
        /// </summary>
        /// <param name="s">A string</param>
        /// <param name="index">The character position in s</param>
        /// <returns>
        /// true if the character at position index in s is a multi-width 
        /// character; otherwise, false.
        /// </returns>
        public static bool IsMultiWidth(string s, int index)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }

            if (index >= s.Length)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return IsMultiWidth(s[index]);
        }

        public static bool IsFullWidthSymbol(char c)
        {
            for (int i = 0; i < FullWidthSymbolArray.Length; i++)
            {
                if (c == FullWidthSymbolArray[i])
                {
                    return true;
                }
            }

            return false;
        }

        public static bool IsHalfWidthSymbol(char c)
        {
            for (int i = 0; i < HalfWidthSymbolArray.Length; i++)
            {
                if (c == HalfWidthSymbolArray[i])
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determines whether the specified Unicode character is a 
        /// full-width character. Usually CJK characters are considers 
        /// as full widths.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a full-width character; otherwise, false.
        /// </returns>
        public static bool IsFullWidth(char c)
        {
            if (CharEx.IsFullWidthSymbol(c))
            {
                return true;
            }

            if (CharEx.IsHalfWidthSymbol(c))
            {
                return false;
            }

            Blocks block = BelongTo(c);

            //
            // Should be FullWidth if we are lying in the CJK block.
            //
            bool bFullWidth = IsFarEastBlock(block, c);

            //
            // Need to do some special checking if character
            // lies in multi-width block.
            //
            if (block == Blocks.HALFWIDTH_AND_FULLWIDTH_FORMS)
            {
                bFullWidth = ((MultiWidthDetails(c) & CharType.FullWidth) == CharType.FullWidth);
            }

            return bFullWidth;
        }

        /// <summary>
        /// Indicates whether the character at the specified position 
        /// in the specified string is a full-width character. Usually 
        /// CJK characters are considers as full widths.
        /// </summary>
        /// <param name="s">A string</param>
        /// <param name="index">The character position in s</param>
        /// <returns>
        /// true if the character at position index in s is a full width 
        /// character; otherwise, false.
        /// </returns>
        public static bool IsFullWidth(string s, int index)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }

            if (index >= s.Length)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return IsFullWidth(s[index]);
        }

        /// <summary>
        /// Determines whether  a Unicode character doesn't belong to any specific letter.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is doesn't a Katakana character; otherwise, false.
        /// </returns>
        public static bool IsOther(char c)
        {
            return (GetCharType(c) & ~CharType.FullWidth) == CharType.OtherChar;
        }

        /// <summary>
        /// Determines whether the specified Unicode character is 
        /// categorized as a control code.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a control code; otherwise, false.
        /// </returns>
        public static bool IsControl(char c)
        {
            return (GetCharType(c) & ~CharType.FullWidth) == CharType.Control;
        }

        /// <summary>
        /// Indicates whether the character at the specified position 
        /// in the specified string is a control code.
        /// </summary>
        /// <param name="s">A string</param>
        /// <param name="index">The character position in s</param>
        /// <returns>
        /// true if the character at position index in s is a control 
        /// code; otherwise, false.
        /// </returns>
        public static bool IsControl(string s, int index)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }

            if (index >= s.Length)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return IsControl(s[index]);
        }

        /// <summary>
        /// Determines whether the specified Unicode character is 
        /// categorized as a Katakana letter.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a Katakana character; otherwise, false.
        /// </returns>
        public static bool IsKatakana(char c)
        {
            return (GetCharType(c) & ~CharType.FullWidth) == CharType.Katakana;
        }

        /// <summary>
        /// Indicates whether the character at the specified position 
        /// in the specified string is a Katakana character.
        /// </summary>
        /// <param name="s">A string</param>
        /// <param name="index">The character position in s</param>
        /// <returns>
        /// true if the character at position index in s is a Katakana 
        /// character; otherwise, false.
        /// </returns>
        public static bool IsKatakana(string s, int index)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }

            if (index >= s.Length)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return IsKatakana(s[index]);
        }

        /// <summary>
        /// Determines whether the specified Unicode character is 
        /// categorized as a Hiragana letter.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a Hiragana character; otherwise, false.
        /// </returns>
        public static bool IsHiragana(char c)
        {
            return ((GetCharType(c) & ~CharType.FullWidth) == CharType.Hiragana) ||
                   (Array.IndexOf(JpnMixed, c) != -1);
        }

        /// <summary>
        /// Indicates whether the character at the specified position 
        /// in the specified string is a Hiragana character.
        /// </summary>
        /// <param name="s">A string</param>
        /// <param name="index">The character position in s</param>
        /// <returns>
        /// true if the character at position index in s is a Hiragana 
        /// character; otherwise, false.
        /// </returns>
        public static bool IsHiragana(string s, int index)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }

            if (index >= s.Length)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return IsHiragana(s[index]);
        }

        /// <summary>
        /// Determines whether the specified Unicode character is 
        /// categorized as a ShiftJIS letter.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a ShiftJIS character; otherwise, false.
        /// </returns>
        public static bool IsShiftJIS(char c)
        {
            return false;
            //return JISCharSetHelper.IsShiftJIS(c);
        }

        /// <summary>
        /// Indicates whether the character at the specified position 
        /// in the specified string is a ShiftJIS character.
        /// </summary>
        /// <param name="s">A string</param>
        /// <param name="index">The character position in s</param>
        /// <returns>
        /// true if the character at position index in s is a ShiftJIS 
        /// character; otherwise, false.
        /// </returns>
        public static bool IsShiftJIS(string s, int index)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }

            if (index >= s.Length)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return IsShiftJIS(s[index]);
        }

        /// <summary>
        /// Determines whether the specified Unicode character is 
        /// categorized as a JISX0208 letter.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a JISX0208 character; otherwise, false.
        /// </returns>
        public static bool IsJISX0208(char c)
        {
            return false;
            // return JISCharSetHelper.IsJISX0208(c);
        }

        /// <summary>
        /// Indicates whether the character at the specified position 
        /// in the specified string is a JISX0208 character.
        /// </summary>
        /// <param name="s">A string</param>
        /// <param name="index">The character position in s</param>
        /// <returns>
        /// true if the character at position index in s is a JISX0208 
        /// character; otherwise, false.
        /// </returns>
        public static bool IsJISX0208(string s, int index)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }

            if (index >= s.Length)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return IsJISX0208(s[index]);
        }

        /// <summary>
        /// Determines whether the specified Unicode character is 
        /// categorized as a Numeric digit.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a Numeric character; otherwise, false.
        /// </returns>
        public static bool IsDigit(char c)
        {
            return (GetCharType(c) & ~CharType.FullWidth) == CharType.Numeric;
        }

        /// <summary>
        /// Indicates whether the character at the specified position 
        /// in the specified string is a Numeric digit.
        /// </summary>
        /// <param name="s">A string</param>
        /// <param name="index">The character position in s</param>
        /// <returns>
        /// true if the character at position index in s is a Numeric 
        /// digit; otherwise, false.
        /// </returns>
        public static bool IsDigit(string s, int index)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }

            if (index >= s.Length)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return IsDigit(s[index]);
        }

        /// <summary>
        /// Determines whether the specified Unicode character is 
        /// categorized as a Punctuation character.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a Punctuation character; otherwise, false.
        /// </returns>
        public static bool IsPunctuation(char c)
        {
            return (GetCharType(c) & ~CharType.FullWidth) == CharType.Punctuation;
        }

        /// <summary>
        /// Indicates whether the character at the specified position 
        /// in the specified string is a Punctuation character.
        /// </summary>
        /// <param name="s">A string</param>
        /// <param name="index">The character position in s</param>
        /// <returns>
        /// true if the character at position index in s is a Punctuation 
        /// character; otherwise, false.
        /// </returns>
        public static bool IsPunctuation(string s, int index)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }

            if (index >= s.Length)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return IsPunctuation(s[index]);
        }

        /// <summary>
        /// Determines whether the specified Unicode character is 
        /// categorized as a mathematical symbol.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a mathematical character; otherwise, false.
        /// </returns>
        public static bool IsMathSymbol(char c)
        {
            return (GetCharType(c) & ~CharType.FullWidth) == CharType.MathSymbol;
        }

        /// <summary>
        /// Indicates whether the character at the specified position 
        /// in the specified string is a mathematical symbol.
        /// </summary>
        /// <param name="s">A string</param>
        /// <param name="index">The character position in s</param>
        /// <returns>
        /// true if the character at position index in s is a mathematical 
        /// character; otherwise, false.
        /// </returns>
        public static bool IsMathSymbol(string s, int index)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }

            if (index >= s.Length)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return IsMathSymbol(s[index]);
        }

        /// <summary>
        /// Determines whether the specified Unicode character is 
        /// categorized as a Symbol character.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a Symbol character; otherwise, false.
        /// </returns>
        public static bool IsSymbol(char c)
        {
            return (GetCharType(c) & ~CharType.FullWidth) == CharType.Symbol;
        }

        /// <summary>
        /// Indicates whether the character at the specified position 
        /// in the specified string is a Symbol character.
        /// </summary>
        /// <param name="s">A string</param>
        /// <param name="index">The character position in s</param>
        /// <returns>
        /// true if the character at position index in s is a Symbol 
        /// character; otherwise, false.
        /// </returns>
        public static bool IsSymbol(string s, int index)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }

            if (index >= s.Length)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return IsSymbol(s[index]);
        }

        /// <summary>
        /// Determines whether the specified Unicode character is 
        /// categorized as a Lowercase letter.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a Lowercase letter; otherwise, false.
        /// </returns>
        public static bool IsLower(char c)
        {
            return (GetCharType(c) & ~CharType.FullWidth) == CharType.LowerCase;
        }

        /// <summary>
        /// Indicates whether the character at the specified position 
        /// in the specified string is a Lowercase letter.
        /// </summary>
        /// <param name="s">A string</param>
        /// <param name="index">The character position in s</param>
        /// <returns>
        /// true if the character at position index in s is a Lowercase 
        /// letter; otherwise, false.
        /// </returns>
        public static bool IsLower(string s, int index)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }

            if (index >= s.Length)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return IsLower(s[index]);
        }

        /// <summary>
        /// Determines whether the specified Unicode character is 
        /// categorized as a Uppercase letter.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a Uppercase letter; otherwise, false.
        /// </returns>
        public static bool IsUpper(char c)
        {
            return (GetCharType(c) & ~CharType.FullWidth) == CharType.UpperCase;
        }

        /// <summary>
        /// Indicates whether the character at the specified position 
        /// in the specified string is a Uppercase letter.
        /// </summary>
        /// <param name="s">A string</param>
        /// <param name="index">The character position in s</param>
        /// <returns>
        /// true if the character at position index in s is a Uppercase 
        /// letter; otherwise, false.
        /// </returns>
        public static bool IsUpper(string s, int index)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }

            if (index >= s.Length)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return IsLower(s[index]);
        }

        /// <summary>
        /// Determines whether the specified Unicode character is 
        /// categorized as a Numeric or a Math symbol.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a numeric or symbol character; otherwise, false.
        /// </returns>
        public static bool IsDigitOrSymbol(char c)
        {
            return IsDigit(c) || IsMathSymbol(c);
        }

        /// <summary>
        /// Indicates whether the character at the specified position 
        /// in the specified string is a Numeric or a Math symbol.
        /// </summary>
        /// <param name="s">A string</param>
        /// <param name="index">The character position in s</param>
        /// <returns>
        /// true if the character at position index in s is a Numeric 
        /// or a Math symbol; otherwise, false.
        /// </returns>
        public static bool IsDigitOrSymbol(string s, int index)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }

            if (index >= s.Length)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return IsDigitOrSymbol(s[index]);
        }

        /// <summary>
        /// Determines whether the specified Unicode character is 
        /// categorized as a alphabet letter.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a alphabet letter; otherwise, false.
        /// </returns>
        public static bool IsAlphabet(char c)
        {
            return IsUpper(c) || IsLower(c);
        }

        /// <summary>
        /// Indicates whether the character at the specified position 
        /// in the specified string is a alphabet letter.
        /// </summary>
        /// <param name="s">A string</param>
        /// <param name="index">The character position in s</param>
        /// <returns>
        /// true if the character at position index in s is a alphabet 
        /// letter; otherwise, false.
        /// </returns>
        public static bool IsAlphabet(string s, int index)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }

            if (index >= s.Length)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return IsAlphabet(s[index]);
        }

        /// <summary>
        /// Determines whether the specified Unicode character is 
        /// categorized as a alphabet letter or digit.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a alphabet letter or digit; otherwise, false.
        /// </returns>
        public static bool IsAlphaOrDigit(char c)
        {
            return IsUpper(c) || IsLower(c) || IsDigit(c);
        }

        /// <summary>
        /// Indicates whether the character at the specified position 
        /// in the specified string is a alphabet letter or digit.
        /// </summary>
        /// <param name="s">A string</param>
        /// <param name="index">The character position in s</param>
        /// <returns>
        /// true if the character at position index in s is a alphabet 
        /// letter or digit; otherwise, false.
        /// </returns>
        public static bool IsAlphaOrDigit(string s, int index)
        {
            if (s == null)
            {
                throw new ArgumentNullException("s");
            }

            if (index >= s.Length)
            {
                throw new ArgumentOutOfRangeException("index");
            }

            return IsAlphaOrDigit(s[index]);
        }

        /// <summary>
        /// Determines whether the specified Unicode character is 
        /// categorized as a upper (capital) case kana.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a kana upper (capital) case; otherwise, false.
        /// </returns>
        public static bool IsUpperKana(char c)
        {
            return !IsLowerKana(c);
        }

        /// <summary>
        /// Determines whether the specified Unicode character is 
        /// categorized as a lower (normal) case kana.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c is a kana lower (normal) case; otherwise, false.
        /// </returns>
        public static bool IsLowerKana(char c)
        {
            return (LowerKana.IndexOf(c) != -1);
        }

        /// <summary>
        /// Determines whether the related kana has a related lower case.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// true if c has a related lower case kana; otherwise, false.
        /// </returns>
        public static bool HasLowerKana(char c)
        {
            return (UpperKana.IndexOf(c) != -1 || IsLowerKana(c));
        }
        #endregion end of Character Type checking.

        #region Character Conversions
        /// <summary>
        /// Transforms the specified character to a full-width character
        /// if possible. Ranges are Latin basic, Katakana and Hangul 
        /// characters.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// Full width character of c, otherwise original character returned.
        /// </returns>
        public static char ToFullWidth(char c)
        {
            bool dummy;

            return ToFullWidth(out dummy, c);
        }

        /// <summary>
        /// Transforms the specified character to a full-width character if possible. 
        /// </summary>
        /// <param name="c">Array of characters</param>
        /// <param name="processedAll"><b>true</b> when the soundex character is processed; otherwise, <b>false</b></param>
        /// <returns>Full width character of c; otherwise the original character</returns>
        /// <remarks>
        /// This method differs from the other where it takes an array of characters. 
        /// In Far East countries, there are times when multiple half-width characters make a one full-width character.
        /// </remarks>
        public static char ToFullWidth(out bool processedAll, params char[] c)
        {
            processedAll = false;

            if (c.Length == 0)
            {
                throw new ArgumentException(string.Format(C1Localizer.GetString("C1Input.Common.LengthZeroException"), "c"));

            }

            char c1 = c[0];

            if (IsMultiWidth(c1))
            {
                //
                // Latin basic characters can be directly converted
                // by making a few shifts...
                //
                if (c1 < Charstarts[(int)Blocks.LATIN_1_SUPPLEMENT])
                {
                    //
                    // Funny why 'space' was left out of this category.
                    //
                    if (c1 == '\u0020')
                    {
                        return '\u3000';
                    }

                    return (char)(c1 - '\u0021' + (Charstarts[(int)Blocks.HALFWIDTH_AND_FULLWIDTH_FORMS] + 1));
                }

                //
                //- pickup a direct map from the table...
                //
                if ((MultiWidthDetails(c1) & CharType.Katakana) == CharType.Katakana)
                {
                    if (c1 < KANAHALFSTART)
                    {
                        char c2 = GetFullHalfWidthSpecialChar(c1, true);

                        return (c2 != char.MinValue) ? c2 : c1;
                    }

                    c1 = Halfkana[c1 - KANAHALFSTART];

                    //
                    // Handle the soundex here....
                    //				
                    if (c.Length < 2)
                    {
                        return c1;
                    }

                    int daku = c[1] - (KATAKANA_VOICED - 1);	// should be KATAKANA_VOICED or KATAKANA_SEMIVOICED.

                    if (daku == 1 || daku == 2)
                    {
                        processedAll = true;

                        int accent = Accentkana[c1 - (Charstarts[(int)Blocks.KATAKANA] + 1)];

                        if (accent != 0)
                        {
                            if ((Math.Abs(accent) & 2) == daku)
                            {
                                c1++;
                            }

                            c1++;
                        }
                    }
                    if (daku == 1 && c1 == '\u30A6')
                    {
                        c1 = '\u30F4';
                    }
                }
            }

            return c1;
        }

        /// <summary>
        /// Converts the value of a Unicode character to its half-width equivalent. 
        /// Ranges are Latin basic, Katakana, and Hangul characters.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// <para>The half width equivalent of c</para>
        /// <para>- or -</para>
        ///	  <para>
        ///  The unchanged value of c, if c is already a half
        ///  width or not a multi-width character
        /// </para>
        /// </returns>
        public static char ToHalfWidth(char c)
        {
            //
            // Need to return only the first character.
            //
            return ToHalfWidthEx(c)[0];
        }

        /// <summary>
        /// Transforms the specified character to a half-width character if possible. 
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>Character array representing c in half width form; in most cases this array will have only one element</returns>
        /// <remarks>
        /// This method differs from the ToHalfWidth method where it tries to return the accurate half-width character - 
        /// which most likely happens in Far East countries.
        /// </remarks>
        public static char[] ToHalfWidthEx(char c)
        {
            CharType ctype = GetCharType(c);

            bool multiWidth = IsMultiWidth(c);
            //
            // First filter out half-width characters and characters that
            // are not of CJK groups. 
            //
            if ((ctype & CharType.FullWidth) == CharType.FullWidth)
            {
                switch (ctype & ~CharType.FullWidth)
                {
                    case CharType.Punctuation:
                    case CharType.UpperCase:
                    case CharType.LowerCase:
                    case CharType.Symbol:
                    case CharType.Numeric:
                    case CharType.MathSymbol:
                        {
                            char c1 = GetFullHalfWidthSpecialChar(c, false);

                            if (c1 != char.MinValue)
                            {
                                c = c1;
                            }
                            else
                            {
                                if (multiWidth)
                                {
                                    c = (char)((c - (Charstarts[(int)Blocks.HALFWIDTH_AND_FULLWIDTH_FORMS] + 1)) + '\u0021');
                                }
                            }
                        }
                        break;
                    case CharType.Katakana:
                        {
                            int n = c - (Charstarts[(int)Blocks.KATAKANA] + 1);

                            c = Fullkana[n];
                            int accent = Accentkana[n];

                            if (accent > 0)
                            {
                                return new char[] { c, accent == 1 ? KATAKANA_VOICED : KATAKANA_SEMIVOICED };
                            }
                        }
                        break;
                    case CharType.Space:
                        c = '\u0020';
                        break;
                    case CharType.Hangul:
                        break;
                }
            }

            return new char[] { c };
        }

        /// <summary>
        /// Converts the value of a Unicode character to its "Katakana" equivalent. 
        /// Special character handling for the Japanese language.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// <para>The Katakana equivalent of c.</para>
        /// <para>- or -</para>
        ///	  <para>
        ///  The unchanged value of c, if c is already a 
        ///	     Katakana or not Hiragana.
        /// </para>
        /// </returns>
        public static char ToKatakana(char c)
        {
            //
            // Simply return the character if it isn't a hiragana.
            //
            if (!IsCharOfType(c, CharType.Hiragana | CharType.FullWidth))
            {
                return c;
            }

            //
            // Need to handle special characters here...
            //
            char c1 = GetFullHalfWidthSpecialChar(c, false);
            if (c1 != char.MinValue)
            {
                return c1;
            }

            return (char)(c - Charstarts[(int)Blocks.HIRAGANA] + Charstarts[(int)Blocks.KATAKANA]);
        }

        /// <summary>
        /// Converts the value of a Unicode character to its "Katakana" equivalent with an optional setting of
        /// full or half width. Special character handling for the Japanese language.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <param name="fullWidth">
        /// <b>true</b> to return a full width Katakana, and 
        /// <b>false</b> to return a half width Katakana</param>
        /// <returns>
        /// <para>The Katakana equivalent of c.</para>
        /// <para>- or -</para>
        ///	  <para>
        ///  The unchanged value of c, if c is already a 
        ///	     Katakana or not Hiragana.
        /// </para>
        /// </returns>
        public static char[] ToKatakana(char c, bool fullWidth)
        {
            char c1 = ToKatakana(c);

            if (!fullWidth)
            {
                return CharEx.ToHalfWidthEx(c1);
            }

            return new char[] { ToFullWidth(c1) };
        }

        /// <summary>
        /// Converts the value of a Unicode character to its "Hiragana" equivalent. 
        /// Special character handling for the Japanese language.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// <para>The Hiragana equivalent of c.</para>
        /// <para>- or -</para>
        ///	  <para>
        ///  The unchanged value of c, if c is already a 
        ///	     Hiragana or not Katakana.
        /// </para>
        /// </returns>
        public static char ToHiragana(char c)
        {
            //
            // Simply return the character if it isn't a hiragana.
            //
            if (!IsCharOfType(c, CharType.Katakana | CharType.FullWidth))
            {
                return c;
            }

            //
            // Some fullwidth Katakana characters can't be expressed in Hiragaga
            // so mask it out.
            //
            if (c >= '\u30f7' && c <= '\u30ff')
            {
                return c;
            }

            if (c == '\u30f5')
                return '\u304b';
            if (c == '\u30f6')
                return '\u3051';

            return (char)(c - Charstarts[(int)Blocks.KATAKANA] + Charstarts[(int)Blocks.HIRAGANA]);
        }

        /// <summary>
        /// Converts the value of a Unicode character to its ANSI equivalent.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>ANSI character equivalent of c in the current culture.
        /// </returns>
        /// <remarks>
        /// Encodes the specified Unicode character to an ANSI character using the current culture.
        /// </remarks>
        public static char ToAnsi(char c)
        {
            return ToAnsi(c, null);
        }

        /// <summary>
        /// Converts the value of a Unicode character to its ANSI equivalent using the specified culture information.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <param name="culture">CultureInfo object; if a null reference is passed, the current culture is assumed</param>
        /// <returns>ANSI character equivalent of c using the specified culture.
        /// </returns>
        public static char ToAnsi(char c, CultureInfo culture)
        {
            if (culture == null)
            {
                culture = CultureInfo.CurrentCulture;
            }

            //
            // Pickup the the codepage information from the culture object.
            //
            Encoding enc = Encoding.GetEncoding(culture.TextInfo.ANSICodePage);

            return InnerToChar(enc.GetBytes(new char[] { c }, 0, 1));
        }

        /// <summary>
        /// Converts the value of a JIS (Japanese encoding) character to its Shift-JIS equivalent.
        /// </summary>
        /// <param name="c">JIS character</param>
        /// <returns>
        /// <para>The Shift-JIS equivalent of c.</para>
        /// <para>- or -</para>
        ///	  <para>
        /// The unchanged value of c, if c is not a full width
        ///		character.
        /// </para>
        /// </returns>
        /// <remarks> 
        /// <newpara>
        /// Converts a JIS character to it's equivalent Shift-JIS
        /// character - used in the Japanese language.
        ///	  </newpara>
        /// <newpara>
        /// Please note that this method does not take a Unicode
        /// character as it's parameter - use the character value
        /// returned by the "ToJIS" method.
        ///	  </newpara>
        /// </remarks>
        public static char ToSJIS(char c)
        {
            //
            // If half width then simply return.
            //
            if (c < ANSIEND)
            {
                return c;
            }

            //
            // Just by shifting a few bits can be converted to a SJIS character.
            //
            byte[] b = InnerToByte(c);
            b[1] = (byte)(b[1] + (((b[0] & 1) == 1) ? ((b[1] <= 0x5f) ? 0x1f : 0x20) : 0x7e));
            b[0] = (byte)(((b[0] - 0x21) >> 1) + ((b[0] <= 0x5e) ? 0x81 : 0xc1));

            return InnerToChar(b);
        }

        /// <summary>
        /// Converts the value of a Shift-JIS (Japanese encoding) character to its JIS equivalent.
        /// </summary>
        /// <param name="c">A Shift-JIS character</param>
        /// <returns>
        /// <para>The JIS equivalent of c.</para>
        /// <para>- or -</para>
        ///	  <para>
        /// The unchanged value of c, if c is not a full width
        ///		character.
        /// </para>
        /// </returns>
        /// <remarks> 
        /// <newpara>
        /// Converts a Shift-JIS character to it's equivalent JIS
        /// character - used in the Japanese language.
        ///	  </newpara>
        /// <newpara>
        /// Please note that this method does not take a Unicode
        /// character as it's parameter - use the character value
        /// returned by the "ToSJIS" method.
        ///	  </newpara>
        /// </remarks>
        public static char ToJIS(char c)
        {
            //
            // If half width then simply return.
            //
            if (c < ANSIEND)
            {
                return c;
            }

            //
            // Just by shifting a few bits can be converted to a JIS character.
            //
            byte[] b = InnerToByte(c);
            b[0] = (byte)(((b[0] - ((b[0] <= 0x9f) ? 0x81 : 0xc1)) << 1) + ((b[1] <= 0x9e) ? 0x21 : 0x22));
            b[1] = (byte)(b[1] - ((b[1] <= 0x7e) ? 0x1f : (b[1] <= 0x9e) ? 0x20 : 0x7e));

            return InnerToChar(b);
        }

        /// <summary>
        /// Converts the value of an ANSI character to its Unicode equivalent.
        /// </summary>
        /// <param name="c">ANSI character</param>
        /// <returns>Unicode character equivalent of c</returns>
        public static char FromAnsi(char c)
        {
            return FromAnsi(c, null);
        }

        /// <summary>
        /// Converts the value of a ANSI character to its Unicode equivalent using the specified culture information.
        /// </summary>
        /// <param name="c">ANSI character</param>
        /// <param name="culture">CultureInfo object; f a null reference is passed, the current culture is assumed</param>
        /// <returns>Unicode character equivalent of c using the specified culture</returns>
        public static char FromAnsi(char c, CultureInfo culture)
        {
            if (culture == null)
            {
                culture = CultureInfo.CurrentCulture;
            }

            //
            // Pickup the the codepage information from the culture object.
            //
            Encoding enc = Encoding.GetEncoding(culture.TextInfo.ANSICodePage);

            return enc.GetChars(InnerToByte(c))[0];
        }

        /// <summary>
        /// Converts an lower (normal) case kana into a upper (capital) case.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>The equivalent kana upper case character.</returns>
        public static char ToUpperKana(char c)
        {
            int index = LowerKana.IndexOf(c);
            return (index == -1) ? c : UpperKana[index];
        }

        /// <summary>
        /// Converts an upper (capital) case kana into a lower case (normal).
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>The equivalent kana lower case character.</returns>
        public static char ToLowerKana(char c)
        {
            int index = UpperKana.IndexOf(c);
            if (index >= UpperKana.Length - 4 && index < UpperKana.Length)
            {
                // We does not allow convert from \u304B/\u3051 to \u3095/\u3096.
                // And \u30AB/\u30B1 to \u30F5/\u30F6 is not allowed.
                return c;
            }
            return (index == -1) ? c : LowerKana[index];
        }
        #endregion end of Character Conversions.

        #region Content Comparison
        /// <summary>
        /// Serves as a hash function for a particular type, suitable for use in 
        /// hashing algorithms and data structures like a hash table.
        /// </summary>
        /// <returns>Hash code for the current Object</returns>
        public override int GetHashCode()
        {
            return MValue.GetHashCode();
        }

        /// <summary>
        /// Determines whether two Object instances are equal.
        /// </summary>
        /// <param name="value">Object to compare with the current aqDateTime</param>
        /// <returns>
        /// true if value is an instance of aqDateTime and equals the value of 
        /// this instance; otherwise, false.
        /// </returns>
        public override bool Equals(object value)
        {
            //
            // Need to add 'type-checking" of our own....   :-)
            //
            if (value is CharEx)
            {
                value = ((CharEx)value).MValue;
            }

            return MValue.Equals(value);
        }
        #endregion end of Content Comparison.
        #endregion end of Object Methods -(Public).

        #region Object Internal Implementations -(Internal)
        #region Character Blocks
        /// <summary>
        /// Retreives the Unicode character block the specified character belongs to. 
        /// Current version supports a range from '\u000' through '\uffff'.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>Unicode block the specified character belongs to</returns>
        private static Blocks BelongTo(char c)
        {
            int bottom = 0;
            int top = Charstarts.Length;
            int current = top >> 1;

            //
            // Binary search used to find proper type.
            //
            while (top - bottom > 1)
            {
                if (c >= Charstarts[current])
                {
                    bottom = current;
                }
                else
                {
                    top = current;
                }

                current = (top + bottom) >> 1;
            }

            return (Blocks)current;
        }

        /// <summary>
        /// Returns further detail information of the character, since
        /// it lies in a range where it holds a fixture of different character widths.
        /// </summary>
        /// <param name="c">Unicode character</param>
        /// <returns>
        /// An CharType describing the type of the character, and also
        /// the width whether it's full or half.
        /// </returns>
        private static CharType MultiWidthDetails(char c)
        {
            int bottom = 0;
            int top = Fullhalfblocks.Length;
            int current = top >> 1;

            //
            // Binary search used to find proper type.
            //
            while (top - bottom > 1)
            {
                if (c >= Fullhalfblocks[current])
                {
                    bottom = current;
                }
                else
                {
                    top = current;
                }

                current = (top + bottom) >> 1;
            }

            return Mwtable[current];
        }

        /// <summary>
        /// Determines whether the specified block has to do with CJK... ( only CJK have concepts of wide ).
        /// </summary>
        /// <param name="block">Unicode char block</param>
        /// <param name="c"><see cref="char"/> to process</param>
        /// <returns>
        /// true if the block is related to CJK; otherwise false.
        /// </returns>
        private static bool IsFarEastBlock(Blocks block, char c)
        {
            switch (block)
            {
                case Blocks.CJK_COMPATIBILITY:
                case Blocks.CJK_COMPATIBILITY_FORMS:
                case Blocks.CJK_COMPATIBILITY_IDEOGRAPHS:
                case Blocks.CJK_RADICALS_SUPPLEMENT:
                case Blocks.CJK_SYMBOLS_AND_PUNCTUATION:
                case Blocks.CJK_UNIFIED_IDEOGRAPHS:
                case Blocks.CJK_UNIFIED_IDEOGRAPHS_EXTENSION:
                case Blocks.HALFWIDTH_AND_FULLWIDTH_FORMS:
                case Blocks.BOPOMOFO:
                case Blocks.BOPOMOFO_EXTENDED:
                case Blocks.HIRAGANA:
                case Blocks.KATAKANA:
                case Blocks.KANBUN:
                case Blocks.HANGUL_COMPATIBILITY_JAMO:
                case Blocks.HANGUL_JAMO:
                case Blocks.HANGUL_SYLLABLES:
                    return true;

                default:
                    // There are some different requirements from InputMan Winform.
                    // No matter the current cultureinfo.

                    // Special Punctuation characters.
                    // Need to check for double	& single quotes since they lie in
                    // a different range.
                    if (Array.IndexOf(FeQuotes, c) != -1)
                    {
                        return true;
                    }

                    // Check specified code pages, such as: SHIFT-JIS.
                    if (Encoding.GetEncoding(0).GetByteCount(new char[] { c }) > 1)
                    {
                        return true;
                    }

                    if (c > 255)
                    {
                        return true;
                    }
                    break;
            }

            return false;
        }
        #endregion end of Character Blocks.

        #region Inner Helper Functions
        /// <summary>
        /// Converts a byte array into a char type. (Internal usage).
        /// </summary>
        /// <param name="value">Byte array</param>
        /// <returns>Character equivalent of value</returns>
        private static char InnerToChar(byte[] value)
        {
            return (char)(value.Length == 1 ? value[0] : (value[0] << 8) | value[1]);
        }

        /// <summary>
        /// Converts a single character into a byte array. (Internal usage).
        /// </summary>
        /// <param name="c">A character</param>
        /// <returns>Byte array equivalent of c</returns>
        private static byte[] InnerToByte(char c)
        {
            byte[] b;

            if (c < ANSIEND)
            {
                b = new byte[] { (byte)c };
            }
            else
            {
                b = new byte[] { (byte)((c & 0xff00) >> 8), (byte)(c & 0x00ff) };
            }

            return b;
        }

        /// <summary>
        /// Conversion between half and full-width characters for Japanese special characters.
        /// </summary>
        /// <param name="c">Character</param>
        /// <param name="toFull"><b>true</b> to convert to full width; otherwise, <b>false</b></param>
        /// <returns>
        /// The converted character. <see cref="Char.MinValue"/> means conversion
        /// didn't take place.
        /// </returns>
        private static char GetFullHalfWidthSpecialChar(char c, bool toFull)
        {
            char[] srctable = (toFull) ? JpnSpecialHalf : JpnSpecialFull;
            char[] desttable = (toFull) ? JpnSpecialFull : JpnSpecialHalf;

            int found = Array.IndexOf(srctable, c);

            if (found != -1)
            {
                return desttable[found];
            }

            return char.MinValue;
        }
        #endregion end of Inner Helper Functions.
        #endregion end of Object Internal Implementations -(Internal).
        #endregion end of Methods.
    }
}
