﻿#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{
    #region The ShowLiterals enumeration
    /// <summary>
    /// Specifies how the literals are displayed during user entry
    /// </summary>
    /// <remarks>
    /// Specify when the literals are displayed.
    /// </remarks>
    internal enum ShowLiterals
    {
        /// <summary>
        /// Always show the literals
        /// </summary>
        Always = 0,
        /// <summary>
        /// The literals are post displayed as the user types
        /// </summary>
        PostDisplay = 1,
        /// <summary>
        /// The literals are pre displayed as the user types
        /// </summary>
        PreDisplay = 2,
    }
    #endregion end of showliterals enum
}
