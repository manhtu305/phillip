﻿using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Reflection;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{
    #region The MaskFormat class.
    /// <summary>
    /// Represents a mask format.
    /// </summary>
#if !EXTENDER 
    [SmartAssembly.Attributes.DoNotObfuscate]
#endif
    internal class MaskFormat : Format
    {

        #region The Keyword enumeration
        /// <summary>
        /// Specifies the type of keyword of the mask.
        /// </summary>
        internal enum KeywordType
        {
            /// <summary>
            /// Octal ASCII code
            /// </summary>
            OctalAscII,
            /// <summary>
            /// Hexadecimal ASCII
            /// </summary>
            HexAscII,
            /// <summary>
            /// Hexadecimal Unicode
            /// </summary>
            HexUnicode,
            /// <summary>
            /// Defined char base
            /// </summary>
            DefinedCharBase,
            /// <summary>
            /// Defined char addition
            /// </summary>
            DefinedCharAddition,
            /// <summary>
            /// CharSubset
            /// </summary>
            CharSubset,
            /// <summary>
            /// Enum group
            /// </summary>
            EnumGroup,
            /// <summary>
            /// Quantifier
            /// </summary>
            Quantifier,
            /// <summary>
            /// Prompt characters
            /// </summary>
            PromptChar,
            /// <summary>
            /// Unknown character sets
            /// </summary>
            Unknow
        }
        #endregion

        #region  Constructor

        static MaskFormat()
        {
            DBCS_A = CharEx.ToFullWidth('A');
            DBCS_a = CharEx.ToFullWidth('a');
            DBCS_B = CharEx.ToFullWidth('B');
            DBCS_D = CharEx.ToFullWidth('D');
            DBCS_J = CharEx.ToFullWidth('J');
            DBCS_K = CharEx.ToFullWidth('K');
            DBCS_W = CharEx.ToFullWidth('W');
            DBCS_X = CharEx.ToFullWidth('X');
            DBCS_Z = CharEx.ToFullWidth('Z');
            DBCS_T = CharEx.ToFullWidth('T');
            DBCS_M = CharEx.ToFullWidth('M');
            DBCS_I = CharEx.ToFullWidth('I');
            DBCS_N = CharEx.ToFullWidth('N');
            DBCS_G = CharEx.ToFullWidth('G');

            DBCS_z = CharEx.ToFullWidth('z');
            DBCS_0 = CharEx.ToFullWidth('0');
            DBCS_1 = CharEx.ToFullWidth('1');
            DBCS_9 = CharEx.ToFullWidth('9');
            DBCS_F = CharEx.ToFullWidth('F');
            DBCS_f = CharEx.ToFullWidth('f');
            DBCS__ = CharEx.ToFullWidth('_');

            DBCS_E = CharEx.ToFullWidth('E');
            DBCS_V = CharEx.ToFullWidth('V');
        }

        /// <summary>
        /// Creates a new mask format with default values.
        /// </summary>
        public MaskFormat()
            : this(string.Empty, string.Empty, string.Empty)
        {
        }

        /// <summary>
        /// Creates a new mask format for the specified owner.
        /// </summary>
        internal MaskFormat(IMaskFormat owner)
            : this(string.Empty, string.Empty, string.Empty)
        {
            this.Owner = owner;
            this._fields = new MaskFieldCollection(this.Owner);
        }

        /// <summary>
        /// ICreates a new mask format with the specified pattern.
        /// </summary>
        /// <param name="pattern">The mask pattern string</param>
        /// <param name="focusEmpty">Indicates the string expressing the null value when the focus is on it</param>
        /// <param name="displayEmpty">Indicates the string expressing the null value when the focus is not on it</param>
        /// <exception cref="ArgumentException">
        /// <para>The value of Pattern is invalid.</para>
        /// <para>-or-</para>
        /// <para>The value of Null is invalid.</para>
        /// <para>-or-</para>
        /// <para>The value of NonFocusNull is invalid.</para>
        /// </exception>
        public MaskFormat(string pattern, string focusEmpty, string displayEmpty)
        {
            this.Pattern = pattern;
            this.Null = focusEmpty;
            this.NonFocusNull = displayEmpty;
        }

        /// <summary>
        /// Creates a new mask format with the specified owner and pattern.
        /// </summary>
        /// <param name="owner">The owner of MaskFormat</param>
        /// <param name="pattern">The mask pattern string</param>
        /// <param name="focusEmpty">Indicates the string expressing the null value when the focus is on it</param>
        /// <param name="displayEmpty">Indicates the string expressing the null value when the focus is not on it</param>
        /// <exception cref="ArgumentException">
        /// <para>The value of Pattern is invalid.</para>
        /// <para>-or-</para>
        /// <para>The value of Null is invalid.</para>
        /// <para>-or-</para>
        /// <para>The value of NonFocusNull is invalid.</para>
        /// </exception>
        internal MaskFormat(IMaskFormat owner, string pattern, string focusEmpty, string displayEmpty)
        {
            this.Owner = owner;
            this.Pattern = pattern;
            this.Null = focusEmpty;
            this.NonFocusNull = displayEmpty;
        }
        #endregion

        #region Member
        /// <summary>
        /// Indicates the pattern string.
        /// </summary>
        private string _pattern = string.Empty;
        /// <summary>
        /// Indicates the string expressing the null value when the focus is on it.
        /// </summary>
        private string _null = string.Empty;
        /// <summary>
        /// Indicates the string expressing the null value when the focus is not on it.
        /// </summary>
        private string _nonFocusNull = string.Empty;
        /// <summary>
        /// Indicates the field collection object.
        /// </summary>
        private MaskFieldCollection _fields;

        private char _promptChar = '_';

        /// <summary>
        /// Used to save the real expression used for Null and NonFocusNull.
        /// </summary>
        private readonly string[] _expressions = new string[] { null, null };
        #endregion  end of Member

        /// <summary>
        /// Gets the default Format.
        /// </summary>
        internal static MaskFormat DefaultFormat
        {
            get
            {
                return new MaskFormat("", "", "");
            }
        }
        private IMaskFormat _owner;
        /// <summary>
        /// Gets or sets the field data, which holds the field collection.
        /// </summary>
        internal new IMaskFormat Owner
        {
            get
            {
                return _owner;
            }
            set
            {
                _owner = value;
            }
        }

        internal char PromptChar
        {
            get
            {
                if (this.Owner == null)
                {
                    return this._promptChar;
                }
                else
                {
                    return this.Owner.PromptChar;
                }

            }
            set
            {
                this._promptChar = value;
            }
        }

        public string Null
        {
            get
            {
                if (ViewState["Null"] != null)
                    return (string)(ViewState["Null"]);
                else
                    return this._null;
            }
            set
            {
                if (ParseFillExpression(this._null, value, 0))
                {
                    this._null = value;
                    this.OnPropertyChanged("Null");
                }
                ViewState["Null"] = this._null;
            }
        }

        public string NonFocusNull
        {
            get
            {
                if (ViewState["NonFocusNull"] != null)
                {
                    return (string)(ViewState["NonFocusNull"]);
                }
                else
                {
                    return this._nonFocusNull;
                }
            }
            set
            {
                if (ParseFillExpression(this._nonFocusNull, value, 1))
                {
                    this._nonFocusNull = value;
                }
                ViewState["NonFocusNull"] = this._nonFocusNull;
            }
        }

        public new string Pattern
        {
            get
            {
                if (ViewState["Pattern"] != null)
                {
                    return (string)(ViewState["Pattern"]);
                }
                else
                {
                    return this._pattern;
                }
            }
            set
            {
                try
                {
                    if (this._pattern != value)
                    {
                        if (!string.IsNullOrEmpty(value))
                        {
                            MaskFieldCollection fields = null;
                            this.ParsePattern(value, out fields);
                            this._fields = fields;
                        }
                        else
                            this._fields = null;

                        this._pattern = value;

                        this.ParseFillExpression(this._expressions[0], this._null, 0);
                        this.ParseFillExpression(this._expressions[1], this._nonFocusNull, 1);
                        this.OnPropertyChanged("Pattern");
                    }
                    ViewState["Pattern"] = value;
                }
                catch
                {
                    throw new ArgumentException(string.Format(C1Localizer.GetString("C1Input.Common.InvalidParameter"), "MaskFormat"));
                }
            }
        }

        /// <summary>
        /// Gets the field collection in MaskFieldCollection.
        /// </summary>
        internal MaskFieldCollection Fields
        {
            get
            {
                return this._fields;
            }
        }

        /// <summary>
        /// Gets the null expression.
        /// </summary>
        internal string NullExpression
        {
            get
            {
                return this._expressions[0] ?? this.Null;
            }
        }

        /// <summary>
        /// Gets the null expression when it is non-focus status.
        /// </summary>
        internal string NonFocusNullExpression
        {
            get
            {
                return this._expressions[1] ?? this.NonFocusNull;
            }
        }

        #region Clone and Equals method.
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        public virtual object Clone()
        {
            return new MaskFormat(this.Pattern, this.Null, this.NonFocusNull);
        }

        /// <summary>
        /// Returns the generated hash code for this <b>DateFormat</b>.
        /// </summary>
        /// <returns>hash code for this instance.</returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// Tests whether the specified object is equal to this DateFormat.
        /// </summary>
        /// <param name="obj">The object to compare to this object</param>
        /// <returns><b>true</b> if obj is an instance of <b>DateFormat</b> and is equal to this object; otherwise, <b>false</b>.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (!(obj is MaskFormat))
            {
                return false;
            }

            MaskFormat maskFormat = (MaskFormat)obj;

            if (maskFormat.Pattern == this.Pattern && maskFormat.Null == this.Null && maskFormat.NonFocusNull == this.NonFocusNull)
            {
                return true;
            }

            return false;
        }
        #endregion

        /// <summary>
        /// Parses the fill expression based on the new expression.
        /// </summary>
        /// <param name="oldExpression">The original expression</param>
        /// <param name="expression">The new expression try to set</param>
        /// <param name="index">Indicates which one is used. 0 - Null, 1 - NonFocusNull</param>
        /// <returns>true if parses successfully; false otherwise</returns>
        private bool ParseFillExpression(string oldExpression, string expression, int index)
        {
            if (expression == "{}")
            {
                this._expressions[index] = "{}";
                return true;
            }

            if (!string.IsNullOrEmpty(expression))
            {
                // check whether it's a valid one
                try
                {
                    string format = expression;
                    char fillingChar;
                    Format.ParseFillingString(ref format, out fillingChar);
                    // update the real expression here                    
                    if (fillingChar != '\0')
                    {
                        if (!this.NullParse(format))
                        {
                            throw new ArgumentException();
                        }
                        if (this.Fields != null && this.Fields.Count > 0)
                        {
                            this._expressions[index] = this.Fields.GetFillingString(fillingChar);
                        }
                        else
                        {
                            this._expressions[index] = expression;
                        }
                    }
                    else
                    {
                        this._expressions[index] = format;
                    }
                }
                catch
                {
                    throw new ArgumentException(string.Format(C1Localizer.GetString("C1Input.Common.InvalidParameter"), index == 0 ? "Null" : "NonFocusNull"));
                }
            }
            else
            {
                this._expressions[index] = null;//string.Empty;
            }

            return true;
        }

        /// <summary>
        /// Parses format of null and nonfocusnull, can not more .
        /// </summary>
        internal bool NullParse(string text)
        {
            int startIndex = FindSpecialCh(text, "{", 0);
            int endIndex = FindSpecialCh(text, "}", 0);

            if (startIndex > -1 && endIndex > -1)
            {
                if (endIndex > startIndex)
                {
                    //not more '{'
                    if (FindSpecialCh(text, "{", startIndex + 1) > -1)
                    {
                        return false;
                    }

                    if (FindSpecialCh(text, "}", endIndex + 1) > -1)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Finds a special character.
        /// </summary>
        /// <param name="text">Format of null or nonefocusnull</param>
        /// <param name="specialCh">Special character</param>
        /// <param name="idx">Start index</param>
        internal int FindSpecialCh(string text, string specialCh, int idx)
        {
            int sNum = 0;
            int startIndex = idx - 1;

            do
            {
                startIndex = text.IndexOf(specialCh, startIndex + 1);
                if (startIndex == 0 || startIndex == -1)
                {
                    return startIndex;
                }
                for (int sCount = startIndex - 1; sCount > 0; sCount--)
                {
                    if (text[sCount] != '\\')
                    {
                        break;
                    }
                    sNum++;
                }
            }
            while (text[startIndex - 1] == '\\' && (sNum % 2) != 0);

            return startIndex;
        }

        /// <summary>
        /// Parses the regular expression format string.
        /// </summary>
        /// <param name="pattern">The pattern string</param>
        /// <param name="fields">The field collection as result</param>
        internal virtual void ParsePattern(string pattern, out MaskFieldCollection fields)
        {
            // The empty string is invalid pattern.
            if (string.IsNullOrEmpty(pattern))
            {
                throw new ArgumentException(string.Format(C1Localizer.GetString("C1Input.Common.ParameterIsEmpty"), "MaskFormat"));
            }

            fields = new MaskFieldCollection(this.Owner);

            int caret = 0;
            string promptS = string.Empty;
            KeywordType lastKeyType = KeywordType.PromptChar;
            KeywordType nextKeyType = KeywordType.CharSubset;
            KeywordType keyType;
            int keyLen;
            string keyWord = this.GetKeyWord(pattern, caret, out keyType, out keyLen);
            while (pattern.Length > caret)
            {
                FilterField.CharacterFilter filter = null;
                switch (keyType)
                {
                    case KeywordType.Unknow:
                        throw new ArgumentException(string.Format(C1Localizer.GetString("C1Input.Common.InvalidParameter"), "MaskFormat"));
                    case KeywordType.CharSubset:
                        {
                            UnionFilter uf = null;
                            if (!this.AnalyseCharSubset(keyWord, ref uf))
                                throw new ArgumentException(string.Format(C1Localizer.GetString("C1Input.Common.InvalidParameter"), "MaskFormat"));
                            filter = uf;
                            lastKeyType = KeywordType.CharSubset;
                            break;
                        }
                    case KeywordType.DefinedCharAddition:
                        {
                            if (keyWord == "\\K")
                                filter = new HalfWidthKatakanaFilter();
                            else if (keyWord == "\\H")
                                filter = new HalfWidthFilter();
                            else if (keyWord == "\\N")
                                filter = new SBCSKatakanaFilter();
                            else if (keyWord == "\\" + DBCS_K)
                                filter = new FullWidthKatakanaFilter();
                            else if (keyWord == "\\" + DBCS_J)
                                filter = new HiraganaFilter();
                            else if (keyWord == "\\" + DBCS_M)
                                filter = new ShiftJISFilter();
                            else if (keyWord == "\\" + DBCS_I)
                                filter = new JISX0208Filter();
                            else if (keyWord == "\\" + DBCS_G)
                                filter = new DBCSHiraganaFilter();
                            else if (keyWord == "\\" + DBCS_N)
                                filter = new DBCSKatakanaFilter();
                            else if (keyWord == "\\" + DBCS_Z)
                                filter = new FullWidthFilter();
                            else if (keyWord == "\\" + DBCS_T)
                                filter = new SurrogateFilter();
                            else if (keyWord == "\\" + DBCS_E)
                                filter = new EmojiFilter();
                            else if (keyWord == "\\" + DBCS_V)
                                filter = new IVSFilter();
                            else
                                throw new ArgumentNullException();

                            lastKeyType = KeywordType.CharSubset;

                            break;
                        }
                    case KeywordType.EnumGroup:
                        #region
                        ArrayList members;
                        if (this.AnalyseEnumGroup(keyWord, out members))
                        {
                            fields.Add(new EnumField(members));
                            lastKeyType = KeywordType.EnumGroup;
                            caret += keyLen;
                            keyWord = this.GetKeyWord(pattern, caret, out keyType, out keyLen);
                            continue;
                        }
                        else
                            throw new ArgumentException(string.Format(C1Localizer.GetString("C1Input.Common.InvalidParameter"), "MaskFormat"));
                        #endregion

                    case KeywordType.OctalAscII:
                        #region
                        try
                        {
                            Int16 tempInt = Convert.ToInt16(keyWord.Substring(1), 8);
                            if (tempInt == 0)
                            {
                                throw new ArgumentException();
                            }
                            char c = (char)tempInt;
                            if (lastKeyType == KeywordType.PromptChar)
                                promptS += c;
                            else
                            {
                                promptS = c.ToString();
                                lastKeyType = KeywordType.PromptChar;
                            }
                        }
                        catch
                        {
                            throw new ArgumentException(string.Format(C1Localizer.GetString("C1Input.Common.InvalidParameter"), "MaskFormat"));
                        }
                        #endregion

                        break;
                    case KeywordType.HexAscII:
                    case KeywordType.HexUnicode:
                        #region
                        try
                        {
                            Int16 tempInt = Convert.ToInt16(keyWord.Substring(2), 16);
                            if (tempInt == 0)
                            {
                                throw new ArgumentException();
                            }
                            char c = (char)tempInt;
                            if (lastKeyType == KeywordType.PromptChar)
                                promptS += c;
                            else
                            {
                                promptS = c.ToString();
                                lastKeyType = KeywordType.PromptChar;
                            }
                        }
                        catch
                        {
                            throw new ArgumentException(string.Format(C1Localizer.GetString("C1Input.Common.InvalidParameter"), "MaskFormat"));
                        }
                        #endregion

                        break;
                    case KeywordType.PromptChar:
                        #region
                        if (lastKeyType == KeywordType.PromptChar)
                            promptS += keyWord;
                        else
                        {
                            promptS = keyWord;
                            lastKeyType = KeywordType.PromptChar;
                        }
                        #endregion

                        break;
                    case KeywordType.Quantifier:
                        throw new ArgumentException(string.Format(C1Localizer.GetString("C1Input.Common.InvalidParameter"), "MaskFormat"));
                }
                caret += keyLen;

                int max = 1, min = 1;
                if (caret < pattern.Length)		// Next KeyWord is necessary for to get the field maximum and minimum value etc.
                {
                    keyWord = this.GetKeyWord(pattern, caret, out keyType, out keyLen);

                    switch (keyType)
                    {
                        case KeywordType.Unknow:
                            throw new ArgumentException(string.Format(C1Localizer.GetString("C1Input.Common.InvalidParameter"), "MaskFormat"));
                        case KeywordType.CharSubset:
                        case KeywordType.DefinedCharAddition:
                        case KeywordType.EnumGroup:
                            nextKeyType = KeywordType.CharSubset;
                            break;
                        case KeywordType.Quantifier:
                            if (!this.AnalyseQuantifier(keyWord, ref min, ref max))
                                throw new ArgumentException(string.Format(C1Localizer.GetString("C1Input.Common.InvalidParameter"), "MaskFormat"));
                            caret += keyLen;
                            keyWord = this.GetKeyWord(pattern, caret, out keyType, out keyLen);
                            nextKeyType = KeywordType.Quantifier;
                            break;
                        case KeywordType.OctalAscII:
                        case KeywordType.HexUnicode:
                        case KeywordType.HexAscII:
                        case KeywordType.PromptChar:
                            nextKeyType = KeywordType.PromptChar;
                            break;
                    }
                    if (lastKeyType == KeywordType.PromptChar)
                    {
                        if (nextKeyType != KeywordType.PromptChar)
                        {
                            // Quantifier is not allowed occur after PromptField
                            if (nextKeyType == KeywordType.Quantifier)
                                throw new ArgumentException(string.Format(C1Localizer.GetString("C1Input.Common.InvalidParameter"), "MaskFormat"));

                            fields.Add(new PromptField(promptS));
                        }
                    }
                    else
                    {
                        fields.Add(new FilterField(min, max, filter));
                    }
                }
                else
                {
                    if (lastKeyType == KeywordType.PromptChar)
                    {
                        fields.Add(new PromptField(promptS));
                    }
                    else
                    {
                        fields.Add(new FilterField(1, 1, filter));
                    }
                }
            }
            int l = 0;
            for (; l < fields.Count; l++)
            {
                if (!(fields[l] is PromptField))
                    break;
            }
            if (l == fields.Count)
                throw new ArgumentException(string.Format(C1Localizer.GetString("C1Input.Common.InvalidParameter"), "MaskFormat"));

        }

        /// <summary>
        /// Analyses the character subset.
        /// </summary>
        /// <param name="subset">Character subset string</param>
        /// <param name="uf">Result of analysing</param>
        /// <returns>If analysing is successful,return true,otherwise return false.</returns>
        /// <remarks>Subset string is a string that start with '[' character and end with ']' character.</remarks>
        internal bool AnalyseCharSubset(string subset, ref UnionFilter uf)
        {
            ArrayList filters = new ArrayList();
            int caret = 1;
            bool isInclude = true;
            string include = string.Empty;

            if (subset == "[]")
            {
                filters.Add(new FullWidthFilter());
                filters.Add(new HalfWidthFilter());
                if (uf != null && uf.Filters.Length > 0)
                {
                    for (int i = 0; i < uf.Filters.Length; i++)
                    {
                        filters.Add(uf.Filters[i]);
                    }
                }

                uf = new UnionFilter((FilterField.CharacterFilter[])filters.ToArray(typeof(FilterField.CharacterFilter)));
                return true;
            }
            if (subset == "[^]")
            {
                filters.Add(new FullWidthFilter());
                filters.Add(new HalfWidthFilter());
                if (uf != null && uf.Filters.Length > 0)
                {
                    for (int i = 0; i < uf.Filters.Length; i++)
                    {
                        filters.Add(uf.Filters[i]);
                    }
                }

                uf = new UnionFilter((FilterField.CharacterFilter[])filters.ToArray(typeof(FilterField.CharacterFilter)));
                uf.Filters[0].Include = false;
                uf.Filters[1].Include = false;
                return true;
            }

            if (subset[caret] == '^')
            {
                isInclude = false;
                caret++;
            }

            while (caret < subset.Length - 1)
            {
                char cc = subset[caret];
                KeywordType keyType = KeywordType.HexAscII;
                int length = 0;

                switch (cc)
                {
                    case '-':
                        return false;
                    case '^':
                        if (include.Length > 0)
                        {
                            LimitedFilter lf = new LimitedFilter(include, string.Empty);
                            lf.Include = isInclude;
                            filters.Add(lf);
                        }
                        //add filter before "^"
                        if (filters.Count > 0)
                        {
                            uf = new UnionFilter((FilterField.CharacterFilter[])filters.ToArray(typeof(FilterField.CharacterFilter)));
                            //can not be excluded twice
                            for (int i = 0; i < uf.Filters.Length; i++)
                            {
                                if (!uf.Filters[i].Include)
                                {
                                    return false;
                                }
                            }
                            return this.AnalyseCharSubset("[" + subset.Substring(caret, subset.Length - caret), ref uf);
                        }
                        else
                        {
                            return false;
                        }
                    case '\\': // "\\";"\]";"\[";"\-";"\^";"\K";"\u0098"......
                        {
                            // Change the "\u****" to a char.
                            if (this.GetEscapeCharLength(subset.Substring(caret), ref keyType, ref length))
                            {
                                int baseValue = 16;
                                string s = null;
                                if (keyType == KeywordType.OctalAscII)
                                {
                                    baseValue = 8;
                                    s = subset.Substring(caret + 1, length - 1);
                                }
                                else
                                    s = subset.Substring(caret + 2, length - 2);

                                subset = subset.Remove(caret, length);
                                subset = subset.Insert(caret, ((char)Convert.ToInt16(s, baseValue)).ToString());
                                continue;
                            }

                            char c = subset[caret + 1];
                            if (c == 'A')
                            {
                                filters.Add(new RangeFilter('A', 'Z'));
                                caret++;
                            }
                            else if (c == DBCS_A)
                            {
                                filters.Add(new RangeFilter(DBCS_A, DBCS_Z));
                                caret++;
                            }
                            else if (c == 'a')
                            {
                                filters.Add(new RangeFilter('a', 'z'));
                                caret++;
                            }
                            else if (c == DBCS_a)
                            {
                                filters.Add(new RangeFilter(DBCS_a, DBCS_z));
                                caret++;
                            }
                            else if (c == 'D')
                            {
                                filters.Add(new RangeFilter('0', '9'));
                                caret++;
                            }
                            else if (c == DBCS_D)
                            {
                                filters.Add(new RangeFilter(DBCS_0, DBCS_9));
                                caret++;
                            }
                            else if (c == 'B')
                            {
                                filters.Add(new RangeFilter('0', '1'));
                                caret++;
                            }
                            else if (c == DBCS_B)
                            {
                                filters.Add(new RangeFilter(DBCS_0, DBCS_1));
                                caret++;
                            }
                            else if (c == 'X')
                            {
                                filters.Add(new RangeFilter('0', '9'));
                                filters.Add(new RangeFilter('A', 'F'));
                                filters.Add(new RangeFilter('a', 'f'));
                                caret++;
                            }
                            else if (c == DBCS_X)
                            {
                                filters.Add(new RangeFilter(DBCS_0, DBCS_9));
                                filters.Add(new RangeFilter(DBCS_A, DBCS_F));
                                filters.Add(new RangeFilter(DBCS_a, DBCS_f));
                                caret++;
                            }
                            else if (c == 'W')
                            {
                                filters.Add(new RangeFilter('0', '9'));
                                filters.Add(new RangeFilter('A', 'Z'));
                                filters.Add(new RangeFilter('a', 'z'));

                                include += '_';
                                caret++;
                            }
                            else if (c == DBCS_W)
                            {
                                filters.Add(new RangeFilter(DBCS_0, DBCS_9));
                                filters.Add(new RangeFilter(DBCS_A, DBCS_Z));
                                filters.Add(new RangeFilter(DBCS_a, DBCS_z));

                                include += DBCS__;
                                caret++;
                            }
                            else if (c == 'K')
                            {
                                filters.Add(new HalfWidthKatakanaFilter());
                                caret++;
                            }
                            else if (c == 'H')
                            {
                                filters.Add(new HalfWidthFilter());
                                caret++;
                            }
                            else if (c == DBCS_K)
                            {
                                filters.Add(new FullWidthKatakanaFilter());
                                caret++;
                            }
                            else if (c == DBCS_J)
                            {
                                filters.Add(new HiraganaFilter());
                                caret++;
                            }
                            else if (c == DBCS_M)
                            {
                                filters.Add(new ShiftJISFilter());
                                caret++;
                            }
                            else if (c == DBCS_I)
                            {
                                filters.Add(new JISX0208Filter());
                                caret++;
                            }
                            else if (c == 'N')
                            {
                                filters.Add(new SBCSKatakanaFilter());
                                caret++;
                            }
                            else if (c == DBCS_N)
                            {
                                filters.Add(new DBCSKatakanaFilter());
                                caret++;
                            }
                            else if (c == DBCS_G)
                            {
                                filters.Add(new DBCSHiraganaFilter());
                                caret++;
                            }
                            else if (c == DBCS_Z)
                            {
                                filters.Add(new FullWidthFilter());
                                caret++;
                            }
                            else if (c == DBCS_T)
                            {
                                filters.Add(new SurrogateFilter());
                                caret++;
                            }
                            else if (c == DBCS_E)
                            {
                                filters.Add(new EmojiFilter());
                                caret++;
                            }
                            else if (c == DBCS_V)
                            {
                                filters.Add(new IVSFilter());
                                caret++;
                            }
                            else if (c == '\\' || c == ']' || c == '[' || c == '-' || c == '^')
                            {
                                if (include.IndexOf(c) != -1)
                                {
                                    caret++;
                                    continue;
                                }

                                include += c;
                                caret++;
                            }
                            else
                                return false;
                        }
                        break;
                    default:
                        {
                            if (subset.Length > caret + 2)
                            {
                                if (subset[caret + 1] == '-')
                                {
                                    // Change the "\u****" or "\***" or "\x**" to a char.
                                    if (this.GetEscapeCharLength(subset.Substring(caret + 2), ref keyType, ref length))
                                    {
                                        string s;
                                        int baseValue = 16;
                                        if (keyType == KeywordType.OctalAscII)
                                        {
                                            baseValue = 8;
                                            s = subset.Substring(caret + 2 + 1, length - 1);
                                        }
                                        else
                                            s = subset.Substring(caret + 2 + 2, length - 2);

                                        subset = subset.Remove(caret + 2, length);
                                        subset = subset.Insert(caret + 2, ((char)Convert.ToInt16(s, baseValue)).ToString());
                                    }

                                    // Get range start and range end.
                                    char rangeStart;
                                    char rangeEnd;
                                    if (subset[caret] > subset[caret + 2])
                                    {
                                        rangeStart = subset[caret + 2];
                                        rangeEnd = subset[caret];
                                    }
                                    else
                                    {
                                        rangeStart = subset[caret];
                                        rangeEnd = subset[caret + 2];
                                    }

                                    // Create the range filter.
                                    RangeFilter rf = new RangeFilter(rangeStart, rangeEnd);
                                    filters.Add(rf);

                                    caret += 2;
                                    break;
                                }
                            }

                            if (include.IndexOf(cc) != -1)
                            {
                                caret++;
                                continue;
                            }

                            include += cc;

                            break;
                        }
                }

                caret++;
            }

            if (include.Length > 0)
                filters.Add(new LimitedFilter(include, string.Empty));
            int icount = filters.Count;

            if (uf != null && uf.Filters.Length > 0)
            {
                for (int i = 0; i < uf.Filters.Length; i++)
                {
                    filters.Add(uf.Filters[i]);
                }
            }


            uf = new UnionFilter((FilterField.CharacterFilter[])filters.ToArray(typeof(FilterField.CharacterFilter)));
            for (int i = 0; i < icount; i++)
            {
                uf.Filters[i].Include = isInclude;
            }

            return true;
        }

        /// <summary>
        /// Analyses the enumeration group string.
        /// </summary>
        /// <param name="groupStr">Enumeration group string</param>
        /// <param name="members">Result of analysing is a string array</param>
        /// <returns>If analysing is successful,return true,otherwise return false.</returns>
        /// <remarks>Subset string is a string that start with '(' character and end with ')'character.And Members are split by '|' character.</remarks>
        internal bool AnalyseEnumGroup(string groupStr, out ArrayList members)
        {
            groupStr = groupStr.Substring(1, groupStr.Length - 2);
            members = new ArrayList();
            int caret = 0, last = 0;
            while (caret < groupStr.Length)
            {
                int index = groupStr.IndexOfAny(new char[] { '|', ')', '\\', '(' }, caret);
                if (index == -1)
                {
                    string member = groupStr.Substring(last);
                    if (member.Length == 0)
                        return false;
                    members.Add(member);
                    caret = groupStr.Length;
                    continue;
                }
                if (groupStr[index] == '\\')
                {
                    char c = groupStr[index + 1];
                    if (c == '\\' || c == ')' || c == '|' || c == '(')
                    {
                        groupStr = groupStr.Remove(index, 1);
                        caret = index + 1;
                        if (caret == groupStr.Length)
                        {
                            string member = groupStr.Substring(last);
                            if (member.Length == 0)
                                return false;
                            members.Add(member);
                        }
                        continue;
                    }
                    else
                        return false;
                }
                if (groupStr[index] == '|')
                {
                    string member = groupStr.Substring(last, index - last);
                    if (member.Length == 0)
                        return false;
                    members.Add(member);
                    caret = index + 1;
                    last = caret;
                }
                else
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Analyses the quantifier string.
        /// </summary>
        /// <param name="quantifierStr">Quantifier string</param>
        /// <param name="min">Return the quantifier minimum</param>
        /// <param name="max">Return the quantifier maximum</param>
        /// <returns>If analysing is successful,return true,otherwise return false.</returns>
        /// <remarks>Subset string is a string that start with '{' character and end with '}'character.And maximum and minimum are split by ',' character.</remarks>
        internal bool AnalyseQuantifier(string quantifierStr, ref int min, ref int max)
        {
            int index = quantifierStr.IndexOf(',', 1);
            min = max = -1;
            if (index == -1)
            {
                try
                {
                    min = Convert.ToInt32(quantifierStr.Substring(1, quantifierStr.Length - 2));
                    max = min;
                    return max > 0;
                }
                catch
                {
                    return false;
                }
            }
            try
            {
                min = Convert.ToInt32(quantifierStr.Substring(1, index - 1));
                if (index == quantifierStr.Length - 2)
                    max = int.MaxValue;
                else
                    max = Convert.ToInt32(quantifierStr.Substring(index + 1, quantifierStr.Length - index - 2));
                if (min < 0 || min > max || max == 0)
                    return false;
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the first KeyWord after the index from format string.
        /// </summary>
        /// <param name="format">Format string</param>
        /// <param name="index">Start index</param>
        /// <param name="keyType">Return the KeyWord type</param>
        /// <param name="keyLen">Return the KeyWord Original length</param>
        /// <returns>If analysing is successful,return true,otherwise return false.</returns>
        /// <remarks>Maybe the return string length not equals the keyLen value.</remarks>
        internal string GetKeyWord(string format, int index, out KeywordType keyType, out int keyLen)
        {
            keyLen = 0;
            keyType = KeywordType.Unknow;
            if (index >= format.Length)
                return null;

            char cFirst = format[index];
            switch (cFirst)
            {
                case '\\':
                    #region Is Escape Char ?
                    if (this.GetEscapeCharLength(format.Substring(index), ref keyType, ref keyLen))
                    {
                        return format.Substring(index, keyLen);
                    }
                    #endregion
                    #region Is Defined char ?
                    char cSecond = format[index + 1];

                    if (cSecond == 'A')
                    {
                        keyLen = 2;
                        keyType = KeywordType.CharSubset;
                        return "[A-Z]";
                    }
                    else if (cSecond == DBCS_A)
                    {
                        keyLen = 2;
                        keyType = KeywordType.CharSubset;
                        return "[" + DBCS_A + "-" + DBCS_Z + "]";
                    }
                    else if (cSecond == 'a')
                    {
                        keyLen = 2;
                        keyType = KeywordType.CharSubset;
                        return "[a-z]";
                    }
                    else if (cSecond == DBCS_a)
                    {
                        keyLen = 2;
                        keyType = KeywordType.CharSubset;
                        return "[" + DBCS_a + "-" + DBCS_z + "]";
                    }
                    else if (cSecond == 'D')
                    {
                        keyLen = 2;
                        keyType = KeywordType.CharSubset;
                        return "[0-9]";
                    }
                    else if (cSecond == DBCS_D)
                    {
                        keyLen = 2;
                        keyType = KeywordType.CharSubset;
                        return "[" + DBCS_0 + "-" + DBCS_9 + "]";
                    }
                    else if (cSecond == 'B')
                    {
                        keyLen = 2;
                        keyType = KeywordType.CharSubset;
                        return "[0-1]";
                    }
                    else if (cSecond == DBCS_B)
                    {
                        keyLen = 2;
                        keyType = KeywordType.CharSubset;
                        return "[" + DBCS_0 + "-" + DBCS_1 + "]";
                    }
                    else if (cSecond == 'X')
                    {
                        keyLen = 2;
                        keyType = KeywordType.CharSubset;
                        return "[0-9A-Fa-f]";
                    }
                    else if (cSecond == DBCS_X)
                    {
                        keyLen = 2;
                        keyType = KeywordType.CharSubset;
                        return "[" + DBCS_0 + "-" + DBCS_9 + DBCS_A + "-" + DBCS_F + DBCS_a + "-" + DBCS_f + "]";
                    }
                    else
                        if (cSecond == 'W')
                        {
                            keyLen = 2;
                            keyType = KeywordType.CharSubset;
                            return "[a-zA-Z_0-9]";
                        }
                        else if (cSecond == DBCS_W)
                        {
                            keyLen = 2;
                            keyType = KeywordType.CharSubset;
                            return "[" + DBCS_a + "-" + DBCS_z + DBCS_A + "-" + DBCS_Z + DBCS__ + DBCS_0 + "-" + DBCS_9 + "]";
                        }
                        else if (cSecond == 'K' || cSecond == DBCS_K || cSecond == DBCS_J || cSecond == DBCS_Z || cSecond == 'H' || cSecond == DBCS_T || cSecond == DBCS_M || cSecond == DBCS_I || cSecond == DBCS_G || cSecond == DBCS_N || cSecond == 'N' || cSecond == DBCS_E || cSecond == DBCS_V)
                        {
                            keyLen = 2;
                            keyType = KeywordType.DefinedCharAddition;
                            return format.Substring(index, 2);
                        }
                        else if (this.IsSpecialChar(cSecond))
                        {
                            keyLen = 2;
                            keyType = KeywordType.PromptChar;
                            return format.Substring(index + 1, 1);//exclude the '\' character.
                        }
                        else
                        {
                            return string.Empty;// "\\";
                        }

                    #endregion
                case '{':
                    #region Quantifier string
                    {
                        int idx = format.IndexOf('}', index + 1);
                        if (idx == -1)
                            return string.Empty;
                        keyType = KeywordType.Quantifier;
                        keyLen = idx + 1 - index;
                        return format.Substring(index, keyLen);
                    }
                    #endregion
                case '[':
                    #region Char Subset string
                    {
                        int idx = index;
                        //calculate '\\' number
                        int sNum = 0;
                        do
                        {
                            idx = format.IndexOf(']', idx + 1);
                            if (idx == -1)
                                return string.Empty;
                            for (int sCount = idx - 1; sCount > 0; sCount--)
                            {
                                if (format[sCount] != '\\')
                                {
                                    break;
                                }
                                sNum++;
                            }
                        }
                        while (format[idx - 1] == '\\' && (sNum % 2) != 0);

                        keyType = KeywordType.CharSubset;
                        keyLen = idx + 1 - index;
                        return format.Substring(index, keyLen);
                    }
                    #endregion
                case '(':
                    #region Enum group string
                    {
                        int idx = index;
                        do
                        {
                            idx = format.IndexOf(')', idx + 1);
                            if (idx == -1)
                                return string.Empty;
                        }
                        while (format[idx - 1] == '\\');

                        keyType = KeywordType.EnumGroup;
                        keyLen = idx + 1 - index;
                        return format.Substring(index, keyLen);
                    }
                    #endregion
                case '*':
                    keyLen = 1;
                    keyType = KeywordType.Quantifier;
                    return "{0,}";
                case '+':
                    keyLen = 1;
                    keyType = KeywordType.Quantifier;
                    return "{1,}";
                case '?':
                    keyLen = 1;
                    keyType = KeywordType.Quantifier;
                    return "{0,1}";
                default:
                    #region Prompt string
                    {
                        int i = index + 1;
                        for (; i < format.Length; i++)
                        {
                            if (this.IsSpecialChar(format[i]))
                                break;
                        }

                        keyType = KeywordType.PromptChar;
                        keyLen = i - index;
                        return format.Substring(index, keyLen);
                    }
                    #endregion
            }
        }

        #region The DBCS const
        public static readonly char DBCS_A;	// Matches any upper case DBCS alphabet [Ａ-Ｚ].
        public static readonly char DBCS_a;	// Matches any lower case DBCS alphabet [Eｚ].
        public static readonly char DBCS_B;	// Matches DBCS binary.  Same as [０-１].
        public static readonly char DBCS_D;	// Matches any DBCS decimal digit. Same as [０-９].
#if !EXTENDER 
        [SmartAssembly.Attributes.DoNotObfuscate]
#endif
        public static readonly char DBCS_J;	// \Ｊ	Hiragana
#if !EXTENDER 
        [SmartAssembly.Attributes.DoNotObfuscate]
#endif
        public static readonly char DBCS_K;	// \Ｋ	DBCS Katakana
        public static readonly char DBCS_W;	// Matches any DBCS word character. It is same as [EｚＡ-Ｚ＿０-９]. reonlyad   
        public static readonly char DBCS_X;	// Matches DBCS hexadecimal.  Same as [０-９Ａ-Ｆ].
#if !EXTENDER 
        [SmartAssembly.Attributes.DoNotObfuscate]
#endif
        public static readonly char DBCS_Z;	// All DBCS characters.
        public static readonly char DBCS_T; // Four bytes characters.
        public static readonly char DBCS_M; // ShiftJIS characters.
        public static readonly char DBCS_I; // JISX0208 characters.
#if !EXTENDER 
        [SmartAssembly.Attributes.DoNotObfuscate]
#endif
        public static readonly char DBCS_N; // Only large DBCS Katakana
#if !EXTENDER 
        [SmartAssembly.Attributes.DoNotObfuscate]
#endif
        public static readonly char DBCS_G; // Only large hiranana
        public static readonly char DBCS_E; // Only emoji char.
        public static readonly char DBCS_V; // Only ivs char.
        public static readonly char DBCS_z;
        public static readonly char DBCS_0;
        public static readonly char DBCS_1;
#if !EXTENDER 
        [SmartAssembly.Attributes.DoNotObfuscate]
#endif
        public static readonly char DBCS_9;
        public static readonly char DBCS_F;
        public static readonly char DBCS__;
        public static readonly char DBCS_f;
        public PropertyChangedEventHandler PropertyChanged;
        #endregion

        /// <summary>
        /// Gets an escape character length and type.
        /// </summary>
        /// <param name="format">The format string</param>
        /// <param name="keyType">The escape character type</param>
        /// <param name="length">The escape character length</param>
        /// <returns>Whether an escape char at the format string head.</returns>
        internal bool GetEscapeCharLength(string format, ref KeywordType keyType, ref int length)
        {
            if (format[0] != '\\')
                return false;

            const int caret = 1;

            char c = format[1];
            if (c == 'x')
            {
                if (format.Length < caret + 3)
                    return false;

                length = 4;
                keyType = KeywordType.HexAscII;
                return true;
            }
            else if (c == 'u')
            {
                if (format.Length < caret + 5)
                    return false;

                length = 6;
                keyType = KeywordType.HexUnicode;
                return true;
            }
            else if (this.IsOctalChar(c))
            {
                if (format.Length < caret + 3)
                    return false;

                length = 4;
                keyType = KeywordType.OctalAscII;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determines whether the character is an octal digital.
        /// </summary>
        /// <param name="c">Character</param>
        /// <returns>if the character is octal digital return true,else return false</returns>
        protected bool IsOctalChar(char c)
        {
            return (c >= '0' && c <= '7');
        }

        /// <summary>
        /// Determines whether the character is a hexadecimal digital.
        /// </summary>
        /// <param name="c">Character</param>
        /// <returns>if the character is hex digital return true,else return false</returns>
        protected bool IsHexChar(char c)
        {
            return ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F'));
        }

        /// <summary>
        /// Determines whether the character is a special character.
        /// </summary>
        /// <param name="c">Character</param>
        /// <returns>if the character is special character return true,else return false</returns>
        protected bool IsSpecialChar(char c)
        {
            return (c == '\\' || c == '{' || c == '}' || c == '[' || c == ']' || c == '(' || c == ')' || c == '.' || c == '*' || c == '+' || c == '?');
        }

        internal void SetDirty()
        {
        }
    }
    #endregion
}
