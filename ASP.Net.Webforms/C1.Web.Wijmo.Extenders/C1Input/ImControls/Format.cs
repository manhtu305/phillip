﻿using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Text;
using System.Web.UI;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{
    #region The TranslateResult enumeration
    internal enum TranslateResult
    {
        Success = 0,
        Error = 1,
        OutOfMaxValue = 2,
        OutOfMinValue = 3,
        OutOfFormat = 4,
    }
    #endregion end of The TranslateResult enumeration

    #region The enum of FormatStatus Represents the status of the format
    /// <summary>
    /// Specifies the status of the format.
    /// </summary>
    internal enum FormatStatus
    {
        /// <summary>
        /// Format is null
        /// </summary>
        Null = 0,
        /// <summary>
        /// Format pattern is null
        /// </summary>
        NullPattern = 1,
        /// <summary>
        /// Uses system default format
        /// </summary>
        SystemFormat = 2,
        /// <summary>
        /// Uses stardard format
        /// </summary>
        StandardFormat = 3,
    }
    #endregion end of The enum of FormatStatus Represents.

    #region The Field Element Structure from ParseFormat
    /// <summary>
    /// The result of this element used in web control 
    /// when render data from server side to client side.
    /// </summary>
    internal struct ElementResult
    {
        /// <summary>
        /// The text of this element.
        /// </summary>
        public string StrResult;

        /// <summary>
        /// Gets or sets whether this element is a prompt field.
        /// </summary>
        public bool IsPrompt;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="strResult">text of this element</param>
        /// <param name="isPrompt">if this element is prompt field</param>
        public ElementResult(string strResult, bool isPrompt)
        {
            StrResult = strResult;
            IsPrompt = isPrompt;
        }
    }
    #endregion end of The Field Element Structure from ParseFormat

    #region Format class
    /// <summary>
    /// Represents the base class of all format object.
    /// </summary>
    /// <remarks>for Date control only...</remarks>
    internal abstract class Format
    {
        #region FormatKeyTable class
        /// <summary>
        /// To implement the keyword table.
        /// </summary>
        /// <remarks>
        /// FormatKeyTable is a hash table and is responsible for storing char key and min, max values.
        /// </remarks>
        protected class FormatKeyTable : Hashtable
        {
            #region The Key class
            /// <summary>
            /// Indicates the element in Key table.
            /// </summary>
            internal class Key
            {
                #region Members.
                /// <summary>
                /// Indicates the keyword character.
                /// </summary>
                private readonly char _keyCode = '\0';
                /// <summary>
                /// Indicates the minimum length.
                /// </summary>
                private readonly int _min;
                /// <summary>
                /// Indicates the maximum length.
                /// </summary>
                private readonly int _max;
                #endregion end of Members.

                #region Properties.
                /// <summary>
                /// Indicates the keyword character.
                /// </summary>
                public char KeyCode
                {
                    get
                    {
                        return this._keyCode;
                    }
                }

                /// <summary>
                /// Indicates the minimum length.
                /// </summary>
                public int Min
                {
                    get
                    {
                        return this._min;
                    }
                }

                /// <summary>
                /// Indicates the maximum length.
                /// </summary>
                public int Max
                {
                    get
                    {
                        return this._max;
                    }
                }
                #endregion end of Properties.

                #region Constructor
                /// <summary>
                /// Initializes a new instance of the Key class.
                /// </summary>
                /// <param name="keyCode">The keyword character</param>
                /// <param name="min">The minimum length</param>
                /// <param name="max">The maximum length</param>
                public Key(char keyCode, int min, int max)
                {
                    _max = 0;
                    this._keyCode = keyCode;
                    this._min = min;
                    this._max = max;
                }
                #endregion end of Constructor.
            }
            #endregion end of The Key class.

            #region Methods
            /// <summary>
            /// Adds a keyword into the keyword table.
            /// </summary>
            /// <param name="keyword">The keyword character</param>
            /// <param name="min">The minimum length</param>
            /// <param name="max">The maximum length</param>
            public void AddItem(char keyword, int min, int max)
            {
                if (!base.ContainsKey(keyword + "*_*"))
                {
                    base.Add(keyword + "*_*", true);
                }

                base.Add(keyword + min.ToString(CultureInfo.InvariantCulture) + "_" + max.ToString(CultureInfo.InvariantCulture), new FormatKeyTable.Key(keyword, min, max));
            }

            /// <summary>
            /// Determines whether the table contains the keyword.
            /// </summary>
            /// <param name="keyword">The keyword</param>
            /// <returns>A value whether the keyword is contained in the table.</returns>
            public override bool ContainsKey(object keyword)
            {
                return base.ContainsKey(keyword.ToString() + "*_*");
            }
            #endregion end of Methods.
        }
        #endregion end of FormatKeyTable class.

        #region Members
        /// <summary>
        /// The definition of translate character.
        /// </summary>
        private const char TRANSLATE_CHAR = '\\';
        /// <summary>
        /// Gets or sets whether this format for Display.
        /// </summary>
        private bool _forDisplay;
        /// <summary>
        /// Indicates the keyword table describing the relation of the keyword character and fields.
        /// </summary>
        /// <remarks>
        /// This member should be inited in each derived class.
        /// </remarks>
        protected static FormatKeyTable KeyTable = null;
        /// <summary>
        /// Indicates the format pattern string.
        /// </summary>
        private string _pattern = string.Empty;
        /// <summary>
        /// Indicates the field collection.
        /// </summary>
        private FieldCollection _fields;
        private object _owner;
        /// <summary>
        /// Save the result of Parse the Format.
        /// </summary>
        private ArrayList _results = new ArrayList();
        private IDictionary _viewState = new StateBag();
        #endregion end of Members.

        #region Constructor
        /// <summary>
        /// Creates a new format with default values.
        /// </summary>
        protected Format()
        {
        }

        /// <summary>
        /// Creates a new format with the specified owner control.
        /// </summary>
        /// <param name="owner"></param>
        internal Format(object owner)
        {
            this._owner = owner;
        }
        #endregion enf of Constructor.

        #region Properties
        /// <summary>
        /// Gets or sets the field data, which holds the field collection.
        /// </summary>
        internal virtual object Owner
        {
            get
            {
                return this._owner;
            }
            set
            {
                this._owner = value;
            }
        }

        /// <summary>
        /// Gets or sets whether this format is for display.
        /// </summary>
        protected bool ForDisplay
        {
            get
            {
                return this._forDisplay;
            }
            set
            {
                this._forDisplay = value;
            }
        }

        public IDictionary ViewState
        {
            get
            {
                return this._viewState;
            }
        }


        /// <summary>
        /// Gets or sets the pattern string.
        /// </summary>
        /// <exception cref="System.ArgumentException">
        /// The <c>Pattern</c> property is set to an invalid value.
        /// </exception>
        public string Pattern
        {
            get
            {
                if (ViewState["Pattern"] != null)
                {
                    this._pattern = (string)ViewState["Pattern"];
                }

                return this._pattern;
            }
            set
            {
                if (!this._pattern.Equals(value) && !this.ForDisplay)
                {
                    this.OnPropertyChanged("Pattern");
                }

                if (string.IsNullOrEmpty(value))
                {
                    this._pattern = value;
                    this._results = new ArrayList();
                }
                else
                {
                    FieldCollection fields;
                    this._results = this.ParsePattern(value, out fields);
                    this._fields = fields;
                    this._pattern = value;
                }

                ViewState["Pattern"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the result from the parse of pattern string.
        /// </summary>
        private FieldCollection Fields
        {
            get
            {
                return this._fields;
            }
        }

        /// <summary>
        /// Gets or sets the field data, which holds the field collection.
        /// </summary>
        internal ArrayList Results
        {
            get
            {
                return this._results;
            }
        }
        #endregion end of Properties.

        #region Methods

        protected void OnPropertyChanged(string propertyName)
        {
        }
        /// <summary>
        /// Creates a field according the keyword format string.
        /// </summary>
        /// <param name="keyword">The keyword character</param>
        /// <param name="length">The length of keyword character in pattern</param>
        /// <returns>The field object created.</returns>
        internal virtual Field CreateField(char keyword, int length)
        {
            return null;
        }

        /// <summary>
        /// Parses the pattern string.
        /// </summary>
        /// <param name="pattern">The pattern string</param>
        /// <param name="fields">The field collection as result</param>
        /// <remarks>for Date control only.</remarks>
        internal virtual ArrayList ParsePattern(string pattern, out FieldCollection fields)
        {
            // Create the field collection.
            fields = new FieldCollection();

            int length = pattern.Length;

            // Initialize the prompt string for prompt fields.
            string promptString = string.Empty;
            char charPattern;

            ArrayList results = new ArrayList();

            // Check all character in pattern.
            for (int i = 0; i < length; i++)
            {
                // Get a character in pattern.
                charPattern = pattern[i];
                // Check if the character is in the keyword table.
                if (charPattern == TRANSLATE_CHAR)
                {
                    // The character after TRANSLATE_CHAR is a prompt character.
                    i++;
                    if (i < length)
                    {
                        promptString += pattern[i];
                    }
                }
                else if (!Format.KeyTable.ContainsKey(charPattern))
                {	// The character is a prompt character.
                    promptString += pattern[i];
                }
                else if (Conflict(fields, charPattern))
                {	// The character is a prompt character, when it is conflict with previous field.
                    promptString += charPattern;
                }
                else
                {
                    if (promptString.Length > 0)
                    {
                        fields.Add(new PromptField(promptString));
                        promptString = string.Empty;
                    }

                    int len = 1;
                    while ((i + 1 < length) && charPattern == pattern[i + 1])
                    {
                        i++;
                        len++;
                    }
                    fields.Add(this.CreateField(charPattern, len));
                }
            }

            if (promptString.Length > 0)
            {
                fields.Add(new PromptField(promptString));
            }

            if (fields.Count == 0)
            {
                throw new ArgumentException(C1Localizer.GetString("C1Input.Common.InvalidParameter", "pattern"));
            }

            if (!this.ForDisplay && fields.Count == 1 && fields[0] is PromptField)
            {
                throw new ArgumentException(C1Localizer.GetString("C1Input.Common.InvalidParameter", "pattern"));
            }

            return results;
        }

        /// <summary>
        /// Determines whether the field indicated by the keyword character is in conflict with the one in the field collection.
        /// </summary>
        /// <param name="fields">Collection of fields</param>
        /// <param name="keyword">Keyword character</param>		
        internal virtual bool Conflict(FieldCollection fields, char keyword)
        {
            return false;
        }

        /// @ Remove all escape char! @
        /// <summary>
        /// Removes the unnecessary escape char from the specified XmlProcess string and return it back.
        /// </summary>
        /// <param name="format">
        /// [in]The format string that needs to process with  
        /// [out]The result string</param>
        static void RemoveEscapeChar(ref string format)
        {
            // there has \ in it, need special process
            if (format.IndexOf('\\') != -1)
            {
                StringBuilder sb = new StringBuilder();
                int len = format.Length;

                for (int i = 0; i < len; i++)
                {
                    if (format[i] == '\\')
                    {
                        if (i + 1 < len)
                        {
                            sb.Append(format[++i]);
                        }

                        continue;
                    }
                    sb.Append(format[i]);
                }
                format = sb.ToString();
            }
        }

        /// <summary>
        /// Parses the specified format string and get the filling char when one exists.
        /// </summary>
        /// <param name="format">String need to parse</param>
        /// <param name="fillChar">Used to save the filling char when the return value is true</param>
        /// <returns>true when there has a filling char; otherwise, false.</returns>
        internal static bool ParseFillingString(ref string format, out char fillChar)
        {
            fillChar = '\x0';

            if (string.IsNullOrEmpty(format)) return false;

            // nLPos ---> left position of '{'
            // nRPos ---> right position of '}'
            // Check '{' & '}' pairs
            int nLPos = format.IndexOf('{'), nRPos = format.IndexOf('}');

            // No '{' or '}' have been found. All translate chars are removed.
            if (nLPos == -1 && nRPos == -1)
            {
                RemoveEscapeChar(ref format);
                return false;
            }
            // Something interesting was found.
            else
            {
                // It can't be a translate '{'. We must find the valid one.
                while (nLPos > 0 && format[nLPos - 1] == '\\')
                {
                    nLPos = format.IndexOf('{', nLPos + 1);
                }

                // It can't be a translate '}'. We must find the valid one. 
                while (nRPos > 0 && format[nRPos - 1] == '\\')
                {
                    nRPos = format.IndexOf('}', nRPos + 1);
                }

                // Or, something like "_____}__{___" was found.
                if (nLPos < nRPos)
                {
                    // No valid '{' or '}' have been found.
                    if (nLPos == -1)	// Without '{' only '}', treat as normal one
                    {
                        RemoveEscapeChar(ref format);
                        return false;
                    }
                    else
                    {
                        // It's empty.
                        if (nRPos - nLPos == 1)
                        {
                            format = "";
                        }
                        else
                        {
                            // Something like "____{{}__" was found. It's invalid also.
                            if (format[nLPos + 1] == '{')
                                throw new ArgumentException();
                            // Something like "____{\{}__" was found.
                            else if (format[nLPos + 1] == '\\')
                            {
                                // The position of '{' is valid.
                                if (nLPos + 2 < nRPos)
                                {
                                    nLPos++;
                                }
                                else
                                {
                                    throw new ArgumentException();
                                }
                            }
                            // Got the fill char !
                            fillChar = format[nLPos + 1];
                        }
                        return true;
                    }
                }
                else
                {
                    RemoveEscapeChar(ref format);
                    return false;
                }
            }
        }
        #endregion end of Methods.

    }
    #endregion end of Format class.

    #region FormatConverter class
    /// <summary>
    /// Provides a base type converter class that implement the common items.
    /// </summary>
    internal class FormatConverter : TypeConverter
    {
        /// <summary>
        /// Returns whether this converter can convert an object of one type to the type of this converter.
        /// </summary>
        /// <param name="context">An ITypeDescriptorContext that provides a format context</param>
        /// <param name="sourceType">A Type that represents the type you want to convert from</param>
        /// <returns>true if this converter can perform the conversion; otherwise, false.</returns>
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }

            return base.CanConvertFrom(context, sourceType);
        }

        /// <summary>
        /// Returns whether this converter can convert the object to the specified type.
        /// </summary>
        /// <param name="context">An ITypeDescriptorContext that provides a format context</param>
        /// <param name="destinationType">A Type that represents the type you want to convert to</param>
        /// <returns>true if this converter can perform the conversion; otherwise, false.</returns>
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(InstanceDescriptor))
            {
                return true;
            }
            return base.CanConvertTo(context, destinationType);
        }

        /// <summary>
        /// Returns whether this object supports properties.
        /// </summary>
        /// <param name="context">An ITypeDescriptorContext that provides a format context</param>
        /// <returns>true if GetProperties should be called to find the properties of this object; otherwise, false. Always returns true for CalcButtonConverter.</returns>
        public override bool GetPropertiesSupported(ITypeDescriptorContext context)
        {
            return true;
        }
    }
    #endregion end of FormatConverter class.
}
