﻿using System;
using System.Collections;
using System.Text;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif


#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{
    internal interface IMaskFormat
    {
        bool AutoConvert { get; set; }
        char PromptChar { get; set; }
        ShowLiterals ShowLiterals { get; set; }
    }
    #region MaskFieldCollection
    /// <summary>
    /// Represents a collection of mask fields.
    /// </summary>
    internal class MaskFieldCollection : FieldCollection
    {
        #region Member
        private bool _autoConvert = true;
        private char _promptChar = '_';
        private string _oldValue = string.Empty;
        private ShowLiterals _showLiterals = ShowLiterals.Always;
        private IMaskFormat _owner;
        #endregion end of Member

        #region Properties from Owner
        /// <summary>
        /// Gets or sets the field data, which hold the field collection.
        /// </summary>
        public new IMaskFormat Owner
        {
            get
            {
                return _owner;
            }
            set
            {
                _owner = value;
            }
        }

        /// <summary>
        /// Gets or sets whether the control automatically converts to the proper format according to the format setting.
        /// </summary>
        public bool AutoConvert
        {
            get
            {
                if (this.Owner != null)
                {
                    return this.Owner.AutoConvert;
                }

                return this._autoConvert;
            }
            set
            {
                this._autoConvert = value;
            }
        }


        /// <summary>
        /// Gets or sets the prompt character of the field, for filling field text
        /// when the text length is less than the minimum length of this field.
        /// </summary>
        public override char PromptChar
        {
            get
            {
                if (this.Owner != null)
                {
                    return this.Owner.PromptChar;
                }

                return this._promptChar;
            }
            set
            {
                // in property grid, system set a zero to this property.
                if (value == (char)0)
                {
                    value = ' ';
                }

                this._promptChar = value;
            }
        }

        /// <summary>
        /// Gets or sets whether the literals are displayed.
        /// </summary>
        public ShowLiterals ShowLiterals
        {
            get
            {
                if (this.Owner == null)
                {
                    return this._showLiterals;
                }
                else
                {
                    return this.Owner.ShowLiterals;
                }
            }
            set
            {
                this._showLiterals = value;
            }
        }
        #endregion end of Properties from Owner

        #region constructor
        /// <summary>
        /// Initializes a new instance of the MaskFieldCollection class and 
        /// who will hold this fieldCollection.
        /// </summary>
        public MaskFieldCollection(IMaskFormat owner)
        {
            this.Owner = owner;
        }
        #endregion end of constructor

        #region Value and Text
        /// <summary>
        /// Gets whether the value is full.
        /// </summary>
        public bool ValueIsFull
        {
            get
            {
                for (int i = 0; i < this.Count; i++)
                {
                    Field field = this[i];
                    if (field is PromptField)
                    {
                    }
                    else if (field is FilterField)
                    {
                        if (!((FilterField)field).ValueIsFull)
                            return false;

                    }
                    else
                    {
                        if (!((EnumField)field).ValueIsFull)
                            return false;
                    }
                }
                return true;
            }
        }

        /// <summary>
        /// Gets the value in this instance.
        /// </summary>
        // Add comments by Yang at 10:10 June 8th 2007
        // For support JIS2004
        internal string Value
        {
            get
            {
                // Get the lastest field index.
                int endFieldIndex = this.Count - 1;
                for (; endFieldIndex >= 0; endFieldIndex--)
                {
                    Field field = this[endFieldIndex];
                    if (!(field is PromptField) && (field.TrueLength != 0))
                        break;
                }

                if (endFieldIndex < 0)
                    return string.Empty;

                string text = string.Empty;
                for (int i = 0; i <= endFieldIndex; i++)
                {
                    Field field = this[i];
                    if (field is PromptField)
                    {
                    }
                    else if (field is FilterField)
                    {
                        if (field.TrueBitsLength == field.MaxLength)
                            text += ((FilterField)field).Value;
                        else if (field.TrueBitsLength == 0 && Utility.GetStringLength(field.Text) > 0
                                && i != endFieldIndex)
                            text += new String('\x20', field.MinLength);
                        else //if (field.Status == FieldStatus.Part)
                        {
                            string fieldValue = ((FilterField)field).Value;
                            text += fieldValue;

                            #region Neal fix it on Saturday, August 16, 2003 12:21:47 PM.

                            //<Symptoms>
                            //For a Mask control, its format pattern is "\D{2,4}-\D{2,4}-\D{4}".  I Input incomplete string 
                            //in the unfixed filed and then move focus leave the control, an ArgumentOutOfRange exception is thrown.
                            //
                            //<Steps>
                            //1) Run the sample.
                            //2) Move focus leave Mask control, see an ArgumentOutOfRange exception is thrown.

                            // previous code
                            // if (i != endFieldIndex)

                            #endregion

                            if (i != endFieldIndex && field.MinLength > Utility.GetStringLength(fieldValue))
                                text += new string('\x20', field.MinLength - Utility.GetStringLength(fieldValue));
                        }

                    }
                    else if (field is EnumField)
                    {
                        if (((EnumField)field).ActiveItem != -1)
                            text += field.Text;
                        else
                            text += new string('\x20', field.MinLength);
                    }
                }

                return text;
            }
        }

        internal string TextWidthLiterals
        {
            get
            {
                return this.Text.Replace(this.PromptChar.ToString(), " ");
            }
        }

        internal string TextWidthPrompts
        {
            get
            {
                if (this.Count == 0)
                    return string.Empty;
                // Find the last field.
                int lastField = this.GetLastVisibleFieldIndex();

                if (lastField < 0)
                {
                    return string.Empty;
                }

                // Get the text string.
                string text = string.Empty;
                for (int i = 0; i < lastField; i++)
                    if (!(this[i] is PromptField))
                    {
                        text += this[i].Text; 
                    }
                    

                // @ make sure last field is longer than min length. @
                if (lastField < this.Count)
                {
                    Field field = this[lastField];
                    if (field is PromptField)
                    {
                       // text += this[lastField].Text;
                    }
                    else
                    {
                        int length = field.TrueLength > field.MinLength ? field.TrueLength : field.MinLength;
                        if (length > field.TrueBitsLength && this.ShowLiterals != ShowLiterals.Always)
                        {
                            text += Utility.Substring(this[lastField].Text, 0, field.TrueBitsLength);
                        }
                        else
                        {
                            // Make sure initial text
                            if (Utility.GetStringLength(this[lastField].Text) >= length)
                                text += Utility.Substring(this[lastField].Text, 0, length);
                            else
                            {
                                text += this[lastField].Text;
                                text += new string(this.PromptChar, length - Utility.GetStringLength(this[lastField].Text));
                            }
                        }
                    }
                }

                return text;
            }
            
        }

        internal new string Text
        {
            get
            {
                if (this.Count == 0)
                    return string.Empty;
                // Find the last field.
                int lastField = this.GetLastVisibleFieldIndex();

                if (lastField < 0)
                {
                    return string.Empty;
                }

                // Get the text string.
                string text = string.Empty;
                for (int i = 0; i < lastField; i++)
                    text += this[i].Text;

                // @ make sure last field is longer than min length. @
                if (lastField < this.Count)
                {
                    Field field = this[lastField];
                    if (field is PromptField)
                    {
                        text += this[lastField].Text;
                    }
                    else
                    {
                        int length = field.TrueLength > field.MinLength ? field.TrueLength : field.MinLength;
                        if (length > field.TrueBitsLength && this.ShowLiterals != ShowLiterals.Always)
                        {
                            text += Utility.Substring(this[lastField].Text, 0, field.TrueBitsLength);
                        }
                        else
                        {
                            // Make sure initial text
                            if (Utility.GetStringLength(this[lastField].Text) >= length)
                                text += Utility.Substring(this[lastField].Text, 0, length);
                            else
                            {
                                text += this[lastField].Text;
                                text += new string(this.PromptChar, length - Utility.GetStringLength(this[lastField].Text));
                            }
                        }
                    }
                }

                return text;
            }
        }
      
        internal string DisplayText
        {
            get
            {
                if (this.Count == 0)
                {
                    return string.Empty;
                }
                // Find the last field.
                int lastField = this.GetLastVisibleFieldIndex();

                if (lastField < 0)
                {
                    string initext = string.Empty;
                    for (int i = 0; i < this.Count; i++)
                    {
                        Field field = this[i];
                        if (field is PromptField)
                        {
                            initext += field.Text;
                        }
                        else
                        {
                            initext += new string(field.PromptChar, field.MinLength);
                        }
                    }
                    return initext;
                }

                // Get the text string.
                string text = string.Empty;
                for (int i = 0; i < lastField; i++)
                {
                    text += this[i].Text;
                }

                // @ make sure last field is longer than min length. @
                if (lastField < this.Count)
                {
                    Field field = this[lastField];
                    if (field is PromptField)
                    {
                        text += this[lastField].Text;
                    }
                    else
                    {
                        int length = field.TrueLength > field.MinLength ? field.TrueLength : field.MinLength;
                        if (length > field.TrueBitsLength && this.ShowLiterals != ShowLiterals.Always)
                        {
                            text += Utility.Substring(this[lastField].Text, 0, field.TrueBitsLength);
                        }
                        else
                        {
                            // Make sure initial text
                            if (Utility.GetStringLength(this[lastField].Text) >= length)
                                text += Utility.Substring(this[lastField].Text, 0, length);
                            else
                            {
                                text += this[lastField].Text;
                                text += new string(this.PromptChar, length - Utility.GetStringLength(this[lastField].Text));
                            }
                        }
                    }
                }

                return text;
            }
        }

        /// <summary>
        /// Sets the value to this field collection.
        /// </summary>
        /// <param name="text">Text to replace the current whole text</param>
        /// <returns>True, if the string is set successfully; false otherwise</returns>
        public bool SetValue(string text)
        {
#if DEBUG
            if (this.Count == 0)
                return false;
#endif
            if (string.IsNullOrEmpty(text))
            {
                if (this.ClearContent())
                {
                    this._oldValue = this.Value;
                    return true;
                }
                return false;
            }
            if (this.SetValue(0, text))
            {
                this._oldValue = this.Value;
                return true;
            }
            else
            {
                this.RollBack();
                throw new ArgumentException();
            }
        }


        /// <summary>
        /// Sets the text string from a specified field index.
        /// </summary>
        /// <param name="fieldIndex">Field index</param>
        /// <param name="text">Text string</param>
        /// <returns>True, if the string is set successfully; false otherwise</returns>
        private bool SetValue(int fieldIndex, string text)
        {
            // Get the field.
            for (int i = fieldIndex; i < this.Count; i++)
            {
                if (!(this[i] is PromptField))
                {
                    fieldIndex = i;
                    break;
                }
            }
            Field field = this[fieldIndex];
            int textLength = Utility.GetStringLength(text);
            string originalText = text;
            if (textLength >= field.MinLength)
            {
                for (int fieldLength = textLength; fieldLength >= field.MinLength; fieldLength--)
                {
                    // Set the text into the field.
                    bool existInvalid;
                    string subText = Utility.Substring(text, 0, fieldLength);
                    if (field is FilterField)
                        ((FilterField)field).SetValue(ref subText, out existInvalid);
                    else
                        ((EnumField)field).SetValue(ref subText, out existInvalid);

                    if (existInvalid)
                    {
                        subText = Utility.Substring(text, 0, fieldLength).Replace(' ', this.PromptChar);
                        if (field is FilterField)
                            ((FilterField)field).SetValue(ref subText, out existInvalid);
                        else
                            ((EnumField)field).SetValue(ref subText, out existInvalid);
                        if (existInvalid) // It fails, try it again.
                        {
                            continue;
                        }
                    }
                    {
                        text = subText + Utility.Substring(text, fieldLength);
                        // Check whether the text is too long.
                        if (fieldIndex >= this.Count - 1)
                        {
                            if (Utility.GetStringLength(text) == 0)
                                return true;
                            else
                            {
                                return false;
                            }
                        }
                        // Clear remain fields.
                        if (Utility.GetStringLength(text) == 0)
                        {
                            for (int i = fieldIndex + 1; i < this.Count; i++)
                                this[i].Clear();
                            return true;
                        }

                        // Find next field.
                        fieldIndex++;
                        while (fieldIndex < this.Count && this[fieldIndex] is PromptField)
                            fieldIndex++;

                        // If we can not find a field to try, check whether the text is too long.
                        if (fieldIndex >= this.Count)
                        {
                            return false;
                        }

                        // Try next field.
                        if (this.SetValue(fieldIndex, text))
                            return true;
                        else
                        {
                            text = originalText;
                            fieldIndex--;
                        }
                    }
                }
            }
            else
            {
                // Set the text into the field.
                bool existInvalid;
                string subText = Utility.Substring(text, 0, textLength);
                if (field is FilterField)
                    ((FilterField)field).SetValue(ref subText, out existInvalid);
                else
                    ((EnumField)field).SetValue(ref subText, out existInvalid);

                if (existInvalid)
                {
                    subText = Utility.Substring(text, 0, textLength).Replace(' ', this.PromptChar);
                    if (field is FilterField)
                        ((FilterField)field).SetValue(ref subText, out existInvalid);
                    else
                        ((EnumField)field).SetValue(ref subText, out existInvalid);

                    if (existInvalid) // It fails, return false.
                    {
                        return false;
                    }
                }

                for (int i = fieldIndex + 1; i < this.Count; i++)
                {
                    this[i].Clear();
                }

                return true;
            }

            return false;
        }
     
        /// <summary>
        /// Gets the text of the field.
        /// </summary>
        /// <returns>Text</returns>
        public string GetFieldText()
        {
            string fieldText = string.Empty;
            // Get the lastest field index.
            int endFieldIndex = this.Count - 1;

            if (endFieldIndex < 0)
            {
                return string.Empty;
            }

            for (int i = 0; i <= endFieldIndex; i++)
            {
                Field field = this[i];
                if (field is PromptField)
                {
                    continue;
                }

                if (field is FilterField)
                {
                    fieldText += ((FilterField)field).Value
                            .Replace(",", "@#GCC#@").Replace("'", "@#GCD#@").Replace("\"", "@#GCM#@");
                    string validateText = ((FilterField)field).Value;

                    if (validateText.Length == Utility.GetStringLength(validateText))
                    {
                        if (validateText != "")
                        {
                            FilterField filterField = field as FilterField;
                            for (int j = 0; j < filterField.BitsState.Length && j < Utility.GetStringLength(validateText); j++)
                            {
                                if (!filterField.BitsState[j])
                                {
                                    validateText = validateText.Remove(j, 1);
                                    validateText = validateText.Insert(j, Utility.MaskValChar);
                                }
                            }
                        }
                    }
                    else
                    {
                        int textLength = Utility.GetStringLength(validateText);
                        if (validateText != "")
                        {
                            FilterField filterField = field as FilterField;
                            for (int j = 0; j < filterField.BitsState.Length && j < textLength; j++)
                            {
                                if (!filterField.BitsState[j])
                                {
                                    string tempText = validateText;
                                    validateText = Utility.Substring(tempText, 0, j) + Utility.MaskValChar + Utility.Substring(tempText, j + 1);
                                }
                            }
                        }
                    }


                    fieldText += Utility.Sep2 + validateText.Replace(",", "@#GCC#@").Replace("'", "@#GCD#@").Replace("\"", "@#GCM#@");
                }
                else if (field is EnumField)
                {
                    if (((EnumField)field).ActiveItem != -1)
                    {
                        fieldText += field.Text
                            .Replace(",", "@#GCC#@").Replace("'", "@#GCD#@").Replace("\"", "@#GCM#@");
                    }
                }
                if (i < endFieldIndex)
                {
                    fieldText += Utility.Sep1;
                }
            }

            return fieldText;
        }

        /// <summary>
        /// Sets the text of the field.
        /// </summary>
        /// <param name="text">Text</param>
        /// <returns>True, if the string is set successfully; false otherwise</returns>
        public bool SetFieldText(string text)
        {
            if (text == string.Empty)
            {
                for (int fieldIndex = 0; fieldIndex < this.Count; fieldIndex++)
                {
                    Field field = this[fieldIndex];
                    if (field is PromptField)
                    {
                        continue;
                    }
                    field.Clear();
                }
                return true;
            }

            string[] sepText = text.Split(new string[] { Utility.Sep1 }, StringSplitOptions.None);
            //end by Kevin

            for (int i = 0; i < sepText.Length; i++)
                sepText[i] = sepText[i].Replace("@#GCC#@", ",").Replace("@#GCD#@", "'").Replace("@#GCM#@", "\"");

            for (int fieldIndex = 0, j = 0; fieldIndex < this.Count; fieldIndex++)
            {
                Field field = this[fieldIndex];
                if (field is PromptField)
                    continue;
                if (j < sepText.Length)
                {
                    string[] inputText = sepText[j].Split(new string[] { Utility.Sep2 }, StringSplitOptions.None);
                    text = inputText[0];
                    bool existIsValid;
                    if (field is FilterField)
                    {
                        string validateText = "";
                        if (inputText.Length > 1)
                        {
                            validateText = inputText[1];
                        }
                        FilterField filterField = (FilterField)field;
                        filterField.SetSepText(ref text, out existIsValid, validateText);
                    }
                    else
                    {
                        field.SetText(ref text, out existIsValid);
                    }

                    if (text.Length > 0)
                    {
                        return false;
                    }
                    j++;
                }
                else
                {
                    field.Clear();
                }
            }
            return true;
        }

        /// <summary>
        /// Sets the text with the option to include the prompt.
        /// </summary>
        /// <param name="text">Text</param>
        /// <param name="includePrompt">Whether to include the prompt</param>
        /// <returns>True, if the string is set successfully; false otherwise</returns>
        public new bool SetText(string text, bool includePrompt)
        {
            //Get value now to prepare Roolback.

#if DEBUG
            if (this.Count == 0)
            {
                return false;
            }
#endif
            if (string.IsNullOrEmpty(text))
            {
                if (this.ClearContent())
                {
                    this._oldValue = this.Value;
                    return true;
                }

                return false;
            }

            this._oldValue = this.Value;
            if (this.SetText(0, text, includePrompt))
            {
                return true;
            }
            else
            {
                this.RollBack();
                throw new ArgumentException();
            }
        }

        /// <summary>
        /// Sets the text string from a specified field index.
        /// </summary>
        /// <param name="fieldIndex">Field index</param>
        /// <param name="text">Text string</param>
        /// <param name="includePrompt">Value indicating whether the prompt field is considered</param>
        /// <returns>True, if the string is set successfully; false otherwise</returns>
        private bool SetText(int fieldIndex, string text, bool includePrompt)
        {
            // Get the field.
            if (!includePrompt)
            {
                for (int i = fieldIndex; i < this.Count; i++)
                {
                    if (!(this[i] is PromptField))
                    {
                        fieldIndex = i;
                        break;
                    }
                }
            }

            Field field = this[fieldIndex];
            int textLength = Utility.GetStringLength(text);

            string originalText = text;

            // the text is beyond current field scope.
            if (textLength >= field.MinLength)
            {
                // try this text from max length to min length
                for (int fieldLength = textLength; fieldLength >= field.MinLength; fieldLength--)
                {
                    #region Set the text into the field.

                    bool existInvalid;
                    string subText = Utility.Substring(text, 0, fieldLength);
                    field.SetText(ref subText, out existInvalid, true);

                    #endregion

                    // if there's something invalid, roll back...
                    if (existInvalid)	// It fails, try it again.
                    {
                    }
                    // setting text to current field is successful.
                    else
                    {
                        // get left text               
                        text = subText + Utility.Substring(text, fieldLength);

                        #region if this is the last field and there still has left text, roll back ...

                        // Check whether the text is too long.
                        if (fieldIndex >= this.Count - 1)
                        {
                            if (Utility.GetStringLength(text) == 0)
                                return true;
                            else
                            {
                                return false;
                            }
                        }

                        #endregion

                        #region if there's nothing left behind, clear all following fields.

                        // Clear remain fields.
                        if (Utility.GetStringLength(text) == 0)
                        {
                            if (includePrompt)
                            {
                                if (this.ShowLiterals == ShowLiterals.PreDisplay && this[fieldIndex + 1] is PromptField)
                                    return false;
                                if (this.ShowLiterals == ShowLiterals.PostDisplay && fieldIndex == this.Count - 2 && this[fieldIndex + 1] is PromptField)
                                    return false;
                            }

                            for (int i = fieldIndex + 1; i < this.Count; i++)
                            {
                                this[i].Clear();
                            }

                            return true;
                        }
                        #endregion

                        #region find next valid field to set left text
                        // Find next field.
                        fieldIndex++;
                        while (!includePrompt && fieldIndex < this.Count && this[fieldIndex] is PromptField)
                            fieldIndex++;

                        // If we can not find a field to try, check whether the text is too long.
                        if (fieldIndex >= this.Count)
                        {
                            return false;
                        }
                        #endregion

                        #region try next field with RECURSIVE calling.
                        // recursive call
                        // Try next field.
                        if (this.SetText(fieldIndex, text, includePrompt))
                            return true;
                        else
                        {
                            text = originalText;
                            fieldIndex--;
                        }
                        #endregion
                    }
                }
            }
            else
            {
                // Set the text into the field.
                bool existInvalid;
                string subText = Utility.Substring(text, 0, textLength);
                field.SetText(ref subText, out existInvalid, true);
                if (existInvalid)
                {
                }
                else
                {
                    // Clear remain fields.
                    if (fieldIndex < this.Count - 1)
                    {
                        if (includePrompt)
                        {
                            if (this.ShowLiterals == ShowLiterals.PreDisplay && this[fieldIndex + 1] is PromptField)
                                return false;
                            if (this.ShowLiterals == ShowLiterals.PostDisplay && fieldIndex == this.Count - 2 && this[fieldIndex + 1] is PromptField)
                                return false;
                        }

                        for (int i = fieldIndex + 1; i < this.Count; i++)
                            this[i].Clear();
                        return true;
                    }

                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Clears these fields.
        /// </summary>
        /// <returns>The changing informations.</returns>
        public bool ClearContent()
        {
#if DEBUG
            if (this.Count == 0)
                throw new InvalidOperationException();
#endif
            try
            {
                for (int i = 0; i < this.Count; i++)
                    this[i].Clear();
            }
            catch
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// restore old value.
        /// </summary>
        /// <returns>bool</returns>
        public bool RollBack()
        {
            this.SetValue(this._oldValue);

            return false;
        }
        #endregion end of Value and Text

        #region  GetLastVisibleFieldIndex
        /// <summary>
        /// Gets the last visible field index with respect to "ShowLiterals".
        /// </summary>
        /// <remarks>
        /// This method was added by Neal on Wednesday, July 09, 2003 10:28:26 AM.
        /// </remarks>
        public int GetLastVisibleFieldIndex()
        {
            int lastField = this.Count - 1;

            if (this.ShowLiterals != ShowLiterals.Always)
            {
                int end = this.Count - 1;
                bool full = true;
                for (lastField = end; lastField >= 0; lastField--)
                {
                    Field field = this[lastField];
                    // skip prompt field
                    if (field is PromptField)
                    {
                    }
                    // current field is empty.
                    else if (field.TrueLength == 0)
                    {
                        full = false;
                    }
                    // current field is not full.
                    else if (field.TrueLength < field.MinLength)
                        break;
                    else if (this.ShowLiterals == ShowLiterals.PostDisplay)
                    {
                        // if all fields were full last prompt field must be displayed.
                        if (full)
                            lastField = end;
                        break;
                    }
                    else
                    {
                        // add the next extra prompt field
                        while (lastField < end && this[lastField + 1] is PromptField)
                        {
                            lastField++;
                        }
                        break;
                    }
                }
            }
            return lastField;
        }
        #endregion end of GetLastVisibleFieldIndex

        /// <summary>
        /// Gets the filling string with the specified fill char.
        /// </summary>
        /// <param name="fillChar">The char will use to fill the string</param>
        /// <returns>The full string with literals and fill char.</returns>
        /// <remarks>get the text while nothing was inputted.</remarks>
        public override string GetFillingString(char fillChar)
        {
            if (this.Count == 0)
            {
                return string.Empty;
            }
            // Find the last field.
            int lastField = this.GetLastVisibleFieldIndex();

            if (lastField < 0)
            {
                string initext = string.Empty;

                return initext;
            }

            // Get the text string.
            string text = string.Empty;
            for (int i = 0; i < this.Count; i++)
            {
                Field field = this[i];
                if (field is PromptField)
                {
                    text += this[i].Text;
                }
                else
                {
                    text += new string(fillChar, this[i].Length);
                }
            }

            return text;
        }

        /// <summary>
        /// Gets the filling string with the specified fill string.
        /// </summary>
        /// <param name="fillChar">String will use to fill the string</param>
        /// <returns>The full string with literals and fill char.</returns>
        public string GetFillingString(string fillChar)
        {
            if (this.Count == 0)
            {
                return string.Empty;
            }
            // Find the last field.
            int lastField = this.GetLastVisibleFieldIndex();

            if (lastField < 0)
            {
                string initext = string.Empty;

                return initext;
            }

            // Get the text string.
            string text = string.Empty;
            for (int i = 0; i < this.Count; i++)
            {
                Field field = this[i];
                if (field is PromptField)
                {
                    text += this[i].Text;
                }
                else
                {
                    string strNewFillString = string.Empty;
                    for (int j = 0; j < this[i].Length; j++)
                    {
                        strNewFillString += fillChar;
                    }
                    text += strNewFillString;

                }
            }

            return text;
        }
    }
    #endregion  MaskFieldCollection


    #region The EnumField class.
    /// <summary>
    /// Represents the field, which provides items to be selected.
    /// </summary>
    internal class EnumField : Field
    {
        #region Member
        private readonly ArrayList _items;
        private int _activeItem = -1;
        #endregion end of Member

        #region Properties
        /// <summary>
        /// Gets or sets the parent of this Field.
        /// </summary>
        protected new MaskFieldCollection Owner
        {
            get
            {
                return (MaskFieldCollection)base.Owner;
            }
        }

        /// <summary>
        /// Gets the active item now.
        /// </summary>
        public int ActiveItem
        {
            get
            {
                return this._activeItem;
            }
        }

        /// <summary>
        /// Gets whether the value is full.
        /// </summary>
        public bool ValueIsFull
        {
            get
            {
                return this.ActiveItem != -1;
            }
        }

        /// <summary>
        /// Gets the minimum length.
        /// </summary>
        public override int MinLength
        {
            get
            {
                int minLength = int.MaxValue;
                for (int i = 0; i < this._items.Count; i++)
                {
                    minLength = Math.Min(minLength, Utility.GetStringLength(this._items[i].ToString()));
                }

                return minLength;
            }
        }

        /// <summary>
        /// Gets the maximum length.
        /// </summary>
        public override int MaxLength
        {
            get
            {
                int maxLength = 0;
                for (int i = 0; i < this._items.Count; i++)
                {
                    maxLength = Math.Max(maxLength, Utility.GetStringLength(this._items[i].ToString()));
                }

                return maxLength;
            }
        }

        /// <summary>
        /// Gets the text.
        /// </summary>
        public override string Text
        {
            get
            {
                if (this._activeItem == -1)
                {
                    return new string(this.PromptChar, this.MinLength);
                }
                else
                {
                    return this._items[this._activeItem].ToString();
                }
            }
        }


        /// <summary>
        /// Gets whether the control automatically converts to the proper format according to the format setting.
        /// </summary>
        public bool AutoConvert
        {
            get
            {
                if (this.Owner == null)
                {
                    return true;
                }
                else
                {
                    return this.Owner.AutoConvert;
                }
            }
        }

        /// <summary>
        /// Gets items in the field.
        /// </summary>
        public ArrayList Items
        {
            get
            {
                return this._items;
            }
        }
        #endregion end of properties

        #region Method
        /// <summary>
        /// Creates a shallow copy of the EnumField.
        /// </summary>
        public object Clone()
        {
            return new EnumField((ArrayList)this._items.Clone());
        }

        /// <summary>
        /// Initializes a new instance of the EnumField class.
        /// </summary>
        /// <param name="items">The items of the EnumField</param>
        public EnumField(ArrayList items)
        {
            if (items == null || items.Count == 0)
            {
                throw new ArgumentException(C1Localizer.GetString("C1Input.Common.ParameterIsEmpty"));
            }

            this._items = items;
        }

        /// <summary>
        /// Gets a string it is the field data link filling string.
        /// </summary>
        /// <param name="fillChar">Filling character</param>
        /// <param name="fillTail">Whether to fill the field tail</param>
        /// <returns>Field filling string</returns>
        public string GetFillingString(char fillChar, bool fillTail)
        {
            return this.Text;
        }
        #endregion end of Method

        #region InvalidInputInfo class
        /// <summary>
        /// Represents an invalid input information set.
        /// </summary>
        internal class InvalidInputInfo
        {
            #region Member
            private readonly int _destination = -1;
            private readonly int _source = -1;
            #endregion end of Member

            /// <summary>
            /// Creates a new invalid input information set.
            /// </summary>
            /// <param name="source">Source index</param>
            /// <param name="destination">Destination index</param>
            public InvalidInputInfo(int source, int destination)
            {
                if (source < 0 || destination < 0)
                    throw new ArgumentException();

                this._source = source;
                this._destination = destination;
            }

            /// <summary>
            /// Gets the destination.
            /// </summary>
            public int Destination
            {
                get
                {
                    return this._destination;
                }
            }

            /// <summary>
            /// Gets the source.
            /// </summary>
            public int Source
            {
                get
                {
                    return this._source;
                }
            }
        }
        #endregion

        /// <summary>
        /// Sets the text. Overrides BaseClass SetText Method.
        /// </summary>
        /// <param name="text">The text to set</param>
        /// <param name="existInvalid">Whether set failed</param>
        /// <param name="markIsText">Whether mark is text</param>
        public override void SetText(ref string text, out bool existInvalid, bool markIsText)
        {
            this.SetText(ref text, out existInvalid);
        }

        /// <summary>
        /// Sets the text. Overrides BaseClass SetText Method.
        /// </summary>
        /// <param name="text">Text to set</param>
        /// <param name="existInvalid">Whether set failed</param>
        public override void SetText(ref string text, out bool existInvalid)
        {
            existInvalid = false;
            //For bug from sunfei.
            if (string.IsNullOrEmpty(text))
            {
                this._activeItem = -1;
                this.TrueLength = 0;
                return;
            }

            int i;
            int[] textIndex = new int[this.Items.Count];
            int[] itemPoss = new int[this.Items.Count];
            bool[] existInvalids = new bool[this.Items.Count];
            for (i = 0; i < this.Items.Count; i++)
            {
                textIndex[i] = 0;
                itemPoss[i] = 0;
                existInvalids[i] = false;
            }

            for (i = 0; i < this.Items.Count; i++)
            {
                string item = this._items[i].ToString();
                while (textIndex[i] < Utility.GetStringLength(text))
                {
                    if (!this.JudgeIfValid(item, ref itemPoss[i], text, ref textIndex[i]))
                    {
                        existInvalids[i] = true;
                        break;
                    }

                    if (itemPoss[i] == Utility.GetStringLength(item) && textIndex[i] < Utility.GetStringLength(text))
                    {
                        existInvalids[i] = true;
                        break;
                    }

                    if (textIndex[i] == Utility.GetStringLength(text) && itemPoss[i] < Utility.GetStringLength(item))
                    {
                        existInvalids[i] = true;
                        break;
                    }
                }
            }

            for (i = 0; i < this.Items.Count; i++)
            {
                if (existInvalids[i] == false)
                    break;
            }

            if (i == this.Items.Count)
                existInvalid = true;


            if (existInvalid)
            {
                if (text == new string(this.PromptChar, this.MinLength))
                {
                    existInvalid = false;
                    text = string.Empty;
                    this.Clear();
                    return;
                }
                else
                    return;
            }
            else
            {
                this._activeItem = i;
                this.TrueLength = Utility.GetStringLength(this._items[i].ToString());
            }


            text = string.Empty;
        }

        /// <summary>
        /// Sets the value to this field.
        /// </summary>
        /// <param name="text">Text to set</param>
        /// <param name="existInvalid">Whether set failed</param>
        public virtual void SetValue(ref string text, out bool existInvalid)
        {
            this.SetText(ref text, out existInvalid);
        }

        /// <summary>
        /// Clear these field.
        /// </summary>
        public override void Clear()
        {
            this._activeItem = -1;
            this.TrueLength = 0;
        }

        /// <summary>
        /// Determines whether the index char of str1 is same with the index char of str2 according as autoConvet.
        /// </summary>
        /// <param name="str1">First string</param>
        /// <param name="index1">Start index of the first string</param>
        /// <param name="str2">Second string</param>
        /// <param name="index2">Start index of the second string</param>
        /// <returns>True, if the indexes are the same; false otherwise</returns>
        private bool JudgeIfValid(string str1, ref int index1, string str2, ref int index2)
        {
            if (index1 >= Utility.GetStringLength(str1))
                return false;
            if (Utility.Substring(str1, index1, 1) == Utility.Substring(str2, index2, 1))
            {
                index1++;
                index2++;
                return true;
            }
            else if (Char.IsSurrogate(Utility.Substring(str1, index1, 1), 0))
            {
                return false;
            }
            else
            {
                if (!this.AutoConvert)
                    return false;

                #region DBCS lower letter
                if (str2[index2] >= 0xFF41 && str2[index2] <= 0xFF5A)
                {
                    //DBCS upper letter
                    char dbcsUpper = (char)(str2[index2] - 32);
                    if (dbcsUpper == str1[index1])
                    {
                        index1++;
                        index2++;
                        return true;
                    }

                    //SBCS lower letter
                    char sbcsLower = CharEx.ToHalfWidth(str2[index2]);
                    if (sbcsLower == str1[index1])
                    {
                        index1++;
                        index2++;
                        return true;
                    }

                    //SBCS upper letter
                    char sbcsUpper = CharEx.ToHalfWidth(dbcsUpper);
                    if (sbcsUpper == str1[index1])
                    {
                        index1++;
                        index2++;
                        return true;
                    }

                    return false;
                }
                #endregion

                #region DBCS upper letter
                else if (str2[index2] >= 0xFF21 && str2[index2] <= 0xFF3A)
                {
                    //DBCS lower letter
                    char dbcsLower = (char)(str2[index2] + 32);
                    if (dbcsLower == str1[index1])
                    {
                        index1++;
                        index2++;
                        return true;
                    }

                    //SBCS upper letter
                    char sbcsUpper = CharEx.ToHalfWidth(str2[index2]);
                    if (sbcsUpper == str1[index1])
                    {
                        index1++;
                        index2++;
                        return true;
                    }

                    //SBCS lower letter
                    char sbcsLower = CharEx.ToHalfWidth(dbcsLower);
                    if (sbcsLower == str1[index1])
                    {
                        index1++;
                        index2++;
                        return true;
                    }

                    return false;
                }
                #endregion

                #region SBCS upper letter
                else if (CharEx.IsUpper(str2[index2]))
                {
                    //SBCS lower letter
                    char sbcsLower = Char.ToLower(str2[index2]);
                    if (sbcsLower == str1[index1])
                    {
                        index1++;
                        index2++;
                        return true;
                    }

                    //DBCS upper letter
                    char dbcsUpper = CharEx.ToFullWidth(str2[index2]);
                    if (dbcsUpper == str1[index1])
                    {
                        index1++;
                        index2++;
                        return true;
                    }

                    //DBCS lower letter
                    char dbcsLower = CharEx.ToFullWidth(sbcsLower);
                    if (dbcsLower == str1[index1])
                    {
                        index1++;
                        index2++;
                        return true;
                    }

                    return false;
                }
                #endregion

                #region SBCS lower letter
                else if (Char.IsLower(str2[index2]))
                {
                    //SBCS upper letter
                    char sbcsUpper = Char.ToUpper(str2[index2]);
                    if (sbcsUpper == str1[index1])
                    {
                        index1++;
                        index2++;
                        return true;
                    }

                    //DBCS lower letter
                    char dbcsLower = CharEx.ToFullWidth(str2[index2]);
                    if (dbcsLower == str1[index1])
                    {
                        index1++;
                        index2++;
                        return true;
                    }

                    //DBCS upper letter
                    char dbcsUpper = CharEx.ToFullWidth(dbcsLower);
                    if (dbcsUpper == str1[index1])
                    {
                        index1++;
                        index2++;
                        return true;
                    }

                    return false;
                }
                #endregion

                #region Hiragana
                else if (CharEx.IsHiragana(str2[index2]))
                {
                    //DBCS KATAKANA
                    char dbcsKatakana = CharEx.ToKatakana(str2[index2]);
                    if (dbcsKatakana == str1[index1])
                    {
                        index1++;
                        index2++;
                        return true;
                    }

                    //sbcsKatakana
                    char[] sbcsKatakanas = CharEx.ToKatakana(dbcsKatakana, false);
                    if (sbcsKatakanas.Length == 1 && sbcsKatakanas[0] == str1[index1])
                    {
                        index1++;
                        index2++;
                        return true;
                    }
                    else if (sbcsKatakanas.Length == 2 && sbcsKatakanas[0] == str1[index1] && index1 + 1 < str1.Length && sbcsKatakanas[1] == str1[index1 + 1])
                    {
                        index1 += 2;
                        index2++;
                        return true;
                    }

                    return false;
                }
                #endregion

                #region Full Katakana
                else if (CharEx.IsKatakana(str2[index2]) && CharEx.IsFullWidth(str2[index2]))
                {
                    //SBCS Katakana
                    char[] sbcsKatakanas = CharEx.ToKatakana(str2[index2], false);

                    if (sbcsKatakanas.Length == 1 && sbcsKatakanas[0] == str1[index1])
                    {
                        index1++;
                        index2++;
                        return true;
                    }
                    else if (sbcsKatakanas.Length == 2 && sbcsKatakanas[0] == str1[index1] && index1 + 1 < str1.Length && sbcsKatakanas[1] == str1[index1 + 1])
                    {
                        index1 += 2;
                        index2++;
                        return true;
                    }

                    //Hiragana
                    char hiragana = CharEx.ToHiragana(str2[index2]);
                    if (hiragana == '\u3094')
                    {
                        return false;
                    }
                    if (hiragana == str1[index1])
                    {
                        index1++;
                        index2++;
                        return true;
                    }

                    return false;
                }
                #endregion

                #region Half Katakana
                else if (CharEx.IsKatakana(str2[index2]) && !CharEx.IsFullWidth(str2[index2]))
                {
                    char nextChar;
                    if (index2 + 1 < str2.Length && CharEx.IsKatakana(str2[index2 + 1]) && !CharEx.IsFullWidth(str2[index2 + 1]))
                        nextChar = str2[index2 + 1];
                    else
                        nextChar = '\x0';

                    if (nextChar == '\x0')
                    {
                        //DBCS Katakana
                        char dbcsKatakana = CharEx.ToFullWidth(str2[index2]);
                        if (dbcsKatakana == str1[index1])
                        {
                            index1++;
                            index2++;
                            return true;
                        }

                        //Hiragana
                        char hiragana = CharEx.ToHiragana(dbcsKatakana);
                        if (hiragana == '\u3094')
                        {
                            return false;
                        }
                        if (hiragana == str1[index1])
                        {
                            index1++;
                            index2++;
                            return true;
                        }

                        return false;
                    }
                    else
                    {
                        //DBCS Katakana
                        bool processedAll;
                        char dbcsKatakana = CharEx.ToFullWidth(out processedAll, new Char[2] { str2[index2], nextChar });
                        if (dbcsKatakana == str1[index1])
                        {
                            index1++;
                            if (processedAll)
                            {
                                index2 += 2;
                            }
                            else
                            {
                                index2++;
                            }

                            return true;
                        }

                        //Hiragana
                        char hiragana = CharEx.ToHiragana(dbcsKatakana);
                        if (hiragana == '\u3094')
                        {
                            if (processedAll)
                            {
                                if ((new string('\u3046', 1) + new string('\u309B', 1)) == str1.Substring(index1, 2))
                                {
                                    index1 += 2;
                                    index2 += 2;
                                    return true;
                                }
                                else
                                {
                                    return false;
                                }
                            }
                            else
                            {
                                return false;
                            }
                        }
                        if (hiragana == str1[index1])
                        {
                            index1++;
                            if (processedAll)
                            {
                                index2 += 2;
                            }
                            else
                            {
                                index2++;
                            }

                            return true;
                        }

                        return false;
                    }
                }
                #endregion

                #region Full
                else if (CharEx.IsFullWidth(str2[index2]))
                {
                    //half
                    char half = CharEx.ToHalfWidth(str2[index2]);
                    if (half == str1[index1])
                    {
                        index1++;
                        index2++;

                        return true;
                    }

                    return false;
                }
                #endregion

                #region Half
                else
                {
                    //full
                    char full = CharEx.ToFullWidth(str2[index2]);
                    if (full == str1[index1])
                    {
                        index1++;
                        index2++;

                        return true;
                    }

                    return false;
                }
                #endregion
            }
        }
    }
    #endregion

    #region The FilterField class
    /// <summary>
    /// Represents the field, which checks the character through the filter.
    /// </summary>
    internal class FilterField : Field
    {
        #region The CharacterFilter class.
        /// <summary>
        /// Represents a character filter.
        /// </summary>
        internal class CharacterFilter : ICloneable
        {
            /// <summary>
            /// Indicates the owner hold the filter.
            /// </summary>
            private FilterField _owner;

            /// <summary>
            /// Gets or sets whether the character type is included.
            /// </summary>
            private bool _include = true;

            private bool _isAutoConvert = true;

            /// <summary>
            /// Gets or sets whether the character type is included.
            /// </summary>
            public bool Include
            {
                get
                {
                    return this._include;
                }
                set
                {
                    this._include = value;
                }

            }

            /// <summary>
            /// Gets or sets the owner hold the filter.
            /// </summary>
            public FilterField Owner
            {
                get
                {
                    return this._owner;
                }
                set
                {
                    if (value == null)
                        throw new ArgumentNullException();

                    this._owner = value;
                }
            }


            /// <summary>
            /// Gets whether it is converted automatically.
            /// </summary>
            protected virtual bool IsAutoConvert
            {
                get
                {
                    if (this.Owner == null)
                    {
                        return this._isAutoConvert;
                    }
                    else
                    {
                        return this.Owner.AutoConvert;
                    }
                }
                set
                {
                    this._isAutoConvert = value;
                }
            }


            /// <summary>
            /// Initializes a new instance of the Filter class.
            /// </summary>
            public CharacterFilter()
            {
            }


            /// <summary>
            /// Creates a shallow copy of the IFilter.
            /// </summary>
            public virtual object Clone()
            {
                return this._owner == null ? new CharacterFilter() : new CharacterFilter(this.Owner);
            }


            ///// <summary>
            ///// Initializes a new instance of the Filter class.
            ///// </summary>
            ///// <param name="owner">The owner hold the filter</param>
            public CharacterFilter(FilterField owner)
            {
                if (owner == null)
                    throw new ArgumentNullException();

                this.Owner = owner;
            }

            public virtual string Check(string text, ref int index)
            {
#if DEBUG
                if (string.IsNullOrEmpty(text) || index < 0 || index >= Utility.GetStringLength(text))
                    throw new ArgumentNullException();
#endif
                // Check the character type.
                bool isValid;
                string strCheckingText = Utility.Substring(text, index, 1);
                if (strCheckingText.Length == 1)
                {
                    isValid = this.IsValid(strCheckingText[0]);
                }
                else
                {
                    isValid = this.IsValid(strCheckingText);
                }


                if (isValid)
                {
                    text = strCheckingText;
                    index++;
                    return text;
                }

                if (this.IsAutoConvert && (strCheckingText.Length == 1 || strCheckingText.Length > 2))
                    return this.Convert(text, ref index);
                return string.Empty;
            }
            // End by Yang


            /// <summary>
            /// Converts the character at the specified index.
            /// </summary>
            /// <param name="text">String to be checked</param>
            /// <param name="index">Character index in the string</param>
            /// <returns>Converted string</returns>
            protected virtual string Convert(string text, ref int index)
            {
                char c = Utility.Substring(text, index, 1).ToCharArray()[0];

                bool isValid;

                // Convert between upper and lower alphabet automatically.
                if (CharEx.IsAlphabet(c))
                {
                    char r = CharEx.IsLower(c) ? char.ToUpper(c) : char.ToLower(c);
                    isValid = this.IsValid(r);
                    if ((isValid && this.Include) || (!isValid && !this.Include))
                    {
                        index++;
                        return new string(r, 1);
                    }
                    c = CharEx.IsFullWidth(c) ? CharEx.ToHalfWidth(c) : CharEx.ToFullWidth(c);
                    isValid = this.IsValid(c);
                    if ((isValid && this.Include) || (!isValid && !this.Include))
                    {
                        index++;
                        return new string(c, 1);
                    }
                    r = CharEx.IsFullWidth(r) ? CharEx.ToHalfWidth(r) : CharEx.ToFullWidth(r);
                    isValid = this.IsValid(r);
                    if ((isValid && this.Include) || (!isValid && !this.Include))
                    {
                        index++;
                        return new string(r, 1);
                    }
                    return string.Empty;
                }

                // Convert from Hiragana to DBCS/SBCS Katakana automatically.
                if (CharEx.IsHiragana(c))
                {
                    // Large < - > Small                
                    if (CharEx.IsLowerKana(c))
                    {
                        char u = CharEx.ToUpperKana(c);
                        isValid = this.IsValid(u);
                        if ((isValid && this.Include) || (!isValid && !this.Include))
                        {
                            index++;
                            return new string(u, 1);
                        }
                    }
                    else if (CharEx.HasLowerKana(c))
                    {
                        char l = CharEx.ToLowerKana(c);
                        isValid = this.IsValid(l);
                        if ((isValid && this.Include) || (!isValid && !this.Include))
                        {
                            index++;
                            return new string(l, 1);
                        }
                    }

                    c = CharEx.ToKatakana(c);
                    isValid = this.IsValid(c);
                    if ((isValid && this.Include) || (!isValid && !this.Include))
                    {
                        index++;
                        return new string(c, 1);
                    }

                    if (CharEx.IsLowerKana(c))
                    {
                        char u = CharEx.ToUpperKana(c);
                        isValid = this.IsValid(c);
                        if ((isValid && this.Include) || (!isValid && !this.Include))
                        {
                            index++;
                            return new string(u, 1);
                        }
                    }
                    else if (CharEx.HasLowerKana(c))
                    {
                        char l = CharEx.ToLowerKana(c);
                        isValid = this.IsValid(l);
                        if ((isValid && this.Include) || (!isValid && !this.Include))
                        {
                            index++;
                            return new string(l, 1);
                        }
                    }

                    char[] chars = CharEx.ToHalfWidthEx(c);
                    isValid = this.IsValid(chars[0]);
                    if ((isValid && this.Include) || (!isValid && !this.Include))
                    {
                        index++;
                        return new string(chars);
                    }

                    if (CharEx.IsLowerKana(chars[0]))
                    {
                        chars[0] = CharEx.ToUpperKana(chars[0]);
                        isValid = this.IsValid(chars[0]);
                        if ((isValid && this.Include) || (!isValid && !this.Include))
                        {
                            index++;
                            return new string(chars);
                        }
                    }
                    else if (CharEx.HasLowerKana(chars[0]))
                    {
                        chars[0] = CharEx.ToLowerKana(chars[0]);
                        isValid = this.IsValid(chars[0]);
                        if ((isValid && this.Include) || (!isValid && !this.Include))
                        {
                            index++;
                            return new string(chars);
                        }
                    }

                    return string.Empty;
                }

                // Convert from Katakana to Hiragana (or DBCS <-> SBCS)automatically.
                if (CharEx.IsKatakana(c))
                {
                    // Large < - > Small                
                    if (CharEx.IsLowerKana(c))
                    {
                        char u = CharEx.ToUpperKana(c);
                        isValid = this.IsValid(u);
                        if ((isValid && this.Include) || (!isValid && !this.Include))
                        {
                            index++;
                            return new string(u, 1);
                        }
                    }
                    else if (CharEx.HasLowerKana(c))
                    {
                        char l = CharEx.ToLowerKana(c);
                        isValid = this.IsValid(l);
                        if ((isValid && this.Include) || (!isValid && !this.Include))
                        {
                            index++;
                            return new string(l, 1);
                        }
                    }

                    char r = c;
                    bool processedAll = false;

                    // Check the soundex character.
                    if (CharEx.IsFullWidth(c))
                    {
                        char[] newChars = CharEx.ToHalfWidthEx(c);

                        if (newChars.Length > 0)
                        {
                            isValid = this.IsValid(newChars[0]);
                            if ((isValid && this.Include) || (!isValid && !this.Include))
                            {
                                index++;
                                return new string(newChars);
                            }
                        }

                        if (CharEx.IsLowerKana(newChars[0]))
                        {
                            newChars[0] = CharEx.ToUpperKana(newChars[0]);
                            isValid = this.IsValid(newChars[0]);
                            if ((isValid && this.Include) || (!isValid && !this.Include))
                            {
                                index++;
                                return new string(newChars);
                            }
                        }
                        else if (CharEx.HasLowerKana(newChars[0]))
                        {
                            newChars[0] = CharEx.ToLowerKana(newChars[0]);
                            isValid = this.IsValid(newChars[0]);
                            if ((isValid && this.Include) || (!isValid && !this.Include))
                            {
                                index++;
                                return new string(newChars);
                            }
                        }
                    }
                    else
                    {
                        if ((index + 1) < Utility.GetStringLength(text))
                            r = CharEx.ToFullWidth(out processedAll, new char[] { c, Utility.Substring(text, index + 1, 1)[0] });
                        else
                            r = CharEx.ToFullWidth(c);

                        if (!CharEx.IsKatakana(r))
                            return string.Empty;

                        isValid = this.IsValid(r);
                        if ((isValid && this.Include) || (!isValid && !this.Include))
                        {
                            index++;
                            if (processedAll)
                                index++;
                            return new string(r, 1);
                        }

                        if (CharEx.IsLowerKana(r))
                        {
                            char u = CharEx.ToUpperKana(r);
                            isValid = this.IsValid(u);
                            if ((isValid && this.Include) || (!isValid && !this.Include))
                            {
                                index++;
                                return new string(u, 1);
                            }
                        }
                        else if (CharEx.HasLowerKana(r))
                        {
                            char l = CharEx.ToLowerKana(r);
                            isValid = this.IsValid(l);
                            if ((isValid && this.Include) || (!isValid && !this.Include))
                            {
                                index++;
                                return new string(l, 1);
                            }
                        }
                    }

                    r = CharEx.ToHiragana(r);
                    isValid = this.IsValid(r);
                    if ((isValid && this.Include) || (!isValid && !this.Include))
                    {
                        index++;
                        if (processedAll)
                            index++;
                        if (r == '\u3094')
                        {
                            if (processedAll)
                            {
                                return (new string('\u3046', 1) + new string('\u309B', 1));
                            }
                            else
                            {
                                index--;
                                return string.Empty;
                            }
                        }
                        return new string(r, 1);
                    }

                    if (CharEx.IsLowerKana(r))
                    {
                        char u = CharEx.ToUpperKana(r);
                        isValid = this.IsValid(u);
                        if ((isValid && this.Include) || (!isValid && !this.Include))
                        {
                            index++;
                            return new string(u, 1);
                        }
                    }
                    else if (CharEx.HasLowerKana(r))
                    {
                        char l = CharEx.ToLowerKana(r);
                        isValid = this.IsValid(l);
                        if ((isValid && this.Include) || (!isValid && !this.Include))
                        {
                            index++;
                            return new string(l, 1);
                        }
                    }
                }

                // Convert between DBCS and SBCS automatically.
                c = CharEx.IsFullWidth(c) ? CharEx.ToHalfWidth(c) : CharEx.ToFullWidth(c);
                isValid = this.IsValid(c);
                if ((isValid && this.Include) || (!isValid && !this.Include))
                {
                    index++;
                    return new string(c, 1);
                }

                return string.Empty;
            }


            /// <summary>
            /// Determines whether the character is valid.
            /// </summary>
            /// <param name="c">The character to check</param>
            /// <returns>Whether the character is valid</returns>
            public virtual bool IsValid(char c)
            {
                if (c == '\x0D' || c == '\x0A')
                    return true;

                return false;
            }

            public virtual bool IsValid(string c)
            {
                return false;
            }

        }
        #endregion

        /// <summary>
        /// Gets or sets the minimum length.
        /// </summary>
        private readonly int _minLength;
        /// <summary>
        /// Gets or sets the maximum length.
        /// </summary>
        private readonly int _maxLength;

        private readonly bool[] _bitsState;

        private string _text = string.Empty;

        /// <summary>
        /// Indicates the character filter object.
        /// </summary>
        private readonly CharacterFilter _filter;

        /// <summary>
        /// Gets or sets the parent of this Field.
        /// </summary>
        public new MaskFieldCollection Owner
        {
            get
            {
                return (MaskFieldCollection)base.Owner;
            }
        }

        public bool[] BitsState
        {
            get
            {
                return this._bitsState;
            }
        }
        /// <summary>
        /// Gets the minimum length.
        /// </summary>
        public override int MinLength
        {
            get
            {
                return this._minLength;
            }
        }

        /// <summary>
        /// Gets the maximum length.
        /// </summary>
        public override int MaxLength
        {
            get
            {
                return this._maxLength;
            }
        }

        public override int TrueBitsLength
        {
            get
            {
                int length = this.TrueLength;
                for (int i = length - 1; i >= 0; i--)
                {
                    if (_bitsState[i] == false)
                    {
                        length--;
                    }
                    else
                    {
                        break;
                    }
                }
                return length;
            }
        }

        public bool ValueIsFull
        {
            get
            {
                if (Utility.GetStringLength(this.Value) < this.MinLength)
                    return false;

                if (this.MinLength > 0)
                {
                    int length = Utility.GetStringLength(this.Value);
                    int i;
                    for (i = length - 1; i >= 0; i--)
                    {
                        if (_bitsState[i] == false)
                            break;
                    }

                    if (i >= 0)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        /// <summary>
        /// Gets the filter.
        /// </summary>
        public CharacterFilter Filter
        {
            get
            {
                return this._filter;
            }
        }

        /// <summary>
        /// Gets whether the control automatically converts to the proper format according to the format setting.
        /// </summary>
        public bool AutoConvert
        {
            get
            {
                if (this.Owner == null)
                    return true;
                else
                    return this.Owner.AutoConvert;
            }
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        public string Value
        {
            get
            {
                if (this.Text.Length == Utility.GetStringLength(this.Text))
                {
                    StringBuilder text = new StringBuilder(this.Text);

                    for (int k = 0; k < Utility.GetStringLength(text.ToString()); k++)
                    {
                        if (!_bitsState[k])
                        {
                            text[Utility.Substring(text.ToString(), 0, k).Length] = ' ';
                        }
                    }
                    string rettext = text.ToString();

                    if (Utility.GetStringLength(text.ToString()) <= this.MinLength)
                    {
                        for (int i = Utility.GetStringLength(rettext) - 1; i >= 0; i--)
                        {
                            if (!_bitsState[i])
                            {
                                rettext = Utility.Substring(rettext, 0, i) + Utility.Substring(rettext, i + 1);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    return rettext;
                }
                else
                {

                    string text = this.Text;
                    int textLength = Utility.GetStringLength(text);

                    for (int k = 0; k < textLength; k++)
                    {
                        if (!_bitsState[k])
                        {
                            text = Utility.Substring(text, 0, k) + ' ' + Utility.Substring(text, k + 1);
                        }
                    }
                    string rettext = text;

                    if (Utility.GetStringLength(text) <= this.MinLength)
                    {
                        for (int i = Utility.GetStringLength(rettext) - 1; i >= 0; i--)
                        {
                            if (!_bitsState[i])
                            {
                                rettext = Utility.Substring(rettext, 0, i) + Utility.Substring(rettext, i + 1);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    return rettext;
                }
            }
        }

        /// <summary>
        /// Gets the text.
        /// </summary>	
        public override string Text
        {
            get
            {
                // If it is a alone field, return the empty value.
                if (this.Owner == null)
                    return new string(this.PromptChar, this.MinLength);

               
                if (this._text.Length == Utility.GetStringLength(this._text))
                {
                    // Get the text string from field collection.
                    StringBuilder text;
                    if (Utility.GetStringLength(this._text) >= this.MinLength)
                        text = new StringBuilder(this._text);
                    else
                    {
                        string temp = new string(this.PromptChar, this.MinLength);
                        text = new StringBuilder(temp);
                    }

                    for (int i = 0; i < Utility.GetStringLength(text.ToString()); i++)
                    {
                        if (!this._bitsState[i])
                            text[Utility.Substring(text.ToString(), 0, i).Length] = this.PromptChar;
                    }
                    if (Utility.GetStringLength(text.ToString()) < this.MinLength)
                    {
                        for (int i = Utility.GetStringLength(text.ToString()); i < this.MinLength; i++)
                            text[Utility.Substring(text.ToString(), 0, i).Length] = this.PromptChar;
                    }
                    return text.ToString();
                }
                else
                {

                    // Get the text string from field collection.
                    string text;
                    if (Utility.GetStringLength(this._text) >= this.MinLength)
                        text = this._text;
                    else
                    {
                        string temp = new string(this.PromptChar, this.MinLength);
                        text = temp;
                    }
                    int textLength = Utility.GetStringLength(text);
                    for (int i = 0; i < textLength; i++)
                    {
                        if (!this._bitsState[i])
                        {
                            text = Utility.Substring(text, 0, i) + this.PromptChar + Utility.Substring(text, i + 1);
                        }
                    }

                    if (textLength < this.MinLength)
                    {
                        for (int i = textLength; i < this.MinLength; i++)
                        {
                            text += this.PromptChar;
                        }
                    }
                    return text;
                }
            }
            set
            {
                this._text = value;
            }
        }

        /// <summary>
        /// Gets a value indacating whether original state different from state.
        /// </summary>
        public bool HasChange
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Initializes a new instance of the FilterField class.
        /// </summary>
        /// <param name="minlength">Min length of this Field</param>
        /// <param name="maxlength">Max length of this Field</param>
        /// <param name="filter">Filter object</param>
        public FilterField(int minlength, int maxlength, CharacterFilter filter)
        {
            if (minlength > maxlength)
            {
                throw new ArgumentException(string.Format(C1Localizer.GetString("C1Input.Common.MinMaxConflict"),
                    new[] {"minlength", "maxlength"}));
            }
            if (filter == null)
                throw new ArgumentNullException("filter");

            this._minLength = minlength;
            this._maxLength = maxlength;

            this._filter = filter;
            this._filter.Owner = this;

            if (this.MaxLength == int.MaxValue)
            {
                this._bitsState = new bool[1000];
                for (int i = 0; i < 1000; i++)
                {
                    _bitsState[i] = false;
                }
            }
            else
            {
                this._bitsState = new bool[maxlength];
                for (int i = 0; i < maxlength; i++)
                {
                    _bitsState[i] = false;
                }
            }
        }

        /// <summary>
        /// Creates a shallow copy of the FilterField.
        /// </summary>
        public object Clone()
        {
            return new FilterField(this.MinLength, this.MaxLength, (CharacterFilter)this._filter.Clone());
        }

        /// <summary>
        /// Gets a string it is the field data link filling string.
        /// </summary>
        /// <param name="fillChar">Filling character</param>
        /// <param name="fillTail">Whether to fill the field tail</param>
        /// <returns>Field filling string</returns>
        public string GetFillingString(char fillChar, bool fillTail)
        {
           return string.Empty;
        }

        /// <summary>
        /// Clear these field.
        /// </summary>
        public override void Clear()
        {
            this.TrueLength = 0;
            //Reset bitsState
            if (_bitsState != null && _bitsState.Length > 0)
            {
                for (int i = 0; i < _bitsState.Length; i++)
                {
                    _bitsState[i] = false;
                }
            }
            if (this.MinLength > 0)
            {
                if (this.PromptChar.ToString() != string.Empty)
                {
                    this.Text = new string(this.PromptChar, this.MinLength);

                }
                else
                {
                    this.Text = new string('\x20', this.MinLength);
                }

            }
            else
            {
                this.Text = string.Empty;
            }
        }

        private void SetInternalText(ref string text, out bool existInvalid, bool isSetText, string validateText)
        {
            existInvalid = false;

            // Check whether the text is valid and get the converted result.
            string newText = string.Empty;
            char prompt = this.PromptChar;
            int newLength = 0, position = 0;

            if (this.MaxLength == int.MaxValue)
            {
                for (int i = 0; i < 1000; i++)
                {
                    _bitsState[i] = false;
                }
            }
            else
            {
                for (int i = 0; i < this.MaxLength; i++)
                {
                    _bitsState[i] = false;
                }
            }

            int bitStatePosition = 0;

            for (; position < Utility.GetStringLength(text); )
            {
                string result = this.Filter.Check(text, ref position);
                //for fix the codition when ' ' is valid input
                if (result.Length > 0)
                {
                    if (validateText.Length > bitStatePosition && Utility.Substring(validateText, bitStatePosition, 1) == Utility.MaskValChar && Utility.Substring(text, bitStatePosition, 1) != Utility.MaskValChar)
                    {
                        _bitsState[bitStatePosition++] = false;
                        result = prompt.ToString();
                    }
                    else
                    {
                        _bitsState[bitStatePosition++] = true;
                    }
                }
                if (result.Length == 0)
                {
                    // for fix the codition when ' ' is valid input
                    if ((!isSetText && Utility.Substring(text, position, 1) == " ") || (isSetText && Utility.Substring(text, position, 1) == prompt.ToString()))
                    {
                        result = prompt.ToString();
                        bitStatePosition++;
                        position++;
                    }
                }
                // sometimes when hiragana to katakana , 1bits -> 2bits.
                // for example: ぱ-->??
                else if (Utility.GetStringLength(result) == 2)
                {
                    if (newLength + Utility.GetStringLength(result) > this.MaxLength)
                    {
                        existInvalid = true;
                        return;
                    }
                    else
                    {
                        _bitsState[bitStatePosition++] = true;
                    }
                }

                newLength = Utility.GetStringLength(newText) + Utility.GetStringLength(result);

                if (result.Length == 0 || (newLength == this.MaxLength && position < Utility.GetStringLength(text)))
                {
                    existInvalid = true;
                    return;
                }
                else
                    newText += result;
            }

            if (newLength > this.MaxLength)
            {
                newLength = this.MaxLength;
                text = Utility.Substring(newText, newLength) + Utility.Substring(text, position);
                newText = Utility.Substring(newText, newLength);
            }
            else
            {
                text = Utility.Substring(text, position);
            }

            //set the length of this field
            this.TrueLength = Utility.GetStringLength(newText);

            //set the prompt char of this field.
            if (Utility.GetStringLength(newText) < this.MinLength)
            {
                newText += new string(this.PromptChar, this.MinLength - Utility.GetStringLength(newText));
            }

            this.OldText = this.Text;
            this.Text = newText;
        }

        /// <summary>
        /// Sets the value text to this Field
        /// </summary>
        /// <param name="text">Text to set</param>
        /// <param name="existInvalid">Whether there are invalid characters</param>
        /// <remarks>Sometimes when hiragana to katakana , 1bits -> 2bits.
        /// For example: ぱ-->??
        /// It is an unresolved case.
        /// </remarks>
        public override void SetText(ref string text, out bool existInvalid)
        {
            this.SetInternalText(ref text, out existInvalid, false, "");
        }

        /// <summary>
        /// Sets the value text to this Field
        /// </summary>
        /// <param name="text">Text to set</param>
        /// <param name="existInvalid">Whether there are invalid characters</param>
        /// <param name="validateText">the validating string 
        ///     of which every character indicates whether the character in text is prompt char or not</param>
        /// <remarks>Sometimes when hiragana to katakana , 1bits -> 2bits.
        /// For example: ぱ-->??
        /// It is an unresolved case.
        /// </remarks>
        public void SetSepText(ref string text, out bool existInvalid, string validateText)
        {
            this.SetInternalText(ref text, out existInvalid, false, validateText);
        }

        /// <summary>
        /// Sets the text to this Field
        /// </summary>
        /// <param name="text">Text to set</param>
        /// <param name="existInvalid">Whether there are invalid characters</param>
        /// <param name="isSetText">Whether is set text or set value (True means set text)</param>
        public override void SetText(ref string text, out bool existInvalid, bool isSetText)
        {
            this.SetInternalText(ref text, out existInvalid, isSetText, "");
        }

        /// <summary>
        /// Sets the value to this field.
        /// </summary>
        /// <param name="text">Text to set</param>
        /// <param name="existInvalid">Whether set failed</param>
        public virtual void SetValue(ref string text, out bool existInvalid)
        {
            this.SetText(ref text, out existInvalid);
        }

        /// <summary>
        /// Gets the string in current field from the specified start with the specified length exclulde the prompt char if present any.
        /// </summary>
        /// <param name="start">Index of the start position</param>
        /// <param name="length">Total number of characters</param>
        /// <returns>Substring without prompt char.</returns>
        public string GetTextExcludeLiterals(int start, int length)
        {
            return string.Empty;
        }
    }
    #endregion

}
