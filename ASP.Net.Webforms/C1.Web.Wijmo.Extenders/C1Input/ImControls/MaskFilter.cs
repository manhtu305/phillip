﻿using System;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{
    #region The HalfWidthFilter class.
    /// <summary>
    /// Represents the filter for HalfWidthFilter class.
    /// </summary>
    internal class HalfWidthFilter : FilterField.CharacterFilter
    {
        /// <summary>
        /// Initializes a new instance of the SBCSField class.
        /// </summary>
        public HalfWidthFilter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the SBCSField class.
        /// </summary>
        /// <param name="owner">The owner hold the filter</param>
        public HalfWidthFilter(FilterField owner)
            : base(owner)
        {
        }

        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>Whether the character is valid</returns>
        public override bool IsValid(char c)
        {
            if (!CharEx.IsFullWidth(c))
                return true;

            return base.IsValid(c);
        }
    }
    #endregion

    #region The FullWidthFilter class.
    /// <summary>
    /// Represents the filter for FullWidthFilter class.
    /// </summary>
    internal class FullWidthFilter : FilterField.CharacterFilter
    {
        /// <summary>
        /// Initializes a new instance of the DBCSField class.
        /// </summary>
        public FullWidthFilter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the DBCSField class.
        /// </summary>
        /// <param name="owner">The owner hold the filter</param>
        public FullWidthFilter(FilterField owner)
            : base(owner)
        {
        }

        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>Whether the character is valid.</returns>
        public override bool IsValid(char c)
        {
            if (CharEx.IsFullWidth(c))
                return true;

            return base.IsValid(c);
        }
    }
    #endregion

    #region The HiraganaFilter class.
    /// <summary>
    /// Represents the filter for HiraganaFilter class.
    /// </summary>
    internal class HiraganaFilter : FilterField.CharacterFilter
    {
        /// <summary>
        /// Initializes a new instance of the HiraganaFilter class.
        /// </summary>
        public HiraganaFilter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the HiraganaFilter class.
        /// </summary>
        /// <param name="owner">The owner hold the filter</param>
        public HiraganaFilter(FilterField owner)
            : base(owner)
        {
        }

        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>Whether the character is valid.</returns>
        public override bool IsValid(char c)
        {
            if (CharEx.IsHiragana(c))
                return true;

            return base.IsValid(c);
        }
    }
    #endregion

    #region The ShiftJISFilter class.
    /// <summary>
    /// Represents the filter for ShiftJISFilter class.
    /// </summary>
    internal class ShiftJISFilter : FilterField.CharacterFilter
    {
        /// <summary>
        /// Initializes a new instance of the ShiftJISFilter class.
        /// </summary>
        public ShiftJISFilter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ShiftJISFilter class.
        /// </summary>
        /// <param name="owner">The owner hold the filter</param>
        public ShiftJISFilter(FilterField owner)
            : base(owner)
        {
        }

        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>Whether the character is valid.</returns>
        public override bool IsValid(char c)
        {
            if (CharEx.IsShiftJIS(c))
                return true;

            return base.IsValid(c);
        }
    }
    #endregion

    #region The JISX0208Filter class.
    /// <summary>
    /// Represents the filter for JISX0208Filter class.
    /// </summary>
    internal class JISX0208Filter : FilterField.CharacterFilter
    {
        /// <summary>
        /// Initializes a new instance of the JISX0208Filter class.
        /// </summary>
        public JISX0208Filter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the JISX0208Filter class.
        /// </summary>
        /// <param name="owner">The owner hold the filter</param>
        public JISX0208Filter(FilterField owner)
            : base(owner)
        {
        }

        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>Whether the character is valid.</returns>
        public override bool IsValid(char c)
        {
            if (CharEx.IsJISX0208(c))
                return true;

            return base.IsValid(c);
        }
    }
    #endregion

    #region The DBCSHiraganaFilter class.
    /// <summary>
    /// Represents the filter for DBCSKataFilter class.
    /// </summary>
    internal class DBCSHiraganaFilter : FilterField.CharacterFilter
    {
        /// <summary>
        /// Initializes a new instance of the JISX0208Filter class.
        /// </summary>
        public DBCSHiraganaFilter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the JISX0208Filter class.
        /// </summary>
        /// <param name="owner">The owner hold the filter</param>
        public DBCSHiraganaFilter(FilterField owner)
            : base(owner)
        {
        }

        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>Whether the character is valid.</returns>
        public override bool IsValid(char c)
        {
            if (CharEx.IsUpperKana(c) && CharEx.IsHiragana(c))
                return true;

            return base.IsValid(c);
        }
    }
    #endregion

    #region The DBCSKatakanaFilter class.
    /// <summary>
    /// Represents the filter for DBCSKataFilter class.
    /// </summary>
    internal class DBCSKatakanaFilter : FilterField.CharacterFilter
    {
        /// <summary>
        /// Initializes a new instance of the JISX0208Filter class.
        /// </summary>
        public DBCSKatakanaFilter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the JISX0208Filter class.
        /// </summary>
        /// <param name="owner">The owner hold the filter</param>
        public DBCSKatakanaFilter(FilterField owner)
            : base(owner)
        {
        }

        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>Whether the character is valid.</returns>
        public override bool IsValid(char c)
        {
            if (CharEx.IsFullWidth(c) && CharEx.IsKatakana(c) && !CharEx.IsLowerKana(c))
                return true;

            return base.IsValid(c);
        }
    }
    #endregion

    #region The SBCSKatakanaFilter class.
    /// <summary>
    /// Represents the filter for SBCSKataFilter class.
    /// </summary>
    internal class SBCSKatakanaFilter : FilterField.CharacterFilter
    {
        /// <summary>
        /// Initializes a new instance of the JISX0208Filter class.
        /// </summary>
        public SBCSKatakanaFilter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the JISX0208Filter class.
        /// </summary>
        /// <param name="owner">The owner hold the filter</param>
        public SBCSKatakanaFilter(FilterField owner)
            : base(owner)
        {
        }

        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>Whether the character is valid.</returns>
        public override bool IsValid(char c)
        {
            if (!CharEx.IsFullWidth(c) && CharEx.IsKatakana(c) && !CharEx.IsLowerKana(c))
                return true;

            return base.IsValid(c);
        }
    }
    #endregion

    #region The HalfWidthKatakanaFilter class.
    /// <summary>
    /// Represents the filter for HalfWidthKatakanaFilter class.
    /// </summary>
    internal class HalfWidthKatakanaFilter : FilterField.CharacterFilter
    {

        /// <summary>
        /// Initializes a new instance of the HalfWidthKatakanaFilter class.
        /// </summary>
        public HalfWidthKatakanaFilter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the HalfWidthKatakanaFilter class.
        /// </summary>
        /// <param name="owner">The owner hold the filter</param>
        public HalfWidthKatakanaFilter(FilterField owner)
            : base(owner)
        {
        }

        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>Whether the character is valid.</returns>
        public override bool IsValid(char c)
        {
            if (!CharEx.IsFullWidth(c) && CharEx.IsKatakana(c))
                return true;

            return base.IsValid(c);
        }
    }
    #endregion

    #region The FullWidthKatakanaFilter class.
    /// <summary>
    /// Represents the filter for FullWidthKatakanaFilter class.
    /// </summary>
    internal class FullWidthKatakanaFilter : FilterField.CharacterFilter
    {
        /// <summary>
        /// Initializes a new instance of the FullWidthKatakanaFilter class.
        /// </summary>
        public FullWidthKatakanaFilter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the FullWidthKatakanaFilter class.
        /// </summary>
        /// <param name="owner">The owner hold the filter</param>
        public FullWidthKatakanaFilter(FilterField owner)
            : base(owner)
        {
        }

        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>Whether the character is valid.</returns>
        public override bool IsValid(char c)
        {
            if (CharEx.IsFullWidth(c) && CharEx.IsKatakana(c))
                return true;

            return base.IsValid(c);
        }
    }
    #endregion

    #region The HalfWidthSymbolsFilter class.
    /// <summary>
    /// Represents the filter for SBCSSymbolsFilter class.
    /// </summary>
    internal class HalfWidthSymbolsFilter : FilterField.CharacterFilter
    {
        /// <summary>
        /// Initializes a new instance of the SBCSSymbolsFilter class.
        /// </summary>
        public HalfWidthSymbolsFilter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the SBCSSymbolsFilter class.
        /// </summary>
        /// <param name="owner">The owner hold the filter</param>
        public HalfWidthSymbolsFilter(FilterField owner)
            : base(owner)
        {
        }

        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>Whether the character is valid.</returns>
        public override bool IsValid(char c)
        {
            if (!CharEx.IsFullWidth(c) && CharEx.IsSymbol(c))
            {
                return true;
            }

            return base.IsValid(c);
        }
    }
    #endregion

    #region The FullWidthSymbolsFilter class.
    /// <summary>
    /// Represents the filter for DBCSSymbolsFilter class.
    /// </summary>
    internal class FullWidthSymbolsFilter : FilterField.CharacterFilter
    {
        /// <summary>
        /// Initializes a new instance of the DBCSSymbolsFilter class.
        /// </summary>
        public FullWidthSymbolsFilter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the DBCSSymbolsFilter class.
        /// </summary>
        /// <param name="owner">The owner hold the filter</param>
        public FullWidthSymbolsFilter(FilterField owner)
            : base(owner)
        {
        }


        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>Whether the character is valid.</returns>
        public override bool IsValid(char c)
        {
            if (CharEx.IsFullWidth(c) && CharEx.IsSymbol(c))
                return true;

            return base.IsValid(c);
        }
    }
    #endregion

    #region The LimitedFilter class.
    /// <summary>
    /// Represents the filter for LimitedFilter class.
    /// </summary>
    internal class LimitedFilter : FilterField.CharacterFilter
    {
        /// <summary>
        /// Indicates the character set, which is included.
        /// </summary>
        private readonly string _includeChars = string.Empty;
        /// <summary>
        /// Indicates the character set, which is excluded.
        /// </summary>
        private readonly string _excludeChars = string.Empty;

        /// <summary>
        /// Initializes a new instance of the LimitedFilter class.
        /// </summary>
        /// <param name="includeChars"></param>
        /// <param name="excludeChars"></param>
        public LimitedFilter(string includeChars, string excludeChars)
        {
            this._includeChars = includeChars;
            this._excludeChars = excludeChars;
        }

        /// <summary>
        /// Initializes a new instance of the LimitedFilter class.
        /// </summary>
        /// <param name="owner">The owner hold the filter</param>
        /// <param name="includeChars"></param>
        /// <param name="excludeChars"></param>
        public LimitedFilter(FilterField owner, string includeChars, string excludeChars)
            : base(owner)
        {
            this._includeChars = includeChars;
            this._excludeChars = excludeChars;
        }

        /// <summary>
        /// Gets the include chars of this field.
        /// </summary>
        internal string IncludeChars
        {
            get
            {
                return this._includeChars;
            }
        }

        /// <summary>
        /// Gets the exclude chars of this field.
        /// </summary>
        internal string ExcludeChars
        {
            get
            {
                return this._excludeChars;
            }
        }

        public override bool IsValid(string c)
        {
            if ((this._includeChars != "" && this._includeChars.IndexOf(c) > -1) ||
                (this._excludeChars != "" && this._excludeChars.IndexOf(c) == -1))
            {
                return true;
            }

            return base.IsValid(c);
        }

        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>Whether the character is valid.</returns>
        public override bool IsValid(char c)
        {
            if ((this._includeChars != "" && this._includeChars.IndexOf(c) > -1) ||
                (this._excludeChars != "" && this._excludeChars.IndexOf(c) == -1))
            {
                return true;
            }

            return base.IsValid(c);
        }
    }
    #endregion

    #region The Filters.RangeFilter class.
    /// <summary>
    /// Represents the filter for Filters.RangeFilter class.
    /// </summary>
    internal class RangeFilter : FilterField.CharacterFilter
    {
        /// <summary>
        /// Indicates the character set, which is included.
        /// </summary>
        private readonly char _startChar = '\x0';
        /// <summary>
        /// Indicates the character set, which is excluded.
        /// </summary>
        private readonly char _endChar = '\x0';

        /// <summary>
        /// Initializes a new instance of the LimitedFilter class.
        /// </summary>
        /// <param name="startChar"></param>
        /// <param name="endChar"></param>
        public RangeFilter(char startChar, char endChar)
        {
            this._startChar = startChar;
            this._endChar = endChar;
        }

        /// <summary>
        /// Get start char of this filter.
        /// </summary>
        internal Char StartChar
        {
            get
            {
                return this._startChar;
            }
        }

        /// <summary>
        /// Get end char of this filter.
        /// </summary>
        internal Char EndChar
        {
            get
            {
                return this._endChar;
            }
        }
        /// <summary>
        /// Initializes a new instance of the LimitedFilter class.
        /// </summary>
        /// <param name="owner">The owner hold the filter</param>
        /// <param name="startChar"></param>
        /// <param name="endChar"></param>
        public RangeFilter(FilterField owner, char startChar, char endChar)
            : base(owner)
        {
            this._startChar = startChar;
            this._endChar = endChar;
        }

        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>Whether the character is valid.</returns>
        public override bool IsValid(char c)
        {
            if (c >= this._startChar && c <= this._endChar)
            {
                return true;
            }

            return base.IsValid(c);
        }
    }
    #endregion

    #region The UnionFilter class.
    /// <summary>
    /// Represents the filter for UnionFilter class.
    /// </summary>
    internal class UnionFilter : FilterField.CharacterFilter
    {
        private const char SPECIALJPCHAR = '\u30FC';

        private readonly FilterField.CharacterFilter _hiranagaFilter;
        private readonly FilterField.CharacterFilter _katakanaFilter;

        /// <summary>
        /// Indicates a filter group.
        /// </summary>
        private readonly FilterField.CharacterFilter[] _filters;

        /// <summary>
        /// Initializes a new instance of the UnionFilter class.
        /// </summary>
        /// <param name="filters">The filters</param>
        public UnionFilter(FilterField.CharacterFilter[] filters)
        {
            this._filters = filters;

            foreach (FilterField.CharacterFilter filter in filters)
            {
                if (filter is HiraganaFilter)
                {
                    this._hiranagaFilter = filter;
                }
                else if (filter is FullWidthKatakanaFilter)
                {
                    this._katakanaFilter = filter;
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the UnionFilter class.
        /// </summary>
        /// <param name="owner">The owner hold the filter</param>
        /// <param name="filters">The filters</param>
        public UnionFilter(FilterField owner, FilterField.CharacterFilter[] filters)
            : base(owner)
        {
            this._filters = filters;

            foreach (FilterField.CharacterFilter filter in filters)
            {
                if (filter is HiraganaFilter)
                {
                    this._hiranagaFilter = filter;
                }
                else if (filter is FullWidthKatakanaFilter)
                {
                    this._katakanaFilter = filter;
                }
            }
        }

        /// <summary>
        /// Get unionfiled's filters to render.
        /// </summary>
        public FilterField.CharacterFilter[] Filters
        {
            get
            {
                return this._filters;
            }
        }

        public override bool IsValid(string c)
        {
            if (this._filters == null || this._filters.Length == 0)
            {
                return base.IsValid(c);
            }

            //include from unionfilter to its chaildren, Kevin, Oct25,2006
            bool includeResult = false;
            bool hasInclude = false;

            for (int i = 0; i < this._filters.Length; i++)
            {
                if (this._filters[i].Include)
                {
                    hasInclude = true;
                }
            }

            for (int i = 0; i < this._filters.Length; i++)
            {
                if (this._filters[i].IsValid(c))
                {
                    if (!this._filters[i].Include)
                    {
                        return false;
                    }
                    else
                    {
                        includeResult = true;
                    }
                }
                else
                {
                    if (!hasInclude)
                    {
                        includeResult = true;
                    }
                }
            }
            if (includeResult)
            {
                return true;
            }

            return base.IsValid(c);
        }

        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>Whether the character is valid.</returns>
        public override bool IsValid(char c)
        {
            if (this._filters == null || this._filters.Length == 0)
            {
                return base.IsValid(c);
            }

            //include from unionfilter to its chaildren
            bool includeResult = false;
            bool hasInclude = false;

            for (int i = 0; i < this._filters.Length; i++)
            {
                if (this._filters[i].Include)
                {
                    hasInclude = true;
                }
            }

            // The char '\u30FC' is hiranaga while it is katakana type. See TextFilter for details.
            bool isMixedJPChar = (c == SPECIALJPCHAR);
            if (isMixedJPChar)
            {
                if (this._hiranagaFilter != null && this._katakanaFilter != null)
                {
                    if (hasInclude)
                    {
                        return true;
                    }
                }
                else if (this._hiranagaFilter != null)
                {
                    return true;
                }
                else if (this._katakanaFilter != null)
                {
                    return true;
                }
            }

            for (int i = 0; i < this._filters.Length; i++)
            {
                if (this._filters[i].IsValid(c))
                {
                    if (!this._filters[i].Include)
                    {
                        return false;
                    }
                    else
                    {
                        includeResult = true;
                    }
                }
                else
                {
                    if (!hasInclude)
                    {
                        includeResult = true;
                    }
                }
            }
            if (includeResult)
            {
                return true;
            }

            return base.IsValid(c);
        }
    }
    #endregion

    #region The SurrogateFilter class.
    /// <summary>
    /// Represents the filter for SurrogateFilter class.
    /// </summary>
    internal class SurrogateFilter : FilterField.CharacterFilter
    {
        /// <summary>
        /// Initializes a new instance of the SurrogateFilter class.
        /// </summary>
        public SurrogateFilter()
        {
        }

        /// <summary>
        /// Initializes a new instance of the SurrogateFilter class.
        /// </summary>
        /// <param name="owner">The owner hold the filter</param>
        public SurrogateFilter(FilterField owner)
            : base(owner)
        {
        }

        /// <summary>
        /// Determines whether the character is valid.
        /// </summary>
        /// <param name="c">The character to be checked</param>
        /// <returns>Whether the character is valid.</returns>
        public override bool IsValid(char c)
        {
            if (Char.IsSurrogate(c))
                return true;

            return base.IsValid(c);
        }

        public override bool IsValid(string c)
        {
            for (int i = 0; i < c.Length; i++)
            {
                if (this.IsValid(c[i]))
                {
                    return true;
                }
            }
            return base.IsValid(c);
        }
    }
    #endregion

    #region EmojiFilter
    internal class EmojiFilter : FilterField.CharacterFilter
    {
        public override bool IsValid(string c)
        {
            //int length;
            //return EmojiHelper.IsEmoji(c, 0, out length);
            return false;
        }
    }
    #endregion

    #region IVSFilter
    internal class IVSFilter : FilterField.CharacterFilter
    {
        public override bool IsValid(string c)
        {
            //int length;
            //return IVSCharHelper.IsIVSElement(c, 0, out length);
            return false;
        }
    }
    #endregion
}
