﻿using System.ComponentModel;
using System;
using System.Drawing.Design;
using System.Net.Mime;
using System.Web.UI;
#if EXTENDER 
	using C1.Web.Wijmo.Extenders.Localization;
    using C1.Web.Wijmo.Extenders.Converters;
	using C1.Web.Wijmo.Extenders.C1Calendar;
	using C1.Web.Wijmo;
#else
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.C1Calendar;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{
#if EXTENDER
	public partial class C1InputDateExtender
#else
    public partial class C1InputDate
#endif
	{
		#region ** fields
		private PositionSettings _popup;
	    private DatePickers _pickers;
		#endregion

		#region ** options

		/// <summary>
		/// Determines the active field index.
		/// </summary>
        [C1Description("C1Input.ActiveField")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(0)]
		public int ActiveField
		{
			get
			{
				return GetPropertyValue("ActiveField", 0);
			}
			set
			{
				SetPropertyValue("ActiveField", value);
			}
		}

		/// <summary>
		/// Determines the time span, in milliseconds, between two input intentions.
		/// </summary>
        [C1Description("C1Input.KeyDelay")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(800)]
		public int KeyDelay
		{
			get
			{
				return GetPropertyValue("KeyDelay", 800);
			}
			set
			{
				SetPropertyValue("KeyDelay", value);
			}
		}

		/// <summary>
        /// Determines whether the focus moves automatically to the next field.
		/// </summary>
        [C1Description("C1Input.AutoNextField")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(true)]
		public bool AutoNextField
		{
			get
			{
				return GetPropertyValue("AutoNextField", true);
			}
			set
			{
				SetPropertyValue("AutoNextField", value);
			}
		}

		/// <summary>
		/// Determines the default date value for a date input.
		/// </summary>
        [C1Description("C1Input.Date")]
        [C1Category("Category.Data")]
        [Json(true)]
		[DefaultValue(null)]
        [Editor("System.ComponentModel.Design.DateTimeEditor", typeof(UITypeEditor))]
        public DateTime? Date
		{
			get
			{
				return GetPropertyValue<DateTime?>("Date", null);
			}
			set
			{
				SetPropertyValue("Date", value);
			}
		}

        /// <summary>
        /// Determines the minimal date that can be entered.
        /// </summary>
        [C1Description("C1Input.MinDate")]
        [C1Category("Category.Data")]
        [WidgetOption]
        [DefaultValue(null)]
        [Editor("System.ComponentModel.Design.DateTimeEditor", typeof(UITypeEditor))]
        public DateTime? MinDate
        {
            get
            {
                return GetPropertyValue<DateTime?>("MinDate", null);
            }
            set
            {
                DateTime? d = this.MaxDate;
                if (d != null && value != null && value >= d)
                    return;

                SetPropertyValue("MinDate", value);
            }
        }

        /// <summary>
        /// Determines the maximum date that can be entered.
        /// </summary>
        [C1Description("C1Input.MaxDate")]
        [C1Category("Category.Data")]
        [WidgetOption]
        [DefaultValue(null)]
        [Editor("System.ComponentModel.Design.DateTimeEditor", typeof(UITypeEditor))]
        public DateTime? MaxDate
        {
            get
            {
                return GetPropertyValue<DateTime?>("MaxDate", null);
            }
            set
            {
                DateTime? d = this.MinDate;
                if (d != null && value != null && value <= d)
                    return;

                SetPropertyValue("MaxDate", value);
            }
        }

		/// <summary>
        /// Determines the format pattern used to display the date value. 
		/// </summary>
        /// <remarks>
        /// C1InputDate supports two types of formats: Standard Format and Custom Format.
        ///
        ///		A standard date and time format string uses a single format specifier to 
        ///		define the text representation of a date and time value. 
        ///
        ///		Possible values for Standard Format are:
        ///		"d": ShortDatePattern
        ///		"D": LongDatePattern
        ///     "f": Full date and time (long date and short time)
        ///     "F": FullDateTimePattern
        ///	    "g": General (short date and short time)
        ///     "G": General (short date and long time)
        ///     "m": MonthDayPattern
        ///     "M": monthDayPattern
        ///     "r": RFC1123Pattern
        ///     "R": RFC1123Pattern
        ///     "s": SortableDateTimePattern
        ///     "t": shortTimePattern
        ///     "T": LongTimePattern
        ///     "u": UniversalSortableDateTimePattern
        ///     "U": Full date and time (long date and long time) using universal time
        ///     "y": YearMonthPattern
        ///     "Y": yearMonthPattern
        ///
        ///		Any date and time format string that contains more than one character, including white space, 
        ///		is interpreted as a custom date and time format string. For example: 
        ///		"mmm-dd-yyyy", "mmmm d, yyyy", "mm/dd/yyyy", "d-mmm-yyyy", "ddd, mmmm dd, yyyy" etc.
        ///
        ///		Below are the custom date and time format specifiers:
        ///
        ///		"d": The day of the month, from 1 through 31. 
        ///		"dd": The day of the month, from 01 through 31.
        ///		"ddd": The abbreviated name of the day of the week.
        ///		"dddd": The full name of the day of the week.
        ///		"m": The minute, from 0 through 59.
        ///		"mm": The minute, from 00 through 59.
        ///		"M": The month, from 1 through 12.
        ///		"MM": The month, from 01 through 12.
        ///		"MMM": The abbreviated name of the month.
        ///		"MMMM": The full name of the month.
        ///		"y": The year, from 0 to 99.
        ///		"yy": The year, from 00 to 99
        ///		"yyy": The year, with a minimum of three digits.
        ///		"yyyy": The year as a four-digit number
        ///		"h": The hour, using a 12-hour clock from 1 to 12.
        ///		"hh": The hour, using a 12-hour clock from 01 to 12.
        ///		"H": The hour, using a 24-hour clock from 0 to 23.
        ///		"HH": The hour, using a 24-hour clock from 00 to 23.
        ///		"s": The second, from 0 through 59.
        ///		"ss": The second, from 00 through 59.
        ///		"t": The first character of the AM/PM designator.
        ///		"tt": The AM/PM designator.
        /// </remarks>
        [C1Description("C1Input.DateFormat")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue("d")]
#if !EXTENDER
#if ASP_NET4
        [Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputDateFormatUITypeEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputDateFormatUITypeEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#else
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputDateFormatUITypeEditor, C1.Web.Wijmo.Controls.Design.2", typeof(UITypeEditor))]
#endif
#endif
		public string DateFormat
		{
			get
			{
				return GetPropertyValue("DateFormat", "d");
			}
			set
			{
				SetPropertyValue("DateFormat", value);
			}
		}

		/// <summary>
		/// Determines the value of the starting year to be used for the smart input year calculation.
		/// </summary>
        [C1Description("C1Input.StartYear")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(1950)]
		public int StartYear
		{
			get
			{
				return GetPropertyValue("StartYear", 1950);
			}
			set
			{
				SetPropertyValue("StartYear", value);
			}
		}

		/// <summary>
		/// Allows smart input behavior.
		/// </summary>
        /// <remarks>
        /// When SmartInputMode is true, input control will automatically insert the closest century value when user types a number in year section.
        /// </remarks>
        [C1Description("C1Input.SmartInputMode")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(false)]
		public bool SmartInputMode
		{
			get
			{
				return GetPropertyValue("SmartInputMode", false);
			}
			set
			{
				SetPropertyValue("SmartInputMode", value);
			}
		}

		/// <summary>
		/// Determines the calendar element for a date input.
		/// </summary>
        [C1Description("C1Input.Calendar")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue("default")]
		public string Calendar
		{
			get
			{
				return GetPropertyValue("Calendar", "default");
			}
			set
			{
				SetPropertyValue("Calendar", value);
			}
		}

        /// <summary>
        /// Determines the position where the calendar will appear.
        /// </summary>
        [C1Description("C1Input.PopupPosition")]
        [C1Category("Category.Appearance")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		public PositionSettings PopupPosition
		{
			get
			{
				if (_popup == null)
				{
					_popup = new PositionSettings();
					_popup.Offset.Left = 0;
					_popup.Offset.Top = 4;
				}

				return GetPropertyValue("PopupPosition", _popup);
			}
			set
			{
				SetPropertyValue("PopupPosition", value);
			}
		}

		private LocalizationOption _localization;
		/// <summary>
		/// Use the Localization property in order to customize localization strings.
		/// </summary>
        [C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.Localization",
				"Use the Localization property in order to customize localization strings.")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption]
		public LocalizationOption Localization
		{
			get 
			{
				if (_localization == null) 
				{
					_localization = new LocalizationOption(this);
				}
				return _localization;
			}			
		}

        /// <summary>
        /// Gets or sets the IME mode.
        /// </summary>
        [C1Description("C1Input.AMDesignator")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        public string AmDesignator
        {
            get
            {
                string internalValue = GetPropertyValue("AmDesignator", "");
                if (string.IsNullOrEmpty(internalValue))
                {
                    return System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.AMDesignator;
                }
                return internalValue;
            }
            set
            {
                SetPropertyValue("AmDesignator", value);
            }
        }

        /// <summary>
        /// Gets or sets the IME mode.
        /// </summary>
        [C1Description("C1Input.PMDesignator")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        public string PmDesignator
        {
            get
            {
                string internalValue = GetPropertyValue("PmDesignator", "");
                if (string.IsNullOrEmpty(internalValue))
                {
                    return System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.PMDesignator;
                }
                return internalValue;
            }
            set
            {
                SetPropertyValue("PmDesignator", value);
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="Date"/> property in a data binding friendly way.
        /// </summary>
        [C1Description("C1Input.DbValue")]
        [C1Category("Category.Data")]
        [TypeConverter(typeof(DateDbValueTypeConverter))]
        [DefaultValue(null)]
        public object DbValue
        {
            get 
            { 
                return Date; 
            }
            set 
            { 
                Date = value as DateTime?; 
            }
        }


        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeAMDesignator()
        {
            string value = GetPropertyValue("AmDesignator", "");
            string defalutValue = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.AMDesignator;

            if (!string.IsNullOrEmpty(value) && value != defalutValue )
            {
                return true;
            }

            return false;
        }

        private void ResetAMDesignator()
        {
            SetPropertyValue("AmDesignator", "");
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializePMDesignator()
        {
            string value = GetPropertyValue("PmDesignator", "");
            string defalutValue = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.PMDesignator;

            if (!string.IsNullOrEmpty(value) && value != defalutValue)
            {
                return true;
            }

            return false;
        }

        private void ResetPMDesignator()
        {
            SetPropertyValue("PmDesignator", "");
        }

        /// <summary>
        /// Determines the format pattern used to display the date value. 
        /// </summary>
        /// <remarks>
        /// C1InputDate supports two types of formats: Standard Format and Custom Format.
        ///
        ///		A standard date and time format string uses a single format specifier to 
        ///		define the text representation of a date and time value. 
        ///
        ///		Possible values for Standard Format are:
        ///		"d": ShortDatePattern
        ///		"D": LongDatePattern
        ///     "f": Full date and time (long date and short time)
        ///     "F": FullDateTimePattern
        ///	    "g": General (short date and short time)
        ///     "G": General (short date and long time)
        ///     "m": MonthDayPattern
        ///     "M": monthDayPattern
        ///     "r": RFC1123Pattern
        ///     "R": RFC1123Pattern
        ///     "s": SortableDateTimePattern
        ///     "t": shortTimePattern
        ///     "T": LongTimePattern
        ///     "u": UniversalSortableDateTimePattern
        ///     "U": Full date and time (long date and long time) using universal time
        ///     "y": YearMonthPattern
        ///     "Y": yearMonthPattern
        ///
        ///		Any date and time format string that contains more than one character, including white space, 
        ///		is interpreted as a custom date and time format string. For example: 
        ///		"mmm-dd-yyyy", "mmmm d, yyyy", "mm/dd/yyyy", "d-mmm-yyyy", "ddd, mmmm dd, yyyy" etc.
        ///
        ///		Below are the custom date and time format specifiers:
        ///
        ///		"d": The day of the month, from 1 through 31. 
        ///		"dd": The day of the month, from 01 through 31.
        ///		"ddd": The abbreviated name of the day of the week.
        ///		"dddd": The full name of the day of the week.
        ///		"m": The minute, from 0 through 59.
        ///		"mm": The minute, from 00 through 59.
        ///		"M": The month, from 1 through 12.
        ///		"MM": The month, from 01 through 12.
        ///		"MMM": The abbreviated name of the month.
        ///		"MMMM": The full name of the month.
        ///		"y": The year, from 0 to 99.
        ///		"yy": The year, from 00 to 99
        ///		"yyy": The year, with a minimum of three digits.
        ///		"yyyy": The year as a four-digit number
        ///		"h": The hour, using a 12-hour clock from 1 to 12.
        ///		"hh": The hour, using a 12-hour clock from 01 to 12.
        ///		"H": The hour, using a 24-hour clock from 0 to 23.
        ///		"HH": The hour, using a 24-hour clock from 00 to 23.
        ///		"s": The second, from 0 through 59.
        ///		"ss": The second, from 00 through 59.
        ///		"t": The first character of the AM/PM designator.
        ///		"tt": The AM/PM designator.
        /// </remarks>
        [C1Description("C1Input.DateFormat")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue("")]
#if !EXTENDER
#if ASP_NET4
        [Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputDisplayFormatUITypeEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputDisplayFormatUITypeEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#else
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputDisplayFormatUITypeEditor, C1.Web.Wijmo.Controls.Design.2", typeof(UITypeEditor))]
#endif
#endif
        public string DisplayFormat
        {
            get
            {
                return GetPropertyValue("DisplayFormat", "");
            }
            set
            {
                SetPropertyValue("DisplayFormat", value);
            }
        }

        /// <summary>
        /// Gets or sets the range of hours that can be entered in the control.
        /// </summary>
        /// <value>
        /// One of the <b>bool</b> enumeration values.  The default 
        /// is <b>false</b>
        /// </value>
        /// <remarks>
        /// <para>
        /// This proprety determines the range of hours that can be entered or 
        /// displayed when the format expression specified by the <see cref="Format"/> or the <see cref="DisplayFormat"/> properties
        /// describe 12-hour format -- the "tt" and the "HH" keywords included.
        /// </para>
        /// <para>
        /// If set this property to <b>false</b>,
        /// the control sets the range for the hour field from 1 - 12.  If set to <b>true</b>
        /// the range is set to 0 - 11.
        /// </para>
        /// </remarks>
        [C1Description("C1Input.Hour12As0")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(false)]
        public bool Hour12As0
        {
            get
            {
                return GetPropertyValue("Hour12As0", false);
            }
            set
            {
                SetPropertyValue("Hour12As0", value);
            }
        }

        /// <summary>
        /// Gets or sets whether to express midnight as 24:00.
        /// </summary>
        /// <value>
        /// <b>false</b> if expressed as 24 o'clock (24:00:00); otherwise, <b>true</b> expressed 
        /// as 0 o'clock (00:00:00).  The default is <b>true</b>.
        /// </value>
        /// <remarks>
        /// <para>
        /// The <b>MidnightAs24</b> property effects the <see cref="MediaTypeNames.Text"/> 
        /// property when the <see cref="DisplayFormat"/> 
        /// property is set to the 24-hour format ("HH:mm:ss"). 
        /// </para>
        /// </remarks>
        [C1Description("C1Input.MidnightAs0")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(true)]
        public bool MidnightAs0
        {
            get
            {
                return GetPropertyValue("MidnightAs0", true);
            }
            set
            {
                SetPropertyValue("MidnightAs0", value);
            }
        }

        /// <summary>
        ///  Gets or sets the tab action.
        /// </summary>
        [C1Description("C1Input.TabAction")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(TabAction.Field)]
        public TabAction TabAction
        {
            get
            {
                return GetPropertyValue("TabAction", TabAction.Field);
            }
            set
            {
                SetPropertyValue("TabAction", value);
            }
        }

        /// <summary>
        /// Gets or sets how to select the text when the control receives the focus.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Input.HighlightText")]
        [WidgetOption]
        [DefaultValue(HighlightText.Field)]
        [TypeConverter(typeof(DateHighlightTextConvert))]
        public HighlightText HighlightText
        {
            get
            {
                return GetPropertyValue("HighlightText", HighlightText.Field);
            }
            set
            {
                if (value == HighlightText.None)
                {
                    throw new NotSupportedException("None is not supported for the C1InputDate");
                }
                SetPropertyValue("HighlightText", value);
            }
        }

        /// <summary>
        ///   Determines how much to increase/decrease the active field when performing spin on the the active field. 
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Input.Increment")]
        [WidgetOption]
        [DefaultValue(1)]
	    public int Increment
	    {
            get
            {
                return GetPropertyValue("Increment", 1);
            }
            set
            {
                SetPropertyValue("Increment", value);
            }
	    }

	    internal override bool DefaultShowDropDownButton
	    {
	        get { return true; }
	    }

	    /// <summary>
        ///  Get an object that contains the settings for the dropdown window.
        /// </summary>
        [C1Description("C1Input.Pickers")]
        [C1Category("Category.Data")]
        [NotifyParentProperty(true)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        [ReadOnly(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public DatePickers Pickers
	    {
	        get
	        {
	            if (this._pickers == null)
	            {
	                this._pickers = new DatePickers(this.ViewState, this);
	            }
	            return this._pickers;
	        }
	    }

        /// <summary>
        /// Gets or sets whether to force the month not be changed on typing the year and month， if the day exceeds the max days of current month.
        /// </summary>
        /// <remarks>
        /// When typing the year and month field, the day may exceeds the max days in current month. 
        /// The default behavior is as following: <br/>
        /// 1) If the original day is the last day of the original month, set the day to the last day of the new month. <br/>
        /// 2) Otherwise, keep the origina day. If the day exceeds last day of the current month, increase the month.
        /// (it's done by the standard JavaScript.) <br />
        /// Set this property to true will change to the following behavior: <br/>
        /// 1) Keep the day if it's valid for the new month.<br/>
        /// 2) Set the day to the latest day of the new month, if the original day exceeds the last day.
        /// <para>
        /// For example, for current date 1997-01-29, set the month to 2. <br/>
        /// 1) ForceMonth=false, the new date will be 1997-03-01; <br/>
        /// 2) ForceMonth=true, the new date will be 1997-02-28.
        /// </para>
        /// </remarks>
        [C1Category("Category.Behavior")]
        [C1Description("C1Input.ForceMonth")]
        [WidgetOption]
        [DefaultValue(false)]
        public bool ForceMonth
        {
            get
            {
                return GetPropertyValue("ForceMonth", false);
            }
            set
            {
                SetPropertyValue("ForceMonth", value);
            }
        }


	    #endregion

        #region client events

        /// <summary>
        /// Occurs after the date value changed. 
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Input.OnClientDateChanged")]
        [DefaultValue("")]
        [WidgetOption]
        [WidgetOptionName("dateChanged")]
        [WidgetEvent]
        public string OnClientDateChanged
        {
            get
            {
                return GetPropertyValue("OnClientDateChanged", "");
            }
            set
            {
                SetPropertyValue("OnClientDateChanged", value);
            }
        }

        /// <summary>
        /// Occurs after the date value changed. 
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Input.OnClientValueBoundsExceeded")]
        [DefaultValue("")]
        [WidgetOption]
        [WidgetOptionName("valueBoundsExceeded")]
        [WidgetEvent]
        public string OnClientValueBoundsExceeded
        {
            get
            {
                return GetPropertyValue("OnClientValueBoundsExceeded", "");
            }
            set
            {
                SetPropertyValue("OnClientValueBoundsExceeded", value);
            }
        }


        #endregion
    }

	/// <summary>
	/// Localization strings.
	/// </summary>
	public class LocalizationOption : Settings, IJsonEmptiable
	{
#if EXTENDER
        private C1InputDateExtender _c1Inputdate;
#else
		private readonly C1InputDate _c1Inputdate;
#endif	
        /// <summary>
        ///   Initialize a new instance of LocalizationOption class.
        /// </summary>
#if EXTENDER
		public LocalizationOption(C1InputDateExtender inputdate)
#else
		public LocalizationOption(C1InputDate inputdate)
#endif
		{
			_c1Inputdate = inputdate;
		}

		/// <summary>
		/// Gets or sets the format for the title text. 
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Calendar.TitleFormat")]
		[DefaultValue("MMMM yyyy")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string TitleFormat
		{
			get
			{
				//return GetPropertyValue("TitleFormat", "MMMM yyyy");
				return GetPropertyValue("TitleFormat",
					C1Localizer.GetString("C1Calendar.TitleFormat.DefaultValue", "MMMM yyyy", _c1Inputdate.Culture));
			}
			set
			{
				SetPropertyValue("TitleFormat", value);
			}
		}

		/// <summary>
		/// Gets or sets the text for the 'next' button's ToolTip. 
		/// </summary>
		[C1Category("Category.Navigation")]
		[C1Description("C1Calendar.NextToolTip")]
		[DefaultValue("Next")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string NextTooltip
		{
			get
			{
				//return GetPropertyValue("NextTooltip", "Next");
				return GetPropertyValue("NextTooltip",
					C1Localizer.GetString("C1Calendar.NextTooltip.DefaultValue", "Next", _c1Inputdate.Culture));

			}
			set
			{
				SetPropertyValue("NextTooltip", value);
			}
		}
		/// <summary>
		/// Gets or sets the format for the ToolTip.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Calendar.ToolTipFormat")]
		[NotifyParentProperty(true)]
		[DefaultValue("dddd, MMMM dd, yyyy")]
		[WidgetOption]
		public string ToolTipFormat
		{
			get
			{
				//return GetPropertyValue("ToolTipFormat", "dddd, MMMM dd, yyyy");
				return GetPropertyValue("ToolTipFormat",
					C1Localizer.GetString("C1Calendar.ToolTipFormat.DefaultValue", "dddd, MMMM dd, yyyy", _c1Inputdate.Culture));
			}
			set
			{
				SetPropertyValue("ToolTipFormat", value);
			}
		}

		/// <summary>
		/// Gets or sets the text for the 'previous' button's ToolTip. 
		/// </summary>
		[C1Category("Category.Navigation")]
		[C1Description("C1Calendar.PrevToolTip")]
		[DefaultValue("Previous")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string PrevTooltip
		{
			get
			{
				//return GetPropertyValue("PrevTooltip", "Previous");
				return GetPropertyValue("PrevTooltip",
					C1Localizer.GetString("C1Calendar.PrevTooltip.DefaultValue", "Previous", _c1Inputdate.Culture));
			}
			set
			{
				SetPropertyValue("PrevTooltip", value);
			}
		}

		bool IJsonEmptiable.IsEmpty
		{
			get { return false; }
		}
	}

    /// <summary>
    ///   Specifies the setings of the dropdown list and dropdown calendar.
    /// </summary>
    public sealed class DatePickers : Pickers
    {
        private DropDownCalendar _calendar;
        private DatePicker _datePicker;
        private TimePicker _timePicker;
#if EXTENDER
        private C1InputDateExtender _owner;
#else
		private readonly C1InputDate _owner;
#endif

#if EXTENDER
        internal DatePickers(StateBag viewState, C1InputDateExtender owner)
            : base(viewState)
#else
              internal DatePickers(StateBag viewState, C1InputDate owner) : base(viewState)
#endif
        {
            this._owner = owner;
        }

        /// <summary>
        ///   Determine the setings of the dropdown calendar.
        /// </summary>
        [C1Description("C1Input.DatePickers.Calendar")]
        [C1Category("Category.Data")]
        [NotifyParentProperty(true)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        [ReadOnly(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public DropDownCalendar Calendar
        {
            get
            {
                if (this._calendar == null)
                {
                    this._calendar = new DropDownCalendar(this._viewState, this._owner);
                }
                return this._calendar;
            }
        }

        /// <summary>
        ///   Determine the setings of the date picker.
        /// </summary>
        [C1Description("C1Input.DatePickers.DatePicker")]
        [C1Category("Category.Data")]
        [NotifyParentProperty(true)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        [ReadOnly(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public DatePicker DatePicker
        {
            get
            {
                if (this._datePicker == null)
                {
                    this._datePicker = new DatePicker(this._viewState);
                }
                return this._datePicker;
            }
        }

        /// <summary>
        ///   Determine the setings of the time picker.
        /// </summary>
        [C1Description("C1Input.DatePickers.TimePicker")]
        [C1Category("Category.Data")]
        [NotifyParentProperty(true)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        [ReadOnly(true)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public TimePicker TimePicker
        {
            get
            {
                if (this._timePicker == null)
                {
                    this._timePicker = new TimePicker(this._viewState);
                }
                return this._timePicker;
            }
        }
    }

    /// <summary>
    ///   Sepcifes the setings of the dropdown calendar.
    /// </summary>
    public class DropDownCalendar
    {
        private readonly StateBag _viewState;
#if EXTENDER
        private C1InputDateExtender _owner;
#else
		private readonly C1InputDate _owner;
#endif

#if EXTENDER
        internal DropDownCalendar(StateBag viewState, C1InputDateExtender owner)
#else
        internal DropDownCalendar(StateBag viewState, C1InputDate owner)
#endif
        {
            this._viewState = viewState;
            this._owner = owner;
        }

        private V GetPropertyValue<V>(string propertyName, V nullValue)
        {
            if (this._viewState[propertyName] == null)
            {
                return nullValue;
            }
            return (V)this._viewState[propertyName];
        }

        private void SetPropertyValue<V>(string propertyName, V value)
        {
            this._viewState[propertyName] = value;
        }

        /// <summary>
        /// Determines whether users can change the view to month/year/decade while clicking on the calendar title.
        /// </summary>
        [C1Category("Category.Navigation")]
        [C1Description("C1Calendar.AllowQuickPick")]
        [DefaultValue(true)]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public bool AllowQuickPick
        {
            get
            {
                return this.GetPropertyValue("C1Input.DropDownCalendar.AllowQuickPick", true);
            }
            set
            {
                this.SetPropertyValue("C1Input.DropDownCalendar.AllowQuickPick", value);
            }
        }

        /// <summary>
        /// Gets or sets the text for the 'previous' button's ToolTip. 
        /// </summary>
        [C1Category("Category.Navigation")]
        [C1Description("C1Calendar.PrevToolTip")]
        [DefaultValue("Previous")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public string PrevTooltip
        {
            get
            {
                //return GetPropertyValue("PrevTooltip", "Previous");
                return GetPropertyValue("C1Input.DropDownCalendar.PrevTooltip",
                    C1Localizer.GetString("C1Calendar.PrevTooltip.DefaultValue", "Previous", this._owner.Culture));
            }
            set
            {
                SetPropertyValue("C1Input.DropDownCalendar.PrevTooltip", value);
            }
        }

        /// <summary>
        /// Gets or sets the text for the 'next' button's ToolTip. 
        /// </summary>
        [C1Category("Category.Navigation")]
        [C1Description("C1Calendar.NextToolTip")]
        [DefaultValue("Next")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public string NextTooltip
        {
            get
            {
                //return GetPropertyValue("NextTooltip", "Next");
                return GetPropertyValue("C1Input.DropDownCalendar.NextTooltip",
                    C1Localizer.GetString("C1Calendar.NextTooltip.DefaultValue", "Next", this._owner.Culture));

            }
            set
            {
                SetPropertyValue("C1Input.DropDownCalendar.NextTooltip", value);
            }
        }

        /// <summary>
        /// Determines the display type of navigation buttons.
        /// </summary>
        [WidgetOption]
        [DefaultValue(NavButtons.Default)]
        [C1Category("Category.Navigation")]
        [C1Description("C1Calendar.NavButtons")]
        [NotifyParentProperty(true)]
        public NavButtons NavButtons
        {
            get
            {
                return GetPropertyValue("C1Input.DropDownCalendar.NavButtons", NavButtons.Default);
            }
            set
            {
                SetPropertyValue("C1Input.DropDownCalendar.NavButtons", value);
            }
        }

        /// <summary>
        /// Determines whether to add zeroes to days with only one digit.
        /// </summary>
        [WidgetOption]
        [DefaultValue(false)]
        [C1Category("Category.Appearance")]
        [C1Description("C1Calendar.ShowDayPadding")]
        [NotifyParentProperty(true)]
        public bool ShowDayPadding
        {
            get
            {
                return GetPropertyValue("C1Input.DropDownCalendar.ShowDayPadding", false);
            }
            set
            {
                SetPropertyValue("C1Input.DropDownCalendar.ShowDayPadding", value);
            }
        }

        /// <summary>
        /// Determines whether to display the days of the next and/or previous month.
        /// </summary>
        [C1Category("Category.Appearance")]
        [C1Description("C1Calendar.ShowOtherMonthDays")]
        [DefaultValue(true)]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public bool ShowOtherMonthDays
        {
            get
            {
                return GetPropertyValue("C1Input.DropDownCalendar.ShowOtherMonthDays", true);
            }
            set
            {
                SetPropertyValue("C1Input.DropDownCalendar.ShowOtherMonthDays", value);
            }
        }

        /// <summary>
        /// A Boolean property that determines whether to display calendar title.
        /// </summary>
        [WidgetOption]
        [DefaultValue(true)]
        [C1Category("Category.Appearance")]
        [C1Description("C1Calendar.ShowTitle")]
        [NotifyParentProperty(true)]
        public bool ShowTitle
        {
            get
            {
                return GetPropertyValue("C1Input.DropDownCalendar.ShowTitle", true);
            }
            set
            {
                SetPropertyValue("C1Input.DropDownCalendar.ShowTitle", value);
            }
        }

        /// <summary>
        /// A Boolean property that determines whether to display week days.
        /// </summary>
        [WidgetOption]
        [DefaultValue(true)]
        [C1Category("Category.Behavior")]
        [C1Description("C1Calendar.ShowWeekDays")]
        [NotifyParentProperty(true)]
        public bool ShowWeekDays
        {
            get
            {
                return GetPropertyValue("C1Input.DropDownCalendar.ShowWeekDays", true);
            }
            set
            {
                SetPropertyValue("C1Input.DropDownCalendar.ShowWeekDays", value);
            }
        }

        /// <summary>
        /// Determines whether to display week numbers. 
        /// </summary>
        [C1Category("Category.MonthView")]
        [C1Description("C1Calendar.ShowWeekNumbers")]
        [DefaultValue(false)]
        [WidgetOption]
        [NotifyParentProperty(true)]
        public bool ShowWeekNumbers
        {
            get
            {
                return GetPropertyValue("C1Input.DropDownCalendar.ShowWeekNumbers", false);
            }
            set
            {
                SetPropertyValue("C1Input.DropDownCalendar.ShowWeekNumbers", value);
            }
        }

        /// <summary>
        /// Gets or sets the format for the title text. 
        /// </summary>
        [C1Category("Category.Appearance")]
        [C1Description("C1Calendar.TitleFormat")]
        [DefaultValue("MMMM yyyy")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public string TitleFormat
        {
            get
            {
                return GetPropertyValue("C1Input.DropDownCalendar.TitleFormat",
                    C1Localizer.GetString("C1Calendar.TitleFormat.DefaultValue", "MMMM yyyy"));
            }
            set
            {
                SetPropertyValue("C1Input.DropDownCalendar.TitleFormat", value);
            }
        }

        /// <summary>
        /// Gets or sets the format for the ToolTip.
        /// </summary>
        [C1Category("Category.Appearance")]
        [C1Description("C1Calendar.ToolTipFormat")]
        [NotifyParentProperty(true)]
        [DefaultValue("dddd, MMMM dd, yyyy")]
        [WidgetOption]
        public string ToolTipFormat
        {
            get
            {
                return GetPropertyValue("C1Input.DropDownCalendar.ToolTipFormat",
                    C1Localizer.GetString("C1Calendar.ToolTipFormat.DefaultValue", "dddd, MMMM dd, yyyy"));
            }
            set
            {
                SetPropertyValue("C1Input.DropDownCalendar.ToolTipFormat", value);
            }
        }

        ///	<summary>
        ///	Gets or sets the format for the week day. 
        ///	</summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(WeekDayFormat.Short)]
        [C1Description("C1Calendar.WeekDayFormat")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public WeekDayFormat WeekDayFormat
        {
            get
            {
                return GetPropertyValue("C1Input.DropDownCalendar.WeekDayFormat", WeekDayFormat.Short);
            }
            set
            {
                SetPropertyValue("C1Input.DropDownCalendar.WeekDayFormat", value);
            }
        }

        ///	<summary>
        ///	Gets or sets the format for the week day. 
        ///	</summary>
        [C1Category("Category.Appearance")]
        [DefaultValue(false)]
        [C1Description("C1Input.DropDownCalendar.Visible")]
        [NotifyParentProperty(true)]
        [Json(true)]
        public bool Visible
        {
            get
            {
                return GetPropertyValue("C1Input.DropDownCalendar.Visible", false);
            }
            set
            {
                SetPropertyValue("C1Input.DropDownCalendar.Visible", value);
            }
        }
    }

    /// <summary>
    ///   Sepcifes the setings of the dropdown date picker.
    /// </summary>
    public class DatePicker
    {
        private readonly StateBag _viewState;

        internal DatePicker(StateBag viewState)
        {
            this._viewState = viewState;
        }

        private V GetPropertyValue<V>(string propertyName, V nullValue)
        {
            if (this._viewState[propertyName] == null)
            {
                return nullValue;
            }
            return (V)this._viewState[propertyName];
        }

        private void SetPropertyValue<V>(string propertyName, V value)
        {
            this._viewState[propertyName] = value;
        }

        ///	<summary>
        ///	Gets or sets the format for the week day. 
        ///	</summary>
        [C1Category("Category.Appearance")]
        [DefaultValue(false)]
        [C1Description("C1Input.DatePicker.Visible")]
        [NotifyParentProperty(true)]
        [Json(true)]
        public bool Visible
        {
            get
            {
                return GetPropertyValue("C1Input.DatePicker.Visible", false);
            }
            set
            {
                SetPropertyValue("C1Input.DatePicker.Visible", value);
            }
        }

        ///	<summary>
        ///	Gets or sets the format that shows in the date picker.
        ///	</summary>
        [C1Category("Category.Appearance")]
        [DefaultValue("")]
        [C1Description("C1Input.DatePicker.Format")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public string Format
        {
            get
            {
                return GetPropertyValue("C1Input.DatePicker.Format", "");
            }
            set
            {
                this.SetPropertyValue("C1Input.DatePicker.Format", value);
            }
        }
    }

    /// <summary>
    ///   Sepcifes the setings of the dropdown time picker.
    /// </summary>
    public class TimePicker
    {
        private readonly StateBag _viewState;

        internal TimePicker(StateBag viewState)
        {
            this._viewState = viewState;
        }

        private V GetPropertyValue<V>(string propertyName, V nullValue)
        {
            if (this._viewState[propertyName] == null)
            {
                return nullValue;
            }
            return (V)this._viewState[propertyName];
        }

        private void SetPropertyValue<V>(string propertyName, V value)
        {
            this._viewState[propertyName] = value;
        }

        ///	<summary>
        ///	Determines whether shows the time picker.
        ///	</summary>
        [C1Category("Category.Appearance")]
        [DefaultValue(false)]
        [C1Description("C1Input.TimePicker.Visible")]
        [NotifyParentProperty(true)]
        [Json(true)]
        public bool Visible
        {
            get
            {
                return GetPropertyValue("C1Input.TimePicker.Visible", false);
            }
            set
            {
                SetPropertyValue("C1Input.TimePicker.Visible", value);
            }
        }

        ///	<summary>
        ///	Gets or sets the format that shows in the time picker.
        ///	</summary>
        [C1Category("Category.Appearance")]
        [DefaultValue("")]
        [C1Description("C1Input.TimePicker.Format")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public string Format
        {
            get
            {
                return GetPropertyValue("C1Input.TimePicker.Format", "");
            }
            set
            {
                this.SetPropertyValue("C1Input.TimePicker.Format", value);
            }
        }
    }

    internal class DateHighlightTextConvert : EnumConverter 
    {

        public DateHighlightTextConvert():base(typeof(HighlightText))
        {

        }

        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            return new StandardValuesCollection(new HighlightText[]{ HighlightText.All, HighlightText.Field, });
        }
    }
}
