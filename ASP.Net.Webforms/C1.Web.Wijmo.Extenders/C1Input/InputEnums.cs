﻿
#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{
    /// <summary>
    /// Enumeration that determines the alignment of trigger and spinner buttons of input.
    /// </summary>
    public enum ButtonAlign
    {
        /// <summary>
        /// Displays buttons on the left side of input content.
        /// </summary>
        Left = 0,
        /// <summary>
        /// Displays buttons on the right side of the input content.
        /// </summary>
        Right = 1
    }

    /// <summary>
    /// Enumeration that determines the alignment of spinner buttons of input.
    /// </summary>
    public enum SpinnerAlign
    {
        /// <summary>
        /// Dispaly the spin button as one button, and the up button is on the top, and the down button is on the bottom, the position is in control's left.
        /// </summary>
        VerticalLeft,
        /// <summary>
        /// Dispaly the spin button as one button, and the up button is on the top, and the down button is on the bottom, the position is in control's right.
        /// </summary>
        VerticalRight,
        /// <summary>
        /// Display the spin button as two buttons, and  the down button is on the left, the up button is on the right.
        /// </summary>
        HorizontalDownLeft,
        /// <summary>
        /// Display the spin button as two buttons, and  the up button is on the left, the down button is on the right.
        /// </summary>
        HorizontalUpLeft
    }

    /// <summary>
    /// Enumeration that determines the type of the number input.
    /// </summary>
    public enum NumberType
    {
        /// <summary>
        /// A numeric type number input.
        /// </summary>
        Numeric = 0,

        /// <summary>
        /// A percent type number input.
        /// </summary>
        Percent = 1,

        /// <summary>
        /// A currency type number input.
        /// </summary>
        Currency = 2
    }

    /// <summary>
    ///   Enumeration that determines the format mode of the mask format.
    /// </summary>
    public enum FormatMode
    {
        /// <summary>
        ///   The simple format.
        /// </summary>
        Simple,
        /// <summary>
        ///   The advacned format.
        /// </summary>
        Advanced
    }

    /// <summary>
    /// Specifies the mode of the ellipsis (...) to display.
    /// </summary>
    public enum EllipsisMode
    {
        /// <summary>
        /// Does not display the ellipsis
        /// </summary>
        None = 0,
        /// <summary>
        /// Shows the ellipsis on the far side of the control
        /// </summary>
        EllipsisEnd = 1,
        /// <summary>
        /// Exchanges the middle part of the string with an ellipsis
        /// </summary>
        EllipsisPath = 2
    }

    /// <summary>
    /// Specifies the type of selection text in control
    /// </summary>
    /// <remarks>
    /// After getting focus, none, specified fields or all text in editor are selected.
    /// </remarks>
    public enum HighlightText
    {
        /// <summary>No selection specified</summary>
        None = 0,
        /// <summary>Select the specified field</summary>
        Field = 1,
        /// <summary>Select all the text</summary>
        All = 2,
    }

    //public enum DecimalPlaces
    //{
    //    UseDefault = -2,
    //    AsIs = -1,
    //    Zero = 0,
    //    One = 1,
    //    Two = 2,
    //    Three = 3,
    //    Four = 4,
    //    Five = 5,
    //    Six = 6,
    //    Seven = 7,
    //    Eight = 8
    //}
}
