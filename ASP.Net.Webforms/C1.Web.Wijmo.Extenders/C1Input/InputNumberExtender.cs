﻿using System;
using System.Drawing;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using C1.Web.Wijmo;


namespace C1.Web.Wijmo.Extenders.C1Input
{

	/// <summary>
	/// InputNumber Extender.
	/// </summary>
	[LicenseProvider()]
	[TargetControlType(typeof(TextBox))]
	[ToolboxBitmap(typeof(C1InputNumberExtender), "InputNumber.png")]
    [ToolboxItem(true)]
    public partial class C1InputNumberExtender : C1InputExtenderBase
	{
		 private bool _productLicensed = false;

		/// <summary>
		/// Initializes a new instance of the <see cref="C1InputNumberExtender"/> class.
		/// </summary>
		public C1InputNumberExtender()
			: base()
		{
			VerifyLicense();
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1InputNumberExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		/// <summary>
		/// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered in the browser window.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.RenderLicenseWebComment(writer);
			base.Render(writer);
		}
	}
}

