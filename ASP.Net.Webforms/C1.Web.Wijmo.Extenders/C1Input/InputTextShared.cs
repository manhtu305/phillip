﻿using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Globalization;
using System.Web.UI;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;

#else
	using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{
    [WidgetDependencies(
        typeof(WijToolTip)
        )]
#if EXTENDER
    public partial class C1InputTextExtender
#else
    public partial class C1InputText
#endif
    {
        /// <summary>
        /// Gets or sets whether the control automatically converts to the proper format according to the format setting.
        /// </summary>
        [C1Description("C1Input.AutoConvert")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(true)]
        public bool AutoConvert
        {
            get
            {
                return GetPropertyValue("AutoConvert", true);
            }
            set
            {
                SetPropertyValue("AutoConvert", value);
            }
        }

        /// <summary>
        /// Gets or sets whether to treat the wrapped lines as one line or multiple lines 
        /// when counting the lines count.
        /// </summary>
        [C1Description("C1Input.CountWrappedLine")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(false)]
        public bool CountWrappedLine
        {
            get
            {
                return GetPropertyValue("CountWrappedLine", false);
            }
            set
            {
                SetPropertyValue("CountWrappedLine", value);
            }
        }

        /// <summary>
        /// Gets or sets the format string that defines the type of text 
        /// allowed for input in the control.
        /// </summary>
        /// <remarks>
        /// The following key words are supported.
        /// DBCS Keywords:
        ///   Ａ Upper case DBCS alphabet (A-Z).
        ///   ａ Lower case DBCS alphabet (a-z).
        ///   Ｋ DBCS Katakana.
        ///   ９ DBCS Numbers (0-9).
        ///   ＃ DBCS numbers and number related symbols (0-9, +-$%\,.).
        ///   ＠ DBCS symbols.
        ///   Ｊ Hiragana.
        ///   Ｚ All DBCS characters without Space.
        /// SBCS Keywords:
        ///   A Upper case alphabet (A-Z).
        ///   a Lower case alphabet (a-z).
        ///   K Katakana.
        ///   9 Numbers (0-9).
        ///   # Numbers and number related symbols (0-9, +-$%\,.).
        ///   @ Symbols.
        ///   H All SBCS characters without Space.
        ///   ^ Any character not included in the specified format.
        ///   \ Escape character.
        /// For example, format = 'Ａ', then the wijinputtext can only input the upper case DBCS alphabet.
        /// If you input the SBCS 'k', then it will be automatic conver to DBCS 'Ｋ' if the autoConvert is true,
        /// But if the autoConvert is false, you can't input 'k'.
        /// </remarks>
        [DefaultValue("")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [C1Description("C1Input.InputFormat")]
#if !EXTENDER
#if ASP_NET4
        [Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputTextFormatTypeEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputTextFormatTypeEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#else
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputTextFormatTypeEditor, C1.Web.Wijmo.Controls.Design.2", typeof(UITypeEditor))]
#endif
#endif
        public string Format
        {
            get
            {
                return GetPropertyValue("Format", "");
            }
            set
            {
                SetPropertyValue("Format", value);
            }
        }

        /// <summary>
        /// Gets or sets whether the maximum length constraint for 
        /// input is byte-based or character-based.
        /// </summary>
        [WidgetOption]
        [DefaultValue(false)]
        [C1Category("Category.Behavior")]
        [C1Description("C1Input.LengthAsByte")]
        public bool LengthAsByte
        {
            get
            {
                return GetPropertyValue("LengthAsByte", false);
            }
            set
            {
                SetPropertyValue("LengthAsByte", value);
            }
        }

        /// <summary>
        /// Gets or sets the maximum length of text that can be input 
        /// in the control.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        /// The value should not be negative.
        /// </exception>
        [DefaultValue(0)]
        [WidgetOption]
        [C1Category("Category.Behavior")]
        [C1Description("C1Input.MaxLength")]
        public int MaxLength
        {
            get
            {
                return GetPropertyValue("MaxLength", 0);
            }
            set
            {
                SetPropertyValue("MaxLength", value);
            }
        }

        /// <summary>
        /// Gets or sets the max count of lines can be input into the Edit control.         .
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">
        /// The value should not be negative.
        /// </exception>
        [DefaultValue(0)]
        [WidgetOption]
        [C1Category("Category.Behavior")]
        [C1Description("C1Input.MaxLineCount")]
        public int MaxLineCount
        {
            get
            {
                return GetPropertyValue("MaxLineCount", 0);
            }
            set
            {
                SetPropertyValue("MaxLineCount", value);
            }
        }

        /// <summary>
        /// Gets or sets the password char.
        /// </summary>        
        [RefreshProperties(RefreshProperties.All)]
        [WidgetOption]
        [C1Category("Category.Behavior")]
        [C1Description("C1Input.PasswordChar")]
        [DefaultValue("")]
        public string PasswordChar
        {
            get
            {
                return GetPropertyValue("PasswordChar", "");
            }
            set
            {
                SetPropertyValue("PasswordChar", value);
            }
        }

        /// <summary>
        /// Determines the default text to display in an input control.
        /// </summary>
        [C1Description("C1Input.Text")]
        [C1Category("Category.Data")]
        [WidgetOption]
        [DefaultValue("")]
        public string Text
        {
            get
            {
                return GetPropertyValue("Text", "");
            }
            set
            {
                SetPropertyValue("Text", value);
            }
        }

        /// <summary>
        /// Determines the default text to display in an input control.
        /// </summary>
        [C1Description("C1Input.MultiLine")]
        [C1Category("Category.Data")]
        [DefaultValue(false)]
        public bool MultiLine
        {
            get
            {
                return GetPropertyValue("MultiLine", false);
            }
            set
            {
                SetPropertyValue("MultiLine", value);
            }
        }

        /// <summary>
        ///  Gets or sets how to display the ellipsis string when the the control's content is longer than its width.
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(EllipsisMode.None)]
        [C1Description("C1Input.Ellipsis")]
        [WidgetOption]
        public EllipsisMode Ellipsis
        {
            get
            {
                return GetPropertyValue("Ellipsis", EllipsisMode.None);
            }
            set
            {
                SetPropertyValue("Ellipsis", value);
            }
        }

        /// <summary>
        ///  Gets or sets the ellipsis string shows in the control.
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue("...")]
        [C1Description("C1Input.EllipsisString")]
        [WidgetOption]
        public string EllipsisString
        {
            get
            {
                return GetPropertyValue("EllipsisString", "...");
            }
            set
            {
                SetPropertyValue("EllipsisString", value);
            }
        }

        /// <summary>
        /// Gets or sets whether display the overflow tip.
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [C1Description("C1Input.ShowOverflowTip")]
        [WidgetOption]
        public bool ShowOverflowTip
        {
            get
            {
                return GetPropertyValue("ShowOverflowTip", false);
            }
            set
            {
                SetPropertyValue("ShowOverflowTip", value);
            }
        }

        /// <summary>
        /// Gets or sets whether the text is highlighted in the text control.
        /// </summary>
        [C1Description("C1Input.HighlightText")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(false)]
        public bool HighlightText
        {
            get
            {
                return GetPropertyValue("HighlightText", false);
            }
            set
            {
                SetPropertyValue("HighlightText", value);
            }
        }


        /// <summary>
        /// Occurs when the reading ime string generated.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Input.OnClientReadingImeStringOutput")]
        [DefaultValue("")]
        [WidgetOption]
        [WidgetOptionName("readingImeStringOutput")]
        [WidgetEvent]
        public string OnClientReadingImeStringOutput
        {
            get
            {
                return GetPropertyValue("OnClientReadingImeStringOutput", "");
            }
            set
            {
                SetPropertyValue("OnClientReadingImeStringOutput", value);
            }
        }

        private Pickers _pickers;

        /// <summary>
        ///   Get an ojbect that contains the setting for the dropdown list.
        /// </summary>
        [C1Description("C1Input.Pickers")]
        [C1Category("Category.Data")]
        [NotifyParentProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public virtual Pickers Pickers
        {
            get
            {
                if (this._pickers == null)
                {
                    this._pickers = new Pickers(this.ViewState);
                }
                return _pickers;
            }
        }

        #region Hidden Properties
        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override bool AllowSpinLoop
        {
            get
            {
                return base.AllowSpinLoop;
            }
            set
            {
                base.AllowSpinLoop = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override bool ShowSpinner
        {
            get
            {
                return base.ShowSpinner;
            }
            set
            {
                base.ShowSpinner = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Obsolete]
        public override string NullText
        {
            get
            {
                return base.NullText;
            }
            set
            {
                base.NullText = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Obsolete]
        public override bool ShowNullText
        {
            get
            {
                return base.ShowNullText;
            }
            set
            {
                base.ShowNullText = value;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new string OnClientSpinDown
        {
            get
            {
                return base.OnClientSpinDown;
            }
            set
            {
                base.OnClientSpinUp = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new string OnClientSpinUp
        {
            get
            {
                return base.OnClientSpinUp;
            }
            set
            {
                base.OnClientSpinUp = value;
            }
        }

        /// <summary>
        ///
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override SpinnerAlign SpinnerAlign
        {
            get
            {
                return base.SpinnerAlign;
            }
            set
            {
                base.SpinnerAlign = value;
            }
        }

        ///// <summary>
        /////
        ///// </summary>
        //[Browsable(false)]
        //[EditorBrowsable(EditorBrowsableState.Never)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public override string Placeholder
        //{
        //    get
        //    {
        //        return base.Placeholder;
        //    }
        //    set
        //    {
        //        base.Placeholder = value;
        //    }
        //}

        /// <summary>
        ///
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new CultureInfo Culture
        {
            get
            {
                return base.Culture;
            }
            set
            {
                base.Culture = value;
            }
        }


        #endregion

    }
}
