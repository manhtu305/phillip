﻿using System;
using System.ComponentModel;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{
	class DbValueTypeConverter : TypeConverter
	{
		bool IsConvertable(Type sourceType)
		{
			return sourceType == typeof(double) || sourceType == typeof(float) || sourceType == typeof(int) || sourceType == typeof(string);
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (IsConvertable(sourceType))
				return true;
			return base.CanConvertFrom(context, sourceType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			if (value == null || value == DBNull.Value)
				return DBNull.Value;

			if (IsConvertable(value.GetType()))
			{
				double num = Convert.ToDouble(value);
				if (double.IsNaN(num))
					return DBNull.Value;

				return num;
			}

			return base.ConvertFrom(context, culture, value);
		}
	}

	class DateDbValueTypeConverter : TypeConverter
	{
		bool IsConvertable(Type sourceType)
		{
			return sourceType == typeof(string);
		}

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (IsConvertable(sourceType))
				return true;
			return base.CanConvertFrom(context, sourceType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			if (value == null || value == DBNull.Value)
				return DBNull.Value;

			if (IsConvertable(value.GetType()))
			{
				var date = Convert.ToDateTime(value);
				if (date == null)
				{
					return DBNull.Value;
				}
				return date;
			}

			return base.ConvertFrom(context, culture, value);
		}
	}
}
