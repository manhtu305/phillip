﻿using System.ComponentModel;
using System;
using System.Web.UI;
using System.Drawing.Design;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif


#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{
#if EXTENDER
	public partial class C1InputMaskExtender
#else 
	public partial class C1InputMask
#endif
	{
		#region ** options

		/// <summary>
		/// Determines the input mask to use at run time.
		/// </summary>
        [C1Description("C1Input.Mask")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue("")]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Use MaskFormat instead")]
		public string Mask
		{
			get
			{
                return this.MaskFormat;
			}
			set
			{
                this.MaskFormat = value;
			}
		}


        /// <summary>
        /// Determines the form mode for the MaskFormat property.
        /// </summary>
        [C1Description("C1Input.FormatMode")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(FormatMode.Simple)]
        public FormatMode FormatMode
        {
            get
            {
                return GetPropertyValue("FormatMode", FormatMode.Simple);
            }
            set
            {
                if (value != this.FormatMode)
                {
                    SetPropertyValue("FormatMode", value);
                }
            }
        }

        /// <summary>
        /// Determines the input mask to use at run time.
        /// </summary>
        [C1Description("C1Input.Mask")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue("")]
#if !EXTENDER
#if ASP_NET4
        [Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputMaskUITypeEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputMaskUITypeEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#else
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputMaskUITypeEditor, C1.Web.Wijmo.Controls.Design.2", typeof(UITypeEditor))]
#endif
#endif
        public string MaskFormat
        {
            get
            {
                return GetPropertyValue("MaskFormat", "");
            }
            set
            {
                SetPropertyValue("MaskFormat", value);
            }
        }


		/// <summary>
		/// Determines the character used to represent the absence of user input.
		/// </summary>
        [C1Description("C1Input.PromptChar")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue('_')]
		public char PromptChar
		{
			get
			{
				return GetPropertyValue("PromptChar", '_');
			}
			set
			{
				if (PasswordChar.Length > 0 && value == PasswordChar[0])  return;
				if (value == '\0') return;
				SetPropertyValue("PromptChar", value);
			}
		}

        /// <summary>
        /// Determines the default text to display in an input control.
        /// </summary>
        [C1Description("C1Input.Text")]
        [C1Category("Category.Data")]
        [WidgetOption]
        [DefaultValue("")]
#if !EXTENDER
#if ASP_NET4
        [Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputTextUITypeEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputTextUITypeEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#else
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1InputTextUITypeEditor, C1.Web.Wijmo.Controls.Design.2", typeof(UITypeEditor))]
#endif
#endif
        public string Text
        {
            get
            {
                return GetPropertyValue("Text", "");
            }
            set
            {
                SetPropertyValue("Text", value);
            }
        }

		/// <summary>
		/// Determines how an input character that matches the prompt character should be handled.
		/// </summary>
        [C1Description("C1Input.ResetOnPrompt")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(true)]
		public bool ResetOnPrompt
		{
			get
			{
				return GetPropertyValue("ResetOnPrompt", true);
			}
			set
			{
				SetPropertyValue("ResetOnPrompt", value);
			}
		}

		/// <summary>
		/// Determines how a space input character should be handled.
		/// </summary>
        [C1Description("C1Input.ResetOnSpace")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(true)]
		public bool ResetOnSpace
		{
			get
			{
				return GetPropertyValue("ResetOnSpace", true);
			}
			set
			{
				SetPropertyValue("ResetOnSpace", value);
			}
		}

		/// <summary>
        ///  Indicates whether the prompt character can be entered as valid data by the user. 
        ///  The prompt character is the character seen in the text box before a user enters input.  
        ///  See the PromptChar Property for more information.
		/// </summary>
        [C1Description("C1Input.AllowPromptAsInput")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(false)]
		public bool AllowPromptAsInput
		{
			get
			{
				return GetPropertyValue("AllowPromptAsInput", false);
			}
			set
			{
				SetPropertyValue("AllowPromptAsInput", value);
			}
		}

		/// <summary>
		/// Indicates whether the prompt characters in the input mask are hidden when the input loses focus.
		/// </summary>
        [C1Description("C1Input.HidePromptOnLeave")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(false)]
		public bool HidePromptOnLeave
		{
			get
			{
				return GetPropertyValue("HidePromptOnLeave", false);
			}
			set
			{
				SetPropertyValue("HidePromptOnLeave", value);
			}
		}

		/// <summary>
		/// Indicates whether the user is allowed to re-enter literal values.
		/// </summary>
        [C1Description("C1Input.SkipLiterals")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(true)]
		public bool SkipLiterals
		{
			get
			{
				return GetPropertyValue("SkipLiterals", true);
			}
			set
			{
				SetPropertyValue("SkipLiterals", value);
			}
		}

		/// <summary>
		/// Determines the character to be substituted for the actual input characters.
		/// </summary>
        [C1Description("C1Input.PasswordChar")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue("")]
		public virtual string PasswordChar
		{
			get
			{
				return GetPropertyValue("PasswordChar", "");
			}
			set
			{
				SetPropertyValue("PasswordChar", value);
			}
		}
        /// <summary>
        /// Gets or sets how to select the text when the control receives the focus.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Input.HighlightText")]
        [WidgetOption]
        [DefaultValue(HighlightText.None)]
        public HighlightText HighlightText
        {
            get
            {
                return GetPropertyValue("HighlightText", HighlightText.None);
            }
            set
            {
                SetPropertyValue("HighlightText", value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete]
        public override string NullText
        {
            get
            {
                return base.NullText;
            }
            set
            {
                base.NullText = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
	    [Browsable(false)]
	    [EditorBrowsable(EditorBrowsableState.Never)]
	    public override bool AllowSpinLoop
	    {
	        get
	        {
	            return base.AllowSpinLoop;
	        }
	        set
	        {
	            base.AllowSpinLoop = value;
	        }
	    }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
	    public override SpinnerAlign SpinnerAlign 
        {
            get
            {
                return base.SpinnerAlign;
            }
            set
            {
                base.SpinnerAlign = value;
            }
	    }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
	    public override string OnClientSpinDown 
        {
            get
            {
                return base.OnClientSpinDown;
            }
            set
            {
                base.OnClientSpinDown = value;
            }
	    }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
	    public override string OnClientSpinUp
        {
            get
            {
                return base.OnClientSpinUp;
            }
            set
            {
                base.OnClientSpinUp = value;
            }
	    }

	    /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete]
        public override bool ShowNullText
        {
            get
            {
                return base.ShowNullText;
            }
            set
            {
                base.ShowNullText = value;
            }
        }

		// Hide the showspinner for input mark, because the showSpinner is not support in inputmark.
		// edit by dail 2012-6-21
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override bool ShowSpinner
		{
			get
			{
				return base.ShowSpinner;
			}
			set
			{
				base.ShowSpinner = value;
			}
		}

        /// <summary>
        /// Gets or sets the tab action.
        /// </summary>
        [C1Description("C1Input.TabAction")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(TabAction.Control)]
        public virtual TabAction TabAction
        {
            get
            {
                return GetPropertyValue("TabAction", TabAction.Control);
            }
            set
            {
                SetPropertyValue("TabAction", value);
            }
        }

        /// <summary>
        /// Gets or sets whether the control automatically converts to the proper format according to the format setting.
        /// </summary>
        [C1Description("C1Input.AutoConvert")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(true)]
        public bool AutoConvert
        {
            get
            {
                return GetPropertyValue("AutoConvert", true);
            }
            set
            {
                SetPropertyValue("AutoConvert", value);
            }
        }

        private Pickers _pickers;

        /// <summary>
        ///   Get an object contains the settings for the dropdown list. 
        /// </summary>
        [C1Description("C1Input.Pickers")]
        [C1Category("Category.Data")]
        [NotifyParentProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public virtual Pickers Pickers
        {
            get
            {
                if (this._pickers == null)
                {
                    this._pickers = new Pickers(this.ViewState);
                }
                return _pickers;
            }
        }

	    #endregion
	}
}
