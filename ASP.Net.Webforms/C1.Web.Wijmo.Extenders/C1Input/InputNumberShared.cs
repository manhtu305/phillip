﻿using System.ComponentModel;
using System;
using System.Web.UI;


#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Input
#else
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{

#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
	using C1.Web.Wijmo.Extenders.C1ComboBox;
#else
    using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
	public partial class C1InputNumberExtender
#else 
	public partial class C1InputNumber
#endif
	{
		#region ** options


#if EXTENDER
		/// <summary>
		/// Determines the type of the number input.
		/// </summary>
        [C1Description("C1Input.NumberType")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[WidgetOptionName("type")]
		[DefaultValue(NumberType.Numeric)]
		public NumberType NumberType
		{
			get
			{
				return GetPropertyValue<NumberType>("NumberType", NumberType.Numeric);
			}
			set
			{
				SetPropertyValue<NumberType>("NumberType", value);
			}
		}
#else
        /// <summary>
		/// Determines the type of the number input.
		/// </summary>
        [C1Description("C1Input.NumberType")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[WidgetOptionName("type")]
		[DefaultValue(NumberType.Numeric)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual NumberType NumberType
		{
			get { return NumberType.Numeric; }
		}
#endif

        /// <summary>
        ///   Determine the class string that when the input value is negative.
        /// </summary>
        [C1Description("C1Input.NegativeClass")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue("ui-state-negative")]
		public string NegativeClass
		{
			get
			{
                return GetPropertyValue("NegativeClass", "ui-state-negative");
			}
			set
			{
				SetPropertyValue("NegativeClass", value);
			}
		}

        /// <summary>
        ///   Determine the prefix string used for negative value.
        /// </summary>
        [C1Description("C1Input.NegativePrefix")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        public string NegativePrefix 
        {
            get
            {
                return GetPropertyValue("NegativePrefix", this.DefaultNegativePrefix);
            }
            set
            {
                SetPropertyValue("NegativePrefix", value);
            }
        }

        /// <summary>
        ///   Determine the suffix string used for nagative value.
        /// </summary>
        [C1Description("C1Input.NegativeSuffix")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        public string NegativeSuffix 
        {
            get
            {
                return GetPropertyValue("NegativeSuffix", this.DefaultNegativeSuffix);
            }
            set
            {
                SetPropertyValue("NegativeSuffix", value);
            }
        }

        /// <summary>
        ///   Determine the prefix string used for positive value.
        /// </summary>
        [C1Description("C1Input.PositivePrefix")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        public string PositivePrefix 
        {
            get
            {
                return GetPropertyValue("PositivePrefix", this.DefaultPositivePrefix);
            }
            set
            {
                SetPropertyValue("PositivePrefix", value);
            }
        }

        /// <summary>
        ///   Determine the suffix stirng used for positive value.
        /// </summary>
        [C1Description("C1Input.PositiveSuffix")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        public string PositiveSuffix 
        {
            get
            {
                return GetPropertyValue("PositiveSuffix", this.DefaultPositiveSuffix);
            }
            set
            {
                SetPropertyValue("PositiveSuffix", value);
            }
        }

        /// <summary>
        ///   Determines the currency symbol string.
        /// </summary>
        [C1Description("C1Input.CurrencySymbol")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        public string CurrencySymbol
        {
            get
            {
                return GetPropertyValue("CurrencySymbol", this.DefaultCurrencySymbol);
            }
            set
            {
                SetPropertyValue("CurrencySymbol", value);
            }
        }

		/// <summary>
		/// Determines the default numeric value.
		/// </summary>
        [C1Description("C1Input.Value")]
        [C1Category("Category.Data")]
		[WidgetOption]
		[DefaultValue(0.0)]
		public double Value
		{
			get
			{
				object o = GetPropertyValue<double>("Value", 0.0);
				double dbl = (o != null) ? (double)o : 0;
				if (!double.IsNaN(this.MinValue))
					if (dbl < this.MinValue)
						dbl = this.MinValue;
				if (!double.IsNaN(this.MaxValue))
					if (dbl > this.MaxValue)
						dbl = this.MaxValue;

				return dbl;
			}
			set
			{
				SetPropertyValue("Value", value);
			}
		}

        /// <summary>
        /// Gets or sets the IME mode.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [C1Description("C1Input.ImeMode")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(ImeMode.Auto)]
        public ImeMode ImeMode
        {
            get
            {
                return base.ImeMode;
            }
            set
            {
                base.ImeMode = value;
            }
        }

        /// <summary>
        /// Gets or sets whether the text is highlighted in the number control.
        /// </summary>
        [C1Description("C1Input.HighlightText")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(false)]
	    public bool HighlightText
	    {
            get
            {
                return GetPropertyValue("HighlightText", false);
            }
            set
            {
                SetPropertyValue("HighlightText", value);
            }
	    }

	    private string DefaultNegativePrefix
        {
            get
            {
                return this.GetDefaultPrefixSuffix(true, true);
            }
        }

        private string DefaultNegativeSuffix
        {
            get
            {
                return this.GetDefaultPrefixSuffix(true, false);
            }
        }

        private string DefaultPositivePrefix
        {
            get
            {
                return this.GetDefaultPrefixSuffix(false, true);
            }
        }

        private string DefaultPositiveSuffix
        {
            get
            {
                return this.GetDefaultPrefixSuffix(false, false);
            }
        }

        private string GetDefaultPrefixSuffix(bool isNegative, bool isPrefix)
        {
#if EXTENDER        
            if (isNegative && isPrefix)
            {
                return "-";
            }
            return "";          
#else
            string groupSep, decimalSep;
            int decimalCount;
            int[] groupSizes;
            string formatPattern = C1Input.C1NumericToStringFormat.GetFormatPattern(
                isNegative, this.Culture, this.NumberType, this.DecimalPlaces, this.ShowGroup,
                out groupSep, out decimalSep, out decimalCount, out groupSizes);

            formatPattern = formatPattern.Replace("%", this.Culture.NumberFormat.PercentSymbol);
            formatPattern = formatPattern.Replace("$", this.Culture.NumberFormat.CurrencySymbol);
            string[] prefixsuffix = formatPattern.Split('n');
            int index = 0;
            if (!isPrefix)
            {
                index = 1;
            }
            return prefixsuffix.Length > index ? prefixsuffix[index] : "";
#endif
        }

	    private string DefaultCurrencySymbol
	    {
	        get
	        {
	            return this.Culture.NumberFormat.CurrencySymbol;
	        }
	    }

        [EditorBrowsable(EditorBrowsableState.Never)]
	    public bool ShouldSerializeNegativePrefix()
	    {
	        return this.NegativePrefix != this.DefaultNegativePrefix;
	    }

	    private void ResetNegativePrefix()
	    {
	        this.NegativePrefix = null;
	    }

        [EditorBrowsable(EditorBrowsableState.Never)]
	    public bool ShouldSerializeNegativeSuffix()
	    {
	        return this.NegativeSuffix != this.DefaultNegativeSuffix;
	    }

	    private void ResetNegativeSuffix()
	    {
	        this.NegativeSuffix = null;
	    }

        [EditorBrowsable(EditorBrowsableState.Never)]
	    public bool ShouldSerializePositivePrefix()
	    {
	        return this.PositivePrefix != this.DefaultPositivePrefix;
	    }

	    private void ResetPositivePrefix()
	    {
	        this.PositivePrefix = null;
	    }

        [EditorBrowsable(EditorBrowsableState.Never)]
	    public bool ShouldSerializePositiveSuffix()
	    {
	        return this.PositiveSuffix != this.DefaultPositiveSuffix;
	    }

	    private void ResetPositiveSuffix()
	    {
	        this.PositiveSuffix = null;
	    }

        [EditorBrowsable(EditorBrowsableState.Never)]
	    public bool ShouldSerializeCurrencySymbol()
	    {
	        return this.CurrencySymbol != this.DefaultCurrencySymbol;
	    }

	    private void ResetCurrencySymbol()
	    {
	        this.CurrencySymbol = null;
	    }


		/// <summary>
		/// Gets or sets the <see cref="Value"/> property value in a data binding-friendly way.
		/// </summary>
		[C1Description("C1Input.DbValue")]
		[C1Category("Category.Data")]
		[DefaultValue(0.0)]
		[TypeConverter(typeof(DbValueTypeConverter))]
		[Browsable(false)]
		public object DbValue
		{
			get { return Value; }
			set { Value = value == DBNull.Value ? 0 : Convert.ToDouble(value); }
		}

		/// <summary>
		/// Determines the minimal value that can be entered for the number input.
		/// </summary>
        [C1Description("C1Input.MinValue")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(-1000000000.0)]
		public double MinValue
		{
			get
			{
				return GetPropertyValue("MinValue", -1000000000.0);
			}
			set
			{
				SetPropertyValue("MinValue", value);
			}
		}

		/// <summary>
		/// Determines the maximum value that can be entered for the number input.
		/// </summary>
        [C1Description("C1Input.MaxValue")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(1000000000.0)]
		public double MaxValue
		{
			get
			{
				return GetPropertyValue("MaxValue", 1000000000.0);
			}
			set
			{
				SetPropertyValue("MaxValue", value);
			}
		}

		///	<summary>
        ///	Indicates whether the thousands group separator will be inserted between each digital group. 
        ///	The number of digits in the thousands group depends on the selected Culture."
		///	</summary>
        [C1Description("C1Input.ShowGroup")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(false)]
		public bool ShowGroup
		{
			get
			{
				return GetPropertyValue("ShowGroup", false);
			}
			set
			{
				SetPropertyValue("ShowGroup", value);
			}
		}

		/// <summary>
		/// Indicates the number of decimal places to display.
		/// </summary>
        [C1Description("C1Input.DecimalPlaces")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(2)]
		public int DecimalPlaces
		{
			get
			{
				return GetPropertyValue("DecimalPlaces", 2);
			}
			set
			{
				SetPropertyValue("DecimalPlaces", value);
			}
		}

		/// <summary>
		/// Determines how much to increase/decrease the input field.
		/// </summary>
        [C1Description("C1Input.Increment")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(1)]
		public int Increment
		{
			get
			{
				return GetPropertyValue("Increment", 1);
			}
			set
			{
				SetPropertyValue("Increment", value);
			}
		}

        private Pickers _pickers;

        /// <summary>
        ///   
        /// </summary>
        [C1Description("C1Input.Pickers")]
        [C1Category("Category.Data")]
        [NotifyParentProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public virtual Pickers Pickers
        {
            get
            {
                if (this._pickers == null)
                {
                    this._pickers = new Pickers(this.ViewState);
                }
                return _pickers;
            }
        }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool BlurOnLastChar
        {
            get
            {
                return base.BlurOnLastChar;
            }

            set
            {
                base.BlurOnLastChar = value;
            }
        }

        #endregion

		#region client side events

		/// <summary>
		/// Occurs after the value changed. 
		/// </summary>
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1Input.OnClientValueChanged")]
		[DefaultValue("")]
		[WidgetOption]
		[WidgetOptionName("valueChanged")]
		[WidgetEvent]
		public string OnClientValueChanged
		{
			get
			{
				return GetPropertyValue("OnClientValueChanged", "");
			}
			set
			{
				SetPropertyValue("OnClientValueChanged", value);
			}
		}

		/// <summary>
		/// Occurs when the value of the input exceeds the valid range. 
		/// </summary>
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1Input.OnClientValueBoundsExceeded")]
		[DefaultValue("")]
		[WidgetOption]
		[WidgetOptionName("valueBoundsExceeded")]
		[WidgetEvent]
		public string OnClientValueBoundsExceeded
		{
			get
			{
				return GetPropertyValue("OnClientValueBoundsExceeded", "");
			}
			set
			{
				SetPropertyValue("OnClientValueBoundsExceeded", value);
			}
		}


		#endregion
	}
}
