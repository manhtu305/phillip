﻿using System.ComponentModel;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing.Design;
using System.Globalization;

#if EXTENDER
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Input", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Input
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Input", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1Input
#endif
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
	using C1.Web.Wijmo.Extenders.C1ComboBox;
#else
	using C1.Web.Wijmo.Controls.C1ComboBox;
	using C1.Web.Wijmo.Controls.Localization;
#endif
    
	[WidgetDependencies(
        typeof(WijInputBase)
#if !EXTENDER
, "extensions.c1input.js",
ResourcesConst.C1WRAPPER_PRO
#endif
    )]
#if EXTENDER
	public partial class C1InputExtenderBase
#else
    public partial class C1InputBase: ICultureControl
#endif
    {
		#region ** options

		/// <summary>
		/// Gets or sets the culture information for the input control.
		/// </summary>
		[C1Description("C1Input.CultureInfo")]
		[C1Category("Category.Behavior")]
		[TypeConverter(typeof(CultureInfoConverter))]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public CultureInfo Culture
		{
			get
			{
				return GetCulture(GetPropertyValue<CultureInfo>("Culture", null));
			}
			set
			{
				SetPropertyValue("Culture", value);
			}
		}

		/// <summary>
		/// The CSS class applied to the widget when an invalid value is entered.
		/// </summary>
		[C1Description("C1Input.InvalidClass")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue("ui-state-error")]
		public string InvalidClass
		{
			get
			{
				return GetPropertyValue("InvalidClass", "ui-state-error");
			}
			set
			{
				SetPropertyValue("InvalidClass", value);
			}
		}

		/// <summary>
		/// Determines the text that will be displayed for blank status.
		/// </summary>
		[C1Description("C1Input.NullText")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue("")]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Use Placeholder property instead")]
		virtual public string NullText
		{
			get
			{
                return this.Placeholder;
			}
			set
			{
                this.Placeholder = value;
			}
		}

		/// <summary>
		/// Show NullText if the value is empty and the control loses its focus.
		/// </summary>
		[C1Description("C1Input.ShowNullText")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Please direct use Placeholder property")]
		virtual public bool ShowNullText
		{
			get
			{
				return GetPropertyValue("ShowNullText", false);
			}
			set
			{
				SetPropertyValue("ShowNullText", value);
			}
		}

		/// <summary>
		/// If true, then the browser response is disabled when the ENTER key is pressed.
		/// </summary>
		[C1Description("C1Input.HideEnter")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(false)]
		public bool HideEnter
		{
			get
			{
				return GetPropertyValue("HideEnter", false);
			}
			set
			{
				SetPropertyValue("HideEnter", value);
			}
		}

		/// <summary>
		/// Determines whether the user can type a value.
		/// </summary>
		[C1Description("C1Input.DisableUserInput")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(false)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Use ReadOnly Instead")]
		public bool DisableUserInput
		{
			get
			{
                return this.ReadOnly;
			}
			set
			{
                this.ReadOnly = value;
			}
		}

		/// <summary>
		/// Determines the alignment of buttons.
		/// </summary>
		[C1Description("C1Input.ButtonAlign")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		[DefaultValue(ButtonAlign.Right)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Use DropdownButtonAlign Instead")]
		public ButtonAlign ButtonAlign
		{
			get
			{
				return GetPropertyValue("ButtonAlign", ButtonAlign.Right);
			}
			set
			{
				SetPropertyValue("ButtonAlign", value);
			}
		}

        /// <summary>
        /// Determines the alignment of the dropdown button.
        /// </summary>
        [C1Description("C1Input.DropDownButtonAlign")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
        [DefaultValue(ButtonAlign.Right)]
        public ButtonAlign DropDownButtonAlign
        {
            get
            {
                return GetPropertyValue("DropDownButtonAlign", ButtonAlign.Right);
            }
            set
            {
                SetPropertyValue("DropDownButtonAlign", value);
            }
        }


        /// <summary>
        /// Determines the spin button alignment.
        /// </summary>
        [C1Description("C1Input.SpinnerAlign")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
        [DefaultValue(SpinnerAlign.VerticalRight)]
        public virtual SpinnerAlign SpinnerAlign
        {
            get
            {
                return GetPropertyValue("SpinnerAlign", SpinnerAlign.VerticalRight);
            }
            set
            {
                SetPropertyValue("SpinnerAlign", value);
            }
        }

		/// <summary>
		/// Determines whether the trigger button is displayed.
		/// </summary>
		[C1Description("C1Input.ShowTrigger")]
		[C1Category("Category.Appearance")]
        //[WidgetOption]
        //[DefaultValue(false)]
        [Obsolete("Use ShowDropdownButton instead")]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShowTrigger
		{
			get
			{
				return GetPropertyValue("ShowTrigger", this.DefaultShowDropDownButton);
			}
			set
			{
				SetPropertyValue("ShowTrigger", value);
			}
		}

        /// <summary>
        /// Determines whether the dropdown button is displayed.
        /// </summary>
        [C1Description("C1Input.ShowDropDownButton")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
        public bool ShowDropDownButton
        {
            get
            {
                return GetPropertyValue("ShowTrigger", this.DefaultShowDropDownButton);
            }
            set
            {
                SetPropertyValue("ShowTrigger", value);
            }
        }
        
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeShowTrigger()
        {
            return false;
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
	    public bool ShouldSerializeShowDropDownButton()
	    {
	        return this.ShowDropDownButton != this.DefaultShowDropDownButton;
	    }

	    private void ResetShowDropDownButton()
	    {
	        this.SetPropertyValue("ShowTrigger", "");
	    }

		/// <summary>
		/// Determines whether the spinner button is displayed.
		/// </summary>
		[C1Description("C1Input.ShowSpinner")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		[DefaultValue(false)]
		public virtual bool ShowSpinner
		{
			get
			{
				return GetPropertyValue("ShowSpinner", false);
			}
			set
			{
				SetPropertyValue("ShowSpinner", value);
			}
		}

#if EXTENDER
		private Collection<C1ComboBoxItem> _items = null;

		/// <summary>
		/// A value that specifies the underlying data source provider of wijcombobox
		/// </summary>
		[C1Description("C1Input.ComboItems")]
		[C1Category("Category.Data")]
		[WidgetOption]
		[PersistenceMode(PersistenceMode.InnerProperty)]
        [Browsable(false)]
        [Obsolete("Use Pickers.List instead")]
        [EditorBrowsable(EditorBrowsableState.Never)]
		public Collection<C1ComboBoxItem> ComboItems
		{
			get
			{
				if (_items == null)
				{
					_items = new Collection<C1ComboBoxItem>();
				}
				return _items;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeComboItems()
		{
			return (_items != null && _items.Count > 0);
		}
#else
		private Collection<C1ComboBoxItem> _items = null;

		/// <summary>
		/// A value that specifies the underlying data source provider for the wijcombobox.
		/// </summary>
		[C1Description("C1Input.ComboItems")]
		[C1Category("Category.Data")]
		[NotifyParentProperty(true)]
		[Browsable(false)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[MergableProperty(false)]
		[Layout(LayoutType.Appearance)]
		[CollectionItemType(typeof(C1ComboBoxItem))]
		[WidgetOption]
		[ReadOnly(true)]
        [Obsolete("Use Pickers.List instead")]
        [EditorBrowsable(EditorBrowsableState.Never)]
		public Collection<C1ComboBoxItem> ComboItems
		{
			get
			{
				if (_items == null)
					_items = new Collection<C1ComboBoxItem>();
				return _items;
			}
		}
#endif


		/// <summary>
		/// Determines the height of the drop-down list.
		/// </summary>
		[C1Description("C1Input.ComboHeight")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		[DefaultValue(0)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Use Pickers.ComboHeight instead")]
		public int ComboHeight
		{
			get
			{
				return GetPropertyValue<int>("ComboHeight", 0);
			}
			set
			{
				SetPropertyValue<int>("ComboHeight", value);
			}
		}

        /// <summary>
        /// Determines whether allow the spin behavior loop.
        /// </summary>
        [C1Description("C1Input.AllowSpinLoop")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(false)]
        public virtual bool AllowSpinLoop
        {
            get
            {
                return GetPropertyValue<bool>("AllowSpinLoop", false);
            }
            set
            {
                SetPropertyValue<bool>("AllowSpinLoop", value);
            }
        }

        /// <summary>
        /// Gets or sets whether or not the next control in the tab order receives 
        /// the focus as soon as the control is filled at the last character.
        /// </summary>
        [C1Description("C1Input.BlurOnLastChar")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(false)]
        public virtual bool BlurOnLastChar
        {
            get
            {
                return GetPropertyValue<bool>("BlurOnLastChar", false);
            }
            set
            {
                SetPropertyValue<bool>("BlurOnLastChar", value);
            }
        }

        /// <summary>
        /// Gets or set whether the focus automatically moves to the next or previous
        /// tab ordering control when pressing the left, right arrow keys.
        /// </summary>
        [C1Description("C1Input.BlurOnLeftRightKey")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(BlurOnLeftRightKey.None)]
        public BlurOnLeftRightKey BlurOnLeftRightKey
        {
            get
            {
                return GetPropertyValue<BlurOnLeftRightKey>("BlurOnLeftRightKey", BlurOnLeftRightKey.None);
            }
            set
            {
                SetPropertyValue<BlurOnLeftRightKey>("BlurOnLeftRightKey", value);
            }
        }

        /// <summary>
        /// Gets or sets the IME mode.
        /// </summary>
        [C1Description("C1Input.ImeMode")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(ImeMode.Auto)]
	    public ImeMode ImeMode
	    {
            get
            {
                return GetPropertyValue<ImeMode>("ImeMode", ImeMode.Auto);
            }
            set
            {
                SetPropertyValue<ImeMode>("ImeMode", value);
            }
	    }

        /// <summary>
        /// Determines the text that will be displayed for blank status.
        /// </summary>
        [C1Description("C1Input.NullText")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue("")]
        public virtual string Placeholder
        {
            get
            {
                return GetPropertyValue("NullText", "");
            }
            set
            {
                SetPropertyValue("NullText", value);
            }
        }

        /// <summary>
        /// Determines whether the user can type a value.
        /// </summary>
        [C1Description("C1Input.DisableUserInput")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(false)]
        public bool ReadOnly
        {
            get
            {
                return GetPropertyValue("DisableUserInput", false);
            }
            set
            {
                SetPropertyValue("DisableUserInput", value);
            }
        }

		#endregion

		#region client events


		/// <summary>
		/// Occurs before the widget is initialized.
		/// </summary>
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1Input.OnClientInitializing")]
		[DefaultValue("")]
		[WidgetOption]
		[WidgetOptionName("initializing")]
		[WidgetEvent]
		public string OnClientInitializing
		{
			get
			{
				return GetPropertyValue("OnClientInitializing", "");
			}
			set
			{
				SetPropertyValue("OnClientInitializing", value);
			}
		}

		/// <summary>
		/// Occurs after the widget is initialized.
		/// </summary>
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1Input.OnClientInitialized")]
		[DefaultValue("")]
		[WidgetOption]
		[WidgetOptionName("initialized")]
		[WidgetEvent]
		public string OnClientInitialized
		{
			get
			{
				return GetPropertyValue("OnClientInitialized", "");
			}
			set
			{
				SetPropertyValue("OnClientInitialized", value);
			}
		}

		/// <summary>
		/// Occurs when the mouse is pressed on the trigger button.
		/// </summary>
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1Input.OnClientTriggerMouseDown")]
		[DefaultValue("")]
		[WidgetOption]
		[WidgetOptionName("triggerMouseDown")]
		[WidgetEvent]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Use OnClientDropDownButtonMouseDown instead")]
		public string OnClientTriggerMouseDown
		{
			get
			{
				return GetPropertyValue("OnClientTriggerMouseDown", "");
			}
			set
			{
				SetPropertyValue("OnClientTriggerMouseDown", value);
			}
		}

		/// <summary>
		/// Occurs when the mouse is released on the trigger button.
		/// </summary>
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1Input.OnClientTriggerMouseUp")]
		[DefaultValue("")]
		[WidgetOption]
		[WidgetOptionName("triggerMouseUp")]
		[WidgetEvent]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Use OnClientDropDownButtonMouseUp instead")]
		public string OnClientTriggerMouseUp
		{
			get
			{
				return GetPropertyValue("OnClientTriggerMouseUp", "");
			}
			set
			{
				SetPropertyValue("OnClientTriggerMouseUp", value);
			}
		}

        /// <summary>
        /// Occurs when the mouse is pressed on the dropdown button.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Input.OnClientDropdownButtonMouseDown")]
        [DefaultValue("")]
        [WidgetOption]
        [WidgetOptionName("dropDownButtonMouseDown")]
        [WidgetEvent]
        public string OnClientDropDownButtonMouseDown
        {
            get
            {
                return GetPropertyValue("OnClientDropDownButtonMouseDown", "");
            }
            set
            {
                SetPropertyValue("OnClientDropDownButtonMouseDown", value);
            }
        }

        /// <summary>
        /// Occurs when the mouse is released on the dropdown button.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Input.OnClientDropDownButtonMouseUp")]
        [DefaultValue("")]
        [WidgetOption]
        [WidgetOptionName("dropDownButtonMouseUp")]
        [WidgetEvent]
        public string OnClientDropDownButtonMouseUp
        {
            get
            {
                return GetPropertyValue("OnClientDropDownButtonMouseUp", "");
            }
            set
            {
                SetPropertyValue("OnClientDropDownButtonMouseUp", value);
            }
        }

		/// <summary>
		/// Occurs when the text of the input is changed.
		/// </summary>
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1Input.OnClientTextChanged")]
		[DefaultValue("")]
		[WidgetOption]
		[WidgetOptionName("textChanged")]
		[WidgetEvent]
		public string OnClientTextChanged
		{
			get
			{
				return GetPropertyValue("OnClientTextChanged", "");
			}
			set
			{
				SetPropertyValue("OnClientTextChanged", value);
			}
		}

		/// <summary>
		/// Occurs when an invalid character is entered.
		/// </summary>
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1Input.OnClientInvalidInput")]
		[DefaultValue("")]
		[WidgetOption]
		[WidgetOptionName("invalidInput")]
		[WidgetEvent]
		public string OnClientInvalidInput
		{
			get
			{
				return GetPropertyValue("OnClientInvalidInput", "");
			}
			set
			{
				SetPropertyValue("OnClientInvalidInput", value);
			}
		}


        /// <summary>
        /// Occurs when the dropdown editor opended.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Input.OnClientDropDownOpen")]
        [DefaultValue("")]
        [WidgetOption]
        [WidgetOptionName("dropDownOpen")]
        [WidgetEvent]
        public string OnClientDropDownOpen
        {
            get
            {
                return GetPropertyValue("OnClientDropDownOpen", "");
            }
            set
            {
                SetPropertyValue("OnClientDropDownOpen", value);
            }
        }

        /// <summary>
        /// Occurs when the dropdown editor closed.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Input.OnClientDropDownClose")]
        [DefaultValue("")]
        [WidgetOption]
        [WidgetOptionName("dropDownClose")]
        [WidgetEvent]
        public string OnClientDropDownClose
        {
            get
            {
                return GetPropertyValue("OnClientDropDownClose", "");
            }
            set
            {
                SetPropertyValue("OnClientDropDownClose", value);
            }
        }

        /// <summary>
        /// Occurs when the spindown button clicked.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Input.OnClientSpinDown")]
        [DefaultValue("")]
        [WidgetOption]
        [WidgetOptionName("spinDown")]
        [WidgetEvent]
        public virtual string OnClientSpinDown
        {
            get
            {
                return GetPropertyValue("OnClientSpinDown", "");
            }
            set
            {
                SetPropertyValue("OnClientSpinDown", value);
            }
        }

        /// <summary>
        /// Occurs when the spinup button clicked.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Input.OnClientSpinUp")]
        [DefaultValue("")]
        [WidgetOption]
        [WidgetOptionName("spinUp")]
        [WidgetEvent]
        public virtual string OnClientSpinUp
        {
            get
            {
                return GetPropertyValue("OnClientSpinUp", "");
            }
            set
            {
                SetPropertyValue("OnClientSpinUp", value);
            }
        }

        /// <summary>
        /// Occurs when the key exit event fired.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Input.OnClientKeyExit")]
        [DefaultValue("")]
        [WidgetOption]
        [WidgetOptionName("keyExit")]
        [WidgetEvent]
	    public string OnClientKeyExit
	    {
            get
            {
                return GetPropertyValue("OnClientKeyExit", "");
            }
            set
            {
                SetPropertyValue("OnClientKeyExit", value);
            }
	    }


	    #endregion

	    internal virtual bool DefaultShowDropDownButton 
	    {
	        get
	        {
	            return false;
	        }
	    }

	}

    /// <summary>
    /// Specifies the action for left / right arrow key
    /// </summary>
    /// <remarks>
    /// Left or right key lead to lose focus when the caret is at the left-most or right-most position.
    /// </remarks>
    public enum BlurOnLeftRightKey
    {
        /// <summary>
        /// No action
        /// </summary>
        None = 0,
        /// <summary>
        /// Move to the previous control when press left or Ctrl+left keys on the first character of the control
        /// </summary>
        Left = 1,
        /// <summary>
        /// Move to the next control when press right or Ctrl+right keys on the last character of the control
        /// </summary>
        Right = 2,
        /// <summary>
        /// Move to the previous/next control when press left/right or Ctrl+left/Ctrl+right keys 
        /// on the first/last character of the control
        /// </summary>
        Both = 3,
    }

    /// <summary>
    /// Specifies whether IME mode should be supported.
    /// </summary>
    public enum ImeMode
    {
        /// <summary>
        /// Support IME mode
        /// </summary>
        Auto = 0,
        /// <summary>
        /// Support IME mode
        /// </summary>
        Active = 1,
        /// <summary>
        /// Support IME mode
        /// </summary>
        InActive = 2,
        /// <summary>
        /// Disable IME mode
        /// </summary>
        Disabled = 3
    }

    /// <summary>
    /// Specifies how the tab key works
    /// </summary>
    /// <remarks>
    /// Tab key is to move focus among controls or fields.
    /// </remarks>
    public enum TabAction
    {
        /// <summary>
        /// Used to move focus between controls
        /// </summary>
        Control = 0,
        /// <summary>
        /// Used to move cursor position between fields in control
        /// </summary>
        Field = 1,
    }

    /// <summary>
    ///   Specifies the settings of the drop down list.
    /// </summary>
    public class Pickers
    {
        internal protected readonly StateBag _viewState;

        internal Pickers(StateBag viewState)
        {
            this._viewState = viewState;
        }

        /// <summary>
        /// Gets the property value by property name.
        /// </summary>
        /// <typeparam name="V"></typeparam>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="nullValue">The null value.</param>
        /// <returns></returns>
       internal protected V GetPropertyValue<V>(string propertyName, V nullValue)
        {
            if (this._viewState[propertyName] == null)
            {
                return nullValue;
            }
            return (V)this._viewState[propertyName];
        }

        /// <summary>
        /// Sets the property value by property name.
        /// </summary>
        /// <typeparam name="V"></typeparam>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="value">The value.</param>
        internal protected void SetPropertyValue<V>(string propertyName, V value)
        {
            this._viewState[propertyName] = value;
        }


        /// <summary>
        /// Determines the width of the drop-down list.
        /// </summary>
        [C1Description("C1Input.ComboWidth")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
        [DefaultValue(0)]
        [NotifyParentProperty(true)]
        public int Width
        {
            get
            {
                return this.GetPropertyValue("Pickers.Width", 0);
            }
            set
            {
                this.SetPropertyValue("Pickers.Width", value);
            }
        }


        /// <summary>
        /// Determines the height of the drop-down list.
        /// </summary>
        [C1Description("C1Input.ComboHeight")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
        [DefaultValue(0)]
        [NotifyParentProperty(true)]
        public int Height
        {
            get
            {
                return this.GetPropertyValue("Pickers.Height", 0);
            }
            set
            {
                this.SetPropertyValue("Pickers.Height", value);
            }
        }

#if EXTENDER
		private Collection<C1ComboBoxItem> _items = null;

		/// <summary>
		/// A value that specifies the underlying data source provider of wijcombobox
		/// </summary>
		[C1Description("C1Input.ComboItems")]
		[C1Category("Category.Data")]
		[WidgetOption]
		[PersistenceMode(PersistenceMode.InnerProperty)]
        [NotifyParentProperty(true)]
		public Collection<C1ComboBoxItem> List
		{
			get
			{
				if (_items == null)
				{
					_items = new Collection<C1ComboBoxItem>();
				}
				return _items;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeComboItems()
		{
			return (_items != null && _items.Count > 0);
		}
#else
        private Collection<C1ComboBoxItem> _items;

        /// <summary>
        /// A value that specifies the underlying data source provider for the wijcombobox.
        /// </summary>
        [C1Description("C1Input.ComboItems")]
        [C1Category("Category.Data")]
        [NotifyParentProperty(true)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [MergableProperty(false)]
        [Layout(LayoutType.Appearance)]
        [CollectionItemType(typeof(C1ComboBoxItem))]
        [WidgetOption]
        [ReadOnly(true)]
        public Collection<C1ComboBoxItem> List
        {
            get
            {
                if (_items == null)
                {
                    _items = new Collection<C1ComboBoxItem>();                    
                }

                return _items;
            }
        }
#endif
    }
}
