﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Collections;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{

	#region ** PositionSettings class
	/// <summary>
	/// Define the settings of the jQuery position utility.
	/// </summary>
	public class PositionSettings : Settings, ICustomOptionType, IJsonEmptiable
	{

        private Position _my;
        private Position _at;
        private Collision _colision;
        private Offset _offset;
        private bool _serializeAll;

		/// <summary>
		/// Initializes a new instance of the PositionSettings class.
		/// </summary>
		public PositionSettings()
		{
			this.My = new Position();
			this.At = new Position();
			this.Offset = new Offset();
			this.Collision = new Collision();
		}

		/// <summary>
		/// Gets and sets the "my" option of the position utility. 
		/// </summary>
		[DefaultValue(typeof(Position), "Left, Top")]
		[C1Description("PositionSettings.My")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		public Position My
		{
			get
			{
                if (_my == null)
                {
                    _my = new Position();
                }
                return _my;
			}
			set
			{
                _my = value;
			}
		}

        internal bool SerializeAll
        {
            get 
            { 
                return _serializeAll; 
            }
            set 
            { 
                _serializeAll = value; 
            }
        }

		/// <summary>
		/// Gets and sets the "at" option of the position utility.
		/// </summary>
		[DefaultValue(typeof(Position), "Left, Top")]
		[C1Description("PositionSettings.At")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		public Position At
		{
			get 
			{
                if (_at == null)
                {
                    _at = new Position();
                }
                return _at;
			}
			set
			{
                _at = value;
			}
		}

		/// <summary>
		/// Gets and sets the collision option of the position utility.
		/// </summary>
		[DefaultValue(typeof(Collision), "Flip, Flip")]
		[C1Description("PositionSettings.Collision")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		public Collision Collision
		{
			get
			{
                if (_colision == null)
                {
                    _colision = new Collision();
                }
                return _colision;
			}
			set
			{
                _colision = value;
			}
		}

		/// <summary>
		/// Gets and sets the offset option of the position utility.
		/// </summary>
		[DefaultValue(typeof(Collision), "0, 0")]
		[C1Description("PositionSettings.Offset")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		public Offset Offset
		{
			get 
			{
                if (_offset == null)
                {
                    _offset = new Offset();
                }
                return _offset;
			}
			set
			{
                _offset = value;
			}
		}

		/// <summary>
		/// Returns a string that represents the current object.
		/// </summary>
		/// <returns>
		/// Returns a string that represents the current object.
		/// </returns>
		public override string ToString()
		{
			return string.Empty;
		}

		#region ** implement ICustomOptionType interface
		string ICustomOptionType.SerializeValue()
		{
			if (this == null)
			{
				return "";
			}
			StringBuilder sb = new StringBuilder();
			sb.Append("{");
            if (My.ToString() != "Left, Top" || SerializeAll)
			{
				sb.AppendFormat("my: '{0} {1}', ", My.Left, My.Top);
			}
            if (At.ToString() != "Left, Top" || SerializeAll)
			{
				sb.AppendFormat("at: '{0} {1}', ", At.Left, At.Top);
			}
            if (Offset.ToString() != "0, 0" || SerializeAll)
			{
				sb.AppendFormat("offset: '{0} {1}', ", Offset.Left, Offset.Top);
			}
            if (Collision.ToString() != "Flip, Flip" || SerializeAll)
			{
				sb.AppendFormat("collision: '{0} {1}', ", Collision.Left, Collision.Top);
			}
			if (sb.Length > 2)
			{
				sb.Remove(sb.Length - 2, 2);
			}
			sb.Append("}");

			return sb.ToString().ToLower();
		}

#if !EXTENDER
		void ICustomOptionType.DeSerializeValue(object state)
		{
			Hashtable ht = (Hashtable)state;
			foreach (DictionaryEntry de in ht)
			{
				if (!(de.Value is string))
				{
					continue;
				}

				string[] items = ((string)de.Value).Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
				string[] vals = new string[2];

				if (items.Length == 1)
				{
					vals[0] = items[0];
					vals[1] = items[0];
				}
				else
				{
					vals = items;
				}

				if ((string)de.Key == "my")
				{
                    this.My.Left = (HPosition)Enum.Parse(typeof(HPosition), vals[0], true);
					this.My.Top = (VPosition)Enum.Parse(typeof(VPosition), vals[1], true);
				}
				else if ((string)de.Key == "at")
				{
                    this.At.Left = (HPosition)Enum.Parse(typeof(HPosition), vals[0], true);
                    this.At.Top = (VPosition)Enum.Parse(typeof(VPosition), vals[1], true);
				}
				else if ((string)de.Key == "offset")
				{
                    this.Offset.Left = Convert.ToInt32(vals[0]);
                    this.Offset.Top = Convert.ToInt32(vals[1]);
				}
				else if ((string)de.Key == "collision")
				{
                    this.Collision.Left = (CollisionMode)Enum.Parse(typeof(CollisionMode), vals[0], true);
                    this.Collision.Top = (CollisionMode)Enum.Parse(typeof(CollisionMode), vals[1], true);
				}
			}
		}
#endif

		bool ICustomOptionType.IncludeQuotes
		{
			get { return false; }
		}
		#endregion

		#region ** The IJsonEmptiable interface implementations
		bool IJsonEmptiable.IsEmpty
		{
			get 
			{
				return this.My.ToString() == "Left, Top"
				&& this.At.ToString() == "Left, Top"
				&& this.Collision.ToString() == "Flip, Flip"
				&& this.Offset.ToString() == "0, 0";
			}
		}
		#endregion end of ** The IJsonEmptiable interface implementations.
	}
	#endregion

	#region ** Position class
	/// <summary>
	/// A class of the position's my and at option.
	/// </summary>
	public class Position : Settings
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="Position"/> class.
		/// </summary>
        public Position() 
        {
            this.Left = HPosition.Left;
            this.Top = VPosition.Top;
        }

		/// <summary>
		/// Gets and sets the vertical position.
		/// </summary>
		[DefaultValue(VPosition.Top)]
		[C1Description("Position.Top")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public VPosition Top
		{
			get
			{
				return GetPropertyValue<VPosition>("Top", VPosition.Top);
			}
			set
			{
				SetPropertyValue<VPosition>("Top", value);
			}
		}

		/// <summary>
		/// Gets and sets the horizontal position.
		/// </summary>
		[DefaultValue(HPosition.Left)]
		[C1Description("Position.Left")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public HPosition Left
		{
			get
			{
				return GetPropertyValue<HPosition>("Left", HPosition.Left);
			}
			set
			{
				SetPropertyValue<HPosition>("Left", value);
			}
		}

		/// <summary>
		/// Returns a string that represents the current object.
		/// </summary>
		/// <returns>
		/// Returns a string that represents the current object.
		/// </returns>
		public override string ToString()
		{
			return string.Format("{0}, {1}", this.Left, this.Top);
		}
	}
	#endregion

	#region ** Collision class
	/// <summary>
	/// A class of the position's collision option.
	/// </summary>
	public class Collision : Settings
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="Collision"/> class.
		/// </summary>
        public Collision() 
        {
            this.Left = CollisionMode.Flip;
            this.Top = CollisionMode.Flip;
        }

		/// <summary>
		/// Gets and sets the vertical collision mode.
		/// </summary>
		[DefaultValue(CollisionMode.Flip)]
		[C1Description("Collision.Top")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public CollisionMode Top
		{
			get
			{
				return GetPropertyValue<CollisionMode>("Top", CollisionMode.Flip);
			}
			set
			{
				SetPropertyValue<CollisionMode>("Top", value);
			}
		}

		/// <summary>
		/// Gets and sets the horizontal collision mode.
		/// </summary>
		[DefaultValue(CollisionMode.Flip)]
		[C1Description("Collision.Left")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public CollisionMode Left
		{
			get
			{
				return GetPropertyValue<CollisionMode>("Left", CollisionMode.Flip);
			}
			set
			{
				SetPropertyValue<CollisionMode>("Left", value);
			}
		}

		/// <summary>
		/// Returns a string that represents the current object.
		/// </summary>
		/// <returns>
		/// Returns a string that represents the current object.
		/// </returns>
		public override string ToString()
		{
			return string.Format("{0}, {1}", this.Left, this.Top);
		}
	}
	#endregion

	#region ** Offset class
	/// <summary>
	/// A class of the position's offset option.
	/// </summary>
	public class Offset : Settings
	{

		/// <summary>
		/// Gets and sets the left offset.
		/// </summary>
		[DefaultValue(0)]
		[C1Description("Offset.Left")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public int Left
		{
			get
			{
				return GetPropertyValue<int>("Left", 0);
			}
			set
			{
				SetPropertyValue<int>("Left", value);
			}
		}

		/// <summary>
		/// Gets and sets the top offset.
		/// </summary>
		[DefaultValue(0)]
		[C1Description("Offset.Top")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public int Top
		{
			get
			{
				return GetPropertyValue<int>("Top", 0);
			}
			set
			{
				SetPropertyValue<int>("Top", value);
			}
		}

		/// <summary>
		/// Returns a string that represents the current object.
		/// </summary>
		/// <returns>
		/// Returns a string that represents the current object.
		/// </returns>
		public override string ToString()
		{
			return string.Format("{0}, {1}", this.Left, this.Top);
		}
	}
	#endregion end of ** Offset class
}
