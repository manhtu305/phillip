﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1FlipCard
#else
namespace C1.Web.Wijmo.Controls.C1FlipCard
#endif
{
    /// <summary>
    /// Side of the card.
    /// </summary>
    public enum FlipCardSide
    {
        /// <summary>
        /// Front side.
        /// </summary>
        Front,
        /// <summary>
        /// Back side.
        /// </summary>
        Back
    }

    /// <summary>
    /// Card flipping directions.
    /// </summary>
    public enum FlipDirection
    {
        /// <summary>
        /// Flip horizontally.
        /// </summary>
        Horizontal,
        /// <summary>
        /// Flip vertically.
        /// </summary>
        Vertical
    }

    /// <summary>
    /// Flipping trigger method.
    /// </summary>
    public enum TriggerEvent
    {
        /// <summary>
        /// Flip on click.
        /// </summary>
        Click,
        /// <summary>
        /// Flip on hover.
        /// </summary>
        MouseEnter
    }
}
