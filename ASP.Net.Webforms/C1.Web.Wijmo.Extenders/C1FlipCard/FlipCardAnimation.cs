﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;

namespace C1.Web.Wijmo.Extenders.C1FlipCard
#else
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1FlipCard
#endif
{
    /// <summary>
    /// The flip animation options, like direction, duration etc.
    /// </summary>
    [ParseChildren(true)]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class FlipCardAnimation : Settings
    {
        /// <summary>
        /// Get or set whether to disable the animation.
        /// </summary>
        [C1Description("FlipCardAnimation.Disabled")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(false)]
        [NotifyParentProperty(true)]
        public bool Disabled
        {
            get
            {
                return GetPropertyValue("Disabled", false);
            }
            set
            {
                SetPropertyValue("Disabled", value);
            }
        }

        /// <summary>
        /// Get or set the animation type. Default is "flip", users can set it to other jQuery UI effects.
        /// </summary>
        [C1Description("FlipCardAnimation.Type")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue("flip")]
        [NotifyParentProperty(true)]
        public string Type
        {
            get
            {
                return GetPropertyValue("Type", "flip");
            }
            set
            {
                SetPropertyValue("Type", value);
            }
        }

        /// <summary>
        /// Get or set the animation duration, default value is 500ms.
        /// </summary>
        [C1Description("FlipCardAnimation.Duration")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(500)]
        [NotifyParentProperty(true)]
        public int Duration
        {
            get
            {
                return GetPropertyValue("Duration", 500);
            }
            set
            {
                SetPropertyValue("Duration", value);
            }
        }

        /// <summary>
        /// Get or set the flip animation direction, default value is "horizontal", user can set it to "vertical".
        /// </summary>
        [C1Description("FlipCardAnimation.Direction")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
        [DefaultValue(FlipDirection.Horizontal)]
        [NotifyParentProperty(true)]
        public FlipDirection Direction
        {
            get
            {
                return GetPropertyValue("Direction", FlipDirection.Horizontal);
            }
            set
            {
                SetPropertyValue("Direction", value);
            }
        }
    }
}
