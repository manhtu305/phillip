﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;

[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1FlipCard", "wijmo")]

namespace C1.Web.Wijmo.Extenders.C1FlipCard
#else
using C1.Web.Wijmo.Controls.Localization;

[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1FlipCard", "wijmo")]

namespace C1.Web.Wijmo.Controls.C1FlipCard
#endif
{
    [WidgetDependencies(
        typeof(WijFlipCard)
#if !EXTENDER
, "extensions.c1flipcard.js",
ResourcesConst.C1WRAPPER_OPEN
#endif
)]
#if EXTENDER
    public partial class C1FlipCardExtender
#else
    public partial class C1FlipCard
#endif
    {
        #region Fields

        #endregion

        #region Options

        /// <summary>
        /// A value that indicates the event used to flip between two panels.
        /// </summary>
        /// <remarks>
        /// The value can be 'Click', 'MouseEnter'
        /// </remarks>
        [C1Description("C1FlipCard.TriggerEvent")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DefaultValue(TriggerEvent.Click)]
        public TriggerEvent TriggerEvent
        {
            get
            {
                return GetPropertyValue("TriggerEvent", TriggerEvent.Click);
            }
            set
            {
                SetPropertyValue("TriggerEvent", value);
            }
        }

        /// <summary>
        /// The flip animation options, define direction, duration etc.
        /// </summary>
        [C1Description("C1FlipCard.Animation")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public FlipCardAnimation Animation
        {
            get
            {
                return GetPropertyValue("Animation", new FlipCardAnimation());
            }
            set
            {
                SetPropertyValue("Animation", value);
            }
        }

        #endregion

        #region Client events

#if !GRAPECITY

        /// <summary>
        /// This event is triggered when flip animation starts.
        /// </summary>
		[Obsolete("Please use OnClientFlipping instead of OnFlipping.")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string OnFlipping
		{
			get
			{
				return OnClientFlipping;
			}
			set
			{
				OnClientFlipping = value;
			}
		}

        /// <summary>
        /// This event is triggered when flip animation ends.
        /// </summary>
		[Obsolete("Please use OnClientFlipped instead of OnFlipped.")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string OnFlipped
		{
			get
			{
				return OnClientFlipped;
			}
			set
			{
				OnClientFlipped = value;
			}
		}

#endif

		/// <summary>
		/// This event is triggered when flip animation starts.
		/// </summary>
		[C1Description("C1FlipCard.OnClientFlipping")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e, data")]
		[DefaultValue("")]
		public string OnClientFlipping
		{
			get
			{
				return GetPropertyValue("OnClientFlipping", string.Empty);
			}
			set
			{
				SetPropertyValue("OnClientFlipping", value);
			}
		}

		/// <summary>
		/// This event is triggered when flip animation ends.
		/// </summary>
		[C1Description("C1FlipCard.OnClientFlipped")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e, data")]
		[DefaultValue("")]
		public string OnClientFlipped
		{
			get
			{
				return GetPropertyValue("OnClientFlipped", string.Empty);
			}
			set
			{
				SetPropertyValue("OnClientFlipped", value);
			}
		}

        #endregion
    }
}
