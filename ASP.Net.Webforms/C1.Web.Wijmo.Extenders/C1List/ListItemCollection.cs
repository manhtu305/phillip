﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Extenders.Base.Collections;
using System.Diagnostics;

namespace C1.Web.Wijmo.Extenders.List
{
	public class ListItemCollection : C1ObservableItemCollection<List, ListItem>
	{
		internal ulong Version = 1;

		/// <summary>
		/// Initializes a new instance of the ListItemCollection class.
		/// </summary>
		/// <param name="owner">Parent List class instance.</param>
		public ListItemCollection(List owner)
			: base(owner)
		{
			Debug.Assert(owner != null, "ListItemCollection - owner is null");
		}

		/// <summary>
		/// Removes all items from collection.
		/// </summary>
		public new void Clear()
		{
			Version++;
			base.ClearItems();

			this.SetDirty();
		}

		/// <summary>
		/// Adds a new list item to the end of the list.
		/// </summary>
		/// <param name="child"></param>
		public new void Add(ListItem child)
		{
			if (child != null)
			{
				this.Insert(this.Count, child);
				this.SetDirty();
			}
		}

		/// <summary>
		/// Insert a <see cref="AccordionPane"/> item into a specific position in the collection.
		/// </summary>
		/// <param name="index">Item position. Value should be greater or equal to 0</param>
		/// <param name="child">The new <see cref="ListItem"/> item.</param>
		public new void Insert(int index, ListItem child)
		{
			Version++;
			child.SetParent(Owner);
			if (this.Count == 0)
				this.InsertItem(0, child);
			else
				this.InsertItem(index, child);

			this.SetDirty();
		}

		/// <summary>
		/// Removes the specified item from the list.
		/// </summary>
		/// <param name="child">The <see cref="ListItem"/> item to be removed.</param>
		public new void Remove(ListItem child)
		{
			RemoveAt(IndexOf(child));
		}

		/// <summary>
		/// Removes an item from the list using its index.
		/// </summary>
		/// <param name="index">The index of the <see cref="ListItem"/> item to be removed.</param>
		public new void RemoveAt(int index)
		{
			Version++;
			this.RemoveItem(index);
			this.SetDirty();
		}

		private void SetDirty()
		{
			if (Owner != null)
				Owner.SetDirty();
		}
	}
}
