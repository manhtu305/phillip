﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Extenders.Converters;
using C1.Web.Wijmo.Extenders.Localization;

[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1List", "wijmo")]

namespace C1.Web.Wijmo.Extenders.C1List
{
	/// <summary>
	/// List for the expander widget.
	/// </summary>
	[TargetControlType(typeof(Panel))]
	[ToolboxItem(true)]
	[WidgetDependencies(
        "base.globalize.min.js",
        "wijmo.jquery.wijmo.wijsuperpanel.js", 
        "wijmo.jquery.wijmo.wijlist.js",
		ResourcesConst.WIJMO_OPEN_CSS)]
	[ToolboxBitmap(typeof(C1ListExtender), "List.png")]
	[LicenseProviderAttribute()]
	public class C1ListExtender : WidgetExtenderControlBase
	{
		#region Field
		private List<ListItem> _listItems;
		private bool _productLicensed = false;
		#endregion

				/// <summary>
		/// Initializes a new instance of the <see cref="C1ExpanderExtender"/> class.
		/// </summary>
		public C1ListExtender()
			: base()
		{
			VerifyLicense();
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1ListExtender), this, false);
#if GRAPECITY
            _productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		#region Properties

		/// <summary>
		/// A value indicates the selection mode of wijlist.
		/// </summary>
		/// <remarks>
		/// Options are "single" and "multiple". This option should not be set 
		/// again after initialization.
		/// </remarks>
		//[Category("Options")]
		[C1Category("Category.Behavior")]
		[DefaultValue(SelectionMode.Single)]
		[C1Description("C1List.SelectionMode","A value indicates the selection mode of wijlist.")]
		[WidgetOption]
		public SelectionMode SelectionMode
		{
			get
			{
				return this.GetPropertyValue("SelectionMode", SelectionMode.Single);
			}
			set
			{
				this.SetPropertyValue("SelectionMode", value);
			}
		}

		/// <summary>
		///  A value that specifies the index of the item to select.
		/// </summary>
		[DefaultValue(-1)]
		[C1Description("C1List.SelectedIndex", "A value that specifies the index of the item to select.")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		public int SelectedIndex
		{
			get 
			{
				return this.GetPropertyValue<int>("SelectedIndex", -1);
			}
			set
			{
				this.SetPropertyValue<int>("SelectedIndex", value);
			}
		}

		/// <summary>
		/// A value determines whether to auto-size wijlist.
		/// </summary>
		//[Category("Options")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[C1Description("C1List.AutoSize", "A value determines whether to auto-size wijlist.")]
		[WidgetOption]
		public bool AutoSize
		{
			get
			{
				return this.GetPropertyValue("AutoSize", false);
			}
			set
			{
				this.SetPropertyValue("AutoSize", value);
			}
		}

		/// <summary>
		/// A value specifies the max items count to display if 
		///autoSize is set to true.
		/// </summary>
		//[Category("Options")]
		[C1Category("Category.Behavior")]
		[DefaultValue(5)]
		[C1Description("C1List.MaxItemsCount", "A value specifies the max items count to display if autoSize is set to true.")]
		[WidgetOption]
		public int MaxItemsCount
		{
			get
			{
				return this.GetPropertyValue("MaxItemsCount", 5);
			}
			set
			{
				this.SetPropertyValue("MaxItemsCount", value);
			}
		}

		/// <summary>
		/// A value determines whether to add ui-state-hover class to list
		/// item when mouse enters.
		/// </summary>
		//[Category("Options")]
		[C1Category("Category.Appearance")]
		[DefaultValue(true)]
		[C1Description("C1List.AddHoverItemClass", "A value determines whether to add ui-state-hover class to list item when mouse enters.")]
		[WidgetOption]
		public bool AddHoverItemClass
		{
			get
			{
				return this.GetPropertyValue("AddHoverItemClass", true);
			}
			set
			{
				this.SetPropertyValue("AddHoverItemClass", value);
			}
		}

		/// <summary>
		/// A value determines whether to keep item highlight when mouse 
		/// is leaving list. 
		/// </summary>
		//[Category("Options")]
		[C1Category("Category.Appearance")]
		[DefaultValue(false)]
		[C1Description("C1List.KeepHightlightOnMouseLeave", "A value determines whether to keep item highlight when mouse is leaving list. ")]
		[WidgetOption]
		public bool KeepHightlightOnMouseLeave
		{
			get
			{
				return this.GetPropertyValue("KeepHightlightOnMouseLeave", false);
			}
			set
			{
				this.SetPropertyValue("KeepHightlightOnMouseLeave", value);
			}
		}

		/// <summary>
		/// An array that specifies the listItem collections of wijlist.
		/// </summary>
		//[Category("Options")]
		[C1Category("Category.Data")]
		[C1Description("C1List.ListItems", "An array that specifies the listItem collections of wijlist.")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
		public List<ListItem> ListItems
		{
			get
			{
				if (_listItems == null)
				{
					_listItems = new List<ListItem>();
				}
				return _listItems;
			}
		}

		/// <summary>
		/// Serialize ListItems with the specified condition.
		/// </summary>
		/// <returns></returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeListItems()
		{
			if (this.ListItems == null)
			{
				return false;
			}
			else
			{
				return this.ListItems.Count > 0;
			}
		}
		#endregion

		#region Client Events

		/// <summary>
		/// Select event handler of wijlist. A function will be called 
		/// when any item in the list is selected.
		/// </summary>
		[Category("Client Events")]
		[DefaultValue("")]
		[C1Description("C1List.OnClientSelected", "A function will be called when any item in the list is selected.")]
		[WidgetEvent]
		public string OnClientSelected
		{
			get
			{
				return GetPropertyValue("OnClientSelected", "");
			}
			set
			{
				SetPropertyValue("OnClientSelected", value);
			}
		}

		/// <summary>
		/// A function called before an item is focused.
		/// </summary>
		[Category("Client Events")]
		[DefaultValue("")]
		[C1Description("C1List.OnClientFocusing","A function called before an item is focused.")]
		[WidgetEvent]
		public string OnClientFocusing
		{
			get
			{
				return GetPropertyValue("OnClientFocusing", "");
			}
			set
			{
				SetPropertyValue("OnClientFocusing", value);
			}
		}

		/// <summary>
		/// A function called after an item is focused.
		/// </summary>
		[Category("Client Events")]
		[DefaultValue("")]
		[C1Description("C1List.OnClientFocus", "A function called after an item is focused.")]
		[WidgetEvent]
		public string OnClientFocus
		{
			get
			{
				return GetPropertyValue("OnClientFocus", "");
			}
			set
			{
				SetPropertyValue("OnClientFocus", value);
			}
		}

		/// <summary>
		/// A function called when an item loses focus.
		/// </summary>
		[Category("Client Events")]
		[DefaultValue("")]
		[C1Description("C1List.OnClientBlur", "A function called when an item loses focus.")]
		[WidgetEvent]
		public string OnClientBlur
		{
			get
			{
				return GetPropertyValue("OnClientBlur", "");
			}
			set
			{
				SetPropertyValue("OnClientBlur", value);
			}
		}

		/// <summary>
		/// A function called before an item is rendered.
		/// </summary>
		[Category("Client Events")]
		[DefaultValue("")]
		[C1Description("C1List.OnClientItemrendering", "A function called before an item is rendered.")]
		[WidgetEvent]
		public string OnClientItemrendering
		{
			get
			{
				return GetPropertyValue("OnClientItemrendering", "");
			}
			set
			{
				SetPropertyValue("OnClientItemrendering", value);
			}
		}

		/// <summary>
		/// A function called after a item is rendered.
		/// </summary>
		[Category("Client Events")]
		[DefaultValue("")]
		[C1Description("C1List.OnClientItemrendered", "A function called after a item is rendered.")]
		[WidgetEvent]
		public string OnClientItemrendered
		{
			get
			{
				return GetPropertyValue("OnClientItemrendered", "");
			}
			set
			{
				SetPropertyValue("OnClientItemrendered", value);
			}
		}

		/// <summary>
		/// A function called after list is rendered.
		/// </summary>
		[Category("Client Events")]
		[DefaultValue("")]
		[C1Description("C1List.OnClientItemrendered", "A function called after list is rendered.")]
		[WidgetEvent]
		public string OnClientListrendered
		{
			get
			{
				return GetPropertyValue("OnClientListrendered", "");
			}
			set
			{
				SetPropertyValue("OnClientListrendered", value);
			}
		}

		#endregion end of Client Events.

	}
}
