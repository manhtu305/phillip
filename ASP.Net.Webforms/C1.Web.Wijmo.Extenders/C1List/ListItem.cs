﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using C1.Web.Wijmo.Extenders.Localization;

namespace C1.Web.Wijmo.Extenders.C1List
{
	/// <summary>
	///  Defines the item of list.
	/// </summary>
	public class ListItem
	{

		#region ** fields

		//internal List _list;

		#endregion

		#region ** properties

		/// <summary>
		/// The ListItem Constructor.
		/// </summary>
		public ListItem()
		{
			this.Label = "";
			this.Value = "";
			this.Selected = false;
		}

		/// <summary>
		/// The Label Property of ListItem for Wijmo List.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1List.ListItem.Label", "The Label Property of ListItem for Wijmo List.")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string Label
		{
			get;
			set;
		}

		/// <summary>
		/// The Value Property of ListItem for Wijmo List.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1List.ListItem.Value", "The Value Property of ListItem for Wijmo List.")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string Value
		{
			get;
			set;
		}

		/// <summary>
		/// A value that indicates whether this item is selected.
		/// </summary>
		[DefaultValue(false)]
		[C1Description("C1List.ListItem.Selected", "A value that indicates whether this item is selected.")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public bool Selected
		{
			get;
			set;
		}

		#endregion

		#region ** internal and private code

		//internal void SetParent(List Owner)
		//{
		//    _list = Owner;
		//}

		#endregion
	}
}
