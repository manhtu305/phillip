﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Extenders.Localization;
using C1.Web.Wijmo.Extenders.Base;

namespace C1.Web.Wijmo.Extenders.List
{
	[ToolboxData("<{0}:List runat=server></{0}:List>")]
	public class List : TargetDataBoundControlBase
	{
		#region ** fields

		private WidgetExtenderControlBase _extender;
		private ListItemCollection _items;

		#endregion

		#region ** properties

		/// <summary>
		/// Collection of items in the list.
		/// </summary>
		[C1Description("List.ListItems")]
		[C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[MergableProperty(false)]
		[Layout(LayoutType.Appearance)]
		public ListItemCollection ListItems
		{
			get
			{
				if (_items == null)
					_items = new ListItemCollection(this);
				return _items;
			}
		} 

		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return  HtmlTextWriterTag.Div;
			}
		}

		#endregion

		#region ** private and internal implementation

		internal void SetDirty()
		{
			this.ChildControlsCreated = false;
		}

		#endregion

		#region ** Data binding / Create child controls

		protected override void CreateChildControls()
		{
			base.CreateChildControls();
			if (this._extender != null)
			{
				ListExtender listext = this._extender as ListExtender;
				for (int i = 0; i < this.ListItems.Count; i++)
				{
					listext.ListItems.Add(this.ListItems[i]);
				}
			}
		}

		#endregion

		#region ** WidgetExtenderControlBase abstract implementation

		/// <summary>
		/// Gets the wrapped extender control.
		/// </summary>
		/// <value></value>
		protected override WidgetExtenderControlBase Extender
		{
			get
			{
				if (_extender == null)
				{
					_extender = new ListExtender();
					_extender.ID = ID + "_ListExtender";
					_extender.TargetControlID = ID;
				}
				return _extender;
			}
		}

		#endregion
	}
}
