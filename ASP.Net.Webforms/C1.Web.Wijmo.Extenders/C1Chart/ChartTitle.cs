﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	/// <summary>
	/// Represents the ChartTitle class which contains all settings for the chart title.
	/// </summary>
	public partial class ChartTitle : Settings, IJsonEmptiable
	{

		/// <summary>
		/// Initializes a new instance of the ChartTitle class.
		/// </summary>
		public ChartTitle()
		{
			this.TitleStyle = new ChartStyle();
			this.TextStyle = new ChartStyle();
		}

		#region options

		/// <summary>
		/// A value that indicates the text of the title.
		/// </summary>
		[C1Description("C1Chart.ChartTitle.Text")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string Text
		{
			get
			{
				return this.GetPropertyValue<string>("Text", "");
			}
			set
			{
				this.SetPropertyValue<string>("Text", value);
			}
		}

		/// <summary>
		/// A value that indicates the style of the title.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use the TitleStyle property instead.")]
		public ChartStyle Style
		{
			get
			{
				return this.TitleStyle;
			}
			set
			{
				this.TitleStyle = value;
			}
		}

		/// <summary>
		/// A value that indicates the style of the title.
		/// </summary>
		[C1Description("C1Chart.ChartTitle.Style")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[WidgetOptionName("style")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle TitleStyle { get; set; }

		/// <summary>
		/// A value that indicates the style of the title text.
		/// </summary>
		[C1Description("C1Chart.ChartTitle.TextStyle")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle TextStyle { get; set; }

		/// <summary>
		/// A value that indicates the compass of the title.
		/// </summary>
		[C1Description("C1Chart.ChartTitle.Compass")]
		[NotifyParentProperty(true)]
		// fixed bug 20367 by dail 2012-3-9
		//[DefaultValue(ChartCompass.North)]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public ChartCompass Compass
		{
			get
			{
				return this.GetPropertyValue<ChartCompass>("Compass", ChartCompass.North);
			}
			set
			{
				this.SetPropertyValue<ChartCompass>("Compass", value);
			}
		}

		/// <summary>
		/// A value that indicates the visibility of the title.
		/// </summary>
		[C1Description("C1Chart.ChartTitle.Visible")]
		[NotifyParentProperty(true)]
		[DefaultValue(true)]
		[WidgetOption]
		[Json(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Visible
		{
			get
			{
				return this.GetPropertyValue<bool>("Visible", true);
			}
			set
			{
				this.SetPropertyValue<bool>("Visible", value);
			}
		}

		#endregion end of options

		#region Serialize

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal bool ShouldSerialize()
		{
			if (!Visible)
				return true;
			// fixed bug 20367 by dail 2012-3-9
			//if (Orientation != ChartOrientation.Horizontal)
			//    return true;
			//if (Compass != ChartCompass.North)
			//    return true;
			if (TextStyle.ShouldSerialize())
				return true;
			if (TitleStyle.ShouldSerialize())
				return true;
			if (!String.IsNullOrEmpty(Text))
				return true;
			return false;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeStyle()
		{
			return false;
		}
		#endregion
	}
}