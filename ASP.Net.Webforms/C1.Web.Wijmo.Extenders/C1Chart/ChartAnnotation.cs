﻿using System;
using System.Drawing;
using System.ComponentModel;
using System.Web.UI;
using SystemPoint = System.Drawing.Point;
using System.Collections.Generic;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
using PointF = C1.Web.Wijmo.Extenders.PointF;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
using PointF = C1.Web.Wijmo.Controls.PointF;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	/// <summary>
	/// Specifies the attachment of the annotation.
	/// </summary>
	public enum AnnotationAttachment
	{
		/// <summary>
		/// Coordinates of the annotation point are defined by the data series index and 
		/// the data point index.
		/// </summary>
		DataIndex,

		/// <summary>
		/// Annotation point is specified in data coordinates.
		/// </summary>
		DataCoordinate,

		/// <summary>
		/// Annotation point is specified as a relative position inside the control where
		/// (0,0) is the top left corner and (1,1) is the bottom right corner.
		/// </summary>
		Relative,

		/// <summary>
		/// The annotation point is specified in control's pixel coordinates.
		/// </summary>
		Absolute
	}

	/// <summary>
	/// Specifies the position of the annotation.
	/// </summary>
	[Flags]
	public enum AnnotationPosition
	{
		/// <summary>
		/// The annotation appears at the Center of the target point.
		/// </summary>
		Center = 1,
		
		/// <summary>
		/// The annotation appears at the Top of the target point.
		/// </summary>
		Top = 2,
		
		/// <summary>
		/// The annotation appears at the Bottom of the target point.
		/// </summary>
		Bottom = 4,
		
		/// <summary>
		/// The annotation appears at the Left of the target point.
		/// </summary>
		Left = 8,
		
		/// <summary>
		/// The annotation appears at the Right of the target point.
		/// </summary>
		Right = 16
	}

	/// <summary>
	/// Define a class that represents a data point (with x and y coordinates).
	/// </summary>
	public class DataPoint : Settings, IJsonEmptiable
	{
		#region Properties
		/// <summary>
		/// A value indicates x coordinate value.
		/// </summary>
		[C1Description("C1Chart.ChartAnnotation.DataPointX")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public ChartYData X { get; set; }

		/// <summary>
		/// A value indicates y coordinate value.
		/// </summary>
		[C1Description("C1Chart.ChartAnnotation.DataPointY")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public ChartYData Y { get; set; }
		#endregion Properties

		#region Ctors
		public DataPoint()
		{
			X = new ChartYData();
			Y = new ChartYData();
		}
		#endregion Ctors

		#region Serialize
		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		/// <summary>
		/// Returns a <see cref="System.Boolean"/> value that indicates whether the chart annotation 
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a <see cref="System.Boolean"/> value to determine whether this annotation should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal virtual bool ShouldSerialize()
		{
			if (!X.IsNull)
			{
				return true;
			}

			if (!Y.IsNull)
			{
				return true;
			}

			return false;
		}
		#endregion Serialize
	}

	/// <summary>
	/// Define the basic class of the annotations.
	/// </summary>
	public abstract class AnnotationBase : Settings, IJsonEmptiable
	{
		#region Properties
		/// <summary>
		/// A value that indicates the attachment of the annotation.
		/// </summary>
		[C1Description("C1Chart.ChartAnnotation.Attachment")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(AnnotationAttachment.Absolute)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public AnnotationAttachment Attachment { get; set; }

		/// <summary>
		/// A value that indicates the visibility of the chart annotation.
		/// </summary>
		[C1Description("C1Chart.ChartAnnotation.IsVisible")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool IsVisible { get; set; }

		/// <summary>
		/// A <see cref="System.Point"/> value that indicates the offset of the annotation.
		/// </summary>
		[C1Description("C1Chart.ChartAnnotation.Offset")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public Point Offset { get; set; }

		/// <summary>
		/// A value that indicates the point of the annotation.
		/// </summary>
		/// <remarks>
		/// The coordinates of points depends on the Attachment property.
		/// </remarks>
		[C1Description("C1Chart.ChartAnnotation.Point")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public DataPoint Point { get; set; }

		/// <summary>
		/// A value that indicates the data point index of the annotation.
		/// </summary>
		/// <remarks>
		/// Applies only when the <b>Attachment</b> property is set to AnnotationAttachment.DataIndex.
		/// </remarks>
		[C1Category("Category.Behavior")]
		[C1Description("C1Chart.ChartAnnotation.PointIndex")]
		[NotifyParentProperty(true)]
		[DefaultValue(0)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int PointIndex { get; set; }

		/// <summary>
		/// A value that indicates the position of the annotation.
		/// </summary>
		[C1Description("C1Chart.ChartAnnotation.Position")]
		[DefaultValue(AnnotationPosition.Center)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public AnnotationPosition Position { get; set; }

		/// <summary>
		/// A value that indicates the data series index of the annotation.
		/// </summary>
		/// <remarks>
		/// Applies only when the <b>Attachment</b> property is set to AnnotationAttachment.DataIndex.
		/// </remarks>
		[C1Category("Category.Behavior")]
		[C1Description("C1Chart.ChartAnnotation.SeriesIndex")]
		[NotifyParentProperty(true)]
		[DefaultValue(0)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int SeriesIndex { get; set; }

		/// <summary>
		/// A value that indicates the style of the annotation object.
		/// </summary>
		[C1Description("C1Chart.ChartAnnotation.AnnotationStyle")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[WidgetOptionName("style")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public virtual ChartStyle AnnotationStyle { get; set; }

		/// <summary>
		/// A <see cref="System.String"/> value that indicates the tooltip content of the annotation.
		/// </summary>
		/// <remarks>
		/// When the content is empty, the tooltip will not be shown.
		/// </remarks>
		[C1Description("C1Chart.ChartAnnotation.Tooltip")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[C1Category("Category.Data")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public string Tooltip { get; set; }

		/// <summary>
		/// A <see cref="System.String"/> value that indicates the annotation type.
		/// </summary>
		/// <remarks>
		/// It is readonly.
		/// </remarks>
		[WidgetOption]
		[DefaultValue("")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public abstract string Type { get; }
		#endregion Properties

		#region Ctor
		/// <summary>
		/// Initializes a new instance of the AnnotationBase class.
		/// </summary>
		public AnnotationBase()
		{
			this.Attachment = AnnotationAttachment.Absolute;
			this.Position = AnnotationPosition.Center;
			this.Offset = SystemPoint.Empty;
			this.AnnotationStyle = new ChartStyle();
			this.Point = new DataPoint();
			this.IsVisible = true;
		}
		#endregion Ctor

		#region Serialize
		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		/// <summary>
		/// Returns a <see cref="System.Boolean"/> value that indicates whether the chart annotation 
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a <see cref="System.Boolean"/> value to determine whether this annotation should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal virtual bool ShouldSerialize()
		{
			if (Attachment != AnnotationAttachment.Absolute)
				return true;
			if (!IsVisible)
				return true;
			if (!Offset.Equals(SystemPoint.Empty))
				return true;
			if (!((IJsonEmptiable)Point).IsEmpty)
				return true;
			if (Position != AnnotationPosition.Center)
				return true;
			if (PointIndex != 0)
				return true;
			if (SeriesIndex != 0)
				return true;
			if (!string.IsNullOrEmpty(Tooltip))
				return true;
			if (AnnotationStyle.ShouldSerialize())
				return true;
			if (!string.IsNullOrEmpty(Type))
				return true;

			return false;
		}
		#endregion Serialize

	}

	/// <summary>
	/// Represents a text annotation
	/// </summary>
	public class TextAnnotation : AnnotationBase
	{
		#region Properties
		/// <summary>
		/// A <see cref="System.String"/> value that indicates the text of the annotation.
		/// </summary>
		[C1Description("C1Chart.Text.Text")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public string Text { get; set; }

		public override string Type
		{
			get
			{
				return "text";
			}
		}
		#endregion Properties

		#region Serialize
		/// <summary>
		/// Returns a <see cref="System.Boolean"/> value that indicates whether the chart annotation 
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a <see cref="System.Boolean"/> value to determine whether this annotation should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal override bool ShouldSerialize()
		{
			if (!string.IsNullOrEmpty(Text))
				return true;

			return base.ShouldSerialize();
		}
		#endregion Serialize
	}

	/// <summary>
	/// Represents a base class of shape annotations.
	/// </summary>
	public abstract class ShapeAnnotation : AnnotationBase
	{
		#region Properties
		/// <summary>
		/// A <see cref="System.String"/> value that indicates the text of the annotation.
		/// </summary>
		[C1Description("C1Chart.Shape.Content")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public string Content { get; set; }
		#endregion Properties

		#region Serialize
		/// <summary>
		/// Returns a <see cref="System.Boolean"/> value that indicates whether the chart annotation 
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a <see cref="System.Boolean"/> value to determine whether this annotation should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal override bool ShouldSerialize()
		{
			if (!string.IsNullOrEmpty(Content))
				return true;

			return base.ShouldSerialize();
		}
		#endregion Serialize
	}

	/// <summary>
	/// Represents a circle annotation.
	/// </summary>
	public class CircleAnnotation : ShapeAnnotation
	{
		#region Ctor
		/// <summary>
		/// Initializes a new instance of the CircleAnnotation class.
		/// </summary>
		public CircleAnnotation()
		{
			this.Radius = 25;
		}
		#endregion Ctor

		#region Properties
		/// <summary>
		/// A value specifies the radius of the circle.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.Circle.Radius")]
		[NotifyParentProperty(true)]
		[DefaultValue(25)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Radius { get; set; }

		public override string Type
		{
			get
			{
				return "circle";
			}
		}
		#endregion Properties

		#region Serialize
		/// <summary>
		/// Returns a <see cref="System.Boolean"/> value that indicates whether the chart annotation 
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a <see cref="System.Boolean"/> value to determine whether this annotation should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal override bool ShouldSerialize()
		{
			if (Radius != 25)
				return true;

			return base.ShouldSerialize();
		}
		#endregion Serialize
	}

	/// <summary>
	/// Represents a square annotation.
	/// </summary>
	public class SquareAnnotation : ShapeAnnotation
	{
		#region Ctor
		/// <summary>
		/// Initializes a new instance of the SquareAnnotation class.
		/// </summary>
		public SquareAnnotation()
		{
			this.Width = 50;
		}
		#endregion Ctor

		#region Properties
		/// <summary>
		/// A value specifies the width of the square.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.Square.Width")]
		[NotifyParentProperty(true)]
		[DefaultValue(50)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Width { get; set; }

		public override string Type
		{
			get
			{
				return "square";
			}
		}
		#endregion Properties

		#region Serialize
		/// <summary>
		/// Returns a <see cref="System.Boolean"/> value that indicates whether the chart annotation 
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a <see cref="System.Boolean"/> value to determine whether this annotation should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal override bool ShouldSerialize()
		{
			if (Width != 50)
				return true;

			return base.ShouldSerialize();
		}
		#endregion Serialize
	}

	/// <summary>
	/// Represents a rectangle annotation.
	/// </summary>
	public class RectAnnotation : ShapeAnnotation
	{
		#region Ctor
		/// <summary>
		/// Initializes a new instance of the RectAnnotation class.
		/// </summary>
		public RectAnnotation()
		{
			this.Width = 60;
			this.Height = 30;
			this.R = 2;
		}
		#endregion Ctor

		#region Properties
		/// <summary>
		/// A value specifies the width of the rectangle.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.Rect.Width")]
		[NotifyParentProperty(true)]
		[DefaultValue(60)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Width { get; set; }

		/// <summary>
		/// A value specifies the height of the rectangle.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.Rect.Height")]
		[NotifyParentProperty(true)]
		[DefaultValue(30)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Height { get; set; }

		/// <summary>
		/// A value specifies the radius for rounded corners.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.Rect.R")]
		[NotifyParentProperty(true)]
		[DefaultValue(2)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int R { get; set; }

		public override string Type
		{
			get
			{
				return "rect";
			}
		}
		#endregion Properties

		#region Serialize
		/// <summary>
		/// Returns a <see cref="System.Boolean"/> value that indicates whether the chart annotation 
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a <see cref="System.Boolean"/> value to determine whether this annotation should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal override bool ShouldSerialize()
		{
			if (Width != 60 || Height != 30 || R != 2)
				return true;

			return base.ShouldSerialize();
		}
		#endregion Serialize
	}

	/// <summary>
	/// Represents a ellipse annotation.
	/// </summary>
	public class EllipseAnnotation : ShapeAnnotation
	{
		#region Ctor
		/// <summary>
		/// Initializes a new instance of the EllipseAnnotation class.
		/// </summary>
		public EllipseAnnotation()
		{
			this.Width = 60;
			this.Height = 30;
		}
		#endregion Ctor

		#region Properties
		/// <summary>
		/// A value specifies the width of the ellipse.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.Ellipse.Width")]
		[NotifyParentProperty(true)]
		[DefaultValue(60)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Width { get; set; }

		/// <summary>
		/// A value specifies the height of the ellipse.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.Ellipse.Height")]
		[NotifyParentProperty(true)]
		[DefaultValue(30)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Height { get; set; }

		public override string Type
		{
			get
			{
				return "ellipse";
			}
		}
		#endregion Properties

		#region Serialize
		/// <summary>
		/// Returns a <see cref="System.Boolean"/> value that indicates whether the chart annotation 
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a <see cref="System.Boolean"/> value to determine whether this annotation should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal override bool ShouldSerialize()
		{
			if (Width != 60 || Height != 30)
				return true;

			return base.ShouldSerialize();
		}
		#endregion Serialize
	}

	/// <summary>
	/// Represents a line annotation.
	/// </summary>
	public class LineAnnotation : ShapeAnnotation
	{
		#region Properties
		/// <summary>
		/// A value specifies the start point of the line.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.Line.Start")]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public Point Start { get; set; }

		/// <summary>
		/// A value specifies the end point of the line.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.Line.End")]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public Point End { get; set; }

		public override string Type
		{
			get
			{
				return "line";
			}
		}
		#endregion Properties

		#region Serialize
		/// <summary>
		/// Returns a <see cref="System.Boolean"/> value that indicates whether the chart annotation 
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a <see cref="System.Boolean"/> value to determine whether this annotation should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal override bool ShouldSerialize()
		{
			if (!Start.Equals(SystemPoint.Empty) || !End.Equals(SystemPoint.Empty))
				return true;

			return base.ShouldSerialize();
		}
		#endregion Serialize
	}

	/// <summary>
	/// Represents a polygon annotation.
	/// </summary>
	public class PolygonAnnotation : ShapeAnnotation
	{
		#region Properties
		private List<PointF> _points;

		/// <summary>
		/// A value specifies the points of the polygon.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.Polygon.Points")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[CollectionItemType(typeof(PointF))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public List<PointF> Points
		{
			get
			{
				if (_points == null)
				{
					_points = new List<PointF>();
				}
				return _points;
			}
		}

		public override string Type
		{
			get
			{
				return "polygon";
			}
		}
		#endregion Properties

		#region Serialize
		/// <summary>
		/// Returns a <see cref="System.Boolean"/> value that indicates whether the chart annotation 
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a <see cref="System.Boolean"/> value to determine whether this annotation should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal override bool ShouldSerialize()
		{
			if (Points != null && Points.Count > 0)
				return true;

			return base.ShouldSerialize();
		}
		#endregion Serialize
	}

	/// <summary>
	/// Represents a image annotation.
	/// </summary>
	public class ImageAnnotation : ShapeAnnotation
	{
		#region Ctor
		/// <summary>
		/// Initializes a new instance of the ImageAnnotation class.
		/// </summary>
		public ImageAnnotation()
		{
			this.Width = 60;
			this.Height = 60;
		}
		#endregion Ctor

		#region Properties
		/// <summary>
		/// A value specifies the width of the image.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.Image.Width")]
		[NotifyParentProperty(true)]
		[DefaultValue(60)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Width { get; set; }


		/// <summary>
		/// A value specifies the height of the image.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.Image.Height")]
		[NotifyParentProperty(true)]
		[DefaultValue(60)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Height { get; set; }

		/// <summary>
		/// A value specifies the href for image.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.Image.Href")]
		[NotifyParentProperty(true)]
		[EditorAttribute(typeof(System.Web.UI.Design.ImageUrlEditor), typeof(System.Drawing.Design.UITypeEditor))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string Href { get; set; }

		public override string Type
		{
			get
			{
				return "image";
			}
		}
		#endregion Properties

		#region Serialize
		/// <summary>
		/// Returns a <see cref="System.Boolean"/> value that indicates whether the chart annotation 
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a <see cref="System.Boolean"/> value to determine whether this annotation should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal override bool ShouldSerialize()
		{
			if (Width != 60 || Height != 60 || !String.IsNullOrEmpty(Href))
				return true;

			return base.ShouldSerialize();
		}
		#endregion
	}
}