﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Collections;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Text.RegularExpressions;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	/// <summary>
	/// Represents the ChartSeries class which contains all of the settings for the chart series.
	/// </summary>
	public class ChartSeries: Settings, IJsonEmptiable
	{
		private string[] _hintContents;
        private TrendlineChartSeries _trendlineSeries;

		/// <summary>
		/// Initializes a new instance of the ChartSeries class.
		/// </summary>
		public ChartSeries()
		{
			//this.Data = new ChartSeriesData();
			_hintContents = new string[0];
			this.TextStyle = new ChartStyle();
		}

		#region options

		/// <summary>
		/// A value that indicates the style of the series text.
		/// </summary>
		[C1Description("C1Chart.ChartSeries.TextStyle")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public virtual ChartStyle TextStyle { get; set; }

		/// <summary>
		/// A value that indicates the visibility of the chart series.
		/// </summary>
		[C1Description("C1Chart.ChartSeries.Visible")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Visible
		{
			get
			{
				return this.GetPropertyValue<bool>("Visible", true);
			}
			set
			{
				this.SetPropertyValue<bool>("Visible", value);
			}
		}

		/// <summary>
		/// A value that indicates the label of the chart series.
		/// </summary>
		[C1Description("C1Chart.ChartSeries.Label")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string Label
		{
			get
			{
				return this.GetPropertyValue<string>("Label", "");
			}
			set
			{
				this.SetPropertyValue<string>("Label", value);
			}
		}

		/// <summary>
		/// A value that indicates the legend entry of the chart series.
		/// </summary>
		[C1Description("C1Chart.ChartSeries.LegendEntry")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		//[DefaultValue(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool LegendEntry
		{
			get
			{
				return this.GetPropertyValue<bool>("LegendEntry", true);
			}
			set
			{
				this.SetPropertyValue<bool>("LegendEntry", value);
			}
		}

#if !EXTENDER
		
		/// <summary>
		/// A list of hint value.
		/// </summary>
		[C1Description("C1Chart.ChartSeries.HintContents")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[C1Category("Category.Data")]
		[Layout(LayoutType.Data)]
		[PersistenceMode(PersistenceMode.Attribute)]
		[TypeConverter(typeof(StringArrayConverter))]
		public virtual string[] HintContents
		{
			get
			{
				return _hintContents;
			}
			set
			{
				_hintContents = value;
			}
		}

#endif

        /// <summary>
		/// A value that indicates whether the chart series is trend line series.
		/// </summary>
        [C1Description("C1Chart.ChartSeries.IsTrendline")]
		[NotifyParentProperty(true)]
		[WidgetOption]
        [DefaultValue(false)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
        public virtual bool IsTrendline
        {
            get
            {
                return this.GetPropertyValue<bool>("IsTrendline", false);
            }
            set
            {
                this.SetPropertyValue<bool>("IsTrendline", value);
            }
        }

        /// <summary>
        /// A value that indicates the data of the trendline series.
        /// </summary>
        [C1Description("C1Chart.ChartSeries.TrendlineSeries")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
        [C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
        public virtual TrendlineChartSeries TrendlineSeries
        {
            get
            {
                if (_trendlineSeries == null)
                {
                    _trendlineSeries = new TrendlineChartSeries();
                }

                return _trendlineSeries;
            }
        }
		#endregion

		#region Serialize

        /// <summary>
        /// Indicate whether need to seriealize TrendlineSeries.
        /// </summary>
        /// <returns></returns>
        public virtual bool ShouldSerializeTrendlineSeries()
        {
            return this.IsTrendline && TrendlineSeries.ShouldSerialize();
        }

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		/// <summary>
		/// Returns a boolean value that indicates whether the chart series 
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a boolean value to determine whether this series should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal virtual bool ShouldSerialize()
		{
			if (!Visible)
				return true;
			if (!String.IsNullOrEmpty(Label))
				return true;
			if (!LegendEntry)
				return true;
#if !EXTENDER
			if (HintContents.Length > 0)
			{
				return true;
			}
#endif

            if (TrendlineSeries.ShouldSerialize())
                return true;

			return false;
		}
		
#if !EXTENDER
		/// <summary>
		/// Returns a boolean value that indicates whether the HintContent property
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a boolean value to determine whether the HintContent property should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeHintContents()
		{
			return HintContents.Length > 0;
		}
#endif

		#endregion
	}

	/// <summary>
	/// Represents the ChartSeriesData class which contains all of the settings for the chart data series.
	/// </summary>
	public class ChartSeriesData : Settings, IJsonEmptiable
	{
		
		/// <summary>
		/// Initializes a new instance of the ChartSeriesData class.
		/// </summary>
		public ChartSeriesData()
		{
			this.X = new ChartXAxisList();
			this.Y = new ChartYAxisList();
			//this.Xy = new List<object>();
		}

		#region Properties

		///// <summary>
		///// A value that indicates the x values of the chart series data.
		///// </summary>
		//[NotifyParentProperty(true)]
		//[PersistenceMode(PersistenceMode.InnerProperty)]
		//[WidgetOption]
		//public List<object> X { get; set; }

		///// <summary>
		///// A value that indicates the y values of the chart series data.
		///// </summary>
		//[NotifyParentProperty(true)]
		//[PersistenceMode(PersistenceMode.InnerProperty)]
		//[WidgetOption]
		//public List<object> Y { get; set; }
		/// <summary>
		/// A value that indicates the x values of the chart series data.
		/// </summary>
		[C1Description("C1Chart.ChartSeriesData.X")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public ChartXAxisList X { get; set; }

		/// <summary>
		/// A value that indicates the y values of the chart series data.
		/// </summary>
		[C1Description("C1Chart.ChartSeriesData.Y")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public virtual ChartYAxisList Y { get; set; }
		#endregion
		#region Serialize

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal virtual bool ShouldSerialize()
		{
			return X.Length > 0 || Y.Length > 0;
		}

		#endregion
	}

	#region ** Converters
	internal class DoubleArrayConverter : BaseArrayConverter<double, DoubleConverter>
	{
	}

	internal class IntArrayConverter : BaseArrayConverter<int, Int32Converter>
	{
	}

	internal class DateTimeArrayConverter : BaseArrayConverter<DateTime, DateTimeConverter>
	{
	}

	internal class BaseArrayConverter<T, TConverter> : TypeConverter
		where TConverter : TypeConverter
	{

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			if (value is string)
			{
				string s = ((string)value).Trim();
				ArrayList items = new ArrayList();

				if (s.Length == 0)
				{
					return new T[0];
				}

				string[] values = s.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

				foreach (string item in values)
				{
					if (string.IsNullOrEmpty(item.Trim()))
					{
						items.Add(null);
						continue;
					}

					TConverter converter = System.Activator.CreateInstance<TConverter>();
					items.Add((T)converter.ConvertFrom(context, culture, item));
				}

				return items.ToArray(typeof(T));
			}

			return base.ConvertFrom(context, culture, value);
		}

		public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
		{
			if (value is T[] && destinationType == typeof(string))
			{
				T[] items = value as T[];
				StringBuilder sb = new StringBuilder();

				if (items.Length == 0)
				{
					return string.Empty;
				}

				foreach (object item in items)
				{
					TConverter converter = System.Activator.CreateInstance<TConverter>();
					sb.AppendFormat("{0}, ", converter.ConvertTo(context, culture, item, destinationType));
				}

				sb.Remove(sb.Length - 2, 2);
				return sb.ToString();
			}

			return base.ConvertTo(context, culture, value, destinationType);
		}
	}
	#endregion end of ** Converters.

	#region ** The ChartYData class
	/// <summary>
	/// Represents the ChartYData class which contains all of the settings for the y-axis series data.
	/// </summary>
	[DefaultProperty("DoubleValue")]
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class ChartYData : ICustomOptionType, IJsonEmptiable
	{

		#region ** fields
		private double? _doubleValue = null;
		private DateTime? _dateTimeValue = null;
		#endregion ** end of fields.

		#region ** constructors
		/// <summary>
		/// Initializes a new instance of the ChartYData class.
		/// </summary>
		public ChartYData()
		{
		}

		/// <summary>
		/// Initializes a new instance of the ChartYData class.
		/// </summary>
		/// <param name="doubleValue">
		/// A double? value is set to DoubleValue property.
		/// </param>
		public ChartYData(double? doubleValue)
		{
			this.DoubleValue = doubleValue;
		}

		/// <summary>
		/// Initializes a new instance of the ChartYData class.
		/// </summary>
		/// <param name="doubleValue">
		/// A double value is set to DoubleValue property.
		/// </param>
		public ChartYData(double doubleValue)
			: this(new double?(doubleValue))
		{
		}

		/// <summary>
		/// Initializes a new instance of the ChartYData class.
		/// </summary>
		/// <param name="dateTimeValue">
		/// A DateTime? value is set to DateTimeValue property.
		/// </param>
		public ChartYData(DateTime? dateTimeValue)
		{
			this.DateTimeValue = dateTimeValue;
		}

		/// <summary>
		/// Initializes a new instance of the ChartYData class.
		/// </summary>
		/// <param name="dateTimeValue">
		/// A DateTime value is set to DateTimeValue property.
		/// </param>
		public ChartYData(DateTime dateTimeValue)
			: this(new DateTime?(dateTimeValue))
		{
		}
		#endregion ** end of constructors.

		#region ** properties
		/// <summary>
		/// A double? value specifies the data point of a series.
		/// </summary>
		[C1Description("C1Chart.ChartYData.DoubleValue")]
		[DefaultValue(null)]
		[NotifyParentProperty(true)]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public double? DoubleValue
		{
			get
			{
				return this._doubleValue;
			}
			set
			{
				this.ResetProperties();
				this._doubleValue = value;
			}
		}

		/// <summary>
		/// A DateTime? value specifies the data point of a series.
		/// </summary>
		[C1Description("C1Chart.ChartYData.DateTimeValue")]
		[DefaultValue(null)]
		[NotifyParentProperty(true)]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public virtual DateTime? DateTimeValue
		{
			get
			{
				return this._dateTimeValue;
			}
			set
			{
				this.ResetProperties();
				this._dateTimeValue = value;
			}
		}

		internal virtual bool IsNull
		{
			get
			{
				return this.DoubleValue == null && this.DateTimeValue == null;
			}
		}
		#endregion ** end of properties.

		#region ** methods
		internal virtual void ResetProperties()
		{
			this._doubleValue = null;
			this._dateTimeValue = null;
		}
		#endregion ** end of methods.

		#region Serialize
		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		/// <summary>
		/// Returns a <see cref="System.Boolean"/> value that indicates whether the chart annotation 
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a <see cref="System.Boolean"/> value to determine whether this annotation should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal virtual bool ShouldSerialize()
		{
			if (!IsNull)
			{
				return true;
			}

			return false;
		}

		string ICustomOptionType.SerializeValue()
		{
			return GetSerializeValue();
		}

		protected virtual string GetSerializeValue()
		{
			StringBuilder sb = new StringBuilder();
			if (DateTimeValue != null)
			{
				string date = String.Format("new Date({0}) ", DateTimeValue.Value.ToString("yyyy/MM/dd,HH,mm,ss", CultureInfo.InvariantCulture));
				sb.Append(Regex.Replace(date, @"/(.*)/", ",($1-1),"));
			}
			else if (DoubleValue != null)
			{
				sb.AppendFormat("{0} ", DoubleValue.Value.ToString(CultureInfo.InvariantCulture));
			}

			return sb.ToString();
		}

		bool ICustomOptionType.IncludeQuotes
		{
			get
			{
				return false;
			}
		}

#if !EXTENDER
		void ICustomOptionType.DeSerializeValue(object state)
		{
			if (state == null)
			{
				return;
			}

			if (state is DateTime)
			{
				DateTimeValue = (DateTime)state;
			}
			else if (state is int || state is double)
			{
				DoubleValue = state.ToString().ToNullableDouble();
			}
		}
#endif
		#endregion Serialize
	}
	#endregion ** end of The ChartYData class.

	#region ** The ChartXData class
	/// <summary>
	/// Represents the ChartXData class which contains all of the settings for the x-axis series data.
	/// </summary>
	public class ChartXData : ChartYData
	{

		#region ** fields
		private string _strValue = string.Empty;
		#endregion ** end of fields.

		#region ** constructors
		/// <summary>
		/// Initializes a new instance of the ChartXData class.
		/// </summary>
		public ChartXData()
		{
		}

		/// <summary>
		/// Initializes a new instance of the ChartXData class.
		/// </summary>
		/// <param name="strValue">
		/// A string value is set to StringValue property.
		/// </param>
		public ChartXData(string strValue)
		{
			this.StringValue = strValue;
		}
		#endregion ** end of constructors.

		#region ** properties
		/// <summary>
		/// A string value specifies the data point of a series.
		/// </summary>
		[C1Description("C1Chart.ChartXData.StringValue")]
		[DefaultValue("")]
		[NotifyParentProperty(true)]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public string StringValue
		{
			get
			{
				return this._strValue;
			}
			set
			{
				this.ResetProperties();
				this._strValue = value;
			}
		}

		internal override bool IsNull
		{
			get
			{
				return base.IsNull && string.IsNullOrEmpty(this.StringValue);
			}
		}
		#endregion ** end of properties.

		#region ** methods
		internal override void ResetProperties()
		{
			base.ResetProperties();
			this._strValue = string.Empty;
		}

		protected override string GetSerializeValue()
		{
			StringBuilder sb = new StringBuilder();
			sb.Append(base.GetSerializeValue());
			if (sb.Length == 0 && StringValue != null)
			{
				sb.AppendFormat("\"{0}\" ", JsonWriter.QuoteJScriptString(StringValue));
			}
			return sb.ToString();
		}
		#endregion ** end of methods.
	}
	#endregion ** end of The ChartXData class.

	#region ** The ChartY1Data class
	public class ChartY1Data : ChartYData
	{
		#region ** constructors
		/// <summary>
		/// Initializes a new instance of the ChartY1Data class.
		/// </summary>
		public ChartY1Data()
			:base()
		{
		}

		/// <summary>
		/// Initializes a new instance of the ChartY1Data class.
		/// </summary>
		/// <param name="doubleValue">
		/// A double? value is set to DoubleValue property.
		/// </param>
		public ChartY1Data(double? doubleValue)
			:base(doubleValue)
		{
		}

		/// <summary>
		/// Initializes a new instance of the ChartY1Data class.
		/// </summary>
		/// <param name="doubleValue">
		/// A double value is set to DoubleValue property.
		/// </param>
		public ChartY1Data(double doubleValue)
			: this(new double?(doubleValue))
		{
		}
		#endregion ** end of constructors

		#region ** Hidden properties
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override DateTime? DateTimeValue
		{
			get
			{
				return null;
			}
		}
		#endregion ** end of Hidden properites
	}
	#endregion ** end of The chartY1Data class.

	#region ** The IChartYAxisList interface
	/// <summary>
	/// Represents the IChartYAxisList for handling y-axis series data.
	/// </summary>
	public interface IChartYAxisList
	{
		/// <summary>
		/// Clear all data point values for the series.
		/// </summary>
		void Clear();

		/// <summary>
		/// Add null data point value to the series.
		/// </summary>
		void AddEmpty();

		/// <summary>
		/// Add a double data point value to the series.
		/// </summary>
		/// <param name="value">
		/// The double data point value.
		/// </param>
		void Add(double value);

		/// <summary>
		/// Add a double? data point value to the series.
		/// </summary>
		/// <param name="value">
		/// The double? data point value.
		/// </param>
		void Add(double? value);

		/// <summary>
		/// Add a DateTime data point value to the series.
		/// </summary>
		/// <param name="value">
		/// The DateTime data point value.
		/// </param>
		void Add(DateTime value);

		/// <summary>
		/// Add a DateTime? data point value to the series.
		/// </summary>
		/// <param name="value">
		/// The DateTime? data point value.
		/// </param>
		void Add(DateTime? value);

		/// <summary>
		/// Add the specified double data point values to the series.
		/// </summary>
		/// <param name="values">
		/// The specified double data point values.
		/// </param>
		void AddRange(double[] values);

		/// <summary>
		/// Add the specified double? data point values to the series.
		/// </summary>
		/// <param name="values">
		/// The specified double? data point values.
		/// </param>
		void AddRange(double?[] values);

		/// <summary>
		/// Add the specified DateTime data point values to the series.
		/// </summary>
		/// <param name="values">
		/// The specified DateTime data point values.
		/// </param>
		void AddRange(DateTime[] values);

		/// <summary>
		/// Add the specified DateTime? data point values to the series.
		/// </summary>
		/// <param name="values">
		/// The specified DateTime? data point values.
		/// </param>
		void AddRange(DateTime?[] values);
	}
	#endregion ** end of The IChartYAxisList interface.

	#region ** The IChartXAxisList interface
	/// <summary>
	/// Represents the IChartXAxisList for handling x-axis series data.
	/// </summary>
	public interface IChartXAxisList : IChartYAxisList
	{
		/// <summary>
		/// Add a string data point value to the series.
		/// </summary>
		/// <param name="value">
		/// The string data point value.
		/// </param>
		void Add(string value);

		/// <summary>
		/// Add the specified string data point values to the series.
		/// </summary>
		/// <param name="values">
		/// The specified string data point values.
		/// </param>
		void AddRange(string[] values);
	}
	#endregion ** end of The IChartYAxisList interface.

	/// <summary>
	/// Represents the BaseChartAxisList class which contains all of the settings for the series data.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class BaseChartAxisList<T> : IChartYAxisList, ICustomOptionType, IJsonEmptiable
		where T : ChartYData, new()
	{

		private List<T> _values = null;

		/// <summary>
		/// Initializes a new instance of the BaseChartAxisList class.
		/// </summary>
		public BaseChartAxisList()
		{
		}

		#region Properties
		/// <summary>
		/// A list value contains all the data point values for the series.
		/// </summary>
		[C1Description("C1Chart.BaseChartAxisList.Values")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public List<T> Values
		{
			get
			{
				if (this._values == null)
				{
					this._values = new List<T>();
				}

				return this._values;
			}
		}

		/// <summary>
		/// A double collection value that indicates the axes point data.
		/// </summary>
		/// <remarks>
		/// The DoubleValues/DateTimeValues/StringValues are mutual exclusive.
		/// </remarks>
		[Browsable(false)]
		[NotifyParentProperty(true)]
		[Obsolete("Use the Values property instead")]
		[TypeConverter(typeof(DoubleArrayConverter))]
		public double[] DoubleValues
		{
			get
			{
				if (this.Values.Count == 0)
				{
					return new double[0];
				}

				List<double> vals = new List<double>();

				foreach (T val in this.Values)
				{
					if (val == null || val.DoubleValue == null)
					{
						continue;
					}

					vals.Add(val.DoubleValue.Value);
				}

				return vals.ToArray();
			}
			set
			{
				this.AddRange(value);
			}
		}

		/// <summary>
		/// A DateTime collection value that indicates the axes point data.
		/// </summary>
		/// <remarks>
		/// The DoubleValues/DateTimeValues/StringValues are mutual exclusive.
		/// </remarks>
		[Browsable(false)]
		[NotifyParentProperty(true)]
		[Obsolete("Use the Values property instead")]
		[TypeConverter(typeof(DateTimeArrayConverter))]
		public virtual DateTime[] DateTimeValues
		{
			get
			{
				if (this.Values.Count == 0)
				{
					return new DateTime[0];
				}

				List<DateTime> vals = new List<DateTime>();

				foreach (ChartYData val in this.Values)
				{
					if (val == null || val.DateTimeValue == null)
					{
						continue;
					}

					vals.Add(val.DateTimeValue.Value);
				}

				return vals.ToArray();
			}
			set
			{
				this.AddRange(value);
			}
		}

		/// <summary>
		/// Gets the length of the current data points.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		internal protected virtual int Length
		{
			get
			{
				return this.Values.Count;
			}
		}
		#endregion

		#region public methods
		/// <summary>
		/// Clear all data point values for the series.
		/// </summary>
		public void Clear()
		{
			this.Values.Clear();
		}

		/// <summary>
		/// Add null data point value to the series.
		/// </summary>
		public void AddEmpty()
		{
			this.Values.Add(new T());
		}

		/// <summary>
		/// Add a double data point value to the series.
		/// </summary>
		/// <param name="value">
		/// The double data point value.
		/// </param>
		public void Add(double value)
		{
			this.Add(new double?(value));
		}

		/// <summary>
		/// Add a double? data point value to the series.
		/// </summary>
		/// <param name="value">
		/// The double? data point value.
		/// </param>
		public void Add(double? value)
		{
			T val = new T();
			val.DoubleValue = value;
			this.Values.Add(val);
		}

		/// <summary>
		/// Add a DateTime data point value to the series.
		/// </summary>
		/// <param name="value">
		/// The DateTime data point value.
		/// </param>
		public virtual void Add(DateTime value)
		{
			this.Add(new DateTime?(value));
		}

		/// <summary>
		/// Add a DateTime? data point value to the series.
		/// </summary>
		/// <param name="value">
		/// The DateTime? data point value.
		/// </param>
		public virtual void Add(DateTime? value)
		{
			T val = new T();
			val.DateTimeValue = value;
			this.Values.Add(val);
		}

		/// <summary>
		/// Add the specified double data point values to the series.
		/// </summary>
		/// <param name="values">
		/// The specified double data point values.
		/// </param>
		public void AddRange(double[] values)
		{
			this.Clear();

			foreach (double val in values)
			{
				this.Add(val);
			}
		}

		/// <summary>
		/// Add the specified double? data point values to the series.
		/// </summary>
		/// <param name="values">
		/// The specified double? data point values.
		/// </param>
		public void AddRange(double?[] values)
		{
			this.Clear();

			foreach (double? val in values)
			{
				if (val == null)
				{
					this.AddEmpty();
					continue;
				}

				this.Add(val);
			}
		}

		/// <summary>
		/// Add the specified DateTime data point values to the series.
		/// </summary>
		/// <param name="values">
		/// The specified DateTime data point values.
		/// </param>
		public virtual void AddRange(DateTime[] values)
		{
			this.Clear();

			foreach (DateTime val in values)
			{
				this.Add(val);
			}
		}

		/// <summary>
		/// Add the specified DateTime? data point values to the series.
		/// </summary>
		/// <param name="values">
		/// The specified DateTime? data point values.
		/// </param>
		public virtual void AddRange(DateTime?[] values)
		{
			this.Clear();

			foreach (DateTime? val in values)
			{
				if (val == null)
				{
					this.AddEmpty();
					continue;
				}

				this.Add(val);
			}
		}
		#endregion

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeDateTimeValues()
		{
			return false;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeDoubleValues()
		{
			return false;
		}

		#region ICustomOptionType Members
		string ICustomOptionType.SerializeValue()
		{
			StringBuilder sb = new StringBuilder();
			string val = GetSerializeValue();
			if (string.IsNullOrEmpty(val)) {
				return "null";
			}
			sb.Append("[");

			sb.Append(val);
			
			sb.Remove(sb.Length - 2, 2);

			sb.Append("]");
			return sb.ToString();
		}

		bool ICustomOptionType.IncludeQuotes
		{
			get
			{
				return false;
			}
		}

		bool IJsonEmptiable.IsEmpty
		{
			get 
			{
				return this.Length == 0;
			}
		}

#if !EXTENDER
		void ICustomOptionType.DeSerializeValue(object state)
		{
			if (state is ArrayList)
			{
				ArrayList list = state as ArrayList;
				if (list.Count > 0)
				{
					//object item = list[0];
					object item = new object();
					foreach (object i in list)
					{
						if (i != null)
						{
							item = i;
							break;
						}
					}
					SetValues(item, list);
				}
			}
		}
#endif

		/// <summary>
		/// Serialize the current series to a string value.
		/// </summary>
		/// <returns>
		/// Returns the serialized string value.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected virtual string GetSerializeValue()
		{
			StringBuilder sb = new StringBuilder();

			if (this.Length > 0)
			{
				foreach (T item in this.Values)
				{
					if (item == null || item.IsNull)
					{
						sb.Append(", ");
					}
					else
					{
						string serializedValue = ((ICustomOptionType)item).SerializeValue();
						if(!string.IsNullOrEmpty(serializedValue))
						{
							sb.AppendFormat("{0},", serializedValue);
						}
					}
				}
			}

			return sb.ToString();
		}

		/// <summary>
		/// Convert the specified <see cref="System.Collections.ArrayList"/> value to the corresponding
		/// appropriate type array and set it to the related property.
		/// </summary>
		/// <param name="item">
		/// The first item of the <see cref="System.Collections.ArrayList"/> value.
		/// </param>
		/// <param name="list">
		/// The specified <see cref="System.Collections.ArrayList"/> value.
		/// </param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected virtual void SetValues(object item, ArrayList list)
		{
			this.Clear();

			foreach (object val in list)
			{
				if (val == null)
				{
					this.AddEmpty();
					continue;
				}

				if (item is DateTime)
				{
					this.Add((DateTime)val);
				}
				else if (item is int || item is double)
				{
					this.Add(val.ToString().ToNullableDouble());
				}
			}
		}
		#endregion
	}

	/// <summary>
	/// Represents the ChartYAxisList class which contains all of the settings for the series list shown on y axis.
	/// </summary>
	[Serializable]
	public class ChartYAxisList : BaseChartAxisList<ChartYData>
	{

		/// <summary>
		/// Initializes a new instance of the ChartYAxisList class.
		/// </summary>
		public ChartYAxisList()
		{
		}
	}

		/// <summary>
		/// Represents the ChartXAxisList class which contains all of the settings for the series list shown on x axis.
		/// </summary>
	public class ChartXAxisList : BaseChartAxisList<ChartXData>, IChartXAxisList
	{

		/// <summary>
		/// Initializes a new instance of the ChartXAxisList class.
		/// </summary>
		public ChartXAxisList()
		{
		}

		#region Properties
		/// <summary>
		/// A string collection value that indicates the axes point data.
		/// </summary>
		/// <remarks>
		/// The DoubleValues/DateTimeValues/StringValues are mutual exclusive.
		/// </remarks>
		[Browsable(false)]
		[DefaultValue("")]
		[NotifyParentProperty(true)]
		[TypeConverter(typeof(StringArrayConverter))]
		[Obsolete("Use the Values property instead")]
		public string[] StringValues
		{
			get
			{
				if (this.Length == 0)
				{
					return new string[0];
				}

				List<string> vals = new List<string>();

				foreach (ChartXData val in this.Values)
				{
					if (string.IsNullOrEmpty(val.StringValue))
					{
						continue;
					}

					vals.Add(val.StringValue);
				}

				return vals.ToArray();
			}
			set
			{
				this.Values.Clear();

				foreach (string val in value)
				{
					this.Values.Add(new ChartXData(val));
				}
			}
		}
		#endregion

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeStringValues()
		{
			return false;
		}

		/// <summary>
		/// Add a string data point value to the series.
		/// </summary>
		/// <param name="value">
		/// The string data point value.
		/// </param>
		public void Add(string value)
		{
			this.Values.Add(new ChartXData(value));
		}

		/// <summary>
		/// Add the specified string data point values to the series.
		/// </summary>
		/// <param name="values">
		/// The specified string data point values.
		/// </param>
		public void AddRange(string[] values)
		{
			this.Clear();

			foreach (string val in values)
			{
				this.Add(val);
			}
		}

		#region Serialize
		/// <summary>
		/// Convert the specified <see cref="System.Collections.ArrayList"/> value to the corresponding
		/// appropriate type array and set it to the related property.
		/// </summary>
		/// <param name="item">
		/// The first item of the <see cref="System.Collections.ArrayList"/> value.
		/// </param>
		/// <param name="list">
		/// The specified <see cref="System.Collections.ArrayList"/> value.
		/// </param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void SetValues(object item, ArrayList list)
		{
			if (item is string)
			{
				this.Clear();

				foreach (object val in list)
				{
					if (val == null)
					{
						this.AddEmpty();
						continue;
					}

					this.Add((string)val);
				}
			}
			else
			{
				base.SetValues(item, list);
			}
		}
		#endregion
	}

	/// <summary>
	/// Represents the ChartY1AxisList class which contains all of the settings for the series list shown on some axis.
	/// </summary>
	public class ChartY1AxisList : BaseChartAxisList<ChartY1Data>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ChartY1AxisList"/> class.
		/// </summary>
		public ChartY1AxisList()
		{
		}

		#region ** Hidden properties & methods
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override DateTime[] DateTimeValues
		{
			get
			{
				return null;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void Add(DateTime value)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void Add(DateTime? value)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void AddRange(DateTime?[] values)
		{
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public override void AddRange(DateTime[] values)
		{
		}
		#endregion ** end of Hidden properties & methods.
	}

}
