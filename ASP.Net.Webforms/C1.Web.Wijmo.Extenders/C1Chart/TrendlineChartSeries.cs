﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Collections;
using System.ComponentModel.Design;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
    /// <summary>
    /// Represents the type of trendline series.
    /// </summary>
    public enum TrendlineFitType
    {        
        /// <summary>
        /// Regression fit to the equation Y(x) = C0 + C1*x + C2*x2 + : + Cn-1*xn-1, where n - number of terms.
        /// </summary>
	    Polynom = 0,
        /// <summary>
        /// Regression fit to the equation Y(x) = C0 * exp( C1*x)
        /// </summary>
	    Exponent,
        /// <summary>
        /// Regression fit to the equation Y(x) = C0 * ln(C1*x)
        /// </summary>
	    Logarithmic,
        /// <summary>
        /// Regression fit to the equation Y(x) = C0 * pow(x, C1)
        /// </summary>
	    Power,
        /// <summary>
        /// Regression fit to the equation Y(x) = C0 + C1 * cos(x) + C2 * sin(x) + C3 * cos(2*x) + C4 * sin(2*x) + ...
        /// </summary>
	    Fourier,
        /// <summary>
        /// Minimal X-value.
        /// </summary>
	    MinX,
        /// <summary>
        /// Minimal Y-value.
        /// </summary>
	    MinY,
        /// <summary>
        /// Maximum X-value.
        /// </summary>
	    MaxX,
        /// <summary>
        /// Maximum Y-value.
        /// </summary>
	    MaxY,
        /// <summary>
        /// Average X-value.
        /// </summary>
	    AverageX,
        /// <summary>
        /// Average Y-value.
        /// </summary>
	    AverageY
    }

	/// <summary>
    /// Represents the TrendlineSeries class which contains all of the settings for the trendline series.
	/// </summary>
    public class TrendlineChartSeries : Settings, ICustomOptionType
	{
        ChartSeriesData _data = new ChartSeriesData();

		/// <summary>
        /// Initializes a new instance of the TrendlineSeries class.
		/// </summary>
        public TrendlineChartSeries()
		{
		}

		#region options

        /// <summary>
        /// A value that indicates the style of the series.
        /// </summary>
        [C1Description("C1Chart.TrendlineSeries.FitType")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue(typeof(TrendlineFitType), "Polynom")]
        [C1Category("Category.Style")]
        public TrendlineFitType FitType
        {
            get
            {
                return this.GetPropertyValue<TrendlineFitType>("FitType", TrendlineFitType.Polynom);
            }
            set
            {
                this.SetPropertyValue<TrendlineFitType>("FitType", value);
            }
        }

        /// <summary>
        /// A value specifies the sample count for function calculation. It works when the fitType is polynom, power, exponent, logarithmic and fourier.
        /// </summary>
        [C1Description("C1Chart.TrendlineSeries.SampleCount")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue(100)]
        [C1Category("Category.Style")]
        public int SampleCount
        {
            get
            {
                return this.GetPropertyValue<int>("SampleCount", 100);
            }
            set
            {
                this.SetPropertyValue<int>("SampleCount", value);
            }
        }

        /// <summary>
        /// A value defines the number of terms in polynom equation. It should be set to some integer value greater than 1. It works when the fitType is polynom, power, exponent, logarithmic and fourier. 
        /// </summary>
        [C1Description("C1Chart.TrendlineSeries.Order")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue(4)]
        [C1Category("Category.Style")]
        public int Order
        {
            get
            {
                return this.GetPropertyValue<int>("Order", 4);
            }
            set
            {
                this.SetPropertyValue<int>("Order", value);
            }
        }

        /// <summary>
        /// A value that indicates the data of the trendline series.
        /// </summary>
        [C1Description("C1Chart.TrendlineSeries.Data")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
        [C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
        public ChartSeriesData Data 
        {
            get { return _data; }
        }

        #endregion

        #region methods

        /// <summary>
        /// Determine whether the TrendlineSeries property should be serialized to client side.
        /// </summary>
        /// <returns>
        /// Returns false if TrendlineSeries has default values, otherwise return false.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public virtual bool ShouldSerialize()
        {
            if (FitType != TrendlineFitType.Polynom)
                return true;

            if (SampleCount != 100)
                return true;

            if (Order != 4)
                return true;

            if (Data.ShouldSerialize())
                return true;

            return false;
        }

        #endregion

        #region ICustomOptionType

        string ICustomOptionType.SerializeValue()
        {
            StringBuilder sbSerialize = new StringBuilder();

            sbSerialize.Append(string.Format("order: {0}, ", this.Order));
            sbSerialize.Append(string.Format("sampleCount: {0}, ", this.SampleCount));
            sbSerialize.Append(string.Format("fitType: \"{0}\", ", this.FitType.ToCamel()));

            sbSerialize.Append(string.Format("data: {0}", JsonHelper.WidgetToString(this.Data, null)));

            return sbSerialize.ToString();
        }

#if !EXTENDER
        void ICustomOptionType.DeSerializeValue(object state)
        {
        }

#endif

        bool ICustomOptionType.IncludeQuotes
        {
            get { return false; }
        }

        #endregion 
   
    }
}
