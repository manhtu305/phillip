﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Drawing.Design;
using System.Web.UI.WebControls;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
using System.Collections;
using System.ComponentModel.Design.Serialization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{

	#region ChartAxes
	/// <summary>
	/// Represents the objects for the x and y axes.
	/// </summary>
	public class ChartAxes : Settings, IJsonEmptiable
	{

		/// <summary>
		/// Initializes a new instance of the ChartAxes class.
		/// </summary>
		public ChartAxes()
		{
			this.X = new ChartAxis();
			this.Y = new ChartAxis();
			this.Y.Visible = false;
			this.Y.Labels.TextAlign = ChartAxisAlignment.Center;
			this.Y.Compass = ChartCompass.West;
			this.Y.GridMajor.Visible = true;
		}

		/// <summary>
		/// A value that provides information for the X axis.
		/// </summary>
		[C1Description("C1Chart.ChartAxes.X")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Misc")]
#if !EXTENDER
		[Layout(LayoutType.Misc)]
#endif
		public ChartAxis X { get; set; }

		/// <summary>
		/// A value that provides information for the Y axis.
		/// </summary>
		[C1Description("C1Chart.ChartAxes.Y")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Misc")]
#if !EXTENDER
		[Layout(LayoutType.Misc)]
#endif
		public ChartAxis Y { get; set; }

		#region Serialize

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal bool ShouldSerialize()
		{
			if (X.ShouldSerialize())
				return true;
			if (Y.ShouldSerialize())
				return true;
			if (Y.Visible)
				return true;
			return false;
		}
		#endregion
	}
	#endregion

	/// <summary>
	/// Represents the class for the Axis object. The axes appears on the plot area of the chart.
	/// </summary>
	public class ChartAxis: Settings, IJsonEmptiable
	{
		//string[] _valueLabels = null;
		private string[] _valueLabels = null;

		/// <summary>
		/// Initializes a new instance of the ChartAxis class.
		/// </summary>
		public ChartAxis()
		{
			this.AxisStyle = new ChartStyle();
			this.TextStyle=new ChartStyle();
			this.Labels = new ChartAxisLabel();
			this.GridMajor = new ChartAxisGrid();
			this.GridMajor.Visible = true;
			this.GridMinor = new ChartAxisGrid();
			this.TickMajor = new ChartAxisTick();
			this.TickMinor = new ChartAxisTick();

			//this.ValueLabels = new ChartXAxisList();
		}

		#region options

		/// <summary>
		/// A value that indicates the alignment of the axis text.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.Alignment")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(ChartAxisAlignment.Center)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public ChartAxisAlignment Alignment
		{
			get
			{
				return GetPropertyValue<ChartAxisAlignment>("Alignment", ChartAxisAlignment.Center);
			}
			set
			{
				SetPropertyValue<ChartAxisAlignment>("Alignment", value);
			}
		}
		
		/// <summary>
		/// A value that indicates the style of the axis.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use the AxisStyle property instead.")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public ChartStyle Style
		{
			get
			{
				return this.AxisStyle;
			}
			set
			{
				this.AxisStyle = value;
			}
		}

		/// <summary>
		/// A value that indicates the style of the axis.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.Style")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[WidgetOptionName("style")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle AxisStyle { get; set; }

		/// <summary>
		/// A value that indicates the visibility of the axis.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.Visible")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[Json(true)]
		[DefaultValue(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Visible
		{
			get
			{
				return GetPropertyValue<bool>("Visible", true);
			}
			set
			{
				SetPropertyValue<bool>("Visible", value);
			}
		}

		/// <summary>
		/// A value that indicates the text of the axis.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.Text")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string Text
		{
			get
			{
				return GetPropertyValue<string>("Text", "");
			}
			set
			{
				SetPropertyValue<string>("Text", value);
			}
		}

		/// <summary>
		/// A value that indicates the visibility of the axis text.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.TextVisible")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool TextVisible
		{
			get
			{
				return GetPropertyValue<bool>("TextVisible", true);
			}
			set
			{
				SetPropertyValue<bool>("TextVisible", value);
			}
		}

		/// <summary>
		/// A value that indicates the style of text of the axis.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.TextStyle")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle TextStyle { get; set; }

		/// <summary>
		/// A value that provides information for the labels.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.Labels")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public ChartAxisLabel Labels { get; set; }

		/// <summary>
		/// A value that indicates the compass of the axis.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.Compass")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[Json(true,false,ChartCompass.South)]
		[DefaultValue(ChartCompass.South)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public ChartCompass Compass
		{
			get
			{
				return GetPropertyValue<ChartCompass>("Compass", ChartCompass.South);
			}
			set
			{
				SetPropertyValue<ChartCompass>("Compass", value);
			}
		}

		/// <summary>
		/// A value that indicates whether the minimum axis value is calculated automatically.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.AutoMin")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(true)]
		[C1Category("Category.Behavior")]
		// In chart widgets, we have remove the autoMax and autoMin options, if the max and min is default value(null), the charts will auto calculate the max and min.
		// if the max/min is seted to other values, the charts will use the max/min to render charts.
		[Obsolete("When set the Min property, this value will auto set to false, If not set the Min property, this value is true.")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool AutoMin
		{
			get
			{
				return GetPropertyValue<bool>("AutoMin", true);
			}
			set
			{
				SetPropertyValue<bool>("AutoMin", value);
			}
		}

		/// <summary>
		/// A value that indicates whether the maximum axis value is calculated automatically.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.AutoMax")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(true)]
		[C1Category("Category.Behavior")]
		// In chart widgets, we have remove the autoMax and autoMin options, if the max and min is default value(null), the charts will auto calculate the max and min.
		// if the max/min is seted to other values, the charts will use the max/min to render charts.
		[Obsolete("When set the Max property, this value will auto set to false, If not set the Max property, this value is true.")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool AutoMax
		{
			get
			{
				return GetPropertyValue<bool>("AutoMax", true);
			}
			set
			{
				SetPropertyValue<bool>("AutoMax", value);
			}
		}

		/// <summary>
		/// A value that indicates the minimum value of the axis.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.Min")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public double? Min
		{
			get
			{
				return GetPropertyValue<double?>("Min", null);
			}
			set
			{
				SetPropertyValue<double?>("Min", value);
			}
		}

		/// <summary>
		/// A value that indicates the maximum value of the axis.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.Max")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public double? Max
		{
			get
			{
				return GetPropertyValue<double?>("Max", null);
			}
			set
			{
				SetPropertyValue<double?>("Max", value);
			}
		}

		/// <summary>
		/// A value that indicates the origin value of the axis.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.Origin")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double? Origin
		{
			get
			{
				return GetPropertyValue<double?>("Origin", null);
			}
			set
			{
				SetPropertyValue<double?>("Origin", value);
			}
		}

		/// <summary>
		/// A value that indicates whether the major tick mark values are calculated automatically.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.AutoMajor")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool AutoMajor
		{
			get
			{
				return GetPropertyValue<bool>("AutoMajor", true);
			}
			set
			{
				SetPropertyValue<bool>("AutoMajor", value);
			}
		}

		/// <summary>
		/// A value that indicates whether the minor tick mark values are calculated automatically.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.AutoMinor")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool AutoMinor
		{
			get
			{
				return GetPropertyValue<bool>("AutoMinor", true);
			}
			set
			{
				SetPropertyValue<bool>("AutoMinor", value);
			}
		}

		/// <summary>
		/// A value that indicates the units between major tick marks.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.UnitMajor")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(0.0)]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public double UnitMajor
		{
			get
			{
				return GetPropertyValue<double>("UnitMajor", 0.0);
			}
			set
			{
				SetPropertyValue<double>("UnitMajor", value);
			}
		}

		/// <summary>
		/// A value that indicates the units between minor tick marks.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.UnitMinor")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(0.0)]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public double UnitMinor
		{
			get
			{
				return GetPropertyValue<double>("UnitMinor", 0.0);
			}
			set
			{
				SetPropertyValue<double>("UnitMinor", value);
			}
		}

		/// <summary>
		/// A value that provides information for the major grid line.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.GridMajor")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public ChartAxisGrid GridMajor { get; set; }

		/// <summary>
		/// A value that provides information for the minor grid line.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.GridMinor")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public ChartAxisGrid GridMinor { get; set; }

		/// <summary>
		/// A value that provides information for the major tick.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.TickMajor")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public ChartAxisTick TickMajor { get; set; }

		/// <summary>
		/// A value that provides information for the minor tick.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.TickMinor")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public ChartAxisTick TickMinor { get; set; }

		/// <summary>
		/// A value that indicates the method of annotation.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.AnnoMethod")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(ChartAxisAnnoMethod.Values)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public ChartAxisAnnoMethod AnnoMethod
		{
			get
			{
				return GetPropertyValue<ChartAxisAnnoMethod>("AnnoMethod", ChartAxisAnnoMethod.Values);
			}
			set
			{
				SetPropertyValue<ChartAxisAnnoMethod>("AnnoMethod", value);
			}
		}

		/// <summary>
		/// A value that indicates the format string of annotation.
		/// </summary>
		[C1Description("C1Chart.ChartAxis.AnnoFormatString")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Appearance")]
#if !EXTENDER
#if ASP_NET4
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1AnnoFormatStringEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1AnnoFormatStringEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#endif
		[Layout(LayoutType.Appearance)]
#endif
		public string AnnoFormatString
		{
			get
			{
				return GetPropertyValue<string>("AnnoFormatString", "");
			}
			set
			{
				SetPropertyValue<string>("AnnoFormatString", value);
			}
		}

		/// <summary>
		/// A value that shows a collection of valueLabels for the axis.
		/// </summary>
        [Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[TypeConverter(typeof(StringArrayConverter))]
		[Obsolete("Use the ValueLabelList property instead.")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		//public List<string> ValueLabels
		public string[] ValueLabels
		{
			get
			{
				if (_valueLabels == null)
				{
					_valueLabels = new string[0];
				}
				return _valueLabels;
			}
			set
			{
				_valueLabels = value;
				
				this.ValueLabelList.Clear();

				foreach (string val in value)
				{
					ValueLabel vl = new ValueLabel();
					vl.Text = val;

					this.ValueLabelList.Add(vl);
				}
			}
		}

		private List<ValueLabel> _valueLabelList = new List<ValueLabel>();

		/// <summary>
		/// A value that shows a collection of valueLabels for the axis.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("C1Chart.ChartAxis.ValueLabels")]
		[CollectionItemType(typeof(ValueLabel))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
		[WidgetOptionName("valueLabels")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public List<ValueLabel> ValueLabelList
		{
			get
			{
				return this._valueLabelList;
			}
		}

		#endregion end of options

		#region Serialize

		/// <summary>
		/// Determine whether the ValueLabels property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if ValueLabels has values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeValueLabelList()
		{
			//return ValueLabels.Count > 0;
			return this.ValueLabelList.Count > 0;
		}


		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal virtual bool ShouldSerialize()
		{
			if (Alignment != ChartAxisAlignment.Center)
				return true;
			if (AxisStyle.ShouldSerialize())
				return true;
			if (!Visible)
				return true;
			if (!String.IsNullOrEmpty(Text))
				return true;
			if (!TextVisible)
				return true;
			if (TextStyle.ShouldSerialize())
				return true;
			if (Labels.ShouldSerialize())
				return true;
			//if (Compass != ChartCompass.South)
			//    return true;
			if (!AutoMin)
				return true;
			if (!AutoMax)
				return true;
			if (Min != null)
				return true;
			if (Max != null)
				return true;
			if (Origin != null)
				return true;
			if (!AutoMajor)
				return true;
			if (!AutoMinor)
				return true;
			if (UnitMajor != 0.0)
				return true;
			if (UnitMinor != 0.0)
				return true;
			if (GridMajor.ShouldSerializeStyle() || !GridMajor.Visible)
				return true;
			if (GridMinor.ShouldSerializeStyle() || GridMajor.Visible)
				return true;
			if (TickMajor.ShouldSerialize())
				return true;
			if (TickMinor.ShouldSerialize())
				return true;
			if (AnnoMethod != ChartAxisAnnoMethod.Values)
				return true;
			if (!String.IsNullOrEmpty(AnnoFormatString))
				return true;
			//if (ValueLabels.Count > 0)
			if (ValueLabelList.Count > 0)
				return true;
			return false;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeStyle()
		{
			return false;
		}

		//[EditorBrowsable(EditorBrowsableState.Never)]
		//public bool ShouldSerializeGridMajor()
		//{
		//    return !this.GridMajor.Visible || this.GridMajor.ShouldSerialize();
		//}

		//[EditorBrowsable(EditorBrowsableState.Never)]
		//public bool ShouldSerializeGridMinor()
		//{
		//    return this.GridMinor.Visible || this.GridMinor.ShouldSerialize() ;
		//}
		#endregion
	}
}
