﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Collections;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{

	#region ChartAxisLabel

	/// <summary>
	/// Represents the ChartAxisLabel class which contains all of the settings for the axis label.
	/// </summary>
	public class ChartAxisLabel: Settings, IJsonEmptiable
	{

		/// <summary>
		/// Initializes a new instance of the ChartAxisLabel class.
		/// </summary>
		public ChartAxisLabel()
		{
			this.AxisLabelStyle = new ChartStyle();
		}

		#region options
		/// <summary>
		/// A value that indicates the style of major text of the axis.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use the AxisLabelStyle property instead.")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public ChartStyle Style
		{
			get
			{
				return this.AxisLabelStyle;
			}
			set
			{
				this.AxisLabelStyle = value;
			}
		}

		/// <summary>
		/// A value that indicates the style of major text of the axis.
		/// </summary>
		[C1Description("C1Chart.ChartAxisLabel.Style")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[WidgetOptionName("style")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle AxisLabelStyle { get; set; }

		/// <summary>
		/// A value that indicates the alignment of major text of the axis.
		/// </summary>
		[C1Description("C1Chart.ChartAxisLabel.TextAlign")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(ChartAxisAlignment.Near)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public ChartAxisAlignment TextAlign
		{
			get
			{
				return GetPropertyValue<ChartAxisAlignment>("TextAlign", ChartAxisAlignment.Near);
			}
			set
			{
				SetPropertyValue<ChartAxisAlignment>("TextAlign", value);
			}
		}

		/// <summary>
		/// A value that indicates the width of major text of the axis.
		/// </summary>
		[C1Description("C1Chart.ChartAxisLabel.Width")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Sizes)]
#endif
		public int? Width
		{
			get
			{
				return GetPropertyValue<int?>("Width", null);
			}
			set
			{
				SetPropertyValue<int?>("Width", value);
			}
		}

		#endregion

		#region Serialize

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal bool ShouldSerialize()
		{
			if (AxisLabelStyle.ShouldSerialize())
				return true;
			if (TextAlign != ChartAxisAlignment.Near)
				return true;
			if (Width.HasValue)
				return true;
			return false;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeStyle()
		{
			return false;
		}
		#endregion

	}
	#endregion

	#region ChartAxisGrid

	/// <summary>
	/// Represents the ChartAxisGrid class which contains all of the settings for the axis grid.
	/// </summary>
	public class ChartAxisGrid: Settings
	{

		/// <summary>
		/// Initializes a new instance of the ChartAxisGrid class.
		/// </summary>
		public ChartAxisGrid()
		{
			this.GridStyle = new ChartStyle();
		}

		#region options
		/// <summary>
		/// A value that indicates the visibility of the grid line.
		/// </summary>
		[C1Description("C1Chart.ChartAxisGrid.Visible")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		//[DefaultValue(false)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Visible
		{
			get
			{
				return GetPropertyValue<bool>("Visible", false);
			}
			set
			{
				SetPropertyValue<bool>("Visible", value);
			}
		}

		/// <summary>
		/// A value that indicates the style of the grid line.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use the GridStyle property instead.")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public ChartStyle Style
		{
			get
			{
				return this.GridStyle;
			}
			set
			{
				this.GridStyle = value;
			}
		}

		/// <summary>
		/// A value that indicates the style of the grid line.
		/// </summary>
		[C1Description("C1Chart.ChartAxisGrid.Style")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[WidgetOptionName("style")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle GridStyle { get; set; }
		#endregion

		#region Serialize

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeStyle()
		{
			return false;
		}
		#endregion
	}
	#endregion

	#region ChartAxisTick

	/// <summary>
	/// Represents the ChartAxisTick class which contains all settings for the axis tick.
	/// </summary>
	public class ChartAxisTick: Settings, IJsonEmptiable
	{
		/// <summary>
		/// Initializes a new instance of the ChartAxisTick class.
		/// </summary>
		public ChartAxisTick()
		{
			this.TickStyle = new ChartStyle();
		}

		#region options

		/// <summary>
		/// A value that indicates the type of tick mark.
		/// </summary>
		[C1Description("C1Chart.ChartAxisTick.Position")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(ChartAxisTickPosition.None)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public ChartAxisTickPosition Position
		{
			get
			{
				return GetPropertyValue<ChartAxisTickPosition>("Position", ChartAxisTickPosition.None);
			}
			set
			{
				SetPropertyValue<ChartAxisTickPosition>("Position", value);
			}
		}

		/// <summary>
		/// A value that indicates the style of the tick mark.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use the TickStyle property instead.")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public ChartStyle Style
		{
			get
			{
				return this.TickStyle;
			}
			set
			{
				this.TickStyle = value;
			}
		}

		/// <summary>
		/// A value that indicates the style of tick mark.
		/// </summary>
		[C1Description("C1Chart.ChartAxisTick.Style")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[WidgetOptionName("style")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle TickStyle { get; set; }

		/// <summary>
		/// A value that indicates an integral factor for tick mark length.
		/// </summary>
		[C1Description("C1Chart.ChartAxisTick.Factor")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(1)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Factor
		{
			get
			{
				return GetPropertyValue<int>("Factor", 1);
			}
			set
			{
				SetPropertyValue<int>("Factor", value);
			}
		}

		#endregion

		#region Serialize

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal bool ShouldSerialize()
		{
			if (Position != ChartAxisTickPosition.None)
				return true;
			if (TickStyle.ShouldSerialize())
				return true;
			if (Factor != 1)
				return true;
			return false;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeStyle()
		{
			return false;
		}
		#endregion
	}
	#endregion

	#region ValueLabel
	/// <summary>
	/// Represents the ValueLabel class which contains all settings for the value label.
	/// </summary>
	public class ValueLabel : Settings, ICustomOptionType, IJsonEmptiable
	{
		/// <summary>
		/// Initializes a new instance of the ValueLabel class.
		/// </summary>
		public ValueLabel()
		{
			GridLineStyle = new ChartStyle();
		}

		/// <summary>
		/// A value that indicates the text of axis value label.
		/// </summary>
		[NotifyParentProperty(true)]
		[C1Description("C1Chart.ValueLabel.Text")]
		[DefaultValue("")]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public string Text
		{
			get
			{
				return GetPropertyValue<string>("Text", "");
			}
			set
			{
				SetPropertyValue<string>("Text", value);
			}
		}

		/// <summary>
		/// A value that indicates the datetime value of axis value label.
		/// </summary>
		[NotifyParentProperty(true)]
		[C1Description("C1Chart.ValueLabel.DateTimeValue")]
		[DefaultValue(null)]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public DateTime? DateTimeValue
		{
			get
			{
				return GetPropertyValue<DateTime?>("DateTimeValue", null);
			}
			set
			{
				SetPropertyValue<DateTime?>("DateTimeValue", value);
			}
		}


		/// <summary>
		/// A value that indicates the numeric value of axis value label.
		/// </summary>
		[NotifyParentProperty(true)]
		[C1Description("C1Chart.ValueLabel.NumericValue")]
		[DefaultValue(null)]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public double? NumericValue
		{
			get
			{
				return GetPropertyValue<double?>("NumericValue", null);
			}
			set
			{
				SetPropertyValue<double?>("NumericValue", value);
			}
		}

		/// <summary>
		/// A value that indicates the numeric value of axis value label.
		/// </summary>
		[NotifyParentProperty(true)]
		[C1Description("C1Chart.ValueLabel.GridLine")]
		[DefaultValue(false)]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public bool GridLine
		{
			get
			{
				return GetPropertyValue<bool>("GridLine", false);
			}
			set
			{
				SetPropertyValue<bool>("GridLine", value);
			}
		}

		/// <summary>
		/// A value that indicates the style of value label grid line.
		/// </summary>
		[C1Description("C1Chart.ValueLabel.GridLineStyle")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle GridLineStyle { get; set; }

		string ICustomOptionType.SerializeValue()
		{
			Hashtable ht = new Hashtable();
			if (!String.IsNullOrEmpty(Text))
			{
				ht.Add("text", Text);
			}
			if (NumericValue.HasValue)
			{
				ht.Add("value", NumericValue.Value);
			}
			else if (DateTimeValue.HasValue)
			{
				ht.Add("value", DateTimeValue);
			}
			ht.Add("gridLine", GridLine);
			if (GridLineStyle.ShouldSerialize())
			{
				ht.Add("gridLineStyle", GridLineStyle);
			}
			
			return JsonHelper.WidgetToString(ht, null);
		}
		
#if !EXTENDER
		void ICustomOptionType.DeSerializeValue(object state)
		{
			Hashtable ht = state as Hashtable;
			if (ht != null)
			{
				if (ht["text"] != null)
				{
					this.Text = (string)ht["text"];
				}
				if (ht["value"] != null)
				{
					var value = ht["value"];
					if (value is DateTime)
					{
						DateTimeValue = (DateTime?)value;
					}
					else if (value is double)
					{
						NumericValue = (double?)value;
					}
					else if (value is int)
					{
						NumericValue = (double?)(int)value;
					}

				}
				if (ht["gridLine"] != null)
				{
					this.GridLine = (bool)ht["gridLine"];
				}
				if (ht["gridLineStyle"] != null)
				{
					JsonRestoreHelper.RestoreStateFromJson(this.GridLineStyle, ht["gridLineStyle"]);
				}
			}
		}
#endif

		bool ICustomOptionType.IncludeQuotes
		{
			get { return false; }
		}

		#region Serialize

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal bool ShouldSerialize()
		{
			if (!String.IsNullOrEmpty(Text))
				return true;
			if (DateTimeValue.HasValue)
				return true;
			if (NumericValue.HasValue)
				return true;
			if (GridLine)
				return true;
			if (GridLineStyle.ShouldSerialize())
				return true;
			return false;
		}

		#endregion

	}
	
	/*
	internal class ValueLabelsConverter: TypeConverter
	{

		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(List<ValueLabel>) || sourceType == typeof(string);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			if (value is string)
			{
				string s = ((string)value).Trim();
				List<ValueLabel> items = new List<ValueLabel>();

				if (s.Length == 0)
				{
					return items;
				}
				string[] values = s.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
				foreach (string item in values)
				{
					ValueLabel label = new ValueLabel();
					label.Text = item;
					items.Add(label);
				}
			}
			return base.ConvertFrom(context, culture, value);
		}
	}
	 * */

	#endregion
}
