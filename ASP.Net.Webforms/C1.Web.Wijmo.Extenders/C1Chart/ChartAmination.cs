﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace C1.Web.Wijmo.Extenders.C1Chart
{
	/// <summary>
	/// Represents the ChartAnimation class which contains all settings for the animation.
	/// </summary>
	public partial class ChartAnimation : ICustomOptionType, IJsonEmptiable
	{
		string ICustomOptionType.SerializeValue()
		{
			StringBuilder serializeValue = new StringBuilder();
			serializeValue.Append("{");
			serializeValue.Append(GetOptionValue());
			serializeValue.Append("}");
			return serializeValue.ToString();
		}

		/// <summary>
		/// Serializes the current animation object to a string value.
		/// </summary>
		/// <returns>
		/// Returns the string value that serialized from the current animation object.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected virtual string GetOptionValue()
		{
			string optionValue = "";
			if (!this.Enabled)
			{
				optionValue += " enabled: false,";
			}
			else
			{
				optionValue += " enabled: true,";
			}
			optionValue += String.Format(" duration: {0},", this.Duration.ToString());
			optionValue += String.Format(" easing: '{0}',", this.Easing);
			//if (this.Duration != 400)
			//	optionValue += String.Format(" duration: {0},", this.Duration.ToString());
			//if (this.Easing != defaultEasing)
			//	optionValue += String.Format(" easing: '{0}',", this.Easing);
			if (!String.IsNullOrEmpty(optionValue))
				optionValue = optionValue.Substring(0, optionValue.Length - 1);
			return optionValue;
		}

		bool ICustomOptionType.IncludeQuotes
		{
			get { return false; }
		}

		#region Serialize

		/// <summary>
		/// Returns a boolean value that indicates whether the chart animation
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a boolean value to determine whether this chart animation should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		internal protected virtual bool ShouldSerialize()
		{
			if (!Enabled)
				return true;
			if (Duration != 400)
				return true;
			if (Easing != defaultEasing)
				return true;
			return false;
		}

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}
		#endregion
	}
}
