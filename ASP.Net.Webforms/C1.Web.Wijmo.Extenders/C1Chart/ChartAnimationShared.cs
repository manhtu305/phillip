﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	/// <summary>
	/// Represents the ChartAnimation class which contains all settings for the animation.
	/// </summary>
	public partial class ChartAnimation : Settings
	{
		ChartEasing defaultEasing = ChartEasing.EaseInCubic;

		/// <summary>
		/// Initializes a new instance of the ChartAnimation class.
		/// </summary>
		public ChartAnimation()
		{
		}

		/// <summary>
		/// A value that determines whether to show animation.
		/// </summary>
		[NotifyParentProperty(true)]
		[C1Description("C1Chart.ChartAnimation.Enabled")]
		[DefaultValue(true)]
		[WidgetOption]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Enabled
		{
			get
			{
				return GetPropertyValue<bool>("Enabled", true);
			}
			set
			{
				SetPropertyValue<bool>("Enabled", value);
			}
		}

		/// <summary>
		/// A value that indicates the duration for the animation.
		/// </summary>
		[NotifyParentProperty(true)]
		[C1Description("C1Chart.ChartAnimation.Duration")]
		[DefaultValue(400)]
		[WidgetOption]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int Duration
		{
			get
			{
				return GetPropertyValue<int>("Duration", 400);
			}
			set
			{
				SetPropertyValue<int>("Duration", value);
			}
		}

		/*
		/// <summary>
		/// A value that indicates the easing for the animation.
		/// </summary>
		[NotifyParentProperty(true)]
		[Description("A value that indicates the easing for the animation.")]
		[DefaultValue(">")]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string Easing
		{
			get
			{
				return GetPropertyValue<string>("Easing", defaultEasing);
			}
			set
			{
				SetPropertyValue<string>("Easing", value);
			}
		}
		 * */

		/// <summary>
		/// A value that indicates the easing for the animation.
		/// </summary>
		[NotifyParentProperty(true)]
		[C1Description("C1Chart.ChartAnimation.Easing")]
		[DefaultValue(ChartEasing.EaseInCubic)]
		[WidgetOption]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public ChartEasing Easing
		{
			get
			{
				return GetPropertyValue<ChartEasing>("Easing", defaultEasing);
			}
			set
			{
				SetPropertyValue<ChartEasing>("Easing", value);
			}
		}
	}
}
