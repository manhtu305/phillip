﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	/// <summary>
	/// Use the members of this enumeration to set the value of the ChartSeries.Display property.
	/// </summary>
	public enum ChartSeriesDisplay
	{
		/// <summary>
		/// Show the chart series.
		/// </summary>
		Show = 0,
		/// <summary>
		/// Hide the chart series.
		/// </summary>
		Hide = 1,
		/// <summary>
		/// Exclude the chart series.
		/// </summary>
		Exclude = 2,
		/// <summary>
		/// The chart series exclude the hole.
		/// </summary>
		ExcludeHole = 3
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the ChartMarker.Type property.
	/// </summary>
	public enum MarkerType
	{
		/// <summary>
		/// Data point represented by a solid triangle.
		/// </summary>
		Tri = 0,
		/// <summary>
		/// Data point represented by a solid inverted triangle.
		/// </summary>
		InvertedTri = 1,
		/// <summary>
		/// Data point represented by a solid square. 
		/// </summary>
		Box = 2,
		/// <summary>
		/// Data point represented by a solid diamond.
		/// </summary>
		Diamond = 3,
		/// <summary>
		/// Data point represented by crossed horizontal and vertical lines. 
		/// </summary>
		Cross = 4,
		/// <summary>
		/// Data point represented by the outline of a circle.
		/// </summary>
		Circle = 5
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the ChartLegend.Orientation property.
	/// </summary>
	public enum ChartOrientation
	{
		/// <summary>
		/// Legend entries are stacked vertically as legend height permits.
		/// If sufficient height is not available, additional legend columns are created. 
		/// </summary>
		Horizontal = 0,
		/// <summary>
		/// Legend entries are added side by side as (horizontally) as legend width permits.
		/// If sufficient width is not available, additional legend rows are created. 
		/// </summary>
		Vertical = 1
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the ChartAxis.Compass property.
	/// </summary>
	public enum ChartCompass
	{
		/// <summary>
		/// Object is positioned north or above.
		/// </summary>
		North = 0,
		/// <summary>
		/// Object is positioned east or to the right side.
		/// </summary>
		East = 1,
		/// <summary>
		/// Object is positioned south or below.
		/// </summary>
		South = 2,
		/// <summary>
		/// Object is positioned west or to the left side.
		/// </summary>
		West = 3
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the ChartLabel.Compass property.
	/// </summary>
	public enum ChartLabelCompass
	{
		/// <summary>
		/// Places label to the north or above the data point.
		/// </summary>
		North = 0,
		/// <summary>
		/// Places label to the NorthEast or above and right of the data point. 
		/// </summary>
		NorthEast = 1,
		/// <summary>
		/// Places label to the East or to the right of the data point.
		/// </summary>
		East = 2,
		/// <summary>
		/// Places label to the SouthEast or below and right of the data point.
		/// </summary>
		SouthEast = 3,
		/// <summary>
		/// Places label to the South or below the data point.
		/// </summary>
		South = 4,
		/// <summary>
		/// Places label to the SouthWest or below and left of the data point.
		/// </summary>
		SouthWest = 5,
		/// <summary>
		/// Places label to the West or left of the data point. 
		/// </summary>
		West = 6,
		/// <summary>
		/// Places label to the NorthWest or above and left of the data point.
		/// </summary>
		NorthWest = 7
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the ChartAxis.Alignment property.
	/// </summary>
	public enum ChartAxisAlignment
	{
		/// <summary>
		/// Setting the alignment to Center centers the axis title in comparison to the chart area.
		/// </summary>
		Center = 0,
		/// <summary>
		/// Setting the alignment to Near places the axis title to the left side of the chart area.
		/// </summary>
		Near = 1,
		/// <summary>
		/// Setting the alignment to Far places the axis title to the right side of the chart area.
		/// </summary>
		Far = 2
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the ChartAxisTick.Position property.
	/// </summary>
	public enum ChartAxisTickPosition
	{
		/// <summary>
		/// No tick marks along the axis.
		/// </summary>
		None = 0,
		/// <summary>
		/// Tick marks located inside the chart area on the axis.
		/// </summary>
		Inside = 1,
		/// <summary>
		/// Tick marks located outside the chart area on the axis.
		/// </summary>
		Outside = 2,
		/// <summary>
		/// Tick marks cross over the axis.
		/// </summary>
		Cross = 3
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the ChartAxis.AnnoMethod property.
	/// </summary>
	public enum ChartAxisAnnoMethod
	{
		/// <summary>
		/// The axis will be annotated automatically by the chart.
		/// </summary>
		Values = 0,
		/// <summary>
		/// The axis will be annotated with values specified in the ValueLabelsCollection.
		/// </summary>
		ValueLabels = 1
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the ChartAnimation.Easing property.
	/// </summary>
	public enum ChartEasing
	{
		/// <summary>
		/// Cubic easing in. Begins at zero velocity and then accelerates.
		/// </summary>
		EaseInCubic = 0,
		/// <summary>
		/// Cubic easing in and out. Begins at full velocity and then decelerates to zero.
		/// </summary>
		EaseOutCubic = 1,
		/// <summary>
		/// Cubic easing in and out. Begins at zero velocity, accelerates until halfway,
		/// and then decelerates to zero velocity again.
		/// </summary>
		EaseInOutCubic = 2,
		/// <summary>
		/// Back easing in. Starts slowly and then accelerates.
		/// </summary>
		EaseInBack = 3,
		/// <summary>
		/// Back easing out. Begins quickly and then decelerates.
		/// </summary>
		EaseOutBack = 4,
		/// <summary>
		/// Quintic easing out. Begins at full velocity and then decelerates to zero.
		/// </summary>
		EaseOutElastic = 5,
		/// <summary>
		/// Bouncing easing out. Begins quickly and then decelerates.
		/// The number of bounces is related to the duration: longer durations produce more bounces.
		/// </summary>
		EaseOutBounce = 6
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the ChartStyleFill.Type property.
	/// </summary>
	public enum ChartStyleFillType
	{
		/// <summary>
		/// Applies the default chart style fill to the chart.
		/// </summary>
		Default = 0,
		/// <summary>
		/// Applies the linear gradient chart style fill to the chart.
		/// </summary>
		LinearGradient = 1,
		/// <summary>
		/// Applies the radial gradient style fill to the chart.
		/// </summary>
		RadialGradient = 2
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the C1CandlestickChart.Type property.
	/// </summary>
	public enum CandlestickChartType
	{
		/// <summary>
		/// Show the chart with candlestick type.
		/// </summary>
		Candlestick = 0,
		/// <summary>
		/// Show the chart with open-high-low-close type.
		/// </summary>
		Ohlc = 1,
		/// <summary>
		/// Show the chart with high-low type.
		/// </summary>
		Hl = 2
	}

    /// <summary>
    /// Use the members of this enumeration to set the value of the C1PieChart.Direction property.
    /// </summary>
    public enum PieChartDirection
    {
        /// <summary>
        /// Draws PieChart segments in counter-clockwise direction
        /// </summary>
        CounterClockwise = 0,
        /// <summary>
        /// Draws PieChart segments in clockwise direction
        /// </summary>
        Clockwise = 1,
    }
}
