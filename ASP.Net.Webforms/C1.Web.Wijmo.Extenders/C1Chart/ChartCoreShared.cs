﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Globalization;
using System.ComponentModel.Design;
using System.Drawing.Design;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Chart", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.Base;
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Chart", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	//[WidgetDependencies("globalize.min.js", "globalize.cultures.js" /*, "raphael-min.js"*/)]
	// fixed issue 24137, the eventcalendar's globalize is contains in wijmo.open.js, the dependence of 
	// eventcalendar is wijmo.open.js, not globalize.min.js, when the chart controls and eventcalendar control
	// is in one page, because the order of the globalize function, the extend culture globalize object will be overided.
	[WidgetDependencies(typeof(WijChartCore))]
#if EXTENDER
	public partial class C1ChartCoreExtender<Tseries, TAnimation> : WidgetExtenderControlBase
		where Tseries : ChartSeries
		where TAnimation : ChartAnimation, new()
#else
	public abstract partial class C1ChartCore<Tseries, TAnimation, TBinding> : C1TargetDataBoundControlBase, ICultureControl
		where Tseries : ChartSeries, new()
		where TAnimation : ChartAnimation, new()
		where TBinding : C1ChartBinding, new()
#endif
	{
		List<Tseries> _seriesList = null;
		List<ChartStyle> _seriesStyle = null;
		List<ChartStyle> _seriesHoverStyle = null;
		List<AnnotationBase> _annotations = null;

        #region methods

        private void InitChart()
		{
			this.TextStyle = new ChartStyle();
			this.Header = new ChartTitle();
			this.Footer = new ChartTitle();
			this.Footer.Compass = ChartCompass.South;
			this.Footer.Visible = false;
			this.Legend = new ChartLegend();
			this.Axis = new ChartAxes();
			this.Hint = new ChartHint();
			this.ChartLabelStyle = new ChartStyle();
			this.Indicator = new ChartIndicator();
			this.Animation = new TAnimation();
		}

		// added in 2013-10-31, fixing the OADateTime issue, move this method from C1ChartCore to this file.
		/// <summary>
		/// Gets the series data of the specified series.
		/// </summary>
		/// <param name="series">
		/// The specified series.
		/// </param>
		/// <returns>
		/// Returns the series data which retrieved from the specified series.
		/// </returns>
		protected virtual ChartSeriesData GetSeriesData(Tseries series)
		{
            if (series.IsTrendline)
                return series.TrendlineSeries.Data;

			return new ChartSeriesData();
		}

        #endregion

        #region options
        /// <summary>
		/// A value that indicates whether to redraw the chart automatically when resizing the chart element.
		/// </summary>
		[C1Description("C1Chart.ChartCore.AutoResize")]
		[WidgetOption]
		[DefaultValue(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public virtual bool AutoResize
		{
			get
			{
				return GetPropertyValue<bool>("AutoResize", true);
			}
			set
			{
				SetPropertyValue<bool>("AutoResize", value);
			}
		}


		/// <summary>
		/// A value that indicates whether to show animation and the duration and easing for the animation.
		/// </summary>
		[C1Description("C1Chart.ChartCore.Animation")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
        public virtual TAnimation Animation { get; set; }

		/// <summary>
		/// An array collection that contains the data to be charted.
		/// </summary>
		[C1Description("C1Chart.ChartCore.SeriesList")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public virtual List<Tseries> SeriesList
		{
			get
			{
				if (_seriesList == null)
				{
					_seriesList = new List<Tseries>();
				}
				return _seriesList;
			}
		}

		/// <summary>
		/// An array collection that contains the style to be charted.
		/// </summary>
		[C1Description("C1Chart.ChartCore.SeriesStyles")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
		[CollectionItemType(typeof(ChartStyle))]
#endif
		public virtual List<ChartStyle> SeriesStyles
		{
			get
			{
				if (_seriesStyle == null)
				{
					_seriesStyle = new List<ChartStyle>();
				}
				return _seriesStyle;
			}
		}

		/// <summary>
		/// An array collection that contains the style to be charted when hovering the chart element.
		/// </summary>
		[C1Description("C1Chart.ChartCore.SeriesHoverStyles")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
		[CollectionItemType(typeof(ChartStyle))]
#endif
		public virtual List<ChartStyle> SeriesHoverStyles
		{
			get
			{
				if (_seriesHoverStyle == null)
				{
					_seriesHoverStyle = new List<ChartStyle>();
				}
				return _seriesHoverStyle;
			}
		}

		/// <summary>
		/// Gets or sets the culture information for the input control.
		/// </summary>
		[C1Description("C1Input.CultureInfo")]
		[C1Category("Category.Behavior")]
		//[DefaultValue(typeof(CultureInfo), "en-US")]
		[Json(true)]
		[TypeConverter(typeof(CultureInfoConverter))]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public CultureInfo Culture
		{
			get
			{
				return GetCulture(GetPropertyValue<CultureInfo>("Culture", null));
			}
			set
			{
				SetPropertyValue("Culture", value);
			}
		}

		/// <summary>
		/// A value that indicators the culture calendar to format the text. This property must work with Culture property.
		/// </summary>
		[C1Description("Culture.CultureCalendar")]
		[C1Category("Category.Appearance")]
		[DefaultValue("")]
		[WidgetOption]
		public string CultureCalendar
		{
			get
			{
				return GetPropertyValue<string>("CultureCalendar", "");
			}
			set
			{
				SetPropertyValue<string>("CultureCalendar", value);
			}
		}

		/// <summary>
		/// A value that indicates the top margin of the chart area.
		/// </summary>
		[C1Description("C1Chart.ChartCore.MarginTop")]
		[WidgetOption]
		[DefaultValue(25)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Sizes)]
#endif
        public virtual int MarginTop
		{
			get
			{
				return GetPropertyValue<int>("MarginTop", 25);
			}
			set
			{
				SetPropertyValue<int>("MarginTop", value);
			}
		}

		/// <summary>
		/// A value that indicates the right margin of the chart area.
		/// </summary>
		[C1Description("C1Chart.ChartCore.MarginRight")]
		[WidgetOption]
		[DefaultValue(25)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Sizes)]
#endif
        public virtual int MarginRight
		{
			get
			{
				return GetPropertyValue<int>("MarginRight", 25);
			}
			set
			{
				SetPropertyValue<int>("MarginRight", value);
			}
		}

		/// <summary>
		/// A value that indicates the bottom margin of the chart area.
		/// </summary>
		[C1Description("C1Chart.ChartCore.MarginBottom")]
		[WidgetOption]
		[DefaultValue(25)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Sizes)]
#endif
        public virtual int MarginBottom
		{
			get
			{
				return GetPropertyValue<int>("MarginBottom", 25);
			}
			set
			{
				SetPropertyValue<int>("MarginBottom", value);
			}
		}

		/// <summary>
		/// A value that indicates the left margin of the chart area.
		/// </summary>
		[C1Description("C1Chart.ChartCore.MarginLeft")]
		[WidgetOption]
		[DefaultValue(25)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Sizes)]
#endif
        public virtual int MarginLeft
		{
			get
			{
				return GetPropertyValue<int>("MarginLeft", 25);
			}
			set
			{
				SetPropertyValue<int>("MarginLeft", value);
			}
		}

		/// <summary>
		/// A value that indicates the style of the chart text.
		/// </summary>
		[C1Description("C1Chart.ChartCore.TextStyle")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
        public virtual ChartStyle TextStyle { get; set; }

		/// <summary>
		/// An object that value indicates the header of the chart element.
		/// </summary>
		[C1Description("C1Chart.ChartCore.Header")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
        public virtual ChartTitle Header { get; set; }

		/// <summary>
		/// An object value that indicates the footer of the chart element.
		/// </summary>
		[C1Description("C1Chart.ChartCore.Footer")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
        public virtual ChartTitle Footer { get; set; }

		/// <summary>
		/// An object value indicates the legend of the chart element.
		/// </summary>
		[C1Description("C1Chart.ChartCore.Legend")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
        public virtual ChartLegend Legend { get; set; }

		/// <summary>
		/// A value that provides information about the axes.
		/// </summary>
		[C1Description("C1Chart.ChartCore.Axis")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
        public virtual ChartAxes Axis { get; set; }

		/// <summary>
		/// A value that is used to indicate whether to show and what to show on the open tooltip.
		/// </summary>
		[C1Description("C1Chart.ChartCore.Hint")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
        public virtual ChartHint Hint { get; set; }

		/// <summary>
		/// A value that indicates whether to show default chart labels.
		/// </summary>
		[C1Description("C1Chart.ChartCore.ShowChartLabels")]
		[WidgetOption]
		[DefaultValue(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public virtual bool ShowChartLabels
		{
			get
			{
				return GetPropertyValue<bool>("ShowChartLabels", true);
			}
			set
			{
				SetPropertyValue<bool>("ShowChartLabels", value);
			}
		}

		/// <summary>
		/// A value that indicates style of the chart labels.
		/// </summary>
		[C1Description("C1Chart.ChartCore.ChartLabelStyle")]
		[WidgetOption]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public virtual ChartStyle ChartLabelStyle { get; set; }

		/// <summary>
		/// A value that indicates the format string of the chart labels.
		/// </summary>
		[C1Description("C1Chart.ChartCore.ChartLabelFormatString")]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public virtual string ChartLabelFormatString
		{
			get
			{
				return GetPropertyValue<string>("ChartLabelFormatString", "");
			}
			set
			{
				SetPropertyValue<string>("ChartLabelFormatString", value);
			}
		}

		/// <summary>
		/// A value that indicates whether to disable the default text style.
		/// </summary>
		[C1Description("C1Chart.ChartCore.DisableDefaultTextStyle")]
		[WidgetOption]
		[DefaultValue(false)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
        public virtual bool DisableDefaultTextStyle
		{
			get
			{
				return GetPropertyValue<bool>("DisableDefaultTextStyle", false);
			}
			set
			{
				SetPropertyValue<bool>("DisableDefaultTextStyle", value);
			}
		}

		/// <summary>
		/// A value that indicates whether to show shadow for the chart.
		/// </summary>
		[C1Description("C1Chart.ChartCore.Shadow")]
		[WidgetOption]
		[DefaultValue(true)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
        public virtual bool Shadow
		{
			get
			{
				return GetPropertyValue<bool>("Shadow", true);
			}
			set
			{
				SetPropertyValue<bool>("Shadow", value);
			}
		}

		/// <summary>
		/// A value that provides information about the indicator.
		/// </summary>
		[C1Description("C1Chart.ChartCore.Indicator")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public ChartIndicator Indicator
		{
			get;
			set;
		}

		/// <summary>
		/// An array collection that contains the annotations.
		/// </summary>
		[C1Description("C1Chart.ChartCore.Annotations")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
		[C1Category("Category.Data")]
		[Editor(typeof(AnnotationCollectionEditor), typeof(UITypeEditor))]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public virtual List<AnnotationBase> Annotations
		{
			get
			{
				if (_annotations == null)
				{
					_annotations = new List<AnnotationBase>();
				}
				return _annotations;
			}
		}

		#region Events

		/// <summary>
		/// Occurs when the user clicks a mouse button.
		/// </summary>
		[C1Description("C1Chart.ChartCore.OnClientMouseDown")]
		[C1Category("Category.ClientSideEvents")]
		[DefaultValue("")]
		[WidgetEvent("e, data")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientMouseDown
		{
			get
			{
				return GetPropertyValue<string>("OnClientMouseDown", String.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientMouseDown", value);
			}
		}

		/// <summary>
		/// Occurs when the user releases a mouse button while the pointer is over the chart element.
		/// </summary>
		[C1Description("C1Chart.ChartCore.OnClientMouseUp")]
		[C1Category("Category.ClientSideEvents")]
		[DefaultValue("")]
		[WidgetEvent("e, data")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientMouseUp
		{
			get
			{
				return GetPropertyValue<string>("OnClientMouseUp", String.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientMouseUp", value);
			}
		}

		/// <summary>
		/// Occurs when the user first places the pointer over the chart element.
		/// </summary>
		[C1Description("C1Chart.ChartCore.OnClientMouseOver")]
		[C1Category("Category.ClientSideEvents")]
		[DefaultValue("")]
		[WidgetEvent("e, data")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientMouseOver
		{
			get
			{
				return GetPropertyValue<string>("OnClientMouseOver", String.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientMouseOver", value);
			}
		}

		/// <summary>
		/// Occurs when the user moves the pointer off of the chart element.
		/// </summary>
		[C1Description("C1Chart.ChartCore.OnClientMouseOut")]
		[C1Category("Category.ClientSideEvents")]
		[DefaultValue("")]
		[WidgetEvent("e, data")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientMouseOut
		{
			get
			{
				return GetPropertyValue<string>("OnClientMouseOut", String.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientMouseOut", value);
			}
		}

		/// <summary>
		/// Occurs when the user moves the mouse pointer while it is over a chart element.
		/// </summary>
		[C1Description("C1Chart.ChartCore.OnClientMouseMove")]
		[C1Category("Category.ClientSideEvents")]
		[DefaultValue("")]
		[WidgetEvent("e, data")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientMouseMove
		{
			get
			{
				return GetPropertyValue<string>("OnClientMouseMove", String.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientMouseMove", value);
			}
		}

		/// <summary>
		/// Occurs when the user clicks the chart element. 
		/// </summary>
		[C1Description("C1Chart.ChartCore.OnClientClick")]
		[C1Category("Category.ClientSideEvents")]
		[DefaultValue("")]
		[WidgetEvent("e, data")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientClick
		{
			get
			{
				return GetPropertyValue<string>("OnClientClick", String.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientClick", value);
			}
		}

		/// <summary>
		/// Occurs before the series changes.  This event can be cancelled. 
		/// </summary>
		[C1Description("C1Chart.ChartCore.OnClientBeforeSeriesChange")]
		[C1Category("Category.ClientSideEvents")]
		[DefaultValue("")]
		[WidgetEvent("e, data")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientBeforeSeriesChange
		{
			get
			{
				return GetPropertyValue<string>("OnClientBeforeSeriesChange", String.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientBeforeSeriesChange", value);
			}
		}

		/// <summary>
		///  Occurs when the series changes. 
		/// </summary>
		[C1Description("C1Chart.ChartCore.OnClientSeriesChanged")]
		[C1Category("Category.ClientSideEvents")]
		[DefaultValue("")]
		[WidgetEvent("e, data")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientSeriesChanged
		{
			get
			{
				return GetPropertyValue<string>("OnClientSeriesChanged", String.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientSeriesChanged", value);
			}
		}

		/// <summary>
		/// Occurs before the canvas is painted.  This event can be cancelled.
		/// </summary>
		[C1Description("C1Chart.ChartCore.OnClientBeforePaint")]
		[C1Category("Category.ClientSideEvents")]
		[DefaultValue("")]
		[WidgetEvent("e")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientBeforePaint
		{
			get
			{
				return GetPropertyValue<string>("OnClientBeforePaint", String.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientBeforePaint", value);
			}
		}

		/// <summary>
		/// Occurs after the canvas is painted.
		/// </summary>
		[C1Description("C1Chart.ChartCore.OnClientPainted")]
		[C1Category("Category.ClientSideEvents")]
		[DefaultValue("")]
		[WidgetEvent("e")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientPainted
		{
			get
			{
				return GetPropertyValue<string>("OnClientPainted", String.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientPainted", value);
			}
		}

		#endregion end of Events

		#region ** hidden properties
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override string Theme
		{
			get
			{
				return string.Empty;
			}
		}
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public override string ThemeSwatch
		{
			get
			{
				return base.ThemeSwatch;
			}
			set
			{
				base.ThemeSwatch = value;
			}
		}
		#endregion end of ** hidden properties.
		#endregion end of options

		#region Serialize

		/// <summary>
		/// Determine whether the SeriesList property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if SeriesList has values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeSeriesList()
		{
			return SeriesList.Count > 0;
		}

		/// <summary>
		/// Determine whether the SeriesStyles property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if SeriesStyles has values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual bool ShouldSerializeSeriesStyles()
		{
			return SeriesStyles.Count > 0;
		}

		/// <summary>
		/// Determine whether the SeriesHoverStyles property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if SeriesHoverStyles has values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual bool ShouldSerializeSeriesHoverStyles()
		{
			return SeriesHoverStyles.Count > 0;
		}

		/// <summary>
		/// Determine whether the Annotations property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if Annotations has values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeAnnotations()
		{
			return Annotations.Count > 0;
		}

		#endregion 

		#region handle the OADate issue

		protected const double UnitRate = 24 * 60 * 60 * 1000;

		// before render, check the axis, if need convert the OADate, convert it.

		protected override void Render(HtmlTextWriter writer)
		{
			this.ConvertAxisOADate();
			base.Render(writer);
		}
		/// <summary>
		/// When axis is dateTime, because the ToOADate method return value's unit is day, Here we should convert it to millisecond.
		/// </summary>
		protected virtual void ConvertAxisOADate()
		{
			if (this.SeriesList.Count > 0)
			{
				if (IsDateTimeSeries("x"))
				{
					ConvertAxisByOADate(this.Axis.X);
				}
				if (IsDateTimeSeries("y"))
				{
					ConvertAxisByOADate(this.Axis.Y);
				}
			}
		}

		/// <summary>
		/// Convert the Max and Min property for axis when the axis is datetime.
		/// </summary>
		/// <param name="axis"></param>
		protected void ConvertAxisByOADate(ChartAxis axis)
		{
			if (axis.Min != null && NeedConvertOADate((double)axis.Min))
			{
				axis.Min *= UnitRate;
			}

			if (axis.Max != null && NeedConvertOADate((double)axis.Max))
			{
				axis.Max *= UnitRate;
			}

			// Convert the UnitMajor and UnitMinor for axis when the axis is datetime. 
			// Here just need to check the unitmajor/unitminor's unit is day or millisecond
			if (axis.UnitMajor != 0 && NeedConvertOADate(axis.UnitMajor))
			{
				axis.UnitMajor *= UnitRate;
			}
			if (axis.UnitMinor != 0 && NeedConvertOADate(axis.UnitMinor))
			{
				axis.UnitMinor *= UnitRate;
			}
		}

		// get the first data of the series to check the data is datetime type.
		protected bool IsDateTimeSeries(string axisType)
		{
			if (this.SeriesList.Count > 0)
			{
				Tseries series = this.SeriesList[0];
				
				ChartSeriesData data = GetSeriesData(series);
				if (data != null)
				{
					List<ChartYData> values = GetDataFromSeriesByAxis(data, axisType);
					if (values.Count() > 0) 
					{
						return values.First().DateTimeValue != null;
					}
				}
			}
			return false;
		}

		private List<ChartYData> GetDataFromSeriesByAxis(ChartSeriesData data, string axisType)
		{
			// convert the List<ChartXData> to List<ChartYData>. In bellow .net 3.5, it can't convert the type automatic. 
			//	Becasue the ChartXData inherit from ChartYData,  The X data contains StringValues for string type in series x data.
			// Here I just need the Datetime value.  So I use chartYData instead of ChartXData to avoid write repeat codes.
			List<ChartYData> d = new List<ChartYData>();
			if (axisType == "x")
			{
				foreach (ChartXData xData in data.X.Values)
				{
					d.Add(xData);
				}
				return d;
			}
			else {
				return data.Y.Values;
			}
		}

		protected bool NeedConvertOADate(double value) 
		{
			// Here just need to compare to the max DateTime's OAData. if the value is toOAData, the 1d convert to millisecond is large than the max date's OADate.
			//if the value is too large, here consider whether the axis's max and min's unit is converted millisecond
			double maxOADate = DateTime.MaxValue.ToOADate();
			return value < maxOADate;
		}

		#endregion
	}

#if WIJMOCONTROLS
	[SmartAssembly.Attributes.DoNotObfuscate]
#endif
	internal class AnnotationCollectionEditor: CollectionEditor
	{
		public AnnotationCollectionEditor(Type type):base(type)
		{
		}

		protected override bool CanSelectMultipleInstances()
		{
 			 return true;
		}

		protected override Type[] CreateNewItemTypes()
		{
			return new Type[] { typeof(CircleAnnotation), typeof(EllipseAnnotation), typeof(ImageAnnotation),
				typeof(LineAnnotation), typeof(PolygonAnnotation), typeof(RectAnnotation),
				typeof(SquareAnnotation), typeof(TextAnnotation)};
		}

		protected override object CreateInstance(Type itemType)
		{
			if (itemType == typeof(TextAnnotation))
			{
				return new TextAnnotation();
			}
			if (itemType == typeof(CircleAnnotation))
			{
				return new CircleAnnotation();
			}
			if (itemType == typeof(SquareAnnotation))
			{
				return new SquareAnnotation();
			}
			if (itemType == typeof(RectAnnotation))
			{
				return new RectAnnotation();
			}
			if (itemType == typeof(EllipseAnnotation))
			{
				return new EllipseAnnotation();
			}
			if (itemType == typeof(LineAnnotation))
			{
				return new LineAnnotation();
			}
			if (itemType == typeof(PolygonAnnotation))
			{
				return new PolygonAnnotation();
			}
			if (itemType == typeof(ImageAnnotation))
			{
				return new ImageAnnotation();
			}
			return null;
		}
	}
}
