﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using C1.Web.Wijmo.Extenders.Localization;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace C1.Web.Wijmo.Extenders.C1Chart
{
	/// <summary>
	/// Extender for the chart widget.
	/// </summary>
	//[TargetControlType(typeof(Control))]
	[TargetControlType(typeof(Panel))]
	[TargetControlType(typeof(Table))]
	[ToolboxItem(false)]
	public partial class C1ChartCoreExtender<Tseries, TAnimation> : WidgetExtenderControlBase
		where Tseries : ChartSeries
		where TAnimation : ChartAnimation, new()
	{

		public C1ChartCoreExtender()
		{
			this.InitChart();
		}

		/// <summary>
		/// A value that indicates the width of chart.
		/// </summary>
		[C1Description("C1Chart.ChartCore.Width")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		[DefaultValue(null)]
		public int? Width
		{
			get
			{
				return GetPropertyValue<int?>("Width", null);
			}
			set
			{
				SetPropertyValue<int?>("Width", value);
			}
		}

		/// <summary>
		/// A value that indicates the height of chart.
		/// </summary>
		[C1Description("C1Chart.ChartCore.Height")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		[DefaultValue(null)]
		public int? Height
		{
			get
			{
				return GetPropertyValue<int?>("Height", null);
			}
			set
			{
				SetPropertyValue<int?>("Height", value);
			}
		}

        /// <summary>
        /// When overridden in a derived class, registers the WidgetDescriptor objects for the control.
        /// </summary>
        /// <param name="targetControl"></param>
        /// <returns></returns>
        protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
        {
            IEnumerable<ScriptDescriptor> sds = base.GetScriptDescriptors(targetControl);

            foreach (WidgetDescriptor wsd in sds)
            {
                wsd.Options = wsd.Options.Replace("\"trendlineSeries\":", "");
            }

            return sds;
        }
	}
}
