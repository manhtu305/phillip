﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Text.RegularExpressions;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	/// <summary>
	/// Represents the ChartStyle class which contains all of the settings for the chart style.
	/// </summary>
	public class ChartStyle: Settings, IJsonEmptiable
	{

		/// <summary>
		/// Initializes a new instance of the ChartStyle class.
		/// </summary>
		public ChartStyle()
		{
			this.Fill = new ChartStyleFill();
		}

		#region options

		/// <summary>
		/// A value that indicates the clip-rect of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.ClipRect")]
		[NotifyParentProperty(true)]
		[WidgetOptionName("clip-rect")]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public string ClipRect
		{
			get
			{
				return this.GetPropertyValue<string>("ClipRect", "");
			}
			set
			{
				this.SetPropertyValue<string>("ClipRect", value);
			}
		}

		/// <summary>
		/// A value that indicates the cursor of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Cursor")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public string Cursor
		{
			get
			{
				return this.GetPropertyValue<string>("Cursor", "");
			}
			set
			{
				this.SetPropertyValue<string>("Cursor", value);
			}
		}

		/// <summary>
		/// A value that indicates the cx point of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Cx")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public double? Cx
		{
			get
			{
				return this.GetPropertyValue<double?>("Cx", null);
			}
			set
			{
				this.SetPropertyValue<double?>("Cx", value);
			}
		}

		/// <summary>
		/// A value that indicates the cy point of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Cy")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public double? Cy
		{
			get
			{
				return this.GetPropertyValue<double?>("Cy", null);
			}
			set
			{
				this.SetPropertyValue<double?>("Cy", value);
			}
		}

		/// <summary>
		/// A value that indicates the filled color of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Fill")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
		//public string Fill { get; set; }
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyleFill Fill { get; set; }

		/// <summary>
		/// A value that indicates the fill opacity of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.FillOpacity")]
		[NotifyParentProperty(true)]
		[WidgetOptionName("fill-opacity")]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public double? FillOpacity
		{
			get
			{
				return this.GetPropertyValue<double?>("FillOpacity", null);
			}
			set
			{
				this.SetPropertyValue<double?>("FillOpacity", value);
			}
		}

		/// <summary>
		/// A value that indicates the font family of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.FontFamily")]
		[NotifyParentProperty(true)]
		[WidgetOptionName("font-family")]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public string FontFamily
		{
			get
			{
				return this.GetPropertyValue<string>("FontFamily", "");
			}
			set
			{
				this.SetPropertyValue<string>("FontFamily", value);
			}
		}

		/// <summary>
		/// A value that indicates the font size of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.FontSize")]
		[NotifyParentProperty(true)]
		[WidgetOptionName("font-size")]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public string FontSize
		{
			get
			{
				return this.GetPropertyValue<string>("FontSize", "");
			}
			set
			{
				this.SetPropertyValue<string>("FontSize", value);
			}
		}

		/// <summary>
		/// A value that indicates the font weight of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.FontWeight")]
		[NotifyParentProperty(true)]
		[WidgetOptionName("font-weight")]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public string FontWeight
		{
			get
			{
				return this.GetPropertyValue<string>("FontWeight", "");
			}
			set
			{
				this.SetPropertyValue<string>("FontWeight", value);
			}
		}

		/// <summary>
		/// A value that indicates the height of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Height")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public int? Height
		{
			get
			{
				return this.GetPropertyValue<int?>("Height", null);
			}
			set
			{
				this.SetPropertyValue<int?>("Height", value);
			}
		}

		/// <summary>
		/// A value that indicates the href of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Href")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public string Href
		{
			get
			{
				return this.GetPropertyValue<string>("Href", "");
			}
			set
			{
				this.SetPropertyValue<string>("Href", value);
			}
		}

		/// <summary>
		/// A value that indicates the opacity of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Opacity")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public double? Opacity
		{
			get
			{
				return this.GetPropertyValue<double?>("Opacity", null);
			}
			set
			{
				this.SetPropertyValue<double?>("Opacity", value);
			}
		}

		/// <summary>
		/// A value that indicates the path of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Path")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public string Path
		{
			get
			{
				return this.GetPropertyValue<string>("Path", "");
			}
			set
			{
				this.SetPropertyValue<string>("Path", value);
			}
		}

		/// <summary>
		/// A value that indicates the r length of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.R")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public double? R
		{
			get
			{
				return this.GetPropertyValue<double?>("R", null);
			}
			set
			{
				this.SetPropertyValue<double?>("R", value);
			}
		}

		/// <summary>
		/// A value that indicates the rotation of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Rotation")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public double? Rotation
		{
			get
			{
				return this.GetPropertyValue<double?>("Rotation", null);
			}
			set
			{
				this.SetPropertyValue<double?>("Rotation", value);
			}
		}

		/// <summary>
		/// A value that indicates the rx point of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Rx")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public double? Rx
		{
			get
			{
				return this.GetPropertyValue<double?>("Rx", null);
			}
			set
			{
				this.SetPropertyValue<double?>("Rx", value);
			}
		}

		/// <summary>
		/// A value that indicates the ry point of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Ry")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public double? Ry
		{
			get
			{
				return this.GetPropertyValue<double?>("Ry", null);
			}
			set
			{
				this.SetPropertyValue<double?>("Ry", value);
			}
		}

		/// <summary>
		/// A value that indicates the scale of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Scale")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public string Scale
		{
			get
			{
				return this.GetPropertyValue<string>("Scale", "");
			}
			set
			{
				this.SetPropertyValue<string>("Scale", value);
			}
		}

		/// <summary>
		/// A value that indicates the src of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Src")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public string Src
		{
			get
			{
				return this.GetPropertyValue<string>("Src", "");
			}
			set
			{
				this.SetPropertyValue<string>("Src", value);
			}
		}

		/// <summary>
		/// A value that indicates the stroke color of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Stroke")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(typeof(Color), "")]
		[TypeConverter(typeof(WebColorConverter))]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public Color Stroke
		{
			get
			{
				return this.GetPropertyValue<Color>("Stroke", Color.Empty);
			}
			set
			{
				this.SetPropertyValue<Color>("Stroke", value);
			}
		}

		/// <summary>
		/// A value that indicates the stroke dash array of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.StrokeDashArray")]
		[NotifyParentProperty(true)]
		[WidgetOptionName("stroke-dasharray")]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public string StrokeDashArray
		{
			get
			{
				return this.GetPropertyValue<string>("StrokeDashArray", "");
			}
			set
			{
				this.SetPropertyValue<string>("StrokeDashArray", value);
			}
		}

		/// <summary>
		/// A value that indicates the stroke linecap of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.StrokeLineCap")]
		[NotifyParentProperty(true)]
		[WidgetOptionName("stroke-linecap")]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public string StrokeLineCap
		{
			get
			{
				return this.GetPropertyValue<string>("StrokeLineCap", "");
			}
			set
			{
				this.SetPropertyValue<string>("StrokeLineCap", value);
			}
		}

		/// <summary>
		/// A value that indicates the stroke line join of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.StrokeLineJoin")]
		[NotifyParentProperty(true)]
		[WidgetOptionName("stroke-linejoin")]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public string StrokeLineJoin
		{
			get
			{
				return this.GetPropertyValue<string>("StrokeLineJoin", "");
			}
			set
			{
				this.SetPropertyValue<string>("StrokeLineJoin", value);
			}
		}

		/// <summary>
		/// A value that indicates the stroke miter limit of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.StrokeMiterLimit")]
		[NotifyParentProperty(true)]
		[WidgetOptionName("stroke-miterlimit")]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public double? StrokeMiterLimit
		{
			get
			{
				return this.GetPropertyValue<double?>("StrokeMiterLimit", null);
			}
			set
			{
				this.SetPropertyValue<double?>("StrokeMiterLimit", value);
			}
		}

		/// <summary>
		/// A value that indicates the stroke opacity of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.StrokeOpacity")]
		[NotifyParentProperty(true)]
		[WidgetOptionName("stroke-opacity")]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public double? StrokeOpacity
		{
			get
			{
				return this.GetPropertyValue<double?>("StrokeOpacity", null);
			}
			set
			{
				this.SetPropertyValue<double?>("StrokeOpacity", value);
			}
		}

		/// <summary>
		/// A value that indicates the stroke width of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.StrokeWidth")]
		[NotifyParentProperty(true)]
		[WidgetOptionName("stroke-width")]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public double? StrokeWidth
		{
			get
			{
				return this.GetPropertyValue<double?>("StrokeWidth", null);
			}
			set
			{
				this.SetPropertyValue<double?>("StrokeWidth", value);
			}
		}

		/// <summary>
		/// A value that indicates the target of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Target")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public string Target
		{
			get
			{
				return this.GetPropertyValue<string>("Target", "");
			}
			set
			{
				this.SetPropertyValue<string>("Target", value);
			}
		}

		/// <summary>
		/// A value that indicates the text anchor of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.TextAnchor")]
		[NotifyParentProperty(true)]
		[WidgetOptionName("text-anchor")]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public string TextAnchor
		{
			get
			{
				return this.GetPropertyValue<string>("TextAnchor", "");
			}
			set
			{
				this.SetPropertyValue<string>("TextAnchor", value);
			}
		}

		/// <summary>
		/// A value that indicates the title of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Title")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public string Title
		{
			get
			{
				return this.GetPropertyValue<string>("Title", "");
			}
			set
			{
				this.SetPropertyValue<string>("Title", value);
			}
		}

		/// <summary>
		/// A value that indicates the translation of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Translation")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public string Translation
		{
			get
			{
				return this.GetPropertyValue<string>("Translation", "");
			}
			set
			{
				this.SetPropertyValue<string>("Translation", value);
			}
		}

		/// <summary>
		/// A value that indicates the width of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Width")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public int? Width
		{
			get
			{
				return this.GetPropertyValue<int?>("Width", null);
			}
			set
			{
				this.SetPropertyValue<int?>("Width", value);
			}
		}

		/// <summary>
		/// A value that indicates the x point of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.X")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public double? X
		{
			get
			{
				return this.GetPropertyValue<double?>("X", null);
			}
			set
			{
				this.SetPropertyValue<double?>("X", value);
			}
		}

		/// <summary>
		/// A value that indicates the y point of the chart style.
		/// </summary>
		[C1Description("C1Chart.ChartStyle.Y")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public double? Y
		{
			get
			{
				return this.GetPropertyValue<double?>("Y", null);
			}
			set
			{
				this.SetPropertyValue<double?>("Y", value);
			}
		}

		#endregion

		#region Serialize

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal bool ShouldSerialize()
		{
			if (!String.IsNullOrEmpty(ClipRect))
				return true;
			if (!String.IsNullOrEmpty(Cursor))
				return true;
			if (Cx.HasValue)
				return true;
			if (Cy.HasValue)
				return true;
			//if (!String.IsNullOrEmpty(Fill))
			if(Fill.ShouldSerialize())
				return true;
			if (FillOpacity.HasValue)
				return true;
			//if (!String.IsNullOrEmpty(Font))
			//	return true;
			if (!String.IsNullOrEmpty(FontFamily))
				return true;
			if (!String.IsNullOrEmpty(FontSize))
				return true;
			if (!String.IsNullOrEmpty(FontWeight))
				return true;
			if (Height.HasValue)
				return true;
			if (!String.IsNullOrEmpty(Href))
				return true;
			if (Opacity.HasValue)
				return true;
			if (!String.IsNullOrEmpty(Path))
				return true;
			if (R.HasValue)
				return true;
			if (Rotation.HasValue)
				return true;
			if (Rx.HasValue)
				return true;
			if (Ry.HasValue)
				return true;
			if (!String.IsNullOrEmpty(Scale))
				return true;
			if (!String.IsNullOrEmpty(Src))
				return true;
			if (!Stroke.IsEmpty)
				return true;
			//if (!String.IsNullOrEmpty(Stroke))
			//	return true;
			if (!String.IsNullOrEmpty(StrokeDashArray))
				return true;
			if (!String.IsNullOrEmpty(StrokeLineCap))
				return true;
			if (!String.IsNullOrEmpty(StrokeLineJoin))
				return true;
			if (StrokeMiterLimit.HasValue)
				return true;
			if (StrokeOpacity.HasValue)
				return true;
			if (StrokeWidth.HasValue)
				return true;
			if (!String.IsNullOrEmpty(Target))
				return true;
			if (!String.IsNullOrEmpty(TextAnchor))
				return true;
			if (!String.IsNullOrEmpty(Title))
				return true;
			if (!String.IsNullOrEmpty(Translation))
				return true;
			if (Width.HasValue)
				return true;
			if (X.HasValue)
				return true;
			if (Y.HasValue)
				return true;

			return false;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeFill()
		{
			return this.Fill.ShouldSerialize();
		}
		#endregion

		#region DeSerialize
		
#if !EXTENDER
		/// <summary>
		/// Deserialize the stroke from the saved state.
		/// </summary>
		/// <param name="value">
		/// An object that contains the saved state values for the chart control.
		/// </param>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void C1DeserializeStroke(object value)
		{
			var val = value.ToString();
			if (val == "none")
			{
				Stroke = Color.Empty;
			}
			else
			{
				Stroke = C1WebColorConverter.ToColor(value.ToString());
			}
		}
#endif

		#endregion
	}

	#region ChartStyleFill

	/// <summary>
	/// Represents the ChartStyleFill class which contains all of the settings for Fill property of chart style.
	/// </summary>
	[Serializable]
	public class ChartStyleFill : Settings, ICustomOptionType, IJsonEmptiable
	{

		private List<ColorMiddle> _colorMiddles = null;

		/// <summary>
		/// Initializes a new instance of the CharStyleFill class.
		/// </summary>
		public ChartStyleFill()
		{
		}

		#region Properties
		/// <summary>
		/// A value that indicates the type of ChartStyleFill.
		/// </summary>
		[C1Description("C1Chart.ChartStyleFill.Type")]
		[DefaultValue(ChartStyleFillType.Default)]
		[NotifyParentProperty(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public ChartStyleFillType Type
		{
			get
			{
				return this.GetPropertyValue<ChartStyleFillType>("Type", ChartStyleFillType.Default);
			}
			set
			{
				this.SetPropertyValue<ChartStyleFillType>("Type", value);
			}
		}

		/// <summary>
		/// A value that indicates the color of ChartStyleFill.
		/// This property works only when Type is Default.
		/// </summary>
		[C1Description("C1Chart.ChartStyleFill.Color")]
		[DefaultValue(typeof(Color), "")]
		[TypeConverter(typeof(WebColorConverter))]
		[NotifyParentProperty(true)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public Color Color
		{
			get
			{
				return this.GetPropertyValue<Color>("Color", Color.Empty);
			}
			set
			{
				this.SetPropertyValue<Color>("Color", value);
			}
		}

		/// <summary>
		/// A value that indicates the begin color of ChartStyleFill.
		/// This property works when Type is LinearGradient or RadialGradient.
		/// </summary>
		[C1Description("C1Chart.ChartStyleFill.ColorBegin")]
		[DefaultValue(typeof(Color), "")]
		[TypeConverter(typeof(WebColorConverter))]
		[NotifyParentProperty(true)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public Color ColorBegin
		{
			get
			{
				return this.GetPropertyValue<Color>("ColorBegin", Color.Empty);
			}
			set
			{
				this.SetPropertyValue<Color>("ColorBegin", value);
			}
		}

		/// <summary>
		/// A collection value that indicates the middle colors of ChartStyleFill.
		/// This property works when Type is LinearGradient or RadialGradient.
		/// </summary>
		[C1Description("C1Chart.ChartStyleFill.ColorMiddles")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[NotifyParentProperty(true)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public List<ColorMiddle> ColorMiddles 
		{
			get
			{
				if (_colorMiddles == null)
				{
					_colorMiddles = new List<ColorMiddle>();
				}

				return _colorMiddles;
			}
		}

		/// <summary>
		/// A value that indicates the end color of ChartStyleFill.
		/// This property works when Type is LinearGradient or RadialGradient.
		/// </summary>
		[C1Description("C1Chart.ChartStyleFill.ColorEnd")]
		[DefaultValue(typeof(Color), "")]
		[TypeConverter(typeof(WebColorConverter))]
		[NotifyParentProperty(true)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public Color ColorEnd
		{
			get
			{
				return this.GetPropertyValue<Color>("ColorEnd", Color.Empty);
			}
			set
			{
				this.SetPropertyValue<Color>("ColorEnd", value);
			}
		}

		/// <summary>
		/// A value that indicates the angle of linear gradient of ChartStyleFill.
		/// This property works only when Type is LinearGradient.
		/// </summary>
		[C1Description("C1Chart.ChartStyleFill.LinearGradientAngle")]
		[DefaultValue(0.0)]
		[NotifyParentProperty(true)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public double LinearGradientAngle
		{
			get { return this.GetPropertyValue<double>("LinearGradientAngle", 0.0); }
			set
			{
				if (value > 360 || value < 0)
				{
					throw new ArgumentOutOfRangeException("angle coordinates are in 0..360 range");
				}
				this.SetPropertyValue<double>("LinearGradientAngle", value);
			}
		}

		/// <summary>
		/// A value that indicates the x value of radial gradient focus point of ChartStyleFill.
		/// This property works only when Type is RadialGradient.
		/// </summary>
		[C1Description("C1Chart.ChartStyleFill.RadialGradientFocusPointX")]
		[DefaultValue(-1.0)]
		[NotifyParentProperty(true)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public double RadialGradientFocusPointX
		{
			get { return this.GetPropertyValue<double>("RadialGradientFocusPointX", -1.0); }
			set
			{
				if (value > 1 || value < 0)
				{
					throw new ArgumentOutOfRangeException("Focus point coordinates are in 0..1 range");
				}
				this.SetPropertyValue<double>("RadialGradientFocusPointX", value);
			}
		}

		/// <summary>
		/// A value that indicates the y value of radial gradient focus point of ChartStyleFill.
		/// This property works only when Type is RadialGradient.
		/// </summary>
		[C1Description("C1Chart.ChartStyleFill.RadialGradientFocusPointY")]
		[DefaultValue(-1.0)]
		[NotifyParentProperty(true)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public double RadialGradientFocusPointY
		{
			get { return this.GetPropertyValue<double>("RadialGradientFocusPointY", -1.0); }
			set
			{
				if (value > 1 || value < 0)
				{
					throw new ArgumentOutOfRangeException("Focus point coordinates are in 0..1 range");
				}
				this.SetPropertyValue<double>("RadialGradientFocusPointY", value);
			}
		}

		#endregion

		string ICustomOptionType.SerializeValue()
		{
			return GetSerializeColor();
		}

#if !EXTENDER
		//TODO:
		void ICustomOptionType.DeSerializeValue(object state)
		{
			if(state==null)
				return;
			var value = state.ToString();
			if (value == "none")
				return;
			ChartStyleFill fill = RegExp(value);
			this.CopyFrom(fill);
		}
#endif

		bool ICustomOptionType.IncludeQuotes
		{
			get { return true; }
		}

		private string GetSerializeColor()
		{
			if (this.Type == ChartStyleFillType.Default)
			{
				if (Color.IsEmpty)
					return "";

				return ColorTranslator.ToHtml(Color);
			}
			else
			{
				string color = ColorTranslator.ToHtml(ColorBegin);
				if (ColorMiddles.Count > 0)
				{
					foreach (ColorMiddle colorMiddle in ColorMiddles)
					{
						color += String.Format("-{0}:{1}", ColorTranslator.ToHtml(colorMiddle.Color), colorMiddle.Offset.ToString());
					}
				}
				color += "-" + ColorTranslator.ToHtml(ColorEnd);
				if (this.Type == ChartStyleFillType.LinearGradient)
				{
					color = LinearGradientAngle.ToString() + "-" + color;
				}
				else
				{
					if (this.RadialGradientFocusPointX >= 0 && this.RadialGradientFocusPointY >= 0)
					{
						color = String.Format("({0},{1})", this.RadialGradientFocusPointX.ToString(),
							this.RadialGradientFocusPointY.ToString()) + color;
					}
					color = "r" + color;
				}
				return color;
			}
		}

		#region Serialize

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal bool ShouldSerialize()
		{
			string color = GetSerializeColor();
			return !String.IsNullOrEmpty(color);
		}
		#endregion

		private static ChartStyleFill RegExp(string rawStr)
		{
			string angelHeader = @"^(\d{1,3})\-";
			string radialHeader = @"^r(?:\(([01]|(?:0\.\d+))\,([01]|(?:0\.\d+))\))*";
			string colorBegin = @"(?:((?:[a-zA-Z]+)|(?:\#[0-9a-fA-F]{6})|(?:\#[0-9a-fA-F]{3}))\-)";
			string colorMiddle = @"(?:(?:((?:(?:[a-zA-Z]+)|(?:\#[0-9a-fA-F]{6})|(?:\#[0-9a-fA-F]{3}))(?:\:\d{1,3})?)\-)?)*";
			string regColorEnd = @"((?:[a-zA-Z]+)|(?:\#[0-9a-fA-F]{6})|(?:\#[0-9a-fA-F]{3}))$";
			string regColor = @"^((?:[a-zA-Z]+)|(?:\#[0-9a-fA-F]{6})|(?:\#[0-9a-fA-F]{3}))$";

			Regex regexColor = new Regex(regColor);
			Regex regRadial =
				new Regex(string.Format("{0}{1}{2}{3}",
					radialHeader, colorBegin,
					colorMiddle, regColorEnd));
			Regex regAngle =
				new Regex(string.Format("{0}{1}{2}{3}",
					angelHeader, colorBegin,
					colorMiddle, regColorEnd));


			ChartStyleFill fill = new ChartStyleFill();

			if (regexColor.IsMatch(rawStr))
			{
				Match match = regexColor.Match(rawStr);
				fill.Color = Color.FromName(match.Value);
				fill.Type = ChartStyleFillType.Default;
			}
			else if (regAngle.IsMatch(rawStr))
			{
				Match match = regAngle.Match(rawStr);

				int angel = Convert.ToInt32(match.Groups[1].Value);
				string beginColor = match.Groups[2].Value;
				string endColor = match.Groups[4].Value;
				fill.LinearGradientAngle = Convert.ToDouble(angel);
				fill.ColorBegin = Color.FromName(beginColor);
				fill.ColorEnd = Color.FromName(endColor);

				ApplyToFill(match.Groups[3], ref fill);

				fill.Type = ChartStyleFillType.LinearGradient;
			}
			else if (regRadial.IsMatch(rawStr))
			{
				Match match = regRadial.Match(rawStr);
				double fx = -1;
				if (Double.TryParse(match.Groups[1].Value, out fx))
				{
					fill.RadialGradientFocusPointX = fx;
				}
				double fy = -1;
				if (Double.TryParse(match.Groups[2].Value, out fy))
				{
					fill.RadialGradientFocusPointY = fy;
				}

				string beginColor = match.Groups[3].Value;
				string endColor = match.Groups[5].Value;
				fill.ColorBegin = Color.FromName(beginColor);
				fill.ColorEnd = Color.FromName(endColor);
				ApplyToFill(match.Groups[4], ref fill);

				fill.Type = ChartStyleFillType.RadialGradient;
			}
			else
			{
				throw new Exception("String format is not correct!");
			}
			return fill;
		}

		private static ChartStyleFill ApplyToFill(Group match, ref ChartStyleFill fill)
		{
			string regColorMiddleItem = @"^((?:[a-zA-Z]+)|(?:\#[0-9a-fA-F]{0,6}))(?:\:(\d{1,3}))?$";
			Regex regColorMiddle = new Regex(regColorMiddleItem);

			foreach (Capture capture in match.Captures)
			{
				string temp = capture.Value;

				if (regColorMiddle.IsMatch(temp))
				{
					Match middle = regColorMiddle.Match(temp);

					fill.ColorMiddles.Add(new ColorMiddle
					{
						Color = Color.FromName(middle.Groups[1].Value),
						Offset = string.IsNullOrEmpty(middle.Groups[2].Value) ? 0 : Convert.ToDouble(middle.Groups[2].Value)
					});

				}
			}
			return fill;
		}
	}
	

	/// <summary>
	/// Represents the ColorMiddle class which contains all settings for color middle.
	/// </summary>
	public class ColorMiddle : Settings
	{

		/// <summary>
		/// Initializes a new instance of the ColorMiddle class.
		/// </summary>
		public ColorMiddle()
		{
		}

		/// <summary>
		/// A value that indicates the color of ColorMiddle.
		/// </summary>
		[C1Description("C1Chart.ColorMiddle.Color")]
		[DefaultValue(typeof(Color), "")]
		[TypeConverter(typeof(WebColorConverter))]
		[NotifyParentProperty(true)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public Color Color
		{
			get
			{
				return this.GetPropertyValue<Color>("Color", Color.Empty);
			}
			set
			{
				this.SetPropertyValue<Color>("Color", value);
			}
		}

		/// <summary>
		/// A value that indicates the offset of ColorMiddle.
		/// </summary>
		[C1Description("C1Chart.ColorMiddle.Offset")]
		[DefaultValue(0.0)]
		[NotifyParentProperty(true)]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public double Offset
		{
			get
			{
				return this.GetPropertyValue<double>("Offset", 0.0);
			}
			set
			{
				this.SetPropertyValue<double>("Offset", value);
			}
		}
	}
	#endregion
}
