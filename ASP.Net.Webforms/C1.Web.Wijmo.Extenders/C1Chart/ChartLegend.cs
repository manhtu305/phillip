﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	#region ChartLegend
	/// <summary>
	/// Provides access to the customization of the legend for the chart.
	/// </summary>
	public class ChartLegend: Settings, IJsonEmptiable
	{

		/// <summary>
		/// Initializes a new instance of the ChartLegend class.
		/// </summary>
		public ChartLegend()
		{
			this.TextMargin = new ChartTextMargin();
			this.LegendStyle = new ChartStyle();
			this.TextStyle = new ChartStyle();
			this.TitleStyle = new ChartStyle();
			this.Compass = ChartCompass.East;
			this.Orientation = ChartOrientation.Vertical;
			this.Visible = true;
			this.Size = new ChartLegendSize();
		}

		#region options

		/// <summary>
		/// A value that indicates the text of the legend.
		/// </summary>
		[C1Description("C1Chart.ChartLegend.Text")]
		[NotifyParentProperty(true)]
		[DefaultValue("")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string Text 
		{ 
			get
			{
				return this.GetPropertyValue<string>("Text", "");
			}
			set
			{
				this.SetPropertyValue<string>("Text", value);
			}
		}

		/// <summary>
		/// A value that indicates the text margin of the legend item.
		/// </summary>
		[C1Description("C1Chart.ChartLegend.TextMargin")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public ChartTextMargin TextMargin { get; set; }
		
		/// <summary>
		/// A value that indicates the style of the legend.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use the LegendStyle property instead.")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public ChartStyle Style
		{
			get
			{
				return this.LegendStyle;
			}
			set
			{
				this.LegendStyle = value;
			}
		}

		/// <summary>
		/// A value that indicates the style of the legend.
		/// </summary>
		[C1Description("C1Chart.ChartLegend.Style")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[WidgetOptionName("style")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle LegendStyle { get; set; }

		/// <summary>
		/// A value that indicates the width of the legend text.
		/// </summary>
		[C1Description("C1Chart.ChartLegend.TextWidth")]
		[NotifyParentProperty(true)]
		[DefaultValue(null)]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int? TextWidth
		{
			get
			{
				return this.GetPropertyValue<int?>("TextWidth", null);
			}
			set
			{
				this.SetPropertyValue<int?>("TextWidth", value);
			}
		}

		/// <summary>
		/// A value that indicates the style of the legend text.
		/// </summary>
		[C1Description("C1Chart.ChartLegend.TextStyle")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle TextStyle { get; set; }

		/// <summary>
		/// A value that indicates the style of the legend title.
		/// </summary>
		[C1Description("C1Chart.ChartLegend.TitleStyle")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle TitleStyle { get; set; }

		/// <summary>
		/// A value that indicates the compass of the legend.
		/// </summary>
		[C1Description("C1Chart.ChartLegend.Compass")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(ChartCompass.East)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public ChartCompass Compass 
		{ 
			get
			{
				return this.GetPropertyValue<ChartCompass>("Compass", ChartCompass.East);
			}
			set
			{
				this.SetPropertyValue<ChartCompass>("Compass", value);
			}
		}

		/// <summary>
		/// A value that indicates the orientation of the legend.
		/// </summary>
		[C1Description("C1Chart.ChartLegend.Orientation")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(ChartOrientation.Vertical)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public ChartOrientation Orientation 
		{ 
			get
			{
				return this.GetPropertyValue<ChartOrientation>("Orientation", ChartOrientation.Vertical);
			}
			set
			{
				this.SetPropertyValue<ChartOrientation>("Orientation", value);
			}
		}

		/// <summary>
		/// A value that indicates the visibility of the legend.
		/// </summary>
		[C1Description("C1Chart.ChartLegend.Visible")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Visible 
		{ 
			get
			{
				return this.GetPropertyValue<bool>("Visible", true);
			}
			set
			{
				this.SetPropertyValue<bool>("Visible", value);
			}
		}

		/// <summary>
		/// A value that indicates whether to reverse the order of the legend icons.
		/// </summary>
		[C1Description("C1Chart.ChartLegend.Reversed")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(false)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Reversed 
		{
			get 
			{
				return this.GetPropertyValue<bool>("Reversed", false);
			}
			set
			{
				this.SetPropertyValue<bool>("Reversed", value);
			}
		}

		/// <summary>
		/// A value that indicates the size of the legend icon.
		/// </summary>
		[C1Description("C1Chart.ChartLegend.Size")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Sizes)]
#endif
		public ChartLegendSize Size { get; set; }

		#endregion end of options

		#region Serialize

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal bool ShouldSerialize()
		{
			if (!String.IsNullOrEmpty(Text))
				return true;
			if (TextMargin.ShouldSerialize())
				return true;
			if (LegendStyle.ShouldSerialize())
				return true;
			if (TextStyle.ShouldSerialize())
				return true;
			if (TitleStyle.ShouldSerialize())
				return true;
			if (Compass != ChartCompass.East)
				return true;
			if (Orientation != ChartOrientation.Vertical)
				return true;
			if (!Visible)
				return true;
			if (Size.ShouldSerialize())
				return true;
			if (TextWidth.HasValue)
				return true;
			if (Reversed) {
				return true;
			}
			return false;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeStyle()
		{
			return false;
		}
		#endregion
	} 
	#endregion

	#region ChartTextMargin

	/// <summary>
	/// Represents the ChartTextMargin class which contains all of the settings for the text margin.
	/// </summary>
	public class ChartTextMargin: Settings, IJsonEmptiable
	{

		/// <summary>
		/// Initializes a new instance of the ChartTextMargin class.
		/// </summary>
		public ChartTextMargin()
		{
			this.Left = 2;
			this.Top = 2;
			this.Right = 2;
			this.Bottom = 2;
		}

		#region options

		/// <summary>
		/// A value that indicates the left text margin of the legend item.
		/// </summary>
		[C1Description("C1Chart.ChartTextMargin.Left")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(2)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Left 
		{
			get
			{
				return this.GetPropertyValue<int>("Left", 2);
			}
			set
			{
				this.SetPropertyValue<int>("Left", value);
			}
		}

		/// <summary>
		/// A value that indicates the top text margin of the legend item.
		/// </summary>
		[C1Description("C1Chart.ChartTextMargin.Top")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(2)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Top 
		{ 
			get
			{
				return this.GetPropertyValue<int>("Top", 2);
			}
			set
			{
				this.SetPropertyValue<int>("Top", value);
			}
		}

		/// <summary>
		/// A value that indicates the right text margin of the legend item.
		/// </summary>
		[C1Description("C1Chart.ChartTextMargin.Right")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(2)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Right 
		{ 
			get
			{
				return this.GetPropertyValue<int>("Right", 2);
			}
			set
			{
				this.SetPropertyValue<int>("Right", value);
			}
		}

		/// <summary>
		/// A value that indicates the bottom text margin of the legend item.
		/// </summary>
		[C1Description("C1Chart.ChartTextMargin.Bottom")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(2)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Bottom 
		{ 
			get
			{
				return this.GetPropertyValue<int>("Bottom", 2);
			}
			set
			{
				this.SetPropertyValue<int>("Bottom", value);
			}
		}

		#endregion

		#region Serialize

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal bool ShouldSerialize()
		{
			if (Left != 2)
				return true;
			if (Top != 2)
				return true;
			if (Right != 2)
				return true;
			if (Bottom != 2)
				return true;
			return false;
		}
		#endregion
	}
	#endregion

	#region ChartLegendSize

	/// <summary>
	/// Represents the ChartLegendSize class which contains all settings for the legend size.
	/// </summary>
	public class ChartLegendSize: Settings, IJsonEmptiable
	{
		/// <summary>
		/// Initializes a new instance of the ChartLegendSize class.
		/// </summary>
		public ChartLegendSize()
		{
		}

		#region options

		/// <summary>
		/// A value that indicates the width of the legend icon size.
		/// </summary>
		[C1Description("C1Chart.ChartLegendSize.Width")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(22)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Sizes)]
#endif
		public int Width
		{
			get
			{
				return this.GetPropertyValue<int>("Width", 22);
			}
			set
			{
				this.SetPropertyValue<int>("Width", value);
			}
		}

		/// <summary>
		/// A value that indicates the height of the legend icon size.
		/// </summary>
		[C1Description("C1Chart.ChartLegendSize.Height")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(10)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Sizes)]
#endif
		public int Height
		{
			get
			{
				return this.GetPropertyValue<int>("Height", 10);
			}
			set
			{
				this.SetPropertyValue<int>("Height", value);
			}
		}

		#endregion

		#region Serialize

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal bool ShouldSerialize()
		{
			if (Width != 22)
				return true;
			if (Height != 10)
				return true;
			return false;
		}
		#endregion
	}  
	#endregion
}
