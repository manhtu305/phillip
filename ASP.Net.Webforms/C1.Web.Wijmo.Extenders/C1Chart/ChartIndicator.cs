﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	/// <summary>
	/// Represents the ChartIndicator class which contains all settings for the chart indicator.
	/// </summary>
	public class ChartIndicator : Settings, IJsonEmptiable
	{
		public ChartIndicator()
		{
			this.IndicatorStyle = new ChartStyle();
		}

		/// <summary>
		/// A value that indicates the style of the indicator line.
		/// </summary>
		[C1Description("C1Chart.ChartIndicator.Style")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[WidgetOptionName("style")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle IndicatorStyle
		{
			get 
			{
				return GetPropertyValue<ChartStyle>("IndicatorStyle", null);
			}
			set
			{
				SetPropertyValue<ChartStyle>("IndicatorStyle", value);
			}
		}

		/// <summary>
		/// A value specifies whether to show the indicator line.
		/// </summary>
		[C1Description("C1Chart.ChartIndicator.Visible")]
		[WidgetOption]
		[NotifyParentProperty(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Visible
		{
			get 
			{
				return GetPropertyValue<bool>("Visible", false);
			}
			set 
			{
				SetPropertyValue<bool>("Visible", value);
			}
		}

		internal bool ShouldSerialize() 
		{
			return this.IndicatorStyle.ShouldSerialize() || this.Visible;
		}

		bool IJsonEmptiable.IsEmpty
		{
			get { return !this.ShouldSerialize(); }
		}
	}
}
