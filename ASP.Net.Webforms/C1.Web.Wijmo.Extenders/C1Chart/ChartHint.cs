﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	/// <summary>
	/// Represents the ChartHint class which contains all of the settings for the chart hint.
	/// </summary>
	public class ChartHint: Settings, IJsonEmptiable
	{
		private ContentOption _content;
		private ContentOption _title;

		/// <summary>
		/// Initializes a new instance of the ChartHint class.
		/// </summary>
		public ChartHint()
		{
			this.ContentStyle = new ChartStyle();
			this.TitleStyle = new ChartStyle();
			this.CalloutFilledStyle = new ChartStyle();
			this.HintStyle = new ChartStyle();
		}

		#region options

		/// <summary>
		/// A value that indicates whether to show the tooltip.
		/// </summary>
		[C1Description("C1Chart.ChartHint.Enable")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Enable
		{
			get
			{
				return this.GetPropertyValue<bool>("Enable", true);
			}
			set
			{
				this.SetPropertyValue<bool>("Enable", value);
			}
		}

		/// <summary>
		/// A value that will be shown in the content part of the tooltip.
		/// </summary>
		[C1Description("C1Chart.ChartHint.Content")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public ContentOption Content
		{
			get
			{
				if (_content == null)
				{
					_content = new ContentOption();
				}
				return _content;
				//return this.GetPropertyValue<string>("Content", "");
			}
			set
			{
				_content = value;
				//this.SetPropertyValue<string>("Content", value);
			}
		}

		/// <summary>
		/// A value that indicates the style of content text.
		/// </summary>
		[C1Description("C1Chart.ChartHint.ContentStyle")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle ContentStyle { get; set; }

		/// <summary>
		/// A value that will be shown in the title part of the tooltip.
		/// </summary>
		[C1Description("C1Chart.ChartHint.Title")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public ContentOption Title
		{
			get
			{
				if (_title == null)
				{
					_title = new ContentOption();
				}
				return _title;
				//return this.GetPropertyValue<string>("Title", "");
			}
			set
			{
				_title = value;
				//this.SetPropertyValue<string>("Title", value);
			}
		}

		/// <summary>
		/// A value that indicates the style of title text.
		/// </summary>
		[C1Description("C1Chart.ChartHint.TitleStyle")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle TitleStyle { get; set; }

		/// <summary>
		/// A value that indicates the style of hint.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Use the HintStyle property instead.")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public ChartStyle Style
		{
			get
			{
				return this.HintStyle;
			}
			set
			{
				this.HintStyle = value;
			}
		}

		/// <summary>
		/// A value that indicates the style of hint.
		/// </summary>
		[C1Description("C1Chart.ChartHint.Style")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[WidgetOptionName("style")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle HintStyle { get; set; }

		/// <summary>
		/// A value that indicates the effect during show or hide when showAnimated or hideAnimated isn't specified.
		/// </summary>
		[C1Description("C1Chart.ChartHint.Animated")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("fade")]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string Animated
		{
			get
			{
				return this.GetPropertyValue<string>("Animated", "fade");
			}
			set
			{
				this.SetPropertyValue<string>("Animated", value);
			}
		}

		/// <summary>
		/// A value that indicates the effect during show.
		/// </summary>
		[C1Description("C1Chart.ChartHint.ShowAnimated")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string ShowAnimated
		{
			get
			{
				return this.GetPropertyValue<string>("ShowAnimated", "");
			}
			set
			{
				this.SetPropertyValue<string>("ShowAnimated", value);
			}
		}

		/// <summary>
		/// A value that indicates the effect during hide.
		/// </summary>
		[C1Description("C1Chart.ChartHint.HideAnimated")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string HideAnimated
		{
			get
			{
				return this.GetPropertyValue<string>("HideAnimated", "");
			}
			set
			{
				this.SetPropertyValue<string>("HideAnimated", value);
			}
		}

		/// <summary>
		/// A value that indicates the millisecond to show or hide the tooltip when showDuration or hideDuration isn't specified.
		/// </summary>
		[C1Description("C1Chart.ChartHint.Duration")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(120)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int Duration
		{
			get
			{
				return this.GetPropertyValue<int>("Duration", 120);
			}
			set
			{
				this.SetPropertyValue<int>("Duration", value);
			}
		}

		/// <summary>
		/// A value that indicates the millisecond to show the tooltip.
		/// </summary>
		[C1Description("C1Chart.ChartHint.ShowDuration")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int? ShowDuration
		{
			get
			{
				return this.GetPropertyValue<int?>("ShowDuration", null);
			}
			set
			{
				this.SetPropertyValue<int?>("ShowDuration", value);
			}
		}

		/// <summary>
		/// A value that indicates the millisecond to hide the tooltip.
		/// </summary>
		[C1Description("C1Chart.ChartHint.HideDuration")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int? HideDuration
		{
			get
			{
				return this.GetPropertyValue<int?>("HideDuration", null);
			}
			set
			{
				this.SetPropertyValue<int?>("HideDuration", value);
			}
		}

		/// <summary>
		/// A value that indicates the easing during show or hide when showEasing or hideEasing isn't specified. 
		/// </summary>
		[C1Description("C1Chart.ChartHint.Easing")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string Easing
		{
			get
			{
				return this.GetPropertyValue<string>("Easing", "");
			}
			set
			{
				this.SetPropertyValue<string>("Easing", value);
			}
		}

		/// <summary>
		/// A value that indicates the easing during show. 
		/// </summary>
		[C1Description("C1Chart.ChartHint.ShowEasing")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string ShowEasing
		{
			get
			{
				return this.GetPropertyValue<string>("ShowEasing", "");
			}
			set
			{
				this.SetPropertyValue<string>("ShowEasing", value);
			}
		}

		/// <summary>
		/// A value that indicates the easing during hide. 
		/// </summary>
		[C1Description("C1Chart.ChartHint.HideEasing")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string HideEasing
		{
			get
			{
				return this.GetPropertyValue<string>("HideEasing", "");
			}
			set
			{
				this.SetPropertyValue<string>("HideEasing", value);
			}
		}

		/// <summary>
		/// A value that indicates the millisecond delay to show the tooltip.
		/// </summary>
		[C1Description("C1Chart.ChartHint.ShowDelay")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(0)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int ShowDelay
		{
			get
			{
				return this.GetPropertyValue<int>("ShowDelay", 0);
			}
			set
			{
				this.SetPropertyValue<int>("ShowDelay", value);
			}
		}

		/// <summary>
		/// A value that indicates the millisecond delay to hide the tooltip.
		/// </summary>
		[C1Description("C1Chart.ChartHint.HideDelay")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(150)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int HideDelay
		{
			get
			{
				return this.GetPropertyValue<int>("HideDelay", 150);
			}
			set
			{
				this.SetPropertyValue<int>("HideDelay", value);
			}
		}

		/// <summary>
		/// A value that indicates the compass of the tooltip.
		/// </summary>
		[C1Description("C1Chart.ChartHint.Compass")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(ChartLabelCompass.North)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public ChartLabelCompass Compass
		{
			get
			{
				return this.GetPropertyValue<ChartLabelCompass>("Compass", ChartLabelCompass.North);
			}
			set
			{
				this.SetPropertyValue<ChartLabelCompass>("Compass", value);
			}
		}

		/// <summary>
		/// A value that indicates the horizontal offset of the point to show the tooltip.
		/// </summary>
		[C1Description("C1Chart.ChartHint.OffsetX")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(0)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int OffsetX
		{
			get
			{
				return this.GetPropertyValue<int>("OffsetX", 0);
			}
			set
			{
				this.SetPropertyValue<int>("OffsetX", value);
			}
		}

		/// <summary>
		/// A value that indicates the vertical offset of the point to show the tooltip.
		/// </summary>
		[C1Description("C1Chart.ChartHint.OffsetY")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(0)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int OffsetY
		{
			get
			{
				return this.GetPropertyValue<int>("OffsetY", 0);
			}
			set
			{
				this.SetPropertyValue<int>("OffsetY", value);
			}
		}

		/// <summary>
		/// Determines whether to show the callout element.
		/// </summary>
		[C1Description("C1Chart.ChartHint.ShowCallout")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(true)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public bool ShowCallout
		{
			get
			{
				return this.GetPropertyValue<bool>("ShowCallout", true);
			}
			set
			{
				this.SetPropertyValue<bool>("ShowCallout", value);
			}
		}

		/// <summary>
		/// Determines whether to fill the callout. If true, then the callout triangle will be filled.
		/// </summary>
		[C1Description("C1Chart.ChartHint.CalloutFilled")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(false)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public bool CalloutFilled
		{
			get
			{
				return this.GetPropertyValue<bool>("CalloutFilled", false);
			}
			set
			{
				this.SetPropertyValue<bool>("CalloutFilled", value);
			}
		}

		/// <summary>
		/// A value that indicates the style of the callout filled.
		/// </summary>
		[C1Description("C1Chart.ChartHint.CalloutFilledStyle")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle CalloutFilledStyle { get; set; }

        /// <summary>
        /// A value to determine whether to use a tooltip with html style.
        /// </summary>
        [C1Description("C1Chart.ChartHint.IsContentHtml")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue(false)]
        [C1Category("Category.Appearance")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public bool IsContentHtml
        {
            get
            {
                return this.GetPropertyValue<bool>("IsContentHtml", false);
            }
            set
            {
                this.SetPropertyValue<bool>("IsContentHtml", value);
            }
        }

		#endregion end of options

		#region Serialize

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal bool ShouldSerialize()
		{
			if (!Enable)
				return true;
			if (!((IJsonEmptiable)Content).IsEmpty)
				return true;
			if (ContentStyle.ShouldSerialize())
				return true;
			if (!((IJsonEmptiable)Title).IsEmpty)
				return true;
			if (TitleStyle.ShouldSerialize())
				return true;
			if (HintStyle.ShouldSerialize())
				return true;
			if (Animated != "fade")
				return true;
			if (!String.IsNullOrEmpty(ShowAnimated))
				return true;
			if (!String.IsNullOrEmpty(HideAnimated))
				return true;
			if (Duration != 120)
				return true;
			if (ShowDuration.HasValue)
				return true;
			if (HideDuration.HasValue)
				return true;
			if (!String.IsNullOrEmpty(Easing))
				return true;
			if (!String.IsNullOrEmpty(ShowEasing))
				return true;
			if (!String.IsNullOrEmpty(HideEasing))
				return true;
			if (HideDelay != 150)
				return true;
			if (ShowDelay != 0)
				return true;
			if (Compass != ChartLabelCompass.North)
				return true;
			if (OffsetX != 0)
				return true;
			if (OffsetY != 0)
				return true;
			if (!ShowCallout)
				return true;
			if (CalloutFilled)
				return true;
            if (IsContentHtml)
                return true;
			if (CalloutFilledStyle.ShouldSerialize())
				return true;
			return false;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeStyle()
		{
			return false;
		}
		#endregion
	}
}
