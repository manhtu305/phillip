﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif


#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
    /// <summary>
    /// Define the settings of the jQuery ajax options.
    /// </summary>
    public class AjaxSettings : Settings
    {
        /// <summary>
        /// Determines whether to use asynchronous calls.
        /// If you need synchronous requests, set this option to false. 
        /// </summary>
        [C1Description("AjaxSettings.Async")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue(true)]
        public bool Async
        {
            get
            {
                return GetPropertyValue<bool>("Async", true);
            }
            set
            {
                SetPropertyValue<bool>("Async", value);
            }
        }

        /// <summary>
        /// If set to false, it will force requested pages not to be cached by the browser.
        /// </summary>
        [C1Description("AjaxSettings.Cache")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue(false)]
        public bool Cache
        {
            get
            {
                return GetPropertyValue<bool>("Cache", false);
            }
            set
            {
                SetPropertyValue<bool>("Cache", value);
            }
        }

        /// <summary>
        /// Whether to trigger global Ajax event handlers for this request. 
        /// </summary>
        [C1Description("AjaxSettings.Global")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue(true)]
        public bool Global
        {
            get
            {
                return GetPropertyValue<bool>("Global", true);
            }
            set
            {
                SetPropertyValue<bool>("Global", value);
            }
        }

        /// <summary>
        /// A username to be used in response to an HTTP access authentication request.
        /// </summary>
        [C1Description("AjaxSettings.UserName")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue("")]
        public string UserName
        {
            get
            {
                return GetPropertyValue<string>("UserName", "");
            }
            set
            {
                SetPropertyValue<string>("UserName", value);
            }
        }

        /// <summary>
        /// A password to be used in response to an HTTP access authentication request.
        /// </summary>
        [C1Description("AjaxSettings.Password")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue("")]
        public string Password
        {
            get
            {
                return GetPropertyValue<string>("Password", "");
            }
            set
            {
                SetPropertyValue<string>("Password", value);
            }
        }

        /// <summary>
        /// Set a local timeout (in milliseconds) for the request.
        /// </summary>
        [C1Description("AjaxSettings.Timeout")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue(0)]
        public int Timeout
        {
            get
            {
                return GetPropertyValue<int>("Timeout", 0);
            }
            set
            {
                SetPropertyValue<int>("Timeout", value);
            }
        }

        /// <summary>
        /// Set this to true if you wish to use the traditional style of param serialization.
        /// </summary>
        [C1Description("AjaxSettings.Traditional")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue(false)]
        public bool Traditional
        {
            get
            {
                return GetPropertyValue<bool>("Traditional", false);
            }
            set
            {
                SetPropertyValue<bool>("Traditional", value);
            }
        }
    }
}
