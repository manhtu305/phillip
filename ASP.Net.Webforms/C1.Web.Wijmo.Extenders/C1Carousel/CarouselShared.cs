﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using C1.Web.Wijmo;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Carousel", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Carousel
#else
using C1.Web.Wijmo.Controls.Localization;
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Carousel", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1Carousel
#endif
{
    [WidgetDependencies(
        typeof(WijCarousel)
#if !EXTENDER
, "extensions.c1carousel.js",
ResourcesConst.C1WRAPPER_PRO
#endif
)]
#if EXTENDER
	public partial class C1CarouselExtender
#else

    public partial class C1Carousel
#endif
    {

        private Animation _animation;
        private CarouselButtonStyle _prevBtnClass;
        private CarouselButtonStyle _nextBtnClass;
        private PositionSettings _pagerPosition;
        private PositionSettings _controlPosition;
        private CarouselThumbnails _thumbnails;

        #region ** options.

        /// <summary>
        /// When AutoPlay is true the pictures of carousel will be played automatically.
        /// </summary>
        [C1Description("C1Carousel.AutoPlay", "When AutoPlay is true the pictures of carousel will be played automatically.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [WidgetOptionName("auto")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool AutoPlay
        {
            get
            {
                return GetPropertyValue("AutoPlay", false);
            }
            set
            {
                SetPropertyValue("AutoPlay", value);
            }
        }

        /// <summary>
        /// Determines the time span in miliseconds between two pictures being shown in AutoPlay mode.
        /// </summary>
        [C1Description("C1Carousel.Interval", "Determines the time span between 2 pictures showing in autoPlay mode, In millisecond.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(5000)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int Interval
        {
            get
            {
                return GetPropertyValue("Interval", 5000);
            }
            set
            {
                SetPropertyValue("Interval", value);
            }
        }

        /// <summary>
        /// Determines if the carousel timer should be shown.
        /// </summary>
        [C1Description("C1Carousel.ShowTimer", "Determines if the timer of the carousel should be shown.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool ShowTimer
        {
            get
            {
                return GetPropertyValue("ShowTimer", false);
            }
            set
            {
                SetPropertyValue("ShowTimer", value);
            }
        }

        /// <summary>
        /// Gets or sets the position of navigation buttons.
        /// </summary>
        [C1Description("C1Carousel.ButtonPosition", "Determines position value for next button & previous button.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(ButtonPosition.Inside)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public ButtonPosition ButtonPosition
        {
            get
            {
                return GetPropertyValue("ButtonPosition", ButtonPosition.Inside);
            }
            set
            {
                SetPropertyValue("ButtonPosition", value);
            }
        }

        /// <summary>
        /// Determines whether the pager should be shown.
        /// </summary>
        [C1Description("C1Carousel.ShowPager", " Determines whether the pager should be shown.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool ShowPager
        {
            get
            {
                return GetPropertyValue("ShowPager", false);
            }
            set
            {
                SetPropertyValue("ShowPager", value);
            }
        }

        /// <summary>
        /// Allows carousel to loop back to the beginning. 
        /// </summary>
        [C1Description("C1Carousel.Loop", " Allows carousel to loop back to the beginning. ")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool Loop
        {
            get
            {
                return GetPropertyValue("Loop", true);
            }
            set
            {
                SetPropertyValue("Loop", value);
            }
        }

        /// <summary>
        /// Determines the custom start position of the image list in the carousel.
        /// </summary>
        [C1Description("C1Carousel.Start", "Determines the custom start position of the image list in wijcarousel.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(0)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int Start
        {
            get
            {
                return GetPropertyValue("Start", 0);
            }
            set
            {
                SetPropertyValue("Start", value);
            }
        }

        /// <summary>
        /// Determines how many images should be shown in the view area.
        /// </summary>
        [C1Description("C1Carousel.Display", "Determines how many images should be shown in the view area.")]
        [C1Category("Category.Appearance")]
        [DefaultValue(1)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int Display
        {
            get
            {
                return GetPropertyValue("Display", 1);
            }
            set
            {
                if (value <= 0)
                {
                    value = 1;
                }
                SetPropertyValue("Display", value);
            }
        }

        /// <summary>
        /// Determines how many images will be scrolled when you click the Next/Previous button.
        /// </summary>
        [C1Description("C1Carousel.Step", "Determines how many images will be scrolled when you click the Next/Previous button.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(1)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int Step
        {
            get
            {
                return GetPropertyValue("Step", 1);
            }
            set
            {
                SetPropertyValue("Step", value);
            }
        }

        /// <summary>
        /// Determines if a preview of the next item should be displayed in the carousel.
        /// </summary>
        [C1Description("C1Carousel.Preview", " Determines if we should preview the last & next pics.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool Preview
        {
            get
            {
                return GetPropertyValue("Preview", false);
            }
            set
            {
                SetPropertyValue("Preview", value);
            }
        }

        /// <summary>
        /// Determines whether the custom control should be shown.
        /// </summary>
        [C1Description("C1Carousel.ShowControls", " Determines whether the custom control should be shown.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool ShowControls
        {
            get
            {
                return GetPropertyValue("ShowControls", true);
            }
            set
            {
                SetPropertyValue("ShowControls", value);
            }
        }

        /// <summary>
        /// Determines whether the caption of items should be shown.
        /// </summary>
        [C1Description("C1Carousel.ShowCaption", "Determines whether the caption of items should be shown.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool ShowCaption
        {
            get
            {
                return GetPropertyValue("ShowCaption", true);
            }
            set
            {
                SetPropertyValue("ShowCaption", value);
            }
        }

        /// <summary>
        /// Determines whether the controls should be shown after created or hover on the DOM element.
        /// </summary>
        [C1Description("C1Carousel.ShowControlsOnHover", "Determines whether the controls should be shown after created or hover on the DOM element.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool ShowControlsOnHover
        {
            get
            {
                return GetPropertyValue("ShowControlsOnHover", false);
            }
            set
            {
                SetPropertyValue("ShowControlsOnHover", value);
            }
        }

        /// <summary>
        /// Determines whether the custom control should be shown.
        /// </summary>
        [C1Description("C1Carousel.Control", "Determines the innerHtml of the custom control.")]
        [C1Category("Category.Appearance")]
        [DefaultValue("")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string Control
        {
            get
            {
                return GetPropertyValue("Control", "");
            }
            set
            {
                SetPropertyValue("Control", value);
            }
        }

        /// <summary>
        /// Determines what type of pager to use in the carousel. 
        /// The ShowPager property must be set to True for the Pager to be displayed.
        /// </summary>
        [C1Description("C1Carousel.PagerType", "Determines the type of the pager in carousel.")]
        [C1Category("Category.Appearance")]
        [DefaultValue(PagerType.Numbers)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public PagerType PagerType
        {
            get
            {
                return GetPropertyValue("PagerType", PagerType.Numbers);
            }
            set
            {
                SetPropertyValue("PagerType", value);
            }
        }


        /// <summary>
        /// Determines the custom class of the Previous button.
        /// </summary>
        [C1Description("C1Carousel.PrevClass", "Determines the custom class of the previous button. ")]
        [NotifyParentProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [C1Category("Category.Appearance")]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
        [WidgetOptionName("prevBtnClass")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public CarouselButtonStyle PrevClass
        {
            get
            {
                if (this._nextBtnClass == null)
                {
                    this._nextBtnClass = new CarouselButtonStyle();
                }
                return this._nextBtnClass;
            }
            set
            {
                this._nextBtnClass = value;
            }
        }


        /// <summary>
        /// Determines the custom class of the Next button.
        /// </summary>
        [C1Description("C1Carousel.NextClass", "Determines the custom class of the next button.")]
        [NotifyParentProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [C1Category("Category.Appearance")]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
        [WidgetOptionName("nextBtnClass")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public CarouselButtonStyle NextClass
        {
            get
            {
                if (this._prevBtnClass == null)
                {
                    this._prevBtnClass = new CarouselButtonStyle();
                }
                return this._prevBtnClass;
            }
            set
            {
                this._prevBtnClass = value;
            }
        }

        /// <summary>
        /// Sets the carousel pager's position to the 'relativeTo', 'offsetX', and 'offsetY' properties. 
        /// For example, here is the jQuery UI position's position:{my:'top left',at:'right bottom',offset:}.
        /// </summary>
        [C1Description("C1Carousel.PagerPosition", "Sets the position of the pager in wijcarousel.")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public PositionSettings PagerPosition
        {
            get
            {
                if (this._pagerPosition == null)
                {
                    this._pagerPosition = new PositionSettings();
                }
                return this._pagerPosition;
            }
            set
            {
                this._pagerPosition = value;
            }
        }

        /// <summary>
        /// Sets the position of the controls in the carousel to the 'relativeTo', 'offsetX', and 'offsetY' properties. 
        /// For example, here is the jQuery UI position's position:{my:'top left',at:'right bottom',offset:}.
        /// </summary>
        [C1Description("C1Carousel.ControlPosition", "Sets the position of the controls in wijcarousel.")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public PositionSettings ControlPosition
        {
            get
            {
                if (this._controlPosition == null)
                {
                    this._controlPosition = new PositionSettings();
                }
                return this._controlPosition;
            }
            set
            {
                this._controlPosition = value;
            }
        }

        /// <summary>
        /// Controls the carousel's orientation. All images are vertical regardless of the 
        /// orientation of the carousel.
        /// </summary>
        [C1Description("C1Carousel.Orientation", "Controls the carousel's orientation.")]
        [WidgetOption]
        [C1Category("Category.Appearance")]
        [DefaultValue(Orientation.Horizontal)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public Orientation Orientation
        {
            get
            {
                return GetPropertyValue("Orientation", Orientation.Horizontal);
            }
            set
            {
                SetPropertyValue("Orientation", value);
            }
        }

        /// <summary>
        /// Determines the orientation of the slider pager.
        /// </summary>
        [C1Description("C1Carousel.SliderOrientation", "Determines the orientation of the slider pager.")]
        [WidgetOption]
        [C1Category("Category.Appearance")]
        [DefaultValue(Orientation.Horizontal)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public Orientation SliderOrientation
        {
            get
            {
                return GetPropertyValue("SliderOrientation", Orientation.Horizontal);
            }
            set
            {
                SetPropertyValue("SliderOrientation", value);
            }
        }


        /// <summary>
        /// A value determines the settings of the animation effect to be used when the wijcarousel is scrolling.
        /// </summary>
        [C1Description("C1Carousel.Animation", "A value determines the settings of the animation effect to be used when the wijcarousel is scrolling.")]
        [C1Category("Category.Appearance")]
        [NotifyParentProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public Animation Animation
        {
            get
            {
                if (this._animation == null)
                {
                    this._animation = new Animation();
                }
                return this._animation;
            }
            set
            {
                this._animation = value;
            }
        }

        /// <summary>
        /// Gets or sets the list of pager thumbnail images pager when The PagerType is set to Thumbnails.
        /// </summary>
        [C1Description("C1Carousel.Thumbnails", "Determines the thumbnails list for pager when pagerType is Thumbnails. ")]
        [C1Category("Category.Appearance")]
        [NotifyParentProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public CarouselThumbnails Thumbnails
        {
            get
            {
                if (this._thumbnails == null)
                {
                    this._thumbnails = new CarouselThumbnails();
                }
                return this._thumbnails;
            }
            set
            {
                this._thumbnails = value;
            }
        }
        //add option thumbnails

        #endregion end of ** options.

        #region ** client events


        /// <summary>
        /// The name of the function which will be called when the image is clicked.
        /// </summary>
        [C1Description("C1Carousel.OnClientItemClick", "The name of the function which will be called when the image is clicked.")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("ev, index")]
        [WidgetOptionName("itemClick")]
        [DefaultValue("")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientItemClick
        {
            get
            {
                return GetPropertyValue("OnClientItemClick", "");
            }
            set
            {
                SetPropertyValue("OnClientItemClick", value);
            }
        }

        /// <summary>
        /// The name of the function which will be called before scrolling to another image.
        /// </summary>
        [C1Description("C1Carousel.OnClientBeforeScroll", "The name of the function which will be called when before scroll to another image.")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("ev, index")]
        [WidgetOptionName("beforeScroll")]
        [DefaultValue("")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientBeforeScroll
        {
            get
            {
                return GetPropertyValue("OnClientBeforeScroll", "");
            }
            set
            {
                SetPropertyValue("OnClientBeforeScroll", value);
            }
        }

        /// <summary>
        /// The name of the function which will be called after scrolling to another image.
        /// </summary>
        [C1Description("C1Carousel.OnClientAfterScroll", "The name of the function which will be called after scroll to another image.")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("ev, index")]
        [WidgetOptionName("afterScroll")]
        [DefaultValue("")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientAfterScroll
        {
            get
            {
                return GetPropertyValue("OnClientAfterScroll", "");
            }
            set
            {
                SetPropertyValue("OnClientAfterScroll", value);
            }
        }

        /// <summary>
        /// The name of the function which will be called after the DOM element is created.
        /// </summary>
        [C1Description("C1Carousel.OnClientLoadCallback", "The name of the function which will be called after created the DOM element.")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("ev, index")]
        [WidgetOptionName("loadCallback")]
        [DefaultValue("")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientLoadCallback
        {
            get
            {
                return GetPropertyValue("OnClientLoadCallback", "");
            }
            set
            {
                SetPropertyValue("OnClientLoadCallback", value);
            }
        }

        #endregion end of ** client events.

        #region ** methods for serialization

        /// <summary>
        /// Determine whether the PagerPosition property should be serialized to client side.
        /// </summary>
        /// <returns>
        /// Returns true if any subproperty is changed, otherwise return false.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializePagerPosition()
        {
            return !(this.PagerPosition.My.Left == HPosition.Right &&
                this.PagerPosition.My.Top == VPosition.Top &&
                this.PagerPosition.At.Left == HPosition.Right &&
                this.PagerPosition.At.Top == VPosition.Bottom &&
                this.PagerPosition.Offset.Top == 0 &&
                this.PagerPosition.Offset.Left == 0);
        }

        #endregion end of ** methods for serialization.
    }
}
