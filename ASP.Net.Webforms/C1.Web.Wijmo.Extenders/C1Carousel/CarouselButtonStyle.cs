﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using C1.Web.Wijmo;
using System.ComponentModel;
using System.Web.UI;

#if !EXTENDER
using C1.Web.Wijmo.Controls;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Carousel
{
#else
using C1.Web.Wijmo.Extenders.Localization;

namespace C1.Web.Wijmo.Extenders.C1Carousel
{
#endif
    /// <summary>
    /// Determines the style of buttons in the carousel.
    /// </summary>
	public partial class CarouselButtonStyle: Settings, IJsonEmptiable
	{
		/// <summary>
		/// Sets default class of the button.
		/// </summary>
		[C1Description("C1Carousel.CarouselButtonStyle.DefaultClass", "Sets default class of the button.")]
        [C1Category("Category.Style")]
		[NotifyParentProperty(true)]
		[DefaultValue("")]
		[WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
		public string DefaultClass
		{
			get
			{
				return this.GetPropertyValue<string>("DefaultClass", "");
			}
			set
			{
				this.SetPropertyValue<string>("DefaultClass", value);
			}
		}

		/// <summary>
		/// Sets disable class of the button.
		/// </summary>
		[C1Description("C1Carousel.CarouselButtonStyle.DisableClass", "Sets disable class of the button.")]
        [C1Category("Category.Style")]
		[NotifyParentProperty(true)]
		[DefaultValue("")]
		[WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
		public string DisableClass
		{
			get
			{
				return this.GetPropertyValue<string>("DisableClass", "");
			}
			set
			{
				this.SetPropertyValue<string>("DisableClass", value);
			}
		}

		/// <summary>
		/// Sets hover class of the button.
		/// </summary>
		[C1Description("C1Carousel.CarouselButtonStyle.HoverClass", "Sets hover class of the button.")]
        [C1Category("Category.Style")]
		[NotifyParentProperty(true)]
		[DefaultValue("")]
		[WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
		public string HoverClass
		{
			get
			{
				return this.GetPropertyValue<string>("HoverClass", "");
			}
			set
			{
				this.SetPropertyValue<string>("HoverClass", value);
			}
		}

		#region IJsonEmptiable Members

		bool IJsonEmptiable.IsEmpty
		{
			get 
			{ 
				return string.IsNullOrEmpty(this.DefaultClass) 
				&& string.IsNullOrEmpty(this.HoverClass) 
				&& string.IsNullOrEmpty(this.DisableClass); 
			}
		}

		#endregion
	}
}
