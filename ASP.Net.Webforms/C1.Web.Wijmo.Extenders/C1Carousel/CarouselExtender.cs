﻿using System.ComponentModel;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using C1.Web.Wijmo;
using System;

[assembly: TagPrefix("C1.Web.Wijmo.Extenders.Carousel", "Carousel")]
namespace C1.Web.Wijmo.Extenders.C1Carousel
{
	/// <summary>
	/// Carousel Extender.
	/// </summary>
	[TargetControlType(typeof(Panel))]
	[ToolboxBitmap(typeof(C1CarouselExtender), "C1Carousel.png")]
	[LicenseProviderAttribute()]
    [ToolboxItem(true)]
    public partial class C1CarouselExtender : WidgetExtenderControlBase
	{
		private bool _productLicensed = false;
		public C1CarouselExtender()
		{
			VerifyLicense();
            this.PagerPosition.My.Left = HPosition.Right;
            this.PagerPosition.My.Top = VPosition.Top;
            this.PagerPosition.At.Left = HPosition.Right;
            this.PagerPosition.At.Top = VPosition.Bottom;
		}

		internal override bool IsWijMobileExtender
		{
			get
			{
				return false;
			}
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1CarouselExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

	}
}