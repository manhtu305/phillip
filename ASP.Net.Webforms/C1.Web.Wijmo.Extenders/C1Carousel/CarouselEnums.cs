﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Carousel
{
#else
namespace C1.Web.Wijmo.Controls.C1Carousel
{
#endif
	/// <summary>
    /// Specifies the position of the navigation buttons.
	/// </summary>
	public enum ButtonPosition
	{

		/// <summary>
		/// Inside of frame
		/// </summary>
		Inside = 0,
		/// <summary>
		/// Outside of frame
		/// </summary>
		Outside = 1
	}

	/// <summary>
	/// Specifies the type of the pager.
	/// </summary>
	public enum PagerType
	{

		/// <summary>
		/// Slider
		/// </summary>
		Slider = 0,

		/// <summary>
		/// Numbers
		/// </summary>
		Numbers = 1,

		/// <summary>
		/// Dots
		/// </summary>
		Dots = 2,

		/// <summary>
		/// Thumbnails
		/// </summary>
		Thumbnails = 3
	}
}
