﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using C1.Web.Wijmo;
using System.ComponentModel;
using System.Web.UI;

#if !EXTENDER
using C1.Web.Wijmo.Controls;
using C1.Web.Wijmo.Controls.Localization;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Controls.C1Carousel
{
#else
using C1.Web.Wijmo.Extenders.Localization;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Extenders.C1Carousel
{
#endif
    /// <summary>
    /// Used to determine thumnail imags when the PagerType is set to Thumbnails.
    /// </summary>
	public partial class CarouselThumbnails : Settings, IJsonEmptiable
	{
		string[] _images;

		#region Properties

		/// <summary>
		/// Set the width of the image of thumbnails.
		/// </summary>
		[C1Description("C1Carousel.Thumbnails.ImageWidth")]
		[NotifyParentProperty(true)]
		[DefaultValue(58)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int ImageWidth
		{
			get
			{
				return this.GetPropertyValue("ImageWidth", 58);
			}
			set
			{
				this.SetPropertyValue("ImageWidth", value);
			}
		}

		/// <summary>
		/// Set the height of the image of thumbnails.
		/// </summary>
		[C1Description("C1Carousel.Thumbnails.ImageHeight")]
		[NotifyParentProperty(true)]
		[DefaultValue(74)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int ImageHeight
		{
			get
			{
				return this.GetPropertyValue("ImageHeight", 74);
			}
			set
			{
				this.SetPropertyValue("ImageHeight", value);
			}
		}

		/// <summary>
		/// Gets or sets the list of images of thumbnails.
		/// </summary>
		[C1Description("C1Carousel.Thumbnails.Images")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
		[TypeConverter(typeof(StringArrayConverter))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string[] Images
		{
			get
			{
				if (_images == null)
				{
					_images = new string[0];
				}
                if (HttpContext.Current != null)
                {
                    Page cpage = HttpContext.Current.Handler as Page;
                    if (cpage != null)
                    {
                        List<string> clientImgs = new List<string>();
                        foreach (string img in _images)
                        {
                            clientImgs.Add(cpage.ResolveClientUrl(img));
                        }
                        return clientImgs.ToArray();
                    }
                }
				return _images;
			}
			set
			{
				_images = value;
			}
		}

		#endregion

		#region Client side events

		/// <summary>
		/// Specify a function gets called when mouse over the thumbs.
		/// </summary>
		[C1Description("C1Carousel.Thumbnails.OnClientMouseOver")]
		[NotifyParentProperty(true)]
		[DefaultValue("")]
		[WidgetOptionName("mouseover")]
		[WidgetEvent]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientMouseOver
		{
			get
			{
				return this.GetPropertyValue<string>("OnClientMouseOver", "");
			}
			set
			{
				this.SetPropertyValue<string>("OnClientMouseOver", value);
			}
		}

		/// <summary>
		/// Specify a function gets called when mouse out the thumbs.
		/// </summary>
		[C1Description("C1Carousel.Thumbnails.OnClientMouseOut")]
		[NotifyParentProperty(true)]
		[DefaultValue("")]
		[WidgetOptionName("mouseout")]
		[WidgetEvent]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientMouseOut
		{
			get
			{
				return this.GetPropertyValue<string>("OnClientMouseOut", "");
			}
			set
			{
				this.SetPropertyValue<string>("OnClientMouseOut", value);
			}
		}

		/// <summary>
		/// Specify a function gets called when mouse up the thumbs.
		/// </summary>
		[C1Description("C1Carousel.Thumbnails.OnClientMouseUp")]
		[NotifyParentProperty(true)]
		[DefaultValue("")]
		[WidgetOptionName("mouseup")]
		[WidgetEvent]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientMouseUp
		{
			get
			{
				return this.GetPropertyValue<string>("OnClientMouseUp", "");
			}
			set
			{
				this.SetPropertyValue<string>("OnClientMouseUp", value);
			}
		}

		/// <summary>
		/// Specify a function gets called when mouse down the thumbs.
		/// </summary>
		[C1Description("C1Carousel.Thumbnails.OnClientMouseDown")]
		[NotifyParentProperty(true)]
		[DefaultValue("")]
		[WidgetOptionName("mousedown")]
		[WidgetEvent]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientMouseDown
		{
			get
			{
				return this.GetPropertyValue<string>("OnClientMouseDown", "");
			}
			set
			{
				this.SetPropertyValue<string>("OnClientMouseDown", value);
			}
		}

		/// <summary>
		/// Specify a function gets called when click the thumbs.
		/// </summary>
		[C1Description("C1Carousel.Thumbnails.OnClientClick")]
		[NotifyParentProperty(true)]
		[DefaultValue("")]
		[WidgetOptionName("click")]
		[WidgetEvent]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientClick
		{
			get
			{
				return this.GetPropertyValue<string>("OnClientClick", "");
			}
			set
			{
				this.SetPropertyValue<string>("OnClientClick", value);
			}
		}

		#endregion

		#region IJsonEmptiable Members

		bool IJsonEmptiable.IsEmpty
		{
			get
			{
				return string.IsNullOrEmpty(this.OnClientClick)
				&& string.IsNullOrEmpty(this.OnClientMouseDown)
				&& string.IsNullOrEmpty(this.OnClientMouseOut)
				&& string.IsNullOrEmpty(this.OnClientMouseOver)
				&& string.IsNullOrEmpty(this.OnClientMouseUp)
				&& this.ImageHeight == 74
				&& this.ImageWidth == 58
				&& this.Images.Length == 0;
			}
		}

		#endregion
	}
}
