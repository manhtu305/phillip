﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using System.Web.UI;

#if EXTENDER 
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif


#if EXTENDER
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Expander", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Expander
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Expander", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1Expander
#endif
{
	/// <summary>
	/// Represents an expanding panel that shows or hides embedded or external content.
	/// </summary>
	[WidgetDependencies(
        typeof(WijExpander)
#if !EXTENDER 		
        , "extensions.c1expander.js",
		ResourcesConst.C1WRAPPER_OPEN
#endif		
		)]
#if EXTENDER 
	public partial class C1ExpanderExtender
#else
	public partial class C1Expander
#endif	
	{

		#region ** fields

		AnimatedOption _Animated;

		#endregion

		#region ** options

		/// <summary>
		/// Determines if the widget can expand. Set this option to false if
		/// you want to disable collapse/expand ability.
		/// </summary>
		[C1Description("C1Expander.AllowExpand", "Determines if the widget can expand. Set this option to false if you want to disable collapse/expand ability.")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(true)]
		public bool AllowExpand
		{
			get
			{
				return GetPropertyValue("AllowExpand", true);
			}
			set
			{
				SetPropertyValue("AllowExpand", value);
			}
		}

		/// <summary>
		/// Determines the animation easing effect; set this option to false in 
		/// order to disable animation.
		/// Additional options 
		/// that are available for the animation function include:
		/// expand - value of true indicates that content element must be expanded.
		/// horizontal - value of true indicates that expander is horizontally 
		///	orientated (when expandDirection is left or right).
		/// content - jQuery object that contains content element to be expanded or 
		///				collapsed.
		/// </summary>
		[C1Description("C1Expander.Animated", "Determines the animation easing effect.")]
        [C1Category("Category.Behavior")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.Attribute)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption()]
		public AnimatedOption Animated
		{
			get
			{
				if (_Animated == null)
					_Animated = new AnimatedOption();
				return _Animated;
			}
		}

		/// <summary>
		/// Gets ot sets a URL to external content to display in the expander. 
		/// For example, set to "http://componentone.com/" for the ComponentOne Web site.
		/// </summary>
		[C1Description("C1Expander.ContentUrl", "Determines the URL to the external content. For example, http://componentone.com/ for the ComponentOne Web site.")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue("")]
		[UrlPropertyAttribute()]
		public string ContentUrl
		{
			get
			{
				return GetPropertyValue("ContentUrl", "");
			}
			set
			{
				SetPropertyValue("ContentUrl", value);
			}
		}

		/// <summary>
		/// Determines the visibility state of the content panel. If true, the content element is visible.
		/// </summary>
		[C1Description("C1Expander.Expanded", "Determines the visibility state of the content panel. If true, the content element is visible.")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(true)]
		public bool Expanded
		{
			get
			{
				return GetPropertyValue("Expanded", true);
			}
			set
			{
				SetPropertyValue("Expanded", value);
			}
		}

		/// <summary>
		/// Determines the content expand direction. Available values are top, right,
		///	bottom, and left. 
		/// </summary>
		[C1Description("C1Expander.ExpandDirection", "Determines the content expand direction. Available values are top, right,	bottom, and left.")]
        [C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(ExpandDirection.Bottom)]
		public ExpandDirection ExpandDirection
		{
			get
			{
				return GetPropertyValue("ExpandDirection", ExpandDirection.Bottom);
			}
			set
			{
				SetPropertyValue("ExpandDirection", value);
			}
		}

		#region ** client events

		/// <summary>
		/// Occurs before the content area collapses. This event can be cancelled to prevent the content area from collapsing.
		/// </summary>
		[C1Description("C1Expander.OnClientBeforeCollapse", "Occurs before the content area collapses. This event can be cancelled to prevent the content area from collapsing.")]
        [C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e")]
		[DefaultValue("")]
		public string OnClientBeforeCollapse
		{
			get
			{
				return GetPropertyValue("OnClientBeforeCollapse", "");
			}
			set
			{
				SetPropertyValue("OnClientBeforeCollapse", value);
			}
		}

		/// <summary>
		/// Occurs before the content area expands. This event can be cancelled to prevent the content area from expanding.
		/// 
		/// </summary>
		[C1Description("C1Expander.OnClientBeforeExpand", "Occurs before the content area expands. This event can be cancelled to prevent the content area from expanding.")]
        [C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e")]
		[DefaultValue("")]
		public string OnClientBeforeExpand
		{
			get
			{
				return GetPropertyValue("OnClientBeforeExpand", "");
			}
			set
			{
				SetPropertyValue("OnClientBeforeExpand", value);
			}
		}

		/// <summary>
		/// Occurs after the content area collapses.
		/// </summary>
		[C1Description("C1Expander.OnClientAfterCollapse", "Occurs after the content area collapses.")]
        [C1Category("Category.ClientSideEvents")]
		[WidgetEvent()]
		[DefaultValue("")]
		public string OnClientAfterCollapse
		{
			get
			{
				return GetPropertyValue("OnClientAfterCollapse", "");
			}
			set
			{
				SetPropertyValue("OnClientAfterCollapse", value);
			}
		}

		/// <summary>
		/// Occurs after the content area expands.
		/// </summary>
		[C1Description("C1Expander.OnClientAfterExpand", "Occurs after the content area expands.")]
        [C1Category("Category.ClientSideEvents")]
		[WidgetEvent()]
		[DefaultValue("")]
		public string OnClientAfterExpand
		{
			get
			{
				return GetPropertyValue("OnClientAfterExpand", "");
			}
			set
			{
				SetPropertyValue("OnClientAfterExpand", value);
			}
		}

		#endregion

		#endregion

	}

	/// <summary>
	/// Specifies the direction towards which a C1Expander control opens.
	/// </summary>
	public enum ExpandDirection
	{

		/// <summary>
		/// The header will appear below the body and the control's body will expand towards the top of the control.
		/// </summary>
		Top = 0,
		/// <summary>
		/// The header will appear to the left of the body and the control's body will expand towards the right of the control.
		/// </summary>
		Right = 1,
		/// <summary>
		/// The header will appear above the body and the control's body will expand towards the bottom of the control.
		/// </summary>
		Bottom = 2,
		/// <summary>
		/// The header will appear to the right of the body and the control's body will expand towards the left of the control.
		/// </summary>
		Left = 3
	}
}
