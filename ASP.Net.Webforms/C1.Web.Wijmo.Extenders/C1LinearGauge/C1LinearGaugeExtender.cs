﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Drawing;

namespace C1.Web.Wijmo.Extenders.C1Gauge
{
	[TargetControlType(typeof(Control))]
	[ToolboxItem(true)]
    [ToolboxBitmap(typeof(C1LinearGaugeExtender), "C1LinearGauge.png")]
    [LicenseProviderAttribute()]
	public partial class C1LinearGaugeExtender:C1GaugeExtender
	{

		private bool _productLicensed = false;
		public C1LinearGaugeExtender():base()
		{
			VerifyLicense();
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1LinearGaugeExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		[DefaultValue(0)]
		public override int Width
		{
			get
			{
				return GetPropertyValue<int>("Width", 0);
			}
			set
			{
				base.Width = value;
			}
		}

		[DefaultValue(0)]
		public override int Height
		{
			get
			{
				return GetPropertyValue<int>("Height", 0);
			}
			set
			{
				base.Height = value;
			}
		}
	}
}
