﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if !EXTENDER
using C1.Web.Wijmo.Controls.Localization;
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Gauge", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1Gauge
#else
using C1.Web.Wijmo.Extenders.Localization;
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Gauge", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Gauge
#endif
{
    [WidgetDependencies(
        typeof(WijLinearGauge)
#if !EXTENDER
, "extensions.c1lineargauge.js",
ResourcesConst.C1WRAPPER_PRO
#endif
)]
#if !EXTENDER
	public partial class C1LinearGauge
#else
	public partial class C1LinearGaugeExtender
#endif
	{
		#region ** Options
		/// <summary>
		/// A value that indicates whether the gauge shows as horizontal or vertical.
		/// </summary>
		[WidgetOption]
		[C1Description("C1LinearGauge.Orientation")]
		[DefaultValue(Orientation.Horizontal)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Orientation Orientation
		{
			get
			{
				return GetPropertyValue<Orientation>("Orientation", Orientation.Horizontal);
			}
			set
			{
				SetPropertyValue<Orientation>("Orientation", value);
			}
		}

		/// <summary>
		/// Gets or sets the xAxisLocation of the linear gauge
		/// </summary>
		[WidgetOption]
		[C1Description("C1LinearGauge.XAxisLocation")]
		[DefaultValue(0.1)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double XAxisLocation
		{
			get
			{
				return GetPropertyValue<double>("XAxisLocation", 0.1);
			}
			set
			{
				SetPropertyValue<double>("XAxisLocation", value);
			}
		}

		/// <summary>
		/// Gets or sets the length of the xAxis.
		/// </summary>
		[WidgetOption]
		[C1Description("C1LinearGauge.XAxisLength")]
		[DefaultValue(0.8)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double XAxisLength
		{
			get
			{
				return GetPropertyValue<double>("XAxisLength", 0.8);
			}
			set
			{
				SetPropertyValue<double>("XAxisLength", value);
			}
		}

		/// <summary>
		/// Gets or sets the position of the yAxis
		/// </summary>
		[WidgetOption]
		[C1Description("C1LinearGauge.YAxisLocation")]
		[DefaultValue(0.5)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double YAxisLocation
		{
			get
			{
				return GetPropertyValue<double>("YAxisLocation", 0.5);
			}
			set
			{
				SetPropertyValue<double>("YAxisLocation", value);
			}
		}

		#endregion

		#region ** methods
		/// <summary>
		/// Initializes the properties of the class.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void InitProperties()
		{
			base.InitProperties();
			this.Pointer = new GaugePointer();
			this.Pointer.Length = 0.5;
			this.Pointer.Width = 4;
			this.Pointer.PointerStyle = new C1Chart.ChartStyle();
			this.Pointer.Visible = true;
			this.Pointer.Shape = Shape.Tri;
		}
		#endregion

		#region ** Seriliaze
		/// <summary>
		/// Determine whether the Pointer property should be serialized to client side.
		/// </summary>
		/// <returns>Returns true if Pointer has values, otherwise return false.</returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializePointer()
		{
			return this.Pointer.Length != 0.5
				|| this.Pointer.Offset != 0
				|| this.Pointer.PointerStyle.ShouldSerialize()
				|| this.Pointer.Visible == false
				|| this.Pointer.Shape != Shape.Tri
				|| this.Pointer.Width != 4;
		}
		#endregion
	}
}
