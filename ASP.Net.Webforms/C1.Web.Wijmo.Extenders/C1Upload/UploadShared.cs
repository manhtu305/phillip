﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Upload", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Upload
#else
using C1.Web.Wijmo.Controls.Localization;
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Upload", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1Upload
#endif
{
    [WidgetDependencies(
        typeof(WijUpload)
#if !EXTENDER        
        ,typeof(WijProgressBar)
        ,"extensions.c1upload.js",
		ResourcesConst.C1WRAPPER_PRO
#endif
)]
#if EXTENDER
	public partial class C1UploadExtender
#else
    public partial class C1Upload : C1TargetControlBase
#endif
    {
        private LocalizationOption _LocalizationOption;

        #region Properties

        /// <summary>
        /// Gets or sets a value that indicates the server-side handler which handles the post requested.
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue("")]
        [C1Description("C1Upload.Action")]
        [WidgetOption]
#if !EXTENDER
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Layout(LayoutType.Behavior)]
#endif
        public string Action
        {
            get
            {
                return this.GetPropertyValue("Action", "");
            }
            set
            {
                this.SetPropertyValue("Action", value);
            }
        }

        /// <summary>
        /// A value that indicates whether to submit the file as soon as it is selected.
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [C1Description("C1Upload.AutoSubmit")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool AutoSubmit
        {
            get
            {
                return this.GetPropertyValue("AutoSubmit", false);
            }
            set
            {
                this.SetPropertyValue("AutoSubmit", value);
            }
        }

        /// <summary>
        /// Gets or sets the maximum number of files to be uploaded.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Upload.MaximumFiles")]
        [DefaultValue(0)]
        [Json(true, true, 0)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int MaximumFiles
        {
            get
            {
                return this.GetPropertyValue("MaximumFiles", 0);
            }

            set
            {
                if (value < 0) value = 0;
                this.SetPropertyValue("MaximumFiles", value);
            }
        }

        /// <summary>
        /// Gets or sets the accept file type of upload.
        /// audio/* 	All sound files are accepted
        /// video/* 	All video files are accepted
        /// image/* 	All image files are accepted
        /// MIME_type 	A valid MIME type, with no parameters. 
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue("")]
        [C1Description("C1Upload.Accept")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string Accept
        {
            get
            {
                return this.GetPropertyValue("Accept", "");
            }
            set
            {
                this.SetPropertyValue("Accept", value);
            }
        }

        /// <summary>
        /// Set localization string of buttons.
        /// </summary>
        //[C1Description("C1Upload.Localization")]
        //[EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public LocalizationOption Localization
        {
            get
            {
                if (_LocalizationOption == null)
                {
                    _LocalizationOption = new LocalizationOption();
                }
                return _LocalizationOption;
            }
        }

//        /// <summary>
//        /// A value that indicates whether support multiple seletion .
//        /// </summary>
//        [C1Category("Category.Behavior")]
//        [DefaultValue(false)]
//        [C1Description("C1Upload.Multiple")]
//        [WidgetOption]
//#if !EXTENDER
//        [Layout(LayoutType.Behavior)]
//#endif
//        public bool Multiple
//        {
//            get
//            {
//                return this.GetPropertyValue("Multiple", false);
//            }
//            set
//            {
//                this.SetPropertyValue("Multiple", value);
//            }
//        }

        #region Client Events

        /// <summary>
        /// The event associated with this property occurs when a user selects a file. This event can be cancelled.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [C1Description("C1Upload.OnClientChange")]
        [WidgetEvent("ev, data")]
        [WidgetOptionName("change")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientChange
        {
            get
            {
                return GetPropertyValue("OnClientChange", "");
            }
            set
            {
                SetPropertyValue("OnClientChange", value);
            }
        }

        /// <summary>
        /// The event associated with this property occurs before the file is uploaded. This event can be cancelled.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [C1Description("C1Upload.OnClientUpload")]
        [WidgetEvent("ev, data")]
        [WidgetOptionName("upload")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientUpload
        {
            get
            {
                return GetPropertyValue("OnClientUpload", "");
            }
            set
            {
                SetPropertyValue("OnClientUpload", value);
            }
        }

        /// <summary>
        /// The event associated with this property occurs when the uploadAll button is clicked. This event can be cancelled.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [C1Description("C1Upload.OnClientTotalUpload")]
        [WidgetEvent("ev")]
        [WidgetOptionName("totalUpload")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientTotalUpload
        {
            get
            {
                return GetPropertyValue("OnClientTotalUpload", "");
            }
            set
            {
                SetPropertyValue("OnClientTotalUpload", value);
            }
        }

        /// <summary>
        /// The event associated with this property occurs when a file is uploading.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [C1Description("C1Upload.OnClientProgress")]
        [WidgetEvent("ev, data")]
        [WidgetOptionName("progress")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientProgress
        {
            get
            {
                return GetPropertyValue("OnClientProgress", "");
            }
            set
            {
                SetPropertyValue("OnClientProgress", value);
            }
        }

        /// <summary>
        /// The event associated with this property occurs when click the uploadAll button and file uploading. 
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [C1Description("C1Upload.OnClientTotalProgress")]
        [WidgetEvent("ev, data")]
        [WidgetOptionName("totalProgress")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientTotalProgress
        {
            get
            {
                return GetPropertyValue("OnClientTotalProgress", "");
            }
            set
            {
                SetPropertyValue("OnClientTotalProgress", value);
            }
        }

        /// <summary>
        /// The event associated with this property occurs when a file upload is complete.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [C1Description("C1Upload.OnClientComplete")]
        [WidgetEvent("ev, data")]
        [WidgetOptionName("complete")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientComplete
        {
            get
            {
                return GetPropertyValue("OnClientComplete", "");
            }
            set
            {
                SetPropertyValue("OnClientComplete", value);
            }
        }

        /// <summary>
        /// The event associated with this property occurs when the uploadAll button is clicked and the file upload is complete.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [C1Description("C1Upload.OnClientTotalComplete")]
        [WidgetEvent("ev")]
        [WidgetOptionName("totalComplete")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientTotalComplete
        {
            get
            {
                return GetPropertyValue("OnClientTotalComplete", "");
            }
            set
            {
                SetPropertyValue("OnClientTotalComplete", value);
            }
        }

        #endregion

        #endregion       
    }

    [Browsable(false)]
    [EditorBrowsable(EditorBrowsableState.Never)]
    public class LocalizationOption : Settings
    {
        public LocalizationOption() { }

        [WidgetOption()]
        [DefaultValue("Upload All")]
        public string UploadAll
        {
            get
            {
                return this.GetPropertyValue<string>("UploadAll",
                    C1Localizer.GetString("C1Upload.Localization.UploadAll",
                    "Upload All"));
            }
            set
            {
                this.SetPropertyValue<string>("UploadAll", value);
            }
        }

        [WidgetOption()]
        [DefaultValue("Cancel All")]
        public string CancelAll
        {
            get
            {
                return this.GetPropertyValue<string>("CancelAll",
                    C1Localizer.GetString("C1Upload.Localization.CancelAll",
                    "Cancel All"));
            }
            set
            {
                this.SetPropertyValue<string>("CancelAll", value);
            }
        }


        [WidgetOption()]
        [DefaultValue("Upload")]
        public string Upload
        {
            get
            {
                return this.GetPropertyValue<string>("Upload",
                    C1Localizer.GetString("C1Upload.Localization.Upload",
                    "Upload"));
            }
            set
            {
                this.SetPropertyValue<string>("Upload", value);
            }
        }


        [WidgetOption()]
        [DefaultValue("Cancel")]
        public string Cancel
        {
            get
            {
                return this.GetPropertyValue<string>("Cancel",
                    C1Localizer.GetString("C1Upload.Localization.Cancel",
                    "Cancel"));
            }
            set
            {
                this.SetPropertyValue<string>("Cancel", value);
            }
        }

        [WidgetOption()]
        [DefaultValue("Upload files")]
        public string UploadFiles
        {
            get
            {
                return this.GetPropertyValue<string>("UploadFiles",
                    C1Localizer.GetString("C1Upload.Localization.UploadFiles",
                    "Upload files"));
            }
            set
            {
                this.SetPropertyValue<string>("UploadFiles", value);
            }
        }
    }
}
