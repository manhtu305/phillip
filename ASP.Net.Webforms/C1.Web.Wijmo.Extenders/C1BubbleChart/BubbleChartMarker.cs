﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;


#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	/// <summary>
	/// Represents the BubbleChartMarker class which contains all of the settings for the BubbleChartSeries
	/// </summary>
	public class BubbleChartMarker:Settings
	{
		#region ** fields
		private List<BubbleChartSymbol> symbol;		
		#endregion

		/// <summary>
		/// Gets or sets the symbols of the markers
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[C1Description("BubbleChartMarker.Symbol")]
		[C1Category("Category.Appearance")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[CollectionItemType(typeof(BubbleChartSymbol))]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public List<BubbleChartSymbol> Symbol
		{
			get
			{
				if (symbol == null)
				{
					symbol = new List<BubbleChartSymbol>();
				}
				return symbol;
			}
		}
		
		/// <summary>
		/// Gets or sets the type of the markers
		/// </summary>
		[WidgetOption]
		[DefaultValue(BubbleChartMarkerType.Circle)]
		[NotifyParentProperty(true)]
		[C1Description("BubbleChartMarker.Type")]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public BubbleChartMarkerType Type
		{
			get
			{
				return GetPropertyValue<BubbleChartMarkerType>("Type", BubbleChartMarkerType.Circle);
			}
			set
			{
				SetPropertyValue<BubbleChartMarkerType>("Type", value);
			}
		}

		
	}

	#region BubbleChartSymbol
	/// <summary>
	/// Represents the BubbleChartSymbol class which contains all of the settings for the BubbleChartMarker.
	/// </summary>
	public class BubbleChartSymbol : Settings
	{
		/// <summary>
		/// Gets or sets the index of the symbol
		/// </summary>
		[WidgetOption]
		[DefaultValue(-1)]
		[NotifyParentProperty(true)]
		[C1Description("BubbleChartSymbol.Index")]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int Index
		{
			get
			{
				return GetPropertyValue<int>("Index", -1);
			}
			set
			{
				SetPropertyValue<int>("Index", value);
			}
		}

		/// <summary>
		/// Gets or sets the url of the symbol
		/// </summary>
		[WidgetOption]
		[DefaultValue("")]
		[C1Description("BubbleChartSymbol.Url")]
		[C1Category("Category.Behavior")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string Url
		{
			get
			{
				return GetPropertyValue<string>("Url", "");
			}
			set
			{
				SetPropertyValue<string>("Url", value);
			}
		}
	} 
	#endregion

#region ** BubbleChartMarkerType
	/// <summary>
	/// Represents the BubbleChartMarkerType enum which contains all of the types for the BubbleChartMarker type
	/// </summary>
	public enum BubbleChartMarkerType
	{
		/// <summary>
		/// Circle
		/// </summary>
		Circle = 0,
		/// <summary>
		/// triangle
		/// </summary>
		Tri = 1,
		/// <summary>
		/// inverted triangle
		/// </summary>
		InvertedTri = 2,
		/// <summary>
		/// Box
		/// </summary>
		Box = 4,
		/// <summary>
		/// Diamond
		/// </summary>
		Diamond = 8,
		/// <summary>
		/// Cross 
		/// </summary>
		Cross = 16
	}
#endregion
}
