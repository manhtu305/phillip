﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Drawing;

namespace C1.Web.Wijmo.Extenders.C1Chart
{
	/// <summary>
	/// Extender for bar chart widget.
	/// </summary>
	[TargetControlType(typeof(Control))]
	[ToolboxItem(true)]
    [ToolboxBitmap(typeof(C1BubbleChartExtender), "C1BubbleChart.png")]
    [LicenseProviderAttribute()]
	public partial class C1BubbleChartExtender : C1ChartCoreExtender<BubbleChartSeries, ChartAnimation>
	{
		private bool _productLicensed = false;
		public C1BubbleChartExtender()
			: base()
		{
			VerifyLicense();
			this.InitChart();
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1BubbleChartExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}
	}
}
