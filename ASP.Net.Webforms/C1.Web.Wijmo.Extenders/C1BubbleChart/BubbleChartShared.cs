﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
    [WidgetDependencies(
       typeof(WijBubbleChart)
#if !EXTENDER
, "extensions.c1bubblechart.js",
ResourcesConst.C1WRAPPER_PRO
#endif
)]
#if EXTENDER
	public partial class C1BubbleChartExtender : C1ChartCoreExtender<BubbleChartSeries, ChartAnimation>
#else
    public partial class C1BubbleChart : C1ChartCore<BubbleChartSeries, ChartAnimation, C1BubbleChartBinding>
#endif
    {

        #region ** Override methods
        private void InitChart()
        {
            this.Animation = new ChartAnimation();
            this.Animation.Duration = 500;
            this.Animation.Enabled = true;
            this.Animation.Easing = ChartEasing.EaseOutElastic;
            this.ChartLabel = new BubbleChartLabel();
        }

        // added in 2013-10-31, fixing the OADateTime issue, move this method from C1BubbleChart to this file.
        /// <summary>
        /// Gets the series data of the specified series.
        /// </summary>
        /// <param name="series">
        /// The specified series.
        /// </param>
        /// <returns>
        /// Returns the series data which retrieved from the specified series.
        /// </returns>
        protected override ChartSeriesData GetSeriesData(BubbleChartSeries series)
        {
            if (series.IsTrendline)
                return base.GetSeriesData(series);

            return series.Data;
        }
        #endregion

        #region ** Options
        /// <summary>
        /// The minimum bubble size represents the percentage of the diameter (or area) of the plot area
        /// </summary>
        [WidgetOption]
        [C1Description("C1BubbleChart.MinimumSize")]
        [DefaultValue(5)]
        [C1Category("Category.Behavior")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int MinimumSize
        {
            get
            {
                return GetPropertyValue<int>("MinimumSize", 5);
            }
            set
            {
                SetPropertyValue<int>("MinimumSize", value);
            }
        }

        /// <summary>
        /// The maximum bubble size represents the percentage of the diameter (or area) of the plot area.
        /// </summary>
        [DefaultValue(20)]
        [C1Description("C1BubbleChart.MaximumSize")]
        [WidgetOption]
        [C1Category("Category.Behavior")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int MaximumSize
        {
            get
            {
                return GetPropertyValue<int>("MaximumSize", 20);
            }
            set
            {
                SetPropertyValue<int>("MaximumSize", value);
            }
        }

        /// <summary>
        /// Gets or sets the style of the bubble's label.
        /// </summary>
        [C1Description("C1BubbleChart.ChartLabel")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public BubbleChartLabel ChartLabel
        {
            get;
            set;
        }

        /// <summary>
        /// A value that indicates the bubble size calculate by diameter or area.
        /// </summary>
        [WidgetOption]
        [C1Description("C1BubbleChart.SizingMethod")]
        [C1Category("Category.Behavior")]
        [DefaultValue(SizingMethod.Diameter)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public SizingMethod SizingMethod
        {
            get
            {
                return GetPropertyValue<SizingMethod>("SizingMethod", SizingMethod.Diameter);
            }
            set
            {
                SetPropertyValue<SizingMethod>("SizingMethod", value);
            }
        }

        #endregion

        #region ** hidden properties
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public override string ChartLabelFormatString
        {
            get
            {
                return base.ChartLabelFormatString;
            }
            set
            {
                base.ChartLabelFormatString = value;
            }
        }

        [EditorBrowsable(EditorBrowsableState.Never)]
        [Browsable(false)]
        public override ChartStyle ChartLabelStyle
        {
            get
            {
                return base.ChartLabelStyle;
            }
            set
            {
                base.ChartLabelStyle = value;
            }
        }
        #endregion

        #region Serialize

        /// <summary>
        /// Determine whether the SeriesTransition property should be serialized to client side.
        /// </summary>
        /// <returns>
        /// Returns false if SeriesTransition has default values, otherwise return false.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeSeriesTransition()
        {
            if (!this.Animation.Enabled)
                return true;
            if (this.Animation.Duration != 2000)
                return true;
            if (this.Animation.Easing != ChartEasing.EaseOutElastic)
                return true;
            return false;
        }

        #endregion

    }

    #region ** enums
    /// <summary>
    /// Decides the bubble size calculate by diameter or area.
    /// </summary>
    public enum SizingMethod
    {
        /// <summary>
        /// diameter
        /// </summary>
        Diameter = 0,
        /// <summary>
        /// Area
        /// </summary>
        Area = 1
    }
    #endregion
}
