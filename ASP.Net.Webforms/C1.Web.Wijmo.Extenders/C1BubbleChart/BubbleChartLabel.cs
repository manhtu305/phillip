﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	/// <summary>
	/// Represents the BubbleChartLabel class which contains all of the settings for the BubbleChart Labels
	/// </summary>
	public class BubbleChartLabel:Settings, IJsonEmptiable
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="BubbleChartLabel"/> class.
		/// </summary>
		public BubbleChartLabel()
		{
			this.LabelStyle = new ChartStyle();
		}

		/// <summary>
		/// A value that indicates where to show the bubble's label.
		/// </summary>
		[WidgetOption]
		[C1Description("BubbleChartLabel.Position")]
		[DefaultValue(LabelPosition.Inside)]
		[NotifyParentProperty(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public LabelPosition Position
		{
			get
			{
				return GetPropertyValue<LabelPosition>("Position", LabelPosition.Inside);
			}
			set
			{
				SetPropertyValue<LabelPosition>("Position", value);
			}
		}

		/// <summary>
		/// A value that indicates which side to show the bubble's label. 
		/// </summary>
		[WidgetOption]
		[C1Description("BubbleChartLabel.Compass")]
		[DefaultValue(ChartCompass.North)]
		[NotifyParentProperty(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public ChartCompass Compass
		{
			get
			{
				return GetPropertyValue<ChartCompass>("Compass", ChartCompass.North);
			}
			set
			{
				SetPropertyValue<ChartCompass>("Compass", value);
			}
		}

		/// <summary>
		/// A value that indicates whether to show the bubble's label.
		/// </summary>
		[WidgetOption]
		[DefaultValue(true)]
		[C1Category("Category.Behavior")]
		[C1Description("BubbleChartLabel.Visible")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Visible
		{
			get 
			{
				return GetPropertyValue<bool>("Visible", true);
			}
			set 
			{
				SetPropertyValue<bool>("Visible", value);
			}
		}

		/// <summary>
		/// A value that indicates the style of the bubble chart label.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[C1Description("BubbleChartLabel.Style")]
		[C1Category("Category.Style")]
		[WidgetOption]
		[WidgetOptionName("style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle LabelStyle
		{
			get;
			set;
		}

		/// <summary>
		/// A value that indicates the format string of the chart labels.
		/// </summary>
		[WidgetOption]
		[NotifyParentProperty(true)]
		[DefaultValue("")]
		[C1Description("C1Chart.ChartCore.ChartLabelFormatString")]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string ChartLabelFormatString
		{ 
			get
			{
				return GetPropertyValue<string>("ChartLabelFormatString", "");
			}
			set
			{
				SetPropertyValue<string>("ChartLabelFormatString", value);
			}
		}

		bool IJsonEmptiable.IsEmpty
		{
			get
			{
				return !ShouldSerialize();
			}
		}

		internal bool ShouldSerialize()
		{
			return Position != LabelPosition.Inside
					|| Compass != ChartCompass.North
					|| Visible != true
					|| LabelStyle.ShouldSerialize();
		}
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the BubbleChartLabel.Position property.
	/// </summary>
	public enum LabelPosition
	{
		/// <summary>
		/// Inside of the bubble
		/// </summary>
		Inside = 1,
		/// <summary>
		/// Outside of the bubble
		/// </summary>
		Outside = 2
	}

}
