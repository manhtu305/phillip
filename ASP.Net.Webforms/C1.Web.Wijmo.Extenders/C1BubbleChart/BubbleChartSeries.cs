﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Drawing.Design;
using System.Collections;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	/// <summary>
	/// Represents the BubbleChartSeries class which contains all of the settings for the bubblechart series.
	/// </summary>
	public class BubbleChartSeries: ChartSeries
	{
		private int[] labels = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="BubbleChartSeries"/> class.
		/// </summary>
		public BubbleChartSeries()
			: base()
		{
			this.Data = new BubbleChartSeriesData();
			this.Markers = new BubbleChartMarker();
		}

		/// <summary>
		/// A value that indicates the data of the chart series.
		/// </summary>
		[C1Description("BubbleChartSeries.Data")]
		[C1Category("Category.Data")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public BubbleChartSeriesData Data
		{
			get;
			set;
		}

		/// <summary>
		/// A value that indicates the marker of the chart series.
		/// </summary>
		[WidgetOption]
		[C1Description("BubbleChartSeries.Markers")]
		[C1Category("Category.Appearance")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public BubbleChartMarker Markers
		{
			get
			{
				return GetPropertyValue<BubbleChartMarker>("Markers", new BubbleChartMarker());
			}
			set
			{
				SetPropertyValue<BubbleChartMarker>("Markers", value);
			}
		}

		/// <summary>
		/// A value that indicates which marker is invisible
		/// </summary>
		[WidgetOption]
		[TypeConverter(typeof(IntArrayConverter))]
		[NotifyParentProperty(true)]
		[C1Description("BubbleChartSeries.InvisibleMarkLabels")]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int[] InvisibleMarkLabels
		{
			get
			{
				return this.labels;
			}
			set
			{
				if (value.Length == 0 && this.labels.Length == 0)
				{
					return;
				}
				this.InitValues();
				this.labels = value;
			}
		}



		/// <summary>
		/// Initializes the default properties.
		/// </summary>
		private void InitValues()
		{
			this.labels = new int[0];
		}


		/// <summary>
		/// Determine whether the SeriesTransition property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns false if SeriesTransition has default values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal override bool ShouldSerialize()
		{
			var shouldSerialize = base.ShouldSerialize();
			if (shouldSerialize)
				return true;
			if (Data.ShouldSerialize())
				return true;
			if (this.InvisibleMarkLabels.Length > 0)
			{
				return true;
			}
			return false;
		}
		
	}

	/// <summary>
	/// Represents the BubbleChartSeriesData class which contains all of the settings for the BubbleChartSeries data option.
	/// </summary>
	public class BubbleChartSeriesData : ChartSeriesData
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="BubbleChartSeriesData"/> class.
		/// </summary>
		public BubbleChartSeriesData()
			: base()
		{
			this.Y1 = new ChartY1AxisList();
		}

		/// <summary>
		/// A value that indicates the y1 values of the chart series data.
		/// </summary>
		[C1Description("BubbleChartSeriesData.Y1")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public ChartY1AxisList Y1 { get; set; }


		internal new bool ShouldSerialize()
		{
			return this.X.Length > 0 || this.Y.Length > 0 || this.Y1.Length > 0;
		}
	}
}
