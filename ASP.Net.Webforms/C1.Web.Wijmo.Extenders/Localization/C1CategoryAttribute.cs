﻿using System;
using System.ComponentModel;

namespace C1.Web.Wijmo.Extenders.Localization
{
    /// <summary>
    /// C1CategoryAttribute replaces the CategoryAttribute
    /// and uses the C1Localizer class to return the localized Attribute string
    /// </summary>
    [AttributeUsage(AttributeTargets.All)]
    public class C1CategoryAttribute : CategoryAttribute
    {
        /// <summary>
        /// Initialize C1CategoryAttribute.
        /// </summary>
        /// <param name="name">Category key.</param>
        public C1CategoryAttribute(string name)
            : base(name)
        {
        }

        /// <summary>
        /// Get localized string
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        override protected string GetLocalizedString(string value)
        {
            string s = C1Localizer.GetString(value);
            if (!string.IsNullOrEmpty(s))
            {
                return s;
            }

            return value;
        }
    }
}
