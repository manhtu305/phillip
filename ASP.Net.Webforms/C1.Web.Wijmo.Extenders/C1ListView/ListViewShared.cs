﻿using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

#if EXTENDER
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1ListView", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1ListView
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1ListView", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1ListView
#endif
{
#if EXTENDER
    using C1.Web.Wijmo.Extenders.Localization;
    using C1.Web.Wijmo.Extenders.Converters;
#else
    using C1.Web.Wijmo.Controls.Localization;
    using C1.Web.Wijmo.Controls.Converters;
#endif

    using C1.Web.Wijmo;

    [WidgetDependencies(
        typeof(WijListView)
#if !EXTENDER
, "extensions.c1listview.js",
ResourcesConst.C1WRAPPER_OPEN
#endif
)]
#if EXTENDER
    public partial class C1ListViewExtender
#else
    public partial class C1ListView
#endif
    {
        #region Properties

        /// <summary>
        /// A value determines whether to auto-size wijlist.
        /// </summary>
        [C1Category("Category.Appearance")]
        [DefaultValue(false)]
        [C1Description("C1ListView.Inset", "A value determines whether to auto-size wijlist.")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        public bool Inset
        {
            get { return this.GetPropertyValue("Inset", false); }
            set { this.SetPropertyValue("Inset", value); }
        }

        /// <summary>
        /// Sets the color scheme (swatch) for list item count bubbles.
        /// </summary>
        [C1Category("Category.Appearance")]
        [DefaultValue("c")]
        [C1Description("C1ListView.CountTheme", "Sets the color scheme (swatch) for list item count bubbles.")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        public string CountTheme
        {
            get { return this.GetPropertyValue("CountTheme", "c"); }
            set { this.SetPropertyValue("CountTheme", value); }
        }

        /// <summary>
        /// Sets the color scheme (swatch) for headers of nested list sub pages.
        /// </summary>
        [C1Category("Category.Appearance")]
        [DefaultValue("b")]
        [C1Description("C1ListView.HeaderTheme", "Sets the color scheme (swatch) for headers of nested list sub pages.")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        public string HeaderTheme
        {
            get { return this.GetPropertyValue("HeaderTheme", "b"); }
            set { this.SetPropertyValue("HeaderTheme", value); }
        }

        /// <summary>
        /// Sets the color scheme (swatch) for list dividers.
        /// </summary>
        [C1Category("Category.Appearance")]
        [DefaultValue("b")]
        [C1Description("C1ListView.DividerTheme", "Sets the color scheme (swatch) for list dividers")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        public string DividerTheme
        {
            get { return this.GetPropertyValue("DividerTheme", "b"); }
            set { this.SetPropertyValue("DividerTheme", value); }
        }

        /// <summary>
        /// Applies an icon from the icon set to all split list buttons.
        /// </summary>
        [C1Category("Category.Appearance")]
        [DefaultValue("arrow-r")]
        [C1Description("C1ListView.SplitIcon", "Applies an icon from the icon set to all split list buttons.")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        public string SplitIcon
        {
            get { return this.GetPropertyValue("SplitIcon", "arrow-r"); }
            set { this.SetPropertyValue("SplitIcon", value); }
        }

        /// <summary>
        /// Sets the color scheme (swatch) for split list buttons.
        /// </summary>
        [C1Category("Category.Appearance")]
        [DefaultValue("b")]
        [C1Description("C1ListView.SplitTheme", "Sets the color scheme (swatch) for split list buttons.")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        public string SplitTheme
        {
            get { return this.GetPropertyValue("SplitTheme", "b"); }
            set { this.SetPropertyValue("SplitTheme", value); }
        }

        /// <summary>
        /// Adds a search filter bar to listviews. 
        /// </summary>
        [C1Category("Category.Appearance")]
        [DefaultValue(false)]
        [C1Description("C1ListView.Filter", "Adds a search filter bar to listviews. ")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        public bool Filter
        {
            get { return this.GetPropertyValue("Filter", false); }
            set { this.SetPropertyValue("Filter", value); }
        }

        /// <summary>
        /// Determines whether to auto-hide all the list items when the search field is blank.
        /// </summary>
        [C1Category("Category.Appearance")]
        [DefaultValue(false)]
        [C1Description("C1ListView.FilterReveal", "If 'true', listview will auto-hide all the list items when the search field is blank.")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        [WidgetOptionName("filterReveal")]
        public bool FilterReveal
        {
            get { return this.GetPropertyValue("FilterReveal", false); }
            set { this.SetPropertyValue("FilterReveal", value); }
        }

        /// <summary>
        /// The placeholder text used in search filter bars. 
        /// </summary>
        [C1Category("Category.Appearance")]
        [DefaultValue("Filter items...")]
        [C1Description("C1ListView.FilterPlaceholder", "The placeholder text used in search filter bars. ")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        public string FilterPlaceholder
        {
            get { return this.GetPropertyValue("FilterPlaceholder", "Filter items..."); }
            set { this.SetPropertyValue("FilterPlaceholder", value); }
        }

        /// <summary>
        /// Sets the color scheme (swatch) for the search filter bar.
        /// </summary>
        [C1Category("Category.Appearance")]
        [DefaultValue("c")]
        [C1Description("C1ListView.FilterTheme", "Sets the color scheme (swatch) for the search filter bar.")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        public string FilterTheme
        {
            get { return this.GetPropertyValue("FilterTheme", "c"); }
            set { this.SetPropertyValue("FilterTheme", value); }
        }

        /// <summary>
        /// A listview can be configured to automatically generate dividers for its items. 
        /// </summary>
        [C1Category("Category.Appearance")]
        [DefaultValue(false)]
        [C1Description("C1ListView.AutoDividers", "A listview can be configured to automatically generate dividers for its items. ")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        [WidgetOptionName("autodividers")]
        public bool AutoDividers
        {
            get { return this.GetPropertyValue("AutoDividers", false); }
            set { this.SetPropertyValue("AutoDividers", value); }
        }

        /// <summary>
        /// Applies an icon from the icon set to all linked list buttons.
        /// </summary>
        [C1Category("Category.Appearance")]
        [DefaultValue("arrow-r")]
        [C1Description("C1ListView.Icon", "Applies an icon from the icon set to all linked list buttons.")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        [WidgetOption]
        public string Icon
        {
            get { return this.GetPropertyValue("Icon", "arrow-r"); }
            set { this.SetPropertyValue("Icon", value); }
        }

        /// <summary>
        /// Set the default color scheme (swatch) for items.
        /// </summary>
        [C1Category("Category.Appearance")]
        [WidgetOption]
        [WidgetOptionName("Theme")]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
#endif
        [DefaultValue("c")]
        public override string ThemeSwatch
        {
            get
            {
                return this.GetPropertyValue("ThemeSwatch", "c");
            }
            set
            {
                this.SetPropertyValue("ThemeSwatch", value);
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// The function accepts two arguments -- the text of the list item (or data-filtertext value if present), and the search string. Return true to hide the item, false to leave it visible. 
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [C1Description("C1ListView.OnFilterCallback", "The function to determine which rows to hide when the search filter textbox changes. ")]
        [WidgetEvent]
        [WidgetOptionName("FilterCallback")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientFilterCallback
        {
            get { return this.GetPropertyValue("OnClientFilterCallback", ""); }
            set { this.SetPropertyValue("OnClientFilterCallback", value); }
        }

        /// <summary>
        /// The function to return a value which can specify divider text.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [C1Description("C1ListView.AutodividersSelector", "")]
        [WidgetEvent]
        [WidgetOptionName("autodividersSelector")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientAutodividersSelector
        {
            get { return this.GetPropertyValue("OnClientAutodividersSelector", ""); }
            set { this.SetPropertyValue("OnClientAutodividersSelector", value); }
        }

        /// <summary>
        /// A function which will be triggered when a wijlistview is created.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [C1Description("C1ListView.OnCreate")]
        [WidgetEvent]
        [WidgetOptionName("create")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientCreate
        {
            get { return this.GetPropertyValue("OnClientCreate", ""); }
            set { this.SetPropertyValue("OnClientCreate", value); }
        }

        #endregion
    }
}
