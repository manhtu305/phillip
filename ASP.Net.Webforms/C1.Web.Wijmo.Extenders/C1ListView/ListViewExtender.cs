﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Extenders.C1ListView
{
    [TargetControlType(typeof(Panel))]
    [ToolboxBitmap(typeof(C1ListViewExtender), "C1ListView.png")]
    [LicenseProviderAttribute()]
    public partial class C1ListViewExtender : WidgetExtenderControlBase
    {
        private bool _productLicensed = false;

        internal override bool IsWijWebExtender
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="C1ExpanderExtender"/> class.
        /// </summary>
        public C1ListViewExtender()
            : base()
        {
            VerifyLicense();
        }

        private void VerifyLicense()
        {
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1ListViewExtender), this, false);
#if GRAPECITY
            _productLicensed = licinfo.IsValid;
#else
            _productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
        }

        /// <summary>
        /// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
        /// </summary>
        /// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
            base.OnPreRender(e);
        }
    }
}
