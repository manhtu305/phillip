﻿using System.ComponentModel;
using System.Collections.Generic;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
    [WidgetDependencies(
        typeof(WijCandlestickChart)
#if !EXTENDER
, "extensions.c1candlestickchart.js",
ResourcesConst.C1WRAPPER_PRO
#endif
)]
#if EXTENDER
	public partial class C1CandlestickChartExtender : C1ChartCoreExtender<CandlestickChartSeries, ChartAnimation>
#else
	public partial class C1CandlestickChart : C1ChartCore<CandlestickChartSeries, ChartAnimation, C1CandlestickChartBinding>
#endif
	{
		#region ** Fields
		List<CandlestickChartStyle> _seriesStyle = null;
		List<CandlestickChartStyle> _seriesHoverStyle = null;
		#endregion end ** Fields

		#region ** Properties
		/// <summary>
		/// A value that indicates the type of the chart.
		/// </summary>
		[C1Description("C1Chart.C1CandlestickChart.Type")]
		[WidgetOption]
		[DefaultValue(CandlestickChartType.Candlestick)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public CandlestickChartType Type
		{
			get
			{
				return GetPropertyValue<CandlestickChartType>("Type", CandlestickChartType.Candlestick);
			}
			set
			{
				SetPropertyValue<CandlestickChartType>("Type", value);
			}
		}

		/// <summary>
		/// A value that indicates the formater of the chart.
		/// </summary>
		[DefaultValue("")]
        [C1Description("C1Chart.C1CandlestickChart.CandlestickFormatter")]
		[WidgetOption]
		[WidgetFunction]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
        public string CandlestickFormatter
		{
			get
			{
                return this.GetPropertyValue<string>("CandlestickFormatter", "");
			}
			set
			{
                this.SetPropertyValue<string>("CandlestickFormatter", value);
			}
		}

		/// <summary>
		/// An array collection that contains the style to be charted.
		/// </summary>
		[C1Description("C1Chart.ChartCore.SeriesStyles")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
		[WidgetOptionName("seriesStyles")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
		[CollectionItemType(typeof(CandlestickChartStyle))]
#endif
		public List<CandlestickChartStyle> CandlestickChartSeriesStyles
		{
			get
			{
				if (_seriesStyle == null)
				{
					_seriesStyle = new List<CandlestickChartStyle>();
				}
				return _seriesStyle;
			}
		}

		/// <summary>
		/// An array collection that contains the style to be charted when hovering the chart element.
		/// </summary>
		[C1Description("C1Chart.ChartCore.SeriesHoverStyles")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
		[WidgetOptionName("seriesHoverStyles")]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
		[CollectionItemType(typeof(CandlestickChartStyle))]
#endif
		public List<CandlestickChartStyle> CandlestickChartSeriesHoverStyles
		{
			get
			{
				if (_seriesHoverStyle == null)
				{
					_seriesHoverStyle = new List<CandlestickChartStyle>();
				}
				return _seriesHoverStyle;
			}
		}


		#region ** hidden properties
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public override string ChartLabelFormatString
		{
			get
			{
				return base.ChartLabelFormatString;
			}
			set
			{
				base.ChartLabelFormatString = value;
			}
		}
        [EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public override bool ShowChartLabels
		{
			get
			{
				return base.ShowChartLabels;
			}
			set
			{
				base.ShowChartLabels = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public override ChartStyle ChartLabelStyle
		{
			get
			{
				return base.ChartLabelStyle;
			}
			set
			{
				base.ChartLabelStyle = value;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public override List<ChartStyle> SeriesStyles
		{
			get
			{
				return base.SeriesStyles;
			}
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public override List<ChartStyle> SeriesHoverStyles
		{
			get
			{
				return base.SeriesHoverStyles;
			}
		}
		#endregion end ** hidden properties
		#endregion end ** Properties

		#region ** Methods
		private void InitChart()
		{
			this.Axis.X.GridMajor.Visible = false;
		}

		/// <summary>
        /// Gets the series data of the specified series.
        /// </summary>
        /// <param name="series">
        /// The specified series.
        /// </param>
        /// <returns>
        /// Returns the series data which retrieved from the specified series.
        /// </returns>
        protected override ChartSeriesData GetSeriesData(CandlestickChartSeries series)
        {
            if (series.IsTrendline)
                return base.GetSeriesData(series);

            return series.Data;
        }

        /// <summary>
		/// Determine whether the SeriesStyles property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if SeriesStyles has values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeCandlestickChartSeriesStyles()
		{
			return CandlestickChartSeriesStyles.Count > 0;
		}

		/// <summary>
		/// Determine whether the SeriesHoverStyles property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if SeriesHoverStyles has values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeCandlestickChartSeriesHoverStyles()
		{
			return CandlestickChartSeriesHoverStyles.Count > 0;
		}

        public override bool ShouldSerializeSeriesHoverStyles()
        {
            return false;
        }
        public override bool ShouldSerializeSeriesStyles()
        {
            return false;
        }
		#endregion end ** Methods
	}

	/// <summary>
	/// Represents the CandlestickChartStyle class which contains all of the settings for the candlestickchart style.
	/// </summary>
		public class CandlestickChartStyle : Settings, IJsonEmptiable
	{
		#region ** Ctor
		public CandlestickChartStyle()
		{
			this.HighLow = new ChartStyle();
			this.Open = new ChartStyle();
			this.Close = new ChartStyle();
			this.FallingClose = new ChartStyle();
            this.RisingClose = new ChartStyle();
			this.UnchangeClose = new ChartStyle();
		}
		#endregion end ** Ctor

		#region ** Properties
		/// <summary>
		/// Gets or sets the chart style for high&low element.
		/// </summary>
		[C1Description("C1Chart.CandlestickChartStyle.HighLow")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle HighLow { get; set; }

		/// <summary>
		///  Gets or sets the chart style for open element.
		/// </summary>
		[C1Description("C1Chart.CandlestickChartStyle.Open")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle Open { get; set; }

		/// <summary>
		///  Gets or sets the chart style for close element.
		/// </summary>
		[C1Description("C1Chart.CandlestickChartStyle.Close")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle Close { get; set; }

		/// <summary>
		///  Gets or sets the chart style when close is lower than open.
		/// </summary>
		[C1Description("C1Chart.CandlestickChartStyle.FallingClose")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle FallingClose { get; set; }

		/// <summary>
		/// Gets or sets the chart style when close is higher than open.
		/// </summary>
        [C1Description("C1Chart.CandlestickChartStyle.RisingClose")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
        public ChartStyle RisingClose { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[C1Description("C1Chart.CandlestickChartStyle.UnchangeClose")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public ChartStyle UnchangeClose { get; set; }
		#endregion end ** Properties

		#region Serialize
		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal bool ShouldSerialize()
		{
			return HighLow.ShouldSerialize() && Open.ShouldSerialize()
                && Close.ShouldSerialize() && RisingClose.ShouldSerialize()
				&& FallingClose.ShouldSerialize() && UnchangeClose.ShouldSerialize();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeHighLow()
		{
			return this.HighLow.ShouldSerialize();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeOpen()
		{
			return this.Open.ShouldSerialize();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeClose()
		{
			return this.Close.ShouldSerialize();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeRisingClose()
		{
            return this.RisingClose.ShouldSerialize();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeUnchangeClose()
		{
			return this.UnchangeClose.ShouldSerialize();
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeFallingClose()
		{
			return this.FallingClose.ShouldSerialize();
		}
		#endregion end ** Serialize
	}
}
