﻿using System.Web.UI;
using System.ComponentModel;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	/// <summary>
	/// Represents the CandlestickChartSeries class which contains all of the settings for the chart series.
	/// </summary>
	public class CandlestickChartSeries : ChartSeries
	{
		#region ** Ctor
		public CandlestickChartSeries()
			: base()
		{
			this.Data = new CandlestickChartSeriesData();
		}
		#endregion end ** Ctor

		#region ** Properties
		/// <summary>
		/// A value that indicates the data of the chart series.
		/// </summary>
		[C1Description("C1Chart.ChartSeries.Data")]
		[C1Category("Category.Data")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public CandlestickChartSeriesData Data
		{
			get;
			set;
		}

		#region ** Hidden properties
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override ChartStyle TextStyle
		{
			get
			{
				return base.TextStyle;
			}
		}
		#endregion end ** Hidden properties
		#endregion end ** Properties

		#region ** Serialize
		/// <summary>
		/// Determine whether the CandlestickChartSeries property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns false if CandlestickChartSeries has default values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal override bool ShouldSerialize()
		{
			var shouldSerialize = base.ShouldSerialize();
			if (shouldSerialize)
				return true;
			if (Data.ShouldSerialize())
				return true;
			return false;
		}
		#endregion end ** Serialize
	}


	/// <summary>
	/// Represents the CandlestickChartSeriesData class which contains all settings for the candlestickchart series.
	/// </summary>
	public class CandlestickChartSeriesData : ChartSeriesData
	{
		#region ** Ctor
		/// <summary>
		/// Initializes a new instance of the ChartSeriesData class.
		/// </summary>
		public CandlestickChartSeriesData()
		{
			this.X = new ChartXAxisList();
			this.High = new ChartY1AxisList();
			this.Low = new ChartY1AxisList();
			this.Open = new ChartY1AxisList();
			this.Close = new ChartY1AxisList();
		}
		#endregion end ** Ctor

		#region ** Properties
		/// <summary>
		/// A value that indicates the high values of the chart series data.
		/// </summary>
		[C1Description("CandlestickChartSeriesData.High")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public ChartY1AxisList High { get; set; }

		/// <summary>
		/// A value that indicates the low values of the chart series data.
		/// </summary>
		[C1Description("CandlestickChartSeriesData.Low")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public ChartY1AxisList Low { get; set; }

		/// <summary>
		/// A value that indicates the open values of the chart series data.
		/// </summary>
		[C1Description("CandlestickChartSeriesData.Open")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public ChartY1AxisList Open { get; set; }

		/// <summary>
		/// A value that indicates the close values of the chart series data.
		/// </summary>
		[C1Description("CandlestickChartSeriesData.Close")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public ChartY1AxisList Close { get; set; }

		#region ** Hidden
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override ChartYAxisList Y
		{
			get
			{
				return base.Y;
			}
		}
		#endregion end ** Hidden
		#endregion end ** Properties

		#region ** Serialize

		internal override bool ShouldSerialize()
		{
			return base.ShouldSerialize() || High.Length > 0
				|| Low.Length > 0 || Open.Length > 0 || Close.Length > 0;
		}

		#endregion end ** Serialize
	}
}
