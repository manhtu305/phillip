﻿using System;
using System.Web.UI;
using System.Drawing;
using System.ComponentModel;

namespace C1.Web.Wijmo.Extenders.C1Chart
{
	/// <summary>
	/// Extender for bar chart widget.
	/// </summary>
	[TargetControlType(typeof(Control))]
	[ToolboxBitmap(typeof(C1CandlestickChartExtender), "C1CandlestickChart.png")]
	[ToolboxItem(true)]
	[LicenseProviderAttribute()]
	public partial class C1CandlestickChartExtender : C1ChartCoreExtender<CandlestickChartSeries, ChartAnimation>
	{
		private bool _productLicensed = false;

		/// <summary>
		/// Initializes a new instance of the C1BarChartExtender class.
		/// </summary>
		public C1CandlestickChartExtender()
			: base()
		{
			VerifyLicense();
			this.InitChart();
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1CandlestickChartExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}
	}
}
