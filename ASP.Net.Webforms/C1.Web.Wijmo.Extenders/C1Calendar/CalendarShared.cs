﻿using System.ComponentModel;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Drawing.Design;


#if EXTENDER
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Calendar", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Calendar
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Calendar", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1Calendar
#endif
{

#if EXTENDER 
	using C1.Web.Wijmo.Extenders.Localization;
	using C1.Web.Wijmo.Extenders.Converters;
	using C1.Web.Wijmo;
#else
    using C1.Web.Wijmo.Controls.Localization;
    using C1.Web.Wijmo.Controls.Converters;
#endif


    [WidgetDependencies(
          typeof(WijCalendar)
#if !EXTENDER
, "extensions.c1calendar.js",
ResourcesConst.C1WRAPPER_OPEN
#endif
)]
#if EXTENDER
	public partial class C1CalendarExtender
#else
    public partial class C1Calendar:ICultureControl
#endif
    {
        #region ** fields

        private SelectionSettings _selectionMode = null;
        private DateTime[] _selDates;
        private DateTime[] _disabledDates;

        #endregion

        #region ** options

        /// <summary>
        /// Gets or sets the disable dates collection.
        /// </summary>
        [WidgetOption]
        [C1Category("Category.Dates")]
        [C1Description("C1Calendar.DisabledDates")]
        [DefaultValue(null)]
        [TypeConverter(typeof(DateTimeArrayConverter))]
#if !EXTENDER
#if ASP_NET4
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.DateTimeArrayEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
        [Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.DateTimeArrayEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#endif
#endif
        public DateTime[] DisabledDates
        {
            get
            {
                if (this._disabledDates == null)
                {
                    this._disabledDates = new DateTime[0];
                }

                return this._disabledDates;
            }
            set
            {
                this._disabledDates = value;
            }
        }

        /// <summary>
        /// Gets or sets the selected dates collection.
        /// </summary>
        [WidgetOption()]
        [C1Category("Category.Selection")]
        [C1Description("C1Calendar.SelectedDates")]
        [DefaultValue(null)]
        [TypeConverter(typeof(DateTimeArrayConverter))]
#if !EXTENDER
#if ASP_NET4
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.DateTimeArrayEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
        [Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.DateTimeArrayEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#endif
#endif
        public DateTime[] SelectedDates
        {
            get
            {
                if (this._selDates == null)
                    this._selDates = new DateTime[0];

                return this._selDates;
            }
            set
            {
                this._selDates = value;

                //                bool changed = false;
                //                if (this._selDates != value && this._selDates != null && value != null)
                //                {
                //                    if (this._selDates.Length != value.Length)
                //                    {
                //                        changed = true;
                //                    }
                //                    else
                //                    {
                //                        for (int i = 0; i < this._selDates.Length; i++)
                //                        {
                //                            if (this._selDates[i].Ticks != value[i].Ticks)
                //                            {
                //                                changed = true;
                //                                break;
                //                            }
                //                        }
                //                    }
                //                }
                //                else
                //                {
                //                    if ((this._selDates == null && value.Length > 0) || (value == null && this._selDates.Length > 0))
                //                        changed = true;
                //                }

                //                if (changed)
                //                {
                //                    this._selDates = value;
                //#if !EXTENDER
                //                    this.OnSelectedDatesChanged(EventArgs.Empty);
                //#endif
                //                }
            }
        }

        /// <summary>
        /// Gets or sets the latest selected date.
        /// </summary>
        [C1Category("Category.Selection")]
        [C1Description("C1Calendar.SelectedDate")]
        [DefaultValue(typeof(DateTime), "1900/1/1")]
        [Bindable(true, BindingDirection.TwoWay)]
        public DateTime SelectedDate
        {
            get
            {
                if (this.SelectedDates.Length == 0)
                    return new DateTime(1900, 1, 1);

                return this.SelectedDates[0];
            }
            set
            {
                this.SelectedDates = new DateTime[0];
                if (value != new DateTime(1900, 1, 1))
                {
                    this.SelectedDates = new DateTime[1] { value };
                }
            }
        }

        /// <summary>
        /// Gets or sets culture ID.
        /// </summary>
        [C1Description("C1Calendar.CultureInfo")]
        [C1Category("Category.Behavior")]
        [DefaultValue(typeof(CultureInfo), "en-US")]
        [TypeConverter(typeof(CultureInfoConverter))]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public CultureInfo Culture
        {
            get
            {
                return GetCulture( GetPropertyValue<CultureInfo>("Culture", null));
            }
            set
            {
				SetPropertyValue("Culture", value);
			}
        }

        /// <summary>
        /// A value that indicators the culture calendar to format the text. This property must work with Culture property.
        /// </summary>
        [C1Description("Culture.CultureCalendar")]
        [C1Category("Category.Appearance")]
        [DefaultValue("")]
        [WidgetOption]
        public string CultureCalendar
        {
            get
            {
                return GetPropertyValue<string>("CultureCalendar", "");
            }
            set
            {
                SetPropertyValue<string>("CultureCalendar", value);
            }
        }

        /// <summary>
        /// Gets or sets the initial view type of Calendar.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Calendar.InitialView")]
        [NotifyParentProperty(true)]
        [DefaultValue(ViewType.Day)]
        [WidgetOption]
        public ViewType InitialView
        {
            get
            {
                return GetPropertyValue("InitialView", ViewType.Day);
            }
            set
            {
                SetPropertyValue("InitialView", value);
            }
        }


        /// <summary>
        /// Gets or sets the number of calendar months in horizontal direction.
        /// </summary>
        [C1Category("Category.MonthView")]
        [C1Description("C1Calendar.MonthCols")]
        [NotifyParentProperty(true)]
        [DefaultValue(1)]
        [WidgetOption]
        public int MonthCols
        {
            get
            {
                return GetPropertyValue("MonthCols", 1);
            }
            set
            {
                SetPropertyValue("MonthCols", value);
            }
        }

        /// <summary>
        /// Gets or sets the number of calendar months in vertical direction.
        /// </summary>
        [C1Category("Category.MonthView")]
        [C1Description("C1Calendar.MonthRows")]
        [NotifyParentProperty(true)]
        [DefaultValue(1)]
        [WidgetOption]
        public int MonthRows
        {
            get
            {
                return GetPropertyValue("MonthRows", 1);
            }
            set
            {
                SetPropertyValue("MonthRows", value);
            }
        }

        /// <summary>
        /// Gets "Week" string from localization.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue("Wk")]
        [WidgetOption]
        public string WeekString
        {
            get
            {
                return GetPropertyValue("WeekString",
                     C1Localizer.GetString("C1Calendar.WeekString.DefaultValue", "Wk", this.Culture));
            }
        }

        /// <summary>
        /// Gets or sets the format for the title text. 
        /// </summary>
        [C1Category("Category.Appearance")]
        [C1Description("C1Calendar.TitleFormat")]
        [DefaultValue("MMMM yyyy")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public string TitleFormat
        {
            get
            {
                //return GetPropertyValue("TitleFormat", "MMMM yyyy");
                return GetPropertyValue("TitleFormat",
                    C1Localizer.GetString("C1Calendar.TitleFormat.DefaultValue", "MMMM yyyy", this.Culture));
            }
            set
            {
                SetPropertyValue("TitleFormat", value);
            }
        }

        /// <summary>
        /// A Boolean property that determines whether to display calendar title.
        /// </summary>
        [WidgetOption]
        [DefaultValue(true)]
        [C1Category("Category.Appearance")]
        [C1Description("C1Calendar.ShowTitle")]
        public bool ShowTitle
        {
            get
            {
                return GetPropertyValue("ShowTitle", true);
            }
            set
            {
                SetPropertyValue("ShowTitle", value);
            }
        }

        /// <summary>
        /// Gets or sets the display date for the first month view.  
        /// </summary>
        [WidgetOption]
        [C1Category("Category.Appearance")]
        [C1Description("C1Calendar.DisplayDate")]
        public DateTime DisplayDate
        {
            get
            {
                return GetPropertyValue("DisplayDate", DateTime.Now);
            }
            set
            {
                SetPropertyValue("DisplayDate", value);

#if !EXTENDER
                this.OnDisplayDateChanged(EventArgs.Empty);
#endif
            }
        }

        /// <summary>
        /// Gets or sets the number of day rows. 
        /// </summary>
        [C1Category("Category.MonthView")]
        [C1Description("C1Calendar.DayRows")]
        [DefaultValue(6)]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public int DayRows
        {
            get
            {
                return GetPropertyValue("DayRows", 6);
            }
            set
            {
                SetPropertyValue("DayRows", value);
            }
        }

        /// <summary>
        /// Gets or sets the number of day columns. 
        /// </summary>
        [C1Category("Category.MonthView")]
        [C1Description("C1Calendar.DayColumns")]
        [DefaultValue(7)]
        [NotifyParentProperty(true)]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete()]
        [WidgetOption]
        public int DayCols
        {
            get
            {
                return GetPropertyValue("DayCols", 7);
            }
            set
            {
                SetPropertyValue("DayCols", value);
            }
        }

        ///	<summary>
        ///	Gets or sets the format for the week day. 
        ///	</summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(WeekDayFormat.Short)]
        [C1Description("C1Calendar.WeekDayFormat")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public WeekDayFormat WeekDayFormat
        {
            get
            {
                return GetPropertyValue("WeekDayFormat", WeekDayFormat.Short);
            }
            set
            {
                SetPropertyValue("WeekDayFormat", value);
            }
        }

        /// <summary>
        /// A Boolean property that determines whether to display week days.
        /// </summary>
        [WidgetOption]
        [DefaultValue(true)]
        [C1Category("Category.Behavior")]
        [C1Description("C1Calendar.ShowWeekDays")]
        public bool ShowWeekDays
        {
            get
            {
                return GetPropertyValue("ShowWeekDays", true);
            }
            set
            {
                SetPropertyValue("ShowWeekDays", value);
            }
        }

        /// <summary>
        /// Determines whether to display week numbers. 
        /// </summary>
        [C1Category("Category.MonthView")]
        [C1Description("C1Calendar.ShowWeekNumbers")]
        [DefaultValue(false)]
        [WidgetOption]
        public bool ShowWeekNumbers
        {
            get
            {
                return GetPropertyValue("ShowWeekNumbers", false);
            }
            set
            {
                SetPropertyValue("ShowWeekNumbers", value);
            }
        }

        ///	<summary>
        ///	Defines different rules for determining the first week of the year. 
        ///	</summary>
        [WidgetOption]
        [DefaultValue(CalendarWeekRule.FirstDay)]
        [C1Category("Category.MonthView")]
        [C1Description("C1Calendar.WeekRule")]
        public CalendarWeekRule WeekRule
        {
            get
            {
                return GetPropertyValue("WeekRule", CalendarWeekRule.FirstDay);
            }
            set
            {
                SetPropertyValue("WeekRule", value);
            }
        }

        /// <summary>
        /// Determines the minimum date to display.
        /// </summary>
        [C1Category("Category.Dates")]
        [C1Description("C1Calendar.MinDate")]
        [DefaultValue(typeof(DateTime), "1/1/1900")]
        [WidgetOption]
        public DateTime MinDate
        {
            get
            {
                return GetPropertyValue("MinDate", new DateTime(1900, 1, 1));
            }
            set
            {
                DateTime d = this.MaxDate;
                if (value >= d)
                    return;

                SetPropertyValue("MinDate", value);
            }
        }

        /// <summary>
        /// Determines the maximum date to display. 
        /// </summary>
        [C1Category("Category.Dates")]
        [C1Description("C1Calendar.MaxDate")]
        [DefaultValue(typeof(DateTime), "12/31/2099")]
        [WidgetOption]
        public DateTime MaxDate
        {
            get
            {
                return GetPropertyValue("MaxDate", new DateTime(2099, 12, 31));
            }
            set
            {
                DateTime d = this.MinDate;
                if (value <= d)
                    return;

                SetPropertyValue("MaxDate", value);
            }
        }

        /// <summary>
        /// Determines whether to display the days of the next and/or previous month.
        /// </summary>
        [C1Category("Category.Appearance")]
        [C1Description("C1Calendar.ShowOtherMonthDays")]
        [DefaultValue(true)]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public bool ShowOtherMonthDays
        {
            get
            {
                return GetPropertyValue("ShowOtherMonthDays", true);
            }
            set
            {
                SetPropertyValue("ShowOtherMonthDays", value);
            }
        }

        /// <summary>
        /// Determines whether to add zeroes to days with only one digit.
        /// </summary>
        [WidgetOption]
        [DefaultValue(false)]
        [C1Category("Category.Appearance")]
        [C1Description("C1Calendar.ShowDayPadding")]
        public bool ShowDayPadding
        {
            get
            {
                return GetPropertyValue("ShowDayPadding", false);
            }
            set
            {
                SetPropertyValue("ShowDayPadding", value);
            }
        }

        /// <summary>
        /// Gets or sets the date selection mode on the calendar control that specifies whether the user can select a single day, a week, or an entire month. 
        /// </summary>
        [NotifyParentProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption()]
        [C1Category("Category.Behavior")]
        [C1Description("C1Calendar.SelectionMode")]
        public SelectionSettings SelectionMode
        {
            get
            {
                if (_selectionMode == null) _selectionMode = new SelectionSettings();

                return GetPropertyValue<SelectionSettings>("SelectionMode", _selectionMode);
            }
            set
            {
                SetPropertyValue("SelectionMode", value);
            }
        }

        /// <summary>
        /// Determines whether the preview buttons are displayed.
        /// </summary>
        [C1Category("Category.Preview")]
        [C1Description("C1Calendar.AllowPreview")]
        [DefaultValue(false)]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public bool AllowPreview
        {
            get
            {
                return GetPropertyValue("AllowPreview", false);
            }
            set
            {
                SetPropertyValue("AllowPreview", value);
            }
        }

        /// <summary>
        /// Determines whether users can change the view to month/year/decade while clicking on the calendar title.
        /// </summary>
        [C1Category("Category.Navigation")]
        [C1Description("C1Calendar.AllowQuickPick")]
        [DefaultValue(true)]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public bool AllowQuickPick
        {
            get
            {
                return GetPropertyValue("AllowQuickPick", true);
            }
            set
            {
                SetPropertyValue("AllowQuickPick", value);
            }
        }

        /// <summary>
        /// Gets or sets the format for the ToolTip.
        /// </summary>
        [C1Category("Category.Appearance")]
        [C1Description("C1Calendar.ToolTipFormat")]
        [NotifyParentProperty(true)]
        [DefaultValue("dddd, MMMM dd, yyyy")]
        [WidgetOption]
        public string ToolTipFormat
        {
            get
            {
                //return GetPropertyValue("ToolTipFormat", "dddd, MMMM dd, yyyy");
                return GetPropertyValue("ToolTipFormat",
                    C1Localizer.GetString("C1Calendar.ToolTipFormat.DefaultValue", "dddd, MMMM dd, yyyy", this.Culture));
            }
            set
            {
                SetPropertyValue("ToolTipFormat", value);
            }
        }

        /// <summary>
        /// Gets or sets the text for the 'previous' button's ToolTip. 
        /// </summary>
        [C1Category("Category.Navigation")]
        [C1Description("C1Calendar.PrevToolTip")]
        [DefaultValue("Previous")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public string PrevTooltip
        {
            get
            {
                //return GetPropertyValue("PrevTooltip", "Previous");
                return GetPropertyValue("PrevTooltip",
                    C1Localizer.GetString("C1Calendar.PrevTooltip.DefaultValue", "Previous", this.Culture));
            }
            set
            {
                SetPropertyValue("PrevTooltip", value);
            }
        }

        /// <summary>
        /// Gets or sets the text for the 'next' button's ToolTip. 
        /// </summary>
        [C1Category("Category.Navigation")]
        [C1Description("C1Calendar.NextToolTip")]
        [DefaultValue("Next")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public string NextTooltip
        {
            get
            {
                //return GetPropertyValue("NextTooltip", "Next");
                return GetPropertyValue("NextTooltip",
                    C1Localizer.GetString("C1Calendar.NextTooltip.DefaultValue", "Next", this.Culture));

            }
            set
            {
                SetPropertyValue("NextTooltip", value);
            }
        }

        /// <summary>
        /// Gets or sets the  "quick previous" button's ToolTip.
        /// </summary>
        [C1Category("Category.Navigation")]
        [C1Description("C1Calendar.QuickPrevToolTip")]
        [DefaultValue("Quick Previous")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public string QuickPrevTooltip
        {
            get
            {
                //return GetPropertyValue("QuickPrevTooltip", "Quick Previous");
                return GetPropertyValue("QuickPrevTooltip",
                    C1Localizer.GetString("C1Calendar.QuickPrevTooltip.DefaultValue", "Quick Previous", this.Culture));
            }
            set
            {
                SetPropertyValue("QuickPrevTooltip", value);
            }
        }

        /// <summary>
        /// Gets or sets the "quick next" button's ToolTip.
        /// </summary>
        [C1Category("Category.Navigation")]
        [C1Description("C1Calendar.QuickNextToolTip")]
        [DefaultValue("Quick Next")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public string QuickNextTooltip
        {
            get
            {
                //return GetPropertyValue("QuickNextTooltip", "Quick Next");
                return GetPropertyValue("QuickNextTooltip",
                   C1Localizer.GetString("C1Calendar.QuickNextTooltip.DefaultValue", "Quick Next", this.Culture));
            }
            set
            {
                SetPropertyValue("QuickNextTooltip", value);
            }
        }

        /// <summary>
        /// Gets or sets the "previous preview" button's ToolTip. 
        /// </summary>
        [C1Category("Category.Preview")]
        [C1Description("C1Calendar.PrevPreviewToolTip")]
        [DefaultValue("")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public string PrevPreviewTooltip
        {
            get
            {
                return GetPropertyValue("PrevPreviewTooltip", "");
            }
            set
            {
                SetPropertyValue("PrevPreviewTooltip", value);
            }
        }

        /// <summary>
        /// Gets or sets the format for the title text in the month view.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Calendar.MonthViewTitleFormat")]
        [NotifyParentProperty(true)]
        [DefaultValue("yyyy")]
        [WidgetOption]
        public string MonthViewTitleFormat
        {
            get
            {
                return GetPropertyValue("MonthViewTitleFormat", "yyyy");
            }
            set
            {
                SetPropertyValue("MonthViewTitleFormat", value);
            }
        }


        /// <summary>
        /// Gets or sets the "next preview" button's ToolTip. 
        /// </summary>
        [C1Category("Category.Preview")]
        [C1Description("C1Calendar.NextPreviewToolTip")]
        [DefaultValue("")]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public string NextPreviewTooltip
        {
            get
            {
                return GetPropertyValue("NextPreviewTooltip", "");
            }
            set
            {
                SetPropertyValue("NextPreviewTooltip", value);
            }
        }

        /// <summary>
        /// Determines the display type of navigation buttons.
        /// </summary>
        [WidgetOption]
        [DefaultValue(NavButtons.Default)]
        [C1Category("Category.Navigation")]
        [C1Description("C1Calendar.NavButtons")]
        public NavButtons NavButtons
        {
            get
            {
                return GetPropertyValue("NavButtons", NavButtons.Default);
            }
            set
            {
                SetPropertyValue("NavButtons", value);
            }
        }

        /// <summary>
        /// Determines the inc/dec steps when clicking the quick navigation button.
        /// </summary>
        [C1Category("Category.Navigation")]
        [C1Description("C1Calendar.QuickNavStep")]
        [DefaultValue(12)]
        [NotifyParentProperty(true)]
        [WidgetOption]
        public int QuickNavStep
        {
            get
            {
                return GetPropertyValue("QuickNavStep", 12);
            }
            set
            {
                SetPropertyValue("QuickNavStep", value);
            }
        }

        /// <summary>
        /// Determines the month slide direction.
        /// </summary>
        [WidgetOption]
        [DefaultValue(Orientation.Horizontal)]
        [C1Description("C1Calendar.Direction")]
        [C1Category("Category.Navigation")]
        public Orientation Direction
        {
            get
            {
                return GetPropertyValue("Direction", Orientation.Horizontal);
            }
            set
            {
                SetPropertyValue("Direction", value);
            }
        }

        /// <summary>
        /// Gets or sets the animation duration in milliseconds. 
        /// </summary>
        [C1Description("C1Calendar.Duration")]
        [C1Category("Category.Navigation")]
        [DefaultValue(250)]
        [NotifyParentProperty(true)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        [WidgetOption]
        public int Duration
        {
            get
            {
                return GetPropertyValue("Duration", 250);
            }
            set
            {
                SetPropertyValue("Duration", value);
            }
        }

        /// <summary>
        /// Determines the animations easing effect.
        /// </summary>
        [C1Description("C1Calendar.Easing")]
        [C1Category("Category.Navigation")]
        [DefaultValue(Easing.EaseInQuad)]
        [NotifyParentProperty(true)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        [WidgetOption]
        public Easing Easing
        {
            get
            {
                return GetPropertyValue("Easing", Easing.EaseInQuad);
            }
            set
            {
                SetPropertyValue("Easing", value);
            }
        }

        /// <summary>
        /// A Boolean property that determines whether the c1calendarwijcalendar widget is a pop-up calendar.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Calendar.PopupMode")]
        [NotifyParentProperty(true)]
        [DefaultValue(false)]
        [WidgetOption]
        public bool PopupMode
        {
            get
            {
                return GetPropertyValue("PopupMode", false);
            }
            set
            {
                SetPropertyValue("PopupMode", value);
            }
        }

        /// <summary>
        /// A Boolean property that determines whether to auto hide the calendar in pop-up mode when clicking outside of the calendar.
        /// </summary>
        [WidgetOption]
        [DefaultValue(true)]
        [C1Category("Category.Behavior")]
        [C1Description("C1Calendar.AutoHide")]
        public bool AutoHide
        {
            get
            {
                return GetPropertyValue("AutoHide", true);
            }
            set
            {
                SetPropertyValue("AutoHide", value);
            }
        }

        #endregion

        #region ** client events

        /// <summary>
        /// Occurs before the month view slides. 
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Calendar.OnClientBeforeSlide")]
        [DefaultValue("")]
        [WidgetOption]
        [WidgetOptionName("beforeSlide")]
        [WidgetEvent]
        public string OnClientBeforeSlide
        {
            get
            {
                return GetPropertyValue("OnClientBeforeSlide", "");
            }
            set
            {
                SetPropertyValue("OnClientBeforeSlide", value);
            }
        }

        /// <summary>
        /// Occurs after the month view slides. 
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Calendar.OnClientAfterSlide")]
        [DefaultValue("")]
        [WidgetOption]
        [WidgetOptionName("afterSlide")]
        [WidgetEvent]
        public string OnClientAfterSlide
        {
            get
            {
                return GetPropertyValue("OnClientAfterSlide", "");
            }
            set
            {
                SetPropertyValue("OnClientAfterSlide", value);
            }
        }

        /// <summary>
        /// Occurs before a date is selected. 
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Calendar.OnClientBeforeSelect")]
        [DefaultValue("")]
        [WidgetOption]
        [WidgetOptionName("beforeSelect")]
        [WidgetEvent]
        public string OnClientBeforeSelect
        {
            get
            {
                return GetPropertyValue("OnClientBeforeSelect", "");
            }
            set
            {
                SetPropertyValue("OnClientBeforeSelect", value);
            }
        }

        /// <summary>
        /// Occurs after a date is selected. 
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Calendar.OnClientAfterSelect")]
        [DefaultValue("")]
        [WidgetOption]
        [WidgetOptionName("afterSelect")]
        [WidgetEvent]
        public string OnClientAfterSelect
        {
            get
            {
                return GetPropertyValue("OnClientAfterSelect", "");
            }
            set
            {
                SetPropertyValue("OnClientAfterSelect", value);
            }
        }

        /// <summary>
        /// Occurs when the selected dates collection changed. 
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Calendar.OnClientSelectedDatesChanged")]
        [DefaultValue("")]
        [WidgetOption]
        [WidgetOptionName("selectedDatesChanged")]
        [WidgetEvent]
        public string OnClientSelectedDatesChanged
        {
            get
            {
                return GetPropertyValue("OnClientSelectedDatesChanged", "");
            }
            set
            {
                SetPropertyValue("OnClientSelectedDatesChanged", value);
            }
        }

        /// <summary>
        /// A callback function used for customizing the content, style and attributes of a day cell.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Calendar.OnClientCustomizeDate")]
        [DefaultValue("")]
        [WidgetOption]
        [WidgetOptionName("customizeDate")]
        [WidgetEvent]
        public string OnClientCustomizeDate
        {
            get
            {
                return GetPropertyValue("OnClientCustomizeDate", "");
            }
            set
            {
                SetPropertyValue("OnClientCustomizeDate", value);
            }
        }

        #endregion
    }
}
