﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;


#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Calendar
#else
namespace C1.Web.Wijmo.Controls.C1Calendar
#endif
{
    /// <summary>
    ///  Represents the date selection mode on the calendar control that specifies 
    ///  whether the user can select a single day, a week, or an entire month. 
    /// </summary>
    [PersistChildren(false), ParseChildren(true)]
    public class SelectionSettings : Settings 
    {
        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue(true)]
        public bool Day
        {
            get
            {
                return GetPropertyValue<bool>("Day", true);
            }
            set
            {
                SetPropertyValue<bool>("Day", value);
            }
        }

        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue(true)]
        public bool Days
        {
            get
            {
                return GetPropertyValue<bool>("Days", true);
            }
            set
            {
                SetPropertyValue<bool>("Days", value);
            }
        }

        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue(false)]
        public bool WeekDay
        {
            get
            {
                return GetPropertyValue<bool>("WeekDay", false);
            }
            set
            {
                SetPropertyValue<bool>("WeekDay", value);
            }
        }

        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue(false)]
        public bool WeekNumber
        {
            get
            {
                return GetPropertyValue<bool>("WeekNumber", false);
            }
            set
            {
                SetPropertyValue<bool>("WeekNumber", value);
            }
        }

        [NotifyParentProperty(true)]
        [WidgetOption]
        [DefaultValue(false)]
        public bool Month
        {
            get
            {
                return GetPropertyValue<bool>("Month", false);
            }
            set
            {
                SetPropertyValue<bool>("Month", value);
            }
        }
    }
}
