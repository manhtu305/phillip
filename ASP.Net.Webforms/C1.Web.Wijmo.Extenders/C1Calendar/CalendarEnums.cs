﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Calendar
#else
namespace C1.Web.Wijmo.Controls.C1Calendar
#endif
{
	/// <summary>
	/// Determines the format for displaying the week day.
	/// </summary>
	public enum WeekDayFormat
	{
		/// <summary>
		/// Displays the first two letters of the day of the week.
		/// </summary>
		Short = 0,
		/// <summary>
		/// Displays the full name of the day of the week.
		/// </summary>
		Full = 1,
		/// <summary>
		/// Displays the first single letter of the day of the week.
		/// </summary>
		FirstLetter = 2,
		/// <summary>
		/// Displays the abbreviated name of the day of the week.
		/// </summary>
		Abbreviated = 3
	}

	/// <summary>
	/// Enumeration that determines the display of navigation buttons.
	/// </summary>
	public enum NavButtons
	{
		/// <summary>
		/// Displays the previous and next buttons.
		/// </summary>
		Default = 0,

		/// <summary>
		/// Displays the previous, next, quick previous and quick next buttons.
		/// </summary>
		Quick = 1,

		/// <summary>
		/// No navigation buttons are displayed.
		/// </summary>
		None = 2
	}
   
	[Flags]
	internal enum DayType
	{
		General= 0,
		WeekEnd= 1,
		OtherMonth= 2,
		OutOfRange= 4,
		Today= 8,
		Custom= 16,
		Disabled= 32,
		Selected= 64,
		Gap= 128
	};

	/// <summary>
	/// Enumeration that determines the initial view type of Calendar.
	/// </summary>
	public enum ViewType
	{
		/// <summary>
		/// Displays the days in Calendar.
		/// </summary>
		Day = 0,

		/// <summary>
		/// Displays the months in Calendar.
		/// </summary>
		Month = 1,

		/// <summary>
		/// Displays the years in Calendar.
		/// </summary>
		Year = 2,

		/// <summary>
		/// Displays the decades in Calendar.
		/// </summary>
		Decade = 3
	}

}
