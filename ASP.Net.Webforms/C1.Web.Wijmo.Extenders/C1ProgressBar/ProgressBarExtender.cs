﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Extenders.Converters;

[assembly: TagPrefix("C1.Web.Wijmo.Extenders.ProgressBar", "Progressbar")]
namespace C1.Web.Wijmo.Extenders.C1ProgressBar
{

	/// <summary>
	/// Extender for the progressbar widget.
	/// </summary>
	[ToolboxItem(true)]
	[TargetControlType(typeof(Panel))]
	[ToolboxBitmap(typeof(C1ProgressBarExtender), "ProgressBar.png")]
	[LicenseProviderAttribute()]
	public partial class C1ProgressBarExtender : WidgetExtenderControlBase
	{
		#region ** fields
		private bool _productLicensed = false;
		#endregion end of ** fields.

		#region ** ctors
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ProgressBarExtender"/> class.
		/// </summary>
		public C1ProgressBarExtender()
		{
			this.AnimationOptions = new SlideAnimation();
			VerifyLicense();
		}
		#endregion end of ** ctors.

		internal override bool IsWijMobileExtender
		{
			get
			{
				return false;
			}
		}

		#region ** methods
		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1ProgressBarExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}
		#endregion end of ** methods.
	}

}
