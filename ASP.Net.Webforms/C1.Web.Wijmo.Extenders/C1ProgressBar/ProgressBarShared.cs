﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using C1.Web.Wijmo;
using System.Web.UI.Design;
using System.Drawing.Design;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1ProgressBar", "wijmo")] 
namespace C1.Web.Wijmo.Extenders.C1ProgressBar
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1ProgressBar", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1ProgressBar
#endif
{
    [WidgetDependencies(
        typeof(WijProgressBar)
#if !EXTENDER
, "extensions.c1progressbar.js",
ResourcesConst.C1WRAPPER_OPEN
#endif
)]
#if EXTENDER
	public partial class C1ProgressBarExtender
#else
    public partial class C1ProgressBar
#endif
    {

        private SlideAnimation _animationOptions = null;

        #region ** options
        /// <summary>
        /// Gets or sets the label's alignment on the progress bar. Possible values are:
        /// East The label is aligned on the right side of the progress bar.
        /// West The label is aligned on the left side of the progress bar.
        /// Center The label is aligned in the center of the progress bar.
        /// North The label is aligned at the top of the progress bar.
        /// South The label is aligned at the bottom of the progress bar.
        /// Running The label is aligned with the edge of the progress bar.
        /// </summary>
        [C1Category("Category.Appearance")]
        [C1Description("C1ProgressBar.LabelAlign")]
        [DefaultValue(LabelAlign.Center)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public LabelAlign LabelAlign
        {
            get
            {
                return GetPropertyValue("LabelAlign", LabelAlign.Center);
            }
            set
            {
                SetPropertyValue("LabelAlign", value);
            }
        }

        /// <summary>
        /// Gets or sets the value of the progress bar.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1ProgressBar.Value")]
        [DefaultValue(0)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int Value
        {
            get
            {
                return GetPropertyValue("Value", 0);
            }
            set
            {
                SetPropertyValue("Value", value);
            }
        }

        /// <summary>
        /// Gets or sets the direction from which the progress bar will start to fill. 
        /// Possible options include: East (right-to-left), West (left-to-right), North (top-to-bottom), or South (bottom-to-top).
        /// </summary>
        [C1Category("Category.Appearance")]
        [C1Description("C1ProgressBar.FillDirection")]
        [DefaultValue(FillDirection.East)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public FillDirection FillDirection
        {
            get
            {
                return GetPropertyValue("FillDirection", FillDirection.East);
            }
            set
            {
                SetPropertyValue("FillDirection", value);
            }
        }

        /// <summary>
        /// The remarks need to be spaced with paragraph marks. They are currently running together:
        /// The available formats are as follows:		
        /// {0} or {ProgressValue} Expresses the current progress value.
        /// {1} or {PercentProgress} Expresses the current percent of the progress bar.
        /// {2} or {RemainingProgress} Expresses the remaining progress of the progress bar.
        /// {3} or {PercentageRemaining} Expresses the remaining percent of the progress bar.
        /// {4} or {Min} Expresses the minimum value of the progress bar.
        /// {5} or {Max} Expresses the maximum value of the progress bar.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1ProgressBar.LabelFormatString")]
        [DefaultValue("{1}%")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string LabelFormatString
        {
            get
            {
                return GetPropertyValue("LabelFormatString", "{1}%");
            }
            set
            {
                SetPropertyValue("LabelFormatString", value);
            }
        }

        /// <summary>
        /// Determines the increment, in pixels, that the indicator image moves forward.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1ProgressBar.IndicatorIncrement")]
        [DefaultValue(1)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int IndicatorIncrement
        {
            get
            {
                return GetPropertyValue("IndicatorIncrement", 1);
            }
            set
            {
                SetPropertyValue("IndicatorIncrement", value);
            }
        }

        /// <summary>
        /// Gets or sets the URL of the image for the progress indicator.
        /// </summary>
        [C1Category("Category.Appearance")]
        [C1Description("C1ProgressBar.IndicatorImage")]
        [DefaultValue("")]
        [Editor(typeof(ImageUrlEditor), typeof(UITypeEditor))]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public string IndicatorImage
        {
            get
            {
                return GetPropertyValue("IndicatorImage", "");
            }
            set
            {
                SetPropertyValue("IndicatorImage", value);
            }
        }

        /// <summary>
        /// Gets or sets the delay time of the progress bar in milliseconds before doing animation.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1ProgressBar.AnimationDelay")]
        [DefaultValue(0)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int AnimationDelay
        {
            get
            {
                return GetPropertyValue("AnimationDelay", 0);
            }
            set
            {
                SetPropertyValue("AnimationDelay", value);
            }
        }

        /// <summary>
        /// Gets or sets the option parameter of the jQuery's animation.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1ProgressBar.AnimationOptions")]
        [WidgetOption]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public SlideAnimation AnimationOptions
        {
            get
            {
                if (this._animationOptions == null)
                {
                    this._animationOptions = new SlideAnimation();
                }

                return this._animationOptions;
            }
            set
            {
                this._animationOptions = value;
            }
        }

        /// <summary>
        /// Gets or sets the maximum value of the progress bar.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1ProgressBar.MaximumValue")]
        [DefaultValue(100)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int MaxValue
        {
            get
            {
                return GetPropertyValue("MaxValue", 100);
            }
            set
            {
                if (value < MinValue)
                {
                    throw new Exception("MaxValue of WijProgressBar can't be less than or equal to MinValue");
                }

                SetPropertyValue("MaxValue", value);
            }
        }

        /// <summary>
        /// Gets or sets the minimum value of the progress bar.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1ProgressBar.MinimumValue")]
        [DefaultValue(0)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int MinValue
        {
            get
            {
                return GetPropertyValue("MinValue", 0);
            }
            set
            {
                if (value > MaxValue)
                {
                    throw new Exception("MinValue of WijProgressBar can't be more than or equal to MaxValue");
                }
                SetPropertyValue("MinValue", value);
            }
        }

        /// <summary>
        /// Gets or sets the format of the tooltip for the progress bar. 
        /// The available formats are as follows:
        /// {0} or {ProgressValue} Expresses the current progress value.
        /// {1} or {PercentProgress} Expresses the current percent of the progress bar.
        /// {2} or {RemainingProgress} Expresses the remaining progress of the progress bar.
        /// {3} or {PercentageRemaining} Expresses the remaining percent of the progress bar.
        /// {4} or {Min} Expresses the minimum value of the progress bar.
        /// {5} or {Max} Expresses the maximum value of the progress bar.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1ProgressBar.ToolTip")]
        [DefaultValue("{1}%")]
        [WidgetOption]
        [WidgetOptionName("ToolTipFormatString")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
        public override string ToolTip
#else
		public string ToolTip
#endif
        {
            get
            {
                return GetPropertyValue("ToolTip", "{1}%");
            }
            set
            {
                SetPropertyValue("ToolTip", value);
            }
        }
        #endregion end of ** options.

        #region ** client events
        /// <summary>
        /// Fires when the progress bar is changing.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1ProgressBar.OnClientProgressChanging")]
        [WidgetEvent("e")]
        [DefaultValue("")]
        [WidgetOptionName("progressChanging")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientProgressChanging
        {
            get
            {
                return GetPropertyValue("OnClientProgressChanging", "");
            }
            set
            {
                SetPropertyValue("OnClientProgressChanging", value);
            }
        }

        /// <summary>
        /// Fires before the progress bar changes.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1ProgressBar.OnClientBeforeProgressChanging")]
        [WidgetEvent("e")]
        [DefaultValue("")]
        [WidgetOptionName("beforeProgressChanging")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientBeforeProgressChanging
        {
            get
            {
                return GetPropertyValue("OnClientBeforeProgressChanging", "");
            }
            set
            {
                SetPropertyValue("OnClientBeforeProgressChanging", value);
            }
        }

        /// <summary>
        /// Fires when the progress bar is changed.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1ProgressBar.OnClientProgressChanged")]
        [WidgetEvent("e")]
        [DefaultValue("")]
        [WidgetOptionName("progressChanged")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientProgressChanged
        {
            get
            {
                return GetPropertyValue("OnClientProgressChanged", "");
            }
            set
            {
                SetPropertyValue("OnClientProgressChanged", value);
            }
        }
        #endregion end of ** client events.

        /// <summary>
        /// Determines whether the AnimationOptions property should be serialized.
        /// </summary>
        /// <returns>
        /// Returns true if any subproperty is changed, otherwise return false.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeAnimationOptions()
        {
            return this.AnimationOptions.Disabled != false
                || this.AnimationOptions.Duration != 400
                || this.AnimationOptions.Easing != Easing.Swing;
        }
    }
}
