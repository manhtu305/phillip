﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1ProgressBar
#else
namespace C1.Web.Wijmo.Controls.C1ProgressBar
#endif
{

	#region ** LabelAlign enum
	/// <summary>
	/// Use the members of this enumeration to set the value of the LabelAlign property.
	/// </summary>
	public enum LabelAlign
	{
		/// <summary>
        /// The label is aligned on the right side of the progress bar.
		/// </summary>
		East = 0,
		/// <summary>
        /// The label is aligned on the left side of the progress bar.
		/// </summary>
		West = 1,
		/// <summary>
        /// The label is aligned in the center of the progress bar.
		/// </summary>
		Center = 2,
		/// <summary>
        /// The label is aligned at the top of the progress bar.
		/// </summary>
		North = 4,
		/// <summary>
        /// The label is aligned at the bottom of the progress bar.
		/// </summary>
		South = 8,
		/// <summary>
        /// The label is aligned with the edge of the progress bar.
		/// </summary>
		Running = 16
	}
	#endregion end of ** LabelAlign enum

	#region ** FillDirection enum
	/// <summary>
    /// Determines the direction from which the progress bar will start to fill.
	/// </summary>
	public enum FillDirection
	{
		/// <summary>
        /// Fills from right-to-left.
		/// </summary>
		East = 0,
		/// <summary>
        /// Fills from left-to-right.
		/// </summary>
		West = 1,
		/// <summary>
        /// Fills from top-to-bottom.
		/// </summary>
		North = 2,
		/// <summary>
        /// Fills from bottom-to-top.
		/// </summary>
		South = 4
	}
	#endregion end of ** FillDirection enum
}
