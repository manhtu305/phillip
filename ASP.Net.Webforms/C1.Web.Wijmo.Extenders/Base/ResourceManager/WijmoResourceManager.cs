﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Wijmo.Licensing;
#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
	internal enum ResourceType
	{
		Css,
		Script
	}

	internal enum CDNResourceType
	{
		JQuery,
		JQueryUI,
		JQueryMobile,
		JQueryMobileCss,
		JQueryMobileCssStructure,
		BootStrapCss,
		BootStrapJs
	}

#if !EXTENDER 
	[SmartAssembly.Attributes.DoNotObfuscate]
#endif
	internal static class WijmoResourceManager
	{
		#region ** Fields **

		private static readonly Assembly currentAssembly = typeof(WijmoResourceManager).Assembly;
		private static Dictionary<DateTime, string> dateStamps = new Dictionary<DateTime, string>();
		private static readonly object dateStampsLock = new object();
		private static Dictionary<string, WijmoResourceInfo> embeddedScriptsHash = new Dictionary<string, WijmoResourceInfo>();
		private static Dictionary<string, WijmoResourceInfo> embeddedCssHash = new Dictionary<string, WijmoResourceInfo>();
		private static object findResourcesLock = new object();
		private static object findAllWebResourcesLock = new object();

#if EXTENDER
		private const string scriptControlID = "wijmoScriptResourceForExtender";
		private const string cssControlID = "wijmoCssResourceForExtender";
#else
		private const string scriptControlID = "wijmoScriptResourceForControl";
		private const string cssControlID = "wijmoCssResourceForControl";
#endif
		private const string resourceHiddenField = "wijmoResources";
		internal const string WijmoThemeFileName = "jquery-wijmo.css";
		private const string themeCssClass = "wijmo-stylesheet-wijmo_theme";
		private static Dictionary<CDNResourceType, string> cdnDependencePaths;
		private const string jQueryCDNPath = "http://code.jquery.com";
		private const string BootstrapCDNPath = "http://netdna.bootstrapcdn.com/bootstrap/" + VersionConst.bootstrapVer;
		#endregion

		#region ** Constructor **

		static WijmoResourceManager()
		{
			RegisterAllResources(currentAssembly);
		}

		#endregion

		/// <summary>
		/// The controls should be added before onPreRender.
		/// </summary>
		/// <param name="control"></param>
		internal static void RegisterResourceControl(IWijmoWidgetSupport control)
		{
			bool isCombinedScript = IsCombinedScript(control);
			Control container = GetResourceControlContainer((Control)control);
			ResourceControl cssControl = container.FindControl(cssControlID) as ResourceControl;
			if (cssControl == null)
			{
				cssControl = new ResourceControl(ResourceType.Css);
				cssControl.ID = cssControlID;
				cssControl.EnableViewState = false;
				container.Controls.AddAt(GetControlIndex((IWijmoWidgetSupport)control), cssControl);
			}
			ResourceControl scriptControl = container.FindControl(scriptControlID) as ResourceControl;
			if (scriptControl == null)
			{
				scriptControl = new ResourceControl(ResourceType.Script);
				scriptControl.ID = scriptControlID;
				scriptControl.EnableCombinedJavaScripts = isCombinedScript;
				container.Controls.AddAt(GetControlIndex((IWijmoWidgetSupport)control), scriptControl);
			}
		}

		private static int GetControlIndex(IWijmoWidgetSupport control)
		{
			Control container = GetResourceControlContainer((Control)control);
			int index = 0;
			foreach (Control c in container.Controls)
			{
				Type type = c.GetType();
				string typeName = type.FullName;
				if (type == typeof(System.Web.UI.HtmlControls.HtmlMeta) ||
					typeName == "C1.Web.Wijmo.Controls.ResourceControl" || typeName == "C1.Web.Wijmo.Extenders.ResourceControl")
				{
					index++;
				}
			}
			return index;
		}

		private static Control GetResourceControlContainer(Control control)
		{
			Page page = control.Page;
			Control container = page;
			if (page.Header != null)
			{
				container = page.Header;
			}
			else if (page.Form != null)
			{
				container = page.Form;
			}
			return container;
		}


		internal static bool ProcessRequestForWijmoCombinedJS()
		{
			HttpRequest request = WijmoHttpHandler.GetRequest();
			HttpResponse response = WijmoHttpHandler.GetResponse();
			HttpCachePolicy Cache = response.Cache;

			List<WijmoResourceInfo> requestedResources = GetRequestedResources();
			if (requestedResources.Count == 0)
			{
				response.StatusCode = 0x130;
				response.AddHeader("content-length", "0");
				WijmoHttpHandler.EndResponse();
				return true;
			}
			DateTime scriptModificationDate = GetWijmoScriptsModificationDate(requestedResources);
			string str = request.Headers["If-Modified-Since"];
			if (scriptModificationDate.ToString("R") == str)
			{
				response.ContentType = GetResourcesContentType(requestedResources);
				response.StatusCode = 0x130;
				response.AddHeader("content-length", "0");
				WijmoHttpHandler.EndResponse();
				return true;
			}
			response.Clear();
			try
			{
				foreach (WijmoResourceInfo data in requestedResources)
				{
					data.WriteContent(response.OutputStream);
				}
				Cache.SetCacheability(HttpCacheability.Public);

				Cache.SetLastModified(scriptModificationDate);
				Cache.SetExpires(scriptModificationDate.AddDays(365.0));
				Cache.SetMaxAge(new TimeSpan(0x16d, 0, 0, 0));
				response.ContentType = GetResourcesContentType(requestedResources);
			}
			catch (Exception exception)
			{
				response.ClearHeaders();
				response.ClearContent();
				response.StatusCode = 500;
				response.StatusDescription = exception.Message;
			}
			WijmoHttpHandler.EndResponse();
			return true;
		}

		#region ** CDN

		/// <summary>
		/// Get the specified CDN reference path.
		/// </summary>
		/// <param name="cdnPath"></param>
		/// <param name="referencePath"></param>
		/// <param name="control"></param>
		/// <returns></returns>
		private static string GetCDNReference(IWijmoWidgetSupport control, CDNResourceType key)
		{
			InitCDNDependencyPaths(control);
			if (cdnDependencePaths.ContainsKey(key))
			{
				return cdnDependencePaths[key];
			}
			return null;
		}

		private static void InitCDNDependencyPath(string path, Dictionary<string, CDNResourceType> dic)
		{
			foreach (string name in dic.Keys)
			{
				if (path.Contains(name))
				{
					cdnDependencePaths.Add(dic[name], path);
					break;
				}
			}
		}

		private static void InitCDNDependencyPaths(IWijmoWidgetSupport control)
		{
			if (cdnDependencePaths == null)
			{
				cdnDependencePaths = new Dictionary<CDNResourceType, string>();
				Dictionary<CDNResourceType, string> defaultCDNDependencies = new Dictionary<CDNResourceType, string>() 
				{
 					{CDNResourceType.JQuery, string.Format("{0}/jquery-{1}.min.js", jQueryCDNPath, control.WijmoControlMode == WijmoControlMode.Web ? VersionConst.jQueryVer : VersionConst.jQueryVerWithJQM)},
					{CDNResourceType.JQueryUI, string.Format("{0}/ui/{1}/jquery-ui.min.js", jQueryCDNPath, VersionConst.jQueryUiVer)},
					{CDNResourceType.JQueryMobile, string.Format("{0}/mobile/{1}/jquery.mobile-{1}.min.js", jQueryCDNPath, VersionConst.jQueryMobileVer)},
					{CDNResourceType.JQueryMobileCss, string.Format("{0}/mobile/{1}/jquery.mobile-{1}.min.css", jQueryCDNPath, VersionConst.jQueryMobileVer) },
					{CDNResourceType.JQueryMobileCssStructure, string.Format("{0}/mobile/{1}/jquery.mobile.structure-{1}.min.css", jQueryCDNPath, VersionConst.jQueryMobileVer)},
					{CDNResourceType.BootStrapJs, string.Format("{0}/js/bootstrap.min.js", BootstrapCDNPath)},
					{CDNResourceType.BootStrapCss, string.Format("{0}/css/bootstrap.min.css", BootstrapCDNPath)}
				};
				if (control.CDNDependencyPaths != null)
				{
					Dictionary<string, CDNResourceType> jsDependencies = new Dictionary<string, CDNResourceType>
					{
						{"jquery-ui", CDNResourceType.JQueryUI},
						{"jquery.mobile", CDNResourceType.JQueryMobile},
						{"jquery", CDNResourceType.JQuery},
						{"bootstrap", CDNResourceType.BootStrapJs}
					};

					Dictionary<string, CDNResourceType> cssDependencies = new Dictionary<string, CDNResourceType>() 
					{ 
						{"jquery.mobile.structure", CDNResourceType.JQueryMobileCssStructure},
						{"jquery.mobile", CDNResourceType.JQueryMobileCss},
						{"bootstrap", CDNResourceType.BootStrapCss}
					};

					foreach (string path in control.CDNDependencyPaths)
					{
						if (Path.GetExtension(path) == ".js")
						{
							InitCDNDependencyPath(path, jsDependencies);
						}
						else
						{
							InitCDNDependencyPath(path, cssDependencies);
						}
					}
				}

				foreach (CDNResourceType key in defaultCDNDependencies.Keys)
				{
					if (!cdnDependencePaths.ContainsKey(key))
					{
						cdnDependencePaths.Add(key, defaultCDNDependencies[key]);
					}
				}
			}
		}
		#endregion

		#region register script resources
		// This method is to register script with ScriptManager.
		internal static List<ScriptReference> GetScriptReferences(IWijmoWidgetSupport control)
		{
			List<ScriptReference> references = new List<ScriptReference>();
			Page page = ((Control)control).Page;

			ResourceControl rc = GetResourceControlContainer((Control)control).FindControl(scriptControlID) as ResourceControl;
			if (IsCombinedScript(control))
			{
				// sync the loaded items.
				List<WijmoResourceInfo> items = rc.Items;
				items = SyncResourceList((Control)control, items);
				if (items.Count > 0)
				{
					references.Add(new ScriptReference(GetResourcesUrlString(page, items)));
				}
			}
			else
			{
				foreach (WijmoResourceInfo item in rc.Items)
				{
					// if the resource is cdn resources, the index is -1.  
					if (item.Index == -1)
					{
						references.Add(new ScriptReference(item.Name));
					}
					else
					{
						references.Add(new ScriptReference(item.Name, control.GetType().Assembly.FullName));
					}
				}
			}
			return references;
		}

		private static List<WijmoResourceInfo> SyncResourceList(Control c, List<WijmoResourceInfo> resources)
		{
			// if runtime
			if (HttpContext.Current != null)
			{
				string loadedResources = HttpContext.Current.Request.Form[resourceHiddenField];
				if (!string.IsNullOrEmpty(loadedResources) && ScriptManager.GetCurrent(c.Page).IsInAsyncPostBack)
				{
					string[] ids = loadedResources.Split(';');
                    var validIds = new List<string>();
					foreach (string id in ids)
					{
						WijmoResourceInfo resource = FindScriptResourceByID(id);
                        if (resource != null)
                        {
                            // only return the valid ids to client. client may change and pass invalid id which causes XSS attack.
                            validIds.Add(id);

                            // if the resource is bootstrap, still rendered this resource again.
                            if (resources.Contains(resource) && !resource.Name.Contains("resources.wijmo.bootstrap"))
                            {
                                resources.Remove(resource);
                            }
                        }
					}

                    loadedResources = string.Join(";", validIds.ToArray());

					foreach (WijmoResourceInfo resource in resources)
					{
						loadedResources += ";" + resource.Index.ToString();
					}
				}
				else
				{
					loadedResources = GetResourcesListString(resources);
				}
				ScriptManager.RegisterHiddenField(c, resourceHiddenField, loadedResources);
			}
			return resources;
		}


		private static List<WijmoResourceInfo> GetJQueryScriptResources(IWijmoWidgetSupport control)
		{
			List<WijmoResourceInfo> resources = new List<WijmoResourceInfo>();
			if (IsUseCDN(control))
			{
				if (control.WijmoControlMode == WijmoControlMode.Mobile)
				{
					resources.Add(new WijmoResourceInfo(GetCDNReference(control, CDNResourceType.JQuery), ResourceType.Script, 0));
					resources.Add(FindScriptResourceByName(ResourcesConst.WIJMO_MOBILE_JS));
					resources.Add(new WijmoResourceInfo(GetCDNReference(control, CDNResourceType.JQueryMobile), ResourceType.Script, 0));
				}
				else
				{
					resources.Add(new WijmoResourceInfo(GetCDNReference(control, CDNResourceType.JQuery), ResourceType.Script, 0));
					resources.Add(new WijmoResourceInfo(GetCDNReference(control, CDNResourceType.JQueryUI), ResourceType.Script, 0));
				}
			}
			else
			{
				if (control.WijmoControlMode == WijmoControlMode.Mobile)
				{
					resources.Add(FindScriptResourceByName(ResourcesConst.JQUERY_JQM));
					resources.Add(FindScriptResourceByName(ResourcesConst.WIJMO_MOBILE_JS));
					resources.Add(FindScriptResourceByName(ResourcesConst.JQUERY_MOBILE));
				}
				else
				{
					resources.Add(FindScriptResourceByName(ResourcesConst.JQUERY));
					resources.Add(FindScriptResourceByName(ResourcesConst.JQUERY_UI));
				}
			}
			return resources;
		}

		internal static bool IsDesignMode()
		{
			return HttpContext.Current == null;
		}

		private static bool IsCombinedScript(IWijmoWidgetSupport control)
		{
			// if design mode, using uncombined mode.
			return !IsDesignMode() && control.EnableCombinedJavaScripts && !IsUseCDN(control) && control.WijmoControlMode == WijmoControlMode.Web;
		}

		private static bool IsUseCDN(IWijmoWidgetSupport control)
		{
			return !IsDesignMode() && control.UseCDN;
		}

#if !EXTENDER
		internal static bool IsControlInAppviewPage(IWijmoWidgetSupport control)
		{
			bool isControlInAppviewPage = false;
			Control ctrl = ((Control)control).Parent;
			if (control.WijmoControlMode == WijmoControlMode.Web)
			{
				return false;
			}
			while (ctrl != ((Control)control).Page)
			{
				if (ctrl is C1AppView.C1AppViewPage)
				{
					isControlInAppviewPage = true;
					break;
				}
				ctrl = ctrl.Parent;
			}
			return isControlInAppviewPage;
		}
#endif

#if !EXTENDER 
		[SmartAssembly.Attributes.DoNotObfuscate]
#endif
		public static List<WijmoResourceInfo> GetScriptReferenceForControl(IWijmoWidgetSupport control)
		{
			List<WijmoResourceInfo> resources = new List<WijmoResourceInfo>();
			bool isCombinedScript = IsCombinedScript(control);
			bool isUseCDN = IsUseCDN(control);
#if EXTENDER
			bool isControlInAppView = false;
#else
			bool isControlInAppView = IsControlInAppviewPage(control);
#endif
			// control in appview don't need the common references.
			if (!isControlInAppView)
			{
				resources.AddRange(GetJQueryScriptResources(control));
			}

			OrderedDictionary jsReferences = WidgetDependenciesResolver.GetJsReferences(control);
			IDictionaryEnumerator idm = jsReferences.GetEnumerator();
			while (idm.MoveNext())
			{
				string resourceName = (string)idm.Value;

				// handler the cultures
				if (resourceName.Contains(ResourcesConst.JS_ROOT + ".globalize.cultures.js") && control is ICultureControl && IsCombinedScript(control))
				{
					resources.Add(FindScriptResourceByName(currentAssembly, WidgetDependenciesResolver.GetCultureResource(((ICultureControl)control).Culture)));
					continue;
				}

				if (isCombinedScript && control.WijmoControlMode == WijmoControlMode.Web)
				{
					if (resourceName.Contains(ResourcesConst.WIJMO_OPEN_JS) || resourceName.Contains(ResourcesConst.WIJMO_PRO_JS))
					{
						continue;
					}
#if !EXTENDER
					if (resourceName.Contains(ResourcesConst.C1WRAPPER_PRO) || resourceName.Contains(ResourcesConst.C1WRAPPER_OPEN))
					{
						continue;
					}

					if (resourceName.Contains(ResourcesConst.WIDGETS_CONTROL) || resourceName.Contains(ResourcesConst.C1WRAPPER_CONTROL))
					{
						continue;
					}
#endif
				}
				else
				{
					if (IsPartOfOpenPro(resourceName))
					{
						continue;
					}

					// if control is in appview, don't need to load open and pro resources.
					if (isControlInAppView)
					{
						if (resourceName.Contains(ResourcesConst.WIJMO_OPEN_JS) || resourceName.Contains(ResourcesConst.WIJMO_PRO_JS))
						{
							continue;
						}

#if !EXTENDER
						// if control is in appview, do not load the extensions.  it loaded by appview.
						if (resourceName.Contains(ResourcesConst.C1WRAPPER_PRO) || resourceName.Contains(ResourcesConst.C1WRAPPER_OPEN) || resourceName.Contains(ResourcesConst.C1WRAPPER_CONTROL))
						{
							continue;
						}
#endif
					}
#if !EXTENDER
					// if used without appview
					if (control.WijmoControlMode == WijmoControlMode.Mobile)
					{
						if (resourceName.Contains(ResourcesConst.WIJMO_OPEN_JS) || resourceName.Contains(ResourcesConst.WIJMO_PRO_JS))
						{
							resources.Add(FindScriptResourceByName(ResourcesConst.WIJMO_OPEN_JS));
							resources.Add(FindScriptResourceByName(ResourcesConst.WIJMO_PRO_JS));
							continue;
						}

						if (resourceName.Contains(ResourcesConst.C1WRAPPER_PRO) || resourceName.Contains(ResourcesConst.C1WRAPPER_OPEN) || resourceName.Contains(ResourcesConst.C1WRAPPER_CONTROL))
						{
							resources.Add(FindScriptResourceByName(ResourcesConst.MOBILE_CONTROL));
							continue;
						}
					}

					if (IsPartOfOpenProExtension(resourceName))
					{
						continue;
					}

					if (IsPartOfControlWidgets(resourceName))
					{
						continue;
					}
#endif

					if (isUseCDN)
					{
						if (resourceName.Contains(ResourcesConst.WIJMO_OPEN_JS))
						{
							resources.Add(new WijmoResourceInfo(string.Format("{0}jquery.wijmo-open.all.{1}.min.js", control.CDNPath, VersionConst.WijmoVer), ResourceType.Script, WijmoResourceInfo.WijmoWidgetsScriptPriority));
							continue;
						}
						if (resourceName.Contains(ResourcesConst.WIJMO_PRO_JS))
						{
							resources.Add(new WijmoResourceInfo(string.Format("{0}jquery.wijmo-pro.all.{1}.min.js", control.CDNPath, VersionConst.WijmoVer), ResourceType.Script, WijmoResourceInfo.WijmoWidgetsScriptPriority));
							continue;
						}
					}
				}
				resources.Add(FindScriptResourceByName(currentAssembly, (string)idm.Value));
			}
			if (control.WijmoCssAdapter == "bootstrap" && control.WijmoControlMode == WijmoControlMode.Web)
			{
				if (isUseCDN)
				{
					resources.Add(new WijmoResourceInfo(GetCDNReference(control, CDNResourceType.BootStrapJs), ResourceType.Script, WijmoResourceInfo.BootstrapScriptPriority));
				}
				else
				{
					resources.Add(FindScriptResourceByName(ResourcesConst.BOOTSTRAP_JS));
				}
				resources.Add(FindScriptResourceByName(ResourcesConst.WIJMO_BOOTSTRAP_JS));
			}

			if (control.WijmoControlMode == WijmoControlMode.Mobile)
			{
				resources.Add(FindScriptResourceByName(ResourcesConst.WIJMO_MOBILE_JS));
			}
			return resources;
		}

		public static void RegisterScriptResource(IWijmoWidgetSupport control)
		{
			ResourceControl scriptControl = GetResourceControlContainer((Control)control).FindControl(scriptControlID) as ResourceControl;
			List<WijmoResourceInfo> resources = GetScriptReferenceForControl(control);
			scriptControl.RegisterResources(resources);
		}

		private static bool IsPartOfOpenPro(string jsRef)
		{
			return jsRef.Contains(ResourcesConst.JS_ROOT + ".wijmo.") || jsRef.Contains(ResourcesConst.JS_ROOT + ".base.");
		}

#if !EXTENDER
		private static bool IsPartOfOpenProExtension(string resourceName)
		{
			return resourceName.Contains(ResourcesConst.JS_ROOT + ".extensions.");
		}

		private static bool IsPartOfControlWidgets(string resourceName)
		{
			return resourceName.Contains(ResourcesConst.JS_ROOT + ".widgets.");
		}
#endif
		#endregion

		#region register Css resources.


		private static List<WijmoResourceInfo> GetThemeResources(IWijmoWidgetSupport control)
		{
			List<WijmoResourceInfo> resources = new List<WijmoResourceInfo>();
			bool isUseCDN = IsUseCDN(control);
			WijmoResourceInfo resource;
			if (control.WijmoControlMode == WijmoControlMode.Mobile)
			{
				//Add structure css
				if (isUseCDN)
				{
					resources.Add(new WijmoResourceInfo(GetCDNReference(control, CDNResourceType.JQueryMobileCssStructure), ResourceType.Css, WijmoResourceInfo.ThemeCssPriority));
				}
			}

			if (control.WijmoCssAdapter == "bootstrap")
			{
				if (isUseCDN)
				{
					resources.Add(new WijmoResourceInfo(GetCDNReference(control, CDNResourceType.BootStrapCss), ResourceType.Css, WijmoResourceInfo.ThemeCssPriority, themeCssClass));
				}
				else
				{
					resource = FindCssResourceByName(currentAssembly, string.Format("{0}.bootstrap.{1}", ResourcesConst.THEME_ROOT,
					ResourcesConst.BOOTSTRAP_CSS).Replace("&t", "&amp;t"));
					resource.CssClass = themeCssClass;
					resources.Add(resource);
				}
			}
			else
			{
				string theme = control.Theme;
				string lowerCaseTheme = theme.ToLower().Trim();
				if (string.IsNullOrEmpty(lowerCaseTheme))
				{
					return resources;
				}
				if (lowerCaseTheme.StartsWith("~/") ||
				lowerCaseTheme.StartsWith("http://") ||
					lowerCaseTheme.StartsWith("https://") ||
					lowerCaseTheme.EndsWith(".css"))
				{
					if (lowerCaseTheme.StartsWith("~"))
					{
						theme = ((Control)control).Page.ResolveUrl(theme);
					}
					resources.Add(new WijmoResourceInfo(theme, ResourceType.Css, WijmoResourceInfo.ThemeCssPriority, themeCssClass));
					return resources;
				}

				if (isUseCDN && !string.IsNullOrEmpty(control.CDNPath))
				{
					if (control.WijmoControlMode == WijmoControlMode.Mobile)
					{
						resources.Add(new WijmoResourceInfo(GetCDNReference(control, CDNResourceType.JQueryMobileCss), ResourceType.Css, WijmoResourceInfo.ThemeCssPriority, themeCssClass));
					}
					else
					{
						string path = control.CDNPath;
						if (!path.EndsWith("/"))
						{
							path += "/";
						}
						resources.Add(new WijmoResourceInfo(string.Format("{0}themes/{1}/{2}", control.CDNPath, theme, WijmoThemeFileName), ResourceType.Css, WijmoResourceInfo.ThemeCssPriority, themeCssClass));
					}
				}
				else
				{
					if (control.WijmoControlMode == WijmoControlMode.Mobile)
					{
						resource = FindCssResourceByName(currentAssembly, string.Format("{0}.{1}", ResourcesConst.CSS_ROOT, ResourcesConst.WIJMO_MOBILE_CSS));
					}
					else
					{
						//Check the theme whether exists in the supported theme lists.
						resource = null;
						if (Array.IndexOf(ResourcesConst.INNER_THEMES, theme) >= 0)
						{
							resource = FindCssResourceByName(currentAssembly, string.Format("{0}.{1}.{2}", ResourcesConst.THEME_ROOT,
										theme.Replace("-", "_"), WijmoThemeFileName));
						}
					}
					if (resource != null)
					{
						resource.CssClass = themeCssClass;
						resources.Add(resource);
					}
				}
			}
			return resources;
		}


		public static void RegisterCssResource(IWijmoWidgetSupport control)
		{
			bool useCDN = IsUseCDN(control) && !string.IsNullOrEmpty(control.CDNPath);
			ResourceControl cssControl = GetResourceControlContainer((Control)control).FindControl(cssControlID) as ResourceControl;
			cssControl.RegisterResources(GetThemeResources(control));
			OrderedDictionary cssReferences = WidgetDependenciesResolver.GetCssReferences(control);
			IDictionaryEnumerator ien = cssReferences.GetEnumerator();
			while (ien.MoveNext())
			{
				string value = (string)ien.Value;
				if (useCDN && (value.Contains(ResourcesConst.WIJMO_OPEN_CSS) || value.Contains(ResourcesConst.WIJMO_PRO_CSS)))
				{
					cssControl.RegisterResource(new WijmoResourceInfo(string.Format("{0}jquery.wijmo-pro.all.{1}.min.css", control.CDNPath, VersionConst.WijmoVer), ResourceType.Css, 1));
					continue;
				}
				cssControl.RegisterResource(FindCssResourceByName(currentAssembly, value));
			}
			if (control.WijmoCssAdapter == "bootstrap" && control.WijmoControlMode == WijmoControlMode.Web)
			{
				cssControl.RegisterResource(FindCssResourceByName(currentAssembly, WidgetDependenciesResolver.ResolveDependency(ResourcesConst.WIJMO_BOOTSTRAP_CSS, ".css")));
			}
		}

		#endregion


		#region *** Get informations for wijmo JS resources ***

		internal static string GetResourcesUrlString(Page page, IEnumerable<WijmoResourceInfo> resources)
		{
#if EXTENDER
            return ((GetApplicationUrl(page) + "WijmoExtendersResource.axd?scripts=" + GetResourcesListString(resources)) + "&t=" + GetDateStamp(GetWijmoScriptsModificationDate(resources)));
#else
			return ((GetApplicationUrl(page) + "WijmoControlsResource.axd?scripts=" + GetResourcesListString(resources)) + "&t=" + GetDateStamp(GetWijmoScriptsModificationDate(resources)));
#endif
		}

		private static List<WijmoResourceInfo> GetResourcesByListString(string list)
		{
			List<WijmoResourceInfo> list2 = new List<WijmoResourceInfo>();
			if (!string.IsNullOrEmpty(list))
			{
				string[] strArray = list.Split(new char[] { ';' });
				for (int i = 0; i < strArray.Length; i++)
				{
					WijmoResourceInfo item = FindResourceByID(strArray[i], embeddedScriptsHash);
					if (item != null)
					{
						list2.Add(item);
					}
				}
			}
			return list2;
		}

		[SecuritySafeCritical]
		public static DateTime GetAssemblyModificationDate(Assembly assembly)
		{
			DateTime utcNow = File.GetLastWriteTime(new Uri(assembly.EscapedCodeBase).LocalPath).ToUniversalTime();
			if (utcNow > DateTime.UtcNow)
			{
				utcNow = DateTime.UtcNow;
			}
			return utcNow;
		}

		public static string GetAssemblyShortName(Assembly assembly)
		{
			string[] strArray = assembly.FullName.Split(new char[] { ',' });
			if (strArray.Length > 0)
			{
				return strArray[0].Trim();
			}
			return assembly.FullName;
		}

		public static string GetDateStamp(DateTime date)
		{
			lock (dateStampsLock)
			{
				if (!dateStamps.ContainsKey(date))
				{
					dateStamps.Add(date, date.Ticks.ToString());
				}
				return dateStamps[date];
			}
		}

		public static string GetResourceKey(Assembly assembly, string resourceName)
		{
			return (GetAssemblyShortName(assembly) + "_" + resourceName);
		}

		#endregion

		#region find resource

		private static WijmoResourceInfo FindResourceByName(Assembly assembly, string resourceName, Dictionary<string, WijmoResourceInfo> sources)
		{
			WijmoResourceInfo data = FindResourceByNameCore(assembly, resourceName, sources);
			if (data == null)
			{
				lock (findResourcesLock)
				{
					RegisterAllResources(assembly);
					data = FindResourceByNameCore(assembly, resourceName, sources);
				}
			}
			if (data == null)
			{
				throw new Exception("Can not find the '" + resourceName + "' resource.'");
			}
			return data;
		}

		public static WijmoResourceInfo FindScriptResourceByName(string resourceName)
		{
			return FindScriptResourceByName(currentAssembly, WidgetDependenciesResolver.ResolveDependency(resourceName, ".js"));
		}

		private static WijmoResourceInfo FindScriptResourceByName(Assembly assembly, string resourceName)
		{
			return FindResourceByName(assembly, resourceName, embeddedScriptsHash);
		}

		private static WijmoResourceInfo FindCssResourceByName(Assembly assembly, string resourceName)
		{
			return FindResourceByName(assembly, resourceName, embeddedCssHash);
		}

		private static WijmoResourceInfo FindResourceByID(string id, Dictionary<string, WijmoResourceInfo> sources)
		{
			int index;
			if (int.TryParse(id, out index) && currentAssembly != null)
			{
				foreach (var resource in sources.Values)
				{
					if (resource.Index == index)
						return resource;
				}
			}

			return null;
		}

		private static WijmoResourceInfo FindScriptResourceByID(string id)
		{
			return FindResourceByID(id, embeddedScriptsHash);
		}

		#endregion

		#region ** Private methods **

		private static List<WijmoResourceInfo> GetRequestedResources()
		{
			string str = WijmoHttpHandler.GetRequest().QueryString["scripts"];
			List<WijmoResourceInfo> list = new List<WijmoResourceInfo>();
			foreach (WijmoResourceInfo data in GetResourcesByListString(str))
			{
				list.Add((WijmoResourceInfo)data);
			}
			return list;
		}

		private static DateTime GetWijmoScriptsModificationDate(IEnumerable<WijmoResourceInfo> resources)
		{
			DateTime minValue = DateTime.MinValue;
			//foreach (WijmoScriptResourceInfo data in resources)
			//{
			DateTime assemblyModificationUniversalDate = DateTime.MinValue;
			if (currentAssembly != null)
			{
				assemblyModificationUniversalDate = WijmoResourceManager.GetAssemblyModificationDate(currentAssembly);
			}

			if (assemblyModificationUniversalDate > minValue)
			{
				minValue = assemblyModificationUniversalDate;
			}
			//}
			return minValue;
		}

		private static string GetResourcesContentType(IEnumerable<WijmoResourceInfo> resources)
		{
			string contentType = string.Empty;
			foreach (WijmoResourceInfo data in resources)
			{
				if (string.IsNullOrEmpty(contentType))
				{
					contentType = data.MimeType;
				}
				else if (contentType != data.MimeType)
				{
					throw new Exception("Error, the content is different!");
				}
			}
			return contentType;
		}

		private static string GetResourceUrlInternal(Page page, Assembly assembly, string resourceName)
		{
			string resourcesUrlString = string.Empty;
			if (HttpContext.Current == null)
			{
				Type[] types = assembly.GetTypes();
				Type type = (types.Length > 0) ? types[0] : null;
				return (((page != null) && (type != null)) ? page.ClientScript.GetWebResourceUrl(type, resourceName) : string.Empty);
			}
			WijmoResourceInfo data = FindResourceByName(assembly, resourceName, embeddedScriptsHash);
			if (data != null)
			{
				resourcesUrlString = GetResourcesUrlString(page, new WijmoResourceInfo[] { data });
			}
			return resourcesUrlString;
		}

		private static void RegisterAllResources(Assembly assembly)
		{
			object[] customAttributes;
			try
			{
				customAttributes = assembly.GetCustomAttributes(typeof(WebResourceAttribute), false);
			}
			catch
			{
				return;
			}
			if (customAttributes.Length != 0)
			{
				//Array.Sort<object>(customAttributes, new Comparison<object>(WijmoScriptManager.WebResourceAttributeSorter));
				for (int i = 0; i < customAttributes.Length; i++)
				{
					WebResourceAttribute attribute = (WebResourceAttribute)customAttributes[i];
					if (attribute.ContentType == "text/javascript")
					{
						WijmoResourceInfo data = new WijmoResourceInfo(embeddedScriptsHash.Keys.Count, attribute.WebResource, attribute.ContentType);

						lock (findAllWebResourcesLock)
						{
							if (!embeddedScriptsHash.ContainsKey(data.Key))
							{
								embeddedScriptsHash.Add(data.Key, data);
							}
						}
					}
					else if (attribute.ContentType == "text/css")
					{
						WijmoResourceInfo data = new WijmoResourceInfo(embeddedCssHash.Keys.Count, attribute.WebResource, attribute.ContentType);

						lock (findAllWebResourcesLock)
						{
							if (!embeddedCssHash.ContainsKey(data.Key))
							{
								embeddedCssHash.Add(data.Key, data);
							}
						}
					}
				}
			}
		}

		private static WijmoResourceInfo FindResourceByNameCore(Assembly assembly, string resourceName, Dictionary<string, WijmoResourceInfo> sources)
		{
			string resourceKey = GetResourceKey(assembly, resourceName);
			if (sources.ContainsKey(resourceKey))
			{
				return sources[resourceKey];
			}
			return null;
		}

		internal static string GetResourcesListString(IEnumerable<WijmoResourceInfo> resources)
		{
			string str = string.Empty;
			foreach (WijmoResourceInfo data in resources)
			{
				str = str + data.Index + ";";
			}

			if (string.IsNullOrEmpty(str))
			{
				return str;
			}

			return str.Remove(str.Length - 1);
		}

		private static int WebResourceAttributeSorter(object x, object y)
		{
			WebResourceAttribute attribute = x as WebResourceAttribute;
			WebResourceAttribute attribute2 = y as WebResourceAttribute;
			return StringComparer.InvariantCulture.Compare(attribute.WebResource, attribute2.WebResource);
		}
		#endregion

		#region Utils
		public static string GetApplicationUrl(Page page)
		{
			string str = string.Empty;
			HttpRequest request = WijmoHttpHandler.GetRequest(page);
			if (request == null)
			{
				return str;
			}
			if (request.ApplicationPath != "/")
			{
				return (str + request.ApplicationPath + "/");
			}
			return (str + request.ApplicationPath);
		}
		#endregion
	}
}
