﻿using System;
using System.IO;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
#if !EXTENDER 
        [SmartAssembly.Attributes.DoNotObfuscate]
#endif
    internal class WijmoResourceInfo
    {
        private int _index;
        private string _key;
        private string _name;
        private string _resourceContent;
        private string _mimeType;
        private Encoding _resourceEncoding;
		internal const int CoreScriptPriority = 0;
		internal const int WijmoWidgetsScriptPriority = 1;
		internal const int ControlWidgetsScriptPriority = 2;
		internal const int BootstrapScriptPriority = 3;
		internal const int ExtensionsScriptPriority = 4;

		internal const int ThemeCssPriority = 0;
		internal const int WijmoCssPriority = 1;
		internal const int BootstrapCssPriority = 2;

		#region ** constructor
		public WijmoResourceInfo(int id, string name, string resourceType)
        {
            this._index = id;
            this._name = name;
            this._mimeType = resourceType;
			InitPriority();
        }

		public WijmoResourceInfo(string name, ResourceType type, int priority)
		{
			this.Init(name, type, priority);
		}

		public WijmoResourceInfo(string name, ResourceType type, int priority, string cssClass) 
		{
			this.Init(name, type, priority);
			this.CssClass = cssClass;
		}

		private void Init(string name, ResourceType type, int priority)
		{
			this._index = -1;
			this._name = name;
			this.Priority = priority;
			if (type == ResourceType.Css)
			{
				this._mimeType = "text/css";
			}
			else
			{
				this._mimeType = "text/javascript";
			}
		}
		#endregion

		#region ** init priority of the resources
		private void InitCssPriority()
		{
			string themeFolder = "resources.themes.";
			string lowercaseName = Name.ToLower();

			if (lowercaseName.Contains(themeFolder + "wijmo")) 
			{
				this.Priority = WijmoCssPriority;
			}
			else if (lowercaseName.Contains(ResourcesConst.WIJMO_BOOTSTRAP_CSS.ToLower()))
			{
				this.Priority = BootstrapCssPriority;
			}
			else 
			{
				this.Priority = ThemeCssPriority;
			}
		}

		private void InitScriptPriority() 
		{
			string scriptsFolder = "resources.scripts.";
			string lowercaseName = Name.ToLower();
			if (lowercaseName.Contains(scriptsFolder + "wijmo") || Name.Contains(ResourcesConst.WIJMO_OPEN_JS) || Name.Contains(ResourcesConst.WIJMO_PRO_JS)) 
			{
				this.Priority = WijmoWidgetsScriptPriority;
			}
#if !EXTENDER
			else if (lowercaseName.Contains(scriptsFolder + "extensions") || Name.Contains(ResourcesConst.C1WRAPPER_OPEN) || Name.Contains(ResourcesConst.C1WRAPPER_PRO) || Name.Contains(ResourcesConst.C1WRAPPER_CONTROL) || Name.Contains(ResourcesConst.MOBILE_CONTROL))
			{
				this.Priority = ExtensionsScriptPriority;
			}
			else if(Name.Contains(ResourcesConst.WIDGETS_CONTROL))
			{
				this.Priority = ControlWidgetsScriptPriority;
			}
#endif
			else if (lowercaseName.Contains(scriptsFolder + "bootstrap")) 
			{
				this.Priority = BootstrapScriptPriority;
			}
			else if (lowercaseName.Contains(scriptsFolder + "widgets")) 
			{
				this.Priority = ControlWidgetsScriptPriority;
			}
			else if (lowercaseName.Contains(scriptsFolder + "globalize.cultures.js")) 
			{
				this.Priority = ControlWidgetsScriptPriority;
			}
			else
			{
				this.Priority = CoreScriptPriority;
			}
		}

		private void InitPriority()
		{
			if (this.MimeType == "text/javascript")
			{
				InitScriptPriority();
			}
			else 
			{
				InitCssPriority();
			}
		}
		#endregion

		#region ** properties
#if !EXTENDER 
		[SmartAssembly.Attributes.DoNotObfuscate]
#endif
		public Encoding ResourceEncoding
        {
#if !EXTENDER
            [SmartAssembly.Attributes.DoNotObfuscate]
#endif
            get
            {
                return this._resourceEncoding;
            }
            set
            {
                this._resourceEncoding = value;
            }
        }

        public string MimeType
        {
            get
            {
                return this._mimeType;
            }
        }

        public int Index
        {
            get
            {
                return this._index;
            }
        }

        public string Key
        {
            get
            {
                if (string.IsNullOrEmpty(this._key))
                {
					this._key = WijmoResourceManager.GetResourceKey(this.GetType().Assembly, this.Name);
                }

                return _key;
            }
        }

		public int Priority { get; set; }

		/// <summary>
		/// for themeRoller, we should add an css class to the link element.
		/// </summary>
		public string CssClass
		{
			get;
			set;
		}

        public string Name
        {
            get
            {
                return this._name;
            }
        }

#if !EXTENDER 
        [SmartAssembly.Attributes.DoNotObfuscate]
#endif
        public string ResourceContent
        {
#if !EXTENDER
            [SmartAssembly.Attributes.DoNotObfuscate]
#endif
            get
            {
                if (this._resourceContent == null)
                {
                    this._resourceContent = this.GetResourceContent();
                }
                return this._resourceContent;
            }
        }
		#endregion

		protected string GetResourceContent()
        {
            string str = string.Empty;
            Stream resourceStream = null;
            StreamReader reader = null;
            try
            {
                resourceStream = this.GetType().Assembly.GetManifestResourceStream(this.Name);
                if (resourceStream != null)
                {
                    reader = new StreamReader(resourceStream, true);
                    str = reader.ReadToEnd() + Environment.NewLine;
                    this.ResourceEncoding = reader.CurrentEncoding;
                }
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (resourceStream != null)
                {
                    resourceStream.Close();
                }
            }
            return str;
        }

        public void WriteContent(Stream stream)
        {
            if (!string.IsNullOrEmpty(this.ResourceContent))
            {
                byte[] bytes = this.ResourceEncoding.GetBytes(this.ResourceContent);
                stream.Write(bytes, 0, bytes.Length);
            }
        }
      
    }
}
