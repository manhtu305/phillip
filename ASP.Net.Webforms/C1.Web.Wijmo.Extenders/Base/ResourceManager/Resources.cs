﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
#if !EXTENDER 
	[SmartAssembly.Attributes.DoNotObfuscateType]
#endif
	internal static class ResourcesConst
	{
		public const string JQUERY = "jquery-" + C1.Wijmo.Licensing.VersionConst.jQueryVer + ".min.js";
		public const string JQUERY_UI = "jquery-ui-" + C1.Wijmo.Licensing.VersionConst.jQueryUiVer + ".custom.min.js";
		public const string JQUERY_JQM = "jquery-" + C1.Wijmo.Licensing.VersionConst.jQueryVerWithJQM + ".min.js";
		public const string JQUERY_MOBILE = "jquery.mobile-" + C1.Wijmo.Licensing.VersionConst.jQueryMobileVer + ".min.js";
		public const string BOOTSTRAP_JS = "bootstrap.min.js";
		public const string BOOTSTRAP_CSS = "bootstrap.min.css";
#if DEBUG
		public const string WIJMO_OPEN_JS = "jquery.wijmo-open.js";
		public const string WIJMO_PRO_JS = "jquery.wijmo-pro.js";
		public const string WIJMO_BOOTSTRAP_JS = "bootstrap.wijmo.js";
		public const string WIJMO_MOBILE_JS = "jquery.wijmo.mobile.js";
#else
		public const string WIJMO_OPEN_JS = "jquery.wijmo-open.min.js";
		public const string WIJMO_PRO_JS = "jquery.wijmo-pro.min.js";
		public const string WIJMO_BOOTSTRAP_JS = "bootstrap.wijmo.min.js";
		public const string WIJMO_MOBILE_JS = "jquery.wijmo.mobile.min.js";
#endif

		internal const string TOUCH_UTIL = "wijmo.jquery.wijmo.wijtouchutil.js";
		internal const string UTIL = "wijmo.jquery.wijmo.wijutil.js";
		internal const string BASE_WIDGET = "wijmo.jquery.wijmo.widget.js";
		internal const string BGIFRAME = "base.jquery.bgiframe.js";
		internal const string GLOBALIZE = "base.globalize.min.js";
		internal const string MOUSEWHEEL = "base.jquery.mousewheel.min.js";
		internal const string POSITION = "base.jquery.ui.position.js";
		internal const string CULTURE = "globalize.cultures.js";
		internal const string RAPHAEL = "base.raphael-min.js";
		internal const string WIJMO_RAPHAEL = "wijmo.jquery.wijmo.raphael.js";
		internal const string SWFOBJECT = "swfobject.js";

#if EXTENDER
		public const string WIJMO_OPEN_CSS = "jquery.wijmo-open.Extenders.css";
		public const string WIJMO_PRO_CSS = "jquery.wijmo-pro.Extenders.css";
		public const string WIJMO_MOBILE_CSS = "jquery.wijmo-mobile.Extenders.css";
		public const string WIJMO_BOOTSTRAP_CSS = "jquery.wijmo-bootstrap.Extenders.css";
		internal const string THEME_ROOT = "C1.Web.Wijmo.Extenders.Resources.themes";
		internal const string CSS_ROOT = "C1.Web.Wijmo.Extenders.Resources.themes.wijmo";
		internal const string JS_ROOT = "C1.Web.Wijmo.Extenders.Resources.scripts";
#else
		public const string WIJMO_OPEN_CSS = "jquery.wijmo-open.Controls.css";
		public const string WIJMO_PRO_CSS = "jquery.wijmo-pro.Controls.css";
		public const string WIJMO_MOBILE_CSS = "jquery.wijmo-mobile.Controls.css";
		public const string WIJMO_BOOTSTRAP_CSS = "jquery.wijmo-bootstrap.Controls.css";

		internal const string THEME_ROOT = "C1.Web.Wijmo.Controls.Resources.themes";
		internal const string CSS_ROOT = "C1.Web.Wijmo.Controls.Resources.themes.wijmo";
		internal const string JS_ROOT = "C1.Web.Wijmo.Controls.Resources.scripts";

#if DEBUG
		internal const string C1WRAPPER_CONTROL = "extensions-control.js";
		internal const string C1WRAPPER_OPEN = "extensions-open.js";
		internal const string C1WRAPPER_PRO = "extensions-pro.js";
		internal const string MOBILE_CONTROL = "mobile-control.js";
		internal const string WIDGETS_CONTROL = "widgets-control.js";
#else
		internal const string C1WRAPPER_CONTROL = "extensions-control.min.js";
		internal const string C1WRAPPER_OPEN = "extensions-open.min.js";
		internal const string C1WRAPPER_PRO = "extensions-pro.min.js";
		internal const string MOBILE_CONTROL = "mobile-control.min.js";
		internal const string WIDGETS_CONTROL = "widgets-control.min.js";
#endif
#endif
		internal static readonly string[] INNER_THEMES = new string[] 
		{ 
			"arctic",
			"aristo",
			"cobalt",
			"midnight",
			"rocket",
			"sterling",
			"metro",
			"metro-dark",
			"lucid",
			"stafford"
		};
	}
}
