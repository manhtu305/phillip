﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
	[SmartAssembly.Attributes.DoNotObfuscateType]
	internal class ResourceControl: HtmlControl
	{

		#region ** fields 
		private List<WijmoResourceInfo> _items;
		private const string registeredResourceKey = "__registeredResources";
		#endregion

		#region ** constructor
		public ResourceControl(ResourceType type) 
		{
			this.ResourceType = type;
			this.EnableViewState = false;
		}
		#endregion

		#region ** properties
		public bool EnableCombinedJavaScripts
		{
			get;
			set;
		}

		private ScriptManager ScriptManager 
		{
			get 
			{
				return ScriptManager.GetCurrent(Page);
			}
		}
		
		public List<WijmoResourceInfo> Items 
		{
			get 
			{
				if (_items == null) {
					_items = new List<WijmoResourceInfo>();
				}
				return _items;
			}
		}

		public ResourceType ResourceType
		{
			get;
			set;
		}

		#endregion

		#region ** register resources
		public void RegisterResources(List<WijmoResourceInfo> resources)
		{
			foreach (WijmoResourceInfo resource in resources)
			{
				RegisterResource(resource);
			}
		}

		private bool IsResourceRegistered(WijmoResourceInfo resource) 
		{
			List<string> registeredResources;
			if (Page.Items[registeredResourceKey] == null)
			{
				Page.Items[registeredResourceKey] = new List<string>();
			}
			registeredResources = Page.Items[registeredResourceKey] as List<string>;
			
			if (resource.Index == -1) {
				return IsResourceRegistered(resource.Name, registeredResources);
			}

			string controlResourceFolder = "C1.Web.Wijmo.Controls.Resources.";
			string extenderResourceFolder = "C1.Web.Wijmo.Extenders.Resources.";
			string name = resource.Name.Replace(controlResourceFolder, "").Replace(extenderResourceFolder, "");
			return IsResourceRegistered(name, registeredResources);
		}

		private bool IsResourceRegistered(string name, List<string> registeredResources)
		{
			if (registeredResources.Contains(name))
			{
				if (ResourceType == ResourceType.Script)
				{
					// bootstrap.js and bootstap.wijmo.js need be registered twice for extender and control
					if (name.Contains(ResourcesConst.BOOTSTRAP_JS)
						|| name.Contains(ResourcesConst.WIJMO_BOOTSTRAP_JS))
					{
						return false;
					}
				}
				return true;
			}
			else
			{
				registeredResources.Add(name);
				return false;
			}
		}

		public void RegisterResource(WijmoResourceInfo resource)
		{
			if (IsResourceRegistered(resource))
			{
				return;
			}

			if (this.ResourceType == ResourceType.Css && ScriptManager != null && ScriptManager.IsInAsyncPostBack)
			{
				StringBuilder sb = new StringBuilder();
				sb.AppendLine("$(function(){");
				sb.AppendLine(string.Format("	c1common.registerRuntimeCSS(\"{0}\");", resolveResourcePath(resource)));
				sb.AppendLine("})");
				System.Web.UI.ScriptManager.RegisterClientScriptBlock(Page, typeof(WijmoResourceManager), resource.Name.Replace('.', '_'), sb.ToString(), true);
				return;
			}

			int index = 0;
			foreach (WijmoResourceInfo item in Items)
			{
				if (resource.Priority >= item.Priority)
				{
					index++;
				}
			}
			Items.Insert(index, resource);
		}
		#endregion

		#region ** render the resources
		private string resolveResourcePath(WijmoResourceInfo resource) 
		{
			if (resource.Index == -1)
			{
				return resource.Name;
			}
			else 
			{
				return Page.ClientScript.GetWebResourceUrl(typeof(ResourceControl), resource.Name);
			}
		}

		#region ** render CSS
		private void RenderCSS(WijmoResourceInfo resource, System.Web.UI.HtmlTextWriter writer) 
		{
			writer.AddAttribute(System.Web.UI.HtmlTextWriterAttribute.Rel, "stylesheet");
			writer.AddAttribute(System.Web.UI.HtmlTextWriterAttribute.Href, resolveResourcePath(resource));
			if (!string.IsNullOrEmpty(resource.CssClass)) 
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Class, resource.CssClass);
			}
			writer.RenderBeginTag(System.Web.UI.HtmlTextWriterTag.Link);
			writer.RenderEndTag();
		}

		private void RenderStyles(HtmlTextWriter writer)
		{
			foreach (WijmoResourceInfo item in Items)
			{
				RenderCSS(item, writer);
			}
		}
		#endregion

		#region ** render scripts
		private void RenderScript(WijmoResourceInfo resource, System.Web.UI.HtmlTextWriter writer)
		{
			writer.AddAttribute(System.Web.UI.HtmlTextWriterAttribute.Type, "text/javascript");
			writer.AddAttribute(System.Web.UI.HtmlTextWriterAttribute.Src, resolveResourcePath(resource));
			writer.AddAttribute(HtmlTextWriterAttribute.Class, "wijmoScript");
			writer.RenderBeginTag(System.Web.UI.HtmlTextWriterTag.Script);
			writer.RenderEndTag();
		}

		private void RenderScripts(HtmlTextWriter writer)
		{
			if (EnableCombinedJavaScripts)
			{
				RenderScriptsWithCombinedMode(writer);
			}
			else
			{
				RenderScriptsWithUncombinedMode(writer);
			}
		}

		private void RenderScriptsWithCombinedMode(HtmlTextWriter writer)
		{
			string url = WijmoResourceManager.GetResourcesUrlString(Page, Items);
			writer.AddAttribute(HtmlTextWriterAttribute.Type, "text/javascript");
			writer.AddAttribute(HtmlTextWriterAttribute.Src, url);
			writer.RenderBeginTag(HtmlTextWriterTag.Script);
			writer.RenderEndTag();
		}

		private void RenderScriptsWithUncombinedMode(HtmlTextWriter writer)
		{
			foreach (WijmoResourceInfo item in Items)
			{
				RenderScript(item, writer);
			}
		}
		#endregion

		protected override void Render(System.Web.UI.HtmlTextWriter writer)
		{
			if (ResourceType == ResourceType.Css)
			{
				RenderStyles(writer);
			}
			// if the page contains ScriptManager, using ScriptManager.RegisterScriptControl method to register the scripts.
			else if (ScriptManager == null)
			{
				RenderScripts(writer);
			}
		}
		#endregion
	}
}
