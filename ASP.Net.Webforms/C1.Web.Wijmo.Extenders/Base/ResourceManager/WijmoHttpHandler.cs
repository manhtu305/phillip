﻿using System.Threading;
using System.Web;
using System.Web.UI;

#if EXTENDER 
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
	/// <summary>
	/// A handler to render the combined script resources.
	/// </summary>
    public class WijmoHttpHandler : IHttpHandler
    {
        void IHttpHandler.ProcessRequest(HttpContext context)
        {
            WijmoResourceManager.ProcessRequestForWijmoCombinedJS();
        }

        bool IHttpHandler.IsReusable
        {
            get
            {
                return true;
            }
        }
		/// <summary>
		/// Get HttpRequest Object
		/// </summary>
		/// <returns></returns>
        public static HttpRequest GetRequest()
        {
            return GetRequest((Page)null);
        }

		/// <summary>
		/// Get HttpRequest Object via Page.
		/// </summary>
		/// <param name="page">HttpPage object</param>
		/// <returns></returns>
        public static HttpRequest GetRequest(Page page)
        {
            if (HttpContext.Current != null)
            {
                return HttpContext.Current.Request;
            }
            if (page != null)
            {
                return page.Request;
            }
            return null;
        }

		/// <summary>
		/// Get HttpResponse Object
		/// </summary>
		/// <returns></returns>
        public static HttpResponse GetResponse()
        {
            return GetResponse((Page)null);
        }

		/// <summary>
		/// Get HttpResponse Object via Page object.
		/// </summary>
		/// <param name="page">HttpPage object</param>
		/// <returns></returns>
        public static HttpResponse GetResponse(Page page)
        {
            if (HttpContext.Current != null)
            {
                return HttpContext.Current.Response;
            }
            if (page != null)
            {
                return page.Response;
            }
            return null;
        }

		/// <summary>
		/// End the response.
		/// </summary>
        public static void EndResponse()
        {
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            try
            {
                GetResponse().End();
            }
            catch (ThreadAbortException)
            {
            }
        }
    }
}
