﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web;
using C1.Web.Wijmo.Extenders.Converters;
using System.Reflection;
using System.IO;
using System.Text.RegularExpressions;


namespace C1.Web.Wijmo.Extenders
{
	using C1.Web.Wijmo.Extenders.Base;
	using C1.Wijmo.Licensing;
	using C1.Web.Wijmo.Extenders.Localization;

	/// <summary>
	/// Base class for the Wijmo extender controls.
	/// </summary>
	[ParseChildren(true)]
	[PersistChildren(false)]
	[DefaultProperty("TargetControlID")]
	[NonVisualControl]
	[WidgetDependencies(typeof(WijmoBase), "framework.js")]    
	public abstract class WidgetExtenderControlBase : ExtenderControl, IUrlResolutionService, IC1Extender, IWijmoWidgetSupport 
	{
		#region ** fields

		private bool _isDisposed;
		ScriptManager _scriptManager;
        private bool? _enableCombinedJS = null;

        private const string WIJMOEXTENDERSRESOURCEHANDLER = "~/WijmoExtendersResource.axd";
		#endregion

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetExtenderControlBase"/> class.
		/// </summary>
		public WidgetExtenderControlBase()
		{
		}

		/// <summary>
		/// Enables a server control to perform final clean up before it is released from memory.
		/// </summary>
		public override void Dispose()
		{
			this._isDisposed = true;
			base.Dispose();
			GC.SuppressFinalize(this);
		}

		#endregion

		#region ** properties

        /// <summary>
        /// Enable JavaScripts files combined into one file.
        /// In order to combined enabled, you must register the WijmoHttpHandler in web.config.
        /// </summary>
        [C1Description("C1Base.EnableCombinedJavaScripts")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual bool EnableCombinedJavaScripts
        {
            get
            {
                if (_enableCombinedJS == null)
                {
                    if (HttpContext.Current == null)//design time
                    {
                        _enableCombinedJS = bool.Parse(WebConfigWorker.ReadAppSetting(this, "EnableCombinedJavaScripts", "false"));
                    }
                    else
                    {
                        string result = WebConfigWorker.ReadAppSetting(this, "EnableCombinedJavaScripts", null);
                        bool registeredHandler = checkWijmoHttpHandlerRegistered();

                        if (string.IsNullOrEmpty(result))
                        {
                            //if user not set EnableCombinedJS, then check whether registered httphandler.
                            _enableCombinedJS = registeredHandler;
                        }
                        else
                        {
                            _enableCombinedJS = bool.Parse(result);
                        }
                    }
                }

                return _enableCombinedJS.Value;
            }
            set
            {
                _enableCombinedJS = value;
                WebConfigWorker.WriteAppSetting(this, "EnableCombinedJavaScripts", value.ToString());
            }
        }
		
		/// <summary>
		/// A value indicates whether to register the dependency resources 
		/// according to the control's property settings.
		/// </summary>
		/// <remarks>
		/// This property is enabled only when EnableCombinedJavaScripts is set to true.
		/// </remarks>
		[C1Description("C1Base.EnableConditionalDependencies")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual bool EnableConditionalDependencies
		{
			get
			{
				return bool.Parse(WebConfigWorker.ReadAppSetting(this, "EnableConditionalDependencies", "false"));
			}
			set
			{
				if (this.EnableConditionalDependencies != value)
				{
					WebConfigWorker.WriteAppSetting(this, "EnableConditionalDependencies", value.ToString());
				}
			}
		}

		/// <summary>
		/// Name of the theme that will be used to style widgets. Available embedded themes: aristo / midnight / ui-lightness.
		/// Note, only one theme can be used for the whole page at one time.
		/// Please, make sure that all widget extenders have the same Theme value.
		/// </summary>
		[C1Description("C1Base.Theme")]
		[C1Category("Category.Behavior")]
		[DefaultValue("aristo")]
		[TypeConverter(typeof(WijmoThemeNameConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public virtual string Theme
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoTheme", "aristo");
			}
			set
			{
				if (this.Theme != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoTheme", value);
				}
			}
		}

		/// <summary>
		/// Determines whether the widget extender must load client resources from CDN 
		/// (Content Delivery Network) path given by property CDNPath.
		/// </summary>
		[C1Description("C1Base.UseCDN")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public virtual bool UseCDN
		{
			get
			{
				return bool.Parse(WebConfigWorker.ReadAppSetting(this, "WijmoUseCDN", "false"));

			}
			set
			{
				if (this.UseCDN != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoUseCDN", value.ToString());
				}
			}
		}

		/// <summary>
		/// Content Delivery Network path.
		/// </summary>
		[C1Description("C1Base.CDNPath")]
		[C1Category("Category.Behavior")]
		[DefaultValue("http://cdn.wijmo.com/")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public virtual string CDNPath
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoCDNPath", "http://cdn.wijmo.com/");
			}
			set
			{
				if (this.CDNPath != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoCDNPath", value);
				}
			}
		}

		/// <summary>
		/// A string array value specifies all css/js references that wijmo control depends on.
		/// </summary>
		/// <remarks>
		/// Using this property, user can specify the CDN dependencies(such as jQuery, jQuery UI, jQuery mobile, bootstrap).
		/// If some dependencies are set in this property, wijmo controls will find dependencies path in this property.  If found, use the path to load dependency, 
		/// otherwise, use the default CDN path.
		/// For example, if user wants to specify the dependencies of jQuery and jQuery UI, 
		/// he can set the value like this: ["http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js", "http://ajax.aspnetcdn.com/ajax/jquery.ui/1.11.1/jquery-ui.js"]
		/// </remarks>
		[C1Description("C1Base.CDNDependencyPaths")]
		[C1Category("Category.Behavior")]
		[DefaultValue(null)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public string[] CDNDependencyPaths
		{
			get
			{
				string paths = WebConfigWorker.ReadAppSetting(this, "CDNDependencyPaths", "");
				if (string.IsNullOrEmpty(paths))
				{
					return new string[0];
				}
				return paths.Split('|');
			}
			set
			{
				WebConfigWorker.WriteAppSetting(this, "CDNDependencyPaths", string.Join("|", value));
			}
		}

		/// <summary>
		/// Indicates the control applies the theme of JQuery UI or Bootstrap.
		/// </summary>
        [C1Description("C1Base.WijmoCssAdapter")]
		[C1Category("Category.Behavior")]
		[DefaultValue("jquery-ui")]
		[TypeConverter(typeof(WijmoCssAdapterConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string WijmoCssAdapter
		{
			get
			{
				return WebConfigWorker.ReadAppSetting(this, "WijmoCssAdapter", "jquery-ui");
			}
			set
			{
				if (this.WijmoCssAdapter != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoCssAdapter", value);
				}
			}
		}

		#endregion

		internal virtual bool IsWijMobileExtender
		{
			get
			{
				return true;
			}
		}

		internal virtual bool IsWijWebExtender
		{
			get
			{
				return true;
			}
		}

		/// <summary>
		/// A value that indicates mode of the control, whether it is a mobile or web control.
		/// Note that only one value can be used for the whole website or project. 
		/// </summary>
		[C1Description("C1Base.WijmoControlMode")]
		[C1Category("Category.Behavior")]
		[DefaultValue(WijmoControlMode.Web)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [RefreshProperties(RefreshProperties.All)]
		public virtual WijmoControlMode WijmoControlMode
		{
			get
			{
				return (WijmoControlMode)Enum.Parse(typeof(WijmoControlMode), WebConfigWorker.ReadAppSetting(this, "WijmoMode", "Web"));
			}
			set
			{
				if (this.WijmoControlMode != value)
				{
					WebConfigWorker.WriteAppSetting(this, "WijmoMode", value.ToString());
				}
			}
		}

		/// <summary>
		/// A value that indicates the theme swatch of  the extender, 
		/// this property only works when WijmoControlMode property is Mobile.
		/// </summary>
		[C1Description("C1Base.ThemeSwatch")]
		[C1Category("Category.Appearance")]
		[DefaultValue("")]
		[WidgetOption]
		public virtual string ThemeSwatch
		{
			get
			{
				return GetPropertyValue<string>("ThemeSwatch", "");
			}
			set
			{
				this.SetPropertyValue<string>("ThemeSwatch", value);
			}
		}

		/// <summary>
		/// Indicates whether the extender is enabled.
		/// </summary>
		/// <remarks>
		/// Note, disabled extender does not render any script and css resources.
		/// </remarks>
		[C1Description("C1Base.Enabled")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
        [Browsable(false)]
        [EditorBrowsable( EditorBrowsableState.Never)]
		public virtual bool Enabled
		{
			get
			{
				
				if (this._isDisposed)
				{
					return false;
				}
				return this.GetPropertyValue<bool>("Enabled", true);
			}
			set
			{
				this.SetPropertyValue<bool>("Enabled", value);
			}
		}
		
		/*
		/// <summary>
		/// Gets or sets the ID of the control that the extender is associated with.
		/// </summary>
		[Description("The ID of the control that the extender is associated with.")]		
		[IDReferenceProperty]
		[Category("Behavior")]
		[DefaultValue("")]
		public string TargetControlID {
			get
			{
				return GetPropertyValue<string>("TargetControlID", "");
			}
			set
			{
				SetPropertyValue("TargetControlID", value);
			}
		}*/

		/// <summary>
		/// Target CSS selector that should be used instead of TargetControlID property.
		/// You can use TargetSelector property when you need populate extender 
		/// functionality on several page elements(controls) at once. 
		/// </summary>
		/// <remarks>
		/// When you using this property the extender control will be not registered via 
		/// ScriptManager.
		/// Note, this property takes priority over TargetControlID property.
		/// </remarks>
		[Description("Target CSS selector that should be used instead of TargetControlID property. You can use TargetSelector property when you need populate extender functionality on several page elements(controls) at once.")]
		[Category("Behavior")]
		[DefaultValue("")]
		public string TargetSelector
		{
			get
			{
				return GetPropertyValue<string>("TargetSelector", "");
			}
			set
			{
				SetPropertyValue("TargetSelector", value);
			}
		}
				
		#region ** methods
		private void EnsureEnabledState()
		{
			// check to see if any of our parents are disabled
			if (!this.DesignMode)
			{
				//-
				// fix for [16132] [C1AccordionExtender][C1ExpanderExtender]Enabled property is not working
                Control target = null;
				if (this.NamingContainer != null && 
					!string.IsNullOrEmpty(this.TargetControlID) && 
					string.IsNullOrEmpty(this.TargetSelector))
				{
					target = this.NamingContainer.FindControl(this.TargetControlID);
					if (target is WebControl && !((WebControl)target).Enabled)
					{
						this.Enabled = false;
						return;
					}
				}
				//-
				Control ctl = this.Parent;
				bool enabled = this.Enabled;
				while (ctl != null)
				{
					if (ctl is WebControl && !((WebControl)ctl).Enabled)
					{
						this.Enabled = false;
						return;
					}
					ctl = ctl.Parent;
				}

                if (target is WebControl && ((WebControl)target).Enabled)
                {
                    this.Enabled = true;
                    return;
                }
			}
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
            if (!this.DesignMode &&
                this.EnableCombinedJavaScripts &&
                !this.UseCDN &&
                !this.checkWijmoHttpHandlerRegistered())
            {
                throw new Exception("To Combined the JavaScripts, the Wijmo require WijmoHttpHandler in the web.config.");
            }
			base.OnInit(e);
			WijmoResourceManager.RegisterResourceControl(this);
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            if (!base.DesignMode)
            {
				WijmoResourceManager.RegisterCssResource(this);
                    try
                    {
					// if the page doesn't contains ScriptManager,  an exception will thrown.
                        base.OnPreRender(e);
                    }
				catch { }
				finally
                        {

					WijmoResourceManager.RegisterScriptResource(this);
                }
            }
            else
            {
                base.OnPreRender(e);
            }
            this.EnsureEnabledState();
        }

		/// <summary>
		/// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered in the browser window.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (!this.IsWijMobileExtender && this.WijmoControlMode == WijmoControlMode.Mobile)
			{
				throw new NotImplementedException(C1Localizer.GetString("C1Base.MobileNotImp"));
			}
			else if (!this.IsWijWebExtender && this.WijmoControlMode == WijmoControlMode.Web)
			{
				throw new NotImplementedException(C1Localizer.GetString("C1Base.WebNotImp"));
			}
			this.RenderChildren(writer);
			if (this.Page != null)
			{
				this.Page.VerifyRenderingInServerForm(this);
			}
			if (!base.DesignMode)
			{
				// register script descriptors:
				if (string.IsNullOrEmpty(this.TargetSelector) && this.ScriptManager != null)
				{
					RegisterScriptDescriptorsWithScriptManager();
				}
				else
				{
					RegisterScriptDescriptorsWithoutScriptManager();
				}
			}
		}

		#endregion

		#region ** protected virtual and protected methods (wijmo extenders specific)

		/// <summary>
		/// When overridden in a derived class, registers the WidgetDescriptor objects for the control.
		/// </summary>
		/// <param name="targetControl">The server control to which the extender is associated.</param>
		/// <returns>
		/// An enumeration of WidgetDescriptor objects.
		/// </returns>
		protected override IEnumerable<ScriptDescriptor> GetScriptDescriptors(Control targetControl)
		{
			if (targetControl != null && !targetControl.Visible)
			{
				return null;
			}
			this.EnsureValid();
			WidgetDescriptor descriptor = new WidgetDescriptor(this.WidgetName, !string.IsNullOrEmpty(this.TargetSelector) ? this.TargetSelector : ("#" + targetControl.ClientID));
			WidgetObjectBuilder.DescribeWidget(this, descriptor);
			return new List<ScriptDescriptor>(new WidgetDescriptor[] { descriptor });
		}

		/// <summary>
		/// When overridden in a derived class, registers an additional script libraries for the extender control.
		/// </summary>
		/// <returns>
		/// An object that implements the <see cref="T:System.Collections.IEnumerable"/> interface and that contains ECMAScript (JavaScript) files that have been registered as embedded resources.
		/// </returns>
		protected override IEnumerable<ScriptReference> GetScriptReferences()
		{
			return WijmoResourceManager.GetScriptReferences(this);
		}

		/// <summary>
		/// Gets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="nullValue">The null value.</param>
		/// <returns></returns>
		public V GetPropertyValue<V>(string propertyName, V nullValue)
		{
			if (this.ViewState[propertyName] == null)
			{
				return nullValue;
			}
			return (V)this.ViewState[propertyName];
		}

		/// <summary>
		/// Sets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="value">The value.</param>
		public void SetPropertyValue<V>(string propertyName, V value)
		{
			this.ViewState[propertyName] = value;
		}

		/// <summary>
		/// Get the culture setting of this control. 
		/// </summary>
		/// <remarks>
		/// </remarks>
		/// <param name="culture"></param>
		/// <returns></returns>
		protected virtual CultureInfo GetCulture(CultureInfo culture)
		{
			if (culture != null && !string.IsNullOrEmpty(culture.Name))
			{
				return culture;
			}
#if GRAPECITY
			return CultureInfo.GetCultureInfo("ja-JP");
#else

			return CultureInfo.CurrentCulture;
#endif
		}
		#endregion

		#region ** private implementation
        private bool checkWijmoHttpHandlerRegistered() 
        {
            if (HttpContext.Current.Application["WijmoExtenderHttpHandlerRegistered"] == null)
            {
                HttpContext.Current.Application["WijmoExtenderHttpHandlerRegistered"] = WebConfigWorker.WijmoHttpHandlerExists(this.Context, WIJMOEXTENDERSRESOURCEHANDLER, typeof(WijmoHttpHandler), this.Page.Request.ApplicationPath);                
            }
            return (bool)HttpContext.Current.Application["WijmoExtenderHttpHandlerRegistered"];
        }

		#region ** register startup scripts
		// the calling of this method has been removed from OnPreRender method in changeset 56371. 
		//private void RegisterIncludesWithScriptManager()
		//{
		//	if (string.IsNullOrEmpty(this.TargetControlID))
		//		throw new InvalidOperationException("TargetControlID is empty.");
		//	Control targetControl = this.FindControl(this.TargetControlID);
		//	if (targetControl == null)
		//		throw new InvalidOperationException("TargetControlID is invalid.");
		//	List<ScriptDescriptor> scriptDescr = (List<ScriptDescriptor>)GetScriptDescriptors(targetControl);
		//	if (FindUpdatePanel(this) != FindUpdatePanel(targetControl))
		//	{
		//		throw new InvalidOperationException("ExtenderControl and TargetControl must be located in the same UpdatePanel.");
		//	}
		//	this.ScriptManager.RegisterExtenderControl<WidgetExtenderControlBase>(this, targetControl);
		//}

		private void RegisterScriptDescriptorsWithScriptManager()
		{
			this.ScriptManager.RegisterScriptDescriptors(this);
		}

		private void RegisterScriptDescriptorsWithoutScriptManager()
		{
			if (!string.IsNullOrEmpty(this.TargetSelector))
			{
				List<ScriptDescriptor> scriptDescr = (List<ScriptDescriptor>)GetScriptDescriptors(null);
				foreach (WidgetDescriptor desc in scriptDescr)
				{
					ScriptManager.RegisterStartupScript(this, this.GetType(), "c" + this.ClientID, desc.GetScriptInternal(this.WijmoControlMode, false), true);
				}
			}
			else
			{
				if (string.IsNullOrEmpty(this.TargetControlID))
					throw new InvalidOperationException("TargetControlID is empty.");
				Control targetControl = this.FindControl(this.TargetControlID);
				if (targetControl == null || string.IsNullOrEmpty(targetControl.ClientID))
					throw new InvalidOperationException("TargetControlID is invalid.");
				List<ScriptDescriptor> scriptDescr = (List<ScriptDescriptor>)GetScriptDescriptors(targetControl);
				// Added by dail 2011-6-28, fixed issue 15908.
				if (scriptDescr == null) 
				{
					return;
				}
				// end 

				foreach (WidgetDescriptor desc in scriptDescr)
				{
					ScriptManager.RegisterStartupScript(this, this.GetType(), "c" + this.ClientID, desc.GetScriptInternal(this.WijmoControlMode, false), true);
				}
                    }
                }

		#endregion

		#region ** register css stylesheets
		private void RegisterRunTimeStylesheets()
		{
			WijmoResourceManager.RegisterCssResource(this);
		}

		// For unknown reason, the RegisterDesignTimeStyleSheets calling has been reomved from render method in changeset 95703. 

		//private void RegisterDesignTimeStyleSheets(HtmlTextWriter writer)
		//{
		//	if (HttpContext.Current != null)
		//		return;
		//	if (this.UseCDN && !string.IsNullOrEmpty(this.CDNPath))
		//	{

		//	}
		//	else
		//	{
		//		//wijmo_theme
		//		RegisterDesignTimeStyleSheet(writer, string.Format("C1.Web.Wijmo.Extenders.Resources.themes.{0}.{1}",
		//						this.Theme, WijmoResourceManager.WijmoThemeFileName));
		//		OrderedDictionary cssReferences = WidgetDependenciesResolver.GetCssReferences(this);
		//		IDictionaryEnumerator ien = cssReferences.GetEnumerator();
		//		while (ien.MoveNext())
		//		{
		//			RegisterDesignTimeStyleSheet(writer, (string)ien.Value);
		//		}
		//	}
		//}

		///// <summary>
		///// Register design-time CSS stylesheet.
		///// </summary>
		///// <param name="cssResourceName">CSS resource name.</param>
		///// <remarks>
		///// The cssResourceName is the link to an resource element.
		///// </remarks>
		//public void RegisterDesignTimeStyleSheet(HtmlTextWriter writer, string cssResourceName)
		//{
		//	//string css = "<link href=\"" + cssUrl + "\" id=\"" +  cssID  + "\" type=\"text/css\" rel=\"stylesheet\" />";
		//	string s = GetResourceCssContent(cssResourceName, Assembly.GetAssembly(typeof(WidgetExtenderControlBase)));
		//	writer.Write("<style media=\"all\" type=\"text/css\">");
		//	writer.Write(s);
		//	writer.Write("</style>");
		//}

		///// <summary>
		///// Returns content of the css resource embedded into assembly.
		///// </summary>
		///// <param name="name">The case-sensitive name of the manifest resource being requested.</param>
		///// <param name="assembly">The assembly that contains the resource.</param>
		///// <returns></returns>
		//internal string GetResourceCssContent(string name, Assembly assembly)
		//{

		//	//Assembly.GetAssembly(_control.GetType());
		//	string input = string.Empty;
		//	try
		//	{
		//		using (StreamReader reader = new StreamReader(assembly.GetManifestResourceStream(name)))
		//		{
		//			input = reader.ReadToEnd();
		//		}
		//	}
		//	catch (Exception)
		//	{
		//		throw new HttpException("Resource " + name + " does not exists in assembly.");
		//	}
		//	return Regex.Replace(input, "<%\\s*=\\s*WebResource\\(\"(?<resourceName>[^\"]*)\"\\)\\s*%>", new MatchEvaluator(this.WebResourceEvaluator));
		//}

		//private string WebResourceEvaluator(Match match)
			//{					
		//	return GetPage(this).ClientScript.GetWebResourceUrl(this.GetType(), match.Groups["resourceName"].Value);
			//}

//		private Page GetPage(Control control)
//		{
//			Page page = null;
//			if (control == null)
//			{
//				throw new HttpException("(e9324) Can't get Page for null control.");
//			}
//			if (control.Page != null)
//				page = control.Page;
//			for (Control c = control.Parent; c != null; c = c.Parent)
//			{
//				if (c.Page != null)
//				{
//					page = c.Page;
//					break;
//				}
//			}
//			if (page != null)
//				_vs2005DesignModePage = page;
//			else
//				page = _vs2005DesignModePage;
//#if DEBUG
//			if (page == null)
//			{
//				throw new HttpException("(e9325) Can't get Page for WebControl with id '" + control.ID + "'.");
//			}
//#endif
//			return page;
//		}

		//private static Page _vs2005DesignModePage = null;

		#endregion

		private ScriptManager ScriptManager
		{
			get
			{
				if (this._scriptManager == null)
				{
					Page page = this.Page;
					if (page == null)
					{
						throw new InvalidOperationException("Page cannot be null");
					}
					this._scriptManager = ScriptManager.GetCurrent(page);
				}
				return this._scriptManager;
			}
		}

		private static UpdatePanel FindUpdatePanel(Control control)
		{
			for (Control control2 = control.Parent; control2 != null; control2 = control2.Parent)
			{
				UpdatePanel panel = control2 as UpdatePanel;
				if (panel != null)
				{
					return panel;
				}
			}
			return null;
		}

		protected virtual string WidgetName
		{
			get
			{
				string s = this.GetType().Name.ToLower(CultureInfo.InvariantCulture);
				if (s.StartsWith("c1"))
				{
					s = s.Substring(2);
				}
				s = string.Format("wij{0}", s.Substring(0, s.LastIndexOf("extender")));
				return s;
			}
		}

		#endregion


		protected virtual bool CheckIfValid(bool throwException)
		{
			bool valid = true;
			foreach (PropertyDescriptor prop in TypeDescriptor.GetProperties(this))
			{
				if ((prop.Attributes[typeof(WidgetRequiredOptionAttribute)] != null) && ((prop.GetValue(this) == null) || !prop.ShouldSerializeValue(this)))
				{
					valid = false;
					if (throwException)
					{
						throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, "{0} missing required {1} property value for {2}.", new object[] { base.GetType().ToString(), prop.Name, this.ID }), prop.Name);
					}
				}
			}
			return valid;
		}
 
		public virtual void EnsureValid()
		{
			this.CheckIfValid(true);
		}
    }


}

#if false

					writer.AddAttribute(HtmlTextWriterAttribute.Type, "text/javascript");
					writer.AddAttribute("language", "javascript");
					writer.AddAttribute(HtmlTextWriterAttribute.Src, r.Path);
					writer.RenderBeginTag(HtmlTextWriterTag.Script);
					writer.RenderEndTag();
				
				StringBuilder builder = new StringBuilder();
				builder.Append("jQuery(\"#");
				builder.Append(targetControl.ClientID);
				builder.Append("\").");
				builder.Append(this.WidgetName);
				builder.Append("(");
				builder.Append(WidgetObjectBuilder.SerializeOption(this));
				builder.Append(");");

				writer.AddAttribute(HtmlTextWriterAttribute.Type, "text/javascript");
				writer.AddAttribute("language", "javascript");
				writer.RenderBeginTag(HtmlTextWriterTag.Script);
				writer.Write(builder.ToString());
				writer.RenderEndTag();
#endif