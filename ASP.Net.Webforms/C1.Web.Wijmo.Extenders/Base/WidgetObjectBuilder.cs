﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Globalization;
using System.Drawing;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections;
using System.Runtime;
using System.Web.Script.Serialization;

#if EXTENDER 
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
	/// <summary>
	/// Represents a builder of widget object.
	/// </summary>
	public static class WidgetObjectBuilder
	{
		[StructLayout(LayoutKind.Sequential)]
		private struct ResourceEntry
		{
			public string ResourcePath;
			public Type ComponentType;
			public int Order;

			public ResourceEntry(string path, Type componentType, int order) 
			{
				this.ResourcePath = path;
				this.ComponentType = componentType;
				this.Order = order;
			}

			private string AssemblyName
			{
				get
				{
					if (this.ComponentType != null)
					{
						return this.ComponentType.Assembly.FullName;
					}
					return "";
				}
			}

			public ScriptReference ToScriptReference()
			{
				ScriptReference refr = new ScriptReference();
				refr.Assembly = this.AssemblyName;
				refr.Name = this.ResourcePath;
				return refr;
			}

			public override bool Equals(object obj)
			{
				WidgetObjectBuilder.ResourceEntry other = (WidgetObjectBuilder.ResourceEntry)obj;
				return (this.ResourcePath.Equals(other.ResourcePath, StringComparison.OrdinalIgnoreCase) && this.AssemblyName.Equals(other.AssemblyName, StringComparison.OrdinalIgnoreCase));
			}

			public static bool operator ==(WidgetObjectBuilder.ResourceEntry obj1, WidgetObjectBuilder.ResourceEntry obj2)
			{
				return obj1.Equals(obj2);

			}
			public static bool operator !=(WidgetObjectBuilder.ResourceEntry obj1, WidgetObjectBuilder.ResourceEntry obj2)
			{
				return !obj1.Equals(obj2);

			}
			public override int GetHashCode() 
			{
				return (this.AssemblyName.GetHashCode() ^ this.ResourcePath.GetHashCode());
			}
		}


		private static readonly Dictionary<Type, List<ResourceEntry>> _cache;
		private static readonly Dictionary<Type, IList<string>> _cssCache;
		private static readonly object _sync;

		static WidgetObjectBuilder()
		{
			_cache = new Dictionary<Type, List<ResourceEntry>>();
			_cssCache = new Dictionary<Type, IList<string>>();
			_sync = new object();
		}

		/// <summary>
		/// Describe widget.
		/// </summary>
		/// <param name="instance">widget instance</param>
		/// <param name="descriptor">descriptor of widget</param>
		public static void DescribeWidget(object instance, WidgetDescriptor descriptor)
		{
			if (instance == null)  throw new ArgumentNullException("instance");
			if (descriptor == null)  throw new ArgumentNullException("descriptor");
			IUrlResolutionService urlResolver = instance as IUrlResolutionService;
			descriptor.Options = SerializeOption(instance);
		}

		/// <summary>
		/// Serialize option to json string.
		/// </summary>
		/// <param name="obj">widget object</param>
		/// <returns></returns>
		public static string SerializeOption(object obj)
		{

			
			IUrlResolutionService urlResolver = obj as IUrlResolutionService;

			return JsonHelper.WidgetToString(obj, urlResolver);
			/*
			StringWriter strwr = new StringWriter();
			

			WidgetOptionSerializer wr = new WidgetOptionSerializer(strwr, urlResolver);
			wr.WriteValue(obj);
			StringBuilder b = strwr.GetStringBuilder();

			return b.ToString();*/
		}
	}
}
