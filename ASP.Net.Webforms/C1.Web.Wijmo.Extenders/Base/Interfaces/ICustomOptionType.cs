﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
	/// <summary>
	/// An interface indicates that the object will be serialize to JSON or deserialize from JSON manually.
	/// </summary>
	interface ICustomOptionType
	{
		/// <summary>
		/// Serializes the value.
		/// </summary>
		/// <returns></returns>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		string SerializeValue();

#if !EXTENDER
		/// <summary>
		/// DeSerializes the value.
		/// </summary>
		/// <returns></returns>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		void DeSerializeValue(object state);
#endif

		/// <summary>
		/// Gets a value indicating whether value must be enclosed in quotes.
		/// </summary>
		/// <value><c>true</c> if [include quotes]; otherwise, <c>false</c>.</value>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		bool IncludeQuotes { get;  }
	}
}
