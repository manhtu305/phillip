﻿using System;

namespace C1.Web.Wijmo.Extenders.Base
{
	using C1.Web.Wijmo;

	/// <summary>
	/// Use this interface to add support to get extender itself.
	/// </summary>
	public interface IC1Target : IC1Extender
	{
		/// <summary>
		/// Gets the wrapped extender control.
		/// </summary>
		WidgetExtenderControlBase Extender { get; }
	}
}
