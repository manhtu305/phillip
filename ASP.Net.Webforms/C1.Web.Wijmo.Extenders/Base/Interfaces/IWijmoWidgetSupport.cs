﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER 
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
	/// <summary>
	/// Mode of the control
	/// </summary>
	public enum WijmoControlMode
	{
		/// <summary>
		/// A standard WebControl
		/// </summary>
		Web,
		/// <summary>
		/// A mobile control
		/// </summary>
		Mobile
	}

	/// <summary>
	/// Support wijmo widget.
	/// </summary>
	public interface IWijmoWidgetSupport: IWijmoWidgetDependenciesSupport
	{
		/// <summary>
		/// Name of the theme that will be used to style widgets. Available embedded themes: aristo / midnight / ui-lightness.
		/// Note, only one theme can be used for the whole page at one time.
		/// Please, make sure that all widget extenders have the same Theme value.
		/// </summary>
		string Theme { get; set; }

		/// <summary>
		/// Determines whether the widget extender must load client resources from CDN 
		/// (Content Delivery Network) path given by property CDNPath.
		/// </summary>
		bool UseCDN { get; set; }

        /// <summary>
        /// Combined the JavaScripts into one file.
        /// </summary>         
        bool EnableCombinedJavaScripts { get; set; }

		/// <summary>
		/// Content Delivery Network path.
		/// </summary>
		string CDNPath { get; set; }

		/// <summary>
		/// A string array value specifies all css/js references that wijmo control depends on.
		/// </summary>
		string[] CDNDependencyPaths { get; set; }
		/// <summary>
		/// A value that indicates whether the control is a mobile control.
		/// Note that only one value can be used for the whole page at one time. 
		/// Please make sure that all widget controls have the same IsMobile value. 
		/// </summary>
		WijmoControlMode WijmoControlMode { get; set; }

		string WijmoCssAdapter { get; set; }

		/// <summary>
		/// A value indicates whether to register the dependency resources 
		/// according to the control's property settings.
		/// </summary>
		/// <remarks>
		/// This property is enabled only when EnableCombinedJavaScripts is set to true.
		/// </remarks>
		bool EnableConditionalDependencies { get; set; }
	}
}
