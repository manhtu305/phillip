﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
	/// <summary>
	/// Use this interface to add globalize culture support for control/extender.
	/// </summary>
	internal interface ICultureControl
	{
		/// <summary>
		/// gets or sets the culture of control/extender
		/// </summary>
		CultureInfo Culture { get; set; }
	}
}
