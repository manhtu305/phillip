﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
    /// <summary>
    /// An interface that indicates the object could be a function type
    /// </summary>
    interface IFunctionType : ICustomOptionType
    {
        /// <summary>
        /// Function name of the option.
        /// </summary>
        string Function { get; set; }
    }
}
