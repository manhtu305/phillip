﻿using System;

namespace C1.Web.Wijmo.Extenders.Base
{
	/// <summary>
	/// Use this interface to add theme/cdn support for c1extender.
	/// </summary>
	public interface IC1Extender
	{
		/// <summary>
		/// Determines whether the widget extender must load client resources from CDN (Content Delivery Network) path given by property CDNPath.
		/// </summary>
		bool UseCDN { get; set; }

		/// <summary>
		/// Content Delivery Network path.
		/// </summary>
		string CDNPath { get; set; }

		/// <summary>
		/// Name of the theme that will be used to style widgets.
		/// </summary>
		string Theme { get; set; }
	}
}
