﻿using System;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
	/// <summary>
	/// Supports cloning.
	/// </summary>
	public interface IC1Cloneable : ICloneable 
	{
		/// <summary>
		/// Creates a new instance of same class as the instance class that this method is called from.
		/// </summary>
		/// <returns></returns>
		IC1Cloneable CreateInstance();

		/// <summary>
		/// Copies the properties of the <paramref name="copyFrom"/> object into the instance class that this method is called from.
		/// </summary>
		/// <param name="copyFrom">An object that represents the properties to copy.</param>
		void CopyFrom(object copyFrom);
	}
}
