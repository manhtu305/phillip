﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Converters;
namespace C1.Web.Wijmo.Extenders
#else
using C1.Web.Wijmo.Controls.Converters;
namespace C1.Web.Wijmo.Controls
#endif
{
	/// <summary>
	/// Provides a type converter to convert an wijmo theme name to and from other representations.
	/// </summary>
	public class WijmoThemeNameConverter : StringListConverter
	{
		public WijmoThemeNameConverter()
		{
			SetList(false, ResourcesConst.INNER_THEMES);
		}
	}

	/// <summary>
	/// Provides a type converter to convert an wijmo ThemeSwatch name to and from other representations.
	/// </summary>
	public class WijmoThemeSwatchConverter : StringListConverter
	{
		public WijmoThemeSwatchConverter()
		{
			SetList(false, new string[]
						{
							"a",
							"b",
							"c",
							"d",
							"e",
						});
		}
	}

	/// <summary>
	/// Provides a type converter to convert an wijmo ThemeSwatch name to and from other representations.
	/// </summary>
	public class WijmoCssAdapterConverter : StringListConverter
	{
		public WijmoCssAdapterConverter()
		{
			SetList(false, new string[]
						{
							"jquery-ui",
							"bootstrap",
						});
		}
	}
}
