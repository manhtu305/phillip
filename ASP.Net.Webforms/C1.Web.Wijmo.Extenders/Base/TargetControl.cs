﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace C1.Web.Wijmo.Extenders
{
	using C1.Web.Wijmo.Extenders.Base;
	using C1.Web.Wijmo.Extenders.Localization;

	/// <summary>
	/// Class to be used as base for the wijmo extenders target controls.
	/// </summary>
	public abstract partial class TargetControlBase : WebControl, IC1Target
	{				
		#region ** constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="C1.Web.Wijmo.Controls.Base.TargetControlBase"> class that represents a Span HTML tag.
		/// </summary>
		protected TargetControlBase()
			: base()
		{

		}

		#endregion

		#region ** properties

		/// <summary>
		/// Determines whether the widget extender must load client resources from CDN 
		/// (Content Delivery Network) path given by property CDNPath.
		/// </summary>
		[C1Description("C1Base.UseCDN")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		public virtual bool UseCDN
		{
			get
			{
				return this.GetPropertyValue<bool>("UseCDN", false);
			}
			set
			{
				this.SetPropertyValue<bool>("UseCDN", value);
			}
		}

		/// <summary>
		/// Content Delivery Network path.
		/// </summary>
		[C1Description("C1Base.CDNPath")]
		[C1Category("Category.Behavior")]
		[DefaultValue("http://cdn.wijmo.com/")]
		public virtual string CDNPath
		{
			get
			{
				return this.GetPropertyValue<string>("CDNPath", "http://cdn.wijmo.com/");
			}
			set
			{
				this.SetPropertyValue<string>("CDNPath", value);
			}
		}

		/// <summary>
		/// Name of the theme that will be used to style widgets. Available embedded themes: aristo / midnight / ui-lightness.
		/// Note, only one theme can be used for the whole page at one time.
		/// Please, make sure that all widget extenders have the same Theme value.
		/// </summary>
		[C1Description("C1Base.Theme")]
		[C1Category("Category.Behavior")]
		[DefaultValue("aristo")]
		[TypeConverter(typeof(WijmoThemeNameConverter))]
		public virtual string Theme
		{
			get
			{
				return GetPropertyValue<string>("Theme", "aristo");
			}
			set
			{
				SetPropertyValue("Theme", value);
			}
		}

		/// <summary>
		/// Indicates the control applies the theme of JQuery UI or Bootstrap.
		/// </summary>
		[C1Description("C1Base.WijmoCssAdapter")]
		[C1Category("Category.Behavior")]
		[DefaultValue("jquery-ui")]
		[TypeConverter(typeof(WijmoCssAdapterConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string WijmoCssAdapter
		{
			get
			{
				return GetPropertyValue<string>("WijmoCssAdapter", "jquery-ui");
			}
			set
			{
				if (this.WijmoCssAdapter != value)
				{
					SetPropertyValue("WijmoCssAdapter", value);
				}
			}
		}

		#endregion

		#region ** virtual and protected methods/properties

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			base.CreateChildControls();
			if (!this.Controls.Contains(this.Extender))
			{
				this.Controls.AddAt(0, this.Extender);
			}
		}

		/// <summary>
		/// Gets the wrapped extender control.
		/// </summary>
		protected abstract WidgetExtenderControlBase Extender { get; }

		/// <summary>
		/// Gets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="nullValue">The null value.</param>
		/// <returns></returns>
		protected V GetPropertyValue<V>(string propertyName, V nullValue)
		{
			return this.Extender.GetPropertyValue(propertyName, nullValue);
		}

		/// <summary>
		/// Sets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="value">The value.</param>
		protected void SetPropertyValue<V>(string propertyName, V value)
		{
			this.Extender.SetPropertyValue(propertyName, value);
		}

		/// <summary>
		/// Gets a value indicating whether a control is being used on a design surface.
		/// </summary>
		protected bool IsDesignMode = (System.Web.HttpContext.Current == null);

		#endregion

		#region IC1Target Members

		WidgetExtenderControlBase IC1Target.Extender
		{
			get { return Extender; }
		}

		#endregion
	}

	/// <summary>
	/// Class to be used as base for the wijmo extenders target controls.
	/// </summary>
	public abstract partial class TargetDataBoundControlBase : DataBoundControl, IC1Target
	{				
		#region ** constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="C1.Web.Wijmo.Controls.Base.TargetDataBoundControlBase"> class that represents a Span HTML tag.
		/// </summary>
		protected TargetDataBoundControlBase()
			: base()
		{

		}

		#endregion

		#region ** properties

		/// <summary>
		/// Determines whether the widget extender must load client resources from CDN 
		/// (Content Delivery Network) path given by property CDNPath.
		/// </summary>
		[C1Description("C1Base.UseCDN")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		public virtual bool UseCDN
		{
			get
			{
				return this.GetPropertyValue<bool>("UseCDN", false);
			}
			set
			{
				this.SetPropertyValue<bool>("UseCDN", value);
			}
		}

		/// <summary>
		/// Content Delivery Network path.
		/// </summary>
		[C1Description("C1Base.CDNPath")]
		[C1Category("Category.Behavior")]
		[DefaultValue("http://cdn.wijmo.com/")]
		public virtual string CDNPath
		{
			get
			{
				return this.GetPropertyValue<string>("CDNPath", "http://cdn.wijmo.com/");
			}
			set
			{
				this.SetPropertyValue<string>("CDNPath", value);
			}
		}

		/// <summary>
		/// Name of the theme that will be used to style widgets. Available embedded themes: aristo / midnight / ui-lightness.
		/// Note, only one theme can be used for the whole page at one time.
		/// Please, make sure that all widget extenders have the same Theme value.
		/// </summary>
		[C1Description("C1Base.Theme")]
		[C1Category("Category.Behavior")]
		[DefaultValue("aristo")]
		[TypeConverter(typeof(WijmoThemeNameConverter))]
		public virtual string Theme
		{
			get
			{
				return GetPropertyValue<string>("Theme", "aristo");
			}
			set
			{
				SetPropertyValue("Theme", value);
			}
		}

		/// <summary>
		/// Indicates the control applies the theme of JQuery UI or Bootstrap.
		/// </summary>
		[C1Description("C1Base.WijmoCssAdapter")]
		[C1Category("Category.Behavior")]
		[DefaultValue("jquery-ui")]
		[TypeConverter(typeof(WijmoCssAdapterConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string WijmoCssAdapter
		{
			get
			{
				return GetPropertyValue<string>("WijmoCssAdapter", "jquery-ui");
			}
			set
			{
				if (this.WijmoCssAdapter != value)
				{
					SetPropertyValue("WijmoCssAdapter", value);
				}
			}
		}

		#endregion

		#region ** virtual and protected methods/properties

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			base.CreateChildControls();
			if (!this.Controls.Contains(this.Extender))
			{
				this.Controls.AddAt(0, this.Extender);
			}
		}

		/// <summary>
		/// Gets the wrapped extender control.
		/// </summary>
		protected abstract WidgetExtenderControlBase Extender { get; }

		/// <summary>
		/// Gets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="nullValue">The null value.</param>
		/// <returns></returns>
		protected V GetPropertyValue<V>(string propertyName, V nullValue)
		{
			return this.Extender.GetPropertyValue(propertyName, nullValue);
		}

		/// <summary>
		/// Sets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="value">The value.</param>
		protected void SetPropertyValue<V>(string propertyName, V value)
		{
			this.Extender.SetPropertyValue(propertyName, value);
		}

		/// <summary>
		/// Gets a value indicating whether a control is being used on a design surface.
		/// </summary>
		protected bool IsDesignMode = (System.Web.HttpContext.Current == null);

		#endregion

		#region IC1Target Members

		WidgetExtenderControlBase IC1Target.Extender
		{
			get { return Extender; }
		}

		#endregion
	}

	/// <summary>
	/// Class to be used as base for the wijmo extenders target controls.
	/// </summary>
	public abstract partial class TargetCompositeControlBase : CompositeControl, IC1Target
	{				
		#region ** constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="C1.Web.Wijmo.Controls.Base.TargetCompositeControlBase"> class that represents a Span HTML tag.
		/// </summary>
		protected TargetCompositeControlBase()
			: base()
		{

		}

		#endregion

		#region ** properties

		/// <summary>
		/// Determines whether the widget extender must load client resources from CDN 
		/// (Content Delivery Network) path given by property CDNPath.
		/// </summary>
		[C1Description("C1Base.UseCDN")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		public virtual bool UseCDN
		{
			get
			{
				return this.GetPropertyValue<bool>("UseCDN", false);
			}
			set
			{
				this.SetPropertyValue<bool>("UseCDN", value);
			}
		}

		/// <summary>
		/// Content Delivery Network path.
		/// </summary>
		[C1Description("C1Base.CDNPath")]
		[C1Category("Category.Behavior")]
		[DefaultValue("http://cdn.wijmo.com/")]
		public virtual string CDNPath
		{
			get
			{
				return this.GetPropertyValue<string>("CDNPath", "http://cdn.wijmo.com/");
			}
			set
			{
				this.SetPropertyValue<string>("CDNPath", value);
			}
		}

		/// <summary>
		/// Name of the theme that will be used to style widgets. Available embedded themes: aristo / midnight / ui-lightness.
		/// Note, only one theme can be used for the whole page at one time.
		/// Please, make sure that all widget extenders have the same Theme value.
		/// </summary>
		[C1Description("C1Base.Theme")]
		[C1Category("Category.Behavior")]
		[DefaultValue("aristo")]
		[TypeConverter(typeof(WijmoThemeNameConverter))]
		public virtual string Theme
		{
			get
			{
				return GetPropertyValue<string>("Theme", "aristo");
			}
			set
			{
				SetPropertyValue("Theme", value);
			}
		}

		/// <summary>
		/// Indicates the control applies the theme of JQuery UI or Bootstrap.
		/// </summary>
		[C1Description("C1Base.WijmoCssAdapter")]
		[C1Category("Category.Behavior")]
		[DefaultValue("jquery-ui")]
		[TypeConverter(typeof(WijmoCssAdapterConverter))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[RefreshProperties(RefreshProperties.All)]
		public virtual string WijmoCssAdapter
		{
			get
			{
				return GetPropertyValue<string>("WijmoCssAdapter", "jquery-ui");
			}
			set
			{
				if (this.WijmoCssAdapter != value)
				{
					SetPropertyValue("WijmoCssAdapter", value);
				}
			}
		}

		#endregion

		#region ** virtual and protected methods/properties

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			base.CreateChildControls();
			if (!this.Controls.Contains(this.Extender))
			{
				this.Controls.AddAt(0, this.Extender);
			}
		}

		/// <summary>
		/// Gets the wrapped extender control.
		/// </summary>
		protected abstract WidgetExtenderControlBase Extender { get; }

		/// <summary>
		/// Gets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="nullValue">The null value.</param>
		/// <returns></returns>
		protected V GetPropertyValue<V>(string propertyName, V nullValue)
		{
			return this.Extender.GetPropertyValue(propertyName, nullValue);
		}

		/// <summary>
		/// Sets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="value">The value.</param>
		protected void SetPropertyValue<V>(string propertyName, V value)
		{
			this.Extender.SetPropertyValue(propertyName, value);
		}

		/// <summary>
		/// Gets a value indicating whether a control is being used on a design surface.
		/// </summary>
		protected bool IsDesignMode = (System.Web.HttpContext.Current == null);

		#endregion

		#region IC1Target Members

		WidgetExtenderControlBase IC1Target.Extender
		{
			get { return Extender; }
		}

		#endregion
	}
}
