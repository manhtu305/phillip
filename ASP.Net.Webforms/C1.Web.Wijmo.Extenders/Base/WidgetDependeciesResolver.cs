﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text.RegularExpressions;
using System.Linq;
using System.Globalization;

#if EXTENDER 
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
#if !EXTENDER 
	[SmartAssembly.Attributes.DoNotObfuscate]
#endif
	internal static class WidgetDependenciesResolver
    {
        #region ** fields
#if DEBUG
        public const string JSSuffix = ".js";
#else
        public const string JSSuffix = ".min.js";
#endif
		#endregion

		internal static string GetCultureResource(CultureInfo culture) 
		{
			return ResourcesConst.JS_ROOT + ".cultures.globalize.culture-" + culture.Name + JSSuffix;
		}

		private static Regex _reNonChar = new Regex(@"\W+", RegexOptions.Compiled);

        //helper for _GetReferences(), remember already parsed types to avoid circle references..
        private static Dictionary<Type, bool> _parsedTypes = new Dictionary<Type, bool>();

		private static bool IsMinifiedScriptResource(string resource)
		{
			List<string> minifiedResourceNames = new List<string>() { ResourcesConst.BOOTSTRAP_JS, "globalize.cultures.js", 
				ResourcesConst.JQUERY, ResourcesConst.JQUERY_UI, ResourcesConst.JQUERY_JQM, ResourcesConst.JQUERY_MOBILE, 
				ResourcesConst.WIJMO_OPEN_JS, ResourcesConst.WIJMO_PRO_JS, ResourcesConst.WIJMO_BOOTSTRAP_JS, 
				ResourcesConst.WIJMO_MOBILE_JS};
#if !EXTENDER
			List<string> wrappers = new List<string>(){ResourcesConst.C1WRAPPER_CONTROL, ResourcesConst.C1WRAPPER_OPEN, 
				ResourcesConst.C1WRAPPER_PRO, ResourcesConst.MOBILE_CONTROL, ResourcesConst.WIDGETS_CONTROL};
			return resource.StartsWith("base.") || minifiedResourceNames.Contains(resource) || wrappers.Contains(resource);
#else
			return resource.StartsWith("base.") || minifiedResourceNames.Contains(resource);
#endif
		}

		/// <summary>
		/// Returns css references list from which <paramref name="extender"/> depends.
		/// </summary>
		/// <param name="extender"></param>
		/// <param name="extenderInstance"></param>
		/// <returns></returns>
		public static OrderedDictionary GetCssReferences(Type extender, IWijmoWidgetSupport extenderInstance = null)
		{
			if (extender != null)
			{
                return GetReferences(extender, extenderInstance, ".css");
			}

			return null;
		}

		/// <summary>
		/// Returns css references list from which <paramref name="extender"/> depends.
		/// </summary>
		/// <param name="extender"></param>
		/// <returns></returns>
		public static OrderedDictionary GetCssReferences(IWijmoWidgetSupport extender)
		{
			return (extender != null)
				? GetCssReferences(extender.GetType(), extender)
				: null;
		}

		/// <summary>
		/// Returns js references list from which <paramref name="extender"/> depends.
		/// </summary>
		/// <param name="extender"></param>
		/// <param name="extenderInstance"></param>
		/// <returns></returns>
		public static OrderedDictionary GetJsReferences(Type extender, IWijmoWidgetSupport extenderInstance = null)
		{
			if (extender != null)
			{
                return GetReferences(extender, extenderInstance, ".js");
			}

			return null;
		}

		/// <summary>
		/// Returns js references list from which <paramref name="extender"/> depends.
		/// </summary>
		/// <param name="extender"></param>
		/// <returns></returns>
		public static OrderedDictionary GetJsReferences(IWijmoWidgetSupport extender)
		{
			return (extender != null)
				? GetJsReferences(extender.GetType(), extender)
				: null;
		}

        /// <summary>
        /// Core function to get type's references.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="filterBy">js or css</param>
        /// <returns></returns>
		private static OrderedDictionary GetReferences(Type type, IWijmoWidgetSupport instance, string filterBy)
		{
            OrderedDictionary result = new OrderedDictionary();

            // Gather dependencies by inheritance chain. Dependencies of a parent type have higher priority, will be added first.
            Dictionary<string, string> references = new Dictionary<string, string>();

            _parsedTypes.Clear();

			ExtractTypeDependencies(type, instance, references);
                        
            // Determines extender dependencies from .js, .css or other extenders.
            foreach (string key in references.Keys)
            {
                string strVal = references[key] as string;

                strVal = ResolveDependency(strVal, filterBy);//get the correct file path.
                if (strVal != null)
                {
                    result.Add(key, strVal);
                }
            }

            return result;
		}
        
		/// <summary>
		/// Extract type's dependencies which are marked as WidgetDependenciesAttribute.
		/// </summary>
		/// <param name="type"></param>
		/// <param name="instance"></param>		
		/// <returns>Type's dependency list (.js, .cs) which is ordred as the same sequence as WidgetDependenciesAttribute.</returns>
		private static void ExtractTypeDependencies(Type type, IWijmoWidgetSupport instance, Dictionary<string, string> references)
		{
			if (type == null || !IsTypeSupportDependencies(type) || _parsedTypes.ContainsKey(type))
			{
				return;
			}

			bool typeSupportsCondDependency = IsTypeSupportConditionalDependency(type);

			if (!typeSupportsCondDependency) // do not cache types inherited from WijmoConditionalBase because their instances are created at run-time (see code below) to resolve dependencies dynamically (depending on properties of the particular Wijmo control), so there may be conflicts in the future.
			{
				//remember already parsed types to avoid circle references.
				_parsedTypes[type] = true;
			}

			//get base type's dependencies.
			ExtractTypeDependencies(type.BaseType, instance, references);

			WidgetDependenciesAttribute[] wdAttributes = (WidgetDependenciesAttribute[])type.GetCustomAttributes(typeof(WidgetDependenciesAttribute), false);

			foreach (WidgetDependenciesAttribute wda in wdAttributes)
			{
				foreach (object dependency in wda.Dependencies)
				{
					ProcessDependencyItem(dependency, instance, references);
				}
			}

			// conditional dependencies
			if (instance != null && typeSupportsCondDependency && !type.IsAbstract)
			{
				WidgetConditionalDependencyAttribute[] wcdAttributes = (WidgetConditionalDependencyAttribute[])type.GetCustomAttributes(typeof(WidgetConditionalDependencyAttribute), false);
				if (wcdAttributes.Length > 0)
				{
					ConditionalDependencyProxy helperInstance = (ConditionalDependencyProxy)Activator.CreateInstance(type); // Create an instance of the proxy class with WidgetConditionalDependencyAttribute attribute defined.

					foreach (WidgetConditionalDependencyAttribute wcda in wcdAttributes)
					{
						// rollout dependency if...
						if (!wcda.CanBeResolvedBy(instance) // ...the instance is NOT designed for it, so process it as a regular, non-conditional dependency
							|| helperInstance.Resolve(instance, wcda.Dependency))  // ...or it can be resolved by the instance
						{
							ProcessDependencyItem(wcda.Dependency, instance, references);
						}
					}
				}
			}
		}

		private static void ProcessDependencyItem(object dependency, IWijmoWidgetSupport instance, Dictionary<string, string> references)
		{
			if (dependency is Type)
			{
				//get depend type's dependencies.
				ExtractTypeDependencies(dependency as Type, instance, references);
			}
			else
			{
				string value = dependency as string;
				string key = _reNonChar.Replace(value, string.Empty).ToLower();
				if (!references.ContainsKey(key))
				{
					references[key] = value;
				}
			}
		}

        /// <summary>
		/// Check whether given type support WidgetDependenciesAttribute and WidgetConditionalDependencyAttribute.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static bool IsTypeSupportDependencies(Type type)
        {
			return typeof(IWijmoWidgetDependenciesSupport).IsAssignableFrom(type);
        }

		/// <summary>
		/// Check whether given type support WidgetConditionalDependencyAttribute.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		private static bool IsTypeSupportConditionalDependency(Type type)
		{
			return typeof(ConditionalDependencyProxy).IsAssignableFrom(type);
		}

        /// <summary>
        /// Get dependency's full file path.
        /// </summary>
        /// <param name="dependency"></param>
        /// <param name="filterBy"></param>
        /// <returns></returns>
#if !EXTENDER 
		[SmartAssembly.Attributes.DoNotObfuscate]
#endif
		public static string ResolveDependency(string dependency, string filterBy)
		{
			if (dependency.EndsWith(filterBy, StringComparison.OrdinalIgnoreCase))
			{
				if (dependency.StartsWith("@")) // absoulte path
				{
					return dependency.Substring(1);
				}

				// convert relative path to absolute.
                if (filterBy == ".js")
                {
					if (IsMinifiedScriptResource(dependency))
                    {
						return ResourcesConst.JS_ROOT + "." + dependency;
                    }
                    else
                    {
                        int location = dependency.LastIndexOf(".js");
                        dependency = dependency.Substring(0, location);
						return ResourcesConst.JS_ROOT + "." + dependency + JSSuffix;
                    }
                }
                else // .css
                {
					if (dependency.StartsWith("extensions")) 
					{
						return ResourcesConst.THEME_ROOT + "." + dependency;
					}
                    return ResourcesConst.CSS_ROOT + "." + dependency;
                }
			}

			return null;
		}
	}
}