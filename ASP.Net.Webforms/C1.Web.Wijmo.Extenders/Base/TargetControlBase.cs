﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace C1.Web.Wijmo.Extenders
{
	using C1.Web.Wijmo.Extenders.Base;
	using System.Collections;
	using C1.Web.Wijmo.Extenders.Localization;

	/// <summary>
	/// Class to be used as base for the wijmo extenders target base controls.
	/// </summary>
	public abstract partial class TargetControlBase
	{		
		#region ** constructors

		/// <summary>
		/// Initializes a new instance of the System.Web.UI.WebControls.WebControl class using the specified HTML tag.
		/// </summary>
		/// <param name="tag">One of the System.Web.UI.HtmlTextWriterTag values..</param>
		public TargetControlBase(HtmlTextWriterTag tag)
			: base(tag)
		{

		}

		/// <summary>
		/// Initializes a new instance of the System.Web.UI.WebControls.WebControl class using the specified HTML tag.
		/// </summary>
		/// <param name="tag">An HTML tag.</param>
		protected TargetControlBase(string tag)
			: base(tag)
		{

		}

		#endregion

		private Hashtable _innerStates = null;
		/// <summary>
		/// When control has an javascript function type property, get the property names.
		/// </summary>
		public virtual Hashtable InnerStates
		{
			get
			{
				if (this._innerStates == null)
				{
					this._innerStates = new Hashtable();
				}

				return this._innerStates;
			}
		}
	}
}
