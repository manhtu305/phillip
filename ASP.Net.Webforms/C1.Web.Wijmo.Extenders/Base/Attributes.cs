﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


#if EXTENDER 
namespace C1.Web.Wijmo.Extenders
#else
using C1.Web.Wijmo.Controls.Base;
namespace C1.Web.Wijmo.Controls	
#endif
{
	#region ** attributes

	/// <summary>
	/// Use this attribute in order to mark property as widget option.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class WidgetOptionAttribute : Attribute
	{
		public WidgetOptionAttribute()
		{
		}
	}

	/// <summary>
	/// Use this attribute in order to change name of the option.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class WidgetOptionNameAttribute : Attribute
	{
		// Fields
		private string _name;

		/// <summary>
		/// Initializes a new instance of the WidgetOptionNameAttribute class.
		/// </summary>
		public WidgetOptionNameAttribute()
		{
		}

		/// <summary>
		/// Initializes a new instance of the WidgetOptionNameAttribute class.
		/// </summary>
		/// <param name="name">name of widget option</param>
		public WidgetOptionNameAttribute(string name)
		{
			this._name = name;
		}

		/// <summary>
		/// Indicates whether the value of this instance is the default value for the derived class.
		/// </summary>
		/// <returns></returns>
		public override bool IsDefaultAttribute()
		{
			return string.IsNullOrEmpty(this.Name);
		}

		/// <summary>
		/// Name of the widget option
		/// </summary>
		public string Name
		{
			get
			{
				return this._name;
			}
		}
	}

	/// <summary>
	/// Use this attribute in order to mark option property as required.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	class WidgetRequiredOptionAttribute : Attribute
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetRequiredOptionAttribute"/> class.
		/// </summary>
		public WidgetRequiredOptionAttribute()
		{
		}
	}

	/// <summary>
	/// Use this attribute in order to mark property as widget event.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class WidgetEventAttribute : Attribute
	{
		private string _arguments;


		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetEventAttribute"/> class.
		/// </summary>
		public WidgetEventAttribute()
		{
			this._arguments = "";
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetEventAttribute"/> class.
		/// </summary>
		/// <example>
		/// Example of the string with attributes: "e, param1, param2"
		/// </example>
		/// <param name="args">Event handler arguments.</param>
		public WidgetEventAttribute(string args)
		{
			this._arguments = args;
		}

		/// <summary>
		/// Client side handler arguments.
		/// </summary>
		/// <example>
		/// Example of the string with attributes: "e, param1, param2"
		/// </example>
		public string Arguments
		{
			get
			{
				return this._arguments;
			}
		}
	}

	/// <summary>
	/// Determines extender dependencies from .js, .css or other extenders.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)] // AllowMultiple = false should be used here because Type.GetCustomAttributes() doesn't guarantees that custom attributes will be returned in any particular order.
	public class WidgetDependenciesAttribute : Attribute
	{
		private object[] _dependencies;

		/* WARNING!!!: don't use or uncomment this constructor. Causes exception during rendering in Visual Studio designer when parameters contains typeof() clause.
		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="dependencies">The dependencies.</param>
		public WidgetDependenciesAttribute(params object[] dependencies)
		{
			_dependencies = dependencies;
		}*/
		
		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="dependencies">The dependencies.</param>
		public WidgetDependenciesAttribute(params string[] dependencies)
		{
			_dependencies = dependencies;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="d1">The dependency.</param>
		public WidgetDependenciesAttribute(object d1)
		{
			_dependencies = new object[] { d1 };
		}

#pragma warning disable 1591

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="d1">The dependency.</param>
		/// <param name="d2"></param>
		public WidgetDependenciesAttribute(object d1, object d2)
		{
			_dependencies = new object[] { d1, d2 };
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="d1"></param>
		/// <param name="d2"></param>
		/// <param name="d3"></param>
		public WidgetDependenciesAttribute(object d1, object d2, object d3)
		{
			_dependencies = new object[] { d1, d2, d3 };
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="d1"></param>
		/// <param name="d2"></param>
		/// <param name="d3"></param>
		/// <param name="d4"></param>
		public WidgetDependenciesAttribute(object d1, object d2, object d3, object d4)
		{
			_dependencies = new object[] { d1, d2, d3, d4 };
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="d1"></param>
		/// <param name="d2"></param>
		/// <param name="d3"></param>
		/// <param name="d4"></param>
		/// <param name="d5"></param>
		public WidgetDependenciesAttribute(object d1, object d2, object d3, object d4, object d5)
		{
			_dependencies = new object[] { d1, d2, d3, d4, d5 };
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="d1"></param>
		/// <param name="d2"></param>
		/// <param name="d3"></param>
		/// <param name="d4"></param>
		/// <param name="d5"></param>
		/// <param name="d6"></param>
		public WidgetDependenciesAttribute(object d1, object d2, object d3, object d4, object d5, object d6)
		{
			_dependencies = new object[] { d1, d2, d3, d4, d5, d6 };
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="d1"></param>
		/// <param name="d2"></param>
		/// <param name="d3"></param>
		/// <param name="d4"></param>
		/// <param name="d5"></param>
		/// <param name="d6"></param>
		/// <param name="d7"></param>
		public WidgetDependenciesAttribute(object d1, object d2, object d3, object d4, object d5, object d6, object d7)
		{
			_dependencies = new object[] { d1, d2, d3, d4, d5, d6, d7 };
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="d1"></param>
		/// <param name="d2"></param>
		/// <param name="d3"></param>
		/// <param name="d4"></param>
		/// <param name="d5"></param>
		/// <param name="d6"></param>
		/// <param name="d7"></param>
		/// <param name="d8"></param>
		public WidgetDependenciesAttribute(object d1, object d2, object d3, object d4, object d5, object d6, object d7, object d8)
		{
			_dependencies = new object[] { d1, d2, d3, d4, d5, d6, d7, d8 };
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="d1"></param>
		/// <param name="d2"></param>
		/// <param name="d3"></param>
		/// <param name="d4"></param>
		/// <param name="d5"></param>
		/// <param name="d6"></param>
		/// <param name="d7"></param>
		/// <param name="d8"></param>
		/// <param name="d9"></param>
		public WidgetDependenciesAttribute(object d1, object d2, object d3, object d4, object d5, object d6, object d7, object d8, object d9)
		{
			_dependencies = new object[] { d1, d2, d3, d4, d5, d6, d7, d8, d9 };
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="d1"></param>
		/// <param name="d2"></param>
		/// <param name="d3"></param>
		/// <param name="d4"></param>
		/// <param name="d5"></param>
		/// <param name="d6"></param>
		/// <param name="d7"></param>
		/// <param name="d8"></param>
		/// <param name="d9"></param>
		/// <param name="d10"></param>
		public WidgetDependenciesAttribute(object d1, object d2, object d3, object d4, object d5, object d6, object d7, object d8, object d9, object d10)
		{
			_dependencies = new object[] { d1, d2, d3, d4, d5, d6, d7, d8, d9, d10 };
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="d1"></param>
		/// <param name="d2"></param>
		/// <param name="d3"></param>
		/// <param name="d4"></param>
		/// <param name="d5"></param>
		/// <param name="d6"></param>
		/// <param name="d7"></param>
		/// <param name="d8"></param>
		/// <param name="d9"></param>
		/// <param name="d10"></param>
		/// <param name="d11"></param>
		public WidgetDependenciesAttribute(object d1, object d2, object d3, object d4, object d5, object d6, object d7, object d8, object d9, object d10, object d11)
		{
			_dependencies = new object[] { d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11 };
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="d1"></param>
		/// <param name="d2"></param>
		/// <param name="d3"></param>
		/// <param name="d4"></param>
		/// <param name="d5"></param>
		/// <param name="d6"></param>
		/// <param name="d7"></param>
		/// <param name="d8"></param>
		/// <param name="d9"></param>
		/// <param name="d10"></param>
		/// <param name="d11"></param>
		/// <param name="d12"></param>
		public WidgetDependenciesAttribute(object d1, object d2, object d3, object d4, object d5, object d6, object d7, object d8, object d9, object d10, object d11, object d12)
		{
			_dependencies = new object[] { d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12 };
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="d1"></param>
		/// <param name="d2"></param>
		/// <param name="d3"></param>
		/// <param name="d4"></param>
		/// <param name="d5"></param>
		/// <param name="d6"></param>
		/// <param name="d7"></param>
		/// <param name="d8"></param>
		/// <param name="d9"></param>
		/// <param name="d10"></param>
		/// <param name="d11"></param>
		/// <param name="d12"></param>
		/// <param name="d13"></param>
		public WidgetDependenciesAttribute(object d1, object d2, object d3, object d4, object d5, object d6, object d7, object d8, object d9, object d10, object d11, object d12, object d13)
		{
			_dependencies = new object[] { d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13 };
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="d1"></param>
		/// <param name="d2"></param>
		/// <param name="d3"></param>
		/// <param name="d4"></param>
		/// <param name="d5"></param>
		/// <param name="d6"></param>
		/// <param name="d7"></param>
		/// <param name="d8"></param>
		/// <param name="d9"></param>
		/// <param name="d10"></param>
		/// <param name="d11"></param>
		/// <param name="d12"></param>
		/// <param name="d13"></param>
		/// <param name="d14"></param>
		public WidgetDependenciesAttribute(object d1, object d2, object d3, object d4, object d5, object d6, object d7, object d8, object d9, object d10, object d11, object d12, object d13, object d14)
		{
			_dependencies = new object[] { d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14 };
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="d1"></param>
		/// <param name="d2"></param>
		/// <param name="d3"></param>
		/// <param name="d4"></param>
		/// <param name="d5"></param>
		/// <param name="d6"></param>
		/// <param name="d7"></param>
		/// <param name="d8"></param>
		/// <param name="d9"></param>
		/// <param name="d10"></param>
		/// <param name="d11"></param>
		/// <param name="d12"></param>
		/// <param name="d13"></param>
		/// <param name="d14"></param>
		/// <param name="d15"></param>
		public WidgetDependenciesAttribute(object d1, object d2, object d3, object d4, object d5, object d6, object d7, object d8, object d9, object d10, object d11, object d12, object d13, object d14, object d15)
		{
			_dependencies = new object[] { d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15 };
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetDependenciesAttribute"/> class.
		/// </summary>
		/// <param name="d1"></param>
		/// <param name="d2"></param>
		/// <param name="d3"></param>
		/// <param name="d4"></param>
		/// <param name="d5"></param>
		/// <param name="d6"></param>
		/// <param name="d7"></param>
		/// <param name="d8"></param>
		/// <param name="d9"></param>
		/// <param name="d10"></param>
		/// <param name="d11"></param>
		/// <param name="d12"></param>
		/// <param name="d13"></param>
		/// <param name="d14"></param>
		/// <param name="d15"></param>
		/// <param name="d16"></param>
		public WidgetDependenciesAttribute(object d1, object d2, object d3, object d4, object d5, object d6, object d7, 
			object d8, object d9, object d10, object d11, object d12, object d13, object d14, object d15, object d16)
		{
			_dependencies = new object[] { d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, d16 };
		}

#pragma warning restore 1591

		/// <summary>
		/// Array of dependencies.
		/// </summary>
		/// 
		/// In the array of dependencies each item can be one of the following:
		/// 
		/// - relative path to a resource.
		///   for .js path should be relative to C1.Web.Wijmo.Extenders.Resources.wijmo
		///   for .css path should be relative to C1.Web.Wijmo.Extenders.Resources.themes.wijmo
		///   Example:
		///   [WidgetDependencies("jquery.wijmo.wijcalendar.js"]
		///   [WidgetDependencies("jquery.wijmo.wijcalendar.css"]
		/// 
		/// - absolute path to a resource, in this case item should be prefixed with "@".
		///   Example:
		///   [WidgetDependencies("@C1.Web.Wijmo.Extenders.Resources.resource.js"]
		///   
		/// - typeof(Extender)
		///   Example:
		///   [WidgetDependencies(typeof(CalendarExtender))]
		public object[] Dependencies
		{
			get { return _dependencies; }
		}
	}


	/// <summary>
	/// Determines conditional extender dependencies from .js, .css or other extenders.
	/// </summary>
	/// <remarks>
	/// Should be only specified on classes derived from ConditionalDependencyProxy.
	/// </remarks>
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)] // Restrict to a single instance because Type.GetCustomAttributes() doesn't guarantees that custom attributes will be returned in any particular order.
	public class WidgetConditionalDependencyAttribute : Attribute
	{
		/// <summary>
		/// Dependency.
		/// </summary>
		public readonly object Dependency;

		/// <summary>
		/// Defines the type, the instance of which can resolve the dependency.
		/// </summary>
		public readonly Type Resolver;

		/// <summary>
		/// Initializes a new instance of the <see cref="WidgetConditionalDependencyAttribute"/> class.
		/// </summary>
		/// <param name="dependency">A dependency, see the WidgetDependencyAttribute.Dependencies property for more details.</param>
		/// <param name="resolver">Defines the type, the instance of which is designed for resolve the dependency.</param>
		public WidgetConditionalDependencyAttribute(object dependency, Type resolver)
		{
			if (dependency == null)
			{
				throw new ArgumentNullException("dependency");
			}

			if (resolver == null)
			{
				throw new ArgumentNullException("resolver");
			}

			Dependency = dependency;
			Resolver = resolver;
		}

		/// <summary>
		/// Test whether instance is designed for resolve dependency.
		/// </summary>
		/// <param name="instance"></param>
		/// <returns></returns>
		public bool CanBeResolvedBy(IWijmoWidgetSupport instance)
		{
			return instance.EnableCombinedJavaScripts && instance.EnableConditionalDependencies
				&& Resolver.IsAssignableFrom(instance.GetType()); // same type or derived class
		}
	}

	/// <summary>
	/// Use this attribute in order to mark property as widget javascript function.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class WidgetFunctionAttribute : Attribute
	{
		#region *** Ctor
		public WidgetFunctionAttribute()
			:base()
		{
		}
		#endregion end *** Ctor
	}

	#endregion

	#region ** the CollectionItemTypeAttribute attribute
	/// <summary>
	/// Use this attribute in order to specify the type of the item for the collection property.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class CollectionItemTypeAttribute : Attribute
	{

		#region ** fields
		private Type _type;
		#endregion end of ** fields.

		#region ** constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="CollectionItemTypeAttribute"/> class.
		/// </summary>
		/// <param name="type"></param>
		public CollectionItemTypeAttribute(Type type)
		{
			this._type = type;
		}
		#endregion end of ** constructors.

		#region ** properties
		/// <summary>
		/// Item Type.
		/// </summary>
		public Type ItemType
		{
			get
			{
				return this._type;
			}
		}
		#endregion end of ** properties.
	} 
	#endregion end of ** the CollectionItemTypeAttribute attribute.
	
	#region ** the ArrayItemTypeAttribute attribute
	/// <summary>
	/// Use this attribute in order to specify the type of the item for the array property.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class ArrayItemTypeAttribute : Attribute
	{

		#region ** fields
		private Type _type;
		#endregion end of ** fields.

		#region ** constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="ArrayItemTypeAttribute"/> class.
		/// </summary>
		/// <param name="type"></param>
		public ArrayItemTypeAttribute(Type type)
		{
			this._type = type;
		}
		#endregion end of ** constructors.

		#region ** properties
		/// <summary>
		/// Item type.
		/// </summary>
		public Type ItemType
		{
			get
			{
				return this._type;
			}
		}
		#endregion end of ** properties.
	}
	#endregion end of ** the ArrayItemTypeAttribute attribute.
#if !EXTENDER


	/// <summary>
	/// Use this attribute in order to save/load the settings of the widget option.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Class, AllowMultiple = false)]
	public class LayoutAttribute : System.Attribute
	{
		private LayoutType _layoutType = LayoutType.All;

		internal LayoutAttribute()
		{
		}

		/// <summary>
		/// Layout Attribute
		/// </summary>
		/// <param name="layoutTypes"></param>
		public LayoutAttribute(LayoutType layoutTypes)
		{
			_layoutType = layoutTypes;
		}

		/// <summary>
		/// Layout Type
		/// </summary>
		public LayoutType LayoutType
		{
			get
			{
				return _layoutType;
			}
		}

	}

	/// <summary>
	/// Used for XML serialization.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
	public class SerializeAllAttribute : System.Attribute
	{
		private bool _value = true;

		internal SerializeAllAttribute()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="SerializeAllAttribute"/> class.
		/// </summary>
		/// <param name="value"></param>
		public SerializeAllAttribute(bool value)
		{
			_value = value;
		}

		/// <summary>
		/// Value of Serialize All Attribute
		/// </summary>
		public bool Value
		{
			get
			{
				return _value;
			}
		}
	}

#endif


	/*
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class ElementReferenceAttribute : Attribute
	{
		// Methods
		public ElementReferenceAttribute() { }
	}
	*/
}

#region ** the BitmapSuffixInSameAssemblyAttribute
namespace System.Drawing
{
	[AttributeUsage(AttributeTargets.Assembly)]
	internal class BitmapSuffixInSameAssemblyAttribute : Attribute
	{
	}
}

#endregion