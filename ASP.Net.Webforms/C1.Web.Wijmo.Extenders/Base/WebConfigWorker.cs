﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.Design;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Reflection;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
	internal class WebConfigWorker
	{
		#region ** fields

		private static Hashtable _runtimeCache = new Hashtable();
#if EXTENDER
    #if ASP_NET4
        public const string HANDLERTYPE = "C1.Web.Wijmo.Extenders.WijmoHttpHandler, C1.Web.Wijmo.Extenders.4";
    #elif ASP_NET35
		    public const string HANDLERTYPE = "C1.Web.Wijmo.Extenders.WijmoHttpHandler, C1.Web.Wijmo.Extenders.3";
#else
		    public const string HANDLERTYPE = "C1.Web.Wijmo.Extenders.WijmoHttpHandler, C1.Web.Wijmo.Extenders.2";
#endif
#else
#if ASP_NET4
            //public const string HANDLERTYPE = "C1.Web.Wijmo.Controls.WijmoHttpHandler, C1.Web.Wijmo.Controls.4";
		public static Type type = typeof(C1.Web.Wijmo.Controls.WijmoHttpHandler);
		public static string HANDLERTYPE = type.FullName + ", " + type.Assembly.FullName;
#elif ASP_NET35
		    public const string HANDLERTYPE = "C1.Web.Wijmo.Controls.WijmoHttpHandler, C1.Web.Wijmo.Controls.3";
#else
		    public const string HANDLERTYPE = "C1.Web.Wijmo.Controls.WijmoHttpHandler, C1.Web.Wijmo.Controls.2";
#endif
#endif
        #endregion

        #region ** methods

        internal static string ReadAppSetting(Control control, string strKey, string defaultValue)
		{
            if (HttpContext.Current != null)
            {
                string runStrKey = strKey;
                if (strKey == "WijmoTheme" &&
                    control.Page != null &&
                    HttpContext.Current.Session != null /* fix for 28896 */)
                {
                    // Use Session for WijmoTheme at runtime:
                    // use it indirectly in order to avoid problems with callbacks
                    runStrKey = strKey + control.Page.Session.SessionID;
                }
                if (_runtimeCache.ContainsKey(runStrKey))
                {
                    return (string)_runtimeCache[runStrKey];
                }
            }
            string resultValue = string.Empty;
            try
            {
                // fix for issue with local web.config:
                // first, try to read key from local web.config:
                System.Configuration.Configuration cfg = OpenWebConfig(control, true);
                if (cfg.AppSettings.Settings[strKey] != null)
                {
                    resultValue = cfg.AppSettings.Settings[strKey].Value;
                }
            }
            catch (Exception) { }
            if (string.IsNullOrEmpty(resultValue))
            {
                // then, try to read key from global web.config:
                resultValue = WebConfigurationManager.AppSettings[strKey];
            }

            resultValue = string.IsNullOrEmpty(resultValue) ? defaultValue : resultValue;
            if (HttpContext.Current != null)
            {
                _runtimeMode_WriteAppSetting(control, strKey, resultValue);
            }
            else 
            {
                _runtimeCache[strKey] = resultValue;
            }
            return resultValue;
        }

		internal static void WriteAppSetting(Control control, string strKey, string value)
		{
			if (HttpContext.Current == null)
			{
				_designMode_WriteAppSetting(control, strKey, value);
			}
			else
			{
				_runtimeMode_WriteAppSetting(control, strKey, value);
			}
		}

		#endregion

		#region ** private implementation

		private static Configuration OpenWebConfig(Control control, bool isReadOnly)
		{
			if (HttpContext.Current == null)
			{
				System.ComponentModel.ISite site = GetSite(control);
				IWebApplication webApp = (IWebApplication)site.GetService(typeof(IWebApplication));
				return webApp.OpenWebConfiguration(isReadOnly);
			}
			else
			{
				return WebConfigurationManager.OpenWebConfiguration(HostingEnvironment.ApplicationVirtualPath);
			}
		}

		private static void _runtimeMode_WriteAppSetting(Control control, string strKey, string value)
		{
			if (strKey == "WijmoTheme" && control.Page != null && 
				HttpContext.Current.Session != null /* fix for 28896 */)
			{
				// Use Session for WijmoTheme at runtime:
                // use it indirectly in order to avoid problems with callbacks
				_runtimeCache[strKey + control.Page.Session.SessionID] = value;
			}
			else
			{
				_runtimeCache[strKey] = value;
			}
		}


		private static void _designMode_WriteAppSetting(Control control, string strKey, string value)
		{
			try
			{
				try
				{
					System.IO.FileInfo fileInfo = new System.IO.FileInfo(GetWebConfigPhysicalFilePath(control));
					if (fileInfo.IsReadOnly == true)
					{
						if (System.Windows.Forms.MessageBox.Show(
								String.Format("Do you want to make web.config writable in order to change setting {0} to {1}?", strKey, value),
							"Confirmation", System.Windows.Forms.MessageBoxButtons.YesNo,
							System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
						{
							fileInfo.IsReadOnly = false;
						}
						else
						{
							return;
						}

					}
				}
				catch (Exception ex)
				{
					System.Windows.Forms.MessageBox.Show(string.Format("Unable to make web.config file writable ({0}).", ex.Message),
							"Error", System.Windows.Forms.MessageBoxButtons.OK,
							System.Windows.Forms.MessageBoxIcon.Error);
					return;
				}
				Configuration cfg = OpenWebConfig(control, false);
				AppSettingsSection appSettings = cfg.AppSettings;
				if (appSettings != null)
				{
					if (appSettings.Settings[strKey] != null)
					{
						appSettings.Settings[strKey].Value = value;
					}
					else
					{
						appSettings.Settings.Add(strKey, value);
					}
                    _runtimeCache[strKey] = value;
				}
				cfg.Save(ConfigurationSaveMode.Minimal);
			}
			catch (ConfigurationErrorsException ex)
			{
				System.Windows.Forms.MessageBox.Show(string.Format("Unable to change web.config file ({0}).", ex.Message));
				throw ex;
			}
		}


		private static string GetWebConfigPhysicalFilePath(Control control)
		{
			if (HttpContext.Current == null)
			{
				//design mode:
				IWebApplication webApp = (IWebApplication)control.Site.GetService(typeof(IWebApplication));
				return webApp.OpenWebConfiguration(true).FilePath;
			}
			else
			{
				return WebConfigurationManager.OpenWebConfiguration(HostingEnvironment.ApplicationVirtualPath).FilePath;
			}
		}

		private static System.ComponentModel.ISite GetSite(System.Web.UI.Control webControl)
		{
			if (webControl.Site != null) return webControl.Site;
			for (System.Web.UI.Control control = webControl.Parent; control != null; control = control.Parent)
			{
				if (control.Site != null) return control.Site;
			}
			return null;
		}

		#endregion

        internal static bool IsIIS7Request(HttpContext context)
        {
            IServiceProvider provider = context;
            HttpWorkerRequest service = (HttpWorkerRequest)provider.GetService(typeof(HttpWorkerRequest));
            return service.GetType().ToString().Contains("System.Web.Hosting.IIS7WorkerRequest");
        }

        internal static bool WijmoHttpHandlerExists(HttpContext context, string path, Type  type, string applicationPath)
        {
            try
            {
                path = VirtualPathUtility.MakeRelative(VirtualPathUtility.AppendTrailingSlash(applicationPath), path);
                if (IsIIS7Request(context))
                {
                    Type _type = Type.GetType("Microsoft.Web.Administration.WebConfigurationManager, Microsoft.Web.Administration, Version=7.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35");
                    if (_type == null)
                    {
                        return true;
                    }
                    object target = _type.InvokeMember("GetSection", BindingFlags.InvokeMethod, null, null, new object[] { "system.webServer/handlers" });
                    IEnumerable enumerable = (IEnumerable)target.GetType().InvokeMember("GetCollection", BindingFlags.InvokeMethod, null, target, new object[0]);
                    foreach (object obj3 in enumerable)
                    {
                        string str = (string)obj3.GetType().InvokeMember("GetAttributeValue", BindingFlags.InvokeMethod, null, obj3, new object[] { "path" });
                        string typeName = (string)obj3.GetType().InvokeMember("GetAttributeValue", BindingFlags.InvokeMethod, null, obj3, new object[] { "type" });
						if (str == path && typeName.Contains(type.FullName))
						{
                            return true;
                        }
                      }
                    return false;
                }
                else
                {
                    HttpHandlersSection section = (HttpHandlersSection)ConfigurationManager.GetSection("system.web/httpHandlers");
                    foreach (HttpHandlerAction action in section.Handlers)
                    {
                        if (action.Path == path && action.Type.Contains(type.FullName))
                        {
                            return true;
                        }
                    }
                    return false;
                }
            }
            catch (Exception)
            {

                return false;
            }
        }             

    }
}
