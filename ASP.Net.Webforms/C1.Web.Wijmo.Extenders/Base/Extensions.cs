﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
	using System.Text.RegularExpressions;

	internal static class Extensions
	{
		public static string ToCamel(this string value)
		{
			if (!string.IsNullOrEmpty(value))
			{
				value = char.ToLowerInvariant(value[0]) + value.Substring(1);
			}

			return value;
		}

		private static Regex _re = new Regex(@"^\p{Lu}|,\s*\p{Lu}");
		public static string ToCamel(this Enum value)
		{
			return _re.Replace(value.ToString(), match => match.Value.ToLower());
		}

		public static Double? ToNullableDouble(this string value)
		{
			if (string.IsNullOrEmpty(value))
			{
				return null;
			}

			return Double.Parse(value);
		}

		public static DateTime? ToNullableDateTime(this string value)
		{
			if (string.IsNullOrEmpty(value))
			{
				return null;
			}

			return DateTime.Parse(value);
		}

		public static string SafelyToString(this object source)
		{
			return source == null
				? String.Empty
				: source.ToString();
		}

		public static T ToEnum<T>(this string source)
		{
			return String.IsNullOrEmpty(source)
				? (T)Enum.ToObject(typeof(T), 0)
				: (T)Enum.Parse(typeof(T), source, true);
		}

		private const char CssClassSeparator = ' ';
		public static void AddCss(this WebControl control, string cssClass)
		{
			control.CssClass = String.Join(CssClassSeparator.ToString(),
				control.CssClass.Split(CssClassSeparator)
				.Except(new[] {"", cssClass})
				.Concat(new[] {cssClass})
				.ToArray());
		}

		public static void RemoveCss(this WebControl control, string cssClass)
		{
			control.CssClass = String.Join(CssClassSeparator.ToString(),
				control.CssClass.Split(CssClassSeparator)
				.Except(new[] {"", cssClass})
				.ToArray());
		}

		internal static bool StringArrayEquals(string[] list1, string[] list2)
		{
			if (list1 == null || list2 == null)
			{
				return list1 == null && list2 == null;
			}

			if (list1.Count() != list2.Count())
			{
				return false;
			}

			for (int i = 0; i < list1.Count(); i++)
			{
				if (list1[i] != list2[i])
				{
					return false;
				}
			}

			return true;
		}

		public static bool IsParentEnabled(this Control control)
		{
			for (var parent = control.Parent; parent != null; parent = parent.Parent)
			{
				var webcontrol = parent as WebControl;
				if (webcontrol != null && !webcontrol.Enabled)
				{
					return false;
				}
			}

			return true;
		}
	}
}
