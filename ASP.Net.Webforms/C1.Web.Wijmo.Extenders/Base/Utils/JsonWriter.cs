using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;
using System.CodeDom.Compiler;
using System.Globalization;
using System.ComponentModel;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Diagnostics;
using System.Reflection;


#if EXTENDER
using C1.Web.Wijmo.Extenders.Converters;
namespace C1.Web.Wijmo.Extenders
#else
using C1.Web.Wijmo.Controls.Base;
using C1.Web.Wijmo.Controls.Converters;
namespace C1.Web.Wijmo.Controls	
#endif
{


	/// <summary>
	/// JSON writer.
	/// </summary>
	internal class JsonWriter
	{
		#region ** fields
		
		private static Dictionary<Type, Converter<object, string>> _customConverters;
		private bool _serializeAll = false;
		private bool _convertEnumToString = false;
		private bool _convertNamesToCamelCase = false;

		#endregion

		#region ** constructors
		
		public JsonWriter(TextWriter writer, IUrlResolutionService urlResolutionService)
		{
			this._writer = new IndentedTextWriter(writer, true);
			this._scopes = new Stack<JsonWriter.Scope>();
			this._urlResolutionService = urlResolutionService;
			this._serializeAll = false;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="JsonWriter"/> class.
		/// </summary>
		/// <param name="writer">The writer.</param>
		/// <param name="urlResolutionService">The URL resolution service.</param>
		/// <param name="serializeAll">if set to <c>true</c> writer will serialize all public  properties and public fields, JsonAttribuute will be ignored.</param>
		public JsonWriter(TextWriter writer, IUrlResolutionService urlResolutionService, bool serializeAll)
		{
			this._writer = new IndentedTextWriter(writer, true);
			this._scopes = new Stack<JsonWriter.Scope>();
			this._urlResolutionService = urlResolutionService;
			this._serializeAll = serializeAll;
		}

		static JsonWriter()
		{
			_customConverters = new Dictionary<Type, Converter<object, string>>();
			_customConverters.Add(typeof(Color), delegate(object value)
			{
				return ColorTranslator.ToHtml((Color)value);
			});
		}

		#endregion

		#region ** properties
		public bool ConvertEnumToString
		{
			get
			{
				return _convertEnumToString;
			}
			set
			{
				_convertEnumToString = value;
			}
		}

		public bool ConvertNamesToCamelCase
		{
			get
			{
				return _convertNamesToCamelCase;
			}
			set
			{
				_convertNamesToCamelCase = value;
			}
		}

		public bool PutClientEventsInQuotes
		{
			get;
			set;
		}
		
		#endregion



		public void EndScope()
		{
			if (this._scopes.Count == 0)
			{
				throw new InvalidOperationException("No active scope to end.");
			}

			//Add comments by RyanWu@20100818.
			//For fixing the issue#12167.
			#if DEBUG
			this._writer.WriteLine();
			#endif
			//end by RyanWu@20100818.

			this._writer.Indent--;
			JsonWriter.Scope scope1 = this._scopes.Pop();
			if (scope1.Type == JsonWriter.ScopeType.Array)
			{
				this._writer.Write("]");
			}
			else
			{
				this._writer.Write("}");
			}
		}

		internal static string QuoteJScriptString(string s)
		{
			if (string.IsNullOrEmpty(s))
			{
				return string.Empty;
			}
			StringBuilder builder1 = null;
			int num1 = 0;
			int num2 = 0;
			for (int num3 = 0; num3 < s.Length; num3++)
			{
				char ch1 = s[num3];
				if ((((ch1 == '\r') || (ch1 == '\t')) || ((ch1 == '"') || (ch1 == '\''))) || (((ch1 == '\\') || (ch1 == '\r')) || ((ch1 < ' ') || (ch1 > '\x007f'))))
				{
					if (builder1 == null)
					{
						builder1 = new StringBuilder(s.Length + 6);
					}
					if (num2 > 0)
					{
						builder1.Append(s, num1, num2);
					}
					num1 = num3 + 1;
					num2 = 0;
				}
				switch (ch1)
				{
					case '\'':
						builder1.Append("\u0027");
						break;
					case '\\':
						builder1.Append(@"\\");
						break;
					case '\t':
						builder1.Append(@"\t");
						break;
					case '\n':
						builder1.Append(@"\n");
						break;
					case '\r':
						builder1.Append(@"\r");
						break;
					case '"':
						builder1.Append("\\\""); //Fixed issue in JsonWriter with double quotes.
						// do not use following, because \u0022 will be decoded by javascript automatically before parsing action in client side JSON parser:
						// builder1.Append("\u0022");// \u0022 - Double Quote "
						break;
					default:
						if ((ch1 < ' ') || (ch1 > '\x007f'))
						{
							builder1.AppendFormat(CultureInfo.InvariantCulture, @"\u{0:x4}", new object[] { (int)ch1 });
						}
						else
						{
							num2++;
						}
						break;
				}
			}
			string text1 = s;
			if (builder1 == null)
			{
				return text1;
			}
			if (num2 > 0)
			{
				builder1.Append(s, num1, num2);
			}
			return builder1.ToString();
		}

		public void StartArrayScope()
		{
			this.StartScope(JsonWriter.ScopeType.Array);
		}

		public void StartObjectScope()
		{
			this.StartScope(JsonWriter.ScopeType.Object);
		}

		private void StartScope(JsonWriter.ScopeType type)
		{
			if (this._scopes.Count != 0)
			{
				JsonWriter.Scope scope1 = this._scopes.Peek();
				if ((scope1.Type == JsonWriter.ScopeType.Array) && (scope1.ObjectCount != 0))
				{
					this._writer.Write(", ");
				}
				scope1.ObjectCount++;
			}
			JsonWriter.Scope scope2 = new JsonWriter.Scope(type);
			this._scopes.Push(scope2);
			if (type == JsonWriter.ScopeType.Array)
			{
				this._writer.Write("[");
			}
			else
			{
				this._writer.Write("{");
			}
			this._writer.Indent++;

			//Add comments by RyanWu@20100818.
			//For fixing the issue#12167.
			#if DEBUG
			this._writer.WriteLine();
			#endif
			//end by RyanWu@20100818.
		}


		internal void WriteCore(string text, bool quotes)
		{
			if (this._scopes.Count != 0)
			{
				JsonWriter.Scope scope1 = this._scopes.Peek();
				if (scope1.Type == JsonWriter.ScopeType.Array)
				{
					if (scope1.ObjectCount != 0)
					{
						this._writer.Write(", ");
					}
					scope1.ObjectCount++;
				}
			}
			if (quotes)
			{
				this._writer.Write('"');
			}
			this._writer.Write(text);
			if (quotes)
			{
				this._writer.Write('"');
			}
		}

		public virtual void WriteName(string name)
		{
			WriteName(name, this._convertNamesToCamelCase);
		}

		public void WriteName(string name, bool camelize)
		{
			if (string.IsNullOrEmpty(name))
			{
				throw new ArgumentNullException("name");
			}
			if (this._scopes.Count == 0)
			{
				throw new InvalidOperationException("No active scope to write into.");
			}
			if (this._scopes.Peek().Type != JsonWriter.ScopeType.Object)
			{
				throw new InvalidOperationException("Names can only be written into Object scopes.");
			}
			JsonWriter.Scope scope1 = this._scopes.Peek();
			if (scope1.Type == JsonWriter.ScopeType.Object)
			{
				if (scope1.ObjectCount != 0)
				{
					this._writer.Write(", ");
				}
				scope1.ObjectCount++;
			}
			//20090213
			this._writer.Write("\"");
			this._writer.Write(camelize ? name.ToCamel() : name);
			this._writer.Write("\"");
			//20090213
			this._writer.Write(": ");
		}


		public void WriteValue(bool value)
		{
			this.WriteCore(value ? "true" : "false", false);
		}


		public void WriteValue(ICollection items)
		{
			if ((items == null) || (items.Count == 0))
			{
				this.WriteCore("[]", false);
			}
			else
			{
				this.StartArrayScope();
				foreach (object obj1 in items)
				{
					//IJsonEmptiable jsonEmptiable = obj1 as IJsonEmptiable;
					//if (jsonEmptiable != null && jsonEmptiable.IsEmpty)
					//{
					//    continue;
					//}

					this.WriteValue(obj1);
				}
				this.EndScope();
			}
		}

		public void WriteValue(IDictionary record)
		{
			if ((record == null) || (record.Count == 0))
			{
				this.WriteCore("{}", false);
			}
			else
			{
				this.StartObjectScope();
				foreach (DictionaryEntry entry1 in record)
				{
					string text1 = entry1.Key as string;
					if (string.IsNullOrEmpty(text1))
					{
						throw new ArgumentException("Key of unsupported type contained in Hashtable.");
					}

					IJsonEmptiable jsonEmptiable = entry1.Value as IJsonEmptiable;
					if (jsonEmptiable != null && jsonEmptiable.IsEmpty)
					{
						continue;
					}

					this.WriteName(text1);
					this.WriteValue(entry1.Value);
				}
				this.EndScope();
			}
		}


		public void WriteValue(ICustomOptionType iCustomOptionType)
		{
			this.WriteCore(iCustomOptionType.SerializeValue(), iCustomOptionType.IncludeQuotes);
		}

		public void WriteValue(DateTime dateTime)
		{
			if (dateTime == null)
			{
				throw new ArgumentOutOfRangeException("dateTime");
			}

			//"YYYY-MM-DDThh:mm:ss:mssZ"
			string jsonDateStr = FillByZero(dateTime.Year, 4) + "-" + FillByZero(dateTime.Month, 2)
				+ "-" + FillByZero(dateTime.Day, 2)
				+ "T" + FillByZero(dateTime.Hour, 2)
				+ ":" + FillByZero(dateTime.Minute, 2)
				+ ":" + FillByZero(dateTime.Second, 2)
				+ ":" + FillByZero(dateTime.Millisecond, 3)
				+ "Z";

			this.WriteCore(jsonDateStr, true);

		}

		private string FillByZero(int val, int k)
		{
			string s = val.ToString();
			while (s.Length < k)
			{
				s = "0" + s;
			}
			return s;
		}

		public void WriteValue(int value)
		{
			this.WriteCore(value.ToString("G16", CultureInfo.InvariantCulture), false);
		}

		/// <summary>
		/// Writes the enum value.
		/// </summary>
		/// <param name="value">The value.</param>
		public virtual void WriteValue(Enum value)
		{
			if (_convertEnumToString)
			{
				this.WriteCore("\"" + value.ToCamel() + "\"", false);
			}
			else
			{
				int k = (int)(object)value;
				this.WriteCore(k.ToString(CultureInfo.InvariantCulture), false);
			}
		}

		/// <summary>
		/// Writes the point value.
		/// </summary>
		/// <param name="value">The value.</param>
		public virtual void WriteValue(Point value)
		{
			this.WriteCore(string.Format("{{x: {0}, y: {1}}}", value.X, value.Y), false);
		}

		/// <summary>
		/// Writes the object value.
		/// </summary>
		/// <param name="o">The o.</param>
		public virtual void WriteValue(object o)
		{
			if (o == null || o is DBNull)
			{
				this.WriteCore("null", false);
			}
			else if(o is Guid){
				// [20111214dma] fixed stackoverflow problem with Guid values:
				this.WriteValue(((Guid)o).ToString());
			}
			else if (o is ICustomOptionType) {
				this.WriteValue((ICustomOptionType)o);
			}
			else if (o is bool)
			{
				this.WriteValue((bool)o);
			}
			else if (o is int)
			{
				this.WriteValue((int)o);
			}
			else if (o is float)
			{
				this.WriteValue((float)o);
			}
			else if (o is double)
			{
				this.WriteValue((double)o);
			}
			else if (o is DateTime)
			{
				this.WriteValue((DateTime)o);
			}
			else if (o is string)
			{
				this.WriteValue((string)o);
			}
			else if (o is char)
			{
				this.WriteValue((char)o);
			}
			else if (o is Unit)
			{
				this.WriteValue(((Unit)o).IsEmpty ? "" : ((Unit)o).ToString());
			}
			else if (o is IDictionary)
			{
				this.WriteValue((IDictionary)o);
			}
			else if (o is ICollection)
			{
				this.WriteValue((ICollection)o);
			}
			else if (o is Enum)
			{
				this.WriteValue((Enum)o);
			}
			else if (o is Color)
			{
				this.WriteValue((Color)o);
			}
			else if (o is CultureInfo)
			{
				this.WriteValue((CultureInfo)o);
			}
			else if (o is Point)
			{
				this.WriteValue((Point)o);
			}
			//else if (o is PositionSettings)
			//{
			//    this.WriteValue((PositionSettings)o);
			//}
			else if (o.GetType().IsPrimitive || o is decimal)
			{
				this.WriteCore(Convert.ToString(o, CultureInfo.InvariantCulture), false);
			}
			else
			{
				this.StartObjectScope();
				// Write public fields:
				FieldInfo[] fields = o.GetType().GetFields();
				foreach (FieldInfo descriptor1 in fields)
				{
					if (this._shouldSerialize(descriptor1, o))
					{
						object val = descriptor1.GetValue(o);
						val = this._adjustValue(val, descriptor1);

						this.WriteName(this._adjustName(descriptor1.Name, descriptor1));
						this.WriteValue(val);
					}
				}
				// Write public properties:
				PropertyDescriptorCollection collection1 = TypeDescriptor.GetProperties(o);
				foreach (PropertyDescriptor descriptor1 in collection1)
				{
					if (this._shouldSerialize(descriptor1, o))
					{
						object val = descriptor1.GetValue(o);

						//Add comments by RyanWu@20110630.
						//We should serialized the Enabled property.  At client side it should be disabled.
						if (descriptor1.Name == "Enabled" && o is Control)
						{
							this.WriteName("disabled");
							this.WriteValue(!((bool)val));
							continue;
						}
						//end by RyanWu@20110630.

						val = this._adjustValue(val, descriptor1);
						this.WriteName(this._adjustName(descriptor1.Name, descriptor1));

						//Add comments by RyanWu@20110413.
						//Fix the issue that serializing the client event name with quotes.
						WidgetEventAttribute eventAttr = (WidgetEventAttribute)this._getAttributeByType(descriptor1, typeof(WidgetEventAttribute));

						if (eventAttr != null)
						{
							this.WriteCore((string)val, PutClientEventsInQuotes);
						}
						else
						{
							//Process the attribute which type is function in client side.
							WidgetFunctionAttribute funcAttr = (WidgetFunctionAttribute)this._getAttributeByType(descriptor1, typeof(WidgetFunctionAttribute));
							if (funcAttr != null)
							{
								this.WriteCore((string)val, false);
							}
							else
							{
								this.WriteValue(val);
							}
						}
						//end by RyanWu@20110413.
					}
				}
				this.EndScope();
			}
		}

		private string _adjustName(string name, object memberDescriptor)
		{
			WidgetOptionNameAttribute optionNameAttr = (WidgetOptionNameAttribute)this._getAttributeByType(memberDescriptor, typeof(WidgetOptionNameAttribute));
			if ((optionNameAttr != null) && !string.IsNullOrEmpty(optionNameAttr.Name))
			{
				name = optionNameAttr.Name;
			}
			else
			{
				WidgetEventAttribute eventAttr = (WidgetEventAttribute)this._getAttributeByType(memberDescriptor, typeof(WidgetEventAttribute));
				if (eventAttr != null)
				{
					name = name.Replace("OnClient", "").ToCamel(); // events names are changed to camel-case format since Wijmo 1.3.0.
				}
			}
			return name;
		}

		private object _adjustValue(object value, object propertyDescriptor)
		{
			Type memberType = this._determineMemberType(propertyDescriptor);
			if (!memberType.IsPrimitive)
			{				
				Converter<object, string> customConverter = null;
				if (!_customConverters.TryGetValue(memberType, out customConverter))
				{
					foreach (KeyValuePair<Type, Converter<object, string>> pair in _customConverters)
					{
						if (memberType.IsSubclassOf(pair.Key))
						{
							customConverter = pair.Value;
							break;
						}
					}
				}
				if (customConverter != null)
				{
					value = customConverter(value);
				}
			}

			if (this._urlResolutionService != null && value != null)
			{
				EditorAttribute attrEditor = (EditorAttribute)this._getAttributeByType(propertyDescriptor, typeof(EditorAttribute));
				UrlPropertyAttribute urlProperty = (UrlPropertyAttribute)this._getAttributeByType(propertyDescriptor, typeof(UrlPropertyAttribute)); 
				if (attrEditor != null && attrEditor.EditorTypeName.IndexOf("ImageUrlEditor") != -1
					|| urlProperty != null)
				{
					value = _urlResolutionService.ResolveClientUrl(value.ToString());
				}
			}
			return value;
		}

		private object _getAttributeByType(object memberDescriptor, Type type)
		{
			PropertyDescriptor propertyDescriptor = memberDescriptor as PropertyDescriptor;
			if (propertyDescriptor != null)
			{
				return propertyDescriptor.Attributes[type];
			}
			FieldInfo fieldInfo = memberDescriptor as FieldInfo;
			if (fieldInfo != null)
			{
				object[] attrs = fieldInfo.GetCustomAttributes(true);
				foreach (object attr in attrs)
				{
					if (attr != null && type.IsInstanceOfType(attr))
					{
						return attr;
					}
				}
			}
			return null;
		}

		private Type _determineMemberType(object memberDescriptor)
		{
			if (memberDescriptor is PropertyDescriptor)
			{
				return ((PropertyDescriptor)memberDescriptor).PropertyType;
			}
			else if (memberDescriptor is FieldInfo)
			{
				return ((FieldInfo)memberDescriptor).FieldType;
			}
			else if (memberDescriptor is EventDescriptor)
			{
				return ((EventDescriptor)memberDescriptor).EventType;
			}
			
			return memberDescriptor.GetType();
		}

		private bool _shouldSerialize(object descriptor1, object member)
		{
			if (this._serializeAll)
			{
				return true;
			}
			JsonAttribute jsonAttribute = (JsonAttribute)this._getAttributeByType(descriptor1, typeof(JsonAttribute));
			WidgetOptionAttribute optionAttr = (WidgetOptionAttribute)this._getAttributeByType(descriptor1, typeof(WidgetOptionAttribute));
			WidgetEventAttribute eventAttr = (WidgetEventAttribute)this._getAttributeByType(descriptor1, typeof(WidgetEventAttribute));
			if ((jsonAttribute == null || !jsonAttribute.IsSerializableToJson) && optionAttr == null && eventAttr == null)
			{
				return false;
			}
			/*
			if ((eventAttr != null) && (this._determineMemberType(member) != typeof(string)))
			{
				throw new InvalidOperationException("WidgetEventAttribute can only be applied to a property/field with a type of System.String.");
			}			
			*/
			if (jsonAttribute != null)
			{
				if (jsonAttribute.IsSerializableToJson && !jsonAttribute.IsSkipIfEmpty)
				{
					return true;
				}

				object val = this._getMemberValue(descriptor1, member);			
				IJsonEmptiable jsonEmptiable = val as IJsonEmptiable;
				if (jsonAttribute.IsSkipIfEmpty && ((val == null || CompareObjectWithEmptyValue(val, jsonAttribute.EmptyValue) == true) ||
					(jsonEmptiable != null && jsonEmptiable.IsEmpty)))
				{
					return false;
				}
			}

			//Add comments by RyanWu@20110402.
			//In order to not serialize the default value to the string.
			if (optionAttr != null || eventAttr != null)
			{
				DefaultValueAttribute defValueAttr = (DefaultValueAttribute)this._getAttributeByType(descriptor1, typeof(DefaultValueAttribute));
				object val = this._getMemberValue(descriptor1, member);

				if (defValueAttr != null)
				{
					if (val == null)
					{
						return defValueAttr.Value != null;
					}

					return !val.Equals(defValueAttr.Value);
				}

				IJsonEmptiable jsonEmptiable = val as IJsonEmptiable;

				// If the class has 'ShouldSerialize' method, use the ShouldSerialize method to indicate 
				// whether serialize this property. Else, use the IJsonEmptiable's IsEmpty to indicate whether serialize.
				if ((descriptor1 is PropertyDescriptor)) {
					PropertyDescriptor pd = descriptor1 as PropertyDescriptor;
					string propertyName = pd.Name;
					if (member.GetType().GetMethod("ShouldSerialize" + propertyName) != null)
					{
						if ((descriptor1 is PropertyDescriptor) &&
						!(descriptor1 as PropertyDescriptor).ShouldSerializeValue(member))
						{
							return false;
						}
					}
					else 
					{
						if (jsonEmptiable != null && jsonEmptiable.IsEmpty)
						{
							return false;
						}
					}
				}

				// fixed serialize bug, when control has "ShouldSerialize" method.
				// use "ShouldSerialize" method to determine the serializition
				// or use "jsonEmptiable" method                
				//if((descriptor1 is PropertyDescriptor) &&
				//    !(descriptor1 as PropertyDescriptor).ShouldSerializeValue(member))
				//{
				//    return false;
				//}

				//if (jsonEmptiable != null && jsonEmptiable.IsEmpty)
				//{
				//    // if control doesn't have "ShouldSerialize" method, 
				//    // the ShouldSerializeValue method will return true,
				//    // and if "jsonEmptiable" default value is not the "ShouldSerialize" default value 
				//    // the property should be serialized.

				//    if ((descriptor1 is PropertyDescriptor) &&
				//        member.GetType().GetMethod("ShouldSerialize" + (descriptor1 as PropertyDescriptor).Name) != null)
				//    {
				//        return true;
				//    }
				//    //end
				//    return false;
				//}
				// end
			}
			//end by RyanWu@20110402.

			return true;
		}

		private object _getMemberValue(object memberDescriptor, object member)
		{
			if (memberDescriptor is PropertyDescriptor)
			{
				return ((PropertyDescriptor)memberDescriptor).GetValue(member);
			}
			else if (memberDescriptor is FieldInfo)
			{
				return ((FieldInfo)memberDescriptor).GetValue(member);
			}
			return null;			
		}

		private bool CompareObjectWithEmptyValue(object valObj, object emptyValObj)
		{

			if (valObj is bool && emptyValObj is bool)
				return ((bool)valObj) == ((bool)emptyValObj);
			if (valObj is int && emptyValObj is int)
				return ((int)valObj) == ((int)emptyValObj);
			if (valObj is float && emptyValObj is float)
				return ((float)valObj) == ((float)emptyValObj);
			if (valObj is DateTime && emptyValObj is DateTime)
				return ((DateTime)valObj) == ((DateTime)emptyValObj);
			if (valObj is string && emptyValObj is string)
				return ((string)valObj) == ((string)emptyValObj);
			if (valObj is Enum && emptyValObj is Enum)
			{
				int int1 = (int)valObj;
				int int2 = (int)emptyValObj;
				return int1 == int2;
				//return ((Enum)obj1) == ((Enum)obj2);
			}
			if (valObj is Color)
			{
				if (emptyValObj is Color)
				{
					return ((Color)valObj).Equals((Color)emptyValObj);
				}
				else
				{
					string sColor = emptyValObj as string;
					if (sColor != null)
					{
						Color color = C1WebColorConverter.ToColor(sColor);
						if (color != null)
							return ((Color)valObj).Equals(color);
						else
							return ((Color)valObj).IsEmpty;
					}
					else
					{
						return ((Color)valObj).IsEmpty;
					}
				}
			}
			if (valObj is Unit && emptyValObj is string)
			{
				string s = (string)emptyValObj;
				if (s == "" && ((Unit)valObj).IsEmpty)
					return true;
				Unit resultEmptyUnit = Unit.Parse(s);
				return resultEmptyUnit.Equals((Unit)valObj);
			}
			return valObj == emptyValObj;


		}

		public void WriteValue(float value)
		{
			this.WriteCore(value.ToString("G16", CultureInfo.InvariantCulture), false);
		}

		public void WriteValue(double value)
		{
			this.WriteCore(value.ToString("G16", CultureInfo.InvariantCulture), false);
		}

		public void WriteValue(char ch)
		{
			this.WriteCore(JsonWriter.QuoteJScriptString(ch.ToString()), true);
		}

		public void WriteValue(string s)
		{
			if (s == null)
			{
				this.WriteCore("null", false);
			}
			else
			{
				this.WriteCore(JsonWriter.QuoteJScriptString(s), true);
			}
		}

		public void WriteValue(Color c)
		{
			this.WriteCore(JsonWriter.QuoteJScriptString(C1WebColorConverter.ToStringHexColor(c)), true);
		}

		public void WriteValue(CultureInfo c)
		{
			this.WriteCore(c.Name, true);
		}

		//// Serialize the positionSettings object. Edit by dail 2011-1-21
		//public void WriteValue(PositionSettings p)
		//{
		//    this.WriteCore(QuoteJScriptString(PositionSettingsConverter.ToString(p)), false);
		//}

		// Fields
		private Stack<Scope> _scopes;
		private IndentedTextWriter _writer;
		private IUrlResolutionService _urlResolutionService;
		//internal static readonly DateTime MinDate;

		// Nested Types
		private sealed class Scope
		{
			public Scope(JsonWriter.ScopeType type)
			{
				this._type = type;
			}

			// Properties
			public int ObjectCount
			{
				get
				{
					return this._objectCount;
				}
				set
				{
					this._objectCount = value;
				}
			}

			public JsonWriter.ScopeType Type
			{
				get
				{
					return this._type;
				}
			}



			// Fields
			private int _objectCount;
			private JsonWriter.ScopeType _type;
		}


		private enum ScopeType
		{
			Array,
			Object
		}
	}

	internal sealed class IndentedTextWriter : TextWriter
	{
		// Fields
		private int _indentLevel;
		private bool _minimize;
		private bool _tabsPending;
		private string _tabString;
		private TextWriter _writer;

		// Methods
		public IndentedTextWriter(TextWriter writer, bool minimize)
			: base(CultureInfo.InvariantCulture)
		{
			this._writer = writer;
			this._minimize = minimize;
			if (this._minimize)
			{
				this.NewLine = "\r";//"";// 
			}
			this._tabString = "    ";
			this._indentLevel = 0;
			this._tabsPending = false;
		}

		public override void Close()
		{
			this._writer.Close();
		}

		public override void Flush()
		{
			this._writer.Flush();
		}

		private void OutputTabs()
		{
			if (this._tabsPending)
			{
				if (!this._minimize)
				{
					for (int i = 0; i < this._indentLevel; i++)
					{
						this._writer.Write(this._tabString);
					}
				}
				this._tabsPending = false;
			}
		}

		public override void Write(bool value)
		{
			this.OutputTabs();
			this._writer.Write(value);
		}

		public override void Write(char value)
		{
			this.OutputTabs();
			this._writer.Write(value);
		}

		public override void Write(string s)
		{
			this.OutputTabs();
			this._writer.Write(s);
		}

		public override void Write(char[] buffer)
		{
			this.OutputTabs();
			this._writer.Write(buffer);
		}

		public override void Write(double value)
		{
			this.OutputTabs();
			this._writer.Write(value);
		}

		public override void Write(int value)
		{
			this.OutputTabs();
			this._writer.Write(value);
		}

		public override void Write(long value)
		{
			this.OutputTabs();
			this._writer.Write(value);
		}

		public override void Write(object value)
		{
			this.OutputTabs();
			this._writer.Write(value);
		}

		public override void Write(float value)
		{
			this.OutputTabs();
			this._writer.Write(value);
		}

		public override void Write(string format, params object[] arg)
		{
			this.OutputTabs();
			this._writer.Write(format, arg);
		}

		public override void Write(string format, object arg0)
		{
			this.OutputTabs();
			this._writer.Write(format, arg0);
		}

		public override void Write(char[] buffer, int index, int count)
		{
			this.OutputTabs();
			this._writer.Write(buffer, index, count);
		}

		public override void Write(string format, object arg0, object arg1)
		{
			this.OutputTabs();
			this._writer.Write(format, arg0, arg1);
		}

		public override void WriteLine()
		{
			this.OutputTabs();
			this._writer.WriteLine();
			this._tabsPending = true;
		}

		public override void WriteLine(bool value)
		{
			this.OutputTabs();
			this._writer.WriteLine(value);
			this._tabsPending = true;
		}

		public override void WriteLine(char value)
		{
			this.OutputTabs();
			this._writer.WriteLine(value);
			this._tabsPending = true;
		}

		public override void WriteLine(double value)
		{
			this.OutputTabs();
			this._writer.WriteLine(value);
			this._tabsPending = true;
		}

		public override void WriteLine(int value)
		{
			this.OutputTabs();
			this._writer.WriteLine(value);
			this._tabsPending = true;
		}

		public override void WriteLine(char[] buffer)
		{
			this.OutputTabs();
			this._writer.WriteLine(buffer);
			this._tabsPending = true;
		}

		public override void WriteLine(long value)
		{
			this.OutputTabs();
			this._writer.WriteLine(value);
			this._tabsPending = true;
		}

		public override void WriteLine(object value)
		{
			this.OutputTabs();
			this._writer.WriteLine(value);
			this._tabsPending = true;
		}

		public override void WriteLine(float value)
		{
			this.OutputTabs();
			this._writer.WriteLine(value);
			this._tabsPending = true;
		}

		public override void WriteLine(string s)
		{
			this.OutputTabs();
			this._writer.WriteLine(s);
			this._tabsPending = true;
		}

		public override void WriteLine(uint value)
		{
			this.OutputTabs();
			this._writer.WriteLine(value);
			this._tabsPending = true;
		}

		public override void WriteLine(string format, params object[] arg)
		{
			this.OutputTabs();
			this._writer.WriteLine(format, arg);
			this._tabsPending = true;
		}

		public override void WriteLine(string format, object arg0)
		{
			this.OutputTabs();
			this._writer.WriteLine(format, arg0);
			this._tabsPending = true;
		}

		public override void WriteLine(string format, object arg0, object arg1)
		{
			this.OutputTabs();
			this._writer.WriteLine(format, arg0, arg1);
			this._tabsPending = true;
		}

		public override void WriteLine(char[] buffer, int index, int count)
		{
			this.OutputTabs();
			this._writer.WriteLine(buffer, index, count);
			this._tabsPending = true;
		}

		public void WriteLineNoTabs(string s)
		{
			this._writer.WriteLine(s);
		}

		public void WriteNewLine()
		{
			if (!this._minimize)
			{
				this.WriteLine();
			}
		}

		public void WriteSignificantNewLine()
		{
			this.WriteLine();
		}

		public void WriteTrimmed(string text)
		{
			if (!this._minimize)
			{
				this.Write(text);
			}
			else
			{
				this.Write(text.Trim());
			}
		}

		// Properties
		public override Encoding Encoding
		{
			get
			{
				return this._writer.Encoding;
			}
		}

		public int Indent
		{
			get
			{
				return this._indentLevel;
			}
			set
			{
				if (value < 0)
				{
					value = 0;
				}
				this._indentLevel = value;
			}
		}

		public override string NewLine
		{
			get
			{
				return this._writer.NewLine;
			}
			set
			{
				this._writer.NewLine = value;
			}
		}
	}

	/// <summary>
	/// Attribute used to specifying whether Json helper need 
	/// to serialize member what owns this attribute.
	/// </summary>
	public class JsonAttribute : Attribute
	{
		private bool _isSerializableToJson;
		private bool _skipIfEmpty;
		private object _emptyValue;

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="isSerializableToJson"></param>
		/// <param name="skipIfEmpty"></param>
		/// <param name="emptyValue"></param>
		public JsonAttribute(bool isSerializableToJson, bool skipIfEmpty, object emptyValue)
			: base()
		{
			this._isSerializableToJson = isSerializableToJson;
			this._skipIfEmpty = skipIfEmpty;
			this._emptyValue = emptyValue;
		}

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="isSerializableToJson"></param>
		public JsonAttribute(bool isSerializableToJson)
			: this(isSerializableToJson, false, null)
		{
		}

		/// <summary>
		/// Is Serializable To Json
		/// </summary>
		public bool IsSerializableToJson
		{
			get
			{
				return this._isSerializableToJson;
			}
		}

		/// <summary>
		/// Skip property value is Empty or null.
		/// </summary>
		public bool IsSkipIfEmpty
		{
			get
			{
				return this._skipIfEmpty;
			}
		}

		/// <summary>
		/// Empty Value.
		/// </summary>
		public object EmptyValue
		{
			get
			{
				return this._emptyValue;
			}
		}
	}

	/// <summary>
	/// Interface used by serialization to check if an object should be serialized.
	/// </summary>
	public interface IJsonEmptiable
	{
		/// <summary>
		/// Gets value that indicates if the object is empty or not.
		/// If object is empty than it will be skipped during the serialization process.
		/// </summary>
		bool IsEmpty { get; }
	}

}
