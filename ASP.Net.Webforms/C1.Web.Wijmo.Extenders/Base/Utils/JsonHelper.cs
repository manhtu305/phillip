using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Design;
using System.Globalization;
using System.Collections;
using System.Web.UI;
using System.IO;
using System.Text.RegularExpressions;

#if EXTENDER 
namespace C1.Web.Wijmo.Extenders
#else
using C1.Web.Wijmo.Controls.Base;
namespace C1.Web.Wijmo.Controls	
#endif
{

	/// <summary>
	/// JsonHelper class. Contains static methods for working with JSON strings.
	/// </summary>
	public sealed class JsonHelper
	{

		/// <summary>
		/// Converts object into JSON string.
		/// </summary>
		/// <param name="obj">Required. Object to convert into JSON string</param>
		/// <param name="urlResolutionService">Optional. URL Resolution Service</param>
		/// <returns>Javascript object notation string</returns>
		public static string ObjectToString(object obj, IUrlResolutionService urlResolutionService)
		{
			StringWriter strwr = new StringWriter();
			JsonWriter wr = new JsonWriter(strwr, urlResolutionService);
			wr.WriteValue(obj);
			StringBuilder b = strwr.GetStringBuilder();
			return b.ToString();
		}

		/// <summary>
		/// Converts object into JSON string. 
		/// Difference from ObjectToString method:
		/// 1) All field/property names will be converted to camelCase.
		/// 2) All enumerations will be converted to string value.
		/// </summary>
		/// <param name="obj">Required. Object to convert into JSON string</param>
		/// <param name="urlResolutionService">Optional. URL Resolution Service</param>
		/// <returns>Javascript object notation string</returns>
		public static string WidgetToString(object obj, IUrlResolutionService urlResolutionService)
		{
			StringWriter strwr = new StringWriter();
			JsonWriter wr = new JsonWriter(strwr, urlResolutionService);
			wr.ConvertEnumToString = true;
			wr.ConvertNamesToCamelCase = true;
			wr.WriteValue(obj);
			StringBuilder b = strwr.GetStringBuilder();
			return b.ToString();
		}

		/// <summary>
		/// Converts object into JSON string. 
		/// Difference from WidgetToString method:
		/// 1) All client events will be quoted.
		/// </summary>
		/// <param name="obj">Required. Object to convert into JSON string</param>
		/// <param name="urlResolutionService">Optional. URL Resolution Service</param>
		/// <returns>Javascript object notation string</returns>
		public static string WidgetToStringQuoteClientEvents(object obj, IUrlResolutionService urlResolutionService)
		{
			using (StringWriter stringWriter = new StringWriter())
			{
				JsonWriter jsonWriter = new JsonWriter(stringWriter, urlResolutionService)
				{
					ConvertEnumToString = true,
					ConvertNamesToCamelCase = true,
					PutClientEventsInQuotes = true
				};

				jsonWriter.WriteValue(obj);

				return stringWriter.ToString();
			}
		}

		/// <summary>
		/// Converts object into JSON string.
		/// </summary>
		/// <param name="obj">Required. Object to convert into JSON string</param>
		/// <param name="urlResolutionService">Required. URL Resolution Service</param>
		/// <param name="serializeAll">if set to <c>true</c> writer will serialize all public  properties and public fields, JsonAttribuute will be ignored.</param>
		/// <returns>Javascript object notation string</returns>
		public static string ObjectToString(object obj, IUrlResolutionService urlResolutionService, bool serializeAll)
		{
			StringWriter strwr = new StringWriter();
			JsonWriter wr = new JsonWriter(strwr, urlResolutionService, serializeAll);
			wr.WriteValue(obj);
			StringBuilder b = strwr.GetStringBuilder();
			return b.ToString();
		}

		/// <summary>
		/// Converts JSON string into object.
		/// </summary>
		/// <param name="s">Javascript object notation string.</param>
		/// <returns></returns>
		public static Hashtable StringToObject(string s)
		{
			if (string.IsNullOrEmpty(s)) return null;
			s = ConvertFromHexUnicode(s);
			TextReader reader = new StringReader((string)s);
			object obj = new JsonReader(reader).ReadValue();
			Hashtable hashtable = obj as Hashtable;
			return hashtable;

		}

		internal static string ConvertFromHexUnicode(string result)
		{
			Regex regex = new Regex(@"\\[uU]([0-9A-F]{4})", RegexOptions.IgnoreCase);
			result = regex.Replace(result, match => ((char)int.Parse(match.Groups[1].Value, NumberStyles.HexNumber)).ToString());
			return result;
		}

	}
}
