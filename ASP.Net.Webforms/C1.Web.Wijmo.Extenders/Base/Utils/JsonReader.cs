using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
using C1.Web.Wijmo.Controls.Base;
namespace C1.Web.Wijmo.Controls	
#endif
{

    /// <summary>
    /// Json Reader.
    /// </summary>
    internal sealed class JsonReader
    {
        public JsonReader(TextReader reader)
        {
            this._reader = reader;
        }
        private char GetNextSignificantCharacter()
        {
            char ch1 = (char)((ushort)this._reader.Read());
            while ((ch1 != '\0') && char.IsWhiteSpace(ch1))
            {
                ch1 = (char)((ushort)this._reader.Read());
            }
            return ch1;
        }

        private char PeekNextSignificantCharacter()
        {
            char ch1 = (char)((ushort)this._reader.Peek());
            while ((ch1 != '\0') && char.IsWhiteSpace(ch1))
            {
                this._reader.Read();
                ch1 = (char)((ushort)this._reader.Peek());
            }
            return ch1;
        }

        private ArrayList ReadArray()
        {
            ArrayList list1 = new ArrayList();
            this._reader.Read();
            while (true)
            {
                char ch1 = this.PeekNextSignificantCharacter();
                switch (ch1)
                {
                    case '\0':
                        throw new FormatException("Unterminated array literal.");

                    case ']':
                        this._reader.Read();
                        return list1;
                }
                if (list1.Count != 0)
                {
                    if (ch1 != ',')
                    {
                        throw new FormatException("Invalid array literal.");
                    }
                    this._reader.Read();
                }
                object obj1 = this.ReadValue();
                list1.Add(obj1);
            }
        }

        private bool ReadBoolean()
        {
            string text1 = this.ReadName(false);
            if (text1 != null)
            {
                if (text1.Equals("true", StringComparison.Ordinal))
                {
                    return true;
                }
                if (text1.Equals("false", StringComparison.Ordinal))
                {
                    return false;
                }
            }
            throw new FormatException("Invalid boolean literal.");
        }


        private string ReadName(bool allowQuotes)
        {
            switch (this.PeekNextSignificantCharacter())
            {
                case '"':
                case '\'':
                    if (!allowQuotes)
                    {
                        return null;
                    }
                    return this.ReadString();
            }
            StringBuilder builder1 = new StringBuilder();
            while (true)
            {
                char ch1 = (char)((ushort)this._reader.Peek());
                if ((ch1 != '_') && !char.IsLetterOrDigit(ch1))
                {
                    return builder1.ToString();
                }
                this._reader.Read();
                builder1.Append(ch1);
            }
        }

        private void ReadNull()
        {
            string text1 = this.ReadName(false);
            if ((text1 == null) || !text1.Equals("null", StringComparison.Ordinal))
            {
                throw new FormatException("Invalid null literal.");
            }
        }


        private object ReadNumber()
        {
            char ch1 = (char)((ushort)this._reader.Read());
            StringBuilder builder1 = new StringBuilder();
            bool isDouble = ch1 == '.';
            builder1.Append(ch1);
            while (true)
            {
                ch1 = this.PeekNextSignificantCharacter();
                if (!char.IsDigit(ch1) && (ch1 != '.') /*added by YanKun@20100903 to parse scientific format double number*/ && ch1 != 'e' && ch1 != 'E' && ch1 != '+' && ch1 != '-' /*End YanKun@20100903*/)
                {
                    break;
                }
                isDouble = (isDouble || ch1 == '.');
                this._reader.Read();
                builder1.Append(ch1);
            }
            string text1 = builder1.ToString();
            if (!isDouble)
            {
                int num1;
                if (int.TryParse(text1, out num1))
                {
                    return num1;
                }

                isDouble = true;
            }

            if (isDouble)
            {
                double d;
                if (double.TryParse(text1, NumberStyles.Float | NumberStyles.AllowThousands, System.Globalization.CultureInfo.InvariantCulture, out d))
                {
                    return d;
                }
                d = double.Parse(text1, System.Globalization.CultureInfo.InvariantCulture);
                return d;
            }

            throw new FormatException("Invalid numeric literal.");
        }

        private Hashtable ReadObject()
        {
            Hashtable hashtable1 = new Hashtable();
            this._reader.Read();
            while (true)
            {
                char ch1 = this.PeekNextSignificantCharacter();
                switch (ch1)
                {
                    case '\0':
                        throw new FormatException("Unterminated object literal.");

                    case '}':
                        this._reader.Read();
                        return hashtable1;
                }
                if (hashtable1.Count != 0)
                {
                    if (ch1 != ',')
                    {
                        throw new FormatException("Invalid object literal.");
                    }
                    this._reader.Read();
                }
                string text1 = this.ReadName(true);
                ch1 = this.PeekNextSignificantCharacter();
                if (ch1 != ':')
                {
                    throw new FormatException("Unexpected name/value pair syntax in object literal.");
                }
                this._reader.Read();
                object obj1 = this.ReadValue();
                hashtable1[text1] = obj1;
            }
        }


        private string ReadString()
        {
            char ch2;
            StringBuilder builder1 = new StringBuilder();
            char ch1 = (char)((ushort)this._reader.Read());
            bool escapeFound = false;
        Label_NextCharacter:
            //ch2 = this.GetNextSignificantCharacter();
            ch2 = (char)((ushort)this._reader.Read());
            if (ch2 == '\0')
            {
                throw new FormatException("Unterminated string literal.");
            }
            if (escapeFound)
            {
                // read \t, \n, \r as special character:
                switch (ch2)
                {
                    case 't':
                        builder1.Append('\t');
                        break;
                    case 'n':
                        builder1.Append('\n');
                        break;
                    case 'r':
                        builder1.Append('\r');
                        break;
                    default:
                        // by default skip escape and read next charcter:
                        builder1.Append(ch2);
                        break;
                }
                escapeFound = false;
                goto Label_NextCharacter;
            }
            if (ch2 == '\\')
            {
                escapeFound = true;
                goto Label_NextCharacter;
            }
            if (ch2 == ch1)
            {
                return builder1.ToString();
            }
            builder1.Append(ch2);
            goto Label_NextCharacter;
        }

        public object ReadValue()
        {
            object obj1 = null;
            bool flag1 = false;
            char ch1 = this.PeekNextSignificantCharacter();
            switch (ch1)
            {
                case '[':
                    obj1 = this.ReadArray();
                    break;

                case '{':
                    obj1 = this.ReadObject();
                    break;

                case '\'':
                case '"':
                    {
                        string text1 = this.ReadString();
                        if (text1 == "@")
                        {
                            // Delta(ANSCH000091) fix:
                            obj1 = text1;
                        }

                        try
                        {
                            //C1 extended ANSI DateTime format(with milliseconds):
                            Regex RegexObj = new Regex("^(\\d{4})-(\\d{2})-(\\d{2})T(\\d{2}):(\\d{2}):(\\d{2}(?:\\.\\d*)?)(:|\\.)(\\d{3})Z$");
                            bool FoundMatch = RegexObj.IsMatch(text1);
                            if (FoundMatch)
                            {
                                GroupCollection mathedGroups = RegexObj.Match(text1).Groups;
                                obj1 = new DateTime(
                                    int.Parse(mathedGroups[1].Value),
                                    int.Parse(mathedGroups[2].Value),
                                    int.Parse(mathedGroups[3].Value),
                                    int.Parse(mathedGroups[4].Value),
                                    int.Parse(mathedGroups[5].Value),
                                    int.Parse(mathedGroups[6].Value),
                                    int.Parse(mathedGroups[8].Value)
                                    );
                            }
                            else
                            {
                                // ANSI DateTime format(without milliseconds):
                                // (IE8 format)
                                RegexObj = new Regex("^(\\d{4})-(\\d{2})-(\\d{2})T(\\d{2}):(\\d{2}):(\\d{2}(?:\\.\\d*)?)Z$");
                                FoundMatch = RegexObj.IsMatch(text1);
                                if (FoundMatch)
                                {
                                    // Actually we redefine toJSON method for client side, so we must never goes here
                                    GroupCollection mathedGroups = RegexObj.Match(text1).Groups;
                                    obj1 = new DateTime(
                                        int.Parse(mathedGroups[1].Value),
                                        int.Parse(mathedGroups[2].Value),
                                        int.Parse(mathedGroups[3].Value),
                                        int.Parse(mathedGroups[4].Value),
                                        int.Parse(mathedGroups[5].Value),
                                        int.Parse(mathedGroups[6].Value)
                                        );
                                    // Note, IE8 uses UTC offset
                                    obj1 = ((DateTime)obj1).ToLocalTime();
                                }
                            }
                        }
                        catch (ArgumentException)
                        {
                            // Syntax error in the regular expression
                        }

                        if (obj1 == null)
                        {
                            obj1 = text1;
                        }
                        break;
                    }
                default:
                    if ((char.IsDigit(ch1) || (ch1 == '-')) || (ch1 == '.'))
                    {
                        obj1 = this.ReadNumber();
                    }
                    else if ((ch1 == 't') || (ch1 == 'f'))
                    {
                        obj1 = this.ReadBoolean();
                    }
                    else if (ch1 == 'n')
                    {
                        this.ReadNull();
                        flag1 = true;
                    }
                    break;
            }
            if ((obj1 == null) && !flag1)
            {
                throw new FormatException("Invalid JSON text.");
            }
            return obj1;
        }




        // Fields
        private TextReader _reader;


    }


}
