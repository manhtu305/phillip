﻿using System;


namespace C1.Web.Wijmo.Extenders
{
    using System.ComponentModel;

    /// <summary>
    /// Represents value of an object that need to serialize manually.
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class ObjectValue : ICustomOptionType, IJsonEmptiable
    {
        #region Properties

        /// <summary>
        /// The string representation of the value
        /// </summary>
        [DefaultValue(null)]
        [NotifyParentProperty(true)]
        public string ValueString
        {
            get;
            set;
        }

        /// <summary>
        /// The value used in serialization to determine whether the quotes is needed or not
        /// </summary>
        [DefaultValue(false)]
        [NotifyParentProperty(true)]
        public bool NeedQuotes
        {
            get;
            set;
        }

        #endregion

        #region ICustomOptionType Members

        string ICustomOptionType.SerializeValue()
        {
            return this.ValueString;
        }

        bool ICustomOptionType.IncludeQuotes
        {
            get
            {
                return this.NeedQuotes;
            }
        }

        #endregion

        #region Methods

        internal bool ShouldSerialize()
        {
            return !string.IsNullOrEmpty(this.ValueString);
        }

        internal void Reset()
        {
            this.ValueString = null;
            this.NeedQuotes = false;
        }

        #endregion

        #region IJsonEmptiable Members

        bool IJsonEmptiable.IsEmpty
        {
            get { return !ShouldSerialize(); }
        }

        #endregion
    }
}
