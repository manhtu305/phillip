﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
	/// <summary>
	/// Infrastructure.
	/// Indicates a class which can have WidgetConditionalDependencyAttribute or WidgetDependenciesAttribute attribute specified.
	/// This is a marker interface only.
	/// </summary>
	public interface IWijmoWidgetDependenciesSupport
	{
	}

	/// <summary>
	/// Proxy class that assists WidgetDependenciesResolver to resolve conditional dependencies.
	/// Any class that wants to specify WidgetConditionalDependency attribute should be derived from it.
	/// No dependencies here, derived classes must have default constructor only.
	/// </summary>
	internal abstract class ConditionalDependencyProxy : IWijmoWidgetDependenciesSupport
	{
		/// <summary>
		/// Resolves condtional dependency.
		/// </summary>
		/// <param name="instance"></param>
		/// <param name="dependency"></param>
		/// <returns>True of dependency should be registered otherwise False.</returns>
		/// <remarks>
		/// At the moment the dependency argument is redundant because only one WidgetConditionalDependencyAttribute can be specified for a single proxy class.
		/// </remarks>
		protected internal abstract bool Resolve(IWijmoWidgetSupport instance, object dependency);
	}

    /// <summary>
    /// Base class represents wijmo widget. 
    /// Major purpose is to hold js or css dependencies.
    /// </summary>
    [WidgetDependencies(
		ResourcesConst.UTIL,
		ResourcesConst.WIJMO_OPEN_JS,
        ResourcesConst.TOUCH_UTIL,
        ResourcesConst.BASE_WIDGET
    )]
	internal class WijmoBase : IWijmoWidgetDependenciesSupport
    {
    }

    //This control is only used in mobile device and it should register all necessary js files.
    //So we use the special way to write its dependencies(not inherits from WijmoBase).
    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_PRO_CSS,
        ResourcesConst.WIJMO_MOBILE_CSS,
        ResourcesConst.WIJMO_PRO_JS
    )]
    internal class WijAppView : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
		"wijmo.jquery.wijmo.wijaccordion.js")]
    internal class WijAccordion : WijmoBase
    {
    }


    internal class WijBarCode : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_PRO_CSS,
        ResourcesConst.WIJMO_PRO_JS,
        typeof(WijPager),
        typeof(WijSlider),
		ResourcesConst.BGIFRAME,
		"wijmo.jquery.wijmo.wijcarousel.js")]
    internal class WijCarousel : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_PRO_JS,
        typeof(WijToolTip),
        ResourcesConst.GLOBALIZE,
		ResourcesConst.CULTURE,
        ResourcesConst.RAPHAEL,
		ResourcesConst.WIJMO_RAPHAEL,
        "wijmo.jquery.wijmo.wijchartcore.js",
		"wijmo.exportUtil.js",
		"wijmo.chartexport.js"
	)]
    internal class WijChartCore : WijmoBase
    {
    }

    [WidgetDependencies("wijmo.jquery.wijmo.wijbarchart.js")]
    internal class WijBarChart : WijChartCore
    {
    }

	[WidgetDependencies("wijmo.jquery.wijmo.wijbubblechart.js")]
    internal class WijBubbleChart : WijChartCore
    {
    }

	[WidgetDependencies("wijmo.jquery.wijmo.wijcandlestickchart.js")]
    internal class WijCandlestickChart : WijChartCore
    {
    }

	[WidgetDependencies("wijmo.jquery.wijmo.wijlinechart.js")]
    internal class WijLineChart : WijChartCore
    {
    }

	[WidgetDependencies("wijmo.jquery.wijmo.wijpiechart.js")]
    internal class WijPieChart : WijChartCore
    {
    }

	[WidgetDependencies("wijmo.jquery.wijmo.wijscatterchart.js")]
    internal class WijScatterChart : WijChartCore
    {
    }

    [WidgetDependencies(
        typeof(WijBarChart),
        typeof(WijBubbleChart),
        typeof(WijCandlestickChart),
        typeof(WijLineChart),
        typeof(WijPieChart),
        typeof(WijScatterChart),
		"wijmo.jquery.wijmo.wijcompositechart.js")]
    internal class WijCompositeChart : WijChartCore
    {
    }

    [WidgetDependencies(typeof(WijSlider),
        typeof(WijLineChart),
    ResourcesConst.WIJMO_PRO_CSS,
    "wijmo.jquery.wijmo.wijchartnavigator.js")]
    internal class WijChartNavigator : WijChartCore
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        typeof(WijPopup),
		ResourcesConst.GLOBALIZE,
		ResourcesConst.CULTURE,
		"wijmo.jquery.wijmo.wijcalendar.js")]
    internal class WijCalendar : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_PRO_CSS,
        ResourcesConst.WIJMO_PRO_JS,
        typeof(WijList),
		"wijmo.jquery.wijmo.wijcombobox.js"
    )]
    internal class WijCombobox : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.BGIFRAME,
		"wijmo.jquery.wijmo.wijdialog.js")]
    internal class WijDialog : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_PRO_CSS,
        ResourcesConst.WIJMO_PRO_JS,
        typeof(WijSplitter),
        typeof(WijDialog),
        typeof(WijRibbon),
       ResourcesConst.GLOBALIZE,
       ResourcesConst.CULTURE,
		"wijmo.jquery.wijmo.wijeditor.js")]
    internal class WijEditor : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_PRO_CSS,
        ResourcesConst.WIJMO_PRO_JS,
        typeof(WijCalendar),
        //typeof(WijTabs),
        typeof(WijDialog),
        typeof(WijDatePager),
        typeof(WijSuperPanel),
        typeof(WijCheckBox),
        typeof(WijTextBox),
        typeof(WijDropDown),
        typeof(WijInputBase),
        "wijmo.jquery.wijmo.wijevcal.js",
        "wijmo.exportUtil.js",
        "wijmo.eventscalendarexport.js")]
    internal class WijEvcal : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
		"wijmo.jquery.wijmo.wijexpander.js")]
    internal class WijExpander : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_PRO_CSS,
        ResourcesConst.WIJMO_PRO_JS,
        typeof(WijSlider),
        typeof(WijPager),
        typeof(WijCarousel),
       ResourcesConst.SWFOBJECT,
		"wijmo.jquery.wijmo.wijgallery.js")]
    internal class WijGallery : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_PRO_JS,
		ResourcesConst.GLOBALIZE,
        ResourcesConst.RAPHAEL,
		ResourcesConst.WIJMO_RAPHAEL,
		"wijmo.jquery.wijmo.wijgauge.js")]
    internal class WijGauge : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_PRO_CSS,
        ResourcesConst.WIJMO_PRO_JS,
		typeof(WijGrid.WijPagerConditionalDependency),
		typeof(WijGrid.WijSuperPanelConditionalDependency),
		typeof(WijGrid.WijInputBaseConditionalDependency),
        ResourcesConst.GLOBALIZE,
        ResourcesConst.CULTURE,
		"wijmo.wijmo.data.js",
        ResourcesConst.POSITION,
		"wijmo.jquery.wijmo.wijgrid.js",
		"wijmo.exportUtil.js",
		"wijmo.gridexport.js"
    )]
    internal class WijGrid : WijmoBase
    {
#if EXTENDER
		[WidgetConditionalDependency(typeof(WijPager), typeof(C1Grid.C1GridExtender))]
#else
		[WidgetConditionalDependency(typeof(WijPager), typeof(C1GridView.C1GridView))]
#endif
		internal sealed class WijPagerConditionalDependency: ConditionalDependencyProxy
		{
			protected internal override bool Resolve(IWijmoWidgetSupport instance, object dependency)
			{
	#if EXTENDER
				return ((C1Grid.C1GridExtender)instance).AllowPaging;
	#else
				return ((C1GridView.C1GridView)instance).AllowPaging;
	#endif
			}
		}

#if EXTENDER
		[WidgetConditionalDependency(typeof(WijSuperPanel), typeof(C1Grid.C1GridExtender))]
#else
		[WidgetConditionalDependency(typeof(WijSuperPanel), typeof(C1GridView.C1GridView))]
#endif
		internal sealed class WijSuperPanelConditionalDependency: ConditionalDependencyProxy
		{
			protected internal override bool Resolve(IWijmoWidgetSupport instance, object dependency)
			{
	#if EXTENDER
				return ((C1Grid.C1GridExtender)instance)._InternalScrollMode() != C1Grid.ScrollMode.None;
#else
				return ((C1GridView.C1GridView)instance)._InternalScrollMode() != C1GridView.ScrollMode.None;
	#endif
			}
		}

#if EXTENDER
		[WidgetConditionalDependency(typeof(WijInputBase), typeof(C1Grid.C1GridExtender))]
#else
		[WidgetConditionalDependency(typeof(WijInputBase), typeof(C1GridView.C1GridView))]
#endif
		internal sealed class WijInputBaseConditionalDependency: ConditionalDependencyProxy
		{
			protected internal override bool Resolve(IWijmoWidgetSupport instance, object dependency)
			{
	#if EXTENDER
				return ((C1Grid.C1GridExtender)instance).ShowFilter;
	#else
				return ((C1GridView.C1GridView)instance).ShowFilter || ((C1GridView.C1GridView)instance).AllowC1InputEditors;
	#endif
			}
		}
    }

    [WidgetDependencies(
        typeof(WijInputCore),
        typeof(WijInputDate),
        typeof(WijInputMask),
        typeof(WijInputNumber),
        typeof(WijInputText)
    )]
    internal class WijInputBase : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_PRO_CSS,
        ResourcesConst.WIJMO_PRO_JS,
        typeof(WijPopup),
        typeof(WijList),
        typeof(WijSuperPanel),
        typeof(WijToolTip),
        ResourcesConst.GLOBALIZE,
        ResourcesConst.CULTURE,
		"wijmo.jquery.wijmo.wijcharex.js",
		"wijmo.jquery.wijmo.wijinputcore.js",
		"wijmo.jquery.wijmo.wijinpututility.js",
		"wijmo.jquery.wijmo.wijstringinfo.js"
    )]
    internal class WijInputCore : WijmoBase
    {
    }

    [WidgetDependencies(
        typeof(WijTabs),
        typeof(WijCalendar),
		"wijmo.jquery.wijmo.wijinputdateformat.js",
		"wijmo.jquery.wijmo.wijinputdateroller.js",
		"wijmo.jquery.wijmo.wijinputdate.js"
    )]
    internal class WijInputDate : WijmoBase
    {
    }
        
    [WidgetDependencies(
		"wijmo.jquery.wijmo.wijinputmaskcore.js",
		"wijmo.jquery.wijmo.wijinputmaskformat.js",
		"wijmo.jquery.wijmo.wijinputmask.js"
    )]
    internal class WijInputMask : WijmoBase
    {
    }

    [WidgetDependencies(
		"wijmo.jquery.wijmo.wijinputnumberformat.js",
		"wijmo.jquery.wijmo.wijinputnumber.js"
    )]
    internal class WijInputNumber : WijmoBase
    {
    }

    [WidgetDependencies(
		"wijmo.jquery.wijmo.wijinputtextformat.js",
		"wijmo.jquery.wijmo.wijinputtext.js"
    )]
    internal class WijInputText : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_PRO_CSS,
        ResourcesConst.WIJMO_PRO_JS,
        typeof(WijVideo),
        ResourcesConst.BGIFRAME,
        ResourcesConst.SWFOBJECT,
		"wijmo.jquery.wijmo.wijlightbox.js"
    )]
    internal class WijLightBox : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_PRO_JS,
		"wijmo.jquery.wijmo.wijlineargauge.js")]
    internal class WijLinearGauge : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_MOBILE_CSS,
		"wijmo.jquery.wijmo.wijlistview.js")]
    internal class WijListView : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        typeof(WijSuperPanel),
        ResourcesConst.BGIFRAME,
		"wijmo.jquery.wijmo.wijmenu.js")]
    internal class WijMenu : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_PRO_CSS,
        ResourcesConst.WIJMO_PRO_JS,
		"wijmo.jquery.wijmo.wijpager.js")]
    internal class WijPager : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
		"wijmo.jquery.wijmo.wijprogressbar.js")]
    internal class WijProgressBar : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_PRO_JS,
		"wijmo.jquery.wijmo.wijradialgauge.js")]
    internal class WijRadialGauge : WijmoBase
    {
    }
    
    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_PRO_CSS,
        ResourcesConst.WIJMO_PRO_JS,
		"wijmo.jquery.wijmo.wijrating.js")]
    internal class WijRating : WijmoBase
    {
    }

#if!EXTENDER

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_PRO_CSS,
        ResourcesConst.WIJMO_PRO_JS, 
        typeof(WijExpander),
        typeof(WijDialog),
        typeof(WijSplitter),
        typeof(WijSuperPanel),
        typeof(WijTabs),
        typeof(WijTextBox),
        typeof(WijTree),
        typeof(WijCalendar),
        typeof(WijInputBase),
        typeof(WijMenu),
        "extensions.wijreportviewer.css",
        "widgets.wijreportviewer.js",
		ResourcesConst.WIDGETS_CONTROL
    )]
    internal class WijReportViewer : WijmoBase
    {
    }
#endif
    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
		"wijmo.jquery.wijmo.wijslider.js")]
    internal class WijSlider : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
		"wijmo.jquery.wijmo.wijsplitter.js")]
    internal class WijSplitter : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.MOUSEWHEEL,
        ResourcesConst.POSITION,
		"wijmo.jquery.wijmo.wijsuperpanel.js")]
    internal class WijSuperPanel : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        typeof(WijSuperPanel),
		"wijmo.jquery.wijmo.wijtabs.js")]
    internal class WijTabs : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
		"wijmo.jquery.wijmo.wijtooltip.js")]
    internal class WijToolTip : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_PRO_CSS,
        ResourcesConst.WIJMO_PRO_JS,
        typeof(WijTextBox),
		"wijmo.jquery.wijmo.wijtree.js")]
    internal class WijTree : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_PRO_CSS,
        ResourcesConst.WIJMO_PRO_JS,
		ResourcesConst.BGIFRAME,
        "swfupload.js",
		"wijmo.jquery.wijmo.wijupload.js")]
    internal class WijUpload : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_PRO_CSS,
        ResourcesConst.WIJMO_PRO_JS,
        typeof(WijSuperPanel),
		"wijmo.jquery.wijmo.wijwizard.js")]
    internal class WijWizard : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
		"wijmo.jquery.wijmo.wijcheckbox.js")]
    internal class WijCheckBox : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_PRO_JS,
		"wijmo.jquery.wijmo.wijdatepager.js")]
    internal class WijDatePager : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
		"base.jquery.bgiframe.js",
		"wijmo.jquery.wijmo.wijdropdown.js")]
    internal class WijDropDown : WijmoBase
    {
    }

    [WidgetDependencies(
        typeof(WijToolTip),
		"wijmo.jquery.wijmo.wijvideo.js")]
    internal class WijVideo : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        typeof(WijSuperPanel),
		"wijmo.jquery.wijmo.wijlist.js")]
    internal class WijList : WijmoBase
    {
    }
    
    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
		"wijmo.jquery.wijmo.wijpopup.js")]
    internal class WijPopup : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
		"wijmo.jquery.wijmo.wijradio.js")]
    internal class WijRadio : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
        ResourcesConst.WIJMO_PRO_JS,
        typeof(WijTabs),
        typeof(WijMenu),
		"wijmo.jquery.wijmo.wijribbon.js")]
    internal class WijRibbon : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
		"wijmo.jquery.wijmo.wijtextbox.js")]
    internal class WijTextBox : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_OPEN_CSS,
		"wijmo.jquery.wijmo.wijflipcard.js")]
    internal class WijFlipCard : WijmoBase
    {
    }

    [WidgetDependencies(
        ResourcesConst.WIJMO_PRO_JS,
        ResourcesConst.RAPHAEL,
		ResourcesConst.WIJMO_RAPHAEL,
		"wijmo.jquery.wijmo.wijsparkline.js")]
    internal class WijSparkline : WijmoBase
    {
    }

	[WidgetDependencies(
		ResourcesConst.WIJMO_OPEN_CSS,
		ResourcesConst.WIJMO_PRO_CSS,
		ResourcesConst.WIJMO_PRO_JS,
		typeof(WijGrid),
		typeof(WijPager),
		typeof(WijDialog),
		typeof(WijTree),
		typeof(WijSplitter),
		typeof(WijMenu),
		"wijmo.jquery.wijmo.wijfileexplorer.js",
		ResourcesConst.GLOBALIZE,
		ResourcesConst.CULTURE
	)]
	internal class WijFileExplorer : WijmoBase
	{
	}

	[WidgetDependencies(
	ResourcesConst.WIJMO_OPEN_CSS,
	ResourcesConst.WIJMO_PRO_CSS,
	ResourcesConst.MOUSEWHEEL,
	ResourcesConst.RAPHAEL,
	ResourcesConst.WIJMO_RAPHAEL,
	ResourcesConst.WIJMO_PRO_JS,
	typeof(WijSlider),
	"wijmo.jquery.wijmo.wijmaps.js"
	)]
	internal class WijMaps : WijmoBase
	{
	}

	[WidgetDependencies(ResourcesConst.WIJMO_OPEN_CSS,
		ResourcesConst.WIJMO_PRO_CSS,
		ResourcesConst.WIJMO_PRO_JS,
		typeof(WijToolTip),
		"wijmo.jquery.wijmo.wijtreemap.js"
	)]
	internal class WijTreeMap : WijmoBase 
	{ 
	}
}
