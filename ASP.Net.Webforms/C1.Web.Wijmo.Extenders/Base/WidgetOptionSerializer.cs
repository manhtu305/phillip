﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Runtime.InteropServices;
using System.Reflection;
using System.Globalization;
using System.Drawing;
using System.Text.RegularExpressions;
using System.IO;
using System.Collections;
using System.Runtime;
using System.Web.Script.Serialization;

#if EXTENDER 
using C1.Web.Wijmo.Extenders;
using C1.Web.Wijmo.Extenders.Converters;
#else
using C1.Web.Wijmo.Controls.Converters;
#endif

#if EXTENDER 
namespace C1.Web.Wijmo
#else
namespace C1.Web.Wijmo.Controls
#endif
{
	/// <summary>
	/// Json Writer.
	/// </summary>
	internal sealed class WidgetOptionSerializer
	{
		private Stack<Scope> _scopes;
		private IndentedTextWriter _writer;
		private IUrlResolutionService _urlResolutionService;
		//private IControlResolver _ctrlResolver;
		private static Dictionary<Type, Converter<object, string>> _customConverters;

		public WidgetOptionSerializer(TextWriter writer, IUrlResolutionService urlResolutionService)
		{
			this._writer = new IndentedTextWriter(writer, true);
			this._scopes = new Stack<WidgetOptionSerializer.Scope>();
			this._urlResolutionService = urlResolutionService;
			//this._ctrlResolver = controlResolver;
		}

		static WidgetOptionSerializer()
		{
			_customConverters = new Dictionary<Type, Converter<object, string>>();
			_customConverters.Add(typeof(Color), delegate(object value)
			{
				return ColorTranslator.ToHtml((Color)value);
			});
		}

		public void EndScope()
		{
			if (this._scopes.Count == 0)
			{
				throw new InvalidOperationException("No active scope to end.");
			}

			this._writer.Indent--;
			WidgetOptionSerializer.Scope scope1 = this._scopes.Pop();
			if (scope1.Type == WidgetOptionSerializer.ScopeType.Array)
			{
				this._writer.Write("]");
			}
			else
			{
				this._writer.Write("}");
			}
		}

		private static string QuoteJScriptString(string s)
		{
			if (string.IsNullOrEmpty(s))
			{
				return string.Empty;
			}
			StringBuilder builder1 = null;
			int num1 = 0;
			int num2 = 0;
			for (int num3 = 0; num3 < s.Length; num3++)
			{
				char ch1 = s[num3];
				if ((((ch1 == '\r') || (ch1 == '\t')) || ((ch1 == '"') || (ch1 == '\''))) || (((ch1 == '\\') || (ch1 == '\r')) || ((ch1 < ' ') || (ch1 > '\x007f'))))
				{
					if (builder1 == null)
					{
						builder1 = new StringBuilder(s.Length + 6);
					}
					if (num2 > 0)
					{
						builder1.Append(s, num1, num2);
					}
					num1 = num3 + 1;
					num2 = 0;
				}
				switch (ch1)
				{
					case '\'':
						builder1.Append("\u0027");
						break;
					case '\\':
						builder1.Append(@"\\");
						break;
					case '\t':
						builder1.Append(@"\t");
						break;
					case '\n':
						builder1.Append(@"\n");
						break;
					case '\r':
						builder1.Append(@"\r");
						break;
					case '"':
						builder1.Append("\\\""); //Fixed issue in WidgetOptionSerializer with double quotes.
						// do not use following, because \u0022 will be decoded by javascript automatically before parsing action in client side JSON parser:
						// builder1.Append("\u0022");// \u0022 - Double Quote "
						break;
					default:
						if ((ch1 < ' ') || (ch1 > '\x007f'))
						{
							builder1.AppendFormat(CultureInfo.InvariantCulture, @"\u{0:x4}", new object[] { (int)ch1 });
						}
						else
						{
							num2++;
						}
						break;
				}
			}
			string text1 = s;
			if (builder1 == null)
			{
				return text1;
			}
			if (num2 > 0)
			{
				builder1.Append(s, num1, num2);
			}
			return builder1.ToString();
		}

		public void StartArrayScope()
		{
			this.StartScope(WidgetOptionSerializer.ScopeType.Array);
		}

		public void StartObjectScope()
		{
			this.StartScope(WidgetOptionSerializer.ScopeType.Object);
		}

		private void StartScope(WidgetOptionSerializer.ScopeType type)
		{
			if (this._scopes.Count != 0)
			{
				WidgetOptionSerializer.Scope scope1 = this._scopes.Peek();
				if ((scope1.Type == WidgetOptionSerializer.ScopeType.Array) && (scope1.ObjectCount != 0))
				{
					this._writer.Write(", ");
				}
				scope1.ObjectCount++;
			}
			WidgetOptionSerializer.Scope scope2 = new WidgetOptionSerializer.Scope(type);
			this._scopes.Push(scope2);
			if (type == WidgetOptionSerializer.ScopeType.Array)
			{
				this._writer.Write("[");
			}
			else
			{
				this._writer.Write("{");
			}
			this._writer.Indent++;
		}


		private void WriteCore(string text, bool quotes)
		{
			if (this._scopes.Count != 0)
			{
				WidgetOptionSerializer.Scope scope1 = this._scopes.Peek();
				if (scope1.Type == WidgetOptionSerializer.ScopeType.Array)
				{
					if (scope1.ObjectCount != 0)
					{
						this._writer.Write(", ");
					}
					scope1.ObjectCount++;
				}
			}
			if (quotes)
			{
				this._writer.Write('"');
			}
			this._writer.Write(text);
			if (quotes)
			{
				this._writer.Write('"');
			}
		}

		public void WriteName(string name)
		{
			WriteName(name, true);
		}

		public void WriteName(string name, bool camelize)
		{
			if (string.IsNullOrEmpty(name))
			{
				throw new ArgumentNullException("name");
			}
			if (this._scopes.Count == 0)
			{
				throw new InvalidOperationException("No active scope to write into.");
			}
			if (this._scopes.Peek().Type != WidgetOptionSerializer.ScopeType.Object)
			{
				throw new InvalidOperationException("Names can only be written into Object scopes.");
			}
			WidgetOptionSerializer.Scope scope1 = this._scopes.Peek();
			if (scope1.Type == WidgetOptionSerializer.ScopeType.Object)
			{
				if (scope1.ObjectCount != 0)
				{
					this._writer.Write(", ");
				}
				scope1.ObjectCount++;
			}

			if (camelize)
			{
				name = name.ToCamel();
			}

			if (name.Contains("-"))
			{
				this._writer.Write("\"");
				this._writer.Write(name);
				this._writer.Write("\"");
			}
			else
			{
				this._writer.Write(name);
			}
			this._writer.Write(": ");
		}


		public void WriteValue(ICustomOptionType iCustomOptionType)
		{
			this.WriteCore(iCustomOptionType.SerializeValue(), iCustomOptionType.IncludeQuotes);
		}

		public void WriteValue(bool value)
		{
			this.WriteCore(value ? "true" : "false", false);
		}


		public void WriteValue(ICollection items)
		{
			if ((items == null) || (items.Count == 0))
			{
				this.WriteCore("[]", false);
			}
			else
			{
				this.StartArrayScope();
				foreach (object obj1 in items)
				{
					/*IJsonEmptiable jsonEmptiable = obj1 as IJsonEmptiable;
					if (jsonEmptiable != null && jsonEmptiable.IsEmpty)
					{
						continue;
					}*/

					this.WriteValue(obj1);
				}
				this.EndScope();
			}
		}

		public void WriteValue(IDictionary record)
		{
			if ((record == null) || (record.Count == 0))
			{
				this.WriteCore("{}", false);
			}
			else
			{
				this.StartObjectScope();
				foreach (DictionaryEntry entry1 in record)
				{
					string text1 = entry1.Key as string;
					if (string.IsNullOrEmpty(text1))
					{
						throw new ArgumentException("Key of unsupported type contained in Hashtable.");
					}

					IJsonEmptiable jsonEmptiable = entry1.Value as IJsonEmptiable;
					if (jsonEmptiable != null && jsonEmptiable.IsEmpty)
					{
						continue;
					}

					this.WriteName(text1);
					this.WriteValue(entry1.Value);
				}
				this.EndScope();
			}
		}


		public void WriteValue(DateTime dateTime)
		{
			if (dateTime == null)
			{
				throw new ArgumentOutOfRangeException("dateTime");
			}

			////"YYYY-MM-DDThh:mm:ss:mssZ"
			//string jsonDateStr = FillByZero(dateTime.Year, 4) + "-" + FillByZero(dateTime.Month, 2)
			//    + "-" + FillByZero(dateTime.Day, 2)
			//    + "T" + FillByZero(dateTime.Hour, 2)
			//    + ":" + FillByZero(dateTime.Minute, 2)
			//    + ":" + FillByZero(dateTime.Second, 2)
			//    + ":" + FillByZero(dateTime.Millisecond, 3)
			//    + "Z";

			//this.WriteCore(jsonDateStr, true);

			DateTime d1 = new DateTime(1970, 1, 1);
			DateTime d2 = dateTime.ToUniversalTime();
			TimeSpan ts = new TimeSpan(d2.Ticks - d1.Ticks);

			string jsonDateStr = string.Format("new Date({0})", ts.TotalMilliseconds);
			this.WriteCore(jsonDateStr, false);
		}

		private string FillByZero(int val, int k)
		{
			string s = val.ToString();
			while (s.Length < k)
			{
				s = "0" + s;
			}
			return s;
		}

		/*public void WriteValue(int value)
		{
			this.WriteCore(value.ToString(CultureInfo.InvariantCulture), false);
		}*/

		public void WriteValue(Enum value)
		{
			this.WriteCore("\'" + value.ToString().ToCamel() + "\'", false);
		}

		public void WriteValue(object o)
		{
			if (o == null)
			{
				this.WriteCore("null", false);
			}
			else if ((o is IJsonEmptiable) && ((IJsonEmptiable)o).IsEmpty)
			{
				return;
			}
			else if (o is ICustomOptionType)
			{
				this.WriteValue((ICustomOptionType)o);
			}
			else if (o is bool)
			{
				this.WriteValue((bool)o);
			}
			/*else if (o is int)
			{
				this.WriteValue((int)o);
			}
			else if (o is float)
			{
				this.WriteValue((float)o);
			}
			else if (o is double)
			{
				this.WriteValue((double)o);
			}*/
			else if (o is DateTime)
			{
				this.WriteValue((DateTime)o);
			}
			else if (o is string)
			{
				this.WriteValue((string)o);
			}
			else if (o is char)
			{
				this.WriteValue((char)o);
			}
			else if (o is Unit)
			{
				this.WriteValue(((Unit)o).IsEmpty ? "" : ((Unit)o).ToString());
			}
			else if (o is IDictionary)
			{
				this.WriteValue((IDictionary)o);
			}
			else if (o is ICollection)
			{
				this.WriteValue((ICollection)o);
			}
			else if (o is Enum)
			{
				this.WriteValue((Enum)o);
			}
			else if (o is Color)
			{
				this.WriteValue((Color)o);
			}
			else if (o is PositionSettings)
			{
				this.WriteValue((PositionSettings)o);
			}
			else if (o.GetType().IsPrimitive || o is decimal)
			{
				this.WriteCore(Convert.ToString(o, CultureInfo.InvariantCulture), false);
			}
			else
			{
				this.StartObjectScope();

				// Write public properties:
				foreach (PropertyDescriptor prop in TypeDescriptor.GetProperties(o))
				{
					WidgetOptionAttribute optionAttr = null;
					WidgetEventAttribute eventAttr = null;
					WidgetOptionNameAttribute nameAttr = null;
					IDReferencePropertyAttribute idRefAttr = null;
					UrlPropertyAttribute urlAttr = null;
					//DefaultValueAttribute defValueAttr = null;
					ElementReferenceAttribute elementAttr = null;
					//JQueryReferenceAttribute jqAttr = null;

					foreach (Attribute attr in prop.Attributes)
					{
						Type attrType = attr.GetType();
						/*if (attrType == typeof(DefaultValueAttribute))
						{
							defValueAttr = attr as DefaultValueAttribute;							
						}
						else*/

						if (attrType == typeof(WidgetOptionAttribute))
						{
							optionAttr = attr as WidgetOptionAttribute;
						}
						else
						{
							if (attrType == typeof(WidgetEventAttribute))
							{
								eventAttr = attr as WidgetEventAttribute;
								continue;
							}
							if (attrType == typeof(WidgetOptionNameAttribute))
							{
								nameAttr = attr as WidgetOptionNameAttribute;
								continue;
							}
							if (attrType == typeof(IDReferencePropertyAttribute))
							{
								idRefAttr = attr as IDReferencePropertyAttribute;
								continue;
							}
							if (attrType == typeof(UrlPropertyAttribute))
							{
								urlAttr = attr as UrlPropertyAttribute;
								continue;
							}
							if (attrType == typeof(ElementReferenceAttribute))
							{
								elementAttr = attr as ElementReferenceAttribute;
								continue;
							}
							/*
							if (attrType == typeof(JQueryReferenceAttribute))
							{
								jqAttr = attr as JQueryReferenceAttribute;
							}*/
						}
					}

					string optionName = prop.Name;
					if (optionAttr != null || eventAttr != null)
					{
						if ((nameAttr != null) && !string.IsNullOrEmpty(nameAttr.Name))
						{
							optionName = nameAttr.Name;
						}

						object value = prop.GetValue(o);

						if ((value is IJsonEmptiable && !((IJsonEmptiable)value).IsEmpty) ||
						   (!(value is IJsonEmptiable) && (prop.ShouldSerializeValue(o) || prop.IsReadOnly)))
						//if (prop.ShouldSerializeValue(o) || prop.IsReadOnly)
						{
							Control c = null;
							if (value != null)
							{
								if ((eventAttr != null) && (prop.PropertyType != typeof(string)))
								{
									throw new InvalidOperationException("WidgetEventAttribute can only be applied to a property with a PropertyType of System.String.");
								}
								if (!prop.PropertyType.IsPrimitive)
								{
									Converter<object, string> customConverter = null;
									if (!_customConverters.TryGetValue(prop.PropertyType, out customConverter))
									{
										foreach (KeyValuePair<Type, Converter<object, string>> pair in _customConverters)
										{
											if (prop.PropertyType.IsSubclassOf(pair.Key))
											{
												customConverter = pair.Value;
												break;
											}
										}
									}
									if (customConverter != null)
									{
										value = customConverter(value);
									}
									else if (optionAttr == null)
									{
										value = prop.Converter.ConvertToString(null, CultureInfo.InvariantCulture, value);
									}
								}
								/*
								if ((idRefAttr != null) && (_ctrlResolver != null))
								{
									c = _ctrlResolver.ResolveControl((string)value);
								}
								*/
								if ((urlAttr != null) && (_urlResolutionService != null))
								{
									value = _urlResolutionService.ResolveClientUrl((string)value);
								}
								if (elementAttr != null)
								{
									/*todo:?
									if ((c == null) && (_ctrlResolver != null))
									{
										c = _ctrlResolver.ResolveControl((string)value);
									}*/
									if (c != null)
									{
										value = c.ClientID;
									}
									value = this.ConvertToElement(value.ToString());
								}
								/*todo:?
								if (jqAttr != null)
								{
									if ((c == null) && (_ctrlResolver != null))
									{
										c = _ctrlResolver.ResolveControl((string)value);
									}
									if (c != null)
									{
										value = c.ClientID;
									}
									value = this.ConvertToJQueryReference(value.ToString());
								}
								*/
								if (c != null)
								{
									value = c.ClientID;
								}

								/*
								if (defValueAttr != null)
								{
									//if (defValueAttr.Value == value){
									//throw new Exception(optionName + ":Found default value: " + defValueAttr.Value + "? " + (defValueAttr.Value == value) + ", defValueAttr.Value=" + defValueAttr.Value + ", value=" + value);
									//}
									// skip
									// do not serialize default value.
								}*/

								if (eventAttr != null)
								{
									if ((nameAttr != null) && !string.IsNullOrEmpty(nameAttr.Name))
									{
										optionName = nameAttr.Name;
									}
									else
										optionName = optionName.Replace("OnClient", "").ToLowerInvariant();

									this.WriteName(optionName);
									this.WriteCore((string)value, false);
								}
								else
								{
									this.WriteName(optionName);
									this.WriteValue(value);
								}
							}
						}
					}
				}

				this.EndScope();
			}
		}

		public static string ConvertFromHexUnicode(string result)
		{
			Regex regex = new Regex(@"\\[uU]([0-9A-F]{4})", RegexOptions.IgnoreCase);
			result = regex.Replace(result, match => ((char)int.Parse(match.Groups[1].Value, NumberStyles.HexNumber)).ToString());
			return result;
		}


		private string ConvertToElement(string id)
		{
			return "jQuery(\"#" + id + "\")[0]";
			//System.Web.HttpUtility.JavaScriptStringEncode(
		}

		private string ConvertToJQueryReference(string id)
		{
			return "jQuery(\"#" + id + "\")";
			//System.Web.HttpUtility.JavaScriptStringEncode(
		}

		private bool CompareObjectWithEmptyValue(object valObj, object emptyValObj)
		{

			if (valObj is bool && emptyValObj is bool)
				return ((bool)valObj) == ((bool)emptyValObj);
			if (valObj is int && emptyValObj is int)
				return ((int)valObj) == ((int)emptyValObj);
			if (valObj is float && emptyValObj is float)
				return ((float)valObj) == ((float)emptyValObj);
			if (valObj is DateTime && emptyValObj is DateTime)
				return ((DateTime)valObj) == ((DateTime)emptyValObj);
			if (valObj is string && emptyValObj is string)
				return ((string)valObj) == ((string)emptyValObj);
			if (valObj is Enum && emptyValObj is Enum)
			{
				int int1 = (int)valObj;
				int int2 = (int)emptyValObj;
				return int1 == int2;
				//return ((Enum)obj1) == ((Enum)obj2);
			}
			if (valObj is Color)
			{
				if (emptyValObj is Color)
				{
					return ((Color)valObj).Equals((Color)emptyValObj);
				}
				else
				{
					string sColor = emptyValObj as string;
					if (sColor != null)
					{
						Color color = C1WebColorConverter.ToColor(sColor);
						if (color != null)
							return ((Color)valObj).Equals(color);
						else
							return ((Color)valObj).IsEmpty;
					}
					else
					{
						return ((Color)valObj).IsEmpty;
					}
				}
			}
			if (valObj is Unit && emptyValObj is string)
			{
				string s = (string)emptyValObj;
				if (s == "" && ((Unit)valObj).IsEmpty)
					return true;
				Unit resultEmptyUnit = Unit.Parse(s);
				return resultEmptyUnit.Equals((Unit)valObj);
			}
			return valObj == emptyValObj;


		}

		/*public void WriteValue(float value)
		{
			this.WriteCore(value.ToString(CultureInfo.InvariantCulture), false);
		}

		public void WriteValue(double value)
		{
			this.WriteCore(value.ToString(CultureInfo.InvariantCulture), false);
		}*/

		public void WriteValue(char ch)
		{
			this.WriteCore(WidgetOptionSerializer.QuoteJScriptString(ch.ToString()), true);
		}

		public void WriteValue(string s)
		{
			if (s == null)
			{
				this.WriteCore("null", false);
			}
			else
			{
				this.WriteCore(WidgetOptionSerializer.QuoteJScriptString(s), true);
			}
		}

		public void WriteValue(Color c)
		{
			this.WriteCore(WidgetOptionSerializer.QuoteJScriptString(C1WebColorConverter.ToStringHexColor(c)), true);
		}

		// Serialize the positionSettings object. Edit by dail 2011-1-21
		public void WriteValue(PositionSettings p)
		{
			this.WriteCore(WidgetOptionSerializer.QuoteJScriptString(PositionSettingsConverter.ToString(p)), false);
		}

		// Nested Types
		private sealed class Scope
		{
			public Scope(WidgetOptionSerializer.ScopeType type)
			{
				this._type = type;
			}

			// Properties
			public int ObjectCount
			{
				get
				{
					return this._objectCount;
				}
				set
				{
					this._objectCount = value;
				}
			}

			public WidgetOptionSerializer.ScopeType Type
			{
				get
				{
					return this._type;
				}
			}



			// Fields
			private int _objectCount;
			private WidgetOptionSerializer.ScopeType _type;
		}


		private enum ScopeType
		{
			Array,
			Object
		}
	}

	internal sealed class IndentedTextWriter : TextWriter
	{
		// Fields
		private int _indentLevel;
		private bool _minimize;
		private bool _tabsPending;
		private string _tabString;
		private TextWriter _writer;

		// Methods
		public IndentedTextWriter(TextWriter writer, bool minimize)
			: base(CultureInfo.InvariantCulture)
		{
			this._writer = writer;
			this._minimize = minimize;
			if (this._minimize)
			{
				this.NewLine = "\r";//"";// 
			}
			this._tabString = "    ";
			this._indentLevel = 0;
			this._tabsPending = false;
		}

		public override void Close()
		{
			this._writer.Close();
		}

		public override void Flush()
		{
			this._writer.Flush();
		}

		private void OutputTabs()
		{
			if (this._tabsPending)
			{
				if (!this._minimize)
				{
					for (int i = 0; i < this._indentLevel; i++)
					{
						this._writer.Write(this._tabString);
					}
				}
				this._tabsPending = false;
			}
		}

		public override void Write(bool value)
		{
			this.OutputTabs();
			this._writer.Write(value);
		}

		public override void Write(char value)
		{
			this.OutputTabs();
			this._writer.Write(value);
		}

		public override void Write(string s)
		{
			this.OutputTabs();
			this._writer.Write(s);
		}

		public override void Write(char[] buffer)
		{
			this.OutputTabs();
			this._writer.Write(buffer);
		}

		public override void Write(double value)
		{
			this.OutputTabs();
			this._writer.Write(value);
		}

		public override void Write(int value)
		{
			this.OutputTabs();
			this._writer.Write(value);
		}

		public override void Write(long value)
		{
			this.OutputTabs();
			this._writer.Write(value);
		}

		public override void Write(object value)
		{
			this.OutputTabs();
			this._writer.Write(value);
		}

		public override void Write(float value)
		{
			this.OutputTabs();
			this._writer.Write(value);
		}

		public override void Write(string format, params object[] arg)
		{
			this.OutputTabs();
			this._writer.Write(format, arg);
		}

		public override void Write(string format, object arg0)
		{
			this.OutputTabs();
			this._writer.Write(format, arg0);
		}

		public override void Write(char[] buffer, int index, int count)
		{
			this.OutputTabs();
			this._writer.Write(buffer, index, count);
		}

		public override void Write(string format, object arg0, object arg1)
		{
			this.OutputTabs();
			this._writer.Write(format, arg0, arg1);
		}

		public override void WriteLine()
		{
			this.OutputTabs();
			this._writer.WriteLine();
			this._tabsPending = true;
		}

		public override void WriteLine(bool value)
		{
			this.OutputTabs();
			this._writer.WriteLine(value);
			this._tabsPending = true;
		}

		public override void WriteLine(char value)
		{
			this.OutputTabs();
			this._writer.WriteLine(value);
			this._tabsPending = true;
		}

		public override void WriteLine(double value)
		{
			this.OutputTabs();
			this._writer.WriteLine(value);
			this._tabsPending = true;
		}

		public override void WriteLine(int value)
		{
			this.OutputTabs();
			this._writer.WriteLine(value);
			this._tabsPending = true;
		}

		public override void WriteLine(char[] buffer)
		{
			this.OutputTabs();
			this._writer.WriteLine(buffer);
			this._tabsPending = true;
		}

		public override void WriteLine(long value)
		{
			this.OutputTabs();
			this._writer.WriteLine(value);
			this._tabsPending = true;
		}

		public override void WriteLine(object value)
		{
			this.OutputTabs();
			this._writer.WriteLine(value);
			this._tabsPending = true;
		}

		public override void WriteLine(float value)
		{
			this.OutputTabs();
			this._writer.WriteLine(value);
			this._tabsPending = true;
		}

		public override void WriteLine(string s)
		{
			this.OutputTabs();
			this._writer.WriteLine(s);
			this._tabsPending = true;
		}

		public override void WriteLine(uint value)
		{
			this.OutputTabs();
			this._writer.WriteLine(value);
			this._tabsPending = true;
		}

		public override void WriteLine(string format, params object[] arg)
		{
			this.OutputTabs();
			this._writer.WriteLine(format, arg);
			this._tabsPending = true;
		}

		public override void WriteLine(string format, object arg0)
		{
			this.OutputTabs();
			this._writer.WriteLine(format, arg0);
			this._tabsPending = true;
		}

		public override void WriteLine(string format, object arg0, object arg1)
		{
			this.OutputTabs();
			this._writer.WriteLine(format, arg0, arg1);
			this._tabsPending = true;
		}

		public override void WriteLine(char[] buffer, int index, int count)
		{
			this.OutputTabs();
			this._writer.WriteLine(buffer, index, count);
			this._tabsPending = true;
		}

		public void WriteLineNoTabs(string s)
		{
			this._writer.WriteLine(s);
		}

		public void WriteNewLine()
		{
			if (!this._minimize)
			{
				this.WriteLine();
			}
		}

		public void WriteSignificantNewLine()
		{
			this.WriteLine();
		}

		public void WriteTrimmed(string text)
		{
			if (!this._minimize)
			{
				this.Write(text);
			}
			else
			{
				this.Write(text.Trim());
			}
		}

		// Properties
		public override Encoding Encoding
		{
			get
			{
				return this._writer.Encoding;
			}
		}

		public int Indent
		{
			get
			{
				return this._indentLevel;
			}
			set
			{
				if (value < 0)
				{
					value = 0;
				}
				this._indentLevel = value;
			}
		}

		public override string NewLine
		{
			get
			{
				return this._writer.NewLine;
			}
			set
			{
				this._writer.NewLine = value;
			}
		}
	}

	/// <summary>
	/// Interface used by serializator to check if an object should be serialized.
	/// <remarks>
	public interface IJsonEmptiable
	{
		/// <summary>
		/// Gets value that indicates if the object is empty or not.
		/// If object is empty than it will be skipped during the serialization process.
		/// </summary>
		bool IsEmpty { get; }
	}
}
