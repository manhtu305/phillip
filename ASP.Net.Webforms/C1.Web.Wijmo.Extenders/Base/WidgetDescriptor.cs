﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Web.Script.Serialization;
using System.Runtime;


#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
	/// <summary>
	/// Represents that how to defines script that creates an instance of a client widget class.
	/// </summary>
	public class WidgetDescriptor :  ScriptDescriptor
	{
		// Fields
		private string _type;
		private string _targetSelector;
		private string _options;
		private bool _registerDispose;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="C1.Web.Wijmo.Controls.WidgetDescriptor" /> class.
		/// </summary>
		/// <param name="type"></param>
		public WidgetDescriptor(string type)
		{
			this._registerDispose = true;
			if (string.IsNullOrEmpty(type))
			{
				throw new ArgumentException("Null or Empty", "name");
			}
			this._type = type;
		}

		internal WidgetDescriptor(string type, string targetSelector)
			: this(type)
		{
			if (string.IsNullOrEmpty(targetSelector))
			{
				throw new ArgumentException("Null or Empty", "targetSelector");
			}
			this._targetSelector = targetSelector;
		}

		internal string TargetSelector
		{
			get
			{
				return this._targetSelector;
			}
		}

		/// <summary>
		/// Type of widget descriptor.
		/// </summary>
		public string Type
		{
			get
			{
				return this._type;
			}
			set
			{
				if (string.IsNullOrEmpty(value))
				{
					throw new ArgumentException("Null or Empty", "value");
				}
				this._type = value;
			}
		}

		/// <summary>
		/// Options of widget descriptor.
		/// </summary>
		public string Options
		{
			get { return _options; }
			set { _options = value; }
		}

		//internal string GetScriptInternal()
		//{
			////return this.GetScript();
			//return this.GetScriptInternal(WijmoControlMode.Web);
		//}
		internal string GetScriptInternal(WijmoControlMode mode, bool isControlInAppView)
		{
			StringBuilder builder = new StringBuilder();
			if (mode == WijmoControlMode.Mobile)
			{
				//create widget in pageinit.
				//if control is a part of C1AppViewPage, use "one" to bind "c1appviewpageinit" event,
				//just like what we did in WidgetExplorerMobile sample.
				if (isControlInAppView)
				{
					builder.Append("jQuery(document).one('c1appviewpageinit', function () {");
				}
				else
				{
					builder.Append("jQuery(document).one('pageinit', function () {"); 
				}
			}
			builder.Append(this.GetScript());

			if (mode == WijmoControlMode.Mobile)
			{
				builder.Append("});");
			}

			return builder.ToString();
		}

		/// <summary>
		/// Get javascrtipt of initializing widget to be rendered on page.
		/// </summary>
		/// <returns></returns>
		protected override string GetScript()
		{
			StringBuilder builder = new StringBuilder();
			//builder.Append("jQuery(document).ready(function () {"); 
			builder.Append("jQuery(\"");
			builder.Append(this.TargetSelector);
			builder.Append("\").");
			builder.Append(this.Type);
			builder.Append("(wijmoASPNetParseOptions(");
			builder.Append(this.Options);
			builder.Append("));");
			//builder.Append("});");
			return builder.ToString();
		}

		internal bool RegisterDispose
		{
			get
			{
				return this._registerDispose;
			}
			set
			{
				this._registerDispose = value;
			}
		}

		
	}

}
