﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
	/// <summary>
	/// Define the jQuery slide animation.
	/// </summary>
	public class SlideAnimation : Settings
	{

		/// <summary>
		/// Initializes a new instance of the <see cref="SlideAnimation"/> class.
		/// </summary>
		public SlideAnimation()
		{
		}

		/// <summary>
		/// Defines the duration to animate
		/// </summary>
		[DefaultValue(400)]
		[C1Description("SlideAnimation.Duration")]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int Duration
		{
			get
			{
				return this.GetPropertyValue<int>("Duration", 400);
			}
			set
			{
				this.SetPropertyValue<int>("Duration", value);
			}
		}

		/// <summary>
		/// Defines the easing for animation.
		/// </summary>
		[DefaultValue(Easing.Swing)]
		[C1Description("SlideAnimation.Easing")]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Easing Easing
		{
			get
			{
				return this.GetPropertyValue<Easing>("Easing", Easing.Swing);
			}
			set
			{
				this.SetPropertyValue<Easing>("Easing", value);
			}
		}

		/// <summary>
		/// Detemines whether use the animation.
		/// </summary>
		[DefaultValue(false)]
		[C1Description("SlideAnimation.Disabled")]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Disabled
		{
			get
			{
				return this.GetPropertyValue<bool>("Disabled", false);
			}
			set
			{
				this.SetPropertyValue<bool>("Disabled", value);
			}
		}
	}
}
