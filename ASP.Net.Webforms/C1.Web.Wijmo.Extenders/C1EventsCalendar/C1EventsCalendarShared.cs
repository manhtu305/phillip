﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Drawing.Design;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Web;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
using C1.Web.Wijmo.Extenders.Converters;

[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1EventsCalendar", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1EventsCalendar
#else
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.Converters;

[assembly: TagPrefix("namespace C1.Web.Wijmo.Controls.C1EventsCalendar", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1EventsCalendar
#endif
{
	[WidgetDependencies(
		typeof(WijEvcal)
#if !EXTENDER
		,"extensions.c1eventscalendar.css",
		"extensions.c1eventscalendar.js",
		ResourcesConst.C1WRAPPER_PRO
#else
		// note, c1json2 is needed for events calendar built-in xml data storage
		,"c1json2.js"
#endif
)]
#if EXTENDER
	public partial class C1EventsCalendarExtender
#else
	public partial class C1EventsCalendar : ICultureControl
#endif
	{

		#region ** constants
#if EXTENDER
		private const string DESTINATION_WEBSERVICE_FILENAME = "c1evcalextservice.ashx";
#else
		private const string DESTINATION_WEBSERVICE_FILENAME = "c1evcalservice.ashx";
#endif
		#endregion

		#region ** fields

		private SelectedDatesCollection _selectedDates;
		C1EventsCalendarStorage _DataStorage;
		private DayViewHeaderFormatOption _DayViewHeaderFormatOption;
		private LocalizationOption _LocalizationOption;
		private List<View> _views;
		private DatePagerLocalizationOption _DatePagerLocalizationOption;
		private C1EventsCalendarViewType _viewType;
		private bool _isDefaultVisibleCalendars = true;

		#endregion

		#region ** options

		/// <summary>
		/// The URL to the web service which will be used 
		///	to store information about events.
		/// </summary>
		/// <value>The HTTP handler URL.</value>
		[C1Category("Category.Behavior")]
		[C1Description("C1EventsCalendar.WebServiceUrl", "The URL to the web service which will be used to store information about events.")]
		[WidgetOption()]
		[UrlProperty()]
		[DefaultValue("~/" + DESTINATION_WEBSERVICE_FILENAME)]
		public string WebServiceUrl
		{
			get
			{

				if (HttpContext.Current != null)
				{
					// runtime
					if (!eventsDSPresent && !calendarsDSPresent
						&& string.IsNullOrEmpty(this.DataStorage.DataFile))
					{
						// fix for 27363 case 2://qq: re-check with && string.IsNullOrEmpty(this.DataStorage.DataFile)
						return ResolveClientUrl(GetPropertyValue<string>("WebServiceUrl", ""));
					}
					else
					{
						return ResolveClientUrl(GetPropertyValue<string>("WebServiceUrl",
															"~/" + DESTINATION_WEBSERVICE_FILENAME));
					}
				}
				return GetPropertyValue<string>("WebServiceUrl", "~/" + DESTINATION_WEBSERVICE_FILENAME);
			}
			set
			{
				SetPropertyValue<string>("WebServiceUrl", value);
			}
		}

		/// <summary>
		/// The colors property specifies the list of color names which 
		///	will be shown in the color name drop down list.
		/// </summary>
		[DefaultValue(new string[] { "red", "darkorchid", "green",
								"blue", "cornflowerblue", "yellow", "bronze" })]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.Colors",
			"The Colors property specifies the list of color name which will be shown in the color name drop down list in the event dialog.")]
		[TypeConverter(typeof(StringArrayConverter))]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string[] Colors
		{
			get
			{
				return GetPropertyValue<string[]>("Colors",
					new string[] { "red", "darkorchid", "green",
								"blue", "cornflowerblue", "yellow", "bronze" });
			}
			set
			{
				SetPropertyValue<string[]>("Colors", value);
			}
		}

		/// <summary>
		/// Enables built-in log console.
		/// </summary>
		[DefaultValue(false)]
		[C1Category("Category.Behavior")]
		[C1Description("C1EventsCalendar.EnableLogs", "Enables built-in log console.")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		[WidgetOption()]
		public bool EnableLogs
		{
			get
			{
				return GetPropertyValue<bool>("EnableLogs", false);
			}
			set
			{
				SetPropertyValue<bool>("EnableLogs", value);
			}
		}

		/// <summary>
		/// Indicating the event dialog element will be append to the body or eventcalendar container.
		/// </summary>
		[DefaultValue(false)]
		[C1Category("Category.Behavior")]
		[C1Description("C1EventsCalendar.EnsureEventDialogOnBody", "Indicating the event dialog element will be append to the body or eventcalendar container.")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		[WidgetOption()]
		public bool EnsureEventDialogOnBody
		{
			get
			{
				return GetPropertyValue<bool>("EnsureEventDialogOnBody", false);
			}
			set
			{
				SetPropertyValue<bool>("EnsureEventDialogOnBody", value);
			}
		}

		/// <summary>
		/// Indicates whether the header bar will be visible.
		/// </summary>
		[DefaultValue(true)]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.HeaderBarVisible",
					"Indicates whether the header bar will be visible.")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public bool HeaderBarVisible
		{
			get
			{
				return GetPropertyValue<bool>("HeaderBarVisible", true);
			}
			set
			{
				SetPropertyValue<bool>("HeaderBarVisible", value);
			}
		}

		/// <summary>
		/// Used for export, indicates which theme is applied to this control.
		/// </summary>
		[WidgetOption()]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override string Theme
		{
			get
			{
				return base.Theme;
			}
			set
			{
				base.Theme = value;
			}
		}

		/// <summary>
		/// Indicates whether the bottom navigation bar will be visible.
		/// </summary>
		[DefaultValue(true)]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.NavigationBarVisible",
					"Indicates whether the bottom navigation bar will be visible.")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public bool NavigationBarVisible
		{
			get
			{
				return GetPropertyValue<bool>("NavigationBarVisible", true);
			}
			set
			{
				SetPropertyValue<bool>("NavigationBarVisible", value);
			}
		}

		/// <summary>The selected date.</summary>
		/// <returns>A <see cref="T:System.DateTime"/> that represents the selected date. </returns>
		[RefreshProperties(RefreshProperties.All)]
		[NotifyParentProperty(true)]/// 
		[C1Description("C1EventsCalendar.SelectedDate", "The selected date.")]
		[DefaultValue(typeof(DateTime), null)]
		[Bindable(true, BindingDirection.TwoWay)]
		[WidgetOption()]
		public DateTime SelectedDate
		{
			get
			{
				if (this.SelectedDates.Count > 0)
				{
					return this.SelectedDates[0];
				}
				else
				{
					DateTime now = DateTime.Now;
					return new DateTime(now.Year, now.Month, now.Day);
				}
			}
			set
			{
				if (value == null)
				{
					this.SelectedDates.Clear();
				}
				else
				{
					// For SelectedDate allow only date without time.
					// fix for 3446:
					value = new DateTime(value.Year, value.Month, value.Day);
					this.SelectedDates.SelectRange(value, value);
				}
			}
		}

		/// <summary>Collection of DateTime objects that represent the selected dates on the C1EventsCalendar control for the DayView.</summary>
		[C1Description("C1EventsCalendar.SelectedDates",
			"Collection of DateTime objects that represent the selected dates on the C1EventsCalendar control for the DayView.")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		public SelectedDatesCollection SelectedDates
		{
			get
			{
				if (this._selectedDates == null)
				{
					this._selectedDates = new SelectedDatesCollection(new ArrayList());
				}
				/*
				// Don't add current day here:
				// because
				// SelectedDates.Clear()
				// SelectedDates.Add(..)
				// will add excess date
				if (this._selectedDates.Count == 0)
				{
					DateTime now = DateTime.Now;
					this._selectedDates.Add(new DateTime(now.Year, now.Month, now.Day));                    
				}
				*/
				return this._selectedDates;
			}
		}

		/// <summary>
		/// Format of the title text for the event.
		/// Format arguments:
		///  {0} = Start, {1} = End, {2} = Subject, {3} = Location, {4} = Icons.
		/// </summary>
		[DefaultValue("{2}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.EventTitleFormat",
				"Format of the title text for the event. Format arguments: {0} = Start, {1} = End, {2} = Subject, {3} = Location, {4} = Icons.")]
		[TypeConverter(typeof(EventTitleFormatConverter))]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string EventTitleFormat
		{
			get
			{
				return GetPropertyValue<string>("EventTitleFormat", "{2}");
			}
			set
			{
				SetPropertyValue<string>("EventTitleFormat", value);
			}
		}

		/// <summary>
		/// First day of the week. This property is read-only. Use Culture property in order to change the first day of the week.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.FirstDayOfWeek",
				"First day of the week. This property is read-only. Use Culture property in order to change the first day of the week.")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		//[WidgetOption()]
		[JsonAttribute(true)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int FirstDayOfWeek
		{
			get
			{

				return (int)this.Culture.DateTimeFormat.FirstDayOfWeek;
			}
		}

		/// <summary>
		/// CultureInfo object. Each culture has different conventions 
		/// for displaying dates, time, numbers, and other 
		/// information. Neutrals cultures are not supported.
		/// </summary>
		/// <remarks>returning value can be null if Culture is not defined</remarks>
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[C1Description("C1EventsCalendar.Culture", "CultureInfo object. Each culture has different conventions for displaying dates, time, numbers, and other information. Neutrals cultures are not supported.")]
		[Bindable(false)]
		[Editor(typeof(UITypeEditor), typeof(CultureInfoConverter))]
		[DefaultValue(null)]
		[WidgetOption]
		public CultureInfo Culture
		{
			get
			{
				return GetCulture(GetPropertyValue<CultureInfo>("Culture", null));
			}
			set
			{
				if ((value != null) && value.IsNeutralCulture)
				{
					throw new ArgumentException(C1Localizer.GetString("NeutralCulture is not supported for Culture property"));
				}
				SetPropertyValue<CultureInfo>("Culture", value);
			}
		}

		/// <summary>
		/// A value that indicators the culture calendar to format the text. This property must work with Culture property.
		/// </summary>
		[C1Description("Culture.CultureCalendar")]
		[C1Category("Category.Appearance")]
		[DefaultValue("")]
		[WidgetOption]
		public string CultureCalendar
		{
			get
			{
				return GetPropertyValue<string>("CultureCalendar", "");
			}
			set
			{
				SetPropertyValue<string>("CultureCalendar", value);
			}
		}


		/// <summary>
		/// Time interval for the Day view (in minutes).
		/// </summary>
		[DefaultValue(30)]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.TimeInterval",
				"Time interval for the Day view (in minutes).")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public int TimeInterval
		{
			get
			{
				return GetPropertyValue<int>("TimeInterval", 30);
			}
			set
			{
				SetPropertyValue<int>("TimeInterval", value);
			}
		}

		/// <summary>
		/// The Day view time interval row height in pixels.
		/// </summary>
		[DefaultValue(15)]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.TimeIntervalHeight",
				"The Day view time interval row height in pixels.")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public int TimeIntervalHeight
		{
			get
			{
				return GetPropertyValue<int>("TimeIntervalHeight", 15);
			}
			set
			{
				SetPropertyValue<int>("TimeIntervalHeight", value);
			}
		}

		/// <summary>
		/// Time ruler interval for the Day view (in minutes).
		/// </summary>
		[DefaultValue(60)]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.TimeRulerInterval",
				"Time ruler interval for the Day view (in minutes).")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public int TimeRulerInterval
		{
			get
			{
				return GetPropertyValue<int>("TimeRulerInterval", 60);
			}
			set
			{
				SetPropertyValue<int>("TimeRulerInterval", value);
			}
		}

		/// <summary>
		/// Time ruler format for the Day view.
		/// Format argument:
		///  {0} = Current ruler time.
		/// </summary>
		[DefaultValue("{0:h tt}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.TimeRulerFormat",
				"Time ruler format for the Day view. Format argument: {0} = Current ruler time.")]
		[TypeConverter(typeof(TimeRulerFormatConverter))]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string TimeRulerFormat
		{
			get
			{
				return GetPropertyValue<string>("TimeRulerFormat", "{0:h tt}");
			}
			set
			{
				SetPropertyValue<string>("TimeRulerFormat", value);
			}
		}


		/// <summary>
		/// Format of the text for the day cell header in the month view. 
		/// Format argument:
		///  {0} = Day date.
		/// </summary>
		[DefaultValue("{0:d }")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DayHeaderFormat",
				"Format of the text for the day cell header in the month view. Format argument: {0} = Day date.")]
		[TypeConverter(typeof(DayHeaderFormatConverter))]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string DayHeaderFormat
		{
			get
			{
				return GetPropertyValue<string>("DayHeaderFormat", "{0:d }");
			}
			set
			{
				SetPropertyValue<string>("DayHeaderFormat", value);
			}
		}

		/// <summary>
		/// Format of the text for the first cell header in the first row of the month view.
		/// Format argument:
		///  {0} = Day date.
		/// </summary>
		[DefaultValue("{0:ddd d}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.FirstRowDayHeaderFormat",
				"Format of the text for the first cell header in the first row of the month view. Format argument: {0} = Day date.")]
		[TypeConverter(typeof(FirstRowDayHeaderFormatConverter))]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string FirstRowDayHeaderFormat
		{
			get
			{
				return GetPropertyValue<string>("FirstRowDayHeaderFormat", "{0:ddd d}");
			}
			set
			{
				SetPropertyValue<string>("FirstRowDayHeaderFormat", value);
			}
		}

		/// <summary>
		/// Array of the calendar names which need to be shown.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.VisibleCalendars",
			"Array of the calendar names which need to be shown.")]
		[TypeConverter(typeof(StringArrayConverter))]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string[] VisibleCalendars
		{
			get
			{
				return GetPropertyValue<string[]>("VisibleCalendars", new string[] { });
			}
			set
			{
				SetPropertyValue<string[]>("VisibleCalendars", value);
			}
		}

		/// <summary>
		/// Array of the default calendar names shown.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption()]
		[Browsable(false)]
		public string[] DefaultCalendars
		{
			get
			{
				return new string[] {
					C1Localizer.GetString("C1EventsCalendar.CalendarNames.Default", "Default", this.Culture),
					C1Localizer.GetString("C1EventsCalendar.CalendarNames.Home", "Home", this.Culture),
					C1Localizer.GetString("C1EventsCalendar.CalendarNames.Work", "Work", this.Culture) };
			}
		}

		/// <summary>
		/// The active view type.
		/// Format argument:
		///  {0} = Day date.
		/// </summary>
		[DefaultValue(C1EventsCalendarViewType.Day)]
		[Obsolete("Use View.IsActive property instead")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public C1EventsCalendarViewType ViewType
		{
			get
			{
				View activeView = GetActiveView();
				return activeView == null ? _viewType : activeView.Type;
			}
			set
			{
				_viewType = value;
				bool foundActiveView = false;
				foreach (var view in Views)
				{
					if (view.Type == value && !foundActiveView)
					{
						view.IsActive = true;
						foundActiveView = true;
					}
					else
					{
						view.IsActive = false;
					}
				}
			}
		}

		private View GetActiveView()
		{
			View activeView = null;
			foreach (var view in Views)
			{
				if (view.IsActive)
				{
					activeView = view;
					break;
				}
			}
			return activeView;
		}

		private C1EventsCalendarViewType GetViewTypeByString(string view)
		{
			switch (view.ToLower())
			{
				case "day":
					return C1EventsCalendarViewType.Day;
				case "week":
					return C1EventsCalendarViewType.Week;
				case "month":
					return C1EventsCalendarViewType.Month;
				case "list":
					return C1EventsCalendarViewType.List;
				default:
					return C1EventsCalendarViewType.Custom;
			}
		}

		/// <summary>
		/// The active view of C1EventCalendar.
		/// </summary>
		[DefaultValue("day")]
		[WidgetOptionName("viewType")]
		[WidgetOption]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string ActiveView
		{
			get
			{
				View activeView = GetActiveView();
				activeView = activeView ?? Views[0];
				string activeViewName = string.Empty;

				if (activeView.Type == C1EventsCalendarViewType.Custom)
				{
					activeViewName = activeView.Name;
				}
				else
				{
					activeViewName = activeView.Type.ToString().ToLower();
				}
				return string.IsNullOrEmpty(activeViewName) ? "day" : activeViewName;
			}
			set
			{
				C1EventsCalendarViewType viewType = GetViewTypeByString(value);
				bool foundActiveView = false;
				foreach (var view in Views)
				{
					if (foundActiveView)
					{
						view.IsActive = false;
						continue;
					}
					if (viewType != C1EventsCalendarViewType.Custom)
					{
						if (viewType == view.Type)
						{
							view.IsActive = true;
							foundActiveView = true;
							continue;
						}
					}
					else
					{
						if (string.Compare(value, view.Name, true) == 0)
						{
							view.IsActive = true;
							foundActiveView = true;
							continue;
						}
					}
					view.IsActive = false;
				}
				//If none of these views is active, will select first view.
				if (!foundActiveView)
				{
					Views[0].IsActive = true;
				}
			}
		}

		/// <summary>
		/// List of the views which need to be shown.
		/// Both default views("day", "week", "month", "list") and custom views can be added.
		/// If add a default view, only Type property of View should be set.
		/// If add a custom view, All the properties of View should be set.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.Views",
				"List of the view types which need to be shown.")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[MergableProperty(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[CollectionItemType(typeof(View))]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public List<View> Views
		{
			get
			{
				if (_views == null)
				{
					_views = new List<View>();
				}
				return _views;
			}
		}

		/// <summary>
		/// Format of the text for the day header in the day/week or list view. 
		/// Format argument:
		///  {0} = Day date.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DayViewHeaderFormat",
				"Format of the text for the day header in the day/week or list view. Format argument: {0} = Day date.")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[NotifyParentProperty(true)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public DayViewHeaderFormatOption DayViewHeaderFormat
		{
			get
			{
				if (_DayViewHeaderFormatOption == null)
				{
					_DayViewHeaderFormatOption = new DayViewHeaderFormatOption(this);
				}
				return _DayViewHeaderFormatOption;
			}
		}

		/// <summary>
		/// Use the Localization property in order to customize localization strings.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.Localization",
				"Use the Localization property in order to customize localization strings.")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public LocalizationOption Localization
		{
			get
			{
				if (_LocalizationOption == null)
				{
					_LocalizationOption = new LocalizationOption(this);
				}
				return _LocalizationOption;
			}
		}

		/// <summary>
		/// Use the Localization property in order to customize localization strings.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalization",
				"Use the Localization property in order to customize localization strings.")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public DatePagerLocalizationOption DatePagerLocalization
		{
			get
			{
				if (_DatePagerLocalizationOption == null)
				{
					_DatePagerLocalizationOption = new DatePagerLocalizationOption(this);
				}
				return _DatePagerLocalizationOption;
			}
		}



		#region ** client events

		/// <summary>
		/// Occurs before the built-in event dialog is shown.
		/// Return false or call event.preventDefault() in order to cancel event and
		///	prevent the built-in dialog to be shown. args.data - the event data.
		/// args.targetCell - target offset DOM element which can be used for popup callout.
		/// </summary>
		[C1Description("C1EventsCalendar.OnClientBeforeEditEventDialogShow",
			"Occurs before the built-in event dialog is shown. Return false or call event.preventDefault() in order to cancel event and prevent the built-in dialog to be shown. args.data - the event data. args.targetCell - target offset DOM element which can be used for popup callout.")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e, args")]
		[WidgetOptionName("beforeEditEventDialogShow")]
		[DefaultValue("")]
		public string OnClientBeforeEditEventDialogShow
		{
			get
			{
				return GetPropertyValue("OnClientBeforeEditEventDialogShow", "");
			}
			set
			{
				SetPropertyValue("OnClientBeforeEditEventDialogShow", value);
			}
		}

		/// <summary>
		/// Occurs when event markup is creating. 
		/// args.data - the event data. args.eventTemplate The template of events displayed on the view.
		/// </summary>
		[C1Description("C1EventsCalendar.OnClientEventCreating",
			"Occurs when event markup is creating. args.data - the event data. args.eventTemplate The template of events displayed on the view.")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e, args")]
		[WidgetOptionName("eventCreating")]
		[DefaultValue("")]
		public string OnClientEventCreating
		{
			get
			{
				return GetPropertyValue("OnClientEventCreating", "");
			}
			set
			{
				SetPropertyValue("OnClientEventCreating", value);
			}
		}

		/// <summary>
		/// Occurs when calendars option has been changed.
		/// args.calendars - the new calendars option value.
		/// </summary>
		[C1Description("C1EventsCalendar.OnClientCalendarsChanged",
			"Occurs when calendars option has been changed. args.calendars - the new calendars option value.")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e, args")]
		[WidgetOptionName("calendarsChanged")]
		[DefaultValue("")]
		public string OnClientCalendarsChanged
		{
			get
			{
				return GetPropertyValue("OnClientCalendarsChanged", "");
			}
			set
			{
				SetPropertyValue("OnClientCalendarsChanged", value);
			}
		}

		/// <summary>
		/// Occurs when events calendar is constructed and events
		/// data is loaded from an external or local data source.
		/// </summary>
		[C1Description("C1EventsCalendar.OnClientInitialized",
			"Occurs when events calendar is constructed and events data is loaded from an external or local data source.")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e")]
		[WidgetOptionName("initialized")]
		[DefaultValue("")]
		public string OnClientInitialized
		{
			get
			{
				return GetPropertyValue("OnClientInitialized", "");
			}
			set
			{
				SetPropertyValue("OnClientInitialized", value);
			}
		}

		/// <summary>
		/// Occurs when selectedDates option has been changed.
		/// args.selectedDates - the new selectedDates value.
		/// </summary>
		[C1Description("C1EventsCalendar.OnClientSelectedDatesChanged",
			"Occurs when selectedDates option has been changed. args.selectedDates - the new selectedDates value.")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e, args")]
		[WidgetOptionName("selectedDatesChanged")]
		[DefaultValue("")]
		public string OnClientSelectedDatesChanged
		{
			get
			{
				return GetPropertyValue("OnClientSelectedDatesChanged", "");
			}
			set
			{
				SetPropertyValue("OnClientSelectedDatesChanged", value);
			}
		}

		/// <summary>
		/// Occurs when viewType option has been changed.
		/// </summary>
		[C1Description("C1EventsCalendar.OnClientViewTypeChanged",
			"Occurs when viewType option has been changed.")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e, viewType")]
		[WidgetOptionName("viewTypeChanged")]
		[DefaultValue("")]
		public string OnClientViewTypeChanged
		{
			get
			{
				return GetPropertyValue("OnClientViewTypeChanged", "");
			}
			set
			{
				SetPropertyValue("OnClientViewTypeChanged", value);
			}
		}

		/// <summary>
		/// Occurs before the add event action.
		/// Return false or call event.preventDefault() in order to cancel event and
		///	prevent the save action.
		///	args.data - the new event data that should be added to a data source.
		/// </summary>
		[C1Description("C1EventsCalendar.OnClientBeforeAddEvent",
			"Occurs before the add event action. Return false or call event.preventDefault() in order to cancel event and prevent the save action. args.data - the new event data that should be added to a data source.")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e, args")]
		[WidgetOptionName("beforeAddEvent")]
		[DefaultValue("")]
		public string OnClientBeforeAddEvent
		{
			get
			{
				return GetPropertyValue("OnClientBeforeAddEvent", "");
			}
			set
			{
				SetPropertyValue("OnClientBeforeAddEvent", value);
			}
		}

		/// <summary>
		/// Occurs before the update event action.
		/// Return false or call event.preventDefault() in order to cancel event and
		///	prevent the save action.
		///	args.data - the new event data that should be updated.
		///	args.prevData - previous event data,
		///					this argument is empty for a new event.
		/// </summary>
		[C1Description("C1EventsCalendar.OnClientBeforeUpdateEvent",
			"Occurs before the save event action. Return false or call event.preventDefault() in order to cancel event and prevent the save action. args.data - the new event data that should be updated. args.prevData - previous event data, this argument is empty for a new event.")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e, args")]
		[WidgetOptionName("beforeUpdateEvent")]
		[DefaultValue("")]
		public string OnClientBeforeUpdateEvent
		{
			get
			{
				return GetPropertyValue("OnClientBeforeUpdateEvent", "");
			}
			set
			{
				SetPropertyValue("OnClientBeforeUpdateEvent", value);
			}
		}

		/// <summary>
		/// Occurs before the delete action.
		/// Return false or call event.preventDefault() in order to cancel event and
		///	prevent the delete action.
		///	args.data - the event object that should be deleted.
		/// </summary>
		[C1Description("C1EventsCalendar.OnClientBeforeDeleteEvent",
			"Occurs before the delete action. Return false or call event.preventDefault() in order to cancel event and prevent the delete action. args.data - the new event data that should be saved to a data source. args.data - the event object that should be deleted.")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e, args")]
		[WidgetOptionName("beforeDeleteEvent")]
		[DefaultValue("")]
		public string OnClientBeforeDeleteEvent
		{
			get
			{
				return GetPropertyValue("OnClientBeforeDeleteEvent", "");
			}
			set
			{
				SetPropertyValue("OnClientBeforeDeleteEvent", value);
			}
		}

		/// <summary>
		/// Occurs before the save calendar action.
		/// Return false or call event.preventDefault() in order to cancel event and
		///	prevent the save action.
		///	args.data - the new calendar data that should be saved to a data source.
		///	args.prevData - previous calendar data,
		///					this argument is empty for a new calendar.
		/// </summary>
		[C1Description("C1EventsCalendar.OnClientBeforeSaveCalendar",
			"Occurs before the save calendar action. Return false or call event.preventDefault() in order to cancel event and prevent the save action. args.data - the new calendar data that should be saved to a data source. args.prevData - previous calendar data, this argument is empty for a new calendar.")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e, args")]
		[WidgetOptionName("beforeSaveCalendar")]
		[DefaultValue("")]
		public string OnClientBeforeSaveCalendar
		{
			get
			{
				return GetPropertyValue("OnClientBeforeSaveCalendar", "");
			}
			set
			{
				SetPropertyValue("OnClientBeforeSaveCalendar", value);
			}
		}

		/// <summary>
		/// Occurs before the delete calendar action.
		/// Return false or call event.preventDefault() in order to cancel event and
		///	prevent the delete action.
		///	args.data - the calendar data that should be deleted from a data source.
		/// </summary>
		[C1Description("C1EventsCalendar.OnClientBeforeDeleteCalendar",
			"Occurs before the delete calendar action. Return false or call event.preventDefault() in order to cancel event and prevent the delete action. args.data - the calendar data that should be deleted from a data source.")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e, args")]
		[WidgetOptionName("beforeDeleteCalendar")]
		[DefaultValue("")]
		public string OnClientBeforeDeleteCalendar
		{
			get
			{
				return GetPropertyValue("OnClientBeforeDeleteCalendar", "");
			}
			set
			{
				SetPropertyValue("OnClientBeforeDeleteCalendar", value);
			}
		}

		#endregion

		#endregion

		#region ** properties

		/// <summary>
		/// C1EventsCalendar storage configuration.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("C1EventsCalendar.DataStorage", "C1EventsCalendar storage configuration.")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[EditorBrowsable(EditorBrowsableState.Always), Browsable(true)]
		[Bindable(false)]
		public C1EventsCalendarStorage DataStorage
		{
			get
			{
				if (_DataStorage == null)
				{
					_DataStorage = new C1EventsCalendarStorage(this);
					if (IsTrackingViewState)
					{
						((IStateManager)_DataStorage).TrackViewState();
					}
				}
				return _DataStorage;
			}
		}

		#endregion

		#region ** methods

		private void AddDefaultViews()
		{
			Views.Add(new View(C1EventsCalendarViewType.Day));
			Views.Add(new View(C1EventsCalendarViewType.Week));
			Views.Add(new View(C1EventsCalendarViewType.Month));
			Views.Add(new View(C1EventsCalendarViewType.List));
		}

		private void SetActiveView()
		{
			bool foundActiveView = false;
			foreach (var view in Views)
			{
				if (foundActiveView && view.IsActive)
				{
					view.IsActive = false;
				}
				if (view.IsActive)
				{
					foundActiveView = true;
				}
			}

			if (foundActiveView)
			{
				return;
			}

			foreach (var view in Views)
			{
				if (view.Type == ViewType)
				{
					view.IsActive = true;
					return;
				}
			}
			Views[0].IsActive = true;
		}

		private void AddEventToViews()
		{
			foreach (var view in Views)
			{
				view.IsActiveChanged += ViewIsActiveChanged;
			}
		}

		private void ViewIsActiveChanged(object sender, EventArgs e)
		{
			View currentView = sender as View;
			if (!currentView.IsActive)
			{
				Views[0].IsActive = true;
				return;
			}
			else
			{
				foreach (var view in Views)
				{
					if (view == currentView)
					{
						continue;
					}
					view.ShouldFireIsActiveChanged = false;
					view.IsActive = false;
					view.ShouldFireIsActiveChanged = true;
				}
			}
		}

		/// <summary>
		/// Searches the current naming container for a server control with the specified <paramref name="id"/> parameter.
		/// </summary>
		/// <param name="id">The identifier for the control to be found.</param>
		/// <returns>
		/// The specified control, or null if the specified control does not exist.
		/// </returns>
		public override Control FindControl(string id)
		{
			Control cntrl = base.FindControl(id);
			if (cntrl == null)
			{
				this.EnsureChildControls();
				return this.FindControlInternal(id, this.Controls);
			}
			return cntrl;
		}

		bool eventsDSPresent;
		bool calendarsDSPresent;
		protected override void OnInit(EventArgs e)
		{
#if !EXTENDER
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
#endif

			if (Views.Count == 0)
			{
				AddDefaultViews();
			}
			SetActiveView();
			AddEventToViews();

			base.OnInit(e);
			if (HttpContext.Current != null)
			{
				// runtime:
				object dsControl;
#if EXTENDER
				string clientId = this.TargetControlID;
				Control targetControl = this.FindControl(this.TargetControlID);
				if (targetControl != null)
				{
					clientId = targetControl.ClientID;
				}
#else
				string clientId = this.ClientID;
#endif
				eventsDSPresent = false;
				calendarsDSPresent = false;
				HttpContext.Current.Cache[clientId + "_DataStorage"] = this.DataStorage;
				if (!string.IsNullOrEmpty(this.DataStorage.EventStorage.DataSourceID))
				{
					dsControl = FindControlInternal(this.DataStorage.EventStorage.DataSourceID, this.Page.Controls);
					if (dsControl != null)
					{
						HttpContext.Current.Cache[clientId + "_EventsDataSource"] = dsControl;
						eventsDSPresent = true;
					}
				}
				if (!string.IsNullOrEmpty(this.DataStorage.CalendarStorage.DataSourceID))
				{
					dsControl = FindControlInternal(this.DataStorage.CalendarStorage.DataSourceID, this.Page.Controls);
					if (dsControl != null)
					{
						HttpContext.Current.Cache[clientId + "_CalendarsDataSource"] = dsControl;
						calendarsDSPresent = true;
					}
				}

				this.DataStorage.Initialize();
			}
		}

		/// <summary>
		/// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered in the browser window.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			Licensing.LicCheck.RenderLicenseWebComment(writer);
			this.EnsureHttpHandlerExists();
#if !EXTENDER
			this.EnsureChildControls();
#endif
			base.Render(writer);
		}

		#endregion

		#region ** private implementation

		private void EnsureHttpHandlerExists()
		{

			string siteRootPath = "";
			if (HttpContext.Current != null)
				siteRootPath = Page.Server.MapPath("~/");
			else
				siteRootPath = ResolvePhysicalPath(this, "~/");
			siteRootPath = siteRootPath.TrimEnd(Path.DirectorySeparatorChar) + Path.DirectorySeparatorChar;

#if EXTENDER
			Assembly assembly = Assembly.GetAssembly(typeof(C1EventsCalendarExtender));
			string name = "C1.Web.Wijmo.Extenders.C1EventsCalendar.Resources.c1evcalservice.ashx";
#else
			Assembly assembly = Assembly.GetAssembly(typeof(C1EventsCalendar));
			string name = "C1.Web.Wijmo.Controls.C1EventsCalendar.Resources.c1evcalservice.ashx";
#endif

			string fileName = siteRootPath + DESTINATION_WEBSERVICE_FILENAME;


			FileInfo file = new FileInfo(fileName);
			if (!file.Exists)
			{
				try
				{
					string input = string.Empty;
					using (StreamReader reader = new StreamReader(assembly.GetManifestResourceStream(name)))
					{
						input = reader.ReadToEnd();
					}
#if EXTENDER
					input = input.Replace("/*USING*/", "using C1.Web.Wijmo.Extenders;using C1.Web.Wijmo.Extenders.C1EventsCalendar;");
#else
					input = input.Replace("/*USING*/", "using C1.Web.Wijmo.Controls;using C1.Web.Wijmo.Controls.C1EventsCalendar;");
#endif
					using (StreamWriter sw = new StreamWriter(file.FullName, false))
					{
						sw.Write(input);
					}
				}
				catch (Exception)
				{
					throw new HttpException("The '" + DESTINATION_WEBSERVICE_FILENAME + "' HTTP handler not found.");
				}
			}
			else
			{
				string firstLine = string.Empty;
				string ver = "120317";
				try
				{
					using (StreamReader reader = new StreamReader(file.FullName))
					{
						firstLine = reader.ReadLine();
					}
				}
				catch (Exception)
				{

				}
				if (!firstLine.Contains(ver))
				{
					throw new HttpException("The '" + DESTINATION_WEBSERVICE_FILENAME + "' HTTP handler need to be upgraded (the new version is " +
						ver + "). Please delete " + DESTINATION_WEBSERVICE_FILENAME + " from site root or update version to " + ver + ".");
				}


			}
		}

		private string ResolveFileServerPath(string relativePath)
		{
			if (HttpContext.Current != null)
				return this.Page.Server.MapPath(relativePath); // runtime
			else if (this.Page != null)
			{

				string path = ResolvePhysicalPath(this, relativePath);
				if (path.EndsWith(Path.DirectorySeparatorChar.ToString()))
				{
					path = path.Remove(path.Length - Path.DirectorySeparatorChar.ToString().Length);
				}
				return path;
			}
			else
				return relativePath;
		}

#if EXTENDER
		internal static string ResolvePhysicalPath(System.Web.UI.Control control, string url)
		{
			System.ComponentModel.ISite site = GetSite(control);
			if (site == null) return url;

			System.Web.UI.Design.IWebApplication service = (System.Web.UI.Design.IWebApplication)site.GetService(typeof(System.Web.UI.Design.IWebApplication));
			System.Web.UI.Design.IProjectItem item = service.GetProjectItemFromUrl(url);
			if (item == null)
				return url.Replace("~/", service.RootProjectItem.PhysicalPath);

			string path = item.PhysicalPath;
			return path;
		}

		private static ISite GetSite(System.Web.UI.Control webControl)
		{
			if (webControl.Site != null) return webControl.Site;
			for (System.Web.UI.Control control = webControl.Parent; control != null; control = control.Parent)
			{
				if (control.Site != null) return control.Site;
			}
			return null;
		}
#endif

		private Control FindControlInternal(string id, ControlCollection controls)
		{
			if (controls == null)
				controls = this.Controls;

			foreach (Control ctrl in controls)
			{
				if (ctrl.ID == id)
					return ctrl;

				if (ctrl.Controls.Count > 0)
				{
					Control child = this.FindControlInternal(id, ctrl.Controls);
					if (child != null)
						return child;
				}
			}
			return null;
		}


		#endregion

	}

	/// <summary>
	/// Provides a type converter to convert an event title format to and from other representations.
	/// </summary>
	public class EventTitleFormatConverter : StringListConverter
	{
		public EventTitleFormatConverter()
		{

			SetList(false, new string[]
						{
							"{2}",
							"{0:h:mmtt}-{1:h:mmtt} {4} {2}",
							"{4} {2} {3}"
						});

		}
	}

	/// <summary>
	/// Provides a type converter to convert time ruler format to and from other representations.
	/// </summary>
	public class TimeRulerFormatConverter : StringListConverter
	{
		public TimeRulerFormatConverter()
		{

			SetList(false, new string[]
						{							
							"{0:h tt}",
							"{0:h:mmtt}",
							"{0:HH}",
							"{0:HH:mm}"
						});

		}
	}

	/// <summary>
	/// Provides a type converter to convert day header format to and from other representations.
	/// </summary>
	public class DayHeaderFormatConverter : StringListConverter
	{
		public DayHeaderFormatConverter()
		{

			SetList(false, new string[]
						{							
							"{0:d }"
						});

		}
	}

	/// <summary>
	/// Provides a type converter to convert first row day header format to and from other representations.
	/// </summary>
	public class FirstRowDayHeaderFormatConverter : StringListConverter
	{
		public FirstRowDayHeaderFormatConverter()
		{

			SetList(false, new string[]
						{							
							"{0:ddd d}"
						});

		}
	}


	//[DefaultValue()]

	/// <summary>
	/// Specifies the type of the calendar view.
	/// </summary>
	public enum C1EventsCalendarViewType
	{

		/// <summary>
		/// Day
		/// </summary>
		Day = 0,

		/// <summary>
		/// Week
		/// </summary>
		Week = 1,

		/// <summary>
		/// Month
		/// </summary>
		Month = 2,

		/// <summary>
		/// List
		/// </summary>
		List = 3,

		/// <summary>
		/// Custom
		/// </summary>
		Custom = 4
	}

	/// <summary>
	/// Specifies the unit of the calendar custom view.
	/// </summary>
	public enum C1EventsCalendarCustomViewUnit
	{
		/// <summary>
		/// Day
		/// </summary>
		Day = 0,

		/// <summary>
		/// Week
		/// </summary>
		Week = 1,

		/// <summary>
		/// Month
		/// </summary>
		Month = 2,

		/// <summary>
		/// Year
		/// </summary>
		Year = 3
	}

	#region ** View class

	/// <summary>
	/// The view can be added to C1EventsCalendara's Views.
	/// </summary>
	public class View : Settings, IJsonEmptiable, ICustomOptionType
	{
		private bool _isActive;
		private bool _shouldFireIsActiveChanged = true;

		/// <summary>
		/// Initializes a new instance of the <see cref="View"/> class.
		/// </summary>
		public View(C1EventsCalendarViewType type)
			:this(type, false)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="View"/> class.
		/// </summary>
		public View(C1EventsCalendarViewType type, bool isActive)
		{
			Type = type;
			IsActive = isActive;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="View"/> class.
		/// </summary>
		public View()
		{
		}

		internal event EventHandler IsActiveChanged;

		private void OnIsActiveChanged()
		{
			if (IsActiveChanged != null)
			{
				IsActiveChanged(this, null);
			}
		}

		internal bool ShouldFireIsActiveChanged
		{
			get
			{
				return _shouldFireIsActiveChanged;
			}
			set
			{
				_shouldFireIsActiveChanged = value;
			}
		}

		/// <summary>
		/// Determine if this view is active.
		/// </summary>
		[DefaultValue(false)]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.View.IsActive",
			"Determine if this view is active.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public bool IsActive
		{
			get
			{
				return GetPropertyValue<bool>("IsActive", false);
			}
			set
			{
				if (_isActive != value)
				{
					SetPropertyValue<bool>("IsActive", value);
					if (ShouldFireIsActiveChanged)
					{
						OnIsActiveChanged();
					}
					_isActive = value;
				}
			}
		}

		/// <summary>
		/// The type of view.
		/// If it is a custom view, it should be set to C1EventsCalendarViewType.Custom.
		/// </summary>
		[DefaultValue(C1EventsCalendarViewType.Day)]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.View.Type",
			"The type of view")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public C1EventsCalendarViewType Type
		{
			get
			{
				return GetPropertyValue<C1EventsCalendarViewType>("Type", C1EventsCalendarViewType.Day);
			}
			set
			{
				SetPropertyValue<C1EventsCalendarViewType>("Type", value);
			}
		}

		/// <summary>
		/// The unique name of custom view which is displayed on the toolbar.
		/// It is effective only when the view is custom view.
		/// </summary>
		/// <remarks>
		/// Name cannot be set to "day" / "week" / "month" / "list".
		/// They are key words for custom view.
		/// </remarks>
		[DefaultValue("")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.View.Name",
			"The name of custom view which is displayed on the toolbar.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string Name
		{
			get
			{
				if (Type == C1EventsCalendarViewType.Custom)
				{
					return GetPropertyValue<string>("Name", C1Localizer.GetString("C1EventsCalendar.View.Name.Default"));
				}
				else
				{
					return GetPropertyValue<string>("Name", "");
				}
			}
			set
			{
				SetPropertyValue<string>("Name", value);
			}
		}

		/// <summary>
		/// The time unit of custom view. Possible values are day, week, month, year.
		/// It is effective only when the view is custom view.
		/// </summary>
		[DefaultValue(C1EventsCalendarCustomViewUnit.Day)]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.View.Unit",
			"The time unit of custom view.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public C1EventsCalendarCustomViewUnit Unit
		{
			get
			{
				return GetPropertyValue<C1EventsCalendarCustomViewUnit>("Unit", C1EventsCalendarCustomViewUnit.Day);
			}
			set
			{
				SetPropertyValue<C1EventsCalendarCustomViewUnit>("Unit", value);
			}
		}

		/// <summary>
		/// The count of time span, depends on the unit.
		/// It is effective only when the view is custom view.
		/// </summary>
		[DefaultValue(1)]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.View.Count",
			"The count of time span, depends on the unit.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public int Count
		{
			get
			{
				return GetPropertyValue<int>("Count", 1);
			}
			set
			{
				SetPropertyValue<int>("Count", value);
			}
		}

		string ICustomOptionType.SerializeValue()
		{
			string serializedString = string.Empty;
			switch (Type)
			{
				case C1EventsCalendarViewType.Day:
					serializedString = "day";
					break;
				case C1EventsCalendarViewType.Week:
					serializedString = "week";
					break;
				case C1EventsCalendarViewType.Month:
					serializedString = "month";
					break;
				case C1EventsCalendarViewType.List:
					serializedString = "list";
					break;
				case C1EventsCalendarViewType.Custom:
					serializedString = "{" + string.Format("name:\"{0}\", unit:\"{1}\", count:{2}", Name, Unit.ToString().ToLower(), Count) + "}";
					break;
			}
			return serializedString;
		}

#if !EXTENDER
		void ICustomOptionType.DeSerializeValue(object state)
		{
			var viewType = state as string;
			if (viewType != null)
			{
				Type = (C1EventsCalendarViewType)Enum.Parse(typeof(C1EventsCalendarViewType), viewType, true);
				return;
			}
			var ht = state as Hashtable;
			if (ht != null)
			{
				Type = C1EventsCalendarViewType.Custom;
				if (ht["name"] != null)
				{
					Name = Convert.ToString(ht["name"]);
				}
				if (ht["unit"] != null)
				{
					Unit = (C1EventsCalendarCustomViewUnit)Enum.Parse(typeof(C1EventsCalendarCustomViewUnit), Convert.ToString(ht["unit"]), true);
				}
				if (ht["count"] != null)
				{
					Count = Convert.ToInt32(ht["count"]);
				}
			}
		}
#endif

		bool ICustomOptionType.IncludeQuotes
		{
			get
			{
				if (Type == C1EventsCalendarViewType.Custom)
				{
					return false;
				}
				return true;
			}
		}

		bool IJsonEmptiable.IsEmpty
		{
			get { return false; }
		}
	}

	#endregion ** CustomViewOptions class

	#region ** DayViewHeaderFormatOption class

	/// <summary>
	/// Format of the text for the day header in the day/week or list view.
	/// </summary>
	public class DayViewHeaderFormatOption : Settings, IJsonEmptiable
	{

#if !EXTENDER
		C1EventsCalendar _evCal;
#else
		C1EventsCalendarExtender _evCal;
#endif

		/// <summary>
		/// Initializes a new instance of the <see cref="DayViewHeaderFormatOption"/> class.
		/// </summary>
#if !EXTENDER
		public DayViewHeaderFormatOption(C1EventsCalendar evCal)
#else
		public DayViewHeaderFormatOption(C1EventsCalendarExtender evCal)
#endif
		{
			_evCal = evCal;
		}

		/// <summary>
		/// Format of the text for the day header in the day view.
		/// </summary>
		[DefaultValue("all-day events")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DayViewHeaderFormatOption.Day",
			"Format of the text for the day header in the day view.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string Day
		{
			get
			{
				return GetPropertyValue<string>("Day",
C1Localizer.GetString("C1EventsCalendar.Localization.labelAllDayEvents",
					"all-day events",
					_evCal.Culture)
					);
			}
			set
			{
				SetPropertyValue<string>("Day", value);
			}
		}

		/// <summary>
		/// Format of the text for the day header in the week view.
		/// </summary>
		[DefaultValue("{0:d dddd}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DayViewHeaderFormatOption.Week",
			"Format of the text for the day header in the week view.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string Week
		{
			get
			{
				return GetPropertyValue<string>("Week", "{0:d dddd}");
			}
			set
			{
				SetPropertyValue<string>("Week", value);
			}
		}

		/// <summary>
		/// Format of the text for the day header in the list view.
		/// </summary>
		[DefaultValue("{0:d dddd}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DayViewHeaderFormatOption.List",
			"Format of the text for the day header in the list view.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string List
		{
			get
			{
				return GetPropertyValue<string>("List", "{0:d dddd}");
			}
			set
			{
				SetPropertyValue<string>("List", value);
			}
		}

		/// <summary>
		/// Format of the text for the custom header in the custom view.
		/// </summary>
		[DefaultValue("{0:d dddd}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DayViewHeaderFormatOption.Custom",
			"Format of the text for the custom header in the custom view.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string Custom
		{
			get
			{
				return GetPropertyValue<string>("Custom", "{0:d dddd}");
			}
			set
			{
				SetPropertyValue<string>("Custom", value);
			}
		}

		#region ** implement ICustomOptionType interface

		/*

		/// <summary>
		///Serialize the animated value.
		/// </summary>
		/// <returns></returns>
		string ICustomOptionType.SerializeValue()
		{
			if (this.Disabled)
			{
				return "false";
			}
			else
			{
				return this.Effect;
			}
		}

#if !EXTENDER
		//TODO:
		void ICustomOptionType.DeSerializeValue(object state)
		{
			if (state is bool)
			{
				//this.Disabled = (bool)state;
			}
			else if (state is string)
			{
				//this.Effect = (string)state;
			}
		}
#endif

		bool ICustomOptionType.IncludeQuotes
		{
			get
			{
				if (this.Disabled)
				{
					return false;
				}

				return true;
			}
		}
		 
		*/
		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return base.ToString();
		}

		internal bool ShouldSerialize()
		{
			return true;// Disabled || Effect != "slide";
		}

		#endregion end of ** IJsonEmptiable interface implementation


		bool IJsonEmptiable.IsEmpty
		{
			get { return !this.ShouldSerialize(); }
		}
	}

	#endregion end of ** DayViewHeaderFormatOption class

	#region ** LocalizationOption class

	/// <summary>
	/// Localization strings.
	/// </summary>
	public class LocalizationOption : Settings, IJsonEmptiable
	{
#if !EXTENDER
		C1EventsCalendar _evCal;
#else
		C1EventsCalendarExtender _evCal;
#endif
		/// <summary>
		/// Initializes a new instance of the <see cref="LocalizationOption"/> class.
		/// </summary>
#if !EXTENDER
		public LocalizationOption(C1EventsCalendar evCal)
#else
		public LocalizationOption(C1EventsCalendarExtender evCal)
#endif
		{
			_evCal = evCal;
		}

		#region ** localization string properties

		/// <summary>
		/// messageEndOccursBeforeStart string.
		/// </summary>
		[DefaultValue("The end date you entered occurs before the start date.")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.messageEndOccursBeforeStart",
			"The end date you entered occurs before the start date.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string messageEndOccursBeforeStart
		{
			get
			{

				return GetPropertyValue<string>("messageEndOccursBeforeStart",
					C1Localizer.GetString("C1EventsCalendar.Localization.messageEndOccursBeforeStart",
					"The end date you entered occurs before the start date.",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("messageEndOccursBeforeStart", value);
			}
		}
		//	messageEndOccursBeforeStart: "The end date you entered occurs before the start date."

		/// <summary>
		/// weekViewHeaderFormat2Months string.
		/// </summary>
		[DefaultValue("{0:MMMM} - {1:MMMM yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.weekViewHeaderFormat2Months",
			"Date format for the header of week view between 2 months.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string weekViewHeaderFormat2Months
		{
			get
			{

				return GetPropertyValue<string>("weekViewHeaderFormat2Months",
					C1Localizer.GetString("C1EventsCalendar.Localization.weekViewHeaderFormat2Months",
					"{0:MMMM} - {1:MMMM yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("weekViewHeaderFormat2Months", value);
			}
		}
		//	weekViewHeaderFormat2Months: "{0:MMMM} - {1:MMMM yyyy}"

		/// <summary>
		/// weekViewHeaderFormat string.
		/// </summary>
		[DefaultValue("{0:MMMM yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.weekViewHeaderFormat",
			"Date format for the header of week view.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string weekViewHeaderFormat
		{
			get
			{

				return GetPropertyValue<string>("weekViewHeaderFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.weekViewHeaderFormat",
					"{0:MMMM yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("weekViewHeaderFormat", value);
			}
		}
		//	weekViewHeaderFormat: "{0:MMMM yyyy}"

		/// <summary>
		/// monthViewHeaderFormat string.
		/// </summary>
		[DefaultValue("{0:MMMM yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.monthViewHeaderFormat",
			"Date format for the header of month view.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string monthViewHeaderFormat
		{
			get
			{

				return GetPropertyValue<string>("monthViewHeaderFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.monthViewHeaderFormat",
					"{0:MMMM yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("monthViewHeaderFormat", value);
			}
		}
		//	monthViewHeaderFormat: "{0:MMMM yyyy}"

		/// <summary>
		/// customSingleWeekViewHeaderFormat string.
		/// </summary>
		[DefaultValue("{0:d}")]
		[C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
		[C1Description("C1EventsCalendar.LocalizationOption.customSingleWeekViewHeaderFormat",
			"Date format for the header of custom week view with single week.")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customSingleWeekViewHeaderFormat
		{
			get
			{

				return GetPropertyValue<string>("customSingleWeekViewHeaderFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.customSingleWeekViewHeaderFormat",
					"{0:d}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customSingleWeekViewHeaderFormat", value);
			}
		}
		//	customSingleWeekViewHeaderFormat: "{0:d}"

		/// <summary>
		/// customWeekViewHeaderFormat string.
		/// </summary>
		[DefaultValue("{0:d} - {1:d}")]
		[C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
		[C1Description("C1EventsCalendar.LocalizationOption.customWeekViewHeaderFormat",
			"Date format for the header of custom week view with multiple weeks.")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customWeekViewHeaderFormat
		{
			get
			{

				return GetPropertyValue<string>("customWeekViewHeaderFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.customWeekViewHeaderFormat",
					"{0:d} - {1:d}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customWeekViewHeaderFormat", value);
			}
		}
		//	customWeekViewHeaderFormat: "{0:d} - {1:d}"

		/// <summary>
		/// customYearViewSecondTitleFormat string.
		/// </summary>
		[DefaultValue("{0:yyyy}")]
		[C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
		[C1Description("C1EventsCalendar.LocalizationOption.customYearViewSecondTitleFormat",
			"Date format for the secondary header of custom year view.")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customYearViewSecondTitleFormat
		{
			get
			{

				return GetPropertyValue<string>("customYearViewSecondTitleFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.customYearViewSecondTitleFormat",
					"{0:yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customYearViewSecondTitleFormat", value);
			}
		}
		//	customYearViewSecondTitleFormat: "{0:yyyy}"

		/// <summary>
		/// customMonthViewSecondTitleFormat string.
		/// </summary>
		[DefaultValue("{0:MMM}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.customMonthViewSecondTitleFormat",
			"Date format for the secondary header of custom month view.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customMonthViewSecondTitleFormat
		{
			get
			{

				return GetPropertyValue<string>("customMonthViewSecondTitleFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.customMonthViewSecondTitleFormat",
					"{0:MMM}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customMonthViewSecondTitleFormat", value);
			}
		}
		//	customMonthViewSecondTitleFormat: "{0:MMM}"

		/// <summary>
		/// customSingleMonthViewHeaderFormat string.
		/// </summary>
		[DefaultValue("{0:MMMM yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.customSingleMonthViewHeaderFormat",
			"Date format for the header of custom month view with single month.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customSingleMonthViewHeaderFormat
		{
			get
			{

				return GetPropertyValue<string>("customSingleMonthViewHeaderFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.customSingleMonthViewHeaderFormat",
					"{0:MMMM yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customSingleMonthViewHeaderFormat", value);
			}
		}
		//	customSingleMonthViewHeaderFormat: "{0:MMMM yyyy}"

		/// <summary>
		/// customMonthViewHeaderFormat string.
		/// </summary>
		[DefaultValue("{0:MMMM yyyy} - {1:MMMM yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.customMonthViewHeaderFormat",
			"Date format for the header of custom month view with multiple months.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customMonthViewHeaderFormat
		{
			get
			{

				return GetPropertyValue<string>("customMonthViewHeaderFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.customMonthViewHeaderFormat",
					"{0:MMMM yyyy} - {1:MMMM yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customMonthViewHeaderFormat", value);
			}
		}
		//	customMonthViewHeaderFormat: "{0:MMMM yyyy} - {1:MMMM yyyy}"

		/// <summary>
		/// customSingleYearViewHeaderFormat string.
		/// </summary>
		[DefaultValue("{0:yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.customSingleYearViewHeaderFormat",
			"Date format for the header of custom year view with single year.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customSingleYearViewHeaderFormat
		{
			get
			{

				return GetPropertyValue<string>("customSingleYearViewHeaderFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.customSingleYearViewHeaderFormat",
					"{0:yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customSingleYearViewHeaderFormat", value);
			}
		}
		//	customSingleYearViewHeaderFormat: "{0:yyyy}"

		/// <summary>
		/// customYearViewHeaderFormat string.
		/// </summary>
		[DefaultValue("{0:yyyy} - {1:yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.customYearViewHeaderFormat",
			"Date format for the header of custom year view with multiple year.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customYearViewHeaderFormat
		{
			get
			{

				return GetPropertyValue<string>("customYearViewHeaderFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.customYearViewHeaderFormat",
					"{0:yyyy} - {1:yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customYearViewHeaderFormat", value);
			}
		}
		//	customYearViewHeaderFormat: "{0:yyyy} - {1:yyyy}"

		/// <summary>
		/// customYearViewMonthFormat string.
		/// </summary>
		[DefaultValue("{0:MMM}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.customYearViewMonthFormat",
			"Date format for the month cell of custom year view.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customYearViewMonthFormat
		{
			get
			{

				return GetPropertyValue<string>("customYearViewMonthFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.customYearViewMonthFormat",
					"{0:MMM}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customYearViewMonthFormat", value);
			}
		}
		//	customYearViewMonthFormat: "{0:MMM}"

		/// <summary>
		/// agendaHeaderFullDateFormat string.
		/// </summary>
		[DefaultValue("{0:MMMM d, yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.agendaHeaderFullDateFormat",
			"Date format for the agenda header.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string agendaHeaderFullDateFormat
		{
			get
			{

				return GetPropertyValue<string>("agendaHeaderFullDateFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.agendaHeaderFullDateFormat",
					"{0:MMMM d, yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("agendaHeaderFullDateFormat", value);
			}
		}
		//	agendaHeaderFullDateFormat: "{0:MMMM d, yyyy}"

		/// <summary>
		/// dayDetailsLabelFulldateFormat string.
		/// </summary>
		[DefaultValue("{0:dddd, MMMM d}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.dayDetailsLabelFulldateFormat",
			"Date format for the day details label of full date.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string dayDetailsLabelFulldateFormat
		{
			get
			{

				return GetPropertyValue<string>("dayDetailsLabelFulldateFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.dayDetailsLabelFulldateFormat",
					"{0:dddd, MMMM d}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("dayDetailsLabelFulldateFormat", value);
			}
		}
		//	dayDetailsLabelFulldateFormat: "{0:dddd, MMMM d}"

		/// <summary>
		/// dayDetailsLabelYearFormat string.
		/// </summary>
		[DefaultValue("{0:yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.dayDetailsLabelYearFormat",
			"Date format for the day details label of year.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string dayDetailsLabelYearFormat
		{
			get
			{

				return GetPropertyValue<string>("dayDetailsLabelYearFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.dayDetailsLabelYearFormat",
					"{0:yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("dayDetailsLabelYearFormat", value);
			}
		}
		//	dayDetailsLabelYearFormat: "{0:yyyy}"

		/// <summary>
		/// calendarTitleFormat string.
		/// </summary>
		[DefaultValue("MMMM yyyy")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.calendarTitleFormat",
			"Date format for the calendar title.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string calendarTitleFormat
		{
			get
			{

				return GetPropertyValue<string>("calendarTitleFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.calendarTitleFormat",
					"MMMM yyyy",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("calendarTitleFormat", value);
			}
		}
		//	calendarTitleFormat: "MMMM yyyy"

		/// <summary>
		/// calendarToolTipFormat string.
		/// </summary>
		[DefaultValue("dddd, MMMM dd, yyyy")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.calendarToolTipFormat",
			"Date format for the calendar tootip.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string calendarToolTipFormat
		{
			get
			{

				return GetPropertyValue<string>("calendarToolTipFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.calendarToolTipFormat",
					"dddd, MMMM dd, yyyy",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("calendarToolTipFormat", value);
			}
		}
		//	calendarToolTipFormat: "dddd, MMMM dd, yyyy"

		/// <summary>
		/// titleEditCalendar string.
		/// </summary>
		[DefaultValue("Edit calendar")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.titleEditCalendar",
			"Text for Edit calendar.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string titleEditCalendar
		{
			get
			{

				return GetPropertyValue<string>("titleEditCalendar",
					C1Localizer.GetString("C1EventsCalendar.Localization.titleEditCalendar",
					"Edit calendar",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("titleEditCalendar", value);
			}
		}
		//	titleEditCalendar: "Edit calendar"

		/// <summary>
		/// logCouldntOpenLocalStorage string.
		/// </summary>
		[DefaultValue("Couldn't open built-in local data storage. Please, add amplify.store references.")]
		[C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
		[C1Description("C1EventsCalendar.LocalizationOption.logCouldntOpenLocalStorage",
			"Text for couldn't open built-in local data storage.")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string logCouldntOpenLocalStorage
		{
			get
			{

				return GetPropertyValue<string>("logCouldntOpenLocalStorage",
					C1Localizer.GetString("C1EventsCalendar.Localization.logCouldntOpenLocalStorage",
					"Couldn't open built-in local data storage. Please, add amplify.store references.",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("logCouldntOpenLocalStorage", value);
			}
		}
		//	logCouldntOpenLocalStorage: "Couldn't open built-in local data storage. Please, add amplify.store references."

		/// <summary>
		/// buttonSave string.
		/// </summary>
		[DefaultValue("Save")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.buttonSave",
			"Text for save button.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string buttonSave
		{
			get
			{

				return GetPropertyValue<string>("buttonSave",
					C1Localizer.GetString("C1EventsCalendar.Localization.buttonSave",
					"Save",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("buttonSave", value);
			}
		}
		//	buttonSave: "Save"

		/// <summary>
		/// buttonClearAll string.
		/// </summary>
		[DefaultValue("Clear All")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.buttonClearAll",
			"Text for clear all button.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string buttonClearAll
		{
			get
			{

				return GetPropertyValue<string>("buttonClearAll",
					C1Localizer.GetString("C1EventsCalendar.Localization.buttonClearAll",
					"Clear All",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("buttonClearAll", value);
			}
		}
		//	buttonClearAll: "Clear All"

		/// <summary>
		/// buttonClose string.
		/// </summary>
		[DefaultValue("Close")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.buttonClose",
			"Text for close button.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string buttonClose
		{
			get
			{

				return GetPropertyValue<string>("buttonClose",
					C1Localizer.GetString("C1EventsCalendar.Localization.buttonClose",
					"Close",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("buttonClose", value);
			}
		}
		//	buttonClose: "Close"

		/// <summary>
		/// labelColor string.
		/// </summary>
		[DefaultValue("Color")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.labelColor",
			"Text for color button.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string labelColor
		{
			get
			{

				return GetPropertyValue<string>("labelColor",
					C1Localizer.GetString("C1EventsCalendar.Localization.labelColor",
					"Color",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("labelColor", value);
			}
		}
		//	labelColor: "Color"

		/// <summary>
		/// labelCalendarName string.
		/// </summary>
		[DefaultValue("Calendar name")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.labelCalendarName",
			"Text for calendar name.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string labelCalendarName
		{
			get
			{

				return GetPropertyValue<string>("labelCalendarName",
					C1Localizer.GetString("C1EventsCalendar.Localization.labelCalendarName",
					"Calendar name",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("labelCalendarName", value);
			}
		}
		//	labelCalendarName: "Calendar name"

		/// <summary>
		/// calendarNextTooltip string.
		/// </summary>
		[DefaultValue("Next")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.calendarNextTooltip",
			"Text for calendar next button's tooltip.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string calendarNextTooltip
		{
			get
			{

				return GetPropertyValue<string>("calendarNextTooltip",
					C1Localizer.GetString("C1EventsCalendar.Localization.calendarNextTooltip",
					"Next",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("calendarNextTooltip", value);
			}
		}
		//	calendarNextTooltip: "Next"

		/// <summary>
		/// calendarPrevTooltip string.
		/// </summary>
		[DefaultValue("Previous")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.calendarPrevTooltip",
			"Text for calendar previous button's tooltip.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string calendarPrevTooltip
		{
			get
			{

				return GetPropertyValue<string>("calendarPrevTooltip",
					C1Localizer.GetString("C1EventsCalendar.Localization.calendarPrevTooltip",
					"Previous",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("calendarPrevTooltip", value);
			}
		}
		//	calendarPrevTooltip: "Previous"

		/// <summary>
		/// navigatorBarNextTooltip string.
		/// </summary>
		[DefaultValue("right")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.navigatorBarNextTooltip",
			"Text for calendar next button's tooltip of navigator bar.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string navigatorBarNextTooltip
		{
			get
			{

				return GetPropertyValue<string>("navigatorBarNextTooltip",
					C1Localizer.GetString("C1EventsCalendar.Localization.navigatorBarNextTooltip",
					"right",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("navigatorBarNextTooltip", value);
			}
		}
		//	navigatorBarNextTooltip: "right"

		/// <summary>
		/// navigatorBarPrevTooltip string.
		/// </summary>
		[DefaultValue("left")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.navigatorBarPrevTooltip",
			"Text for calendar previous button's tooltip of navigator bar.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string navigatorBarPrevTooltip
		{
			get
			{

				return GetPropertyValue<string>("navigatorBarPrevTooltip",
					C1Localizer.GetString("C1EventsCalendar.Localization.navigatorBarPrevTooltip",
					"left",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("navigatorBarPrevTooltip", value);
			}
		}
		//	navigatorBarPrevTooltip: "left"

		/// <summary>
		/// activityLoading string.
		/// </summary>
		[DefaultValue("Loading...")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.activityLoading",
			"Prompt string for loading.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityLoading
		{
			get
			{

				return GetPropertyValue<string>("activityLoading",
					C1Localizer.GetString("C1EventsCalendar.Localization.activityLoading",
					"Loading...",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityLoading", value);
			}
		}
		//	activityLoading: "Loading..."

		/// <summary>
		/// activityDeletingCalendar string.
		/// </summary>
		[DefaultValue("Deleting calendar...")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.activityDeletingCalendar",
			"Prompt string for deleting calendar.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityDeletingCalendar
		{
			get
			{

				return GetPropertyValue<string>("activityDeletingCalendar",
					C1Localizer.GetString("C1EventsCalendar.Localization.activityDeletingCalendar",
					"Deleting calendar...",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityDeletingCalendar", value);
			}
		}
		//	activityDeletingCalendar: "Deleting calendar..."

		/// <summary>
		/// activityCreatingCalendar string.
		/// </summary>
		[DefaultValue("Creating calendar...")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.activityCreatingCalendar",
			"Prompt string for creating calendar.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityCreatingCalendar
		{
			get
			{

				return GetPropertyValue<string>("activityCreatingCalendar",
					C1Localizer.GetString("C1EventsCalendar.Localization.activityCreatingCalendar",
					"Creating calendar...",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityCreatingCalendar", value);
			}
		}
		//	activityCreatingCalendar: "Creating calendar..."

		/// <summary>
		/// activityUpdatingCalendar string.
		/// </summary>
		[DefaultValue("Updating calendar...")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.activityUpdatingCalendar",
			"Prompt string for updating calendar.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityUpdatingCalendar
		{
			get
			{

				return GetPropertyValue<string>("activityUpdatingCalendar",
					C1Localizer.GetString("C1EventsCalendar.Localization.activityUpdatingCalendar",
					"Updating calendar...",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityUpdatingCalendar", value);
			}
		}
		//	activityUpdatingCalendar: "Updating calendar..."

		/// <summary>
		/// activityCreatingEvent string.
		/// </summary>
		[DefaultValue("Creating event...")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.activityCreatingEvent",
			"Prompt string for creating event.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityCreatingEvent
		{
			get
			{

				return GetPropertyValue<string>("activityCreatingEvent",
					C1Localizer.GetString("C1EventsCalendar.Localization.activityCreatingEvent",
					"Creating event...",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityCreatingEvent", value);
			}
		}
		//	activityCreatingEvent: "Creating event..."	

		/// <summary>
		/// activityUpdatingEvent string.
		/// </summary>
		[DefaultValue("Updating event...")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.activityUpdatingEvent",
			"Prompt string for updating event.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityUpdatingEvent
		{
			get
			{

				return GetPropertyValue<string>("activityUpdatingEvent",
					C1Localizer.GetString("C1EventsCalendar.Localization.activityUpdatingEvent",
					"Updating event...",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityUpdatingEvent", value);
			}
		}
		//	activityUpdatingEvent: "Updating event..."	

		/// <summary>
		/// activityDeletingEvent string.
		/// </summary>
		[DefaultValue("Deleting event...")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.activityDeletingEvent",
			"Prompt string for deleting event.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityDeletingEvent
		{
			get
			{

				return GetPropertyValue<string>("activityDeletingEvent",
					C1Localizer.GetString("C1EventsCalendar.Localization.activityDeletingEvent",
					"Deleting event...",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityDeletingEvent", value);
			}
		}
		//	activityDeletingEvent: "Deleting event..."	

		/// <summary>
		/// activityUpdating string.
		/// </summary>
		[DefaultValue("Updating...")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.activityUpdating",
			"Prompt string for updating.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string activityUpdating
		{
			get
			{

				return GetPropertyValue<string>("activityUpdating",
					C1Localizer.GetString("C1EventsCalendar.Localization.activityUpdating",
					"Updating...",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("activityUpdating", value);
			}
		}
		//	activityUpdating: "Updating..."	

		/// <summary>
		/// agendaLoadingMoreEvents string.
		/// </summary>
		[DefaultValue("Loading more events...")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.agendaLoadingMoreEvents",
			"Prompt string for agenda loading for more events.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string agendaLoadingMoreEvents
		{
			get
			{

				return GetPropertyValue<string>("agendaLoadingMoreEvents",
					C1Localizer.GetString("C1EventsCalendar.Localization.agendaLoadingMoreEvents",
					"Loading more events...",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("agendaLoadingMoreEvents", value);
			}
		}
		//	agendaLoadingMoreEvents: "Loading more events..."

		/// <summary>
		/// agendaMoreEventsFormat string.
		/// </summary>
		[DefaultValue("More events ({0})...")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.agendaMoreEventsFormat",
			"Text for more events of agenda.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string agendaMoreEventsFormat
		{
			get
			{

				return GetPropertyValue<string>("agendaMoreEventsFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.agendaMoreEventsFormat",
					"More events ({0})...",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("agendaMoreEventsFormat", value);
			}
		}
		//	agendaMoreEventsFormat: "More events ({0})..."	// (0) - Number, invisible events count

		/// <summary>
		/// monthCellMoreEventsFormat string.
		/// </summary>
		[DefaultValue("{0}  more...")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.monthCellMoreEventsFormat",
			"Text for more events of month cell.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string monthCellMoreEventsFormat
		{
			get
			{

				return GetPropertyValue<string>("monthCellMoreEventsFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.monthCellMoreEventsFormat",
					"{0}  more...",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("monthCellMoreEventsFormat", value);
			}
		}
		//	monthCellMoreEventsFormat: "{0}  more..."	// (0) - Number, invisible events count


		/// <summary>
		/// agendaTimeFormat string.
		/// </summary>
		[DefaultValue("{0:hh:mm tt} to {1:hh:mm tt}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.agendaTimeFormat",
			"Date format of agenda time.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string agendaTimeFormat
		{
			get
			{

				return GetPropertyValue<string>("agendaTimeFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.agendaTimeFormat",
					"{0:hh:mm tt} to {1:hh:mm tt}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("agendaTimeFormat", value);
			}
		}
		//agendaTimeFormat: "{0:hh:mm tt} - {1:hh:mm tt}",

		/// <summary>
		/// buttonToday string.
		/// </summary>
		[DefaultValue("today")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.buttonToday",
			"Text for today button.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string buttonToday
		{
			get
			{

				return GetPropertyValue<string>("buttonToday",
					C1Localizer.GetString("C1EventsCalendar.Localization.buttonToday",
					"today",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("buttonToday", value);
			}
		}
		//	buttonToday: "today"

		/// <summary>
		/// buttonDayView string.
		/// </summary>
		[DefaultValue("Day")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.buttonDayView",
			"Text for day view button.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string buttonDayView
		{
			get
			{

				return GetPropertyValue<string>("buttonDayView",
					C1Localizer.GetString("C1EventsCalendar.Localization.buttonDayView",
					"Day",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("buttonDayView", value);
			}
		}
		//	buttonDayView: "Day"	

		/// <summary>
		/// buttonWeekView string.
		/// </summary>
		[DefaultValue("Week")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.buttonWeekView",
			"Text for week view button.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string buttonWeekView
		{
			get
			{

				return GetPropertyValue<string>("buttonWeekView",
					C1Localizer.GetString("C1EventsCalendar.Localization.buttonWeekView",
					"Week",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("buttonWeekView", value);
			}
		}
		//	buttonWeekView: "Week"	

		/// <summary>
		/// buttonMonthView string.
		/// </summary>
		[DefaultValue("Month")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.buttonMonthView",
			"Text for month view button.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string buttonMonthView
		{
			get
			{

				return GetPropertyValue<string>("buttonMonthView",
					C1Localizer.GetString("C1EventsCalendar.Localization.buttonMonthView",
					"Month",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("buttonMonthView", value);
			}
		}
		//	buttonMonthView: "Month"

		/// <summary>
		/// buttonListView string.
		/// </summary>
		[DefaultValue("List")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.buttonListView",
			"Text for list view button.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string buttonListView
		{
			get
			{

				return GetPropertyValue<string>("buttonListView",
					C1Localizer.GetString("C1EventsCalendar.Localization.buttonListView",
					"List",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("buttonListView", value);
			}
		}
		//	buttonListView: "List"	

		/// <summary>
		/// buttonDelete string.
		/// </summary>
		[DefaultValue("Delete")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.buttonDelete",
			"Text for delete button.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string buttonDelete
		{
			get
			{

				return GetPropertyValue<string>("buttonDelete",
					C1Localizer.GetString("C1EventsCalendar.Localization.buttonDelete",
					"Delete",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("buttonDelete", value);
			}
		}
		//	buttonDelete: "Delete"	

		/// <summary>
		/// buttonOK string.
		/// </summary>
		[DefaultValue("OK")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.buttonOK",
			"Text for ok button.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string buttonOK
		{
			get
			{

				return GetPropertyValue<string>("buttonOK",
					C1Localizer.GetString("C1EventsCalendar.Localization.buttonOK",
					"OK",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("buttonOK", value);
			}
		}
		//	buttonOK: "OK"	

		/// <summary>
		/// buttonCancel string.
		/// </summary>
		[DefaultValue("Cancel")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.buttonCancel",
			"Text for cancel button.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string buttonCancel
		{
			get
			{

				return GetPropertyValue<string>("buttonCancel",
					C1Localizer.GetString("C1EventsCalendar.Localization.buttonCancel",
					"Cancel",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("buttonCancel", value);
			}
		}
		//	buttonCancel: "Cancel"	

		/// <summary>
		/// labelAllDay string.
		/// </summary>
		[DefaultValue("all-day")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.labelAllDay",
			"Text for all day label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string labelAllDay
		{
			get
			{

				return GetPropertyValue<string>("labelAllDay",
					C1Localizer.GetString("C1EventsCalendar.Localization.labelAllDay",
					"all-day",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("labelAllDay", value);
			}
		}
		//	labelAllDay: "all-day"	

		/// <summary>
		/// labelToday string.
		/// </summary>
		[DefaultValue("Today")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.labelToday",
			"Text for today label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string labelToday
		{
			get
			{

				return GetPropertyValue<string>("labelToday",
					C1Localizer.GetString("C1EventsCalendar.Localization.labelToday",
					"Today",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("labelToday", value);
			}
		}
		//	labelToday: "Today"	

		/// <summary>
		/// labelName string.
		/// </summary>
		[DefaultValue("name")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.labelName",
			"Text for name label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string labelName
		{
			get
			{

				return GetPropertyValue<string>("labelName",
					C1Localizer.GetString("C1EventsCalendar.Localization.labelName",
					"name",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("labelName", value);
			}
		}
		//	labelName: "name"	

		/// <summary>
		/// labelStarts string.
		/// </summary>
		[DefaultValue("starts")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.labelStarts",
			"Text for start label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string labelStarts
		{
			get
			{

				return GetPropertyValue<string>("labelStarts",
					C1Localizer.GetString("C1EventsCalendar.Localization.labelStarts",
					"starts",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("labelStarts", value);
			}
		}
		//	labelStarts: "starts"	

		/// <summary>
		/// labelEnds string.
		/// </summary>
		[DefaultValue("ends")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.labelEnds",
			"Text for end label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string labelEnds
		{
			get
			{

				return GetPropertyValue<string>("labelEnds",
					C1Localizer.GetString("C1EventsCalendar.Localization.labelEnds",
					"ends",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("labelEnds", value);
			}
		}
		//	labelEnds: "ends"	

		/// <summary>
		/// labelLocation string.
		/// </summary>
		[DefaultValue("location")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.labelLocation",
			"Text for location label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string labelLocation
		{
			get
			{

				return GetPropertyValue<string>("labelLocation",
					C1Localizer.GetString("C1EventsCalendar.Localization.labelLocation",
					"location",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("labelLocation", value);
			}
		}
		///	labelLocation: "location"

		/// <summary>
		/// labelRepeat string.
		/// </summary>
		[DefaultValue("repeat")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.labelRepeat",
			"Text for repeat label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string labelRepeat
		{
			get
			{

				return GetPropertyValue<string>("labelRepeat",
					C1Localizer.GetString("C1EventsCalendar.Localization.labelRepeat",
					"repeat",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("labelRepeat", value);
			}
		}
		//	labelRepeat: "repeat"	

		/// <summary>
		/// labelCalendar string.
		/// </summary>
		[DefaultValue("calendar")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.labelCalendar",
			"Text for calendar label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string labelCalendar
		{
			get
			{

				return GetPropertyValue<string>("labelCalendar",
					C1Localizer.GetString("C1EventsCalendar.Localization.labelCalendar",
					"calendar",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("labelCalendar", value);
			}
		}
		//	labelCalendar: "calendar"	

		/// <summary>
		/// labelDescription string.
		/// </summary>
		[DefaultValue("description")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.labelDescription",
			"Text for description label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string labelDescription
		{
			get
			{

				return GetPropertyValue<string>("labelDescription",
					C1Localizer.GetString("C1EventsCalendar.Localization.labelDescription",
					"description",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("labelDescription", value);
			}
		}
		//	labelDescription: "description"	

		/// <summary>
		/// promptOpenOccurrenceFormat string.
		/// </summary>
		[DefaultValue("{2}  is recurring event. Do you want to open only this occurrence?")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.promptOpenOccurrenceFormat",
			"Prompt text for opening occurrence.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string promptOpenOccurrenceFormat
		{
			get
			{

				return GetPropertyValue<string>("promptOpenOccurrenceFormat",
					C1Localizer.GetString("C1EventsCalendar.Localization.promptOpenOccurrenceFormat",
					"{2}  is recurring event. Do you want to open only this occurrence?",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("promptOpenOccurrenceFormat", value);
			}
		}
		//	promptOpenOccurrenceFormat: "{2}  is recurring event. Do you want to open only this occurrence?"	// {0} = Start, {1} = End, {2} = Subject, {3} = Location

		/// <summary>
		/// textNewEvent string.
		/// </summary>
		[DefaultValue("New event")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.textNewEvent",
			"Text for new event.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string textNewEvent
		{
			get
			{

				return GetPropertyValue<string>("textNewEvent",
					C1Localizer.GetString("C1EventsCalendar.Localization.textNewEvent",
					"New event",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("textNewEvent", value);
			}
		}
		//	textNewEvent: "New event"

		/// <summary>
		/// repeatNone string.
		/// </summary>
		[DefaultValue("None")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.repeatNone",
			"Text for not repeat.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string repeatNone
		{
			get
			{

				return GetPropertyValue<string>("repeatNone",
					C1Localizer.GetString("C1EventsCalendar.Localization.repeatNone",
					"None",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("repeatNone", value);
			}
		}
		//	repeatNone: "None"	

		/// <summary>
		/// repeatDaily string.
		/// </summary>
		[DefaultValue("Every Day")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.repeatDaily",
			"Text for repeat daily.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string repeatDaily
		{
			get
			{

				return GetPropertyValue<string>("repeatDaily",
					C1Localizer.GetString("C1EventsCalendar.Localization.repeatDaily",
					"Every Day",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("repeatDaily", value);
			}
		}
		//	repeatDaily: "Every Day"

		/// <summary>
		/// repeatWorkDays string.
		/// </summary>
		[DefaultValue("Work days")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.repeatWorkDays",
			"Text for repeat work days.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string repeatWorkDays
		{
			get
			{

				return GetPropertyValue<string>("repeatWorkDays",
					C1Localizer.GetString("C1EventsCalendar.Localization.repeatWorkDays",
					"Work days",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("repeatWorkDays", value);
			}
		}
		//	repeatWorkDays: "Work days"	

		/// <summary>
		/// repeatWeekly string.
		/// </summary>
		[DefaultValue("Every Week")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.repeatWeekly",
			"Text for repeat weekly.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string repeatWeekly
		{
			get
			{

				return GetPropertyValue<string>("repeatWeekly",
					C1Localizer.GetString("C1EventsCalendar.Localization.repeatWeekly",
					"Every Week",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("repeatWeekly", value);
			}
		}
		//	repeatWeekly: "Every Week"

		/// <summary>
		/// repeatMonthly string.
		/// </summary>
		[DefaultValue("Every Month")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.repeatMonthly",
			"Text for repeat monthly.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string repeatMonthly
		{
			get
			{

				return GetPropertyValue<string>("repeatMonthly",
					C1Localizer.GetString("C1EventsCalendar.Localization.repeatMonthly",
					"Every Month",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("repeatMonthly", value);
			}
		}
		//	repeatMonthly: "Every Month"	

		/// <summary>
		/// repeatYearly string.
		/// </summary>
		[DefaultValue("Every Year")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.LocalizationOption.repeatYearly",
			"Text for repeat yearly.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string repeatYearly
		{
			get
			{

				return GetPropertyValue<string>("repeatYearly",
					C1Localizer.GetString("C1EventsCalendar.Localization.repeatYearly",
					"Every Year",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("repeatYearly", value);
			}
		}
		//	repeatYearly: "Every Year"	

		#endregion

		#region ** implement ICustomOptionType interface

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return base.ToString();
		}

		internal bool ShouldSerialize()
		{
			return true;
		}

		#endregion end of ** IJsonEmptiable interface implementation


		bool IJsonEmptiable.IsEmpty
		{
			get { return !this.ShouldSerialize(); }
		}
	}

	#endregion end of ** LocalizationOption class

	#region ** DatePagerLocalizationOption class

	/// <summary>
	/// Date pager localization strings.
	/// </summary>
	public class DatePagerLocalizationOption : Settings, IJsonEmptiable
	{
#if !EXTENDER
		C1EventsCalendar _evCal;
#else
		C1EventsCalendarExtender _evCal;
#endif
		/// <summary>
		/// Initializes a new instance of the <see cref="LocalizationOption"/> class.
		/// </summary>
#if !EXTENDER
		public DatePagerLocalizationOption(C1EventsCalendar evCal)
#else
		public DatePagerLocalizationOption(C1EventsCalendarExtender evCal)
#endif
		{
			_evCal = evCal;
		}

		#region ** localization string properties

		/// <summary>
		/// dayViewTooltipFormat string.
		/// </summary>
		[DefaultValue("{0:dddd, MMMM d, yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.dayViewTooltipFormat",
			"Date format for day view tooltip.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string dayViewTooltipFormat
		{
			get
			{

				return GetPropertyValue<string>("dayViewTooltipFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.dayViewTooltipFormat",
					"{0:dddd, MMMM d, yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("dayViewTooltipFormat", value);
			}
		}
		//	dayViewTooltipFormat: "{0:dddd, MMMM d, yyyy}"

		/// <summary>
		/// weekViewTooltipFormat string.
		/// </summary>
		[DefaultValue("{0:MMMM d} - {1:d, yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.weekViewTooltipFormat",
			"Date format for week view tooltip.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string weekViewTooltipFormat
		{
			get
			{

				return GetPropertyValue<string>("weekViewTooltipFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.weekViewTooltipFormat",
					"{0:MMMM d} - {1:d, yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("weekViewTooltipFormat", value);
			}
		}
		//	weekViewTooltipFormat: "{0:MMMM d} - {1:d, yyyy}"

		/// <summary>
		/// weekViewTooltip2MonthesFormat string.
		/// </summary>
		[DefaultValue("{0:MMMM d} - {1:MMMM d, yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.weekViewTooltip2MonthesFormat",
			"Date format for week view tooltip between 2 months.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string weekViewTooltip2MonthesFormat
		{
			get
			{

				return GetPropertyValue<string>("weekViewTooltip2MonthesFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.weekViewTooltip2MonthesFormat",
					"{0:MMMM d} - {1:MMMM d, yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("weekViewTooltip2MonthesFormat", value);
			}
		}
		//	weekViewTooltip2MonthesFormat: "{0:MMMM d} - {1:MMMM d, yyyy}"

		/// <summary>
		/// customViewDayUnitTooltip2MonthesFormat string.
		/// </summary>
		[DefaultValue("{0:MMMM d} - {1:MMMM d, yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.customViewDayUnitTooltip2MonthesFormat",
			"Date format for custom day view tooltip between 2 months.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customViewDayUnitTooltip2MonthesFormat
		{
			get
			{

				return GetPropertyValue<string>("customViewDayUnitTooltip2MonthesFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.customViewDayUnitTooltip2MonthesFormat",
					"{0:MMMM d} - {1:MMMM d, yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customViewDayUnitTooltip2MonthesFormat", value);
			}
		}
		//	customViewDayUnitTooltip2MonthesFormat: "{0:MMMM d} - {1:MMMM d, yyyy}"

		/// <summary>
		/// customViewSingleDayTooltipFormat string.
		/// </summary>
		[DefaultValue("{0:MMMM d yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.customViewSingleDayTooltipFormat",
			"Date format for custom single day view tooltip.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customViewSingleDayTooltipFormat
		{
			get
			{

				return GetPropertyValue<string>("customViewSingleDayTooltipFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.customViewSingleDayTooltipFormat",
					"{0:MMMM d yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customViewSingleDayTooltipFormat", value);
			}
		}
		//	customViewSingleDayTooltipFormat: "{0:MMMM d yyyy}"

		/// <summary>
		/// customViewDayTooltipFormat string.
		/// </summary>
		[DefaultValue("{0:MMMM d} - {1:d, yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.customViewDayTooltipFormat",
			"Date format for custom multiple day view tooltip.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customViewDayTooltipFormat
		{
			get
			{

				return GetPropertyValue<string>("customViewDayTooltipFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.customViewDayTooltipFormat",
					"{0:MMMM d} - {1:d, yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customViewDayTooltipFormat", value);
			}
		}
		//	customViewDayTooltipFormat: "{0:MMMM d} - {1:d, yyyy}"

		/// <summary>
		/// customViewMonthTooltip2YearsFormat string.
		/// </summary>
		[DefaultValue("{0:MMMM yyyy} - {1:MMMM yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.customViewMonthTooltip2YearsFormat",
			"Date format for custom month view tooltip between 2 years.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customViewMonthTooltip2YearsFormat
		{
			get
			{

				return GetPropertyValue<string>("customViewMonthTooltip2YearsFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.customViewMonthTooltip2YearsFormat",
					"{0:MMMM yyyy} - {1:MMMM yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customViewMonthTooltip2YearsFormat", value);
			}
		}
		//	customViewMonthTooltip2YearsFormat: "{0:MMMM yyyy} - {1:MMMM yyyy}"

		/// <summary>
		/// customViewSingleMonthTooltipFormat string.
		/// </summary>
		[DefaultValue("{0:MMMM yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.customViewSingleMonthTooltipFormat",
			"Date format for custom single month view tooltip.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customViewSingleMonthTooltipFormat
		{
			get
			{

				return GetPropertyValue<string>("customViewSingleMonthTooltipFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.customViewSingleMonthTooltipFormat",
					"{0:MMMM yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customViewSingleMonthTooltipFormat", value);
			}
		}
		//	customViewSingleMonthTooltipFormat: "{0:MMMM yyyy}"

		/// <summary>
		/// customViewMonthTooltipFormat string.
		/// </summary>
		[DefaultValue("{0:MMMM} - {1:MMMM, yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.customViewMonthTooltipFormat",
			"Date format for custom multiple month view tooltip.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customViewMonthTooltipFormat
		{
			get
			{

				return GetPropertyValue<string>("customViewMonthTooltipFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.customViewMonthTooltipFormat",
					"{0:MMMM} - {1:MMMM, yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customViewMonthTooltipFormat", value);
			}
		}
		//	customViewMonthTooltipFormat: "{0:MMMM} - {1:MMMM, yyyy}"

		/// <summary>
		/// customViewSingleYearTooltipFormat string.
		/// </summary>
		[DefaultValue("{0:yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.customViewSingleYearTooltipFormat",
			"Date format for custom single year view tooltip.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customViewSingleYearTooltipFormat
		{
			get
			{

				return GetPropertyValue<string>("customViewSingleYearTooltipFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.customViewSingleYearTooltipFormat",
					"{0:yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customViewSingleYearTooltipFormat", value);
			}
		}
		//	customViewSingleYearTooltipFormat: "{0:yyyy}"

		/// <summary>
		/// customViewYearTooltipFormat string.
		/// </summary>
		[DefaultValue("{0:yyyy} - {1:yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.customViewYearTooltipFormat",
			"Date format for custom multiple year view tooltip.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customViewYearTooltipFormat
		{
			get
			{

				return GetPropertyValue<string>("customViewYearTooltipFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.customViewYearTooltipFormat",
					"{0:yyyy} - {1:yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customViewYearTooltipFormat", value);
			}
		}
		//	customViewYearTooltipFormat: "{0:yyyy} - {1:yyyy}"

		/// <summary>
		/// customViewUnitDayLabelFormat string.
		/// </summary>
		[DefaultValue("{0:MMM dd}-{1:dd}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.customViewUnitDayLabelFormat",
			"Date format for custom multiple day view label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customViewUnitDayLabelFormat
		{
			get
			{

				return GetPropertyValue<string>("customViewUnitDayLabelFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.customViewUnitDayLabelFormat",
					"{0:MMM dd}-{1:dd}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customViewUnitDayLabelFormat", value);
			}
		}
		//	customViewUnitDayLabelFormat: "{0:MMM dd}-{1:dd}"

		/// <summary>
		/// customViewUnitSingleDayLabelFormat string.
		/// </summary>
		[DefaultValue("{0:MMM dd}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.customViewUnitSingleDayLabelFormat",
			"Date format for custom single day view label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customViewUnitSingleDayLabelFormat
		{
			get
			{

				return GetPropertyValue<string>("customViewUnitSingleDayLabelFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.customViewUnitSingleDayLabelFormat",
					"{0:MMM dd}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customViewUnitSingleDayLabelFormat", value);
			}
		}
		//	customViewUnitSingleDayLabelFormat: "{0:MMM dd}"

		/// <summary>
		/// customViewUnitDayLabelFormat2Months string.
		/// </summary>
		[DefaultValue("{0:MMM dd}-{1:MMM dd}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.customViewUnitDayLabelFormat2Months",
			"Date format for custom day view label between 2 months.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customViewUnitDayLabelFormat2Months
		{
			get
			{

				return GetPropertyValue<string>("customViewUnitDayLabelFormat2Months",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.customViewUnitDayLabelFormat2Months",
					"{0:MMM dd}-{1:MMM dd}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customViewUnitDayLabelFormat2Months", value);
			}
		}
		//	customViewUnitDayLabelFormat2Months: "{0:MMM dd}-{1:MMM dd}"

		/// <summary>
		/// customViewUnitMonthLabelFormat string.
		/// </summary>
		[DefaultValue("{0:MMM}-{1:MMM, yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.customViewUnitMonthLabelFormat",
			"Date format for custom multiple months view label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customViewUnitMonthLabelFormat
		{
			get
			{

				return GetPropertyValue<string>("customViewUnitMonthLabelFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.customViewUnitMonthLabelFormat",
					"{0:MMM}-{1:MMM, yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customViewUnitMonthLabelFormat", value);
			}
		}
		//	customViewUnitMonthLabelFormat: "{0:MMM}-{1:MMM, yyyy}"

		/// <summary>
		/// customViewUnitSingleMonthLabelFormat string.
		/// </summary>
		[DefaultValue("{0:MMM}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.customViewUnitSingleMonthLabelFormat",
			"Date format for custom single month view label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customViewUnitSingleMonthLabelFormat
		{
			get
			{

				return GetPropertyValue<string>("customViewUnitSingleMonthLabelFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.customViewUnitSingleMonthLabelFormat",
					"{0:MMM}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customViewUnitSingleMonthLabelFormat", value);
			}
		}
		//	customViewUnitSingleMonthLabelFormat: "{0:MMM}"

		/// <summary>
		/// customViewUnitMonthLabelFormat2Years string.
		/// </summary>
		[DefaultValue("{0:MMM yyyy}-{1:MMM yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.customViewUnitMonthLabelFormat2Years",
			"Date format for custom month view label between 2 years.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customViewUnitMonthLabelFormat2Years
		{
			get
			{

				return GetPropertyValue<string>("customViewUnitMonthLabelFormat2Years",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.customViewUnitMonthLabelFormat2Years",
					"{0:MMM yyyy}-{1:MMM yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customViewUnitMonthLabelFormat2Years", value);
			}
		}
		//	customViewUnitMonthLabelFormat2Years: "{0:MMM yyyy}-{1:MMM yyyy}"

		/// <summary>
		/// customViewUnitSingleYearLabelFormat string.
		/// </summary>
		[DefaultValue("{0:yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.customViewUnitSingleYearLabelFormat",
			"Date format for custom single year view label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customViewUnitSingleYearLabelFormat
		{
			get
			{

				return GetPropertyValue<string>("customViewUnitSingleYearLabelFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.customViewUnitSingleYearLabelFormat",
					"{0:yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customViewUnitSingleYearLabelFormat", value);
			}
		}
		//	customViewUnitSingleYearLabelFormat: "{0:yyyy}"

		/// <summary>
		/// customViewUnitYearLabelFormat string.
		/// </summary>
		[DefaultValue("{0:yyyy}-{1:yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.customViewUnitYearLabelFormat",
			"Date format for custom multiple year view label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string customViewUnitYearLabelFormat
		{
			get
			{

				return GetPropertyValue<string>("customViewUnitYearLabelFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.customViewUnitYearLabelFormat",
					"{0:yyyy}-{1:yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("customViewUnitYearLabelFormat", value);
			}
		}
		//	customViewUnitYearLabelFormat: "{0:yyyy}-{1:yyyy}"

		/// <summary>
		/// monthViewTooltipFormat string.
		/// </summary>
		[DefaultValue("{0:MMMM yyyy}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.monthViewTooltipFormat",
			"Date format for month view tooltip.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string monthViewTooltipFormat
		{
			get
			{

				return GetPropertyValue<string>("monthViewTooltipFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.monthViewTooltipFormat",
					"{0:MMMM yyyy}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("monthViewTooltipFormat", value);
			}
		}
		//	monthViewTooltipFormat: "{0:MMMM yyyy}"

		/// <summary>
		/// dayViewLabelFormat string.
		/// </summary>
		[DefaultValue("{0:d }")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.dayViewLabelFormat",
			"Date format for day view label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string dayViewLabelFormat
		{
			get
			{

				return GetPropertyValue<string>("dayViewLabelFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.dayViewLabelFormat",
					"{0:d }",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("dayViewLabelFormat", value);
			}
		}
		//	dayViewLabelFormat: "{0:d }"

		/// <summary>
		/// weekViewLabelFormat string.
		/// </summary>
		[DefaultValue("{0:MMM dd}-{1:dd}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.weekViewLabelFormat",
			"Date format for week view label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string weekViewLabelFormat
		{
			get
			{

				return GetPropertyValue<string>("weekViewLabelFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.weekViewLabelFormat",
					"{0:MMM dd}-{1:dd}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("weekViewLabelFormat", value);
			}
		}
		//	weekViewLabelFormat: "{0:MMM dd}-{1:dd}"

		/// <summary>
		/// monthViewLabelFormat string.
		/// </summary>
		[DefaultValue("{0:MMM}")]
		[C1Category("Category.Appearance")]
		[C1Description("C1EventsCalendar.DatePagerLocalizationOption.monthViewLabelFormat",
			"Date format for month view label.")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		[WidgetOption()]
		public string monthViewLabelFormat
		{
			get
			{

				return GetPropertyValue<string>("monthViewLabelFormat",
					C1Localizer.GetString("C1EventsCalendar.DatePagerLocalization.monthViewLabelFormat",
					"{0:MMM}",
					_evCal.Culture));
			}
			set
			{
				SetPropertyValue<string>("monthViewLabelFormat", value);
			}
		}
		//	monthViewLabelFormat: "{0:MMM}"


		#endregion

		#region ** implement ICustomOptionType interface

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return base.ToString();
		}

		internal bool ShouldSerialize()
		{
			return true;
		}

		#endregion end of ** IJsonEmptiable interface implementation


		bool IJsonEmptiable.IsEmpty
		{
			get { return !this.ShouldSerialize(); }
		}
	}

	#endregion end of ** DatePagerLocalizationOption class

}