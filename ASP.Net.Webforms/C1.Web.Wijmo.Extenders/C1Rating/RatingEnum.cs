﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Rating
#else
namespace C1.Web.Wijmo.Controls.C1Rating
#endif
{
	/// <summary>
	/// Use the members of this enumeration to set the value of the Orientation property.
	/// </summary>
	public enum Orientation
	{
		/// <summary>
		/// Place the stars horizontally.
		/// </summary>
		Horizontal = 0,
		/// <summary>
		/// Place the starts vertically.
		/// </summary>
		Vertical = 1
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the Direction property.
	/// </summary>
	public enum Direction
	{
		/// <summary>
		/// Render the stars from left-to-right or top-to-bottom.
		/// </summary>
		Normal = 0,
		/// <summary>
		/// Render the stars from right-to-left or bottom-to-top.
		/// </summary>
		Reversed = 1
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the RatingMode property.
	/// </summary>
	public enum RatingMode
	{
		/// <summary>
		/// All the stars from first to the rated one will be rated.
		/// </summary>
		Continuous = 0,
		/// <summary>
		/// Only one start can be rated.
		/// </summary>
		Single = 1
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the ResetButton.Position property.
	/// </summary>
	public enum ResetButtonPosition
	{
		/// <summary>
		/// The reset button will be put at the left or top side.
		/// </summary>
		LeftOrTop = 0,
		/// <summary>
		/// The reset button will be put at the right or bottom side.
		/// </summary>
		RightOrBottom = 1
	}
}
