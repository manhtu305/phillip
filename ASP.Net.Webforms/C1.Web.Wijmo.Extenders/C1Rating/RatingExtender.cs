﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Drawing;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Extenders.C1Rating
{
	/// <summary>
	/// Extender for the c1rating widget.
	/// </summary>
	//[TargetControlType(typeof(Control))]
	[TargetControlType(typeof(Panel))]
	[TargetControlType(typeof(DropDownList))]
	[ToolboxItem(true)]
	[ToolboxBitmap(typeof(C1RatingExtender), "C1Rating.png")]
	[LicenseProviderAttribute()]
	public partial class C1RatingExtender : WidgetExtenderControlBase
	{
		public bool _productLicensed = false;

		/// <summary>
		/// Initializes a new instance of the C1RatingExtender class.
		/// </summary>
		public C1RatingExtender()
		{
			VerifyLicense();
			this.InitRating();
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1RatingExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

	}
}
