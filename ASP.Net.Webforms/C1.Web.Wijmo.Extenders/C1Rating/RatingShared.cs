﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Rating", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Rating
#else
using C1.Web.Wijmo.Controls.Localization;
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Rating", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1Rating
#endif
{
	[WidgetDependencies(
		typeof(WijRating)
#if !EXTENDER
		,"extensions.c1rating.js",
		ResourcesConst.C1WRAPPER_PRO
#endif
)]
#if EXTENDER
	public partial class C1RatingExtender
#else 
	public partial class C1Rating: C1TargetControlBase
#endif
	{

		private void InitRating()
		{
			this.ResetButton = new RatingResetButton();
			this.Hint = new RatingHint();
			this.Animation = new RatingAnimation();
			this.Icons = new RatingIcons();
		}

		#region Properties

		/// <summary>
		/// Number of stars to display.
		/// </summary>
		[DefaultValue(5)]
		[C1Description("C1Rating.Count")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Count
		{
			get
			{
				return this.GetPropertyValue("Count", 5);
			}
			set
			{
				this.SetPropertyValue("Count", value);
			}
		}

		/// <summary>
		/// The sections of every star split into.
		/// </summary>
		[DefaultValue(1)]
		[C1Description("C1Rating.Split")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int Split
		{
			get
			{
				return this.GetPropertyValue("Split", 1);
			}
			set
			{
				this.SetPropertyValue("Split", value);
			}
		}

		/// <summary>
		/// Total value of the control.
		/// </summary>
		/// <remarks>
		/// For ex, if the count is 5, split is 2 and the totalValue
		/// is 100, then the value that every step represents is 100/(5 * 2) = 10
		/// and one star represents the 10*(1 * 2) = 20;
		/// </remarks>
		[DefaultValue(5)]
		[C1Description("C1Rating.TotalValue")]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public int TotalValue
		{
			get
			{
				return this.GetPropertyValue("TotalValue", 5);
			}
			set
			{
				this.SetPropertyValue("TotalValue", value);
			}
		}

		/// <summary>
		/// Rated value of the control.
		/// </summary>
		[DefaultValue(0.0)]
		[C1Description("C1Rating.Value")]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public double Value
		{
			get
			{
				return this.GetPropertyValue("Value", 0.0);
			}
			set
			{
				this.SetPropertyValue("Value", value);
			}
		}

		/// <summary>
		/// The minimize value that can be rated.
		/// </summary>
		[DefaultValue(null)]
		[C1Description("C1Rating.Min")]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public double? Min
		{
			get
			{
				return this.GetPropertyValue<double?>("Min", null);
			}
			set
			{
				this.SetPropertyValue<double?>("Min", value);
			}
		}

		/// <summary>
		/// The maximize value that can be rated.
		/// </summary>
		[DefaultValue(null)]
		[C1Description("C1Rating.Max")]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public double? Max
		{
			get
			{
				return this.GetPropertyValue<double?>("Max", null);
			}
			set
			{
				this.SetPropertyValue<double?>("Max", value);
			}
		}

		/// <summary>
		/// A value that represents a reset button.
		/// </summary>
		[C1Description("C1Rating.ResetButton")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public RatingResetButton ResetButton { get; set; }

		/// <summary>
		/// A value that represents the hint information when hover the rating star.
		/// </summary>
		[C1Description("C1Rating.Hint")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public RatingHint Hint { get; set; }

		/// <summary>
		/// Orientation of the control.
		/// </summary>
		[DefaultValue(Orientation.Horizontal)]
		[C1Description("C1Rating.Orientation")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public Orientation Orientation
		{
			get
			{
				return this.GetPropertyValue("Orientation", Orientation.Horizontal);
			}
			set
			{
				this.SetPropertyValue("Orientation", value);
			}
		}

		/// <summary>
		/// Direction of the control.
		/// </summary>
		[DefaultValue(Direction.Normal)]
		[C1Description("C1Rating.Direction")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public Direction Direction
		{
			get
			{
				return this.GetPropertyValue("Direction", Direction.Normal);
			}
			set
			{
				this.SetPropertyValue("Direction", value);
			}
		}

		/// <summary>
		/// Rating mode of the control.
		/// </summary>
		[DefaultValue(RatingMode.Continuous)]
		[C1Description("C1Rating.RatingMode")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public RatingMode RatingMode
		{
			get
			{
				return this.GetPropertyValue("RatingMode", RatingMode.Continuous);
			}
			set
			{
				this.SetPropertyValue("RatingMode", value);
			}
		}

		/// <summary>
		/// A value that indicates the settings for customize rating icons.
		/// </summary>
		[C1Description("C1Rating.Icons")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public RatingIcons Icons { get; set; }

		/// <summary>
		/// Width of icon. All icons should have the same width.
		/// </summary>
		[DefaultValue(16)]
		[C1Description("C1Rating.IconWidth")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int IconWidth
		{
			get
			{
				return this.GetPropertyValue("IconWidth", 16);
			}
			set
			{
				this.SetPropertyValue("IconWidth", value);
			}
		}

		/// <summary>
		/// Height of icon. All icons should have the same height.
		/// </summary>
		[DefaultValue(16)]
		[C1Description("C1Rating.IconHeight")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int IconHeight
		{
			get
			{
				return this.GetPropertyValue("IconHeight", 16);
			}
			set
			{
				this.SetPropertyValue("IconHeight", value);
			}
		}

		/// <summary>
		/// A value indicates whether to show animation and the duration for the animation.
		/// </summary>
		[C1Description("C1Rating.Animation")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public RatingAnimation Animation { get; set; }

		#region ClientEvents

		/// <summary>
		/// Fires before the control rating.  This event can be cancelled. 
		/// "return false;" to cancel the event.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1Rating.OnClientRating")]
		[WidgetEvent("e, data")]
		[WidgetOptionName("rating")]
		[C1Category("Category.ClientSideEvents")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientRating
		{
			get
			{
				return GetPropertyValue("OnClientRating", "");
			}
			set
			{
				SetPropertyValue("OnClientRating", value);
			}
		}

		/// <summary>
		/// Fires after the control is rated.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1Rating.OnClientRated")]
		[WidgetEvent("e, data")]
		[WidgetOptionName("rated")]
		[C1Category("Category.ClientSideEvents")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientRated
		{
			get
			{
				return GetPropertyValue("OnClientRated", "");
			}
			set
			{
				SetPropertyValue("OnClientRated", value);
			}
		}

		/// <summary>
		/// Fires when click the reset button.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1Rating.OnClientReset")]
		[WidgetEvent("e")]
		[WidgetOptionName("reset")]
		[C1Category("Category.ClientSideEvents")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientReset
		{
			get
			{
				return GetPropertyValue("OnClientReset", "");
			}
			set
			{
				SetPropertyValue("OnClientReset", value);
			}
		}

		/// <summary>
		/// Fires when hover the control.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1Rating.OnClientHover")]
		[WidgetEvent("e, data")]
		[WidgetOptionName("hover")]
		[C1Category("Category.ClientSideEvents")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientHover
		{
			get
			{
				return GetPropertyValue("OnClientHover", "");
			}
			set
			{
				SetPropertyValue("OnClientHover", value);
			}
		}

		#endregion

		#endregion

		#region Serialize

		//[EditorBrowsable(EditorBrowsableState.Never)]
		//public bool ShouldSerializeHint()
		//{
		//    System.Diagnostics.Debugger.Launch();
		//    return Hint.ShouldSerialize();
		//}

		//[EditorBrowsable(EditorBrowsableState.Never)]
		//public bool ShouldSerializeIcons()
		//{
		//    System.Diagnostics.Debugger.Launch();
		//    return Icons.ShouldSerialize();
		//}

		//[EditorBrowsable(EditorBrowsableState.Never)]
		//public bool ShouldSerializeAnimation()
		//{
		//    System.Diagnostics.Debugger.Launch();
		//    return Animation.ShouldSerialize();
		//}

		#endregion
	}
}

