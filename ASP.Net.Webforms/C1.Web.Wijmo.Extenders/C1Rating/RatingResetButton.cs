﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Rating
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Rating
#endif
{
	/// <summary>
	/// Represents a reset button.
	/// </summary>
	public class RatingResetButton : Settings, IJsonEmptiable
	{

		/// <summary>
		/// A value that determines whether to show the reset button or not.
		/// </summary>
		[C1Description("C1Rating.RatingResetButton.Disabled")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(false)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Disabled
		{
			get
			{
				return this.GetPropertyValue("Disabled", false);
			}
			set
			{
				this.SetPropertyValue("Disabled", value);
			}
		}

		/// <summary>
		/// A value that indicates the text shown when hover the button.
		/// </summary>
		[C1Description("C1Rating.RatingResetButton.Hint")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("cancel this rating!")]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string Hint
		{
			get
			{
				return this.GetPropertyValue("Hint", "cancel this rating!");
			}
			set
			{
				this.SetPropertyValue("Hint", value);
			}
		}

		/// <summary>
		/// A value that indicates the position of the reset button.
		/// </summary>
		[C1Description("C1Rating.RatingResetButton.Position")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(ResetButtonPosition.LeftOrTop)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public ResetButtonPosition Position
		{
			get
			{
				return this.GetPropertyValue<ResetButtonPosition>("Position", ResetButtonPosition.LeftOrTop);
			}
			set
			{
				this.SetPropertyValue<ResetButtonPosition>("Position", value);
			}
		}

		/// <summary>
		/// A value that indicates the customized class added to the reset button.
		/// </summary>
		[C1Description("C1Rating.RatingResetButton.CustomizedClass")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string CustomizedClass
		{
			get
			{
				return this.GetPropertyValue("CustomizedClass", "");
			}
			set
			{
				this.SetPropertyValue("CustomizedClass", value);
			}
		}

		/// <summary>
		/// A value that indicates the customized class added to the reset button when hover it.
		/// </summary>
		[C1Description("C1Rating.RatingResetButton.CustomizedHoverClass")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string CustomizedHoverClass
		{
			get
			{
				return this.GetPropertyValue("CustomizedHoverClass", "");
			}
			set
			{
				this.SetPropertyValue("CustomizedHoverClass", value);
			}
		}

		#region Serialize
		
		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal bool ShouldSerialize()
		{
			if (Disabled)
			{
				return true;
			}
			if (Hint != "cancel this rating!")
			{
				return true;
			}
			if (Position != ResetButtonPosition.LeftOrTop)
			{
				return true;
			}
			if (!String.IsNullOrEmpty(CustomizedClass))
			{
				return true;
			}
			if (!String.IsNullOrEmpty(CustomizedHoverClass))
			{
				return true;
			}
			return false;
		}

		#endregion

	}
}
