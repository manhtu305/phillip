﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Collections;
using System.Web.UI.WebControls;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Rating
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Rating
#endif
{
	/// <summary>
	/// Represents the RatingIcons class which contains all settings for the customized icons.
	/// </summary>
	public class RatingIcons : Settings, IJsonEmptiable
	{
		public RatingIcons()
		{
			this.IconsClass = new RatingIconClass();
			this.HoverIconsClass = new RatingIconClass();
			this.RatedIconsClass = new RatingIconClass();
		}

		/// <summary>
		/// A string or an array value indicates the urls of icons.
		/// </summary>
		/// <remarks>
		/// If value is a string, all the star will apply the iconsClass.
		/// If value is an array, each star will apply the related iconsClass
		/// value by index.
		/// </remarks>
		[C1Description("C1Rating.RatingIcons.IconsClass")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public RatingIconClass IconsClass { get; set; }

		/// <summary>
		/// A string or an array value indicates the urls of hover icons.
		/// </summary>
		/// <remarks>
		/// If value is a string, all the star will apply the iconsClass on hover.
		/// If value is an array, each star will apply the related iconsClass
		/// value by index on hover.
		/// </remarks>
		[C1Description("C1Rating.RatingIcons.HoverIconsClass")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public RatingIconClass HoverIconsClass { get; set; }

		/// <summary>
		/// A string or an array value indicates the urls of rated icons.
		/// </summary>
		/// <remarks>
		/// If value is a string, all the star will apply the iconsClass.
		/// If value is an array, each star will apply the related iconsClass
		/// </remarks>
		[C1Description("C1Rating.RatingIcons.RatedIconsClass")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public RatingIconClass RatedIconsClass { get; set; }

		#region Serialize

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal bool ShouldSerialize()
		{
			if (IconsClass.ShouldSerialize())
			{
				return true;
			}
			if (HoverIconsClass.ShouldSerialize())
			{
				return true;
			}
			if (RatedIconsClass.ShouldSerialize())
			{
				return true;
			}
			return false;
		}

		#endregion
	}

	/// <summary>
	/// Represents the RatingIconClass class which contains all settings for the icon's class.
	/// </summary>
	public class RatingIconClass: Settings, ICustomOptionType, IJsonEmptiable
	{
		//private List<string> _iconsClass;
		string[] _iconsClass = null;
		public RatingIconClass()
		{
			//_iconsClass = new List<string>();
		}

		/// <summary>
		/// A value that indicates the class to be added to icon.
		/// </summary>
		[NotifyParentProperty(true)]
		[C1Description("C1Rating.RatingIconClass.IconClass")]
		[DefaultValue("")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string IconClass
		{
			get
			{
				return this.GetPropertyValue("IconClass", "");
			}
			set
			{
				this.SetPropertyValue("IconClass", value);
			}
		}

		/// <summary>
		/// A value that indicates a list of class to be added to icon.
		/// </summary>
		[C1Description("C1Rating.RatingIconClass.IconsClass")]
		[NotifyParentProperty(true)]
		//[Editor(typeof(StringArrayEditor), typeof(UITypeEditor))]
		[TypeConverter(typeof(StringArrayConverter))]
		[WidgetOption]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string[] IconsClass
		{
			get
			{
				if (_iconsClass == null)
				{
					_iconsClass = new string[0];
				}
				return _iconsClass;
			}
			set
			{
				_iconsClass = value;
			}
		}
        
		string ICustomOptionType.SerializeValue()
		{
			string optionValue = "";
			if (!String.IsNullOrEmpty(this.IconClass))
			{
				optionValue = this.IconClass;
			}
			else if (this.IconsClass.Length > 0)
			{
				optionValue += "[";
				for (var i = 0; i < IconsClass.Length; i++)
				{
					optionValue += String.Format("'{0}'", IconsClass[i]);
					if (i < IconsClass.Length - 1)
					{
						optionValue += ",";
					}
				}
				optionValue += "]";
			}
			return optionValue;
		}

		bool ICustomOptionType.IncludeQuotes
		{
			get 
			{
				if (!String.IsNullOrEmpty(this.IconClass))
				{
					return true;
				}
				if (this.IconsClass.Length > 0)
				{
					return false;
				}
				return true;
			}
		}

#if !EXTENDER
		void ICustomOptionType.DeSerializeValue(object state)
		{
			//fix tfs issue #21457
			if (state is string)
			{
				this.IconClass = (string)state;
			}
			else if (state is ArrayList)
			{
				this.IconsClass = ((ArrayList)state).ToArray(typeof(string)) as string[];
			}
			/*
			Hashtable ht = state as Hashtable;
			if (ht != null)
			{
				if (ht["iconClass"] != null)
				{
					this.IconClass = (string)ht["iconClass"];
				}
				if (ht["iconsClass"] != null)
				{
					ArrayList al = ht["iconsClass"] as ArrayList;
					if (al != null)
					{
						this.IconsClass = al.ToArray() as string[];
						//this.IconsClass.Clear();
						//foreach (object obj in al)
						//{
						//    this.IconsClass.Add((string)obj);
						//}
					}
				}
			}
			 * */
		}
#endif

		#region Serialize

		internal bool ShouldSerialize()
		{
			if (!String.IsNullOrEmpty(this.IconClass))
			{
				return true;
			}
			if (this.IconsClass.Length > 0)
			{
				return true;
			}
			return false;
		}

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		#endregion
	}
}
