﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Rating
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Rating
#endif
{
	/// <summary>
	/// Represents the RatingAnimation class which contains all settings for the animation.
	/// </summary>
	public class RatingAnimation : Settings, IJsonEmptiable
	{

		/// <summary>
		/// A value that indicates the effect of the animation.
		/// </summary>
		[C1Description("C1Rating.RatingAnimation.Animated")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string Animated
		{
			get
			{
				return this.GetPropertyValue("Animated", "");
			}
			set
			{
				this.SetPropertyValue("Animated", value);
			}
		}

		/// <summary>
		/// A value that indicates the duration of the animation.
		/// </summary>
		[C1Description("C1Rating.RatingAnimation.Duration")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(500)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int Duration
		{
			get
			{
				return this.GetPropertyValue("Duration", 500);
			}
			set
			{
				this.SetPropertyValue("Duration", value);
			}
		}

		/// <summary>
		/// A value that indicates the easing of the animation.
		/// </summary>
		[C1Description("C1Rating.RatingAnimation.Easing")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(Easing.Linear)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Easing Easing
		{
			get
			{
				return this.GetPropertyValue<Easing>("Easing", Easing.Linear);
			}
			set
			{
				this.SetPropertyValue<Easing>("Easing", value);
			}
		}

		/// <summary>
		/// A value that indicates the delay time of each star to play the animation.
		/// </summary>
		[C1Description("C1Rating.RatingAnimation.Delay")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(250)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int Delay
		{
			get
			{
				return this.GetPropertyValue("Delay", 250);
			}
			set
			{
				this.SetPropertyValue("Delay", value);
			}
		}

		#region Serialize

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal bool ShouldSerialize()
		{
			if (!String.IsNullOrEmpty(this.Animated))
			{
				return true;
			}
			if (Duration != 500)
			{
				return true;
			}
			if (Easing != Easing.Linear)
			{
				return true;
			}
			if (Delay != 250)
			{
				return true;
			}
			return false;
		}

		#endregion
	}
}
