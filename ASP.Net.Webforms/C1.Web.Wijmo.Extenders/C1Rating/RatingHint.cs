﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Collections;
using System.Web.UI.WebControls;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Rating
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Rating
#endif
{
	/// <summary>
	/// Represents the hint information when hover the rating star.
	/// </summary>
	public class RatingHint : Settings, IJsonEmptiable
	{
		string[] _content = null;

		/// <summary>
		/// Initializes a new instance of the RatingHint class.
		/// </summary>
		public RatingHint()
		{
		}

		/// <summary>
		/// A value that determines whether to show the hint or not.
		/// </summary>
		[C1Description("C1Rating.RatingHint.Disabled")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(false)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Disabled
		{
			get
			{
				return this.GetPropertyValue("Disabled", false);
			}
			set
			{
				this.SetPropertyValue("Disabled", value);
			}
		}

		/// <summary>
		/// Values will be shown when hover a star.
		/// </summary>
		[C1Description("C1Rating.RatingHint.Content")]
		[NotifyParentProperty(true)]
		[TypeConverter(typeof(StringArrayConverter))]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public string[] Content
		{
			get
			{
				if (_content == null)
				{
					_content = new string[0];
				}
				return _content;
			}
			set
			{
				_content = value;
			}
		}

		#region Serialize

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerialize(); }
		}

		internal bool ShouldSerialize()
		{
			if (Disabled)
			{
				return true;
			}
			if (Content.Length > 0)
			{
				return true;
			}
			return false;
		}

		#endregion
	}
}
