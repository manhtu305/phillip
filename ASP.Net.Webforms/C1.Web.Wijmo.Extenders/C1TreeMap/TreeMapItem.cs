﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1TreeMap
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1TreeMap
#endif
{
#if EXTENDER
	/// <summary>
	/// Represents a item in the <see cref="C1TreeMapExtender"/> control. 
	/// </summary>
	public class TreeMapItem : Settings
#else
	/// <summary>
	/// Represents a item in the <see cref="C1TreeMap"/> control. 
	/// </summary>
	public class TreeMapItem : Settings
#endif
	{
		#region ** fields
		private List<TreeMapItem> _items;
		#endregion

		#region ** constructor
		public TreeMapItem() 
		{ 
		
		}

		public TreeMapItem(string label, double value) 
		{
			this.Label = label;
			this.Value = value;
		}
		#endregion

		#region ** properties
		/// <summary>
		/// Gets a <see cref="TreeMapItem"/> that contains the first-level child items of the current item.
		/// </summary>
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[C1Description("C1TreeMapItem.Items")]
		[CollectionItemType(typeof(TreeMapItem))]
		public List<TreeMapItem> Items
		{
			get
			{
				if (_items == null)
				{
					_items = new List<TreeMapItem>();
				}
				return _items;
			}
		}

		/// <summary>
		/// Gets or sets the label of treemap item.
		/// </summary>
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[C1Description("C1TreeMapItem.Label")]
		[DefaultValue("")]
		public string Label
		{
			get
			{
				return GetPropertyValue<string>("Label", string.Empty);
			}
			set
			{
				SetPropertyValue<string>("Label", value);
			}
		}

		/// <summary>
		/// Gets or sets the value of treemap item.
		/// </summary>
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[C1Description("C1TreeMapItem.Value")]
		[DefaultValue(0)]
		public double Value
		{
			get
			{
				return GetPropertyValue<double>("Value", 0);
			}
			set
			{
				SetPropertyValue<double>("Value", value);
			}
		}

		/// <summary>
		/// Gets or sets the color of treemap item.
		/// </summary>
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[TypeConverter(typeof(WebColorConverter))]
		[C1Description("C1TreeMapItem.Color")]
		[DefaultValue(typeof(Color), "")]
		public Color Color 
		{
			get 
			{
				return GetPropertyValue<Color>("Color", Color.Empty);
			}
			set 
			{
				SetPropertyValue<Color>("Color", value);
			}
		}

		/// <summary>
		/// Gets or sets the minimum color of treemap item.
		/// </summary>
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[C1Description("C1TreeMapItem.MinColor")]
		[DefaultValue(typeof(Color), "")]
		[TypeConverter(typeof(WebColorConverter))]
		public Color MinColor 
		{
			get
			{
				return GetPropertyValue<Color>("MinColor", Color.Empty);
			}
			set
			{
				SetPropertyValue<Color>("MinColor", value);
			}
		}

		/// <summary>
		/// Gets or sets the middle color of treemap item.
		/// </summary>
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[C1Description("C1TreeMapItem.MidColor")]
		[DefaultValue(typeof(Color), "")]
		[TypeConverter(typeof(WebColorConverter))]
		public Color MidColor
		{
			get
			{
				return GetPropertyValue<Color>("MidColor", Color.Empty);
			}
			set
			{
				SetPropertyValue<Color>("MidColor", value);
			}
		}

		/// <summary>
		/// Gets or sets the maximum color of treemap item.
		/// </summary>
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[C1Description("C1TreeMapItem.MaxColor")]
		[DefaultValue(typeof(Color), "")]
		[TypeConverter(typeof(WebColorConverter))]
		public Color MaxColor
		{
			get
			{
				return GetPropertyValue<Color>("MaxColor", Color.Empty);
			}
			set
			{
				SetPropertyValue<Color>("MaxColor", value);
			}
		}

		/// <summary>
		/// Gets or sets the minimum value of treemap item.
		/// </summary>
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[C1Description("C1TreeMapItem.MinColorValue")]
		[DefaultValue(0)]
		public double MinColorValue
		{
			get
			{
				return GetPropertyValue<double>("MinColorValue", 0);
			}
			set
			{
				SetPropertyValue<double>("MinColorValue", value);
			}
		}

		/// <summary>
		/// Gets or sets the middle value of treemap item.
		/// </summary>
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[C1Description("C1TreeMapItem.MidColorValue")]
		[DefaultValue(0)]
		public double MidColorValue
		{
			get
			{
				return GetPropertyValue<double>("MidColorValue", 0);
			}
			set
			{
				SetPropertyValue<double>("MidColorValue", value);
			}
		}

		/// <summary>
		/// Gets or sets the maximum value of treemap item.
		/// </summary>
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[C1Description("C1TreeMapItem.MaxColorValue")]
		[DefaultValue(0)]
		public double MaxColorValue
		{
			get
			{
				return GetPropertyValue<double>("MaxColorValue", 0);
			}
			set
			{
				SetPropertyValue<double>("MaxColorValue", value);
			}
		}

#if !EXTENDER
		/// <summary>
		/// This option used to calcute the current depth for data binding.
		/// </summary>
		internal int Depth
		{
			get;
			set;
		}
#endif
		#endregion

		#region ** serialize methods
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public bool ShouldSerializeItems()
		{
			return this.Items.Count > 0;
		}
		#endregion
	}
}
