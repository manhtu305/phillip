﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1TreeMap
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1TreeMap
#endif
{
	[WidgetDependencies(
			typeof(WijTreeMap)
#if !EXTENDER
, "extensions.c1treemap.js",
ResourcesConst.C1WRAPPER_PRO
#endif
)]
#if EXTENDER
	public partial class C1TreeMapExtender
#else
	public partial class C1TreeMap
#endif
	{
		#region ** fields
		private List<TreeMapItem> _items;
		#endregion

		#region ** Options
		/// <summary>
		/// A value that indicates the type of treemap to be displayed.
		/// </summary>
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DefaultValue(TreeMapType.Squarified)]
		[C1Description("C1TreeMap.Type")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public TreeMapType Type
		{
			get 
			{
				return GetPropertyValue<TreeMapType>("Type", TreeMapType.Squarified);
			}
			set 
			{
				SetPropertyValue < TreeMapType>("Type", value);
			}
		}

		// todo data

		/// <summary>
		/// A value that indicates whether to show the label.
		/// </summary>
		[WidgetOption]
		[C1Category("Category.Appearance")]
		[DefaultValue(true)]
		[C1Description("C1TreeMap.ShowLabel")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public bool ShowLabel 
		{
			get 
			{
				return GetPropertyValue<bool>("ShowLabel", true);
			}
			set 
			{
				SetPropertyValue<bool>("ShowLabel", value);
			}
		}

		/// <summary>
		/// A value that indicates a function which is used to format the label of treemap item.
		/// </summary>
		[WidgetEvent]
		[C1Category("Category.Appearance")]
		[DefaultValue("")]
		[C1Description("C1TreeMap.LabelFormatter")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string LabelFormatter 
		{
			get 
			{
				return GetPropertyValue("LabelFormatter", string.Empty);
			}
			set
			{
				SetPropertyValue("LabelFormatter", value);
			}
		}

		/// <summary>
		/// A value that indicates whether to show the tooltip.
		/// </summary>
		[WidgetOption]
		[C1Category("Category.Appearance")]
		[DefaultValue(false)]
		[C1Description("C1TreeMap.ShowTooltip")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public bool ShowTooltip 
		{
			get 
			{
				return GetPropertyValue<bool>("ShowTooltip", false);
			}
			set
			{
				SetPropertyValue<bool>("ShowTooltip", value);
			}
		}

		// todo tooltipOptions
		
		/// <summary>
		/// A value that indicates whether to show the title.
		/// </summary>
		[WidgetOption]
		[C1Category("Category.Appearance")]
		[DefaultValue(true)]
		[C1Description("C1TreeMap.ShowTitle")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public bool ShowTitle 
		{
			get
			{
				return GetPropertyValue<bool>("ShowTitle", true);
			}
			set
			{
				SetPropertyValue<bool>("ShowTitle", value);
			}
		}

		/// <summary>
		/// A value that indicates the height of the title.
		/// </summary>
		[WidgetOption]
		[C1Category("Category.Appearance")]
		[DefaultValue(20)]
		[C1Description("C1TreeMap.TitleHeight")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int TitleHeight 
		{
			get 
			{
				return GetPropertyValue<int>("TitleHeight", 20);
			}
			set
			{
				SetPropertyValue<int>("TitleHeight", value);
			}
		}

		/// <summary>
		/// A value that indicates a function which is used to format the title of treemap item.
		/// </summary>
		[WidgetEvent]
		[C1Category("Category.Appearance")]
		[DefaultValue("")]
		[C1Description("C1TreeMap.TitleFormatter")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string TitleFormatter 
		{
			get 
			{
				return GetPropertyValue<string>("TitleFormatter", string.Empty);
			}
			set
			{
				SetPropertyValue<string>("TitleFormatter", value);
			}
		}

		/// <summary>
		/// A value that indicates the color of min value.
		/// </summary>
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DefaultValue(typeof(Color), "")]
		[TypeConverter(typeof(WebColorConverter))]
		[C1Description("C1TreeMap.MinColor")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Color MinColor 
		{
			get 
			{
				return GetPropertyValue<Color>("MinColor", Color.Empty);
			}
			set
			{
				SetPropertyValue<Color>("MinColor", value);
			}
		}

		/// <summary>
		/// A value that indicates min value. If this option is not set, treemap will calculate min value automatically.
		/// </summary>
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DefaultValue(null)]
		[C1Description("C1TreeMap.MinColorValue")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double? MinColorValue 
		{
			get 
			{
				return GetPropertyValue<double?>("MinColorValue", null);
			}
			set
			{
				SetPropertyValue<double?>("MinColorValue", value);
			}
		}

		/// <summary>
		/// A value that indicates the color of mid value.
		/// </summary>
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DefaultValue(typeof(Color), "")]
		[TypeConverter(typeof(WebColorConverter))]
		[C1Description("C1TreeMap.MidColor")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Color MidColor 
		{
			get 
			{
				return GetPropertyValue<Color>("MidColor", Color.Empty);
			}
			set
			{
				SetPropertyValue<Color>("MidColor", value);
			}
		}

		/// <summary>
		/// A value that indicates mid value. If this option is not set, treemap will calculate mid value automatically.
		/// </summary>
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DefaultValue(null)]
		[C1Description("C1TreeMap.MidColorValue")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double? MidColorValue 
		{
			get 
			{
				return GetPropertyValue<double?>("MidColorValue", null);
			}
			set
			{
				SetPropertyValue<double?>("MidColorValue", value);
			}
		}

		/// <summary>
		/// A value that indicates the color of max value.
		/// </summary>
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DefaultValue(typeof(Color), "")]
		[TypeConverter(typeof(WebColorConverter))]
		[C1Description("C1TreeMap.MaxColor")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Color MaxColor 
		{
			get 
			{
				return GetPropertyValue<Color>("MaxColor", Color.Empty);
			}
			set
			{
				SetPropertyValue<Color>("MaxColor", value);
			}
		}

		/// <summary>
		/// A value that indicates max value. If this option is not set, treemap will calculate max value automatically.
		/// </summary>
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DefaultValue(null)]
		[C1Description("C1TreeMap.MaxColorValue")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double? MaxColorValue 
		{
			get 
			{
				return GetPropertyValue<double?>("MaxColorValue", null);
			}
			set
			{
				SetPropertyValue<double?>("MaxColorValue", value);
			}
		}

		/// <summary>
		/// A value that indicates whether to show back buttons after treemap drilled down.
		/// </summary>
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[C1Description("C1TreeMap.ShowBackButtons")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool ShowBackButtons 
		{
			get 
			{
				return GetPropertyValue<bool>("ShowBackButtons", false);
			}
			set
			{
				SetPropertyValue<bool>("ShowBackButtons", value);
			}
		}

		/// <summary>
		/// A collection that to bind data to treemap. 
		/// </summary>
		[WidgetOption]
		[WidgetOptionName("data")]
		[C1Category("Category.Behavior")]
		[C1Description("C1TreeMap.Items")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[CollectionItemType(typeof(TreeMapItem))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public List<TreeMapItem> Items 
		{
			get 
			{
				if (_items == null) 
				{
					_items = new List<TreeMapItem>();
				}
				return _items;
			}
		}

		#endregion

		#region ** Client events
		/// <summary>
		/// This event fires before item is painting. User can create customize item in this event.
		/// </summary>
		[WidgetEvent("e, data")]
		[C1Category("Category.Behavior")]
		[DefaultValue("")]
		[C1Description("C1TreeMap.OnClientItemPainting")]
		public string OnClientItemPainting 
		{
			get 
			{
				return GetPropertyValue<string>("OnClientItemPainting", string.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientItemPainting", value);
			}
		}

		/// <summary>
		/// This event fires after item is painted.
		/// </summary>
		[WidgetEvent]
		[C1Category("Category.Behavior")]
		[DefaultValue("")]
		[C1Description("C1TreeMap.OnClientItemPainted")]
		public string OnClientItemPainted 
		{
			get 
			{
				return GetPropertyValue<string>("OnClientItemPainted", string.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientItemPainted", value);
			}
		}

		/// <summary>
		/// This event fires before treemap is painting.
		/// </summary>
		[WidgetEvent("e, args")]
		[C1Category("Category.Behavior")]
		[DefaultValue("")]
		[C1Description("C1TreeMap.OnClientPainting")]
		public string OnClientPainting 
		{
			get 
			{
				return GetPropertyValue<string>("OnClientPainting", string.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientPainting", value);
			}
		}

		/// <summary>
		/// This event fires after treemap is painted.
		/// </summary>
		[WidgetEvent]
		[C1Category("Category.Behavior")]
		[DefaultValue("")]
		[C1Description("C1TreeMap.OnClientPainted")]
		public string OnClientPainted 
		{
			get
			{
				return GetPropertyValue<string>("OnClientPainted", string.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientPainted", value);
			}
		}

		/// <summary>
		/// This event fires before drill down. This event can be cancelled, use "return false;" to cancel the event.
		/// </summary>
		[WidgetEvent("clickItemData, currentData, targetData")]
		[C1Category("Category.Behavior")]
		[DefaultValue("")]
		[C1Description("C1TreeMap.OnClientDrillingDown")]
		public string OnClientDrillingDown 
		{
			get 
			{
				return GetPropertyValue<string>("OnClientDrillingDown", string.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientDrillingDown", value);
			}
		}

		/// <summary>
		/// This event fires after drill down.
		/// </summary>
		[WidgetEvent("e, data")]
		[C1Category("Category.Behavior")]
		[DefaultValue("")]
		[C1Description("C1TreeMap.OnClientDrilledDown")]
		public string OnClientDrilledDown 
		{
			get 
			{
				return GetPropertyValue<string>("OnClientDrilledDown", string.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientDrilledDown", value);
			}
		}

		/// <summary>
		/// This event fires before roll up. This event can be cancelled, use "return false;" to cancel the event.
		/// </summary>
		[WidgetEvent("clickItemData, currentData, targetData")]
		[C1Category("Category.Behavior")]
		[DefaultValue("")]
		[C1Description("C1TreeMap.OnClientRollingUp")]
		public string OnClientRollingUp 
		{
			get
			{
				return GetPropertyValue<string>("OnClientRollingUp", string.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientRollingUp", value);
			}
		}

		/// <summary>
		/// This event fires after roll up.
		/// </summary>
		[WidgetEvent("e, data")]
		[C1Category("Category.Behavior")]
		[DefaultValue("")]
		[C1Description("C1TreeMap.OnClientRolledUp")]
		public string OnClientRolledUp 
		{
			get 
			{
				return GetPropertyValue<string>("OnClientRolledUp", string.Empty);
			}
			set
			{
				SetPropertyValue<string>("OnClientRolledUp", value);
			}
		}
		#endregion
	}
}
