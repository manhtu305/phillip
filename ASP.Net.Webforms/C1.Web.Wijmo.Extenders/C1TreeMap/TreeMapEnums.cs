﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1TreeMap
#else
namespace C1.Web.Wijmo.Controls.C1TreeMap
#endif
{
	/// <summary>
	/// Indicates the type of treemap to display
	/// </summary>
	public enum TreeMapType
	{
		Squarified,
		Horizontal,
		Vertical
	}
}
