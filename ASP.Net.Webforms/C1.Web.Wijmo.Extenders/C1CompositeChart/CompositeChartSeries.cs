﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Drawing;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{

	#region ** The ChartSeriesType enumeration
	/// <summary>
	/// The ChartSeriesType represents the type of the series for the chart.
	/// </summary>
	public enum ChartSeriesType
	{
		/// <summary>
		/// Draw this series with pie chart shape.
		/// </summary>
		Pie = 0,
		/// <summary>
		/// Draw this series with bar chart shape.
		/// </summary>
		Bar = 1,
		/// <summary>
		/// Draw this series with column chart shape.
		/// </summary>
		Column = 2,
		/// <summary>
		/// Draw this series with line chart shape.
		/// </summary>
		Line = 3,
		/// <summary>
		/// Draw this series with spline chart shape.
		/// </summary>
		Spline = 4,
		/// <summary>
		/// Draw this series with bezier chart shape.
		/// </summary>
		Bezier = 5,
		/// <summary>
		/// Draw this series with scatter chart shape.
		/// </summary>
		Scatter = 6,
		/// <summary>
		/// Draw this series with area chart shape.
		/// </summary>
		Area = 7,
		/// <summary>
		/// Draw this series with high-low-open-close chart shape.
		/// </summary>
		Ohlc = 8,
		/// <summary>
		/// Draw this series with high-low chart shape.
		/// </summary>
		Hl = 9,
		/// <summary>
		/// Draw this series with candlestick chart shape.
		/// </summary>
		Candlestick = 10,
        /// <summary>
        /// Draw this series with several pie chart which shared their legend and style.
        /// </summary>
        SharedPie = 11,
        /// <summary>
        /// Draw this series with trendline chart shape.
        /// </summary>
        Trendline = 12
	}
	#endregion ** end of The ChartSeriesType enumeration

	/// <summary>
	/// Represents the CompositeChartSeries class which contains all settings for the composite chart chart series.
	/// </summary>
	public class CompositeChartSeries : ChartSeries
	{

		#region ** fields
		private List<PieChartSeries> _pieSeriesList = null;
        private List<CompositeSharedPieSeries> _sharedPieChartSeries = null;
		#endregion ** end of fields.

		#region ** constructors
		/// <summary>
		/// Initializes a new instance of the CompositeChartSeries class.
		/// </summary>
		public CompositeChartSeries()
			: base()
		{
			this.Data = new ChartSeriesData();
			this.LineMarkers = new LineChartMarker();
			this.MarkerStyle = new ChartStyle();
			this.Type = ChartSeriesType.Column;
			this.Center = Point.Empty;
			this.ScatterMarkerType = MarkerType.Circle;
			this.CandlestickSeries = new CandlestickChartSeries();
		}
		#endregion ** end of constructors

		#region ** properties
		/// <summary>
		/// A <see cref="ChartSeriesData"/> value specifies the data of the chart series.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("C1Chart.CompositeChartSeries.Data")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public ChartSeriesData Data { get; set; }

		/// <summary>
		/// A value that indicates the markers of the line.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.LineChartSeries.Markers")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[WidgetOptionName("markers")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public virtual LineChartMarker LineMarkers { get; set; }

		/// <summary>
		/// A value that indicates the style of the markers.
		/// </summary>
		[C1Category("Category.Style")]
		[C1Description("C1Chart.LineChartSeries.MarkerStyle")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public virtual ChartStyle MarkerStyle { get; set; }

		/// <summary>
		/// A value specifies the type of the chart series.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.CompositeChartSeries.Type")]
		[NotifyParentProperty(true)]
		[Json(true)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public virtual ChartSeriesType Type { get; set; }

		/// <summary>
		/// A value specifies the marker type of the scatter data point.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.CompositeChartSeries.MarkerType")]
		[NotifyParentProperty(true)]
		[DefaultValue(MarkerType.Circle)]
		[WidgetOption]
		[WidgetOptionName("markerType")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public virtual MarkerType ScatterMarkerType { get; set; }

		/// <summary>
		/// A value specifies the center of the pie chart.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.CompositeChartSeries.Center")]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public Point Center { get; set; }

		/// <summary>
		/// A value specifies the radius of the pie chart.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.CompositeChartSeries.Radius")]
		[NotifyParentProperty(true)]
		[DefaultValue(0)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int Radius 
        {
            get
            {
                return this.GetPropertyValue<int>("Radius", 0);
            }
            set
            {
                if (value < 0)
                {
                    throw new Exception("The Radius should be a positive value.");
                }

                this.SetPropertyValue<int>("Radius", value);
            }
        }

		/// <summary>
		/// A collection value specifies the series for the pie chart.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("C1Chart.CompositeChartSeries.PieSeriesList")]
		[CollectionItemType(typeof(PieChartSeries))]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public virtual List<PieChartSeries> PieSeriesList
		{
			get
			{
				if (this._pieSeriesList == null)
				{
					this._pieSeriesList = new List<PieChartSeries>();
				}

				return this._pieSeriesList;
			}
		}

		/// <summary>
		/// A value specifies the series for the candlestick chart.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("C1Chart.CompositeChartSeries.CandlestickSeries")]
		[CollectionItemType(typeof(CandlestickChartSeries))]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public virtual CandlestickChartSeries CandlestickSeries { get; set; }

		/// <summary>
		/// A value that indicate which y axis is the series in multiple y axis compositechart.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Chart.CompositeChartSeries.YAxis")]
		[NotifyParentProperty(true)]
		[DefaultValue(0)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public virtual int YAxis
		{
			get
			{
				return this.GetPropertyValue<int>("YAxis", 0);
			}
			set
			{
				this.SetPropertyValue<int>("YAxis", value);
			}
		}

        /// <summary>
        /// A collection value specifies the series for the shared pie chart.
        /// </summary>
        [C1Category("Category.Data")]
        [C1Description("C1Chart.CompositeChartSeries.SharedPieChartSeries")]
        [CollectionItemType(typeof(CompositeSharedPieSeries))]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Data)]
#endif
        public virtual List<CompositeSharedPieSeries> SharedPieChartSeries 
        {
            get 
            {
                if (_sharedPieChartSeries == null) 
                {
                    _sharedPieChartSeries = new List<CompositeSharedPieSeries>();
                }
                return _sharedPieChartSeries;
            }
        }

        /// <summary>
        /// Hide this property for CompositeChartSeries, CompositeChartSeries will lerverage Type to determine whether it's a trendline series.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override bool IsTrendline
        {
            get
            {
                return this.Type == ChartSeriesType.Trendline;
            }
        }

		#endregion ** end of properties.

		#region ** serialization methods
		/// <summary>
		/// Returns a boolean value that indicates whether the chart series 
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a boolean value to determine whether this series should be serialized.
		/// </returns>
		protected internal override bool ShouldSerialize()
		{
			if (base.ShouldSerialize())
			{
				return true;
			}

			if (this.Data.ShouldSerialize())
			{
				return true;
			}

			if (this.LineMarkers.ShouldSerialize())
			{
				return true;
			}

			if (this.CandlestickSeries.ShouldSerialize())
			{
				return true;
			}

			if (this.MarkerStyle.ShouldSerialize())
			{
				return true;
			}

			if (this.Type != ChartSeriesType.Column)
			{
				return true;
			}

			if (this.ScatterMarkerType != C1Chart.MarkerType.Circle)
			{
				return true;
			}

			if (this.ShouldSerializeCenter())
			{
				return true;
			}

			if (this.ShouldSerializePieSeriesList())
			{
				return true;
			}

			if (this.Radius != 0)
			{
				return true;
			}

			if (this.ShouldSerializeCandlestickSeries())
			{
				return true;
			}

            if (this.ShouldSerializeSharedPieChartSeries()) 
            {
                return true;
            }

			return false;
		}

		/// <summary>
		/// Determine whether the PieSeriesList property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if PieSeriesList has values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializePieSeriesList()
		{
			return this.PieSeriesList.Count > 0;
		}

		/// <summary>
		/// Determine whether the CandlestickSeries property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if CandlestickSeries has values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeCandlestickSeries()
		{
			return this.CandlestickSeries.ShouldSerialize();
		}

		/// <summary>
		/// Determine whether the Center property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if Center is not empty, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeCenter()
		{
			return !this.Center.IsEmpty;
		}

        /// <summary>
        /// Determine whether the SharedPieChartSeries property should be serialized to client side.
        /// </summary>
        /// <returns>
        /// Returns true if SharedPieChartSeries is not empty, otherwise return false.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeSharedPieChartSeries()
        {
            return this.SharedPieChartSeries.Count > 0;
        }

        #endregion ** end of serialization methods
	}

    /// <summary>
    /// Represents the CompositeSharedPieSeries class which just contains the settings for the pie chart chart series.
    /// It is designed for setting each pie chart series in compositechart "SharedPieChartSeries" property.
    /// </summary>
    public class CompositeSharedPieSeries : CompositeChartSeries
    {
        #region Hide unrelated properties

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override LineChartMarker LineMarkers { get; set; }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override ChartStyle MarkerStyle { get; set; }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
		[DefaultValue(ChartSeriesType.Pie)]
        public override ChartSeriesType Type
        {
            get
            {
                return ChartSeriesType.Pie;
            }
        }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override MarkerType ScatterMarkerType { get; set; }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override CandlestickChartSeries CandlestickSeries { get; set; }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override List<CompositeSharedPieSeries> SharedPieChartSeries
        {
            get
            {
                return base.SharedPieChartSeries;
            }
        }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override int YAxis { get; set; }

        #endregion
    }
}
