﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	[WidgetDependencies(
        typeof(WijCompositeChart)
#if !EXTENDER
, "extensions.c1compositechart.js",
ResourcesConst.C1WRAPPER_PRO
#endif
)]
#if EXTENDER
	public partial class C1CompositeChartExtender : C1ChartCoreExtender<CompositeChartSeries,  ChartAnimation>
#else
	public partial class C1CompositeChart : C1ChartCore<CompositeChartSeries, ChartAnimation, C1CompositeChartBinding>
#endif
	{

		#region ** private methods
		private void InitChart()
		{
			this.SeriesTransition = new ChartAnimation();
			this.SeriesTransition.Duration = 400;
			this.SeriesTransition.Easing = ChartEasing.EaseInCubic;
		}
		#endregion ** end of private methods.

		#region ** properties
		/// <summary>
		/// A value specifies whether to show a stacked chart.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Chart.CompositeChart.Stacked")]
		[DefaultValue(false)]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Stacked
		{
			get
			{
				return this.GetPropertyValue<bool>("Stacked", false);
			}
			set
			{
				this.SetPropertyValue<bool>("Stacked", value);
			}
		}

		/// <summary>
		/// A value specifies the percentage of Composite elements in the same cluster overlap.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.CompositeChart.ClusterOverlap")]
		[DefaultValue(0)]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int ClusterOverlap
		{
			get
			{
				return this.GetPropertyValue<int>("ClusterOverlap", 0);
			}
			set
			{
				this.SetPropertyValue<int>("ClusterOverlap", value);
			}
		}

		/// <summary>
		/// A value specifies the percentage of the plot area that each Composite cluster occupies.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.CompositeChart.ClusterWidth")]
		[DefaultValue(85)]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int ClusterWidth
		{
			get
			{
				return this.GetPropertyValue<int>("ClusterWidth", 85);
			}
			set
			{
				this.SetPropertyValue<int>("ClusterWidth", value);
			}
		}

		/// <summary>
		/// A value specifies the corner-radius for the bar.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.CompositeChart.ClusterRadius")]
		[DefaultValue(0)]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int ClusterRadius
		{
			get
			{
				return this.GetPropertyValue<int>("ClusterRadius", 0);
			}
			set
			{
				this.SetPropertyValue<int>("ClusterRadius", value);
			}
		}

		/// <summary>
		/// A value specifies the spacing between the adjacent bars.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Chart.CompositeChart.ClusterSpacing")]
		[DefaultValue(0)]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int ClusterSpacing
		{
			get
			{
				return this.GetPropertyValue<int>("ClusterSpacing", 0);
			}
			set
			{
				this.SetPropertyValue<int>("ClusterSpacing", value);
			}
		}

		/// <summary>
		/// A value specifies whether to show animation and the duration and effect for the animation when reload data.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Chart.CompositeChart.SeriesTransition")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public ChartAnimation SeriesTransition { get; set; }

		private List<CompositeChartYAxis> yaxes;

		/// <summary>
		/// A collection value specifies the y axes for multiple y axis compositechart.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Chart.CompositeChart.YAxes")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
		[CollectionItemType(typeof(CompositeChartYAxis))]
#endif
		public List<CompositeChartYAxis> YAxes
		{
			get 
			{
				if (yaxes == null)
				{
					yaxes = new List<CompositeChartYAxis>();
				}
				return yaxes;
			}
		}


		[Json(true)]
		public override bool ShowChartLabels
		{
			get
			{
				return base.ShowChartLabels;
			}
			set
			{
				base.ShowChartLabels = value;
			}
		}

		#endregion ** end of properties.

		#region ** override methods
		// added in 2013-10-31, fixing the OADateTime issue, move this method from C1ComponentsiteChart to this file.
		/// <summary>
		/// Gets the series data of the specified series.
		/// </summary>
		/// <param name="series">
		/// The specified series.
		/// </param>
		/// <returns>
		/// Returns the series data which retrieved from the specified series.
		/// </returns>
		protected override ChartSeriesData GetSeriesData(CompositeChartSeries series)
		{
            if (series.IsTrendline)
                return base.GetSeriesData(series);

			return series.Data;
		}

		#endregion

		#region ** serialization methods
		/// <summary>
		/// Determine whether the SeriesTransition property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns false if SeriesTransition has default values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeSeriesTransition()
		{
#if !EXTENDER
			if (this.IsDesignMode)
			{
				return true;
			}
#endif
			if (!this.SeriesTransition.Enabled)
				return true;
			if (this.SeriesTransition.Duration != 400)
				return true;
			//if (this.SeriesTransition.Easing != ">")
			if (this.SeriesTransition.Easing != ChartEasing.EaseInCubic)
				return true;
			return false;
		}

		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeYAxes()
		{
			return this.YAxes.Count > 0;
		}
		#endregion ** end of serialization methods.
		
		#region ** handle the OADate issue
		// handle the multiple axis mode.
		protected override void ConvertAxisOADate()
		{	
			if (this.SeriesList.Count() > 0)
			{
				if (IsDateTimeSeries("x"))
				{
					ConvertAxisByOADate(this.Axis.X);
				}
				if (this.YAxes.Count() > 0)
				{
					int index = 0;
					foreach (CompositeChartYAxis axis in this.YAxes)
					{
						if (IsDateTime(index))
						{
							//ConvertAxisMinMaxByOADate(index);
							ConvertAxisByOADate(axis);
						}
						index++;
					}
				}
				else if(IsDateTimeSeries("y"))
				{
					ConvertAxisByOADate(this.Axis.Y);
				}
			}
		}

		// check the axis is datetime.
		private bool IsDateTime(int yindex) 
		{
			foreach (CompositeChartSeries series in this.SeriesList)
			{
				if (yindex == series.YAxis && series.Data.Y.Values.Count() > 0)
				{
					return series.Data.Y.Values[0].DateTimeValue != null;
				}
			}
			return false;
		}

		#endregion
	}
}
