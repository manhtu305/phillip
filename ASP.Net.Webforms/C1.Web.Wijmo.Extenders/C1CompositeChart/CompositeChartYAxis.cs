﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	public class CompositeChartYAxis : ChartAxis
	{
		public CompositeChartYAxis()
		{
			this.Compass = ChartCompass.West;
			this.Labels.TextAlign = ChartAxisAlignment.Center;
			this.GridMajor.Visible = true;
			this.Visible = false;
			this.Height = new CompositeChartYAxisHeight();
		}

		/// <summary>
		/// A value that determine the height of the axis.  This property is for supporting part axis.
		/// </summary>
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[C1Description("C1Chart.CompositeChartYAxis.Height")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public CompositeChartYAxisHeight Height
		{
			get
			{
				return this.GetPropertyValue<CompositeChartYAxisHeight>("Height", null);
			}
			set
			{
				SetPropertyValue<CompositeChartYAxisHeight>("Height", value);
			}
		}

		#region ** serialization methods
		internal override bool ShouldSerialize()
		{
			return base.ShouldSerialize() || this.Height.ShouldSerialize();
		}
		#endregion
	}

	public class CompositeChartYAxisHeight : Settings, ICustomOptionType, IJsonEmptiable
	{
		/// <summary>
		/// A value determine whether the height is auto value.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Chart.CompositeChartYAxisHeight.IsAuto")]
		[NotifyParentProperty(true)]
		public bool IsAuto
		{
			get
			{
				return this.GetPropertyValue<bool>("IsAuto", false);
			}
			set
			{
				this.SetPropertyValue<bool>("IsAuto", value);
			}
		}

		/// <summary>
		/// A value that determines the value of this object. 
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Chart.CompositeChartYAxisHeight.Value")]
		[NotifyParentProperty(true)]
		public int? Value
		{
			get
			{
				return this.GetPropertyValue<int?>("Value", null);
			}
			set
			{
				this.SetPropertyValue<int?>("Value", value);
				if (value != null)
				{
					this.IsAuto = false;
				}
			}
		}

		string ICustomOptionType.SerializeValue()
		{
			if (IsAuto)
			{
				return "auto";
			}
			else if (Value != null)
			{
				return Value.ToString();
			}
			return null;
		}

#if !EXTENDER

		void ICustomOptionType.DeSerializeValue(object state)
		{
			if (state != null)
			{
				string strValue = state.ToString();
				if (strValue == "auto")
				{
					this.IsAuto = true;
				}
				else if (!string.IsNullOrEmpty(strValue))
				{
					int height;
					if (int.TryParse(strValue, out height)) 
					{
						this.Value = height;
					}
				}
			}
		}

#endif
		bool ICustomOptionType.IncludeQuotes
		{
			get { return this.IsAuto; }
		}

		internal bool ShouldSerialize()
		{
			return this.IsAuto || this.Value != null;
		}

		bool IJsonEmptiable.IsEmpty
		{
			get { return !this.ShouldSerialize(); }
		}
	}
}
