﻿using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Drawing;
using System;
using C1.Web.Wijmo.Extenders.Localization;

[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Video", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Video
{

	/// <summary>
	/// Video for the extender widget.
	/// </summary>
	[TargetControlType(typeof(Panel))]
	[ToolboxItem(true)]
	[WidgetDependencies(
		"base.globalize.min.js",
        "base.jquery.mousewheel.min.js",
        "wijmo.jquery.wijmo.wijtooltip.js",
        "wijmo.jquery.wijmo.wijvideo.js",
		ResourcesConst.WIJMO_OPEN_CSS
)]
	[ToolboxBitmap(typeof(C1VideoExtender), "C1Video.png")]
	[LicenseProviderAttribute()]
	public class C1VideoExtender : WidgetExtenderControlBase
	{
		#region Field
		private bool _productLicensed = false;
		#endregion

		#region constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1VideoExtender"/> class.
		/// </summary>
		public C1VideoExtender()
			: base()
		{
			VerifyLicense();
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1VideoExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}
		#endregion

		internal override bool IsWijMobileExtender
		{
			get
			{
				return false;
			}
		}

		#region method

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}
		#endregion

		#region property
		/// <summary>
		/// Full screen button visible. Set this property to true if you want to show
		/// the full screen button.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Video.FullScreenButtonVisible", "Set this property to true if you want to show the  full screen button.")]
		[DefaultValue(true)]
		[WidgetOption]
		public bool FullScreenButtonVisible
		{
			get
			{
				return this.GetPropertyValue<bool>("FullScreenButtonVisible", true);
			}
			set
			{
				this.SetPropertyValue<bool>("FullScreenButtonVisible", value);
			}
		}

		/// <summary>
		/// Determines whether to display the controls only when hovering the mouse to the video.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Video.ShowControlsOnHover", "Determines whether to display the controls only when hovering the mouse to the video.")]
		[DefaultValue(true)]
		[WidgetOption]
		public bool ShowControlsOnHover
		{
			get
			{
				return this.GetPropertyValue<bool>("ShowControlsOnHover", true);
			}
			set
			{
				this.SetPropertyValue<bool>("ShowControlsOnHover", value);
			}
		}
		#endregion
	}
}
