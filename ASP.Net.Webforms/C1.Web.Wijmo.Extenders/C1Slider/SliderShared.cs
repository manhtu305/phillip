﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Globalization;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Slider", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Slider
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Slider", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1Slider
#endif
{
    [WidgetDependencies(
        typeof(WijSlider)
#if !EXTENDER
, "extensions.c1slider.js",
ResourcesConst.C1WRAPPER_OPEN
#endif
)]
#if EXTENDER
	public partial class C1SliderExtender
#else
    public partial class C1Slider
#endif
    {
        #region Properties

        /// <summary>
        /// A value indicates whether to slide handle smoothly when user click outside handle on the bar.
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [C1Description("C1Slider.Animate")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool Animate
        {
            get
            {
                return this.GetPropertyValue("Animate", false);
            }
            set
            {
                this.SetPropertyValue("Animate", value);
            }
        }

        /// <summary>
        /// A value indicates the maximum value of the slider.
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(100)]
        [C1Description("C1Slider.Max")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int Max
        {
            get
            {
                return this.GetPropertyValue("Max", 100);
            }
            set
            {
                this.SetPropertyValue("Max", value);
            }
        }

        /// <summary>
        /// A value indicates the minimum value of the slider.
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(0)]
        [C1Description("C1Slider.Min")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int Min
        {
            get
            {
                return this.GetPropertyValue("Min", 0);
            }
            set
            {
                this.SetPropertyValue("Min", value);
            }
        }

        /// <summary>
        /// A value indicates the size or amount of each interval or step the slider takes between min and max. The full specified value range of the slider (max - min) needs to be evenly divisible by the step.
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(1)]
        [C1Description("C1Slider.Step")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int Step
        {
            get
            {
                return this.GetPropertyValue("Step", 1);
            }
            set
            {
                this.SetPropertyValue("Step", value);
            }
        }

        /// <summary>
        /// If the option value set to true, the slider will detect if you have two handles and create a styleable range element between these two. Two other possible values are 'min' and 'max'. A min range goes from the slider min to one handle. A max range goes from one handle to the slider max.
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
        [C1Description("C1Slider.Range")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool Range
        {
            get
            {
                return this.GetPropertyValue("Range", false);
            }
            set
            {
                this.SetPropertyValue("Range", value);
            }
        }

        /// <summary>
        /// A value indicates the value of the slider, if there's only one handle. If there is more than one handle, determines the value of the first handle.
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(0)]
        [C1Description("C1Slider.Value")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int Value
        {
            get
            {
                return this.GetPropertyValue("Value", 0);
            }
            set
            {
                this.SetPropertyValue("Value", value);
            }
        }

        private int[] _values;
        /// <summary>
        /// This option can be used to specify multiple handles. If range is set to true, the length of 'values' should be 2.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Slider.Values")]
        [TypeConverter(typeof(SliderValuesConverter))]
        [WidgetOption]
        [ArrayItemType(typeof(int))]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int[] Values
        {
            get
            {
                if (_values == null && Range)
                    _values = new int[] { 0, 0 };
                return _values;
            }
            set
            {
                _values = value;
            }
        }

        /// <summary>
        /// A value indicates whether the slider has the min at the left, the max at the right or the min at the bottom, the max at the top. Possible values: 'horizontal', 'vertical'.
        /// </summary>
        [C1Category("Category.Appearance")]
        [DefaultValue(Orientation.Horizontal)]
        [C1Description("C1Slider.Orientation")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public Orientation Orientation
        {
            get
            {
                return this.GetPropertyValue("Orientation", Orientation.Horizontal);
            }
            set
            {
                this.SetPropertyValue("Orientation", value);
                //update for fix bug 15804 by wuhao
#if !EXTENDER
                if (this.IsDesignMode)
                {
                    this.ChildControlsCreated = false;
                }
#endif
                //end for bug 15804
            }
        }

        /// <summary>
        /// A value determines whether the fill may be dragged between the buttons. 
        /// </summary>
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [C1Description("C1Slider.DragFill")]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool DragFill
        {
            get
            {
                return this.GetPropertyValue("DragFill", true);
            }
            set
            {
                this.SetPropertyValue("DragFill", value);
            }
        }

        #endregion

        #region Client Events

        /// <summary>
        /// Raised when the mouse is over the decrement button or increment button.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [C1Description("C1Slider.OnClientButtonMouseOver")]
        [WidgetEvent("ev, data")]
        [WidgetOptionName("buttonMouseOver")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientButtonMouseOver
        {
            get
            {
                return GetPropertyValue("OnClientButtonMouseOver", "");
            }
            set
            {
                SetPropertyValue("OnClientButtonMouseOver", value);
            }
        }

        /// <summary>
        /// Raised when the mouse is leave the decrement button or increment button.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [C1Description("C1Slider.OnClientButtonMouseOut")]
        [WidgetEvent("ev, data")]
        [WidgetOptionName("buttonMouseOut")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientButtonMouseOut
        {
            get
            {
                return GetPropertyValue("OnClientButtonMouseOut", "");
            }
            set
            {
                SetPropertyValue("OnClientButtonMouseOut", value);
            }
        }

        /// <summary>
        /// Raised when the mouse is down on the decrement button or decrement button.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [C1Description("C1Slider.OnClientButtonMouseDown")]
        [WidgetEvent("ev, data")]
        [WidgetOptionName("buttonMouseDown")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientButtonMouseDown
        {
            get
            {
                return GetPropertyValue("OnClientButtonMouseDown", "");
            }
            set
            {
                SetPropertyValue("OnClientButtonMouseDown", value);
            }
        }

        /// <summary>
        /// Raised when the mouse is up on the decrement button or increment button.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [C1Description("C1Slider.OnClientButtonMouseUp")]
        [WidgetEvent("ev, data")]
        [WidgetOptionName("buttonMouseUp")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientButtonMouseUp
        {
            get
            {
                return GetPropertyValue("OnClientButtonMouseUp", "");
            }
            set
            {
                SetPropertyValue("OnClientButtonMouseUp", value);
            }
        }

        /// <summary>
        /// Raised when the decrement button or increment button  has been clicked.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [DefaultValue("")]
        [C1Description("C1Slider.OnClientButtonClick")]
        [WidgetEvent("ev, data")]
        [WidgetOptionName("buttonClick")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientButtonClick
        {
            get
            {
                return GetPropertyValue("OnClientButtonClick", "");
            }
            set
            {
                SetPropertyValue("OnClientButtonClick", value);
            }
        }
        #endregion end of Client Events.
    }
}
