﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Extenders.Converters;
using System.Globalization;

[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Slider", "wijmo")]

namespace C1.Web.Wijmo.Extenders.C1Slider
{
	/// <summary>
	/// Slider for the expander widget.
	/// </summary>
	[TargetControlType(typeof(Panel))]
	[ToolboxItem(true)]
	[ToolboxBitmap(typeof(C1SliderExtender), "Slider.png")]
	[LicenseProviderAttribute()]
	public partial class C1SliderExtender : WidgetExtenderControlBase
	{
		private bool _productLicensed = false;

		public C1SliderExtender(): base()
		{
			VerifyLicense();
		}

		internal override bool IsWijMobileExtender
		{
			get
			{
				return false;
			}
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1SliderExtender), this, false);
#if GRAPECITY
            _productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}
	
	}
}
