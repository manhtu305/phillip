﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Globalization;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Slider
#else
namespace C1.Web.Wijmo.Controls.C1Slider
#endif
{
	/// <summary>
	///  Provides a unified way of converting types of values to other types
	/// </summary>
	internal class SliderValuesConverter : TypeConverter
	{
		/// <summary>
		/// Returns whether this converter can convert an object of the given type to
		/// the type of this converter.
		/// </summary>
		/// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
		/// <param name="sourceType">A System.Type that represents the type you want to convert from.</param>
		/// <returns>true if this converter can perform the conversion; otherwise, false.</returns>
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return (sourceType == typeof(string));
		}

		/// <summary>
		/// Converts the given object to the type of this converter, using the specified context and culture information.
		/// </summary>
		/// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
		/// <param name="culture">The System.Globalization.CultureInfo to use as the current culture.</param>
		/// <param name="value">The System.Object to convert.</param>
		/// <returns>An System.Object that represents the converted value.</returns>
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (!(value is string))
			{
				throw base.GetConvertFromException(value);
			}
			if (((string)value).Length == 0)
			{
				return new Int32[0];
			}
			string[] strArray = ((string)value).Split(',');

			List<Int32> list = new List<Int32>();

			for (int i = 0; i < strArray.Length; i++)
			{
				list.Add(Convert.ToInt32(strArray[i].Trim()));
			}
			return list.ToArray();
		}

		/// <summary>
		/// Converts the given value object to the specified type, using the specified context and culture information.
		/// </summary>
		/// <param name="context">An System.ComponentModel.ITypeDescriptorContext that provides a format context.</param>
		/// <param name="culture">A System.Globalization.CultureInfo. If null is passed, the current culture is assumed.</param>
		/// <param name="value">The System.Object to convert.</param>
		/// <param name="destinationType">The System.Type to convert the value parameter to.</param>
		/// <returns></returns>
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			string val = "<null>";
			if (value != null)
			{
				val = "";
				Int32[] arr = (Int32[])value;
				for (int i = 0; i < arr.Length; i++)
				{
					val += arr[i] + ",";
				}
				if (val != "")
				{
					val = val.Substring(0, val.Length - 1);
				}
			}
			return val;
		}
	}
}
