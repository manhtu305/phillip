using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Extenders.C1Editor
{ 

	/// <summary>
	///Editor for the editor widget.
	/// </summary>
	[TargetControlType(typeof(TextBox))]
	[ToolboxBitmap(typeof(C1EditorExtender), "C1Editor.png")]
	[LicenseProviderAttribute()]
    [ToolboxItem(true)]
    public partial class C1EditorExtender : WidgetExtenderControlBase
	{

		#region ** fields 
		private bool _productLicensed = false;

		#endregion end of ** fields.

		#region ** constructor
		/// <summary>
		/// Creates a new instance of the C1Editor class.
		/// </summary>
		public C1EditorExtender()
		{
			VerifyLicense();
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1EditorExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}
		#endregion end of ** constructor.

		#region ** properties

		internal override bool IsWijMobileExtender
		{
			get
			{
				return false;
			}
		}

		#endregion end of ** properties.

		#region ** methods
		#region ** override methods	
		/// <summary>
		/// Raises the PreRender event.
		/// </summary>
		/// <param name="e">An EventArgs object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);

			//this.Page.ClientScript.GetCallbackEventReference(this, string.Empty, string.Empty, string.Empty);
		}

		#endregion end of ** override methods.
		#endregion end of ** methods. 
	}
}
