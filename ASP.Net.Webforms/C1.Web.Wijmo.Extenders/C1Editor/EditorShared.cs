﻿using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.WebControls;

using System.Collections.Generic;
using System;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;

[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Editor", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Editor
#else
using C1.Web.Wijmo.Controls.Localization;

[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Editor", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1Editor
#endif
{
	[WidgetDependencies(        
		typeof(WijEditor)
#if !EXTENDER
        , typeof(C1Menu.C1Menu)
        , "extensions.c1editor.js",
		ResourcesConst.C1WRAPPER_PRO
#endif
)]
#if EXTENDER
	public partial class C1EditorExtender
#else
	public partial class C1Editor
#endif
	{
		#region ** fields
		private List<FontOption> _fontNames;
		private List<FontOption> _fontSizes;
		#endregion

		#region ** properties
		/// <summary>
		/// Editor mode (WYSIWYG/Code/Split).
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.EditorMode", "Editor mode (WYSIWYG/Code/Split).")]
		[DefaultValue(EditorMode.WYSIWYG)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public EditorMode EditorMode
		{
			get
			{
				return this.GetPropertyValue<EditorMode>("EditorMode", EditorMode.WYSIWYG);
			}
			set
			{
				this.SetPropertyValue<EditorMode>("EditorMode", value);
			}
		}

		/// <summary>
		/// An enum value represents the mode of the C1Editor.
		/// </summary>
		/// <remarks>
		/// It has three options: Ribbon, Simple and BBCode.
		/// </remarks>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.Mode", "An enum value represents the mode of the C1Editor.")]
		[DefaultValue(Mode.Ribbon)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public Mode Mode
		{
			get
			{
				return this.GetPropertyValue<Mode>("Mode", Mode.Ribbon);
			}
			set
			{
				this.SetPropertyValue<Mode>("Mode", value);
				this.ChildControlsCreated = false;
			}
		}

		/// <summary>
		/// The SimpleModeCommands property indicates the commands for the simple C1Editor.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.SimpleModeCommands", "An enum value represents the commands for the simple C1Editor.")]
		[WidgetOption]
		[TypeConverter(typeof(StringArrayConverter))]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string[] SimpleModeCommands
		{
			get
			{
				return this.GetPropertyValue<string[]>("SimpleModeCommands",
					new string[] {"Bold", "Italic", "Link", "BlockQuote", "StrikeThrough", 
						"InsertDate", "InsertImage", "NumberedList", "BulletedList", "InsertCode"});
			}
			set
			{
				this.SetPropertyValue<string[]>("SimpleModeCommands", value);
			}
		}

		/// <summary>
		/// The FontNames property specifies the list of font name 
		/// which will be shown in the font name drop down list.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.FontNames", "The FontNames property specifies the list of font name which will be shown in the font name drop down list.")]
		[TypeConverter(typeof(StringArrayConverter))]
		[Obsolete("Using CustomizeFontNames property instead.")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string[] FontNames
		{
			get
			{
				List<string> names = new List<string>();
				foreach (FontOption name in CustomFontNames) 
				{
					names.Add(name.Name);
				}
				return names.ToArray();
			}
			set
			{
				CustomFontNames.Clear();
				foreach (string name in value) 
				{
					CustomFontNames.Add(new FontOption(name));
				}
			}
		}

		/// <summary>
		/// The FontSizes property specifies the list of font size 
		/// which will be shown in the font size drop down list.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.FontSizes", "The FontSizes property specifies the list of font size which will be shown in the font size drop down list.")]
		[TypeConverter(typeof(StringArrayConverter))]
		[Obsolete("Using CustomizeFontSizes property instead.")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string[] FontSizes
		{
			get
			{
				List<string> sizes = new List<string>();
				foreach (FontOption name in CustomFontSizes)
				{
					sizes.Add(name.Name);
				}
				return sizes.ToArray();
			}
			set
			{
				CustomFontSizes.Clear();
				foreach (string name in value)
				{
					CustomFontSizes.Add(new FontOption(name));
				}
			}
		}

		/// <summary>
		/// The FontNames property specifies the list of font name which will be shown in the font name drop down list.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.FontNames", "The FontNames property specifies the list of font name which will be shown in the font name drop down list.")]
		[CollectionItemType(typeof(FontOption))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
		[WidgetOptionName("fontNames")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public List<FontOption> CustomFontNames 
		{
			get 
			{
				if (_fontNames == null) {
					_fontNames = new List<FontOption>();
				}
				return _fontNames;
			}
		}

		/// <summary>
		/// The FontSizes property specifies the list of font size which will be shown in the font size drop down list.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.FontSizes", "The FontSizes property specifies the list of font size which will be shown in the font size drop down list.")]
		[WidgetOption]
		[WidgetOptionName("fontSizes")]
		[CollectionItemType(typeof(FontOption))]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public List<FontOption> CustomFontSizes
		{
			get
			{
				if (_fontSizes == null) 
				{
					_fontSizes = new List<FontOption>();
				}
				return _fontSizes;
			}
		}

		/// <summary>
		/// The DefaultFontSize property specifies the default font size in the editor. 
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.DefaultFontSize", "The DefaultFontSize property specifies the default font size in the editor. ")]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string DefaultFontSize
		{
			get
			{
				return this.GetPropertyValue<string>("DefaultFontSize",
					"");
			}
			set
			{
				this.SetPropertyValue<string>("DefaultFontSize", value);
			}
		}

		/// <summary>
		/// The DefaultFontName property specifies the default font name in the editor. 
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.DefaultFontName", "The DefaultFontSize property specifies the default font name in the editor. ")]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string DefaultFontName
		{
			get
			{
				return this.GetPropertyValue<string>("DefaultFontName",
					"");
			}
			set
			{
				this.SetPropertyValue<string>("DefaultFontName", value);
			}
		}

		/// <summary>
		/// A value indicating whether the Text in the C1Editor can be edited.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.ReadOnly", "A value indicating whether the Text in the C1Editor can be edited.")]
		[DefaultValue(false)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public bool ReadOnly
		{
			get
			{
				return this.GetPropertyValue<bool>("ReadOnly", false);
			}
			set
			{
				this.SetPropertyValue<bool>("ReadOnly", value);
			}
		}

		/// <summary>
		/// Show the path selector.
		/// </summary>
		[C1Description("C1Editor.ShowPathSelector", "Show the path selector.")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool ShowPathSelector
		{
			get
			{
				return this.GetPropertyValue<bool>("ShowPathSelector", true);
			}
			set
			{
				this.SetPropertyValue<bool>("ShowPathSelector", value);
			}
		}

		/// <summary>
		/// Show the footer.
		/// </summary>
		[C1Description("C1Editor.ShowFooter", "Show the footer.")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool ShowFooter
		{
			get
			{
				return this.GetPropertyValue<bool>("ShowFooter", true);
			}
			set
			{
				this.SetPropertyValue<bool>("ShowFooter", value);

				#if !EXTENDER
				if (this.DesignMode)
				{
					this.ChildControlsCreated = false;
				}
				
#endif
			}
		}

		/// <summary>
		/// Full screen mode. Set this property to true if you want to switch editor to FullScreen Mode.  All client area of the page will be covered by WebEditor.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.FullScreenMode", "Full screen mode. Set this property to true if you want to switch editor to FullScreen Mode.  All client area of the page will be covered by WebEditor.")]
		[DefaultValue(false)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public bool FullScreenMode
		{
			get
			{
				return this.GetPropertyValue<bool>("FullScreenMode", false);
			}
			set
			{
				this.SetPropertyValue<bool>("FullScreenMode", value);
			}
		}

		/// <summary>
		/// Full screen mode container ID. Use this property to specify container which will be used as the parent control for FullScreenMode instead of client's area on the web page.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.FullScreenModeContainerID", "Full screen mode container ID. Use this property to specify container which will be used as the parent control for FullScreenMode instead of client's area on the web page.")]
		[DefaultValue("")]
		[WidgetOption]
		[WidgetOptionName("fullScreenContainerSelector")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string FullScreenModeContainerID
		{
			get
			{
				return this.GetPropertyValue<string>("FullScreenModeContainerID", "");
			}
			set
			{
				this.SetPropertyValue<string>("FullScreenModeContainerID", value);
			}
		}

        /// <summary>
        /// Determine if the custom context menu should be shown.
        /// </summary>
        [C1Description("C1Editor.CustomContextMenu", "Determine if the custom context menu should be shown.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
        [WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
        public bool CustomContextMenu
        {
            get
            {
                return this.GetPropertyValue<bool>("CustomContextMenu", true);
            }
            set
            {
                this.SetPropertyValue<bool>("CustomContextMenu", value);
            }
        }
		#endregion

		#region ** methods for serialization
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeCustomFontNames()
		{
			return CustomFontNames.Count > 0;
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeCustomFontSizes() 
		{
			return CustomFontSizes.Count > 0;
		}
		#endregion
		//CustomContextMenu

		/*/// <summary>
		/// Gets the instance of the class <seealso cref="C1RibbonUI"/>.
		/// </summary>
		[Browsable(false)]
		public C1RibbonUI RibbonUI
		{
			get
			{
				if (_ribbonUI == null)
				{
					//_ribbonUI = new C1RibbonUI(this, IsDesignMode);
					_ribbonUI = new C1RibbonUI(this, false);
				}

				return _ribbonUI;
			}
		} */
	}
}
