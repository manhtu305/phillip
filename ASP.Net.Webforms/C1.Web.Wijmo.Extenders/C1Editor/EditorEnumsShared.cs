﻿using System;
using System.Collections.Generic;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Editor
#else
namespace C1.Web.Wijmo.Controls.C1Editor
#endif
{

	/// <summary>
	/// Represents editor mode (WYSIWYG/Code/Split).
	/// </summary>
	public enum EditorMode
	{
		/// <summary>
		/// WYSIWYG mode.
		/// </summary>
		WYSIWYG = 0,
		/// <summary>
		/// Code editing mode.
		/// </summary>
		Code = 1,
		/// <summary>
		/// Split mode.
		/// </summary>
		Split = 2
	}

	/// <summary>
	/// An enum value represents the mode of the C1Editor.
	/// </summary>
	public enum Mode
	{
		/// <summary>
		/// Ribbon mode.
		/// </summary>
		Ribbon = 0,
		/// <summary>
		/// Simple mode.
		/// </summary>
		Simple = 1,
		/// <summary>
		/// BBCode mode.
		/// </summary>
		Bbcode = 2
	}
}
