﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Editor
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Editor
#endif
{
	/// <summary>
	/// A class used for customizing the fontSize and fontName.
	/// </summary>
	public class FontOption:Settings, IJsonEmptiable
	{

		#region ** constructor
		/// <summary>
		///  Initializes a new instance of the FontOption class.
		/// </summary>
		public FontOption()
		{

		}

		/// <summary>
		/// Initializes a new instance of the FontOption class, if the name and text is the same, using this constructor.
		/// </summary>
		/// <param name="name"></param>
		public FontOption(string name) 
		{
			this.Name = name;
			this.Text = name;
		}

		/// <summary>
		/// Initializes a new instance of the FontOption class.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="text"></param>
		public FontOption(string name, string text) 
		{
			this.Name = name;
			this.Text = text;
		}

		/// <summary>
		/// Initializes a new instance of the FontOption class.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="text"></param>
		/// <param name="tooltip"></param>
		public FontOption(string name, string text, string tooltip) 
		{
			this.Name = name;
			this.Text = text;
			this.Tooltip = tooltip;
		}
		#endregion

		#region ** properties
		/// <summary>
		/// The Name of fontName/fontSize
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.FontOption.Name")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string Name 
		{
			get 
			{
				return GetPropertyValue("Name", "");
			}
			set 
			{
				SetPropertyValue("Name", value);
			}
		}

		/// <summary>
		/// The display text of fontName/fontSize
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.FontOption.Text")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string Text 
		{
			get
			{
				return GetPropertyValue("Text", "");
			}
			set
			{
				SetPropertyValue("Text", value);
			}
		}

		/// <summary>
		/// The tooltip of fontName/fontSize
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.FontOption.Tooltip")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[WidgetOptionName("tip")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string Tooltip 
		{
			get 
			{
				return GetPropertyValue("Tooltip", "");
			}
			set
			{
				SetPropertyValue("Tooltip", value);
			}
		}

		bool IJsonEmptiable.IsEmpty
		{
			get { return string.IsNullOrEmpty(Name) && string.IsNullOrEmpty(Text) && string.IsNullOrEmpty(Tooltip); }
		}
		#endregion
	}
}
