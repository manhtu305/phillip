﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders
#else
using C1.Web.Wijmo.Controls.Localization;
using System.Collections;
namespace C1.Web.Wijmo.Controls
#endif
{
    /// <summary>
    /// It defines the option which can be a javascript function or string. It used in chart and tooltip.
    /// </summary>
    public class ContentOption : Settings, IFunctionType, IJsonEmptiable
    {

        #region ** properties
        /// <summary>
        /// Gets or sets the function.
        /// </summary>
        [DefaultValue("")]
        [C1Description("ContentOption.Function")]
        [NotifyParentProperty(true)]
        public string Function
        {
            get
            {
                return GetPropertyValue<string>("Function", "");
            }
            set
            {
                SetPropertyValue<string>("Function", value);
            }
        }

        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        [DefaultValue("")]
        [C1Description("ContentOption.Content")]
        [NotifyParentProperty(true)]
        public string Content
        {
            get
            {
                return GetPropertyValue<string>("Content", string.Empty);
            }
            set
            {
                SetPropertyValue<string>("Content", value);
            }
        }
        #endregion

        #region ** ICustomOptionType
        string ICustomOptionType.SerializeValue()
        {
            if (!string.IsNullOrEmpty(Function))
            {
                return Function;
            }
            return Content;
        }
        
#if !EXTENDER
        void ICustomOptionType.DeSerializeValue(object state)
        {
            string value = (string)state;
            if (string.IsNullOrEmpty(Function))
            {
                if (value != null)
                {
                    if (value.StartsWith("function"))
                    {
                        Function = value;
                        Content = "";
                    }
                    else
                    {
                        Content = value;
                    }
                }
            }
            else
            {
                Content = "";
            }
        }
#endif


        bool ICustomOptionType.IncludeQuotes
        {
            get 
            {
                if (!string.IsNullOrEmpty(Function))
                {
                    return false;
                }
                return true;            
            }
        }
        #endregion

        #region ** IJsonEmptiable
        bool IJsonEmptiable.IsEmpty
        {
            get
            {
                return string.IsNullOrEmpty(Function) && string.IsNullOrEmpty(Content);
            }
        }
        #endregion
    }
}
