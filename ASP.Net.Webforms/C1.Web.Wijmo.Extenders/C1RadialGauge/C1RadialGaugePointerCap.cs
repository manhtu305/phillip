﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if !EXTENDER
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Gauge
#else
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Gauge
#endif
{
	/// <summary>
    /// Represents the C1RadialGaugePointerCap class which contains all settings for the gauge's cap option.
	/// </summary>
	public class C1RadialGaugePointerCap:Settings, IJsonEmptiable
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="C1RadialGaugePointerCap"/> class.
		/// </summary>
		public C1RadialGaugePointerCap()
		{
			this.PointerCapStyle = new C1Chart.ChartStyle();
		}

		/// <summary>
		/// Gets or sets the radius of the cap.
		/// </summary>
		[DefaultValue(15)]
		[C1Description("C1RadialGaugePointerCap.Radius")]
		[WidgetOption]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int Radius
		{
			get
			{
				return GetPropertyValue<int>("Radius", 15);
			}
			set
			{
				SetPropertyValue<int>("Radius", value);
			}
		}

		/// <summary>
		/// Gets or sets the style of the cap.
		/// </summary>
		[WidgetOption]
		[C1Description("C1RadialGaugePointerCap.Style")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOptionName("style")]
#if !EXTENDER
		[Layout(LayoutType.Styles)]
#endif
		public C1Chart.ChartStyle PointerCapStyle
		{
			get;
			set;
		}

		/// <summary>
		/// A value that indicate whether the cap shows behind of the pointer.
		/// </summary>
		[WidgetOption]
		[C1Description("C1RadialGaugePointerCap.BehindPointer")]
		[DefaultValue(false)]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool BehindPointer
		{
			get
			{
				return GetPropertyValue<bool>("BehindPointer", false);
			}
			set
			{
				SetPropertyValue<bool>("BehindPointer", value);
			}
		}

		/// <summary>
		/// A value that indicate whether show the cap.
		/// </summary>
		[WidgetOption]
		[C1Description("C1RadialGaugePointerCap.Visible")]
		[DefaultValue(true)]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Visible
		{ 
			get
			{
				return GetPropertyValue<bool>("Visible", true);
			}
			set
			{
				SetPropertyValue<bool>("Visible", value);
			}
		}

		/// <summary>
		/// Gets or sets the template of the cap.
		/// </summary>
		[WidgetEvent]
		[C1Description("C1RadialGaugePointerCap.Template")]
		[DefaultValue("")]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string Template
		{
			get
			{
				return GetPropertyValue<string>("Template", string.Empty);
			}
			set
			{
				SetPropertyValue<string>("Template", value);
			}
		}

		internal bool ShouldSeralize()
		{
			return this.PointerCapStyle.ShouldSerialize()
				|| this.BehindPointer != false
				|| this.Radius != 15
				|| this.Visible != true
				|| this.Template != string.Empty;
		}

		bool IJsonEmptiable.IsEmpty
		{
			get 
			{
				return !this.ShouldSeralize();
			}
		}
	}
}
