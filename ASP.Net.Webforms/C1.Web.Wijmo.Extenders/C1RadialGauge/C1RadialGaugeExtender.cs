﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Drawing;

namespace C1.Web.Wijmo.Extenders.C1Gauge
{
	[TargetControlType(typeof(Control))]	
	[ToolboxItem(true)]
    [ToolboxBitmap(typeof(C1RadialGaugeExtender), "C1RadialGauge.png")]
    [LicenseProviderAttribute()]
	public partial class C1RadialGaugeExtender : C1GaugeExtender
	{
		private bool _productLicensed = false;

		public C1RadialGaugeExtender(): base()
		{
			VerifyLicense();
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1RadialGaugeExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		/// <summary>
		/// Gets or sets the width of the radialGauge.
		/// </summary>
		[DefaultValue(800)]
		public override int Width
		{
			get
			{
				return GetPropertyValue<int>("Width", 800);
			}
			set
			{
				base.Width = value;
			}
		}

		/// <summary>
		/// Gets or sets the height of the radialGauge.
		/// </summary>
		[DefaultValue(600)]
		public override int Height
		{
			get
			{
				return GetPropertyValue<int>("Height", 600);
			}
			set
			{
				base.Height = value;
			}
		}

	}
}
