﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if !EXTENDER
using C1.Web.Wijmo.Controls.Localization;
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Gauge", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1Gauge
#else
using C1.Web.Wijmo.Extenders.Localization;
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Gauge", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Gauge
#endif
{
	[WidgetDependencies(
        typeof(WijRadialGauge)
#if !EXTENDER
, "extensions.c1radialgauge.js",
ResourcesConst.C1WRAPPER_PRO
#endif
)]
#if !EXTENDER
	public partial class C1RadialGauge
#else
	public partial class C1RadialGaugeExtender
#endif
	{
		

		#region ** Options
		/// <summary>
		/// Gets or sets the radialGauge's radius.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1RadialGauge.Radius")]
		[WidgetOption]
		[DefaultValue(15)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int Radius
		{
			get
			{
				return GetPropertyValue<int>("Radius", 15);
			}
			set
			{
				SetPropertyValue<int>("Radius", value);
			}
		}

		/// <summary>
		/// Gets or sets the radialGauge's start angle.
		/// </summary>
		[WidgetOption]
		[C1Description("C1RadialGauge.StartAngle")]
		[DefaultValue(0)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double StartAngle
		{
			get
			{
				return GetPropertyValue<double>("StartAngle", 0);
			}
			set
			{
				SetPropertyValue<double>("StartAngle", value);
			}
		}

		/// <summary>
		/// Gets or sets the sweep angle of the radialGauge.
		/// </summary>
		[WidgetOption]
		[DefaultValue(180)]
		[C1Description("C1RadialGauge.SweepAngle")]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public double SweepAngle
		{
			get
			{
				return GetPropertyValue<double>("SweepAngle", 180);
			}
			set
			{
				SetPropertyValue<double>("SweepAngle", value);
			}
		}

		/// <summary>
		/// Gets or sets the origin of the radialGauge.
		/// </summary>
		[WidgetOption]
		[C1Description("C1RadialGauge.Origin")]
		[C1Category("Category.Behavior")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Origin Origin
		{
			get;
			set;
		}

		

		/// <summary>
        /// Gets or sets the cap of the gauge.
		/// </summary>
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[C1Description("C1RadialGauge.Cap")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public C1RadialGaugePointerCap Cap
		{
			get;
			set;
		}
		#endregion

		#region ** methods
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected override void InitProperties()
		{
			base.InitProperties();
			this.Pointer = new GaugePointer();
			this.Pointer.Length = 0.8;
			this.Pointer.Width = 4;
			this.Pointer.PointerStyle = new C1Chart.ChartStyle();
			this.Pointer.Offset = 0.15;
			this.Pointer.Visible = true;

			this.Labels.Offset = 30;
			
			this.TickMajor.Position = Position.Inside;
			this.TickMajor.Offset = 27;

			this.TickMinor.Offset = 30;
			this.TickMinor.Position = Position.Inside;


			this.Origin = new Origin();
			this.Cap = new C1RadialGaugePointerCap();

		}
		#endregion

		#region ** Serialize
		
		/// <summary>
		/// Determine whether the Pointer property should be serialized to client side.
		/// </summary>
		/// <returns>Returns true if Pointer has values, otherwise return false.</returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializePointer()
		{
			return this.Pointer.Length != 0.8
				|| this.Pointer.Offset != 0.15
				|| this.Pointer.PointerStyle.ShouldSerialize()
				|| this.Pointer.Visible == false
				|| this.Pointer.Shape != Shape.Tri
				|| this.Pointer.Width != 4;
		}

		/// <summary>
		/// Determine whether the TickMinor property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns false if TickMinor has default values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override bool ShouldSerializeTickMajor()
		{
			return TickMajor.Factor != 2
				|| TickMajor.Interval != 10
				|| TickMajor.Position != Position.Inside
				|| TickMajor.Offset != 30
				|| TickMajor.Marker != Marker.Rect
				|| TickMajor.Visible != true; ;
		}

		/// <summary>
		/// Determine whether the TickMinor property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns false if TickMinor has default values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override bool ShouldSerializeTickMinor()
		{
			return TickMinor.Factor != 1
				|| TickMinor.Interval != 5
				|| TickMinor.Position != Position.Inside
				|| TickMinor.Offset != 27
				|| TickMinor.Marker != Marker.Rect
				|| TickMinor.Visible != false;
		}
		#endregion
	}

	/// <summary>
    /// Represents the C1RadialGaugePointerCap class which contains all settings for the gauge's origin option.
	/// </summary>
	public class Origin : Settings, IJsonEmptiable
	{
		#region ** Options
		/// <summary>
		/// Gets or sets the X percent value of the origin.
		/// </summary>
		[WidgetOption]
		[NotifyParentProperty(true)]
		[DefaultValue(0.5)]
		public double X
		{
			get
			{
				return GetPropertyValue<double>("X", 0.5);
			}
			set
			{
				double x = value;
				if (x < 0 || x > 1) {
					x = 0.5;
				}
				SetPropertyValue<double>("X", x);
			}
		}

		/// <summary>
		/// Gets or sets the Y percent value of the origin.
		/// </summary>
		[WidgetOption]
		[DefaultValue(0.5)]
		[NotifyParentProperty(true)]
		public double Y
		{
			get
			{
				return GetPropertyValue<double>("Y", 0.5);
			}
			set
			{
				double y = value;
				if (y < 0 || y > 1) {
					y = 0.5;
				}
				SetPropertyValue<double>("Y", y);
			}
		}
		#endregion

		bool IJsonEmptiable.IsEmpty
		{
			get
			{
				return X == 0.5
					&& Y == 0.5;
			}
		}
	}
}
