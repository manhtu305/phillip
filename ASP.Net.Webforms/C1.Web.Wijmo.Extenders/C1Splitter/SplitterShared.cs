﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.WebControls;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Splitter", "wijmo")] 
namespace C1.Web.Wijmo.Extenders.C1Splitter
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Splitter", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1Splitter
#endif
{
    [WidgetDependencies(
        typeof(WijSplitter)
#if !EXTENDER
, "extensions.c1splitter.js",
ResourcesConst.C1WRAPPER_OPEN
#endif
)]
#if EXTENDER 
	public partial class C1SplitterExtender
#else
    public partial class C1Splitter
#endif
    {

        #region ** members
        private SplitterPanel _panel1 = null;
        private SplitterPanel _panel2 = null;
        private SplitterResizeSettings _resizeSettings = null;
        #endregion end of ** members.

        #region ** properties
        #region ** options
        /// <summary>
        /// A value determines whether the expander of Splitter
        /// is allowed to be shown.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Splitter.ShowExpander")]
        [DefaultValue(true)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool ShowExpander
        {
            get
            {
                return this.GetPropertyValue("ShowExpander", true);
            }
            set
            {
                this.SetPropertyValue("ShowExpander", value);
            }
        }

        /// <summary>
        /// Specifies which panel should be collapsed after clicking the expander of splitter.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Splitter.CollapsingPanel")]
        [DefaultValue(CollapsingPanel.Panel1)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public CollapsingPanel CollapsingPanel
        {
            get
            {
                return this.GetPropertyValue("CollapsingPanel", CollapsingPanel.Panel1);
            }
            set
            {
                this.SetPropertyValue("CollapsingPanel", value);
            }
        }

        /// <summary>
        /// A value indicates the location of the splitter, in pixels,
        /// from the left or top edge of the splitter.
        /// </summary>
        [C1Category("Category.Appearance")]
        [C1Description("C1Splitter.SplitterDistance")]
        [DefaultValue(100)]
        [WidgetOption]
        [NotifyParentProperty(true)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public int SplitterDistance
        {
            get
            {
                return this.GetPropertyValue("SplitterDistance", 100);
            }
            set
            {
                if (value < 0)
                {
                    value = 0;
                }

                this.SetPropertyValue("SplitterDistance", value);
            }
        }

        /// <summary>
        /// A value indicating the horizontal or vertical orientation
        /// of the splitter panels.
        /// </summary>
        [C1Category("Category.Appearance")]
        [C1Description("C1Splitter.Orientation")]
        [DefaultValue(Orientation.Vertical)]
        [WidgetOption]
        [NotifyParentProperty(true)]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public Orientation Orientation
        {
            get
            {
                return this.GetPropertyValue("Orientation", Orientation.Vertical);
            }
            set
            {
                this.SetPropertyValue("Orientation", value);
            }
        }

        /// <summary>
        /// A value that indicates whether or not the control is full of document.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Splitter.FullSplit")]
        [DefaultValue(false)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public bool FullSplit
        {
            get
            {
                return this.GetPropertyValue("FullSplit", false);
            }
            set
            {
                this.SetPropertyValue("FullSplit", value);
            }
        }

        /// <summary>
        /// A value indicates the z-index of Splitter bar.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Splitter.BarZIndex")]
        [DefaultValue(-1)]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public int BarZIndex
        {
            get
            {
                return this.GetPropertyValue("BarZIndex", -1);
            }
            set
            {
                this.SetPropertyValue("BarZIndex", value);
            }
        }

        /// <summary>
        /// Defines the information for top or left panel of splitter.
        /// </summary>
        [C1Category("Category.Appearance")]
        [C1Description("C1Splitter.Panel1")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public SplitterPanel Panel1
        {
            get
            {
                if (this._panel1 == null)
                {
                    this._panel1 = new SplitterPanel();
                    this._panel1.PropertyChanged += new PropertyChangedEventHandler(Panel1_PropertyChanged);
                }

                return this._panel1;
            }
            set
            {
                this._panel1 = value;
                this._panel1.PropertyChanged += new PropertyChangedEventHandler(Panel1_PropertyChanged);
            }
        }

        /// <summary>
        /// Defines the information for bottom or right panel of splitter.
        /// </summary>
        [C1Category("Category.Appearance")]
        [C1Description("C1Splitter.Panel2")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Appearance)]
#endif
        public SplitterPanel Panel2
        {
            get
            {
                if (this._panel2 == null)
                {
                    this._panel2 = new SplitterPanel();
                    this._panel2.PropertyChanged += new PropertyChangedEventHandler(Panel2_PropertyChanged);
                }

                return this._panel2;
            }
            set
            {
                this._panel2 = value;
                this._panel2.PropertyChanged += new PropertyChangedEventHandler(Panel2_PropertyChanged);
            }
        }

        /// <summary>
        /// A value defines the animation while the bar of splitter 
        /// is beeing dragged.
        /// </summary>
        [C1Category("Category.Behavior")]
        [C1Description("C1Splitter.ResizeSettings")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [NotifyParentProperty(true)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public SplitterResizeSettings ResizeSettings
        {
            get
            {
                if (this._resizeSettings == null)
                {
                    this._resizeSettings = new SplitterResizeSettings();
                }

                return this._resizeSettings;
            }
            set
            {
                this._resizeSettings = value;
            }
        }
        #endregion end of ** options.

        #region ** client events
        /// <summary>
        /// Gets or sets the javascript function name that would be called when the splitter is loaded.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Splitter.OnClientLoad")]
        [DefaultValue("")]
        [WidgetEvent]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientLoad
        {
            get
            {
                return GetPropertyValue("OnClientLoad", "");
            }
            set
            {
                SetPropertyValue("OnClientLoad", value);
            }
        }

        /// <summary>
        /// Gets or sets the javascript function name that would be called before panel1 is expanded out.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Splitter.OnClientExpand")]
        [DefaultValue("")]
        [WidgetEvent]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientExpand
        {
            get
            {
                return GetPropertyValue("OnClientExpand", "");
            }
            set
            {
                SetPropertyValue("OnClientExpand", value);
            }
        }

        /// <summary>
        /// Gets or sets the javascript function name that would be called before panel1 is collapsed.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Splitter.OnClientCollapse")]
        [DefaultValue("")]
        [WidgetEvent]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientCollapse
        {
            get
            {
                return GetPropertyValue("OnClientCollapse", "");
            }
            set
            {
                SetPropertyValue("OnClientCollapse", value);
            }
        }

        /// <summary>
        /// Gets or sets the javascript function name that would be called when panel1 is expanded out by clicking the collapse/expand image.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Splitter.OnClientExpanded")]
        [DefaultValue("")]
        [WidgetEvent]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientExpanded
        {
            get
            {
                return GetPropertyValue("OnClientExpanded", "");
            }
            set
            {
                SetPropertyValue("OnClientExpanded", value);
            }
        }

        /// <summary>
        /// Gets or sets the javascript function name that would be called when panel1 is collapsed by clicking the collapse/expand image.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Splitter.OnClientCollapsed")]
        [DefaultValue("")]
        [WidgetEvent]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientCollapsed
        {
            get
            {
                return GetPropertyValue("OnClientCollapsed", "");
            }
            set
            {
                SetPropertyValue("OnClientCollapsed", value);
            }
        }

        /// <summary>
        /// Gets or sets the javascript function name that 
        /// would be called at client side when dragging the splitter.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Splitter.OnClientSizing")]
        [DefaultValue("")]
        [WidgetEvent]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientSizing
        {
            get
            {
                return GetPropertyValue("OnClientSizing", "");
            }
            set
            {
                SetPropertyValue("OnClientSizing", value);
            }
        }

        /// <summary>
        /// Gets or sets the javascript function name that 
        /// would be called at client side when finish dragging the splitter.
        /// </summary>
        [C1Category("Category.ClientSideEvents")]
        [C1Description("C1Splitter.OnClientSized")]
        [DefaultValue("")]
        [WidgetEvent]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientSized
        {
            get
            {
                return GetPropertyValue("OnClientSized", "");
            }
            set
            {
                SetPropertyValue("OnClientSized", value);
            }
        }
        #endregion end of ** client events.
        #endregion end of ** properties.

        #region ** methods
        #region ** methods for events
        private void Panel1_PropertyChanged(object sender, PropertyChangedEventArgs eArgs)
        {
            if (this.Panel1.Collapsed && this.Panel2.Collapsed)
            {
                this.Panel2.Collapsed = false;
            }
        }

        private void Panel2_PropertyChanged(object sender, PropertyChangedEventArgs eArgs)
        {
            if (this.Panel2.Collapsed)
            {
                if (this.Panel1.Collapsed)
                {
                    this.Panel1.Collapsed = false;
                }
            }
        }
        #endregion end of ** methods for events.

        #region ** methods for serialization
        /// <summary>
        /// Determine whether the Panel1 property should be serialized to client side.
        /// </summary>
        /// <returns>
        /// Returns true if any subproperty is changed, otherwise return false.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializePanel1()
        {
            return this.Panel1.Collapsed ||
                this.Panel1.MinSize != 1 ||
                this.Panel1.ScrollBars != ScrollBars.Auto;
        }

        /// <summary>
        /// Determine whether the Panel2 property should be serialized to client side.
        /// </summary>
        /// <returns>
        /// Returns true if any subproperty is changed, otherwise return false.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializePanel2()
        {
            return this.Panel2.Collapsed ||
                this.Panel2.MinSize != 1 ||
                this.Panel2.ScrollBars != ScrollBars.Auto;
        }

        /// <summary>
        /// Determine whether the ResizeSettings property should be serialized to client side.
        /// </summary>
        /// <returns>
        /// Returns true if any subproperty is changed, otherwise return false.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeResizeSettings()
        {
            return this.ResizeSettings.Ghost ||
                this.ResizeSettings.ShouldSerializeAnimationOptions();
        }
        #endregion end of ** methods for serialization.
        #endregion end of ** methods.
    }

    public enum CollapsingPanel
    {
        /// <summary>
        /// Panel1
        /// </summary>
        Panel1 = 0,
        /// <summary>
        /// Panel2
        /// </summary>
        Panel2 = 1
    }
}