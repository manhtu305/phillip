﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI.WebControls;
using System.Web.UI;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Splitter
#else
namespace C1.Web.Wijmo.Controls.C1Splitter
#endif
{

	#region ** PropertyChangedEventArgs class
	internal class PropertyChangedEventArgs
	{

		public PropertyChangedEventArgs(string propertyName, object newValue, object oldValue)
		{
			this.PropertyName = propertyName;
			this.NewValue = newValue;
			this.OldValue = oldValue;
		}

		public string PropertyName
		{
			get;
			set;
		}

		public object NewValue
		{
			get;
			set;
		}

		public object OldValue
		{
			get;
			set;
		}
	}
	#endregion end of ** PropertyChangedEventArgs class.

	internal delegate void PropertyChangedEventHandler(object sender, PropertyChangedEventArgs eArgs);

	/// <summary>
	/// Defines the information for panels of splitter.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	[Serializable()]
	public partial class SplitterPanel : Settings
	{

		#region ** fields
		internal event PropertyChangedEventHandler PropertyChanged = null;
		#endregion end of ** fields.

		#region ** constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="SplitterPanel"/> class.
		/// </summary>
		public SplitterPanel()
		{
		}
		#endregion end of ** constructor.

		#region ** properties
		/// <summary>
		/// Gets or sets the minimum distance in pixels when resizing the splitter.
		/// </summary>
		[DefaultValue(1)]
		[C1Description("C1Splitter.MinSize")]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public int MinSize
		{
			get
			{
				return this.GetPropertyValue<int>("MinSize", 1);
			}
			set
			{
				this.SetPropertyValue<int>("MinSize", value);
			}
		}

		/// <summary>
		/// Gets or sets the type of scroll bars to display for splitter panel.
		/// </summary>
		[DefaultValue(ScrollBars.Auto)]
		[C1Description("C1Splitter.ScrollBars")]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public ScrollBars ScrollBars
		{
			get
			{
				return this.GetPropertyValue<ScrollBars>("ScrollBars", ScrollBars.Auto);
			}
			set
			{
				this.SetPropertyValue<ScrollBars>("ScrollBars", value);
			}
		}

		/// <summary>
		/// Gets or sets a value determining whether splitter panel is collapsed or expanded. 
		/// </summary>
		[DefaultValue(false)]
		[C1Description("C1Splitter.Collapsed")]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Collapsed
		{
			get
			{
				return this.GetPropertyValue<bool>("Collapsed", false);
			}
			set
			{
				if (this.Collapsed != value)
				{
					this.SetPropertyValue<bool>("Collapsed", value);
					this.OnPropertyChanged("Collapsed", value, this.Collapsed);
				}
			}
		}
		#endregion end of ** properties.

		#region ** private methods
		private void OnPropertyChanged(string propName, object newValue, object oldValue)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propName, newValue, oldValue));
			}
		}
		#endregion end of ** private methods.
	}
}
