﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Splitter
#else
namespace C1.Web.Wijmo.Controls.C1Splitter
#endif
{

	/// <summary>
	/// A value defines the animation while the bar of splitter is beeing dragged.
	/// </summary>
	public class SplitterResizeSettings : Settings
	{

		private SlideAnimation _animation = null;

		/// <summary>
		/// Initializes a new instance of the <see cref="SplitterResizeSettings"/> class.
		/// </summary>
		public SplitterResizeSettings()
		{
		}

		#region ** properties
		/// <summary>
		/// The options parameter of the jQuery's animation.
		/// </summary>
		[C1Description("C1Splitter.SplitterResizeSettings.AnimationOptions")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public SlideAnimation AnimationOptions
		{
			get
			{
				if (this._animation == null)
				{
					this._animation = new SlideAnimation();
				}

				return this._animation;
			}
			set
			{
				this._animation = value;
			}
		}

		/// <summary>
		/// A value that determines whether an outline of the element is sized.
		/// </summary>
		[DefaultValue(false)]
		[C1Description("C1Splitter.SplitterResizeSettings.Ghost")]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Ghost
		{
			get
			{
				return this.GetPropertyValue<bool>("Ghost", false);
			}
			set
			{
				this.SetPropertyValue<bool>("Ghost", value);
			}
		} 
		#endregion end of ** properties.

		#region ** methods for serialization
		/// <summary>
		/// Determines whether the AnimationOptions property should be serialized.
		/// </summary>
		/// <returns>
		/// Return true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeAnimationOptions()
		{
			return this.AnimationOptions.Disabled != false
				|| this.AnimationOptions.Duration != 400
				|| this.AnimationOptions.Easing != Easing.Swing;
		} 
		#endregion end of ** methods for serialization.
	}
}
