﻿using System;
using System.Collections.Generic;

namespace C1.Web.Wijmo.Extenders.C1Grid
{
	/// <summary>
	/// Represents a collection of <see cref="CustomFilterOperator"/> objects.
	/// </summary>
	public class CustomFilterOperatorCollection : List<CustomFilterOperator>, IJsonEmptiable
	{
		#region IJsonEmptiable Members

		bool IJsonEmptiable.IsEmpty
		{
			get { return Count == 0; }
		}

		#endregion
	}
}
