﻿using System;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Grid
#else
namespace C1.Web.Wijmo.Controls.C1GridView
#endif
{
	using System.ComponentModel;

	/// <summary>
	/// Infrastructure.
	/// </summary>
	[EditorBrowsable(EditorBrowsableState.Never)]
	public interface IOwnerable
	{
		/// <summary>
		/// Owner.
		/// </summary>
		IOwnerable Owner
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Infrastructure.
	/// </summary>
	[EditorBrowsable(EditorBrowsableState.Never)]
	public interface IColumnsContainer : IOwnerable
	{
		/// <summary>
		/// Gets a collection of objects that represent the columns.
		/// </summary>
#if EXTENDER
		C1BaseFieldCollection
#else
		C1BaseFieldCollection
#endif
			Columns
		{
			get;
		}
	}
}