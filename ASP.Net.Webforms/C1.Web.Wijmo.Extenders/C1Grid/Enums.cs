﻿using System;

namespace C1.Web.Wijmo.Extenders.C1Grid
{
	/// <summary>
	/// Determines behavior for column autogeneration.
	/// </summary>
	public enum ColumnsAutogenerationMode
	{
		/// <summary>
		/// Column auto-generation is turned off.
		/// </summary>
		None = 0,

		/// <summary>
		/// A column will be generated for each data field and added to the end of the columns collection.
		/// </summary>
		Append = 1,

		/// <summary>
		/// Each column having dataKey option not specified will be automatically bound to the first unreserved data field.
		/// For each data field not bound to any column a new column will be generated and added to the end of the columns collection.
		/// To prevent automatic binding of a column to a data field set its dataKey option to null.
		/// </summary>
		Merge = 2
	}

	/// <summary>
	/// Determines the order of items in the filter dropdown list.
	/// </summary>
	/// <remarks>
	/// The "NoFilter" operator is always first.
	/// </remarks>
	public enum FilterOperatorsSortMode
	{
		/// <summary>
		/// Operators follow the order of addition, built-in operators goes before custom ones.
		/// </summary>
		None = 0,

		/// <summary>
		/// Operators are sorted alphabetically.
		/// </summary>
		Alphabetical = 1,

		/// <summary>
		/// Operators are sorted alphabetically with custom operators going before built-in ones.
		/// </summary>
		AlphabeticalCustomFirst = 2,

		/// <summary>
		/// Operators are sorted alphabetically with built-in operators going before custom operators.
		/// </summary>
		AlphabeticalEmbeddedFirst
	}

	/// <summary>
	/// Data type.
	/// </summary>
	[Flags]
	public enum FieldDataType
	{
		/// <summary>
		/// String.
		/// </summary>
		String = 1,

		/// <summary>
		/// Number.
		/// </summary>
		Number = 2,

		/// <summary>
		/// Datetime.
		/// </summary>
		Datetime = 4,

		/// <summary>
		/// Currency.
		/// </summary>
		Currency = 8,

		/// <summary>
		/// Boolean.
		/// </summary>
		Boolean = 16
	}

	/// <summary>
	/// Determines the editing mode.
	/// </summary>
	public enum EditingMode
	{
		/// <summary>
		/// The editing ability is disabled.
		/// </summary>
		None = 0,

		/// <summary>
		/// A single cell can be edited via a double click.
		/// </summary>
		Cell = 1,

		/// <summary>
		/// A whole row can be edited via a command column.
		/// </summary>
		Row = 2
	}
}

