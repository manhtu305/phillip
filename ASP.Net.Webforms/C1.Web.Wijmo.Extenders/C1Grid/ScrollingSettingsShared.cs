﻿using System;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Grid
#else
namespace C1.Web.Wijmo.Controls.C1GridView
#endif
{
	using System.ComponentModel;
	using System.Web.UI;
 
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;

#else
	using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// Represents a scrolling settings.
	/// </summary>
	[PersistChildren(false), ParseChildren(true)]
	public sealed partial class ScrollingSettings : IJsonEmptiable
	{
		internal const FreezingMode DEF_FREEZINGMODE = FreezingMode.None;
		internal const int DEF_STATICCOLUMNINDEX = -1;
		internal const int DEF_STATICROWINDEX = -1;
		internal const ScrollMode DEF_MODE = ScrollMode.None;

		private VirtualizationSettings _virtualizationSettings = null;

		/// <summary>
		/// Determines the freezing mode.
		/// </summary>
		[DefaultValue(DEF_FREEZINGMODE)]
		[C1Description("C1Grid.ScrollingSettings.FreezingMode")]
		[C1Category("Category.Behavior")]
		[Json(true, true, DEF_FREEZINGMODE)]
		[WidgetOption]
		[NotifyParentProperty(true)]
		public FreezingMode FreezingMode
		{
			get { return GetPropertyValue("FreezingMode", DEF_FREEZINGMODE); }
			set
			{
				if (GetPropertyValue("FreezingMode", DEF_FREEZINGMODE) != value)
				{
					SetPropertyValue("FreezingMode", value);
					_OnPropertyChanged("FreezingMode"); 
				}
			}
		}

		/// <summary>
		/// Determines the scrolling mode.
		/// </summary>
		[DefaultValue(DEF_MODE)]
		[C1Description("C1Grid.ScrollingSettings.Mode")]
		[C1Category("Category.Behavior")]
		[Json(true, true, DEF_MODE)]
		[WidgetOption]
		[NotifyParentProperty(true)]
		public ScrollMode Mode
		{
			get { return GetPropertyValue("Mode", DEF_MODE); }
			set
			{
				if (GetPropertyValue("Mode", DEF_MODE) != value)
				{
					SetPropertyValue("Mode", value);
					_OnPropertyChanged("FreezingMode");
				}
			}
		}

		/// <summary>
		/// Indicates the index of columns that will always be shown on the left when the grid is scrolled horizontally.
		/// </summary>
		/// <value>
		/// Default value is -1.
		/// </value>
		/// <remarks>
		/// Note that all columns before the static column will be automatically marked as static too.
		/// It can only take effect when <see cref="Mode"/> is not "None"/>.
		/// It will be considered as -1 in a hierarchical grid or when grouping or row merging is enabled.
		/// -1 means no data column but row header is static.
		/// 0 means one data column and row header are static.
		/// </remarks>
		[DefaultValue(DEF_STATICCOLUMNINDEX)]
		[C1Description("C1Grid.ScrollingSettings.StaticColumnIndex")]
		[C1Category("Category.Behavior")]
		[Json(true, true, DEF_STATICCOLUMNINDEX)]
		[WidgetOption]
		[NotifyParentProperty(true)]
		public int StaticColumnIndex
		{
			get { return GetPropertyValue("StaticColumnIndex", DEF_STATICCOLUMNINDEX); }
			set
			{
				if (value < -1)
				{
					throw new ArgumentOutOfRangeException("value", "Must be greater than or equal to -1.");
				}

				if (GetPropertyValue("StaticColumnIndex", DEF_STATICCOLUMNINDEX) != value)
				{
					SetPropertyValue("StaticColumnIndex", value);
					_OnPropertyChanged("StaticColumnIndex");
				}
			}
		}

		/// <summary>
		/// Indicates the index of data rows that will always be shown on the top when the grid is scrolled vertically.
		/// </summary>
		/// <value>
		/// Default value is -1.
		/// </value>
		/// <remarks>
		/// Note, that all rows before the static row will be automatically marked as static too.
		/// It can only take effect when <see cref="Mode"/> is not "None"/>.
		/// It will be considered as -1 in a hierarchical grid or when grouping or row merging is enabled.
		/// -1 means no data row but header row is static.
		/// 0 means one data row and header row are static.
		/// </remarks>
		[DefaultValue(DEF_STATICROWINDEX)]
		[C1Description("C1Grid.ScrollingSettings.StaticRowIndex")]
		[C1Category("Category.Behavior")]
		[Json(true, true, DEF_STATICROWINDEX)]
		[WidgetOption]
		[NotifyParentProperty(true)]
		public int StaticRowIndex
		{
			get { return GetPropertyValue("StaticRowIndex", DEF_STATICROWINDEX); }
			set
			{
				if (value < -1)
				{
					throw new ArgumentOutOfRangeException("value", "Must be greater than or equal to -1.");
				}

				if (GetPropertyValue("StaticRowIndex", DEF_STATICROWINDEX) != value)
				{
					SetPropertyValue("StaticRowIndex", value);
					_OnPropertyChanged("StaticRowIndex");
				}
			}
		}


		/// <summary>
		/// Virtualization settings.
		/// </summary>
		[C1Description("C1Grid.ScrollingSettings.VirtualizationSettings")]
		[C1Category("Category.Behavior")]
		[Json(true, true, null)]
		[WidgetOption]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[NotifyParentProperty(true)]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public VirtualizationSettings VirtualizationSettings
		{
			get
			{
				if (_virtualizationSettings == null)
				{
					_virtualizationSettings = new VirtualizationSettings();
					_virtualizationSettings.PropertyChanged += delegate(object o, PropertyChangedEventArgs e)
					{
						_OnPropertyChanged("VirtualizationSettings." + e.PropertyName);
					};
				}

				return _virtualizationSettings;
			}
		}

		internal bool _IsVirtualizationSettingsCreated
		{
			get { return _virtualizationSettings != null; }
		}

		/// <summary>
		/// Occurs when a property of a <see cref="ScrollingSettings"/> object changes value.
		/// </summary>
		[Browsable(false)]
		public event PropertyChangedEventHandler PropertyChanged;

		private void _OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		#region IJsonEmptiable Members

		bool IJsonEmptiable.IsEmpty
		{
			get
			{
				return this.FreezingMode == DEF_FREEZINGMODE &&
					this.Mode == DEF_MODE &&
					this.StaticColumnIndex == DEF_STATICCOLUMNINDEX &&
					this.StaticRowIndex == DEF_STATICROWINDEX &&
					((IJsonEmptiable)this.VirtualizationSettings).IsEmpty;
			}
		}
		
		#endregion
	}
}
