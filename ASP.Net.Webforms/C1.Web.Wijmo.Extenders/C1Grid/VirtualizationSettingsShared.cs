﻿using System;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Grid
#else
namespace C1.Web.Wijmo.Controls.C1GridView
#endif
{
	using System.ComponentModel;
	using System.Web.UI;

#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
	using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// Represents a pager settings.
	/// </summary>
	[PersistChildren(false), ParseChildren(true)]
	public sealed partial class VirtualizationSettings : IJsonEmptiable
	{
		internal const VirtualizationMode DEF_MODE = VirtualizationMode.None;
		internal const int DEF_ROWHEIGHT = 19;
		private const int DEF_COLUMNWIDTH = 100;

		/// <summary>
		/// Determines the virtualization mode.
		/// </summary>
		[DefaultValue(DEF_MODE)]
		[C1Description("C1Grid.VirtualizationSettings.Mode")]
		[C1Category("Category.Behavior")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_MODE)]
		[WidgetOption]
		public VirtualizationMode Mode
		{
			get { return GetPropertyValue("Mode", DEF_MODE); }
			set
			{
				if (GetPropertyValue("Mode", DEF_MODE) != value)
				{
					SetPropertyValue("Mode", value);
					_OnPropertyChanged("Mode");
				}
			}
		}

		/// <summary>
		/// Determines the height of a rows when rows virtual scrolling is used.
		/// </summary>
		[DefaultValue(DEF_ROWHEIGHT)]
		[C1Description("C1Grid.VirtualizationSettings.RowHeight")]
		[C1Category("Category.Behavior")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_ROWHEIGHT)]
		[WidgetOption]
		public int RowHeight
		{
			get { return GetPropertyValue("RowHeight", DEF_ROWHEIGHT); }
			set
			{
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException("value", "Must be greater than or equal to zero.");
				}

				if (GetPropertyValue("RowHeight", DEF_ROWHEIGHT) != value)
				{
					SetPropertyValue("RowHeight", value);
					_OnPropertyChanged("RowHeight");
				}
			}
		}

		/// <summary>
		/// Determines the default width of a columns when columns virtual scrolling is used.
		/// </summary>
		[DefaultValue(DEF_COLUMNWIDTH)]
		[C1Description("C1Grid.VirtualizationSettings.ColumnWidth")]
		[C1Category("Category.Behavior")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_COLUMNWIDTH)]
		[WidgetOption]
		public int ColumnWidth
		{
			get { return GetPropertyValue("ColumnWidth", DEF_COLUMNWIDTH); }
			set
			{
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException("value", "Must be greater than or equal to zero.");
				}

				if (GetPropertyValue("ColumnWidth", DEF_COLUMNWIDTH) != value)
				{
					SetPropertyValue("ColumnWidth", value);
					_OnPropertyChanged("ColumnWidth");
				}
			}
		}

		/// <summary>
		/// Occurs when a property of a <see cref="VirtualizationSettings"/> object changes values.
		/// </summary>
		[Browsable(false)]
		public event PropertyChangedEventHandler PropertyChanged;

		private void _OnPropertyChanged(string propName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propName));
			}
		}

		#region IJsonEmptiable Members

		bool IJsonEmptiable.IsEmpty
		{
			get
			{
				return ColumnWidth == DEF_COLUMNWIDTH &&
					RowHeight == DEF_ROWHEIGHT &&
					Mode == DEF_MODE;
			}
		}

		#endregion
	}
}
