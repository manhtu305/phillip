﻿using System;


#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Grid
#else
namespace C1.Web.Wijmo.Controls.C1GridView
#endif
{
	/// <summary>
	/// Represents selection behavior.
	/// </summary>
	public enum SelectionMode
	{
		/// <summary>
		/// Selection is turned off.
		/// </summary>
		None = 0,

		/// <summary>
		/// Only a single cell can be selected at the same time.
		/// </summary>
		SingleCell = 1,

		/// <summary>
		/// Only a single column can be selected at the same time.
		/// </summary>
		SingleColumn = 2,

		/// <summary>
		/// Only a single row can be selected at the same time.
		/// </summary>
		SingleRow = 3,

		/// <summary>
		/// Only a single range of cells can be selected at the same time.
		/// </summary>
		SingleRange = 4,

		/// <summary>
		/// It is possible to select more than one row at the same time using the mouse and the CTRL or SHIFT keys.
		/// </summary>
		MultiColumn = 5,

		/// <summary>
		/// It is possible to select more than one row at the same time using the mouse and the CTRL or SHIFT keys.
		/// </summary>
		MultiRow = 6,

		/// <summary>
		/// It is possible to select more than one cells range at the same time using the mouse and the CTRL or SHIFT keys.
		/// </summary>
		MultiRange = 7
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the <see cref="C1Field.Aggregate"/> property.
	/// </summary>
	public enum Aggregate
	{
		/// <summary>
		/// No aggregate is calculated or displayed.
		/// </summary>
		None = 0,

		/// <summary>
		/// Count of non-empty values.
		/// </summary>
		Count = 1,

		/// <summary>
		/// Sum of numerical values.
		/// </summary>
		Sum = 2,

		/// <summary>
		/// Average of the numerical values.
		/// </summary>
		Average = 3,

		/// <summary>
		/// Minimum value (numerical, string, or date).
		/// </summary>
		Min = 4,

		/// <summary>
		/// Maximum value (numerical, string, or date).
		/// </summary>
		Max = 5,

		/// <summary>
		/// Standard deviation (using formula for Sample, n-1).
		/// </summary>
		Std = 6,

		/// <summary>
		/// Standard deviation (using formula for Population, n).
		/// </summary>
		StdPop = 7,

		/// <summary>
		/// Variance (using formula for Sample, n-1).
		/// </summary>
		Var = 8,

		/// <summary>
		/// Variance (using formula for Population, n).
		/// </summary>
		VarPop = 9,

#if EXTENDER
		/// <summary>
		/// Custom value (causing grid to throw the <see cref="C1GridExtender.OnClientGroupAggregate"/> event).
		/// </summary>
#else
		/// <summary>
		/// Custom value (causing grid to throw the <see cref="C1GridView.OnClientGroupAggregate"/> event).
		/// </summary>
#endif
		Custom = 10
	}

	/// <summary>
	/// The FilterOperator enumeration.
	/// </summary>
	public enum FilterOperator
	{
		/// <summary>
		/// No filter.
		/// </summary>
		NoFilter = 0,

#if EXTENDER
		/// <summary>
		/// Applicable to <see cref="FieldDataType.String"/> data type.
		/// </summary>
#else
		/// <summary>
		/// Contains item.
		/// </summary>
#endif
		Contains = 1,

#if EXTENDER
		/// <summary>
		/// Applicable to <see cref="FieldDataType.String"/> data type.
		/// </summary>
#else
		/// <summary>
		/// NotContain item.
		/// </summary>
#endif
		NotContain = 2,

#if EXTENDER
		/// <summary>
		/// Applicable to <see cref="FieldDataType.String"/> data type.
		/// </summary>
#else
		/// <summary>
		/// BeginsWith item.
		/// </summary>
#endif
		BeginsWith = 3,

#if EXTENDER
		/// <summary>
		/// Applicable to <see cref="FieldDataType.String"/> data type.
		/// </summary>
#else
		/// <summary>
		/// EndsWith item.
		/// </summary>
#endif
		EndsWith = 4,

#if EXTENDER
		/// <summary>
		/// Applicable to <see cref="FieldDataType.String"/>, <see cref="FieldDataType.Number"/>, <see cref="FieldDataType.Datetime"/>, <see cref="FieldDataType.Currency"/> and <see cref="FieldDataType.Boolean"/> data types.
		/// </summary>
#else
		/// <summary>
		/// Equals item.
		/// </summary>
#endif
		Equals = 5,

#if EXTENDER
		/// <summary>
		/// Applicable to <see cref="FieldDataType.String"/>, <see cref="FieldDataType.Number"/>, <see cref="FieldDataType.Datetime"/>, <see cref="FieldDataType.Currency"/> and <see cref="FieldDataType.Boolean"/> data types.
		/// </summary>
#else
		/// <summary>
		/// NotEqual item.
		/// </summary>
#endif
		NotEqual = 6,

#if EXTENDER
		/// <summary>
		/// Applicable to <see cref="FieldDataType.String"/>, <see cref="FieldDataType.Number"/>, <see cref="FieldDataType.Datetime"/>, <see cref="FieldDataType.Currency"/> and <see cref="FieldDataType.Boolean"/> data types.
		/// </summary>
#else
		/// <summary>
		/// Greater item.
		/// </summary>
#endif
		Greater = 7,

#if EXTENDER
		/// <summary>
		/// Applicable to <see cref="FieldDataType.String"/>, <see cref="FieldDataType.Number"/>, <see cref="FieldDataType.Datetime"/>, <see cref="FieldDataType.Currency"/> and <see cref="FieldDataType.Boolean"/> data types.
		/// </summary>
#else
		/// <summary>
		/// Less item.
		/// </summary>
#endif
		Less = 8,

#if EXTENDER
		/// <summary>
		/// Applicable to <see cref="FieldDataType.String"/>, <see cref="FieldDataType.Number"/>, <see cref="FieldDataType.Datetime"/>, <see cref="FieldDataType.Currency"/> and <see cref="FieldDataType.Boolean"/> data types.
		/// </summary>
#else
		/// <summary>
		/// GreaterOrEqual item.
		/// </summary>
#endif
		GreaterOrEqual = 9,

#if EXTENDER
		/// <summary>
		/// Applicable to <see cref="FieldDataType.String"/>, <see cref="FieldDataType.Number"/>, <see cref="FieldDataType.Datetime"/>, <see cref="FieldDataType.Currency"/> and <see cref="FieldDataType.Boolean"/> data types.
		/// </summary>
#else
		/// <summary>
		/// LessOrEqual item.
		/// </summary>
#endif
		LessOrEqual = 10,

#if EXTENDER
		/// <summary>
		/// Applicable to <see cref="FieldDataType.String"/> data type.
		/// </summary>
#else
		/// <summary>
		/// IsEmpty item.
		/// </summary>
#endif
		IsEmpty = 11,

#if EXTENDER
		/// <summary>
		/// Applicable to <see cref="FieldDataType.String"/> data type.
		/// </summary>
#else
		/// <summary>
		/// NotIsEmpty item.
		/// </summary>
#endif
		NotIsEmpty = 12,

#if EXTENDER
		/// <summary>
		/// Applicable to <see cref="FieldDataType.String"/>, <see cref="FieldDataType.Number"/>, <see cref="FieldDataType.Datetime"/>, <see cref="FieldDataType.Currency"/> and <see cref="FieldDataType.Boolean"/> data types.
		/// </summary>
#else
		/// <summary>
		/// IsNull item.
		/// </summary>
#endif
		IsNull = 13,

#if EXTENDER
		/// <summary>
		/// Applicable to <see cref="FieldDataType.String"/>, <see cref="FieldDataType.Number"/>, <see cref="FieldDataType.Datetime"/>, <see cref="FieldDataType.Currency"/> and <see cref="FieldDataType.Boolean"/> data types.
		/// </summary>
#else
		/// <summary>
		/// NotIsNull item.
		/// </summary>
#endif
		NotIsNull = 14,

		/// <summary>
		/// Custom item.
		/// </summary>
		Custom = 15
	}

	/// <summary>
	/// Controls the state of the input method editor for text fields.
	/// <remarks>
	/// Please refer to https://developer.mozilla.org/en-US/docs/Web/CSS/ime-mode for more info.
	/// </remarks>
	/// </summary>
	public enum IMEMode
	{
		/// <summary>
		/// No change is made to the current input method editor state.
		/// </summary>
		Auto = 1,

		/// <summary>
		/// The input method editor is initially active; text entry is performed using it unless the user specifically dismisses it.
		/// </summary>
		Active = 2,

		/// <summary>
		/// The input method editor is initially inactive, but the user may activate it if they wish.
		/// </summary>
		Inactive = 3,

		/// <summary>
		/// The input method editor is disabled and may not be activated by the user.
		/// </summary>
		Disabled = 4
	}

	/// <summary>
	/// Specifies row merging behaviour.
	/// </summary>
	public enum RowMerge
	{
		/// <summary>
		/// No row merging.
		/// </summary>
		None = 0,

		/// <summary>
		/// Allows row with identical text to merge.
		/// </summary>
		Free = 1,

		/// <summary>
		/// Keeps rows with identical text from merging if rows in the previous column are merged.
		/// </summary>
		Restricted = 2
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the <see cref="C1Field.SortDirection"/> property in the <see cref="C1Field"/> class.
	/// </summary>
	public enum C1SortDirection
	{
		/// <summary>
		/// No sorting.
		/// </summary>
		None = 0,

		/// <summary>
		/// Sort from smallest to largest.
		/// </summary>
		Ascending = 1,

		/// <summary>
		/// Sort from largest to smallest.
		/// </summary>
		Descending = 2
	}

	/// <summary>
	/// Determines the position of the group.
	/// </summary>
	public enum GroupPosition
	{
		/// <summary>
		/// Disables grouping for the column.
		/// </summary>
		None = 0,

		/// <summary>
		/// Inserts header rows.
		/// </summary>
		Header = 1,

		/// <summary>
		/// Inserts footer rows.
		/// </summary>
		Footer = 2,

		/// <summary>
		/// Inserts header and footer rows.
		/// </summary>
		HeaderAndFooter = 3
	}

	/// <summary>
	/// Use the members of this enumeration to set the value of the <see cref="GroupInfo.OutlineMode"/> property
	/// in the <see cref="GroupInfo"/> class.
	/// </summary>
	public enum OutlineMode
	{
		/// <summary>
		/// Disables collapsing and expanding.
		/// </summary>
		None = 0,

		/// <summary>
		/// Groups are initially collapsed.
		/// </summary>
		StartExpanded = 1,

		/// <summary>
		/// Groups are initially expanded.
		/// </summary>
		StartCollapsed = 2
	}

	/// <summary>
	/// Specifies the position of the pager.
	/// </summary>
	public enum PagerPosition
	{
		/// <summary>
		/// A pager positioned at the bottom of the control.
		/// </summary>
		Bottom = 0,

		/// <summary>
		/// A pager positioned at the top of the control.
		/// </summary>
		Top = 1,

		/// <summary>
		/// Pagers positioned at both the top and the bottom of the control.
		/// </summary>
		TopAndBottom = 2
	}

	/// <summary>
	/// Determines the scrolling mode.
	/// </summary>
	public enum ScrollMode
	{
		/// <summary>
		/// Scrolling is not used.
		/// </summary>
		None = 0,

		/// <summary>
		/// Scrollbars appear automatically depending upon content size.
		/// </summary>
		Auto = 1,

		/// <summary>
		/// Horizontal scrollbar is active.
		/// </summary>
		Horizontal = 2,

		/// <summary>
		/// Vertical scrollbar is active.
		/// </summary>
		Vertical = 3,

		/// <summary>
		/// Both horizontal and vertical scrollbars are active.
		/// </summary>
		Both = 4
	}

	/// <summary>
	/// Determines the virtualization mode.
	/// </summary>
	public enum VirtualizationMode
	{
		/// <summary>
		/// Virtual scrolling is not used.
		/// </summary>
		None = 0,

#if !EXTENDER
		/// <summary>
		/// Rows are rendered on demand.
		/// </summary>
		/// <remarks>
		/// <para>
		/// There are two modes of rows virtual scrolling determined by the <see cref="C1GridView.CallbackSettings"/> property.
		/// The first one is static when all the data are available to the client but rows are rendered when scrolled.
		/// The second one is dynamic, set by the <see cref="CallbackAction.Scrolling"/> value. New portions of data are requested from server when need to be rendered.
		/// </para>
		/// <para>
		/// Static mode cannot be used at the same time when paging, fixed rows or columns merging are used.
		/// Dynamic mode cannot work with columns grouping in addition.
		/// </para>
		/// </remarks>
		Rows = 1,
#else
		/// <summary>
		/// Rows are rendered on demand.
		/// </summary>
		/// <remarks>
		/// Cannot be used at the same time when when paging, fixed rows or columns merging are used.
		/// </remarks>
		Rows = 1,
#endif

		/// <summary>
		/// Columns are rendered on demand.
		/// </summary>
		/// <remarks>
		/// Cannot be used at the same time when grouping or band columns are used.
		/// </remarks>
		Columns = 2,

		/// <summary>
		/// Both Rows and Columns modes are used.
		/// </summary>
		Both = 3
	}

	/// <summary>
	/// Determines the freezing mode.
	/// </summary>
	public enum FreezingMode
	{
		/// <summary>
		/// The freezing handle cannot be dragged.
		/// </summary>
		None = 0,

		/// <summary>
		/// The user can drag the vertical freezing handle to change position of the static column.
		/// </summary>
		Columns = 1,

		/// <summary>
		/// The user can drag the horizontal freezing handle to change position of the static row.
		/// </summary>
		Rows = 2,

		/// <summary>
		/// The user can drag both horizontal and vertical freezing handles.
		/// </summary>
		Both = 3
	}

	/// <summary>
	/// Specifies the action to perform when the TAB key are pressed.
	/// </summary>
	public enum KeyActionEnum
	{
		/// <summary>
		/// Move to the next column. At the end of the row, wrap to the start of the next row.
		/// </summary>
		MoveAcross = 0,

		/// <summary>
		///Same as MoveAcross, but move the focus to the next control in the tab order when the last cell in the grid is reached.
		/// </summary>
		MoveAcrossOut = 1
	}
}
