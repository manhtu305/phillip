﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Globalization;
#if EXTENDER
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Grid", "wijmo")]

namespace C1.Web.Wijmo.Extenders.C1Grid
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1GridView", "wijmo")]

namespace C1.Web.Wijmo.Controls.C1GridView
#endif
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
	using C1.Web.Wijmo.Controls.Localization;
#endif

	[WidgetDependencies(
		typeof(WijGrid)
#if !EXTENDER
,
		typeof(C1GridDependencies.C1InputConditionalDependency),
		"extensions.c1gridview.js",
		ResourcesConst.C1WRAPPER_PRO,
		"extensions.c1gridview.css"
#endif
)]
	internal sealed class C1GridDependencies : WijmoBase
	{
#if !EXTENDER
		[WidgetConditionalDependency(typeof(C1Input.C1InputBase), typeof(C1GridView))]
		internal sealed class C1InputConditionalDependency : ConditionalDependencyProxy
		{
			protected internal override bool Resolve(IWijmoWidgetSupport instance, object dependency)
			{
				return ((C1GridView)instance).AllowC1InputEditors;
			}
		}
#endif
	}

	[WidgetDependencies(typeof(C1GridDependencies))]
#if EXTENDER
	public partial class C1GridExtender
#else
	public partial class C1GridView
#endif
 : IColumnsContainer, ICultureControl
	{

		#region const

		private const bool DEF_ALLOWCOLSIZING = false;
		private const bool DEF_ALLOWCOLMOVING = false;
		private const bool DEF_ALLOWKEYBOARDNAVIGATION = true;
		private const KeyActionEnum DEF_KEYACTIONTAB = KeyActionEnum.MoveAcross;
		private const bool DEF_ALLOWPAGING = false;
		private const bool DEF_ALLOWSORTING = false;
		private const string DEF_GROUPAREACAPTION = "Drag a column here to group by that column.";
		private const int DEF_GROUPINDENT = 10;
		private const bool DEF_HIGHLIGHTCURRENTCELL = false;
		private const string DEF_LOADINGTEXT = "Loading...";
		private const int DEF_PAGEINDEX = 0;
		private const int DEF_PAGESIZE = 10;
		private const bool DEF_SHOWFILTER = false;
		private const bool DEF_SHOWFOOTER = false;
		private const bool DEF_SHOWROWHEADER = false;
		private const bool DEF_SHOWGROUPAREA = false;
		private const int DEF_TOTALROWS = -1;

		#endregion

		#region private members
#if !EXTENDER
		internal int _pageIndex = DEF_PAGEINDEX;
#endif
		private PagerSettings _pagerSettings;
		private ScrollingSettings _scrollingSettings;

		#endregion

		#region options

		/// <summary>
		/// Gets or sets culture ID.
		/// </summary>
		[C1Description("C1Grid.Culture")]
		[C1Category("Category.Behavior")]
		[DefaultValue(typeof(CultureInfo), "en-US")]
		[TypeConverter(typeof(CultureInfoConverter))]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public CultureInfo Culture
		{
			get
			{
				return GetCulture(GetPropertyValue<CultureInfo>("Culture", null));
			}
			set
			{
				SetPropertyValue<CultureInfo>("Culture", value);
			}
		}

		/// <summary>
		/// A value that indicators the culture calendar to format the text. This property must work with Culture property.
		/// </summary>
		[C1Description("Culture.CultureCalendar")]
		[C1Category("Category.Appearance")]
		[DefaultValue("")]
		[WidgetOption]
		public string CultureCalendar
		{
			get
			{
				return GetPropertyValue<string>("CultureCalendar", "");
			}
			set
			{
				SetPropertyValue<string>("CultureCalendar", value);
			}
		}

		/// <summary>
		/// A value indicating whether columns can be sized.
		/// </summary>
		[DefaultValue(DEF_ALLOWCOLSIZING)]
		[C1Description("C1Grid.AllowColSizing")]
		[C1Category("Category.Behavior")]
		[Json(true, true, DEF_ALLOWCOLSIZING)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public virtual bool AllowColSizing
		{
			get { return GetPropertyValue("AllowColSizing", DEF_ALLOWCOLSIZING); }
			set
			{
				if (GetPropertyValue("AllowColSizing", DEF_ALLOWCOLSIZING) != value)
				{
					SetPropertyValue("AllowColSizing", value);
				}
			}
		}

		/// <summary>
		/// A value indicating whether columns can be moved.
		/// </summary>
		[DefaultValue(DEF_ALLOWCOLMOVING)]
		[C1Description("C1Grid.AllowColMoving")]
		[C1Category("Category.Behavior")]
		[Json(true, true, DEF_ALLOWCOLMOVING)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public virtual bool AllowColMoving
		{
			get { return GetPropertyValue("AllowColMoving", DEF_ALLOWCOLMOVING); }
			set
			{
				if (GetPropertyValue("AllowColMoving", DEF_ALLOWCOLMOVING) != value)
				{
					SetPropertyValue("AllowColMoving", value);
				}
			}
		}

		/// <summary>
		/// A value indicating whether keyboard navigation is allowed.
		/// </summary>
		[DefaultValue(DEF_ALLOWKEYBOARDNAVIGATION)]
		[C1Description("C1Grid.AllowKeyboardNavigation")]
		[C1Category("Category.Behavior")]
		[Json(true, true, DEF_ALLOWKEYBOARDNAVIGATION)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public virtual bool AllowKeyboardNavigation
		{
			get { return GetPropertyValue("AllowKeyboardNavigation", DEF_ALLOWKEYBOARDNAVIGATION); }
			set
			{
				if (GetPropertyValue("AllowKeyboardNavigation", DEF_ALLOWKEYBOARDNAVIGATION) != value)
				{
					SetPropertyValue("AllowKeyboardNavigation", value);
				}
			}
		}


		/// <summary>
		/// Gets or sets the action to be performed when the user presses the TAB key.
		/// </summary>
		/// <remarks>
		/// This option is invalid when the AllowKeyboardNavigation is set to false.
		/// </remarks>
		[DefaultValue(DEF_KEYACTIONTAB)]
		[C1Description("C1Grid.KeyActionTab")]
		[C1Category("Category.Behavior")]
		[Json(true, true, DEF_KEYACTIONTAB)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public virtual KeyActionEnum KeyActionTab
		{
			get { return GetPropertyValue("KeyActionTab", DEF_KEYACTIONTAB); }
			set
			{
				if (GetPropertyValue("KeyActionTab", DEF_KEYACTIONTAB) != value)
				{
					SetPropertyValue("KeyActionTab", value);
				}
			}
		}

		/// <summary>
		/// A value indicating whether the widget can be paged.
		/// </summary>
		[DefaultValue(DEF_ALLOWPAGING)]
		[C1Description("C1Grid.AllowPaging")]
		[C1Category("Category.Behavior")]
		[Json(true, true, DEF_ALLOWPAGING)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public virtual bool AllowPaging
		{
			get { return GetPropertyValue("AllowPaging", DEF_ALLOWPAGING); }
			set
			{
				if (GetPropertyValue("AllowPaging", DEF_ALLOWPAGING) != value)
				{
					SetPropertyValue("AllowPaging", value);
					OnRequiresDataBinding();
				}
			}
		}

		/// <summary>
		/// A value indicating whether the widget can be sorted.
		/// </summary>
		[DefaultValue(DEF_ALLOWSORTING)]
		[C1Description("C1Grid.AllowSorting")]
		[C1Category("Category.Behavior")]
		[Json(true, true, DEF_ALLOWSORTING)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public virtual bool AllowSorting
		{
			get { return GetPropertyValue("AllowSorting", DEF_ALLOWSORTING); }
			set
			{
				if (GetPropertyValue("AllowSorting", DEF_ALLOWSORTING) != value)
				{
					SetPropertyValue("AllowSorting", value);
					OnRequiresDataBinding();
				}
			}
		}

		/// <summary>
		/// Use the ScrollingSettings.VirtualizationSettings.Mode property instead.
		/// </summary>
		[Json(false)]
		[Obsolete("Set the ScrollingSettings.VirtualizationSettings.Mode property to Rows instead.")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual bool AllowVirtualScrolling
		{
			get
			{
				return _InternalAllowVVirtualScrolling();
			}
			set
			{
				VirtualizationMode mode = _InternalVirtualizationMode();

				if (value)
				{
					switch (mode)
					{
						case VirtualizationMode.Columns:
							ScrollingSettings.VirtualizationSettings.Mode = VirtualizationMode.Both;
							break;

						case VirtualizationMode.None:
							ScrollingSettings.VirtualizationSettings.Mode = VirtualizationMode.Rows;
							break;
					}
				}
				else
				{
					switch (mode)
					{
						case VirtualizationMode.Both:
							ScrollingSettings.VirtualizationSettings.Mode = VirtualizationMode.Rows;
							break;

						case VirtualizationMode.Rows:
							ScrollingSettings.VirtualizationSettings.Mode = VirtualizationMode.None;
							break;
					}
				}
			}
		}

		/// <summary>
		/// Use ScrollingSettings.FreezingMode property instead.
		/// </summary>
		[Json(false)]
		[Obsolete("Use ScrollingSettings.FreezingMode property instead.")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual FreezingMode FreezingMode
		{
			get { return _InternalFreezingMode(); }
			set { ScrollingSettings.FreezingMode = value; }
		}

		/// <summary>
		/// Determines the caption of the group area.
		/// </summary>
		[C1Description("C1Grid.GroupAreaCaption")]
		[C1Category("Category.Behavior")]
		[DefaultValue(DEF_GROUPAREACAPTION)]
		[Json(true, true, DEF_GROUPAREACAPTION)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public virtual string GroupAreaCaption
		{
			get
			{
				string defValue = C1Localizer.GetString("C1Grid.GroupAreaCaptionValue", DEF_GROUPAREACAPTION);
				return GetPropertyValue("GroupAreaCaption", defValue);
			}
			set
			{
				string defValue = C1Localizer.GetString("C1Grid.GroupAreaCaptionValue", DEF_GROUPAREACAPTION);

				if (GetPropertyValue("GroupAreaCaption", defValue) != value)
				{
					SetPropertyValue("GroupAreaCaption", value);
				}
			}
		}

		/// <summary>
		/// Determines the indentation of the groups.
		/// </summary>
		[DefaultValue(DEF_GROUPINDENT)]
		[C1Description("C1Grid.GroupIndent")]
		[C1Category("Category.Appearance")]
		[Json(true, true, DEF_GROUPINDENT)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public virtual int GroupIndent
		{
			get { return GetPropertyValue("GroupIndent", DEF_GROUPINDENT); }
			set
			{
				if (GetPropertyValue("GroupIndent", DEF_GROUPINDENT) != value)
				{
					SetPropertyValue("GroupIndent", value);
				}
			}
		}

		/// <summary>
		/// Determines whether position of the current cell is highlighted or not.
		/// </summary>
		[DefaultValue(DEF_HIGHLIGHTCURRENTCELL)]
		[C1Description("C1Grid.HighlightCurrentCell")]
		[C1Category("Category.Appearance")]
		[Json(true, true, DEF_HIGHLIGHTCURRENTCELL)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public virtual bool HighlightCurrentCell
		{
			get { return GetPropertyValue("HighlightCurrentCell", DEF_HIGHLIGHTCURRENTCELL); }
			set
			{
				if (GetPropertyValue("HighlightCurrentCell", DEF_HIGHLIGHTCURRENTCELL) != value)
				{
					SetPropertyValue("HighlightCurrentCell", value);
				}
			}
		}

#if EXTENDER
		/// <summary>
		/// Determines the text to be displayed when the grid is loading.
		/// </summary>
#else
		/// <summary>
		/// Determines the text that should be shown during callback to provide visual feedback.
		/// </summary>
#endif
		[DefaultValue(DEF_LOADINGTEXT)]
		[C1Description("C1Grid.LoadingText")]
		[C1Category("Category.Appearance")]
		[Json(true, true, DEF_LOADINGTEXT)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string LoadingText
		{
			get
			{
				string defValue = C1Localizer.GetString("C1Grid.LoadingTextValue", DEF_LOADINGTEXT);
				return GetPropertyValue("LoadingText", defValue);
			}
			set
			{
				string defValue = C1Localizer.GetString("C1Grid.LoadingTextValue", DEF_LOADINGTEXT);

				if (GetPropertyValue("LoadingText", defValue) != value)
				{
					SetPropertyValue("LoadingText", value);
				}
			}
		}

		/// <summary>
		/// Determines the zero-based index of the current page.
		/// </summary>
		[DefaultValue(DEF_PAGEINDEX)]
		[C1Description("C1Grid.PageIndex")]
		[C1Category("Category.Behavior")]
		[Json(true, false /*true*/, DEF_PAGEINDEX)]
		[WidgetOption]
		public int PageIndex
		{
#if !EXTENDER
			get { return _pageIndex; }
			set
			{
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException("value", "Must be greater than or equal to zero.");
				}

				if (_pageIndex != value)
				{
					_pageIndex = value;
					OnRequiresDataBinding();
				}
			}
#else
			get { return GetPropertyValue("PageIndex", DEF_PAGEINDEX); }
			set
			{
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException("value", "Must be greater than or equal to zero.");
				}

				if (GetPropertyValue("PageIndex", DEF_PAGEINDEX) != value)
				{
					SetPropertyValue("PageIndex", value);
					OnRequiresDataBinding();
				}
			}
#endif
		}

		/// <summary>
		/// Number of rows to place on a single page.
		/// </summary>
		[DefaultValue(DEF_PAGESIZE)]
		[C1Description("C1Grid.PageSize")]
		[C1Category("Category.Behavior")]
		[Json(true, true, DEF_PAGESIZE)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public virtual int PageSize
		{
			get { return GetPropertyValue("PageSize", DEF_PAGESIZE); }
			set
			{
				if (value < 1)
				{
					throw new ArgumentOutOfRangeException("value", "Must be greater than one.");
				}

				if (GetPropertyValue("PageSize", DEF_PAGESIZE) != value)
				{
					SetPropertyValue("PageSize", value);
					OnRequiresDataBinding();
				}
			}
		}

		/// <summary>
		/// Pager settings.
		/// </summary>
		[C1Description("C1Grid.PagerSettings")]
		[C1Category("Category.Behavior")]
		[Json(true, true, null)]
		[WidgetOption]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public virtual PagerSettings PagerSettings
		{
			get
			{
				if (_pagerSettings == null)
				{
					_pagerSettings = new PagerSettings();

					_pagerSettings.PropertyChanged += delegate(object o, EventArgs e)
					{
						OnRequiresDataBinding();
					};
				}

				return _pagerSettings;
			}
		}

		/// <summary>
		/// Scrolling settings.
		/// </summary>
		[C1Description("C1Grid.ScrollingSettings")]
		[C1Category("Category.Behavior")]
		[Json(true, true, null)]
		[WidgetOption]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public virtual ScrollingSettings ScrollingSettings
		{
			get
			{
				if (_scrollingSettings == null)
				{
					_scrollingSettings = new ScrollingSettings();

					_scrollingSettings.PropertyChanged += delegate(object o, PropertyChangedEventArgs e)
					{
						if (e.PropertyName == "VirtualizationSettings.Mode")
						{
							OnRequiresDataBinding();
						}
					};
				}

				return _scrollingSettings;
			}
		}

		/// <summary>
		/// Use ScrollingSettings.VirtualizationSettings.RowHeight property instead.
		/// </summary>
		[Json(false)]
		[Obsolete("Use ScrollingSettings.VirtualizationSettings.RowHeight property instead.")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public int RowHeight
		{
			get { return _InternalRowHeight(); }
			set { ScrollingSettings.VirtualizationSettings.RowHeight = value; }
		}

		/// <summary>
		/// Use ScrollingSettings.Mode property instead.
		/// </summary>
		[Json(false)]
		[Obsolete("Use ScrollingSettings.Mode property instead.")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Browsable(false)]
		public virtual ScrollMode ScrollMode
		{
			get
			{
				return _InternalScrollMode();
			}
			set
			{
				this.ScrollingSettings.Mode = value;
			}
		}

		/// <summary>
		/// A value indicating whether the row header is visible.
		/// </summary>
		[DefaultValue(DEF_SHOWROWHEADER)]
		[C1Description("C1Grid.ShowRowHeader")]
		[C1Category("Category.Behavior")]
		[Json(true, true, DEF_SHOWROWHEADER)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public virtual bool ShowRowHeader
		{
			get { return GetPropertyValue("ShowRowHeader", DEF_SHOWROWHEADER); }
			set
			{
				if (GetPropertyValue("ShowRowHeader", DEF_SHOWROWHEADER) != value)
				{
					SetPropertyValue("ShowRowHeader", value);
				}
			}
		}

		/// <summary>
		/// A value indicating whether filter row is visible.
		/// </summary>
		[DefaultValue(DEF_SHOWFILTER)]
		[C1Description("C1Grid.ShowFilter")]
		[C1Category("Category.Behavior")]
		[Json(true, true, DEF_SHOWFILTER)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public virtual bool ShowFilter
		{
			get { return GetPropertyValue("ShowFilter", DEF_SHOWFILTER); }
			set
			{
				if (GetPropertyValue("ShowFilter", DEF_SHOWFILTER) != value)
				{
					SetPropertyValue("ShowFilter", value);
					OnRequiresDataBinding();
				}
			}
		}

		/// <summary>
		/// A value indicating whether footer row is visible.
		/// </summary>
		[C1Description("C1Grid.ShowFooter")]
		[C1Category("Category.Behavior")]
		[DefaultValue(DEF_SHOWFOOTER)]
		[Json(true, true, DEF_SHOWFOOTER)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public virtual bool ShowFooter
		{
			get { return GetPropertyValue("ShowFooter", DEF_SHOWFOOTER); }
			set
			{
				if (GetPropertyValue("ShowFooter", DEF_SHOWFOOTER) != value)
				{
					SetPropertyValue("ShowFooter", value);
					OnRequiresDataBinding();
				}
			}
		}

		/// <summary>
		/// A value indicating whether group area is visible.
		/// </summary>
		[C1Description("C1Grid.ShowGroupArea")]
		[C1Category("Category.Behavior")]
		[DefaultValue(DEF_SHOWGROUPAREA)]
		[Json(true, true, DEF_SHOWGROUPAREA)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public virtual bool ShowGroupArea
		{
			get { return GetPropertyValue("ShowGroupArea", DEF_SHOWGROUPAREA); }
			set
			{
				if (GetPropertyValue("ShowGroupArea", DEF_SHOWGROUPAREA) != value)
				{
					SetPropertyValue("ShowGroupArea", value);
				}
			}
		}

		/// <summary>
		/// Use ScrollingSettings.StaticColumnIndex property instead.
		/// </summary>
		[Json(false)]
		[Obsolete("Use ScrollingSettings.StaticColumnIndex property instead.")]
		[Browsable(false)]
		public virtual int StaticColumnIndex
		{
			get { return _InternalStaticColumnIndex(); }
			set { this.ScrollingSettings.StaticColumnIndex = value; }
		}

		/// <summary>
		/// Use ScrollingSettings.StaticRowIndex property instead.
		/// </summary>
		[Json(false)]
		[Obsolete("Use ScrollingSettings.StaticRowIndex property instead.")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual int StaticRowIndex
		{
			get { return _InternalStaticRowIndex(); }
			set { this.ScrollingSettings.StaticRowIndex = value; }
		}

		/// <summary>
		/// Total rows.
		/// </summary>
		[DefaultValue(DEF_TOTALROWS)]
		[C1Description("C1Grid.TotalRows")]
		[C1Category("Category.Behavior")]
		[Json(true, true, DEF_TOTALROWS)]
		[WidgetOption]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public virtual int TotalRows
		{
#if EXTENDER
			get { return GetPropertyValue("TotalRows", DEF_TOTALROWS); }
			set
			{
				if (value < -1)
				{
					throw new ArgumentOutOfRangeException("value", "Must be greater than or equal to -1.");
				}

				if (GetPropertyValue("TotalRows", DEF_TOTALROWS) != value)
				{
					SetPropertyValue("TotalRows", value);
				}
			}
#else
			get
			{
				return (_c1ds != null && ServerSideRowsScrollingAllowed())
					? _bodyRowsCount // use _bodyRowsCount instead of _c1ds.DataSourceCount to take the Detail rows into account. Although rows virtual scrolling + master-detail hierararchy is not supported...
					: DEF_TOTALROWS;
			}
#endif
		}

		/// <summary>
		/// A value that indicates whether to register the dependency resources 
		/// according to the control's property settings.
		/// </summary>
		/// <remarks>
		/// This property is enabled only when EnableCombinedJavaScripts is set to true.
		/// </remarks>
		[Browsable(true)]
		[EditorBrowsable(EditorBrowsableState.Always)]
		public override bool EnableConditionalDependencies
		{
			get
			{
				return base.EnableConditionalDependencies;
			}
			set
			{
				base.EnableConditionalDependencies = value;
			}
		}
		#endregion

		#region internal

		internal bool _InternalAllowVVirtualScrolling()
		{
			VirtualizationMode mode = _InternalVirtualizationMode();
			return mode == VirtualizationMode.Both || mode == VirtualizationMode.Rows;
		}

		internal VirtualizationMode _InternalVirtualizationMode()
		{
			VirtualizationMode mode = _scrollingSettings != null && _scrollingSettings._IsVirtualizationSettingsCreated
				? _scrollingSettings.VirtualizationSettings.Mode
				: VirtualizationSettings.DEF_MODE;

			return mode;
		}


		internal FreezingMode _InternalFreezingMode()
		{
			return (_scrollingSettings != null)
				? _scrollingSettings.FreezingMode
				: ScrollingSettings.DEF_FREEZINGMODE;
		}

		internal int _InternalRowHeight()
		{
			return (_scrollingSettings != null && _scrollingSettings._IsVirtualizationSettingsCreated)
				? _scrollingSettings.VirtualizationSettings.RowHeight
				: VirtualizationSettings.DEF_ROWHEIGHT;
		}

#if !EXTENDER 
		[SmartAssembly.Attributes.DoNotObfuscate]
#endif
		internal ScrollMode _InternalScrollMode()
		{
			return (_scrollingSettings != null)
				? _scrollingSettings.Mode
				: ScrollingSettings.DEF_MODE;
		}


		internal int _InternalStaticColumnIndex()
		{
			return (_scrollingSettings != null)
				? _scrollingSettings.StaticColumnIndex
				: ScrollingSettings.DEF_STATICCOLUMNINDEX;
		}

		internal int _InternalStaticRowIndex()
		{
			return (_scrollingSettings != null)
				? _scrollingSettings.StaticRowIndex
				: ScrollingSettings.DEF_STATICROWINDEX;
		}

		#endregion

		#region client-side events

		/// <summary>
		/// A function called after editing is completed.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>cell</term>
		///						<description>Gets the edited cell's information.</description>
		///					</item>
		///					<item>
		///						<term>event</term>
		///						<description>Event that initiated the cell updating.</description>
		///					</item>
		///					<item>
		///						<term>handled</term>
		///						<description>
		///							Gets or sets value determining whether the developer finalizes editing of the cell manually.
		///							The default value is false which means that the widget will try to finalize editing of the cell automatically.
		///							If the developer provides a custom editing front end then this property must be set to true.
		///						</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientAfterCellEdit")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientAfterCellEdit
		{
			get { return GetPropertyValue("OnClientAfterCellEdit", ""); }
			set { SetPropertyValue("OnClientAfterCellEdit", value); }
		}

		/// <summary>
		/// A function called after a cell has been updated.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>cell</term>
		///						<description>Gets the edited cell's information.</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientAfterCellUpdate")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientAfterCellUpdate
		{
			get { return GetPropertyValue("OnClientAfterCellUpdate", ""); }
			set { SetPropertyValue("OnClientAfterCellUpdate", value); }
		}

		/// <summary>
		/// A function called before a cell enters edit mode. Cancellable.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>cell</term>
		///						<description>Information about the cell to be edited.</description>
		///					</item>
		///					<item>
		///						<term>event</term>
		///						<description>Event initiated cell editing.</description>
		///					</item>
		///					<item>
		///						<term>handled</term>
		///						<description>
		///							Gets or sets a value determining whether developer initiates cell editor(s) manually.
		///							The default value is false which means that widget will trying to provide editing control automatically.
		///							If cells contain custom controls or if developer wants to provide a custom editing front end then he must set this property to true.
		///						</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientBeforeCellEdit")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientBeforeCellEdit
		{
			get { return GetPropertyValue("OnClientBeforeCellEdit", ""); }
			set { SetPropertyValue("OnClientBeforeCellEdit", value); }
		}

		/// <summary>
		/// A function called before a cell is updated.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>cell</term>
		///						<description>Gets information of the edited cell.</description>
		///					</item>
		///					<item>
		///						<term>value</term>
		///						<description>
		///							Returns the new cell value. If the property value is not changed the widget will try to extract the new cell value automatically.
		///							If the developer provides custom editing front end then the new cell value must be returned within this property.		
		///						</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientBeforeCellUpdate")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientBeforeCellUpdate
		{
			get { return GetPropertyValue("OnClientBeforeCellUpdate", ""); }
			set { SetPropertyValue("OnClientBeforeCellUpdate", value); }
		}

		/// <summary>
		/// A function called when column dragging is started, but before wijgrid handles the operation. Cancellable.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>drag</term>
		///						<description>Drag source, column being dragged.</description>
		///					</item>
		///					<item>
		///						<term>dragSource</term>
		///						<description>The place where the dragged column widget is located, possible value: "groupArea", "columns".</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientColumnDragging")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientColumnDragging
		{
			get { return GetPropertyValue("OnClientColumnDragging", ""); }
			set { SetPropertyValue("OnClientColumnDragging", value); }
		}

		/// <summary>
		/// A function called when column dragging has been started.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>drag</term>
		///						<description>Drag source, column being dragged.</description>
		///					</item>
		///					<item>
		///						<term>dragSource</term>
		///						<description>The place where the dragged column widget is located, possible value: "groupArea", "columns".</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientColumnDragged")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientColumnDragged
		{
			get { return GetPropertyValue("OnClientColumnDragged", ""); }
			set { SetPropertyValue("OnClientColumnDragged", value); }
		}

		/// <summary>
		/// A function called when column is dropped, but before wijgrid handles the operation. Cancellable.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>drag</term>
		///						<description>Drag source, column being dragged.</description>
		///					</item>
		///					<item>
		///						<term>drop</term>
		///						<description>Drop target, column on which drag source is dropped(be null if dropping a column into empty group area).</description>
		///					</item>
		///					<item>
		///						<term>at</term>
		///						<description>Position to drop (one of the "left", "right" and "center" values) relative to drop target(be "left" if dropping a column into empty group area).</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientColumnDropping")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientColumnDropping
		{
			get { return GetPropertyValue("OnClientColumnDropping", ""); }
			set { SetPropertyValue("OnClientColumnDropping", value); }
		}

		/// <summary>
		/// A function called when column is dropped into the group area, but before wijgrid handles the operation. Cancellable.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>drag</term>
		///						<description>Drag source, column being dragged.</description>
		///					</item>
		///					<item>
		///						<term>drop</term>
		///						<description>Drop target, column on which drag source is dropped (be null if dropping a column into empty group area).</description>
		///					</item>
		///					<item>
		///						<term>dragSource</term>
		///						<description>The place where the dragged column widget is located, possible value: "groupArea", "columns".</description>
		///					</item>
		///					<item>
		///						<term>dropSource</term>
		///						<description>The place where the dropped column widget is located, possible value: "groupArea", "columns".</description>
		///					</item>
		///					<item>
		///						<term>at</term>
		///						<description>Position to drop (one of the "left", "right" and "center" values) relative to drop target(be "left" if dropping a column into empty group area).</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientColumnGrouping")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientColumnGrouping
		{
			get { return GetPropertyValue("OnClientColumnGrouping", ""); }
			set { SetPropertyValue("OnClientColumnGrouping", value); }
		}

		/// <summary>
		/// A function called when column is resized, but before wijgrid handles the operation. Cancellable.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>column</term>
		///						<description>Column that is being resized.</description>
		///					</item>
		///					<item>
		///						<term>oldWidth</term>
		///						<description>The old width of the column before resized.</description>
		///					</item>
		///					<item>
		///						<term>newWidth</term>
		///						<description>The new width being set to the column.</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientColumnResizing")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientColumnResizing
		{
			get { return GetPropertyValue("OnClientColumnResizing", ""); }
			set { SetPropertyValue("OnClientColumnResizing", value); }
		}

		/// <summary>
		/// A function called when column has been resized.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>column</term>
		///						<description>Column that is being resized.</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientColumnResized")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientColumnResized
		{
			get { return GetPropertyValue("OnClientColumnResized", ""); }
			set { SetPropertyValue("OnClientColumnResized", value); }
		}

		/// <summary>
		/// A function called when column is removed from the group area, but before wijgrid handles the operation. Cancellable.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>column</term>
		///						<description>Column being removed.</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientColumnUngrouping")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientColumnUngrouping
		{
			get { return GetPropertyValue("OnClientColumnUngrouping", ""); }
			set { SetPropertyValue("OnClientColumnUngrouping", value); }
		}

		/// <summary>
		/// A function called before the current cell is changed. Cancellable.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>cellIndex</term>
		///						<description>New cell index.</description>
		///					</item>
		///					<item>
		///						<term>rowIndex</term>
		///						<description>New row index.</description>
		///					</item>
		///					<item>
		///						<term>oldCellIndex</term>
		///						<description>Old cell index.</description>
		///					</item>
		///					<item>
		///						<term>oldRowIndex</term>
		///						<description>Old row index.</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientCurrentCellChanging")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientCurrentCellChanging
		{
			get { return GetPropertyValue("OnClientCurrentCellChanging", ""); }
			set { SetPropertyValue("OnClientCurrentCellChanging", value); }
		}

		/// <summary>
		/// A function called after the current cell is changed.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientCurrentCellChanged")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientCurrentCellChanged
		{
			get { return GetPropertyValue("OnClientCurrentCellChanged", ""); }
			set { SetPropertyValue("OnClientCurrentCellChanged", value); }
		}

		/// <summary>
		/// A function called before the filter drop-down list is shown.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>column</term>
		///						<description>Associated column.</description>
		///					</item>
		///					<item>
		///						<term>operators</term>
		///						<description>An array of filter operators.</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientFilterOperatorsListShowing")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientFilterOperatorsListShowing
		{
			get { return GetPropertyValue("OnClientFilterOperatorsListShowing", ""); }
			set { SetPropertyValue("OnClientFilterOperatorsListShowing", value); }
		}

		/// <summary>
		/// A function called when groups are being created and the "aggregate" option of the column object has been set to "custom".
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>data</term>
		///						<description>Data object.</description>
		///					</item>
		///					<item>
		///						<term>column</term>
		///						<description>Column that is being grouped.</description>
		///					</item>
		///					<item>
		///						<term>groupByColumn</term>
		///						<description>Column initiated grouping.</description>
		///					</item>
		///					<item>
		///						<term>groupText</term>
		///						<description>Text that is being grouped.</description>
		///					</item>
		///					<item>
		///						<term>text</term>
		///						<description>Text that will be displayed in the group header or group footer.</description>
		///					</item>
		///					<item>
		///						<term>groupingStart</term>
		///						<description>First index for the data being grouped.</description>
		///					</item>
		///					<item>
		///						<term>groupingEnd</term>
		///						<description>Last index for the data being grouped.</description>
		///					</item>
		///					<item>
		///						<term>isGroupHeader</term>
		///						<description>Indicates whether row that is being grouped is a group header or not.</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientGroupAggregate")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientGroupAggregate
		{
			get { return GetPropertyValue("OnClientGroupAggregate", ""); }
			set { SetPropertyValue("OnClientGroupAggregate", value); }
		}

		/// <summary>
		/// A function called when groups are being created and the groupInfo.headerText or groupInfo.footerText of the groupInfo option has been set to "custom".
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>data</term>
		///						<description>Data object.</description>
		///					</item>
		///					<item>
		///						<term>column</term>
		///						<description>Column that is being grouped.</description>
		///					</item>
		///					<item>
		///						<term>groupByColumn</term>
		///						<description>Column initiated grouping.</description>
		///					</item>
		///					<item>
		///						<term>groupText</term>
		///						<description>Text that is being grouped.</description>
		///					</item>
		///					<item>
		///						<term>text</term>
		///						<description>Text that will be displayed in the group header or group footer.</description>
		///					</item>
		///					<item>
		///						<term>groupingStart</term>
		///						<description>First index for the data being grouped.</description>
		///					</item>
		///					<item>
		///						<term>groupingEnd</term>
		///						<description>Last index for the data being grouped.</description>
		///					</item>
		///					<item>
		///						<term>isGroupHeader</term>
		///						<description>Indicates whether row that is being grouped is a group header or not.</description>
		///					</item>
		///					<item>
		///						<term>aggregate</term>
		///						<description>Aggregate value.</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientGroupText")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientGroupText
		{
			get { return GetPropertyValue("OnClientGroupText", ""); }
			set { SetPropertyValue("OnClientGroupText", value); }
		}

		/// <summary>
		/// A function called when a cell needs to start updating but the cell value is invalid.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>cell</term>
		///						<description>Gets the information of edited cell.</description>
		///					</item>
		///					<item>
		///						<term>value</term>
		///						<description>Current value.</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientInvalidCellValue")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientInvalidCellValue
		{
			get { return GetPropertyValue("OnClientInvalidCellValue", ""); }
			set { SetPropertyValue("OnClientInvalidCellValue", value); }
		}

		/// <summary>
		/// A function called before page index is changed. Cancellable.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>newPageIndex</term>
		///						<description>New page index.</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientPageIndexChanging")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientPageIndexChanging
		{
			get { return GetPropertyValue("OnClientPageIndexChanging", ""); }
			set { SetPropertyValue("OnClientPageIndexChanging", value); }
		}

		/// <summary>
		/// A function called after the selection is changed.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>addedCells</term>
		///						<description>Cells added to the selection.</description>
		///					</item>
		///					<item>
		///						<term>removedCells</term>
		///						<description>Cells removed from the selection.</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientSelectionChanged")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientSelectionChanged
		{
			get { return GetPropertyValue("OnClientSelectionChanged", ""); }
			set { SetPropertyValue("OnClientSelectionChanged", value); }
		}

		/// <summary>
		/// A function called before the sorting operation is started. Cancellable.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>column</term>
		///						<description>Column that is being sorted.</description>
		///					</item>
		///					<item>
		///						<term>sortDirection</term>
		///						<description>New sort direction.</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientSorting")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientSorting
		{
			get { return GetPropertyValue("OnClientSorting", ""); }
			set { SetPropertyValue("OnClientSorting", value); }
		}

		/// <summary>
		/// A function called when wijgrid loads a portion of data from the underlying datasource.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientDataLoading")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientDataLoading
		{
			get { return GetPropertyValue("OnClientDataLoading", ""); }
			set { SetPropertyValue("OnClientDataLoading", value); }
		}

		/// <summary>
		/// A function called when data are loaded.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientDataLoaded")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientDataLoaded
		{
			get { return GetPropertyValue("OnClientDataLoaded", ""); }
			set { SetPropertyValue("OnClientDataLoaded", value); }
		}

		/// <summary>
		/// A function called at the beginning of the wijgrid's lifecycle.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientLoading")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientLoading
		{
			get { return GetPropertyValue("OnClientLoading", ""); }
			set { SetPropertyValue("OnClientLoading", value); }
		}

		/// <summary>
		/// A function called at the end the wijgrid's lifecycle when wijgrid is filled with data and rendered.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientLoaded")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientLoaded
		{
			get { return GetPropertyValue("OnClientLoaded", ""); }
			set { SetPropertyValue("OnClientLoaded", value); }
		}

		/// <summary>
		/// A function called when wijgrid is about to render.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientRendering")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientRendering
		{
			get { return GetPropertyValue("OnClientRendering", ""); }
			set { SetPropertyValue("OnClientRendering", value); }
		}

		/// <summary>
		/// A function called when wijgrid is rendered.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientRendered")]
		[C1Category("Category.ClientSideEvents")]
		[Json(true, true, "")]
		[WidgetEvent("e")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientRendered
		{
			get { return GetPropertyValue("OnClientRendered", ""); }
			set { SetPropertyValue("OnClientRendered", value); }
		}

		#endregion

		#region IOwnerable members

		IOwnerable IOwnerable.Owner
		{
			get { return null; }
			set { }
		}

		#endregion
	}
}
