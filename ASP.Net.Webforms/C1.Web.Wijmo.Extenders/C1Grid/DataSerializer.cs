﻿using System;

namespace C1.Web.Wijmo.Extenders.C1Grid
{
	using System.Collections;
	using System.Collections.Generic;
	using System.ComponentModel;
	using System.IO;
	using System.Reflection;

	internal sealed class DataSerializer
	{
		public string Serialize(object data, string dataMember)
		{
			IEnumerable ien = Resolve(data, dataMember);
			if (data != null)
			{
				using (StringWriter sw = new StringWriter())
				{
					JsonWriter writer = new JsonWriter(sw, null);

					writer.StartArrayScope();

					string[] fields = GetFieldNames(ien);

					if (fields.Length > 0)
					{
						foreach (object item in ien)
						{
							if (IsBindable(item.GetType())) // item.GetType().IsPrimitive
							{
								writer.StartArrayScope();
								writer.WriteValue(item);
								writer.EndScope();
							}
							else
							{
								writer.StartObjectScope();

								foreach (string name in fields)
								{
									writer.WriteName(name, false);
									writer.WriteValue(System.Web.UI.DataBinder.GetPropertyValue(item, name));
								}

								writer.EndScope();
							}
						}
					}

					writer.EndScope();

					sw.Flush();
					return sw.ToString();
				}
			}

			return string.Empty;
		}

		private bool IsBindable(Type type)
		{
			if (type == null)
			{
				return false;
			}

			Type underlyingType = Nullable.GetUnderlyingType(type);
			if (underlyingType != null)
			{
				type = underlyingType;
			}

			return (type.IsPrimitive ||
				type == typeof(string) ||
				type == typeof(DateTime) ||
				type == typeof(decimal) ||
				type == typeof(Guid) ||
				type == typeof(DateTimeOffset) ||
				type == typeof(TimeSpan));
		}

		private IEnumerable Resolve(object data, string dataMember)
		{
			IListSource listSource = data as IListSource;
			if (listSource != null)
			{
				IList list = listSource.GetList();
				if (!(listSource.ContainsListCollection))
					return list;

				if (list != null && list as ITypedList != null)
				{
					ITypedList typedList = (ITypedList)list;
					PropertyDescriptorCollection propertyDescriptorCollection = typedList.GetItemProperties(new PropertyDescriptor[0]);
					if (propertyDescriptorCollection != null && propertyDescriptorCollection.Count != 0)
					{
						PropertyDescriptor propertyDescriptor = string.IsNullOrEmpty(dataMember)
							? propertyDescriptorCollection[0]
							: propertyDescriptor = propertyDescriptorCollection.Find(dataMember, true);

						if (propertyDescriptor != null)
						{
							object obj = propertyDescriptor.GetValue(list[0]);
							if (obj != null && obj as IEnumerable != null)
								return (IEnumerable)obj;
						}

						throw new Exception("Missing DataMember");
					}

					throw new Exception("Missing DataMember");
				}
			}

			return (data as IEnumerable);
		}

		private string[] GetFieldNames(IEnumerable data)
		{
			List<string> fields = new List<string>();

			PropertyDescriptorCollection pdc = (data is ITypedList)
				? ((ITypedList)data).GetItemProperties(new PropertyDescriptor[0])
				: null;

			if (pdc == null)
			{
				object firstItem = null;
				Type firstItemType = null;


				PropertyInfo pi = data.GetType().GetProperty("Item", BindingFlags.Public | BindingFlags.Instance);
				if (pi != null)
				{
					firstItemType = pi.PropertyType;
				}

				if (firstItemType == null || firstItemType == typeof(object))
				{
					IEnumerator ien = data.GetEnumerator();
					if (ien.MoveNext())
					{
						firstItem = ien.Current;
						firstItemType = firstItem.GetType();
					}
				}

				if ((firstItem != null) && (firstItem is ICustomTypeDescriptor))
				{
					pdc = TypeDescriptor.GetProperties(firstItem);
				}
				else
					if (firstItemType != null)
					{
						if (IsBindable(firstItemType))
						{
							fields.Add("Item");
						}
						else
						{
							pdc = TypeDescriptor.GetProperties(firstItemType);
						}
					}
			}

			if (pdc != null)
			{
				foreach (PropertyDescriptor pd in pdc)
				{
					if (IsBindable(pd.PropertyType))
					{
						fields.Add(pd.Name);
					}
				}
			}

			return fields.ToArray();
		}
	}
}
