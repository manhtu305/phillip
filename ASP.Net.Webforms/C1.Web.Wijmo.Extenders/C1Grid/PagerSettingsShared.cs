﻿using System;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Grid
#else
namespace C1.Web.Wijmo.Controls.C1GridView
#endif
{
	using System.ComponentModel;
	using System.Web.UI;
#if EXTENDER
	using C1.Web.Wijmo.Extenders.C1Pager;
	using C1.Web.Wijmo.Extenders.Localization;

#else
	using C1.Web.Wijmo.Controls.C1Pager;
	using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// Represents a pager settings.
	/// </summary>
	[PersistChildren(false), ParseChildren(true)]
	public sealed partial class PagerSettings : IJsonEmptiable, IPagerOptions 
	{
		private const PagerPosition DEF_POSITION = PagerPosition.Bottom;
#if !EXTENDER
		private const bool DEF_VISIBLE = true;
#endif

		#region IPagerOptions members

		/// <summary>
		/// The class of the first-page button.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_FIRSTPAGECLASS)]
		[C1Description("PagerSettings.FirstPageClass")]
        [C1Category("Category.Style")]
		[NotifyParentProperty(true)]
		[Json(true, true, PagerConstants.DEF_FIRSTPAGECLASS)]
		[WidgetOption]
		public string FirstPageClass
		{
			get { return GetPropertyValue("FirstPagerClass", PagerConstants.DEF_FIRSTPAGECLASS); }
			set
			{
				if (GetPropertyValue("FirstPagerClass", PagerConstants.DEF_FIRSTPAGECLASS) != value)
				{
					SetPropertyValue("FirstPagerClass", value);
					_OnPropertyChanged();
				}
			}
		}

		/// <summary>
		/// The text to display for the first-page button.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_FIRSTPAGETEXT)]
		[C1Description("PagerSettings.FirstPageText")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
		[Json(true)]
		[WidgetOption]
		public string FirstPageText
		{
			get
			{
				string defValue = C1Localizer.GetString("PagerSettings.FirstPageTextValue", PagerConstants.DEF_FIRSTPAGETEXT);
				return GetPropertyValue("FirstPageText", defValue);
			}
			set
			{
				string defValue = C1Localizer.GetString("PagerSettings.FirstPageTextValue", PagerConstants.DEF_FIRSTPAGETEXT);

				if (GetPropertyValue("FirstPageText", defValue) != value)
				{
					SetPropertyValue("FirstPageText", value);
					_OnPropertyChanged();
				}
			}
		}

		/// <summary>
		/// The class of the last-page button.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_LASTPAGECLASS)]
		[C1Description("PagerSettings.LastPageClass")]
        [C1Category("Category.Style")]
		[NotifyParentProperty(true)]
		[Json(true, true, PagerConstants.DEF_LASTPAGECLASS)]
		[WidgetOption]
		public string LastPageClass
		{
			get { return GetPropertyValue("LastPageClass", PagerConstants.DEF_LASTPAGECLASS); }
			set
			{
				if (GetPropertyValue("LastPageClass", PagerConstants.DEF_LASTPAGECLASS) != value)
				{
					SetPropertyValue("LastPageClass", value);
					_OnPropertyChanged();
				}
			}
		}

		/// <summary>
		/// The text to display for the last-page button.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_LASTPAGETEXT)]
		[C1Description("PagerSettings.LastPageText")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
		[Json(true)]
		[WidgetOption]
		public string LastPageText
		{
			get
			{
				string defValue = C1Localizer.GetString("PagerSettings.LastPageTextValue", PagerConstants.DEF_LASTPAGETEXT);
				return GetPropertyValue("LastPageText", defValue);
			}
			set
			{
				string defValue = C1Localizer.GetString("PagerSettings.LastPageTextValue", PagerConstants.DEF_LASTPAGETEXT);

				if (GetPropertyValue("LastPageText", defValue) != value)
				{
					SetPropertyValue("LastPageText", value);
					_OnPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Determines the pager mode.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_MODE)]
		[C1Description("PagerSettings.Mode")]
        [C1Category("Category.Behavior")]
		[NotifyParentProperty(true)]
		[Json(true, true, PagerConstants.DEF_MODE)]
		[WidgetOption]
		public PagerMode Mode
		{
			get { return GetPropertyValue("Mode", PagerConstants.DEF_MODE); }
			set
			{
				if (GetPropertyValue("Mode", PagerConstants.DEF_MODE) != value)
				{
					SetPropertyValue("Mode", value);
					_OnPropertyChanged();
				}
			}
		}

		/// <summary>
		/// The class of the next-page button.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_NEXTPAGECLASS)]
		[C1Description("PagerSettings.NextPageClass")]
        [C1Category("Category.Style")]
		[NotifyParentProperty(true)]
		[Json(true, true, PagerConstants.DEF_NEXTPAGECLASS)]
		[WidgetOption]
		public string NextPageClass
		{
			get { return GetPropertyValue("NextPageClass", PagerConstants.DEF_NEXTPAGECLASS); }
			set
			{
				if (GetPropertyValue("NextPageClass", PagerConstants.DEF_NEXTPAGECLASS) != value)
				{
					SetPropertyValue("NextPageClass", value);
					_OnPropertyChanged();
				}
			}
		}

		/// <summary>
		/// The text to display for the next-page button.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_NEXTPAGETEXT)]
		[C1Description("PagerSettings.NextPageText")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
		[Json(true)]
		[WidgetOption]
		public string NextPageText
		{
			get
			{
				string defValue = C1Localizer.GetString("PagerSettings.NextPageTextValue", PagerConstants.DEF_NEXTPAGETEXT);
				return GetPropertyValue("NextPageText", defValue);
			}
			set
			{
				string defValue = C1Localizer.GetString("PagerSettings.NextPageTextValue", PagerConstants.DEF_NEXTPAGETEXT);

				if (GetPropertyValue("NextPageText", defValue) != value)
				{
					SetPropertyValue("NextPageText", value);
					_OnPropertyChanged();
				}
			}
		}

		/// <summary>
		/// The number of page buttons to display in the pager.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_PAGEBUTTONCOUNT)]
		[C1Description("PagerSettings.PageButtonCount")]
        [C1Category("Category.Behavior")]
		[NotifyParentProperty(true)]
		[Json(true, true, PagerConstants.DEF_PAGEBUTTONCOUNT)]
		[WidgetOption]
		public int PageButtonCount
		{
			get { return GetPropertyValue("PageButtonCount", PagerConstants.DEF_PAGEBUTTONCOUNT); }
			set
			{
				if (value < 1)
				{
					throw new ArgumentOutOfRangeException("value", "Must be greater than zero");
				}

				if (GetPropertyValue("PageButtonCount", PagerConstants.DEF_PAGEBUTTONCOUNT) != value)
				{
					SetPropertyValue("PageButtonCount", value);
				}

				_OnPropertyChanged();
			}
		}

		/// <summary>
		/// The class of the previous-page button.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_PREVIOUSPAGECLASS)]
		[C1Description("PagerSettings.PreviousPageClass")]
        [C1Category("Category.Style")]
		[NotifyParentProperty(true)]
		[Json(true, true, PagerConstants.DEF_PREVIOUSPAGECLASS)]
		[WidgetOption]
		public string PreviousPageClass
		{
			get { return GetPropertyValue("PreviousPageClass", PagerConstants.DEF_PREVIOUSPAGECLASS); }
			set
			{
				if (GetPropertyValue("PreviousPageClass", PagerConstants.DEF_PREVIOUSPAGECLASS) != value)
				{
					SetPropertyValue("PreviousPageClass", value);
				}

				_OnPropertyChanged();
			}
		}

		/// <summary>
		/// The text to display for the previous-page button.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_PREVIOUSPAGETEXT)]
		[C1Description("PagerSettings.PreviousPageText")]
        [C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
		[Json(true)]
		[WidgetOption]
		public string PreviousPageText
		{
			get
			{
				string defValue = C1Localizer.GetString("PagerSettings.PreviousPageTextValue", PagerConstants.DEF_PREVIOUSPAGETEXT);

				return GetPropertyValue("PreviousPageText", defValue);
			}
			set
			{
				string defValue = C1Localizer.GetString("PagerSettings.PreviousPageTextValue", PagerConstants.DEF_PREVIOUSPAGETEXT);

				if (GetPropertyValue("PreviousPageText", defValue) != value)
				{
					SetPropertyValue("PreviousPageText", value);
				}

				_OnPropertyChanged();
			}
		}

		int IPagerOptions.PageCount
		{
			get { return PagerConstants.DEF_PAGECOUNT; }
			set { }
		}

		int IPagerOptions.PageIndex
		{
			get { return PagerConstants.DEF_PAGEINDEX; }
			set { }
		}

		#endregion

		/// <summary>
		/// Gets or sets a value that specifies the location where the pager is displayed.
		/// </summary>
		[DefaultValue(DEF_POSITION)]
		[C1Description("PagerSettings.Position")]
        [C1Category("Category.Layout")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_POSITION)]
		[WidgetOption]
		public PagerPosition Position
		{
			get { return GetPropertyValue("Position", DEF_POSITION); }
			set 
			{
				if (GetPropertyValue("Position", DEF_POSITION) != value)
				{
					SetPropertyValue("Position", value);
				}
			}
		}

#if !EXTENDER
		/// <summary>
		/// Gets or sets a value indicating whether the paging controls are displayed in a <see cref="C1GridView"/> control.
		/// </summary>
		[C1Description("PagerSettings.Visible")]
		[DefaultValue(DEF_VISIBLE)]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_VISIBLE)]
		public bool Visible
		{
			get { return GetPropertyValue("Visible", DEF_VISIBLE); }
			set
			{
				if (GetPropertyValue("Visible", DEF_VISIBLE) != value)
				{
					SetPropertyValue("Visible", value);
				}
			}
		}
#endif

		/// <summary>
		/// Occurs when a property of a <see cref="PagerSettings"/> object changes values.
		/// </summary>
		[Browsable(false)]
		public event EventHandler PropertyChanged;

		private void _OnPropertyChanged()
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, EventArgs.Empty);
			}
		}

		#region IJsonEmptiable Members

		bool IJsonEmptiable.IsEmpty
		{
			get
			{
				return (this.FirstPageClass == PagerConstants.DEF_FIRSTPAGECLASS &&
					this.FirstPageText == PagerConstants.DEF_FIRSTPAGETEXT &&
					this.LastPageClass == PagerConstants.DEF_LASTPAGECLASS &&
					this.LastPageText == PagerConstants.DEF_LASTPAGETEXT &&
					this.Mode == PagerConstants.DEF_MODE &&
					this.NextPageClass == PagerConstants.DEF_NEXTPAGECLASS &&
					this.NextPageText == PagerConstants.DEF_NEXTPAGETEXT &&
					this.PageButtonCount == PagerConstants.DEF_PAGEBUTTONCOUNT &&
					this.Position == DEF_POSITION &&
					this.PreviousPageClass == PagerConstants.DEF_PREVIOUSPAGECLASS &&
					this.PreviousPageText == PagerConstants.DEF_PREVIOUSPAGETEXT
#if !EXTENDER
					&& this.Visible == DEF_VISIBLE
#endif
					);
			}
		}

		#endregion
	}
}
