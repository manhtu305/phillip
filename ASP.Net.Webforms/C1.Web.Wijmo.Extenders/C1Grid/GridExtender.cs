﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo;

namespace C1.Web.Wijmo.Extenders.C1Grid
{
	using C1.Web.Wijmo.Extenders.Localization;

	/// <summary>
	/// Extender for the <b>wijgrid</b> widget.
	/// </summary>
	[LicenseProvider()]
	[TargetControlType(typeof(Table))]
	[TargetControlType(typeof(GridView))]
	[TargetControlType(typeof(DataGrid))]
	[ToolboxItem(true)]
	[ToolboxBitmap(typeof(C1GridExtender), "Grid.png")]
	public partial class C1GridExtender : WidgetExtenderControlBase
	{
		#region const

		private const bool DEF_ALLOWEDITING = false;
		private const SelectionMode DEF_SELECTIONMODE = SelectionMode.SingleRow;
		private const bool DEF_SHOWSELECTIONONRENDER = true; 

		#endregion

		#region private members

		private bool _productLicensed = false;

		private C1BaseFieldCollection _columns;
		private CustomFilterOperatorCollection _customFilterOperators;
		private DataOption _dataOption;

		#endregion

		/// <summary>
		/// Initializes a new instance of the <b>C1GridExtender</b> class.
		/// </summary>
		public C1GridExtender()	: base()
		{
			VerifyLicense();
		}

		#region options

		/// <summary>
		/// A value indicating whether editing is enabled.
		/// This property is obsolete. Use <b>EditingMode</b> instead.
		/// </summary>
		[DefaultValue(DEF_ALLOWEDITING)]
		[C1Description("C1Grid.AllowEditing")]
		[Category("Options")]
		[Json(true, true, DEF_ALLOWEDITING)]
		[WidgetOption]
		public bool AllowEditing
		{
			get { return GetPropertyValue("AllowEditing", DEF_ALLOWEDITING); }
			set
			{
				if (GetPropertyValue("AllowEditing", DEF_ALLOWEDITING) != value)
				{
					SetPropertyValue("AllowEditing", value);
				}
			}
		}


		/// <summary>
		/// Determines whether wijgrid should parse underlying data at each operation requiring data re-fetching, like calling the ensureControl(true) method, paging, sorting, etc.
		/// If the option is disabled, wijgrid parses data only at the first fetch.
		/// The option is ignored if dynamic data load feature is used, in this case data are always parsed.
		/// </summary>
		/// <remarks>
		/// Turning off the option enhance wijgrid perfomance but if underlying data are changed by a developer it is necessary
		/// that changes match column datatype.
		/// </remarks>
		[DefaultValue(true)]
		[C1Description("C1Grid.AlwaysParseData")]
		[Category("Options")]
		[Json(true, true, true)]
		[WidgetOption]
		public bool AlwaysParseData
		{
			get { return GetPropertyValue("AllowEditing", DEF_ALLOWEDITING); }
			set	{ SetPropertyValue("AllowEditing", value); }
		}

		/// <summary>
		/// Function used for styling the cells in wijgrid.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1Grid.CellStyleFormatter")]
		[Category("Options")]
		[Json(true, true, "")]
		[WidgetEvent("args")]
		[WidgetOptionName("cellStyleFormatter")]
		public string CellStyleFormatter
		{
			get { return GetPropertyValue("CellStyleFormatter", ""); }
			set { SetPropertyValue("CellStyleFormatter", value); }
		}

		/// <summary>
		/// An array of column options.
		/// </summary>
		[C1Description("C1Grid.Columns")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[Category("Options")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
		public C1BaseFieldCollection Columns
		{
			get
			{
				if (_columns == null)
				{
					_columns = new C1BaseFieldCollection(this);
				}

				return _columns;
			}
		}

		/// <summary>
		/// Determines behavior for column autogeneration.
		/// </summary>
		[DefaultValue(ColumnsAutogenerationMode.Merge)]
		[C1Description("C1Grid.ColumnsAutogenerationMode")]
		[Category("Options")]
		[Json(true, true, ColumnsAutogenerationMode.Merge)]
		[WidgetOption]
		public ColumnsAutogenerationMode ColumnsAutogenerationMode
		{
			get { return GetPropertyValue("ColumnsAutogenerationMode", ColumnsAutogenerationMode.Merge); }
			set { SetPropertyValue("ColumnsAutogenerationMode", value); }
		}

		/// <summary>
		/// An array of custom user filters.
		/// </summary>
		[C1Description("C1Grid.CustomFilterOperators")]
		[Category("Options")]
		[Json(true, true, null)]
		[WidgetOption]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public CustomFilterOperatorCollection CustomFilterOperators
		{
			get
			{
				if (_customFilterOperators == null)
				{
					_customFilterOperators = new CustomFilterOperatorCollection();
				}

				return _customFilterOperators;
			}
		}

		/// <summary>
		/// Determines the datasource.
		/// </summary>
		[C1Description("C1Grid.Data")]
		[Category("Options")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[Json(true, true, null)]
		[WidgetOption]
		public DataOption Data
		{
			get
			{
				if (_dataOption == null)
				{
					_dataOption = new DataOption(this);
				}

				return _dataOption;
			}
		}

		/// <summary>
		/// Gets or sets the name of the list or table in the data source for which the <see cref="C1GridExtender"/> is displaying data.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1Grid.DataMember")]
		public string DataMember
		{
			get { return GetPropertyValue("DataMember", ""); }
			set { SetPropertyValue("DataMember", value); }
		}

		/// <summary>
		/// Determines the editing mode.
		/// </summary>
		[DefaultValue(EditingMode.None)]
		[C1Description("C1Grid.EditingMode")]
		[Category("Options")]
		[Json(true, true, EditingMode.None)]
		[WidgetOption]
		public EditingMode EditingMode
		{
			get { return GetPropertyValue("EditingMode", EditingMode.None); }
			set { SetPropertyValue("EditingMode", value); }
		}

		/// <summary>
		/// Determines whether to use number type column width as the real width of the column.
		/// </summary>
		[DefaultValue(false)]
		[C1Description("C1Grid.EnsureColumnsPxWidth")]
		[Category("Options")]
		[Json(true, true, false)]
		[WidgetOption]
		public bool EnsureColumnsPxWidth
		{
			get { return GetPropertyValue("EnsureColumnsPxWidth", false); }
			set { SetPropertyValue("EnsureColumnsPxWidth", value); }
		}

		/// <summary>
		/// Determines the order of items in the filter dropdown list.
		/// </summary>
		[DefaultValue(FilterOperatorsSortMode.AlphabeticalCustomFirst)]
		[C1Description("C1Grid.FilterOperatorsSortMode")]
		[Category("Options")]
		[Json(true, true, FilterOperatorsSortMode.AlphabeticalCustomFirst)]
		[WidgetOption]
		public FilterOperatorsSortMode FilterOperatorsSortMode
		{
			get { return GetPropertyValue("FilterOperatorsSortMode", FilterOperatorsSortMode.AlphabeticalCustomFirst); }
			set { SetPropertyValue("FilterOperatorsSortMode", value); }
		}

		/// <summary>
		/// Cell values equal to this property value are considered as null value.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1Grid.NullString")]
		[Category("Options")]
		[Json(true, true, "")]
		[WidgetOption]
		public string NullString
		{
			get { return GetPropertyValue("NullString", ""); }
			set { SetPropertyValue("NullString", value); }
		}

		/// <summary>
		/// A value indicating whether DOM cell attributes can be passed within a data values.
		/// </summary>
		/// <remarks>
		/// <para>
		/// This option allows binding collection of values to data and automatically converting them as attributes of corresponded DOM table cells during rendering.
		/// Values should be passed as an array of two items, where first item is a value of the data field, the second item is a list of values.
		/// </para>
		/// <para>
		/// If DOM table is used as a datasource then attributes belonging to the cells in tBody section of the original table will be read and applied to the new cells.
		/// </para>
		/// </remarks>
		[DefaultValue(false)]
		[C1Description("C1Grid.ReadAttributesFromData")]
		[Category("Options")]
		[Json(true, true, false)]
		[WidgetOption()]
		public bool ReadAttributesFromData
		{
			get { return GetPropertyValue("ReadAttributesFromData", false); }
			set { SetPropertyValue("ReadAttributesFromData", value); }
		}

		/// <summary>
		/// Function used for styling the rows in wijgrid.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1Grid.RowStyleFormatter")]
		[Category("Options")]
		[Json(true, true, "")]
		[WidgetEvent("args")]
		[WidgetOptionName("rowStyleFormatter")]
		public string RowStyleFormatter
		{
			get { return GetPropertyValue("RowStyleFormatter", ""); }
			set { SetPropertyValue("RowStyleFormatter", value); }
		}

		/// <summary>
		/// Represents selection behavior.
		/// </summary>
		[DefaultValue(DEF_SELECTIONMODE)]
		[C1Description("C1Grid.SelectionMode")]
		[Category("Options")]
		[Json(true, true, DEF_SELECTIONMODE)]
		[WidgetOption]
		public virtual SelectionMode SelectionMode
		{
			get { return GetPropertyValue("SelectionMode", DEF_SELECTIONMODE); }
			set
			{
				if (GetPropertyValue("SelectionMode", DEF_SELECTIONMODE) != value)
				{
					SetPropertyValue("SelectionMode", value);
				}
			}
		}

		/// <summary>
		/// A value indicating whether a selection will be automatically displayed at the current cell position when the wijgrid is rendered.
		/// </summary>
		[DefaultValue(DEF_SHOWSELECTIONONRENDER)]
		[C1Description("C1Grid.ShowSelectionOnRender")]
		[Category("Options")]
		[Json(true, true, DEF_SHOWSELECTIONONRENDER)]
		[WidgetOption]
		public bool ShowSelectionOnRender
		{
			get { return GetPropertyValue("ShowSelectionOnRender", DEF_SHOWSELECTIONONRENDER); }
			set { SetPropertyValue("ShowSelectionOnRender", value); }
		}

		#endregion

		#region client-side events

		/// <summary>
		/// A function called when column has been dropped.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>drag</term>
		///						<description>Drag source, column being dragged.</description>
		///					</item>
		///					<item>
		///						<term>drop</term>
		///						<description>Drop target, column on which drag source is dropped(be null if dropping a column into empty group area).</description>
		///					</item>
		///					<item>
		///						<term>at</term>
		///						<description>Position to drop (one of the "left", "right" and "center" values) relative to drop target(be "left" if dropping a column into empty group area).</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientColumnDropped")]
		[Category("Client Events")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
		public string OnClientColumnDropped
		{
			get { return GetPropertyValue("OnClientColumnDropped", ""); }
			set { SetPropertyValue("OnClientColumnDropped", value); }
		}

		/// <summary>
		/// A function called when column has been dropped.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>drag</term>
		///						<description>Drag source, column being dragged.</description>
		///					</item>
		///					<item>
		///						<term>drop</term>
		///						<description>Drop target, column on which drag source is dropped(be null if dropping a column into empty group area).</description>
		///					</item>
		///					<item>
		///						<term>dragSource</term>
		///						<description>The place where the dragged column widget is located, possible value: "groupArea", "columns".</description>
		///					</item>
		///					<item>
		///						<term>dropSource</term>
		///						<description>The place where the dropped column widget is located, possible value: "groupArea", "columns".</description>
		///					</item>
		///					<item>
		///						<term>at</term>
		///						<description>Position to drop (one of the "left", "right" and "center" values) relative to drop target(be "left" if dropping a column into empty group area).</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientColumnGrouped")]
		[Category("Client Events")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
		public string OnClientColumnGrouped
		{
			get { return GetPropertyValue("OnClientColumnGrouped", ""); }
			set { SetPropertyValue("OnClientColumnGrouped", value); }
		}

		/// <summary>
		/// A function called when column has been removed from the group area.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>column</term>
		///						<description>column being removed.</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientColumnUngrouped")]
		[Category("Client Events")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
		public string OnClientColumnUngrouped
		{
			get { return GetPropertyValue("OnClientColumnUngrouped", ""); }
			set { SetPropertyValue("OnClientColumnUngrouped", value); }
		}

		/// <summary>
		/// A function called after page index is changed.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientPageIndexChanged")]
		[Category("Client Events")]
		[Json(true, true, "")]
		[WidgetEvent("e")]
		public string OnClientPageIndexChanged
		{
			get { return GetPropertyValue("OnClientPageIndexChanged", ""); }
			set { SetPropertyValue("OnClientPageIndexChanged", value); }
		}

		/// <summary>
		/// A function called after the widget is sorted.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>column</term>
		///						<description>column that is being sorted.</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientSorted")]
		[Category("Client Events")]
		[Json(true, true, "")]
		[WidgetEvent("e")]
		public string OnClientSorted
		{
			get { return GetPropertyValue("OnClientSorted", ""); }
			set { SetPropertyValue("OnClientSorted", value); }
		}

		/// <summary>
		/// A function called when wijgrid is bound to remote data and the ajax request fails.
		/// </summary>
		/// 
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>XMLHttpRequest</term>
		///						<description>The XMLHttpRequest object.</description>
		///					</item>
		///					<item>
		///						<term>textStatus</term>
		///						<description>A string describing the error type.</description>
		///					</item>
		///					<item>
		///						<term>errorThrown</term>
		///						<description>An exception object.</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// 
		/// <para>
		/// Refer to the jQuery.ajax.error event documentation for more details on this arguments.
		/// </para>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Grid.OnClientAjaxError")]
		[Category("Client Events")]
		[Json(true, true, "")]
		[WidgetEvent("e, args")]
		public string OnClientAjaxError
		{
			get { return GetPropertyValue("OnClientAjaxError", ""); }
			set { SetPropertyValue("OnClientAjaxError", value); }
		}

		#endregion

		#region protected members

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		/// <summary>
		/// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered in the browser window.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.RenderLicenseWebComment(writer);
			base.Render(writer);
		}

		#endregion

		#region private members

		private void OnRequiresDataBinding()
		{
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1GridExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		#endregion
	}
}