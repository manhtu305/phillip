﻿using System;

namespace C1.Web.Wijmo.Extenders.C1Grid
{
	using System.ComponentModel;
	using System.Web.UI;

	public partial class C1Band : C1BaseField
	{
		/// <summary>
		/// Constructor. Creates a new instance of the <b>C1Band</b> class.
		/// </summary>
		public C1Band(): base(null)
		{
		}

		internal C1Band(IColumnsContainer owner): base(owner)
		{
		}

		/// <summary>
		/// Creates a new instance of the <see cref="C1Band"/> class.
		/// </summary>
		/// <returns>A new instance of the <see cref="C1Band"/> class.</returns>
		protected internal override Settings CreateInstance()
		{
			return new C1Band();
		}
	}
}
