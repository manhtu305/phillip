﻿using System;
using System.ComponentModel;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Grid
#else
namespace C1.Web.Wijmo.Controls.C1GridView
#endif
{
	using System.Web.UI.WebControls;
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
	using C1.Web.Wijmo.Controls.Localization;
#endif

	public abstract partial class C1BaseField : IOwnerable
	{
		#region const

		private const bool DEF_ALLOWMOVING = true;
		private const bool DEF_ALLOWSIZING = true;
		private const bool DEF_ENSUREPXWIDTH = true;
		private const string DEF_HEADERTEXT = "";
		private const string DEF_FOOTERTEXT = "";
		private const bool DEF_VISIBLE = true;
		private const string DEF_WIDTH = "";

		#endregion


		private /*IColumnsContainer*/ IOwnerable _owner;
		private EventHandler _fieldChanged;

		#region options

		/// <summary>
		/// A value indicating whether the column can be moved.
		/// </summary>
		[C1Category("Category.Behavior")]
		[DefaultValue(DEF_ALLOWMOVING)]
		[C1Description("C1BaseField.AllowMoving")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_ALLOWMOVING)]
		[WidgetOption]
		public virtual bool AllowMoving
		{
			get { return GetPropertyValue("AllowMoving", DEF_ALLOWMOVING); }
			set
			{
				if (GetPropertyValue("AllowMoving", DEF_ALLOWMOVING) != value)
				{
					SetPropertyValue("AllowMoving", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// A value indicating whether the column can be sized.
		/// </summary>
		[C1Category("Category.Behavior")]
		[DefaultValue(DEF_ALLOWSIZING)]
		[C1Description("C1BaseField.AllowSizing")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_ALLOWSIZING)]
		[WidgetOption]
		public virtual bool AllowSizing
		{
			get { return GetPropertyValue("AllowSizing", DEF_ALLOWSIZING); }
			set
			{
				if (GetPropertyValue("AllowSizing", DEF_ALLOWSIZING) != value)
				{
					SetPropertyValue("AllowSizing", value);
					OnFieldChanged();
				}
			}
		}

#if EXTENDER
		/// <summary>
		/// Determines whether to use the <see cref="Width"/> option as the real width of the column.
		/// </summary>
		[C1Category("Category.Behavior")]
		[DefaultValue(DEF_ENSUREPXWIDTH)]
		[C1Description("C1BaseField.EnsurePxWidth")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_ENSUREPXWIDTH)]
		[WidgetOption]
		public virtual bool EnsurePxWidth
		{
			get { return GetPropertyValue("EnsurePxWidth", DEF_ENSUREPXWIDTH); }
			set
			{
				if (GetPropertyValue("EnsurePxWidth", DEF_ENSUREPXWIDTH) != value)
				{
					SetPropertyValue("EnsurePxWidth", value);
					OnFieldChanged();
				}
			}
		}
#else
		/// <summary>
		/// 
		/// </summary>
		[DefaultValue(DEF_ENSUREPXWIDTH)]
		[Json(true, true, DEF_ENSUREPXWIDTH)]
		[WidgetOption()]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public bool EnsurePxWidth
		{
			get	{ return true; }
		}
#endif

		/// <summary>
		/// Gets or sets the header text.
		/// </summary>
		[C1Category("Category.Appearance")]
		[DefaultValue(DEF_HEADERTEXT)]
		[C1Description("C1BaseField.HeaderText")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_HEADERTEXT)]
		[WidgetOption]
		public virtual string HeaderText
		{
			get { return GetPropertyValue("HeaderText", DEF_HEADERTEXT); }
			set
			{
				if (GetPropertyValue("HeaderText", DEF_HEADERTEXT) != value)
				{
					SetPropertyValue("HeaderText", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the footer text.
		/// </summary>
		[C1Category("Category.Appearance")]
		[DefaultValue(DEF_FOOTERTEXT)]
		[C1Description("C1BaseField.FooterText")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_FOOTERTEXT)]
		[WidgetOption]
		public virtual string FooterText
		{
			get { return GetPropertyValue("FooterText", DEF_FOOTERTEXT); }
			set
			{
				if (GetPropertyValue("FooterText", DEF_FOOTERTEXT) != value)
				{
					SetPropertyValue("FooterText", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// A value indicating whether column is visible.
		/// </summary>
		[C1Category("Category.Behavior")]
		[DefaultValue(DEF_VISIBLE)]
		[C1Description("C1BaseField.Visible")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_VISIBLE)]
		[WidgetOption]
		public virtual bool Visible
		{
			get { return GetPropertyValue("Visible", DEF_VISIBLE); }
			set
			{
				if (GetPropertyValue("Visible", DEF_VISIBLE) != value)
				{
					SetPropertyValue("Visible", value);
					OnFieldChanged();
				}
			}
		}

		/// <summary>
		/// Determines the width of the column.
		/// </summary>
		[C1Category("Category.Layout")]
		[DefaultValue(typeof(Unit), DEF_WIDTH)]
		[C1Description("C1BaseField.Width")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_WIDTH)]
		[WidgetOption]
		public virtual Unit Width
		{
			get { return GetPropertyValue("Width", Unit.Parse(DEF_WIDTH)); }
			set
			{
				if (GetPropertyValue("Width", Unit.Parse(DEF_WIDTH)) != value)
				{
					SetPropertyValue("Width", value);
					OnFieldChanged();
				}
			}
		}

		#endregion

		#region IOwnerable Members

		internal /*IColumnsContainer*/IOwnerable Owner
		{
			get { return _owner; }
			set { _owner = value; }
		}

		IOwnerable IOwnerable.Owner
		{
			get { return Owner; }
			set { Owner = /*(IColumnsContainer)*/value; }
		}

		#endregion

		/// <summary>
		/// Raises an event signaling that the <see cref="C1BaseField"/> object's state has changed.
		/// </summary>
		protected virtual void OnFieldChanged()
		{
			if (_fieldChanged != null)
			{
				_fieldChanged(this, EventArgs.Empty);
			}
		}

		internal event EventHandler FieldChanged
		{
			add { _fieldChanged = (EventHandler)Delegate.Combine(_fieldChanged, value); }
			remove { _fieldChanged = (EventHandler)Delegate.Remove(_fieldChanged, value); }
		}
	}
}
