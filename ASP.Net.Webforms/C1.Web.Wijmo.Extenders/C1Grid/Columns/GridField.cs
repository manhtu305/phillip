﻿using System;

namespace C1.Web.Wijmo.Extenders.C1Grid
{
	using System.ComponentModel;
	using System.Web.UI;
	using C1.Web.Wijmo.Extenders.C1DataSource;
	using C1.Web.Wijmo.Extenders.Localization;

	/// <summary>
	/// Represents a base class for all data-bound fields.
	/// </summary>
	public class C1Field : C1BaseField
	{
		#region const

		private const Aggregate DEF_AGGREGATE = Aggregate.None;
		private const bool DEF_ALLOWSORT = true;
		private const string DEF_DATAPARSER = "";
		private const FieldDataType DEF_DATATYPE = FieldDataType.String;
		private const FilterOperator DEF_FILTEROPERATOR = C1Grid.FilterOperator.NoFilter;
		private const string DEF_DATAFORMATSTRING = "";
		private const IMEMode DEF_IMEMODE = IMEMode.Auto;
		private const bool DEF_READONLY = false;
		private const RowMerge DEF_ROWMERGE = RowMerge.None;
		private const bool DEF_SHOWFILTER = true;
		private const C1SortDirection DEF_SORTDIRECTION = C1SortDirection.None;
		private const bool DEF_VALUEREQUIRED = false;

		#endregion

		#region private fields

		private GroupInfo _groupInfo;
		private ObjectValue _dataKey;
		private ObjectValue _filterValue;

		#endregion

		/// <summary>
		/// Initializes a new instance of the <b>C1Field</b> class.
		/// </summary>
		public C1Field() : this(null)
		{
		}

		internal C1Field(IColumnsContainer owner)	: base(owner)
		{
		}

		#region options

		/// <summary>
		/// Causes the grid to calculate aggregate values on the column and place them in the group header and footer rows.
		/// </summary>
		/// <remarks>
		/// If the grid does not contain any groups, setting the "aggregate" property has no effect.
		/// </remarks>
		[Category("Options")]
		[DefaultValue(DEF_AGGREGATE)]
		[C1Description("C1Field.Aggregate")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_AGGREGATE)]
		[WidgetOption]
		public Aggregate Aggregate
		{
			get { return GetPropertyValue("Aggregate", DEF_AGGREGATE); }
			set { SetPropertyValue("Aggregate", value); }
		}

		/// <summary>
		/// A value indicating whether column can be sorted.
		/// </summary>
		[Category("Options")]
		[DefaultValue(DEF_ALLOWSORT)]
		[C1Description("C1Field.AllowSort")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_ALLOWSORT)]
		[WidgetOption]
		public bool AllowSort
		{
			get { return GetPropertyValue("AllowSort", DEF_ALLOWSORT); }
			set { SetPropertyValue("AllowSort", value); }
		}

		/// <summary>
		/// A value indicating the key of the data field associated with a column.
		/// </summary>
		/// <remarks>
		/// If an array of hashes is used as a datasource for wijgrid, this should be string value, otherwise this should be an integer determining an index of the field in the datasource.
		/// </remarks>
		[Category("Options")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[C1Description("C1Field.DataKey")]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[Json(true, true, null)]
		[WidgetOption]
		public ObjectValue DataKey
		{
			get
			{
				if (_dataKey == null)
				{
					_dataKey = new ObjectValue();
				}

				return _dataKey;
			}
		}

		/// <summary>
		/// Data converter that is able to translate values from a string representation to column data type and back.
		/// </summary>
		/// <remarks>
		/// The dataParser is an object which must contains the following methods:
		///   parseDOM(value, culture, format): converts given DOM element into the typed value.
		///   parse(value, culture, format): converts the value into typed value.
		///   toStr(value, culture, format): converts the value into its string representation.
		/// </remarks>
		[Category("Options")]
		[DefaultValue(DEF_DATAPARSER)]
		[C1Description("C1Field.DataParser")]
		[NotifyParentProperty(true)]
		[WidgetEvent]
		[Json(true, true, DEF_DATAPARSER)]
		[WidgetOptionName("dataParser")]
		public string DataParser
		{
			get { return GetPropertyValue("DataParser", DEF_DATAPARSER); }
			set { SetPropertyValue("DataParser", value); }
		}

		/// <summary>
		/// Column data type. Used to determine the rules for sorting, grouping, aggregate calculation, and so on.
		/// </summary>
		[Category("Options")]
		[DefaultValue(DEF_DATATYPE)]
		[C1Description("C1Field.DataType")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_DATATYPE)]
		[WidgetOption]
		public FieldDataType DataType
		{
			get { return GetPropertyValue("DataType", DEF_DATATYPE); }
			set { SetPropertyValue("DataType", value); }
		}

		/// <summary>
		/// A pattern used for formatting and parsing column values.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The default value is undefined ("n" pattern will be used for "number" dataType, "d" for "datetime", "c" for "currency").
		/// </para>
		/// <para>
		/// See globalize.min.js for possible values.
		/// </para>
		/// </remarks>
		[Category("Options")]
		[DefaultValue(DEF_DATAFORMATSTRING)]
		[C1Description("C1Field.DataFormatString")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_DATAFORMATSTRING)]
		[WidgetOption]
		public string DataFormatString
		{
			get { return GetPropertyValue("DataFormatString", DEF_DATAFORMATSTRING); }
			set { SetPropertyValue("DataFormatString", value); }
		}

		/// <summary>
		/// An operation set for filtering. Must be either one of the embedded operators or custom filter operator.
		/// </summary>
		[Category("Options")]
		[DefaultValue(DEF_FILTEROPERATOR)]
		[C1Description("C1Field.FilterOperator")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_FILTEROPERATOR)]
		[WidgetOption]
		public FilterOperator FilterOperator
		{
			get { return GetPropertyValue("FilterOperator", DEF_FILTEROPERATOR); }
			set { SetPropertyValue("FilterOperator", value); }
		}

		/// <summary>
		/// A value set for filtering.
		/// </summary>
		[Category("Options")]
		[C1Description("C1Field.FilterValue")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[Json(true, true, null)]
		[WidgetOption]
		public ObjectValue FilterValue
		{
			get
			{
				if (_filterValue == null)
				{
					_filterValue = new ObjectValue();
				}

				return _filterValue;
			}
		}

		/// <summary>
		/// Using to customize the appearance and position of groups.
		/// </summary>
		[Category("Options")]
		[C1Description("C1Field.GroupInfo")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[Json(true, true, null)]
		[WidgetOption]
		public GroupInfo GroupInfo
		{
			get
			{
				if (_groupInfo == null)
				{
					_groupInfo = new GroupInfo(this);
				}

				return _groupInfo;
			}
		}

		/// <summary>
		/// A value indicating the state of the input method editor for text fields.
		/// </summary>
		[Category("Options")]
		[DefaultValue(DEF_IMEMODE)]
		[C1Description("C1Field.ImeMode")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_IMEMODE)]
		[WidgetOption]
		public IMEMode ImeMode
		{
			get { return GetPropertyValue("ImeMode", DEF_IMEMODE); }
			set { SetPropertyValue("ImeMode", value); }
		}

		/// <summary>
		/// A value indicating whether the cells in the column can be edited.
		/// </summary>
		[Category("Options")]
		[DefaultValue(DEF_READONLY)]
		[C1Description("C1Field.ReadOnly")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_READONLY)]
		[WidgetOption]
		public bool ReadOnly
		{
			get { return GetPropertyValue("ReadOnly", DEF_READONLY); }
			set { SetPropertyValue("ReadOnly", value); }
		}

		/// <summary>
		/// Determines whether rows are merged.
		/// </summary>
		[Category("Options")]
		[DefaultValue(DEF_ROWMERGE)]
		[C1Description("C1Field.RowMerge")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_ROWMERGE)]
		[WidgetOption]
		public RowMerge RowMerge
		{
			get { return GetPropertyValue("RowMerge", DEF_ROWMERGE); }
			set { SetPropertyValue("RowMerge", value); }
		}

		/// <summary>
		/// A value indicating whether filter editor will be shown in the filter row.
		/// </summary>
		[Category("Options")]
		[DefaultValue(DEF_SHOWFILTER)]
		[C1Description("C1Field.ShowFilter")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_SHOWFILTER)]
		[WidgetOption]
		public bool ShowFilter
		{
			get { return GetPropertyValue("ShowFilter", DEF_SHOWFILTER); }
			set { SetPropertyValue("ShowFilter", value); }
		}

		/// <summary>
		/// Determines the sort direction.
		/// </summary>
		[Category("Options")]
		[DefaultValue(DEF_SORTDIRECTION)]
		[C1Description("C1Field.SortDirection")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_SORTDIRECTION)]
		[WidgetOption]
		public C1SortDirection SortDirection
		{
			get { return GetPropertyValue("SortDirection", DEF_SORTDIRECTION); }
			set { SetPropertyValue("SortDirection", value); }
		}

		/// <summary>
		/// A value indicating whether null value is allowed during editing.
		/// </summary>
		[Category("Options")]
		[DefaultValue(DEF_VALUEREQUIRED)]
		[C1Description("C1Field.ValueRequired")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_VALUEREQUIRED)]
		[WidgetOption]
		public bool ValueRequired
		{
			get { return GetPropertyValue("ValueRequired", DEF_VALUEREQUIRED); }
			set { SetPropertyValue("ValueRequired", value); }
		}

		#endregion

		/// <summary>
		/// Creates a new instance of the <see cref="C1Field"/> class.
		/// </summary>
		/// <returns>A new instance of the <see cref="C1Field"/> class.</returns>
		protected internal override Settings CreateInstance()
		{
			return new C1Field();
		}
	}
}
