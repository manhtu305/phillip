﻿using System;
using System.ComponentModel;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Grid
#else
namespace C1.Web.Wijmo.Controls.C1GridView
#endif
{
	using System.Web.UI;

#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
	using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// Contain that allows organizing columns into hierarchical structure. Used to create multilevel column headers.
	/// </summary>
	public partial class C1Band	: IColumnsContainer
	{
		#region private fields

#if EXTENDER
		private C1BaseFieldCollection _columns;
#else
		private C1BaseFieldCollection _columns;
#endif

		#endregion

		#region options

		/// <summary>
		/// Gets a array of objects representing the columns of the band.
		/// </summary>
        [C1Category("Category.Default")]
		[C1Description("C1Band.Columns")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[WidgetOption]
#if EXTENDER
		public C1BaseFieldCollection Columns
#else
		[Browsable(false)]
		public C1BaseFieldCollection Columns
#endif
		{
			get
			{
				if (_columns == null)
				{
#if EXTENDER
					_columns = new C1BaseFieldCollection(this);
#else
					_columns = new C1BaseFieldCollection(this);
#endif
					_columns.FieldsChanged += OnBandChanged;

#if !EXTENDER
					if (IsTrackingViewState)
					{
						((IStateManager)_columns).TrackViewState();
					}
#endif
				}

				return _columns;
			}
		}

		#endregion

		private void OnBandChanged(object sender, EventArgs e)
		{
			OnFieldChanged();
		}
	}
}
