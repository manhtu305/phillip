﻿using System;
using System.ComponentModel;

namespace C1.Web.Wijmo.Extenders.C1Grid
{
	using C1.Web.Wijmo.Extenders.Localization;

	/// <summary>
	/// The base class for bands and all columns.
	/// </summary>
	public abstract partial class C1BaseField : Settings
	{
		private const string DEF_CELLFORMATTER = "";

		/// <summary>
		/// Constructor. Creates a new instance of the <see cref="C1BaseField"/> class.
		/// </summary>
		public C1BaseField(): this(null)
		{
		}

		internal C1BaseField(/*IColumnsContainer*/IOwnerable owner)
		{
			_owner = owner;
		}

		#region options

		/// <summary>
		/// Function used for changing content, style and attributes of the column cells.
		/// </summary>
		[Category("Options")]
		[DefaultValue(DEF_CELLFORMATTER)]
		[C1Description("C1BaseField.CellFormatter", "Function used for changing content, style and attributes of the column cells.")]
		[NotifyParentProperty(true)]
		[WidgetEvent("args")]
		[Json(true, true, DEF_CELLFORMATTER)]
		[WidgetOptionName("cellFormatter")]
		public string CellFormatter
		{
			get { return GetPropertyValue("CellFormatter", DEF_CELLFORMATTER); }
			set
			{
				if (!TestPropertyValue("CellFormatter", value))
				{
					SetPropertyValue("CellFormatter", value);
					OnFieldChanged();
				}
			}
		}

		#endregion
	}
}