﻿using System;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Grid
#else
namespace C1.Web.Wijmo.Controls.C1GridView
#endif
{
	using System.ComponentModel;
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
	using C1.Web.Wijmo.Controls.Localization;
#endif

	/// <summary>
	/// GroupInfo is used to customize the appearance and position of groups.
	/// </summary>
	public partial class GroupInfo : IJsonEmptiable, IOwnerable
	{
		#region const

		private const bool DEF_GROUPSINGLEROW = true;
		private const string DEF_COLLAPSEDIMAGECLASS = "ui-icon-plus";
		private const string DEF_EXPANDEDIMAGECLASS = "ui-icon-minus";
		private const GroupPosition DEF_POSITION = GroupPosition.None;
		private const OutlineMode DEF_OUTLINEMODE = OutlineMode.StartExpanded;
		private const string DEF_HEADERTEXT = "";
		private const string DEF_FOOTERTEXT = "";

		#endregion

		private IOwnerable _owner;

		/// <summary>
		/// Initializes a new instance of the <b>GroupInfo</b> class.
		/// </summary>
		public GroupInfo() : this(null)
		{
		}

		internal GroupInfo(IOwnerable owner) : base()
		{
			_owner = owner;
		}

		#region options

		/// <summary>
		/// Using to customize the appearance and position of groups.
		/// </summary>
        [C1Category("Category.Behavior")]
		[DefaultValue(DEF_GROUPSINGLEROW)]
		[C1Description("GroupInfo.GroupSingleRow")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_GROUPSINGLEROW)]
		[WidgetOption]
		public bool GroupSingleRow
		{
			get { return GetPropertyValue("GroupSingleRow", DEF_GROUPSINGLEROW); }
			set
			{
				if (GetPropertyValue("GroupSingleRow", DEF_GROUPSINGLEROW) != value)
				{
					SetPropertyValue("GroupSingleRow", value);
				}
			}
		}

		/// <summary>
		/// Determines the css used to show collapsed nodes on the grid.
		/// </summary>
        [C1Category("Category.Style")]
		[DefaultValue(DEF_COLLAPSEDIMAGECLASS)]
		[C1Description("GroupInfo.CollapsedImageClass")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_COLLAPSEDIMAGECLASS)]
		[WidgetOption]
		public string CollapsedImageClass
		{
			get { return GetPropertyValue("CollapsedImageClass", DEF_COLLAPSEDIMAGECLASS); }
			set
			{
				if (GetPropertyValue("CollapsedImageClass", DEF_COLLAPSEDIMAGECLASS) != value)
				{
					SetPropertyValue("CollapsedImageClass", value);
				}
			}
		}

		/// <summary>
		/// Determines the css used to show expanded nodes on the grid.
		/// </summary>
        [C1Category("Category.Style")]
		[DefaultValue(DEF_EXPANDEDIMAGECLASS)]
		[C1Description("GroupInfo.ExpandedImageClass")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_EXPANDEDIMAGECLASS)]
		[WidgetOption]
		public string ExpandedImageClass
		{
			get { return GetPropertyValue("ExpandedImageClass", DEF_EXPANDEDIMAGECLASS); }
			set
			{
				if (GetPropertyValue("ExpandedImageClass", DEF_EXPANDEDIMAGECLASS) != value)
				{
					SetPropertyValue("ExpandedImageClass", value);
				}
			}
		}

		/// <summary>
		/// Determines whether the grid should insert group header and/or group footer rows for this column.
		/// </summary>
        [C1Category("Category.Layout")]
		[DefaultValue(DEF_POSITION)]
		[C1Description("GroupInfo.Position")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_POSITION)]
		[WidgetOption]
		public GroupPosition Position
		{
			get { return GetPropertyValue("Position", DEF_POSITION); }
			set
			{
				if (GetPropertyValue("Position", DEF_POSITION) != value)
				{
					SetPropertyValue("Position", value);
				}
			}
		}

		/// <summary>
		/// Determines whether the user will be able to collapse and expand the groups by clicking on the group headers, and also determines whether groups will be initially collapsed or expanded.
		/// </summary>
        [C1Category("Category.Behavior")]
		[DefaultValue(DEF_OUTLINEMODE)]
		[C1Description("GroupInfo.OutlineMode")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_OUTLINEMODE)]
		[WidgetOption]
		public OutlineMode OutlineMode
		{
			get { return GetPropertyValue("OutlineMode", DEF_OUTLINEMODE); }
			set
			{
				if (GetPropertyValue("OutlineMode", DEF_OUTLINEMODE) != value)
				{
					SetPropertyValue("OutlineMode", value);
				}
			}
		}

		/// <summary>
		/// Determines the text that is displayed in the group header rows.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The text may include up to three placeholders:
		/// "{0}" is replaced with the value being grouped on.
		/// "{1}" is replaced with the group's column header.
		/// "{2}" is replaced with the aggregate
		///</para>
		///<para>
		/// The text may be set to "custom". Doing so causes the OnClientGroupText event to be raised when processing a grouped header.
		/// </para>
		/// </remarks>
        [C1Category("Category.Appearance")]
		[DefaultValue(DEF_HEADERTEXT)]
		[C1Description("GroupInfo.HeaderText")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_HEADERTEXT)]
		[WidgetOption]
		public string HeaderText
		{
			get { return GetPropertyValue("HeaderText", DEF_HEADERTEXT); }
			set
			{
				if (GetPropertyValue("HeaderText", DEF_HEADERTEXT) != value)
				{
					SetPropertyValue("HeaderText", value);
				}
			}
		}

		/// <summary>
		/// Determines the text that is displayed in the group footer rows.
		/// </summary>
		/// <remarks>
		/// <para>
		/// The text may include up to three placeholders:
		/// "{0}" is replaced with the value being grouped on.
		/// "{1}" is replaced with the group's column header.
		/// "{2}" is replaced with the aggregate
		///</para>
		///<para>
		/// The text may be set to "custom". Doing so causes the OnClientGroupText event to be raised when processing a grouped footer.
		/// </para>
		/// </remarks>
        [C1Category("Category.Appearance")]
		[DefaultValue(DEF_FOOTERTEXT)]
		[C1Description("GroupInfo.FooterText")]
		[NotifyParentProperty(true)]
		[Json(true, true, DEF_FOOTERTEXT)]
		[WidgetOption]
		public string FooterText
		{
			get { return GetPropertyValue("FooterText", DEF_FOOTERTEXT); }
			set 
			{
				if (GetPropertyValue("FooterText", DEF_FOOTERTEXT) != value)
				{
					SetPropertyValue("FooterText", value);
				}
			}
		}

		#endregion

		#region IJsonEmptiable Members

		bool IJsonEmptiable.IsEmpty
		{
			get
			{
				return (this.CollapsedImageClass == DEF_COLLAPSEDIMAGECLASS &&
					this.ExpandedImageClass == DEF_EXPANDEDIMAGECLASS &&
					this.FooterText == DEF_FOOTERTEXT &&
					this.GroupSingleRow == DEF_GROUPSINGLEROW &&
					this.HeaderText == DEF_HEADERTEXT &&
					this.OutlineMode == DEF_OUTLINEMODE &&
					this.Position == DEF_POSITION);
			}
		}

		#endregion

		#region IOwnerable Members

		IOwnerable IOwnerable.Owner
		{
			get { return _owner; }
			set { _owner = value; }
		}

		#endregion
	}
}