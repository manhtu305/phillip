﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Grid
#else
namespace C1.Web.Wijmo.Controls.C1GridView
#endif
{
	using System.ComponentModel;
	using C1.Web.Wijmo.Extenders.Base.Collections;

	/// <summary>
	/// Represents a collection of <see cref="C1BaseField"/> objects.
	/// </summary>
	[Editor(typeof(C1BaseFieldCollection._Editor), typeof(System.Drawing.Design.UITypeEditor))]
	public class C1BaseFieldCollection : C1ObservableItemCollection<IColumnsContainer, C1BaseField>, IJsonEmptiable
	{
		private class _Editor : System.ComponentModel.Design.CollectionEditor
		{
			public _Editor(Type type) : base(type)
			{
			}

			protected override Type[] CreateNewItemTypes()
			{
				return new Type[] {
						typeof(C1Field),
						typeof(C1Band)
				};
			}
		}

		private EventHandler _fieldsChanged;

		/// <summary>
		/// Initializes a new instance of the <b>C1BaseFieldCollection</b> class.
		/// </summary>
		public C1BaseFieldCollection()
			: this(null)
		{
		}

		internal C1BaseFieldCollection(IColumnsContainer owner) : base(owner)
		{
		}

		/// <summary>
		/// Removes all elements from the collection.
		/// </summary>
		protected override void ClearItems()
		{
			foreach (C1BaseField field in Items)
			{
				field.Owner = null;
			}

			base.ClearItems();
		}

		/// <summary>
		/// Inserts an element into the collection at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index at which item should be inserted.</param>
		/// <param name="item">The element to insert.</param>
		protected override void InsertItem(int index, C1BaseField item)
		{
			item.Owner = Owner;
			base.InsertItem(index, item);
		}

		/// <summary>
		/// Removes the element at the specified index of the collection.
		/// </summary>
		/// <param name="index">The zero-based index of the element to remove.</param>
		protected override void RemoveItem(int index)
		{
			Items[index].Owner = null;
			base.RemoveItem(index);
		}

		/// <summary>
		/// Replaces the element at the specified index.
		/// </summary>
		/// <param name="index">The zero-based index of the element to replace.</param>
		/// <param name="item"> The new value for the element at the specified index.</param>
		protected override void SetItem(int index, C1BaseField item)
		{
			if (item != null)
			{
				item.Owner = Owner;
				base.SetItem(index, item);
			}
		}

		#region IJsonEmptiable Members

		bool IJsonEmptiable.IsEmpty
		{
			get { return Count == 0; }
		}

		#endregion

		internal event EventHandler FieldsChanged
		{
			add { _fieldsChanged = (EventHandler)Delegate.Combine(_fieldsChanged, value); }
			remove { _fieldsChanged = (EventHandler)Delegate.Remove(_fieldsChanged, value); }
		}

		private void OnFieldsChanged(object sender, EventArgs e)
		{
			if (_fieldsChanged != null)
			{
				_fieldsChanged(sender, e);
			}
		}
	}
}
