﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace C1.Web.Wijmo.Extenders.C1Grid
{
	using C1.Web.Wijmo.Extenders.Converters;

	/// <summary>
	/// Represents a custom filter operator.
	/// </summary>
	public class CustomFilterOperator : ICustomOptionType, IJsonEmptiable
	{
		private const string DEF_NAME = "";
		private const int DEF_ARITY = 1;
		private const FieldDataType DEF_APPLICABLETO = FieldDataType.String;
		private const string DEF_OPERATOR = "";

		private int _arity = 1;

		/// <summary>
		/// Initializes a new instance of the <b>CustomFilterOperator</b> class.
		/// </summary>
		public CustomFilterOperator()
		{
			// set default values
			Name = DEF_NAME;
			Arity = DEF_ARITY;
			ApplicableTo = DEF_APPLICABLETO;
			Operator = DEF_OPERATOR;
		}

		#region options

		/// <summary>
		/// Operator name.
		/// </summary>
		[DefaultValue(DEF_NAME)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// The number of filter operands. Can be either 1 or 2.
		/// </summary>
		[DefaultValue(DEF_ARITY)]
		[WidgetOption]
		[NotifyParentProperty(true)]
		public int Arity
		{
			get { return _arity; }
			set
			{
				if (value < 1 || value > 2)
				{
					throw new ArgumentOutOfRangeException("value", "Value should be either 1 or 2");
				}

				_arity = value;
			}
		}

		/// <summary>
		/// An array of datatypes to which the filter can be applied.
		/// </summary>
		[DefaultValue(DEF_APPLICABLETO)]
		[WidgetOption]
		[NotifyParentProperty(true)]
		public FieldDataType ApplicableTo
		{
			get;
			set;
		}

		/// <summary>
		/// Comparsion operator, the number of accepted parameters depends upon the <see cref="Arity"/>.
		/// The first parameter is a data value, the second parameter is a filter value.
		/// </summary>
		[DefaultValue(DEF_OPERATOR)]
		[WidgetOption]
		public string Operator
		{
			get;
			set;
		}

		#endregion

		#region private members

		#endregion


		#region ICustomOptionType Members

		string ICustomOptionType.SerializeValue()
		{

			FieldDataType dt = (FieldDataType)new EnumConverter(typeof(FieldDataType)).ConvertFrom("currency");

			if (string.IsNullOrEmpty(Name))
			{
				throw new ArgumentException("Name", "value should be specified.");
			}

			if (string.IsNullOrEmpty(Operator))
			{
				throw new ArgumentException("Operator", "value should be specified.");
			}

			if (ApplicableTo == 0)
			{
				throw new ArgumentException("ApplicableTo", "value should be specified.");
			}

			return string.Format("{{ name: \"{0}\", arity: {1}, applicableTo: [{2}], operator: {3} }}",
				Name, Arity, new QuotedEnumConverter(typeof(FieldDataType)).ConvertTo(ApplicableTo, typeof(string)), Operator);
		}

		bool ICustomOptionType.IncludeQuotes
		{
			get { return false; }
		}

		#endregion

		#region IJsonEmptiable Members

		bool IJsonEmptiable.IsEmpty
		{
			get
			{
				return (ApplicableTo == DEF_APPLICABLETO && Arity == DEF_ARITY && Name == DEF_NAME && Operator == DEF_OPERATOR);
			}
		}

		#endregion
	}
}
