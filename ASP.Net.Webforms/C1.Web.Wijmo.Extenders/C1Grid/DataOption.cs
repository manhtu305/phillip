﻿using System;
using System.Collections;

namespace C1.Web.Wijmo.Extenders.C1Grid
{
	using System.ComponentModel;
	using System.Web.UI.WebControls;
	using C1.Web.Wijmo.Extenders.Localization;

	/// <summary>
	/// Determines the datasource of the <see cref="C1Grid"/>.
	/// </summary>
	public class DataOption : Settings, ICustomOptionType, IJsonEmptiable
	{
		private C1GridExtender _grid;

		/// <summary>
		/// Initializes a new instance of the <b>DataOption</b> class.
		/// </summary>
		public DataOption()
		{
		}

		internal DataOption(C1GridExtender grid)
		{
			_grid = grid;
		}

		/// <summary>
		/// Gets or sets the object from which the data-bound control retrieves its list of data items.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public object DataSource
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the ID of the control from which the data-bound control retrieves its list of data items.
		/// </summary>
		[DefaultValue("")]
		[C1Description("C1Grid.Data.DataSourceID")]
		public string DataSourceID
		{
			get { return GetPropertyValue("DataSourceID", string.Empty); }
			set { SetPropertyValue("DataSourceID", value); }
		}

		#region ICustomOptionType Members

		string ICustomOptionType.SerializeValue()
		{
			if (string.IsNullOrEmpty(DataSourceID) && DataSource != null)
			{
				return new DataSerializer().Serialize(DataSource, _grid.DataMember);
			}
			else
			{
				return this.DataSourceID;
			}
		}

		bool ICustomOptionType.IncludeQuotes
		{
			get { return false; }
		}

		#endregion

		#region IJsonEmptiable Members

		bool IJsonEmptiable.IsEmpty
		{
			get { return string.IsNullOrEmpty(DataSourceID) && DataSource == null; }
		}

		#endregion
	}
}
