﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using C1.Web.Wijmo;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Tree", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Tree
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1TreeView", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1TreeView
#endif
{
		[WidgetDependencies(
            typeof(WijTree)
#if !EXTENDER
, "extensions.c1treeview.js",
ResourcesConst.C1WRAPPER_PRO
#endif
)]
#if EXTENDER
	public partial class C1TreeExtender
#else 
	public partial class C1TreeView
#endif
	{
		private Animation _collapseOption;
		private Animation _expandOption;

		#region ** options.
		/// <summary>
		/// Allows tree nodes to be dragged
		/// </summary>
		[C1Description("C1TreeView.AllowDrag")]
        [C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool AllowDrag
		{
			get
			{
				return GetPropertyValue("AllowDrag", false);
			}
			set
			{
				SetPropertyValue("AllowDrag", value);
			}
		}

		/// <summary>
		/// Allows tree nodes to be dropped
		/// </summary>
		[C1Description("C1TreeView.AllowDrop")]
        [C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool AllowDrop
		{
			get
			{
				return GetPropertyValue("AllowDrop", false);
			}
			set
			{
				SetPropertyValue("AllowDrop", value);
			}
		}

		/// <summary>
		/// Allows tree nodes to be edited at run time.
		/// </summary>
		[C1Description("C1TreeView.AllowEdit")]
        [C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool AllowEdit
		{
			get
			{
				return GetPropertyValue("AllowEdit", false);
			}
			set
			{
				SetPropertyValue("AllowEdit", value);
			}
		}

		/// <summary>
		/// Allows tree nodes to be sorted at run time.
		/// </summary>
		[C1Description("C1TreeView.AllowSorting")]
        [C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool AllowSorting
		{
			get
			{
				return GetPropertyValue("AllowSorting", true);
			}
			set
			{
				SetPropertyValue("AllowSorting", value);
			}
		}

		/// <summary>
		/// Allows triple state of checkBox.
		/// </summary>
		[C1Description("C1TreeView.AllowTriState")]
        [C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool AllowTriState
		{
			get
			{
				return GetPropertyValue("AllowTriState", true);
			}
			set
			{
				SetPropertyValue("AllowTriState", value);
			}
		}

		/// <summary>
		/// Allows sub-nodes to be checked upon parent node check.
		/// </summary>
		[C1Description("C1TreeView.AutoCheckNodes")]
        [C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool AutoCheckNodes
		{
			get
			{
				return GetPropertyValue("AutoCheckNodes", true);
			}
			set
			{
				SetPropertyValue("AutoCheckNodes", value);
			}
		}

		/// <summary>
		/// If this option is set to true, 
		/// the expanded node will be collapsed if another node is expanded.
		/// </summary>
		[C1Description("C1TreeView.AutoCollapse")]
        [C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool AutoCollapse
		{
			get
			{
				return GetPropertyValue("AutoCollapse", false);
			}
			set
			{
				SetPropertyValue("AutoCollapse", value);
			}
		}

		/// <summary>
		/// If this option is set to true, the tree will be expand/Collapse
		/// when the mouse hovers on the expand/Collapse button.
		/// </summary>
		[C1Description("C1TreeView.ExpandCollapseHoverUsed")]
        [C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool ExpandCollapseHoverUsed
		{
			get
			{
				return GetPropertyValue("ExpandCollapseHoverUsed", false);
			}
			set
			{
				SetPropertyValue("ExpandCollapseHoverUsed", value);
			}
		}

		/// <summary>
		/// Allows the CheckBox to be shown on tree nodes
		/// </summary>
		[C1Description("C1TreeView.ShowCheckBoxes")]
        [C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public bool ShowCheckBoxes
		{
			get
			{
				return GetPropertyValue("ShowCheckBoxes", false);
			}
			set
			{
				SetPropertyValue("ShowCheckBoxes", value);
			}
		}

		/// <summary>
		/// Allows tree nodes to be expanded or collapsed.
		/// </summary>
		[C1Description("C1TreeView.ShowExpandCollapse")]
        [C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public bool ShowExpandCollapse
		{
			get
			{
				return GetPropertyValue("ShowExpandCollapse", true);
			}
			set
			{
				SetPropertyValue("ShowExpandCollapse", value);
			}
		}

		/// <summary>
		/// Animation options for showing the child nodes when the parent node is expanded.
		/// </summary>
		[C1Description("C1TreeView.ExpandAnimation")]
        [C1Category("Category.Behavior")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Animation ExpandAnimation
		{
			get
			{
				if (this._expandOption == null)
				{
					this._expandOption = new Animation();
				}
				return this._expandOption;
			}
			set
			{
				this._expandOption = value;
			}
		}

		/// <summary>
		/// Animation options for showing the child nodes when the parent node is collapsed.
		/// </summary>
		[C1Description("C1TreeView.CollapseAnimation")]
        [C1Category("Category.Behavior")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Animation CollapseAnimation
		{
			get
			{
				if (this._collapseOption == null)
				{
					this._collapseOption = new Animation();
				}
				return this._collapseOption;
			}
			set
			{
				this._collapseOption = value;
			}
		}

		/// <summary>
		/// The duration of the time to delay before the node is expanded.
		/// </summary>
		[C1Description("C1TreeView.ExpandDelay")]
        [C1Category("Category.Behavior")]
		[DefaultValue(0)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int ExpandDelay
		{
			get
			{
				return GetPropertyValue("ExpandDelay", 0);
			}
			set
			{
				SetPropertyValue("ExpandDelay", value);
			}
		}

		/// <summary>
		/// The duration of the time to delay before the node is collapsed.
		/// </summary>
		[C1Description("C1TreeView.CollapseDelay")]
        [C1Category("Category.Behavior")]
		[DefaultValue(0)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int CollapseDelay
		{
			get
			{
				return GetPropertyValue("CollapseDelay", 0);
			}
			set
			{
				SetPropertyValue("CollapseDelay", value);
			}
		}
		#endregion end of ** options.

		#region ** client events

		/// <summary>
		/// The name of the function which will be called when the node is expanded.
		/// </summary>
		[C1Description("C1TreeView.OnClientNodeExpanded")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, node")]
		[WidgetOptionName("nodeExpanded")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientNodeExpanded
		{
			get
			{
				return this.GetPropertyValue("OnClientNodeExpanded", "");
			}
			set
			{
				this.SetPropertyValue("OnClientNodeExpanded", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called when the node is collapsed.
		/// </summary>
		[C1Description("C1TreeView.OnClientNodeCollapsed")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, node")]
		[WidgetOptionName("nodeCollapsed")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientNodeCollapsed
		{
			get
			{
				return this.GetPropertyValue("OnClientNodeCollapsed", "");
			}
			set
			{
				this.SetPropertyValue("OnClientNodeCollapsed", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called when the node begin to be dragged.
		/// </summary>
		[C1Description("C1TreeView.OnClientNodeDragStarted")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, node")]
		[WidgetOptionName("nodeDragStarted")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientNodeDragStarted
		{
			get
			{
				return this.GetPropertyValue("OnClientNodeDragStarted", "");
			}
			set
			{
				this.SetPropertyValue("OnClientNodeDragStarted", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called when the node is dragged.
		/// </summary>
		[C1Description("C1TreeView.OnClientNodeDragging")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, node")]
		[WidgetOptionName("nodeDragging")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientNodeDragging
		{
			get
			{
				return this.GetPropertyValue("OnClientNodeDragging", "");
			}
			set
			{
				this.SetPropertyValue("OnClientNodeDragging", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called after the node is dropped.
		/// </summary>
		[C1Description("C1TreeView.OnClientNodeDropped")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, data")]
		[WidgetOptionName("nodeDropped")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientNodeDropped
		{
			get
			{
				return this.GetPropertyValue("OnClientNodeDropped", "");
			}
			set
			{
				this.SetPropertyValue("OnClientNodeDropped", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called after the node is clicked.
		/// </summary>
		[C1Description("C1TreeView.OnClientNodeClicked")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, node")]
		[WidgetOptionName("nodeClick")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientNodeClick
		{
			get
			{
				return this.GetPropertyValue("OnClientNodeClick", "");
			}
			set
			{
				this.SetPropertyValue("OnClientNodeClick", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called before the node is checked.
		/// This event can be cancelled by returning false.
		/// </summary>
		[C1Description("C1TreeView.OnClientNodeCheckChanging")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, node")]
		[WidgetOptionName("nodeCheckChanging")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientNodeCheckChanging
		{
			get
			{
				return this.GetPropertyValue("OnClientNodeCheckChanging", "");
			}
			set
			{
				this.SetPropertyValue("OnClientNodeCheckChanging", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called when the node is checked.
		/// </summary>
		[C1Description("C1TreeView.OnClientNodeCheckChanged")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, node")]
		[WidgetOptionName("nodeCheckChanged")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientNodeCheckChanged
		{
			get
			{
				return this.GetPropertyValue("OnClientNodeCheckChanged", "");
			}
			set
			{
				this.SetPropertyValue("OnClientNodeCheckChanged", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called when mouse over the node.
		/// </summary>
		[C1Description("C1TreeView.OnClientNodeMouseOver")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, node")]
		[WidgetOptionName("nodeMouseOver")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientNodeMouseOver
		{
			get
			{
				return this.GetPropertyValue("OnClientNodeMouseOver", "");
			}
			set
			{
				this.SetPropertyValue("OnClientNodeMouseOver", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called when mouse out the node.
		/// </summary>
		[C1Description("C1TreeView.OnClientNodeMouseOut")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, node")]
		[WidgetOptionName("nodeMouseOut")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientNodeMouseOut
		{
			get
			{
				return this.GetPropertyValue("OnClientNodeMouseOut", "");
			}
			set
			{
				this.SetPropertyValue("OnClientNodeMouseOut", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called when the text of node changed.
		/// </summary>
		[C1Description("C1TreeView.OnClientNodeTextChanged")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, node")]
		[WidgetOptionName("nodeTextChanged")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientNodeTextChanged
		{
			get
			{
				return this.GetPropertyValue("OnClientNodeTextChanged", "");
			}
			set
			{
				this.SetPropertyValue("OnClientNodeTextChanged", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called when the selected nodes are changed.
		/// </summary>
		[C1Description("C1TreeView.OnClientSelectedNodesChanged")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, node")]
		[WidgetOptionName("selectedNodeChanged")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientSelectedNodeChanged
		{
			get
			{
				return this.GetPropertyValue("OnClientSelectedNodeChanged", "");
			}
			set
			{
				this.SetPropertyValue("OnClientSelectedNodeChanged", value);
			}
		}

		#endregion end of ** client events.

		#region ** methods for serialization

		/// <summary>
		/// Determine whether the ExpandAnimation property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeExpandAnimation()
		{
			return this.ExpandAnimation.Animated.Disabled != false
				|| this.ExpandAnimation.Animated.Effect != "slide"
				|| this.ExpandAnimation.Duration != 400
				|| this.ExpandAnimation.Option.Count != 0
				|| this.ExpandAnimation.Easing != Easing.Swing;
		}

		/// <summary>
		/// Determine whether the CollapseAnimation property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeCollapseAnimation()
		{
			return this.CollapseAnimation.Animated.Disabled != false
				|| this.CollapseAnimation.Animated.Effect != "slide"
				|| this.CollapseAnimation.Duration != 400
				|| this.CollapseAnimation.Option.Count != 0
				|| this.CollapseAnimation.Easing != Easing.Swing;
		}

		#endregion end of ** methods for serialization.
	}
}
