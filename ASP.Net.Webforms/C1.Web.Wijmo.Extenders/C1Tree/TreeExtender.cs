﻿using System.ComponentModel;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using C1.Web.Wijmo;
using System;

[assembly: TagPrefix("C1.Web.Wijmo.Extenders.Tree", "Tree")]
namespace C1.Web.Wijmo.Extenders.C1Tree
{
	/// <summary>
	/// Tree Extender.
	/// </summary>
	[TargetControlType(typeof(Panel))]
	[ToolboxItem(true)]
	[ToolboxBitmap(typeof(C1TreeExtender), "Tree.png")]
	[LicenseProviderAttribute()]
	public partial class C1TreeExtender : WidgetExtenderControlBase
	{
		private bool _productLicensed = false;
		public C1TreeExtender()
		{
			VerifyLicense();
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1TreeExtender), this, false);
#if GRAPECITY
            _productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}
	}
}