﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Drawing;
using System.Web.UI;
using C1.Web.Wijmo.Extenders.Converters;

namespace C1.Web.Wijmo.Extenders.C1Tooltip
{
	/// <summary>
	/// Tooltip extender
	/// </summary>
	[ToolboxBitmap(typeof(C1TooltipExtender), "Tooltip.png")]
	[LicenseProvider]
	[ToolboxItem(true)]
	public partial class C1TooltipExtender : WidgetExtenderControlBase
	{

		#region ** fields
		private bool _productLicensed = false;
		#endregion

		#region ** constructor
		public C1TooltipExtender()
		{
			VerifyLicense();

			this.Position = new PositionSettings();
			this.Position.My = new Position() { Left = HPosition.Left, Top = VPosition.Bottom };
			this.Position.At = new Position() { Left = HPosition.Right, Top = VPosition.Top };
			this.Position.Offset = new Offset();
			this.Position.Collision = new Collision() { Left = CollisionMode.Flip, Top = CollisionMode.Flip };
			this.Animation = new Animation() { Animated = new AnimatedOption() { Effect = "fade" }, Duration = 400, Easing = Easing.Swing, Option = new Dictionary<string, string>() };
			this.ShowAnimation = new Animation() { Animated = new AnimatedOption() { Effect = "fade" } };
			this.HideAnimation = new Animation() { Animated = new AnimatedOption() { Effect = "fade" } };
			this.CalloutAnimation = new SlideAnimation();
		}

		internal virtual void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1TooltipExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		} 
		#endregion

		#region ** options


		#endregion

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}
		/// <summary>
		/// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered in the browser window.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.RenderLicenseWebComment(writer);
			base.Render(writer);
		}
	}
}
