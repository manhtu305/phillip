﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Collections;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif


#if EXTENDER
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Tooltip", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1Tooltip
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1ToolTip", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1ToolTip
#endif
{
	[WidgetDependencies(
        typeof(WijToolTip)
#if !EXTENDER
, "extensions.c1tooltip.js",
ResourcesConst.C1WRAPPER_OPEN
#endif
)]
	#if EXTENDER
	public partial class C1TooltipExtender
	#else
	public partial class C1ToolTip
	#endif
	{

		#region ** fields
		private PositionSettings _position;
		private Animation _animation;
		private Animation _showAnimation;
		private Animation _hideAnimation;
		private SlideAnimation _calloutAnimation;
		private ContentOption _content;
		private ContentOption _title;
		#endregion
		
		#region ** options
		/// <summary>
		/// Gets and sets the tooltip's content.
		/// </summary>
		[C1Description("C1ToolTip.Content")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if!EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public ContentOption Content
		{
			get
			{
				if (_content == null)
				{
					_content = new ContentOption();
				}
				return _content;
			}
			set
			{
				_content = value;
			}
		}
		
		/// <summary>
		/// Specifies a value that sets the tooltip’s title.
		/// </summary>
		[C1Description("C1ToolTip.Title")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if!EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public ContentOption Title
		{
			get
			{
				if (_title == null)
				{
					_title = new ContentOption();
				}
				return _title;
			}
			set
			{
				_title = value;
			}
		}
	
		/// <summary>
		/// Determines how to close the tooltip.
		/// </summary>
		[C1Description("C1ToolTip.CloseBehavior")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DefaultValue(CloseBehavior.Auto)]
#if!EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public CloseBehavior CloseBehavior
		{
			get
			{
				return GetPropertyValue("CloseBehavior", CloseBehavior.Auto);
			}
			set
			{
				SetPropertyValue("CloseBehavior", value);
			}
		}

		/// <summary>
		/// If true, then the tooltip moves with the mouse.
		/// </summary>
		[C1Description("C1ToolTip.MouseTrailing")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
#if!EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool MouseTrailing
		{
			get
			{
				return GetPropertyValue("MouseTrailing", false);
			}
			set
			{
				SetPropertyValue("MouseTrailing", value);
			}
		}

		/// <summary>
		/// Gets and sets the event that will cause the tooltip to appear.
		/// </summary>
		[C1Description("C1ToolTip.Triggers")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DefaultValue(Trigger.Hover)]
#if!EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Trigger Triggers
		{
			get
			{
				return GetPropertyValue("Triggers", Trigger.Hover);
			}
			set
			{
				SetPropertyValue("Triggers", value);
			}
		}

		/// <summary>
		/// Sets the tooltip's position mode in relation to the 'relativeTo', 'offsetX', 
		/// and 'offsetY' properties. For example, here is the jQuery ui position's 
		/// position:{my:'top left',at:'right bottom',offset:}.
		/// </summary>
		[C1Description("C1ToolTip.Position")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if!EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public PositionSettings Position
		{
			get
			{
				if (_position == null)
				{
					_position = new PositionSettings();
				}
				return _position;
			}
			set
			{
				_position = value;
			}
		}

		/// <summary>
		/// Determines whether to show the callout element.
		/// </summary>
		[C1Description("C1ToolTip.ShowCallout")]
		[WidgetOption]
		[C1Category("Category.Appearance")]
		[DefaultValue(true)]
#if!EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public bool ShowCallout
		{
			get
			{
				return GetPropertyValue("ShowCallout", true);
			}
			set
			{
				SetPropertyValue("ShowCallout", value);
			}
		}

		/// <summary>
        /// Defines the animation to show or hide the tooltip.
		/// If the showAnimation/hideAnimation is not specified, then this property works.
		/// </summary>
		[C1Description("C1ToolTip.Animation")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if!EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Animation Animation
		{
			get
			{
				if (_animation == null)
				{
					_animation = new Animation();
				}
				return _animation;
			}
			set
			{
				_animation = value;
			}
		}

		/// <summary>
		/// Defines the animation to show the tooltip.
		/// </summary>
		[C1Description("C1ToolTip.ShowAnimation")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if!EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Animation ShowAnimation
		{
			get
			{
				if (_showAnimation == null)
				{
					_showAnimation = new Animation();
				}
				return _showAnimation;
			}
			set
			{
				_showAnimation = value;
			}
		}

		/// <summary>
		/// Defines the animation to hide the tooltip.
		/// </summary>
		[C1Description("C1ToolTip.HideAnimation")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if!EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Animation HideAnimation
		{
			get
			{
				if (_hideAnimation == null)
				{
					_hideAnimation = new Animation();
				}
				return _hideAnimation;
			}
			set
			{
				_hideAnimation = value;
			}
		}

		/// <summary>
		/// Determines the length of the delay before the tooltip appears.
		/// </summary>
		[C1Description("C1ToolTip.ShowDelay")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DefaultValue(200)]
#if!EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int ShowDelay
		{
			get
			{
				return GetPropertyValue("ShowDelay", 200);
			}
			set
			{
				SetPropertyValue("ShowDelay", value);
			}
		}

		/// <summary>
		/// Determines the length of the delay before the tooltip disappears.
		/// </summary>
		[C1Description("C1ToolTip.HideDelay")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue(200)]
#if!EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int HideDelay
		{
			get
			{
				return GetPropertyValue("HideDelay", 200);
			}
			set
			{
				SetPropertyValue("HideDelay", value);
			}
		}

		/// <summary>
		/// Defines the animation to move the tooltip position.
		/// </summary>
		[C1Description("C1ToolTip.CalloutAnimation")]
		[WidgetOption]
		[C1Category("Category.Behavior")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if!EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public SlideAnimation CalloutAnimation
		{
			get
			{
				if (_calloutAnimation == null)
				{
					_calloutAnimation = new SlideAnimation();
				}
				return _calloutAnimation;
			}
			set
			{
				_calloutAnimation = value;
			}
		}

		/// <summary>
		/// Determines the callout's class style. If true, then the callout triangle will be filled.
		/// </summary>
		[C1Description("C1ToolTip.CalloutFilled")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		[DefaultValue(false)]
#if!EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public bool CalloutFilled
		{
			get
			{
				return GetPropertyValue("CalloutFilled", false);
			}
			set
			{
				SetPropertyValue("CalloutFilled", value);
			}
		}

		/// <summary>
		/// Determines when show a tooltip, whether show a modal layout.
		/// </summary>
		[C1Description("C1ToolTip.Modal")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		[DefaultValue(false)]
#if!EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public bool Modal
		{
			get
			{
				return GetPropertyValue("Modal", false);
			}
			set
			{
				SetPropertyValue("Modal", value);
			}
		}

		/// <summary>
		/// If the group options sets the same as another tooltip widget, 
		/// the two widget share one tooltip DOM.
		/// </summary>
		[C1Description("C1ToolTip.Group")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		[DefaultValue("")]
#if!EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string Group
		{
			get
			{
				return GetPropertyValue("Group", "");
			}
			set
			{
				SetPropertyValue("Group", value);
			}
		}

		/// <summary>
		/// Defines the callback for get content by AJAX mode.
		/// </summary>
		[C1Description("C1ToolTip.AjaxCallback")]
		[C1Category("Category.Behavior")]
		[WidgetEvent]
		[DefaultValue("")]
		[WidgetOptionName("ajaxCallback")]
#if!EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string AjaxCallback
		{
			get
			{
				return GetPropertyValue<String>("AjaxCallback", "");
			}
			set 
			{
				SetPropertyValue<string>("AjaxCallback", value);
			}
		}
		#endregion

		#region ** Client events
		/// <summary>
		/// Triggered before showing the tooltip.
		/// </summary>
		[C1Description("C1ToolTip.OnClientShowing")]
		[C1Category("Category.Events")]
		[WidgetEvent("e")]
		[DefaultValue("")]
		[WidgetOptionName("showing")]
#if!EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientShowing
		{
			get
			{
				return GetPropertyValue("OnClientShowing", "");
			}
			set
			{
				SetPropertyValue("OnClientShowing", value);
			}
		}

		/// <summary>
		/// Triggered once the tooltip has shown.
		/// </summary>
		[C1Description("C1ToolTip.OnClientShown")]
		[C1Category("Category.Events")]
		[WidgetEvent("e")]
		[DefaultValue("")]
		[WidgetOptionName("shown")]
#if!EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientShown
		{
			get
			{
				return GetPropertyValue("OnClientShown", "");
			}
			set
			{
				SetPropertyValue("OnClientShown", value);
			}
		}

		/// <summary>
		/// Triggered before hiding the tooltip. If 'data.cancel' is set to true, then the tooltip is no longer hidden.
		/// </summary>
		[C1Description("C1ToolTip.OnClientHiding")]
		[C1Category("Category.Events")]
		[WidgetEvent("e")]
		[DefaultValue("")]
		[WidgetOptionName("hiding")]
#if!EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientHiding
		{
			get
			{
				return GetPropertyValue("OnClientHiding", "");
			}
			set
			{
				SetPropertyValue("OnClientHiding", value);
			}
		}

		/// <summary>
		/// Triggered once the tooltip is hidden.
		/// </summary>
		[C1Description("C1ToolTip.OnClientHidden")]
		[C1Category("Category.Events")]
		[WidgetEvent("e")]
		[DefaultValue("")]
		[WidgetOptionName("hidden")]
#if!EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientHidden
		{
			get
			{
				return GetPropertyValue("OnClientHidden", "");
			}
			set
			{
				SetPropertyValue("OnClientHidden", value);
			}
		}
		#endregion

		#region ** methods for serialization
		/// <summary>
		/// Determines whether the Position property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Return true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializePosition()
		{
			return this.Position.My.Left != HPosition.Left
				|| this.Position.My.Top != VPosition.Bottom
				|| this.Position.At.Left != HPosition.Right
				|| this.Position.At.Top != VPosition.Top
				|| this.Position.Collision.Left != CollisionMode.Flip
				|| this.Position.Collision.Top != CollisionMode.Flip
				|| this.Position.Offset.Left != 0
				|| this.Position.Offset.Top != 0;
		}

		/// <summary>
		/// Determines whether the Animation property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Return true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeAnimation()
		{
			return this.Animation.Animated.Disabled != false
				|| this.Animation.Animated.Effect != "fade"
				|| this.Animation.Duration != 400
				|| this.Animation.Easing != Easing.Swing
				|| this.Animation.Option.Count != 0;
		}

		/// <summary>
		/// Determines whether the ShowAnimation property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Return true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeShowAnimation()
		{
			return this.ShowAnimation.Animated.Disabled != false
				|| this.ShowAnimation.Animated.Effect != "fade"
				|| this.ShowAnimation.Duration != 400
				|| this.ShowAnimation.Easing != Easing.Swing
				|| this.ShowAnimation.Option.Count != 0;
		}

		/// <summary>
		/// Determines whether the HideAnimation property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Return true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeHideAnimation()
		{
			return this.HideAnimation.Animated.Disabled != false
				|| this.HideAnimation.Animated.Effect != "fade"
				|| this.HideAnimation.Duration != 400
				|| this.HideAnimation.Easing != Easing.Swing
				|| this.HideAnimation.Option.Count != 0;
		}

		/// <summary>
		/// Determines whether the CalloutAnimation property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Return true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeCalloutAnimation()
		{
			return this.CalloutAnimation.Disabled != false
				|| this.CalloutAnimation.Duration != 400
				|| this.CalloutAnimation.Easing != Easing.Swing;
		}
		#endregion
	}
}