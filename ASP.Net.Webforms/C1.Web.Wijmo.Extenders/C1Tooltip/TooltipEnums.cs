﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Tooltip
#else
namespace C1.Web.Wijmo.Controls.C1ToolTip
#endif
{

	#region ** CloseBehavior enum
	/// <summary>
	/// Defines the behavior of close the tooltip.
	/// </summary>
	public enum CloseBehavior
	{
		/// <summary>
		/// If it is auto ,auto hides the tooltip
		/// </summary>
		Auto = 0,
		/// <summary>
		/// If it is none, not hides the tooltip
		/// </summary>
		None = 1,
		/// <summary>
		/// If it is sticky, show the close button and click the button to hide the tooltip.
		/// </summary>
		Sticky = 2
	}
	#endregion end of ** CloseBehavior enum

	#region ** Trigger enum
	/// <summary>
	/// Defines the trigger event for show tooltip.
	/// </summary>
	public enum Trigger
	{
		/// <summary>
		/// Mouseover the element, show the tooltip; mouseout it, hide the tooltip.
		/// </summary>
		Hover = 0,
		/// <summary>
		/// Click the element, show the tooltip.
		/// </summary>
		Click = 1,
		/// <summary>
		/// Focus the element, show the tooltip.
		/// </summary>
		Focus = 2,
		/// <summary>
		/// Right click the element, show the tooltip.
		/// </summary>
		RightClick = 4,
		/// <summary>
		/// Not show the tooltip, user only can call the show or showAt method to show the tooltip.
		/// </summary>
		Custom = 8
	}
	#endregion end of ** Trigger enum
}
