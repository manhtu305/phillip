﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Extenders.C1AppView
{
	/// <summary>
	/// Menu extender
	/// </summary>
	[TargetControlType(typeof(Panel))]
	[ToolboxBitmap(typeof(C1AppViewExtender), "C1AppView.png")]
	[LicenseProvider]
	public partial class C1AppViewExtender : WidgetExtenderControlBase
	{
		#region ** fields
		private bool _productLicensed = false;
		#endregion

		#region ** constructor
		public C1AppViewExtender()
		{
			VerifyLicense();

			this.LoadSettings = new LoadSettings();
		}

		internal virtual void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1AppViewExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}
		#endregion

		internal override bool IsWijWebExtender
		{
			get
			{
				return false;
			}
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}
		/// <summary>
		/// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered in the browser window.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.RenderLicenseWebComment(writer);
			base.Render(writer);
		}
	}
}
