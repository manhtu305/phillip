﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1AppView", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1AppView
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1AppView", "wijmo")]
namespace C1.Web.Wijmo.Controls.C1AppView
#endif
{
    [WidgetDependencies(
        typeof(WijAppView)
#if !EXTENDER
		,ResourcesConst.MOBILE_CONTROL
#endif
    )]
#if EXTENDER
	public partial class C1AppViewExtender
#else
    public partial class C1AppView
#endif
    {

        #region ** fields
        private LoadSettings _loadSettings;
        #endregion

        #region ** Options

        /// <summary>
        /// Additional Ajax loading options to consider when loading pages.
        /// </summary>
        [NotifyParentProperty(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        [C1Category("Category.Misc")]
        [WidgetOption()]
        [C1Description("C1AppView.LoadSettings")]
#if !EXTENDER
        [Layout(LayoutType.Misc)]
#endif
        public LoadSettings LoadSettings
        {
            get
            {
                if (_loadSettings == null) _loadSettings = new LoadSettings();

                return GetPropertyValue<LoadSettings>("LoadSettings", _loadSettings);
            }
            set
            {
                SetPropertyValue<LoadSettings>("LoadSettings", value);
            }
        }

        /// <summary>
        /// Defines the name of url parameter for appview.
        /// </summary>
        [C1Description("C1AppView.UrlParamName")]
        [C1Category("Category.Misc")]
        [WidgetOption]
        [DefaultValue("appviewpage")]
#if !EXTENDER
        [Layout(LayoutType.Misc)]
#endif
        public string UrlParamName
        {
            get
            {
                return GetPropertyValue<String>("UrlParamName", "appviewpage");
            }
            set
            {
                SetPropertyValue<String>("UrlParamName", value);
            }
        }

        #region ** client events

        /// <summary>
        /// Triggered when appview load a page succeeded.
        /// </summary>
        [C1Description("C1AppView.OnClientPageLoad")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("e")]
        [DefaultValue("")]
        [WidgetOptionName("pageload")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientPageLoad
        {
            get
            {
                return GetPropertyValue("OnClientPageLoad", "");
            }
            set
            {
                SetPropertyValue("OnClientPageLoad", value);
            }
        }

        /// <summary>
        /// Triggered when appview change a page succeeded.
        /// </summary>
        [C1Description("C1AppView.OnClientPageChange")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("e")]
        [DefaultValue("")]
        [WidgetOptionName("pagechange")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientPageChange
        {
            get
            {
                return GetPropertyValue("OnClientPageChange", "");
            }
            set
            {
                SetPropertyValue("OnClientPageChange", value);
            }
        }

        /// <summary>
        /// Triggered before loading page.
        /// </summary>
        [C1Description("C1AppView.OnClientPageBeforeLoad")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("e")]
        [DefaultValue("")]
        [WidgetOptionName("pagebeforeload")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientPageBeforeLoad
        {
            get
            {
                return GetPropertyValue("OnClientPageBeforeLoad", "");
            }
            set
            {
                SetPropertyValue("OnClientPageBeforeLoad", value);
            }
        }
        /// <summary>
        /// Triggered before changing page.
        /// </summary>
        [C1Description("C1AppView.OnClientPageBeforeChange")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("e, item")]
        [DefaultValue("")]
        [WidgetOptionName("pagebeforechange")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientPageBeforeChange
        {
            get
            {
                return GetPropertyValue("OnClientPageBeforeChange", "");
            }
            set
            {
                SetPropertyValue("OnClientPageBeforeChange", value);
            }
        }
        /// <summary>
        /// Triggered when appview load page failed.
        /// </summary>
        [C1Description("C1AppView.OnClientPageLoadFailed")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("e, item")]
        [DefaultValue("")]
        [WidgetOptionName("pageloadfailed")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientPageLoadFailed
        {
            get
            {
                return GetPropertyValue("OnClientPageLoadFailed", "");
            }
            set
            {
                SetPropertyValue("OnClientPageLoadFailed", value);
            }
        }
        /// <summary>
        /// Triggered when appview change page failed.
        /// </summary>
        [C1Description("C1AppView.OnClientPageChangeFailed")]
        [C1Category("Category.ClientSideEvents")]
        [WidgetEvent("e, item")]
        [DefaultValue("")]
        [WidgetOptionName("pagechangefailed")]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
        public string OnClientPageChangeFailed
        {
            get
            {
                return GetPropertyValue("OnClientPageChangeFailed", "");
            }
            set
            {
                SetPropertyValue("OnClientPageChangeFailed", value);
            }
        }
        #endregion

        #endregion

        #region ** methods for serialization
        /// <summary>
        /// Determine whether the LoadSettings property should be serialized to client side.
        /// </summary>
        /// <returns>
        /// Returns true if any subproperty is changed, otherwise return false.
        /// </returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeLoadSettings()
        {
            return this.LoadSettings.Type != RequestType.Get
#if ASP_NET4
				|| !string.IsNullOrWhiteSpace(this.LoadSettings.Data)
#else
 || !string.IsNullOrEmpty(this.LoadSettings.Data)
#endif
 || this.LoadSettings.ReloadPage
                || this.LoadSettings.ShowLoadMsg
                || this.LoadSettings.LoadMsgDelay != 50;
        }
        #endregion end of ** methods for serialization.
    }
}
