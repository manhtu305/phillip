﻿using System;
using System.ComponentModel;
using System.Web.UI;
using C1.Web.Wijmo;


#if EXTENDER 
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Pager", "wijmo")]

namespace C1.Web.Wijmo.Extenders.C1Pager
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Pager", "wijmo")]

namespace C1.Web.Wijmo.Controls.C1Pager
#endif
{
#if EXTENDER
	using C1.Web.Wijmo.Extenders.Localization;
#else
	using C1.Web.Wijmo.Controls.Localization;
#endif

	[WidgetDependencies(
        typeof(WijPager)
#if !EXTENDER
		, "extensions.c1pager.js",
		ResourcesConst.C1WRAPPER_PRO
#endif
		)]
#if EXTENDER
	public partial class C1PagerExtender: IPagerOptions
#else 
	public partial class C1Pager: IPagerOptions 
#endif
	{
		#region options
		/// <summary>
		/// The class of the first-page button.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_FIRSTPAGECLASS)]
		[C1Description("PagerSettings.FirstPageClass")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		public string FirstPageClass
		{
			get { return GetPropertyValue("FirstPagerClass", PagerConstants.DEF_FIRSTPAGECLASS); }
			set { SetPropertyValue("FirstPagerClass", value); }
		}

		/// <summary>
		/// The text to display for the first-page button.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_FIRSTPAGETEXT)]
		[C1Description("PagerSettings.FirstPageText")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		public string FirstPageText
		{
			get	{ return GetPropertyValue("FirstPageText", C1Localizer.GetString("PagerSettings.FirstPageTextValue", PagerConstants.DEF_FIRSTPAGETEXT)); }
			set { SetPropertyValue("FirstPageText", value); }
		}

		/// <summary>
		/// The class of the last-page button.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_LASTPAGECLASS)]
		[C1Description("PagerSettings.LastPageClass")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		public string LastPageClass
		{
			get { return GetPropertyValue("LastPageClass", PagerConstants.DEF_LASTPAGECLASS); }
			set { SetPropertyValue("LastPageClass", value); }
		}

		/// <summary>
		/// The text to display for the last-page button.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_LASTPAGETEXT)]
		[C1Description("PagerSettings.LastPageText")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		public string LastPageText
		{
			get
			{
				return GetPropertyValue("LastPageText",
					C1Localizer.GetString("PagerSettings.LastPageTextValue", PagerConstants.DEF_LASTPAGETEXT));
			}
			set { SetPropertyValue("LastPageText", value); }
		}

		/// <summary>
		/// Determines the pager mode.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_MODE)]
		[C1Description("PagerSettings.Mode")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		public PagerMode Mode
		{
			get { return GetPropertyValue("Mode", PagerConstants.DEF_MODE); }
			set { SetPropertyValue("Mode", value); }
		}

		/// <summary>
		/// The class of the next-page button.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_NEXTPAGECLASS)]
		[C1Description("PagerSettings.NextPageClass")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		public string NextPageClass
		{
			get { return GetPropertyValue("NextPageClass", PagerConstants.DEF_NEXTPAGECLASS); }
			set { SetPropertyValue("NextPageClass", value); }
		}

		/// <summary>
		/// The text to display for the next-page button.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_NEXTPAGETEXT)]
		[C1Description("PagerSettings.NextPageText")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		public string NextPageText
		{
			get
			{
				return GetPropertyValue("NextPageText",
					C1Localizer.GetString("PagerSettings.NextPageTextValue", PagerConstants.DEF_NEXTPAGETEXT));
			}
			set { SetPropertyValue("NextPageText", value); }
		}

		/// <summary>
		/// The number of page buttons to display in the pager.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_PAGEBUTTONCOUNT)]
		[C1Description("PagerSettings.PageButtonCount")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		public int PageButtonCount
		{
			get { return GetPropertyValue("PageButtonCount", PagerConstants.DEF_PAGEBUTTONCOUNT); }
			set
			{
				if (value < 1)
				{
					throw new ArgumentOutOfRangeException("value", "Must be greater than zero");
				}

				SetPropertyValue("PageButtonCount", value);
			}
		}

		/// <summary>
		/// The class of the previous-page button.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_PREVIOUSPAGECLASS)]
		[C1Description("PagerSettings.PreviousPageClass")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		public string PreviousPageClass
		{
			get { return GetPropertyValue("PreviousPageClass", PagerConstants.DEF_PREVIOUSPAGECLASS); }
			set { SetPropertyValue("PreviousPageClass", value); }
		}

		/// <summary>
		/// The text to display for the previous-page button.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_PREVIOUSPAGETEXT)]
		[C1Description("PagerSettings.PreviousPageText")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		public string PreviousPageText
		{
			get
			{
				return GetPropertyValue("PreviousPageText",
					C1Localizer.GetString("PagerSettings.PreviousPageTextValue", PagerConstants.DEF_PREVIOUSPAGETEXT));
			}
			set { SetPropertyValue("PreviousPageText", value); }
		}

		/// <summary>
		/// Total number of pages.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_PAGECOUNT)]
		[C1Description("C1Pager.PageCount")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		public int PageCount
		{
			get { return GetPropertyValue("PageCount", PagerConstants.DEF_PAGECOUNT); }
			set
			{
				if (value < 1)
				{
					throw new ArgumentOutOfRangeException("value", "Must be greater than zero");
				}

				SetPropertyValue("PageCount", value);
			}
		}

		/// <summary>
		/// The zero-based index of the current page.
		/// </summary>
		[DefaultValue(PagerConstants.DEF_PAGEINDEX)]
		[C1Description("C1Pager.PageIndex")]
		[C1Category("Category.Behavior")]
		[WidgetOption]
		public int PageIndex
		{
			get { return GetPropertyValue("PageIndex", PagerConstants.DEF_PAGEINDEX); }
			set
			{
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException("value", "Must be greater than or equal to zero");
				}

				SetPropertyValue("PageIndex", value);
			}
		}

		#endregion

		#region client-side events

		/// <summary>
		/// A function called when page index is changing. Cancellable.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		///		<item>
		///			<term>args</term>
		///			<description>
		///				The data with this event.
		///				<list type="bullet">
		///					<item>
		///						<term>newPageIndex</term>
		///						<description>New page index.</description>
		///					</item>
		///				</list>
		///			</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Pager.OnClientPageIndexChanging")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e, args")]
		public string OnClientPageIndexChanging
		{
			get { return GetPropertyValue("OnClientPageIndexChanging", ""); }
			set { SetPropertyValue("OnClientPageIndexChanging", value); }
		}

		/// <summary>
		/// A function called when the page index is changed.
		/// </summary>
		/// <remarks>
		/// Parameters:
		/// <list type="bullet">
		///		<item>
		///			<term>e</term>
		///			<description>jQuery.Event object.</description>
		///		</item>
		/// </list>
		/// </remarks>
		[DefaultValue("")]
		[C1Description("C1Pager.OnClientPageIndexChanged")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("e")]
		public string OnClientPageIndexChanged
		{
			get { return GetPropertyValue("OnClientPageIndexChanged", ""); }
			set { SetPropertyValue("OnClientPageIndexChanged", value); }
		}

		#endregion
	}
}