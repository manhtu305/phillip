﻿using System;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Pager
#else
namespace C1.Web.Wijmo.Controls.C1Pager
#endif
{
	/// <summary>
	/// Determines the pager mode.
	/// </summary>
	public enum PagerMode
	{
		/// <summary>
		/// A set of pagination controls consisting of Previous and Next buttons.
		/// </summary>
		NextPrevious = 0,

		/// <summary>
		/// A set of pagination controls consisting of Previous, Next, First and Last buttons.
		/// </summary>
		NextPreviousFirstLast = 1,

		/// <summary>
		/// A set of pagination controls consisting of numbered link buttons to access pages directly.
		/// </summary>
		Numeric = 2,

		/// <summary>
		/// A set of pagination controls consisting of numbered and First and Last link buttons.
		/// </summary>
		NumericFirstLast = 3
	}
}