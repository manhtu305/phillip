﻿using System;
using System.ComponentModel;


#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Pager
#else
namespace C1.Web.Wijmo.Controls.C1Pager
#endif
{
	[Browsable(false)]
	internal interface IPagerOptions
	{
		string FirstPageClass { get; set; }
		string FirstPageText { get; set; }
		string LastPageClass { get; set; }
		string LastPageText { get; set; }
		PagerMode Mode { get; set; }
		string NextPageClass { get; set; }
		string NextPageText { get; set; }
		int PageButtonCount { get; set; }
		string PreviousPageClass { get; set; }
		string PreviousPageText { get; set; }
		int PageCount { get; set; }
		int PageIndex { get; set; }
	}

	[Browsable(false)]
	internal static class PagerConstants
	{
		#region const

		public const string DEF_FIRSTPAGECLASS = "ui-icon-seek-first";
		public const string DEF_FIRSTPAGETEXT = "First";
		public const string DEF_LASTPAGECLASS = "ui-icon-seek-end";
		public const string DEF_LASTPAGETEXT = "Last";
		public const PagerMode DEF_MODE = PagerMode.Numeric;
		public const string DEF_NEXTPAGECLASS = "ui-icon-seek-next";
		public const string DEF_NEXTPAGETEXT = "Next";
		public const int DEF_PAGEBUTTONCOUNT = 10;
		public const string DEF_PREVIOUSPAGECLASS = "ui-icon-seek-prev";
		public const string DEF_PREVIOUSPAGETEXT = "Previous";
		public const int DEF_PAGECOUNT = 1;
		public const int DEF_PAGEINDEX = 0;

		#endregion
	}
}
