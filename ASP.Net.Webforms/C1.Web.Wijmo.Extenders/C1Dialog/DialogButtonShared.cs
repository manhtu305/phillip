﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using C1.Web.Wijmo;
using System.Web.UI;
using System.ComponentModel;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Dialog
#else
namespace C1.Web.Wijmo.Controls.C1Dialog
#endif
{
	/// <summary>
	/// Represents a WijDialogButton of the WijDialog.
	/// </summary>
	public partial class DialogButton : Settings
	{

		/// <summary>
		/// Initializes a new instance of the <see cref="DialogButton"/> class.
		/// </summary>
		/// <param name="name">Set a text on the button</param>
		/// <param name="function">Specify a function gets called when you click the button.</param>
		public DialogButton(string text, string function)
		{
			OnClientClick = function;
			Text = text;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DialogButton"/> class.
		/// </summary>
		public DialogButton()
			: this("", "")
		{

		}

		/// <summary>
		/// Set a text on the button
		/// </summary>
		[C1Description("C1Dialog.Button.Text")]
		[NotifyParentProperty(true)]
		[DefaultValue("")]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string Text
		{
			get
			{
				return this.GetPropertyValue<string>("Text", "");
			}
			set
			{
				this.SetPropertyValue<string>("Text", value);
			}
		}

		/// <summary>
		/// Specify a function gets called when you click the button.
		/// </summary>
		[C1Description("C1Dialog.Button.OnClientClick")]
		[NotifyParentProperty(true)]
		[DefaultValue("")]
		[WidgetOptionName("click")]
		[WidgetEvent]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientClick
		{
			get
			{
				return this.GetPropertyValue<string>("OnClientClick", "");
			}
			set
			{
				this.SetPropertyValue<string>("OnClientClick", value);
			}
		}
	}
}
