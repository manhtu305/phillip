﻿using System.ComponentModel;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using C1.Web.Wijmo;
using C1.Web.Wijmo.Extenders.Localization;
using System;

[assembly: TagPrefix("C1.Web.Wijmo.Extenders.Dialog", "Dialog")]
namespace C1.Web.Wijmo.Extenders.C1Dialog
{
	/// <summary>
	/// Dialog Extender.
	/// </summary>
	[TargetControlType(typeof(Panel))]
	[ToolboxBitmap(typeof(C1DialogExtender), "Dialog.png")]
	[LicenseProviderAttribute()]
    [ToolboxItem(true)]
    public partial class C1DialogExtender : WidgetExtenderControlBase
	{
		private bool _productLicensed = false;
		public C1DialogExtender()
		{
			VerifyLicense();
			this.CaptionButtons = new DialogCaptionButtons();
			this.CollapsingAnimation = new Animation();
			this.ExpandingAnimation = new Animation();
		}

		internal override bool IsWijMobileExtender
		{
			get
			{
				return false;
			}
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1DialogExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		/// <summary>
		/// The width of the dialog, in pixels.
		/// </summary>
		[C1Description("C1Dialog.Width")]
		[Category("Options")]
		[DefaultValue(300)]
		[WidgetOption]
		public int Width
		{
			get
			{
				return GetPropertyValue("Width", 300);
			}
			set
			{
				SetPropertyValue("Width", value);
			}
		}

		/// <summary>
		/// The height of the dialog, in pixels.
		/// </summary>
		[C1Description("C1Dialog.Height")]
		[Category("Options")]
		[DefaultValue(0)]
		[WidgetOption]
		public int Height
		{
			get
			{
				return GetPropertyValue("Height", 0);
			}
			set
			{
				SetPropertyValue("Height", value);
			}
		}
	}
}