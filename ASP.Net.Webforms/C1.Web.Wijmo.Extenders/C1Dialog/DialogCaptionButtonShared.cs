﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using C1.Web.Wijmo;
using System.ComponentModel;
using System.Web.UI;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Dialog
#else
namespace C1.Web.Wijmo.Controls.C1Dialog
#endif
{
	/// <summary>
	/// Represents a DialogCaptionButton of the WijDialog.
	/// </summary>
	public partial class DialogCaptionButton : Settings
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="DialogCaptionButton"/> class.
		/// </summary>
		public DialogCaptionButton()
		{
			this.Visible = true;
			this.OnClientClick = "";
			this.IconClassOff = "";
			this.IconClassOn = "";
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DialogCaptionButton"/> class.
		/// </summary>
		/// <param name="iconClassOn">An css class of the button</param>
		/// <param name="iconClassOff">An css class of the toggle icon of button</param>
		public DialogCaptionButton(string iconClassOn, string iconClassOff)
		{
			this.Visible = true;
			this.OnClientClick = "";
			this.IconClassOff = iconClassOff;
			this.IconClassOn = iconClassOn;
		}

		/// <summary>
        /// Set visibility of button
		/// </summary>
		[C1Description("C1Dialog.CaptionButton.Visible")]
		[NotifyParentProperty(true)]
		[DefaultValue(true)]
		[WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
		public bool Visible
		{
			get
			{
				return this.GetPropertyValue<bool>("Visible", true);
			}
			set
			{
				this.SetPropertyValue<bool>("Visible", value);
			}
		}

		/// <summary>
		/// Specify a function gets called when you click the button.
		/// </summary>
		[C1Description("C1Dialog.CaptionButton.OnClientClick")]
		[NotifyParentProperty(true)]
		[DefaultValue("")]
		[WidgetEvent]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
		public string OnClientClick
		{
			get
			{
				return this.GetPropertyValue<string>("OnClientClick", "");
			}
			set
			{
				this.SetPropertyValue<string>("OnClientClick", value);
			}
		}

		/// <summary>
		/// Set the icon of button
		/// </summary>
		[NotifyParentProperty(true)]
		[C1Description("C1Dialog.CaptionButton.IconClassOn")]
		[DefaultValue("")]
		[WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
		public string IconClassOn
		{
			get
			{
				return this.GetPropertyValue<string>("IconClassOn", "");
			}
			set
			{
				this.SetPropertyValue<string>("IconClassOn", value);
			}
		}

		/// <summary>
		/// Set the toggle icon of button
		/// </summary>
		[NotifyParentProperty(true)]
		[C1Description("C1Dialog.CaptionButton.IconClassOff")]
		[DefaultValue("")]
		[WidgetOption]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
		public string IconClassOff
		{
			get
			{
				return this.GetPropertyValue<string>("IconClassOff", "");
			}
			set
			{
				this.SetPropertyValue<string>("IconClassOff", value);
			}
		}
	}
}
