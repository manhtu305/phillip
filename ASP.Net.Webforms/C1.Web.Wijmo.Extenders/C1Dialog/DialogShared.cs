﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using C1.Web.Wijmo;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1Dialog", "wijmo")] 
namespace C1.Web.Wijmo.Extenders.C1Dialog
#else
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1Dialog", "wijmo")] 
namespace C1.Web.Wijmo.Controls.C1Dialog
#endif
{
	[WidgetDependencies(			
		typeof(WijDialog)
#if !EXTENDER
, "extensions.c1dialog.js",
ResourcesConst.C1WRAPPER_OPEN
#endif
)]
	#if EXTENDER 
	public partial class C1DialogExtender
	#else 
	public partial class C1Dialog
	#endif
	{
		private List<DialogButton> _buttons;
		private Animation _collapsingAnimation;
		private Animation _expandingAnimation;
		private DialogCaptionButtons _captionButtons;
		private LocalizationOption _localizationOption;

		#region ** options.

		/// <summary>
		/// A jQuery selector that indicate which element the dialog (and overlay, if modal) should be appended to. 
		/// </summary>
		[C1Description("C1Dialog.AppendTo")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string AppendTo 
		{
			get
			{
				return GetPropertyValue("AppendTo", "body");
			}
			set
			{
				SetPropertyValue("AppendTo", value);
			}
		}

		/// <summary>
		/// When autoOpen is true the dialog will open automatically when dialog is called. 
		/// </summary>
		[C1Description("C1Dialog.ShowOnLoad")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOptionName("AutoOpen")]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool ShowOnLoad
		{
			get
			{
				return GetPropertyValue("ShowOnLoad", true);
			}
			set
			{
				SetPropertyValue("ShowOnLoad", value);
			}
		}

		/// <summary>
		/// If set to true, the dialog will be draggable by the titlebar.
		/// </summary>
		[C1Description("C1Dialog.Draggable")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Draggable
		{
			get
			{
				return GetPropertyValue("Draggable", true);
			}
			set
			{
				SetPropertyValue("Draggable", value);
			}
		}

		/// <summary>
		/// A URL string specifies the URL for the iframe element inside wijdialog.
		/// </summary>
		[C1Description("C1Dialog.ContentUrl")]
		[C1Category("Category.Behavior")]
		[DefaultValue("")]
		[UrlProperty()]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string ContentUrl
		{
			get
			{
				return GetPropertyValue("ContentUrl", string.Empty);
			}
			set
			{
				SetPropertyValue("ContentUrl", value);
			}
		}

		/// <summary>
		/// The Title of the Dialog.
		/// </summary>
		[C1Description("C1Dialog.Title")]
		[C1Category("Category.Behavior")]
		[DefaultValue("")]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string Title
		{
			get
			{
				return GetPropertyValue("Title", string.Empty);
			}
			set
			{
				SetPropertyValue("Title", value);
			}
		}

		/// <summary>
		/// If set to true, the dialog will have modal behavior; 
		/// other items on the page will be disabled (i.e. cannot be interacted with).
		/// Modal dialogs create an overlay below the dialog but above other page elements.
		/// </summary>
		[C1Description("C1Dialog.Modal")]
		[C1Category("Category.Behavior")]
		[DefaultValue(false)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Modal
		{
			get
			{
				return GetPropertyValue("Modal", false);
			}
			set
			{
				SetPropertyValue("Modal", value);
			}
		}

		/// <summary>
		/// If set to true, the dialog will be resizable.
		/// </summary>
		[C1Description("C1Dialog.Resizable")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Resizable
		{
			get
			{
				return GetPropertyValue("Resizable", true);
			}
			set
			{
				SetPropertyValue("Resizable", value);
			}
		}


		/// <summary>
		/// Specifies whether the dialog will stack on top of other dialogs. 
		/// This will cause the dialog to move to the front of other dialogs when it gains focus.
		/// </summary>
		[C1Description("C1Dialog.Stack")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool Stack
		{
			get
			{
				return GetPropertyValue("Stack", true);
			}
			set
			{
				SetPropertyValue("Stack", value);
			}
		}

		/// <summary>
		/// The starting z-index for the dialog.
		/// </summary>
		[C1Description("C1Dialog.ZIndex")]
		[C1Category("Category.Behavior")]
		[DefaultValue(1000)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int ZIndex
		{
			get
			{
				return GetPropertyValue("ZIndex", 1000);
			}
			set
			{
				SetPropertyValue("ZIndex", value);
			}
		}

		/// <summary>
		/// The maximum height to which the dialog can be resized, in pixels.
		/// </summary>
		[C1Description("C1Dialog.MaxHeight")]
		[C1Category("Category.Behavior")]
		[DefaultValue(0)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int MaxHeight
		{
			get
			{
				return GetPropertyValue("MaxHeight", 0);
			}
			set
			{
				SetPropertyValue("MaxHeight", value);
			}
		}

		/// <summary>
		/// The maximum width to which the dialog can be resized, in pixels.
		/// </summary>
		[C1Description("C1Dialog.MaxWidth")]
		[C1Category("Category.Behavior")]
		[DefaultValue(0)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int MaxWidth
		{
			get
			{
				return GetPropertyValue("MaxWidth", 0);
			}
			set
			{
				SetPropertyValue("MaxWidth", value);
			}
		}

		/// <summary>
		/// The minimum height to which the dialog can be resized, in pixels.
		/// </summary>
		[C1Description("C1Dialog.MinHeight")]
		[C1Category("Category.Behavior")]
		[DefaultValue(150)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int MinHeight
		{
			get
			{
				return GetPropertyValue("MinHeight", 150);
			}
			set
			{
				SetPropertyValue("MinHeight", value);
			}
		}

		/// <summary>
		/// The minimum width to which the dialog can be resized, in pixels.
		/// </summary>
		[C1Description("C1Dialog.MinWidth")]
		[C1Category("Category.Behavior")]
		[DefaultValue(150)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int MinWidth
		{
			get
			{
				return GetPropertyValue("MinWidth", 150);
			}
			set
			{
				SetPropertyValue("MinWidth", value);
			}
		}

		/// <summary>
		/// Specifies whether the dialog should close 
		/// when it has focus and the user presses the escape (ESC) key.
		/// </summary>
		[C1Description("C1Dialog.CloseOnEscape")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool CloseOnEscape
		{
			get
			{
				return GetPropertyValue("CloseOnEscape", true);
			}
			set
			{
				SetPropertyValue("CloseOnEscape", value);
			}
		}

		/// <summary>
		/// Specifies the text for the close button. 
		/// Note that the close text is visibly hidden when using a standard theme.
		/// </summary>
		[C1Description("C1Dialog.CloseText")]
		[C1Category("Category.Behavior")]
		[DefaultValue("")]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string CloseText
		{
			get
			{
				return GetPropertyValue("CloseText", string.Empty);
			}
			set
			{
				SetPropertyValue("CloseText", value);
			}
		}

		/// <summary>
		/// The effect to be used when the dialog is opened.
		/// </summary>
		[C1Description("C1Dialog.Show")]
		[C1Category("Category.Behavior")]
		[DefaultValue("")]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string Show
		{
			get
			{
				return GetPropertyValue("Show", "blind");
			}
			set
			{
				SetPropertyValue("Show", value);
			}
		}

		/// <summary>
		/// Specifies where the dialog should be displayed. Possible values: 
		/// 1) a single string representing position within viewport: 'center', 'left', 'right', 'top', 'bottom'. 
		/// 2) a string with array format containing an x,y coordinate pair in pixel offset from left, top corner of viewport (e.g. "[350,100]") 
		/// 3) a string with array format containing x,y position string values (e.g. "['right','top']" for top right corner).
		/// </summary>
		[C1Description("C1Dialog.Position")]
		[C1Category("Category.Appearance")]
		[WidgetOption]
		[DefaultValue("center")]
#if!EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public string Position
		{
			get
			{
				return GetPropertyValue("Position", "center");
			}
			set
			{
				SetPropertyValue("Position", value);
			}
		}

		/// <summary>
		/// The effect to be used when the dialog is closed.
		/// </summary>
		[C1Description("C1Dialog.Hide")]
		[C1Category("Category.Behavior")]
		[DefaultValue("blind")]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string Hide
		{
			get
			{
				return GetPropertyValue("Hide", "blind");
			}
			set
			{
				SetPropertyValue("Hide", value);
			}
		}

		/// <summary>
		/// A value determines the settings of the animation effect to be used when the wijdialog is collapsed.
		/// </summary>
		[C1Description("C1Dialog.CollapsingAnimation")]
		[C1Category("Category.Behavior")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Animation CollapsingAnimation
		{
			get
			{
				if (this._collapsingAnimation == null)
				{
					this._collapsingAnimation = new Animation();
				}
				return this._collapsingAnimation;
			}
			set
			{
				this._collapsingAnimation = value;
			}
		}

		/// <summary>
		/// A value determines the settings of the animation effect to be used when the wijdialog is expanded.
		/// </summary>
		[C1Description("C1Dialog.ExpandingAnimation")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Animation ExpandingAnimation
		{
			get
			{
				if (this._expandingAnimation == null)
				{
					this._expandingAnimation = new Animation();
				}
				return this._expandingAnimation;
			}
			set
			{
				this._expandingAnimation = value;
			}
		}

		/// <summary>
		/// Specifies visibility, click event, and icon for the caption buttons on the dialog. 
		/// </summary>
		[C1Description("C1Dialog.CaptionButtons")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[C1Category("Category.Behavior")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public DialogCaptionButtons CaptionButtons
		{
			get
			{
				if (this._captionButtons == null)
				{
					this._captionButtons = new DialogCaptionButtons();
				}
				return this._captionButtons;
			}
			set
			{
				this._captionButtons = value;
			}
		}

		/// <summary>
		/// Gets or sets which buttons should be displayed on the dialog.
		/// </summary>
		[C1Description("C1Dialog.Buttons")]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[C1Category("Category.Behavior")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
		[CollectionItemType(typeof(DialogButton))]
#endif
		public List<DialogButton> Buttons
		{
			get
			{
				if (_buttons == null)
				{
					_buttons = new List<DialogButton>() { };
				}
				return _buttons;
			}
		}

		/// <summary>
		/// Use the Localization property in order to customize localization strings.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[WidgetOption]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public LocalizationOption Localization
		{
			get { return _localizationOption ?? (_localizationOption = new LocalizationOption()); }
		}

		#endregion end of ** options.

		#region ** client events
		/// <summary>
		/// The name of the function which will be called when the dialog is closed.
		/// </summary>
		[C1Description("C1Dialog.OnClientClose")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, index")]
		[WidgetOptionName("close")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientClose
		{
			get
			{
				return GetPropertyValue("OnClientClose", "");
			}
			set
			{
				SetPropertyValue("OnClientClose", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called when the dialog is created.
		/// </summary>
		[C1Description("C1Dialog.OnClientCreate")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, index")]
		[WidgetOptionName("create")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientCreate
		{
			get
			{
				return GetPropertyValue("OnClientCreate", "");
			}
			set
			{
				SetPropertyValue("OnClientCreate", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called before close.
		/// </summary>
		[C1Description("C1Dialog.OnClientBeforeClose")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, index")]
		[WidgetOptionName("beforeClose")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientBeforeClose
		{
			get
			{
				return GetPropertyValue("OnClientBeforeClose", "");
			}
			set
			{
				SetPropertyValue("OnClientBeforeClose", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called when dialog is opened.
		/// </summary>
		[C1Description("C1Dialog.OnClientOpen")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, index")]
		[WidgetOptionName("open")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientOpen
		{
			get
			{
				return GetPropertyValue("OnClientOpen", "");
			}
			set
			{
				SetPropertyValue("OnClientOpen", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called when dialog is focused.
		/// </summary>
		[C1Description("C1Dialog.OnClientFocus")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, index")]
		[WidgetOptionName("focus")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientFocus
		{
			get
			{
				return GetPropertyValue("OnClientFocus", "");
			}
			set
			{
				SetPropertyValue("OnClientFocus", value);
			}
		}


		/// <summary>
		/// The name of the function which will be called when dialog begin to be dragged.
		/// </summary>
		[C1Description("C1Dialog.OnClientDragStart")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, index")]
		[WidgetOptionName("dragStart")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientDragStart
		{
			get
			{
				return GetPropertyValue("OnClientDragStart", "");
			}
			set
			{
				SetPropertyValue("OnClientDragStart", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called when dialog is dragged.
		/// </summary>
		[C1Description("C1Dialog.OnClientDrag")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, index")]
		[WidgetOptionName("drag")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientDrag
		{
			get
			{
				return GetPropertyValue("OnClientDrag", "");
			}
			set
			{
				SetPropertyValue("OnClientDrag", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called after the dialog has been dragged.
		/// </summary>
		[C1Description("C1Dialog.OnClientDragStop")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, index")]
		[WidgetOptionName("dragStop")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientDragStop
		{
			get
			{
				return GetPropertyValue("OnClientDragStop", "");
			}
			set
			{
				SetPropertyValue("OnClientDragStop", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called when the dialog begin to be resized.
		/// </summary>
		[C1Description("C1Dialog.OnClientResizeStart")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, index")]
		[WidgetOptionName("resizeStart")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientResizeStart
		{
			get
			{
				return GetPropertyValue("OnClientResizeStart", "");
			}
			set
			{
				SetPropertyValue("OnClientResizeStart", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called when the dialog is resized.
		/// </summary>
		[C1Description("C1Dialog.OnClientResize")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, index")]
		[WidgetOptionName("resize")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientResize
		{
			get
			{
				return GetPropertyValue("OnClientResize", "");
			}
			set
			{
				SetPropertyValue("OnClientResize", value);
			}
		}

		/// <summary>
		/// The name of the function which will be called after the dialog has been resized.
		/// </summary>
		[C1Description("C1Dialog.OnClientResizeStop")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("ev, index")]
		[WidgetOptionName("resizeStop")]
		[DefaultValue("")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public string OnClientResizeStop
		{
			get
			{
				return GetPropertyValue("OnClientResizeStop", "");
			}
			set
			{
				SetPropertyValue("OnClientResizeStop", value);
			}
		}

		#endregion end of ** client events.

		#region ** methods for serialization

		/// <summary>
		/// Determine whether the ExpandingAnimation property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeExpandingAnimation()
		{
			return this.ExpandingAnimation.Animated.Disabled != false
				|| this.ExpandingAnimation.Animated.Effect != "slide"
				|| this.ExpandingAnimation.Duration != 400
				|| this.ExpandingAnimation.Option.Count != 0
				|| this.ExpandingAnimation.Easing != Easing.Swing;
		}

		/// <summary>
		/// Determine whether the CollapsingAnimation property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeCollapsingAnimation()
		{
			return this.CollapsingAnimation.Animated.Disabled != false
				|| this.CollapsingAnimation.Animated.Effect != "slide"
				|| this.CollapsingAnimation.Duration != 400
				|| this.CollapsingAnimation.Option.Count != 0
				|| this.CollapsingAnimation.Easing != Easing.Swing;
		}

		///// <summary>
		///// Determine whether the CaptionButtons property should be serialized to client side.
		///// </summary>
		///// <returns>
		///// Returns true if any subproperty is changed, otherwise return false.
		///// </returns>
		//[EditorBrowsable(EditorBrowsableState.Never)]
		//public bool ShouldSerializeCaptionButtons()
		//{
		//    return this.CaptionButtons.ShouldSerializePin()
		//        || this.CaptionButtons.ShouldSerializeClose()
		//        || this.CaptionButtons.ShouldSerializeMaximize()
		//        || this.CaptionButtons.ShouldSerializeMinimize()
		//        || this.CaptionButtons.ShouldSerializeRefresh()
		//        || this.CaptionButtons.ShouldSerializeToggle();
		//}

		/// <summary>
		/// Determine whether the Buttons property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns true if any subproperty is changed, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeButtons()
		{
			return this.Buttons.Count > 0;
		}

		#endregion end of ** methods for serialization.

		#region ** localization

		/// <summary>
		/// The localization of caption button titles
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public class LocalizationOption : Settings
		{
			/// <summary>
			/// Pin string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string Pin
			{
				get { return GetPropertyValue("Pin", C1Localizer.GetString("C1Dialog.Pin", "Pin")); }
				set { SetPropertyValue("Pin", value); }
			}

			/// <summary>
			/// Refresh string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string Refresh
			{
				get { return GetPropertyValue("Refresh", C1Localizer.GetString("C1Dialog.Refresh", "Refresh")); }
				set { SetPropertyValue("Refresh", value); }
			}

			/// <summary>
			/// Toggle string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string Toggle
			{
				get { return GetPropertyValue("Toggle", C1Localizer.GetString("C1Dialog.Toggle", "Toggle")); }
				set { SetPropertyValue("Toggle", value); }
			}

			/// <summary>
			/// Minimize string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string Minimize
			{
				get { return GetPropertyValue("Minimize", C1Localizer.GetString("C1Dialog.Minimize", "Minimize")); }
				set { SetPropertyValue("Minimize", value); }
			}

			/// <summary>
			/// Maximize string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string Maximize
			{
				get { return GetPropertyValue("Maximize", C1Localizer.GetString("C1Dialog.Maximize", "Maximize")); }
				set { SetPropertyValue("Maximize", value); }
			}

			/// <summary>
			/// Close string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string Close
			{
				get { return GetPropertyValue("Close", C1Localizer.GetString("C1Dialog.Close", "Close")); }
				set { SetPropertyValue("Close", value); }
			}
		}

		#endregion
	}
}
