﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using C1.Web.Wijmo;
using System.Web.UI;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1Dialog
#else
namespace C1.Web.Wijmo.Controls.C1Dialog
#endif
{
	/// <summary>
	/// Represents all DialogCaptionButtons of the WijDialog 
	/// (include: Pin, Maximize, Minimize, Refresh, Toggle, Close).
	/// </summary>
	public partial class DialogCaptionButtons : Settings, IJsonEmptiable
	{
		private const string CAPTION_BUTTONS = "DialogCaptionButton";
		/// <summary>
		/// Set pin button on the title of dialog
		/// </summary>
		[WidgetOption]
		[C1Description("C1Dialog.CaptionButton.PinButton")]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
		public DialogCaptionButton Pin
		{
			get
			{
				return this.GetPropertyValue<DialogCaptionButton>(string.Format("{0}_{1}", CAPTION_BUTTONS, "Pin"),
					new DialogCaptionButton());
			}
			set
			{
				this.SetPropertyValue<DialogCaptionButton>(string.Format("{0}_{1}", CAPTION_BUTTONS, "Pin"), value);
			}
		}
		/// <summary>
		/// Set refresh button on the title of dialog
		/// </summary>
		[WidgetOption]
		[C1Description("C1Dialog.CaptionButton.Refresh")]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
		public DialogCaptionButton Refresh
		{
			get
			{
				return this.GetPropertyValue<DialogCaptionButton>(string.Format("{0}_{1}", CAPTION_BUTTONS, "Refresh"),
					new DialogCaptionButton());
			}
			set
			{
				this.SetPropertyValue<DialogCaptionButton>(string.Format("{0}_{1}", CAPTION_BUTTONS, "Refresh"), value);
			}
		}

		/// <summary>
		/// Set toggle button on the title of dialog
		/// </summary>
		[WidgetOption]
		[C1Description("C1Dialog.CaptionButton.Toggle")]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
		public DialogCaptionButton Toggle
		{
			get
			{
				return this.GetPropertyValue<DialogCaptionButton>(string.Format("{0}_{1}", CAPTION_BUTTONS, "Toggle"),
					new DialogCaptionButton());
			}
			set
			{
				this.SetPropertyValue<DialogCaptionButton>(string.Format("{0}_{1}", CAPTION_BUTTONS, "Toggle"), value);
			}
		}


		/// <summary>
		/// Set minimize button on the title of dialog
		/// </summary>
		[WidgetOption]
		[C1Description("C1Dialog.CaptionButton.Minimize")]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
		public DialogCaptionButton Minimize
		{
			get
			{
				return this.GetPropertyValue<DialogCaptionButton>(string.Format("{0}_{1}", CAPTION_BUTTONS, "Minimize"),
					new DialogCaptionButton());
			}
			set
			{
				this.SetPropertyValue<DialogCaptionButton>(string.Format("{0}_{1}", CAPTION_BUTTONS, "Minimize"), value);
			}
		}

		/// <summary>
		/// Set maximize button on the title of dialog
		/// </summary>
		[WidgetOption]
		[C1Description("C1Dialog.CaptionButton.Maximize")]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
		public DialogCaptionButton Maximize
		{
			get
			{
				return this.GetPropertyValue<DialogCaptionButton>(string.Format("{0}_{1}", CAPTION_BUTTONS, "Maximize"),
					new DialogCaptionButton());
			}
			set
			{
				this.SetPropertyValue<DialogCaptionButton>(string.Format("{0}_{1}", CAPTION_BUTTONS, "Maximize"), value);
			}
		}

		/// <summary>
		/// Set close button on the title of dialog
		/// </summary>
		[WidgetOption]
		[C1Description("C1Dialog.CaptionButton.Close")]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
#if !EXTENDER
        [Layout(LayoutType.Behavior)]
#endif
		public DialogCaptionButton Close
		{
			get
			{
				return this.GetPropertyValue<DialogCaptionButton>(string.Format("{0}_{1}", CAPTION_BUTTONS, "Close"),
					new DialogCaptionButton());
			}
			set
			{
				this.SetPropertyValue<DialogCaptionButton>(string.Format("{0}_{1}", CAPTION_BUTTONS, "Close"), value);
			}
		}

		#region ** methods for serialization
		private bool ShouldSerializePin()
		{
			return ShouldSerializeCaptionButton(this.Pin);
		}

		private bool ShouldSerializeMaximize()
		{
			return ShouldSerializeCaptionButton(this.Maximize);
		}

		private bool ShouldSerializeMinimize()
		{
			return ShouldSerializeCaptionButton(this.Minimize);
		}

		private bool ShouldSerializeRefresh()
		{
			return ShouldSerializeCaptionButton(this.Refresh);
		}

		private bool ShouldSerializeToggle()
		{
			return ShouldSerializeCaptionButton(this.Toggle);
		}

		private bool ShouldSerializeClose()
		{
			return ShouldSerializeCaptionButton(this.Close);
		}

		private bool ShouldSerializeCaptionButton(DialogCaptionButton button)
		{
			return button.IconClassOff != string.Empty
				|| button.IconClassOn != string.Empty
				|| button.Visible != true
				|| button.OnClientClick != string.Empty;
		}

		private bool ShouldSerializeCaptionButtons()
		{
			return this.ShouldSerializePin()
				|| this.ShouldSerializeClose()
				|| this.ShouldSerializeMaximize()
				|| this.ShouldSerializeMinimize()
				|| this.ShouldSerializeRefresh()
				|| this.ShouldSerializeToggle();
		}

		#region IJsonEmptiable Members

		bool IJsonEmptiable.IsEmpty
		{
			get { return !ShouldSerializeCaptionButtons(); }
		}

		#endregion

		#endregion end of ** methods for serialization.
	}
}
