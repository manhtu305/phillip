﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using C1.Web.Wijmo;
using System.Web.UI;

namespace C1.Web.Wijmo.Extenders.C1Dialog
{
	/// <summary>
	/// Represents all DialogCaptionButtons of the WijDialog 
	/// (include: Pin, Maximize, Minimize, Refresh, Toggle, Close).
	/// </summary>
	public partial class DialogCaptionButtons
	{
		public DialogCaptionButtons()
		{
			this.Pin = new DialogCaptionButton();
			this.Maximize = new DialogCaptionButton();
			this.Minimize = new DialogCaptionButton();
			this.Refresh = new DialogCaptionButton();
			this.Toggle = new DialogCaptionButton();
			this.Close = new DialogCaptionButton();
		}		
	}
}
