﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Linq;
using System.Text;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls
#endif
{
	/// <summary>
	/// Represents an ordered pair of float x- and y-coordinates that defines a point in a two-dimensional plane.
	/// </summary>
	[Serializable]
	[TypeConverter(typeof(PointFConverter))]
	public struct PointF : ICustomOptionType
	{
		/// <summary>
		/// Represents a <see cref="PointF"/> that has <see cref="X"/> and <see cref="Y"/> values set to zero.
		/// </summary>
		public static readonly PointF Empty = new PointF();

		private float _x;
		private float _y;

		/// <summary>
		/// Initializes a new instance of the PointF class with the specified coordinates.
		/// </summary>
		/// <param name="x">The horizontal position of the point.</param>
		/// <param name="y">The vertical position of the point.</param>
		public PointF(float x, float y)
		{
			_x = x;
			_y = y;
		}

		/// <summary>
		/// Gets or sets the x-coordinate of this point.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("PointF.X")]
		[DefaultValue(0f)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public float X
		{
			get { return _x; }
			set { _x = value; }
		}

		/// <summary>
		/// Gets or sets the y-coordinate of this point.
		/// </summary>
		[C1Category("Category.Data")]
		[C1Description("PointF.Y")]
		[DefaultValue(0f)]
		[NotifyParentProperty(true)]
		[RefreshProperties(RefreshProperties.All)]
		public float Y
		{
			get { return _y; }
			set { _y = value; }
		}

		/// <summary>
		/// Compares two PointF objects. 
		/// The result specifies whether the values of the <see cref="X"/> and 
		/// <seealso cref="Y"/> properties of the two PointF objects are equal.
		/// </summary>
		/// <param name="left">A <see cref="PointF"/> to compare.</param>
		/// <param name="right">A <see cref="PointF"/> to compare.</param>
		/// <returns><c>true</c> if the <see cref="X"/> and <see cref="Y"/> values of the 
		/// <paramref name="left"/> and <paramref name="right"/> are equal; 
		/// otherwise, <c>false</c>.</returns>
		public static bool operator ==(PointF left, PointF right)
		{
			return left.Equals(right);
		}

		/// <summary>
		/// Compares two PointF objects.
		/// The result specifies whether the values of the <seealso cref="X"/> and
		/// <see cref="Y"/> properties of the two PointF objects are unequal.
		/// </summary>
		/// <param name="left">A <see cref="PointF"/> to compare.</param>
		/// <param name="right">A <see cref="PointF"/> to compare.</param>
		/// <returns><c>true</c> if the <see cref="X"/> and <see cref="Y"/> values of the 
		/// <paramref name="left"/> and <paramref name="right"/> differ; 
		/// otherwise, <c>false</c>.</returns>
		public static bool operator !=(PointF left, PointF right)
		{
			return !(left.Equals(right));
		}

		/// <summary>
		/// Specifies whether this <see cref="PointF"/> contains the same coordinates as the specified <see cref="PointF"/>.
		/// </summary>
		/// <param name="obj">The <see cref="Object"/> to test.</param>
		/// <returns>
		/// <c>true</c> if <paramref name="obj"/> is a <see cref="PointF"/> 
		/// and has the same coordinates as this <see cref="PointF"/>.
		/// </returns>
		public override bool Equals(object obj)
		{
			if (!(obj is PointF)) return false;

			var that = (PointF)obj;
			return _x == that._x
				   && _y == that._y;
		}

		/// <summary>
		/// Returns a hash code for this <see cref="PointF"/>.
		/// </summary>
		/// <returns>
		/// An integer value that specifies a hash value for this <see cref="PointF"/>.
		/// </returns>
		public override int GetHashCode()
		{
			return _x.GetHashCode() ^ _y.GetHashCode();
		}


		#region ICustomOptionType

		string ICustomOptionType.SerializeValue()
		{
			return string.Format("{{\"x\":{0}, \"y\":{1}}}", X, Y);
		}

		#if !EXTENDER
		void ICustomOptionType.DeSerializeValue(object state)
		{
			var ht = state as Hashtable;
			if (ht != null)
			{
				if (ht["x"] != null)
				{
					_x = Convert.ToSingle(ht["x"]);
				}

				if (ht["y"] != null)
				{
					_y = Convert.ToSingle(ht["y"]);
				}
			}
		}
		#endif

		bool ICustomOptionType.IncludeQuotes
		{
			get { return false; }
		}

		#endregion
	}
	internal class PointFConverter : TypeConverter
	{
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType == typeof(InstanceDescriptor) || base.CanConvertTo(context, destinationType);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (!(value is string))
			{
				return base.ConvertFrom(context, culture, value);
			}
			string text = ((string)value).Trim();
			if (text.Length == 0)
			{
				return null;
			}
			if (culture == null)
			{
				culture = CultureInfo.CurrentCulture;
			}

			char ch = culture.TextInfo.ListSeparator[0];
			var textArray = text.Split(new[] { ch });
			var numArray = new float[textArray.Length];
			if (numArray.Length != 2)
			{
				throw new ArgumentException(string.Format(C1Localizer.GetString("PointFConverter.TextParseFailedFormat"), text, "x, y"));
			}
			var converter = TypeDescriptor.GetConverter(typeof(float));
			for (var i = 0; i < numArray.Length; i++)
			{
				numArray[i] = (float)converter.ConvertFromString(context, culture, textArray[i]);
			}
			return new PointF(numArray[0], numArray[1]);
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == null)
			{
				throw new ArgumentNullException("destinationType");
			}

			if ((destinationType == typeof(string)) && (value is PointF))
			{
				var PointF = (PointF)value;
				if (culture == null)
				{
					culture = CultureInfo.CurrentCulture;
				}
				string separator = culture.TextInfo.ListSeparator[0] + " ";
				TypeConverter converter = TypeDescriptor.GetConverter(typeof(float));
				var textArray = new string[2];
				var num = 0;
				textArray[num++] = converter.ConvertToString(context, culture, PointF.X);
				textArray[num] = converter.ConvertToString(context, culture, PointF.Y);
				return string.Join(separator, textArray);
			}

			if ((destinationType == typeof(InstanceDescriptor)) && (value is PointF))
			{
				var PointF2 = (PointF)value;
				var member = typeof(PointF).GetConstructor(new[] { typeof(float), typeof(float) });
				if (member != null)
				{
					return new InstanceDescriptor(member, new object[] { PointF2.X, PointF2.Y });
				}
			}

			return base.ConvertTo(context, culture, value, destinationType);

		}

		public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
		{
			return TypeDescriptor.GetProperties(typeof(PointF), attributes).Sort(new[] { "X", "Y" });
		}

		public override bool GetPropertiesSupported(ITypeDescriptorContext context)
		{
			return true;
		}
	}
}
