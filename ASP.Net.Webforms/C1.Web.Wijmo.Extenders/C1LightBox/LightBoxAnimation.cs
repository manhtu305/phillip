﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif



#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1LightBox
#else
namespace C1.Web.Wijmo.Controls.C1LightBox
#endif
{
	public class TransitionAnimation : Settings
	{
		/// <summary>
		/// Determines the effects to animate.
		/// </summary>
		[C1Description("Animation.Animated")]
		[NotifyParentProperty(true)]
		[DefaultValue(TransitionMode.Fade)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public TransitionMode Animated
		{
			get
			{
				return base.GetPropertyValue<TransitionMode>("Animated", TransitionMode.Fade);
			}
			set
			{
				SetPropertyValue<TransitionMode>("Animated", value);
			}
		}

		/// <summary>
		/// Defines the duration to animate
		/// </summary>
		[DefaultValue(400)]
		[C1Description("Animation.Duration")]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int Duration
		{
			get
			{
				return base.GetPropertyValue<int>("Duration", 400);
			}
			set
			{
				SetPropertyValue<int>("Duration", value);
			}
		}

		/// <summary>
		/// Defines the easing to animate
		/// </summary>
		[DefaultValue(Easing.EaseInQuad)]
		[C1Description("Animation.Easing")]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public Easing Easing
		{
			get
			{
				return GetPropertyValue<Easing>("Easing", Easing.EaseInQuad);
			}
			set
			{
				SetPropertyValue<Easing>("Easing", value);
			}
		}
	}



	public class ResizeAnimation : Settings
	{

		/// <summary>
        /// Determines the effects to animate.
		/// </summary>
		[C1Description("Animation.Animated")]
		[NotifyParentProperty(true)]
		[DefaultValue(ResizeMode.Sync)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public ResizeMode Animated
		{
			get
			{
				return base.GetPropertyValue<ResizeMode>("Animated", ResizeMode.Sync);
			}
			set
			{
				SetPropertyValue<ResizeMode>("Animated", value);
			}
		}


		/// <summary>
		/// Defines the duration to animate
		/// </summary>
		[DefaultValue(400)]
		[C1Description("Animation.Duration")]
		[NotifyParentProperty(true)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public int Duration
		{
			get
			{
				return base.GetPropertyValue<int>("Duration", 400);
			}
			set
			{
				SetPropertyValue<int>("Duration", value);
			}
		}

	}




}
