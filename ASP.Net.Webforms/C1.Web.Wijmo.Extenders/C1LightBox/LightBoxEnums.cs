﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders.C1LightBox
#else
namespace C1.Web.Wijmo.Controls.C1LightBox
#endif
{


    /// <summary>
    /// Determines the position of text description.
    /// </summary>
    public enum TextPosition
    {
        /// <summary>
        /// Text description is hidden.
        /// </summary>
        None = 0,

        /// <summary>
        /// Displays the text description inside the content.
        /// </summary>
        Inside = 1,

        /// <summary>
        /// Displays the text description outside the content.
        /// </summary>
        Outside = 2,

        /// <summary>
        /// Displays the text description inside the content with overlay style.
        /// </summary>
        Overlay = 3,

        /// <summary>
        /// Displays the text description inside the content with title overlay style.
        /// </summary>
        TitleOverlay = 4
    }

    /// <summary>
    /// Determines the position of the nav-buttons.
    /// </summary>
    public enum ButtonPosition
    {

        /// <summary>
        /// Inside of content.
        /// </summary>
        Inside = 0,
        /// <summary>
        /// Outside of content.
        /// </summary>
        Outside = 1
    }

    /// <summary>
    /// Determines the visibility of control buttons.
    /// </summary>
    [Flags]
    public enum CtrlButtons
    {
        /// <summary>
        /// No control buttons are displayed.
        /// </summary>
        None = 0x0,

        /// <summary>
        /// Displays the Play button.
        /// </summary>
        Play = 0x1,

        /// <summary>
        /// Displays the Stop button.
        /// </summary>
        Stop = 0x2
    }

    /// <summary>
    /// Determines the visibility of dialog buttons.
    /// </summary>
    [Flags]
    public enum DialogButtons
    {
        /// <summary>
        /// No dialog buttons are displayed.
        /// </summary>
        None = 0x0,

        /// <summary>
        /// Displays the Close button.
        /// </summary>
        Close = 0x1,

        /// <summary>
        /// Displays the FullSize button.
        /// </summary>
        FullSize = 0x2
    }

    /// <summary>
    /// Determines the type counter style.
    /// </summary>
    public enum CounterType
    {
        /// <summary>
        /// Default counter style that uses the CounterFormat string.
        /// </summary>
        Default = 0,

        /// <summary>
        /// Sequent digits counter style.
        /// </summary>
        Sequence = 1
    }
    
    /// <summary>
    /// Determines the animation style when switching between pages. 
    /// </summary>
    public enum TransitionMode
    {
        /// <summary>
        /// No animation.
        /// </summary>
        None = 0,

        /// <summary>
        /// Slide animation.
        /// </summary>
        Slide = 1,

        /// <summary>
        /// Fade animation.
        /// </summary>
        Fade = 2
    }

    /// <summary>
    /// Determines the animation style when resizing. 
    /// </summary>
    public enum ResizeMode
    {
        /// <summary>
        /// No animation.
        /// </summary>
        None = 0,

        /// <summary>
        /// Resize the width in advance of height.
        /// </summary>
        Wh = 1,

        /// <summary>
        /// Resize the height in advance of width.
        /// </summary>
        Hw = 2,

        /// <summary>
        /// Resize both the width and height at the same time.
        /// </summary>
        Sync = 3
    }

    /// <summary>
    /// Determines the name of player to host the content.
    /// </summary>
    public enum PlayerName
    {
        /// <summary>
        /// Automatically detect the player by URL.
        /// </summary>
        AutoDetect = 0,

        /// <summary>
        /// Content is an inline section.
        /// </summary>
        Inline = 1,

        /// <summary>
        /// Content will be hosted inside an IFrame.
        /// </summary>
        Iframe = 2,

        /// <summary>
        /// Content will be hosted in an Img tag.
        /// </summary>
        Img = 3,

        /// <summary>
        /// Content will be hosted in flash object.
        /// </summary>
        Swf = 4,

        /// <summary>
        /// Content is an flash video and will be hosted in flash player.
        /// </summary>
        Flv = 5,

        /// <summary>
        /// Content will be hosted inside windows media player object.
        /// </summary>
        Wmp = 6,

        /// <summary>
        /// Content will be hosted inside quick time object.
        /// </summary>
        Qt = 7,

        /// <summary>
        /// Content will be played by the Wijmo HTML5 Video Player.
        /// </summary>
        Wijvideo = 8
    }

}
