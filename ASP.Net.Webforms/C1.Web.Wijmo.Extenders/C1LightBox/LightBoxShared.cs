﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;
using System.Drawing.Design;
using C1.Web.Wijmo;


#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1LightBox", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1LightBox
#else
using C1.Web.Wijmo.Controls.Localization;
[assembly: TagPrefix("C1.Web.Wijmo.Controls.C1LightBox", "wijmo")] 
namespace C1.Web.Wijmo.Controls.C1LightBox
#endif
{
	[WidgetDependencies(
        typeof(WijLightBox)
#if !EXTENDER
, "extensions.c1lightbox.js",
ResourcesConst.C1WRAPPER_PRO
#endif
)]
#if EXTENDER
	public partial class C1LightBoxExtender
#else
	public partial class C1LightBox
#endif
	{
		#region Fields

		private PositionSettings _positionSettings;
		private TransitionAnimation _transAnimation;
		private ResizeAnimation _resizeAnimation;
		private SlideAnimation _textShowAnimation;
		private SlideAnimation _textHideAnimation;

		#endregion

		#region Options


		/// <summary>
		/// Determines the position of text description.
		/// </summary>
        [C1Description("C1LightBox.TextPosition", "Determines the position of text description.")]
        [C1Category("Category.Appearance")]
		[DefaultValue(TextPosition.Overlay)]
		[WidgetOption]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public TextPosition TextPosition
		{
			get
			{
				return GetPropertyValue("TextPosition", TextPosition.Overlay);
			}
			set
			{
				SetPropertyValue("TextPosition", value);
			}
		}



		/// <summary>
		/// A value that indicates the maximum width of the content.
		/// </summary>
        [C1Description("C1LightBox.MaxWidth", "A value that indicates the maxinum width of the content.")]
        [C1Category("Category.Appearance")]
        [NotifyParentProperty(true)]
		[WidgetOption]
		[WidgetOptionName("width")]
		[DefaultValue(600)]
#if !EXTENDER
		[Layout(LayoutType.Sizes)]
#endif
		public int MaxWidth
		{
			get
			{
				return this.GetPropertyValue<int>("MaxWidth", 600);
			}
			set
			{
				this.SetPropertyValue<int>("MaxWidth", value);
			}
		}

		/// <summary>
		/// A value that indicates the maximum height of the content.
		/// </summary>
        [C1Description("C1LightBox.MaxHeight", "A value that indicates the maxinum height of the content.")]
        [C1Category("Category.Appearance")]
        [NotifyParentProperty(true)]
		[WidgetOption]
		[WidgetOptionName("height")]
		[DefaultValue(400)]
#if !EXTENDER
		[Layout(LayoutType.Sizes)]
#endif
		public int MaxHeight
		{
			get
			{
				return this.GetPropertyValue<int>("MaxHeight", 400);
			}
			set
			{
				this.SetPropertyValue<int>("MaxHeight", value);
			}
		}

		/// <summary>
		/// A value determines whether to auto-size to keep the original width/height ratio of content.
		/// </summary>
        [C1Description("C1LightBox.AutoSize", "A value determines whether to auto-size to keep the original width/height ratio of content.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
		[WidgetOption]
		public bool AutoSize
		{
			get
			{
				return this.GetPropertyValue("AutoSize", true);
			}
			set
			{
				this.SetPropertyValue("AutoSize", value);
			}
		}

		/// <summary>
		/// Determines the root url for each item.
		/// </summary>
        [C1Description("C1LightBox.RootUrl", "Determines the root url for each item.")]
        [C1Category("Category.Behavior")]
        [DefaultValue("")]
		[WidgetOption]
		public string RootUrl
		{
			get
			{
				return this.GetPropertyValue("RootUrl", "");
			}
			set
			{
				this.SetPropertyValue("RootUrl", value);
			}
		}

		/// <summary>
		/// Determines the visibility of the control buttons.
		/// </summary>
#if ASP_NET4
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1FlagsEnumUITypeEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1FlagsEnumUITypeEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#else
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1FlagsEnumUITypeEditor, C1.Web.Wijmo.Controls.Design.2", typeof(UITypeEditor))]
#endif
        [C1Description("C1LightBox.CtrlButtons", "Determines the visibility of the control buttons.")]
        [C1Category("Category.Appearance")]
        [DefaultValue(CtrlButtons.None)]
		[WidgetOption]
		public CtrlButtons CtrlButtons
		{
			get
			{
				return this.GetPropertyValue("CtrlButtons", CtrlButtons.None);
			}
			set
			{
				this.SetPropertyValue("CtrlButtons", value);
			}
		}

		/// <summary>
		/// Determines the visibility of the dialog buttons.
		/// </summary>
#if ASP_NET4
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1FlagsEnumUITypeEditor, C1.Web.Wijmo.Controls.Design.4", typeof(UITypeEditor))]
#elif ASP_NET35
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1FlagsEnumUITypeEditor, C1.Web.Wijmo.Controls.Design.3", typeof(UITypeEditor))]
#else
		[Editor("C1.Web.Wijmo.Controls.Design.UITypeEditors.C1FlagsEnumUITypeEditor, C1.Web.Wijmo.Controls.Design.2", typeof(UITypeEditor))]
#endif
        [C1Description("C1LightBox.DialogButtons", "Determines the visibility of the dialog buttons.")]
        [C1Category("Category.Appearance")]
        [DefaultValue(DialogButtons.Close)]
		[WidgetOption]
		public DialogButtons DialogButtons
		{
			get
			{
				return this.GetPropertyValue("DialogButtons", DialogButtons.Close);
			}
			set
			{
				this.SetPropertyValue("DialogButtons", value);
			}
		}

		/// <summary>
		/// Determines the type counter style.
		/// </summary>
        [C1Description("C1LightBox.CounterType", "Determines the type counter style.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(CounterType.Default)]
		[WidgetOption]
		public CounterType CounterType
		{
			get
			{
				return this.GetPropertyValue("CounterType", CounterType.Default);
			}
			set
			{
				this.SetPropertyValue("CounterType", value);
			}
		}

		/// <summary>
		/// Determines the text format of counter.
		/// </summary>
        [C1Description("C1LightBox.CounterFormat", "Determines the text format of counter.")]
        [C1Category("Category.Behavior")]
        [DefaultValue("[i] of [n]")]
		[WidgetOption]
		public string CounterFormat
		{
			get
			{
                return this.GetPropertyValue("CounterFormat",
                    C1Localizer.GetString("C1Multimedia.CounterDefaultValue", "[i] of [n]"));
                    //JP "[i] / [n]" ,others "[i] of [n]");
                    //return this.GetPropertyValue("CounterFormat", "[i] of [n]");
			}
			set
			{
				this.SetPropertyValue("CounterFormat", value);
			}
		}

		/// <summary>
		/// Determines the maximum number of digit buttons in sequence counter type.
		/// </summary>
        [C1Description("C1LightBox.CounterLimit", "Determines the maxinum number of digit buttons in sequence counter type.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(10)]
		[WidgetOption]
		public int CounterLimit
		{
			get
			{
				return this.GetPropertyValue("CounterLimit", 10);
			}
			set
			{
				if (value <= 0) return;

				this.SetPropertyValue("CounterLimit", value);
			}
		}


		/// <summary>
		/// Determines whether to display the counter.
		/// </summary>
        [C1Description("C1LightBox.ShowCounter", "Determines whether to display the counter.")]
        [C1Category("Category.Appearance")]
        [DefaultValue(true)]
		[WidgetOption]
		public bool ShowCounter
		{
			get
			{
				return this.GetPropertyValue("ShowCounter", true);
			}
			set
			{
				this.SetPropertyValue("ShowCounter", value);
			}
		}

		/// <summary>
		/// Determines whether to display the navigation buttons.
		/// </summary>
        [C1Description("C1LightBox.ShowNavButtons", "Determines whether to display the navigation buttons.")]
        [C1Category("Category.Appearance")]
        [DefaultValue(true)]
		[WidgetOption]
		public bool ShowNavButtons
		{
			get
			{
				return this.GetPropertyValue("ShowNavButtons", true);
			}
			set
			{
				this.SetPropertyValue("ShowNavButtons", value);
			}
		}

		/// <summary>
		/// Determines whether to display the time bar.
		/// </summary>
        [C1Description("C1LightBox.ShowTimer", "Determines whether to display the time bar.")]
        [C1Category("Category.Appearance")]
        [DefaultValue(false)]
		[WidgetOption]
		public bool ShowTimer
		{
			get
			{
				return this.GetPropertyValue("ShowTimer", false);
			}
			set
			{
				this.SetPropertyValue("ShowTimer", value);
			}
		}

		/// <summary>
		/// Determines the position of control buttons.
		/// </summary>
        [C1Description("C1LightBox.ControlsPosition", "Determines the position of control buttons.")]
        [C1Category("Category.Appearance")]
        [DefaultValue(ButtonPosition.Inside)]
		[WidgetOption]
		public ButtonPosition ControlsPosition
		{
			get
			{
				return this.GetPropertyValue("ControlsPosition", ButtonPosition.Inside);
			}
			set
			{
				this.SetPropertyValue("ControlsPosition", value);
			}
		}


		/// <summary>
		/// Determines whether to display the control buttons only when hovering the mouse to the content.
		/// </summary>
        [C1Description("C1LightBox.ShowControlsOnHover", "Determines whether to display the control buttons only when hovering the mouse to the content.")]
        [C1Category("Category.Appearance")]
        [DefaultValue(true)]
		[WidgetOption]
		public bool ShowControlsOnHover
		{
			get
			{
				return this.GetPropertyValue("ShowControlsOnHover", true);
			}
			set
			{
				this.SetPropertyValue("ShowControlsOnHover", value);
			}
		}

		/// <summary>
		/// Determines whether to pause the auto-play when clicking the content.
		/// </summary>
        [C1Description("C1LightBox.ClickPause", "Determines whether to pause the auto-play when clicking the content.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
		[WidgetOption]
		public bool ClickPause
		{
			get
			{
				return this.GetPropertyValue("ClickPause", false);
			}
			set
			{
				this.SetPropertyValue("ClickPause", value);
			}
		}

		/// <summary>
		/// Determines whether to allow keyboard navigation.
		/// </summary>
        [C1Description("C1LightBox.KeyNav", "Determines whether to allow keyboard navigation.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
		[WidgetOption]
		public bool KeyNav
		{
			get
			{
				return this.GetPropertyValue("KeyNav", false);
			}
			set
			{
				this.SetPropertyValue("KeyNav", value);
			}
		}


		/// <summary>
		/// Determines whether the popped up content is displayed in modal style container.
		/// </summary>
        [C1Description("C1LightBox.Modal", "Determines whether the popped up content is displayed in modal style container.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(false)]
		[WidgetOption]
		public bool Modal
		{
			get
			{
				return this.GetPropertyValue("Modal", false);
			}
			set
			{
				this.SetPropertyValue("Modal", value);
			}
		}

		/// <summary>
		/// Determines the popup position of content window.
		/// </summary>
        [C1Description("C1LightBox.Position", "Determines the popup position of content window.")]
        [C1Category("Category.Appearance")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public PositionSettings Position
		{
			get
			{
				if (_positionSettings == null)
				{
					_positionSettings = new PositionSettings();
				}
				return _positionSettings;
			}
			set
			{
				_positionSettings = value;
			}
		}

		/// <summary>
		/// Determines whether to close the popup window when user presses the ESC key.
		/// </summary>
        [C1Description("C1LightBox.CloseOnEscape", "Determines whether to close the popup window when user presses the ESC key.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
		[WidgetOption]
		public bool CloseOnEscape
		{
			get
			{
				return this.GetPropertyValue("CloseOnEscape", true);
			}
			set
			{
				this.SetPropertyValue("CloseOnEscape", value);
			}
		}

		/// <summary>
		/// Determines whether to close the popup window when user clicks on the out side of content.
		/// </summary>
        [C1Description("C1LightBox.CloseOnOuterClick", "Determines whether to close the popup window when user clicks on the out side of content.")]
        [C1Category("Category.Behavior")]
        [DefaultValue(true)]
		[WidgetOption]
		public bool CloseOnOuterClick
		{
			get
			{
				return this.GetPropertyValue("CloseOnOuterClick", true);
			}
			set
			{
				this.SetPropertyValue("CloseOnOuterClick", value);
			}
		}

		/// <summary>
		/// Determines whether pages are automatically displayed in order.
		/// </summary>
        [C1Description("C1LightBox.AutoPlay", "Determines whether pages are automatically displayed in order.")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
		[DefaultValue(false)]
		public bool AutoPlay
		{
			get
			{
				return GetPropertyValue<bool>("AutoPlay", false);
			}
			set
			{
				SetPropertyValue<bool>("AutoPlay", value);
			}
		}

		/// <summary>
		/// Determines the time span in milliseconds between pages in autoplay mode. 
		/// </summary>
        [C1Description("C1LightBox.Delay", "Determines the time span in milliseconds between pages in autoplay mode. ")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
		[DefaultValue(2000)]
		public int Delay
		{
			get
			{
				return GetPropertyValue<int>("Delay", 2000);
			}
			set
			{
				SetPropertyValue<int>("Delay", value);
			}
		}

		/// <summary>
		/// Determines whether start from the first page when reaching the end in autoplay mode.
		/// </summary>
        [C1Description("C1LightBox.Loop", "Determines whether start from the first page when reaching the end in autoplay mode.")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
		[DefaultValue(true)]
		public bool Loop
		{
			get
			{
				return GetPropertyValue<bool>("Loop", true);
			}
			set
			{
				SetPropertyValue<bool>("Loop", value);
			}
		}

		/// <summary>
		/// Determines whether to turn on the movie controls in movie player.
		/// </summary>
        [C1Description("C1LightBox.ShowMovieControls", "Determines whether to turn on the movie controls in movie player.")]
        [C1Category("Category.Appearance")]
        [WidgetOption]
		[DefaultValue(true)]
		public bool ShowMovieControls
		{
			get
			{
				return GetPropertyValue<bool>("ShowMovieControls", true);
			}
			set
			{
				SetPropertyValue<bool>("ShowMovieControls", value);
			}
		}

		/// <summary>
		/// Determines whether to turn on the autoplay option in movie player.
		/// </summary>
        [C1Description("C1LightBox.AutoPlayMovies", "Determines whether to turn on the autoplay option in movie player.")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
		[DefaultValue(true)]
		public bool AutoPlayMovies
		{
			get
			{
				return GetPropertyValue<bool>("AutoPlayMovies", true);
			}
			set
			{
				SetPropertyValue<bool>("AutoPlayMovies", value);
			}
		}

		/// <summary>
		/// Gets or sets the relative path and name of the flash video player.
		/// </summary>
        [C1Description("C1LightBox.FlvPlayer", "Gets or sets the relative path and name of the flash video player.")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
		[DefaultValue("player\\player.swf")]
		public string FlvPlayer
		{
			get
			{
				return GetPropertyValue<string>("FlvPlayer", "player\\player.swf");
			}
			set
			{
				SetPropertyValue<string>("FlvPlayer", value);
			}
		}

		/// <summary>
		/// Gets or sets the relative path and name of the flash installation guide.
		/// </summary>
        [C1Description("C1LightBox.FlashInstall", "Gets or sets the relative path and name of the flash installation guide.")]
        [C1Category("Category.Behavior")]
        [WidgetOption]
		[DefaultValue("player\\expressInstall.swf")]
		public string FlashInstall
		{
			get
			{
				return GetPropertyValue<string>("FlashInstall", "player\\expressInstall.swf");
			}
			set
			{
				SetPropertyValue<string>("FlashInstall", value);
			}
		}

		/// <summary>
		/// Determines the animation style when switching between pages. 
		/// </summary>
        [C1Description("C1LightBox.TransAnimation", "Determines the animation style when switching between pages.")]
        [C1Category("Category.Behavior")]
        [NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption()]
		public TransitionAnimation TransAnimation
		{
			get
			{
				if (_transAnimation == null)
					_transAnimation = new TransitionAnimation();

				return _transAnimation;
			}
		}

		/// <summary>
		/// Determines the animation style when resizing. 
		/// </summary>
        [C1Description("C1LightBox.ResizeAnimation", "Determines the animation style when resizing.")]
        [C1Category("Category.Behavior")]
        [NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption()]
		public ResizeAnimation ResizeAnimation
		{
			get
			{
				if (_resizeAnimation == null)
					_resizeAnimation = new ResizeAnimation();

				return _resizeAnimation;
			}
		}

		/// <summary>
		/// Determines the animation style when showing out the text.
		/// </summary>
        [C1Description("C1LightBox.TextShowOption", "Determines the animation style when showing out the text.")]
        [C1Category("Category.Behavior")]
        [NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption()]
		public SlideAnimation TextShowOption
		{
			get
			{
				if (_textShowAnimation == null)
					_textShowAnimation = new SlideAnimation();

				return _textShowAnimation;
			}
		}

		/// <summary>
		/// Determines the animation style when hiding the text.
		/// </summary>
        [C1Description("C1LightBox.TextHideOption", "Determines the animation style when hidding the text.")]
        [C1Category("Category.Behavior")]
        [NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption()]
		public SlideAnimation TextHideOption
		{
			get
			{
				if (_textHideAnimation == null)
					_textHideAnimation = new SlideAnimation();

				return _textHideAnimation;
			}
		}

		/// <summary>
		/// Determines the slide direction.
		/// </summary>
		[WidgetOption]
		[DefaultValue(Orientation.Horizontal)]
        [C1Category("Category.Appearance")]
        [C1Description("C1LightBox.SlideDirection", "Determines the slide direction.")]
		public Orientation SlideDirection
		{
			get
			{
				return GetPropertyValue("SlideDirection", Orientation.Horizontal);
			}
			set
			{
				SetPropertyValue("SlideDirection", value);
			}
		}


		/// <summary>
		/// Determines the name of player to host the content.
		/// </summary>
		[WidgetOption]
		[DefaultValue(PlayerName.AutoDetect)]
        [C1Category("Category.Behavior")]
        [C1Description("C1LightBox.Player", "Determines the name of player to host the content.")]
		public PlayerName Player
		{
			get
			{
				return GetPropertyValue("Player", PlayerName.AutoDetect);
			}
			set
			{
				SetPropertyValue("Player", value);
			}
		}

		#endregion

		#region client events

		/// <summary>
		/// Occurs before a page's content is shown.
		/// </summary>
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1LightBox.OnClientBeforeShow")]
		[DefaultValue("")]
		[WidgetOption]
		[WidgetOptionName("beforeShow")]
		[WidgetEvent]
		public string OnClientBeforeShow
		{
			get
			{
				return GetPropertyValue("OnClientBeforeShow", "");
			}
			set
			{
				SetPropertyValue("OnClientBeforeShow", value);
			}
		}

		/// <summary>
		/// Occurs after a page's content is shown.
		/// </summary>
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1LightBox.OnClientShow")]
		[DefaultValue("")]
		[WidgetOption]
		[WidgetOptionName("show")]
		[WidgetEvent]
		public string OnClientShow
		{
			get
			{
				return GetPropertyValue("OnClientShow", "");
			}
			set
			{
				SetPropertyValue("OnClientShow", value);
			}
		}

		/// <summary>
		/// Occurs after the popped up container is opened.
		/// </summary>
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1LightBox.OnClientOpen")]
		[DefaultValue("")]
		[WidgetOption]
		[WidgetOptionName("open")]
		[WidgetEvent]
		public string OnClientOpen
		{
			get
			{
				return GetPropertyValue("OnClientOpen", "");
			}
			set
			{
				SetPropertyValue("OnClientOpen", value);
			}
		}

		/// <summary>
		/// Occurs before the popped up container is closed.
		/// </summary>
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1LightBox.OnClientBeforeClose")]
		[DefaultValue("")]
		[WidgetOption]
		[WidgetOptionName("beforeClose")]
		[WidgetEvent]
		public string OnClientBeforeClose
		{
			get
			{
				return GetPropertyValue("OnClientBeforeClose", "");
			}
			set
			{
				SetPropertyValue("OnClientBeforeClose", value);
			}
		}

		/// <summary>
		/// Occurs after the popped up container is closed.
		/// </summary>
		[C1Category("Category.ClientSideEvents")]
		[C1Description("C1LightBox.OnClientClose")]
		[DefaultValue("")]
		[WidgetOption]
		[WidgetOptionName("close")]
		[WidgetEvent]
		public string OnClientClose
		{
			get
			{
				return GetPropertyValue("OnClientClose", "");
			}
			set
			{
				SetPropertyValue("OnClientClose", value);
			}
		}

		#endregion

	}
}
