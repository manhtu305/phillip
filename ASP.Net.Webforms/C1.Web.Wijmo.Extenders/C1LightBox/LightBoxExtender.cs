﻿using System.ComponentModel;
using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using C1.Web.Wijmo;


[assembly: TagPrefix("C1.Web.Wijmo.Extenders.C1LightBox", "wijmo")]
namespace C1.Web.Wijmo.Extenders.C1LightBox
{
	/// <summary>
	/// LightBox Extender.
	/// </summary>
	[TargetControlType(typeof(Panel))]
	[ToolboxBitmap(typeof(C1LightBoxExtender), "C1LightBox.png")]
	[LicenseProviderAttribute()]
    [ToolboxItem(true)]
    public partial class C1LightBoxExtender : WidgetExtenderControlBase
	{
		private bool _productLicensed = false;
		public C1LightBoxExtender()
		{
			VerifyLicense();
		}

		internal override bool IsWijMobileExtender
		{
			get
			{
				return false;
			}
		}

		private void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1LightBoxExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		/// <summary>
		/// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered in the browser window.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.RenderLicenseWebComment(writer);
			base.Render(writer);
		}

        #region ** properties
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override bool Enabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                base.Enabled = value;
            }
        }
        #endregion
	}
}
