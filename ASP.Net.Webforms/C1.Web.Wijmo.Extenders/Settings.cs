﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
namespace C1.Web.Wijmo.Extenders
{
	/// <summary>
	/// The settings for widget's options.
	/// </summary>
	public class Settings : IC1Cloneable
#else
namespace C1.Web.Wijmo.Controls
{
	/// <summary>
	/// The settings for widget's options.
	/// </summary>
	public class Settings : IC1Cloneable, IJsonRestore
#endif
	{
		private Hashtable _props;
		/// <summary>
		/// Gets a hashtable to store property info.
		/// </summary>
		protected Hashtable Props
		{
			get 
			{
				if (_props == null) _props = new Hashtable();
				return _props;
			}
		}

		/// <summary>
		/// Gets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="nullValue">The null value.</param>
		/// <returns></returns>
		protected V GetPropertyValue<V>(string propertyName, V nullValue)
		{
			if (this.Props[propertyName] == null)
			{
				return nullValue;
			}
			return (V)this.Props[propertyName];
		}

		/// <summary>
		/// Sets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="value">The value.</param>
		protected void SetPropertyValue<V>(string propertyName, V value)
		{
			this.Props[propertyName] = value;
		}

		/// <summary>
		/// Tests the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="testValue">The test value.</param>
		/// <returns></returns>
		protected bool TestPropertyValue<V>(string propertyName, V testValue)
		{
			object value;
			return (_props != null && ((value = _props[propertyName]) != null) && value.Equals(testValue));
		}

		#region IC1Cloneable Members

		/// <summary>
		/// Creates a new instance of the <see cref="Settings"/> class.
		/// </summary>
		/// <returns></returns>
		protected internal virtual Settings CreateInstance()
		{
			return new Settings();
		}

		IC1Cloneable IC1Cloneable.CreateInstance()
		{
			return CreateInstance();
		}

		/// <summary>
		/// Copies the properties of the specified <see cref="Settings"/> into the instance of the <see cref="Settings"/> class that this method is called from.
		/// </summary>
		/// <param name="copyFrom">A <see cref="Settings"/> that represents the properties to copy.</param>
		protected internal virtual void CopyFrom(Settings copyFrom)
		{
			Settings settings = copyFrom as Settings;
			if (settings != null && settings._props != null)
			{
				IDictionaryEnumerator ien = settings.Props.GetEnumerator();
				while (ien.MoveNext())
				{
					Props[ien.Key] = ien.Value;
				}
			}
		}

		void IC1Cloneable.CopyFrom(object copyFrom)
		{
			Settings settings = copyFrom as Settings;
			if (settings != null)
			{
				CopyFrom(settings);
			}
		}

		#endregion

		#region ICloneable Members

		/// <summary>
		/// Returns a copy of this instance of the <see cref="Settings"/> object.
		/// </summary>
		/// <returns>A copy of this instance of <see cref="Settings"/>.</returns>
		protected internal Settings Clone()
		{
			Settings settings = CreateInstance();
			settings.CopyFrom(this);
			return settings;
		}

		object ICloneable.Clone()
		{
			return Clone();
		}

		#endregion

#if !EXTENDER
		#region ** IJsonRestore interfact implementations
		void IJsonRestore.RestoreStateFromJson(object state)
		{
			this.RestoreStateFromJson(state);
		}

		/// <summary>
		/// Restore the states from the JSON object.
		/// </summary>
		/// <param name="state">The state which is parsed from the JSON string.</param>
		protected virtual void RestoreStateFromJson(object state)
		{
			JsonRestoreHelper.RestoreStateFromJson(this, state);
		}
		#endregion end of ** IJsonRestore interfact implementations.
#endif
	}
}
