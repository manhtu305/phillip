﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	[WidgetDependencies(
        typeof(WijScatterChart)
#if !EXTENDER
, "extensions.c1scatterchart.js",
ResourcesConst.C1WRAPPER_PRO
#endif
)]
#if EXTENDER
	public partial class C1ScatterChartExtender : C1ChartCoreExtender<ScatterChartSeries, ChartAnimation>
#else
	public partial class C1ScatterChart : C1ChartCore<ScatterChartSeries, ChartAnimation, C1ChartBinding>
#endif
	{

		private void InitChart()
		{
			this.Legend.Size.Width = 30;
			this.Legend.Size.Height = 3;
			this.Animation.Duration = 2000;
			this.SeriesTransition = new ChartAnimation();
			this.SeriesTransition.Duration = 2000;
			//this.SeriesTransition.Easing = ">";
			this.SeriesTransition.Easing = ChartEasing.EaseInCubic;

			//TODO: remove this after fix scatter's performance issue.
			this.Animation.Enabled = false;
			this.SeriesTransition.Enabled = false;
			//end comments
		}

		// added in 2013-10-31, fixing the OADateTime issue, move this method from C1ScatterChart to this file.
		/// <summary>
		/// Gets the series data of the specified series.
		/// </summary>
		/// <param name="series">
		/// The specified series.
		/// </param>
		/// <returns>
		/// Returns the series data which retrieved from the specified series.
		/// </returns>
		protected override ChartSeriesData GetSeriesData(ScatterChartSeries series)
		{
            if (series.IsTrendline)
                return series.TrendlineSeries.Data;

			return series.Data;
		}

		#region ** hide chart label properties
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override bool ShowChartLabels
		{
			get
			{
				return base.ShowChartLabels;
			}
			set
			{
				base.ShowChartLabels = value;
			}
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override ChartStyle ChartLabelStyle
		{
			get
			{
				return base.ChartLabelStyle;
			}
			set
			{
				base.ChartLabelStyle = value;
			}
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override string ChartLabelFormatString
		{
			get
			{
				return base.ChartLabelFormatString;
			}
			set
			{
				base.ChartLabelFormatString = value;
			}
		}
		#endregion

		/// <summary>
		/// A value that indicates whether to show animation and the duration and effect for the animation when reload data.
		/// </summary>
		[C1Description("C1Chart.ScatterChart.SeriesTransition")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public ChartAnimation SeriesTransition { get; set; }

		/// <summary>
		/// A value that indicates whether to zoom in on the marker on hover.
		/// </summary>
		[C1Description("C1Chart.ScatterChart.ZoomOnHover")]
		[WidgetOption]
		[DefaultValue(true)]
		[C1Category("Category.Behavior")]
#if !EXTENDER
		[Layout(LayoutType.Behavior)]
#endif
		public bool ZoomOnHover
		{
			get
			{
				return this.GetPropertyValue<bool>("ZoomOnHover", true);
			}
			set
			{
				this.SetPropertyValue<bool>("ZoomOnHover", value);
			}
		}

		#region Serialize

		/// <summary>
		/// Determine whether the SeriesTransition property should be serialized to client side.
		/// </summary>
		/// <returns>
		/// Returns false if SeriesTransition has default values, otherwise return false.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool ShouldSerializeSeriesTransition()
		{
			if (!this.SeriesTransition.Enabled)
				return true;
			if (this.SeriesTransition.Duration != 2000)
				return true;
			//if (this.SeriesTransition.Easing != ">")
			if (this.SeriesTransition.Easing != ChartEasing.EaseInCubic)
				return true;
			return false;
		}

		#endregion
	}
}
