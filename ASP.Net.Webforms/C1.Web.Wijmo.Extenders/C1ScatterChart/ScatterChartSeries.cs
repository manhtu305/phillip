﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Web.UI;

#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
namespace C1.Web.Wijmo.Extenders.C1Chart
#else
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1Chart
#endif
{
	/// <summary>
	/// Represents the ScatterChartSeires class which contains all settings for the chart series.
	/// </summary>
	public class ScatterChartSeries : ChartSeries
	{
		/// <summary>
		/// Initializes a new instance of the ScatterChartSeries class.
		/// </summary>
		public ScatterChartSeries()
			:base()
		{
			this.Data = new ChartSeriesData();
		}

		/// <summary>
		/// A value that indicates the data of the chart series.
		/// </summary>
		[C1Description("C1Chart.ScatterChartSeries.Data")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		[WidgetOption]
		[C1Category("Category.Data")]
#if !EXTENDER
		[Layout(LayoutType.Data)]
#endif
		public ChartSeriesData Data { get; set; }

		/// <summary>
		/// A value that indicates the type of the scatter.
		/// </summary>
		[C1Description("C1Chart.ScatterChartSeries.MarkerType")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(MarkerType.Circle)]
		[C1Category("Category.Appearance")]
#if !EXTENDER
		[Layout(LayoutType.Appearance)]
#endif
		public MarkerType MarkerType
		{
			get
			{
				return this.GetPropertyValue<MarkerType>("MarkerType", MarkerType.Circle);
			}
			set
			{
				this.SetPropertyValue<MarkerType>("MarkerType", value);
			}
		}

		/// <summary>
		/// Returns a boolean value that indicates whether the scatter chart series
		/// should be serialized to string.
		/// </summary>
		/// <returns>
		/// Returns a boolean value to determine whether this scatter chart series should be serialized.
		/// </returns>
		[EditorBrowsable(EditorBrowsableState.Never)]
		protected internal override bool ShouldSerialize()
		{
			var shouldSerialize = base.ShouldSerialize();
			if (shouldSerialize)
				return true;
			if (Data.ShouldSerialize())
				return true;
			return false;
		}
	}
}
