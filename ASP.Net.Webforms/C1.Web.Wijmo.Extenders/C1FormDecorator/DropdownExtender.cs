﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Extenders.Localization;

namespace C1.Web.Wijmo.Extenders.C1FormDecorator
{
	/// <summary>
	/// Extender for the <b>wijdropdown</b> widget.
	/// </summary>
    [WidgetDependencies(
        "wijmo.jquery.wijmo.wijsuperpanel.js",
        "wijmo.jquery.wijmo.wijdropdown.js",
		ResourcesConst.WIJMO_OPEN_CSS)]
	[ToolboxBitmap(typeof(C1DropdownExtender), "Dropdown.png")]
	[TargetControlType(typeof(DropDownList))]
	[LicenseProvider]
    [ToolboxItem(true)]
    public class C1DropdownExtender : WidgetExtenderControlBase
	{

		#region ** fields
		private bool _productLicensed = false;
		#endregion

		#region ** constructor
		public C1DropdownExtender() 
		{
			VerifyLicense();
		}

		internal virtual void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1DropdownExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}
		#endregion

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		internal override bool IsWijMobileExtender
		{
			get
			{
				return false;
			}
		}

		#region ** Options
		/// <summary>
		/// Gets or sets the dropdown's z-index.
		/// </summary>
		[C1Description("C1Dropdown.ZIndex")]
		[Category("Options")]
		[WidgetOption]
		[DefaultValue(0)]
		public int ZIndex
		{
			get
			{
				return GetPropertyValue("ZIndex", 0);
			}
			set
			{
				SetPropertyValue("ZIndex", value);
			}
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}
		#endregion

		
	}
}
