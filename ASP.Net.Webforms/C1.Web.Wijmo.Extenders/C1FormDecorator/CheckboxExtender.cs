﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

namespace C1.Web.Wijmo.Extenders.C1FormDecorator
{
	/// <summary>
	/// Extender for the <b>wijcheckbox</b> widget.
	/// </summary>
	[TargetControlType(typeof(CheckBox))]
    [WidgetDependencies("wijmo.jquery.wijmo.wijcheckbox.js",
		ResourcesConst.WIJMO_OPEN_CSS)]
	[ToolboxBitmap(typeof(C1CheckboxExtender), "Checkbox.png")]
	[LicenseProvider]
    [ToolboxItem(true)]
    public class C1CheckboxExtender : WidgetExtenderControlBase
	{

		#region ** fields
		private bool _productLicensed = false;
		#endregion

		#region ** constructor
		public C1CheckboxExtender() 
		{
			VerifyLicense();
		}

		internal virtual void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1CheckboxExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		#endregion

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		internal override bool IsWijMobileExtender
		{
			get
			{
				return false;
			}
		}

		#region ** properties
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}
		#endregion

	}
}
