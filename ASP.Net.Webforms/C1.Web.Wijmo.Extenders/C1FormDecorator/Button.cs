﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using C1.Web.Wijmo.Extenders.Localization;

namespace C1.Web.Wijmo.Extenders.C1FormDecorator
{
	/// <summary>
	/// Extender for the <b>button</b> widget.
	/// </summary>
	[TargetControlType(typeof(WebControl))]
	[ToolboxItem(true)]
	[LicenseProvider]
	[ToolboxBitmap(typeof(C1ButtonExtender), "Button.png")]
	public class C1ButtonExtender : WidgetExtenderControlBase
	{

		#region ** fields
		private ButtonIcons _icons;
		private bool _productLicensed = false;
		#endregion

		#region ** contructor
		public C1ButtonExtender() 
		{
			VerifyLicense();
		}

		internal virtual void VerifyLicense()
		{
            var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1ButtonExtender), this, false);
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}
        #endregion

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Extenders.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

        internal override bool IsWijMobileExtender
		{
			get
			{
				return false;
			}
		}

		/// <summary>
		/// Whether to show any text - when set to false (display no text), icons (see icons option) must be enabled, otherwise it'll be ignored
		/// </summary>
		#region ** properties


		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}


		[WidgetOption]
		[DefaultValue(true)]
		[Category("Options")]
		[C1Description("C1Button.Text")]
		public bool Text
		{
			get
			{
				return GetPropertyValue<bool>("Text", true);
			}
			set
			{
				SetPropertyValue<bool>("Text", value);
			}
		}

		/// <summary>
		/// Text to show on the button. When not specified (null), the element's html content is used, 
		/// or its value attribute when it's an input element of type submit or reset; 
		/// or the html content of the associated label element if its an input of type radio or checkbox
		/// </summary>
		[WidgetOption]
		[DefaultValue("")]
		[Category("Options")]
		[C1Description("C1Button.Label")]
		public string Label
		{
			get
			{
				return GetPropertyValue<string>("Label", "");
			}
			set
			{
				SetPropertyValue<string>("Label", value);
			}
		}

		/// <summary>
		/// Icons to display, with or without text (see text option). The primary icon is displayed by default on the left of the label text, 
		/// the secondary by default is on the right. Value for the primary and secondary properties must be a classname (String), 
		/// eg. "ui-icon-gear". For using only one icon: icons: {primary:'ui-icon-locked'}. 
		/// For using two icons: icons: {primary:'ui-icon-gear',secondary:'ui-icon-triangle-1-s'}
		/// </summary>
		[WidgetOption]
		[Category("Options")]
		[C1Description("C1Button.Icons")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		public ButtonIcons Icons
		{
			get
			{
				if (_icons == null) 
				{
					_icons = new ButtonIcons();
				}
				return _icons;
			}
			set
			{
				_icons = value;
			}
		}

		protected override string WidgetName
		{
			get
			{
				return "button";
			}
		} 
		#endregion
	}


	#region ** Icon class

	public class ButtonIcons:Settings, IJsonEmptiable 
	{
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		public string Primary
		{
			get
			{
				return GetPropertyValue<string>("Primary", "");
			}
			set
			{
				SetPropertyValue<string>("Primary", value);
			}
		}

		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue("")]
		public string Secondary 
		{
			get
			{
				return GetPropertyValue<string>("Secondary", "");
			}
			set
			{
				SetPropertyValue<string>("Secondary", value);
			}
		}

		bool IJsonEmptiable.IsEmpty
		{
			get { return string.IsNullOrEmpty(Primary) && string.IsNullOrEmpty(Secondary); }
		}
	}
	#endregion
}
