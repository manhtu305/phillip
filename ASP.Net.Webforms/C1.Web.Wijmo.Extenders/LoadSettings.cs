﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
#if EXTENDER
using C1.Web.Wijmo.Extenders.Localization;
#else
using C1.Web.Wijmo.Controls.Localization;
#endif


#if EXTENDER
namespace C1.Web.Wijmo.Extenders
#else
namespace C1.Web.Wijmo.Controls
#endif
{
	/// <summary>
	/// Define the settings of the jQuery ajax load options.
	/// </summary>
	public class LoadSettings : Settings
	{
		/// <summary>
		/// Indicates the ajax request type is 'GET' or 'POST'.
		/// </summary>
		[C1Description("LoadSettings.Type")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(RequestType.Get)]
		public RequestType Type
		{
			get
			{
				return GetPropertyValue<RequestType>("Type", RequestType.Get);
			}
			set
			{
				SetPropertyValue<RequestType>("Type", value);
			}
		}

		/// <summary>
		/// Defines the data that ajax sents to server.
		/// </summary>
		[C1Description("LoadSettings.Data")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(null)]
		public String Data
		{
			get
			{
				return GetPropertyValue<String>("Data", null);
			}
			set
			{
				SetPropertyValue<String>("Data", value);
			}
		}

		/// <summary>
		/// Determines whether to reload the page.
		/// </summary>
		[C1Description("LoadSettings.ReloadPage")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(false)]
		public bool ReloadPage
		{
			get
			{
				return GetPropertyValue<bool>("ReloadPage", false);
			}
			set
			{
				SetPropertyValue<bool>("ReloadPage", value);
			}
		}

		/// <summary>
		/// Determines whether to show the load message when loading the page.
		/// </summary>
		[C1Description("LoadSettings.ShowLoadMsg")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(false)]
		public bool ShowLoadMsg
		{
			get
			{
				return GetPropertyValue<bool>("ShowLoadMsg", false);
			}
			set
			{
				SetPropertyValue<bool>("ShowLoadMsg", value);
			}
		}

		/// <summary>
		/// This delay allows loads that pull from browser cache to occur without showing the loading message.
		/// </summary>
		[C1Description("LoadSettings.LoadMsgDelay")]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[DefaultValue(50)]
		public int LoadMsgDelay
		{
			get
			{
				return GetPropertyValue<int>("LoadMsgDelay", 50);
			}
			set
			{
				SetPropertyValue<int>("LoadMsgDelay", value);
			}
		}
	}
}
