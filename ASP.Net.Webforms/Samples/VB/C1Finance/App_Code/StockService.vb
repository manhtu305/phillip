﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Runtime.Serialization
Imports System.ServiceModel
Imports System.ServiceModel.Activation
Imports System.ServiceModel.Web
Imports System.Text
Imports System.Web.Services

<ServiceContract([Namespace]:="http://stockinfoservice.componentone.com/")> _
<WebService([Namespace]:="http://stockinfoservice.componentone.com/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.Web.Script.Services.ScriptService()> _
<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)> _
Public Class StockService
    ' To use HTTP GET, add [WebGet] attribute. (Default ResponseFormat is WebMessageFormat.Json)
    ' To create an operation that returns XML,
    '     add [WebGet(ResponseFormat=WebMessageFormat.Xml)],
    '     and include the following line in the operation body:
    '         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
    <WebMethod()> _
    <OperationContract()> _
    Public Function GetStocks(Symbols As String) As List(Of Stock)
        Return Stocks.GetStocks(Symbols)
    End Function

    <WebMethod()> _
    <OperationContract()> _
    Public Function GetStockHistory(Symbol As String, StartDate As DateTime, EndDate As DateTime) As List(Of Stock)
        Return Stocks.GetStockHistory(Symbol, StartDate, EndDate)
    End Function
End Class


