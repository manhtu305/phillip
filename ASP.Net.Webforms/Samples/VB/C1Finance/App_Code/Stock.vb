﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Net
Imports System.Text
Imports System.IO
Imports System.Globalization
Imports System.Security.Authentication

''' <summary>
''' Summary description for Stock
''' </summary>
Public Class Stock
    '
    ' TODO: Add constructor logic here
    '
    Public Sub New()
    End Sub
    Public Property Open() As Single
        Get
            Return m_Open
        End Get
        Set(value As Single)
            m_Open = Value
        End Set
    End Property
    Private m_Open As Single
    Public Property Close() As Single
        Get
            Return m_Close
        End Get
        Set(value As Single)
            m_Close = Value
        End Set
    End Property
    Private m_Close As Single
    Public Property High() As Single
        Get
            Return m_High
        End Get
        Set(value As Single)
            m_High = Value
        End Set
    End Property
    Private m_High As Single
    Public Property Low() As Single
        Get
            Return m_Low
        End Get
        Set(value As Single)
            m_Low = Value
        End Set
    End Property
    Private m_Low As Single
    Public Property Volume() As Single
        Get
            Return m_Volume
        End Get
        Set(value As Single)
            m_Volume = Value
        End Set
    End Property
    Private m_Volume As Single
    Public Property PE() As Single
        Get
            Return m_PE
        End Get
        Set(value As Single)
            m_PE = Value
        End Set
    End Property
    Private m_PE As Single
    Public Property Valid() As Boolean
        Get
            Return m_Valid
        End Get
        Set(value As Boolean)
            m_Valid = Value
        End Set
    End Property
    Private m_Valid As Boolean
    Public Property Symbol() As String
        Get
            Return m_Symbol
        End Get
        Set(value As String)
            m_Symbol = Value
        End Set
    End Property
    Private m_Symbol As String
    Public Property Name() As String
        Get
            Return m_Name
        End Get
        Set(value As String)
            m_Name = Value
        End Set
    End Property
    Private m_Name As String
    Public Property [Date]() As DateTime
        Get
            Return m_Date
        End Get
        Set(value As DateTime)
            m_Date = Value
        End Set
    End Property
    Private m_Date As DateTime
End Class
Public Class Stocks

    Const _Tls12 As SslProtocols = DirectCast(&HC00, SslProtocols)
    Const Tls12 As SecurityProtocolType = DirectCast(_Tls12, SecurityProtocolType)
    Public Shared Function GetStocks(Symbols__1 As String) As List(Of Stock)
        Dim lst As New List(Of Stock)()

        Dim fmt = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={0}&apikey=EQ8R2LTG732VP7HE&datatype=csv&outputsize=full"
        Dim startDate = New DateTime(2018, 1, 1)

        For Each symbol As String In Symbols__1.Split(",")
            Dim url = String.Format(fmt, symbol)

            ' get content
            Dim sb = New StringBuilder()
            Dim wc = New WebClient()

            Dim defaultProxy As IWebProxy = WebRequest.DefaultWebProxy
            If defaultProxy IsNot Nothing Then
                defaultProxy.Credentials = CredentialCache.DefaultCredentials
                wc.Proxy = defaultProxy
            End If

            ServicePointManager.SecurityProtocol = Tls12

            Using sr = New StreamReader(wc.OpenRead(url))
                ' skip headers
                sr.ReadLine()

                Dim line = sr.ReadLine()
                If line IsNot Nothing And Not line.Contains("alphavantage") Then
                    Dim items = line.Split(","c)

                    Dim stock = New Stock()
                    stock.Open = Single.Parse(items(1), CultureInfo.InvariantCulture)
                    stock.Close = Single.Parse(items(4), CultureInfo.InvariantCulture)
                    stock.High = Single.Parse(items(2), CultureInfo.InvariantCulture)
                    stock.Low = Single.Parse(items(3), CultureInfo.InvariantCulture)
                    stock.Volume = Single.Parse(items(5), CultureInfo.InvariantCulture)
                    stock.Valid = True
                    stock.Symbol = symbol
                    stock.[Date] = DateTime.Parse(items(0), CultureInfo.InvariantCulture)

                    lst.Add(stock)
                End If
            End Using
        Next

        Return lst
    End Function

    Public Shared Function GetStockHistory(symbol As String, startDate As DateTime, endDate As DateTime) As List(Of Stock)
        Dim list As New List(Of Stock)()

        Dim fmt = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={0}&apikey=EQ8R2LTG732VP7HE&datatype=csv&outputsize=full"

        Dim url = String.Format(fmt, symbol)

        ' get content
        Dim sb = New StringBuilder()
        Dim wc = New WebClient()

        Dim defaultProxy As IWebProxy = WebRequest.DefaultWebProxy
        If defaultProxy IsNot Nothing Then
            defaultProxy.Credentials = CredentialCache.DefaultCredentials
            wc.Proxy = defaultProxy
        End If

        ServicePointManager.SecurityProtocol = Tls12

        Using sr = New StreamReader(wc.OpenRead(url))
            ' skip headers
            sr.ReadLine()

            Dim i As Integer = 0
            ' read each line
            Dim line = sr.ReadLine()
            While line IsNot Nothing
                Dim items = line.Split(","c)

                Dim stock = New Stock()
                stock.Open = Single.Parse(items(1), CultureInfo.InvariantCulture)
                stock.Close = Single.Parse(items(4), CultureInfo.InvariantCulture)
                stock.High = Single.Parse(items(2), CultureInfo.InvariantCulture)
                stock.Low = Single.Parse(items(3), CultureInfo.InvariantCulture)
                stock.Volume = Single.Parse(items(5), CultureInfo.InvariantCulture)
                stock.Valid = True
                stock.Symbol = symbol
                stock.[Date] = DateTime.Parse(items(0), CultureInfo.InvariantCulture)
                If (stock.Date <= Date.Now And stock.Date >= startDate) Then
                    list.Add(stock)
                End If

                line = sr.ReadLine()
            End While
        End Using

        Return list
    End Function
End Class


