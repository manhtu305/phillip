﻿<%@ Page Title="About Us" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="false"
    CodeFile="About.aspx.vb" Inherits="About" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        ComponentOne Stock Portfolio
    </h2>
    <p>
        This website was built in ASP.NET and uses control from Studio for ASP.NET Wijmo. It uses stock data from Yahoo Finance. The sample shows how to integrate server-side and client-side code to build rich interactive web apps.
    </p>
</asp:Content>