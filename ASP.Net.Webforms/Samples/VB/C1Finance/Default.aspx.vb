﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(sender As Object, e As EventArgs)
        If Not IsPostBack Then
            TxtDateFrom.[Date] = New DateTime(2018, 1, 1)
            TxtDateTo.[Date] = DateTime.Now.[Date]
        End If
    End Sub

    Protected Sub C1GridView1_SelectedIndexChanging(sender As Object, e As C1.Web.Wijmo.Controls.C1GridView.C1GridViewSelectEventArgs) Handles C1GridView1.SelectedIndexChanging
        If C1GridView1.SelectedRow IsNot Nothing Then
            Dim smbl As String = C1GridView1.Rows(e.NewSelectedIndex).Cells(2).Text
            ObjectDataSource2.SelectParameters("Symbol").DefaultValue = smbl
        End If
    End Sub
End Class
