ASP.NET WebForms C1ReportViewerPrintWithoutPreview
--------------------------------------------
How to provide a custom print behavior for the C1ReportViewer control.

The sample illustrates how to provide a custom print button that allows the user to print a report without having to open the Print Preview dialog.

The C1ReportViewer toolbar is modified from the code behind:


<code>
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim toolBar As C1ReportViewerToolbar = C1ReportViewer1.ToolBar
        DirectCast(toolBar.Controls(0), HtmlGenericControl).Style("display") = "none" ' hide original print button		
        ' Add custom print button
        Dim printButton As New HtmlGenericControl("button")
        printButton.Attributes("title") = "Custom Print"
        printButton.Attributes("class") = "custom-print"
        printButton.InnerHtml = "Custom Print"
        toolBar.Controls.AddAt(0, printButton)
    End Sub
</code>

When the user clicks the button, the custom client-side method is called. The loadPreviewHtml method is used to load the HTMLlayout code inside of a separate iframe element, 
which is then used for the print action.

Note that we need to wait until *all* images are loaded before executing the print action.


<code>
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			$(".custom-print").button({
				icons: {
					primary: "ui-icon-gear"
				}
			}).click(function () {
				var viewerSelector = "#<%=C1ReportViewer1.ClientID%>";
				var docStatus = $(viewerSelector).c1reportviewer("option", "documentStatus");
				if (docStatus.isGenerating) {
					alert("Can't print. Document is not generated.");
				} else {
					var customPrintFrame = document.getElementById("customprintframe"),
					doc = customPrintFrame.contentWindow.document;
					var printOptions = {
						rangeSubset: 0, /*(0 - all/ 1 - odd/ 2 - even)*/
						reversePages: false,
						dpi: 96,
						width: 800,
						height: 600
					};
					$("#print_status").html("Loading print layout");
					$("#print_status").show();
					$(viewerSelector).c1reportviewer("loadPreviewHtml", printOptions, function (data) {
						doc.body.innerHTML = data;
						// Wait while images load:
						$imgs = $(doc.body).find("img");
						_imagesCount = $imgs.length;
						for (var i = 0; i < $imgs.length; i++) {
							var im = $imgs[i];
							if (im.complete) {
								// image already loaded:
								onImageLoad();
							}
							else {
								$(im).bind("load", onImageLoad);
							}
						}
					});

					function onImageLoad() {
						_imagesCount--;
						$("#print_status").html("(Preparing for print) Images to be loaded: " + _imagesCount);
						if (_imagesCount == 0) {
							$("#print_status").hide();
							customPrintFrame.contentWindow.focus();
							customPrintFrame.contentWindow.print();
						}
					}
				}
				return false;
			});
		});
		var _imagesCount = 0;

	</script>
</code>