﻿Imports C1.C1Preview
Imports C1.Web.Wijmo.Controls.C1ReportViewer
Imports System.Web.UI.HtmlControls

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim toolBar As C1ReportViewerToolbar = C1ReportViewer1.ToolBar
        DirectCast(toolBar.Controls(0), HtmlGenericControl).Style("display") = "none"
        ' hide original print button		
        ' Add custom print button
        Dim printButton As New HtmlGenericControl("button")
        printButton.Attributes("title") = "Custom Print"
        printButton.Attributes("class") = "custom-print"
        printButton.InnerHtml = "Custom Print"
        toolBar.Controls.AddAt(0, printButton)
    End Sub
End Class
