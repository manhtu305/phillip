﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<%@ Register assembly="C1.Web.Wijmo.Controls.3" namespace="C1.Web.Wijmo.Controls.C1ReportViewer" tagprefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Custom toolbar sample</title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>    
	<script language="javascript" type="text/javascript">
	    $(document).ready(function () {
	        $(".custom-export").button({
	            icons: {
	                primary: "ui-icon-gear"
	            },
	            text: false
	        }).click(function () {
	            exportToFile();
	            return false;
	        });
	    });

	    function exportToFile() {
	        var viewerSelector = "#<%=C1ReportViewer1.ClientID%>";
	        var docStatus = $(viewerSelector).c1reportviewer("option", "documentStatus");
	        if (docStatus.isGenerating) {
	            alert("Can't export. Document is not yet generated.");
	        }
	        else {
	            document.getElementById("customsaveframe").src = "CustomSaveHttpHandler.ashx?documentKey=" + docStatus.documentKey + "&exportFormat=Open%20XML%20Excel&exportFormatExt=xlsx";
	        }
	        return false;
	    }
    </script>

        <cc1:C1ReportViewer ID="C1ReportViewer1" runat="server"  ReportsFolderPath="~/tempReports"
            Width="850px" Height="650px" 
            Zoom="100%"
            FileName="~/C1ReportXML/CommonTasks.xml"
            />

            <iframe id="customsaveframe" style="width:1px;height:1px;"></iframe>
    </form>
</body>
</html>