ASP.NET WebForms C1ReportViewerChangeExportButtonBehavior
--------------------------------------------
How to change behavior of the save button.

This sample illustrates how to add a custom export button that allows a user to download an exported report directly without opening a new browser window.

The C1ReportViewer toolbar is modified from the code behind:

<code>
        Dim toolBar As C1ReportViewerToolbar = C1ReportViewer1.ToolBar
        DirectCast(toolBar.Controls(1), HtmlGenericControl).Style("display") = "none" ' hide original save(export) button
        ' Add own export button
        Dim saveButton As New HtmlGenericControl("button")
        saveButton.Attributes("title") = "Custom export"
        saveButton.Attributes("class") = "custom-export"
        saveButton.InnerHtml = "Custom Save"
        toolBar.Controls.AddAt(1, saveButton)
    End Sub
</code>

When the user clicks the button, the custom HTTP handler "CustomSaveHttpHandler.ashx" is called and the exported file is written to the IFrame element.

<code>
	document.getElementById("customsaveframe").src = "CustomSavetHttpHandler.ashx?command=SaveToXML&documentKey=" + docStatus.documentKey + "&exportFormat=Open%20XML%20Excel&exportFormatExt=xlsx";
</code>