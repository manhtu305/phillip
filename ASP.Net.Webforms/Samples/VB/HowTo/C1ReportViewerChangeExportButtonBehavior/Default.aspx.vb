﻿Imports C1.C1Preview
Imports C1.Web.Wijmo.Controls.C1ReportViewer
Imports System.Web.UI.HtmlControls

Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Overrides Sub OnInit(e As EventArgs)
        MyBase.OnInit(e)
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Dim toolBar As C1ReportViewerToolbar = C1ReportViewer1.ToolBar
        DirectCast(toolBar.Controls(1), HtmlGenericControl).Style("display") = "none"
        ' hide original save(export) button
        ' Add own export button
        Dim saveButton As New HtmlGenericControl("button")
        saveButton.Attributes("title") = "Custom export"
        saveButton.Attributes("class") = "custom-export"
        saveButton.InnerHtml = "Custom Save"
        toolBar.Controls.AddAt(1, saveButton)
    End Sub
End Class
