﻿using System;
using System.Data.Services;
using System.Collections.Generic;
using System.Linq;

namespace DataService
{
    public class Chinook : DataService<iTunes.ChinookModel.ChinookEntities>
    {
        public static void InitializeService(IDataServiceConfiguration config)
        {
            config.SetEntitySetAccessRule("*", EntitySetRights.All);
        }
    }
}
