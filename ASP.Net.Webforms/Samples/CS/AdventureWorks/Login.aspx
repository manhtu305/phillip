﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="AdventureWorks.Login" %>
<%@ Register TagPrefix="uc1" TagName="CustomerRegister" Src="~/UserControls/CustomerLogin.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>AdventureWorks Cycles -  Sign Up or Register</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <uc1:CustomerRegister ID="ctrlCustomerRegister" runat="server" />
</asp:Content>
