﻿using System;
using System.Globalization;
using System.Linq;
using AdventureWorksDataModel;
using EpicAdventureWorks;

namespace AdventureWorks
{
	public partial class ReviewOrder : System.Web.UI.Page
	{
		protected void Page_Init(object sender, EventArgs e)
		{
			if (RequestContext.Current.Contact == null)
			{
				string loginUrl = Common.GetLoginPageURL(true, true);
				Response.Redirect(loginUrl);
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			Response.CacheControl = "private";
			Response.Expires = 0;
			Response.AddHeader("pragma", "no-cache");

			if (!IsPostBack)
			{
				BindGridView();
				BindPageControls();
			}
		}


		private void BindPageControls()
		{
			Contact cont = RequestContext.Current.Contact;
			Customer customer = CustomerManager.GetCustomerByContactID(cont.ContactID);
			SalesOrderHeader salesOrderHeader = SalesOrderManager.GetLatestSalesOrderHeaderByCustomerID(customer.CustomerID);
			Subtotal.Text = "$" + salesOrderHeader.SubTotal.ToString("N2");
			Feight.Text = "$" + salesOrderHeader.Freight.ToString("N2");
			TotalDue.Text = "$" + salesOrderHeader.TotalDue.ToString("N2");

			//bill info
			Address billAddress = salesOrderHeader.BillAddress;
			BillFirstName.Text = cont.FirstName;
			BillLastName.Text = cont.LastName;
			BillCreditCardType.Text = salesOrderHeader.CreditCard.CardType;
			BillCreditCardNumber.Text = salesOrderHeader.CreditCard.CardNumber;
			string expireMonth = salesOrderHeader.CreditCard.ExpMonth.ToString(CultureInfo.InvariantCulture);
			string expireYear = salesOrderHeader.CreditCard.ExpYear.ToString(CultureInfo.InvariantCulture);
			if (expireYear.Length < 2)
			{
				expireYear = DateTime.Now.Year.ToString("YYYY");
			}
			BillCreditCardExpire.Text = expireMonth + "/" + expireYear.Substring(expireYear.Length - 2);
			BillAddressLine1.Text = billAddress.AddressLine1;
			BillAddressLine2.Text = billAddress.AddressLine2;
			BillAddress.Text = billAddress.City + ", " + billAddress.StateProvince.StateProvinceCode + " " + billAddress.PostalCode;

			//ship info
			Address shipAddress = AddressManager.GetAddressByID(salesOrderHeader.ShipAddress.AddressID);
			Contact contact = salesOrderHeader.Contact;
			ShipFirstName.Text = contact.FirstName;
			ShipLastName.Text = contact.LastName;
			ShipAddressLine1.Text = shipAddress.AddressLine1;
			ShipAddressLine2.Text = shipAddress.AddressLine2;
			ShipAddress.Text = shipAddress.City + ", " + shipAddress.StateProvince.StateProvinceCode + " " + shipAddress.PostalCode;
			ShipPhone.Text = contact.Phone;
			ShipEmail.Text = contact.EmailAddress;
		}

		private void BindGridView()
		{
			Contact contact = RequestContext.Current.Contact;
			Customer customer = CustomerManager.GetCustomerByContactID(contact.ContactID);
			int salesOrderId = SalesOrderManager.GetLatestSalesOrderHeaderByCustomerID(customer.CustomerID).SalesOrderID;

			Entities entities = Common.DataEntities;
			var items = from item in entities.SalesOrderDetail
						join p in entities.Product on item.SpecialOfferProduct.ProductID equals p.ProductID
						where item.SalesOrderID == salesOrderId
						select new
						{
							item.SalesOrderDetailID,
							p.Name,
							item.UnitPrice,
							item.OrderQty,
							item.LineTotal
						};
			gvOrderDetails.DataSource = items;
			gvOrderDetails.DataBind();
		}

		protected void btnBack_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/Shipping.aspx");
		}
		protected void btnNext_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/OrderComplete.aspx");
		}
	}
}