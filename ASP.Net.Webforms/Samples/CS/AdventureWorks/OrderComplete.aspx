﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="OrderComplete.aspx.cs" Inherits="AdventureWorks.OrderComplete" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.3" namespace="C1.Web.Wijmo.Controls.C1ReportViewer" tagprefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>AdventureWorks Cycles - Shopping Cart</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="body">
        <h1 class="OrderCompleteTitle">
            Order Complete
        </h1>
        <p>
            Your order was successfully charged. You will receive a confirmation email with shipping information. 
            Below is a copy of your receipt.
        </p>
        <wijmo:C1ReportViewer ID="C1ReportViewer1" runat="server" Width="950px" Height="600px" Zoom="Fit Page" />
    </div>
</asp:Content>
