ASP.NET WebForms AdventureWorks
-------------------------------------------------------------------
The AdventureWorks sample is built based on the AdventureWorks SQL Server sample database.

The sample uses Entity Framework, Linq, AJAX, jQuery, CSS, and so much more. This is a full blown reference application for the fictitious AdventureWorks store.