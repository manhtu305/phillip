﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AdventureWorks
{
	public partial class MasterPage : System.Web.UI.MasterPage
	{
		/// <summary>
		/// Gets the shopping cart items count.
		/// </summary>
		/// <value>The shopping cart items count.</value>
		protected int ShoppingCartItemsCount
		{
			get
			{

				return 0;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{

		}
	}
}