﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="ReviewOrder.aspx.cs" Inherits="AdventureWorks.ReviewOrder" %>
<%@ Register TagPrefix="c1" TagName="OrderProgress" Src="~/UserControls/Order/OrderProgress.ascx" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>AdventureWorks Cycles - Shopping Cart</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <div class="shoppingcart">
        <c1:OrderProgress ID="OrderSummary1" runat="server" OrderStep="Review Order"></c1:OrderProgress>
        <wijmo:C1GridView ID="gvOrderDetails" runat="server" DataKeyNames="SalesOrderDetailID"
            AutoGenerateColumns="false" Width="950px">
            <Columns>
                <wijmo:C1BoundField DataField="Name" HeaderText="Item">
                </wijmo:C1BoundField>
                <wijmo:C1BoundField DataField="UnitPrice" HeaderText="Price" DataFormatString="C">
                    <ItemStyle CssClass="number" />
                    <HeaderStyle CssClass="number" />
                </wijmo:C1BoundField>
                <wijmo:C1BoundField DataField="OrderQty" HeaderText="Quantity">
                    <ItemStyle CssClass="number" />
                    <HeaderStyle CssClass="number" />
                </wijmo:C1BoundField>
                <wijmo:C1BoundField DataField="LineTotal" HeaderText="Cost" DataFormatString="C">
                    <ItemStyle CssClass="number" />
                    <HeaderStyle CssClass="number" />
                </wijmo:C1BoundField>
            </Columns>
        </wijmo:C1GridView>
        <div class="cartinfo">
            <h6>
                Sub-Total:</h6>
            <div class="item subtotal">
                <asp:Label runat="server" ID="Subtotal"></asp:Label>
            </div>
            <h6>
                Shipping:</h6>
            <div class="item shipping">
                <asp:Label runat="server" ID="Feight"></asp:Label>
            </div>
            <asp:PlaceHolder runat="server" ID="phTaxTotal">
                <h6>
                    Tax:</h6>
                <div class="item tax">
                    <asp:Label ID="lblTaxAmount" runat="server" CssClass="productPrice" Text="$0" />
                </div>
            </asp:PlaceHolder>
            <div class="total">
                <h6>
                    Total:</h6>
                <div class="item totalinner">
                    <asp:Label runat="server" ID="TotalDue"></asp:Label>
                </div>
            </div>
        </div>
        <div class="BillTo">
        <fieldset>
            <legend>Bill To</legend>
            <ul>
                <li class="BillName">
                    <asp:Label runat="server" ID="BillFirstName" CssClass="BillFirstName">
                    </asp:Label> <asp:Label runat="server"
                        ID="BillLastName" CssClass="BillLastName"></asp:Label>
                </li>
                <li class="CreditCardType">
                    <asp:Label runat="server" ID="BillCreditCardType"></asp:Label>
                </li>
                <li class="CreditCardNumber">
                    <asp:Label runat="server" ID="BillCreditCardNumber"></asp:Label>
                </li>
                <li class="CreditCardExpire">
                    <asp:Label runat="server" ID="BillCreditCardExpire"></asp:Label>
                </li>
                <li class="BillAddress">
                    <asp:Label runat="server" ID="BillAddressLine1"></asp:Label>
                </li>
                <li>
                    <asp:Label runat="server" ID="BillAddressLine2"></asp:Label>
                </li>
                <li>
                    <asp:Label runat="server" ID="BillAddress"></asp:Label>
                </li>
            </ul>
        </fieldset>
        </div>
        <div class="ShipTo">
        <fieldset>
            <legend>Ship To</legend>
            <ul>
                <li>
                    <asp:Label runat="server" ID="ShipFirstName" CssClass="ShipFirstName"></asp:Label>
                    <asp:Label runat="server" ID="ShipLastName" CssClass="ShipLastName"></asp:Label>
                </li>
                <li>
                    <asp:Label runat="server" ID="ShipAddressLine1"></asp:Label>
                </li>
                <li>
                    <asp:Label runat="server" ID="ShipAddressLine2"></asp:Label>
                </li>
                <li>
                    <asp:Label runat="server" ID="ShipAddress"></asp:Label>
                </li>
                <li>
                    <asp:Label runat="server" ID="ShipPhone"></asp:Label>
                </li>
                <li>
                    <asp:Label runat="server" ID="ShipEmail"></asp:Label>
                </li>
            </ul>
        </fieldset>
        </div>
        <div class="CheckOutFooter">
            <div class="right">
                <asp:Button runat="server" ID="btnNext" Text="Complete Order" OnClick="btnNext_Click" CssClass="NextButton"/>
            </div>
            <div class="right">
                <asp:Button runat="server" ID="btnBack" Text="Back" OnClick="btnBack_Click" CssClass="BackButton"/>
            </div>
        </div>
    </div>
</asp:Content>
