﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShoppingCart.aspx.cs" Inherits="AdventureWorks.ShoppingCart" MasterPageFile="~/Site.master" %>
<%@ Register TagPrefix="c1" TagName="OrderSummary" Src="~/UserControls/Order/OrderSummary.ascx" %>
<%@ Register TagPrefix="c1" TagName="OrderProgress" Src="~/UserControls/Order/OrderProgress.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<title>AdventureWorks Cycles - Shopping Cart</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="ShoppingCart">
        <c1:OrderProgress ID="OrderSummary1" runat="server" OrderStep="Shopping Cart">
        </c1:OrderProgress>
        <div class="ShoppingBody">
            <c1:OrderSummary ID="OrderSummaryControl" runat="server" />
        </div>
        <div class="ShoppingFooter">
            <asp:Button CssClass="ShoppingCartContinueButton" runat="server" ID="btnContinue" Text="Continue Shopping" onclick="btnContinue_Click" />
            <asp:Button CssClass="ShoppingCartCheckOutButton" runat="server" ID="btnCheckOut" OnClick="CheckOut" Text="Check Out" EnableViewState ="false"/>
        </div>
    </div>
</asp:Content>
