﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using EpicAdventureWorks;

namespace AdventureWorks
{
	[ServiceContract(Namespace = "AdventureWorks")]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	public class ShoppingCartService
	{
		[OperationContract]
		public string UpdateQuantity(int cartItemId, int quantity)
		{
			try
			{
				ShoppingCartManager.UpdateShoppingCartItem(cartItemId, quantity);
				return "{'success':true}";
			}
			catch (Exception ex)
			{
				return "{'success':false,'error':'Update failed. Detail: " + ex.Message + "'}";
			}
		}

		[OperationContract]
		public string Remove(int cartItemId)
		{
			try
			{
				ShoppingCartManager.DeleteShoppingCartItem(cartItemId);
				return "{'success':true}";
			}
			catch (Exception ex)
			{
				return "{'success':false,'error':'Remove failed. Detail: " + ex.Message + "'}";
			}
		}
	}
}
