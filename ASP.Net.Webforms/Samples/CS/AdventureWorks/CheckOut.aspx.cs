﻿using System;
using System.Globalization;
using AdventureWorksDataModel;
using EpicAdventureWorks;

namespace AdventureWorks
{
	public partial class CheckOut : System.Web.UI.Page
	{
		protected void Page_Init(object sender, EventArgs e)
		{
			BillInfo1.EmailInput.ReadOnly = true;

			if (RequestContext.Current.Contact == null)
			{
				string loginUrl = Common.GetLoginPageURL(true, true);
				Response.Redirect(loginUrl);
			}

		}

		private Address BillAddress
		{
			get
			{
				if (ViewState["BillAddress"] == null)
				{
					return null;
				}
				return (Address)ViewState["BillAddress"];
			}
			set
			{
				ViewState["BillAddress"] = value;
			}
		}


		protected void Page_Load(object sender, EventArgs e)
		{
			Response.CacheControl = "private";
			Response.Expires = 0;
			Response.AddHeader("pragma", "no-cache");

			if (!IsPostBack)
			{
				GetCustomerAndAddressInfo();
				Expiration.Date = DateTime.Today;
			}

		}
		protected void btnBack_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/ShoppingCart.aspx");
		}
		protected void btnNext_Click(object sender, EventArgs e)
		{
			if (RequestContext.Current.Contact == null)
			{
				string loginUrl = Common.GetLoginPageURL(true, true);
				Response.Redirect(loginUrl);
			}
			if (Page.IsValid)
			{
				SaveCustomerAndAddressInfo();
				SaveSalesOrderCreditCardInfo();
				Response.Redirect("~/Shipping.aspx");
			}
		}

		//save address and customer info.
		private void SaveCustomerAndAddressInfo()
		{
			Contact contact = RequestContext.Current.Contact;
			contact.EmailAddress = BillInfo1.Email;
			contact.FirstName = BillInfo1.FirstName;
			contact.LastName = BillInfo1.LastName;
			contact.Phone = BillInfo1.Phone;
			ContactManager.SaveChanges(contact);
			Entities entities = Common.DataEntities;
			if (BillAddress == null)
			{
				AddAddress(entities, contact);
				return;
			}

			Address address = AddressManager.GetAddressByID(BillAddress.AddressID, entities);
			if (address == null)
			{
				AddAddress(entities, contact);
				return;
			}

			SetAddressInfo(address, entities);
			AddressManager.SaveChanges(address);
			BillAddress = address;
		}

		private void AddAddress(Entities entities, Contact contact)
		{
			Address address = new Address();
			SetAddressInfo(address, entities);
			Customer customer = CustomerManager.GetCustomerByContactID(contact.ContactID, entities);
			BillAddress = AddressManager.AddToAddress("Bill", address, customer, entities);
		}

		private void SetAddressInfo(Address address, Entities entities)
		{
			address.AddressLine1 = BillInfo1.AddressLine1;
			address.AddressLine2 = BillInfo1.AddressLine2;
			address.City = BillInfo1.City;
			StateProvince stateProvince = AddressManager.GetStateProvinceFromCode(BillInfo1.State, entities);
			address.StateProvince = stateProvince;
			address.PostalCode = BillInfo1.Zip;
			address.ModifiedDate = DateTime.Now;
			address.rowguid = Guid.NewGuid();
		}

		private void SaveSalesOrderCreditCardInfo()
		{
			Contact contact = RequestContext.Current.Contact;
			Customer customer = CustomerManager.GetCustomerByContactID(contact.ContactID);
			if (customer == null)
			{
				return;
			}
			Entities entities = Common.DataEntities;
			SalesOrderHeader salesOrderHeader = SalesOrderManager.GetLatestSalesOrderHeaderByCustomerID(customer.CustomerID,
				entities);
			Address addr = AddressManager.GetAddressByID(BillAddress.AddressID, entities);
			salesOrderHeader.BillAddress = addr;
			salesOrderHeader.ShipAddress = addr;
			CreditCard creditCard = new CreditCard();
			creditCard.CardNumber = CardNumber.Text;
			creditCard.CardType = PaymentType.SelectedValue;
			if (Expiration.Date.HasValue)
			{
				creditCard.ExpMonth = (byte) (Expiration.Date.Value).Month;
				creditCard.ExpYear = (short) (Expiration.Date.Value).Year;
			}
			creditCard.ModifiedDate = DateTime.Now;
			salesOrderHeader.CreditCardApprovalCode = Code.Value.ToString(CultureInfo.InvariantCulture);
			SalesOrderManager.UpdateCreditCardInfo(salesOrderHeader, creditCard, entities);
		}

		private void GetCustomerAndAddressInfo()
		{
			Contact contact = RequestContext.Current.Contact;
			if (contact != null)
			{
				BillInfo1.Email = contact.EmailAddress;
				BillInfo1.FirstName = contact.FirstName;
				BillInfo1.LastName = contact.LastName;

				Customer customer = CustomerManager.GetCustomerByContactID(contact.ContactID);
				BillAddress = AddressManager.GetBillAddressByCustomerID(customer.CustomerID);
				if (BillAddress == null) BillAddress = CreateDefaultBillAddress();

				BillInfo1.AddressLine1 = BillAddress.AddressLine1;
				BillInfo1.AddressLine2 = BillAddress.AddressLine2;
				BillInfo1.City = BillAddress.City;
				BillInfo1.State = BillAddress.StateProvince.StateProvinceCode.Trim();
				BillInfo1.Zip = BillAddress.PostalCode;
			}
		}

		private Address CreateDefaultBillAddress()
		{
			var random = new Random();
			return new Address
			{
				AddressLine1 = random.Next(1, 999) + " S Highland Ave",
				AddressLine2 = "3rd Floor",
				City = "Pittsburgh",
				StateProvince = new StateProvince
				{
					StateProvinceCode = BillInfo1.StateComboBox.Items[random.Next(0, BillInfo1.StateComboBox.Items.Count - 1)].Value
				},
				PostalCode = random.Next(1, 9) + random.Next(0, 9999).ToString("0000")
			};
		}
	}
}