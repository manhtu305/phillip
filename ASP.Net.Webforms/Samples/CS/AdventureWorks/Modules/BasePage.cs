﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
namespace AdventureWorks
{
	/// <summary>
	/// Summary description for BaseModule
	/// </summary>
	public class BasePage : Page
	{

		protected void Page_PreRender(object sender, EventArgs e)
		{
			if (Page.Master != null)
			{
				HtmlControl bdy = (HtmlControl)Page.Master.FindControl("body1");
				if (bdy != null)
				{
					if (bdy.Attributes["class"] != null)
					{
						if (bdy.Attributes["class"].Contains(ClassName) == false)
						{
							bdy.Attributes["class"] += " " + ClassName;
						}
					}
					else
					{
						bdy.Attributes["class"] = ClassName;
					}
				}
			}
		}
		public BasePage()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public string ClassName
		{
			get
			{
				if (ViewState["ClassName"] == null)
				{
					ViewState["ClassName"] = "";
				}
				return (string)ViewState["ClassName"];
			}
			set
			{
				ViewState["ClassName"] = value;
			}
		}
	}
}