﻿
using System.Web;
using EpicAdventureWorks;

namespace AdventureWorks
{
    /// <summary>
    /// Summary description for ProductModule
    /// </summary>
    public class ProductPage : BasePage
    {
        protected AdventureWorksDataModel.Product _currentProduct;
        protected AdventureWorksDataModel.ProductCategory _currentProductCategory;
        protected AdventureWorksDataModel.ProductSubcategory _currentProductSubcategory;

        /// <summary>
        /// Gets the current product.
        /// </summary>
        /// <value>The current product.</value>
        public AdventureWorksDataModel.Product CurrentProduct
        {
            get
            {
                AdventureWorksDataModel.Product p = null;
                if (_currentProduct != null)
                {
                    p = _currentProduct;
                }
                else
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["Product"]))
                    {
                        _currentProduct = ProductManager.GetProductByName(Request.QueryString["Product"]);
                        p = _currentProduct;
                    }
                }

                return p;

            }
        }

        /// <summary>
        /// Gets the current product subcategory.
        /// </summary>
        /// <value>The current product subcategory.</value>
        public AdventureWorksDataModel.ProductSubcategory CurrentProductSubcategory
        {
            get
            {
                AdventureWorksDataModel.ProductSubcategory c = null;
                if (_currentProductSubcategory != null)
                {
                    c = _currentProductSubcategory;
                }
                else
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["SubCategory"]))
                    {
                        string s = HttpUtility.UrlDecode(Request.QueryString["SubCategory"]);
                        c = CategoryManager.GetProductSubcategoryByName(s);
                        _currentProductSubcategory = c;
                    }
                }
                return c;
            }
        }

        /// <summary>
        /// Gets the current product category.
        /// </summary>
        /// <value>The current product category.</value>
        public AdventureWorksDataModel.ProductCategory CurrentProductCategory
        {
            get
            {
                AdventureWorksDataModel.ProductCategory c = null;
                if (_currentProductCategory != null)
                {
                    c = _currentProductCategory;
                }
                else
                {

                    if (!string.IsNullOrEmpty(Request.QueryString["Category"]))
                    {
                        string s = HttpUtility.UrlDecode(Request.QueryString["Category"]);
                        c = CategoryManager.GetCategoryByName(s);
                        _currentProductCategory = c;
                    }
                }
                return c;
            }
        }
    }
}
