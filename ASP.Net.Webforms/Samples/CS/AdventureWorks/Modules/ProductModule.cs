﻿using System.Linq;
using System.Web;
using EpicAdventureWorks;
namespace AdventureWorks
{
	/// <summary>
	/// Summary description for ProductModule
	/// </summary>
	public class ProductModule : BaseModule
	{
		private AdventureWorksDataModel.Product _currentProduct;
		private AdventureWorksDataModel.ProductCategory _currentProductCategory;
		protected AdventureWorksDataModel.ProductSubcategory _currentProductSubcategory;

		/// <summary>
		/// Gets the current product.
		/// </summary>
		/// <value>The current product.</value>
		public AdventureWorksDataModel.Product CurrentProduct
		{
			get
			{
				AdventureWorksDataModel.Product p = null;
				if (_currentProduct != null)
				{
					p = _currentProduct;
				}
				else
				{
					var s = "";
					if (Request.QueryString["Product"] != null)
					{
						s = HttpUtility.UrlDecode(Request.QueryString["Product"]);
						p = Common.DataEntities.Product.First(prod => prod.Name == s);
						_currentProduct = p;
					}
				}
				return p;
			}
		}

		/// <summary>
		/// Gets the current product category.
		/// </summary>
		/// <value>The current product category.</value>
		protected AdventureWorksDataModel.ProductCategory CurrentProductCategory
		{
			get
			{
				AdventureWorksDataModel.ProductCategory c;
				if (_currentProductCategory == null)
				{
					string category = CurrentProductCategoryName;
					if (!string.IsNullOrEmpty(category))
					{
						string s = HttpUtility.UrlDecode(category);
						c = CategoryManager.GetCategoryByName(s);
						_currentProductCategory = c;
					}
				}
				return _currentProductCategory;
			}
		}


		/// <summary>
		/// Gets the current product subcategory.
		/// </summary>
		/// <value>The current product subcategory.</value>
		public AdventureWorksDataModel.ProductSubcategory CurrentProductSubcategory
		{
			get
			{
				AdventureWorksDataModel.ProductSubcategory c = null;
				if (_currentProductSubcategory != null)
				{
					c = _currentProductSubcategory;
				}
				else
				{
					if (!string.IsNullOrEmpty(Request.QueryString["SubCategory"]))
					{
						string s = HttpUtility.UrlDecode(Request.QueryString["SubCategory"]);
						c = CategoryManager.GetProductSubcategoryByName(s);
						_currentProductSubcategory = c;
					}
				}
				return c;
			}
		}

		/// <summary>
		/// Gets the current product category.
		/// </summary>
		/// <value>The current product category.</value>
		public string CurrentProductCategoryName
		{
			get
			{
				return Request.QueryString["Category"]; 	
			}
		}

		/// <summary>
		/// Gets the current sub product category.
		/// </summary>
		/// <value>The current product category.</value>
		public string CurrentSubProductCategoryName
		{
			get
			{
				return Request.QueryString["SubCategory"]; 
			}
		}
	}
}