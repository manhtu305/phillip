﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Web.UI.WebControls;
using AdventureWorksDataModel;
using C1.Web.Wijmo.Controls.C1ComboBox;
using C1.Web.Wijmo.Controls.C1GridView;
using EpicAdventureWorks;

namespace AdventureWorks
{
	public partial class ShoppingCart : BasePage
	{
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			var gv = OrderSummaryControl.FindControl("gvShopCart") as C1GridView;
			Debug.Assert(gv != null, "gv != null");
			gv.DataBound += C1GridView_DataBound;
		}

		private void C1GridView_DataBound(object sender, EventArgs e)
		{
			var gv = sender as C1GridView;
			Debug.Assert(gv != null, "gv != null");
			btnCheckOut.Visible = gv.Rows.Count != 0;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			Response.CacheControl = "private";
			Response.Expires = 0;
			Response.AddHeader("pragma", "no-cache");
		}
		protected void CheckOut(object sender, EventArgs e)
		{
			if (RequestContext.Current.Contact == null)
			{
				string loginUrl = Common.GetLoginPageURL(true, true);
				Response.Redirect(loginUrl);
			}
			else
			{
				Contact contact = RequestContext.Current.Contact;
				SalesOrderHeader header = SetOrderHeader();
				List<string> productIds = new List<string>();
				List<SalesOrderDetail> detailList = SetOrderDetail(productIds);
				if (productIds.Count > 0)
				{
					SalesOrderManager.AddToSalesOrder(contact, header, detailList, productIds);
					//Delete from ShopCartItem.
					ShoppingCartManager.DeleteShoppingCartItemByCartID(contact.ContactID.ToString(CultureInfo.InvariantCulture));
					Response.Redirect("~/Checkout.aspx");
				}
				else
				{
					ClientScript.RegisterStartupScript(GetType(), "SelectProduct", "alert('Please select a product first.');", true);
				}
			}
		}

		private SalesOrderHeader SetOrderHeader()
		{
			SalesOrderHeader header = new SalesOrderHeader();
			header.RevisionNumber = 1;
			header.ModifiedDate = header.OrderDate = header.DueDate = DateTime.Now;
			header.Status = 5;
			header.OnlineOrderFlag = false;
			header.SalesOrderNumber = "SO71774";

			var lblSubTotalAmount = OrderSummaryControl.FindControl("lblSubTotalAmount") as Label;
			Debug.Assert(lblSubTotalAmount != null, "lblSubTotalAmount != null");
			header.SubTotal = Convert.ToDecimal(lblSubTotalAmount.Text.Substring(1));

			header.TaxAmt = 0;

			var shippingCompany = OrderSummaryControl.FindControl("shippingCompany") as C1ComboBox;
			Debug.Assert(shippingCompany != null, "shippingCompany != null");
			header.Freight = Convert.ToDecimal(shippingCompany.SelectedItem.Value);

			header.TotalDue = header.SubTotal + header.Freight;
			header.rowguid = Guid.NewGuid();
			return header;
		}

		private List<SalesOrderDetail> SetOrderDetail(List<string> productIds)
		{
			List<SalesOrderDetail> list = new List<SalesOrderDetail>();

			C1GridView gv = OrderSummaryControl.FindControl("gvShopCart") as C1GridView;
			Debug.Assert(gv != null, "gv != null");
			for (int i = 0; i < gv.Rows.Count; i++)
			{
				SalesOrderDetail detail = new SalesOrderDetail();

				var dataKey = gv.DataKeys[i];
				Debug.Assert(dataKey != null, "dataKey != null");
				string itemId = dataKey.Value.ToString();

				ShoppingCartItem shopCartItem = ShoppingCartManager.GetShoppingCartItemByID(Int32.Parse(itemId));
				if(shopCartItem == null) continue;

				int quantity = shopCartItem.Quantity;
				decimal unitPrice = shopCartItem.Product.ListPrice;
				detail.OrderQty = (short)quantity;
				detail.UnitPrice = unitPrice;
				detail.UnitPriceDiscount = 0;
				detail.LineTotal = quantity * unitPrice;
				detail.ModifiedDate = DateTime.Now;
				detail.rowguid = Guid.NewGuid();
				list.Add(detail);

				var hiddenProductId = gv.Rows[i].FindControl("HFProductId") as HiddenField;
				Debug.Assert(hiddenProductId != null, "hiddenProductId != null");
				productIds.Add(hiddenProductId.Value);
			}
			return list;
		}
		protected void btnContinue_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/Default.aspx");
		}
	}

}