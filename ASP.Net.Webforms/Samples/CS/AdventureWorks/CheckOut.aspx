﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="CheckOut.aspx.cs" Inherits="AdventureWorks.CheckOut" %>
<%@ Register TagPrefix="c1" TagName="OrderProgress" Src="~/UserControls/Order/OrderProgress.ascx" %>
<%@ Register Src="UserControls/BillInfo.ascx" TagName="BillInfo" TagPrefix="uc1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>AdventureWorks Cycles - Shopping Cart</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="CheckOut">
        <c1:OrderProgress ID="OrderSummary1" runat="server" OrderStep="Billing"></c1:OrderProgress>
        <div class="CheckOutBody">
            <div class="BillInfo">
                <uc1:BillInfo ID="BillInfo1" Type="Bill" runat="server" />
            </div>
            <div class="PaymentInfo">
                <fieldset class="Payment">
                    <legend class="PaymentTypeTitle">Payment Type</legend>
                    <ul class="PaymentType">
                        <li class="PaymentCard">
                            <asp:RadioButtonList runat="server" ID="PaymentType" RepeatLayout="Flow" Repealiirection="Horizontal">
                                <asp:ListItem Text="Visa" Value="Visa" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="MasterCard" Value="MasterCard"></asp:ListItem>
                                <asp:ListItem Text="American Express" Value="AmericanExpress"></asp:ListItem>
                            </asp:RadioButtonList>
                        </li>
                        <li class="PaymentCardNumber">Card Number</li>
                        <li class="PaymentCardNumberValue">
							<wijmo:C1InputText runat="server" ID="CardNumber" Text="1234123412341234" Format="9" MaxLength="50">
		</wijmo:C1InputText>
                            <asp:RequiredFieldValidator runat="server" ID="cardnumberVal" ControlToValidate="CardNumber" ErrorMessage="Card Number can't be empty" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="cardRange" runat="server" ControlToValidate="CardNumber" MinimumValue="1" ErrorMessage="Invalid Card Number" Display="Dynamic" MaximumValue="9999999999999999" Type="Double"></asp:RangeValidator>
                        </li>
                        <li class="CardExpirationDate">Expiration</li>
                        <li class="CardExpirationDateValue">
                            <wijmo:C1InputDate runat="server" ID="Expiration" DateFormat="MM/yy"  width="300px" ShowSpinner="true"/>
                        </li>
                        <li class="CardCode">Code</li>
                        <li class="CardCodeValue">
                            <wijmo:C1InputNumeric runat="server" ID="Code" DecimalPlaces="0" Value="123" width="300px"/>
                        </li>
                    </ul>
                </fieldset>
            </div>
        </div>
        <div class="CheckOutFooter">
            <div class="right">
                <asp:Button CssClass="NextButton" runat="server" ID="btnNext" Text="Next (Shipping)" OnClick="btnNext_Click" />
            </div>
            <div class="right">
                <asp:Button CssClass="BackButton" runat="server" ID="btnBack" Text="Back" OnClick="btnBack_Click" />
            </div>
        </div>
    </div>
</asp:Content>
