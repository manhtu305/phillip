﻿using System;
using AdventureWorksDataModel;
using EpicAdventureWorks;

namespace AdventureWorks
{
	public partial class Shipping : System.Web.UI.Page
	{
		protected void Page_Init(object sender, EventArgs e)
		{
			if (RequestContext.Current.Contact == null)
			{
				string loginUrl = Common.GetLoginPageURL(true, true);
				Response.Redirect(loginUrl);
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			Response.CacheControl = "private";
			Response.Expires = 0;
			Response.AddHeader("pragma", "no-cache");
			ShipDate.MinDate = DateTime.Today;
			ShipDate.MaxDate = DateTime.Today.AddDays(14);
			if (!IsPostBack)
			{
				ShipInfo.SetReadOnly(true);
				GetCustomerAndAddressInfo();
			}
		}
		protected void btnBack_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/CheckOut.aspx");
		}
		protected void btnNext_Click(object sender, EventArgs e)
		{
			Contact contact = RequestContext.Current.Contact;
			if (contact == null)
			{
				string loginUrl = Common.GetLoginPageURL(true, true);
				Response.Redirect(loginUrl);
				return;
			}

			DateTime dt = ShipDate.SelectedDate;
			if (dt < DateTime.Now)
			{
				dt = DateTime.Now;
			}
			int addressId;

			int contactId = contact.ContactID;
			Customer customer = CustomerManager.GetCustomerByContactID(contact.ContactID);
			if (!SameAddress.Checked)
			{
				//Save ship address.
				contactId = SaveContact();
				addressId = SaveShipAddress();
			}
			else
			{
				addressId = AddressManager.GetBillAddressByCustomerID(customer.CustomerID).AddressID;
			}
			//update salesOrder.
			SalesOrderManager.UpdateShippingInfo(customer.CustomerID, contactId, addressId, dt);

			Response.Redirect("~/ReviewOrder.aspx");
		}

		private int SaveShipAddress()
		{
			Entities entities = Common.DataEntities;
			Address address = GetAddressFromPage(entities);
			Address dbAddress = AddressManager.GetAddress(address);
			if (dbAddress == null)
			{
				entities.AddToAddress(address);
				entities.SaveChanges();
				return address.AddressID;
			}
			
			return dbAddress.AddressID;
		}

		private int SaveContact()
		{
			Contact contact = GetContactFromPage();
			Entities entities = Common.DataEntities;
			entities.AddToContact(contact);
			entities.SaveChanges();
			return contact.ContactID;
		}

		private Address GetAddressFromPage(Entities entities)
		{
			Address address = new Address();
			address.AddressLine1 = ShipInfo.AddressLine1;
			address.AddressLine2 = ShipInfo.AddressLine2;
			address.City = ShipInfo.City;

			address.StateProvince = AddressManager.GetStateProvinceFromCode(ShipInfo.State, entities);
			address.PostalCode = ShipInfo.Zip;
			address.rowguid = Guid.NewGuid();
			address.ModifiedDate = DateTime.Now;
			return address;
		}
		private Contact GetContactFromPage()
		{
			Contact contact = new Contact();
			contact.FirstName = ShipInfo.FirstName;
			contact.LastName = ShipInfo.LastName;
			contact.EmailAddress = ShipInfo.Email;
			contact.Phone = ShipInfo.Phone;
			contact.rowguid = Guid.NewGuid();
			contact.ModifiedDate = DateTime.Now;
			contact.PasswordHash = "1";
			contact.PasswordSalt = "1";
			return contact;
		}

		private void GetCustomerAndAddressInfo()
		{
			Contact contact = RequestContext.Current.Contact;
			if (contact != null)
			{
				ShipInfo.Email = contact.EmailAddress;
				ShipInfo.FirstName = contact.FirstName;
				ShipInfo.LastName = contact.LastName;
				ShipInfo.Phone = contact.Phone;

				Customer customer = CustomerManager.GetCustomerByContactID(contact.ContactID);
				Address address = AddressManager.GetBillAddressByCustomerID(customer.CustomerID);
				if (address != null)
				{
					ShipInfo.AddressLine1 = address.AddressLine1;
					ShipInfo.AddressLine2 = address.AddressLine2;
					ShipInfo.City = address.City;
					ShipInfo.State = address.StateProvince.StateProvinceCode.Trim();
					ShipInfo.Zip = address.PostalCode;
				}
			}
		}
		protected void SameAddress_CheckedChanged(object sender, EventArgs e)
		{
			ShipInfo.SetReadOnly(SameAddress.Checked);
			//Reset the value
			if (SameAddress.Checked)
			{
				GetCustomerAndAddressInfo();
			}
		}
	}
}