﻿using System;
using EpicAdventureWorks;

namespace AdventureWorks
{
	public partial class Logout : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			Response.CacheControl = "private";
			Response.Expires = 0;
			Response.AddHeader("pragma", "no-cache");

			CustomerManager.Logout();
			Response.Redirect("~/default.aspx");
		}
	}
}