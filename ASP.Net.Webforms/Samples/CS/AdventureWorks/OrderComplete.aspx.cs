﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using AdventureWorksDataModel;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using EpicAdventureWorks;

namespace AdventureWorks
{
	public partial class OrderComplete : System.Web.UI.Page
	{
		protected void Page_Init(object sender, EventArgs e)
		{
			if (RequestContext.Current.Contact == null)
			{
				string loginUrl = Common.GetLoginPageURL(true, true);
				Response.Redirect(loginUrl);
			}
		}

		private const string ReportNamePrefix = "ReceiptReport";
		private static string _reportName = ReportNamePrefix;
		protected void Page_Load(object sender, EventArgs e)
		{
			Response.CacheControl = "private";
			Response.Expires = 0;
			Response.AddHeader("pragma", "no-cache");
			C1ReportViewer1.Cache.ShareBetweenSessions = false;
			
			if (C1ReportViewer.IsDocumentRegistered(_reportName))
			{
				C1ReportViewer.UnRegisterDocument(_reportName);
			}
			_reportName = ReportNamePrefix + DateTime.UtcNow.Ticks;
			C1ReportViewer.RegisterDocument(_reportName, GetReport);
			C1ReportViewer1.ReportName = _reportName;
			C1ReportViewer1.FileName = _reportName;
			C1ReportViewer1.CollapseToolsPanel = true;
		}

		/// <summary>
		/// Gets the report.
		/// </summary>
		/// <returns></returns>
		private object GetReport()
		{
			Contact contact = RequestContext.Current.Contact;
			Customer customer = CustomerManager.GetCustomerByContactID(contact.ContactID);
			SalesOrderHeader salesOrderHeader = SalesOrderManager.GetLatestSalesOrderHeaderByCustomerID(customer.CustomerID);

			C1.C1Report.C1Report rep = C1ReportViewer.CreateC1Report();
			rep.UseGdiPlusTextRendering = true;
			rep.EmfType = System.Drawing.Imaging.EmfType.EmfPlusOnly;
			// load report into control
			string reportFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,"adventureworks.xml");
			rep.Load(reportFile, "ReceiptReport");
			DataView dv = GetReportRecordSet(salesOrderHeader);
			rep.DataSource.Recordset = dv;

			foreach (C1.C1Report.Field f in rep.Fields)
			{
				C1.C1Report.C1Report sr = f.Subreport;
				if (sr == null) continue;
				sr.DataSource.Recordset = GetReportGridRecordSet(salesOrderHeader);

			}

			return rep;
		}

		private DataView GetReportRecordSet(SalesOrderHeader salesOrderHeader)
		{
			salesOrderHeader.BillAddressReference.Load();

			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("BillingAddress", typeof(string));
			dataTable.Columns.Add("BillingAddress2", typeof(string));
			dataTable.Columns.Add("BillingCity", typeof(string));
			dataTable.Columns.Add("BillingPostalCode", typeof(string));
			dataTable.Columns.Add("BillingStateProvinceCode", typeof(string));

			dataTable.Columns.Add("ShippingAddress", typeof(string));
			dataTable.Columns.Add("ShippingAddress2", typeof(string));
			dataTable.Columns.Add("ShippingCity", typeof(string));
			dataTable.Columns.Add("ShippingPostalCode", typeof(string));
			dataTable.Columns.Add("ShippingStateProvinceCode", typeof(string));
			dataTable.Columns.Add("SubTotal", typeof(decimal));
			dataTable.Columns.Add("Freight", typeof(decimal));

			dataTable.Columns.Add("FullName", typeof(string));
			dataTable.Columns.Add("EmailAddress", typeof(string));
			dataTable.Columns.Add("Phone", typeof(string));
			dataTable.Columns.Add("CardType", typeof(string));
			dataTable.Columns.Add("CardNumber", typeof(string));


			DataRow dr = dataTable.NewRow();
			dr["BillingAddress"] = salesOrderHeader.BillAddress.AddressLine1;
			dr["BillingAddress2"] = salesOrderHeader.BillAddress.AddressLine2;
			dr["BillingCity"] = salesOrderHeader.BillAddress.City;
			dr["BillingPostalCode"] = salesOrderHeader.BillAddress.PostalCode;
			salesOrderHeader.BillAddress.StateProvinceReference.Load();
			dr["BillingStateProvinceCode"] = salesOrderHeader.BillAddress.StateProvince.StateProvinceCode;

			dr["ShippingAddress"] = salesOrderHeader.ShipAddress.AddressLine1;
			dr["ShippingAddress2"] = salesOrderHeader.ShipAddress.AddressLine2;
			dr["ShippingCity"] = salesOrderHeader.ShipAddress.City;
			dr["ShippingPostalCode"] = salesOrderHeader.ShipAddress.PostalCode;
			salesOrderHeader.ShipAddress.StateProvinceReference.Load();
			dr["ShippingStateProvinceCode"] = salesOrderHeader.ShipAddress.StateProvince.StateProvinceCode;
			dr["SubTotal"] = salesOrderHeader.SubTotal;
			dr["Freight"] = salesOrderHeader.Freight;
			salesOrderHeader.ContactReference.Load();
			dr["FullName"] = salesOrderHeader.Contact.FirstName + " " + salesOrderHeader.Contact.LastName;
			dr["EmailAddress"] = salesOrderHeader.Contact.EmailAddress;
			dr["Phone"] = salesOrderHeader.Contact.Phone;
			salesOrderHeader.CreditCardReference.Load();
			dr["CardType"] = salesOrderHeader.CreditCard.CardType;
			dr["CardNumber"] = salesOrderHeader.CreditCard.CardNumber;
			dataTable.Rows.Add(dr);
			return dataTable.DefaultView;
		}

		private DataView GetReportGridRecordSet(SalesOrderHeader salesOrderHeader)
		{

			DataTable dataTable = new DataTable();
			dataTable.Columns.Add("SalesOrderID", typeof(int));
			dataTable.Columns.Add("Name", typeof(string));
			dataTable.Columns.Add("UnitPrice", typeof(decimal));
			dataTable.Columns.Add("OrderQty", typeof(short));
			dataTable.Columns.Add("LineTotal", typeof(decimal));

			salesOrderHeader.SalesOrderDetail.Load();
			IEnumerator<SalesOrderDetail> dt = salesOrderHeader.SalesOrderDetail.GetEnumerator();
			while (dt.MoveNext())
			{
				DataRow dr1 = dataTable.NewRow();
				Debug.Assert(dt.Current != null, "dt.Current != null");
				dr1["SalesOrderID"] = dt.Current.SalesOrderID;
				dt.Current.SpecialOfferProductReference.Load();
				dt.Current.SpecialOfferProduct.ProductReference.Load();
				dr1["Name"] = dt.Current.SpecialOfferProduct.Product.Name;
				dr1["UnitPrice"] = dt.Current.UnitPrice;
				dr1["OrderQty"] = dt.Current.OrderQty;
				dr1["LineTotal"] = dt.Current.LineTotal;

				dataTable.Rows.Add(dr1);
			}

			return dataTable.DefaultView;
		}
	}
}