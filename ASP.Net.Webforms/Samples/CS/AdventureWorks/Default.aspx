﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AdventureWorks.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	
	<div class="bikes_button">
        <a href="Products.aspx?Category=Bikes">
            <div class="bikes_inner">
                <h1>
                    Check out our full <strong>Bikes</strong> selection!</h1>
            </div>
        </a>
    </div>
    <div class="islandcontainer">
        <div class="island">
            <a href="Products.aspx?Category=Bikes&SubCategory=Mountain Bikes">
                <div class="mountain_tooltip">
                    <h1>
                        Mountain</h1>
                </div>
                <div id="mountain" class="mountain">
                </div>
            </a><a href="Products.aspx?Category=Bikes&SubCategory=Touring Bikes">
                <div class="road_tooltip">
                    <h1>
                        Touring</h1>
                </div>
                <div id="road" class="road">
                </div>
            </a><a href="Products.aspx?Category=Bikes&SubCategory=Road Bikes">
                <div class="touring_tooltip">
                    <h1>
                        Road</h1>
                </div>
                <div id="touring" class="touring">
                    &nbsp;
                </div>
            </a>
        </div>
    </div>
    <script type="text/javascript">

    	$(document).ready(function () {


    		$('.islandcontainer').fadeIn('slow');

    	});

    </script>
</asp:Content>
