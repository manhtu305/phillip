﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShopCart.aspx.cs" Inherits="AdventureWorks.ShopCart" %>

<%@ Register TagPrefix="c1" TagName="OrderSummary" Src="~/UserControls/Order/OrderSummary.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>AdventureWorks Cycles - Shopping Cart</title>
</head>
<body>
	<form id="form1" runat="server">
		<div class="ShoppingBody">
			<c1:OrderSummary ID="OrderSummaryControl" runat="server" IsInWindow="true">
			</c1:OrderSummary>
		</div>
	</form>
</body>
</html>
