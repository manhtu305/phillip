﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Shipping.aspx.cs" Inherits="AdventureWorks.Shipping" %>
<%@ Register TagPrefix="c1" TagName="OrderProgress" Src="~/UserControls/Order/OrderProgress.ascx" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1Calendar" TagPrefix="Wijmo" %>
<%@ Register Src="UserControls/BillInfo.ascx" TagName="BillInfo" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<title>AdventureWorks Cycles - Shopping Cart</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="shoppingcart">
        <c1:OrderProgress ID="OrderSummary1" runat="server" OrderStep="Shipping"></c1:OrderProgress>
        <div class="ShippingDate">
            <fieldset>
                <legend>Select Shipping Date</legend>
                <p>
                    Please select your preferred shipping date. Note: <em>You will recieve the shipment 3* days after the shipping date.</em>
                </p>
                <wijmo:C1Calendar runat="server" ID="ShipDate" AllowQuickPick="False">
					<SelectionMode Days="False" />
                </wijmo:C1Calendar>
            </fieldset>
        </div>
        <div class="ShippingInfo">
            <fieldset>
                <legend>Shipping</legend>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:CheckBox runat="server" AutoPostBack="true" ID="SameAddress" Text="Same address as Billing" Checked="true" OnCheckedChanged="SameAddress_CheckedChanged" />
                        <asp:Panel runat="server" ID="AddressContainer">
                            <uc1:BillInfo ID="ShipInfo" Type="Ship" runat="server" />
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </fieldset>
        </div>
        <div class="CheckOutFooter">
            <div class="right">
                <asp:Button runat="server" ID="btnNext" Text="Next (Preview)" OnClick="btnNext_Click" Class="BackButton"/>
            </div>
            <div class="right">
                <asp:Button runat="server" ID="btnBack" Text="Back" OnClick="btnBack_Click" Class="NextButton"/>
            </div>
        </div>
    </div>
</asp:Content>
