﻿using System;
using System.Drawing;
using System.Web.Security;
using EpicAdventureWorks;

namespace AdventureWorks.UserControls
{
	public partial class CustomerLogin : System.Web.UI.UserControl
	{
		protected void Login(object sender, EventArgs e)
		{
			// only user name is valdiated.
			if (CustomerManager.Login(UserName.Text, Password.Text))
			{
				FormsAuthentication.RedirectFromLoginPage(UserName.Text, RememberMe.Checked);
			}
			else
			{
				FailureText.Text = "Email or Password is invalid.";
				FailureText.ForeColor = Color.Red;
				FailureText.Visible = true;
			}
		}

		protected void SignUp(object sender, EventArgs e)
		{
			if (Membership.GetUser(txtUserEmail.TextWithLiterals) == null && ContactManager.GetContactByEmail(txtUserEmail.TextWithLiterals) == null)
			{
				CustomerManager.AddToCustomer(txtUserEmail.TextWithLiterals, txtPassword.Text, txtUserEmail.TextWithLiterals, txtFirstName.Text, txtLastName.Text);
				FormsAuthentication.RedirectFromLoginPage(txtUserEmail.TextWithLiterals, RememberMe.Checked);
			}
			else
			{
				lblErrorMessage.ForeColor = Color.Red;
				lblErrorMessage.Text = "User exists.";
				lblErrorMessage.Visible = true;
			}
		}
	}
}