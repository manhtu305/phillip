﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Tooltip.ascx.cs" Inherits="AdventureWorks.UserControls.Tooltip" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1ToolTip" TagPrefix="C1" %>
<C1:C1ToolTip runat="server" ID="Tooltip1" ShowDelay="0" CalloutFilled="true" 
	EnableCallBackMode="true" HideDelay="100" OnOnAjaxUpdate="Tooltip1_OnAjaxUpdate" Width="330px">
		<Position>
			<My Top="Bottom" Left="Center" />
			<At Top="Top" Left="Center" />
		</Position>
		
		<Content Content="abc" />
</C1:C1ToolTip>
<script type="text/javascript">
	var tooltip = $.wijmo.c1tooltip;
	if (tooltip && tooltip.prototype) {
		var _doCallback = tooltip.prototype._doCallback;
		tooltip.prototype._doCallback = function () {
			this.encode();
			_doCallback.apply(this, arguments);
		}

		var hide = tooltip.prototype.hide;
		tooltip.prototype.hide = function () {
			var tooltipCache = this._tooltipCache;
			if (tooltipCache.ajaxcancels && tooltipCache.ajaxcancels.length) {
				$.each(tooltipCache.ajaxcancels, function (index, callback) {
					callback();
				});
				tooltipCache.ajaxcancels = [];
			}
			hide.apply(this, arguments);
		}

		tooltip.prototype.showAt = function (point) {
			var self = this,
                tooltipCache = self._tooltipCache,
                _$tooltip = tooltipCache ? tooltipCache._$tooltip : null,
                o = self.options;
			if (!tooltipCache || o.disabled) {
				return;
			}

			_$tooltip.stop(true, true);

			if (tooltipCache._showAnimationTimer) {
				clearTimeout(tooltipCache._showAnimationTimer);
				tooltipCache._showAnimationTimer = null;
			}
			if (tooltipCache._showAtAnimationTimer) {
				clearTimeout(tooltipCache._showAtAnimationTimer);
				tooltipCache._showAtAnimationTimer = null;
			}
			if (tooltipCache._hideAnimationTimer) {
				clearTimeout(tooltipCache._hideAnimationTimer);
				tooltipCache._hideAnimationTimer = null;
			}

			if (!point) {
				if (o.ajaxCallback && $.isFunction(o.ajaxCallback) && !self._callbacked) {
					self._isAjaxCallback = true;
					o.ajaxCallback.call(self.element);
					tooltipCache.ajaxcancels = tooltipCache.ajaxcancels || [];
					tooltipCache.ajaxcancels.push(function () {
						self._isAjaxCallback = false;
					});
					return;
				}
				self._setText();
			}

			if (!!o.showDelay) {
				tooltipCache._showAtAnimationTimer =
					setTimeout(function () {
						self._showToolTipHelper(point, _$tooltip);
					}, o.showDelay);
			}
			else {
				self._showToolTipHelper(point, _$tooltip);
			}
		}

		tooltip.prototype._bindLiveEvents = function () {
			var self = this,
				o = self.options,
				element = self.element;

			if (self._content === undefined) {
				self._content = element.attr("title");
				element.attr("title", "");
			}

			element.unbind('.tooltip');

			if (o.mouseTrailing) {
				element.bind("mousemove.tooltip", function (e) {
					if (o.disabled) {
						return;
					}
					var offset = o.position.offset || "",
					offsets = offset.split(" ");
					if (offsets.length === 2) {
						self.showAt({
							x: e.pageX + parseInt(offsets[0], 10),
							y: e.pageY + parseInt(offsets[1], 10)
						});
					}
					else {
						self.showAt({ x: e.pageX, y: e.pageY });
					}
				});
			}

			element.bind("mouseleave.tooltip", $.proxy(self._hideIfNeeded, self));
			switch (o.triggers) {
				case "hover":
					element.bind("mouseenter.tooltip", $.proxy(self.show, self));
					break;
				case "click":
					element.bind("click.tooltip", $.proxy(self.show, self));
					break;
				case "focus":
					element.bind("focus.tooltip", $.proxy(self.show, self))
                    .bind("blur.tooltip", $.proxy(self._hideIfNeeded, self));
					break;
				case "rightClick":
					element.bind("contextmenu.tooltip", function (e) {
						self.show();
						e.preventDefault();
					});
					break;
			}
		}
	}

</script>