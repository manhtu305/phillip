﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="AdventureWorks.UserControls.Header" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1Tabs" TagPrefix="C1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1Menu" TagPrefix="C1" %>

<div id="hd">
    <div id="doc2" class="yui-t3 wrapper">
        <div class="hd-links-wrapper">
            <div class="hd-links">
                <ul>
                    <asp:LoginView ID="topLoginView" runat="server">
                        <AnonymousTemplate>
                            <li><a href="<%=Page.ResolveUrl("~/Login.aspx")%>" class="hd-ico-login">Sign Up or Register</a></li>
                        </AnonymousTemplate>
                        <LoggedInTemplate>
                            <li>
                                <%=Page.User.Identity.Name %>
                            </li>
                            <li><a href="<%=Page.ResolveUrl("~/Logout.aspx")%>" class="hd-ico-logout">Logout</a></li>
                        </LoggedInTemplate>
                    </asp:LoginView>
                    <li><a href="<%=Page.ResolveUrl("~/ShoppingCart.aspx")%>" class="hd-ico-cart">Shopping Cart</a> <a href="<%=Page.ResolveUrl("~/ShoppingCart.aspx")%>" class="hd-ico-cartItems">
                        <%=ShoppingCartItemsCount%></a> </li>
                </ul>
            </div>
        </div>
        <h2 class="banner">
			<asp:HyperLink NavigateUrl="~/Default.aspx" id="LnkHome" runat="server">
                <img src="Images/awc_logo.png" alt="ComponentOne" />
				</asp:HyperLink>
        </h2>
    </div>
	<div id="mainMenuContainer">
	<C1:C1Menu runat="server" ID="MainMenu"></C1:C1Menu>
		</div>
	<div id="subMenuContainer">
	<C1:C1Menu runat="server" ID="SubMenu"></C1:C1Menu>
		</div>
</div>

