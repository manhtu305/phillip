﻿using System;
using System.Collections.Generic;
using AdventureWorksDataModel;
using C1.Web.Wijmo.Controls.C1Menu;
using EpicAdventureWorks;

namespace AdventureWorks.UserControls
{
	public partial class Header : ProductModule
	{

		/// <summary>
		/// Gets the shopping cart items count.
		/// </summary>
		/// <value>The shopping cart items count.</value>
		protected int ShoppingCartItemsCount
		{
			get
			{
				EpicAdventureWorks.ShoppingCart sp = RequestContext.Current.UserShoppingCart;
				return sp.ShoppingCarItems.Count;
			}
		}



		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LoadMenus();
			}
		}
		private void LoadMenus()
		{
			List<ProductCategory> mainCatetories = CategoryManager.GetMainCategories();
			foreach (ProductCategory pc in mainCatetories)
			{
				C1MenuItem item = new C1MenuItem();
				item.Text = pc.Name;
				item.NavigateUrl = "~/Products.aspx?Category=" + pc.Name;
				MainMenu.Items.Add(item);
				if (pc.Name == CurrentProductCategoryName)
				{
					item.Selected = true;
					item.CssClass = "current";

					// load sub categories
					pc.ProductSubcategory.Load();
					foreach (ProductSubcategory productSubcategory in pc.ProductSubcategory)
					{
						C1MenuItem subitem = new C1MenuItem();
						subitem.Text = productSubcategory.Name;
						subitem.NavigateUrl = "~/Products.aspx?Category=" + pc.Name + "&SubCategory=" + productSubcategory.Name;
						subitem.CssClass += productSubcategory.Name.Replace(" ", "");
						SubMenu.Items.Add(subitem);
						if (CurrentProductSubcategory != null && subitem.Text == CurrentProductSubcategory.Name)
						{
							subitem.CssClass = "current";
							subitem.Selected = true;
						}
					}
				}
			}
		}
		
	}
}