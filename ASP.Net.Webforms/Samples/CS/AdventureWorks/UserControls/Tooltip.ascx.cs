﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdventureWorksDataModel;
using EpicAdventureWorks;

namespace AdventureWorks.UserControls
{
	public partial class Tooltip : System.Web.UI.UserControl
	{
		public string TargetSelector
		{
			get
			{
				return (string)ViewState["TargetSelector"];
			}
			set
			{
				ViewState["TargetSelector"] = value;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				this.Tooltip1.TargetSelector = TargetSelector;
			}
		}

		protected void Tooltip1_OnAjaxUpdate(C1.Web.Wijmo.Controls.C1ToolTip.C1ToolTipCallBackEventArgs e)
		{
			//OnAjaxUpdate(e);
			string source = e.Source;
			int productID = int.Parse(e.Source.Replace("product_", ""));
			Product prod = ProductManager.GetProductByProductId(productID);
			if (prod != null)
			{
				StringBuilder sb = new StringBuilder();

				sb.Append("<div class=\"ToolTipTable\">");
				sb.AppendFormat("<div class=\"ToolTipLabelName\">{0}</div>", prod.Name);
				sb.AppendFormat("<div class=\"ToolTipLabelPrice\">{0}</div>", prod.ListPrice.ToString("C"));
				sb.Append("<ul class=\"description\" >");
				sb.Append("<li colspan=\"2\" class=\"ToolTipLabelSpecs\">Specs</li>");
				sb.Append("<li class=\"ToolTipLabelColor\">Color:</li>");
				sb.AppendFormat("<li class=\"ToolTipLabelColorValue\">{0}</li>", prod.Color);
				sb.Append("<li class=\"ToolTipLabelSize\">Size:</li>");
				sb.AppendFormat("<li class=\"ToolTipLabelSizeValue\">{0}</li>", prod.Size);
				sb.Append("<li class=\"ToolTipLabelWeight\">Weight:</li>");
				sb.AppendFormat("<li class=\"ToolTipLabelWeightValue\">{0}Kg</li>", prod.Weight);
				sb.Append("</ul>");
				sb.Append("</div>");
				e.Content = sb.ToString();
				sb.Remove(0, sb.Length);
			}
		}
	}
}