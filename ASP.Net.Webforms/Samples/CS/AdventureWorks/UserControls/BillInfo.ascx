﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BillInfo.ascx.cs" Inherits="AdventureWorks.UserControls.BillInfo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<fieldset>
    <legend>Info</legend>
    <ul>
        <li>Email</li>
        <li>
			<wijmo:C1InputMask Width="190px" ID="EmailCon" runat="server" MaskFormat="CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC"
				 HidePromptOnLeave="true"></wijmo:C1InputMask>
        </li>
        <li>First Name</li>
        <li>
		<wijmo:C1InputText ID="FirstNameCon" runat="server" MaxLength="50">
		</wijmo:C1InputText>
        </li>
        <li>Last Name </li>
        <li>
		<wijmo:C1InputText ID="LastNameCon" runat="server" MaxLength="50">
		</wijmo:C1InputText>
        </li>
        <li>Address </li>
        <li>
		<wijmo:C1InputText Width="190px" runat="server" ID="AddressLine1Con" Text="201 S Highland Ave" MaxLength="100">
		</wijmo:C1InputText>
		<wijmo:C1InputText Width="190px" runat="server" ID="AddressLine2Con" Text="3rd Floor" MaxLength="100">
		</wijmo:C1InputText>
        </li>
        <li>City </li>
        <li>
		<wijmo:C1InputText runat="server" ID="CityCon" Text="Pittsburgh" MaxLength="100">
		</wijmo:C1InputText>
        </li>
        <li>State </li>
        <li>
            <wijmo:C1ComboBox runat="server" ID="StateCon" DropDownHeight="250">
                <Items>
                    <wijmo:C1ComboBoxItem Value="AL" Text="Alabama">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="AK" Text="Alaska">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="AZ" Text="Arizona">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="AR" Text="Arkansas">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="CA" Text="California">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="CO" Text="Colorado">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="CT" Text="Connecticut">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="DE" Text="Delaware">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="DC" Text="District of Columbia">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="FL" Text="Florida">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="GA" Text="Georgia">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="HI" Text="Hawaii">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="ID" Text="Idaho">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="IL" Text="Illinois">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="IN" Text="Indiana">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="IA" Text="Iowa">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="KS" Text="Kansas">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="KY" Text="Kentucky">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="LA" Text="Louisiana">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="ME" Text="Maine">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="MD" Text="Maryland">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="MA" Text="Massachusetts">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="MI" Text="Michigan">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="MN" Text="Minnesota">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="MS" Text="Mississippi">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="MO" Text="Missouri">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="MT" Text="Montana">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="NE" Text="Nebraska">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="NV" Text="Nevada">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="NH" Text="New Hampshire">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="NJ" Text="New Jersey">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="NM" Text="New Mexico">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="NY" Text="New York">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="NC" Text="North Carolina">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="ND" Text="North Dakota">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="OH" Text="Ohio">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="OK" Text="Oklahoma">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="OR" Text="Oregon">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="PA" Text="Pennsylvania" Selected="true">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="RI" Text="Rhode Island">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="SC" Text="South Carolina">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="SD" Text="South Dakota">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="TN" Text="Tennessee">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="TX" Text="Texas">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="UT" Text="Utah">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="VT" Text="Vermont">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="VA" Text="Virginia">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="WA" Text="Washington">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="WV" Text="West Virginia">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="WI" Text="Wisconsin">
                    </wijmo:C1ComboBoxItem>
                    <wijmo:C1ComboBoxItem Value="WY" Text="Wyoming">
                    </wijmo:C1ComboBoxItem>
                </Items>
            </wijmo:C1ComboBox>
        </li>
        <li>Zip </li>
        <li>
			<wijmo:C1InputText runat="server" ID="ZipCon" Width="60px" Text="15206" MaxLength="50" Format="9">
		</wijmo:C1InputText>
        </li>
        <li>Phone </li>
        <li>
			<wijmo:C1InputMask runat="server" ID="PhoneCon" MaskFormat="(999)000-0000" HidePromptOnLeave="true"
				 Text="(412)681-4343" ></wijmo:C1InputMask>
        </li>
    </ul>
</fieldset>
<asp:RequiredFieldValidator runat="server" ID="emailVal" ControlToValidate="EmailCon"
    ErrorMessage="Email can't be null" Display="None"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator runat="server" ID="firstnameVal" ControlToValidate="FirstNameCon"
    ErrorMessage="First Name can't be null" Display="None"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator runat="server" ID="lastnameVal" ControlToValidate="LastNameCon"
    ErrorMessage="Last Name can't be null" Display="None"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator runat="server" ID="addressline1Val" ControlToValidate="AddressLine1Con"
    ErrorMessage="Address Line 1 can't be null" Display="None"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator runat="server" ID="cityVal" ControlToValidate="CityCon"
    ErrorMessage="City can't be null" Display="None"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator runat="server" ID="stateVal" ControlToValidate="StateCon"
    ErrorMessage="State can't be null" Display="None"></asp:RequiredFieldValidator>
<asp:RequiredFieldValidator runat="server" ID="postalcodeVal" ControlToValidate="ZipCon"
    ErrorMessage="Postal Code can't be null" Display="None"></asp:RequiredFieldValidator>
<asp:ValidationSummary ID="valSummary" runat="server" HeaderText="Errors:" ShowSummary="true"
    DisplayMode="List" />