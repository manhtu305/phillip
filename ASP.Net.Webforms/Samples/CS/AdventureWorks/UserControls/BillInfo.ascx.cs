﻿using System;
using System.Web.UI;
using C1.Web.Wijmo.Controls.C1ComboBox;
using C1.Web.Wijmo.Controls.C1Input;

namespace AdventureWorks.UserControls
{
	public partial class BillInfo : UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}
		public string Email
		{
			get { return EmailCon.Text; }
			set { EmailCon.Text = value; }
		}
		public C1InputMask EmailInput
		{
			get
			{
				return EmailCon;
			}
		}

		public C1ComboBox StateComboBox
		{
			get
			{
				return StateCon;
			}
		}
		public string FirstName
		{
			get { return FirstNameCon.Text; }
			set { FirstNameCon.Text = value; }
		}
		public string LastName
		{
			get { return LastNameCon.Text; }
			set { LastNameCon.Text = value; }
		}
		public string AddressLine1
		{
			get { return AddressLine1Con.Text; }
			set { AddressLine1Con.Text = value; }
		}
		public string AddressLine2
		{
			get { return AddressLine2Con.Text; }
			set { AddressLine2Con.Text = value; }
		}
		public string City
		{
			get { return CityCon.Text; }
			set { CityCon.Text = value; }
		}

		public string State
		{
			get { return StateCon.SelectedValue; }
			set { StateCon.SelectedValue = value; }
		}

		public void SetReadOnly(bool value)
		{
			foreach (Control control in Controls)
			{
				if(control == null)
					continue;

				var readOnlyProp = control.GetType().GetProperty("ReadOnly");
				if (readOnlyProp != null)
				{
					readOnlyProp.SetValue(control, value, new object[0]);
					continue;
				}

				var enabledProp = control.GetType().GetProperty("Enabled");
				if (enabledProp != null)
				{
					enabledProp.SetValue(control, !value, new object[0]);
				}
			}
		}


		public string Zip
		{
			get { return ZipCon.Text; }
			set { ZipCon.Text = value; }
		}
		public string Phone
		{
			get { return PhoneCon.Text; }
			set { PhoneCon.Text = value; }
		}

		private string _type = "";
		public string Type
		{
			get
			{
				return _type;
			}
			set
			{
				_type = value;
			}
		}
	}
}