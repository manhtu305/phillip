﻿using System;

namespace AdventureWorks.UserControls.Order
{
	public partial class OrderProgress : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				SetLinks();
			}
		}
		private void SetLinks()
		{
			switch (OrderStep)
			{
				case "Shopping Cart":
					{

						shoppingProgress.Value = 1;
					}
					break;
				case "Billing":
					{
						shoppingProgress.Value = 2;
					}
					break;
				case "Shipping":
					{
						shoppingProgress.Value = 3;
					}
					break;
				case "Review Order":
					{
						shoppingProgress.Value = 4;
					}
					break;
				case "complete":
					{
					}
					break;
				default:
					{

					}
					break;
			}
		}

		/// <summary>
		/// Gets or sets the order step.
		/// </summary>
		/// <value>The order step.</value>
		public string OrderStep
		{
			get
			{
				object obj2 = ViewState["OrderStep"];
				if (obj2 != null)
					return (string)obj2;

					return string.Empty;
			}
			set
			{
				ViewState["OrderStep"] = value;
			}
		}
	}
}