﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderSummary.ascx.cs" Inherits="AdventureWorks.UserControls.Order.OrderSummary" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="C1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="C1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="C1" %>
<C1:C1GridView ID="gvShopCart" CallbackOptions="None" runat="server" DataKeyNames="ShoppingCartItemID" AutoGenerateColumns="false" Width="950px">
    <Columns>
        <c1:C1BoundField DataField="Name" HeaderText="Item">
        </c1:C1BoundField>
        <c1:C1BoundField DataField="ListPrice" HeaderText="Price" DataFormatString="c">
            <ItemStyle CssClass="number" />
            <HeaderStyle CssClass="number" />
        </c1:C1BoundField>
        <c1:C1TemplateField HeaderText="Quantity">
            <ItemStyle CssClass="wijdata-type-number" />
            <HeaderStyle CssClass="number" />
            <ItemTemplate>
                <asp:HiddenField ID="HFProductId" runat="server" Value='<%#Eval("ProductId") %>' />
               <C1:C1InputNumeric ID="NIQuantity" Width="50px" runat="server" DecimalPlaces="0" 
				   Value='<%#Convert.ToDouble(Eval("Quantity"))%>' MaxValue="1000" MinValue="1" 
				   ShowSpinner="True" OnClientValueChanged="QuantityChanged"></C1:C1InputNumeric>
            </ItemTemplate>
        </c1:C1TemplateField>
		<c1:C1TemplateField HeaderText="Cost">
            <ItemStyle CssClass="wijdata-type-number" />
            <HeaderStyle CssClass="number" />
            <ItemTemplate>
                <asp:label runat="server" ID="itemSubTotal" Text=<%#Eval("Cost", "{0:c}")%>/>
                <asp:HiddenField ID="hdShoppingCartItemID" runat="server" Value='<%#Eval("ShoppingCartItemID") %>' />
                <asp:HiddenField ID="hdItemPrice" runat="server" Value='<%#Eval("ListPrice") %>' />
            </ItemTemplate>
        </c1:C1TemplateField>
		<c1:C1TemplateField HeaderText="Remove">
            <HeaderStyle CssClass="number" />
            <ItemTemplate>
                <asp:Button runat="server" ID="itemRemoveBtn" Text="Remove"/>
            </ItemTemplate>
        </c1:C1TemplateField>
    </Columns>
</C1:C1GridView>
<asp:Panel runat="server" ID="Total">
    <div class="cartinfo">
        <h6>
            Sub-Total:</h6>
        <div class="item subtotal">
            <asp:Label ID="lblSubTotalAmount" runat="server" CssClass="productPrice" />
        </div>
        <h6>
            Shipping:</h6>
        <div class="item shipping">
            <asp:Label ID="lblShippingAmount" runat="server" CssClass="productPrice" />
            <C1:C1ComboBox ID="shippingCompany" runat="server" TemplateItemHighdivghted="True" Text="UPS" 
				Width="60px" DropDownHeight="70" ExpandAnimation="ScrollinFromTop" CollapseAnimation="ScrollOutToTop"
				 CollapseDuration="100" ExpandDuration="100" OnClientSelectedIndexChanged="ShippingCompanyChanged">
                <Items>
                    <c1:C1ComboBoxItem runat="server" Text="UPS" Selected="true" Value="25">
                    </c1:C1ComboBoxItem>
                    <c1:C1ComboBoxItem runat="server" Text="FedEX" Value="35">
                    </c1:C1ComboBoxItem>
                </Items>
            </C1:C1ComboBox>
        </div>
        <asp:PlaceHolder runat="server" ID="phTaxTotal">
            <h6>
                Tax:</h6>
            <div class="item tax">
                <asp:Label ID="lblTaxAmount" runat="server" CssClass="productPrice" Text="$0" />
            </div>
        </asp:PlaceHolder>
        <div class="total">
            <h6>
                Total:</h6>
            <div class="item totalinner">
                <asp:Label ID="lblTotalAmount" runat="server" CssClass="productPrice" />
            </div>
        </div>
    </div>
</asp:Panel>
	
<script>
	$(function () {
		$("[id$='itemRemoveBtn']").click(RemoveItem);
	});


	function RemoveItem(e) {
		e.preventDefault();

		var successCallback = function () {
			$(e.target).parents("tr").remove();
		};

		var svcSetting = GetCallServiceSetting(e, successCallback);
		svcSetting.svc["Remove"](svcSetting.cartItemId, svcSetting.success, svcSetting.fail);
	}

	function GetIdPrefix(id) {
		return id.substring(0, id.lastIndexOf("_") + 1);
	}

	var shoppingCartService;
	function GetCallServiceSetting(e, successCallback) {
		var idPrefix = GetIdPrefix(e.target.id);
		var cartItemId = parseInt($("#" + idPrefix + "hdShoppingCartItemID").val());

		var fail = function (data) {
			if (data) eval("var result=" + data);
			var error;
			if (data && result) {
				if (result.error) {
					error = result.error;
				} else {
					error = result;
				}
			}

			if (!error) {
				error = "Failed.";
			}

			alert(error);
		};

		var success = function (data) {
			if (data) eval("var result=" + data);
			if (!data && !result && !result.success) {
				fail(data);
				return;
			}

			if (successCallback) successCallback();
			UpdateShoppingCart();
		};

		if (shoppingCartService == null) {
			shoppingCartService = new AdventureWorks.ShoppingCartService();
		}

		return {
			svc: shoppingCartService, cartItemId: cartItemId,
			success: success, fail: fail
		};
	}

	function QuantityChanged(e, data) {
		var quantity = parseInt(data.value);
		var svcSetting = GetCallServiceSetting(e);
		svcSetting.svc["UpdateQuantity"](svcSetting.cartItemId, quantity, svcSetting.success, svcSetting.fail);
	}

	function UpdateShoppingCart() {
		var subTotal = 0;
		var quantityElement = $("[id$='NIQuantity']");
		quantityElement.each(function (index, element) {
			var $element = $(element);
			var quantity = $element.c1inputnumeric("option").value;
			var idPrefix = GetIdPrefix(element.id);
			var itemPrice = parseFloat($("#" + idPrefix + "hdItemPrice").val());
			var itemSubTotal = quantity * itemPrice;
			$("#" + idPrefix + "itemSubTotal").html(ToCurrency(itemSubTotal));
			subTotal += itemSubTotal;
		});

		$("[id$='lblSubTotalAmount']").html(ToCurrency(subTotal));
		var freight = parseFloat($("[id$='shippingCompany']").c1combobox("option").selectedValue);
		$("[id$='lblShippingAmount']").html(ToCurrency(freight));
		$("[id$='lblTotalAmount']").html(ToCurrency(subTotal + freight));
		$("a[class='hd-ico-cartItems']").html(quantityElement.length);
		if (quantityElement.length == 0) $("[id$='btnCheckOut']").css("display", "none");
	}

	function ToCurrency(n) {
		return "$" + n.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
	}

	function ShippingCompanyChanged(e, data) {
		UpdateShoppingCart();
	}
</script>