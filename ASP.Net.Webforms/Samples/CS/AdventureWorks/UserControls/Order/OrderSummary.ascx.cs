﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdventureWorksDataModel;
using C1.Web.Wijmo.Controls.C1GridView;
using EpicAdventureWorks;

namespace AdventureWorks.UserControls.Order
{
	public partial class OrderSummary : System.Web.UI.UserControl
	{
		private bool _isWindow;

		/// <summary>
		/// Gets or sets a value indicating whether this instance is in window.
		/// </summary>
		public bool IsInWindow
		{
			get
			{
				return _isWindow;
			}
			set
			{
				_isWindow = value;
				if (value)
				{
					Total.Visible = false;
					gvShopCart.Width = 550;
					gvShopCart.Columns[2].Visible = false;
					gvShopCart.Columns[4].Visible = false;
				}
			}
		}

		/// <summary>
		/// Handles the Init event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Init(object sender, EventArgs e)
		{
			gvShopCart.RowUpdating += gvShopCart_RowUpdating;
		}

		/// <summary>
		/// Handles the RowUpdating event of the gvShopCart control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="C1.Web.Wijmo.Controls.C1GridView.C1GridViewUpdateEventArgs"/> instance containing the event data.</param>
		void gvShopCart_RowUpdating(object sender, C1GridViewUpdateEventArgs e)
		{
			LoadData();
			BindTotalData();
		}


		protected void Page_Load(object sender, EventArgs e)
		{
			LoadData();
			BindTotalData();
		}

		/// <summary>
		/// Loads the data.
		/// </summary>
		private void LoadData()
		{
			List<ShoppingCartItem> ls = RequestContext.Current.UserShoppingCart.ShoppingCarItems;
			var binddata = from i in ls
						   select new { i.ShoppingCartItemID, i.Product.ProductID, i.Product.Name, i.Product.ListPrice, i.Quantity, Cost = i.Product.ListPrice * i.Quantity };
			gvShopCart.DataSource = binddata.ToList();
			gvShopCart.DataBind();
		}

		/// <summary>
		/// Binds the total data.
		/// </summary>
		private void BindTotalData()
		{
			List<ShoppingCartItem> ls = RequestContext.Current.UserShoppingCart.ShoppingCarItems;
			var binddata = from i in ls
						   select new { i.ShoppingCartItemID, i.Product.ProductID, i.Product.Name, i.Product.ListPrice, i.Quantity, Cost = i.Product.ListPrice * i.Quantity };

			var subTotal = binddata.Sum(item => item.Cost);

			lblSubTotalAmount.Text = subTotal.ToString("C");

			lblShippingAmount.Text = decimal.Parse(shippingCompany.SelectedItem.Value).ToString("C");
			lblTotalAmount.Text = (subTotal + decimal.Parse(shippingCompany.SelectedItem.Value)).ToString("C");
		}
	}
}