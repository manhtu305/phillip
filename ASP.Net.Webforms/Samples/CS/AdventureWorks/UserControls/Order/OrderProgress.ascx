﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderProgress.ascx.cs" Inherits="AdventureWorks.UserControls.Order.OrderProgress" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1ProgressBar" TagPrefix="wijmo" %>
<div class="OrderProgress">
    <wijmo:C1ProgressBar runat="server" Width="950px" ID="shoppingProgress"
        MaxValue="4" Value="1" MinValue="0" LabelAlign="Center" LabelFormatString="Step {0} of {5}">
    </wijmo:C1ProgressBar>
</div>