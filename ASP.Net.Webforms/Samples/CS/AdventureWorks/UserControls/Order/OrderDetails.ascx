﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderDetails.ascx.cs" Inherits="AdventureWorks.UserControls.Order.OrderDetails" %>
<div class="OrderDetails">
    <table width="100%">
        <tr>
            <td style="text-align: left;">
                <div class="title">
                    OrderInformation
                </div>
            </td>
            <td style="text-align: right;">
                <asp:HyperLink runat="server" ID="lnkPrint" Text="Print" Target="_blank" CssClass="orderdetailsprintbutton" />
            </td>
        </tr>
    </table>
    <div class="clear">
    </div>
    <div class="info">
        <div class="OrderOverview">
            <table width="100%" cellspacing="0" cellpadding="2" border="0">
                <tbody>
                    <tr>
                        <td colspan="2">
                            <b>
                                Order#<asp:Label ID="lblOrderID" runat="server" />
                            </b>
                        </td>
                    </tr>
                    <tr>
                        <td class="smallText">
                            OrderDate:
                            <asp:Label ID="lblCreatedOn" runat="server"></asp:Label>
                        </td>
                        <td align="right" class="smallText">
                            OrderTotal:
                            <asp:Label ID="lblOrderTotal" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="smallText">
                            OrderStatus
                            <asp:Label ID="lblOrderStatus" runat="server"></asp:Label>
                        </td>
                        <td colspan="2">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="clear">
        </div>
        <asp:Panel CssClass="ShippingBox" runat="server" ID="pnlShipping">
            <table width="100%" border="0">
                <tbody>
                    <tr>
                        <td>
                            <b>
                                ShippingAddress</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Literal ID="lShippingFirstName" runat="server"></asp:Literal>
                            <asp:Literal ID="lShippingLastName" runat="server"></asp:Literal><br />
                            <div>
                                Order.Email:
                                <asp:Literal ID="lShippingEmail" runat="server"></asp:Literal></div>
                            <div>
                                Order.Phone:
                                <asp:Literal ID="lShippingPhoneNumber" runat="server"></asp:Literal></div>
                            <div>
                                Order.Fax:
                                <asp:Literal ID="lShippingFaxNumber" runat="server"></asp:Literal></div>
                            <asp:Panel ID="pnlShippingCompany" runat="server">
                                <asp:Literal ID="lShippingCompany" runat="server"></asp:Literal></asp:Panel>
                            <div>
                                <asp:Literal ID="lShippingAddress1" runat="server"></asp:Literal></div>
                            <asp:Panel ID="pnlShippingAddress2" runat="server">
                                <asp:Literal ID="lShippingAddress2" runat="server"></asp:Literal></asp:Panel>
                            <div>
                                <asp:Literal ID="lShippingCity" runat="server"></asp:Literal>,
                                <asp:Literal ID="lShippingStateProvince" runat="server"></asp:Literal>
                                <asp:Literal ID="lShippingZipPostalCode" runat="server"></asp:Literal></div>
                            <asp:Panel ID="pnlShippingCountry" runat="server">
                                <asp:Literal ID="lShippingCountry" runat="server"></asp:Literal></asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                Order.ShippingMethod</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblShippingMethod" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                Order.ShippedOn</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblShippedDate" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>
                                Order.Weight</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblOrderWeight" runat="server"></asp:Label>
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
        <div class="clear">
        </div>
        <div class="SectionTitle">
            Order.BillingInformation
        </div>
        <div class="BillingBox">
            <table width="100%" border="0">
                <tbody>
                    <tr>
                        <td>
                            <table width="100%" cellspacing="3" cellpadding="2" border="0">
                                <tbody>
                                    <tr>
                                        <td>
                                            <b>
                                                Order.BillingAddress</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Literal ID="lBillingFirstName" runat="server"></asp:Literal>
                                            <asp:Literal ID="lBillingLastName" runat="server"></asp:Literal><br />
                                            <div>
                                                Order.Email:
                                                <asp:Literal ID="lBillingEmail" runat="server"></asp:Literal></div>
                                            <div>
                                                Order.Phone:
                                                <asp:Literal ID="lBillingPhoneNumber" runat="server"></asp:Literal></div>
                                            <div>
                                                Order.Fax")%>:
                                                <asp:Literal ID="lBillingFaxNumber" runat="server"></asp:Literal></div>
                                            <asp:Panel ID="pnlBillingCompany" runat="server">
                                                <asp:Literal ID="lBillingCompany" runat="server"></asp:Literal></asp:Panel>
                                            <div>
                                                <asp:Literal ID="lBillingAddress1" runat="server"></asp:Literal></div>
                                            <asp:Panel ID="pnlBillingAddress2" runat="server">
                                                <asp:Literal ID="lBillingAddress2" runat="server"></asp:Literal></asp:Panel>
                                            <div>
                                                <asp:Literal ID="lBillingCity" runat="server"></asp:Literal>,
                                                <asp:Literal ID="lBillingStateProvince" runat="server"></asp:Literal>
                                                <asp:Literal ID="lBillingZipPostalCode" runat="server"></asp:Literal></div>
                                            <asp:Panel ID="pnlBillingCountry" runat="server">
                                                <asp:Literal ID="lBillingCountry" runat="server"></asp:Literal></asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>
                                                Order.PaymentMethod</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Literal runat="server" ID="lPaymentMethod"></asp:Literal>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <table width="100%" cellspacing="0" cellpadding="2" border="0">
                                <tbody>
                                    <tr>
                                        <td width="100%" align="right">
                                            <b>
                                                Order.Sub-Total:</b>
                                        </td>
                                        <td align="right">
                                            <span style="white-space: nowrap;">
                                                <asp:Label ID="lblOrderSubtotal" runat="server"></asp:Label>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="right">
                                            <b>
                                                Order.Shipping:</b>
                                        </td>
                                        <td align="right">
                                            <span style="white-space: nowrap;">
                                                <asp:Label ID="lblOrderShipping" runat="server"></asp:Label>
                                            </span>
                                        </td>
                                    </tr>
                                    <asp:PlaceHolder runat="server" ID="phPaymentMethodAdditionalFee">
                                        <tr>
                                            <td width="100%" align="right">
                                                <b>
                                                    Order.PaymentMethodAdditionalFee:</b>
                                            </td>
                                            <td align="right">
                                                <span style="white-space: nowrap;">
                                                    <asp:Label ID="lblPaymentMethodAdditionalFee" runat="server"></asp:Label>
                                                </span>
                                            </td>
                                        </tr>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder runat="server" ID="phTaxTotal">
                                        <tr>
                                            <td width="100%" align="right">
                                                <b>
                                                    Order.Tax:</b>
                                            </td>
                                            <td align="right">
                                                <span style="white-space: nowrap;">
                                                    <asp:Label ID="lblOrderTax" runat="server"></asp:Label>
                                                </span>
                                            </td>
                                        </tr>
                                    </asp:PlaceHolder>
                                    <tr>
                                        <td width="100%" align="right">
                                            <b>
                                                Order.OrderTotal:</b>
                                        </td>
                                        <td align="right">
                                            <b><span style="white-space: nowrap;">
                                                <asp:Label ID="lblOrderTotal2" runat="server"></asp:Label>
                                            </span></b>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="clear">
        </div>
        <div class="SectionTitle">
            Order.Product(s)</div>
        <div class="clear">
        </div>
        <div class="ProductsBox">
            
        </div>
    </div>
</div>