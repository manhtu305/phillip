﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="AdventureWorks.UserControls.Footer" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1Expander" TagPrefix="C1" %>
<div id="ft">
	<div class="footerinner">
		<C1:C1Expander runat="server" ID="PnlExpFooter" Expanded="false">
			<Header>
						SiteMap
					</Header>
			<Content>
				<asp:PlaceHolder runat="server" ID="SitePld"></asp:PlaceHolder>
			</Content>
		</C1:C1Expander>
	</div>
</div>
<div class="copyright">
	<div class="copyrightinner">
		<div class="copyrighttext">
			<p>
				Studio for ASP.NET Wijmo delivers a complete development toolkit for creating and styling modern Web applications powered by the core wijmo technology. Built with HTML5, jQuery, jQuery Mobile, CSS3, and SVG. Studio for ASP.NET Wijmo controls make your applications ready for today's web.
			</p>
			<p>
				©1987-
                <% =DateTime.Today.Year.ToString() %>
                ComponentOne LLC All Rights Reserved
			</p>
		</div>
		<div class="c1logo">
			<a href="https://www.grapecity.com/en/aspnet-webforms">
				<img src="../Images/c1wijmologo.png" alt="C1" style="border-width: 0px;">
			</a>
		</div>
		<div class="clear">
		</div>
	</div>
</div>
