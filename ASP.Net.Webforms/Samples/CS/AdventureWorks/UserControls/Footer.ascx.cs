﻿using System;
using System.Collections.Generic;
using AdventureWorksDataModel;
using EpicAdventureWorks;
using System.Web.UI.HtmlControls;

namespace AdventureWorks.UserControls
{
	public partial class Footer : System.Web.UI.UserControl
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack) 
			{
				LoadSiteMap();
			}
		}

		void LoadSiteMap() {
			List<ProductCategory> mainCatetories = CategoryManager.GetMainCategories();
			foreach (ProductCategory category in mainCatetories)
			{
				HtmlGenericControl div = new HtmlGenericControl("div");
				SitePld.Controls.Add(div);
				div.Attributes.Add("class", category.Name + "Menu");
				HtmlGenericControl h1 = new HtmlGenericControl("h1");
				HtmlAnchor mainLink = new HtmlAnchor();
				mainLink.InnerText = category.Name;
				mainLink.HRef = this.Page.ResolveUrl("~/Products.aspx?Category=" + category.Name);
				div.Controls.Add(h1);
				h1.Controls.Add(mainLink);
				category.ProductSubcategory.Load();
				HtmlGenericControl ul = new HtmlGenericControl("ul");
				ul.Attributes.Add("class", category.Name + "List");
				div.Controls.Add(ul);
				foreach (ProductSubcategory psub in category.ProductSubcategory) 
				{
					HtmlGenericControl li = new HtmlGenericControl("li");
					HtmlAnchor link = new HtmlAnchor();
					link.InnerText = psub.Name;
					link.HRef = this.Page.ResolveUrl(string.Format("~/Products.aspx?Category={0}&SubCategory={1}", category.Name, psub.Name));
					li.Controls.Add(link);
					ul.Controls.Add(li);
				}
			}
		}
	}
}