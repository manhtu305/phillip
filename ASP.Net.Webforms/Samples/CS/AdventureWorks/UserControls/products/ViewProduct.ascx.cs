﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdventureWorksDataModel;
using C1.Web.Wijmo.Controls.C1Menu;
using EpicAdventureWorks;

namespace AdventureWorks.UserControls.products
{
	public partial class ViewProduct : ProductModule, ICallbackEventHandler
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			SetCookies();
			DoLoad();
			if (!IsPostBack)
			{
				this.Page.ClientScript.GetCallbackEventReference(this, null, null, null);
			}
		}
		protected void AddToCart(object sender, EventArgs e)
		{
			if (CurrentProduct != null)
			{
				ShoppingCartManager.AddToCart(CurrentProduct.ProductID, 1);
			}
			popWin.ShowOnLoad = true;
			//ScriptManager.RegisterStartupScript(this, this.GetType(), "showWindow", "showWindow();", true);
		}

		public void DoLoad()
		{
			if (this.Visible == true)
			{
				if (CurrentProduct != null)
				{
					Product prod = CurrentProduct;
					if (prod != null)
					{
						prod.ProductSubcategoryReference.Load();
						prod.ProductSubcategory.ProductCategoryReference.Load();
						Page.Title += "AdventureWorks Cycles - " + prod.Name;
						BasePage.ClassName = prod.ProductSubcategory.Name.Replace(" ", "") + " " + prod.ProductSubcategory.ProductCategory.Name.Replace(" ", "");
						winProdName.Text = lblProdName.Text = prod.Name;
						lblPrice.Text = prod.ListPrice.ToString("C");
						winProdImg.Src = prodImg.Src = "../../ProductImage.ashx?ProductID=" + HttpUtility.UrlEncode(prod.ProductID.ToString()) + "&size=large";
						hdProdId.Value = prod.ProductID.ToString();
						SetSpecs(prod);
						prod.ProductModelReference.Load();
						if (prod.ProductModel != null)
						{
							prod.ProductModel.ProductModelProductDescriptionCulture.Load();
							if (prod.ProductModel.ProductModelProductDescriptionCulture.Count > 0)
							{
								ProductModelProductDescriptionCulture desc = prod.ProductModel.ProductModelProductDescriptionCulture.First(pd => pd.CultureID.Trim() == "en");
								desc.ProductDescriptionReference.Load();
								lblProdDesc.Text = desc.ProductDescription.Description;
								SetModel(prod.ProductModel);
							}
							else
							{
								lblProdDesc.Visible = false;
							}

						}
						SetRelatedProd();
						SetRecentlyViewedProduct();
						SetWebChart();
					}
				}
			}
		}

		/// <summary>
		/// Set Viewed Product Info into cookies
		/// </summary>
		private void SetCookies()
		{
			string currentProdInfo = CurrentProduct.Name + "_" + CurrentProductCategory.Name + "_" + CurrentProductSubcategory.Name;
			string currentProdId = CurrentProduct.ProductID.ToString();
			if (Request.Cookies["product"] == null)
			{
				HttpCookie cookie = new HttpCookie("product");
				cookie.Expires = DateTime.Now.AddDays(1);
				cookie.Values[currentProdId] = currentProdInfo;
				Response.Cookies.Add(cookie);
			}
			else
			{
				HttpCookie cookie = Request.Cookies["product"];
				cookie.Expires = DateTime.Now.AddDays(1);
				NameValueCollection cookieValue = cookie.Values;
				bool isExistInCookie = false;
				for (int i = 0; i < cookieValue.Count; i++)
				{
					if (cookieValue.AllKeys[i].Equals(currentProdId))
					{
						isExistInCookie = true;
						break;
					}
				}
				if (!isExistInCookie)
					cookie.Values[currentProdId] = currentProdInfo;
				Response.Cookies.Add(cookie);
			}
		}

		/// <summary>
		/// Set Product's Specs Data
		/// </summary>
		/// <param name="prodId"></param>
		/// <param name="panel"></param>
		private void SetSpecs(Product prod)
		{
			if (prod != null)
			{
				StringBuilder sb = new StringBuilder();
				sb.Append("<ul class=\"ProductSpecsContainer\">");
				if (!string.IsNullOrEmpty(prod.Color))
				{
					sb.Append("<li class=\"ProductSpecsColorLabel\">Color:</li>");
					sb.AppendFormat("<li class=\"ProductSpecsColorValue\">{0}</li>", prod.Color);
				}
				if (!string.IsNullOrEmpty(prod.Size))
				{
					sb.Append("<li class=\"ProductSpecsSizeLabel\">Size:</li>");
					prod.SizeUnitMeasureReference.Load();

					sb.AppendFormat("<li class=\"ProductSpecsSizeValue\">{0} {1}</li>", prod.Size, prod.SizeUnitMeasure == null ? "" : prod.SizeUnitMeasure.Name);
				}
				if (prod.Weight != null && prod.Weight > 0)
				{
					sb.Append("<li class=\"ProductSpecsWeightLabel\">Weight:</li>");
					prod.WeightUnitMeasureReference.Load();
					sb.AppendFormat("<li class=\"ProductSpecsWeightValue\">{0} {1}</li>", prod.Weight, prod.WeightUnitMeasure == null ? "" : prod.WeightUnitMeasure.Name);
				}
				sb.Append("</ul>");
				phSpecs.Controls.Add(new LiteralControl(sb.ToString()));
				sb.Remove(0, sb.Length);
			}
		}

		/// <summary>
		/// Set Product's Model Data
		/// </summary>
		/// <param name="prodId"></param>
		/// <param name="panel"></param>
		private void SetModel(ProductModel model)
		{
			if (model != null)
			{
				StringBuilder sb = new StringBuilder();

				sb.Append("<div class=\"ProductModelContainer\">");
				sb.Append("<h1 class=\"ProductModelSizeLabel\">About</h1>");
				sb.AppendFormat("<p class=\"ProductModelDescriptionValue\">{0}</p>", model.CatalogDescription);
				sb.Append("</div>");
				phModel.Controls.Add(new LiteralControl(sb.ToString()));
				sb.Remove(0, sb.Length);
			}
		}

		/// <summary>
		/// Set the Related Product Link
		/// </summary>
		private void SetRelatedProd()
		{
			menuRelated.Items.Clear();
			// ramdom products
			IQueryable<Product> categoryList = ProductManager.GetProductByCategory(CurrentProductSubcategory);
			var items = from ls in categoryList.ToList()
						select new { ls, GID = Guid.NewGuid() };
			var items1 = from i in items
						 orderby i.GID
						 select i.ls;
			List<Product> productcategoryList = items1.Take(5).ToList();
			foreach (Product item in productcategoryList)
			{
				// if the categery is current category not show the category product link 
				if (!(CurrentProduct.ProductID == item.ProductID))
				{
					C1MenuItem menuItem = new C1MenuItem();
					menuItem.Text = item.Name;
					menuItem.NavigateUrl = string.Format("../../Products.aspx?Category={0}&SubCategory={1}&Product={2}", HttpUtility.UrlEncode(CurrentProductCategory.Name), HttpUtility.UrlEncode(CurrentProductSubcategory.Name), HttpUtility.UrlEncode(item.Name));
					menuRelated.Items.Add(menuItem);
				}
			}

		}

		/// <summary>
		/// set the recently viewed link
		/// </summary>
		private void SetRecentlyViewedProduct()
		{
			menuViewed.Items.Clear();
			HttpCookie cookie = Request.Cookies["product"];
			if (cookie != null)
			{
				NameValueCollection cookievalue = cookie.Values;
				string prodName = null;
				string category = null;
				string subcategory = null;
				int valueCnt = cookievalue.Count;
				for (int i = (valueCnt - 1); i >= (valueCnt > 5 ? (valueCnt - 5) : 0); i--)
				{
					if (!cookievalue.AllKeys[i].Equals(CurrentProduct.ProductID.ToString()))
					{
						string[] values = cookievalue[i].Split('_');
						if (values.Length == 3)
						{
							prodName = values[0];
							category = values[1];
							subcategory = values[2];
							C1MenuItem menuItem = new C1MenuItem();
							menuItem.Text = prodName;
							menuItem.NavigateUrl = string.Format("../../Products.aspx?Category={0}&SubCategory={1}&Product={2}", HttpUtility.UrlEncode(category), HttpUtility.UrlEncode(subcategory), HttpUtility.UrlEncode(prodName));
							menuViewed.Items.Add(menuItem);
						}
					}
				}
			}
		}

		/// <summary>
		/// WebChart Show
		/// </summary>
		private void SetWebChart()
		{
			if (CurrentProductCategoryName == "Bikes")
			{
				if (CurrentProductSubcategory != null)
				{
					ChartCategory1.Category = CurrentProductSubcategory.Name;
				}
			}
			else
			{
				statExpander.Visible = false;
				//statExpander.DisplayVisible = false;
			}
		}

		/// <summary>
		/// checkout click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void checkout_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/ShoppingCart.aspx");
		}

		string args;

		public string GetCallbackResult()
		{
			if(args == "addToCart")
			{
				if (CurrentProduct != null)
				{
					ShoppingCartManager.AddToCart(CurrentProduct.ProductID, 1);
				}
				int count = RequestContext.Current.UserShoppingCart.ShoppingCarItems.Count;
				Hashtable data = new Hashtable();
				data.Add("status", "OK");
				data.Add("count", count);
				return C1.Web.Wijmo.Controls.JsonHelper.ObjectToString(data, this.Page);
			}
			throw new NotImplementedException();
		}

		public void RaiseCallbackEvent(string eventArgument)
		{
			args = eventArgument;
		}
	}
}