﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AdventureWorksDataModel;
using C1.Web.Wijmo.Controls.C1Carousel;
using C1.Web.Wijmo.Controls.C1ComboBox;
using EpicAdventureWorks;

namespace AdventureWorks.UserControls.products
{
	public partial class ListProducts : ProductModule, ICallbackEventHandler
	{

		protected void Page_Init(object sender, EventArgs e)
		{
			CboColorPick.SelectedIndexChanged += CboColorPick_SelectedIndexChanged;
			CboWeightPick.SelectedIndexChanged += CboWeightPick_SelectedIndexChanged;
			//SldSize.
		}

		void CboWeightPick_SelectedIndexChanged(object sender, C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxEventArgs args)
		{
			FilterProduct();
		}

		void CboColorPick_SelectedIndexChanged(object sender, C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxEventArgs args)
		{
			FilterProduct();
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				DoLoad();
				callbackRef.Text = Page.ClientScript.GetCallbackEventReference(this, "arg1", "sliderChange", "");
			}
			else 
			{
				if (!string.IsNullOrEmpty(tbSizeValue.Text)) {
					int size = 0;
					int.TryParse(tbSizeValue.Text, out size);
					SldSize.Value = size;
					FilterProduct();
					tbSizeValue.Text = "";
				}
			}
		}

		private void DoLoad()
		{
			if (this.Visible)
			{
				if (CurrentProductCategoryName != null)
				{
					Page.Title += "AdventureWorks Cycles - " + CurrentProductSubcategory.Name;
					BasePage.ClassName += CurrentProductCategory.Name.Replace(" ", "");
					BasePage.ClassName += " " + CurrentProductSubcategory.Name.Replace(" ", "");
					FilterProduct();
				}

				int currentCategoryId = CurrentProductSubcategory.ProductSubcategoryID;
				List<string> colors = ProductManager.GetProductColor(currentCategoryId);

				if (colors != null && colors.Count() > 0)
				{
					CboColorPick.Items.Clear();
					CboColorPick.AppendDataBoundItems = true;
					var ci = new C1ComboBoxItem("All", "All");
					ci.Selected = true;
					CboColorPick.Items.Add(ci);
					CboColorPick.DataSource = colors;
					CboColorPick.DataBind();

				}
				else
				{
					ItmColor.Visible = false;
				}

				List<decimal?> weights = ProductManager.GetProductWeight(currentCategoryId);

				if (weights.Count() > 0)
				{
					CboWeightPick.Items.Clear();
					CboWeightPick.AppendDataBoundItems = true;
					CboWeightPick.Items.Add(new C1ComboBoxItem("All", "All"));
					CboWeightPick.DataSource = weights;
					CboWeightPick.DataBind();
				}
				else
				{
					ItmWeight.Visible = false;
				}

				List<string> sizes = ProductManager.GetProductSize(currentCategoryId);
				int sizeCnt = sizes.Count();
				if (sizeCnt > 0)
				{
					SldSize.Min = 0;
					SldSize.Max = sizeCnt;
					List<string> sizeList = new List<string>();
					sizeList.Add("All");
					foreach (string size in sizes)
					{
						sizeList.Add(size);
					}
					ViewState["SizeList"] = sizeList;
				}
				else
				{
					ItmSize.Visible = false;
				}
			}
			else
			{
				SldSize.Min = 0;
				SldSize.Max = 4;
				List<string> sizeList = new List<string>();
				sizeList.Add("All");
				sizeList.Add("S");
				sizeList.Add("M");
				sizeList.Add("L");
				sizeList.Add("XL");
				ViewState["SizeList"] = sizeList;
			}

		}
		private void FilterProduct()
		{
			IQueryable<Product> prods = ProductManager.GetProductByCategory(CurrentProductSubcategory);
			if (CboColorPick.SelectedItem != null && CboColorPick.SelectedIndex != -1 && CboColorPick.SelectedIndex != 0)
			{
				string currentColor = CboColorPick.SelectedItem.Value;
				prods = prods.Where(p => p.Color == currentColor);
			}
			if (CboColorPick.SelectedItem != null && CboWeightPick.SelectedIndex != -1 && CboWeightPick.SelectedIndex != 0)
			{
				decimal currentWeight = decimal.Parse(CboWeightPick.SelectedItem.Value);
				prods = prods.Where(p => p.Weight == currentWeight);
			}
			if (SldSize.Visible && SldSize.Value != 0)
			{
				if (ViewState["SizeList"] != null)
				{
					string currentSize = (ViewState["SizeList"] as List<string>)[SldSize.Value];
					prods = prods.Where(p => p.Size == currentSize);
					SldSize.ToolTip = currentSize;
					LblSize.Text = "Size: " + ("<span class=\"currentsize\">") + currentSize;
				}
			}
			else if (SldSize.Value == 0)
			{
				LblSize.Text = "Size: <span class=\"currentsize\"> All</span>";
			}

			BindCarousel(prods);
		}
		private void BindCarousel(IQueryable<Product> products)
		{
			ProductCarousel.Items.Clear();

			if (products.Count() > 0)
			{
				int index = 0;
				int rowIndex = 0;
				C1CarouselItem item = null;
				Table table = null;
				TableRow tRow = null;
				foreach (Product prod in products)
				{
					// Multipage,s PageView add 
					if (rowIndex % 4 == 0)
					{
						item = new C1CarouselItem();
						table = new Table();
						table.Style.Add(HtmlTextWriterStyle.Width, "100%");
					}
					if (index % 4 == 0)
					{
						tRow = new TableRow();
						tRow.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
					}
					index++;
					HtmlGenericControl link = new HtmlGenericControl("a");

					link.Attributes.Add("href", "Products.aspx?Category=" + HttpUtility.UrlEncode(CurrentProductCategory.Name) + "&SubCategory=" + HttpUtility.UrlEncode(CurrentProductSubcategory.Name) + "&Product=" + HttpUtility.UrlEncode(prod.Name));
					link.Attributes.Add("class", "ListProductsImage");

					HtmlGenericControl img = new HtmlGenericControl("img");
					img.Attributes.Add("alt", prod.Name);
					link.Attributes.Add("id", prod.ProductID.ToString());
					img.Attributes.Add("src", "ProductImage.ashx?ProductID=" + HttpUtility.UrlEncode(prod.ProductID.ToString()));
					link.Controls.Add(img);

					HtmlGenericControl div = new HtmlGenericControl("div");
					div.InnerText = prod.Name;
					div.Attributes.Add("class", "ListProductHeader");
					TableCell tCell = new TableCell();
					tCell.Attributes.Add("class", "ListProductBlock");
					tCell.Controls.Add(link);
					tCell.Controls.Add(div);
					tRow.Cells.Add(tCell);

					if (index % 4 == 0)
					{
						table.Rows.Add(tRow);
						rowIndex++;
					}
					else if (index == products.Count() && (index % 4) != 0)
					{
						table.Rows.Add(tRow);
					}

					if ((rowIndex % 4 == 0) && (index % 4 == 0))
					{
						item.Controls.Add(table);
						ProductCarousel.Items.Add(item);
					}
					else if (index == products.Count() && (index + 1) % 16 != 0)
					{
						item.Controls.Add(table);
						ProductCarousel.Items.Add(item);
					}
				}
			}
			if (ProductCarousel.Items.Count == 0)
			{
				ProductCarousel.Visible = false;
				PnlEmptyList.Visible = true;
			}
			else
			{
				ProductCarousel.Visible = true;
				PnlEmptyList.Visible = false;
			}
		}


		private IQueryable<Product> GetCallbackData()
		{
			IQueryable<Product> prods = ProductManager.GetProductByCategory(CurrentProductSubcategory);
			if (callbackArgs["color"] != null && callbackArgs["color"].ToString() != "All")
			{
				string color = callbackArgs["color"].ToString();
				prods = prods.Where(p => p.Color == color);
			}

			if (callbackArgs["weight"] != null)
			{
				decimal weight = 0;
				decimal.TryParse(callbackArgs["weight"].ToString(), out weight);
				prods = prods.Where(p => p.Weight == weight);
			}

			if (callbackArgs["size"]!=null)
			{
				int index = 0;
				int.TryParse(callbackArgs["size"].ToString(), out index);
				if (ViewState["SizeList"] != null && index != 0)
				{
					string currentSize = (ViewState["SizeList"] as List<string>)[index];
					prods = prods.Where(p => p.Size == currentSize);
				}
			}

			return prods;
		}

		private string RenderCallbackCarousel(IQueryable<Product> products) 
		{

			if (products.Count() > 0)
			{
				StringBuilder sb = new StringBuilder();
				System.IO.StringWriter sw = new System.IO.StringWriter(sb);
				HtmlTextWriter hw = new HtmlTextWriter(sw);

				int index = 0;
				int rowIndex = 0;
				HtmlGenericControl item = new HtmlGenericControl("li");
				Table table = null;
				TableRow tRow = null;
				foreach (Product prod in products)
				{
					// Multipage,s PageView add 
					if (rowIndex % 4 == 0)
					{
						item = new HtmlGenericControl("li");
						table = new Table();
						table.Style.Add(HtmlTextWriterStyle.Width, "100%");
					}
					if (index % 4 == 0)
					{
						tRow = new TableRow();
						tRow.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
					}
					index++;
					HtmlGenericControl link = new HtmlGenericControl("a");

					link.Attributes.Add("href", "Products.aspx?Category=" + HttpUtility.UrlEncode(CurrentProductCategory.Name) + "&SubCategory=" + HttpUtility.UrlEncode(CurrentProductSubcategory.Name) + "&Product=" + HttpUtility.UrlEncode(prod.Name));
					link.Attributes.Add("class", "ListProductsImage");

					HtmlGenericControl img = new HtmlGenericControl("img");
					img.Attributes.Add("alt", prod.Name);
					link.Attributes.Add("id", prod.ProductID.ToString());
					img.Attributes.Add("src", "ProductImage.ashx?ProductID=" + HttpUtility.UrlEncode(prod.ProductID.ToString()));
					link.Controls.Add(img);

					HtmlGenericControl div = new HtmlGenericControl("div");
					div.InnerText = prod.Name;
					div.Attributes.Add("class", "ListProductHeader");
					TableCell tCell = new TableCell();
					tCell.Attributes.Add("class", "ListProductBlock");
					tCell.Controls.Add(link);
					tCell.Controls.Add(div);
					tRow.Cells.Add(tCell);

					if (index % 4 == 0)
					{
						table.Rows.Add(tRow);
						rowIndex++;
					}
					else if (index == products.Count() && (index % 4) != 0)
					{
						table.Rows.Add(tRow);
					}

					if ((rowIndex % 4 == 0) && (index % 4 == 0))
					{
						item.Controls.Add(table);
						item.RenderControl(hw);
					}
					else if (index == products.Count() && (index + 1) % 16 != 0)
					{
						item.Controls.Add(table);
						item.RenderControl(hw);
					}
				}
				return sb.ToString();
			}

			return string.Empty;

		}


		Hashtable callbackArgs;

		public string GetCallbackResult()
		{
			IQueryable<Product> prods = GetCallbackData();
			Hashtable data = new Hashtable();
			if (callbackArgs["size"] != null)
			{
				data["index"] = callbackArgs["size"];
				data["sliderValue"] = (ViewState["SizeList"] as List<string>)[int.Parse(callbackArgs["size"].ToString())];
			}
			data["carouselData"] = RenderCallbackCarousel(prods);
			return C1.Web.Wijmo.Controls.JsonHelper.ObjectToString(data, this.Page);
		}



		public void RaiseCallbackEvent(string eventArgument)
		{
			if (eventArgument.StartsWith("updataCarousel_"))
			{
				callbackArgs = C1.Web.Wijmo.Controls.JsonHelper.StringToObject(eventArgument.Replace("updataCarousel_", ""));
			}
		}
	}
}