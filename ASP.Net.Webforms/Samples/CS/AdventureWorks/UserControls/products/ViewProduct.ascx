﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewProduct.ascx.cs" Inherits="AdventureWorks.UserControls.products.ViewProduct" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1Expander" TagPrefix="C1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1Menu" TagPrefix="C1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1Dialog" TagPrefix="C1" %>
<%@ Register Src="~/UserControls/products/ChartCategory.ascx" TagPrefix="uc1" TagName="ChartCategory" %>
<div class="yui-gf">
    <div class="yui-u">
        <input type="hidden" id="hdProdId" name="hdProdId" runat="server" />
        <div class="MainProductImage">
            <img id="prodImg" runat="server" alt="Amazing photo of product!" />
        </div>
        <div class="ProductDescription">
            <h1>
                <asp:Label ID="lblProdName" runat="server"></asp:Label></h1>
            <div class="ProductPrice">
                <asp:Label ID="lblPrice" runat="server"></asp:Label>
                <input type="button" value="Buy" class="LoginButton" onclick="addToCart()" />
                <%--<asp:Button ID="btnBuy" CssClass="LoginButton" OnClick="AddToCart" runat="server"
                    Text="Buy" />--%>
            </div>
            <asp:PlaceHolder ID="phSpecs" runat="server"></asp:PlaceHolder>
            <div class="DescriptionContainer">
                <asp:PlaceHolder ID="phModel" runat="server"></asp:PlaceHolder>
                <asp:Label ID="lblProdDesc" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="yui-u first">
        <c1:C1Expander ID="C1Expander1" runat="server" Width="100%">
            <Header>
                <h3>
                    Related Products</h3>
            </Header>
            <Content>
                <c1:C1Menu ID="menuRelated" runat="server"
                    Orientation="Vertical" Height="">
                </c1:C1Menu>
            </Content>
        </c1:C1Expander>
        <c1:C1Expander ID="viewedProd" runat="server"  Width="100%">
            <Header>
                <h3>
                    Recently Viewed</h3>
            </Header>
            <Content>
                <c1:C1Menu ID="menuViewed" runat="server" 
                    Orientation="Vertical">
                </c1:C1Menu>
            </Content>
        </c1:C1Expander>
        <c1:C1Expander ID="statExpander" runat="server" Width="100%" >
            <Header>
                <h3>
                    Overall Stats</h3>
            </Header>
            <Content>
                    <uc1:ChartCategory ID="ChartCategory1" runat="server" />
                
            </Content>
        </c1:C1Expander>
    </div>
    <C1:C1Dialog runat="server" ID ="popWin" Width="580px" Height="420px" Draggable="false" Resizable="true" Modal="true" ShowOnLoad="false" Show="false">
        <Content>
            <div class="WindowShoppingCart">
                <h1 class="ProductMsg">
                    Added to Cart!
                </h1>
                <div class="ProductImage">
                    <img id="winProdImg" runat="server" alt="Product" /></div>
                <h2 class="WindowShoppingCartProductName">
                    <asp:Label ID="winProdName" runat="server"></asp:Label></h2>
            </div>
            <iframe id="ifShopCartItems" class="ShoppingCartFrame" width="100%" marginwidth="0"
                marginheight="0" hspace="0" vspace="0" frameborder="0" scrolling="no" allowtransparency="true">
            </iframe>
            <div class="ButtonContainer">
                <input type="button" id="continue" value="Continue Shopping" class="ButtonContinue" onclick="closeDialog()" />
                <asp:Button ID="checkout" runat="server" Text="Checkout" OnClick="checkout_Click"
                    OnClientClick="closeDialog()" CssClass="ButtonCheckOut" />
            </div>
        </Content>
    </C1:C1Dialog>
    <script type="text/javascript">
        function closeDialog() {
            var dialog = $("#<%= popWin.ClientID %>");
            dialog.c1dialog("close");
        }

        function addToCart() {
            WebForm_DoCallback('ctl00$MainContent$ctl00', "addToCart", function(data) {
                var jsData = __JSONC1.parse(data);
                if (jsData.status === "OK") {
                    $(".hd-ico-cartItems").text(jsData.count);
                var dialog = $("#<%= popWin.ClientID %>");
                $("#ifShopCartItems").attr("src", '<%=Page.ResolveUrl("ShopCart.aspx")%>');
                    dialog.c1dialog("open");
                }
            }, "", null, false);
        }
    </script>
</div>