﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListCategories.ascx.cs" Inherits="AdventureWorks.UserControls.products.ListCategories" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1Carousel" TagPrefix="C1" %>
<%@ Register Src="~/UserControls/products/ChartCategory.ascx" TagPrefix="uc1" TagName="ChartCategory" %>
<%@ Register Src="~/UserControls/Tooltip.ascx" TagPrefix="uc1" TagName="Tooltip" %>


<div class="SubCategoryContainer">
	<asp:Repeater ID="LstCategories" runat="server" OnItemDataBound="LstCategories_ItemDataBound">
        <ItemTemplate>
            <div class="SubCategory">
                <div runat="server" id="chartContainer" class="yui-u C1ChartContainer">
					<uc1:ChartCategory runat="server" ID="ChartCategory" />
                </div>
                <div class="yui-gc">
                    <h3 class="SubCategoryName">
                        <a id="LnkItem" runat="server">
                            <%#Eval("Name") %></a>
                    </h3>
                    <div class="yui-u first">
                       <C1:C1Carousel runat="server" ID="Carousel" Width="950" Height="104" Loop="false" ButtonPosition="Outside">
                       </C1:C1Carousel>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>

	<uc1:Tooltip runat="server" id="Tooltip" TargetSelector=".SubCategory li a" />

</div>