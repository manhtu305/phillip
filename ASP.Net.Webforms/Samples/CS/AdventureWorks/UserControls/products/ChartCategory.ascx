﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChartCategory.ascx.cs" Inherits="AdventureWorks.UserControls.products.ChartCategory" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="C1" %>
<C1:C1BarChart runat="server" ID="BarChart" Horizontal="false" Width="220" Height="110" ChartLabelFormatString="p0">
	<ChartLabelStyle>
		<Fill Color="#0099cc"></Fill>
	</ChartLabelStyle>
	<Axis>
		<X Visible="false">
			<AxisStyle>
				<Fill Color="#0099cc"></Fill>
			</AxisStyle>
			
			<TextStyle StrokeWidth="0" FontWeight="normal" Stroke="#0099cc" FontFamily="Segoe UI, Myriad, Myriad Pro, Calibri, Arial, Sans-Serif">
				<Fill Color="#0099cc"></Fill>
			</TextStyle>
			<Labels>
				<AxisLabelStyle>
					<Fill Color="#0099cc"></Fill>
				</AxisLabelStyle>
			</Labels>
			<GridMajor Visible="false"></GridMajor>

		</X>
		<Y Visible="false"  Min="0" Max="1" Compass="West" TextVisible="false">
			<GridMajor Visible="false"></GridMajor>
		</Y>
	</Axis>
	<Legend Visible="false"></Legend>
	<Header Visible="false"></Header>
	<Footer Visible="false"></Footer>
	<Hint Enable="false"></Hint>
	<SeriesStyles>
		<C1:ChartStyle StrokeWidth="0">
			<Fill Color="#0099cc"></Fill>
		</C1:ChartStyle>
	</SeriesStyles>
	<SeriesHoverStyles>
		<C1:ChartStyle StrokeWidth="0"></C1:ChartStyle>
	</SeriesHoverStyles>
</C1:C1BarChart>