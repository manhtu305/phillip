﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.C1Chart;

namespace AdventureWorks.UserControls.products
{
	public partial class ChartCategory : System.Web.UI.UserControl
	{

		public string Category
		{
			get
			{
				if (ViewState["Chartcat"] != null)
				{
					return ViewState["Chartcat"].ToString();
				}
				else
				{
					return null;
				}
			}
			set
			{
				ViewState["Chartcat"] = value;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				DoLoad();
			}
		}

		public void DoLoad()
		{
			BarChartSeries series = new BarChartSeries();
			if (Category == "Mountain Bikes")
			{
				series.Data.Y.AddRange(new double[] { 1, 0.9, 0.2, 0.4 });
			}
			else if (Category == "Road Bikes")
			{
				series.Data.Y.AddRange(new double[] { 0.2, 0.3, 0.8, 0.6 });
			}
			else if (Category == "Touring Bikes")
			{
				series.Data.Y.AddRange(new double[] { 0.1, 0.1, 0.7, 1 });
			}
			if (series.Data.Y.Values.Count > 0)
			{
				series.Data.X.AddRange(new string[] { "Speed", "Rugged", "Smooth", "Shocks" });
				BarChart.SeriesList.Add(series);
			}
			else {
				this.Visible = false;
			}
		}
	}
}