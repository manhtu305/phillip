﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AdventureWorksDataModel;
using C1.Web.Wijmo.Controls.C1Carousel;
using EpicAdventureWorks;

namespace AdventureWorks.UserControls.products
{
	public partial class ListCategories : ProductModule
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				LoadData();
			}
		}

		public void LoadData() 
		{
			if (Visible)
			{
				if (CurrentProductCategory != null)
				{
					Page.Title += "AdventureWorks Cycles - " + CurrentProductCategory.Name;
					BasePage.ClassName += CurrentProductCategory.Name.Replace(" ", "");
					CurrentProductCategory.ProductSubcategory.Load();
					var random = from sub in CurrentProductCategory.ProductSubcategory
								 select new { sub, GID = Guid.NewGuid() };
					var random1 = from sub in random
								  orderby sub.GID
								  select sub.sub;
					List<ProductSubcategory> subCats = random1.Take(7).OrderBy(c=>c.Name).ToList();
					LstCategories.DataSource = subCats;
					LstCategories.DataBind();
				}
			}
		}

		/// <summary>
		/// Handles the ItemDataBound event of the LstCategories control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.Web.UI.WebControls.RepeaterItemEventArgs"/> instance containing the event data.</param>
		protected void LstCategories_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.DataItem != null)
			{
				HtmlAnchor a = (HtmlAnchor)e.Item.FindControl("LnkItem");
				ProductSubcategory subCat = (ProductSubcategory)e.Item.DataItem;
				a.HRef = "../../Products.aspx?Category=" + subCat.ProductCategory.Name + "&SubCategory=" + HttpUtility.UrlEncode(subCat.Name);

				ChartCategory uc = (ChartCategory)e.Item.FindControl("ChartCategory");
				HtmlGenericControl webChartContainer = (HtmlGenericControl)e.Item.FindControl("chartContainer");
				C1Carousel carousel = (C1Carousel)e.Item.FindControl("Carousel");

				IQueryable<Product> prods = ProductManager.GetProductByCategory(subCat);
				if (prods.Count() > 0)
				{
					C1CarouselItem item = new C1CarouselItem();
					int index = 0;
					//C1PageView c1Page = null;
					foreach (Product prod in prods)
					{
						// Multipage,s PageView add 
						if (index % 7 == 0)
							item = new C1CarouselItem();

						//item.Style.Add(HtmlTextWriterStyle.Height, "104px");

						index++;
						HtmlGenericControl link = new HtmlGenericControl("a");						
						link.Attributes.Add("href", "Products.aspx?Category=" + HttpUtility.UrlEncode(subCat.ProductCategory.Name) + "&SubCategory=" + HttpUtility.UrlEncode(subCat.Name) + "&Product=" + HttpUtility.UrlEncode(prod.Name));
						HtmlGenericControl img = new HtmlGenericControl("img");
						img.Attributes.Add("class", "SubCategoryChart");
						img.Attributes.Add("alt", prod.Name);
						link.Attributes.Add("id", "product_" + prod.ProductID.ToString());
						img.Attributes.Add("src", "ProductImage.ashx?ProductID=" + HttpUtility.UrlEncode(prod.ProductID.ToString()));
						link.Controls.Add(img);
						item.Controls.Add(link);

						if (index % 7 == 0)
							carousel.Items.Add(item);
						else if (index == prods.Count() && index % 7 != 0)
							carousel.Items.Add(item);

					}
					if (CurrentProductCategory.Name == "Bikes")
					{
						uc.Visible = true;
						uc.Category = subCat.Name;
					}
				}
				
			}
		}
	}
}