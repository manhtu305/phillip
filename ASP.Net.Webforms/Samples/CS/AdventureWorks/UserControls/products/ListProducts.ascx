﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListProducts.ascx.cs" Inherits="AdventureWorks.UserControls.products.ListProducts" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1Carousel" TagPrefix="C1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="C1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.3" Namespace="C1.Web.Wijmo.Controls.C1Slider" TagPrefix="C1" %>
<%@ Register Src="~/UserControls/Tooltip.ascx" TagPrefix="uc1" TagName="Tooltip" %>
<asp:TextBox ID="callbackRef" style="display:none" runat="server"></asp:TextBox>
<div class="yui-gf">
            <div class="yui-u">
                <C1:C1Carousel runat="server" ID="ProductCarousel" Loop="false" ButtonPosition="Outside" Height="555"></C1:C1Carousel>
                <asp:Panel ID="PnlEmptyList" runat="server" Visible="false">
                    <h3>
                        Out of Stock</h3>
                    <p>
                        Sorry, we do not have any products that match your criteria. Please make a different selection or try back again soon!</p>
                </asp:Panel>
            </div>
            <div class="yui-u first">
                <fieldset>
                    <legend>Find a Product</legend>
                    <div class="Filter">
                        <div id="ItmColor" runat="server">
                            <h3>
                                <asp:Label class="C1ColorComboLabel" ID="LblColor" runat="server" AssociatedControlID="CboColorPick" Text="Color"></asp:Label></h3>
                            <div class="C1ColorComboContainer">
                                <c1:C1ComboBox CssClass="C1ColorCombo" ID="CboColorPick" runat="server" DropDownResizable="true" OnClientSelectedIndexChanged="colorChanged" Width="150px">
                                </c1:C1ComboBox>
                            </div>
                        </div>
                        <h3>
                            <asp:Label class="C1SizeSliderLabel" ID="LblSize" runat="server" Text="Size"></asp:Label></h3>
                        <div id="ItmSize" runat="server" class="C1SizeSliderContainer">
                            <c1:C1Slider CssClass="C1SizeSlider" ID="SldSize" runat="server" Height="30px">
                            </c1:C1Slider>
							<asp:TextBox style ="display:none" runat="server" ID ="tbSizeValue"></asp:TextBox>
                        </div>
                        <h3 id="hWeight" runat="server" visible="false">
                            <asp:Label class="C1WeightComboLabel" ID="LblWeight" runat="server" AssociatedControlID="CboWeightPick" Text="Weight"></asp:Label></h3>
                        <div id="ItmWeight" runat="server" class="C1WeightComboContainer" visible="false">
                            <c1:C1ComboBox CssClass="C1WeightCombo" ID="CboWeightPick" runat="server" AutoPostBack="True" Width="150px" DropDownResizable="true">
                            </c1:C1ComboBox>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>

<uc1:Tooltip runat="server" ID="Tooltip1" TargetSelector="#ctl00_MainContent_ctl00_ProductCarousel .ListProductsImage" />

<script type="text/javascript">

	var callbackData = {};

	function colorChanged(e, data) {
		callbackData.color = data.selectedItem.value;
		doCallback();
	}

	function updateCarousel(data) {
		var carousel = $("#<%= ProductCarousel.ClientID %>")
		carousel.c1carousel("destroy");
		var ul = carousel.find("ul").empty();
		ul.html(data);
		carousel.c1carousel({
			loop: false,
			buttonPosition: "outside"
		});

		//update the tooltip
		$("#ctl00_MainContent_ctl00_ProductCarousel .ListProductsImage").c1tooltip({
			position: {
				my: 'center bottom', at: 'center top'
			},
			enableCallBackMode: true,
			id: "ctl00_MainContent_ctl00_Tooltip1_Tooltip1",
			uniqueID: "ctl00$MainContent$ctl00$Tooltip1$Tooltip1",
			targetSelector: ".wijmo-wijcarousel-item .ListProductsImage"
		});
	}

	function doCallback() {
		var args = __JSONC1.stringify(callbackData),
			sliderlabel = $("#<%= LblSize.ClientID %>");;
		WebForm_DoCallback('ctl00$MainContent$ctl00', "updataCarousel_" + args, function (data) {
			var jsData = __JSONC1.parse(data),
				labelStr = "All";
			updateCarousel(jsData.carouselData);
			if (jsData.sliderValue !== undefined && jsData.sliderValue !== 0) {
				labelStr = jsData.sliderValue;
			}
			sliderlabel.html("Size: <span class=\"currentsize\"> " + labelStr + "</span>");
		}, "", null, false)
	}

	$(document).ready(function () {
		var slider = $("#<%= SldSize.ClientID %>");
		// in some case the slider is not on the page
		if (slider.c1slider) {
			slider.c1slider({
				change: function (e, data) {
					callbackData.size = data.value;
					doCallback();
				}
			});
		}
		$("#ctl00_MainContent_ctl00_ProductCarousel .ListProductsImage").c1tooltip({
			beforeAjaxCallback: function (ele) {
				var widget = this.data("wijmo-c1tooltip");
				widget.options.content = "";
				
				return this.attr("id");
			}
		});


		$("#ctl00_MainContent_ctl00_ProductCarousel .ListProductsImage img").bind("mouseover", function (e) {
			e.stopPropagation();
		})

		$("#ctl00_MainContent_ctl00_ProductCarousel .ListProductsImage").bind("mouseleave", function (e) {
			$(this).c1tooltip("hide");
			hideTooltip = true;
		});
		
	});

	function showTooltip() {
		if (hideTooltip) {
			//$(this).c1tooltip("hide");
			//hideTooltip = false;
		}
	}

	function hidingTooltip() {
		console.log("hide");
	}

	var hideTooltip = false;
</script>