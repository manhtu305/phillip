﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerLogin.ascx.cs" Inherits="AdventureWorks.UserControls.CustomerLogin" %>
<%@ Register TagPrefix="wijmo" Namespace="C1.Web.Wijmo.Controls.C1Input" Assembly="C1.Web.Wijmo.Controls.3" %>
<div class="RegisterBlock">
    <fieldset class="register">
        <legend class="RegisterTitle">Register</legend>
        <ul>
            <li>
                <asp:Label ID="lblUserEmail" runat="server" AssociatedControlID="txtUserEmail">Email:</asp:Label>
            </li>
            <li>
				<wijmo:C1InputMask ID="txtUserEmail" runat="server" FormatMode="Advanced" MaskFormat="[\H^@ ]+@[\H^@ ]+"  CssClass="TextBox UserEmailTextBox" HidePromptOnLeave="True" HighlightText="All">
				</wijmo:C1InputMask>
                <asp:RequiredFieldValidator ID="UserNameRequired"  InitialValue="_@_" runat="server" ControlToValidate="txtUserEmail" ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="SignupUser">*</asp:RequiredFieldValidator>
            </li>
            <li>
                <asp:Label ID="lblPassword" runat="server" AssociatedControlID="txtPassword">Password:</asp:Label>
            </li>
            <li>
				<wijmo:C1InputText ID="txtPassword" runat="server" PasswordChar="*" CssClass="TextBox PasswordTextBox"  MaxLength="50">
		</wijmo:C1InputText>
                <asp:RequiredFieldValidator ID="txtPasswordRequired" runat="server" ControlToValidate="txtPassword" ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="SignupUser">*</asp:RequiredFieldValidator>
            </li>
            <li>
                <asp:Label ID="lblConfirmPassword" runat="server" AssociatedControlID="txtConfirmPassword">Confirm Password:</asp:Label>
            </li>
            <li>
				<wijmo:C1InputText ID="txtConfirmPassword" runat="server" PasswordChar="*" CssClass="TextBox ConfirmPasswordTextBox"  MaxLength="50">
		</wijmo:C1InputText>
                <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" runat="server" ControlToValidate="txtConfirmPassword" ErrorMessage="Confirm Password is required." ToolTip="Confirm Password is required." ValidationGroup="SignupUser">*</asp:RequiredFieldValidator>
            </li>
            <li>
                <asp:Label ID="lblFirstName" runat="server" AssociatedControlID="txtFirstName">First Name:</asp:Label>
            </li>
            <li>
                <wijmo:C1InputText ID="txtFirstName" runat="server" CssClass="TextBox FirstNameTextBox"></wijmo:C1InputText>
                <asp:RequiredFieldValidator ID="FirstNameRequired" runat="server" ControlToValidate="txtFirstName" ErrorMessage="First name is required." ToolTip="First name is required." ValidationGroup="SignupUser">*</asp:RequiredFieldValidator>
            </li>
            <li>
                <asp:Label ID="lblLastName" runat="server" AssociatedControlID="txtLastName">Last Name:</asp:Label>
            </li>
            <li>
                <wijmo:C1InputText ID="txtLastName" runat="server" CssClass="TextBox LastNameTextBox"></wijmo:C1InputText>
                <asp:RequiredFieldValidator ID="LastNameRequired" runat="server" ControlToValidate="txtLastName" ErrorMessage="Last name is required." ToolTip="Last name is required." ValidationGroup="SignupUser">*</asp:RequiredFieldValidator>
            </li>
            <li>
                <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword" Display="Dynamic" ErrorMessage="The Password and Confirmation Password must match." ValidationGroup="SignupUser"></asp:CompareValidator>
            </li>
			<li>
				<asp:Label ID="lblErrorMessage" runat="server" EnableViewState="False"></asp:Label>
			</li>
        </ul>
    </fieldset>
    <div class="buttonbox">
        <asp:Button ID="btnSignUp" CssClass="SignUpButton" OnClick="SignUp" runat="server" Text="Sign Up" ValidationGroup="SignupUser"/>
    </div>
</div>
<asp:Panel ID="pnlLogin" runat="server" CssClass="LoginBlock" DefaultButton="LoginButton">
    <fieldset class="login">
        <legend class="LoginTitle">Login</legend>
        <ul>
            <li>
                <asp:Label runat="server" ID="lblLoginEmail" Text="Email: (Enter: info@componentone.com)" AssociatedControlID="UserName" />
            </li>
            <li>
                <wijmo:C1InputMask ID="UserName" runat="server" CssClass="TextBox LoginEmailTextBox" Text="info@componentone.com"></wijmo:C1InputMask>
                <asp:RequiredFieldValidator ID="LoginUserNameRequired" runat="server" ControlToValidate="UserName" ErrorMessage="Username is required." ToolTip="Username is required." ValidationGroup="LoginForm">*</asp:RequiredFieldValidator>
            </li>
            <li>
                <asp:Label runat="server" ID="LoginlblPassword" Text="Password: (Enter: pass)" AssociatedControlID="Password" />
            </li>
            <li>
				<wijmo:C1InputText ID="Password" runat="server" PasswordChar="*" CssClass="TextBox LoginPasswordTextBox" MaxLength="50">
		</wijmo:C1InputText>
                <asp:RequiredFieldValidator ID="LoginPasswordRequired" runat="server" ControlToValidate="Password" ErrorMessage="Password Required" ToolTip="Password Required" ValidationGroup="LoginForm">*</asp:RequiredFieldValidator>
            </li>
            <li>
                <asp:Label ID="FailureText" runat="server" EnableViewState="False" />
            </li>
        </ul>
    </fieldset>
    <div class="buttonbox">
        <asp:CheckBox ID="RememberMe" runat="server" Checked="true" /><asp:Label ID="lblRemember" runat="server">Remember Me</asp:Label>
        <asp:Button UseSubmitBehavior="true" ID="LoginButton" CssClass="LoginButton" OnClick="Login" runat="server" Text="Login" ValidationGroup="LoginForm" />
    </div>
</asp:Panel>
