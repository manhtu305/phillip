﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AdventureWorksDataModel;
using C1.Web.Wijmo.Controls.C1Menu;
using C1.Web.Wijmo.Controls.C1Tabs;
using EpicAdventureWorks;


namespace AdventureWorks
{
	public partial class Products : ProductPage
	{

		/// <summary>
		/// Handles the Init event of the Page control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
		protected void Page_Init(object sender, EventArgs e)
		{
			if (CurrentProduct != null)
			{
				PnlLoader.Controls.Add(LoadControl("~/UserControls/Products/ViewProduct.ascx"));
				return;
			}
			if (CurrentProductSubcategory != null)
			{
				PnlLoader.Controls.Add(LoadControl("~/UserControls/Products/ListProducts.ascx"));
				return;
			}
			if (CurrentProductCategory != null)
			{
				PnlLoader.Controls.Add(LoadControl("~/UserControls/Products/ListCategories.ascx"));
				return;
			}
			Response.Redirect("~/Default.aspx");
		}
		
	}
}