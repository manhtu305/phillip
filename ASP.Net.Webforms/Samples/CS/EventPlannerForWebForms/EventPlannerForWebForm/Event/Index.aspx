﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/ReadOnlyPage.Master" CodeBehind="Index.aspx.cs" Inherits="EventPlannerForWebForm.Event.Index" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ListView" TagPrefix="wijmo" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="AppViewPageHeader" runat="server">
    <a href="../Main.aspx" data-icon="back">Back</a>
    <h2>List View</h2>
    <a href="Create.aspx" data-icon="plus" data-iconpos="notext">Add</a>
</asp:Content>

<asp:Content ID="ContentContent" ContentPlaceHolderID="AppViewPageContent" runat="server">
    <wijmo:C1ListView ID="C1ListView1" runat="server" Inset="true">
    </wijmo:C1ListView>
</asp:Content>
