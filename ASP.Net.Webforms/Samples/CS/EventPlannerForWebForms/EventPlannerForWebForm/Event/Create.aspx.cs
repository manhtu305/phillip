﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventPlannerForWebForm.Models;

namespace EventPlannerForWebForm.Event
{
	public partial class Create : System.Web.UI.Page
	{
		protected void Page_PreRenderComplete(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				EventObj newEvent = EventAction.Create();
                SubjectInput.Text = newEvent.Subject;
                LocationInput.Text = newEvent.Location;
                StartInput.Text = newEvent.Start.ToString("G");
                EndInput.Text = newEvent.End.ToString("G");
                DescriptionInput.Text = newEvent.Description;
                AllDayInput.SelectedON = newEvent.AllDay;

				createForm.Action = "Event/Create.aspx";
			}
			else
			{
				EventObj newEvent = EventAction.Create();
                newEvent.Subject = SubjectInput.Text;
                newEvent.Location = LocationInput.Text;
                newEvent.Start = DateTime.Parse(StartInput.Text);
                newEvent.End = DateTime.Parse(EndInput.Text);
                newEvent.Description = DescriptionInput.Text;
                newEvent.AllDay = AllDayInput.SelectedON;

				EventAction.Add(newEvent);

				Response.Redirect("../Main.aspx#appviewpage=Event/Index.aspx");
			}
		}
	}
}