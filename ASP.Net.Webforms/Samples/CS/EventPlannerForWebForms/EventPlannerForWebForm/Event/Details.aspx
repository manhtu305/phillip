﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/ReadOnlyPage.Master" CodeBehind="Details.aspx.cs" Inherits="EventPlannerForWebForm.Event.Details" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ListView" TagPrefix="wijmo" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="AppViewPageHeader" runat="server">
    <a href="Index.aspx" data-icon="back">Back</a>
    <h2>Details</h2>
    <a href="Edit.aspx?id=<%=id %>" data-icon="gear">Edit</a>
</asp:Content>
<asp:Content ID="ContentContent" ContentPlaceHolderID="AppViewPageContent" runat="server">
        <wijmo:C1ListView ID="C1ListView1" runat="server" Inset="true">
            <Items>
                <wijmo:C1ListViewInputItem ID="SubjectInput" LabelText="Subject" Type="label"></wijmo:C1ListViewInputItem>
                <wijmo:C1ListViewInputItem ID="LocationInput" LabelText="Location" Type="label"></wijmo:C1ListViewInputItem>
                <wijmo:C1ListViewInputItem ID="StartInput" LabelText="Start" Type="label"></wijmo:C1ListViewInputItem>
                <wijmo:C1ListViewInputItem ID="EndInput" LabelText="End" Type="label"></wijmo:C1ListViewInputItem>
                <wijmo:C1ListViewInputItem ID="DescriptionInput" LabelText="Description" Type="label"></wijmo:C1ListViewInputItem>
                <wijmo:C1ListViewFlipSwitchItem ID="AllDayInput" LabelText="AllDay" Disable="true" ONMessage="Yes" ONValue="true" OFFMessage="No" OFFValue="false"></wijmo:C1ListViewFlipSwitchItem>
            </Items>
        </wijmo:C1ListView>
        <a href="Delete.aspx?id=<%=id %>" data-role="button" data-theme="e">Delete</a>
</asp:Content>