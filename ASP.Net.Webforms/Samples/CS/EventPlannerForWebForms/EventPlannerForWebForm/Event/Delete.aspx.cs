﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventPlannerForWebForm.Models;

namespace EventPlannerForWebForm.Event
{
	public partial class Delete : System.Web.UI.Page
	{
		protected string id;
		protected void Page_PreRenderComplete(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				id = Request["id"];
				EventObj detail = EventAction.GetEventDetail(id);
				SubjectInput.Text = detail.Subject;

				deleteForm.Action = "Event/Delete.aspx?id=" + id.ToString();
			}
			else
			{
				id = Request["id"];

				EventAction.Delete(id);

				Response.Redirect("../Main.aspx#appviewpage=Event/Index.aspx");
			}
		}
	}
}