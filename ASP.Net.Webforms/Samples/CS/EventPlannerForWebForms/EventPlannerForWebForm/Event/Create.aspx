﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/EditablePage.Master" CodeBehind="Create.aspx.cs" Inherits="EventPlannerForWebForm.Event.Create" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ListView" TagPrefix="wijmo" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="AppViewPageHeader" runat="server">
    <h2>Create</h2>
    <input type="button" value="Save" class="ui-btn-right" data-theme="b" onclick="$('#createForm').trigger('submit')" />
</asp:Content>
<asp:Content ID="ContentContent" ContentPlaceHolderID="AppViewPageContent" runat="server">
    <form id="createForm" runat="server">
        <wijmo:C1ListView ID="C1ListView1" runat="server" Inset="true">
            <Items>
                <wijmo:C1ListViewInputItem ID="SubjectInput" LabelText="Subject" Type="text"></wijmo:C1ListViewInputItem>
                <wijmo:C1ListViewInputItem ID="LocationInput" LabelText="Location" Type="text"></wijmo:C1ListViewInputItem>
                <wijmo:C1ListViewInputItem ID="StartInput" LabelText="Start" Type="text"></wijmo:C1ListViewInputItem>
                <wijmo:C1ListViewInputItem ID="EndInput" LabelText="End" Type="text"></wijmo:C1ListViewInputItem>
                <wijmo:C1ListViewInputItem ID="DescriptionInput" LabelText="Description" Type="text"></wijmo:C1ListViewInputItem>
                <wijmo:C1ListViewFlipSwitchItem ID="AllDayInput" LabelText="AllDay" ONMessage="Yes" ONValue="true" OFFMessage="No" OFFValue="false"></wijmo:C1ListViewFlipSwitchItem>
            </Items>
        </wijmo:C1ListView>
    </form>
</asp:Content>