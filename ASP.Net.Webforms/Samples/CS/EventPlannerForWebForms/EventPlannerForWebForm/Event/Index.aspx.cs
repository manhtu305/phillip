﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.C1ListView;
using EventPlannerForWebForm.Models;

namespace EventPlannerForWebForm.Event
{
	public partial class Index : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				foreach (EventObj item in EventAction.GetEvents())
				{
                    C1ListViewLinkItem listItem = new C1ListViewLinkItem();
					listItem.Text = item.Subject;
					listItem.NavigateUrl = "~/Event/Details.aspx?id=" + item.Id;

					C1ListView1.Items.Add(listItem);
				}
			}
		}
	}
}