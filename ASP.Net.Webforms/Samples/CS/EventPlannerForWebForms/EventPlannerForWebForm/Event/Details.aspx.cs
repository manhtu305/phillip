﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventPlannerForWebForm.Models;

namespace EventPlannerForWebForm.Event
{
	public partial class Details : System.Web.UI.Page
	{
		protected string id;
		protected void Page_PreRenderComplete(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				id = Request["id"];
				EventObj eventDetail = EventAction.GetEventDetail(id);
                SubjectInput.Text = eventDetail.Subject;
                LocationInput.Text = eventDetail.Location;
                StartInput.Text = eventDetail.Start.ToString("G");
                EndInput.Text = eventDetail.End.ToString("G");
                DescriptionInput.Text = eventDetail.Description;
                AllDayInput.SelectedON = eventDetail.AllDay;
			}
		}
	}
}