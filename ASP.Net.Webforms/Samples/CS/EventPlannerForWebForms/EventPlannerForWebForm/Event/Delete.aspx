﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/EditablePage.Master" CodeBehind="Delete.aspx.cs" Inherits="EventPlannerForWebForm.Event.Delete" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ListView" TagPrefix="wijmo" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="AppViewPageHeader" runat="server">
    <h2>Delete</h2>
</asp:Content>
<asp:Content ID="ContentContent" ContentPlaceHolderID="AppViewPageContent" runat="server">
    <form id="deleteForm" runat="server">
        <wijmo:C1ListView ID="C1ListView1" runat="server" Inset="true">
            <Items>
                <wijmo:C1ListViewInputItem ID="SubjectInput" LabelText="Subject" Type="label"></wijmo:C1ListViewInputItem>
            </Items>
        </wijmo:C1ListView>
        <input type="button" value="Delete" data-theme="e" onclick="$('#deleteForm').trigger('submit')" />
        <a href="Index.aspx" data-theme="c" data-role="button">Cancel</a>
        </form>
</asp:Content>
