﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventPlannerForWebForm.Models;

namespace EventPlannerForWebForm.Event
{
	public partial class Edit : System.Web.UI.Page
	{
		private EventObj detail;
		protected string id;
		protected void Page_PreRenderComplete(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				id = Request["id"];
                detail = EventAction.GetEventDetail(id);
                SubjectInput.Text = detail.Subject;
                LocationInput.Text = detail.Location;
                StartInput.Text = detail.Start.ToString("G");
                EndInput.Text = detail.End.ToString("G");
                DescriptionInput.Text = detail.Description;
                AllDayInput.SelectedON = detail.AllDay;

				editForm.Action = "Event/Edit.aspx?id=" + id.ToString();
			}
			else
			{
				id = Request["id"];
                detail = EventAction.GetEventDetail(id);
                detail.Subject = SubjectInput.Text;
                detail.Location = LocationInput.Text;
                detail.Start = DateTime.Parse(StartInput.Text);
                detail.End = DateTime.Parse(EndInput.Text);
                detail.Description = DescriptionInput.Text;
                detail.AllDay = AllDayInput.SelectedON;

				EventAction.Edit(detail);

				Response.Redirect("../Main.aspx#appviewpage=Event/Index.aspx");
			}
		}
	}
}