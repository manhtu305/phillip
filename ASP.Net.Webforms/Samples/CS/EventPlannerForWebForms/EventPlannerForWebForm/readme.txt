ASP.NET WebForms EventPlanner App
-------------------------------------------------------------------------------
An EventPlanner sample app that fully utilizes the Web Forms Mobile Controls.

EventPlanner is built using the latest in .NET 4 technology and C1AppView Control. 
It uses the ASP.NET 4 Web Application Project Template, Entity Framework CodeFirst, 
C1AppView and more. See how you can build responsive mobile applications in 
WebForms using C1AppView and C1ListView.