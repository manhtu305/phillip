﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="EventPlannerForWebForm.Main" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1AppView" TagPrefix="wijmo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<link type="text/css" rel="stylesheet" href="Site.css" />
</head>
<body>
	<form id="form1" runat="server">
    <div data-role="page">
		<wijmo:C1AppView ID="C1AppView1" runat="server" HeaderTitle="Home Page">
			<LoadSettings Type="Get" LoadMsgDelay="100" ReloadPage="False" />
			<DefaultContent>
				<p>
					This application was built with Wijmo, jQuery Mobile. Learn more about Wijmo at <a href="http://wijmo.com">http://wijmo.com</a>.
				</p>
			</DefaultContent>
			<Items>
				<wijmo:C1AppViewItem Text="List view" AppViewPageUrl="~/Event/Index.aspx"></wijmo:C1AppViewItem>
				<wijmo:C1AppViewItem Text="Calendar view" AppViewPageUrl="~/Calendar/Index.aspx"></wijmo:C1AppViewItem>
				<wijmo:C1AppViewItem Text="About" AppViewPageUrl="~/Home/About.aspx"></wijmo:C1AppViewItem>
				<wijmo:C1AppViewItem Text="Contact" AppViewPageUrl="~/Home/Contact.aspx"></wijmo:C1AppViewItem>
			</Items>
		</wijmo:C1AppView>
    </div>
	</form>
</body>
</html>