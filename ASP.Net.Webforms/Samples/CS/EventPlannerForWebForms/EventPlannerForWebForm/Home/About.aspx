﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="EventPlannerForWebForm.Home.About" %>

<div data-role="appviewpage" data-title="About">
	<div data-role="header"><h2>About</h2></div>
	<div data-role="content">
		<article>
			<p>
				Use this area to provide additional information.
			</p>
		</article>
		<aside>
			<h3>Aside Title</h3>
			<p>
				Use this area to provide additional information.
			</p>
			<ul>
				<li><a href="Index.aspx">Home</a></li>
				<li><a href="About.aspx">About</a></li>
				<li><a href="Contact.aspx">Contact</a></li>
			</ul>
		</aside>
	</div>
</div>