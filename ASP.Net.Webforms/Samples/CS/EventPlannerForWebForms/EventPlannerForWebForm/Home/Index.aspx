﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="EventPlannerForWebForm.Home.Index" %>

<div data-role="appviewpage" data-title="Index">
	<div data-role="header"><h2>Home Page</h2></div>
	<div data-role="content">
		<p>
			This application was built with Wijmo, jQuery Mobile. Learn more about Wijmo at <a href="http://wijmo.com">http://wijmo.com</a>.
		</p>
	</div>
</div>