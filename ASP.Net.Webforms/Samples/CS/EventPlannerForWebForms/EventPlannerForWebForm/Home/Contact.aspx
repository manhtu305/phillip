﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="EventPlannerForWebForm.Home.Contact" %>
<div data-role="appviewpage" data-title="Contact">
	<div data-role="header"><h2>Contact</h2></div>
	<div data-role="content">
		<h3>Phone</h3>
		<span>425.555.0100</span>

		<h3>Email</h3>
		<span><a href="mailto:General@example.com">General@example.com</a></span>
	</div>
</div>
