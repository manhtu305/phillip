﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/ReadOnlyPage.Master" CodeBehind="Index.aspx.cs" Inherits="EventPlannerForWebForm.Calendar.Index" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar" TagPrefix="cc1" %>

<asp:Content ID="DataSource" ContentPlaceHolderID="DataSourceCount" runat="server">
    <asp:EntityDataSource ID="EventPlannerEntityDataSource" runat="server"
        OnContextCreating="EventPlannerEntityDataSource_ContextCreating"
        EnableDelete="true" EnableInsert="true" EnableUpdate="true"
        EntitySetName="Events">
    </asp:EntityDataSource>
</asp:Content>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="AppViewPageHeader" runat="server">
    <a href="../Main.aspx" data-icon="back">Back</a>
    <h2>Event Calendar</h2>
</asp:Content>

<asp:Content ID="ContentContent" ContentPlaceHolderID="AppViewPageContent" runat="server">
    <cc1:C1EventsCalendar ID="C1EventsCalendar1" runat="server">
        <DataStorage>
            <EventStorage DataSourceID="EventPlannerEntityDataSource">
                <Mappings>
                    <IdMapping MappingName="Id" />
                    <SubjectMapping MappingName="Subject" />
                    <StartMapping MappingName="Start" />
                    <EndMapping MappingName="End" />
                    <LocationMapping MappingName="Location" />
                    <DescriptionMapping MappingName="Description" />
                </Mappings>
            </EventStorage>
        </DataStorage>
    </cc1:C1EventsCalendar>
</asp:Content>