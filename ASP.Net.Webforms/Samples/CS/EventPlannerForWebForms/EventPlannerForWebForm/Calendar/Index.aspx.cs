﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EventPlannerForWebForm.Models;

namespace EventPlannerForWebForm.Calendar
{
	public partial class Index : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
		}

		protected void EventPlannerEntityDataSource_ContextCreating(object sender, EntityDataSourceContextCreatingEventArgs e)
		{
			var db = EventAction.GetEventDb();
			e.Context = (db as IObjectContextAdapter).ObjectContext;
		}
	}
}