//ver:[120317]
<%@ WebHandler Language="C#" Class="reportService" %>


using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

using C1.Web.Wijmo.Controls;using C1.Web.Wijmo.Controls.C1EventsCalendar;

/*EVCAL CONTROL:
using C1.Web.Wijmo.Controls;
using C1.Web.Wijmo.Controls.C1EventsCalendar;
*/

/*EVCAL EXTENDER:
using C1.Web.Wijmo.Extenders;
using C1.Web.Wijmo.Extenders.C1EventsCalendar;
*/

using System.IO;
using System.Xml;
using System.Globalization;
using System.Text;

public class reportService : IHttpHandler
{

	private C1EventsCalendarStorage _dataStorage;

	public void ProcessRequest(HttpContext context)
    {
			
			object answer;
			Hashtable eventData, calendarData;
			NameValueCollection args = context.Request.Params;
			string clientId = args["clientId"];
			if (string.IsNullOrEmpty(clientId))
			{
				throw new HttpException("Parameter clientId can not be empty.");
			}
			this._dataStorage = HttpContext.Current.Cache[clientId +
										"_DataStorage"] as C1EventsCalendarStorage;
			if (this._dataStorage == null)
			{
				throw new HttpException("[e951] Data storage not found.");
			}
			if (!string.IsNullOrEmpty((string)args["fileName"]))
			{
				this._dataStorage.DataFile = (string)args["fileName"];
			}		
		
			this._dataStorage.LoadData();
		
			string command = args["command"];

			if (string.IsNullOrEmpty(command))
				command = "";

			switch (command)
			{
				case "error":
					throw new Exception("test error");
				case "addEvent":
					try
					{


						Event insertedEventData = _dataStorage.AddEvent(ReadJsonPostData(context.Request));
						answer = "update:" + JsonHelper.ObjectToString(insertedEventData, null, false);
					}
					catch (Exception ex)
					{
						WriteError(context.Response, ex);
						return;
					}			
					context.Response.ContentType = "text/plain";
					context.Response.Write(answer);
					context.Response.End();
					break;
				case "updateEvent":
					try
					{
						eventData = ReadJsonPostData(context.Request);
						Event updatedEventData = _dataStorage.UpdateEvent(eventData);
						answer = "update:" + JsonHelper.ObjectToString(updatedEventData, null, true);
					}
					catch (Exception ex)
					{
						WriteError(context.Response, ex);
						return;
					}						
					context.Response.ContentType = "text/plain";
					context.Response.Write(answer);
					context.Response.End();
					break;
				case "deleteEvent":
					try
					{
						eventData = ReadJsonPostData(context.Request);
						_dataStorage.DeleteEvent(eventData);
						answer = "ok";
					}
					catch (Exception ex)
					{
						WriteError(context.Response, ex);
						return;
					}						
					context.Response.ContentType = "text/plain";
					context.Response.Write("ok");
					context.Response.End();
					break;
				case "loadEvents":
					try
					{
						// Fix for 20618  
						// (Collection was modified; enumeration operation may not execute)
						answer = _dataStorage.Events.ToArray();
					}
					catch (Exception ex)
					{
						WriteError(context.Response, ex);
						return;
					}
					context.Response.ContentType = "application/json";
					try
					{
						context.Response.Write(JsonHelper.ObjectToString(answer, null, false));
					}
					catch (Exception ex)
					{
						WriteError(context.Response, ex);
						return;
					}
					context.Response.End();
					break;					
				case "addCalendar":
					try
					{
						calendarData = ReadJsonPostData(context.Request);
						Hashtable insertedCalData = _dataStorage.AddCalendar(calendarData);
						answer = "update:" + JsonHelper.ObjectToString(insertedCalData, null, true);
					}
					catch (Exception ex)
					{
						WriteError(context.Response, ex);
						return;
					}					
					context.Response.ContentType = "text/plain";
					context.Response.Write(answer);
					context.Response.End();
					break;
				case "updateCalendar":
					try
					{
						calendarData = ReadJsonPostData(context.Request);
						Hashtable updatedCalData = _dataStorage.UpdateCalendar(calendarData);
						answer = "update:" + JsonHelper.ObjectToString(updatedCalData, null, true);
					}
					catch (Exception ex)
					{
						WriteError(context.Response, ex);
						return;
					}
					context.Response.ContentType = "text/plain";
					context.Response.Write(answer);
					context.Response.End();
					break;
				case "deleteCalendar":
					try
					{
						calendarData = ReadJsonPostData(context.Request);
						_dataStorage.DeleteCalendar(calendarData);
					}
					catch (Exception ex)
					{
						WriteError(context.Response, ex);
						return;
					}					
					context.Response.ContentType = "text/plain";
					context.Response.Write("ok");
					context.Response.End();
					break;
				case "loadCalendars":
					try
					{
						answer = _dataStorage.Calendars;
					}
					catch (Exception ex)
					{
						WriteError(context.Response, ex);
						return;
					}					
					context.Response.ContentType = "application/json";
					context.Response.Write(JsonHelper.ObjectToString(answer, null, true));
					context.Response.End();
					break;
				default:
					break;
			}

		
	}

	internal static Hashtable ReadJsonPostData(HttpRequest httpListenerRequest)
	{
		StreamReader reader = new StreamReader(httpListenerRequest.InputStream);
		string json = reader.ReadToEnd();
		if (json.StartsWith("json="))
		{
			json = json.Substring("json=".Length);
		}
		if (json.StartsWith("jsonData="))
		{
			json = json.Substring("jsonData=".Length);
		}
		Hashtable ht = JsonHelper.StringToObject(json);
		return ht;
	}

	void WriteError(HttpResponse r, Exception ex)
	{
		r.ContentType = "text/plain";
		r.Write("error:" + ex.Message);// + "; StackTrace:" + ex.StackTrace);
		r.End();
	}

	public bool IsReusable
    {
        get
        {
            return false;
        }
    }


    
}



                