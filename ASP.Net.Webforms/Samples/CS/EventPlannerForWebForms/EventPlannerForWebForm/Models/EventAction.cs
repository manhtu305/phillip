﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace EventPlannerForWebForm.Models
{
	public static class EventAction
	{
		private static EventPlannerEntities _eventDb = new EventPlannerEntities();

		internal static EventPlannerEntities GetEventDb()
		{
			return new EventPlannerEntities();
		}

		public static IList<EventObj> GetEvents()
		{
			return _eventDb.Events.ToList();
		}

		public static EventObj GetEventDetail(string id)
		{
			return _eventDb.Events.Find(id);
		}

		public static EventObj Create()
		{
			return new EventObj
			{
				Subject = "New event",
				Start = DateTime.Today,
				End = DateTime.Today.AddDays(1).AddSeconds(-1),
				AllDay = false
			};
		}

		public static void Add(EventObj eventObj)
		{
			_eventDb.Events.Add(eventObj);
			_eventDb.SaveChanges();
		}

		public static void Edit(EventObj eventObj)
		{
			_eventDb.Entry(eventObj).State = EntityState.Modified;
			_eventDb.SaveChanges();
		}

		public static void Delete(string id)
		{
			EventObj eventObj = _eventDb.Events.Find(id);
			_eventDb.Events.Remove(eventObj);
			_eventDb.SaveChanges();
		}
	}
}