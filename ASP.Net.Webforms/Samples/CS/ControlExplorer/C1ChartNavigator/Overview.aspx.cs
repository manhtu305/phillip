﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.C1Chart;

namespace ControlExplorer.C1ChartNavigator
{
    public partial class Overview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PrepareOptions();
            }
        }

        private void PrepareOptions()
        {
            var valuesX = new List<DateTime?>(){
            DateTime.Parse("2011-12-01"),
            DateTime.Parse("2011-12-02"),
            DateTime.Parse("2011-12-05"),
            DateTime.Parse("2011-12-06"),
            DateTime.Parse("2011-12-07"),
            DateTime.Parse("2011-12-08"),
            DateTime.Parse("2011-12-09"),
            DateTime.Parse("2011-12-12"),
            DateTime.Parse("2011-12-13"),
            DateTime.Parse("2011-12-14"),
            DateTime.Parse("2011-12-15"),
            DateTime.Parse("2011-12-16"),
            DateTime.Parse("2011-12-19"),
            DateTime.Parse("2011-12-20"),
            DateTime.Parse("2011-12-21"),
            DateTime.Parse("2011-12-22"),
            DateTime.Parse("2011-12-23"),
            DateTime.Parse("2011-12-26"),
            DateTime.Parse("2011-12-27"),
            DateTime.Parse("2011-12-28"),
            DateTime.Parse("2011-12-29"),
            DateTime.Parse("2011-12-30"),
            DateTime.Parse("2012-01-02"),
            DateTime.Parse("2012-01-03"),
            DateTime.Parse("2012-01-04"),
            DateTime.Parse("2012-01-05"),
            DateTime.Parse("2012-01-06"),
            DateTime.Parse("2012-01-09"),
            DateTime.Parse("2012-01-10"),
            DateTime.Parse("2012-01-11"),
            DateTime.Parse("2012-01-12"),
            DateTime.Parse("2012-01-13"),
            DateTime.Parse("2012-01-16"),
            DateTime.Parse("2012-01-17"),
            DateTime.Parse("2012-01-18"),
            DateTime.Parse("2012-01-19"),
            DateTime.Parse("2012-01-20"),
            DateTime.Parse("2012-01-23"),
            DateTime.Parse("2012-01-24"),
            DateTime.Parse("2012-01-25"),
            DateTime.Parse("2012-01-26"),
            DateTime.Parse("2012-01-27"),
            DateTime.Parse("2012-01-30"),
            DateTime.Parse("2012-01-31")};

            var valuesY = new List<double?>() { 10,12,11,14,16,20,18,17,17.5,20,22,21,22.5,20,21,20.8,20,19,18,17,16,15,15,14,13,12,
						11.5,10.9,10,9,9.5,10,12,11,14,16,20,18,17,17.5,20,22,21,22.5 };
            var valuesHigh = new List<double?>() { 10,12,11,14,16,20,18,17,17.5,20,22,21,22.5,20,21,20.8,20,19,18,17,16,15,15,14,13,12,
						11.5,10.9,10,9,9.5,10,12,11,14,16,20,18,17,17.5,20,22,21,22.5 };
            var valuesLow = new List<double?>() { 7.5,8.6,4.4,4.2,8,9,11,10,12.2,12,16,15.5,16,15,16,16.5,16,16,15,14.5,14,13.5,13,12,11,
						11,10,9,8,7.5,7.9,7.5,8.6,4.4,4.2,8,9,11,10,12.2,12,16,15.5,16 };
            var valuesOpen = new List<double?>() { 8,8.6,11,6.2,13.8,15,14,12,16,15,17,18,17.2,18.5,17.8,18.6,19.8,18,16.9,15.6,14.7,14.2,13.9,13.2,
						12.8,11.7,11.2,10.5,9.4,8.9,8.4,8,8.6,11,6.2,13.8,15,14,12,16,15,17,18,17.2 };
            var valuesClose = new List<double?>() { 8.6,11,6.2,13.8,15,14,12,16,15,17,18,17.2,18.5,17.8,18.6,19.8,18,16.9,15.6,14.7,14.2,13.9,13.2,
						12.8,11.7,11.2,10.5,9.4,8.9,8.4,8,8.6,11,6.2,13.8,15,14,12,16,15,17,18,17.2,18.5 };

            var candleSeries = new CandlestickChartSeries();
            candleSeries.Data.X.AddRange(valuesX.ToArray<DateTime?>());
            candleSeries.Data.High.AddRange(valuesHigh.ToArray<double?>());
            candleSeries.Data.Low.AddRange(valuesLow.ToArray<double?>());
            candleSeries.Data.Open.AddRange(valuesOpen.ToArray<double?>());
            candleSeries.Data.Close.AddRange(valuesClose.ToArray<double?>());
            candleSeries.Label = "MSFT";
            candleSeries.LegendEntry = true;
            this.C1CandlestickChart1.SeriesList.Add(candleSeries);

            var lineSeries = new LineChartSeries();
            lineSeries.Data.X.AddRange(valuesX.ToArray<DateTime?>());
            lineSeries.Data.Y.AddRange(valuesY.ToArray<double?>());
            this.ChartNavigator1.SeriesList.Add(lineSeries);
            this.ChartNavigator1.RangeMin = DateTime.Parse("2011-12-14").ToOADate();
            this.ChartNavigator1.RangeMax = DateTime.Parse("2012-01-04").ToOADate();
        }
    }
}