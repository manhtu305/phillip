<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeBehind="Overview.aspx.cs" Inherits="ControlExplorer.C1ChartNavigator.Overview" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <style>
        .hideTargetChart {
            position: absolute!important;
            clip: rect(1px, 1px, 1px, 1px);
        }
    </style>
    <script type="text/javascript">
        function hintContent() {
            return this.label + ' - ' +
							Globalize.format(this.x, "d") +
							'\n High:' + this.high +
							'\n Low:' + this.low +
							'\n Open:' + this.open +
							'\n Close:' + this.close;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div style="height: 310px; background-color: #fff">
    <wijmo:C1CandlestickChart ID="C1CandlestickChart1" runat="server" Height="250" Width="700" MarginTop="5" MarginBottom="10" MarginLeft="5" MarginRight="5" CssClass="hideTargetChart">
            <Animation Enabled="false" />
		<TextStyle FontFamily="Segoe UI, Frutiger, Frutiger Linotype, Dejavu Sans, Helvetica Neue, Arial, sans-serif" FontSize="13px">
		</TextStyle>
		<Header Compass="North" Text="<%$ Resources:C1ChartNavigator, Overview_HeaderText %>" TextStyle-FontSize="14px"></Header>
		<Legend Visible="false"></Legend>
		<Axis>
			<X Visible="false" TextVisible="false">
                <GridMajor Visible="False"></GridMajor>
			</X>
			<Y Visible="true" Compass="West">
				<GridMajor Visible="True"></GridMajor>
				<GridMinor Visible="False"></GridMinor>
			</Y>
		</Axis>
		<CandlestickChartSeriesStyles>
			<wijmo:CandlestickChartStyle>
				<HighLow Width="2">
					<Fill Color="#8C8C8C"></Fill>
				</HighLow>
				<FallingClose Width="6">
					<Fill Color="#F07E6E"></Fill>
				</FallingClose>
				<RisingClose Width="6">
					<Fill Color="#90CD97"></Fill>
				</RisingClose>
			</wijmo:CandlestickChartStyle>
		</CandlestickChartSeriesStyles>
		<Hint>
			<ContentStyle FontFamily="Segoe UI, Frutiger, Frutiger Linotype, Dejavu Sans, Helvetica Neue, Arial, sans-serif" FontSize="12px">
			</ContentStyle>
			<Content Function="hintContent" />
			<HintStyle Stroke="Transparent">
				<Fill Color="#444444">
				</Fill>
			</HintStyle>
        </Hint>
    </wijmo:C1CandlestickChart>

        <wijmo:C1ChartNavigator ID="ChartNavigator1" runat="server" Width="700" Height="56" TargetSelector="#C1CandlestickChart1">
            <SeriesStyles>
                <wijmo:ChartStyle Fill-Color="#88bde6" Stroke="#88bde6"></wijmo:ChartStyle>
            </SeriesStyles>
        </wijmo:C1ChartNavigator>
    </div>
    <script type="text/javascript">
        $("document").ready(function () {
            $(".hideTargetChart").removeClass("hideTargetChart");
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p><%= Resources.C1ChartNavigator.Overview_Text0 %></p>
    <p><%= Resources.C1ChartNavigator.Overview_Text1 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>
