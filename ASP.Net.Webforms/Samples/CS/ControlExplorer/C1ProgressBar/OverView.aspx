<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="OverView.aspx.cs" Inherits="ControlExplorer.C1ProgressBar.OverView" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ProgressBar" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div><%= Resources.C1ProgressBar.OverView_HorizontalProgressbar %></div>
<div>
<wijmo:C1ProgressBar runat="server" ID="HProgressbar" Value="50" 
		onruntask="HProgressbar_RunTask"></wijmo:C1ProgressBar>
</div>
<div style="clear:both"><%= Resources.C1ProgressBar.OverView_Text %></div>
<wijmo:C1ProgressBar runat="server" ID="VProgressbar" FillDirection="South" Value="50"></wijmo:C1ProgressBar>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
<p><%= Resources.C1ProgressBar.OverView_Text0 %></p><br/>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
