<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"	CodeBehind="Direction.aspx.cs" Inherits="ControlExplorer.C1ProgressBar.Direction" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ProgressBar"	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div style="padding-bottom:1em;">
	    <wijmo:C1ProgressBar runat="server" ID="Progressbar1" MinValue="0" MaxValue="100" FillDirection="East" LabelAlign="Center" Value="50">
	    </wijmo:C1ProgressBar>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1ProgressBar.Direction_Text0 %></p><br/>
	<p><%= Resources.C1ProgressBar.Direction_Text1 %></p>
	<ul>
		<li><%= Resources.C1ProgressBar.Direction_Li1 %></li>
		<li><%= Resources.C1ProgressBar.Direction_Li2 %></li>
		<li><%= Resources.C1ProgressBar.Direction_Li3 %></li>
	</ul>
	<p><%= Resources.C1ProgressBar.Direction_Text2 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class ="settingcontent">
            <ul>
                <li>
                    <label><%= Resources.C1ProgressBar.Direction_Value %></label>
                    <asp:TextBox ID="c1Input1" runat="server" Width="80" Text="50"></asp:TextBox>
                </li>
                <li>
                    <label><%= Resources.C1ProgressBar.Direction_LabelAlign %></label>
                    <asp:DropDownList ID="c1Combobox1" Width="80" runat="server">
                        <asp:ListItem Text="East" Value="east" />
                        <asp:ListItem Text="West" Value="west" />
                        <asp:ListItem Text="Center" Selected="true" Value="center" />
                        <asp:ListItem Text="North" Value="north" />
                        <asp:ListItem Text="South" Value="south" />
                        <asp:ListItem Text="Running" Value="running" />
                    </asp:DropDownList><div><%= Resources.C1ProgressBar.Direction_LabelAlignTip %></div>
                </li>
                <li>
                    <label><%= Resources.C1ProgressBar.Direction_FillDirection %></label>
                    <asp:DropDownList ID="c1Combobox2" Width="80" runat="server">
                       <Items>
                            <asp:ListItem Text="East" Selected="true" Value="east" />
                            <asp:ListItem Text="West" Value="west" />
                            <asp:ListItem Text="North" Value="north" />
                            <asp:ListItem Text="South" Value="south" />
                        </Items>
                    </asp:DropDownList>
                </li>
            </ul>
        </div>
    </div>
	<script type="text/javascript">
	    $(document).ready(function () {
	        var $v = $('#<%=c1Input1.ClientID%>');
	        var $d = $('#<%=c1Combobox2.ClientID%>');
	        var $a = $('#<%=c1Combobox1.ClientID%>');
	        var $p = $('#<%=Progressbar1.ClientID%>');
	        $v.change(function () {
	            if(isNaN($v.val()))
	                return false;
	            $p.c1progressbar('option', 'value', parseFloat($v.val()));
	        });
	        $a.change(function () {
	            $p.c1progressbar('option', 'labelAlign', $a.val());
	        });
	        $d.change(function () {
	            $p.c1progressbar('option', 'fillDirection', $d.val());
	        });
		});
	</script>
</asp:Content>
