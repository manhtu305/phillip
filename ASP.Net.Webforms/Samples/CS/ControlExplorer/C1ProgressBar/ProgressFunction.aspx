<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ProgressFunction.aspx.cs" Inherits="ControlExplorer.C1ProgressBar.ProgressFunction" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ProgressBar"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
 <wijmo:C1ProgressBar runat="server" ID="Progressbar1"
		onruntask="Progressbar1_RunTask" StartTaskButton="btnStart" 
		StopTaskButton="btnStop">
	</wijmo:C1ProgressBar>
	<asp:Button runat="server" ID="btnStart" Text="<%$ Resources:C1ProgressBar, ProgressFunction_Start %>" />
	<asp:Button runat="server" ID="btnStop" Text="<%$ Resources:C1ProgressBar, ProgressFunction_Stop %>" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1ProgressBar.ProgressFunction_Text0 %></p><br/>
	<p><%= Resources.C1ProgressBar.ProgressFunction_Text1 %></p>
	<ul>
		<li><%= Resources.C1ProgressBar.ProgressFunction_Li1 %></li>
		<li><%= Resources.C1ProgressBar.ProgressFunction_Li2 %></li>
		<li><%= Resources.C1ProgressBar.ProgressFunction_Li3 %></li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
