//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "11.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class C1Maps {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal C1Maps() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.C1Maps", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Using the vector layer, the map shows the largest cities of the world (by population)..
        /// </summary>
        internal static string Cities_Text0 {
            get {
                return ResourceManager.GetString("Cities_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The sample shows how to use C1VirtualLayer to show the factories/offices/stores.  
        ///		At first, all factories and offices are shown on the map.  When you zoom in to 10, then all the stores will be shown on the map..
        /// </summary>
        internal static string Factories_Text0 {
            get {
                return ResourceManager.GetString("Factories_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add a C1ItemsLayer with the data of the cities..
        /// </summary>
        internal static string FlightRoutes_Li1 {
            get {
                return ResourceManager.GetString("FlightRoutes_Li1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add a C1VectorLayer with the data of flights from Bern..
        /// </summary>
        internal static string FlightRoutes_Li2 {
            get {
                return ResourceManager.GetString("FlightRoutes_Li2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add a C1VectorLayer with the data of flights from Kiev..
        /// </summary>
        internal static string FlightRoutes_Li3 {
            get {
                return ResourceManager.GetString("FlightRoutes_Li3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In document ready event, hide one of the C1VectorLayers..
        /// </summary>
        internal static string FlightRoutes_Li4 {
            get {
                return ResourceManager.GetString("FlightRoutes_Li4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In the switcher element&apos;s click event, show the hidden C1VectorLayer and hide the showing C1VectorLayer..
        /// </summary>
        internal static string FlightRoutes_Li5 {
            get {
                return ResourceManager.GetString("FlightRoutes_Li5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample demonstrates how to switch different flights information in &lt;b&gt;C1Maps&lt;/b&gt; control..
        /// </summary>
        internal static string FlightRoutes_Text0 {
            get {
                return ResourceManager.GetString("FlightRoutes_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The steps to create this sample:.
        /// </summary>
        internal static string FlightRoutes_Text1 {
            get {
                return ResourceManager.GetString("FlightRoutes_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The sample shows how to use a remote geojson file to draw vector layers on the map..
        /// </summary>
        internal static string GeoJson_Text0 {
            get {
                return ResourceManager.GetString("GeoJson_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The vector layer allows you to add parallels and meridians to the map. You can use the vector layer on top of a Tile Source to draw geometries/shapes/polygons/paths with geo coordinates..
        /// </summary>
        internal static string Grid_Text0 {
            get {
                return ResourceManager.GetString("Grid_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The sample shows how you can create a map chart based on country polygons loaded from KMZ file (zipped KML) into a C1VectorLayer. It also shows that you can use a C1Maps without any Tile Source..
        /// </summary>
        internal static string MapChart_Text0 {
            get {
                return ResourceManager.GetString("MapChart_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;C1Maps&lt;/strong&gt; pre-defines 3 types of bing map sources(Aerial/Road/Hybrid), you can easily set the Source property to achieve it. 
        ///		It also allows you to use custom source just like this sample uses google map source.  In order to use custom source, you should set Source property
        ///		to CustomSource and define a javascript function to retrieve the tile sources.  Certainly, if you don&apos;t want anything rendered, you can
        ///		set the Source property to None..
        /// </summary>
        internal static string MapSource_Text0 {
            get {
                return ResourceManager.GetString("MapSource_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The sample shows how you can add marks to the map control.  You can click the mouse to add a new mark and once a new mark is added,
        ///		it will connect to the previous mark.  When you click the existing mark, the mark will be removed from maps..
        /// </summary>
        internal static string Marks_Text0 {
            get {
                return ResourceManager.GetString("Marks_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;C1Maps&lt;/strong&gt; can display online maps from various built-in and custom sources. 
        ///        By default, &lt;strong&gt;C1Maps&lt;/strong&gt; provides three built-in sources for Microsoft Bing Maps™ including aerial, road and hybrid views. 
        ///        Additionally, C1Maps makes it easy to use the online map tiles of your choice. We include custom samples using the Google Maps. 
        ///        You can use these samples to extend the control to view any additional map source..
        /// </summary>
        internal static string OverView_Text0 {
            get {
                return ResourceManager.GetString("OverView_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The sample shows how to use C1VectorLayer with 3 different datasources..
        /// </summary>
        internal static string VectorLayer_Text0 {
            get {
                return ResourceManager.GetString("VectorLayer_Text0", resourceCulture);
            }
        }
    }
}
