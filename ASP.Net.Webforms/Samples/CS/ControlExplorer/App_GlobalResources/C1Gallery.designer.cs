//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "11.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class C1Gallery {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal C1Gallery() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.C1Gallery", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Animation.
        /// </summary>
        internal static string Animation_Animation {
            get {
                return ResourceManager.GetString("Animation_Animation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample demonstrates animation effects when transitioning from one image to
        ///        the next..
        /// </summary>
        internal static string Animation_Text0 {
            get {
                return ResourceManager.GetString("Animation_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set following properties to support this feature:.
        /// </summary>
        internal static string Animation_Text1 {
            get {
                return ResourceManager.GetString("Animation_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;DataSourceID &lt;/strong&gt;- sets the ID of the data source..
        /// </summary>
        internal static string DataBinding_Li1 {
            get {
                return ResourceManager.GetString("DataBinding_Li1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;DataCaptionField &lt;/strong&gt;-&amp;nbsp; sets the field in the data source from 
        ///			which to load  the caption of the images..
        /// </summary>
        internal static string DataBinding_Li2 {
            get {
                return ResourceManager.GetString("DataBinding_Li2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;DataLinkUrlField &lt;/strong&gt;-&amp;nbsp; sets the field in the data source from which to load link fields..
        /// </summary>
        internal static string DataBinding_Li3 {
            get {
                return ResourceManager.GetString("DataBinding_Li3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;DataImageUrlField &lt;/strong&gt;-&amp;nbsp; sets the field in the data source from which to load image url fields..
        /// </summary>
        internal static string DataBinding_Li4 {
            get {
                return ResourceManager.GetString("DataBinding_Li4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to LoadOnDemand.
        /// </summary>
        internal static string DataBinding_LoadOnDemand {
            get {
                return ResourceManager.GetString("DataBinding_LoadOnDemand", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;C1Gallery&lt;/strong&gt; supports data binding, it can bind image url, caption, and link fields, or bind any data in template..
        /// </summary>
        internal static string DataBinding_Text0 {
            get {
                return ResourceManager.GetString("DataBinding_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data binding is allowed if the following properties are set:.
        /// </summary>
        internal static string DataBinding_Text1 {
            get {
                return ResourceManager.GetString("DataBinding_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample illustrates the flash mode of &lt;strong&gt;C1Gallery &lt;/strong&gt;control, which
        ///        has an inner flash player in the frame..
        /// </summary>
        internal static string Flash_Text0 {
            get {
                return ResourceManager.GetString("Flash_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The property &lt;b&gt;Mode&lt;/b&gt; should set to &quot;Swf&quot;..
        /// </summary>
        internal static string Flash_Text1 {
            get {
                return ResourceManager.GetString("Flash_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to http://cdn.wijmo.com/images/componentone_thumb.png.
        /// </summary>
        internal static string Iframe_ImageUrl1 {
            get {
                return ResourceManager.GetString("Iframe_ImageUrl1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to http://cdn.wijmo.com/images/microsoft_thumb.png.
        /// </summary>
        internal static string Iframe_ImageUrl2 {
            get {
                return ResourceManager.GetString("Iframe_ImageUrl2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to https://www.componentone.com.
        /// </summary>
        internal static string Iframe_LinkUrl1 {
            get {
                return ResourceManager.GetString("Iframe_LinkUrl1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to https://windows.microsoft.com/en-US/windows/home.
        /// </summary>
        internal static string Iframe_LinkUrl2 {
            get {
                return ResourceManager.GetString("Iframe_LinkUrl2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample illustrates the iframe mode of &lt;strong&gt;C1Gallery &lt;/strong&gt;control, which has an inner iframe that links to another URL..
        /// </summary>
        internal static string Iframe_Text0 {
            get {
                return ResourceManager.GetString("Iframe_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The property &lt;b&gt;Mode&lt;/b&gt; should set to &quot;Iframe&quot;..
        /// </summary>
        internal static string Iframe_Text1 {
            get {
                return ResourceManager.GetString("Iframe_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample demonstrates how the orientation of the thumbnails can be determined using the &lt;b&gt;ThumbnailOrientation&lt;/b&gt; property..
        /// </summary>
        internal static string Orientation_Text0 {
            get {
                return ResourceManager.GetString("Orientation_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Orientation is &quot;Vertical&quot;, at left side.
        /// </summary>
        internal static string Orientation_Title1 {
            get {
                return ResourceManager.GetString("Orientation_Title1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Orientation is &quot;Vertical&quot;, at right side.
        /// </summary>
        internal static string Orientation_Title2 {
            get {
                return ResourceManager.GetString("Orientation_Title2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Orientation is &quot;Horizontal&quot;, at top.
        /// </summary>
        internal static string Orientation_Title3 {
            get {
                return ResourceManager.GetString("Orientation_Title3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample illustrates the default &lt;strong&gt;C1Gallery &lt;/strong&gt;control, which is
        ///        useful for displaying images and their thumbnails..
        /// </summary>
        internal static string Overview_Text0 {
            get {
                return ResourceManager.GetString("Overview_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample illustrates the paging of the gallery..
        /// </summary>
        internal static string Paging_Text0 {
            get {
                return ResourceManager.GetString("Paging_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The property &lt;b&gt;ShowPager&lt;/b&gt; is set to determine whether should show the pager..
        /// </summary>
        internal static string Paging_Text1 {
            get {
                return ResourceManager.GetString("Paging_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ShowThumbnailCaptions.
        /// </summary>
        internal static string ShowThumbnailCaptions_ShowThumbnailCaptions {
            get {
                return ResourceManager.GetString("ShowThumbnailCaptions_ShowThumbnailCaptions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample illustrates how to show the caption of thumbnails..
        /// </summary>
        internal static string ShowThumbnailCaptions_Text0 {
            get {
                return ResourceManager.GetString("ShowThumbnailCaptions_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The property &lt;b&gt;ShowThumbnailCaptions&lt;/b&gt; is set to determine 
        ///        whether the caption of thumbnails should be shown..
        /// </summary>
        internal static string ShowThumbnailCaptions_Text1 {
            get {
                return ResourceManager.GetString("ShowThumbnailCaptions_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to http://cdn.wijmo.com/images/keyboardcat.png.
        /// </summary>
        internal static string Video_ImageUrl1 {
            get {
                return ResourceManager.GetString("Video_ImageUrl1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to http://cdn.wijmo.com/images/panda.png.
        /// </summary>
        internal static string Video_ImageUrl2 {
            get {
                return ResourceManager.GetString("Video_ImageUrl2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to http://cdn.wijmo.com/images/talkingcats.png.
        /// </summary>
        internal static string Video_ImageUrl3 {
            get {
                return ResourceManager.GetString("Video_ImageUrl3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to https://www.youtube.com/v/J---aiyznGQ?version=3.
        /// </summary>
        internal static string Video_LinkUrl1 {
            get {
                return ResourceManager.GetString("Video_LinkUrl1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to http://www.youtube.com/v/FzRH3iTQPrk?version=3.
        /// </summary>
        internal static string Video_LinkUrl2 {
            get {
                return ResourceManager.GetString("Video_LinkUrl2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to http://www.youtube.com/v/z3U0udLH974?version=3.
        /// </summary>
        internal static string Video_LinkUrl3 {
            get {
                return ResourceManager.GetString("Video_LinkUrl3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample illustrates that we can play videos with &lt;strong&gt;C1Gallery &lt;/strong&gt;control..
        /// </summary>
        internal static string Video_Text0 {
            get {
                return ResourceManager.GetString("Video_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The property &lt;b&gt;Mode&lt;/b&gt; should set to &quot;Swf&quot;..
        /// </summary>
        internal static string Video_Text1 {
            get {
                return ResourceManager.GetString("Video_Text1", resourceCulture);
            }
        }
    }
}
