//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "11.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class C1BinaryImage {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal C1BinaryImage() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.C1BinaryImage", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Country: .
        /// </summary>
        internal static string CustomHandler_CountryLabel {
            get {
                return ResourceManager.GetString("CustomHandler_CountryLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ID: .
        /// </summary>
        internal static string CustomHandler_IdLabel {
            get {
                return ResourceManager.GetString("CustomHandler_IdLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set &lt;b&gt;HttpHandlerUrl&lt;/b&gt; property to &quot;CustomBinaryImageHandler.ashx&quot;..
        /// </summary>
        internal static string CustomHandler_Li1 {
            get {
                return ResourceManager.GetString("CustomHandler_Li1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The class CustomBinaryImageHandler inherits from C1BinaryImageHandler(class: C1.Web.Wijmo.Controls.C1BinaryImage.C1BinaryImageHandler)&lt;br /&gt;
        ///            and overrides ProcessImageData method to customize the image:&lt;br /&gt;&lt;br /&gt;
        ///            &lt;pre&gt;
        ///public class CustomBinaryImageHandler : C1BinaryImageHandler
        ///{
        ///    public override C1BinaryImageData ProcessImageData(C1BinaryImageData imageData)
        ///    {
        ///        using (var outStream = new System.IO.MemoryStream())
        ///        using (var inStream = new System.IO.M [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string CustomHandler_Li2 {
            get {
                return ResourceManager.GetString("CustomHandler_Li2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Place: .
        /// </summary>
        internal static string CustomHandler_PlaceLabel {
            get {
                return ResourceManager.GetString("CustomHandler_PlaceLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This demo uses &lt;b&gt;HttpHandlerUrl&lt;/b&gt; property to specify a http handler to process the image..
        /// </summary>
        internal static string CustomHandler_Text0 {
            get {
                return ResourceManager.GetString("CustomHandler_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sample:.
        /// </summary>
        internal static string CustomHandler_Text1 {
            get {
                return ResourceManager.GetString("CustomHandler_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Country: .
        /// </summary>
        internal static string OverView_CountryLabel {
            get {
                return ResourceManager.GetString("OverView_CountryLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ID: .
        /// </summary>
        internal static string OverView_IdLabel {
            get {
                return ResourceManager.GetString("OverView_IdLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Place: .
        /// </summary>
        internal static string OverView_PlaceLabel {
            get {
                return ResourceManager.GetString("OverView_PlaceLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;C1BinaryImage&lt;/strong&gt; provides an easy way to display an image which is stored as binary data directly in the database.  The control can be used in any data bound control (Repeater, DataList, GridView etc.) to display images which originate from binary image field in the data source..
        /// </summary>
        internal static string OverView_Text0 {
            get {
                return ResourceManager.GetString("OverView_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bottom.
        /// </summary>
        internal static string ResizeMode_CropPositionBottom {
            get {
                return ResourceManager.GetString("ResizeMode_CropPositionBottom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Center.
        /// </summary>
        internal static string ResizeMode_CropPositionCenter {
            get {
                return ResourceManager.GetString("ResizeMode_CropPositionCenter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CropPosition:.
        /// </summary>
        internal static string ResizeMode_CropPositionLabel {
            get {
                return ResourceManager.GetString("ResizeMode_CropPositionLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Left.
        /// </summary>
        internal static string ResizeMode_CropPositionLeft {
            get {
                return ResourceManager.GetString("ResizeMode_CropPositionLeft", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Right.
        /// </summary>
        internal static string ResizeMode_CropPositionRight {
            get {
                return ResourceManager.GetString("ResizeMode_CropPositionRight", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Top.
        /// </summary>
        internal static string ResizeMode_CropPositionTop {
            get {
                return ResourceManager.GetString("ResizeMode_CropPositionTop", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Height:.
        /// </summary>
        internal static string ResizeMode_HeightLabel {
            get {
                return ResourceManager.GetString("ResizeMode_HeightLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Resize Mode:.
        /// </summary>
        internal static string ResizeMode_OptionsTitle {
            get {
                return ResourceManager.GetString("ResizeMode_OptionsTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Crop.
        /// </summary>
        internal static string ResizeMode_ResizeModeCrop {
            get {
                return ResourceManager.GetString("ResizeMode_ResizeModeCrop", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fill.
        /// </summary>
        internal static string ResizeMode_ResizeModeFill {
            get {
                return ResourceManager.GetString("ResizeMode_ResizeModeFill", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fit.
        /// </summary>
        internal static string ResizeMode_ResizeModeFit {
            get {
                return ResourceManager.GetString("ResizeMode_ResizeModeFit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ResizeMode:.
        /// </summary>
        internal static string ResizeMode_ResizeModeLabel {
            get {
                return ResourceManager.GetString("ResizeMode_ResizeModeLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to None.
        /// </summary>
        internal static string ResizeMode_ResizeModeNone {
            get {
                return ResourceManager.GetString("ResizeMode_ResizeModeNone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The &lt;strong&gt;C1BinaryImage&lt;/strong&gt; control support resizing(Crop/Fill/Fit) action..
        /// </summary>
        internal static string ResizeMode_Text0 {
            get {
                return ResourceManager.GetString("ResizeMode_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Source image(300x225):.
        /// </summary>
        internal static string ResizeMode_Text1 {
            get {
                return ResourceManager.GetString("ResizeMode_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Resized image:.
        /// </summary>
        internal static string ResizeMode_Text2 {
            get {
                return ResourceManager.GetString("ResizeMode_Text2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Width:.
        /// </summary>
        internal static string ResizeMode_WidthLabel {
            get {
                return ResourceManager.GetString("ResizeMode_WidthLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Apply.
        /// </summary>
        internal static string SavedImageName_Apply {
            get {
                return ResourceManager.GetString("SavedImageName_Apply", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SavedImageName:.
        /// </summary>
        internal static string SavedImageName_SavedImageNameLabel {
            get {
                return ResourceManager.GetString("SavedImageName_SavedImageNameLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Right click the image and click &quot;Save Picture as ...&quot;, it shows a save dialog, customers can specify the default file name..
        /// </summary>
        internal static string SavedImageName_Text0 {
            get {
                return ResourceManager.GetString("SavedImageName_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use the &lt;strong&gt;SavedImageName&lt;/strong&gt; property to change the default file name..
        /// </summary>
        internal static string SavedImageName_Text1 {
            get {
                return ResourceManager.GetString("SavedImageName_Text1", resourceCulture);
            }
        }
    }
}
