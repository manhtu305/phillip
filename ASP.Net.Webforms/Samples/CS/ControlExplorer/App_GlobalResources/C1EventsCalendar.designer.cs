//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "11.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class C1EventsCalendar {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal C1EventsCalendar() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.C1EventsCalendar", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;dataStorage&lt;/strong&gt; - use this option in order to implement the custom data storage layer..
        /// </summary>
        internal static string CustomDataStorage_Li1 {
            get {
                return ResourceManager.GetString("CustomDataStorage_Li1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;visibleCalendars&lt;/strong&gt; - array of the calendar names that need to be shown..
        /// </summary>
        internal static string CustomDataStorage_Li2 {
            get {
                return ResourceManager.GetString("CustomDataStorage_Li2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample demonstrates how to implement custom data storage for the events calendar..
        /// </summary>
        internal static string CustomDataStorage_Text0 {
            get {
                return ResourceManager.GetString("CustomDataStorage_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The sample uses the &lt;b&gt;amplify.store&lt;/b&gt; library in order to implement local data storage..
        /// </summary>
        internal static string CustomDataStorage_Text1 {
            get {
                return ResourceManager.GetString("CustomDataStorage_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Options used in the sample include the following:.
        /// </summary>
        internal static string CustomDataStorage_Text2 {
            get {
                return ResourceManager.GetString("CustomDataStorage_Text2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add event dialog caption.
        /// </summary>
        internal static string CustomEditEvent_AddCaption {
            get {
                return ResourceManager.GetString("CustomEditEvent_AddCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        internal static string CustomEditEvent_CancelText {
            get {
                return ResourceManager.GetString("CustomEditEvent_CancelText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        internal static string CustomEditEvent_DeleteText {
            get {
                return ResourceManager.GetString("CustomEditEvent_DeleteText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit event dialog caption.
        /// </summary>
        internal static string CustomEditEvent_EditCaption {
            get {
                return ResourceManager.GetString("CustomEditEvent_EditCaption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        internal static string CustomEditEvent_SaveText {
            get {
                return ResourceManager.GetString("CustomEditEvent_SaveText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Subject : .
        /// </summary>
        internal static string CustomEditEvent_SubjectLabel {
            get {
                return ResourceManager.GetString("CustomEditEvent_SubjectLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The sample demonstrates how to create new custom event dialog..
        /// </summary>
        internal static string CustomEditEvent_Text0 {
            get {
                return ResourceManager.GetString("CustomEditEvent_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Company.
        /// </summary>
        internal static string CustomEventTemplate_Location1 {
            get {
                return ResourceManager.GetString("CustomEventTemplate_Location1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Restaurant.
        /// </summary>
        internal static string CustomEventTemplate_Location2 {
            get {
                return ResourceManager.GetString("CustomEventTemplate_Location2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Work.
        /// </summary>
        internal static string CustomEventTemplate_Subject1 {
            get {
                return ResourceManager.GetString("CustomEventTemplate_Subject1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lunch.
        /// </summary>
        internal static string CustomEventTemplate_Subject2 {
            get {
                return ResourceManager.GetString("CustomEventTemplate_Subject2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample demonstrates how to create custom event pane..
        /// </summary>
        internal static string CustomEventTemplate_Text0 {
            get {
                return ResourceManager.GetString("CustomEventTemplate_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Export.
        /// </summary>
        internal static string CustomHandleResponse_ExportText {
            get {
                return ResourceManager.GetString("CustomHandleResponse_ExportText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File Format:.
        /// </summary>
        internal static string CustomHandleResponse_FileFormatLabel {
            get {
                return ResourceManager.GetString("CustomHandleResponse_FileFormatLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File Name:.
        /// </summary>
        internal static string CustomHandleResponse_FileNameLabel {
            get {
                return ResourceManager.GetString("CustomHandleResponse_FileNameLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Server Url:.
        /// </summary>
        internal static string CustomHandleResponse_ServerUrlLabel {
            get {
                return ResourceManager.GetString("CustomHandleResponse_ServerUrlLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample demonstrates how to export &lt;strong&gt;EventsCalendar&lt;/strong&gt; by sending the 
        ///        request with json data and handling the response data to save as a file..
        /// </summary>
        internal static string CustomHandleResponse_Text0 {
            get {
                return ResourceManager.GetString("CustomHandleResponse_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Export.
        /// </summary>
        internal static string CustomSendingRequest_ExportText {
            get {
                return ResourceManager.GetString("CustomSendingRequest_ExportText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File Format:.
        /// </summary>
        internal static string CustomSendingRequest_FileFormatLabel {
            get {
                return ResourceManager.GetString("CustomSendingRequest_FileFormatLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File Name:.
        /// </summary>
        internal static string CustomSendingRequest_FileNameLabel {
            get {
                return ResourceManager.GetString("CustomSendingRequest_FileNameLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Server Url:.
        /// </summary>
        internal static string CustomSendingRequest_ServerUrlLabel {
            get {
                return ResourceManager.GetString("CustomSendingRequest_ServerUrlLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample demonstrates how customize to send the request to export &lt;strong&gt;EventsCalendar&lt;/strong&gt;..
        /// </summary>
        internal static string CustomSendingRequest_Text0 {
            get {
                return ResourceManager.GetString("CustomSendingRequest_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;TimeInterval&lt;/strong&gt; - the time interval in minutes for the Day view..
        /// </summary>
        internal static string CustomTimeInterval_Li1 {
            get {
                return ResourceManager.GetString("CustomTimeInterval_Li1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;TimeIntervalHeight&lt;/strong&gt; - the Day view time interval row height in pixels..
        /// </summary>
        internal static string CustomTimeInterval_Li2 {
            get {
                return ResourceManager.GetString("CustomTimeInterval_Li2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;strong&gt;TimeRulerInterval&lt;/strong&gt; - the time ruler interval in minutes for the Day view..
        /// </summary>
        internal static string CustomTimeInterval_Li3 {
            get {
                return ResourceManager.GetString("CustomTimeInterval_Li3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample demonstrates how to change the day view time interval..
        /// </summary>
        internal static string CustomTimeInterval_Text0 {
            get {
                return ResourceManager.GetString("CustomTimeInterval_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Properties used in the sample:.
        /// </summary>
        internal static string CustomTimeInterval_Text1 {
            get {
                return ResourceManager.GetString("CustomTimeInterval_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample demonstrates how to create custom view..
        /// </summary>
        internal static string CustomView_Text0 {
            get {
                return ResourceManager.GetString("CustomView_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Custom views can be added by updating the Views property of &lt;strong&gt;C1EventsCalendar&lt;/strong&gt; control..
        /// </summary>
        internal static string CustomView_Text1 {
            get {
                return ResourceManager.GetString("CustomView_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to By default the C1EventsCalendar has 4 default Views: &quot;Day&quot;, &quot;Week&quot;, &quot;Month&quot;, &quot;List&quot;.
        ///				When adding a default view, only Type property of View should be set.
        ///				If adding a custom view, All the properties of View should be set..
        /// </summary>
        internal static string CustomView_Text2 {
            get {
                return ResourceManager.GetString("CustomView_Text2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2 Days.
        /// </summary>
        internal static string CustomView_TwoDaysName {
            get {
                return ResourceManager.GetString("CustomView_TwoDaysName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2 Months.
        /// </summary>
        internal static string CustomView_TwoMonthsName {
            get {
                return ResourceManager.GetString("CustomView_TwoMonthsName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2 Weeks.
        /// </summary>
        internal static string CustomView_TwoWeeksName {
            get {
                return ResourceManager.GetString("CustomView_TwoWeeksName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 2 Years.
        /// </summary>
        internal static string CustomView_TwoYearsName {
            get {
                return ResourceManager.GetString("CustomView_TwoYearsName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add data source control to your page..
        /// </summary>
        internal static string DataBinding_Li1 {
            get {
                return ResourceManager.GetString("DataBinding_Li1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Setup the event storage&apos;s DataSourceID property. If needed, fill the DataMember property..
        /// </summary>
        internal static string DataBinding_Li2 {
            get {
                return ResourceManager.GetString("DataBinding_Li2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Generate Insert/Update/Delete commands if you wish to allow end-user to edit events. e.g.:.
        /// </summary>
        internal static string DataBinding_Li3 {
            get {
                return ResourceManager.GetString("DataBinding_Li3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Configure event storage data mappings. e.g.:
        ///&lt;pre class=&quot;controldescription-code&quot;&gt;
        ///&amp;lt;EventStorage DataSourceID=&quot;AccessDataSource_Events&quot;&amp;gt;
        ///	&amp;lt;Mappings&amp;gt;	
        ///		&amp;lt;IdMapping MappingName=&quot;AppointmentId&quot; /&amp;gt;			
        ///		&amp;lt;StartMapping MappingName=&quot;Start&quot; /&amp;gt;
        ///		&amp;lt;EndMapping MappingName=&quot;End&quot; /&amp;gt;
        ///		&amp;lt;SubjectMapping MappingName=&quot;Subject&quot; /&amp;gt;
        ///		&amp;lt;LocationMapping MappingName=&quot;Location&quot; /&amp;gt;
        ///		&amp;lt;DescriptionMapping MappingName=&quot;Description&quot; /&amp;gt;
        ///		&amp;lt;ColorMapping MappingName=&quot;Color&quot; /&amp;gt; [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string DataBinding_Li4 {
            get {
                return ResourceManager.GetString("DataBinding_Li4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add data source control to your page..
        /// </summary>
        internal static string DataBinding_Li5 {
            get {
                return ResourceManager.GetString("DataBinding_Li5", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Setup the calendar storage&apos;s DataSourceID property. If needed, fill the DataMember property..
        /// </summary>
        internal static string DataBinding_Li6 {
            get {
                return ResourceManager.GetString("DataBinding_Li6", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Generate Insert/Update/Delete commands if you wish to allow end-user to edit calendars..
        /// </summary>
        internal static string DataBinding_Li7 {
            get {
                return ResourceManager.GetString("DataBinding_Li7", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Configure calendar storage data mappings. e.g:
        ///&lt;pre class=&quot;controldescription-code&quot;&gt;
        ///&amp;lt;CalendarStorage DataSourceID=&quot;AccessDataSource_Calendars&quot;&amp;gt;
        ///	&amp;lt;Mappings&amp;gt;
        ///		&amp;lt;IdMapping MappingName=&quot;CalendarId&quot; /&amp;gt;
        ///		&amp;lt;LocationMapping MappingName=&quot;Location&quot; /&amp;gt;
        ///		&amp;lt;ColorMapping MappingName=&quot;Color&quot; /&amp;gt;
        ///		&amp;lt;DescriptionMapping MappingName=&quot;Description&quot; /&amp;gt;
        ///		&amp;lt;NameMapping MappingName=&quot;Name&quot; /&amp;gt;
        ///		&amp;lt;PropertiesMapping MappingName=&quot;Properties&quot; /&amp;gt;
        ///		&amp;lt;TagMapping MappingName=&quot;Tag&quot;  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string DataBinding_Li8 {
            get {
                return ResourceManager.GetString("DataBinding_Li8", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample shows how to bind data to the &lt;strong&gt;C1EventsCalendar&lt;/strong&gt; control..
        /// </summary>
        internal static string DataBinding_Text0 {
            get {
                return ResourceManager.GetString("DataBinding_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to In order to bind the C1EventsCalendar to a data source, you need to follow these steps:.
        /// </summary>
        internal static string DataBinding_Text1 {
            get {
                return ResourceManager.GetString("DataBinding_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Configure EventStorage..
        /// </summary>
        internal static string DataBinding_Text2 {
            get {
                return ResourceManager.GetString("DataBinding_Text2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Configure CalendarStorage..
        /// </summary>
        internal static string DataBinding_Text3 {
            get {
                return ResourceManager.GetString("DataBinding_Text3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Calendars.
        /// </summary>
        internal static string DataModel_CalendarsLabel {
            get {
                return ResourceManager.GetString("DataModel_CalendarsLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add event using dialog.
        /// </summary>
        internal static string DataModel_EventsAdd1 {
            get {
                return ResourceManager.GetString("DataModel_EventsAdd1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add event without dialog.
        /// </summary>
        internal static string DataModel_EventsAdd2 {
            get {
                return ResourceManager.GetString("DataModel_EventsAdd2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete all events for the active day (without confirmation).
        /// </summary>
        internal static string DataModel_EventsDelete {
            get {
                return ResourceManager.GetString("DataModel_EventsDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Events.
        /// </summary>
        internal static string DataModel_EventsLabel {
            get {
                return ResourceManager.GetString("DataModel_EventsLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The sample demonstrates how to edit events using client side script..
        /// </summary>
        internal static string DataModel_Text2 {
            get {
                return ResourceManager.GetString("DataModel_Text2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Options used in the sample:.
        /// </summary>
        internal static string DataModel_Text3 {
            get {
                return ResourceManager.GetString("DataModel_Text3", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Client side methods used in the sample:.
        /// </summary>
        internal static string DataModel_Text4 {
            get {
                return ResourceManager.GetString("DataModel_Text4", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample demonstrates how export &lt;strong&gt;EventsCalendar&lt;/strong&gt; to different formats of image..
        /// </summary>
        internal static string ExportingImage_Text0 {
            get {
                return ResourceManager.GetString("ExportingImage_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample demonstrates how export &lt;strong&gt;EventsCalendar&lt;/strong&gt; to pdf..
        /// </summary>
        internal static string ExportingPdf_Text0 {
            get {
                return ResourceManager.GetString("ExportingPdf_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample demostrates how to export &lt;strong&gt;EventsCalendar&lt;/strong&gt; with 2 different modes:.
        /// </summary>
        internal static string ExportMode_Text0 {
            get {
                return ResourceManager.GetString("ExportMode_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The sample demonstrates how to import events from iCal format..
        /// </summary>
        internal static string Import_Text0 {
            get {
                return ResourceManager.GetString("Import_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Methods used in the sample:.
        /// </summary>
        internal static string Import_Text1 {
            get {
                return ResourceManager.GetString("Import_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please, note, after import operation, you should call SaveData() method in order to make data changes persistent..
        /// </summary>
        internal static string Import_Text2 {
            get {
                return ResourceManager.GetString("Import_Text2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample demonstrates how to localize the events calendar..
        /// </summary>
        internal static string Localization_Text0 {
            get {
                return ResourceManager.GetString("Localization_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Properties used in the sample:.
        /// </summary>
        internal static string Localization_Text1 {
            get {
                return ResourceManager.GetString("Localization_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample demonstrates the default &lt;strong&gt;C1EventsCalendar&lt;/strong&gt; behavior..
        /// </summary>
        internal static string Overview_Text0 {
            get {
                return ResourceManager.GetString("Overview_Text0", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The &lt;strong&gt;C1EventsCalendar&lt;/strong&gt; control is a fully functional schedule that allows users to add, edit, and manage their appointments..
        /// </summary>
        internal static string Overview_Text1 {
            get {
                return ResourceManager.GetString("Overview_Text1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to By default the C1EventsCalendar uses offline data source, so you can easily 
        ///				add this control to your page without additional configuration..
        /// </summary>
        internal static string Overview_Text2 {
            get {
                return ResourceManager.GetString("Overview_Text2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to This sample demonstrates how to customize the events calendar UI..
        /// </summary>
        internal static string UICustomization_Text0 {
            get {
                return ResourceManager.GetString("UICustomization_Text0", resourceCulture);
            }
        }
    }
}
