<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ClientSideEvents.aspx.cs" Inherits="ControlExplorer.C1FileExplorer.ClientSideEvents" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1FileExplorer" TagPrefix="cc1" %>
<%@ Register Src="../ClientLogger.ascx" TagName="ClientLogger" TagPrefix="ClientLogger" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
	
        function printMessage(type, data) {
            var membersInfo = "";
			if(data){
				for(var key in data){
					membersInfo = membersInfo ? membersInfo + "; " : " - ";
					membersInfo += key + ": " + data[key];
				}
			}
			
			log.message("<b>"+type+"</b>" + membersInfo);
        }

        function OnClientFileOpened(event, data) {
            printMessage("onClientFileOpened", data);
        }

        function OnClientFileOpening(event, data) {
            printMessage("OnClientFileOpening", data);
        }

        function OnClientItemCopying(event, data) {
            printMessage("OnClientItemCopying", data);
        }

        function OnClientItemCopied(event, data) {
            printMessage("OnClientItemCopied", data);
        }

        function OnClientItemRenaming(event, data) {
            printMessage("OnClientItemRenaming", data);
        }

        function OnClientItemRenamed(event, data) {
            printMessage("OnClientItemRenamed", data);
        }

        function OnClientItemDeleting(event, data) {
            printMessage("OnClientItemDeleting", data);
        }

        function OnClientItemDeleted(event, data) {
            printMessage("OnClientItemDeleted", data);
        }

        function OnClientItemPasting(event, data) {
            printMessage("OnClientItemPasting", data);
        }

        function OnClientItemPasted(event, data) {
            printMessage("OnClientItemPasted", data);
        }

        function OnClientItemMoving(event, data) {
            printMessage("OnClientItemMoving", data);
        }

        function OnClientItemMoved(event, data) {
            printMessage("OnClientItemMoved", data);
        }

        function OnClientNewFolderCreating(event, data) {
            printMessage("OnClientNewFolderCreating", data);
        }

        function OnClientNewFolderCreated(event, data) {
            printMessage("OnClientNewFolderCreated", data);
        }

        function OnClientFolderChanged(event, data) {
            printMessage("OnClientFolderChanged", data);
        }

        function OnClientFolderLoaded(event, data) {
            printMessage("OnClientFolderLoaded", data);
        }

        function OnClientErrorOccurred(event, data) {
            printMessage("OnClientErrorOccurred", data);
        }

        function OnClientItemSelected(event, data) {
            printMessage("OnClientItemSelected", data);
        }

        function OnClientFiltering(event, data) {
            printMessage("OnClientFiltering", data);
        }

        function OnClientFiltered(event, data) {
            printMessage("OnClientFiltered", data);
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <cc1:C1FileExplorer ID="C1FileExplorer1" runat="server" InitPath="~/C1FileExplorer/Example" 
        SearchPatterns="*.jpg,*.png,*.jpeg,*.gif" Width="800px" 
        OnClientFileOpened="OnClientFileOpened" OnClientFileOpening="OnClientFileOpening"
        OnClientItemCopying="OnClientItemCopying" OnClientItemCopied="OnClientItemCopied"
        OnClientItemRenaming="OnClientItemRenaming" OnClientItemRenamed="OnClientItemRenamed"
        OnClientItemDeleting="OnClientItemDeleting" OnClientItemDeleted="OnClientItemDeleted"
		OnClientItemPasting="OnClientItemPasting" OnClientItemPasted="OnClientItemPasted"
        OnClientItemMoving="OnClientItemMoving" OnClientItemMoved="OnClientItemMoved"
        OnClientNewFolderCreating="OnClientNewFolderCreating" OnClientNewFolderCreated="OnClientNewFolderCreated"
        OnClientFolderChanged="OnClientFolderChanged" OnClientFolderLoaded="OnClientFolderLoaded"
        OnClientErrorOccurred="OnClientErrorOccurred" OnClientItemSelected="OnClientItemSelected"
        OnClientFiltering="OnClientFiltering" OnClientFiltered="OnClientFiltered">
    </cc1:C1FileExplorer>
        <br />
	<ClientLogger:ClientLogger ID="ClientLogger1" runat="server"/>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1FileExplorer.ClientSideEvents_Text0 %></p>
    <ul>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li1 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li2 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li3 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li4 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li5 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li6 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li7 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li8 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li9 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li10 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li11 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li12 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li13 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li14 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li15 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li16 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li17 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li18 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li19 %></li>
        <li><%= Resources.C1FileExplorer.ClientSideEvents_Li20 %></li>
    </ul>
    <p><%= Resources.C1FileExplorer.ClientSideEvents_Text1 %></p>
</asp:Content>
