<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ServerSideEvents.aspx.cs" Inherits="ControlExplorer.C1FileExplorer.ServerSideEvents" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1FileExplorer" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <cc1:C1FileExplorer ID="C1FileExplorer1" runat="server" Width="800px" InitPath="~/C1FileExplorer/Example" SearchPatterns="*.jpg,*.png,*.jpeg,*.gif" >
    </cc1:C1FileExplorer>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1FileExplorer.ServerSideEvents_Text0 %></p>
    <p><%= Resources.C1FileExplorer.ServerSideEvents_Text1 %></p>
    <p><%= Resources.C1FileExplorer.ServerSideEvents_Text2 %></p>
    <p><%= Resources.C1FileExplorer.ServerSideEvents_Text3 %></p>
</asp:Content>
