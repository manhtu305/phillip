<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="KeyboardSupports.aspx.cs" Inherits="ControlExplorer.C1FileExplorer.KeyboardSupports" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1FileExplorer" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .settingcontainer .settingcontent li label {
            width: 120px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <cc1:C1FileExplorer ID="C1FileExplorer1" runat="server" Width="800px" InitPath="~/C1FileExplorer/Example" VisibleControls="All" AllowPaging="true" PageSize="2" SearchPatterns="*.jpg,*.png,*.jpeg,*.gif">
    </cc1:C1FileExplorer>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <label><%= Resources.C1FileExplorer.KeyboardSupports_TreePanelWidth %></label>
                    <wijmo:C1InputText ID="inputFocusFileExplorer" runat="server" Text="Ctrl+F2" Width="70px"></wijmo:C1InputText>
                </li>
                <li>
                    <label><%= Resources.C1FileExplorer.KeyboardSupports_FocusToolbar %></label>
                    <wijmo:C1InputText ID="inputFocusToolbar" runat="server" Text="Shift+1" Width="70px"></wijmo:C1InputText>
                </li>
                <li>
                    <label><%= Resources.C1FileExplorer.KeyboardSupports_FocusAddressBar %></label>
                    <wijmo:C1InputText ID="inputFocusAddressBar" runat="server" Text="Shift+2" Width="70px"></wijmo:C1InputText>
                </li>
                <li>
                    <label><%= Resources.C1FileExplorer.KeyboardSupports_FocusTreeView %></label>
                    <wijmo:C1InputText ID="inputFocusTreeView" runat="server" Text="Shift+3" Width="70px"></wijmo:C1InputText>
                </li>
                <li>
                    <label>FocusGrid</label>
                    <wijmo:C1InputText ID="inputFocusGrid" runat="server" Text="Shift+4" Width="70px"></wijmo:C1InputText>
                </li>
                <li>
                    <label><%= Resources.C1FileExplorer.KeyboardSupports_FocusPagingSlider %></label>
                    <wijmo:C1InputText ID="inputFocusPaging" runat="server" Text="Shift+5" Width="70px"></wijmo:C1InputText>
                </li>
                <li>
                    <label><%= Resources.C1FileExplorer.KeyboardSupports_ContextMenu %></label>
                    <wijmo:C1InputText ID="inputOpenContextMenu" runat="server" Text="Shift+M" Width="70px"></wijmo:C1InputText>
                </li>
                <li>
                    <label><%= Resources.C1FileExplorer.KeyboardSupports_Back %></label>
                    <wijmo:C1InputText ID="inputBack" runat="server" Text="Ctrl+K" Width="70px"></wijmo:C1InputText>
                </li>
                <li>
                    <label><%= Resources.C1FileExplorer.KeyboardSupports_Forward %></label>
                    <wijmo:C1InputText ID="inputForward" runat="server" Text="Ctrl+L" Width="70px"></wijmo:C1InputText>
                </li>
                <li>
                    <label><%= Resources.C1FileExplorer.KeyboardSupports_Open %></label>
                    <wijmo:C1InputText ID="inputOpen" runat="server" Text="Enter" Width="70px"></wijmo:C1InputText>
                </li>
                <li>
                    <label><%= Resources.C1FileExplorer.KeyboardSupports_Refresh %></label>
                    <wijmo:C1InputText ID="inputRefresh" runat="server" Text="Ctrl+F3" Width="70px"></wijmo:C1InputText>
                </li>
                <li>
                    <label><%= Resources.C1FileExplorer.KeyboardSupports_NewFolder %></label>
                    <wijmo:C1InputText ID="inputNewFolder" runat="server" Text="Shift+N" Width="70px"></wijmo:C1InputText>
                </li>
                <li>
                    <label><%= Resources.C1FileExplorer.KeyboardSupports_Delete %></label>
                    <wijmo:C1InputText ID="inputDelete" runat="server" Text="Delete" Width="70px"></wijmo:C1InputText>
                </li>
            </ul>
            <div class="settingcontrol">
                <asp:Button ID="btnApply" Text="<%$ Resources:C1FileExplorer, KeyboardSupports_Apply %>" CssClass="settingapply" runat="server" OnClick="btnApply_Click" />
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <div>
        <p><%= Resources.C1FileExplorer.KeyboardSupports_Text0 %></p>
        <p><%= Resources.C1FileExplorer.KeyboardSupports_Text1 %></p>
        <p><%= Resources.C1FileExplorer.KeyboardSupports_Text2 %></p>
    </div>
</asp:Content>
