<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ViewMode.aspx.cs" Inherits="ControlExplorer.C1FileExplorer.ViewMode" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1FileExplorer" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <cc1:C1FileExplorer ID="C1FileExplorer1" runat="server" Width="800px" InitPath="~/C1FileExplorer/Example" SearchPatterns="*.jpg,*.png,*.jpeg,*.gif">
    </cc1:C1FileExplorer>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <label class="settinglegend"><%= Resources.C1FileExplorer.ViewMode_ModeLabel %></label>
                    <asp:DropDownList ID="dplMode" runat="server">
                        <asp:ListItem Text="Default" Value="Default" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="FileTree" Value="FileTree"></asp:ListItem>
                    </asp:DropDownList>
                </li>
                <li>
                    <label class="settinglegend"><%= Resources.C1FileExplorer.ViewMode_ViewModeLabel %></label>
                    <asp:DropDownList ID="dplViewMode" runat="server">
                        <asp:ListItem Text="Detail" Value="Detail" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Thumbnail" Value="Thumbnail"></asp:ListItem>
                    </asp:DropDownList>
                </li>
            </ul>
            <div class="settingcontrol">
                <asp:Button ID="btnApply" Text="<%$ Resources:C1FileExplorer, ViewMode_Apply %>" CssClass="settingapply" runat="server" OnClick="btnApply_Click" />
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <div>
        <p><%= Resources.C1FileExplorer.ViewMode_Text0 %></p>
        <p><%= Resources.C1FileExplorer.ViewMode_Text1 %></p>

        <%= Resources.C1FileExplorer.ViewMode_Text2 %>
        <ul>
            <li>
                <%= Resources.C1FileExplorer.ViewMode_Li1 %>
            </li>
            <li>
                <%= Resources.C1FileExplorer.ViewMode_Li2 %>
            </li>
        </ul>

        <%= Resources.C1FileExplorer.ViewMode_Text3 %>
        <ul>
            <li>
                <%= Resources.C1FileExplorer.ViewMode_Li3 %>
            </li>
            <li>
                <%= Resources.C1FileExplorer.ViewMode_Li4 %>
            </li>
        </ul>
    </div>
</asp:Content>
