<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Functions.aspx.cs" Inherits="ControlExplorer.C1FileExplorer.Functions" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1FileExplorer" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>

        .settingcontent li{
            white-space: nowrap;
        }

    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <cc1:C1FileExplorer ID="C1FileExplorer1" runat="server" Width="800px" InitPath="~/C1FileExplorer/Example" SearchPatterns="*.*">
    </cc1:C1FileExplorer>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <asp:CheckBox ID="ckxEnableCopy" Text="<%$ Resources:C1FileExplorer, Functions_EnableCopyLabel %>" runat="server" Checked="true" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxEnableOpenFile" Text="<%$ Resources:C1FileExplorer, Functions_EnableOpenFileLabel %>" runat="server" Checked="true" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxAllowFileExtensionRename" Text="<%$ Resources:C1FileExplorer, Functions_AllowChangeFileExtensionLabel %>" runat="server" Checked="false" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxEnableCreateFolder" Text="<%$ Resources:C1FileExplorer, Functions_EnableCreateNewFolderLabel %>" runat="server" Checked="true" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxAllowMultipleSelection" Text="<%$ Resources:C1FileExplorer, Functions_AllowMultipleSelectionLabel %>" runat="server" Checked="false" />
                </li>
                <li>
                    <label><%= Resources.C1FileExplorer.Functions_TreePanelWidthLabel %></label>
                    <wijmo:C1InputNumeric ID="inputTreeWidth" DecimalPlaces="0" Value="200" runat="server"></wijmo:C1InputNumeric>
                </li>
            </ul>
            <div class="settingcontrol">
                <asp:Button ID="btnApply" Text="<%$ Resources:C1FileExplorer, Functions_ApplyText %>" CssClass="settingapply" runat="server" OnClick="btnApply_Click" />
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1FileExplorer.Functions_Text0 %></p>
    <p><%= Resources.C1FileExplorer.Functions_Text1 %></p>
</asp:Content>
