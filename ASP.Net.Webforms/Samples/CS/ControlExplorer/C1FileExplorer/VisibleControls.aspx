<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="VisibleControls.aspx.cs" Inherits="ControlExplorer.C1FileExplorer.VisibleControls" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1FileExplorer" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .settingcontent li{
            white-space: nowrap;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <cc1:C1FileExplorer ID="C1FileExplorer1" runat="server" Width="800px" InitPath="~/C1FileExplorer/Example" SearchPatterns="*.jpg,*.png,*.jpeg,*.gif">
    </cc1:C1FileExplorer>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <asp:CheckBox ID="ckxShowToolbar" Text="<%$ Resources:C1FileExplorer, VisibleControls_ShowToolbar %>" runat="server" Checked="true" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxShowAddressBox" Text="<%$ Resources:C1FileExplorer, VisibleControls_ShowAddressBox %>" runat="server" Checked="true" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxShowFilterTextBox" Text="<%$ Resources:C1FileExplorer, VisibleControls_ShowFilterTextBox %>" runat="server" Checked="true" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxShowTreeView" Text="<%$ Resources:C1FileExplorer, VisibleControls_ShowTreeView %>" runat="server" Checked="true" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxShowGrid" Text="<%$ Resources:C1FileExplorer, VisibleControls_ShowGrid %>" runat="server"  Checked="true" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxShowListView" Text="<%$ Resources:C1FileExplorer, VisibleControls_ShowListView %>" runat="server" Checked="true" />
                </li>
                <li>
                    <asp:CheckBox ID="ckxShowContextMenu" Text="<%$ Resources:C1FileExplorer, VisibleControls_ShowContextMenu %>" runat="server" Checked="true" />
                </li>
            </ul>
            <div class="settingcontrol">
                <asp:Button ID="btnApply" Text="<%$ Resources:C1FileExplorer, VisibleControls_Apply %>" CssClass="settingapply" runat="server" OnClick="btnApply_Click" />
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1FileExplorer.VisibleControls_Text0 %></p>
    <p><%= Resources.C1FileExplorer.VisibleControls_Text1 %></p>
</asp:Content>
