<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="PathSettings.aspx.cs" Inherits="ControlExplorer.C1FileExplorer.PathSettings" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1FileExplorer" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .settingcontainer .settingcontent .fullwidth label {
            width: 100px;
        }
        
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <cc1:C1FileExplorer ID="C1FileExplorer1" runat="server" Width="800px" ViewPaths="~/C1FileExplorer" InitPath="~/C1FileExplorer" SearchPatterns="*.jpg,*.png,*.jpeg,*.gif">
    </cc1:C1FileExplorer>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li class="fullwidth">
                    <label ><%= Resources.C1FileExplorer.PathSettings_ViewPathsLabel %></label>
                    <wijmo:C1InputText ID="inputViewPaths" runat="server" Text="~/C1FileExplorer/Example" Width="350px"></wijmo:C1InputText>
                </li>
                <li class="fullwidth">
                    <label><%= Resources.C1FileExplorer.PathSettings_InitPathLabel %></label>
                    <wijmo:C1InputText ID="inputInitPath" runat="server" Text="~/C1FileExplorer/Example" Width="350px"></wijmo:C1InputText>
                </li>
                <li class="fullwidth">
                    <label><%= Resources.C1FileExplorer.PathSettings_DeletePathsLabel %></label>
                    <wijmo:C1InputText ID="inputDeletePaths" runat="server" Text="~/C1FileExplorer/Example" Width="350px"></wijmo:C1InputText>
                </li>
                <li class="fullwidth">
                    <label><%= Resources.C1FileExplorer.PathSettings_SearchPatternsLabel %></label>
                    <wijmo:C1InputText ID="inputSearchPatterns" runat="server" Text="*.jpg,*.png,*.jpeg,*.gif" Width="350px"></wijmo:C1InputText>
                </li>
            </ul>
            <div class="settingcontrol">
                <asp:Button ID="btnApply" Text="<%$ Resources:C1FileExplorer, PathSettings_ApplyText %>" CssClass="settingapply" runat="server" OnClick="btnApply_Click" />
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">

    <p><%= Resources.C1FileExplorer.PathSettings_Text0 %></p>
    <ul>
        <li>
            <%= Resources.C1FileExplorer.PathSettings_Li1 %>
        </li>
        <li>
            <%= Resources.C1FileExplorer.PathSettings_Li2 %>
        </li>
        <li>
            <%= Resources.C1FileExplorer.PathSettings_Li3 %>
        </li>
        <li>
            <%= Resources.C1FileExplorer.PathSettings_Li4 %>
        </li>
    </ul>

</asp:Content>
