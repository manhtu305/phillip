<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Annotation.aspx.cs" Inherits="ControlExplorer.C1ScatterChart.Annotation" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1ScatterChart ID="C1ScatterChart1" runat="server" Height="475" Width="756">
		<Hint IsContentHtml="true">
			<Content Function="hintContent" />
		</Hint>
		<SeriesTransition Duration="2000" Enabled="false"></SeriesTransition>
		<Animation Duration="2000" Enabled="false"></Animation>
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#AFE500">
				<Fill Color="#AFE500">
				</Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#FF9900">
				<Fill Color="#FF9900">
				</Fill>
			</wijmo:ChartStyle>
		</SeriesStyles>
		<Header Text="<%$ Resources:C1ScatterChart, Annotation_HeaderText %>">
		</Header>
		<Footer Compass="South" Visible="False">
		</Footer>
		<Legend Visible="true">
			<Size Width="30" Height="3"></Size>
		</Legend>
		<Axis>
			<X Text="<%$ Resources:C1ScatterChart, Annotation_AxisXText %>">
				<Labels>
					<AxisLabelStyle FontSize="11pt" Rotation="-45">
						<Fill Color="#7F7F7F">
						</Fill>
					</AxisLabelStyle>
				</Labels>
				<GridMajor Visible="false"></GridMajor>
				<TickMajor Position="Outside">
					<TickStyle Stroke="#7F7F7F" />
				</TickMajor>
			</X>
			<Y Compass="West" Text="<%$ Resources:C1ScatterChart, Annotation_AxisYText %>" Visible="true">
				<Labels TextAlign="Center">
					<AxisLabelStyle FontSize="11pt">
						<Fill Color="#7F7F7F">
						</Fill>
					</AxisLabelStyle>
				</Labels>
				<GridMajor Visible="True">
					<GridStyle Stroke="#353539" StrokeDashArray="- "></GridStyle>
				</GridMajor>
				<TickMajor Position="Outside">
					<TickStyle Stroke="#7F7F7F" />
				</TickMajor>
				<TickMinor Position="Outside">
					<TickStyle Stroke="#7F7F7F" />
				</TickMinor>
			</Y>
		</Axis>
		<Annotations>
			<wijmo:TextAnnotation Attachment="Relative" Offset="0, 0" Text="Relative" 
				Tooltip="<%$ Resources:C1ScatterChart, Annotation_Tooltip1 %>">
				<Point>
					<X DoubleValue="0.75" />
					<Y DoubleValue="0.15" />
				</Point>
				<AnnotationStyle FontSize="26" FontWeight="Bold">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:TextAnnotation>
			<wijmo:RectAnnotation Attachment="DataIndex" Content="DataIndex" Width="100" Offset="0, 0" PointIndex="1" 
				Tooltip="<%$ Resources:C1ScatterChart, Annotation_Tooltip2 %>">
				<AnnotationStyle FillOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:RectAnnotation>
			<wijmo:EllipseAnnotation Attachment="Relative" Content="Relative" Height="35" Offset="0, 0" 
				Tooltip="<%$ Resources:C1ScatterChart, Annotation_Tooltip3 %>" Width="75">
				<Point>
					<X DoubleValue="0.4" />
					<Y DoubleValue="0.45" />
				</Point>
				<AnnotationStyle FillOpacity="1" Stroke="#c2955f" StrokeWidth="2" StrokeOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:EllipseAnnotation>
			<wijmo:CircleAnnotation Attachment="DataIndex" PointIndex="1" Radius="40" SeriesIndex="1" Content="DataIndex" Offset="0, 0" 
				Tooltip="<%$ Resources:C1ScatterChart, Annotation_Tooltip4 %>">
				<AnnotationStyle FillOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:CircleAnnotation>
			<wijmo:ImageAnnotation Attachment="DataIndex" PointIndex="2" Href="./../Images/c1icon.png" Offset="0, 0" Content=" " 
				Tooltip="<%$ Resources:C1ScatterChart, Annotation_Tooltip5 %>">
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:ImageAnnotation>
			<wijmo:PolygonAnnotation Offset="0, 0" Tooltip="<%$ Resources:C1ScatterChart, Annotation_Tooltip6 %>" Content="Absolute">
				<Points>
					<wijmo:PointF X="200" Y="0" />
					<wijmo:PointF X="150" Y="50" />
					<wijmo:PointF X="175" Y="100" />
					<wijmo:PointF X="225" Y="100" />
					<wijmo:PointF X="250" Y="50" />
				</Points>
				<AnnotationStyle FillOpacity="2" Stroke="#01A9DB" StrokeWidth="4" StrokeOpacity="1">
					<Fill Color="#FFFFFF"> </Fill >
				</AnnotationStyle>
			</wijmo:PolygonAnnotation>
			<wijmo:SquareAnnotation Attachment="DataIndex" Width="40" Content="DataIndex" Offset="0, 0" PointIndex="4"
				Tooltip="<%$ Resources:C1ScatterChart, Annotation_Tooltip7 %>">
				<AnnotationStyle FillOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:SquareAnnotation>
			<wijmo:LineAnnotation Content="Absolute" End="350, 300" Offset="0, 0" Start="50, 150" 
				Tooltip="<%$ Resources:C1ScatterChart, Annotation_Tooltip8 %>">
				<AnnotationStyle FillOpacity="2" StrokeWidth="4" Stroke="#01A9DB">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:LineAnnotation>
		</Annotations>
	</wijmo:C1ScatterChart>
	<script type="text/javascript">
		function hintContent() {
			return this.x + ' cm, ' + this.y + ' kg';
		}
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1ScatterChart.Annotation_Text0 %></p>
	<h3><%= Resources.C1ScatterChart.Annotation_Text1 %></h3>
	<ul>
		<li><%= Resources.C1ScatterChart.Annotation_Li1 %></li>
		<li><%= Resources.C1ScatterChart.Annotation_Li2 %></li>
	</ul>
</asp:Content>
