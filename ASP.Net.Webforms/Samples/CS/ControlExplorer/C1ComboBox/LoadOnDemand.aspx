<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="LoadOnDemand.aspx.cs" Inherits="ControlExplorer.C1ComboBox.LoadOnDemand" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<h3><%= Resources.C1ComboBox.LoadOnDemand_H1 %></h3>
	<wijmo:C1ComboBox ID="C1ComboBox1" runat="server" Width="160px" CallbackDataPopulate="ItemPopulate"
		EnableCallBackMode="True" OnItemPopulate="C1ComboBox1_ItemPopulate">
		<Items>
			<wijmo:C1ComboBoxItem Text="c++" Value="c++" />
			<wijmo:C1ComboBoxItem Text="java" Value="java" />
			<wijmo:C1ComboBoxItem Text="php" Value="php" />
			<wijmo:C1ComboBoxItem Text="coldfusion" Value="coldfusion" />
			<wijmo:C1ComboBoxItem Text="javascript" Value="javascript" />
			<wijmo:C1ComboBoxItem Text="asp" Value="asp" />
			<wijmo:C1ComboBoxItem Text="ruby" Value="ruby" />
			<wijmo:C1ComboBoxItem Text="python" Value="python" />
			<wijmo:C1ComboBoxItem Text="c" Value="c" />
			<wijmo:C1ComboBoxItem Text="scala" Value="scala" />
			<wijmo:C1ComboBoxItem Text="groovy" Value="groovy" />
			<wijmo:C1ComboBoxItem Text="haskell" Value="haskell" />
			<wijmo:C1ComboBoxItem Text="perl" Value="perl" />
		</Items>
	</wijmo:C1ComboBox>
		<h3><%= Resources.C1ComboBox.LoadOnDemand_H2 %></h3>
	<wijmo:C1ComboBox ID="C1ComboBox2" runat="server" Width="160px"  
		EnableCallBackMode="True"  oncallbackdatabind="C1ComboBox2_CallbackDataBind" DataTextField="Text" DataValueField="Value" DataSelectedField="Selected"  >
	</wijmo:C1ComboBox>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

		<p><%= Resources.C1ComboBox.LoadOnDemand_Text0 %></p>

		<p><%= Resources.C1ComboBox.LoadOnDemand_Text1 %></p>

		<p><%= Resources.C1ComboBox.LoadOnDemand_Text2 %></p>
		<ul>
		<li>EnableCallBackMode: true</li>
		<li>CallbackDataPopulate:ItemPopulate</li>
		<li>OnItemPopulate</li>
	  </ul>
	  <p><%= Resources.C1ComboBox.LoadOnDemand_Text3 %></p>
		<p><%= Resources.C1ComboBox.LoadOnDemand_Text4 %></p>
		<p><%= Resources.C1ComboBox.LoadOnDemand_Text5 %></p>
		<ul>
		<li>EnableCallBackMode: true</li>
		<li>OnCallbackdatabind </li>
		</ul>
		<p><%= Resources.C1ComboBox.LoadOnDemand_Text6 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
