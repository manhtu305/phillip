<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="Animation.aspx.cs" Inherits="ControlExplorer.C1ComboBox.Animation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
		<style type ="text/css">
			.ui-effects-explode
			{
				z-index:99999;
			}
		</style>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
	<wijmo:C1ComboBox ID="C1ComboBox1" runat="server" Width="160px">
	<ShowingAnimation Duration="1000">
	<Animated Effect="Scale" />
	</ShowingAnimation>
	<HidingAnimation Duration="1000">
	<Animated Effect="explode" />
	</HidingAnimation>
		<Items>
			<wijmo:C1ComboBoxItem Text="c++" Value="c++" />
			<wijmo:C1ComboBoxItem Text="java" Value="java" />
			<wijmo:C1ComboBoxItem Text="php" Value="php" />
			<wijmo:C1ComboBoxItem Text="coldfusion" Value="coldfusion" />
			<wijmo:C1ComboBoxItem Text="javascript" Value="javascript" />
			<wijmo:C1ComboBoxItem Text="asp" Value="asp" />
			<wijmo:C1ComboBoxItem Text="ruby" Value="ruby" />
			<wijmo:C1ComboBoxItem Text="python" Value="python" />
			<wijmo:C1ComboBoxItem Text="c" Value="c" />
			<wijmo:C1ComboBoxItem Text="scala" Value="scala" />
			<wijmo:C1ComboBoxItem Text="groovy" Value="groovy" />
			<wijmo:C1ComboBoxItem Text="haskell" Value="haskell" />
			<wijmo:C1ComboBoxItem Text="perl" Value="perl" />
		</Items>
	</wijmo:C1ComboBox>
				</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1ComboBox.Animation_Text0 %></p>
	<p><%= Resources.C1ComboBox.Animation_Text1 %></p>
	<ul>
	<li><%= Resources.C1ComboBox.Animation_Li1 %></li>
	<li><%= Resources.C1ComboBox.Animation_Li2 %></li>
	</ul>
    <p><%= Resources.C1ComboBox.Animation_Text2 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
			<ContentTemplate>
<div class="settingcontainer">
<div class="settingcontent">
<ul>
	<li class="fullwidth"><label class="settinglegend"><%= Resources.C1ComboBox.Animation_AnimationLabel %></label>
	<li><label><%= Resources.C1ComboBox.Animation_ShowingSpeedLabel %></label>
		<asp:TextBox runat="server" id="ShowingSpeedTxt" text="1000" />
	</li>
	<li>
		<label><%= Resources.C1ComboBox.Animation_ShowingEffectLabel %></label>
			<asp:DropDownList id="ShowingEffectTypesDdl" runat="server">
				<asp:ListItem value="none">none</asp:ListItem>
				<asp:ListItem value="blind">Blind</asp:ListItem>
				<asp:ListItem value="bounce">Bounce</asp:ListItem>
				<asp:ListItem value="clip">Clip</asp:ListItem>
				<asp:ListItem value="drop">Drop</asp:ListItem>
				<asp:ListItem value="explode">Explode</asp:ListItem>
				<asp:ListItem value="fade">Fade</asp:ListItem>
				<asp:ListItem value="fold">Fold</asp:ListItem>
				<asp:ListItem value="highlight">Highlight</asp:ListItem>
				<asp:ListItem value="puff">Puff</asp:ListItem>
				<asp:ListItem value="pulsate">Pulsate</asp:ListItem>
				<asp:ListItem value="scale" selected="true">Scale</asp:ListItem>
				<asp:ListItem value="shake">Shake</asp:ListItem>
				<asp:ListItem value="size">Size</asp:ListItem>
				<asp:ListItem value="slide">Slide</asp:ListItem>
			</asp:DropDownList>
	</li>
	<li><label><%= Resources.C1ComboBox.Animation_HidingSpeedLabel %></label>
		<asp:TextBox runat="server" id="HidingSpeedTxt" text="1000" />
	</li>
	<li>
		<label><%= Resources.C1ComboBox.Animation_HidingEffectLabel %></label>
			<asp:DropDownList id="HidingEffectTypesDdl" runat="server">
				<asp:ListItem value="none">none</asp:ListItem>
				<asp:ListItem value="blind">Blind</asp:ListItem>
				<asp:ListItem value="bounce">Bounce</asp:ListItem>
				<asp:ListItem value="clip">Clip</asp:ListItem>
				<asp:ListItem value="drop">Drop</asp:ListItem>
				<asp:ListItem value="explode">Explode</asp:ListItem>
				<asp:ListItem value="fade">Fade</asp:ListItem>
				<asp:ListItem value="fold">Fold</asp:ListItem>
				<asp:ListItem value="highlight">Highlight</asp:ListItem>
				<asp:ListItem value="puff">Puff</asp:ListItem>
				<asp:ListItem value="pulsate">Pulsate</asp:ListItem>
				<asp:ListItem value="scale" selected="true">Scale</asp:ListItem>
				<asp:ListItem value="shake">Shake</asp:ListItem>
				<asp:ListItem value="size">Size</asp:ListItem>
				<asp:ListItem value="slide">Slide</asp:ListItem>
			</asp:DropDownList>
	</li>
</ul>
</div>
	<div class="settingcontrol">
	<asp:Button ID="ApplyBtn" Text="<%$ Resources:C1ComboBox, Animation_ApplyText %>" CssClass="settingapply" runat="server" OnClick="ApplyBtn_Click"/>
	</div>
</div>
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
