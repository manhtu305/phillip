<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="DataBinding.aspx.cs" Inherits="ControlExplorer.C1ComboBox.DataBinding" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1ComboBox ID="C1ComboBox1" runat="server" Width="160px" 
		DataSourceID="AccessDataSource1" DataTextField="UnitPrice" 
		DataTextFormatString="{0:C}" DataValueField="OrderID">
	</wijmo:C1ComboBox>
	<asp:AccessDataSource ID="AccessDataSource1" runat="server" DataFile="~/App_Data/C1NWind.mdb"
		SelectCommand="SELECT top 100 [UnitPrice], [OrderID] FROM [Order Details]"></asp:AccessDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
		<p><%= Resources.C1ComboBox.DataBinding_Text0 %></p>

		<p><%= Resources.C1ComboBox.DataBinding_Text1 %></p>
		<ul>
		<li><%= Resources.C1ComboBox.DataBinding_Li1 %></li>
		<li><%= Resources.C1ComboBox.DataBinding_Li2 %></li>
		<li><%= Resources.C1ComboBox.DataBinding_Li3 %></li>
		</ul>
		<p><%= Resources.C1ComboBox.DataBinding_Text2 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
