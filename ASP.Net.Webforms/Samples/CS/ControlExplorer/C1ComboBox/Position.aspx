<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="Position.aspx.cs" Inherits="ControlExplorer.C1ComboBox.Position" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<script id="scriptInit" type="text/javascript">
		function changed() {
			$("#<%=C1ComboBox1.ClientID%>").c1combobox('option', 'dropDownListPosition', {
				my: $('#my_horizontal').val() + ' ' + $('#my_vertical').val(),
				at: $('#at_horizontal').val() + ' ' + $('#at_vertical').val(),
				offset: $('#offset').val(),
				collision: $("#collision_horizontal").val() + ' ' + $("#collision_vertical").val()
			});
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
	<wijmo:C1ComboBox ID="C1ComboBox1" runat="server" Width="160px" >
	<DropDownListPosition >
		<At Left="Center" Top="Bottom" />
		<My Left="Right" Top="Top" />
	</DropDownListPosition>
		<Items>
			<wijmo:C1ComboBoxItem Text="c++" Value="c++" />
			<wijmo:C1ComboBoxItem Text="java" Value="java" />
			<wijmo:C1ComboBoxItem Text="php" Value="php" />
			<wijmo:C1ComboBoxItem Text="coldfusion" Value="coldfusion" />
			<wijmo:C1ComboBoxItem Text="javascript" Value="javascript" />
			<wijmo:C1ComboBoxItem Text="asp" Value="asp" />
			<wijmo:C1ComboBoxItem Text="ruby" Value="ruby" />
			<wijmo:C1ComboBoxItem Text="python" Value="python" />
			<wijmo:C1ComboBoxItem Text="c" Value="c" />
			<wijmo:C1ComboBoxItem Text="scala" Value="scala" />
			<wijmo:C1ComboBoxItem Text="groovy" Value="groovy" />
			<wijmo:C1ComboBoxItem Text="haskell" Value="haskell" />
			<wijmo:C1ComboBoxItem Text="perl" Value="perl" />
		</Items>
	</wijmo:C1ComboBox>
	<script type="text/javascript">
		$(document).ready(function () {
			$('.position').bind('change', changed);
		});
	</script>
				</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1ComboBox.Position_Text0 %></p>
		<ul>
	<li><%= Resources.C1ComboBox.Position_Li1 %></li>
	</ul>

	<p><%= Resources.C1ComboBox.Position_Text1 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
<ContentTemplate>
<div class="settingcontainer">
<div class="settingcontent">
<ul>
	<li class="fullwidth"><label class="settinglegend"><%= Resources.C1ComboBox.Position_SettingsTitle1 %></label>
	<li class="widesetting"><label><%= Resources.C1ComboBox.Position_DropDownListLabel %></label>
		<asp:DropDownList ID="MyHPDdl" runat="server">
				<asp:ListItem value="Left" Text="<%$ Resources:C1ComboBox, Position_HorizontalLeft %>"></asp:ListItem>
				<asp:ListItem value="Center" Text="<%$ Resources:C1ComboBox, Position_HorizontalCenter %>" Selected="True"></asp:ListItem>
				<asp:ListItem value="Right" Text="<%$ Resources:C1ComboBox, Position_HorizontalRight %>"></asp:ListItem>
			</asp:DropDownList>
			<asp:DropDownList ID="MyVPDdl" runat="server">
				<asp:ListItem value="Top" Text="<%$ Resources:C1ComboBox, Position_VerticalTop %>"></asp:ListItem>
				<asp:ListItem value="Center" Text="<%$ Resources:C1ComboBox, Position_VerticalCenter %>"></asp:ListItem>
				<asp:ListItem value="Bottom" Text="<%$ Resources:C1ComboBox, Position_VerticalBottom %>" Selected="True"></asp:ListItem>
			</asp:DropDownList>
	</li>
	<li class="widesetting">
		<label><%= Resources.C1ComboBox.Position_AlignsToTextboxLabel %></label>
			<asp:DropDownList ID="AtHPDdl" runat="server">
				<asp:ListItem value="Left" Text="<%$ Resources:C1ComboBox, Position_HorizontalLeft %>"></asp:ListItem>
				<asp:ListItem value="Center" Text="<%$ Resources:C1ComboBox, Position_HorizontalCenter %>"></asp:ListItem>
				<asp:ListItem value="Right" Text="<%$ Resources:C1ComboBox, Position_HorizontalRight %>" Selected="True"></asp:ListItem>
			</asp:DropDownList>
			<asp:DropDownList ID="AtVPDdl" runat="server">
				<asp:ListItem value="Top" Text="<%$ Resources:C1ComboBox, Position_VerticalTop %>" Selected="True"></asp:ListItem>
				<asp:ListItem value="Center" Text="<%$ Resources:C1ComboBox, Position_VerticalCenter %>"></asp:ListItem>
				<asp:ListItem value="Bottom" Text="<%$ Resources:C1ComboBox, Position_VerticalBottom %>"></asp:ListItem>
			</asp:DropDownList>
	</li>
	<li class="widesetting">
		<label><%= Resources.C1ComboBox.Position_HorizontalCollisionDetectionLabel %></label>
			<asp:DropDownList ID="CollisionLeftDdl" runat="server">
				<asp:ListItem value="Flip" Text="<%$ Resources:C1ComboBox, Position_DirectionFlip %>"></asp:ListItem>
				<asp:ListItem value="Fit" Text="<%$ Resources:C1ComboBox, Position_DirectionFit %>" Selected="True"></asp:ListItem>
				<asp:ListItem value="None" Text="<%$ Resources:C1ComboBox, Position_DirectionNone %>"></asp:ListItem>
			</asp:DropDownList>
	</li>
	<li class="widesetting">
		<label><%= Resources.C1ComboBox.Position_VerticalCollisionDetectionLabel %></label>
			<asp:DropDownList ID="CollisionTopDdl" runat="server">
				<asp:ListItem value="Flip" Text="<%$ Resources:C1ComboBox, Position_DirectionFlip %>" Selected="True"></asp:ListItem>
				<asp:ListItem value="Fit" Text="<%$ Resources:C1ComboBox, Position_DirectionFit %>"></asp:ListItem>
				<asp:ListItem value="None" Text="<%$ Resources:C1ComboBox, Position_DirectionNone %>"></asp:ListItem>
			</asp:DropDownList>
	</li>
	<li><label><%= Resources.C1ComboBox.Position_OffsetXLabel %></label>
		<asp:TextBox runat="server" id="XOffsetTxt" text="0" />
	</li>
	<li><label><%= Resources.C1ComboBox.Position_OffsetYLabel %></label>
		<asp:TextBox runat="server" id="YOffsetTxt" text="0" />
	</li>
</ul>
</div>
	<div class="settingcontrol">
	<asp:Button ID="ApplyBtn" Text="<%$ Resources:C1ComboBox, Position_ApplyText %>" CssClass="settingapply" runat="server" OnClick="ApplyBtn_Click"/>
	</div>
</div>
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
