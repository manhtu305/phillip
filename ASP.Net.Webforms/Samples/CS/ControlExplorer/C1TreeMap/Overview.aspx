<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Overview.aspx.cs" MasterPageFile="~/Wijmo.Master"
	Inherits="ControlExplorer.C1TreeMap.Overview" %>

<%@ Register Namespace="C1.Web.Wijmo.Controls.C1TreeMap" Assembly="C1.Web.Wijmo.Controls.45"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<script type="text/javascript">
		function labelFormatter() {
			return "Country: " + this.label + "<br/> GDP: " + this.value + " $BN";
		}
	</script>
</asp:Content>
<asp:Content runat="server" ID="MainContent" ContentPlaceHolderID="MainContent">
	<asp:ScriptManager ID="ScriptManager1" runat="server">
	</asp:ScriptManager>
	<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<wijmo:c1treemap runat="server" id="C1TreeMap1" maxcolor="#ff0000" mincolor="#ffaaaa"
				mincolorvalue="0" height="600" Width="834" labelformatter="labelFormatter">
			<Items>
				<wijmo:TreeMapItem Label="North America" Value="18625">
					<Items>
						<wijmo:TreeMapItem Label="United States" Value="16800"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Canada" Value="1825"></wijmo:TreeMapItem>
					</Items>
				</wijmo:TreeMapItem>
				<wijmo:TreeMapItem Label="Asia" Value="18934">
					<Items>
						<wijmo:TreeMapItem Label="China" Value="9240"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Japan" Value="4901"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="India" Value="1876"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="South Korea" Value="1304"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Indonesia" Value="868"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Saudi Arabia" Value="745"></wijmo:TreeMapItem>
					</Items>
				</wijmo:TreeMapItem>
				<wijmo:TreeMapItem Label="Europe" Value="16685">
					<Items>
						<wijmo:TreeMapItem Label="Germany" Value="3634"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="France" Value="2734"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="United Kingdom" Value="2522"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Russia" Value="2096"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Italy" Value="2071"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Spain" Value="1358"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Turkey" Value="820"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Netherlands" Value="800"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Switzerland" Value="650"></wijmo:TreeMapItem>
					</Items>
				</wijmo:TreeMapItem>
				<wijmo:TreeMapItem Label="South America" Value="4554">
					<Items>
						<wijmo:TreeMapItem Label="Brazil" Value="2245"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Mexico" Value="1260"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Argentina" Value="611"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Venezuela" Value="438"></wijmo:TreeMapItem>
					</Items>
				</wijmo:TreeMapItem>
				<wijmo:TreeMapItem Label="Australasia & Oceania" Value="1742">
					<Items>
						<wijmo:TreeMapItem Label="Australia" Value="1560"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="New Zealand" Value="182"></wijmo:TreeMapItem>
					</Items>
				</wijmo:TreeMapItem>
				<wijmo:TreeMapItem Label="Africa" Value="872">
					<Items>
						<wijmo:TreeMapItem Label="Nigeria" Value="522"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="South Africa" Value="350"></wijmo:TreeMapItem>
					</Items>
				</wijmo:TreeMapItem>
			</Items>
		</wijmo:c1treemap>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1TreeMap.Overview_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<asp:UpdatePanel ID="UpdatePanel2" runat="server">
		<ContentTemplate>
			<label><%= Resources.C1TreeMap.Overview_Type %></label>
			<asp:RadioButtonList runat="server" ID="rblType" OnSelectedIndexChanged="rblType_SelectedIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal">
				<asp:ListItem Text="<%$ Resources:C1TreeMap, Overview_Squarified %>" Value="squarified" Selected="True"></asp:ListItem>
				<asp:ListItem Text="<%$ Resources:C1TreeMap, Overview_Horizontal %>" Value="horizontal"></asp:ListItem>
				<asp:ListItem Text="<%$ Resources:C1TreeMap, Overview_Vertical %>" Value="vertical"></asp:ListItem>
			</asp:RadioButtonList>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
