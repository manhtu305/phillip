<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExternalData.aspx.cs" MasterPageFile="~/Wijmo.Master"
	Inherits="ControlExplorer.C1TreeMap.ExternalData" %>

<%@ Register Namespace="C1.Web.Wijmo.Controls.C1TreeMap" Assembly="C1.Web.Wijmo.Controls.45"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<script type="text/javascript">
		var dataCount = 0,
			treemapData = [];

		$(function () {
			$.ajax({
				url: "https://services.odata.org/Northwind/Northwind.svc/Categories?$format=json&$expand=Products",
				crossOrigin: true,
				success: function (result) {
					$.each(result.value, function (idx, r) {
						var data = {
							name: r.CategoryName,
							count: 0,
							items: []
						};

						$.each(r.Products, function (i, p) {
							var count = p.UnitsInStock,
								d = {
									name: p.ProductName,
									count: count
								};
							data.items[i] = d;
							data.count += count;
						});

						treemapData.push(data);
					});
					createTreemap();
				}
			});
		});

		function createTreemap() {
			$('#<%=C1TreeMap1.ClientID%>').c1treemap({
				showTooltip: true,
				valueBinding: "count",
				labelBinding: "name",
				data: treemapData
			});
		}
	</script>
</asp:Content>
<asp:Content runat="server" ID="MainContent" ContentPlaceHolderID="MainContent">
	<wijmo:c1treemap runat="server" id="C1TreeMap1" Width="95%" Height="600px">
	</wijmo:c1treemap>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1TreeMap.ExternalData_Text0 %></p>
	<ul>
		<li><%= Resources.C1TreeMap.ExternalData_Li1 %></li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
