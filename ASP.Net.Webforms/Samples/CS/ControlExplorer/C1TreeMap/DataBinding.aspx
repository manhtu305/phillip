<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="DataBinding.aspx.cs" Inherits="ControlExplorer.C1TreeMap.DataBinding" %>
<%@ Register Namespace="C1.Web.Wijmo.Controls.C1TreeMap" Assembly="C1.Web.Wijmo.Controls.45" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<script type="text/javascript">
		function labelFormatter() {
			return "Country: " + this.label + "<br/> GDP: " + this.value + " $BN";
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1TreeMap runat="server" ID="C1TreeMap1" MaxColor="#ff0000" MinColor="#ffaaaa" MinColorValue="0" DataSourceID="XmlDataSource1" Height="600" Width="834" LabelFormatter="labelFormatter">
		<DataBindings>
			<wijmo:C1TreeMapItemBinding LabelField="label" ValueField="value" />
		</DataBindings>
	</wijmo:C1TreeMap>
	<asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="~/App_Data/treemap_structure.xml" XPath="root/treeMapItem"></asp:XmlDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1TreeMap.DataBinding_Text0 %></p>
</asp:Content>
