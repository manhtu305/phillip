﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ControlExplorer.C1TreeMap
{
	public partial class Overview : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void rblType_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch (rblType.SelectedValue) 
			{
				case "squarified":
					C1TreeMap1.Type = C1.Web.Wijmo.Controls.C1TreeMap.TreeMapType.Squarified;
					break;
				case "horizontal":
					C1TreeMap1.Type = C1.Web.Wijmo.Controls.C1TreeMap.TreeMapType.Horizontal;
					break;
				case "vertical":
					C1TreeMap1.Type = C1.Web.Wijmo.Controls.C1TreeMap.TreeMapType.Vertical;
					break;
			}
			UpdatePanel1.Update();
		}
	}
}