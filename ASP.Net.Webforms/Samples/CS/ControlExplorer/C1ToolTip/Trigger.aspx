<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Trigger.aspx.cs" Inherits="ControlExplorer.C1ToolTip.Trigger" %>

<%@ Register Namespace="C1.Web.Wijmo.Controls.C1ToolTip" Assembly="C1.Web.Wijmo.Controls.45" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .link
        {
            color: #000000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h3 class="ui-helper-reset ui-widget-header ui-corner-all" style="padding: 1em;">
                <asp:HyperLink runat="server" ID="HyperLink1" NavigateUrl="#" ToolTip="<%$ Resources:C1ToolTip, Trigger_Tooltip %>" CssClass="link ui-widget ui-corner-all">Anchor</asp:HyperLink>
            </h3>
            <wijmo:C1ToolTip runat="server" ID="Tooltip1" TargetSelector="#HyperLink1" CloseBehavior="Sticky">
            </wijmo:C1ToolTip>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script language="javascript">
        function FocusOnAnchor() {
            $("#<%=HyperLink1.ClientID %>").focus();
        }

        function ShowTooltip() {
            $("#<%=HyperLink1.ClientID %>").c1tooltip("show");
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1ToolTip.Trigger_Text0 %></p>
    <p><%= Resources.C1ToolTip.Trigger_Text1 %></p>
    <ul>
        <li><%= Resources.C1ToolTip.Trigger_Li1 %></li>
        <li><%= Resources.C1ToolTip.Trigger_Li2 %></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div class="settingcontainer">
                <div class="settingcontent">
                    <ul>
                        <li class="fullwidth">
                            <label><%= Resources.C1ToolTip.Trigger_Trigger %></label>
                            <asp:DropDownList ID="trigger" runat="server" AutoPostBack="true" OnSelectedIndexChanged="trigger_SelectedIndexChanged">
                                <asp:ListItem Selected="True">Hover</asp:ListItem>
                                <asp:ListItem>Click</asp:ListItem>
                                <asp:ListItem>Focus</asp:ListItem>
                                <asp:ListItem>RightClick</asp:ListItem>
                                <asp:ListItem>Custom</asp:ListItem>
                            </asp:DropDownList>
                        </li>
                        <li class="longbutton">
                            <input type="button" value="<%= Resources.C1ToolTip.Trigger_FocusOnAnchor %>" onclick="FocusOnAnchor()"/>
                        </li>
                        <li class="longbutton">
                            <input type="button" value="<%= Resources.C1ToolTip.Trigger_ShowTooltip %>" onclick="ShowTooltip()"/>
                        </li>
                    </ul>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
