<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CloseBehavior.aspx.cs" Inherits="ControlExplorer.C1ToolTip.CloseBehavior" %>

<%@ Register Namespace="C1.Web.Wijmo.Controls.C1ToolTip" Assembly="C1.Web.Wijmo.Controls.45" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .link
        {
            color: #000000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h3 class="ui-helper-reset ui-widget-header ui-corner-all" style="padding: 1em;">
        <asp:HyperLink ID="HyperLink1" runat="server" ToolTip="<%$ Resources:C1ToolTip, CloseBehavior_Tooltip %>" CssClass="link ui-widget ui-corner-all">Anchor</asp:HyperLink>
    </h3>

            <wijmo:C1ToolTip runat="server" ID="Tooltip1" TargetSelector="#HyperLink1">
            </wijmo:C1ToolTip>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1ToolTip.CloseBehavior_Text0 %></p>
    <p><%= Resources.C1ToolTip.CloseBehavior_Text1 %></p>
    <p><%= Resources.C1ToolTip.CloseBehavior_Text2 %></p>
    <ul>
        <li><%= Resources.C1ToolTip.CloseBehavior_Li1 %></li>
        <li><%= Resources.C1ToolTip.CloseBehavior_Li2 %></li>
        <li><%= Resources.C1ToolTip.CloseBehavior_Li3 %></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <label><%= Resources.C1ToolTip.CloseBehavior_CloseBehavior %></label>
                    <asp:DropDownList ID="closeBehavior" runat="server">
                        <asp:ListItem Selected="True" Text ="<%$ Resources:C1ToolTip, CloseBehavior_Auto %>" Value="auto"></asp:ListItem>
                        <asp:ListItem Text ="<%$ Resources:C1ToolTip, CloseBehavior_None %>" Value="none"></asp:ListItem>
                        <asp:ListItem Text ="<%$ Resources:C1ToolTip, CloseBehavior_Sticky %>" Value="sticky"></asp:ListItem>
                    </asp:DropDownList>
                </li>
            </ul>
        </div>
    </div>        
    <script id="scriptInit" type="text/javascript">
        $(document).ready(function () {
            $("#<%=closeBehavior.ClientID %>").change(function () {
                $("#<%=HyperLink1.ClientID %>").c1tooltip("option", "closeBehavior", $("#<%=closeBehavior.ClientID %>").val());
            });

        });
    </script>
</asp:Content>
