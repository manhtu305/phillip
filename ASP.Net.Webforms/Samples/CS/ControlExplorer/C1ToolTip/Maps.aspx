<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Maps.aspx.cs" Inherits="ControlExplorer.C1ToolTip.Maps" %>

<%@ Register Namespace="C1.Web.Wijmo.Controls.C1ToolTip" Assembly="C1.Web.Wijmo.Controls.45" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager runat="server" ID="ScriptManager2">
    </asp:ScriptManager>
    <div id="targetContainer">
        <img src="Images/maps.png" alt="c1address" width="628" height="342" usemap="#map" />
        <map id="map" name="map">
            <area shape="circle" alt="c1logo" title="c1logo" id="c1address" href="#" coords="314,124,38" />
        </map>
    </div>
    <wijmo:C1ToolTip runat="server" ID="Tooltip1" TargetSelector="#targetContainer #c1address" CloseBehavior="Sticky" MouseTrailing="True">
        <Template>
            <table cellpadding="0" cellspacing="0" border="0" style="width: 320px;">
                <tr>
                    <td class="maps">
                        <h3>
                            ComponentOne</h3>
                        <p><%= Resources.C1ToolTip.Maps_Text0 %></p>
                        <p><%= Resources.C1ToolTip.Maps_Text1 %></p>
                        <p><%= Resources.C1ToolTip.Maps_Text2 %></p>
                    </td>
                    <td style="width: 132px; height: 161px;">
                        <img src="Images/websitepreview.png" width="132" height="161" alt="website preview" />
                    </td>
                </tr>
            </table>
        </Template>
    </wijmo:C1ToolTip>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1ToolTip.Maps_Text3 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
