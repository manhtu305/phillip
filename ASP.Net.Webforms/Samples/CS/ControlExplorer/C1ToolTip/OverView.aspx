<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="OverView.aspx.cs" Inherits="ControlExplorer.C1ToolTip.OverView" %>

<%@ Register Namespace="C1.Web.Wijmo.Controls.C1ToolTip" Assembly="C1.Web.Wijmo.Controls.45" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        #targetContainer
        {
			height: 445px;
            position: relative;
        }
        #targetContainer img
        {
            margin: 0;
            padding: 0;
            position: absolute;
            border: solid 3px #333;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="targetContainer">
                <img src="http://www.fillmurray.com/257/109" style="left: 0; top: 0;" title="<%= Resources.C1ToolTip.OverView_Title1 %>"
                    alt="" />
                <img src="http://www.fillmurray.com/257/150" style="left: 0; top: 109px;" title="<%= Resources.C1ToolTip.OverView_Title2 %>"
                    alt="" />
                <img src="http://www.fillmurray.com/482/180" style="left: 0; top: 259px;" title="<%= Resources.C1ToolTip.OverView_Title3 %>"
                    alt="" />
                <img src="http://www.fillmurray.com/225/256" style="left: 257px; top: 0;" title="<%= Resources.C1ToolTip.OverView_Title4 %>"
                    alt="" />
                <img src="http://www.fillmurray.com/111/143" style="left: 482px; top: 0px;" title="<%= Resources.C1ToolTip.OverView_Title5 %>"
                    alt="" />
                <img src="http://www.fillmurray.com/111/296" style="left: 482px; top: 143px;" title="<%= Resources.C1ToolTip.OverView_Title6 %>"
                    alt="" />
                <img src="http://www.fillmurray.com/151/200" style="left: 593px; top: 0;" title="N<%= Resources.C1ToolTip.OverView_Title7 %>"
                    alt="" />
                <img src="http://www.fillmurray.com/152/239" style="left: 593px; top: 200px;" title="<%= Resources.C1ToolTip.OverView_Title8 %>"
                    alt="" />
    </div>
    <wijmo:C1ToolTip runat="server" ID="ToolTip1" TargetSelector="#targetContainer [title]">
    </wijmo:C1ToolTip>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1ToolTip.OverView_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
