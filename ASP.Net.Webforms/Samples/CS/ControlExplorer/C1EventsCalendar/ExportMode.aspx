<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="ExportMode.aspx.cs" Inherits="ControlExplorer.C1EventsCalendar.ExportMode" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar"
	TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<wijmo:C1EventsCalendar runat="server" ID="C1EventsCalendar1" Width="100%"  Height="475px"></wijmo:C1EventsCalendar>
<script type="text/javascript">
    $(function () {
        $("#exportFile").click(exportEventsCalendar);
    });

    function exportEventsCalendar() {
        $("#<%=C1EventsCalendar1.ClientID%>").c1eventscalendar("exportEventsCalendar", {
            serviceUrl: $("#serverUrl").val() + "/exportapi/eventscalendar",
            exportFileType: wijmo.exporter.ExportFileType[$("#fileFormats > option:selected").val()],
            method: wijmo.exporter.ExportMethod[$("#exportMode > option:selected").val()],
            fileName: $("#fileName").val()
        });
    }
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1EventsCalendar.ExportMode_Text0 %></p>
    <ul>
        <li><%= Resources.C1EventsCalendar.ExportMode_Li1 %></li>
        <li><%= Resources.C1EventsCalendar.ExportMode_Li2 %></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
<div class="settingcontainer">
    <div class="settingcontent">
	    <ul>
		    <li class="fullwidth"><input type="button" value="<%= Resources.C1EventsCalendar.ExportMode_ExportText %>" id="exportFile"/></li>
            <li>
				<label><%= Resources.C1EventsCalendar.ExportMode_ExportModeLabel %></label>
			    <select id="exportMode">
				    <option selected="selected" value="Content"><%= Resources.C1EventsCalendar.ExportMode_ExportModeContent %></option>
				    <option value="Options"><%= Resources.C1EventsCalendar.ExportMode_ExportModeOptions %></option>
			    </select> 
			</li>
		    <li>
			    <label><%= Resources.C1EventsCalendar.ExportMode_FileFormatLabel %></label>
			    <select id="fileFormats">
				    <option selected="selected" value="Png">Png</option>
				    <option value="Jpg">Jpg</option>
				    <option value="Bmp">Bmp</option>
				    <option value="Gif">Gif</option>
				    <option value="Tiff">Tiff</option>
				    <option value="Pdf">Pdf</option>
			    </select> 
		    </li>
            <li class="longinput">
				<label><%= Resources.C1EventsCalendar.ExportMode_ServerUrlLabel %></label>
				<input type="text" id="serverUrl" value="https://demos.componentone.com/ASPNET/ExportService">
			</li>
            <li>
				<label><%= Resources.C1EventsCalendar.ExportMode_FileNameLabel %></label>
				<input type="text" id="fileName" value="export">
			</li>
	    </ul>
    </div>
</div>
</asp:Content>
