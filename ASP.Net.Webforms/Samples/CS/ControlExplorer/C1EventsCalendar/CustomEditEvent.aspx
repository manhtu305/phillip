<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CustomEditEvent.aspx.cs" Inherits="ControlExplorer.C1EventsCalendar.CustomEditEvent" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        var eventData;
        function saveEvent() {
            var evcal = $("#<%= C1EventCalendar1.ClientID %>");
            eventData.subject = $("#<%=TextBox1.ClientID%>").val();
            eventData.tag = JSON.stringify({
                priority: $("#<%=PriorityList.ClientID%>").val(),
                note: $("#<%=Note.ClientID%>").val()
            });

            if (!eventData.prevData) {
                // add new event
                evcal.c1eventscalendar("addEvent", eventData,
					function (data) { /*console.log("Event added: Data")*/ },
					function (err) { alert("(Add event) Error:" + err) });
            } else {
                evcal.c1eventscalendar("updateEvent", eventData,
				function () { /*alert("Event updated.") */ },
				function (err) { alert("(Update event) Error:" + err) });
            }
            $("#<%=Panel1.ClientID%>").parent().hide();
        }

        function cancel() {
            $("#<%=Panel1.ClientID%>").parent().hide();
		}

		function deleteEvent() {
		    var evcal = $("#<%= C1EventCalendar1.ClientID %>");
		    eventData.subject = $("#<%=TextBox1.ClientID%>").val();

		    if (eventData.prevData) {
		        evcal.c1eventscalendar("deleteEvent", eventData,
				function () { /*alert("Event deleted.") */ },
					function (err) { alert("(Delete event) Error:" + err) });
		    }
		    $("#<%=Panel1.ClientID%>").parent().hide();
        }

        function beforeShowDialog(e, args) {
            // Clear previous value
            $("#<%=PriorityList.ClientID%>").val(1);
            $("#<%=Note.ClientID%>").val("");
            // Processing Data
            eventData = args.data;
            if (eventData) {
                $("#<%=Label1.ClientID%>").text(eventData.prevData ? "<%= Resources.C1EventsCalendar.CustomEditEvent_EditCaption %>" : "<%= Resources.C1EventsCalendar.CustomEditEvent_AddCaption %>");
                $("#<%=TextBox1.ClientID%>").val(eventData.subject);
                if (eventData.tag) {
                    var userData = JSON.parse(eventData.tag);
                    if (userData) {
                        $("#<%=PriorityList.ClientID%>").val(userData.priority);
                        $("#<%=Note.ClientID%>").val(userData.note);
                    }
                }

                if (eventData.prevData) {
                    $("#delButton").show();
                } else {
                    $("#delButton").hide();
                }
            }
		    return true;
		}

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1EventsCalendar runat="server" ID="C1EventCalendar1" OnClientBeforeEditEventDialogShow="beforeShowDialog" Width="100%">
        <EventDialogTemplate>
            <asp:Panel ID="Panel1" runat="server" CssClass="ui-widget-content ui-corner-all" HorizontalAlign="Center" Height="250" Width="300" BorderColor="Black" BorderWidth="1">
                <br />
                <br />
                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                <br />
                <br />
                <asp:Label Width="80" style="text-align: right;"  ID="Label2" runat="server" Text="<%$ Resources:C1EventsCalendar, CustomEditEvent_SubjectLabel %>"></asp:Label><asp:TextBox Width="160" ID="TextBox1" runat="server"></asp:TextBox>
                <br />
                <br />
                <asp:Label Width="80" style="text-align: right;" ID="Label3" runat="server" Text="<%$ Resources:C1EventsCalendar, CustomEditEvent_Priority %>"></asp:Label> <asp:DropDownList Width="160" runat="server" id="PriorityList"> 
                 <asp:listitem text="<%$ Resources:C1EventsCalendar, CustomEditEvent_Priority_1 %>" value="1"></asp:listitem>
                 <asp:listitem text="<%$ Resources:C1EventsCalendar, CustomEditEvent_Priority_2 %>" value="2"></asp:listitem>
                 <asp:listitem text="<%$ Resources:C1EventsCalendar, CustomEditEvent_Priority_3 %>" value="3"></asp:listitem>
                 <asp:listitem text="<%$ Resources:C1EventsCalendar, CustomEditEvent_Priority_4 %>" value="4"></asp:listitem>                 
                </asp:DropDownList>
                <br />
                <br />
                <asp:Label Width="80" style="text-align: right;" ID="Label4" runat="server" Text="<%$ Resources:C1EventsCalendar, CustomEditEvent_Note %>"></asp:Label><asp:TextBox Width="160" ID="Note" runat="server"></asp:TextBox>
                <br />
                <br />
                <input id="saveButton" type="button" onclick="saveEvent();" value="<%= Resources.C1EventsCalendar.CustomEditEvent_SaveText %>" /><input style="margin: 0px 16px;" id="cancelButton" type="button" onclick="    cancel();" value="<%= Resources.C1EventsCalendar.CustomEditEvent_CancelText %>" /><input id="delButton" type="button" onclick="    deleteEvent();" value="<%= Resources.C1EventsCalendar.CustomEditEvent_DeleteText %>" />
            </asp:Panel>
        </EventDialogTemplate>
    </wijmo:C1EventsCalendar>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1EventsCalendar.CustomEditEvent_Text0 %></p>
</asp:Content>
