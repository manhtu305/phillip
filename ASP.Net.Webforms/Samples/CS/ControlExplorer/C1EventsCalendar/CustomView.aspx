<%@ Page Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CustomView.aspx.cs" Inherits="ControlExplorer.C1EventsCalendar.CustomView" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar"
	TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1EventsCalendar runat="server" ID="C1EventsCalendar1" Width="100%"  Height="475px">
		<Views>
			<wijmo:View Type="Day"></wijmo:View>
			<wijmo:View Type="Custom" Name="<%$ Resources:C1EventsCalendar, CustomView_TwoDaysName %>" Unit="Day" Count="2" IsActive ="true"></wijmo:View>
			<wijmo:View Type="Custom" Name="<%$ Resources:C1EventsCalendar, CustomView_TwoWeeksName %>" Unit="Week" Count="2"></wijmo:View>
			<wijmo:View Type="Custom" Name="<%$ Resources:C1EventsCalendar, CustomView_TwoMonthsName %>" Unit="Month" Count="2"></wijmo:View>
			<wijmo:View Type="Custom" Name="<%$ Resources:C1EventsCalendar, CustomView_TwoYearsName %>" Unit="Year" Count="2"></wijmo:View>
		</Views>
	</wijmo:C1EventsCalendar>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
			<p><%= Resources.C1EventsCalendar.CustomView_Text0 %></p>
			<p><%= Resources.C1EventsCalendar.CustomView_Text1 %></p>
			<p><%= Resources.C1EventsCalendar.CustomView_Text2 %></p>
</asp:Content>
