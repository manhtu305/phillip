<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="DataBinding.aspx.cs" Inherits="ControlExplorer.C1EventsCalendar.DataBinding" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar"
	TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1EventsCalendar runat="server" ID="C1EventsCalendar1" Width="100%"  Height="475px">
	<DataStorage>
		<EventStorage DataSourceID="AccessDataSource_Events">
			<Mappings>	
				<IdMapping MappingName="AppointmentId" />			
				<StartMapping MappingName="Start" />
				<EndMapping MappingName="End" />
				<SubjectMapping MappingName="Subject" />
				<LocationMapping MappingName="Location" />
				<DescriptionMapping MappingName="Description" />
				<ColorMapping MappingName="Color" />
				<CalendarMapping MappingName="Calendar" />
				<TagMapping MappingName="Tag" />
			</Mappings>
		</EventStorage>
		<CalendarStorage DataSourceID="AccessDataSource_Calendars">
			<Mappings>
				<IdMapping MappingName="CalendarId" />
				<LocationMapping MappingName="Location" />
				<ColorMapping MappingName="Color" />
				<DescriptionMapping MappingName="Description" />
				<NameMapping MappingName="Name" />
				<PropertiesMapping MappingName="Properties" />
				<TagMapping MappingName="Tag" />
			</Mappings>
		</CalendarStorage>
	</DataStorage>
	</wijmo:C1EventsCalendar>

	<asp:AccessDataSource ID="AccessDataSource_Events" runat="server" 
		DataFile="~/App_Data/EventsCalendarNwind.mdb" 
		DeleteCommand="DELETE FROM [Appointments] WHERE [AppointmentId] = ?" 
		InsertCommand="INSERT INTO [Appointments] ([AppointmentId], [Description], [End], [Location], [Start], [Subject], [Properties], [Color], [Calendar], [Tag]) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" 
		SelectCommand="SELECT * FROM [Appointments]" 
		
		UpdateCommand="UPDATE [Appointments] SET [Description] = ?, [End] = ?, [Location] = ?, [Start] = ?, [Subject] = ?, [Properties] = ?, [Color] = ?, [Calendar] = ?, [Tag] = ? WHERE [AppointmentId] = ?">
		<DeleteParameters>
			<asp:Parameter Name="AppointmentId" Type="Object" />
		</DeleteParameters>
		<InsertParameters>
			<asp:Parameter Name="AppointmentId" Type="Object" />
			<asp:Parameter Name="Description" Type="String" />
			<asp:Parameter Name="End" Type="DateTime" />
			<asp:Parameter Name="Location" Type="String" />
			<asp:Parameter Name="Start" Type="DateTime" />
			<asp:Parameter Name="Subject" Type="String" />
			<asp:Parameter Name="Properties" Type="String" />
			<asp:Parameter Name="Color" Type="String" />
			<asp:Parameter Name="Calendar" Type="String" />
			<asp:Parameter Name="Tag" Type="String" />
		</InsertParameters>
		<UpdateParameters>
			<asp:Parameter Name="Description" Type="String"/>
			<asp:Parameter Name="End" Type="DateTime" />
			<asp:Parameter Name="Location" Type="String" />
			<asp:Parameter Name="Start" Type="DateTime" />
			<asp:Parameter Name="Subject" Type="String" />
			<asp:Parameter Name="Properties" Type="String" />
			<asp:Parameter Name="Color" Type="String" />
			<asp:Parameter Name="Calendar" Type="String" />
			<asp:Parameter Name="Tag" Type="String" />
			<asp:Parameter Name="AppointmentId" Type="Object" />
		</UpdateParameters>
	</asp:AccessDataSource>
	<asp:AccessDataSource ID="AccessDataSource_Calendars" runat="server" 
		DataFile="~/App_Data/EventsCalendarNwind.mdb" 
		DeleteCommand="DELETE FROM [Calendars] WHERE [CalendarId] = ?" 
		InsertCommand="INSERT INTO [Calendars] ([CalendarId], [Name], [Description], [Color], [Tag], [Location], [Properties]) VALUES (?, ?, ?, ?, ?, ?, ?)" 
		SelectCommand="SELECT * FROM [Calendars]" 
		
		UpdateCommand="UPDATE [Calendars] SET [Name] = ?, [Description] = ?, [Color] = ?, [Tag] = ?, [Location] = ?, [Properties] = ? WHERE [CalendarId] = ?">
		<DeleteParameters>
			<asp:Parameter Name="CalendarId" Type="String" />
		</DeleteParameters>
		<InsertParameters>
			<asp:Parameter Name="CalendarId" Type="String" />
			<asp:Parameter Name="Name" Type="String" />
			<asp:Parameter Name="Description" Type="String" />
			<asp:Parameter Name="Color" Type="String" />
			<asp:Parameter Name="Tag" Type="String" />
			<asp:Parameter Name="Location" Type="String" />
			<asp:Parameter Name="Properties" Type="String" />
		</InsertParameters>
		<UpdateParameters>
			<asp:Parameter Name="Name" Type="String" />
			<asp:Parameter Name="Description" Type="String" />
			<asp:Parameter Name="Color" Type="String" />
			<asp:Parameter Name="Tag" Type="String" />
			<asp:Parameter Name="Location" Type="String" />
			<asp:Parameter Name="Properties" Type="String" />
			<asp:Parameter Name="CalendarId" Type="String" />
		</UpdateParameters>
	</asp:AccessDataSource>
	<br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1EventsCalendar.DataBinding_Text0 %></p>
	<p><%= Resources.C1EventsCalendar.DataBinding_Text1 %></p>
	<p><%= Resources.C1EventsCalendar.DataBinding_Text2 %></p>
	<ul>
		<li>
			<%= Resources.C1EventsCalendar.DataBinding_Li1 %>
		</li>
		<li>
			<%= Resources.C1EventsCalendar.DataBinding_Li2 %>
		</li>
		<li>			
			<%= Resources.C1EventsCalendar.DataBinding_Li3 %>
		</li>
		<li>
			<%= Resources.C1EventsCalendar.DataBinding_Li4 %>
		</li>
	</ul>

	<p><%= Resources.C1EventsCalendar.DataBinding_Text3 %></p>
	<ul>
		<li>
			<%= Resources.C1EventsCalendar.DataBinding_Li5 %>
		</li>
		<li>
			<%= Resources.C1EventsCalendar.DataBinding_Li6 %>
		</li>
		<li>			
			<%= Resources.C1EventsCalendar.DataBinding_Li7 %>
		</li>
		<li>
			<%= Resources.C1EventsCalendar.DataBinding_Li8 %>
		</li>
	</ul>
</asp:Content>
