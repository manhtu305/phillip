<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="CustomSendingRequest.aspx.cs" Inherits="ControlExplorer.C1EventsCalendar.CustomSendingRequest" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar"
	TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<wijmo:C1EventsCalendar runat="server" ID="C1EventsCalendar1" Width="100%"  Height="475px"></wijmo:C1EventsCalendar>
<script type="text/javascript">
    $(function () {
        $("#exportFile").click(exportEventsCalendar);
    });

    function customSendingRequest(content, setting) {
        var formInnerHtml = '<input type="hidden" name="type" value="application/json"/>';
        formInnerHtml += '<input type="hidden" name="data" value="' + _htmlSpecialCharsEntityEncode(content) + '" />';
        var $iframe = $("<iframe style='display: none' src='about:blank'></iframe>").appendTo("body");
        $iframe.ready(function () {
            var formDoc = _getiframeDocument($iframe);
            formDoc.write("<html><head></head><body><form method='Post' action='" + setting.serviceUrl + "'>" + formInnerHtml + "</form>dummy windows for postback</body></html>");
            var $form = $(formDoc).find('form');
            $form.submit();
        });
    }
    function _getiframeDocument($iframe) {
        var iframeDoc = $iframe[0].contentWindow || $iframe[0].contentDocument;
        if (iframeDoc.document) {
            iframeDoc = iframeDoc.document;
        }
        return iframeDoc;
    }
    var _htmlSpecialCharsRegEx = /[<>&\r\n"']/gm;
    var _htmlSpecialCharsPlaceHolders = {
        '<': 'lt;',
        '>': 'gt;',
        '&': 'amp;',
        '\r': "#13;",
        '\n': "#10;",
        '"': 'quot;',
        "'": 'apos;'
    };
    function _htmlSpecialCharsEntityEncode(str) {
        return str.replace(_htmlSpecialCharsRegEx, function (match) {
            return '&' + _htmlSpecialCharsPlaceHolders[match];
        });
    }

    function exportEventsCalendar() {
        $("#<%=C1EventsCalendar1.ClientID%>").c1eventscalendar("exportEventsCalendar", {
            sender: customSendingRequest,
            serviceUrl: $("#serverUrl").val() + "/exportapi/eventscalendar",
            exportFileType: wijmo.exporter.ExportFileType[$("#fileFormats > option:selected").val()],
            fileName: $("#fileName").val()
        });
    }
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1EventsCalendar.CustomSendingRequest_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
<div class="settingcontainer">
    <div class="settingcontent">
	    <ul>
		    <li class="fullwidth"><input type="button" value="<%= Resources.C1EventsCalendar.CustomSendingRequest_ExportText %>" id="exportFile"/></li>
            <li class="longinput">
				<label><%= Resources.C1EventsCalendar.CustomSendingRequest_ServerUrlLabel %></label>
				<input type="text" id="serverUrl" value="https://demos.componentone.com/ASPNET/ExportService">
			</li>
            <li>
				<label><%= Resources.C1EventsCalendar.CustomSendingRequest_FileNameLabel %></label>
				<input type="text" id="fileName" value="export">
			</li>
		    <li>
			    <label><%= Resources.C1EventsCalendar.CustomSendingRequest_FileFormatLabel %></label>
			    <select id="fileFormats">
				    <option selected="selected" value="Png">Png</option>
				    <option value="Jpg">Jpg</option>
				    <option value="Bmp">Bmp</option>
				    <option value="Gif">Gif</option>
				    <option value="Tiff">Tiff</option>
				    <option value="Pdf">Pdf</option>
			    </select> 
		    </li>
	    </ul>
    </div>
</div>
</asp:Content>