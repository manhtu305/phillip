<%@ Page Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CustomEventTemplate.aspx.cs" Inherits="ControlExplorer.C1EventsCalendar.CustomEventTemplate" %>

<%@ register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1ToolTip" tagprefix="wijmo" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar"
	TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<script type="text/javascript">
		function eventCreating(e, args) {
			var eventData = args.data, template = $(args.eventTemplate);
			template.find("#<%=SubjectLabel.ClientID%>").text(eventData.subject);
			template.find("#<%=TimeLabel.ClientID%>").text(eventData.start.getHours() + ":" + eventData.start.getMinutes() + " - " + eventData.end.getHours() + ":" + eventData.end.getMinutes());
			template.find("#<%=LocationLabel.ClientID%>").text(eventData.location).attr("title", eventData.description);
			args.eventTemplate = template[0].outerHTML;
		}

		$(document).ready(function () {
			$("#<%=C1EventsCalendar1.ClientID%>").c1eventscalendar({
				dataStorage: {
					addEvent: function (obj, successCallback, errorCallback) { successCallback(); },
					updateEvent: function (obj, successCallback, errorCallback) { successCallback(); },
					deleteEvent: function (obj, successCallback, errorCallback) {
						successCallback();
					},
					loadEvents: function (visibleCalendars,
											successCallback, errorCallback) {
						successCallback([
							{
								id: "id1",
								description: "Finish work in time",
								location: "<%= Resources.C1EventsCalendar.CustomEventTemplate_Location1 %>",
								subject: "<%= Resources.C1EventsCalendar.CustomEventTemplate_Subject1 %>",
								calendar: "John",
								start: new Date(2015, 0, 1, 12, 30),
								end: new Date(2015, 0, 1, 16, 30),
								color: "black"
							},
							{
								id: "id2",
								description: "Have supper with workmates",
								location: "<%= Resources.C1EventsCalendar.CustomEventTemplate_Location2 %>",
								subject: "<%= Resources.C1EventsCalendar.CustomEventTemplate_Subject2 %>",
								calendar: "John",
								start: new Date(2015, 0, 1, 17, 30),
								end: new Date(2015, 0, 1, 21, 30),
								color: "white"
							}
						]);
					}
				}
			});
		});
	</script>
	<wijmo:C1EventsCalendar runat="server" ID="C1EventsCalendar1" Width="100%"  Height="475px" SelectedDate="2015-01-01" OnClientEventCreating="eventCreating">
		<EventTemplate>
			<asp:Panel ID="Panel1" runat="server" BackColor="Silver" HorizontalAlign="Center">
				<br/>
				<asp:Label ID="SubjectLabel" runat="server" Text="Label" Height ="40" Font-Size="Large" Font-Bold="true"></asp:Label>
				<br/>
				<asp:Label ID="TimeLabel" runat="server" Text="Label" Height ="40" Font-Size="Medium"></asp:Label>
				<br/>
				<asp:Label ID="LocationLabel" runat="server" Text="Label" Height ="40" Font-Size="Medium"></asp:Label>
			</asp:Panel>
		</EventTemplate>
	</wijmo:C1EventsCalendar>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
			<p><%= Resources.C1EventsCalendar.CustomEventTemplate_Text0 %></p>
</asp:Content>