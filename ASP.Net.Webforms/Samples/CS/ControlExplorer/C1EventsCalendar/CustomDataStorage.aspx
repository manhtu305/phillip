<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CustomDataStorage.aspx.cs" Inherits="ControlExplorer.C1EventsCalendar.CustomDataStorage" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar"
	TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">

	<script src="../explore/js/amplify.core.min.js" type="text/javascript"></script>
	<script src="../explore/js/amplify.store.min.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1EventsCalendar runat="server" ID="C1EventsCalendar1" Width="100%"  Height="475px"></wijmo:C1EventsCalendar>

	<script type="text/javascript">
		$(document).ready(function () {
			var c1eventscalendar = $("#<%=C1EventsCalendar1.ClientID%>");

			c1eventscalendar.c1eventscalendar({
				dataStorage:
				{
					addEvent: _updateEvent,
					updateEvent: _updateEvent,
					deleteEvent: function (obj, successCallback, errorCallback) {
						window.setTimeout(function () {
							var curCalStore = amplify.store("calendarstore_" + obj.calendar);
							if (!curCalStore) {
								curCalStore = {};
							}
							if (curCalStore[obj.id]) {
								delete curCalStore[obj.id];
							}
							amplify.store("calendarstore_" + obj.calendar, curCalStore);
							successCallback()
						}, 400);
					},
					loadEvents: function (visibleCalendars,
												successCallback, errorCallback) {
						var i, j, events = [];
						window.setTimeout(function () {
							for (i = 0; i < visibleCalendars.length; i++) {

								var curCalStore = amplify.store("calendarstore_" + visibleCalendars[i]);
								if (!curCalStore) {
									curCalStore = {};
									amplify.store("calendarstore_" + visibleCalendars[i], curCalStore);
								}
								for (j in curCalStore) {
									events.push(curCalStore[j]);
								}
							}
							successCallback(events);
						}, 500);
					},
					addCalendar: _updateCalendar,
					updateCalendar: _updateCalendar,
					deleteCalendar: function (obj, successCallback, errorCallback) {
						successCallback();
					}
				}
			});

		});

		function _updateEvent(obj, successCallback, errorCallback) {
			window.setTimeout(function () {
				var curCalStore = amplify.store("calendarstore_" + obj.calendar);
				if (!curCalStore) {
					curCalStore = {};
				}
				curCalStore[obj.id] = obj;
				amplify.store("calendarstore_" + obj.calendar, curCalStore);
				successCallback()
			}, 400);
		}

		function _updateCalendar(obj, successCallback, errorCallback) {
			successCallback();
		}
	</script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
			<p><%= Resources.C1EventsCalendar.CustomDataStorage_Text0 %></p>
			<p><%= Resources.C1EventsCalendar.CustomDataStorage_Text1 %></p>
			<p><%= Resources.C1EventsCalendar.CustomDataStorage_Text2 %></p>
			<ul>
			<li><%= Resources.C1EventsCalendar.CustomDataStorage_Li1 %></li>
			<li><%= Resources.C1EventsCalendar.CustomDataStorage_Li2 %></li>			
			</ul>
</asp:Content>
