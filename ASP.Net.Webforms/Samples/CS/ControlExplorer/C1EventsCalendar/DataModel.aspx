<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="DataModel.aspx.cs" Inherits="ControlExplorer.C1EventsCalendar.DataModel" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar"
	TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<script src="../explore/js/amplify.core.min.js"></script>
	<script src="../explore/js/amplify.store.min.js"></script>
    <style type="text/css">
        .top-pane
        {
            margin: 1em 0;
            padding: 1em;
        }
        .calendaractions p, 
        .addnewevent, .events-title
        {
            margin-bottom: 1em;
        }
        #eventscalendar
        {
            width: 750px;
        }                
        
		.calendarslist .ui-selecting { background: #FECA40; }
		.calendarslist .ui-selected { background: #F39814; color: white; }
		.calendarslist { list-style-type: none; margin: 0; padding: 0; width: 60%; }
		.calendarslist li { margin-left: 0px; margin: 3px; padding: 0.2em; font-size: 1em; height: 16px; }        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<wijmo:C1EventsCalendar runat="server" ID="C1EventsCalendar1" Width="100%"  Height="475px" VisibleCalendars="John's Work,John's Home"
 OnClientInitialized="initialized" HeaderBarVisible="true" OnClientCalendarsChanged="calendarsChanged">
 <DataStorage DataFile=""> 
 </DataStorage>
 </wijmo:C1EventsCalendar>
	<script type="text/javascript">
		function initialized(e) {
			var evcal = getEvCal();
			evcal.c1eventscalendar("addCalendar", { name: "John's Work" });
			evcal.c1eventscalendar("addCalendar", { name: "John's Home" });
			// load available calendars when widget is initialized
			loadCalendarsList();
		}
		function calendarsChanged(e, args) {
			// load available calendars when calendars option has been changed
			loadCalendarsList();
			updateVisibleCalendars();
		}
		function getEvCal() {
			return $("#<%= C1EventsCalendar1.ClientID%>");
    	}
    	function onAddNewEventClick() {
    		getEvCal().c1eventscalendar("showEditEventDialog");
    	}
    	function onAddNewEventWithoutDialogClick() {
    		var o = {};
    		o.subject = "New event";
    		o.start = new Date();
    		o.end = new Date(o.start.getTime() + 1000 * 60 * 60 * 2); // duration 2 hours
    		getEvCal().c1eventscalendar("addEvent", o);
    	}
    	function onDeleteEventsForActiveDayClick() {
    		var now = new Date(),
					evcal = getEvCal(),
					selectedDate = evcal.c1eventscalendar("option", "selectedDate"),
    				start = new Date(selectedDate.getFullYear(), selectedDate.getMonth(), selectedDate.getDate()),
					end = new Date(start.getTime() + 1000 * 60 * 60 * 24),
					events, i;
    		events = evcal.c1eventscalendar("getOccurrences", start, end);

    		for (i = 0; i < events.length; i++) {
    			evcal.c1eventscalendar("deleteEvent", events[i]);
    		}
    	}
    	function onAddNewCalendarClick() {
    		getEvCal().c1eventscalendar("showEditCalendarDialog");
    	}
    	function onEditCalendarClick() {
    		if ($(".calendarslist").find(".ui-selected").length > 0) {
    			var checkbox = $(".calendarslist").find(".ui-selected").find("input")[0];
    			getEvCal().c1eventscalendar("showEditCalendarDialog", checkbox.value);
    		}
    	}
    	function onDeleteCalendarClick() {
    		$(".calendarslist").find(".ui-selected").each(function () {
    			var checkbox = $(this).find("input")[0];
    			getEvCal().c1eventscalendar("deleteCalendar", checkbox.value);
    		});
    	}
    	function loadCalendarsList() {
    		var evcal = getEvCal(),
				calendars = evcal.c1eventscalendar("option", "calendars"),
				visibleCalendars = evcal.c1eventscalendar("option", "visibleCalendars"),
				i, c, calendarslist = $(".calendarslist");
    		calendarslist.html("");
    		for (i = 0, c = calendars.length; i < c; i += 1) {
    			var calName = calendars[i].name;
    			var checkAttrText = $.inArray(calName, visibleCalendars) != -1 ? " checked=\"checked\"" : "";
    			calendarslist.append("<li class=\"ui-widget-content\"><label><input type=\"checkbox\"" + checkAttrText + " value=\"" + calName + "\" />" + calName + "</label></li>");
    		}
    		$(".calendaractions .calendarsettings").button("option", "disabled", true);
    		$(".calendaractions .deletecalendar").button("option", "disabled", true);
    	}

    	function updateVisibleCalendars() {
    		var checkboxes = $(".calendarslist").find("input"), i, visibleCalendars = [];
    		for (i = 0; i < checkboxes.length; i++) {
    			if (checkboxes[i].checked) {
    				visibleCalendars.push(checkboxes[i].value);
    			}
    		}
    		getEvCal().c1eventscalendar("option", "visibleCalendars", visibleCalendars);
    	}

    	$(document).ready(function () {
    		$(".eventactions .addnewevent")/*.button()*/
					.click($.proxy(onAddNewEventClick, this));
    		$(".eventactions .addneweventwithoutdialog")/*.button()*/
					.click($.proxy(onAddNewEventWithoutDialogClick, this));
    		$(".eventactions .deleteEventsForActiveDay")/*.button()*/
					.click($.proxy(onDeleteEventsForActiveDayClick, this));
    		//

    		$(".calendaractions .addnewcalendar").button()
							.click($.proxy(onAddNewCalendarClick, this));
    		$(".calendaractions .calendarsettings").button({ disabled: true })
							.click($.proxy(onEditCalendarClick, this));
    		$(".calendaractions .deletecalendar").button({ disabled: true })
							.click($.proxy(onDeleteCalendarClick, this));

    		$(".calendarslist").selectable({
    			selected: function (event, ui) {
    				if ($(".calendarslist").find(".ui-selected").length > 0) {
    					$(".calendaractions .deletecalendar").button("option", "disabled", false);
    					if ($(".calendarslist").find(".ui-selected").length === 1) {
    						$(".calendaractions .calendarsettings").button("option", "disabled", false);
    					} else {
    						$(".calendaractions .calendarsettings").button("option", "disabled", true);
    					}
    				}
    			},
    			unselected: function (event, ui) {
    				if ($(".calendarslist").find(".ui-selected").length === 1) {
    					$(".calendaractions .calendarsettings").button("option", "disabled", false);
    				} else if ($(".calendarslist").find(".ui-selected").length < 1) {
    					$(".calendaractions .calendarsettings").button("option", "disabled", true);
    					$(".calendaractions .deletecalendar").button("option", "disabled", true);
    				}
    			}
    		});

    		$(".calendarslist").on("change", "input", function () {
    			updateVisibleCalendars();
    		});
    	});
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">

                <div class="top-pane ui-helper-clearfix ui-widget-content ui-corner-all">
                    <div class="calendaractions">
                        <h3 class="events-title">
                            <%= Resources.C1EventsCalendar.DataModel_EventsLabel %></h3>
                        <div class="eventactions">
                            <a class="addnewevent" href="#"><%= Resources.C1EventsCalendar.DataModel_EventsAdd1 %></a>&nbsp;&nbsp;
							<a class="addneweventwithoutdialog" href="#"><%= Resources.C1EventsCalendar.DataModel_EventsAdd2 %></a>&nbsp;&nbsp;
							<a class="deleteEventsForActiveDay" href="#"><%= Resources.C1EventsCalendar.DataModel_EventsDelete %></a>
                        </div>
                        <h3>
                            <%= Resources.C1EventsCalendar.DataModel_CalendarsLabel %></h3>
                        <div class="calendarslist-container">
                            <ol class="calendarslist">
                            </ol>
                        </div>
                        <p><a class="calendarsettings"><%= Resources.C1EventsCalendar.DataModel_EditText %></a> <a class="deletecalendar"><%= Resources.C1EventsCalendar.DataModel_DeleteText %></a></p>
                        <p><a class="addnewcalendar"><%= Resources.C1EventsCalendar.DataModel_AddCalendarText %></a></p>
                    </div>
                </div>
    
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
                <p><%= Resources.C1EventsCalendar.DataModel_Text2 %></p>
			<p><%= Resources.C1EventsCalendar.DataModel_Text3 %></p>
			<ul>
				<li><%= Resources.C1EventsCalendar.DataModel_Li1 %></li>
				<li><%= Resources.C1EventsCalendar.DataModel_Li2 %></li>
				<li><%= Resources.C1EventsCalendar.DataModel_Li3 %></li>			
			</ul>
			<p><%= Resources.C1EventsCalendar.DataModel_Text4 %></p>
			<ul>
				<li><%= Resources.C1EventsCalendar.DataModel_Li4 %></li>
				<li><%= Resources.C1EventsCalendar.DataModel_Li5 %></li>
				<li><%= Resources.C1EventsCalendar.DataModel_Li6 %></li>			
				<li><%= Resources.C1EventsCalendar.DataModel_Li7 %></li>			
				<li><%= Resources.C1EventsCalendar.DataModel_Li8 %></li>
				<li><%= Resources.C1EventsCalendar.DataModel_Li9 %></li>								
			</ul>
</asp:Content>
