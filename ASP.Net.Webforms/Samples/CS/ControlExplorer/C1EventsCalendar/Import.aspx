<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Import.aspx.cs" Inherits="ControlExplorer.C1EventsCalendar.Import1" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar"
	TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
		<wijmo:C1EventsCalendar runat="server" ID="C1EventsCalendar1" 
			ViewType="Month" SelectedDate="2011-03-01"
			Width="100%"  Height="475px"></wijmo:C1EventsCalendar>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
                <p><%= Resources.C1EventsCalendar.Import_Text0 %></p>
			<p><%= Resources.C1EventsCalendar.Import_Text1 %></p>
			<ul>
				<li><%= Resources.C1EventsCalendar.Import_Li1 %></li>
				<li><%= Resources.C1EventsCalendar.Import_Li2 %></li>
			</ul>
			<p><%= Resources.C1EventsCalendar.Import_Text2 %></p>
			<pre class="controldescription-code">
  C1EventsCalendar1.DataStorage.Import(
			Server.MapPath("pens_schedule_1011_full.ics"), 
			FileFormatEnum.iCal);
  C1EventsCalendar1.DataStorage.SaveData();
			</pre>
</asp:Content>
