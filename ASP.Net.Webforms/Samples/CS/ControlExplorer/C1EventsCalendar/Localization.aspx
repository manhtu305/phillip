﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Localization.aspx.cs" Inherits="ControlExplorer.C1EventsCalendar.Localization" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1EventsCalendar runat="server" ID="C1EventCalendar1" Culture="de-De" Width="100%">
		<Localization agendaTimeFormat="{0:hh:mm tt} - {1:hh:mm tt}" buttonToday="heute"
			 buttonWeekView="Woche" buttonDayView="Tag" buttonMonthView="Monat"
			 buttonListView="Liste" buttonDelete="löschen" buttonOK="OK" buttonCancel="stornieren"
			 labelAllDay="ganztägig" labelToday="Heute" labelName="name" labelStarts="beginnt"
			 labelEnds="endet" labelLocation="lage" labelRepeat="repetieren" labelCalendar="kalender"
			 labelDescription="sorte" textNewEvent="neues Ereignis" repeatNone="keiner"
			 repeatDaily="täglich" repeatWorkDays="Tag der Arbeit" repeatWeekly="Jede Woche"
			 repeatMonthly="Jeden Monat" repeatYearly="jährlich" />
		<DayViewHeaderFormat Day="ganztägige Ereignisse" Week="{0:d dddd}" List="{0:d dddd}" />
	</wijmo:C1EventsCalendar>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1EventsCalendar.Localization_Text0 %></p>
			<p><%= Resources.C1EventsCalendar.Localization_Text1 %></p>
			<ul>
				<li><%= Resources.C1EventsCalendar.Localization_Li1 %></li>
				<li><%= Resources.C1EventsCalendar.Localization_Li2 %></li>
				<li><%= Resources.C1EventsCalendar.Localization_Li3 %></li>
			</ul>
</asp:Content>
