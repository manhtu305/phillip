<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CustomTimeInterval.aspx.cs" Inherits="ControlExplorer.C1EventsCalendar.CustomTimeInterval" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar"
	TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<wijmo:C1EventsCalendar runat="server" ID="C1EventsCalendar1" Width="100%"  Height="475px" TimeInterval="60" TimeIntervalHeight="25" TimeRulerInterval="120"></wijmo:C1EventsCalendar>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
			<p><%= Resources.C1EventsCalendar.CustomTimeInterval_Text0 %></p>
			<p><%= Resources.C1EventsCalendar.CustomTimeInterval_Text1 %></p>
			<ul>
			<li><%= Resources.C1EventsCalendar.CustomTimeInterval_Li1 %></li>
			<li><%= Resources.C1EventsCalendar.CustomTimeInterval_Li2 %></li>
			<li><%= Resources.C1EventsCalendar.CustomTimeInterval_Li3 %></li>			
			</ul>
</asp:Content>
