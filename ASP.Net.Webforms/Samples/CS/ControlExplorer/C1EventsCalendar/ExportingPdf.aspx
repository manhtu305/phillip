<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExportingPdf.aspx.cs" MasterPageFile="~/Wijmo.Master" Inherits="ControlExplorer.C1EventsCalendar.ExportingPdf" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar"
	TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<wijmo:C1EventsCalendar runat="server" ID="C1EventsCalendar1" Width="100%"  Height="475px"></wijmo:C1EventsCalendar>
<script type="text/javascript">
    $(function () {
        $("#exportPdf").click(exportPdf);
    });

    function exportPdf() {
        var fileName = $("#fileName").val(), url = $("#serverUrl").val() + "/exportapi/eventscalendar", pdfSetting = {
            imageQuality: wijmo.exporter.ImageQuality[$("#imageQuality option:selected").val()],
            compression: wijmo.exporter.CompressionType[$("#compression option:selected").val()],
            fontType: wijmo.exporter.FontType[$("#fontType option:selected").val()],
            author: $("#pdfAuthor").val(),
            creator: $("#pdfCreator").val(),
            subject: $("#pdfSubject").val(),
            title: $("#pdfTitle").val(),
            producer: $("#pdfProducer").val(),
            keywords: $("#pdfKeywords").val(),
            encryption: wijmo.exporter.PdfEncryptionType[$("#encryption option:selected").val()],
            ownerPassword: $("#ownerPassword").val(),
            userPassword: $("#userPassword").val(),
            allowCopyContent: $("#allowCopyContent").prop('checked'),
            allowEditAnnotations: $("#allowEditAnnotations").prop('checked'),
            allowEditContent: $("#allowEditContent").prop('checked'),
            allowPrint: $("#allowPrint").prop('checked')
        }
        $("#<%=C1EventsCalendar1.ClientID%>").c1eventscalendar("exportEventsCalendar", {
            fileName:fileName,
            exportFileType: wijmo.exporter.ExportFileType["Pdf"],
            pdf: pdfSetting,
            serviceUrl:url
        });
    }
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1EventsCalendar.ExportingPdf_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
<div class="settingcontainer">
    <div class="settingcontent">
	    <ul>
		    <li class="fullwidth"><input type="button" value="<%= Resources.C1EventsCalendar.ExportingPdf_ExportText %>" id="exportPdf"/></li>
			<li class="fullwidth">
				<label class="settinglegend"><%= Resources.C1EventsCalendar.ExportingPdf_FileContentLabel %></label>
			</li>
			<li>
				<label><%= Resources.C1EventsCalendar.ExportingPdf_ImageQualityLabel %></label>
				<select id="imageQuality">
					<option selected="selected" value="Default"><%= Resources.C1EventsCalendar.ExportingPdf_ImageQualityDefault %></option>
					<option value="Low"><%= Resources.C1EventsCalendar.ExportingPdf_ImageQualityLow %></option>
					<option value="Medium"><%= Resources.C1EventsCalendar.ExportingPdf_ImageQualityMedium %></option>
					<option value="High"><%= Resources.C1EventsCalendar.ExportingPdf_ImageQualityHight %></option>
				</select> 
			</li>
			<li>
				<label><%= Resources.C1EventsCalendar.ExportingPdf_CompressionLabel %></label>
				<select id="compression">
					<option selected="selected" value="Default"><%= Resources.C1EventsCalendar.ExportingPdf_CompressionDefault %></option>
					<option value="None"><%= Resources.C1EventsCalendar.ExportingPdf_CompressionNone %></option>
					<option value="BestSpeed"><%= Resources.C1EventsCalendar.ExportingPdf_CompressionBestSpeed %></option>
					<option value="BestCompression"><%= Resources.C1EventsCalendar.ExportingPdf_CompressionBestCompression %></option>
				</select> 
			</li>
			<li>
				<label><%= Resources.C1EventsCalendar.ExportingPdf_FontTypeLabel %></label>
				<select id="fontType">
					<option value="Standard"><%= Resources.C1EventsCalendar.ExportingPdf_FontTypeStandard %></option>
					<option value="TrueType" selected="selected"><%= Resources.C1EventsCalendar.ExportingPdf_FontTypeTrueType %></option>
					<option value="Embedded"><%= Resources.C1EventsCalendar.ExportingPdf_FontTypeEmbedded %></option>
				</select> 
			</li>
			<li class="fullwidth">
				<label class="settinglegend"><%= Resources.C1EventsCalendar.ExportingPdf_DcoumentInfoLabel %></label>
			</li>
			<li>
				<label><%= Resources.C1EventsCalendar.ExportingPdf_AuthorLabel %></label><input type="text" value="ComponentOne" id="pdfAuthor"/>
			</li>
			<li>
				<label><%= Resources.C1EventsCalendar.ExportingPdf_CreatorLabel %></label><input type="text" value="ComponentOne" id="pdfCreator"/>
			</li>
			<li>
				<label><%= Resources.C1EventsCalendar.ExportingPdf_SubjectLabel %></label><input type="text" id="pdfSubject"/>
			</li>
			<li>
				<label><%= Resources.C1EventsCalendar.ExportingPdf_TitleLabel %></label><input type="text" value="Export" id="pdfTitle"/>
			</li>
			<li>
				<label><%= Resources.C1EventsCalendar.ExportingPdf_ProducerLabel %></label><input type="text" value="ComponentOne" id="pdfProducer"/>
			</li>
			<li>
				<label><%= Resources.C1EventsCalendar.ExportingPdf_KeywordsLabel %></label><input type="text" id="pdfKeywords"/>
			</li>
			<li class="fullwidth">
				<label class="settinglegend"><%= Resources.C1EventsCalendar.ExportingPdf_DcoumentSecurityLabel %></label>
			</li>
			<li class="fullwidth">
				<label><%= Resources.C1EventsCalendar.ExportingPdf_EncryptionTypeLabel %></label>
				<select id="encryption">
					<option selected="selected" value="NotPermit"><%= Resources.C1EventsCalendar.ExportingPdf_EncryptionTypeNotPermit %></option>
					<option value="Standard40"><%= Resources.C1EventsCalendar.ExportingPdf_EncryptionTypeStandard40 %></option>
					<option value="Standard128"><%= Resources.C1EventsCalendar.ExportingPdf_EncryptionTypeStandard128 %></option>
					<option value="Aes128"><%= Resources.C1EventsCalendar.ExportingPdf_EncryptionTypeAes128 %></option>
				</select> 
			</li>
			<li>
				<label><%= Resources.C1EventsCalendar.ExportingPdf_OwnerPasswordLabel %></label><input type="password" id="ownerPassword"/>
			</li>
			<li>
				<label><%= Resources.C1EventsCalendar.ExportingPdf_UserPasswordLabel %></label><input type="password" id="userPassword"/>
			</li>
			<li><input type="checkbox" checked="checked" id="allowCopyContent"/><label class="widelabel"><%= Resources.C1EventsCalendar.ExportingPdf_AllowCopyContentLabel %></label></li>
			<li><input type="checkbox" checked="checked" id="allowEditAnnotations"/><label class="widelabel"><%= Resources.C1EventsCalendar.ExportingPdf_AllowEditAnnotationsLabel %></label></li>
			<li><input type="checkbox" checked="checked" id="allowEditContent"/><label class="widelabel"><%= Resources.C1EventsCalendar.ExportingPdf_AllowEditContentLabel %></label></li>
			<li><input type="checkbox" checked="checked" id="allowPrint"/><label class="widelabel"><%= Resources.C1EventsCalendar.ExportingPdf_AllowPrintLabel %></label></li>
			<li class="fullwidth">
				<label class="settinglegend"><%= Resources.C1EventsCalendar.ExportingPdf_ConfigurationLabel %></label>
			</li>
            <li class="longinput">
				<label><%= Resources.C1EventsCalendar.ExportingPdf_ServerUrlLabel %></label>
				<input type="text" id="serverUrl" value="https://demos.componentone.com/ASPNET/ExportService">
			</li>
            <li>
				<label><%= Resources.C1EventsCalendar.ExportingPdf_FileNameLabel %></label>
				<input type="text" id="fileName" value="export">
			</li>
	    </ul>
    </div>
</div>
</asp:Content>