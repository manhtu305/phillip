<%@ Page Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.cs" Inherits="ControlExplorer.C1PDF.Overview" %>

<asp:Content ID="content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <asp:LinkButton ID="btnexport" runat="server" OnClick="btnexport_Click" Text="Create PDF" Font-Underline="true" />
</asp:Content>

<asp:Content ID="content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1PDF.Overview_Text0 %></p>
</asp:Content>
