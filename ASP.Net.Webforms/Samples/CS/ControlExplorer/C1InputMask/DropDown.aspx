<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputMask_DropDown" CodeBehind="DropDown.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <p><wijmo:C1InputMask ID="C1InputMask1" runat="server" MaskFormat="000-0000" ShowTrigger="true">
            <ComboItems>
                <wijmo:C1ComboBoxItem Text="100-1000" Value="100-1000" />
                <wijmo:C1ComboBoxItem Text="200-2000" Value="200-2000" />
                <wijmo:C1ComboBoxItem Text="123-2909" Value="123-2909" />
            </ComboItems>
        </wijmo:C1InputMask></p>
    <br />
    <p><%= Resources.C1InputMask.DropDown_Text1 %></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p><%= Resources.C1InputMask.DropDown_Text2 %></p>

    <p><%= Resources.C1InputMask.DropDown_Text3 %></p>

    <p><%= Resources.C1InputMask.DropDown_Text4 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>

