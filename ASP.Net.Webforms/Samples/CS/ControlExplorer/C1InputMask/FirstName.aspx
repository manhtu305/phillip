<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputMask_FirstName" CodeBehind="FirstName.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <p><wijmo:C1InputMask ID="C1InputMask1" runat="server" MaskFormat=">L|LLLLLLLLL" HidePromptOnLeave="true">
        </wijmo:C1InputMask></p>
    <br />
    <p><%= Resources.C1InputMask.FirstName_Text1 %></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">

    <p><%= Resources.C1InputMask.FirstName_Text2 %></p>

    <p><%= Resources.C1InputMask.FirstName_Text3 %></p>

    <p><%= Resources.C1InputMask.FirstName_Text4 %></p>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>

