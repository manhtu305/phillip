ASP.NET WebForms Control Explorer Sample
-------------------------------------------------------------------
The Control Explorer sample demonstrates features for all controls in Studio for ASP.NET Web Forms.

Based on the new Web Forms Framework, the ComponentOne ASP.NET controls have been completely re-engineered from the ground up using CSS, XHTML, and the jQuery UI Framework. 

Each sample is broken into the following areas to help you better understand the feature being showcased:
- DESCRIPTION: An explanation of what the sample is demonstrating.
- SAMPLE STEPS: Steps on how to work the sample at run time.
- EXAMPLE: A brief set of steps to explain how to use the feature in your own project. 
