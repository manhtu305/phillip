<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Annotation.aspx.cs" Inherits="ControlExplorer.C1CompositeChart.Annotation" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1CompositeChart runat="server" ID="C1CompositeChart1" Height="475" Width="756" Stacked="false">
		<Footer Compass="South" Visible="False"></Footer>
		<Axis>
			<X Text="">
				<GridMajor Visible="True"></GridMajor>

				<GridMinor Visible="False"></GridMinor>
			</X>
			<Y Text="<%$ Resources:C1CompositeChart, Annotation_AxisYText %>" Compass="West">
				<GridMajor Visible="True"></GridMajor>

				<GridMinor Visible="False"></GridMinor>
			</Y>
		</Axis>
		<Header Text="<%$ Resources:C1CompositeChart, Annotation_HeaderText %>">
		</Header>
		<SeriesList>
			<wijmo:CompositeChartSeries Label="West" LegendEntry="true" Type="Column">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Desktops"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Notebooks"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="AIO"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Tablets"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Phones"></wijmo:ChartXData>
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="5"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="3"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="4"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="7"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="2"></wijmo:ChartYData>
						</Values>
					</Y>
				</Data>

				<CandlestickSeries LegendEntry="True"></CandlestickSeries>
			</wijmo:CompositeChartSeries>
			<wijmo:CompositeChartSeries Label="Central" LegendEntry="true" Type="Column">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Desktops"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Notebooks"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="AIO"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Tablets"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Phones"></wijmo:ChartXData>
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="2"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="2"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="3"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="2"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="1"></wijmo:ChartYData>
						</Values>
					</Y>
				</Data>

				<CandlestickSeries LegendEntry="True"></CandlestickSeries>
			</wijmo:CompositeChartSeries>
			<wijmo:CompositeChartSeries Label="East" LegendEntry="true" Type="Column">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Desktops"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Notebooks"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="AIO"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Tablets"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Phones"></wijmo:ChartXData>
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="3"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="4"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="4"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="2"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="5"></wijmo:ChartYData>
						</Values>
					</Y>
				</Data>

				<CandlestickSeries LegendEntry="True"></CandlestickSeries>
			</wijmo:CompositeChartSeries>
			<wijmo:CompositeChartSeries Label="US" LegendEntry="true" Type="Line">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Desktops"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Notebooks"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="AIO"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Tablets"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Phones"></wijmo:ChartXData>
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="3"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="6"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="2"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="9"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="5"></wijmo:ChartYData>
						</Values>
					</Y>
				</Data>

				<CandlestickSeries LegendEntry="True"></CandlestickSeries>
			</wijmo:CompositeChartSeries>
			<wijmo:CompositeChartSeries Label="Canada" LegendEntry="true" Type="Line">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Desktops"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Notebooks"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="AIO"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Tablets"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Phones"></wijmo:ChartXData>
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="3"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="4"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="7"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="2"></wijmo:ChartYData>
						</Values>
					</Y>
				</Data>
				<CandlestickSeries LegendEntry="True"></CandlestickSeries>
			</wijmo:CompositeChartSeries>
		</SeriesList>

		<Indicator Visible="False"></Indicator>
		<Hint IsContentHtml="true"></Hint>
		<Annotations>
			<wijmo:TextAnnotation Attachment="Relative" Offset="0, 0" Text="Relative" 
				Tooltip="<%$ Resources:C1CompositeChart, Annotation_Tooltip1 %>">
				<Point>
					<X DoubleValue="0.75" />
					<Y DoubleValue="0.15" />
				</Point>
				<AnnotationStyle FontSize="26" FontWeight="Bold">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:TextAnnotation>
			<wijmo:RectAnnotation Attachment="DataIndex" Content="DataIndex" Width="100" Offset="0, 0" PointIndex="1" 
				Tooltip="<%$ Resources:C1CompositeChart, Annotation_Tooltip2 %>">
				<AnnotationStyle FillOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:RectAnnotation>
			<wijmo:EllipseAnnotation Attachment="Relative" Content="Relative" Height="35" Offset="0, 0" 
				Tooltip="<%$ Resources:C1CompositeChart, Annotation_Tooltip3 %>" Width="75">
				<Point>
					<X DoubleValue="0.4" />
					<Y DoubleValue="0.45" />
				</Point>
				<AnnotationStyle FillOpacity="1" Stroke="#c2955f" StrokeWidth="2" StrokeOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
					</AnnotationStyle>
			</wijmo:EllipseAnnotation>
			<wijmo:CircleAnnotation Attachment="DataIndex" PointIndex="3" SeriesIndex="1" Radius="40" Content="DataIndex" Offset="0, 0" 
				Tooltip="<%$ Resources:C1CompositeChart, Annotation_Tooltip4 %>">
				<AnnotationStyle FillOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:CircleAnnotation>
			<wijmo:ImageAnnotation Attachment="DataIndex" PointIndex="0" Href="./../Images/c1icon.png" Offset="0, 0" Content=" " 
				Tooltip="<%$ Resources:C1CompositeChart, Annotation_Tooltip5 %>">
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:ImageAnnotation>
			<wijmo:PolygonAnnotation Offset="0, 0" Tooltip="This is polygon annotation.</br>Points: [(200,0),(150,50),(175,100),(255,100),(250, 50)]</br> Attachment: Absolute" Content="Absolute">
				<Points>
					<wijmo:PointF X="200" Y="0" />
					<wijmo:PointF X="150" Y="50" />
					<wijmo:PointF X="175" Y="100" />
					<wijmo:PointF X="225" Y="100" />
					<wijmo:PointF X="250" Y="50" />
				</Points>
				<AnnotationStyle FillOpacity="2" Stroke="#01A9DB" StrokeWidth="4" StrokeOpacity="1">
					<Fill Color="#FFFFFF"> </Fill >
				</AnnotationStyle>
			</wijmo:PolygonAnnotation>
			<wijmo:SquareAnnotation Attachment="DataIndex" Width="40" Content="DataIndex" Offset="0, 0" PointIndex="4" SeriesIndex="2" 
				Tooltip="<%$ Resources:C1CompositeChart, Annotation_Tooltip6 %>">
				<AnnotationStyle FillOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:SquareAnnotation>
			<wijmo:LineAnnotation Content="Absolute" End="350, 300" Offset="0, 0" Start="50, 150" 
				Tooltip="<%$ Resources:C1CompositeChart, Annotation_Tooltip7 %>">
				<AnnotationStyle FillOpacity="2" StrokeWidth="4" Stroke="#01A9DB">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:LineAnnotation>
		</Annotations>

	</wijmo:C1CompositeChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1CompositeChart.Annotation_Text0 %></p>
	<h3><%= Resources.C1CompositeChart.Annotation_Text1 %></h3>
	<ul>
		<li><%= Resources.C1CompositeChart.Annotation_Li %></li>
		<li><%= Resources.C1CompositeChart.Annotation_Li2 %></li>
	</ul>
</asp:Content>
