<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="SharedPieDataBind.aspx.cs" Inherits="ControlExplorer.C1CompositeChart.SharedPieDataBind" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1CompositeChart runat="server" ID="CompositeChart1" Culture="en-US" DataSourceID="AccessDataSource1" Height="475" Width="756">
		<DataBindings>
			<wijmo:C1CompositeChartBinding Type="SharedPie" Center="200, 140" PieSeriesDataField="Sales" Radius="80" PieSeriesLabelField="CategoryName" SharedPieGroup="A" />
			<wijmo:C1CompositeChartBinding Type="SharedPie" Center="400, 140" PieSeriesDataField="Sales" Radius="80" PieSeriesLabelField="CategoryName" SharedPieGroup="B" />
			<wijmo:C1CompositeChartBinding Type="SharedPie" Center="200, 320" PieSeriesDataField="Sales" Radius="80" PieSeriesLabelField="CategoryName" SharedPieGroup="A" />
			<wijmo:C1CompositeChartBinding Type="SharedPie" Center="400, 320" PieSeriesDataField="Sales" Radius="80" PieSeriesLabelField="CategoryName" SharedPieGroup="B" />
		</DataBindings>
	</wijmo:C1CompositeChart>
	<asp:AccessDataSource ID="AccessDataSource1" runat="server" DataFile="~/App_Data/C1NWind.mdb" SelectCommand="select top 5 CategoryName, sum(ProductSales) as Sales from (SELECT DISTINCTROW Categories.CategoryName as CategoryName, Products.ProductName, Sum([Order Details Extended].ExtendedPrice) AS ProductSales
FROM Categories INNER JOIN (Products INNER JOIN (Orders INNER JOIN [Order Details Extended] ON Orders.OrderID = [Order Details Extended].OrderID) ON Products.ProductID = [Order Details Extended].ProductID) ON Categories.CategoryID = Products.CategoryID
WHERE (((Orders.OrderDate) Between #1/1/95# And #12/31/95#))
GROUP BY Categories.CategoryID, Categories.CategoryName, Products.ProductName
ORDER BY Products.ProductName) group by CategoryName;"></asp:AccessDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	 <p><%= Resources.C1CompositeChart.SharedPieDataBind_Text0 %></p>
</asp:Content>
