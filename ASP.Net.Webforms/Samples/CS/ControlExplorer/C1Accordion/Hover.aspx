<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Hover.aspx.cs" Inherits="ControlExplorer.C1Accordion.Hover" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Accordion"
	TagPrefix="C1Accordion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<C1Accordion:C1Accordion ID="C1Accordion1" runat="server" Event="mouseover" Width="80%">
		<Panes>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane1" runat="server">
				<Header><%= Resources.C1Accordion.Hover_Header1 %></Header>
				<Content>
					<p><%= Resources.C1Accordion.Hover_Text0 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane2" runat="server">
				<Header><%= Resources.C1Accordion.Hover_Header2 %></Header>
				<Content>
					<p><%= Resources.C1Accordion.Hover_Text1 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane3" runat="server">
				<Header><%= Resources.C1Accordion.Hover_Header3 %></Header>
				<Content>
					<p><%= Resources.C1Accordion.Hover_Text2 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane4" runat="server">
				<Header><%= Resources.C1Accordion.Hover_Header4 %></Header>
				<Content>
					<p><%= Resources.C1Accordion.Hover_Text3 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
		</Panes>
	</C1Accordion:C1Accordion>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Accordion.Hover_Text4 %></p>
	<p><%= Resources.C1Accordion.Hover_Text5 %></p>
	<p><%= Resources.C1Accordion.Hover_Text6 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
