<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="RequireOpenedPane.aspx.cs" Inherits="ControlExplorer.C1Accordion.RequireOpenedPane" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Accordion"
	TagPrefix="C1Accordion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<C1Accordion:C1Accordion ID="C1Accordion1" runat="server" RequireOpenedPane="false" Width="80%" SelectedIndex="-1">
		<Panes>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane1" runat="server">
				<Header><%= Resources.C1Accordion.RequireOpenedPane_Header1 %></Header>
				<Content>
					<p><%= Resources.C1Accordion.RequireOpenedPane_Text0 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane2" runat="server">
				<Header><%= Resources.C1Accordion.RequireOpenedPane_Header2 %></Header>
				<Content>
					<p><%= Resources.C1Accordion.RequireOpenedPane_Text1 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane3" runat="server">
				<Header><%= Resources.C1Accordion.RequireOpenedPane_Header3 %></Header>
				<Content>
					<p><%= Resources.C1Accordion.RequireOpenedPane_Text2 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane4" runat="server">
				<Header><%= Resources.C1Accordion.RequireOpenedPane_Header4 %></Header>
				<Content>
					<p><%= Resources.C1Accordion.RequireOpenedPane_Text3 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
		</Panes>
	</C1Accordion:C1Accordion>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Accordion.RequireOpenedPane_Text4 %></p>
	<p><%= Resources.C1Accordion.RequireOpenedPane_Text5 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
