<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
	Inherits="ControlExplorer.Accordion.Accordion_Overview" CodeBehind="Overview.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Accordion"
	TagPrefix="C1Accordion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<C1Accordion:C1Accordion runat="server" ID="C1Accordion1" Width="80%">
		<Panes>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane1" runat="server">
				<Header>
					<%= Resources.C1Accordion.Overview_Header1 %>
				</Header>
				<Content>
					<%= Resources.C1Accordion.Overview_Content1 %>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane2" runat="server">
				<Header>
					<%= Resources.C1Accordion.Overview_Header2 %>
				</Header>
				<Content>
					<%= Resources.C1Accordion.Overview_Content2 %>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane3" runat="server">
				<Header>
					<%= Resources.C1Accordion.Overview_Header3 %>
				</Header>
				<Content>
					<%= Resources.C1Accordion.Overview_Content3 %>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane4" runat="server" Expanded="True">
				<Header>
					<%= Resources.C1Accordion.Overview_Header4 %>
				</Header>
				<Content>
					<%= Resources.C1Accordion.Overview_Content4 %>
				</Content>
			</C1Accordion:C1AccordionPane>
		</Panes>
	</C1Accordion:C1Accordion>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Accordion.Overview_Text0 %></p>
	<p><%= Resources.C1Accordion.Overview_Text1 %></p>
	<p><%= Resources.C1Accordion.Overview_Text2 %></p>
	<p><%= Resources.C1Accordion.Overview_Text3 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
