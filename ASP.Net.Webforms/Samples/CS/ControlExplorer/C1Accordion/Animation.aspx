<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="Animation.aspx.cs" Inherits="ControlExplorer.C1Accordion.Animation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Accordion"
	TagPrefix="C1Accordion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<h3><%= Resources.C1Accordion.Animation_AnimationWithEasing %></h3>
	<C1Accordion:C1Accordion ID="C1Accordion1" runat="server" Animated-Effect="easeInOutCirc" Width="80%">
		<Panes>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane1" runat="server">
				<Header>
					<%= Resources.C1Accordion.Animation_Header1 %>
				</Header>
				<Content>
					<p><%= Resources.C1Accordion.Animation_Text0 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane2" runat="server">
				<Header>
					<%= Resources.C1Accordion.Animation_Header2 %>
				</Header>
				<Content>
					<p><%= Resources.C1Accordion.Animation_Text1 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane3" runat="server">
				<Header>
					<%= Resources.C1Accordion.Animation_Header3 %>
				</Header>
				<Content>
					<p><%= Resources.C1Accordion.Animation_Text2 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
		</Panes>
	</C1Accordion:C1Accordion>
	<br />
	<h3><%= Resources.C1Accordion.Animation_DisabledAnimation %></h3>
	<C1Accordion:C1Accordion ID="C1Accordion2" runat="server" Animated-Disabled="True" Width="80%">
		<Panes>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane4" runat="server">
				<Header>
					<%= Resources.C1Accordion.Animation_Header4 %>
				</Header>
				<Content>
					<p><%= Resources.C1Accordion.Animation_Text3 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane5" runat="server">
				<Header>
					<%= Resources.C1Accordion.Animation_Header5 %>
				</Header>
				<Content>
					<p><%= Resources.C1Accordion.Animation_Text4 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane6" runat="server">
				<Header>
					<%= Resources.C1Accordion.Animation_Header6 %>
				</Header>
				<Content>
					<p><%= Resources.C1Accordion.Animation_Text5 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
		</Panes>
	</C1Accordion:C1Accordion>
	<br />
	<h3><%= Resources.C1Accordion.Animation_CustomAnimation %></h3>
	<C1Accordion:C1Accordion ID="C1Accordion3" runat="server" Animated-Effect="custom1" Width="80%">
		<Panes>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane7" runat="server">
				<Header>
					<%= Resources.C1Accordion.Animation_Header7 %>
				</Header>
				<Content>
					<p><%= Resources.C1Accordion.Animation_Text6 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane8" runat="server">
				<Header>
					<%= Resources.C1Accordion.Animation_Header8 %>
				</Header>
				<Content>
					<p><%= Resources.C1Accordion.Animation_Text7 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane9" runat="server">
				<Header>
					<%= Resources.C1Accordion.Animation_Header9 %>
				</Header>
				<Content>
					<p><%= Resources.C1Accordion.Animation_Text8 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
		</Panes>
	</C1Accordion:C1Accordion>
	<script id="scriptInit" type="text/javascript">
		$(document).ready(function () {
			jQuery.wijmo.wijaccordion.animations.custom1 = function (options) {
				this.slide(options, {
					easing: options.down ? "easeOutBounce" : "swing",
					duration: options.down ? 1000 : 200
				});
			}
		});
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Accordion.Animation_Text9 %></p>
	<p><%= Resources.C1Accordion.Animation_Text10 %></p>
	<p><%= Resources.C1Accordion.Animation_Text11 %></p>
	<p><%= Resources.C1Accordion.Animation_Text12 %></p>
	<p><%= Resources.C1Accordion.Animation_Text13 %></p>
	<p><%= Resources.C1Accordion.Animation_Text14 %></p>
	<ul>
		<li><%= Resources.C1Accordion.Animation_Li1 %></li>
		<li><%= Resources.C1Accordion.Animation_Li2 %></li>
		<li><%= Resources.C1Accordion.Animation_Li3 %> </li>
		<li><%= Resources.C1Accordion.Animation_Li4 %></li>
		<li><%= Resources.C1Accordion.Animation_Li5 %></li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
