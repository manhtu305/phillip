<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="PostBackEvents.aspx.cs" Inherits="ControlExplorer.C1Accordion.AutoPostBack" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Accordion"
	TagPrefix="C1Accordion" %>
<%@ Register Src="~/ServerSideLogger.ascx" TagPrefix="uc1" TagName="ServerSideLogger" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server">
	</asp:ScriptManager>
	<h2><%= Resources.C1Accordion.PostBackEvents_Title1 %></h2>
	<asp:UpdatePanel ID="UpdatePanel1" runat="server">
		<ContentTemplate>
			<C1Accordion:C1Accordion ID="C1Accordion1" CssClass="serversidelogger-before" runat="server" Width="300px" OnSelectedIndexChanged="C1Accordion1_OnSelectedIndexChanged">
				<Panes>
					<C1Accordion:C1AccordionPane ID="C1AccordionPane1" runat="server">
						<Header>
							<%= Resources.C1Accordion.PostBackEvents_Header1 %>
						</Header>
						<Content>
							<%= Resources.C1Accordion.PostBackEvents_Content1 %>
						</Content>
					</C1Accordion:C1AccordionPane>
					<C1Accordion:C1AccordionPane ID="C1AccordionPane2" runat="server">
						<Header>
							<%= Resources.C1Accordion.PostBackEvents_Header2 %>
						</Header>
						<Content>
							<%= Resources.C1Accordion.PostBackEvents_Content2 %>
						</Content>
					</C1Accordion:C1AccordionPane>
					<C1Accordion:C1AccordionPane ID="C1AccordionPane3" runat="server">
						<Header>
							<%= Resources.C1Accordion.PostBackEvents_Header3 %>
						</Header>
						<Content>
							<%= Resources.C1Accordion.PostBackEvents_Content3 %>
						</Content>
					</C1Accordion:C1AccordionPane>
					<C1Accordion:C1AccordionPane ID="C1AccordionPane4" runat="server" Expanded="True">
						<Header>
							<%= Resources.C1Accordion.PostBackEvents_Header4 %>
						</Header>
						<Content>
							<%= Resources.C1Accordion.PostBackEvents_Content4 %>
						</Content>
					</C1Accordion:C1AccordionPane>
				</Panes>
			</C1Accordion:C1Accordion>
			<uc1:serversidelogger runat="server" id="UpdatePanelServerSideLogger" Title="<%$ Resources:C1Accordion, PostBackEvents_LastFiredEvent %>"/>
		</ContentTemplate>
	</asp:UpdatePanel>
	<br>
	<h2><%= Resources.C1Accordion.PostBackEvents_Title2 %></h2>
	<C1Accordion:C1Accordion ID="C1Accordion2" CssClass="serversidelogger-before" runat="server" Width="300px" OnSelectedIndexChanged="C1Accordion2_OnSelectedIndexChanged">
		<Panes>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane1" runat="server">
				<Header>
					<%= Resources.C1Accordion.PostBackEvents_Header1 %>
				</Header>
				<Content>
					<%= Resources.C1Accordion.PostBackEvents_Content1 %>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane2" runat="server">
				<Header>
					<%= Resources.C1Accordion.PostBackEvents_Header2 %>
				</Header>
				<Content>
					<%= Resources.C1Accordion.PostBackEvents_Content2 %>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane3" runat="server">
				<Header>
					<%= Resources.C1Accordion.PostBackEvents_Header3 %>
				</Header>
				<Content>
					<%= Resources.C1Accordion.PostBackEvents_Content3 %>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane4" runat="server" Expanded="True">
				<Header>
					<%= Resources.C1Accordion.PostBackEvents_Header4 %>
				</Header>
				<Content>
					<%= Resources.C1Accordion.PostBackEvents_Content4 %>
				</Content>
			</C1Accordion:C1AccordionPane>
		</Panes>
	</C1Accordion:C1Accordion>
	<uc1:serversidelogger runat="server" id="ServerSideLogger" Title="<%$ Resources:C1Accordion, PostBackEvents_LastFiredEvent %>"/>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Accordion.PostBackEvents_Text0 %></p>
	<p><%= Resources.C1Accordion.PostBackEvents_Text1 %></p>
	<p><%= Resources.C1Accordion.PostBackEvents_Text2 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
