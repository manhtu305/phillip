<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ClientModel.aspx.cs" Inherits="ControlExplorer.C1Accordion.ClientObjectModel" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Accordion"
	TagPrefix="C1Accordion" %>
<%@ Register Src="../ClientLogger.ascx" TagName="ClientLogger" TagPrefix="ClientLogger" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<C1Accordion:C1Accordion runat="server" ID="C1Accordion1" 
		OnClientSelectedIndexChanged="onClientSelectedIndexChanged" Width="80%">
		<Panes>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane1" runat="server">
				<Header>
					<%= Resources.C1Accordion.ClientModel_Header1 %>
				</Header>
				<Content><%= Resources.C1Accordion.ClientModel_Content1 %></Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane2" runat="server">
				<Header>
					<%= Resources.C1Accordion.ClientModel_Header2 %>
				</Header>
				<Content><%= Resources.C1Accordion.ClientModel_Content2 %></Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane3" runat="server">
				<Header>
					<%= Resources.C1Accordion.ClientModel_Header3 %>
				</Header>
				<Content><%= Resources.C1Accordion.ClientModel_Content3 %></Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="Accordion1Pane4" runat="server" Expanded="True">
				<Header>
					<%= Resources.C1Accordion.ClientModel_Header4 %>
				</Header>
				<Content><%= Resources.C1Accordion.ClientModel_Content4 %></Content>
			</C1Accordion:C1AccordionPane>
		</Panes>
	</C1Accordion:C1Accordion>
  <br/>
	<ClientLogger:ClientLogger ID="ClientLogger1" runat="server" />
	<script type="text/javascript">
		var selectedIndexInputUpdating = false;
		function updateSelectedIndexInput() {
			selectedIndexInputUpdating = true;
			var input = $("#selectedIndexInput");
			input.empty();
			var c1Accordion1 = $("#<%=C1Accordion1.ClientID%>");
		var count = c1Accordion1.c1accordion("count");
		if (count > 0) {
			for (var i = 0; i < count; i++) {
				input.append($("<option>").val(i).text(i));
			}
			input.val(c1Accordion1.c1accordion("option", "selectedIndex"));
		}
		selectedIndexInputUpdating = false;
	}

	function clearPanes() {
		$('#<%=C1Accordion1.ClientID%>	').c1accordion('clear');
		updateSelectedIndexInput();
	}

	function onClientSelectedIndexChanged(ev, args) {
	    log.message("<%= Resources.C1Accordion.ClientModel_Log1 %>" + args.newIndex);
		updateSelectedIndexInput();
	}
	var _dynPanesCount = 0;
	function addNewPane() {
		$("#<%=C1Accordion1.ClientID%>").c1accordion("add", "dyn header " + ++_dynPanesCount, "dyn content " + _dynPanesCount);
		updateSelectedIndexInput();
	}
	function insertNewPane() {
		var index = parseInt($("#paneIndexToInsert").val(), 10);
		if ((index || index === 0) && index >= 0 && index < $("#<%=C1Accordion1.ClientID%>").c1accordion("count") + 1) {
			_dynPanesCount++;
			$("#<%=C1Accordion1.ClientID%>").c1accordion("insert", index, "dyn header " + index, "dyn content " + index);
			updateSelectedIndexInput();
		} else {
		    alert("<%= Resources.C1Accordion.ClientModel_Log2 %>" + $("#paneIndexToInsert").val());
		}
	}
	function showPanesCount() {
		alert($("#<%=C1Accordion1.ClientID%>").c1accordion("count"));
	}
	function removePaneByIndex() {
		var index = parseInt($("#paneIndexToRemove").val(), 10);
		if ((index || index === 0) && index >= 0 && index < $("#<%=C1Accordion1.ClientID%>").c1accordion("count")) {
			$("#<%=C1Accordion1.ClientID%>").c1accordion("removeAt", index);
		    log.message("<%= Resources.C1Accordion.ClientModel_Log3 %>" + index + "<%= Resources.C1Accordion.ClientModel_Log4 %>");
			updateSelectedIndexInput();
		} else {
		    alert("<%= Resources.C1Accordion.ClientModel_Log5 %>" + $("#paneIndexToRemove").val());
		}
	}
	$(document).ready(function () {

		$("#requireOpenedPaneCheckBox")[0].checked = $("#<%=C1Accordion1.ClientID%>").c1accordion("option", "requireOpenedPane");
		updateSelectedIndexInput();

		$("#requireOpenedPaneCheckBox").bind("change", function () {
			var requireOpenedPane = $("#requireOpenedPaneCheckBox")[0].checked;
			$("#<%=C1Accordion1.ClientID%>").c1accordion("option", "requireOpenedPane", requireOpenedPane);
		    log.message("<%= Resources.C1Accordion.ClientModel_Log6 %>" + requireOpenedPane);
		});
	});

	function applySelectedIndex() {
		if (selectedIndexInputUpdating) return;

		var index = parseInt($("#selectedIndexInput").val(), 10);
		if ((index || index === 0) && index >= 0 && index < $("#<%=C1Accordion1.ClientID%>").c1accordion("count")) {
			$("#<%=C1Accordion1.ClientID%>").c1accordion("option", "selectedIndex", index);
		    log.message("<%= Resources.C1Accordion.ClientModel_Log7 %>" + index + "");
		} else {
		    alert("<%= Resources.C1Accordion.ClientModel_Log8 %>" + $("#selectedIndexInput").val());
		}
	}
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
<p><%= Resources.C1Accordion.ClientModel_Text0 %></p>
<p><%= Resources.C1Accordion.ClientModel_Text1 %></p>
<ul>
<li><%= Resources.C1Accordion.ClientModel_Li1 %></li>
<li><%= Resources.C1Accordion.ClientModel_Li2 %></li>
</ul>
<p><%= Resources.C1Accordion.ClientModel_Text2 %></p>
<ul>
<li><%= Resources.C1Accordion.ClientModel_Li3 %></li>
<li><%= Resources.C1Accordion.ClientModel_Li4 %></li>
<li><%= Resources.C1Accordion.ClientModel_Li5 %></li>
<li><%= Resources.C1Accordion.ClientModel_Li6 %></li>
<li><%= Resources.C1Accordion.ClientModel_Li7 %></li>
</ul>
<p><%= Resources.C1Accordion.ClientModel_Text3 %></p>
<p><%= Resources.C1Accordion.ClientModel_Text4 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
<div class="settingcontainer">
<div class="settingcontent">
<ul>
	<li class="fullwidth">
		<input type="checkbox" id="requireOpenedPaneCheckBox" /><label for="requireOpenedPaneCheckBox"><%= Resources.C1Accordion.ClientModel_RequireOpenedPane %></label>
	</li>
	<li class="fullwidth">
		<Label><%= Resources.C1Accordion.ClientModel_SelectedIndex %></Label>
		<select id="selectedIndexInput" onchange="applySelectedIndex(); return false;"></select>
	</li>
	<li class="fullwidth">
		<label class="settinglegend"><%= Resources.C1Accordion.ClientModel_Panes %></label>
	</li>
	<li class="fullwidth">
		<input type="button" title="<%= Resources.C1Accordion.ClientModel_GetPanesCount %>" value="<%= Resources.C1Accordion.ClientModel_Count %>" onclick="showPanesCount(); return false;"/>
		<input type="button" value="<%= Resources.C1Accordion.ClientModel_Add %>" onclick="addNewPane(); return false;"/>
		<input type="button" title="<%= Resources.C1Accordion.ClientModel_RemoveAllPanes %>" value="<%= Resources.C1Accordion.ClientModel_Clear %>" onclick="clearPanes(); return false;"/>
	</li>
	<li class="fullwidth">
		<input type="button" title="<%= Resources.C1Accordion.ClientModel_InsertNewPane %>" value="<%= Resources.C1Accordion.ClientModel_InsertAt %>" onclick="insertNewPane(); return false;"/>
		<input type="text" id="paneIndexToInsert" value="0" />
	</li>
	<li class="fullwidth">
		<input type="button" title="<%= Resources.C1Accordion.ClientModel_RemovePaneByIndex %>" value="<%= Resources.C1Accordion.ClientModel_RemoveAt %>" onclick="removePaneByIndex(); return false;"/>
		<input type="text" id="paneIndexToRemove" value="0" />
	</li>
</ul>
</div>
</div>
</asp:Content>
