<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ExpandDirection.aspx.cs" Inherits="ControlExplorer.C1Accordion.ExpandDirection" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Accordion"
	TagPrefix="C1Accordion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
	<C1Accordion:C1Accordion ID="C1Accordion1" runat="server">
		<Panes>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane1" runat="server">
				<Header>1</Header>
				<Content>
					<p><%= Resources.C1Accordion.ExpandDirection_Text0 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane2" runat="server">
				<Header>2</Header>
				<Content>
					<p><%= Resources.C1Accordion.ExpandDirection_Text1 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane3" runat="server">
				<Header>3</Header>
				<Content>
					<p><%= Resources.C1Accordion.ExpandDirection_Text2 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
			<C1Accordion:C1AccordionPane ID="C1AccordionPane4" runat="server">
				<Header>4</Header>
				<Content>
					<p><%= Resources.C1Accordion.ExpandDirection_Text3 %></p>
					<p><%= Resources.C1Accordion.ExpandDirection_Text4 %></p>
				</Content>
			</C1Accordion:C1AccordionPane>
		</Panes>
	</C1Accordion:C1Accordion>
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Accordion.ExpandDirection_Text5 %></p>
	<p><%= Resources.C1Accordion.ExpandDirection_Text6 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
<script type="text/javascript">
	function setExpandDirection(direction) {
		// clear fixed padding values that was assigned 
		// during vertical animation.
		// Note, this is not needed when you use the server 
		// side ExpandDirection property.
		$("#<%=C1Accordion1.ClientID%> > .ui-accordion-content")
				.css("paddingTop", "")
				.css("paddingBottom", "");
		// Change the expandDirection option.
		$("#<%=C1Accordion1.ClientID%>")
					.c1accordion("option", "expandDirection", direction);
	}

	$(function() {
		$("#<%=CSExpandDirectionDDL.ClientID%>").change(function(event) {
			setExpandDirection($(event.target).find("option:selected").text());
		});
	});
</script>
<div class="settingcontainer">
<div class="settingcontent">
<ul>
	<li class="fullwidth"><label style="width:250px"><%= Resources.C1Accordion.ExpandDirection_Label1 %></label>
		<asp:DropDownList ID="CSExpandDirectionDDL" runat="server"></asp:DropDownList>
	</li>
	<li class="fullwidth">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
			<ContentTemplate>
        <label style="width:250px"><%= Resources.C1Accordion.ExpandDirection_Label2 %></label>
		    <asp:DropDownList ID="SSExpandDirectionDDL" runat="server" AutoPostBack="True" OnSelectedIndexChanged="SSExpandDirectionDDL_SelectedIndexChanged"></asp:DropDownList>
			</ContentTemplate>
		</asp:UpdatePanel>
	</li>
</ul>
</div>
</div>
</asp:Content>
