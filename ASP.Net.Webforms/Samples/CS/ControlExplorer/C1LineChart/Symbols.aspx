<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
    CodeBehind="Symbols.aspx.cs" Inherits="C1LineChart_Symbols" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <script type="text/javascript">
        function hintContent() {
            return this.label + '\n' + Globalize.format(this.x, "MMMM yyyy") + " : " + Globalize.format(this.y, "c2");
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <wijmo:C1LineChart runat="server" ID="C1LineChart1" ShowChartLabels="False" Height="475" Width="756">
        <Header Text="<%$ Resources:C1LineChart, Symbols_HeaderText %>"></Header>
        <Axis>
            <X Compass="South" Text="<%$ Resources:C1LineChart, Symbols_AxisXText %>"></X>
            <Y Compass="West" Text="<%$ Resources:C1LineChart, Symbols_AxisYText %>"></Y>
        </Axis>
        <Hint>
            <Content Function="hintContent" />
        </Hint>
        <SeriesStyles>
            <wijmo:ChartStyle Stroke="#FFA41C" StrokeWidth="1" Opacity="0.7" />
        </SeriesStyles>
        <SeriesList>
            <wijmo:LineChartSeries Label="#Amazon" LegendEntry="true">
                <Markers Visible="true" Type="Circle">
                </Markers>
                <Data>
                    <X>
                        <Values>
                            <wijmo:ChartXData DateTimeValue="2012-1-1" />
                            <wijmo:ChartXData DateTimeValue="2012-2-1" />
                            <wijmo:ChartXData DateTimeValue="2012-3-1" />
                            <wijmo:ChartXData DateTimeValue="2012-4-1" />
                            <wijmo:ChartXData DateTimeValue="2012-5-1" />
                            <wijmo:ChartXData DateTimeValue="2012-6-1" />
                            <wijmo:ChartXData DateTimeValue="2012-7-1" />
                            <wijmo:ChartXData DateTimeValue="2012-8-1" />
                            <wijmo:ChartXData DateTimeValue="2012-9-1" />
                            <wijmo:ChartXData DateTimeValue="2012-10-1" />
                            <wijmo:ChartXData DateTimeValue="2012-11-1" />
                            <wijmo:ChartXData DateTimeValue="2012-12-1" />
                        </Values>
                    </X>
                    <Y>
                        <Values>
                            <wijmo:ChartYData DoubleValue="194.44" />
                            <wijmo:ChartYData DoubleValue="179" />
                            <wijmo:ChartYData DoubleValue="202.51" />
                            <wijmo:ChartYData DoubleValue="231.90" />
                            <wijmo:ChartYData DoubleValue="212.91" />
                            <wijmo:ChartYData DoubleValue="228.35" />
                            <wijmo:ChartYData DoubleValue="233.30" />
                            <wijmo:ChartYData DoubleValue="248.27" />
                            <wijmo:ChartYData DoubleValue="254.32" />
                            <wijmo:ChartYData DoubleValue="232.89" />
                            <wijmo:ChartYData DoubleValue="252.05" />
                            <wijmo:ChartYData DoubleValue="250.87" />
                        </Values>
                    </Y>
                </Data>
            </wijmo:LineChartSeries>
        </SeriesList>
    </wijmo:C1LineChart>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p><%= Resources.C1LineChart.Symbols_Text0 %></p>
    <p><%= Resources.C1LineChart.Symbols_Text1 %></p>
    <ul>
        <li><%= Resources.C1LineChart.Symbols_Li1 %></li>
        <li><%= Resources.C1LineChart.Symbols_Li2 %></li>
        <li><%= Resources.C1LineChart.Symbols_Li3 %></li>
        <li><%= Resources.C1LineChart.Symbols_Li4 %></li>
        <li><%= Resources.C1LineChart.Symbols_Li5 %></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <label><%= Resources.C1LineChart.Symbols_MarkerType %></label>
                    <asp:DropDownList ID="cbxMarkerTypes" AutoPostBack="true" runat="server" OnSelectedIndexChanged="cbxMarkerTypes_SelectedIndexChanged">
                        <asp:ListItem runat="server" Selected="True" Text="<%$ Resources:C1LineChart, Symbols_Circle %>" Value="Circle" />
                        <asp:ListItem runat="server" Text="<%$ Resources:C1LineChart, Symbols_Cross %>" Value="Cross" />
                        <asp:ListItem runat="server" Text="<%$ Resources:C1LineChart, Symbols_Diamond %>" Value="Diamond" />
                        <asp:ListItem runat="server" Text="<%$ Resources:C1LineChart, Symbols_Tri %>" Value="Tri" />
                        <asp:ListItem runat="server" Text="<%$ Resources:C1LineChart, Symbols_InvertedTri %>" Value="InvertedTri" />
                    </asp:DropDownList>
                </li>
            </ul>
        </div>
    </div>

</asp:Content>

