﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.C1Chart;

namespace ControlExplorer.C1LineChart
{
	public partial class Annotation : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				var series = new LineChartSeries();
				series.Label = "Sale";
				series.Data.Y.AddRange(new double[]{
				96, 19, 54, 83, 15, 56, 36, 4, 29, 93,
				38, 71, 50, 77, 69, 13, 79, 57, 29, 62,
				4, 27, 66, 96, 65, 12, 52, 3, 61, 48, 50,
				70, 39, 33, 25, 49, 69, 46, 44, 40, 35,
				72, 64, 10, 66, 63, 78, 19, 96, 26});
				for (int i = 0; i < 50; i++)
				{
					series.Data.X.Add(new DateTime(2014, 1, 1).AddDays(i));
				}
				C1LineChart1.SeriesList.Add(series);
			}
		}
	}
}