<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeBehind="StrokeStyles.aspx.cs" Inherits="C1LineChart_StrokeStyles" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <script type="text/javascript">
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <wijmo:C1LineChart ID="C1LineChart1" ShowChartLabels="false" runat="server" Height="475" Width="756">
        <Header Text="<%$ Resources:C1LineChart, StrokeStyles_HeaderText %>"></Header>
        <Hint Enable="false">
        </Hint>
        <Footer Compass="South" Visible="False"></Footer>
        <Legend Visible="true" Compass="South" Orientation="Horizontal"></Legend>
        <Axis>
            <X Text="<%$ Resources:C1LineChart, StrokeStyles_AxisXText %>" Compass="South"></X>
            <Y Text="<%$ Resources:C1LineChart, StrokeStyles_AxisYText %>" Compass="West"></Y>
        </Axis>
        <SeriesStyles>
            <wijmo:ChartStyle Stroke="#2d2d2d" StrokeWidth="3" StrokeOpacity="0.7" StrokeDashArray=""></wijmo:ChartStyle>
            <wijmo:ChartStyle Stroke="#5f9996" StrokeWidth="3" StrokeOpacity="0.7"></wijmo:ChartStyle>
            <wijmo:ChartStyle Stroke="#afe500" StrokeWidth="3" StrokeOpacity="0.7"></wijmo:ChartStyle>
            <wijmo:ChartStyle Stroke="#b2c76d" StrokeWidth="3" StrokeOpacity="0.7"></wijmo:ChartStyle>
            <wijmo:ChartStyle Stroke="#959595" StrokeWidth="3" StrokeOpacity="0.7"></wijmo:ChartStyle>
        </SeriesStyles>
        <SeriesHoverStyles>
            <wijmo:ChartStyle StrokeWidth="5" StrokeOpacity="1"></wijmo:ChartStyle>
            <wijmo:ChartStyle StrokeWidth="5" StrokeOpacity="1"></wijmo:ChartStyle>
            <wijmo:ChartStyle StrokeWidth="5" StrokeOpacity="1"></wijmo:ChartStyle>
            <wijmo:ChartStyle StrokeWidth="5" StrokeOpacity="1"></wijmo:ChartStyle>
            <wijmo:ChartStyle StrokeWidth="5" StrokeOpacity="1"></wijmo:ChartStyle>
        </SeriesHoverStyles>
        <SeriesList>
            <wijmo:LineChartSeries Label="NVIDIA GeForce 9800" FitType="Spline" LegendEntry="true">
                <Data>
                    <X>
                        <Values>
                            <wijmo:ChartXData StringValue="May" />
                            <wijmo:ChartXData StringValue="Jun" />
                            <wijmo:ChartXData StringValue="Jul" />
                            <wijmo:ChartXData StringValue="Aug" />
                            <wijmo:ChartXData StringValue="Sep" />
                        </Values>
                    </X>
                    <Y>
                        <Values>
                            <wijmo:ChartYData DoubleValue="3.64" />
                            <wijmo:ChartYData DoubleValue="3.70" />
                            <wijmo:ChartYData DoubleValue="3.57" />
                            <wijmo:ChartYData DoubleValue="3.79" />
                            <wijmo:ChartYData DoubleValue="3.97" />
                        </Values>
                    </Y>
                </Data>
            </wijmo:LineChartSeries>
            <wijmo:LineChartSeries Label="NVIDIA GeForce 8800" FitType="Spline" LegendEntry="true">
                <Data>
                    <X>
                        <Values>
                            <wijmo:ChartXData StringValue="May" />
                            <wijmo:ChartXData StringValue="Jun" />
                            <wijmo:ChartXData StringValue="Jul" />
                            <wijmo:ChartXData StringValue="Aug" />
                            <wijmo:ChartXData StringValue="Sep" />
                        </Values>
                    </X>
                    <Y>
                        <Values>
                            <wijmo:ChartYData DoubleValue="4.46" />
                            <wijmo:ChartYData DoubleValue="4.42" />
                            <wijmo:ChartYData DoubleValue="4.23" />
                            <wijmo:ChartYData DoubleValue="4.11" />
                            <wijmo:ChartYData DoubleValue="4.10" />
                        </Values>
                    </Y>
                </Data>
            </wijmo:LineChartSeries>
            <wijmo:LineChartSeries Label="ATI Radeon HD 5700" FitType="Spline" LegendEntry="true">
                <Data>
                    <X>
                        <Values>
                            <wijmo:ChartXData StringValue="May" />
                            <wijmo:ChartXData StringValue="Jun" />
                            <wijmo:ChartXData StringValue="Jul" />
                            <wijmo:ChartXData StringValue="Aug" />
                            <wijmo:ChartXData StringValue="Sep" />
                        </Values>
                    </X>
                    <Y>
                        <Values>
                            <wijmo:ChartYData DoubleValue="4.22" />
                            <wijmo:ChartYData DoubleValue="4.92" />
                            <wijmo:ChartYData DoubleValue="5.46" />
                            <wijmo:ChartYData DoubleValue="5.84" />
                            <wijmo:ChartYData DoubleValue="6.82" />
                        </Values>
                    </Y>
                </Data>
            </wijmo:LineChartSeries>
            <wijmo:LineChartSeries Label="ATI Radeon HD 5800" FitType="Spline" LegendEntry="true">
                <Data>
                    <X>
                        <Values>
                            <wijmo:ChartXData StringValue="May" />
                            <wijmo:ChartXData StringValue="Jun" />
                            <wijmo:ChartXData StringValue="Jul" />
                            <wijmo:ChartXData StringValue="Aug" />
                            <wijmo:ChartXData StringValue="Sep" />
                        </Values>
                    </X>
                    <Y>
                        <Values>
                            <wijmo:ChartYData DoubleValue="4.66" />
                            <wijmo:ChartYData DoubleValue="5.20" />
                            <wijmo:ChartYData DoubleValue="5.62" />
                            <wijmo:ChartYData DoubleValue="6.02" />
                            <wijmo:ChartYData DoubleValue="7.04" />
                        </Values>
                    </Y>
                </Data>
            </wijmo:LineChartSeries>
            <wijmo:LineChartSeries Label="Mobile Intel 4 Series Express" FitType="Spline" LegendEntry="true">
                <Data>
                    <X>
                        <Values>
                            <wijmo:ChartXData StringValue="May" />
                            <wijmo:ChartXData StringValue="Jun" />
                            <wijmo:ChartXData StringValue="Jul" />
                            <wijmo:ChartXData StringValue="Aug" />
                            <wijmo:ChartXData StringValue="Sep" />
                        </Values>
                    </X>
                    <Y>
                        <Values>
                            <wijmo:ChartYData DoubleValue="1.91" />
                            <wijmo:ChartYData DoubleValue="1.82" />
                            <wijmo:ChartYData DoubleValue="1.94" />
                            <wijmo:ChartYData DoubleValue="1.89" />
                            <wijmo:ChartYData DoubleValue="1.72" />
                        </Values>
                    </Y>
                </Data>
            </wijmo:LineChartSeries>
        </SeriesList>
    </wijmo:C1LineChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p><%= Resources.C1LineChart.StrokeStyles_Text0 %></p>
    <h3><%= Resources.C1LineChart.StrokeStyles_Text1 %></h3>
    <ul>
        <li><%= Resources.C1LineChart.StrokeStyles_Li1 %></li>
        <li><%= Resources.C1LineChart.StrokeStyles_Li2 %></li>
        <li><%= Resources.C1LineChart.StrokeStyles_Li3 %></li>
        <li><%= Resources.C1LineChart.StrokeStyles_Li4 %></li>
        <li><%= Resources.C1LineChart.StrokeStyles_Li5 %></li>
        <li><%= Resources.C1LineChart.StrokeStyles_Li6 %></li>
        <li><%= Resources.C1LineChart.StrokeStyles_Li7 %></li>
        <li><%= Resources.C1LineChart.StrokeStyles_Li8 %></li>
        <li><%= Resources.C1LineChart.StrokeStyles_Li9 %></li>
        <li><%= Resources.C1LineChart.StrokeStyles_Li10 %></li>
    </ul>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <label><%= Resources.C1LineChart.StrokeStyles_StrokeWidth %></label>
                    <wijmo:C1InputNumeric ID="numberStrokeWidth" runat="server" Width="60px" Value="3"></wijmo:C1InputNumeric>
                </li>
                <li>
                    <label><%= Resources.C1LineChart.StrokeStyles_StrokeOpacity %></label>
                    <wijmo:C1InputNumeric ID="numberStrokeOpacity" runat="server" Width="60px" Value="0.75"></wijmo:C1InputNumeric>
                </li>
                <li>
                    <label><%= Resources.C1LineChart.StrokeStyles_StrokePattern %></label>
                    <asp:DropDownList ID="cbxStrokePattern" runat="server">
                        <asp:ListItem Value="" Text="Solid" Selected="true" />
                        <asp:ListItem Value="-" Text="- " />
                        <asp:ListItem Value="." Text=". " />
                        <asp:ListItem Value="-." Text="-." />
                        <asp:ListItem Value="-.." Text="-.." />
                        <asp:ListItem Value=". " Text=". " />
                        <asp:ListItem Value="- " Text="- " />
                        <asp:ListItem Value="--" Text="--" />
                        <asp:ListItem Value="- ." Text="- ." />
                        <asp:ListItem Value="--." Text="--." />
                        <asp:ListItem Value="--.." Text="--.." />
                    </asp:DropDownList>
                </li>
            </ul>
            <div class="settingcontrol">
                <asp:Button ID="btnApply" runat="server" Text="<%$ Resources:C1LineChart, StrokeStyles_Apply %>" Width="65px" Height="25px" OnClick="btnApply_Click" />
            </div>
        </div>
    </div>
</asp:Content>

