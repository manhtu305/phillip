<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" 
	CodeFile="DataBinding.aspx.cs" Inherits="C1LineChart_DataBinding" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
	<script type = "text/javascript">
		function hintContent() {
			return this.y;
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<wijmo:C1LineChart ID="C1LineChart1" runat="server" DataSourceID="AccessDataSource1" ShowChartLabels="false" Height="475" Width = "756">
		<Hint>
			<Content Function="hintContent" />
		</Hint>
		<Axis>
			<X>
				<Labels>
					<AxisLabelStyle  FontSize="11pt" Rotation="-45">
						<Fill Color="#7f7f7f"></Fill>
					</AxisLabelStyle>
				</Labels>
				<TickMajor Position="Outside">
					<TickStyle Stroke="#7f7f7f" />
				</TickMajor>
			</X>
			<Y Visible="False" Compass="West">
				<Labels TextAlign="Center">
					<AxisLabelStyle FontSize="11pt">
						<Fill Color="#7f7f7f"></Fill>
					</AxisLabelStyle>
				</Labels>
				<GridMajor Visible="True">
					<GridStyle Stroke="#353539" StrokeDashArray="- " />
				</GridMajor>
				<TickMajor Position="Outside">
					<TickStyle Stroke="#7f7f7f" />
				</TickMajor>
				<TickMinor Position="Outside">
					<TickStyle Stroke="#7f7f7f" />
				</TickMinor>
			</Y>
		</Axis>
		<Header Text="<%$ Resources:C1LineChart, DataBinding_HeaderTet %>"></Header>
		<SeriesHoverStyles>
			<wijmo:ChartStyle StrokeWidth="8" Opacity="1" />
		</SeriesHoverStyles>
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#00a6dd" StrokeWidth="5" Opacity="0.8" />
		</SeriesStyles>
		<DataBindings>
			<wijmo:C1ChartBinding XField="CategoryName" XFieldType="String" YField="Sales" YFieldType="Number" />
		</DataBindings>
	</wijmo:C1LineChart>
	
	<asp:AccessDataSource ID="AccessDataSource1" runat="server" 
		DataFile="~/App_Data/C1NWind.mdb" 
		SelectCommand="select CategoryName, sum(ProductSales) as Sales from (SELECT DISTINCTROW Categories.CategoryName as CategoryName, Products.ProductName, Sum([Order Details Extended].ExtendedPrice) AS ProductSales
FROM Categories INNER JOIN (Products INNER JOIN (Orders INNER JOIN [Order Details Extended] ON Orders.OrderID = [Order Details Extended].OrderID) ON Products.ProductID = [Order Details Extended].ProductID) ON Categories.CategoryID = Products.CategoryID
WHERE (((Orders.OrderDate) Between #1/1/95# And #12/31/95#))
GROUP BY Categories.CategoryID, Categories.CategoryName, Products.ProductName
ORDER BY Products.ProductName) group by CategoryName;">
	</asp:AccessDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p><%= Resources.C1LineChart.DataBinding_Text0 %></p><br/>
	<p><%= Resources.C1LineChart.DataBinding_Text1 %></p>
	<ul>
		<li><%= Resources.C1LineChart.DataBinding_Li1 %></li>
		<li><%= Resources.C1LineChart.DataBinding_Li2 %></li>
		<li><%= Resources.C1LineChart.DataBinding_Li3 %></li>
		<li><%= Resources.C1LineChart.DataBinding_Li4 %></li>
		<li><%= Resources.C1LineChart.DataBinding_Li5 %></li>
		<li><%= Resources.C1LineChart.DataBinding_Li6 %></li>
	</ul>
	<p><%= Resources.C1LineChart.DataBinding_Text2 %></p>
	<ul>
		<li><%= Resources.C1LineChart.DataBinding_Li7 %></li>
		<li><%= Resources.C1LineChart.DataBinding_Li8 %></li>
	</ul>
	<p><%= Resources.C1LineChart.DataBinding_Text3 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

