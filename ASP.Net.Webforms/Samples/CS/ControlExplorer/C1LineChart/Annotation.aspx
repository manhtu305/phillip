<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Annotation.aspx.cs" Inherits="ControlExplorer.C1LineChart.Annotation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1LineChart runat="server" ID="C1LineChart1" ShowChartLabels="False" Height="475" Width="756">
		<Hint IsContentHtml="true"></Hint>
						<Annotations>
			<wijmo:TextAnnotation Attachment="Relative" Offset="0, 0" Text="Relative" 
				Tooltip="<%$ Resources:C1LineChart, Annotation_Tooltip1 %>">
					<Point>
					<X DoubleValue="0.75" />
						<Y DoubleValue="0.15" />
					</Point>
				<AnnotationStyle FontSize="26" FontWeight="Bold">
					<Fill Color="#e6e6e6" > </Fill >
					</AnnotationStyle>
				</wijmo:TextAnnotation>
			<wijmo:RectAnnotation Attachment="DataCoordinate" Content="DataCoordinate" Width="100" Offset="0, 0" Position="Center, Bottom" 
				Tooltip="<%$ Resources:C1LineChart, Annotation_Tooltip2 %>">
				<Point>
					<X DateTimeValue="1/10/2014" />
					<Y DoubleValue="30" />
				</Point>
				<AnnotationStyle FillOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
					</AnnotationStyle>
				</wijmo:RectAnnotation>
			<wijmo:EllipseAnnotation Attachment="Relative" Content="Relative" Height="35" Offset="0, 0" 
				Tooltip="<%$ Resources:C1LineChart, Annotation_Tooltip3 %>" Width="75">
				<Point>
					<X DoubleValue="0.4" />
					<Y DoubleValue="0.45" />
				</Point>
				<AnnotationStyle FillOpacity="1" Stroke="#c2955f" StrokeWidth="2" StrokeOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
					</AnnotationStyle>
				</wijmo:EllipseAnnotation>
			<wijmo:CircleAnnotation Attachment="DataIndex" PointIndex="33" Radius="40" SeriesIndex="0" Content="DataIndex" Offset="0, 0" 
				Tooltip="<%$ Resources:C1LineChart, Annotation_Tooltip4 %>">
				<AnnotationStyle FillOpacity="1" Stroke="#7b9e7e">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:CircleAnnotation>
			<wijmo:ImageAnnotation Attachment="DataCoordinate" Href="./../Images/c1icon.png" Offset="0, 0" Content=" " 
				Tooltip="<%$ Resources:C1LineChart, Annotation_Tooltip5 %>">
					<Point>
				<X DateTimeValue="1/25/2014" />
				<Y DoubleValue="20" />
					</Point>
					<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#e6e6e6" > </Fill >
					</AnnotationStyle>
				</wijmo:ImageAnnotation>
			<wijmo:PolygonAnnotation Offset="0, 0" Tooltip="<%$ Resources:C1LineChart, Annotation_Tooltip6 %>" Content="Absolute">
					<Points>
						<wijmo:PointF X="200" Y="0" />
						<wijmo:PointF X="150" Y="50" />
						<wijmo:PointF X="175" Y="100" />
						<wijmo:PointF X="225" Y="100" />
						<wijmo:PointF X="250" Y="50" />
					</Points>
				<AnnotationStyle FillOpacity="2" Stroke="#01A9DB" StrokeWidth="4" StrokeOpacity="1">
					<Fill Color="#FFFFFF"> </Fill >
					</AnnotationStyle>
				</wijmo:PolygonAnnotation>
			<wijmo:SquareAnnotation Attachment="DataIndex" Width="40" Content="DataIndex" Offset="0, 0" PointIndex="45" SeriesIndex="0" 
				Tooltip="<%$ Resources:C1LineChart, Annotation_Tooltip7 %>">
				<AnnotationStyle FillOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
					</AnnotationStyle>
				</wijmo:SquareAnnotation>
			<wijmo:LineAnnotation Content="Absolute" End="350, 300" Offset="0, 0" Start="50, 150" 
				Tooltip="<%$ Resources:C1LineChart, Annotation_Tooltip8 %>">
				<AnnotationStyle FillOpacity="2" StrokeWidth="4" Stroke="#01A9DB">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
				</wijmo:LineAnnotation>
			</Annotations>
	</wijmo:c1linechart>
		<script type="text/javascript">
			function hintContent() {
				return this.y;
			}
			function pageLoad() {
				var resizeTimer = null;

				$(window).resize(function () {
					window.clearTimeout(resizeTimer);
					resizeTimer = window.setTimeout(function () {
						var jqLine = $("#<%= C1LineChart1.ClientID %>"),
						width = jqLine.width(),
						height = jqLine.height();

						if (!width || !height) {
							window.clearTimeout(resizeTimer);
							return;
						}

						jqLine.c1linechart("redraw", width, height);
					}, 250);
				});
			}
		</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1LineChart.Annotation_Text0 %></p>
	<h3><%= Resources.C1LineChart.Annotation_Text1 %></h3>
	<ul>
		<li><%= Resources.C1LineChart.Annotation_Li1 %></li>
		<li><%= Resources.C1LineChart.Annotation_Li2 %></li>
	</ul>
</asp:Content>
