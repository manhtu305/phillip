<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Animation.aspx.cs" Inherits="ControlExplorer.C1LineChart.Animation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script type="text/javascript">
        function hintContent() {
            return this.data.lineSeries.label + '\n' +
			this.x + '\n' + this.y + '';
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <wijmo:C1LineChart runat="server" ID="C1LineChart1" BackColor="#242529" ShowChartLabels="false" Height="475" Width="756">
        <Animation Direction="Vertical" />
        <Footer Compass="South" Visible="False"></Footer>
        <Legend Visible="false"></Legend>
        <Hint OffsetY="-10">
            <Content Function="hintContent" />
            <ContentStyle FontSize="10pt" />
        </Hint>
        <SeriesStyles>
            <wijmo:ChartStyle Stroke="#ff9900" StrokeWidth="3" />
        </SeriesStyles>
        <SeriesHoverStyles>
            <wijmo:ChartStyle StrokeWidth="4"></wijmo:ChartStyle>
        </SeriesHoverStyles>
        <Axis>
            <X Text="<%$ Resources:C1LineChart, Animation_AxisXText %>">
                <Labels>
                    <AxisLabelStyle FontSize="11pt" Rotation="-45">
                        <Fill Color="#7f7f7f"></Fill>
                    </AxisLabelStyle>
                </Labels>
                <TickMajor Position="Outside">
                    <TickStyle Stroke="#7f7f7f" />
                </TickMajor>
                <GridMajor Visible="false"></GridMajor>
                <GridMinor Visible="false"></GridMinor>
            </X>
            <Y Text="<%$ Resources:C1LineChart, Animation_AxisYText %>" AutoMax="false" AutoMin="false" Max="100" Min="0" Compass="West">
                <Labels TextAlign="Center">
                    <AxisLabelStyle FontSize="11pt">
                        <Fill Color="#7f7f7f"></Fill>
                    </AxisLabelStyle>
                </Labels>
                <GridMajor Visible="false">
                    <GridStyle Stroke="#353539" StrokeDashArray="- " />
                </GridMajor>
                <GridMinor Visible="false"></GridMinor>
                <TickMajor Position="Outside">
                    <TickStyle Stroke="#7f7f7f" />
                </TickMajor>
                <TickMinor Position="Outside">
                    <TickStyle Stroke="#7f7f7f" />
                </TickMinor>
            </Y>
        </Axis>
    </wijmo:C1LineChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p><%= Resources.C1LineChart.Animation_Text0 %></p>
    <br />
    <p><%= Resources.C1LineChart.Animation_Text1 %></p>
    <ul>
        <li><%= Resources.C1LineChart.Animation_Li1 %></li>
        <li><%= Resources.C1LineChart.Animation_Li2 %></li>
        <li><%= Resources.C1LineChart.Animation_Li3 %></li>
        <li><%= Resources.C1LineChart.Animation_Li4 %></li>
    </ul>
    <p><%= Resources.C1LineChart.Animation_Text2 %></p>
    <ul>
        <li>easeInCubic(">")</li>
        <li>easeOutCubic("<")</li>
        <li>easeInOutCubic("<>")</li>
        <li>easeInBack("backIn")</li>
        <li>easeOutBack("backOut")</li>
        <li>easeOutElastic("elastic")</li>
        <li>easeOutBounce("bounce")</li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li class="fullwidth">
                    <label class="settinglegend" for="ckxShowXAxis"><%= Resources.C1LineChart.Animation_AnimateSettings %></label>
                </li>
                <li>
                    <asp:CheckBox ID="ckxEnableAnimation" Text="<%$ Resources:C1LineChart, Animation_Enabled %>" TextAlign="Left" runat="server" Checked="true" />
                </li>
                <li>
                    <label><%= Resources.C1LineChart.Animation_Duration %></label>
                    <wijmo:C1InputNumeric ID="numberAnimationDuration" runat="server" Width="50px" DecimalPlaces="0" Value="1000"></wijmo:C1InputNumeric>
                </li>
                <li>
                    <label><%= Resources.C1LineChart.Animation_Easing %></label>
                    <asp:DropDownList ID="cbxAnimationEasing" runat="server">
                        <asp:ListItem Text="EaseInCubic" Value="EaseInCubic" Selected="true" />
                        <asp:ListItem Text="EaseOutCubic" Value="EaseOutCubic" />
                        <asp:ListItem Text="EaseInOutCubic" Value="EaseInOutCubic" />
                        <asp:ListItem Text="EaseInBack" Value="EaseInBack" />
                        <asp:ListItem Text="EaseOutBack" Value="EaseOutBack" />
                        <asp:ListItem Text="EaseOutElastic" Value="EaseOutElastic" />
                        <asp:ListItem Text="EaseOutBounce" Value="EaseOutBounce" />
                    </asp:DropDownList>
                </li>
                <li>
                    <label><%= Resources.C1LineChart.Animation_Direction %></label>
                    <asp:DropDownList ID="cbxAnimationDirection" runat="server">
                        <asp:ListItem Text="Horizontal" Value="Horizontal" Selected="true" />
                        <asp:ListItem Text="Vertical" Value="Vertical" />
                    </asp:DropDownList>
                </li>
                <li class="fullwidth">
                    <label class="settinglegend" for="ckxShowXAxis"><%= Resources.C1LineChart.Animation_SeriesTransitionSettings %></label>
                </li>
                <li>
                    <asp:CheckBox ID="ckxEnableTransition" Text="<%$ Resources:C1LineChart, Animation_Enabled %>" TextAlign="Left" runat="server" Checked="true" />
                </li>
                <li>
                    <label><%= Resources.C1LineChart.Animation_Duration %></label>
                    <wijmo:C1InputNumeric ID="numberTranDuration" runat="server" Width="50px" DecimalPlaces="0" Value="1000"></wijmo:C1InputNumeric>
                </li>
                <li>
                    <label><%= Resources.C1LineChart.Animation_Easing %></label>
                    <asp:DropDownList ID="cbxTranEasing" runat="server">
                        <asp:ListItem Text="EaseInCubic" Value="EaseInCubic" Selected="true" />
                        <asp:ListItem Text="EaseOutCubic" Value="EaseOutCubic" />
                        <asp:ListItem Text="EaseInOutCubic" Value="EaseInOutCubic" />
                        <asp:ListItem Text="EaseInBack" Value="EaseInBack" />
                        <asp:ListItem Text="EaseOutBack" Value="EaseOutBack" />
                        <asp:ListItem Text="EaseOutElastic" Value="EaseOutElastic" />
                        <asp:ListItem Text="EaseOutBounce" Value="EaseOutBounce" />
                    </asp:DropDownList>
                </li>
            </ul>
            <div class="settingcontrol">
                <asp:Button ID="btnApply" Text="<%$ Resources:C1LineChart, Animation_Apply %>" CssClass="settingapply" runat="server" OnClick="btnReload_Click" />
            </div>
        </div>
    </div>
</asp:Content>
