<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ExtremeValues.aspx.cs" Inherits="C1LineChart_ExtremeValues" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script type="text/javascript">
        function hintContent() {
            return this.x + ' * ' + this.x + ' = ' + this.y;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1LineChart ShowChartLabels="false" runat="server" ID="C1LineChart1" Height="400" Width="756">
        <Hint>
            <Content Function="hintContent" />
        </Hint>
        <Header Text="<%$ Resources:C1LineChart, ExtremeValues_HeaderText %>">
        </Header>
        <Legend Visible="false">
            <Size Width="30" Height="3"></Size>
        </Legend>
        <SeriesList>
            <wijmo:LineChartSeries Label="Parabola" FitType="Spline" LegendEntry="true">
                <Markers Visible="false" Type="Circle">
                </Markers>
                <Data>
                    <X>
                        <Values>
                            <wijmo:ChartXData DoubleValue="-10" />
                            <wijmo:ChartXData DoubleValue="-9" />
                            <wijmo:ChartXData DoubleValue="-8" />
                            <wijmo:ChartXData DoubleValue="-7" />
                            <wijmo:ChartXData DoubleValue="-6" />
                            <wijmo:ChartXData DoubleValue="-5" />
                            <wijmo:ChartXData DoubleValue="-4" />
                            <wijmo:ChartXData DoubleValue="-3" />
                            <wijmo:ChartXData DoubleValue="-2" />
                            <wijmo:ChartXData DoubleValue="-1" />
                            <wijmo:ChartXData DoubleValue=" 0" />
                            <wijmo:ChartXData DoubleValue="1" />
                            <wijmo:ChartXData DoubleValue="2" />
                            <wijmo:ChartXData DoubleValue="3" />
                            <wijmo:ChartXData DoubleValue="4" />
                            <wijmo:ChartXData DoubleValue="5" />
                            <wijmo:ChartXData DoubleValue="6" />
                            <wijmo:ChartXData DoubleValue="7" />
                            <wijmo:ChartXData DoubleValue="8" />
                            <wijmo:ChartXData DoubleValue="9" />
                            <wijmo:ChartXData DoubleValue="10" />
                        </Values>
                    </X>
                    <Y>
                        <Values>
                            <wijmo:ChartXData DoubleValue="100" />
                            <wijmo:ChartXData DoubleValue="81" />
                            <wijmo:ChartXData DoubleValue="64" />
                            <wijmo:ChartXData DoubleValue="49" />
                            <wijmo:ChartXData DoubleValue="36" />
                            <wijmo:ChartXData DoubleValue="25" />
                            <wijmo:ChartXData DoubleValue="16" />
                            <wijmo:ChartXData DoubleValue="9" />
                            <wijmo:ChartXData DoubleValue="4" />
                            <wijmo:ChartXData DoubleValue="1" />
                            <wijmo:ChartXData DoubleValue="0" />
                            <wijmo:ChartXData DoubleValue="1" />
                            <wijmo:ChartXData DoubleValue="4" />
                            <wijmo:ChartXData DoubleValue="9" />
                            <wijmo:ChartXData DoubleValue="16" />
                            <wijmo:ChartXData DoubleValue="25" />
                            <wijmo:ChartXData DoubleValue="36" />
                            <wijmo:ChartXData DoubleValue="49" />
                            <wijmo:ChartXData DoubleValue="64" />
                            <wijmo:ChartXData DoubleValue="81" />
                            <wijmo:ChartXData DoubleValue="100" />
                        </Values>
                    </Y>
                </Data>
            </wijmo:LineChartSeries>
        </SeriesList>
        <Axis>
            <X AutoMin="true" AutoMax="true" Visible="true">
            </X>
            <Y AutoMin="true" AutoMax="true" Visible="true" Compass="West">
            </Y>
        </Axis>
    </wijmo:C1LineChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">

    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li class="fullwidth">
                    <label class="settinglegend" for="ckxShowXAxis"><%= Resources.C1LineChart.ExtremeValues_XAxis %></label>
                </li>
                <li>
                    <asp:CheckBox ID="ckxEnableAutoMinX" runat="server" Text="<%$ Resources:C1LineChart, ExtremeValues_AutoMinEnabled %>" Checked="true" />
                </li>
                <li>
                    <label><%= Resources.C1LineChart.ExtremeValues_MinValue %></label>
                    <wijmo:C1InputNumeric ID="numberXMin" runat="server" Width="80px" Value="-5">
                    </wijmo:C1InputNumeric>
                </li>
                <li style="white-space:nowrap;">
                    <asp:CheckBox ID="ckxEnableAutoMaxX" runat="server" Text="<%$ Resources:C1LineChart, ExtremeValues_AutoMaxEnabled %>" Checked="true" />
                </li>
                <li>
                    <label><%= Resources.C1LineChart.ExtremeValues_MaxValue %></label>
                    <wijmo:C1InputNumeric ID="numberXMax" runat="server" Width="80px" Value="5">
                    </wijmo:C1InputNumeric>
                </li>
                <li class="fullwidth">
                    <label class="settinglegend" for="ckxShowXAxis"><%= Resources.C1LineChart.ExtremeValues_YAxis %></label>
                </li>
                <li>
                    <asp:CheckBox ID="ckxEnableAutoMinY" runat="server" Text="<%$ Resources:C1LineChart, ExtremeValues_AutoMinEnabled %>" Checked="true" />
                </li>
                <li>
                    <label><%= Resources.C1LineChart.ExtremeValues_MinValue %></label>
                    <wijmo:C1InputNumeric ID="numberYMin" runat="server" Width="80px" Value="-5">
                    </wijmo:C1InputNumeric>
                </li>
                <li style="white-space:nowrap;">
                    <asp:CheckBox ID="ckxEnableAutoMaxY" runat="server" Text="<%$ Resources:C1LineChart, ExtremeValues_AutoMaxEnabled %>" Checked="true" />
                </li>
                <li>
                    <label><%= Resources.C1LineChart.ExtremeValues_MaxValue %></label>
                    <wijmo:C1InputNumeric ID="numberYMax" runat="server" Width="80px" Value="25">
                    </wijmo:C1InputNumeric>
                </li>
            </ul>
            <div class="settingcontrol">
                <asp:Button ID="btnApply" runat="server" OnClick="btnApply_Click" Text="<%$ Resources:C1LineChart, ExtremeValues_Apply %>" Width="70px" Height="25px" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1LineChart.ExtremeValues_Text0 %></p>
    <h3>Test the features</h3>
    <ul>
        <li><%= Resources.C1LineChart.ExtremeValues_Li1 %></li>
        <li><%= Resources.C1LineChart.ExtremeValues_Li2 %></li>
        <li><%= Resources.C1LineChart.ExtremeValues_Li3 %></li>
        <li><%= Resources.C1LineChart.ExtremeValues_Li4 %></li>
    </ul>

</asp:Content>
