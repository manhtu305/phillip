<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Range.aspx.cs" Inherits="ControlExplorer.C1Slider.Range" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Slider" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .header2
        {
            margin-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2 class="header2">
        <%= Resources.C1Slider.Range_Range %></h2>
    <wijmo:C1Slider runat="server" ID="C1Slider1" DragFill="false" Max="500" Range="true" Min="0" Step="2" Orientation="Horizontal" Values="100,400" Width="300px" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Slider.Range_Text0 %></p>
    <p><%= Resources.C1Slider.Range_Text1 %></p>
    <ul>
        <li>Range: true</li>
        <li>Values</li>
    </ul>
    <p><%= Resources.C1Slider.Range_Text2 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
