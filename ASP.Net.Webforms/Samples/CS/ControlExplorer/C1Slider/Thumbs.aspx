<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Thumbs.aspx.cs" Inherits="ControlExplorer.C1Slider.Thumbs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Slider" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .header2
        {
            margin-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2 class="header2">
        <%= Resources.C1Slider.Thumbs_Thumbs %></h2>
    <wijmo:C1Slider runat="server" ID="C1Slider1" Max="500" Range="false" Min="0" Step="50" Orientation="Horizontal" Values="400,40" Width="300px" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Slider.Thumbs_Text0 %></p>
    <p><%= Resources.C1Slider.Thumbs_Text1 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
