<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ToolbarCustomization.aspx.cs" Inherits="ControlExplorer.C1ReportViewer.ToolBarCustomization" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer" TagPrefix="C1ReportViewer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div style="width:720px">
    <C1ReportViewer:C1ReportViewer runat="server" ID="C1ReportViewer1" FileName="~/C1ReportViewer/C1ReportXML/BarcodeLabels.xml" ReportName="Product Labels (EAN-13, Label 5096)" Zoom="75%" CollapseToolsPanel="True" Height="475px" Width="100%">
    </C1ReportViewer:C1ReportViewer>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1ReportViewer.ToolbarCustomization_Text0 %></p>
    <p><%= Resources.C1ReportViewer.ToolbarCustomization_Text1 %></p>
    <ul>
        <li><%= Resources.C1ReportViewer.ToolbarCustomization_Li1 %></li>
        <li><%= Resources.C1ReportViewer.ToolbarCustomization_Li2 %></li>
        <li><%= Resources.C1ReportViewer.ToolbarCustomization_Li3 %></li>
        <li><%= Resources.C1ReportViewer.ToolbarCustomization_Li4 %></li>
        <li><%= Resources.C1ReportViewer.ToolbarCustomization_Li5 %></li>
        <li><%= Resources.C1ReportViewer.ToolbarCustomization_Li6 %></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
