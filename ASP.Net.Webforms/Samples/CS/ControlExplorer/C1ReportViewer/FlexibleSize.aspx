<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="FlexibleSize.aspx.cs" Inherits="ControlExplorer.C1ReportViewer.FlexibleSize" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer" TagPrefix="C1ReportViewer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="parentContainerSample" style="width: 720px; height: 475px;">
        <C1ReportViewer:C1ReportViewer runat="server" ID="C1ReportViewer1" FileName="C1ReportViewer/C1ReportXML/BarcodeLabels.xml" ReportName="Product Labels (EAN-13, Label 5096)" Zoom="Fit width" Height="100%" Width="100%" CollapseToolsPanel="True">
        </C1ReportViewer:C1ReportViewer>
    </div>
    <script type="text/javascript">
        function changeParentSize(w, h) {
            $("#parentContainerSample").width(w).height(h);
            $("#<%=C1ReportViewer1.ClientID%>").c1reportviewer("refresh");
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1ReportViewer.FlexibleSize_Text0 %></p>
    <p><%= Resources.C1ReportViewer.FlexibleSize_Text1 %></p>
    <ul>
        <li><%= Resources.C1ReportViewer.FlexibleSize_Li1 %></li>
        <li><%= Resources.C1ReportViewer.FlexibleSize_Li2 %></li>
        <li><%= Resources.C1ReportViewer.FlexibleSize_Li3 %></li>
        <li><%= Resources.C1ReportViewer.FlexibleSize_Li4 %></li>
        <li><%= Resources.C1ReportViewer.FlexibleSize_Li5 %></li>
        <li><%= Resources.C1ReportViewer.FlexibleSize_Li6 %></li>
    </ul>
    <p><%= Resources.C1ReportViewer.FlexibleSize_Text2 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li class="fullwidth"><h3><%= Resources.C1ReportViewer.FlexibleSize_ChangeParentContainerSize %></h3></li>
                <li><a href="javascript:changeParentSize(320, 240);"><%= Resources.C1ReportViewer.FlexibleSize_ChangeSize1 %></a>
                </li>
                <li><a href="javascript:changeParentSize(600, 400);"><%= Resources.C1ReportViewer.FlexibleSize_ChangeSize2 %></a>
                </li>
                <li><a href="javascript:changeParentSize('100%', '475');"><%= Resources.C1ReportViewer.FlexibleSize_ChangeSize3 %></a>
                </li>
            </ul>
        </div>
    </div>
</asp:Content>
