<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ErrorHandling.aspx.cs" Inherits="ControlExplorer.C1ReportViewer.ErrorHandling" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer" TagPrefix="C1ReportViewer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div style="width:720px">
    <C1ReportViewer:C1ReportViewer runat="server" ID="C1ReportViewer1" FileName="C1ReportViewer/C1ReportXML/BarcodeLabels.xml" ReportName="Wrong report name" EnableLogs="true" CollapseToolsPanel="True" Height="475px" Width="100%">
    </C1ReportViewer:C1ReportViewer>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1ReportViewer.ErrorHandling_Text0 %></p>
    <p><%= Resources.C1ReportViewer.ErrorHandling_Text1 %></p>
    <ul>
        <li><%= Resources.C1ReportViewer.ErrorHandling_Li1 %></li>
        <li><%= Resources.C1ReportViewer.ErrorHandling_Li2 %></li>
        <li><%= Resources.C1ReportViewer.ErrorHandling_Li3 %></li>
        <li><%= Resources.C1ReportViewer.ErrorHandling_Li4 %></li>
        <li><%= Resources.C1ReportViewer.ErrorHandling_Li5 %></li>
        <li><%= Resources.C1ReportViewer.ErrorHandling_Li6 %></li>
    </ul>
    <p><%= Resources.C1ReportViewer.ErrorHandling_Text2 %></p>
    <p><%= Resources.C1ReportViewer.ErrorHandling_Text3 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
