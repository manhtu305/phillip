<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ReportParameters.aspx.cs" Inherits="ControlExplorer.C1ReportViewer.ReportParameters" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer" TagPrefix="C1ReportViewer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div style="width:720px">
    <C1ReportViewer:C1ReportViewer runat="server" ID="C1ReportViewer1" FileName="~/C1ReportViewer/RDL/ReportParameters01.rdl" Zoom="75%" Height="475px" Width="100%">
    </C1ReportViewer:C1ReportViewer>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1ReportViewer.ReportParameters_Text0 %></p>
    <p><%= Resources.C1ReportViewer.ReportParameters_Text1 %></p>
    <ul>
        <li><%= Resources.C1ReportViewer.ReportParameters_Li1 %></li>
        <li><%= Resources.C1ReportViewer.ReportParameters_Li2 %></li>
        <li><%= Resources.C1ReportViewer.ReportParameters_Li3 %></li>
        <li><%= Resources.C1ReportViewer.ReportParameters_Li4 %></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
