<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.cs" Inherits="ControlExplorer.C1ReportViewer.Overview" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer" TagPrefix="C1ReportViewer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div style="width:720px">
    <C1ReportViewer:C1ReportViewer runat="server" ID="C1ReportViewer1" FileName="C1ReportViewer/Rdl/Nice1_StandardGroups.rdl" Zoom="Fit Width" Height="475px" Width="100%">
    </C1ReportViewer:C1ReportViewer>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1ReportViewer.Overview_Text0 %></p>
    <p><%= Resources.C1ReportViewer.Overview_Text1 %></p>
    <p><%= Resources.C1ReportViewer.Overview_Text2 %></p>
    <ul>
        <li><%= Resources.C1ReportViewer.Overview_Li1 %></li>
        <li><%= Resources.C1ReportViewer.Overview_Li2 %></li>
        <li><%= Resources.C1ReportViewer.Overview_Li3 %></li>
        <li><%= Resources.C1ReportViewer.Overview_Li4 %></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
