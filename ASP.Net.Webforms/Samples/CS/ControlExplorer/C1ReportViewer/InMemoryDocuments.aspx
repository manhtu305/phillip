<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="InMemoryDocuments.aspx.cs" Inherits="ControlExplorer.C1ReportViewer.InMemoryDocument" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ReportViewer" TagPrefix="C1ReportViewer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div style="width:720px">
    <C1ReportViewer:C1ReportViewer runat="server" ID="C1ReportViewer1" FileName="InMemoryBasicTable" Zoom="75%" Height="475px" Width="100%">
    </C1ReportViewer:C1ReportViewer>
</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1ReportViewer.InMemoryDocuments_Text0 %></p>
    <p><%= Resources.C1ReportViewer.InMemoryDocuments_Text1 %></p>
    <p><%= Resources.C1ReportViewer.InMemoryDocuments_Text2 %></p>
    <p><%= Resources.C1ReportViewer.InMemoryDocuments_Text3 %></p>
    <p><%= Resources.C1ReportViewer.InMemoryDocuments_Text4 %></p>
    <%= Resources.C1ReportViewer.InMemoryDocuments_Text5 %>
    <%= Resources.C1ReportViewer.InMemoryDocuments_Text6 %>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
