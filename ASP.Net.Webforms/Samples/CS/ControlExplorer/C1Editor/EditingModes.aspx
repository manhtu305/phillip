<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="EditingModes.aspx.cs" Inherits="ControlExplorer.C1Editor.EditingModes" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Editor"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<wijmo:C1Editor runat="server" ID="Editor1" Width="760" Height="530"></wijmo:C1Editor>
		</ContentTemplate>
	</asp:UpdatePanel>
	<script type="text/javascript">
		function switchview(index) {
			var eidtor = $("#<%= Editor1.ClientID %>");
			switch (index) {
				case 1:
					//sets the editor is "wysiwyg" mode.
					eidtor.c1editor("executeEditorAction", "wysiwyg");
					break;
				case 2:
					//sets the editor is "code" mode.
					eidtor.c1editor("executeEditorAction", "code");
					break;
				case 3:
					//sets the editor is "split" mode(wysiwyg and code).
					eidtor.c1editor("executeEditorAction", "split");
					break;
			}
		}

	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Editor.EditingModes_Text0 %></p>
	<p><%= Resources.C1Editor.EditingModes_Text1 %></p>
	<ul>
		<li><%= Resources.C1Editor.EditingModes_Li1 %></li>
		<li><%= Resources.C1Editor.EditingModes_Li2 %></li>
		<li><%= Resources.C1Editor.EditingModes_Li3 %></li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<asp:UpdatePanel ID="UpdatePanel2" runat="server">
		<ContentTemplate>
			<div class="settingcontainer">
				<div class="settingcontent">
					<label><%= Resources.C1Editor.EditingModes_EditingModesLabel %></label>
					<ul>
						<li>
							<input type="radio" id="view_Design" name="view" checked="checked" onclick="switchview(1)" />
                            <label for="view_Design">
                                <%= Resources.C1Editor.EditingModes_DesignViewLabel %></label>
						</li>
						<li>
							 <input type="radio" id="view_Source" name="view" onclick="switchview(2)" />
                            <label for="view_Source">
                                <%= Resources.C1Editor.EditingModes_SourceViewLabel %></label>
						</li>
						<li>
							<input type="radio" id="view_Split" name="view" onclick="switchview(3)" />
                            <label for="view_Split">
                                <%= Resources.C1Editor.EditingModes_SplitViewLabel %></label>
						</li>
					</ul>
				</div>
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
