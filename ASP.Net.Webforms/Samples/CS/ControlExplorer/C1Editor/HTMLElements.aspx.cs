﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ControlExplorer.C1Editor
{
	public partial class HTMLElements : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				this.C1Editor1.Text = Resources.C1Editor.HTMLElements_Content;
			}
		}
	}
}