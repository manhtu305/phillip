<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ReadOnly.aspx.cs" Inherits="ControlExplorer.C1Editor.ReadOnly" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Editor"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1Editor runat="server" ID="edt" ReadOnly="true" Text="<%$ Resources:C1Editor, ReadOnly_Content %>" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Editor.ReadOnly_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<asp:CheckBox runat="server" ID="chkReadOnly" Checked="true" Text="<%$ Resources:C1Editor, ReadOnly_ReadOnlyLabel %>" 
		AutoPostBack="true" OnCheckedChanged="chkReadOnly_CheckedChanged"/>
</asp:Content>
