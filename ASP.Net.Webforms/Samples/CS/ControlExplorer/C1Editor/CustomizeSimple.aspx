<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="CustomizeSimple.aspx.cs" Inherits="ControlExplorer.C1Editor.CustomizeSimple" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Editor"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1Editor runat="server" ID="Editor1" Width="760" Height="530" Mode="Simple"></wijmo:C1Editor>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Editor.CustomizeSimple_Text0 %></p>
	<p><%= Resources.C1Editor.CustomizeSimple_Text1 %></p>
		<ul style="list-style-type:none">
			<li><%= Resources.C1Editor.CustomizeSimple_Li1 %></li>
			<li><%= Resources.C1Editor.CustomizeSimple_Li2 %></li>
		</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
