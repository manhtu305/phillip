﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using System.IO;
using C1.Web.Wijmo.Controls.C1TreeView;

namespace ControlExplorer
{
    public partial class Wijmo : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindNavigation();
            RegisterCallbacks();
        }

        protected override void OnPreRender(EventArgs e)
        {
            RemoveUselessPane(this.Description, 1);
            RemoveUselessPane(this.ControlOptions, 0);

            base.OnPreRender(e);
        }

        private void RemoveUselessPane(ContentPlaceHolder cph, int index)
        {
            var needRemovePane = cph.Controls.Count == 0;

            if (cph.Controls.Count == 1 && (cph.Controls[0] is LiteralControl))
            {
                var lc = cph.Controls[0] as LiteralControl;

                if (string.IsNullOrEmpty(lc.Text.Trim(new char[] { '\r', '\n', '\t', ' ' })))
                {
                    needRemovePane = true;
                }
            }

            if (needRemovePane)
            {
                var sb = new System.Text.StringBuilder();
                using (var sw = new System.IO.StringWriter(sb))
                {
                    using (var tw = new HtmlTextWriter(sw))
                    {
                        try
                        {
                            cph.RenderControl(tw);
                        }
                        catch
                        {
                            tw.Close();
                        }
                    }
                }
                needRemovePane = string.IsNullOrEmpty(sb.ToString().Trim());
            }

            if (needRemovePane)
            {
                this.C1Accordion1.Panes.RemoveAt(index);
            }
        }

        private void BindNavigation()
        {
            var culture = System.Threading.Thread.CurrentThread.CurrentCulture;
            bool isJp = culture.TwoLetterISOLanguageName == "ja";

            System.Xml.Linq.XElement xEle;
            if (Cache["ControlList"] == null)
            {
                xEle = System.Xml.Linq.XElement.Load(Server.MapPath("~/ControlList.xml"));
                Cache["ControlList"] = xEle;
            }
            else
            {
                xEle = (System.Xml.Linq.XElement)Cache["ControlList"];
            }
            IEnumerable<XElement> catalogs = from c in xEle.Elements("Control") select c;

            var relativePaths = Request.AppRelativeCurrentExecutionFilePath.Split('/');

            IEnumerable<XElement> pageList = from control2 in catalogs
                                             where control2.Attribute("name").Value.ToLower() == relativePaths[1].Replace("C1", "").ToLower()
                                             select control2;

            NavTreeView.Nodes.Clear();

            if (pageList != null)
            {
                if (pageList.Count() < 1)
                {
                    var childControls = from c in xEle.Descendants("ChildControl") select c;

                    pageList = from control2 in childControls
                               where control2.Attribute("name").Value.ToLower() == relativePaths[1].Replace("C1", "").ToLower()
                               select control2;
                }

                if (pageList.Count() > 0)
                {
                    BuildTreeViewNode<C1.Web.Wijmo.Controls.C1TreeView.C1TreeView>(NavTreeView, pageList, isJp);
                }
            }
            else
            {
                this.NavTreeView.Visible = false;
            }

            var control = pageList == null ? null : pageList.FirstOrDefault();
            string docKey = null;
            if (control != null)
            {
                var keyAttr = control.Attribute("documentationKey");
                if (keyAttr != null)
                {
                    docKey = keyAttr.Value;
                }
            }

            string docPath;
            var relativePath = relativePaths[1].ToLower();

            if (isJp)
            {
                if (relativePath.Contains("eventscalendar"))
                {
                    docPath = "https://docs.grapecity.com/help/c1/aspnet/aspwij_eventscalendar/";
                }
                else if (relativePath.Contains("c1input"))
                {
                    docPath = "https://docs.grapecity.com/help/c1/aspnet/aspwij_input/";
                }
                else if (relativePath.Contains("pdf"))
                {
                    docPath = "https://docs.grapecity.com/help/c1/winforms-aspnet/winasp_pdf/";
                }
                else if (relativePath.Contains("excel"))
                {
                    docPath = "https://docs.grapecity.com/help/c1/winforms-aspnet/winasp_excel/";
                }
                else
                {
                    docPath = "https://docs.grapecity.com/help/c1/aspnet/aspwij_" + DecorateDocKey(docKey);
                }
            }
            else
            {
                if (docKey != null)
                {
                    docPath = "http://help.grapecity.com/componentone/NetHelp/" + docKey + "/webframe.html";                  
                }
                else
                {
                    //Build documentation link
                    if (relativePath.Contains("eventscalendar"))
                    {
                        //fix for evcal
                        docPath = "http://help.grapecity.com/componentone/NetHelp/c1evcalWeb/webframe.html";
                    }
                    else if (relativePath.Contains("c1input"))
                    {
                        docPath = "http://help.grapecity.com/componentone/NetHelp/c1inputWeb/webframe.html";
                    }
                    else
                    {
                        docPath = "http://help.grapecity.com/componentone/NetHelp/" + relativePath + "Web/webframe.html";
                    }
                }
            }
            docs.HRef = docPath;


            //Hide theme switcher for chart/gauge/barcode/qrcode pages
            if (relativePath.Contains("chart") || relativePath.Contains("gauge")
                || relativePath == ("c1barcode") || relativePath == ("c1qrcode"))
            {
                switcherContainer.Visible = false;
            }
        }

        private string DecorateDocKey(string docKey)
        {
            return docKey.ToLower().Replace("c1", "").Replace("web","");
        }

        private void BuildTreeViewNode<T>(T node, IEnumerable<XElement> actionList, bool isJp) where T : IC1TreeViewNodeCollectionOwner
        {
            foreach (XElement action in actionList.First().Elements("action"))
            {
                C1TreeViewNode n = new C1TreeViewNode();
                node.Nodes.Add(n);

                var page = action.Attribute("page");
                var name = action.Attribute("name");

                if (name != null)
                {
                    n.Text = name.Value;
                }

                if (isJp)
                {
                    var jpText = action.Attribute("jpText");
                    if (jpText != null)
                    {
                        n.Text = jpText.Value;
                    }
                }

                if (page != null)
                {
                    n.Url = page.Value;

                    //Highlight treeview node.
                    if (page.Value.ToLower() == Request.AppRelativeCurrentExecutionFilePath.ToLower())
                    {
                        if (node is C1TreeViewNode)
                        {
                            (node as C1TreeViewNode).Expanded = true;
                        }

                        n.Selected = true;

                        var parent = action.Parent;
                        var ctrlName = string.Empty;
                        var title = n.Text; //action.name

                        do
                        {
                            var parentName = parent.Attribute("name");

                            if (parentName != null)
                            {
                                var parentTitle = parentName.Value;
                                title = parentTitle + " - " + title;

                                if (parent.Name.LocalName.ToLower().Equals("control"))
                                {
                                    ctrlName = parentTitle;
                                    if (isJp)
                                    {
                                        var jpText = parent.Attribute("jpText");
                                        if (jpText != null)
                                        {
                                            ctrlName = jpText.Value;
                                        }
                                    }
                                    break;
                                }
                            }

                            parent = parent.Parent;

                        } while (parent != null);

                        Page.Title = title;
                        ControlName.InnerText = ctrlName;
                        ControlSampleLabel.Text = n.Text;
                    }
                }

                var subActions = action.Elements("subactions");

                if (subActions.Count() > 0)
                {
                    BuildTreeViewNode<C1TreeViewNode>(n, subActions,isJp);
                }
            }
        }
    }
}