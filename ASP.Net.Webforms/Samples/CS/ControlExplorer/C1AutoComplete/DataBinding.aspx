<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="DataBinding.aspx.cs" Inherits="ControlExplorer.C1AutoComplete.DataBinding" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1AutoComplete" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1AutoComplete ID="C1AutoComplete1" runat="server" Width="250px"
        DataSourceID="AccessDataSource1">
    </wijmo:C1AutoComplete>
	<asp:AccessDataSource ID="AccessDataSource1" runat="server" 
	DataFile="~/App_Data/C1NWind.mdb" 
	SelectCommand="SELECT ProductID, ProductName FROM Products">
	</asp:AccessDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1AutoComplete.DataBinding_Text0 %></p>
    <p><%= Resources.C1AutoComplete.DataBinding_Text1 %></p>
    <ul>
        <li><%= Resources.C1AutoComplete.DataBinding_Li1 %></li>
        <li><%= Resources.C1AutoComplete.DataBinding_Li2 %></li>
        <li><%= Resources.C1AutoComplete.DataBinding_Li3 %></li>
        <li><%= Resources.C1AutoComplete.DataBinding_Li4 %></li>
    </ul>
</asp:Content>
