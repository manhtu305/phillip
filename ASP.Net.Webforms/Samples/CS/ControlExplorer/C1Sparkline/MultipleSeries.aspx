<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
	Inherits="Sparkline_MultipleSeries" CodeBehind="MultipleSeries.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Sparkline"
	TagPrefix="C1Sparkline" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<C1Sparkline:C1Sparkline ID="Sparkline1" runat="server">
        <Animation Duration="800" />
        <SeriesList>
            <C1Sparkline:SparklineSeries Bind="Score" Type="Area">
                <SeriesStyle Fill-Color="Orange" Stroke="Orange"></SeriesStyle>
            </C1Sparkline:SparklineSeries>
            <C1Sparkline:SparklineSeries Bind="Mood">
                <SeriesStyle StrokeWidth="2"></SeriesStyle>
            </C1Sparkline:SparklineSeries>
        </SeriesList>
	</C1Sparkline:C1Sparkline>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Sparkline.MultipleSeries_Text0 %></p>
	<p><%= Resources.C1Sparkline.MultipleSeries_Text1 %></p>
</asp:Content>
