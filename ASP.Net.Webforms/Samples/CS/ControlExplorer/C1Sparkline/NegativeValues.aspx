<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
	Inherits="Sparkline_NegativeValues" CodeBehind="NegativeValues.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Sparkline"
	TagPrefix="C1Sparkline" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <h3><%= Resources.C1Sparkline.NegativeValues_ValueAxisOff %></h3>
	<C1Sparkline:C1Sparkline ID="Sparkline1" runat="server">
        <SeriesList>
            <C1Sparkline:SparklineSeries Type="Area">
            </C1Sparkline:SparklineSeries>
        </SeriesList>
	</C1Sparkline:C1Sparkline>
    <h3><%= Resources.C1Sparkline.NegativeValues_ValueAxisOn %></h3>
    <C1Sparkline:C1Sparkline ID="Sparkline2" runat="server" ValueAxis="true">
        <SeriesList>
            <C1Sparkline:SparklineSeries Type="Area">
            </C1Sparkline:SparklineSeries>
        </SeriesList>
	</C1Sparkline:C1Sparkline>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Sparkline.NegativeValues_Text0 %></p>
	<p><%= Resources.C1Sparkline.NegativeValues_Text1 %></p>
</asp:Content>
