<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
	Inherits="Sparkline_SeriesAndBind" CodeBehind="SeriesAndBind.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Sparkline"
	TagPrefix="C1Sparkline" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<C1Sparkline:C1Sparkline ID="Sparkline1" runat="server">
        <SeriesList>
            <C1Sparkline:SparklineSeries Bind="score" />
        </SeriesList>
	</C1Sparkline:C1Sparkline>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Sparkline.SeriesAndBind_Text0 %></p>
	<p><%= Resources.C1Sparkline.SeriesAndBind_Text1 %></p>
</asp:Content>
