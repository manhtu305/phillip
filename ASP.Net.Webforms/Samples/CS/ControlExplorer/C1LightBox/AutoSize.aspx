<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="AutoSize.aspx.cs" Inherits="ControlExplorer.C1LightBox.AutoSize" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <wijmo:C1LightBox ID="C1LightBox1" runat="server" Player="Img" TextPosition="TitleOverlay" AutoSize="true" KeyNav="true">
        <Items>
            <wijmo:C1LightBoxItem ID="LightBoxItem1" Title="Sport1" Text="Sport1"
                ImageUrl="~/Images/Sports/1_small.jpg"
                LinkUrl="~/Images/Sports/1.jpg" />
            <wijmo:C1LightBoxItem ID="LightBoxItem2" Title="Sport2" Text="Sport2"
                ImageUrl="~/Images/Sports/2_small.jpg"
                LinkUrl="~/Images/Sports/2.jpg" />
            <wijmo:C1LightBoxItem ID="C1LightBoxItem3" Title="Sport3" Text="Sport3"
                ImageUrl="~/Images/Sports/3_small.jpg"
                LinkUrl="~/Images/Sports/3.jpg" />
            <wijmo:C1LightBoxItem ID="C1LightBoxItem4" Title="Sport4" Text="Sport4"
                ImageUrl="~/Images/Sports/4_small.jpg"
                LinkUrl="~/Images/Sports/4.jpg" />
        </Items>
    </wijmo:C1LightBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

    <p><%= Resources.C1LightBox.AutoSize_Text0 %></p>

    <p><%= Resources.C1LightBox.AutoSize_Text1 %></p>

    <p><%= Resources.C1LightBox.AutoSize_Text2 %></p>
    <ul>
        <li><%= Resources.C1LightBox.AutoSize_Li1 %></li>
        <li><%= Resources.C1LightBox.AutoSize_Li2 %></li>
        <li><%= Resources.C1LightBox.AutoSize_Li3 %></li>
        <li><%= Resources.C1LightBox.AutoSize_Li4 %></li>
    </ul>
    <p><%= Resources.C1LightBox.AutoSize_Text3 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <label><%= Resources.C1LightBox.AutoSize_Animation %></label>
                    <asp:DropDownList ID="cbxAnimation" AutoPostBack="true" runat="server" OnSelectedIndexChanged="cbxAnimation_SelectedIndexChanged">
                        <asp:ListItem Value="Sync" Text="<%$ Resources:C1LightBox, AutoSize_AnimationSync %>" Selected="true"></asp:ListItem>
                        <asp:ListItem Value="Wh" Text="<%$ Resources:C1LightBox, AutoSize_AnimationWh %>"></asp:ListItem>
                        <asp:ListItem Value="Hw" Text="<%$ Resources:C1LightBox, AutoSize_AnimationHw %>"></asp:ListItem>
                        <asp:ListItem Value="None" Text="<%$ Resources:C1LightBox, AutoSize_AnimationNone %>"></asp:ListItem>
                    </asp:DropDownList>
                </li>
            </ul>
        </div>
    </div>
</asp:Content>
