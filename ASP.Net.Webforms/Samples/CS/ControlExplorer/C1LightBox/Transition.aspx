<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Transition.aspx.cs" Inherits="ControlExplorer.C1LightBox.Transition" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <wijmo:C1LightBox ID="C1LightBox1" runat="server" Player="Img" TextPosition="TitleOverlay" SlideDirection="Horizontal">
        <TransAnimation Animated="Fade" Duration="1000" />
        <Items>
            <wijmo:C1LightBoxItem ID="LightBoxItem1" Title="Abstract1" Text="Abstract1"
                ImageUrl="~/Images/Sports/1_small.jpg"
                LinkUrl="~/Images/Sports/1.jpg" />
            <wijmo:C1LightBoxItem ID="LightBoxItem2" Title="Abstract2" Text="Abstract2"
                ImageUrl="~/Images/Sports/2_small.jpg"
                LinkUrl="~/Images/Sports/2.jpg" />
            <wijmo:C1LightBoxItem ID="C1LightBoxItem3" Title="Abstract3" Text="Abstract3"
                ImageUrl="~/Images/Sports/3_small.jpg"
                LinkUrl="~/Images/Sports/3.jpg" />
            <wijmo:C1LightBoxItem ID="C1LightBoxItem4" Title="Abstract4" Text="Abstract4"
                ImageUrl="~/Images/Sports/4_small.jpg"
                LinkUrl="~/Images/Sports/4.jpg" />
        </Items>
    </wijmo:C1LightBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

    <p><%= Resources.C1LightBox.Transition_Text0 %></p>

    <p><%= Resources.C1LightBox.Transition_Text1 %></p>

    <ul>
        <li><%= Resources.C1LightBox.Transition_Li1 %></li>
        <li><%= Resources.C1LightBox.Transition_Li2 %></li>
        <li><%= Resources.C1LightBox.Transition_Li3 %></li>
    </ul>


    <p><%= Resources.C1LightBox.Transition_Text2 %></p>


    <p><%= Resources.C1LightBox.Transition_Text3 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <label><%= Resources.C1LightBox.Transition_Animation %></label>
                    <asp:DropDownList ID="cbxAnimation" AutoPostBack="true" runat="server" OnSelectedIndexChanged="cbxAnimation_SelectedIndexChanged">
                        <asp:ListItem Value="Fade" Selected="true">Fade</asp:ListItem>
                        <asp:ListItem Value="Slide">Slide</asp:ListItem>
                        <asp:ListItem Value="None">None</asp:ListItem>
                    </asp:DropDownList>
                </li>
                <li>
                    <label><%= Resources.C1LightBox.Transition_SlideDirection %></label>
                    <asp:DropDownList ID="cbxSlideDirecion" AutoPostBack="true" runat="server" OnSelectedIndexChanged="cbxSlideDirecion_SelectedIndexChanged">
                        <asp:ListItem Value="Horizontal" Selected="true">Horizontal</asp:ListItem>
                        <asp:ListItem Value="Vertical">Vertical</asp:ListItem>
                    </asp:DropDownList>
                </li>
            </ul>
        </div>
    </div>
</asp:Content>
