<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="QuickTime.aspx.cs" Inherits="ControlExplorer.C1LightBox.QuickTime" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox"
    TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" Player="Qt" TextPosition="Outside" ControlsPosition="Outside" ShowCounter="false">
	<Items>
		<wijmo:C1LightBoxItem ID="LightBoxItem1" Title="<%$ Resources:C1LightBox, QuickTime_Title %>" Text="<%$ Resources:C1LightBox, QuickTime_Text %>" 
			ImageUrl="~/C1LightBox/images/small/quicktime.png" 
			LinkUrl="https://trailers.apple.com/movies/universal/despicableme/despicableme-tlr1_r640s.mov?width=640&height=360" />
	</Items>
</wijmo:C1LightBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">

<p><%= Resources.C1LightBox.QuickTime_Text0 %></p>

<p><%= Resources.C1LightBox.QuickTime_Text1 %></p>

<p><%= Resources.C1LightBox.QuickTime_Text2 %></p>

</asp:Content>
