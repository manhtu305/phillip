<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="TextPosition.aspx.cs" Inherits="ControlExplorer.C1LightBox.TextPosition" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <wijmo:C1LightBox ID="C1LightBox1" runat="server" Player="Img" TextPosition="Inside">
        <Items>
            <wijmo:C1LightBoxItem ID="LightBoxItem1" Title="Abstract1" Text="Abstract1"
                ImageUrl="~/Images/Sports/1_small.jpg"
                LinkUrl="~/Images/Sports/1.jpg" />
            <wijmo:C1LightBoxItem ID="LightBoxItem2" Title="Abstract2" Text="Abstract2"
                ImageUrl="~/Images/Sports/2_small.jpg"
                LinkUrl="~/Images/Sports/2.jpg" />
            <wijmo:C1LightBoxItem ID="C1LightBoxItem3" Title="Abstract3" Text="Abstract3"
                ImageUrl="~/Images/Sports/3_small.jpg"
                LinkUrl="~/Images/Sports/3.jpg" />
            <wijmo:C1LightBoxItem ID="C1LightBoxItem4" Title="Abstract4" Text="Abstract4"
                ImageUrl="~/Images/Sports/4_small.jpg"
                LinkUrl="~/Images/Sports/4.jpg" />
        </Items>
    </wijmo:C1LightBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

    <p><%= Resources.C1LightBox.TextPosition_Text0 %></p>

    <p><%= Resources.C1LightBox.TextPosition_Text1 %></p>

    <ul>
        <li><%= Resources.C1LightBox.TextPosition_Li1 %></li>
        <li><%= Resources.C1LightBox.TextPosition_Li2 %></li>
        <li><%= Resources.C1LightBox.TextPosition_Li5 %></li>
        <li><%= Resources.C1LightBox.TextPosition_Li3 %></li>
        <li><%= Resources.C1LightBox.TextPosition_Li4 %></li>
    </ul>

    <p>
    </p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li>
                    <label><%= Resources.C1LightBox.TextPosition_TextPosition %></label>
                    <asp:DropDownList ID="cbxTextPosition" AutoPostBack="true" runat="server" OnSelectedIndexChanged="cbxTextPosition_SelectedIndexChanged">
                        <asp:ListItem Value="Inside" Selected="true">Inside</asp:ListItem>
                        <asp:ListItem Value="Outside">Outside</asp:ListItem>
                        <asp:ListItem Value="Overlay">Overlay</asp:ListItem>
                        <asp:ListItem Value="TitleOverlay">TitleOverlay</asp:ListItem>
                        <asp:ListItem Value="None">None</asp:ListItem>
                    </asp:DropDownList>
                </li>
            </ul>
        </div>
    </div>

</asp:Content>
