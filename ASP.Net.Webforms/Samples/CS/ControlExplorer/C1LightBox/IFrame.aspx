<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="IFrame.aspx.cs" Inherits="ControlExplorer.C1LightBox.IFrame" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" Player="Iframe" MaxWidth="960" MaxHeight="600">
	<Items>
		<wijmo:C1LightBoxItem ID="LightBoxItem2" Title="<%$ Resources:C1LightBox, IFrame_Title1 %>" Text="<%$ Resources:C1LightBox, IFrame_Text01 %>"
			ImageUrl="~/C1LightBox/images/small/componentone.png" 
			LinkUrl="https://www.componentone.com" />
		<wijmo:C1LightBoxItem ID="C1LightBoxItem3" Title="<%$ Resources:C1LightBox, IFrame_Title2 %>" Text="<%$ Resources:C1LightBox, IFrame_Text02 %>"
			ImageUrl="~/C1LightBox/images/small/microsoft.png" 
			LinkUrl="https://www.microsoft.com" />
	</Items>
</wijmo:C1LightBox>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p><%= Resources.C1LightBox.IFrame_Text0 %></p>

<p><%= Resources.C1LightBox.IFrame_Text1 %></p>

<p><%= Resources.C1LightBox.IFrame_Text2 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
