<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="WMP.aspx.cs" Inherits="ControlExplorer.C1LightBox.WMP" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" Player="Wmp" MaxWidth="600" MaxHeight="450"  TextPosition="Outside" ControlsPosition="Outside" >
	<Items>
		<wijmo:C1LightBoxItem ID="LightBoxItem1" Title="<%$ Resources:C1LightBox, WMP_Title %>" Text="<%$ Resources:C1LightBox, WMP_Text %>" 
			ImageUrl="~/C1LightBox/images/small/mediaplayer.png" 
			LinkUrl="~/C1LightBox/movies/sample.mpeg" />
	</Items>
</wijmo:C1LightBox>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p><%= Resources.C1LightBox.WMP_Text0 %></p>

<p><%= Resources.C1LightBox.WMP_Text1 %></p>

<p><%= Resources.C1LightBox.WMP_Text2 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
