<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Flash.aspx.cs" Inherits="ControlExplorer.C1LightBox.Flash" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" TextPosition="Outside" ControlsPosition="Outside" ShowCounter="false">
    <Items>
        <wijmo:C1LightBoxItem ID="LightBoxItem1" 
            ImageUrl="~/C1LightBox/images/small/happygirl.png" 
            LinkUrl="~/C1LightBox/movies/skip.swf" Title="Happy Girl" 
            Text="Happy Girl"/>
        <wijmo:C1LightBoxItem ID="LightBoxItem2" 
            ImageUrl="~/C1LightBox/images/small/caveman.png" 
            LinkUrl="~/C1LightBox/movies/caveman.swf" Text="Cave Man" 
            Title="Cave Man" />
        <wijmo:C1LightBoxItem ID="LightBoxItem3" 
            ImageUrl="~/C1LightBox/images/small/oldman.png" 
            LinkUrl="~/C1LightBox/movies/old_man.swf" Text="Old Man" Title="Old Man" />
    </Items>
</wijmo:C1LightBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p><%= Resources.C1LightBox.Flash_Text0 %></p>

<p><%= Resources.C1LightBox.Flash_Text1 %></p>

<p><%= Resources.C1LightBox.Flash_Text2 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
