<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="YouTube.aspx.cs" Inherits="ControlExplorer.C1LightBox.YouTube" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" Player="Swf" TextPosition="Outside" ControlsPosition="Outside" >
	<Items>
		<wijmo:C1LightBoxItem ID="LightBoxItem1" Title="Keyboard Cat"
			ImageUrl="~/C1LightBox/images/small/keyboardcat.png" 
			LinkUrl="https://www.youtube.com/embed/J---aiyznGQ" />
		<wijmo:C1LightBoxItem ID="LightBoxItem2" Title="Sneezing Panda"
			ImageUrl="~/C1LightBox/images/small/panda.png" 
			LinkUrl="https://www.youtube.com/embed/FzRH3iTQPrk" />
		<wijmo:C1LightBoxItem ID="C1LightBoxItem3" Title="Talking Cats"
			ImageUrl="~/C1LightBox/images/small/talkingcats.png" 
			LinkUrl="https://www.youtube.com/embed/z3U0udLH974" />
	</Items>
</wijmo:C1LightBox>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p><%= Resources.C1LightBox.YouTube_Text0 %></p>

<p><%= Resources.C1LightBox.YouTube_Text1 %></p>

<p>
</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
