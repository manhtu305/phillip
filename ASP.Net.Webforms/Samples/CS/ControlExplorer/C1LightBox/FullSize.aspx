<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="FullSize.aspx.cs" Inherits="ControlExplorer.C1LightBox.FullSize" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" Player="Img" DialogButtons="Close, FullSize"  
		TextPosition="TitleOverlay" Modal="true">
	<Items>
		<wijmo:C1LightBoxItem ID="LightBoxItem1" Title="Sport1" Text="Sport1" 
			ImageUrl="~/Images/Sports/1_small.jpg" 
			LinkUrl="~/Images/Sports/1.jpg" />
	</Items>
</wijmo:C1LightBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p><%= Resources.C1LightBox.FullSize_Text0 %></p>

<p><%= Resources.C1LightBox.FullSize_Text1 %></p>
<ul>
<li><%= Resources.C1LightBox.FullSize_Li1 %></li>
<li><%= Resources.C1LightBox.FullSize_Li2 %></li>
<li><%= Resources.C1LightBox.FullSize_Li3 %></li>
</ul>

<p><%= Resources.C1LightBox.FullSize_Text2 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
