<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="SeqCounter.aspx.cs" Inherits="ControlExplorer.C1LightBox.SeqCounter" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" Player="Img" TextPosition="TitleOverlay" CounterType="Sequence">
    <Items>
        <wijmo:C1LightBoxItem ID="LightBoxItem1" Title="Sport1" Text="Sport1"
            ImageUrl="~/Images/Sports/1_small.jpg" 
            LinkUrl="~/Images/Sports/1.jpg" />
        <wijmo:C1LightBoxItem ID="LightBoxItem2" Title="Sport2" Text="Sport2"
            ImageUrl="~/Images/Sports/2_small.jpg" 
            LinkUrl="~/Images/Sports/2.jpg" />
        <wijmo:C1LightBoxItem ID="C1LightBoxItem3" Title="Sport3" Text="Sport3"
            ImageUrl="~/Images/Sports/3_small.jpg" 
            LinkUrl="~/Images/Sports/3.jpg" />
        <wijmo:C1LightBoxItem ID="C1LightBoxItem4" Title="Sport4" Text="Sport4"
            ImageUrl="~/Images/Sports/4_small.jpg" 
            LinkUrl="~/Images/Sports/4.jpg" />
    </Items>
</wijmo:C1LightBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p><%= Resources.C1LightBox.SeqCounter_Text0 %></p>

<p><%= Resources.C1LightBox.SeqCounter_Text1 %></p>

<p><%= Resources.C1LightBox.SeqCounter_Text2 %></p>

<p><%= Resources.C1LightBox.SeqCounter_Text3 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
