<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="FlashVideo.aspx.cs" Inherits="ControlExplorer.C1LightBox.FlashVideo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<wijmo:C1LightBox ID="C1LightBox1" runat="server" TextPosition="Outside" 
        ControlsPosition="Outside" ShowCounter="false" 
        FlashInstall="player/expressInstall.swf" FlvPlayer="player/player.swf">
    <Items>
        <wijmo:C1LightBoxItem ID="LightBoxItem1" 
            ImageUrl="<%$ Resources:C1LightBox, FlashVideo_ImageUrl %>" 
            LinkUrl="<%$ Resources:C1LightBox, FlashVideo_LinkUrl %>" Title="<%$ Resources:C1LightBox, FlashVideo_Title %>" 
            Text="<%$ Resources:C1LightBox, FlashVideo_Text %>"/>
    </Items>
</wijmo:C1LightBox>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p><%= Resources.C1LightBox.FlashVideo_Text0 %></p>

<p><%= Resources.C1LightBox.FlashVideo_Text1 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
