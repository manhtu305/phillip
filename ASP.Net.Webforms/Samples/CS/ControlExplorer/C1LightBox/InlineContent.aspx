<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="InlineContent.aspx.cs" Inherits="ControlExplorer.C1LightBox.InlineContent" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


<wijmo:C1LightBox ID="C1LightBox1" runat="server" TextPosition="Outside" ControlsPosition="Outside" Player="Inline">
    <Items>
        <wijmo:C1LightBoxItem ID="LightBoxItem1" Title="<%$ Resources:C1LightBox, InlineContent_Title1 %>" Text="<%$ Resources:C1LightBox, InlineContent_Text01 %>"
            ImageUrl="~/Images/Sports/1_small.jpg" 
            LinkUrl="#content-1" />
        <wijmo:C1LightBoxItem ID="LightBoxItem2" Title="<%$ Resources:C1LightBox, InlineContent_Title2 %>" Text="<%$ Resources:C1LightBox, InlineContent_Text02 %>"
            ImageUrl="~/Images/Sports/2_small.jpg" 
            LinkUrl="#content-2" />
        <wijmo:C1LightBoxItem ID="C1LightBoxItem3" Title="<%$ Resources:C1LightBox, InlineContent_Title3 %>" Text="<%$ Resources:C1LightBox, InlineContent_Text03 %>"
            ImageUrl="~/Images/Sports/3_small.jpg" 
            LinkUrl="#content-3" />
    </Items>
</wijmo:C1LightBox>

<div id="content-1" style="display:none;">
    <p><%= Resources.C1LightBox.InlineContent_Text0 %></p>
    <p><%= Resources.C1LightBox.InlineContent_Text1 %></p>
</div>
 
 
<div id="content-2" style="display:none;">
    <p><%= Resources.C1LightBox.InlineContent_Text2 %></p>
</div>
 
<div id="content-3" style="display:none;">
    <p><%= Resources.C1LightBox.InlineContent_Text3 %></p>
    <p><%= Resources.C1LightBox.InlineContent_Text4 %></p>
</div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p><%= Resources.C1LightBox.InlineContent_Text5 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
