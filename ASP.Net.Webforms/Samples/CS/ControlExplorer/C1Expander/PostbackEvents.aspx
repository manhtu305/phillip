<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="PostbackEvents.aspx.cs" Inherits="ControlExplorer.C1Expander.PostbackEvents" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Expander"
	TagPrefix="C1Expander" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server">
	</asp:ScriptManager>
	<h2>
		<%= Resources.C1Expander.PostbackEvents_Title1 %></h2>
	<asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" DisplayAfter="100" runat="server">
		<ProgressTemplate><div style="margin-top:20px;">Refreshing UpdatePanel1...</div></ProgressTemplate>
	</asp:UpdateProgress>
	<asp:UpdatePanel ID="UpdatePanel1" runat="server">
		<ContentTemplate>
			<h3>
				<asp:Label ID="Label1" runat="server" Text=""></asp:Label></h3>
			<C1Expander:C1Expander runat="server" ID="C1Expander1" OnExpandedChanged="C1Expander1_OnExpandedChanged">
				<Header>
					<%= Resources.C1Expander.PostbackEvents_Header1 %>
				</Header>
				<Content>
					<%= Resources.C1Expander.PostbackEvents_Content1 %>
				</Content>
			</C1Expander:C1Expander>
		</ContentTemplate>
	</asp:UpdatePanel>

	<h2>
		<%= Resources.C1Expander.PostbackEvents_Title2 %></h2>
	<h3>
		<asp:Label ID="Label2" runat="server" Text=""></asp:Label></h3>
	<C1Expander:C1Expander runat="server" ID="C1Expander2" OnExpandedChanged="C1Expander2_OnExpandedChanged">
		<Header>
			<%= Resources.C1Expander.PostbackEvents_Header2 %>
		</Header>
		<Content>
			<%= Resources.C1Expander.PostbackEvents_Content2 %>
		</Content>
	</C1Expander:C1Expander>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

	<p><%= Resources.C1Expander.PostbackEvents_Text0 %></p>
	<p><%= Resources.C1Expander.PostbackEvents_Text1 %></p>
	<p><%= Resources.C1Expander.PostbackEvents_Text2 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
