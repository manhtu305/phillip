<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ContentUrl.aspx.cs" Inherits="ControlExplorer.C1Expander.ContentUrl" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Expander"
	TagPrefix="C1Expander" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<style type="text/css" media="all">
		.ui-expander-content 
		{
			height: 400px;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<C1Expander:C1Expander runat="server" ID="C1Expander3" ContentUrl="<%$ Resources:C1Expander, ContentUrl_ContentUrl %>">
		<Header>
			<%= Resources.C1Expander.ContentUrl_Header1 %>
		</Header>
		<Content>
			
		</Content>
	</C1Expander:C1Expander>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Expander.ContentUrl_Text0 %></p>
	<p><%= Resources.C1Expander.ContentUrl_Text1 %></p>
	<p><%= Resources.C1Expander.ContentUrl_Text2 %></p>
	<p><%= Resources.C1Expander.ContentUrl_Text3 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
