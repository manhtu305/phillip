<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
	Inherits="Expander_Overview" CodeBehind="Overview.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Expander"
	TagPrefix="C1Expander" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<C1Expander:C1Expander runat="server" ID="Expander1">
		<Header>
			<%= Resources.C1Expander.Overview_Header1 %>
		</Header>
		<Content>
			<%= Resources.C1Expander.Overview_Content1 %>
		</Content>
	</C1Expander:C1Expander>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Expander.Overview_Text0 %></p>
	<p><%= Resources.C1Expander.Overview_Text1 %></p>

</asp:Content>
