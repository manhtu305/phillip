<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Animation.aspx.cs" Inherits="ControlExplorer.C1Expander.Animation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Expander"
	TagPrefix="C1Expander" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<script id="scriptInit" type="text/javascript">
		$(document).ready(function () {
			jQuery.wijmo.wijexpander.animations.custom1 = function (options) {
				this.slide(options, {
					easing: "easeOutExpo",
					duration: 900
				});
			}
			jQuery.wijmo.wijexpander.animations.custom2 = function (options) {
				if (options.expand)
					options.content.show("puff", options, 300);
				else
					options.content.hide("explode", options, 300);
			}
		});
	</script>
	<h4>
		<%= Resources.C1Expander.Animation_Title1 %></h4>

	<C1Expander:C1Expander runat="server" ID="C1Expander1">
		<Header>
			<%= Resources.C1Expander.Animation_Header1 %>
		</Header>
		<Content>
			<%= Resources.C1Expander.Animation_Content1 %>
		</Content>
	</C1Expander:C1Expander>
	<br />
	<h4>
		<%= Resources.C1Expander.Animation_Title2 %></h4>
	<C1Expander:C1Expander runat="server" ID="C1Expander2" Animated-Effect="custom1" Expanded="false">
		<Header>
			<%= Resources.C1Expander.Animation_Header2 %>
		</Header>
		<Content>
			<%= Resources.C1Expander.Animation_Content2 %>
		</Content>
	</C1Expander:C1Expander>

	<br />
	<h4>
		<%= Resources.C1Expander.Animation_Title3 %></h4>
	<C1Expander:C1Expander runat="server" ID="C1Expander3" Animated-Effect="custom2" Expanded="false">
		<Header>
			<%= Resources.C1Expander.Animation_Header3 %>
		</Header>
		<Content>
			<%= Resources.C1Expander.Animation_Content3 %>
		</Content>
	</C1Expander:C1Expander>
	
	<br />
	<h4>
		<%= Resources.C1Expander.Animation_Title4 %></h4>
	<C1Expander:C1Expander runat="server" ID="C1Expander4" Animated-Disabled="true" Expanded="false">
		<Header>
			<%= Resources.C1Expander.Animation_Header4 %>
		</Header>
		<Content>
			<%= Resources.C1Expander.Animation_Content4 %>
		</Content>
	</C1Expander:C1Expander>

	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Expander.Animation_Text0 %></p>
    <p><%= Resources.C1Expander.Animation_Text1 %></p>
    <ul>
        <li><%= Resources.C1Expander.Animation_Li1 %></li>
        <li><%= Resources.C1Expander.Animation_Li2 %></li>
        <li><%= Resources.C1Expander.Animation_Li3 %></li>
        <li><%= Resources.C1Expander.Animation_Li4 %></li>
    </ul>
	<p><%= Resources.C1Expander.Animation_Text2 %></p>
<%= Resources.C1Expander.Animation_Code1 %>
	<p><%= Resources.C1Expander.Animation_Text3 %></p>
		<ul>
			<li><%= Resources.C1Expander.Animation_Li5 %></li>
			<li><%= Resources.C1Expander.Animation_Li6 %></li>
			<li><%= Resources.C1Expander.Animation_Li7 %></li>
		</ul>
	
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
