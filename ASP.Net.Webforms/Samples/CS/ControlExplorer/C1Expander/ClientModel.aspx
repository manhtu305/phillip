<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="ClientModel.aspx.cs" Inherits="ControlExplorer.C1Expander.ClientModel" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Expander"
	TagPrefix="C1Expander" %>
<%@ Register Src="../ClientLogger.ascx" TagName="ClientLogger" TagPrefix="ClientLogger" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<ClientLogger:ClientLogger ID="ClientLogger1" runat="server" />
	<C1Expander:C1Expander runat="server" ID="C1Expander1">
		<Header>
			<%= Resources.C1Expander.ClientModel_Header1 %>
		</Header>
		<Content>
			<%= Resources.C1Expander.ClientModel_Content1 %>
		</Content>
	</C1Expander:C1Expander>
	<script type="text/javascript">
		$(document).ready(
		function () {
			$("#expandedOption")[0].checked = $("#<%=C1Expander1.ClientID%>").c1expander("option", "expanded");
			$("#allowExpandOption")[0].checked = $("#<%=C1Expander1.ClientID%>").c1expander("option", "allowExpand");
			$("#expandDirectionOption").val($("#<%=C1Expander1.ClientID%>").c1expander("option", "expandDirection"));
			$("#expandedOption").change(function () {
				var expanded = $("#expandedOption")[0].checked;
				$("#<%=C1Expander1.ClientID%>").c1expander("option", "expanded", expanded);
				log.message("<%= Resources.C1Expander.ClientModel_ExpandedMessage %>" + expanded);
			})

			$("#allowExpandOption").change(function () {
				var allowExpand = $("#allowExpandOption")[0].checked;
				$("#<%=C1Expander1.ClientID%>").c1expander("option", "allowExpand", allowExpand);
				log.message("<%= Resources.C1Expander.ClientModel_AllowExpandMessage %>" + allowExpand);
			});

			$("#expandDirectionOption").change(function () {
				var selectedDir = $("#expandDirectionOption").val();
				$("#<%=C1Expander1.ClientID%>").c1expander("option", "expandDirection", selectedDir);
				log.message("<%= Resources.C1Expander.ClientModel_ExpandDirectionMessage %>" + selectedDir);
			})

			$("#bindExpandEvents").change(function () {
				if ($("#bindExpandEvents")[0].checked) {
					$("#<%=C1Expander1.ClientID%>").bind("c1expanderbeforeexpand.sampleExpandEvents", function (e) {
						log.message("<%= Resources.C1Expander.ClientModel_BeforeExpandMessage %>");
						/*if (confirm("Do you want to cancel beforeExpand event?")) {
						e.preventDefault();
						}*/
					}).bind("c1expanderafterexpand.sampleExpandEvents", function () {
						log.message("<%= Resources.C1Expander.ClientModel_AfterExpandMessage %>");
					});
				} else {
					$("#<%=C1Expander1.ClientID%>").unbind(".sampleExpandEvents");
				}
			});

			$("#bindCollapseEvents").change(function () {
				if ($("#bindCollapseEvents")[0].checked) {
					$("#<%=C1Expander1.ClientID%>").bind("c1expanderbeforecollapse.sampleCollapseEvents", function (e) {
						log.message("<%= Resources.C1Expander.ClientModel_BeforeCollapseMessage %>");
						/*if (confirm("Do you want to cancel beforeCollapse event?")) {
						e.preventDefault();
						}*/
					}).bind("c1expanderaftercollapse.sampleCollapseEvents", function () {
						log.message("<%= Resources.C1Expander.ClientModel_AfterCollapseMessage %>");
					});
				} else {
					$("#<%=C1Expander1.ClientID%>").unbind(".sampleCollapseEvents");
				}
			});
		}
		);
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Expander.ClientModel_Text0 %></p>
	<p><%= Resources.C1Expander.ClientModel_Text1 %></p>
	<ul>
		<li><%= Resources.C1Expander.ClientModel_Li1 %></li>
		<li><%= Resources.C1Expander.ClientModel_Li2 %></li>
		<li><%= Resources.C1Expander.ClientModel_Li3 %></li>
	</ul>
	<p><%= Resources.C1Expander.ClientModel_Text2 %></p>
	<ul>
		<li><%= Resources.C1Expander.ClientModel_Li4 %></li>
		<li><%= Resources.C1Expander.ClientModel_Li5 %></li>
		<li><%= Resources.C1Expander.ClientModel_Li6 %></li>
		<li><%= Resources.C1Expander.ClientModel_Li7 %></li>
	</ul>
	
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<div class="settingcontainer">
		<div class="settingcontent">
			<ul>
				<li class="fullwidth">
					<input type="checkbox" id="expandedOption" />
					<label>
						<%= Resources.C1Expander.ClientModel_ExpandedLabel %></label>
				</li>
				<li class="fullwidth">
					<label>
						<%= Resources.C1Expander.ClientModel_ExpandDirectionLabel %></label>
					<select id="expandDirectionOption">
						<option value="top" selected="selected"><%= Resources.C1Expander.ClientModel_DirectionTop %></option>
						<option value="right"><%= Resources.C1Expander.ClientModel_DirectionRight %></option>
						<option value="bottom"><%= Resources.C1Expander.ClientModel_DirectionBottom %></option>
						<option value="left"><%= Resources.C1Expander.ClientModel_DirectionLeft %></option>
					</select>
				</li>
				<li class="fullwidth">
					<input type="checkbox" id="allowExpandOption" />
					<label>
						<%= Resources.C1Expander.ClientModel_AllowExpandLabel %></label>
				</li>
				<li class="fullwidth">
					<input type="checkbox" id="bindCollapseEvents" />
					<label>
						<%= Resources.C1Expander.ClientModel_CollapseLabel %></label>
				</li>
				<li class="fullwidth">
					<input type="checkbox" id="bindExpandEvents" />
					<label>
						<%= Resources.C1Expander.ClientModel_ExpandLabel %></label>
				</li>
			</ul>
		</div>
	</div>
	
</asp:Content>
