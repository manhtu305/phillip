﻿using System;

namespace ControlExplorer.C1QRCode
{
	using C1.Web.Wijmo.Controls.C1QRCode;

	public partial class UseQRCodeImageUrl : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				C1QRCode qrCode = new C1QRCode();
				qrCode.Text = "1234567890";

				this.Image1.ImageUrl = qrCode.ImageUrl;
			}
		}
	}
}