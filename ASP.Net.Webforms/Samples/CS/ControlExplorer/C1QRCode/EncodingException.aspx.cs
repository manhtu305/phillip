﻿using System;
using C1.Web.Wijmo.Controls.C1QRCode;

namespace ControlExplorer.C1QRCode
{
	public partial class EncodingException : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				UpdateQRCode();
			}
		}

		private void UpdateQRCode()
		{
			C1QRCode1.Encoding = (Encoding) Enum.Parse(typeof (Encoding), DdlEncoding.SelectedItem.Text);
			C1QRCode1.Text = TxtText.Text;
			var exc = C1QRCode1.EncodingException;

			ServerSideLogger.Content = exc == null ? "" : exc.Message;
		}

		protected void ApplyBtn_Click(object sender, EventArgs e)
		{
			UpdateQRCode();
			UpdatePanel1.Update();
		}
	}
}