﻿using System;
using System.Web.UI.WebControls;

namespace ControlExplorer.C1QRCode
{
    public partial class AutoSize : System.Web.UI.Page
    {
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				UpdateSettings();
			}
		}

		private void UpdateSettings()
		{
			C1QRCode1.Width = string.IsNullOrWhiteSpace(TxtWidth.Text) ? Unit.Empty : int.Parse(TxtWidth.Text);
			C1QRCode1.Height = string.IsNullOrWhiteSpace(TxtHeight.Text) ? Unit.Empty : int.Parse(TxtHeight.Text);
			C1QRCode1.AutoSize = CkbAutoSize.Checked;
			C1QRCode1.Text = TxtText.Text;

			TxtWidth.Text = C1QRCode1.Width == Unit.Empty ? string.Empty : C1QRCode1.Width.Value.ToString();
			TxtHeight.Text = C1QRCode1.Height == Unit.Empty ? string.Empty : C1QRCode1.Height.Value.ToString();
		}

		protected void ApplyBtn_Click(object sender, EventArgs e)
		{
			UpdateSettings();
			UpdatePanel1.Update();
		}
    }
}