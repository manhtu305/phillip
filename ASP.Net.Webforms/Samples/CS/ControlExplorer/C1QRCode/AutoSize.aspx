<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="AutoSize.aspx.cs" Inherits="ControlExplorer.C1QRCode.AutoSize" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1QRCode"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content2" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
	<br />
<wijmo:C1QRCode ID="C1QRCode1" runat="server"/>
	<br />
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
	<p><%= Resources.C1QRCode.AutoSize_Text0 %></p>
	<p><%= Resources.C1QRCode.AutoSize_Text1 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
	<asp:UpdatePanel ID="UpdatePanel2" runat="server">
			<ContentTemplate>
<div class="settingcontainer">
<div class="settingcontent">
	<ul>
		<li>
			<label><%= Resources.C1QRCode.AutoSize_Width %></label>
			<asp:TextBox ID="TxtWidth" runat="server" Text="200"></asp:TextBox>
        </li>
		<li>
			<label><%= Resources.C1QRCode.AutoSize_Height %></label>
			<asp:TextBox ID="TxtHeight" runat="server" Text="200"></asp:TextBox>
		</li>
		<li><asp:CheckBox id="CkbAutoSize" runat="server" Checked="false" Text="<%$ Resources:C1QRCode, AutoSize_AutoSize %>"/></li>
		<li class="fullwidth autoheight">
			<label><%= Resources.C1QRCode.AutoSize_Text %></label>
		</li>
		<li class="fullwidth"><asp:TextBox ID="TxtText" TextMode="MultiLine" runat="server" Text="C1QRCode"></asp:TextBox>
        </li>
        <li class="fullwidth">
			<asp:CompareValidator ForeColor="Red" ID="CvWidth" runat="server" ControlToValidate="TxtWidth" ErrorMessage="Width should be a numeric value." Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
	        <br/>
			<asp:CompareValidator ForeColor="Red" ID="CvHeight" runat="server" ControlToValidate="TxtHeight" ErrorMessage="Height should be a numeric value." Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
        </li>
	</ul></div>
	<br/>
	<div class="settingcontrol">
	<asp:Button ID="ApplyBtn" Text="<%$ Resources:C1QRCode, AutoSize_Apply %>" CssClass="settingapply" runat="server" OnClick="ApplyBtn_Click"/>
	</div>
</div>
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>