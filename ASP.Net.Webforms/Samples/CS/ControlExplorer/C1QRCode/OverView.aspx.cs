﻿using System;
using C1.Web.Wijmo.Controls.C1QRCode;

namespace ControlExplorer.C1QRCode
{
    public partial class OverView : System.Web.UI.Page
    {
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				UpdateSettings();
			}
		}

		private void UpdateSettings()
		{
			if (string.IsNullOrWhiteSpace(TxtCodeVersion.Text))
			{
				TxtCodeVersion.Text = C1QRCode1.CodeVersion.ToString();
			}
			else
			{
				C1QRCode1.CodeVersion = int.Parse(TxtCodeVersion.Text);
			}

			if (string.IsNullOrWhiteSpace(TxtSymbolSize.Text))
			{
				TxtSymbolSize.Text = C1QRCode1.SymbolSize.ToString();
			}
			else
			{
				C1QRCode1.SymbolSize = int.Parse(TxtSymbolSize.Text);
			}

			C1QRCode1.ErrorCorrectionLevel = (ErrorCorrectionLevel)Enum.Parse(typeof(ErrorCorrectionLevel), DdlErrorCorrectionLevel.SelectedItem.Text);
			C1QRCode1.Encoding = (Encoding)Enum.Parse(typeof(Encoding), DdlEncoding.SelectedItem.Text);
			C1QRCode1.Text = TxtText.Text;
		}

		protected void ApplyBtn_Click(object sender, EventArgs e)
		{
			UpdateSettings();
			UpdatePanel1.Update();
		}
    }
}