<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="OverView.aspx.cs" Inherits="ControlExplorer.C1QRCode.OverView" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1QRCode" TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content2" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
	<br />
<wijmo:C1QRCode ID="C1QRCode1" runat="server" AutoSize="true" />
	<br />
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p><%= Resources.C1QRCode.OverView_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
	<asp:UpdatePanel ID="UpdatePanel2" runat="server">
			<ContentTemplate>
<div class="settingcontainer">
<div class="settingcontent">
	<ul>
		<li>
			<label><%= Resources.C1QRCode.OverView_CodeVersion %></label>
			<asp:TextBox ID="TxtCodeVersion" runat="server" Text="0"></asp:TextBox>
        </li>
		<li>
			<label><%= Resources.C1QRCode.OverView_SymbolSize %></label>
			<asp:TextBox ID="TxtSymbolSize" runat="server" Text="3"></asp:TextBox>
		</li>
		<li>
			<label><%= Resources.C1QRCode.OverView_ErrorCorrection %></label>
			<asp:dropdownlist id="DdlErrorCorrectionLevel" runat="server">
				<asp:ListItem Selected="True">L</asp:ListItem>
				<asp:ListItem>M</asp:ListItem>
				<asp:ListItem>Q</asp:ListItem>
				<asp:ListItem>H</asp:ListItem>
			</asp:DropDownList>
        </li>
		<li>
			<label><%= Resources.C1QRCode.OverView_Encoding %></label>
			<asp:dropdownlist id="DdlEncoding" runat="server">
				<asp:ListItem Selected="True">Automatic</asp:ListItem>
				<asp:ListItem>AlphaNumeric</asp:ListItem>
				<asp:ListItem>Numeric</asp:ListItem>
				<asp:ListItem>Byte</asp:ListItem>
			</asp:DropDownList>
        </li>
		<li class="fullwidth autoheight">
			<label><%= Resources.C1QRCode.OverView_TextTip %></label>
		</li>
		<li class="fullwidth"><asp:TextBox ID="TxtText" TextMode="MultiLine" runat="server" Text="C1QRCode"></asp:TextBox>
        </li>
        <li class="fullwidth">
        	<asp:RangeValidator ID="RvCodeVersion" runat="server" ControlToValidate="TxtCodeVersion" ErrorMessage="Invalid CodeVersion value (must be between 0 and 10)." MaximumValue="10" MinimumValue="0" Type="Integer" ForeColor="Red"></asp:RangeValidator>
	        <br/>
        	<asp:RangeValidator ID="RvSymbolSize" runat="server" ControlToValidate="TxtSymbolSize" ErrorMessage="Invalid symbol size (should be between 2 and 10)." MaximumValue="10" MinimumValue="2" Type="Integer" ForeColor="Red"></asp:RangeValidator>
        </li>
	</ul></div>
	<div class="settingcontrol">
	<asp:Button ID="ApplyBtn" Text="<%$ Resources:C1QRCode, OverView_Apply %>" CssClass="settingapply" runat="server" OnClick="ApplyBtn_Click"/>
	</div>
</div>
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
