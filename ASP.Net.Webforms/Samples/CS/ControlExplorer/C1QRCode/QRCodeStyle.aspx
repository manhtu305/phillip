<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="QRCodeStyle.aspx.cs" Inherits="ControlExplorer.C1QRCode.QRCodeStyle" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1QRCode"
    TagPrefix="wijmo" %>

<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content2" runat="server">
    <div>
        <p><%= Resources.C1QRCode.QRCodeStyle_Text0 %></p>
        <wijmo:C1QRCode ID="C1QRCode1" runat="server" Text="1234567890" BackColor="Yellow" ForeColor="Blue" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p><%= Resources.C1QRCode.QRCodeStyle_Text1 %></p>
    <p><%= Resources.C1QRCode.QRCodeStyle_Text2 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>
