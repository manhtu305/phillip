<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="UseQRCodeImageUrl.aspx.cs" Inherits="ControlExplorer.C1QRCode.UseQRCodeImageUrl" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1QRCode"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content2" runat="server">
    
    <div >
        <div> <%= Resources.C1QRCode.UseQRCodeImageUrl_ValueTip %></div>
        <br />
        <asp:Image ID="Image1" runat="server" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	    <p><%= Resources.C1QRCode.UseQRCodeImageUrl_Text0 %></p>
    <p><%= Resources.C1QRCode.UseQRCodeImageUrl_Text1 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>