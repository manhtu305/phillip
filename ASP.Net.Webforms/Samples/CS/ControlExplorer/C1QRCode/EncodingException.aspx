<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="EncodingException.aspx.cs" Inherits="ControlExplorer.C1QRCode.EncodingException" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1QRCode" TagPrefix="wijmo" %>
<%@ Register Src="~/ServerSideLogger.ascx" TagPrefix="uc1" TagName="ServerSideLogger" %>

<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content2" runat="server"><asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
        <br />
		<wijmo:C1QRCode ID="C1QRCode1" runat="server" width="100px" Height="100px" CssClass="serversidelogger-before"/>
		<uc1:ServerSideLogger runat="server" id="ServerSideLogger" Title="<%$ Resources:C1QRCode, EncodingException_LoggerTitle %>"/>
        <br />
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p><%= Resources.C1QRCode.EncodingException_Text0 %></p>
    <p><%= Resources.C1QRCode.EncodingException_Text1 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
	<asp:UpdatePanel ID="UpdatePanel2" runat="server">
			<ContentTemplate>
<div class="settingcontainer">
<div class="settingcontent">
	<ul>
		<li>
			<label><%= Resources.C1QRCode.EncodingException_Encoding %></label>
			<asp:dropdownlist id="DdlEncoding" runat="server">
				<asp:ListItem Selected="True">Automatic</asp:ListItem>
				<asp:ListItem>AlphaNumeric</asp:ListItem>
				<asp:ListItem>Numeric</asp:ListItem>
				<asp:ListItem>Byte</asp:ListItem>
			</asp:DropDownList>
        </li>
		<li class="fullwidth autoheight">
			<label><%= Resources.C1QRCode.EncodingException_TextTip %></label>
		</li>
		<li class="fullwidth"><asp:TextBox ID="TxtText" TextMode="MultiLine" runat="server" Text="C1QRCode"></asp:TextBox>
        </li>
	</ul></div>
	<div class="settingcontrol">
	<asp:Button ID="ApplyBtn" Text="<%$ Resources:C1QRCode, EncodingException_Apply %>" CssClass="settingapply" runat="server" OnClick="ApplyBtn_Click"/>
	</div>
</div>
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
