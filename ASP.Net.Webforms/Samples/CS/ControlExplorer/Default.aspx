<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/WijmoSite.Master" CodeBehind="Default.aspx.cs" Inherits="ControlExplorer.Default" %>

<asp:Content ID="headContent" runat="server" ContentPlaceHolderID="head">
    <!--jQuery References-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="HeaderButtons">
    <a runat="server"><%= Resources.Resources.Default_Home %></a>
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainContent">
    <article class="ui-helper-clearfix">
        <aside>
            <div class="padder">
                <span class="content-title">
                    <%= Resources.Resources.Default_Text1 %></span>
                <p><%= Resources.Resources.Default_Text2 %></p>
            </div>
            <div class="padder">
                <span class="content-title">
                    <%= Resources.Resources.Default_WhatSNew %></span>
                <asp:Repeater runat="server" ID="RptWhatsNew">
                    <HeaderTemplate>
                        <ul class="widget-icons ui-helper-clearfix">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li>
                            <a>
                                <img id="Img1" runat="server" src='<%# Eval("Icon") %>' alt='<%# Eval("Name") %>' />
                            </a>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("Title") %>' CssClass="description"></asp:Label>
                            <a runat="server" href='<%# Eval("link") %>'  class="description"><%# Eval("Description") %></a>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </aside>
        <div class="main-content">
            <div class="padder">
                <span class="content-title">
                    <%= Resources.Resources.Default_Favorites %> <a runat="server" href="~/Controls.aspx" class="description"><%= Resources.Resources.Default_ViewAll %></a></span>
                <asp:Repeater runat="server" ID="RptFavoriteControls">
                    <HeaderTemplate>
                        <ul class="favorite-widgets widget-icons ui-helper-clearfix">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <li><a runat="server" href='<%# Eval("Link") %>'>
                            <img runat="server" src='<%# Eval("Icon") %>' alt='<%# Eval("Title") %>' /><%# Eval("Title")%></a></li>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </article>
</asp:Content>
