<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1Wizard_NavButtons" CodeBehind="NavButtons.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Wizard" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <wijmo:C1Wizard ID="C1Wizard1" runat="server" NavButtons="Edge">
        <Steps>
            <wijmo:C1WizardStep ID="C1WizardStep1" Title="<%$ Resources:C1Wizard, NavButtons_Title1 %>" Description="<%$ Resources:C1Wizard, NavButtons_Description1 %>">
                <%= Resources.C1Wizard.NavButtons_Content1 %>
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep2" Title="<%$ Resources:C1Wizard, NavButtons_Title2 %>" Description="<%$ Resources:C1Wizard, NavButtons_Description2 %>">
                <%= Resources.C1Wizard.NavButtons_Content2 %>
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep3" Title="<%$ Resources:C1Wizard, NavButtons_Text3 %>" Description="<%$ Resources:C1Wizard, NavButtons_Description3 %>">
                <%= Resources.C1Wizard.NavButtons_Content3 %>
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep4" Title="<%$ Resources:C1Wizard, NavButtons_Title4 %>" Description="<%$ Resources:C1Wizard, NavButtons_Description4 %>">
                <%= Resources.C1Wizard.NavButtons_Content4 %>
            </wijmo:C1WizardStep>
        </Steps>
    </wijmo:C1Wizard>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p><%= Resources.C1Wizard.NavButtons_Text0 %></p>
    <p><%= Resources.C1Wizard.NavButtons_Text1 %></p>
    <p><%= Resources.C1Wizard.NavButtons_Text2 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>
