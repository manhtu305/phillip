<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1Wizard_AutoPlay" CodeBehind="AutoPlay.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Wizard" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <wijmo:C1Wizard ID="C1Wizard1" runat="server" AutoPlay="true" Loop="true" NavButtons="None">
        <Steps>
            <wijmo:C1WizardStep ID="C1WizardStep1" Title="<%$ Resources:C1Wizard, AutoPlay_Title1 %>" Description="<%$ Resources:C1Wizard, AutoPlay_Description1 %>">
                <%= Resources.C1Wizard.AutoPlay_Content1 %>
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep2" Title="<%$ Resources:C1Wizard, AutoPlay_Title2 %>" Description="<%$ Resources:C1Wizard, AutoPlay_Description2 %>">
                <%= Resources.C1Wizard.AutoPlay_Content2 %>
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep3" Title="<%$ Resources:C1Wizard, AutoPlay_Step3 %>" Description="<%$ Resources:C1Wizard, AutoPlay_Description3 %>">
                <%= Resources.C1Wizard.AutoPlay_Content3 %>
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep4" Title="<%$ Resources:C1Wizard, AutoPlay_Title4 %>" Description="<%$ Resources:C1Wizard, AutoPlay_Description4 %>">
                <%= Resources.C1Wizard.AutoPlay_Content4 %>
            </wijmo:C1WizardStep>
        </Steps>
    </wijmo:C1Wizard>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p><%= Resources.C1Wizard.AutoPlay_Text0 %></p>
    <p><%= Resources.C1Wizard.AutoPlay_Text1 %></p>
    <p><%= Resources.C1Wizard.AutoPlay_Text2 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>
