<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Pager.aspx.cs" Inherits="ControlExplorer.C1Wizard.Pager" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Wizard" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Pager" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function onClientPageIndexChanged() {
            var pageIndex = $("#<%=C1Pager1.ClientID%>").c1pager("option", "pageIndex");
                    $("#<%=C1Wizard1.ClientID%>").c1wizard({ activeIndex: pageIndex });
        };

        $(document).ready(function () {

            $(':radio').change(function (e) {
                switch ($(this).val()) {
                    case "Default":
                        $("#<%=C1Pager1.ClientID%>").c1pager('option', {
                            firstPageClass: "ui-icon-seek-first",
                            previousPageClass: "ui-icon-seek-prev",
                            nextPageClass: "ui-icon-seek-next",
                            lastPageClass: "ui-icon-seek-end",
                            firstPageText: "<%= Resources.C1Wizard.Pager_First %>",
                            previousPageText: "<%= Resources.C1Wizard.Pager_Previous %>",
                            nextPageText: "<%= Resources.C1Wizard.Pager_Next %>",
                            lastPageText: "<%= Resources.C1Wizard.Pager_Last %>"
                        });
                        break;

                    case "Text":
                        $("#<%=C1Pager1.ClientID%>").c1pager('option', {
                        firstPageClass: "",
                        previousPageClass: "",
                        nextPageClass: "",
                        lastPageClass: "",
                        firstPageText: "<%= Resources.C1Wizard.Pager_First %>",
                        previousPageText: "<%= Resources.C1Wizard.Pager_Previous %>",
                        nextPageText: "<%= Resources.C1Wizard.Pager_Next %>",
                        lastPageText: "<%= Resources.C1Wizard.Pager_Last %>"
                    });
                    break;

                case "Custom":
                    $("#<%=C1Pager1.ClientID%>").c1pager('option', {
                        firstPageClass: "",
                        previousPageClass: "",
                        nextPageClass: "",
                        lastPageClass: "",
                        firstPageText: "|<",
                        previousPageText: "<",
                        nextPageText: ">",
                        lastPageText: ">|"
                    });
                    break;
            }
            });
        });
    </script>

    <wijmo:C1Wizard ID="C1Wizard1" runat="server" NavButtons="None">
        <Steps>
            <wijmo:C1WizardStep ID="C1WizardStep1" Title="<%$ Resources:C1Wizard, Pager_Title1 %>">
                <%= Resources.C1Wizard.Pager_Content1 %>
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep2" Title="<%$ Resources:C1Wizard, Pager_Title2 %>">
                <%= Resources.C1Wizard.Pager_Content2 %>
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep3" Title="<%$ Resources:C1Wizard, Pager_Title3 %>">
                <%= Resources.C1Wizard.Pager_Content3 %>
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep4" Title="<%$ Resources:C1Wizard, Pager_Title4 %>">
                <%= Resources.C1Wizard.Pager_Content4 %>
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep5" Title="<%$ Resources:C1Wizard, Pager_Title5 %>">
                <%= Resources.C1Wizard.Pager_Content5 %>
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep6" Title="<%$ Resources:C1Wizard, Pager_Title6 %>">
                <%= Resources.C1Wizard.Pager_Content6 %>
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep7" Title="<%$ Resources:C1Wizard, Pager_Title7 %>">
                <%= Resources.C1Wizard.Pager_Content7 %>
            </wijmo:C1WizardStep>
        </Steps>
    </wijmo:C1Wizard>

    <wijmo:C1Pager runat="server" ID="C1Pager1" Mode="NextPreviousFirstLast" OnClientPageIndexChanged="onClientPageIndexChanged" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

    <p><%= Resources.C1Wizard.Pager_Text0 %></p>

    <p><%= Resources.C1Wizard.Pager_Text1 %></p>

    <p><%= Resources.C1Wizard.Pager_Text2 %></p>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li class="fullwidth">
                    <label class="settinglegend"><%= Resources.C1Wizard.Pager_ShowOptions %></label>
                </li>
                <li>
                    <input type="radio" value="Default" name="set1" id="setDefault" checked="checked" /><label for="setDefault"><%= Resources.C1Wizard.Pager_Default %></label>
                    <input type="radio" value="Text" name="set1" id="setText" /><label for="setText"><%= Resources.C1Wizard.Pager_Text %></label>
                </li>
                <li>
                    <input type="radio" value="Custom" name="set1" id="setCustom" /><label for="setCustom"><%= Resources.C1Wizard.Pager_CustomText %></label>
                </li>
            </ul>
        </div>
    </div>
</asp:Content>
