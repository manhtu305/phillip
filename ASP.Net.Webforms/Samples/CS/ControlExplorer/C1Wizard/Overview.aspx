<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1Wizard_Overview" CodeBehind="Overview.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Wizard" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <wijmo:C1Wizard ID="C1Wizard1" runat="server">
        <Steps>
            <wijmo:C1WizardStep ID="C1WizardStep1" Title="<%$ Resources:C1Wizard, Overview_Title1 %>" Description="<%$ Resources:C1Wizard, Overview_Description1 %>" Height="300">
                <%= Resources.C1Wizard.Overview_Content1 %>
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep2" Title="<%$ Resources:C1Wizard, Overview_Title2 %>" Description="<%$ Resources:C1Wizard, Overview_Description2 %>" Height="300">
                <%= Resources.C1Wizard.Overview_Content2 %>
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep3" Title="<%$ Resources:C1Wizard, Overview_Title3 %>" Description="<%$ Resources:C1Wizard, Overview_Description3 %>" Height="300">
                <%= Resources.C1Wizard.Overview_Content3 %>
            </wijmo:C1WizardStep>
            <wijmo:C1WizardStep ID="C1WizardStep4" Title="<%$ Resources:C1Wizard, Overview_Title4 %>" Description="<%$ Resources:C1Wizard, Overview_Description4 %>" Height="300">
                <%= Resources.C1Wizard.Overview_Content4 %>
            </wijmo:C1WizardStep>
        </Steps>
    </wijmo:C1Wizard>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p><%= Resources.C1Wizard.Overview_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>
