<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1Wizard_Animation" CodeBehind="Animation.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Wizard" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <wijmo:C1Wizard ID="C1Wizard1" runat="server">
                <Steps>
                    <wijmo:C1WizardStep ID="C1WizardStep1" Title="<%$ Resources:C1Wizard, Animation_Title1 %>" Description="<%$ Resources:C1Wizard, Animation_Description1 %>" Height="300">
                        <%= Resources.C1Wizard.Animation_Content1 %>
                    </wijmo:C1WizardStep>
                    <wijmo:C1WizardStep ID="C1WizardStep2" Title="<%$ Resources:C1Wizard, Animation_Text2 %>" Description="<%$ Resources:C1Wizard, Animation_Description2 %>" Height="300">
                        <%= Resources.C1Wizard.Animation_Content2 %>
                    </wijmo:C1WizardStep>
                    <wijmo:C1WizardStep ID="C1WizardStep3" Title="<%$ Resources:C1Wizard, Animation_Title3 %>" Description="<%$ Resources:C1Wizard, Animation_Description3 %>" Height="300">
                        <%= Resources.C1Wizard.Animation_Content3 %>
                    </wijmo:C1WizardStep>
                    <wijmo:C1WizardStep ID="C1WizardStep4" Title="<%$ Resources:C1Wizard, Animation_Title4 %>" Description="<%$ Resources:C1Wizard, Animation_Description4 %>" Height="300">
                        <%= Resources.C1Wizard.Animation_Content4 %>
                    </wijmo:C1WizardStep>
                </Steps>
            </wijmo:C1Wizard>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p><%= Resources.C1Wizard.Animation_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div class="settingcontainer">
                <div class="settingcontent">
                    <ul>
                        <li class="fullwidth">
                            <label class="settinglegend"><%= Resources.C1Wizard.Animation_ShowOptions %></label>
                        </li>
                        <li>
                            <asp:CheckBox ID="showblind" runat="server" Text="<%$ Resources:C1Wizard, Animation_Blind %>" />
                            <asp:CheckBox ID="showfade" runat="server" Text="<%$ Resources:C1Wizard, Animation_Fade %>" />
                        </li>
                        <li>
                            <label><%= Resources.C1Wizard.Animation_Duration %></label>
                            <asp:DropDownList ID="showduration" runat="server">
                                <asp:ListItem Selected="True">200</asp:ListItem>
                                <asp:ListItem>400</asp:ListItem>
                                <asp:ListItem>800</asp:ListItem>
                                <asp:ListItem>1500</asp:ListItem>
                            </asp:DropDownList>
                        </li>

                        <li class="fullwidth">
                            <label class="settinglegend"><%= Resources.C1Wizard.Animation_HideOptions %></label>
                        </li>
                        <li>
                            <asp:CheckBox ID="hideblind" runat="server" Text="<%$ Resources:C1Wizard, Animation_Blind %>" />
                            <asp:CheckBox ID="hidefade" runat="server" Text="<%$ Resources:C1Wizard, Animation_Fade %>" />
                        </li>
                        <li>
                            <label><%= Resources.C1Wizard.Animation_Duration %></label>
                            <asp:DropDownList ID="hideduration" runat="server">
                                <asp:ListItem Selected="True">200</asp:ListItem>
                                <asp:ListItem>400</asp:ListItem>
                                <asp:ListItem>800</asp:ListItem>
                                <asp:ListItem>1500</asp:ListItem>
                            </asp:DropDownList>
                        </li>
                    </ul>
                </div>
                <div class="settingcontrol">
                    <asp:Button ID="apply" runat="server" Text="<%$ Resources:C1Wizard, Animation_Apply %>" CssClass="settingapply" OnClick="apply_Click" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
