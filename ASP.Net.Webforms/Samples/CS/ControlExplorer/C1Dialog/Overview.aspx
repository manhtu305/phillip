<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="Overview.aspx.cs" Inherits="ControlExplorer.C1Window.Overview" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Dialog"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <cc1:C1Dialog ID="dialog" runat="server" Title="<%$ Resources:C1Dialog, Overview_DialogTitle %>" Width="300" Height="185">
        <CaptionButtons>
            <Refresh Visible="false" />
        </CaptionButtons>
        <Content>
            <p><%= Resources.C1Dialog.Overview_Text0 %></p>
        </Content>
    </cc1:C1Dialog>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Dialog.Overview_Text1 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">

    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li class="fullwidth">
                    <asp:Button runat="server" ID="btnHide" Text="<%$ Resources:C1Dialog, Overview_HideDialogText %>"  OnClientClick="return false;" />
                    <asp:Button runat="server" ID="btnShow" Text="<%$ Resources:C1Dialog, Overview_ShowDialogText %>"  OnClientClick="return false;" />
                </li>
            </ul>
        </div>
    </div>

    <script>
        $(document).ready(function () {

            var $dlg = $("#<%=dialog.ClientID%>");

            $("#" + "<%=btnHide.ClientID%>").on("click", function () {
                $dlg.c1dialog("close");
            });

            $("#" + "<%=btnShow.ClientID%>").on("click", function () {
                $dlg.c1dialog("open");
            });
        });
    </script>
</asp:Content>
