<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="True"
	CodeBehind="ModalDialog.aspx.cs" Inherits="ControlExplorer.C1Window.ModalDialog" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Dialog"
	TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">	
			<cc1:C1Dialog ID="dialog" runat="server" Width="400px" Height="180px" Modal="true"
				Stack="True" CloseText="Close" Title="<%$ Resources:C1Dialog, ModalDialog_DialogTitle %>">
				<Content>
					<p><%= Resources.C1Dialog.ModalDialog_Text0 %></p>
				</Content>
				<ExpandingAnimation Duration="400" />
				<CollapsingAnimation Duration="300" />
			</cc1:C1Dialog>
	<script type="text/javascript">
		function showDialog() {
			$('#<%=dialog.ClientID%>').c1dialog("open");
		}
		// there is a issue when the dialog is inside of an update panel, and the panel is inside of another widget, which sets relative position.
		// there is a workaround for this issue, firstly init the outer widget.
		$("#ctl00_ctl00_MainContent_WidgetTabs").c1tabs();
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Dialog.ModalDialog_Text1 %></p>
	<p><%= Resources.C1Dialog.ModalDialog_Text2 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
			<div class="settingcontainer">
				<div class="settingcontent">
					<ul>
						<li>
							<asp:Button runat="server" ID="btnShow" Text="<%$ Resources:C1Dialog, ModalDialog_ShowDialogText %>" OnClientClick="showDialog(); return false;" /></li>
					</ul>
				</div>
			</div>
</asp:Content>
