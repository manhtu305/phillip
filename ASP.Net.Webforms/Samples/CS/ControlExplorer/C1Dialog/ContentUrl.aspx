<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
    CodeFile="ContentUrl.aspx.cs" Inherits="Dialog_ContentUrl" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Dialog"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <cc1:C1Dialog ID="dialog" runat="server" Width="760px" Height="460px" Stack="True"
        CloseText="Close" Title="<%$ Resources:C1Dialog, ContentUrl_DialogTitle %>" ContentUrl="<%$ Resources:C1Dialog, ContentUrl_ContentUrl %>">
        <CollapsingAnimation>
            <Animated Effect="blind" />
        </CollapsingAnimation>
    </cc1:C1Dialog>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Dialog.ContentUrl_Text0 %></p>
    <p><%= Resources.C1Dialog.ContentUrl_Text1 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
