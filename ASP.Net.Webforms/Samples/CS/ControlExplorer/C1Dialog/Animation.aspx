<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="Animation.aspx.cs" Inherits="ControlExplorer.Dialog.Animation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Dialog"
	TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
	<style>
		.settingcontainer .settingcontent li label
		{
			width:120px;
		}
	</style>	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
			<cc1:C1Dialog ID="dialog" runat="server" Width="550px" Height="240px" Title="<%$ Resources:C1Dialog, Animation_DialogTitle %>">
				<Content>
					<%= Resources.C1Dialog.Animation_DialogContent %>
				</Content>
			</cc1:C1Dialog>		
	<script type="text/javascript">
		function showDialog(p) {
			$('#<%=dialog.ClientID%>').c1dialog(p);
		}
		$(document).ready(function () {

			$('#<%=expandEffectTypes.ClientID%>').change(function () {
        		var ee = $("#<%=expandEffectTypes.ClientID%> option:selected").val();
        		$("#<%=dialog.ClientID%>").c1dialog("option", "expandingAnimation", { animated: ee, duration: 500 });
        	});

        	$('#<%=collapseEffectTypes.ClientID%>').change(function () {
        		var ce = $("#<%=collapseEffectTypes.ClientID%> option:selected").val();
        		$("#<%=dialog.ClientID%>").c1dialog("option", "collapsingAnimation", { animated: ce, duration: 500 });
        	})

        	$('#<%=showEffectTypes.ClientID%>').change(function () {
        		var ee = $("#<%=showEffectTypes.ClientID%> option:selected").val();
        		$("#<%=dialog.ClientID%>").c1dialog("option", "show", ee);
        	});

        	$('#<%=hideEffectTypes.ClientID%>').change(function () {
        		var ce = $("#<%=hideEffectTypes.ClientID%> option:selected").val();
        		$("#<%=dialog.ClientID%>").c1dialog("option", "hide", ce);
        	})
		});

		// there is a issue when the dialog is inside of an update panel, and the panel is inside of another widget, which sets relative position.
		// there is a workaround for this issue, firstly init the outer widget.
		$("#ctl00_ctl00_MainContent_WidgetTabs").c1tabs();
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Dialog.Animation_Text0 %></p>
	<p><%= Resources.C1Dialog.Animation_Text1 %></p>
    <ul>
		<li><b>ExpandingAnimation</b></li>
		<li><b>CollapsingAnimation</b></li>
		<li><b>Show</b></li>
		<li><b>Hide</b></li>
	</ul>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">	
			<div class="settingcontainer">
				<div class="settingcontent">
					<ul>
						<li class="fullwidth">
							<asp:Button ID="btnShow" runat="server"  Text="<%$ Resources:C1Dialog, Animation_ShowDialogText %>" OnClientClick="showDialog('open'); return false;"  />
							<asp:Button ID="btnHide" runat="server"  Text="<%$ Resources:C1Dialog, Animation_HideDialogText %>" OnClientClick="showDialog('close'); return false;"  />
						</li>
						<li class="fullwidth">
							<label for="showingEffectTypes">
								<%= Resources.C1Dialog.Animation_ShowAnimationLabel %>
							</label>
							<asp:DropDownList ID="showEffectTypes" runat="server">
								<asp:ListItem Value="blind" Selected="True">Blind</asp:ListItem>
								<asp:ListItem Value="bounce">Bounce</asp:ListItem>
								<asp:ListItem Value="clip">Clip</asp:ListItem>
								<asp:ListItem Value="drop">Drop</asp:ListItem>
								<asp:ListItem Value="explode">Explode</asp:ListItem>
								<asp:ListItem Value="fade">Fade</asp:ListItem>
								<asp:ListItem Value="fold">Fold</asp:ListItem>
								<asp:ListItem Value="highlight">Highlight</asp:ListItem>
								<asp:ListItem Value="puff">Puff</asp:ListItem>
								<asp:ListItem Value="pulsate">Pulsate</asp:ListItem>
								<asp:ListItem Value="scale">Scale</asp:ListItem>
								<asp:ListItem Value="size">Size</asp:ListItem>
								<asp:ListItem Value="slide">Slide</asp:ListItem>
							</asp:DropDownList>
						</li>
						<li class="fullwidth">
							<label for="showingEffectTypes">
								<%= Resources.C1Dialog.Animation_HideAnimationLabel %>
							</label>
							<asp:DropDownList ID="hideEffectTypes" runat="server">
								<asp:ListItem Value="blind">Blind</asp:ListItem>
								<asp:ListItem Value="bounce">Bounce</asp:ListItem>
								<asp:ListItem Value="clip">Clip</asp:ListItem>
								<asp:ListItem Value="drop">Drop</asp:ListItem>
								<asp:ListItem Value="explode" Selected="True">Explode</asp:ListItem>
								<asp:ListItem Value="fade">Fade</asp:ListItem>
								<asp:ListItem Value="fold">Fold</asp:ListItem>
								<asp:ListItem Value="highlight">Highlight</asp:ListItem>
								<asp:ListItem Value="puff">Puff</asp:ListItem>
								<asp:ListItem Value="pulsate">Pulsate</asp:ListItem>
								<asp:ListItem Value="scale">Scale</asp:ListItem>
								<asp:ListItem Value="size">Size</asp:ListItem>
								<asp:ListItem Value="slide">Slide</asp:ListItem>
							</asp:DropDownList>
						</li>
						<li class="fullwidth">
							<label for="showingEffectTypes">
								<%= Resources.C1Dialog.Animation_ExpandAnimationLabel %>
							</label>
							<asp:DropDownList ID="expandEffectTypes" runat="server">
								<asp:ListItem Value="blind">Blind</asp:ListItem>
								<asp:ListItem Value="bounce">Bounce</asp:ListItem>
								<asp:ListItem Value="clip">Clip</asp:ListItem>
								<asp:ListItem Value="drop">Drop</asp:ListItem>
								<asp:ListItem Value="explode">Explode</asp:ListItem>
								<asp:ListItem Value="fade">Fade</asp:ListItem>
								<asp:ListItem Value="fold">Fold</asp:ListItem>
								<asp:ListItem Value="highlight" Selected="True">Highlight</asp:ListItem>
								<asp:ListItem Value="puff">Puff</asp:ListItem>
								<asp:ListItem Value="pulsate">Pulsate</asp:ListItem>
								<asp:ListItem Value="scale">Scale</asp:ListItem>
								<asp:ListItem Value="size">Size</asp:ListItem>
								<asp:ListItem Value="slide">Slide</asp:ListItem>
							</asp:DropDownList>
						</li>
						<li class="fullwidth">
							<label for="showingEffectTypes">
								<%= Resources.C1Dialog.Animation_CollapseAnimationLabel %>
							</label>
							<asp:DropDownList ID="collapseEffectTypes" runat="server">
								<asp:ListItem Value="blind">Blind</asp:ListItem>
								<asp:ListItem Value="bounce">Bounce</asp:ListItem>
								<asp:ListItem Value="clip">Clip</asp:ListItem>
								<asp:ListItem Value="drop">Drop</asp:ListItem>
								<asp:ListItem Value="explode">Explode</asp:ListItem>
								<asp:ListItem Value="fade">Fade</asp:ListItem>
								<asp:ListItem Value="fold">Fold</asp:ListItem>
								<asp:ListItem Value="highlight">Highlight</asp:ListItem>
								<asp:ListItem Value="puff" Selected="True">Puff</asp:ListItem>
								<asp:ListItem Value="pulsate">Pulsate</asp:ListItem>
								<asp:ListItem Value="scale">Scale</asp:ListItem>
								<asp:ListItem Value="size">Size</asp:ListItem>
								<asp:ListItem Value="slide">Slide</asp:ListItem>
							</asp:DropDownList>
						</li>
					</ul>
				</div>
			</div>		
</asp:Content>
