<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
	CodeFile="ModalAlert.aspx.cs" Inherits="Dialog_ModalAlert" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Dialog"
	TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
	<script type="text/javascript">
		function btnClick() {
			$(this).c1dialog("close");
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<cc1:C1Dialog ID="dialog" runat="server" Width="550px" Height="240px" Modal="true"
		Stack="True" CloseText="Close" Title="<%$ Resources:C1Dialog, ModalAlert_DialogTitle %>">
		<Content>
			<p><%= Resources.C1Dialog.ModalAlert_Text0 %></p>
		</Content>
		<ExpandingAnimation Duration="400">
		</ExpandingAnimation>
		<Buttons>
			<cc1:DialogButton OnClientClick="btnClick" Text="<%$ Resources:C1Dialog, ModalAlert_DialogButtonText1 %>" />
		</Buttons>
		<CaptionButtons>
			<Pin Visible="False" />
			<Refresh Visible="False" />
			<Toggle Visible="False" />
			<Minimize Visible="False" />
			<Maximize Visible="False" />
		</CaptionButtons>
		<CollapsingAnimation Duration="300" />
	</cc1:C1Dialog>

	<script type="text/javascript">
		function showDialog() {
			$('#<%=dialog.ClientID%>').c1dialog('open');
		}
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Dialog.ModalAlert_Text1 %></p>
	<ul>
		<li><%= Resources.C1Dialog.ModalAlert_Li1 %></li>
		<li><%= Resources.C1Dialog.ModalAlert_Li2 %></li>
		<li><%= Resources.C1Dialog.ModalAlert_Li3 %></li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<div class="settingcontainer">
		<div class="settingcontent">
			<ul>
				<li>
					<input type="button" value="<%= Resources.C1Dialog.ModalAlert_ShowAlertText %>" onclick="showDialog()" /></li>
			</ul>
		</div>
	</div>
</asp:Content>
