<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
	CodeFile="ModalConfirm.aspx.cs" Inherits="Dialog_ModalConfirm" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Dialog"
	TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
	<script type="text/javascript">
		function delClick() {
			$(this).c1dialog("close");
		}
		function cancelClick() {
			$(this).c1dialog("close");
		}

		function showDialog() {
			$('#<%=dialog.ClientID%>').c1dialog('open');
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<cc1:C1Dialog ID="dialog" runat="server" Resizable="false" Width="400" Height="220" Title="<%$ Resources:C1Dialog, ModalConfirm_DialogTitle %>"
		Modal="True" CloseText="Close">
		<Content>
			<p><%= Resources.C1Dialog.ModalConfirm_Text0 %></p>
		</Content>
		<ExpandingAnimation Duration="400" />
		<CollapsingAnimation Duration="300" />
		<Buttons>
			<cc1:DialogButton OnClientClick="delClick" Text="<%$ Resources:C1Dialog, ModalConfirm_DialogButtonText1 %>" />
			<cc1:DialogButton OnClientClick="cancelClick" Text="<%$ Resources:C1Dialog, ModalConfirm_DialogButtonText2 %>" />
		</Buttons>
		<CaptionButtons>
			<Pin Visible="false" />
			<Refresh Visible="False" />
			<Toggle Visible="False" />
			<Minimize Visible="False" />
			<Maximize Visible="False" />
		</CaptionButtons>
	</cc1:C1Dialog>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Dialog.ModalConfirm_Text1 %></p>
	<ul>
		<li><%= Resources.C1Dialog.ModalConfirm_Li1 %></li>
		<li><%= Resources.C1Dialog.ModalConfirm_Li2 %></li>
		<li><%= Resources.C1Dialog.ModalConfirm_Li3 %></li>
	</ul>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<div class="settingcontainer">
		<div class="settingcontent">
			<ul>
				<li>
					<input type="button" value="<%= Resources.C1Dialog.ModalConfirm_ShowConfirmText %>" onclick="showDialog()" /></li>
			</ul>
		</div>
	</div>
</asp:Content>
