<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Symbols.aspx.cs" Inherits="ControlExplorer.C1BubbleChart.Symbols" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
	function hint () {
		return this.data.y1 + "%";
	}
</script>

<wijmo:C1BubbleChart runat="server" ID="BubbleChart1" Height="475" Width = "756" Legend-Visible="false">
	<Axis>
		<X Text=""></X>
		<Y Text="<%$ Resources:C1BubbleChart, Symbols_AxisYText %>" Compass="West"></Y>
	</Axis>
	<Hint>
		<Content Function="hint" />
	</Hint>
	<Header Text="<%$ Resources:C1BubbleChart, Symbols_HeaderText %>"></Header>
	<SeriesHoverStyles>
		<wijmo:ChartStyle Opacity="0.5"></wijmo:ChartStyle>
	</SeriesHoverStyles>
	<ChartLabel Position="Outside"></ChartLabel>
	<SeriesList>
		<wijmo:BubbleChartSeries Label="Market Share" LegendEntry="true">
			<Data>
				<X>
					<Values>
						<wijmo:ChartXData StringValue ="Chrome" />
						<wijmo:ChartXData StringValue="Firefox" />
						<wijmo:ChartXData StringValue="IE" />
						<wijmo:ChartXData StringValue="Opera" />
						<wijmo:ChartXData StringValue="Safari" />
					</Values>
				</X>
				<Y>
					<Values>
						<wijmo:ChartYData DoubleValue="22.9" />
						<wijmo:ChartYData DoubleValue="39.7" />
						<wijmo:ChartYData DoubleValue="30.5" />
						<wijmo:ChartYData DoubleValue="4.0" />
						<wijmo:ChartYData DoubleValue="2.2" />
					</Values>
				</Y>
				<Y1 DoubleValues="22.9,39.7,30.5,4.0,2.2" />
			</Data>
			<Markers>
				<Symbol>
					<wijmo:BubbleChartSymbol Index="0" Url="images/chrome.png" />
					<wijmo:BubbleChartSymbol Index="1" Url="images/firefox.png" />
					<wijmo:BubbleChartSymbol Index="2" Url="images/ie.png" />
					<wijmo:BubbleChartSymbol Index="3" Url="images/opera.png" />
					<wijmo:BubbleChartSymbol Index="4" Url="images/safari.png" />
				</Symbol>
			</Markers>
		</wijmo:BubbleChartSeries>
	</SeriesList>
</wijmo:C1BubbleChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1BubbleChart.Symbols_Text0 %></p>
	<h3><%= Resources.C1BubbleChart.Symbols_Text1 %></h3>
	<ul>
		<li><%= Resources.C1BubbleChart.Symbols_Li1 %></li>
		<li><%= Resources.C1BubbleChart.Symbols_Li2 %></li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
