<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Annotation.aspx.cs" Inherits="ControlExplorer.C1BubbleChart.Annotation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1BubbleChart runat="server" ID="C1BubbleChart1" DataSourceID="AccessDataSource1" Height="475" Width="756">
		<Animation Duration="500" Easing="EaseOutElastic"></Animation>
		<Header Compass="North"></Header>
		<Footer Compass="South" Visible="False"></Footer>
		<Axis>
			<X>
				<TextStyle Rotation="-45">
				</TextStyle>
				<GridMajor Visible="True"></GridMajor>
				<GridMinor Visible="False"></GridMinor>
			</X>
			<Y Visible="False" Compass="West">
				<Labels TextAlign="Center"></Labels>
				<GridMajor Visible="True"></GridMajor>
				<GridMinor Visible="False"></GridMinor>
			</Y>
		</Axis>
		<Annotations>
			<wijmo:TextAnnotation Attachment="Relative" Offset="0, 0" Text="Relative" 
				Tooltip="Point: { X: 0.75, Y: 0.15 } Attachment: Relative">
				<Point>
					<X DoubleValue="0.75" />
					<Y DoubleValue="0.15" />
				</Point>
				<AnnotationStyle FontSize="26" FontWeight="Bold">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:TextAnnotation>
			<wijmo:RectAnnotation Attachment="DataIndex" Content="DataIndex" Width="100" Offset="0, 0" PointIndex="1" 
				Tooltip="PointIndex:1 Attachment: DataIndex">
				<AnnotationStyle FillOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:RectAnnotation>
			<wijmo:EllipseAnnotation Attachment="Relative" Content="Relative" Height="35" Offset="0, 0" 
				Tooltip="Point: { X: 0.4, Y: 0.45 } Attachment: Relative" Width="75">
				<Point>
					<X DoubleValue="0.4" />
					<Y DoubleValue="0.45" />
				</Point>
				<AnnotationStyle FillOpacity="1" Stroke="#c2955f" StrokeWidth="2" StrokeOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:EllipseAnnotation>
			<wijmo:CircleAnnotation Attachment="DataIndex" PointIndex="5" Radius="40" SeriesIndex="0" Content="DataIndex" Offset="0, 0" 
				Tooltip="Radius: 40 PointIndex: 5 Attachment: DataIndex">
				<AnnotationStyle FillOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:CircleAnnotation>
			<wijmo:ImageAnnotation Attachment="DataIndex" PointIndex="7" Href="./../Images/c1icon.png" Offset="0, 0" Content=" "
				Tooltip="PointIndex: 7 Attachment: DataIndex">
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:ImageAnnotation>
			<wijmo:PolygonAnnotation Offset="0, 0" Tooltip="Points: [(200,0),(150,50),(175,100),(255,100),(250, 50)]" Content="Absolute">
				<Points>
						<wijmo:PointF X="200" Y="0" />
						<wijmo:PointF X="150" Y="50" />
						<wijmo:PointF X="175" Y="100" />
						<wijmo:PointF X="225" Y="100" />
						<wijmo:PointF X="250" Y="50" />
				</Points>
				<AnnotationStyle FillOpacity="2" Stroke="#01A9DB" StrokeWidth="4" StrokeOpacity="1">
					<Fill Color="#FFFFFF"> </Fill >
				</AnnotationStyle>
			</wijmo:PolygonAnnotation>
			<wijmo:SquareAnnotation Attachment="DataIndex" Width="40" Content="DataIndex" Offset="0, 0" PointIndex="4" SeriesIndex="0" 
				Tooltip="Width: 40 PointIndex: 4 Attachment: DataIndex">
				<AnnotationStyle FillOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:SquareAnnotation>
			<wijmo:LineAnnotation Content="Absolute" End="350, 300" Offset="0, 0" Start="50, 150"
				Tooltip="Start: { X: 50, Y: 150 } End: { X: 350, Y: 300 } Attachment: Absolute">
				<AnnotationStyle FillOpacity="2" StrokeWidth="4" Stroke="#01A9DB">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:LineAnnotation>
		</Annotations>

		<Indicator Visible="False"></Indicator>
		<DataBindings>
			<wijmo:C1BubbleChartBinding XField="CategoryName" XFieldType="String" YField="Sales" YFieldType="Number" Y1Field="CT" />
		</DataBindings>
	</wijmo:C1BubbleChart>

	<asp:AccessDataSource ID="AccessDataSource1" runat="server"
		DataFile="~/App_Data/C1NWind.mdb"
		SelectCommand="select CategoryName, sum(ProductSales) as Sales, count(1) as CT from (SELECT DISTINCTROW Categories.CategoryName as CategoryName, Products.ProductName, Sum([Order Details Extended].ExtendedPrice) AS ProductSales
FROM Categories INNER JOIN (Products INNER JOIN (Orders INNER JOIN [Order Details Extended] ON Orders.OrderID = [Order Details Extended].OrderID) ON Products.ProductID = [Order Details Extended].ProductID) ON Categories.CategoryID = Products.CategoryID
WHERE (((Orders.OrderDate) Between #1/1/95# And #12/31/95#))
GROUP BY Categories.CategoryID, Categories.CategoryName, Products.ProductName
ORDER BY Products.ProductName) group by CategoryName;"></asp:AccessDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1BubbleChart.Annotation_Text0 %></p>
	<h3><%= Resources.C1BubbleChart.Annotation_Text1 %></h3>
	<ul>
		<li><%= Resources.C1BubbleChart.Annotation_Li1 %></li>
		<li><%= Resources.C1BubbleChart.Annotation_Li2 %></li>
	</ul>
</asp:Content>
