<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="DataBind.aspx.cs" Inherits="ControlExplorer.C1BubbleChart.DataBind" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<wijmo:C1BubbleChart runat="server" ID="C1BubbleChart1" DataSourceID="AccessDataSource1" Height="475" Width = "756">
<Animation Duration="500" Easing="EaseOutElastic"></Animation>

<Footer Compass="South" Visible="False"></Footer>

<Axis>
	<X>
		<TextStyle Rotation="-45">
		</TextStyle>
	</X>
<Y Visible="False" Compass="West">
<Labels TextAlign="Center"></Labels>

<GridMajor Visible="True"></GridMajor>
</Y>
</Axis>
<DataBindings>
<wijmo:C1BubbleChartBinding XField="CategoryName" XFieldType="String" YField="Sales" YFieldType="Number" Y1Field="CT" />
</DataBindings>
</wijmo:C1BubbleChart>

<asp:AccessDataSource ID="AccessDataSource1" runat="server" 
		DataFile="~/App_Data/C1NWind.mdb" 
		SelectCommand="select CategoryName, sum(ProductSales) as Sales, count(1) as CT from (SELECT DISTINCTROW Categories.CategoryName as CategoryName, Products.ProductName, Sum([Order Details Extended].ExtendedPrice) AS ProductSales
FROM Categories INNER JOIN (Products INNER JOIN (Orders INNER JOIN [Order Details Extended] ON Orders.OrderID = [Order Details Extended].OrderID) ON Products.ProductID = [Order Details Extended].ProductID) ON Categories.CategoryID = Products.CategoryID
WHERE (((Orders.OrderDate) Between #1/1/95# And #12/31/95#))
GROUP BY Categories.CategoryID, Categories.CategoryName, Products.ProductName
ORDER BY Products.ProductName) group by CategoryName;">
	</asp:AccessDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
<p><%= Resources.C1BubbleChart.DataBind_Text0 %></p>
	<p><%= Resources.C1BubbleChart.DataBind_Text1 %></p>
	<ul>
		<li><%= Resources.C1BubbleChart.DataBind_Li1 %></li>
		<li><%= Resources.C1BubbleChart.DataBind_Li2 %></li>
		<li><%= Resources.C1BubbleChart.DataBind_Li3 %></li>
		<li><%= Resources.C1BubbleChart.DataBind_Li4 %></li>
		<li><%= Resources.C1BubbleChart.DataBind_Li5 %></li>
		<li><%= Resources.C1BubbleChart.DataBind_Li6 %></li>
		<li><%= Resources.C1BubbleChart.DataBind_Li7 %></li>
	</ul>
	<p><%= Resources.C1BubbleChart.DataBind_Text2 %></p>
	<ul>
		<li><%= Resources.C1BubbleChart.DataBind_Li8 %></li>
		<li><%= Resources.C1BubbleChart.DataBind_Li9 %></li>
	</ul>
	<p><%= Resources.C1BubbleChart.DataBind_Text3 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
