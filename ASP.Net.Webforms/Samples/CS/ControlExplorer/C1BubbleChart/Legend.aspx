<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Legend.aspx.cs" Inherits="ControlExplorer.C1BubbleChart.Legend" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
<wijmo:C1BubbleChart runat="server" ID="C1BubbleChart1" MinimumSize="3" MaximumSize="15" Height="475" Width = "756">
	<ChartLabel ChartLabelFormatString="c2'kkk'"></ChartLabel>

<Footer Compass="South" Visible="False"></Footer>

	<Legend Compass="North" Orientation="Horizontal" Text="<%$ Resources:C1BubbleChart, Legend_LegendText %>">
		<Size Width="100" />
	</Legend>
	<Axis>
		<X Text="<%$ Resources:C1BubbleChart, Legend_AxisXText %>">
<GridMajor Visible="False"></GridMajor>

<GridMinor Visible="False"></GridMinor>
		</X>
		<Y Text="<%$ Resources:C1BubbleChart, Legend_AxisYText %>" Compass="West">
<GridMajor Visible="True"></GridMajor>

<GridMinor Visible="False"></GridMinor>
		</Y>
	</Axis>
	<Hint>
		<Content Function="hint" />
	</Hint>

<Animation Duration="500" Easing="EaseOutElastic"></Animation>

	<SeriesList>
		<wijmo:BubbleChartSeries Label="China" LegendEntry="True">
			<Data>
				<Y1>
					<Values>
						<wijmo:ChartY1Data DoubleValue="11.6" />
						<wijmo:ChartY1Data DoubleValue="9.7" />
						<wijmo:ChartY1Data DoubleValue="0" />
						<wijmo:ChartY1Data DoubleValue="7" />
						<wijmo:ChartY1Data DoubleValue="8.4" />
					</Values>
				</Y1>
				<X>
					<Values>
						<wijmo:ChartXData StringValue="Machines, engines" />
						<wijmo:ChartXData StringValue="Electronics" />
						<wijmo:ChartXData StringValue="Oil" />
						<wijmo:ChartXData StringValue="Vehicles" />
						<wijmo:ChartXData StringValue="Aircraft, spacecraft" />
					</Values>
				</X>
				<Y>
					<Values>
						<wijmo:ChartYData DoubleValue="10.48" />
						<wijmo:ChartYData DoubleValue="8.77" />
						<wijmo:ChartYData DoubleValue="0" />
						<wijmo:ChartYData DoubleValue="6.32" />
						<wijmo:ChartYData DoubleValue="7.59" />
					</Values>
				</Y>
			</Data>
		</wijmo:BubbleChartSeries>
		<wijmo:BubbleChartSeries Label="Japan" LegendEntry="True">
			<Data>
				<Y1>
					<Values>
						<wijmo:ChartY1Data DoubleValue="5.9" />
						<wijmo:ChartY1Data DoubleValue="4.2" />
						<wijmo:ChartY1Data DoubleValue="0" />
						<wijmo:ChartY1Data DoubleValue="7.3" />
						<wijmo:ChartY1Data DoubleValue="5.7" />
					</Values>
				</Y1>
				<X>
					<Values>
						<wijmo:ChartXData StringValue="Machines, engines" />
						<wijmo:ChartXData StringValue="Electronics" />
						<wijmo:ChartXData StringValue="Oil" />
						<wijmo:ChartXData StringValue="Vehicles" />
						<wijmo:ChartXData StringValue="Aircraft, spacecraft" />
					</Values>
				</X>
				<Y>
					<Values>
						<wijmo:ChartYData DoubleValue="8" />
						<wijmo:ChartYData DoubleValue="7.71" />
						<wijmo:ChartYData DoubleValue="3.71" />
						<wijmo:ChartYData DoubleValue="0" />
						<wijmo:ChartYData DoubleValue="11.85" />
					</Values>
				</Y>
			</Data>
		</wijmo:BubbleChartSeries>
		<wijmo:BubbleChartSeries Label="Germany" LegendEntry="True">
			<Data>
				<Y1>
					<Values>
						<wijmo:ChartY1Data DoubleValue="5.9" />
						<wijmo:ChartY1Data DoubleValue="4.2" />
						<wijmo:ChartY1Data DoubleValue="0" />
						<wijmo:ChartY1Data DoubleValue="7.3" />
						<wijmo:ChartY1Data DoubleValue="5.7" />
					</Values>
				</Y1>
				<X>
					<Values>
						<wijmo:ChartXData StringValue="Machines, engines" />
						<wijmo:ChartXData StringValue="Electronics" />
						<wijmo:ChartXData StringValue="Oil" />
						<wijmo:ChartXData StringValue="Vehicles" />
						<wijmo:ChartXData StringValue="Aircraft, spacecraft" />
					</Values>
				</X>
				<Y>
					<Values>
						<wijmo:ChartYData DoubleValue="12.19" />
						<wijmo:ChartYData DoubleValue="8.67" />
						<wijmo:ChartYData DoubleValue="0" />
						<wijmo:ChartYData DoubleValue="15.08" />
						<wijmo:ChartYData DoubleValue="11.77" />
					</Values>
				</Y>
			</Data>
		</wijmo:BubbleChartSeries>
		<wijmo:BubbleChartSeries Label="UK" LegendEntry="True">
			<Data>
				<Y1>
					<Values>
						<wijmo:ChartY1Data DoubleValue="7.7" />
						<wijmo:ChartY1Data DoubleValue="4.8" />
						<wijmo:ChartY1Data DoubleValue="7.2" />
						<wijmo:ChartY1Data DoubleValue="1.4" />
						<wijmo:ChartY1Data DoubleValue="6.1" />
					</Values>
				</Y1>
				<X>
					<Values>
						<wijmo:ChartXData StringValue="Machines, engines" />
						<wijmo:ChartXData StringValue="Electronics" />
						<wijmo:ChartXData StringValue="Oil" />
						<wijmo:ChartXData StringValue="Vehicles" />
						<wijmo:ChartXData StringValue="Aircraft, spacecraft" />
					</Values>
				</X>
				<Y>
					<Values>
						<wijmo:ChartYData DoubleValue="17.62" />
						<wijmo:ChartYData DoubleValue="10.98" />
						<wijmo:ChartYData DoubleValue="16.47" />
						<wijmo:ChartYData DoubleValue="3.2" />
						<wijmo:ChartYData DoubleValue="13.95" />
					</Values>
				</Y>
			</Data>
		</wijmo:BubbleChartSeries>
		<wijmo:BubbleChartSeries Label="France" LegendEntry="True">
			<Data>
				<Y1>
					<Values>
						<wijmo:ChartY1Data DoubleValue="2.8" />
						<wijmo:ChartY1Data DoubleValue="2.4" />
						<wijmo:ChartY1Data DoubleValue="4.4" />
						<wijmo:ChartY1Data DoubleValue="0.685" />
						<wijmo:ChartY1Data DoubleValue="8.4" />
					</Values>
				</Y1>
				<X>
					<Values>
						<wijmo:ChartXData StringValue="Machines, engines" />
						<wijmo:ChartXData StringValue="Electronics" />
						<wijmo:ChartXData StringValue="Oil" />
						<wijmo:ChartXData StringValue="Vehicles" />
						<wijmo:ChartXData StringValue="Aircraft, spacecraft" />
					</Values>
				</X>
				<Y>
					<Values>
						<wijmo:ChartYData DoubleValue="8.78" />
						<wijmo:ChartYData DoubleValue="6.25" />
						<wijmo:ChartYData DoubleValue="13.75" />
						<wijmo:ChartYData DoubleValue="2.14" />
						<wijmo:ChartYData DoubleValue="26.25" />
					</Values>
				</Y>
			</Data>
		</wijmo:BubbleChartSeries>
	</SeriesList>

	<Header Text="<%$ Resources:C1BubbleChart, Legend_HeaderText %>"></Header>
</wijmo:C1BubbleChart>
	<script type="text/javascript">
		function hint() {
		    return "<%= Resources.C1BubbleChart.Legend_Hint1 %>" + this.label + "<%= Resources.C1BubbleChart.Legend_Hint2 %>" + this.data.data.x[this.x] +
				"<%= Resources.C1BubbleChart.Legend_Hint3 %>" + this.y + '%' + "<%= Resources.C1BubbleChart.Legend_Hint4 %>" + this.data.y1;
		}
	</script>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
<p><%= Resources.C1BubbleChart.Legend_Text0 %></p>
	<h3><%= Resources.C1BubbleChart.Legend_Text1 %></h3>
            <ul>
                <li><%= Resources.C1BubbleChart.Legend_Li1 %></li>
                <li><%= Resources.C1BubbleChart.Legend_Li2 %></li>
                <li><%= Resources.C1BubbleChart.Legend_Li3 %></li>
                <li><%= Resources.C1BubbleChart.Legend_Li4 %></li>
            </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
			<ContentTemplate>
<div class="settingcontainer">
<div class="settingcontent">
	<ul>
		<li class="fullwidth"><label class="settinglegend"><%= Resources.C1BubbleChart.Legend_LegendLayoutLabel %></label></li>
		<li class="fullwidth"><asp:CheckBox ID="LegendVisibleCkb" runat="server" Checked="True" Text="<%$ Resources:C1BubbleChart, Legend_VisibleText %>" /></li>
		<li><label><%= Resources.C1BubbleChart.Legend_CompassLabel %></label>
			<asp:dropdownlist id="CompassDdl" runat="server">
			<asp:ListItem Text="<%$ Resources:C1BubbleChart, Legend_CompassNorth %>" Value="North"></asp:ListItem>
			<asp:ListItem Selected="True" Text="<%$ Resources:C1BubbleChart, Legend_CompassEast %>" Value="East"></asp:ListItem>
			<asp:ListItem Text="<%$ Resources:C1BubbleChart, Legend_CompassSouth %>" Value="South"></asp:ListItem>
			<asp:ListItem Text="<%$ Resources:C1BubbleChart, Legend_CompassWest %>" Value="West"></asp:ListItem>
			</asp:DropDownList>
		</li>
		<li><label><%= Resources.C1BubbleChart.Legend_OrientationLabel %></label>
			<asp:dropdownlist id="OrientationDdl" runat="server">
			<asp:ListItem Text="<%$ Resources:C1BubbleChart, Legend_OrientationHorizontal %>" Value="Horizontal"></asp:ListItem>
			<asp:ListItem Selected="True" Text="<%$ Resources:C1BubbleChart, Legend_OrientationVertical %>" Value="Vertical"></asp:ListItem>
			</asp:DropDownList>
		</li>
	</ul></div>
	<div class="settingcontrol">
	<asp:Button ID="ApplyBtn" runat="server" CssClass="settingapply" OnClick="ApplyBtn_Click" Text="<%$ Resources:C1BubbleChart, Legend_Apply %>" />
	</div>
</div>
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
