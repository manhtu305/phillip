<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="OutsideLabel.aspx.cs" Inherits="ControlExplorer.C1BubbleChart.OutsideLabel" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
<ContentTemplate>
<wijmo:C1BubbleChart runat="server" ID="BubbleChart1" MinimumSize="3" MaximumSize="15" Height="475" Width = "756">
	<Axis>
		<X Text=""></X>
		<Y Text="<%$ Resources:C1BubbleChart, OutsideLabel_AxisYText %>" Compass="West"></Y>
	</Axis>
	<Hint>
		<Content Function="hint" />
	</Hint>
	<Legend Visible="false"></Legend>
	<Header Text="<%$ Resources:C1BubbleChart, OutsideLabel_HeaderText %>"></Header>
	<ChartLabel Position="Outside" Compass="North"></ChartLabel>
	<SeriesList>
		<wijmo:BubbleChartSeries Label="West" LegendEntry="true">
			<Data>
				<X>
					<Values>
						<wijmo:ChartXData DoubleValue="5" />
						<wijmo:ChartXData DoubleValue="14" />
						<wijmo:ChartXData DoubleValue="20" />
						<wijmo:ChartXData DoubleValue="18" />
						<wijmo:ChartXData DoubleValue="22" />
					</Values>
				</X>
				<Y>
					<Values>
						<wijmo:ChartYData DoubleValue="5500" />
						<wijmo:ChartYData DoubleValue="12200" />
						<wijmo:ChartYData DoubleValue="60000" />
						<wijmo:ChartYData DoubleValue="24400" />
						<wijmo:ChartYData DoubleValue="32000" />
					</Values>
				</Y>
				<Y1 DoubleValues="3,12,33,10,42" />
			</Data>
		</wijmo:BubbleChartSeries>
	</SeriesList>
</wijmo:C1BubbleChart><script type="text/javascript">
function hint() {
    return this.data.label + ' x:' + this.x + ',y:' + this.y + ",y1:" + this.data.y1;
}
</script>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1BubbleChart.OutsideLabel_Text0 %></p>
	<p><%= Resources.C1BubbleChart.OutsideLabel_Text1 %></p>
	<h3><%= Resources.C1BubbleChart.OutsideLabel_Text2 %></h3>
	<ul>
		<li><%= Resources.C1BubbleChart.OutsideLabel_Li1 %></li>
		<li><%= Resources.C1BubbleChart.OutsideLabel_Li2 %></li>
		<li><%= Resources.C1BubbleChart.OutsideLabel_Li3 %></li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
<ContentTemplate>
<div class="settingcontainer">
<div class="settingcontent">
	<ul>
		<li class="fullwidth"><label class="settinglegend"><%= Resources.C1BubbleChart.OutsideLabel_LabelLayoutOptionsLabel %></label></li>
		<li><label><%= Resources.C1BubbleChart.OutsideLabel_PositionLabel %></label><asp:DropDownList ID="DdlPosition" runat="server">
			<asp:ListItem Text="<%$ Resources:C1BubbleChart, OutsideLabel_PositionInside %>" Value="Inside"></asp:ListItem>
			<asp:ListItem Selected="True" Text="<%$ Resources:C1BubbleChart, OutsideLabel_PositionOutside %>" Value="Outside"></asp:ListItem>
			</asp:DropDownList></li>
		<li><label><%= Resources.C1BubbleChart.OutsideLabel_CompassLabel %></label>
			<asp:DropDownList ID="DdlCompass" runat="server">
			<asp:ListItem Selected="True" Text="<%$ Resources:C1BubbleChart, OutsideLabel_CompassNorth %>" Value="North"></asp:ListItem>
			<asp:ListItem Text="<%$ Resources:C1BubbleChart, OutsideLabel_CompassSouth %>" Value="South"></asp:ListItem>
			<asp:ListItem Text="<%$ Resources:C1BubbleChart, OutsideLabel_CompassEast %>" Value="East"></asp:ListItem>
			<asp:ListItem Text="<%$ Resources:C1BubbleChart, OutsideLabel_CompassWest %>" Value="West"></asp:ListItem>
			</asp:DropDownList></li>
		<li><asp:CheckBox ID="CbVisible" runat="server" Checked="True" Text="<%$ Resources:C1BubbleChart, OutsideLabel_VisibleText %>" /></li>
	</ul></div>
	<div class="settingcontrol">
	<asp:Button ID="ApplyBtn" runat="server" CssClass="settingapply" OnClick="ApplyBtn_Click" Text="<%$ Resources:C1BubbleChart, OutsideLabel_ApplyText %>" />
	</div>
</div>
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
