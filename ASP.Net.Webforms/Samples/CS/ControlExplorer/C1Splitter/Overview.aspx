<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.cs" Inherits="ControlExplorer.C1Splitter.Overview" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Splitter" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        .splitterContainer
        {
            height: 210px;
        }
        
        .layout
        {
            float: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="splitterContainer">
        <div class="layout">
            <h3><%= Resources.C1Splitter.Overview_Vertical %></h3>
            <h4>
                <%= Resources.C1Splitter.Overview_CollapsingPanel1 %></h4>
            <wijmo:C1Splitter runat="server" ID="C1Splitter1" Orientation="Vertical" Width="200px" Height="200px">
                <Panel1>
                    <ContentTemplate>
                        <div>panel1</div>
                    </ContentTemplate>
                </Panel1>
                <Panel2>
                    <ContentTemplate>
                        <div>panel2</div>
                    </ContentTemplate>
                </Panel2>
            </wijmo:C1Splitter>
            <h4>
                <%= Resources.C1Splitter.Overview_CollapsingPanel2 %></h4>
            <wijmo:C1Splitter runat="server" ID="C1Splitter3" Orientation="Vertical" Width="200px" CollapsingPanel="Panel2" Height="200px">
                <Panel1>
                    <ContentTemplate>
                        <div>panel1</div>
                    </ContentTemplate>
                </Panel1>
                <Panel2>
                    <ContentTemplate>
                        <div>panel2</div>
                    </ContentTemplate>
                </Panel2>
            </wijmo:C1Splitter>
        </div>
        <div class="layout" style="width: 100px;">
            &nbsp;</div>
        <div class="layout">
            <h3><%= Resources.C1Splitter.Overview_Horizontal %></h3>
            <h4>
                <%= Resources.C1Splitter.Overview_CollapsingPanel1 %></h4>
            <wijmo:C1Splitter runat="server" ID="C1Splitter2" Orientation="Horizontal" Width="200px" Height="200px">
                <Panel1>
                    <ContentTemplate>
                        <div>panel1</div>
                    </ContentTemplate>
                </Panel1>
                <Panel2>
                    <ContentTemplate>
                        <div>panel2</div>
                    </ContentTemplate>
                </Panel2>
            </wijmo:C1Splitter>
            <h4>
                <%= Resources.C1Splitter.Overview_CollapsingPanel2 %></h4>
            <wijmo:C1Splitter runat="server" ID="C1Splitter4" Orientation="Horizontal" Width="200px" CollapsingPanel="Panel2" Height="200px">
                <Panel1>
                    <ContentTemplate>
                        <div>panel1</div>
                    </ContentTemplate>
                </Panel1>
                <Panel2>
                    <ContentTemplate>
                        <div>panel2</div>
                    </ContentTemplate>
                </Panel2>
            </wijmo:C1Splitter>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Splitter.Overview_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
