<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Nested.aspx.cs" Inherits="ControlExplorer.C1Splitter.Nested" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Splitter" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script type="text/javascript">
        function refresh() {
            $("#<%=hsplitter.ClientID%>").c1splitter("refresh");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1Splitter ID="vsplitter" runat="server" OnClientExpanded="refresh" OnClientCollapsed="refresh" OnClientSized="refresh">
        <Panel2>
            <ContentTemplate>
                <wijmo:C1Splitter ID="hsplitter" runat="server" FullSplit="True" Orientation="Horizontal">
                </wijmo:C1Splitter>
            </ContentTemplate>
        </Panel2>
    </wijmo:C1Splitter>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Splitter.Nested_Text0 %></p>
    <p><%= Resources.C1Splitter.Nested_Text1 %></p>
    <ul>
        <li>ContentTemplate</li>
        <li>FullSplit</li>
        <li>Orientation</li>
        <li>OnClientExpanded</li>
        <li>OnClientCollapsed</li>
        <li>OnClientSized</li>
    </ul>
    <p><%= Resources.C1Splitter.Nested_Text2 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
