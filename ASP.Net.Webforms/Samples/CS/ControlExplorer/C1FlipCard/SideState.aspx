<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
    Inherits="FlipCard_SideState" CodeBehind="SideState.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1FlipCard"
    TagPrefix="C1FlipCard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <C1FlipCard:C1FlipCard ID="FlipCard1" runat="server" Width="50" Height="50" OnSideChanged="FlipCard1_SideChanged">
        <FrontSide><%= Resources.C1FlipCard.SideState_FronSide1 %></FrontSide>
        <BackSide><%= Resources.C1FlipCard.SideState_BackSide1 %></BackSide>
    </C1FlipCard:C1FlipCard>
    <br />
    <asp:Button ID="Button1" runat="server" Text="<%$ Resources:C1FlipCard, SideState_Post %>" OnClick="Button1_Click" />
    <br />
    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1FlipCard.SideState_Text0 %></p>
    <p><%= Resources.C1FlipCard.SideState_Text1 %></p>
</asp:Content>
