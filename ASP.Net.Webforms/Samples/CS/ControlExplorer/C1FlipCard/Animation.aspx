<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
    Inherits="FlipCard_Animation" CodeBehind="Animation.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1FlipCard"
    TagPrefix="C1FlipCard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <C1FlipCard:C1FlipCard ID="FlipCard1" runat="server">
        <Animation Direction="Vertical" Duration="800" />
        <FrontSide>
            <%= Resources.C1FlipCard.Animation_FrontSide1 %>
        </FrontSide>
        <BackSide>
            <%= Resources.C1FlipCard.Animation_BackSide1 %>
        </BackSide>
    </C1FlipCard:C1FlipCard>
    <br />
    <C1FlipCard:C1FlipCard ID="C1FlipCard2" runat="server" TriggerEvent="MouseEnter">
        <Animation Duration="800" />
        <FrontSide>
            <%= Resources.C1FlipCard.Animation_FrontSide2 %>
        </FrontSide>
        <BackSide>
            <%= Resources.C1FlipCard.Animation_BackSide2 %>
        </BackSide>
    </C1FlipCard:C1FlipCard>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1FlipCard.Animation_Text0 %></p>
    <p><%= Resources.C1FlipCard.Animation_Text1 %></p>
</asp:Content>
