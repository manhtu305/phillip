<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
    CodeFile="TrendlineDataBind.aspx.cs" Inherits="C1BarChart_TrendlineDataBinding" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
   <script type = "text/javascript">
	  $(document).ready(function () {
	  	$("text.wijbarchart-label").attr("text-anchor", 'start');
	  	$("text.wijbarchart-label").css("text-anchor", 'start');
	  });
	</script>
    <wijmo:C1BarChart ID="C1BarChart1" runat="server" DataSourceID="AccessDataSource1" Height="475" Width="756">
        <Hint>
            <Content Function="hintContent" />
        </Hint>
        <Header Text="<%$ Resources:C1BarChart, TrendlineDataBind_HeaderText %>"></Header>
        <SeriesStyles>
            <wijmo:ChartStyle Stroke="#7fc73c" Opacity="0.8">
                <Fill Color="#8ede43"></Fill>
            </wijmo:ChartStyle>
        </SeriesStyles>
        <SeriesHoverStyles>
            <wijmo:ChartStyle StrokeWidth="1.5" Opacity="1" />
        </SeriesHoverStyles>
        <DataBindings>
            <wijmo:C1ChartBinding XField="CategoryName" XFieldType="String" YField="Sales" YFieldType="Number" />
            <wijmo:C1ChartBinding IsTrendline="true" TrendlineFitType="Polynom" TrendlineOrder="4" TrendlineSampleCount="100" XField="CategoryName" XFieldType="String" YField="Sales" YFieldType="Number" />
        </DataBindings>
    </wijmo:C1BarChart>
    <script type="text/javascript">
        function hintContent() {
            return this.data.label + '\n' + this.y + '';
        }
    </script>
    <asp:AccessDataSource ID="AccessDataSource1" runat="server" DataFile="~/App_Data/C1NWind.mdb"
        SelectCommand="select CategoryName, sum(ProductSales) as Sales from (SELECT DISTINCTROW Categories.CategoryName as CategoryName, Products.ProductName, Sum([Order Details Extended].ExtendedPrice) AS ProductSales
FROM Categories INNER JOIN (Products INNER JOIN (Orders INNER JOIN [Order Details Extended] ON Orders.OrderID = [Order Details Extended].OrderID) ON Products.ProductID = [Order Details Extended].ProductID) ON Categories.CategoryID = Products.CategoryID
WHERE (((Orders.OrderDate) Between #1/1/95# And #12/31/95#))
GROUP BY Categories.CategoryID, Categories.CategoryName, Products.ProductName
ORDER BY Products.ProductName) group by CategoryName;"></asp:AccessDataSource>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p><%= Resources.C1BarChart.TrendlineDataBind_Text0 %></p>
    <ul>
        <li><%= Resources.C1BarChart.TrendlineDataBind_Li1 %></li>
        <li><%= Resources.C1BarChart.TrendlineDataBind_Li2 %></li>
        <li><%= Resources.C1BarChart.TrendlineDataBind_Li3 %></li>
    </ul>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>

