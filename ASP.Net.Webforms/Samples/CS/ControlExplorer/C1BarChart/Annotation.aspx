<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Annotation.aspx.cs" Inherits="ControlExplorer.C1BarChart.Annotation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1BarChart runat="server" ID="C1BarChart1" Horizontal="false" Height="450" Width="765">
		<Hint IsContentHtml="true">
			<Content Function="hintContent" />
			<ContentStyle FontSize="14px">
				<Fill Color="#CCCCCC">
				</Fill>
			</ContentStyle>
			<Style StrokeWidth="0">
			</Style>
			<HintStyle StrokeWidth="0">
			</HintStyle>
		</Hint>
		<Footer Compass="South" Visible="False"></Footer>
		<Legend Visible="True" Compass="North" Orientation="Horizontal">
			<TextStyle StrokeWidth="0">
			</TextStyle>
		</Legend>
		<Axis>
			<X>
				<GridMajor Visible="True"></GridMajor>

				<GridMinor Visible="False"></GridMinor>
			</X>

			<Y Text="<%$ Resources:C1BarChart, Annotation_AxisYText %>" Compass="West" AnnoFormatString="n0" Max="4000" Min="0" AutoMax="False" AutoMin="False" Alignment="Far">
				<GridMajor Visible="True"></GridMajor>

				<GridMinor Visible="False"></GridMinor>
			</Y>
		</Axis>
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#94C3E8">
				<Fill Color="#94C3E8">
				</Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#FBBA69">
				<Fill Color="#FBBA69">
				</Fill>
			</wijmo:ChartStyle>
		</SeriesStyles>
		<SeriesHoverStyles>
			<wijmo:ChartStyle StrokeWidth="0">
			</wijmo:ChartStyle>
		</SeriesHoverStyles>
		<TextStyle FontSize="13px">
		</TextStyle>
		<Header Text="<%$ Resources:C1BarChart, Annotation_HeaderText %>"></Header>
		<SeriesList>
			<wijmo:BarChartSeries Label="Domestic" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData DateTimeValue="2014-01-01" />
							<wijmo:ChartXData DateTimeValue="2014-02-01" />
							<wijmo:ChartXData DateTimeValue="2014-03-01" />
							<wijmo:ChartXData DateTimeValue="2014-04-01" />
							<wijmo:ChartXData DateTimeValue="2014-05-01" />
							<wijmo:ChartXData DateTimeValue="2014-06-01" />
							<wijmo:ChartXData DateTimeValue="2014-07-01" />
							<wijmo:ChartXData DateTimeValue="2014-08-01" />
							<wijmo:ChartXData DateTimeValue="2014-09-01" />
							<wijmo:ChartXData DateTimeValue="2014-10-01" />
							<wijmo:ChartXData DateTimeValue="2014-11-01" />
							<wijmo:ChartXData DateTimeValue="2014-12-01" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1983" />
							<wijmo:ChartYData DoubleValue="2343" />
							<wijmo:ChartYData DoubleValue="2593" />
							<wijmo:ChartYData DoubleValue="2283" />
							<wijmo:ChartYData DoubleValue="2574" />
							<wijmo:ChartYData DoubleValue="2838" />
							<wijmo:ChartYData DoubleValue="2382" />
							<wijmo:ChartYData DoubleValue="2634" />
							<wijmo:ChartYData DoubleValue="2938" />
							<wijmo:ChartYData DoubleValue="2739" />
							<wijmo:ChartYData DoubleValue="2983" />
							<wijmo:ChartYData DoubleValue="3493" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries LegendEntry="True" Label="International">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData DateTimeValue="2014-01-01" />
							<wijmo:ChartXData DateTimeValue="2014-02-01" />
							<wijmo:ChartXData DateTimeValue="2014-03-01" />
							<wijmo:ChartXData DateTimeValue="2014-04-01" />
							<wijmo:ChartXData DateTimeValue="2014-05-01" />
							<wijmo:ChartXData DateTimeValue="2014-06-01" />
							<wijmo:ChartXData DateTimeValue="2014-07-01" />
							<wijmo:ChartXData DateTimeValue="2014-08-01" />
							<wijmo:ChartXData DateTimeValue="2014-09-01" />
							<wijmo:ChartXData DateTimeValue="2014-10-01" />
							<wijmo:ChartXData DateTimeValue="2014-11-01" />
							<wijmo:ChartXData DateTimeValue="2014-12-01" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="574" />
							<wijmo:ChartYData DoubleValue="636" />
							<wijmo:ChartYData DoubleValue="673" />
							<wijmo:ChartYData DoubleValue="593" />
							<wijmo:ChartYData DoubleValue="644" />
							<wijmo:ChartYData DoubleValue="679" />
							<wijmo:ChartYData DoubleValue="593" />
							<wijmo:ChartYData DoubleValue="139" />
							<wijmo:ChartYData DoubleValue="599" />
							<wijmo:ChartYData DoubleValue="583" />
							<wijmo:ChartYData DoubleValue="602" />
							<wijmo:ChartYData DoubleValue="690" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
		</SeriesList>

		<Indicator Visible="False"></Indicator>
		<Annotations>
			<wijmo:TextAnnotation Attachment="Relative" Offset="0, 0" Text="Relative" 
				Tooltip="<%$ Resources:C1BarChart, Annotation_Tooltip1 %>">
				<Point>
					<X DoubleValue="0.75" />
					<Y DoubleValue="0.15" />
				</Point>
				<AnnotationStyle FontSize="26" FontWeight="Bold">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:TextAnnotation>
			<wijmo:RectAnnotation Attachment="DataIndex" Content="DataIndex" Width="100" Offset="0, 0" Position="Center, Bottom" 
				PointIndex="10" SeriesIndex="0"
				Tooltip="<%$ Resources:C1BarChart, Annotation_Tooltip2 %>">
				<AnnotationStyle FillOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:RectAnnotation>
			<wijmo:EllipseAnnotation Attachment="Relative" Content="Relative" Height="35" Offset="0, 0"
				Tooltip="<%$ Resources:C1BarChart, Annotation_Tooltip3 %>" Width="75">
				<Point>
					<X DoubleValue="0.4" />
					<Y DoubleValue="0.45" />
				</Point>
				<AnnotationStyle FillOpacity="1" Stroke="#c2955f" StrokeWidth="2" StrokeOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:EllipseAnnotation>
			<wijmo:CircleAnnotation Attachment="DataIndex" PointIndex="7" Radius="40" SeriesIndex="0" Content="DataIndex" Offset="0, 0"
				Tooltip="<%$ Resources:C1BarChart, Annotation_Tooltip4 %>">
				<AnnotationStyle FillOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:CircleAnnotation>
			<wijmo:ImageAnnotation Attachment="DataIndex" PointIndex="0" Href="./../Images/c1icon.png" Offset="0, 0" Content=" "
				Tooltip="<%$ Resources:C1BarChart, Annotation_Tooltip5 %>">
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:ImageAnnotation>
			<wijmo:PolygonAnnotation Offset="0, 0" Tooltip="This is polygon annotation.</br>Points: [(200,0),(150,50),(175,100),(255,100),(250, 50)]</br> Attachment: Absolute" Content="Absolute">
					<Points>
						<wijmo:PointF X="200" Y="0" />
						<wijmo:PointF X="150" Y="50" />
						<wijmo:PointF X="175" Y="100" />
						<wijmo:PointF X="225" Y="100" />
						<wijmo:PointF X="250" Y="50" />
					</Points>
				<AnnotationStyle FillOpacity="2" Stroke="#01A9DB" StrokeWidth="4" StrokeOpacity="1">
					<Fill Color="#FFFFFF"> </Fill >
					</AnnotationStyle>
				</wijmo:PolygonAnnotation>
			<wijmo:SquareAnnotation Attachment="DataIndex" Width="40" Content="DataIndex" Offset="0, 0" PointIndex="9" SeriesIndex="1"
				Tooltip="<%$ Resources:C1BarChart, Annotation_Tooltip6 %>">
				<AnnotationStyle FillOpacity="1">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:SquareAnnotation>
			<wijmo:LineAnnotation Content="Absolute" End="350, 300" Offset="0, 0" Start="50, 150"
				Tooltip="<%$ Resources:C1BarChart, Annotation_Tooltip7 %>">
				<AnnotationStyle FillOpacity="2" StrokeWidth="4" Stroke="#01A9DB">
					<Fill Color="#e6e6e6" > </Fill >
				</AnnotationStyle>
			</wijmo:LineAnnotation>
		</Annotations>

	</wijmo:C1BarChart>
	<script type="text/javascript">
		function hintContent() {
			//Check if multiple data points are on one axis entry. For example, multiple data entries for a single date. 
			if ($.isArray(this)) {
				var content = "";
				//Multiple entries of data on this point, so we need to loop through them to create the tooltip content.
				for (var i = 0; i < this.length; i++) {
					content += this[i].label + ': ' + Globalize.format(this[i].y * 1000, 'c0') + '\n';
				}
				return content;
			}
			else {
				//Only a single data point, so we return a formatted version of it. "/n" is a line break.
				return this.data.label + '\n' +
					//Format x as Short Month and long year (Jan 2010). Then format y value as calculated currency with no decimal ($1,983,000). 
					Globalize.format(this.x, 'MMM yyyy') + ': ' + Globalize.format(this.y * 1000, 'c0');
			}
		}
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1BarChart.Annotation_Text0 %></p>
	<h3><%= Resources.C1BarChart.Annotation_Text1 %></h3>
	<ul>
		<li><%= Resources.C1BarChart.Annotation_Li1 %></li>
		<li><%= Resources.C1BarChart.Annotation_Li2 %></li>
	</ul>
</asp:Content>
