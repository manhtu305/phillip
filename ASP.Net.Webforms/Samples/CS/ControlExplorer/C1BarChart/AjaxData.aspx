<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" 
	CodeFile="AjaxData.aspx.cs" Inherits="C1BarChart_AjaxData" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<wijmo:C1BarChart runat = "server" ID="C1BarChart1" Height="475" Width = "756">
		<Hint>
			<Content Function="hintContent" />
		</Hint>
		<Axis>
			<Y Text="<%$ Resources:C1BarChart, AjaxData_AxisYText %>" AutoMin="true" AutoMax="true" Compass="West"></Y>
			<X Text="<%$ Resources:C1BarChart, AjaxData_AxisXText %>"></X>
		</Axis>
		<Header Text="<%$ Resources:C1BarChart, AjaxData_HeaderText %>"></Header>
	</wijmo:C1BarChart>
	<script type = "text/javascript">
	  function hintContent() {
	    return this.data.label + '\n ' + this.y + '';
	  }
	  $(document).ready(function () {
	    $.support.cors = true;
	    var oData = "https://demos.componentone.com/aspnet/Northwind/northwind.svc/Products?$format=json&$top=10&$orderby=Unit_Price%20desc";

	    $.ajax({
	      crossDomain: true,
	      header: { "content-type": "application/javascript" },
	      url: oData,
	      jsonp: "$callback",
	      success: callback
	    });
	  });

	  function callback(result) {
	    // unwrap result
	    var names = [];
	    var prices = [];

	    var products = result["d"];

	    for (var i = 0; i < products.length; i++) {

	      names.push(products[i].Product_Name);
	      prices.push(products[i].Unit_Price);
	    }

	    $("#<%= C1BarChart1.ClientID %>").c1barchart("option", "seriesList", [
					{
					  label: "Prices",
					  legendEntry: true,
					  data: {
					    x: names,
					    y: prices
					  }
					}
	    ]);
	    $("text.wijbarchart-label").attr("text-anchor", 'start');
	    $("text.wijbarchart-label").css("text-anchor", 'start');
    }
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p><%= Resources.C1BarChart.AjaxData_Text0 %></p>
	<ul>
		<li><%= Resources.C1BarChart.AjaxData_Li1 %></li>
		
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

