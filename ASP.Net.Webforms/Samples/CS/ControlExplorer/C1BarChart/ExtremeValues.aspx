<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1BarChart_ExtremeValues" Codebehind="ExtremeValues.aspx.cs" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
  	<script type = "text/javascript">
	  $(document).ready(function () {
	  	$("text.wijbarchart-label").attr("text-anchor", 'start');
	  	$("text.wijbarchart-label").css("text-anchor", 'start');
	  });
	</script>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
	<wijmo:C1BarChart runat = "server" ID="C1BarChart1" Height="475" Width = "756">
		<Hint>
			<Content Function="hintContent" />
		</Hint>

<Footer Compass="South" Visible="False"></Footer>

		<Legend Visible="False"></Legend>

		<Axis>
			<Y Text="<%$ Resources:C1BarChart, ExtremeValues_AxisYText %>" Compass="West">
<GridMajor Visible="True"></GridMajor>

<GridMinor Visible="False"></GridMinor>
			</Y>
			<X Text="">
<GridMajor Visible="True"></GridMajor>

<GridMinor Visible="False"></GridMinor>
			</X>
		</Axis>
		<Header Text="<%$ Resources:C1BarChart, ExtremeValues_HeaderText %>"></Header>
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#7fc73c" Opacity="0.8">
				<Fill Color="#1AD11A"></Fill>
			</wijmo:ChartStyle>
		</SeriesStyles>
		<SeriesHoverStyles>
			<wijmo:ChartStyle StrokeWidth="1.5" Opacity="1" />
		</SeriesHoverStyles>
		<SeriesList>
			<wijmo:BarChartSeries Label="US" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="PS3" />
							<wijmo:ChartXData StringValue="XBOX360" />
							<wijmo:ChartXData StringValue="Wii" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="12.35" />
							<wijmo:ChartYData DoubleValue="21.50" />
							<wijmo:ChartYData DoubleValue="30.56" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
		</SeriesList>
	</wijmo:C1BarChart>
	<script type="text/javascript">
			function hintContent() {
				return this.data.label + '\n ' + this.y + '';
			}
	</script>
	</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p><%= Resources.C1BarChart.ExtremeValues_Text0 %></p>
<h3><%= Resources.C1BarChart.ExtremeValues_Text1 %></h3>
<ul>
    <li><%= Resources.C1BarChart.ExtremeValues_Li1 %></li>
    <li><%= Resources.C1BarChart.ExtremeValues_Li2 %></li>
    <li><%= Resources.C1BarChart.ExtremeValues_Li3 %></li>
    <li><%= Resources.C1BarChart.ExtremeValues_Li4 %></li>
</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
<ContentTemplate>
	<div class="settingcontainer">
<div class="settingcontent">
	<ul>
		<li class="narrowcheckbox"><asp:CheckBox ID="AutoMinCkb" runat="server" Checked="True" Text="<%$ Resources:C1BarChart, ExtremeValues_EnableAutoMin %>" /></li>
		<li><%= Resources.C1BarChart.ExtremeValues_MinValue%> <asp:TextBox id="MinValueTxt" runat="server" Text="0"/></li>
		<li class="narrowcheckbox"><asp:CheckBox ID="AutoMaxCkb" runat="server" Checked="True" Text="<%$ Resources:C1BarChart, ExtremeValues_EnableAutoMax %>" /></li>
		<li><%= Resources.C1BarChart.ExtremeValues_MaxValue %><asp:TextBox id="MaxValueTxt" runat="server" Text="35"/></li>
	</ul>
	</div>
	<div class="settingcontrol">
	<asp:Button ID="Button1" Text="<%$ Resources:C1BarChart, ExtremeValues_Apply %>" CssClass="settingapply" runat="server" OnClick="ApplyBtn_Click"/>
	</div>
</div>
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>

