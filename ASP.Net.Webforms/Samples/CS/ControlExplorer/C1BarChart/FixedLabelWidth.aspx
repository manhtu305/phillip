<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" 
	CodeFile="FixedLabelWidth.aspx.cs" Inherits="C1BarChart_FixedLabelWidth" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<script type = "text/javascript">
	  $(document).ready(function () {
	  	$("text.wijbarchart-label").attr("text-anchor", 'start');
	  	$("text.wijbarchart-label").css("text-anchor", 'start');
	  });
	</script>

	<wijmo:C1BarChart runat = "server" ID="C1BarChart1" Height="475" Width = "756">
		<Axis>
			<Y Text="<%$ Resources:C1BarChart, FixedLabelWidth_AxisYText %>" Compass="West">	
			</Y>
			<X Text="<%$ Resources:C1BarChart, FixedLabelWidth_AxisXText %>">
				<Labels Width="80" TextAlign="Center">
					<AxisLabelStyle Rotation="-30"></AxisLabelStyle>					
				</Labels>
			</X>
		</Axis>
		<Legend Text="Month"></Legend>
		<Header Text="<%$ Resources:C1BarChart, FixedLabelWidth_HeaderText %>"></Header>
		<SeriesStyles>
			<wijmo:ChartStyle Opacity="0.8" Stroke="#2d2d2d" StrokeWidth="1.5">
				<Fill Type="LinearGradient" ColorBegin="#333333" ColorEnd="#2d2d2d"></Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Opacity="0.8" Stroke="#5f9996" StrokeWidth="1.5">
				<Fill Type="LinearGradient" ColorBegin="#6aaba7" ColorEnd="#5f9996"></Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Opacity="0.8" Stroke="#afe500" StrokeWidth="1.5">
				<Fill Type="LinearGradient" ColorBegin="#c3ff00" ColorEnd="#afe500"></Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Opacity="0.8" Stroke="#b2c76d" StrokeWidth="1.5">
				<Fill Type="LinearGradient" ColorBegin="#c7de7a" ColorEnd="#b2c76d"></Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Opacity="0.8" Stroke="#959595" StrokeWidth="1.5">
				<Fill Type="LinearGradient" ColorBegin="#a6a6a6" ColorEnd="#959595"></Fill>
			</wijmo:ChartStyle>
		</SeriesStyles>
		<SeriesHoverStyles>
			<wijmo:ChartStyle Opacity="1" StrokeWidth="1.5" />
			<wijmo:ChartStyle Opacity="1" StrokeWidth="1.5" />
			<wijmo:ChartStyle Opacity="1" StrokeWidth="1.5" />
			<wijmo:ChartStyle Opacity="1" StrokeWidth="1.5" />
			<wijmo:ChartStyle Opacity="1" StrokeWidth="1.5" />
		</SeriesHoverStyles>
		<SeriesList>
			<wijmo:BarChartSeries Label="May" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Mobile Intel 4 Series Express" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 8600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTS 150" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 9600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTX 260" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1.91" />
							<wijmo:ChartYData DoubleValue="1.90" />
							<wijmo:ChartYData DoubleValue="1.61" />
							<wijmo:ChartYData DoubleValue="2.23" />
							<wijmo:ChartYData DoubleValue="2.85" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries Label="Jun" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Mobile Intel 4 Series Express" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 8600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTS 150" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 9600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTX 260" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1.82" />
							<wijmo:ChartYData DoubleValue="1.88" />
							<wijmo:ChartYData DoubleValue="1.77" />
							<wijmo:ChartYData DoubleValue="2.33" />
							<wijmo:ChartYData DoubleValue="2.97" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries Label="Jul" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Mobile Intel 4 Series Express" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 8600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTS 150" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 9600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTX 260" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1.94" />
							<wijmo:ChartYData DoubleValue="1.80" />
							<wijmo:ChartYData DoubleValue="1.81" />
							<wijmo:ChartYData DoubleValue="2.23" />
							<wijmo:ChartYData DoubleValue="2.83" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries Label="Aug" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Mobile Intel 4 Series Express" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 8600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTS 150" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 9600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTX 260" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1.89" />
							<wijmo:ChartYData DoubleValue="1.84" />
							<wijmo:ChartYData DoubleValue="1.96" />
							<wijmo:ChartYData DoubleValue="2.29" />
							<wijmo:ChartYData DoubleValue="2.93" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries Label="Sep" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Mobile Intel 4 Series Express" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 8600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTS 150" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 9600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTX 260" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1.72" />
							<wijmo:ChartYData DoubleValue="1.80" />
							<wijmo:ChartYData DoubleValue="2.17" />
							<wijmo:ChartYData DoubleValue="2.40" />
							<wijmo:ChartYData DoubleValue="3.30" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
		</SeriesList>
	</wijmo:C1BarChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p><%= Resources.C1BarChart.FixedLabelWidth_Text0 %></p>
	<p><%= Resources.C1BarChart.FixedLabelWidth_Text1 %></p>
	<ul>
		<li><%= Resources.C1BarChart.FixedLabelWidth_Li1 %></li>
		<li><%= Resources.C1BarChart.FixedLabelWidth_Li2 %></li>
		<li><%= Resources.C1BarChart.FixedLabelWidth_Li3 %></li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">

</asp:Content>


