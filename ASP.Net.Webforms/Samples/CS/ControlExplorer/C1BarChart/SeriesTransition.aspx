<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1BarChart_SeriesTransition" Codebehind="SeriesTransition.aspx.cs" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<input type="button" value="<%= Resources.C1BarChart.SeriesTransition_Reload %>" onclick="reload()"/>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
	<wijmo:C1BarChart runat = "server" ID="C1BarChart1" ClusterRadius = "5" Height="475" Width = "756">
		<Hint>
			<Content Function="hintContent" />
		</Hint>
		<SeriesList>
			<wijmo:BarChartSeries Label="2010" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="January" />
							<wijmo:ChartXData StringValue="February" />
							<wijmo:ChartXData StringValue="March" />
							<wijmo:ChartXData StringValue="April" />
							<wijmo:ChartXData StringValue="May" />
							<wijmo:ChartXData StringValue="June" />
							<wijmo:ChartXData StringValue="July" />
							<wijmo:ChartXData StringValue="August" />
							<wijmo:ChartXData StringValue="September" />
							<wijmo:ChartXData StringValue="October" />
							<wijmo:ChartXData StringValue="November" />
							<wijmo:ChartXData StringValue="December" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="81" />
							<wijmo:ChartYData DoubleValue="95" />
							<wijmo:ChartYData DoubleValue="21" />
							<wijmo:ChartYData DoubleValue="88" />
							<wijmo:ChartYData DoubleValue="12" />
							<wijmo:ChartYData DoubleValue="23" />
							<wijmo:ChartYData DoubleValue="62" />
							<wijmo:ChartYData DoubleValue="79" />
							<wijmo:ChartYData DoubleValue="90" />
							<wijmo:ChartYData DoubleValue="62" />
							<wijmo:ChartYData DoubleValue="69" />
							<wijmo:ChartYData DoubleValue="46" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
		</SeriesList>
		<Axis>
			<Y Text = "<%$ Resources:C1BarChart, SeriesTransition_AxisYText %>" AutoMax = "false" AutoMin = "false" Max = "100" Min = "0" Compass="West">
			</Y>
			<X Text = "<%$ Resources:C1BarChart, SeriesTransition_AxisXText %>">
			</X>
		</Axis>
	</wijmo:C1BarChart>
	<script type="text/javascript">
		function hintContent() {
			return this.data.label + '\n' + this.y + '';
		}
		$(document).ready(function () {
		  $("text.wijbarchart-label").attr("text-anchor", 'start');
		  $("text.wijbarchart-label").css("text-anchor", 'start');
		});
		function reload() {
			$("#<%= C1BarChart1.ClientID %>").c1barchart("option", "seriesList", [createRandomSeriesList("2010")]);
		}

		function createRandomSeriesList(label) {
			var data = [],
				randomDataValuesCount = 12,
				labels = ["January", "February", "March", "April", "May", "June",
					"July", "August", "September", "October", "November", "December"],
				idx;
			for (idx = 0; idx < randomDataValuesCount; idx++) {
				data.push(createRandomValue());
			}
			return {
				label: label,
				legendEntry: true,
				data: { x: labels, y: data }
			};
		}

		function createRandomValue() {
			return Math.round(Math.random() * 100);
		}
	</script>
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p><%= Resources.C1BarChart.SeriesTransition_Text0 %></p>
	<p><%= Resources.C1BarChart.SeriesTransition_Text1 %></p>
	<ul>
		<li><%= Resources.C1BarChart.SeriesTransition_Li1 %></li>
		<li><%= Resources.C1BarChart.SeriesTransition_Li2 %></li>
		<li><%= Resources.C1BarChart.SeriesTransition_Li3 %></li>
	</ul>
	<p><%= Resources.C1BarChart.SeriesTransition_Text2 %></p>
	<ul>
		<li>EaseInCubic</li>
		<li>EaseOutCubic</li>
		<li>EaseInOutCubic</li>
		<li>EaseInBack</li>
		<li>EaseOutBack</li>
		<li>EaseOutElastic</li>
		<li>EaseOutBounce</li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
		<asp:UpdatePanel ID="UpdatePanel2" runat="server">
			<ContentTemplate>
				<div class="settingcontainer">
<div class="settingcontent">
<ul>
	<li class="fullwidth"><label class="settinglegend"><%= Resources.C1BarChart.SeriesTransition_AnimationLabel %></label></li>
	<li class="fullwidth"><asp:CheckBox runat="server" ID="EnabledCk" Checked="true"/><label><%= Resources.C1BarChart.SeriesTransition_EnabledText %></label>
	</li>
	<li>
		<label><%= Resources.C1BarChart.SeriesTransition_EasingLabel %></label>
		<asp:DropDownList id="EasingDdl" runat="server">
			<asp:ListItem value="EaseInCubic" Selected="True">EaseInCubic</asp:ListItem>
			<asp:ListItem value="EaseOutCubic">EaseOutCubic</asp:ListItem>
			<asp:ListItem value="EaseInOutCubic">EaseInOutCubic</asp:ListItem>
			<asp:ListItem value="EaseInBack">EaseInBack</asp:ListItem>
			<asp:ListItem value="EaseOutBack">EaseOutBack</asp:ListItem>
			<asp:ListItem value="EaseOutElastic">EaseOutElastic</asp:ListItem>
			<asp:ListItem value="EaseOutBounce">EaseOutBounce</asp:ListItem>
		</asp:DropDownList>
	</li>
	<li>
		<label><%= Resources.C1BarChart.SeriesTransition_DurationLabel %></label>
		<asp:TextBox runat="server" ID="DurationTxt" Text="250" />
	</li>
</ul>
</div>
	<div class="settingcontrol">
	<asp:Button ID="ApplyBtn" runat="server" CssClass="settingapply" OnClick="ApplyBtn_Click" Text="<%$ Resources:C1BarChart, SeriesTransition_Apply %>" />
	</div>
</div>
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>

