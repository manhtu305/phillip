<%@ Page Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeBehind="Legend.aspx.cs" Inherits="ControlExplorer.C1PieChart.Legend" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="update1" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <wijmo:C1PieChart runat="server" ID="C1PieChart1" Radius="180" Height="450" Width="756" CssClass="ui-widget ui-widget-content ui-corner-all">
                <Hint>
                    <Content Function="hintContent" />
                </Hint>
                <Animation Enabled="false" />
                <Legend Visible="true" Text="<%$ Resources:C1PieChart, Legend_LegendText %>" Compass="North" Orientation="Horizontal">
                    <Size Width="15" Height="10" />
                </Legend>
                <Header Text="<%$ Resources:C1PieChart, Legend_HeaderText %>">
                </Header>
                <SeriesList>
                    <wijmo:PieChartSeries Label="China" Data="1387" Offset="15" LegendEntry="true">
                    </wijmo:PieChartSeries>
                    <wijmo:PieChartSeries Label="India" Data="1255" Offset="0" LegendEntry="true">
                    </wijmo:PieChartSeries>
                    <wijmo:PieChartSeries Label="USA" Data="320" Offset="0" LegendEntry="true">
                    </wijmo:PieChartSeries>
                    <wijmo:PieChartSeries Label="Indonesia" Data="250" Offset="0" LegendEntry="true">
                    </wijmo:PieChartSeries>
                    <wijmo:PieChartSeries Label="Brasil" Data="200" Offset="0" LegendEntry="true">
                    </wijmo:PieChartSeries>
                    <wijmo:PieChartSeries Label="Other world" Data="3770" Offset="0" LegendEntry="true">
                    </wijmo:PieChartSeries>
                </SeriesList>
                <Footer Compass="South" Visible="False">
                </Footer>
                <Axis>
                    <Y Visible="False" Compass="West">
                        <Labels TextAlign="Center">
                        </Labels>
                        <GridMajor Visible="True">
                        </GridMajor>
                    </Y>
                </Axis>
            </wijmo:C1PieChart>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <div>
        <h3><%= Resources.C1PieChart.Legend_Text1 %></h3>
        <ul>
            <li><%= Resources.C1PieChart.Legend_Li1 %></li>
            <li><%= Resources.C1PieChart.Legend_Li2 %></li>
            <li><%= Resources.C1PieChart.Legend_Li3 %></li>
            <li><%= Resources.C1PieChart.Legend_Li4 %></li>
        </ul>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
    <asp:UpdatePanel ID="update2" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div class="settingcontainer">
                <div class="settingcontent">
                    <ul>
                        <li class="fullwidth">
                            <label><%= Resources.C1PieChart.Legend_LegendEnabled %></label>
                            <asp:CheckBox ID="cb1" AutoPostBack="true" Checked="true" runat="server" />
                        </li>
                        <li class="fullwidth">
                            <h3><%= Resources.C1PieChart.Legend_LegendPosition %></h3>
                        </li>
                        <li class="fullwidth">
                            <label><%= Resources.C1PieChart.Legend_Compass %></label>
                            <asp:DropDownList ID="cbbCompass" AutoPostBack="true" runat="server">
                                <asp:ListItem Text="<%$ Resources:C1PieChart, Legend_CompassNorth %>" Value="north" Selected="true" />
                                <asp:ListItem Text="<%$ Resources:C1PieChart, Legend_CompassEast %>" Value="east" />
                                <asp:ListItem Text="<%$ Resources:C1PieChart, Legend_CompassSouth %>" Value="south" />
                                <asp:ListItem Text="<%$ Resources:C1PieChart, Legend_CompassWest %>" Value="west" />
                            </asp:DropDownList>
                            <label><%= Resources.C1PieChart.Legend_Orientation %></label>
                            <asp:DropDownList ID="ccbOrientation" AutoPostBack="true" runat="server">
                                <asp:ListItem Text="<%$ Resources:C1PieChart, Legend_OrientationHorizontal %>" Value="horizontal" Selected="true" />
                                <asp:ListItem Text="<%$ Resources:C1PieChart, Legend_OrientationVertical %>" Value="vertical" />
                            </asp:DropDownList>
                        </li>
                    </ul>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        function hintContent() {
            return this.data.label + " : " + Globalize.format(this.value / this.total, "p2");
        }
    </script>
</asp:Content>
