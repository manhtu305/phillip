<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" 
	CodeFile="DataBinding.aspx.cs" Inherits="C1PieChart_DataBinding" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
	<script type="text/javascript">
		function hintContent() {
		    return this.data.label + " : " + Globalize.format(this.value / this.total, "p2");
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
		<wijmo:C1PieChart ID="C1PieChart1" runat="server" Radius="140" DataSourceID="AccessDataSource1" Height="475" Width = "756" CssClass ="ui-widget ui-widget-content ui-corner-all">
			<Hint>
				<Content Function="hintContent" />
			</Hint>
			<Header Text="<%$ Resources:C1PieChart, DataBinding_HeaderText %>">
			</Header>
			<SeriesStyles>
				<wijmo:ChartStyle StrokeWidth="1.5" Stroke="#AFE500">
					<Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#C3FF00" ColorEnd="#AFE500"></Fill>
				</wijmo:ChartStyle>
				<wijmo:ChartStyle StrokeWidth="1.5" Stroke="#7FC73C">
					<Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#8EDE43" ColorEnd="#7FC73C"></Fill>
				</wijmo:ChartStyle>
				<wijmo:ChartStyle StrokeWidth="1.5" Stroke="#5F9996">
					<Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#6AABA7" ColorEnd="#5F9996"></Fill>
				</wijmo:ChartStyle>
				<wijmo:ChartStyle StrokeWidth="1.5" Stroke="#3E5F77">
					<Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#466A85" ColorEnd="#3E5F77"></Fill>
				</wijmo:ChartStyle>
				<wijmo:ChartStyle StrokeWidth="1.5" Stroke="#959595">
					<Fill Type="LinearGradient" LinearGradientAngle="180" ColorBegin="#A6A6A6" ColorEnd="#959595"></Fill>
				</wijmo:ChartStyle>
			</SeriesStyles>
			<Footer Compass="South" Visible="False">
			</Footer>
			<DataBindings>
				<wijmo:C1PieChartBinding DataField="Sales" LabelField="CategoryName"  />
			</DataBindings>
		</wijmo:C1PieChart>
		<asp:AccessDataSource ID="AccessDataSource1" runat="server" 
			DataFile="~/App_Data/C1NWind.mdb" 
			SelectCommand="select CategoryName, sum(ProductSales) as Sales from (SELECT DISTINCTROW Categories.CategoryName as CategoryName, Products.ProductName, Sum([Order Details Extended].ExtendedPrice) AS ProductSales
FROM Categories INNER JOIN (Products INNER JOIN (Orders INNER JOIN [Order Details Extended] ON Orders.OrderID = [Order Details Extended].OrderID) ON Products.ProductID = [Order Details Extended].ProductID) ON Categories.CategoryID = Products.CategoryID
WHERE (((Orders.OrderDate) Between #1/1/95# And #12/31/95#))
GROUP BY Categories.CategoryID, Categories.CategoryName, Products.ProductName
ORDER BY Products.ProductName) group by CategoryName;">
		</asp:AccessDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p><%= Resources.C1PieChart.DataBinding_Text0 %></p><br/>
	<p><%= Resources.C1PieChart.DataBinding_Text1 %></p>
	<ul>
		<li><%= Resources.C1PieChart.DataBinding_Li1 %></li>
		<li><%= Resources.C1PieChart.DataBinding_Li2 %></li>
		<li><%= Resources.C1PieChart.DataBinding_Li3 %></li>
		<li><%= Resources.C1PieChart.DataBinding_Li4 %></li>
	</ul>
	<p><%= Resources.C1PieChart.DataBinding_Text2 %></p>
	<ul>
		<li><%= Resources.C1PieChart.DataBinding_Li5 %></li>
		<li><%= Resources.C1PieChart.DataBinding_Li6 %></li>
		<li><%= Resources.C1PieChart.DataBinding_Li7 %></li>
	</ul>
	<p><%= Resources.C1PieChart.DataBinding_Text3 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

