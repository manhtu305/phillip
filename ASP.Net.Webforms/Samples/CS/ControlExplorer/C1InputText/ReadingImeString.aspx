<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ReadingImeString.aspx.cs" Inherits="ControlExplorer.C1InputText.ReadingImeString" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function OnReadingImeStringOutput(sender, data) {
            var output = $("#textbox2");
            $("#textbox2").val(output.val() + data.readingString);
        }

        $(function () {
            $("#textbox2").wijtextbox();
        }
        );

    </script>
    <p><%= Resources.C1InputText.ReadingImeString_Text0 %></p>
    <br />
    <p><wijmo:C1InputText ID="C1InputText1" runat="server" ImeMode="Active"  OnClientReadingImeStringOutput="OnReadingImeStringOutput"></wijmo:C1InputText></p>
    <br />
    <input type="text" id="textbox2" readonly="true" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1InputText.ReadingImeString_Text2 %></p>
</asp:Content>
