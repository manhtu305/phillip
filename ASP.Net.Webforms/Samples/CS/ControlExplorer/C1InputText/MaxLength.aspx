<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="MaxLength.aspx.cs" Inherits="ControlExplorer.C1InputText.MaxLength" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <p><wijmo:C1InputText ID="C1InputText1" runat="server" MaxLength="5" Text="12345"></wijmo:C1InputText></p>
    <br />
    <p><%= Resources.C1InputText.MaxLength_Text1 %></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1InputText.MaxLength_Text2 %></p>
</asp:Content>
