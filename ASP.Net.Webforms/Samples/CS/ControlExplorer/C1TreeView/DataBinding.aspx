<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="DataBinding.aspx.cs" Inherits="ControlExplorer.C1TreeView.DataBanding" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1TreeView" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:XmlDataSource ID="XmlDataSource" runat="server" DataFile="~/App_Data/treeview_structure.xml" XPath="root/treeviewnode"></asp:XmlDataSource>
    <asp:SiteMapDataSource ID="SiteMapDataSource" runat="server" ShowStartingNode="False" />

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <wijmo:C1TreeView ID="C1TreeView1" ShowCheckBoxes="true" DataSourceID="SiteMapDataSource" ShowExpandCollapse="true" Width="350px" runat="server">
            </wijmo:C1TreeView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1TreeView.DataBinding_Text0 %></p>
    <p><%= Resources.C1TreeView.DataBinding_Text1 %></p>
    <ul>
        <li><%= Resources.C1TreeView.DataBinding_Li1 %></li>
        <li><%= Resources.C1TreeView.DataBinding_Li2 %></li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="settingcontainer">
                <div class="settingcontent">
                    <ul>
                        <li class="fullwidth"><label class="settinglegend"><%= Resources.C1TreeView.DataBinding_SelectDataBindingSource %></label></li>
                        <li class="fullwidth">
                            <asp:DropDownList ID="dataSource" AutoPostBack="true" runat="server" OnSelectedIndexChanged="dataSource_SelectedIndexChanged" Width="160px">
                                <asp:ListItem Text="<%$ Resources:C1TreeView, DataBinding_SiteMapDataSource %>" Value="SiteMap Data Source"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:C1TreeView, DataBinding_XMLDataSource %>" Value="XML Data Source"></asp:ListItem>
                            </asp:DropDownList>
                        </li>
                    </ul>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
