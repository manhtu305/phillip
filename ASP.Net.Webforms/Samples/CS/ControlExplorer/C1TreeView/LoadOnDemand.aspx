<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="LoadOnDemand.aspx.cs" Inherits="ControlExplorer.C1TreeView.LoadOnDemand" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1TreeView" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SiteMapDataSource ID="SiteMapDataSource" runat="server" ShowStartingNode="False" />
    <wijmo:C1TreeView ID="C1TreeView1" ShowCheckBoxes="true" LoadOnDemand="true" DataSourceID="SiteMapDataSource" ShowExpandCollapse="true" DataBindStartLevel="0" Width="350px" runat="server">
    </wijmo:C1TreeView>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1TreeView.LoadOnDemand_Text0 %></p>
    <p><%= Resources.C1TreeView.LoadOnDemand_Text1 %></p>
    <ul>
        <li><%= Resources.C1TreeView.LoadOnDemand_Li1 %></li>
        <li><%= Resources.C1TreeView.LoadOnDemand_Li2 %></li>
    </ul>
    <p><%= Resources.C1TreeView.LoadOnDemand_Text2 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
