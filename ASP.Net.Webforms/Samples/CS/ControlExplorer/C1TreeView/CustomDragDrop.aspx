<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CustomDragDrop.aspx.cs" Inherits="ControlExplorer.C1TreeView.CustomDragDrop" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1TreeView" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style>
        #trash
        {
            float: right;
            min-height: 12em;
            padding: 1%;
            width: 32%;
        }
        #trash ul
        {
            list-style: none;
        }
         
        #trash ul li
        {
            margin-bottom: 10px;
        }
               
        div.wijmo-wijtree
        {
            float: left;
        }
         
             
        .dropVisual
        {
            height:1px;
            font-size:0px;/*fix ie 6 issue*/
            background-color:Red;
        }    
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="main demo">             
        <div>
            <wijmo:C1TreeView ID="C1TreeView1" AllowDrag="true" AllowDrop="false" runat="server">
                <Nodes>
                    <wijmo:C1TreeViewNode Text="Folder 1">
                        <Nodes>
                            <wijmo:C1TreeViewNode Text="Folder 1.1">
                                <Nodes>
                                    <wijmo:C1TreeViewNode Text="Folder 1.1.1">
                                    </wijmo:C1TreeViewNode>
                                    <wijmo:C1TreeViewNode Text="Folder 1.1.2">
                                    </wijmo:C1TreeViewNode>
                                    <wijmo:C1TreeViewNode Text="Folder 1.1.3">
                                    </wijmo:C1TreeViewNode>
                                    <wijmo:C1TreeViewNode Text="Folder 1.1.4">
                                    </wijmo:C1TreeViewNode>
                                </Nodes>
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 1.2">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 1.3">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 1.4">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 1.5">
                            </wijmo:C1TreeViewNode>
                        </Nodes>
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 2">
                        <Nodes>
                            <wijmo:C1TreeViewNode Text="Folder 2.1">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 2.2">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 2.3">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 2.4">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 2.5">
                            </wijmo:C1TreeViewNode>
                        </Nodes>
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 3">
                        <Nodes>
                            <wijmo:C1TreeViewNode Text="Folder 3.1">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 3.2">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 3.3">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 3.4">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 3.5">
                            </wijmo:C1TreeViewNode>
                        </Nodes>
                    </wijmo:C1TreeViewNode>
                </Nodes>
            </wijmo:C1TreeView>
        </div>
 
           
        <div id="trash" class="ui-widget-content">
            <h4 class="ui-widget-header">
                <span class="ui-icon ui-icon-trash">Trash</span> <%= Resources.C1TreeView.CustomDragDrop_Trash %></h4>
            <ul>
            </ul>
        </div>
        <div style="clear:both"></div>
        <p><%= Resources.C1TreeView.CustomDragDrop_Text0 %></p>
        <div>
            <wijmo:C1TreeView ID="C1TreeView2" runat="server">
                <Nodes>
                    <wijmo:C1TreeViewNode Text="Folder 4">
                        <Nodes>
                            <wijmo:C1TreeViewNode Text="Folder 4.1">
                                <Nodes>
                                    <wijmo:C1TreeViewNode Text="Folder 4.1.1">
                                    </wijmo:C1TreeViewNode>
                                    <wijmo:C1TreeViewNode Text="Folder 4.1.2">
                                    </wijmo:C1TreeViewNode>
                                    <wijmo:C1TreeViewNode Text="Folder 4.1.3">
                                    </wijmo:C1TreeViewNode>
                                    <wijmo:C1TreeViewNode Text="Folder 4.1.4">
                                    </wijmo:C1TreeViewNode>
                                </Nodes>
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 4.2">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 4.3">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 4.4">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 4.5">
                            </wijmo:C1TreeViewNode>
                        </Nodes>
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 5">
                        <Nodes>
                            <wijmo:C1TreeViewNode Text="Folder 5.1">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 5.2">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 5.3">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 5.4">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 5.5">
                            </wijmo:C1TreeViewNode>
                        </Nodes>
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 6">
                        <Nodes>
                            <wijmo:C1TreeViewNode Text="Folder 6.1">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 6.2">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 6.3">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 6.4">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 6.5">
                            </wijmo:C1TreeViewNode>
                        </Nodes>
                    </wijmo:C1TreeViewNode>
                </Nodes>
            </wijmo:C1TreeView>
        </div>
    </div>
    <div style="clear:both"></div>

    <script id="scriptInit" type="text/javascript">
        var span = "<span class=\"ui-widget-content helperInner\">";

        // Get the initial HTML markup of dragged node.
        var getInitMarkup = function (dragNode) {
            var node = dragNode.clone();
            node.find("a").unwrap().unwrap();
            node.find("li>span,>span").remove();
            return node;
        };

        var delTreeNode = function (node, trash) {
            var parent = node.parent()
            .closest(":wijmo-c1treeviewnode,:wijmo-c1treeview");
            if (parent.is(":wijmo-c1treeview")) {
                parent.c1treeview("remove", node);
            }else {
                parent.c1treeviewnode("remove", node);
            }
            node.appendTo($(trash).children("ul:eq(0)"));
        }

        $(function () {
            $("#<%=C1TreeView1.ClientID%>").c1treeview("option", "draggable", {
                revert: "invalid",
                start: function (event, ui) {
                    ui.helper.html(ui.helper.html() + span);
                },
                //Shows the coordinate of dragged-node.
                drag: function (event, ui) {
                    var inner = ui.helper.children(".helperInner");
                    if (inner.length) {
                        inner.html("x:" + event.pageX + " y:" + event.pageX);
                    }
                },
                stop: function (event, ui) {
                    $(this).hide()
                    $(this).show("highlight", 500);
                }
            });

            $("#<%=C1TreeView2.ClientID%>").c1treeview("option", {
                droppable:{
                    //Clone the dragged node and append to the new parent-node.
                    drop: function (event, ui) {
                        var dragNode = ui.draggable,
                                p = ui.newParent,
                                po = ui.newIndex, node;

                        node = getInitMarkup(dragNode);

                        if (p.is("[role='tree']")) {
                            p.c1treeview("add", node, po);
                        }
                        else {
                            p.c1treeviewnode("add", node, po);
                        }
                    }
                },
                allowDrop: true
            });

            $("#trash").droppable({
                activeClass: "ui-state-hover",
                hoverClass: "ui-state-active",
                scope: "tree",
                //Drop the node to "trash".
                drop: function (event, ui) {
                    var node = ui.draggable;
                    delTreeNode(node, this);
                }
            });
        });
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1TreeView.CustomDragDrop_Text1 %></p>
    <p><%= Resources.C1TreeView.CustomDragDrop_Text2 %></p>
    <ul>
        <li><%= Resources.C1TreeView.CustomDragDrop_Li1 %></li>
        <li><%= Resources.C1TreeView.CustomDragDrop_Li2 %></li>
    </ul>
    <p><%= Resources.C1TreeView.CustomDragDrop_Text3 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
