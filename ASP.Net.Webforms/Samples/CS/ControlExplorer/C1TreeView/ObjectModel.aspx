<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ObjectModel.aspx.cs" Inherits="ControlExplorer.C1TreeView.ObjectModel" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1TreeView" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script type="text/javascript">
        function getNode() {
            return $("#<%= C1TreeView1.ClientID %>").c1treeview("findNodeByText", "Folder 1"); ;
        }

        function add() {
            var node = getNode();
            if (node != null)
                node.element.c1treeviewnode("add", $("#addValue").val(), parseInt($("#addIndex").val()));
        }

        function removenode() {
            var node = getNode();
            if (node != null)
                node.element.c1treeviewnode("remove", parseInt($("#removeIndex").val()));
        }

    </script>

    <style type="text/css">
        .settingcontent .settingitem
        {
            width:200px;
        }

        .settingcontent .settingitem label
        {
             width:60px;
        }

        .settingcontent .settingcommand
        {
            width:90px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1TreeView ID="C1TreeView1" runat="server">
        <Nodes>
            <wijmo:C1TreeViewNode Text="Folder 1" Expanded="True">
                <Nodes>
                    <wijmo:C1TreeViewNode Text="Folder 1.1">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 1.2">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 1.3">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 1.4">
                    </wijmo:C1TreeViewNode>
                </Nodes>
            </wijmo:C1TreeViewNode>
        </Nodes>
    </wijmo:C1TreeView>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1TreeView.ObjectModel_Text0 %></p>
    <p><%= Resources.C1TreeView.ObjectModel_Text1 %></p>
    <ul>
        <li>add</li>
        <li>remove</li>
    </ul>
    <p><%= Resources.C1TreeView.ObjectModel_Text2 %></p>
    <ul>
        <li><%= Resources.C1TreeView.ObjectModel_Li1 %></li>
        <li><%= Resources.C1TreeView.ObjectModel_Li2 %></li>
    </ul>
    <p><%= Resources.C1TreeView.ObjectModel_Text3 %></p>
    <ul>
        <li><%= Resources.C1TreeView.ObjectModel_Li3 %></li>
    </ul>
    <p><%= Resources.C1TreeView.ObjectModel_Text4 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li class="fullwidth"><label class="settinglegend"><%= Resources.C1TreeView.ObjectModel_Add %></label></li>
                <li class="settingitem">
                    <label><%= Resources.C1TreeView.ObjectModel_Index %></label>
                    <input id="addIndex" type="text" value="0" />
                </li>
                <li class="settingitem">
                    <label><%= Resources.C1TreeView.ObjectModel_Value %></label>
                    <input id="addValue" type="text" value="New node" />
                </li>
                <li class="settingcommand">
                    <input id="addNode" onclick="add();" type="button" value="<%= Resources.C1TreeView.ObjectModel_AddText %>" />
                </li>

                <li class="fullwidth"><label class="settinglegend"><%= Resources.C1TreeView.ObjectModel_Remove %></label></li>
                <li class="settingitem">
                    <label><%= Resources.C1TreeView.ObjectModel_Index %></label>
                    <input id="removeIndex" type="text" value="0" />
                </li>
                <li class="settingcommand">
                    <input id="removeNode" onclick="removenode();" type="button" value="<%= Resources.C1TreeView.ObjectModel_RemoveText %>" />
                </li>
            </ul>
        </div>
    </div>
</asp:Content>
