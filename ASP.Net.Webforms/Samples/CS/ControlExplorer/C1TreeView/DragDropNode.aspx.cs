﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ControlExplorer.C1TreeView
{
	public partial class DragDropNode : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}
		protected void Confirm_Click(object sender, EventArgs e)
		{
			int nodeDragType = NodeAllowDrag.SelectedIndex;
			int nodeDropType = NodeAllowDrop.SelectedIndex;
			C1.Web.Wijmo.Controls.C1TreeView.C1TreeViewNode node;
			C1TreeView1.AllowDrag = TreeDrag.Checked;
			C1TreeView1.AllowDrop = TreeDrop.Checked;
			if (C1TreeView1.SelectedNodes.Count > 0) {
				node = C1TreeView1.SelectedNodes[0];
				switch (nodeDragType)
				{
					case 0:
						node.AllowDrag = null;
						break;
					case 1:
						node.AllowDrag = true;
						break;
					case 2:
						node.AllowDrag = false;
						break;
				}
				switch (nodeDropType)
				{
					case 0:
						node.AllowDrop = null;
						break;
					case 1:
						node.AllowDrop = true;
						break;
					case 2:
						node.AllowDrop = false;
						break;
				}
			}
			UpdatePanel1.Update();
		}
	}
}