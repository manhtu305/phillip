<%@ Page Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="DragDropNode.aspx.cs" Inherits="ControlExplorer.C1TreeView.DragDropNode" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1TreeView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script>
        var showNodeSettings = function (e, data) {
            var allowDrag, allowDrop,
                nodes = $("#<%=C1TreeView1.ClientID %>").c1treeview("getSelectedNodes"),
                allowDragRadios = $("#<%=NodeAllowDrag.ClientID %>").find("input[type='radio']"),
                allowDropRadios = $("#<%=NodeAllowDrop.ClientID %>").find("input[type='radio']");
            if (nodes.length > 0) {
                allowDrag = nodes[0].options.allowDrag;
                allowDrop = nodes[0].options.allowDrop;
                switch (allowDrag) {
                    case null:
                        allowDragRadios.eq(0).prop("checked", true);
                        break;
                    case true:
                        allowDragRadios.eq(1).prop("checked", true);
                        break;
                    case false:
                        allowDragRadios.eq(2).prop("checked", true);
                        break;
                }
                switch (allowDrop) {
                    case null:
                        allowDropRadios.eq(0).prop("checked", true);
                        break;
                    case true:
                        allowDropRadios.eq(1).prop("checked", true);
                        break;
                    case false:
                        allowDropRadios.eq(2).prop("checked", true);
                        break;
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <wijmo:C1TreeView ID="C1TreeView1" runat="server" AllowDrag="true" AllowDrop="true" OnClientSelectedNodeChanged="showNodeSettings">
                <Nodes>
                    <wijmo:C1TreeViewNode Text="System Folder" AllowDrag="false" AllowDrop="false" ExpandedIconClass="ui-icon-folder-open" CollapsedIconClass="ui-icon-folder-collapsed">
                        <Nodes>
                            <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="System File 1" AllowDrag="false" AllowDrop="false">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="System File 2" AllowDrag="false" AllowDrop="false">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="System File 3" AllowDrag="false" AllowDrop="false">
                            </wijmo:C1TreeViewNode>
                        </Nodes>
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="User Folder" AllowDrag="true" AllowDrop="true" ExpandedIconClass="ui-icon-folder-open" CollapsedIconClass="ui-icon-folder-collapsed">
                        <Nodes>
                            <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="User File 1" AllowDrag="true" AllowDrop="false">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="User File 2" AllowDrag="true" AllowDrop="false">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode ItemIconClass="ui-icon-document" Text="User File 3" AllowDrag="true" AllowDrop="false">
                            </wijmo:C1TreeViewNode>
                        </Nodes>
                    </wijmo:C1TreeViewNode>
                </Nodes>
            </wijmo:C1TreeView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1TreeView.DragDropNode_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div class="settingcontainer">
                <div class="settingcontent">
                    <ul>
                        <li class="fullwidth">
                            <label class="settinglegend"><%= Resources.C1TreeView.DragDropNode_TreeSettings %></label></li>
                        <li>
                            <asp:CheckBox ID="TreeDrag" Checked="true" Text="<%$ Resources:C1TreeView, DragDropNode_AllowDrag %>" runat="server" /></li>
                        <li>
                            <asp:CheckBox ID="TreeDrop" Checked="true" Text="<%$ Resources:C1TreeView, DragDropNode_AllowDrop %>" runat="server" /></li>
                    </ul>
                    <ul>
                        <li class="fullwidth">
                            <label class="settinglegend"><%= Resources.C1TreeView.DragDropNode_SelectedNodeSettings %></label></li>
                        <li>
                            <label><%= Resources.C1TreeView.DragDropNode_AllowDragLabel %></label>
                            <asp:RadioButtonList ID="NodeAllowDrag" runat="server" TextAlign="Left">
                                <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:C1TreeView, DragDropNode_Default %>"></asp:ListItem>
                                <asp:ListItem Value="1" Text="<%$ Resources:C1TreeView, DragDropNode_True %>"></asp:ListItem>
                                <asp:ListItem Value="2" Text="<%$ Resources:C1TreeView, DragDropNode_False %>"></asp:ListItem>
                            </asp:RadioButtonList>
                        </li>
                        <li>
                            <label><%= Resources.C1TreeView.DragDropNode_AllowDropLabel %></label>
                            <asp:RadioButtonList ID="NodeAllowDrop" runat="server" TextAlign="Left">
                                <asp:ListItem Value="0" Selected="True" Text="<%$ Resources:C1TreeView, DragDropNode_Default %>"></asp:ListItem>
                                <asp:ListItem Value="1" Text="<%$ Resources:C1TreeView, DragDropNode_True %>"></asp:ListItem>
                                <asp:ListItem Value="2" Text="<%$ Resources:C1TreeView, DragDropNode_False %>"></asp:ListItem>
                            </asp:RadioButtonList>
                        </li>
                    </ul>
                </div>
                <div class="settingcontrol">
                    <asp:Button ID="Confirm" runat="server" Text="<%$ Resources:C1TreeView, DragDropNode_Apply %>" CssClass="settingapply" OnClick="Confirm_Click" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
