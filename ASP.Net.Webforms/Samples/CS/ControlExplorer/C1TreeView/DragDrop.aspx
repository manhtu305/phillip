<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="DragDrop.aspx.cs" Inherits="ControlExplorer.C1TreeView.DragDrop" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1TreeView" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <wijmo:C1TreeView ID="C1TreeView1" AllowDrag="true" AllowDrop="true" runat="server">
            <Nodes>
                <wijmo:C1TreeViewNode Text="Folder 1">
                    <Nodes>
                        <wijmo:C1TreeViewNode Text="Folder 1.1">
                            <Nodes>
                                <wijmo:C1TreeViewNode Text="Folder 1.1.1">
                                </wijmo:C1TreeViewNode>
                                <wijmo:C1TreeViewNode Text="Folder 1.1.2">
                                </wijmo:C1TreeViewNode>
                                <wijmo:C1TreeViewNode Text="Folder 1.1.3">
                                </wijmo:C1TreeViewNode>
                                <wijmo:C1TreeViewNode Text="Folder 1.1.4">
                                </wijmo:C1TreeViewNode>
                            </Nodes>
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="Folder 1.2">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="Folder 1.3">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="Folder 1.4">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="Folder 1.5">
                        </wijmo:C1TreeViewNode>
                    </Nodes>
                </wijmo:C1TreeViewNode>
                <wijmo:C1TreeViewNode Text="Folder 2">
                    <Nodes>
                        <wijmo:C1TreeViewNode Text="Folder 2.1">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="Folder 2.2">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="Folder 2.3">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="Folder 2.4">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="Folder 2.5">
                        </wijmo:C1TreeViewNode>
                    </Nodes>
                </wijmo:C1TreeViewNode>
                <wijmo:C1TreeViewNode Text="Folder 3">
                    <Nodes>
                        <wijmo:C1TreeViewNode Text="Folder 3.1">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="Folder 3.2">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="Folder 3.3">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="Folder 3.4">
                        </wijmo:C1TreeViewNode>
                        <wijmo:C1TreeViewNode Text="Folder 3.5">
                        </wijmo:C1TreeViewNode>
                    </Nodes>
                </wijmo:C1TreeViewNode>
            </Nodes>
        </wijmo:C1TreeView>
        <p style="color:#333333">
            <%= Resources.C1TreeView.DragDrop_Between2Trees %></p>
    </div>
    <wijmo:C1TreeView ID="C1TreeView2" AllowDrop="true" runat="server">
        <Nodes>
            <wijmo:C1TreeViewNode Text="Folder 1">
                <Nodes>
                    <wijmo:C1TreeViewNode Text="Folder 1.1">
                        <Nodes>
                            <wijmo:C1TreeViewNode Text="Folder 1.1.1">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 1.1.2">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 1.1.3">
                            </wijmo:C1TreeViewNode>
                            <wijmo:C1TreeViewNode Text="Folder 1.1.4">
                            </wijmo:C1TreeViewNode>
                        </Nodes>
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 1.2">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 1.3">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 1.4">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 1.5">
                    </wijmo:C1TreeViewNode>
                </Nodes>
            </wijmo:C1TreeViewNode>
            <wijmo:C1TreeViewNode Text="Folder 2">
                <Nodes>
                    <wijmo:C1TreeViewNode Text="Folder 2.1">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 2.2">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 2.3">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 2.4">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 2.5">
                    </wijmo:C1TreeViewNode>
                </Nodes>
            </wijmo:C1TreeViewNode>
            <wijmo:C1TreeViewNode Text="Folder 3">
                <Nodes>
                    <wijmo:C1TreeViewNode Text="Folder 3.1">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 3.2">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 3.3">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 3.4">
                    </wijmo:C1TreeViewNode>
                    <wijmo:C1TreeViewNode Text="Folder 3.5">
                    </wijmo:C1TreeViewNode>
                </Nodes>
            </wijmo:C1TreeViewNode>
        </Nodes>
    </wijmo:C1TreeView>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1TreeView.DragDrop_Text0 %></p>
    <p><%= Resources.C1TreeView.DragDrop_Text1 %></p>
    <ul>
        <li><%= Resources.C1TreeView.DragDrop_Li1 %></li>
        <li><%= Resources.C1TreeView.DragDrop_Li2 %></li>
    </ul>
    <p><%= Resources.C1TreeView.DragDrop_Text2 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
