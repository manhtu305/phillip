<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ClientSide.aspx.cs" Inherits="ControlExplorer.C1Upload.ClientSide" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.45" namespace="C1.Web.Wijmo.Controls.C1Upload" tagprefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1Upload ID="c1Upload1" runat="server" ShowUploadedFiles="true" Width="300px" OnClientUpload="c1Upload1_ClientUpload" OnClientComplete="c1Upload1_ClientComplete" OnClientProgress="c1Upload1_ClientProgress" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Upload.ClientSide_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">

    <script type="text/javascript">
        function c1Upload1_ClientUpload(e, args) {
            clearlog();
            logMsg("<%= Resources.C1Upload.ClientSide_UploadBegin %>");
            return confirm("<%= Resources.C1Upload.ClientSide_ConfirmMessage %>");
        }

        function c1Upload1_ClientComplete(e, args) {
            logMsg("<%= Resources.C1Upload.ClientSide_UploadEnd %>");
        }

        function c1Upload1_ClientProgress(e, args) {
            logMsg("<%= Resources.C1Upload.ClientSide_ProgressChanged %>");
        }
     
        function logMsg(msg)
        {
            $("#TextArea1").val($("#TextArea1").val() + "[" + msg + "]" + "\r\n");
        }
    
        function clearlog()
        {
            $("#TextArea1").val("");
        }
    </script>
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li class="fullwidth">
                    <label class="settinglegend"><%= Resources.C1Upload.ClientSide_ClientEventList %></label>
                </li>
                <li class="fullwidth autoheight">
                    <textarea id="TextArea1" name="S1" cols="20" style="height:100px;width:300px;"></textarea>
                </li>
            </ul>
        </div>
        <div class="settingcontrol">
            <input id="Button2" type="button" value="<%= Resources.C1Upload.ClientSide_ClearLog %>" onclick="clearlog();"/>
        </div>
    </div>
</asp:Content>
