<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExportPdf.ascx.cs" Inherits="ControlExplorer.ChartCore.ExportPdf" %>
<script type="text/javascript">
    $(function() {
        $("#exportPdf").click(exportPdf);
    });

	function exportPdf() {
		var fileName = $("#fileName").val();
        var url = $("#serverUrl").val() + "/exportapi/chart";
		var pdfSetting = {
			imageQuality: wijmo.exporter.ImageQuality[$("#imageQuality option:selected").val()],
			compression: wijmo.exporter.CompressionType[$("#compression option:selected").val()],
			fontType: wijmo.exporter.FontType[$("#fontType option:selected").val()],
			author: $("#pdfAuthor").val(),
			creator: $("#pdfCreator").val(),
			subject: $("#pdfSubject").val(),
			title: $("#pdfTitle").val(),
			producer: $("#pdfProducer").val(),
			keywords: $("#pdfKeywords").val(),
			encryption: wijmo.exporter.PdfEncryptionType[$("#encryption option:selected").val()],
			ownerPassword: $("#ownerPassword").val(),
			userPassword: $("#userPassword").val(),
			allowCopyContent: $("#allowCopyContent").prop('checked'),
			allowEditAnnotations: $("#allowEditAnnotations").prop('checked'),
			allowEditContent: $("#allowEditContent").prop('checked'),
			allowPrint: $("#allowPrint").prop('checked')
		}
        $(":data(<%=ChartWidgetType %>)").<%=C1ChartWidgetName%>("exportChart", fileName, "pdf", pdfSetting, url);
	}
</script>
<div class="settingcontainer">
    <div class="settingcontent">
	    <ul>
		    <li class="fullwidth"><input type="button" value="<%= Resources.ChartCore.ExportText %>" id="exportPdf"/></li>
			<li class="fullwidth">
				<label class="settinglegend"><%= Resources.ChartCore.FileContentLabel %></label>
			</li>
			<li>
				<label><%= Resources.ChartCore.ImageQualityLabel %></label>
				<select id="imageQuality">
					<option selected="selected" value="Default"><%= Resources.ChartCore.ImageQualityDefault %></option>
					<option value="Low"><%= Resources.ChartCore.ImageQualityLow %></option>
					<option value="Medium"><%= Resources.ChartCore.ImageQualityMedium %></option>
					<option value="High"><%= Resources.ChartCore.ImageQualityHigh %></option>
				</select> 
			</li>
			<li>
				<label><%= Resources.ChartCore.CompressionLabel %></label>
				<select id="compression">
					<option selected="selected" value="Default"><%= Resources.ChartCore.CompressionDefault %></option>
					<option value="None"><%= Resources.ChartCore.CompressionNone %></option>
					<option value="BestSpeed"><%= Resources.ChartCore.CompressionBestSpeed %></option>
					<option value="BestCompression"><%= Resources.ChartCore.CompressionBestCompression %></option>
				</select> 
			</li>
			<li>
				<label><%= Resources.ChartCore.FontTypeLabel %></label>
				<select id="fontType">
					<option value="Standard"><%= Resources.ChartCore.FontTypeStandard %></option>
					<option value="TrueType" selected="selected"><%= Resources.ChartCore.FontTypeTrueType %></option>
					<option value="Embedded"><%= Resources.ChartCore.FontTypeEmbedded %></option>
				</select> 
			</li>
			<li class="fullwidth">
				<label class="settinglegend"><%= Resources.ChartCore.DcoumentInfoLabel %></label>
			</li>
			<li>
				<label><%= Resources.ChartCore.AuthorLabel %></label><input type="text" value="ComponentOne" id="pdfAuthor"/>
			</li>
			<li>
				<label><%= Resources.ChartCore.CreatorLabel %></label><input type="text" value="ComponentOne" id="pdfCreator"/>
			</li>
			<li>
				<label><%= Resources.ChartCore.SubjectLabel %></label><input type="text" id="pdfSubject"/>
			</li>
			<li>
				<label><%= Resources.ChartCore.TitleLabel %></label><input type="text" value="Export" id="pdfTitle"/>
			</li>
			<li>
				<label><%= Resources.ChartCore.ProducerLabel %></label><input type="text" value="ComponentOne" id="pdfProducer"/>
			</li>
			<li>
				<label><%= Resources.ChartCore.KeywordsLabel %></label><input type="text" id="pdfKeywords"/>
			</li>
			<li class="fullwidth">
				<label class="settinglegend"><%= Resources.ChartCore.DcoumentSecurityLabel %></label>
			</li>
			<li class="widesetting fullwidth">
				<label><%= Resources.ChartCore.EncryptionTypeLabel %></label>
				<select id="encryption">
					<option selected="selected" value="NotPermit"><%= Resources.ChartCore.EncryptionNotPermit %></option>
					<option value="Standard40"><%= Resources.ChartCore.EncryptionStandard40 %></option>
					<option value="Standard128"><%= Resources.ChartCore.EncryptionStandard128 %></option>
					<option value="Aes128"><%= Resources.ChartCore.EncryptionAes128 %></option>
				</select> 
			</li>
			<li class="widesetting fullwidth">
				<label><%= Resources.ChartCore.OwnerPasswordLabel %></label><input type="password" id="ownerPassword"/>
			</li>
			<li class="widesetting fullwidth">
				<label><%= Resources.ChartCore.UserPasswordLabel %></label><input type="password" id="userPassword"/>
			</li>
			<li><input type="checkbox" checked="checked" id="allowCopyContent"/><label class="widelabel"><%= Resources.ChartCore.AllowCopyContentLabel %></label></li>
			<li><input type="checkbox" checked="checked" id="allowEditAnnotations"/><label class="widelabel"><%= Resources.ChartCore.AllowEditAnnotationsLabel %></label></li>
			<li><input type="checkbox" checked="checked" id="allowEditContent"/><label class="widelabel"><%= Resources.ChartCore.AllowEditContentLabel %></label></li>
			<li><input type="checkbox" checked="checked" id="allowPrint"/><label class="widelabel"><%= Resources.ChartCore.AllowPrintLabel %></label></li>
			<li class="fullwidth">
				<label class="settinglegend"><%= Resources.ChartCore.ConfigurationLabel %></label>
			</li>
            <li class="longinput">
				<label><%= Resources.ChartCore.ServerUrlLabel %></label>
				<input type="text" id="serverUrl" value="https://demos.componentone.com/ASPNET/ExportService">
			</li>
            <li>
				<label><%= Resources.ChartCore.FileNameLabel %></label>
				<input type="text" id="fileName" value="export">
			</li>
	    </ul>
    </div>
</div>