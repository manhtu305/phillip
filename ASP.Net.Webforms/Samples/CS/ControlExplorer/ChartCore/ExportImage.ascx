<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExportImage.ascx.cs" Inherits="ControlExplorer.ChartCore.ExportImage" %>
<script type="text/javascript">
    $(function() {
        $("#exportImage").click(exportImage);
    });
	
    function exportImage() {
        var fileName = $("#fileName").val();
        var type = $("#fileFormats > option:selected").val();
        var url = $("#serverUrl").val() + "/exportapi/chart";
        $(":data(<%=ChartWidgetType %>)").<%=C1ChartWidgetName%>("exportChart", fileName, type, url);
    }
</script>
<div class="settingcontainer">
    <div class="settingcontent">
	    <ul>
		    <li class="fullwidth"><input type="button" value="<%= Resources.ChartCore.ExportText %>" id="exportImage"/></li>
		    <li>
			    <label><%= Resources.ChartCore.FileFormatLabel %></label>
			    <select id="fileFormats">
				    <option selected="selected" value="Png"><%= Resources.ChartCore.FileFormatPng %></option>
				    <option value="Jpg"><%= Resources.ChartCore.FileFormatJpg %></option>
				    <option value="Bmp"><%= Resources.ChartCore.FileFormatBmp %></option>
				    <option value="Gif"><%= Resources.ChartCore.FileFormatGif %></option>
				    <option value="Tiff"><%= Resources.ChartCore.FileFormatTiff %></option>
			    </select> 
		    </li>
            <li>
				<label><%= Resources.ChartCore.FileNameLabel %></label>
				<input type="text" id="fileName" value="export">
			</li>
            <li class="longinput">
				<label><%= Resources.ChartCore.ServerUrlLabel %></label>
				<input type="text" id="serverUrl" value="https://demos.componentone.com/ASPNET/ExportService">
			</li>
	    </ul>
    </div>
</div>