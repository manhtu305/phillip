<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ExportMode.ascx.cs" Inherits="ControlExplorer.ChartCore.ExportMode" %>
<script type="text/javascript">
    $(function() {
        $("#exportFile").click(exportFile);
    });
	
	function exportFile() {
		wijmo.exporter.exportChart({
			fileName: $("#fileName").val(),
			serviceUrl: $("#serverUrl").val()+"/exportapi/chart",
			chart: $(":data(<%=ChartWidgetType %>)").data("<%=ChartWidgetType %>"),
			exportFileType: wijmo.exporter.ExportFileType[$("#fileFormats > option:selected").val()],
			method: wijmo.exporter.ChartExportMethod[$("#exportMode > option:selected").val()]
		});
	}
</script>
<div class="settingcontainer">
    <div class="settingcontent">
	    <ul>
		    <li class="fullwidth"><input type="button" value="<%= Resources.ChartCore.ExportText %>" id="exportFile"/></li>
            <li>
				<label><%= Resources.ChartCore.ExportModeLabel %></label>
			    <select id="exportMode">
				    <option selected="selected" value="Content"><%= Resources.ChartCore.ExportModeContent %></option>
				    <option value="Options"><%= Resources.ChartCore.ExportModeOptions %></option>
			    </select> 
			</li>
		    <li>
			    <label><%= Resources.ChartCore.FileFormatLabel %></label>
			    <select id="fileFormats">
				    <option selected="selected" value="Png"><%= Resources.ChartCore.FileFormatPng %></option>
				    <option value="Jpg"><%= Resources.ChartCore.FileFormatJpg %></option>
				    <option value="Bmp"><%= Resources.ChartCore.FileFormatBmp %></option>
				    <option value="Gif"><%= Resources.ChartCore.FileFormatGif %></option>
				    <option value="Tiff"><%= Resources.ChartCore.FileFormatTiff %></option>
				    <option value="Pdf"><%= Resources.ChartCore.FileFormatPdf %></option>
			    </select> 
		    </li>
            <li class="longinput">
				<label><%= Resources.ChartCore.ServerUrlLabel %></label>
				<input type="text" id="serverUrl" value="https://demos.componentone.com/ASPNET/ExportService">
			</li>
            <li>
				<label><%= Resources.ChartCore.FileNameLabel %></label>
				<input type="text" id="fileName" value="export">
			</li>
	    </ul>
    </div>
</div>