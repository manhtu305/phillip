<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="OverView.aspx.cs" Inherits="ControlExplorer.C1BinaryImage.OverView" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1BinaryImage"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
	<style type="text/css">
		.dataList
		{
			margin: 5px;
			padding: 5px;
			border: 1px solid #CCCCCC;
			width: 168px;
			height: 180px;
		}
       
	</style>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content2" runat="server">
	<asp:DataList ID="DataList1" runat="server" DataKeyField="PhotoID" 
		DataSourceID="AccessDataSource1" RepeatDirection="Horizontal" RepeatColumns="4" Width="100%">
			<ItemTemplate>
				<div class="dataList">
					<wijmo:C1BinaryImage ID="BinaryImage1" runat="server" AlternateText='<%# Eval("Place") %>'
						ImageData='<%# Eval("Photo") %>' SavedImageName='<%# Eval("Place") %>'
						ToolTip='<%# Eval("Place") %>' />
					<br />
					<br />
                    <asp:Label runat="server" Text="<%$ Resources:C1BinaryImage, OverView_IdLabel %> "></asp:Label>
					<asp:Label ID="Id"  runat="server" Text='<%# Eval("PhotoID") %>' />
                    <br />
                     <asp:Label runat="server" Text="<%$ Resources:C1BinaryImage, OverView_PlaceLabel %>"></asp:Label>
                    <asp:Label ID="Place" runat="server" Text='<%# Eval("Place") %>' />
                    <br />
                     <asp:Label runat="server" Text="<%$ Resources:C1BinaryImage, OverView_CountryLabel %>"></asp:Label>
                    <asp:Label ID="Country" runat="server" Text='<%# Eval("Country") %>' />
                    <br />
				</div>
			</ItemTemplate>
	</asp:DataList>
	<asp:AccessDataSource ID="AccessDataSource1" runat="server" 
		DataFile="~/App_Data/C1NWind.mdb" 
		SelectCommand="SELECT top 8 [PhotoID], [Place], [Country], [Photo] FROM [Photos]">
	</asp:AccessDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p><%= Resources.C1BinaryImage.OverView_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>
