<%@ Page Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="MoveFocus.aspx.cs" Inherits="ControlExplorer.C1GridView.MoveFocus" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<asp:ScriptManager runat="server" ID="ScriptManger1" />

	<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<div style="margin-bottom: 10px;">
				<label for="prevControl"><%= Resources.C1GridView.MoveFocus_PrevControl %></label>
				<input id="prevControl" type="text" />
			</div>
			<wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="SqlDataSource1" AllowKeyboardNavigation="true" KeyActionTab="MoveAcross" HighlightCurrentCell="true" ShowFilter="true"
				DataKeyNames="OrderID" AutogenerateColumns="false" ShowRowHeader="true" AllowPaging="true" PageSize="10" CallbackSettings-Action="All">
				<Columns>
					<wijmo:C1BoundField DataField="ShipName" HeaderText="ShipName" FilterOperator="Contains" />
					<wijmo:C1BoundField DataField="ShipCity" HeaderText="ShipCity" FilterOperator="Contains" />
					<wijmo:C1BoundField DataField="ShippedDate" HeaderText="ShippedDate" FilterOperator="Less" />
				</Columns>
			</wijmo:C1GridView>
			<div style="margin-top: 10px;">
				<label for="nextControl"><%= Resources.C1GridView.MoveFocus_NextControl %></label>
				<input id="nextControl" type="text" />
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>

	<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\C1NWind.mdb;Persist Security Info=True"
		ProviderName="System.Data.OleDb" SelectCommand="SELECT [OrderID], [ShipName], [ShipCity], [ShippedDate] FROM ORDERS"></asp:SqlDataSource>

</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1GridView.MoveFocus_Text0 %></p>
	<p><%= Resources.C1GridView.MoveFocus_Text1 %></p>
	<ul>
		<li><%= Resources.C1GridView.MoveFocus_Li1 %></li>
		<li><%= Resources.C1GridView.MoveFocus_Li2 %></li>
	</ul>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<div class="settingcontainer">
		<div class="settingcontent">
			<asp:Label runat="server" ID="lblkeyActionTab" Text="<%$ Resources:C1GridView, MoveFocus_KeyActionTab %>" AssociatedControlID="dplkeyActionTab"></asp:Label>
			<asp:DropDownList runat="server" ID="dplkeyActionTab" AutoPostBack="true" OnSelectedIndexChanged="dplkeyActionTab_SelectedIndexChanged">
				<asp:ListItem Value="MoveAcross" Selected="True">MoveAcross</asp:ListItem>
				<asp:ListItem Value="MoveAcrossOut">MoveAcrossOut</asp:ListItem>
			</asp:DropDownList>
		</div>
	</div>

</asp:Content>



