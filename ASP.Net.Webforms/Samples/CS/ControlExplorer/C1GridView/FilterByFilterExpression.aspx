<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="FilterByFilterExpression.aspx.cs" Inherits="ControlExplorer.C1GridView.FilterByFilterExpression" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Dialog" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="dbOrders"
		AutogenerateColumns="False" ShowFilter="true" DataKeyNames="OrderID" CallbackSettings-Action="Filtering,Paging"
		ScrollingSettings-Mode="Auto" AllowPaging="true" OnClientRendered="onClientRendered" OnPageIndexChanging="C1GridView1_PageIndexChanging"
		OnFiltering="C1GridView1_Filtering">
		<Columns>
			<wijmo:C1BoundField HeaderText="OrderID" DataField="OrderID" Width="80px" />
			<wijmo:C1BoundField HeaderText="ProductName" DataField="ProductName" />
			<wijmo:C1BoundField HeaderText="UnitPrice" DataField="UnitPrice" DataFormatString="c1" Width="120px" />
			<wijmo:C1BoundField HeaderText="Quantity" DataField="Quantity" Width="120px" />
			<wijmo:C1BoundField HeaderText="Discount" DataField="Discount" DataFormatString="p1" Width="100px" />
			<wijmo:C1TemplateField HeaderText="Total Amount">
				<ItemTemplate>
					<asp:Label runat="server" ID="lbl1" CssClass="custom" Text='<%# String.Format("{0:c1}", Eval("Total")) %>'></asp:Label>
				</ItemTemplate>
			</wijmo:C1TemplateField>
		</Columns>
	</wijmo:C1GridView>
	<wijmo:C1Dialog ID="dlg" runat="server" Title="Filter By Total Amount" Modal="true" Resizable="false" Height="200" Width="400" ShowOnLoad="False">
		<Content>
			<br />
			<wijmo:C1ComboBox ID="ddlOperator" runat="server" IsEditable="false" Height="25">
				<Items>
					<wijmo:C1ComboBoxItem Text="NoFilter" Selected="true" />
					<wijmo:C1ComboBoxItem Value="=" Text="Equal" />
					<wijmo:C1ComboBoxItem Value=">" Text="Greater" />
					<wijmo:C1ComboBoxItem Value=">=" Text="GreaterOrEqual" />
					<wijmo:C1ComboBoxItem Value="<" Text="Less" />
					<wijmo:C1ComboBoxItem Value="<=" Text="LessOrEqual" />
					<wijmo:C1ComboBoxItem Value="<>" Text="NotEqual" />
				</Items>
			</wijmo:C1ComboBox>
			<wijmo:C1InputCurrency ID="txtTotal" runat="server" Height="29" ShowSpinner="true" />
			<br />
		</Content>
	</wijmo:C1Dialog>
	<asp:AccessDataSource ID="dbOrders" runat="server"
		DataFile="~/App_Data/C1NWind.mdb"
		SelectCommand="Select * from (SELECT OrderID, ProductName, [Order Details].UnitPrice as UnitPrice, Quantity, Discount,(UnitPrice - (UnitPrice * Discount)) * Quantity as Total
FROM Products INNER JOIN [Order Details] ON Products.ProductID = [Order Details].ProductID) Order By OrderID"></asp:AccessDataSource>
	<script type="text/javascript">
		var dlg, c1GridView1;
		$(function () {
			var txtTotal = "#" + '<%= txtTotal.ClientID %>',
				filterExpression,
				ddlOperator = "#" + '<%= ddlOperator.ClientID %>';
			c1GridView1 = "#" + '<%= C1GridView1.ClientID %>';
			dlg = $("#" + '<%= dlg.ClientID %>').c1dialog("option", "buttons",
				{
					"OK": function () {
						filterExpression = $(ddlOperator).c1combobox("option", "selectedIndex") ?
						$(ddlOperator).c1combobox("option", "selectedValue") +
						$(txtTotal).c1inputcurrency("option", "value") : "";
						$(c1GridView1).c1gridview("doFilter", "Total Amount", filterExpression);
						$(this).c1dialog("close");
					},
					"Cancel": function () {
						$(this).c1dialog("close");
					}
				});
		});
		function onClientRendered() {
			// Create Filter button.
			if (!$("#btnFilter")[0]) {
				$("<table><tr><td style='width:100%'></td><td class='wijmo-wijgrid-filter-trigger ui-corner-right'>" +
					"<span id='btnFilter' class='ui-icon ui-icon-triangle-1-s'></span></td></tr></table>")
					.css("padding", "1px")
					.click(function () { $(dlg).c1dialog("open"); })
					.appendTo($(".wijmo-wijgrid-filterrow").find("td:last"));
			}
			// Set "Total Amount" column to right-align.
			$("td[headers*='Total']", c1GridView1)
				.removeClass("wijdata-type-string")
				.addClass("wijdata-type-number");
		}
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1GridView.FilterByFilterExpression_Text0 %></p>
	<ul>
		<li><%= Resources.C1GridView.FilterByFilterExpression_Li1 %></li>
		<li><%= Resources.C1GridView.FilterByFilterExpression_Li2 %></li>
	</ul>
	<p><%= Resources.C1GridView.FilterByFilterExpression_Text1 %></p>
</asp:Content>


