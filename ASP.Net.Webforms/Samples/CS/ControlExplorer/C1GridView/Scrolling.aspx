<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Scrolling.aspx.cs" Inherits="ControlExplorer.C1GridView.Scrolling" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
	
	<asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
		<ContentTemplate>
			<wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="SqlDataSource1" AllowKeyboardNavigation="true" Height="400px">
				<ScrollingSettings Mode="Auto" StaticColumnIndex="0" StaticRowIndex="1">
				</ScrollingSettings>
			</wijmo:C1GridView>
		</ContentTemplate>
	</asp:UpdatePanel>

	<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\C1NWind.mdb;Persist Security Info=True"
		ProviderName="System.Data.OleDb" SelectCommand="SELECT TOP 30 * FROM ORDERS">
	</asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
  <p><%= Resources.C1GridView.Scrolling_Text0 %></p>
			<p><%= Resources.C1GridView.Scrolling_Text1 %></p>
			<p><%= Resources.C1GridView.Scrolling_Text2 %></p>
			<p><%= Resources.C1GridView.Scrolling_Text3 %></p>
</asp:Content>

