<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ExportingPdf.aspx.cs" Inherits="ControlExplorer.C1GridView.ExportingPdf" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1GridView runat="server" ID="C1GridView1" AutogenerateColumns="False" DataSourceID="SqlDataSource1">
		<Columns>
			<wijmo:C1Band HeaderText="Product Information">
				<Columns>
					<wijmo:C1BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName">
						<ItemStyle HorizontalAlign="Center" />
					</wijmo:C1BoundField>
					<wijmo:C1BoundField DataField="UnitPrice" HeaderText="UnitPrice" SortExpression="UnitPrice">
					</wijmo:C1BoundField>
				</Columns>
			</wijmo:C1Band>
			<wijmo:C1Band HeaderText="Order Information">
				<Columns>
					<wijmo:C1BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity">
					</wijmo:C1BoundField>
			        <wijmo:C1CheckBoxField DataField="Discontinued" HeaderText="Discontinued" SortExpression="Discontinued">
			        </wijmo:C1CheckBoxField>
				</Columns>
			</wijmo:C1Band>
			<wijmo:C1Band HeaderText="Order Details">
				<Columns>
					<wijmo:C1BoundField DataField="OrderDate" HeaderText="OrderDate" SortExpression="OrderDate">
						<ItemStyle HorizontalAlign="Center" />
					</wijmo:C1BoundField>
					<wijmo:C1BoundField DataField="ShipName" HeaderText="ShipName" SortExpression="ShipName">
					</wijmo:C1BoundField>
				</Columns>
			</wijmo:C1Band>
		</Columns>
	</wijmo:C1GridView>
	<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:C1NWindConnectionString %>" ProviderName="<%$ ConnectionStrings:C1NWindConnectionString.ProviderName %>" 
    SelectCommand="SELECT top 25 [Order Details].OrderID, [Order Details].UnitPrice, [Order Details].Quantity,
     [Order Details].Discount, Products.ProductName, Orders.OrderDate, Orders.ShipName , Products.Discontinued
    FROM ((Products INNER JOIN [Order Details] ON Products.ProductID = [Order Details].ProductID) 
    INNER JOIN Orders ON [Order Details].OrderID = Orders.OrderID)"></asp:SqlDataSource>
<script type="text/javascript">
    $(function() {
        $("#exportPdf").click(exportPdf);
		addPaperKind();
		$("#paperKind").change(function(){
			var isCustom = wijmo.exporter.PaperKind[$("#paperKind option:selected").val()] == wijmo.exporter.PaperKind.Custom;
			$("#pageWidth").prop("disabled", !isCustom);
			$("#pageHeight").prop("disabled", !isCustom);
			$("#pageWidth").toggleClass("ui-state-disabled", !isCustom);
			$("#pageHeight").toggleClass("ui-state-disabled", !isCustom);
		});
    });
	
	function addPaperKind(){
		$.each(wijmo.exporter.PaperKind, function(item){
			if(!isNaN(parseInt(item))) return;
			$("#paperKind").append($("<option>").val(item).text(item).prop("selected",item=="Custom"));
		});
	}

	function getPdfSetting() {
	    return {
			repeatHeader: $("#repeatHeader").prop('checked'),
			landscape: $("#landscape").prop('checked'),
			autoFitWidth: $("#autoFitWidth").prop('checked'),
			pageSize: {
				width: parseFloat($("#pageWidth").val()),
				height: parseFloat($("#pageHeight").val())
			},
			paperKind: wijmo.exporter.PaperKind[$("#paperKind option:selected").val()],
			margins: {
				top: parseFloat($("#marginsTop").val()),
				right: parseFloat($("#marginsRight").val()),
				bottom: parseFloat($("#marginsBottom").val()),
				left: parseFloat($("#marginsLeft").val())
			},
			imageQuality: wijmo.exporter.ImageQuality[$("#imageQuality option:selected").val()],
			compression: wijmo.exporter.CompressionType[$("#compression option:selected").val()],
			fontType: wijmo.exporter.FontType[$("#fontType option:selected").val()],
			author: $("#pdfAuthor").val(),
			creator: $("#pdfCreator").val(),
			subject: $("#pdfSubject").val(),
			title: $("#pdfTitle").val(),
			producer: $("#pdfProducer").val(),
			keywords: $("#pdfKeywords").val(),
			encryption: wijmo.exporter.PdfEncryptionType[$("#encryption option:selected").val()],
			ownerPassword: $("#ownerPassword").val(),
			userPassword: $("#userPassword").val(),
			allowCopyContent: $("#allowCopyContent").prop('checked'),
			allowEditAnnotations: $("#allowEditAnnotations").prop('checked'),
			allowEditContent: $("#allowEditContent").prop('checked'),
			allowPrint: $("#allowPrint").prop('checked')
	    }
	}

    function exportPdf() {
		var fileName = $("#fileName").val();
        var pdfSetting = getPdfSetting();
        var url = $("#serverUrl").val() + "/exportapi/grid";
        $("#<%=C1GridView1.ClientID%>").c1gridview("exportGrid", fileName, "pdf", pdfSetting, url);
	}
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1GridView.ExportingPdf_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
<div class="settingcontainer">
    <div class="settingcontent">
	    <ul>
		    <li class="fullwidth"><input type="button" value="<%= Resources.C1GridView.ExportingPdf_Export %>" id="exportPdf"/></li>
		    <li class="narrowcheckbox"><input type="checkbox" checked="checked" id="repeatHeader"/><label class="widelabel"><%= Resources.C1GridView.ExportingPdf_RepeatGridHeader %></label></li>
			<li class="narrowcheckbox"><input type="checkbox" checked="checked" id="autoFitWidth"/><label class="widelabel"><%= Resources.C1GridView.ExportingPdf_AutoFitWidth %></label></li>
			<li class="narrowcheckbox"><input type="checkbox" checked="checked" id="landscape"/><label class="widelabel"><%= Resources.C1GridView.ExportingPdf_Landscape %></label></li>
			<li class="fullwidth"><label class="settinglegend"><%= Resources.C1GridView.ExportingPdf_Margins %></label></li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_Top %></label><input type="text" value="72" id="marginsTop"/>
			</li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_Right %></label><input type="text" value="72" id="marginsRight"/>
			</li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_Bottom %></label><input type="text" value="72" id="marginsBottom"/>
			</li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_Left %></label><input type="text" value="72" id="marginsLeft"/>
			</li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_PaperKind %></label>
				<select id="paperKind">
				</select> 
			</li>
			<li class="fullwidth">
				<label class="settinglegend"><%= Resources.C1GridView.ExportingPdf_PageSize %></label>
			</li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_Width %></label><input type="text" value="500" id="pageWidth"/>
			</li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_Height %></label><input type="text" value="900" id="pageHeight"/>
			</li>
			<li class="fullwidth">
				<label class="settinglegend"><%= Resources.C1GridView.ExportingPdf_FileContent %></label>
			</li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_ImageQuality %></label>
				<select id="imageQuality">
					<option selected="selected" value="Default"><%= Resources.C1GridView.ExportingPdf_ImageQualityDefault %></option>
					<option value="Low"><%= Resources.C1GridView.ExportingPdf_ImageQualityLow %></option>
					<option value="Medium"><%= Resources.C1GridView.ExportingPdf_ImageQualityMedium %></option>
					<option value="High"><%= Resources.C1GridView.ExportingPdf_ImageQualityHigh %></option>
				</select> 
			</li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_Compression %></label>
				<select id="compression">
					<option selected="selected" value="Default"><%= Resources.C1GridView.ExportingPdf_CompressionDefault %></option>
					<option value="None"><%= Resources.C1GridView.ExportingPdf_CompressionNone %></option>
					<option value="BestSpeed"><%= Resources.C1GridView.ExportingPdf_CompressionBestSpeed %></option>
					<option value="BestCompression"><%= Resources.C1GridView.ExportingPdf_CompressionBestCompression %></option>
				</select> 
			</li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_FontType %></label>
				<select id="fontType">
					<option value="Standard"><%= Resources.C1GridView.ExportingPdf_FontTypeStandard %></option>
					<option value="TrueType" selected="selected"><%= Resources.C1GridView.ExportingPdf_FontTypeTrueType %></option>
					<option value="Embedded"><%= Resources.C1GridView.ExportingPdf_FontTypeEmbedded %></option>
				</select> 
			</li>
			<li class="fullwidth">
				<label class="settinglegend"><%= Resources.C1GridView.ExportingPdf_DcoumentInfo %></label>
			</li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_Author %></label><input type="text" value="ComponentOne" id="pdfAuthor"/>
			</li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_Creator %></label><input type="text" value="ComponentOne" id="pdfCreator"/>
			</li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_Subject %></label><input type="text" id="pdfSubject"/>
			</li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_Title %></label><input type="text" value="Export Grid" id="pdfTitle"/>
			</li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_Producer %></label><input type="text" value="ComponentOne" id="pdfProducer"/>
			</li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_Keywords %></label><input type="text" id="pdfKeywords"/>
			</li>
			<li class="fullwidth">
				<label class="settinglegend"><%= Resources.C1GridView.ExportingPdf_DcoumentSecurity %></label>
			</li>
			<li class="fullwidth">
				<label><%= Resources.C1GridView.ExportingPdf_EncryptionType %></label>
				<select id="encryption">
					<option selected="selected" value="NotPermit"><%= Resources.C1GridView.ExportingPdf_EncryptionTypeNotPermit %></option>
					<option value="Standard40"><%= Resources.C1GridView.ExportingPdf_EncryptionTypeStandard40 %></option>
					<option value="Standard128"><%= Resources.C1GridView.ExportingPdf_EncryptionTypeStandard128 %></option>
					<option value="Aes128"><%= Resources.C1GridView.ExportingPdf_EncryptionTypeAes128 %></option>
				</select> 
			</li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_OwnerPassword %></label><input type="password" id="ownerPassword"/>
			</li>
			<li>
				<label><%= Resources.C1GridView.ExportingPdf_UserPassword %></label><input type="password" id="userPassword"/>
			</li>
			<li><input type="checkbox" checked="checked" id="allowCopyContent"/><label class="widelabel"><%= Resources.C1GridView.ExportingPdf_AllowCopyContent %></label></li>
			<li><input type="checkbox" checked="checked" id="allowEditAnnotations"/><label class="widelabel"><%= Resources.C1GridView.ExportingPdf_AllowEditAnnotations %></label></li>
			<li><input type="checkbox" checked="checked" id="allowEditContent"/><label class="widelabel"><%= Resources.C1GridView.ExportingPdf_AllowEditContent %></label></li>
			<li><input type="checkbox" checked="checked" id="allowPrint"/><label class="widelabel"><%= Resources.C1GridView.ExportingPdf_AllowPrint %></label></li>
			<li class="fullwidth">
				<label class="settinglegend"><%= Resources.C1GridView.ExportingPdf_Configuration %></label>
			</li>
            <li class="longinput">
				<label><%= Resources.C1GridView.ExportingPdf_ServerUrl %></label>
				<input type="text" id="serverUrl" value="https://demos.componentone.com/ASPNET/ExportService">
			</li>
            <li>
				<label><%= Resources.C1GridView.ExportingPdf_FileName %></label>
				<input type="text" id="fileName" value="export">
			</li>
	    </ul>
    </div>
</div>
</asp:Content>
