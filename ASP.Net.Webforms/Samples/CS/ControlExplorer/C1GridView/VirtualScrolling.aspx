<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="VirtualScrolling.aspx.cs" Inherits="ControlExplorer.C1GridView.WebForm1" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
	
	<wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="SqlDataSource1" Height="400px">
		<CallbackSettings Action="Scrolling" />
		<ScrollingSettings Mode="Both">
			<VirtualizationSettings Mode="Rows" /> 
		</ScrollingSettings>
	</wijmo:C1GridView>

	<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\C1NWind.mdb;Persist Security Info=True"
		ProviderName="System.Data.OleDb" SelectCommand="SELECT * FROM ORDERS">
	</asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
  <p><%= Resources.C1GridView.VirtualScrolling_Text0 %></p>
	<ul>
		<li><%= Resources.C1GridView.VirtualScrolling_Li1 %></li>
		<li><%= Resources.C1GridView.VirtualScrolling_Li2 %></li>
	</ul>
  </p>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
