﻿using C1.Web.Wijmo.Controls.C1GridView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ControlExplorer.C1GridView
{
	public partial class FilterByFilterExpression : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}
		protected void C1GridView1_Filtering(object sender, C1GridViewFilterEventArgs e)
		{
			ApplyFilterExpression();
		}

		protected void C1GridView1_PageIndexChanging(object sender, EventArgs e)
		{
			ApplyFilterExpression();
		}

		public void ApplyFilterExpression()
		{
			var FilterExpression = (C1GridView1.Columns[5] as C1TemplateField).FilterExpression;
			var FilterCondition = !string.IsNullOrEmpty(FilterExpression) ?
				"Where Total " + FilterExpression : string.Empty;
			var SelectCommand = "Select * from (SELECT OrderID, ProductName, [Order Details].UnitPrice as UnitPrice," +
				"Quantity, Discount,(UnitPrice - (UnitPrice * Discount)) * Quantity as Total " +
				"FROM Products INNER JOIN [Order Details] ON Products.ProductID = [Order Details].ProductID) " +
				FilterCondition + " Order By OrderID ";

			dbOrders.SelectCommand = SelectCommand;
		}
	}
}