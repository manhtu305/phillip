﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;
using C1.Web.Wijmo.Controls.C1GridView;
using Grid = C1.Web.Wijmo.Controls.C1GridView;

namespace ControlExplorer.C1GridView
{
	public partial class HierarchicalGridCustomBinding : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Customers.DataSource = GetCustomers();
				Customers.DataBind();
			}
		}

		private DataTable GetCustomers()
		{
			DataTable customers;

			using (OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\C1NWind.mdb;Persist Security Info=True"))
			{
				using (OleDbCommand command = new OleDbCommand("SELECT TOP 2 * FROM Customers", connection))
				{
					using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
					{
						customers = new DataTable();
						adapter.Fill(customers);
						customers.PrimaryKey = new DataColumn[] { customers.Columns["CustomerID"] };
					}
				}
			}

			return customers;
		}


		private DataTable GetOrders(string customerID)
		{
			DataTable orders;

			using (OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\C1NWind.mdb;Persist Security Info=True"))
			{
				using (OleDbCommand command = new OleDbCommand("SELECT TOP 2 * FROM Orders WHERE CustomerID = @CustomerID", connection))
				{
					command.Parameters.AddWithValue("CustomerID", customerID);

					using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
					{
						orders = new DataTable();
						adapter.Fill(orders);
						orders.PrimaryKey = new DataColumn[] { orders.Columns["OrderID"] };
					}
				}
			}

			return orders;
		}

		private DataTable GetOrderDetails(int orderID)
		{
			DataTable orderDetails;

			using (OleDbConnection connection = new OleDbConnection(@"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\C1NWind.mdb;Persist Security Info=True"))
			{
				using (OleDbCommand command = new OleDbCommand("SELECT TOP 2 * FROM [Order Details] where OrderID = @OrderID", connection))
				{
					command.Parameters.AddWithValue("OrderID", orderID);

					using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
					{
						orderDetails = new DataTable();
						adapter.Fill(orderDetails);
					}
				}
			}

			return orderDetails;
		}

		protected void Customers_DetailRequiresDataSource(object sender, C1GridViewDetailRequiresDataSourceEventArgs e)
		{
			e.Detail.DataSource = GetOrders((string)e.Detail.MasterDataKey["CustomerID"]);
		}

		protected void Orders_DetailRequiresDataSource(object sender, C1GridViewDetailRequiresDataSourceEventArgs e)
		{
			e.Detail.DataSource = GetOrderDetails((int)e.Detail.MasterDataKey["OrderID"]);
		}

		protected void Orders_PageIndexChanging(object sender, C1GridViewPageEventArgs e)
		{
			Grid.C1DetailGridView detail = (Grid.C1DetailGridView)sender;

			detail.PageIndex = e.NewPageIndex;

			detail.DataSource = GetOrders((string)detail.MasterDataKey["CustomerID"]);
			detail.DataBind();
		}

		protected void Customers_HierarchyRowToggled(object sender, C1GridViewHierarchyRowToggledEventArgs e)
		{
			DataBoundControl grid = (DataBoundControl)sender;

			grid.DataSource = GetCustomers();
			grid.DataBind();
		}

		protected void Orders_HierarchyRowToggled(object sender, C1GridViewHierarchyRowToggledEventArgs e)
		{
			DataBoundControl grid = (DataBoundControl)sender;

			grid.DataSource = GetOrders((string)e.Keys["CustomerID"]);
			grid.DataBind();
		}

		protected void Details_PageIndexChanging(object sender, C1GridViewPageEventArgs e)
		{
			C1DetailGridView detail = (C1DetailGridView)sender;

			detail.PageIndex = e.NewPageIndex;

			detail.DataSource = GetOrderDetails((int)detail.MasterDataKey["OrderID"]);
			detail.DataBind();
		}
	}
}