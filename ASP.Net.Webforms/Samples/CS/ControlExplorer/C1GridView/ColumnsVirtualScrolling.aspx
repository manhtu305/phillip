<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ColumnsVirtualScrolling.aspx.cs" Inherits="ControlExplorer.C1GridView.ColumnsVirtualScrolling" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
	
	<wijmo:C1GridView ID="C1GridView1" runat="server" Height="400px" Width="800px">
		<CallbackSettings Action="Scrolling" />
		<ScrollingSettings Mode="Both">
			<VirtualizationSettings Mode="Both" /> 
		</ScrollingSettings>
	</wijmo:C1GridView>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
  <p><%= Resources.C1GridView.ColumnsVirtualScrolling_Text0 %></p>
	<ul>
		<li><%= Resources.C1GridView.ColumnsVirtualScrolling_Li1 %></li>
		<li><%= Resources.C1GridView.ColumnsVirtualScrolling_Li2 %></li>
	</ul>
  </p>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
