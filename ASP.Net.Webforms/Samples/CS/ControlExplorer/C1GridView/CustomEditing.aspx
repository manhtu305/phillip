<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CustomEditing.aspx.cs" Inherits="ControlExplorer.C1GridView.Editing" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Calendar" TagPrefix="wijmo" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server">
	</asp:ScriptManager>

	<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
				<wijmo:C1GridView ID="C1GridView1" runat="server" AllowPaging="True" PageSize="10"
					DataKeyNames="OrderID" AutoGenerateColumns="False"
					OnPageIndexChanging="C1GridView1_PageIndexChanging"
					OnRowCancelingEdit="C1GridView1_RowCancelingEdit" OnRowEditing="C1GridView1_RowEditing"
					OnRowUpdating="C1GridView1_RowUpdating" OnRowDeleting="C1GridView1_RowDeleting">
					<PagerSettings Mode="NextPreviousFirstLast" />
					<Columns>
						<wijmo:C1TemplateField>
							<ItemStyle Width="15px" />
						</wijmo:C1TemplateField>
						<wijmo:C1BoundField DataField="OrderID" HeaderText="ID" ReadOnly="true" DataFormatString="d" />
					   
						<wijmo:C1TemplateField HeaderText="Ship name">
							<UpdateBindings>
								<wijmo:C1GridViewUpdateBinding ControlProperty="tbShipName.Text" UpdateField="ShipName" />
							</UpdateBindings>
							<EditItemTemplate>
								<asp:TextBox ID="tbShipName" runat="server" Text='<%# Bind("ShipName") %>' BorderStyle="None" Width="100%" />
							</EditItemTemplate>
							<ItemTemplate>
								<%# HtmlEncode(Eval("ShipName")) %>
							</ItemTemplate>
						</wijmo:C1TemplateField>
						 
						<wijmo:C1TemplateField HeaderText="Ship city">
							<UpdateBindings>
								<wijmo:C1GridViewUpdateBinding ControlProperty="tbShipCity.Text" UpdateField="ShipCity" />
							</UpdateBindings>
							<EditItemTemplate>
								<asp:TextBox ID="tbShipCity" runat="server" Text='<%# Bind("ShipCity") %>' BorderStyle="None" Width="100%" />
							</EditItemTemplate>
							<ItemTemplate>
								<%# HtmlEncode(Eval("ShipCity")) %>
							</ItemTemplate>
						</wijmo:C1TemplateField>
						<wijmo:C1CommandField ShowEditButton="True" ShowDeleteButton="false" />
					</Columns>
				</wijmo:C1GridView>
	   
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1GridView.CustomEditing_Text0 %></p>

	<p><%= Resources.C1GridView.CustomEditing_Text1 %></p>

	<p><%= Resources.C1GridView.CustomEditing_Text2 %></p>
	<ul>
	  <li><%= Resources.C1GridView.CustomEditing_Li1 %></li>
	  <li><%= Resources.C1GridView.CustomEditing_Li2 %></li>
	  <li><%= Resources.C1GridView.CustomEditing_Li3 %></li>
	</ul>

	<p><%= Resources.C1GridView.CustomEditing_Text3 %></p>
	<ul>
		<li><%= Resources.C1GridView.CustomEditing_Li4 %></li>
		<li><%= Resources.C1GridView.CustomEditing_Li5 %></li>
		<li><%= Resources.C1GridView.CustomEditing_Li6 %></li>
		<li><%= Resources.C1GridView.CustomEditing_Li7 %></li>
	</ul>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<asp:UpdatePanel ID="UpdatePanel2" runat="server">
		<ContentTemplate>
			<div class="settingcontainer">
				<div class="settingcontent">
					<p><%= Resources.C1GridView.CustomEditing_Text4 %></p>
					<ul>
						<li class="fullwidth"><asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true" Text="<%$ Resources:C1GridView, CustomEditing_ShowEditButton %>"
							Checked="true" OnCheckedChanged="CheckBox1_CheckedChanged" /></li>
						<li class="fullwidth"><asp:CheckBox ID="CheckBox2" runat="server" AutoPostBack="true" Text="<%$ Resources:C1GridView, CustomEditing_ShowDeleteButton %>"
							Checked="false" OnCheckedChanged="CheckBox1_CheckedChanged" /></li>
					</ul>
				</div>
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>

</asp:Content>
