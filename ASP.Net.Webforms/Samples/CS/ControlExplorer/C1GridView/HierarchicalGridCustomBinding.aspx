<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="HierarchicalGridCustomBinding.aspx.cs" Inherits="ControlExplorer.C1GridView.HierarchicalGridCustomBinding" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1GridView ID="Customers" runat="server" DataKeyNames="CustomerID, CompanyName" 
		AutogenerateColumns="false" 
		OnDetailRequiresDataSource="Customers_DetailRequiresDataSource"
		OnHierarchyRowToggled="Customers_HierarchyRowToggled">
		<CallbackSettings Action="All" />
		<Columns>
			<wijmo:C1BoundField DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName" />
			<wijmo:C1BoundField DataField="Country" HeaderText="Country" SortExpression="Country" />
			<wijmo:C1BoundField DataField="City" HeaderText="City"  SortExpression="City" />
		</Columns>
		<Detail>
			<wijmo:C1DetailGridView runat="server" ID="Orders" DataKeyNames="OrderID" AutogenerateColumns="false"
				OnDetailRequiresDataSource="Orders_DetailRequiresDataSource"
				OnHierarchyRowToggled="Orders_HierarchyRowToggled">
				<Columns>
					<wijmo:C1BoundField DataField="ShippedDate" HeaderText="ShippedDate" SortExpression="ShippedDate" />
					<wijmo:C1BoundField DataField="Freight" HeaderText="Freight"  SortExpression="Freight" />
					<wijmo:C1BoundField DataField="ShipVia" HeaderText="ShipVia" SortExpression="ShipVia" />
				</Columns>
				<Relation>
					<wijmo:MasterDetailRelation DetailDataKeyName="CustomerID" MasterDataKeyName="CustomerID" />
				</Relation>
				<Detail>
					<wijmo:C1DetailGridView runat="server" ID="Details" DataKeyNames="OrderID" AutogenerateColumns="false">
						<Columns>
							<wijmo:C1BoundField DataField="UnitPrice" HeaderText="UnitPrice" SortExpression="UnitPrice" />
							<wijmo:C1BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
							<wijmo:C1BoundField DataField="Discount" HeaderText="Discount" SortExpression="Discount" />
						</Columns>
						<Relation>
							<wijmo:MasterDetailRelation DetailDataKeyName="OrderID" MasterDataKeyName="OrderID" />
						</Relation>
					</wijmo:C1DetailGridView>
				</Detail>
			</wijmo:C1DetailGridView> 
		</Detail>
	</wijmo:C1GridView>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
</asp:Content>
