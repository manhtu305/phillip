<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ExportAllPages.aspx.cs" Inherits="ControlExplorer.C1GridView.ExportAllPages" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1GridView runat="server" ID="C1GridView1" AutogenerateColumns="False" 
    DataSourceID="SqlDataSource1" AllowPaging="true" PageSize="10" CallbackSettings-Action="All">
		<Columns>
			<wijmo:C1Band HeaderText="Product Information">
				<Columns>
					<wijmo:C1BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName">
						<ItemStyle HorizontalAlign="Center" />
					</wijmo:C1BoundField>
					<wijmo:C1BoundField DataField="UnitPrice" HeaderText="UnitPrice" SortExpression="UnitPrice">
					</wijmo:C1BoundField>
				</Columns>
			</wijmo:C1Band>
			<wijmo:C1Band HeaderText="Order Information">
				<Columns>
					<wijmo:C1BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity">
					</wijmo:C1BoundField>
					<wijmo:C1BoundField DataField="Discount" HeaderText="Discount" SortExpression="Discount">
					</wijmo:C1BoundField>
				</Columns>
			</wijmo:C1Band>
			<wijmo:C1Band HeaderText="Order Details">
				<Columns>
					<wijmo:C1BoundField DataField="OrderDate" HeaderText="OrderDate" SortExpression="OrderDate">
						<ItemStyle HorizontalAlign="Center" />
					</wijmo:C1BoundField>
					<wijmo:C1BoundField DataField="ShipName" HeaderText="ShipName" SortExpression="ShipName">
					</wijmo:C1BoundField>
				</Columns>
			</wijmo:C1Band>
		</Columns>
	</wijmo:C1GridView>
	<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:C1NWindConnectionString %>" ProviderName="<%$ ConnectionStrings:C1NWindConnectionString.ProviderName %>" SelectCommand="SELECT top 55 [Order Details].OrderID, [Order Details].UnitPrice, [Order Details].Quantity, [Order Details].Discount, Products.ProductName, Orders.OrderDate, Orders.ShipName FROM ((Products INNER JOIN [Order Details] ON Products.ProductID = [Order Details].ProductID) INNER JOIN Orders ON [Order Details].OrderID = Orders.OrderID)"></asp:SqlDataSource>
<script type="text/javascript">
    $(function() {
    	$("#export").click(exportGrid);
    });

    function getGrid(){
		return $("#<%=C1GridView1.ClientID%>").data("wijmo-c1gridview");
	}

	function exportGrid() {
		wijmo.exporter.exportGrid({
			fileName: $("#fileName").val(),
			serviceUrl: $("#serverUrl").val() + "/exportapi/grid",
			exportFileType: wijmo.exporter.ExportFileType[$("#fileFormats > option:selected").val()],
			grid: getGrid(),
			onlyCurrentPage: !$("#exportAllPage").prop('checked')
		});
	}

</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1GridView.ExportAllPages_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
<div class="settingcontainer">
    <div class="settingcontent">
	    <ul>
		    <li><input type="button" value="<%= Resources.C1GridView.ExportAllPages_Export %>" id="export"/></li>
		    <li><input type="checkbox" id="exportAllPage" checked="checked"/><label class="widelabel"><%= Resources.C1GridView.ExportAllPages_ExportAllPage %></label></li>
		    <li class="longinput">
				<label><%= Resources.C1GridView.ExportAllPages_ServerUrl %></label>
			    <input type="text" id="serverUrl" value="https://demos.componentone.com/ASPNET/ExportService"/>
			</li>
            <li>
				<label><%= Resources.C1GridView.ExportAllPages_FileName %></label>
	            <input type="text" id="fileName" value="export"/>
			</li>
            <li>
			    <label><%= Resources.C1GridView.ExportAllPages_FileFormat %></label>
			    <select id="fileFormats">
				    <option selected="selected" value="Pdf">Pdf</option>
				    <option value="Xls">Xls</option>
				    <option value="Xlsx">Xlsx</option>
				    <option value="Csv">Csv</option>
			    </select> 
		    </li>
	    </ul>
    </div>
</div>
</asp:Content>
