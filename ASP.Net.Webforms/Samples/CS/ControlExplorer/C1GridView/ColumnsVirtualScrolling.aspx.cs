﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ControlExplorer.C1GridView
{
	public partial class ColumnsVirtualScrolling : System.Web.UI.Page
	{
		private const int ROWS = 100;
		private const int COLUMNS = 100;

		protected void Page_Load(object sender, EventArgs e)
		{
			C1GridView1.DataSource = GetDummyData();

			if (!IsPostBack)
			{
				C1GridView1.DataBind();
			}
		}

		private DataTable GetDummyData()
		{
			if (Session["DummyData"] == null)
			{
				DataTable dummy = new DataTable();

				for (int i = 0; i < COLUMNS; i++)
				{
					dummy.Columns.Add("Column" + i.ToString(), typeof(string));
				}

				for (int i = 0; i < ROWS; i++)
				{
					string[] values = new string[COLUMNS];

					for (int j = 0; j < COLUMNS; j++)
					{
						values[j] = string.Format("row {0}, col {1}", i.ToString(), j.ToString());
					}

					dummy.Rows.Add(values);
				}

				Session["DummyData"] = dummy;
			}

			return (DataTable)Session["DummyData"];
		}
	}
}