<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="FilteringTemplateField.aspx.cs" Inherits="ControlExplorer.C1GridView.FilteringTemplateField" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="dbEmployees"
		AutogenerateColumns="False" ShowFilter="true" DataKeyNames="EmployeeID"
		CallbackSettings-Action="Filtering">
		<Columns>
			<wijmo:C1BoundField HeaderText="EmployeeID" DataField="EmployeeID"
				SortExpression="EmployeeID" Width="120" />
			<wijmo:C1TemplateField HeaderText="Title" DataField="Title">
				<ItemTemplate>
					<asp:Label runat="server" Font-Italic="true" ID="lbl2" Text='<%# Bind("Title") %>'></asp:Label>
				</ItemTemplate>
			</wijmo:C1TemplateField>
			<wijmo:C1BoundField HeaderText="BirthDate" DataField="BirthDate"
				SortExpression="BirthDate" />
			<wijmo:C1BoundField HeaderText="HireDate" DataField="HireDate"
				SortExpression="HireDate" />
		</Columns>
	</wijmo:C1GridView>
	<asp:AccessDataSource ID="dbEmployees" runat="server"
		DataFile="~/App_Data/C1NWind.mdb"
		SelectCommand="SELECT * FROM [Employees]"></asp:AccessDataSource>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1GridView.FilteringTemplateField_Text0 %></p>
	<p><%= Resources.C1GridView.FilteringTemplateField_Text1 %></p>
	<ul>
		<li><%= Resources.C1GridView.FilteringTemplateField_Li1 %></li>
		<li><%= Resources.C1GridView.FilteringTemplateField_Li2 %></li>
	</ul>

	<p><%= Resources.C1GridView.FilteringTemplateField_Text2 %></p>
</asp:Content>
