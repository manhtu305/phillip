<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Columns.aspx.cs" Inherits="ControlExplorer.C1GridView.Columns" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:ScriptManager runat="server" ID="ScriptManager1" />
	
	<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="SqlDataSource1"
				AutoGenerateColumns="false" AllowColMoving="true" AllowSorting="true">
				<ScrollingSettings Mode="Auto"></ScrollingSettings>
				<Columns>
					<wijmo:C1BoundField DataField="OrderID" HeaderText="ID" SortExpression="OrderID" DataFormatString="d"/>
					<wijmo:C1BoundField DataField="ShipName" HeaderText="Ship name" SortExpression="ShipName" />
					<wijmo:C1BoundField DataField="ShipCity" HeaderText="Ship city" SortExpression="ShipCity"/>
					<wijmo:C1Band HeaderText="Dates">
						<Columns>
							<wijmo:C1BoundField DataField="OrderDate" HeaderText="Order date" DataFormatString="d" SortExpression="OrderDate" />
							<wijmo:C1BoundField DataField="RequiredDate" HeaderText="Required date" DataFormatString="d" SortExpression="RequiredDate" />
							<wijmo:C1BoundField DataField="ShippedDate" HeaderText="Shipped date" DataFormatString="d" SortExpression="ShippedDate" />
						</Columns>
					</wijmo:C1Band>
				</Columns>
			</wijmo:C1GridView>
		</ContentTemplate>
	</asp:UpdatePanel>

	<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\C1NWind.mdb;Persist Security Info=True"
		ProviderName="System.Data.OleDb" SelectCommand="SELECT TOP 10 [OrderID], [ShipName], [ShipCity], [OrderDate], [RequiredDate], [ShippedDate] FROM ORDERS">
	</asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1GridView.Columns_Text0 %></p>

	<p><%= Resources.C1GridView.Columns_Text1 %></p>
	<ul>
		<li><%= Resources.C1GridView.Columns_Li1 %></li>
		<li><%= Resources.C1GridView.Columns_Li2 %></li>
		<li><%= Resources.C1GridView.Columns_Li3 %></li>
	</ul>

	<p><%= Resources.C1GridView.Columns_Text2 %></p>
	<p><%= Resources.C1GridView.Columns_Text3 %></p>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<asp:UpdatePanel ID="UpdatePanel2" runat="server">
		<ContentTemplate>
			<div class="settingcontainer">
				<div class="settingcontent">
					<ul>
						<li class="fullwidth"> <asp:CheckBox ID="CheckBox4" runat="server" AutoPostBack="true" Text="<%$ Resources:C1GridView, Columns_ShowRowHeader %>"
					Checked="true" OnCheckedChanged="CheckBox1_CheckedChanged" /></li>
						<li class="fullwidth">
							<asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="true" Text="<%$ Resources:C1GridView, Columns_AllowColumnMoving %>"
					Checked="true" OnCheckedChanged="CheckBox1_CheckedChanged" />
						</li>
						<li class="fullwidth">
							<asp:CheckBox ID="CheckBox3" runat="server" AutoPostBack="true" Text="<%$ Resources:C1GridView, Columns_AllowSorting %>"
					Checked="true" OnCheckedChanged="CheckBox1_CheckedChanged" />
						</li>
					</ul>
				</div>
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
