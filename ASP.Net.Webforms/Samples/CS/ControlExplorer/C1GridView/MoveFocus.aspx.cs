﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ControlExplorer.C1GridView
{
    public partial class MoveFocus : System.Web.UI.Page
    {

        protected void dplkeyActionTab_SelectedIndexChanged(object sender, EventArgs e)
        {
            // apply new settings
            switch (dplkeyActionTab.SelectedItem.Value)
            {
                case "MoveAcross":
                    C1GridView1.KeyActionTab = C1.Web.Wijmo.Controls.C1GridView.KeyActionEnum.MoveAcross;
                    break;
                case "MoveAcrossOut":
                    C1GridView1.KeyActionTab = C1.Web.Wijmo.Controls.C1GridView.KeyActionEnum.MoveAcrossOut;
                    break;

            }
            UpdatePanel1.Update();
        }
    }
}