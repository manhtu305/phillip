<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Sorting.aspx.cs" Inherits="ControlExplorer.C1GridView.Sorting" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1GridView ID="C1GridView1" runat="server" DataSourceID="SqlDataSource1" AutoGenerateColumns="False" AllowSorting="True" style="top: 0px; left: 0px" AllowPaging="true" PageSize="15">
		<CallbackSettings Action="All" />		
		<Columns>
			<wijmo:C1BoundField DataField="ProductName" HeaderText="ProductName" SortExpression="ProductName">
			</wijmo:C1BoundField>
			<wijmo:C1BoundField DataField="UnitPrice" HeaderText="UnitPrice" SortExpression="UnitPrice" DataFormatString="c">
			</wijmo:C1BoundField>
			<wijmo:C1BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" DataFormatString="n0">
			</wijmo:C1BoundField>
			<wijmo:C1BoundField DataField="Discount" HeaderText="Discount" SortExpression="Discount" DataFormatString="p">
			</wijmo:C1BoundField>
			<wijmo:C1BoundField DataField="OrderDate" HeaderText="OrderDate" SortExpression="OrderDate" DataFormatString="MMM-dd-yyyy">
				<ItemStyle HorizontalAlign="Center" />
			</wijmo:C1BoundField>
			<wijmo:C1CheckBoxField DataField="Discontinued" HeaderText="Discontinued" SortExpression="Discontinued">
			</wijmo:C1CheckBoxField>
		</Columns>
	</wijmo:C1GridView>

	<asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:C1NWindConnectionString %>" ProviderName="<%$ ConnectionStrings:C1NWindConnectionString.ProviderName %>" SelectCommand="SELECT TOP 40 Products.ProductName, Products.Discontinued, Orders.OrderDate, [Order Details].UnitPrice, [Order Details].Quantity, [Order Details].Discount FROM (([Order Details] INNER JOIN Orders ON [Order Details].OrderID = Orders.OrderID) INNER JOIN Products ON [Order Details].ProductID = Products.ProductID) ORDER BY Orders.OrderDate DESC"></asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1GridView.Sorting_Text0 %></p>

	<p><%= Resources.C1GridView.Sorting_Text1 %></p>

	<p><%= Resources.C1GridView.Sorting_Text2 %></p>
	<ul>
		<li><%= Resources.C1GridView.Sorting_Li1 %></li>
	</ul>

	<p><%= Resources.C1GridView.Sorting_Text3 %></p>
	<ul>
		<li><%= Resources.C1GridView.Sorting_Li2 %></li>
		<li><%= Resources.C1GridView.Sorting_Li3 %></li>
	</ul>

	<p><%= Resources.C1GridView.Sorting_Text4 %></p>
</asp:Content>
