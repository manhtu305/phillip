<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="HierarchicalGrid.aspx.cs" Inherits="ControlExplorer.C1GridView.HierarchicalGrid" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1GridView" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<wijmo:C1GridView ID="Customers" runat="server" DataSourceID="SqlDataSource1" DataKeyNames="CustomerID, CompanyName" AutogenerateColumns="false" ShowRowHeader="false" AllowSorting="true"
		AllowPaging="true" PageSize="2"  AllowColMoving="true">
		<CallbackSettings Action="All" />

		<Columns>
			<wijmo:C1BoundField DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName" />
			<wijmo:C1BoundField DataField="Country" HeaderText="Country" SortExpression="Country" />
			<wijmo:C1BoundField DataField="City" HeaderText="City"  SortExpression="City" />
		</Columns>

		<Detail>
			<wijmo:C1DetailGridView runat="server" ID="Orders" DataSourceID="SqlDataSource2" DataKeyNames="OrderID" AutogenerateColumns="false" AllowSorting="true" AllowPaging="true" PageSize="2"  AllowColMoving="true">
				<Columns>
					<wijmo:C1BoundField DataField="ShippedDate" HeaderText="ShippedDate" SortExpression="ShippedDate" />
					<wijmo:C1BoundField DataField="Freight" HeaderText="Freight"  SortExpression="Freight" />
					<wijmo:C1BoundField DataField="ShipVia" HeaderText="ShipVia" SortExpression="ShipVia" />
				</Columns>

				<Relation>
					<wijmo:MasterDetailRelation DetailDataKeyName="CustomerID" MasterDataKeyName="CustomerID" />
				</Relation>

				<Detail>
					<wijmo:C1DetailGridView runat="server" ID="Details" DataSourceID="SqlDataSource3" DataKeyNames="OrderID" AutogenerateColumns="false" AllowPaging="true" PageSize="2" AllowColMoving="true">
						<Columns>
							<wijmo:C1BoundField DataField="UnitPrice" HeaderText="UnitPrice" SortExpression="UnitPrice" />
							<wijmo:C1BoundField DataField="Quantity" HeaderText="Quantity" SortExpression="Quantity" />
							<wijmo:C1BoundField DataField="Discount" HeaderText="Discount" SortExpression="Discount" />
						</Columns>
						<Relation>
							<wijmo:MasterDetailRelation DetailDataKeyName="OrderID" MasterDataKeyName="OrderID" />
						</Relation>
					</wijmo:C1DetailGridView>
				</Detail>
			</wijmo:C1DetailGridView> 
		</Detail>
	</wijmo:C1GridView>

	<asp:SqlDataSource ID="SqlDataSource1" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\C1NWind.mdb;Persist Security Info=True"
		ProviderName="System.Data.OleDb" runat="server"
		SelectCommand="SELECT TOP 5 * FROM Customers">
	</asp:SqlDataSource>

	<asp:SqlDataSource ID="SqlDataSource2" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\C1NWind.mdb;Persist Security Info=True"
		ProviderName="System.Data.OleDb" runat="server"
		SelectCommand="SELECT top 5 * FROM Orders Where CustomerID = @CustomerID">
		<SelectParameters>
			<asp:SessionParameter Name="CustomerID" SessionField="CustomerID" Type="string">
			</asp:SessionParameter>
		</SelectParameters>
	</asp:SqlDataSource>

	<asp:SqlDataSource ID="SqlDataSource3" ConnectionString="Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\C1NWind.mdb;Persist Security Info=True"
		ProviderName="System.Data.OleDb"
		SelectCommand="SELECT * FROM [Order Details] where OrderID = @OrderID"
		runat="server">
		<SelectParameters>
			<asp:SessionParameter Name="OrderID" SessionField="OrderID" Type="Int32"></asp:SessionParameter>
		</SelectParameters>
	</asp:SqlDataSource>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
</asp:Content>
