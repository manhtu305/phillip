<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="InitialView.aspx.cs" Inherits="ControlExplorer.C1Calendar.InitialView" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Calendar" TagPrefix="wijmo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<%= Resources.C1Calendar.InitialView_InitialViewDay %>
	<wijmo:C1Calendar runat="server" InitialView="Day"></wijmo:C1Calendar>
	<%= Resources.C1Calendar.InitialView_InitialViewMonth %>
	<wijmo:C1Calendar runat="server" InitialView="Month"></wijmo:C1Calendar>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
	<p><%= Resources.C1Calendar.InitialView_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>

