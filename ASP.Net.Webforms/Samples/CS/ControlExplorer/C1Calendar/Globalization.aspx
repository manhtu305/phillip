<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Globalization.aspx.cs" Inherits="ControlExplorer.C1Calendar.Globalization" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Calendar" TagPrefix="wijmo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
<wijmo:C1Calendar ID="C1Calendar1" runat="server">
</wijmo:C1Calendar>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
<p><%= Resources.C1Calendar.Globalization_Text0 %></p>
<p><%= Resources.C1Calendar.Globalization_Text1 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
			<ContentTemplate>
<div class="settingcontainer">
<div class="settingcontent">
	<ul>
		<li class="fullwidth"><label class="settinglegend"><%= Resources.C1Calendar.Globalization_ChangeCultureLabel %></label></li>
		<li class="fullwidth autoheight"><asp:RadioButtonList ID="RadioButtonList1" runat="server" 
        AutoPostBack="True" onselectedindexchanged="RadioButtonList1_SelectedIndexChanged">
    <asp:ListItem Value="zh-CN" Text="<%$ Resources:C1Calendar, Globalization_CultureZhCN %>"></asp:ListItem>
    <asp:ListItem Value="en-CA" Text="<%$ Resources:C1Calendar, Globalization_CultureEnCA %>"></asp:ListItem>
    <asp:ListItem Value="en-GB" Text="<%$ Resources:C1Calendar, Globalization_CultureEnGB %>"></asp:ListItem>
    <asp:ListItem Value="en-TT" Text="<%$ Resources:C1Calendar, Globalization_CultureEnTT %>"></asp:ListItem>
    <asp:ListItem Value="fr-CA" Text="<%$ Resources:C1Calendar, Globalization_CultureFrCA %>"></asp:ListItem>
    <asp:ListItem Value="fr-CH" Text="<%$ Resources:C1Calendar, Globalization_CultureFrCH %>"></asp:ListItem>
    </asp:RadioButtonList>
    </li>
	</ul></div>
</div>
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
