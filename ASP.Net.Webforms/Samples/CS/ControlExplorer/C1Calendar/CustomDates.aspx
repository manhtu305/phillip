<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CustomDates.aspx.cs" Inherits="ControlExplorer.C1Calendar.CustomDates" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Calendar" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function customizeDate($daycell, date, dayType, hover, preview) {
            if (date.getMonth() === 11 && date.getDate() === 25) {
                var $img = $('<div></div>').width(16).height(16).css('background-image', 'url(images/xmas.png)');

                $daycell.attr('align', 'right').empty().append($img);

                if ($daycell.hasClass('ui-datepicker-current-day')) {
                    $daycell.css('background-color', '#aaa');
                } else
                    $daycell.css('background-color', hover ? 'lightgray' : '');
                return true;
            }

            return false;
        }

    </script>
    <wijmo:C1Calendar ID="C1Calendar1" runat="server" DisplayDate="2011-12-01" OnClientCustomizeDate="customizeDate">
    </wijmo:C1Calendar>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Calendar.CustomDates_Text0 %></p>
    <p><%= Resources.C1Calendar.CustomDates_Text1 %></p>
    <p><%= Resources.C1Calendar.CustomDates_Text2 %></p>
        <ul>
            <li><%= Resources.C1Calendar.CustomDates_Li1 %></li>
            <li><%= Resources.C1Calendar.CustomDates_Li2 %></li>
            <li><%= Resources.C1Calendar.CustomDates_Li3 %></li>
            <li><%= Resources.C1Calendar.CustomDates_Li4 %></li>
            <li><%= Resources.C1Calendar.CustomDates_Li5 %></li>
        </ul>
    <p><%= Resources.C1Calendar.CustomDates_Text3 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
