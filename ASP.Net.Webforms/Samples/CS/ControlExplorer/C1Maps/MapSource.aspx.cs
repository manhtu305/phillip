﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.C1Maps;
using System.Collections.ObjectModel;
using System.Drawing;
namespace ControlExplorer.C1Maps
{
    public partial class MapSource : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (DropDownList1.SelectedValue)
            {
                case "BingMapsRoadSource":
                    C1Maps1.Source = C1.Web.Wijmo.Controls.C1Maps.MapSource.BingMapsRoadSource;
                    break;
                case "BingMapsAerialSource":
                    C1Maps1.Source = C1.Web.Wijmo.Controls.C1Maps.MapSource.BingMapsAerialSource;
                    break;
                case "BingMapsHybridSource":
                    C1Maps1.Source = C1.Web.Wijmo.Controls.C1Maps.MapSource.BingMapsHybridSource;
                    break;
                case "CustomSource":
                    C1Maps1.Source = C1.Web.Wijmo.Controls.C1Maps.MapSource.CustomSource;
                    SetCustomMapsource();
                    break;
                case "None":
                    C1Maps1.Source = C1.Web.Wijmo.Controls.C1Maps.MapSource.None;
                    break;
            }
        }

        private void SetCustomMapsource()
        {
            C1Maps1.CustomSource.MaxZoom = 22;
            C1Maps1.CustomSource.MinZoom = 1;
            C1Maps1.CustomSource.TileWidth = 256;
            C1Maps1.CustomSource.TileHeight = 256;
            C1Maps1.CustomSource.GetUrl = "getUrl";
        }
    }
}