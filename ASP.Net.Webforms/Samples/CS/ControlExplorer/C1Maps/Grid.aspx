<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="Grid.aspx.cs"
    Inherits="ControlExplorer.C1Maps.Grid" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Maps"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
    <style type="text/css">
        span.wijmo-wijmaps-vectorlayer-marklabel
        {
            font-size: 9px;
            color: #ccddff;
            white-space: nowrap;
        }
    </style>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="MainContent">
    <wijmo:C1Maps ID="C1Maps1" runat="server" Height="475px" Width="756px" 
        ShowTools="True" Source="BingMapsAerialSource" Zoom="2">
	    <Layers>
		    <wijmo:C1VectorLayer>
				<placemark labelvisibility="AutoHide" />
			</wijmo:C1VectorLayer>
	    </Layers>
	</wijmo:C1Maps>
</asp:Content>
<asp:Content ContentPlaceHolderID="Description" ID="Content3" runat="server">
    <p><%= Resources.C1Maps.Grid_Text0 %></p>
</asp:Content>