<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="OverView.aspx.cs"
    Inherits="ControlExplorer.C1Maps.OverView" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Maps"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="MainContent">
    <wijmo:C1Maps ID="C1Maps1" runat="server" Height="475px" Width="756px" 
        ShowTools="True" Source="BingMapsRoadSource" Zoom="2">
    </wijmo:C1Maps>
</asp:Content>
<asp:Content ContentPlaceHolderID="Description" ID="Content3" runat="server">
    <p><%= Resources.C1Maps.OverView_Text0 %></p>
</asp:Content>