<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="GeoJson.aspx.cs" Inherits="ControlExplorer.C1Maps.GeoJson" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Maps"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
    <script type="text/javascript">
        function onUsShapeCreated(e, d) {
            d.shape.attr("fill", "#00F").attr("fill-opacity", 0.2);
        }
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="MainContent">
    <wijmo:C1Maps ID="C1Maps1" runat="server" Height="475px" Width="756px" Zoom="2" 
        ShowTools="True" Source="BingMapsRoadSource" Center="0, 0">
    </wijmo:C1Maps>
</asp:Content>
<asp:Content ContentPlaceHolderID="Description" ID="Content3" runat="server">
    <p><%= Resources.C1Maps.GeoJson_Text0 %></p>
</asp:Content>