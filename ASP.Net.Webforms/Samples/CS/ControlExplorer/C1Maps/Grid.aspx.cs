﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.UI;
using C1.Web.Wijmo.Controls.C1Maps;

namespace ControlExplorer.C1Maps
{
	public partial class Grid : Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				(C1Maps1.Layers[0] as C1VectorLayer).DataWijJson.Vectors.AddRange(GetVectorsData());
			}
		}

		private readonly Color _lineColor = ColorTranslator.FromHtml("#d3d3d3");
		private const double StrokeOpacity = 0.6;
		private const double StrokeWidth = 1;
		private const string StrokeDashArray = "- ";

		private IEnumerable<C1VectorItemBase> GetVectorsData()
		{
			var vectors = new List<C1VectorItemBase>();
			for (var lon = -180; lon <= 180; lon += 30)
			{
				var vector = new C1VectorPolyline
				{
					Stroke = _lineColor,
					StrokeOpacity = StrokeOpacity,
					StrokeWidth = StrokeWidth,
					StrokeDashArray = StrokeDashArray
				};

				vector.Points.Add(new PointD(lon, 85));
				vector.Points.Add(new PointD(lon, -85));
				vectors.Add(vector);

				var lbl = Math.Abs(lon) + "°";
				if (lon > 0)
				{
					lbl += "E";
				}
				else if (lon < 0)
				{
					lbl += "W";
				}

				var placemark = new C1VectorPlacemark
				{
					Name = lbl,
					Point = new PointD(lon, 0)
				};

				vectors.Add(placemark);
			}

			for (var lat = -80; lat <= 80; lat += 20)
			{
				var vector = new C1VectorPolyline
				{
					Stroke = _lineColor,
					StrokeOpacity = StrokeOpacity,
					StrokeWidth = StrokeWidth,
					StrokeDashArray = StrokeDashArray
				};

				vector.Points.Add(new PointD(-180, lat));
				vector.Points.Add(new PointD(180, lat));
				vectors.Add(vector);

				var lbl = Math.Abs(lat) + "°";
				if (lat > 0)
				{
					lbl += "N";
				}
				else if (lat < 0)
				{
					lbl += "S";
				}

				var placemark = new C1VectorPlacemark
				{
					Name = lbl,
					Point = new PointD(0, lat)
				};

				vectors.Add(placemark);
			}

			return vectors;
		}
	}
}