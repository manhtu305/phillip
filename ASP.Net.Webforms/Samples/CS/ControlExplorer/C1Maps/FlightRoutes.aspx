<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="FlightRoutes.aspx.cs"
    Inherits="ControlExplorer.C1Maps.FlightRoutes" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ToolTip"
    TagPrefix="wijmo" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Maps"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
	<script type="text/javascript">

        var cities = [
        {
            image: "Resources/fromTarget.png",
            title: "Bern",
            latitude: 46.9480,
            longitude: 7.4481
        },
        {
            image: "Resources/fromTarget.png",
            title: "Kiev",
            latitude: 50.4422,
            longitude: 30.5367
        },
        {
            image: "Resources/toTarget.png",
            title: "Beijing",
            latitude: 39.9,
            longitude: 116.3,
        },
        {
            image: "Resources/toTarget.png",
            title: "Tokyo",
            latitude: 35.6,
            longitude: 139.7,
        },
        {
            image: "Resources/toTarget.png",
            title: "London",
            latitude: 51.5002,
            longitude: -0.1262,
        },
        {
            image: "Resources/toTarget.png",
            title: "Vilnius",
            latitude: 54.6896,
            longitude: 25.2799,
        },
        {
            image: "Resources/toTarget.png",
            title: "Brussels",
            latitude: 50.8371,
            longitude: 4.3676
        },
        {
            image: "Resources/toTarget.png",
            title: "Prague",
            latitude: 50.0878,
            longitude: 14.4205
        },
        {
            image: "Resources/toTarget.png",
            title: "Athens",
            latitude: 37.9792,
            longitude: 23.7166
        },
        {
            image: "Resources/toTarget.png",
            title: "Dublin",
            latitude: 53.3441,
            longitude: -6.2675
        },
        {
            image: "Resources/toTarget.png",
            title: "Oslo",
            latitude: 59.9138,
            longitude: 10.7387
        },
        {
            image: "Resources/toTarget.png",
            title: "Lisbon",
            latitude: 38.7072,
            longitude: -9.1355
        },
        {
            image: "Resources/toTarget.png",
            title: "Moscow",
            latitude: 55.7558,
            longitude: 37.6176
        },
        {
            image: "Resources/toTarget.png",
            title: "Belgrade",
            latitude: 44.8048,
            longitude: 20.4781
        },
        {
            image: "Resources/toTarget.png",
            title: "Bratislava",
            latitude: 48.2116,
            longitude: 17.1547
        },
        {
            image: "Resources/toTarget.png",
            title: "Ljubljana",
            latitude: 46.0514,
            longitude: 14.5060
        },
        {
            image: "Resources/toTarget.png",
            title: "Madrid",
            latitude: 40.4167,
            longitude: -3.7033
        },
        {
            image: "Resources/toTarget.png",
            title: "Stockholm",
            latitude: 59.3328,
            longitude: 18.0645
        },
        {
            image: "Resources/toTarget.png",
            title: "Paris",
            latitude: 48.8567,
            longitude: 2.3510
        }];

        function requestCities(paper) {
        	var result = { items: [] },
                len = cities.length;

        	for (var i = 0; i < len; i++) {
        		var city = cities[i];
        		var size;
        		if (city.title === "Bern" || city.title === "Kiev") {
        			size = { width: 30, height: 30 };
        		}
        		else {
        			size = { width: 20, height: 20 };
        		}

        		var item = {
        			"location": { x: city.longitude, y: city.latitude },
        			"size": size, pinpoint: { x: size.width / 2, y: size.height / 2 }
        		};

        		paper.setStart();
        		var img = paper.image(city.image, 0, 0, size.width, size.height);
        		$(img.node).wijtooltip({ content: city.title, position: { my: "left center", at: "right+20px top+10px" } });
        		var set = paper.setFinish();
        		item.elements = set;

        		result.items.push(item);
        	}
        	return result;
        }

        var curFlightRoutes,
			bernFlightRoutesDiv,
			kievFlightRoutesDiv;

        $(function () {
            var layerDivs = $('#<%=C1Maps1.ClientID%>').find(".wijmo-wijmaps-container>.wijmo-wijmaps-layercontainer>.wijmo-wijmaps-layercontainer");
        	bernFlightRoutesDiv = layerDivs.eq(1);
        	kievFlightRoutesDiv = layerDivs.eq(2);
        	switchFlightRoutes();
        });

        function showBernFlightRoutes() {
        	bernFlightRoutesDiv.show();
        	kievFlightRoutesDiv.hide();
        	$('#<%=C1Maps1.ClientID%>').c1maps({ "targetCenter": { x: 7.4481, y: 46.9480 }, "targetZoom": 3 });
		}

        function showKievFlightRoutes()
        {
        	kievFlightRoutesDiv.show();
        	bernFlightRoutesDiv.hide();
        	$('#<%=C1Maps1.ClientID%>').c1maps({ "targetCenter": { x: 30.5367, y: 50.4422 }, "targetZoom": 2 });
        }

        function switchFlightRoutes() {
            if (curFlightRoutes === "Kiev") {
                curFlightRoutes = "Bern";
                showBernFlightRoutes();
                $("#flightsFrom").text("Bern");
                $("#showFlightsFrom").text("Kiev");
            }
            else {
                curFlightRoutes = "Kiev";
                showKievFlightRoutes();
                $("#flightsFrom").text("Kiev");
                $("#showFlightsFrom").text("Bern");
            }
        }

    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="MainContent">
	<wijmo:C1ToolTip ID="C1ToolTip1" runat="server" TargetSelector=".maps" DisplayVisible="False">
    </wijmo:C1ToolTip>
    <div style="position:relative">
        <wijmo:C1Maps ID="C1Maps1" runat="server" Height="475px" Width="756px" 
            ShowTools="True" Source="BingMapsRoadSource" Center="7.4481, 46.9480" Zoom="1">
			<Layers>
				<wijmo:C1ItemsLayer Request="requestCities"/>
				<wijmo:C1VectorLayer DataType="WijJson">
					<DataWijjson>
						<Vectors>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="7.4481" Y="46.948" />
									<wijmo:PointD X="4.3676" Y="50.8371" />
								</Points>
							</wijmo:C1VectorPolyline>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="7.4481" Y="46.948" />
									<wijmo:PointD X="10.7387" Y="59.9138" />
								</Points>
							</wijmo:C1VectorPolyline>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="7.4481" Y="46.948" />
									<wijmo:PointD X="-3.7033" Y="40.4167" />
								</Points>
							</wijmo:C1VectorPolyline>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="7.4481" Y="46.948" />
									<wijmo:PointD X="14.4205" Y="50.0878" />
								</Points>
							</wijmo:C1VectorPolyline>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="7.4481" Y="46.948" />
									<wijmo:PointD X="17.1547" Y="48.2116" />
								</Points>
							</wijmo:C1VectorPolyline>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="7.4481" Y="46.948" />
									<wijmo:PointD X="20.4781" Y="44.8048" />
								</Points>
							</wijmo:C1VectorPolyline>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="7.4481" Y="46.948" />
									<wijmo:PointD X="37.6176" Y="55.7558" />
								</Points>
							</wijmo:C1VectorPolyline>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="7.4481" Y="46.948" />
									<wijmo:PointD X="23.7166" Y="37.9792" />
								</Points>
							</wijmo:C1VectorPolyline>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="7.4481" Y="46.948" />
									<wijmo:PointD X="-6.2675" Y="53.3441" />
								</Points>
							</wijmo:C1VectorPolyline>
						</Vectors>
					</DataWijjson>
				</wijmo:C1VectorLayer>
				<wijmo:C1VectorLayer DataType="WijJson">
					<DataWijjson>
						<Vectors>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="30.5367" Y="50.4422" />
									<wijmo:PointD X="18.0645" Y="59.3328" />
								</Points>
							</wijmo:C1VectorPolyline>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="30.5367" Y="50.4422" />
									<wijmo:PointD X="-3.7033" Y="40.4167" />
								</Points>
							</wijmo:C1VectorPolyline>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="30.5367" Y="50.4422" />
									<wijmo:PointD X="14.5060" Y="46.0514" />
								</Points>
							</wijmo:C1VectorPolyline>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="30.5367" Y="50.4422" />
									<wijmo:PointD X="17.1547" Y="48.2116" />
								</Points>
							</wijmo:C1VectorPolyline>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="30.5367" Y="50.4422" />
									<wijmo:PointD X="20.4781" Y="44.8048" />
								</Points>
							</wijmo:C1VectorPolyline>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="30.5367" Y="50.4422" />
									<wijmo:PointD X="37.6176" Y="55.7558" />
								</Points>
							</wijmo:C1VectorPolyline>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="30.5367" Y="50.4422" />
									<wijmo:PointD X="-9.1355" Y="38.7072" />
								</Points>
							</wijmo:C1VectorPolyline>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="30.5367" Y="50.4422" />
									<wijmo:PointD X="25.2799" Y="54.6896" />
								</Points>
							</wijmo:C1VectorPolyline>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="30.5367" Y="50.4422" />
									<wijmo:PointD X="139.7" Y="35.6" />
								</Points>
							</wijmo:C1VectorPolyline>
							<wijmo:C1VectorPolyline Stroke="Red" StrokeOpacity="0.6">
								<Points>
									<wijmo:PointD X="30.5367" Y="50.4422" />
									<wijmo:PointD X="116.3" Y="39.9" />
								</Points>
							</wijmo:C1VectorPolyline>
						</Vectors>
					</DataWijjson>
				</wijmo:C1VectorLayer>
			</Layers>
        </wijmo:C1Maps>
        <div style="position: absolute; left:80px; top:30px">
            <img src="Resources/plane.png" style="width:20px; height:20px;" />
            <span style="font-size: 18px; color: #990000;">Flights from <span id="flightsFrom">Bern</span></span><br />
            <a href="javascript: switchFlightRoutes(); void(0);"><span style="font-size: 11px; margin-left:24px;">show flights from <span id="showFlightsFrom">Kiev</span></span></a>
        </div>
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="Description" ID="Content3" runat="server">
    <p><%= Resources.C1Maps.FlightRoutes_Text0 %></p>
	<p><%= Resources.C1Maps.FlightRoutes_Text1 %></p>
	<ul>
		<li><%= Resources.C1Maps.FlightRoutes_Li1 %></li>
		<li><%= Resources.C1Maps.FlightRoutes_Li2 %></li>
		<li><%= Resources.C1Maps.FlightRoutes_Li3 %></li>
		<li><%= Resources.C1Maps.FlightRoutes_Li4 %></li>
		<li><%= Resources.C1Maps.FlightRoutes_Li5 %></li>
	</ul>
</asp:Content>