<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="Cities.aspx.cs"
    Inherits="ControlExplorer.C1Maps.Cities" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Maps"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
    <style type="text/css">
        span.wijmo-wijmaps-vectorlayer-marklabel
        {
            font-size: 9px;
            color: #ccddff;
            white-space: nowrap;
        }
    </style>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="MainContent">
    <wijmo:C1Maps ID="C1Maps1" runat="server" Height="475px" Width="756px" Zoom="2"
        ShowTools="True" Source="BingMapsAerialSource">
    </wijmo:C1Maps>
</asp:Content>
<asp:Content ContentPlaceHolderID="Description" ID="Content3" runat="server">
    <p><%= Resources.C1Maps.Cities_Text0 %></p>
</asp:Content>