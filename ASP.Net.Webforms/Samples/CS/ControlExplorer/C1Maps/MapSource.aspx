<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="MapSource.aspx.cs"
    Inherits="ControlExplorer.C1Maps.MapSource" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Maps"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
    <script type="text/javascript">
	    function getUrl(zoom, x, y) {
		    var uriFormat = "http://mt{subdomain}.google.cn/vt/lyrs=r&hl=en-us&gl=cn&x={x}&y={y}&z={zoom}";
		    var subdomains = ["0", "1", "2", "3"];
		    var subdomain = subdomains[(y * Math.pow(2, zoom) + x) % subdomains.length];
		    return uriFormat.replace("{subdomain}", subdomain).replace("{x}", x).replace("{y}", y).replace("{zoom}", zoom);
	    }
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="MainContent">
    <wijmo:C1Maps ID="C1Maps1" runat="server" Height="475px" Width="756px" 
        ShowTools="True" Source="BingMapsAerialSource" Zoom="2">
    </wijmo:C1Maps>
</asp:Content>
<asp:Content ContentPlaceHolderID="Description" ID="Content3" runat="server">
    <p><%= Resources.C1Maps.MapSource_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="settingcontainer">
        <div class="settingcontent">
            <ul>
                <li class="fullwidth">
                    <label>
                        <%= Resources.C1Maps.MapSource_MapSource %></label>
                </li>
                <li>
                    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="DropDownList1_SelectedIndexChanged" Width="160px">
                        <asp:ListItem Value="BingMapsAerialSource">BingMapsAerialSource</asp:ListItem>
                        <asp:ListItem Value="BingMapsRoadSource">BingMapsRoadSource</asp:ListItem>
                        <asp:ListItem Value="BingMapsHybridSource">BingMapsHybridSource</asp:ListItem>
                        <asp:ListItem Value="CustomSource">CustomSource</asp:ListItem>
                        <asp:ListItem Value="None">None</asp:ListItem>
                    </asp:DropDownList>
               </li>
            </ul>
        </div>
    </div>
</asp:Content>