<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="Marks.aspx.cs"
    Inherits="ControlExplorer.C1Maps.Marks" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Maps"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
    <script type="text/javascript">
        $(function () {
            $('#<%=C1Maps1.ClientID%>').c1maps("option", {
                layers: [
                {
                    type: "vector",
                    data: { vectors: [] }
                },
                {
                    type: "vector",
                    data: { vectors: [] },
                    placemark: {
                        labelVisibility: "hidden",
                        image: "Resources/google_placemarker.png",
                        size: { width: 50, height: 35 },
                        pinPoint: { x: 24, y: 32 },
                        render: function (d) {
                            return $("<div style='width:50px;height:35px;cursor:pointer;'><img style='position:absolute;left:0px;top:0px;' src='Resources/google_placemarker.png' /><div style='color:white;font-size:10pt;position:absolute;top:5px;width:100%;height:100%;text-align:center;'>" + d.vector.data.name + "</div></div>")
                            .on("click", function (e) {
                                markclicked(e, d.vector.data);
                            });
                        }
                    }
                }]
            }).off(".layer")
            .on("click.layer", function (e) {
                if (window.layerMoved || e.button !== 0) return;
                var offset = $('#<%=C1Maps1.ClientID%>').offset();
                var point = { x: e.pageX - offset.left, y: e.pageY - offset.top };
                var geoPoint = $('#<%=C1Maps1.ClientID%>').c1maps("viewToGeographic", point);
                var layers = $('#<%=C1Maps1.ClientID%>').c1maps("option", "layers");

                var layerMark = layers[1];
                var vector = {
                    type: "placemark",
                    coordinates: [geoPoint.x, geoPoint.y],
                    name: layerMark.data.vectors.length + 1 + ""
                };
                layerMark.data.vectors.push(vector);

                var layerLine = layers[0];
                var coords = [];
                var vectors = layerMark.data.vectors;
                for (var i = 0; i < vectors.length; i++) {
                    var mark = vectors[i];
                    coords.push(mark.coordinates[0]);
                    coords.push(mark.coordinates[1]);
                }
                var line = {
                    type: "polyline",
                    coordinates: coords,
                    stroke: "gray",
                    strokeWidth: "2",
                    strokeDashArray: "- "
                };
                layerLine.data.vectors[0] = line;

                $('#<%=C1Maps1.ClientID%>').c1maps("refreshLayers");
            })
            .on("mousedown.layer", function (e) {
                window.layerMoved = false;
            })
            .on("mousemove.layer", function (e) {
                window.layerMoved = true;
            });
        });

        function markclicked(e, d) {
            e.stopPropagation();

            var layers = $('#<%=C1Maps1.ClientID%>').c1maps("option", "layers");
            var layerMark = layers[1];
            var vectors = layerMark.data.vectors;
            var index = $.inArray(d, vectors);
            vectors.splice(index, 1);
            for (var i = 0; i < vectors.length; i++) {
                vectors[i].name = i + 1 + "";
            }

            var layerLine = layers[0];
            var coords = [];
            for (var i = 0; i < vectors.length; i++) {
                var mark = vectors[i];
                coords.push(mark.coordinates[0]);
                coords.push(mark.coordinates[1]);
            }
            var line = {
                type: "polyline",
                coordinates: coords,
                stroke: "gray",
                strokeWidth: "2",
                strokeDashArray: "- "
            };
            layerLine.data.vectors[0] = line;

            $('#<%=C1Maps1.ClientID%>').c1maps("refreshLayers");

            return false;
        }
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="MainContent">
    <wijmo:C1Maps ID="C1Maps1" runat="server" Height="475px" Width="756px" 
        ShowTools="True" Source="BingMapsRoadSource" Zoom="2">
    </wijmo:C1Maps>
</asp:Content>
<asp:Content ContentPlaceHolderID="Description" ID="Content3" runat="server">
    <p><%= Resources.C1Maps.Marks_Text0 %></p>
</asp:Content>