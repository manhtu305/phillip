﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.C1Maps;
using C1.Web.Wijmo.Controls.C1Maps.GeoJson;
using GeoJsonFeature = C1.Web.Wijmo.Controls.C1Maps.GeoJson.Feature;
using System.Drawing;

namespace ControlExplorer.C1Maps
{
	public partial class GeoJson : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				var uslayer = new C1VectorLayer();
				var countriesLayer = new C1VectorLayer();
				C1Maps1.Layers.Add(uslayer);
				C1Maps1.Layers.Add(countriesLayer);

				uslayer.OnClientShapeCreated = "onUsShapeCreated";
				uslayer.DataType = DataType.GeoJson;
				uslayer.DataUrl = "Resources/us.geo.json";

				countriesLayer.DataType = DataType.GeoJson;
				countriesLayer.DataUrl = "Resources/countries.geo.json";
			}
		}
	}
}