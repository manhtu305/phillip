<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="MapChart.aspx.cs"
    Inherits="ControlExplorer.C1Maps.MapChart" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Maps"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
    <script type="text/javascript">
        $(function () {
            var colorValues =
            [
                { color: "rgb(241, 244, 255)", value: 0 },
                { color: "rgb(241, 244, 255)", value: 5000 },
                { color: "rgb(224, 224, 246)", value: 10000 },
                { color: "rgb(203, 205, 255)", value: 20000 },
                { color: "rgb(179, 182, 230)", value: 50000 },
                { color: "rgb(156, 160, 240)", value: 100000 },
                { color: "rgb(127, 132, 243)", value: 200000 },
                { color: "rgb(89, 97, 230)", value: 500000 },
                { color: "rgb(56, 64, 217)", value: 1000000 },
                { color: "rgb(19, 26, 148)", value: 2000000 }
            ];

            var legendList = $(".legend ul");
            $.each(colorValues, function (index, val) {
                var legendItem = $("<span class=\"legend-item\"></span>").css("background-color", val.color);
                var legendText = $("<span></span>").text(val.value);
                $("<li></li>").append(legendItem).append(legendText).appendTo(legendList);
            });
        });
    </script>
    <style type="text/css">
        .c1maps
        {
            float: left;
        }

        .legend
        {
            float: left;
            margin-left: 5px;
            margin-top: 15px;
        }

        .legend ul
        {
            list-style: none;
            font-size: 10px;
        }

        .legend .legend-item
        {
            width: 20px;
            height: 20px;
            display: inline-block;
            margin-right: 2px;
        }
    </style>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="MainContent">
    <wijmo:C1Maps ID="C1Maps1" CssClass="c1maps" runat="server" Height="475px" Width="756px"
        ShowTools="True" Source="None" Zoom="1">
    </wijmo:C1Maps>
    <div class="legend">
        <span><%= Resources.C1Maps.MapChart_Text1 %></span>
        <ul>
        </ul>
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="Description" ID="Content3" runat="server">
    <p><%= Resources.C1Maps.MapChart_Text0 %></p>
</asp:Content>
