<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="Factories.aspx.cs"
    Inherits="ControlExplorer.C1Maps.Factories" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ToolTip"
    TagPrefix="wijmo" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Maps"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
    <script type="text/javascript">
        var factoriesDataBase,
            minStoreZoom = 10,
            storeSlices = Math.floor(Math.pow(2, minStoreZoom));
        
        function loadFactoriesDataBase() {
            $.ajax({
                url: "Resources/factories.json",
                data: null,
                context: null,
                success: function (result) {
                    factoriesDataBase = result;
                    minStoreZoom = 10;
                    storeSlices = Math.floor(Math.pow(2, minStoreZoom));
                },
                error: null,
                dataType: "json",
                async: false
            });
        }

        function requestFactories(paper, zoom, maxZoom, lowerLeft, upperRight) {
            if (!factoriesDataBase) {
                loadFactoriesDataBase();
            }

            var result = { items: [] },
                factories = factoriesDataBase.factories,
                len = factories.length;
            for (var i = 0; i < len; i++) {
                var factory = factories[i];
                if (factory.longitude <= lowerLeft.x ||
                    factory.longitude > upperRight.x ||
                    factory.latitude <= lowerLeft.y ||
                    factory.latitude > upperRight.y) {
                    continue;
                }

                var item = {
                    location: { x: factory.longitude, y: factory.latitude },
                    size: { width: 60, height: 60 }, pinpoint: { x: 30, y: 30 }
                };

                paper.setStart();
                var img = paper.image("Resources/factory.png", 0, 0, 60, 60);
                $(img.node).wijtooltip({
                    content: factory.name, position: { my: "left center", at: "right+30px top+30px" },
                    showing: function (e, o) {
                        currentTooltip = o;
                    }, hiding: function (e) {
                        currentTooltip = null;
                    }
                });

                var set = paper.setFinish();
                set.data("name", factory.name);
                item.elements = set;

                result.items.push(item);
            }
            return result;
        }

        var currentTooltip;
        function targetZoomChanged(e, o) {
            if (currentTooltip) {
                currentTooltip.hide();
                currentTooltip = null;
            }
        }

        function requestOffices(paper) {
            if (!factoriesDataBase) {
                loadFactoriesDataBase();
            }

            var result = { items: [] },
                offices = factoriesDataBase.offices,
                len = offices.length;

            for (var i = 0; i < len; i++) {
                var office = offices[i];

                var item = {
                    location: { x: office.longitude, y: office.latitude },
                    size: { width: 60, height: 60 }, pinpoint: { x: 30, y: 30 }
                };

                paper.setStart();
                var img = paper.image("Resources/office.png", 0, 0, 60, 60);
                $(img.node).wijtooltip({
                    content: office.name, position: { my: "left center", at: "right+30px top+30px" },
                    showing: function (e, o) {
                        currentTooltip = o;
                    }, hiding: function (e) {
                        currentTooltip = null;
                    }
                });

                paper.circle(50, 10, 10).attr("fill", "#222222");
                paper.text(50, 10, office.stores).attr("fill", "white");
                var set = paper.setFinish();
                item.elements = set;

                result.items.push(item);
            }
            return result;
        }

        function requestStores(paper, minZoom, maxZoom, lowerLeft, upperRight) {
            if (!factoriesDataBase) {
                loadFactoriesDataBase();
            }

            if (minZoom < minStoreZoom)
                return null;

            var result = { items: [] },
                stores = factoriesDataBase.stores,
                len = stores.length;

            for (var i = 0; i < len; i++) {
                var store = stores[i];
                if (store.longitude <= lowerLeft.x ||
                    store.longitude > upperRight.x ||
                    store.latitude <= lowerLeft.y ||
                    store.latitude > upperRight.y) {
                    continue;
                }

                var item = {
                    location: { x: store.longitude, y: store.latitude },
                    size: { width: 60, height: 60 }, pinpoint: { x: 30, y: 30 }
                };

                paper.setStart();
                var img = paper.image("Resources/store.png", 0, 0, 60, 60);
                $(img.node).wijtooltip({
                    content: store.name, position: { my: "left center", at: "right+30px top+30px" },
                    showing: function (e, o) {
                        currentTooltip = o;
                    }, hiding: function (e) {
                        currentTooltip = null;
                    }
                });

                var set = paper.setFinish();
                item.elements = set;

                result.items.push(item);
            }
            return result;
        }
    </script>
</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="MainContent">
    <wijmo:C1ToolTip ID="C1ToolTip1" runat="server" TargetSelector=".maps" DisplayVisible="False">
    </wijmo:C1ToolTip>
    <wijmo:C1Maps ID="C1Maps1" runat="server" Height="475px" Width="756px" OnClientTargetZoomChanged="targetZoomChanged"
        ShowTools="True" Source="BingMapsRoadSource" Zoom="2">
		<Layers>
			<wijmo:C1VirtualLayer Request="requestFactories">
				<Slices>
					<wijmo:MapSlice LatitudeSlices="1" LongitudeSlices="1" Zoom="0" />
				</Slices>
			</wijmo:C1VirtualLayer>
			<wijmo:C1ItemsLayer Request="requestOffices" />
			<wijmo:C1VirtualLayer Request="requestStores">
				<Slices>
					<wijmo:MapSlice LatitudeSlices="1" LongitudeSlices="1" Zoom="0" />
					<wijmo:MapSlice LatitudeSlices="1024" LongitudeSlices="1024" Zoom="10" />
				</Slices>
			</wijmo:C1VirtualLayer>
		</Layers>
    </wijmo:C1Maps>
</asp:Content>
<asp:Content ContentPlaceHolderID="Description" ID="Content3" runat="server">
    <p><%= Resources.C1Maps.Factories_Text0 %></p>
</asp:Content>