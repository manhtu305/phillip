function saveFile(data, setting) {
	if (!data) {
		throw "Empty file data!";
	}

	if (data.type.indexOf("json") > -1 ||
		(data.type.indexOf("xml") > -1 && data.type.indexOf("openxml") < 0)) {
		var reader = new FileReader();
		reader.addEventListener("loadend", function (e) {
			throw reader.result;
		});
		
		reader.readAsText(data);
		return;
	}
	
	saveAs(data, getFileName(setting));
}

function getFileName(setting) {
	var fileName = setting.fileName.trim();
	if (!fileName || !fileName.length) fileName = "export";
	switch (setting.exportFileType) {
	case wijmo.exporter.ExportFileType.Pdf:
		return fileName + ".pdf";
	case wijmo.exporter.ExportFileType.Xls:
		return fileName + ".xls";
	case wijmo.exporter.ExportFileType.Xlsx:
		return fileName + ".xlsx";
	case wijmo.exporter.ExportFileType.Csv:
		return fileName + ".csv";
	case wijmo.exporter.ExportFileType.Jpg:
		return fileName + ".jpg";
	case wijmo.exporter.ExportFileType.Bmp:
		return fileName + ".bmp";
	case wijmo.exporter.ExportFileType.Gif:
		return fileName + ".gif";
	case wijmo.exporter.ExportFileType.Tiff:
		return fileName + ".tiff";
	case wijmo.exporter.ExportFileType.Png:
		return fileName + ".png";
	}
	return fileName;
}