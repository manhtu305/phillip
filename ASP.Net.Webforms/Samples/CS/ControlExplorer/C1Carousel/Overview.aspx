<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="Overview.aspx.cs" Inherits="ControlExplorer.C1Carousel.Overview" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Carousel"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1Carousel ID="C1Carousel1" runat="server" Width="750px" Height="300px" Display="1"
        EnableTheming="True">
        <Items>
            <wijmo:C1CarouselItem runat="server" ImageUrl="~/Images/Sports/1.jpg" Caption="Sport 1">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem runat="server" ImageUrl="~/Images/Sports/2.jpg" Caption="Sport 2">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem runat="server" ImageUrl="~/Images/Sports/3.jpg" Caption="Sport 3">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem runat="server" ImageUrl="~/Images/Sports/4.jpg" Caption="Sport 4">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem runat="server" ImageUrl="~/Images/Sports/5.jpg" Caption="Sport 5">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem runat="server" ImageUrl="~/Images/Sports/6.jpg" Caption="Sport 6">
            </wijmo:C1CarouselItem>
        </Items>
        <PagerPosition>
            <My Left="Right"></My>
            <At Top="Bottom" Left="Right"></At>
        </PagerPosition>
    </wijmo:C1Carousel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Carousel.Overview_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
