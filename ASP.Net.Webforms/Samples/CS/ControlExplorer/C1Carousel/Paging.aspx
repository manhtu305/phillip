<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="Paging.aspx.cs" Inherits="ControlExplorer.C1Carousel.Paging" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Carousel"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h3><%= Resources.C1Carousel.Paging_NumbersTitle %></h3>
    <wijmo:C1Carousel ID="C1Carousel1" runat="server" Width="750px" Height="300px" Display="1"
        ShowPager="true" PagerType="Numbers" EnableTheming="True">
        <Items>
            <wijmo:C1CarouselItem ID="C1CarouselItem1" runat="server" ImageUrl="~/Images/Sports/1.jpg"
                Caption="Sport 1">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem2" runat="server" ImageUrl="~/Images/Sports/2.jpg"
                Caption="Sport 2">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem3" runat="server" ImageUrl="~/Images/Sports/3.jpg"
                Caption="Sport 3">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem4" runat="server" ImageUrl="~/Images/Sports/4.jpg"
                Caption="Sport 4">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem5" runat="server" ImageUrl="~/Images/Sports/5.jpg"
                Caption="Sport 5">
            </wijmo:C1CarouselItem>
        </Items>
        <PagerPosition>
            <My Left="Right"></My>
            <At Top="Bottom" Left="Right"></At>
        </PagerPosition>
    </wijmo:C1Carousel>
    <br />
    <h3><%= Resources.C1Carousel.Paging_DotsTitle %></h3>
    <br />
    <wijmo:C1Carousel ID="C1Carousel2" runat="server" Width="750px" Height="300px" Display="1"
        ShowPager="true" PagerType="Dots" EnableTheming="True">
        <Items>
            <wijmo:C1CarouselItem ID="C1CarouselItem7" runat="server" ImageUrl="~/Images/Cities/1.jpg"
                Caption="City 1">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem8" runat="server" ImageUrl="~/Images/Cities/2.jpg"
                Caption="City 2">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem9" runat="server" ImageUrl="~/Images/Cities/3.jpg"
                Caption="City 3">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem10" runat="server" ImageUrl="~/Images/Cities/4.jpg"
                Caption="City 4">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem11" runat="server" ImageUrl="~/Images/Cities/5.jpg"
                Caption="City 5">
            </wijmo:C1CarouselItem>
        </Items>
        <PagerPosition>
            <My Left="Center" Top="Top"></My>
            <At Left="Center" Top="Bottom"></At>
        </PagerPosition>
    </wijmo:C1Carousel>
    <br />
    <h3><%= Resources.C1Carousel.Paging_SliderTitle %></h3>
    <br />
    <wijmo:C1Carousel ID="C1Carousel3" runat="server" Width="750px" Height="300px" Display="1"
        ShowPager="true" PagerType="Slider" EnableTheming="True">
        <Items>
            <wijmo:C1CarouselItem ID="C1CarouselItem13" runat="server" ImageUrl="~/Images/Sports/1.jpg"
                Caption="World 1">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem14" runat="server" ImageUrl="~/Images/Sports/2.jpg"
                Caption="World 2">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem15" runat="server" ImageUrl="~/Images/Sports/3.jpg"
                Caption="World 3">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem16" runat="server" ImageUrl="~/Images/Sports/4.jpg"
                Caption="World 4">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem17" runat="server" ImageUrl="~/Images/Sports/5.jpg"
                Caption="World 5">
            </wijmo:C1CarouselItem>
        </Items>
        <PagerPosition>
            <My Left="Center" Top="Top"></My>
            <At Left="Center" Top="Bottom"></At>
        </PagerPosition>
    </wijmo:C1Carousel>
    <br />
    <br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Carousel.Paging_Text0 %></p>
</asp:Content>
