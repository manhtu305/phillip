<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Animation.aspx.cs" Inherits="ControlExplorer.C1Carousel.Animation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Carousel"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
	<wijmo:C1Carousel ID="C1Carousel1" runat="server" Width="750px" Height="300px" Display="1"
		EnableTheming="True">
		<Items>
			<wijmo:C1CarouselItem ID="C1CarouselItem1" runat="server" ImageUrl="~/Images/Sports/1.jpg" Caption="Word Caption 1">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem2" runat="server" ImageUrl="~/Images/Sports/2.jpg" Caption="Word Caption 2">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem3" runat="server" ImageUrl="~/Images/Sports/3.jpg" Caption="Word Caption 3">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem4" runat="server" ImageUrl="~/Images/Sports/4.jpg" Caption="Word Caption 4">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem5" runat="server" ImageUrl="~/Images/Sports/5.jpg" Caption="Word Caption 5">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem6" runat="server" ImageUrl="~/Images/Sports/6.jpg" Caption="Word Caption 6">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem7" runat="server" ImageUrl="~/Images/Sports/7.jpg" Caption="Word Caption 7">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem8" runat="server" ImageUrl="~/Images/Sports/8.jpg" Caption="Word Caption 8">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem9" runat="server" ImageUrl="~/Images/Sports/9.jpg" Caption="Word Caption 9">
			</wijmo:C1CarouselItem>
			<wijmo:C1CarouselItem ID="C1CarouselItem10" runat="server" ImageUrl="~/Images/Sports/10.jpg" Caption="Word Caption 10">
			</wijmo:C1CarouselItem>
		</Items>
		<PagerPosition>
			<My Left="Right"></My>
			<At Top="Bottom" Left="Right"></At>
		</PagerPosition>
	</wijmo:C1Carousel>
				</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
<asp:UpdatePanel ID="UpdatePanel2" runat="server">
<ContentTemplate>
<div class="settingcontainer">
<div class="settingcontent">
<ul>
	<li class="fullwidth"><label class="settinglegend"><%= Resources.C1Carousel.Animation_AnimationLabel %></label>
	<li>
		<label><%= Resources.C1Carousel.Animation_EasingLabel %></label>
		<asp:DropDownList id="EasingDdl" runat="server">
			<asp:ListItem value="Swing">Swing</asp:ListItem>
			<asp:ListItem value="Linear">Linear</asp:ListItem>
			<asp:ListItem value="EaseInQuad" selected="true">EaseInQuad</asp:ListItem>
			<asp:ListItem value="EaseOutQuad">EaseOutQuad</asp:ListItem>
			<asp:ListItem value="EaseInOutQuad">EaseInOutQuad</asp:ListItem>
			<asp:ListItem value="EaseInCubic">EaseInCubic</asp:ListItem>
			<asp:ListItem value="EaseOutCubic">EaseOutCubic</asp:ListItem>
			<asp:ListItem value="EaseInOutCubic">EaseInOutCubic</asp:ListItem>
			<asp:ListItem value="EaseInQuart">EaseInQuart</asp:ListItem>
			<asp:ListItem value="EaseOutQuart">EaseOutQuart</asp:ListItem>
			<asp:ListItem value="EaseInOutQuart">EaseInOutQuart</asp:ListItem>
			<asp:ListItem value="EaseInQuint">EaseInQuint</asp:ListItem>
			<asp:ListItem value="EaseOutQuint">EaseOutQuint</asp:ListItem>
			<asp:ListItem value="EaseInOutQuint">EaseInOutQuint</asp:ListItem>
			<asp:ListItem value="EaseInSine">EaseInSine</asp:ListItem>
			<asp:ListItem value="EaseOutSine">EaseOutSine</asp:ListItem>
			<asp:ListItem value="EaseInOutSine">EaseInOutSine</asp:ListItem>
			<asp:ListItem value="EaseInExpo">EaseInExpo</asp:ListItem>
			<asp:ListItem value="EaseOutExpo">EaseOutExpo</asp:ListItem>
			<asp:ListItem value="EaseInOutExpo">EaseInOutExpo</asp:ListItem>
			<asp:ListItem value="EaseInCirc">EaseInCirc</asp:ListItem>
			<asp:ListItem value="EaseOutCirc">EaseOutCirc</asp:ListItem>
			<asp:ListItem value="EaseInOutCirc">EaseInOutCirc</asp:ListItem>
			<asp:ListItem value="EaseInElastic">EaseInElastic</asp:ListItem>
			<asp:ListItem value="EaseOutElastic">EaseOutElastic</asp:ListItem>
			<asp:ListItem value="EaseInOutElastic">EaseInOutElastic</asp:ListItem>
			<asp:ListItem value="EaseInBack">EaseInBack</asp:ListItem>
			<asp:ListItem value="EaseOutBack">EaseOutBack</asp:ListItem>
			<asp:ListItem value="EaseInOutBack">EaseInOutBack</asp:ListItem>
			<asp:ListItem value="EaseInBounce">EaseInBounce</asp:ListItem>
			<asp:ListItem value="EaseOutBounce">EaseOutBounce</asp:ListItem>
			<asp:ListItem value="EaseInOutBounce">EaseInOutBounce</asp:ListItem>
		</asp:DropDownList>
	</li>
	<li>
		<label><%= Resources.C1Carousel.Animation_DurationLabel %></label>
		<asp:TextBox runat="server" ID="DurationTxt" Text="1000" />
	</li>
</ul>
</div>
	<div class="settingcontrol">
	<asp:Button ID="ApplyBtn" Text="<%$ Resources:C1Carousel, Animation_ApplyText %>" CssClass="settingapply" runat="server" OnClick="ApplyBtn_Click"/>
	</div>                    
</div>
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
<p><%= Resources.C1Carousel.Animation_Text0 %></p>
</asp:Content>
