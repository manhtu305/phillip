<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="LightBox.aspx.cs" Inherits="ControlExplorer.C1Carousel.LightBox" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Carousel"
    TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1LightBox" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <script type="text/javascript">
        function onItemClick(event, ui) {
            var a = ui.el.children("a"), img = a.children("img:eq(0)"),
			url = a.attr("href"), title = img.attr("title");
            if (!a.is(":wijmo-wijlightbox")) {
                a.attr("rel", "wijlightbox[stock];player=img");
                var text = a.siblings(".wijmo-wijcarousel-text").children("span").text();
                a.children("img").attr("alt", text);
                a.wijlightbox();
            }
            a.trigger("click");
            event.preventDefault();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1Carousel ID="C1Carousel1" runat="server" Display="5" Step="4" Loop="true"
        EnableTheming="True" OnClientItemClick="onItemClick" Width="650px" Height="90px">
        <Items>
            <wijmo:C1CarouselItem ID="C1CarouselItem1" runat="server" ImageUrl="~/Images/Sports/1_Small.jpg"
                LinkUrl="~/Images/Sports/1.jpg" Caption="Sports 1">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem2" runat="server" ImageUrl="~/Images/Sports/2_Small.jpg"
                LinkUrl="~/Images/Sports/2.jpg" Caption="Sports 2">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem3" runat="server" ImageUrl="~/Images/Sports/3_Small.jpg"
                LinkUrl="~/Images/Sports/3.jpg" Caption="Sports 3">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem4" runat="server" ImageUrl="~/Images/Sports/4_Small.jpg"
                LinkUrl="~/Images/Sports/4.jpg" Caption="Sports 4">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem5" runat="server" ImageUrl="~/Images/Sports/5_Small.jpg"
                LinkUrl="~/Images/Sports/5.jpg" Caption="Sports 5">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem6" runat="server" ImageUrl="~/Images/Sports/6_Small.jpg"
                LinkUrl="~/Images/Sports/6.jpg" Caption="Sports 6">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem7" runat="server" ImageUrl="~/Images/Sports/7_Small.jpg"
                LinkUrl="~/Images/Sports/7.jpg" Caption="Sports 7">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem8" runat="server" ImageUrl="~/Images/Sports/8_Small.jpg"
                LinkUrl="~/Images/Sports/8.jpg" Caption="Sports 8">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem9" runat="server" ImageUrl="~/Images/Sports/9_Small.jpg"
                LinkUrl="~/Images/Sports/9.jpg" Caption="Sports 9">
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem10" runat="server" ImageUrl="~/Images/Sports/10_Small.jpg"
                LinkUrl="~/Images/Sports/10.jpg" Caption="Sports 10">
            </wijmo:C1CarouselItem>
        </Items>
        <PagerPosition>
            <My Left="Right"></My>
            <At Top="Bottom" Left="Right"></At>
        </PagerPosition>
    </wijmo:C1Carousel>
    <wijmo:C1LightBox runat="server" CssClass="ui-helper-hidden-accessible">
    </wijmo:C1LightBox>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Carousel.LightBox_Text0 %></p>
</asp:Content>
