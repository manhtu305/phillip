<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="Content.aspx.cs" Inherits="ControlExplorer.C1Carousel.Content" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Carousel"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        #<%=C1Carousel1.ClientID%>
        {
            width: 680px;
            height: 195px;
        }
        
        #<%=C1Carousel1.ClientID%> img
        {
            float: left;
            background: #fafafa;
            border: solid 1px #999DB2;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius: 4px;
            padding: 5px;
            margin: 0 1em 0 0;
        }
        #<%=C1Carousel1.ClientID%> li
        {
            padding: 1em 2em;
        }
        #<%=C1Carousel1.ClientID%> .image-shadow
        {
            float: left;
            padding-bottom: 9px;
        }
        #<%=C1Carousel1.ClientID%> p
        {
            font-style: italic;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1Carousel ID="C1Carousel1" runat="server" Width="750px" Height="200px" Display="1"
        EnableTheming="True"  CssClass="ui-corner-all ui-widget-content">
        <PagerPosition>
            <My Left="Right"></My>
            <At Top="Bottom" Left="Right"></At>
        </PagerPosition>
        <Items>
            <wijmo:C1CarouselItem ID="C1CarouselItem1" runat="server">
                <Template>
                    <div class="image-shadow">
                        <img src="../Images/Cities/1_small.jpg" alt="City 1" />
                    </div>
                    <h3>
                        Lorem Ipsum</h3>
                    <p><%= Resources.C1Carousel.Content_Text0 %></p>
                </Template>
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem2" runat="server">
                <Template>
                    <div class="image-shadow">
                        <img src="../Images/Cities/2_small.jpg" alt="City 2" />
                    </div>
                    <h3>
                        Vestibulum</h3>
                    <p><%= Resources.C1Carousel.Content_Text1 %></p>
                </Template>
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem3" runat="server">
                <Template>
                    <div class="image-shadow">
                        <img src="../Images/Cities/3_small.jpg" alt="City 3" />
                    </div>
                    <h3>
                        Phasellus</h3>
                    <p><%= Resources.C1Carousel.Content_Text2 %></p>
                </Template>
            </wijmo:C1CarouselItem>
            <wijmo:C1CarouselItem ID="C1CarouselItem4" runat="server">
                <Template>
                    <div class="image-shadow">
                        <img src="../Images/Cities/4_small.jpg" alt="City 4" />
                    </div>
                    <h3>
                        Venenatis</h3>
                    <p><%= Resources.C1Carousel.Content_Text3 %></p>
                </Template>
            </wijmo:C1CarouselItem>
        </Items>
    </wijmo:C1Carousel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Carousel.Content_Text4 %></p>
</asp:Content>
