<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="BarCodeStyle.aspx.cs" Inherits="ControlExplorer.C1BarCode.BarCodeStyle" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1BarCode"
    TagPrefix="wijmo" %>

<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
    <style type="text/css">
        .codeContainer
        {
            display: inline-block;
            padding: 20px;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content2" runat="server">
    
    <div >
        <h2><%= Resources.C1BarCode.BarCodeStyle_Title %></h2> 
        <br />
        <div><%= Resources.C1BarCode.BarCodeStyle_Text5 %></div>
        <br />
        <h4><%= Resources.C1BarCode.BarCodeStyle_Text6 %></h4>
        <wijmo:C1BarCode ID="C1BarCode1" runat="server" Text="1234567890" ShowText="true" />
        <br />
        <div>
            <h4><%= Resources.C1BarCode.BarCodeStyle_Text7 %></h4>
            <div>
                <%= Resources.C1BarCode.BarCodeStyle_Normal %>
            <wijmo:C1BarCode ID="C1BarCode4" runat="server" Text="1234567890" ShowText="true" />
            </div>
            <div class="codeContainer">
                <%= Resources.C1BarCode.BarCodeStyle_Up %>
            <wijmo:C1BarCode ID="C1BarCode2" runat="server" Text="1234567890" ShowText="true" BarDirection="Up" />
            </div>
            <div class="codeContainer">
                <%= Resources.C1BarCode.BarCodeStyle_Down %>
            <wijmo:C1BarCode ID="C1BarCode3" runat="server" Text="1234567890" ShowText="true" BarDirection="Down" />
            </div>
        </div>
        <br />
        <div>
            <h4><%= Resources.C1BarCode.BarCodeStyle_Text8 %></h4>
            <p><%= Resources.C1BarCode.BarCodeStyle_Text0 %></p>
            <p><%= Resources.C1BarCode.BarCodeStyle_Text1 %></p>
            <wijmo:C1BarCode ID="C1BarCode5" runat="server" Text="1234567890" ShowText="true" BackColor="Yellow" ForeColor="Blue" />
        </div>
        <br />
        <div>
            <h4><%= Resources.C1BarCode.BarCodeStyle_Text9 %></h4>
            <p><%= Resources.C1BarCode.BarCodeStyle_Text2 %></p>
            <wijmo:C1BarCode ID="C1BarCode6" runat="server" Text="1234567890" ShowText="true" TextFont="Consolas, 8.25pt, style=Italic"/>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
    <p><%= Resources.C1BarCode.BarCodeStyle_Text3 %></p>
    <p><%= Resources.C1BarCode.BarCodeStyle_Text4 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">

</asp:Content>
