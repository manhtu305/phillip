<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="AutoSize.aspx.cs" Inherits="ControlExplorer.C1BarCode.AutoSize" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1BarCode"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
    <style type="text/css">
        .codeContainer
        {
            display: inline-block;
            padding: 20px;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content2" runat="server">
	<p><%= Resources.C1BarCode.AutoSize_Text0 %></p>
	<br />
	<wijmo:C1BarCode ID="C1BarCode4" runat="server" Text="1234567890" ShowText="true" Width="200" Height="60" />
	<p><%= Resources.C1BarCode.AutoSize_Text1 %></p>
	<br />
	<wijmo:C1BarCode ID="C1BarCode2" runat="server" Text="1234" ShowText="true" Width="200" Height="60" />
	<p><%= Resources.C1BarCode.AutoSize_Text2 %></p>
	<br />
	<p><%= Resources.C1BarCode.AutoSize_Text3 %></p>
	<br />
	<wijmo:C1BarCode ID="C1BarCode1" runat="server" Text="1234567890" ShowText="true" Width="200" Height="60" AutoSize="true"/>
	<br />
	<wijmo:C1BarCode ID="C1BarCode3" runat="server" Text="1234" ShowText="true" Width="200" Height="60" AutoSize="true"/>
	<br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>