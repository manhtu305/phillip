<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="CodeType.aspx.cs" Inherits="ControlExplorer.C1BarCode.CodeType" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1BarCode"
    TagPrefix="wijmo" %>
<asp:Content ContentPlaceHolderID="Head" ID="Content1" runat="server">
    <style type="text/css">
        .codeContainer
        {
            display: inline-block;
            padding: 20px;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content2" runat="server">

    <div>
        <div>
            <h2><%= Resources.C1BarCode.CodeType_Title %></h2>
            <div>
                <div class="codeContainer">
                    <h4>Code39 :</h4>
                    <p><%= Resources.C1BarCode.CodeType_Text0 %></p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode4" runat="server" Text="C1BarCode%Code39" ShowText="true" />
                    </div>
                    <br />
                </div>
                <div class="codeContainer">
                    <h4>Code93 :</h4>
                    <p><%= Resources.C1BarCode.CodeType_Text1 %></p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode1" runat="server" Text="Code93" ShowText="true" CodeType="Code93" />
                    </div>
                    <br />
                </div>
                <div class="codeContainer">
                    <h4>Code128 :</h4>
                    <p><%= Resources.C1BarCode.CodeType_Text2 %></p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode2" runat="server" Text="Code128" ShowText="true" CodeType="Code128" />
                    </div>
                    <br />
                </div>
            </div>
            <div>
                <div class="codeContainer">
                    <h4>CodeI2of5 :</h4>
                    <p><%= Resources.C1BarCode.CodeType_Text3 %></p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode3" runat="server" Text="123456789" ShowText="true" CodeType="CodeI2of5" />
                    </div>
                    <br />
                </div>
                <div class="codeContainer">
                    <h4>Codabar :</h4>
                    <p><%= Resources.C1BarCode.CodeType_Text4 %></p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode5" runat="server" Text="12345678901234567890" ShowText="true" CodeType="Codabar" />
                    </div>
                    <br />
                </div>
                <div class="codeContainer">
                    <h4>PostNet :</h4>
                    <p><%= Resources.C1BarCode.CodeType_Text5 %></p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode8" runat="server" Text="80122" ShowText="true" CodeType="PostNet" />
                    </div>
                    <br />
                </div>
                
            </div>
            <div>
                <div class="codeContainer">
                    <h4>Ean13 :</h4>
                    <p><%= Resources.C1BarCode.CodeType_Text6 %></p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode6" runat="server" Text="690123456789" ShowText="true" CodeType="Ean13" />
                    </div>
                    <br />
                </div>
                <div class="codeContainer">
                    <h4>Ean8 :</h4>
                    <p><%= Resources.C1BarCode.CodeType_Text7 %></p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode7" runat="server" Text="4711234" ShowText="true" CodeType="Ean8" />
                    </div>
                    <br />
                </div>
                
                <div class="codeContainer">
                    <h4>UpcA :</h4>
                    <p><%= Resources.C1BarCode.CodeType_Text8 %></p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode9" runat="server" Text="300746606017" ShowText="true" CodeType="UpcA" />
                    </div>
                    <br />
                </div>
                <div class="codeContainer">
                    <h4>UpcE :</h4>
                    <p><%= Resources.C1BarCode.CodeType_Text9 %></p>
                    <br />
                    <div>
                        <wijmo:C1BarCode ID="C1BarCode10" runat="server" Text="01240000001" ShowText="true" CodeType="UpcE" />
                    </div>
                    <br />
                </div>
            </div>

            <br />
            <br />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
	    <h2><%= Resources.C1BarCode.CodeType_Text48 %></h2>
    <br />
    <h4>Code39 :</h4>
    <p><%= Resources.C1BarCode.CodeType_Text10 %></p>
	<p><%= Resources.C1BarCode.CodeType_Text11 %></p>
	<p><%= Resources.C1BarCode.CodeType_Text12 %></p>
    <br />
    <h4>Code93 :</h4>
    <p><%= Resources.C1BarCode.CodeType_Text13 %></p>
    <br />
    <h4>Code128 :</h4>
    <p><%= Resources.C1BarCode.CodeType_Text14 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text15 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text16 %></p>
    <br />
    <h4>CodeI2of5 :</h4>
    <p><%= Resources.C1BarCode.CodeType_Text17 %></p>
	<p><%= Resources.C1BarCode.CodeType_Text18 %></p>
    <br />
    <h4>Codabar :</h4>
    <p><%= Resources.C1BarCode.CodeType_Text19 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text20 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text21 %></p>
    <br />
    <h4>PostNet :</h4>
    <p><%= Resources.C1BarCode.CodeType_Text22 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text23 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text24 %></p>
    <br />
    <h4>Ean13 :</h4>
    <p><%= Resources.C1BarCode.CodeType_Text25 %></p>
	<p><%= Resources.C1BarCode.CodeType_Text26 %></p>
	<p><%= Resources.C1BarCode.CodeType_Text27 %></p>
	<p><%= Resources.C1BarCode.CodeType_Text28 %></p>
    <br />
    <h4>Ean8 :</h4>
    <p><%= Resources.C1BarCode.CodeType_Text29 %></p>
	<p><%= Resources.C1BarCode.CodeType_Text30 %></p>
	<p><%= Resources.C1BarCode.CodeType_Text31 %></p>
	<p><%= Resources.C1BarCode.CodeType_Text32 %></p>
    <br />
    <h4>UPCA</h4>
    <p><%= Resources.C1BarCode.CodeType_Text33 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text34 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text35 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text36 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text37 %></p>
    <br />
    <h4>UpcE</h4>
    <p><%= Resources.C1BarCode.CodeType_Text38 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text39 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text40 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text41 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text42 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text43 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text44 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text45 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text46 %></p>
    <p><%= Resources.C1BarCode.CodeType_Text47 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>
