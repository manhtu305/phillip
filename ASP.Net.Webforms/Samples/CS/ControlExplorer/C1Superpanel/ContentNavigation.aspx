<%@ Page Language="C#"  MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ContentNavigation.aspx.cs" Inherits="ControlExplorer.C1Superpanel.ContentNavigation" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1SuperPanel" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
    <style type="text/css">
        a, a:link, a:active, a:visited {
            text-decoration: none;
        }
        a:hover {
            text-decoration: underline;
        }
        li{
            padding-top:3px;
            padding-right:0;
        }
        ol{
            padding-left:20px;
            margin-right:0;
            padding-right:0;
        }
        p,h2{
            padding:5px;
        }
    </style>
	<script type="text/javascript">
		function navTo(dom) {
			var scroll = $('#<%=superPanel1.ClientID %>');
         	var ident = $(dom).attr("href");
         	var d = $(ident);
         	scroll.c1superpanel('scrollChildIntoView', d);
         	return false;
         }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Superpanel.ContentNavigation_Text0 %></p>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table style="width: 100%">
        <tr>
            <td valign="top" style="padding-left:3px;">
                <h3>
                    Table of Contents</h3>
                <ol>
                    <li><a href="#A1" onclick="return navTo(this);"><%= Resources.C1Superpanel.ContentNavigation_LoremIpsum %></a> </li>
                    <li><a  href="#A2" onclick="return navTo(this)"><%= Resources.C1Superpanel.ContentNavigation_NuncSitAmet %></a> </li>
                    <li><a href="#A3" onclick="return navTo(this)"><%= Resources.C1Superpanel.ContentNavigation_DonecAnteMetus %></a> </li>
                    <li><a href="#A4" onclick="return navTo(this)"><%= Resources.C1Superpanel.ContentNavigation_AeneanVehicula %></a> </li>
                    <li><a href="#A5" onclick="return navTo(this)"><%= Resources.C1Superpanel.ContentNavigation_FusceScelerisque %></a> </li>
                </ol>
            </td>
            <td>
                <wijmo:C1SuperPanel ID="superPanel1" runat="server" Width="500" Height="300">
                    <ContentTemplate>
                            <h2 id="A1" style="color:#000;">
                                <%= Resources.C1Superpanel.ContentNavigation_LoremIpsum %></h2>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text1 %></p>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text2 %></p>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text3 %></p>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text4 %></p>
                            <h2  id="A2" style="color:#000;">
                                <%= Resources.C1Superpanel.ContentNavigation_NuncSitAmet %></h2>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text5 %></p>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text6 %></p>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text7 %></p>
                            <h2 id="A3" style="color:#000;">
                                 <%= Resources.C1Superpanel.ContentNavigation_DonecAnteMetus %></h2>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text8 %></p>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text9 %></p>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text10 %></p>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text11 %></p>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text12 %></p>
                            <h2  id="A4" style="color:#000;">
                                <%= Resources.C1Superpanel.ContentNavigation_AeneanVehicula %></h2>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text13 %></p>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text14 %></p>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text15 %></p>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text16 %></p>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text17 %></p>
                            <h2 id="A5" style="color:#000;">
                                <%= Resources.C1Superpanel.ContentNavigation_FusceScelerisque %></h2>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text18 %></p>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text19 %></p>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text20 %></p>
                            <p><%= Resources.C1Superpanel.ContentNavigation_Text21 %></p>
                    </ContentTemplate>
                    <HScroller ScrollBarVisibility="Auto" ScrollValue="0">
                    </HScroller>
                    <VScroller ScrollBarVisibility="Visible" ScrollValue="0">
                    </VScroller>
                </wijmo:C1SuperPanel>
                </td>
            </tr>
        </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>

