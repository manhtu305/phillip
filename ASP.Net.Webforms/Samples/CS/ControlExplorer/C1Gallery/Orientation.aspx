<%@ Page Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Orientation.aspx.cs"
	Inherits="ControlExplorer.C1Gallery.Orientation" Title="Orientation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gallery"
	TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<h3><%= Resources.C1Gallery.Orientation_Title1 %></h3>
	<div>
		<cc1:C1Gallery ID="Gallery" runat="server" ShowTimer="false" Width="750" Height="200" ShowCounter="false"
			ThumbnailOrientation="Vertical" ThumbsDisplay="2" ShowPager="false" ThumbnailDirection="Before" ThumbsLength="150">
			<Items>
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/1.jpg" ImageUrl="~/Images/Sports/1_small.jpg" Title="Word Caption 1" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/2.jpg" ImageUrl="~/Images/Sports/2_small.jpg" Title="Word Caption 2" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/3.jpg" ImageUrl="~/Images/Sports/3_small.jpg" Title="Word Caption 3" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/4.jpg" ImageUrl="~/Images/Sports/4_small.jpg" Title="Word Caption 4" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/5.jpg" ImageUrl="~/Images/Sports/5_small.jpg" Title="Word Caption 5" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/6.jpg" ImageUrl="~/Images/Sports/6_small.jpg" Title="Word Caption 6" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/7.jpg" ImageUrl="~/Images/Sports/7_small.jpg" Title="Word Caption 7" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/8.jpg" ImageUrl="~/Images/Sports/8_small.jpg" Title="Word Caption 8" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/9.jpg" ImageUrl="~/Images/Sports/9_small.jpg" Title="Word Caption 9" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/10.jpg" ImageUrl="~/Images/Sports/10_small.jpg" Title="Word Caption 10" />
			</Items>
		</cc1:C1Gallery>
	</div>
	<h3><%= Resources.C1Gallery.Orientation_Title2 %>
	</h3>
	<div>
		<cc1:C1Gallery runat="server" Width="750" Height="200" ID="GelleryRV" ThumbnailOrientation="Vertical" ThumbsDisplay="2" ShowCounter="false" ThumbsLength="150" ShowTimer="false">
			<Items>
				<cc1:C1GalleryItem LinkUrl="~/Images/Cities/1.jpg" ImageUrl="~/Images/Cities/1_small.jpg" Title="Word Caption 1" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Cities/2.jpg" ImageUrl="~/Images/Cities/2_small.jpg" Title="Word Caption 2" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Cities/3.jpg" ImageUrl="~/Images/Cities/3_small.jpg" Title="Word Caption 3" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Cities/4.jpg" ImageUrl="~/Images/Cities/4_small.jpg" Title="Word Caption 4" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Cities/5.jpg" ImageUrl="~/Images/Cities/5_small.jpg" Title="Word Caption 5" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Cities/6.jpg" ImageUrl="~/Images/Cities/6_small.jpg" Title="Word Caption 6" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Cities/7.jpg" ImageUrl="~/Images/Cities/7_small.jpg" Title="Word Caption 7" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Cities/8.jpg" ImageUrl="~/Images/Cities/8_small.jpg" Title="Word Caption 8" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Cities/9.jpg" ImageUrl="~/Images/Cities/8_small.jpg" Title="Word Caption 9" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Cities/10.jpg" ImageUrl="~/Images/Cities/10_small.jpg" Title="Word Caption 10" />
			</Items>
		</cc1:C1Gallery>
	</div>
	<h3><%= Resources.C1Gallery.Orientation_Title3 %></h3>
	<div>
		<cc1:C1Gallery ID="C1Gallery1" Width="750" Height="411" runat="server" ThumbnailDirection="Before" ShowCounter="false" ShowTimer="false">
			<Items>
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/1.jpg" ImageUrl="~/Images/Sports/1_small.jpg" Title="Word Caption 1" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/2.jpg" ImageUrl="~/Images/Sports/2_small.jpg" Title="Word Caption 2" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/3.jpg" ImageUrl="~/Images/Sports/3_small.jpg" Title="Word Caption 3" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/4.jpg" ImageUrl="~/Images/Sports/4_small.jpg" Title="Word Caption 4" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/5.jpg" ImageUrl="~/Images/Sports/5_small.jpg" Title="Word Caption 5" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/6.jpg" ImageUrl="~/Images/Sports/6_small.jpg" Title="Word Caption 6" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/7.jpg" ImageUrl="~/Images/Sports/7_small.jpg" Title="Word Caption 7" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/8.jpg" ImageUrl="~/Images/Sports/8_small.jpg" Title="Word Caption 8" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/9.jpg" ImageUrl="~/Images/Sports/9_small.jpg" Title="Word Caption 9" />
				<cc1:C1GalleryItem LinkUrl="~/Images/Sports/10.jpg" ImageUrl="~/Images/Sports/10_small.jpg" Title="Word Caption 10" />
			</Items>
		</cc1:C1Gallery>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Gallery.Orientation_Text0 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
