<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Paging.aspx.cs" Inherits="ControlExplorer.C1Gallery.Paging" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gallery"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <cc1:C1Gallery ID="gallery" runat="server" ShowTimer="True" Width="750px" Height="300px"
        ThumbnailOrientation="Vertical" ThumbsDisplay="3" ShowPager="true">
        <Items>
            <cc1:C1GalleryItem LinkUrl="~/Images/Sports/1.jpg" ImageUrl="~/Images/Sports/1_small.jpg" />
            <cc1:C1GalleryItem LinkUrl="~/Images/Sports/2.jpg" ImageUrl="~/Images/Sports/2_small.jpg" />
            <cc1:C1GalleryItem LinkUrl="~/Images/Sports/3.jpg" ImageUrl="~/Images/Sports/3_small.jpg" />
            <cc1:C1GalleryItem LinkUrl="~/Images/Sports/4.jpg" ImageUrl="~/Images/Sports/4_small.jpg" />
            <cc1:C1GalleryItem LinkUrl="~/Images/Sports/5.jpg" ImageUrl="~/Images/Sports/5_small.jpg" />
            <cc1:C1GalleryItem LinkUrl="~/Images/Sports/6.jpg" ImageUrl="~/Images/Sports/6_small.jpg" />
        </Items>
    </cc1:C1Gallery>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Gallery.Paging_Text0 %></p>
    <p><%= Resources.C1Gallery.Paging_Text1 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
