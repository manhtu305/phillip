<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="Video.aspx.cs" Inherits="ControlExplorer.C1Gallery.Video" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gallery"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <cc1:C1Gallery ID="gallery" runat="server" ShowTimer="True" Width="750px" Height="600px"
        Mode="Swf" ThumbnailOrientation="Horizontal" ThumbsDisplay="4" ShowPager="false">
        <Items>
            <cc1:C1GalleryItem LinkUrl="<%$ Resources:C1Gallery, Video_LinkUrl1 %>" ImageUrl="<%$ Resources:C1Gallery, Video_ImageUrl1 %>" />
            <cc1:C1GalleryItem LinkUrl="<%$ Resources:C1Gallery, Video_LinkUrl2 %>" ImageUrl="<%$ Resources:C1Gallery, Video_ImageUrl2 %>" />
            <cc1:C1GalleryItem LinkUrl="<%$ Resources:C1Gallery, Video_LinkUrl3 %>" ImageUrl="<%$ Resources:C1Gallery, Video_ImageUrl3 %>" />
        </Items>
    </cc1:C1Gallery>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Gallery.Video_Text0 %></p>
    <p><%= Resources.C1Gallery.Video_Text1 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
