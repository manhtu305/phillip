<%@ Page Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="DataBinding.aspx.cs"
    Inherits="ControlExplorer.C1Gallery.DataBinding" Title="Data Bind" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gallery"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <cc1:C1Gallery ID="C1Gallery1" runat="server" ShowTimer="True" Width="750px" Height="416px"
        ThumbsDisplay="4" LoadOnDemand="true">
    </cc1:C1Gallery>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Gallery.DataBinding_Text0 %></p>
    <p><%= Resources.C1Gallery.DataBinding_Text1 %></p>
		<ul>
		<li><%= Resources.C1Gallery.DataBinding_Li1 %></li>
		<li><%= Resources.C1Gallery.DataBinding_Li2 %></li>
		<li><%= Resources.C1Gallery.DataBinding_Li3 %></li>
        <li><%= Resources.C1Gallery.DataBinding_Li4 %></li></ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<div class="settingcontainer">
		<div class="settingcontent">
			<ul>
				<li><asp:CheckBox ID="CheckBox1" AutoPostBack="true" runat="server" Text="<%$ Resources:C1Gallery, DataBinding_LoadOnDemand %>"
        OnCheckedChanged="CheckBox1_CheckedChanged" /></li>
			</ul>
		</div>
	</div>
    
</asp:Content>
