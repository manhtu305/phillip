<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="ShowThumbnailCaptions.aspx.cs" Inherits="ControlExplorer.C1Gallery.ShowThumbnailCaptions" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gallery"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <cc1:C1Gallery ID="gallery" runat="server" ShowTimer="True" Width="750px" Height="416px"
        ThumbsDisplay="4" ShowPager="false" ShowThumbnailCaptions="true">
        <Items>
            <cc1:C1GalleryItem LinkUrl="~/Images/Sports/1.jpg" ImageUrl="~/Images/Sports/1_small.jpg"
                Caption="Word Caption 1" />
            <cc1:C1GalleryItem LinkUrl="~/Images/Sports/2.jpg" ImageUrl="~/Images/Sports/2_small.jpg"
                Caption="Word Caption 2" />
            <cc1:C1GalleryItem LinkUrl="~/Images/Sports/3.jpg" ImageUrl="~/Images/Sports/3_small.jpg"
                Caption="Word Caption 3" />
            <cc1:C1GalleryItem LinkUrl="~/Images/Sports/4.jpg" ImageUrl="~/Images/Sports/4_small.jpg"
                Caption="Word Caption 4" />
            <cc1:C1GalleryItem LinkUrl="~/Images/Sports/5.jpg" ImageUrl="~/Images/Sports/5_small.jpg"
                Caption="Word Caption 5" />
            <cc1:C1GalleryItem LinkUrl="~/Images/Sports/6.jpg" ImageUrl="~/Images/Sports/6_small.jpg"
                Caption="Word Caption 6" />
        </Items>
    </cc1:C1Gallery>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
	<div class="settingcontainer">
		<div class="settingcontent">
			<ul>
				<li><asp:CheckBox ID="cbShowThumbCap" runat="server" Text="<%$ Resources:C1Gallery, ShowThumbnailCaptions_ShowThumbnailCaptions %>" Checked="true"  AutoPostBack="true"
        oncheckedchanged="cbShowThumbCap_CheckedChanged" /></li>
			</ul>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Gallery.ShowThumbnailCaptions_Text0 %></p>
    <p><%= Resources.C1Gallery.ShowThumbnailCaptions_Text1 %></p>
</asp:Content>
