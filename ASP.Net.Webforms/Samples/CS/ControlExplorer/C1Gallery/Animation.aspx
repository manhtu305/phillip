<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
	CodeBehind="Animation.aspx.cs" Inherits="ControlExplorer.C1Gallery.Animation" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gallery"
	TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<cc1:C1Gallery ID="Gallery" runat="server" ShowTimer="True" Width="750px" Height="256px"
				ThumbnailOrientation="Vertical" ThumbsDisplay="3" ShowPager="false">
				<Transitions>
					<Animated Disabled="false" Effect="slide" />
				</Transitions>
				<Items>
					<cc1:C1GalleryItem LinkUrl="~/Images/Sports/1.jpg" ImageUrl="~/Images/Sports/1_small.jpg" />
					<cc1:C1GalleryItem LinkUrl="~/Images/Sports/2.jpg" ImageUrl="~/Images/Sports/2_small.jpg" />
					<cc1:C1GalleryItem LinkUrl="~/Images/Sports/3.jpg" ImageUrl="~/Images/Sports/3_small.jpg" />
					<cc1:C1GalleryItem LinkUrl="~/Images/Sports/4.jpg" ImageUrl="~/Images/Sports/4_small.jpg" />
					<cc1:C1GalleryItem LinkUrl="~/Images/Sports/5.jpg" ImageUrl="~/Images/Sports/5_small.jpg" />
					<cc1:C1GalleryItem LinkUrl="~/Images/Sports/6.jpg" ImageUrl="~/Images/Sports/6_small.jpg" />
				</Items>
			</cc1:C1Gallery>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p><%= Resources.C1Gallery.Animation_Text0 %></p>
	<p><%= Resources.C1Gallery.Animation_Text1 %></p>
	<ul>
		<li><b>Transitions</b></li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<div class="settingcontainer">
		<div class="settingcontent">
			<asp:UpdatePanel ID="UpdatePanel2" runat="server">
				<ContentTemplate>
					<ul>
						<li>
							<label for="showingEffectTypes">
								<%= Resources.C1Gallery.Animation_Animation %>
							</label>

							<asp:DropDownList ID="showEffectTypes" runat="server" AutoPostBack="true" OnSelectedIndexChanged="showEffectTypes_SelectedIndexChanged">
								<asp:ListItem Value="blind" Selected="True">Blind</asp:ListItem>
								<asp:ListItem Value="clip">Clip</asp:ListItem>
								<asp:ListItem Value="drop">Drop</asp:ListItem>
								<asp:ListItem Value="explode">Explode</asp:ListItem>
								<asp:ListItem Value="fade">Fade</asp:ListItem>
								<asp:ListItem Value="fold">Fold</asp:ListItem>
								<asp:ListItem Value="highlight">Highlight</asp:ListItem>
								<asp:ListItem Value="puff">Puff</asp:ListItem>
								<asp:ListItem Value="pulsate">Pulsate</asp:ListItem>
								<asp:ListItem Value="scale">Scale</asp:ListItem>
								<asp:ListItem Value="size">Size</asp:ListItem>
								<asp:ListItem Value="slide">Slide</asp:ListItem>
							</asp:DropDownList>

						</li>
					</ul>
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>
	</div>


</asp:Content>
