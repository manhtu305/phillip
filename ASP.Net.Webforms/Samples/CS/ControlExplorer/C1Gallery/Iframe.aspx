<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true"
    CodeBehind="Iframe.aspx.cs" Inherits="ControlExplorer.C1Gallery.Iframe" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Gallery"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <cc1:C1Gallery ID="gallery" runat="server" ShowTimer="false" ShowCaption="false" ShowControlsOnHover="false" Width="750px" Height="600px"
        ThumbnailOrientation="Horizontal" ThumbsLength="100" ThumbsDisplay="4" ShowPager="false" Mode="Iframe">
        <Items>
            <cc1:C1GalleryItem LinkUrl="<%$ Resources:C1Gallery, Iframe_LinkUrl1 %>" ImageUrl="<%$ Resources:C1Gallery, Iframe_ImageUrl1 %>" Caption="ComponentOne" />
            <cc1:C1GalleryItem LinkUrl="<%$ Resources:C1Gallery, Iframe_LinkUrl2 %>" ImageUrl="<%$ Resources:C1Gallery, Iframe_ImageUrl2 %>" Caption="Microsoft" />
        </Items>
    </cc1:C1Gallery>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1Gallery.Iframe_Text0 %></p>
    <p><%= Resources.C1Gallery.Iframe_Text1 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
