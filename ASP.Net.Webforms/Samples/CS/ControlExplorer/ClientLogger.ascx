﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ClientLogger.ascx.cs"
	Inherits="ControlExplorer.ClientLogger" %>

<b><asp:Label runat="server" ID="Label1" Text="<%$ Resources:Resources, LoggerLabel %>"></asp:Label></b>
<div id="sampleLog">
</div>
<script type="text/javascript">
	function ClientLogger() {

	}
	ClientLogger.prototype = {
	    message: function (msg) {
	        msg = "[" + new Date().toLocaleTimeString() + "] " + msg;
			$("#sampleLog").html(msg + "<br />" + $("#sampleLog").html());
		}	
	}
	window.log = new ClientLogger();

</script>
