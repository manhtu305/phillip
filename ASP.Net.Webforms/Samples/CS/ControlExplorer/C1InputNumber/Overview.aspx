<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputNumber_Overview" CodeBehind="Overview.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <p><wijmo:C1InputNumeric ID="C1InputNumeric1" runat="server" ShowSpinner="true" Value="2.324" MinValue="-100" MaxValue="1000" DecimalPlaces="3">
        </wijmo:C1InputNumeric></p>
    <br />
    <p><%= Resources.C1InputNumber.Overview_Text1 %></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">

    <p><%= Resources.C1InputNumber.Overview_Text2 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>

