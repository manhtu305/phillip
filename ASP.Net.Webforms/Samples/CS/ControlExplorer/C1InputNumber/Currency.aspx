<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputNumber_Currency" CodeBehind="Currency.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <wijmo:C1InputCurrency ID="C1InputCurrency1" runat="server" ShowSpinner="true" Value="25.50" MinValue="0" MaxValue="1000">
    </wijmo:C1InputCurrency>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">

    <p><%= Resources.C1InputNumber.Currency_Text0 %></p>

    <p><%= Resources.C1InputNumber.Currency_Text1 %></p>

    <p><%= Resources.C1InputNumber.Currency_Text2 %></p>


</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>

