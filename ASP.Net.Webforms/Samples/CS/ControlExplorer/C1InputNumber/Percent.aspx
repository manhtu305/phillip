<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputNumber_Percent" CodeBehind="Percent.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <p><wijmo:C1InputPercent ID="C1InputPercent1" runat="server" ShowSpinner="true" Value="50" DecimalPlaces="4">
        </wijmo:C1InputPercent></p>
    <br />
    <p><%= Resources.C1InputNumber.Percent_Text1 %></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">

    <p><%= Resources.C1InputNumber.Percent_Text2 %></p>

    <p><%= Resources.C1InputNumber.Percent_Text3 %></p>

    <p><%= Resources.C1InputNumber.Percent_Text4 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>

