<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputNumber_Increment" CodeBehind="Increment.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <p><wijmo:C1InputCurrency ID="C1InputCurrency1" runat="server" ShowSpinner="true" Value="0" MinValue="-100" MaxValue="1000" Increment="10">
        </wijmo:C1InputCurrency></p>
    <br />
    <p><%= Resources.C1InputNumber.Increment_Text1 %></p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">

    <p><%= Resources.C1InputNumber.Increment_Text2 %></p>

    <p><%= Resources.C1InputNumber.Increment_Text3 %></p>

    <p><%= Resources.C1InputNumber.Increment_Text4 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>

