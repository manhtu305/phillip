<%@ Page Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Overview.aspx.cs" Inherits="ControlExplorer.C1Excel.CreatingWorkSheets" %>

<asp:Content id="content1" ContentPlaceHolderID="Head" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <asp:Button ID="btnexcel" runat="server" OnClick="btnexcel_Click" Text="<%$ Resources:C1Excel, Overview_GenerateExcelText %>" />
        <br/>
        <asp:Image ID="xlsimage" runat="server" ImageUrl="~/Images/excelWorkSheets.png" />
     </asp:Content>

    <asp:Content ID="content3" ContentPlaceHolderID="Description" runat="server">
		<p><%= Resources.C1Excel.Overview_Text0 %></p>
    </asp:Content>