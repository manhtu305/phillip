<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Wijmo.Master" CodeBehind="LoadingWorkSheets.aspx.cs" Inherits="ControlExplorer.C1Excel.LoadingWorkSheets" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Chart" TagPrefix="wijmo" %>



<asp:Content id="content1" ContentPlaceHolderID="Head" runat="Server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    
      <wijmo:C1CompositeChart ID="weatherchart" runat="server" Width="800" Height="500">

      </wijmo:C1CompositeChart>
   
    </asp:Content>

    <asp:Content ID="content3" ContentPlaceHolderID="Description" runat="server">
		<p><%= Resources.C1Excel.LoadingWorkSheets_Text0 %></p>
    </asp:Content>