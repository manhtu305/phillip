<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1Tabs_Overview" CodeBehind="Overview.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Tabs" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <wijmo:C1Tabs ID="C1Tab1" runat="server">
        <Pages>
            <wijmo:C1TabPage ID="Page1" Text="<%$ Resources:C1Tabs, Overview_NuncTincidunt %>">
                    <p><%= Resources.C1Tabs.Overview_Text0 %></p>
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page2" Text="<%$ Resources:C1Tabs, Overview_ProinDolor %>">
                    <p><%= Resources.C1Tabs.Overview_Text1 %></p>
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page3" Text="<%$ Resources:C1Tabs, Overview_AeneanLacinia %>">
                    <p><%= Resources.C1Tabs.Overview_Text2 %></p>
                    <p><%= Resources.C1Tabs.Overview_Text3 %></p>
            </wijmo:C1TabPage>
        </Pages>
    </wijmo:C1Tabs>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p><%= Resources.C1Tabs.Overview_Text4 %></p>
    <p><%= Resources.C1Tabs.Overview_Text5 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>
