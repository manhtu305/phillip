<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1Tabs_Validation" CodeBehind="Validation.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Tabs" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <style type="text/css">
        .labelstyle
        {
            float:left;
            width:100px;
        }

        .inputPanel
        {
            margin:4px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <wijmo:C1Tabs ID="C1Tab1" runat="server" CausesValidation="True" ValidationGroup="LoginValidationGroup" Enabled="True" OnClientSelect="function(){return Page_ClientValidate('LoginValidationGroup')}">
        <Pages>
            <wijmo:C1TabPage ID="Page1" Text="<%$ Resources:C1Tabs, Validation_NuncTincidunt %>">
                <div style="padding-top: 20px; padding-left: 20px;">
                    <div class="inputPanel">
                        <asp:Label ID="Label1" CssClass="labelstyle" runat="server" AssociatedControlID="userName" Text="<%$ Resources:C1Tabs, Validation_Username %>"></asp:Label>
                        <asp:TextBox runat="server" ID="userName" CausesValidation="True"></asp:TextBox>
                        <asp:RequiredFieldValidator ForeColor="Red" runat="server" ID="validator1"
                            ControlToValidate="userName" Display="Dynamic" ErrorMessage="<%$ Resources:C1Tabs, Validation_UsernameErrorMessage %>"
                            ValidationGroup="LoginValidationGroup" />
                    </div>
                    <div class="inputPanel">
                        <asp:Label ID="Label2" CssClass="labelstyle" runat="server" AssociatedControlID="passWord" Text="<%$ Resources:C1Tabs, Validation_Password %>"></asp:Label>
                        <asp:TextBox runat="server" TextMode="password" ID="passWord" CausesValidation="True"></asp:TextBox>
                        <asp:RequiredFieldValidator ForeColor="Red" runat="server" ID="validator2"
                            ControlToValidate="passWord" Display="Dynamic" ErrorMessage="<%$ Resources:C1Tabs, Validation_PasswordErrorMessage %>"
                            ValidationGroup="LoginValidationGroup" />
                    </div>
                </div>
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page2" Text="<%$ Resources:C1Tabs, Validation_ProinDolor %>">
                <p><%= Resources.C1Tabs.Validation_Text0 %></p>
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page3" Text="<%$ Resources:C1Tabs, Validation_AeneanLacinia %>">
                <p><%= Resources.C1Tabs.Validation_Text1 %></p>
                <p><%= Resources.C1Tabs.Validation_Text2 %></p>
            </wijmo:C1TabPage>
        </Pages>
    </wijmo:C1Tabs>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p><%= Resources.C1Tabs.Validation_Text3 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>
