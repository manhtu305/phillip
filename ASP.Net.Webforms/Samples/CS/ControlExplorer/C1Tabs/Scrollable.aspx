<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1Tabs_Scrollable" CodeBehind="Scrollable.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Tabs" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <wijmo:C1Tabs ID="C1Tab1" runat="server" Scrollable="True" Width="820px">
        <Pages>
            <wijmo:C1TabPage ID="Page1" Text="<%$ Resources:C1Tabs, Scrollable_NuncTincidunt %>">
                    <p><%= Resources.C1Tabs.Scrollable_Text0 %></p>
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page2" Text="<%$ Resources:C1Tabs, Scrollable_ProinDolor %>">
                    <p><%= Resources.C1Tabs.Scrollable_Text1 %></p>
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page3" Text="<%$ Resources:C1Tabs, Scrollable_AeneanLacinia %>">
                    <p><%= Resources.C1Tabs.Scrollable_Text2 %></p>
                    <p><%= Resources.C1Tabs.Scrollable_Text3 %></p>
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page4" Text="<%$ Resources:C1Tabs, Scrollable_DemoTab4 %>">
                    <p><%= Resources.C1Tabs.Scrollable_Text4 %></p>
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page5" Text="<%$ Resources:C1Tabs, Scrollable_DemoTab5 %>">
                    <p><%= Resources.C1Tabs.Scrollable_Text5 %></p>
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page6" Text="<%$ Resources:C1Tabs, Scrollable_DemoTab6 %>">
                    <p><%= Resources.C1Tabs.Scrollable_Text6 %></p>
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page7" Text="<%$ Resources:C1Tabs, Scrollable_DemoTab7 %>">
                    <p><%= Resources.C1Tabs.Scrollable_Text7 %></p>
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page8" Text="<%$ Resources:C1Tabs, Scrollable_DemoTab8 %>">
                    <p><%= Resources.C1Tabs.Scrollable_Text8 %></p>
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page9" Text="<%$ Resources:C1Tabs, Scrollable_DemoTab9 %>">
                    <p><%= Resources.C1Tabs.Scrollable_Text9 %></p>
            </wijmo:C1TabPage>
        </Pages>
    </wijmo:C1Tabs>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p><%= Resources.C1Tabs.Scrollable_Text10 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>
