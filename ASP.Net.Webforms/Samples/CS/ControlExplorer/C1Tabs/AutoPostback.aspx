<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1Tabs_AutoPostback" CodeBehind="AutoPostback.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Tabs" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
    <wijmo:C1Tabs ID="C1Tab1" runat="server" AutoPostBack="true" OnSelectedChanged="C1Tab1_SelectedChanged">
        <Pages>
            <wijmo:C1TabPage ID="Page1" Text="<%$ Resources:C1Tabs, AutoPostback_Tab1 %>">
                <%= Resources.C1Tabs.AutoPostback_Text3 %>
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page2" Text="<%$ Resources:C1Tabs, AutoPostback_Tab2 %>">
                <%= Resources.C1Tabs.AutoPostback_Text4 %>
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page3" Text="<%$ Resources:C1Tabs, AutoPostback_Tab3 %>">
                <%= Resources.C1Tabs.AutoPostback_Text5 %>
            </wijmo:C1TabPage>
            <wijmo:C1TabPage ID="Page4" Text="<%$ Resources:C1Tabs, AutoPostback_Tab4 %>">
                <%= Resources.C1Tabs.AutoPostback_6 %>
            </wijmo:C1TabPage>
        </Pages>
    </wijmo:C1Tabs>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p><%= Resources.C1Tabs.AutoPostback_Text0 %></p>
    <p><%= Resources.C1Tabs.AutoPostback_Text1 %></p>
    <p><%= Resources.C1Tabs.AutoPostback_Text2 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>
