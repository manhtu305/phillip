<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1Tabs_Alignment" CodeBehind="Alignment.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Tabs" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <wijmo:C1Tabs ID="C1Tab1" runat="server">
                <Pages>
                    <wijmo:C1TabPage ID="Page1" Text="<%$ Resources:C1Tabs, Alignment_NuncTincidunt %>">
                        <p><%= Resources.C1Tabs.Alignment_Text0 %></p>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage ID="Page2" Text="<%$ Resources:C1Tabs, Alignment_ProinDolor %>">
                        <p><%= Resources.C1Tabs.Alignment_Text1 %></p>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage ID="Page3" Text="<%$ Resources:C1Tabs, Alignment_AeneanLacinia %>">
                        <p><%= Resources.C1Tabs.Alignment_Text2 %></p>
                        <p><%= Resources.C1Tabs.Alignment_Text3 %></p>
                    </wijmo:C1TabPage>
                </Pages>
            </wijmo:C1Tabs>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p><%= Resources.C1Tabs.Alignment_Text4 %></p>
    <p><%= Resources.C1Tabs.Alignment_Text5 %></p>
    <p><%= Resources.C1Tabs.Alignment_Text6 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div class="settingcontainer">
                <div class="settingcontent">
                    <ul>
                        <li>
                            <label><%= Resources.C1Tabs.Alignment_Alignment %></label>
                            <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                <asp:ListItem Selected="True" Text="<%$ Resources:C1Tabs, Alignment_AlignmentTop %>" Value="Top"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:C1Tabs, Alignment_AlignmentText %>" Value="Bottom"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:C1Tabs, Alignment_AlignmentLeft %>" Value="Left"></asp:ListItem>
                                <asp:ListItem Text="<%$ Resources:C1Tabs, Alignment_AlignmentRight %>" Value="Right"></asp:ListItem>
                            </asp:DropDownList>
                        </li>
                    </ul>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
