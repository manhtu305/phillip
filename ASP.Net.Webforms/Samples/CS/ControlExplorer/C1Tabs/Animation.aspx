<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1Tabs_Animation" CodeBehind="Animation.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Tabs" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <wijmo:C1Tabs ID="C1Tab1" runat="server">
                <Pages>
                    <wijmo:C1TabPage ID="Page1" Text="<%$ Resources:C1Tabs, Animation_NuncTincidunt %>">
                        <p><%= Resources.C1Tabs.Animation_Text0 %></p>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage ID="Page2" Text="<%$ Resources:C1Tabs, Animation_ProinDolor %>">
                        <p><%= Resources.C1Tabs.Animation_Text1 %></p>
                    </wijmo:C1TabPage>
                    <wijmo:C1TabPage ID="Page3" Text="<%$ Resources:C1Tabs, Animation_AeneanLacinia %>">
                        <p><%= Resources.C1Tabs.Animation_Text2 %></p>
                        <p><%= Resources.C1Tabs.Animation_Text3 %></p>
                    </wijmo:C1TabPage>
                </Pages>
            </wijmo:C1Tabs>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">
    <p><%= Resources.C1Tabs.Animation_Text4 %></p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <div class="settingcontainer">
                <div class="settingcontent">
                    <ul>
                        <li class="fullwidth">
                            <label class="settinglegend"><%= Resources.C1Tabs.Animation_ShowOptions %></label>
                        </li>
                        <li>
                            <asp:CheckBox ID="showblind" runat="server" Text="<%$ Resources:C1Tabs, Animation_Blind %>" />
                            <asp:CheckBox ID="showfade" runat="server" Text="<%$ Resources:C1Tabs, Animation_Fade %>" />
                        </li>
                        <li>
                            <label><%= Resources.C1Tabs.Animation_Duration %></label>
                            <asp:DropDownList ID="showduration" runat="server">
                                <asp:ListItem Selected="True">200</asp:ListItem>
                                <asp:ListItem>400</asp:ListItem>
                                <asp:ListItem>800</asp:ListItem>
                                <asp:ListItem>1500</asp:ListItem>
                            </asp:DropDownList>
                        </li>

                        <li class="fullwidth">
                            <label class="settinglegend"><%= Resources.C1Tabs.Animation_HideOptions %></label>
                        </li>
                        <li>
                            <asp:CheckBox ID="hideblind" runat="server" Text="<%$ Resources:C1Tabs, Animation_Blind %>" />
                            <asp:CheckBox ID="hidefade" runat="server" Text="<%$ Resources:C1Tabs, Animation_Fade %>" />
                        </li>
                        <li>
                            <label><%= Resources.C1Tabs.Animation_Duration %></label>
                            <asp:DropDownList ID="hideduration" runat="server">
                                <asp:ListItem Selected="True">200</asp:ListItem>
                                <asp:ListItem>400</asp:ListItem>
                                <asp:ListItem>800</asp:ListItem>
                                <asp:ListItem>1500</asp:ListItem>
                            </asp:DropDownList>
                        </li>
                    </ul>
                </div>
                <div class="settingcontrol">
                    <asp:Button ID="apply" runat="server" Text="<%$ Resources:C1Tabs, Animation_Apply %>" CssClass="settingapply" OnClick="apply_Click" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
