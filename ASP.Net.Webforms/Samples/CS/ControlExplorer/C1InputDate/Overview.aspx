<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" Inherits="C1InputDate_Overview" CodeBehind="Overview.aspx.cs" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

	<ul class="ui-helper-reset">
		<li>
			<label><%= Resources.C1InputDate.Overview_Text1 %></label>
			<wijmo:C1InputDate ID="C1InputDate1" runat="server" DateFormat="T"></wijmo:C1InputDate>
		</li>
		<li>
			<label><%= Resources.C1InputDate.Overview_Text2 %></label>
			<wijmo:C1InputDate ID="C1InputDate2" runat="server" DateFormat="d"></wijmo:C1InputDate>
		</li>
		<li>
			<label><%= Resources.C1InputDate.Overview_Text3 %></label>
			<wijmo:C1InputDate ID="C1InputDate3" runat="server" DateFormat="g"></wijmo:C1InputDate>
		</li>
		<li>
			<label><%= Resources.C1InputDate.Overview_Text4 %></label>
			<wijmo:C1InputDate ID="C1InputDate4" runat="server" Width="300" DateFormat="U"></wijmo:C1InputDate>
		</li>
	</ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="Server">

	<p><%= Resources.C1InputDate.Overview_Text0 %></p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="Server">
</asp:Content>

