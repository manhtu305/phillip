<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="NullText.aspx.cs" Inherits="ControlExplorer.C1InputDate.NullText" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.45" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <wijmo:C1InputDate ID="C1InputDate1" runat="server" Width="350px" DateFormat="d" Placeholder="<%$ Resources:C1InputDate, NullText_Placeholder1 %>">
    </wijmo:C1InputDate>
    <br/>
    <wijmo:C1InputDate ID="C1InputDate2" runat="server" Width="350px" DateFormat="g" Placeholder="<%$ Resources:C1InputDate, NullText_Text02 %>">
    </wijmo:C1InputDate>
    <br/>
    <wijmo:C1InputDate ID="C1InputDate3" runat="server" Width="350px" DateFormat="U" Placeholder="<%$ Resources:C1InputDate, NullText_Placeholder3 %>">
    </wijmo:C1InputDate>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
    <p><%= Resources.C1InputDate.NullText_Text0 %></p>
    <p><%= Resources.C1InputDate.NullText_Text1 %></p>
</asp:Content>
