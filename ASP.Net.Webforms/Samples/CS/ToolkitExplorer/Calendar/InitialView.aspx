﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeBehind="InitialView.aspx.cs" Inherits="ToolkitExplorer.Calendar.InitialView" %>

<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1Calendar" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	InitialView = "Day"
	<asp:Panel ID="Panel1" runat="server">
	</asp:Panel>
	<wijmo:C1CalendarExtender ID="DayView" runat="server" TargetControlID="Panel1" InitialView="Day" />
	InitialView = "Month"
	<asp:Panel ID="Panel2" runat="server">
	</asp:Panel>
	<wijmo:C1CalendarExtender ID="MonthView" runat="server" TargetControlID="Panel2" InitialView="Month" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
		Set the initial view type of Calendar.
	</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
