﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeBehind="Annotation.aspx.cs" Inherits="ToolkitExplorer.CandlestickChart.Annotation" %>

<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1Chart" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:Panel ID="candlestickchart" Height="475" Width="756" runat="server" CssClass="ui-widget ui-widget-content ui-corner-all">
	</asp:Panel>
	<wijmo:C1CandlestickChartExtender runat="server" ID="CandlestickChartExtender1"
		TargetControlID="candlestickchart">
		<SeriesList>
			<wijmo:CandlestickChartSeries LegendEntry="True" Label="MSFT">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData DateTimeValue="2011-12-01" />
							<wijmo:ChartXData DateTimeValue="2011-12-02" />
							<wijmo:ChartXData DateTimeValue="2011-12-05" />
							<wijmo:ChartXData DateTimeValue="2011-12-06" />
							<wijmo:ChartXData DateTimeValue="2011-12-07" />
							<wijmo:ChartXData DateTimeValue="2011-12-08" />
							<wijmo:ChartXData DateTimeValue="2011-12-09" />
						</Values>
					</X>
					<High DoubleValues="10,12,11,14,16,20,18">
						<Values>
							<wijmo:ChartY1Data DoubleValue="10"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="12"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="11"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="14"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="16"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="20"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="18"></wijmo:ChartY1Data>
						</Values>
					</High>
					<Low DoubleValues="7.5,8.6,4.4,4.2,8,9,11">
						<Values>
							<wijmo:ChartY1Data DoubleValue="7.5"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="8.6"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="4.4"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="4.2"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="8"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="9"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="11"></wijmo:ChartY1Data>
						</Values>
					</Low>
					<Open DoubleValues="8,8.6,11,6.2,13.8,15,14">
						<Values>
							<wijmo:ChartY1Data DoubleValue="8"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="8.6"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="11"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="6.2"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="13.8"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="15"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="14"></wijmo:ChartY1Data>
						</Values>
					</Open>
					<Close DoubleValues="8.6,11,6.2,13.8,15,14,12">
						<Values>
							<wijmo:ChartY1Data DoubleValue="8.6"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="11"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="6.2"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="13.8"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="15"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="14"></wijmo:ChartY1Data>
							<wijmo:ChartY1Data DoubleValue="12"></wijmo:ChartY1Data>
						</Values>
					</Close>
				</Data>
			</wijmo:CandlestickChartSeries>
		</SeriesList>
		<TextStyle FontFamily="Segoe UI, Frutiger, Frutiger Linotype, Dejavu Sans, Helvetica Neue, Arial, sans-serif" FontSize="13px">
		</TextStyle>

		<Header Compass="North" Text="Stock History"></Header>

		<Footer Compass="South" Visible="False"></Footer>

		<Legend Compass="North" Orientation="Horizontal"></Legend>
		<Axis>
			<X>
				<TextStyle FontWeight="normal">
				</TextStyle>
				<GridMajor Visible="True"></GridMajor>

				<GridMinor Visible="False"></GridMinor>

				<TickMajor Position="Outside">
					<Style Stroke="#999999"></Style>

					<TickStyle Stroke="#999999">
					</TickStyle>
				</TickMajor>
			</X>

			<Y Visible="False" Compass="West" Alignment="Far" Text="Stock Price">
				<TextStyle FontWeight="normal">
				</TextStyle>
				<Labels TextAlign="Center"></Labels>

				<GridMajor Visible="True"></GridMajor>

				<GridMinor Visible="False"></GridMinor>
			</Y>
		</Axis>
		<CandlestickChartSeriesStyles>
			<wijmo:CandlestickChartStyle>
				<HighLow Width="2">
					<Fill Color="#8C8C8C"></Fill>
				</HighLow>
				<FallingClose Width="6">
					<Fill Color="#F07E6E"></Fill>
				</FallingClose>
				<RisingClose Width="6">
					<Fill Color="#90CD97"></Fill>
				</RisingClose>
			</wijmo:CandlestickChartStyle>
		</CandlestickChartSeriesStyles>
		<Hint>
			<ContentStyle FontFamily="Segoe UI, Frutiger, Frutiger Linotype, Dejavu Sans, Helvetica Neue, Arial, sans-serif" FontSize="12px">
			</ContentStyle>
			<Content Function="hintContent" />

			<Style Stroke="Transparent">
				<Fill Color="#444444" > </Fill >
			</Style>

			<HintStyle Stroke="Transparent">
				<Fill Color="#444444">
				</Fill>
			</HintStyle>
		</Hint>

		<Indicator Visible="False"></Indicator>
		<Annotations>
			<wijmo:TextAnnotation Attachment="Relative" Offset="0, 0" Text="Text Annotation" Tooltip="Text Annotation">
				<Point>
					<X DoubleValue="0.5" />
					<Y DoubleValue="0.15" />
				</Point>
				<AnnotationStyle FontSize="20" FontWeight="Bold">
					<Fill Color="#FF66FF" > </Fill >
				</AnnotationStyle>
			</wijmo:TextAnnotation>
			<wijmo:RectAnnotation Attachment="DataIndex" Content="Rect" Height="30" Offset="0, 0" Position="Center, Bottom" Tooltip="Rect" Width="60" PointIndex="4">
				<AnnotationStyle FillOpacity="0.25">
					<Fill Color="#CC99FF" > </Fill >
				</AnnotationStyle>
			</wijmo:RectAnnotation>
			<wijmo:EllipseAnnotation Attachment="DataIndex" Content="Ellipse" Height="35" Offset="0, 0" Tooltip="Ellipse" Width="75" PointIndex="6">
				<AnnotationStyle FillOpacity="0.15">
					<Fill Color="#9966FF" > </Fill >
				</AnnotationStyle>
			</wijmo:EllipseAnnotation>
			<wijmo:CircleAnnotation Content="Circle" Offset="0, 0" Tooltip="Circle">
				<Point>
					<X DoubleValue="270" />
					<Y DoubleValue="100" />
				</Point>
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#666699" > </Fill >
				</AnnotationStyle>
			</wijmo:CircleAnnotation>
			<wijmo:ImageAnnotation Attachment="DataIndex" Href="./../images/c1logo_215x150.png" Offset="0, 0" PointIndex="5" SeriesIndex="0" Tooltip="Image">
			</wijmo:ImageAnnotation>
			<wijmo:SquareAnnotation Attachment="DataIndex" Content="Square" Offset="0, 0" PointIndex="2" SeriesIndex="0" Tooltip="Square">
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#66FFFF" > </Fill >
				</AnnotationStyle>
			</wijmo:SquareAnnotation>
			<wijmo:LineAnnotation Content="Line" End="100, 100" Offset="0, 0" Start="10, 10" Tooltip="Line">
			</wijmo:LineAnnotation>
			<wijmo:PolygonAnnotation Offset="0, 0" Tooltip="Polygon" Content="Polygon">
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#3399FF" > </Fill >
				</AnnotationStyle>
				<Points>
					<wijmo:PointF X="200" Y="0" />
					<wijmo:PointF X="150" Y="50" />
					<wijmo:PointF X="175" Y="100" />
					<wijmo:PointF X="225" Y="100" />
					<wijmo:PointF X="250" Y="50" />
				</Points>
			</wijmo:PolygonAnnotation>
		</Annotations>
	</wijmo:C1CandlestickChartExtender>
	<script type="text/javascript">
		function hintContent() {
			return this.label + ' - ' +
							Globalize.format(this.x, "d") +
							'\n High:' + this.high +
							'\n Low:' + this.low +
							'\n Open:' + this.open +
							'\n Close:' + this.close;
		}
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>
		This sample demonstrates how to add annotation in the <strong>C1CandlestickChartExtender</strong>.
	</p>
	<h3>Test the features</h3>
	<ul>
		<li>Hover over mouse over annotation to see the tooltip.</li>
		<li>Click the legend item to toggle the visibility.</li>
	</ul>
</asp:Content>
