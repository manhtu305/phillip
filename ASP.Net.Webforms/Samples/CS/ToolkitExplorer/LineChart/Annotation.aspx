﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeBehind="Annotation.aspx.cs" Inherits="ToolkitExplorer.LineChart.Annotation" %>

<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1Chart" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:Panel ID="linechart" Height="475" Width="756" runat="server" CssClass="ui-widget ui-widget-content ui-corner-all">
	</asp:Panel>
	<wijmo:C1LineChartExtender runat="server" ID="C1LineChart1" Type="Area" ShowChartLabels="False"
		TargetControlID="linechart">
		<Header Text="Twitter trends in October 2010">
		</Header>
		<Hint OffsetY="-10">
			<Content Function="hintContent" />
		</Hint>
		<SeriesHoverStyles>
			<wijmo:ChartStyle StrokeWidth="8" Opacity="1" />
		</SeriesHoverStyles>
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#00a6dd" StrokeWidth="5" Opacity="0.8" />
		</SeriesStyles>
		<SeriesList>
			<wijmo:LineChartSeries Label="#Wijmo" LegendEntry="true" FitType="Spline">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData DateTimeValue="2010-11-21" />
							<wijmo:ChartXData DateTimeValue="2010-11-22" />
							<wijmo:ChartXData DateTimeValue="2010-11-23" />
							<wijmo:ChartXData DateTimeValue="2010-11-24" />
							<wijmo:ChartXData DateTimeValue="2010-11-25" />
							<wijmo:ChartXData DateTimeValue="2010-11-26" />
							<wijmo:ChartXData DateTimeValue="2010-11-27" />
							<wijmo:ChartXData DateTimeValue="2010-11-28" />
							<wijmo:ChartXData DateTimeValue="2010-11-29" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="12" />
							<wijmo:ChartYData DoubleValue="30" />
							<wijmo:ChartYData DoubleValue="6" />
							<wijmo:ChartYData DoubleValue="22" />
							<wijmo:ChartYData DoubleValue="14" />
							<wijmo:ChartYData DoubleValue="25" />
							<wijmo:ChartYData DoubleValue="41" />
							<wijmo:ChartYData DoubleValue="14" />
							<wijmo:ChartYData DoubleValue="3" />
						</Values>
					</Y>
				</Data>
			</wijmo:LineChartSeries>
		</SeriesList>
		<Annotations>
			<wijmo:TextAnnotation Attachment="Relative" Offset="0, 0" Text="Text Annotation" Tooltip="Text Annotation">
				<Point>
					<X DoubleValue="0.5" />
					<Y DoubleValue="0.15" />
				</Point>
				<AnnotationStyle FontSize="20" FontWeight="Bold">
					<Fill Color="#FF66FF" > </Fill >
				</AnnotationStyle>
			</wijmo:TextAnnotation>
			<wijmo:RectAnnotation Attachment="DataIndex" Content="Rect" Height="30" Offset="0, 0" Position="Center, Bottom" Tooltip="Rect" Width="50" PointIndex="4">
				<AnnotationStyle FillOpacity="0.25">
					<Fill Color="#CC99FF" > </Fill >
				</AnnotationStyle>
			</wijmo:RectAnnotation>
			<wijmo:EllipseAnnotation Attachment="DataIndex" Content="Ellipse" Height="35" Offset="0, 0" Tooltip="Ellipse" Width="75" PointIndex="6">
				<AnnotationStyle FillOpacity="0.15">
					<Fill Color="#9966FF" > </Fill >
				</AnnotationStyle>
			</wijmo:EllipseAnnotation>
			<wijmo:CircleAnnotation Content="Circle" Offset="0, 0" Tooltip="Circle">
				<Point>
					<X DoubleValue="200" />
					<Y DoubleValue="100" />
				</Point>
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#666699" > </Fill >
				</AnnotationStyle>
			</wijmo:CircleAnnotation>
			<wijmo:ImageAnnotation Attachment="DataIndex" Href="./../images/c1logo_215x150.png" Width="70" Height="70" Offset="0, 0" PointIndex="5" SeriesIndex="0" Tooltip="Image">
			</wijmo:ImageAnnotation>
			<wijmo:PolygonAnnotation Offset="0, 0" Tooltip="Polygon" Content="Polygon">
				<Points>
					<wijmo:PointF X="200" Y="0" />
					<wijmo:PointF X="150" Y="50" />
					<wijmo:PointF X="175" Y="100" />
					<wijmo:PointF X="225" Y="100" />
					<wijmo:PointF X="250" Y="50" />
				</Points>
				<AnnotationStyle FillOpacity="0.25">
					<Fill Color="#9966FF" > </Fill >
				</AnnotationStyle>
			</wijmo:PolygonAnnotation>
			<wijmo:SquareAnnotation Attachment="DataIndex" Width="30" Content="Square" Offset="0, 0" PointIndex="2" SeriesIndex="0" Tooltip="Square">
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#66FFFF" > </Fill >
				</AnnotationStyle>
			</wijmo:SquareAnnotation>
			<wijmo:LineAnnotation Content="Line" End="100, 100" Offset="0, 0" Start="10, 10" Tooltip="Line">
			</wijmo:LineAnnotation>
		</Annotations>
	</wijmo:C1LineChartExtender>
	<script type="text/javascript">
		function hintContent() {
			return this.y;
		}
		function pageLoad() {
			var resizeTimer = null;

			$(window).resize(function () {
				window.clearTimeout(resizeTimer);
				resizeTimer = window.setTimeout(function () {
					var jqLine = $("#<%= C1LineChart1.ClientID %>"),
						width = jqLine.width(),
						height = jqLine.height();

					if (!width || !height) {
						window.clearTimeout(resizeTimer);
						return;
					}

					jqLine.c1linechart("redraw", width, height);
				}, 250);
			});
		}
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>
		This sample demonstrates how to add annotation in the <strong>C1LineChartExtender</strong>.
	</p>
	<h3>Test the features</h3>
	<ul>
		<li>Hover over mouse over annotation to see the tooltip.</li>
		<li>Click the legend item to toggle the visibility.</li>
	</ul>
</asp:Content>
