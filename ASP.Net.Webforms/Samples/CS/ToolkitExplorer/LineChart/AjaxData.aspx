﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeFile="AjaxData.aspx.cs" Inherits="LineChart_AjaxData" %>
<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1Chart" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<asp:ScriptManager ID="ScriptManager1" runat="server">
	</asp:ScriptManager>
	<script type="text/javascript">
		function hintContent() {
			return this.x + '\n ' + this.y + '';
		}

		function pageLoad() {
			var netflx = "https://demos.componentone.com/aspnet/Northwind/northwind.svc/Products?$format=json&$top=10&$orderby=Unit_Price%20desc";

			$("#<%= linechart.ClientID %>").wijlinechart("option", "hint", { content: hintContent });
			$.ajax({
				crossDomain: true,
				header: { "content-type": "application/javascript" },
				url: netflx,
				jsonp: "$callback",
				success: callback
			});
		}

		function callback(result) {
			// unwrap result
			var names = [];
			var prices = [];

			var products = result["d"];

			for (var i = 0; i < products.length; i++) {

				names.push(products[i].Product_Name);
				prices.push(parseFloat(products[i].Unit_Price));
			}

			$("#<%= linechart.ClientID %>").wijlinechart("option", "seriesList", [
					{
						label: "Prices",
						legendEntry: true,
						fitType: "spline",
						data: {
							x: names,
							y: prices
						},
						markers: {
							visible: true,
							type: "circle"
						}
					}
				]);
		}
	</script>
	<asp:Panel ID="linechart" runat="server" Height="475" Width = "756" CssClass="ui-widget ui-widget-content ui-corner-all">
	</asp:Panel>
	<wijmo:C1LineChartExtender runat = "server" ID="LineChartExtender1" TargetControlID="linechart">
		<Hint>
			<Content Function="hintContent" />
		</Hint>
		<Footer Compass="South" Visible="False"></Footer>
		<Legend>
			<Size Width="30" Height="3"></Size>
		</Legend>
		<Axis>
			<Y Text="Prices"></Y>
			<X>
				<Labels>
					<AxisLabelStyle Rotation="-45"></AxisLabelStyle>
				</Labels>
			</X> 
		</Axis>
		<Header Text="Top 10 Products by Unit Price - Northwind OData" />
	</wijmo:C1LineChartExtender>
	
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p>
		This sample illustrates how to create a chart using data from an external data source. In this example, we are using data from the ComponentOne OData feed.
	</p>
	<ul>
		<li>Data URL: <a href="https://demos.componentone.com/aspnet/Northwind/northwind.svc/Products?$format=json&$top=10&$orderby=Unit_Price%20desc">https://demos.componentone.com/aspnet/Northwind/northwind.svc/Products?$format=json&$top=10&$orderby=Unit_Price%20desc</a> </li>
	</ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>