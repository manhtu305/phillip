﻿<%@ Page Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Paging.aspx.cs"
    Inherits="ToolkitExplorer.Gallery.Paging" Title="Paging" %>

<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1Gallery"
    TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1Pager"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="gallery" runat="server" Font-Overline="False" Title="Overview" Width="750"
        Height="300">
                <ul class="">
            <li class=""><a href="../Images/Sports/1.jpg">
                <img alt="1" src="../Images/Sports/1_small.jpg" title="Word Caption 1" />
            </a></li>
            <li class=""><a href="../Images/Sports/2.jpg">
                <img alt="2" src="../Images/Sports/2_small.jpg" title="Word Caption 2" />
            </a></li>
            <li class=""><a href="../Images/Sports/3.jpg">
                <img alt="3" src="../Images/Sports/3_small.jpg" title="Word Caption 3" />
            </a></li>
            <li class=""><a href="../Images/Sports/4.jpg">
                <img alt="4" src="../Images/Sports/4_small.jpg" title="Word Caption 4" />
            </a></li>
            <li class=""><a href="../Images/Sports/5.jpg">
                <img alt="5" src="../Images/Sports/5_small.jpg" title="Word Caption 5" />
            </a></li>
            <li class=""><a href="../Images/Sports/6.jpg">
                <img alt="6" src="../Images/Sports/6_small.jpg" title="Word Caption 6" />
            </a></li>
            <li class=""><a href="../Images/Sports/7.jpg">
                <img alt="7" src="../Images/Sports/7_small.jpg" title="Word Caption 7" />
            </a></li>
            <li class=""><a href="../Images/Sports/8.jpg">
                <img alt="8" src="../Images/Sports/8_small.jpg" title="Word Caption 8" />
            </a></li>
            <li class=""><a href="../Images/Sports/9.jpg">
                <img alt="9" src="../Images/Sports/9_small.jpg" title="Word Caption 9" />
            </a></li>
            <li class=""><a href="../Images/Sports/10.jpg">
                <img alt="10" src="../Images/Sports/10_small.jpg" title="Word Caption 10" />
            </a></li>
        </ul>
    </asp:Panel>
    <wijmo:C1GalleryExtender runat="server" ID="CarouselExtender2" ThumbnailDirection="After"
        ShowControlsOnHover="false" ShowPager="true" ThumbsDisplay="3" ThumbsLength="120" TargetControlID="gallery">
    </wijmo:C1GalleryExtender>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        This sample illustrates the paging of the gallery.
    </p>
    <ul>
        <li>The property <b>ShowPager</b> is set to determine whether should show the pager.</li>
    </ul>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
