﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Extenders.C1Chart;

namespace ToolkitExplorer.ScatterChart
{
	public partial class Annotation : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				PrepareOptions();
			}
		}

		private void PrepareOptions()
		{
			var valuesX = new List<double?>() { 161.4, 169.0, 166.2, 159.4, 162.5, 159.0, 162.8 };
			var valuesY = new List<double?>() { 63.4, 58.2, 58.6, 45.7, 52.2, 48.6, 57.8 };
			//serieslist Female
			var series = new ScatterChartSeries();
			this.ScatterChartExtender1.SeriesList.Add(series);
			series.MarkerType = MarkerType.Circle;
			series.Data.X.AddRange(valuesX.ToArray<double?>());
			series.Data.Y.AddRange(valuesY.ToArray<double?>());
			series.Label = "Female";
			series.LegendEntry = true;
			series.Visible = true;

			//series Male
			valuesX = new List<double?>() { 157.0, 164.0, 165.1, 167.0, 162.0, 166.5, 169.4 };
			valuesY = new List<double?>() { 70.2, 73.4, 70.5, 68.9, 102.3, 88.4, 65.9 };

			series = new ScatterChartSeries();
			this.ScatterChartExtender1.SeriesList.Add(series);
			series.MarkerType = MarkerType.Diamond;
			series.Data.X.AddRange(valuesX.ToArray<double?>());
			series.Data.Y.AddRange(valuesY.ToArray<double?>());
			series.Label = "Male";
			series.LegendEntry = true;
			series.Visible = true;
		}
	}
}