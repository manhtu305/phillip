﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeBehind="Annotation.aspx.cs" Inherits="ToolkitExplorer.ScatterChart.Annotation" %>

<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1Chart" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:Panel ID="scatterchart" Height="475" Width="756" runat="server" CssClass="ui-widget ui-widget-content ui-corner-all">
	</asp:Panel>
	<wijmo:C1ScatterChartExtender runat="server" ID="ScatterChartExtender1"
		TargetControlID="scatterchart">
		<Hint>
			<Content Function="hintContent" />
		</Hint>
		<SeriesTransition Duration="2000" Enabled="false"></SeriesTransition>
		<Animation Duration="2000" Enabled="false"></Animation>
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#AFE500">
				<Fill Color="#AFE500">
				</Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#FF9900">
				<Fill Color="#FF9900">
				</Fill>
			</wijmo:ChartStyle>
		</SeriesStyles>
		<Header Text="Height Versus Weight of 7 Individuals by Gender">
		</Header>
		<Footer Compass="South" Visible="False">
		</Footer>
		<Legend Visible="true">
			<Size Width="30" Height="3"></Size>
		</Legend>
		<Axis>
			<X Text="Height (cm)">
				<Labels>
					<AxisLabelStyle FontSize="11pt" Rotation="-45">
						<Fill Color="#7F7F7F">
						</Fill>
					</AxisLabelStyle>
				</Labels>
				<GridMajor Visible="false"></GridMajor>
				<TickMajor Position="Outside">
					<TickStyle Stroke="#7F7F7F" />
				</TickMajor>
			</X>
			<Y Compass="West" Text="Weight (kg)" Visible="true">
				<Labels TextAlign="Center">
					<AxisLabelStyle FontSize="11pt">
						<Fill Color="#7F7F7F">
						</Fill>
					</AxisLabelStyle>
				</Labels>
				<GridMajor Visible="True">
					<GridStyle Stroke="#353539" StrokeDashArray="- "></GridStyle>
				</GridMajor>
				<TickMajor Position="Outside">
					<TickStyle Stroke="#7F7F7F" />
				</TickMajor>
				<TickMinor Position="Outside">
					<TickStyle Stroke="#7F7F7F" />
				</TickMinor>
			</Y>
		</Axis>
		<Annotations>
			<wijmo:TextAnnotation Attachment="Relative" Offset="0, 0" Text="Text Annotation" Tooltip="Text Annotation">
				<Point>
					<X DoubleValue="0.5" />
					<Y DoubleValue="0.15" />
				</Point>
				<AnnotationStyle FontSize="20" FontWeight="Bold">
					<Fill Color="#FF66FF" > </Fill >
				</AnnotationStyle>
			</wijmo:TextAnnotation>
			<wijmo:RectAnnotation Attachment="DataIndex" Content="Rect" Height="30" Offset="0, 0" Position="Center, Bottom" Tooltip="Rect" Width="60" PointIndex="4">
				<AnnotationStyle FillOpacity="0.25">
					<Fill Color="#CC99FF" > </Fill >
				</AnnotationStyle>
			</wijmo:RectAnnotation>
			<wijmo:EllipseAnnotation Attachment="DataIndex" Content="Ellipse" Height="35" Offset="0, 0" Tooltip="Ellipse" Width="75" PointIndex="6">
				<AnnotationStyle FillOpacity="0.15">
					<Fill Color="#9966FF" > </Fill >
				</AnnotationStyle>
			</wijmo:EllipseAnnotation>
			<wijmo:CircleAnnotation Content="Circle" Offset="0, 0" Tooltip="Circle">
				<Point>
					<X DoubleValue="200" />
					<Y DoubleValue="100" />
				</Point>
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#666699" > </Fill >
				</AnnotationStyle>
			</wijmo:CircleAnnotation>
			<wijmo:ImageAnnotation Attachment="DataIndex" Href="./../images/c1logo_215x150.png" Offset="0, 0" PointIndex="5" SeriesIndex="0" Tooltip="Image">
			</wijmo:ImageAnnotation>
			<wijmo:PolygonAnnotation Offset="0, 0" Tooltip="Polygon" Content="Polygon">
				<Points>
					<wijmo:PointF X="200" Y="0" />
					<wijmo:PointF X="150" Y="50" />
					<wijmo:PointF X="175" Y="100" />
					<wijmo:PointF X="225" Y="100" />
					<wijmo:PointF X="250" Y="50" />
				</Points>
				<AnnotationStyle FillOpacity="0.25">
					<Fill Color="#9966FF" > </Fill >
				</AnnotationStyle>
			</wijmo:PolygonAnnotation>
			<wijmo:SquareAnnotation Attachment="DataIndex" Content="Square" Offset="0, 0" PointIndex="2" SeriesIndex="0" Tooltip="Square">
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#66FFFF" > </Fill >
				</AnnotationStyle>
			</wijmo:SquareAnnotation>
			<wijmo:LineAnnotation Content="Line" End="100, 100" Offset="0, 0" Start="10, 10" Tooltip="Line">
			</wijmo:LineAnnotation>
		</Annotations>
	</wijmo:C1ScatterChartExtender>
	<script type="text/javascript">
		function hintContent() {
			return this.x + ' cm, ' + this.y + ' kg';
		}
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>
		This sample demonstrates how to add annotation in the <strong>C1ScatterChartExtender</strong>.
	</p>
	<h3>Test the features</h3>
	<ul>
		<li>Hover over mouse over annotation to see the tooltip.</li>
		<li>Click the legend item to toggle the visibility.</li>
	</ul>
</asp:Content>
