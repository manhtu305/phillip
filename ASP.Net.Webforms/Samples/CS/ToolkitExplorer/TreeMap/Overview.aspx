﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeBehind="Overview.aspx.cs" Inherits="ToolkitExplorer.TreeMap.Overview" %>
<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1TreeMap"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<script type="text/javascript">
		function labelFormatter() {
			return "Country: " + this.label + "<br/> GDP: " + this.value + " $BN";
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:Panel runat="server" ID="TreeMapPanel" Height="600"></asp:Panel>
	<wijmo:C1TreeMapExtender runat="server" ID="C1TreeMapExtender1" TargetControlID="TreeMapPanel"  MaxColor="#ff0000" MinColor="#ffaaaa" MinColorValue="0" LabelFormatter="labelFormatter">
		<Items>
				<wijmo:TreeMapItem Label="North America" Value="18625">
					<Items>
						<wijmo:TreeMapItem Label="United States" Value="16800"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Canada" Value="1825"></wijmo:TreeMapItem>
					</Items>
				</wijmo:TreeMapItem>
				<wijmo:TreeMapItem Label="Asia" Value="18934">
					<Items>
						<wijmo:TreeMapItem Label="China" Value="9240"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Japan" Value="4901"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="India" Value="1876"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="South Korea" Value="1304"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Indonesia" Value="868"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Saudi Arabia" Value="745"></wijmo:TreeMapItem>
					</Items>
				</wijmo:TreeMapItem>
				<wijmo:TreeMapItem Label="Europe" Value="16685">
					<Items>
						<wijmo:TreeMapItem Label="Germany" Value="3634"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="France" Value="2734"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="United Kingdom" Value="2522"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Russia" Value="2096"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Italy" Value="2071"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Spain" Value="1358"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Turkey" Value="820"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Netherlands" Value="800"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Switzerland" Value="650"></wijmo:TreeMapItem>
					</Items>
				</wijmo:TreeMapItem>
				<wijmo:TreeMapItem Label="South America" Value="4554">
					<Items>
						<wijmo:TreeMapItem Label="Brazil" Value="2245"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Mexico" Value="1260"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Argentina" Value="611"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="Venezuela" Value="438"></wijmo:TreeMapItem>
					</Items>
				</wijmo:TreeMapItem>
				<wijmo:TreeMapItem Label="Australasia & Oceania" Value="1742">
					<Items>
						<wijmo:TreeMapItem Label="Australia" Value="1560"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="New Zealand" Value="182"></wijmo:TreeMapItem>
					</Items>
				</wijmo:TreeMapItem>
				<wijmo:TreeMapItem Label="Africa" Value="872">
					<Items>
						<wijmo:TreeMapItem Label="Nigeria" Value="522"></wijmo:TreeMapItem>
						<wijmo:TreeMapItem Label="South Africa" Value="350"></wijmo:TreeMapItem>
					</Items>
				</wijmo:TreeMapItem>
			</Items>
	</wijmo:C1TreeMapExtender>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>
        <p>The <b>C1TreeMapExtender</b> control displays hierarchical data as a set of nested rectangles where the area of each rectangle is proportional to its value.</p>
</asp:Content>
