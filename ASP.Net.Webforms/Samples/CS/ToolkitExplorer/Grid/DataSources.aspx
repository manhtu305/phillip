﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="DataSources.aspx.cs" Inherits="ControlExplorer.Grid.DataSources" %>
<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1Grid" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<style type="text/css">
		.wijmo-wijgrid 
		{
			min-height: 100px;
		}
	</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<script type="text/javascript">

	    // random data generators
	    function getData(count) {
	        var data = [];
	        var name = "Lorem,Ipsum,Dolor,Amet,Consectetur,Adipiscing,Elit,Vivamus,Pharetra,Velit,Eget,Imperdiet,Mattis,Egestas,Donec,Tempus,Vestibulum,Tincidunt,Blandit,Lacinia,Lobortis,Feugiat,Mauris,Massa,Rutrum,Pulvinar,Ornare,Facilisi,Sociis,Natoque,Penatibus".split(",");
	        var suffix = "LLC,Inc,International,Transpacific,Services,Financial,Enterprises,Consultants,Foundation,Institute".split(",");
	        for (var i = 0; i < count; i++) {
	            data.push({
	                TransactionID: i,
	                ProductName: getString(name) + " " + getString(suffix),
	                UnitPrice: getNumber(10, 100),
	                Quantity: Math.floor(getNumber(1, 500))
	            });
	        }
	        return data;
	    }

	    function getString(arr) {
	        return arr[Math.floor((Math.random() * arr.length))];
	    }

	    function getNumber(lo, hi) {
	        return lo + Math.random() * (hi - lo);
	    }

	    var dataArray = getData(5);
	</script>

	<p>DOM table:</p>
	<asp:GridView runat="server" ID="demoDOM" ShowHeader="false" DataSourceID="AccessDataSource1" />

	<asp:AccessDataSource ID="AccessDataSource1" runat="server" 
		DataFile="~/App_Data/C1NWind.mdb" 
		SelectCommand="SELECT TOP 5 [EmployeeID], [LastName], [FirstName], [Title] FROM [Employees]">
	</asp:AccessDataSource>

	<wijmo:C1GridExtender runat="server" ID="GridExtender1" TargetControlID="demoDOM">
		<Columns>
			<wijmo:C1Field HeaderText="EmployeeID" />
			<wijmo:C1Field HeaderText="LastName" />
			<wijmo:C1Field HeaderText="FirstName" />
			<wijmo:C1Field HeaderText="Title" />
		</Columns> 
	</wijmo:C1GridExtender>

	<p>DataTable:</p>
	<asp:Table runat="server" ID="demoDataTable"></asp:Table>
	<wijmo:C1GridExtender runat="server" ID="GridExtender2" TargetControlID="demoDataTable" />

	<p>JavaScript Array:</p>
	<asp:Table runat="server" ID="demoJavaScriptArray"></asp:Table>
	<wijmo:C1GridExtender runat="server" ID="GridExtender3" TargetControlID="demoJavaScriptArray">
		<Data DataSourceID="dataArray" />
	</wijmo:C1GridExtender>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
		This sample shows how to load data into the grid using various methods such as DOM table, DataTable and JavaScript array.
	</p>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
