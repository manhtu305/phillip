﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="CellRowFormatting.aspx.cs" Inherits="ControlExplorer.Grid.CellRowFormatting" %>
<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1Grid" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1Tooltip" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<style type="text/css">
		.wijmo-wijgrid 
		{
			min-height: 100px;
		}
	</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<script type="text/javascript">
	    function flagCellFormatter(args) {
	        // get url to flag image
	        var getFlagUrl = function (country) {
	            var url = "http://www.geonames.org/flags/m/";
	            switch (country) {
	                case "USA": return url + "us.png";
	                case "UK": return url + "uk.png";
	                case "Germany": return url + "de.png";
	                case "Italy": return url + "it.png";
	                case "Japan": return url + "jp.png";
	                case "Brazil": return url + "br.png";
	                case "Canada": return url + "ca.png";
	            }
	            return "";
	        };

			if (args.row.type & $.wijmo.wijgrid.rowType.data) { // data row (not group header)
			    var img = $("<img/>")
					.attr("src", getFlagUrl(args.row.data.Country)); // flag url

				args.$container
					.css("text-align", "center")    // center the flag
					.empty()                        // remove original content
					.append(img);                   // add image element

				return true;                        // content has been customized
			}
		}

		function discountCellFormatter(args) {
		    if (args.row.type & $.wijmo.wijgrid.rowType.data) { // data row (not group header)
		        if (args.row.type & $.wijmo.wijgrid.rowType.data) { // data row (not group header)
		            if (args.row.data.Discount > .25) {             // discount > 25%? bold + red!
		                args.$container.css("font-weight", "bold");
		                args.$container.css("color", "red");
		            }
		        }
		    }
		}

		// random data generators
		function getData(count) {
		    var data = [];
		    var country = "USA,UK,Germany,Italy,Japan,Brazil,Canada".split(",");
		    var name = "Lorem,Ipsum,Dolor,Amet,Consectetur".split(",");
		    var suffix = "LLC,Inc".split(",");
		    for (var i = 0; i < count; i++) {
		        data.push({
		            TransactionID: i,
		            Country: getString(country),
		            ProductName: getString(name) + " " + getString(suffix),
		            UnitPrice: Math.floor(getNumber(5, 10)),
		            Quantity: Math.floor(getNumber(1, 5)) * 10,
		            Discount: getNumber(0, 0.3)
		        });
		    }
		    return data;
		}
		function getString(arr) {
		    return arr[Math.floor((Math.random() * arr.length))];
		}
		function getNumber(lo, hi) {
		    return lo + Math.random() * (hi - lo);
		}
		function getDate(daysAgo) {
		    return new Date((new Date()).getTime() - daysAgo * 24 * 3600 * 1000);
		}

		var dataArray = getData(12);
	</script>

	<a id="tooltipAnchor"></a>
	<asp:Table runat="server" ID="demo"></asp:Table>

	<wijmo:C1TooltipExtender runat="server" ID="TooltipExtender" TargetSelector="#foo">
	</wijmo:C1TooltipExtender>

	<wijmo:C1GridExtender runat="server" ID="GridExtender1" TargetControlID="demo" AllowPaging="true" PageSize="5" ColumnsAutogenerationMode="None">
		<Data DataSourceID="dataArray" />
		<Columns>
			<wijmo:C1Field HeaderText="Country" DataType="String">
				<DataKey ValueString="Country" NeedQuotes="true" />
			</wijmo:C1Field> 
			<wijmo:C1Field HeaderText="Flag" DataType="String" CellFormatter="flagCellFormatter">
				<DataKey ValueString="Country" NeedQuotes="true" />
			</wijmo:C1Field> 
			<wijmo:C1Field HeaderText="Product Name" DataType="String">
				<DataKey ValueString="ProductName" NeedQuotes="true" />
			</wijmo:C1Field> 
			<wijmo:C1Field HeaderText="Unit Price" DataType="Currency">
				<DataKey ValueString="UnitPrice" NeedQuotes="true" />
			</wijmo:C1Field> 
			<wijmo:C1Field HeaderText="Quantity" DataType="Number" DataFormatString="n0">
				<DataKey ValueString="Quantity" NeedQuotes="true" />
			</wijmo:C1Field> 
			<wijmo:C1Field HeaderText="Discount" DataType="Number" DataFormatString="p0" CellFormatter="discountCellFormatter">
				<DataKey ValueString="Discount" NeedQuotes="true" />
			</wijmo:C1Field> 
		</Columns>
	</wijmo:C1GridExtender>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
	This sample shows how to change the content, style and attributes of the cells using the <b>cellFormatter</b> property.
	</p>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
