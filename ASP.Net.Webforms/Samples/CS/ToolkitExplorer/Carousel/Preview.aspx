﻿<%@ Page Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Preview.aspx.cs"
    Inherits="ToolkitExplorer.Carousel.Preview" Title="Untitled Page" %>

<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1Carousel"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Panel ID="carousel" runat="server" Font-Overline="False" Title="Overview" Width="500"
        Height="200">
        <ul>
            <li>
                <img src="../Images/Sports/1.jpg" />
                <span>
                    <h3>
                        Word Caption 1</h3>
                    Word Caption 1</span> </li>
            <li>
                <img src="../Images/Sports/2.jpg"/>
                <span>
                    <h3>
                        Word Caption 2</h3>
                    Word Caption 2</span> </li>
            <li>
                <img src="../Images/Sports/3.jpg"/>
                <span>
                    <h3>
                        Word Caption 3</h3>
                    Word Caption 3</span> </li>
            <li>
                <img src="../Images/Sports/4.jpg"/>
                <span>
                    <h3>
                        Word Caption 4</h3>
                    Word Caption 4</span> </li>
           <li>
                <img src="../Images/Sports/5.jpg"/>
                <span>
                    <h3>
                        Word Caption 5</h3>
                    Word Caption 5</span> </li>
                   <li>
                <img src="../Images/Sports/6.jpg"/>
                <span>
                    <h3>
                        Word Caption 6</h3>
                    Word Caption 6</span> </li>
        </ul>
    </asp:Panel>
    <wijmo:C1CarouselExtender runat="server" ID="CarouselExtender1" TargetControlID="carousel"
        Preview="true" Loop="false">
    </wijmo:C1CarouselExtender>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        In this sample, you will see the preview mode of carousel, the next and previous
        picture will show partially for preview.
    </p>
    <p>
        Preview mode is allowed if the property <strong>Preview</strong> is set to true
        and <strong>Loop</strong> is set to false.
    </p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
