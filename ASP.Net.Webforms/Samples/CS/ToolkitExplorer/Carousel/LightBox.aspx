﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="LightBox.aspx.cs" Inherits="ToolkitExplorer.Carousel.LightBox" %>

<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1Carousel"
	TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<script type = "text/javascript">
		function onItemClick(event, ui) {
			var a = ui.el.children("a"), img = a.children("img:eq(0)"),
                    url = a.attr("href"), title = img.attr("title");
			if (!a.is(":wijmo-wijlightbox")) {
			    a.wijlightbox();
			}
			a.trigger("click");
			event.preventDefault();
		}
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:Panel ID="carousel" runat="server" CssClass="ui-corner-all ui-widget-content" Width="650px"
		Height="90px">
		<ul>
			<li><a href="../Images/Sports/1.jpg">
				<img alt="1" src="../Images/Sports/1_Small.jpg" title="Word Caption 1" />
			</a></li>
			<li><a href="../Images/Sports/2.jpg">
				<img alt="2" src="../Images/Sports/2_Small.jpg" title="Word Caption 2" />
			</a></li>
			<li><a href="../Images/Sports/3.jpg">
				<img alt="3" src="../Images/Sports/3_Small.jpg" title="Word Caption 3" />
			</a></li>
			<li><a href="../Images/Sports/4.jpg">
				<img alt="4" src="../Images/Sports/4_Small.jpg" title="Word Caption 4" />
			</a></li>
			<li><a href="../Images/Sports/5.jpg">
				<img alt="5" src="../Images/Sports/5_Small.jpg" title="Word Caption 5" />
			</a></li>
			<li><a href="../Images/Sports/6.jpg">
				<img alt="6" src="../Images/Sports/6_Small.jpg" title="Word Caption 6" />
			</a></li>
			<li><a href="../Images/Sports/7.jpg">
				<img alt="7" src="../Images/Sports/7_Small.jpg" title="Word Caption 7" />
			</a></li>
			<li><a href="../Images/Sports/8.jpg">
				<img alt="8" src="../Images/Sports/8_Small.jpg" title="Word Caption 8" />
			</a></li>
			<li><a href="../Images/Sports/9.jpg">
				<img alt="9" src="../Images/Sports/9_Small.jpg" title="Word Caption 9" />
			</a></li>
			<li><a href="../Images/Sports/10.jpg">
				<img alt="10" src="../Images/Sports/10_Small.jpg" title="Word Caption 10" />
			</a></li>
		</ul>
	</asp:Panel>
	<wijmo:C1CarouselExtender runat="server" ID="CarouselExtender1" TargetControlID="carousel" 
		Display="5" Step = "4" Loop ="false" OnClientItemClick = "onItemClick">
	</wijmo:C1CarouselExtender>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	This sample demonstrates how the lightbox extender can be integrated with the carousel extender to show images from a carousel in a lightbox.
</asp:Content>
