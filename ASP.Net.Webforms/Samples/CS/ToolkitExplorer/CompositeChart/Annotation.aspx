﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeBehind="Annotation.aspx.cs" Inherits="ToolkitExplorer.CompositeChart.Annotation" %>

<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1Chart" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:Panel ID="compositechart" Height="475" Width="756" runat="server" CssClass="ui-widget ui-widget-content ui-corner-all">
	</asp:Panel>
	<wijmo:C1CompositeChartExtender runat="server" ID="CompositeChartExtender1"
		TargetControlID="compositechart">
		<Footer Compass="South" Visible="False"></Footer>
		<Axis>
			<X Text="">
				<GridMajor Visible="True"></GridMajor>

				<GridMinor Visible="False"></GridMinor>
			</X>
			<Y Text="Total Hardware" Compass="West">
				<GridMajor Visible="True"></GridMajor>

				<GridMinor Visible="False"></GridMinor>
			</Y>
		</Axis>
		<Header Text="Hardware Distribution">
		</Header>
		<SeriesList>
			<wijmo:CompositeChartSeries Label="West" LegendEntry="true" Type="Column">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Desktops"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Notebooks"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="AIO"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Tablets"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Phones"></wijmo:ChartXData>
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="5"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="3"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="4"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="7"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="2"></wijmo:ChartYData>
						</Values>
					</Y>
				</Data>

				<CandlestickSeries LegendEntry="True"></CandlestickSeries>
			</wijmo:CompositeChartSeries>
			<wijmo:CompositeChartSeries Label="Central" LegendEntry="true" Type="Column">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Desktops"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Notebooks"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="AIO"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Tablets"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Phones"></wijmo:ChartXData>
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="2"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="2"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="3"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="2"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="1"></wijmo:ChartYData>
						</Values>
					</Y>
				</Data>

				<CandlestickSeries LegendEntry="True"></CandlestickSeries>
			</wijmo:CompositeChartSeries>
			<wijmo:CompositeChartSeries Label="East" LegendEntry="true" Type="Column">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Desktops"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Notebooks"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="AIO"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Tablets"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Phones"></wijmo:ChartXData>
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="3"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="4"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="4"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="2"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="5"></wijmo:ChartYData>
						</Values>
					</Y>
				</Data>

				<CandlestickSeries LegendEntry="True"></CandlestickSeries>
			</wijmo:CompositeChartSeries>
			<wijmo:CompositeChartSeries Label="US" LegendEntry="true" Type="Line">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Desktops"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Notebooks"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="AIO"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Tablets"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Phones"></wijmo:ChartXData>
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="3"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="6"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="2"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="9"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="5"></wijmo:ChartYData>
						</Values>
					</Y>
				</Data>

				<CandlestickSeries LegendEntry="True"></CandlestickSeries>
			</wijmo:CompositeChartSeries>
			<wijmo:CompositeChartSeries Label="Canada" LegendEntry="true" Type="Line">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Desktops"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Notebooks"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="AIO"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Tablets"></wijmo:ChartXData>
							<wijmo:ChartXData StringValue="Phones"></wijmo:ChartXData>
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="3"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="4"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="7"></wijmo:ChartYData>
							<wijmo:ChartYData DoubleValue="2"></wijmo:ChartYData>
						</Values>
					</Y>
				</Data>
				<CandlestickSeries LegendEntry="True"></CandlestickSeries>
			</wijmo:CompositeChartSeries>
		</SeriesList>

		<Indicator Visible="False"></Indicator>
		<Annotations>
			<wijmo:TextAnnotation Attachment="Relative" Offset="0, 0" Text="Text Annotation" Tooltip="Text Annotation">
				<Point>
					<X DoubleValue="0.5" />
					<Y DoubleValue="0.15" />
				</Point>
				<AnnotationStyle FontSize="20" FontWeight="Bold">
					<Fill Color="#FF66FF" > </Fill >
				</AnnotationStyle>
			</wijmo:TextAnnotation>
			<wijmo:RectAnnotation Attachment="DataIndex" Content="Rect" Height="30" Offset="0, 0" Position="Center, Bottom" Tooltip="Rect" Width="60" PointIndex="4">
				<AnnotationStyle FillOpacity="0.25">
					<Fill Color="#CC99FF" > </Fill >
				</AnnotationStyle>
			</wijmo:RectAnnotation>
			<wijmo:EllipseAnnotation Attachment="DataIndex" Content="Ellipse" Height="35" Offset="0, 0" SeriesIndex="3" Tooltip="Ellipse" Width="75" PointIndex="3">
				<AnnotationStyle FillOpacity="0.15">
					<Fill Color="#9966FF" > </Fill >
				</AnnotationStyle>
			</wijmo:EllipseAnnotation>
			<wijmo:CircleAnnotation Content="Circle" Offset="0, 0" Tooltip="Circle">
				<Point>
					<X DoubleValue="270" />
					<Y DoubleValue="100" />
				</Point>
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#666699" > </Fill >
				</AnnotationStyle>
			</wijmo:CircleAnnotation>
			<wijmo:ImageAnnotation Attachment="DataIndex" Href="./../images/c1logo_215x150.png" Offset="0, 0" PointIndex="4" SeriesIndex="1" Tooltip="Image">
			</wijmo:ImageAnnotation>
			<wijmo:SquareAnnotation Attachment="DataIndex" Content="Square" Offset="0, 0" PointIndex="2" SeriesIndex="0" Tooltip="Square">
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#66FFFF" > </Fill >
				</AnnotationStyle>
			</wijmo:SquareAnnotation>
			<wijmo:LineAnnotation Content="Line" End="100, 100" Offset="0, 0" Start="10, 10" Tooltip="Line">
				<AnnotationStyle StrokeWidth="10">
				</AnnotationStyle>
			</wijmo:LineAnnotation>
			<wijmo:PolygonAnnotation Offset="0, 0" Tooltip="Polygon" Content="Polygon">
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#3399FF" > </Fill >
				</AnnotationStyle>
				<Points>
					<wijmo:PointF X="200" Y="0" />
					<wijmo:PointF X="150" Y="50" />
					<wijmo:PointF X="175" Y="100" />
					<wijmo:PointF X="225" Y="100" />
					<wijmo:PointF X="250" Y="50" />
				</Points>
			</wijmo:PolygonAnnotation>
		</Annotations>
	</wijmo:C1CompositeChartExtender>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>
		This sample demonstrates how to add annotation in the <strong>C1CompositeChartExtender</strong>.
	</p>
	<h3>Test the features</h3>
	<ul>
		<li>Hover over mouse over annotation to see the tooltip.</li>
		<li>Click the legend item to toggle the visibility.</li>
	</ul>
</asp:Content>
