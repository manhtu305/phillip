﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ToolkitExplorer.Editor
{
	public partial class ReadOnly : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}
		protected void chkReadOnly_CheckedChanged(object sender, EventArgs e)
		{
			edt.ReadOnly = chkReadOnly.Checked;
		}
	}
}