﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ReadOnly.aspx.cs" Inherits="ToolkitExplorer.Editor.ReadOnly" %>

<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1Editor"
	TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:TextBox ID="TextBox1" runat="server" Height="530" TextMode="MultiLine"
		Width="800">
			Extenders
	</asp:TextBox>
	<wijmo:C1EditorExtender id="edt" readonly="true" runat="server" targetcontrolid="TextBox1" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
	<asp:CheckBox runat="server" ID="chkReadOnly" Checked="true" Text="ReadOnly" 
		AutoPostBack="true" OnCheckedChanged="chkReadOnly_CheckedChanged"/>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>
		<strong>C1EditorExtender</strong> supports readonly mode. 
	</p>
</asp:Content>
