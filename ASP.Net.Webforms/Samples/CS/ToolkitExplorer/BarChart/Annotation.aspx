﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeBehind="Annotation.aspx.cs" Inherits="ToolkitExplorer.BarChart.Annotation" %>

<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1Chart" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:Panel ID="barchart" Height="450" Width="765" runat="server" CssClass="ui-widget ui-widget-content ui-corner-all">
	</asp:Panel>
	<wijmo:C1BarChartExtender runat="server" ID="BarChartExtender1" Horizontal="false"
		TargetControlID="barchart">
		<Hint>
			<Content Function="hintContent" />
			<ContentStyle FontSize="14px">
				<Fill Color="#CCCCCC">
				</Fill>
			</ContentStyle>
			<Style StrokeWidth="0">
			</Style>
			<HintStyle StrokeWidth="0">
			</HintStyle>
		</Hint>
		<Footer Compass="South" Visible="False"></Footer>
		<Legend Visible="True" Compass="North" Orientation="Horizontal">
			<TextStyle StrokeWidth="0">
			</TextStyle>
		</Legend>
		<Axis>
			<X>
				<GridMajor Visible="True"></GridMajor>

				<GridMinor Visible="False"></GridMinor>
			</X>

			<Y Text="USD (thousands)" Compass="West" AnnoFormatString="n0" Max="4000" Min="0" AutoMax="False" AutoMin="False" Alignment="Far">
				<GridMajor Visible="True"></GridMajor>

				<GridMinor Visible="False"></GridMinor>
			</Y>
		</Axis>
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#94C3E8">
				<Fill Color="#94C3E8">
				</Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Stroke="#FBBA69">
				<Fill Color="#FBBA69">
				</Fill>
			</wijmo:ChartStyle>
		</SeriesStyles>
		<SeriesHoverStyles>
			<wijmo:ChartStyle StrokeWidth="0">
			</wijmo:ChartStyle>
		</SeriesHoverStyles>
		<TextStyle FontSize="13px">
		</TextStyle>
		<Header Text="2014 Sales Revenue"></Header>
		<SeriesList>
			<wijmo:BarChartSeries Label="Domestic" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData DateTimeValue="2014-01-01" />
							<wijmo:ChartXData DateTimeValue="2014-02-01" />
							<wijmo:ChartXData DateTimeValue="2014-03-01" />
							<wijmo:ChartXData DateTimeValue="2014-04-01" />
							<wijmo:ChartXData DateTimeValue="2014-05-01" />
							<wijmo:ChartXData DateTimeValue="2014-06-01" />
							<wijmo:ChartXData DateTimeValue="2014-07-01" />
							<wijmo:ChartXData DateTimeValue="2014-08-01" />
							<wijmo:ChartXData DateTimeValue="2014-09-01" />
							<wijmo:ChartXData DateTimeValue="2014-10-01" />
							<wijmo:ChartXData DateTimeValue="2014-11-01" />
							<wijmo:ChartXData DateTimeValue="2014-12-01" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1983" />
							<wijmo:ChartYData DoubleValue="2343" />
							<wijmo:ChartYData DoubleValue="2593" />
							<wijmo:ChartYData DoubleValue="2283" />
							<wijmo:ChartYData DoubleValue="2574" />
							<wijmo:ChartYData DoubleValue="2838" />
							<wijmo:ChartYData DoubleValue="2382" />
							<wijmo:ChartYData DoubleValue="2634" />
							<wijmo:ChartYData DoubleValue="2938" />
							<wijmo:ChartYData DoubleValue="2739" />
							<wijmo:ChartYData DoubleValue="2983" />
							<wijmo:ChartYData DoubleValue="3493" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries LegendEntry="True" Label="International">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData DateTimeValue="2014-01-01" />
							<wijmo:ChartXData DateTimeValue="2014-02-01" />
							<wijmo:ChartXData DateTimeValue="2014-03-01" />
							<wijmo:ChartXData DateTimeValue="2014-04-01" />
							<wijmo:ChartXData DateTimeValue="2014-05-01" />
							<wijmo:ChartXData DateTimeValue="2014-06-01" />
							<wijmo:ChartXData DateTimeValue="2014-07-01" />
							<wijmo:ChartXData DateTimeValue="2014-08-01" />
							<wijmo:ChartXData DateTimeValue="2014-09-01" />
							<wijmo:ChartXData DateTimeValue="2014-10-01" />
							<wijmo:ChartXData DateTimeValue="2014-11-01" />
							<wijmo:ChartXData DateTimeValue="2014-12-01" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="574" />
							<wijmo:ChartYData DoubleValue="636" />
							<wijmo:ChartYData DoubleValue="673" />
							<wijmo:ChartYData DoubleValue="593" />
							<wijmo:ChartYData DoubleValue="644" />
							<wijmo:ChartYData DoubleValue="679" />
							<wijmo:ChartYData DoubleValue="593" />
							<wijmo:ChartYData DoubleValue="139" />
							<wijmo:ChartYData DoubleValue="599" />
							<wijmo:ChartYData DoubleValue="583" />
							<wijmo:ChartYData DoubleValue="602" />
							<wijmo:ChartYData DoubleValue="690" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
		</SeriesList>

		<Indicator Visible="False"></Indicator>
		<Annotations>
			<wijmo:TextAnnotation Attachment="Relative" Offset="0, 0" Text="Text Annotation" Tooltip="Text Annotation">
				<Point>
					<X DoubleValue="0.5" />
					<Y DoubleValue="0.15" />
				</Point>
				<AnnotationStyle FontSize="20" FontWeight="Bold">
					<Fill Color="#FF66FF" > </Fill >
				</AnnotationStyle>
			</wijmo:TextAnnotation>
			<wijmo:RectAnnotation Attachment="DataIndex" Content="sIdx:0,pIdx:4" Height="30" Offset="0, 0" Position="Center, Bottom" SeriesIndex="0" Tooltip="sIdx:0,pIdx:4" PointIndex="4" Width="70">
				<AnnotationStyle FillOpacity="0.25">
					<Fill Color="#CC99FF" > </Fill >
				</AnnotationStyle>
			</wijmo:RectAnnotation>
			<wijmo:EllipseAnnotation Attachment="DataIndex" Content="sIdx:1,pIdx:11" Height="35" Offset="0, 0" SeriesIndex="1" Tooltip="sIdx:1,pIdx:11" Width="75" PointIndex="11">
				<AnnotationStyle FillOpacity="0.15">
					<Fill Color="#9966FF" > </Fill >
				</AnnotationStyle>
			</wijmo:EllipseAnnotation>
			<wijmo:CircleAnnotation Attachment="DataCoordinate" Content="Circle" Offset="0, 0" Tooltip="Circle">
				<Point>
					<X DateTimeValue="2014-01-02" />
					<Y DoubleValue="3000" />
				</Point>
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#666699" > </Fill >
				</AnnotationStyle>
			</wijmo:CircleAnnotation>
			<wijmo:ImageAnnotation Attachment="DataIndex" Href="./../images/c1logo_215x150.png" Width="70" Height="70" Offset="0, 0" PointIndex="5" SeriesIndex="1" Tooltip="">
			</wijmo:ImageAnnotation>
			<wijmo:PolygonAnnotation Offset="0, 0" Tooltip="" Content="Polygon">
				<Points>
					<wijmo:PointF X="200" Y="0" />
					<wijmo:PointF X="150" Y="50" />
					<wijmo:PointF X="175" Y="100" />
					<wijmo:PointF X="225" Y="100" />
					<wijmo:PointF X="250" Y="50" />
				</Points>
				<AnnotationStyle FillOpacity="0.25">
					<Fill Color="#9966FF" > </Fill >
				</AnnotationStyle>
			</wijmo:PolygonAnnotation>
			<wijmo:SquareAnnotation Attachment="DataIndex" Content="Square" Offset="0, 0" PointIndex="2" SeriesIndex="1" Tooltip="">
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#66FFFF" > </Fill >
				</AnnotationStyle>
			</wijmo:SquareAnnotation>
			<wijmo:LineAnnotation Content="Line" End="100, 100" Offset="0, 0" AnnotationStyle-StrokeWidth="5" Start="10, 10" Tooltip="Line">
			</wijmo:LineAnnotation>
		</Annotations>
	</wijmo:C1BarChartExtender>
	<script type="text/javascript">
		function hintContent() {
			//Check if multiple data points are on one axis entry. For example, multiple data entries for a single date. 
			if ($.isArray(this)) {
				var content = "";
				//Multiple entries of data on this point, so we need to loop through them to create the tooltip content.
				for (var i = 0; i < this.length; i++) {
					content += this[i].label + ': ' + Globalize.format(this[i].y * 1000, 'c0') + '\n';
				}
				return content;
			}
			else {
				//Only a single data point, so we return a formatted version of it. "/n" is a line break.
				return this.data.label + '\n' +
					//Format x as Short Month and long year (Jan 2010). Then format y value as calculated currency with no decimal ($1,983,000). 
					Globalize.format(this.x, 'MMM yyyy') + ': ' + Globalize.format(this.y * 1000, 'c0');
			}
		}
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>
		This sample demonstrates how to add annotation in the <strong>C1BarChartExtender</strong>.
	</p>
	<h3>Test the features</h3>
	<ul>
		<li>Hover over mouse over annotation to see the tooltip.</li>
		<li>Click the legend item to toggle the visibility.</li>
	</ul>
</asp:Content>
