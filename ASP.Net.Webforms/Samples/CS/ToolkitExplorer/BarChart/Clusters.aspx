﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeFile="Clusters.aspx.cs" Inherits="BarChart_Clusters" %>
<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1Chart" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
      	<script type = "text/javascript">
	  $(document).ready(function () {
	  	$("text.wijbarchart-label").attr("text-anchor", 'start');
	  	$("text.wijbarchart-label").css("text-anchor", 'start');
	  });
	</script>
	<asp:ScriptManager ID="ScriptManager1" runat="server">
	</asp:ScriptManager>
	<asp:Panel ID="barchart" Height="475" Width = "756" runat="server" CssClass="ui-widget ui-widget-content ui-corner-all">
	</asp:Panel>
	<wijmo:C1BarChartExtender runat = "server" ID="BarChartExtender1"
		TargetControlID="barchart">
		<Axis>
			<X Text="Percentage (%)"></X>
			<Y Text="Graphics Card" Compass="West"></Y>
		</Axis>

<Footer Compass="South" Visible="False"></Footer>

		<Legend Text="Month"></Legend>
		<Header Text="Steam Top 5 Video Cards - Hardware Survey"></Header>
		<SeriesStyles>
			<wijmo:ChartStyle Opacity="0.8" Stroke="#2d2d2d" StrokeWidth="1.5" >
				<Fill ColorBegin="#333333" ColorEnd="#2D2D2D" Type="LinearGradient">
				</Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Opacity="0.8" Stroke="#5f9996" StrokeWidth="1.5" >
				<Fill ColorBegin="#6AABA7" ColorEnd="#5F9996" Type="LinearGradient">
				</Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Opacity="0.8" Stroke="#afe500" StrokeWidth="1.5" >
				<Fill ColorBegin="#C3FF00" ColorEnd="#AFE500" Type="LinearGradient">
				</Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Opacity="0.8" Stroke="#b2c76d" StrokeWidth="1.5" >
				<Fill ColorBegin="#C7DE7A" ColorEnd="#B2C76D" Type="LinearGradient">
				</Fill>
			</wijmo:ChartStyle>
			<wijmo:ChartStyle Opacity="0.8" Stroke="#959595" StrokeWidth="1.5" >
				<Fill ColorBegin="#A6A6A6" ColorEnd="#959595" Type="LinearGradient">
				</Fill>
			</wijmo:ChartStyle>
		</SeriesStyles>
		<SeriesHoverStyles>
			<wijmo:ChartStyle Opacity="1" StrokeWidth="1.5" />
			<wijmo:ChartStyle Opacity="1" StrokeWidth="1.5" />
			<wijmo:ChartStyle Opacity="1" StrokeWidth="1.5" />
			<wijmo:ChartStyle Opacity="1" StrokeWidth="1.5" />
			<wijmo:ChartStyle Opacity="1" StrokeWidth="1.5" />
		</SeriesHoverStyles>
		<SeriesList>
			<wijmo:BarChartSeries Label="May" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Mobile Intel 4 Series Express" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 8600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTS 150" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 9600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTX 260" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1.91" />
							<wijmo:ChartYData DoubleValue="1.90" />
							<wijmo:ChartYData DoubleValue="1.61" />
							<wijmo:ChartYData DoubleValue="2.23" />
							<wijmo:ChartYData DoubleValue="2.85" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries Label="Jun" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Mobile Intel 4 Series Express" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 8600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTS 150" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 9600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTX 260" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1.82" />
							<wijmo:ChartYData DoubleValue="1.88" />
							<wijmo:ChartYData DoubleValue="1.77" />
							<wijmo:ChartYData DoubleValue="2.33" />
							<wijmo:ChartYData DoubleValue="2.97" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries Label="Jul" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Mobile Intel 4 Series Express" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 8600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTS 150" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 9600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTX 260" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1.94" />
							<wijmo:ChartYData DoubleValue="1.80" />
							<wijmo:ChartYData DoubleValue="1.81" />
							<wijmo:ChartYData DoubleValue="2.23" />
							<wijmo:ChartYData DoubleValue="2.83" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries Label="Aug" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Mobile Intel 4 Series Express" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 8600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTS 150" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 9600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTX 260" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1.89" />
							<wijmo:ChartYData DoubleValue="1.84" />
							<wijmo:ChartYData DoubleValue="1.96" />
							<wijmo:ChartYData DoubleValue="2.29" />
							<wijmo:ChartYData DoubleValue="2.93" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
			<wijmo:BarChartSeries Label="Sep" LegendEntry="true">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData StringValue="Mobile Intel 4 Series Express" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 8600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTS 150" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce 9600" />
							<wijmo:ChartXData StringValue="NVIDIA GeForce GTX 260" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="1.72" />
							<wijmo:ChartYData DoubleValue="1.80" />
							<wijmo:ChartYData DoubleValue="2.17" />
							<wijmo:ChartYData DoubleValue="2.40" />
							<wijmo:ChartYData DoubleValue="3.30" />
						</Values>
					</Y>
				</Data>
			</wijmo:BarChartSeries>
		</SeriesList>
	</wijmo:C1BarChartExtender>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p>
		The C1BarChartExtender supports clustered bars.
	</p>
	<p>
		Clustered bar chart is allowed if set more than one BarChartSeries.
	</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>

