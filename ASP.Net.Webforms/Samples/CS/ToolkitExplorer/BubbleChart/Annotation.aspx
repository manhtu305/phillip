﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeBehind="Annotation.aspx.cs" Inherits="ToolkitExplorer.BubbleChart.Annotation" %>

<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1Chart" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders" TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:Panel ID="bubblechart" Height="475" Width="756" runat="server" CssClass="ui-widget ui-widget-content ui-corner-all">
	</asp:Panel>
	<wijmo:C1BubbleChartExtender runat="server" ID="BubbleChartExtender1"
		TargetControlID="bubblechart">
		<Animation Duration="500" Easing="EaseOutElastic"></Animation>
		<Header Compass="North"></Header>
		<Footer Compass="South" Visible="False"></Footer>
		<Axis>
			<X>
				<TextStyle Rotation="-45">
				</TextStyle>
				<GridMajor Visible="True"></GridMajor>
				<GridMinor Visible="False"></GridMinor>
			</X>
			<Y Visible="False" Compass="West">
				<Labels TextAlign="Center"></Labels>
				<GridMajor Visible="True"></GridMajor>
				<GridMinor Visible="False"></GridMinor>
			</Y>
		</Axis>
		<Annotations>
			<wijmo:TextAnnotation Attachment="Relative" Offset="0, 0" Text="Text Annotation" Tooltip="Text Annotation">
				<Point>
					<X DoubleValue="0.5" />
					<Y DoubleValue="0.15" />
				</Point>
				<AnnotationStyle FontSize="20" FontWeight="Bold">
					<Fill Color="#FF66FF" > </Fill >
				</AnnotationStyle>
			</wijmo:TextAnnotation>
			<wijmo:RectAnnotation Attachment="DataIndex" Content="Rect" Height="30" Offset="0, 0" Position="Center, Bottom" Tooltip="Rect" Width="60" PointIndex="4">
				<AnnotationStyle FillOpacity="0.25">
					<Fill Color="#CC99FF" > </Fill >
				</AnnotationStyle>
			</wijmo:RectAnnotation>
			<wijmo:EllipseAnnotation Attachment="DataIndex" Content="Ellipse" Height="35" Offset="0, 0" Tooltip="Ellipse" Width="75" PointIndex="6">
				<AnnotationStyle FillOpacity="0.15">
					<Fill Color="#9966FF" > </Fill >
				</AnnotationStyle>
			</wijmo:EllipseAnnotation>
			<wijmo:CircleAnnotation Content="Circle" Offset="0, 0" Tooltip="Circle">
				<Point>
					<X DoubleValue="200" />
					<Y DoubleValue="100" />
				</Point>
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#666699" > </Fill >
				</AnnotationStyle>
			</wijmo:CircleAnnotation>
			<wijmo:ImageAnnotation Attachment="DataIndex" Href="./../images/c1logo_215x150.png" Offset="0, 0" PointIndex="5" SeriesIndex="0" Tooltip="Image">
			</wijmo:ImageAnnotation>
			<wijmo:PolygonAnnotation Offset="0, 0" Tooltip="Polygon" Content="Polygon">
				<Points>
					<wijmo:PointF X="200" Y="0" />
					<wijmo:PointF X="150" Y="50" />
					<wijmo:PointF X="175" Y="100" />
					<wijmo:PointF X="225" Y="100" />
					<wijmo:PointF X="250" Y="50" />
				</Points>
				<AnnotationStyle FillOpacity="0.25">
					<Fill Color="#9966FF" > </Fill >
				</AnnotationStyle>
			</wijmo:PolygonAnnotation>
			<wijmo:SquareAnnotation Attachment="DataIndex" Content="Square" Offset="0, 0" Position="Right,Center" PointIndex="2" SeriesIndex="0" Tooltip="Square">
				<AnnotationStyle FillOpacity="0.2">
					<Fill Color="#66FFFF" > </Fill >
				</AnnotationStyle>
			</wijmo:SquareAnnotation>
			<wijmo:LineAnnotation Content="Line" End="100, 100" Offset="0, 0" Start="10, 10" Tooltip="Line">
			</wijmo:LineAnnotation>
		</Annotations>
		<SeriesList>
			<wijmo:BubbleChartSeries Label="West">
				<Data>
					<X>
						<Values>
							<wijmo:ChartXData DoubleValue="5" />
							<wijmo:ChartXData DoubleValue="14" />
							<wijmo:ChartXData DoubleValue="20" />
							<wijmo:ChartXData DoubleValue="18" />
							<wijmo:ChartXData DoubleValue="22" />
							<wijmo:ChartXData DoubleValue="17" />
							<wijmo:ChartXData DoubleValue="12" />
						</Values>
					</X>
					<Y>
						<Values>
							<wijmo:ChartYData DoubleValue="5500" />
							<wijmo:ChartYData DoubleValue="12200" />
							<wijmo:ChartYData DoubleValue="60000" />
							<wijmo:ChartYData DoubleValue="24400" />
							<wijmo:ChartYData DoubleValue="32000" />
							<wijmo:ChartYData DoubleValue="12000" />
							<wijmo:ChartYData DoubleValue="22000" />
						</Values>
					</Y>
					<Y1 DoubleValues="15,12,33,10,40,30,20" />
				</Data>
			</wijmo:BubbleChartSeries>
		</SeriesList>
	</wijmo:C1BubbleChartExtender>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>
		This sample demonstrates how to add annotation in the <strong>C1BubbleChartExtender</strong>.
	</p>
	<h3>Test the features</h3>
	<ul>
		<li>Hover over mouse over annotation to see the tooltip.</li>
		<li>Click the legend item to toggle the visibility.</li>
	</ul>
</asp:Content>
