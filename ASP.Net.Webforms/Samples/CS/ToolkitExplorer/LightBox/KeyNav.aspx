﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="KeyNav.aspx.cs" Inherits="ToolkitExplorer.LightBox.KeyNav" %>
<%@ Register Assembly="C1.Web.Wijmo.Extenders.3" Namespace="C1.Web.Wijmo.Extenders.C1LightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<asp:Panel ID="Panel1" runat="server">
		<a href="../Images/Sports/1.jpg" rel="wijlightbox[stock];player=img">
            <img src="../Images/Sports/1_small.jpg"  title="Grass Field" alt="Grass Field with a car in the background" /></a>
        <a href="../Images/Sports/2.jpg" rel="wijlightbox[stock];player=img">
            <img src="../Images/Sports/2_small.jpg"  title="Mountains" alt="Mountains and a blue cloudy sky" /></a>
        <a href="../Images/Sports/3.jpg"  rel="wijlightbox[stock];player=img">
            <img src="../Images/Sports/3_small.jpg" title="Bridge" alt="Bridge with mountains" /></a>
        <a href="../Images/Sports/4.jpg" rel="wijlightbox[stock];player=img">
            <img src="../Images/Sports/4_small.jpg"  title="Building" alt="Building with interesting architecture" /></a>
	</asp:Panel>

	<wijmo:C1LightBoxExtender ID="Panel1_C1LightBoxExtender" runat="server" 
		TargetControlID="Panel1" Player="Img" TextPosition="TitleOverlay" KeyNav="true">
	</wijmo:C1LightBoxExtender>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p>
This sample demonstrates keyboard navigation.
</p>

<p>
The keyboard navigation is enabled by setting the <b>KeyNav</b> property to True.
</p>

<p>
The following keys are supported by default:
</p>
<ul>
<li>LEFT/DOWN - Moves to the previous page.</li>
<li>RIGHT/UP - Moves to the next page.</li>
<li>HOME - Moves to the first page.</li>
<li>END - Moves to the last page.</li>
</ul>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
