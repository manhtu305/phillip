﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.3" namespace="C1.Web.Wijmo.Controls.C1Chart" tagPrefix="wijmo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<script type = "text/javascript">
		function hintContent() {
			//return this.x + '<br/>' + this.y + '<br/>' + this.hintContent;
			return this.x + '\n' + this.hintContent;
		}
	</script>
</head>
<body>
	<form id="form1" runat="server">
	<div>
	<wijmo:C1LineChart ID="C1LineChart1" runat="server" DataSourceID="AccessDataSource1" ShowChartLabels="false" Height="475" Width = "756">
		<Axis>
			<X>
				<Labels>
					<Style FontSize="11pt" Rotation="-45">
						<Fill Color="#7f7f7f"></Fill>
					</Style>
				</Labels>
				<TickMajor Position="Outside">
					<Style Stroke="#7f7f7f" />
				</TickMajor>
			</X>
			<Y Visible="False" Compass="West">
				<Labels TextAlign="Center">
					<Style FontSize="11pt">
						<Fill Color="#7f7f7f"></Fill>
					</Style>
				</Labels>
				<GridMajor Visible="True">
					<Style Stroke="#353539" StrokeDashArray="- " />
				</GridMajor>
				<TickMajor Position="Outside">
					<Style Stroke="#7f7f7f" />
				</TickMajor>
				<TickMinor Position="Outside">
					<Style Stroke="#7f7f7f" />
				</TickMinor>
			</Y>
		</Axis>
		<Header Text="Sales"></Header>
		<SeriesHoverStyles>
			<wijmo:ChartStyle StrokeWidth="8" Opacity="1" />
		</SeriesHoverStyles>
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#00a6dd" StrokeWidth="5" Opacity="0.8" />
		</SeriesStyles>
		<DataBindings>
			<wijmo:C1ChartBinding XField="CategoryName" XFieldType="String" YField="Sales" YFieldType="Number" HintField="CN2" />
		</DataBindings>
	</wijmo:C1LineChart>
	
	<wijmo:C1LineChart ID="C1LineChart2" runat="server" DataSourceID="AccessDataSource1" ShowChartLabels="false" Height="475" Width = "756">
		<Hint>
			<Content Function="hintContent" />
		</Hint>
		<Axis>
			<X>
				<Labels>
					<Style FontSize="11pt" Rotation="-45">
						<Fill Color="#7f7f7f"></Fill>
					</Style>
				</Labels>
				<TickMajor Position="Outside">
					<Style Stroke="#7f7f7f" />
				</TickMajor>
			</X>
			<Y Visible="False" Compass="West">
				<Labels TextAlign="Center">
					<Style FontSize="11pt">
						<Fill Color="#7f7f7f"></Fill>
					</Style>
				</Labels>
				<GridMajor Visible="True">
					<Style Stroke="#353539" StrokeDashArray="- " />
				</GridMajor>
				<TickMajor Position="Outside">
					<Style Stroke="#7f7f7f" />
				</TickMajor>
				<TickMinor Position="Outside">
					<Style Stroke="#7f7f7f" />
				</TickMinor>
			</Y>
		</Axis>
		<Header Text="Sales"></Header>
		<SeriesHoverStyles>
			<wijmo:ChartStyle StrokeWidth="8" Opacity="1" />
		</SeriesHoverStyles>
		<SeriesStyles>
			<wijmo:ChartStyle Stroke="#00a6dd" StrokeWidth="5" Opacity="0.8" />
		</SeriesStyles>
		<DataBindings>
			<wijmo:C1ChartBinding XField="CategoryName" XFieldType="String" YField="Sales" YFieldType="Number" HintField="CN2" />
		</DataBindings>
	</wijmo:C1LineChart>
	
	<asp:AccessDataSource ID="AccessDataSource1" runat="server" 
		DataFile="~/App_Data/C1NWind.mdb" 
		SelectCommand="select CategoryName,CategoryName+'_2' as CN2, sum(ProductSales) as Sales from (SELECT DISTINCTROW Categories.CategoryName as CategoryName, Products.ProductName, Sum([Order Details Extended].ExtendedPrice) AS ProductSales
FROM Categories INNER JOIN (Products INNER JOIN (Orders INNER JOIN [Order Details Extended] ON Orders.OrderID = [Order Details Extended].OrderID) ON Products.ProductID = [Order Details Extended].ProductID) ON Categories.CategoryID = Products.CategoryID
WHERE (((Orders.OrderDate) Between #1/1/95# And #12/31/95#))
GROUP BY Categories.CategoryID, Categories.CategoryName, Products.ProductName
ORDER BY Products.ProductName) group by CategoryName;">
	</asp:AccessDataSource>
	</div>
	</form>
</body>
</html>
