ASP.NET WebForms C1LineChartBindingHint
--------------------------------------------
How to provide support for displaying customized tooltips for data points in Charts.

The sample shows how to provide support for displaying customized tooltips for data points in Charts.

A new property "HintField" has been added to C1ChartBinding. When set the "HintField", there are two cases:

1.Chart.Hint.Content is not set, the hint will show the value of "HintField" automatically.See chart1 in sample.

2.Chart.Hint.Content.Function is set, we can access the "hintContent" in the function.See chart2 in sample.

	<code>
		<script type = "text/javascript">
			function hintContent() {
				return this.x + '<br/>' + this.hintContent;
			}
		</script>
	</code>