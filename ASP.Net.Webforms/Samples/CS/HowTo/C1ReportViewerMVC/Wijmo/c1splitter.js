/*globals jQuery*/

(function ($) {
	"use strict";
	var c1splitterCss = "__c1splitter";
	$.widget("c1.c1splitter", $.wijmo.wijsplitter, {
		options: {
			/// <summary>
			/// A value indicating the width of the wijsplitter.
			/// Default: 400.
			/// Type: Int.
			/// </summary>
			width: 400,
			/// <summary>
			/// A value indicates the height of the wijsplitter.
			/// Default: 250.
			/// Type: Int.
			/// </summary>
			height: 250
		},

		_setOption: function (key, value) {
			if (key === "width" || key === "height") {
				this.refresh(true, false);
			}

			$.wijmo.wijsplitter.prototype._setOption.apply(this, arguments);
		},

		widgetEventPrefix: "c1splitter",

		_create: function () {
			var self = this,
				element = self.element,
				parent = self.element.parents("." + c1splitterCss + ":first");

			//Remove the temp css label which is set at server side.
			element.removeClass(c1splitterCss);
			element.removeClass("ui-helper-hidden-accessible");

			if (self.options.fullSplit) {
				if (parent && parent.length > 0) {
					parent.one("c1splitterload", function (e, data) { //here we should use one other than bind to prevent propagation.
						if (data.element.attr("id") !== element.attr("id")) {
							$.wijmo.wijsplitter.prototype._create.apply(self, arguments);
						}
					});

					return;
				}
			}

			$.wijmo.wijsplitter.prototype._create.apply(self, arguments);
		}
	});
} (jQuery));