﻿ASP.NET WebForms Using ReportViewer in MVC
--------------------------------------------
How to add the ReportViewer to an ASP.NET MVC application.

This sample demonstrates how to use the ReportViewer from the C1.Web.Wijmo.Controls 
assembly in an ASP.NET MVC Application. ReportViewer renders reports using C1Report 
on the server as high-definition graphics rather than HTML. This allows reports on 
the web to be rendered with pixel-perfection. It also means reports will be consistent 
and render identically across all browsers. 

C1ReportViewer is a WebForms Control, so in order to use it in MVC we will simply use 
its client-side JavaScript/CSS for the UI and its report rendering web service. 

In order to use these, we need to add a WebResources UrlHelper that allows embedded 
resources from an assembly to be referenced in Razor Views. Notice in 
Views/Shared/_Layout.cshtml we reference Wijmo along with the additional embedded 
resources from C1.Web.Wijmo.Controls that the ReportViewer requires.

In View/ReportViewerSample/Index.cshtml you can see that we use the ReportViewer in 
JavaScript as a jQuery UI Widget (an HTML element that is enhanced using jQuery). We 
simply need to set the reportServiceUrl, fileName and reportName options and then 
call the generate method. The reportServiceUrl is the url of the handler, in this 
case it is reportService.ashx.