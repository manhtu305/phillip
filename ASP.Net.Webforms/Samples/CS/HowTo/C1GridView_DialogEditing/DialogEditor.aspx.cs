﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Data;

namespace C1GridView_DialogEditing
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        OleDbConnection con = new OleDbConnection("provider=Microsoft.Jet.Oledb.4.0; Data Source=|DataDirectory|\\C1NWind.mdb");

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGrid();
        }

        public void BindGrid()
        {
            OleDbDataAdapter da = new OleDbDataAdapter("SELECT [OrderID], [ShipVia], [Freight], [ShipName], [ShipCountry] FROM [Orders]", con);
            DataTable dt = new DataTable();

            da.Fill(dt);

            C1GridView1.DataSource = dt;
            C1GridView1.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            OleDbCommand cmd = new OleDbCommand("Update Orders Set [ShipVia]=@ShipVia, [Freight]=@Freight, [ShipName]=@ShipName, [ShipCountry]=@ShipCountry,[ShipAddress]=@ShipAddress, [ShipCity]=@ShipCity where [OrderID]=@OrderID", con);
            cmd.Parameters.AddWithValue("@ShipVia", txtShipVia.Text);
            cmd.Parameters.AddWithValue("@Freight", txtFreight.Text);
            cmd.Parameters.AddWithValue("@ShipName", txtShipName.Text);
            cmd.Parameters.AddWithValue("@ShipCountry", txtShipCountry.Text);
            cmd.Parameters.AddWithValue("@ShipAddress", txtShipAddress.Text);
            cmd.Parameters.AddWithValue("@ShipCity", txtShipCity.Text);
            cmd.Parameters.AddWithValue("@OrderID", lblOrderID.Text);
            con.Open();
            cmd.ExecuteScalar();
            con.Close();

            BindGrid();
            C1Dialog1.ShowOnLoad = false;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            C1Dialog1.ShowOnLoad = false;
        }        

        protected void C1GridView1_RowEditing(object sender, C1.Web.Wijmo.Controls.C1GridView.C1GridViewEditEventArgs e)
        {
            C1GridView1.SelectedIndex = -1;
            e.Cancel = true;
            int index = e.NewEditIndex;
            C1GridView1.EditIndex = -1;

            int orderID = int.Parse(C1GridView1.Rows[index].Cells[1].Text);

            OleDbDataAdapter da = new OleDbDataAdapter("Select ShipVia, Freight, ShipName, ShipCountry, ShipAddress, ShipCity, ShipRegion from Orders Where OrderID=" + orderID, con);
            DataTable dt = new DataTable();
            da.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                lblOrderID.Text = orderID.ToString();
                txtShipVia.Text = dt.Rows[0]["ShipVia"].ToString();
                txtFreight.Text = dt.Rows[0]["Freight"].ToString();
                txtShipName.Text = dt.Rows[0]["ShipName"].ToString();
                txtShipCountry.Text = dt.Rows[0]["ShipCountry"].ToString();
                txtShipAddress.Text = dt.Rows[0]["ShipAddress"].ToString();
                txtShipCity.Text = dt.Rows[0]["ShipCity"].ToString();
            }            
            
            C1GridView1.SelectedIndex = index;
            C1Dialog1.ShowOnLoad = true;
        }
    }
}