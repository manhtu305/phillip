﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DialogEditor.aspx.cs" Inherits="C1GridView_DialogEditing.WebForm1" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Dialog"
    TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1GridView"
    TagPrefix="wijmo" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <asp:PlaceHolder runat="server">
        <script type="text/javascript">
            function onBtnCancelClick() {
                $("#C1Dialog1").c1dialog("close");
                return false;
            }
        </script>
    </asp:PlaceHolder>
</head>
<body>
    <form id="form1" runat="server">
    <asp:AccessDataSource ID="AccessDataSource1" runat="server" DataFile="~/App_Data/C1NWind.mdb"
        SelectCommand="SELECT [OrderID], [ShipVia], [Freight], [ShipName], [ShipCountry] FROM [Orders]">
    </asp:AccessDataSource>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <wijmo:C1GridView ID="C1GridView1" runat="server" AutogenerateColumns="False" DataKeyNames="OrderID"
            OnRowEditing="C1GridView1_RowEditing" Height="800px" ScrollMode="Vertical" Width="1100px"
            ClientSelectionMode="None">
            <Columns>
                <wijmo:C1CommandField EditText="Edit" ShowEditButton="true">
                </wijmo:C1CommandField>
                <wijmo:C1BoundField DataField="OrderID" HeaderText="OrderID" ReadOnly="True" SortExpression="OrderID">
                </wijmo:C1BoundField>
                <wijmo:C1BoundField DataField="ShipVia" HeaderText="ShipVia" SortExpression="ShipVia">
                </wijmo:C1BoundField>
                <wijmo:C1BoundField DataField="Freight" HeaderText="Freight" SortExpression="Freight">
                </wijmo:C1BoundField>
                <wijmo:C1BoundField DataField="ShipName" HeaderText="ShipName" SortExpression="ShipName">
                </wijmo:C1BoundField>
                <wijmo:C1BoundField DataField="ShipCountry" HeaderText="ShipCountry" SortExpression="ShipCountry">
                </wijmo:C1BoundField>
            </Columns>
        </wijmo:C1GridView>
        <br />
        <wijmo:C1Dialog ID="C1Dialog1" runat="server" ShowOnLoad="False" Height="320px" Width="400px"
            MaintainVisibilityOnPostback="False" Show="blind" Modal="true">
            <CollapsingAnimation Option="">
            </CollapsingAnimation>
            <ExpandingAnimation Option="">
            </ExpandingAnimation>
            <CaptionButtons>
                <Pin IconClassOn="ui-icon-pin-w" IconClassOff="ui-icon-pin-s"></Pin>
                <Refresh IconClassOn="ui-icon-refresh"></Refresh>
                <Minimize IconClassOn="ui-icon-minus"></Minimize>
                <Maximize IconClassOn="ui-icon-extlink"></Maximize>
                <Close Visible="true" />
            </CaptionButtons>
            <Content>
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text="OrderID"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblOrderID" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblShipVia" runat="server" Text="ShipVia"></asp:Label>&nbsp;
                        </td>
                        <td>
                            <asp:TextBox ID="txtShipVia" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblFreight" runat="server" Text="Freight"></asp:Label>&nbsp;
                        </td>
                        <td>
                            <asp:TextBox ID="txtFreight" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblShipName" runat="server" Text="ShipName"></asp:Label>&nbsp;
                        </td>
                        <td>
                            <asp:TextBox ID="txtShipName" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblShipAddress" runat="server" Text="ShipAddress"></asp:Label>&nbsp;
                        </td>
                        <td>
                            <asp:TextBox ID="txtShipAddress" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblShipCity" runat="server" Text="ShipCity"></asp:Label>&nbsp;
                        </td>
                        <td>
                            <asp:TextBox ID="txtShipCity" runat="server"></asp:TextBox>
                        </td>
                    </tr>
      
                    <tr>
                        <td>
                            <asp:Label ID="lblShipCountry" runat="server" Text="ShipCountry"></asp:Label>&nbsp;
                        </td>
                        <td>
                            <asp:TextBox ID="txtShipCountry" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                        </td>
                        <td>
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" UseSubmitBehavior="false" OnClientClick="return onBtnCancelClick();"  />
                        </td>
                    </tr>
                </table>
            </Content>
        </wijmo:C1Dialog>
    </div>
    </form>
</body>
</html>
