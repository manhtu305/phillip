﻿ASP.NET WebForms C1GridView_DialogEditing
---------------------------------------------
This sample demonstrates how you can use C1Dialog to Edit data displayed in a C1GridView.

It also demonstrates how you can edit additional fields which are not displayed in C1GridView.