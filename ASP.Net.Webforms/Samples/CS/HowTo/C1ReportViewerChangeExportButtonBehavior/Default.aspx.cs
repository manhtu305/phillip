﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Drawing.Text;
using System.Data;

using C1.Web.Wijmo.Controls.C1ReportViewer;
using System.Web.UI.HtmlControls;

public partial class _Default : System.Web.UI.Page 
{

    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
    }

	protected void Page_Load(object sender, EventArgs e)
	{
		C1ReportViewerToolbar toolBar = C1ReportViewer1.ToolBar;
		((HtmlGenericControl)toolBar.Controls[1]).Style["display"] = "none"; // hide original save(export) button
		// Add own export button
		HtmlGenericControl saveButton = new HtmlGenericControl("button");
		saveButton.Attributes["title"] = "Custom export";
		saveButton.Attributes["class"] = "custom-export";
		saveButton.InnerHtml = "Custom Save";
		toolBar.Controls.AddAt(1, saveButton);
	}

 
}
