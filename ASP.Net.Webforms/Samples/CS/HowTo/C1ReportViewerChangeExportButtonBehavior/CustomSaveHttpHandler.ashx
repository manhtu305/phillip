<%@ WebHandler Language="C#" Class="C1ReportHttpHandler" %>
using System;
using System.Web;
using System.Collections;
using System.IO;
using C1.Web.Wijmo.Controls.C1ReportViewer;
using C1.Web.Wijmo.Controls.C1ReportViewer.ReportService;

public class C1ReportHttpHandler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {        
        IC1WebReportService reportService = C1WebReportServiceHelper.MakeHelper((string)context.Request.Params["documentKey"]);
        // Export report to a file:
		string url = reportService.ExportToFile((string)context.Request.Params["documentKey"], context.Request.Params["exportFormat"]);

		// Convert url to a file path:
		string pathSource = url.Substring(url.IndexOf("tempReports"));
		pathSource = context.Server.MapPath(pathSource);
		        
        using (FileStream fsSource = new FileStream(pathSource, FileMode.Open, FileAccess.Read))
        {
            // Read the source file into a byte array.
            byte[] bytes = new byte[fsSource.Length];
            int numBytesToRead = (int)fsSource.Length;
            int numBytesRead = 0;
            while (numBytesToRead > 0)
            {
                // Read may return anything from 0 to numBytesToRead.
                int n = fsSource.Read(bytes, numBytesRead, numBytesToRead);
                // Break when the end of the file is reached.
                if (n == 0)
                    break;
                numBytesRead += n;
                numBytesToRead -= n;
            }
            // Clear all content output from the buffer stream 
            context.Response.Clear();
            // Add a HTTP header to the output stream that specifies the default filename 
            // for the browser's download dialog 
            context.Response.AddHeader("Content-Disposition", "attachment; filename=" + (string)context.Request.Params["documentKey"] + "." + context.Request.Params["exportFormatExt"]);
            // Add a HTTP header to the output stream that contains the 
            // content length(File Size). This lets the browser know how much data is being transfered 
            context.Response.AddHeader("Content-Length", bytes.Length.ToString());
            // Set the HTTP MIME type of the output stream 
            context.Response.ContentType = "application/octet-stream";
            // Write the data out to the client. 
            context.Response.BinaryWrite(bytes);
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}