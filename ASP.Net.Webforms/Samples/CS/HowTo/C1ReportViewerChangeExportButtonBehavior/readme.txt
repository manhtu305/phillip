ASP.NET WebForms C1ReportViewerChangeExportButtonBehavior
--------------------------------------------
Shows how to customize the behavior of the save button in the C1ReportViewer control.

This sample illustrates how to add a custom export button that allows a user to download
an exported report directly without opening a new browser window.

The C1ReportViewer toolbar is modified from the code behind:

<code>
	protected void Page_Load(object sender, EventArgs e)
	{
		C1ReportViewerToolbar toolBar = C1ReportViewer1.ToolBar;
		((HtmlGenericControl)toolBar.Controls[1]).Style["display"] = "none"; // hide original save(export) button
		// Add own export button
		HtmlGenericControl saveButton = new HtmlGenericControl("button");
		saveButton.Attributes["title"] = "Custom export";
		saveButton.Attributes["class"] = "custom-export";
		saveButton.InnerHtml = "Custom Save";
		toolBar.Controls.AddAt(1, saveButton);
	}
</code>

When the user clicks the button, the custom HTTP handler "CustomSaveHttpHandler.ashx" is called and 
the exported file is written to the IFrame element.

<code>
	document.getElementById("customsaveframe").src = "CustomSavetHttpHandler.ashx?command=SaveToXML&documentKey=" + docStatus.documentKey + "&exportFormat=Open%20XML%20Excel&exportFormatExt=xlsx";
</code>