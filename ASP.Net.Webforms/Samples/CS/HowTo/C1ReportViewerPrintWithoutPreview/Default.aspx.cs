﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.C1ReportViewer;

namespace C1ReportViewerPrintWithoutPreview
{
	public partial class Default : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{


			C1ReportViewerToolbar toolBar = C1ReportViewer1.ToolBar;
			((HtmlGenericControl)toolBar.Controls[0]).Style["display"] = "none"; // hide original print button		
			// Add custom print button
			HtmlGenericControl printButton = new HtmlGenericControl("button");
			printButton.Attributes["title"] = "Custom Print";
			printButton.Attributes["class"] = "custom-print";
			printButton.InnerHtml = "Custom Print";
			toolBar.Controls.AddAt(0, printButton);
		}
	}
}