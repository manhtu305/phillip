﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeFile="Overview.aspx.cs" Inherits="LineChart_Overview" %>

<%@ Register Assembly="C1.Web.Wijmo.Complete.Juice.4" Namespace="C1.Web.Wijmo.Juice.WijChart" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
	<script type = "text/javascript">
		function hintContent() {
			return this.data.lineSeries.label + '\n' +
				this.x + '\n' + this.y + '';
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
	<asp:Panel ID="linechart" runat="server" Height="475" Width = "756">
	</asp:Panel>
	<wijmo:WijLineChart runat = "server" ID="LineChartExtender1" 
		TargetControlID="linechart" ShowChartLabels="False" >
		        <Header Text="Users Online">
        </Header>
        <Footer Compass="South" Visible="False">
        </Footer>
        <Legend Visible="false">
        </Legend>
        <Hint OffsetY="-10">
            <Content Function="hintContent" />
        </Hint>
	</wijmo:WijLineChart>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" Runat="Server">
	<p>The WijLineChart(wijlinechart at client side) allows you to create customized line charts. The samples in this section highlight some of the unique features of WijLineChart, and they will help you get started with WijLineChart.</p>
	<p>The source in this sample will show you how to add a header to the chart; how to format the axes; how to add chart labels; and how to populate the chart with data.</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" Runat="Server">
</asp:Content>