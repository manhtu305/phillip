﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="ColumnMoving.aspx.cs" Inherits="ControlExplorer.Grid.ColumnMoving" %>
<%@ Register Assembly="C1.Web.Wijmo.Complete.Juice.4" Namespace="C1.Web.Wijmo.Juice.WijGrid"
    TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:GridView runat="server" ID="demo" DataSourceID="AccessDataSource1" ShowHeader="False" />

    <asp:AccessDataSource ID="AccessDataSource1" runat="server" 
        DataFile="~/App_Data/C1NWind.mdb" 
        SelectCommand="SELECT TOP 5 [CustomerID], [CompanyName], [ContactName], [ContactTitle], [Address], [City], [Country], [PostalCode], [Phone], [Fax] FROM [Customers]">
    </asp:AccessDataSource>
  
    <wijmo:WijGrid runat="server" ID="GridExtender1" TargetControlID="demo" AllowColMoving="true">
        <Columns>
            <wijmo:WijField HeaderText="ID" />
            <wijmo:WijField HeaderText="Company" />
            <wijmo:WijBand HeaderText="Additional information">
                <Columns>
                    <wijmo:WijBand HeaderText="Contact">
                        <Columns>
                            <wijmo:WijField HeaderText="Name" />
                            <wijmo:WijField HeaderText="Title" />
                        </Columns>
                    </wijmo:WijBand>
                    <wijmo:WijBand  HeaderText="Regional information">
                        <Columns>
                            <wijmo:WijBand  HeaderText="Address information">
                                <Columns>
                                    <wijmo:WijField HeaderText="Address" />
                                    <wijmo:WijField HeaderText="City" />
                                    <wijmo:WijField HeaderText="Country" />
                                    <wijmo:WijField HeaderText="PostalCode" />
                                </Columns>
                            </wijmo:WijBand>
                            <wijmo:WijBand  HeaderText="Communication">
                                <Columns>
                                    <wijmo:WijField HeaderText="Phone" />
                                    <wijmo:WijField HeaderText="Fax" />
                                </Columns>
                            </wijmo:WijBand>
                        </Columns>
                    </wijmo:WijBand>
                </Columns>
            </wijmo:WijBand>
        </Columns>
    </wijmo:WijGrid>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        This sample shows how to use column moving feature.
    </p>
    <p>
        Column moving is done by dragging a source column header on a target column header. Column can be inserted
        either to the right or to the left of the target column, depending upon closer to which border dragged column
        is dropped. To add column to a band its header should be dropped to the center of band header.
    </p>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
