﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Sorting.aspx.cs" Inherits="ControlExplorer.Grid.Sorting" %>

<%@ Register Assembly="C1.Web.Wijmo.Complete.Juice.4" Namespace="C1.Web.Wijmo.Juice.WijGrid"
    TagPrefix="wijmo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:GridView runat="server" ID="demo" DataSourceID="XmlDataSource1" EnableModelValidation="True" ShowHeader="false">
    </asp:GridView>
  
    <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="~/App_Data/Players.xml" XPath="Players/Player[@Nationality='Canada' ]" />
  
    <wijmo:WijGrid runat="server" ID="GridExtender1" TargetControlID="demo" AllowSorting="true">
        <Columns>
            <wijmo:WijField HeaderText="Number" DataType="Number" DataFormatString="n0" SortDirection="Ascending" /> 
            <wijmo:WijField HeaderText="Nationality" />
            <wijmo:WijField HeaderText="Player" />
            <wijmo:WijField HeaderText="Position" />
            <wijmo:WijField HeaderText="Handedness" />
            <wijmo:WijField HeaderText="Age" />
            <wijmo:WijField HeaderText="Acquired" />
            <wijmo:WijField HeaderText="Birthplace" />
        </Columns>
    </wijmo:WijGrid>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
        This sample shows that setting the <b>allowSorting</b> option to true allows you to sort grid
        columns by clicking the column header.
    </p>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
