﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Filtering.aspx.cs" Inherits="ControlExplorer.Grid.Filtering" %>
<%@ Register Assembly="C1.Web.Wijmo.Complete.Juice.4" Namespace="C1.Web.Wijmo.Juice.WijGrid"
    TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<asp:Table runat="server" ID="demo"></asp:Table>
	<wijmo:WijGrid runat="server" ID="GridExtender1" TargetControlID="demo" ShowFilter="true">
		<Columns>
			<wijmo:WijField DataType="Number" DataFormatString="n0" />
			<wijmo:WijField DataType="Currency" ValueRequired="true" FilterOperator="GreaterOrEqual">
				<FilterValue ValueString="30" />
			</wijmo:WijField>
			<wijmo:WijField DataType="Number" DataFormatString="n0" FilterOperator="GreaterOrEqual">
				<FilterValue ValueString="5" />
			</wijmo:WijField>
			<wijmo:WijField DataType="Number" DataFormatString="p0" />
		</Columns>
	</wijmo:WijGrid>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
		This sample shows how you can filter data by enabling the <b>showFilter</b> option.
	</p>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>