﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="IFrame.aspx.cs" Inherits="JuiceCompleteExplorer.LightBox.IFrame" %>
<%@ Register Assembly="C1.Web.Wijmo.Complete.Juice.4" Namespace="C1.Web.Wijmo.Juice.WijLightBox" TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<asp:Panel ID="Panel1" runat="server">
		<a href="http://www.yahoo.com/" rel="wijlightbox[stock]" width="960" height="600">
			<img src="images/small/yahoo.png" alt="Yahoo"/></a> 
		<a href="http://www.componentone.com" rel="wijlightbox[stock]">
			<img src="images/small/componentone.png" alt="ComponentOne"</a> 
		<a href="http://www.microsoft.com" rel="wijlightbox[stock]">
			<img src="images/small/microsoft.png" alt="Microsoft" /></a> 
	</asp:Panel>

	<wijmo:WijLightBox ID="Panel1_C1LightBoxExtender" runat="server" 
		TargetControlID="Panel1" Player="Iframe" MaxWidth="960" MaxHeight="600">
	</wijmo:WijLightBox>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">

<p>
C1LightBox supports displaying the content of URL in an iframe.
</p>

<p>
This is enabled by setting the <b>Player</b> property to Iframe.
</p>

<p>
In this sample, LightBox hosts the Yahoo, ComponentOne and Microsoft websites.
</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
