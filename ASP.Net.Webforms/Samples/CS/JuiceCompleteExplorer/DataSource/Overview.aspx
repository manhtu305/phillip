﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeFile="Overview.aspx.cs" Inherits="DataSource_Overview" %>

<%@ Register assembly="C1.Web.Wijmo.Complete.Juice.4" namespace="C1.Web.Wijmo.Juice.WijDataSource" tagPrefix="wijmo" %>

<%@ Register assembly="C1.Web.Wijmo.Open.Juice.4" namespace="C1.Web.Wijmo.Juice.WijList" tagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <wijmo:WijDataSource ID="DataSource1" runat="server" OnClientLoading="datasource_loading" OnClientLoaded="datasource_loaded">
        <Reader>
            <Fields>
                <wijmo:ReaderField Name="label">
                    <Mapping Function="reader_mapping" />
                </wijmo:ReaderField>
                <wijmo:ReaderField Name="value">
                    <Mapping Name="name" />
                </wijmo:ReaderField>
                <wijmo:ReaderField Name="selected">
                    <DefaultValue ValueString="false" />
                </wijmo:ReaderField>
            </Fields>
        </Reader>
        <Proxy>
            <Options>
                <wijmo:ProxyField Name="url">
                    <Value NeedQuotes="True" ValueString="http://ws.geonames.org/searchJSON" />
                </wijmo:ProxyField>
                <wijmo:ProxyField Name="dataType">
                    <Value NeedQuotes="True" ValueString="jsonp" />
                </wijmo:ProxyField>
                <wijmo:ProxyField Name="data">
                    <Value ValueString="ajaxData" />
                </wijmo:ProxyField>
                <wijmo:ProxyField Name="key">
                    <Value NeedQuotes="True" ValueString="geonames" />
                </wijmo:ProxyField>
            </Options>
        </Proxy>
    </wijmo:WijDataSource>
	
    <wijmo:WijList runat="server" TargetControlID="listp" ID="list"></wijmo:WijList>
	<script id="scriptInit" type="text/javascript">
		var list, input;
		var ajaxData = {
			featureClass: "P",
			style: "full",
			maxRows: 12,
			name_startsWith: 'ab'
		};
		function reader_mapping(item) {
			return item.name + (item.adminName1 ? ", " + item.adminName1 : "") + ", " + item.countryName
		}
		function list_selected(event, ui) {
			var item = ui.item;
			input.val(item.value);
			list.wijlist('deactivate');
		}
		function datasource_loading() {
			input.addClass('wijmo-wijcombobox-loading');
		}
		function datasource_loaded(data) {
			list.wijlist('setItems', data.items);
			list.wijlist('renderList');
			list.wijlist('refreshSuperPanel');
			input.removeClass('wijmo-wijcombobox-loading');
		}
		$(document).ready(function () {
			// put all your jQuery goodness in here.
			list = $("#<%=listp.ClientID%>");
			input = $('#testinput');
			input.bind("keydown.wijcombobox", function (event) {
				var keyCode = $.ui.keyCode;
				switch (event.keyCode) {
					case keyCode.UP:
						list.wijlist('previous', event);
						// prevent moving cursor to beginning of text field in some browsers
						event.preventDefault();
						break;
					case keyCode.DOWN:
						if (!list.is(':visible')) {
							list.show();
							return;
						}
						list.wijlist('next', event);
						// prevent moving cursor to end of text field in some browsers
						event.preventDefault();
						break;
					case keyCode.ENTER:
						event.preventDefault();
						list.wijlist('select', event);
						break;
					case keyCode.PAGE_UP:
						list.wijlist('previousPage');
						break;
					case keyCode.PAGE_DOWN:
						list.wijlist('nextPage');
						break;
					default:
						break;
				}
			});

			DataSource1.load();
		});

		function loadRemoteData() {
			var datasource = DataSource1;
			datasource.proxy.options.data.name_startsWith = $('#testinput').val();
			datasource.load();
		}
    </script>
	<script type="text/javascript">
		amplify.subscribe('<%=listp.ClientID%>.wijlist.selected', list_selected);
	</script>
    <div class="ui-widget">
        <input style="width: 400px" id="testinput" type="text" class="ui-widget-content ui-corner-all" /><input type="button" onclick="loadRemoteData()" id="loadremote" value="Load Remote Data" />
		<asp:Panel runat="server" ID="listp" Width="400" Height=""></asp:Panel>
        <%--<div id="list" style="height: 300px; width: 400px;">
        </div>--%>
    </div>
    
</asp:Content>

