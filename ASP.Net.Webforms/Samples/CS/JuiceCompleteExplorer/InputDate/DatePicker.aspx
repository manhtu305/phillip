﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeFile="DatePicker.aspx.cs" Inherits="InputDate_DatePicker" %>

<%@ Register Assembly="C1.Web.Wijmo.Complete.Juice.4" Namespace="C1.Web.Wijmo.Juice.WijInput"
    TagPrefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

	<asp:TextBox ID="TextBox1" runat="server" Width="220px"></asp:TextBox>
	<wijmo:WijInputDate ID="InputDateExtender1" runat="server" TargetControlID="TextBox1" ShowTrigger="true"/>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
<p>
WijInputDate supports selecting date from a dropdown calendar.
</p>

<p>
You can set the <b>ShowTrigger</b> property to <b>true</b> to enabel the dropdown calendar.
</p>

<p>
Setting the <b>ButtonAlign</b> property would change the alignment of trigger button.
</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>


