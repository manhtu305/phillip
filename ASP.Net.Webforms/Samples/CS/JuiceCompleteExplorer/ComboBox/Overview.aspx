﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
	CodeFile="Overview.aspx.cs" Inherits="ComboBox_Overview" %>

<%@ Register Assembly="C1.Web.Wijmo.Complete.Juice.4" Namespace="C1.Web.Wijmo.Juice.WijComboBox"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<div>
		<label>
			TextBox</label>
		<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
		<wijmo:WijComboBox ID="ComboBoxExtender2" runat="server" TargetControlID="TextBox1">
			<Data>
				<Items>
					<wijmo:WijComboBoxItem Label="c++" Value="c++"  />
					<wijmo:WijComboBoxItem Label="java" Value="java" />
					<wijmo:WijComboBoxItem Label="php" Value="php" />
					<wijmo:WijComboBoxItem Label="coldfusion" Value="coldfusion" />
					<wijmo:WijComboBoxItem Label="javascript" Value="javascript" />
					<wijmo:WijComboBoxItem Label="asp" Value="asp" />
					<wijmo:WijComboBoxItem Label="ruby" Value="ruby" />
					<wijmo:WijComboBoxItem Label="python" Value="python" />
					<wijmo:WijComboBoxItem Label="c" Value="c" />
					<wijmo:WijComboBoxItem Label="scala" Value="scala" />
					<wijmo:WijComboBoxItem Label="groovy" Value="groovy" />
					<wijmo:WijComboBoxItem Label="haskell" Value="haskell" />
					<wijmo:WijComboBoxItem Label="perl" Value="perl" />
				</Items>
			</Data>
			<HidingAnimation Duration="4000" Easing="EaseInOutQuad">
			</HidingAnimation>
		</wijmo:WijComboBox>
	</div>
	<div>
		<label>
			DropDownList</label>
		<asp:DropDownList runat="server" ID="DropDownList1">
			<asp:ListItem Value="c++" Text="c++"></asp:ListItem>
			<asp:ListItem Value="java" Text="java"></asp:ListItem>
			<asp:ListItem Value="php" Text="php"></asp:ListItem>
			<asp:ListItem Value="coldfusion" Text="coldfusion"></asp:ListItem>
			<asp:ListItem Value="javascript" Text="javascript"></asp:ListItem>
			<asp:ListItem Value="asp" Text="asp"></asp:ListItem>
			<asp:ListItem Value="ruby" Text="ruby"></asp:ListItem>
			<asp:ListItem Value="python" Text="python"></asp:ListItem>
			<asp:ListItem Value="c" Text="c"></asp:ListItem>
			<asp:ListItem Value="scala" Text="scala"></asp:ListItem>
			<asp:ListItem Value="groovy" Text="groovy"></asp:ListItem>
			<asp:ListItem Value="haskell" Text="haskell"></asp:ListItem>
			<asp:ListItem Value="perl" Text="perl"></asp:ListItem>
		</asp:DropDownList>
		<wijmo:WijComboBox ID="ComboBoxExtender1" runat="server" 
			TargetControlID="DropDownList1" >
			<HidingAnimation Duration="4000" Easing="EaseInCubic">
			</HidingAnimation>
		</wijmo:WijComboBox>
	</div>
	<script id="scriptInit" type="text/javascript">
		$(document).ready(function () {
			$('#show').click(function () {
				var isCheck = $('#show').attr("checked");
				if (isCheck) {
					$("#<%=DropDownList1.ClientID %>").show();
				}
				else {
					$("#<%=DropDownList1.ClientID %>").hide();
				}
			})
		});
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>The C1ComboBoxExtender display an editable text box/drop-down list on ASPX page.</p>

	<p>User can control the appearance of C1ComboBoxExtender and items of dropdown list.</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<input id="show" type="checkbox" /><label for="show">Show Original Select Element</label>
</asp:Content>
