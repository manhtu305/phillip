﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
	CodeFile="Animation.aspx.cs" Inherits="ComboBox_Animation" %>

<%@ Register Assembly="C1.Web.Wijmo.Complete.Juice.4" Namespace="C1.Web.Wijmo.Juice.WijComboBox"
	TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
	<script id="scriptInit" type="text/javascript">
		function changeProperties() {
			var showingSpeed, hidingSpeed, expandingSpeed, collapsingSpeed;
			showingSpeed = parseNumberFromString($('#showingSpeed').val());
			hidingSpeed = parseNumberFromString($('#hidingSpeed').val());
			var showEffect = $('#showingEffectTypes').get(0).value;
			var hideEffect = $('#hidingEffectTypes').get(0).value;
			showEffect = showEffect == "none" ? false : showEffect;
			hideEffect = hideEffect == "none" ? false : hideEffect;
			var showingAnimation = {
				effect: showEffect,
				options: null,
				speed: showingSpeed
			};
			var hidingAnimation = {
				effect: hideEffect,
				options: null,
				speed: hidingSpeed
			};
			var wij = $('#<%=TextBox1.ClientID %>');
			wij.wijcombobox('option', 'showingAnimation', showingAnimation);
			wij.wijcombobox('option', 'hidingAnimation', hidingAnimation);
		}
		function parseNumberFromString(st) {
			var i = 1000;
			try {
				i = parseInt(st);
			}
			catch (e) {
				alert(e);
			}
			return i;
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
		<style type ="text/css">
			.ui-effects-explode
			{
				z-index:99999;
			}
		</style>
	<div>
		<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
		<wijmo:WijComboBox ID="ComboBoxExtender2" runat="server" TargetControlID="TextBox1">
			<ShowingAnimation Duration="1000">
				<Animated Effect="Scale" />
			</ShowingAnimation>
			<HidingAnimation Duration="1000">
			<Animated Effect="explode" />
				</HidingAnimation>
			<Data>
				<Items>
					<wijmo:WijComboBoxItem Label="c++" Value="c++" />
					<wijmo:WijComboBoxItem Label="java" Value="java" />
					<wijmo:WijComboBoxItem Label="php" Value="php" />
					<wijmo:WijComboBoxItem Label="coldfusion" Value="coldfusion" />
					<wijmo:WijComboBoxItem Label="javascript" Value="javascript" />
					<wijmo:WijComboBoxItem Label="asp" Value="asp" />
					<wijmo:WijComboBoxItem Label="ruby" Value="ruby" />
					<wijmo:WijComboBoxItem Label="python" Value="python" />
					<wijmo:WijComboBoxItem Label="c" Value="c" />
					<wijmo:WijComboBoxItem Label="scala" Value="scala" />
					<wijmo:WijComboBoxItem Label="groovy" Value="groovy" />
					<wijmo:WijComboBoxItem Label="haskell" Value="haskell" />
					<wijmo:WijComboBoxItem Label="perl" Value="perl" />
				</Items>
			</Data>
		</wijmo:WijComboBox>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
		The C1ComboBoxExtender supports showing and hiding animation setting at client.
	</p>
	<p>
		Showing and hiding animation is allowed if the following properties are set to corresponding values:
	</p>
	<ul>
	<li><strong>ShowingAnimation </strong>- takes effect when the drop-down list is 
		visible.</li>
	<li><strong>HidingAnimation </strong>- takes effect when the drop-down list is 
		hidden.</li>
	</ul>
	<p> 
		Animation options include speed and effect.
	</p>
	<p>
		End-users can observe the animation effect once the showingAnimation and hidingAnimation are set.
	</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
	<div class="demo-options">
		<!-- Begin options markup -->
		<div class="option-row">
			<label for="showingSpeed">
				Showing Speed
			</label>
			<input id="showingSpeed" type="text" value="1000" onblur="changeProperties()" />
		</div>
		<div class="option-row">
			<label for="showingEffectTypes">
				Showing Effect
			</label>
			<select id="showingEffectTypes" name="effects" onchange="changeProperties()">
				<option value="none">none</option>
				<option value="blind">Blind</option>
				<option value="bounce">Bounce</option>
				<option value="clip">Clip</option>
				<option value="drop">Drop</option>
				<option value="explode">Explode</option>
				<option value="fade">Fade</option>
				<option value="fold">Fold</option>
				<option value="highlight">Highlight</option>
				<option value="puff">Puff</option>
				<option value="pulsate">Pulsate</option>
				<option value="scale">Scale</option>
				<option value="shake">Shake</option>
				<option value="size">Size</option>
				<option value="slide">Slide</option>
			</select>
		</div>
		<div class="option-row">
			<label for="hidingSpeed">
				Hiding Speed
			</label>
			<input id="hidingSpeed" type="text" value="1000" onblur="changeProperties()" />
		</div>
		<div class="option-row">
			<label for="hidingEffectTypes">
				Hiding Effect
			</label>
			<select id="hidingEffectTypes" name="effects" onchange="changeProperties()">
				<option value="none">none</option>
				<option value="blind">Blind</option>
				<option value="bounce">Bounce</option>
				<option value="clip">Clip</option>
				<option value="drop">Drop</option>
				<option value="explode">Explode</option>
				<option value="fade">Fade</option>
				<option value="fold">Fold</option>
				<option value="highlight">Highlight</option>
				<option value="puff">Puff</option>
				<option value="pulsate">Pulsate</option>
				<option value="scale">Scale</option>
				<option value="shake">Shake</option>
				<option value="size">Size</option>
				<option value="slide">Slide</option>
			</select>
		</div>
		<!-- End options markup -->
	</div>
</asp:Content>
