﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
    CodeFile="DynamicData.aspx.cs" Inherits="ComboBox_DynamicData" %>

<%@ Register Assembly="C1.Web.Wijmo.Complete.Juice.4" Namespace="C1.Web.Wijmo.Juice.WijComboBox"
    TagPrefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <script id="scriptInit" type="text/javascript">
        function setData() {
            $('#<%=TextBox1.ClientID %>').wijcombobox("option", "data", [
                {
                    label: 'delphi',
                    value: 'delphi'
                },
                {
                    label: 'visual studio',
                    value: 'visual studio'
                },
                {
                    label: 'flash',
                    value: 'flash'
                }
            ]);
        }
            
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <wijmo:WijComboBox ID="ComboBoxExtender2" runat="server" TargetControlID="TextBox1">
            <Data>
                <Items>
                    <wijmo:WijComboBoxItem Label="c++" Value="c++" />
                    <wijmo:WijComboBoxItem Label="java" Value="java" />
                    <wijmo:WijComboBoxItem Label="php" Value="php" />
                    <wijmo:WijComboBoxItem Label="coldfusion" Value="coldfusion" />
                    <wijmo:WijComboBoxItem Label="javascript" Value="javascript" />
                    <wijmo:WijComboBoxItem Label="asp" Value="asp" />
                    <wijmo:WijComboBoxItem Label="ruby" Value="ruby" />
                    <wijmo:WijComboBoxItem Label="python" Value="python" />
                    <wijmo:WijComboBoxItem Label="c" Value="c" />
                    <wijmo:WijComboBoxItem Label="scala" Value="scala" />
                    <wijmo:WijComboBoxItem Label="groovy" Value="groovy" />
                    <wijmo:WijComboBoxItem Label="haskell" Value="haskell" />
                    <wijmo:WijComboBoxItem Label="perl" Value="perl" />
                </Items>
            </Data>
        </wijmo:WijComboBox>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
    <p>
	The C1ComboBoxExtender supports adding dynamical data to the wijcombobox at client.</p>

	<p>DynamicData setting is allowed if the following options are set to corresponding values:</p>
	<ul>
	<li>data</li>
	</ul>
	<p>for example: [{label:'delphi', value: 'delphi'}]</p>

	<p>Once the data is set dynamically at client, the content of dropdown list will 
	be replaced by setting data.
	</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
    <div class="demo-options">
        <input type="button" value="Set Dynamic Data " onclick="setData()" />
    </div>
</asp:Content>
