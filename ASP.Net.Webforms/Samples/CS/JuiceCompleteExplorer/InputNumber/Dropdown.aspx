﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeFile="Dropdown.aspx.cs" Inherits="InputNumber_Dropdown" %>
<%@ Register Assembly="C1.Web.Wijmo.Complete.Juice.4" Namespace="C1.Web.Wijmo.Juice.WijInput"
    TagPrefix="wijmo" %>


<%@ Register assembly="C1.Web.Wijmo.Complete.Juice.4" namespace="C1.Web.Wijmo.Juice.WijComboBox" tagprefix="ComboBox" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    
    <wijmo:WijInputNumber ID="InputNumberExtender1" runat="server" TargetControlID="TextBox1"   
        ShowTrigger="true" NumberType="Currency" >
        <comboitems>
            <ComboBox:WijComboBoxItem Label="100,12$" Value="100.12" />
            <ComboBox:WijComboBoxItem Label="1200$" Value="1200" />
            <ComboBox:WijComboBoxItem Label="2000$" Value="2000" />
            <ComboBox:WijComboBoxItem Label="5200$" Value="5200" />
        </comboitems>
    </wijmo:WijInputNumber>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
<p>
Currency input supports selecting predefined values from a dropdown list.
</p>

<p>
You need to set the <b>ShowTriggers</b> property to true, as well as specifing the list items to the <b>ComboItems</b> property.
</p>

<p>
Setting the <b>ButtonAlign</b> property would change the alignment of trigger button.
</p>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>

