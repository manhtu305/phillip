﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.Master" AutoEventWireup="true" CodeBehind="Events.aspx.cs" Inherits="JuiceCompleteExplorer.Rating.Events" %>
<%@ Register assembly="C1.Web.Wijmo.Complete.Juice.4" namespace="C1.Web.Wijmo.Juice.WijRating" tagprefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="server">
	<script type="text/javascript">
		function starHover(e, args) {
			$("#hover").html(args.value);
		}
		function starRated(e, args) {
			$("#rated").html(args.value);
		}
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<asp:Panel runat="server" ID="ratingDiv"></asp:Panel>
		<wijmo:WijRating ID="ratingDiv_C1RatingExtender" runat="server" TargetControlID="ratingDiv" Split="2" Value="3" Count="5">
		</wijmo:WijRating>
	</div>
		<script type="text/javascript">
	  	amplify.subscribe('<%=ratingDiv.ClientID%>.wijrating.hover', starHover);
		amplify.subscribe('<%=ratingDiv.ClientID%>.wijrating.rated', starRated);
	</script>
&nbsp;<div>hover value is:<label id="hover"></label></div>
	<div>rated value is:<label id="rated"></label></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ControlOptions" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Description" runat="server">
	<p>This sample shows how to use the events of your rating.</p>
</asp:Content>
