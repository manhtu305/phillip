﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ControlExplorer.Dialog
{
	public partial class Animation : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				Dialog1.Show = showEffectTypes.SelectedValue.Trim();
				Dialog1.Hide = hideEffectTypes.SelectedValue.Trim();
				Dialog1.ExpandingAnimation.Animated.Effect = expandEffectTypes.SelectedValue.Trim();
				Dialog1.CollapsingAnimation.Animated.Effect = collapseEffectTypes.SelectedValue.Trim();
			}
		}
	}
}