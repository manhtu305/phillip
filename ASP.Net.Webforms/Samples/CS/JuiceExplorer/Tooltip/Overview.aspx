﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeFile="Overview.aspx.cs" Inherits="Tooltip_Overview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" Runat="Server">
    <style type="text/css">
        #targetContainer
        {
            position: relative;
        }
        #targetContainer img
        {
            margin: 0;
            padding: 0;
            position: absolute;
            border: solid 3px #333;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<div id="targetContainer">
    <asp:Image runat="server" ID="Image1" ImageUrl="http://placekitten.com/g/257/109" style="left: 0; top: 0;" title="KITTY HAS REACHED CRITICAL MASS" />
    <asp:Image runat="server" ID="Image2" ImageUrl="http://placekitten.com/g/257/150" style="left: 0; top: 109px;" title="You eated my cookie?" />
    <asp:Image runat="server" ID="Image3" ImageUrl="http://placekitten.com/482/180" style="left: 0; top: 259px;" title="O Hay Guys" />
    <asp:Image runat="server" ID="Image4" ImageUrl="http://placekitten.com/g/225/256" style="left: 257px; top: 0;" title="You can't see me, I'm invisible"  />
    <asp:Image runat="server" ID="Image5" ImageUrl="http://placekitten.com/111/143" style="left: 482px; top: 0px;" title="It's Caturday?" />
    <asp:Image runat="server" ID="Image6" ImageUrl="http://placekitten.com/g/111/296" style="left: 482px; top: 143px;" title="placeakitten.com" />
    <asp:Image runat="server" ID="Image7" ImageUrl="http://placekitten.com/g/151/200" style="left: 593px; top: 0;" title="I'm on ur internets" />
    <asp:Image runat="server" ID="Image8" ImageUrl="http://placekitten.com/152/239" style="left: 593px; top: 200px;" title="I big scary monster" />
               <%-- <img src="http://placekitten.com/g/257/109" style="left: 0; top: 0;" title="KITTY HAS REACHED CRITICAL MASS" alt="" />
                <img src="http://placekitten.com/g/257/150" style="left: 0; top: 109px;" title="You eated my cookie?" alt=""  />
                <img src="http://placekitten.com/482/180" style="left: 0; top: 259px;" title="O Hay Guys" alt=""  />
                <img src="http://placekitten.com/g/225/256" style="left: 257px; top: 0;" title="You can't see me, I'm invisible" alt=""  />
                <img src="http://placekitten.com/111/143" style="left: 482px; top: 0px;" title="It's Caturday?" alt=""  />
                <img src="http://placekitten.com/g/111/296" style="left: 482px; top: 143px;" title="placeakitten.com" alt=""  />
                <img src="http://placekitten.com/g/151/200" style="left: 593px; top: 0;" title="I'm on ur internets" alt="" />
                <img src="http://placekitten.com/152/239" style="left: 593px; top: 200px;" title="I big scary monster" alt=""  />--%>
                
                <wijmo:WijTooltip ID="Tooltip1" runat="server" TargetControlID="Image1">
                </wijmo:WijTooltip>
                <wijmo:WijTooltip ID="WijTooltip1" runat="server" TargetControlID="Image2">
                </wijmo:WijTooltip>
                <wijmo:WijTooltip ID="WijTooltip2" runat="server" TargetControlID="Image3">
                </wijmo:WijTooltip>
                <wijmo:WijTooltip ID="WijTooltip3" runat="server" TargetControlID="Image4">
                </wijmo:WijTooltip>
                <wijmo:WijTooltip ID="WijTooltip4" runat="server" TargetControlID="Image5">
                </wijmo:WijTooltip>
                <wijmo:WijTooltip ID="WijTooltip5" runat="server" TargetControlID="Image6">
                </wijmo:WijTooltip>
                <wijmo:WijTooltip ID="WijTooltip6" runat="server" TargetControlID="Image7">
                </wijmo:WijTooltip>
                <wijmo:WijTooltip ID="WijTooltip7" runat="server" TargetControlID="Image8">
                </wijmo:WijTooltip>
                    
            </div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="Description" ID="Content3">
<p>With the <strong>WijTooltip</strong>, you can easily create custom tooltips to offer your end-users a better user experience. You can control the behavior, the position, the event that shows the tooltip, and more with <strong>WijTooltip</strong>. The samples in this section will introduce you to some of these great features.</p>
</asp:Content>
