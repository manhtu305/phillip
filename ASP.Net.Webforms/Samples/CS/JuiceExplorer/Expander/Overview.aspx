﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
	Inherits="Expander_Overview" CodeBehind="Overview.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<asp:Panel ID="Panel1" runat="server">
		<h3>
			Header</h3>
		<div>
			Vestibulum ut eros non enim commodo hendrerit. Donec porttitor tellus non magna.
			Nam ligula elit, pretium et, rutrum non, hendrerit id, ante. Nunc mauris sapien,
			cursus in.
		</div>
	</asp:Panel>
	<wijmo:wijExpander ID="Panel1_Expander" runat="server" TargetControlID="Panel1">
	</wijmo:wijExpander>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Description" runat="server">
	<p>
		This sample demonstrates the default WijExpander behavior.
	</p>
	<p>&nbsp;</p>
	<p>
		The WijExpander allows you to show or hide embedded or external content within
		an expanding panel.
		Set the Expanded property to false if you wish to collapse content panel. 
		Default value for the Expanded property is true.
	</p>
</asp:Content>

