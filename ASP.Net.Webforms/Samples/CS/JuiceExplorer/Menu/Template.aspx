﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true" CodeFile="Template.aspx.cs" Inherits="Menu_Template" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:Panel ID="Panel1" runat="server">
        <ul id="wijmenu1">
            <li><a href="#">File</a>
                <ul>
                    <li>
                        <div>
                            <a href="#" class="wijmo-wijmenu-text">New</a><span class="wijmo-wijmenu-icon-right">Ctrl+N</span>
                        </div>
                    </li>
                    <li>
                        <div>
                            <a href="#" class="wijmo-wijmenu-text">Open</a><span class="wijmo-wijmenu-icon-right">Ctrl+O</span>
                        </div>
                    </li>
                    <li>
                        <div>
                            <a href="#" class="wijmo-wijmenu-text">Save</a><span class="wijmo-wijmenu-icon-right">Ctrl+S</span>
                        </div>
                    </li>
                </ul>
            </li>
            <li><a href="#">Edit</a>
                <ul>
                    <li>
                        <div>
                            <a href="#" class="wijmo-wijmenu-text">Undo</a><span class="wijmo-wijmenu-icon-right">Ctrl+Z</span>
                        </div>
                    </li>
                    <li></li>
                    <li>
                        <div>
                            <a href="#" class="wijmo-wijmenu-text">Cut</a><span class="wijmo-wijmenu-icon-right">Ctrl+X</span>
                        </div>
                    </li>
                    <li>
                        <div>
                            <a href="#" class="wijmo-wijmenu-text">Copy</a><span class="wijmo-wijmenu-icon-right">Ctrl+C</span>
                        </div>
                    </li>
                    <li>
                        <div>
                            <a href="#" class="wijmo-wijmenu-text">Paste</a><span class="wijmo-wijmenu-icon-right">Ctrl+V</span>
                        </div>
                    </li>
                </ul>
            </li>
            <li><a href="#">View</a>
                <ul>
                    <li>
                        <asp:CheckBox runat="server" ID="CheckBox1" Text="Status Bar" />
                    </li>
                    <li>
                        <asp:CheckBox runat="server" ID="CheckBox2" Text="Side Bar" />
                    </li>
                    <li></li>
                    <li><a href="#">Character Encoding</a>
                        <ul>
                            <li>
                                <asp:RadioButton runat="server" ID="RadioButton1" Text="Western (ISO-8859-1)" />
                            </li>
                            <li>
                                <asp:RadioButton runat="server" ID="RadioButton2" Text="Unicode (UTF-8)" />
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </asp:Panel>
    <wijmo:WijMenu ID="Menu1" runat="server" TargetControlID="Panel1" />
    <wijmo:WijCheckbox ID="Check1" runat="server" TargetControlID="CheckBox1" />
    <wijmo:WijCheckbox ID="Check2" runat="server" TargetControlID="CheckBox2" />
    <wijmo:WijRadio ID="Radio1" runat="server" TargetControlID="RadioButton1" />
    <wijmo:WijRadio ID="Radio2" runat="server" TargetControlID="RadioButton2" />
    <script id="scriptInit" type="text/javascript">
        $(document).ready(function () {
            $(".wijmo-wijmenu-text").parent().bind("click", function () {
                $("#<%=Panel1.ClientID %>").wijmenu("hideAllMenus");
            }).hover(function () {
                $(this).addClass("ui-state-hover");
            }, function () {
                $(this).removeClass("ui-state-hover");
            })
        });
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="Description" ID="Content3">
    <p>You can provide rich content for menu items using menu item templates.  This sample provides a template with menus, shortcut menu items, radio buttons, 
        and check boxes.</p>
</asp:Content>