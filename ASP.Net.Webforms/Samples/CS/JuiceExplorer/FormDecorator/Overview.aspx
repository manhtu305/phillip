﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Wijmo.master" AutoEventWireup="true"
    CodeFile="Overview.aspx.cs" Inherits="FormDecorator_Overview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Head" runat="Server">
    <style type="text/css">
		.formdecorator label
		{
			display: block;
		}       

		.formdecorator
		{
			list-style: none;
			margin: 0;
			padding: 0;
		}       

		.formdecorator li
		{
			clear: both;
			margin-bottom: 1em;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <ul class="formdecorator">
        <li>
            <h3>
                Input</h3>
            <asp:TextBox runat="server" ID="TextBox1"></asp:TextBox>
        </li>
        <li>
            <h3>
                TextArea</h3>
            <asp:TextBox runat="server" ID="TextBox3" TextMode="MultiLine"></asp:TextBox>
        </li>
        <li>
            <h3>
                Dropdown with Optgroups</h3>
            <asp:DropDownList runat="server" ID="DropDownList1">
                <asp:ListItem Text="A"></asp:ListItem>
                <asp:ListItem Text="B"></asp:ListItem>
                <asp:ListItem Text="C"></asp:ListItem>
                <asp:ListItem Text="D"></asp:ListItem>
                <asp:ListItem Text="E"></asp:ListItem>
                <asp:ListItem Text="F"></asp:ListItem>
                <asp:ListItem Text="G"></asp:ListItem>
                <asp:ListItem Text="H"></asp:ListItem>
                <asp:ListItem Text="I"></asp:ListItem>
                <asp:ListItem Text="J"></asp:ListItem>
                <asp:ListItem Text="K"></asp:ListItem>
                <asp:ListItem Text="L"></asp:ListItem>
                <asp:ListItem Text="M"></asp:ListItem>
            </asp:DropDownList>
            <%--<select id="select1">
				<optgroup label="A-G">
					<option>A</option>
					<option selected="selected">B</option>
					<option>C</option>
					<option>D</option>
					<option>E</option>
					<option>F</option>
					<option>G</option>
				</optgroup>
				<optgroup label="H-M">
					<option>H</option>
					<option>I</option>
					<option>J</option>
					<option>K</option>
					<option>L</option>
					<option>M</option>
				</optgroup>
			</select>--%>
        </li>
        <li>
            <h3>
                CheckBox</h3>
            <asp:CheckBox runat="server" ID="CheckBox1" Text="checkbox1" />
            <asp:CheckBox runat="server" ID="CheckBox2" Text="checkbox2" />
            <asp:CheckBox runat="server" ID="CheckBox3" Text="checkbox3" OnPreRender="CheckBox3_PreRender" />
            <asp:CheckBox runat="server" ID="CheckBox4" Text="checkbox4" />
            <%--<input id="checkbox1" type="checkbox" /><label for="checkbox1">checkbox1</label>
			<input id="checkbox2" type="checkbox" /><label for="checkbox2">checkbox2</label>
			<input id="checkbox3" type="checkbox" /><label for="checkbox3">checkbox3</label>
			<input id="checkbox4" type="checkbox" /><label for="checkbox4">checkbox4</label>--%>
        </li>
        <li>
            <h3>
                Radio</h3>
            <asp:RadioButton ID="RadioButton1" GroupName="radiobutton1" runat="server" Text="radio1" />
            <asp:RadioButton ID="RadioButton2" GroupName="radiobutton1" runat="server" Text="radio2" />
            <asp:RadioButton ID="RadioButton3" GroupName="radiobutton1" runat="server" Text="radio3" />
            <asp:RadioButton ID="RadioButton4" GroupName="radiobutton1" runat="server" Text="radio4" />
            <%--	<input type="radio" name="radiobutton1" id="radio1" /><label for="radio1">radio1</label>
			<input type="radio" name="radiobutton1" id="radio2" /><label for="radio2">radio2</label>
			<input type="radio" name="radiobutton1" id="radio3" /><label for="radio3">radio3</label>
			<input type="radio" name="radiobutton1" id="radio4" /><label for="radio4">radio4</label>--%>
        </li>
        <li>
            <h3>
                Button</h3>
            <asp:Button runat="server" ID="Button1" Text="Submit1" />
            <%--<input type="button" name="button1" value="Submit" />--%>
        </li>
    </ul>
    <!-- This TargetSelector matches all textboxes (single and multiline)-->
    <wijmo:WijTextbox ID="Tooltip1" runat="server" TargetControlID="TextBox1">
    </wijmo:WijTextbox>
    <wijmo:WijTextbox ID="WijTextbox1" runat="server" TargetControlID="TextBox3">
    </wijmo:WijTextbox>
    <wijmo:WijDropdown ID="WijDropdown1" runat="server" TargetControlID="DropDownList1"
        ZIndex="1982" />
    <wijmo:WijCheckbox ID="WijCheckbox1" runat="server" TargetControlID="CheckBox1" />
    <wijmo:WijCheckbox ID="WijCheckbox2" runat="server" TargetControlID="CheckBox2" />
    <wijmo:WijCheckbox ID="WijCheckbox3" runat="server" TargetControlID="CheckBox3" />
    <wijmo:WijCheckbox ID="WijCheckbox4" runat="server" TargetControlID="CheckBox4" />
    <wijmo:WijRadio ID="WijRadio1" runat="server" TargetControlID="RadioButton1" />
    <wijmo:WijRadio ID="WijRadio2" runat="server" TargetControlID="RadioButton2" />
    <wijmo:WijRadio ID="WijRadio3" runat="server" TargetControlID="RadioButton3" />
    <wijmo:WijRadio ID="WijRadio4" runat="server" TargetControlID="RadioButton4" />
    <wijmo:WijButton runat="server" ID="WijButton" TargetControlID="Button1">
        <Icons Primary="ui-icon-folder-collapsed" Secondary="ui-icon-folder-open" />
    </wijmo:WijButton>
    <%--<!-- This TargetSelector matches all textboxes (dropdownlists)-->
	<wijmo:WijDropdown ID="Dropdown1" runat="server" TargetSelector="select" />
    
    <!-- This TargetSelector matches all textboxes (checkboxes)-->
	<wijmo:WijCheckbox ID="Checkbox1" runat="server" TargetSelector=":input[type='checkbox']" />

    <!-- This TargetSelector matches all textboxes (radiobuttons)-->
	<wijmo:WijRadio ID="Radio1" runat="server" TargetSelector=":input[type='radio']" />
    
    <!-- This TargetSelector matches all textboxes (buttons)-->
	<wijmo:WijButton ID="Button1" runat="server" TargetSelector=":button">
	</wijmo:WijButton>--%>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="Description" ID="Content3">
    <p>
        The Wijmo Form Decorator widgets (wijradio, wijcheckbox, wijdropdown, wijtextbox)
        are used to decorate a standard HTML form elements. The Form Decorator widgets allow
        any form element to be styled uniformly in any browser. Form Decorators use a property
        called TargetSelector to turn all matched elements into decorated widgets. The TargetSelector
        property accepts a jQuery selector string which is similar to CSS.</p>
</asp:Content>
