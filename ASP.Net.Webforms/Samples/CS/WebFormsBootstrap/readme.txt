ASP.NET WebForms Bootstrap
--------------------------------------------------------------------------------------------
Use Studio for ASP.NET Web Forms with Bootstrap as a Theme.

This sample shows how to integrate Bootstrap and C1 ASP.NET Web Forms controls together. You can optionally use 
Bootstrap instead of a Theme. In order to do so, you simply add bootstrap and then our 
integration js and css files.