﻿<%@ Page Title="C1Calendar using Bootstrap" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebFormsBootstrap._Default" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Calendar" TagPrefix="wijmo" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <div>
        <wijmo:C1Calendar ID="Calendar1" runat="server"></wijmo:C1Calendar>
    </div>

</asp:Content>
