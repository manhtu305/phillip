﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UserDefaultMaster.Master" AutoEventWireup="true" CodeBehind="UserAddNurse.aspx.cs" Inherits="Grapecity.Samples.HospitalOne.H1ASPNet.UserAddNurse" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajt" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Upload" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1AutoComplete" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1GridView" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Dialog" tagprefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>        
        <tr>
            <td></td>
        </tr>        
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="PnlEdit" runat="server">
                            <table class="AddUpdateTbl">
                                <tr>
                                    <td class="AddUpdateTbl_Col1">&nbsp;</td>
                                    <td class="AddUpdateTbl_Col2">Reg. Date</td>
                                    <td>
                                        <wijmo:C1InputDate ID="C1RegDate" runat="server" Date="06/06/2014 15:39:00" TabIndex="1">
                                        </wijmo:C1InputDate>
                                    </td>
                                    <td class="AddUpdateTbl_Col4">
                                        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator13" runat="server" ControlToValidate="C1RegDate" Display="Dynamic" ErrorMessage="Select Registration Date" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                        <asp:CompareValidator CssClass="Validations" ID="CompareValidator1" runat="server" ControlToValidate="C1RegDate" Display="Dynamic" ErrorMessage="Enter valid Reg. Date" Operator="DataTypeCheck" Type="Date" ValidationGroup="save">*</asp:CompareValidator>
                                    </td>
                                    <td class="AddUpdateTbl_Col5">Login Name</td>
                                    <td>
                                        <wijmo:C1InputText ID="txtLoginUserName" runat="server" Format="A9a." MaxLength="20" TabIndex="2">
                                        </wijmo:C1InputText>
                                    </td>
                                    <td class="AddUpdateTbl_Col7">
                                        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtLoginUserName" Display="Dynamic" ErrorMessage="Enter Login User Name" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>Name</td>
                                    <td>
                                        <wijmo:C1InputText ID="txtContactPerson" runat="server" Format="Aa9 " MaxLength="50" TabIndex="3">
                                        </wijmo:C1InputText>
                </td>
                                    <td>
                                        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtContactPerson" Display="Dynamic" ErrorMessage="Enter Contact Person" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td>Address</td>
                                    <td rowspan="2">
                                        <asp:TextBox ID="txtAddress" runat="server" CssClass="txtMultiline" Height="65px" MaxLength="250" TabIndex="6" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                    <td rowspan="2"></td>
                                </tr>
                                <tr>
                                    <td ></td>
                                    <td >Sex</td>
                                    <td >
                                        <wijmo:C1ComboBox ID="C1ddlSex" runat="server" AutoComplete="False" TabIndex="4">
                                        </wijmo:C1ComboBox>
                                    </td>
                                    <td >
                                        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator23" runat="server" ControlToValidate="C1ddlSex" Display="Dynamic" ErrorMessage="Select Sex" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td ></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>Father&#39;s Name</td>
                                    <td>
                                        <wijmo:C1InputText ID="txtFatherName" runat="server" Format="Aa9 " MaxLength="50" TabIndex="5">
                                        </wijmo:C1InputText>
                </td>
                                    <td>
                                        &nbsp;</td>
                                    <td>Land Mark</td>
                                    <td>
                                        <wijmo:C1InputText ID="txtLandMark" runat="server" MaxLength="50" TabIndex="7">
                                        </wijmo:C1InputText>
                            </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>Country</td>
                                    <td>
                                        <wijmo:C1ComboBox ID="C1ddlcountry" runat="server" AutoPostBack="True" OnSelectedIndexChanged="C1ddlcountry_SelectedIndexChanged" TabIndex="8" ForceSelectionText="True">
                                        </wijmo:C1ComboBox>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator24" runat="server" ControlToValidate="C1ddlcountry" Display="Dynamic" ErrorMessage="Select Country" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td>State</td>
                                    <td>
                                        <wijmo:C1ComboBox ID="C1ddlstate" runat="server" AutoPostBack="True" OnSelectedIndexChanged="C1ddlstate_SelectedIndexChanged" TabIndex="9" ForceSelectionText="True">
                                        </wijmo:C1ComboBox>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator29" runat="server" ControlToValidate="C1ddlstate" Display="Dynamic" ErrorMessage="Select State" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>City</td>
                                    <td>
                                        <wijmo:C1ComboBox ID="C1ddlCity" runat="server" TabIndex="10">
                                        </wijmo:C1ComboBox>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator25" runat="server" ControlToValidate="C1ddlCity" Display="Dynamic" ErrorMessage="Select/Enter City" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td>Zip Code</td>
                                    <td>
                                        <wijmo:C1InputText ID="C1txtZipcode" runat="server" Format="A9- " MaxLength="20" TabIndex="11">
                                        </wijmo:C1InputText>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>Contact No.</td>
                                    <td>
                                        <wijmo:C1InputText ID="C1txtMobile" runat="server" Format="9+-() " MaxLength="30" TabIndex="12">
                                        </wijmo:C1InputText>
</td>
                                    <td>&nbsp;</td>
                                    <td>Date of Birth</td>
                                    <td>                                        
                                        <wijmo:C1InputDate ID="C1DOB" runat="server" Date="06/06/2014 15:39:00" TabIndex="13">
                                        </wijmo:C1InputDate>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator30" runat="server" ControlToValidate="C1DOB" Display="Dynamic" ErrorMessage="Enter Date of birth" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                        <asp:CompareValidator CssClass="Validations" ID="CompareValidator6" runat="server" ControlToValidate="C1DOB" Display="Dynamic" ErrorMessage="Enter valid Date of birth" Operator="DataTypeCheck" Type="Date" ValidationGroup="save">*</asp:CompareValidator></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>Email </td>
                                    <td>
                                        <wijmo:C1InputText ID="C1txtEmail" runat="server" MaxLength="50" TabIndex="14">
                                        </wijmo:C1InputText>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator26" runat="server" ControlToValidate="C1txtEmail" Display="Dynamic" ErrorMessage="Enter e-mail id" SetFocusOnError="true" ValidationGroup="save" Visible="False">*</asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator CssClass="Validations" ID="RegularExpressionValidator3" runat="server" ControlToValidate="C1txtEmail" ErrorMessage="Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="save">*</asp:RegularExpressionValidator>
                                    </td>
                                    <td>SSN</td>
                                    <td>
                                        <wijmo:C1InputText ID="C1txtSSN" runat="server" Format="A9-" MaxLength="50" TabIndex="15">
                                        </wijmo:C1InputText>
                                    </td>
                                    <td>
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>Qualification</td>
                                    <td>
                                        <wijmo:C1ComboBox ID="C1ddlQualification" runat="server" TabIndex="16" ForceSelectionText="True">
                                        </wijmo:C1ComboBox>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator33" runat="server" ControlToValidate="C1ddlQualification" Display="Dynamic" ErrorMessage="Select Qualification" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td>Blood Group</td>
                                    <td>
                                        <wijmo:C1ComboBox ID="C1ddlBloodGroup" runat="server" TabIndex="17" ForceSelectionText="True">
                                        </wijmo:C1ComboBox>
                                    </td>
                                    <td>
                                        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator31" runat="server" ControlToValidate="C1ddlBloodGroup" Display="Dynamic" ErrorMessage="Select Blood Group" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td ></td>
                                    <td >Speciality</td>
                                    <td>
                                        <wijmo:C1ComboBox ID="C1ddlSpecialist" runat="server" TabIndex="18" ForceSelectionText="True">
                                        </wijmo:C1ComboBox>
                                    </td>
                                    <td >
                                        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator32" runat="server" ControlToValidate="C1ddlSpecialist" Display="Dynamic" ErrorMessage="Select Specialization of Nurse" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                    </td>
                                    <td >Profile Photo</td>
                                    <td >
                                        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="FileUploader" TabIndex="19" />
                                    </td>
                                    <td >
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td></td>
                                    <td>
                                        &nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td></td>
                                    <td>
                                    </td>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td id="msglbl">&nbsp;</td>
                                    <td id="msglbl0" colspan="5">
                                        <asp:Label ID="LblMsg" runat="server" CssClass="msglbl" Font-Bold="True"></asp:Label>
                                    </td>
                                    <td id="msglbl1">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td colspan="5">
                                        <asp:Panel ID="PnlUpdateBtns" runat="server" HorizontalAlign="left">
                                            <table align="left">
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="BtnSubmit" runat="server" CssClass="myButton" OnClick="BtnSubmit_Click" TabIndex="20" Text="Submit" ValidationGroup="save" />
                                                        <ajt:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure to save record?" TargetControlID="BtnSubmit">
                                                        </ajt:ConfirmButtonExtender>
                                                    </td>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="BtnReset" runat="server" CssClass="myButton" OnClick="BtnReset_Click" TabIndex="21" Text="Cancel" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:HiddenField ID="HFRegID" runat="server" />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="False" ValidationGroup="save" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            
            
            
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        </table>


</asp:Content>
