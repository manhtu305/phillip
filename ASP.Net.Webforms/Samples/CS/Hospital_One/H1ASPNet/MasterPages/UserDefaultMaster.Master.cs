﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using Grapecity.Samples.HospitalOne.H1DBService_DBML;
using C1.Web.Wijmo.Controls.C1ComboBox;
using C1.Web.Wijmo.Controls.C1Carousel;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Data.Entity;
using System.Data.Linq;
using System.Text;

namespace Grapecity.Samples.HospitalOne.H1ASPNet
{
    public partial class UserDefaultMaster : System.Web.UI.MasterPage, ICallbackEventHandler
    {
        UIBinder UIBObj1 = new UIBinder();


        private string _callbackArgument = "";
        private string _callbackResult = "unknown command";

        protected void RegisterCallbacks()
        {

            string webFormDoCallbackScript = this.Page.ClientScript.GetCallbackEventReference(this, "arg", "onCallbackSuccess", null, true);
            string serverCallScript = "function executeCallback(arg){" + webFormDoCallbackScript + ";\n}\n";

            if (!this.Page.ClientScript.IsClientScriptBlockRegistered("executeCallbackScript"))
            {
                this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "executeCallbackScript", serverCallScript, true);
            }
        }

        public string GetCallbackResult()
        {
            return _callbackResult;
        }

        //To change theme of wijmo controls
        public void RaiseCallbackEvent(string eventArgument)
        {
            _callbackArgument = eventArgument;
            string[] arr = eventArgument.Split('=');
            if (arr.Length == 2)
            {
                switch (arr[0])
                {
                    case "theme":
                        C1InputText1.Theme = arr[1];
                        _callbackResult = "ok";
                        break;
                    default:
                        break;
                }
            }
        }

        //To check page rights for current user
        protected void CheckPageRights()
        {
            if (Session["UserType_ID"] == null || Session["UserType"] == null || Session["User_ID"] == null)
            {
                Response.Redirect("Logout.aspx");
            }
        }

        //To fill Menus according to user rights
        protected void SetMenus()
        {
            StringBuilder SB1 = new StringBuilder(100000);
            HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
            List<Grapecity.Samples.HospitalOne.H1DBService_DBML.PRC_GetParentMenuListResult> Result1 = H1EFObj1.PRC_GetParentMenuList(long.Parse(Session["User_ID"].ToString())).ToList();
            for (int PMenuID = 0; PMenuID < Result1.Count; PMenuID++)
            {
                SB1.Append("<li><a href='#'><b>" + Result1[PMenuID].ParentMenu_Name + "</b></a><ul>");
                List<PRC_GetChildMenuListResult> Result2 = H1EFObj1.PRC_GetChildMenuList(long.Parse(Session["User_ID"].ToString()), Result1[PMenuID].ParentMenu_ID).ToList();
                for (int CMenuID = 0; CMenuID < Result2.Count; CMenuID++)
                {
                    SB1.Append("<li><a href=" + Result2[CMenuID].Page_URL + ">" + Result2[CMenuID].ChildMenu_Name + "</a></li>");
                }
                SB1.Append("</ul></li>");
            }
            LblMenuList.Text = SB1.ToString();
        }

        //To set page for first time loading
        protected void SetPageLoad()
        {
            if (!IsPostBack)
            {
                SetMenus();        
                LblWelcomeMsg.Text = Session["User_Name"].ToString();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckPageRights();
                SetPageLoad();
                RegisterCallbacks();
            }
            catch (Exception ex)
            {                
            }
        }

        protected void LBtnLogout_Click(object sender, EventArgs e)
        {
            Response.Redirect("Logout.aspx");
        }
    }
}