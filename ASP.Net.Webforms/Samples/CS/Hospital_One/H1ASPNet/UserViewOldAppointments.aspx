﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UserDefaultMaster.Master" AutoEventWireup="true" CodeBehind="UserViewOldAppointments.aspx.cs" Inherits="Grapecity.Samples.HospitalOne.H1ASPNet.UserViewOldAppointments" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1AutoComplete" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Input" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1ComboBox" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Calendar" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1GridView" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Upload" tagprefix="wijmo1" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Tabs" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Dialog" tagprefix="wijmo" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="auto-style1">
        <tr>
            <td>                
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table style="width: 900px">
                            <tr>
                                <td>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="PnlSearchPatient" runat="server">
                                        <table>
                                            <tr>
                                                <td>
                                                    <wijmo:C1ComboBox ID="C1ddlSearchName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="C1ddlSearchName_SelectedIndexChanged" TabIndex="1">
                                                    </wijmo:C1ComboBox>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImgBtnSearchUserList" runat="server" ImageUrl="~/images/Search_Image.png" OnClick="ImgBtnSearchUserList_Click" TabIndex="2" Visible="False" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="LblMsg" runat="server" CssClass="msglbl" Font-Bold="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Panel ID="PnlAppointmentsList" runat="server">
                                        <wijmo:C1GridView ID="C1GVOldAppointments" runat="server" AllowPaging="True" AutogenerateColumns="False" OnPageIndexChanging="C1GVOldAppointments_PageIndexChanging" PageSize="10" TabIndex="3" Width="900px">
                                            <PagerSettings Mode="NextPreviousFirstLast" />
                                            <Columns>
                                                <wijmo:C1TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImgBtn_C1GVOldAppointments_ViewAppointment" runat="server" CommandArgument='<%# Eval("Appointment_ID") %>' Height="15px" ImageUrl="~/images/View_Image.png" OnClick="ImgBtn_C1GVOldAppointments_ViewAppointment_Click" ToolTip="View Appointment" />                                                                                                              
                                                    </ItemTemplate>
                                                    <ControlStyle HorizontalAlign="Center" />
                                                    <FooterStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" Width="20px"/>
                                                </wijmo:C1TemplateField>
                                                <wijmo:C1TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImgBtn_C1GVOldAppointments_EditAppointment" runat="server" CommandArgument='<%# Eval("Appointment_ID") %>' Height="15px" ImageUrl="~/images/Edit_Image.png" OnClick="ImgBtn_C1GVOldAppointments_EditAppointment_Click" ToolTip="Edit Appointment" />
                                                    </ItemTemplate>
                                                    <ControlStyle HorizontalAlign="Center" />
                                                    <FooterStyle HorizontalAlign="Center" />
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" Width="20px"/>
                                                </wijmo:C1TemplateField>
                                                <wijmo:C1BoundField DataField="Appointment_ID" HeaderText="App.No.">
                                                    <ControlStyle HorizontalAlign="Left" Width="50px" />
                                                    <FooterStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </wijmo:C1BoundField>
                                                <wijmo:C1BoundField DataField="Doctor_Name" HeaderText="Doctor">
                                                    <ControlStyle HorizontalAlign="Left" Width="650px" />
                                                    <FooterStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </wijmo:C1BoundField>
                                                <wijmo:C1BoundField DataField="Appointment_DateTime" HeaderText="App. Schedule">
                                                    <ControlStyle HorizontalAlign="Left" Width="100px" />
                                                    <FooterStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </wijmo:C1BoundField>
                                                <wijmo:C1BoundField DataField="Diagnosis_Status" HeaderText="Diagnosis Status">
                                                    <ControlStyle HorizontalAlign="Left" Width="60px" />
                                                    <FooterStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </wijmo:C1BoundField>
                                            </Columns>
                                        </wijmo:C1GridView>
                                    </asp:Panel>
                                    <asp:HiddenField ID="HFPatientID" runat="server" />
                                    <asp:HiddenField ID="HFPatientName" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <wijmo:C1Dialog ID="C1DialogAddViewEditAppointment" runat="server" AppendTo="body" AutoExpand="True" MaintainVisibilityOnPostback="False" Show="blind" ShowOnLoad="False" Width="900px">
                                        <CollapsingAnimation Option="">
                                        </CollapsingAnimation>
                                        <ExpandingAnimation Option="">
                                        </ExpandingAnimation>
                                        <CaptionButtons>
                                            <Pin IconClassOff="ui-icon-pin-s" IconClassOn="ui-icon-pin-w" />
                                            <Refresh IconClassOn="ui-icon-refresh" />
                                            <Minimize IconClassOn="ui-icon-minus" />
                                            <Maximize IconClassOn="ui-icon-extlink" />
                                            <Close IconClassOn="ui-icon-close" />
                                        </CaptionButtons>
                                        <Content>
                                            <asp:Panel ID="PnlAddAppointment" runat="server">
                                                <table>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td colspan="5">&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td colspan="5">
                                                            <asp:Label ID="LblPatientNameAtAddApp" runat="server" style="font-weight: 700" Visible="False"></asp:Label>
                                                            <asp:HiddenField ID="HFAddAppPatientID" runat="server" />
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30px"></td>
                                                        <td width="200px">Associate Doctor*</td>
                                                        <td>
                                                            <wijmo:C1ComboBox ID="C1ComboAssociateDoctor" runat="server" TabIndex="4">
                                                            </wijmo:C1ComboBox>
                                                        </td>
                                                        <td width="100px">
                                                            <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator4" runat="server" ControlToValidate="C1ComboAssociateDoctor" Display="Dynamic" ErrorMessage="Select Associate Doctor" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td width="200px">Appointment Schedule*</td>
                                                        <td>
                                                            <wijmo:C1InputDate ID="C1InpDateAppointmentSchedule" runat="server" Date="06/23/2014 17:24:00" DateFormat="g" TabIndex="5">
                                                            </wijmo:C1InputDate>
                                                        </td>
                                                        <td width="30px">
                                                            <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator5" runat="server" ControlToValidate="C1InpDateAppointmentSchedule" Display="Dynamic" ErrorMessage="Select Appointment Schedule" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                         <td colspan="5"><hr size="1" noshade="noshade" style="color: #E4E4E4;" /></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>Height</td>
                                                        <td>
                                                            <wijmo:C1InputNumeric ID="C1InNumHeight" runat="server" TabIndex="6">
                                                            </wijmo:C1InputNumeric>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>Weight</td>
                                                        <td>
                                                            <wijmo:C1InputNumeric ID="C1InNumWeight" runat="server" TabIndex="7">
                                                            </wijmo:C1InputNumeric>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>Body Temperature</td>
                                                        <td>
                                                            <wijmo:C1InputNumeric ID="C1InNumTemperature" runat="server" TabIndex="8">
                                                            </wijmo:C1InputNumeric>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>Heart Rate</td>
                                                        <td>
                                                            <wijmo:C1InputNumeric ID="C1InNumHeartRate" runat="server" TabIndex="9">
                                                            </wijmo:C1InputNumeric>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>Systolic BP</td>
                                                        <td>
                                                            <wijmo:C1InputNumeric ID="C1InNumSysBP" runat="server" TabIndex="10">
                                                            </wijmo:C1InputNumeric>
                                                        </td>
                                                        <td></td>
                                                        <td>Notes</td>
                                                        <td rowspan="3">
                                                            <asp:TextBox ID="TxtAppointmentNotes" runat="server" CssClass="txtMultiline" Height="95px" MaxLength="500" TabIndex="13" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>Diastolic BP</td>
                                                        <td>
                                                            <wijmo:C1InputNumeric ID="C1InNumDiastBP" runat="server" TabIndex="11">
                                                            </wijmo:C1InputNumeric>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td>Diagnosis Status</td>
                                                        <td>
                                                            <wijmo:C1ComboBox ID="C1ComboDiagnosisStatus" runat="server" TabIndex="12">
                                                            </wijmo:C1ComboBox>
                                                        </td>
                                                        <td>
                                                            <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator6" runat="server" ControlToValidate="C1ComboDiagnosisStatus" Display="Dynamic" ErrorMessage="Select Diagnosis Status" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td></td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td></td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td colspan="5">
                                                            <asp:Label ID="LblMsgDialog" runat="server" CssClass="msglbl" Font-Bold="True"></asp:Label>
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td colspan="5">
                                                            <asp:Panel ID="PnlAddAppointmentBtns" runat="server">
                                                                <table align="left">
                                                                    <tr>
                                                                        <td>
                                                                            <asp:Button ID="BtnSubmit" runat="server" CssClass="myButton" OnClick="BtnSubmit_Click" TabIndex="14" ValidationGroup="save" />
                                                                            <ajt:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure to save/update record?" TargetControlID="BtnSubmit">
                                                                            </ajt:ConfirmButtonExtender>
                                                                        </td>
                                                                        <td>
                                                                            <asp:Button ID="BtnReset" runat="server" CssClass="myButton" OnClick="BtnReset_Click" TabIndex="15" Text="Reset" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </asp:Panel>
                                                            <asp:HiddenField ID="HFAppointmentDialogType" runat="server" />
                                                            <asp:HiddenField ID="HFAppointmentID" runat="server" />
                                                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowSummary="False" ValidationGroup="save" />
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                        <td colspan="5">&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                </table>
                                                </td>
                                                <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                </table>
                                            </asp:Panel>
                                        </Content>
                                    </wijmo:C1Dialog>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
