﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UserDefaultMaster.Master" AutoEventWireup="true" CodeBehind="UserManageDoctor.aspx.cs" Inherits="Grapecity.Samples.HospitalOne.H1ASPNet.UserManageDoctor" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajt" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Upload" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1AutoComplete" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1GridView" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Dialog" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Wizard" tagprefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <asp:PlaceHolder ID="PlaceHolder1" runat="server">    
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script type="text/javascript">
            function onBtnCancelClick() {
                $("C1DialogViewEdit").c1dialog("close");
                return false;
            }

            $(document).ready(function () {
            });
        </script>
    </asp:PlaceHolder>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table>                
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td >
                        <asp:Panel ID="PnlSearchMain" runat="server" Width="900px">
                            <table width="100%">
                                <tr>
                                    <td>
                                                                                   
                                            <table>
                                                <tr>
                                                    <td style="vertical-align: top">
                                        <wijmo:C1ComboBox ID="C1ddlSearchName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="C1ddlSearchName_SelectedIndexChanged" TabIndex="1" Text="Select Doctor">
                                        </wijmo:C1ComboBox></td>
                                                    <td style="vertical-align: bottom">&nbsp;&nbsp;<a href="UserAddDoctor.aspx"><img src="images/Plus_Image.png" height="30" alt="Add New Doctor" tabindex="2"/></a>
                                                    </td>
                                                </tr>
                                            </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        <asp:CheckBox ID="CBoxAdvanceSearch" runat="server" OnCheckedChanged="CBoxAdvanceSearch_CheckedChanged" Text="Advance Search" AutoPostBack="True" TabIndex="3" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="PnlAdvanceSearch"  HorizontalAlign="Left" runat="server" Width="100%" Visible="False">
                                             

                                            <table border="0" cellspacing="0" cellpadding="0" class="AdvanceSearch" width="100%">
  <tr>
    <td align="left" valign="top" style="text-align:left"><label for="textfield2"></label>
      Reg.No.<br>      
        <wijmo:C1InputText ID="txtSearchRegID" runat="server" Format="9" TabIndex="4">
        </wijmo:C1InputText>
        
    <td align="left" valign="top">SSN<br>     <wijmo:C1InputText ID="txtSearchSSN" runat="server" Format="9-" TabIndex="5">
                                                        </wijmo:C1InputText>
                                                </td>
    <td align="left" valign="top">Reg. Date (From-To)<br>
      <wijmo:C1InputDate ID="C1SearchDate1" runat="server" Date="06/06/2014 15:39:00" TabIndex="6">
                                                        </wijmo:C1InputDate>
                                                        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator21" runat="server" ControlToValidate="C1SearchDate1" Display="Dynamic" ErrorMessage="Enter Search From Date" SetFocusOnError="true" ValidationGroup="search">*</asp:RequiredFieldValidator>
                                                        <asp:CompareValidator CssClass="Validations" ID="CompareValidator4" runat="server" ControlToValidate="C1SearchDate1" Display="Dynamic" ErrorMessage="Enter valid Search From Date" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="search">*</asp:CompareValidator>
                                                        -</td>
    <td align="left" valign="top"><br>      <wijmo:C1InputDate ID="C1SearchDate2" runat="server" Date="06/06/2014 15:39:00" TabIndex="7">
                                                        </wijmo:C1InputDate>
                                                        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator22" runat="server" ControlToValidate="C1SearchDate2" Display="Dynamic" ErrorMessage="Enter Search To Date" SetFocusOnError="true" ValidationGroup="search">*</asp:RequiredFieldValidator>
                                                        <asp:CompareValidator CssClass="Validations" ID="CompareValidator5" runat="server" ControlToValidate="C1SearchDate2" Display="Dynamic" ErrorMessage="Enter valid Search To Date" Operator="DataTypeCheck" SetFocusOnError="True" Type="Date" ValidationGroup="search">*</asp:CompareValidator></td>
  </tr>
  <tr>
    <td align="left" valign="top">Name<br>      <wijmo:C1InputText ID="txtSearchName" runat="server" Format="Aa9 " TabIndex="8">
                                                        </wijmo:C1InputText>
                                                </td>
    <td align="left" valign="top">City/Area<br>      
      <wijmo:C1InputText ID="txtSearchArea" runat="server" TabIndex="9">
                                                        </wijmo:C1InputText>
                                                </td>
    <td align="left" valign="top"><label for="select"></label>
      Speciality<br>
      <wijmo:C1ComboBox ID="C1ddlSearchSpeciality" runat="server" ViewStateMode="Enabled" TabIndex="10">
                                                        </wijmo:C1ComboBox>
                                                        
                                                </td>
    <td align="left" valign="top"><a href="#"><br>
      <asp:ImageButton ID="ImgBtnAdvanceSearch" runat="server" ImageUrl="~/images/Search_Image.png" OnClick="ImgBtnAdvanceSearch_Click" ValidationGroup="search" TabIndex="11" /></td>
      <asp:ValidationSummary ID="ValidationSummary4" runat="server" ShowMessageBox="True" ShowSummary="False" ValidationGroup="search" />
  </tr>
</table>

                                            
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LblMsg" runat="server" CssClass="msglbl" Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left">
                                        <asp:Panel ID="PnlSearchGrid" runat="server" Width="900px" HorizontalAlign="Left">
                                            <asp:HiddenField ID="HFFillGridType" runat="server" />
                                            <wijmo:C1GridView ID="C1GridView1" runat="server" AllowPaging="True" AutogenerateColumns="False" style="top: 0px; left: 0px" OnPageIndexChanging="C1GridView1_PageIndexChanging" Width="900px" TabIndex="12" >
                                                <PagerSettings Mode="NextPreviousFirstLast" />
                                                <Columns>
                                                    <wijmo:C1TemplateField>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="ImgBtnView" runat="server" CommandArgument='<%# Eval("User_ID") %>' Height="15" ImageUrl="~/images/View_Image.png" OnClick="ImgBtnView_Click" ToolTip="View Details" />
                                                        </ItemTemplate>
                                                        <ControlStyle HorizontalAlign="Center" />
                                                        <FooterStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center"/>
                                                        <ItemStyle HorizontalAlign="Center" Width="20px"/>
                                                    </wijmo:C1TemplateField>

                                                    <wijmo:C1TemplateField>
                                                        <ItemTemplate>                                                           
                                                           <asp:ImageButton ID="ImgBtnEdit" runat="server" CommandArgument='<%# Eval("User_ID") %>' Height="15" ImageUrl="~/images/Edit_Image.png" OnClick="ImgBtnEdit_Click" ToolTip="Edit Details" />
                                                        </ItemTemplate>
                                                        <ControlStyle HorizontalAlign="Center" />
                                                        <FooterStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center"/>
                                                        <ItemStyle HorizontalAlign="Center" Width="20px"/>
                                                    </wijmo:C1TemplateField>

                                                    <wijmo:C1TemplateField>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="ImgBtnDelete" runat="server" CommandArgument='<%# Eval("User_ID") %>' Height="15" ImageUrl="~/images/Delete_Image.png" OnClick="ImgBtnDelete_Click" OnClientClick="return confirm('Are you sure to delete this record?');" ToolTip="Delete Record" />
                                                        </ItemTemplate>
                                                        <ControlStyle HorizontalAlign="Center" />
                                                        <FooterStyle HorizontalAlign="Center" />
                                                        <HeaderStyle HorizontalAlign="Center"/>
                                                        <ItemStyle HorizontalAlign="Center" Width="20"/>
                                                    </wijmo:C1TemplateField>
                                                    
                                                    <wijmo:C1BoundField DataField="User_ID" HeaderText="Reg. No" DataFormatString="d">
                                                        <ControlStyle HorizontalAlign="Left" Width="90px" />
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </wijmo:C1BoundField>
                                                    <wijmo:C1BoundField DataField="User_Name" HeaderText="Doctor's Name" >
                                                        <ControlStyle HorizontalAlign="Left" Width="200px" />
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </wijmo:C1BoundField>
                                                    <wijmo:C1BoundField DataField="Sex" HeaderText="Sex" >
                                                        <ControlStyle HorizontalAlign="Left" Width="100px" />
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </wijmo:C1BoundField>
                                                    <wijmo:C1BoundField DataField="Specialist" HeaderText="Speciality" >
                                                        <ControlStyle HorizontalAlign="Left" Width="150px" />
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </wijmo:C1BoundField>
                                                    <wijmo:C1BoundField DataField="City" HeaderText="City/Area" >
                                                        <ControlStyle HorizontalAlign="Left" Width="150" />
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </wijmo:C1BoundField>
                                                    <wijmo:C1BoundField DataField="Contact_No" HeaderText="Contact No" >
                                                        <ControlStyle HorizontalAlign="Left" Width="150px" />
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" />
                                                    </wijmo:C1BoundField>
                                           
                                                </Columns>
                                            </wijmo:C1GridView>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td ></td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        
                        <wijmo:C1Dialog ID="C1DialogViewEdit" runat="server" ShowOnLoad="False" MaintainVisibilityOnPostback="False" Show="blind" Modal="True" AppendTo="body" AutoExpand="True" Width="900px" Resizable="False">
                            <CollapsingAnimation Option="">
                            </CollapsingAnimation>
                            <ExpandingAnimation Option="">
                            </ExpandingAnimation>
                            <CaptionButtons>
                                <Pin IconClassOff="ui-icon-pin-s" IconClassOn="ui-icon-pin-w" />
                                <Refresh IconClassOn="ui-icon-refresh" />
                                <Minimize IconClassOn="ui-icon-minus" />
                                <Maximize IconClassOn="ui-icon-extlink" />
                                <Close IconClassOn="ui-icon-close" />
                            </CaptionButtons>
                            <Content>
                                <asp:Panel ID="PnlEdit" runat="server" width="95%">
                                    <table width="800px">
                                        <tr>
                                            <td width="30px">&nbsp;</td>
                                            <td width="100px">&nbsp;</td>
                                            <td>
                                                &nbsp;</td>
                                            <td width="100px">
                                                &nbsp;</td>
                                            <td width="100px"></td>
                                            <td>
                                            &nbsp;</td>
                                            <td  width="30px">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="30px">&nbsp;</td>
                                            <td width="100px">Reg. Date</td>
                                            <td>
                                                <wijmo:C1InputDate ID="C1RegDate" runat="server" Date="06/06/2014 15:39:00" TabIndex="13">
                                                </wijmo:C1InputDate>
                                            </td>
                                            <td width="100px">
                                                <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator13" runat="server" ControlToValidate="C1RegDate" Display="Dynamic" ErrorMessage="Select Registration Date" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                <asp:CompareValidator CssClass="Validations" ID="CompareValidator1" runat="server" ControlToValidate="C1RegDate" Display="Dynamic" ErrorMessage="Enter valid Reg. Date" Operator="DataTypeCheck" Type="Date" ValidationGroup="save">*</asp:CompareValidator>
                                            </td>
                                            <td width="100px">Login Name</td>
                                            <td>
                                                <wijmo:C1InputText ID="txtLoginUserName" runat="server" MaxLength="20" Format="A9a." TabIndex="14">
                                                </wijmo:C1InputText>
                                                
                                            </td>
                                            <td width="30px">
                                                <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator36" runat="server" ControlToValidate="txtLoginUserName" Display="Dynamic" ErrorMessage="Enter Login User Name" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>Name</td>
                                            <td>
                                                <wijmo:C1InputText ID="txtContactPerson" runat="server" MaxLength="50" Format="Aa9 " TabIndex="15">
                                                </wijmo:C1InputText>                                            
                                            </td>
                                            <td>
                                                <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtContactPerson" Display="Dynamic" ErrorMessage="Enter Contact Person" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td>Address</td>
                                            <td rowspan="2">
                                            
                                                <asp:TextBox ID="txtAddress" runat="server" CssClass="txtMultiline" Height="65px" MaxLength="250" TabIndex="18" TextMode="MultiLine"></asp:TextBox>
                                            
                                            </td>
                                            <td rowspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td ></td>
                                            <td >Sex</td>
                                            <td >
                                                <wijmo:C1ComboBox ID="C1ddlSex" runat="server" AutoComplete="False" ForceSelectionText="True" TabIndex="16">
                                                </wijmo:C1ComboBox>
                                            </td>
                                            <td >
                                                <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator23" runat="server" ControlToValidate="C1ddlSex" Display="Dynamic" ErrorMessage="Select Sex" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td ></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>Father&#39;s Name</td>
                                            <td>
                                                <wijmo:C1InputText ID="txtFatherName" runat="server" MaxLength="50" Format="Aa9 " TabIndex="17">
                                                </wijmo:C1InputText>
                                            
                                            </td>
                                            <td>
                                                &nbsp;</td>
                                            <td>Land Mark</td>
                                            <td>
                                                <wijmo:C1InputText ID="txtLandMark" runat="server" MaxLength="50" TabIndex="19">
                                                </wijmo:C1InputText>
                                            
                                            </td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>Country</td>
                                            <td>
                                                <wijmo:C1ComboBox ID="C1ddlcountry" runat="server" AutoPostBack="True" OnSelectedIndexChanged="C1ddlcountry_SelectedIndexChanged" ForceSelectionText="True" TabIndex="20">
                                                </wijmo:C1ComboBox>
                                            </td>
                                            <td>
                                                <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator24" runat="server" ControlToValidate="C1ddlcountry" Display="Dynamic" ErrorMessage="Select Country" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td>State</td>
                                            <td>                                                <wijmo:C1ComboBox ID="C1ddlstate" runat="server" AutoPostBack="True" OnSelectedIndexChanged="C1ddlstate_SelectedIndexChanged" ForceSelectionText="True" TabIndex="21">
                                                </wijmo:C1ComboBox>
                                            </td>
                                            <td>
                                                <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator29" runat="server" ControlToValidate="C1ddlstate" Display="Dynamic" ErrorMessage="Select State" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>City</td>
                                            <td>
                                                <wijmo:C1ComboBox ID="C1ddlCity" runat="server" TabIndex="22">
                                                </wijmo:C1ComboBox>
                                            </td>
                                            <td>
                                                <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator37" runat="server" ControlToValidate="C1ddlCity" Display="Dynamic" ErrorMessage="Select/Enter City" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                            </td>
                                            <td>Zip Code</td>
                                            <td>
                                                <wijmo:C1InputText ID="C1txtZipcode" runat="server" Format="A9- " MaxLength="20" TabIndex="23">
                                                </wijmo:C1InputText>
                                            </td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>Contact No.</td>
                                            <td>
                                                <wijmo:C1InputText ID="C1txtMobile" runat="server" Format="9+-() " MaxLength="30" TabIndex="24">
                                                </wijmo:C1InputText>
                                            </td>
                                            <td>&nbsp;</td>
                                            <td>Date of Birth</td>
                                            <td>
                                                <wijmo:C1InputDate ID="C1DOB" runat="server" Date="06/06/2014 15:39:00" TabIndex="25">
                                                </wijmo:C1InputDate>
                                            </td>
                                            <td>
                                                <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator30" runat="server" ControlToValidate="C1DOB" Display="Dynamic" ErrorMessage="Enter Date of birth" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                <asp:CompareValidator CssClass="Validations" ID="CompareValidator6" runat="server" ControlToValidate="C1DOB" Display="Dynamic" ErrorMessage="Enter valid Date of birth" Operator="DataTypeCheck" Type="Date" ValidationGroup="save">*</asp:CompareValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style3"></td>
                                            <td class="auto-style3">Email </td>
                                            <td class="auto-style3">
                                                <wijmo:C1InputText ID="C1txtEmail" runat="server" MaxLength="50" TabIndex="26">
                                                </wijmo:C1InputText>
                                            </td>
                                            <td class="auto-style3">
                                                <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator26" runat="server" ControlToValidate="C1txtEmail" Display="Dynamic" ErrorMessage="Enter e-mail id" SetFocusOnError="true" ValidationGroup="save" Visible="False">*</asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator CssClass="Validations" ID="RegularExpressionValidator3" runat="server" ControlToValidate="C1txtEmail" ErrorMessage="Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="save">*</asp:RegularExpressionValidator>
                                            </td>
                                            <td class="auto-style3"></td>
                                            <td rowspan="4">
                                                <asp:Image ID="ImgProfilePic" runat="server" Height="110px" />
                                            </td>
                                            <td class="auto-style3">
                                                </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>SSN</td>
                                            <td>

                                                <wijmo:C1InputText ID="C1txtSSN" runat="server" Format="A9-" MaxLength="50" TabIndex="27">
                                                </wijmo:C1InputText>
                                            </td>
                                            <td>
                                                &nbsp;</td>
                                            <td></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>Blood Group</td>
                                            <td><wijmo:C1ComboBox ID="C1ddlBloodGroup" runat="server" ForceSelectionText="True" TabIndex="28">
                                                </wijmo:C1ComboBox></td>
                                            <td>
                                                <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator31" runat="server" ControlToValidate="C1ddlBloodGroup" Display="Dynamic" ErrorMessage="Select Blood Group" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator></td>
                                            <td></td>
                                            <td>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>Qualification</td>
                                            <td><wijmo:C1ComboBox ID="C1ddlQualification" runat="server" ForceSelectionText="True" TabIndex="29">
                                                </wijmo:C1ComboBox></td>
                                            <td>
                                                <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator33" runat="server" ControlToValidate="C1ddlQualification" Display="Dynamic" ErrorMessage="Select Qualification" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>

                                            </td>
                                            <td></td>
                                            <td>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>Speciality</td>
                                            <td>
                                                <wijmo:C1ComboBox ID="C1ddlSpecialist" runat="server" ForceSelectionText="True" TabIndex="30">
                                                </wijmo:C1ComboBox>

                                            </td>
                                            <td><asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator32" runat="server" ControlToValidate="C1ddlSpecialist" Display="Dynamic" ErrorMessage="Select Specialization of Doctor" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator></td>
                                            <td>&nbsp;</td>
                                            <td colspan="2">
                                                
                                                <asp:FileUpload ID="FileUpload1" runat="server" CssClass="FileUploader" TabIndex="31" />
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>
                                                &nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>
                                                &nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td id="msglbl">
                                                &nbsp;</td>
                                            <td id="msglbl" colspan="5">
                                                <asp:Label ID="LblMsgDialog" runat="server" CssClass="msglbl" Font-Bold="True"></asp:Label>
                                            </td>
                                            <td id="msglbl">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                            <td colspan="5">
                                                &nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                            <td colspan="5">
                                                <asp:Panel ID="PnlUpdateBtns" runat="server" HorizontalAlign="left">
                                                    <table align="left">
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="BtnSubmit" runat="server" OnClick="BtnSubmit_Click" TabIndex="32" Text="Submit" ValidationGroup="save" CssClass="myButton" />
                                                                <ajt:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure to update record?" TargetControlID="BtnSubmit">
                                                                </ajt:ConfirmButtonExtender>
                                                            </td>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="BtnReset" runat="server" OnClick="BtnReset_Click" TabIndex="33" Text="Cancel" Visible="True" CssClass="myButton" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:HiddenField ID="HFRegID" runat="server" />
                                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="False" ValidationGroup="save" />
                                            </td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </Content>
                        </wijmo:C1Dialog>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
    </Triggers>
    </asp:UpdatePanel>

</asp:Content>
