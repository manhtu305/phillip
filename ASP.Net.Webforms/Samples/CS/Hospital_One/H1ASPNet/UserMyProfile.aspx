﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UserDefaultMaster.Master" AutoEventWireup="true" CodeBehind="UserMyProfile.aspx.cs" Inherits="Grapecity.Samples.HospitalOne.H1ASPNet.UserMyProfile" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajt" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Upload" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>
<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1AutoComplete" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1GridView" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Dialog" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Wizard" tagprefix="wijmo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
                <tr>
                    <td>
                        <div class="container myprofileheading"><asp:Label ID="LblMyName" runat="server"></asp:Label></div>
                    </td>
                </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table>                
                <tr>
                    <td>
                        <asp:Label ID="LblMsg" runat="server" CssClass="msglbl" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="vertical-align: top;">
                                    <asp:Image ID="ImgProfilePic" runat="server" Width="150px" />
                                </td>
                                <td>
                                    <asp:Panel ID="PnlEdit" runat="server" width="100%">
                                        <table width="100%">                                           
                                            <tr>
                                                <td >&nbsp;</td>
                                                <td width="100px">Reg. Date</td>
                                                <td>
                                                    <wijmo:C1InputDate ID="C1RegDate" runat="server" Date="06/06/2014 15:39:00" TabIndex="1" ReadOnly="True">
                                                    </wijmo:C1InputDate>
                                                </td>
                                                <td width="100px">
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="C1RegDate" CssClass="Validations" Display="Dynamic" ErrorMessage="Select Registration Date" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="C1RegDate" CssClass="Validations" Display="Dynamic" ErrorMessage="Enter valid Reg. Date" Operator="DataTypeCheck" Type="Date" ValidationGroup="save">*</asp:CompareValidator>
                                                </td>
                                                <td width="100px">Login Name</td>
                                                <td>
                                                    <wijmo:C1InputText ID="txtLoginUserName" runat="server" Format="A9a." MaxLength="20" TabIndex="2" ReadOnly="True">
                                                    </wijmo:C1InputText>
                                                </td>
                                                <td >
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator36" runat="server" ControlToValidate="txtLoginUserName" CssClass="Validations" Display="Dynamic" ErrorMessage="Enter Login User Name" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>Name</td>
                                                <td>
                                                    <wijmo:C1InputText ID="txtContactPerson" runat="server" Format="Aa9 " MaxLength="50" TabIndex="3" ReadOnly="True">
                                                    </wijmo:C1InputText>
                                                </td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtContactPerson" CssClass="Validations" Display="Dynamic" ErrorMessage="Enter Contact Person" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>Address</td>
                                                <td rowspan="2">
                                                    <asp:TextBox ID="txtAddress" runat="server" CssClass="txtMultiline" Height="65px" MaxLength="250" TabIndex="6" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                                <td rowspan="2"></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td>Sex</td>
                                                <td>
                                                    <wijmo:C1ComboBox ID="C1ddlSex" runat="server" AutoComplete="False" ForceSelectionText="True" TabIndex="4" Enabled="False">
                                                    </wijmo:C1ComboBox>
                                                </td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ControlToValidate="C1ddlSex" CssClass="Validations" Display="Dynamic" ErrorMessage="Select Sex" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>Father&#39;s Name</td>
                                                <td>
                                                    <wijmo:C1InputText ID="txtFatherName" runat="server" Format="Aa9 " MaxLength="50" TabIndex="5">
                                                    </wijmo:C1InputText>
                                                </td>
                                                <td>&nbsp;</td>
                                                <td>Land Mark</td>
                                                <td>
                                                    <wijmo:C1InputText ID="txtLandMark" runat="server" MaxLength="50" TabIndex="7">
                                                    </wijmo:C1InputText>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>Country</td>
                                                <td>
                                                    <wijmo:C1ComboBox ID="C1ddlcountry" runat="server" AutoPostBack="True" ForceSelectionText="True" OnSelectedIndexChanged="C1ddlcountry_SelectedIndexChanged" TabIndex="8">
                                                    </wijmo:C1ComboBox>
                                                </td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator24" runat="server" ControlToValidate="C1ddlcountry" CssClass="Validations" Display="Dynamic" ErrorMessage="Select Country" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>State</td>
                                                <td>
                                                    <wijmo:C1ComboBox ID="C1ddlstate" runat="server" AutoPostBack="True" ForceSelectionText="True" OnSelectedIndexChanged="C1ddlstate_SelectedIndexChanged" TabIndex="9">
                                                    </wijmo:C1ComboBox>
                                                </td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ControlToValidate="C1ddlstate" CssClass="Validations" Display="Dynamic" ErrorMessage="Select State" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>City</td>
                                                <td>
                                                    <wijmo:C1ComboBox ID="C1ddlCity" runat="server" TabIndex="10">
                                                    </wijmo:C1ComboBox>
                                                </td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator37" runat="server" ControlToValidate="C1ddlCity" CssClass="Validations" Display="Dynamic" ErrorMessage="Select/Enter City" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>Zip Code</td>
                                                <td>
                                                    <wijmo:C1InputText ID="C1txtZipcode" runat="server" Format="A9- " MaxLength="20" TabIndex="11">
                                                    </wijmo:C1InputText>
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>Contact No.</td>
                                                <td>
                                                    <wijmo:C1InputText ID="C1txtMobile" runat="server" Format="9+-() " MaxLength="30" TabIndex="12">
                                                    </wijmo:C1InputText>
                                                </td>
                                                <td>&nbsp;</td>
                                                <td>Date of Birth</td>
                                                <td>
                                                    <wijmo:C1InputDate ID="C1DOB" runat="server" Date="06/06/2014 15:39:00" TabIndex="13">
                                                    </wijmo:C1InputDate>
                                                </td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ControlToValidate="C1DOB" CssClass="Validations" Display="Dynamic" ErrorMessage="Enter Date of birth" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                    <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="C1DOB" CssClass="Validations" Display="Dynamic" ErrorMessage="Enter valid Date of birth" Operator="DataTypeCheck" Type="Date" ValidationGroup="save">*</asp:CompareValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td ></td>
                                                <td >Email </td>
                                                <td >
                                                    <wijmo:C1InputText ID="C1txtEmail" runat="server" MaxLength="50" TabIndex="14">
                                                    </wijmo:C1InputText>
                                                </td>
                                                <td >
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ControlToValidate="C1txtEmail" CssClass="Validations" Display="Dynamic" ErrorMessage="Enter e-mail id" SetFocusOnError="true" ValidationGroup="save" Visible="False">*</asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="C1txtEmail" CssClass="Validations" ErrorMessage="Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="save">*</asp:RegularExpressionValidator>
                                                </td>
                                                <td >SSN</td>
                                                <td>
                                                    <wijmo:C1InputText ID="C1txtSSN" runat="server" Format="A9-" MaxLength="50" TabIndex="15">
                                                    </wijmo:C1InputText>
                                                </td>
                                                <td ></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>Blood Group</td>
                                                <td>
                                                    <wijmo:C1ComboBox ID="C1ddlBloodGroup" runat="server" ForceSelectionText="True" TabIndex="16">
                                                    </wijmo:C1ComboBox>
                                                </td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ControlToValidate="C1ddlBloodGroup" CssClass="Validations" Display="Dynamic" ErrorMessage="Select Blood Group" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>Qualification</td>
                                                <td>
                                                    <wijmo:C1ComboBox ID="C1ddlQualification" runat="server" ForceSelectionText="True" TabIndex="17">
                                                    </wijmo:C1ComboBox>
                                                </td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ControlToValidate="C1ddlQualification" CssClass="Validations" Display="Dynamic" ErrorMessage="Select Qualification" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>
                                                    <asp:Label ID="LblSpecialityDisease" runat="server" Text=""></asp:Label>
                                                </td>
                                                <td>
                                                    <wijmo:C1ComboBox ID="C1ddlSpecialist" runat="server" ForceSelectionText="True" TabIndex="18">
                                                    </wijmo:C1ComboBox>
                                                    <wijmo:C1ComboBox ID="C1ddlDisease" runat="server" ForceSelectionText="True" TabIndex="18">
                                                    </wijmo:C1ComboBox>
                                                </td>
                                                <td>
                                                    <asp:RequiredFieldValidator ID="RFVSpeciality" runat="server" ControlToValidate="C1ddlSpecialist" CssClass="Validations" Display="Dynamic" ErrorMessage="Select Specialization" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                    <asp:RequiredFieldValidator ID="RFVDisease" runat="server" ControlToValidate="C1ddlDisease" CssClass="Validations" Display="Dynamic" ErrorMessage="Select Disease" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                </td>
                                                <td>&nbsp;</td>
                                                <td colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td id="msglbl">&nbsp;</td>
                                                <td id="msglbl0" colspan="5">
                                                    <asp:Label ID="LblMsgDialog" runat="server" CssClass="msglbl" Font-Bold="True"></asp:Label>
                                                </td>
                                                <td id="msglbl1">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="5">
                                                    <asp:HiddenField ID="HFRegID" runat="server" />
                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                                                </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;width:150px">
                                    &nbsp;</td>
                                <td>
                                                    <asp:Panel ID="PnlUpdateBtns" runat="server" HorizontalAlign="Right">
                                                        <table align="Right">
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="BtnSubmit" runat="server" CssClass="myButton" OnClick="BtnSubmit_Click" TabIndex="19" Text="Submit" ValidationGroup="save" />
                                                                    <ajt:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" ConfirmText="Are you sure to edit/update your profile?" TargetControlID="BtnSubmit">
                                                                    </ajt:ConfirmButtonExtender>
                                                                </td>
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="BtnReset" runat="server" CssClass="myButton" OnClick="BtnReset_Click" TabIndex="33" Text="Cancel" Visible="True" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;</td>
                                                                <td>&nbsp;</td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                    
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
            </td>
        </tr>
    </table>    
    
</asp:Content>
