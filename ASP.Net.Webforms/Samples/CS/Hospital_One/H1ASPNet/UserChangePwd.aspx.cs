﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace Grapecity.Samples.HospitalOne.H1ASPNet
{
    public partial class UserChangePwd : System.Web.UI.Page
    {
        UIBinder UIBObj1 = new UIBinder();

        //To check page rights for current user
        protected void CheckPageRights()
        {
            if (Session["UserType_ID"] == null || Session["UserType"] == null || Session["User_ID"] == null)
            {
                Response.Redirect("Logout.aspx");
            }
            if (UIBObj1.HasRightsForShow(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                Session["Msg"] = "Sorry! You are not authorized to change your password.";
                Response.Redirect("UserShowMsg.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Label MPLblPageTitle = (Label)Master.FindControl("LblPageTitle");
                if (MPLblPageTitle != null)
                {
                    MPLblPageTitle.Text = "Change Password";
                }

                CheckPageRights();
                txtPass.Focus();
            }
        }

        //To redirect user dashboard/default page
        protected void cmdReset_Click(object sender, EventArgs e)
        {
            Server.Transfer("UserDashboard.aspx");
        }

        //To Change login password of current user
        protected void cmdSubmit_Click(object sender, EventArgs e)
        {
            LblMsg.Text = null;
            LblMsg.Text = UIBObj1.ChangeLoginPwd(Session["User_ID"].ToString(), txtPass, txtNewPass, txtCNewPass, Panel1);
        }

    }
}