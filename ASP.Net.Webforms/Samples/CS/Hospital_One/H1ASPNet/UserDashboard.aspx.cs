﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using Grapecity.Samples.HospitalOne.H1DBService_DBML;
using C1.Web.Wijmo.Controls.C1ComboBox;
using C1.Web.Wijmo.Controls.C1Carousel;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Data.Entity;
using System.Xml;
using System.Xml.Linq;
using System.Reflection;


namespace Grapecity.Samples.HospitalOne.H1ASPNet
{
    public partial class UserDashboard : System.Web.UI.Page
    {
        UIBinder UIBObj1 = new UIBinder();
        long Reg_ID = 0;

        //To check page rights for current user
        protected void CheckPageRights()
        {
            if (Session["UserType_ID"] == null || Session["UserType"] == null || Session["User_ID"] == null)
            {
                Response.Redirect("Logout.aspx");
            }
            if (Session["UserType_ID"].ToString() == "4")
            {
                Response.Redirect("UserPatientAppointmentCalender.aspx");
            }
            if (UIBObj1.HasRightsForShow(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                Session["Msg"] = "Sorry! You are not authorized to Add Appointments.";
                Response.Redirect("UserShowMsg.aspx");
            }            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckPageRights();
                if (!IsPostBack)
                {
                    SetPageLoad();
                }
            }
            catch (Exception ex)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Error occurred: " + ex.Message;
            }
        }

        //To set page for first time loading
        protected void SetPageLoad()
        {
            Label MPLblPageTitle = (Label)Master.FindControl("LblPageTitle");
            if (MPLblPageTitle != null)
            {
                MPLblPageTitle.Text = "My Dashboard";
            }
            PnlBackToPatientList.Visible = false;
            UIBObj1.BindUserForDashboard(C1ddlSearchName, 0, "1");
            PnlSearchList.Visible = false;
            PnlPatientList.Visible = false;
            
            if (Request.QueryString["DID"] != null && Request.QueryString["PID"] != null)
            {
                HFDoctorID.Value = Request.QueryString["DID"].ToString();
                string PatientId = Request.QueryString["PID"].ToString();
                PnlDoctorInfo.Visible = false;
                if (HFDoctorID.Value != "0")
                {
                    ImgDoctorPic.ImageUrl = "Soft_Data\\Users\\UserPics\\UPP_Default.png";
                    LblDoctorName.Text = "N/A";
                    LblDoctorQualification.Text = "N/A";
                    LblDoctorSpecialist.Text = "N/A";
                    LblDoctorCity.Text = "N/A";
                    List<PRC_GetUserListResult> Result1 = UIBObj1.GetUserList(long.Parse(HFDoctorID.Value), 2, "", null, null, "", "", 0, 0);
                    if (Result1.Count > 0)
                    {
                        ImgDoctorPic.ImageUrl = Result1[0].Img_URL;
                        LblDoctorName.Text = Result1[0].User_Name;
                        LblDoctorQualification.Text = Result1[0].Qualification;
                        LblDoctorSpecialist.Text = Result1[0].Specialist;
                        LblDoctorCity.Text = Result1[0].City;
                        PnlDoctorInfo.Visible = true;
                    }
                }
                if (PatientId != "0")
                {
                    PnlPatientList.Visible = true;
                    FillPatientInfo(long.Parse(PatientId));
                }
                else
                {
                    C1ddlSearchName.SelectedValue = HFDoctorID.Value;
                    SetForAddAppointment(long.Parse(HFDoctorID.Value));
                }

                if (Request.QueryString["CF"] != null)
                {
                    if (Request.QueryString["CF"].ToString() == "PL")
                    {
                        PnlBackToPatientList.Visible = true;
                        C1ddlSearchName.SelectedValue = PatientId;
                    }
                    else if (Request.QueryString["CF"].ToString() == "AP")
                    {
                        C1ddlSearchName.SelectedValue = PatientId;
                    }
                    else if (Request.QueryString["CF"].ToString() == "AD")
                    {
                        C1ddlSearchName.SelectedValue = HFDoctorID.Value;
                    }
                }
            }
            else
            {
                if (Session["UserType_ID"].ToString() == "2")
                {
                    C1ddlSearchName.SelectedValue = Session["User_ID"].ToString();
                    SetForAddAppointment(long.Parse(Session["User_ID"].ToString()));
                }
                else if (Session["UserType_ID"].ToString() == "3")
                {
                    C1ddlSearchName.SelectedValue = "102034";
                    SetForAddAppointment(102034);
                }
            }
        }

        //To fill patient list with today's appointments of selected doctor
        protected void FillPatientList(long Doctor_ID, long Patient_ID)
        {
            HFDoctorID.Value = Doctor_ID.ToString();
            PnlPatientInfo.Visible = false;
            C1CarouselPatientList.Items.Clear();
            if (Doctor_ID > 0)
            {
                if (ImgBtnPatientListView.ToolTip == "Calendar View")
                {
                    C1CarouselPatientList.Visible = false;
                    FillDoctorPatientListCalender(Doctor_ID);
                }
                else
                {
                    C1EventCalPatientList.Visible = false;
                    FillDoctorPatientListCarousel((UIBObj1.GetPatientAppointmentsList(0, 0, Doctor_ID, DateTime.Today, "", "").OrderBy(x => x.Appointment_DateTime).ToList()));
                }                
            }
        }

        //To fill carousel with patient list
        protected void FillDoctorPatientListCarousel(List<PRC_GetPatientAppointmentListResult> Result1)
        {
            int i = 0;
            C1CarouselPatientList.Items.Clear();
            if (HFDoctorID.Value != "0")
            {
                if (Result1.Count > 0)
                {
                    int index = 0;
                    int rowIndex = 0;
                    C1CarouselItem item = null;
                    Table table = null;
                    TableRow tRow = null;
                    for (i = 0; i < Result1.Count; i++)
                    {
                        if (rowIndex % 4 == 0)
                        {
                            item = new C1CarouselItem();
                            table = new Table();
                        }
                        if (index % 4 == 0)
                        {
                            tRow = new TableRow();
                            tRow.Style.Add(HtmlTextWriterStyle.TextAlign, "left");
                        }
                        index++;
                        HtmlGenericControl link = new HtmlGenericControl("a");

                        link.Attributes.Add("href", "UserDashboard.aspx?DID=" + HFDoctorID.Value + "&PID=" + Result1[i].User_ID + "&CF=PL");
                        link.Attributes.Add("id", Result1[i].User_ID.ToString());

                        HtmlGenericControl img = new HtmlGenericControl("img");
                        img.Attributes.Add("alt", Result1[i].User_Name + "-" + Result1[i].User_ID.ToString());
                        img.Attributes.Add("src", Result1[i].Img_URL);
                        img.Attributes["width"] = "70px";
                        img.Attributes["height"] = "70px";

                        HtmlGenericControl div1 = new HtmlGenericControl("div");
                        div1.InnerText = DateTime.Parse(Result1[i].Appointment_DateTime.ToString()).ToShortTimeString();
                        HtmlGenericControl div2 = new HtmlGenericControl("div");
                        div2.InnerText = Result1[i].User_ID.ToString();
                        HtmlGenericControl div3 = new HtmlGenericControl("div");
                        div3.InnerText = Result1[i].User_Name;
                        HtmlGenericControl div4 = new HtmlGenericControl("div");
                        div4.InnerText = DateTime.Parse(Result1[i].DOB.ToString()).ToShortDateString();
                        HtmlGenericControl div5 = new HtmlGenericControl("div");
                        div5.InnerText = Result1[i].Contact_No;

                        Table TableItem = new Table();
                        TableRow TableRowItem = new TableRow();
                        TableCell TableCellItem1 = new TableCell();
                        TableCellItem1.Style.Add(HtmlTextWriterStyle.TextAlign, HorizontalAlign.Center.ToString());
                        TableCellItem1.Controls.Add(img);
                        TableCellItem1.Style.Add(HtmlTextWriterStyle.VerticalAlign, VerticalAlign.Middle.ToString());

                        TableCell TableCellItem2 = new TableCell();
                        TableCellItem2.Controls.Add(div3);
                        TableCellItem2.Controls.Add(div5);
                        TableCellItem2.Controls.Add(div1);
                        TableCellItem2.Style.Add(HtmlTextWriterStyle.VerticalAlign, VerticalAlign.Middle.ToString());

                        TableRowItem.Cells.Add(TableCellItem1);
                        TableRowItem.Cells.Add(TableCellItem2);

                        TableItem.Rows.Add(TableRowItem);
                        TableItem.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                        TableItem.Style.Add(HtmlTextWriterStyle.BorderColor, "#1FB5EF");
                        TableItem.Style.Add(HtmlTextWriterStyle.BorderStyle, BorderStyle.Solid.ToString());
                        TableItem.Style.Add(HtmlTextWriterStyle.Width, "220px");
                        TableItem.Style.Add(HtmlTextWriterStyle.Height, "110px");

                        link.Controls.Add(TableItem);

                        TableCell tCell1 = new TableCell();
                        tCell1.Controls.Add(link);
                        tRow.Cells.Add(tCell1);

                        if (index % 4 == 0)
                        {
                            table.Rows.Add(tRow);
                            rowIndex++;
                        }
                        else if (index == Result1.Count() && (index % 4) != 0)
                        {
                            table.Rows.Add(tRow);
                        }

                        if (((rowIndex + 1) % 4 == 0) && (index % 4 == 0))
                        {
                            item.Controls.Add(table);
                            C1CarouselPatientList.Items.Add(item);
                        }
                        else if (index == Result1.Count() && (index + 1) % 16 != 0)
                        {
                            item.Controls.Add(table);
                            C1CarouselPatientList.Items.Add(item);
                        }
                    }
                }
                int Temp = (int)(Result1.Count / 4);
                if (Result1.Count % 4 != 0)
                {
                    Temp++;
                }
                Temp = Temp * 114;
                if (Temp > 114 * 4)
                {
                    Temp = 114 * 4;
                }
                Temp += 20;
                C1CarouselPatientList.Height = Temp;
                if (Result1.Count > 16)
                {
                    C1CarouselPatientList.ShowPager = true;
                }
                else
                {
                    C1CarouselPatientList.ShowPager = false;
                }
            }
            if (C1CarouselPatientList.Items.Count == 0)
            {
                C1CarouselPatientList.Visible = false;
                LblPatientListTitle.Text = "No Appointment available for today.";
            }
            else
            {
                C1CarouselPatientList.Visible = true;
                LblPatientListTitle.Text = "Today's Appointment: " + i.ToString();
            }
            LblNoOfApps.Text = i.ToString();
        }

        //To Fill C1EventCalendar with information of doctor's appointements
        protected void FillDoctorPatientListCalender(long Doctor_ID)
        {
            HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
            List<PRC_GetDoctorPatientListResult> Result1 = H1EFObj1.PRC_GetDoctorPatientList(Doctor_ID, null).ToList();
            if (Result1.Count > 0)
            {
                XDocument XdApp = XDocument.Load(Server.MapPath("c1evcaldata.xml"));
                XElement Appointments = XdApp.Descendants("Appointments").ToList()[0];
                Appointments.RemoveAll();
                foreach (PRC_GetDoctorPatientListResult row in Result1)
                {
                    XElement Appointment = new XElement("Appointment");
                    Guid appguid = Guid.NewGuid();
                    Appointment.Add(new XAttribute("Id", appguid.ToString()));
                    Appointment.Add(new XAttribute("index", "-1"));

                    XElement subject = new XElement("Subject");
                    subject.Value = row.User_Name + "-" + row.Disease_Name;
                    Appointment.Add(subject);

                    XElement AppDateTime = new XElement("Start");
                    AppDateTime.Value = row.GetType().GetProperty("Appointment_DateTime").GetValue(row).ToString();
                    Appointment.Add(AppDateTime);

                    Appointments.Add(Appointment);
                }
                string AppointmentsXML = XdApp.Root.ToString();
                string FileName = Server.MapPath("/Temp/doctorpatientlist.xml");
                if (File.Exists(FileName))
                {
                    File.Delete(FileName);
                }
                File.WriteAllText(FileName, AppointmentsXML);
                C1EventCalPatientList.SelectedDate = DateTime.Today;
                C1EventCalPatientList.DataStorage.Import(FileName, C1.Web.Wijmo.Controls.C1EventsCalendar.FileFormatEnum.XML);
                C1EventCalPatientList.DataStorage.SaveData();
                C1EventCalPatientList.Visible = true;
            }
            else
            {
                C1EventCalPatientList.Visible = false;
                LblMsg.ForeColor = System.Drawing.Color.Gray;
                LblMsg.Text = "No Appointment is available for selected doctor.";
            }
        }

        //To Search patient/doctor
        protected void ImgBtnSearchUserList_Click(object sender, ImageClickEventArgs e)
        {
            LblMsg.Text = null;
            C1DialogAddViewEditAppointment.ShowOnLoad = false;
            FillC1GVSearchList();
        }

        //To open detail of selected doctor/patient
        protected void C1ddlSearchName_SelectedIndexChanged(object sender, C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxEventArgs args)
        {
            PnlBackToPatientList.Visible = false;
            LblMsg.Text = null;
            C1DialogAddViewEditAppointment.ShowOnLoad = false;            
            HFDoctorID.Value = "0";
            FillC1GVSearchList();
        }

        //To fill searched result (doctors and patients) in a grid
        protected void FillC1GVSearchList()
        {
            List<PRC_GetUserListResult> Result1 = new List<PRC_GetUserListResult>();
            if (C1ddlSearchName.SelectedIndex >= 0)
            {
                C1GVSearchList.DataSource = null;
                C1GVSearchList.DataBind();
                Result1 = UIBObj1.GetUserList(long.Parse(C1ddlSearchName.SelectedValue), 0, "", null, null, "", "", 0, 0);
                if (Result1[0].UserType_ID == 4)
                {
                    PnlPatientList.Visible = true;
                    C1CarouselPatientList.Visible = false;
                    C1EventCalPatientList.Visible = false;
                    FillPatientInfo(Result1[0].User_ID);
                }
                else if (Result1[0].UserType_ID == 2)
                {
                    SetForAddAppointment(long.Parse(C1ddlSearchName.SelectedValue));
                }
            }
            else
            {
                Result1 = UIBObj1.GetUserList(0, 0, "", null, null, C1ddlSearchName.Text.Trim(), "", 0, 0);
                C1GVSearchList.DataSource = Result1;
                C1GVSearchList.DataBind();
                PnlPatientList.Visible = false;
                LblMsg.ForeColor = System.Drawing.Color.Gray;
                LblMsg.Text = "Found Results: " + Result1.Count.ToString();
            }            
            if (C1GVSearchList.Rows.Count > 0)
            {
                PnlSearchList.Visible = true;
                C1GVSearchList.Columns[1].Visible = false;
                C1GVSearchList.Columns[2].Visible = false;        
            }
            else
            {
                PnlSearchList.Visible = false;
            }            
        }

        //To Fill C1EventCalendar with information of patient's all appointements
        protected void FillPatientAppointmentsCalender(List<PRC_GetPatientAppointmentListResult> Result1)
        {
            if (Result1.Count > 0)
            {
                XDocument XdApp = XDocument.Load(Server.MapPath("c1evcaldata.xml"));
                XElement Appointments = XdApp.Descendants("Appointments").ToList()[0];
                Appointments.RemoveAll();
                foreach (PRC_GetPatientAppointmentListResult row in Result1)
                {
                    XElement Appointment = new XElement("Appointment");
                    Guid appguid = Guid.NewGuid();
                    Appointment.Add(new XAttribute("Id", appguid.ToString()));
                    Appointment.Add(new XAttribute("index", "-1"));

                    XElement subject = new XElement("Subject");
                    subject.Value = row.Disease_Name + "-" + row.Diagnosis_Status;
                    Appointment.Add(subject);

                    XElement AppDateTime = new XElement("Start");
                    AppDateTime.Value = row.GetType().GetProperty("Appointment_DateTime").GetValue(row).ToString();
                    Appointment.Add(AppDateTime);
                    Appointments.Add(Appointment);
                }
                string AppointmentsXML = XdApp.Root.ToString();
                string FileName = Server.MapPath("/Temp/patientoldappointments.xml");
                if (File.Exists(FileName))
                {
                    File.Delete(FileName);
                }
                File.WriteAllText(FileName, AppointmentsXML);
                C1EventCalPatientOldAppList.SelectedDate = DateTime.Today;
                C1EventCalPatientOldAppList.ViewType = C1.Web.Wijmo.Controls.C1EventsCalendar.C1EventsCalendarViewType.Month;
                C1EventCalPatientOldAppList.DataStorage.Import(FileName, C1.Web.Wijmo.Controls.C1EventsCalendar.FileFormatEnum.XML);
                C1EventCalPatientOldAppList.DataStorage.SaveData();
                C1EventCalPatientOldAppList.Visible = true;
            }
            else
            {
                C1EventCalPatientOldAppList.Visible = false;
            }
        }

        //To fill selected patient's details
        protected void FillPatientInfo(long Patient_ID)
        {
            PnlDoctorInfo.Visible = false;
            LblPatientListTitle.Text = "Patient Details";
            HFPatientID.Value = null;
            LblPatientID.Text = "N/A";
            LblRegDate.Text = "N/A";
            LblPatientName.Text = "N/A";
            LblPatientAge.Text = "N/A";
            LblPatientContactNo.Text = "N/A";
            HLinkPatientEmailid.Text = "N/A";
            HLinkPatientEmailid.NavigateUrl = "#";
            C1EventCalPatientOldAppList.Visible = false;

            List<PRC_GetPatientAppointmentListResult> Result1 = UIBObj1.GetPatientAppointmentsList(0, Patient_ID, long.Parse(HFDoctorID.Value), null, "", "");
            C1GVOldAppointments.DataSource = Result1;
            C1GVOldAppointments.DataBind();            
            if (Result1.Count > 0)
            {
                //Demographics Info
                ImgPatientPic.ImageUrl = Result1[0].Img_URL;
                HFPatientID.Value = Result1[0].User_ID.ToString();
                LblPatientID.Text = Result1[0].User_ID.ToString();
                LblRegDate.Text = DateTime.Parse(Result1[0].Reg_Date.ToString()).ToShortDateString();
                LblPatientName.Text = Result1[0].User_Name;
                LblPatientAge.Text = DateTime.Parse(Result1[0].DOB.ToString()).ToShortDateString();//UIBObj1.GetAge(Result1[0].DOB, DateTime.Today);
                LblPatientContactNo.Text = Result1[0].Contact_No;
                HLinkPatientEmailid.Text = Result1[0].Email_ID;
                HLinkPatientEmailid.NavigateUrl = "mailto:" + Result1[0].Email_ID;

                //Visit Info
                LblFirstVisitDate.Text = Result1[0].Appointment_DateTime.ToShortDateString();
                LblFirstVisitTime.Text = Result1[0].Appointment_DateTime.ToShortTimeString();
                LblAppDate.Text = Result1[Result1.Count - 1].Appointment_DateTime.ToShortDateString();
                LblApptime.Text = Result1[Result1.Count - 1].Appointment_DateTime.ToShortTimeString();

                //Vitals Info
                LblHeight.Text = Result1[0].Height.ToString();
                LblWeight.Text = Result1[0].Weight.ToString();
                LblTemperature.Text = Result1[0].Temperature.ToString();
                LblHeartRate.Text = Result1[0].Heart_Rate.ToString();
                LblSystolicBP.Text = Result1[0].Systolic_BP.ToString();
                LblDiastolicBP.Text = Result1[0].Diastolic_BP.ToString();
                LblDiagnosisStatus.Text = Result1[0].Diagnosis_Status;
                FillPatientAppointmentsCalender(Result1);
            }
            else
            {
                List<PRC_GetUserListResult> Result2 = UIBObj1.GetUserList(Patient_ID, 4, "", null, null, "", "", 0, 0);
                //Demographics Info
                ImgPatientPic.ImageUrl = Result2[0].Img_URL;
                HFPatientID.Value = Result2[0].User_ID.ToString();
                LblPatientID.Text = Result2[0].User_ID.ToString();
                LblRegDate.Text = DateTime.Parse(Result2[0].Reg_Date.ToString()).ToShortDateString();
                LblPatientName.Text = Result2[0].User_Name;
                LblPatientAge.Text = DateTime.Parse(Result2[0].DOB.ToString()).ToShortDateString();//UIBObj1.GetAge(Result1[0].DOB, DateTime.Today);
                LblPatientContactNo.Text = Result2[0].Contact_No;
                HLinkPatientEmailid.Text = Result2[0].Email_ID;
                HLinkPatientEmailid.NavigateUrl = "mailto:" + Result2[0].Email_ID;

                //Visit Info
                LblFirstVisitDate.Text = "N/A";
                LblFirstVisitTime.Text = "N/A";
                LblAppDate.Text = "N/A";
                LblApptime.Text = "N/A";

                //Vitals Info
                LblHeight.Text = "N/A";
                LblWeight.Text = "N/A";
                LblTemperature.Text = "N/A";
                LblHeartRate.Text = "N/A";
                LblSystolicBP.Text = "N/A";
                LblDiastolicBP.Text = "N/A";
                LblDiagnosisStatus.Text = "N/A";
            }
            PnlPatientInfo.Visible = true;
            C1TabPatientInfo_Basic.Focus();
            C1TabPatientInfo.Selected = 0;
        }

        //To fill full details of all appointments of selected patient
        protected void FillC1GVOldAppointments(long Patient_ID)
        {
            C1GVOldAppointments.DataSource = UIBObj1.GetPatientAppointmentsList(0, Patient_ID, long.Parse(HFDoctorID.Value), null, "", "");
            C1GVOldAppointments.DataBind();
            if (UIBObj1.HasRightsForShow(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                C1GVOldAppointments.Columns[0].Visible = false;
            }
            if (UIBObj1.HasRightsForUpdate(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID("UserViewOldAppointments.aspx")) != "1")
            {
                C1GVOldAppointments.Columns[1].Visible = false;
            }            
        }

        //To set controls for add/edit/view appointment
        protected void SetForAddAppointment(long User_ID)
        {
            HFDoctorID.Value = "0";
            if (User_ID != 0)
            {
                PnlSearchList.Visible = false;
                ImgDoctorPic.ImageUrl = "Soft_Data\\Users\\UserPics\\UPP_Default.png";
                LblDoctorName.Text = "N/A";
                LblDoctorQualification.Text = "N/A";
                LblDoctorSpecialist.Text = "N/A";
                LblDoctorCity.Text = "N/A";
                List<PRC_GetUserListResult> Result1 = UIBObj1.GetUserList(User_ID, 2, "", null, null, "", "", 0, 0);
                if (Result1.Count > 0)
                {
                    HFDoctorID.Value = User_ID.ToString();
                    ImgDoctorPic.ImageUrl = Result1[0].Img_URL;
                    LblDoctorName.Text = Result1[0].User_Name;
                    LblDoctorQualification.Text = Result1[0].Qualification;
                    LblDoctorSpecialist.Text = Result1[0].Specialist;
                    LblDoctorCity.Text = Result1[0].City;
                    PnlDoctorInfo.Visible = true;                    
                    ImgBtnPatientListView.ImageUrl = "~/images/Calender_View.png";
                    ImgBtnPatientListView.ToolTip = "Carousel View";
                    FillPatientList(User_ID, 0);
                    ImgBtnPatientListView.ToolTip = "Calendar View";
                }
                else
                {
                    List<PRC_GetUserListResult> Result2 = UIBObj1.GetUserList(User_ID, 4, "", null, null, "", "", 0, 0);
                    if (Result2.Count > 0)
                    {
                        PnlDoctorInfo.Visible = false;
                        C1CarouselPatientList.Visible = false;
                        Response.Redirect("UserDashboard.aspx?DID=0&PID=" + User_ID.ToString());
                    }
                    else
                    {
                        Session["Msg"] = "You have tried to perform invalid operation.";
                        Response.Redirect("UserShowMsg.aspx");
                    }
                }
                PnlPatientList.Visible = true;
            }
            else
            {
                Session["Msg"] = "You have tried to perform invalid operation.";
                Response.Redirect("UserShowMsg.aspx");
            }
        }

        //To handle click event of Add Appointment/Add Nurse button of C1GridView showing search result
        protected void ImgBtn_C1GVSearchList_AddAppointment_Click(object sender, ImageClickEventArgs e)
        {
            LblMsg.Text = null;
            C1DialogAddViewEditAppointment.ShowOnLoad = false;
            ImageButton LBtn1 = sender as ImageButton;
            if (LBtn1 != null)
            {
                string id = LBtn1.CommandArgument;
                List<PRC_GetUserListResult> Result1 = UIBObj1.GetUserList(long.Parse(id), 0, "", null, null, "", "", 0, 0);
                if (Result1[0].UserType_ID != 3)
                {
                    SetForAddAppointment(long.Parse(id));
                }
                else if (Result1[0].UserType_ID == 3 && UIBObj1.HasRightsForInsert(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID("UserAddNurse.aspx")) == "1")
                {
                    Response.Redirect("UserAddNurse.aspx");
                }
                else
                {
                    LblMsg.ForeColor = System.Drawing.Color.Red;
                    LblMsg.Text = "You are not authorized to add a new Nurse.";
                }
            }

        }

        //To handle click event of edit profile button of C1GridView showing search result
        protected void ImgBtn_C1GVSearchList_EditProfile_Click(object sender, ImageClickEventArgs e)
        {
            C1DialogAddViewEditAppointment.ShowOnLoad = false;
            ImageButton LBtn1 = sender as ImageButton;
            if (LBtn1 != null)
            {
                string id = LBtn1.CommandArgument;
                List<PRC_GetUserListResult> Result1 = UIBObj1.GetUserList(long.Parse(id), 0, "", null, null, "", "", 0, 0);
                if (Result1[0].UserType_ID == 2)
                {
                   //Doctor
                    if (UIBObj1.HasRightsForUpdate(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID("UserManageDoctor.aspx")) == "1")
                    {
                        Response.Redirect("UserManageDoctor.aspx?UID=" + id + "&OT=E");
                    }
                    else
                    {
                        LblMsg.ForeColor = System.Drawing.Color.Red;
                        LblMsg.Text = "You are not authorized to edit Doctors.";
                    }
                }
                else if (Result1[0].UserType_ID == 3)
                {
                    //Nurse
                    if (UIBObj1.HasRightsForUpdate(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID("UserManageNurse.aspx")) == "1")
                    {
                        Response.Redirect("UserManageNurse.aspx?UID=" + id + "&OT=E");
                    }
                    else
                    {
                        LblMsg.ForeColor = System.Drawing.Color.Red;
                        LblMsg.Text = "You are not authorized to edit Nurses.";
                    }                   
                }
                else if (Result1[0].UserType_ID == 4)
                {
                    //Patient
                    if (UIBObj1.HasRightsForUpdate(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID("UserManagePatient.aspx")) == "1")
                    {
                        Response.Redirect("UserManagePatient.aspx?UID=" + id + "&OT=E");
                    }
                    else
                    {
                        LblMsg.ForeColor = System.Drawing.Color.Red;
                        LblMsg.Text = "You are not authorized to edit Patients.";
                    }                    
                }
                else
                {
                    LblMsg.ForeColor = System.Drawing.Color.Red;
                    LblMsg.Text = "No operation to perform. Try Again.";
                }
            }
        }

        //To handle click event of view profile button of C1GridView showing search result
        protected void ImgBtn_C1GVSearchList_ViewProfile_Click(object sender, ImageClickEventArgs e)
        {
            C1DialogAddViewEditAppointment.ShowOnLoad = false;
            ImageButton LBtn1 = sender as ImageButton;
            if (LBtn1 != null)
            {
                string id = LBtn1.CommandArgument;
                List<PRC_GetUserListResult> Result1 = UIBObj1.GetUserList(long.Parse(id), 0, "", null, null, "", "", 0, 0);
                if (Result1[0].UserType_ID == 2)
                {
                    //Doctor
                    if (UIBObj1.HasRightsForShow(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID("UserManageDoctor.aspx")) == "1")
                    {
                        Response.Redirect("UserManageDoctor.aspx?UID=" + id + "&OT=V");
                    }
                    else
                    {
                        LblMsg.ForeColor = System.Drawing.Color.Red;
                        LblMsg.Text = "You are not authorized to view Doctor's details.";
                    }
                }
                else if (Result1[0].UserType_ID == 3)
                {
                    //Nurse
                    if (UIBObj1.HasRightsForShow(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID("UserManageNurse.aspx")) == "1")
                    {
                        Response.Redirect("UserManageNurse.aspx?UID=" + id + "&OT=V");
                    }
                    else
                    {
                        LblMsg.ForeColor = System.Drawing.Color.Red;
                        LblMsg.Text = "You are not authorized to view Nurse's details.";
                    }
                }
                else if (Result1[0].UserType_ID == 4)
                {
                    //Patient
                    if (UIBObj1.HasRightsForShow(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID("UserManagePatient.aspx")) == "1")
                    {
                        Response.Redirect("UserManagePatient.aspx?UID=" + id + "&OT=V");
                    }
                    else
                    {
                        LblMsg.ForeColor = System.Drawing.Color.Red;
                        LblMsg.Text = "You are not authorized to view Patient's details.";
                    }
                }
                else
                {
                    LblMsg.ForeColor = System.Drawing.Color.Red;
                    LblMsg.Text = "No operation to perform. Try Again.";
                }
            }
        }

        //To add a new appointment of patient associated with selected doctor, if any
        protected void ImgButtonAddNewAppointment_Click(object sender, ImageClickEventArgs e)
        {
            CreateAppointmentForPatient();
        }

        //To edit details of a selected appointment of patient
        protected void ImgBtn_C1GVOldAppointments_EditAppointment_Click(object sender, ImageClickEventArgs e)
        {
            LblMsg.Text = LblMsgDialog.Text = null;
            ImageButton LBtn1 = sender as ImageButton;
            if (LBtn1 != null)
            {
                long Doctor_ID = 0;
                string id = LBtn1.CommandArgument;
                if (HFPatientID.Value.Length > 0)
                {
                    if (HFDoctorID.Value.Length > 0)//if (PnlDoctorInfo.Visible && HFDoctorID.Value.Length > 0)
                    {
                        Doctor_ID = long.Parse(HFDoctorID.Value);
                    }
                    OpenAppointmentDialog("EDIT","Doctor", long.Parse(HFPatientID.Value), Doctor_ID, long.Parse(id));
                }
            }
        }

        //To view details of a selected appoinment of patient
        protected void ImgBtn_C1GVOldAppointments_ViewAppointment_Click(object sender, ImageClickEventArgs e)
        {
            LblMsg.Text = LblMsgDialog.Text = null;
            ImageButton LBtn1 = sender as ImageButton;
            if (LBtn1 != null)
            {
                long Doctor_ID = 0;
                string id = LBtn1.CommandArgument;
                if (HFPatientID.Value.Length > 0)
                {
                    if (HFDoctorID.Value.Length > 0)//if (PnlDoctorInfo.Visible && HFDoctorID.Value.Length > 0)
                    {
                        Doctor_ID = long.Parse(HFDoctorID.Value);
                    }
                    OpenAppointmentDialog("VIEW", "Doctor", long.Parse(HFPatientID.Value), Doctor_ID, long.Parse(id));
                }
            }
        }

        //To open dialog for add/edit/view appointment
        protected void OpenAppointmentDialog(string DialogType,string AssociateType, long Patient_ID, long Doctor_ID, long Appointment_ID)
        {
            if (AssociateType == "Doctor")
            {
                LblAssociateDoctorPatient.Text = "Associate Doctor";
                UIBObj1.BindDoctor(C1ComboAssociateDoctorPatient, 0, "1");
                RFVAssociateDoctorPatient.ErrorMessage = "Select Associate Doctor";
            }
            else
            {
                LblAssociateDoctorPatient.Text = "Associate Patient";
                UIBObj1.BindPatient(C1ComboAssociateDoctorPatient, 0, "1");
                RFVAssociateDoctorPatient.ErrorMessage = "Select Associate Patient";
            }
            UIBObj1.BindDiagnosisStatus(C1ComboDiagnosisStatus, 0, "1");
            HFAppointmentDialogType.Value = DialogType;
            HFAppointmentID.Value = Appointment_ID.ToString();
            HFAddAppPatientID.Value = Patient_ID.ToString();
            LblPatientNameAtAddApp.Text = LblPatientName.Text + " (" + HFAddAppPatientID.Value + ")";
            SetAppointmentDialog();
            if (DialogType == "NEW")
            {
                C1ComboAssociateDoctorPatient.SelectedValue = Doctor_ID.ToString();
            }
            C1DialogAddViewEditAppointment.Width = 870;
            C1DialogAddViewEditAppointment.ShowOnLoad = true;
        }

        //To set dialog and its contros for add/edit/view appointments
        protected void SetAppointmentDialog()
        {
            ClearAppointmentDialog();
            if (HFAppointmentDialogType.Value == "NEW")
            {
                C1DialogAddViewEditAppointment.Title = "Add Appointment";
                PnlAddAppointmentBtns.Visible = true;
                BtnSubmit.Text = "Add";
            }
            else
            {
                List<PRC_GetPatientAppointmentListResult> Result1 = UIBObj1.GetPatientAppointmentsList(long.Parse(HFAppointmentID.Value), 0, 0, null, "", "");
                if (Result1.Count > 0)
                {
                    C1ComboAssociateDoctorPatient.SelectedValue = Result1[0].Doctor_ID.ToString();
                    C1InpDateAppointmentSchedule.Date = Result1[0].Appointment_DateTime;
                    C1InNumHeight.Value = double.Parse(Result1[0].Height.ToString());
                    C1InNumWeight.Value = double.Parse(Result1[0].Weight.ToString());
                    C1InNumTemperature.Value = double.Parse(Result1[0].Temperature.ToString());
                    C1InNumHeartRate.Value = double.Parse(Result1[0].Heart_Rate.ToString());
                    C1InNumSysBP.Value = double.Parse(Result1[0].Systolic_BP.ToString());
                    C1InNumDiastBP.Value = double.Parse(Result1[0].Diastolic_BP.ToString());
                    C1ComboDiagnosisStatus.SelectedValue = Result1[0].Diagnosis_Status_ID.ToString();
                    TxtAppointmentNotes.Text = Result1[0].Notes;
                    if (HFAppointmentDialogType.Value == "EDIT")
                    {
                        C1DialogAddViewEditAppointment.Title = "Edit Appointment";
                        PnlAddAppointmentBtns.Visible = true;
                        BtnSubmit.Text = "Update";
                    }
                    if (HFAppointmentDialogType.Value == "VIEW")
                    {
                        C1DialogAddViewEditAppointment.Title = "View Appointment";
                        PnlAddAppointmentBtns.Visible = false;
                        BtnSubmit.Text = "";
                    }
                }
                else
                {
                    LblMsgDialog.Text = "No Appointment is selected. Please! select an Appointment to view/edit.";
                }
            }
        }

        protected void ClearAppointmentDialog()
        {
            C1InpDateAppointmentSchedule.Date = null;
            C1InNumHeight.Value = 0;
            C1InNumWeight.Value = 0;
            C1InNumTemperature.Value = 0;
            C1InNumHeartRate.Value = 0;
            C1InNumSysBP.Value = 0;
            C1InNumDiastBP.Value = 0;
            C1ComboDiagnosisStatus.SelectedIndex = 0;
            TxtAppointmentNotes.Text = null;
        }

       //To check whether appointment dialog data is valid for save/update or not
        protected bool IsValidForSaveUpdate()
        {
            LblMsgDialog.ForeColor = System.Drawing.Color.Red;
            bool RValue = false;
            if (!UIBObj1.IsValidDateTime(C1InpDateAppointmentSchedule.Date.ToString()))
            {
                LblMsgDialog.Text = "Please! Select a valid Appointment Schedule.";
                C1InpDateAppointmentSchedule.Focus();
                return RValue;
            }
            if (LblAssociateDoctorPatient.Text == "Associate Doctor")
            {
                if (UIBObj1.CheckAppointmentBeforeInsertUpdate(Reg_ID, 0, long.Parse(C1ComboAssociateDoctorPatient.SelectedValue), C1InpDateAppointmentSchedule.Date) > 0)
                {
                    LblMsgDialog.Text = "Requested Schedule of Doctor is not free. Please! Try another schedule.";
                    C1InpDateAppointmentSchedule.Focus();
                    return RValue;
                }
                if (UIBObj1.CheckAppointmentBeforeInsertUpdate(Reg_ID, long.Parse(HFAddAppPatientID.Value), 0, C1InpDateAppointmentSchedule.Date) > 0)
                {
                    LblMsgDialog.Text = "Requested Schedule of Patient is not free. Please! Try another schedule.";
                    C1InpDateAppointmentSchedule.Focus();
                    return RValue;
                }
            }
            else if (LblAssociateDoctorPatient.Text == "Associate Patient")
            {
                if (UIBObj1.CheckAppointmentBeforeInsertUpdate(Reg_ID, 0, long.Parse(HFDoctorID.Value), C1InpDateAppointmentSchedule.Date) > 0)
                {
                    LblMsgDialog.Text = "Requested Schedule of Doctor is not free. Please! Try another schedule.";
                    C1InpDateAppointmentSchedule.Focus();
                    return RValue;
                }
                if (UIBObj1.CheckAppointmentBeforeInsertUpdate(Reg_ID, long.Parse(C1ComboAssociateDoctorPatient.SelectedValue), 0, C1InpDateAppointmentSchedule.Date) > 0)
                {
                    LblMsgDialog.Text = "Requested Schedule of Patient is not free. Please! Try another schedule.";
                    C1InpDateAppointmentSchedule.Focus();
                    return RValue;
                }
            }            
            RValue = true;
            return RValue;
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                LblMsg.Text = LblMsgDialog.Text = null;
                Reg_ID = 0;
                //To Insert New Appointment Record
                if (BtnSubmit.Text == "Add")
                {
                    if (IsValidForSaveUpdate())
                    {
                        long Result = 0;
                        string ErrorMsg = "";
                        if (LblAssociateDoctorPatient.Text == "Associate Doctor")
                        {
                            UIBObj1.AppointmentInsertUpdate(0, long.Parse(HFAddAppPatientID.Value), long.Parse(C1ComboAssociateDoctorPatient.SelectedValue), (DateTime)C1InpDateAppointmentSchedule.Date, (DateTime)C1InpDateAppointmentSchedule.Date, C1InNumHeight.Value, C1InNumWeight.Value, C1InNumTemperature.Value, C1InNumHeartRate.Value, C1InNumSysBP.Value, C1InNumDiastBP.Value, long.Parse(C1ComboDiagnosisStatus.SelectedValue), TxtAppointmentNotes.Text.Trim(), long.Parse(Session["User_ID"].ToString()), "Active", out  Result, out  ErrorMsg);
                        }
                        else if (LblAssociateDoctorPatient.Text == "Associate Patient")
                        {
                            UIBObj1.AppointmentInsertUpdate(0, long.Parse(C1ComboAssociateDoctorPatient.SelectedValue), long.Parse(HFDoctorID.Value), C1InpDateAppointmentSchedule.Date, C1InpDateAppointmentSchedule.Date, C1InNumHeight.Value, C1InNumWeight.Value, C1InNumTemperature.Value, C1InNumHeartRate.Value, C1InNumSysBP.Value, C1InNumDiastBP.Value, long.Parse(C1ComboDiagnosisStatus.SelectedValue), TxtAppointmentNotes.Text.Trim(), long.Parse(Session["User_ID"].ToString()), "Active", out Result, out ErrorMsg);
                        }
                        Reg_ID = (long)Result;
                        if (Result > 0)
                        {
                            LblMsg.ForeColor = System.Drawing.Color.Gray;
                            LblMsg.Text = "Appointment added successfully.";
                            C1DialogAddViewEditAppointment.ShowOnLoad = false;
                            if (LblAssociateDoctorPatient.Text == "Associate Doctor")
                            {
                                FillPatientInfo(long.Parse(HFAddAppPatientID.Value));
                            }
                        }
                        else
                        {
                            LblMsg.ForeColor = System.Drawing.Color.Red;
                            LblMsgDialog.Text = "Error occurred: " + ErrorMsg;
                        }
                    }
                }
                //To Update an existing Appointment Record
                else if (BtnSubmit.Text == "Update")
                {
                    Reg_ID = long.Parse(HFAppointmentID.Value);
                    if (IsValidForSaveUpdate())
                    {
                        long Result = 0;
                        string ErrorMsg = "";
                        UIBObj1.AppointmentInsertUpdate(Reg_ID, long.Parse(HFAddAppPatientID.Value), long.Parse(C1ComboAssociateDoctorPatient.SelectedValue), C1InpDateAppointmentSchedule.Date, C1InpDateAppointmentSchedule.Date, C1InNumHeight.Value, C1InNumWeight.Value, C1InNumTemperature.Value, C1InNumHeartRate.Value, C1InNumSysBP.Value, C1InNumDiastBP.Value, long.Parse(C1ComboDiagnosisStatus.SelectedValue), TxtAppointmentNotes.Text.Trim(), long.Parse(Session["User_ID"].ToString()), "Active", out  Result, out  ErrorMsg);
                        if (Result == 0 && ErrorMsg=="N/A")
                        {
                            LblMsg.ForeColor = System.Drawing.Color.Gray;
                            LblMsg.Text = "Appointment updated successfully.";
                            C1DialogAddViewEditAppointment.ShowOnLoad = false;
                            FillPatientInfo(long.Parse(HFAddAppPatientID.Value));
                        }
                        else
                        {
                            LblMsg.ForeColor = System.Drawing.Color.Red;
                            LblMsgDialog.Text = "Error occurred: " + ErrorMsg;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LblMsgDialog.Text = ex.Message;
            }

            if (BtnSubmit.Text == "Add" && LblAssociateDoctorPatient.Text == "Associate Patient")
            {
                if (ImgBtnPatientListView.ToolTip == "Calendar View")
                {
                    ImgBtnPatientListView.ToolTip = "Carousel View";
                    FillPatientList(long.Parse(HFDoctorID.Value), 0);
                    ImgBtnPatientListView.ToolTip = "Calendar View";
                }
                else// if (ImgBtnPatientListView.ToolTip == "Carousel View")
                {
                    ImgBtnPatientListView.ToolTip = "Calendar View";
                    FillPatientList(long.Parse(HFDoctorID.Value), 0);
                    ImgBtnPatientListView.ToolTip = "Carousel View";
                }
            }
        }

        protected void BtnReset_Click(object sender, EventArgs e)
        {
            if (HFAppointmentDialogType.Value == "EDIT")
            {
                OpenAppointmentDialog("EDIT", "Doctor", long.Parse(HFPatientID.Value), 0, long.Parse(HFAppointmentID.Value));
            }
            else
            {
                ClearAppointmentDialog();
            }
            if (ImgBtnPatientListView.ToolTip == "Calendar View")
            {
                ImgBtnPatientListView.ToolTip = "Carousel View";
                FillPatientList(long.Parse(HFDoctorID.Value), 0);
                ImgBtnPatientListView.ToolTip = "Calendar View";
            }
            else// if (ImgBtnPatientListView.ToolTip == "Carousel View")
            {
                ImgBtnPatientListView.ToolTip = "Calendar View";
                FillPatientList(long.Parse(HFDoctorID.Value), 0);
                ImgBtnPatientListView.ToolTip = "Carousel View";
            }
        }

        //To handle page index changing event of C1Gridview of Search List
        protected void C1GVSearchList_PageIndexChanging(object sender, C1.Web.Wijmo.Controls.C1GridView.C1GridViewPageEventArgs e)
        {
            C1GVSearchList.PageIndex = e.NewPageIndex;
            FillC1GVSearchList();
        }

        //To handle page index changing event of C1Gridview of patient's appointments
        protected void C1GVOldAppointments_PageIndexChanging(object sender, C1.Web.Wijmo.Controls.C1GridView.C1GridViewPageEventArgs e)
        {
            C1GVOldAppointments.PageIndex = e.NewPageIndex;
            FillC1GVOldAppointments(long.Parse(HFPatientID.Value));
        }

        protected void C1GVSearchList_RowDataBound(object sender, C1.Web.Wijmo.Controls.C1GridView.C1GridViewRowEventArgs e)
        {
            if(e.Row.RowType==C1.Web.Wijmo.Controls.C1GridView.C1GridViewRowType.DataRow)
            {
                ImageButton IBtnTemp1 = (ImageButton)e.Row.FindControl("ImgBtn_C1GVSearchList_AddAppointment");
                string User_ID = IBtnTemp1.CommandArgument, UserType = e.Row.Cells[5].Text;
                if (UserType == "Doctor")
                {
                    IBtnTemp1.ToolTip = "View Appointments";
                }
                else if (UserType == "Nurse")
                {
                    IBtnTemp1.ToolTip = "Add New Nurse";
                }
                else if (UserType == "Patient")
                {
                    IBtnTemp1.ToolTip = "New Appointment";
                }
            }
        }

        //To add a new appointment of selected doctor
        protected void ImgButtonAddNewAppointment3_Click(object sender, ImageClickEventArgs e)
        {
            CreateAppointmentForDoctor();
        }

        //To handle view (carousel/calendar) of patient list of a selected doctor
        protected void ImgBtnPatientListView_Click(object sender, ImageClickEventArgs e)
        {
            PatientListViewChange();
        }

        protected void LBtnPatientListView_Click(object sender, EventArgs e)
        {

        }

        //To Create a new appointment of selected doctor
        protected void LBtnAddNewAppointment3_Click(object sender, EventArgs e)
        {
            CreateAppointmentForDoctor();
        }

        protected void CreateAppointmentForDoctor()
        {
            LblMsg.Text = LblMsgDialog.Text = null;
            long Doctor_ID = 0;
            if (HFDoctorID.Value.Length > 0)
            {
                Doctor_ID = long.Parse(HFDoctorID.Value);
            }
            OpenAppointmentDialog("NEW", "Patient", 0, Doctor_ID, 0);
            if (ImgBtnPatientListView.ToolTip == "Calendar View")
            {
                ImgBtnPatientListView.ToolTip = "Carousel View";
                FillPatientList(long.Parse(HFDoctorID.Value), 0);
                ImgBtnPatientListView.ToolTip = "Calendar View";
            }
        }

        protected void C1PatientListView_SelectedIndexChanged(object sender, C1ComboBoxEventArgs args)
        {
            PatientListViewChange();
        }

        protected void PatientListViewChange()
        {
            C1DialogAddViewEditAppointment.ShowOnLoad = false;
            FillPatientList(long.Parse(HFDoctorID.Value), 0);
            if (ImgBtnPatientListView.ToolTip == "Calendar View")
            {
                ImgBtnPatientListView.ImageUrl = "~/images/Carousel_View.png";
                ImgBtnPatientListView.ToolTip = "Carousel View";
            }
            else if (ImgBtnPatientListView.ToolTip == "Carousel View")
            {

                ImgBtnPatientListView.ImageUrl = "~/images/Calender_View.png";
                ImgBtnPatientListView.ToolTip = "Calendar View";
            }
        }

        protected void CreateAppointmentForPatient()
        {
            LblMsg.Text = LblMsgDialog.Text = null;
            long Doctor_ID = 0;
            if (HFPatientID.Value.Length > 0)
            {
                if (HFDoctorID.Value.Length > 0)//if (PnlDoctorInfo.Visible && HFDoctorID.Value.Length > 0)
                {
                    Doctor_ID = long.Parse(HFDoctorID.Value);
                }
                OpenAppointmentDialog("NEW", "Doctor", long.Parse(HFPatientID.Value), Doctor_ID, 0);
            }

        }
        protected void LBtnAddNewAppointment2_Click(object sender, EventArgs e)
        {
            CreateAppointmentForPatient();
        }

        protected void ImgBtnBackToPatientList_Click(object sender, ImageClickEventArgs e)
        {
            BackToPatientList();
        }

        protected void LBtnBackToPatientList_Click(object sender, EventArgs e)
        {
            BackToPatientList();
        }

        //To redirect to patient list of doctor
        protected void BackToPatientList()
        {
            if (HFDoctorID.Value != "0")
            {
                Response.Redirect("UserDashboard.aspx?DID=" + HFDoctorID.Value + "&PID=0");
            }
        }
    }
}