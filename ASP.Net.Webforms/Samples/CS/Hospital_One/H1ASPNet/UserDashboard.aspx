﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UserDefaultMaster.Master" AutoEventWireup="true" CodeBehind="UserDashboard.aspx.cs" Inherits="Grapecity.Samples.HospitalOne.H1ASPNet.UserDashboard" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1EventsCalendar" TagPrefix="cc1" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1AutoComplete" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Input" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1ComboBox" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Calendar" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1GridView" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Upload" tagprefix="wijmo1" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Tabs" tagprefix="wijmo" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Dialog" tagprefix="wijmo" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="ajt" %>
<%@ Register assembly="C1.Web.Wijmo.Controls.4" namespace="C1.Web.Wijmo.Controls.C1Carousel" tagprefix="wijmo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">   
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        </style>   
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function goBack() {
            window.history.back()
        }
    </script>
          <table >              
              <tr>
                  <td>
                      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                          <ContentTemplate>
                              <table>
                                  <tr>
                                      <td>
                                          &nbsp;</td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <table>
                                              <tr>
                                                  <td>
                                                        <wijmo:C1ComboBox ID="C1ddlSearchName" runat="server" AutoPostBack="True" OnSelectedIndexChanged="C1ddlSearchName_SelectedIndexChanged" TabIndex="1">
                                                      </wijmo:C1ComboBox>
                                                  </td>
                                                  <td>
                                                      &nbsp;&nbsp;<asp:ImageButton ID="ImgBtnSearchUserList" runat="server" ImageUrl="~/images/Search_Image.png" OnClick="ImgBtnSearchUserList_Click" TabIndex="2" />
                                                  </td>
                                                  <td>
                                                       
                                                    </td>
                                                  <td>&nbsp;</td>
                                              </tr>
                                          </table>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <asp:Label ID="LblMsg" runat="server" CssClass="msglbl" Font-Bold="True"></asp:Label>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <asp:Panel ID="PnlSearchList" runat="server" Visible="False">
                                              <wijmo:C1GridView ID="C1GVSearchList" runat="server" AllowPaging="True" AutogenerateColumns="False" OnPageIndexChanging="C1GVSearchList_PageIndexChanging" OnRowDataBound="C1GVSearchList_RowDataBound" Width="900px" TabIndex="3">
                                                  <PagerSettings Mode="NextPreviousFirstLast" />
                                                  <CallbackSettings Action="Sorting" />
                                                  <Columns>
                                                      <wijmo:C1TemplateField Width="30px">
                                                          <ItemTemplate>
                                                              <asp:ImageButton ID="ImgBtn_C1GVSearchList_AddAppointment" runat="server" CommandArgument='<%# Eval("User_ID") %>' Height="15" ImageUrl="~/images/Plus_Image.png" OnClick="ImgBtn_C1GVSearchList_AddAppointment_Click" ToolTip="Add New Appointment" />
                                                          </ItemTemplate>
                                                          <ControlStyle HorizontalAlign="Center" />
                                                          <FooterStyle HorizontalAlign="Center" />
                                                          <HeaderStyle HorizontalAlign="Center" />
                                                          <ItemStyle HorizontalAlign="Center"/>
                                                      </wijmo:C1TemplateField>
                                                      <wijmo:C1TemplateField Width="30px">
                                                          <ItemTemplate>
                                                              <asp:ImageButton ID="ImgBtn_C1GVSearchList_EditProfile" runat="server" CommandArgument='<%# Eval("User_ID") %>' Height="15" ImageUrl="~/images/Edit_Image.png" OnClick="ImgBtn_C1GVSearchList_EditProfile_Click" ToolTip="Edit Profile" />
                                                          </ItemTemplate>
                                                          <ControlStyle HorizontalAlign="Center" />
                                                          <FooterStyle HorizontalAlign="Center" />
                                                          <HeaderStyle HorizontalAlign="Center" />
                                                          <ItemStyle HorizontalAlign="Center"/>
                                                      </wijmo:C1TemplateField>
                                                      <wijmo:C1TemplateField Width="30px">
                                                          <ItemTemplate>
                                                              <asp:ImageButton ID="ImgBtn_C1GVSearchList_ViewProfile" runat="server" CommandArgument='<%# Eval("User_ID") %>' Height="15" ImageUrl="~/images/View_Image.png" OnClick="ImgBtn_C1GVSearchList_ViewProfile_Click" ToolTip="View Profile" />
                                                          </ItemTemplate>
                                                          <ControlStyle HorizontalAlign="Center" />
                                                          <FooterStyle HorizontalAlign="Center" />
                                                          <HeaderStyle HorizontalAlign="Center" />
                                                          <ItemStyle HorizontalAlign="Center"/>
                                                      </wijmo:C1TemplateField>
                                                      <wijmo:C1BoundField DataField="User_ID" HeaderText="Reg. No" SortExpression="User_ID" Width="120px" FilterOperator="Equals" DataFormatString="d" NullDisplayText="N/A" ReadOnly="True">
                                                          <ControlStyle HorizontalAlign="Left"/>
                                                          <FooterStyle HorizontalAlign="Left" />
                                                          <HeaderStyle HorizontalAlign="Left" />
                                                          <ItemStyle HorizontalAlign="Left" />
                                                      </wijmo:C1BoundField>
                                                      <wijmo:C1BoundField DataField="User_Name" HeaderText="Name" SortExpression="User_Name"  Width="550px" FilterOperator="Contains">
                                                          <ControlStyle HorizontalAlign="Left"/>
                                                          <FooterStyle HorizontalAlign="Left" />
                                                          <HeaderStyle HorizontalAlign="Left" />
                                                          <ItemStyle HorizontalAlign="Left" />
                                                      </wijmo:C1BoundField>
                                                      <wijmo:C1BoundField DataField="UserType" HeaderText="User Type" SortExpression="UserType" Width="200px" FilterOperator="Contains">
                                                          <ControlStyle HorizontalAlign="Left"/>
                                                          <FooterStyle HorizontalAlign="Left" />
                                                          <HeaderStyle HorizontalAlign="Left" />
                                                          <ItemStyle HorizontalAlign="Left" />
                                                      </wijmo:C1BoundField>
                                                  </Columns>
                                              </wijmo:C1GridView>
                                          </asp:Panel>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <asp:Panel ID="PnlPatientList" runat="server" Visible="False" Width="900px">
                                              <table style="width:900px">
                                                  <tr>
                                                      <td>
                                                          <asp:Panel ID="PnlDoctorInfo" runat="server">
                                                              
                                                              <div class="userdetails">                                                              
                                                                  <ul>                                                              
                                                                      <li><asp:Image ID="ImgDoctorPic" runat="server" Height="120px" Width="120px" /></li>
                                                                      <li>Name: <asp:Label ID="LblDoctorName" runat="server"></asp:Label></li>
                                                                      <li>Speciality: <asp:Label ID="LblDoctorSpecialist" runat="server"></asp:Label></li>
                                                                      <li>Qualification: <asp:Label ID="LblDoctorQualification" runat="server"></asp:Label></li>
                                                                      <li>City: <asp:Label ID="LblDoctorCity" runat="server"></asp:Label></li>
                                                                  </ul>
                                                              </div>
                                                                            <div class="modulecircle" id="createAppointments">
                                                                                <asp:ImageButton ID="ImgButtonAddNewAppointment3" runat="server" ImageUrl="~/images/appointments.png" OnClick="ImgButtonAddNewAppointment3_Click" TabIndex="8" ToolTip="Add New Appointment" width="48" height="48" AlternateText="Alt Text"/>
                                                                                <br>                                                                                
                                                                                <asp:LinkButton ID="LBtnAddNewAppointment3" runat="server" OnClick="LBtnAddNewAppointment3_Click" TabIndex="8" ToolTip="Add New Appointment">Create Appointment</asp:LinkButton>
                                                                            </div>                                                                            
                                                                            <div class="modulecircle" id="todaysAppointments">
                                                                                <asp:Label ID="LblNoOfApps" runat="server" Text="0"></asp:Label><br>
                                                                                <a href="#">Today's<br>Appointments</a></div>
                                                                <div class="viewholder">
                                                                    Patient List View: 
                                                                    <asp:ImageButton ID="ImgBtnPatientListView" runat="server" ImageUrl="~/images/Calender_View.png" OnClick="ImgBtnPatientListView_Click" TabIndex="8" Visible="false"/>
                                                                    <wijmo:C1ComboBox ID="C1PatientListView" runat="server" AutoComplete="False" AutoFilter="False" AutoPostBack="True" OnSelectedIndexChanged ="C1PatientListView_SelectedIndexChanged" Width="160px">
                                                                        <ShowingAnimation Option="">
                                                                        </ShowingAnimation>
                                                                        <HidingAnimation Option="">
                                                                        </HidingAnimation>
                                                                        <Items>
                                                                            <wijmo:C1ComboBoxItem runat="server" Text="Carousel View" Value="Carousel View" Selected="true"/>
                                                                            <wijmo:C1ComboBoxItem runat="server" Text="Calendar View" Value="Calendar View" />
                                                                        </Items>
                                                                    </wijmo:C1ComboBox>
                                                                </div>
                                                              <asp:HiddenField ID="HFDoctorID" runat="server" Value="0" />                                                              
                                                          </asp:Panel>
                                                      </td>
                                                  </tr>                                                  
                                                  <tr>
                                                      <td>
                                                          <asp:Label ID="LblPatientListTitle" runat="server" ForeColor="White" Font-Bold="True" Visible="false"></asp:Label>
                                                          <asp:Panel ID="Panel1" runat="server">
                                                              <table class="auto-style1">
                                                                  <tr>
                                                                      <td>
                                                                          <wijmo:C1Carousel ID="C1CarouselPatientList" runat="server" Height="50px" TabIndex="5" Visible="False" Width="900px">
                                                                              <PagerPosition>
                                                                                  <My Left="Right" />
                                                                                  <At Left="Right" Top="Bottom" />
                                                                              </PagerPosition>
                                                                              <ControlPosition>
                                                                                  <At Left="Center" Top="Center" />
                                                                              </ControlPosition>
                                                                              <Animation Option="">
                                                                              </Animation>
                                                                          </wijmo:C1Carousel>
                                                                          <cc1:C1EventsCalendar ID="C1EventCalPatientList" runat="server" Width="900px" Visible="False">
                                                                          </cc1:C1EventsCalendar>
                                                                      </td>
                                                                  </tr>
                                                              </table>
                                                          </asp:Panel>
                                                      </td>
                                                  </tr>
                                                  <tr>
                                                      <td>
                                                          <asp:Panel ID="PnlPatientInfo" runat="server">

                                                                      <div class="userdetails"><b> Patient's Demographics Information</b>
                                                                        <ul>
                                                                            <li>
                                                                                <asp:Image ID="ImgPatientPic" runat="server" ImageUrl="~/Soft_Data/Users/UserPics/UPP_Default.png" Width="140px" Height="140px" />
                                                                            </li>
                                                                            <li>
                                                                                Reg. No.: <asp:Label ID="LblPatientID" runat="server"></asp:Label>
                                                                            </li>
                                                                            <li>
                                                                                Reg. Date: <asp:Label ID="LblRegDate" runat="server"></asp:Label>
                                                                            </li>
                                                                            <li>
                                                                                Name: <asp:Label ID="LblPatientName" runat="server"></asp:Label>
                                                                            </li>
                                                                            <li>
                                                                                Age/DOB: <asp:Label ID="LblPatientAge" runat="server"></asp:Label>
                                                                            </li>
                                                                            <li>
                                                                                Contact Nos.: <asp:Label ID="LblPatientContactNo" runat="server"></asp:Label>
                                                                            </li>
                                                                            <li>
                                                                                Email: <asp:HyperLink ID="HLinkPatientEmailid" runat="server"></asp:HyperLink>
                                                                            </li>
                                                                            </ul>
                                                                          </div>

                                                                        
                                                                        <div class="modulecircle" id="createAppointments2">
                                                                            <asp:ImageButton ID="ImgButtonAddNewAppointment2" runat="server" ImageUrl="~/images/appointments.png" OnClick="ImgButtonAddNewAppointment_Click" TabIndex="7" ToolTip="Add New Appointment" width="48" height="48"/>
                                                                            <br />
                                                                            <asp:LinkButton ID="LBtnAddNewAppointment2" runat="server" OnClick="LBtnAddNewAppointment2_Click" TabIndex="7" ToolTip="Add New Appointment">Create Appointment</asp:LinkButton>
                                                                        </div>

                                                                        <asp:Panel ID="PnlBackToPatientList" runat="server">
                                                                            <div class="modulecircle" id="backtopatient">                                                                              
                                                                              <asp:ImageButton ID="ImgBtnBackToPatientList" runat="server" ImageUrl="~/images/back.png" OnClick="ImgBtnBackToPatientList_Click" TabIndex="8" ToolTip="Back to Appointments List" width="48" height="48"/>
                                                                              <br />
                                                                              <asp:LinkButton ID="LBtnBackToPatientList" runat="server" OnClick="LBtnBackToPatientList_Click" TabIndex="8" ToolTip="Back to Appointments List">Back to Appointments List</asp:LinkButton>
                                                                            </div>
                                                                       </asp:Panel>
                                                                                  
                                                              <div>
                                                                <br />  
                                                              <table style="border: 1px solid #1FB5EF;">
                                                              <tr>
                                                                  <td style="vertical-align:top;">&nbsp;</td>
                                                                  <td style="vertical-align:top;" colspan="2">
                                                                      <wijmo:C1Tabs ID="C1TabPatientInfo" runat="server" Selected="1" TabIndex="6" Width="900px">
                                                                          <HideOption Blind="True" />
                                                                          <pages>
                                                                              <wijmo:C1TabPage ID="C1TabPatientInfo_Basic" runat="server" Direction="NotSet" StaticKey="" Text="Patient Info">
                                                                                  <table style="text-align:left;width:95%">
                                                                                      <tr>
                                                                                          <td colspan="2"><strong>Visit Information
                                                                                              <br />
                                                                                              </strong></td>
                                                                                          <td>&nbsp;</td>
                                                                                          <td>&nbsp;</td>
                                                                                          <td>&nbsp;</td>
                                                                                      </tr>
                                                                                      <tr>
                                                                                          <td style="width: 150px">First Visit Date:</td>
                                                                                          <td>
                                                                                              <asp:Label ID="LblFirstVisitDate" runat="server"></asp:Label>
                                                                                          </td>
                                                                                          <td>&nbsp;</td>
                                                                                          <td style="width: 150px">First Visit Time:</td>
                                                                                          <td><asp:Label ID="LblFirstVisitTime" runat="server"></asp:Label></td>
                                                                                      </tr>
                                                                                      <tr>
                                                                                          <td>Appointment Date:</td>
                                                                                          <td>
                                                                                              <asp:Label ID="LblAppDate" runat="server"></asp:Label>
                                                                                          </td>
                                                                                          <td>&nbsp;</td>
                                                                                          <td>Appointment Time:</td>
                                                                                          <td><asp:Label ID="LblApptime" runat="server"></asp:Label></td>
                                                                                      </tr>
                                                                                      <tr>
                                                                                          <td></td>
                                                                                          <td>
                                                                                              
                                                                                          </td>
                                                                                          <td>&nbsp;</td>
                                                                                          <td>&nbsp;</td>
                                                                                          <td>&nbsp;</td>
                                                                                      </tr>
                                                                                      <tr>
                                                                                          <td></td>
                                                                                          <td></td>
                                                                                          <td>&nbsp;</td>
                                                                                          <td>&nbsp;</td>
                                                                                          <td>&nbsp;</td>
                                                                                      </tr>
                                                                                      <tr>
                                                                                          <td colspan="2"><strong>Vitals Information<br /></strong></td>
                                                                                          <td>&nbsp;</td>
                                                                                          <td>&nbsp;</td>
                                                                                          <td>&nbsp;</td>
                                                                                      </tr>
                                                                                      <tr>
                                                                                          <td>Height</td>
                                                                                          <td>
                                                                                              
                                                                                              <asp:Label ID="LblHeight" runat="server"></asp:Label>
                                                                                              
                                                                                          </td>
                                                                                          <td>&nbsp;</td>
                                                                                          <td>Diastolic BP:</td>
                                                                                          <td>
                                                                                              <asp:Label ID="LblDiastolicBP" runat="server"></asp:Label>
                                                                                          </td>
                                                                                      </tr>
                                                                                      <tr>
                                                                                          <td>Weight:</td>
                                                                                          <td>
                                                                                              <asp:Label ID="LblWeight" runat="server"></asp:Label>
                                                                                          </td>
                                                                                          <td>&nbsp;</td>
                                                                                          <td>Systolic BP:</td>
                                                                                          <td>
                                                                                              <asp:Label ID="LblSystolicBP" runat="server"></asp:Label>
                                                                                          </td>
                                                                                      </tr>
                                                                                      <tr>
                                                                                          <td>Heart Rate:</td>
                                                                                          <td>
                                                                                              <asp:Label ID="LblHeartRate" runat="server"></asp:Label>
                                                                                          </td>
                                                                                          <td>&nbsp;</td>
                                                                                          <td>Diagnosis Status:</td>
                                                                                          <td><asp:Label ID="LblDiagnosisStatus" runat="server"></asp:Label></td>
                                                                                      </tr>
                                                                                      <tr>
                                                                                          <td>Body Temperature:</td>
                                                                                          <td>
                                                                                              <asp:Label ID="LblTemperature" runat="server"></asp:Label>
                                                                                          </td>
                                                                                          <td></td>
                                                                                          <td></td>
                                                                                          <td></td>
                                                                                      </tr>
                                                                                      <tr>
                                                                                          <td></td>
                                                                                          <td>
                                                                                              
                                                                                          </td>
                                                                                          <td>&nbsp;</td>
                                                                                          <td>&nbsp;</td>
                                                                                          <td>&nbsp;</td>
                                                                                      </tr>
                                                                                  </table>
                                                                                  <asp:HiddenField ID="HFPatientID" runat="server" />
                                                                              </wijmo:C1TabPage>
                                                                              <wijmo:C1TabPage ID="C1TabPatientInfo_Appointments" runat="server" StaticKey="" Text="Appointments">
                                                                                  <table>
                                                                                      <tr>
                                                                                          <td>
                                                                                              <asp:ImageButton ID="ImgButtonAddNewAppointment" runat="server" ImageUrl="~/images/NewAppointment_Image.png" OnClick="ImgButtonAddNewAppointment_Click" TabIndex="8" ToolTip="Add New Appointment" Visible="False" />
                                                                                          </td>
                                                                                      </tr>
                                                                                      <tr>
                                                                                          <td>
                                                                                              <wijmo:C1GridView ID="C1GVOldAppointments" runat="server" AllowPaging="True" AutogenerateColumns="False" OnPageIndexChanging="C1GVOldAppointments_PageIndexChanging" PageSize="5" TabIndex="9" Width="800px">
                                                                                                  <PagerSettings Mode="NextPreviousFirstLast" />
                                                                                                  <CallbackSettings Action="Sorting" />
                                                                                                  <Columns>
                                                                                                      <wijmo:C1TemplateField Width="30px">
                                                                                                          <ItemTemplate>
                                                                                                              <asp:ImageButton ID="ImgBtn_C1GVOldAppointments_ViewAppointment" runat="server" CommandArgument='<%# Eval("Appointment_ID") %>' Height="15px" ImageUrl="~/images/View_Image.png" OnClick="ImgBtn_C1GVOldAppointments_ViewAppointment_Click" ToolTip="View Appointment" />
                                                                                                          </ItemTemplate>
                                                                                                          <ControlStyle HorizontalAlign="Center" />
                                                                                                          <FooterStyle HorizontalAlign="Center" />
                                                                                                          <HeaderStyle HorizontalAlign="Center" />
                                                                                                          <ItemStyle HorizontalAlign="Center" />
                                                                                                      </wijmo:C1TemplateField>
                                                                                                      <wijmo:C1TemplateField Width="30px">
                                                                                                          <ItemTemplate>
                                                                                                              <asp:ImageButton ID="ImgBtn_C1GVOldAppointments_EditAppointment" runat="server" CommandArgument='<%# Eval("Appointment_ID") %>' Height="15px" ImageUrl="~/images/Edit_Image.png" OnClick="ImgBtn_C1GVOldAppointments_EditAppointment_Click" ToolTip="Edit Appointment" />
                                                                                                          </ItemTemplate>
                                                                                                          <ControlStyle HorizontalAlign="Center" />
                                                                                                          <FooterStyle HorizontalAlign="Center" />
                                                                                                          <HeaderStyle HorizontalAlign="Center" />
                                                                                                          <ItemStyle HorizontalAlign="Center" />
                                                                                                      </wijmo:C1TemplateField>
                                                                                                      <wijmo:C1BoundField DataField="Appointment_ID" HeaderText="App.No." SortExpression="Appointment_ID" Width="120px" DataFormatString="d" NullDisplayText="N/A">
                                                                                                          <ControlStyle HorizontalAlign="Left" />
                                                                                                          <FooterStyle HorizontalAlign="Left" />
                                                                                                          <HeaderStyle HorizontalAlign="Left" />
                                                                                                          <ItemStyle HorizontalAlign="Left" />
                                                                                                      </wijmo:C1BoundField>
                                                                                                      <wijmo:C1BoundField DataField="Doctor_Name" HeaderText="Doctor" SortExpression="Doctor_Name">
                                                                                                          <ControlStyle HorizontalAlign="Left" />
                                                                                                          <FooterStyle HorizontalAlign="Left" />
                                                                                                          <HeaderStyle HorizontalAlign="Left" />
                                                                                                          <ItemStyle HorizontalAlign="Left" />
                                                                                                      </wijmo:C1BoundField>
                                                                                                      <wijmo:C1BoundField DataField="Appointment_DateTime" HeaderText="Schedule" SortExpression="Appointment_DateTime" Width="120px" DataFormatString="d" NullDisplayText="N/A">
                                                                                                          <ControlStyle HorizontalAlign="Left" />
                                                                                                          <FooterStyle HorizontalAlign="Left" />
                                                                                                          <HeaderStyle HorizontalAlign="Left" />
                                                                                                          <ItemStyle HorizontalAlign="Left" />
                                                                                                      </wijmo:C1BoundField>
                                                                                                      <wijmo:C1BoundField DataField="Diagnosis_Status" HeaderText="Status" SortExpression="Diagnosis_Status" Width="150px">
                                                                                                          <ControlStyle HorizontalAlign="Left" />
                                                                                                          <FooterStyle HorizontalAlign="Left" />
                                                                                                          <HeaderStyle HorizontalAlign="Left" />
                                                                                                          <ItemStyle HorizontalAlign="Left" />
                                                                                                      </wijmo:C1BoundField>
                                                                                                  </Columns>
                                                                                              </wijmo:C1GridView>
                                                                                              <asp:Label ID="LblTest" runat="server"></asp:Label>
                                                                                          </td>
                                                                                      </tr>
                                                                                  </table>
                                                                              </wijmo:C1TabPage>
                                                                              <wijmo:C1TabPage ID="C1TabPatientInfo_AppCalender" runat="server" StaticKey="" Text="Appointment History">
                                                                                  <cc1:C1EventsCalendar ID="C1EventCalPatientOldAppList" runat="server" Width="800px">
                                                                                      <DataStorage>
                                                                                          <EventStorage>
                                                                                              <Mappings>
                                                                                                  <CalendarMapping DefaultValue="" />
                                                                                                  <TagMapping DefaultValue="" />
                                                                                                  <PropertiesMapping DefaultValue="" />
                                                                                                  <LocationMapping DefaultValue="" />
                                                                                                  <StartMapping DefaultValue="" />
                                                                                                  <EndMapping DefaultValue="" />
                                                                                                  <SubjectMapping DefaultValue="" />
                                                                                                  <DescriptionMapping DefaultValue="" />
                                                                                                  <ColorMapping DefaultValue="" />
                                                                                                  <IdMapping DefaultValue="" />
                                                                                              </Mappings>
                                                                                          </EventStorage>
                                                                                          <CalendarStorage>
                                                                                              <Mappings>
                                                                                                  <NameMapping DefaultValue="" />
                                                                                                  <LocationMapping DefaultValue="" />
                                                                                                  <DescriptionMapping DefaultValue="" />
                                                                                                  <ColorMapping DefaultValue="" />
                                                                                                  <PropertiesMapping DefaultValue="" />
                                                                                                  <TagMapping DefaultValue="" />
                                                                                                  <IdMapping DefaultValue="" />
                                                                                              </Mappings>
                                                                                          </CalendarStorage>
                                                                                      </DataStorage>
                                                                                  </cc1:C1EventsCalendar>
                                                                              </wijmo:C1TabPage>
                                                                          </pages>
                                                                      </wijmo:C1Tabs>
                                                                  </td>
                                                              </tr>
                                                          </table>
                                                              </div>
                                                          </asp:Panel>
                                                      </td>
                                                  </tr>
                                              </table>
                                          </asp:Panel>
                                          <wijmo:C1Dialog ID="C1DialogAddViewEditAppointment" runat="server" AppendTo="body" AutoExpand="True" MaintainVisibilityOnPostback="False" Show="blind" ShowOnLoad="False" Width="900px" Modal="True" Resizable="False">
                                              <CollapsingAnimation Option="">
                                              </CollapsingAnimation>
                                              <ExpandingAnimation Option="">
                                              </ExpandingAnimation>
                                              <CaptionButtons>
                                                  <Pin IconClassOff="ui-icon-pin-s" IconClassOn="ui-icon-pin-w" />
                                                  <Refresh IconClassOn="ui-icon-refresh" />
                                                  <Minimize IconClassOn="ui-icon-minus" />
                                                  <Maximize IconClassOn="ui-icon-extlink" />
                                                  <Close IconClassOn="ui-icon-close" />
                                              </CaptionButtons>
                                              <Content>
                                                  <asp:Panel ID="PnlAddAppointment" runat="server">
                                                      <table>
                                                          <tr>
                                                              <td>&nbsp;</td>
                                                              <td colspan="5">&nbsp;</td>
                                                              <td>&nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>&nbsp;</td>
                                                              <td colspan="5">
                                                                  <asp:Label ID="LblPatientNameAtAddApp" runat="server" style="font-weight: 700" Visible="False"></asp:Label>
                                                                  <asp:HiddenField ID="HFAddAppPatientID" runat="server" />
                                                              </td>
                                                              <td>&nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td width="30px"></td>
                                                              <td width="200px">
                                                                  <asp:Label ID="LblAssociateDoctorPatient" runat="server"></asp:Label>*
                                                              </td>
                                                              <td>
                                                                  <wijmo:C1ComboBox ID="C1ComboAssociateDoctorPatient" runat="server" TabIndex="11">
                                                                  </wijmo:C1ComboBox>
                                                              </td>
                                                              <td width="100px">
                                                                  <asp:RequiredFieldValidator CssClass="Validations" ID="RFVAssociateDoctorPatient" runat="server" ControlToValidate="C1ComboAssociateDoctorPatient" Display="Dynamic" ErrorMessage="Select Associate Doctor/Patient" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                              </td>
                                                              <td width="200px">Appointment Schedule*</td>
                                                              <td>
                                                                  <wijmo:C1InputDate ID="C1InpDateAppointmentSchedule" runat="server" Date="06/23/2014 17:24:00" DateFormat="g" TabIndex="12">
                                                                  </wijmo:C1InputDate>
                                                              </td>
                                                              <td width="30px">
                                                                  <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator5" runat="server" ControlToValidate="C1InpDateAppointmentSchedule" Display="Dynamic" ErrorMessage="Select Appointment Schedule" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                        <td></td>
                                                         <td colspan="5"><hr size="1" noshade="noshade" style="color: #E4E4E4;" /></td>
                                                        <td></td>
                                                    </tr>
                                                          <tr>
                                                              <td>&nbsp;</td>
                                                              <td>Height</td>
                                                              <td>
                                                                  <wijmo:C1InputNumeric ID="C1InNumHeight" runat="server" TabIndex="13">
                                                                  </wijmo:C1InputNumeric>
                                                              </td>
                                                              <td>&nbsp;</td>
                                                              <td>Weight</td>
                                                              <td>
                                                                  <wijmo:C1InputNumeric ID="C1InNumWeight" runat="server" TabIndex="14">
                                                                  </wijmo:C1InputNumeric>
                                                              </td>
                                                              <td>&nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>&nbsp;</td>
                                                              <td>Body Temperature</td>
                                                              <td>
                                                                  <wijmo:C1InputNumeric ID="C1InNumTemperature" runat="server" TabIndex="15">
                                                                  </wijmo:C1InputNumeric>
                                                              </td>
                                                              <td>&nbsp;</td>
                                                              <td>Heart Rate</td>
                                                              <td>
                                                                  <wijmo:C1InputNumeric ID="C1InNumHeartRate" runat="server" TabIndex="16">
                                                                  </wijmo:C1InputNumeric>
                                                              </td>
                                                              <td>&nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td></td>
                                                              <td>Systolic BP</td>
                                                              <td>
                                                                  <wijmo:C1InputNumeric ID="C1InNumSysBP" runat="server" TabIndex="17">
                                                                  </wijmo:C1InputNumeric>
                                                              </td>
                                                              <td></td>
                                                              <td>Notes</td>
                                                              <td rowspan="3">
                                                                  <asp:TextBox ID="TxtAppointmentNotes" runat="server" CssClass="txtMultiline" Height="95px" MaxLength="500" TextMode="MultiLine" TabIndex="20"></asp:TextBox>
                                                              </td>
                                                              <td></td>
                                                          </tr>
                                                          <tr>
                                                              <td>&nbsp;</td>
                                                              <td>Diastolic BP</td>
                                                              <td>
                                                                  <wijmo:C1InputNumeric ID="C1InNumDiastBP" runat="server" TabIndex="18">
                                                                  </wijmo:C1InputNumeric>
                                                              </td>
                                                              <td>&nbsp;</td>
                                                              <td>&nbsp;</td>
                                                              <td></td>
                                                          </tr>
                                                          <tr>
                                                              <td></td>
                                                              <td>Diagnosis Status</td>
                                                              <td>
                                                                  <wijmo:C1ComboBox ID="C1ComboDiagnosisStatus" runat="server" TabIndex="19">
                                                                  </wijmo:C1ComboBox>
                                                              </td>
                                                              <td>
                                                                  <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator6" runat="server" ControlToValidate="C1ComboDiagnosisStatus" Display="Dynamic" ErrorMessage="Select Diagnosis Status" SetFocusOnError="true" ValidationGroup="save">*</asp:RequiredFieldValidator>
                                                              </td>
                                                              <td></td>
                                                              <td></td>
                                                          </tr>
                                                          <tr>
                                                              <td>&nbsp;</td>
                                                              <td></td>
                                                              <td>&nbsp;</td>
                                                              <td>&nbsp;</td>
                                                              <td>&nbsp;</td>
                                                              <td></td>
                                                              <td>&nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>&nbsp;</td>
                                                              <td colspan="5">
                                                                  <asp:Label ID="LblMsgDialog" runat="server" CssClass="msglbl" Font-Bold="True"></asp:Label>
                                                              </td>
                                                              <td>&nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>&nbsp;</td>
                                                              <td>&nbsp;</td>
                                                              <td>&nbsp;</td>
                                                              <td>&nbsp;</td>
                                                              <td>&nbsp;</td>
                                                              <td>&nbsp;</td>
                                                              <td>&nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>&nbsp;</td>
                                                              <td colspan="5">
                                                                  <asp:Panel ID="PnlAddAppointmentBtns" runat="server">
                                                                      <table align="left">
                                                                          <tr>
                                                                              <td>
                                                                                  <asp:Button ID="BtnSubmit" runat="server" CssClass="myButton" OnClick="BtnSubmit_Click" TabIndex="21" ValidationGroup="save" />
                                                                                  <ajt:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure to save/update record?" TargetControlID="BtnSubmit">
                                                                                  </ajt:ConfirmButtonExtender>
                                                                              </td>
                                                                              <td>
                                                                                  <asp:Button ID="BtnReset" runat="server" CssClass="myButton" OnClick="BtnReset_Click" TabIndex="22" Text="Reset" />
                                                                              </td>
                                                                          </tr>
                                                                      </table>
                                                                  </asp:Panel>
                                                                  <asp:HiddenField ID="HFAppointmentDialogType" runat="server" />
                                                                  <asp:HiddenField ID="HFAppointmentID" runat="server" />
                                                                  <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowSummary="False" ValidationGroup="save" />
                                                              </td>
                                                              <td>&nbsp;</td>
                                                          </tr>
                                                          <tr>
                                                              <td>&nbsp;</td>
                                                              <td colspan="5">
                                                              </td>
                                                              <td>&nbsp;</td>
                                                          </tr>
                                                      </table>
                                                  </asp:Panel>
                                              </Content>
                                          </wijmo:C1Dialog>
                                      </td>
                                  </tr>
                              </table>
                          </ContentTemplate>
                      </asp:UpdatePanel>
                  </td>
              </tr>
          </table>


    </asp:Content>
