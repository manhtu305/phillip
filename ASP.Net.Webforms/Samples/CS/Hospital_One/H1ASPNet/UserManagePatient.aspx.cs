﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using Grapecity.Samples.HospitalOne.H1DBService_DBML;
using C1.Web.Wijmo.Controls.C1ComboBox;
using System.Data;

namespace Grapecity.Samples.HospitalOne.H1ASPNet
{
    public partial class UserManagePatient : System.Web.UI.Page
    {
        UIBinder UIBObj1 = new UIBinder();
        long Reg_ID = 0;

        //To check page rights for current user
        protected void CheckPageRights()
        {
            if (Session["UserType_ID"] == null || Session["UserType"] == null || Session["User_ID"] == null)
            {
                Response.Redirect("Logout.aspx");
            }
            if (UIBObj1.HasRightsForShow(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                Session["Msg"] = "Sorry! You are not authorized to manage Patients.";
                Response.Redirect("UserShowMsg.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckPageRights();
                if (!IsPostBack)
                {
                    SetPageLoad();
                    C1ddlSearchName.Focus();
                }
            }
            catch (Exception ex)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Error occurred: " + ex.Message;
            }
        }

        //To set page for first time loading
        protected void SetPageLoad()
        {
            Label MPLblPageTitle = (Label)Master.FindControl("LblPageTitle");
            if (MPLblPageTitle != null)
            {
                MPLblPageTitle.Text = "Manage Patients";
            }

            UIBObj1.BindPatient(C1ddlSearchName, 0, "1");

            C1ddlSex.Items.Clear();
            C1ddlSex.Items.Add(new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("Female", "Female"));
            C1ddlSex.Items.Add(new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("Male", "Male"));
            UIBObj1.BindCountry(C1ddlcountry, 0, "1");
            UIBObj1.BindBloodGroup(C1ddlBloodGroup, 0, "0");
            UIBObj1.BindQualification(C1ddlQualification, 0, "0");
            UIBObj1.BindDisease(C1ddlDisease, 0, "0"); 

            PnlAdvanceSearch.Visible = CBoxAdvanceSearch.Checked = true;
            UIBObj1.BindDisease(C1ddlSearchDisease, 0, "1");

            C1RegDate.Date = DateTime.Now;
            C1DOB.Date = DateTime.Today;
            C1SearchDate1.Date = DateTime.Now;
            C1SearchDate2.Date = DateTime.Now;
            PnlSearchGrid.Visible = false;
            if (Request.QueryString["UID"] != null && Request.QueryString["OT"] != null)
            {
                HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
                List<PRC_GetUserTypeResult> Result1 = H1EFObj1.PRC_GetUserType(long.Parse(Request.QueryString["UID"].ToString())).ToList();
                if (Result1.Count > 0)
                {
                    if (Result1[0].UserType_ID == 4)
                    {
                        C1ddlSearchName.SelectedValue = Request.QueryString["UID"].ToString();
                        CallOnC1ddlSearchNameIndexChanged();
                        if (Request.QueryString["OT"].ToString() == "V")
                        {
                            OpenRecordForView(long.Parse(Request.QueryString["UID"].ToString()));
                        }
                        else if (Request.QueryString["OT"].ToString() == "E")
                        {
                            OpenRecordForEdit(long.Parse(Request.QueryString["UID"].ToString()));
                        }
                    }
                }
            }
        }

        //To Fill states
        protected void FillState()
        {
            try
            {
                if (C1ddlcountry.SelectedIndex >= 0)
                {
                    UIBObj1.BindState(C1ddlstate, 0, long.Parse(C1ddlcountry.SelectedValue), "1");
                }
                else
                {
                    C1ddlstate.DataSource = null;
                    C1ddlstate.DataBind();
                }
            }
            catch (Exception ex)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Error occurred: " + ex.Message;
            }
            if (C1ddlstate.Items.Count > 0)
            {
                C1ddlstate.Focus();
            }
            else
            {
                C1ddlcountry.Focus();
            }
        }

        //To Fill Cities
        protected void FillCity()
        {
            try
            {
                if (C1ddlstate.SelectedIndex >= 0)
                {
                    UIBObj1.BindCity(C1ddlCity, 0, long.Parse(C1ddlstate.SelectedValue), "1");
                }
                else
                {
                    C1ddlCity.DataSource = null;
                    C1ddlCity.DataBind();
                }
            }
            catch (Exception ex)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Error occurred: " + ex.Message;
            }
            if (C1ddlCity.Items.Count > 0)
            {
                C1ddlCity.Focus();
            }
            else
            {
                C1ddlstate.Focus();
            }
        }

        //To check whether C1Dialog data is valid for update or not
        protected bool IsValidForInsertUpdate()
        {
            LblMsg.ForeColor = System.Drawing.Color.Red;
            bool RValue = false;
            if (BtnSubmit.Text == "Submit" && UIBObj1.HasRightsForInsert(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                LblMsg.Text = "Sorry! You are not authorized to add a new Patient.";
                return RValue;
            }
            if (BtnSubmit.Text == "Update" && UIBObj1.HasRightsForUpdate(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                LblMsg.Text = "Sorry! You are not authorized to edit a Patient.";
                return RValue;
            }
            if (UIBObj1.CheckUserBeforeInsertUpdate(Reg_ID, txtLoginUserName.Text.Trim(), C1txtMobile.Text.ToString().Trim()) > 0)//if (Res1.RCount > 0)
            {
                LblMsg.Text = "This Patient is already registered.";
                return RValue;
            }
            if (C1ddlcountry.SelectedIndex < 0)
            {
                LblMsg.Text = "Please! Select a Country.";
                return RValue;
            }
            if (C1ddlstate.SelectedIndex < 0)
            {
                LblMsg.Text = "Please! Select a State.";
                return RValue;
            }
            if (FileUpload1.HasFile)
            {
                string extn = System.IO.Path.GetExtension(FileUpload1.FileName);
                if (extn != "gif" && extn != "jpg" && extn != "png" && extn != "jpeg" && extn != "bmp")
                {
                    LblMsg.Text = "You can select only image files of following formats: gif,jpg,jpeg,png and bmp.";
                    return RValue;
                }
            }
            RValue = true;
            return RValue;
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            string LoginPwd = "";
            try
            {
                LblMsg.Text = null;
                LblMsgDialog.Text = null;
                string ImgURL = "N/A";
                if (FileUpload1.HasFile)
                {
                    ImgURL = "1";
                }
                Reg_ID = 0;
                //To Update selected Record of Patient
                if (BtnSubmit.Text == "Update")
                {
                    Reg_ID = long.Parse(HFRegID.Value);
                    if (IsValidForInsertUpdate())
                    {
                        long Result = 0;
                        string ErrorMsg = "";
                        UIBObj1.UserInsertUpdate(Reg_ID, C1RegDate.Date, 4, txtLoginUserName.Text.Trim(), LoginPwd, C1txtSSN.Text.Trim(), txtContactPerson.Text.Trim(), C1ddlSex.SelectedValue, txtFatherName.Text.Trim(), txtAddress.Text.Trim(), txtLandMark.Text.Trim(), long.Parse(C1ddlstate.SelectedValue), C1ddlCity.Text, C1txtZipcode.Text.ToString().Trim(), C1DOB.Date, C1txtMobile.Text.ToString().Trim(), C1txtEmail.Text.Trim(), long.Parse(C1ddlBloodGroup.SelectedValue), long.Parse(C1ddlQualification.SelectedValue), 0, 0, long.Parse(C1ddlDisease.SelectedValue), ImgURL, long.Parse(Session["User_ID"].ToString()), "Active", out    Result, out     ErrorMsg);
                        if (Result > 0)
                        {
                            UploadPics(Reg_ID);
                            string TempValue = C1ddlSearchName.SelectedValue;
                            UIBObj1.BindPatient(C1ddlSearchName, 0, "1");
                            C1ddlSearchName.SelectedValue = TempValue;
                            FillGrid(HFFillGridType.Value);
                            LblMsg.ForeColor = System.Drawing.Color.Gray;
                            LblMsg.Text = "Patient updated Successfully.";
                            C1DialogViewEdit.ShowOnLoad = false;
                            ClearField();
                        }
                        else
                        {
                            LblMsg.ForeColor = System.Drawing.Color.Red;
                            LblMsg.Text = "Error occurred: " + ErrorMsg;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = ex.Message;
            }
            LblMsgDialog.Text = LblMsg.Text;
        }

        //To reset all controls of page
        protected void BtnReset_Click(object sender, EventArgs e)
        {
            LblMsgDialog.Text = null;
            ClearField();
            C1DialogViewEdit.ShowOnLoad = false;
        }

        //To upload profile picture of user
        protected void UploadPics(long RegNo)
        {
            try
            {
                string PicName = "";
                if (FileUpload1.HasFile)
                {
                    string extn = System.IO.Path.GetExtension(FileUpload1.FileName);
                    if (extn == "gif" || extn == "jpg" || extn == "png" || extn == "jpeg" || extn == "bmp")
                    {
                        PicName = "UPP_" + RegNo + ".png";
                        UploadNewPicture(FileUpload1, PicName, "Soft_Data/Users/UserPics");
                    }
                    else
                    {
                        LblMsg.ForeColor = System.Drawing.Color.Red;
                        LblMsg.Text = "You can select only image files of following formats: gif,jpg,jpeg,png and bmp.";
                    }
                }
            }
            catch (Exception ex)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Error occured: " + ex.Message;
            }
        }

        private bool UploadNewPicture(FileUpload FileUploader, string strPictureName, string strImgFolder)
        {
            bool RetValue = false;
            string strImageFolderPath, strImagePath;
            //Construct saving path
            strImageFolderPath = Path.Combine(Request.PhysicalApplicationPath, strImgFolder);
            strImagePath = Path.Combine(strImageFolderPath, strPictureName);
            //Upload image
            FileUploader.SaveAs(strImagePath);
            RetValue = true;
            return RetValue;
        }

        //To clear/reset all controls of Page
        void ClearField()
        {
            PnlEdit.Visible = false;
            HFRegID.Value = "";
            txtLoginUserName.Text = null;
            txtContactPerson.Text = null;
            txtFatherName.Text = null;
            txtLandMark.Text = null;
            txtAddress.Text = null;
            C1txtZipcode.Text = null;
            C1txtMobile.Text = null;
            C1txtEmail.Text = null;
            C1DOB.Date = null;
            C1txtSSN.Text = null;
            C1ddlBloodGroup.SelectedIndex = 0;
            C1ddlQualification.SelectedIndex = 0;
            C1ddlDisease.SelectedIndex = 0;
            ImgProfilePic.ImageUrl = null;
            ImgProfilePic.Visible = false;
        }

        //To Open details of selected Patient
        protected void OpenRecord(string id)
        {
            List<PRC_GetUserListResult> PRC_Result1 = UIBObj1.GetUserList(long.Parse(id), 4, "", null, null, "", "", 0, 0);//H1EFObj1.PRC_GetPatientList(long.Parse(id), "", null, null, "", "", 0).ToList();
            if (PRC_Result1.Count <= 0)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "No Record found. try again";
                PnlEdit.Visible = false;
            }
            else
            {
                HFRegID.Value = PRC_Result1[0].User_ID.ToString();
                C1RegDate.Date = DateTime.Parse(PRC_Result1[0].Reg_Date.ToString());
                txtLoginUserName.Text = PRC_Result1[0].User_LoginName;
                txtContactPerson.Text = PRC_Result1[0].User_Name;
                C1ddlSex.SelectedValue = PRC_Result1[0].Sex;
                txtFatherName.Text = PRC_Result1[0].Father_Name;
                txtLandMark.Text = PRC_Result1[0].Land_Mark;
                txtAddress.Text = PRC_Result1[0].Address1;
                C1txtZipcode.Text = PRC_Result1[0].Zip_Code;
                C1ddlcountry.SelectedValue = PRC_Result1[0].Country_ID.ToString();
                FillState();
                C1ddlstate.SelectedValue = PRC_Result1[0].State_ID.ToString();
                FillCity();
                C1ddlCity.Text = PRC_Result1[0].City;
                C1txtMobile.Text = PRC_Result1[0].Contact_No;
                C1txtEmail.Text = PRC_Result1[0].Email_ID;
                C1DOB.Date = DateTime.Parse(PRC_Result1[0].Reg_Date.ToString());//.ToShortDateString();
                C1txtSSN.Text = PRC_Result1[0].SSN;
                C1ddlBloodGroup.SelectedValue = PRC_Result1[0].BloodGroup_ID.ToString();
                C1ddlQualification.SelectedValue = PRC_Result1[0].Qualification_ID.ToString();
                C1ddlDisease.SelectedValue = PRC_Result1[0].Disease_ID.ToString();
                ImgProfilePic.ImageUrl = PRC_Result1[0].Img_URL;
                ImgProfilePic.Visible = true;
                BtnSubmit.Text = "Update";
                PnlEdit.Visible = true;
            }
        }

        //To Delete record of selected Patient
        protected void DeleteRecord(string id)
        {
            HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
            int? Result = 0;
            string ErrorMsg = "";
            H1EFObj1.PRC_UserDelete(long.Parse(id), long.Parse(Session["User_ID"].ToString()), ref Result, ref ErrorMsg);
            if (Result > 0)
            {
                FillGrid(HFFillGridType.Value);

                UIBObj1.BindPatient(C1ddlSearchName, 0, "1");
                ClearField();
                LblMsg.ForeColor = System.Drawing.Color.Gray;
                LblMsg.Text = "Patient deleted successfully.";
            }
            else
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Error occurred: " + ErrorMsg.ToString();
            }
        }

        //To Fill search results in a C1GridView
        void FillGrid(string SearchType)
        {
            List<PRC_GetUserListResult> Result1;
            if (SearchType == "Normal")
            {
                string temp = C1ddlSearchName.SelectedValue;
                Result1 = UIBObj1.GetUserList(long.Parse(temp), 4, "", null, null, "", "", 0, 0);//SessionDBClass.Current.UserList.Where(x => x.User_ID == long.Parse(temp)).ToList(); //H1EFObj1.PRC_GetPatientList(long.Parse(temp), "", null, null, "", "", 0).ToList();
                C1GridView1.DataSource = Result1;
                C1GridView1.DataBind();
            }
            else //if (SearchType == "Advance")
            {
                long SearchPatient_ID = 0, SearchDisease_ID = 0;
                if (txtSearchRegID.Text.Trim().Length > 0)
                {
                    SearchPatient_ID = long.Parse(txtSearchRegID.Text.Trim());
                }
                DateTime SearchDate1 = DateTime.Parse(DateTime.Parse(C1SearchDate1.Date.ToString()).ToShortDateString() + " 12:00:00 AM"), SearchDate2 = DateTime.Parse(DateTime.Parse(C1SearchDate2.Date.ToString()).ToShortDateString() + " 11:59:59 PM");
                if (C1ddlSearchDisease.SelectedIndex < 0)
                {
                    SearchDisease_ID = 0;
                }
                else
                {
                    SearchDisease_ID = long.Parse(C1ddlSearchDisease.SelectedValue);
                }
                Result1 = UIBObj1.GetUserList(SearchPatient_ID, 4, txtSearchSSN.Text.Trim(), SearchDate1, SearchDate2, txtSearchName.Text.Trim(), txtSearchArea.Text.Trim(), SearchDisease_ID, 0);//SessionDBClass.Current.UserList.Where( //H1EFObj1.PRC_GetPatientList(SearchPatient_ID, txtSearchSSN.Text.Trim(), SearchDate1, SearchDate2, txtSearchName.Text.Trim(), txtSearchArea.Text.Trim(), SearchDisease_ID).ToList();
                C1GridView1.DataSource = Result1;
                C1GridView1.DataBind();
            }
            PnlSearchGrid.Visible = true;
            if (UIBObj1.HasRightsForShow(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                C1GridView1.Columns[0].Visible = false;
            }
            if (UIBObj1.HasRightsForUpdate(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                C1GridView1.Columns[1].Visible = false;
            }
            if (UIBObj1.HasRightsForDelete(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                C1GridView1.Columns[2].Visible = false;
            }
            LblMsg.ForeColor = System.Drawing.Color.Gray;
            LblMsg.Text = "Found Results: " + Result1.Count.ToString();
        }

        //For Advance Search of Patients List
        protected void ImgBtnAdvanceSearch_Click(object sender, ImageClickEventArgs e)
        {
            LblMsg.Text = null;
            C1DialogViewEdit.ShowOnLoad = false;
            PnlEdit.Visible = false;
            try
            {
                HFFillGridType.Value = "Advance";
                FillGrid(HFFillGridType.Value);
            }
            catch (Exception ex)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Error occurred: " + ex.Message;
            }
        }

        protected void CBoxAdvanceSearch_CheckedChanged(object sender, EventArgs e)
        {
            C1DialogViewEdit.ShowOnLoad = false;
            PnlAdvanceSearch.Visible = CBoxAdvanceSearch.Checked;
        }

        protected void C1ddlcountry_SelectedIndexChanged(object sender, C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxEventArgs args)
        {
            FillState();
        }

        protected void C1ddlstate_SelectedIndexChanged(object sender, C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxEventArgs args)
        {
            FillCity();
        }

        //To view details of selected Patient
        protected void ImgBtnView_Click(object sender, ImageClickEventArgs e)
        {
            LblMsg.Text = LblMsgDialog.Text = null;
            ImageButton btn = sender as ImageButton;
            if (btn != null)
            {
                string id = btn.CommandArgument;
                OpenRecordForView(long.Parse(id));
            }
        }

        //To open details of selected Patient in view only mode
        protected void OpenRecordForView(long User_ID)
        {
            if (UIBObj1.HasRightsForShow(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Sorry! You are not authorized to view Patient's profile.";
            }
            else
            {
                LblMsg.Text = LblMsgDialog.Text = null;
                List<PRC_GetUserListResult> Result1 = UIBObj1.GetUserList(User_ID, 0, "", null, null, "", "", 0, 0);//H1EFObj1.PRC_GetUserType(User_ID).ToList();
                if (Result1.Count > 0)
                {
                    if (Result1[0].UserType_ID == 4)
                    {
                        OpenRecord(User_ID.ToString());
                        FileUpload1.Visible = false;
                        PnlUpdateBtns.Visible = false;
                        C1DialogViewEdit.Width = 830;
                        C1DialogViewEdit.Title = "View";
                        C1DialogViewEdit.ShowOnLoad = true;
                    }
                }
            }
        }

        //To edit details of selected Patient
        protected void ImgBtnEdit_Click(object sender, ImageClickEventArgs e)
        {
            LblMsg.Text = LblMsgDialog.Text = null;
            if (UIBObj1.HasRightsForUpdate(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Sorry! You are not authorized to edit a Patient's profile.";
            }
            else
            {
                ImageButton btn = sender as ImageButton;
                if (btn != null)
                {
                    string id = btn.CommandArgument;
                    OpenRecordForEdit(long.Parse(id));
                }
            }
        }

        //To open details of selected Patient in edit mode
        protected void OpenRecordForEdit(long User_ID)
        {
            if (UIBObj1.HasRightsForUpdate(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Sorry! You are not authorized to edit a Patient's profile.";
            }
            else
            {
                LblMsg.Text = LblMsgDialog.Text = null;
                List<PRC_GetUserListResult> Result1 = UIBObj1.GetUserList(User_ID, 0, "", null, null, "", "", 0, 0);//H1EFObj1.PRC_GetUserType(User_ID).ToList();
                if (Result1.Count > 0)
                {
                    if (Result1[0].UserType_ID == 4)
                    {
                        OpenRecord(User_ID.ToString());
                        PnlUpdateBtns.Visible = true;
                        FileUpload1.Visible = true;
                        C1DialogViewEdit.Width = 830;
                        C1DialogViewEdit.Title = "Edit";
                        C1DialogViewEdit.ShowOnLoad = true;
                    }
                }
            }
        }

        protected void ImgBtnDelete_Click(object sender, ImageClickEventArgs e)
        {
            LblMsg.Text = null;
            if (UIBObj1.HasRightsForDelete(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Sorry! You are not authorized to delete a Patient's record.";
            }
            else
            {
                C1DialogViewEdit.ShowOnLoad = false;
                ImageButton btn = sender as ImageButton;
                if (btn != null)
                {
                    string id = btn.CommandArgument;
                    DeleteRecord(id);
                }
            }
        }

        //To open details of selected Patient
        protected void C1ddlSearchName_SelectedIndexChanged(object sender, C1ComboBoxEventArgs args)
        {
            CallOnC1ddlSearchNameIndexChanged();
        }

        protected void CallOnC1ddlSearchNameIndexChanged()
        {
            LblMsg.Text = null;
            C1DialogViewEdit.ShowOnLoad = false;
            try
            {
                HFFillGridType.Value = "Normal";
                FillGrid(HFFillGridType.Value);
            }
            catch (Exception ex)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Error occurred: " + ex.Message;
            }
        }

        //To handle event of page index changing of C1Gridview contains search results
        protected void C1GridView1_PageIndexChanging(object sender, C1.Web.Wijmo.Controls.C1GridView.C1GridViewPageEventArgs e)
        {
            C1GridView1.PageIndex = e.NewPageIndex;
            FillGrid(HFFillGridType.Value);
        }

    }
}