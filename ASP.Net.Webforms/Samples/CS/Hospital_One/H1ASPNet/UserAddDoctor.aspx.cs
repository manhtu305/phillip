﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using Grapecity.Samples.HospitalOne.H1DBService_DBML;
using C1.Web.Wijmo.Controls.C1ComboBox;

namespace Grapecity.Samples.HospitalOne.H1ASPNet    
{
    public partial class UserAddDoctor : System.Web.UI.Page
    {
        UIBinder UIBObj1 = new UIBinder();
        long Reg_ID = 0;

        //To check page rights for current user
        protected void CheckPageRights()
        {
            if (Session["UserType_ID"] == null || Session["UserType"] == null || Session["User_ID"] == null)
            {
                Response.Redirect("Logout.aspx");
            }
            if (UIBObj1.HasRightsForShow(long.Parse(Session["User_ID"].ToString()),UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath)))!="1")
            {
                Session["Msg"] = "Sorry! You are not authorized to add a new doctor.";
                Response.Redirect("UserShowMsg.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckPageRights();
                if (!IsPostBack)
                {
                    if (Request.QueryString["UId"] != null)
                    {
                        LblMsg.Text = "UId: " + Request.QueryString["UId"].ToString();
                    }                    
                    SetPageLoad();
                    C1RegDate.Focus();
                }
            }
            catch (Exception ex)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Error occurred: " + ex.Message;
            }
        }

        //To set page for first time loading
        protected void SetPageLoad()
        {
            Label MPLblPageTitle = (Label)Master.FindControl("LblPageTitle");
            if (MPLblPageTitle != null)
            {
                MPLblPageTitle.Text = "Add Doctors";
            }
            C1ddlSex.Items.Clear();
            C1ddlSex.Items.Add(new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("Female", "Female"));
            C1ddlSex.Items.Add(new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("Male", "Male"));
            UIBObj1.BindCountry(C1ddlcountry, 0, "1");
            UIBObj1.BindBloodGroup(C1ddlBloodGroup, 0, "0");
            UIBObj1.BindQualification(C1ddlQualification, 0, "0");
            UIBObj1.BindSpecialist(C1ddlSpecialist, 0, "0");
            C1RegDate.Date = DateTime.Now;
            C1DOB.Date = DateTime.Today;
            ClearField();
        }

        //To Fill states
        protected void FillState()
        {
            try
            {
                if (C1ddlcountry.SelectedIndex >= 0)
                {
                    UIBObj1.BindState(C1ddlstate, 0, long.Parse(C1ddlcountry.SelectedValue), "1");
                }
                else
                {
                    C1ddlstate.DataSource = null;
                    C1ddlstate.DataBind();
                }
            }
            catch (Exception ex)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Error occurred: " + ex.Message;
            }
            if (C1ddlstate.Items.Count > 0)
            {
                C1ddlstate.Focus();
            }
            else
            {
                C1ddlcountry.Focus();
            }
        }

        //To Fill Cities
        protected void FillCity()
        {
            try
            {
                if (C1ddlstate.SelectedIndex >= 0)
                {
                    UIBObj1.BindCity(C1ddlCity, 0, long.Parse(C1ddlstate.SelectedValue), "1");
                }
                else
                {
                    C1ddlCity.DataSource = null;
                    C1ddlCity.DataBind();
                }
            }
            catch (Exception ex)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Error occurred: " + ex.Message;
            }
            if (C1ddlCity.Items.Count > 0)
            {
                C1ddlCity.Focus();
            }
            else
            {
                C1ddlstate.Focus();
            }
        }       

        //To check whether form data is valid for insert or not
        protected bool IsValidForInsertUpdate()
        {
            LblMsg.ForeColor = System.Drawing.Color.Red;
            bool RValue = false;
            if (BtnSubmit.Text=="Submit" && UIBObj1.HasRightsForInsert(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                LblMsg.Text = "Sorry! You are not authorized to add a new Doctor.";
                return RValue;
            }
            if (BtnSubmit.Text == "Update" && UIBObj1.HasRightsForUpdate(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                LblMsg.Text = "Sorry! You are not authorized to edit a doctor.";
                return RValue;
            }
            if(UIBObj1.CheckUserBeforeInsertUpdate(Reg_ID,txtLoginUserName.Text.Trim(),C1txtMobile.Text.ToString().Trim())>0)//if (Res1.RCount > 0)
            {
                LblMsg.Text = "This Doctor is already registered.";
                return RValue;
            }
            if (C1ddlcountry.SelectedIndex < 0)
            {
                LblMsg.Text = "Please! Select a Country.";
                return RValue;
            }
            if (C1ddlstate.SelectedIndex < 0)
            {
                LblMsg.Text = "Please! Select a State.";
                return RValue;
            }
            if (FileUpload1.HasFile)
            {
                string extn = System.IO.Path.GetExtension(FileUpload1.FileName);
                if (extn != "gif" && extn != "jpg" && extn != "png" && extn != "jpeg" && extn != "bmp")
                {
                    LblMsg.Text = "You can select only image files of following formats: gif,jpg,jpeg,png and bmp.";
                    return RValue;
                }
            }
            RValue = true;
            return RValue;
        }

        //To insert a new record of doctor
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            string LoginPwd = "";
            try
            {
                LblMsg.Text = null;
                string ImgURL = "N/A";
                if (FileUpload1.HasFile)
                {
                    ImgURL = "1";
                }
                Reg_ID = 0;
                //To Insert New Record of Doctor
                if (BtnSubmit.Text == "Submit")
                {
                    if (IsValidForInsertUpdate())
                    {
                        long Result = 0;
                        string ErrorMsg = "";
                        LoginPwd = UIBObj1.GetRandomPwd(2);
                        UIBObj1.UserInsertUpdate(0, C1RegDate.Date, 2, txtLoginUserName.Text.Trim(), LoginPwd, C1txtSSN.Text.Trim(), txtContactPerson.Text.Trim(), C1ddlSex.SelectedValue, txtFatherName.Text.Trim(), txtAddress.Text.Trim(), txtLandMark.Text.Trim(), long.Parse(C1ddlstate.SelectedValue), C1ddlCity.Text, C1txtZipcode.Text.ToString().Trim(), C1DOB.Date, C1txtMobile.Text.ToString().Trim(), C1txtEmail.Text.Trim(), long.Parse(C1ddlBloodGroup.SelectedValue), long.Parse(C1ddlQualification.SelectedValue), 0, long.Parse(C1ddlSpecialist.SelectedValue), 0, ImgURL, long.Parse(Session["User_ID"].ToString()), "Active", out    Result, out     ErrorMsg);
                        Reg_ID = (long)Result;                        
                        if (Result > 0)
                        {
                            LblMsg.ForeColor = System.Drawing.Color.Gray;
                            LblMsg.Text = "Doctor added Successfully. Login Details:- User Name: " + txtLoginUserName.Text.Trim() + ", Password: " + LoginPwd;
                            UploadPics(Reg_ID);
                            ClearField();
                            Response.Redirect("UserDashboard.aspx?DID=" + Result.ToString() + "&PID=0&CF=AD");
                        }
                        else
                        {
                            LblMsg.ForeColor = System.Drawing.Color.Red;
                            LblMsg.Text = "Error occurred: " + ErrorMsg;
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = ex.Message;
            }        
        }

        //To reset all controls of page
        protected void BtnReset_Click(object sender, EventArgs e)
        {
            LblMsg.Text = null;
            ClearField();
        }

        //To upload profile picture of user
        protected void UploadPics(long RegNo)
        {
            try
            {
                string PicName = "";
                if (FileUpload1.HasFile)
                {
                    PicName = "UPP_" + RegNo + ".png";
                    UploadNewPicture(FileUpload1, PicName, "Soft_Data/Users/UserPics");
                }
            }
            catch (Exception ex)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Error occurred: " + ex.Message;
            }
        }

        private bool UploadNewPicture(FileUpload FileUploader, string strPictureName, string strImgFolder)
        {
            bool RetValue = false;
            string strImageFolderPath, strImagePath;
            //Construct saving path
            strImageFolderPath = Path.Combine(Request.PhysicalApplicationPath, strImgFolder);
            strImagePath = Path.Combine(strImageFolderPath, strPictureName);
            //Upload image
            FileUploader.SaveAs(strImagePath);
            RetValue = true;
            return RetValue;
        }

        //To clear/reset all controls of Page
        void ClearField()
        {
            HFRegID.Value = "";
            txtLoginUserName.Text = null;
            txtContactPerson.Text = null;
            C1ddlSex.SelectedIndex = 0;
            txtFatherName.Text = null;
            txtLandMark.Text = null;
            txtAddress.Text = null;
            C1txtZipcode.Text = null;
            C1txtMobile.Text = null;
            C1txtEmail.Text = null;
            C1txtSSN.Text = null;
            C1ddlBloodGroup.SelectedIndex = 0;
            C1ddlQualification.SelectedIndex = 0;
            C1ddlSpecialist.SelectedIndex = 0;
            BtnSubmit.Text = "Submit";
        }

        protected void C1ddlcountry_SelectedIndexChanged(object sender, C1ComboBoxEventArgs args)
        {
            FillState();
        }

        protected void C1ddlstate_SelectedIndexChanged(object sender, C1ComboBoxEventArgs args)
        {
            FillCity();
        }

    }

}