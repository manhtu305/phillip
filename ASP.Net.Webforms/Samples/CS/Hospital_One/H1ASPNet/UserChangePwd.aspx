﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/UserDefaultMaster.Master" AutoEventWireup="true" CodeBehind="UserChangePwd.aspx.cs" Inherits="Grapecity.Samples.HospitalOne.H1ASPNet.UserChangePwd" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>            
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <table style="text-align:center">
                                <tr>
                                    <td style="text-align:left">
                                        <asp:Label ID="LblMsg" runat="server" CssClass="msglbl" Font-Bold="False" Font-Names="Tahoma" Font-Size="Small" ForeColor="#660066"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Left">
                                            <table align="left" border="0" cellpadding="5" cellspacing="0">
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td style="margin-left: 80px">
                                                        &nbsp;</td>
                                                    <td style="margin-left: 80px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td style="text-align: left">Current Password</td>
                                                    <td style="margin-left: 80px">
                                                        <asp:TextBox ID="txtPass" runat="server" MaxLength="50" TabIndex="1" TextMode="Password"></asp:TextBox>
                                                        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtPass" Display="Dynamic" ErrorMessage="Enter your current password" SetFocusOnError="true" ValidationGroup="submit">*</asp:RequiredFieldValidator>
                                                    </td>
                                                    <td style="margin-left: 80px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>New Password</td>
                                                    <td style="margin-left: 80px">
                                                        <asp:TextBox ID="txtNewPass" runat="server" MaxLength="50" TabIndex="2" TextMode="Password"></asp:TextBox>
                                                        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator14" runat="server" ControlToValidate="txtNewPass" Display="Dynamic" ErrorMessage="Enter new password" SetFocusOnError="true" ValidationGroup="submit">*</asp:RequiredFieldValidator>
                                                    </td>
                                                    <td style="margin-left: 80px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td>Re-enter New Password</td>
                                                    <td style="margin-left: 80px">
                                                        <asp:TextBox ID="txtCNewPass" runat="server" MaxLength="50" TabIndex="3" TextMode="Password"></asp:TextBox>
                                                        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtCNewPass" Display="Dynamic" ErrorMessage="Re-enter new password" SetFocusOnError="true" ValidationGroup="submit">*</asp:RequiredFieldValidator>
                                                        <asp:CompareValidator CssClass="Validations" ID="CompareValidator2" runat="server" ControlToCompare="txtNewPass" ControlToValidate="txtCNewPass" Display="Dynamic" ErrorMessage="Re-enter new password" ValidationGroup="submit">*</asp:CompareValidator>
                                                    </td>
                                                    <td style="margin-left: 80px">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td colspan="2">
                                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="submit" />
                                                    </td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">&nbsp;</td>
                                                    <td align="center" colspan="2">
                                                        <table width="50%">
                                                            <tr>
                                                                <td>
                                                                    <asp:Button ID="cmdSubmit" runat="server" onclick="cmdSubmit_Click" TabIndex="4" Text="Submit" ValidationGroup="submit" CssClass="myButton" />
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="cmdReset" runat="server" onclick="cmdReset_Click" TabIndex="5" Text="Cancel" CssClass="myButton" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td align="center">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td align="center">&nbsp;</td>
                                                    <td align="center" colspan="2">&nbsp;</td>
                                                    <td align="center">&nbsp;</td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
</asp:Content>
