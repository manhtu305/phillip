﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using Grapecity.Samples.HospitalOne.H1DBService_DBML;
using C1.Web.Wijmo.Controls.C1ComboBox;
using System.Data;
using System.Xml.Linq;

namespace Grapecity.Samples.HospitalOne.H1ASPNet
{
    public partial class UserPatientAppointmentCalender : System.Web.UI.Page
    {
        UIBinder UIBObj1 = new UIBinder();
        long Reg_ID = 0;

        //To check page rights for current user
        protected void CheckPageRights()
        {
            if (Session["UserType_ID"] == null || Session["UserType"] == null || Session["User_ID"] == null)
            {
                Response.Redirect("Logout.aspx");
            }
            if (UIBObj1.HasRightsForShow(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                Session["Msg"] = "Sorry! You are not authorized to view Patient Appointments Calender.";
                Response.Redirect("UserShowMsg.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckPageRights();
                SetPageLoad();
            }
            catch (Exception ex)
            {
                LblMsg.Text = "Error occurred: " + ex.Message;
            }
        }

        //To set page for first time loading
        protected void SetPageLoad()
        {
            if (!IsPostBack)
            {
                Label MPLblPageTitle = (Label)Master.FindControl("LblPageTitle");
                if (MPLblPageTitle != null)
                {
                    MPLblPageTitle.Text = "Appointments Calendar";
                }

                UIBObj1.BindPatient(C1ddlSearchName, 0, "1");
                PnlAppointmentsList.Visible = false;
                if (Session["UserType_ID"].ToString() == "4")
                {
                    PnlSearchPatient.Visible = false;
                    HFPatientID.Value = Session["User_ID"].ToString();
                    FillPatientAppointmentsCalender(long.Parse(HFPatientID.Value));
                    PnlAppointmentsList.Visible = true;
                }
                else
                {
                    PnlSearchPatient.Visible = true;
                    if (C1ddlSearchName.Items.Count > 0)
                    {
                        C1ddlSearchName.SelectedValue = "102031";
                        SearchPatient_SelectionChanged();
                    }
                }
            }
        }

        //To open details of selected patient's appointments
        protected void C1ddlSearchName_SelectedIndexChanged(object sender, C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxEventArgs args)
        {
            SearchPatient_SelectionChanged();
        }

        //To open details of selected patient's appointments
        protected void SearchPatient_SelectionChanged()
        {
            LblMsg.Text = null;
            if (C1ddlSearchName.SelectedIndex >= 0)
            {
                HFPatientID.Value = C1ddlSearchName.SelectedValue;
                FillPatientAppointmentsCalender(long.Parse(HFPatientID.Value));
                PnlAppointmentsList.Visible = true;
            }
            else
            {
                HFPatientID.Value = null;
                PnlAppointmentsList.Visible = false;
                LblMsg.ForeColor = System.Drawing.Color.Gray;
                LblMsg.Text = "Select a Patient to view Appointment List.";
            }
        }

        //To fill appointement data of selected/current patient in C1EventCalendar
        protected void FillPatientAppointmentsCalender(long Patient_ID)
        {
            List<PRC_GetPatientAppointmentListResult> Result1 = UIBObj1.GetPatientAppointmentsList(0, Patient_ID, 0, null, "", "");
            if (Result1.Count > 0)
            {
                XDocument XdApp = XDocument.Load(Server.MapPath("c1evcaldata.xml"));
                XElement Appointments = XdApp.Descendants("Appointments").ToList()[0];
                Appointments.RemoveAll();
                foreach (PRC_GetPatientAppointmentListResult row in Result1)
                {
                    XElement Appointment = new XElement("Appointment");
                    Guid appguid = Guid.NewGuid();
                    Appointment.Add(new XAttribute("Id", appguid.ToString()));
                    Appointment.Add(new XAttribute("index", "-1"));

                    XElement subject = new XElement("Subject");
                    subject.Value = row.Disease_Name + "-" + row.Diagnosis_Status;
                    Appointment.Add(subject);

                    XElement AppDateTime = new XElement("Start");
                    AppDateTime.Value = row.GetType().GetProperty("Appointment_DateTime").GetValue(row).ToString();
                    Appointment.Add(AppDateTime);
                    Appointments.Add(Appointment);
                }
                string AppointmentsXML = XdApp.Root.ToString();
                string FileName = Server.MapPath("/Temp/patientallappointments.xml");
                if (File.Exists(FileName))
                {
                    File.Delete(FileName);
                }
                File.WriteAllText(FileName, AppointmentsXML);
                C1EventCalPatientAppList.SelectedDate = DateTime.Today;
                C1EventCalPatientAppList.ViewType = C1.Web.Wijmo.Controls.C1EventsCalendar.C1EventsCalendarViewType.Month;
                C1EventCalPatientAppList.DataStorage.Import(FileName, C1.Web.Wijmo.Controls.C1EventsCalendar.FileFormatEnum.XML);
                C1EventCalPatientAppList.DataStorage.SaveData();
                C1EventCalPatientAppList.Visible = true;
                LblMsg.ForeColor = System.Drawing.Color.Gray;
                LblMsg.Text = "Total Appointments: " + Result1.Count.ToString();
            }
            else
            {
                C1EventCalPatientAppList.Visible = false;
                LblMsg.ForeColor = System.Drawing.Color.Gray;
                LblMsg.Text = "No Appointment is available.";
            }
        }
    }
}