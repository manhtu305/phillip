﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using Grapecity.Samples.HospitalOne.H1DBService_DBML;
using C1.Web.Wijmo.Controls.C1ComboBox;
using System.Data;

namespace Grapecity.Samples.HospitalOne.H1ASPNet
{
    public partial class UserMyProfile : System.Web.UI.Page
    {
        UIBinder UIBObj1 = new UIBinder();
        long Reg_ID = 0;

        //To check page rights for current user
        protected void CheckPageRights()
        {
            if (Session["UserType_ID"] == null || Session["UserType"] == null || Session["User_ID"] == null)
            {
                Response.Redirect("Logout.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckPageRights();
                if (!IsPostBack)
                {
                    SetPageLoad();
                    OpenRecord(Session["User_ID"].ToString());
                }
            }
            catch (Exception ex)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Error occurred: " + ex.Message;
            }
        }

        protected void C1ddlcountry_SelectedIndexChanged(object sender, C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxEventArgs args)
        {
            FillState();
        }

        protected void C1ddlstate_SelectedIndexChanged(object sender, C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxEventArgs args)
        {
            FillCity();
        }

        //To set page for first time loading
        protected void SetPageLoad()
        {
            Label MPLblPageTitle = (Label)Master.FindControl("LblPageTitle");
            if (MPLblPageTitle != null)
            {
                MPLblPageTitle.Text = "My Profile";
            }
            LblMyName.Text = Session["User_Name"].ToString();
            C1ddlSex.Items.Clear();
            C1ddlSex.Items.Add(new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("Female", "Female"));
            C1ddlSex.Items.Add(new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("Male", "Male"));
            UIBObj1.BindCountry(C1ddlcountry, 0, "1");
            UIBObj1.BindBloodGroup(C1ddlBloodGroup, 0, "0");
            UIBObj1.BindQualification(C1ddlQualification, 0, "0");
            UIBObj1.BindSpecialist(C1ddlSpecialist, 0, "0");
            UIBObj1.BindDisease(C1ddlDisease, 0, "0");

            C1RegDate.Date = DateTime.Now;
            C1DOB.Date = DateTime.Today;
            if (Session["UserType_ID"].ToString() == "2" || Session["UserType_ID"].ToString() == "3")
            {
                LblSpecialityDisease.Text = "Speciality";
                C1ddlSpecialist.Visible = true;
                RFVSpeciality.Visible = true;
                C1ddlDisease.Visible = false;
                RFVDisease.Visible = false;
            }
            else
            {
                LblSpecialityDisease.Text = "Disease";
                C1ddlSpecialist.Visible = false;
                RFVSpeciality.Visible = false;
                C1ddlDisease.Visible = true;
                RFVDisease.Visible = true;
            }
        }

        //To Fill states
        protected void FillState()
        {
            try
            {
                if (C1ddlcountry.SelectedIndex >= 0)
                {
                    UIBObj1.BindState(C1ddlstate, 0, long.Parse(C1ddlcountry.SelectedValue), "1");
                }
                else
                {
                    C1ddlstate.DataSource = null;
                    C1ddlstate.DataBind();
                }
            }
            catch (Exception ex)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Error occurred: " + ex.Message;
            }
            if (C1ddlstate.Items.Count > 0)
            {
                C1ddlstate.Focus();
            }
            else
            {
                C1ddlcountry.Focus();
            }
        }

        //To Fill Cities
        protected void FillCity()
        {
            try
            {
                if (C1ddlstate.SelectedIndex >= 0)
                {
                    UIBObj1.BindCity(C1ddlCity, 0, long.Parse(C1ddlstate.SelectedValue), "1");
                }
                else
                {
                    C1ddlCity.DataSource = null;
                    C1ddlCity.DataBind();
                }
            }
            catch (Exception ex)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "Error occurred: " + ex.Message;
            }
            if (C1ddlCity.Items.Count > 0)
            {
                C1ddlCity.Focus();
            }
            else
            {
                C1ddlstate.Focus();
            }
        }

        //To check whether profile data is valid for update or not
        protected bool IsValidForInsertUpdate()
        {
            LblMsgDialog.ForeColor = System.Drawing.Color.Red;
            bool RValue = false;
            if (UIBObj1.CheckUserBeforeInsertUpdate(Reg_ID, txtLoginUserName.Text.Trim(), C1txtMobile.Text.Trim()) > 0)
            {
                LblMsgDialog.Text = "This User is already registered.";
                return RValue;
            }
            if (C1ddlcountry.SelectedIndex < 0)
            {
                LblMsgDialog.Text = "Please! Select a Country.";
                return RValue;
            }
            if (C1ddlstate.SelectedIndex < 0)
            {
                LblMsgDialog.Text = "Please! Select a State.";
                return RValue;
            }
            RValue = true;
            return RValue;
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                LblMsgDialog.Text = null;
                LblMsg.Text = null;
                string ImgURL = "N/A";
                Reg_ID = 0;
                //To Update existing Record of Doctor
                if (BtnSubmit.Text == "Edit")
                {
                    BtnSubmit.Text = "Update";
                    BtnSubmit.ValidationGroup = "save";
                    ValidationSummary1.Visible = true;
                    BtnReset.Visible = true;
                    PnlEdit.Enabled = true;
                }
                else if (BtnSubmit.Text == "Update")
                {
                    Reg_ID = long.Parse(HFRegID.Value);
                    if (IsValidForInsertUpdate())
                    {
                        long Result = 0;
                        string ErrorMsg = "";
                        if (Session["UserType_ID"].ToString() == "2" || Session["UserType_ID"].ToString() == "3")
                        {
                            UIBObj1.UserInsertUpdate(Reg_ID, DateTime.Parse(C1RegDate.Date.ToString()), long.Parse(Session["UserType_ID"].ToString()), txtLoginUserName.Text.Trim(), "", C1txtSSN.Text.Trim(), txtContactPerson.Text.Trim(), C1ddlSex.SelectedItem.Text, txtFatherName.Text.Trim(), txtAddress.Text.Trim(), txtLandMark.Text.Trim(), long.Parse(C1ddlstate.SelectedValue), C1ddlCity.Text.Trim(), C1txtZipcode.Text.Trim(), DateTime.Parse(C1DOB.Date.ToString()), C1txtMobile.Text.Trim(), C1txtEmail.Text.Trim(), long.Parse(C1ddlBloodGroup.SelectedValue), long.Parse(C1ddlQualification.SelectedValue), 0, long.Parse(C1ddlSpecialist.SelectedValue), 0, ImgURL, long.Parse(Session["User_ID"].ToString()), "Active", out  Result, out ErrorMsg);
                        }
                        else
                        {
                            UIBObj1.UserInsertUpdate(Reg_ID, DateTime.Parse(C1RegDate.Date.ToString()), long.Parse(Session["UserType_ID"].ToString()), txtLoginUserName.Text.Trim(), "", C1txtSSN.Text.Trim(), txtContactPerson.Text.Trim(), C1ddlSex.SelectedItem.Text, txtFatherName.Text.Trim(), txtAddress.Text.Trim(), txtLandMark.Text.Trim(), long.Parse(C1ddlstate.SelectedValue), C1ddlCity.Text.Trim(), C1txtZipcode.Text.Trim(), DateTime.Parse(C1DOB.Date.ToString()), C1txtMobile.Text.Trim(), C1txtEmail.Text.Trim(), long.Parse(C1ddlBloodGroup.SelectedValue), long.Parse(C1ddlQualification.SelectedValue), 0, 0, long.Parse(C1ddlSpecialist.SelectedValue), ImgURL, long.Parse(Session["User_ID"].ToString()), "Active", out   Result, out  ErrorMsg);
                        }
                        if (Result > 0)
                        {
                            LblMsg.ForeColor = System.Drawing.Color.Gray;
                            LblMsg.Text = "Your Profile has been updated successfully.";
                            BtnSubmit.Text = "Edit";
                            BtnSubmit.ValidationGroup = "N/A";
                            ValidationSummary1.Visible = false;
                            BtnReset.Visible = false;
                            PnlEdit.Enabled = false;
                        }
                        else
                        {
                            LblMsgDialog.ForeColor = System.Drawing.Color.Red;
                            LblMsgDialog.Text = "Error occurred: " + ErrorMsg;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LblMsgDialog.ForeColor = System.Drawing.Color.Red;
                LblMsgDialog.Text = ex.Message;
            }
        }

        //To reset all controls of page
        protected void BtnReset_Click(object sender, EventArgs e)
        {
            LblMsgDialog.Text = null;
            LblMsgDialog.Text = null;
            BtnSubmit.Text = "Edit";
            BtnSubmit.ValidationGroup = "N/A";
            ValidationSummary1.Visible = false;
            BtnReset.Visible = false;
            PnlEdit.Enabled = false;
        }

        //To Open details of current user profile
        protected void OpenRecord(string id)
        {
            List<PRC_GetUserListResult> PRC_Result1 = UIBObj1.GetUserList(long.Parse(id), 0, "", null, null, "", "", 0, 0);
            if (PRC_Result1.Count <= 0)
            {
                LblMsg.ForeColor = System.Drawing.Color.Red;
                LblMsg.Text = "No Record found. try again";
                PnlEdit.Visible = false;
            }
            else
            {
                HFRegID.Value = PRC_Result1[0].User_ID.ToString();
                C1RegDate.Date = DateTime.Parse(PRC_Result1[0].Reg_Date.ToString());
                txtLoginUserName.Text = PRC_Result1[0].User_LoginName;
                txtContactPerson.Text = PRC_Result1[0].User_Name;
                C1ddlSex.SelectedValue = PRC_Result1[0].Sex;
                txtFatherName.Text = PRC_Result1[0].Father_Name;
                txtLandMark.Text = PRC_Result1[0].Land_Mark;
                txtAddress.Text = PRC_Result1[0].Address1;
                C1txtZipcode.Text = PRC_Result1[0].Zip_Code;
                C1ddlcountry.SelectedValue = PRC_Result1[0].Country_ID.ToString();
                FillState();
                C1ddlstate.SelectedValue = PRC_Result1[0].State_ID.ToString();
                FillCity();
                C1ddlCity.Text = PRC_Result1[0].City;
                C1txtMobile.Text = PRC_Result1[0].Contact_No;
                C1txtEmail.Text = PRC_Result1[0].Email_ID;
                C1DOB.Date = DateTime.Parse(PRC_Result1[0].Reg_Date.ToString());
                C1txtSSN.Text = PRC_Result1[0].SSN;
                C1ddlBloodGroup.SelectedValue = PRC_Result1[0].BloodGroup_ID.ToString();
                C1ddlQualification.SelectedValue = PRC_Result1[0].Qualification_ID.ToString();
                C1ddlSpecialist.SelectedValue = PRC_Result1[0].Specialist_ID.ToString();
                C1ddlDisease.SelectedValue = PRC_Result1[0].Disease_ID.ToString();
                ImgProfilePic.ImageUrl = PRC_Result1[0].Img_URL;
                ImgProfilePic.Visible = true;

                BtnSubmit.Text = "Edit";
                BtnSubmit.ValidationGroup = "N/A";
                ValidationSummary1.Visible = false;
                BtnSubmit.Visible = true;
                BtnReset.Visible = false;
                PnlEdit.Enabled = false;
            }
        }
    }
}