﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Net.Mail;
using System.Data.Entity;
using Grapecity.Samples.HospitalOne.H1DBService_DBML;
using C1.Web.Wijmo.Controls;
using System.Linq;
using System.Data.Linq;

namespace Grapecity.Samples.HospitalOne.H1ASPNet
{
    public class SessionDBClass
    {
        //UIBinder UIBObj = new UIBinder();
        
        // private constructor
        private SessionDBClass()
        {
            HospitalOneDataContext H1EFObj = new HospitalOneDataContext();
            //Property1 = "default value";
            //UserList = new List<PRC_GetUserListResult>();
            UserList = H1EFObj.PRC_GetUserList(0, 0, "", null, null, "", "", 0, 0).ToList();//UIBObj.GetUserList(0, 0, "", null, null, "", "", 0, 0);
            //PatientAppointmentList = new List<PRC_GetPatientAppointmentListResult>();
            PatientAppointmentList = H1EFObj.PRC_GetPatientAppointmentList(0, 0, 0, null, "", "").ToList(); //UIBObj.GetPatientAppointmentsList(0, 0, 0, null, "", "");
            H1EFObj.Dispose();
        }

        // Gets the current session.
        public static SessionDBClass Current
        {
            get
            {
                SessionDBClass session =
                  (SessionDBClass)System.Web.HttpContext.Current.Session["__SessionDBClass__"];
                if (session == null)
                {
                    session = new SessionDBClass();
                    HttpContext.Current.Session["__SessionDBClass__"] = session;
                }
                return session;
            }
        }

        // **** add your session properties here, e.g like this:
        public List<PRC_GetUserListResult> UserList { get; set; }
        public List<PRC_GetPatientAppointmentListResult> PatientAppointmentList { get; set; }

        //public string Property1 { get; set; }
        //public DateTime MyDate { get; set; }
        //public int LoginId { get; set; }

        //public ISingleResult<PRC_GetCountryListResult> Countries { get; set; }
        //public ISingleResult<PRC_GetUserListResult> Users { get; set; }
        //public ISingleResult<PRC_GetDoctorListResult> Doctors { get; set; }
        //public ISingleResult<PRC_GetUserTypeResult> UserTypes { get; set; }
        //public ISingleResult<PRC_GetPatientListResult> Patients { get; set; }
    }
}