﻿using Grapecity.Samples.HospitalOne.H1DBService_DBML;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Linq;
using System.Text;

namespace Grapecity.Samples.HospitalOne.H1ASPNet
{
    public partial class Login : System.Web.UI.Page
    {   
        //To Login using entered user information
        protected void CallLogin()
        {
            try
            {
                LblMsg.Text = null;
                if (C1InputTextID.Text.Length <= 0) 
                {
                    LblMsg.Text = "Please! Enter your User ID.";
                    C1InputTextID.Focus();
                }
                else if (C1InputTextPassword.Text.Length <= 0)
                {
                    LblMsg.Text = "Please! Enter Password.";
                    C1InputTextPassword.Focus();

                }
                else
                {
                    HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
                    List<PRC_CheckUserForLoginResult> loginResults = H1EFObj1.PRC_CheckUserForLogin(C1InputTextID.Text.Trim()).ToList();
                    if (loginResults.Count > 0)
                    {
                        if (loginResults[0].UserType_ID != 1 || loginResults[0].Pwd == C1InputTextPassword.Text.Trim())
                        {
                            if (loginResults[0].Status == "Active")
                            {
                                Session["UserType_ID"] = loginResults[0].UserType_ID;
                                Session["UserType"] = loginResults[0].UserType;
                                Session["User_ID"] = loginResults[0].User_ID;
                                Session["User_LoginName"] = loginResults[0].User_LoginName;
                                Session["User_Name"] = loginResults[0].User_Name;
                                if (loginResults[0].UserType_ID == 4)
                                {
                                    H1EFObj1.Dispose();
                                    Response.Redirect("UserPatientAppointmentCalender.aspx");
                                }
                                else
                                {
                                    H1EFObj1.Dispose();
                                    Response.Redirect("UserDashboard.aspx");
                                }
                            }
                            else
                            {
                                LblMsg.Text = "Sorry! Your User ID is blocked. Please! Contact to admin for further details.";
                                C1InputTextID.Focus();
                            }
                        }
                        else
                        {
                            LblMsg.Text = "Sorry! Invalid User ID OR Password. Try Again.";
                            C1InputTextID.Focus();
                        }
                    }
                    LblMsg.Text = "Sorry! Invalid User ID OR Password. Try Again.";
                    C1InputTextID.Focus();
                    H1EFObj1.Dispose();
                }
            }
            catch (Exception ex)
            {
                LblMsg.Text = "Error: " + ex.Message;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
                string ErrMsg = "N/A";
                H1EFObj1.PRC_SetForDemo(ref ErrMsg);
                if (ErrMsg != "N/A")
                {
                    LblMsg.Text = "Error occurred: " + ErrMsg;
                }                
                FillNews();
                FillUserTypes();
                C1InputTextID.Focus();
                H1EFObj1.Dispose();
            }
        }

        //To Fill News
        protected void FillNews()
        {
            StringBuilder SB1 = new StringBuilder();
            SB1.Append("No News Available");            
            HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
            List<PRC_GetHomeNewsListResult> ResultNews = H1EFObj1.PRC_GetHomeNewsList().ToList();
            for (int i = 0; i < ResultNews.Count; i++)
            {
                if (i == 0)
                {
                    SB1.Clear();
                }
                SB1.Append("<span style='font-size: 18px;color: #333;clear: both;'>" + DateTime.Parse(ResultNews[i].News_Date.ToString()).ToShortDateString() + "</span>");
                SB1.Append("<p style='margin: 0 0 3px 0;line-height: 22px;'>" + ResultNews[i].News.ToString() + "</p>");
                SB1.Append("<a href='#' class='viewmore'>View More</a>");
            }
            LblHomeNews.Text = SB1.ToString();
            H1EFObj1.Dispose();
        }


        //To Fill User Types
        protected void FillUserTypes()
        {
            UIBinder UIBObj1 = new UIBinder();
            UIBObj1.BindUserTypesLoginPage(C1CombolUserType, 0);
            if (C1CombolUserType.Items.Count > 0)
            {
                C1CombolUserType.SelectedIndex = 0;
            }
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            CallLogin();
        }
        
        //To fill login informaiton of selected user type
        protected void C1CombolUserType_SelectedIndexChanged(object sender, C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxEventArgs args)
        {
            if (C1CombolUserType.SelectedIndex <= 0)
            {
                C1InputTextID.Text = null;
                C1InputTextPassword.Text = null;
                C1InputTextPassword.PasswordChar = "*";
            }
            else
            {
                HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
                List<PRC_GetUserListResult> Result1 = H1EFObj1.PRC_GetUserList(0, long.Parse(C1CombolUserType.SelectedValue), "", null, null, "", "", 0, 0).ToList();
                if (Result1.Count > 0)
                {
                    C1InputTextID.Text = Result1[0].User_LoginName;
                    C1InputTextPassword.PasswordChar = "";
                    C1InputTextPassword.Text = "******";
                    BtnSubmit.Focus();
                }
                H1EFObj1.Dispose();
            }

        }

        protected void C1InputTextID_TextChanged(object sender, EventArgs e)
        {
            C1InputTextPassword.PasswordChar = "*";
            C1InputTextPassword.Focus();
        }
 
    }
}