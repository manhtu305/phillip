﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Grapecity.Samples.HospitalOne.H1ASPNet.Login" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1Input" TagPrefix="wijmo" %>

<%@ Register Assembly="C1.Web.Wijmo.Controls.4" Namespace="C1.Web.Wijmo.Controls.C1ComboBox" TagPrefix="wijmo" %>
<%@ Register src="UserControls/PageLoader.ascx" tagname="PageLoader" tagprefix="uc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Welcome to Hospital One</title>

    <link href="css/home.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div id="Div1" >
            <uc1:PageLoader ID="PageLoader1" runat="server" />
        </div>
<div id="wrapper">
  <div id="header">
    <div id="logo"><a href="#">HospitalOne</a></div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
      <div id="loginModule"><img src="images/signin.png" width="219" height="19" alt="Sign In">
        <br />
          <br />
          <wijmo:C1ComboBox ID="C1CombolUserType" runat="server" AutoPostBack="True" TabIndex="1" OnSelectedIndexChanged="C1CombolUserType_SelectedIndexChanged" Width="100px" AutoComplete="False" ForceSelectionText="True">
              <ShowingAnimation Option="">
              </ShowingAnimation>
              <HidingAnimation Option="">
              </HidingAnimation>
          </wijmo:C1ComboBox>
          <br /><br />
         <label for="UserName"></label>
          <wijmo:C1InputText ID="C1InputTextID" runat="server" TabIndex="2" AutoPostBack="true" MaxLength="20" OnTextChanged="C1InputTextID_TextChanged" Width="220px"></wijmo:C1InputText>
          <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator1" runat="server" 
                            ControlToValidate="C1InputTextID" ErrorMessage="Enter User ID" SetFocusOnError="True" 
                            ValidationGroup="Login">*</asp:RequiredFieldValidator>
        <br/>
          <wijmo:C1InputText ID="C1InputTextPassword" runat="server" TabIndex="3" MaxLength="50"  Width="220px"></wijmo:C1InputText>
        <asp:RequiredFieldValidator CssClass="Validations" ID="RequiredFieldValidator2" runat="server" 
                            ControlToValidate="C1InputTextPassword" ErrorMessage="Enter Login Password" 
                            SetFocusOnError="True" ValidationGroup="Login">*</asp:RequiredFieldValidator>
          <br />
          <asp:Label ID="LblMsg" runat="server" Font-Bold="True" CssClass="msglbl"></asp:Label>
                                    <br />
                                                <asp:Button ID="BtnSubmit" runat="server" Text="Submit" 
                    onclick="BtnSubmit_Click" TabIndex="4" ValidationGroup="Login" />
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="False" ValidationGroup="Login"/>
      </div>
    </ContentTemplate>
        </asp:UpdatePanel>
    <div id="news"> <img src="images/news.png" width="62" height="17" alt="News"><br/>
        <asp:Label ID="LblHomeNews" runat="server"></asp:Label>
    </div>
  </div>
  <div id="footer">©<script>document.write(new Date().getFullYear())</script> GrapeCity, inc. All Rights Reserved.</div>
</div>


    </form>
</body>
</html>
