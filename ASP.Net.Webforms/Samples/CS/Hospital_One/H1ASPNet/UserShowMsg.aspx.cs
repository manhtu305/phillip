﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Grapecity.Samples.HospitalOne.H1ASPNet
{
    public partial class UserShowMsg : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //To Check whether a message in session is available or not.
            if (Session["Msg"] == null)
            {
                Response.Redirect("UserDashboard.aspx");
            }
            else
            {
                LblMsg.Text = Session["Msg"].ToString();
            }
            Label MPLblPageTitle = (Label)Master.FindControl("LblPageTitle");
            if (MPLblPageTitle != null)
            {
                MPLblPageTitle.Text = "Message";
            }

            Session["Msg"] = null;
        }
    }
}