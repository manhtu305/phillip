function onCallbackSuccess(result) {

}

$(document).ready(function () {

    if ($(".wijmo-stylesheet-wijmo_theme")[0]) {
        if ($(".wijmo-stylesheet-wijmo_theme").attr("href").indexOf("WebResource") != -1) {
            $('#themeswitcher').val("https://cdn.grapecity.com/wijmo/3/themes/cobalt/jquery-wijmo.css");
        } else {
            $('#themeswitcher').val($(".wijmo-stylesheet-wijmo_theme").attr("href"));
        }
    }

    if ($('#themeswitcher')[0]) {
        $('#themeswitcher').bind("change", function () {
            $(".wijmo-stylesheet-wijmo_theme").attr("href", $(this).val());
            executeCallback("theme=" + $(this).val());
        });
    }

    if ($ && $.fn.wijsuperpanel) {
        $(".navigation").wijsuperpanel();
        $(".navigation").wijsuperpanel("refresh");
    }

});