﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Net;
using System.Runtime.Serialization;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Net.Mail;
using System.Data.Entity;
using Grapecity.Samples.HospitalOne.H1DBService_DBML;
using C1.Web.Wijmo.Controls;
using System.Linq;
using System.Data.Linq;

namespace Grapecity.Samples.HospitalOne.H1ASPNet
{
    public class UIBinder
    {

        public List<PRC_GetBloodGroupListResult> GetBloodGroupList(long BloodGroup_ID)
        {
            HospitalOneDataContext H1EFObj = new HospitalOneDataContext();
            List<PRC_GetBloodGroupListResult> ResultList = H1EFObj.PRC_GetBloodGroupList(BloodGroup_ID).ToList();
            return ResultList;
        }

        public List<PRC_GetQualificationListResult> GetQualificationList(long Qualification_ID)
        {
            HospitalOneDataContext H1EFObj = new HospitalOneDataContext();
            List<PRC_GetQualificationListResult> ResultList = H1EFObj.PRC_GetQualificationList(Qualification_ID).ToList();
            return ResultList;
        }

        public List<PRC_GetProfessionListResult> GetProfessionList(long Profession_ID)
        {
            HospitalOneDataContext H1EFObj = new HospitalOneDataContext();
            List<PRC_GetProfessionListResult> ResultList = H1EFObj.PRC_GetProfessionList(Profession_ID).ToList();
            return ResultList;
        }

        public List<PRC_GetDiseaseListResult> GetDiseaseList(long Disease_ID)
        {
            HospitalOneDataContext H1EFObj = new HospitalOneDataContext();
            List<PRC_GetDiseaseListResult> ResultList = H1EFObj.PRC_GetDiseaseList(Disease_ID).ToList();
            return ResultList;
        }

        public List<PRC_GetSpecialistListResult> GetSpecialityList(long Speciality_ID)
        {
            HospitalOneDataContext H1EFObj = new HospitalOneDataContext();
            List<PRC_GetSpecialistListResult> ResultList = H1EFObj.PRC_GetSpecialistList(Speciality_ID).ToList();
            return ResultList;
        }

        public List<PRC_GetCountryListResult> GetCountryList(long Country_ID,long State_ID)
        {
            HospitalOneDataContext H1EFObj = new HospitalOneDataContext();
            List<PRC_GetCountryListResult> ResultList = H1EFObj.PRC_GetCountryList(Country_ID, State_ID).ToList();
            return ResultList;
        }

        public List<PRC_GetStateListResult> GetStateList(long State_ID, long Country_ID)
        {
            HospitalOneDataContext H1EFObj = new HospitalOneDataContext();
            List<PRC_GetStateListResult> ResultList = H1EFObj.PRC_GetStateList(State_ID, Country_ID).ToList();
            return ResultList;
        }

        public List<PRC_GetCityListResult> GetCityList(long State_ID, long City_ID)
        {
            HospitalOneDataContext H1EFObj = new HospitalOneDataContext();
            List<PRC_GetCityListResult> ResultList = H1EFObj.PRC_GetCityList(State_ID, City_ID).ToList();
            return ResultList;
        }

        public List<PRC_GetParentMenuListResult> GetUserParentMenus(long UserID)
        {
            HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
            return (H1EFObj1.PRC_GetParentMenuList(UserID).ToList());
        }

        public List<PRC_GetChildMenuListResult> GetUserChildMenus(long UserID, long PMenuID)
        {
            HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
            return (H1EFObj1.PRC_GetChildMenuList(UserID, PMenuID).ToList());
        }

        public List<PRC_GetHomeNewsListResult> GetHomeNewsList()
        {
            HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
            //H1EFObj1.PRC_SetForDemo(ref ErrMsg);
            List<PRC_GetHomeNewsListResult> ResultNews = H1EFObj1.PRC_GetHomeNewsList().ToList();
            return ResultNews;
        }

        public string GetUserName(string UserType_ID)
        {
            HospitalOneDataContext HEFObj1 = new HospitalOneDataContext();
            List<PRC_GetUserListResult> Result1 = HEFObj1.PRC_GetUserList(0, long.Parse(UserType_ID), "", null, null, "", "", 0, 0).ToList();
            return (Result1[0].User_LoginName);
        }

        //To Save/Update Users in Session Database
        public void UserInsertUpdate(long User_ID, DateTime? Reg_Date, long UserType_ID, string LoginUserName, string LoginPwd, string SSN, string ContactPerson, string Sex, string FatherName, string Address, string LandMark, long State_ID, string City, string Zipcode, DateTime? DOB, string Mobile, string Email, long BloodGroup_ID, long Qualification_ID, long Profession_ID, long Speciality_ID, long Disease_ID, string ImgURL, long AddUpdateUser_ID, string Status, out   long Result, out   string ErrorMsg)
        {
            HospitalOneDataContext H1EFObj = new HospitalOneDataContext();
            PRC_GetCountryListResult CountryInfo = H1EFObj.PRC_GetCountryList(0, State_ID).Single();
            PRC_GetUserTypeListResult UserTypeInfo = H1EFObj.PRC_GetUserTypeList(UserType_ID).Single();
            string User_Name_WithType = ContactPerson + " (" + UserTypeInfo.UserType + ")";
            ErrorMsg = "N/A";
            Result = -1;
            string AddUpdateUser_LoginName = SessionDBClass.Current.UserList.Where(x => x.User_ID == AddUpdateUser_ID).Single().User_LoginName, AddUpdateUser_Name = SessionDBClass.Current.UserList.Where(x => x.User_ID == AddUpdateUser_ID).Single().User_Name, Country_Name = CountryInfo.Country_Name, UserType_SF = UserTypeInfo.UserType_SF, UserType = UserTypeInfo.UserType;
            long Country_ID = CountryInfo.Country_ID;
            try
            {
                if (User_ID == 0)
                {
                    long NewID = SessionDBClass.Current.UserList.Max(x => x.User_ID) + 1;
                    //Add new Record
                    if (ImgURL == "N/A")
                    {
                        ImgURL = "Soft_Data\\Users\\UserPics\\UPP_Default.png";
                    }
                    else
                    {
                        ImgURL = "Soft_Data\\Users\\UserPics\\UPP_" + NewID.ToString() + ".png";
                    }
                    SessionDBClass.Current.UserList.Add(new PRC_GetUserListResult { Add_Date = DateTime.Now, Address1 = Address, AddUser_ID = AddUpdateUser_ID, AddUser_LoginName = AddUpdateUser_LoginName, AddUser_Name = AddUpdateUser_Name, BloodGroup = GetBloodGroupList(BloodGroup_ID)[0].BloodGroup, BloodGroup_ID = BloodGroup_ID, City = City, Contact_No = Mobile, Country_ID = Country_ID, Country_Name = Country_Name, Disease_ID = Disease_ID, Disease_Name = GetDiseaseList(Disease_ID)[0].Disease_Name, DOB = DOB, Email_ID = Email, Father_Name = FatherName, Img_URL = ImgURL, Land_Mark = LandMark, Profession_ID = Profession_ID, Profession_Name = GetProfessionList(Profession_ID)[0].Profession_Name, Pwd = LoginPwd, Qualification = GetQualificationList(Qualification_ID)[0].Qualification, Qualification_ID = Qualification_ID, Reg_Date = Reg_Date, Sex = Sex, Specialist = GetSpecialityList(Speciality_ID)[0].Specialist, Specialist_ID = Speciality_ID, SSN = SSN, State_ID = State_ID, State_Name = GetStateList(State_ID, 0)[0].State_Name, Status = Status, Update_Date = DateTime.Now, UpdateUser_ID = AddUpdateUser_ID, User_ID = NewID, User_LoginName = LoginUserName, User_Name = ContactPerson, User_Name_WithType = User_Name_WithType, UserType = UserType, UserType_ID = UserType_ID, UserType_SF = UserType_SF, Zip_Code = Zipcode });
                    Result = NewID;
                }
                else
                {
                    //Update a Record
                    PRC_GetUserListResult ToUpdate = SessionDBClass.Current.UserList.Where(x => x.User_ID == User_ID).First();
                    if (ImgURL != "N/A")
                    {
                        ImgURL = "Soft_Data\\Users\\UserPics\\UPP_" + User_ID.ToString() + ".png";
                    }
                    else
                    {
                        ImgURL = ToUpdate.Img_URL;
                    }
                    if (SessionDBClass.Current.UserList.Remove(ToUpdate))
                    {
                        SessionDBClass.Current.UserList.Add(new PRC_GetUserListResult { Add_Date = DateTime.Now, Address1 = Address, AddUser_ID = AddUpdateUser_ID, AddUser_LoginName = AddUpdateUser_LoginName, AddUser_Name = AddUpdateUser_Name, BloodGroup = GetBloodGroupList(BloodGroup_ID)[0].BloodGroup, BloodGroup_ID = BloodGroup_ID, City = City, Contact_No = Mobile, Country_ID = Country_ID, Country_Name = Country_Name, Disease_ID = Disease_ID, Disease_Name = GetDiseaseList(Disease_ID)[0].Disease_Name, DOB = DOB, Email_ID = Email, Father_Name = FatherName, Img_URL = ImgURL, Land_Mark = LandMark, Profession_ID = Profession_ID, Profession_Name = GetProfessionList(Profession_ID)[0].Profession_Name, Pwd = LoginPwd, Qualification = GetQualificationList(Qualification_ID)[0].Qualification, Qualification_ID = Qualification_ID, Reg_Date = Reg_Date, Sex = Sex, Specialist = GetSpecialityList(Speciality_ID)[0].Specialist, Specialist_ID = Speciality_ID, SSN = SSN, State_ID = State_ID, State_Name = GetStateList(State_ID, 0)[0].State_Name, Status = Status, Update_Date = DateTime.Now, UpdateUser_ID = AddUpdateUser_ID, User_ID = User_ID, User_LoginName = LoginUserName, User_Name = ContactPerson, User_Name_WithType = User_Name_WithType, UserType = UserType, UserType_ID = UserType_ID, UserType_SF = UserType_SF, Zip_Code = Zipcode });
                    }
                    else
                    {
                        ErrorMsg = "Unknown error.";
                    }
                    Result = User_ID;
                }
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
            }
            H1EFObj.Dispose();
        }

        public int CheckUserBeforeInsertUpdate(long Reg_ID, string LoginUserName, string Mobile)
        {
            int RetValue = 0;
            if (Reg_ID == 0)
            {
                RetValue = GetUserList(0, 0, "", null, null, "", "", 0, 0).Where(x => x.User_LoginName == LoginUserName).Count();
            }
            else
            {
                RetValue = GetUserList(0, 0, "", null, null, "", "", 0, 0).Where(x => (x.User_LoginName == LoginUserName) & (x.User_ID != Reg_ID)).Count();
            }
            return RetValue;
        }

        public int CheckAppointmentBeforeInsertUpdate(long App_ID, long Patient_ID, long Doctor_ID,DateTime? App_DateTime)
        {
            int RetValue = 0;
            RetValue = GetPatientAppointmentsList(0, 0, 0, null, "", "").Where(x => (App_ID == 0 | x.Appointment_ID != App_ID) & (Patient_ID == 0 | x.User_ID == Patient_ID) & (Doctor_ID == 0 | x.Doctor_ID == Doctor_ID) & (App_DateTime == null | (x.Appointment_DateTime.Day == App_DateTime.Value.Day & x.Appointment_DateTime.Month == App_DateTime.Value.Month & x.Appointment_DateTime.Year == App_DateTime.Value.Year & x.Appointment_DateTime.Hour == App_DateTime.Value.Hour & x.Appointment_DateTime.Minute == App_DateTime.Value.Minute))).Count();
            return RetValue;
        }


        public void AppointmentInsertUpdate(long App_ID, long Patient_ID, long Doctor_ID, DateTime? App_DateTime, DateTime? Actual_DateTime, double Height, double Weight, double Temperature, double HeartRate, double SysBP, double DiastBP, long DiagnosisStatus_ID, string Notes, long AddUpdateUser_ID, string Status, out long Result, out string ErrorMsg)
        {
            HospitalOneDataContext H1EFObj = new HospitalOneDataContext();
            ErrorMsg = "N/A";
            Result = -1;
            try
            {
                if (App_ID == 0)
                {
                    long NewID = SessionDBClass.Current.PatientAppointmentList.Max(x => x.Appointment_ID) + 1;
                    //Add new Record
                    SessionDBClass.Current.PatientAppointmentList.Add(new PRC_GetPatientAppointmentListResult { Actual_DateTime = Actual_DateTime, Add_Date = DateTime.Now, Address1 = "", AddUser_ID = AddUpdateUser_ID, AddUser_LoginName = GetUserList(AddUpdateUser_ID, 0, "", null, null, "", "", 0, 0).First().User_LoginName, AddUser_Name = GetUserList(AddUpdateUser_ID, 0, "", null, null, "", "", 0, 0).First().User_Name, Appointment_DateTime = (DateTime)App_DateTime, Appointment_ID = NewID, BloodGroup = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().BloodGroup, BloodGroup_ID = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().BloodGroup_ID, City = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().City, Contact_No = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().Contact_No, Country_ID = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().Country_ID, Country_Name = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().Country_Name, Diagnosis_Status = GetDiagnosisStatusList(DiagnosisStatus_ID).First().Diagnosis_Status, Diagnosis_Status_ID = DiagnosisStatus_ID, Diastolic_BP = DiastBP, Disease_ID = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().Disease_ID, Disease_Name = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().Disease_Name, DOB = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().DOB, Doctor_ID = Doctor_ID, Doctor_Name = GetUserList(Doctor_ID, 0, "", null, null, "", "", 0, 0).First().User_Name, Email_ID = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().Email_ID, Father_Name = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().Father_Name, Heart_Rate = HeartRate, Height = Height, Img_URL = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().Img_URL, Land_Mark = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().Land_Mark, Notes = Notes, Profession_ID = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().Profession_ID, Profession_Name = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().Profession_Name, Pwd = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().Pwd, Qualification = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().Qualification, Qualification_ID = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().Qualification_ID, Reg_Date = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().Reg_Date, Sex = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().Sex, Specialist = GetUserList(Doctor_ID, 0, "", null, null, "", "", 0, 0).First().Specialist, Specialist_ID = GetUserList(Doctor_ID, 0, "", null, null, "", "", 0, 0).First().Specialist_ID, SSN = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().SSN, State_ID = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().State_ID, State_Name = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().State_Name, Status = Status, Systolic_BP = SysBP, Temperature = Temperature, User_ID = Patient_ID, User_LoginName = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().User_LoginName, User_Name = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().User_Name, UserType_ID = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().UserType_ID, Weight = Weight, Zip_Code = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).First().Zip_Code });
                    Result = NewID;
                }
                else
                {
                    SessionDBClass.Current.PatientAppointmentList.Where(x => x.Appointment_ID == App_ID).ToList().ForEach(x =>
                    {
                        x.Actual_DateTime = Actual_DateTime;
                        x.Add_Date = DateTime.Now; x.Address1 = "";
                        x.AddUser_ID = AddUpdateUser_ID;
                        x.AddUser_LoginName = GetUserList(AddUpdateUser_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().User_LoginName;
                        x.AddUser_Name = GetUserList(AddUpdateUser_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().User_Name;
                        x.Appointment_DateTime = (DateTime)App_DateTime;
                        x.Appointment_ID = App_ID;
                        x.BloodGroup = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().BloodGroup;
                        x.BloodGroup_ID = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().BloodGroup_ID; x.City = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().City; x.Contact_No = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Contact_No; x.Country_ID = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Country_ID; x.Country_Name = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Country_Name; x.Diagnosis_Status = GetDiagnosisStatusList(DiagnosisStatus_ID).FirstOrDefault().Diagnosis_Status; x.Diagnosis_Status_ID = DiagnosisStatus_ID; x.Diastolic_BP = DiastBP; x.Disease_ID = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Disease_ID; x.Disease_Name = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Disease_Name; x.DOB = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().DOB; x.Doctor_ID = Doctor_ID; x.Doctor_Name = GetUserList(Doctor_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().User_Name; x.Email_ID = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Email_ID; x.Father_Name = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Father_Name; x.Heart_Rate = HeartRate; x.Height = Height; x.Img_URL = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Img_URL; x.Land_Mark = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Land_Mark; x.Notes = Notes; x.Profession_ID = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Profession_ID; x.Profession_Name = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Profession_Name; x.Pwd = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Pwd; x.Qualification = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Qualification; x.Qualification_ID = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Qualification_ID; x.Reg_Date = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Reg_Date; x.Sex = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Sex; x.Specialist = GetUserList(Doctor_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Specialist; x.Specialist_ID = GetUserList(Doctor_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Specialist_ID; x.SSN = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().SSN; x.State_ID = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().State_ID; x.State_Name = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().State_Name; x.Status = Status; x.Systolic_BP = SysBP; x.Temperature = Temperature; x.User_ID = Patient_ID; x.User_LoginName = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().User_LoginName; x.User_Name = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().User_Name; x.UserType_ID = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().UserType_ID; x.Weight = Weight; x.Zip_Code = GetUserList(Patient_ID, 0, "", null, null, "", "", 0, 0).FirstOrDefault().Zip_Code; x.Update_Date = DateTime.Now;
                        x.UpdateUser_ID = AddUpdateUser_ID;
                    });
                    Result = 0;
                }
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
            }
        }

        public List<PRC_GetUserListResult> GetUserList(long User_ID, long UserType_ID, string SSN, DateTime? Reg_Date1, DateTime? Reg_Date2, string User_Name, string City, long Speciality_ID, long Disease_ID)
        {
            return SessionDBClass.Current.UserList.Where(x => (User_ID == 0 | x.User_ID == User_ID) & (UserType_ID == 0 | x.UserType_ID == UserType_ID) & (SSN == "" | x.SSN.Contains(SSN)) & (Reg_Date1 == null | x.Reg_Date >= Reg_Date1) & (Reg_Date2 == null | x.Reg_Date <= Reg_Date2) & (User_Name == "" | x.User_Name.Contains(User_Name)) & (City == "" | x.City.Contains(City)) & (Speciality_ID == 0 | x.Specialist_ID == Speciality_ID) & (Disease_ID == 0 | x.Disease_ID == Disease_ID)).ToList();
        }

        public List<PRC_GetPatientAppointmentListResult> GetPatientAppointmentsList(long Appointment_ID, long Patient_ID, long Doctor_ID, DateTime? SearchDate, string Diagnosis_Status_E, string Diagnosis_Status_NE)
        {
            if (SearchDate == null)
            {
                return (SessionDBClass.Current.PatientAppointmentList.Where(x => (Appointment_ID == 0 | x.Appointment_ID == Appointment_ID) & (Patient_ID == 0 | x.User_ID == Patient_ID) & (Doctor_ID == 0 | x.Doctor_ID == Doctor_ID) & (Diagnosis_Status_E == "" | x.Diagnosis_Status == Diagnosis_Status_E) & (Diagnosis_Status_NE == "" | x.Diagnosis_Status != Diagnosis_Status_NE)).ToList());
            }
            else
            {
                return (SessionDBClass.Current.PatientAppointmentList.Where(x => (Appointment_ID == 0 | x.Appointment_ID == Appointment_ID) & (Patient_ID == 0 | x.User_ID == Patient_ID) & (Doctor_ID == 0 | x.Doctor_ID == Doctor_ID) & (x.Appointment_DateTime.Day == SearchDate.Value.Day & x.Appointment_DateTime.Month == SearchDate.Value.Month & x.Appointment_DateTime.Year == SearchDate.Value.Year) & (Diagnosis_Status_E == "" | x.Diagnosis_Status == Diagnosis_Status_E) & (Diagnosis_Status_NE == "" | x.Diagnosis_Status != Diagnosis_Status_NE)).ToList());
            }
            
        }

        public List<PRC_GetDiagnosisStatusListResult> GetDiagnosisStatusList(long Diagnosis_Status_ID)
        {
            HospitalOneDataContext H1EFObj = new HospitalOneDataContext();
            List<PRC_GetDiagnosisStatusListResult> Result1 = H1EFObj.PRC_GetDiagnosisStatusList(Diagnosis_Status_ID).ToList();
            H1EFObj.Dispose();
            return (Result1);
        }

        //To return menu id of a page
        public long GetMenuID(string PageName)
        {
            long? Menu_ID = 0;
            HospitalOneDataContext HEFObj = new HospitalOneDataContext();           
            HEFObj.PRC_GetMenuID(PageName,ref  Menu_ID);
            return (long)Menu_ID;
        }

        //To check show rights of a menu for a specific user
        public string HasRightsForShow(long User_ID,long Menu_ID)
        {            
            string ShowRight = "0", InsertRight = "0", UpdateRight = "0", DeleteRight = "0";
            HospitalOneDataContext HEFObj = new HospitalOneDataContext();
             HEFObj.PRC_CheckUserRights(User_ID, Menu_ID, ref ShowRight, ref InsertRight, ref UpdateRight, ref DeleteRight);
             return ShowRight;
        }

        //To check insert rights of a menu for a specific user
        public string HasRightsForInsert(long User_ID, long Menu_ID)
        {
            string ShowRight = "0", InsertRight = "0", UpdateRight = "0", DeleteRight = "0";
            HospitalOneDataContext HEFObj = new HospitalOneDataContext();
            HEFObj.PRC_CheckUserRights(User_ID, Menu_ID, ref ShowRight, ref InsertRight, ref UpdateRight, ref DeleteRight);
            return InsertRight;
        }

        //To check update rights of a menu for a specific user
        public string HasRightsForUpdate(long User_ID, long Menu_ID)
        {
           string ShowRight = "0", InsertRight = "0", UpdateRight = "0", DeleteRight = "0";
            HospitalOneDataContext HEFObj = new HospitalOneDataContext();
            HEFObj.PRC_CheckUserRights(User_ID, Menu_ID, ref ShowRight, ref InsertRight, ref UpdateRight, ref DeleteRight);
            return UpdateRight;
        }

        //To check delete rights of a menu for a specific user
        public string HasRightsForDelete(long User_ID, long Menu_ID)
        {
            string ShowRight = "0", InsertRight = "0", UpdateRight = "0", DeleteRight = "0";
            HospitalOneDataContext HEFObj = new HospitalOneDataContext();
            HEFObj.PRC_CheckUserRights(User_ID, Menu_ID, ref ShowRight, ref InsertRight, ref UpdateRight, ref DeleteRight);
            return DeleteRight;
        }

        //To check whether a string is a valid datetime or not
        public bool IsValidDateTime(string TestDateTime)
        {
            bool RetValue = false;
            try
            {
                DateTime Temp = DateTime.Parse(TestDateTime);
                RetValue = true;
            }
            catch (Exception ex)
            {
            }
            return RetValue;
        }

        //To generate a random password of required length
        public string GetRandomPwd(int Length)
        {
            string RetValue = "";
            Random randomGenerator = new Random();
            for (int i = 1; i <= Length; i++)
            {
                string Temp1 = (i - 1).ToString() + (i + 1).ToString() + (i + 2).ToString() + (i + 3).ToString(), Temp2 = (i + 5).ToString() + (i + 5).ToString() + (i + 4).ToString() + (i + 3).ToString();
                RetValue = RetValue + GetRandomNum(int.Parse(Temp1.ToString()), int.Parse(Temp2.ToString()));
            }
            return RetValue;
        }

        //To generate a random integer between two values
        public string GetRandomNum(int MinValue, int MaxValue)
        {
            Random randomGenerator = new Random();
            return (randomGenerator.Next(MinValue, MaxValue).ToString());
        }

        //To Change login password of a user
        public string ChangeLoginPwd(string User_ID, TextBox OldPass, TextBox NewPass, TextBox CNewPass, Panel Panel1)
        {
            string RValue = "";
            if (OldPass.Text.Trim().Length <= 0)
            {
                RValue = "Please! Enter Current Login Password.";
                OldPass.Focus();
            }
            else if (NewPass.Text.Trim().Length <= 0)
            {
                RValue = "Please! Enter New Login Password.";
                NewPass.Focus();
            }
            else if (CNewPass.Text.Trim().Length <= 0)
            {
                RValue = "Please! Re-enter New Login Password.";
                CNewPass.Focus();
            }
            else if (NewPass.Text.Trim() != CNewPass.Text.Trim())
            {
                RValue = "New Password and Confirm new Password must be same. Please! Enter again.";
                CNewPass.Focus();
            }
            else
            {
                
                string Res = "";
                HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
                H1EFObj1.PRC_ChangeUserPwd(long.Parse(User_ID.ToString()), OldPass.Text.Trim(), NewPass.Text.Trim(), ref Res);
                RValue = Res;
                if (RValue == "Login Password has been changed successfully.")
                {
                    Panel1.Visible = false;
                }
            }
            return RValue;
        }

        //To Bind a C1Combobox with User Types
        public void BindUserTypesLoginPage(C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBox   DDL, long UserType_ID)
        {
            HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
            DDL.DataSource = H1EFObj1.PRC_GetUserTypeList_LoginPage(0);
            DDL.DataTextField = "UserType";
            DDL.DataValueField = "UserType_ID";
            DDL.DataBind();
            DDL.Items.Insert(0, new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("User Type", "0"));
            H1EFObj1.Dispose();
        }

        //To Bind a C1 Combobox with blood groups
        public void BindBloodGroup(C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBox DDL, long BG_ID, string MandateStatus)
        {
            HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
            DDL.DataSource = H1EFObj1.PRC_GetBloodGroupList(BG_ID);
            DDL.DataTextField = "BloodGroup";
            DDL.DataValueField = "BG_ID";
            DDL.DataBind();
            if (MandateStatus == "0")
            {
                DDL.Items.Insert(0, new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("N/A", "0"));
            }
        }

        //To Bind a C1 Combobox with countries
        public void BindCountry(C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBox DDL, long Country_ID, string MandateStatus)
        {
            HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
            DDL.DataSource = H1EFObj1.PRC_GetCountryList(Country_ID,0);
            DDL.DataTextField = "Country_Name";
            DDL.DataValueField = "Country_ID";
            DDL.DataBind();
            if (MandateStatus == "0")
            {
                DDL.Items.Insert(0, new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("N/A", "0"));
            }           
        }

        //To Bind a C1 Combobox with diagnosis status
        public void BindDiagnosisStatus(C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBox DDL, long Diagnosis_Status_ID, string MandateStatus)
        {
            HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
            DDL.DataSource = H1EFObj1.PRC_GetDiagnosisStatusList(Diagnosis_Status_ID);
            DDL.DataTextField = "Diagnosis_Status";
            DDL.DataValueField = "Diagnosis_Status_ID";
            DDL.DataBind();
            if (MandateStatus == "0")
            {
                DDL.Items.Insert(0, new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("N/A", "0"));
            }
        }

        //To Bind a C1 Combobox with diseases
        public void BindDisease(C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBox DDL, long Disease_ID, string MandateStatus)
        {
            HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
            DDL.DataSource = H1EFObj1.PRC_GetDiseaseList(Disease_ID);
            DDL.DataTextField = "Disease_Name";
            DDL.DataValueField = "Disease_ID";
            DDL.DataBind();
            if (MandateStatus == "0")
            {
                DDL.Items.Insert(0, new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("N/A", "0"));
            }
       }

        //To Bind a C1 Combobox with cities
        public void BindCity(C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBox DDL, long City_ID, long State_ID, string MandateStatus)
        {
            HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
            DDL.DataSource = H1EFObj1.PRC_GetCityList(City_ID, State_ID);
            DDL.DataTextField = "City_Name";
            DDL.DataValueField = "City_ID";
            DDL.DataBind();
            if (MandateStatus == "0")
            {
                DDL.Items.Insert(0, new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("N/A", "0"));
            }
       }

        //To Bind a C1 Combobox with profession
        public void BindProfession(C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBox DDL, long Profession_ID, string MandateStatus)
        {
            HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
            DDL.DataSource = H1EFObj1.PRC_GetProfessionList(Profession_ID);
            DDL.DataTextField = "Profession_Name";
            DDL.DataValueField = "Profession_ID";
            DDL.DataBind();
            if (MandateStatus == "0")
            {
                DDL.Items.Insert(0, new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("N/A", "0"));
            }
        }

        //To Bind a C1 Combobox with qualification
        public void BindQualification(C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBox DDL, long Qualification_ID, string MandateStatus)
        {
            HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
            DDL.DataSource = H1EFObj1.PRC_GetQualificationList(Qualification_ID);
            DDL.DataTextField = "Qualification";
            DDL.DataValueField = "Qualification_ID";
            DDL.DataBind();
            if (MandateStatus == "0")
            {
                DDL.Items.Insert(0, new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("N/A", "0"));
            }
        }

        //To Bind a C1 Combobox with specialities
        public void BindSpecialist(C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBox DDL, long Specialist_ID, string MandateStatus)
        {
            HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
            DDL.DataSource = H1EFObj1.PRC_GetSpecialistList(Specialist_ID);
            DDL.DataTextField = "Specialist";
            DDL.DataValueField = "Specialist_ID";
            DDL.DataBind();
            if (MandateStatus == "0")
            {
                DDL.Items.Insert(0, new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("N/A", "0"));
            }           
        }

        //To Bind a C1 Combobox with states
        public void BindState(C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBox DDL, long State_ID, long County_ID, string MandateStatus)
        {
            HospitalOneDataContext H1EFObj1 = new HospitalOneDataContext();
            DDL.DataSource = H1EFObj1.PRC_GetStateList(State_ID, County_ID);
            DDL.DataTextField = "State_Name";
            DDL.DataValueField = "State_ID";
            DDL.DataBind();
            if (MandateStatus == "0")
            {
                DDL.Items.Insert(0, new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("N/A", "0"));
            }
        }

        //To Bind a C1 Combobox with doctors
        public void BindDoctor(C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBox DDL, long User_ID, string MandateStatus)
        {
            
            DDL.DataSource = SessionDBClass.Current.UserList.Where(x => (x.UserType_ID == 2) & (User_ID == 0 | x.User_ID == User_ID));//H1EFObj1.PRC_GetDoctorList(User_ID, "", null, null, "", "", 0);
            DDL.DataTextField = "User_Name";
            DDL.DataValueField = "User_ID";
            DDL.DataBind();
            if (MandateStatus == "0")
            {
                DDL.Items.Insert(0, new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("N/A", "0"));
            }
        }

        //To Bind a C1 Combobox with nurses
        public void BindNurse(C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBox DDL, long User_ID, string MandateStatus)
        {
            
            DDL.DataSource = SessionDBClass.Current.UserList.Where(x => (x.UserType_ID == 3) & (User_ID == 0 | x.User_ID == User_ID));//H1EFObj1.PRC_GetNurseList(User_ID, "", null, null, "", "", 0);
            DDL.DataTextField = "User_Name";
            DDL.DataValueField = "User_ID";
            DDL.DataBind();
            if (MandateStatus == "0")
            {
                DDL.Items.Insert(0, new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("N/A", "0"));
            }
        }

        //To Bind a C1 Combobox with patients
        public void BindPatient(C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBox DDL, long User_ID, string MandateStatus)
        {
            
            DDL.DataSource = SessionDBClass.Current.UserList.Where(x => (x.UserType_ID == 4) & (User_ID == 0 | x.User_ID == User_ID));//H1EFObj1.PRC_GetPatientList(User_ID, "", null, null, "", "", 0);
            DDL.DataTextField = "User_Name";
            DDL.DataValueField = "User_ID";
            DDL.DataBind();
            if (MandateStatus == "0")
            {
                DDL.Items.Insert(0, new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("N/A", "0"));
            }
        }

        //To Bind a C1 Combobox with users for dashboard
        public void BindUserForDashboard(C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBox DDL, long User_ID, string MandateStatus)
        {
            
            DDL.DataSource = GetUserList_Dashboard(0, "", 0);
            DDL.DataTextField = "User_Name_WithType";
            DDL.DataValueField = "User_ID";
            DDL.DataBind();
            if (MandateStatus == "0")
            {
                DDL.Items.Insert(0, new C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxItem("N/A", "0"));
            }
        }

        public List<PRC_GetUserListResult> GetUserList_Dashboard(long User_ID, string User_Name, long UserType_ID)
        {
            return (SessionDBClass.Current.UserList.Where(x => (x.UserType_ID > 1 & (User_ID == 0 | x.User_ID == User_ID) & (User_Name == "" | x.User_Name.Contains(User_Name)) & (UserType_ID == 0 | x.UserType_ID == UserType_ID))).ToList());
        }

    }
}