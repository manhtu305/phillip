﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.IO;
using Grapecity.Samples.HospitalOne.H1DBService_DBML;
using C1.Web.Wijmo.Controls.C1ComboBox;
using System.Data;

namespace Grapecity.Samples.HospitalOne.H1ASPNet
{
    public partial class UserViewOldAppointments : System.Web.UI.Page
    {
        UIBinder UIBObj1 = new UIBinder();
        long Reg_ID = 0;

        //To check page rights for current user
        protected void CheckPageRights()
        {
            if (Session["UserType_ID"] == null || Session["UserType"] == null || Session["User_ID"] == null)
            {
                Response.Redirect("Logout.aspx");
            }
            if (UIBObj1.HasRightsForShow(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                Session["Msg"] = "Sorry! You are not authorized to view old appointments.";
                Response.Redirect("UserShowMsg.aspx");
            }            
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                CheckPageRights();
                if (!IsPostBack)
                {
                    SetPageLoad();
                }
            }
            catch (Exception ex)
            {
                LblMsg.Text = "Error occurred: " + ex.Message;
            }
        }

        //To set page for first time loading
        protected void SetPageLoad()
        {
            string ListType = "";
            if (Request.QueryString["LT"] != null)
            {
                ListType = Request.QueryString["LT"].ToString();
            }
            Label MPLblPageTitle = (Label)Master.FindControl("LblPageTitle");
            if (MPLblPageTitle != null)
            {
                
                if (ListType == "R")
                {
                    MPLblPageTitle.Text = "Resolved Diagnosis";
                }
                else
                {
                    MPLblPageTitle.Text = "Open Diagnosis";
                }
            }

            UIBObj1.BindPatient(C1ddlSearchName, 0, "1");            
            PnlAppointmentsList.Visible = false;
            if (Session["UserType_ID"].ToString() == "4")
            {
                PnlSearchPatient.Visible = false;
                HFPatientID.Value = Session["User_ID"].ToString();
                HFPatientName.Value = Session["User_Name"].ToString();
                FillC1GVOldAppointments(long.Parse(HFPatientID.Value));
                PnlAppointmentsList.Visible = true;
            }
            else
            {
                PnlSearchPatient.Visible = true;
                if (C1ddlSearchName.Items.Count > 0)
                {
                    C1ddlSearchName.SelectedValue = "102031";
                    SearchPatient_SelectionChanged();
                }
            }
        }

        //To open details of selected patient's appointments
        protected void C1ddlSearchName_SelectedIndexChanged(object sender, C1.Web.Wijmo.Controls.C1ComboBox.C1ComboBoxEventArgs args)
        {
            C1DialogAddViewEditAppointment.ShowOnLoad = false;
            SearchPatient_SelectionChanged();
        }

        //To open details of selected patient's appointments
        protected void SearchPatient_SelectionChanged()
        {
            LblMsg.Text = null;
            if (C1ddlSearchName.SelectedIndex >= 0)
            {
                HFPatientID.Value = C1ddlSearchName.SelectedValue;
                HFPatientName.Value = C1ddlSearchName.SelectedItem.Text;
                FillC1GVOldAppointments(long.Parse(HFPatientID.Value));
                PnlAppointmentsList.Visible = true;
            }
            else
            {
                HFPatientID.Value = null;
                C1GVOldAppointments.DataSource = null;
                C1GVOldAppointments.DataBind();
                PnlAppointmentsList.Visible = false;
                LblMsg.ForeColor = System.Drawing.Color.Gray;
                LblMsg.Text = "Select a Patient to view Appointment List.";
            }
        }

        //To fill appointement data of selected/current patient in C1GridView
        protected void FillC1GVOldAppointments(long Patient_ID)
        {            
            string ListType = "", EqualStr = "", NEqualStr = "";
            if (Request.QueryString["LT"] != null)
            {
                ListType = Request.QueryString["LT"].ToString();
            }
            Label MPLblPageTitle = (Label)Master.FindControl("LblPageTitle");
            if (MPLblPageTitle != null)
            {
                if (ListType == "R")
                {
                    MPLblPageTitle.Text = "Resolved Diagnosis";
                    EqualStr = "Resolved";
                    NEqualStr = "";

                }
                else
                {
                    MPLblPageTitle.Text = "Open Diagnosis";
                    EqualStr = "";
                    NEqualStr = "Resolved";
                }
            }
            
            C1GVOldAppointments.DataSource = UIBObj1.GetPatientAppointmentsList(0, Patient_ID, 0, null, EqualStr, NEqualStr);//H1EFObj1.PRC_GetPatientAppointmentList(0, Patient_ID, 0, null, EqualStr, NEqualStr);
            C1GVOldAppointments.DataBind();
            if (UIBObj1.HasRightsForShow(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                C1GVOldAppointments.Columns[0].Visible = false;
            }
            if (UIBObj1.HasRightsForUpdate(long.Parse(Session["User_ID"].ToString()), UIBObj1.GetMenuID(Path.GetFileName(Request.PhysicalPath))) != "1")
            {
                C1GVOldAppointments.Columns[1].Visible = false;
            }
        }

        //To handle event of page index changing of C1GridView
        protected void C1GVOldAppointments_PageIndexChanging(object sender, C1.Web.Wijmo.Controls.C1GridView.C1GridViewPageEventArgs e)
        {
            C1GVOldAppointments.PageIndex = e.NewPageIndex;
            FillC1GVOldAppointments(long.Parse(HFPatientID.Value));
        }

        //To open details of selected appointment in edit mode
        protected void ImgBtn_C1GVOldAppointments_EditAppointment_Click(object sender, ImageClickEventArgs e)
        {
            LblMsg.Text = LblMsgDialog.Text = null;
            ImageButton LBtn1 = sender as ImageButton;
            if (LBtn1 != null)
            {
                long Doctor_ID = 0;
                string id = LBtn1.CommandArgument;
                if (HFPatientID.Value.Length > 0)
                {                 
                    OpenAppointmentDialog("EDIT", long.Parse(HFPatientID.Value), Doctor_ID, long.Parse(id));
                }
            }
        }

        //To open details of selected appointment in view mode
        protected void ImgBtn_C1GVOldAppointments_ViewAppointment_Click(object sender, ImageClickEventArgs e)
        {
            LblMsg.Text = LblMsgDialog.Text = null;
            ImageButton LBtn1 = sender as ImageButton;
            if (LBtn1 != null)
            {
                long Doctor_ID = 0;
                string id = LBtn1.CommandArgument;
                if (HFPatientID.Value.Length > 0)
                {
                    OpenAppointmentDialog("VIEW", long.Parse(HFPatientID.Value), Doctor_ID, long.Parse(id));
                }
            }
        }

        //To open appointment dialog
        protected void OpenAppointmentDialog(string DialogType, long Patient_ID, long Doctor_ID, long Appointment_ID)
        {
            UIBObj1.BindDoctor(C1ComboAssociateDoctor, 0, "1");
            UIBObj1.BindDiagnosisStatus(C1ComboDiagnosisStatus, 0, "1");
            HFAppointmentDialogType.Value = DialogType;
            HFAppointmentID.Value = Appointment_ID.ToString();
            HFAddAppPatientID.Value = Patient_ID.ToString();
            LblPatientNameAtAddApp.Text = HFPatientName.Value + " (" + HFAddAppPatientID.Value + ")";
            SetAppointmentDialog();
            if (DialogType == "NEW")
            {
                C1ComboAssociateDoctor.SelectedValue = Doctor_ID.ToString();
            }            
            C1DialogAddViewEditAppointment.Width = 870;
            C1DialogAddViewEditAppointment.ShowOnLoad = true;
        }

        //To set appointment dialog controls
        protected void SetAppointmentDialog()
        {
            LblMsg.ForeColor = LblMsgDialog.ForeColor = System.Drawing.Color.Gray;
            ClearAppointmentDialog();
            if (HFAppointmentDialogType.Value == "NEW")
            {
                C1DialogAddViewEditAppointment.Title = "Add Appointment (" + HFPatientName.Value + "-" + HFAddAppPatientID.Value + ")";
                PnlAddAppointmentBtns.Visible = true;
                BtnSubmit.Text = "Add";
            }
            else
            {
                List<PRC_GetPatientAppointmentListResult> Result1 = UIBObj1.GetPatientAppointmentsList(long.Parse(HFAppointmentID.Value), 0, 0, null, "", "");
                if (Result1.Count > 0)
                {
                    C1ComboAssociateDoctor.SelectedValue = Result1[0].Doctor_ID.ToString();
                    C1InpDateAppointmentSchedule.Date = Result1[0].Appointment_DateTime;
                    C1InNumHeight.Value = double.Parse(Result1[0].Height.ToString());
                    C1InNumWeight.Value = double.Parse(Result1[0].Weight.ToString());
                    C1InNumTemperature.Value = double.Parse(Result1[0].Temperature.ToString());
                    C1InNumHeartRate.Value = double.Parse(Result1[0].Heart_Rate.ToString());
                    C1InNumSysBP.Value = double.Parse(Result1[0].Systolic_BP.ToString());
                    C1InNumDiastBP.Value = double.Parse(Result1[0].Diastolic_BP.ToString());
                    C1ComboDiagnosisStatus.SelectedValue = Result1[0].Diagnosis_Status_ID.ToString();
                    TxtAppointmentNotes.Text = Result1[0].Notes;
                    if (HFAppointmentDialogType.Value == "EDIT")
                    {
                        C1DialogAddViewEditAppointment.Title = "Edit (" + HFPatientName.Value + "-" + HFAddAppPatientID.Value + ")";
                        PnlAddAppointmentBtns.Visible = true;
                        BtnSubmit.Text = "Update";
                    }
                    if (HFAppointmentDialogType.Value == "VIEW")
                    {
                        C1DialogAddViewEditAppointment.Title = "View (" + HFPatientName.Value + "-" + HFAddAppPatientID.Value + ")";
                        PnlAddAppointmentBtns.Visible = false;
                        BtnSubmit.Text = "";
                    }
                }
                else
                {
                    LblMsg.Text = LblMsgDialog.Text = "No Appointment has been selected. Please! select an Appointment to view/edit.";
                }
            }
        }

        //To reset/clear controls of appointment C1dialog
        protected void ClearAppointmentDialog()
        {
            C1InpDateAppointmentSchedule.Date = null;
            C1InNumHeight.Value = 0;
            C1InNumWeight.Value = 0;
            C1InNumTemperature.Value = 0;
            C1InNumHeartRate.Value = 0;
            C1InNumSysBP.Value = 0;
            C1InNumDiastBP.Value = 0;
            C1ComboDiagnosisStatus.SelectedIndex = 0;
            TxtAppointmentNotes.Text = null;
        }

        //To check whether C1dialog data is valid for update or not
        protected bool IsValidForSaveUpdate()
        {
            LblMsgDialog.ForeColor = System.Drawing.Color.Red;
            bool RValue = false;
            if (!UIBObj1.IsValidDateTime(C1InpDateAppointmentSchedule.Date.ToString()))
            {
                LblMsgDialog.Text = "Please! Select a valid Appointment Schedule.";
                C1InpDateAppointmentSchedule.Focus();
                return RValue;
            }
            if (UIBObj1.CheckAppointmentBeforeInsertUpdate(Reg_ID, long.Parse(HFAddAppPatientID.Value), long.Parse(C1ComboAssociateDoctor.SelectedValue), C1InpDateAppointmentSchedule.Date) > 0)
            {
                LblMsgDialog.Text = "Requested Schedule is not free. Please! Try another schedule.";
                C1InpDateAppointmentSchedule.Focus();
                return RValue;
            }
            RValue = true;
            return RValue;
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                LblMsg.Text = LblMsgDialog.Text = null;
                Reg_ID = 0;
                //To Insert New Appointment Record
                if (BtnSubmit.Text == "Add")
                {
                    if (IsValidForSaveUpdate())
                    {
                        long Result = 0;
                        string ErrorMsg = "";
                        UIBObj1.AppointmentInsertUpdate(0, long.Parse(HFAddAppPatientID.Value), long.Parse(C1ComboAssociateDoctor.SelectedValue), C1InpDateAppointmentSchedule.Date, C1InpDateAppointmentSchedule.Date, C1InNumHeight.Value, C1InNumWeight.Value, C1InNumTemperature.Value, C1InNumHeartRate.Value, C1InNumSysBP.Value, C1InNumDiastBP.Value, long.Parse(C1ComboDiagnosisStatus.SelectedValue), TxtAppointmentNotes.Text.Trim(), long.Parse(Session["User_ID"].ToString()), "Active", out Result, out  ErrorMsg);
                        Reg_ID = (long)Result;
                        if (Result > 0)
                        {
                            LblMsgDialog.ForeColor = LblMsg.ForeColor = System.Drawing.Color.Gray;
                            LblMsg.Text = "Appointment saved successfully.";
                            C1DialogAddViewEditAppointment.ShowOnLoad = false;
                            FillC1GVOldAppointments(long.Parse(HFPatientID.Value));
                        }
                        else
                        {
                            LblMsgDialog.ForeColor = System.Drawing.Color.Red;
                            LblMsgDialog.Text = "Error occurred: " + ErrorMsg;
                        }
                    }
                }
                //To Update an existing Appointment Record
                else if (BtnSubmit.Text == "Update")
                {
                    Reg_ID = long.Parse(HFAppointmentID.Value);
                    if (IsValidForSaveUpdate())
                    {
                        long Result = 0;
                        string ErrorMsg = "";
                        UIBObj1.AppointmentInsertUpdate(Reg_ID, long.Parse(HFAddAppPatientID.Value), long.Parse(C1ComboAssociateDoctor.SelectedValue), C1InpDateAppointmentSchedule.Date, C1InpDateAppointmentSchedule.Date, C1InNumHeight.Value, C1InNumWeight.Value, C1InNumTemperature.Value, C1InNumHeartRate.Value, C1InNumSysBP.Value, C1InNumDiastBP.Value, long.Parse(C1ComboDiagnosisStatus.SelectedValue), TxtAppointmentNotes.Text.Trim(), long.Parse(Session["User_ID"].ToString()), "Active", out Result, out ErrorMsg);
                        Reg_ID = (long)Result;
                        if (Result != -1)
                        {
                            LblMsgDialog.ForeColor = LblMsg.ForeColor = System.Drawing.Color.Gray;
                            LblMsg.Text = "Appointment updated successfully.";
                            C1DialogAddViewEditAppointment.ShowOnLoad = false;
                            FillC1GVOldAppointments(long.Parse(HFPatientID.Value));
                        }
                        else
                        {
                            LblMsgDialog.ForeColor = System.Drawing.Color.Red;
                            LblMsgDialog.Text = "Error occurred: " + ErrorMsg;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LblMsgDialog.Text = ex.Message;
            }            
        }

        protected void BtnReset_Click(object sender, EventArgs e)
        {
            ClearAppointmentDialog();
        }

        protected void ImgBtnSearchUserList_Click(object sender, ImageClickEventArgs e)
        {

        }
    
    
    }
}