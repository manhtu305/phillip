ASP.NET WebForms HospitalOne Sample
-----------------------------------------------------------------------
An application using Studio for ASP.NET Web Forms controls for medical vertical. This sample requires VS2012 and SQLExpress

This application uses a database with patient records. A user will be able to search the database for all of the patients doctors in the DB. 
Once a user selects a doctor, it will load all of that doctors patients for the day in a carousel.    
The doctor can then tap or click on a patient.  Once this is done, a dashboard like view will appear.
	All of the patients personal information will appear for viewing.   
	An Edit menu will be present at the top which will allow the doctor to edit all of the personal details. 
	A grid will appear that lists all of the customers inprocess diagnostics.   There also will be an option to schedule another appointment.  
	A doctor will be able to view old appointments in the eventscalender and view the details of that appointment. 

Once the user searches for a patient, they are all displayed in the carousel view.   Once a patient has been selected from the carousel view, 
it will display all of the contact info in the grid
	All of the patients contact info will be displayed.   SSN/Address will be blacked out.
	List of all open diagnostics in the grid. 
	Historical appointments in an EvenstCalender.

Schedule Appointments
Only a doctor/Nurse will be able to schedule the appointments. The button will only appear if a doctor or nurse was searched for. 
View old appointments
The old appointments will appear in the grid with paging. 
Dashboard
The dashboard  consists of following items (or from where the following activities can be performed):
	Search for a doctor
	Search for a patient
	Quick button to schedule an appointment
	List of appointments across departments/doctors for a specific period [4 hour window - an hour before current time, to next 3 hours]
On selecting any of the option, the user will be navigated to another page, or some other section of same page to perform the operation.
