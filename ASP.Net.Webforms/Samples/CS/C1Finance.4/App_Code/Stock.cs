﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Text;
using System.IO;
using System.Globalization;

/// <summary>
/// Summary description for Stock
/// </summary>
public class Stock
{
    public Stock()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public float Open { get; set; }
    public float Close { get; set; }
    public float High { get; set; }
    public float Low { get; set; }
    public float Volume { get; set; }
    public float PE { get; set; }
    public bool Valid { get; set; }
    public string Symbol { get; set; }
    public string Name { get; set; }
    public DateTime Date { get; set; }
}
public class Stocks
{
    public static List<Stock> GetStocks(string Symbols)
    {
        List<Stock> lst = new List<Stock>();
        
        var fmt = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={0}&apikey=EQ8R2LTG732VP7HE&datatype=csv&outputsize=full";

        var startDate = new DateTime(2018, 1, 1);

        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072; //TLS 1.2
        foreach (var symbol in Symbols.Split(','))
        {
            var url = string.Format(fmt, symbol);

            // get content
            var sb = new StringBuilder();
            var wc = new WebClient();

            IWebProxy defaultProxy = WebRequest.DefaultWebProxy;
            if (defaultProxy != null)
            {
                defaultProxy.Credentials = CredentialCache.DefaultCredentials;
                wc.Proxy = defaultProxy;
            }

            using (var sr = new StreamReader(wc.OpenRead(url)))
            {
                // skip headers
                sr.ReadLine();

                var line = sr.ReadLine();
                if (line != null && !line.Contains("alphavantage"))
                {
                    var items = line.Split(',');

                    var stock = new Stock();
                    stock.Open = float.Parse(items[1], CultureInfo.InvariantCulture);
                    stock.Close = float.Parse(items[4], CultureInfo.InvariantCulture);
                    stock.High = float.Parse(items[2], CultureInfo.InvariantCulture);
                    stock.Low = float.Parse(items[3], CultureInfo.InvariantCulture);
                    stock.Volume = float.Parse(items[5], CultureInfo.InvariantCulture);
                    stock.Valid = true;
                    stock.Symbol = symbol;
                    stock.Date = DateTime.Parse(items[0], CultureInfo.InvariantCulture);
                    if (stock.Date <= DateTime.Now && stock.Date >= startDate)
                    lst.Add(stock);
                }
            }
        }

        return lst;
    }

    public static List<Stock> GetStockHistory(string symbol, DateTime startDate, DateTime endDate)
    {
        List<Stock> list = new List<Stock>();

       
        var fmt = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={0}&apikey=EQ8R2LTG732VP7HE&datatype=csv&outputsize=full";

        var url = string.Format(fmt, symbol);

        // get content
        var sb = new StringBuilder();
        var wc = new WebClient();

        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072; //TLS 1.2
        IWebProxy defaultProxy = WebRequest.DefaultWebProxy;
        if (defaultProxy != null)
        {
            defaultProxy.Credentials = CredentialCache.DefaultCredentials;
            wc.Proxy = defaultProxy;
        }

        using (var sr = new StreamReader(wc.OpenRead(url)))
        {
            // skip headers
            sr.ReadLine();

            int i = 0;
            // read each line
            for (var line = sr.ReadLine(); line != null; line = sr.ReadLine())
            {
                var items = line.Split(',');

                var stock = new Stock();
                stock.Open = float.Parse(items[1], CultureInfo.InvariantCulture);
                stock.Close = float.Parse(items[4], CultureInfo.InvariantCulture);
                stock.High = float.Parse(items[2], CultureInfo.InvariantCulture);
                stock.Low = float.Parse(items[3], CultureInfo.InvariantCulture);
                stock.Volume = float.Parse(items[5], CultureInfo.InvariantCulture);
                stock.Valid = true;
                stock.Symbol = symbol;
                stock.Date = DateTime.Parse(items[0], CultureInfo.InvariantCulture);

                if(stock.Date<=DateTime.Now && stock.Date >= startDate)
                list.Add(stock);
            }
        }

        return list;
    }
}