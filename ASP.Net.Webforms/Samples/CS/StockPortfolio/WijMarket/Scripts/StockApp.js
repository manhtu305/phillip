﻿var stockApp = {};
stockApp.symbols = "";
stockApp.currentSymbol = "";
stockApp.minDate = "";
stockApp.minDay = 1;
stockApp.maxDate = "";
stockApp.maxDay = null;
stockApp.beginDate = "";
stockApp.endDate = "";
stockApp.timeSpan = 2000;
stockApp.isRefresh = false;
stockApp.data = [];
stockApp.newStock = null;

stockApp.init = function () {
	var d = new Date(), innerSet = false, minDate = new Date();

	stockApp.symbols = "MSFT,GOOG,YHOO,AAPL,INTC,NVDA,ORCL,GE,AMD";
	stockApp.currentSymbol = "MSFT";

	minDate.setMonth(minDate.getMonth() - 3)
	stockApp.minDate = minDate.toDateString();
	stockApp.minDay = 1;
	stockApp.maxDate = d.toDateString();
	stockApp.maxDay = stockApp.getDaysCount(stockApp.maxDate, stockApp.minDate)
	stockApp.beginDay = stockApp.maxDay - 30;
	stockApp.beginDate = stockApp.getDateFromNumber(stockApp.minDate, stockApp.beginDay);
	stockApp.endDay = stockApp.maxDay;
	stockApp.endDate = stockApp.getDateFromNumber(stockApp.minDate, stockApp.endDay);

	/* Add Stock */
	$("#addStock").button().click(function () {
		var isExsit = false;
		$.each(stockApp.data, function (i, n) {
			if (n.Symbol === stockApp.newStock) {
				isExsit = true;
				return false;
			}
		});
		if (!isExsit) {
			$.ajax({
				url: "StockSymbols.ashx",
				data: { symbols: stockApp.newStock },
				dataType: "json",
				success: function (d) {
					var data;
					if (d.length === 1) {
						data = d[0];
						stockApp.data.push(data);
						$("#stockList").wijgrid("data").push(data);
						$("#stockList").wijgrid("ensureControl", true);
						stockApp.symbols += "," + stockApp.newStock;
					}
					stockApp.newStock = null;
				}
			});
		}
		else {
			alert("This stock is exsit!");
		}
		return false;
	});

	var proxy = new wijhttpproxy({
		url: "StockSuggestion.ashx",
		dataType: "json",
		data: { count: 20 }
	});

	var myReader = new wijarrayreader([{
		name: 'label',
		mapping: function (item) {
			return item.Symbol + " (" + item.Name + ")";
		}
	}, {
		name: 'value',
		mapping: 'Symbol'
	}, {
		name: 'selected',
		defaultValue: false
	}]);

	var datasource = new wijdatasource({
		reader: myReader,
		proxy: proxy
	});

	$("#autocomplete").wijcombobox({
		data: datasource,
		minLength: 1,
		showTrigger: false,
		search: function (e, obj) {
			obj.datasrc.proxy.options.data.symbol = obj.term.value;
		},
		select: function (e, item) {
			stockApp.newStock = item.value;
		}
	});
	/* End Add Stock */

	/* Date range */
	$("#showDateDialog").button({
		icons: { primary: "ui-icon-triangle-1-s" },
		text: false
	}).click(function () {
		var dialog = $("#dateRangeDialog"), btn = $(this), left;
		if (dialog.is(":hidden")) {
			left = btn.offset().left + btn.width() - dialog.width();
			dialog.fadeIn();
			dialog.css({ left: left });
		}
		else {
			dialog.fadeOut();
		}
		return false;
	});

	$("#calendar1").wijcalendar({
		monthRows: 1,
		monthCols: 4,
		displayDate: new Date(stockApp.minDate),
		minDate: new Date(stockApp.minDate),
		maxDate: new Date(stockApp.maxDate),
		selectedDatesChanged: function (e, d) {
			var start, end, dates = $("#calendar1")
                .wijcalendar("option", "selectedDates"),
                length = dates.length;
			if (length) {
				start = new Date(dates[0]);
				end = new Date(dates[length - 1]);

				innerSet = true;
				$("#startDateInput").wijinputdate("option", "date", start);
				$("#endDateInput").wijinputdate("option", "date", end);
				innerSet = false;
			}
		}
	});

	var setCalendarDate = function () {
		var dates = [], startDate, endDate, d;
		startDate = new Date($("#startDateInput").wijinputdate("option", "date"));
		endDate = new Date($("#endDateInput").wijinputdate("option", "date"));

		for (d = startDate; d <= endDate; d.setDate(d.getDate() + 1)) {
			dates.push(new Date(d));
		}
		$("#calendar1").wijcalendar("option", "selectedDates", dates);
	}

	$("#startDateInput, #endDateInput").wijinputdate({
		dateChanged: function (e, d) {
			var minDate = new Date(stockApp.minDate),
			    maxDate = new Date(stockApp.maxDate);
			if (d.date.getTime() > maxDate.getTime()) {
				$(this).wijinputdate("option", "date", maxDate);
			}
			if (d.date.getTime() < minDate.getTime()) {
				$(this).wijinputdate("option", "date", minDate);
			}
			if (!innerSet) setCalendarDate();
		}
	});

	$("#dateApplyBtn").button().click(function () {
		var begin = $("#startDateInput").wijinputdate("option", "date"),
            end = $("#endDateInput").wijinputdate("option", "date"), f, t;

		stockApp.beginDate = begin.toDateString();
		stockApp.endDate = end.toDateString();
		stockApp.dateChange();

		$("#showDateRange span:eq(0)").text(stockApp.beginDate +
            " - " + stockApp.endDate);

		f = stockApp.getDaysCount(stockApp.beginDate, stockApp.minDate);
		t = stockApp.getDaysCount(stockApp.endDate, stockApp.minDate);
		$("#dateRange").wijslider("values", 0, f);
		$("#dateRange").wijslider("values", 1, t);

		$("#dateRangeDialog").fadeOut();
		return false;
	});

	$("#dateCancelBtn").button().click(function () {
		$("#dateRangeDialog").fadeOut();
		return false;
	});

	$("#dateRangeDialog").hide();

	$("#dateRange").wijslider({
		min: stockApp.minDay,
		max: stockApp.maxDay,
		minRange: 1,
		range: true,
		step: 1,
		values: [stockApp.beginDay, stockApp.endDay],
		stop: function (event, ui) {
			stockApp.beginDay = $("#dateRange").wijslider("values", 0);
			stockApp.beginDate = stockApp.getDateFromNumber(stockApp.minDate, stockApp.beginDay);
			stockApp.endDay = $("#dateRange").wijslider("values", 1);
			stockApp.endDate = stockApp.getDateFromNumber(stockApp.minDate, stockApp.endDay);
			stockApp.dateChange();
			$("#showDateRange span:eq(0)").text(stockApp.beginDate +
                " - " + stockApp.endDate);
		},
		buttonClick: function (event, ui) {
			stockApp.beginDay = $("#dateRange").wijslider("values", 0);
			stockApp.beginDate = stockApp.getDateFromNumber(stockApp.minDate, stockApp.beginDay); ;
			stockApp.endDay = $("#dateRange").wijslider("values", 1);
			stockApp.endDate = stockApp.getDateFromNumber(stockApp.minDate, stockApp.endDay);
			stockApp.dateChange();
			$("#showDateRange span:eq(0)").text(stockApp.beginDate +
                " - " + stockApp.endDate);
		}
	});

	$("#showDateRange span:eq(0)").text(stockApp.beginDate +
                " - " + stockApp.endDate);
	/* End Date range */

	/* Chart */
	$("#stockHistory").wijlinechart({
		animation: {
			duration: 500,
			direction: "vertical"
		},
		seriesTransition: {
			duration: 500,
			direction: "vertical"
		},
		height: 300,
		hint: {
			contentStyle: { "font-size": "24pt", stroke: "none" },
			content: function () {
				return this.data.lineSeries.label + " : " + this.y;
			}
		},
		header: {
			text: "Stock History",
			visible: false
		},
		seriesStyles: [{ stroke: "#5F83B9", opacity: 0.9, "stroke-width": "3"}],
		seriesHoverStyles: [{ stroke: "#5F83B9", opacity: 1, "stroke-width": "4"}]
	});

	$("#stockFullHistory").wijlinechart({
		animation: {
			duration: 500,
			direction: "vertical"
		},
		height: 140,
		showChartLabels: false,
		header: {
			text: "Stock History",
			visible: false
		},
		legend: {
			visible: false
		},
		seriesStyles: [{ stroke: "#5F83B9", opacity: 0.9, "stroke-width": "3"}],
		seriesHoverStyles: [{ stroke: "#5F83B9", opacity: 1, "stroke-width": "4"}]
	});
	stockApp.loadStocks();
};

/* End Chart */

/* Grid */
var _numberParser = {
	parseDOM: function (value, culture, format, nullString) {
		return this.parse(value.innerHTML, culture, format, nullString);
	},
	parse: function (value, culture, format, nullString) {
		if (typeof (value) === "number") return value;
		if (!value || (value === "N/A") || (value === nullString)) {
			return null;
		}
		return NaN;
	},
	toStr: function (value, culture, format, nullString) {
		return (value === null) ? "N/A" : value.toString();
	}
}

var _getInnerHtml = function (value) {
	var content = $("<div class=\"cell-right\">"), img = $("<img>"),
	span = $("<span></span>").html("N/A").appendTo(content);
	img.hide();
	return content;
}

stockApp.loadStocks = function () {
	$.ajax({
		url: "StockSymbols.ashx",
		data: { symbols: stockApp.symbols },
		dataType: "json",
		success: function (data) {
			stockApp.data = data;
			$("#stockList").wijgrid({
				data: stockApp.data,
				allowSorting: true,
				columns: [
                        { dataKey: "Symbol" },
                        { dataKey: "Name" },
                        { dataKey: "Ask", dataType: "number", visible: true, dataParser: _numberParser },
                        { dataKey: "Bid", dataType: "number", visible: true, dataParser: _numberParser },
                        { dataKey: "LastSale", dataType: "number", visible: true, dataParser: _numberParser },
                        { dataKey: "Valid", visible: false }
                    ],
				selectionChanged: function (e, args) {
					stockApp.currentSymbol = $("#stockList").wijgrid("selection").selectedCells().item(0).value();
					stockApp.stockChange();
				},
				cellStyleFormatter: function (args) {
					var c = args.$cell;
					if ((args.column.dataKey === "Bid" ||
                        args.column.dataKey === "Ask" ||
                        args.column.dataKey === "LastSale") &&
                        args.state === 1 && args.row.dataRowIndex !== -1) {
						var content, value = args.row.data["LastSale"];
						content = _getInnerHtml(value);
						c.children("div.wijmo-wijgrid-innercell:eq(0)")
                            .wrapInner("<div class='cell-left'></div>")
                            .append(content).addClass("cell_" + args.row.data.Symbol +
                            "_" + args.column.dataKey);
					}
				}
			});
			stockApp.autoUpdate();
		}
	});
};
/* End Grid */

stockApp.loadStockHistory = function () {
	$.ajax({
		url: "StockSymbolHistoryByDates.ashx",
		data: { symbol: stockApp.currentSymbol, startDate: stockApp.beginDate, endDate: stockApp.endDate },
		dataType: "json",
		success: function (data) {
			var stockSeriesList = [];
			var stockDates = [];
			var stockPoints = [];
			if (data && data.length) {
				for (var i = 0; i < data.length - 1; i++) {
					stockDates.push(new Date(data[i].DateString));
					stockPoints.push(data[i].Close);
				}
				var stockSeries = {
					label: data[i].Symbol,
					legendEntry: true,
					data: {
						x: stockDates,
						y: stockPoints
					},
					markers: {
						visible: true,
						type: "circle"
					}
				};
				stockSeriesList.push(stockSeries);
				$("#stockHistory").wijlinechart("option", "seriesList", stockSeriesList);
			}
		}
	});
};

stockApp.loadStockFullHistory = function () {
	$.ajax({
		url: "StockSymbolHistoryByDates.ashx",
		dataType: "json",
		data: { symbol: stockApp.currentSymbol, startDate: stockApp.minDate, endDate: stockApp.maxDate },
		success: function (data) {
			var stockSeriesList = [];
			var stockDates = [];
			var stockPoints = [];

			if (data && data.length) {
				for (var i = 0; i < data.length - 1; i++) {
					stockDates.push(new Date(data[i].DateString));
					stockPoints.push(data[i].Close);
				}
				var stockSeries = {
					label: data[i].Symbol,
					legendEntry: true,
					data: {
						x: stockDates,
						y: stockPoints
					},
					markers: {
						visible: false,
						type: "circle"
					}
				};
				stockSeriesList.push(stockSeries);
				$("#stockFullHistory").wijlinechart("option", "seriesList", stockSeriesList);
			}
		}
	});
};

stockApp.loadStockSummary = function () {
	var toStr = function (val) {
		return val !== null ? val.toString() : "N/A";
	}
	$.ajax({
		url: "StockSymbol.ashx",
		data: { symbol: stockApp.currentSymbol },
		dataType: "json",
		success: function (data) {
			var changeText,
                lastTrade = toStr(data.LastSale),
                tradeTime = data.DateString.toLocaleString(),
                prevClose = toStr(data.Close),
                open = toStr(data.Open),
                name = toStr(data.Name),
                symbol = data.Symbol.toString(),
                bid = toStr(data.Bid),
                oneYear = toStr(data.OneyrTargetPrice),
                ask = toStr(data.Ask),
                dayLow = toStr(data.DayLow),
                dayHigh = toStr(data.DayHigh),
                yearLow = toStr(data.YearLow),
                yearHigh = toStr(data.YearHigh),
                volume = data.Volume.toString(),
                avgVol = data.AverageDailyVolume.toString(),
                marketCap = toStr(data.MarketCapitalization),
                pe = toStr(data.PE),
                EPS = toStr(data.EarningsShare),
                divYield = toStr(data.DividendYield),
                img, change = $("#summaryChange");


			$("#summaryLastTrade").text(lastTrade);
			$("#summaryTradeTime").text(tradeTime);
			$("#summaryPrevClose").text(prevClose);
			$("#summaryOpen").text(open);
			$("#summaryBid").text(bid);
			$("#summary1y").text(oneYear);

			$("#summaryAsk").text(ask);
			$("#summaryDayLow").text(dayLow);
			$("#summaryDayHigh").text(dayHigh);
			$("#summaryYearLow").text(yearLow);
			$("#summaryYearHight").text(yearHigh);
			$("#summaryVolume").text(volume);
			$("#summaryAvgVol").text(avgVol);
			$("#summaryMarketCap").text(marketCap);
			$("#summaryPE").text(pe);
			$("#summaryEPS").text(EPS);
			$("#summaryDivYield").text(divYield);
			$("#corpName").text(name);
			$("#symbolName").text(symbol);

			changeText = data.Change + "(" +
                data.ChangePercent + ")";
			change.children("span:eq(0)").text(changeText);
			img = $("#summaryChange>img:eq(0)");
			if (data.Change > 0) {
				change.removeClass().addClass("green");
				img.attr("src", "/images/up.png");
				img.show();
			}
			else if (data.Change < 0) {
				change.removeClass().addClass("red");
				img.attr("src", "/images/down.png");
				img.show();
			}
			else {
				change.removeClass()
				img.hide();
			}
		}
	});
};

// auto update
stockApp.autoUpdate = function () {
	var timeSpan = stockApp.timeSpan;

	if (!stockApp.isRefresh) {
		window.setTimeout(function () {
			$.ajax({
				url: "StockSymbols.ashx",
				data: { symbols: stockApp.symbols },
				dataType: "json",
				success: function (data) {
					stockApp.data = data;
					stockApp.updateGrid(data);
					stockApp.isRefresh = false;
					stockApp.autoUpdate();
				},
				error: function () {
					stockApp.isRefresh = false;
					stockApp.autoUpdate();
				}
			});
			stockApp.loadStockSummary();
		}, timeSpan);
	}
	stockApp.isRefresh = true;
};

stockApp.updateGrid = function (data) {
	$.each(data, function (i, n) {
		var symbol = n.Symbol;
		stockApp.updateCell(symbol, "Ask", n);
		stockApp.updateCell(symbol, "Bid", n);
		stockApp.updateCell(symbol, "LastSale", n);
	});
};

stockApp.updateCell = function (symbol, key, data) {
	var cell = $("#stockList").find(".cell_" + symbol + "_" + key + ":eq(0)"),
        left = cell.children(".cell-left:eq(0)"),
        right = cell.children(".cell-right:eq(0)"),
        old = parseFloat(left.text()),
        val = data[key], span, img, diff;

	left.text(val !== null ? val : "N/A");
	img = right.children("img:eq(0)");
	span = right.children("span:eq(0)");

	if (!isNaN(old) && val !== null) {
		diff = old - val;
		diff = Math.round(diff * Math.pow(10, 2)) / Math.pow(10, 2);
		span.text(diff.toString());
		if (diff > 0) {
			right.removeClass("red").addClass("green");
			img.show();
			img.attr("src", "/images/down.png");
		}
		else if (diff < 0) {
			right.removeClass("green").addClass("red");
			img.show();
			img.attr("src", "/images/up.png");
		}
		else {
			right.removeClass("red").removeClass("green")
			img.hide();
		}
	}
	else {
		span.text("N/A");
		right.removeClass("red").removeClass("green");
		img.hide();
	}
};

stockApp.dateChange = function () {
	stockApp.loadStockHistory();
};

stockApp.stockChange = function () {
	stockApp.loadStockFullHistory();
	stockApp.loadStockHistory();
	stockApp.loadStockSummary();
};

stockApp.getDaysCount = function (from, to) {
	var f = new Date(from), t = new Date(to);
	return (f.getTime() - t.getTime()) / (24 * 60 * 60 * 1000);
};

stockApp.getDateFromNumber = function (f, n) {
	var d = new Date(f), t;
	t = d.setDate(d.getDate() + n);
	return d.toDateString();
};