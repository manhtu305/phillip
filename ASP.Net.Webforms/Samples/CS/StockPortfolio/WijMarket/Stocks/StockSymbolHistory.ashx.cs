﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WijMarket;
using WijMarket.Models;


namespace WijMarket
{
	/// <summary>
	/// Summary description for StockSymbolHistory
	/// </summary>
	public class StockSymbolHistory : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "text/plain";

			string symbol = context.Request["symbol"];
			string dateRange = context.Request["dateRange"];

			var beginDate = new DateTime();
			var endDate = new DateTime();
			var interval = "d";
			switch (dateRange)
			{
				case "1d":
					beginDate = DateTime.Now.AddDays(-1);
					endDate = DateTime.Now;
					break;
				case "1w":
					beginDate = DateTime.Now.AddDays(-7);
					endDate = DateTime.Now;
					interval = "d";
					break;
				case "1m":
					beginDate = DateTime.Now.AddMonths(-1);
					endDate = DateTime.Now;
					interval = "d";
					break;
				case "2m":
					beginDate = DateTime.Now.AddMonths(-2);
					endDate = DateTime.Now;
					interval = "w";
					break;
				case "3m":
					beginDate = DateTime.Now.AddMonths(-3);
					endDate = DateTime.Now;
					interval = "w";
					break;
				case "4m":
					beginDate = DateTime.Now.AddMonths(-4);
					endDate = DateTime.Now;
					interval = "w";
					break;
				case "5m":
					beginDate = DateTime.Now.AddMonths(-5);
					endDate = DateTime.Now;
					interval = "w";
					break;
				case "6m":
					endDate = DateTime.Now;
					beginDate = DateTime.Now.AddMonths(-6);
					interval = "m";
					break;
				case "9m":
					beginDate = DateTime.Now.AddMonths(-9);
					endDate = DateTime.Now;
					interval = "m";
					break;
				case "1y":
					beginDate = DateTime.Now.AddYears(-1);
					endDate = DateTime.Now;
					interval = "m";
					break;
				case "2y":
					beginDate = DateTime.Now.AddYears(-2);
					endDate = DateTime.Now;
					interval = "m";
					break;
			}

			var stocks = Stocks.GetStockHistory(symbol, beginDate, endDate, interval);

			string result = JsonHelper.GetJson(stocks);
			context.Response.Write(result);
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}