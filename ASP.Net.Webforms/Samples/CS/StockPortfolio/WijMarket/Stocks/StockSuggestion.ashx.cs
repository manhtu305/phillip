﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WijMarket;
using WijMarket.Models;

namespace WijMarket
{
	/// <summary>
	/// Summary description for StockSuggestion
	/// </summary>
	public class StockSuggestion : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "text/plain";
			string symbol = context.Request["symbol"];
			int count = 10;
			int.TryParse(context.Request["count"], out count);
			var stocks = Stocks.GetStockSymbols(symbol, 3, count);

			string result = JsonHelper.GetJson(stocks);
			context.Response.Write(result);
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}