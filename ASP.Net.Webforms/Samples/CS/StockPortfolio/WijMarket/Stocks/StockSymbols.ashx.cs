﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WijMarket;
using WijMarket.Models;

namespace WijMarket
{
	/// <summary>
	/// Summary description for StockSymbols
	/// </summary>
	public class StockSymbols : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "text/plain";//application/json
			
			string symbols = context.Request["symbols"];
			var stocks = Stocks.GetStocks(symbols);

			string result = JsonHelper.GetJson(stocks);
			context.Response.Write(result);
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}