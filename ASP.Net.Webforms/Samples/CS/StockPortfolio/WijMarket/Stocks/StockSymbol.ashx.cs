﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WijMarket;
using WijMarket.Models;

namespace WijMarket
{
	/// <summary>
	/// Summary description for StockSymbol
	/// </summary>
	public class StockSymbol : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "text/plain";

			string symbol = context.Request["symbol"];
			var stock = Stocks.GetStock(symbol);

			string result = JsonHelper.GetJson(stock);
			context.Response.Write(result);

		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}