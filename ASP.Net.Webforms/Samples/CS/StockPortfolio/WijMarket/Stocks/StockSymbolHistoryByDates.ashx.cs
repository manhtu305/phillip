﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WijMarket;
using WijMarket.Models;

namespace WijMarket
{
	/// <summary>
	/// Summary description for StockSymbolHistoryByDates
	/// </summary>
	public class StockSymbolHistoryByDates : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			context.Response.ContentType = "text/plain";

			string symbol = context.Request["symbol"];
			string startDate = context.Request["startDate"];
			string endDate = context.Request["endDate"];

			var stocks = Stocks.GetStockHistory(symbol, DateTime.Parse(startDate), DateTime.Parse(endDate), "d");
			string result = JsonHelper.GetJson(stocks);
			context.Response.Write(result);
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}