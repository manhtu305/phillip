using System.ComponentModel;
using System.Drawing;
using System.Web.UI;


namespace C1.Web.Wijmo.Controls.C1Input
{
    /// <summary>
    /// Web control derived from C1NumericInput specialized for 
    /// editing percent values. 
    /// Using the numeric editor, you can specify input without 
    /// writing any custom validation logic in your application.
    /// </summary>
    [ToolboxData("<{0}:C1InputPercent runat=server></{0}:C1InputPercent>")]
    [ToolboxBitmap(typeof(C1InputPercent), "Number.C1InputPercent.png")]
    [LicenseProviderAttribute]
    public class C1InputPercent : C1InputNumber
    {
        /// <summary>
        /// Initializes a new instance of the C1InputPercent class.
        /// </summary>
        public C1InputPercent() : base()
        {
        }

        /// <summary>
        /// Determines the type of the number input.
        /// </summary>
        public override NumberType NumberType
        {
            get
            {
                return NumberType.Percent;
            }
        }

    }
}
