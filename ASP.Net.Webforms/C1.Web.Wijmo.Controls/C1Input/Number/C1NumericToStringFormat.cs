using System;
using System.Globalization;


namespace C1.Web.Wijmo.Controls.C1Input
{
    internal class C1NumericToStringFormat
    {
        public static double DeFormatValue(string sValue, CultureInfo culture, NumberType type, int digitsPlaces, bool aExplictDeformat)
        {            
            double doubleResult;
            bool isNegative = false;
            int aNegIndex = sValue.IndexOf("-");
            if (aNegIndex == -1)
                aNegIndex = sValue.IndexOf("(");
            if (aNegIndex != -1)
                isNegative = true;
            sValue = sValue.Replace("(", "");
            sValue = sValue.Replace(")", "");
            sValue = sValue.Replace("-", "");
            sValue = sValue.Replace(culture.NumberFormat.PercentSymbol, "");
            sValue = sValue.Replace(culture.NumberFormat.CurrencySymbol, "");

            string sGroupSeparator = culture.NumberFormat.NumberGroupSeparator;
            string sDecimalSeparator = culture.NumberFormat.NumberDecimalSeparator;
            switch (type)
            {
                case NumberType.Percent:
                    sGroupSeparator = culture.NumberFormat.PercentGroupSeparator;
                    sDecimalSeparator = culture.NumberFormat.PercentDecimalSeparator;
                    break;
                case NumberType.Currency:
                    sGroupSeparator = culture.NumberFormat.CurrencyGroupSeparator;
                    sDecimalSeparator = culture.NumberFormat.CurrencyDecimalSeparator;
                    break;
            }

            //MessageBox.Show(sValue + "|" + sGroupSeparator + "|" + sDecimalSeparator);
            sValue = sValue.Replace(sGroupSeparator, "");
            //sValue = sValue.Replace(sDecimalSeparator, ",");
            sValue = sValue.Replace(" ", "");
            /*
            int aInd = sValue.IndexOf('.');
            char checkedSep = ',';
            if (aInd == -1)
            {
                aInd = sValue.IndexOf(',');
                checkedSep = '.';
            }
            if (aInd != -1 && sValue.IndexOf(checkedSep) == -1)
            {
                try
                {
                    string sAddDecValue = sValue.Substring(aInd + 1);
                    sValue = sValue.Substring(0, aInd) + sDecimalSeparator + sAddDecValue;
                    //MessageBox.Show(sValue + ":" + sGroupSeparator + ":" + sDecimalSeparator);
                }
                catch (Exception) { }
            }*/
            
            if (isNegative)
                sValue = "-" + sValue;
            try
            {
                if (aExplictDeformat)
                {
                    string sAnotherStrVal = "";
                    for (int i = 0; i < sValue.Length; i++)
                    {
                        char ch = sValue[i];
                        bool aIsDigit = char.IsDigit(ch);
                        bool aIsGroupOrDecimalSep = (
                               ch == '.'
                            || ch == ','
                            || ch == ' '
                            || ch == '-'
                            || ch + "" == sGroupSeparator
                            || ch + "" == sDecimalSeparator);
                        if (aIsDigit || aIsGroupOrDecimalSep)
                        {
                            sAnotherStrVal = sAnotherStrVal + ch;
                        }
                    }
                    sValue = sAnotherStrVal;
                }
                doubleResult = double.Parse(sValue, culture);
            }
            catch (Exception)
            {
                return double.NaN;
            }
            return doubleResult;
        }


        public static string GetFormatPattern(bool isNegative, CultureInfo culture, NumberType type, int digitsPlaces,bool showGroup,
            out string groupSep, out string decimalSep, out int decimalCount, out int[] groupSizes)
        {
            groupSep = " ";
            decimalSep = ".";
            groupSizes = new int[] { 3 };
            groupSizes[0] = 3;

            string formatPattern = "n";
            switch (type)
            {
                case NumberType.Numeric:
                    if (isNegative)
                        formatPattern = GetNumberNegativePattern(culture.NumberFormat.NumberNegativePattern);
                    else
                        formatPattern = "n";
                    groupSep = culture.NumberFormat.NumberGroupSeparator;
                    decimalSep = culture.NumberFormat.NumberDecimalSeparator;
                    groupSizes = culture.NumberFormat.NumberGroupSizes;
                    break;
                case NumberType.Percent:
                    if (isNegative)
                        formatPattern = GetPercentNegativePattern(culture.NumberFormat.PercentNegativePattern);
                    else
                        formatPattern = GetPercentPositivePattern(culture.NumberFormat.PercentPositivePattern);
                    groupSep = culture.NumberFormat.PercentGroupSeparator;
                    decimalSep = culture.NumberFormat.PercentDecimalSeparator;
                    groupSizes = culture.NumberFormat.PercentGroupSizes;
                    break;
                case NumberType.Currency:
                    if (isNegative)
                        formatPattern = GetCurrencyNegativePattern(culture.NumberFormat.CurrencyNegativePattern);
                    else
                        formatPattern = GetCurrencyPositivePattern(culture.NumberFormat.CurrencyPositivePattern);
                    groupSep = culture.NumberFormat.CurrencyGroupSeparator;
                    decimalSep = culture.NumberFormat.CurrencyDecimalSeparator;
                    groupSizes = culture.NumberFormat.CurrencyGroupSizes;
                    break;
            }
            //if (digitsPlaces != (int)C1DecimalDegitsEnum.Default)
            //{
            decimalCount = digitsPlaces;
            //}
            if (showGroup == false)
            {
                groupSizes = new int[1];
                groupSizes[0] = 0;
            }

            return formatPattern;
        }

        public static string FormatValue(double value, CultureInfo culture, NumberType type, int digitsPlaces, bool showGroup)
        {
             string groupSep;
            string decimalSep;
            int decimalCount;
            int[] groupSizes;
            string formatPattern = GetFormatPattern(value < 0, culture, type, digitsPlaces, showGroup, out groupSep,
                out decimalSep, out decimalCount, out groupSizes);
            string digitsString = FormatDegits(value, groupSep, decimalSep, decimalCount, groupSizes);
            return ApplyFormatPattern(formatPattern, digitsString, culture.NumberFormat.PercentSymbol, culture.NumberFormat.CurrencySymbol);
        }

        private static string ApplyFormatPattern(string formatPattern, string digitsString, string aPercentSymbol, string aCurrencySymbol)
        {
            string sResult = formatPattern;
            sResult = sResult.Replace("n", digitsString);
            sResult = sResult.Replace("%", aPercentSymbol);
            sResult = sResult.Replace("$", aCurrencySymbol);
            return sResult;
        }

        /// <summary>
        /// Format digits, add groups separators, decimal separator, decimals
        /// </summary>
        /// <param name="value"></param>
        /// <param name="groupSep"></param>
        /// <param name="decimalSep"></param>
        /// <param name="decimalCount">-1 means autosize</param>
        /// <param name="groupSizes"></param>
        /// <returns></returns>
        private static string FormatDegits(double value, string groupSep, string decimalSep, int decimalCount, int[] groupSizes)
        {
            string sAbsValue = Math.Abs(value).ToString();
            int aDecIndx = sAbsValue.IndexOf(decimalSep);
            if (aDecIndx == -1)
            {
                aDecIndx = sAbsValue.IndexOf(".");
            }
            if (aDecIndx == -1)
            {
                aDecIndx = sAbsValue.IndexOf(",");
            }
            if (aDecIndx == -1)
            {
                aDecIndx = sAbsValue.Length;
            }
            string sResult = "";
            int aGroupSizesIndx = 0;
            int aGroupCount = 0;
            //abs degits:
            for (int i = sAbsValue.Length - 1; i >= 0; i--)
            {
                char ch = sAbsValue[i];
                if (i < aDecIndx)
                {
                    sResult = ch + sResult;
                    aGroupCount++;
                    if (aGroupCount == groupSizes[aGroupSizesIndx] && groupSizes[aGroupSizesIndx] != 0 && i !=0 )
                    {
                        sResult = groupSep + sResult;
                        aGroupCount = 0;
                        if (groupSizes.Length - 1 > aGroupSizesIndx)
                            aGroupSizesIndx++;
                    }
                }
            }
            // decimals degits:
            if (decimalCount > 0)
            {
                sResult = sResult + decimalSep;
                for (int i=0; i<decimalCount; i++)
                {
                    char ch = '0';
                    if (i + aDecIndx + 1 < sAbsValue.Length)
                    {
                        ch = sAbsValue[i + aDecIndx + 1];
                    }
                    sResult = sResult + ch;
                }
            }
            if (decimalCount == -1)
            {
                if (aDecIndx < sAbsValue.Length-1)
                {
                    sResult = sResult + decimalSep;
                    sResult = sResult + sAbsValue.Substring(aDecIndx + 1);
                }
            }
            return sResult;
        }

        private static string GetCurrencyPositivePattern(int p)
        {
            string sResult = "$n";
            switch (p)
            {
                case 0:
                    sResult = "$n";
                    break;
                case 1:
                    sResult = "n$";
                    break;
                case 2:
                    sResult = "$ n";
                    break;
                case 3:
                    sResult = "n $";
                    break;
            }
            return sResult;
        }

        private static string GetCurrencyNegativePattern(int p)
        {
            string sResult = "$-n";
            switch (p)
            {
                case 0:
                    sResult = "($n)";
                    break;
                case 1:
                    sResult = "-$n";
                    break;
                case 2:
                    sResult = "$-n";
                    break;
                case 3:
                    sResult = "$n-";
                    break;
                case 4:
                    sResult = "(n$)";
                    break;
                case 5:
                    sResult = "-n$";
                    break;
                case 6:
                    sResult = "n-$";
                    break;
                case 7:
                    sResult = "n$-";
                    break;
                case 8:
                    sResult = "-n $";
                    break;
                case 9:
                    sResult = "-$ n";
                    break;
                case 10:
                    sResult = "n $-";
                    break;
                case 11:
                    sResult = "$ n-";
                    break;
                case 12:
                    sResult = "$ -n";
                    break;
                case 13:
                    sResult = "n- $";
                    break;
                case 14:
                    sResult = "($ n)";
                    break;
                case 15:
                    sResult = "(n $)";
                    break;
            }
            return sResult;
        }

        private static string GetPercentPositivePattern(int p)
        {
            string sResult = "n%";
            switch (p)
            {
                case 0:
                    sResult = "n %";
                    break;
                case 1:
                    sResult = "n%";
                    break;
                case 2:
                    sResult = "%n";
                    break;
                case 3:
                    sResult = "% n";
                    break;
            }
            return sResult;
        }

        private static string GetPercentNegativePattern(int p)
        {
            string sResult = "-n%";
            switch (p)
            {
                case 0:
                    sResult = "-n %";
                    break;
                case 1:
                    sResult = "-n%";
                    break;
                case 2:
                    sResult = "-%n";
                    break;
                case 3:
                    sResult = "%-n";
                    break;
                case 4:
                    sResult = "%n-";
                    break;
                case 5:
                    sResult = "n-%";
                    break;
                case 6:
                    sResult = "n%-";
                    break;
                case 7:
                    sResult = "-%n";
                    break;
                case 8:
                    sResult = "n %-";
                    break;
                case 9:
                    sResult = "% n-";
                    break;
                case 10:
                    sResult = "% -n";
                    break;
                case 11:
                    sResult = "n- %";
                    break;
            }
            return sResult;
        }

        public static string GetNumberNegativePattern(int p)
        {
            string sResult = "-n";
            switch (p)
            {
                case 0:
                    sResult = "(n)";
                    break;
                case 1:
                    sResult = "-n";
                    break;
                case 2:
                    sResult = "- n";
                    break;
                case 3:
                    sResult = "n-";
                    break;
                case 4:
                    sResult = "n -";
                    break;
            }
            return sResult;
        }

    }
}
