using System.ComponentModel;
using System.Drawing;
using System.Web.UI;


namespace C1.Web.Wijmo.Controls.C1Input
{
    /// <summary>
    /// Web control specialized for editing numeric values. 
    /// Using the numeric editor, you can specify input without 
    /// writing any custom validation logic in your application.
    /// </summary>
    [ToolboxData("<{0}:C1InputNumeric runat=server></{0}:C1InputNumeric>")]
    [ToolboxBitmap(typeof(C1InputNumeric), "Number.C1InputNumeric.png")]
    [LicenseProviderAttribute]
    public class C1InputNumeric : C1InputNumber
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the C1InputNumeric class.
        /// </summary>
        public C1InputNumeric() : base()
        {
        }

        #endregion

  
    }
}
