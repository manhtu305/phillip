using System.ComponentModel;
using System.Drawing;
using System.Web.UI;


namespace C1.Web.Wijmo.Controls.C1Input
{
    /// <summary>
    /// Web control specialized for editing currency values. 
    /// Using the numeric editor, you can specify input without 
    /// writing any custom validation logic in your application.
    /// </summary>
    [ToolboxData("<{0}:C1InputCurrency runat=server></{0}:C1InputCurrency>")]
    [ToolboxBitmap(typeof(C1InputCurrency), "Number.C1InputCurrency.png")]
    [LicenseProviderAttribute]
    public class C1InputCurrency : C1InputNumber
    {
        /// <summary>
        /// Initializes a new instance of the C1InputCurrency class.
        /// </summary>
        public C1InputCurrency() : base()
        {
        }

        /// <summary>
        /// Determines the type of the number input.
        /// </summary>
        public override NumberType NumberType
        {
            get
            {
                return NumberType.Currency;
            }
        }

    }

}
