﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Collections;
using System.Web.UI;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Input
{
    /// <summary>
    /// Abstract base class for C1InputCurrency, C1InputNumeric and C1InputPercent.
    /// </summary>
    [DefaultProperty("Value")]
	[ControlValueProperty("Value")]
    [DefaultEvent("ValueChanged")]
    [ValidationProperty("Value")]
    public abstract partial class C1InputNumber : C1InputBase
    {
        #region Overrides

        /// <summary>
        /// Gets the alignment of text in style value.
        /// </summary>
        /// <returns>The alignment of text in style value</returns>
        protected override string GetTextAlign()
        {
            return "right";
        }

        /// <summary>
        /// Gets the default CSS class for input.
        /// </summary>
        /// <returns>The default CSS class for input.</returns>
        /// <remarks>Internal use only.</remarks>
        protected override string GetInputCss()
        {
            return "wijmo-wijinput-number";
        }

        /// <summary>
        /// Gets the text value of input for design time.
        /// </summary>
        /// <returns>The text value of input for design time.</returns>
        /// <remarks>Internal use only.</remarks>
        protected override string GetDesignTimeTextValue()
        {
            return C1NumericToStringFormat.FormatValue(this.Value, this.Culture, this.NumberType, this.DecimalPlaces, this.ShowGroup);
        }

        /// <summary>
        /// Loads data from the client.
        /// </summary>
        /// <param name="data">A hashtable that stores the data name and value.</param>
        protected override bool LoadClientData(Hashtable data)
        {
            base.LoadClientData(data);

            object value = data["value"] == null ? 0.0 : data["value"];
            double d;
            try
            {
                d = (Double)value;
            }
            catch
            {
                d = double.Parse(value.ToString(), CultureInfo.CurrentCulture);
            }

            if (this.Value != d)
            {
                this.Value = d;
				return true;
            }

            return false;
        }
		protected override void RaisePostDataChangedEvent()
		{
			base.RaisePostDataChangedEvent();
			this.OnValueChanged(EventArgs.Empty);
		}

        #endregion

        #region Server Events

        private static readonly object ValueChangedEventKey = new object();

        /// <summary>
        /// Fires when the value is changed.
        /// </summary>
        [C1Description("C1Input.ValueChanged")]
        public event C1ValueChangedEventHandler ValueChanged
        {
            add
            {
                Events.AddHandler(ValueChangedEventKey, value);
            }

            remove
            {
                Events.RemoveHandler(ValueChangedEventKey, value);
            }
        }

        internal void OnValueChanged(EventArgs args)
        {
            C1ValueChangedEventHandler d = (C1ValueChangedEventHandler)Events[ValueChangedEventKey];
            if (d != null)
            {
                d(this, args);
            }
        }

        #endregion

        #region Internal Methods

        internal bool SetValueFromText(string value)
        {
            bool success;
            double newValue = C1NumericToStringFormat.DeFormatValue(value, this.Culture, this.NumberType, this.DecimalPlaces, false);
            if (double.IsNaN(newValue))
            {
                newValue = C1NumericToStringFormat.DeFormatValue(value, this.Culture, this.NumberType, this.DecimalPlaces, true);
            }
            if (!double.IsNaN(newValue))
            {
                this.Value = newValue;
                success = true;
            }
            else
            {
                success = false;
            }
            return success;
        }

        #endregion
    }

    /// <summary>
    /// Represents the method that will handle the ValueChanged event of the control. 
    /// </summary>
    /// <param name="sender">Source of the event.</param>
    /// <param name="e">The EventArgs that contains the event data.</param>
    public delegate void C1ValueChangedEventHandler(object sender, EventArgs e);
}
