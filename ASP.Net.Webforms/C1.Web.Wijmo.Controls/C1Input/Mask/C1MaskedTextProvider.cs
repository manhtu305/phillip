using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Globalization;
using System.Text.RegularExpressions;
using C1.Web.Wijmo.Controls.Localization;


namespace C1.Web.Wijmo.Controls.C1Input
{
    /// <summary>
    /// Type of enum part
    /// </summary>
    internal enum EnumPartType
    {
        /// <summary>
        /// 
        /// </summary>
        Symbol = 1,

        /// <summary>
        /// 
        /// </summary>
        Degit = 2
    }

    /// <summary>
    /// C1MaskedTextProvider. Custom wrapper for MaskedTextProvider.
    /// </summary>
    public class C1MaskedTextProvider
    {
        #region Fields
        private readonly MaskedTextProvider _maskedTextProvider;
        internal bool HaveEnumParts = false;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mask">Mask string.</param>
        /// <param name="aCultureInfo">Culture.</param>
        public C1MaskedTextProvider(string mask, CultureInfo aCultureInfo)
        {
            try
            {
                mask = this.ParseEnumerationParts(mask);
            }
            catch (ArgumentException aex)
            {
                throw aex;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            if (aCultureInfo != null)
                this._maskedTextProvider = new MaskedTextProvider(mask, aCultureInfo, true, '_', '\0', false);
            else
                this._maskedTextProvider = new MaskedTextProvider(mask);
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public bool IncludeLiterals
        {
            get
            {
                return _maskedTextProvider.IncludeLiterals;
            }
            set
            {
                _maskedTextProvider.IncludeLiterals = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IncludePrompt
        {
            get
            {
                return _maskedTextProvider.IncludePrompt;
            }
            set
            {
                _maskedTextProvider.IncludePrompt = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public char PromptChar
        {
            get
            {
                return _maskedTextProvider.PromptChar;
            }
            set
            {
                _maskedTextProvider.PromptChar = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public char PasswordChar
        {
            get
            {
                return _maskedTextProvider.PasswordChar;
            }
            set
            {
                _maskedTextProvider.PasswordChar = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Mask
        {
            get
            {
                return _maskedTextProvider.Mask;
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// ApplyFormatToEnumValue
        /// </summary>
        /// <param name="sEnumValue"></param>
        /// <param name="aMaxLen"></param>
        /// <param name="aEnumType"></param>
        /// <returns></returns>
        private static string ApplyFormatToEnumValue(string sEnumValue, int aMaxLen, EnumPartType aEnumType)
        {
            string sResultEnumValue = "";
            for (int i = 0; i < aMaxLen; i++)
            {
                if (aMaxLen > sEnumValue.Length + i)
                {
                    if (aEnumType.Equals(EnumPartType.Degit))
                    {
                        sResultEnumValue += "#";
                    }
                    else
                    {
                        //sResultEnumValue += "\\ ";
                        sResultEnumValue += "&";
                    }
                }
                else
                {
                    if (aEnumType.Equals(EnumPartType.Degit))
                    {
                        sResultEnumValue += "#";
                    }
                    else
                    {
                        //sResultEnumValue = sResultEnumValue + "\\" + sEnumValue[i - (aMaxLen - sEnumValue.Length)];
                        sResultEnumValue = sResultEnumValue + "&";
                    }
                }
            }
            return sResultEnumValue;
        }

        /// <summary>
        /// Get and returns formatted default value for enumeration part
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="aEnumType"></param>
        /// <returns></returns>
        private static string GetDefaultValueForEnumArray(string[] arr, EnumPartType aEnumType)
        {
            if (arr == null || arr.Length <= 0)
                return "";
            int aMaxLen = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                int aCurLen = arr[i].Length;
                aMaxLen = aCurLen > aMaxLen ? aCurLen : aMaxLen;
            }
            string sDefaultValue = arr[0];
            for (int i = 0; i < arr.Length; i++)
            {
                if (aEnumType == EnumPartType.Degit)
                {
                    bool aIsGoodVal = Regex.IsMatch(arr[i], "\\A[0-9]+\\z", RegexOptions.Multiline);
                    if (!aIsGoodVal)
                        throw new ArgumentException(C1Localizer.GetString("Numeric range accepts only unsigned integers."));
                }

                if (arr[i].Length > 0 && arr[i][0] == '*')
                {
                    sDefaultValue = arr[i].Substring(1);
                    arr[i] = sDefaultValue;
                }
            }
            return C1MaskedTextProvider.ApplyFormatToEnumValue(sDefaultValue, aMaxLen, aEnumType);
        }

        /// <summary>
        /// Preparse mask for enumeration parts:
        /// </summary>
        /// <param name="mask"></param>
        /// <returns></returns>
        private string ParseEnumerationParts(string mask)
        {
            string sResultMask = "";
            int aBegTagIndex = mask.IndexOf("<<", 0);
            if (aBegTagIndex == -1)
                return mask;
            while (aBegTagIndex != -1)
            {
                sResultMask += mask.Substring(0, aBegTagIndex);
                mask = mask.Substring(aBegTagIndex);
                aBegTagIndex = 0;// mask.IndexOf("<<", aEndTagIndex);
                int aEndTagIndex = mask.IndexOf(">>", 0);
                if (aEndTagIndex != -1)
                {
                    string s = mask.Substring(2, aEndTagIndex - 2);
                    if (s.IndexOf('|') != -1)
                    {
                        string[] arr = s.Split('|');
                        sResultMask += C1MaskedTextProvider.GetDefaultValueForEnumArray(arr, EnumPartType.Symbol);
                        this.HaveEnumParts = true;
                    }
                    else if (s.IndexOf("...") != -1)
                    {
                        bool aIsGoodVal = Regex.IsMatch(s, "\\A[0-9]{1,15}[.]{3}[0-9]{1,15}\\z", RegexOptions.Multiline);
                        if (aIsGoodVal == false)
                        {
                            throw new ArgumentException(C1Localizer.GetString("Numeric range accepts only unsigned integers(numbers between 0 and 999,999,999,999,999 inclusive)."));
                            /*sResultMask += "<" + s + ">";
                            mask = mask.Substring(aEndTagIndex + 2);
                            aBegTagIndex = mask.IndexOf("<<", 0);
                            continue;*/
                        }
                        string[] arr = s.Split(new string[] { "..." }, StringSplitOptions.RemoveEmptyEntries);
                        sResultMask += C1MaskedTextProvider.GetDefaultValueForEnumArray(arr, EnumPartType.Degit);
                        this.HaveEnumParts = true;
                    }
                    else
                    {
                        sResultMask += "<" + s + ">";
                    }
                    mask = mask.Substring(aEndTagIndex + 2);
                }
                else
                {
                    sResultMask += mask.Substring(aBegTagIndex);
                    mask = "";
                }
                aBegTagIndex = mask.IndexOf("<<", 0);
            }
            sResultMask += mask;
            return sResultMask;
        }

        #endregion

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Set(string value)
        {
            if (value == null)
                value = string.Empty;
            return _maskedTextProvider.Set(value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ignorePasswordChar"></param>
        /// <returns></returns>
        public string ToString(bool ignorePasswordChar)
        {
            return _maskedTextProvider.ToString(ignorePasswordChar);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="includePrompt"></param>
        /// <param name="includeLiterals"></param>
        /// <returns></returns>
        public string ToString(bool includePrompt, bool includeLiterals)
        {
            return _maskedTextProvider.ToString(includePrompt, includeLiterals);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ignorePassword"></param>
        /// <param name="includePrompt"></param>
        /// <param name="includeLiterals"></param>
        /// <param name="startPos"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public string ToString(bool ignorePassword, bool includePrompt, bool includeLiterals, int startPos, int len)
        {
            return _maskedTextProvider.ToString(ignorePassword, includePrompt, includeLiterals, startPos, len);
        }

        /// <summary>
        /// Returns mask without custom parts(such as enumeration parts)
        /// </summary>
        /// <param name="aMask"></param>
        /// <param name="aCultureInfo"></param>
        /// <returns></returns>
        public static string GetResultingMaskForStandardMaskedTextBox(string aMask, CultureInfo aCultureInfo)
        {
            try
            {
                aMask = new C1MaskedTextProvider(aMask, aCultureInfo).Mask;
            }
            catch (ArgumentException)
            {
                //throw aex;
                return aMask;
            }
            catch
            {
                // Mask cannot be null
            }

            return aMask;
        }

        #endregion
    }
}
