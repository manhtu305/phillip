﻿using System;
using System.Collections;
using System.Drawing;
using System.Web.UI;
using System.ComponentModel;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Input
{
    /// <summary>
    /// The main Web control used for entering and editing information 
    /// of any data type in a text form. Supports data formatting, 
    /// edit mask, data validation and other features. 
    /// Supports formatted and masked editing of all data types, 
    /// including additional functionality. 
    /// Apart from being the main data editor control, 
    /// C1InputMask also serves as the base class for specialized 
    /// controls such as C1DateInput and C1NumericInput.
    /// </summary>    
    [ToolboxData("<{0}:C1InputMask runat=server></{0}:C1InputMask>")]
    [ToolboxBitmap(typeof(C1InputMask), "Mask.C1InputMask.png")]
    [DefaultProperty("Mask")]
	[ControlValueProperty("Text")]
    [LicenseProviderAttribute]
    [ValidationProperty("TextWithPromptAndLiterals")]
    public partial class C1InputMask : C1InputBase 
    {
        #region Fields
        private C1MaskedTextProvider _maskProvider; // helper object to format masked strings
        private MaskControl _imControl;
        private bool _needSyncText = true;
        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the C1InputMask class.
        /// </summary>
        public C1InputMask() : base()
        {
        }

        #endregion

        #region Properties
    
        /// <summary>
        /// Gets the text input by the user as well as any instances of 
        /// the prompt character.
        /// </summary>
        [C1Description("C1InputMask.TextWithPrompts")]
        [C1Category("Category.Appearance")]
        [Bindable(false)]
        [Browsable(false)]
        public virtual string TextWithPrompts
        {
            get
            {
                if (this.FormatMode == FormatMode.Simple)
                {
                    if (this._maskProvider == null || this.MaskFormat.Length == 0)
                        return this.Text;
                    return this._maskProvider.ToString(true, false);
                }
                else
                {
                    return this._imControl.TextWidthPrompts;
                }
            }
        }

        /// <summary>
        /// Gets the text input by the user as well as any literal 
        /// characters defined in the mask and any instances of the 
        /// prompt character.
        /// </summary>
        [C1Description("C1InputMask.TextWithPromptAndLiterals")]
        [C1Category("Category.Appearance")]
        [Bindable(false)]
        [Browsable(false)]
        [Json(true)]
        public virtual string TextWithPromptAndLiterals
        {
            get
            {
                if (this.FormatMode == FormatMode.Simple)
                {
                    if (this._maskProvider == null || this.MaskFormat.Length == 0)
                        return this.Text;
                    return this._maskProvider.ToString(true, true);
                }
                else
                {
                    return this._imControl.TextWidthPromptAndLiterals;
                }
                
            }
        }

        /// <summary>
        /// Gets the text input by the user as well as any literal 
        /// characters defined in the mask.
        /// </summary>        
        [C1Description("C1InputMask.TextWithLiterals")]
        [C1Category("Category.Appearance")]
        [Bindable(false)]
        [Browsable(false)]
        public string TextWithLiterals
        {
            get
            {
                if (this.FormatMode == FormatMode.Simple)
                {
                    if (this._maskProvider == null || this.MaskFormat.Length == 0)
                        return this.Text;
                    return this._maskProvider.ToString(false, true);
                }
                else
                {
                    return this._imControl.TextWidthLiterals;
                }
            }
        }

        #endregion

        #region Server Events

        private static readonly object TextChangedEventKey = new object();


        /// <summary>
        /// Fires when the text is changed.
        /// </summary>
        [C1Description("C1Input.TextChanged")]
        public event C1TextChangedEventHandler TextChanged
        {
            add
            {
                Events.AddHandler(TextChangedEventKey, value);
            }

            remove
            {
                Events.RemoveHandler(TextChangedEventKey, value);
            }
        }

        /// <summary>
        /// Raise the TextChanged event.
        /// </summary>
        /// <param name="args">Event arguments.</param>
        public virtual void OnTextChanged(EventArgs args)
        {
            C1TextChangedEventHandler d = (C1TextChangedEventHandler)Events[TextChangedEventKey];
            if (d != null)
            {
                d(this, args);
            }
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Loads data from the client.
        /// </summary>
        /// <param name="data">A hashtable that stores the data name and value.</param>
        protected override bool LoadClientData(Hashtable data)
        {
            string o = data["text"] as string;
            string advancedText = data["advancedText"] as string;

            if (this.FormatMode == C1Input.FormatMode.Advanced)
            {
                bool result = o != this.Text;
                this.SetAdvancedText(advancedText);
                if (result)
                {
                    return true;
                }
            }
            else
            {

                if (o != this.Text)
                {
                    this.Text = o;

                    // fixed an issue that when use validation controls, the time of set the _maskProvider's text is late.
                    // when load cliend data, reset the text of the _maskProvider.
                    if (this._maskProvider != null)
                    {
                        this._maskProvider.Set(this.Text);
                    }

                    return true;
                }
                // fixed an issue that when use validation controls, the time of set the _maskProvider's text is late.
                // when load cliend data, reset the text of the _maskProvider.
                if (this._maskProvider != null)
                {
                    this._maskProvider.Set(this.Text);
                }
            }
            return false;
        }

        private void SetAdvancedText(string advancedText)
        {
            this._imControl.Text = advancedText;
            this._needSyncText = false;
            try
            {
                TypeDescriptor.GetProperties(this)["Text"].SetValue(this, this._imControl.Value);
            }
            finally
            {
                this._needSyncText = true;
            }

        }

		protected override void RaisePostDataChangedEvent()
		{
			base.RaisePostDataChangedEvent();
			this.OnTextChanged(EventArgs.Empty);
		}

        protected override void OnPropertyValueChanged<V>(string propertyName, V value)
        {
            base.OnPropertyValueChanged<V>(propertyName, value);

            try
            {
                if (this.FormatMode == FormatMode.Advanced)
                {
                    switch (propertyName)
                    {
                        case "Mask":
                        case "MaskFormat":
                        case "PromptChar":
                        case "FormatMode":
                        case "Text":
                            UpdateImMaskControl();
                            break;
                    }
                }
                else
                {
                    switch (propertyName)
                    {
                        case "Mask":
                        case "MaskFormat":
                        case "PromptChar":
                        case "PasswordChar":
                        case "Culture":
                            UpdateMaskedTextProvider();
                            break;

                        case "Text":
                            if (_maskProvider != null)
                                _maskProvider.Set(Text);
                            break;
                    }
                }
            }
            catch
            {
                if (propertyName == "FormatMode")
                {
                    TypeDescriptor.GetProperties(this)["MaskFormat"].SetValue(this, "");
                }
                else
                {
                    throw;
                }
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Update Masked Text provider with current control's properties
        /// </summary>
        private void UpdateMaskedTextProvider()
        {
            if (MaskFormat.Length == 0)
            {
                this._maskProvider = null;
                return;
            }
            this._maskProvider = new C1MaskedTextProvider(MaskFormat, Culture);
            if (this.MaskFormat.Length > 0)
            {
                this._maskProvider.IncludeLiterals = true;
                this._maskProvider.IncludePrompt = true;
                this._maskProvider.PromptChar = this.PromptChar;
                try
                {
                    if (this.PasswordChar.Length > 0)
                        this._maskProvider.PasswordChar = this.PasswordChar[0];
                }
                catch
                {
                }
            }
            this._maskProvider.Set(this.Text);
        }

        private void UpdateImMaskControl()
        {
            if (this._imControl == null)
            {
                this._imControl = new MaskControl();
            }
            this.SyncImControlProperties();
        }

        private void SyncImControlProperties()
        {
            if (!this._needSyncText)
            {
                return;
            }
            this._imControl.Format = new MaskFormat(this.MaskFormat, this.Placeholder, this.Placeholder);
            this._imControl.PromptChar = this.PromptChar;

            this._imControl.AutoConvert = this.AutoConvert;
            try
            {
                try
                {
                    this._imControl.Value = this.Text;
                }
                catch
                {
                }

                if (this.Text != this._imControl.Value)
                {
                    TypeDescriptor.GetProperties(this)["Text"].SetValue(this, this._imControl.Value);
                }
            }
            catch
            {
            }
        }


        /// <summary>
        /// Gets the text value of input for design time.
        /// </summary>
        /// <returns>The text value for design time.</returns>
        /// <remarks>Internal use only.</remarks>
        protected override string GetDesignTimeTextValue()
        {
            if (this.FormatMode == FormatMode.Simple)
            {
                if (this._maskProvider == null || this.MaskFormat.Length <= 0)
                    return string.IsNullOrEmpty(PasswordChar) ? Text : new string(PasswordChar[0], Text.Length);

                bool includePrompt = !(this.IsDesignMode == false && this.HidePromptOnLeave);
                return this._maskProvider.ToString(string.IsNullOrEmpty(PasswordChar), includePrompt, true, 0, 10000);
            }
            else
            {
                return this._imControl.Text;
            }
        }

        /// <summary>
        /// Gets the default CSS class for input.
        /// </summary>
        /// <returns>The default CSS class for input.</returns>
        /// <remarks>Internal use only.</remarks>
        protected override string GetInputCss()
        {
            return "wijmo-wijinput-mask";
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal string EmptyString
        {
            [SmartAssembly.Attributes.DoNotObfuscate]
            get
            {
                if (this._imControl != null)
                {
                    return this._imControl.EmptyString;    
                }
                else
                {
                    return "";
                }
            }
        }


        #endregion

    }

    /// <summary>
    /// Represents the method that will handle the TextChanged event of the control. 
    /// </summary>
    /// <param name="sender">Source of the event.</param>
    /// <param name="e">The EventArgs that contains the event data.</param>
    public delegate void C1TextChangedEventHandler(object sender, EventArgs e);

}
