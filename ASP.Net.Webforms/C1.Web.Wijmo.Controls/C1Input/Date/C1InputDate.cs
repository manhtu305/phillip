﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Collections;
using System.Web.UI;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Input
{
    /// <summary>
    /// An input control that allows the date time value editing.
    /// </summary>
    [ToolboxData("<{0}:C1InputDate runat=server></{0}:C1InputDate>")]
    [ToolboxBitmap(typeof(C1InputNumeric), "Date.C1InputDate.png")]
    [DefaultEvent("DateChanged")]
    [DefaultProperty("Date")]
	[ControlValueProperty("Date")]
    [LicenseProviderAttribute]
	[ValidationProperty("DateString")]
    public partial class C1InputDate : C1InputBase
    {
        #region Constructor

        /// <summary>
        /// Initializes a new instance of the C1InputNumeric class.
        /// </summary>
        public C1InputDate()
            : base()
        {
        }

        #endregion

		#region Properties
		// add this field for asp.net validation controls.
		[EditorBrowsable( EditorBrowsableState.Never)]
		[Browsable(false)]
		public string DateString
		{
			get 
			{
				
				if (this.Date == null)
				{
					return string.Empty;
				}
				else
				{					
					return this.GetDesignTimeTextValue();
				}
			}
		}
		#endregion

        #region Overrides


		/// <summary>
        /// Gets the default CSS class for input.
        /// </summary>
        /// <returns>The default CSS class for input.</returns>
        /// <remarks>Internal use only.</remarks>
        protected override string GetInputCss()
        {
            return "wijmo-wijinput-date";
        }

        /// <summary>
        /// Gets the text value of input for design time.
        /// </summary>
        /// <returns></returns>
        protected override string GetDesignTimeTextValue()
        {
            string format = this.DisplayFormat != "" ? this.DisplayFormat : this.DateFormat;
            string ptn = ParseDateFormatIntoDatePattern(format);
            return this.GetMaskedText(this.GetDate(), ptn, this.Culture);
        }

        private string GetMaskedText(DateTime date, string format, CultureInfo culture)
        {
            if (format == "g")
            {
                return date.ToString(format, culture);
            }

            ArrayList arrayList = this.GetForamtPatterns(format);
            string text = String.Empty;
            for (int i = 0; i < arrayList.Count; i++)
            {
                string value = arrayList[i].ToString();
                if (value.IndexOf('g') != -1)
                {
                    if (value == "g")
                    {
                        text += DateTimeInfo.GetSymbolEraNames(date);
                    }
                    else if (value == "gg")
                    {
                        text += DateTimeInfo.GetAbbreviatedEraNames(date);
                    }
                    else
                    {
                        text += DateTimeInfo.GetEraName(date);
                    }
                }
                else if (value.IndexOf('e') != -1)
                {
                    int year = DateTimeInfo.GetEraYear(date);
                    if (value == "e")
                    {
                        text += year.ToString();
                    }
                    else
                    {
                        if (year < 10)
                        {
                            text += "0" + year.ToString();
                        }
                        else
                        {
                            text += year.ToString();
                        }
                    }
                }
                else if (value.IndexOf('A') != -1)
                {
                    text += "A.D.";
                }
                else if (value.IndexOf('E') != -1)
                {
                    int year = DateTimeInfo.GetEraYear(date);
                    if (year == 1)
                    {
                        text += "元";
                    }
                    else
                    {
                        text += year.ToString();
                    }
                }
                else
                {
                    if (!String.IsNullOrEmpty(value.Trim()))
                    {
                        text += date.ToString(value, culture);
                    }
                    else
                    {
                        text += value;
                    }
                }
            }

            return text;
        }

        private ArrayList GetForamtPatterns(string format)
        {
            ArrayList arrayList = new ArrayList();

            string subString = String.Empty;
            string gString = String.Empty;
            string eString = String.Empty;
            string EString = String.Empty;
            string AString = String.Empty;

            for (int i = 0; i < format.Length; i++)
            {
                string c = format.Substring(i, 1);

                if (c == "g")
                {
                    if (subString != "")
                    {
                        arrayList.Add(subString);
                        subString = String.Empty;
                    }

                    if (eString != "")
                    {
                        arrayList.Add(eString);
                        eString = String.Empty;
                    }

                    if (AString != "")
                    {
                        arrayList.Add(AString);
                        AString = String.Empty;
                    }

                    if (EString != "")
                    {
                        arrayList.Add(EString);
                        EString = String.Empty;
                    }

                    gString = gString + c;
                }
                else if (c == "e")
                {
                    if (subString != "")
                    {
                        arrayList.Add(subString);
                        subString = String.Empty;
                    }

                    if (gString != "")
                    {
                        arrayList.Add(eString);
                        gString = String.Empty;
                    }

                    if (AString != "")
                    {
                        arrayList.Add(AString);
                        AString = String.Empty;
                    }

                    if (EString != "")
                    {
                        arrayList.Add(EString);
                        EString = String.Empty;
                    }

                    eString = eString + c;
                }
                else if (c == "E")
                {
                    if (subString != "")
                    {
                        arrayList.Add(subString);
                        subString = String.Empty;
                    }

                    if (gString != "")
                    {
                        arrayList.Add(eString);
                        gString = String.Empty;
                    }

                    if (AString != "")
                    {
                        arrayList.Add(AString);
                        AString = String.Empty;
                    }

                    if (eString != "")
                    {
                        arrayList.Add(eString);
                        eString = String.Empty;
                    }

                    EString = EString + c;
                }
                else if (c == "A")
                {
                    if (subString != "")
                    {
                        arrayList.Add(subString);
                        subString = String.Empty;
                    }

                    if (gString != "")
                    {
                        arrayList.Add(eString);
                        gString = String.Empty;
                    }

                    if (EString != "")
                    {
                        arrayList.Add(EString);
                        EString = String.Empty;
                    }

                    if (eString != "")
                    {
                        arrayList.Add(eString);
                        eString = String.Empty;
                    }

                    AString = AString + c;
                }
                else
                {
                    if (gString != "")
                    {
                        arrayList.Add(gString);
                        gString = String.Empty;
                    }

                    if (eString != "")
                    {
                        arrayList.Add(eString);
                        eString = String.Empty;
                    }

                    if (AString != "")
                    {
                        arrayList.Add(AString);
                        AString = String.Empty;
                    }

                    if (EString != "")
                    {
                        arrayList.Add(EString);
                        EString = String.Empty;
                    }

                    subString = subString + c;
                }
            }

            if (subString != "")
            {
                arrayList.Add(subString);
            }
            if (gString != "")
            {
                arrayList.Add(gString);
            }
            if (eString != "")
            {
                arrayList.Add(eString);
            }
            if (AString != "")
            {
                arrayList.Add(AString);
                AString = String.Empty;
            }
            if (EString != "")
            {
                arrayList.Add(EString);
                EString = String.Empty;
            }

            return arrayList;
        }

        /// <summary>
        /// Loads data from the client.
        /// </summary>
        /// <param name="data">A hashtable that stores the data name and value.</param>
        protected override bool LoadClientData(Hashtable data)
        {
            base.LoadClientData(data);

            DateTime? date = (DateTime?)data["date"];
            if ((date == null && this.Date != null) || (date != null && ((DateTime)date).CompareTo(this.Date) != 0))
            {
                this.Date = date;
                return true;
            }

            return false;
        }

		protected override void RaisePostDataChangedEvent()
		{
			base.RaisePostDataChangedEvent();
			this.OnDateChanged(EventArgs.Empty);
		}

        #endregion

        #region Server Events

        internal static readonly object DateChangedEventKey = new object();

        /// <summary>
        /// Fired when the Date is changed. 
        /// </summary>
        [C1Description("C1Input.DateChanged")]
        public event C1DateChangedEventHandler DateChanged
        {
            add
            {
                Events.AddHandler(DateChangedEventKey, value);
            }

            remove
            {
                Events.RemoveHandler(DateChangedEventKey, value);
            }
        }

        private void OnDateChanged(EventArgs args)
        {
            C1DateChangedEventHandler d = (C1DateChangedEventHandler)Events[DateChangedEventKey];
            if (d != null)
            {
                d(this, args);
            }
        }

        #endregion

        #region Internal Methods

        /// <summary>
        /// Gets the date value. This method is only used by the control developer.
        /// </summary>
        /// <returns></returns>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public DateTime GetDate()
        {
            DateTime ret = DateTime.Now;
            DateTime? d = this.Date;
            if (d != null)
                ret = (DateTime)d;

            return ret;
        }

        /// <summary>
        /// Translate the date time format string into pattern string. This method is only used by the control developer.
        /// </summary>
        /// <param name="dateFormat"></param>
        /// <returns></returns>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public string ParseDateFormatIntoDatePattern(string dateFormat)
        {
            // Creates and initializes a DateTimeFormatInfo associated 
            // with a current culture.
            DateTimeFormatInfo dtFmt = this.Culture.DateTimeFormat;

            string result = dateFormat;
            if (dateFormat.Length <= 1)
            {
                switch (dateFormat)
                {
                    case "":
                    case "d": // ShortDatePattern
                        result = dtFmt.ShortDatePattern;
                        break;
                    case "D": // LongDatePattern
                        result = dtFmt.LongDatePattern;
                        break;
                    case "f": // Full date and time (long date and short time)
                        result = dtFmt.LongDatePattern + " " + dtFmt.ShortTimePattern;
                        break;
                    case "F": // FullDateTimePattern
                        result = dtFmt.FullDateTimePattern;
                        break;
                    case "g": // General (short date and short time)
                        result = dtFmt.ShortDatePattern + " " + dtFmt.ShortTimePattern;
                        break;
                    case "G": // General (short date and long time)
                        result = dtFmt.ShortDatePattern + " " + dtFmt.LongTimePattern;
                        break;
                    case "m": // MonthDayPattern
                        result = dtFmt.MonthDayPattern;
                        break;
                    case "M": // MonthDayPattern
                        result = dtFmt.MonthDayPattern;
                        break;
                    case "r": // RFC1123Pattern
                        result = dtFmt.RFC1123Pattern;
                        break;
                    case "R": // RFC1123Pattern
                        result = dtFmt.RFC1123Pattern;
                        break;
                    case "s": // SortableDateTimePattern
                        result = dtFmt.SortableDateTimePattern;
                        break;
                    case "t": // ShortTimePattern
                        result = dtFmt.ShortTimePattern;
                        break;
                    case "T": // LongTimePattern
                        result = dtFmt.LongTimePattern;
                        break;
                    case "u": // UniversalSortableDateTimePattern
                        result = dtFmt.UniversalSortableDateTimePattern;
                        break;
                    case "U": // Full date and time (long date and long time) using universal time
                        result = dtFmt.LongDatePattern + " " + dtFmt.LongTimePattern;
                        break;
                    case "y": // YearMonthPattern
                        result = dtFmt.YearMonthPattern;
                        break;
                    case "Y": // YearMonthPattern
                        result = dtFmt.YearMonthPattern;
                        break;
                }
            }
            else
            {
                string resultPtrn = "";
                string curPtrn = "";
                bool exceptNext = false;
                bool exceptLoop = false;
                char prevCh = ' ';
                for (int i = 0; i < result.Length; i++)
                {
                    char ch = result[i];
                    if (exceptNext)
                    {
                        exceptNext = false;
                        resultPtrn = HandlePreviewPattern(resultPtrn, curPtrn);
                        resultPtrn = resultPtrn + ch;
                        curPtrn = "";
                        continue;
                    }
                    else if (ch == '\\')
                    {
                        exceptNext = true;
                        resultPtrn = HandlePreviewPattern(resultPtrn, curPtrn);
                        resultPtrn = resultPtrn + ch;
                        curPtrn = "";
                        continue;
                    }
                    if (ch == '\'')
                    {
                        if (exceptLoop)
                            exceptLoop = false;
                        else
                            exceptLoop = true;
                        resultPtrn = HandlePreviewPattern(resultPtrn, curPtrn);
                        resultPtrn = resultPtrn + ch;
                        curPtrn = "";
                        continue;
                    }
                    if (exceptLoop)
                    {
                        resultPtrn = HandlePreviewPattern(resultPtrn, curPtrn);
                        resultPtrn = resultPtrn + ch;
                        curPtrn = "";
                        continue;
                    }
                    if (prevCh != ch)
                    {
                        //curPtrn         
                        resultPtrn = HandlePreviewPattern(resultPtrn, curPtrn);
                        curPtrn = "";
                        curPtrn = "" + ch;
                    }
                    else
                    {
                        curPtrn = curPtrn + ch;
                    }
                    if (i == result.Length - 1)
                    {
                        //curPtrn
                        resultPtrn = HandlePreviewPattern(resultPtrn, curPtrn);
                        curPtrn = "";
                    }
                    prevCh = ch;
                }
                return resultPtrn;
            }
            return result;

        }

        private string HandlePreviewPattern(string resultPtrn, string curPtrn)
        {
            string s;
            if (curPtrn.IndexOf('z') != -1)
            {
                s = resultPtrn + "'" + this.GetDate().ToString("Q" + curPtrn, this.Culture).Substring(1) + "'";
            }
            //else if (curPtrn.IndexOf('g') != -1)
            //{
            //    s = resultPtrn + "'" + this.GetDate().ToString("Q" + curPtrn, this.Culture).Substring(1) + "'";
            //}
            else
            {
                s = resultPtrn + curPtrn;
            }
            return s;
        }

        #endregion
    }

    /// <summary>
    /// Represents the method that will handle the DateChanged event of the control. 
    /// </summary>
    /// <param name="sender">Source of the event.</param>
    /// <param name="e">The EventArgs that contains the event data.</param>
    public delegate void C1DateChangedEventHandler(object sender, EventArgs e);

    [SmartAssembly.Attributes.DoNotObfuscate]
    internal class DateTimeInfo
    {
        #region Static Member

        internal static DateTime[] EraStartDates = new DateTime[4] { new DateTime(1868, 9, 8), new DateTime(1912, 7, 30), new DateTime(1926, 12, 25), new DateTime(1989, 1, 8) };
        internal static int[] WarekiDate = new int[] { 18680908, 19120730, 19261225, 19890108, 20871231 };
        internal static int[] EraYears = new int[] { 45, 15, 64, 99 };
        internal static int EraCount = 4;
        internal static DateTime EraMax = new DateTime(2087, 12, 31, 23, 59, 59);
        internal static DateTime EraMin = new DateTime(1868, 9, 8);
        internal static string DateTimeLongFormat = "yyyyMMddHHmmss";
        internal static string[] Designators;
        internal static string[] EraNames = { "\u660E\u6CBB", "\u5927\u6B63", "\u662D\u548C", "\u5E73\u6210" };
        internal static string[] AbbreviatedEraNames = { "\u660E", "\u5927", "\u662D", "\u5E73" };
        internal static string[] SymbolEraNames = new string[] { "M", "T", "S", "H" };
        internal static string[][] ErasShortcuts = null;
        internal static string[] WeekDays;
        internal static string[] MonthNames;
        internal static string[] ShortWeekDays;
        internal static string[] ShortMonthNames;
        internal static bool IsUS;
        internal static CultureInfo USCulture;
        internal static CultureInfo CurrentCulture;
        internal static int Digits = 2;
        internal static int EraYearMax = 99;
        #endregion end of Static Member.

        #region Static constructor

        public DateTimeInfo(string cultureName)
        {
            DateTimeFormatInfo dtinfo;
            CultureInfo culture = new CultureInfo(cultureName, true);

            // add by Sean Huang at 2008.10.09 -->
            // using the current culture if the culture name does not change.
            if (cultureName == CultureInfo.CurrentCulture.Name)
            {
                culture = CultureInfo.CurrentCulture;
            }
            // end of Sean Huang <--

            IsUS = (culture.LCID & 0xFF) == 0x09;	// 0x??09	English

            USCulture = new CultureInfo("en-US");

            try
            {
                dtinfo = culture.DateTimeFormat;
            }
            catch
            {
                CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

                foreach (CultureInfo cul in cultures)
                {
                    if (cul.Name.ToLower().IndexOf(cultureName.ToLower()) != -1)
                    {
                        culture = cul;
                    }
                }

                dtinfo = culture.DateTimeFormat;
            }

            CurrentCulture = culture;
            // Get the names from current culture
            Designators = new String[] { dtinfo.AMDesignator, dtinfo.PMDesignator };
            WeekDays = dtinfo.DayNames;
            MonthNames = dtinfo.MonthNames;
            ShortMonthNames = dtinfo.AbbreviatedMonthNames;
            ShortWeekDays = DateTimeInfo.GetShortWeekDays(culture);
        }

        static DateTimeInfo()
        {
            CultureInfo culture = CultureInfo.CurrentCulture;
            CurrentCulture = culture;
            IsUS = (culture.LCID & 0xFF) == 0x09;	// 0x??09	English

            USCulture = new CultureInfo("en-US");

            DateTimeFormatInfo dtinfo = culture.DateTimeFormat;

            // Get the names from current culture
            Designators = new String[] { dtinfo.AMDesignator, dtinfo.PMDesignator };
            WeekDays = dtinfo.DayNames;
            MonthNames = dtinfo.MonthNames;
            ShortMonthNames = dtinfo.AbbreviatedMonthNames;
            ShortWeekDays = DateTimeInfo.GetShortWeekDays(culture);
        }
        #endregion end of constructor.

        #region Methods

        internal static bool IsValidEraDate(DateTime datetime)
        {
            if (datetime < DateTimeInfo.EraMin || datetime > DateTimeInfo.EraMax)
            {
                return false;
            }

            return true;
        }

        public static int ConvertToGregorianYear(int era, int eraYear, bool strict)
        {
            if (era < 0 || era >= EraCount || eraYear < 1 || strict && eraYear > EraYears[era])
            {
                return -1;
            }

            return EraStartDates[era].Year + eraYear - 1;
        }

        public static DateTime? ConvertToGregorianDateTime(int era, int eraYear, int month, int day, int hour, int minute, int second, bool strict)
        {
            int year = DateTimeInfo.ConvertToGregorianYear(era, eraYear, strict);

            if (year < 1 || year > 9999)
            {
                return null;
            }
            if (month < 1 || month > 12)
            {
                return null;
            }
            int maxdays;
            if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
            {
                maxdays = 31;
            }
            else if (month == 4 || month == 6 || month == 9 || month == 11)
            {
                maxdays = 30;
            }
            else
            {
                maxdays = DateTime.IsLeapYear(year) ? 29 : 28;
            }

            if (day < 1 || day > maxdays)
            {
                return null;
            }
            if (hour < 0 || hour > 23)
            {
                return null;
            }
            if (minute < 0 || minute > 59)
            {
                return null;
            }
            if (second < 0 || second > 59)
            {
                return null;
            }
            DateTime dateTime = new DateTime(year, month, day, hour, minute, second);

            if (strict)
            {
                DateTime startDate = EraStartDates[era];
                DateTime endDate = era + 1 != EraCount ? EraStartDates[era + 1].AddMilliseconds(-1) : EraMax;

                if (dateTime < startDate || dateTime > endDate)
                {
                    return null;
                }
            }

            return dateTime;
        }

        internal static int DateTime2Int(DateTime date)
        {
            return (date.Year * 100 + date.Month) * 100 + date.Day;
        }

        internal static DateTime Int2DateTime(int value)
        {
            return new DateTime(value / 10000, (value % 10000) / 100, value % 100);
        }

        internal static bool GetEraDate(DateTime date, out int era, out int eraYear)
        {
            era = -1;
            eraYear = -1;
            if (!IsValidEraDate(date))
            {
                return false;
            }

            //Fix bug#6526.
            DateTime maxDate;

            if (EraMax == DateTime.MaxValue)
            {
                maxDate = EraMax;

                if (date == EraMax)
                {
                    era = EraCount - 1;
                    eraYear = date.Year - EraStartDates[era].Year + 1;

                    return true;
                }
            }
            else
            {
                maxDate = EraMax.AddMilliseconds(1);
            }

            for (int i = 0; i < EraCount; i++)
            {
                DateTime nextDate = i + 1 != EraCount ? EraStartDates[i + 1] : maxDate;

                if (date < nextDate)
                {
                    era = i;
                    eraYear = date.Year - EraStartDates[i].Year + 1;

                    return true;
                }
            }

            return false;
        }

        internal static bool GetEraDate(DateTime date, out int era, out int erayear, bool startAsPrevious)
        {
            if (GetEraDate(date, out era, out erayear))
            {
                if (startAsPrevious && era > 0 && erayear == 1)
                {
                    if (date.Month == (WarekiDate[era] % 10000) / 100)
                    {
                        era--;
                        erayear = EraYears[era];
                    }
                }
                return true;
            }

            return false;
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal static string GetEraName(DateTime date)
        {
            if (!IsValidEraDate(date))
            {
                return String.Empty;
            }

            int era;
            int eraYear;

            if (GetEraDate(date, out era, out eraYear))
            {
                return EraNames[era];
            }

            return String.Empty;
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal static string GetAbbreviatedEraNames(DateTime date)
        {
            if (!IsValidEraDate(date))
            {
                return String.Empty;
            }

            int era;
            int eraYear;

            if (GetEraDate(date, out era, out eraYear))
            {
                return AbbreviatedEraNames[era];
            }

            return String.Empty;
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal static string GetSymbolEraNames(DateTime date)
        {
            if (!IsValidEraDate(date))
            {
                return String.Empty;
            }

            int era;
            int eraYear;

            if (GetEraDate(date, out era, out eraYear))
            {
                return SymbolEraNames[era];
            }

            return String.Empty;
        }

        [SmartAssembly.Attributes.DoNotObfuscate]
        internal static int GetEraYear(DateTime date)
        {
            if (!IsValidEraDate(date))
            {
                return -1;
            }

            int era;
            int eraYear;

            if (GetEraDate(date, out era, out eraYear))
            {
                return eraYear;
            }

            return -1;
        }

        public static string[] GetShortWeekDays(CultureInfo ci)
        {
            string[] shortWeekdays;
            string s;

            try
            {
                shortWeekdays = ci.DateTimeFormat.AbbreviatedDayNames;
            }
            catch
            {
                CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

                foreach (CultureInfo cul in cultures)
                {
                    if (cul.Name.ToLower().IndexOf(ci.Name.ToLower()) != -1)
                    {
                        ci = cul;
                    }
                }

                shortWeekdays = ci.DateTimeFormat.AbbreviatedDayNames;
            }

            if (!((ci.LCID & 0xFF) == 0x09))
            {
                // Need special process with name of Chinese short weekdays
                if ((ci.LCID & 0xFF) == 0x04)	// 0x??04	Chinese
                {
                    for (int i = 0; i < shortWeekdays.Length; i++)
                    {
                        s = shortWeekdays[i];
                        shortWeekdays[i] = s.Substring(s.Length - 1, 1);
                    }

                }
            }
            else
            {
                // Change the name of short weekdays for Calendar
                for (int i = 0; i < shortWeekdays.Length; i++)
                {
                    s = shortWeekdays[i];
                    shortWeekdays[i] = s.Substring(0, 1);
                }
            }

            return shortWeekdays;
        }
        #endregion end of methods.
    }
}
