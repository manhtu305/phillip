﻿using System;
using System.Collections;
using System.Drawing;
using System.Web.UI;
using System.ComponentModel;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Input
{
    /// <summary>
    /// The main Web control used for entering and editing information 
    /// of any data type in a text form.
    /// </summary>    
    [ToolboxData("<{0}:C1InputText runat=server></{0}:C1InputText>")]
    [ToolboxBitmap(typeof(C1InputText), "TextBox.C1InputText.png")]
    [DefaultProperty("Text")]
	[ControlValueProperty("Text")]
    [LicenseProviderAttribute()]
    [ValidationProperty("Text")]
    public partial class C1InputText : C1InputBase
    {
        private static readonly object TextChangedEventKey = new object();
        private readonly EditControl _editControl;

        /// <summary>
        ///  Initialize a new instance of the C1InputText control.
        /// </summary>
        public C1InputText()
        {
            this._editControl = new EditControl(this);
        }

        /// <summary>
        /// Fires when the text is changed.
        /// </summary>
        [C1Description("C1Input.TextChanged")]
        public event C1TextChangedEventHandler TextChanged
        {
            add
            {
                Events.AddHandler(TextChangedEventKey, value);
            }

            remove
            {
                Events.RemoveHandler(TextChangedEventKey, value);
            }
        }

        /// <summary>
        /// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
        /// server control. This property is used primarily by control developers.
        /// </summary>
        /// <returns>
        /// One of the System.Web.UI.HtmlTextWriterTag enumeration values.
        /// </returns>
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                if (!this.DesignMode && this.MultiLine)
                {
                    return HtmlTextWriterTag.Textarea;
                }
                else
                {
                    return base.TagKey;
                }
            }
        }

        /// <summary>
        /// Raise the TextChanged event.
        /// </summary>
        /// <param name="args">Event arguments.</param>
        public virtual void OnTextChanged(EventArgs args)
        {
            C1TextChangedEventHandler d = (C1TextChangedEventHandler)Events[TextChangedEventKey];
            if (d != null)
            {
                d(this, args);
            }
        }

        protected override void OnPropertyValueChanged<V>(string propertyName, V value)
        {
            base.OnPropertyValueChanged(propertyName, value);

            switch (propertyName)
            {
                case "Format":
                case "PromptChar":
                case "PasswordChar":
                case "AutoConvert":
                case "CountWrappedLine":
                case "LengthAsByte":
                case "MaxLength":
                case "MaxLineCount":
                case "MultiLine":
                    UpdateEditControl();
                    break;

                case "Text":
                    if (this.Text != this._editControl.Text)
                    {
                        this._editControl.Text = this.Text;
                        TypeDescriptor.GetProperties(this)["Text"].SetValue(this, this._editControl.Text);
                    }
                    break;
            }
        }

        private void UpdateEditControl()
        {
            this._editControl.DesignMode = this.DesignMode;
            this._editControl.Format = this.Format;
            this._editControl.AutoConvert = this.AutoConvert;
            this._editControl.CountWrappedLine = this.CountWrappedLine;
            this._editControl.LengthAsByte = this.LengthAsByte;
            this._editControl.MaxLength = this.MaxLength;
            this._editControl.MaxLineCount = this.MaxLineCount;
            this._editControl.MultiLine = this.MultiLine;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        protected override bool LoadClientData(Hashtable data)
        {
            string o = data["text"] as string;
            if (o != this.Text)
            {
                this.Text = o;
                return true;
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void RaisePostDataChangedEvent()
        {
            base.RaisePostDataChangedEvent();
            this.OnTextChanged(EventArgs.Empty);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override string GetDesignTimeTextValue()
        {
            return this._editControl.Text;
        }
    }
}
