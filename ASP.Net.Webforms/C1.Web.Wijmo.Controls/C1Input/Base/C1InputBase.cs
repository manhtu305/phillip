﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Collections;
using System.Globalization;
using System.Reflection;
using System.Security.Permissions;
using System.IO;
using System.Collections.Specialized;



namespace C1.Web.Wijmo.Controls.C1Input
{
	using C1.Web.Wijmo.Controls.Base;
	using C1.Web.Wijmo.Controls.Localization;


	/// <summary>
	/// Base class for C1InputMask, C1InputDate, C1InputText, C1InputNumber, C1InputCurrency and C1InputPercent.
	/// </summary>
#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1Input.C1InputDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1Input.C1InputDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1Input.C1InputDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1Input.C1InputDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	[ParseChildren(true)]
	public abstract partial class C1InputBase : C1TargetControlBase,
		IPostBackDataHandler,
		IPostBackEventHandler, 
		INamingContainer 
	{
		#region  Fields
		private bool _productLicensed = false;
		private bool _shouldNag;
		#endregion

		#region Constructor

		/// <summary>
		///Creates a new instance of the C1InputBase class.
		/// </summary>
		public C1InputBase()
		{
			VerifyLicense();
		}

		internal C1InputBase(string key)
			: this()
		{
			VerifyLicense(key);
		}
	   
		#endregion

		#region Licensing

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1InputBase), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		/// <summary>
		/// Verifies the license.
		/// </summary>
		/// <param name="key">The key.</param>
		internal void VerifyLicense(string key)
		{
#if GRAPECITY
			_productLicensed = true;
#else
			// ttZzKzJ5mwNr/IihpBl2pA==
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1InputBase), this,
				Assembly.GetExecutingAssembly(), key);
			_shouldNag = licinfo.ShouldNag;
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		#endregion

		#region Properties

        /// <summary>
        /// Gets or sets the Cascading Style Sheet (CSS) class rendered by the Web server
        /// control on the client.
        /// </summary>
        [Json(true)]
        public override string CssClass
        {
            get { return base.CssClass; }
            set {  base.CssClass = value; }
        }

		/// <summary>
		/// PostBack Event Reference. Empty string if AutoPostBack property is false.
		/// </summary>
		[WidgetOption()]
		[DefaultValue("")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string PostBackEventReference
		{
			get
			{
				if (!IsDesignMode && this.AutoPostBack)
				{
					return this.Page.ClientScript.GetPostBackEventReference(this, "");
				}
				return "";
			}
		}

		/// <summary>
		/// Gets or sets a value that indicates whether an automatic postback to the server occurs when the TextBox control loses focus.
		/// </summary>
		[DefaultValue(false)]
		[C1Category("Category.Behavior")]
		[Layout(LayoutType.Behavior)]
		[C1Description("C1Input.AutoPostBack", "Gets or sets a value that indicates whether an automatic postback to the server occurs when the TextBox control loses focus.")]
		[WidgetOption()]
		public bool AutoPostBack
		{
			get
			{
				return this.GetPropertyValue<bool>("AutoPostBack", false);
			}
			set
			{
				this.SetPropertyValue<bool>("AutoPostBack", value);
			}
		}

		/// <summary>
		/// Gets the text value of input for design time.
		/// </summary>
		/// <returns></returns>
		protected virtual string GetDesignTimeTextValue()
		{
			return "";
		}

		//protected virtual bool IsPasswordMode()
		//{
		//    return false;
		//}

		/// <summary>
		/// Gets the alignment of text in style value.
		/// </summary>
		/// <returns>The alignment of text in style value</returns>
		protected virtual string GetTextAlign()
		{
			return "left";
		}

		/// <summary>
		/// Gets the default CSS class for input.
		/// </summary>
		/// <returns>The default CSS class for input.</returns>
		/// <remarks>Internal use only.</remarks>
		protected virtual string GetInputCss()
		{
			return "";
		}

		#endregion

		internal override bool IsWijMobileControl
		{
			get
			{
				return false;
			}
		}

		#region IPostBackEventHandler

		/// <summary>
		/// Enables a server control to process an event raised when a form is posted to the server.
		/// </summary>
		/// <param name="eventArgument"></param>
		void IPostBackEventHandler.RaisePostBackEvent(string eventArgument)
		{
		}

		#endregion

		#region IPostBackDataHandler

		/// <summary>
		/// Loads data from the client.
		/// </summary>
		/// <param name="data">A hashtable that stores the data name and value.</param>
		protected virtual bool LoadClientData(Hashtable data)
		{
			return false;
		}

		/// <summary>
		/// Processes the postback data.
		/// </summary>
		/// <param name="postDataKey"></param>
		/// <param name="postCollection"></param>
		/// <returns></returns>
		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);
			return this.LoadClientData(data);
		}

		/// <summary>
		/// Signals the server control to notify the ASP.NET application that the state of the control has changed.
		/// </summary>
		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
			RaisePostDataChangedEvent();
		}
		protected virtual void RaisePostDataChangedEvent()
		{
		}


		#endregion

		#region Render

		/// <summary>
		///     Called by the ASP.NET page framework to notify server controls that use composition-based
		///     implementation to create any child controls they contain in preparation for
		///     posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			if (this.IsDesignMode)
			{
				this.Controls.Clear();
				this.CreateWrapper();
				this.CreateTriggerAndSpinner();
			}

			base.CreateChildControls();
		}

		internal static class DesignTimeLayout
		{
			public const int WrapperWidth = 150;
			public const int WrapperHeight = 30;
			public const int TriggerWidth = 27;
			public const int SpinnerWidth = 17;
		}

        internal int GetPaddingLeft()
        {
            int paddingLeft = 0;

            if (this.ShowDropDownButton)
            {
                if (this.DropDownButtonAlign == ButtonAlign.Left)
                {
                    paddingLeft += DesignTimeLayout.TriggerWidth;
                }
            }

            if (this.ShowSpinner)
            {
                switch (this.SpinnerAlign)
                {
                    case SpinnerAlign.VerticalLeft:
                    case SpinnerAlign.HorizontalUpLeft:
                    case SpinnerAlign.HorizontalDownLeft:
                        paddingLeft += DesignTimeLayout.SpinnerWidth;
                        break;
                }
            }

            return paddingLeft;
        }

        internal int GetPaddingRight()
        {
            int paddingRight = 0;

            if (this.ShowDropDownButton)
            {
                if (this.DropDownButtonAlign == ButtonAlign.Right)
                {
                    paddingRight += DesignTimeLayout.TriggerWidth;
                }
            }
            
            
            if (this.ShowSpinner)
            {
                switch (this.SpinnerAlign)
                {
                    case SpinnerAlign.VerticalRight:
                    case SpinnerAlign.HorizontalUpLeft:
                    case SpinnerAlign.HorizontalDownLeft:
                        paddingRight += DesignTimeLayout.SpinnerWidth;
                        break;
                }
            }

            return paddingRight;
        }

		private void CreateWrapper()
		{
			int width = (int)this.Width.Value;
			int height = (int)this.Height.Value;
			if (width == 0) width = C1InputBase.DesignTimeLayout.WrapperWidth;
			if (height == 0) height = DesignTimeLayout.WrapperHeight;

			HtmlGenericControl wrapper = new HtmlGenericControl("span");
			wrapper.Attributes["class"] = this.CssClass + " wijmo-wijinput-wrapper";

		    int paddingLeft = this.GetPaddingLeft();
            int paddingRight = this.GetPaddingRight();
            wrapper.Style.Add(HtmlTextWriterStyle.PaddingLeft, paddingLeft.ToString() + "px");
            wrapper.Style.Add(HtmlTextWriterStyle.PaddingRight, paddingRight.ToString() + "px");
	
            bool isRightAligned = this.GetTextAlign() == "right";
			if (isRightAligned)
			{
			    if (!this.ShowDropDownButton && !this.ShowSpinner)
			    {
	                width -= 8;
			    }
			    else
                {
                    width -= 2 + paddingLeft + paddingRight;//(this.ShowTrigger ? DesignTimeLayout.TriggerWidth : 0) + (this.ShowSpinner ? DesignTimeLayout.SpinnerWidth : 0);

                }
			}

			TextBox txtBox = new TextBox();
			txtBox.CssClass = "wijmo-wijinput-input ui-corner-all " + this.GetInputCss();
			txtBox.Width = width;
			txtBox.Height = height;
			txtBox.ToolTip = this.ToolTip;
			txtBox.AccessKey = this.AccessKey;
			txtBox.Style.Add(HtmlTextWriterStyle.TextAlign, this.GetTextAlign());
            txtBox.TextMode = TextBoxMode.SingleLine;
			txtBox.Text = GetDesignTimeTextValue();

			wrapper.Controls.Add(txtBox);
			this.Controls.Add(wrapper);
		}

        private void CreateTriggerAndSpinner()
		{
            int height = (int)this.Height.Value;
            if (height == 0) height = DesignTimeLayout.WrapperHeight;
            bool leftDropDownBtn = this.DropDownButtonAlign == C1Input.ButtonAlign.Left;
            bool leftSpinnerBtn = this.SpinnerAlign == SpinnerAlign.VerticalLeft;

            #region CreateElement

            HtmlGenericControl span = new HtmlGenericControl("span");
            Panel trigger = new Panel();
            span.Attributes["class"] = "ui-icon ui-icon-triangle-1-s";
            // fixed the issue 28891 by dail 2012-10-15. 
            // for vs2012, the 50% style is seems to lost effect in designer.                

            trigger.Height = height;
            trigger.CssClass = "wijmo-wijinput-trigger ui-state-default" + (leftDropDownBtn ? " ui-corner-left" : " ui-corner-right");
            trigger.Style.Add(HtmlTextWriterStyle.BorderWidth, "0px");
            trigger.Style.Add(HtmlTextWriterStyle.BorderStyle, "soild");
            trigger.Style.Add(HtmlTextWriterStyle.BorderColor, "red");
            trigger.Controls.Add(span);

            Panel spinnerLeft = new Panel();
            Panel spinnerRight = new Panel();
            Panel arrowUp = new Panel();
            Panel arrowDown = new Panel();
            Panel spinUpElement = new Panel();
            Panel spinDownElement = new Panel();

            spinnerLeft.Height = height;
            spinnerRight.Height = height;
            spinUpElement.Style.Add("border-top-width", "0px");
            spinUpElement.Style.Add("border-bottom-width", "0px");
            spinDownElement.Style.Add("border-top-width", "0px");
            spinDownElement.Style.Add("border-bottom-width", "0px");
            spinnerLeft.CssClass = "wijmo-wijinput-spinner-left wijmo-wijinput-button";
            spinnerRight.CssClass = "wijmo-wijinput-spinner-right wijmo-wijinput-button";
            spinnerLeft.Width = DesignTimeLayout.SpinnerWidth;
            spinnerRight.Width = DesignTimeLayout.SpinnerWidth;

            if (this.SpinnerAlign == SpinnerAlign.VerticalLeft || this.SpinnerAlign == SpinnerAlign.VerticalRight)
            {
                arrowUp.CssClass = "ui-icon ui-icon-triangle-1-n";
                arrowDown.CssClass = "ui-icon ui-icon-triangle-1-s";
                spinUpElement.CssClass = "ui-state-default wijmo-wijinput-spinup";
                spinDownElement.CssClass = "ui-state-default wijmo-wijinput-spindown";
            }
            else
            {
                arrowUp.CssClass = "ui-icon ui-icon-plus";
                arrowDown.CssClass = "ui-icon ui-icon-minus";
                spinUpElement.CssClass = "ui-state-default wijmo-wijinput-spin";
                spinDownElement.CssClass = "ui-state-default wijmo-wijinput-spin";
            }

            spinUpElement.Controls.Add(arrowUp);
            spinDownElement.Controls.Add(arrowDown);

            #endregion

            #region CreateFlagStyle

            if (this.ShowDropDownButton && this.ShowSpinner)
            {
                if (leftDropDownBtn)
                {
                    switch (this.SpinnerAlign)
                    {
                        case SpinnerAlign.VerticalLeft:
                            this.CssClass += "ui-input-spinner-trigger-left";
                            break;
                        case SpinnerAlign.VerticalRight:
                            this.CssClass += "ui-input-trigger-left ui-input-spinner-right";
                            break;
                        case SpinnerAlign.HorizontalUpLeft:
                        case SpinnerAlign.HorizontalDownLeft:
                            this.CssClass += "ui-input-spinner-trigger-left ui-input-spinner-right";
                            break;
                    }
                }
                else
                {
                    switch (this.SpinnerAlign)
                    {
                        case SpinnerAlign.VerticalLeft:
                            this.CssClass += "ui-input-trigger-right ui-input-spinner-left";
                            break;
                        case SpinnerAlign.VerticalRight:
                            this.CssClass += "ui-input-spinner-trigger-right";
                            break;
                        case SpinnerAlign.HorizontalUpLeft:
                        case SpinnerAlign.HorizontalDownLeft:
                            this.CssClass += "ui-input-spinner-trigger-right ui-input-spinner-left";
                            break;
                    }
                }

            }
            else if (this.ShowDropDownButton)
            {
                if (leftDropDownBtn)
                {
                    this.CssClass += "ui-input-trigger-left";
                }
                else
                {
                    this.CssClass += "ui-input-trigger-right";
                }
            }
            else if (this.ShowSpinner)
            {
                switch (this.SpinnerAlign)
                {
                    case SpinnerAlign.VerticalLeft:
                        this.CssClass += "ui-input-spinner-left";
                        break;
                    case SpinnerAlign.VerticalRight:
                        this.CssClass += "ui-input-spinner-right";
                        break;
                    case SpinnerAlign.HorizontalUpLeft:
                    case SpinnerAlign.HorizontalDownLeft:
                        this.CssClass += "ui-input-spinner-left ui-input-spinner-right";
                        break;
                }
            }

            #endregion

            #region CreateCornerStyle

            if (this.ShowDropDownButton && this.ShowSpinner)
            {
                if (leftDropDownBtn)
                {
                    switch (this.SpinnerAlign)
                    {
                        case SpinnerAlign.VerticalRight:
                            spinUpElement.CssClass += " ui-corner-tr";
                            spinDownElement.CssClass += " ui-corner-br";
                            break;
                        case SpinnerAlign.HorizontalUpLeft:
                            spinUpElement.CssClass += " ui-corner-right";
                            break;
                        case SpinnerAlign.HorizontalDownLeft:
                            spinDownElement.CssClass += " ui-corner-right";
                            break;
                    }
                }
                else
                {
                    switch (this.SpinnerAlign)
                    {
                        case SpinnerAlign.VerticalLeft:
                            spinUpElement.CssClass += " ui-corner-tl";
                            spinDownElement.CssClass += " ui-corner-bl";
                            break;
                        case SpinnerAlign.HorizontalDownLeft:
                            spinDownElement.CssClass += " ui-corner-left";
                            break;
                        case SpinnerAlign.HorizontalUpLeft:
                            spinUpElement.CssClass += " ui-corner-left";
                            break;
                    }
                }

            }
            else if (this.ShowSpinner)
            {
                switch (this.SpinnerAlign)
                {
                    case SpinnerAlign.VerticalLeft:
                        spinUpElement.CssClass += " ui-corner-tl";
                        spinDownElement.CssClass += " ui-corner-bl";
                        break;
                    case SpinnerAlign.VerticalRight:
                        spinUpElement.CssClass += " ui-corner-tr";
                        spinDownElement.CssClass += " ui-corner-br";
                        break;
                    case SpinnerAlign.HorizontalDownLeft:
                        spinDownElement.CssClass += " ui-corner-left";
                        spinUpElement.CssClass += " ui-corner-right";
                        break;
                    case SpinnerAlign.HorizontalUpLeft:
                        spinUpElement.CssClass += " ui-corner-left";
                        spinDownElement.CssClass += " ui-corner-right";
                        break;
                }
            }

            #endregion

            #region CreateLayout
            if (this.ShowDropDownButton)
            {
                this.Controls.Add(trigger);
            }

            if (this.ShowSpinner)
            {
                switch (this.SpinnerAlign)
                {
                    case SpinnerAlign.VerticalLeft:
                        spinnerLeft.Controls.Add(spinUpElement);
                        spinnerLeft.Controls.Add(spinDownElement);
                        this.Controls.Add(spinnerLeft);
                        break;
                    case SpinnerAlign.VerticalRight:
                        spinnerRight.Controls.Add(spinUpElement);
                        spinnerRight.Controls.Add(spinDownElement);
                        this.Controls.Add(spinnerRight);
                        break;
                    case SpinnerAlign.HorizontalDownLeft:
                        this.Controls.Add(spinnerLeft);
                        this.Controls.Add(spinnerRight);
                        spinnerLeft.Controls.Add(spinDownElement);
                        spinnerRight.Controls.Add(spinUpElement);
                        break;
                    case SpinnerAlign.HorizontalUpLeft:
                        this.Controls.Add(spinnerLeft);
                        this.Controls.Add(spinnerRight);
                        spinnerLeft.Controls.Add(spinUpElement);
                        spinnerRight.Controls.Add(spinDownElement);
                        break;
                }
            }

            #endregion
		}

	    /// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="HtmlTextWriter"/>
		/// </summary>
		/// <param name="writer">The <see cref="HtmlTextWriter"/> object that receives the server control content.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
            var cssClass = new StringBuilder();

            if (this.IsDesignMode)
            {
                cssClass.Append(CssClass).Append(" wijmo-wijinput ui-widget ui-helper-clearfix ui-state-default ui-corner-all");

                if (this.ShowDropDownButton || this.ShowSpinner)
                {
                    cssClass.Append(" ui-input");
                    if (this.ShowSpinner)
                        cssClass.Append("-spinner");
                    if (this.ShowDropDownButton)
                        cssClass.Append("-trigger");
                    if (this.DropDownButtonAlign == C1Input.ButtonAlign.Left)
                        cssClass.Append("-left");
                    else
                        cssClass.Append("-right");
                }

                var width = !this.Width.IsEmpty ? Width : DesignTimeLayout.WrapperWidth;
                this.Style["width"] = width.ToString();
                this.Style["display"] = "inline-block";
            }
            else
            {
                cssClass.Append(String.Format(" {0}", Utils.GetHiddenClass()));
            }

            string origCssClass = CssClass;
            bool origEnabled = Enabled;
            var origWidth = Width;
            var origHeight = Height;
            try
            {
                this.CssClass = cssClass.ToString();
                this.Enabled = true;

				if (IsDesignMode)
				{
					if (Width.Value == 0.0)
						Width = DesignTimeLayout.WrapperWidth;
					if (Height.Value == 0.0)
						Height = DesignTimeLayout.WrapperHeight;
				}

                base.AddAttributesToRender(writer);
            }
            finally
            {
                CssClass = origCssClass;
                Enabled = origEnabled;
                Width = origWidth;
                Height = origHeight;
            }
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}

		/// <summary>
		///  Raises the PreRender event.
		/// </summary>
		/// <param name="e">An EventArgs object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);

			if (this.AutoPostBack)
			{
				Page.ClientScript.GetPostBackEventReference(this, "");
			}
		}


		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The HtmlTextWriter object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			this.EnsureChildControls();
			base.Render(writer);
		}


		//
		// Summary:
		//     Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
		//     server control. This property is used primarily by control developers.
		//
		// Returns:
		//     One of the System.Web.UI.HtmlTextWriterTag enumeration values.
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				if (this.IsDesignMode)
				{
					return HtmlTextWriterTag.Div;
				}
				else
				{
					return HtmlTextWriterTag.Input;
				}
			}
		}

		protected override void EnsureEnabledState()
		{
			return;
		}

		#endregion end of ** methods
    }
}
