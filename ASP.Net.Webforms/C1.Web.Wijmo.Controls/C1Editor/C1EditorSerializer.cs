﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1Editor
{
	/// <summary>
	/// Serializes C1Editor Control.
	/// </summary>
	public class C1EditorSerializer : C1BaseSerializer<C1Editor, object, object>
	{
		#region ** constructor
		/// <summary>
		/// Initializes a new instance of the <see cref="C1EditorSerializer"/> class.
		/// </summary>
		/// <param name="obj">Serializable Object.</param>
		public C1EditorSerializer(Object obj)
			: base(obj)
		{
		}
		#endregion end of ** constructor
	}
}
