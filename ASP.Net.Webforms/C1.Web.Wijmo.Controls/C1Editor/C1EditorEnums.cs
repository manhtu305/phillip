﻿using System;
using System.Collections.Generic;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Editor
{

	#region ** the TextImageRelation enumerations
	/// <summary>
	/// Enumeration that determines the position relationship of text and image.
	/// </summary>
	public enum TextImageRelation
	{
		/// <summary>
		/// Image is aligned before the text.
		/// </summary>
		ImageBeforeText = 0,
		/// <summary>
		/// Text is aligned before the image.
		/// </summary>
		TextBeforeImage = 1,
		/// <summary>
		/// Image is aligned above the text.
		/// </summary>
		ImageAboveText = 2,
		/// <summary>
		/// Text is aligned above the image.
		/// </summary>
		TextAboveImage = 3
	}
	#endregion end of ** the TextImageRelation enumerations.
}
