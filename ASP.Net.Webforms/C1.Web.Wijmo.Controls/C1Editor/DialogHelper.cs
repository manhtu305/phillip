﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Web.UI;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using C1.Web.Wijmo.Controls.Localization;
using System.Globalization;

namespace C1.Web.Wijmo.Controls.C1Editor
{

	internal class DialogHelper
	{
		#region fields
		private CultureInfo _culture;
		#endregion fields

		#region ** private properties
		private string NoFileMessage
		{
			get
			{
				return C1Localizer.GetString("C1Editor.NoFileMessage", _culture);
			}
		}

		private string UploadSuccessMessage
		{
			get
			{
				return C1Localizer.GetString("C1Editor.UploadSuccessMessage", _culture);
			}
		}

		private string UploadFailureMessage
		{
			get
			{
				return C1Localizer.GetString("C1Editor.UploadFailureMessage", _culture);
			}
		}

		private string NoImagesMessage
		{
			get
			{
				return C1Localizer.GetString("C1Editor.NoImagesMessage", _culture);
			}
		}

		private string NoFolderSpecifiedMessage
		{
			get
			{
				return C1Localizer.GetString("C1Editor.NoFolderSpecifiedMessage", _culture);
			}
		}

		private string NoFileToDeleteMessage
		{
			get
			{
				return C1Localizer.GetString("C1Editor.NoFileToDeleteMessage", _culture);
			}
		}

		private string InvalidFileTypeMessage
		{
			get
			{
				return C1Localizer.GetString("C1Editor.InvalidFileTypeMessage", _culture);
			}
		}

		#endregion end of ** private properties.

		#region ctor
		public DialogHelper(CultureInfo culture)
		{
			_culture = culture;
		}
		#endregion ctor

		#region ** common methods
		private static string[] GetFiles(Page page, string path)
		{
			if (path == "")
			{
				return null;
			}

			try
			{
				string foldName = page.MapPath(path);

				if (!Directory.Exists(foldName))
				{
					Directory.CreateDirectory(foldName);
				}

				return Directory.GetFiles(foldName, "*");
			}
			catch
			{
				return null;
			}
		}

		private static bool IsValidFileType(string fileName, string[] validTypes)
		{
			if (validTypes == null)
			{
				return true;
			}

			string ext = fileName.Substring(fileName.LastIndexOf(".") + 1);

			foreach (string validType in validTypes)
			{
				if (ext.ToLower().Equals(validType.ToLower()))
				{
					return true;
				}
			}

			return false;
		}

		internal Hashtable SaveFile(HttpPostedFile postedFile, Page page, string dlgUploadFold)
		{
			return SaveFile(postedFile, page, dlgUploadFold, null);
		}

		internal Hashtable SaveFile(HttpPostedFile postedFile, Page page, string dlgUploadFold, string[] validTypes)
		{
			string filePath = page.MapPath(dlgUploadFold);
			string fileName = postedFile.FileName.Trim();
			Hashtable retValues = new Hashtable();
			retValues.Add("isSuc", false);

			if (string.IsNullOrEmpty(filePath))
			{
				retValues.Add("msg", NoFolderSpecifiedMessage);
				return retValues;
			}

			if (string.IsNullOrEmpty(fileName))
			{
				retValues.Add("msg", NoFileMessage);
				return retValues;
			}

			if (!IsValidFileType(fileName, validTypes))
			{
				retValues.Add("msg", InvalidFileTypeMessage);
				return retValues;
			}

			try
			{

				if (!Directory.Exists(filePath))
				{
					Directory.CreateDirectory(filePath);
				}
				string name = Path.GetFileName(fileName);
				postedFile.SaveAs(string.Format("{0}\\{1}", filePath, name));

				retValues["isSuc"] = true;
				retValues.Add("url", string.Format("{0}/{1}", page.ResolveClientUrl(dlgUploadFold), name));
				retValues.Add("name", name.Remove(name.LastIndexOf('.')));
				retValues.Add("msg", UploadSuccessMessage);
				return retValues;
			}
			catch { }

			retValues.Add("msg", UploadFailureMessage);
			return retValues;
		}
		#endregion end of ** common methods.

		#region ** template methods
		internal Hashtable LoadTemplateList(Page page, string dlgUploadFold)
		{
			Hashtable tplList = new Hashtable();

			foreach (string fileName in GetFiles(page, dlgUploadFold))
			{
				try
				{
					FileInfo file = new FileInfo(fileName);

					if (!Regex.IsMatch(file.Extension.ToLower(), "\\b(html|htm)$"))
					{
						continue;
					}

					Hashtable template = new Hashtable();
					string fName = file.Name;
					string name = fName.Substring(0, fName.LastIndexOf('.'));
					string src = string.Format("{0}/{1}", page.ResolveClientUrl(dlgUploadFold), fName);

					using (StreamReader sr = file.OpenText())
					{
						string title = sr.ReadLine();
						Regex reg = new Regex("<title>(.*)</title>");
						GroupCollection groups = reg.Match(title).Groups;

						if (groups.Count > 1)
						{
							string value = groups[1].Value;

							if (!string.IsNullOrEmpty(value))
							{
								template.Add("desc", value);
							}
						}
					}

					template.Add("src", src);
					tplList.Add(name, template);
				}
				catch { }
			}

			return tplList;
		}

		internal void SaveTemplate(string name, string desc, string content, Page page, string dlgUploadFold)
		{
			try
			{
				string path = page.MapPath(dlgUploadFold);
				string fileName = string.Format("{0}\\{1}.html", path, name);
				string title = string.Format("<title>{0}</title>", desc);

				using (FileStream fs = File.Open(fileName, FileMode.Create, FileAccess.ReadWrite))
				{
					using (StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.Default))
					{
						sw.WriteLine(title);
						sw.WriteLine(content);
					}
				}
			}
			catch {}
		}

		internal bool DeleteTemplate(string tplName, Page page, string dlgUploadFold)
		{
			try
			{
				string path = page.MapPath(dlgUploadFold);

				foreach (string file in GetFiles(page, dlgUploadFold))
				{
					FileInfo fi = new FileInfo(file);
					string fName = fi.Name;
					string name = fName.Substring(0, fName.LastIndexOf('.'));

					if (name.Equals(tplName, StringComparison.InvariantCultureIgnoreCase)) 
					{
						File.Delete(file);
						return true;
					}
				};
			}
			catch {
                return false;
            }

            return false;
		}
	#endregion end of ** template methods.

		#region ** image dialog methods
		internal Hashtable LoadImageList(Page page, string dlgUploadFold, string[] validTypes)
		{
			Hashtable imageList = new Hashtable();

			foreach (string fileName in GetFiles(page, dlgUploadFold))
			{
				try
				{
					FileInfo file = new FileInfo(fileName);

					if (validTypes.Contains(file.Extension.ToLower().TrimStart(new char[]{'.'})))
					{
						string fName = file.Name;
						string name = fName.Substring(0, fName.LastIndexOf('.'));
						string src = string.Format("{0}/{1}", page.ResolveClientUrl(dlgUploadFold), fName);

						imageList.Add(name, src);
					}
				}
				catch { }
			}

			return imageList;
		}
		#endregion end of ** image dialog methods.

	}
}
