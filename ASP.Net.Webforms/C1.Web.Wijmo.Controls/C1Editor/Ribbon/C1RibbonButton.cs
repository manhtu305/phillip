using System;
using System.ComponentModel;
using C1.Web.Wijmo.Controls.Localization;
using System.Web.UI.WebControls;
using System.Collections;
using System.Web.UI;
using C1.Web.Wijmo.Controls.Base;
using System.Web.UI.HtmlControls;

namespace C1.Web.Wijmo.Controls.C1Editor
{

	/// <summary>
	/// Represents the C1EditorButton to specify the button information of the ribbon for C1Editor.
	/// </summary>
	public class C1RibbonButton : UIElement
	{

		#region ** fields
		private Hashtable _props = null; 
		#endregion end of ** fields.

		#region ** constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="C1RibbonButton"/> class.
		/// </summary>
		public C1RibbonButton()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="C1RibbonButton"/> class.
		/// </summary>
		/// <param name="tag">
		/// The tag key of the ribbon button.
		/// </param>
		public C1RibbonButton(HtmlTextWriterTag tag)
			: base(tag)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		public C1RibbonButton(string name, string tooltip, string iconClass)
			: this(name, string.Empty, tooltip, iconClass)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		public C1RibbonButton(string name, string text, 
			string tooltip, string iconClass)
			: this(name, text, tooltip, string.Empty, iconClass)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		public C1RibbonButton(string name, string text, string tooltip,
			string cssClass, string iconClass)
			: this(name, text, tooltip, cssClass,
				iconClass, TextImageRelation.ImageBeforeText)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		/// <param name="textImageRelation">
		/// The textImageRelation of the ribbon button.
		/// </param>
		public C1RibbonButton(string name, string text, string tooltip,
			string cssClass, string iconClass, TextImageRelation textImageRelation)
			: this(name, text, tooltip, string.Empty, cssClass,
				iconClass, textImageRelation)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="value">
		/// The value of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		/// <param name="textImageRelation">
		/// The textImageRelation of the ribbon button.
		/// </param>
		public C1RibbonButton(string name, string text, string tooltip, string value,
			string cssClass, string iconClass, TextImageRelation textImageRelation)
			: this(name, text, tooltip, value, cssClass,
				iconClass, textImageRelation, HtmlTextWriterTag.Button)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="value">
		/// The value of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		/// <param name="textImageRelation">
		/// The textImageRelation of the ribbon button.
		/// </param>
		/// <param name="tag">
		/// The tag key of the ribbon button.
		/// </param>
		public C1RibbonButton(string name, string text, string tooltip, 
			string value, string cssClass, string iconClass, 
			TextImageRelation textImageRelation, HtmlTextWriterTag tag)
			: base(tag)
		{
			this.Name = name;
			this.Text = text;
			this.ToolTip = tooltip;
			this.Value = value;
			this.CssClass = cssClass;
			this.IconClass = iconClass;
			this.TextImageRelation = textImageRelation;
		}
		#endregion end of ** constructors.

		#region ** properties
		#region ** private properties
		protected Hashtable Props
		{
			get
			{
				if (_props == null)
				{
					_props = new Hashtable();
				}

				return _props;
			}
		}
		#endregion end of ** private properties.

		#region ** public properties
		/// <summary>
		/// A value indicates the name of the ribbon button 
        /// which is used to identify the button.
		/// </summary>
		[NotifyParentProperty(true)]
		public string Name
		{
			get
			{
				return this.GetPropertyValue<string>("Name", "");
			}
			set
			{
				this.SetPropertyValue<string>("Name", value);
			}
		}

		/// <summary>
		/// A value indicates the text of the ribbon button.
		/// </summary>
		[NotifyParentProperty(true)]
		public string Text
		{
			get
			{
				return this.GetPropertyValue<string>("Text", "");
			}
			set
			{
				this.SetPropertyValue<string>("Text", value);
			}
		}

		/// <summary>
		/// A value indicates the value of the ribbon button.
		/// </summary>
		[NotifyParentProperty(true)]
		public string Value
		{
			get
			{
				return this.GetPropertyValue<string>("Value", "");
			}
			set
			{
				this.SetPropertyValue<string>("Value", value);
			}
		}

		/// <summary>
		/// A value indicates the icon class for the ribbon button.
		/// </summary>
		[NotifyParentProperty(true)]
		public string IconClass
		{
			get
			{
				return this.GetPropertyValue<string>("IconClass", "");
			}
			set
			{
				this.SetPropertyValue<string>("IconClass", value);
			}
		}

		/// <summary>
		/// A value indicates the flow side for the image.
		/// </summary>
		[NotifyParentProperty(true)]
		public TextImageRelation TextImageRelation
		{
			get
			{
				return this.GetPropertyValue<TextImageRelation>("TextImageRelation", TextImageRelation.ImageAboveText);
			}
			set
			{
				this.SetPropertyValue<TextImageRelation>("TextImageRelation", value);
			}
		}
		#endregion end of ** public properties.

		#region ** protected properties

		/// <summary>
		/// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
		/// server control. This property is used primarily by control developers.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Button;
			}
		}
		#endregion end of ** protected properties.
		#endregion end of ** properties.

		#region ** methods
		/// <summary>
		/// Gets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="nullValue">The null value.</param>
		/// <returns></returns>
		protected V GetPropertyValue<V>(string propertyName, V nullValue)
		{
			if (this.Props[propertyName] == null)
			{
				return nullValue;
			}

			return (V)this.Props[propertyName];
		}

		/// <summary>
		/// Sets the property value by property name.
		/// </summary>
		/// <typeparam name="V"></typeparam>
		/// <param name="propertyName">Name of the property.</param>
		/// <param name="value">The value.</param>
		protected void SetPropertyValue<V>(string propertyName, V value)
		{
			this.Props[propertyName] = value;
		}

		/// <summary>
		/// Gets the compound css class.
		/// </summary>
		/// <returns>Returns the css string.</returns>
		protected virtual string GetCompoundCss()
		{
			string css = this.IsDesignMode ? "ui-button ui-widget ui-state-default" : "";

			if (this.IsDesignMode)
			{
				if (string.IsNullOrEmpty(this.Text))
				{
					css = string.Format("{0} {1}", css, "ui-button-icon-only");
				}
				else if (string.IsNullOrEmpty(this.IconClass))
				{
					css = string.Format("{0} {1}", css, "ui-button-text-only");
				}
				else
				{
					if (this.TextImageRelation == TextImageRelation.ImageAboveText ||
						this.TextImageRelation == TextImageRelation.ImageBeforeText)
					{
						css = string.Format("{0} {1}", css, "ui-button-text-icon-primary");
					}
					else
					{
						css = string.Format("{0} {1}", css, "ui-button-text-icon-secondary");
					}
				}
			}
			else
			{
				if (string.IsNullOrEmpty(this.Text) &&
					!string.IsNullOrEmpty(this.IconClass))
				{
					return string.Format("{0} {1}", this.IconClass, css).Trim();
				}
			}

			return css;
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to
		/// the specified <see cref="HtmlTextWriterTag"/>.
		/// </summary>
		/// <param name="writer">A <see cref="HtmlTextWriter"/>that represents
		/// the output stream to render HTML content on the client.
		/// </param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			string oldCss = this.CssClass;

			this.CssClass = string.Format("{0} {1}", this.GetCompoundCss(), oldCss).Trim();
			
			base.AddAttributesToRender(writer);

			this.CssClass = oldCss;

			if (!string.IsNullOrEmpty(this.Name))
			{
				writer.AddAttribute("name", this.Name);
			}
		}

		/// <summary>
        /// Renders <see cref="C1TreeView"/> elements. Renders hidden element used be the control for serialization. 
		/// </summary>
		/// <param name="writer">>A <see cref="HtmlTextWriter"/>that represents
		/// the output stream to render HTML content on the client.
		/// </param>
		protected override void Render(HtmlTextWriter writer)
		{
			this.EnsureChildControls();

			base.Render(writer);
		}

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that 
		/// use composition-based implementation to create any child controls 
		/// they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			this.Controls.Clear();

			if ((!string.IsNullOrEmpty(this.IconClass) &&
				!string.IsNullOrEmpty(this.Text)))
			{
				if (this.TextImageRelation == TextImageRelation.ImageAboveText ||
					this.TextImageRelation == TextImageRelation.ImageBeforeText)
				{
					this.Controls.Add(this.CreateIconSpan());
					this.Controls.Add(this.CreateTextSpan());
				}
				else
				{
					this.Controls.Add(this.CreateTextSpan());
					this.Controls.Add(this.CreateIconSpan());
				}
			}
			else if (!string.IsNullOrEmpty(this.Text))
			{
				LiteralControl text = new LiteralControl(this.Text);
				this.Controls.Add(text);
			}
			else if (this.IsDesignMode)
			{
				HtmlGenericControl txt = this.CreateTextSpan(this.Name);
				txt.Attributes["class"] = string.Format("{0} {1} ui-icon-designtime", txt.Attributes["class"], this.IconClass);
				this.Controls.Add(txt);
			}

			base.CreateChildControls();
		}

		/// <summary>
		/// Create the text span of ribbon button,
		/// </summary>
		/// <returns>
		/// The text span control.
		/// </returns>
		protected virtual HtmlGenericControl CreateTextSpan()
		{
			return this.CreateTextSpan(this.Text);
		}
		 
		/// <summary>
		/// Create the text span that shown in ribbon button.
		/// </summary>
		/// <param name="text">The text shown in text span.</param>
		/// <returns>The text span control.</returns>
		protected virtual HtmlGenericControl CreateTextSpan(string text)
		{
			HtmlGenericControl span = new HtmlGenericControl();

			if (!this.IsDesignMode && this.TextImageRelation == TextImageRelation.TextAboveImage)
			{
				span = new HtmlGenericControl("div");
			}

			if (this.IsDesignMode)
			{
				span.Attributes["class"] = "ui-button-text";
			}

			span.InnerText = text;

			return span;
		}

		/// <summary>
		/// Create the icon span that displayed in ribbon button.
		/// </summary>
		/// <returns>
		/// The icon span control.
		/// </returns>
		protected virtual HtmlGenericControl CreateIconSpan()
		{
			HtmlGenericControl span = new HtmlGenericControl("span");
			string css = this.IconClass;

			if (!this.IsDesignMode)
			{
				if (this.TextImageRelation == TextImageRelation.ImageAboveText)
				{
					span = new HtmlGenericControl("div");
				}
			}
			else
			{
				if (this.TextImageRelation == TextImageRelation.ImageAboveText ||
					this.TextImageRelation == TextImageRelation.ImageBeforeText)
				{
					css = string.Format("{0} {1}", css, "ui-button-icon-primary ui-icon");
				}
				else
				{
					css = string.Format("{0} {1}", css, "ui-button-icon-secondary ui-icon");
				}
			}

			span.Attributes["class"] = css;

			return span;
		}

		#endregion end of ** methods.
	}
}
