using System;
using System.ComponentModel;
using C1.Web.Wijmo.Controls.Localization;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace C1.Web.Wijmo.Controls.C1Editor
{

	/// <summary>
	/// Represents the C1EditorButton to specify the button information of the ribbon for C1Editor.
	/// </summary>
	public class C1RibbonToggleButton : C1RibbonButton
	{

		#region ** constructors
		public C1RibbonToggleButton()
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonToggleButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		public C1RibbonToggleButton(string name, string tooltip, string iconClass)
			: this(name, string.Empty, tooltip, iconClass)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonToggleButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		public C1RibbonToggleButton(string name, string text, 
			string tooltip, string iconClass)
			: this(name, text, tooltip, string.Empty, iconClass)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonToggleButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		public C1RibbonToggleButton(string name, string text, string tooltip,
			string cssClass, string iconClass)
			: this(name, text, tooltip, cssClass,
				iconClass, TextImageRelation.ImageBeforeText)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonToggleButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		/// <param name="textImageRelation">
		/// The textImageRelation of the ribbon button.
		/// </param>
		public C1RibbonToggleButton(string name, string text, string tooltip,
			string cssClass, string iconClass, TextImageRelation textImageRelation)
			: this(name, text, tooltip, string.Empty, cssClass,
				iconClass, textImageRelation)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonToggleButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="value">
		/// The value of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		/// <param name="textImageRelation">
		/// The textImageRelation of the ribbon button.
		/// </param>
		public C1RibbonToggleButton(string name, string text, string tooltip, string value,
			string cssClass, string iconClass, TextImageRelation textImageRelation)
			: this(name, text, tooltip, value, cssClass,
				iconClass, textImageRelation, false)
		{
		}

		/// <summary>
		/// The constructor for the <seealso cref="C1RibbonToggleButton"/>
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="value">
		/// The value of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		/// <param name="textImageRelation">
		/// The textImageRelation of the ribbon button.
		/// </param>
		/// <param name="isChecked">
		/// The checked state of the ribbon button.
		/// </param>
		public C1RibbonToggleButton(string name, string text, string tooltip,
			string value, string cssClass, string iconClass,
			TextImageRelation textImageRelation, bool isChecked) :
			base(name, text, tooltip, value, cssClass, 
				iconClass, textImageRelation, HtmlTextWriterTag.Span)
		{
			this.Checked = isChecked;
		}
		#endregion end of ** constructors.

		#region ** properties
		#region ** public properties
		/// <summary>
		/// A value indicates the state of the ribbon button.
		/// </summary>
		[DefaultValue(false)]
		[NotifyParentProperty(true)]
		public bool Checked
		{
			get
			{
				return this.GetPropertyValue<bool>("Checked", false);
			}
			set
			{
				this.SetPropertyValue<bool>("Checked", value);
			}
		}
		#endregion end of ** public properties.

		#region ** override properties

		/// <summary>
		/// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
		/// server control. This property is used primarily by control developers.
		/// </summary>
		protected override System.Web.UI.HtmlTextWriterTag TagKey
		{
			get
			{
				if (this.IsDesignMode)
				{
					return base.TagKey;
				}

				return System.Web.UI.HtmlTextWriterTag.Span;
			}
		}
		#endregion end of ** override properties. 
		#endregion end of ** properties.

		/// <summary>
		/// Create checkbox input HTML control for C1RibbonToggleButton.
		/// </summary>
		/// <returns></returns>
		protected virtual HtmlGenericControl CreateInput()
		{
			HtmlGenericControl input = new HtmlGenericControl("input");
			input.Attributes["type"] = "checkbox";
			input.Attributes["id"] = this.ClientID;

			if (this.Checked)
			{
				input.Attributes["checked"] = "checked";
			}

			if (this.IsDesignMode)
			{
				input.Attributes["class"] = "ui-helper-hidden-accessible";
			}

			return input;
		}

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that 
		/// use composition-based implementation to create any child controls 
		/// they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			if (this.IsDesignMode)
			{
				base.CreateChildControls();
				return;
			}

			this.Controls.Clear();

			HtmlGenericControl input = this.CreateInput();
			this.Controls.Add(input);

			HtmlGenericControl label = new HtmlGenericControl("label");
			label.Attributes["for"] = this.ClientID;

			if (!string.IsNullOrEmpty(this.Name))
			{
				label.Attributes["name"] = this.Name;
			}

			if (!string.IsNullOrEmpty(this.ToolTip))
			{
				label.Attributes["title"] = this.ToolTip;
			}

			this.Controls.Add(label);

			if (!string.IsNullOrEmpty(this.Text))
			{
				if (!string.IsNullOrEmpty(this.IconClass))
				{
					if (this.TextImageRelation == TextImageRelation.ImageAboveText ||
						this.TextImageRelation == TextImageRelation.ImageBeforeText)
					{
						label.Controls.Add(this.CreateIconSpan());
						label.Controls.Add(this.CreateTextSpan());
					}
					else
					{
						label.Controls.Add(this.CreateTextSpan());
						label.Controls.Add(this.CreateIconSpan());
					}
				}
				else
				{
					label.InnerText = this.Text;
				}
			}
			else if (!string.IsNullOrEmpty(this.IconClass))
			{
				label.Attributes["class"] = string.Format("{0} {1}", this.GetCompoundCss(), this.CssClass).Trim();
			}
		}

		/// <summary>
		///  Renders the HTML opening tag of the control into the specified writer. This
		///	 method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">
		///  A System.Web.UI.HtmlTextWriter that represents the output stream to render HTML content on the client.
		/// </param>
		public override void RenderBeginTag(HtmlTextWriter writer)
		{
			if (this.IsDesignMode)
			{
				base.RenderBeginTag(writer);
			}
		}

		/// <summary>
		///  Renders the HTML closing tag of the control into the specified writer. This
		///	 method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">
		///  A System.Web.UI.HtmlTextWriter that represents the output stream to render HTML content on the client.
		/// </param>
		public override void RenderEndTag(HtmlTextWriter writer)
		{
			if (this.IsDesignMode)
			{
				base.RenderEndTag(writer);
			}
		}
	}
}
