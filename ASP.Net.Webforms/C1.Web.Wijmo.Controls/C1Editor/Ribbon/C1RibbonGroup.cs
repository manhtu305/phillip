using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1Editor
{

	/// <summary>
	/// Represents the C1RibbonGroup class to save 
	/// all the information for the ribbon group.
	/// </summary>
	public class C1RibbonGroup : Settings
	{

		private C1RibbonButtonCollection _buttons = null;

		/// <summary>
		/// A value indicates the name of the ribbon group 
        /// which is used to identify the group.
		/// </summary>
		[NotifyParentProperty(true)]
		public string Name
		{
			get
			{
				return this.GetPropertyValue<string>("Name", "");
			}
			set
			{
				this.SetPropertyValue<string>("Name", value);
			}
		}

		/// <summary>
		/// A value indicates the head text of the ribbon group.
		/// </summary>
		[NotifyParentProperty(true)]
		public string HeaderText
		{
			get
			{
				return this.GetPropertyValue<string>("HeaderText", "");
			}
			set
			{
				this.SetPropertyValue<string>("HeaderText", value);
			}
		}

		/// <summary>
		/// A value indicates the css class of the ribbon group.
		/// </summary>
		[NotifyParentProperty(true)]
		public string CssClass
		{
			get
			{
				return this.GetPropertyValue<string>("CssClass", "");
			}
			set
			{
				this.SetPropertyValue<string>("CssClass", value);
			}
		}

		/// <summary>
		/// A <seealso cref="C1RibbonButtonCollection"/> instance 
        /// which saves all the button information.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public C1RibbonButtonCollection Buttons
		{
			get 
			{
				if (this._buttons == null)
				{
					this._buttons = new C1RibbonButtonCollection();
				}

				return this._buttons;
			}
		}

		internal string DisplayName
		{
			get;
			set;
		}
	}
}
