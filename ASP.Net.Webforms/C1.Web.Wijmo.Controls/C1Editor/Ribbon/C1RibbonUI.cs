﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.Design;
using C1.Web.Wijmo.Controls.Localization;
using System.Collections;

namespace C1.Web.Wijmo.Controls.C1Editor
{

	/// <summary>
	/// Represents the RibbonUI class to construct the ribbon for C1Editor.
	/// </summary>
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class C1RibbonUI
	{

		#region ** fields
		private readonly C1Editor _editor;
		private readonly bool _isDesignMode;
		private C1RibbonTabPageCollection _tabPages;
		private C1RibbonDropDownListButton _btnFontSizes, _btnFontNames;

		private HtmlGenericControl bodyPanel = null;

		private const string CONST_WIJMO_CSS_PREFIX = "wijmo-";
		private const string CONST_WIJRIBBON_CSS = CONST_WIJMO_CSS_PREFIX + "wijribbon";
		private const string CONST_WIJBIGBUTTON_CSS = CONST_WIJRIBBON_CSS + "-bigbutton";
		private const string CONST_WIJEDITOR_CSS = CONST_WIJMO_CSS_PREFIX + "wijeditor";
		private const string CONST_WIJCONTAINER_CSS = CONST_WIJEDITOR_CSS + "-container";
		private const string CONST_WIJFOOTER_CSS = CONST_WIJEDITOR_CSS + "-footer";
		private const string CONST_WIJPATHSELECTOR_CSS = CONST_WIJEDITOR_CSS + "-pathselector";
		private const string CONST_WIJMODES_CSS = CONST_WIJRIBBON_CSS + "-modes";
		private const string CONST_WIJPATHLABEL_CSS = CONST_WIJEDITOR_CSS + "-label";
		private const string CONST_WIDGET_CSS = "ui-widget";
		private const string CONST_WIDGET_CONTENT_CSS = "ui-widget-content";
		private const string CONST_STATE_DEFAULT_CSS = "ui-state-default";
		private const string CONST_EDITOR_FOOTER_CSS = "wijeditor-footer";
		private readonly string[] defaultFontSizes = new string[] { "VerySmall", "Smaller", "Small", 
					"Medium", "Large", "Larger", "VeryLarge"};
		private readonly string[] defaultFontNames = new string[] { "Arial", "Courier New", "Garamond",
						"Tahoma", "Times New Roman", "Verdana", "Wingdings"};

		#region ** const css classes
		private const string CONST_SAVE_CSS = CONST_WIJRIBBON_CSS + "-save";
		private const string CONST_SAVE16_CSS = CONST_WIJRIBBON_CSS + "-save16";
		private const string CONST_UNDO_CSS = CONST_WIJRIBBON_CSS + "-undo";
		private const string CONST_REDO_CSS = CONST_WIJRIBBON_CSS + "-redo";
		private const string CONST_PREVIEW_CSS = CONST_WIJRIBBON_CSS + "-preview";
		private const string CONST_CLEANUP_CSS = CONST_WIJRIBBON_CSS + "-cleanup";
		private const string CONST_CUT_CSS = CONST_WIJRIBBON_CSS + "-cut";
		private const string CONST_COPY_CSS = CONST_WIJRIBBON_CSS + "-copy";
		private const string CONST_PASTE_CSS = CONST_WIJRIBBON_CSS + "-paste";
		private const string CONST_SELECTALL_CSS = CONST_WIJRIBBON_CSS + "-selectall";
		private const string CONST_BACKCOLOR_CSS = CONST_WIJRIBBON_CSS + "-bgcolor";
		private const string CONST_FONTCOLOR_CSS = CONST_WIJRIBBON_CSS + "-color";
		private const string CONST_BOLD_CSS = CONST_WIJRIBBON_CSS + "-bold";
		private const string CONST_ITALIC_CSS = CONST_WIJRIBBON_CSS + "-italic";
		private const string CONST_UNDERLINE_CSS = CONST_WIJRIBBON_CSS + "-underline";
		private const string CONST_STRIKE_CSS = CONST_WIJRIBBON_CSS + "-strike";
		private const string CONST_SUBSCRIPT_CSS = CONST_WIJRIBBON_CSS + "-sub";
		private const string CONST_SUPERSCRIPT_CSS = CONST_WIJRIBBON_CSS + "-sup";
		private const string CONST_TEMPLATE_CSS = CONST_WIJRIBBON_CSS + "-template";
		private const string CONST_REMOVEFORMAT_CSS = CONST_WIJRIBBON_CSS + "-removeformat";
		private const string CONST_JUSTIFYLEFT_CSS = CONST_WIJRIBBON_CSS + "-justifyleft";
		private const string CONST_JUSTIFYCENTER_CSS = CONST_WIJRIBBON_CSS + "-justifycenter";
		private const string CONST_JUSTIFYRIGHT_CSS = CONST_WIJRIBBON_CSS + "-justifyright";
		private const string CONST_JUSTIFYFULL_CSS = CONST_WIJRIBBON_CSS + "-justifyfull";
		private const string CONST_BORDERS_CSS = CONST_WIJRIBBON_CSS + "-borders";
		private const string CONST_NUMLIST_CSS = CONST_WIJRIBBON_CSS + "-orderlist";
		private const string CONST_BULLIST_CSS = CONST_WIJRIBBON_CSS + "-unorderlist";
		private const string CONST_OUTDENT_CSS = CONST_WIJRIBBON_CSS + "-outdent";
		private const string CONST_INDENT_CSS = CONST_WIJRIBBON_CSS + "-indent";
		private const string CONST_SPELLING_CSS = CONST_WIJRIBBON_CSS + "-spelling";
		private const string CONST_INSPECT_CSS = CONST_WIJRIBBON_CSS + "-inspect";
		private const string CONST_FIND_CSS = CONST_WIJRIBBON_CSS + "-find";
		private const string CONST_TABLE_CSS = CONST_WIJRIBBON_CSS + "-table";
		private const string CONST_INSERT_CSS = CONST_WIJRIBBON_CSS + "-inserttable";
		private const string CONST_EDIT_CSS = CONST_WIJRIBBON_CSS + "-edittable";
		private const string CONST_INSERTCOL_CSS = CONST_WIJRIBBON_CSS + "-insertcol";
		private const string CONST_INSERTROW_CSS = CONST_WIJRIBBON_CSS + "-insertrow";
		private const string CONST_INSERTCELL_CSS = CONST_WIJRIBBON_CSS + "-insertcell";
		private const string CONST_SPLITCELL_CSS = CONST_WIJRIBBON_CSS + "-splitcell";
		private const string CONST_MERGECELL_CSS = CONST_WIJRIBBON_CSS + "-mergecell";
		private const string CONST_DELETEROW_CSS = CONST_WIJRIBBON_CSS + "-deleterow";
		private const string CONST_DELETECOL_CSS = CONST_WIJRIBBON_CSS + "-deletecol";
		private const string CONST_DELETECELL_CSS = CONST_WIJRIBBON_CSS + "-deletecell";
		private const string CONST_INSERTBREAK_CSS = CONST_WIJRIBBON_CSS + "-insertbreak";
		private const string CONST_INSERTPARAGRAPH_CSS = CONST_WIJRIBBON_CSS + "-insertparagraph";
		private const string CONST_INSERTPRINTPAGEBREAK_CSS = CONST_WIJRIBBON_CSS + "-insertprintpagebreak";
		private const string CONST_INSERTHLINE_CSS = CONST_WIJRIBBON_CSS + "-inserthline";
		private const string CONST_FORM_CSS = CONST_WIJRIBBON_CSS + "-form";
		private const string CONST_TEXTAREA_CSS = CONST_WIJRIBBON_CSS + "-textarea";
		private const string CONST_TEXTBOX_CSS = CONST_WIJRIBBON_CSS + "-textbox";
		private const string CONST_PASSWORD_CSS = CONST_WIJRIBBON_CSS + "-password";
		private const string CONST_HIDDENFIELD_CSS = CONST_WIJRIBBON_CSS + "-hiddenfield";
		private const string CONST_IMAGEBUTTON_CSS = CONST_WIJRIBBON_CSS + "-imagebutton";
		private const string CONST_BUTTON_CSS = CONST_WIJRIBBON_CSS + "-button";
		private const string CONST_LISTBOX_CSS = CONST_WIJRIBBON_CSS + "-listbox";
		private const string CONST_DROPDOWNLIST_CSS = CONST_WIJRIBBON_CSS + "-dropdownlist";
		private const string CONST_RADIO_CSS = CONST_WIJRIBBON_CSS + "-radio";
		private const string CONST_CHECKBOX_CSS = CONST_WIJRIBBON_CSS + "-checkbox";
		private const string CONST_LINK_CSS = CONST_WIJRIBBON_CSS + "-link";
		private const string CONST_LINK16_CSS = CONST_WIJRIBBON_CSS + "-link16";
		private const string CONST_IMAGEBROWSER_CSS = CONST_WIJRIBBON_CSS + "-imagebrowser";
		private const string CONST_MEDIA_CSS = CONST_WIJRIBBON_CSS + "-media";
		private const string CONST_SPECIALCHAR_CSS = CONST_WIJRIBBON_CSS + "-specialchar";
		private const string CONST_DATETIME_CSS = CONST_WIJRIBBON_CSS + "-datetime";
		private const string CONST_DESIGNVIEW_CSS = CONST_WIJRIBBON_CSS + "-designview";
		private const string CONST_SOURCEVIEW_CSS = CONST_WIJRIBBON_CSS + "-sourceview";
		private const string CONST_SPLITVIEW_CSS = CONST_WIJRIBBON_CSS + "-splitview";
		private const string CONST_WORDWRAP_CSS = CONST_WIJRIBBON_CSS + "-wordwrap";
		private const string CONST_SEPARATOR_CSS = CONST_WIJRIBBON_CSS + "-separator";
		private const string CONST_FULLSCREEN_CSS = CONST_WIJRIBBON_CSS + "-fullscreen";


		private const string CONST_CODE_CSS = CONST_WIJRIBBON_CSS + "-insertcode";
		private const string CONST_BLOCKQUOTE_CSS = CONST_WIJRIBBON_CSS + "-blockquote";

		private const string CONST_REVIEWWITHSPELLING_CSS = CONST_WIJRIBBON_CSS + "-reviewwithspelling";
		#endregion end of ** const css classes.

		#region ** format tab names
		private const string CONST_FORMATTAB_NAME = "Format";

		#region ** action group names
		private const string CONST_ACTION_NAME = "action";
		private const string CONST_SAVE_NAME = "save";
		private const string CONST_UNDO_NAME = "undo";
		private const string CONST_REDO_NAME = "redo";
		private const string CONST_PREVIEW_NAME = "preview";
		private const string CONST_CLEANUP_NAME = "cleanup";
		private const string CONST_CUT_NAME = "cut";
		private const string CONST_COPY_NAME = "copy";
		private const string CONST_PASTE_NAME = "paste";
		private const string CONST_SELECTALL_NAME = "selectall";
		#endregion end of ** action group names.

		#region ** font group names
		private const string CONST_FONT_NAME = "font";
		private const string CONST_FONTNAME_NAME = "fontname";

		private const string CONST_FONTSIZE_NAME = "fontsize";
		private const string CONST_VERYSMALL_NAME = "xx-small";
		private const string CONST_SMALLER_NAME = "x-small";
		private const string CONST_SMALL_NAME = "small";
		private const string CONST_MEDIUM_NAME = "medium";
		private const string CONST_LARGE_NAME = "large";
		private const string CONST_LARGER_NAME = "x-large";
		private const string CONST_VERYLARGE_NAME = "xx-large";

		private const string CONST_BACKCOLOR_NAME = "backcolor";
		private const string CONST_FONTCOLOR_NAME = "fontcolor";
		private const string CONST_BOLD_NAME = "bold";
		private const string CONST_ITALIC_NAME = "italic";
		private const string CONST_UNDERLINE_NAME = "underline";
		private const string CONST_STRIKE_NAME = "strikethrough";
		private const string CONST_SUBSCRIPT_NAME = "subscript";
		private const string CONST_SUPERSCRIPT_NAME = "superscript";

		private const string CONST_TEMPLATE_NAME = "template";
		private const string CONST_REMOVEFORMAT_NAME = "removeformat";
		#endregion end of ** font group names.

		#region ** para group names
		private const string CONST_PARA_NAME = "paragraph";
		private const string CONST_JUSTIFYLEFT_NAME = "justifyleft";
		private const string CONST_JUSTIFYCENTER_NAME = "justifycenter";
		private const string CONST_JUSTIFYRIGHT_NAME = "justifyright";
		private const string CONST_JUSTIFYFULL_NAME = "justifyfull";

		private const string CONST_BORDERS_NAME = "borders";

		private const string CONST_NUMLIST_NAME = "insertorderedlist";
		private const string CONST_BULLIST_NAME = "insertunorderedlist";

		private const string CONST_OUTDENT_NAME = "outdent";
		private const string CONST_INDENT_NAME = "indent";
		#endregion end of ** para group names.

		#region ** review group names
		private const string CONST_REVIEW_NAME = "review";
		private const string CONST_SPELLING_NAME = "spelling";
		private const string CONST_INSPECT_NAME = "inspect";
		private const string CONST_FIND_NAME = "find";
		#endregion end of ** review group names.
		#endregion end of ** format tab names.

		#region ** insert tab names
		private const string CONST_INSERTTAB_NAME = "insert";

		#region ** tables group names
		private const string CONST_TABLES_NAME = "tables";
		private const string CONST_TABLE_NAME = "tablebutton";
		private const string CONST_INSERT_NAME = "inserttable";
		private const string CONST_EDIT_NAME = "edittable";

		private const string CONST_INSERTCOL_NAME = "insertcol";
		private const string CONST_INSERTROW_NAME = "insertrow";
		private const string CONST_INSERTCELL_NAME = "insertcell";

		private const string CONST_SPLITCELL_NAME = "splitcell";
		private const string CONST_MERGECELL_NAME = "mergecell";

		private const string CONST_DELETECOL_NAME = "deletecol";
		private const string CONST_DELETEROW_NAME = "deleterow";
		private const string CONST_DELETECELL_NAME = "deletecell";
		#endregion end of ** tables group names.

		#region ** breaks group names
		private const string CONST_BREAKS_NAME = "breaks";
		private const string CONST_INSERTBREAK_NAME = "insertbreak";
		private const string CONST_INSERTPARA_NAME = "insertparagraph";
		private const string CONST_INSERTPRINTPAGEBREAK_NAME = "insertprintpagebreak";
		private const string CONST_INSERTHRULE_NAME = "inserthorizontalrule";
		#endregion end of ** breaks group names.

		#region ** forms group names
		private const string CONST_FORMS_NAME = "forms";

		private const string CONST_FORM_NAME = "form";

		private const string CONST_TEXTAREA_NAME = "textarea";
		private const string CONST_TEXTBOX_NAME = "textbox";
		private const string CONST_PASSWORD_NAME = "passwordfield";
		private const string CONST_HIDDENFIELD_NAME = "hiddenfield";

		private const string CONST_IMAGEBUTTON_NAME = "imagebutton";
		private const string CONST_BUTTON_NAME = "button";

		private const string CONST_LISTBOX_NAME = "list";
		private const string CONST_DROPDOWNLIST_NAME = "dropdownlist";
		private const string CONST_RADIO_NAME = "radio";
		private const string CONST_CHECKBOX_NAME = "checkbox";
		#endregion end of ** forms group names.

		#region ** special group names
		private const string CONST_SPECIAL_NAME = "special";

		private const string CONST_LINK_NAME = "link";

		private const string CONST_IMAGEBROWSER_NAME = "imagebrowser";
		private const string CONST_MEDIA_NAME = "media";
		private const string CONST_SPECIALCHAR_NAME = "specialchar";
		private const string CONST_DATETIME_NAME = "datetime";
		#endregion end of ** special group names.
		#endregion end of ** insert tab names.

		#region ** simple command names
		private const string CONST_CODE_NAME = "code";
		private const string CONST_BLOCKQUOTE_NAME = "blockquote";
		#endregion end of ** simple command names.

		#region ** footer names
		private const string CONST_DESIGNVIEW_NAME = "designview";
		private const string CONST_SOURCEVIEW_NAME = "sourceview";
		private const string CONST_SPLITVIEW_NAME = "splitview";
		private const string CONST_WORDWRAP_NAME = "wordwrap";
		private const string CONST_SEPARATOR_NAME = "separator";
		private const string CONST_FULLSCREEN_NAME = "fullscreen"; 
		#endregion end of ** footer names.
		#endregion end of ** fields.

		#region ** constructors
		/// <summary>
		/// RibbonUI constructor.
		/// </summary>
		/// <param name="editor">Parent <see cref="C1Editor"/> for this instance.</param>
		/// <param name="isDesign">A value indicates whether the <see cref="C1Editor"/> is rendered for design mode.</param>
		internal C1RibbonUI(C1Editor editor, bool isDesign)
		{
			this._editor = editor;
			this._isDesignMode = isDesign;
			this.CreateDefaultTabPages();
		} 
		#endregion end of ** constructors.

		#region ** properties
		#region ** public properties
		/// <summary>
        /// Gets or sets a value which stores all the tab page information for the ribbon editor.
		/// </summary>
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		public C1RibbonTabPageCollection TabPages
		{
			get
			{
				if (this._tabPages == null)
				{
					this._tabPages = new C1RibbonTabPageCollection();
				}

				return this._tabPages;
			}
		}
		#endregion end of ** public properties.

		#region ** private properties
		private C1Editor Owner
		{
			get
			{
				return this._editor;
			}
		}

		private Mode Mode
		{
			get
			{
				if (this.Owner != null)
				{
					return this.Owner.Mode;
				}

				return Mode.Ribbon;
			}
		}

		private string[] SimpleModeCommands
		{
			get
			{
				if (this.Owner != null)
				{
					return this.Owner.SimpleModeCommands;
				}

				return new string[] {"Bold", "Italic", "Link", "BlockQuote", "StrikeThrough",
						"InsertDate", "InsertImage", "NumberedList", "BulletedList", "Code"};
			}
		}

		private List<FontOption> FontNames
		{
			get
			{
				if (this.Owner != null && this.Owner.CustomFontNames.Count > 0)
				{
					return this.Owner.CustomFontNames;
				}
				else 
				{
					return GetFonts(defaultFontNames);
				}
			}
		}

		private List<FontOption> FontSizes 
		{
			get 
			{
				if (this.Owner != null && this.Owner.CustomFontSizes.Count > 0)
				{
					return this.Owner.CustomFontSizes;
				}
				else 
				{
					return GetFonts(defaultFontSizes);
				}
			}
		}

		private string OwnerID
		{
			get
			{
				if (this.Owner != null)
				{
					return this.Owner.ClientID;
				}

				return string.Empty;
			}
		}

		private bool IsIE6
		{
			get
			{
				return this._editor.Page.Request.Browser.Browser == "IE"
					&& this._editor.Page.Request.Browser.MajorVersion == 6;
			}
		}

		private bool UseGifImage
		{
			get
			{
				return this._isDesignMode || this.IsIE6;
			}
		}

		private int BodyPanelHeight
		{
			get
			{
				if (this.Mode == Controls.C1Editor.Mode.Ribbon)
				{
					return Convert.ToInt32(this.Owner.Height.Value) - 100 - 60;
				}
				
				return Convert.ToInt32(this.Owner.Height.Value) - 24 - 60;
			}
		}
		#endregion end of ** private properties.
		#endregion end of ** properties.

		#region ** methods
		#region ** private methods
		private List<FontOption> GetFonts(string[] defaultFonts)
		{
			List<FontOption> fonts = new List<FontOption>();
			foreach (string font in defaultFonts)
			{
				fonts.Add(new FontOption(GetFontSizeCommand(font), font));
			}
			return fonts;
		}

		#region ** create default tab pages
		/// <summary>
		/// Create default tab pages.
		/// </summary>
		private void CreateDefaultTabPages()
		{
			#region ** format tab
			C1RibbonTabPage formatTab = new C1RibbonTabPage();
			formatTab.Text = C1Localizer.GetString("C1Editor.FormatTab", _editor.Culture);
			formatTab.Name = CONST_FORMATTAB_NAME;
			formatTab.Groups.Clear();
			this.TabPages.Add(formatTab);

			//Action group
			formatTab.Groups.Add(this.CreateActionGroup());

			//Font group
			formatTab.Groups.Add(this.CreateFontGroup());

			//Para group
			formatTab.Groups.Add(this.CreateParagraphGroup());

			//Review group
			formatTab.Groups.Add(this.CreateReviewGroup());
			#endregion end of ** format tab.

			#region ** insert tab
			C1RibbonTabPage insertTab = new C1RibbonTabPage();
			insertTab.Text = C1Localizer.GetString("C1Editor.InsertPage", _editor.Culture);
			insertTab.Name = CONST_INSERTTAB_NAME;
			insertTab.Groups.Clear();
			this.TabPages.Add(insertTab);

			//tables group
			insertTab.Groups.Add(this.CreateTablesGroup());

			//breaks group
			insertTab.Groups.Add(this.CreateBreaksGroup());

			//forms group
			insertTab.Groups.Add(this.CreateFormsGroup());

			//special group
			insertTab.Groups.Add(this.CreateSpecialGroup());
			#endregion end of ** insert tab.
		}
		#endregion end of ** create default tab pages.

		#region ** all buttons
		[SmartAssembly.Attributes.DoNotObfuscate]
		/// <summary>
		/// Create save button.
		/// </summary>
		/// <returns></returns>
		private C1RibbonButton CreateSaveButton()
		{
			string saveText = C1Localizer.GetString("C1Editor.FormatPage.ActionsGroup.Save", _editor.Culture);

			if (this.Mode == Controls.C1Editor.Mode.Ribbon)
			{
				return new C1RibbonButton(CONST_SAVE_NAME, saveText, saveText,
					CONST_WIJBIGBUTTON_CSS, CONST_SAVE_CSS, TextImageRelation.ImageAboveText);
			}

			return new C1RibbonButton(CONST_SAVE_NAME, saveText, CONST_SAVE16_CSS);
		}

		/// <summary>
		/// Create undo button.
		/// </summary>
		/// <returns></returns>
		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateUndoButton()
		{
			string undoText = C1Localizer.GetString("C1Editor.FormatPage.ActionsGroup.Undo", _editor.Culture);
			C1RibbonButton btnUndo = new C1RibbonButton(CONST_UNDO_NAME, undoText, CONST_UNDO_CSS);

			return btnUndo;
		}

		/// <summary>
		/// Create redo button.
		/// </summary>
		/// <returns></returns>
		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateRedoButton()
		{
			string redoText = C1Localizer.GetString("C1Editor.FormatPage.ActionsGroup.Redo", _editor.Culture);
			C1RibbonButton btnRedo = new C1RibbonButton(CONST_REDO_NAME, redoText, CONST_REDO_CSS);
			return btnRedo;
		}

		/// <summary>
		/// Create preview button.
		/// </summary>
		/// <returns></returns>
		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreatePreviewButton()
		{
			string previewText = C1Localizer.GetString("C1Editor.FormatPage.ActionsGroup.Preview", _editor.Culture);
			C1RibbonButton btnPreview = new C1RibbonButton(CONST_PREVIEW_NAME, previewText, CONST_PREVIEW_CSS);
			return btnPreview;
		}

		/// <summary>
		/// Create cleanup button.
		/// </summary>
		/// <returns></returns>
		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateCleanupButton()
		{
			string cleanupText = C1Localizer.GetString("C1Editor.FormatPage.ActionsGroup.Cleanup", _editor.Culture);
			C1RibbonButton btnCleanup = new C1RibbonButton(CONST_CLEANUP_NAME, cleanupText, CONST_CLEANUP_CSS);
			return btnCleanup;
		}

		/// <summary>
		/// Create cut button.
		/// </summary>
		/// <returns></returns>
		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateCutButton()
		{
			string cutText = C1Localizer.GetString("C1Editor.FormatPage.ActionsGroup.Cut", _editor.Culture);
			C1RibbonButton btnCut = new C1RibbonButton(CONST_CUT_NAME, cutText, CONST_CUT_CSS);
			return btnCut;
		}

		/// <summary>
		/// Create copy button.
		/// </summary>
		/// <returns></returns>
		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateCopyButton()
		{
			string copyText = C1Localizer.GetString("C1Editor.FormatPage.ActionsGroup.Copy", _editor.Culture);
			C1RibbonButton btnCopy = new C1RibbonButton(CONST_COPY_NAME, copyText, CONST_COPY_CSS);
			return btnCopy;
		}

		/// <summary>
		/// Create paste button.
		/// </summary>
		/// <returns></returns>
		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreatePasteButton()
		{
			string pasteText = C1Localizer.GetString("C1Editor.FormatPage.ActionsGroup.Paste", _editor.Culture);
			C1RibbonButton btnPaste = new C1RibbonButton(CONST_PASTE_NAME, pasteText, CONST_PASTE_CSS);
			return btnPaste;
		}

		/// <summary>
		/// Create select all button.
		/// </summary>
		/// <returns></returns>
		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateSelectAllButton()
		{
			string selectAllText = C1Localizer.GetString("C1Editor.FormatPage.ActionsGroup.SelectAll", _editor.Culture);
			C1RibbonButton btnSelectAll = new C1RibbonButton(CONST_SELECTALL_NAME, selectAllText, CONST_SELECTALL_CSS);
			return btnSelectAll;
		}

		/// <summary>
		/// Create font name button.
		/// </summary>
		/// <returns></returns>
		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonDropDownListButton CreateFontNameButton()
		{
			string fontNameText = C1Localizer.GetString("C1Editor.FormatPage.FontGroup.FontName", _editor.Culture);
			_btnFontNames = new C1RibbonDropDownListButton(CONST_FONTNAME_NAME,
				fontNameText, fontNameText, string.Empty);
			CreateFontNameItems();
			return _btnFontNames;
		}

		private string GetFontSizeCommand(string fontSize)
		{
			if (fontSize == "VerySmall")
			{
				return CONST_VERYSMALL_NAME;
			}

			if (fontSize == "Smaller")
			{
				return CONST_SMALLER_NAME;
			}

			if (fontSize == "Small")
			{
				return CONST_SMALL_NAME;
			}

			if (fontSize == "Medium")
			{
				return CONST_MEDIUM_NAME;
			}

			if (fontSize == "Large")
			{
				return CONST_LARGE_NAME;
			}

			if (fontSize == "Larger")
			{
				return CONST_LARGER_NAME;
			}

			if (fontSize == "VeryLarge")
			{
				return CONST_VERYLARGE_NAME;
			}

			return fontSize;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonDropDownListButton CreateFontSizeButton()
		{
			string fontSizeText = C1Localizer.GetString("C1Editor.FormatPage.FontGroup.FontSize", _editor.Culture);
			_btnFontSizes = new C1RibbonDropDownListButton(CONST_FONTSIZE_NAME,
				fontSizeText, fontSizeText, string.Empty);
			CreateFontSizeItems();
			return _btnFontSizes;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateBackColorButton()
		{
			string bcText = C1Localizer.GetString("C1Editor.FormatPage.FontGroup.BackgroundColor", _editor.Culture);
			C1RibbonButton btnBackColor = new C1RibbonButton(CONST_BACKCOLOR_NAME, bcText, CONST_BACKCOLOR_CSS);
			return btnBackColor;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateForeColorButton()
		{
			string fcText = C1Localizer.GetString("C1Editor.FormatPage.FontGroup.FontColor", _editor.Culture);
			C1RibbonButton btnFontColor = new C1RibbonButton(CONST_FONTCOLOR_NAME, fcText, CONST_FONTCOLOR_CSS);
			return btnFontColor;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonToggleButton CreateBoldButton()
		{
			string boldText = C1Localizer.GetString("C1Editor.FormatPage.FontGroup.Bold", _editor.Culture);
			C1RibbonToggleButton btnBold = new C1RibbonToggleButton(CONST_BOLD_NAME, boldText, CONST_BOLD_CSS);
			return btnBold;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonToggleButton CreateItalicButton()
		{
			string italicText = C1Localizer.GetString("C1Editor.FormatPage.FontGroup.Italic", _editor.Culture);
			C1RibbonToggleButton btnItalic = new C1RibbonToggleButton(CONST_ITALIC_NAME, italicText, CONST_ITALIC_CSS);
			return btnItalic;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonToggleButton CreateUnderLineButton()
		{
			string underlineText = C1Localizer.GetString("C1Editor.FormatPage.FontGroup.Underline", _editor.Culture);
			C1RibbonToggleButton btnUnderline = new C1RibbonToggleButton(CONST_UNDERLINE_NAME, underlineText, CONST_UNDERLINE_CSS);
			return btnUnderline;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonToggleButton CreateStrikeThroughButton()
		{
			string strikeText = C1Localizer.GetString("C1Editor.FormatPage.FontGroup.Strikethrough", _editor.Culture);
			C1RibbonToggleButton btnStrike = new C1RibbonToggleButton(CONST_STRIKE_NAME, strikeText, CONST_STRIKE_CSS);
			return btnStrike;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonToggleButton CreateSubScriptButton()
		{
			string subScriptText = C1Localizer.GetString("C1Editor.FormatPage.FontGroup.Subscript", _editor.Culture);
			C1RibbonToggleButton btnSubScript = new C1RibbonToggleButton(CONST_SUBSCRIPT_NAME, subScriptText, CONST_SUBSCRIPT_CSS);
			return btnSubScript;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonToggleButton CreateSuperScriptButton()
		{
			string superScriptText = C1Localizer.GetString("C1Editor.FormatPage.FontGroup.Superscript", _editor.Culture);
			C1RibbonToggleButton btnSuperScript = new C1RibbonToggleButton(CONST_SUPERSCRIPT_NAME, superScriptText, CONST_SUPERSCRIPT_CSS);
			return btnSuperScript;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateTemplateButton()
		{
			string templateText = C1Localizer.GetString("C1Editor.FormatPage.FontGroup.Template", _editor.Culture);
			C1RibbonButton btnTemplate = new C1RibbonButton(CONST_TEMPLATE_NAME, templateText, CONST_TEMPLATE_CSS);
			return btnTemplate;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateRemoveFormatButton()
		{
			string removeFormatText = C1Localizer.GetString("C1Editor.FormatPage.FontGroup.RemoveFormat", _editor.Culture);
			C1RibbonButton btnRemoveFormat = new C1RibbonButton(CONST_REMOVEFORMAT_NAME, removeFormatText, CONST_REMOVEFORMAT_CSS);
			return btnRemoveFormat;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonRadioButton CreateJustifyLeftButton()
		{
			string leftText = C1Localizer.GetString("C1Editor.FormatPage.ParagraphGroup.JustifyLeft", _editor.Culture);
			C1RibbonRadioButton btnLeft = new C1RibbonRadioButton(CONST_JUSTIFYLEFT_NAME, leftText, CONST_JUSTIFYLEFT_CSS,  this.OwnerID + "_alignment");
			return btnLeft;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonRadioButton CreateJustifyCenterButton()
		{
			string centerText = C1Localizer.GetString("C1Editor.FormatPage.ParagraphGroup.JustifyCenter", _editor.Culture);
            C1RibbonRadioButton btnCenter = new C1RibbonRadioButton(CONST_JUSTIFYCENTER_NAME, centerText, CONST_JUSTIFYCENTER_CSS, this.OwnerID + "_alignment");
			return btnCenter;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonRadioButton CreateJustifyRightButton()
		{
			string rightText = C1Localizer.GetString("C1Editor.FormatPage.ParagraphGroup.JustifyRight", _editor.Culture);
            C1RibbonRadioButton btnRight = new C1RibbonRadioButton(CONST_JUSTIFYRIGHT_NAME, rightText, CONST_JUSTIFYRIGHT_CSS, this.OwnerID + "_alignment");
			return btnRight;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonRadioButton CreateJustifyFullButton()
		{
			string fullText = C1Localizer.GetString("C1Editor.FormatPage.ParagraphGroup.JustifyFull", _editor.Culture);
            C1RibbonRadioButton btnFull = new C1RibbonRadioButton(CONST_JUSTIFYFULL_NAME, fullText, CONST_JUSTIFYFULL_CSS, this.OwnerID + "_alignment");
			return btnFull;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonToggleButton CreateBorderButton()
		{
			string borderText = C1Localizer.GetString("C1Editor.FormatPage.ParagraphGroup.Border", _editor.Culture);
			C1RibbonToggleButton btnBorder = new C1RibbonToggleButton(CONST_BORDERS_NAME, borderText, CONST_BORDERS_CSS);
			return btnBorder;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonRadioButton CreateNumberedListButton()
		{
			string numberedListText = C1Localizer.GetString("C1Editor.FormatPage.ParagraphGroup.NumberedList", _editor.Culture);
            C1RibbonRadioButton btnNumberedList = new C1RibbonRadioButton(CONST_NUMLIST_NAME, numberedListText, CONST_NUMLIST_CSS, this.OwnerID + "_list");
			return btnNumberedList;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonRadioButton CreateBulletedListButton()
		{
			string bulletedListText = C1Localizer.GetString("C1Editor.FormatPage.ParagraphGroup.BulletedList", _editor.Culture);
            C1RibbonRadioButton btnBulletedList = new C1RibbonRadioButton(CONST_BULLIST_NAME, bulletedListText, CONST_BULLIST_CSS, this.OwnerID + "_list");
			return btnBulletedList;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonRadioButton CreateOutdentButton()
		{
			string outdentText = C1Localizer.GetString("C1Editor.FormatPage.ParagraphGroup.Outdent", _editor.Culture);
            C1RibbonRadioButton btnOutdent = new C1RibbonRadioButton(CONST_OUTDENT_NAME, outdentText, CONST_OUTDENT_CSS, this.OwnerID + "_block");
			return btnOutdent;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonRadioButton CreateIndentButton()
		{
			string indentText = C1Localizer.GetString("C1Editor.FormatPage.ParagraphGroup.Indent", _editor.Culture);
            C1RibbonRadioButton btnIndent = new C1RibbonRadioButton(CONST_INDENT_NAME, indentText, CONST_INDENT_CSS, this.OwnerID + "_block");
			return btnIndent;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateSpellingButton()
		{
			string spellingText = C1Localizer.GetString("C1Editor.FormatPage.ReviewGroup.Spelling", _editor.Culture);
			C1RibbonButton btnSpelling = new C1RibbonButton(CONST_SPELLING_NAME, spellingText,
				spellingText, CONST_WIJBIGBUTTON_CSS, CONST_SPELLING_CSS, TextImageRelation.ImageAboveText);
			return btnSpelling;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateInspectButton()
		{
			string inspectText = C1Localizer.GetString("C1Editor.FormatPage.ReviewGroup.Inspect", _editor.Culture);
			string inspectTooltip = C1Localizer.GetString("C1Editor.FormatPage.ReviewGroup.TagInspect", _editor.Culture);
			C1RibbonButton btnInspect = new C1RibbonButton(CONST_INSPECT_NAME, inspectText,
				inspectTooltip, "", CONST_INSPECT_CSS);
			return btnInspect;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateFindButton()
		{
			string findText = C1Localizer.GetString("C1Editor.FormatPage.ReviewGroup.Find", _editor.Culture);
			string findTooltip = C1Localizer.GetString("C1Editor.FormatPage.ReviewGroup.FindAndReplace", _editor.Culture);
			C1RibbonButton btnFind = new C1RibbonButton(CONST_FIND_NAME, findText,
				findTooltip, "", CONST_FIND_CSS);
			return btnFind;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonSplitButton CreateTableButton()
		{
			string tableText = C1Localizer.GetString("C1Editor.InsertPage.TablesGroup.Table", _editor.Culture);
			C1RibbonSplitButton btnTable = new C1RibbonSplitButton(CONST_TABLE_NAME, tableText,
				tableText, CONST_WIJBIGBUTTON_CSS, CONST_TABLE_CSS, TextImageRelation.ImageAboveText);

			//dropdown list
			//insert table
			string insertText = C1Localizer.GetString("C1Editor.InsertPage.TablesGroup.Insert", _editor.Culture);
			string insertTooltip = C1Localizer.GetString("C1Editor.InsertPage.TablesGroup.InsertTable", _editor.Culture);
			btnTable.Buttons.Add(new C1RibbonButton(CONST_INSERT_NAME, insertText,
				insertTooltip, CONST_INSERT_CSS));

			//edit table
			string editText = C1Localizer.GetString("C1Editor.InsertPage.TablesGroup.Edit", _editor.Culture);
			string editTooltip = C1Localizer.GetString("C1Editor.InsertPage.TablesGroup.EditTable", _editor.Culture);
			btnTable.Buttons.Add(new C1RibbonButton(CONST_EDIT_NAME, editText,
				editTooltip, CONST_EDIT_CSS));

			return btnTable;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateInsertColumnButton()
		{
			string insertColumnTooltip = C1Localizer.GetString("C1Editor.InsertPage.TablesGroup.InsertColumn", _editor.Culture);
			C1RibbonButton btnInsertCol = new C1RibbonButton(CONST_INSERTCOL_NAME, insertColumnTooltip,
				CONST_INSERTCOL_CSS);
			return btnInsertCol;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateInsertRowButton()
		{
			string insertRowTooltip = C1Localizer.GetString("C1Editor.InsertPage.TablesGroup.InsertRow", _editor.Culture);
			C1RibbonButton btnInsertRow = new C1RibbonButton(CONST_INSERTROW_NAME, insertRowTooltip,
				CONST_INSERTROW_CSS);
			return btnInsertRow;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateInsertCellButton()
		{
			string insertCellTooltip = C1Localizer.GetString("C1Editor.InsertPage.TablesGroup.InsertCell", _editor.Culture);
			C1RibbonButton btnInsertCell = new C1RibbonButton(CONST_INSERTCELL_NAME, insertCellTooltip,
				CONST_INSERTCELL_CSS);
			return btnInsertCell;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateSplitCellButton()
		{
			string splitCellTooltip = C1Localizer.GetString("C1Editor.InsertPage.TablesGroup.SplitCell", _editor.Culture);
			C1RibbonButton btnSplitCell = new C1RibbonButton(CONST_SPLITCELL_NAME, splitCellTooltip,
				CONST_SPLITCELL_CSS);
			return btnSplitCell;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateMergeCellButton()
		{
			string mergeCellTooltip = C1Localizer.GetString("C1Editor.InsertPage.TablesGroup.MergeCell", _editor.Culture);
			C1RibbonButton btnMergeCell = new C1RibbonButton(CONST_MERGECELL_NAME, mergeCellTooltip,
				CONST_MERGECELL_CSS);
			return btnMergeCell;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateDeleteColumnButton()
		{
			string deleteColumnTooltip = C1Localizer.GetString("C1Editor.InsertPage.TablesGroup.DeleteColumn", _editor.Culture);
			C1RibbonButton btnDeleteColumn = new C1RibbonButton(CONST_DELETECOL_NAME, deleteColumnTooltip,
				CONST_DELETECOL_CSS);
			return btnDeleteColumn;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateDeleteRowButton()
		{
			string deleteRowTooltip = C1Localizer.GetString("C1Editor.InsertPage.TablesGroup.DeleteRow", _editor.Culture);
			C1RibbonButton btnDeleteRow = new C1RibbonButton(CONST_DELETEROW_NAME, deleteRowTooltip,
				CONST_DELETEROW_CSS);
			return btnDeleteRow;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateDeleteCellButton()
		{
			string deleteCellTooltip = C1Localizer.GetString("C1Editor.InsertPage.TablesGroup.DeleteCell", _editor.Culture);
			C1RibbonButton btnDeleteCell = new C1RibbonButton(CONST_DELETECELL_NAME, deleteCellTooltip,
				CONST_DELETECELL_CSS);
			return btnDeleteCell;
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateInsertBreakButton()
		{
			string insertBreakTooltip = C1Localizer.GetString("C1Editor.InsertPage.BreaksGroup.InsertBreak", _editor.Culture);

			return new C1RibbonButton(CONST_INSERTBREAK_NAME, insertBreakTooltip, CONST_INSERTBREAK_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateInsertParagraphButton()
		{
			string insertParagraphTooltip = C1Localizer.GetString("C1Editor.InsertPage.BreaksGroup.InsertParagraph", _editor.Culture);

			return new C1RibbonButton(CONST_INSERTPARA_NAME, insertParagraphTooltip, CONST_INSERTPARAGRAPH_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateInsertPrintButton()
		{
			string insertPrintTooltip = C1Localizer.GetString("C1Editor.InsertPage.BreaksGroup.InsertPrint", _editor.Culture);

			return new C1RibbonButton(CONST_INSERTPRINTPAGEBREAK_NAME, insertPrintTooltip, CONST_INSERTPRINTPAGEBREAK_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateInsertHRButton()
		{
			string insertHorizontalLineTooltip = C1Localizer.GetString("C1Editor.InsertPage.BreaksGroup.InsertHorizontalLine", _editor.Culture);

			return new C1RibbonButton(CONST_INSERTHRULE_NAME, insertHorizontalLineTooltip, CONST_INSERTHLINE_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateFormButton()
		{
			string formTooltip = C1Localizer.GetString("C1Editor.InsertPage.FormsGroup.Form", _editor.Culture);

			return new C1RibbonButton(CONST_FORM_NAME, formTooltip, CONST_FORM_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateTextAreaButton()
		{
			string textAreaTooltip = C1Localizer.GetString("C1Editor.InsertPage.FormsGroup.TextArea", _editor.Culture);

			return new C1RibbonButton(CONST_TEXTAREA_NAME, textAreaTooltip, CONST_TEXTAREA_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateTextBoxButton()
		{
			string textBoxTooltip = C1Localizer.GetString("C1Editor.InsertPage.FormsGroup.TextBox", _editor.Culture);

			return new C1RibbonButton(CONST_TEXTBOX_NAME, textBoxTooltip, CONST_TEXTBOX_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreatePasswordFieldButton()
		{
			string passwordFieldTooltip = C1Localizer.GetString("C1Editor.InsertPage.FormsGroup.PasswordField", _editor.Culture);

			return new C1RibbonButton(CONST_PASSWORD_NAME, passwordFieldTooltip, CONST_PASSWORD_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateHiddenFieldButton()
		{
			string hiddenFieldTooltip = C1Localizer.GetString("C1Editor.InsertPage.FormsGroup.HiddenField", _editor.Culture);

			return new C1RibbonButton(CONST_HIDDENFIELD_NAME, hiddenFieldTooltip, CONST_HIDDENFIELD_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateImageButton()
		{
			string imageButtonTooltip = C1Localizer.GetString("C1Editor.InsertPage.FormsGroup.ImageButton", _editor.Culture);

			return new C1RibbonButton(CONST_IMAGEBUTTON_NAME, imageButtonTooltip, CONST_IMAGEBUTTON_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateButtonButton()
		{
			string buttonTooltip = C1Localizer.GetString("C1Editor.InsertPage.FormsGroup.Button", _editor.Culture);

			return new C1RibbonButton(CONST_BUTTON_NAME, buttonTooltip, CONST_BUTTON_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateListButton()
		{
			string listBoxTooltip = C1Localizer.GetString("C1Editor.InsertPage.FormsGroup.ListBox", _editor.Culture);

			return new C1RibbonButton(CONST_LISTBOX_NAME, listBoxTooltip, CONST_LISTBOX_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateDropDownListButton()
		{
			string dropDownListTooltip = C1Localizer.GetString("C1Editor.InsertPage.FormsGroup.DropDownList", _editor.Culture);

			return new C1RibbonButton(CONST_DROPDOWNLIST_NAME, dropDownListTooltip, CONST_DROPDOWNLIST_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateRadioButton()
		{
			string radioButtonTooltip = C1Localizer.GetString("C1Editor.InsertPage.FormsGroup.RadioButton", _editor.Culture);

			return new C1RibbonButton(CONST_RADIO_NAME, radioButtonTooltip, CONST_RADIO_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateCheckBoxButton()
		{
			string checkBoxTooltip = C1Localizer.GetString("C1Editor.InsertPage.FormsGroup.CheckBox", _editor.Culture);

			return new C1RibbonButton(CONST_CHECKBOX_NAME, checkBoxTooltip, CONST_CHECKBOX_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateLinkButton()
		{
			string linkText = C1Localizer.GetString("C1Editor.InsertPage.SpecialGroup.Link", _editor.Culture);

			if (this.Mode == Controls.C1Editor.Mode.Ribbon)
			{
				return new C1RibbonButton(CONST_LINK_NAME, linkText,
					linkText, CONST_WIJBIGBUTTON_CSS, CONST_LINK_CSS, TextImageRelation.ImageAboveText);
			}

			return new C1RibbonButton(CONST_LINK_NAME, linkText, CONST_LINK16_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateInsertImageButton()
		{
			string imageBrowserTooltip = C1Localizer.GetString("C1Editor.InsertPage.SpecialGroup.ImageBrowser", _editor.Culture);

			return new C1RibbonButton(CONST_IMAGEBROWSER_NAME, imageBrowserTooltip, CONST_IMAGEBROWSER_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateMediaButton()
		{
			string mediaTooltip = C1Localizer.GetString("C1Editor.InsertPage.SpecialGroup.Media", _editor.Culture);

			return new C1RibbonButton(CONST_MEDIA_NAME, mediaTooltip, CONST_MEDIA_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateInsertSpecialCharButton()
		{
			string insertSpecialCharacterTooltip = C1Localizer.GetString("C1Editor.InsertPage.SpecialGroup.InsertSpecialCharacter", _editor.Culture);

			return new C1RibbonButton(CONST_SPECIALCHAR_NAME, insertSpecialCharacterTooltip, CONST_SPECIALCHAR_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateInsertDateButton()
		{
			string insertDateTimeTooltip = C1Localizer.GetString("C1Editor.InsertPage.SpecialGroup.InsertDateTime", _editor.Culture);

			return new C1RibbonButton(CONST_DATETIME_NAME, insertDateTimeTooltip, CONST_DATETIME_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateInsertCodeButton()
		{
			string codeTooltip = C1Localizer.GetString("C1Editor.Code", _editor.Culture);

			return new C1RibbonButton(CONST_CODE_NAME, codeTooltip, CONST_CODE_CSS);
		}

		[SmartAssembly.Attributes.DoNotObfuscate]
		private C1RibbonButton CreateBlockQuoteButton()
		{
			string blockQuoteTooltip = C1Localizer.GetString("C1Editor.BlockQuote", _editor.Culture);

			return new C1RibbonButton(CONST_BLOCKQUOTE_NAME, blockQuoteTooltip, CONST_BLOCKQUOTE_CSS);
		}

		private C1RibbonButton CreateButton(string command)
		{
			System.Reflection.MethodInfo mi = this.GetType().GetMethod("Create" + command + "Button",
				System.Reflection.BindingFlags.NonPublic |
				System.Reflection.BindingFlags.InvokeMethod |
				System.Reflection.BindingFlags.Instance);

			if (mi == null)
			{
				return null;
			}

			return (C1RibbonButton)mi.Invoke(this, null);
		}
		#endregion ** end of all buttons.

		#region ** format tab
		#region ** create action group
		/// <summary>
		/// Creates all buttons for action group.
		/// </summary>
		/// <returns>
		/// Returns the action group.
		/// </returns>
		private C1RibbonGroup CreateActionGroup()
		{
			C1RibbonGroup gpAction = new C1RibbonGroup();
			gpAction.HeaderText = C1Localizer.GetString("C1Editor.FormatPage.ActionGroup", _editor.Culture);
			gpAction.Name = CONST_ACTION_NAME;
			gpAction.DisplayName = "Actions";
			gpAction.Buttons.Clear();

			#region ** big save button
			//Save button
			gpAction.Buttons.Add(this.CreateSaveButton());
			#endregion end of ** big save button.

			#region ** undo & redo group
			//undo&redo group.
			C1RibbonButtonGroup undoRedoGroup = new C1RibbonButtonGroup();
			gpAction.Buttons.Add(undoRedoGroup);

			//undo
			undoRedoGroup.Buttons.Add(this.CreateUndoButton());
			//redo
			undoRedoGroup.Buttons.Add(this.CreateRedoButton());
			#endregion end of ** undo & redo group.

			#region ** preview & cleanup group
			// Preview&Cleanup group
			C1RibbonButtonGroup previewPrintGroup = new C1RibbonButtonGroup();
			gpAction.Buttons.Add(previewPrintGroup);

			// preview button
			previewPrintGroup.Buttons.Add(this.CreatePreviewButton());
			// cleanup button
			previewPrintGroup.Buttons.Add(this.CreateCleanupButton());
			#endregion end of ** preview & cleanup group.

			#region ** edit group
			// Preview&Cleanup group
			C1RibbonButtonGroup editGroup = new C1RibbonButtonGroup();
			gpAction.Buttons.Add(editGroup);

			// cut button
			editGroup.Buttons.Add(this.CreateCutButton());
			// copy button
			editGroup.Buttons.Add(this.CreateCopyButton());
			// paste button
			editGroup.Buttons.Add(this.CreatePasteButton());
			// select all button
			editGroup.Buttons.Add(this.CreateSelectAllButton());
			#endregion end of ** edit group.

			return gpAction;
		}
		#endregion end of ** create action group.

		#region ** create font group
		/// <summary>
		/// Creates all buttons for font group.
		/// </summary>
		/// <returns>
		/// Returns the font group.
		/// </returns>
		private C1RibbonGroup CreateFontGroup()
		{
			C1RibbonGroup gpFont = new C1RibbonGroup();
			gpFont.HeaderText = C1Localizer.GetString("C1Editor.FormatPage.FontGroup", _editor.Culture);
			gpFont.Name = CONST_FONT_NAME;
			gpFont.DisplayName = CONST_FONT_NAME;
			gpFont.Buttons.Clear();

			//font name button
			gpFont.Buttons.Add(this.CreateFontNameButton());
			//font size button
			gpFont.Buttons.Add(this.CreateFontSizeButton());

			#region ** back & font color group
			//back&font color group.
			C1RibbonButtonGroup backFontColorGroup = new C1RibbonButtonGroup();
			gpFont.Buttons.Add(backFontColorGroup);

			//back color
			backFontColorGroup.Buttons.Add(this.CreateBackColorButton());
			//font color
			backFontColorGroup.Buttons.Add(this.CreateForeColorButton());
			#endregion end of ** back & font color group.

			#region ** font style group
			//font style group.
			C1RibbonButtonGroup fontStyleGroup = new C1RibbonButtonGroup();
			gpFont.Buttons.Add(fontStyleGroup);

			//bold
			fontStyleGroup.Buttons.Add(this.CreateBoldButton());
			//italic
			fontStyleGroup.Buttons.Add(this.CreateItalicButton());
			//underline
			fontStyleGroup.Buttons.Add(this.CreateUnderLineButton());
			//strikethrough
			fontStyleGroup.Buttons.Add(this.CreateStrikeThroughButton());
			//sub script
			fontStyleGroup.Buttons.Add(this.CreateSubScriptButton());
			//super script
			fontStyleGroup.Buttons.Add(this.CreateSuperScriptButton());
			#endregion end of ** font style group.

			#region ** template group
			C1RibbonButtonGroup templateGroup = new C1RibbonButtonGroup();
			gpFont.Buttons.Add(templateGroup);

			//template
			templateGroup.Buttons.Add(this.CreateTemplateButton());
			#endregion end of ** template group.

			#region ** remove format button
			C1RibbonButtonGroup removeFormatGroup = new C1RibbonButtonGroup();
			gpFont.Buttons.Add(removeFormatGroup);

			//remove format
			removeFormatGroup.Buttons.Add(this.CreateRemoveFormatButton());
			#endregion end of ** remove format button.

			return gpFont;
		}
		#endregion end of ** create action group.

		#region ** create paragraph group
		/// <summary>
		/// Creates all buttons for paragraph group.
		/// </summary>
		/// <returns>
		/// Returns the paragraph group.
		/// </returns>
		private C1RibbonGroup CreateParagraphGroup()
		{
			C1RibbonGroup gpPara = new C1RibbonGroup();
			gpPara.HeaderText = C1Localizer.GetString("C1Editor.FormatPage.ParagraphGroup", _editor.Culture);
			gpPara.Name = CONST_PARA_NAME;
			gpPara.DisplayName = CONST_PARA_NAME;
			gpPara.Buttons.Clear();

			#region ** alignment group
			//alignment group.
			C1RibbonButtonGroup alignmentGroup = new C1RibbonButtonGroup();
			gpPara.Buttons.Add(alignmentGroup);

			//left
			alignmentGroup.Buttons.Add(this.CreateJustifyLeftButton());
			//center
			alignmentGroup.Buttons.Add(this.CreateJustifyCenterButton());
			//right
			alignmentGroup.Buttons.Add(this.CreateJustifyRightButton());
			//full
			alignmentGroup.Buttons.Add(this.CreateJustifyFullButton());
			#endregion end of ** alignment group.

			#region ** border button
			C1RibbonButtonGroup borderGroup = new C1RibbonButtonGroup();
			gpPara.Buttons.Add(borderGroup);

			//border
			borderGroup.Buttons.Add(this.CreateBorderButton());
			#endregion end of ** border button.

			#region ** list group
			//list group.
			C1RibbonButtonGroup listGroup = new C1RibbonButtonGroup();
			gpPara.Buttons.Add(listGroup);

			//number
			listGroup.Buttons.Add(this.CreateNumberedListButton());
			//bulleted
			listGroup.Buttons.Add(this.CreateBulletedListButton());
			#endregion end of ** list group.

			#region ** indent group
			//indent group.
			C1RibbonButtonGroup indentGroup = new C1RibbonButtonGroup();
			gpPara.Buttons.Add(indentGroup);

			//outdent
			indentGroup.Buttons.Add(this.CreateOutdentButton());
			//indent
			indentGroup.Buttons.Add(this.CreateIndentButton());
			#endregion end of ** indent group.

			return gpPara;
		}
		#endregion end of ** create paragraph group.

		#region ** create review group
		/// <summary>
		/// Creates all buttons for review group.
		/// </summary>
		/// <returns>
		/// Returns the review group.
		/// </returns>
		private C1RibbonGroup CreateReviewGroup()
		{
			C1RibbonGroup gpReview = new C1RibbonGroup();
			gpReview.HeaderText = C1Localizer.GetString("C1Editor.FormatPage.ReviewGroup", _editor.Culture);
			gpReview.Name = CONST_REVIEW_NAME;
			gpReview.DisplayName = CONST_REVIEW_NAME;
			gpReview.CssClass = CONST_REVIEWWITHSPELLING_CSS;
			gpReview.Buttons.Clear();

			//spelling
			gpReview.Buttons.Add(this.CreateSpellingButton());
			//inspect
			gpReview.Buttons.Add(this.CreateInspectButton());
			//find
			gpReview.Buttons.Add(this.CreateFindButton());

			return gpReview;
		}
		#endregion end of ** create review group.
		#endregion end of ** format tab.

		#region ** insert tab
		#region ** create tables group
		/// <summary>
		/// Creates all buttons for tables group.
		/// </summary>
		/// <returns>
		/// Returns the tables group.
		/// </returns>
		private C1RibbonGroup CreateTablesGroup()
		{
			C1RibbonGroup gpTables = new C1RibbonGroup();
			gpTables.HeaderText = C1Localizer.GetString("C1Editor.InsertPage.TablesGroup", _editor.Culture);
			gpTables.Name = CONST_TABLES_NAME;
			gpTables.DisplayName = CONST_TABLES_NAME;
			gpTables.Buttons.Clear();

			#region ** table split button
			//table button.
			gpTables.Buttons.Add(this.CreateTableButton());
			#endregion end of ** table split button.

			#region ** insert group
			//insert group
			C1RibbonButtonGroup insertGroup = new C1RibbonButtonGroup();
			gpTables.Buttons.Add(insertGroup);

			//insert column
			insertGroup.Buttons.Add(this.CreateInsertColumnButton());
			//insert row
			insertGroup.Buttons.Add(this.CreateInsertRowButton());
			//insert cell
			insertGroup.Buttons.Add(this.CreateInsertCellButton());
			#endregion end of ** insert group.

			#region ** edit group
			//edit group
			C1RibbonButtonGroup editGroup = new C1RibbonButtonGroup();
			gpTables.Buttons.Add(editGroup);

			//split cell
			editGroup.Buttons.Add(this.CreateSplitCellButton());
			//merge cell
			editGroup.Buttons.Add(this.CreateMergeCellButton());
			#endregion end of ** edit group.

			#region ** delete group
			//delete group
			C1RibbonButtonGroup deleteGroup = new C1RibbonButtonGroup();
			gpTables.Buttons.Add(deleteGroup);

			//delete column
			deleteGroup.Buttons.Add(this.CreateDeleteColumnButton());
			//delete row
			deleteGroup.Buttons.Add(this.CreateDeleteRowButton());
			//delete cell
			deleteGroup.Buttons.Add(this.CreateDeleteCellButton());
			#endregion end of ** delete group.

			return gpTables;
		}
		#endregion end of ** create tables group.

		#region ** create breaks group
		/// <summary>
		/// Creates all buttons for breaks group.
		/// </summary>
		/// <returns>
		/// Returns the breaks group.
		/// </returns>
		private C1RibbonGroup CreateBreaksGroup()
		{
			C1RibbonGroup gpBreaks = new C1RibbonGroup();
			gpBreaks.HeaderText = C1Localizer.GetString("C1Editor.InsertPage.BreaksGroup", _editor.Culture);
			gpBreaks.Name = CONST_BREAKS_NAME;
			gpBreaks.DisplayName = CONST_BREAKS_NAME;
			gpBreaks.Buttons.Clear();

			//insert break
			gpBreaks.Buttons.Add(this.CreateInsertBreakButton());
			//insert para
			gpBreaks.Buttons.Add(this.CreateInsertParagraphButton());
			//insert break
			gpBreaks.Buttons.Add(this.CreateInsertPrintButton());
			//insert break
			gpBreaks.Buttons.Add(this.CreateInsertHRButton());

			return gpBreaks;
		}
		#endregion end of ** create breaks group.

		#region ** create forms group
		/// <summary>
		/// Creates all buttons for forms group.
		/// </summary>
		/// <returns>
		/// Returns the forms group.
		/// </returns>
		private C1RibbonGroup CreateFormsGroup()
		{
			C1RibbonGroup gpForms = new C1RibbonGroup();
			gpForms.HeaderText = C1Localizer.GetString("C1Editor.InsertPage.FormsGroup", _editor.Culture);
			gpForms.Name = CONST_FORMS_NAME;
			gpForms.DisplayName = CONST_FORMS_NAME;
			gpForms.Buttons.Clear();

			#region ** form group
			C1RibbonButtonGroup gpForm = new C1RibbonButtonGroup();
			gpForms.Buttons.Add(gpForm);

			//form button
			gpForm.Buttons.Add(this.CreateFormButton());
			#endregion end of ** form group.

			#region ** text group
			C1RibbonButtonGroup gpText = new C1RibbonButtonGroup();
			gpForms.Buttons.Add(gpText);

			//textarea button
			gpText.Buttons.Add(this.CreateTextAreaButton());
			//textbox button
			gpText.Buttons.Add(this.CreateTextBoxButton());
			//password field button
			gpText.Buttons.Add(this.CreatePasswordFieldButton());
			//hidden field button
			gpText.Buttons.Add(this.CreateHiddenFieldButton());
			#endregion end of ** text group.

			#region ** button group
			C1RibbonButtonGroup gpButton = new C1RibbonButtonGroup();
			gpForms.Buttons.Add(gpButton);

			//image button button
			gpButton.Buttons.Add(this.CreateImageButton());
			//button button
			gpButton.Buttons.Add(this.CreateButtonButton());
			#endregion end of ** button group.

			#region ** list group
			C1RibbonButtonGroup gpList = new C1RibbonButtonGroup();
			gpForms.Buttons.Add(gpList);

			//list button
			gpList.Buttons.Add(this.CreateListButton());
			//dropdownlist button
			gpList.Buttons.Add(this.CreateDropDownListButton());
			//radio button
			gpList.Buttons.Add(this.CreateRadioButton());
			//checkbox button
			gpList.Buttons.Add(this.CreateCheckBoxButton());
			#endregion end of ** list group.

			return gpForms;
		}
		#endregion end of ** create forms group.

		#region ** create special group
		/// <summary>
		/// Creates all buttons for special group.
		/// </summary>
		/// <returns>
		/// Returns the special group.
		/// </returns>
		private C1RibbonGroup CreateSpecialGroup()
		{
			C1RibbonGroup gpSpecial = new C1RibbonGroup();
			gpSpecial.HeaderText = C1Localizer.GetString("C1Editor.InsertPage.SpecialGroup", _editor.Culture);
			gpSpecial.Name = CONST_SPECIAL_NAME;
			gpSpecial.DisplayName = CONST_SPECIAL_NAME;
			gpSpecial.Buttons.Clear();

			#region ** link button
			//table button.
			gpSpecial.Buttons.Add(this.CreateLinkButton());
			#endregion end of ** link button.

			#region ** other buttons
			//image browser button
			gpSpecial.Buttons.Add(this.CreateInsertImageButton());
			//media button
			gpSpecial.Buttons.Add(this.CreateMediaButton());
			//insert special char button
			gpSpecial.Buttons.Add(this.CreateInsertSpecialCharButton());
			//insert data time button
			gpSpecial.Buttons.Add(this.CreateInsertDateButton());
			#endregion end of ** other buttons.

			return gpSpecial;
		}
		#endregion end of ** create special group.
		#endregion end of ** insert tab.

		#region ** create ribbon tabs
		private HtmlGenericControl CreateHeaderPanel()
		{
			HtmlGenericControl divHeader = new HtmlGenericControl("div");
			if (this._isDesignMode)
			{
				divHeader.Attributes["class"] = CONST_WIJRIBBON_CSS + " ui-widget ui-widget-content ui-helper-clearfix";
				divHeader.Style[HtmlTextWriterStyle.Width] = "100%";
			}

			if (this.Mode == Mode.Simple)
			{
				foreach (string command in this.SimpleModeCommands)
				{
					C1RibbonButton rbButton = this.CreateButton(command);

					if (rbButton != null)
					{
						divHeader.Controls.Add(rbButton);
					}
				}

				return divHeader;
			}

			if (this.Mode == Mode.Bbcode)
			{
				divHeader.Controls.Add(this.CreateBoldButton());
				divHeader.Controls.Add(this.CreateItalicButton());
				divHeader.Controls.Add(this.CreateStrikeThroughButton());
				divHeader.Controls.Add(this.CreateUnderLineButton());
				divHeader.Controls.Add(this.CreateForeColorButton());
				divHeader.Controls.Add(this.CreateFontSizeButton());
				divHeader.Controls.Add(this.CreateLinkButton());
				divHeader.Controls.Add(this.CreateInsertImageButton());
				divHeader.Controls.Add(this.CreateNumberedListButton());
				divHeader.Controls.Add(this.CreateBulletedListButton());
				divHeader.Controls.Add(this.CreateBlockQuoteButton());

				return divHeader;
			}

			if (this._isDesignMode)
			{
				divHeader.Attributes["class"] += " ui-tabs wijmo-wijtabs ui-tabs-top";
			}

			bool page1Created = false;
			
			if (this.TabPages.Count > 0)
			{
				HtmlGenericControl ul = new HtmlGenericControl("ul");
				divHeader.Controls.Add(ul);

				if (this._isDesignMode)
				{
					ul.Attributes["class"] = "ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header";
				}

				foreach (C1RibbonTabPage page in this.TabPages)
				{
					ul.Controls.Add(this.CreateTab(page, page1Created));

					//Design time we only need show the first ribbon tab.
					if (this._isDesignMode && page1Created)
					{
						continue;
					}

					divHeader.Controls.Add(this.CreateTabPage(page));
					page1Created = true;
				}
			}

			return divHeader;
		}

		private Control CreateTab(C1RibbonTabPage page, bool hasATabActive)
		{
			HtmlGenericControl li = new HtmlGenericControl("li");

			//update for issue 21292 at 2012/7/23
			//HtmlAnchor anchor = new HtmlAnchor();
			//anchor.HRef = string.Format("#{0}{1}", this.OwnerID, page.Name);
			//anchor.InnerText = page.Text;

			HtmlGenericControl anchor = new HtmlGenericControl("a"); 
			anchor.Attributes.Add("href", string.Format("#{0}{1}", this.OwnerID, page.Name));
			anchor.InnerText = page.Text;
			//end for 21292 issue

			li.Controls.Add(anchor);

			if (this._isDesignMode)
			{
				if (!hasATabActive)
				{
					li.Attributes["class"] = "ui-state-default ui-tabs-selected ui-state-active";
				}
				else
				{
					li.Attributes["class"] = "ui-state-default";
				}
			}

			return li;
		}

		private Control CreateTabPage(C1RibbonTabPage page)
		{
			HtmlGenericControl divPage = new HtmlGenericControl("div");
			divPage.Attributes["id"] = string.Format("{0}{1}", this.OwnerID, page.Name);
			
			if (page.Groups.Count > 0)
			{
				HtmlGenericControl ul = new HtmlGenericControl("ul");
				divPage.Controls.Add(ul);

				if (this._isDesignMode)
				{
					ul.Attributes["class"] = "ui-helper-reset ui-helper-clearfix ui-widget-content wijmo-wijribbon-groups";
				}

				foreach (C1RibbonGroup group in page.Groups)
				{
					ul.Controls.Add(this.CreateRibbonGroup(group));
				}
			}

			if (this._isDesignMode)
			{
				divPage.Attributes["class"] = "ui-tabs-panel ui-widget-content wijmo-wijribbon-panel";
			}

			return divPage;
		}

		private Control CreateRibbonGroup(C1RibbonGroup group)
		{
			HtmlGenericControl li = new HtmlGenericControl("li");
			HtmlGenericControl divContent = new HtmlGenericControl("div");
			HtmlGenericControl divLabel = new HtmlGenericControl("div");
			string groupDisplayName = string.Empty;

			if (this._isDesignMode && group.Buttons.Count > 0)
			{
				divContent.Attributes["class"] = "wijmo-wijribbon-group-content";
				li.Controls.Add(divContent);

				foreach (C1RibbonButton button in group.Buttons)
				{
					divContent.Controls.Add(button);
				}
			}
			else
			{
				if (!string.IsNullOrEmpty(group.CssClass))
				{
					li.Attributes["class"] = group.CssClass;
				}
				
				foreach (C1RibbonButton button in group.Buttons)
				{
					li.Controls.Add(button);
				}
			}

			divLabel.InnerText = group.HeaderText;
			//update for localization
			if (group.DisplayName != null)
			{
				groupDisplayName = group.DisplayName.ToLower().Trim();
			}
			else
			{
				groupDisplayName = group.Name.ToLower().Trim();
			}

			divLabel.Attributes["displayname"] = groupDisplayName;
			
			li.Controls.Add(divLabel);

			if (this._isDesignMode)
			{
				//li.Attributes["class"] = "ui-state-default wijmo-wijribbon-group wijmo-wijribbon-" + group.HeaderText.ToLower().Trim();
				//update for localization
				li.Attributes["class"] = "ui-state-default wijmo-wijribbon-group wijmo-wijribbon-" + groupDisplayName.ToLower().Trim();
				divLabel.Attributes["class"] = "wijmo-wijribbon-group-label";
			}

			return li;
		}
		#endregion end of ** create ribbon tabs.

		#region ** create body panel
		private HtmlGenericControl CreateBodyPanel()
		{
			bodyPanel = new HtmlGenericControl("div");

			if (this._isDesignMode)
			{
				bodyPanel.Style[HtmlTextWriterStyle.Overflow] = "hidden";
				bodyPanel.Style[HtmlTextWriterStyle.Width] = this.Owner.Width.ToString();
				bodyPanel.Style[HtmlTextWriterStyle.Height] = BodyPanelHeight + "px";
				bodyPanel.Attributes.Add("class", "ui-widget-content");
			}

			return bodyPanel;
		}
		#endregion end of ** create body panel.

		#region ** create foot panel
		private HtmlGenericControl CreateFooterPanel()
		{
			HtmlGenericControl footerPanel = new HtmlGenericControl("div");

			if (this.Owner.ShowPathSelector)
			{
				footerPanel.Controls.Add(this.CreatePathSelector());
			}

			HtmlGenericControl modes = new HtmlGenericControl("div");
			modes.Style["position"] = "relative";
			modes.Style["width"] = "100%";
			footerPanel.Controls.Add(modes);

			HtmlGenericControl modesPanel = new HtmlGenericControl("span");
			modes.Controls.Add(modesPanel);
			//design/source/split view modes
			C1RibbonButtonGroup modesGroup = new C1RibbonButtonGroup();
			modesPanel.Controls.Add(modesGroup);

			modesGroup.Buttons.Add(new C1RibbonRadioButton(CONST_DESIGNVIEW_NAME,
                C1Localizer.GetString("C1Editor.Footer.ModesGroup.DesignView", _editor.Culture), CONST_DESIGNVIEW_CSS, this.OwnerID + "_modes"));
			modesGroup.Buttons.Add(new C1RibbonRadioButton(CONST_SOURCEVIEW_NAME,
				C1Localizer.GetString("C1Editor.Footer.ModesGroup.SourceView", _editor.Culture), CONST_SOURCEVIEW_CSS, this.OwnerID + "_modes"));
			modesGroup.Buttons.Add(new C1RibbonRadioButton(CONST_SPLITVIEW_NAME,
				C1Localizer.GetString("C1Editor.Footer.ModesGroup.SplitView", _editor.Culture), CONST_SPLITVIEW_CSS, this.OwnerID + "_modes"));

			C1RibbonButton wordWrap = new C1RibbonButton(CONST_WORDWRAP_NAME,
				C1Localizer.GetString("C1Editor.Footer.ModesGroup.WordWrap", _editor.Culture), CONST_WORDWRAP_CSS);
			wordWrap.Attributes["disabled"] = "disabled";
			modesPanel.Controls.Add(wordWrap);

			HtmlGenericControl separator = new HtmlGenericControl("span");
			separator.Attributes["class"] = CONST_SEPARATOR_CSS;
			modesPanel.Controls.Add(separator);

			modesPanel.Controls.Add(new C1RibbonButton(CONST_FULLSCREEN_NAME,
				C1Localizer.GetString("C1Editor.Footer.ModesGroup.FullScreen", _editor.Culture), CONST_FULLSCREEN_CSS));

			if (this._isDesignMode)
			{
				footerPanel.Attributes["class"] = string.Format("{0} {1} {2} {3}",
					CONST_WIJFOOTER_CSS, CONST_WIDGET_CSS, CONST_WIDGET_CONTENT_CSS, CONST_STATE_DEFAULT_CSS);
				modesPanel.Attributes["class"] = string.Format("{0} {1}", CONST_WIJRIBBON_CSS, CONST_WIJMODES_CSS);
			}
			else
			{
				footerPanel.Attributes["class"] = CONST_EDITOR_FOOTER_CSS;
			}

			return footerPanel;
		}

		/// <summary>
		/// Create a PathSelector for the control.
		/// </summary>
		/// <returns></returns>
		private Control CreatePathSelector()
		{
			HtmlGenericControl bar = new HtmlGenericControl("div");

			if (this._isDesignMode)
			{
				bar.Attributes["class"] = CONST_WIJPATHSELECTOR_CSS;

				HtmlGenericControl label = new HtmlGenericControl("label");
				label.Attributes["class"] = CONST_WIJPATHLABEL_CSS;
				label.InnerText = "body";

				bar.Controls.Add(label);
			}

			return bar;
		}
		#endregion end of ** create foot panel.
		#endregion end of ** private methods.

		#region ** internal methods
		/// <summary>
		/// Create a Ribbon UI for C1Editor
		/// </summary>
		/// <returns>Parent control that contains all the children of this UI.</returns>
		internal ArrayList Create()
		{
			ArrayList controls = new ArrayList();

			var footerPanel = CreateFooterPanel();
			footerPanel.Visible = Owner.ShowFooter;

			if (this._isDesignMode)
			{
				Panel container = new Panel();
				container.CssClass = CONST_WIJCONTAINER_CSS;
				container.Width = Unit.Percentage(100);
				container.Height = Unit.Percentage(100);
				container.Style["position"] = "relative";

				container.Controls.Add(this.CreateHeaderPanel());
				container.Controls.Add(this.CreateBodyPanel());
				container.Controls.Add(footerPanel);

				controls.Add(container);
			}
			else
			{
				controls.Add(this.CreateHeaderPanel());
				controls.Add(this.CreateBodyPanel());
				controls.Add(footerPanel);
			}

			return controls;
		}

		internal void SetRibbonContent(string text)
		{
			if (this.bodyPanel != null)
			{
				this.bodyPanel.InnerHtml = text;
			}
		}

		internal void CreateFontNameItems()
		{
			_btnFontNames.Buttons.Clear();
			foreach (FontOption fn in this.FontNames)
			{
				_btnFontNames.Buttons.Add(new C1RibbonRadioButton(fn.Name, fn.Text, fn.Tooltip, string.Empty, this.OwnerID + "_fontname"));
			}
		}

		internal void CreateFontSizeItems()
		{
			_btnFontSizes.Buttons.Clear();
			foreach (FontOption fs in this.FontSizes)
			{
				string text = fs.Text;
				if (GetFontSizeCommand(fs.Text) == fs.Name)
				{
					var resourceText = C1Localizer.GetResourceText("C1Editor.FormatPage.FontGroup.FontSize." + fs.Text, _editor.Culture);
					if(resourceText != null)
					{
						text = resourceText;
					}
				}
				_btnFontSizes.Buttons.Add(new C1RibbonRadioButton(fs.Name, text, fs.Tooltip, string.Empty, this.OwnerID + "_fontsize"));
			}
		}
		#endregion end of ** internal methods.
		#endregion end of ** methods.
	}
}