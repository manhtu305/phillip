using System;
using System.ComponentModel;
using C1.Web.Wijmo.Controls.Localization;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace C1.Web.Wijmo.Controls.C1Editor
{

	/// <summary>
	/// Represents the C1RibbonRadioButton to specify the radio button information of the ribbon for C1Editor.
	/// </summary>
	public class C1RibbonRadioButton : C1RibbonToggleButton
	{

		#region ** constructors
		/// <summary>
		/// The constructor for the C1RibbonRadioButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		/// <param name="group">
		/// The group that the ribbon button belongs to.
		/// </param>
		public C1RibbonRadioButton(string name, string tooltip, string iconClass, string group)
			: this(name, string.Empty, tooltip, iconClass, group)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonRadioButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		/// <param name="group">
		/// The group that the ribbon button belongs to.
		/// </param>
		public C1RibbonRadioButton(string name, string text,
			string tooltip, string iconClass, string group)
			: this(name, text, tooltip, string.Empty, iconClass, group)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonRadioButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		/// <param name="group">
		/// The group that the ribbon button belongs to.
		/// </param>
		public C1RibbonRadioButton(string name, string text, string tooltip,
			string cssClass, string iconClass, string group)
			: this(name, text, tooltip, cssClass,
				iconClass, TextImageRelation.ImageBeforeText, group)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonRadioButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		/// <param name="textImageRelation">
		/// The textImageRelation of the ribbon button.
		/// </param>
		/// <param name="group">
		/// The group that the ribbon button belongs to.
		/// </param>
		public C1RibbonRadioButton(string name, string text, string tooltip,
			string cssClass, string iconClass,
			TextImageRelation textImageRelation, string group)
			: this(name, text, tooltip, string.Empty, cssClass,
				iconClass, textImageRelation, group)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonRadioButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="value">
		/// The value of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		/// <param name="textImageRelation">
		/// The textImageRelation of the ribbon button.
		/// </param>
		/// <param name="group">
		/// The group that the ribbon button belongs to.
		/// </param>
		public C1RibbonRadioButton(string name, string text, string tooltip, string value,
			string cssClass, string iconClass,
			TextImageRelation textImageRelation, string group)
			: this(name, text, tooltip, value, cssClass,
				iconClass, textImageRelation, false, group)
		{
		}

		/// <summary>
		/// The constructor for the <seealso cref="C1RibbonRadioButton"/>
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="value">
		/// The value of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		/// <param name="textImageRelation">
		/// The textImageRelation of the ribbon button.
		/// </param>
		/// <param name="isChecked">
		/// The checked state of the ribbon button.
		/// </param>
		/// <param name="group">
		/// The group that the ribbon button belongs to.
		/// </param>
		public C1RibbonRadioButton(string name, string text, string tooltip,
			string value, string cssClass, string iconClass,
			TextImageRelation textImageRelation, bool isChecked, string group) :
			base(name, text, tooltip, value, cssClass, iconClass,
				textImageRelation, isChecked)
		{
			this.Group = group;
		}
		#endregion end of ** constructors.

		#region ** properties
		/// <summary>
		/// Gets or sets a value to indicate which group this button belongs to.
		/// </summary>
		public string Group
		{
			get
			{
				return this.GetPropertyValue<string>("Group", "");
			}
			set
			{
				this.SetPropertyValue<string>("Group", value);
			}
		}
		#endregion end of ** properties.

		#region ** methods

		/// <summary>
		/// Create radio input HTML control for C1RibbonRadioButton.
		/// </summary>
		/// <returns></returns>
		protected override HtmlGenericControl CreateInput()
		{
			HtmlGenericControl input = base.CreateInput() as HtmlGenericControl;

			if (input == null)
			{
				return base.CreateInput();
			}

			input.Attributes["type"] = "radio";
			input.Attributes["name"] = this.Group;

			return input;
		}
		#endregion end of ** methods.
	}
}
