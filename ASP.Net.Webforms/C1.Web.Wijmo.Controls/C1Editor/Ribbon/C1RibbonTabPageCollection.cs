using System;
using System.Collections;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Editor
{

	/// <summary>
	/// C1EditorTabPageCollection
	/// </summary>
	public class C1RibbonTabPageCollection : CollectionBase
	{

		/// <summary>
		/// constructor
		/// </summary>
		public C1RibbonTabPageCollection()
		{
		}

		/// <summary>
		/// Add TabPage to collection.
		/// </summary>
		/// <param name="aTabPage">The specified C1EditorTabPage object.</param>
		public void Add(C1RibbonTabPage aTabPage)
		{
			this.List.Add(aTabPage);
		}

		/// <summary>
		/// Remove TabPage from collection.
		/// </summary>
		/// <param name="aTabPage">The specified C1EditorTabPage object.</param>
		public void Remove(C1RibbonTabPage aTabPage)
		{
			this.List.Remove(aTabPage);
		}

		/// <summary>
		/// Get C1EditorTabPage by index of collection.
		/// </summary>
		/// <param name="Index">The specified index.</param>
		/// <returns>Return C1EditorTabPage object.</returns>
		public C1RibbonTabPage this[int Index]
		{
			get
			{
				if (Index >= 0 && Index < Count)
				{
					return (C1RibbonTabPage)this.List[Index];
				}
				return null;
			}
			set
			{
				this.List[Index] = value;
			}
		}

		/// <summary>
		/// Remove C1EditorTabPage by key name of collection.
		/// </summary>
		/// <param name="Name">The specified key.</param>
		public void Remove(string Name)
		{
			for (int i = Count - 1; i >= 0; i--)
			{
				C1RibbonTabPage tab = (C1RibbonTabPage)this.List[i];
				if (tab.Name.ToLower() == Name.ToLower())
				{
					this.List.Remove(tab);
					break;
				}
			}
		}

		/// <summary>
		/// Get C1EditorTabPage by key name of collection.
		/// </summary>
		/// <param name="Name">The specified key.</param>
		/// <returns>Return C1EditorTabPage object.</returns>
		public C1RibbonTabPage this[string Name]
		{
			get
			{
				for (int i = 0; i < Count; i++)
				{
					C1RibbonTabPage tab = (C1RibbonTabPage)this.List[i];
					if (tab.Name.ToLower() == Name.ToLower())
					{
						return tab;
					}
				}

				return null;
			}
		}
	}
}
