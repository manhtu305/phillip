using System;
using System.ComponentModel;
using C1.Web.Wijmo.Controls.Localization;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace C1.Web.Wijmo.Controls.C1Editor
{

	/// <summary>
	/// Represents the C1RibbonSplitButton to group some buttons for C1Editor.
	/// </summary>
	public class C1RibbonSplitButton : C1RibbonDropDownListButton
	{

		#region ** fields
		private const string CONST_SPLITBUTTON_CSS = "wijmo-wijribbon-splitbutton";
		#endregion end of ** fields.

		#region ** constructors
		public C1RibbonSplitButton()
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonSplitButton.
		/// </summary>		
		/// <param name="tag">
		/// The tag key of the ribbon button.
		/// </param>
		public C1RibbonSplitButton(HtmlTextWriterTag tag)
			: base(tag)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonSplitButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		public C1RibbonSplitButton(string name, string tooltip, string iconClass)
			: this(name, string.Empty, tooltip, iconClass)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonSplitButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		public C1RibbonSplitButton(string name, string text, 
			string tooltip, string iconClass)
			: this(name, text, tooltip, string.Empty, iconClass)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonSplitButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		public C1RibbonSplitButton(string name, string text, 
			string tooltip, string cssClass, string iconClass)
			: this(name, text, tooltip, string.Empty, cssClass,
				iconClass, TextImageRelation.TextBeforeImage)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonSplitButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		/// <param name="textImageRelation">
		/// The textImageRelation of the ribbon button.
		/// </param>
		public C1RibbonSplitButton(string name, string text,
			string tooltip, string cssClass, string iconClass, TextImageRelation textImageRelation)
			: this(name, text, tooltip, string.Empty, cssClass,
				iconClass, textImageRelation)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonSplitButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="value">
		/// The value of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		/// <param name="textImageRelation">
		/// The textImageRelation of the ribbon button.
		/// </param>
		public C1RibbonSplitButton(string name, string text,
			string tooltip, string value, string cssClass, string iconClass,
			TextImageRelation textImageRelation)
			: this(name, text, tooltip, value, cssClass,
				iconClass, textImageRelation, HtmlTextWriterTag.Div)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonSplitButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="value">
		/// The value of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		/// <param name="textImageRelation">
		/// The textImageRelation of the ribbon button.
		/// </param>
		/// <param name="tag">
		/// The tag key of the ribbon button.
		/// </param>
		public C1RibbonSplitButton(string name, string text, 
			string tooltip, string value, string cssClass, string iconClass,
			TextImageRelation textImageRelation, HtmlTextWriterTag tag)
			: base(name, text, tooltip, value, cssClass,
				iconClass, textImageRelation, tag)
		{
		}
		#endregion end of ** constructors.

		#region ** methods

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that 
		/// use composition-based implementation to create any child controls 
		/// they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			this.Controls.Clear();

			this.Controls.Add(new C1RibbonButton(this.Name, this.Text, this.ToolTip, this.CssClass, this.IconClass, this.TextImageRelation));
			HtmlButton trigger = new HtmlButton();
			trigger.Attributes["class"] = this.CssClass;
			this.Controls.Add(trigger);

			if (!this.IsDesignMode && this.Buttons.Count > 0)
			{
				HtmlGenericControl ul = new HtmlGenericControl("ul");
				this.Controls.Add(ul);

				foreach (C1RibbonButton ribbonButton in this.Buttons)
				{
					HtmlGenericControl li = new HtmlGenericControl("li");
					ul.Controls.Add(li);
					li.Controls.Add(ribbonButton);
				}
			}
		}

		/// <summary>
		/// Gets the compound css class.
		/// </summary>
		/// <returns>Returns the css string.</returns>
		protected override string GetCompoundCss()
		{
			return CONST_SPLITBUTTON_CSS;
		}
		#endregion end of ** methods.
	}
}
