using System;
using System.ComponentModel;
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.Base;
using System.Web.UI.HtmlControls;

namespace C1.Web.Wijmo.Controls.C1Editor
{

	/// <summary>
	/// C1EditorTabPage
	/// </summary>
	public class C1RibbonTabPage
	{

		private string _Name = string.Empty;
		private string _Text = string.Empty;
		private C1RibbonGroupCollection _groups;

		/// <summary>
		/// Name property
		/// </summary>
		[NotifyParentProperty(true)]
		public string Name
		{
			get { return _Name; }
			set { _Name = value; }
		}

		/// <summary>
		/// Text
		/// </summary>
		[NotifyParentProperty(true)]
		public string Text
		{
			get { return _Text; }
			set { _Text = value; }
		}

		/// <summary>
		/// Groups property
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		public C1RibbonGroupCollection Groups
		{
			get
			{
				if (_groups == null)
				{
					_groups = new C1RibbonGroupCollection();
				}

				return _groups;
			}
		}
	}
}
