using System;
using System.Collections;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Editor
{

	/// <summary>
	/// Represents the C1RibbonButtonCollection class to save 
	/// all the instances of the <see cref=" C1RibbonButton"/> class.
	/// </summary>
	public class C1RibbonButtonCollection : CollectionBase
	{

		/// <summary>
		/// Add a ribbon button to the collection.
		/// </summary>
		/// <param name="button">
		/// The button that will be added to the collection.
		/// </param>
		public void Add(C1RibbonButton button)
		{
			this.List.Add(button);
		}

		/// <summary>
		/// Remove a ribbon button from the collection.
		/// </summary>
		/// <param name="button">
		/// The button that will be removed from the collection.
		/// </param>
		public void Remove(C1RibbonButton button)
		{
			this.Remove(button);
		}

		/// <summary>
		/// Remove a ribbon button from the collection by the specified name.
		/// </summary>
		/// <param name="name">
		/// The name of the button that will be removed from the collection.
		/// </param>
		public void Remove(string name)
		{
			foreach (C1RibbonButton button in this.List)
			{
				if (button.Name == name)
				{
					this.List.Remove(button);
					break;
				}
			}
		}

		/// <summary>
		/// Gets or sets the button by the specified index.
		/// </summary>
		/// <param name="index">
		/// The specified index.
		/// </param>
		/// <returns>
		/// Returns the specified button by the index.
		/// </returns>
		public C1RibbonButton this[int index]
		{
			get
			{
				if (index >= 0 && index < this.Count)
				{
					return (C1RibbonButton)this.List[index];
				}

				return null;
			}
			set
			{
				this.List[index] = value;
			}
		}

		/// <summary>
		/// Gets the button by the name of the button.
		/// </summary>
		/// <param name="name">
		/// The specified name.
		/// </param>
		/// <returns>
		/// Returns the specified button by the name.
		/// </returns>
		public C1RibbonButton this[string name]
		{
			get
			{
				foreach (C1RibbonButton button in this.List)
				{
					if (button.Name == name)
					{
						return button;
					}
				}

				return null;
			}
		}
	}
}
