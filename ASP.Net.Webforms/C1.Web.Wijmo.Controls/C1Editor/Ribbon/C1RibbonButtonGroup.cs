using System;
using System.ComponentModel;
using C1.Web.Wijmo.Controls.Localization;
using System.Web.UI;

namespace C1.Web.Wijmo.Controls.C1Editor
{

	/// <summary>
	/// Represents the C1RibbonButtonGroup to group some buttons for C1Editor.
	/// </summary>
	public class C1RibbonButtonGroup : C1RibbonButton
	{

		#region ** fields
		private C1RibbonButtonCollection _buttons = null;

		private const string CONST_BUTTONGROUP_CSS = "wijmo-wijribbon-list";
		#endregion end of ** fields.

		#region ** constructors
		public C1RibbonButtonGroup() : this(HtmlTextWriterTag.Span)
		{
		}

		/// <param name="tag">
		/// The tag key of the ribbon button.
		/// </param>
		public C1RibbonButtonGroup(HtmlTextWriterTag tag)
			: base(tag)
		{
		}
		#endregion end of ** constructors.

		#region ** properties
		#region ** public properties
		/// <summary>
		/// A <seealso cref="C1RibbonButtonCollection"/> instance 
        /// which saves all the button information.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public C1RibbonButtonCollection Buttons
		{
			get
			{
				if (this._buttons == null)
				{
					this._buttons = new C1RibbonButtonCollection();
				}

				return this._buttons;
			}
		}
		#endregion end of ** public properties.

		#region ** override properties
		/// <summary>
		/// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
		/// server control. This property is used primarily by control developers.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Span;
			}
		}
		#endregion end of ** override properties.
		#endregion end of ** properties.

		#region ** override methods

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that 
		/// use composition-based implementation to create any child controls 
		/// they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			this.Controls.Clear();

			foreach (C1RibbonButton ribbonButton in this.Buttons)
			{
				this.Controls.Add(ribbonButton);
			}
		}

		/// <summary>
		/// Gets the compound css class.
		/// </summary>
		/// <returns>Returns the css string.</returns>
		protected override string GetCompoundCss()
		{
			if (this.IsDesignMode)
			{
				return string.Format("{0} {1}", CONST_BUTTONGROUP_CSS, "ui-buttonset");
			}

			return CONST_BUTTONGROUP_CSS;
		}
		#endregion end of ** override methods.
	}
}
