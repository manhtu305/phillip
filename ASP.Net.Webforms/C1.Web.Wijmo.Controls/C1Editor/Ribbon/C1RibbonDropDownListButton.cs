using System;
using System.ComponentModel;
using C1.Web.Wijmo.Controls.Localization;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace C1.Web.Wijmo.Controls.C1Editor
{

	/// <summary>
	/// Represents the C1RibbonButtonGroup to group some buttons for C1Editor.
	/// </summary>
	public class C1RibbonDropDownListButton : C1RibbonButton
	{

		#region ** fields
		private C1RibbonButtonCollection _buttons = null;

		private const string CONST_DROPDOWNLISTBUTTON_CSS = "wijmo-wijribbon-dropdownbutton";
		#endregion end of ** fields.

		#region ** constructors
		public C1RibbonDropDownListButton()
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonDropDownListButton.
		/// </summary>		
		/// <param name="tag">
		/// The tag key of the ribbon button.
		/// </param>
		public C1RibbonDropDownListButton(HtmlTextWriterTag tag)
			: base(tag)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonDropDownListButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		public C1RibbonDropDownListButton(string name, string tooltip, string iconClass)
			: this(name, string.Empty, tooltip, iconClass)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonDropDownListButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		public C1RibbonDropDownListButton(string name, string text, 
			string tooltip, string iconClass)
			: this(name, text, tooltip, string.Empty, iconClass)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonDropDownListButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		public C1RibbonDropDownListButton(string name, string text, 
			string tooltip, string cssClass, string iconClass)
			: this(name, text, tooltip, string.Empty, cssClass,
				iconClass, TextImageRelation.TextBeforeImage)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonDropDownListButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="value">
		/// The value of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		/// <param name="textImageRelation">
		/// The textImageRelation of the ribbon button.
		/// </param>
		public C1RibbonDropDownListButton(string name, string text,
			string tooltip, string value, string cssClass, string iconClass,
			TextImageRelation textImageRelation)
			: this(name, text, tooltip, value, cssClass,
				iconClass, textImageRelation, HtmlTextWriterTag.Div)
		{
		}

		/// <summary>
		/// The constructor for the C1RibbonDropDownListButton.
		/// </summary>
		/// <param name="name">
		/// The name of the ribbon button.
		/// </param>
		/// <param name="text">
		/// The text of the ribbon button.
		/// </param>
		/// <param name="tooltip">
		/// The tooltip of the ribbon button.
		/// </param>
		/// <param name="value">
		/// The value of the ribbon button.
		/// </param>
		/// <param name="cssClass">
		/// The css class of the ribbon button.
		/// </param>
		/// <param name="iconClass">
		/// The iconClass of the ribbon button.
		/// </param>
		/// <param name="textImageRelation">
		/// The textImageRelation of the ribbon button.
		/// </param>
		/// <param name="tag">
		/// The tag key of the ribbon button.
		/// </param>
		public C1RibbonDropDownListButton(string name, string text, 
			string tooltip, string value, string cssClass, string iconClass,
			TextImageRelation textImageRelation, HtmlTextWriterTag tag)
			: base(name, text, tooltip, value, cssClass,
				iconClass, textImageRelation, tag)
		{
		}
		#endregion end of ** constructors.

		#region ** properties
		#region ** public properties
		/// <summary>
		/// A <seealso cref="C1RibbonButtonCollection"/> instance 
        /// which saves all the button information.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		public C1RibbonButtonCollection Buttons
		{
			get
			{
				if (this._buttons == null)
				{
					this._buttons = new C1RibbonButtonCollection();
				}

				return this._buttons;
			}
		}
		#endregion end of ** public properties.

		#region ** override properties

		/// <summary>
		/// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
		/// server control. This property is used primarily by control developers.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}
		#endregion end of ** override properties.
		#endregion end of ** properties.

		#region ** methods

		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that 
		/// use composition-based implementation to create any child controls 
		/// they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			this.Controls.Clear();
			
			if (!this.IsDesignMode)
			{
				C1RibbonButton button = new C1RibbonButton(this.Name, this.Text, this.ToolTip, this.CssClass, this.IconClass, this.TextImageRelation);
				this.Controls.Add(button);

				if (this.Buttons.Count > 0)
				{
					HtmlGenericControl ul = new HtmlGenericControl("ul");
					this.Controls.Add(ul);

					foreach (C1RibbonButton ribbonButton in this.Buttons)
					{
						HtmlGenericControl li = new HtmlGenericControl("li");
						ul.Controls.Add(li);
						li.Controls.Add(ribbonButton);
					}
				}
			}
			else
			{
				HtmlGenericControl span = new HtmlGenericControl("span");
				span.Attributes["class"] = "ui-button ui-widget ui-state-default ui-button-text-icon-secondary";
				span.Style["width"] = "auto";
				this.Controls.Add(span);

				HtmlGenericControl txtSpan = new HtmlGenericControl("span");
				txtSpan.Attributes["class"] = "ui-button-text";
				txtSpan.InnerText = string.IsNullOrEmpty(this.Text) ? this.Name : this.Text;
				span.Controls.Add(txtSpan);

				HtmlGenericControl triggerSpan = new HtmlGenericControl("span");
				triggerSpan.Attributes["class"] = "ui-icon-triangle-1-s ui-button-icon-secondary ui-icon";
				span.Controls.Add(triggerSpan);
			}
		}

		/// <summary>
		/// Gets the compound css class.
		/// </summary>
		/// <returns>Returns the css string.</returns>
		protected override string GetCompoundCss()
		{
			return CONST_DROPDOWNLISTBUTTON_CSS;
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to
		/// the specified <see cref="HtmlTextWriterTag"/>.
		/// </summary>
		/// <param name="writer">A <see cref="HtmlTextWriter"/>that represents
		/// the output stream to render HTML content on the client.
		/// </param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			this.EnsureChildControls();

			string oldCss = this.CssClass;
			string oldName = this.Name;

			this.CssClass = string.Empty;
			this.Name = string.Empty;

			base.AddAttributesToRender(writer);

			this.CssClass = oldCss;
			this.Name = oldName;
		}
		#endregion end of ** methods.
	}
}
