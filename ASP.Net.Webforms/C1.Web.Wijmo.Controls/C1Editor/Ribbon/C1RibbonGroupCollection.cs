using System;
using System.Collections;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Editor
{

	/// <summary>
	/// C1RibbonGroupCollection
	/// </summary>
	public class C1RibbonGroupCollection : CollectionBase
	{

		/// <summary>
		/// Add C1RibbonGroup to collection.
		/// </summary>
		/// <param name="aTabPage">The specified object.</param>
		public void Add(C1RibbonGroup aTabPage)
		{
			this.List.Add(aTabPage);
		}

		/// <summary>
		/// Remove C1RibbonGroup from collection.
		/// </summary>
		/// <param name="aTabPage">The specified C1RibbonGroup object.</param>
		public void Remove(C1RibbonGroup aTabPage)
		{
			this.Remove(aTabPage);
		}

		/// <summary>
		/// Get C1RibbonGroup by index of collection.
		/// </summary>
		/// <param name="Index">The specified index.</param>
		/// <returns>Return C1RibbonGroup object.</returns>
		public C1RibbonGroup this[int Index]
		{
			get
			{
				if (Index >= 0 && Index < Count)
				{
					return (C1RibbonGroup)this.List[Index];
				}

				return null;
			}
			set
			{
				this.List[Index] = value;
			}
		}

		/// <summary>
		/// Remove C1RibbonGroup from collection by key name.
		/// </summary>
		/// <param name="Name">The specified key.</param>
		public void Remove(string Name)
		{
			for (int i = Count - 1; i >= 0; i--)
			{
				C1RibbonGroup group = (C1RibbonGroup)this.List[i];

				if (group.Name.ToLower() == Name.ToLower())
				{
					this.List.Remove(group);
					break;
				}
			}
		}

		/// <summary>
		/// Get C1RibbonGroup by key of collection.
		/// </summary>
		/// <param name="Name">The specified key.</param>
		/// <returns>Return C1RibbonGroup object.</returns>
		public C1RibbonGroup this[string Name]
		{
			get
			{
				for (int i = 0; i < Count; i++)
				{
					C1RibbonGroup group = (C1RibbonGroup)this.List[i];

					if (group.Name.ToLower() == Name.ToLower())
					{
						return group;
					}
				}

				return null;
			}
		}
	}
}
