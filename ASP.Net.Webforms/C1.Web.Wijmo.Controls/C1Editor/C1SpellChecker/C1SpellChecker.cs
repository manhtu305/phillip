using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Diagnostics;
using System.Reflection;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Collections.Specialized;
using C1.Web.Wijmo.Controls.Localization;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using C1.Web.Wijmo.Controls;
using System.Globalization;

namespace C1.Web.Wijmo.Controls.C1Editor.C1SpellChecker
{

	/// <summary>
	/// Represents the C1SpellChecker to check the specified text.
	/// </summary>
	//[ToolboxData("<{0}:C1SpellChecker runat=\"server\"></{0}:C1SpellChecker>")]
    [WidgetDependencies(
        typeof(C1Dialog.C1Dialog),
        "extensions.c1spellchecker.js",
		"extensions.c1spellchecker.css"
    )]
	[ToolboxItem(false)]
	public class C1SpellChecker : C1TargetControlBase, INamingContainer, ICallbackEventHandler
	{

		#region ** fields
		private string _fParams = "";
		private C1SpellCheckerComponent _spellChecker;
		private static readonly Guid DesignTimeEnvironmentCLSID = new Guid(0x4a72314, 0x32e9, 0x48e2, 0x9b, 0x87, 0xa6, 0x36, 3, 0x45, 0x4f, 0x3e);
		private LocalizationOption _LocalizationOption;
		private CultureInfo _culture;
		#endregion end of ** fields.

		#region ** constructors
		/// <summary>
		/// C1SpellChecker
		/// </summary>
		public C1SpellChecker()
			: this(null)
		{
		}

		internal C1SpellChecker(CultureInfo culture)
		{
			_culture = culture;
		}
		#endregion end of ** constructors.

		#region ** public properties
		/// <summary>
		/// SpellChecker dictionary path.
		/// </summary>
		/// <example>
		/// Example of the relative path: "~/Dictionaries/C1Spell_de-DE.dct"
		/// </example>
		[DefaultValue("")]
		[C1Description("C1Editor.DictionaryPath")]
		[C1Category("Category.Data")]
		public virtual string DictionaryPath
		{
			get
			{
				return this.GetPropertyValue<string>("DictionaryPath", string.Empty);
			}
			set
			{
				this.SetPropertyValue<string>("DictionaryPath", value);
			}
		}

		/// <summary>
		/// Use the Localization property in order to customize localization strings.
		/// </summary>
        [C1Category("Category.Appearance")]
		[C1Description("C1SpellChecker.Localization",
				"Use the Localization property in order to customize localization strings.")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		//[Layout(LayoutType.Appearance)]
		[WidgetOption()]
		[Browsable(false)]
		public LocalizationOption Localization
		{
			get
			{
				if (_LocalizationOption == null)
				{
					_LocalizationOption = new LocalizationOption(this);
				}
				return _LocalizationOption;
			}
		}
		#endregion end of ** public properties.

		#region ** private properties
		internal C1SpellCheckerComponent SpellChecker
		{
			get
			{
				if (_spellChecker == null)
				{
					_spellChecker = new C1SpellCheckerComponent();
				}

				return _spellChecker;
			}
		}

		internal CultureInfo Culture
		{
			get
			{
				return _culture;
			}
		}
		#endregion end of ** private properties.

		#region ** protected properties
		/// <summary>
		/// Changed the type of control's tag.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

		//add for fixing issue 20370 by wh at 2011/3/9
		/// <summary>
		/// Gets the uniqueID of this control
		/// </summary>
		[Json(true)]
		[Browsable(false)]
		public override string UniqueID
		{
			get
			{
				return base.UniqueID;
			}
		}
		//end for 20370
		#endregion end of ** protected properties.

		#region ** private methods
		private string DoSpellCheck(string sTextToCheck)
		{
			string sDct = Page.MapPath(this.DictionaryPath);
			CharRangeList aRangeList = null;

			try
			{
                // fixed an issue: when user set DictionaryPath, it has been set to
                // UserDictionary, UserDictionary is an complementary dictionary for MainDictionary
                // TODO: Maybe need to add an property "UserDictionaryPath" for user

                /**
                if (!File.Exists(sDct))
                {
                    sDct = string.Empty;
                    SpellChecker.MainDictionary.FileName = sDct;
                }
                else
                {
                    SpellChecker.UserDictionary.FileName = sDct;
                }*/

                if (File.Exists(sDct))
                {
                    SpellChecker.MainDictionary.FileName = sDct;
                }

				aRangeList = SpellChecker.CheckText(sTextToCheck);
			}
			catch (Exception ex)
			{
				return "[{\"type_name\":\"error\",\"message\":\"" + ex.Message + "\"}]";
			}

			if (aRangeList.Count == 0)
			{
				// No errors:
				return "[{\"type_name\":\"no_spell_errors\"}]";
			}
			else
			{
				string s = "[";
				// Errors:
				for (int i = 0; i < aRangeList.Count; i++)
				{
					bool aDuplicate = aRangeList[i].Duplicate;
					int aStart = aRangeList[i].Start;
					int aEnd = aRangeList[i].End;
					int aLength = aRangeList[i].Length;
					string sText = aRangeList[i].Text;
					string[] aSuggestions = SpellChecker.GetSuggestions(sText);
					string sSgsts = "[";
					for (int j = 0; j < aSuggestions.Length; j++)
					{
						if (j > 0)
							sSgsts = sSgsts + ",";
						sSgsts = sSgsts + "\"" + UTFSupport.EncodeString(aSuggestions[j]) + "\"";
					}
					sSgsts = sSgsts + "]";
					string sErr =
						"{"
						+ "\"type_name\":\"word_error\""
						+ ",\"duplicate\":" + aDuplicate.ToString().ToLower() + ""
						+ ",\"start\":" + aStart.ToString()
						+ ",\"end\":" + aEnd.ToString()
						+ ",\"length\":" + aLength.ToString()
						+ ",\"text\":\"" + UTFSupport.EncodeString(sText) + "\""
						+ ",\"suggestions\":" + sSgsts
						+ "}";
					if (i > 0)
						s = s + ",";
					s = s + sErr;
				}
				s = s + "]";
				return s;
			}
		}
		#endregion end of ** private methods.

		#region ** override methods
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			Page.ClientScript.GetCallbackEventReference(this, "", "", "");
		}
		#endregion end of ** override methods.

		#region ** the ICallbackEventHandler interface implementations
		/// <summary>
		/// Returns the results of a callback event that targets a <seealso cref="C1SpellChecker"/>.
		/// </summary>
		/// <returns>
		/// The result of the callback.
		/// </returns>
		string ICallbackEventHandler.GetCallbackResult()
		{
			if (_fParams.StartsWith("CheckText|"))
			{
				string txt = _fParams.Substring(10, _fParams.Length - 10);
				string sText = UTFSupport.DecodeString(txt);

				sText = sText.Replace("&nbsp;", "      ");

				return DoSpellCheck(sText);
			}
			else if (_fParams.StartsWith("AddWord2Dic|"))
			{
				string word = _fParams.Substring(12, _fParams.Length - 12);

				return this.AddWord2Dictionary(word);
			}

			return "[{\"type_name\":\"error\",\"message\":\"wrong request\"}]";
		}

		private string AddWord2Dictionary(string word)
		{
			if (this.SpellChecker == null || this.SpellChecker.UserDictionary == null)
			{
				return "[AddWord]" + C1Localizer.GetString("C1SpellChecker.Add2Dic.OperateFailed", Culture);
			}

			string sDct = Page.MapPath(this.ResolveClientUrl(this.DictionaryPath));

            // fixed an issue: when user set DictionaryPath, it has been set to
            // UserDictionary, UserDictionary is an complementary dictionary for MainDictionary
            // TODO: Maybe need to add an property "UserDictionaryPath" for user            
            /**
            if (File.Exists(sDct))
            {
                this.SpellChecker.UserDictionary.FileName = sDct;
            }*/

			try
			{
				this.SpellChecker.UserDictionary.AddWord(word);
			}
			catch
			{
				return "[AddWord]" + C1Localizer.GetString("C1SpellChecker.Add2Dic.OperateFailed", Culture);
			}

			return "[AddWord]" + C1Localizer.GetString("C1SpellChecker.Add2Dic.AddedSuccessfully", Culture);
		}

		/// <summary>
		/// Processes a callback event that targets a <seealso cref="C1SpellChecker"/>.
		/// </summary>
		/// <param name="eventArgument">
		/// A string that represents an event argument to pass to the event handler.
		/// </param>
		void ICallbackEventHandler.RaiseCallbackEvent(string eventArgument)
		{
			_fParams = eventArgument;
		}
		#endregion end of ** the ICallbackEventHandler interface implementations.
	}

	/// <summary>
	/// The localization of the editor string
	/// </summary>
	public class LocalizationOption : Settings, IJsonEmptiable
	{
		C1SpellChecker _spellchecker;

		/// <summary>
		/// Initializes a new instance of the <see cref="LocalizationOption"/> class.
		/// </summary>
		public LocalizationOption(C1SpellChecker spellchecker)
		{
			_spellchecker = spellchecker;
		}

		#region ** localization string properties

		/// <summary>
		/// Add2DicAddedSuccessfully string.
		/// </summary>
		[DefaultValue("Added successfully.")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string Add2DicAddedSuccessfully
		{
			get
			{
				return GetPropertyValue<string>("Add2DicAddedSuccessfully",
					C1Localizer.GetString("C1SpellChecker.Add2Dic.AddedSuccessfully",
					"Added successfully.", _spellchecker.Culture));
			}
			set
			{
				SetPropertyValue<string>("Add2DicAddedSuccessfully", value);
			}
		}


		/// <summary>
		/// Add2DicOperateFailed string.
		/// </summary>
		[DefaultValue("Operate failed.")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string Add2DicOperateFailed
		{
			get
			{
				return GetPropertyValue<string>("Add2DicOperateFailed",
					C1Localizer.GetString("C1SpellChecker.Add2Dic.OperateFailed",
					"Operate failed.", _spellchecker.Culture));
			}
			set
			{
				SetPropertyValue<string>("Add2DicOperateFailed", value);
			}
		}

		/// <summary>
		/// Add2Dictionary string.
		/// </summary>
		[DefaultValue("Add to Dictionary")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string Add2Dictionary
		{
			get
			{
				return GetPropertyValue<string>("Add2Dictionary",
					C1Localizer.GetString("C1SpellChecker.Add2Dictionary",
					"Add to Dictionary", _spellchecker.Culture));
			}
			set
			{
				SetPropertyValue<string>("Add2Dictionary", value);
			}
		}

		/// <summary>
		/// Change string.
		/// </summary>
		[DefaultValue("Change")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string Change
		{
			get
			{
				return GetPropertyValue<string>("Change",
					C1Localizer.GetString("C1SpellChecker.Change",
					"Change", _spellchecker.Culture));
			}
			set
			{
				SetPropertyValue<string>("Change", value);
			}
		}

		/// <summary>
		/// ChangeAll string.
		/// </summary>
		[DefaultValue("ChangeAll")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ChangeAll
		{
			get
			{
				return GetPropertyValue<string>("ChangeAll",
					C1Localizer.GetString("C1SpellChecker.ChangeAll",
					"Change", _spellchecker.Culture));
			}
			set
			{
				SetPropertyValue<string>("ChangeAll", value);
			}
		}

		/// <summary>
		/// ChangeTo string.
		/// </summary>
		[DefaultValue("Change To:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ChangeTo
		{
			get
			{
				return GetPropertyValue<string>("ChangeTo",
					C1Localizer.GetString("C1SpellChecker.ChangeTo",
					"Change To:", _spellchecker.Culture));
			}
			set
			{
				SetPropertyValue<string>("ChangeTo", value);
			}
		}

		/// <summary>
		/// Ignore string.
		/// </summary>
		[DefaultValue("Ignore")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string Ignore
		{
			get
			{
				return GetPropertyValue<string>("Ignore",
					C1Localizer.GetString("C1SpellChecker.Ignore",
					"Ignore", _spellchecker.Culture));
			}
			set
			{
				SetPropertyValue<string>("Ignore", value);
			}
		}

		/// <summary>
		/// IgnoreAll string.
		/// </summary>
		[DefaultValue("Ignore All")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string IgnoreAll
		{
			get
			{
				return GetPropertyValue<string>("IgnoreAll",
					C1Localizer.GetString("C1SpellChecker.IgnoreAll",
					"Ignore All", _spellchecker.Culture));
			}
			set
			{
				SetPropertyValue<string>("IgnoreAll", value);
			}
		}

		/// <summary>
		/// NotInDictionary string.
		/// </summary>
		[DefaultValue("Not in Dictionary:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string NotInDictionary
		{
			get
			{
				return GetPropertyValue<string>("NotInDictionary",
					C1Localizer.GetString("C1SpellChecker.NotInDictionary",
					"Not in Dictionary:", _spellchecker.Culture));
			}
			set
			{
				SetPropertyValue<string>("NotInDictionary", value);
			}
		}

		/// <summary>
		/// Suggestions string.
		/// </summary>
		[DefaultValue("Suggestions:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string Suggestions
		{
			get
			{
				return GetPropertyValue<string>("Suggestions",
					C1Localizer.GetString("C1SpellChecker.Suggestions",
					"Suggestions:", _spellchecker.Culture));
			}
			set
			{
				SetPropertyValue<string>("Suggestions", value);
			}
		}

		
		/// <summary>
		/// Suggestions string.
		/// </summary>
		[DefaultValue("Spell Checker")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string Text
		{
			get
			{
				return GetPropertyValue<string>("Text",
					C1Localizer.GetString("C1SpellChecker.Text",
					"Spell Checker", _spellchecker.Culture));
			}
			set
			{
				SetPropertyValue<string>("Text", value);
			}
		}

		/// <summary>
		/// NoErrors string.
		/// </summary>
		[DefaultValue("No errors.")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string NoErrors
		{
			get
			{
				return GetPropertyValue<string>("NoErrors",
					C1Localizer.GetString("C1SpellChecker.NoErrors",
					"No errors.", _spellchecker.Culture));
			}
			set
			{
				SetPropertyValue<string>("NoErrors", value);
			}
		}

		/// <summary>
		/// SpellCheckDone string.
		/// </summary>
		[DefaultValue("Spell checking done.")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string SpellCheckDone
		{
			get
			{
				return GetPropertyValue<string>("NoErrors",
					C1Localizer.GetString("C1SpellChecker.SpellCheckDone",
					"Spell checking done.", _spellchecker.Culture));
			}
			set
			{
				SetPropertyValue<string>("SpellCheckDone", value);
			}
		}

		/// <summary>
		/// Cancel string.
		/// </summary>
		[DefaultValue("Cancel")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string Cancel
		{
			get
			{
				return GetPropertyValue<string>("Cancel",
					C1Localizer.GetString("C1SpellChecker.Cancel",
					"Cancel", _spellchecker.Culture));
			}
			set
			{
				SetPropertyValue<string>("Cancel", value);
			}
		}
		#endregion

		#region ** implement ICustomOptionType interface

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return base.ToString();
		}

		internal bool ShouldSerialize()
		{
			return true;
		}

		#endregion end of ** IJsonEmptiable interface implementation


		bool IJsonEmptiable.IsEmpty
		{
			get { return !this.ShouldSerialize(); }
		}
	}
}
