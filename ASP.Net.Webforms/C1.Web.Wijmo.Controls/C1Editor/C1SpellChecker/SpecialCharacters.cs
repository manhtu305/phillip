using System;
using System.Collections.Generic;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Editor.C1SpellChecker
{

    internal class SpecialCharacters
    {

        internal SpecialCharacters()
        {
        }

        /// <summary>
        ///   Looks up a localized string similar to abcdefghijklmnopqrstuvwxyz.
        /// </summary>
        internal static string commonChars
        {
            get
            {
                return "abcdefghijklmnopqrstuvwxyz";
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to &apos;åàáâäçèéêëíîïñóôöõùúûüœæлидерыкпфгаютвянйзоьсмбучхцжшщёэ.
        /// </summary>
        internal static string specialChars
        {
            get
            {
                return "'åàáâäçèéêëíîïñóôöõùúûüœæлидерыкпфгаютвянйзоьсм";
            }
        }

        /*
        // gets an array with valid characters to create suggestions with
        private char[] GetCommonChars()
        {
            return "abcdefghijklmnopqrstuvwxyz".ToCharArray();
        }

        // gets an array with valid characters to create suggestions with
        private char[] GetSpecialChars()
        {
            return "'еабвдзийклнопсуфцхщъыьњж".ToCharArray();
        }

        */
    }
}
