using System;
using System.Collections;
using System.Windows.Forms;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.C1Editor.C1SpellChecker
{
    /// <summary>
    /// Represents the method that sell me will handle the C1SpellChecker.BadWordFound event.
    /// </summary>
    /// <param name="sender">C1SpellChecker that fired the event.</param>
    /// <param name="e">BadWordEventArgs that contains the event data.</param>
    public delegate void BadWordEventHandler(object sender, BadWordEventArgs e);

    /// <summary>
    /// Provides data for the C1SpellChecker.BadWordFound event.
    /// </summary>
    public class BadWordEventArgs : EventArgs
    {
        // fields
        Form _dialog;
        Control _control;
        CharRange _error;
        CharRangeList _errors;
        bool _cancel;

        // ctor

        /// <summary>
        /// Initializes a new instance of a <see cref="BadWordEventArgs"/>.
        /// </summary>
        /// <param name="dialog">Dialog that raised the event, or null if the event was not raised by a dialog.</param>
        /// <param name="control"><see cref="Control"/> being spell-checked.</param>
        /// <param name="error"><see cref="CharRange"/> that contains the bad word and its position within the text.</param>
        /// <param name="errors"><see cref="CharRangeList"/> that contains all the bad words found in the text.</param>
        public BadWordEventArgs(Form dialog, Control control, CharRange error, CharRangeList errors)
        {
            _dialog = dialog;
            _control = control;
            _error = error;
            _errors = errors;
        }

        // object model

        /// <summary>
        /// Gets a reference to the ISpellDialog that is being used to spell-check a control.
        /// </summary>
        /// <remarks>
        /// If this parameter is null, then no spell dialog is active and the bad word was found in 
        /// as-you-type mode.
        /// </remarks>
        public Form Dialog
        {
            get { return _dialog; }
        }
        /// <summary>
        /// Gets a reference to the control that is being spell-checked.
        /// </summary>
        public Control Control
        {
            get { return _control; }
        }
        /// <summary>
        /// Gets a <see cref="CharRange"/> object that contains the bad word and its location within 
        /// the text being spell-checked.
        /// </summary>
        public CharRange BadWord
        {
            get { return _error; }
        }
        /// <summary>
        /// Gets a <see cref="CharRangeList"/> that contains all errors detected in the text.
        /// </summary>
        /// <remarks>
        /// The <see cref="BadWord"/> property contains the specific error being handled. You can
        /// use the collection to determine the total number of errors and the index of this error
        /// within the collection.
        /// </remarks>
        public CharRangeList BadWordList
        {
            get { return _errors; }
        }
        /// <summary>
        /// Gets or sets whether this bad word should be ignored by the <see cref="C1SpellChecker"/>.
        /// </summary>
        /// <remarks>
        /// <para>If the <see cref="Dialog"/> property is not null, then the error is about to be displayed
        /// in a spell dialog. Setting the <b>Cancel</b> parameter to true in this case causes the dialog
        /// to skip to the next error.</para>
        /// <para>If the <see cref="Dialog"/> property is null, then the error was detected in as-you-type
        /// mode and the word is about to be underlined. Setting the <b>Cancel</b> parameter to true in
        /// this case prevents the word from being underlined.</para>
        /// </remarks>
        public bool Cancel
        {
            get { return _cancel; }
            set { _cancel = value; }
        }
    }
    /// <summary>
    /// Represents the method that will handle the C1SpellChecker.AutoReplace event.
    /// </summary>
    /// <param name="sender">C1SpellChecker that fired the event.</param>
    /// <param name="e">AutoReplaceEventArgs that contains the event data.</param>
    public delegate void AutoReplaceEventHandler(object sender, AutoReplaceEventArgs e);
    
    /// <summary>
    /// Provides data for the C1SpellChecker.AutoReplace event.
    /// </summary>
    public class AutoReplaceEventArgs : EventArgs
    {
        // fields
        Control _control;
        string _found;
        string _replace;
        int _index;
        bool _cancel;

        /// <summary>
        /// Initializes a new instance of an <see cref="AutoReplaceEventArgs"/>.
        /// </summary>
        /// <param name="control"><see cref="Control"/> where the text is being replaced.</param>
        /// <param name="found">String that contains the text about to be replaced.</param>
        /// <param name="replace">String that contains the replacement text.</param>
        /// <param name="index">Position of the <paramref name="found"/> string in the control's text.</param>
        public AutoReplaceEventArgs(Control control, string found, string replace, int index)
        {
            _control = control;
            _found = found;
            _replace = replace;
            _index = index;
        }
        /// <summary>
        /// Gets a reference to the control where the replacement will take place.
        /// </summary>
        public Control Control
        {
            get { return _control; }
        }
        /// <summary>
        /// Gets the string that is about to be replaced.
        /// </summary>
        public string Found
        {
            get { return _found; }
        }
        /// <summary>
        /// Gets or sets the string that contains the replacement text.
        /// </summary>
        public string Replace
        {
            get { return _replace; }
            set { _replace = value; }
        }
        /// <summary>
        /// Gets or sets whether to skip this replacement.
        /// </summary>
        public bool Cancel
        {
            get { return _cancel; }
            set { _cancel = value; }
        }
    }
}
