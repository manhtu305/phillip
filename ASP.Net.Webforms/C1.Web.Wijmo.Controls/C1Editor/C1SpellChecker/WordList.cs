using System;
using System.ComponentModel;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Editor.C1SpellChecker
{
    /// <summary>
    /// Keyed list of words to ignore while spell-checking.
    /// </summary>
    public class WordList : KeyedCollection<int, string>
    {
        //-----------------------------------------------------------------------------
        #region ** fields

        C1SpellCheckerComponent _spell;

        #endregion

        //-----------------------------------------------------------------------------
        #region ** ctor

        internal WordList(C1SpellCheckerComponent spell)
        {
            _spell = spell;
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** overrides

        /// <summary>
        /// Removes all elements from the list.
        /// </summary>
        protected override void ClearItems()
        {
            if (base.Count > 0)
            {
                base.ClearItems();
                OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
            }
        }
        /// <summary>
        /// Inserts a word into the list at the specified index.
        /// </summary>
        /// <param name="index">The zero-based <paramref name="index"/> at which the new item should be inserted.</param>
        /// <param name="item">The item to insert into the list.</param>
        /// <remarks>
        /// Duplicate items are automatically discarded; the list will contain at most one copy of any given string.
        /// </remarks>
        protected override void InsertItem(int index, string item)
        {
            if (!base.Contains(item))
            {
                base.InsertItem(index, item);
                OnListChanged(new ListChangedEventArgs(ListChangedType.ItemAdded, index));
            }
        }
        /// <summary>
        /// Removes the word at the specified index from the collection.
        /// </summary>
        /// <param name="index">The zero-based <paramref name="index"/> at which the new item should be removed.</param>
        protected override void RemoveItem(int index)
        {
            base.RemoveItem(index);
            OnListChanged(new ListChangedEventArgs(ListChangedType.ItemDeleted, index));
        }
        /// <summary>
        /// Not supported.
        /// </summary>
        /// <param name="index">Index of the element to replace.</param>
        /// <param name="item">Element to replace.</param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        protected override void SetItem(int index, string item)
        {
            throw new NotSupportedException("Cannot change items in the list.");
        }
        /// <summary>
        /// Obtains a key for the given element. 
        /// </summary>
        /// <param name="item">The element from which to obtain the key.</param>
        /// <returns>The key for the specified element.</returns>
        protected override int GetKeyForItem(string item)
        {
            return item.GetHashCode();
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** private

        // refresh all monitors when the list changes
        private void OnListChanged(ListChangedEventArgs e)
        {

        }

        #endregion
    }

    /// <summary>
    /// Dictionary of words and their replacements.
    /// </summary>
    public class WordDictionary : Dictionary<string, string>
    {
    }
}
