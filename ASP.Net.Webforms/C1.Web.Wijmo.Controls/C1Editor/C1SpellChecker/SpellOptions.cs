using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.ComponentModel;
using System.Globalization;
using System.Diagnostics;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Drawing.Design;

namespace C1.Web.Wijmo.Controls.C1Editor.C1SpellChecker
{
    using C1.Web.Wijmo.Controls.Localization;

    /// <summary>
    /// Specifies flags that determine whether words should be ignored during spell-checking.
    /// </summary>
    [Flags]
    public enum IgnoreOptions
    {
        /// <summary>
        /// Do not ignore any words.
        /// </summary>
        None = 0,
        /// <summary>
        /// Ignore words that contain numbers (digits).
        /// </summary>
        Numbers = 1,
        /// <summary>
        /// Ignore words that are all uppercase.
        /// </summary>
        UpperCase = 2,
        /// <summary>
        /// Ignore words that contain a mix of uppercase and lowercase characters, except
        /// when there is a single uppercase at the start (for example, "Lowercase" would 
        /// not be ignored, but "LowerCase" and "lowerCase" would).
        /// </summary>
        MixedCase = 4,
        /// <summary>
        /// Ignore words that look like URLs or file names.
        /// </summary>
        Urls = 8,
        /// <summary>
        /// Ignore text in HTML/XML tags.
        /// </summary>
        HtmlTags = 16,
        /// <summary>
        /// Default setting: ignores numbers, mixed case, URLs, and HTML tags.
        /// </summary>
        Default = Numbers | MixedCase | Urls | HtmlTags,
        /// <summary>
        /// Ignores numbers, mixed case, URLs, HTML tags, and uppercase.
        /// </summary>
        All = Numbers | UpperCase | MixedCase | Urls | HtmlTags
    }

    /// <summary>
    /// Specifies the language to be used when displaying the built-in spell dialog
    /// </summary>
    public enum DialogLanguage
    {
        /// <summary>
        /// Choose dialog language according to the current application culture.
        /// </summary>
        Automatic,
        /// <summary>
        /// Show Dutch dialog.
        /// </summary>
        Dutch,
        /// <summary>
        /// Show English dialog.
        /// </summary>
        English,
        /// <summary>
        /// Show French dialog.
        /// </summary>
        French,
        /// <summary>
        /// Show German dialog.
        /// </summary>
        German,
        /// <summary>
        /// Show Italian dialog.
        /// </summary>
        Italian,
        /// <summary>
        /// Show Portuguese dialog.
        /// </summary>
        Portuguese,
        /// <summary>
        /// Show Spanish dialog.
        /// </summary>
        Spanish,
    }
    /// <summary>
    /// Class that contains spelling options including words to ignore, whether to show suggestions in
    /// a spell context menu, etc.
    /// </summary>
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class SpellOptions
    {
        //-----------------------------------------------------------------------------
        #region ** fields

        private C1SpellCheckerComponent  _spell;                         // owner
        private IgnoreOptions   _ignoreOptions;                 // ignore words with mixed casing etc
        //private bool            _suggestionsInContextMenu;      // show suggestions in context menu
        //private int             _maxSuggestions;                // maximum suggestions to show in context menu
        //private bool            _activeSpellingEnabled = false;         // suspend active spelling globally
        //private TextureBrush    _wigglyBrush;                   // brush used to paint wiggly underlines
        private Color           _underlineColor = Color.Empty;                // color of the wiggly underlines
        private DialogLanguage        _dialogLanguage;                // language of the built-in spell dialog

        #endregion

        //-----------------------------------------------------------------------------
        #region ** ctor

        /// <summary>
        /// Creates a new instance of the C1Editor class.
        /// </summary>
        /// <param name="spell">The specified C1SpellCheckerComponent object.</param>
        internal SpellOptions(C1SpellCheckerComponent spell)
        {
            // save reference to owner
            _spell = spell;

            // set defaults
            _ignoreOptions = IgnoreOptions.Default;
            //_suggestionsInContextMenu = true;
            //_maxSuggestions = 6;
            //_activeSpellingEnabled = true;
            _underlineColor = Color.Red;
            _dialogLanguage = DialogLanguage.Automatic;
             
         
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** object model


        /// <summary>
        /// Gets or sets types of words to ignore during the spell-check.
        /// </summary>
        [
        C1Description("Ignore", "Gets or sets types of word to ignore during the spell-check."),
        DefaultValue(IgnoreOptions.Default),
        Editor(typeof(FlagEditor), typeof(UITypeEditor))
        ]
        public IgnoreOptions Ignore
        {
            get { return _ignoreOptions; }
            set
            {
                if (_ignoreOptions != value)
                {
                    _ignoreOptions = value;
                }
            }
        }


        /// <summary>
        /// Gets or sets the DialogLanguage  used when displaying the built-in spell dialog.
        /// </summary>
        /// <remarks>
        ///  The dialog language does not affect spelling. To change the language used for spelling, use
        /// theC1SpellChecker.MainDictionary  property to select a different spelling dictionary. 
        /// By default, the built-in spell dialog  C1SpellDialog is displayed in the
        /// language specified by the current culture. If the current culture specifies a language that is not
        /// available, then the dialog will be displayed in English.
        /// See the DialogLanguage enumeration for a list of supported dialog languages.
        /// </remarks>
        [
        C1Description("DialogLanguage", "Gets or sets the language used when displaying the built-in spell dialog."),
        DefaultValue(DialogLanguage.Automatic)
        ]
        public DialogLanguage DialogLanguage
        {
            get { return _dialogLanguage; }
            set { _dialogLanguage = value; }
        }
        /// <summary>
        /// Gets a string representation of this object.
        /// </summary>
        /// <returns>A string representation of this object.</returns>
        public override string ToString()
        {
            return "(SpellOptions)";
        }
        #endregion

        //-----------------------------------------------------------------------------
        #region ** internal

        // get the two-letter ISO name for the currently selected dialog culture
        internal string DialogCultureString
        {
            get
            {
                switch (DialogLanguage)
                {
                    case DialogLanguage.Automatic:    return CultureInfo.CurrentCulture.TwoLetterISOLanguageName;
                    case DialogLanguage.Dutch:        return "nl";
                    case DialogLanguage.English:      return "en";
                    case DialogLanguage.French:       return "fr";
                    case DialogLanguage.German:       return "de";
                    case DialogLanguage.Italian:      return "it";
                    case DialogLanguage.Portuguese:   return "pt";
                    case DialogLanguage.Spanish:      return "es";
                }
                Debug.Assert(false, "Unknown DialogLanguage value.");
                return null;
            }
        }


        #endregion
        
    }

    /// <summary>
    /// <see cref="UITypeEditor"/> that can be used to edit enumerations with a <b>Flags</b> attribute.
    /// </summary>
    /// <remarks>
    /// Shows a checked list box with one entry per flag, users can check any combination.
    /// </remarks>
    internal class FlagEditor : UITypeEditor
    {
        private IWindowsFormsEditorService _edSvc;
        private CheckedListBox _listBox;
        private bool _cancel;

        public FlagEditor()
        {
            // build selector list
            _listBox = new CheckedListBox();
            _listBox.BorderStyle = BorderStyle.None;
            _listBox.CheckOnClick = true;
            _listBox.ThreeDCheckBoxes = false;
            _listBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(_listBox_KeyPress);
            _listBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(_listBox_ItemCheck);
        }
        override public UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext ctx)
        {
            return UITypeEditorEditStyle.DropDown;
        }
        override public object EditValue(ITypeDescriptorContext ctx, IServiceProvider provider, object value)
        {
            // initialize editor service
            _edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
            if (_edSvc == null) return value;

            // populate the list
            _listBox.Items.Clear();
            foreach (object item in Enum.GetValues(ctx.PropertyDescriptor.PropertyType))
            {
                // skip 'none', people have to uncheck all options to get it
                // (if we added this to the list, it would always be checked...)
                if ((int)item == 0) continue;

                // add this item with the proper check state
                CheckState check = (((int)value & (int)item) == (int)item)
                    ? CheckState.Checked
                    : CheckState.Unchecked;
                _listBox.Items.Add(item, check);
            }

            // show the list
            _cancel = false;
            _edSvc.DropDownControl(_listBox);

            // build return value from checked items on the list
            if (!_cancel)
            {
                // build a comma-delimited string with the checked items
                StringBuilder sb = new StringBuilder();
                foreach (object item in _listBox.CheckedItems)
                {
                    if (sb.Length > 0) sb.Append(", ");
                    sb.Append(item.ToString());
                }

                // convert empty string into '0' (no flags) and then into proper type
                string s = sb.Length > 0 ? sb.ToString() : "0";
                value = ctx.PropertyDescriptor.Converter.ConvertFrom(s);
            }

            // done
            return value;
        }

        // close editor if the user presses enter or escape
        private void _listBox_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case (char)27:
                    _cancel = true;
                    if (_edSvc != null) _edSvc.CloseDropDown();
                    break;
                case (char)13:
                    if (_edSvc != null) _edSvc.CloseDropDown();
                    break;
            }
        }

        // after an item is checked, update all other items
        // (in case we have combination flags, e.g. All)
        bool _checking = false;
        private void _listBox_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
        {
            // no reentrancy
            if (!_checking)
            {
                _checking = true;

                // get current value
                int value = 0;
                for (int i = 0; i < _listBox.Items.Count; i++)
                {
                    if (i != e.Index && _listBox.GetItemChecked(i))
                        value |= (int)_listBox.Items[i];
                }

                // apply the change that was just made
                if (e.NewValue == CheckState.Checked)
                    value |= (int)_listBox.Items[e.Index];
                else
                    value &= ~(int)_listBox.Items[e.Index];

                // apply new value to all items on the list
                for (int i = 0; i < _listBox.Items.Count; i++)
                {
                    bool check = (value & (int)_listBox.Items[i]) == (int)_listBox.Items[i];
                    _listBox.SetItemChecked(i, check);
                }

                // all done
                _checking = false;
            }
        }
    }
}
