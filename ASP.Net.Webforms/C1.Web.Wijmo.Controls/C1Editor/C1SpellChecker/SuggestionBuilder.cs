using System;
using System.Collections.Generic;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Editor.C1SpellChecker
{
    /// <summary>
    /// Special chars:
    ///     en:    ?  ��?  ��??   ?
    ///     es:    ?   ? ? ��    ??
    ///     fr:   ������������ ��??��
    ///     de: ??��  ��    ? ?   ?
    ///     pt :  ��?���� ?  �� ???
    ///     ALL: ����������������������
    /// 
    /// Start with alpha + '
    /// if not enough, try ALL
    /// </summary>
    internal class SuggestionBuilder
    {
        //-----------------------------------------------------------------------------
        #region ** enums

        private enum WordCasing
        {
            Upper,
            Lower,
            Proper,
            Mixed
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** fields

        private C1SpellCheckerComponent      _spell;
        private string              _word;
        private int                 _len;
        private int                 _maxCount;
        private char[]              _buffer;
        private WordCasing          _casing;
        private char[]              _validChars;
        private List<string>        _list;

        #endregion

        //-----------------------------------------------------------------------------
        #region ** ctor

        internal SuggestionBuilder(C1SpellCheckerComponent spell)
        {
            _spell = spell;
            _list = new List<string>();
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** object model

        // gets an array of valid spelling suggestions for a word
        public string[] GetSuggestions(string word, int maxCount)
        {
            // prepare
            _casing = GetWordCasing(word);
            _len = word.Length;
            _maxCount = maxCount;
            _buffer = new char[word.Length + 1];
            _list.Clear();

            // always test lower-case version
            _word = _casing == WordCasing.Lower ? word : word.ToLower();

            // generate suggestions with common chars
            _validChars = GetCommonChars();

            // generate suggestions
            // (try more common mistakes first so they appear at the start of the list)
            CapitalizeChar();
            SwapChar();
            AddChar();
            DropChar();
            ChangeChar();
            SplitWord();

            // if not enough, try suggestions with special chars
            if (_list.Count < _maxCount)
            {
                _validChars = GetSpecialChars();
                AddChar();
                ChangeChar();
            }

            // get suggestions, clear list
            string[] suggestions = _list.ToArray();
            _list.Clear();

            // return result
            return suggestions;
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** private stuff

        // capitalize first char ('paul' -> 'Paul')
        private void CapitalizeChar()
        {
            if (_casing == WordCasing.Lower) // && _word.Length > 1) // <<B5>> "i" -> "I"
            {
                _word.CopyTo(0, _buffer, 0, _len);
                _buffer[0] = char.ToUpper(_buffer[0]);
                TryWord(_buffer, _len, true);
            }
        }

        // swap pairs of adjacent characters ('becuase' -> 'because')
        private void SwapChar()
        {
            // try changes
            for (int i = 0; i < _len - 1; i++)
            {
                // copy word to buffer
                _word.CopyTo(0, _buffer, 0, _len);

                // check that both have the same case
                if ((char.IsLower(_buffer[i]) == char.IsLower(_buffer[i + 1])))
                {
                    // swap chars
                    char c = _buffer[i];
                    _buffer[i] = _buffer[i + 1];
                    _buffer[i + 1] = c;
                    
                    // test new string
                    TryWord(_buffer, _len, false);

                    // break if we can
                    if (_list.Count >= _maxCount)
                        return;
                }
            }
        }

        // remove extra chars ('commmon' -> 'common')
        private void DropChar()
        {
            if (_len > 2)
            {
                for (int i = _len - 1; i >= 0; i--)
                {
                    // drop char
                    _word.CopyTo(0, _buffer, 0, i);
                    _word.CopyTo(i + 1, _buffer, i, _len - i - 1);

                    // test new string
                    TryWord(_buffer, _len - 1, false);

                    // break if we can
                    if (_list.Count >= _maxCount)
                        return;
                }
            }
        }

        // insert characters at every position ('comon' -> 'common')
        private void AddChar()
        {
            for (int i = _len; i >= 0; i--)
            {
                // try all chars and apostrophe
                foreach (char c in _validChars)
                {
                    // add char
                    _word.CopyTo(0, _buffer, 0, i);
                    _buffer[i] = c;
                    _word.CopyTo(i, _buffer, i + 1, _len - i);
                    TryWord(_buffer, _len + 1, false);

                    // break if we can
                    if (_list.Count >= _maxCount)
                        return;
                }
            }
        }

        // substitute characters ('tezt' -> 'test')
        private void ChangeChar()
        {
            // try changing each character
            for (int i = _len - 1; i >= 0; i--)
            {
                foreach (char c in _validChars)
                {
                    if (c != _word[i])
                    {
                        _word.CopyTo(0, _buffer, 0, _len);
                        _buffer[i] = c;
                        TryWord(_buffer, _len, false);

                        // break if we can
                        if (_list.Count >= _maxCount)
                            return;
                    }
                }
            }
        }

        // split word into two ('bluesky' -> 'blue sky')
        private void SplitWord()
        {
            if (_len >= 4 && _list.Count < _maxCount)
            {
                // try changes
                for (int i = 2; i < _len - 1; i++)
                {
                    string w1 = _word.Substring(0, i);
                    if (_spell.CheckWord(w1))
                    {
                        string w2 = _word.Substring(i);
                        if (_spell.CheckWord(w2))
                        {
                            // add combination
                            string suggestion = string.Format("{0} {1}", FixSuggestionCasing(w1), FixSuggestionCasing(w2));
                            _list.Add(suggestion);

                            // break if we can
                            if (_list.Count >= _maxCount)
                                return;
                        }
                    }
                }
            }
        }

        // test a word, add to list if not there yet
        private void TryWord(char[] buffer, int length, bool keepCasing)
        {
            if (_list.Count < _maxCount)
            {
                string word = new string(buffer, 0, length);
                if (word != _word)
                {
                    if (!keepCasing)
                    {
                        word = FixSuggestionCasing(word);
                    }
                    if (!_list.Contains(word) && _spell.CheckWord(word))
                    {
                        _list.Add(word);
                    }
                }
            }
        }

        // determine the casing of a word (UPPER, lower, Proper, MiXed)
        private WordCasing GetWordCasing(string word)
        {
            // scan string
            int charCount = 0;
            int upperCount = 0;
            bool upperFirst = false;
            foreach (char c in word)
            {
                if (char.IsLetter(c))
                {
                    charCount++;
                    if (char.IsUpper(c))
                    {
                        upperCount++;
                        if (charCount == 1)
                        {
                            upperFirst = true;
                        }
                    }
                }
            }

            // determine casing
            WordCasing casing = WordCasing.Mixed;
            if (charCount > 0)
            {
                if (upperCount == charCount)
                {
                    casing = WordCasing.Upper;
                }
                else if (upperCount == 0)
                {
                    casing = WordCasing.Lower;
                }
                else if (upperCount == 1 && upperFirst)
                {
                    casing = WordCasing.Proper;
                }
            }

            // done
            return casing;
        }

        // fix suggestion casing to match original word
        private string FixSuggestionCasing(string word)
        {
            switch (_casing)
            {
                case WordCasing.Upper:
                    word = word.ToUpper();
                    break;
                case WordCasing.Proper:
                    word = char.ToUpper(word[0]) + word.Substring(1);
                    break;
            }
            return word;
        }

        // gets an array with valid characters to create suggestions with
        private static char[] _commonChars = SpecialCharacters.commonChars.ToCharArray();
        private char[] GetCommonChars()
        {
            return _commonChars;
        }

        // gets an array with valid characters to create suggestions with
        // including russian characters <<B11>>
        //private static char[] _specialChars = "'������������������������".ToCharArray();
        private static char[] _specialChars = SpecialCharacters.specialChars.ToCharArray();
        private char[] GetSpecialChars()
        {
            return _specialChars;
        }

        #endregion
    }
}
