using System;
using System.IO;
using System.Reflection;
using System.ComponentModel;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text;
using System.Web;

namespace C1.Web.Wijmo.Controls.C1Editor.C1SpellChecker
{
    /// <summary>
    /// Describes the current state of a <see cref="SpellDictionary"/>.
    /// </summary>
    public enum DictionaryState
    {
        /// <summary>
        /// The dictionary has not been loaded yet.
        /// </summary>
        Empty,
        /// <summary>
        /// The dictionary has been successfully loaded.
        /// </summary>
        Loaded,
        /// <summary>
        /// The dictionary has not been loaded because the specified file could not be found.
        /// </summary>
        FileNotFound,
        /// <summary>
        /// The dictionary has not been loaded because the specified file has invalid format.
        /// </summary>
        InvalidFileFormat,
        /// <summary>
        /// The dictionary is protected and the password used to load it was missing or incorrect.
        /// </summary>
        InvalidPassword
    }

    /// <summary>
    /// Interface implemented by all spell dictionaries.
    /// </summary>
    public interface ISpellDictionary
    {
        /// <summary>
        /// Checks whether the dictionary contains a given word.
        /// </summary>
        /// <param name="word">Word to lookup.</param>
        /// <returns>True if the dictionary contains the word, false otherwise.</returns>
        bool Contains(string word);
    }

    /// <summary>
    /// Base class for built-in and user dictionaries (see (<see cref="SpellDictionary"/> and
    /// <see cref="UserDictionary"/>).
    /// </summary>
    [
    TypeConverter(typeof(ExpandableObjectConverter))
    ]
    public abstract class SpellDictionaryBase : ISpellDictionary 
    {
        //-----------------------------------------------------------------------------
        #region ** fields

        /// <summary>
        /// Name of the file where the word list was loaded from.
        /// </summary>
        protected string m_fileName;
        /// <summary>
        /// Gets or sets a <see cref="DictionaryState"/> value that indicates whether 
        /// the dictionary has been loaded or why it hasn't.
        /// </summary>
        protected DictionaryState m_state;
        /// <summary>
        /// Gets or sets whether the dictionary is enabled.
        /// </summary>
        protected bool m_enabled;
        /// <summary>
        /// <see cref="C1SpellChecker"/> that owns the dictionary.
        /// </summary>
        internal C1SpellCheckerComponent m_spell;

        #endregion

        //-----------------------------------------------------------------------------
        #region ** ctor

        internal SpellDictionaryBase(C1SpellCheckerComponent spell)
        {
            m_spell = spell;
            m_enabled = true;
            m_fileName = string.Empty;
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** object model

        /// <summary>
        /// Gets or sets the name of the file that contains the dictionary.
        /// </summary>
        [DefaultValue("")]
        public virtual string FileName
        {
            get { return m_fileName; }
            set { m_fileName = value; }
        }
        /// <summary>
        /// Gets or sets whether the dictionary is enabled.
        /// </summary>
        [DefaultValue(true)]
        public virtual bool Enabled
        {
            get { return m_enabled; }
            set { m_enabled = value; }
        }
        /// <summary>
        /// Gets a <see cref="DictionaryState"/> value that indicates whether the dictionary was loaded
        /// successfully.
        /// </summary>
        [Browsable(false), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DictionaryState State
        {
            get { return m_state; }
            set { m_state = value; }
        }
        /// <summary>
        /// Ensures the dictionary is loaded, reading words from the file if necessary.
        /// </summary>
        /// <returns>True if the dictionary is enabled and has been loaded successfully.</returns>
        public bool EnsureLoaded()
        {
            if (State == DictionaryState.Empty && Enabled)
            {
                if (m_spell.Site == null || m_spell.Site.DesignMode == false)
                {
                    return Load();
                }
            }
            return State == DictionaryState.Loaded && Enabled;
        }
        /// <summary>
        /// Gets a string that represents this object.
        /// </summary>
        /// <returns>A string that represents this object.</returns>
        public override string ToString()
        {
            return string.Format("({0})", string.IsNullOrEmpty(FileName)
                ? "en-US Dictionary"
                : FileName);
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** protected

        /// <summary>
        /// Gets the fully qualified name of the file that contains the dictionary.
        /// </summary>
        /// <param name="mustExist">Whether to return a filename even if the file doesn't exist.</param>
        /// <returns>A complete file name which is guaranteed to exist, or an empty
        /// string if the file name was not specified or could not be found.</returns>
        /// <remarks>
        /// If the path is not specified, the component looks for it in the 
        /// application folder, in the folder where the <see cref="C1SpellChecker"/>
        /// assembly is installed, and in the system folder.
        /// </remarks>
        protected string GetFileName(bool mustExist)
        {
            // get file name, handle trivial cases
            string fileName = FileName;
            if (string.IsNullOrEmpty(fileName))
            {
                return string.Empty;
            }

            // user specified a complete filename, no need to look for it
            if (Path.IsPathRooted(fileName))
            {
                return (mustExist && !File.Exists(fileName))
                    ? string.Empty
                    : fileName;
            }

            // look in application folder
            string path = HttpContext.Current.Server.MapPath("~/"); ;
            string full = Path.Combine(path, fileName);
            if (mustExist == false || File.Exists(full))
            {
                return full;
            }

            // look in dll folder
            path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
            if (path.StartsWith(@"file:\", StringComparison.OrdinalIgnoreCase))
            {
                path = path.Substring(6);
            }
            full = Path.Combine(path, fileName);
            if (mustExist == false || File.Exists(full))
            {
                return full;
            }

            // look in system folder
            path = System.Environment.SystemDirectory;
            full = Path.Combine(path, fileName);
            if (mustExist == false || File.Exists(full))
            {
                return full;
            }

            // no dice...
            return string.Empty;
        }
        #endregion

        //-----------------------------------------------------------------------------
        #region ** abstract

        /// <summary>
        /// Loads the dictionary from the file specified by the <see cref="FileName"/> property.
        /// </summary>
        /// <returns>True if the dictionary was loaded successfully, false otherwise.</returns>
        /// <remarks>
        /// If the dictionary fails to load, this method returns false. In this case,
        /// you can check the value of the dictionary's <see cref="State"/> property to determine 
        /// why the file failed to load.
        /// </remarks>
        protected abstract bool Load();
        /// <summary>
        /// Checks whether the dictionary contains a given word.
        /// </summary>
        /// <param name="word">Word to lookup.</param>
        /// <returns>True if the dictionary contains the word, false otherwise.</returns>
        public abstract bool Contains(string word);
        #endregion
    }
}
