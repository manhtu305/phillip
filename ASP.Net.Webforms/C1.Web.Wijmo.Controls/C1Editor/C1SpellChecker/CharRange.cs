using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Editor.C1SpellChecker
{
    /// <summary>
    /// Interface that must be implemented by custom spell-check parsers.
    /// </summary>
    public interface ISpellParser
    {
        /// <summary>
        /// Method that retrieves the next word to spell-check in a string.
        /// </summary>
        /// <param name="text">String that contains the text being spell-checked.</param>
        /// <param name="start">Position being checked within the <paramref name="text"/> string.</param>
        /// <param name="ignore"><see cref="IgnoreOptions"/> that specifies types of words that should be ignored.</param>
        /// <param name="previousWord">Last word retrieved by the parser (used to detect duplicates).</param>
        /// <returns>A <see cref="CharRange"/> object that represents the next word to be spell-checked, or 
        /// null if all the text has been spell-checked.</returns>
        CharRange GetNextWord(string text, int start, IgnoreOptions ignore, string previousWord);
        /// <summary>
        /// Method that filters any unwanted characters out of a string to be checked.
        /// </summary>
        /// <param name="word">Raw text to be checked.</param>
        /// <returns>A modified word to be checked, null to skip checking this string, 
        /// or the same word if no filtering is required.</returns>
        /// <remarks>
        /// <para>This method is useful in custom parsers that should ignore specific characters
        /// or words.</para>
        /// <para>For example, a parser that parses strings with accelerator characters should
        /// filter out the ampersands before the word is checked.</para>
        /// </remarks>
        string FilterWord(string word);
    }

    /// <summary>
    /// Class that represents a character range within a string.
    /// This is used by the <see cref="C1SpellChecker"/> control to represent the
    /// spelling mistakes found in text.
    /// </summary>
    public class CharRange
    {
        //-----------------------------------------------------------------------------
        #region ** fields

        private int     _start;
        private string  _text;
        private bool    _duplicate;

        internal const char QUOTE = '\'';
        internal const char QUOTE_REV = '`';
        internal const char QUOTE_CURLY = (char)8217;
        internal const string URL_FILE_CHARS = @":\/.?=";

        #endregion

        //-----------------------------------------------------------------------------
        #region ** ctor

        /// <summary>
        /// Initializes a new instance of a <see cref="CharRange"/>.
        /// </summary>
        /// <param name="text">Text contained in the range.</param>
        /// <param name="start">Starting index of the range within the text.</param>
        /// <param name="duplicate">Whether the range is misspelled or just a duplicate.</param>
        public CharRange(string text, int start, bool duplicate)
        {
            _text = text;
            _start = start;
            _duplicate = duplicate;
        }
        #endregion

        //-----------------------------------------------------------------------------
        #region ** object model

        /// <summary>
        /// Gets the string represented by this <see cref="CharRange"/>.
        /// </summary>
        public string Text
        {
            get { return _text; }
        }
        /// <summary>
        /// Gets the starting index of the string represented by this <see cref="CharRange"/> within the text.
        /// </summary>
        public int Start
        {
            get { return _start; }
        }
        /// <summary>
        /// Gets the length of the string represented by this <see cref="CharRange"/>.
        /// </summary>
        public int Length
        {
            get { return string.IsNullOrEmpty(_text) ? 0 : _text.Length; }
        }
        /// <summary>
        /// Gets the end index of the string represented by this <see cref="CharRange"/>.
        /// </summary>
        public int End
        {
            get { return _start + Length; }
        }
        /// <summary>
        /// Gets whether this <see cref="CharRange"/> contains the same string as the previous one.
        /// </summary>
        public bool Duplicate
        {
            get { return _duplicate; }
        }
        /// <summary>
        /// Determines whether this <see cref="CharRange"/> is equal to another.
        /// </summary>
        /// <param name="obj"><see cref="CharRange"/> to compare to this one.</param>
        /// <returns>True if both objects contain the same word at the same position.</returns>
        public override bool Equals(object obj)
        {
            CharRange se = obj as CharRange;
            return se != null &&
                se.Start == Start &&
                se.Duplicate == Duplicate &&
                se.Text == Text;
        }
        /// <summary>
        /// Serves as a hash function for the <see cref="CharRange"/> type.
        /// </summary>
        /// <returns>A hash code for this <see cref="CharRange"/>.</returns>
        public override int GetHashCode()
        {
            // to keep compiler happy
            return base.GetHashCode();
        }
        #endregion

        //-----------------------------------------------------------------------------
        #region ** static

        /// <summary>
        /// Extends a <see cref="CharRange"/> over whitespace.
        /// </summary>
        /// <param name="text">String that contains the <see cref="CharRange"/>.</param>
        /// <param name="error"><see cref="CharRange"/> that will be expanded.</param>
        /// <returns>Expanded <see cref="CharRange"/>.</returns>
        /// <remarks>
        /// <para>The method returns a new <see cref="CharRange"/> object that includes the original <paramref name="error"/>
        /// plus any whitespace that immediately follows it. If there is no whitespace after the original <paramref name="error"/>,
        /// then the returned value is extended to include any whitespace that precedes the original <paramref name="error"/>.</para>
        /// <para>This method is used to extend editor selections before deleting duplicate words.</para>
        /// </remarks>
        public static CharRange ExpandOverWhitespace(string text, CharRange error)
        {
            // get parameters
            int selStart = error.Start;
            int selLength = error.Text.Length;
            int next = selStart + selLength;

            // if selection is followed by a period, extend selection back
            if (next < text.Length && !char.IsWhiteSpace(text, next))
            {
                int prev = selStart - 1;
                while (prev >= 0 && char.IsWhiteSpace(text, prev))
                {
                    prev--;
                    selStart--;
                    selLength++;
                }
            }
            else // otherwise, extend it forward
            {
                while (next < text.Length && char.IsWhiteSpace(text, next))
                {
                    next++;
                    selLength++;
                }
            }

            // done
            return new CharRange(text.Substring(selStart, selLength), selStart, false);
        }
        /// <summary>
        /// Determines whether a character is spell-checkable.
        /// </summary>
        /// <param name="c">Character to test.</param>
        /// <returns>True if the character is spell-checkable, false otherwise.</returns>
        /// <remarks>This method returns true for all letter and digit characters, underscores, and single quotes.</remarks>
        public static bool IsWordCharacter(char c)
        {
            return char.IsLetterOrDigit(c) ||
                c == '_' ||
                c == QUOTE || c == QUOTE_REV || c == QUOTE_CURLY;   // single quotes of various types
        }
        /// <summary>
        /// Gets the next word in a string, starting from a given position.
        /// </summary>
        /// <param name="text">String that contains the text.</param>
        /// <param name="start">Starting position where to look for a word.</param>
        /// <param name="ignore"><see cref="IgnoreOptions"/> that determines words to ignore.</param>
        /// <param name="previousWord">Previous word (used to detect duplicates).</param>
        /// <returns>A <see cref="CharRange"/> object that contains the next word in the string,
        /// or null if there are no more words in the string.</returns>
        public static CharRange GetNextWord(string text, int start, IgnoreOptions ignore, string previousWord)
        {
            // elements to ignore
            bool ignoreHtml = (ignore & IgnoreOptions.HtmlTags) != 0;
            bool ignoreUrls = (ignore & IgnoreOptions.Urls) != 0;

            // advance start until we get a letter or a digit
            int length;
            string word;
            int textLength = text.Length;
            while (start < textLength && !char.IsLetterOrDigit(text, start))
            {
                // skip Html/Xml tags
                if (ignoreHtml)
                {
                    if (text[start] == '<')
                    {
                        int pos = SkipHtmlTag(text, start);
                        if (pos > -1)
                        {
                            return GetNextWord(text, pos + 1, ignore, null);
                        }
                    }
                    else if (text[start] == '&')
                    {
                        int pos = text.IndexOf(';', start);
                        if (pos > -1)
                        {
                            length = pos - start + 1;
                            word = text.Substring(start, length);
                            if (!ContainsWhiteSpace(word))
                            {
                                return GetNextWord(text, pos + 1, ignore, null);
                            }
                        }
                    }

                }

                // clear previous word if this is not a space (e.g. "love cars... cars are nice!")
                if (text[start] != ' ')
                {
                    previousWord = null;
                }

                // move on
                start++;
            }

            // no more text?
            if (start >= textLength)
            {
                return null;
            }

            // advance end until we get a word end
            int end = start;
            while (end < textLength - 1 && IsWordCharacter(text[end + 1]))
            {
                end++;
            }

            // advance past Urls and file names
            if (ignoreUrls && end < textLength - 1 && URL_FILE_CHARS.IndexOf(text[end + 1]) > -1)
            {
                int pos = SkipUrlFile(text, textLength, end + 1);
                if (pos > -1)
                {
                    length = pos - start + 1;
                    word = text.Substring(start, length);
                    return new CharRange(word, start, false);
                }
            }

            // make sure word doesn't end with apostrophe (foo''''' bar)
            int wordEnd = end;
            while (wordEnd > start && !char.IsLetterOrDigit(text, wordEnd))
            {
                wordEnd--;
            }

            // found the word
            length = wordEnd - start + 1;
            word = text.Substring(start, length);

            // check for duplicates
            bool duplicate = previousWord != null && string.Compare(word, previousWord, true) == 0;

            // done
            return new CharRange(word, start, duplicate);
        }
        /// <summary>
        /// Gets the word at a given position in the string.
        /// </summary>
        /// <param name="text">String that contains the text.</param>
        /// <param name="pos">Position where to look for a word.</param>
        /// <param name="ignore"><see cref="IgnoreOptions"/> that determines words to ignore.</param>
        /// <returns>A <see cref="CharRange"/> object that contains the word at the specified
        /// position in the string, or null if there are no more words at the specified position.</returns>
        public static CharRange GetWordAt(string text, int pos, IgnoreOptions ignore)
        {
            // trivial case
            if (pos < 0 || pos >= text.Length)
            {
                return null;
            }

            // find starting point to search (nearest line break)
            int start = pos;
            while (start > 0 && text[start] != '\r' && text[start] != '\n')
            {
                start--;
            }

            // search from the adjusted start
            while (true)
            {
                CharRange rg = GetNextWord(text, start, ignore, null);
                if (rg == null || rg.Start > pos)
                {
                    return null;
                }
                start = rg.End;
                if (start >= pos)
                {
                    return rg;
                }
            }
        }

        // check if a word contains whitespace
        private static bool ContainsWhiteSpace(string word)
        {
            for (int i = 0; i < word.Length; i++)
            {
                if (char.IsWhiteSpace(word, i))
                    return true;
            }
            return false;
        }

        // calculate position of Html tag end
        private static int SkipHtmlTag(string text, int start)
        {
            // sanity
            Debug.Assert(text[start] == '<');

            // special case 1: don't check scripts <script></script>
            if (string.Compare(text, start, "<script>", 0, 8, true) == 0)
            {
                return text.IndexOf("</script>", start, StringComparison.InvariantCultureIgnoreCase);
            }

            // special case 2: do check comments <!-- xxx -->
            if (string.Compare(text, start, "<!--", 0, 4, true) == 0)
            {
                return -1;
            }

            // others: look for tag end
            return text.IndexOf(">", start, StringComparison.InvariantCultureIgnoreCase);
        }

        // calculate position of url/filename end
        private static int SkipUrlFile(string text, int textLength, int start)
        {
            int newStart = -1;
            for (int pos = start; pos < textLength; pos++)
            {
                char c = text[pos];
                if (IsWordCharacter(c))
                {
                    // record position of last word character in Url
                    newStart = pos;
                    continue;
                }
                if (URL_FILE_CHARS.IndexOf(c) > -1)
                {
                    // continue over url/file chars
                    continue;
                }

                // done
                break;
            }
            return newStart;
        }
        #endregion
    }


    /// <summary>
    /// List of <see cref="CharRange"/> objects.
    /// </summary>
    public class CharRangeList : List<CharRange>
    {
        //-----------------------------------------------------------------------------
        #region ** object model

        /// <summary>
        /// Adds a new <see cref="CharRange"/> to the list.
        /// </summary>
        /// <param name="word">Word to add to the list.</param>
        /// <param name="start">Starting index of the character range within the text.</param>
        /// <param name="duplicate">Whether the range is a duplicate of the previous range in the text.</param>
        public void Add(string word, int start, bool duplicate)
        {
            Add(new CharRange(word, start, duplicate));
        }
        /// <summary>
        /// Gets the <see cref="CharRange"/> that contains a specified position within the text.
        /// </summary>
        /// <param name="charPosition">Position within the text.</param>
        /// <returns>A <see cref="CharRange"/> that contains the specified position,
        /// or null if there is no error at the specified position.</returns>
        public CharRange GetRangeFromPosition(int charPosition)
        {
            int index = GetRangeIndexFromPosition(charPosition);
            return index < 0 ? null : this[index];
        }
        /// <summary>
        /// Gets the index of the <see cref="CharRange"/> that contains the specified position within the text.
        /// </summary>
        /// <param name="charPosition">Position within the text.</param>
        /// <returns>Index of the <see cref="CharRange"/> in the list that contains the specified position,
        /// or -1 if there is no error at the specified position.</returns>
        public int GetRangeIndexFromPosition(int charPosition)
        {
            // find error that contains the given char position
            for (int index = 0; index < this.Count; index++)
            {
                CharRange err = this[index];
                if (err.Start > charPosition) break;
                if (charPosition <= err.Start + err.Text.Length)
                {
                    return index;
                }
            }

            // no errors found at given char position
            return -1;
        }
        /// <summary>
        /// Determines whether the given <see cref="CharRangeList"/> is equal to this one.
        /// </summary>
        /// <param name="obj"><see cref="CharRangeList"/> to compare to this one.</param>
        /// <returns>True if both objects contain the same members.</returns>
        public override bool Equals(object obj)
        {
            CharRangeList sel = obj as CharRangeList;
            if (sel == null || sel.Count != Count)
            {
                return false;
            }
            for (int i = 0; i < Count; i++)
            {
                if (!sel[i].Equals(this[i]))
                    return false;
            }
            return true;
        }
        /// <summary>
        /// Serves as a hash function for the <see cref="CharRangeList"/> type.
        /// </summary>
        /// <returns>A hash code for this <see cref="CharRangeList"/>.</returns>
        public override int GetHashCode()
        {
            // to keep compiler happy
            return base.GetHashCode();
        }
        #endregion
    }

}
