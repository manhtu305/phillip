using System;
using System.Drawing;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Globalization;
using System.ComponentModel;
using System.Collections.Generic;
using System.Reflection;
using System.Diagnostics;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Editor.C1SpellChecker
{
    using C1.Web.Wijmo.Controls.Localization;

    /// <summary>
    /// Component that provides spell-checking services to controls and applications.
    /// </summary>
    /// <remarks>
    /// The C1SpelChecker component supports three spell-checking modes:
    /// 
    /// Batch mode
    /// Use the CheckText(string), CheckWord, and GetSuggestions(string, int)
    /// methods to check strings and get lists of errors and spelling suggestions.
    /// 
    /// Dialog mode
    /// Use the CheckControl(ISpellCheckableEditor method to check the content of any 
    /// TextBoxBase-derived controls using a modal dialog. The C1SpellChecker will
    /// check the text and display a dialog where the user can choose to correct or ignore each error.
    /// You can also check editors that do not derive from TextBoxBase. To do that, you
    /// have to create a wrapper class that implements the ISpellCheckableEditor interface.
    /// 
    /// As-you-type mode
    /// Set the extender property SpellCheck on any TextBoxBase-derived control to true,
    /// and the C1SpellChecker will monitor changes to the control. Any spelling mistakes will be indicated 
    /// on the control by a red, wavy underline; right-clicking the errors will show a context menu with spelling 
    /// suggestions.
    /// You can also provide as-you-type spelling support for editors that do not derive from TextBoxBase.
    /// To do that, you have to create a wrapper class that implements the ISpellCheckableRichEditor 
    /// interface.
    /// 
    /// Dictionary Deployment
    /// 
    /// The C1SpellChecker component uses spelling dictionaries stored in files with a 'dct' extension.
    /// 
    /// To select which dictionary to use, use the MainDictionary property and set
    /// SpellDictionaryBase.FileName to the name of the file you want to use.
    /// 
    /// The easiest way to ensure the dictionary is deployed correctly is to add to your projects a reference 
    /// to the dictionary file(s) you want to use, then set the Build Action property to None and 
    /// the Copy to Output Directory property to Copy if newer. This way, the dictionaries will 
    /// be copied to the output directory along with the application executable and the C1SpellChecker 
    /// assembly.
    /// 
    /// When using this deployment method, make sure the main dictionary's SpellDictionaryBase.FileName
    /// value specifies a file name without a path. This way, the component will search for the dictionary in the
    /// directory where the C1SpellChecker assembly is located.
    /// 
    /// </remarks>
    [ToolboxItem(false)]
    internal class C1SpellCheckerComponent : Component, ISupportInitialize
    {
        //-----------------------------------------------------------------------------
        #region ** fields

        private SpellDictionary _mainDict;              // main dictionary
        private UserDictionary _userDict;               // user dictionary
        private ISpellDictionary _customDict;           // custom dictionary
        private ISpellParser _customParser;             // custom spell-check parser
        private SpellOptions _spellOptions;             // as-you-type spell-check options
        private SuggestionBuilder _suggestionBuilder;   // class used to build suggestion lists
        private bool _initializing;                     // for ISupportInitialize
        private WordList _ignoreList;                   // list of words to ignore while spell-checking
        private WordDictionary _autoReplaceList;        // list of words to replace automatically
        private char[] _urlFileChars;

        #endregion

        //-----------------------------------------------------------------------------
        #region ** ctors

        /// <summary>
        /// Initializes a new instance of a <see cref="C1SpellChecker"/>.
        /// </summary>
        public C1SpellCheckerComponent()
        {
            InitializeComponent();
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** ISupportInitialize

        void ISupportInitialize.BeginInit()
        {
            _initializing = true;
        }
        void ISupportInitialize.EndInit()
        {
            _initializing = false;
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** private

        // initialize the component
        private void InitializeComponent()
        {
            // initialize statics
            if (_urlFileChars == null)
            {
                _urlFileChars = CharRange.URL_FILE_CHARS.ToCharArray();
            }

            // initialize properties
            _ignoreList = new WordList(this);
            _autoReplaceList = new WordDictionary();
            _spellOptions = new SpellOptions(this);
            _suggestionBuilder = new SuggestionBuilder(this);

            // create dictionaries
            _mainDict = new SpellDictionary(this);
            _userDict = new UserDictionary(this);
            //_userDictionary = ...
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** object model

        #region -- properties

        /// <summary>
        /// Gets a value that indicates whether the main spelling dictionary has been loaded.
        /// </summary>
        /// <remarks>
        /// <para>If <see cref="C1SpellChecker"/> cannot find the spelling dictionary, it will not throw
        /// any exceptions. The <b>Enabled</b> property will be set to false and the component will not 
        /// be able to spell-check any text.</para>
        /// <para>You can check the state of the <see cref="MainDictionary"/> and the <see cref="UserDictionary"/>
        /// by inspecting their <see cref="SpellDictionaryBase.State"/> property.</para>
        /// </remarks>
        [
        Browsable(false),
        C1Description("Enabled", "Gets a value that indicates whether the main spelling dictionary has been loaded.")
        ]
        public bool Enabled
        {
            get
            {
                // never enabled at design time
                if (_initializing || DesignMode)
                    return false;

                // the main dictionary should be loaded
                return _mainDict.EnsureLoaded();// || _userDict.EnsureLoaded();
            }
        }
        /// <summary>
        /// Gets a <see cref="SpellOptions"/> object that specifies spell-checking options.
        /// </summary>
        /// <remarks>
        /// Options available include types of words to ignore, whether to display suggestions in a context menu,
        /// the number of suggestions to display, and so on.
        /// </remarks>
        [
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        C1Description("Options", "Gets a SpellOptions object that specifies spell-checking options.")
        ]
        public SpellOptions Options
        {
            get { return _spellOptions; }
        }
        /// <summary>
        /// Gets the main dictionary used for spell-checking.
        /// </summary>
        /// <remarks>
        /// <para>This is the main dictionary. It is stored in a compressed format, and is read-only.</para>
        /// <para>You can edit the spell dictionaries or create your own using the
        /// <b>C1DictionaryEditor</b> utility that ships with <see cref="C1SpellChecker"/>.
        /// This option can be useful for domain-specific applications that require the
        /// use of technical jargon (for example, medical, legal, and so on).</para>
        /// <para>Users cannot add words to this dictionary, only to the <see cref="UserDictionary"/>.</para>
        /// </remarks>
        [
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        C1Description("MainDictionary", "Gets the main dictionary used for spell-checking.")
        ]
        public SpellDictionary MainDictionary
        {
            get { return _mainDict; }
        }
        /// <summary>
        /// Gets the user dictionary used for spell-checking.
        /// </summary>
        /// <remarks>
        /// This is the user dictionary. The user may add words to this dictionary using the
        /// spell-checking dialog, and it is automatically persisted to the specified file.
        /// </remarks>
        [
        DesignerSerializationVisibility(DesignerSerializationVisibility.Content),
        C1Description("UserDictionary", "Gets the user dictionary used for spell-checking.")
        ]
        public UserDictionary UserDictionary
        {
            get { return _userDict; }
        }
        /// <summary>
        /// Gets or sets a custom spell-checking dictionary.
        /// </summary>
        /// <remarks>
        /// This property allows you to specify additional custom dictionaries to be used 
        /// in addition to the standard <see cref="MainDictionary"/> and <see cref="UserDictionary"/>
        /// dictionaries.
        /// </remarks>
        [
        Browsable(false),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        C1Description("CustomDictionary", "Gets or sets a custom spell-checking dictionary.")
        ]
        public ISpellDictionary CustomDictionary
        {
            get { return _customDict; }
            set
            {
                if (_customDict != value)
                {
                    _customDict = value;
                }
            }
        }
        /// <summary>
        /// Gets or sets a custom spell-checking parser.
        /// </summary>
        /// <remarks>
        /// <para>This property allows you to specify a custom parser for breaking up text into words
        /// to be spell-checked.</para>
        /// <para>For example, you may want to use a custom parser when checking source code files. 
        /// This way you can spell-check the comments in the code and skip the actual code.</para>
        /// </remarks>
        [
        Browsable(false),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        C1Description("CustomParser", "Gets or sets a custom spell-checking parser.")
        ]
        public ISpellParser CustomParser
        {
            get { return _customParser; }
            set
            {
                if (_customParser != value)
                {
                    _customParser = value;
                }
            }
        }
        /// <summary>
        /// Gets the list of words to ignore during spell-checking.
        /// </summary>
        /// <remarks>
        /// <para>The list is used by the built-in spell dialog and by the as-you-type mechanism.</para>
        /// <para>The user may add words to this list using the "Ignore All" button in the spell dialog 
        /// or the "Ignore All" option in the as-you-type context menu.</para>
        /// <para>Strings in the list are case-sensitive.</para>
        /// </remarks>
        [
        Browsable(false),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        C1Description("IgnoreList", "Gets the list of words to ignore during spell-checking.")
        ]
        public WordList IgnoreList
        {
            get { return _ignoreList; }
        }
        /// <summary>
        /// Gets a list of words and replacements to use while spell-checking.
        /// </summary>
        /// <remarks>
        /// <para>This list is used by the built-in spell dialog and by the as-you-type mechanism.</para>
        /// <para>The built-in spell dialog checks the list when it finds a misspelled word. If a match
        /// is found, the misspelled word is replaced with the corresponding list entry.</para>
        /// <para>The as-you-type mechanism looks for matches whenever the user types a character that
        /// is not a letter or a digit. If a match is found, the text is replaced with the corresponding
        /// list entry.</para>
        /// <para>Note the difference in operation between the two modes. In as-you-type spell-checking,
        /// any matches will be replaced, whether the key is spelled correctly or not. In dialog mode,
        /// keys that are spelled correctly will not be flagged as errors and therefore will not be 
        /// replaced. For example, if the <b>AutoReplaceList</b> contains an entry with the strings
        /// ("attn.", "attention of"), then typing "attn. " into the editor will trigger a replacement 
        /// and the editor will contain "attention of ". However, because "attn." is not a spelling 
        /// error, the spell dialog will not replace instances of the string "attn." with "attention of".</para>
        /// <para>The <see cref="C1SpellChecker"/> fires the <see cref="AutoReplace"/> event before
        /// making each automatic replacement. The event handler may cancel the replacement.</para>
        /// <para>Strings in the list are case-sensitive.</para>
        /// </remarks>
        /// <example>
        /// The example below causes the <see cref="C1SpellChecker"/> to monitor spelling on a 
        /// <see cref="TextBox"/> control. If the user types the string "teh", it is automatically 
        /// replaced with "the". If the user types "cant", it is automatically replaced with "can't".
        /// <code>
        /// // build AutoReplace list
        /// c1SpellChecker1.AutoReplaceList.Clear();
        /// c1SpellChecker1.AutoReplaceList.Add("teh", "the");
        /// c1SpellChecker1.AutoReplaceList.Add("cant", "can't");
        /// 
        /// // activate as-you-type spell-checking on textBox1
        /// c1SpellChecker1.SetSpellChecking(textBox1, true);
        /// </code>
        /// </example>
        [
        Browsable(false),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden),
        C1Description("AutoReplaceList", "Gets the dictionary of automatic replacements used during spell-checking.")
        ]
        public WordDictionary AutoReplaceList
        {
            get { return _autoReplaceList; }
        }
        #endregion

        #region -- basic spell-checking (batch mode)

        /// <summary>
        /// Checks a string containing text and returns a list of spelling errors.
        /// </summary>
        /// <param name="text">Text to be spell-checked.</param>
        /// <param name="start">Index of the character where to start checking.</param>
        /// <param name="length">Length of the string to check.</param>
        /// <returns>List of <see cref="CharRange"/> objects that describe the spelling mistakes.</returns>
        public CharRangeList CheckText(string text, int start, int length)
        {
            // initialize error list
            CharRangeList errors = new CharRangeList();

            // save value of start parameter, reset to check all the text
            int userStart = start;
            start = 0;

            // spell-check if we have a dictionary loaded
            if (Enabled)
            {
                // loop to break the words
                string previousWord = null;
                int end = start + length;
                for (; start <= end; )
                {
                    // get next word, break when done
                    CharRange checkWord = _customParser != null
                        ? _customParser.GetNextWord(text, start, Options.Ignore, previousWord)
                        : CharRange.GetNextWord(text, start, Options.Ignore, previousWord);

                    // stop when we can
                    if (checkWord == null || checkWord.Start > end)
                    {
                        break;
                    }

                    // check this word if it is in range
                    if (checkWord.End >= userStart)
                    {
                        // analyze this word
                        if (checkWord.Duplicate)
                        {
                            // all duplicates are errors
                            errors.Add(checkWord);
                        }
                        else
                        {
                            // check this word
                            if (!CheckWord(checkWord.Text))
                            {
                                // check failed, look for abbreviations
                                bool abbreviation = false;
                                int next = checkWord.Start + checkWord.Text.Length;

                                // if the next character is a period
                                if (next < text.Length && text[next] == '.')
                                {
                                    if (CheckWord(checkWord.Text + '.'))
                                    {
                                        abbreviation = true;
                                    }
                                }

                                if (!abbreviation)
                                {
                                    // abbreviation failed too...
                                    errors.Add(checkWord);
                                }
                            }
                        }
                    }

                    // move on to next word
                    start = checkWord.Start + checkWord.Text.Length;
                    previousWord = checkWord.Text;
                }
            }

            // return error list
            return errors;
        }
        /// <summary>
        /// Checks a string containing text and returns a list of spelling errors.
        /// </summary>
        /// <param name="text">Text to be spell-checked.</param>
        /// <param name="start">Index of the character where to start checking.</param>
        /// <returns>List of <see cref="CharRange"/> objects that describe the spelling mistakes.</returns>
        public CharRangeList CheckText(string text, int start)
        {
            return CheckText(text, start, text.Length);
        }
        /// <summary>
        /// Checks a string containing text and returns a list of spelling errors.
        /// </summary>
        /// <param name="text">Text to be spell-checked.</param>
        /// <returns>List of <see cref="CharRange"/> objects that describe the spelling mistakes.</returns>
        public CharRangeList CheckText(string text)
        {
            return CheckText(text, 0, text.Length);
        }
        /// <summary>
        /// Checks a word to see if it's spelled correctly.
        /// </summary>
        /// <param name="word">Word to check.</param>
        /// <returns>True if the word is in the dictionary, false otherwise.</returns>
        public virtual bool CheckWord(string word)
        {
            // allow custom parser to filter the word
            if (CustomParser != null)
            {
                word = CustomParser.FilterWord(word);
            }

            // trivial case
            if (string.IsNullOrEmpty(word))
            {
                return true;
            }

            // look up in main dictionary
            if (_mainDict.Contains(word))
            {
                return true;
            }

            // look up in user dictionary
            if (_userDict.Contains(word))
            {
                return true;
            }

            // look up in custom dictionary
            if (_customDict != null && _customDict.Contains(word))
            {
                return true;
            }

            // look up in ignore words list
            if (_ignoreList.Contains(word))
            {
                return true;
            }

            // honor ignore options
            if (IgnoreWord(word))
            {
                return true;
            }

            // failed...
            return false;
        }
        /// <summary>
        /// Gets an array containing suggestions for a misspelled word.
        /// </summary>
        /// <param name="word">Word to suggest alternatives for.</param>
        /// <param name="maxCount">Maximum number of suggestions to provide.</param>
        /// <returns>An array containing suggestions for the <paramref name="word"/> parameter.</returns>
        public string[] GetSuggestions(string word, int maxCount)
        {
            return _suggestionBuilder.GetSuggestions(word, maxCount);
        }
        /// <summary>
        /// Gets an array containing suggestions for a misspelled word.
        /// </summary>
        /// <param name="word">Word to suggest alternatives for.</param>
        /// <returns>An array containing suggestions for the <paramref name="word"/> parameter.</returns>
        public string[] GetSuggestions(string word)
        {
            return GetSuggestions(word, 15);
        }

        #endregion

        #region -- generate suggestions

        private void GetSuggestions(string word, List<string> list, int maxCount)
        {
            // try more common mistakes first 
            // so they appear at the top of the list
            CapitalizeChar(word, list, maxCount);
            DropChar(word, list, maxCount);
            AddChar(word, list, maxCount);
            ChangeChar(word, list, maxCount);
            SwapChar(word, list, maxCount);
            SplitWord(word, list, maxCount);
        }
        private void CapitalizeChar(string word, List<string> list, int maxCount)
        {
            // try capitalizing the first letter (e.g. "I'll" for "i'll")
            if (char.IsLower(word, 0))
            {
                string upper = char.ToUpper(word[0]) + word.Substring(1);
                TryWord(upper, list, maxCount);
            }
        }
        private void SwapChar(string word, List<string> list, int maxCount)
        {
            // setup
            int len = word.Length;
            char[] tmp = new char[len];
            word.CopyTo(0, tmp, 0, len);

            // try changes
            for (int i = 0; i < len - 1; i++)
            {
                // check that both have the same case
                if ((char.IsLower(tmp[i]) == char.IsLower(tmp[i + 1])))
                {
                    // swap chars
                    char ch = tmp[i];
                    tmp[i] = tmp[i + 1];
                    tmp[i + 1] = ch;

                    // test new string
                    TryWord(new string(tmp), list, maxCount);

                    // unswap chars
                    ch = tmp[i];
                    tmp[i] = tmp[i + 1];
                    tmp[i + 1] = ch;
                }
            }
        }
        private void DropChar(string word, List<string> list, int maxCount)
        {
            int len = word.Length;
            if (len > 2)
            {
                for (int i = 0; i < len; i++)
                {
                    // drop char
                    StringBuilder sb = new StringBuilder(len);
                    sb.Append(word.Substring(0, i));
                    sb.Append(word.Substring(i + 1));

                    // test new string
                    TryWord(sb.ToString(), list, maxCount);
                }
            }
        }
        private void AddChar(string word, List<string> list, int maxCount)
        {
            int len = word.Length;
            for (int i = 0; i <= len; i++)
            {
                // try apostrophe
                StringBuilder sb = new StringBuilder(len + 1);
                sb.Append(word.Substring(0, i));
                sb.Append('\'');
                sb.Append(word.Substring(i));
                TryWord(sb.ToString(), list, maxCount);

                // and try all lowercase chars
                for (char ch = 'a'; ch <= 'z'; ch++)
                {
                    sb = new StringBuilder(len + 1);
                    sb.Append(word.Substring(0, i));
                    sb.Append(ch);
                    sb.Append(word.Substring(i));
                    TryWord(sb.ToString(), list, maxCount);
                }
            }
        }
        private void ChangeChar(string word, List<string> list, int maxCount)
        {
            // setup
            int len = word.Length;
            char[] tmp = new char[len];
            word.CopyTo(0, tmp, 0, len);

            // try changing each character
            for (int i = 0; i < len; i++)
            {
                // save char
                char ch = tmp[i];

                // get bounds
                int charOffset = (char.IsLetter(ch) && char.IsUpper(ch)) ? 'A' - 'a' : 0;

                // switch
                for (char ctry = 'a'; ctry <= 'z'; ctry++)
                {
                    if (ctry != ch)
                    {
                        tmp[i] = (char)(ctry + charOffset);
                        TryWord(new string(tmp), list, maxCount);
                    }
                }

                // restore char
                tmp[i] = ch;
            }
        }
        private void SplitWord(string word, List<string> list, int maxCount)
        {
            int len = word.Length;
            if (len > 4)
            {
                // try changes
                for (int i = 1; i < len - 1; i++)
                {
                    string w1 = word.Substring(0, i);
                    if (CheckWord(w1))
                    {
                        string w2 = word.Substring(i);
                        if (CheckWord(w2))
                        {
                            // can't call TryWord here since we really have two words
                            string trial = string.Format("{0} {1}", w1, w2);
                            if (list.Count < maxCount)
                            {
                                if (!list.Contains(trial))
                                {
                                    list.Add(trial);
                                }
                            }
                        }
                    }
                }
            }
        }
        private void TryWord(string word, List<string> list, int maxCount)
        {
            if (list.Count < maxCount)
            {
                if (!list.Contains(word) && CheckWord(word))
                {
                    list.Add(word);
                }
            }
        }

        #endregion

        #endregion

        //-----------------------------------------------------------------------------
        #region ** internal utilities

        // checks whether this word should be ignored during the spell-check
        internal bool IgnoreWord(string word)
        {
            IgnoreOptions ignore = Options.Ignore;

            // ignore Html tags
            if ((ignore & IgnoreOptions.HtmlTags) != 0 && word[0] == '<')
            {
                return true;
            }

            // ignore files and Urls
            if ((ignore & IgnoreOptions.Urls) != 0)
            {
                int pos = word.IndexOfAny(_urlFileChars);
                if (pos > -1 && pos < word.Length - 1)
                {
                    return true;
                }
            }

            // count chars of each type
            int digits = 0;
            int upper = 0;
            int lower = 0;
            int symbols = 0;
            int len = word.Length;
            for (int i = 0; i < len; i++)
            {
                if (char.IsDigit(word, i))
                {
                    digits++;
                }
                else if (char.IsUpper(word, i))
                {
                    upper++;
                }
                else if (char.IsLower(word, i))
                {
                    lower++;
                }
                else
                {
                    symbols++;
                }
            }

            // ignore all digits/symbols
            if (digits + symbols == len)
            {
                return true;
            }

            // ignore words with digits
            if ((ignore & IgnoreOptions.Numbers) != 0 && digits > 0)
            {
                return true;
            }

            // ignore all upper-case
            if ((ignore & IgnoreOptions.UpperCase) != 0 && upper + symbols == len)
            {
                return true;
            }

            // ignore mixed case
            if ((ignore & IgnoreOptions.MixedCase) != 0 && upper > 0 && lower > 0)
            {
                if (char.IsLower(word, 0) || upper > 1)
                    return true;
            }

            // do not ignore this one
            return false;
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** events

        /// <summary>
        /// Event that fires when a spelling error is found by the C1SpellChecker.
        /// </summary>
        /// <remarks>
        /// This event fires when the component detects a spelling error in a control. 
        /// This happens while a spell dialog is displayed (see the CheckControl(ISpellCheckableEditor 
        /// method) or while the component is painting the red wavy underlines that mark misspelled words
        /// in dynamic mode (see the SetSpellChecking method).
        /// The event parameters have a Dialog property that allow the handler to determine
        /// whether a spell dialog is being displayed or not.
        /// The event parameters also have a Cancel property that can be used to ignore 
        /// the error.
        /// </remarks>
        public event BadWordEventHandler BadWordFound;
        /// <summary>
        /// Raises the <see cref="OnBadWordFound"/> event.
        /// </summary>
        /// <param name="e"><see cref="BadWordEventArgs"/> that contains the event data.</param>
        public virtual void OnBadWordFound(BadWordEventArgs e)
        {
            if (BadWordFound != null)
                BadWordFound(this, e);
        }
        /// <summary>
        /// Event that fires before the <see cref="C1SpellChecker"/> makes an automatic replacement
        /// in the control's text.
        /// </summary>
        /// <remarks>
        /// <see cref="C1SpellChecker"/> automatically replaces text as the user types based on the
        /// list of words specified by the <see cref="AutoReplaceList"/> property.
        /// </remarks>
        public event AutoReplaceEventHandler AutoReplace;
        /// <summary>
        /// Raises the <see cref="AutoReplace"/> event.
        /// </summary> 
        /// <param name="e"><see cref="AutoReplaceEventArgs"/> that contains the event data.</param>
        public virtual void OnAutoReplace(AutoReplaceEventArgs e)
        {
            if (AutoReplace != null)
                AutoReplace(this, e);
        }
        #endregion

        //-----------------------------------------------------------------------------
        #region ** testing

#if DEBUG
        /// <summary>
        /// 
        /// </summary>
        public void Test()
        {
            TestSuggestions();
        }

        /// <summary>
        /// 
        /// </summary>
        public void TestSuggestions()
        {
            // handle underscore
            TestSuggestions("super_man", "superman");
            TestSuggestions("SUPER_MAN", "SUPERMAN");
            TestSuggestions("ADASA_MALASA", string.Empty);

            // capitalize first char ('paul' -> 'Paul')
            TestSuggestions("paul", "Paul");
            TestSuggestions("albert", "Albert");
            TestSuggestions("i'll", "I'll");

            // swap pairs of adjacent characters ('becuase' -> 'because')
            TestSuggestions("becuase", "because");
            TestSuggestions("ebcause", "because");
            TestSuggestions("becaues", "because");

            // remove extra chars and capitalize
            TestSuggestions("Praul", "Paul");
            TestSuggestions("Praul", "Raul");

            // remove extra chars ('commmon' -> 'common')
            TestSuggestions("comxmon", "common");
            TestSuggestions("xcommon", "common");
            TestSuggestions("commonx", "common");

            // insert characters at every position ('comon' -> 'common')
            TestSuggestions("drizly", "drizzly");
            TestSuggestions("comon", "common");
            TestSuggestions("commo", "common");
            TestSuggestions("ommon", "common");

            // insert apostrophe
            TestSuggestions("dont", "don't");
            TestSuggestions("wont", "won't");
            TestSuggestions("YOULL", "YOU'LL");
            TestSuggestions("SHANT", "SHAN'T");

            // substitute characters ('tezt' -> 'test')
            TestSuggestions("tezt", "test");
            TestSuggestions("eest", "test");
            TestSuggestions("tesl", "test");

            // split word into two ('bluesky' -> 'blue sky')
            TestSuggestions("keyboards", "key boards");
            TestSuggestions("bluesky", "blue sky");
            TestSuggestions("orus", "or us");

            // capitalization
            TestSuggestions("THER", "THERE");
            TestSuggestions("KEYBOAR", "KEYBOARD");
            TestSuggestions("EYBOARD", "KEYBOARD");
            TestSuggestions("KEYOARD", "KEYBOARD");
            TestSuggestions("KEYBXOARD", "KEYBOARD");
            TestSuggestions("KEBYOARD", "KEYBOARD");
            TestSuggestions("KEYBBARD", "KEYBOARD");
            TestSuggestions("keyboArd", "keyboards");

            // weird chars
            TestSuggestions("naxve", string.Empty);
            TestSuggestions("naivetx", string.Empty);
            TestSuggestions("faxade", string.Empty);
            TestSuggestions("nxe", string.Empty);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bad"></param>
        /// <param name="good"></param>
        public void TestSuggestions(string bad, string good)
        {
            bool ok = good.Length == 0;
            Console.WriteLine("testing '{0}' -> '{1}'", bad, good);
            if (CheckWord(bad))
            {
                Console.WriteLine("NOTE: '{0}' looks like a good word to begin with...", bad);
            }
            foreach (string word in GetSuggestions(bad, 100))
            {
                Console.Write("\t{0}", word);
                if (word == good)
                {
                    Console.Write(" <---");
                    ok = true;
                }
                Console.WriteLine();
            }
            if (!ok)
                throw new Exception("Didn't find suggestion " + good);
        }
#endif

        #endregion

    }
}

