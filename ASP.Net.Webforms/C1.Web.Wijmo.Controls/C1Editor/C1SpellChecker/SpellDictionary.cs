// Define the USE_HASHKEY symbol to use string CRCs as keys into the dictionary 
// instead of using the strings themselves.
//
// This results in a dictionary that is very compact (much smaller than using the 
// strings themselves as keys), and is close to being 100% reliable.
//
// 'Reliable' in this context means a given word will have a unique code. That is 
// 100% guaranteed if the strings themselves are used as keys, but not quite when
// codes are used. If two words happen to have the same CRC code, and one of them
// is in the dictionary, the other will also be treated as a member.
//
// The 'SuperHash' CRC used here combines the CRC used in Zip with the GetHashCode() 
// in .NET. This combination ensures 100% reliability for all our dictionaries, 
// some of which have in excess of 400,000 words. The CRC alone (without hashcode)
// gets close to this but not quite.
//
// ** en-US.dct:
// String:      Done reading 118,038 words in 297 milliseconds, using 4,424,744 bytes.
// SuperHash:   Done reading 118,038 words in 250 milliseconds, using 133,984 bytes (!?!?).
//
// ** de-DE.dct:
// String:      Done reading 431,753 words in 1,125 milliseconds, using 28,793,080 bytes.
// SuperHash:   Done reading 431,753 words in 938 milliseconds, using 14,593,396 bytes.
//
// ** es-ES.dct:
// String:      Done reading 447,721 words in 1,156 milliseconds, using 27,906,568 bytes.
// SuperHash:   Done reading 447,721 words in 938 milliseconds, using 14,593,440 bytes.
//
#define USE_HASHKEY

using System;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Drawing.Design;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Editor.C1SpellChecker
{
	using C1.C1Zip;

	/// <summary>
	/// Class that contains a list of words and methods looking them up and loading them from a compressed file.
	/// </summary>
	public class SpellDictionary : SpellDictionaryBase
	{
		//-----------------------------------------------------------------------------
		#region ** fields

		internal string             _password;          // password used to load the file
		internal DictionaryEntries  _entries;           // word list in use by this dictionary
		internal List<string>       _ignoreSuffixes;    // ignore these suffixes (e.g. 's)
		internal List<string>       _ignorePrefixes;    // ignore these prefixes (e.g. d')


		// static table with file names and word lists (load once and share)
		private static Dictionary<string, DictionaryEntries> _wordLists = new Dictionary<string, DictionaryEntries>();
        // fixed an issue: the _wordLists is static, then can be cache;
        // but _ignorePrefixes has lose the state, so cache the _ignorePrefixes
        // if there still some problem, then make the _wordLists not a static variable:
        //  when do spell check then load the file every time.
        // TODO: cache _ignoreSuffixes
        private static List<string> _ignorePrefixesCache = new List<string>(); 

		// default dictionary
		// (empty string means use embedded dct resource)
		private const string MAIN_DICTIONARY = ""; // "C1Spell_en-US.dct";

		#endregion

		//-----------------------------------------------------------------------------
		#region ** ctor

		internal SpellDictionary(C1SpellCheckerComponent spell) : base(spell)
		{
			// initialize state
			_ignoreSuffixes = new List<string>();
			_ignoreSuffixes.Add("'s");              // good for English, can be overridden in dictionary rules
			_ignorePrefixes = new List<string>();   // empty by default, can be overridden in dictionary rules

			// initialize properties
			Enabled = true;
			FileName = MAIN_DICTIONARY;
			Password = string.Empty;
		}

		#endregion

		//-----------------------------------------------------------------------------
		#region ** object model

		/// <summary>
		/// Gets or sets the name of the file that contains the dictionary.
		/// </summary>
		[
		Description("Gets or sets the name of the file that contains the dictionary."),
		DefaultValue(MAIN_DICTIONARY)
		]
		public override string FileName
		{
			get { return base.FileName; }
			set
			{
				if (FileName != value)
				{

					// change dictionary
					base.FileName = value;
					base.State = DictionaryState.Empty;
					_entries = null;

				}
			}
		}
		/// <summary>
		/// Sets the password needed to load password-protected dictionaries.
		/// </summary>
		/// <remarks>
		/// <para>If the dictionary file in use is password-protected, you must provide a
		/// valid password in order to use it.</para>
		/// <para>The dictionary files that ship with <see cref="C1SpellChecker"/> are not
		/// password-protected. You can create password-protected dictionaries using the
		/// dictionary utility shipped with <see cref="C1SpellChecker"/>.</para>
		/// </remarks>
		[
		Description("Sets the password needed to load password-protected dictionaries."),
		DefaultValue("")
		]
		public virtual string Password
		{
			get { return _password; }
			set
			{
				if (_password != value)
				{
					_password = value;
					m_state = DictionaryState.Empty;
					_entries = null;
				}
			}
		}
		/// <summary>
		/// Gets the number of words loaded in the dictionary.
		/// </summary>
		[Browsable(false)]
		public int WordCount
		{
			get { return _entries != null ? _entries.Count : 0; }
		}
		/// <summary>
		/// Checks whether the dictionary contains a given word.
		/// </summary>
		/// <param name="word">Word to lookup.</param>
		/// <returns>True if the dictionary contains the word, false otherwise.</returns>
		override public bool Contains(string word)
		{
			// make sure we're loaded and enabled
			if (!EnsureLoaded())
			{
				return false;
			}

			// trivial case
			if (string.IsNullOrEmpty(word))
			{
				return true;
			}

			// easy case
			if (_entries.Contains(word))
			{
				return true;
			}

			// failed; don't give up yet...
			bool changed = false;

			// replace curly quotes with straight
			if (word.IndexOf(CharRange.QUOTE_REV) > -1)
			{
				word = word.Replace(CharRange.QUOTE_REV, CharRange.QUOTE);
				changed = true;
			}
			if (word.IndexOf(CharRange.QUOTE_CURLY) > -1)
			{
				word = word.Replace(CharRange.QUOTE_CURLY, CharRange.QUOTE);
				changed = true;
			}

			// remove common suffixes, prefixed
			foreach (string prefix in _ignorePrefixes)
			{
				if (word.StartsWith(prefix, StringComparison.InvariantCultureIgnoreCase))
				{
					word = word.Substring(prefix.Length);
					changed = true;
					break;
				}
			}
			foreach (string suffix in _ignoreSuffixes)
			{
				if (word.EndsWith(suffix, StringComparison.InvariantCultureIgnoreCase))
				{
					word = word.Substring(0, word.Length - suffix.Length);
					changed = true;
					break;
				}
			}

			// try again
			if (changed)
			{
				return _entries.Contains(word);
			}

			// not found
			return false;
		}

		#endregion

		//-----------------------------------------------------------------------------
		#region ** protected

		/// <summary>
		/// Loads the dictionary from the file specified by the <see cref="FileName"/> property.
		/// </summary>
		/// <returns>True if the dictionary was loaded successfully, false otherwise.</returns>
		/// <remarks>
		/// If the dictionary fails to load, this method returns false. In this case,
		/// you can check the value of the dictionary's <see cref="SpellDictionaryBase.State"/> property to determine 
		/// why the file failed to load.
		/// </remarks>
		protected override bool Load()
		{
			// not loaded yet
			State = DictionaryState.Empty;
			_entries = null;

			// get fully qualified filename
			string path = GetFileName(true);

			// file not found?
			// note: FileName = empty means use default en-US dictionary from app resource.
			if (string.IsNullOrEmpty(FileName) == false &&
				string.IsNullOrEmpty(path) == true)
			{
				State = DictionaryState.FileNotFound;
				return false;
			}

			// load from cache if possible
			if (_wordLists.ContainsKey(path))
			{
				State = DictionaryState.Loaded;
				_entries = _wordLists[path];
                _ignorePrefixes = _ignorePrefixesCache;
				return true;
			}

			// create and load word list
			_entries = new DictionaryEntries(150000);
			State = _entries.Load(this, path, _password);

			// save word list in static cache so other instances may share it
			if (State == DictionaryState.Loaded)
			{
				_wordLists[path] = (DictionaryEntries)_entries;
                _ignorePrefixesCache = _ignorePrefixes;
			}

			// done
			return State == DictionaryState.Loaded;
		}

		#endregion
	}

	/// <summary>
	/// DictionaryEntries is a dictionary that associates:
	///   key: hash codes for the lower-case version of the strings
	///   value: bytes (0 for lower-case strings, 1 for upper-case strings)
	/// </summary>
	internal class DictionaryEntries 
#if USE_HASHKEY
		: Dictionary<long, byte>
#else
		: Dictionary<string, byte>
#endif
	{

		private const string DEFAULT_DIC_PATH = "C1.Web.Wijmo.Controls.C1Editor.C1SpellChecker.Resources.C1Spell_en-US.dct";

		//-----------------------------------------------------------------------------
		#region ** ctor

		/// <summary>
		/// Initializes a new instance of a <see cref="DictionaryEntries"/>.
		/// </summary>
		/// <param name="capacity">Initial capacity of the list.</param>
		public DictionaryEntries(int capacity)
			: base(capacity)
		{
		}

		#endregion

		//-----------------------------------------------------------------------------
		#region ** object model

		/// <summary>
		/// Adds a word to the list.
		/// </summary>
		/// <param name="word">Word to add to the list.</param>
		public bool Add(string word)
		{
			// trim word
			word = word.Trim();

			// add only if it has a length
			if (word.Length > 0)
			{
#if USE_HASHKEY
				// key is CRC for lowercase version of the string
				long key = GetCrc(word.ToLowerInvariant());
#else
				// key is lowercase version of the string
				string key = word.ToLowerInvariant();
#endif
				// if lowercase word is already in dictionary, don't add uppercase (nice / Nice)
				byte value;
				if (this.TryGetValue(key, out value))
				{
					if (value == 0)
					{
						return false;
					}
				}

				// add to dictionary using hash as key
				// the value indicates whether the original word started with an uppercase character
				this[key] = (byte)(char.IsUpper(word, 0) ? 1 : 0);
				return true;
			}

			// done
			return false;
		}
		/// <summary>
		/// Checks whether the dictionary contains a given word.
		/// </summary>
		/// <param name="word">Word to lookup.</param>
		/// <returns>True if the dictionary contains the word, false otherwise.</returns>
		public bool Contains(string word)
		{
#if USE_HASHKEY
			// key is CRC for lowercase version of the string
			long key = GetCrc(word.ToLowerInvariant());
#else
			// key is lowercase version of the string
			string key = word.ToLowerInvariant();
#endif
			// look up key
			byte value;
			if (TryGetValue(key, out value))
			{
				// handle case-sensitive mode
				if (value == 1 && word.Length > 0 && char.IsLower(word, 0))
				{
					return false;
				}

				// seems fine
				return true;
			}
			return false;
		}
		/// <summary>
		/// Loads the word list from a file.
		/// </summary>
		/// <param name="dictionary"><see cref="SpellDictionary"/> that owns the word list.</param>
		/// <param name="fileName">Fully qualified name of the file that contains the word list.</param>
		/// <param name="password">Password used to load the entries if the file is protected.</param>
		public DictionaryState Load(SpellDictionary dictionary, string fileName, string password)
		{
#if DEBUG
			long before = GC.GetTotalMemory(true);
			DateTime start = DateTime.Now;
#endif
			DictionaryState state = DictionaryState.InvalidFileFormat;
			try
			{
				// file name 
				if (string.IsNullOrEmpty(fileName))
				{
					// load from embedded resource (en-US)
					using (Stream stream = this.GetType().Assembly.GetManifestResourceStream(DEFAULT_DIC_PATH))
					{
						C1ZipFile zip = new C1ZipFile(stream);
						state = LoadFromZip(dictionary, zip);
					}
				}
				else
				{
					// check that file exists and is a valid zip file
					if (!File.Exists(fileName))
					{
						state = DictionaryState.FileNotFound;
					}
					else if (!C1ZipFile.IsZipFile(fileName))
					{
						state = DictionaryState.InvalidFileFormat;
					}
					else
					{
						// load from zip file
						C1ZipFile zip = new C1ZipFile(fileName, false);
						zip.Password = password;
						state = LoadFromZip(dictionary, zip);
					}
				}
			}
			catch { }
#if DEBUG
			TimeSpan ts = DateTime.Now.Subtract(start);
			long after = GC.GetTotalMemory(true);
			Console.WriteLine("Done reading '{3}': {0:#,##0} words in {1:#,##0} milliseconds, using {2:#,##0} bytes.",
				this.Count, ts.TotalMilliseconds, after - before, Path.GetFileName(fileName));
#endif
			// done
			return state;
		}
		
		// load the word list from a zip file
		private DictionaryState LoadFromZip(SpellDictionary dictionary, C1ZipFile zip)
		{
			foreach (C1ZipEntry ze in zip.Entries)
			{
				// make sure the password is good
				if (!ze.CheckPassword())
				{
					return DictionaryState.InvalidPassword;
				}

				// scan entries
				if (ze.FileName.EndsWith(".words", StringComparison.InvariantCultureIgnoreCase))
				{
					// read word list (*.words)
					Stream stream = ze.OpenReader();
					using (StreamReader sr = new StreamReader(stream, Encoding.UTF8))
					{
						// read each line in the file (each may contain multiple words)
						for (string line = sr.ReadLine(); line != null; line = sr.ReadLine())
						{
							// handle words with multiple suffixes (e.g. after  burner  math)
							string[] words = line.Split('\t', ' ');
							Add(words[0]);
							for (int i = 1; i < words.Length; i++)
								Add(words[0] + words[i]);
						}
					}
				}
				else if (string.Compare(ze.FileName, "rules", true) == 0)
				{
					// read rules (rules)
					Stream stream = ze.OpenReader();
					using (StreamReader sr = new StreamReader(stream, Encoding.UTF8))
					{
						// read each line in the file
						for (string line = sr.ReadLine(); line != null; line = sr.ReadLine())
						{
							string[] words = line.Split(' ');
							if (words.Length > 0)
							{
								if (words[0].StartsWith("IgnorePrefix", StringComparison.InvariantCultureIgnoreCase))
								{
									dictionary._ignorePrefixes.Clear();
									for (int index = 1; index < words.Length; index++)
										dictionary._ignorePrefixes.Add(words[index]);
								}
								else if (words[0].StartsWith("IgnoreSuffix", StringComparison.InvariantCultureIgnoreCase))
								{
									dictionary._ignoreSuffixes.Clear();
									for (int index = 1; index < words.Length; index++)
										dictionary._ignoreSuffixes.Add(words[index]);
								}
							}
						}
					}
				}
			}
			return DictionaryState.Loaded;
		}
	#endregion

		//-----------------------------------------------------------------------------
		#region ** private

#if USE_HASHKEY

		// compute a CRC key that is better than string.GetHashCode().
		// (adapted from the CRC code in C1Zip, also better than Adler)
		public static long GetCrc(string word)
		{
			// initialize
			uint crc = 0 ^ 0xffffffff;
			uint[] crc_table = C1.C1Zip.ZLib.CRC32.crc_table;

			// scan word
			for (int i = 0; i < word.Length; i++)
			{
				// get char
				char c = word[i];

				// convert to bytes (faster than Encoding.UTF32.GetBytes())
				byte b1 = (byte)(c & 0xff);
				byte b2 = (byte)((c >> 08) & 0xff);
				byte b3 = (byte)((c >> 16) & 0xff);
				byte b4 = (byte)((c >> 24) & 0xff);

				// encode bytes
				crc = crc_table[((int)crc ^ (b1)) & 0xff] ^ (crc >> 8);
				crc = crc_table[((int)crc ^ (b2)) & 0xff] ^ (crc >> 8);
				crc = crc_table[((int)crc ^ (b3)) & 0xff] ^ (crc >> 8);
				crc = crc_table[((int)crc ^ (b4)) & 0xff] ^ (crc >> 8);
			}

			// finish CRC
			crc ^= 0xffffffff;

			// combine with string hash to get another 32 bits
			return (long)(crc | ((long)word.GetHashCode() << 32));
		}
#endif
		#endregion
	}
}
