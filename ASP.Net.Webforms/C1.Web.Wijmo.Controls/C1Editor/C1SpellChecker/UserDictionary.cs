using System;
using System.IO;
using System.ComponentModel;
using System.Drawing.Design;
using System.Collections.Generic;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1Editor.C1SpellChecker
{
    /// <summary>
    /// Class that contains a list of additional words and methods for managing the list.
    /// </summary>
    /// <remarks>
    /// This class is used to store user dictionaries. It provides methods for adding and 
    /// removing words, as well as saving the dictionary.
    /// </remarks>
    public class UserDictionary : SpellDictionaryBase
    {
        //-----------------------------------------------------------------------------
        #region ** fields

        private UserDictionaryEntries   _entries;
        private string                  _saveFileName;
        private Exception               _saveException;

        private const string CUSTOM_DICTIONARY = "Custom.dct";

        #endregion

        //-----------------------------------------------------------------------------
        #region ** ctor

        internal UserDictionary(C1SpellCheckerComponent spell) : base(spell)
        {
            _entries = new UserDictionaryEntries();
            FileName = CUSTOM_DICTIONARY;
        }

        #endregion

        //-----------------------------------------------------------------------------
        #region ** object model

        /// <summary>
        /// Gets or sets the name of the file that contains the dictionary.
        /// </summary>
        [
        Description("Gets or sets the name of the file that contains the dictionary."),        
        DefaultValue(CUSTOM_DICTIONARY) // << override to change default
        ]
        override public string FileName
        {
            get { return base.FileName; }
            set 
            {
                if (value != FileName)
                {
                    base.FileName = value;
                    _saveFileName = GetFileName(false);
                    Load();
                }
            }
        }
        /// <summary>
        /// Adds a word to the dictionary.
        /// </summary>
        /// <param name="word">Word to add to the dictionary.</param>
        /// <returns>True if the word was added to the dictionary, false if it was already present.</returns>
        public bool AddWord(string word)
        {
            if (_entries.Add(word))
            {
                Save();
                return true;
            }
            return false;
        }
        /// <summary>
        /// Removes a word from the dictionary.
        /// </summary>
        /// <param name="word">Word to remove from the dictionary.</param>
        /// <returns>True if the word was removed from the dictionary, false if it was not found.</returns>
        public bool RemoveWord(string word)
        {
            if (_entries.ContainsKey(word))
            {
                _entries.Remove(word);
                Save();
                return true;
            }
            return false;
        }
        /// <summary>
        /// Clears the dictionary removing all words in it.
        /// </summary>
        public void Clear()
        {
            _entries.Clear();
            Save();
        }
        /// <summary>
        /// Saves the dictionary back to a file.
        /// </summary>
        /// <returns>True in case of success, false otherwise.</returns>
        /// <remarks>If the dictionary cannot be saved, the reason is stored in the <see cref="Exception"/> property.</remarks>
        public bool Save()
        {
            // save modified user dictionary
            try
            {
                // save
                _saveException = null;
                _entries.Save(_saveFileName);
            }
            catch (Exception x)
            {
                _saveException = x;
            }

            // done
            return _saveException == null;
        }
        #endregion

        //-----------------------------------------------------------------------------
        #region ** overrides

        /// <summary>
        /// Loads the dictionary from a file.
        /// </summary>
        /// <returns>True if the dictionary was loaded successfully, false otherwise.</returns>
        override protected bool Load()
        {
            // not loaded yet
            m_state = DictionaryState.Empty;
            _entries.Clear();
            _saveFileName = string.Empty;
            if (string.IsNullOrEmpty(FileName))
            {
                return false;
            }

            // load strings from file
            string fileName = GetFileName(false);
            _entries.Load(this, fileName, null);

            // loaded!
            _saveFileName = fileName;
            m_state = DictionaryState.Loaded;

            // done
            return true;
        }
        /// <summary>
        /// Checks whether the dictionary contains a given word.
        /// </summary>
        /// <param name="word">Word to lookup.</param>
        /// <returns>True if the dictionary contains the word, false otherwise.</returns>
        public override bool Contains(string word)
        {
            // make sure we're loaded and enabled
            if (!EnsureLoaded())
            {
                return false;
            }

            // trivial case
            if (string.IsNullOrEmpty(word))
            {
                return true;
            }

            // easy case
            if (_entries.Contains(word))
            {
                return true;
            }

            // no dice
            return false;
        }
        #endregion
    }
    /// <summary>
    /// DictionaryEntries is a dictionary that associates:
    ///   key: hash codes for lower case strings
    ///   value: bytes (0 for lower-case entry, 1 for upper-case)
    /// </summary>
    internal class UserDictionaryEntries : Dictionary<string, bool>
    {
        //-----------------------------------------------------------------------------
        #region ** object model

        /// <summary>
        /// Adds a word to the list.
        /// </summary>
        /// <param name="word">Word to add to the list.</param>
        public bool Add(string word)
        {
            // add if word has a length and is not present already
            if (!string.IsNullOrEmpty(word))
            {
                word = word.Trim();
                if (word.Length > 0 && !base.ContainsKey(word))
                {
                    base.Add(word, true);
                    return true;
                }
            }

            // empty or already present
            return false;
        }
        /// <summary>
        /// Checks whether the dictionary contains a given word.
        /// </summary>
        /// <param name="word">Word to lookup.</param>
        /// <returns>True if the dictionary contains the word, false otherwise.</returns>
        public bool Contains(string word)
        {
            return 
                base.ContainsKey(word) || 
                base.ContainsKey(word.ToLower());
        }
        /// <summary>
        /// Loads the word list from a UTF-8 encoded text file.
        /// </summary>
        /// <param name="dictionary"><see cref="SpellDictionary"/> that owns the word list.</param>
        /// <param name="fileName">Fully qualified name of the UTF-8 file that contains the word list.</param>
        /// <param name="password">Not used.</param>
        public DictionaryState Load(UserDictionary dictionary, string fileName, string password)
        {
            if (string.IsNullOrEmpty(fileName) || !File.Exists(fileName))
            {
                return DictionaryState.FileNotFound;
            }
            try
            {
                using (StreamReader sr = new StreamReader(fileName, Encoding.UTF8))
                {
                    while (!sr.EndOfStream)
                    {
                        Add(sr.ReadLine());
                    }
                }
            }
            catch 
            {
                return DictionaryState.InvalidFileFormat;
            }
            return DictionaryState.Loaded;
        }
        /// <summary>
        /// Saves the word list to an UTF-8 encoded file.
        /// </summary>
        /// <param name="fileName">Fully qualified name of the file to save</param>
        public void Save(string fileName)
        {
            // sort words
            List<string> list = new List<string>();
            foreach (string word in this.Keys)
                list.Add(word);
            list.Sort();

            // save sorted list
            using (StreamWriter sw = new StreamWriter(fileName, false, Encoding.UTF8))
            {
                foreach (string word in list)
                    sw.WriteLine(word);
            }
        }

        #endregion
    }
}
