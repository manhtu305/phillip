using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing.Design;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Security.Permissions;

using C1.Web.Wijmo.Controls.Localization;
using System.Collections.Specialized;
using C1.Web.Wijmo.Controls.Base.Interfaces;
using System.Globalization;
using C1.Web.Wijmo.Controls.C1Editor.C1SpellChecker;

namespace C1.Web.Wijmo.Controls.C1Editor
{

    /// <summary>
    /// Represents a C1Editor in an ASP.NET Web page.
    /// </summary>
#if ASP_NET45
    [Designer("C1.Web.Wijmo.Controls.Design.C1Editor.C1EditorDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1Editor.C1EditorDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1Editor.C1EditorDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1Editor.C1EditorDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    [ToolboxData("<{0}:C1Editor runat=server ></{0}:C1Editor>")]
	[ToolboxBitmap(typeof(C1Editor), "C1Editor.png")]
	[LicenseProviderAttribute()]
	public partial class C1Editor : C1TargetControlBase, INamingContainer, IC1Serializable, IPostBackDataHandler, ICallbackEventHandler, ICultureControl
	{

		#region ** fields
		private C1RibbonUI _ribbonUI = null;
		private string _commandArgument = "";
		private C1SpellChecker.C1SpellChecker _spellChecker = null;

		private const string CONST_DESIGNTIME_CSS = "wijmo-designtime";
		private const string CONST_EDITOR_CSS = "wijmo-wijeditor";

		private const string CONST_CMD_TEMPLATELIST = "tplList";
		private const string CONST_CMD_IMAGELIST = "imgList";
		private const string CONST_CMD_SAVETEMPLATE = "saveTpl";
		private const string CONST_CMD_DELETETEMPLATE = "delTpl";
		private const string CONST_CMD_DELETEIMAGE = "delImg";        
		private const string CONST_CMD_UPLOADIMAGE = "uploadImage";
		private const string CONST_CMD_UPLOADLINK = "uploadLink";
		private const string CONST_CMD_FRAMEHELPERNAME = "c1framehelper";
		private const string CONST_CMD_FILEUPLOADNAME = "c1fileupload";
		private const string CONST_CMD_SAVETEXT = "textSaved";

		private const string CONST_SPELLCHECKER_ID = "spellchecker";

		private readonly string[] ValidImageTypes = new string[] { "jpg", "jpeg", "jpe", "gif", "png", "bmp" };

		private bool _productLicensed = false;
		private bool _shouldNag;
		private LocalizationOption _LocalizationOption;
		private bool _isDelImgSucc = false;
		private DialogHelper _dialogHelper;
		#endregion end of ** fields.

		#region ** events
		/// <summary>
		/// TextChanged event.
		/// </summary>
		[C1Description("C1Editor.TextChanged", "Fires when the control's data binding expressions are to be evaluated.")]
        [C1Category("Category.Events")]
		public event EventHandler TextChanged;

		/// <summary>
		/// TextSaved event.
		/// </summary>
		[C1Description("C1Editor.TextSaved", "Fires when the user has saved text")]
        [C1Category("Category.Events")]
		public event EventHandler TextSaved;
		#endregion end of ** events.

		#region ** constructor
		/// <summary>
		/// Creates a new instance of the C1Editor class.
		/// </summary>
		public C1Editor()
		{
			this.Width = Unit.Pixel(750);
			this.Height = Unit.Pixel(500);

			VerifyLicense();
		}

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Editor), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}
		#endregion end of ** constructor.

		#region ** properties

		#region ** public properties

		/// <summary>
		/// Gets or sets culture ID.
		/// </summary>
		[C1Description("C1Editor.CultureInfo")]
		[C1Category("Category.Behavior")]
		[DefaultValue(typeof(CultureInfo), "en-US")]
		[TypeConverter(typeof(CultureInfoConverter))]
		[NotifyParentProperty(true)]
		[WidgetOption]
		[Browsable(false)]
		public CultureInfo Culture
		{
			get
			{
				return GetCulture(GetPropertyValue("Culture", CultureInfo.CurrentCulture));
			}
			set
			{
				SetPropertyValue("Culture", value);
			}
		}

		/// <summary>
		/// SpellChecker dictionary path.
		/// </summary>
		/// <example>
		/// Example of the relative path: "~/Dictionaries/C1Spell_de-DE.dct"
		/// </example>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.DictionaryPath", "SpellChecker dictionary path.")]
		[Layout(LayoutType.Appearance)]
		[DefaultValue("")]
		[WidgetOption]
		public string DictionaryPath
		{
			get
			{
				return this.GetPropertyValue<string>("DictionaryPath", "");
			}
			set
			{
				this.SetPropertyValue<string>("DictionaryPath", value);
			}
		}

		/// <summary>
		/// Gets or sets types of words to ignore during the spell-check.
		/// </summary>
		[
		C1Description("C1Editor.SpellCheckIgnoreOptions", "Gets or sets types of word to ignore during the spell-check."),
		DefaultValue(IgnoreOptions.Default),
		Editor(typeof(FlagEditor), typeof(UITypeEditor))
		]
		public IgnoreOptions SpellCheckIgnoreOptions
		{
			get
			{
				return this.GetPropertyValue<IgnoreOptions>("SpellCheckIgnoreOptions", IgnoreOptions.Default);
			}
			set
			{
				this.SetPropertyValue<IgnoreOptions>("SpellCheckIgnoreOptions", value);
			}
		}

		/// <summary>
		/// Gets or sets whether to show the virtual border for table.
		/// </summary>
		/// <remarks>It only works when the border style is not set in table.</remarks>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DefaultValue(true)]
		[WidgetOption]
		public bool TableVirtualBorderShowing
		{
			get
			{
				return this.GetPropertyValue<bool>("TableVirtualBorderShowing", true);
			}
			set
			{
				this.SetPropertyValue<bool>("TableVirtualBorderShowing", value);
			}
		}

		/*/// <summary>
		///  Indicates whether to show SpellChecker dialog or just allow a user to select suggestions from context menu.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.ShowSpellCheckerDialog", "Indicates whether to show SpellChecker dialog or just allow a user to select suggestions from context menu.")]
		[DefaultValue(true)]
		[WidgetOption]
		public bool ShowSpellCheckerDialog
		{
			get
			{
				return this.GetPropertyValue<bool>("ShowSpellCheckerDialog", true);
			}
			set
			{
				this.SetPropertyValue<bool>("ShowSpellCheckerDialog", value);
			}
		} */

		/*/// <summary>
		/// Editor mode (WYSIWYG/Code/Split).
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.EditorMode", "Editor mode (WYSIWYG/Code/Split).")]
		[DefaultValue(EditorMode.WYSIWYG)]
		[WidgetOption]
		public EditorMode EditorMode
		{
			get
			{
				return this.GetPropertyValue<EditorMode>("EditorMode", EditorMode.WYSIWYG);
			}
			set
			{
				this.SetPropertyValue<EditorMode>("EditorMode", value);
			}
		} 

		/// <summary>
		/// Show the path selector.
		/// </summary>
		[C1Description("C1Editor.ShowPathSelector", "Show the path selector.")]
		[C1Category("Category.Behavior")]
		[DefaultValue(true)]
		[WidgetOption]
		public bool ShowPathSelector
		{
			get
			{
				return this.GetPropertyValue<bool>("ShowPathSelector", true);
			}
			set
			{
				this.SetPropertyValue<bool>("ShowPathSelector", value);
			}
		} 

		/// <summary>
		/// Full screen mode. Set this property to true if you want to switch editor to FullScreen Mode.  All client area of the page will be covered by WebEditor.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.FullScreenMode", "Full screen mode. Set this property to true if you want to switch editor to FullScreen Mode.  All client area of the page will be covered by WebEditor.")]
		[DefaultValue(false)]
		[WidgetOption]
		public bool FullScreenMode
		{
			get
			{
				return this.GetPropertyValue<bool>("FullScreenMode", false);
			}
			set
			{
				this.SetPropertyValue<bool>("FullScreenMode", value);
			}
		}

		/// <summary>
		/// Full screen mode container ID. Use this property to specify container which will be used as the parent control for FullScreenMode instead of client's area on the web page.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.FullScreenModeContainerID", "Full screen mode container ID. Use this property to specify container which will be used as the parent control for FullScreenMode instead of client's area on the web page.")]
		[DefaultValue("")]
		[WidgetOption]
		public string FullScreenModeContainerID
		{
			get
			{
				return this.GetPropertyValue<string>("FullScreenModeContainerID", "");
			}
			set
			{
				this.SetPropertyValue<string>("FullScreenModeContainerID", value);
			}
		} */

		/// <summary>
		/// The HTML text to edit.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1Editor.Text", "The HTML text to edit.")]
		[Layout(LayoutType.Appearance)]
		[DefaultValue("")]
		[WidgetOption]
		public string Text
		{
			get
			{
				return this.GetPropertyValue<string>("Text", "");
			}
			set
			{
				this.SetPropertyValue<string>("Text", value);
			}
		}

		/// <summary>
		/// Gets or sets the virtual path of the folder, where C1Editor will automatically save the valid files after the FileUpload of dialog completes.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Editor.DialogUploadFolder", "Gets or sets the virtual path of the folder, where C1Editor will automatically save the valid files after the FileUpload of dialog completes.")]
		[DefaultValue("~/DialogUploadFolder")]
		[Layout(LayoutType.Behavior)]
		[WidgetOption]
		public string DialogUploadFolder
		{
			get
			{
				return this.GetPropertyValue<string>("DialogUploadFolder", "~/DialogUploadFolder");
			}
			set
			{
				this.SetPropertyValue<string>("DialogUploadFolder", value);
			}
		}

		/// <summary>
		/// Gets the instance of the class <seealso cref="C1RibbonUI"/>.
		/// </summary>
		[Browsable(false)]
		public C1RibbonUI RibbonUI
		{
			get
			{
				if (_ribbonUI == null)
				{
					_ribbonUI = new C1RibbonUI(this, IsDesignMode);
				}

				return _ribbonUI;
			}
		}


		/// <summary>
		/// Use the Localization property in order to customize localization strings.
		/// </summary>
        [C1Category("Category.Appearance")]
		[C1Description("C1Editor.Localization",
				"Use the Localization property in order to customize localization strings.")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[NotifyParentProperty(true)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TypeConverter(typeof(ExpandableObjectConverter))]
		//[Layout(LayoutType.Appearance)]
		[WidgetOption()]
		[Browsable(false)]
		public LocalizationOption Localization
		{
			get
			{
				if (_LocalizationOption == null)
				{
					_LocalizationOption = new LocalizationOption(this);
				}
				return _LocalizationOption;
			}
		}


		/// <summary>
		/// A hashtable value contains all information which need transfer from server to client.
		/// </summary>
		[WidgetOption]
		public override Hashtable InnerStates
		{
			get
			{
				base.InnerStates["uid"] = this.UniqueID;
				base.InnerStates["spcid"] = this.SpellChecker.ClientID;

				return base.InnerStates;
			}
		}
		#endregion end of ** public properties.

		#region ** override properties
		/// <summary>
		/// Gets or sets if control is enabled.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1Editor.Enabled", "Gets or sets if control is Enabled.")]
		[Layout(LayoutType.Behavior)]
		[WidgetOption]
		public override bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}

		/// <summary>
		///  Gets or sets the height of the C1Editor.
		/// </summary>
		[DefaultValue(typeof(Unit), "500")]
		[WidgetOption]
		public override Unit Height
		{
			get
			{
				return base.Height;
			}
			set
			{
				if (value.Value < 160)
				{
					value = Unit.Parse("160");
				}

				base.Height = value;
			}
		}

		/// <summary>
		/// Gets or sets the width of the C1Editor.
		/// </summary>
		[DefaultValue(typeof(Unit), "1150")]
		[WidgetOption]
		public override Unit Width
		{
			get
			{
				return base.Width;
			}
			set
			{
				base.Width = value;
			}
		}
		#endregion end of ** override properties.

		#region ** protected properties
		/// <summary>
		/// Changed the type of  control's  tag.
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}

		/// <summary>
		/// Registers an OnSubmit statement in order to save the states of the widget
		/// to json hidden input.
		/// </summary>
		protected override void RegisterOnSubmitStatement()
		{
			this.RegisterOnSubmitStatement("adjustOptions");
		}
		#endregion end of ** protected properties.

		#region internal/private
		internal override bool IsWijMobileControl
		{
			get
			{
				return false;
			}
		}

		private DialogHelper DialogHelper
		{
			get
			{
				if (_dialogHelper == null)
				{
					_dialogHelper = new DialogHelper(Culture);
				}
				return _dialogHelper;
			}
		}
		#endregion internal/private
		#endregion end of ** properties.

		#region ** methods
		#region ** protected methods
		/// <summary>
		/// Raises TextChanged event.
		/// </summary>
		/// <param name="e">The specified EventArgs object.</param>
		protected virtual void OnTextChanged(EventArgs e)
		{
			if (TextChanged != null)
			{
				TextChanged(this, e);
			}
		}

		/// <summary>
		/// Raises TextSaved event.
		/// </summary>
		/// <param name="e">The specified EventArgs object.</param>
		protected virtual void OnTextSaved(EventArgs e)
		{
			if (TextSaved != null)
			{
				TextSaved(this, e);
			}
		}
		#endregion end of ** protected methods.

		#region ** private methods
		//private string GetCallbackFunction(string arguments, string clientID, string context)
		//{
		//    return this.Page.ClientScript.GetCallbackEventReference(this, arguments, clientID + "_ClientCallBack", context, clientID + "_ClientErrorCallBack", true);
		//}

		private string DetermineCompoundCssClass()
		{
			if (this.IsDesignMode)
			{
				return string.Format("{0} {1}", "wijmo-designtime", "wijmo-wijeditor");
			}

			return "wijmo-wijeditor";
		}
		
		private C1SpellChecker.C1SpellChecker SpellChecker
		{
			get
			{
				if (this._spellChecker == null)
				{
					this._spellChecker = new C1SpellChecker.C1SpellChecker(Culture);
					this._spellChecker.ID = CONST_SPELLCHECKER_ID;
					this._spellChecker.DictionaryPath = this.DictionaryPath;
					this._spellChecker.SpellChecker.Options.Ignore = this.SpellCheckIgnoreOptions;
				}

				return this._spellChecker;
			}
		}
		#endregion end of ** private methods.

		#region ** override methods
		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that use composition-based
		/// implementation to create any child controls they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{
			this.Controls.Clear();

			foreach (Control ctrl in this.RibbonUI.Create())
			{
				this.Controls.Add(ctrl);
			}

			if (!this.IsDesignMode)
			{
				this.Controls.Add(this.SpellChecker);
				//update for fixing issue 19406 by wh at 2011/2/1
				//HtmlInputHidden hidText = new HtmlInputHidden();
				//hidText.Attributes["class"] = "C1EditorText";
				//hidText.ID = "Text";
				//end for issue 19406
			}
 
			base.CreateChildControls();
		}

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (this.IsDesignMode)
			{
				this.RegisterDesignTimeStyleSheet(writer, "C1.Web.Wijmo.Controls.C1Editor.Resources.c1editordesigner.css");
				if (CultureInfo.CurrentCulture.Name.Equals("ja-JP"))
				{
					writer.Write("<style media=\"all\" type=\"text/css\">");
					writer.Write(".wijmo-designtime .wijmo-wijeditor-container .wijmo-wijribbon .wijmo-wijribbon-font" +
					"{ 	width:26em;}");
					writer.Write("</style>");
				}
			}
			this.RibbonUI.SetRibbonContent(this.Text);
			base.Render(writer);
		}

		///// <summary>
		///// Create a spell checker dialog for the control.
		///// </summary>
		///// <returns></returns>
		//private C1SpellChecker.C1SpellChecker CreateSpellCheckerDialog()
		//{
		//    C1SpellChecker.C1SpellChecker dialog = new C1SpellChecker.C1SpellChecker(false);
		//    dialog.UseEmbeddedjQuery = this.UseEmbeddedjQuery;
		//    dialog.ID = "SpellCheckerDialog";
		//    dialog.VisualStyle = this.VisualStyle;
		//    dialog.DictionaryPath = this.DictionaryPath;
		//    //dialog.VisualStylePath = this.VisualStylePath;
		//    dialog.UseEmbeddedVisualStyles = this.UseEmbeddedVisualStyles;

		//    return dialog;
		//}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}

		/// <summary>
		/// Raises the PreRender event.
		/// </summary>
		/// <param name="e">An EventArgs object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);

			this.Page.ClientScript.GetCallbackEventReference(this, string.Empty, string.Empty, string.Empty);
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified HtmlTextWriterTag.
		/// </summary>
		/// <param name="writer">A HtmlTextWriter that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			this.EnsureChildControls();
			string cssClass = this.CssClass;

			if (this.IsDesignMode)
			{
				this.CssClass = string.Format("{0} {1}", this.DetermineCompoundCssClass(), cssClass).Trim();
			}
			else
			{
				this.CssClass = string.Format("{0} {1}", Utils.GetHiddenClass(), cssClass).Trim();
			}

			base.AddAttributesToRender(writer);

			this.CssClass = cssClass;
		}

    public override void Focus()
    {
      base.Focus();

      if (this.Page != null)
      {
        string script = "jQuery(document).ready(function(){jQuery(\"#" + this.ClientID + "\").data(\"wijmoC1editor\")._setFocus();});";

        ScriptManager sm = ScriptManager.GetCurrent(this.Page);
        if (sm != null)
        {
          ScriptManager.RegisterStartupScript(this, typeof(C1Editor), "AutoFocus", script, true);
        }
        else
        {
          this.Page.ClientScript.RegisterStartupScript(typeof(C1Editor), "AutoFocus", script, true);
        }
      }
    }

    #endregion end of ** override methods.
    #endregion end of ** methods.

    #region ** the ICallbackEventHandler interface implementations
    /// <summary>
    /// Returns the results of a callback event that targets a C1Editor.
    /// </summary>
    /// <returns>
    /// The result of the callback.
    /// </returns>
    string ICallbackEventHandler.GetCallbackResult()
		{
			if (string.IsNullOrEmpty(this._commandArgument))
			{
				return string.Empty;
			}

			if (this._commandArgument == CONST_CMD_TEMPLATELIST)
			{
				Hashtable tplList = DialogHelper.LoadTemplateList(this.Page, this.DialogUploadFolder);

				if (tplList.Count == 0)
				{
					return string.Empty;
				}

				//return JsonHelper.ObjectToString(tplList, this);
				return JsonHelper.WidgetToString(tplList, this);
			}
			
			if (this._commandArgument == CONST_CMD_IMAGELIST ||
				(this._commandArgument == CONST_CMD_DELETEIMAGE && _isDelImgSucc))
			{
				Hashtable imgList = DialogHelper.LoadImageList(this.Page, this.DialogUploadFolder, ValidImageTypes);

				if (imgList.Count == 0)
				{
					return string.Empty;
				}

				//return JsonHelper.ObjectToString(imgList, this);
				return JsonHelper.WidgetToString(imgList, this);
			}

			return string.Empty;
		}

		/// <summary>
		/// Processes a callback event that targets a C1Editor.
		/// </summary>
		/// <param name="eventArgument">
		/// A string that represents an event argument to pass to the event handler.
		/// </param>
		void ICallbackEventHandler.RaiseCallbackEvent(string eventArgument)
		{
			_isDelImgSucc = false;
			if (eventArgument.StartsWith(CONST_CMD_SAVETEXT))
			{
				this.Text = Page.Server.HtmlDecode(eventArgument.Substring(9, eventArgument.Length - 9));
				this.OnTextSaved(EventArgs.Empty);
			}
			else if (eventArgument.StartsWith(CONST_CMD_SAVETEMPLATE))
			{
				string arg = eventArgument.Substring(CONST_CMD_SAVETEMPLATE.Length, 
						eventArgument.Length - CONST_CMD_SAVETEMPLATE.Length);
				string[] items = arg.Split(new char[]{'|'});
				DialogHelper.SaveTemplate(items[0], items[1], items[2], this.Page, this.DialogUploadFolder);
			}
			else if (eventArgument.StartsWith(CONST_CMD_DELETETEMPLATE))
			{
				string arg = eventArgument.Substring(CONST_CMD_DELETETEMPLATE.Length,
						eventArgument.Length - CONST_CMD_DELETETEMPLATE.Length);
				DialogHelper.DeleteTemplate(arg, this.Page, this.DialogUploadFolder);
			}
			else if (eventArgument.StartsWith(CONST_CMD_DELETEIMAGE))
			{
				string arg = eventArgument.Substring(CONST_CMD_DELETEIMAGE.Length,
						eventArgument.Length - CONST_CMD_DELETEIMAGE.Length);
				_isDelImgSucc = DialogHelper.DeleteTemplate(arg, this.Page, this.DialogUploadFolder);
				this._commandArgument = CONST_CMD_DELETEIMAGE;
			}
			else
			{
				this._commandArgument = eventArgument;
			}
		}
		#endregion end of ** the ICallbackEventHandler interface implementations.

		#region ** the IPostBackDataHandler interface implementations
		/// <summary>
		/// Processes postback data for an ASP.NET server control.
		/// </summary>
		/// <param name="postDataKey">The key identifier for the control.</param>
		/// <param name="postCollection">The collection of all incoming name values.</param>
		/// <returns>true if the server control's state changes as a result of the postback; otherwise, false.</returns>
		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			string cmd = postCollection["__EVENTTARGET"];

			if (cmd.Equals(CONST_CMD_UPLOADIMAGE) || cmd.Equals(CONST_CMD_UPLOADLINK))
			{
				HttpPostedFile file = this.Page.Request.Files[CONST_CMD_FILEUPLOADNAME];

				if (file == null)
				{
					return false;
				}

				string[] validTypes = cmd.Equals(CONST_CMD_UPLOADIMAGE) ? ValidImageTypes : null;
				Hashtable retValues = DialogHelper.SaveFile(file, this.Page, this.DialogUploadFolder, validTypes);
				retValues.Add("cmd", cmd);
				//string response = JsonHelper.ObjectToString(retValues, this);
				string response = JsonHelper.WidgetToString(retValues, this);
				response = string.Format("window.parent.$('#{0}').{1}('uploadFileComplete', {2});", this.ClientID, this.WidgetName, response);

				this.Page.Response.Clear();
				this.Page.Response.Write("<script>");
				this.Page.Response.Write(response);
				this.Page.Response.Write("</script>");
				this.Page.Response.End();

				return false;
			}

			if (Page.IsCallback)
			{
				return false;
			}

			//
			bool isChanged = false;
			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);

			if (data != null)
			{
				string oldText = this.Text;
				this.RestoreStateFromJson(data);
				// When load the post back data, the ribbon buttons has been created. So here need to 
 				// recreate the buttons.
				if (this.CustomFontNames.Count > 0)
				{
					this.RibbonUI.CreateFontNameItems();
				}
				if (this.CustomFontSizes.Count > 0)
				{
					this.RibbonUI.CreateFontSizeItems();
				}

				this.Text = Page.Server.HtmlDecode(this.Text);

				if (this.Text != oldText) 
				{
					isChanged = true;
				}
			}

			return isChanged;
		}

		/// <summary>
		/// Notify the ASP.NET application that the state of the control has changed.
		/// </summary>
		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
			OnTextChanged(EventArgs.Empty);
		}
		#endregion end of ** the IPostBackDataHandler interface implementations.

		#region ** IC1Serializable interface implementations
		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1EditorSerializer sz = new C1EditorSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			C1EditorSerializer sz = new C1EditorSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1EditorSerializer sz = new C1EditorSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1EditorSerializer sz = new C1EditorSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}
		#endregion end ** IC1Serializable interface implementations
	}

	#region ** LocalizationOption class

	/// <summary>
	/// The localization of the editor string
	/// </summary>
	public class LocalizationOption : Settings, IJsonEmptiable
	{
		C1Editor _editor;

		/// <summary>
		/// Initializes a new instance of the <see cref="LocalizationOption"/> class.
		/// </summary>
		public LocalizationOption(C1Editor editor)
		{
			_editor = editor;
		}

		#region ** localization string properties
		/// <summary>
		/// HyperLinkDialogAddress string.
		/// </summary>
		[DefaultValue("Address :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogAddress
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogAddress",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.Address",
					"Address :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogAddress", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogAnchor string.
		/// </summary>
		[DefaultValue("anchor")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogAnchor
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogAnchor",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.Anchor",
					"anchor", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogAnchor", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogAnchor1 string.
		/// </summary>
		[DefaultValue("#anchor1")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogAnchor1
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogAnchor1",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.Anchor1",
					"#anchor1", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogAnchor1", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogAnchor2 string.
		/// </summary>
		[DefaultValue("#anchor2")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogAnchor2
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogAnchor2",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.Anchor2",
					"#anchor2", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogAnchor2", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogAnchor3 string.
		/// </summary>
		[DefaultValue("#anchor3")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogAnchor3
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogAnchor3",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.Anchor3",
					"#anchor3", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogAnchor3", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogAnchor4 string.
		/// </summary>
		[DefaultValue("#anchor4")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogAnchor4
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogAnchor4",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.Anchor4",
					"#anchor4", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogAnchor4", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogAnchor5 string.
		/// </summary>
		[DefaultValue("#anchor5")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogAnchor5
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogAnchor5",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.Anchor5",
					"#anchor5", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogAnchor5", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogAnchor6 string.
		/// </summary>
		[DefaultValue("#anchor6")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogAnchor6
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogAnchor6",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.Anchor6",
					"#anchor6", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogAnchor6", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogAnchor7 string.
		/// </summary>
		[DefaultValue("#anchor7")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogAnchor7
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogAnchor7",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.Anchor7",
					"#anchor7", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogAnchor7", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogCss string.
		/// </summary>
		[DefaultValue("Css :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogCss
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogCss",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.Css",
					"Css :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogCss", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogEmail string.
		/// </summary>
		[DefaultValue("email")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogEmail
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogEmail",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.Email",
					"email", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogEmail", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogLocalFile string.
		/// </summary>
		[DefaultValue("local file")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogLocalFile
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogLocalFile",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.LocalFile",
					"local file", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogLocalFile", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogSelectAnchor string.
		/// </summary>
		[DefaultValue("select anchor")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogSelectAnchor
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogSelectAnchor",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.SelectAnchor",
					"select anchor", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogSelectAnchor", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogTarget string.
		/// </summary>
		[DefaultValue("Target :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogTarget
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogTarget",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.Target",
					"Target :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogTarget", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogTextToDisplay string.
		/// </summary>
		[DefaultValue("Text to display :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogTextToDisplay
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogTextToDisplay",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.TextToDisplay",
					"Text to display :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogTextToDisplay", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogIconType string.
		/// </summary>
		[DefaultValue("Icon Type :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogIconType
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogIconType",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.IconType",
					"Icon Type :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogIconType", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogUpload string.
		/// </summary>
		[DefaultValue("Upload")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogUpload
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogUpload",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.Upload",
					"Upload", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogUpload", value);
			}
		}

		/// <summary>
		/// Delete Selected Image.
		/// </summary>
		[DefaultValue("Delete Selected")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ImageDialogDelImg
		{
			get
			{
				return GetPropertyValue<string>("ImageDialogDelImg",
					C1Localizer.GetString("C1Editor.ImageDialog.DelImg",
					"Delete Selected", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ImageDialogDelImg", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogUrl string.
		/// </summary>
		[DefaultValue("url")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogUrl
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogUrl",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.Url",
					"url", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogUrl", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogImage string.
		/// </summary>
		[DefaultValue("image")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogImage
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogImage",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.Image",
					"image", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogImage", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogInserthyperLink string.
		/// </summary>
		[DefaultValue("Insert hyperLink")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogInserthyperLink
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogInserthyperLink",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.InserthyperLink",
					"Insert hyperLink", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogInserthyperLink", value);
			}
		}

		/// <summary>
		/// HyperLinkDialogText string.
		/// </summary>
		[DefaultValue("text")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string HyperLinkDialogText
		{
			get
			{
				return GetPropertyValue<string>("HyperLinkDialogText",
					C1Localizer.GetString("C1Editor.HyperLinkDialog.Text",
					"text", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("HyperLinkDialogText", value);
			}
		}

		/// <summary>
		/// FindAndReplaceDialogFind string.
		/// </summary>
		[DefaultValue("Find:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string FindAndReplaceDialogFind
		{
			get
			{
				return GetPropertyValue<string>("FindAndReplaceDialogFind",
					C1Localizer.GetString("C1Editor.FindAndReplace.Find",
					"Find:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("FindAndReplaceDialogFind", value);
			}
		}

		/// <summary>
		/// FindAndReplaceDialogFindButton string.
		/// </summary>
		[DefaultValue("Find")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string FindAndReplaceDialogFindButton
		{
			get
			{
				return GetPropertyValue<string>("FindAndReplaceDialogFindButton",
					C1Localizer.GetString("C1Editor.FindAndReplace.FindButton",
					"Find", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("FindAndReplaceDialogFindButton", value);
			}
		}

		/// <summary>
		/// FindAndReplaceDialogReplace string.
		/// </summary>
		[DefaultValue("Replace:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string FindAndReplaceDialogReplace
		{
			get
			{
				return GetPropertyValue<string>("FindAndReplaceDialogReplace",
					C1Localizer.GetString("C1Editor.FindAndReplace.Replace",
					"Replace:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("FindAndReplaceDialogReplace", value);
			}
		}

		/// <summary>
		/// FindAndReplaceDialogReplaceButton string.
		/// </summary>
		[DefaultValue("Replace")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string FindAndReplaceDialogReplaceButton
		{
			get
			{
				return GetPropertyValue<string>("FindAndReplaceDialogReplaceButton",
					C1Localizer.GetString("C1Editor.FindAndReplace.ReplaceButton",
					"Replace", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("FindAndReplaceDialogReplaceButton", value);
			}
		}

		/// <summary>
		/// FindAndReplaceDialogTitle string.
		/// </summary>
		[DefaultValue("Find and replace")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string FindAndReplaceDialogTitle
		{
			get
			{
				return GetPropertyValue<string>("FindAndReplaceDialogTitle",
					C1Localizer.GetString("C1Editor.FindAndReplace.Title",
					"Find and replace", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("FindAndReplaceDialogTitle", value);
			}
		}

		/// <summary>
		/// FindAndReplaceFind string.
		/// </summary>
		[Obsolete("Use the FindAndReplaceDialogFind property instead.")]
		[DefaultValue("Find:")]
		[NotifyParentProperty(true)]
		public string FindAndReplaceFind
		{
			get
			{
				return this.FindAndReplaceDialogFind;
			}
			set
			{
				this.FindAndReplaceDialogFind = value;
			}
		}

		/// <summary>
		/// FindAndReplaceFindButton string.
		/// </summary>
		[Obsolete("Use the FindAndReplaceDialogFindButton property instead.")]
		[DefaultValue("Find")]
		[NotifyParentProperty(true)]
		public string FindAndReplaceFindButton
		{
			get
			{
				return this.FindAndReplaceDialogFindButton;
			}
			set
			{
				this.FindAndReplaceDialogFindButton = value;
			}
		}

		/// <summary>
		/// FindAndReplaceReplace string.
		/// </summary>
		[Obsolete("Use the FindAndReplaceDialogReplace property instead.")]
		[DefaultValue("Replace:")]
		[NotifyParentProperty(true)]
		public string FindAndReplaceReplace
		{
			get
			{
				return this.FindAndReplaceDialogReplace;
			}
			set
			{
				this.FindAndReplaceDialogReplace = value;
			}
		}


		/// <summary>
		/// FindAndReplaceReplaceButton string.
		/// </summary>
		[Obsolete("Use the FindAndReplaceDialogReplaceButton property instead.")]
		[DefaultValue("Replace")]
		[NotifyParentProperty(true)]
		public string FindAndReplaceReplaceButton
		{
			get
			{
				return  this.FindAndReplaceDialogReplaceButton;
			}
			set
			{
				this.FindAndReplaceDialogReplaceButton = value;
			}
		}

		/// <summary>
		/// FindAndReplaceTitle string.
		/// </summary>
		[Obsolete("Use the FindAndReplaceDialogTitle property instead.")]
		[DefaultValue("Find and replace")]
		[NotifyParentProperty(true)]
		public string FindAndReplaceTitle
		{
			get
			{
				return this.FindAndReplaceDialogTitle;
			}
			set
			{
				this.FindAndReplaceDialogTitle = value;
			}
		}

		/// <summary>
		/// CleanUpDialogReplaceSymbol string.
		/// </summary>
		[DefaultValue("Replace  &amp;amp;nbsp; symbol")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string CleanUpDialogReplaceSymbol
		{
			get
			{
				return GetPropertyValue<string>("CleanUpDialogReplaceSymbol",
					C1Localizer.GetString("C1Editor.CleanUpDialog.ReplaceSymbol",
					"Replace  &amp;amp;nbsp; symbol", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("CleanUpDialogReplaceSymbol", value);
			}
		}

		/// <summary>
		/// CleanUpDialogStripClass string.
		/// </summary>
		[DefaultValue("Strip CLASS tag")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string CleanUpDialogStripClass
		{
			get
			{
				return GetPropertyValue<string>("CleanUpDialogStripClass",
					C1Localizer.GetString("C1Editor.CleanUpDialog.StripClass",
					"Strip CLASS tag", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("CleanUpDialogStripClass", value);
			}
		}

		/// <summary>
		/// CleanUpDialogStripSpanTag string.
		/// </summary>
		[DefaultValue("Strip SPAN tag")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string CleanUpDialogStripSpanTag
		{
			get
			{
				return GetPropertyValue<string>("CleanUpDialogStripSpanTag",
					C1Localizer.GetString("C1Editor.CleanUpDialog.StripSpanTag",
					"Strip SPAN tag", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("CleanUpDialogStripSpanTag", value);
			}
		}

		/// <summary>
		/// CleanUpDialogStripStyle string.
		/// </summary>
		[DefaultValue("Strip STYLE attribute")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string CleanUpDialogStripStyle
		{
			get
			{
				return GetPropertyValue<string>("CleanUpDialogStripStyle",
					C1Localizer.GetString("C1Editor.CleanUpDialog.StripStyle",
					"Strip STYLE attribute", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("CleanUpDialogStripStyle", value);
			}
		}

		/// <summary>
		/// CleanUpDialogTransformParagraphToDIV string.
		/// </summary>
		[DefaultValue("Transform Paragraph to DIV")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string CleanUpDialogTransformParagraphToDIV
		{
			get
			{
				return GetPropertyValue<string>("CleanUpDialogTransformParagraphToDIV",
					C1Localizer.GetString("C1Editor.CleanUpDialog.TransformParagraphToDIV",
					"Transform Paragraph to DIV", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("CleanUpDialogTransformParagraphToDIV", value);
			}
		}

		/// <summary>
		/// CleanUpDialogTitle string.
		/// </summary>
		[DefaultValue("Clean up source HTML document")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string CleanUpDialogTitle
		{
			get
			{
				return GetPropertyValue<string>("CleanUpDialogTitle",
					C1Localizer.GetString("C1Editor.CleanUpDialog.Title",
					"Clean up source HTML document", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("CleanUpDialogTitle", value);
			}
		}

		/// <summary>
		/// CleanUpDialogDocumentSource string.
		/// </summary>
		[DefaultValue("Document source :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string CleanUpDialogDocumentSource
		{
			get
			{
				return GetPropertyValue<string>("CleanUpDialogDocumentSource",
					C1Localizer.GetString("C1Editor.CleanUpDialog.DocumentSource",
					"Document source :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("CleanUpDialogDocumentSource", value);
			}
		}

		/// <summary>
		/// SpecialCharacterDialogDiacritics string.
		/// </summary>
		[DefaultValue("Diacritics")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string SpecialCharacterDialogDiacritics
		{
			get
			{
				return GetPropertyValue<string>("SpecialCharacterDialogDiacritics",
					C1Localizer.GetString("C1Editor.SpecialCharacterDialog.Diacritics",
					"Diacritics", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("SpecialCharacterDialogDiacritics", value);
			}
		}

		/// <summary>
		/// SpecialCharacterDialogTitle string.
		/// </summary>
		[DefaultValue("Insert special character")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string SpecialCharacterDialogTitle
		{
			get
			{
				return GetPropertyValue<string>("SpecialCharacterDialogTitle",
					C1Localizer.GetString("C1Editor.SpecialCharacterDialog.Title",
					"Insert special character", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("SpecialCharacterDialogTitle", value);
			}
		}

		/// <summary>
		/// SpecialCharacterDialogPunctuation string.
		/// </summary>
		[DefaultValue("Punctuation")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string SpecialCharacterDialogPunctuation
		{
			get
			{
				return GetPropertyValue<string>("SpecialCharacterDialogPunctuation",
					C1Localizer.GetString("C1Editor.SpecialCharacterDialog.Punctuation",
					"Punctuation", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("SpecialCharacterDialogPunctuation", value);
			}
		}

		/// <summary>
		/// SpecialCharacterDialogSymbols string.
		/// </summary>
		[DefaultValue("Symbols")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string SpecialCharacterDialogSymbols
		{
			get
			{
				return GetPropertyValue<string>("SpecialCharacterDialogSymbols",
					C1Localizer.GetString("C1Editor.SpecialCharacterDialog.Symbols",
					"Symbols", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("SpecialCharacterDialogSymbols", value);
			}
		}

		/// <summary>
		/// TemplateDialogApply string.
		/// </summary>
		[DefaultValue("Apply")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TemplateDialogApply
		{
			get
			{
				return GetPropertyValue<string>("TemplateDialogApply",
					C1Localizer.GetString("C1Editor.TemplateDialog.Apply",
					"Apply", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TemplateDialogApply", value);
			}
		}

		/// <summary>
		/// TemplateDialogApplyTemplate string.
		/// </summary>
		[DefaultValue("Apply Template")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TemplateDialogApplyTemplate
		{
			get
			{
				return GetPropertyValue<string>("TemplateDialogApplyTemplate",
					C1Localizer.GetString("C1Editor.TemplateDialog.ApplyTemplate",
					"Apply Template", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TemplateDialogApplyTemplate", value);
			}
		}

		/// <summary>
		/// TemplateDialogDeleteSelected string.
		/// </summary>
		[DefaultValue("Delete selected")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TemplateDialogDeleteSelected
		{
			get
			{
				return GetPropertyValue<string>("TemplateDialogDeleteSelected",
					C1Localizer.GetString("C1Editor.TemplateDialog.DeleteSelected",
					"Delete selected", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TemplateDialogDeleteSelected", value);
			}
		}

		/// <summary>
		/// TemplateDialogSaveCurrentPage string.
		/// </summary>
		[DefaultValue("Save current page as template")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TemplateDialogSaveCurrentPage
		{
			get
			{
				return GetPropertyValue<string>("TemplateDialogSaveCurrentPage",
					C1Localizer.GetString("C1Editor.TemplateDialog.SaveCurrentPage",
					"Save current page as template", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TemplateDialogDeleteSelected", value);
			}
		}

		/// <summary>
		/// TemplateDialogDescription string.
		/// </summary>
		[DefaultValue("Description :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TemplateDialogDescription
		{
			get
			{
				return GetPropertyValue<string>("TemplateDialogDescription",
					C1Localizer.GetString("C1Editor.TemplateDialog.Description",
					"Description :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TemplateDialogDescription", value);
			}
		}

		/// <summary>
		/// TemplateDialogLastModifyTime string.
		/// </summary>
		[DefaultValue("Last modify time :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TemplateDialogLastModifyTime
		{
			get
			{
				return GetPropertyValue<string>("TemplateDialogLastModifyTime",
					C1Localizer.GetString("C1Editor.TemplateDialog.LastModifyTime",
					"Last modify time :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TemplateDialogLastModifyTime", value);
			}
		}

		/// <summary>
		/// TemplateDialogName string.
		/// </summary>
		[DefaultValue("Name :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TemplateDialogName
		{
			get
			{
				return GetPropertyValue<string>("TemplateDialogName",
					C1Localizer.GetString("C1Editor.TemplateDialog.Name",
					"Name :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TemplateDialogName", value);
			}
		}

		/// <summary>
		/// TemplateDialogSelectTemplate string.
		/// </summary>
		[DefaultValue("Select Template:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TemplateDialogSelectTemplate
		{
			get
			{
				return GetPropertyValue<string>("TemplateDialogSelectTemplate",
					C1Localizer.GetString("C1Editor.TemplateDialog.SelectTemplate",
					"SSelect Template:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TemplateDialogSelectTemplate", value);
			}
		}

		/// <summary>
		/// TemplateDialogSize string.
		/// </summary>
		[DefaultValue("Size :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TemplateDialogSize
		{
			get
			{
				return GetPropertyValue<string>("TemplateDialogSize",
					C1Localizer.GetString("C1Editor.TemplateDialog.Size",
					"Size :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TemplateDialogSize", value);
			}
		}

		/// <summary>
		/// TemplateDialogTemplatePreview string.
		/// </summary>
		[DefaultValue("Template Preview:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TemplateDialogTemplatePreview
		{
			get
			{
				return GetPropertyValue<string>("TemplateDialogTemplatePreview",
					C1Localizer.GetString("C1Editor.TemplateDialog.TemplatePreview",
					"Template Preview:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TemplateDialogTemplatePreview", value);
			}
		}

		/// <summary>
		/// ImageEditorDialogCssText string.
		/// </summary>
		[DefaultValue("Css text:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ImageEditorDialogCssText
		{
			get
			{
				return GetPropertyValue<string>("ImageEditorDialogCssText",
					C1Localizer.GetString("C1Editor.ImageEditorDialog.CssText",
					"Css text:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ImageEditorDialogCssText", value);
			}
		}

		/// <summary>
		/// ImageEditorDialogHorizontalResolution string.
		/// </summary>
		[DefaultValue("Horizontal resolution:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ImageEditorDialogHorizontalResolution
		{
			get
			{
				return GetPropertyValue<string>("ImageEditorDialogHorizontalResolution",
					C1Localizer.GetString("C1Editor.ImageEditorDialog.HorizontalResolution",
					"Horizontal resolution:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ImageEditorDialogHorizontalResolution", value);
			}
		}

		/// <summary>
		/// ImageEditorDialogImageAltText string.
		/// </summary>
		[DefaultValue("Image alt text:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ImageEditorDialogImageAltText
		{
			get
			{
				return GetPropertyValue<string>("ImageEditorDialogImageAltText",
					C1Localizer.GetString("C1Editor.ImageEditorDialog.ImageAltText",
					"Image alt text:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ImageEditorDialogImageAltText", value);
			}
		}

		/// <summary>
		/// ImageEditorDialogImageHeight string.
		/// </summary>
		[DefaultValue("Image height:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ImageEditorDialogImageHeight
		{
			get
			{
				return GetPropertyValue<string>("ImageEditorDialogImageHeight",
					C1Localizer.GetString("C1Editor.ImageEditorDialog.ImageHeight",
					"Image height:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ImageEditorDialogImageHeight", value);
			}
		}

		/// <summary>
		/// ImageEditorDialogImageBrowser string.
		/// </summary>
		[DefaultValue("Image Browser")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ImageEditorDialogImageBrowser
		{
			get
			{
				return GetPropertyValue<string>("ImageEditorDialogImageBrowser",
					C1Localizer.GetString("C1Editor.ImageEditorDialog.ImageBrowser",
					"Image Browser", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ImageEditorDialogImageBrowser", value);
			}
		}

		/// <summary>
		/// ImageEditorDialogImageSize string.
		/// </summary>
		[DefaultValue("Image size:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ImageEditorDialogImageSize
		{
			get
			{
				return GetPropertyValue<string>("ImageEditorDialogImageSize",
					C1Localizer.GetString("C1Editor.ImageEditorDialog.ImageSize",
					"Image size:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ImageEditorDialogImageSize", value);
			}
		}

		/// <summary>
		/// ImageEditorDialogImageSrc string.
		/// </summary>
		[DefaultValue("Image Src:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ImageEditorDialogImageSrc
		{
			get
			{
				return GetPropertyValue<string>("ImageEditorDialogImageSrc",
					C1Localizer.GetString("C1Editor.ImageEditorDialog.ImageSrc",
					"Image Src:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ImageEditorDialogImageSrc", value);
			}
		}

		/// <summary>
		/// ImageEditorDialogImageWidth string.
		/// </summary>
		[DefaultValue("Image width:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ImageEditorDialogImageWidth
		{
			get
			{
				return GetPropertyValue<string>("ImageEditorDialogImageWidth",
					C1Localizer.GetString("C1Editor.ImageEditorDialog.ImageWidth",
					"Image width:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ImageEditorDialogImageWidth", value);
			}
		}

		/// <summary>
		/// ImageEditorDialogLastModifyTime string.
		/// </summary>
		[DefaultValue("Last modify time:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ImageEditorDialogLastModifyTime
		{
			get
			{
				return GetPropertyValue<string>("ImageEditorDialogLastModifyTime",
					C1Localizer.GetString("C1Editor.ImageEditorDialog.LastModifyTime",
					"Last modify time:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ImageEditorDialogLastModifyTime", value);
			}
		}

		/// <summary>
		/// ImageEditorDialogPixelFormat string.
		/// </summary>
		[DefaultValue("Pixel format:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ImageEditorDialogPixelFormat
		{
			get
			{
				return GetPropertyValue<string>("ImageEditorDialogPixelFormat",
					C1Localizer.GetString("C1Editor.ImageEditorDialog.PixelFormat",
					"Pixel format:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ImageEditorDialogPixelFormat", value);
			}
		}

		/// <summary>
		/// ImageEditorDialogSize string.
		/// </summary>
		[DefaultValue("Size:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ImageEditorDialogSize
		{
			get
			{
				return GetPropertyValue<string>("ImageEditorDialogSize",
					C1Localizer.GetString("C1Editor.ImageEditorDialog.Size",
					"Size:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ImageEditorDialogSize", value);
			}
		}

		/// <summary>
		/// ImageEditorDialogUpload string.
		/// </summary>
		[DefaultValue("Upload")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ImageEditorDialogUpload
		{
			get
			{
				return GetPropertyValue<string>("ImageEditorDialogUpload",
					C1Localizer.GetString("C1Editor.ImageEditorDialog.Upload",
					"Upload", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ImageEditorDialogUpload", value);
			}
		}

		/// <summary>
		/// ImageEditorDialogUrl string.
		/// </summary>
		[DefaultValue("Url:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ImageEditorDialogUrl
		{
			get
			{
				return GetPropertyValue<string>("ImageEditorDialogUrl",
					C1Localizer.GetString("C1Editor.ImageEditorDialog.Url",
					"Url:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ImageEditorDialogUrl", value);
			}
		}

		/// <summary>
		/// MediaDialogTitle string.
		/// </summary>
		[DefaultValue("Insert media")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string MediaDialogTitle
		{
			get
			{
				return GetPropertyValue<string>("MediaDialogTitle",
					C1Localizer.GetString("C1Editor.MediaDialog.Title",
					"Insert media", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("MediaDialogTitle", value);
			}
		}

		/// <summary>
		/// MediaDialogHeight string.
		/// </summary>
		[DefaultValue("Height :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string MediaDialogHeight
		{
			get
			{
				return GetPropertyValue<string>("MediaDialogHeight",
					C1Localizer.GetString("C1Editor.MediaDialog.Height",
					"Height :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("MediaDialogHeight", value);
			}
		}

		/// <summary>
		/// MediaDialogMediaType string.
		/// </summary>
		[DefaultValue("Media Type :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string MediaDialogMediaType
		{
			get
			{
				return GetPropertyValue<string>("MediaDialogMediaType",
					C1Localizer.GetString("C1Editor.MediaDialog.MediaType",
					"Media Type :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("MediaDialogMediaType", value);
			}
		}

		/// <summary>
		/// MediaDialogMediaUrl string.
		/// </summary>
		[DefaultValue("Media Url :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string MediaDialogMediaUrl
		{
			get
			{
				return GetPropertyValue<string>("MediaDialogMediaUrl",
					C1Localizer.GetString("C1Editor.MediaDialog.MediaUrl",
					"Media Url :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("MediaDialogMediaUrl", value);
			}
		}

		/// <summary>
		/// ContextMenuCut string.
		/// </summary>
		[DefaultValue("Cut")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ContextMenuCut
		{
			get
			{
				return GetPropertyValue<string>("ContextMenuCut",
					C1Localizer.GetString("C1Editor.ContextMenu.Cut",
					"Cut", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ContextMenuCut", value);
			}
		}

		/// <summary>
		/// ContextMenuCopy string.
		/// </summary>
		[DefaultValue("Copy")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ContextMenuCopy
		{
			get
			{
				return GetPropertyValue<string>("ContextMenuCopy",
					C1Localizer.GetString("C1Editor.ContextMenu.Copy",
					"Copy", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ContextMenuCopy", value);
			}
		}

		/// <summary>
		/// ContextMenuPaste string.
		/// </summary>
		[DefaultValue("Paste")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ContextMenuPaste
		{
			get
			{
				return GetPropertyValue<string>("ContextMenuPaste",
					C1Localizer.GetString("C1Editor.ContextMenu.Paste",
					"Paste", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ContextMenuPaste", value);
			}
		}
        
		/// <summary>
		/// DialogPixel string.
		/// </summary>
		[DefaultValue("px")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string DialogPixel
		{
			get
			{
				return GetPropertyValue<string>("DialogPixel",
					C1Localizer.GetString("C1Editor.Dialog.Pixel",
					"px", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("DialogPixel", value);
			}
		}

		/// <summary>
		/// MediaDialogWidth string.
		/// </summary>
		[DefaultValue("Width :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string MediaDialogWidth
		{
			get
			{
				return GetPropertyValue<string>("MediaDialogWidth",
					C1Localizer.GetString("C1Editor.MediaDialog.Width",
					"Width :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("MediaDialogWidth", value);
			}
		}

		/// <summary>
		/// PreviewDialogDocument string.
		/// </summary>
		[DefaultValue("Document")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string PreviewDialogDocument
		{
			get
			{
				return GetPropertyValue<string>("PreviewDialogDocument",
					C1Localizer.GetString("C1Editor.PreviewDialog.Document",
					"Document", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("PreviewDialogDocument", value);
			}
		}

		/// <summary>
		/// PreviewDialogPreview string.
		/// </summary>
		[DefaultValue("Preview")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string PreviewDialogPreview
		{
			get
			{
				return GetPropertyValue<string>("PreviewDialogPreview",
					C1Localizer.GetString("C1Editor.Preview",
					"Preview", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("PreviewDialogPreview", value);
			}
		}

		/// <summary>
		/// PreviewDialogNextPage string.
		/// </summary>
		[DefaultValue("Next page")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string PreviewDialogNextPage
		{
			get
			{
				return GetPropertyValue<string>("PreviewDialogNextPage",
					C1Localizer.GetString("C1Editor.PreviewDialog.NextPage",
					"Next page", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("PreviewDialogNextPage", value);
			}
		}
        
		/// <summary>
		/// PreviewDialogPreviewSize string.
		/// </summary>
		[DefaultValue("Preview Size:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string PreviewDialogPreviewSize
		{
			get
			{
				return GetPropertyValue<string>("PreviewDialogPreviewSize",
					C1Localizer.GetString("C1Editor.PreviewDialog.PreviewSize",
					"Preview Size:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("PreviewDialogPreviewSize", value);
			}
		}

		/// <summary>
		/// PreviewDialogPrevPage string.
		/// </summary>
		[DefaultValue("Prev page")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string PreviewDialogPrevPage
		{
			get
			{
				return GetPropertyValue<string>("PreviewDialogPrevPage",
					C1Localizer.GetString("C1Editor.PreviewDialog.PrevPage",
					"Prev page", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("PreviewDialogPrevPage", value);
			}
		}

		/// <summary>
		/// PreviewDialogPrintAll string.
		/// </summary>
		[DefaultValue("Print All")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string PreviewDialogPrintAll
		{
			get
			{
				return GetPropertyValue<string>("PreviewDialogPrintAll",
					C1Localizer.GetString("C1Editor.PreviewDialog.PrintAll",
					"Print All", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("PreviewDialogPrintAll", value);
			}
		}

		/// <summary>
		/// PreviewDialogPrintPage string.
		/// </summary>
		[DefaultValue("Print Page")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string PreviewDialogPrintPage
		{
			get
			{
				return GetPropertyValue<string>("PreviewDialogPrintPage",
					C1Localizer.GetString("C1Editor.PreviewDialog.PrintPage",
					"Print Page", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("PreviewDialogPrintPage", value);
			}
		}

		/// <summary>
		/// PreviewDialogSplit string.
		/// </summary>
		[DefaultValue("Split pages")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string PreviewDialogSplit
		{
			get
			{
				return GetPropertyValue<string>("PreviewDialogSplit",
					C1Localizer.GetString("C1Editor.PreviewDialog.Split",
					"Split pages", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("PreviewDialogSplit", value);
			}
		}

		/// <summary>
		/// InsertTableDialogTitle string.
		/// </summary>
		[DefaultValue("Insert Table")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string InsertTableDialogTitle
		{
			get
			{
				return GetPropertyValue<string>("InsertTableDialogTitle",
					C1Localizer.GetString("C1Editor.InsertTableDialog.Title",
					"Insert Table", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("InsertTableDialogTitle", value);
			}
		}

		/// <summary>
		/// EditTableDialogTitle string.
		/// </summary>
		[DefaultValue("Edit Table")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string EditTableDialogTitle
		{
			get
			{
				return GetPropertyValue<string>("EditTableDialogTitle",
					C1Localizer.GetString("C1Editor.EditTableDialog.Title",
					"Edit Table", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("EditTableDialogTitle", value);
			}
		}

		/// <summary>
		/// TableDialogBackgroundColor string.
		/// </summary>
		[DefaultValue("Background Color:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TableDialogBackgroundColor
		{
			get
			{
				return GetPropertyValue<string>("TableDialogBackgroundColor",
					C1Localizer.GetString("C1Editor.TableDialog.BackgroundColor",
					"Background Color:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TableDialogBackgroundColor", value);
			}
		}

		/// <summary>
		/// TableDialogBorder string.
		/// </summary>
		[DefaultValue("Border thickness:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TableDialogBorder
		{
			get
			{
				return GetPropertyValue<string>("TableDialogBorder",
					C1Localizer.GetString("C1Editor.TableDialog.Border",
					"Border thickness:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TableDialogBorder", value);
			}
		}

		/// <summary>
		/// TableDialogCellPadding string.
		/// </summary>
		[DefaultValue("Cell Padding :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TableDialogCellPadding
		{
			get
			{
				return GetPropertyValue<string>("TableDialogCellPadding",
					C1Localizer.GetString("C1Editor.TableDialog.CellPadding",
					"Cell Padding :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TableDialogCellPadding", value);
			}
		}

		/// <summary>
		/// TableDialogCellSpacing string.
		/// </summary>
		[DefaultValue("Cell Spacing :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TableDialogCellSpacing
		{
			get
			{
				return GetPropertyValue<string>("TableDialogCellSpacing",
					C1Localizer.GetString("C1Editor.TableDialog.CellSpacing",
					"Cell Spacing :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TableDialogCellSpacing", value);
			}
		}

		/// <summary>
		/// TableDialogColumns string.
		/// </summary>
		[DefaultValue("Columns :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TableDialogColumns
		{
			get
			{
				return GetPropertyValue<string>("TableDialogColumns",
					C1Localizer.GetString("C1Editor.TableDialog.Columns",
					"Columns :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TableDialogColumns", value);
			}
		}

		/// <summary>
		/// TableDialogCssText string.
		/// </summary>
		[DefaultValue("Css Text :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TableDialogCssText
		{
			get
			{
				return GetPropertyValue<string>("TableDialogCssText",
					C1Localizer.GetString("C1Editor.TableDialog.CssText",
					"Css Text :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TableDialogCssText", value);
			}
		}

		/// <summary>
		/// TableDialogPixels string.
		/// </summary>
		[DefaultValue("pixels")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TableDialogPixels
		{
			get
			{
				return GetPropertyValue<string>("TableDialogPixels",
					C1Localizer.GetString("C1Editor.TableDialog.Pixels",
					"pixels", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TableDialogPixels", value);
			}
		}

		/// <summary>
		/// TableDialogRows string.
		/// </summary>
		[DefaultValue("Rows :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TableDialogRows
		{
			get
			{
				return GetPropertyValue<string>("TableDialogRows",
					C1Localizer.GetString("C1Editor.TableDialog.Rows",
					"Rows :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TableDialogRows", value);
			}
		}

		/// <summary>
		/// TableDialogTableHeight string.
		/// </summary>
		[DefaultValue("Table Height :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TableDialogTableHeight
		{
			get
			{
				return GetPropertyValue<string>("TableDialogTableHeight",
					C1Localizer.GetString("C1Editor.TableDialog.TableHeight",
					"Table Height :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TableDialogTableHeight", value);
			}
		}

		/// <summary>
		/// TableDialogTableWidth string.
		/// </summary>
		[DefaultValue("Table Width :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TableDialogTableWidth
		{
			get
			{
				return GetPropertyValue<string>("TableDialogTableWidth",
					C1Localizer.GetString("C1Editor.TableDialog.TableWidth",
					"Table Width :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TableDialogTableWidth", value);
			}
		}

		/// <summary>
		/// TableDialogCellSpacingError string.
		/// </summary>
		[DefaultValue("Please input a number for 'Cell Spacing' textbox.")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TableDialogCellSpacingError
		{
			get
			{
				return GetPropertyValue<string>("TableDialogCellSpacingError",
					C1Localizer.GetString("C1Editor.TableDialog.CellSpacingError",
					"Please input a number for 'Cell Spacing' textbox.", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TableDialogCellSpacingError", value);
			}
		}

		/// <summary>
		/// TableDialogCellPaddingError string.
		/// </summary>
		[DefaultValue("Please input a number for 'Cell Padding' textbox.")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TableDialogCellPaddingError
		{
			get
			{
				return GetPropertyValue<string>("TableDialogCellPaddingError",
					C1Localizer.GetString("C1Editor.TableDialog.CellPaddingError",
					"Please input a number for 'Cell Padding' textbox.", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TableDialogCellPaddingError", value);
			}
		}

		/// <summary>
		/// TableDialogBorderError string.
		/// </summary>
		[DefaultValue("Please input a number for 'Border thickness' textbox.")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TableDialogBorderError
		{
			get
			{
				return GetPropertyValue<string>("TableDialogBorderError",
					C1Localizer.GetString("C1Editor.TableDialog.BorderError",
					"Please input a number for 'Border thickness' textbox.", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TableDialogBorderError", value);
			}
		}

		/// <summary>
		/// TagInspectorDialogAttributes string.
		/// </summary>
		[DefaultValue("Attributes:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TagInspectorDialogAttributes
		{
			get
			{
				return GetPropertyValue<string>("TagInspectorDialogAttributes",
					C1Localizer.GetString("C1Editor.TagInspectorDialog.Attributes",
					"Attributes:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TagInspectorDialogAttributes", value);
			}
		}

		/// <summary>
		/// DialogCancel string.
		/// </summary>
		[DefaultValue("Cancel")]
		[NotifyParentProperty(true)]
		//[Layout(LayoutType.Appearance)]
		[WidgetOption()]
		public string DialogCancel
		{
			get
			{
				return GetPropertyValue<string>("DialogCancel",
					C1Localizer.GetString("C1Editor.Dialog.Cancel",
					"Cancel", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("DialogCancel", value);
			}
		}

		/// <summary>
		/// TagInspectorDialogCSSText string.
		/// </summary>
		[DefaultValue("Css Text:")]
		[NotifyParentProperty(true)]
		//[Layout(LayoutType.Appearance)]
		[WidgetOption()]
		public string TagInspectorDialogCSSText
		{
			get
			{
				return GetPropertyValue<string>("TagInspectorDialogCSSText",
					C1Localizer.GetString("C1Editor.TagInspectorDialog.CSSText",
					"Css Text:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TagInspectorDialogCSSText", value);
			}
		}

		/// <summary>
		/// TagInspectorDialogDisplayNotEmptyAttributes string.
		/// </summary>
		[DefaultValue("Display not empty attributes only")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TagInspectorDialogDisplayNotEmptyAttributes
		{
			get
			{
				return GetPropertyValue<string>("TagInspectorDialogDisplayNotEmptyAttributes",
					C1Localizer.GetString("C1Editor.TagInspectorDialog.DisplayNotEmptyAttributes",
					"Display not empty attributes only", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TagInspectorDialogDisplayNotEmptyAttributes", value);
			}
		}

		/// <summary>
		/// TagInspectorDialogInnerHTML string.
		/// </summary>
		[DefaultValue("Inner HTML:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TagInspectorDialogInnerHTML
		{
			get
			{
				return GetPropertyValue<string>("TagInspectorDialogInnerHTML",
					C1Localizer.GetString("C1Editor.TagInspectorDialog.InnerHTML",
					"Inner HTML:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TagInspectorDialogInnerHTML", value);
			}
		}

		/// <summary>
		/// TagInspectorDialogSave string.
		/// </summary>
		[DefaultValue("Save")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TagInspectorDialogSave
		{
			get
			{
				return GetPropertyValue<string>("TagInspectorDialogSave",
					C1Localizer.GetString("C1Editor.TagInspectorDialog.Save",
					"Save", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TagInspectorDialogSave", value);
			}
		}

		
		/// <summary>
		/// DialogOK string.
		/// </summary>
		[DefaultValue("OK")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string DialogOK
		{
			get
			{
				return GetPropertyValue<string>("DialogOK",
					C1Localizer.GetString("C1Editor.Dialog.OK",
					"OK", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("DialogOK", value);
			}
		}

		/// <summary>
		/// TagInspectorDialogSelectedTag string.
		/// </summary>
		[DefaultValue("Selected tag :")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TagInspectorDialogSelectedTag
		{
			get
			{
				return GetPropertyValue<string>("TagInspectorDialogSelectedTag",
					C1Localizer.GetString("C1Editor.TagInspectorDialog.SelectedTag",
					"Selected tag :", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TagInspectorDialogSelectedTag", value);
			}
		}

		/// <summary>
		/// TagInspectorDialogTitle string.
		/// </summary>
		[DefaultValue("Tag Inspector")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string TagInspectorDialogTitle
		{
			get
			{
				return GetPropertyValue<string>("TagInspectorDialogTitle",
					C1Localizer.GetString("C1Editor.TagInspectorDialog.Title",
					"Tag Inspector", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("TagInspectorDialogTitle", value);
			}
		}

		/// <summary>
		/// BackColorDialogSelectedColor string.
		/// </summary>
		[DefaultValue("Selected Color:")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string BackColorDialogSelectedColor
		{
			get
			{
				return GetPropertyValue<string>("BackColorDialogSelectedColor",
					C1Localizer.GetString("C1Editor.BackColorDialog.SelectedColor",
					"Selected Color:", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("BackColorDialogSelectedColor", value);
			}
		}

		/// <summary>
		/// BackColorDialogTitle string.
		/// </summary>
		[DefaultValue("Set BackColor")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string BackColorDialogTitle
		{
			get
			{
				return GetPropertyValue<string>("BackColorDialogTitle",
					C1Localizer.GetString("C1Editor.BackColorDialog.Title",
					"Set BackColor", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("BackColorDialogTitle", value);
			}
		}

		/// <summary>
		/// ForeColorDialogTitle string.
		/// </summary>
		[DefaultValue("Set ForeColor")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ForeColorDialogTitle
		{
			get
			{
				return GetPropertyValue<string>("ForeColorDialogTitle",
					C1Localizer.GetString("C1Editor.ForeColorDialog.Title",
					"Set ForeColor", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ForeColorDialogTitle", value);
			}
		}

		/// <summary>
		/// ErrorMessageCutError string.
		/// </summary>
		[DefaultValue("This function is not supported in current browser.  Please use (Ctrl + X).")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageCutError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageCutError",
					C1Localizer.GetString("C1Editor.ErrorMessage.CutError",
					"This function is not supported in current browser.  Please use (Ctrl + X).", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageCutError", value);
			}
		}

		/// <summary>
		/// ErrorMessageCopyError string.
		/// </summary>
		[DefaultValue("This function is not supported in current browser. Please use (Ctrl + C).")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageCopyError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageCopyError",
					C1Localizer.GetString("C1Editor.ErrorMessage.CopyError",
					"This function is not supported in current browser. Please use (Ctrl + C).", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageCopyError", value);
			}
		}

		/// <summary>
		/// ErrorMessagePasteError string.
		/// </summary>
		[DefaultValue("This function is not supported in current browser. Please use (Ctrl + V).")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessagePasteError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessagePasteError",
					C1Localizer.GetString("C1Editor.ErrorMessage.PasteError",
					"This function is not supported in current browser. Please use (Ctrl + V).", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessagePasteError", value);
			}
		}

		/// <summary>
		/// ErrorMessageTemplateNameError string.
		/// </summary>
		[DefaultValue("Please input a template name!")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageTemplateNameError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageTemplateNameError",
					C1Localizer.GetString("C1Editor.ErrorMessage.TemplateNameError",
					"Please input a template name!", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageTemplateNameError", value);
			}
		}

		/// <summary>
		/// ErrorMessageTemplateFileError string.
		/// </summary>
		[DefaultValue("Please select a template file.")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageTemplateFileError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageTemplateFileError",
					C1Localizer.GetString("C1Editor.ErrorMessage.TemplateFileError",
					"Please input a template name!", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageTemplateFileError", value);
			}
		}

		/// <summary>
		/// ErrorMessageValidTemplateFileError string.
		/// </summary>
		[DefaultValue("Please input a template name!")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageValidTemplateFileError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageValidTemplateFileError",
					C1Localizer.GetString("C1Editor.ErrorMessage.ValidTemplateFileError",
					"Please select a valid template file.", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageValidTemplateFileError", value);
			}
		}

		/// <summary>
		/// ErrorMessageSelectImageError string.
		/// </summary>
		[DefaultValue("Please select a image.")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageSelectImageError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageSelectImageError",
					C1Localizer.GetString("C1Editor.ErrorMessage.SelectImageError",
					"Please select a image.", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageSelectImageError", value);
			}
		}

		/// <summary>
		/// ErrorMessageImageWidthError string.
		/// </summary>
		[DefaultValue("Please input a number for 'Image width' textbox.")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageImageWidthError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageImageWidthError",
					C1Localizer.GetString("C1Editor.ErrorMessage.ImageWidthError",
					"Please input a number for 'Image width' textbox.", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageImageWidthError", value);
			}
		}

		/// <summary>
		/// ErrorMessageImageHeightError string.
		/// </summary>
		[DefaultValue("Please input a number for 'Image height' textbox.")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageImageHeightError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageImageHeightError",
					C1Localizer.GetString("C1Editor.ErrorMessage.ImageHeightError",
					"Please input a number for 'Image height' textbox.", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageImageHeightError", value);
			}
		}

		/// <summary>
		/// ErrorMessageTemplateNameError string.
		/// </summary>
		[DefaultValue("Please input address!")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageAddressError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageAddressError",
					C1Localizer.GetString("C1Editor.ErrorMessage.AddressError",
					"Please input address!", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageAddressError", value);
			}
		}

		/// <summary>
		/// ErrorMessageDisplayTextError string.
		/// </summary>
		[DefaultValue("Please input display text!")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageDisplayTextError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageDisplayTextError",
					C1Localizer.GetString("C1Editor.ErrorMessage.DisplayTextError",
					"Please input display text!", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageDisplayTextError", value);
			}
		}

		/// <summary>
		/// ErrorMessageHyperLinkImageUrlError string.
		/// </summary>
		[DefaultValue("Please input image url!")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageHyperLinkImageUrlError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageHyperLinkImageUrlError",
					C1Localizer.GetString("C1Editor.ErrorMessage.HyperLinkImageUrlError",
					"Please input image url!", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageHyperLinkImageUrlError", value);
			}
		}

		/// <summary>
		/// ErrorMessageHyperLinkImageWidthError string.
		/// </summary>
		[DefaultValue("Please input correct image width!")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageHyperLinkImageWidthError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageHyperLinkImageWidthError",
					C1Localizer.GetString("C1Editor.ErrorMessage.HyperLinkImageWidthError",
					"Please input correct image width!", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageHyperLinkImageWidthError", value);
			}
		}

		/// <summary>
		/// HyperLinkImageHeightError string.
		/// </summary>
		[DefaultValue("Please input correct image height!")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageHyperLinkImageHeightError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageHyperLinkImageHeightError",
					C1Localizer.GetString("C1Editor.ErrorMessage.HyperLinkImageHeightError",
					"Please input correct image height!", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageHyperLinkImageHeightError", value);
			}
		}

		/// <summary>
		/// ErrorMessageHyperLinkEmail string.
		/// </summary>
		[DefaultValue("Please input correct email!")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageHyperLinkEmail
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageHyperLinkEmail",
					C1Localizer.GetString("C1Editor.ErrorMessage.HyperLinkEmail",
					"Please input correct email!", _editor.Culture));
			}
			set
			{

				SetPropertyValue<string>("ErrorMessageHyperLinkEmail", value);
			}
		}

		/// <summary>
		/// ErrorMessageHyperLinkUrl string.
		/// </summary>
		[DefaultValue("Please input correct url!")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageHyperLinkUrl
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageHyperLinkUrl",
					C1Localizer.GetString("C1Editor.ErrorMessage.HyperLinkUrl",
					"Please input correct url!", _editor.Culture));
			}
			set
			{

				SetPropertyValue<string>("ErrorMessageHyperLinkUrl", value);
			}
		}

		/// <summary>
		/// ErrorMessageHyperLinkUrl string.
		/// </summary>
		[DefaultValue("Please input correct anchor!")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageHyperLinkAnchor
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageHyperLinkAnchor",
					C1Localizer.GetString("C1Editor.ErrorMessage.HyperLinkAnchor",
					"Please input correct anchor!", _editor.Culture));
			}
			set
			{

				SetPropertyValue<string>("ErrorMessageHyperLinkAnchor", value);
			}
		}

		/// <summary>
		/// HyperLinkImageHeightError string.
		/// </summary>
		[DefaultValue("Please input a number for 'Rows' textbox.")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageRowsError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageRowsError",
					C1Localizer.GetString("C1Editor.ErrorMessage.RowsError",
					"Please input a number for 'Rows' textbox.", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageRowsError", value);
			}
		}

		/// <summary>
		/// ErrorMessageColumnsError string.
		/// </summary>
		[DefaultValue("Please input a number for 'Columns' textbox.")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageColumnsError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageColumnsError",
					C1Localizer.GetString("C1Editor.ErrorMessage.ColumnsError",
					"Please input a number for 'Columns' textbox.", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageColumnsError", value);
			}
		}

		/// <summary>
		/// ErrorMessageTableWidthError string.
		/// </summary>
		[DefaultValue("Please input a number for 'Table Width ' textbox.")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageTableWidthError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageTableWidthError",
					C1Localizer.GetString("C1Editor.ErrorMessage.TableWidthError",
					"Please input a number for 'Table Width ' textbox.", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageTableWidthError", value);
			}
		}

		/// <summary>
		/// ErrorMessageTableHeightError string.
		/// </summary>
		[DefaultValue("Please input a number for 'Table Height' textbox.")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageTableHeightError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageTableHeightError",
					C1Localizer.GetString("C1Editor.ErrorMessage.TableHeightError",
					"Please input a number for 'Table Height' textbox.", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageTableHeightError", value);
			}
		}

		/// <summary>
		/// ErrorMessageSelectTableError string.
		/// </summary>
		[DefaultValue("Please select a table!")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageSelectTableError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageSelectTableError",
					C1Localizer.GetString("C1Editor.ErrorMessage.SelectTableError",
					"Please select a table!", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageSelectTableError", value);
			}
		}

		/// <summary>
		/// ErrorMessageReplaceStringError string.
		/// </summary>
		[DefaultValue("please input replace string!")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageReplaceStringError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageReplaceStringError",
					C1Localizer.GetString("C1Editor.ErrorMessage.ReplaceStringError",
					"please input replace string!", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageReplaceStringError", value);
			}
		}

		/// <summary>
		/// ErrorMessageUrlEmptyError string.
		/// </summary>
		[DefaultValue("URL is empty!")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageUrlEmptyError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageUrlEmptyError",
					C1Localizer.GetString("C1Editor.ErrorMessage.UrlEmptyError",
					"URL is empty!", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageUrlEmptyError", value);
			}
		}

		/// <summary>
		/// ErrorMessageUrlCorrectError string.
		/// </summary>
		[DefaultValue("please input correct URL!")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageUrlCorrectError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageUrlCorrectError",
					C1Localizer.GetString("C1Editor.ErrorMessage.UrlCorrectError",
					"please input correct URL!", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageUrlCorrectError", value);
			}
		}

		/// <summary>
		/// ErrorMessageMediaWidthError string.
		/// </summary>
		[DefaultValue("please input width of the media!")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageMediaWidthError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageMediaWidthError",
					C1Localizer.GetString("C1Editor.ErrorMessage.MediaWidthError",
					"please input width of the media!", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageMediaWidthError", value);
			}
		}

		/// <summary>
		/// ErrorMessageMediaHeightError string.
		/// </summary>
		[DefaultValue("please input height of the media!")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageMediaHeightError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageMediaHeightError",
					C1Localizer.GetString("C1Editor.ErrorMessage.MediaHeightError",
					"please input height of the media!", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageMediaHeightError", value);
			}
		}

		/// <summary>
		/// ErrorMessageMediaHeightError string.
		/// </summary>
		[DefaultValue("Copy text to Clipboard \nIf you are using firefox please do the following :\n1. " + 
			"Write in your url box : 'about:config'\n2. Change signed.applets.codebase_principal_support = true\n")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageCopyToClipboardError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageCopyToClipboardError",
					C1Localizer.GetString("C1Editor.ErrorMessage.CopyToClipboardError",
					"Copy text to Clipboard \nIf you are using firefox please do the following :\n1."+
					" Write in your url box : 'about:config'\n2. Change signed.applets.codebase_principal_support = true\n", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageCopyToClipboardError", value);
			}
		}

		/// <summary>
		/// ErrorMessageFindTextError string.
		/// </summary>
		[DefaultValue(" String Not Found!")]
		[NotifyParentProperty(true)]
		[WidgetOption()]
		public string ErrorMessageFindTextError
		{
			get
			{
				return GetPropertyValue<string>("ErrorMessageFindTextError",
					C1Localizer.GetString("C1Editor.ErrorMessage.ErrorMessageFindTextError",
					" String Not Found!", _editor.Culture));
			}
			set
			{
				SetPropertyValue<string>("ErrorMessageFindTextError", value);
			}
		}
		#endregion

		#region ** implement ICustomOptionType interface

		/// <summary>
		/// Returns a <see cref="System.String"/> that represents this instance.
		/// </summary>
		/// <returns>
		/// A <see cref="System.String"/> that represents this instance.
		/// </returns>
		public override string ToString()
		{
			return base.ToString();
		}

		internal bool ShouldSerialize()
		{
			return true;
		}

		#endregion end of ** IJsonEmptiable interface implementation


		bool IJsonEmptiable.IsEmpty
		{
			get { return !this.ShouldSerialize(); }
		}
	}

	#endregion end of ** LocalizationOption class
}
