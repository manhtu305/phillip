﻿using System;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.C1FileExplorer
{
    /// <summary>
    /// Operations in FileExplorer.
    /// </summary>
    public enum FileCommand
    {
        /// <summary>
		/// Open a folder
        /// </summary>
		[Obsolete("Please use GetItems instead.")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		Open = 2,

        /// <summary>
		/// Paste a file or folder to a location
        /// </summary>
		Paste = 1,

        /// <summary>
		/// Get sub folders and files contained in
        /// </summary>
		GetItems = 2,

        /// <summary>
		/// Create a new directory
        /// </summary>
		CreateDirectory = 3,

        /// <summary>
		/// Delete files or folders
        /// </summary>
		Delete = 4,

        /// <summary>
		/// Rename a file or directory
        /// </summary>
		Rename = 5,

        /// <summary>
        /// Move files or folders to a new folder
        /// </summary>
		Move = 6
    }
}
