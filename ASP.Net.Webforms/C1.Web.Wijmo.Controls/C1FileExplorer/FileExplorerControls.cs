﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1FileExplorer
{

    /// <summary>
    /// Control components in FileExplorer.
    /// </summary>
    [Flags]
    public enum FileExplorerControls
    {
        /// <summary>
        /// A toolbar, which provides shortcuts for the file explorer commands (delete, new folder, back, forward, etc.)
        /// </summary>
        Toolbar = 1,
        /// <summary>
        /// A textbox, which shows the current selected path in the file explorer
        /// </summary>
        AddressBox = 2,
        /// <summary>
        /// A textbox, which shows the current selected path in the file explorer
        /// </summary>
        FilterTextBox = 4,
        /// <summary>
        /// A treeview, which shows the folders in the file explorer.
        /// </summary>
        TreeView = 8,
        /// <summary>
        ///  A grid, which shows the files/folders in the current file explorer folder
        /// </summary>
        Grid = 16,
        /// <summary>
        /// The C1ListView control to hold the thumbnails view
        /// </summary>
        ListView = 32,
        /// <summary>
        /// The context menu which is shown when the user right clicks inside the controls.
        /// </summary>
        ContextMenu = 64,
        /// <summary>
        /// The default value for the C1FileExplorer control - all controls are shown
        /// </summary>
        All = Toolbar | AddressBox | FilterTextBox | TreeView | Grid | ListView | ContextMenu
    }
}
