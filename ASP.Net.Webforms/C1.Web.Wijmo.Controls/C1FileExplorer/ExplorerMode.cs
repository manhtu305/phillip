﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1FileExplorer
{
    /// <summary>
    /// File explorer mode.
    /// </summary>
    public enum ExplorerMode
    {
        
        /// <summary>
        /// Only display folder in the treeview, and display file and folders in the right panel 
        /// according to the current folder
        /// </summary>
        Default,
        /// <summary>
        /// Display all the folders and files in the treeview control
        /// </summary>
        FileTree
    }
}
