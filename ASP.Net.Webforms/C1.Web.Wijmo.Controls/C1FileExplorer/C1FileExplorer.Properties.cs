﻿using System.Collections;
using System.Collections.Generic;
using C1.Web.Wijmo.Controls.C1FileExplorer.Actions;
using C1.Web.Wijmo.Controls.Localization;
using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web;
using System.Globalization;

namespace C1.Web.Wijmo.Controls.C1FileExplorer
{
	public partial class C1FileExplorer: ICultureControl
	{

		#region fields

		private Hashtable _innerStates;
		private FileExplorerAction _action;
		private readonly Shortcuts _shortcuts = new Shortcuts();
		private LocalizationOption _localizationOption;

		#endregion

		#region properties

		// Currently, this property are not supported.
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override object DataSource
		{
			get { return base.DataSource; }
			set { base.DataSource = value; }
		}

		// Currently, this property are not supported.
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override string DataSourceID
		{
			get { return base.DataSourceID; }
			set { base.DataSourceID = value; }
		}


		/// <summary>
		/// A value specifies the current opened folder url
		/// </summary>
		[Obsolete("Please use CurrentFolder instead.")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string Address
		{
			get { return CurrentFolder; }
			set { CurrentFolder = value; }
		}

		/// <summary>
		/// Get or set the initial path to load data into TreeView.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1FileExplorer.InitPath")]
		[DefaultValue("")]
		[WidgetOption]
		public string InitPath
		{
			get { return GetPropertyValue("InitPath", ""); }
			set
			{
				if (InitPath == value)
				{
					return;
				}

				SetPropertyValue("InitPath", value);
				ResetCurrentFolder();
				ResetDeletePaths();
				ResetData();
			}
		}

		/// <summary>
		/// Get or set a value indicating whether to allow creating new folders.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1FileExplorer.EnableCreateNewFolder")]
		[WidgetOption]
		[DefaultValue(true)]
		public bool EnableCreateNewFolder
		{
			get { return GetPropertyValue("EnableCreateNewFolder", true); }
			set { SetPropertyValue("EnableCreateNewFolder", value); }
		}

		/// <summary>
		/// Get or set a value indicating whether to allow opening a new window with the file.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1FileExplorer.EnableOpenFile")]
		[WidgetOption]
		[DefaultValue(true)]
		public bool EnableOpenFile
		{
			get { return GetPropertyValue("EnableOpenFile", true); }
			set { SetPropertyValue("EnableOpenFile", value); }
		}

		/// <summary>
		/// Get or set whether to allow multiple items selection.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1FileExplorer.AllowMultipleSelection")]
		[WidgetOption]
		[DefaultValue(false)]
		public bool AllowMultipleSelection
		{
			get { return GetPropertyValue("AllowMultipleSelection", false); }
			set
			{
				SetPropertyValue("AllowMultipleSelection", value);
			}
		}

		/// <summary>
		/// Gets or sets culture ID.
		/// </summary>
		[C1Description("C1Calendar.CultureInfo")]
		[C1Category("Category.Behavior")]
		[DefaultValue(typeof(CultureInfo), "en-US")]
		[TypeConverter(typeof(CultureInfoConverter))]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public CultureInfo Culture 
		{
			get 
			{
				return GetCulture(GetPropertyValue<CultureInfo>("Culture", null));
			}
			set
			{
				SetPropertyValue<CultureInfo>("Culture", value);
			}
		}

		/// <summary>
		/// Get or set a value indicating whether to allow copying of files/folders.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1FileExplorer.EnableCopy")]
		[WidgetOption]
		[DefaultValue(true)]
		public bool EnableCopy
		{
			get { return GetPropertyValue("EnableCopy", true); }
			set { SetPropertyValue("EnableCopy", value); }
		}

		/// <summary>
		/// A bool value specifies whether to perform the filtering after the 'Enter' key is pressed.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1FileExplorer.EnableFilteringOnEnterPressed")]
		[WidgetOption]
		[DefaultValue(false)]
		public bool EnableFilteringOnEnterPressed
		{
			get { return GetPropertyValue("EnableFilteringOnEnterPressed", false); }
			set { SetPropertyValue("EnableFilteringOnEnterPressed", value); }
		}

		/// <summary>
		/// Get or set whether to allow changing the extension of the file while renaming
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1FileExplorer.AllowFileExtensionRename")]
		[WidgetOption]
		[DefaultValue(false)]
		public bool AllowFileExtensionRename
		{
			get { return GetPropertyValue("AllowFileExtensionRename", false); }
			set { SetPropertyValue("AllowFileExtensionRename", value); }
		}

		/// <summary>
		/// Get or set the components to show in FileExplorer.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1FileExplorer.VisibleControls")]
		[WidgetOption]
		[DefaultValue(FileExplorerControls.All)]
		public FileExplorerControls VisibleControls
		{
			get { return GetPropertyValue("VisibleControls", FileExplorerControls.All); }
			set
			{
				SetPropertyValue("VisibleControls", value);
			}
		}

		/// <summary>
		/// Get or set the url of current selected node in the tree.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1FileExplorer.CurrentFolder")]
		[DefaultValue("")]
		[WidgetOption]
		public string CurrentFolder
		{
			get { return GetPropertyValue("CurrentFolder", ""); }
			set
			{
				if (value == CurrentFolder)
				{
					return;
				}

				SetPropertyValue("CurrentFolder", value);
				ResetData();
			}
		}

		/// <summary>
		/// Get or set the width of the TreeView. 
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1FileExplorer.TreePanelWidth")]
		[WidgetOption]
		[DefaultValue(200)]
		public int TreePanelWidth
		{
			get { return GetPropertyValue("TreePanelWidth", 200); }
			set { SetPropertyValue("TreePanelWidth", value); }
		}

		/// <summary>
		/// Get or set the ViewMode of the detail.
		/// The value will be GridView or ThumbnailsView.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1FileExplorer.ViewMode")]
		[WidgetOption]
		[DefaultValue(ViewMode.Detail)]
		public ViewMode ViewMode
		{
			get { return GetPropertyValue("ViewMode", ViewMode.Detail); }
			set { SetPropertyValue("ViewMode", value); }
		}

		/// <summary>
		/// When using paging, get or set the number of items loaded per page.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1FileExplorer.PageSize")]
		[WidgetOption]
		[DefaultValue(10)]
		public int PageSize
		{
			get { return GetPropertyValue("PageSize", 10); }
			set
			{
				if (value == PageSize)
				{
					return;
				}

				SetPropertyValue("PageSize", value);
				ResetData();
			}
		}

		/// <summary>
		/// Get or set whether to use paging.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1FileExplorer.AllowPaging")]
		[WidgetOption]
		[DefaultValue(false)]
		public bool AllowPaging
		{
			get { return GetPropertyValue("AllowPaging", false); }
			set
			{
				if (value == AllowPaging)
				{
					return;
				}

				SetPropertyValue("AllowPaging", value);
				ResetData();
			}
		}

		/// <summary>
		/// Get or set the folder paths to show.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1FileExplorer.ViewPaths")]
		[WidgetOption]
		[DefaultValue(null)]
		[ArrayItemType(typeof (string))]
		[TypeConverter(typeof (StringArrayConverter))]
		public string[] ViewPaths
		{
			get { return GetPropertyValue<string[]>("ViewPaths", null); }
			set
			{
				if (Extensions.StringArrayEquals(ViewPaths, value))
				{
					return;
				}

				SetPropertyValue("ViewPaths", value);
				ResetCurrentFolder();
				ResetDeletePaths();
				ResetData();
			}
		}

		/// <summary>
		/// Gets or sets which paths in which files or subdirectories can be deleted.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1FileExplorer.DeletePaths")]
		[NotifyParentProperty(true)]
		[TypeConverter(typeof (StringArrayConverter))]
		[DefaultValue(null)]
		public string[] DeletePaths
		{
			get { return GetPropertyValue<string[]>("DeletePaths", null); }
			set
			{
				SetPropertyValue("DeletePaths", value);
			}
		}

		//Added by RyanWu.
		/// <summary>
		/// Get or set the folder paths to upload.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1FileExplorer.UploadPaths")]
		[WidgetOption]
		[ArrayItemType(typeof (string))]
		[NotifyParentProperty(true)]
		[TypeConverter(typeof (StringArrayConverter))]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DefaultValue(null)]
		public string[] UploadPaths
		{
			get { return GetPropertyValue<string[]>("UploadPaths", null); }
			set { SetPropertyValue("UploadPaths", value); }
		}

		/// <summary>
		/// Get or set the patterns of files that are shown, usually the file extensions.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1FileExplorer.SearchPatterns")]
		[WidgetOption]
		[ArrayItemType(typeof (string))]
		[NotifyParentProperty(true)]
		[TypeConverter(typeof (StringArrayConverter))]
		[DefaultValue("*.jpg,*.jpeg,*.gif,*.png")]
		public string[] SearchPatterns
		{
			get { return GetPropertyValue("SearchPatterns", new [] {"*.jpg", "*.jpeg", "*.gif", "*.png"}); }
			set
			{
				if (Extensions.StringArrayEquals(SearchPatterns, value))
				{
					return;
				}

				SetPropertyValue("SearchPatterns", value);
				ResetData();
			}
		}

		/// <summary>
		/// Get or set whether to allow async file upload.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1FileExplorer.EnableAsyncUpload")]
		[WidgetOption]
		[DefaultValue(true)]
		[NotifyParentProperty(true)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool EnableAsyncUpload
		{
			get { return GetPropertyValue("EnableAsyncUpload", true); }
			set { SetPropertyValue("EnableAsyncUpload", value); }
		}

		/// <summary>
		/// Get or set the ExplorerMode.
		/// </summary>
		[C1Category("Category.Appearance")]
		[C1Description("C1FileExplorer.Mode")]
		[WidgetOption]
		[DefaultValue(ExplorerMode.Default)]
		public ExplorerMode Mode
		{
			get { return GetPropertyValue("Mode", ExplorerMode.Default); }
			set
			{
				if (Mode != value)
				{
					SetPropertyValue("Mode", value);
					ChildControlsCreated = false;
				}
			}
		}

		/// <summary>
		/// Get or set the supported keyboard shortcuts.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1FileExplorer.Shortcuts")]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[NotifyParentProperty(true)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[MergableProperty(false)]
		[WidgetOption]
		[RefreshProperties(RefreshProperties.All)]
		public Shortcuts Shortcuts
		{
			get { return GetPropertyValue("Shortcuts", _shortcuts); }
		}

		/// <summary>
		/// Gets the current app host uri.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[WidgetOption]
		public string HostUri
		{
			get
			{
				string appVirtualPath;
				try
				{
					appVirtualPath = HttpRuntime.AppDomainAppVirtualPath ?? string.Empty;
				}
				catch (Exception)
				{
					appVirtualPath = string.Empty;
				}
				if (!appVirtualPath.EndsWith("/")) appVirtualPath += "/";
				return appVirtualPath;
			}
		}

		/// <summary>
		/// Use the Localization property in order to customize localization strings.
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[WidgetOption]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public LocalizationOption Localization
		{
			get { return _localizationOption ?? (_localizationOption = new LocalizationOption()); }
		}

		/// <summary>
		/// Time columns will be shown if the value set is true
		/// </summary>
		[C1Category("Category.Appearance")]
        [C1Description("C1FileExplorer.EnableTimeView")]
        [WidgetOption]
        [DefaultValue(false)]
		public bool EnableTimeView
		{
			get { return GetPropertyValue("EnableTimeView", false); }
			set { SetPropertyValue("EnableTimeView", value); }
		}

        /// <summary>
        /// The datetime format will be applied if this value is set.
        /// </summary>
		[C1Category("Category.Appearance")]
        [C1Description("C1FileExplorer.TimeFormat")]
        [WidgetOption]
        [DefaultValue(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string TimeFormat
		{
			get { return GetPropertyValue("TimeFormat", ""); }
			set { SetPropertyValue("TimeFormat", value); }
		}

		/// <summary>
		/// Gets the <see cref="T:System.Web.UI.HtmlTextWriterTag"/> value that corresponds to this Web server control. This property is used primarily by control developers.
		/// </summary>
		/// <value></value>
		/// <returns>
		/// One of the <see cref="T:System.Web.UI.HtmlTextWriterTag"/> enumeration values.
		/// </returns>
		protected override HtmlTextWriterTag TagKey
		{
			get { return HtmlTextWriterTag.Div; }
		}

		/// <summary>
		/// A hashtable value contains all information which need transfer from server to client.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Json(true)]
		public virtual Hashtable InnerStates
		{
			get { return _innerStates ?? (_innerStates = new Hashtable()); }
		}

		private int PageIndex
		{
			get { return GetInnerState<int>("pageIndex"); }
			set { SetInnerState("pageIndex", value); }
		}

		private int PageCount
		{
			get { return GetInnerState("pageCount", 1); }
			set { SetInnerState("pageCount", value); }
		}

		private IList<FileExplorerItem> TreeInitData
		{
			get { return GetInnerState<IList<FileExplorerItem>>("treeInitData"); }
			set { SetInnerState("treeInitData", value); }
		}

		private IList<FileExplorerItem> CurrentFolderData
		{
			get { return GetInnerState<IList<FileExplorerItem>>("currentFolderData"); }
			set { SetInnerState("currentFolderData", value); }
		}

		private SortDirection SortDirection
		{
			get 
			{ 
				return GetInnerState("sortDirection", SortDirection.Ascending, s => s.SafeParseEnum<SortDirection>());
			}
			set { SetInnerState("sortDirection", value); }
		}

		private SortField SortExpression
		{
			get
			{
				return GetInnerState("sortExpression", SortField.Name, s => s.SafeParseEnum<SortField>());
			}
			set { SetInnerState("sortExpression", value); }
		}

		private string FilterExpression
		{
			get { return GetInnerState<string>("filterExpression"); }
			set { SetInnerState("filterExpression", value); }
		}

		private FileExplorerAction Action
		{
			get
			{
				return _action ?? (_action = new FileExplorerAction());
			}
		}

		#endregion

		#region client side events

		/// <summary>
		/// The event fired after an item is renamed.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientItemRenamed")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientItemRenamed
		{
			get { return GetPropertyValue("OnClientItemRenamed", ""); }
			set { SetPropertyValue("OnClientItemRenamed", value); }
		}

		/// <summary>
		/// The event fired before an item is renamed.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientItemRenaming")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientItemRenaming
		{
			get { return GetPropertyValue("OnClientItemRenaming", ""); }
			set { SetPropertyValue("OnClientItemRenaming", value); }
		}

		/// <summary>
		/// The event fired when an error occurs.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientErrorOccurred")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientErrorOccurred
		{
			get { return GetPropertyValue("OnClientErrorOccurred", ""); }
			set { SetPropertyValue("OnClientErrorOccurred", value); }
		}

		/// <summary>
		/// The event fired after the item is selected.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientItemSelected")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientItemSelected
		{
			get { return GetPropertyValue("OnClientItemSelected", ""); }
			set { SetPropertyValue("OnClientItemSelected", value); }
		}

		/// <summary>
		/// The event fired after a folder content is loaded.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientFolderLoaded")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientFolderLoaded
		{
			get { return GetPropertyValue("OnClientFolderLoaded", ""); }
			set { SetPropertyValue("OnClientFolderLoaded", value); }
		}

		/// <summary>
		/// The event fired after a file is opened.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientFileOpened")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientFileOpened
		{
			get { return GetPropertyValue("OnClientFileOpened", ""); }
			set { SetPropertyValue("OnClientFileOpened", value); }
		}

		/// <summary>
		/// The event fired before a file is opened.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientFileOpening")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientFileOpening
		{
			get { return GetPropertyValue("OnClientFileOpening", ""); }
			set { SetPropertyValue("OnClientFileOpening", value); }
		}

		/// <summary>
		/// The event fired after current active folder is changed.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientFolderChanged")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientFolderChanged
		{
			get { return GetPropertyValue("OnClientFolderChanged", ""); }
			set { SetPropertyValue("OnClientFolderChanged", value); }
		}

		/// <summary>
		/// The event fired after a new folder is created.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientNewFolderCreated")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientNewFolderCreated
		{
			get { return GetPropertyValue("OnClientNewFolderCreated", ""); }
			set { SetPropertyValue("OnClientNewFolderCreated", value); }
		}

		/// <summary>
		/// The event fired before a new folder is created.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientNewFolderCreating")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientNewFolderCreating
		{
			get { return GetPropertyValue("OnClientNewFolderCreating", ""); }
			set { SetPropertyValue("OnClientNewFolderCreating", value); }
		}

		/// <summary>
		/// The event fired after an item is deleted.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientItemDeleted")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientItemDeleted
		{
			get { return GetPropertyValue("OnClientItemDeleted", ""); }
			set { SetPropertyValue("OnClientItemDeleted", value); }
		}

		/// <summary>
		/// The event fired before an item is deleted.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientItemDeleting")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientItemDeleting
		{
			get { return GetPropertyValue("OnClientItemDeleting", ""); }
			set { SetPropertyValue("OnClientItemDeleting", value); }
		}

		/// <summary>
		/// The event fired before deleting a folder.
		/// </summary>
		[Obsolete("Please use OnClientItemDeleting instead.")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string OnClientDelete
		{
			get { return OnClientItemDeleting; }
			set { OnClientItemDeleting = value; }
		}

		/// <summary>
		/// The event fired after an item is moved.
		/// </summary>
		[Obsolete("Please use OnClientItemMoved instead.")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string OnClientMove
		{
			get { return OnClientItemMoved; }
			set { OnClientItemMoved = value; }
		}

		/// <summary>
		/// The event fired before an item is moved.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientItemMoving")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientItemMoving
		{
			get { return GetPropertyValue("OnClientItemMoving", ""); }
			set { SetPropertyValue("OnClientItemMoving", value); }
		}

		/// <summary>
		/// The event fired after an item is moved.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientItemMoved")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientItemMoved
		{
			get { return GetPropertyValue("OnClientItemMoved", ""); }
			set { SetPropertyValue("OnClientItemMoved", value); }
		}

		/// <summary>
		/// The event fired after an item is pasted.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientItemPasted")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientItemPasted
		{
			get { return GetPropertyValue("OnClientItemPasted", ""); }
			set { SetPropertyValue("OnClientItemPasted", value); }
		}

		/// <summary>
		/// The event fired before an item is pasted.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientItemPasting")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientItemPasting
		{
			get { return GetPropertyValue("OnClientItemPasting", ""); }
			set { SetPropertyValue("OnClientItemPasting", value); }
		}

		/// <summary>
		/// The event fired before an item is copied to the new destination directory.
		/// </summary>
		[Obsolete("Please use OnClientItemPasting instead.")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string OnClientCopy
		{
			get { return OnClientItemPasting; }
			set { OnClientItemPasting = value; }
		}

		/// <summary>
		/// The event fired after an item is copied.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientItemCopied")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientItemCopied
		{
			get { return GetPropertyValue("OnClientItemCopied", ""); }
			set { SetPropertyValue("OnClientItemCopied", value); }
		}

		/// <summary>
		/// The event fired before an item is copied.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientItemCopying")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientItemCopying
		{
			get { return GetPropertyValue("OnClientItemCopying", ""); }
			set { SetPropertyValue("OnClientItemCopying", value); }
		}

		/// <summary>
		/// The event fired after doing filter.
		/// </summary>
		[Obsolete("Please use OnClientFiltered instead.")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string OnClientFilter
		{
			get { return OnClientFiltered; }
			set { OnClientFiltered = value; }
		}

		/// <summary>
		/// The event fired after filtering.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientFiltered")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientFiltered
		{
			get { return GetPropertyValue("OnClientFiltered", ""); }
			set { SetPropertyValue("OnClientFiltered", value); }
		}

		/// <summary>
		/// The event fired before filtering.
		/// </summary>
		[C1Description("C1FileExplorer.OnClientFiltering")]
		[C1Category("Category.ClientSideEvents")]
		[WidgetEvent("event, data")]
		[DefaultValue("")]
		[Layout(LayoutType.Behavior)]
		public string OnClientFiltering
		{
			get { return GetPropertyValue("OnClientFiltering", ""); }
			set { SetPropertyValue("OnClientFiltering", value); }
		}

		#endregion

		#region server side events

		/// <summary>
		/// Event fired when any file or folder operation is made at the server side.
		/// If you want to cancel the operation, juse set C1FileExplorerEventArgs.Cancel = true. 
		/// </summary>
		[C1Category("Category.Events")]
		[C1Description("C1FileExplorer.ItemCommand")]
		public event C1FileExplorerEventHandler ItemCommand;

		#endregion

		#region ** LocalizationOption class

		/// <summary>
		/// The localization of the fileExplorer string
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public class LocalizationOption : Settings, IJsonEmptiable
		{
			#region ** localization string properties

			/// <summary>
			/// EmptyFolder string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string EmptyFolder
			{
				get
				{
					return GetPropertyValue("EmptyFolder",
						C1Localizer.GetString("C1FileExplorer.EmptyFolder",
							"This folder is empty."));
				}
				set { SetPropertyValue("EmptyFolder", value); }
			}

			/// <summary>
			/// OpenDialogTitlePrefix string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string OpenDialogTitlePrefix
			{
				get
				{
					return GetPropertyValue("OpenDialogTitlePrefix",
						C1Localizer.GetString("C1FileExplorer.OpenDialog.TitlePrefix",
							"View File:"));
				}
				set { SetPropertyValue("OpenDialogTitlePrefix", value); }
			}


			/// <summary>
			/// NewFolderDialogTitle string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string NewFolderDialogTitle
			{
				get
				{
					return GetPropertyValue("NewFolderDialogTitle",
						C1Localizer.GetString("C1FileExplorer.NewFolderDialog.Title",
							"Enter new folder name"));
				}
				set { SetPropertyValue("NewFolderDialogTitle", value); }
			}

			/// <summary>
			/// DefaultNewFolderName string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string DefaultNewFolderName
			{
				get
				{
					return GetPropertyValue("DefaultNewFolderName",
						C1Localizer.GetString("C1FileExplorer.NewFolderDialog.DefaultFolderName",
							"New Folder"));
				}
				set { SetPropertyValue("DefaultNewFolderName", value); }
			}


			/// <summary>
			/// DialogOK string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string DialogOK
			{
				get
				{
					return GetPropertyValue("DialogOK",
						C1Localizer.GetString("C1FileExplorer.Dialog.OK",
							"OK"));
				}
				set { SetPropertyValue("DialogOK", value); }
			}

			/// <summary>
			/// DialogCancel string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string DialogCancel
			{
				get
				{
					return GetPropertyValue("DialogCancel",
						C1Localizer.GetString("C1FileExplorer.Dialog.Cancel",
							"Cancel"));
				}
				set { SetPropertyValue("DialogCancel", value); }
			}

			/// <summary>
			/// DeleteDialogTitle string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string DeleteDialogTitle
			{
				get
				{
					return GetPropertyValue("DeleteDialogTitle",
						C1Localizer.GetString("C1FileExplorer.DeleteDialog.Title",
							"Warning"));
				}
				set { SetPropertyValue("DeleteDialogTitle", value); }
			}


			/// <summary>
			/// DeleteDialogAlertingText string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string DeleteDialogAlertingText
			{
				get
				{
					return GetPropertyValue("DeleteDialogAlertingText",
						C1Localizer.GetString("C1FileExplorer.DeleteDialog.AlertingText",
							"Are you sure to delete the selected item? This operation cannot be undone."));
				}
				set { SetPropertyValue("DeleteDialogAlertingText", value); }
			}


			/// <summary>
			/// CommandOpen string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string CommandOpen
			{
				get
				{
					return GetPropertyValue("CommandOpen", C1Localizer.GetString("C1FileExplorer.CommandOpen", "Open"));
				}
				set { SetPropertyValue("CommandOpen", value); }
			}


			/// <summary>
			/// CommandNewFolder string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string CommandNewFolder
			{
				get
				{
					return GetPropertyValue("CommandNewFolder", C1Localizer.GetString("C1FileExplorer.CommandNewFolder", "New Folder"));
				}
				set { SetPropertyValue("CommandNewFolder", value); }
			}


			/// <summary>
			/// CommandCopy string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string CommandCopy
			{
				get
				{
					return GetPropertyValue("CommandCopy", C1Localizer.GetString("C1FileExplorer.CommandCopy", "Copy"));
				}
				set { SetPropertyValue("CommandCopy", value); }
			}

			/// <summary>
			/// CommandSetThumbnailViewMode string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string CommandSetThumbnailViewMode
			{
				get
				{
					return GetPropertyValue("CommandSetThumbnailViewMode", 
						C1Localizer.GetString("C1FileExplorer.CommandSetThumbnailViewMode", "Thumbnail View Mode"));
				}
				set { SetPropertyValue("CommandSetThumbnailViewMode", value); }
			}

			/// <summary>
			/// CommandSetDetailViewMode string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string CommandSetDetailViewMode
			{
				get
				{
					return GetPropertyValue("CommandSetDetailViewMode", 
						C1Localizer.GetString("C1FileExplorer.CommandSetDetailViewMode", "Detail View Mode"));
				}
				set { SetPropertyValue("CommandSetDetailViewMode", value); }
			}

			/// <summary>
			/// CommandForward string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string CommandForward
			{
				get
				{
					return GetPropertyValue("CommandForward", C1Localizer.GetString("C1FileExplorer.CommandForward", "Forward"));
				}
				set { SetPropertyValue("CommandForward", value); }
			}

			/// <summary>
			/// CommandBack string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string CommandBack
			{
				get
				{
					return GetPropertyValue("CommandBack", C1Localizer.GetString("C1FileExplorer.CommandBack", "Back"));
				}
				set { SetPropertyValue("CommandBack", value); }
			}

			/// <summary>
			/// CommandPaste string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string CommandPaste
			{
				get
				{
					return GetPropertyValue("CommandPaste", C1Localizer.GetString("C1FileExplorer.CommandPaste","Paste"));
				}
				set { SetPropertyValue("CommandPaste", value); }
			}


			/// <summary>
			/// CommandDelete string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string CommandDelete
			{
				get
				{
					return GetPropertyValue("CommandDelete", C1Localizer.GetString("C1FileExplorer.CommandDelete", "Delete"));
				}
				set { SetPropertyValue("CommandDelete", value); }
			}


			/// <summary>
			/// CommandRefresh string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string CommandRefresh
			{
				get
				{
					return GetPropertyValue("CommandRefresh", C1Localizer.GetString("C1FileExplorer.CommandRefresh", "Refresh"));
				}
				set { SetPropertyValue("CommandRefresh", value); }
			}


			/// <summary>
			/// CommandRename string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string CommandRename
			{
				get
				{
					return GetPropertyValue("CommandRename", C1Localizer.GetString("C1FileExplorer.CommandRename", "Rename"));
				}
				set { SetPropertyValue("CommandRename", value); }
			}


			/// <summary>
			/// RenameDialogTitle string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string RenameDialogTitle
			{
				get
				{
					return GetPropertyValue("RenameDialogTitle",
						C1Localizer.GetString("C1FileExplorer.RenameDialog.Title",
							"Enter new name"));
				}
				set { SetPropertyValue("RenameDialogTitle", value); }
			}

			/// <summary>
			/// DetailViewColumnNameTitle string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string DetailViewColumnNameTitle
			{
				get
				{
					return GetPropertyValue("DetailViewColumnNameTitle",
						C1Localizer.GetString("C1FileExplorer.DetailViewColumnNameTitle",
							"Name"));
				}
				set { SetPropertyValue("DetailViewColumnNameTitle", value); }
			}

			/// <summary>
			/// DetailViewColumnSizeTitle string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string DetailViewColumnSizeTitle
			{
				get
				{
					return GetPropertyValue("DetailViewColumnSizeTitle",
						C1Localizer.GetString("C1FileExplorer.DetailViewColumnSizeTitle",
							"Size"));
				}
				set { SetPropertyValue("DetailViewColumnSizeTitle", value); }
			}

			/// <summary>
			/// DetailViewColumnSizeUnit string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string DetailViewColumnSizeUnit
			{
				get
				{
					return GetPropertyValue("DetailViewColumnSizeUnit",
						C1Localizer.GetString("C1FileExplorer.DetailViewColumnSizeUnit",
							"KB"));
				}
				set { SetPropertyValue("DetailViewColumnSizeUnit", value); }
			}

			/// <summary>
			/// LoadingText string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string LoadingText
			{
				get
				{
					return GetPropertyValue("LoadingText",
						C1Localizer.GetString("C1FileExplorer.LoadingText",
							"Loading..."));
				}
				set { SetPropertyValue("LoadingText", value); }
			}

			/// <summary>
			/// OpenFileDialogTitle string.
			/// </summary>
			[NotifyParentProperty(true)]
			[WidgetOption]
			[Browsable(false)]
			[EditorBrowsable(EditorBrowsableState.Never)]
			public string OpenFileDialogTitle
			{
				get
				{
					return GetPropertyValue("OpenFileDialogTitle",
						C1Localizer.GetString("C1FileExplorer.OpenFileDialogTitle",
							"View file: "));
				}
				set { SetPropertyValue("OpenFileDialogTitle", value); }
			}

			#endregion

			#region ** implement ICustomOptionType interface

			internal bool ShouldSerialize()
			{
				return true;
			}

			#endregion end of ** IJsonEmptiable interface implementation


			bool IJsonEmptiable.IsEmpty
			{
				get { return !ShouldSerialize(); }
			}
		}

		#endregion end of ** LocalizationOption class

	}
}
