﻿using System;

namespace C1.Web.Wijmo.Controls.C1FileExplorer
{

	///<summary>
	/// Represents the method that will handle <see cref="C1FileExplorer.ItemCommand"/> event.
	///</summary>
	///<param name="sender">The source of the event.</param>
	///<param name="args">A <see cref="C1FileExplorerEventArgs"/> that contains event data.</param>
	public delegate void C1FileExplorerEventHandler(object sender, C1FileExplorerEventArgs args);

	/// <summary>
	/// Contains event data for <see cref="C1FileExplorer.ItemCommand"/> event while performing 
	/// <see cref="FileCommand.GetItems"/> command.
	/// </summary>
	public class C1FileExplorerEventArgs : EventArgs
	{
		/// <summary>
		/// Create a new C1FileExplorerEventArgs.
		/// </summary>
		/// <param name="targetPath">The file/folder path for the current command.</param>
		internal C1FileExplorerEventArgs(string targetPath = null)
		{
			TargetPath = targetPath;
		}

		/// <summary>
		/// The target file/folder path.
		/// </summary>
		public string TargetPath { get; private set; }

		/// <summary>
		/// Gets the command type.
		/// </summary>
		public virtual FileCommand Command 
		{
			get
			{
				return FileCommand.GetItems;
			}
		}

		/// <summary>
		/// Gets or sets a value indicate whether to cancel current command.
		/// </summary>
		public bool Cancel { get; set; }
	}

	/// <summary>
	/// Contains event data for <see cref="C1FileExplorer.ItemCommand"/> event while performing 
	/// <see cref="FileCommand.CreateDirectory"/> command.
	/// </summary>
	public class C1FileExplorerCreateDirectoryEventArgs : C1FileExplorerEventArgs
	{
		internal C1FileExplorerCreateDirectoryEventArgs(string targetPath, string folderName)
			: base(targetPath)
		{
			FolderName = folderName;
		}

		/// <summary>
		/// Gets the new folder name.
		/// </summary>
		public string FolderName { get; private set; }

		/// <summary>
		/// Gets the command type.
		/// </summary>
		public override FileCommand Command
		{
			get { return FileCommand.CreateDirectory; }
		}
	}

	/// <summary>
	/// Contains event data for <see cref="C1FileExplorer.ItemCommand"/> event while performing 
	/// <see cref="FileCommand.Rename"/> command.
	/// </summary>
	public class C1FileExplorerRenameEventArgs : C1FileExplorerEventArgs
	{
		internal C1FileExplorerRenameEventArgs(string targetPath, string newName)
			: base(targetPath)
		{
			NewName = newName;
		}

		/// <summary>
		/// Gets the new name of the item used for renaming.
		/// </summary>
		public string NewName { get; private set; }

		/// <summary>
		/// Gets the command type.
		/// </summary>
		public override FileCommand Command
		{
			get { return FileCommand.Rename; }
		}
	}

	/// <summary>
	/// Contains event data for <see cref="C1FileExplorer.ItemCommand"/> event while performing 
	/// <see cref="FileCommand.Delete"/> command.
	/// </summary>
	public class C1FileExplorerDeleteEventArgs : C1FileExplorerEventArgs
	{
		internal C1FileExplorerDeleteEventArgs(string[] paths)
		{
			DeletePaths = paths;
		}

		/// <summary>
		/// Paths to delete.
		/// </summary>
		public string[] DeletePaths { get; private set; }

		/// <summary>
		/// Gets the command type.
		/// </summary>
		public override FileCommand Command
		{
			get { return FileCommand.Delete; }
		}
	}

	/// <summary>
	/// Contains event data for <see cref="C1FileExplorer.ItemCommand"/> event while performing 
	/// <see cref="FileCommand.Paste"/> command.
	/// </summary>
	public class C1FileExplorerPasteEventArgs : C1FileExplorerEventArgs
	{
		internal C1FileExplorerPasteEventArgs(string[] paths, string desPath)
			: base(desPath)
		{
			CopiedPaths = paths;
		}

		/// <summary>
		/// Paths copied that will be pasted to 
		/// </summary>
		public string[] CopiedPaths { get; private set; }

		/// <summary>
		/// Gets the command type.
		/// </summary>
		public override FileCommand Command
		{
			get { return FileCommand.Paste; }
		}
	}

	/// <summary>
	/// Contains event data for <see cref="C1FileExplorer.ItemCommand"/> event while performing 
	/// <see cref="FileCommand.Move"/> command.
	/// </summary>
	public class C1FileExplorerMoveEventArgs : C1FileExplorerEventArgs
	{
		internal C1FileExplorerMoveEventArgs(string[] paths, string despath)
			: base(despath)
		{
			MovePaths = paths;
		}

		/// <summary>
		/// Paths will be moved.
		/// </summary>
		public string[] MovePaths { get; private set; }

		/// <summary>
		/// Gets the command type.
		/// </summary>
		public override FileCommand Command
		{
			get { return FileCommand.Move; }
		}
	}
}