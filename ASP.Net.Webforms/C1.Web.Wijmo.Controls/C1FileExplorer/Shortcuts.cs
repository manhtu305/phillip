﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.UI;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1FileExplorer
{
    /// <summary>
    /// Keyboard shortcuts for FileExplorer.
    /// </summary>
	[ParseChildren(true)]
	[TypeConverter(typeof(ExpandableObjectConverter))]
	public class Shortcuts : Settings, IJsonEmptiable
	{
		private const string DEF_FocusFileExplorer = "Ctrl+F2";
		private const string DEF_FocusTreeView = "Shift+3";
		private const string DEF_FocusToolBar = "Shift+1";
		private const string DEF_FocusGrid = "Shift+4";
		private const string DEF_FocusAddressBar = "Shift+2";
		private const string DEF_PopupWindowClose = "Esc";
        private const string DEF_FocusPager = "Shift+5";
		private const string DEF_ContextMenu = "Shift+M";
		private const string DEF_Back = "Ctrl+K";
		private const string DEF_Forward = "Ctrl+L";
		private const string DEF_Open = "Enter";
		private const string DEF_Refresh = "Ctrl+F3";
		private const string DEF_NewFolder = "Shift+N";
		private const string DEF_Delete = "Delete";
		private const string DEF_UploadFile = "Ctrl+U";

        /// <summary>
        /// Get or set the shortcuts for focusing the FileExplorer.
        /// </summary>
        [C1Category("Category.Shortcuts")]
        [C1Description("C1FileExplorer.Shortcuts.FocusFileExplorer")]
		[DefaultValue(DEF_FocusFileExplorer)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string FocusFileExplorer 
		{
			get
			{
				return GetPropertyValue<string>("FocusFileExplorer", DEF_FocusFileExplorer);
			}
			set
			{
				SetPropertyValue<string>("FocusFileExplorer", value);
			}
		}

        /// <summary>
        /// Get or set the shortcuts for focusing the TreeView.
        /// </summary>
        [C1Category("Category.Shortcuts")]
        [C1Description("C1FileExplorer.Shortcuts.FocusTreeView")]
		[DefaultValue(DEF_FocusTreeView)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string FocusTreeView
		{
			get
			{
				return GetPropertyValue<string>("FocusTreeView", DEF_FocusTreeView);
			}
			set
			{
				SetPropertyValue<string>("FocusTreeView", value);
			}
		}

        /// <summary>
        /// Get or set the shortcuts for focusing the ToolBar.
        /// </summary>
        [C1Category("Category.Shortcuts")]
        [C1Description("C1FileExplorer.Shortcuts.FocusToolBar")]
        [DefaultValue(DEF_FocusToolBar)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string FocusToolBar
		{
			get
			{
				return GetPropertyValue<string>("FocusToolBar", DEF_FocusToolBar);
			}
			set
			{
				SetPropertyValue<string>("FocusToolBar", value);
			}
		}

        /// <summary>
        /// Get or set the shortcuts for focusing the Grid.
        /// </summary>
        [C1Category("Category.Shortcuts")]
        [C1Description("C1FileExplorer.Shortcuts.FocusGrid")]
        [DefaultValue(DEF_FocusGrid)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string FocusGrid
		{
			get
			{
				return GetPropertyValue<string>("FocusGrid", DEF_FocusGrid);
			}
			set
			{
				SetPropertyValue<string>("FocusGrid", value);
			}
		}

        /// <summary>
        /// Get or set the shortcuts for focusing the AddressBar.
        /// </summary>
        [C1Category("Category.Shortcuts")]
        [C1Description("C1FileExplorer.Shortcuts.FocusAddressBar")]
        [DefaultValue(DEF_FocusAddressBar)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string FocusAddressBar
		{
			get
			{
				return GetPropertyValue<string>("FocusAddressBar", DEF_FocusAddressBar);
			}
			set
			{
				SetPropertyValue<string>("FocusAddressBar", value);
			}
		}

        /// <summary>
        /// Get or set the shortcuts for closing the popup window.
        /// </summary>
        [C1Category("Category.Shortcuts")]
        [C1Description("C1FileExplorer.Shortcuts.PopupWindowClose")]
        [DefaultValue(DEF_PopupWindowClose)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string PopupWindowClose
		{
			get
			{
				return GetPropertyValue<string>("PopupWindowClose", DEF_PopupWindowClose);
			}
			set
			{
				SetPropertyValue<string>("PopupWindowClose", value);
			}
		}

        /// <summary>
        /// Get or set the shortcuts for focusing the view's pager.
        /// </summary>
        [C1Category("Category.Shortcuts")]
        [C1Description("C1FileExplorer.Shortcuts.FocusPager")]
        [DefaultValue(DEF_FocusPager)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string FocusPager
		{
			get
			{
                return GetPropertyValue<string>("FocusPager", DEF_FocusPager);
			}
			set
			{
                SetPropertyValue<string>("FocusPager", value);
			}
		}

        /// <summary>
        /// Get or set the shortcuts for focusing the grid paging slider.
        /// </summary>
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [Obsolete("Please use FocusPager property instead.")]
        [NotifyParentProperty(true)]
        public string FocusGridPagingSlider
        {
            get
            {
                return this.FocusPager;
            }
            set
            {
                this.FocusPager = value;
            }
        }

        /// <summary>
        /// Get or set the shortcuts for opening the ContextMenu.
        /// </summary>
        [C1Category("Category.Shortcuts")]
        [C1Description("C1FileExplorer.Shortcuts.ContextMenu")]
        [DefaultValue(DEF_ContextMenu)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string ContextMenu
		{
			get
			{
				return GetPropertyValue<string>("ContextMenu", DEF_ContextMenu);
			}
			set
			{
				SetPropertyValue<string>("ContextMenu", value);
			}
		}

        /// <summary>
        /// Get or set the shortcuts for navigating back.
        /// </summary>
        [C1Category("Category.Shortcuts")]
        [C1Description("C1FileExplorer.Shortcuts.Back")]
        [DefaultValue(DEF_Back)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string Back
		{
			get
			{
				return GetPropertyValue<string>("Back", DEF_Back);
			}
			set
			{
				SetPropertyValue<string>("Back", value);
			}
		}

        /// <summary>
        /// Get or set the shortcuts for navigating forward.
        /// </summary>
        [C1Category("Category.Shortcuts")]
        [C1Description("C1FileExplorer.Shortcuts.Forward")]
        [DefaultValue(DEF_Forward)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string Forward
		{
			get
			{
				return GetPropertyValue<string>("Forward", DEF_Forward);
			}
			set
			{
				SetPropertyValue<string>("Forward", value);
			}
		}

        /// <summary>
        /// Get or set the shortcuts for opening a file or folder.
        /// </summary>
        [C1Category("Category.Shortcuts")]
        [C1Description("C1FileExplorer.Shortcuts.Open")]
        [DefaultValue(DEF_Open)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string Open
		{
			get
			{
				return GetPropertyValue<string>("Open", DEF_Open);
			}
			set
			{
				SetPropertyValue<string>("Open", value);
			}
		}

        /// <summary>
        /// Get or set the shortcuts for refreshing the control.
        /// </summary>
        [C1Category("Category.Shortcuts")]
        [C1Description("C1FileExplorer.Shortcuts.Refresh")]
        [DefaultValue(DEF_Refresh)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string Refresh
		{
			get
			{
				return GetPropertyValue<string>("Refresh", DEF_Refresh);
			}
			set
			{
				SetPropertyValue<string>("Refresh", value);
			}
		}

        /// <summary>
        /// Get or set the shortcuts for creating a new folder.
        /// </summary>
        [C1Category("Category.Shortcuts")]
        [C1Description("C1FileExplorer.Shortcuts.NewFolder")]
        [DefaultValue(DEF_NewFolder)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string NewFolder
		{
			get
			{
				return GetPropertyValue<string>("NewFolder", DEF_NewFolder);
			}
			set
			{
				SetPropertyValue<string>("NewFolder", value);
			}
		}

        /// <summary>
        /// Get or set the shortcuts for deleting a file.
        /// </summary>
        [C1Category("Category.Shortcuts")]
        [C1Description("C1FileExplorer.Shortcuts.Delete")]
        [DefaultValue(DEF_Delete)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string Delete
		{
			get
			{
				return GetPropertyValue<string>("Delete", DEF_Delete);
			}
			set
			{
				SetPropertyValue<string>("Delete", value);
			}
		}

        /// <summary>
        /// Get or set the shortcuts for uploading a file.
        /// </summary>
        [C1Category("Category.Shortcuts")]
        [C1Description("C1FileExplorer.Shortcuts.UploadFile")]
        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        [DefaultValue(DEF_UploadFile)]
		[NotifyParentProperty(true)]
		[WidgetOption]
		public string UploadFile
		{
			get
			{
				return GetPropertyValue<string>("UploadFile", DEF_UploadFile);
			}
			set
			{
				SetPropertyValue<string>("UploadFile", value);
			}
		}

        bool IJsonEmptiable.IsEmpty
        {
            get 
            {
                return FocusFileExplorer == DEF_FocusFileExplorer
                    && FocusToolBar == DEF_FocusToolBar
                    && FocusAddressBar == DEF_FocusAddressBar
                    && FocusTreeView == DEF_FocusTreeView
                    && FocusGrid == DEF_FocusGrid
                    && FocusPager == DEF_FocusPager
                    && PopupWindowClose == DEF_PopupWindowClose
                    && ContextMenu == DEF_ContextMenu
                    && Back == DEF_Back
                    && Forward == DEF_Forward
                    && Open == DEF_Open
                    && Refresh == DEF_Refresh
                    && NewFolder == DEF_NewFolder
                    && Delete == DEF_Delete;
            }
        }
    }
}
