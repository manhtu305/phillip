﻿using System.Collections.Specialized;
using System.IO;
using System.Linq;
using C1.Web.Wijmo.Controls.C1FileExplorer.Actions;
using C1.Web.Wijmo.Controls.Localization;
using System;
using System.ComponentModel;
using System.Web.UI;
using C1.Web.Wijmo.Controls.Base;
using System.Drawing;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Controls.C1FileExplorer
{
	/// <summary>
	/// C1FileExplorer is a control that mimics the Windows Explorer.
	/// </summary>
	[ToolboxData("<{0}:C1FileExplorer runat=server></{0}:C1FileExplorer>")]
	[ToolboxBitmap(typeof (C1FileExplorer), "C1FileExplorer.png")]
	[ParseChildren(true)]
	[LicenseProvider]
	[WidgetDependencies(typeof (WijFileExplorer), "extensions.c1fileexplorer.js", ResourcesConst.C1WRAPPER_PRO)]
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1FileExplorer.C1FileExplorerDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1FileExplorer.C1FileExplorerDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1FileExplorer.C1FileExplorerDesigner, C1.Web.Wijmo.Controls.Design.3" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
	public partial class C1FileExplorer : C1TargetDataBoundControlBase, INamingContainer, ICallbackEventHandler, IPostBackDataHandler
	{

		#region ** fields

		private ActionParam _actionParam;
		private bool _actionCanceled;
		private bool _dataEnsured;
		private const string CssFileexplorer = "wijmo-wijfileexplorer";
		private bool _productLicensed;
		private bool _shouldNag;

		#endregion ** end of fields.

		#region Constructor

		/// <summary>
		/// Construct a new instance of C1FileExplorer.
		/// </summary>
		[C1Description("C1FileExplorer.Constructor")]
		public C1FileExplorer()
		{
			VerifyLicense();
		}

		#endregion

		#region Licensing

		internal void VerifyLicense()
		{
			var licinfo = Util.Licensing.ProviderInfo.Validate(typeof (C1FileExplorer), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}

		#endregion

		#region Methods

		/// <summary>
		/// To fire ItemCommand event.
		/// </summary>
		/// <param name="args"></param>
		protected virtual void OnItemCommand(C1FileExplorerEventArgs args)
		{
			if (ItemCommand != null)
			{
				ItemCommand(this, args);
			}
		}

		private void SetInnerState<T>(string key, T value)
		{
			InnerStates[key] = value;
		}

		private T GetInnerState<T>(string key, T defaultValue = default(T), Func<string, T> converter = null)
		{
			if (!InnerStates.ContainsKey(key))
			{
				return defaultValue;
			}

			var value = InnerStates[key];
			if (value is T)
			{
				return (T) value;
			}

			if (converter == null || value == null)
			{
				return defaultValue;
			}

			return converter(value.ToString());
		}

		internal override bool IsWijMobileControl
		{
			get
			{
				return false;
			}
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">
		/// An <see cref="T:System.EventArgs"/> object that contains the event data.
		/// </param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}

		/// <summary>
		/// Registers an OnSubmit statement in order to save the states of the widget
		/// to json hidden input.
		/// </summary>
		protected override void RegisterOnSubmitStatement()
		{
			this.RegisterOnSubmitStatement("adjustOptions");
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			if (DeletePaths == null)
			{
				ResetDeletePaths();
			}
			EnsureInnerStates();
			Page.ClientScript.GetCallbackEventReference(this, "", "", "");
			base.OnPreRender(e);
		}

		private void EnsureInnerStates()
		{
			SetInnerState("clientID", ClientID);
			SetInnerState("uniqueID", UniqueID);
			EnsureData();
		}

		private void EnsureData()
		{
			if (_dataEnsured)
			{
				return;
			}

			InitTreeInitData();
			InitCurrentFolderData();
			_dataEnsured = true;
		}

		private void ResetDeletePaths()
		{
			if (ViewPaths != null && ViewPaths.Length > 0)
			{
				DeletePaths = (string[])ViewPaths.Clone();
			}
			else
			{
				DeletePaths = new[] { InitPath };
			}
		}

		private void ResetCurrentFolder()
		{
			if (ViewPaths != null && ViewPaths.Length > 0)
			{
				CurrentFolder = ViewPaths[0];
			}
			else
			{
				CurrentFolder = InitPath;
			}
		}

		private void ResetData()
		{
			_dataEnsured = false;
			PageIndex = 0;
			PageCount = 1;
			FilterExpression = string.Empty;
			SortDirection = SortDirection.Ascending;
			SortExpression = SortField.Name;
			CurrentFolderData = null;
			TreeInitData = null;
		}

		private void InitCurrentFolderData()
		{
			if (string.IsNullOrEmpty(CurrentFolder))
			{
				return;
			}

			var param = new ActionParam
			{
				Path = CurrentFolder,
				OnlyFolder = false,
				CommandName = FileCommand.GetItems,
				SearchPatterns = SearchPatterns,
				FilterExpression = FilterExpression,
				SortDirection = SortDirection,
				SortExpression = SortExpression,
				PageIndex = AllowPaging ? PageIndex : 0,
				PageSize = AllowPaging ? PageSize : 0
			};

			var result = Action.Process(param);
			CurrentFolderData = result.ItemOperationResults.OfType<FileExplorerItem>().ToList();
			PageIndex = result.PageIndex;
			PageCount = result.PageCount;
		}

		private void InitTreeInitData()
		{
			var viewPaths = ViewPaths;
			if (ViewPaths == null || ViewPaths.Length == 0)
			{
				viewPaths = new[] {InitPath};
			}

			TreeInitData = viewPaths.SkipWhile(string.IsNullOrEmpty).Select(p =>
			{
				var item = new FileExplorerItem(p);

				if (Helper.IsAncestorFolder(p, CurrentFolder) || p == CurrentFolder)
				{
					var param = new ActionParam
					{
						Path = p,
						OnlyFolder = Mode == ExplorerMode.Default,
						CommandName = FileCommand.GetItems,
						SearchPatterns = SearchPatterns,
						UntilPath = CurrentFolder
					};

					item.Children = Action.Process(param).ItemOperationResults.OfType<FileExplorerItem>().ToList();
				}

				return item;
			}).ToList();
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified System.Web.UI.HtmlTextWriterTag. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">
		/// A System.Web.UI.HtmlTextWriter that represents the output stream to render HTML content on the client.
		/// </param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			var css = string.Format("{0} ui-widget", CssFileexplorer);
			this.AddCss(css);
			base.AddAttributesToRender(writer);
		}

		/// <summary>
		/// Sends server control content to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered in the browser window.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the server control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			if (IsDesignMode)
			{
				new DesignTimeRenderProcesser(this).Process(writer);
			}

			base.Render(writer);
		}

		#endregion

		#region ICallbackEventHandler members

		string ICallbackEventHandler.GetCallbackResult()
		{
			ActionResult result;
			if (_actionCanceled)
			{
				result = new ActionResult
				{
					Success = false,
					Error = string.Format(
					C1Localizer.GetString("C1FileExplorer.ItemCommandCanceled", "{0} command is canceled."), 
					_actionParam.CommandName)
				};
			}
			else
			{
				Action.DeletePaths = DeletePaths;
				result = Action.Process(_actionParam);
			}

			_actionParam = null;
			return JsonHelper.WidgetToString(result, this);
		}

		private bool ProcessItemCommandEvent(ActionParam actionParam)
		{
			C1FileExplorerEventArgs args;
			switch (actionParam.CommandName)
			{
				case FileCommand.GetItems:
					args = new C1FileExplorerEventArgs(actionParam.Path);
					break;
				case FileCommand.CreateDirectory:
					args = new C1FileExplorerCreateDirectoryEventArgs(Path.GetDirectoryName(actionParam.Path), Path.GetFileName(actionParam.Path));
					break;
				case FileCommand.Delete:
					args = new C1FileExplorerDeleteEventArgs(actionParam.SourcePaths.ToArray());
					break;
				case FileCommand.Move:
					args = new C1FileExplorerMoveEventArgs(actionParam.SourcePaths.ToArray(), actionParam.Path);
					break;
				case FileCommand.Paste:
					args = new C1FileExplorerPasteEventArgs(actionParam.SourcePaths.ToArray(), actionParam.Path);
					break;
				case FileCommand.Rename:
					args = new C1FileExplorerRenameEventArgs(actionParam.SourcePaths[0], Path.GetFileName(actionParam.Path));
					break;
				default:
					throw new InvalidOperationException("Wrong command.");
			}

			OnItemCommand(args);
			return !args.Cancel;
		}

		void ICallbackEventHandler.RaiseCallbackEvent(string eventArgument)
		{
			var state = JsonHelper.StringToObject(eventArgument);
			_actionParam = new ActionParam();
			JsonRestoreHelper.RestoreStateFromJson(_actionParam, state);
			_actionCanceled = !ProcessItemCommandEvent(_actionParam);
		}

		#endregion

		#region IPostbackDataHandler members

		bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			var data = JsonSerializableHelper.GetJsonData(postCollection);

			RestoreStateFromJson(data);

			return false;
		}

		void IPostBackDataHandler.RaisePostDataChangedEvent()
		{
		}

		#endregion

	}
}
