﻿using System;
using System.Collections.Generic;
using System.Text;

namespace C1.Web.Wijmo.Controls.C1FileExplorer
{
    /// <summary>
    /// Detail view type.
    /// </summary>
    public enum ViewMode
    {
        /// <summary>
        /// Show files in detail view.
        /// </summary>
        Detail,
        /// <summary>
        /// Show files in thumbnails view.
        /// </summary>
        Thumbnail
    }
}
