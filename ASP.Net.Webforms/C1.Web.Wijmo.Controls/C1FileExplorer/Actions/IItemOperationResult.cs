﻿using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.C1FileExplorer.Actions
{
	/// <summary>
	/// The result of operating FileExplorerItem while call GetItems, Paste, Delete or Move command.
	/// </summary>
	[Browsable(false)]
	[EditorBrowsable(EditorBrowsableState.Never)]
	public interface IItemOperationResult
	{
		/// <summary>
		/// The path of the item.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		string Path { get; }
	}
}
