﻿using System.Collections.Generic;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.C1FileExplorer.Actions
{
	/// <summary>
	/// The result of invoking the action.
	/// </summary>
	[Browsable(false)]
	[EditorBrowsable(EditorBrowsableState.Never)]
	public sealed class ActionResult
	{
		private int _pageCount = 1;

		/// <summary>
		/// The page count. Only for GetItems command.
		/// </summary>
		[WidgetOption]
		[DefaultValue(1)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public int PageCount
		{
			get
			{
				return _pageCount;
			}
			set
			{
				_pageCount = value;
			}
		}

		/// <summary>
		/// The page index. Only for GetItems command.
		/// </summary>
		[WidgetOption]
		[DefaultValue(0)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public int PageIndex
		{
			get; 
			set;
		}

		/// <summary>
		/// Gets and sets whether the action is success.
		/// </summary>
		[WidgetOption]
		[DefaultValue(false)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool Success
		{
			get;
			set;
		}

		/// <summary>
		/// The error.
		/// </summary>
		[WidgetOption]
		[DefaultValue(null)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string Error
		{
			get;
			set;
		}

		/// <summary>
		/// The results of operating items.
		/// It has different meaning for different command.
		/// For GetItems command, it is an IFileExplorerItem collection which contains 
		/// sub folders and files.
		/// It contains the success or error informations when call following commands:
		/// Paste
		/// Delete
		/// Move
		/// </summary>
		[WidgetOption]
		[DefaultValue(null)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public IList<IItemOperationResult> ItemOperationResults
		{
			get;
			set;
		}
	}
}