﻿using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.C1FileExplorer.Actions
{
	/// <summary>
	/// The result of operating an item.
	/// </summary>
	[Browsable(false)]
	[EditorBrowsable(EditorBrowsableState.Never)]
	public sealed class ItemOperationResult : IItemOperationResult
	{
		public ItemOperationResult(string path)
		{
			Path = path;
		}

		/// <summary>
		/// Gets whether the operation succeeds.
		/// </summary>
		[WidgetOption]
		[DefaultValue(false)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool Success
		{
			get;
			set;
		}

		/// <summary>
		/// Gets the error.
		/// </summary>
		[WidgetOption]
		[DefaultValue(null)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string Error
		{
			get;
			set;
		}

		/// <summary>
		/// Gets the item's path.
		/// </summary>
		[WidgetOption]
		[DefaultValue(null)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string Path
		{
			get; private set;
		}
	}
}