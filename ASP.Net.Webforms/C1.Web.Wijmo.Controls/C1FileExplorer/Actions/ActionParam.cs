﻿using System.ComponentModel;
using System.Web.UI.WebControls;

namespace C1.Web.Wijmo.Controls.C1FileExplorer.Actions
{
	/// <summary>
	/// The param of the FileExplorer action.
	/// </summary>
	[Browsable(false)]
	[EditorBrowsable(EditorBrowsableState.Never)]
	public sealed class ActionParam
	{
		private static readonly string[] DefaultSearchPatterns = { "*.jpg", "*.jpeg", "*.gif", "*.png" };
		private string[] _searchPatterns;
		private string _filterExpression;
		private string[] _sourcePaths;

		/// <summary>
		/// The FileCommand.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public FileCommand CommandName
		{
			get;
			set;
		}

		/// <summary>
		/// The search patterns. Only used for GetItems command.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string[] SearchPatterns
		{
			get
			{
				return _searchPatterns ?? DefaultSearchPatterns;
			}
			set
			{
				_searchPatterns = value;
			}
		}

		/// <summary>
		/// The filter expression. Only used for GetItems command.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string FilterExpression
		{
			get
			{
				return _filterExpression ?? string.Empty;
			}
			set
			{
				_filterExpression = value;
			}
		}

		/// <summary>
		/// The page size. Only used for GetItems command.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public int PageSize
		{
			get;
			set;
		}

		/// <summary>
		/// The page index. Only used for GetItems command.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public int PageIndex
		{
			get;
			set;
		}

		/// <summary>
		/// The sort expression. Only used for GetItems command.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public SortField SortExpression
		{
			get;
			set;
		}

		/// <summary>
		/// The sort direction. Only used for GetItems command.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public SortDirection SortDirection
		{
			get;
			set;
		}

		/// <summary>
		/// Gets and sets a value to indicate whether only get the folders. Only used for GetItems command.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool OnlyFolder
		{
			get;
			set;
		}

		/// <summary>
		/// The path or target path. The target path to perform the operation.
		/// It has different meaning for different command:
		/// GetItems - Get the sub folders and files of the specified path.
		/// Paste - Copy the specified paths defined in sourcePaths to the path.
		/// CreateDirectory - The path of the new directory to create.
		/// Rename - Rename the specified path defined in sourcePaths to the new path defined in path.
		/// Move - Move the specified paths defined in sourcePaths to the path.
		/// Delete - Invalid.
		/// GetHostUri - Invalid.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string Path
		{
			get;
			set;
		}

		/// <summary>
		// The source paths used for delete, move or paste command.
		/// Delete - Delete the specified paths.
		/// Paste - Copy the specified paths defined in sourcePaths to the path.
		/// Move - Move the specified paths defined in sourcePaths to the path.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string[] SourcePaths
		{
			get { return _sourcePaths ?? (_sourcePaths = new string[] {}); }
			set { _sourcePaths = value; }
		}

		/// <summary>
		/// Only for GetItems. Get items from the folder specified in path until the sub-folder specified in UntilPath.
		/// </summary>
		internal string UntilPath
		{
			get; 
			set;
		}

		internal ActionParam Clone()
		{
			return new ActionParam
			{
				CommandName = CommandName,
				SearchPatterns = SearchPatterns,
				FilterExpression = FilterExpression,
				PageSize = PageSize,
				PageIndex = PageIndex,
				SortExpression = SortExpression,
				SortDirection = SortDirection,
				OnlyFolder = OnlyFolder,
				Path = Path,
				SourcePaths = SourcePaths,
				UntilPath = UntilPath
			};
		}
	}
}