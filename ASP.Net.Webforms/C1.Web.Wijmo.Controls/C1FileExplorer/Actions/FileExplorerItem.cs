﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;

namespace C1.Web.Wijmo.Controls.C1FileExplorer.Actions
{
	/// <summary>
	/// FileExplorer Item.
	/// </summary>
	[Browsable(false)]
	[EditorBrowsable(EditorBrowsableState.Never)]
	public sealed class FileExplorerItem : IItemOperationResult
	{
		/// <summary>
		/// Create a FileExplorerItem with the specified path.
		/// </summary>
		/// <param name="path"></param>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public FileExplorerItem(string path)
		{
			var localPath = Helper.MapPath(path);
			if (Helper.IsDirectory(localPath))
			{
				var dirInfo = new DirectoryInfo(localPath);
				InitByDirInfo(dirInfo);
			}
			else
			{
				var fileInfo = new FileInfo(localPath);
				InitByDirInfo(fileInfo);
			}

			Path = path;
		}

		/// <summary>
		/// Create a FileExplorerItem with the specified parent path and DirectoryInfo.
		/// </summary>
		/// <param name="virtualParentPath"></param>
		/// <param name="dirInfo"></param>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public FileExplorerItem(string virtualParentPath, DirectoryInfo dirInfo)
		{
			InitByDirInfo(dirInfo);
			Path = Helper.CombinePath(virtualParentPath, Name);
		}

		/// <summary>
		/// Create a FileExplorerItem with the specified parent path and FileInfo.
		/// </summary>
		/// <param name="virtualParentPath"></param>
		/// <param name="fileInfo"></param>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public FileExplorerItem(string virtualParentPath, FileInfo fileInfo)
		{
			InitByDirInfo(fileInfo);
			Path = Helper.CombinePath(virtualParentPath, Name);
		}

		private void InitByDirInfo(FileInfo fileInfo)
		{
			Name = fileInfo.Name;
			NameWithoutExtension = System.IO.Path.GetFileNameWithoutExtension(fileInfo.FullName);
			Size = fileInfo.Length;
			CreatedAt = Helper.ToUnixTime(fileInfo.CreationTime);
			ModifiedAt = Helper.ToUnixTime(fileInfo.LastWriteTime);
		}

		private void InitByDirInfo(DirectoryInfo dirInfo)
		{
			IsFolder = true;
			HasChildren = dirInfo.GetFileSystemInfos().Length > 0;
			HasSubFolders = dirInfo.GetDirectories().Length > 0;
			Name = dirInfo.Name;
			NameWithoutExtension = dirInfo.Name;
			CreatedAt = Helper.ToUnixTime(dirInfo.CreationTime);
			ModifiedAt = Helper.ToUnixTime(dirInfo.LastWriteTime);
		}

		/// <summary>
		/// Last modified date
		/// </summary>
		[WidgetOption]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public long ModifiedAt { get; private set; }

		/// <summary>
		/// Created Date
		/// </summary>
		[WidgetOption]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public long CreatedAt { get; private set; }

		/// <summary>
		/// Gets current item's path.
		/// </summary>
		[WidgetOption]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string Path { get; private set; }

		internal string NameWithoutExtension { get; private set; }

		internal string Name { get; private set; }

		/// <summary>
		/// Gets whether current item has subfolders.
		/// </summary>
		[WidgetOption]
		[DefaultValue(null)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool? HasSubFolders { get; private set; }

		/// <summary>
		/// Gets whether current item has children.
		/// </summary>
		[WidgetOption]
		[DefaultValue(null)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool? HasChildren { get; private set; }

		/// <summary>
		/// Gets whether current item is a folder.
		/// </summary>
		[WidgetOption]
		[DefaultValue(false)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public bool IsFolder { get; private set; }

		/// <summary>
		/// Gets whether current item's size.
		/// </summary>
		[WidgetOption]
		[DefaultValue(null)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public long? Size { get; private set; }

		/// <summary>
		/// Gets current item's children.
		/// </summary>
		[WidgetOption]
		[DefaultValue(null)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public IList<FileExplorerItem> Children
		{
			get; set;
		}
	}
}