﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace C1.Web.Wijmo.Controls.C1FileExplorer.Actions
{
    internal static class Helper
    {
        private const char ForwardSlash = '/';
        private const char BackwardSlash = '\\';
        //private const char FolderSeparator = '/';

        public static int ComparisonWithName(FileExplorerItem a, FileExplorerItem b)
        {
            var aNameWithoutExtension = a.NameWithoutExtension;
            var bNameWithoutExtension = b.NameWithoutExtension;

            if (string.Equals(aNameWithoutExtension, bNameWithoutExtension, StringComparison.OrdinalIgnoreCase))
            {
                return String.Compare(a.Name, b.Name, StringComparison.OrdinalIgnoreCase);
            }

            var numRegex = new Regex("\\d+");
            var matches = numRegex.Matches(aNameWithoutExtension + bNameWithoutExtension).Cast<Match>().ToList();
            if (matches.Any())
            {
                var maxLen = matches.Max(num => num.Length);
                aNameWithoutExtension = numRegex.Replace(aNameWithoutExtension, (m => m.Value.PadLeft(maxLen, '0')));
                bNameWithoutExtension = numRegex.Replace(bNameWithoutExtension, (m => m.Value.PadLeft(maxLen, '0')));
            }

            return String.Compare(aNameWithoutExtension, bNameWithoutExtension, StringComparison.OrdinalIgnoreCase);
        }


        public static string MapPath(string path)
        {
            return HttpContext.Current.Server.MapPath(path);
        }

        public static bool IsDirectory(string path)
        {
            return (File.GetAttributes(path) & FileAttributes.Directory) == FileAttributes.Directory;
        }

        public static FileExplorerItem CreateFileExplorerItem(string parentPath, string localPath)
        {
            if (IsDirectory(localPath))
            {
                var dirInfo = new DirectoryInfo(localPath);
                return new FileExplorerItem(parentPath, dirInfo);
            }

            var fileInfo = new FileInfo(localPath);
            return new FileExplorerItem(parentPath, fileInfo);
        }

        public static bool IsAncestorFolder(string mayAncestorPath, string mayDescendantPath)
        {
            return mayDescendantPath.IndexOf(EnsureFolderSeparator(mayAncestorPath), StringComparison.OrdinalIgnoreCase) == 0;
        }

        public static string EnsureFolderSeparator(string path)
        {
            var folderSeparator = GetFolderSeparator(path);
            return path[path.Length - 1] == folderSeparator ? path : path + folderSeparator;
        }

        public static char GetFolderSeparator(string path)
        {
            return path.Contains(ForwardSlash) ? ForwardSlash : BackwardSlash;
        }

        public static string CombinePath(string path1, string path2)
        {
            var folderSeparator = GetFolderSeparator(path1);
            var paths = new string[] { path1, path2 };

            return string.Join(folderSeparator.ToString(), paths);
        }

        public static TEnum SafeParseEnum<TEnum>(this string strValue) where TEnum : struct, IConvertible
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            try
            {
                return (TEnum)Enum.Parse(typeof(TEnum), strValue, true);
            }
            catch (Exception)
            {
                return default(TEnum);
            }
        }

        public static long ToUnixTime(DateTime date)
        {
            return (long)(date.ToUniversalTime().Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }
    }
}