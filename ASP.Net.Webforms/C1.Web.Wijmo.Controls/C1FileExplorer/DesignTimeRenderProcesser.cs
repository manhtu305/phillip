﻿using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1FileExplorer
{
	internal class DesignTimeRenderProcesser
	{
		private const int DefaultHeight = 400;
		private const int ToolbarHeight = 45;
		private const int AddressBarHeight = 30;

		private const string CssFileexplorer = "wijmo-wijfileexplorer";
		private const string CssFileexplorerDesigntime = CssFileexplorer + "-designtime";
		private const string CssLink = "wijmo-wijfileexplorer-link";

		//toolbar
		private const string CssToolbar = CssFileexplorer + "-toolbar";
		private const string CssBack = CssFileexplorer + "-back";
		private const string CssForward = CssFileexplorer + "-forward";
		private const string CssOpen = CssFileexplorer + "-open";
		private const string CssRefresh = CssFileexplorer + "-refresh";
		private const string CssNewfolder = CssFileexplorer + "-newfolder";
		private const string CssDelete = CssFileexplorer + "-delete";
		private const string CssGridview = CssFileexplorer + "-grid";
		private const string CssThumbnail = CssFileexplorer + "-thumbnail";

		//address bar
		private const string CssAddressbar = CssFileexplorer + "-addressbar";

		//address
		private const string CssAddresspanel = CssFileexplorer + "-addresspanel";
		private const string CssAddress = CssFileexplorer + "-address";

		//filter
		private const string CssFilterpanel = CssFileexplorer + "-filterpanel";
		private const string CssFilter = CssFileexplorer + "-filter";

		private readonly C1FileExplorer _c1FileExplorer;
		private const string DesignTimeCssFile = "C1.Web.Wijmo.Controls.C1FileExplorer.Resources.c1fileexplorerdesign.css";

		public DesignTimeRenderProcesser(C1FileExplorer c1FileExplorer)
		{
			_c1FileExplorer = c1FileExplorer;
		}

		private FileExplorerControls VisibleControls
		{
			get { return _c1FileExplorer.VisibleControls; }
		}

		private ControlCollection Controls
		{
			get { return _c1FileExplorer.Controls; }
		}

		private ExplorerMode Mode
		{
			get { return _c1FileExplorer.Mode; }
		}

		private bool ShowToolbar
		{
			get { return (VisibleControls & FileExplorerControls.Toolbar) == FileExplorerControls.Toolbar; }
		}

		private bool ShowAddressBox
		{
			get { return (VisibleControls & FileExplorerControls.AddressBox) == FileExplorerControls.AddressBox; }
		}

		private bool ShowFilterTextBox
		{
			get { return (VisibleControls & FileExplorerControls.FilterTextBox) == FileExplorerControls.FilterTextBox; }
		}

		private bool ShowGrid
		{
			get { return Mode == ExplorerMode.Default && (VisibleControls & FileExplorerControls.Grid) == FileExplorerControls.Grid; }
		}

		private bool ShowListView
		{
			get { return Mode == ExplorerMode.Default && (VisibleControls & FileExplorerControls.ListView) == FileExplorerControls.ListView; }
		}

		private bool ShowTreeView
		{
			get { return (VisibleControls & FileExplorerControls.TreeView) == FileExplorerControls.TreeView; }
		}

		public void Process(HtmlTextWriter writer)
		{
			_c1FileExplorer.RegisterDesignTimeStyleSheet(writer, DesignTimeCssFile);
			_c1FileExplorer.AddCss(CssFileexplorerDesigntime);
			CreateChildControls();
		}

		private HtmlGenericControl GenerateButton(string cssName, string tooltip)
		{
			var btn = new HtmlGenericControl("li");
			var css = cssName;

			if (_c1FileExplorer.WijmoCssAdapter == "bootstrap")
			{
				css = string.Format("btn btn-primary {0}", css);
			}

			btn.Attributes["class"] = string.Format("ui-state-default ui-corner-all {0}", css);
			btn.InnerHtml = string.Format("<a href=\"#\" class=\"{0}\"><span class=\"ui-icon\"></span></a>", CssLink);
			btn.Attributes["title"] = tooltip;

			return btn;
		}

		private Panel GenerateToolBar()
		{
			var toolbar = new HtmlGenericControl("ul");
			toolbar.Attributes["class"] = string.Format("ui-widget ui-helper-reset ui-helper-clearfix ui-state-default {0}", CssToolbar);

			toolbar.Controls.Add(GenerateButton(CssBack, C1Localizer.GetString("C1FileExplorer.Toolbar.Back", "Back")));
			toolbar.Controls.Add(GenerateButton(CssForward, C1Localizer.GetString("C1FileExplorer.Toolbar.Forward", "Forward")));
			toolbar.Controls.Add(GenerateButton(CssOpen, C1Localizer.GetString("C1FileExplorer.Toolbar.Open", "Open")));
			toolbar.Controls.Add(GenerateButton(CssRefresh, C1Localizer.GetString("C1FileExplorer.Toolbar.Refresh", "Refresh")));
			toolbar.Controls.Add(GenerateButton(CssNewfolder, C1Localizer.GetString("C1FileExplorer.Toolbar.NewFolder", "New Folder")));
			toolbar.Controls.Add(GenerateButton(CssDelete, C1Localizer.GetString("C1FileExplorer.Toolbar.Delete", "Delete")));
			
			if (ShowGrid && ShowListView)
			{
				toolbar.Controls.Add(GenerateButton(CssGridview,
					C1Localizer.GetString("C1FileExplorer.Toolbar.GridView", "Grid View")));
				toolbar.Controls.Add(GenerateButton(CssThumbnail,
					C1Localizer.GetString("C1FileExplorer.Toolbar.ThumbnailsView", "Thumbnails View")));
			}

			var toolbarPanel = new Panel();
			toolbarPanel.Controls.Add(toolbar);

			return toolbarPanel;
		}

		private Panel GenerateAddressBar()
		{
			var pnl = new Panel {CssClass = string.Format("{0} ui-helper-clearfix ui-widget-content", CssAddressbar)};
			if (ShowAddressBox)
			{
				var addressBoxPanel = new Panel {CssClass = CssAddresspanel};
				var addressBox = new TextBox {CssClass = CssAddress, ReadOnly = true};
				addressBoxPanel.Controls.Add(addressBox);
				pnl.Controls.Add(addressBoxPanel);
			}

			if (ShowFilterTextBox)
			{
				var filterBoxPanel = new Panel {CssClass = CssFilterpanel};
				var filter = new TextBox {CssClass = CssFilter};
				filterBoxPanel.Controls.Add(filter);
				pnl.Controls.Add(filterBoxPanel);
			}

			return pnl;
		}

		private C1Splitter.C1Splitter GenerateFileSplitter(int height)
		{
			return new C1Splitter.C1Splitter("c1v01xIXFksavw5HDgsOLwp/ClRxGd33Djw==")
			{
				ID = "fs",
				// if set splitter's width 100%, it works as 100px,
				// so set width to a large enough value
				Width = _c1FileExplorer.TreePanelWidth + 20,
				SplitterDistance = _c1FileExplorer.TreePanelWidth,
				Height = Unit.Pixel(height)
			};
		}

		private void CreateChildControls()
		{
			var heightInUnit = _c1FileExplorer.Height;
			var height = (heightInUnit.Type == UnitType.Pixel && heightInUnit != Unit.Empty) 
				? (int)_c1FileExplorer.Height.Value : DefaultHeight;

			Controls.Clear();
			if (ShowToolbar)
			{
				Controls.Add(GenerateToolBar());
				height -= ToolbarHeight;
			}

			if (ShowAddressBox || ShowFilterTextBox)
			{
				Controls.Add(GenerateAddressBar());
				height -= AddressBarHeight;
			}

			if (height < 0)
			{
				return;
			}

			if (ShowTreeView && (ShowGrid || ShowListView))
			{
				Controls.Add(GenerateFileSplitter(height));
			}
			// add a fake treeview or grid/listview
			else if(ShowTreeView || (ShowGrid || ShowListView))
			{
				Controls.Add(new Panel { Width = Unit.Percentage(100), Height = Unit.Pixel(height) });
			}
		}
	}
}
