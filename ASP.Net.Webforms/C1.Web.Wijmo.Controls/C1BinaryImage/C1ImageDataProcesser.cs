﻿using System;
using System.Drawing.Imaging;
using System.Linq;
using System.Web.UI.WebControls;
using System.Drawing;
using System.IO;
using Image = System.Drawing.Image;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1BinaryImage
{
	internal class C1ImageDataProcesser
	{
		public static C1BinaryImageData ProcessImageData(byte[] dataValue, string savedImageName, ImageResizeMode resizeMode, ImageCropPosition cropPosition, Unit width, Unit height)
		{
			if (dataValue == null)
			{
				return null;
			}

			var mimeType = "";
			byte[] imageData;
			imageData = ProcessData(dataValue, out mimeType);

			var image = GetImageByData(imageData);
			if (image != null)
			{
				if (resizeMode != ImageResizeMode.None)
				{
					if (width.IsEmpty)
					{
						throw new ArgumentException(C1Localizer.GetString("C1BinaryImageData.MustSetWidth"));
					}

					if (height.IsEmpty)
					{
						throw new ArgumentException(C1Localizer.GetString("C1BinaryImageData.MustSetHeight"));
					}

					var pixWidth = (int)width.Value;
					var pixHeight = (int)height.Value;
					imageData = ConvertImageToBytes(Resize(image, resizeMode, cropPosition, pixWidth, pixHeight), mimeType);
				}
			}
			else
			{
				return null;
			}

			var data = new C1BinaryImageData();
			data.Data = imageData;
			data.MimeType = mimeType;
			data.ImageFileName = savedImageName;
			return data;
		}

		private static byte[] ProcessData(byte[] dataValue, out string mimeType)
		{
			for (int i = 0; i < dataValue.Length; i++)
			{
				mimeType = GetMimeType(dataValue, i);
				if (!string.IsNullOrEmpty(mimeType))
				{
					return SubData(dataValue, i);
				}
			}

			mimeType = "application/octet-stream";
			return dataValue;
		}

		private static string GetMimeType(byte[] dataValue, int offset)
		{
			if (IsMatchMask(dataValue, offset, new byte[] { 0x42, 0x4d }))
			{
				return "image/bmp";
			}

			if (IsMatchMask(dataValue, offset, new byte[] { 0xff, 0xd8 }))
			{
				return "image/jpeg";
			}

			if (IsMatchMask(dataValue, offset, new byte[] { 0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A }))
			{
				return "image/png";
			}

			if (IsMatchMask(dataValue, offset, new byte[] { 0x4d, 0x4d })
				|| IsMatchMask(dataValue, offset, new byte[] { 0x49, 0x49 }))
			{
				return "image/tiff";
			}
	
			if(IsMatchMask(dataValue, offset, new byte[] { 0x47, 0x49, 0x46, 0x38, 0x39, 0x61 })
				|| IsMatchMask(dataValue, offset, new byte[] { 0x47, 0x49, 0x46, 0x38, 0x37, 0x61 }))
			{
				return "image/gif";
			}

			return string.Empty;
		}

		private static bool IsMatchMask(byte[] dataValue, int offset, byte[] mask)
		{
			for (int i = 0; i < mask.Length; i++)
			{
				if (!(offset + i < dataValue.Length))
				{
					return false;
				}

				if (mask[i] != dataValue[offset + i])
				{
					return false;
				}
			}
			return true;
		}

		private static byte[] SubData(byte[] dataValue, int offset)
		{
			var sub = new byte[dataValue.Length - offset];
			for (int i = 0; i < sub.Length; i++)
			{
				sub[i] = dataValue[offset + i];
			}
			return sub;
		}

		private static Image Resize(Image image, ImageResizeMode resizeMode, ImageCropPosition cropPosition, int width, int height)
		{
			switch (resizeMode)
			{
				case ImageResizeMode.Crop:
					return CropImage(image, cropPosition, width, height);
				case ImageResizeMode.Fill:
					return FillImage(image, width, height);
				case ImageResizeMode.Fit:
					return FitImage(image, width, height);
				default:
					return image;
			}
		}

		private static Image FitImage(Image image, int width, int height)
		{
			Size realSize = new Size();
			if ((double)image.Width / (double)image.Height > (double)width / (double)height)
			{
				realSize.Width = width;
				realSize.Height = realSize.Width * image.Height / image.Width;
			}
			else
			{
				realSize.Height = height;
				realSize.Width = realSize.Height * image.Width / image.Height;
			}

			var newImg = new Bitmap(realSize.Width, realSize.Height);
			using (var g = Graphics.FromImage(newImg))
			{
				g.DrawImage(image, 0, 0, realSize.Width, realSize.Height);
			}

			return newImg;
		}

		private static Image FillImage(Image image, int width, int height)
		{
			var newImg = new Bitmap(width, height);
			using (var g = Graphics.FromImage(newImg))
			{
				g.DrawImage(image, 0, 0, width, height);
			}

			return newImg;
		}

		private static Image CropImage(Image image, ImageCropPosition cropPosition, int width, int height)
		{
			var realSize = new Size();

			realSize.Width = Math.Min(image.Width, width);
			realSize.Height = Math.Min(image.Height, height);

			if (realSize == image.Size)
			{
				return image;
			}
			else
			{
				var newPosition = new Point();
				switch (cropPosition)
				{
					case ImageCropPosition.Top:
						newPosition.X = (image.Width - realSize.Width) / 2;
						newPosition.Y = 0;
						break;
					case ImageCropPosition.Bottom:
						newPosition.X = (image.Width - realSize.Width) / 2;
						newPosition.Y = image.Height - realSize.Height;
						break;
					case ImageCropPosition.Left:
						newPosition.X = 0;
						newPosition.Y = (image.Height - realSize.Height) / 2;
						break;
					case ImageCropPosition.Right:
						newPosition.X = image.Width - realSize.Width;
						newPosition.Y = (image.Height - realSize.Height) / 2;
						break;
					case ImageCropPosition.Center:
						newPosition.X = (image.Width - realSize.Width) / 2;
						newPosition.Y = (image.Height - realSize.Height) / 2;
						break;
					default:
						return image;
				}

				var newImg = new Bitmap(realSize.Width, realSize.Height);
				using (var g = Graphics.FromImage(newImg))
				{
					g.DrawImage(image, 0, 0, new Rectangle(newPosition, realSize), GraphicsUnit.Pixel);
				}

				return newImg;
			}
		}


		public static Image GetImageByData(byte[] imageData)
		{
			try
			{
				return Bitmap.FromStream(new MemoryStream(imageData));
			}
			catch
			{
				return null;
			}
		}

		private static byte[] ConvertImageToBytes(Image image, string mimeType)
		{
			byte[] imageData;

			using (MemoryStream stream = new MemoryStream())
			{
				image.Save(stream, GetImageFormatByMimeType(mimeType));
				imageData = stream.ToArray();
			}

			return imageData;
		}

		private static ImageFormat GetImageFormatByMimeType(string mimeType)
		{
			switch (mimeType)
			{
				case "image/bmp":
					return ImageFormat.Bmp;
				case "image/gif":
					return ImageFormat.Gif;
				case "image/jpeg":
					return ImageFormat.Jpeg;
				case "image/png":
					return ImageFormat.Png;
				case "image/tiff":
					return ImageFormat.Tiff;
				default:
					return ImageFormat.Bmp;
			}
		}
	}
}
