﻿using System;

namespace C1.Web.Wijmo.Controls.C1BinaryImage
{
	/// <summary>
	/// the information about the binary image
	/// </summary>
	[Serializable]
	public class C1BinaryImageData
	{
		/// <summary>
		/// The binary image data.
		/// </summary>
		public byte[] Data { get; set; }

		/// <summary>
		/// The name of the file which will appear inside of the SaveAs browser dialog
		/// </summary>
		public string ImageFileName { get; set; }

		/// <summary>
		/// The mimeType of the binary image.
		/// </summary>
		public string MimeType { get; set; }
	}
}
