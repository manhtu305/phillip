﻿using System;

namespace C1.Web.Wijmo.Controls.C1BinaryImage
{
	/// <summary>
	/// The crop position will use when cropping the image.
	/// </summary>
	public enum ImageCropPosition
	{
		/// <summary>
		/// Crop images from the top center.
		/// </summary>
		Top,

		/// <summary>
		/// Crop images from the center.
		/// </summary>
		Center,

		/// <summary>
		/// Crop images from the bottom center.
		/// </summary>
		Bottom,

		/// <summary>
		/// Crop images from the middle left.
		/// </summary>
		Left,

		/// <summary>
		/// Crop images from the middle right.
		/// </summary>
		Right
	}

	/// <summary>
	/// The resize mode that BinaryImage will use to resize the image.
	/// </summary>
	public enum ImageResizeMode
	{
		/// <summary>
		/// Default, do nothing.
		/// </summary>
		None,

		/// <summary>
		/// The image will be sized to fill both given dimensions.
		/// </summary>
		Fit,

		/// <summary>
		/// The image will be trimmed.
		/// </summary>
		Crop,

		/// <summary>
		/// The image will be sized to fill both given dimensions.
		/// </summary>
		Fill
	}
}
