﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace C1.Web.Wijmo.Controls.C1BinaryImage
{
	/// <summary>
	/// A httpHandler to output the image data.
	/// </summary>
	public class C1BinaryImageHandler : IHttpHandler
	{
		/// <summary>
		/// Enables processing of HTTP Web requests by a custom HttpHandler.
		/// </summary>
		/// <param name="context">The current <see cref="HttpContext"/>.</param>
		public void ProcessRequest(HttpContext context)
		{
			context.Response.Clear();

			var imageKey = context.Request.QueryString.Get("imgkey");
			if (!string.IsNullOrEmpty(imageKey))
			{
				var imageData = ProcessImageData(C1BinaryImageCacheStorage.LoadImage(imageKey));

				if (imageData != null)
				{
					if (!string.IsNullOrEmpty(imageData.MimeType))
					{
						context.Response.ContentType = imageData.MimeType;
					}

					if (!string.IsNullOrEmpty(imageData.ImageFileName))
					{
						context.Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"; filename*=UTF-8''{0}", HttpUtility.UrlEncode(imageData.ImageFileName, System.Text.Encoding.UTF8)));
					}

					context.Response.BinaryWrite(imageData.Data);
				}
			}
		}

		/// <summary>
		/// Process the <see cref="C1BinaryImageData"/>.
		/// </summary>
		/// <param name="imageData">The <see cref="C1BinaryImageData"/>.</param>
		/// <returns>The <see cref="C1BinaryImageData"/> after process.</returns>
		public virtual C1BinaryImageData ProcessImageData(C1BinaryImageData imageData)
		{
			return imageData;
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}
