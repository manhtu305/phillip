﻿using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Web;
using System.Collections.Generic;
using C1.Web.Wijmo.Controls.Localization;

[assembly: WebResource("C1.Web.Wijmo.Controls.C1BinaryImage.Images.C1BinaryImage_Desgin.png", "image/x-png")]
[assembly: WebResource("C1.Web.Wijmo.Controls.C1BinaryImage.Images.C1BinaryImage_Error.png", "image/x-png")]
namespace C1.Web.Wijmo.Controls.C1BinaryImage
{

	/// <summary>
	/// Represents a BinaryImage in an ASP.NET Web page.
	/// </summary>
	#if ASP_NET45
	[Designer("C1.Web.Wijmo.Controls.Design.C1BinaryImage.C1BinaryImageDesigner, C1.Web.Wijmo.Controls.Design.45" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
	[Designer("C1.Web.Wijmo.Controls.Design.C1BinaryImage.C1BinaryImageDesigner, C1.Web.Wijmo.Controls.Design.4" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1BinaryImage.C1BinaryImageDesigner, C1.Web.Wijmo.Controls.Design.3" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
	[Designer("C1.Web.Wijmo.Controls.Design.C1BinaryImage.C1BinaryImageDesigner, C1.Web.Wijmo.Controls.Design.2" + C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif

	[DefaultProperty("ImageData")]
	[LicenseProviderAttribute()]
	[ToolboxBitmap(typeof(C1BinaryImage), "C1BinaryImage.png")]
	[ToolboxData("<{0}:C1BinaryImage runat=server></{0}:C1BinaryImage>")]
	public class C1BinaryImage : C1TargetControlBase
	{
		#region ** fields
		[SmartAssembly.Attributes.DoNotObfuscate]
		internal const string DefaultHttpHandlerUrl = "~/C1BinaryImageResource.axd";
		private string imgkey;

		#region licensing
		internal bool _productLicensed;
		private bool _shouldNag;
		#endregion

		#endregion

		#region ** public properties

		/// <summary>
		/// Gets or sets the binary image data for the control.
		/// </summary>
		[Bindable(true)]
		[Browsable(false)]
		[C1Category("Category.Data")]
		[DefaultValue((string)null)]
		public byte[] ImageData { get; set; }

		/// <summary>
		/// The alternate text displayed in the Image control when the image is unavailable.
		/// </summary>
		[Bindable(true)]
		[C1Category("Category.Appearance")]
		[C1Description("C1BinaryImage.AlternateText")]
		[DefaultValue("")]
		public string AlternateText
		{
			get
			{
				object val = ViewState["AlternateText"];
				if (val != null)
				{
					return (string)val;
				}
				return "";
			}
			set
			{
				ViewState["AlternateText"] = value;
			}
		}

		/// <summary>
		/// The name of the file which will appear inside of the SaveAs browser dialog.
		/// This property is effective only when setting ImageData property.
		/// </summary>
		[Bindable(true)]
		[C1Category("Category.Behavior")]
		[C1Description("C1BinaryImage.SavedImageName")]
		[DefaultValue("")]
		public string SavedImageName
		{
			get
			{
				object val = ViewState["SavedImageName"];
				if (val != null)
				{
					return (string)val;
				}
				return "";
			}
			set
			{
				ViewState["SavedImageName"] = value;
			}
		}

		/// <summary>
		/// The location of an image to display in the BinaryImage control.
		/// </summary>
		[Bindable(true)]
		[C1Category("Category.Appearance")]
		[C1Description("C1BinaryImage.ImageUrl")]
		[DefaultValue("")]
		[Editor("System.Web.UI.Design.ImageUrlEditor, System.Design", typeof(UITypeEditor))]
		[UrlProperty]
		public string ImageUrl
		{
			get
			{
				object val = ViewState["ImageUrl"];
				if (val != null)
				{
					return (string)val;
				}
				return "";
			}
			set
			{
				ViewState["ImageUrl"] = value;
			}
		}

		/// <summary>
		/// The URL for the file that contains a detailed description for the image.
		/// </summary>
		[Bindable(true)]
        [C1Category("Category.Behavior")]
		[C1Description("C1BinaryImage.DescriptionUrl")]
		[DefaultValue("")]
		[Editor("System.Web.UI.Design.ImageUrlEditor, System.Design", typeof(UITypeEditor))]
		[UrlProperty]
		public string DescriptionUrl
		{
			get
			{
				object val = ViewState["DescriptionUrl"];
				if (val != null)
				{
					return (string)val;
				}
				return "";
			}
			set
			{
				ViewState["DescriptionUrl"] = value;
			}
		}

		/// <summary>
		/// Specifies the crop position will use when cropping the image. This property has a meaning only when the ResizeMode property is set to BinaryImageResizeMode.Crop. Default value is BinaryImageCropPosition.Center.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1BinaryImage.CropPosition")]
		[DefaultValue(typeof(ImageCropPosition), "Center")]
		public ImageCropPosition CropPosition
		{
			get
			{
				object val = ViewState["CropPosition"];
				if (val != null)
				{
					return (ImageCropPosition)val;
				}
				return ImageCropPosition.Center;
			}
			set
			{
				ViewState["CropPosition"] = value;
			}
		}

		/// <summary>
		/// Specifies the resize mode that BinaryImage will use to resize the image. Default value is BinaryImageResizeMode.None, indicating no resizing will be performed.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1BinaryImage.ResizeMode")]
		[DefaultValue(ImageResizeMode.None)]
		public ImageResizeMode ResizeMode
		{
			get
			{
				object val = ViewState["ResizeMode"];
				if (val != null)
				{
					return (ImageResizeMode)val;
				}
				return ImageResizeMode.None;
			}
			set
			{
				ViewState["ResizeMode"] = value;
			}
		}

		/// <summary>
		/// Specifies if the HTML image element's dimensions are inferred from image's binary data
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1BinaryImage.AutoSize")]
		[DefaultValue(true)]
		public bool AutoSize
		{
			get
			{
				object val = ViewState["AutoSize"];
				if (val != null)
				{
					return (bool)val;
				}
				return true;
			}
			set
			{
				ViewState["AutoSize"] = value;
			}
		}

		/// <summary>
		/// Gets or sets the alignment of the BinaryImage control in relation to other elements on the Web page
		/// </summary>
		[C1Category("Category.Layout")]
		[C1Description("C1BinaryImage.ImageAlign")]
		[DefaultValue(ImageAlign.NotSet)]
		public ImageAlign ImageAlign
		{
			get
			{
				object val = ViewState["ImageAlign"];
				if (val != null)
				{
					return (ImageAlign)val;
				}
				return ImageAlign.NotSet;
			}
			set
			{
				ViewState["ImageAlign"] = value;
			}
		}

		/// <summary>
		/// Specifies the URL of the HTTPHandler from which the image will be served.
		/// </summary>
		[C1Category("Category.Misc")]
		[C1Description("C1BinaryImage.HttpHandlerUrl")]
		[DefaultValue(DefaultHttpHandlerUrl)]
		public string HttpHandlerUrl
		{
			get
			{
				object val = ViewState["HttpHandlerUrl"];
				if (val != null)
				{
					return (string)val;
				}
				return DefaultHttpHandlerUrl;
			}
			set
			{
				ViewState["HttpHandlerUrl"] = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the image data will be persisted if the control is invisible.
		/// </summary>
		[C1Category("Category.Behavior")]
		[C1Description("C1BinaryImage.PersistDataIfNotVisible")]
		[DefaultValue(false)]
		public bool PersistDataIfNotVisible
		{
			get
			{
				object val = ViewState["PersistDataIfNotVisible"];
				if (val != null)
				{
					return (bool)val;
				}
				return false;
			}
			set
			{
				ViewState["PersistDataIfNotVisible"] = value;
			}
		}
		#endregion

		#region ** override properties
		/// <summary>
		/// Gets the System.Web.UI.HtmlTextWriterTag value that corresponds to this Web
		/// server control. This property is used primarily by control developers. 
		/// </summary>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Img;
			}
		}

		/// <summary>
		/// No need registers the WidgetDescriptor objects for the control.
		/// </summary>
		/// <returns></returns>
		public override IEnumerable<ScriptDescriptor> GetScriptDescriptors()
		{
			return new List<ScriptDescriptor>();
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public override string Theme
		{
			get
			{
				return string.Empty;
			}
			set { }
		}


		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public override string ThemeSwatch
		{
			get
			{
				return string.Empty;
			}
			set { }
		}

		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public override bool Enabled { get; set; }
		#endregion

		#region ** constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="C1BinaryImage"/> class.
		/// </summary>
		public C1BinaryImage()
		{
			VerifyLicense();
		}

		internal virtual void VerifyLicense()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1BinaryImage), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
			_productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
		}
		#endregion

		#region ** methods
		private string ProcessImageData()
		{
			var imageData = C1ImageDataProcesser.ProcessImageData(ImageData, SavedImageName, ResizeMode, CropPosition, Width, Height);

			if (imageData == null)
			{
				return "";
			}

			if (AutoSize)
			{
				var image = C1ImageDataProcesser.GetImageByData(imageData.Data);
				Width = image.Width;
				Height = image.Height;
			}

			return C1BinaryImageCacheStorage.SaveImage(imageData);
		}

		/// <summary>
		/// Handles the <see cref="E:System.Web.UI.Control.Load"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains event data.</param>
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if (ImageData != null && PersistDataIfNotVisible)
			{
				imgkey = ProcessImageData();
				if (!string.IsNullOrEmpty(imgkey))
				{
					ImageUrl = ResolveClientUrl(HttpHandlerUrl + "?imgkey=" + imgkey);
				}
			}
		}

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
		}

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">
		/// An <see cref="T:System.EventArgs"/> object that contains the event data.
		/// </param>
		protected override void OnInit(EventArgs e)
		{
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			writer.AddAttribute(HtmlTextWriterAttribute.Alt, AlternateText);

			if (string.IsNullOrEmpty(imgkey))
			{
				imgkey = ProcessImageData();
			}

			if (!string.IsNullOrEmpty(imgkey))
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Src, ResolveClientUrl(HttpHandlerUrl + "?imgkey=" + imgkey));
			}
			else if (!string.IsNullOrEmpty(ImageUrl))
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Src, ResolveClientUrl(ImageUrl));
			}
			else
			{
				var imgUrl = Page.ClientScript.GetWebResourceUrl(typeof(C1BinaryImage), IsDesignMode ? "C1.Web.Wijmo.Controls.C1BinaryImage.Images.C1BinaryImage_Desgin.png" : "C1.Web.Wijmo.Controls.C1BinaryImage.Images.C1BinaryImage_Error.png");
				writer.AddAttribute(HtmlTextWriterAttribute.Src, imgUrl);
			}

			if (!string.IsNullOrEmpty(DescriptionUrl))
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Longdesc, base.ResolveClientUrl(DescriptionUrl));
			}

			AddAlignAttribute(writer);
			base.AddAttributesToRender(writer);
		}

		private void AddAlignAttribute(HtmlTextWriter writer)
		{
			if (ImageAlign != ImageAlign.NotSet)
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Align, this.ImageAlign.ToString().ToLower());
			}
		}
		#endregion
	}
}
