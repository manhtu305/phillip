﻿using System;
using System.Web;
using System.Web.Caching;

namespace C1.Web.Wijmo.Controls.C1BinaryImage
{
	/// <summary>
	/// image cache storage management class.
	/// </summary>
	internal static class C1BinaryImageCacheStorage
	{
		private static HttpContext CurrentContext
		{
			get
			{
				return HttpContext.Current;
			}
		}

		/// <summary>
		/// Save the <see cref="C1BinaryImageData"/> to image cache storage and return a imageKey.
		/// </summary>
		/// <param name="imageData">The specified <see cref="C1BinaryImageData"/> </param>
		/// <returns>Returns a imageKey which used to find the <see cref="C1BinaryImageData"/> from image cache storage.</returns>
		public static string SaveImage(C1BinaryImageData imageData)
		{
			var imageKey = Guid.NewGuid().ToString().Replace("-", "");

			if ((CurrentContext != null) && (CurrentContext.Cache != null))
			{
				CurrentContext.Cache.Add(imageKey, imageData, null, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(5.0), CacheItemPriority.Normal, null);
			}

			return imageKey;
		}

		/// <summary>
		/// Load the <see cref="C1BinaryImageData"/> from image cache storage by imageKey.
		/// </summary>
		/// <param name="imageKey">The specified imageKey </param>
		/// <returns>Returns a <see cref="C1BinaryImageData"/> from image cache storage by imageKey.</returns>
		public static C1BinaryImageData LoadImage(string imageKey)
		{
			if ((CurrentContext != null) && (CurrentContext.Cache != null))
			{
				return (C1BinaryImageData)CurrentContext.Cache.Get(imageKey);
			}
			return null;
		}
	}
}
