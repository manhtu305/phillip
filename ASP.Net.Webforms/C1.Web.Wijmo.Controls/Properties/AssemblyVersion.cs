using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Web.UI;

namespace C1.Wijmo.Licensing
{
    internal static class VersionConst
    {
        const string ReleaseNumber = "20202";
        const string BuildNumber = "332";
#if ASP_NET45
        internal const string VerString = "4.5." + ReleaseNumber + "." + BuildNumber;
#elif ASP_NET4
        internal const string VerString = "4.0." + ReleaseNumber + "." + BuildNumber;
#elif ASP_NET35
        internal const string VerString = "3.5." + ReleaseNumber + "." + BuildNumber;
#else
        internal const string VerString = "2.0." + ReleaseNumber + "." + BuildNumber;
#endif
        internal const string WijmoVer = "3.20202.160";
        internal const string jQueryVer = "1.11.1";
        internal const string jQueryUiVer = "1.11.0";
        internal const string jQueryVerWithJQM = "1.11.1";
        internal const string jQueryMobileVer = "1.4.0";
        internal const string bootstrapVer = "3.2.0";
        internal const string DesignerVer = ", Version="+VerString+", Culture=neutral, PublicKeyToken=9b75583953471eea";
    }
}
