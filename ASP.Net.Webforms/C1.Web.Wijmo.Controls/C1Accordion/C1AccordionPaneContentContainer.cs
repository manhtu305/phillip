﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web;
using System.ComponentModel;

namespace C1.Web.Wijmo.Controls.C1Accordion
{
	/// <summary>
	/// Holds the content for the panes in the C1Accordion control.
	/// </summary>
	[ToolboxItem(false)]
	public class C1AccordionPaneContentContainer : Panel, INamingContainer
	{

		#region ** fields

		internal string _literalContent = "";
		private C1AccordionPane _owner;
		private System.Web.UI.AttributeCollection _contentElementAttributes;
		private bool IsDesignMode = HttpContext.Current == null;

		#endregion		

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1AccordionPaneHeaderContainer"/> class.
		/// </summary>
		/// <param name="owner">The owner accordion pane.</param>
		public C1AccordionPaneContentContainer(C1AccordionPane owner)
			: base()
		{
			this._owner = owner;
		}

		#endregion

		#region ** properties

		/// <summary>
		/// Gets a collection of all attribute name and value pairs 
		/// expressed on the content element tag within the ASP.NET page.
		/// </summary>
		/// <value>The content element attributes.</value>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Web.UI.AttributeCollection ContentElementAttributes
		{
			get
			{
				if (_contentElementAttributes == null)
					_contentElementAttributes = new System.Web.UI.AttributeCollection(new StateBag());
				return _contentElementAttributes;
			}
		}

		#endregion

		#region ** methods

		#region ** render methods

		/// <summary>
		/// Renders the HTML opening tag of the control to the specified writer. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		public override void RenderBeginTag(HtmlTextWriter writer)
		{
			string cssClass = "";
			if (!_owner.Enabled)
			{
				//writer.AddAttribute(HtmlTextWriterAttribute.Disabled, "true");
				cssClass += "ui-state-disabled ";
			}
			if (!string.IsNullOrEmpty(_owner.CssClass))
			{
				cssClass += _owner.CssClass + " ";
			}
			if (IsDesignMode)
			{
				cssClass += "wijmo-wijaccordion-content-active ui-corner-bottom wijmo-wijaccordion-content ui-helper-reset ui-widget-content ";
				this.Attributes["role"] = "tabpanel";
				this.ContentElementAttributes.AddAttributes(writer);
			}
			if (!string.IsNullOrEmpty(cssClass))
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);
			}
			if (!string.IsNullOrEmpty(_owner.ToolTip))
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Title, _owner.ToolTip);
			}
			base.RenderBeginTag(writer);
		}

		/// <summary>
		/// Renders the HTML closing tag of the control into the specified writer. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		public override void RenderEndTag(HtmlTextWriter writer)
		{
			base.RenderEndTag(writer);
		}
		private static Page _pageFix;
		/// <summary>
		/// Gets a reference to the <see cref="T:System.Web.UI.Page"/> instance that contains the server control.
		/// </summary>
		/// <value></value>
		/// <returns>
		/// The <see cref="T:System.Web.UI.Page"/> instance that contains the server control.
		/// </returns>
		/// <exception cref="T:System.InvalidOperationException">
		/// The control is a <see cref="T:System.Web.UI.WebControls.Substitution"/> control.
		/// </exception>
		public override Page Page
		{
			get
			{
				// fix for [28908] C1Accordion- Designer Issue with UpdatePanel in Pane
				if (base.Page != null)
				{
					_pageFix = base.Page;
				}
				else
				{
					if (this._owner != null && this._owner.Page != null)
					{
						_pageFix = this._owner.Page;
					}
					if (_pageFix != null)
					{
						return _pageFix;
					}
				}
				//-
				return base.Page;
			}
			set
			{
				base.Page = value;
			}
		}
		/// <summary>
		/// Renders the contents of the control to the specified writer. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
		{
			base.RenderContents(writer);
			if (!string.IsNullOrEmpty(_literalContent))
			{
				writer.Write(_literalContent);
			}
		}

		#endregion

		#endregion

	}
}
