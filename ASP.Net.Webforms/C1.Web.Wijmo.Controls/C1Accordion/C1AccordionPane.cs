﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.ComponentModel;
using System.Web.UI.WebControls;
using C1.Web.Wijmo.Controls.Localization;
using System.Web;
using System.ComponentModel.Design;
using System.Web.UI.Design;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1Accordion
{
	/// <summary>
	/// The AccordionPane control is an expandable pane containing a header 
	/// or title for information to be displayed in its accordion or viewing area.
	/// </summary>
	[ToolboxItem(false)]
	public class C1AccordionPane : WebControl, INamingContainer
	{
		#region ** fields
		
		internal C1Accordion _accordion;
		private C1AccordionPaneHeaderContainer _headerPanelControl;
		private C1AccordionPaneContentContainer _contentPanelControl;

		private ITemplate HeaderTemplate;
		private ITemplate ContentTemplate;
		private bool IsDesignMode = HttpContext.Current == null;
		//private string _headerText;

		#endregion

		#region ** constructors

		/// <summary>
		/// Initializes a new instance of the AccordionPane class.
		/// </summary>
		public C1AccordionPane()
			: base()
		{
		}

		/// <summary>
		/// Initializes a new instance of the AccordionPane class. 
		/// This constructor is primarily used by controls developers. 
		/// Use this constructor if you need to reference the parent Accordion control 
		/// inside overridden methods when the pane is not yet added to the Accordion's 
		/// Panes collection.
		/// </summary>
		/// <remarks>
		/// Note, this constructor will not add newly created AccordionPane into Accordion's Panes collection automatically.
		/// So, developer need to do it manually.
		/// </remarks>
		/// <param name="accordion">Parent <see cref="C1Accordion"/> instance.</param>
		public C1AccordionPane(C1Accordion accordion)
			: base()
		{
			_accordion = accordion;
		}

		/// <summary>
		/// Initializes a new instance of the AccordionPane class.
		/// </summary>
		/// <param name="headerText">Text that will be rendered into header panel.</param>
		public C1AccordionPane(string headerText)
			: base(headerText)
		{
		}

		#endregion

		#region ** hidden properties

		#region hide inline css style properties:BackColor,BorderColor,BorderStyle,BorderWidth,ForeColor,Font

		/// <summary>
		/// This property is hidden
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color BackColor
		{
			get
			{
				return base.BackColor;
			}
			set
			{
				base.BackColor = value;
			}
		}
		/// <summary>
		/// This property is hidden
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color BorderColor
		{
			get
			{
				return base.BorderColor;
			}
			set
			{
				base.BorderColor = value;
			}
		}
		/// <summary>
		/// This property is hidden
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override BorderStyle BorderStyle
		{
			get
			{
				return base.BorderStyle;
			}
			set
			{
				base.BorderStyle = value;
			}
		}
		/// <summary>
		/// This property is hidden
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override Unit BorderWidth
		{
			get
			{
				return base.BorderWidth;
			}
			set
			{
				base.BorderWidth = value;
			}
		}
		/// <summary>
		/// This property is hidden
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override System.Drawing.Color ForeColor
		{
			get
			{
				return base.ForeColor;
			}
			set
			{
				base.ForeColor = value;
			}
		}
		/// <summary>
		/// This property is hidden
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public override FontInfo Font
		{
			get
			{
				return base.Font;
			}
		}
		#endregion

		#endregion


		#region ** properties

		/// <summary>
		/// Gets or sets the header template used to display the content of the control's header.
		/// </summary>
		[C1Description("AccordionPane.Header")]
		[Browsable(false)]
		[DefaultValue(null)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateContainer(typeof(C1AccordionPaneHeaderContainer))]
		[TemplateInstance(TemplateInstance.Single)]
		[RefreshProperties(RefreshProperties.All)]
		[Layout(LayoutType.Misc)]
		public ITemplate Header
		{
			get { return HeaderTemplate; }
			set { HeaderTemplate = value; }
		}

		/// <summary>
		/// Gets or sets the content of a ContentControl.
		/// </summary>
		[C1Description("AccordionPane.Content")]
		[Browsable(false)]
		[DefaultValue(null)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[TemplateContainer(typeof(C1AccordionPaneContentContainer))]
		[TemplateInstance(TemplateInstance.Single)]
		[RefreshProperties(RefreshProperties.All)]
		[Layout(LayoutType.Misc)]
		public ITemplate Content
		{
			get { return ContentTemplate; }
			set { ContentTemplate = value; }
		}

		/// <summary>
		/// Gets the control that is used as the header panel.
		/// </summary>
		[C1Description("AccordionPane.HeaderPanel")]
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public C1AccordionPaneHeaderContainer HeaderPanel
		{
			get
			{
				if (_headerPanelControl == null)
				{
					_headerPanelControl = new C1AccordionPaneHeaderContainer(this);
					_headerPanelControl._literalContent = _headerDataboundContent;
					_headerPanelControl.ID = "hdr";
					if (this.IsDesignMode)
					{
						// fix for designer regions overflow
						// TFS issues: 1986
						//[C1Expander] Header and Content regions is not correctly drawn C1Expander when 'ExpandDirection' is set to 'Right'/'Left'
						_headerPanelControl.Attributes.Add("style", "overflow:hidden;margin:0;padding:0;");
					}
				}
				return _headerPanelControl;
			}
		}



		/// <summary>
		/// Gets the control that is used as the content panel.
		/// </summary>
		[C1Description("AccordionPane.ContentPanel")]
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public C1AccordionPaneContentContainer ContentPanel
		{
			get
			{
				if (_contentPanelControl == null)
				{
					_contentPanelControl = new C1AccordionPaneContentContainer(this);
					_contentPanelControl._literalContent = _contentDataboundContent;
					_contentPanelControl.ID = "cnt";
					if (this.IsDesignMode)
					{
						// fix for designer regions overflow, 
						// TFS issues: 1986
						//[C1Expander] Header and Content regions is not correctly drawn C1Expander when 'ExpandDirection' is set to 'Right'/'Left'
						_contentPanelControl.Attributes.Add("style", "overflow:auto;margin:0;padding:0;");
					}
				}
				return _contentPanelControl;
			}
		}



		/// <summary>
		/// Gets or sets a value that indicates whether a server control is rendered as UI on the page.
		/// </summary>
		/// <returns>True if the control is visible on the page; otherwise False.</returns>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		//[Obsolete("This property is obsolete, use DisplayVisible property instead.")]
		public override bool Visible
		{
			get
			{
				// Return base.Visible here since pane is not removed from control.
				return base.Visible; // this.DisplayVisible;//
			}
			set
			{
				/*
				this.DisplayVisible = value;
				if (_accordion != null)
					_accordion._visiblePanesVersion = 0;
				*/

				/*
				if (!value && this.Expanded)
				{
					this.Expanded = false;
				}
				if (_accordion != null)
				{
					// clear version number in order to update visible panes list.
					_accordion._visiblePanesVersion = 0;
				}
				*/
			}
		}


		#region ** overriden and hidden

		#endregion
			   

		/// <summary>
		/// Gets or sets a value that determines whether the panel is in an expanded state.
		/// </summary>
		[C1Description("Accordion.Expanded")]
		[DefaultValue(false)]
		public virtual bool Expanded
		{
			get
			{
				return (bool)(ViewState["Expanded"] ?? false);
			}
			set
			{
				if (this.Expanded != value)
				{
					ViewState["Expanded"] = value;
					// When pane expanded change selected index only at design time:
					// because at runtime AccordionPane can restore expanded state on post back, 
					// Accordion takes care about SelectedIndex value at runtime.
					if (IsDesignMode && value && this._accordion != null && this._accordion.RequireOpenedPane)
					{
						this._accordion.SelectedIndex = this._accordion.Panes.IndexOf(this);
					}
				}
			}
		}


		
		#endregion

		#region ** methods

		#region ** Render, RenderChildren, RenderContents...

		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			EnsureChildControls();
		}

		/// <summary>
		/// Determines whether the server control contains child controls. If it does not, it creates child controls.
		/// </summary>
		protected override void EnsureChildControls()
		{
			base.EnsureChildControls();
		}

		/// <summary>
		/// Gets a <see cref="T:System.Web.UI.ControlCollection"/> object that represents the child controls for a specified server control in the UI hierarchy.
		/// </summary>
		/// <value></value>
		/// <returns>
		/// The collection of child controls for the specified server control.
		/// </returns>
		public override ControlCollection Controls
		{
			get
			{
				EnsureChildControls();
				return base.Controls;
			}
		}


		/// <summary>
		/// Called by the ASP.NET page framework to notify server controls that use composition-based implementation to create any child controls they contain in preparation for posting back or rendering.
		/// </summary>
		protected override void CreateChildControls()
		{

			// Create the controls
			Controls.Clear();
			//HeaderPanel.Controls.Clear();//not needed?
			//ContentPanel.Controls.Clear();//not needed?
			/*
			if (!string.IsNullOrEmpty(this._headerText))
			{
				LiteralControl literal = new LiteralControl(this._headerText);
				HeaderPanel.Controls.Add(literal);
			}
			*/
			// Load the templates into the controls
			if (HeaderTemplate != null)
				HeaderTemplate.InstantiateIn(HeaderPanel);
			else if (this.IsDesignMode)
				CreateDefaultHeaderTemplateForDesign();

			//if (string.IsNullOrEmpty(this.ContentUrl)) //ABC191{
			if (ContentTemplate != null)
				ContentTemplate.InstantiateIn(ContentPanel);
			else if (this.IsDesignMode)
				CreateDefaultContentTemplateForDesign();
			//}
			// Render without tables at runtime 
			// and for vertical orientation at design time:
			Controls.Add(HeaderPanel);
			if (!this.Expanded && IsDesignMode)
			{
                // Add collapsed content panel at design time but set it as invisible.
                // For case 42246.
                ContentPanel.Visible = false;
                Controls.Add(ContentPanel);
			}
			else
			{
				Controls.Add(ContentPanel);
			}
			this.EnsureIDInternal();
		}

		private void EnsureIDInternal()
		{

			if (string.IsNullOrEmpty(this.ID))
			{
				this.EnsureID();
				if (string.IsNullOrEmpty(this.ID) && _accordion != null)
				{

					this.ID = "DynamicC1AccordionPane" + _accordion.Panes.IndexOf(this);
				}
			}
		}

		private void CreateDefaultContentTemplateForDesign()
		{
			ISite site = GetSite(this);
			if (site != null)
			{
				IDesignerHost host = (IDesignerHost)site.GetService(typeof(IDesignerHost));
				if (host != null)
				{
					this.ContentTemplate = ControlParser.ParseTemplate(host, " ");
				}
			}
		}

		private void CreateDefaultHeaderTemplateForDesign()
		{
			ISite site = GetSite(this);
			if (site != null)
			{
				IDesignerHost host = (IDesignerHost)site.GetService(typeof(IDesignerHost));
				if (host != null)
				{
					this.HeaderTemplate = ControlParser.ParseTemplate(host, " ");
				}
			}
		}

		/// <summary>
		/// AddAttributesToRender override.
		/// </summary>
		/// <param name="writer"></param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			base.EnsureID();

			Unit originalWidth = this.Width;
			Unit originalHeight = this.Height;

			writer.AddStyleAttribute(HtmlTextWriterStyle.Overflow, "visible");
			if (!this.DesignMode && !this.Visible)//DisplayVisible
				writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");


			//this.Width = DetermineControlWidth();
			//this.Height = DetermineControlHeight();
			base.AddAttributesToRender(writer);

			this.Width = originalWidth;
			this.Height = originalHeight;

		}



		/// <summary>
		/// Raises the <see cref="E:System.Web.UI.Control.PreRender"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			//C1.Web.UI.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
			// fix for 
			// 6023 [C1NavPanel] Control inside disabled pane is working at Run time when this pane is set as SlectedIndex
			this.HeaderPanel.Enabled = this.Enabled;
			this.ContentPanel.Enabled = this.Enabled;
			// --
		}

		private void EnsureEnabledState()
		{
			// check to see if any of our parents are disabled
			if (!this.DesignMode)
			{
				Control ctl = this.Parent;
				bool enabled = this.Enabled;
				while (ctl != null)
				{
					if (ctl is WebControl && !((WebControl)ctl).Enabled)
					{
						this.Enabled = false;
						return;
					}
					ctl = ctl.Parent;
				}
			}
		}

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			this.EnsureChildControls();
			if (this.Expanded == false)
			{
				if (IsDesignMode)
				{
					ContentPanel.Style.Add(HtmlTextWriterStyle.Display, "none");
					ContentPanel.Style.Add(HtmlTextWriterStyle.Height, "0");
				}				
			}

			EnsureEnabledState();

			// Do not render exact width/height to parent container
			string prevCssClass = this.CssClass;
			//string prevFlowPanelCssClass = this.FlowPanelControl.CssClass;
			string prevHeaderCssClass = this.HeaderPanel.CssClass;
			string prevContentCssClass = this.ContentPanel.CssClass;
			Unit prevHeaderW = this.HeaderPanel.Width;
			Unit prevHeaderH = this.HeaderPanel.Height;
			Unit prevContentW = this.ContentPanel.Width;
			Unit prevContentH = this.ContentPanel.Height;
			if (this.DetermineOrientation() == Orientation.Vertical)
			{


				// Set Header height to specified value:
				double headerHeight = DetermineHeaderHeightFromCssInPx();
				/*if (!this.HeaderSize.IsEmpty)
				{
					headerHeight = this.HeaderSize.Value;
					this.HeaderPanel.Height = Unit.Pixel((int)headerHeight);
				}*/
				// Set Content height to 100%:
				if (!this.Height.IsEmpty)
				{
					double heightValue = this.Height.Value;
					double contentHeight = heightValue - headerHeight;
					if (contentHeight > 0)
					{
						this.ContentPanel.Height = Unit.Pixel((int)contentHeight);
					}
				}
			}
			else
			{
				// Set Header/Content height to 100%:
				if (!this.Height.IsEmpty)
				{
					this.HeaderPanel.Height = this.Height;
					this.ContentPanel.Height = this.Height;
				}


			}
			base.Render(writer);

			this.CssClass = prevCssClass;
			this.HeaderPanel.CssClass = prevHeaderCssClass;
			this.ContentPanel.CssClass = prevContentCssClass;
			this.HeaderPanel.Width = prevHeaderW;
			this.HeaderPanel.Height = prevHeaderH;
			this.ContentPanel.Width = prevContentW;
			this.ContentPanel.Height = prevContentH;
		}

		/// <summary>
		/// Renders the HTML opening tag of the control to the specified writer. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		public override void RenderBeginTag(HtmlTextWriter writer)
		{			
			// do not render outer container
			//base.RenderBeginTag(writer);
		}

		/// <summary>
		/// Renders the HTML closing tag of the control into the specified writer. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		public override void RenderEndTag(HtmlTextWriter writer)
		{			
			// do not render outer container
			//base.RenderEndTag(writer);
		}


		/// <summary>
		/// RenderChildren override.
		/// </summary>
		/// <param name="writer"></param>
		protected override void RenderChildren(HtmlTextWriter writer)
		{
			EnsureChildControls();
			if (this.IsDesignMode)
			{
				if (this._accordion.ExpandDirection == ExpandDirection.Top || this._accordion.ExpandDirection == ExpandDirection.Bottom)
				{
					writer.RenderBeginTag(HtmlTextWriterTag.Tr);
				}
				writer.AddAttribute(HtmlTextWriterAttribute.Valign, "top");
				//writer.AddAttribute(HtmlTextWriterAttribute.Border, "15");writer.AddStyleAttribute("border", "5px solid blue");
				writer.RenderBeginTag(HtmlTextWriterTag.Td);
			}
			else
			{

			}

			if (IsDesignMode && (_accordion.ExpandDirection == ExpandDirection.Left
				|| _accordion.ExpandDirection == ExpandDirection.Top))
			{
				ContentPanel.RenderControl(writer);
			}
			else
			{
				HeaderPanel.RenderControl(writer);
			}
			if (this.IsDesignMode)
			{
				writer.RenderEndTag(); //</td
				if (this._accordion.ExpandDirection == ExpandDirection.Top || this._accordion.ExpandDirection == ExpandDirection.Bottom)
				{
					writer.RenderEndTag(); //</tr
					writer.RenderBeginTag(HtmlTextWriterTag.Tr);
				}
				writer.AddAttribute(HtmlTextWriterAttribute.Valign, "top");
				writer.RenderBeginTag(HtmlTextWriterTag.Td);
			}

			if (IsDesignMode && (_accordion.ExpandDirection == ExpandDirection.Left
|| _accordion.ExpandDirection == ExpandDirection.Top))
			{
				HeaderPanel.RenderControl(writer);
			}
			else
			{
				ContentPanel.RenderControl(writer);
			}
			if (this.IsDesignMode)
			{
				writer.RenderEndTag(); //</td				
				if (this._accordion.ExpandDirection == ExpandDirection.Top || this._accordion.ExpandDirection == ExpandDirection.Bottom)
				{
					writer.RenderEndTag(); //</tr
				}
			}
		}
		
		#endregion

		/// <summary>
		/// Gets parent accordion orientation.
		/// </summary>
		/// <returns></returns>
		protected Orientation DetermineOrientation()
		{
			if (this._accordion.ExpandDirection == ExpandDirection.Top
				|| this._accordion.ExpandDirection == ExpandDirection.Bottom)
			{
				return Orientation.Vertical;
			}
			else
			{
				return Orientation.Horizontal;
			}
		}

		#endregion

		#region internal and private code

		internal void SetParent(C1Accordion Owner)
		{
			_accordion = Owner;
		}
		
		internal virtual double DetermineHeaderHeightFromCssInPx()
		{
			// Assume that border box is 2,2

			string visualStyle = "ArcticFox";
			if (_accordion != null)
				visualStyle = this._accordion.Theme;
			switch (visualStyle)
			{
				case "ArcticFox":
					return 36; // 38 - 2 = 36
				case "Office2007Black":
					return 31; // 33 - 2 = 31
				case "Office2007Blue":
					return 31;
				case "Office2007Silver":
					return 31;
				case "Vista":
					return 24; // 26 - 2 = 24
				default:
					break;
			}
			return 24;
		}

		#endregion


		private string _headerDataboundContent;
		private string _contentDataboundContent;

		internal void SetHeaderPanelContent(string headerContent)
		{
			ControlCollection col = this.Controls;
			_headerDataboundContent = headerContent;
			if (_headerPanelControl != null)
				_headerPanelControl._literalContent = _headerDataboundContent;
		}

		internal void SetContentPanelContent(string contentContent)
		{
			ControlCollection col = this.Controls;
			_contentDataboundContent = contentContent;
			if (_contentPanelControl != null)
				_contentPanelControl._literalContent = _contentDataboundContent;

		}

		private string GenerateValueForIDProperty()
		{
			return "Pane" + DateTime.Now.Ticks;
		}

		private static ISite GetSite(System.Web.UI.Control webControl)
		{
			if (webControl.Site != null) return webControl.Site;
			for (System.Web.UI.Control control = webControl.Parent; control != null; control = control.Parent)
			{
				if (control.Site != null) return control.Site;
			}
			return null;
		}
	}
}
