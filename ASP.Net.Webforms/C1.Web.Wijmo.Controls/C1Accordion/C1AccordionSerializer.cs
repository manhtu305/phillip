﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base;
using System.Collections;
using System.Web.UI;
using System.Web;
using System.Globalization;

namespace C1.Web.Wijmo.Controls.C1Accordion
{
	/// <summary>
	/// Serializes the Accordion Control.
	/// </summary>
	public class C1AccordionSerializer : C1BaseSerializer<C1Accordion, C1AccordionPane, C1Accordion>
	{

		#region ** fields

		private Hashtable _usedIds = new Hashtable();

		#endregion

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the AccordionSerializer class.
		/// </summary>
		public C1AccordionSerializer(Object obj)
			: base(obj)
		{
		}

		#endregion

		#region ** protected override

		/// <summary>
		/// Provides a custom implementation to add deserialized objects to the items collection.
		/// </summary>        
		/// <remarks>This method is primarily used by control developers.</remarks>
		/// <param name="collection">Target collection to add items.</param>
		/// <param name="item">Item to be added to the collection.</param>
		/// <returns>Returns True if the "item" object is added to the target collection. Otherwise, False.</returns>
		protected override bool OnAddItem(object collection, object item)
		{
			if (collection is C1AccordionPaneCollection)
			{
				try
				{
					if (item != null && item is C1AccordionPane)
					{
						if (this.SerializableObject != null && this.SerializableObject is C1Accordion)
						{
							if (string.IsNullOrEmpty(((C1AccordionPane)item).ID))
							{
								((C1AccordionPane)item).ID = GetNextUniqueAccordionPaneName((C1Accordion)this.SerializableObject, _usedIds);
							}
							else
							{
								ResolveIDConflictsIfAny((C1AccordionPane)item, (C1Accordion)this.SerializableObject);
							}
						}
						((C1AccordionPaneCollection)collection).Add((C1AccordionPane)item);
						return true;
					}
					else
					{
						if (item != null)
						{
							//   throw new HttpException("item is not AccordionPane?? " + item.GetType());
						}
						else
						{
							//throw new HttpException("item is NULL. strange...");
						}
						return true;
					}
				}
				catch (Exception ex)
				{
					throw new HttpException("OnAddItem error:" + ex.Message + "," + ex.StackTrace);
				}
			}
			return base.OnAddItem(collection, item);
		}

		/// <summary>
		/// Removes all the items from the <see cref="C1AccordionPaneCollection"/>.
		/// </summary>
		/// <param name="collection">Target collection to remove items from.</param>
        /// <returns>Returns True if all the items are successfully removed from the collection. Otherwise, False.</returns>
		protected override bool OnClearItems(object collection)
		{
			if (collection is C1AccordionPaneCollection)
			{
				((C1AccordionPaneCollection)collection).Clear();
				return true;
			}
			return base.OnClearItems(collection);
		}

		#endregion

		#region ** private implementation

		/// <summary>
		/// Gets a unique name for <see cref="C1AccordionPane"/>.                
		/// </summary>
		/// <returns>Returns a unique name like "Accordion1Pane2".</returns>
		public static string GetNextUniqueAccordionPaneName(C1Accordion parent)
		{
			return GetNextUniqueAccordionPaneName(parent, null);
		}

		private static string GetNextUniqueAccordionPaneName(C1Accordion parent, Hashtable usedIds)
		{
			if (usedIds == null)
				usedIds = new Hashtable();
			string baseName = parent.ID;
			if (string.IsNullOrEmpty(baseName))
			{
				baseName = parent.GetType().Name;
			}
			//Look within Control:
			int count = 1;
			string testId = baseName + "Pane" + count.ToString(CultureInfo.InvariantCulture);
			C1AccordionPane pane = FindAccordionPane(testId, parent);
			while (pane != null || usedIds.Contains(testId))
			{
				testId = baseName + "Pane" + count.ToString(CultureInfo.InvariantCulture);
				pane = FindAccordionPane(baseName + "Pane" + count.ToString(CultureInfo.InvariantCulture), parent);
				count++;
			}
			// Look in Page:
			if (parent.Page != null)
			{
				Control c = parent.Page.FindControl(testId);
				int k = 1;
				while (c != null || usedIds.Contains(testId))
				{
					testId = baseName + "Pane" + count.ToString(CultureInfo.InvariantCulture) + "_" + k;
					c = parent.Page.FindControl(testId);
					k++;
				}
			}
			usedIds[testId] = true;
			return testId;
		}

		private static C1AccordionPane FindAccordionPane(string id, C1Accordion Accordion)
		{
			C1AccordionPane activePanel = null;
			for (int i = 0; i < Accordion.Panes.Count; i++)
			{
				C1AccordionPane pane = Accordion.Panes[i];
				if (pane.ID == id)
					activePanel = pane;
			}
			if (activePanel == null)
				activePanel = (C1AccordionPane)Accordion.FindControl(id);
			return activePanel;
		}

		private void ResolveIDConflictsIfAny(C1AccordionPane pane, C1Accordion parent)
		{
			string testId = pane.ID;
			Control testPane = parent.Page != null ? FindControlRecursive(parent.Page, testId) : null;
			if (testPane != null)
			{
				// Control with same ID already exists, so generate new ID:
				pane.ID = GetNextUniqueAccordionPaneName(parent, _usedIds);
				_regenerateAllId = true;
			}
			else if (_usedIds.Contains(testId) || _regenerateAllId)
			{
				pane.ID = GetNextUniqueAccordionPaneName(parent, _usedIds);
				_regenerateAllId = true;
			}
		}

		private bool _regenerateAllId;

		/// <summary>
		/// Finds a Control recursively. Note finds the first match and exists
		/// </summary>
		/// <param name="owner">The owner.</param>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		private static Control FindControlRecursive(Control owner, string id)
		{
			if (owner.ID == id)
				return owner;
			foreach (Control curControl in owner.Controls)
			{
				Control foundControl = FindControlRecursive(curControl, id);
				if (foundControl != null)
					return foundControl;
			}
			return null;
		}
		#endregion

	}
}
