﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using C1.Web.Wijmo.Controls.Base.Collections;
using System.Diagnostics;

namespace C1.Web.Wijmo.Controls.C1Accordion
{

	/// <summary>
	/// Represents a collection of AccordionPane objects that are used by the Panes property.
	/// </summary>
	public class C1AccordionPaneCollection :
	C1ObservableItemCollection<C1Accordion, C1AccordionPane>
	{

		internal ulong Version = 1;

		/// <summary>
		/// Initializes a new instance of the AccordionPaneCollection class.
		/// </summary>
		/// <param name="owner">Parent Accordion class instance.</param>
		public C1AccordionPaneCollection(C1Accordion owner)
			: base(owner)
		{
			Debug.Assert(owner != null, "AccordionPaneCollection - owner is null");
		}

		/// <summary>
		/// Removes all items from collection.
		/// </summary>
		public new void Clear()
		{
			Version++;
			base.ClearItems();

			this.SetDirty();
		}

		/// <summary>
		/// Adds a new pane item to the end of the list.
		/// </summary>
		/// <param name="child"></param>
		public new void Add(C1AccordionPane child)
		{
			if (child != null)
			{
				this.Insert(this.Count, child);
				this.SetDirty();
			}
		}

		/// <summary>
		/// Insert a <see cref="C1AccordionPane"/> item into a specific position in the collection.
		/// </summary>
		/// <param name="index">Item position. Value should be greater or equal to 0</param>
		/// <param name="child">The new <see cref="C1AccordionPane"/> item.</param>
		public new void Insert(int index, C1AccordionPane child)
		{
			Version++;
			child.SetParent(Owner);
			if (this.Count == 0)
				this.InsertItem(0, child);
			else
				this.InsertItem(index, child);

			this.SetDirty();
		}

		/// <summary>
		/// Removes the specified item from the list.
		/// </summary>
		/// <param name="child">The <see cref="C1AccordionPane"/> item to be removed.</param>
		public new void Remove(C1AccordionPane child)
		{
			RemoveAt(IndexOf(child));
		}

		/// <summary>
		/// Removes an item from the list using its index.
		/// </summary>
		/// <param name="index">The index of the <see cref="C1AccordionPane"/> item to be removed.</param>
		public new void RemoveAt(int index)
		{
			Version++;
			this.RemoveItem(index);
			this.SetDirty();
		}

		private void SetDirty()
		{
			if (Owner != null)
				Owner.SetDirty();
		}
	}
}
