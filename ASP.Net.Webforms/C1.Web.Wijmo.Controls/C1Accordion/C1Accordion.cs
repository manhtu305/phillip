﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Drawing;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Collections;
using System.IO;

namespace C1.Web.Wijmo.Controls.C1Accordion
{

	using C1.Web.Wijmo.Controls.Base;
	using C1.Web.Wijmo.Controls.Localization;
	using C1.Web.Wijmo;
	using System.Collections.Specialized;

	/// <summary>
	/// The Accordion is a web control that allows you to provide multiple panes 
	/// and display them one at a time. 
	/// </summary>
	[ParseChildren(true)]
	[ToolboxData("<{0}:C1Accordion runat=server></{0}:C1Accordion>")]
	[ToolboxBitmap(typeof(C1Accordion), "C1Accordion.png")]
#if ASP_NET45
    [Designer("C1.Web.Wijmo.Controls.Design.C1Accordion.C1AccordionDesigner, C1.Web.Wijmo.Controls.Design.45"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET4
    [Designer("C1.Web.Wijmo.Controls.Design.C1Accordion.C1AccordionDesigner, C1.Web.Wijmo.Controls.Design.4"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#elif ASP_NET35
	[Designer("C1.Web.Wijmo.Controls.Design.C1Accordion.C1AccordionDesigner, C1.Web.Wijmo.Controls.Design.3"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#else
    [Designer("C1.Web.Wijmo.Controls.Design.C1Accordion.C1AccordionDesigner, C1.Web.Wijmo.Controls.Design.2"+C1.Wijmo.Licensing.VersionConst.DesignerVer)]
#endif
    [LicenseProviderAttribute()]
	public partial class C1Accordion : C1TargetDataBoundControlBase, ICompositeControlDesignerAccessor,
		IPostBackEventHandler, IPostBackDataHandler
	{

		#region ** fields

		private C1AccordionPaneCollection _panes;		
		private List<C1AccordionPane> _visiblePanes;
		internal ulong _visiblePanesVersion = 0;
		internal bool _productLicensed = false;

		#endregion

		#region ** constructor
		private bool _shouldNag;
		/// <summary>
		/// Initializes a new instance of the <see cref="C1Accordion"/> class.
		/// </summary>
		public C1Accordion()
			: base()
		{
			var licinfo = C1.Util.Licensing.ProviderInfo.Validate(typeof(C1Accordion), this, false);
			_shouldNag = licinfo.ShouldNag;
#if GRAPECITY
            _productLicensed = licinfo.IsValid;
#else
			_productLicensed = licinfo.IsValid || licinfo.IsLocalHost;
#endif
	/*
	  2011/08/16
	  fix for
	  [15949] [C1Accordion]C1Accordion control UI cannot be seen in Design mode of the Web Form when drag and drop the control at first time
	  do not use preview control?
	  note, problem can be in licensing code, licensing box shown twice.
	 */
		}
		#endregion

		#region ** properties

		/// <summary>
		/// Gets or sets the access key that allows you to quickly navigate to the Web server control.
		/// </summary>
		/// <value></value>
		/// <returns>
		/// The access key for quick navigation to the Web server control. The default value is <see cref="F:System.String.Empty"/>, which indicates that this property is not set.
		/// </returns>
		/// <exception cref="T:System.ArgumentOutOfRangeException">
		/// The specified access key is neither null, <see cref="F:System.String.Empty"/> nor a single character string.
		/// </exception>
		[Layout(LayoutType.Accessibility)]
		public override string AccessKey
		{
			get
			{
				return base.AccessKey;
			}
			set
			{
				base.AccessKey = value;
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether the Web server control is enabled.
		/// </summary>
		/// <value></value>
		/// <returns>true if control is enabled; otherwise, false. The default is true.
		/// </returns>
		[Layout(LayoutType.Behavior)]
		public override bool Enabled
		{
			get
			{
				return base.Enabled;
			}
			set
			{
				base.Enabled = value;
			}
		}

		/// <summary>
		/// Passes a reference to the control during postback. It will pass an empty string if the AutoPostBack property is false.
		/// </summary>
		[WidgetOption()]
		[DefaultValue("")]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public string PostBackEventReference
		{
			get
			{
				if (!IsDesignMode && (AutoPostBack || SelectedIndexChanged != null))
				{
					return this.Page.ClientScript.GetPostBackEventReference(this, "selectedIndex={0}");
				}
				return "";
			}
		}
		/// <summary>
		/// Gets or sets the ID of the control from which the data-bound control retrieves its list of data items.
		/// </summary>
		/// <returns>
		/// The ID of a control that represents the data source from which the data-bound control retrieves its data.
		/// </returns>
		[Layout(LayoutType.Data)]
		[C1Category("Category.Data")]
		public override string DataSourceID
		{
			get
			{
				return base.DataSourceID;
			}
			set
			{
				base.DataSourceID = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of the list of data that the data-bound control binds to, in cases where the data source contains more than one distinct list of data items.
		/// </summary>
		/// <returns>
		/// The name of the specific list of data that the data-bound control binds to, if more than one list is supplied by a data source control. The default value is <see cref="F:System.String.Empty"/>.
		/// </returns>
		[Layout(LayoutType.Data)]
		[C1Category("Category.Data")]
		public override string DataMember
		{
			get
			{
				return base.DataMember;
			}
			set
			{
				base.DataMember = value;
			}
		}

		/// <summary>
		/// Gets or sets the name of the field from the data source to bind to the panes header.
		/// </summary>
		[C1Description("C1Accordion.HeaderField", "The name of the field from the data source to bind to the panes header.")]
		[DefaultValue("")]
		[C1Category("Category.Data")]
		[Layout(LayoutType.Data)]
		public string HeaderField
		{
			get
			{
				object o = ViewState["HeaderField"];
				return ((o == null) ? string.Empty : (string)o);
			}
			set
			{
				ViewState["HeaderField"] = value;
				if (Initialized)
				{
					OnDataPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Gets or sets the name of the field from the data source to bind to the panes content.
		/// </summary>
		[C1Description("C1Accordion.ContentField", "The name of the field from the data source to bind to the panes content.")]
		[DefaultValue("")]
		[C1Category("Category.Data")]
		[Layout(LayoutType.Data)]
		public string ContentField
		{
			get
			{
				object o = ViewState["ContentField"];
				return ((o == null) ? string.Empty : (string)o);
			}
			set
			{
				ViewState["ContentField"] = value;
				if (Initialized)
				{
					OnDataPropertyChanged();
				}
			}
		}

		/// <summary>
		/// Collection of child panes in the accordion.
		/// </summary>
		[C1Description("Accordion.Panes")]
		[C1Category("Category.Appearance")]
		[NotifyParentProperty(true)]
		[Browsable(false)]
		[RefreshProperties(RefreshProperties.All)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		[PersistenceMode(PersistenceMode.InnerProperty)]
		[MergableProperty(false)]
		[Layout(LayoutType.Appearance)]
		public C1AccordionPaneCollection Panes
		{
			get
			{
				if (_panes == null)
					_panes = new C1AccordionPaneCollection(this);
				return _panes;
			}
		}

		/// <summary>
		/// Gets list of the visible panes that will be rendered into page.
		/// </summary>
		/// <value>The panes list.</value>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public List<C1AccordionPane> AvailablePanes
		{
			get
			{

				if (_visiblePanes == null
					||
					_visiblePanesVersion != this.Panes.Version)
				{
					_visiblePanes = new List<C1AccordionPane>();
					for (int i = 0; i < this.Panes.Count; i++)
					{
						if (this.Panes[i].Visible)
						{
							_visiblePanes.Add(this.Panes[i]);
						}
					}
					_visiblePanesVersion = this.Panes.Version;
				}
				return _visiblePanes;
			}
		}

		/// <summary>
		/// Gets or sets the selected pane in the accordion.
		/// </summary>
		[C1Description("Accordion.SelectedPane")]
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public C1AccordionPane SelectedPane
		{
			get
			{
				EnsureSelectedPane();
				int i = SelectedIndex;
				if (i < 0 || i >= AvailablePanes.Count)
				{
					return null;
				}
				return AvailablePanes[i];
			}
			set
			{
				int i = AvailablePanes.IndexOf(value);
				if (i < 0)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				SelectedIndex = i;
			}
		}

		/// <summary>
		/// Gets the name of the control tag. This property is used primarily by control developers.
		/// </summary>
		/// <value></value>
		/// <returns>
		/// The name of the control tag.
		/// </returns>
		protected override string TagName
		{
			get
			{
				return "div";
			}
		}
		/// <summary>
		/// Gets the <see cref="T:System.Web.UI.HtmlTextWriterTag"/> value that corresponds to this Web server control. This property is used primarily by control developers.
		/// </summary>
		/// <value></value>
		/// <returns>
		/// One of the <see cref="T:System.Web.UI.HtmlTextWriterTag"/> enumeration values.
		/// </returns>
		protected override HtmlTextWriterTag TagKey
		{
			get
			{
				return HtmlTextWriterTag.Div;
			}
		}


		/// <summary>
		/// Sets or retrieves a value that indicates whether or not the control posts back to the server each time a user interacts with the control. 
		/// </summary>
		[DefaultValue(false)]
		[C1Category("Category.Behavior")]
		[Layout(LayoutType.Behavior)]
		[C1Description("C1Accordion.AutoPostBack", "Indicates whether or not the control posts back to the server each time a user interacts with the control.")]
		public bool AutoPostBack
		{
			get
			{
				return this.ViewState["AutoPostBack"] == null ? false : (bool)this.ViewState["AutoPostBack"];
			}
			set
			{
				this.ViewState["AutoPostBack"] = value;
			}
		}

		/// <summary>
		/// This property is hidden
		/// </summary>
		[WidgetOption()]
		[DefaultValue(null)]
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public int[] ExpandedIndices
		{
			get
			{
				if (!IsDesignMode && !RequireOpenedPane)
				{
					List<int> list = new List<int>();
					for (int i = 0; i < this.Panes.Count; i++)
					{
						if (this.Panes[i].Expanded)
						{
							list.Add(i);
						}
					}
					return list.ToArray();
				}
				else
				{
					return null;
				}
			}
		}
		#endregion

		#region ** events

		/// <summary>
		/// Raised during postback when the selected index changed.
		/// </summary>
		[C1Description("C1Accordion.SelectedIndexChanged", "Raised during postback when the selected index changed.")]
		[C1Category("Category.Behavior")]
		public event EventHandler SelectedIndexChanged;

		/// <summary>
		/// Called when selected pane is changed.
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnSelectedIndexChanged(EventArgs e)
		{
			if (this.SelectedIndexChanged != null)
			{
				SelectedIndexChanged(this, e);

			}
		}

		#endregion

		#region ** methods

		#region ** Data binding / Create child controls

		#region ** DataBinding

		/// <summary>
		/// Retrieves data from the associated data source.
		/// </summary>
		protected override void PerformSelect()
		{

			// Call OnDataBinding here if bound to a data source using the
			// DataSource property (instead of a DataSourceID), because the
			// data binding statement is evaluated before the call to GetData.       
			if (!IsBoundUsingDataSourceID)
			{
				OnDataBinding(EventArgs.Empty);
			}

			// The GetData method retrieves the DataSourceView object from  
			// the IDataSource associated with the data-bound control.            
			GetData().Select(CreateDataSourceSelectArguments(),
				OnDataSourceViewSelectCallback);

			// The PerformDataBinding method has completed.
			RequiresDataBinding = false;
			MarkAsDataBound();

			// Raise the DataBound event.
			OnDataBound(EventArgs.Empty);
		}

		private void OnDataSourceViewSelectCallback(IEnumerable retrievedData)
		{
			// Call OnDataBinding only if it has not already been 
			// called in the PerformSelect method.
			if (IsBoundUsingDataSourceID)
			{
				OnDataBinding(EventArgs.Empty);
			}
			// The PerformDataBinding method binds the data in the  
			// retrievedData collection to elements of the data-bound control.
			PerformDataBinding(retrievedData);
		}

		/// <summary>
		/// Binds data from the data source to the control.
		/// </summary>
		/// <param name="retrievedData">The IEnumerable list of data returned from a PerformSelect method call.</param>
		protected override void PerformDataBinding(IEnumerable retrievedData)
		{
			base.PerformDataBinding(retrievedData);
			if (!IsBoundUsingDataSourceID && (DataSource == null))
			{
				this.Controls.Clear();
				this.CreateChildControlsFromPanes(false);
				return;
			}
			PerformDataBindingInternal(retrievedData);
		}

		private int PerformDataBindingInternal(IEnumerable retrievedData)
		{
			int count = 0;
			// If the data is retrieved from an IDataSource as an 
			// IEnumerable collection, attempt to bind its values to a 
			// set of TextBox controls.
			if (retrievedData != null)
			{
				// remember selected index before clear panes(fix for design time databinding)
				int selectedIndex = this.SelectedIndex; 
				this.Panes.Clear();
				foreach (object dataItem in retrievedData)
				{
					C1AccordionPane pane = new C1AccordionPane(this);
					this.Panes.Add(pane);
					pane.Expanded = (count == selectedIndex);// this not works at design time?

					string contentContent = "";
					// Content:
					if (ContentField.Length > 0)
					{
						contentContent = DataBinder.GetPropertyValue(dataItem, ContentField, null);
					}
					else
					{
						PropertyDescriptorCollection props = TypeDescriptor.GetProperties(dataItem);

						// Set the "default" value of the TextBox.
						contentContent = String.Empty;

						// Set the true data-bound value of the TextBox,
						// if possible.
						if (props.Count >= 2)
						{
							if (null != props[1].GetValue(dataItem))
							{
								contentContent = props[1].GetValue(dataItem).ToString();
							}
						}
					}
					string headerContent = "";
					// Header:
					if (HeaderField.Length > 0)
					{
						headerContent = DataBinder.GetPropertyValue(dataItem, HeaderField, null);
					}
					else
					{
						PropertyDescriptorCollection props = TypeDescriptor.GetProperties(dataItem);

						// Set the "default" value of the TextBox.
						headerContent = String.Empty;

						// Set the true data-bound value of the TextBox,
						// if possible.
						if (props.Count >= 1)
						{
							if (null != props[0].GetValue(dataItem))
							{
								headerContent = props[0].GetValue(dataItem).ToString();
							}
						}
					}
					pane.SetHeaderPanelContent(headerContent);
					pane.SetContentPanelContent(contentContent);
					
					count++;
				}

				this.Controls.Clear();
				this.CreateChildControlsFromPanes(true);
				ChildControlsCreated = true;
				// restore selected index after data binding(fix for design time databinding)
				this.SelectedIndex = selectedIndex; 
				EnsureSelectedPane();
			}
			return count;
		}

		/// <summary>
		/// This method overrides the generic DataBind method to bind the related data to the control.
		/// </summary>
		public sealed override void DataBind()
		{
			base.DataBind();
		}

		/// <summary>
		/// This method overrides the EnsureDataBound method in order to make sure that sub controls are also bound.
		/// </summary>
		protected override void EnsureDataBound()
		{
			base.EnsureDataBound();
			//if (!this._subControlsDataBound)
			//{
			//    foreach (Control control in this.Controls)
			//    {
			//        control.DataBind();
			//    }
			//    this._subControlsDataBound = true;
			//}
		}


		#endregion

		/// <summary>
		/// Handles the <see cref="E:System.Web.UI.Control.Init"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnInit(EventArgs e)
		{
			
			if (IsDesignMode && !_productLicensed && _shouldNag)
			{
				_shouldNag = false;
				ShowAbout();
			}
			base.OnInit(e);
			if (!IsDesignMode &&
				(!string.IsNullOrEmpty(this.DataSourceID)
				|| (this.DataSource != null)))
			{
				//Always create Panes from data source even if postback occurs.
				this.RequiresDataBinding = true;
			}										
			//Issue [1983] fix:
			this.EnsureChildControls();
			if (_Page_InitComplete == null)
			{
				_Page_InitComplete = new EventHandler(Page_InitComplete);
				this.Page.InitComplete += _Page_InitComplete;
			}
		}
		private EventHandler _Page_InitComplete;
		void Page_InitComplete(object sender, EventArgs e)
		{
			// create child controls before view state load and after page_init handler code:
			this.EnsureChildControls();
			if (_Page_InitComplete != null)
			{
				this.Page.InitComplete -= _Page_InitComplete;
				_Page_InitComplete = null;
			}
		}


		/// <summary>
		/// Force Accordion to create child controls.
		/// </summary>
		[Browsable(false)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public void ForceCreateChildControls()
		{
			CreateChildControls();
		}
		/// <summary>
		/// Overrides the method CreateChildControls in order to create the child controls for Accordion.
		/// </summary>
		protected override void CreateChildControls()
		{			
			if (base.RequiresDataBinding
				&& (!string.IsNullOrEmpty(this.DataSourceID)
				|| (this.DataSource != null)))
			{
				this.Controls.Clear();
				this.EnsureDataBound();
			}
			else
			{
				if (!this.IsDesignMode && this.Controls.Count == this.AvailablePanes.Count)
				{
					bool panesDirty = false;
					for (int i = 0; i < this.AvailablePanes.Count; i++)
					{
						if (this.Controls.IndexOf(this.AvailablePanes[i]) != i)
						{
							panesDirty = true;
							break;
						}						
					}
					if (!panesDirty)
					{
						// do not clear already created controls.
						//fix for:
						// +Is there any way to get the checkbox check events to fire within the Accordion
						// allow bind to child control events.
						return;
					}
				}				
				this.Controls.Clear();
				this.CreateChildControlsFromPanes(false);
			}
		}


		/// <summary>
		/// Creates a child controls collection using the <see cref="C1AccordionPane"/> collection.
		/// </summary>
		/// <remarks> This method is primarily used by control developers.</remarks>
		/// <param name="dataBinding">Indicates if data binding is used to create the collection.</param>
		protected virtual void CreateChildControlsFromPanes(bool dataBinding)
		{
			for (int i = 0; i < this.AvailablePanes.Count; i++)
			{
				if (this.IsDesignMode)
				{
					//if (AvailablePanes[i].DisplayVisible && !AvailablePanes[i].Removed)
					if (AvailablePanes[i].Visible)
					{
						this.Controls.Add(this.AvailablePanes[i]);
					}
				}
				else
				{
					this.Controls.Add(this.AvailablePanes[i]);
				}
			}

			// Ensure creation of child controls
			int kk = 0;
			foreach (C1AccordionPane pane in AvailablePanes)
			{
				EnsurePaneCeation(pane, kk);
				kk++;
			}
			ChildControlsCreated = true;
		}

		private void EnsurePaneCeation(C1AccordionPane pane, int paneIndex)
		{
			string sId = pane.ClientID;
			ControlCollection controls = pane.Controls;

		}

		#endregion

		/// <summary>
		/// Raises the <see cref="M:System.Web.UI.Control.OnPreRender(System.EventArgs)"/> event and registers the extender control with the <see cref="T:System.Web.UI.ScriptManager"/> control.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains the event data.</param>
		protected override void OnPreRender(EventArgs e)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.OnPreRenderCheckLicense(_productLicensed, Page, this);
			base.OnPreRender(e);
			if (this.Page != null)
			{
				this.Page.RegisterRequiresRaiseEvent(this);
				//this.Page.RegisterRequiresPostBack(this);
			}
		}

		/// <summary>
		/// Adds HTML attributes and styles that need to be rendered to the specified <see cref="T:System.Web.UI.HtmlTextWriterTag"/>. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void AddAttributesToRender(HtmlTextWriter writer)
		{
			string cssClass = string.IsNullOrEmpty(this.CssClass) ? "" : (this.CssClass + " ");
			if (!IsDesignMode)
			{
				cssClass += String.Format(" {0} ", Utils.GetHiddenClass());
			}
			else
			{
				cssClass += string.Format("wijmo-wijaccordion ui-accordion ui-widget ui-helper-reset ui-accordion-icons ui-accordion-{0}{1}",
							this.ExpandDirection.ToString().ToLowerInvariant(),
							(this.ExpandDirection == ExpandDirection.Left || 
							this.ExpandDirection == ExpandDirection.Right) ? " ui-helper-horizontal" : "");
			}
			writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);
			base.AddAttributesToRender(writer);
		}

		/// <summary>
		/// Renders the control to the specified HTML writer.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the control content.</param>
		protected override void Render(HtmlTextWriter writer)
		{
			C1.Web.Wijmo.Controls.Licensing.LicCheck.RenderLicenseWebComment(writer);
			this.EnsureChildControls();
			base.Render(writer);
		}

		/// <summary>
		/// Outputs the content of a server control's children to a provided <see cref="T:System.Web.UI.HtmlTextWriter"/> object, which writes the content to be rendered on the client.
		/// </summary>
		/// <param name="writer">The <see cref="T:System.Web.UI.HtmlTextWriter"/> object that receives the rendered content.</param>
		protected override void RenderChildren(HtmlTextWriter writer)
		{
			if (this.IsDesignMode 
				&& this.Panes.Count >0)
			{
				//writer.AddStyleAttribute("border", "5px solid red");writer.AddAttribute(HtmlTextWriterAttribute.Border, "15");
				writer.AddAttribute(HtmlTextWriterAttribute.Cellpadding, "0");
				writer.AddAttribute(HtmlTextWriterAttribute.Cellspacing, "0");
				writer.RenderBeginTag(HtmlTextWriterTag.Table);
				if (this.ExpandDirection == ExpandDirection.Left || this.ExpandDirection == ExpandDirection.Right)
				{
					writer.RenderBeginTag(HtmlTextWriterTag.Tr);
				}
				base.RenderChildren(writer);
				writer.RenderEndTag();
				if (this.ExpandDirection == ExpandDirection.Left || this.ExpandDirection == ExpandDirection.Right)
				{
					writer.RenderEndTag();
				}				
			}
			else
			{
				base.RenderChildren(writer);
			}
		}

		#endregion

		#region ** IPostBackEventHandler interface implementations

		/// <summary>
		/// Process an event
		/// raised when a form is posted to the server.
		/// </summary>
		/// <param name="eventArgument">A System.String that represents an optional event argument to be passed to
		/// the event handler.</param>
		public void RaisePostBackEvent(string eventArgument)
		{
			if (eventArgument.StartsWith("selectedIndex="))
			{
				int newInd = int.Parse(eventArgument.Remove(0, "selectedIndex=".Length));
				if (newInd != this.SelectedIndex)
				{
					this.SelectedIndex = newInd;
					OnSelectedIndexChanged(new EventArgs());
				}
			}			
		}

		#endregion
		
		#region ** ICompositeControlDesignerAccessor interface implementation

		/// <summary>
		/// Recreates child controls.
		/// </summary>
		public void RecreateChildControls()
		{
			CreateChildControls();
		}

		#endregion

		#region ** IC1Serializable interface implementations

		/// <summary>
		/// Saves the control layout properties to the file.
		/// </summary>
		/// <param name="filename">The file where the values of the layout properties will be saved.</param> 
		public void SaveLayout(string filename)
		{
			C1AccordionSerializer sz = new C1AccordionSerializer(this);
			sz.SaveLayout(filename);
		}

		/// <summary>
		/// Saves control layout properties to the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be saved.</param> 
		public void SaveLayout(Stream stream)
		{
			C1AccordionSerializer sz = new C1AccordionSerializer(this);
			sz.SaveLayout(stream);
		}

		/// <summary>
		/// Loads control layout properties from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		public void LoadLayout(string filename)
		{
			LoadLayout(filename, LayoutType.All);
		}

		/// <summary>
		/// Load control layout properties from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of layout properties will be loaded.</param> 
		public void LoadLayout(Stream stream)
		{
			LoadLayout(stream, LayoutType.All);
		}

		/// <summary>
		/// Loads control layout properties with specified types from the file.
		/// </summary>
		/// <param name="filename">The file where the values of layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(string filename, LayoutType layoutTypes)
		{
			C1AccordionSerializer sz = new C1AccordionSerializer(this);
			sz.LoadLayout(filename, layoutTypes);
		}

		/// <summary>
		/// Loads the control layout properties with specified types from the stream.
		/// </summary>
		/// <param name="stream">The stream where the values of the layout properties will be loaded.</param> 
		/// <param name="layoutTypes">The layout types to load.</param>
		public void LoadLayout(Stream stream, LayoutType layoutTypes)
		{
			C1AccordionSerializer sz = new C1AccordionSerializer(this);
			sz.LoadLayout(stream, layoutTypes);
		}


		#endregion

		#region ** private and internal implementation

		internal void SetDirty()
		{
			this.ChildControlsCreated = false;
		}

		private void EnsureSelectedPane()
		{
			bool selectionFound = false;
			for (int i = 0; i < AvailablePanes.Count; i++)
			{
				if (i == SelectedIndex)
				{
					AvailablePanes[i].Expanded = true;
					selectionFound = true;
				}
				else
				{
					if (RequireOpenedPane)
					{
						AvailablePanes[i].Expanded = false;
					}
				}
			}
			if (!selectionFound
				&& SelectedIndex > AvailablePanes.Count
				&& AvailablePanes.Count > 0)
			{
				SelectedIndex = AvailablePanes.Count - 1;
			}
		}

		private int GetValidIndex(int index)
		{
			if (this.IsPaneVisible(index))
				return index;

			int newIndex = this.GetNextIndex(index, false);
			if (newIndex < 0)
				newIndex = this.GetPrevIndex(index, false);

			return newIndex;
		}

		private int GetPrevIndex(int index, bool loop)
		{
			index--;
			while (index >= 0)
			{
				if (this.IsPaneVisible(index)) return index;
				index--;
			}

			if (index < 0)
			{
				if (loop)
					index = this.Panes.Count - 1;
			}

			while (index >= 0)
			{
				if (this.IsPaneVisible(index)) return index;
				index--;
			}

			if (index < 0)
				return -1;

			return index;
		}

		private int GetNextIndex(int index, bool loop)
		{
			index++;
			while (index < this.Panes.Count)
			{
				if (this.IsPaneVisible(index)) return index;
				index++;
			}
			if (index >= this.Panes.Count)
			{
				if (loop)
					index = 0;
			}
			while (index < this.Panes.Count)
			{
				if (this.IsPaneVisible(index)) return index;
				index++;
			}
			if (index >= this.Panes.Count)
				return -1;
			return index;
		}

		private bool IsPaneVisible(int index)
		{
			if (index < 0 || index >= this.Panes.Count) return false;
			if (this.DesignMode) return true;// needed in order to allow select pane at design time.
			//return ((AccordionPane)this.Panes[index]).DisplayVisible;
			return ((C1AccordionPane)this.Panes[index]).Visible;
		}

		private int ParseInt(object o)
		{
			if (o is int)
				return (int)o;
			if (o is string)
				return int.Parse((string)o);
			return int.Parse(o.ToString());
		}


		#endregion

		#region ** IPostBackDataHandler interface implementation

		private bool _raiseSelectedIndexChanged;

		/// <summary>
		/// When implemented by a class, processes postback data for an ASP.NET server control.
		/// </summary>
		/// <param name="postDataKey">The key identifier for the control.</param>
		/// <param name="postCollection">The collection of all incoming name values.</param>
		/// <returns>
		/// true if the server control's state changes as a result of the postback; otherwise, false.
		/// </returns>
		public bool LoadPostData(string postDataKey, NameValueCollection postCollection)
		{
			// fix for 15444:
			_postCollection = postCollection;
			Hashtable data = this.JsonSerializableHelper.GetJsonData(postCollection);
			return this.LoadClientData(data);			
		}
		/// <summary>
		/// Handles the <see cref="E:System.Web.UI.Control.Load"/> event.
		/// </summary>
		/// <param name="e">An <see cref="T:System.EventArgs"/> object that contains event data.</param>
		protected override void OnLoad(EventArgs e)
		{

			for (int i = 0; i < this.Panes.Count; i++)
			{
				this._apply15444fixRecur(this.Panes[i].Controls);
			}

			base.OnLoad(e);
		}
		private NameValueCollection _postCollection;

		private void _apply15444fixRecur(ControlCollection controls)
		{
			// fix for 15444
			// todo: ensure viewstate is loaded for templated controls then remove this fix.
			if (_postCollection != null && controls != null)
			{
				for (int i = 0; i < controls.Count; i++)
				{
					try
					{
						if (!string.IsNullOrEmpty(controls[i].UniqueID))
						{
							if (controls[i] is TextBox)
							{
								string text = _postCollection.Get(controls[i].UniqueID);
								if (!string.IsNullOrEmpty(text))
								{
									((TextBox)controls[i]).Text = text;
								}
							}
							else if (controls[i] is DropDownList)
							{
								string text = _postCollection.Get(controls[i].UniqueID);
								if (!string.IsNullOrEmpty(text))
								{
									((DropDownList)controls[i]).Text = text;
								}
							}
							else if (controls[i] is CheckBox)
							{
								string text = _postCollection.Get(controls[i].UniqueID);
								if (!string.IsNullOrEmpty(text) && text.ToLowerInvariant() == "on")
								{
									((CheckBox)controls[i]).Checked = true;
								}
								else
								{
									//fix for 28797:
									((CheckBox)controls[i]).Checked = false;
								}
							}
							else
							{
								_apply15444fixRecur(controls[i].Controls);
							}
						}
					}
					catch (Exception) { }
				}
			}
		}

		/// <summary>
		/// When implemented by a class, signals the server control to notify the ASP.NET application that the state of the control has changed.
		/// </summary>
		public void RaisePostDataChangedEvent()
		{
			if (!this.RequireOpenedPane && _clientExpandedIndices != null)
			{
				//First collapse all other panes:
				for (int i = 0; i < this.Panes.Count; i++)
				{
					this.Panes[i].Expanded = false;
				}
				for (int i = 0; i < _clientExpandedIndices.Count; i++)
				{
					if (this.Panes.Count > (int)_clientExpandedIndices[i])
					{
						this.Panes[(int)_clientExpandedIndices[i]].Expanded = true;
					}
				}
				_clientExpandedIndices = null;
			}
			if (_raiseSelectedIndexChanged)
			{
				this.OnSelectedIndexChanged(EventArgs.Empty);
			}
		}
		ArrayList _clientExpandedIndices = null;
		internal bool LoadClientData(Hashtable data)
		{
			bool isViewStateChanged = false;
			object o = data["selectedIndex"];
			if (o != null)
			{
				if (this.SelectedIndex != (int)o)
				{
					isViewStateChanged = true;
					this.SelectedIndex = (int)o;
					_raiseSelectedIndexChanged = true;					
				}
			}
			// fix for 26152:
			o = data["expandedIndices"];
			if (o != null)
			{
				_clientExpandedIndices = (ArrayList)o;
				isViewStateChanged = true;
			}
			return isViewStateChanged;
		}


		#endregion

	}
}
