﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.ComponentModel;
using System.Web;

namespace C1.Web.Wijmo.Controls.C1Accordion
{
	/// <summary>
	/// Holds the content for the pane's header in the C1Accordion control.
	/// </summary>
    [ToolboxItem(false)]
    public class C1AccordionPaneHeaderContainer : Panel, INamingContainer
	{

		#region ** fields
		
		internal string _literalContent = "";
		private C1AccordionPane _owner;
		private System.Web.UI.AttributeCollection _contentElementAttributes;
		private bool IsDesignMode = HttpContext.Current == null;

		#endregion		

		#region ** constructor

		/// <summary>
		/// Initializes a new instance of the <see cref="C1AccordionPaneHeaderContainer"/> class.
		/// </summary>
		/// <param name="owner">The owner accordion pane.</param>
		public C1AccordionPaneHeaderContainer(C1AccordionPane owner)
			: base()
		{
			this._owner = owner;
		}

		#endregion

		#region ** properties

		/// <summary>
		/// Gets a collection of all attribute name and value pairs 
		/// expressed on the content element tag within the ASP.NET page.
		/// </summary>
		/// <value>The content element attributes.</value>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public System.Web.UI.AttributeCollection ContentElementAttributes
		{
			get
			{
				if (_contentElementAttributes == null)
					_contentElementAttributes = new System.Web.UI.AttributeCollection(new StateBag());
				return _contentElementAttributes;
			}
		}

		#endregion

		#region ** methods

		#region ** render methods

		/// <summary>
		/// Renders the HTML opening tag of the control to the specified writer. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		public override void RenderBeginTag(HtmlTextWriter writer)
		{
			string cssClass = "";
			if (!_owner.Enabled)
			{
				//writer.AddAttribute(HtmlTextWriterAttribute.Disabled, "true");
				cssClass += "ui-state-disabled ";
			}
			if (!string.IsNullOrEmpty(_owner.CssClass))
			{
				cssClass += _owner.CssClass + " ";
			}
			if (IsDesignMode)
			{
				this.Attributes["role"] = "tab";
				if (_owner.Expanded)
				{
					cssClass += "wijmo-wijaccordion-header ui-helper-reset ui-state-active ui-corner-top ";					
					this.Attributes["tabindex"] = "0";
					this.Attributes["aria-expanded"] = "true";
				}
				else
				{
					cssClass += "wijmo-wijaccordion-header ui-helper-reset ui-state-default ui-corner-all ";					
					this.Attributes["tabindex"] = "-1";
					this.Attributes["aria-expanded"] = "false";
				}
			}
			if (!string.IsNullOrEmpty(cssClass))
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Class, cssClass);
			}
			if (!string.IsNullOrEmpty(_owner.ToolTip))
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Title, _owner.ToolTip);
			}
			if (!string.IsNullOrEmpty(_owner.AccessKey))
			{
				writer.AddAttribute(HtmlTextWriterAttribute.Accesskey, _owner.AccessKey);				
			}
			base.RenderBeginTag(writer);

			if (IsDesignMode)
			{
				string iconClass = "ui-icon-triangle-1-";
				if (_owner.Expanded)
				{
					switch (_owner._accordion.ExpandDirection)
					{
						case ExpandDirection.Left:
							iconClass += "w";
							break;
						case ExpandDirection.Right:
							iconClass += "e";
							break;
						case ExpandDirection.Top:
							iconClass += "n";
							break;
						case ExpandDirection.Bottom:
						default:
							iconClass += "s";
							break;
					}
					writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-icon " + iconClass);
					writer.RenderBeginTag(HtmlTextWriterTag.Span);
					writer.RenderEndTag();
				}
				else
				{
					switch (_owner._accordion.ExpandDirection)
					{
						case ExpandDirection.Left:
						case ExpandDirection.Right:
							iconClass += "s";
							break;
						case ExpandDirection.Top:
						case ExpandDirection.Bottom:
						default:
							iconClass += "e";
							break;
					}
					writer.AddAttribute(HtmlTextWriterAttribute.Class, "ui-icon " + iconClass);
					writer.RenderBeginTag(HtmlTextWriterTag.Span);
					writer.RenderEndTag();
				}
				this.ContentElementAttributes.AddAttributes(writer);
				//writer.AddStyleAttribute("min-height", "50px");
				//writer.AddStyleAttribute("min-width", "350px");
				//writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "block");
				//writer.AddStyleAttribute(HtmlTextWriterStyle.BackgroundColor, "green");
				
				// render div instead of anchor because of problems with editing designer regions:
				//writer.AddAttribute(HtmlTextWriterAttribute.Href, "#");
				//writer.RenderBeginTag(HtmlTextWriterTag.A);
				writer.RenderBeginTag(HtmlTextWriterTag.Div);
			}
		}

		/// <summary>
		/// Renders the HTML closing tag of the control into the specified writer. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		public override void RenderEndTag(HtmlTextWriter writer)
		{
			if (IsDesignMode)
			{
				writer.RenderEndTag(); // A (DIV)
			}
			base.RenderEndTag(writer);
		}

		/// <summary>
		/// Renders the contents of the control to the specified writer. This method is used primarily by control developers.
		/// </summary>
		/// <param name="writer">A <see cref="T:System.Web.UI.HtmlTextWriter"/> that represents the output stream to render HTML content on the client.</param>
		protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
		{
			base.RenderContents(writer);
			if (!string.IsNullOrEmpty(_literalContent))
			{
				writer.Write(_literalContent);
			}
		}

		#endregion

		#endregion
	}
}
