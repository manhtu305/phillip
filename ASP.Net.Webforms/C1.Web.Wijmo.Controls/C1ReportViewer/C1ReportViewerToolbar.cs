﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.HtmlControls;
using System.Web.UI;
using System.Web;
using System.IO;
using C1.Web.Wijmo.Controls.Localization;

namespace C1.Web.Wijmo.Controls.C1ReportViewer
{
	/// <summary>
	/// Represents toolbar UI.
	/// </summary>
	public class C1ReportViewerToolbar : HtmlGenericControl
	{

		#region ** fields
		private static ulong _uidCounter = 0;
		private C1ReportViewer _reportViewer;
		#endregion

		#region ** constructor
		
		/// <summary>
		/// Initializes a new instance of the <see cref="C1ReportViewerToolbar"/> class.
		/// </summary>
		public C1ReportViewerToolbar(C1ReportViewer reportViewer)
			: base("div")
		{
			this._reportViewer = reportViewer;
		}

		#endregion

		#region ** properties

		#endregion


		#region ** methods

		/// <summary>
		/// Creates the toolbar control.
		/// </summary>
		/// <param name="reportViewer">The report viewer.</param>
		public static C1ReportViewerToolbar CreateToolbar(C1ReportViewer reportViewer)
		{
			C1ReportViewerToolbar toolBar = new C1ReportViewerToolbar(reportViewer);
			toolBar.Attributes["class"] = "c1-c1reportviewer-toolbar ui-widget-header ui-corner-all";

			toolBar.Controls.Add(C1ReportViewerToolbar.CreateToolbarButton("print", C1Localizer.GetString("C1ReportViewer.Button.Print", "Print", reportViewer.Culture)));

			HtmlGenericControl exportGroup;
			
		
			if (HttpContext.Current == null)
			{
				// design time
				exportGroup = new HtmlGenericControl("span");
				exportGroup.InnerHtml = "<div class=\"export ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only\""+
						" role=\"button\" aria-disabled=\"false\" title=\"Export\"><span class=\"ui-button-icon-primary ui-icon ui-icon-disk\">"+
						"</span><span class=\"ui-button-text\">"+
						C1Localizer.GetString("C1ReportViewer.Button.Export", "Export", reportViewer.Culture) +
						"</span></div><div class=\"select_export_format ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only\" role=\"button\" aria-disabled=\"false\" title=\"select export format\"><span class=\"ui-button-icon-primary ui-icon ui-icon-triangle-1-s\"></span><span class=\"ui-button-text\">"+
						C1Localizer.GetString("C1ReportViewer.Button.SelectExportFormat", "select export format", reportViewer.Culture)+
						"</span></div>";
				exportGroup.Attributes["class"] = "exportgroup ui-buttonset";
			}
			else
			{
				// runtime
				exportGroup = new HtmlGenericControl("span");				
				exportGroup.InnerHtml = "<button class=\"export\">"+
					C1Localizer.GetString("C1ReportViewer.Button.Export", "Export", reportViewer.Culture)
					+"</button><button class=\"select_export_format\">"+
					C1Localizer.GetString("C1ReportViewer.Button.SelectExportFormat", "select export format", reportViewer.Culture)+
					"</button>";
				exportGroup.Attributes["class"] = "exportgroup";
			}
			
			toolBar.Controls.Add(exportGroup);
			/*
<span class="exportgroup">
	<button class="export">Export</button>
	<button class="select_export_format">Select export format</button>
</span>			 
			 */

			toolBar.Controls.Add(C1ReportViewerToolbar.CreateToolbarButton("firstpage",
				C1Localizer.GetString("C1ReportViewer.Button.FirstPage", "First page", reportViewer.Culture)
				));
			toolBar.Controls.Add(C1ReportViewerToolbar.CreateToolbarButton("previouspage",
				C1Localizer.GetString("C1ReportViewer.Button.PreviousPage", "Previous page", reportViewer.Culture)
				));

			toolBar.Controls.Add(new HtmlGenericControl("span")
			{
				InnerHtml = "<input type=\"text\" class=\"pageindex\" value=\"1\" aria-label=\"pageindex\" /> / <label class=\"pagecount\">0</label>"
			});

			toolBar.Controls.Add(C1ReportViewerToolbar.CreateToolbarButton("nextpage",
				C1Localizer.GetString("C1ReportViewer.Button.NextPage", "Next page", reportViewer.Culture)
				));
			toolBar.Controls.Add(C1ReportViewerToolbar.CreateToolbarButton("lastpage",
				C1Localizer.GetString("C1ReportViewer.Button.LastPage", "Last page", reportViewer.Culture)
				));
			toolBar.Controls.Add(C1ReportViewerToolbar.CreateToolbarButton("zoomout", 
				C1Localizer.GetString("C1ReportViewer.Button.ZoomOut", "Zoom out", reportViewer.Culture)
				));
			toolBar.Controls.Add(C1ReportViewerToolbar.CreateToolbarButton("zoomin", 
				C1Localizer.GetString("C1ReportViewer.Button.ZoomIn", "Zoom in", reportViewer.Culture)
				));

			toolBar.Controls.Add(C1ReportViewerToolbar.CreateToolbarCombo("zoom", new string[] { 
					"10%", 
					"25%",
					"50%",
					"75%",
					"100%",
					"125%",
					"150%",
					"200%",
					"400%",
					"actual size",
					"fit page",
					"fit width",
					"fit height"}, 
					new string[] { 
					"10%", 
					"25%",
					"50%",
					"75%",
					"100%",
					"125%",
					"150%",
					"200%",
					"400%",
					C1Localizer.GetString("C1ReportViewer.Zoom.ActualSize",  "Actual size", reportViewer.Culture),
					C1Localizer.GetString("C1ReportViewer.Zoom.FitPage",  "Fit page", reportViewer.Culture),
					C1Localizer.GetString("C1ReportViewer.Zoom.FitWidth",  "Fit width", reportViewer.Culture),
					C1Localizer.GetString("C1ReportViewer.Zoom.FitHeight",  "Fit height", reportViewer.Culture)
					}, reportViewer.Zoom));
			
			toolBar.Controls.Add(C1ReportViewerToolbar.CreateToolbarCheckbox("continuousview",
				C1Localizer.GetString("C1ReportViewer.Button.ContinuousView", "continuous view", reportViewer.Culture)
				, !reportViewer.PagedView));
			toolBar.Controls.Add(C1ReportViewerToolbar.CreateToolbarCheckbox("fullscreenmode",
				C1Localizer.GetString("C1ReportViewer.Button.FullScreenMode", "full screen", reportViewer.Culture)
				, reportViewer.FullScreen));
			return toolBar;
		}

		/// <summary>
		/// Creates the toolbar button. 
		/// Note, css class is used to identify toolbar button. 
		/// There are number of reserved css classes:
		/// "print", "firstpage", "nextpage", "lastpage", "zoomout", 
		/// "zoomin", "continuousview"
		/// </summary>
		/// <param name="className">Name of the css class that will be added to the button.</param>
		/// <param name="text">The title text.</param>
		/// <returns></returns>
		public static HtmlGenericControl CreateToolbarButton(string className, string text)
		{
			HtmlGenericControl button;
			if (HttpContext.Current == null)
			{
				button = new HtmlGenericControl("div");//render div at design time
				button.Attributes["class"] = string.Format("{0} ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only", className);
				button.Attributes["role"] = "button";
				button.Attributes["aria-disabled"] = "false";
				button.Attributes["title"] = text;
				button.InnerHtml = string.Format("<span class=\"ui-button-icon-primary ui-icon {0}\"></span><span class=\"ui-button-text\">{1}</span>", DetermineUIIconByClassName(className), text);

				button.Attributes["style"] = "height: 40px;";//design-time buttons height fix
			}
			else
			{
				button = new HtmlGenericControl("button");
				button.Attributes["class"] = className;
				button.Attributes["title"] = text;
				button.InnerHtml = text;
			}
			return button;
		}

		/// <summary>
		/// Creates the toolbar checkbox.
		/// </summary>
		/// <param name="className">Name of the css class that will be added to the checkbox.</param>
		/// <param name="text">The label text.</param>
		/// <returns></returns>
		public static Control CreateToolbarCheckbox(string className, string text, bool isChecked)
		{
			if (HttpContext.Current == null)
			{
				HtmlGenericControl button = C1ReportViewerToolbar.CreateToolbarButton(className, text);
				if (isChecked)
				{
					button.Attributes["class"] += button.Attributes["class"] + " ui-state-active";
				}
				return button;
			}
			else
			{
				return new HtmlGenericControl("span")
				{
					InnerHtml = string.Format("<input type=\"checkbox\"{3} id=\"c1reportviewer_{2}_{0}\" class=\"{2}\" /><label for=\"c1reportviewer_{2}_{0}\">{1}</label>",
					(_uidCounter++).ToString(), text, className, isChecked ? " checked=\"checked\"" : "")
				};
			}
		}

		/// <summary>
		/// Creates the toolbar combo.
		/// </summary>
		/// <param name="className">The className.</param>
		/// <param name="optionValues">The option values.</param>
		/// <param name="optionTexts">Optional. The option texts.</param>
		/// <returns></returns>
		public static Control CreateToolbarCombo(string className, string[] optionValues, string[] optionTexts, string defaultValue)
		{
			HtmlGenericControl select = new HtmlGenericControl("select");
			select.Attributes["class"] = className;
			if (optionTexts == null)
			{
				optionTexts = optionValues;
			}
			if (optionValues.Length != optionTexts.Length)
			{
				throw new Exception("Length of the optionTexts array should be same as optionValues array length.");
			}
			if (optionValues != null)
			{
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < optionValues.Length; i++)
				{
					sb.Append(string.Format("<option{2} value=\"{0}\">{1}</option>", optionValues[i], optionTexts[i],
							optionValues[i].ToLowerInvariant() == defaultValue.ToLowerInvariant() ? " selected=\"selected\"" : ""));

				}
				select.InnerHtml = sb.ToString();
			}
			return select;
		}

		#endregion

		#region ** private and internal implementation

		private static string DetermineUIIconByClassName(string className)
		{
			switch (className)
			{
				case "print":
					return "ui-icon-print";
				case "firstpage":
					return "ui-icon-seek-start";
				case "nextpage":
					return "ui-icon-triangle-1-e";
				case "lastpage":
					return "ui-icon-seek-end";
				case "zoomout":
					return "ui-icon-zoomout";
				case "zoomin":
					return "ui-icon-zoomin";
				case "continuousview":
					return "ui-icon-carat-2-n-s";//ui-icon-carat-2-n-s
			}
			return "";
		}

		#endregion

	}
}
