using System;
using System.IO;
using System.Web;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using C1.C1Preview.Export;


#if WIJMOCONTROLS
namespace C1.Web.Wijmo.Controls.C1ReportViewer.ReportService
#else
namespace C1.Web.UI.Controls.C1Report.ReportService
#endif
{
    // TODO:
    // - think how to delete page zip if cache is disabled.

    /*
     * Caching overview:
     * 
     * NOTE: currently work with disabled cache is NOT supported!
     * 
     * The following objects are placed in server cache (HttpContext.Cache):
     * 
     * - CachedDocument
     * - CachedGeneratingDocument
     * - CachedPageImage
     * - ThreadedErrors
     * - PageRequestQueue
     * 
     * All work with HttpContext.Cache is done via the static helper class CacheHandler,
     * with specialized methods for each of the above types.
     * 
     * All server cache keys are created by methods of the static helper class KeyMaker.
     * 
     * - CachedDocument:
     *      Represents a document or report being viewed by the client (viewer).
     *      Identified by the combination of FileName and ReportName, plus
     *      session id ONLY IF ShareBetweenSessions is false. If the viewed document
     *      is an in-memory document created by user code, it MUST be identified by
     *      a unique ReportName (i.e. user code must assign/test that if different
     *      in-memory documents may be created).
     * 
     *      CachedDocument is created when the client first tries to access the specified
     *      document, and inserted into the server cache by CachedDocument.InsertDocument
     *      method. After that, all work with a document starts by fetching it from cache
     *      via CachedDocument.GetCachedDocument method.
     *      
     *      If during creation the document is empty (C1PrintDocument.Pages.Count==0),
     *      the GenerateDoc.StartGenerateDocument method is called. It spawns two threads:
     *      - "zip" (AddGeneratedPagesToZipThread) - which waits on a semaphore and adds
     *      available pages to the page zip as they become available;
     *      - "generate" (GenerateDocument) - generates document, subscribes to page
     *      added/changed and document ended events, and adds page metafiles to the queue
     *      to be zipped by the "zip" thread. (Metafiles must be created in the same thread
     *      as the document generation for GDI to work correctly.)
     *      
     *      Otherwise, a separate thread is created to add all existing pages to the page zip
     *      via CachedDocument.InsertPagesToZip
     *      
     *      Instance prop CachedDocument.SyncRoot should be used to sync access to the
     *      CachedDocument.
     *      
     * - CachedGeneratingDocument:
     *      Used to keep track of a currently generating document. Basically, it allows
     *      to associate CachedDocument with a C1PrintDocument (after a document's
     *      generation has ended, C1PrintDocument is no longer accessible).
     * 
     * - CachedPageImage:
     *      Corresponds to a page image (PNG) temp file on disk. Has two functions:
     *      - keeps track of temp images files on disk, deletes those files when
     *        corresponding CachedPageImage are removed from cache;
     *      - stores page size for easy retrieval.
     * 
     * - ThreadedErrors:
     *      Per-CachedDocument queue of errors that may have occurred during document generation.
     * 
     * - PageRequestQueue:
     *      Per-CachedDocument queue of specific page requests. Page indices are added to that
     *      queue so that the "zip" thread may add certain pages ahead of order.
     * 
     * ------ below is outdated info, qq/todo: update -----
     * 
     * 
     * 
     * - CachedDocument.Page
     * Cached pages contain page metafiles and additional info (such as text
     * markup). While semantically pages are part of CachedDocument, physically
     * they may be stored in a temp zip file on disk. Pages are cached in a
     * separate thread, and become available as they are added.
     * 
     * - CachedPageRequestsQueue
     * Used by frontend threads to request specific pages. Background thread
     * adding CachedDocument.Page's to zip file checks this queue and tries to
     * cache requested pages as soon as possible.
     * 
     * - CachedPageImage
     * Used to keep track of created page image files (PNG) on disk.
     * When an item of this type expires, the corresponding image file is deleted.
     * 
     * If cache is disabled, each instance of it retrieves and stores within itself
     * the C1PrintDocument - so each request from the client, because it produces its
     * own copy of the ServiceCache, would cause the document/report to be regenerated.
     * 
     * Multithreading:
     * 
     * The CachedDocument object in cache is locked when it is being changed. When
     * pages are added to it, it is locked when a page is added, then unlocked, then locked
     * again for the next page. Thus, e.g. page 0 may be retrieved when the pages are
     * still being added.
     * 
     * 
     */


    /// <summary>
    /// Implements server caching logic for report control or service.
    /// </summary>
    internal class ServiceCache
    {
        #region data members
        // in-memory document or report:
        private object _document = null;
        // maps of user properties:
        private ReportCache _reportCache = null;
        private ViewType _viewType = ViewType.PageImages;
        private bool _showParameterInputForm = true;
        private PdfSecurity _pdfSecurity = null;
        // private data:
        private string _workDir = null;
        private string _workUrl = null;
        private const int c_fileTimeoutMsec = 5000;
        private const int c_fileWaitMsec = 456; // 900
        // this is used to store a single document ONLY if cache is disabled:
        //private CachedDocument _uncachedDocument = null; //200100301 DMA, commented in order to resolve build warning.
        #endregion

        #region private types
        private delegate CachedDocument GetCachedDocument();
        #endregion

        #region ctor and infrastructure
        /// <summary>
        /// Initializes a new instance of the service cache class.
        /// </summary>
        public ServiceCache(object document, ReportCache reportCache, Dictionary<string, object> extraOptions, string workDir, string workUrl)
		{
            _document = document;
            _reportCache = reportCache;
            _workDir = workDir;
            _workUrl = workUrl;
            //
            object showParameterInputForm;
            object pdfSecurity;
            if (extraOptions != null && extraOptions.TryGetValue("ShowParameterInputForm", out showParameterInputForm) && (showParameterInputForm is bool))
                _showParameterInputForm = (bool)showParameterInputForm;

            if (extraOptions != null && extraOptions.TryGetValue("PdfSecurity", out pdfSecurity) && (pdfSecurity is PdfSecurity))
                _pdfSecurity = (PdfSecurity)pdfSecurity;

            // create work folder if it doesn't exist:
            if (!Directory.Exists(_workDir))
                Directory.CreateDirectory(_workDir);
        }
        #endregion

        #region public properties
        public object Document
        {
            get { return _document; }
        }

        public ReportCache ReportCache
        {
            get { return _reportCache; }
        }

        public PdfSecurity PdfSecurity
        {
            get { return _pdfSecurity; }
        }

        public ViewType ViewType
        {
            get { return _viewType; }
            set
            {
                _viewType = value;
                // todo: what?
            }
        }

        public string WorkDir
        {
            get { return _workDir; }
        }

        public string WorkUrl
        {
            get { return _workUrl; }
        }

        public bool ShowParameterInputForm
        {
            get { return _showParameterInputForm; }
        }
        #endregion

        #region debug logging
        [Conditional("DEBUG")]
        static public void LogMessage(string msg)
        {
            System.Diagnostics.Debug.WriteLine("SERVICE CACHE Message: " + msg);
        }

        [Conditional("DEBUG")]
        static public void LogWarning(string msg)
        {
            System.Diagnostics.Debug.WriteLine("SERVICE CACHE Warning: " + msg);
        }

        [Conditional("DEBUG")]
        static public void LogError(string msg)
        {
            System.Diagnostics.Debug.WriteLine("SERVICE CACHE ERROR: " + msg);
        }
        #endregion

        #region public methods
        /// <summary>
        /// This method should be called by the client when the in-memory document has changed.
        /// </summary>
        /// <param name="document">The new document value.</param>
        public void OnDocumentChanged(object document)
        {
            _document = document;
            // todo: clear cache???
        }

        public bool HasCachedDocument(string fileName, string reportName, ReportParameterValue[] paramValues)
        {
            DocIdentity di = new DocIdentity(fileName, reportName, paramValues);
            string documentKey = KeyMaker.CachedDocumentKey(di, _reportCache.GetShareBetweenSessions(), this.ShowParameterInputForm, HttpContext.Current);
            CachedDocument cdoc = CacheHandler.GetCachedDocument(documentKey);
            return cdoc != null;
        }

        public string GetDocumentKey(string fileName, string reportName, ReportParameterValue[] paramValues)
        {
            DocIdentity di = new DocIdentity(fileName, reportName, paramValues);
            return KeyMaker.CachedDocumentKey(di, _reportCache.GetShareBetweenSessions(), this.ShowParameterInputForm, HttpContext.Current);
        }

        public DocumentStatus GetDocumentStatus(string fileName, string reportName, ReportParameterValue[] paramValues, string cookie)
        {
            string errorDesc = "";
            DocIdentity di = new DocIdentity(fileName, reportName, paramValues);
            CachedDocument cdoc = CachedDocument.GetCachedDocument(this, di, ref errorDesc);
            
            if (cdoc == null && di.ParamInfos != null)
            {
                // params need to be input:
                DocumentStatus paramStatus = new DocumentStatus();
                paramStatus.reportParams = di.ParamInfos.ToArray();
                paramStatus.state = GeneratingState.ParametersRequested;
                paramStatus.documentKey = string.Empty;
                return paramStatus;
            }

            DocumentStatus status = GetDocumentStatus(cdoc, cookie);
            if (status == null)
            {
                // do it here as we can provide better diagnostics:
                status = new DocumentStatus();
                //If the ReportName property is set to an invalid report name then the 
                //control *should* throw an exception and should pass information about what 
                //happened (cannot find report 'ABC' in report definition file 'XYZ').

                string errorMessage = string.Format(CultureInfo.InvariantCulture, "Unable to load report. FileName: {0}, ReportName: {1}", Path.GetFileName(fileName), reportName);
                if (!string.IsNullOrEmpty(errorDesc))
                    errorMessage = errorDesc;
                try
                {
                    if (!File.Exists(fileName))
                    {
                        errorMessage = string.Format(CultureInfo.InvariantCulture, "Report definition file '{0}' not found.", Path.GetFileName(fileName));
                    }
                    else
                    {
                        // test if report name is correct
                        string[] reports = C1.C1Report.C1Report.GetReportList(fileName);
                        if (reports != null && reports.Length > 0)
                        {
                            bool found = false;
                            for (int i = 0; i < reports.Length; i++)
                            {
                                if (reports[i] == reportName)
                                    found = true;
                            }
                            if (!found)
                            {
#if WIJMOCONTROLS
								
									errorMessage = string.Format(CultureInfo.InvariantCulture,
C1ReportViewer.LocalizeString("C1ReportViewer.Messages.CantFindReportInReportDefinitionFileFormat", "Cannot find report '{0}' in report definition file '{1}'."), 
										reportName, Path.GetFileName(fileName));
#else
									errorMessage = string.Format(CultureInfo.InvariantCulture, "Cannot find report '{0}' in report definition file '{1}'.", reportName, Path.GetFileName(fileName));
#endif
                                
                            }
                        }
                    }
                }
                catch (Exception)
                {
                }
                status.error = errorMessage;
            }
            return status;
        }

        public DocumentStatus GetDocumentStatus(string documentKey, string cookie)
        {
            return GetDocumentStatus(CachedDocument.GetCachedDocument(documentKey), cookie);
        }

        private DocumentStatus GetDocumentStatus(CachedDocument cdoc, string cookie)
        {
            try
            {
                if (cdoc != null)
                    return cdoc.GetDocumentStatus(cookie);
                else
                    return null;
            }
            catch (Exception ex)
            {
                DocumentStatus err = new DocumentStatus();
                err.error = ex.Message;
                return err;
            }
        }

        public PageImagesMarkup GetPageImagesMarkup(string documentKey, int dpi, int[] pageIndices, bool noTextMarkup)
        {
            CachedDocument cdoc = CachedDocument.GetCachedDocument(documentKey);
            return GetPageImagesMarkup(cdoc, dpi, pageIndices, noTextMarkup);
        }

        private PageImagesMarkup GetPageImagesMarkup(CachedDocument cdoc, int dpi, int[] pageIndices, bool noTextMarkup)
        {
            PageImagesMarkup markup = new PageImagesMarkup();
            try
            {
                markup.documentKey = cdoc.Key;
                if (pageIndices.Length > 0)
                {
                    double zoom = (double)dpi / (double)cdoc.Dpi;
                    List<PageImageMarkup> pageMarkups = cdoc.GetPagesFromZip(new List<int>(pageIndices), zoom, dpi, noTextMarkup, false);
                    markup.pages = pageMarkups.ToArray();
                }
                else
                    markup.pages = new PageImageMarkup[0];

                markup.error = ThreadedErrors.PopErrors(cdoc.Key);
            }
            catch (Exception ex)
            {
                markup.error = ex.Message;
            }
            return markup;
        }

        public string GetUnpagedHtmlMarkup(string documentKey)
        {
            return "GetUnpagedHtmlMarkup(string documentKey) is not yet supported";
        }

        public string GetPageHtmlMarkup(string documentKey, int pageIndex)
        {
            return "GetPageHtmlMarkup(string documentKey, int pageIndex) is not yet supported";
        }

        public DocumentOutline GetDocumentOutline(string documentKey)
        {
            return GetDocumentOutline(CachedDocument.GetCachedDocument(documentKey));
        }

        private DocumentOutline GetDocumentOutline(CachedDocument cdoc)
        {
            try
            {
                return cdoc.Outline;
            }
            catch (Exception ex)
            {
                DocumentOutline outline = new DocumentOutline();
                outline.error = ex.Message;
                return outline;
            }
        }

        public SearchResults SearchText(string documentKey, string query, bool caseSensitive, bool useRegExp)
        {
            return SearchText(CachedDocument.GetCachedDocument(documentKey), query, caseSensitive, useRegExp);
        }

        private SearchResults SearchText(CachedDocument cdoc, string query, bool caseSensitive, bool useRegExp)
        {
            try
            {
                return cdoc.SearchText(query, caseSensitive, useRegExp);
            }
            catch (Exception ex)
            {
                SearchResults err = new SearchResults();
                err.error = ex.Message;
                return err;
            }
        }

        public byte[] GetPageImage(string documentKey, int dpi, int pageIndex, bool printTarget)
        {
            GetCachedDocument getCachedDocument = () =>
            {
                return CachedDocument.GetCachedDocument(documentKey);
            };
            return GetPageImage(getCachedDocument, dpi, pageIndex, printTarget);
        }

		public string[] GetPageImageUrls(string documentKey, int dpi)
		{
			CachedDocument cdoc = null;
			string[] imageUrls = null;
			try
			{
				if (cdoc == null)
				{
					cdoc = CachedDocument.GetCachedDocument(documentKey);
				}
				if (cdoc != null)
				{
					imageUrls = cdoc.GetPageImageUrls(dpi);
				}
			}
			catch
			{
				// ignore errors...
			}
			return imageUrls;
		}

        private byte[] GetPageImage(GetCachedDocument getCachedDocument, int dpi, int pageIndex, bool printTarget)
        {
            const int totalTimeout = 60 * 1000; // 1 minute
            const int retryTimeout = 1000; // retry each second
            try
            {
                CachedDocument cdoc = null;
                for (int waitedMsec = 0; waitedMsec < totalTimeout; waitedMsec += retryTimeout)
                {
                    try
                    {
                        if (cdoc == null)
                        {
                            // We get the cached document here as it may not be available yet,
                            // in which case we need to retry getting it later:
                            cdoc = getCachedDocument();
                        }
                        if (cdoc != null)
                        {
                            byte[] bytes = cdoc.GetPageImage(dpi, pageIndex, printTarget);
                            if (bytes != null)
                                return bytes;
                        }
                    }
                    catch
                    {
                        // ignore errors...
                    }
                    Thread.Sleep(retryTimeout);
                }
                return null;
            }
            catch
            {
                // todo: diagnostics?
                return null;
            }
        }

        public void UpdatePdfSecurity(Dictionary<string, object> extraOptions)
        {
            object pdfSecurity; 
            if (extraOptions != null && extraOptions.TryGetValue("PdfSecurity", out pdfSecurity) && (pdfSecurity is PdfSecurity))
                _pdfSecurity = (PdfSecurity)pdfSecurity;
        }

		public string ExportToFile(string documentKey, string format, string exportedFileName)
        {
            CachedDocument cdoc = CachedDocument.GetCachedDocument(documentKey);
            if (cdoc != null)
	                return cdoc.ExportToFile(format, exportedFileName, _pdfSecurity);
            else
                return CachedDocument.ExportToFileError("could not load document");
        }

        public void ClearCachedDocument(string documentKey)
        {
            CachedDocument.ClearCachedDocument(documentKey);
        }
        #endregion
    }
}
