﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.Caching;
using System.Collections.Generic;
using System.Threading;
using System.Text;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using C1.C1Preview;

#if WIJMOCONTROLS
using C1.Web.Wijmo.Controls.Localization;
namespace C1.Web.Wijmo.Controls.C1ReportViewer.ReportService	
#else
namespace C1.Web.UI.Controls.C1Report.ReportService
#endif
{
    internal enum DocumentType
    {
        None,
        C1PrintDocument,
        C1Report,
        C1RdlReport,
        C1MultiDocument,
    }

    internal static class Constants
    {
        public const int ZipLockTimeoutMsec = 30 * 1000;
        public const int ImageFileSaveTimeoutMsec = 12 * 1000;
        public const int ImageFileSaveRetryMsec = 160;
        public const int CacheC1DocSlidingExpireMin = 10; // minutes (todo: make it user-adjustable)
        public const int CacheChangedPageCookiesMin = 30; // minutes
        public const int ReadFileDataRetries = 5;
        public const int ReadFileDataDelayMsec = 100;
    }

    internal static class Strings
    {
#if WIJMOCONTROLS
		public static readonly string DocumentEmptyError = C1Localizer.GetString(
		"C1ReportViewer.ReportService.DocumentEmptyError", "The document is empty.", Thread.CurrentThread.CurrentUICulture);
		public static readonly string CannotExportObjectFmt = C1Localizer.GetString(
		"C1ReportViewer.ReportService.CannotExportObjectFmt", "Cannot export current document to {0}.", Thread.CurrentThread.CurrentUICulture);
		public static readonly string AddCachedPageToZipFailed = C1Localizer.GetString(
		"C1ReportViewer.ReportService.AddCachedPageToZipFailed", "Failed to write page to disk cache.", Thread.CurrentThread.CurrentUICulture);
#else
		public const string DocumentEmptyError = "The document is empty.";
        public const string CannotExportObjectFmt = "Cannot export current document to {0}.";
        public const string AddCachedPageToZipFailed = "Failed to write page to disk cache.";
#endif
	}

    /// <summary>
    /// Holds either a C1PrintDocument or a C1MultiDocument reference.
    /// </summary>
    internal abstract class C1DocHolder
    {
        public static C1DocHolder FromC1PrintDocument(C1PrintDocument doc)
        {
            return new C1PrintDocumentHolder(doc);
        }

        public static C1DocHolder FromC1MultiDocument(C1MultiDocument mdoc)
        {
            return new C1MultiDocumentHolder(mdoc);
        }

        public bool IsMultiDoc { get { return MultiDoc != null; } }
        public virtual C1PrintDocument Document { get { return null; } }
        public virtual C1MultiDocument MultiDoc { get { return null; } }
        // properties:
        public abstract int PageCount { get; }
        public abstract bool IsGenerating { get; }
        public abstract DateTime CreationTime { get; }
        // methods:
        public abstract C1Page GetPage(int index);
        public abstract Size GetDefaultPageSize(int dpi);
        public abstract Metafile MakePageMetafile(int index);
        public abstract C1AnchorInfo FindAnchor(string anchorName);
        public abstract OutlineNodeCollection GetOutlines();

        public SizeD GetPageSize(int index, int dpi)
        {
            C1Page page = GetPage(index);
            return new SizeD(
                page.Document.FromRU(page.ResolvedPage.FullWidth, UnitTypeEnum.Pixel, dpi),
                page.Document.FromRU(page.ResolvedPage.FullHeight, UnitTypeEnum.Pixel, dpi));
        }
        public OffsetsD GetPageMargins(int index, int dpi)
        {
            C1Page page = GetPage(index);
            return page.Document.FromRU(page.ResolvedInfo.PageMargins, UnitTypeEnum.Pixel, dpi, dpi);
        }
        public DocumentOutline MakeOutline(string documentKey)
        {
            return Util.MakeDocumentOutline(this, documentKey);
        }
    }

    internal class C1PrintDocumentHolder : C1DocHolder
    {
        private C1PrintDocument _doc = null;

        public C1PrintDocumentHolder(C1PrintDocument doc)
        {
            Debug.Assert(doc != null);
            _doc = doc;
        }

        public override C1PrintDocument Document { get { return _doc; } }

        public override int PageCount { get { return _doc.Pages.Count; } }
        public override bool IsGenerating { get { return _doc.IsGenerating; } }
        public override DateTime CreationTime { get { return _doc.DocumentInfo.CreationTime; } }

        public override C1Page GetPage(int index)
        {
            return _doc.Pages[index];
        }
        public override Size GetDefaultPageSize(int dpi)
        {
            return new Size(
                (int)Math.Round(_doc.PageLayout.PageSettings.Width.ConvertUnit(_doc.CreationDpi, UnitTypeEnum.Pixel, dpi)),
                (int)Math.Round(_doc.PageLayout.PageSettings.Height.ConvertUnit(_doc.CreationDpi, UnitTypeEnum.Pixel, dpi)));
        }
        public override Metafile MakePageMetafile(int index)
        {
            // Note: whiten may be needed (?) if image format is jpeg:
            const bool whiten = false;
            using (GraphicsHolder gh = GraphicsHolder.FromBitmap())
                return _doc.Pages[index].AsMetafile(gh.Graphics, EmfType.EmfPlusOnly, whiten, true, System.Drawing.Drawing2D.SmoothingMode.HighQuality);
        }
        public override C1AnchorInfo FindAnchor(string anchorName)
        {
            return _doc.FindAnchor(anchorName);
        }
        public override OutlineNodeCollection GetOutlines()
        {
            return _doc.Outlines;
        }
    }

    internal class C1MultiDocumentHolder : C1DocHolder
    {
        private C1MultiDocument _mdoc = null;
        private DateTime _creationTime = DateTime.Now; // todo: add it to MultiDoc

        public C1MultiDocumentHolder(C1MultiDocument mdoc)
        {
            Debug.Assert(mdoc != null);
            _mdoc = mdoc;
        }
        public override C1MultiDocument MultiDoc { get { return _mdoc; } }

        public override int PageCount { get { return _mdoc.PageCount; } }
        public override bool IsGenerating { get { return _mdoc.IsGenerating; } }
        public override DateTime CreationTime { get { return _creationTime; } }

        public override Size GetDefaultPageSize(int dpi)
        {
            double width, height, margin;
            UnitTypeEnum unit;
            C1PageSettings.GetCurrentLocaleDefaultPaperSize(out width, out height, out margin, out unit);
            return new Size(
                (int)C1.C1Preview.Utils.ConvertUnits(width, unit, UnitTypeEnum.Pixel, 0, dpi),
                (int)C1.C1Preview.Utils.ConvertUnits(height, unit, UnitTypeEnum.Pixel, 0, dpi));
        }
        public override C1Page GetPage(int index)
        {
            return _mdoc.GetResolvedPage(index).Page;
        }
        public override Metafile MakePageMetafile(int index)
        {
            return _mdoc.PageAsMetafile(index, true, System.Drawing.Drawing2D.SmoothingMode.HighQuality);
        }
        public override C1AnchorInfo FindAnchor(string anchorName)
        {
            return _mdoc.FindAnchor(anchorName);
        }
        public override OutlineNodeCollection GetOutlines()
        {
            return _mdoc.MakeOutlines();
        }
    }

    /// <summary>
    /// Represents the set of properties that uniquely identify a generated and cached
    /// report or document. Changing any of FileName/ReportName/Parameters would
    /// normally result in a new document being generated on the server.
    /// The presence of ParamInfos here is patchy - those are NOT used to id a document.
    /// </summary>
    internal class DocIdentity
    {
        /// <summary>
        /// Gets the report file name.
        /// </summary>
        public string FileName { get; private set; }
        /// <summary>
        /// Gets the report name.
        /// </summary>
        public string ReportName { get; private set; }
        /// <summary>
        /// Gets parameter values for the report.
        /// </summary>
        public ReportParameterValue[] Parameters { get; private set; }
        /// <summary>
        /// Gets or sets the list of parameter infos that may be used to build UI for parameter input.
        /// </summary>
        public List<ReportParameterInfo> ParamInfos { get; set; }

		public DocIdentity(string fileName, string reportName, ReportParameterValue[] paramValues)
        {
			if (fileName.StartsWith("~/") && HttpContext.Current != null && HttpContext.Current.Server != null)
			{
				// fix for problems with document key generation
				fileName = HttpContext.Current.Server.MapPath(fileName);
			}
			FileName = fileName;
            ReportName = reportName;
            Parameters = paramValues;
            ParamInfos = null;
        }

        public bool HasParameters { get { return Parameters != null && Parameters.Length > 0; } }
    }

    /// <summary>
    /// Used to pass error information from generating, zipping or other
    /// threads to the UI thread.
    /// Use the static thread-safe methods to add and retrieve errors.
    /// </summary>
    internal class ThreadedErrors
    {
        private List<string> _errors = null;
        private ThreadedErrors()
        {
            _errors = new List<string>();
        }
        public static void PushError(HttpContext context, string documentKey, string error)
        {
            string key = KeyMaker.ThreadedErrorsKey(documentKey);
            lock (typeof(ThreadedErrors))
            {
                ThreadedErrors te = CacheHandler.GetThreadedErrors(key, context);
                if (te == null)
                {
                    te = new ThreadedErrors();
                    CacheHandler.Add(key, te, context);
                }
                te._errors.Add(error);
            }
        }
        /// <summary>
        /// Pops all current errors.
        /// If there are no errors, this method returns null.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="documentKey"></param>
        /// <returns></returns>
        public static string[] PopAllErrors(HttpContext context, string documentKey)
        {
            string key = KeyMaker.ThreadedErrorsKey(documentKey);
            lock (typeof(ThreadedErrors))
            {
                ThreadedErrors te = CacheHandler.GetThreadedErrors(key, context);
                if (te == null || te._errors.Count == 0)
                    return null;
                string[] errors = te._errors.ToArray();
                te._errors.Clear();
                return errors;
            }
        }

        /// <summary>
        /// Pops all errors into a single string, using the current context.
        /// </summary>
        /// <param name="documentKey"></param>
        /// <returns></returns>
        public static string PopErrors(string documentKey)
        {
            Debug.Assert(HttpContext.Current != null);

            string[] errs = PopAllErrors(HttpContext.Current, documentKey);
            if (errs == null)
                return null;
            StringBuilder sb = new StringBuilder();
            foreach (string err in errs)
            {
                sb.Append(err);
                sb.Append('\n');
            }
            return sb.ToString();
        }
    }

    /// <summary>
    /// FIFO queue for page requests.
    /// Page requests are pushed onto the queue by client requests processing thread(s).
    /// Page requests are popped from the queue by the generating thread - when a page
    /// is popped it is added to the page zip.
    /// </summary>
    internal class PageRequestQueue
    {
        // requested page indices
        private List<int> _requests = new List<int>();
        // used to keep track of cached pages and their generations
        // (key is page index, int value is cached page generation):
        private Dictionary<int, int> _cached = new Dictionary<int, int>();

        private static PageRequestQueue GetQueue(HttpContext context, string documentKey)
        {
            string key = KeyMaker.PageRequestsQueueKey(documentKey);
            PageRequestQueue queue = CacheHandler.GetPageRequestQueue(key, context);
            if (queue == null)
            {
                queue = new PageRequestQueue();
                CacheHandler.Add(key, queue, context);
            }
            return queue;
        }

        public static void DeleteQueue(HttpContext context, string documentKey)
        {
            string key = KeyMaker.PageRequestsQueueKey(documentKey);
            CacheHandler.Remove(key, context);
        }

        public static void Push(HttpContext context, string documentKey, int pageIndex)
        {
            lock (typeof(PageRequestQueue))
            {
                PageRequestQueue queue = GetQueue(context, documentKey);
                if (!queue._requests.Contains(pageIndex) && !queue._cached.ContainsKey(pageIndex))
                    queue._requests.Add(pageIndex);
            }
        }

        public static int[] GetQueuedPages(HttpContext context, string documentKey)
        {
            lock (typeof(PageRequestQueue))
            {
                PageRequestQueue queue = GetQueue(context, documentKey);
                return queue._requests.ToArray();
            }
        }

        public static bool IsPageCached(HttpContext context, string documentKey, int pageIndex, int generation)
        {
            lock (typeof(PageRequestQueue))
            {
                PageRequestQueue queue = GetQueue(context, documentKey);
                return queue._cached.ContainsKey(pageIndex) && queue._cached[pageIndex] == generation;
            }
        }

        /// <summary>
        /// Should be called prior to adding a page to the cache.
        /// Return value indicates whether the page needs to be added.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="documentKey">The document key.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="generation">The generation.</param>
        /// <returns></returns>
        public static bool AddingCachedPage(HttpContext context, string documentKey, int pageIndex, int generation)
        {
            lock (typeof(PageRequestQueue))
            {
                PageRequestQueue queue = GetQueue(context, documentKey);
                if (queue._cached.ContainsKey(pageIndex) && queue._cached[pageIndex] == generation)
                    return false;
                queue._requests.Remove(pageIndex);
                queue._cached[pageIndex] = generation;
                return true;
            }
        }
    }
}
