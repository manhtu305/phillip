﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Caching;
using System.Diagnostics;
using System.IO;

#if WIJMOCONTROLS
namespace C1.Web.Wijmo.Controls.C1ReportViewer.ReportService
#else
namespace C1.Web.UI.Controls.C1Report.ReportService
#endif
{
    internal static class CacheHandler
    {
        #region CachedDocument
        public static void Add(CachedDocument cdoc, ReportCache reportCache)
        {
            Debug.Assert(HttpContext.Current != null);

            lock (cdoc.SyncRoot)
            {
                Debug.Assert(HttpContext.Current != null);
                Debug.Assert(HttpContext.Current.Cache.Get(cdoc.Key) == null);

                // handle expiration policy
                DateTime abs = DateTime.MaxValue;
                TimeSpan slide = TimeSpan.Zero;
                if (reportCache.Sliding)
                {
                    abs = DateTime.MaxValue;
                    slide = TimeSpan.FromMinutes(reportCache.Expiration);
                }
                else
                {
                    abs = DateTime.Now.Add(TimeSpan.FromMinutes(reportCache.Expiration));
                    slide = TimeSpan.Zero;
                }

                // handle file dependencies:
                CacheDependency dep = new CacheDependency(cdoc.FileName);

                // store it
                HttpContext.Current.Cache.Insert(cdoc.Key, cdoc, dep, abs, slide, reportCache.Priority, CacheDocumentRemovedCallback);
            }
        }

        public static CachedDocument GetCachedDocument(string documentKey)
        {
            return GetCachedDocument(documentKey, HttpContext.Current);
        }

        public static CachedDocument GetCachedDocument(string documentKey, HttpContext context)
        {
            try
            {
                return context.Cache.Get(documentKey) as CachedDocument;
            }
            catch
            {
                return null;
            }
        }

        public static CachedDocument RemoveCachedDocument(string documentKey)
        {
            return RemoveCachedDocument(documentKey, HttpContext.Current);
        }

        public static CachedDocument RemoveCachedDocument(string documentKey, HttpContext context)
        {
            try
            {
                return context.Cache.Remove(documentKey) as CachedDocument;
            }
            catch
            {
                return null;
            }
        }

        private static void CacheDocumentRemovedCallback(string key, object value, CacheItemRemovedReason reason)
        {
            CachedDocument cdoc = value as CachedDocument;
            // Debug.WriteLine(string.Format("--- CacheDocumentRemovedCallback key: {0}, file: {1}", cdoc.DocumentKey, cdoc.PageZipPath));
            Debug.Assert(cdoc != null);
            ServiceCache.LogWarning(string.Format("CacheDocumentRemovedCallback: deleting work directory {0}...", cdoc.WorkDir));
            try
            {
                lock (cdoc.SyncRoot)
                    Util.DeleteWorkDir(cdoc.WorkDir, true);
                ServiceCache.LogWarning(string.Format("CacheDocumentRemovedCallback: deleted work directory {0} - DONE.", cdoc.WorkDir));
            }
            catch
            {
                ServiceCache.LogError(string.Format("Could not remove work directory: {0} (FileName: {1}, ReportName: {2})", cdoc.WorkDir, cdoc.FileName, cdoc.ReportName));
                throw;
            }
        }
        #endregion

        #region CachedGeneratingDocument
        public static void Add(string key, CachedGeneratingDocument gdoc)
        {
            Debug.Assert(HttpContext.Current != null);
            CacheDependency cd = new CacheDependency(null, new string[] { gdoc.DocumentKey });
            HttpContext.Current.Cache.Insert(key, gdoc, cd, Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration,
                CacheItemPriority.NotRemovable, null);
        }

        public static CachedGeneratingDocument GetGeneratingDocument(string generatingDocumentKey, HttpContext context)
        {
            return context.Cache.Get(generatingDocumentKey) as CachedGeneratingDocument;
        }
        #endregion

        #region CachedC1Doc
        public static void Add(string key, CachedC1DocHolder cDocHolder, CachedDocument cdoc, HttpContext context)
        {
            CacheDependency cd = new CacheDependency(null, new string[] { cdoc.Key });
            context.Cache.Insert(key, cDocHolder, cd, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(Constants.CacheC1DocSlidingExpireMin),
                CacheItemPriority.Default, null);
        }

        public static CachedC1DocHolder GetCachedC1DocHolder(string key)
        {
            Debug.Assert(HttpContext.Current != null);
            return HttpContext.Current.Cache.Get(key) as CachedC1DocHolder;
        }

        public static CachedC1DocHolder RemoveCachedC1DocHolder(string key)
        {
            Debug.Assert(HttpContext.Current != null);
            return HttpContext.Current.Cache.Remove(key) as CachedC1DocHolder;
        }
        #endregion

        #region changed pages cookies
        public static void Add(string key, string documentKey, List<CachedDocument.PageGenRun> runs)
        {
            Debug.Assert(HttpContext.Current != null);
            CacheDependency cd = new CacheDependency(null, new string[] { documentKey });
            HttpContext.Current.Cache.Insert(key, runs, cd, Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(Constants.CacheChangedPageCookiesMin),
                CacheItemPriority.NotRemovable, null);
        }

        public static List<CachedDocument.PageGenRun> GetPageGenRuns(string key)
        {
            Debug.Assert(HttpContext.Current != null);
            return HttpContext.Current.Cache.Get(key) as List<CachedDocument.PageGenRun>;
        }
        #endregion

        #region ThreadedErrors
        public static void Add(string key, ThreadedErrors te, HttpContext context)
        {
            // todo: think about expiration rules for this:
            context.Cache.Insert(key, te, null, Cache.NoAbsoluteExpiration, TimeSpan.FromHours(1), CacheItemPriority.High, null);
        }

        public static ThreadedErrors GetThreadedErrors(string key, HttpContext context)
        {
            return context.Cache.Get(key) as ThreadedErrors;
        }
        #endregion

        #region PageRequestQueue
        public static void Add(string key, PageRequestQueue prq, HttpContext context)
        {
            // todo: think about expiration rules for this:
            context.Cache.Insert(key, prq, null, Cache.NoAbsoluteExpiration, TimeSpan.FromHours(1), CacheItemPriority.High, null);
        }

        public static PageRequestQueue GetPageRequestQueue(string key, HttpContext context)
        {
            return context.Cache.Get(key) as PageRequestQueue;
        }
        #endregion

        #region CachedPageImage
        public static void Add(CachedPageImage cpi, string documentKey, string cachedDocumentPageKey)
        {
            // CachedPageImage are cached to ensure that image files are deleted when they expire
            // (which they do when their cached document expires).
            Debug.Assert(HttpContext.Current != null);
            Debug.Assert(File.Exists(cpi.FilePath));
            CacheDependency cd = new CacheDependency(null, new string[] { documentKey, cachedDocumentPageKey });
            HttpContext.Current.Cache.Insert(cpi.FilePath, cpi, cd, Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration,
                CacheItemPriority.NotRemovable, CachePageImageRemovedCallback);
        }

        public static CachedPageImage GetCachedPageImage(string key)
        {
            Debug.Assert(HttpContext.Current != null);
            CachedPageImage cpi = HttpContext.Current.Cache.Get(key) as CachedPageImage;
            Debug.Assert(cpi == null || File.Exists(cpi.FilePath));
            return cpi;
        }

        private static void CachePageImageRemovedCallback(string key, object value, CacheItemRemovedReason reason)
        {
            CachedPageImage cpi = value as CachedPageImage;
            Debug.Assert(cpi != null && key == cpi.FilePath);
            Util.DeleteWorkFile(cpi.FilePath);
        }
        #endregion

        #region CachedC1d
        public static void Add(CachedC1d cc1d, string documentKey, HttpContext context)
        {
            Debug.Assert(context != null);
            Debug.Assert(File.Exists(cc1d.FilePath));
            CacheDependency cd = new CacheDependency(null, new string[] { documentKey });
            context.Cache.Insert(cc1d.FilePath, cc1d, cd, Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration,
                CacheItemPriority.NotRemovable, CachedC1dRemovedCallback);
        }

#if not_needed
        public static CachedC1d GetCachedC1d(string key)
        {
            Debug.Assert(HttpContext.Current != null);
            CachedC1d cc1d = HttpContext.Current.Cache.Get(key) as CachedC1d;
            Debug.Assert(cc1d == null || File.Exists(cc1d.FilePath));
            return cc1d;
        }
#endif

        private static void CachedC1dRemovedCallback(string key, object value, CacheItemRemovedReason reason)
        {
            CachedC1d cpi = value as CachedC1d;
            Debug.Assert(cpi != null && key == cpi.FilePath);
            // if we fail to obtain a write lock, we just leave the file alone...
            if (cpi.Lock.TryEnterWriteLock(1000))
            {
                try
                {
                    Util.DeleteWorkFile(cpi.FilePath);
                }
                finally
                {
                    cpi.Lock.ExitWriteLock();
                }
            }
        }
        #endregion

        #region CachedDocumentPage - used to remove stale pages when their generation increases
        public static void Add(string cachedDocumentPageKey, string documentKey, HttpContext context)
        {
            CacheDependency cd = new CacheDependency(null, new string[] { documentKey });
            context.Cache.Insert(cachedDocumentPageKey, cachedDocumentPageKey, cd, Cache.NoAbsoluteExpiration, Cache.NoSlidingExpiration,
                CacheItemPriority.NotRemovable, null);
        }
        #endregion

        #region util
        public static void Remove(string key, HttpContext context)
        {
            context.Cache.Remove(key);
        }
        #endregion
    }
}
