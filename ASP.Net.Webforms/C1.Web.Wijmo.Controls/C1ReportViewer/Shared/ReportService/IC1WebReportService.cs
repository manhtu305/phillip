﻿using System;
using System.Collections.Generic;

#if WIJMOCONTROLS
namespace C1.Web.Wijmo.Controls.C1ReportViewer.ReportService
#else
namespace C1.Web.UI.Controls.C1Report
#endif
{
	
	/// <summary>
	/// Interface that must implemented by C1WebReport service.
	/// </summary>
#if JAVASCRIPT
	[Imported]
#endif
	public interface IC1WebReportService
	{
		/// <summary>
		/// Retrieves a document's or report's status by file/report/parameters.
		/// </summary>
		/// <param name="fileName">Document or report file name.</param>
		/// <param name="reportName">Report name.</param>
		/// <param name="paramValues">Report parameter values.</param>
		/// <param name="cookie">Arbitrary string that was passed back from the service in DocumentStatus
		/// in the previous GetDocumentStatus call, or null if there was no previous call.</param>
		/// <returns>
		/// A <see cref="DocumentStatus"/> instance containing information about the requested document.
		/// </returns>
		DocumentStatus GetDocumentStatus(string fileName, string reportName, ReportParameterValue[] paramValues, string cookie);

		/// <summary>
		/// Gets a key that can be used to access a cached document.
		/// </summary>
		/// <param name="fileName"></param>
		/// <param name="reportName"></param>
		/// <param name="paramValues"></param>
		/// <returns>The document key.</returns>
		string GetDocumentKey(string fileName, string reportName, ReportParameterValue[] paramValues);


		/// <summary>
		/// Gets the document/report page images markup.
		/// </summary>
		/// <param name="documentKey">Cached document key.</param>
		/// <param name="dpi">Client screen resolution.</param>
		/// <param name="zoom">Zoom, in percent (100 is actual size).</param>
		/// <param name="pageIndices">The page indices that must be loaded.</param>
		/// <param name="getImagesOnly">if set to <c>true</c> this method will generate page images only without information about document text.</param>
		/// <returns></returns>
		PageImagesMarkup GetPageImagesMarkup(string documentKey, int dpi, int zoom, int[] pageIndices, bool getImagesOnly);

		/// <summary>
		/// Gets the HTML report markup.
		/// </summary>
		/// <param name="documentKey">Cached document key.</param>
		/// <returns></returns>
		string GetUnpagedHtmlMarkup(string documentKey);

		/// <summary>
		/// Gets the page HTML markup.
		/// </summary>
		/// <param name="documentKey">Cached document key.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <returns>Returns HTML markup for given pageIndex.</returns>
		string GetPageHtmlMarkup(string documentKey, int pageIndex);

		/// <summary>
		/// Gets the document outlines' tree.
		/// </summary>
		/// <param name="documentKey">Cached document key.</param>
		/// <returns>The tree of document outline entries.</returns>
		DocumentOutline GetDocumentOutline(string documentKey);

		/// <summary>
		/// Searches the text.
		/// </summary>
		/// <param name="documentKey">Cached document key.</param>
		/// <param name="query">The search query.</param>
		/// <param name="caseSensitive">if set to <c>true</c> search will be case sensitive.</param>
		/// <param name="useRegExp">if set to <c>true</c> query text will be recognized as a regular expression.</param>
		/// <returns>The Search Results.</returns>
		SearchResults SearchText(string documentKey, string query, bool caseSensitive, bool useRegExp);

		/// <summary>
		/// Gets the page image.
		/// </summary>
		/// <param name="documentKey">Cached document key.</param>
		/// <param name="dpi">The dpi.</param>
		/// <param name="zoom">The zoom.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="printTarget">Indicates that the pages are requested for printing.</param>
		/// <returns>Raw image data.</returns>
		byte[] GetPageImage(string documentKey, int dpi, int zoom, int pageIndex, bool printTarget);

		/// <summary>
		/// Gets the page image url array.
		/// </summary>
		/// <param name="documentKey">Cached document key.</param>
		/// <param name="dpi">The dpi.</param>
		/// <param name="zoom">The zoom.</param>
		/// <returns>Image Url Array</returns>
		string[] GetPageImageUrls(string documentKey, int dpi, int zoom);

		/// <summary>
		/// Export the document specified by documentKey to the specified format.
		/// </summary>
		/// <param name="documentKey">The document key.</param>
		/// <param name="format">The format.</param>
		/// <returns>Document data.</returns>
		byte[] Export(string documentKey, string format);

		/// <summary>
		/// Export the document specified by documentKey to the specified format and returns url that can be used to download file.
		/// </summary>
		/// <param name="documentKey">The document key.</param>
		/// <param name="format">The format.</param>
		/// <returns>Url to exported file.</returns>
		string ExportToFile(string documentKey, string format);

		/// <summary>
		/// Export the document specified by documentKey to the specified format and returns url that can be used to download file.
		/// </summary>
		/// <param name="documentKey">The document key.</param>
		/// <param name="format">The format.</param>
		/// <param name="exportedFileName">Optional. Exported file name.</param>
		/// <returns>Url to exported file.</returns>
		string ExportToFile(string documentKey, string format, string exportedFileName);

        /// <summary>
        /// Export the document specified by documentKey to the specified format and returns url that can be used to download file.
        /// </summary>
        /// <param name="documentKey">The document key.</param>
        /// <param name="format">The format.</param>
        /// <param name="exportedFileName">Optional. Exported file name.</param>
        /// <param name="extraOptions">Extra Options: Pdf security setting.</param>
        /// <returns>Url to exported file.</returns>
        string ExportToFile(string documentKey, string format, string exportedFileName, Dictionary<string, object> extraOptions);

		/// <summary>
		/// Checks whether a previously cached report or document is available.
		/// </summary>
		/// <param name="fileName">Report or document file name.</param>
		/// <param name="reportName">Report name.</param>
		/// <returns><c>true</c> if a cached report or document is available, <c>false</c> otherwise.</returns>
		bool HasCachedDocument(string fileName, string reportName);

		/// <summary>
		/// Checks whether a previously cached report or document is available.
		/// </summary>
		/// <param name="fileName">Report or document file name.</param>
		/// <param name="reportName">Report name.</param>
		/// <param name="paramValues">Report parameters or null.</param>
		/// <returns><c>true</c> if a cached report or document is available, <c>false</c> otherwise.</returns>
		bool HasCachedDocument(string fileName, string reportName, ReportParameterValue[] paramValues);

		// todo: is this the right place?
		/// <summary>
		/// Tells the service that the document has changed.
		/// </summary>
		/// <param name="newDocument">The new document value.</param>
		void OnDocumentChanged(object newDocument);
	}

	#region ** enumerations

	/// <summary>
	/// Enumerates possible report states.
	/// </summary>
	public enum GeneratingState
	{
		/// <summary>
		/// Report is completely generated.
		/// </summary>
		Ready = 0,
		/// <summary>
		/// Report is currently generating, total page count may change.
		/// </summary>
		GeneratingPages = 1,
		/// <summary>
		/// Report is still generating, individual pages may change but total page count is final.
		/// </summary>
		UpdatingPages = 2,
		/// <summary>
		/// Report needs parameters, status contains ParamInfos describing the requested parameters.
		/// All other fields in status are not initialized.
		/// </summary>
		ParametersRequested = 3,
	}

	/// <summary>
	/// Enumerates possible report view types.
	/// </summary>
	public enum ViewType
	{
		/// <summary>
		/// Paged Images.
		/// </summary>
		PageImages = 0,
		/// <summary>
		/// Unpaged Html.
		/// </summary>
		UnpagedHtml = 1
	}

	/// <summary>
	/// Print Range Subset.
	/// </summary>
	public enum PrintRangeSubset
	{
		/// <summary>
		/// All pages.
		/// </summary>
		All = 0,
		/// <summary>
		/// Odd pages only.
		/// </summary>
		Odd = 1,
		/// <summary>
		/// Even pages only.
		/// </summary>
		Even = 2
	}

	#endregion

	#region ** data classes

	/// <summary>
	/// CommonReport Information.
	/// </summary>
#if JAVASCRIPT
	[Imported]
#endif
	public class DocumentStatus
	{
		/// <summary>
		/// Unique key identifying a cached document.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string documentKey;

		/// <summary>
		/// Export formats available for current document.
		/// The list is a sequence of pairs: export format description/default extension for that format.
		/// <para>The description should uniquely identify the format within the list.</para>
		/// <para>The extension must be lowercase and should not include the dot (e.g. "pdf").</para>
		/// <para>This member contains data only when the document is not generating (<see cref="isGenerating"/> is <c>false</c>).</para>
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string[] exportFormats;

		/// <summary>
		/// Indicates whether current report is generating and report information is not available.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public bool isGenerating;

		/// <summary>
		/// The current report generating state (ready, pages are being added, or pages are being updated).
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public GeneratingState state;

		/// <summary>
		/// If report is generating, gets the approximate percent of work complete, from 0 to 100.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif        
		public int percentComplete;

		/// <summary>
		/// Report name
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string name;

		/// <summary>
		/// Date created.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public DateTime created;

		/// <summary>
		/// Total page count.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int pageCount;

		/// <summary>
		/// View type (page images or HTML).
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public ViewType type;

		/// <summary>
		/// Default page width in pixels for Zoom 100% and 96 DPI.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int w;

		/// <summary>
		/// Default page width in pixels for Zoom 100% and 96 DPI.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int h;

		/// <summary>
		/// Indices of pages that changed since the last GetDocumentStatus call
		/// (calls are tracked via the cookie field).
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int[] changedPages;

		/// <summary>
		/// Array of intervals of pages with the same size.
		/// The intervals are guaranteed to be sorted by page indices,
		/// and to cover all pages from 0 to pageCount.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public PageRangeSizeInfo[] pageSizes;

		/// <summary>
		/// Cookie data that generated by C1Report service.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string cookie;

		/// <summary>
		/// If an error occurred on the server, contains the error description.
		/// Normally should be null.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string error;

		/// <summary>
		/// The array of REQUIRED report parameters, filled in by the server.
		/// 
		/// May be null, BUT IF this is non-null on return from server,
		/// the client must use this info to fill the param name/param value list,
		/// and pass it to the server in order to generate the document.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public ReportParameterInfo[] reportParams;

	}


	/// <summary>
	/// Info that is used to build a UI needed to input a report parameter.
	/// </summary>
#if JAVASCRIPT
	[Imported]
#endif
	public class ReportParameterInfo
	{
		/// <summary>
		/// Parameter name. This is used as the key identifying the parameter value.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string n;

		/// <summary>
		/// Parameter UI prompt.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string p;

		/// <summary>
		/// The initial parameter value or values.
		/// An example of multiple values parameter: select days of week,
		/// UI should show a list of Mon/Tue/..., with a checkbox near each day,
		/// and some days selected initially - those would be ones specified here.
		/// May be null.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public object[] vs;

		/// <summary>
		/// Parameter type name.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string t;

		/// <summary>
		/// The list of possible values. Should be used to populate dropdowns,
		/// radiobuttons and the like. This may be null.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public ReportParameterSingleValue[] pvs;

		/// <summary>
		/// Flag indicating whether the list of possible values is exclusive
		/// (i.e. input must be limited to those values only).
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public bool vx;

		/// <summary>
		/// Flag indicating whether the parameter is required
		/// (must be filled in by user).
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public bool rq;

		/// <summary>
		/// Flag indicating whether the parameter may be null.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public bool nu;

		/// <summary>
		/// Flag indicating whether the parameter can accept multiple values.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public bool mv;
	}


	/// <summary>
	/// Represents a single value of a parameter, together with that value's label.
	/// The label may be used to represent the parameter in the UI.
	/// E.g. for weekdays, integer values (0..6) may be used, with
	/// corresponding labels "Mon".."Sun".
	/// </summary>
#if JAVASCRIPT
	[Imported]
#endif
	public class ReportParameterSingleValue
	{
		/// <summary>
		/// Parameter value.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public object v;

		/// <summary>
		/// Parameter label. This may be null,
		/// in which case v.ToString() should be used as the label.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string l;
	}


	/// <summary>
	/// Parameter and its value passed from client to server.
	/// </summary>
#if JAVASCRIPT
	[Imported]
#endif
	public class ReportParameterValue
	{
		/// <summary>
		/// Parameter name. Corresponds to ReportParameterInfo.n.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string n;

		/// <summary>
		/// The parameter value or values.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public object[] vs;
	}


	/// <summary>
	/// PageImagesMarkup data class.
	/// </summary>
#if JAVASCRIPT
	[Imported]
#endif
	public class PageImagesMarkup
	{
		/// <summary>
		/// Indicates whether current report is generating and report information is not available.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public bool isGenerating;

		/// <summary>
		/// Unique key identifying a cached document.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string documentKey;

		/// <summary>
		/// Array of PageImage Markups.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public PageImageMarkup[] pages;
		
		/// <summary>
		/// Zoom factor for which the markup was generated.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int zoom;

		/// <summary>
		/// Error description.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string error;
	}

	/// <summary>
	/// PageImageMarkup data class.
	/// </summary>
#if JAVASCRIPT
	[Imported]
#endif
	public class PageImageMarkup
	{
		
		/// <summary>
		/// Index of the page in the document (0-based).
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int idx;

		/// <summary>
		/// Page width (pixels).
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int w;

		/// <summary>
		/// Page height (pixels).
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int h;

		/*
		// Note, imageUrl is no longer used, Images loaded dynamically using http handler.
		/// <summary>
		/// Page image url.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string imageUrl;
		*/
		/// <summary>
		/// Array of page text run descriptors.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public TextRunMarkup[] texts;

		/// <summary>
		/// Array of Hyperlinks that located on this page. Can be null if page does not contains hyperlinks.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public HyperlinkEntry[] links;

	}

	/// <summary>
	/// Describes a text run (a single line of text sharing the same formatting).
	/// A text run may contain zero or more words (semantics of words are defined
	/// by the server; in particular, words may be single chars).
	/// </summary>
#if JAVASCRIPT
	[Imported]
#endif
	public class TextRunMarkup
	{
		/// <summary>
		/// The X coordinate of the text run relative to the left edge of the page, in pixels.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int x;

		/// <summary>
		/// The Y coordinate of the text run relative to the top edge of the page, in pixels.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int y;

		/// <summary>
		/// The width of the text run, in pixels.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int w;

		/// <summary>
		/// The height of the text run, in pixels.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int h;

		/// <summary>
		/// Array containing X coordinates of words' first chars, relative to
		/// the left bound of the text run. May be null or have 0 elements.
		/// If the first element is present, it would normally contain 0
		/// i.e. the X coordinate of the first char of the first word in the text run.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int[] p;

		/// <summary>
		/// Array containing 0-based indices of words' first chars into the "text" string.
		/// If the "p" array is null or empty, this array must also be null or empty.
		/// If the "p" array is not empty, and this array has exactly one element "-1",
		/// the "p" array is assumed to contain coordinates of all individual chars in "text".
		/// In all other cases, this array must have the same number of elements as the "p" array,
		/// and must contain indices into "text" string of subsequent words' first chars.
		/// In that case the first element of this array must contain "0" (index of the first char
		/// of the first word in the text run). The length of a word is determined by
		/// the starting indices of that and next word.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int[] ix;

		/// <summary>
		/// The text of the text run. May not contain line breaks.
		/// </summary>
#if ASP_NET_2&&!JAVASCRIPT
		[Json(true)]
#endif
		public string text;
	}

	/// <summary>
	/// OutlineEntry data class.
	/// </summary>
#if JAVASCRIPT
	[Imported]
#endif
	public class OutlineEntry
	{

		/// <summary>
		/// Text for the outline entry.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string text;
		/// <summary>
		/// 0-based index of page associated with this outline entry.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int p;

		/// <summary>
		/// Array of page text run descriptors that contains coordinates 
		/// of the text to be focused. Can not be null.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public TextRunMarkup[] tr;

		/// <summary>
		/// Array of child outline entries. May be null for leaf nodes.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public OutlineEntry[] oes;
	}

	/// <summary>
	/// DocumentOutline data class.
	/// </summary>
#if JAVASCRIPT
	[Imported]
#endif
	public class DocumentOutline
	{
		/// <summary>
		/// Unique key identifying a cached document.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string documentKey;

		/// <summary>
		/// Indicates whether current report is generating and report information is not available.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public bool isGenerating;
		
		/// <summary>
		/// Array of top-level outline entries associated with the document.
		/// May be null if there are no outlines in the document.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public OutlineEntry[] oes;

		/// <summary>
		/// Error description.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string error;
	}

	/// <summary>
	/// Search Results.
	/// </summary>
#if JAVASCRIPT
	[Imported]
#endif
	public class SearchResults
	{
		/// <summary>
		/// Unique key identifying a cached document.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string documentKey;

		/// <summary>
		/// Indicates whether current report is generating and report information is not available.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public bool isGenerating;

		/// <summary>
		/// Query text that was used for search.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string query;

		/// <summary>
		/// Search matches count. 0 if nothing found, otherwise this values equals ses array length.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int count;

		/// <summary>
		/// Array of search entries.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public SearchEntry[] ses;

		/// <summary>
		/// Error description.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string error;

	}

	/// <summary>
	/// Describes a hyperlink on a page of a document.
	/// </summary>
#if JAVASCRIPT
	[Imported]
#endif
	public class HyperlinkEntry
	{
		/// <summary>
		/// The hyperlink text. This is usually displayed in the browser's status when
		/// the mouse is over this hyperlink.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string text;

		/// <summary>
		/// 0-based index of the page containing this hyperlink.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int p;

		/// <summary>
		/// Array of page text run descriptors that contains coordinates 
		/// of the text. Can not be null.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public TextRunMarkup[] tr; //Note, text can be located on several text lines, so we need array here.

		/// <summary>
		/// Action that will be executed when hyperlink is clicked by user, can be null.
		/// Action examples:
		/// "http://google.com'", url to external page that will be opened in current window (user can use ctrl+click in order to open hyperlink in the new window)
		/// "javascript:some_javascript code", e.g. "javascript:alert('hello world')"        
		/// "exec:somemethod(parame, parame2)", executes C1ReportViewer method, e.g. "exec:exportToFile('Adobe PDF','pdf')"
		/// "moveto:p,i", where p - page index, i - index of the TextRunMarkup within tr array of the page given by index p , report view will be scrolled to given place", e.g. "javascript:alert('moveto:5,10')"
		/// todo: add moveTo method for the C1ReportViewer client side object and use it along with exec action
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string a;
	}

	/// <summary>
	/// Search Entry. This class is used by search tool.
	/// </summary>
#if JAVASCRIPT
	[Imported]
#endif
	public class SearchEntry
	{
		/// <summary>
		/// Text that was found.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public string text;

		/// <summary>
		/// Page index where text is located.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int p;

		/// <summary>
		/// Array of page text run descriptors that contains coordinates 
		/// of the text. Can not be null.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public TextRunMarkup[] tr; //Note, text can be located on several text lines, so we need array here.
	}

	/// <summary>
	/// PageSetSize.
	/// </summary>
#if JAVASCRIPT
	[Imported]
#endif
	public class PageRangeSizeInfo
	{
		/// <summary>
		/// Start page index. This index will be included into current set.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int s;

		/// <summary>
		/// End page index. This index will be included into current set.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int e;

		/// <summary>
		/// Page width in pixels for Zoom 100% and 96 DPI.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int w;

		/// <summary>
		/// Page height in pixels for Zoom 100% and 96 DPI.
		/// </summary>
#if JAVASCRIPT
		[PreserveCase]
#elif ASP_NET_2
		[Json(true)]
#endif
		public int h;
	}

	#endregion
}

