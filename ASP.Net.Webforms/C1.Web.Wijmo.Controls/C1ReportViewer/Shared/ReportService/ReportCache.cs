// todo: this should be user interface to ServiceCashe
using System;
using System.IO;
using System.Collections;
using System.Drawing;
using System.Drawing.Design;
using System.Drawing.Printing;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Web;
using System.Web.UI;
using System.Web.Caching;
using System.Runtime.Serialization;
using System.ComponentModel;
using System.Globalization;
using System.Diagnostics;
using System.Xml;

using C1.C1Report;
using C1.C1Preview;

#if WIJMOCONTROLS
using C1.Web.Wijmo.Controls.Localization;
#else
using C1.Web.UI.Controls.Localization;
using C1.Web.UI.Localization;
#endif


#if WIJMOCONTROLS
namespace C1.Web.Wijmo.Controls.C1ReportViewer.ReportService
#else
namespace C1.Web.UI.Controls.C1Report.ReportService
#endif
{
    
    /// <summary>
    /// Reports cache implementation (controlled by web report's Cache property or by service).
    /// </summary>
    public class ReportCache : C1BaseStateManager
	{

        #region ** properties

        /// <summary>
        /// Specifies whether the control should store rendered reports in the cache.
        /// </summary>
        /// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
        /// <remarks>
        /// 	<para>This property is set to true by default, allowing the <see cref="C1ReportViewer"/> to use
        /// the <see cref="Page.Cache"/> object to store frequently used reports. This enhances
        /// performance significantly, but it does use some server resources.</para>
        /// 	<para>If you want to reduce the memory requirements on the server, setting the
        /// <see cref="Enabled"/> property to false will cause the <see cref="C1ReportViewer"/> control
        /// not to use the cache. Reports will be rendered every time they are requested, and memory
        /// usage will be reduced at the expense of more data access and processing time needed to
        /// create the reports multiple times.</para>
        /// 	<para>In general, you should only turn the cache off if you are rendering a custom report
        /// that probably won't be requested again in the next few hours.</para>
        /// </remarks>
        [DefaultValue(true)]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [C1Category("Category.Behavior")]
        [C1Description("C1ReportViewer.Cache.Enabled", "Specifies whether the control should store rendered reports in the cache.")]
        public bool Enabled
        {
            get { 
                return this.ViewState["Enabled"] == null ? true : (bool)this.ViewState["Enabled"]; 
            }
            set {
                this.ViewState["Enabled"] = value;
            }
        }

		/// <summary>
		/// Gets or sets the number of minutes that elapse before reports are discarded from the cache.
		/// </summary>
		/// <remarks>
		/// <para>The default value for this property is 30, causing reports to be stored in the cache for 
		/// 30 minutes before they are discarded. You can use the <see cref="Sliding"/> property to 
		/// determine whether this is an absolute or a "sliding" limit (meaning the interval is reset 
		/// every time the report in the cache is reused).</para>
		/// <para>The amount of time reports remain in the cache is also affected by file dependencies.
		/// By default, changes in the report definition file cause cached reports to be discarded.</para>
		/// </remarks>
        [DefaultValue(30)]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [C1Category("Category.Behavior")]
        [C1Description("C1ReportViewer.Cache.Expiration", "Gets or sets the number of minutes that elapse before cached reports are discarded.")]
        public int Expiration
        {
            get {
                return this.ViewState["Expiration"] == null ? 30 : (int)this.ViewState["Expiration"]; 
            }
            set {
                this.ViewState["Expiration"] = value; 
            }
        }

        		/// <summary>
		/// Gets or sets the cost of reports relative to other objects in the cache.
		/// </summary>
		/// <remarks>
		/// <para>If your application stores many types of object in the cache, you may want to set 
		/// the <see cref="Priority"/> property to reflect the relative cost of each type of item. 
		/// For example, you may be generating technical diagrams that take a long time to create, 
		/// and should have a higher cache priority than simple reports. Or you may be caching 
		/// customer information that is easy to retrieve from the database and should have a 
		/// lower priority.</para>
		/// <para>For more details on this property, and a list of valid settings, see the 
		/// <see cref="Cache.Insert(string, object)"/> method in the .NET framework documentation.</para>
		/// </remarks>
        [DefaultValue(CacheItemPriority.Default)]
        [NotifyParentProperty(true)]
        [C1Category("Category.Behavior")]
        [C1Description("C1ReportViewer.Cache.Priority", "Gets or sets the cost of reports relative to other objects in the cache.")]
        public CacheItemPriority Priority
        {
            get { 
                return this.ViewState["Priority"] == null ? CacheItemPriority.Default : (CacheItemPriority)this.ViewState["Priority"]; 
            }
            set { 
                this.ViewState["Priority"] = value;
            }
        }

        

		/// <summary>
        /// Gets or sets a value indicating whether cached reports and documents are shared between different sessions.
        /// The default value is true, which means that different browser sessions will be able to access the same cached report or document.
        /// Set to false to prevent such sharing.
		/// </summary>
        [DefaultValue(true)]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [C1Category("Category.Behavior")]
        [C1Description("C1ReportViewer.Cache.ShareBetweenSessions", "Gets or sets a value indicating whether cached reports and documents are shared between different sessions.")]
        public bool ShareBetweenSessions
        {
            get {
                return this.ViewState["ShareBetweenSessions"] == null ? true : (bool)this.ViewState["ShareBetweenSessions"]; 
            }
            set {
                this.ViewState["ShareBetweenSessions"] = value;
            }
        }

        /// <summary>
        /// This method is for internal use.
        /// </summary>
        /// <returns></returns>
        internal bool GetShareBetweenSessions()
        {
            return Enabled && ShareBetweenSessions;
        }

		/// <summary>
		/// Specifies whether the expiration timer should be reset when reports are retrieved from the cache.
		/// </summary>
		/// <remarks>
		/// The default value for this property is true, meaning that the expiration time is resets
		/// when reports are retrieved from the cache.
		/// </remarks>
		[DefaultValue(true)]
        [NotifyParentProperty(true)]
        [RefreshProperties(RefreshProperties.All)]
        [C1Category("Category.Behavior")]
        [C1Description("C1ReportViewer.Cache.Sliding", "Specifies whether the expiration timer should be reset when reports are retrieved from the cache.")]
        public bool Sliding
        {
            get {
                return this.ViewState["Sliding"] == null ? true : (bool)this.ViewState["Sliding"]; 
            }
            set {
                this.ViewState["Sliding"] = value; 
            }
        }

        #endregion

    }
}

