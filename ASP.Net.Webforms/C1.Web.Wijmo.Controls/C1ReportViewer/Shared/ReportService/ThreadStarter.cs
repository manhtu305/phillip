﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Text;
using System.Diagnostics;
using System.Web;

#if WIJMOCONTROLS
namespace C1.Web.Wijmo.Controls.C1ReportViewer.ReportService
#else
namespace C1.Web.UI.Controls.C1Report.ReportService
#endif
{
    internal static class ThreadStarter
    {
        public class Params
        {
            public HttpContext Context;
            public ParameterizedThreadStart Start;
            public object Param;
            public string DocumentKey;
            public Params(HttpContext context, ParameterizedThreadStart start, object param, string documentKey)
            {
                Context = context;
                Start = start;
                Param = param;
                DocumentKey = documentKey;
            }
        }

        /// <summary>
        /// Start ALL THREADS via this!!!
        /// </summary>
        public static void Start(object o)
        {
            try
            {
                ThreadStarter.Params pars = o as ThreadStarter.Params;
                try
                {
                    pars.Start(pars.Param);
                }
                catch (Exception ex)
                {
                    ThreadedErrors.PushError(pars.Context, pars.DocumentKey, ex.Message);
                }
            }
            catch (Exception ex)
            {
                ServiceCache.LogError(string.Format("ThreadStarter  aborted: {0}", ex.Message));
                // eat exceptions anyway
            }
        }
    }
}
