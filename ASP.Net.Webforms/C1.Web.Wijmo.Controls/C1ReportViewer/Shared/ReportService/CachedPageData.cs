using System;
using System.Web;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using C1.C1Preview;
using C1.C1Preview.Util;

#if WIJMOCONTROLS
namespace C1.Web.Wijmo.Controls.C1ReportViewer.ReportService
#else
namespace C1.Web.UI.Controls.C1Report.ReportService
#endif
{
    /// <summary>
    /// Represents page data serialized as a separate entry into the page zip file.
    /// Contains the graphic page representation (<see cref="MetafileData"/>),
    /// text and hyperlinks markup, and other info.
    /// </summary>
    [Serializable()]
    internal class CachedPageData
    {
        /// <summary>
        /// 0-based page index.
        /// </summary>
        public int PageIndex { get; private set; }
        /// <summary>
        /// Page size in pixels (using CachedDocument.Dpi).
        /// </summary>
        public SizeD Size { get; private set; }
        /// <summary>
        /// Metafile data as serialized via GraphicsUtils.WriteMetafileToMemory.
        /// </summary>
        public byte[] MetafileData { get; private set; }
        /// <summary>
        /// TextRunInfos (using CachedDocument.Dpi).
        /// </summary>
        public List<TextWordsRunInfo> TextRunInfos { get; private set; }
        /// <summary>
        /// The list of HyperlinkEntry for this page (using CachedDocument.Dpi).
        /// </summary>
        public List<HyperlinkInfo> Hyperlinks { get; private set; }
        /// <summary>
        /// Page generation (version). This starts at 0 when the page is first created,
        /// and increments by 1 each time the page is changed.
        /// </summary>
        public int Generation { get; private set; }

        private CachedPageData()
        {
            PageIndex = -1;
            Size = SizeD.Empty;
            MetafileData = null;
            TextRunInfos = null;
            Hyperlinks = null;
            Generation = 0;
        }

        /// <summary>
        /// ctor
        /// </summary>
        public static CachedPageData Make(CachedDocument cdoc, C1DocHolder docHolder, int pageIndex, HttpContext context)
        {
            Debug.Assert(cdoc.PageInfos.Count > pageIndex);

            CachedDocument.PageInfo pageInfo;
            lock (cdoc.SyncRoot)
                pageInfo = cdoc.PageInfos[pageIndex];

            CachedPageData cpage = new CachedPageData();
            cpage.PageIndex = pageIndex;
            cpage.Size = pageInfo.Size;
            cpage.Generation = pageInfo.Generation;
            cpage.TextRunInfos = docHolder.GetPage(pageIndex).GetTextRunInfos(cdoc.Dpi, new char[] { ' ' });
            cpage.Hyperlinks = MakeHyperlinkEntries(cdoc, docHolder, pageIndex);
            // get page metafile data:
            using (Metafile mf = docHolder.MakePageMetafile(pageIndex))
                cpage.MetafileData = GraphicsUtils.WriteMetafileToMemory(mf);

            string cachedDocumentPageKey = KeyMaker.CachedDocumentPageKey(cdoc.Key, pageIndex, pageInfo.Generation);
            CacheHandler.Add(cachedDocumentPageKey, cdoc.Key, context);

            return cpage;
        }

        private static List<HyperlinkInfo> MakeHyperlinkEntries(CachedDocument cdoc, C1DocHolder docHolder, int pageIndex)
        {
            List<HyperlinkInfo> links = new List<HyperlinkInfo>();

            C1Page page = docHolder.GetPage(pageIndex);
            C1HyperlinkInfoCollection c1docLinks = page.GetHyperlinks();
            if (c1docLinks == null)
                return links;

            using (GraphicsHolder gh = GraphicsHolder.FromBitmap())
            {
                System.Diagnostics.Debug.Assert(gh.Graphics.DpiX == gh.Graphics.DpiY);
                double zoom = (double)cdoc.Dpi / (double)gh.Graphics.DpiX;
                foreach (C1HyperlinkInfo c1hi in c1docLinks)
                {
                    HyperlinkInfo hi = new HyperlinkInfo();
                    hi.Action = LinkToAction(c1hi, docHolder, pageIndex);
                    hi.Text = c1hi.Hyperlink != null && !string.IsNullOrEmpty(c1hi.Hyperlink.StatusText)? c1hi.Hyperlink.StatusText : hi.Action;
                    // convert coordinates of regions to Pixels
                    foreach (C1HyperlinkInfo.Area c1area in c1hi.Areas)
                    {
                        page.Document.FromRU(c1area.Region, UnitTypeEnum.Pixel, cdoc.Dpi, cdoc.Dpi);
                        RectangleD area = c1area.Region.GetBounds(gh.Graphics);
                        area.X *= zoom;
                        area.Y *= zoom;
                        area.Width *= zoom;
                        area.Height *= zoom;
                        hi.Areas.Add(area);
                    }
                    links.Add(hi);
                }
            }
            return links;
        }

        private static string LinkToAction(C1HyperlinkInfo c1hi, C1DocHolder docHolder, int pageIndex)
        {
            if (c1hi == null)
                return null;

            else if (c1hi.LinkTarget is C1LinkTargetAnchor)
            {
                C1AnchorInfo ai;
                ai = docHolder.FindAnchor(((C1LinkTargetAnchor)c1hi.LinkTarget).AnchorName);

                if (ai != null)
#if WIJMOCONTROLS
					return string.Format(CultureInfo.InvariantCulture, "exec:scrollToPage({0})", ai.PageIndex);
#else
                    return string.Format(CultureInfo.InvariantCulture, "exec:set_currentPageIndex({0})", ai.PageIndex);
#endif

			}
            else if (c1hi.LinkTarget is C1LinkTargetDocumentLocation)
            {
                IDocumentLocation loc = ((C1LinkTargetDocumentLocation)c1hi.LinkTarget).DocumentLocation;
                if (loc != null)
#if WIJMOCONTROLS
					return string.Format(CultureInfo.InvariantCulture, "exec:scrollToPage({0})", loc.PageIndex);
#else
                    return string.Format(CultureInfo.InvariantCulture, "exec:set_currentPageIndex({0})", loc.PageIndex);
#endif

			}
            else if (c1hi.LinkTarget is C1LinkTargetExternalAnchor)
                return null; // todo
            else if (c1hi.LinkTarget is C1LinkTargetFile)
            {
                // PdfURL works better in web context, following work fine:
                // http://www.google.com
                // javascript:alert('hello world')
                return c1hi.LinkTarget.PdfURL;
            }
            else if (c1hi.LinkTarget is C1LinkTargetPage)
            {
                C1LinkTargetPage ltp = (C1LinkTargetPage)c1hi.LinkTarget;
                switch (ltp.PageJumpType)
                {
                    case PageJumpTypeEnum.Absolute:
#if WIJMOCONTROLS
						return string.Format(CultureInfo.InvariantCulture, "exec:scrollToPage({0})", ltp.PageNo);
#else
                        return string.Format(CultureInfo.InvariantCulture, "exec:set_currentPageIndex({0})", ltp.PageNo);
#endif
					case PageJumpTypeEnum.First:
                        return "exec:firstPage()";
                    case PageJumpTypeEnum.Last:
                        return "exec:lastPage()";
                    case PageJumpTypeEnum.Next:
                        return "exec:nextPage()";
					case PageJumpTypeEnum.Previous:
#if WIJMOCONTROLS                    
						return "exec:previousPage()";
#else
						return "exec:prevPage()";
#endif
                    case PageJumpTypeEnum.Relative:
#if WIJMOCONTROLS
						return string.Format(CultureInfo.InvariantCulture, "exec:scrollToPage({0})", pageIndex + 1 + ltp.PageNo);
#else
                        return string.Format(CultureInfo.InvariantCulture, "exec:set_currentPageIndex({0})", pageIndex + 1 + ltp.PageNo);						
#endif

					default:
                        Debug.Assert(false, "Unknown PageJumpType");
                        return null;
                }
            }
            else if (c1hi.LinkTarget is C1LinkTargetUser)
                return c1hi.LinkTarget.PdfURL;

            Debug.Assert(false, "Unknown C1LinkTarget type.");
            return null;
        }
    }
}
