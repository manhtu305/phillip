﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Web;

#if WIJMOCONTROLS
namespace C1.Web.Wijmo.Controls.C1ReportViewer.ReportService
#else
namespace C1.Web.UI.Controls.C1Report.ReportService
#endif
{
    /// <summary>
    /// Used to make cache keys, file names and such.
    /// </summary>
    internal static class KeyMaker
    {
        /// <summary>
        /// Gets current session id.
        /// </summary>
        public static string SessionID(HttpContext context)
        {
            // avoids a bug: unless something is written first, SessionID is different all the time:
            string sessionID = (string)context.Session["__C1ReportSessionID"];
            if (sessionID == null)
            {
                sessionID = (DateTime.Now.Ticks + "_" + new Random().Next(1, 1000)).ToString();
                context.Session["__C1ReportSessionID"] = sessionID;
            }
            return sessionID;
        }

        /// <summary>
        /// Makes full path for the pages cache zip file.
        /// </summary>
        public static string PageZipPath(string workDir, string documentKey)
        {
            return string.Format(CultureInfo.InvariantCulture, @"{0}\{1}_pages.zip", workDir, documentKey);
        }

        /// <summary>
        /// Makes name of an entry in the pages cache zip file.
        /// </summary>
        public static string PageZipEntry(int pageIndex)
        {
            return string.Format(CultureInfo.InvariantCulture, "Page_{0:00000}", pageIndex);
        }

        /// <summary>
        /// Makes file name (without path) for page image (.png).
        /// </summary>
        public static string PageImageFileName(string documentKey, int pageIdx, int dpi, string ext)
        {
            string fn = string.Format(CultureInfo.InvariantCulture, @"{0}_{1}_{2}{3}", documentKey, pageIdx, dpi, ext);
            return fn;
        }

        /// <summary>
        /// Makes c1d(m)(x) file name (without path) for a generated document.
        /// Ext must have dot (.c1d, .c1dx or .c1mdx).
        /// </summary>
        public static string C1dFileName(string documentKey, string ext)
        {
            string fn = string.Format(CultureInfo.InvariantCulture, @"{0}{1}", documentKey, ext);
            return fn;
        }


        /// <summary>
        /// Makes file name FORMAT (expected to be used with page index) (without path) for page thumbnails.
        /// </summary>
        public static string PageThumbnailFileNameFormat(string documentKey, string ext)
        {
            string fn = string.Format(CultureInfo.InvariantCulture, @"{0}_{{0}}_thumb{1}", documentKey, ext);
            return fn;
        }

        /// <summary>
        /// Makes full path name for a work file.
        /// </summary>
        public static string FilePath(string workDir, string fileName)
        {
            return string.Format(CultureInfo.InvariantCulture, @"{0}\{1}", workDir, fileName);
        }

        /// <summary>
        /// Makes URL for a work file.
        /// </summary>
        public static string FileUrl(string workUrl, string fileName)
        {
            string fmt;
            if (workUrl.EndsWith("/"))
                fmt = @"{0}{1}";
            else
                fmt = @"{0}/{1}";
            string url = string.Format(CultureInfo.InvariantCulture, fmt, workUrl, fileName);
            return url;
        }

        /// <summary>
        /// Makes file name (without path) for unpaged html.
        /// </summary>
        public static string UnpagedHtmlFileName(string documentKey, string ext)
        {
            string fn = string.Format(CultureInfo.InvariantCulture, @"{0}_unpaged{1}", documentKey, ext);
            return fn;
        }

        /// <summary>
        /// Makes the cached document key.
        /// Pass empty string in sessionId to share document between sessions.
        /// Keys are unique for different pairs of file and report names, and optionally session IDs.
        /// </summary>
        public static string CachedDocumentKey(DocIdentity di, bool share, bool inputParameters, HttpContext context)
        {
            string sessionId = share ? string.Empty : KeyMaker.SessionID(context);
            string paramsId = "";
            if (di.HasParameters && inputParameters)
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < di.Parameters.Length; ++i)
                {
                    sb.AppendFormat("par{0}:", di.Parameters[i].n);
                    if (di.Parameters[i].vs != null)
                        for (int j = 0; j < di.Parameters[i].vs.Length; ++j)
                            sb.AppendFormat("v{0}:{1:X}", j, di.Parameters[i].vs[j] != null ? di.Parameters[i].vs[j].GetHashCode() : 0);
                }
                paramsId = sb.ToString();
            }
            string docId = string.Format(CultureInfo.InvariantCulture, "{0}#{1}#{2}", di.FileName, di.ReportName, paramsId);
            string docKey = string.Format(CultureInfo.InvariantCulture, "{0:X}_{1:X}", sessionId.GetHashCode(), docId.GetHashCode());
			_rememberDocKey(di, docKey);
            return docKey;
        }

        /// <summary>
        /// Makes the cache key for the generating document.
        /// </summary>
        public static string GeneratingDocumentKey(string documentKey)
        {
            string key = string.Format(CultureInfo.InvariantCulture, "GeNeRaTiNg_{0}", documentKey);
            return key;
        }

        /// <summary>
        /// Cache entry that expires when a page's generation changes, used to remove stale page images.
        /// </summary>
        public static string CachedDocumentPageKey(string documentKey, int pageIndex, int pageGeneration)
        {
            string key = string.Format(CultureInfo.InvariantCulture, "{0}_page{1}_gen{2}", documentKey, pageIndex, pageGeneration);
            return key;
        }

        /// <summary>
        /// Cached C1PrintDocument in memory (for search/export).
        /// </summary>
        public static string CachedC1DocKey(string documentKey)
        {
            string key = string.Format(CultureInfo.InvariantCulture, "{0}_c1doc", documentKey);
            return key;
        }

        /// <summary>
        /// Makes the cache key for the cached errors collection.
        /// </summary>
        public static string ThreadedErrorsKey(string documentKey)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0}-errors-queue", documentKey);
        }

        /// <summary>
        /// Makes the cache key for the page requests queue.
        /// </summary>
        public static string PageRequestsQueueKey(string documentKey)
        {
            return string.Format(CultureInfo.InvariantCulture, "{0}-page-requests-queue", documentKey);
        }

		#region ** fix for [29422] [24221-139][C1ReportViewer]C1Preview.C1PrintDocument is not disposing

		private static Dictionary<string, string> _docKeys = new Dictionary<string, string>();

		internal static void _rememberDocKey(DocIdentity di, string docKey)
		{
			_docKeys[String.Format("{0}_{1}", _shortFileName(di.FileName), di.ReportName)] = docKey;
		}

		internal static void _forgetDocKey(string docKey)
		{
			foreach (var item in _docKeys)
			{
				if (item.Value == docKey)
				{
					_docKeys.Remove(item.Key);
					return;
				}
			}				 
		}

		private static string _shortFileName(string s)
		{
			if (string.IsNullOrEmpty(s))
			{
				return "";
			}
			int i = s.LastIndexOf("\\");
			if (i >= 0)
			{
				s = s.Substring(i + 1);
			}
			i = s.LastIndexOf("/");
			if (i >= 0)
			{
				s = s.Substring(i + 1);
			}
			return s;
		}

		/// <summary>
		/// Get documentKey for previously generated document.
		/// </summary>
		/// <param name="fileName"></param>
		/// <param name="reportName"></param>
		/// <returns></returns>
		internal static string GetDocumentKeyByFileAndReportName(string fileName, string reportName)
		{
			if (_docKeys.ContainsKey(String.Format("{0}_{1}", _shortFileName(fileName), reportName)))
			{
				return (string)_docKeys[String.Format("{0}_{1}", _shortFileName(fileName), reportName)];
			}
			return string.Empty;
		}
		#endregion
	}
}
