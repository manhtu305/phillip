﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Drawing.Imaging;
using C1.C1Preview;

#if WIJMOCONTROLS
namespace C1.Web.Wijmo.Controls.C1ReportViewer.ReportService
#else
namespace C1.Web.UI.Controls.C1Report.ReportService
#endif
{
    internal static class Util
    {
        public static void AdjustNewDocOrReport(object document)
        {
            if (document is C1.C1Rdl.Rdl2008.C1RdlReport)
            {
                var rdl = (C1.C1Rdl.Rdl2008.C1RdlReport)document;
                if (!rdl.UseGdiPlusTextRendering)
                {
                    rdl.UseGdiPlusTextRendering = true;
                    rdl.EmfType = EmfType.EmfPlusOnly;
                }
            }
            else if (document is C1.C1Report.C1Report)
            {
                var rep = (C1.C1Report.C1Report)document;
                if (!rep.UseGdiPlusTextRendering || !rep.ColorizeHyperlinks)
                {
                    rep.UseGdiPlusTextRendering = true;
                    rep.ColorizeHyperlinks = true;
                    // Fix the issue 42965, Japan team wants to show the bookmark when instantiate C1Report from C1ReportViewer. Remove following code to show bookmark.
                    //rep.EmfType = EmfType.EmfPlusOnly;
                }
            }
            else if (document is C1PrintDocument)
            {
                var doc = (C1PrintDocument)document;
                if (!doc.UseGdiPlusTextRendering)
                {
                    doc.Pages.Clear();
                    doc.UseGdiPlusTextRendering = true;
                    doc.EmfType = EmfType.EmfPlusOnly;
                }
            }
            else if (document is C1MultiDocument)
            {
                var doc = (C1MultiDocument)document;
                bool needClear = false;
                // "must have" props are UseGdiPlusTextRendering & ColorizeHyperlinks:
                if (doc.UseGdiPlusTextRendering.HasValue && doc.UseGdiPlusTextRendering.Value &&
                    doc.ColorizeHyperlinks.HasValue && doc.ColorizeHyperlinks.Value)
                {
                    needClear = false;
                }
                else
                {
                    foreach (C1MultiDocumentItem mi in doc.Items)
                    {
                        // !HasValue means that that property does not exist on item's doc - impossible in this case:
                        Debug.Assert(mi.UseGdiPlusTextRendering.HasValue);
                        if (!mi.UseGdiPlusTextRendering.HasValue || !mi.UseGdiPlusTextRendering.Value ||
                            (mi.ColorizeHyperlinks.HasValue && !mi.ColorizeHyperlinks.Value))
                        {
                            needClear = true;
                            break;
                        }
                    }
                }
                if (needClear)
                {
                    doc.ClearGeneratedPages();
                    doc.UseGdiPlusTextRendering = true;
                    doc.EmfType = EmfType.EmfPlusOnly;
                    doc.ColorizeHyperlinks = true;
                }
            }
            else
                System.Diagnostics.Debug.Assert(document == null, "Unknown document type");
        }

        public static DocumentOutline MakeDocumentOutline(C1DocHolder docHolder, string documentKey)
        {
            DocumentOutline outline = new DocumentOutline();
            outline.documentKey = documentKey;
            OutlineNodeCollection docOutlines = docHolder.GetOutlines();

            if (docOutlines != null && docOutlines.Count > 0)
            {
                outline.oes = new OutlineEntry[docOutlines.Count];
                FillOutlineEntryChildren(docHolder, docOutlines, outline.oes);
            }
            return outline;
        }

        /// <summary>
        /// Fills a collection of OutlineEntry.
        /// </summary>
        /// <param name="nodes">Source outline nodes.</param>
        /// <param name="entries">Target outline entry array. Must be initialized to the same
        /// number of empty elements as <paramref name="nodes"/>.</param>
        /// <param name="docHolder">Document holder (C1PrintDocument or C1MultiDocument).</param>
        private static void FillOutlineEntryChildren(C1DocHolder docHolder, C1.C1Preview.OutlineNodeCollection nodes, OutlineEntry[] entries)
        {
            System.Diagnostics.Debug.Assert(nodes.Count == entries.Length);

            for (int i = 0; i < nodes.Count; ++i)
            {
                OutlineNode node = nodes[i];
                OutlineEntry entry = new OutlineEntry();
                entry.text = node.Caption;
                if (docHolder.IsMultiDoc)
                    entry.p = node.LinkTarget.GetDocumentLocation(-1, docHolder.MultiDoc).PageIndex;
                else
                    entry.p = node.LinkTarget.GetDocumentLocation(-1, docHolder.Document).PageIndex;

                /*todo: 
                 fill tr field in order to fix issue:
                 9629 [C1ReportViewer] Clicking on outlines, does not move focus to the repective topic of the report.*/
                if (node.Children != null && node.Children.Count > 0)
                {
                    entry.oes = new OutlineEntry[node.Children.Count];
                    FillOutlineEntryChildren(docHolder, node.Children, entry.oes);
                }
                entries[i] = entry;
            }
        }

        public static byte[] ReadFileData(string fn)
        {
            if (!File.Exists(fn))
                return null;
            for (int retry = 0; retry < Constants.ReadFileDataRetries; ++retry)
            {
                try
                {
                    return File.ReadAllBytes(fn);
                }
                catch
                {
                    Thread.Sleep(Constants.ReadFileDataDelayMsec);
                }
            }
            return null;
        }

        public static void DeleteWorkFile(string path)
        {
            Debug.WriteLine("Deleting work file: " + path + "...");
            try
            {
                File.Delete(path);
            }
            catch (Exception ex)
            {
                ServiceCache.LogWarning(string.Format("Could not delete work file {0}: {1}", path, ex.Message));
            }
        }

        public static void DeleteWorkDir(string path, bool throwErrors)
        {
            ServiceCache.LogWarning("Deleting work directory: " + path + "...");
            try
            {
                Directory.Delete(path, true);
            }
            catch (Exception ex)
            {
                if (throwErrors)
                {
                    ServiceCache.LogError(string.Format("Could not delete work file {0}: {1}", path, ex.Message));
                    throw;
                }
                else
                    ServiceCache.LogWarning(string.Format("Could not delete work file {0}: {1}", path, ex.Message));
            }
        }

        public static void RecreateWorkDir(string path)
        {
            ServiceCache.LogWarning(string.Format("RecreateWorkDir: called for {0}...", path));
            DirectoryInfo dir = new DirectoryInfo(path);
            if (dir.Exists)
            {
                ServiceCache.LogWarning(string.Format("RecreateWorkDir: deleting {0}...", path));
                FileInfo[] files = dir.GetFiles();
                foreach (FileInfo fi in files)
                    fi.Delete();
                DirectoryInfo[] dirs = dir.GetDirectories();
                foreach (DirectoryInfo di in dirs)
                    di.Delete(true);
                ServiceCache.LogWarning(string.Format("RecreateWorkDir: deleted {0} - DONE.", path));
                dir.Refresh();
            }
            ServiceCache.LogWarning(string.Format("RecreateWorkDir: creating {0}...", path));
            dir.Create();
            ServiceCache.LogWarning(string.Format("RecreateWorkDir: created {0} - DONE.", path));
            Debug.Assert(Directory.Exists(path));
        }
    }
}
