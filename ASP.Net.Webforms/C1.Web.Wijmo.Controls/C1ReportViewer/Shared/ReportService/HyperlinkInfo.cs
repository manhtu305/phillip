﻿using System;
using System.Collections.Generic;
using System.Text;
using C1.C1Preview;

#if WIJMOCONTROLS
namespace C1.Web.Wijmo.Controls.C1ReportViewer.ReportService
#else
namespace C1.Web.UI.Controls.C1Report.ReportService
#endif
{
    [Serializable()]
    internal class HyperlinkInfo
    {
        public HyperlinkInfo()
        {
            Text = string.Empty;
            Action = string.Empty;
            Areas = new List<RectangleD>();
        }
        public string Text;
        public string Action;
        public List<RectangleD> Areas;
    }
}
