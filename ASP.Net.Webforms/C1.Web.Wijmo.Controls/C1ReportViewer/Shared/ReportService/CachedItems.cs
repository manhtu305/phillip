using System;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.Caching;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using C1.C1Preview;
using C1.C1Preview.Util;
using C1.C1Zip;

#if WIJMOCONTROLS
namespace C1.Web.Wijmo.Controls.C1ReportViewer.ReportService
#else
namespace C1.Web.UI.Controls.C1Report.ReportService
#endif
{
    /// <summary>
    /// Represents a cached item associated with a temporary/work file.
    /// When the item is removed from cache, the associated file is deleted.
    /// </summary>
    internal class CachedWorkFile
    {
        public string FilePath;
        public CachedWorkFile(string filePath)
        {
            FilePath = filePath;
        }
    }

    /// <summary>
    /// Used to keep info about a currently generating document in cache.
    /// </summary>
    internal class CachedGeneratingDocument
    {
        /// <summary>
        /// Gets the associated CachedDocument key.
        /// </summary>
        public string DocumentKey { get; private set; }

        /// <summary>
        /// Gets the C1PrintDocument (standalone or c1report's internal),
        /// or the C1MultiDocument.
        /// </summary>
        public C1DocHolder DocHolder { get; private set; }

        /// <summary>
        /// Initializes a new instance of CachedGeneratingDocument.
        /// </summary>
        /// <param name="cachedDocumentKey">Key for the associated CachedDocument (must be already in cache).</param>
        /// <param name="docHolder">The doc holder.</param>
        public CachedGeneratingDocument(string cachedDocumentKey, C1DocHolder docHolder)
        {
            DocumentKey = cachedDocumentKey;
            DocHolder = docHolder;
        }
    }

    /// <summary>
    /// Used to cache a path to a page image file and its pixel dimension.
    /// </summary>
    internal class CachedPageImage : CachedWorkFile
    {
        public Size Size;
        public CachedPageImage(string imagePath, Size size)
            : base(imagePath)
        {
            Size = size;
        }
    }

    /// <summary>
    /// Used to cache a path to a saved c1d(m)(x).
    /// </summary>
    internal class CachedC1d : CachedWorkFile
    {
        public CachedC1d(string c1dPath, bool isMultiDoc, ReaderWriterLockSlim cachedC1dock)
            : base(c1dPath)
        {
            Lock = cachedC1dock;
            IsMultiDoc = isMultiDoc;
        }

        /// <summary>
        /// This MUST be used when deleting the file!
        /// </summary>
        public ReaderWriterLockSlim Lock { get; private set; }

        public bool IsMultiDoc { get; private set; }
    }

    /// <summary>
    /// Used to cache a C1PrintDocument or a C1MultiDocument in memory for search/export.
    /// </summary>
    internal class CachedC1DocHolder
    {
        public C1DocHolder DocHolder { get; private set; }

        private object _syncRoot = new object();

        public CachedC1DocHolder(C1DocHolder docHolder)
        {
            DocHolder = docHolder;
        }

        /// <summary>
        /// Use this to use C1Doc.
        /// </summary>
        public object SyncRoot { get { return _syncRoot; } }
    }
}
