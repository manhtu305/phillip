﻿using System;
using System.Collections.Generic;
using System.Web;
using C1.C1Preview;

#if WIJMOCONTROLS
namespace C1.Web.Wijmo.Controls.C1ReportViewer.ReportService
#else
namespace C1.Web.UI.Controls.C1Report.ReportService
#endif
{
    /// <summary>
    /// Used to pass info about a cached document around.
    /// </summary>
    internal class CachedDocumentInfo
    {
        public CachedDocument CachedDoc { get; private set; }
        public C1DocHolder DocHolder { get; private set; }
        public HttpContext Context { get; private set; }

        public CachedDocumentInfo(CachedDocument cdoc, C1DocHolder docHolder, HttpContext context)
        {
            CachedDoc = cdoc;
            DocHolder = docHolder;
            Context = context;
        }
    }
}
