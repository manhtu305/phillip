using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using C1.C1Preview;

#if WIJMOCONTROLS
namespace C1.Web.Wijmo.Controls.C1ReportViewer.ReportService
#else
namespace C1.Web.UI.Controls.C1Report.ReportService
#endif
{
    internal class GenerateDoc
    {
        /// <summary>
        /// Used to pass data to event handlers of the document being generated.
        /// </summary>
        private class GeneratingDocInfo
        {
            /// <summary>
            /// Gets info about the cached document.
            /// </summary>
            public CachedDocumentInfo CachedDocInfo { get; private set; }
            /// <summary>
            /// Gets the CachedGeneratingDocument key.
            /// </summary>
            public string GeneratingDocumentKey { get; private set; }
            /// <summary>
            /// This is the queue of cache-ready pages waiting to be put into the page zip.
            /// The queue is created by the C1PrintDocument page (added, changed) and end
            /// event handlers. Pages are drawn in the same thread as the document is generated,
            /// and then put into this queue to be saved to the page zip. Because page zip
            /// saving may be slow, using a separate thread for that should speed things up
            /// as document generation won't have to wait for zip updates.
            /// 
            /// NULL element closes the queue and exits page zip updating thread.
            /// 
            /// Use lock(ToZipQueue) to access the queue.
            /// </summary>
            public List<CachedPageData> ToZipQueue { get; private set; }
            /// <summary>
            /// Used by the page zip updating thread to wait for new pages to be added to the queue.
            /// </summary>
            public EventWaitHandle ToZipQueueWaitHandle { get; private set; }
            /// <summary>
            /// Ctor.
            /// </summary>
            /// <param name="cdocInfo">Cached document info.</param>
            /// <param name="generatingDocumentKey">Generating document key.</param>
            public GeneratingDocInfo(CachedDocumentInfo cdocInfo, string generatingDocumentKey)
            {
                CachedDocInfo = cdocInfo;
                GeneratingDocumentKey = generatingDocumentKey;
                ToZipQueue = new List<CachedPageData>();
                ToZipQueueWaitHandle = new EventWaitHandle(false, EventResetMode.AutoReset);
            }
        }

        /// <summary>
        /// Holds parameters passed to the document generating thread.
        /// </summary>
        private class GenerateDocumentParams
        {
            /// <summary>
            /// Report or document being generated:
            /// C1Report, C1RdlReport, C1PrintDocument or C1MultiDocument.
            /// </summary>
            public object Report { get; private set; }
            /// <summary>
            /// Generating document info (used by document/report event handlers).
            /// </summary>
            public GeneratingDocInfo GenDocInfo { get; private set; }
            /// <summary>
            /// C1Report parameters.
            /// </summary>
            public Dictionary<string, string> C1ReportScriptObjects { get; private set; }
            /// <summary>
            /// Ctor.
            /// </summary>
            /// <param name="report">C1Report, C1RdlReport, C1PrintDocument or C1MultiDocument.</param>
            /// <param name="genDocInfo">Generating document info</param>
            /// <param name="sos">C1Report parameters (may be null).</param>
            public GenerateDocumentParams(object report, GeneratingDocInfo genDocInfo, Dictionary<string, string> sos)
            {
                Report = report;
                C1ReportScriptObjects = sos;
                GenDocInfo = genDocInfo;
            }
        }

        /// <summary>
        /// Creates a CachedGeneratingDocument in cache, and spawns a separate thread
        /// to generate the document. Note that CachedDocument must be already created
        /// and added to cache when this method is called.
        /// </summary>
        /// <param name="scache">The service cache.</param>
        /// <param name="cdocInfo">Cached document info.</param>
        /// <param name="report">C1Report, C1RdlReport, C1PrintDocument or C1MultiDocument to generate, or null.</param>
        /// <param name="docHolder">The underlying C1PrintDocument or C1MultiDocument.</param>
        /// <param name="c1reportParamValues">C1Report parameters.</param>
        public static void StartGenerateDocument(ServiceCache scache, CachedDocumentInfo cdocInfo,
            object report, C1DocHolder docHolder, Dictionary<string, string> c1reportParamValues)
        {
            if (report == null)
                return;

            Debug.Assert(docHolder != null);

            GeneratingDocInfo genDocInfo = new GeneratingDocInfo(cdocInfo, KeyMaker.GeneratingDocumentKey(cdocInfo.CachedDoc.Key));

            // Create generating document and insert it into the cache:
            CachedGeneratingDocument cgd = new CachedGeneratingDocument(cdocInfo.CachedDoc.Key, docHolder);
            CacheHandler.Add(genDocInfo.GeneratingDocumentKey, cgd);

            // Add handlers to report/document generating events. These handlers
            // add and update generated pages in the cache, and update generation status:
            if (!docHolder.IsMultiDoc)
            {
                docHolder.Document.PageAdded += (dd, ee) => OnPageAdded(genDocInfo, ee.Page, ee.Page.Index);
                docHolder.Document.PageChanged += (dd, ee) => OnPageChanged(genDocInfo, ee.Page, ee.Page.Index);
                docHolder.Document.DocumentEnded += (ss, ee) => OnDocumentEnded(genDocInfo);

                if (report is C1.C1Rdl.Rdl2008.C1RdlReport)
                    ((C1.C1Rdl.Rdl2008.C1RdlReport)report).LongOperation += (ss, ee) => OnLongOperation(genDocInfo, ee);
                else if (report is C1.C1Report.C1Report)
                    ((C1.C1Report.C1Report)report).LongOperation += (ss, ee) => OnLongOperation(genDocInfo, ee);
                else
                    ((C1PrintDocument)report).LongOperation += (ss, ee) => OnLongOperation(genDocInfo, ee);
            }
            else
            {
                docHolder.MultiDoc.PagesAdded += (mm, ee) =>
                    {
                        for (int i = 0; i < ee.PageCount; ++i)
                        {
                            C1Page page = mm.GetResolvedPage(ee.FirstPageIndex + i).Page;
                            OnPageAdded(genDocInfo, page, ee.FirstPageIndex + i);
                        }
                    };
                docHolder.MultiDoc.PagesChanged += (mm, ee) =>
                    {
                        for (int i = 0; i < ee.PageCount; ++i)
                        {
                            C1Page page = mm.GetResolvedPage(ee.FirstPageIndex + i).Page;
                            OnPageChanged(genDocInfo, page, ee.FirstPageIndex + i);
                        }
                    };
                docHolder.MultiDoc.DocumentEnded += (ss, ee) => OnDocumentEnded(genDocInfo);
                docHolder.MultiDoc.LongOperation += (ss, ee) => OnLongOperation(genDocInfo, ee);
            }

            // Start thread that adds generated pages to pages' zip file/cache:
            Thread zipThread = new Thread(ThreadStarter.Start);
            zipThread.Start(new ThreadStarter.Params(cdocInfo.Context, AddGeneratedPagesToZipThread, genDocInfo, cdocInfo.CachedDoc.Key));

            // Finally, start report/document generation thread:
            GenerateDocumentParams genDocPars = new GenerateDocumentParams(report, genDocInfo, c1reportParamValues);
            Thread generateThread = new Thread(ThreadStarter.Start);
            generateThread.Start(new ThreadStarter.Params(cdocInfo.Context, GenerateDocumentThread, genDocPars, cdocInfo.CachedDoc.Key));
        }

        /// <summary>
        /// Top thread method adding generated pages of a document to the zip cache.
        /// </summary>
        /// <param name="o">GeneratingDocInfo.</param>
        private static void AddGeneratedPagesToZipThread(object o)
        {
            const int justInCaseTimeout = 1000; // msec
            GeneratingDocInfo genDocInfo = (GeneratingDocInfo)o;
            List<CachedPageData> toZipQueue = genDocInfo.ToZipQueue;
            EventWaitHandle toZipQueueWaitHandle = genDocInfo.ToZipQueueWaitHandle;
            ServiceCache.LogMessage("ToZipPagesQueue Started.");

            while (true)
            {
                bool wait;
                lock (toZipQueue)
                    wait = toZipQueue.Count == 0;

                if (wait)
                    toZipQueueWaitHandle.WaitOne(justInCaseTimeout);

                CachedPageData page = null;
                lock (toZipQueue)
                {
                    if (toZipQueue.Count > 0)
                    {
                        page = toZipQueue[0];
                        toZipQueue.RemoveAt(0);
                        // NULL in the queue indicates that document generation is over
                        // and there will be no more pages.
                        if (page == null)
                        {
                            Debug.Assert(toZipQueue.Count == 0);
                            ServiceCache.LogMessage("ToZipPagesQueue ENDED!");
                            return;
                        }
                    }
                }
                if (page != null)
                {
                    string documentKey = genDocInfo.CachedDocInfo.CachedDoc.Key;
                    CachedDocument cdoc = genDocInfo.CachedDocInfo.CachedDoc;
                    cdoc.RetryAddCachedPageToZip(page, genDocInfo.CachedDocInfo.Context);
                    ServiceCache.LogMessage("ToZipPagesQueue added page");
                }
            }
        }

        /// <summary>
        /// Top thread method generating the document/report.
        /// </summary>
        /// <param name="o">GenerateDocumentParams.</param>
        private static void GenerateDocumentThread(object o)
        {
            GenerateDocumentParams pars = (GenerateDocumentParams)o;
            object report = pars.Report;

            try
            {
                int pageCount = 0;
                if (report is C1.C1Rdl.Rdl2008.C1RdlReport)
                {
                    ((C1.C1Rdl.Rdl2008.C1RdlReport)report).Render();
                    pageCount = ((C1.C1Rdl.Rdl2008.C1RdlReport)report).C1DocumentInternal.Pages.Count;
                }
                else if (report is C1.C1Report.C1Report)
                {
                    Dictionary<string, string> sos = pars.C1ReportScriptObjects;
                    if (sos != null && sos.Count > 0)
                    {
                        EventHandler onStartReport = (s, e) =>
                        {
                            foreach (string key in sos.Keys)
                                ((C1.C1Report.C1Report)s).AddScriptObject(key, sos[key]);
                        };
                        ((C1.C1Report.C1Report)report).StartReport += onStartReport;
                    }
                    ((C1.C1Report.C1Report)report).Render();
                    pageCount = ((C1.C1Report.C1Report)report).C1DocumentInternal.Pages.Count;
                }
                else if (report is C1PrintDocument)
                {
                    ((C1PrintDocument)report).Generate(RefreshModeEnum.RefreshDataBinding);
                    pageCount = ((C1PrintDocument)report).Pages.Count;
                }
                else // C1MultiDocument
                {
                    System.Diagnostics.Debug.Assert(report is C1MultiDocument);
                    ((C1MultiDocument)report).Generate();
                    pageCount = ((C1MultiDocument)report).PageCount;
                }

                // For C1RdlReport at least, DocumentStarting/DocumentEnded is not called if there are no records.
                // But we must make sure that document *is* "ended" (queues flushed etc). Hence this:
                if (pageCount == 0)
                    throw new Exception(Strings.DocumentEmptyError);
            }
            catch
            {
                // we must close the zip queue, otherwise the zip thread will wait indefinitely:
                GeneratingDocInfo genDocInfo = pars.GenDocInfo;

                // close the queue:
                lock (genDocInfo.ToZipQueue)
                {
                    genDocInfo.ToZipQueue.Add(null);
                    ServiceCache.LogMessage("GenerateDocument aborted");
                    genDocInfo.ToZipQueueWaitHandle.Set();
                }
                // Kill generating document in cache:
                CacheHandler.Remove(genDocInfo.GeneratingDocumentKey, genDocInfo.CachedDocInfo.Context);
                ServiceCache.LogMessage("Aborted generating document from cache: " + genDocInfo.GeneratingDocumentKey);

                // re-throw the exception
                throw;
            }
        }

        private static void OnLongOperation(GeneratingDocInfo genDocInfo, LongOperationEventArgs e)
        {
            // ServiceCache.LogMessage("Long op, complete: " + (e.Complete * 100).ToString());
            int complete = (int)Math.Round(e.Complete * 100);

            // state is updated in added/changed page handlers:
            genDocInfo.CachedDocInfo.CachedDoc.SetPercentComplete(complete);
        }

        private static void AddGeneratedPage(GeneratingDocInfo genDocInfo, int pageIndex, int generation)
        {
            if (PageRequestQueue.AddingCachedPage(genDocInfo.CachedDocInfo.Context, genDocInfo.CachedDocInfo.CachedDoc.Key, pageIndex, generation))
            {
                CachedDocument cdoc = genDocInfo.CachedDocInfo.CachedDoc;
                CachedPageData cpage = CachedPageData.Make(cdoc, genDocInfo.CachedDocInfo.DocHolder, pageIndex, genDocInfo.CachedDocInfo.Context);
                lock (genDocInfo.ToZipQueue)
                {
                    genDocInfo.ToZipQueue.Add(cpage);
                    genDocInfo.ToZipQueueWaitHandle.Set();
                }
                ServiceCache.LogMessage("added generated page " + pageIndex.ToString());
            }
        }

        /// <summary>
        /// Adds (or inserts) a generated page.
        /// </summary>
        /// <param name="genDocInfo">Generating document info.</param>
        /// <param name="c1page">The added page.</param>
        /// <param name="pageIndex">Page index in the *whole* document (may differ from page.Index in a C1MultiDocument).</param>
        private static void OnPageAdded(GeneratingDocInfo genDocInfo, C1Page c1page, int pageIndex)
        {
            CachedDocument cdoc = genDocInfo.CachedDocInfo.CachedDoc;
            C1DocHolder docHolder = genDocInfo.CachedDocInfo.DocHolder;

            Debug.Assert(cdoc.CurrentPageCount + 1 == docHolder.PageCount);
            Debug.Assert(cdoc.PageInfos.Count == cdoc.CurrentPageCount);

            // PageAdded may fire for pages that are actually "inserted" between already existing pages.
            // For such cases, we fire "page changed" for already added pages instead.
            if (pageIndex < cdoc.PageInfos.Count)
            {
                // page inserted - fire PageChanged for existing page indices, PageAdded for the new page index (last page):
                ServiceCache.LogMessage(string.Format("OnPageAdded: page inserted, updating pages {0} to {1}.", pageIndex, cdoc.PageInfos.Count - 1));

                for (int i = pageIndex; i < cdoc.PageInfos.Count; ++i)
                    OnPageChanged(genDocInfo, docHolder.GetPage(i), i);

                OnPageAdded(genDocInfo, docHolder.GetPage(cdoc.PageInfos.Count), cdoc.PageInfos.Count);
            }
            else
            {
                // page appended:
                Debug.Assert(cdoc.PageInfos.Count == pageIndex);
                ServiceCache.LogMessage(string.Format("OnPageAdded: page {0} appended.", pageIndex));

                SizeD size = new SizeD(
                    c1page.Document.FromRU(c1page.ResolvedPage.FullWidth, UnitTypeEnum.Pixel, cdoc.Dpi),
                    c1page.Document.FromRU(c1page.ResolvedPage.FullHeight, UnitTypeEnum.Pixel, cdoc.Dpi));

                OffsetsD margins = c1page.Document.FromRU(c1page.ResolvedInfo.PageMargins, UnitTypeEnum.Pixel, cdoc.Dpi, cdoc.Dpi);

                CachedDocument.PageInfo pageInfo = new CachedDocument.PageInfo(c1page.X, c1page.Y, size, margins);
                lock (cdoc.SyncRoot)
                {
                    // Update default page size with 1st page's real size:
                    if (cdoc.PageInfos.Count == 0)
                        cdoc.DefaultPageSize = size.Truncate();
                    cdoc.IncreaseCurrentPageCount();
                    cdoc.PageInfos.Add(pageInfo);
                }

                int[] requestedPages = PageRequestQueue.GetQueuedPages(genDocInfo.CachedDocInfo.Context, genDocInfo.CachedDocInfo.CachedDoc.Key);
                foreach (int i in requestedPages)
                {
                    if (i < cdoc.PageInfos.Count)
                    {

                        lock (cdoc.SyncRoot)
                            pageInfo = cdoc.PageInfos[i];
                        AddGeneratedPage(genDocInfo, i, pageInfo.Generation);
                    }
                }
            }
        }

        private static void OnPageChanged(GeneratingDocInfo genDocInfo, C1Page c1page, int pageIndex)
        {
            ServiceCache.LogMessage("Page changed " + pageIndex.ToString());

            CachedDocument cdoc = genDocInfo.CachedDocInfo.CachedDoc;

            Debug.Assert(cdoc.PageInfos.Count > pageIndex);
            CachedDocument.PageInfo pageInfo;
            lock (cdoc.SyncRoot)
                pageInfo = cdoc.PageInfos[pageIndex];
            pageInfo.IncrementGeneration();

            int[] requestedPages = PageRequestQueue.GetQueuedPages(genDocInfo.CachedDocInfo.Context, genDocInfo.CachedDocInfo.CachedDoc.Key);
            foreach (int i in requestedPages)
            {
                if (i == pageIndex)
                {
                    AddGeneratedPage(genDocInfo, i, pageInfo.Generation);
                    break;
                }
            }
        }

        private static void OnDocumentEnded(GeneratingDocInfo genDocInfo)
        {
            ServiceCache.LogMessage("GeneratingDocument_DocumentEnded");
            CachedDocument cdoc = genDocInfo.CachedDocInfo.CachedDoc;
            var context = genDocInfo.CachedDocInfo.Context;

            // add queued pages:
            int pageCount = genDocInfo.CachedDocInfo.DocHolder.PageCount;
            int[] requestedPages = PageRequestQueue.GetQueuedPages(context, cdoc.Key);
            foreach (int pageIndex in requestedPages)
            {
                if (pageIndex >= 0 && pageIndex < pageCount)
                    AddGeneratedPage(genDocInfo, pageIndex, cdoc.PageInfos[pageIndex].Generation);
            }
            // add the rest:
            for (int pageIndex = 0; pageIndex < pageCount; ++pageIndex)
            {
                int generation = cdoc.PageInfos[pageIndex].Generation;
                if (PageRequestQueue.IsPageCached(context, cdoc.Key, pageIndex, generation))
                    continue;
                AddGeneratedPage(genDocInfo, pageIndex, generation);
            }

            // close the queue:
            lock (genDocInfo.ToZipQueue)
            {
                genDocInfo.ToZipQueue.Add(null);
                genDocInfo.ToZipQueueWaitHandle.Set();
            }

            // delete page request queue so as to not leave stale info (TFS~~20692):
            PageRequestQueue.DeleteQueue(context, cdoc.Key);

            DocumentOutline outline = genDocInfo.CachedDocInfo.DocHolder.MakeOutline(cdoc.Key);
            lock (cdoc.SyncRoot)
            {
                cdoc.Outline = outline;
                cdoc.SetPercentComplete(100);
            }
            // Kill generating document in cache:
            CacheHandler.Remove(genDocInfo.GeneratingDocumentKey, context);
            ServiceCache.LogMessage("Removed generating document from cache: " + genDocInfo.GeneratingDocumentKey);

            // cache c1d/x for export:
            cdoc.CacheC1d(genDocInfo.CachedDocInfo.DocHolder, context);
        }
    }
}
