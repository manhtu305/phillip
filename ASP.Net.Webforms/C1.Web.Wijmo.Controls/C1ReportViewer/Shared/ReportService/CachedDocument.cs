using System;
using System.IO;
using System.Web;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using C1.C1Preview;
using C1.C1Preview.Util;
using C1.C1Preview.Export;
using C1.C1Zip;


#if WIJMOCONTROLS
namespace C1.Web.Wijmo.Controls.C1ReportViewer.ReportService
#else
namespace C1.Web.UI.Controls.C1Report.ReportService
#endif
{
    /// <summary>
    /// Represents a cached document. Note that document pages are stored on disk in a zip file.
    /// </summary>
    internal class CachedDocument : CachedWorkFile
    {
        #region nested classes
        /// <summary>
        /// Represents some page data stored in memory (in the server cache).
        /// </summary>
        public class PageInfo
        {
            private int _generation = 0;
            /// <summary>
            /// Initializes a new instance of PageInfo.
            /// </summary>
            /// <param name="posX">Horizontal (right) page position in the document (0-based).</param>
            /// <param name="posY">Vertical (down) page position in the document (0-based).</param>
            /// <param name="size">Page size in pixels (DPI is determined by owner).</param>
            /// <param name="margins">Page margins in pixels (DPI is determined by owner).</param>
            public PageInfo(int posX, int posY, SizeD size, OffsetsD margins)
            {
                PosX = posX;
                PosY = posY;
                Size = size;
                Margins = margins;
            }
            /// <summary>
            /// Gets the horizontal logical page position in the document (0-based).
            /// Usually this value is 0.
            /// </summary>
            public int PosX { get; private set; }
            /// <summary>
            /// Gets the vertical logical page position in the document (0-based).
            /// Usually this value is same as page index.
            /// </summary>
            public int PosY { get; private set; }
            /// <summary>
            /// Page size in pixels at owner's Dpi.
            /// This is updated in PageAdded, i.e. is actual even if PageGeneration==-1;
            /// </summary>
            public SizeD Size { get; private set; }
            // { get { return _size; } }
            /// <summary>
            /// Page margins in pixels at owner's Dpi.
            /// </summary>
            public OffsetsD Margins { get; private set; }
            /// <summary>
            /// Generation of the corresponding cached page.
            /// Pages are cached AFTER this is updated, so cached pages are guaranteed
            /// not to be older than this.
            /// </summary>
            public int Generation { get { return _generation; } }
            /// <summary>
            /// Gets a value that uniquely identifies the current page within the document.
            /// Ids are ordered but not sequential.
            /// </summary>
            public int Id
            {
                get
                {
                    return PosY << 16 | PosX;
                }
            }
            /// <summary>
            /// Increments generation by 1.
            /// </summary>
            public void IncrementGeneration()
            {
                Interlocked.Increment(ref _generation);
            }
        }

        /// <summary>
        /// Represents a sequence of pages with the same generation number.
        /// Used to calculate the list of changed pages for the client.
        /// NOTE: methods in this class should be called with CachedDocument LOCKED!
        /// </summary>
        [Serializable]
        public class PageGenRun
        {
            #region data fields
            public int StartIdx;
            public int EndIdx;
            public int Gen;
            #endregion

            #region static public methods
            /// <summary>
            /// Builds the list of PageGenRun's for the current document's state.
            /// </summary>
            public static List<PageGenRun> BuildRuns(CachedDocument cdoc)
            {
                Debug.Assert(cdoc.CurrentPageCount == cdoc.PageInfos.Count);
                List<PageGenRun> runs = new List<PageGenRun>();
                int lastGen = -1;
                for (int i = 0; i < cdoc.CurrentPageCount; ++i)
                {
                    if (cdoc.PageInfos[i].Generation != lastGen)
                    {
                        if (runs.Count > 0)
                            runs[runs.Count - 1].EndIdx = i - 1;
                        PageGenRun pgr = new PageGenRun();
                        pgr.StartIdx = i;
                        pgr.Gen = lastGen = cdoc.PageInfos[i].Generation;
                        runs.Add(pgr);
                    }
                }
                if (runs.Count > 0)
                    runs[runs.Count - 1].EndIdx = cdoc.CurrentPageCount - 1;
                return runs;
            }

#if SKIP_COOKIES
            public static List<int> ExpandRuns(string cookie)
            {
                if (string.IsNullOrEmpty(cookie))
                    return new List<int>();
                return ExpandRuns(BuildRuns(cookie));
            }
#endif

            public static List<int> ExpandRuns(List<PageGenRun> runs)
            {
                if (runs == null)
                    return new List<int>();
                List<int> expanded = new List<int>();
#if DEBUG
                int itest = 0;
#endif
                foreach (PageGenRun pgr in runs)
                {
                    for (int i = pgr.StartIdx; i <= pgr.EndIdx; ++i)
                    {
#if DEBUG
                        Debug.Assert(i == itest);
                        ++itest;
#endif
                        expanded.Add(pgr.Gen);
                    }
                }
                return expanded;
            }

#if SKIP_COOKIES
            public static string BuildCookie(List<PageGenRun> runs)
            {
                try
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        BinaryFormatter formatter = new BinaryFormatter();
                        formatter.Serialize(ms, runs);
                        string cookie = Convert.ToBase64String(ms.GetBuffer());
                        // cookie = Utils.UTFSupport.EncodeString(cookie);
                        return cookie;
                    }
                }
                catch
                {
                    Debug.Assert(false);
                    return null;
                }
            }
#endif
            #endregion

            #region static private methods
            private static List<PageGenRun> BuildRuns(string cookie)
            {
                try
                {
                    // cookie = Utils.UTFSupport.DecodeString(cookie);
                    byte[] bytes = Convert.FromBase64String(cookie);
                    using (MemoryStream ms = new MemoryStream(bytes))
                    {
                        BinaryFormatter formatter = new BinaryFormatter();
                        List<PageGenRun> runs = formatter.Deserialize(ms) as List<PageGenRun>;
                        return runs;
                    }
                }
                catch
                {
                    Debug.Assert(false);
                    return null;
                }
            }
            #endregion
        }
        #endregion

        #region private data
        private object _syncRoot = new object();
        private string _key;
        private DocIdentity _di;
        private DocumentType _documentType = DocumentType.None;
        private DocumentOutline _outline;
        private DateTime _creationTime;
        private Size _defaultPageSize;
        private int _currentPageCount;
        private int _percentComplete;
        private List<PageInfo> _pageInfos = new List<PageInfo>();
        private ViewType _viewType;
        private PdfSecurity _pdfSecurity;
        private string _workDir;
        private string _workUrl;
        private string _lastError = string.Empty;
        private ReaderWriterLockSlim _zipLock = new ReaderWriterLockSlim();
        private CachedC1d _cachedC1d = null; // created after the file has been generated
        private ReaderWriterLockSlim _cachedC1dLock = new ReaderWriterLockSlim();
        private const C1DocumentFormatEnum c_c1dFormat = C1DocumentFormatEnum.C1d;
        private const string c_c1dExt = ".c1d";
        private const string c_c1mdxExt = ".c1mdx";
        private const string c_imageExt = ".png";
        private readonly ImageFormat c_imageFmt = ImageFormat.Png;
        // TODO 1: we should allow developer to turn on/off any of the export formats.
        // TODO 2: we should export C1Report as C1Report (currently it is loaded from c1d(x)).
        // Till then, we provide the list of more or less useful formats:
        private static readonly List<ExportProvider> c_exportFormats = new List<ExportProvider>
        {
            ExportProviders.PdfExportProvider,
            ExportProviders.XlsxExportProvider,
            ExportProviders.DocxExportProvider,
            ExportProviders.XlsExportProvider,
            ExportProviders.RtfExportProvider,
            ExportProviders.C1dxExportProvider,
            ExportProviders.C1mdxExportProvider,
            // c1d deferred due to TFS 11495
            // ExportProviders.C1dExportProvider,
            // todo - what else?
        };
        private static readonly Dictionary<DocumentType, Type> c_documentTypes = new Dictionary<DocumentType, Type>
        {
            { DocumentType.C1MultiDocument, typeof(C1MultiDocument) },
            { DocumentType.C1PrintDocument, typeof(C1PrintDocument) },
            { DocumentType.C1RdlReport, typeof(C1.C1Rdl.Rdl2008.C1RdlReport) },
            { DocumentType.C1Report, typeof(C1.C1Report.C1Report) },
            { DocumentType.None, null },
        };

        #endregion

        #region construction
        private CachedDocument(string key, DocIdentity di, C1DocHolder docholder,
            DocumentType documentType, ServiceCache serviceCache, HttpContext context, string workDir, string workUrl)
            :
            base(KeyMaker.PageZipPath(workDir, key))
        {
            Debug.Assert(docholder != null);
            Debug.Assert(!docholder.IsGenerating);
            Debug.Assert(serviceCache.ReportCache != null);

            // make sure we clean old garbage:
            try
            {
                if (File.Exists(PageZipPath))
                    File.Delete(PageZipPath);
            }
            catch { }

            bool docIsGenerated = docholder.PageCount > 0;

            _key = key;
            _di = di;
            _documentType = documentType;
            _creationTime = docholder.CreationTime;
            _percentComplete = docIsGenerated ? 100 : 0;
            _currentPageCount = docholder.PageCount;
            _defaultPageSize = docholder.GetDefaultPageSize(Dpi);
            _workDir = workDir;
            _workUrl = workUrl;
            _viewType = serviceCache.ViewType;
            _pdfSecurity = serviceCache.PdfSecurity;

            if (docIsGenerated)
            {
                // make page infos for all pages:
                for (int i = 0; i < _currentPageCount; ++i)
                {
                    C1Page page = docholder.GetPage(i);
                    SizeD size = docholder.GetPageSize(i, Dpi);
                    OffsetsD margins = docholder.GetPageMargins(i, Dpi);
                    CachedDocument.PageInfo pageInfo = new CachedDocument.PageInfo(page.X, page.Y, size, margins);
                    if (i == 0)
                        _defaultPageSize = size.Truncate(); // update default page size with real size
                    _pageInfos.Add(pageInfo);
                }
                // outline:
                _outline = docholder.MakeOutline(Key);
            }
        }

        /// <summary>
        /// Logically, this is the constructor - the only place where a new instance of CachedDocument
        /// is created.
        /// 
        /// This method is called from within a static lock (typeof(CachedDocument)).
        /// 
        /// Inserts a document into the cache.
        /// Adding pages is handled in 2 different ways depending on whether the Pages array
        /// is currently empty or not. If it is not empty, the document is considered to be
        /// generated, pages are added and never checked for updates.
        /// If currently there are no pages, document's page added/changed events are subscribed to,
        /// and pages are only added on those events.
        /// </summary>
        private static CachedDocument InsertDocument(ServiceCache caller, HttpContext context,
            DocIdentity di, C1DocHolder docHolder, DocumentType documentType,
            string dependencyFile)
        {
            string documentKey = KeyMaker.CachedDocumentKey(di, caller.ReportCache.GetShareBetweenSessions(), caller.ShowParameterInputForm, context);

            bool docIsGenerated = docHolder.PageCount > 0;

            string docWorkDir = Path.Combine(caller.WorkDir, documentKey);
            string docWorkUrl = KeyMaker.FileUrl(caller.WorkUrl, documentKey);
            try
            {
                Util.RecreateWorkDir(docWorkDir);
            }
            catch
            {
                ServiceCache.LogError(string.Format("Could not create work directory: {0} (FileName: {1}, ReportName: {2})", docWorkDir, di.FileName, di.ReportName));
                throw; // cannot work without write access to work dir.
            }

            CachedDocument cdoc = new CachedDocument(documentKey, di, docHolder, documentType, caller, context, docWorkDir, docWorkUrl);

            // TODO: this IGNORES ReportCache.Enabled property.
            // TODO: implement non-cached work? why?
            CacheHandler.Add(cdoc, caller.ReportCache);
            // if (caller.ReportCache.Enabled) - save the document locally?

            if (docIsGenerated)
            {
                // Note: this method spawns a separate thread and returns
                // (for consistency, this must be done after the document has been added to cache):
                cdoc.InsertPagesToZip(docHolder);

                // Note: this method spawns a separate thread and returns
                Thread cacheC1dThread = new Thread(ThreadStarter.Start);
                cacheC1dThread.Start(new ThreadStarter.Params(context, cdoc.CacheGeneratedDoc, new CacheC1dParams(docHolder, context), cdoc.Key));
            }

            return cdoc;
        }
        #endregion construction

        #region public properties
        /// <summary>
        /// Lock this to access the document.
        /// </summary>
        public object SyncRoot { get { return _syncRoot; } }
        /// <summary>
        /// Gets the zip file path.
        /// </summary>
        public string PageZipPath { get { return base.FilePath; } }
        /// <summary>
        /// Gets the key for the current cached document in server cache.
        /// </summary>
        public string Key { get { return _key; } }
        /// <summary>
        /// Gets the report or document file name.
        /// </summary>
        public string FileName { get { return _di.FileName; } }
        /// <summary>
        /// Gets the report name. For in-memory documents, this should be set
        /// to different values for different documents by the developer.
        /// </summary>
        public string ReportName { get { return _di.ReportName; } }
        /// <summary>
        /// Gets or sets the document outline.
        /// </summary>
        public DocumentOutline Outline
        {
            get { return _outline; }
            set { _outline = value; }
        }
        /// <summary>
        /// Gets the resolution used by various pixel sizes and coordinates.
        /// The value is constant 96.
        /// </summary>
        public int Dpi { get { return 96; } }
        /// <summary>
        /// Gets the document's creation time.
        /// </summary>
        public DateTime CreationTime { get { return _creationTime; } }
        /// <summary>
        /// Gets the default page size for the document.
        /// Normally this should not be used - use individual page sizes from PageInfos instead.
        /// </summary>
        public Size DefaultPageSize { get { return _defaultPageSize; } set { _defaultPageSize = value; } }
        /// <summary>
        /// Gets the working directory used by the current cached document.
        /// </summary>
        public string WorkDir { get { return _workDir; } }
        /// <summary>
        /// Gets the forward slash-terminated URL pointing to the working directory.
        /// </summary>
        public string WorkUrl { get { return _workUrl; } }
        /// <summary>
        /// Gets the current document's view type (currently, page images only).
        /// </summary>
        public ViewType ViewType { get { return _viewType; } }
        /// <summary>
        /// Gets the current page count.
        /// </summary>
        public int CurrentPageCount { get { return _currentPageCount; } }
        /// <summary>
        /// Gets the approximate completion ratio for the document if it is currently generating,
        /// from 0 to 100. Gets 100 for generated documents.
        /// </summary>
        public int PercentComplete { get { return _percentComplete; } }
        /// <summary>
        /// Gets the list of <see cref="PageInfo"/> describing document pages.
        /// NOTE: indices in this list correspond to 0-based page indices in the document,
        /// BUT the list may contain NULL elements - not generated yet.
        /// (A C1PrintDocument may generate pages non-sequentially.)
        /// </summary>
        public List<PageInfo> PageInfos { get { return _pageInfos; } }
        #endregion

        #region public static methods
        /// <summary>
        /// Gets a previously cached document.
        /// NOTE: unlike the other overload, this method does NOT create a cached document
        /// if it is not found, so it may return null.
        /// </summary>
        public static CachedDocument GetCachedDocument(string documentKey)
        {
            return CacheHandler.GetCachedDocument(documentKey);
        }

        /// <summary>
        /// Gets a cached document. If the document could not be found in the cache,
        /// it is created and inserted into the cache.
        /// </summary>
        public static CachedDocument GetCachedDocument(ServiceCache caller, DocIdentity di, ref string errorDesc)
        {
            Debug.Assert(HttpContext.Current != null);
            Debug.Assert(caller.ReportCache != null);
            string documentKey = KeyMaker.CachedDocumentKey(di, caller.ReportCache.GetShareBetweenSessions(), caller.ShowParameterInputForm, HttpContext.Current);
            lock (typeof(CachedDocument))
            {
                CachedDocument cdoc = GetCachedDocument(documentKey);
                if (cdoc == null)
                {
                    object document;
                    string dependencyFile;
                    // The underlying document (used to retrieve the generated pages):
                    C1DocHolder docHolder;
                    DocumentType documentType;
                    Dictionary<string, string> c1reportParamValues;
                    // Load document or report, test whether it needs generation (loading is always in the main thread):
                    bool generate = LoadDocument(caller.Document, di, caller.ShowParameterInputForm,
                        out document, out documentType, out docHolder, out dependencyFile, out c1reportParamValues, ref errorDesc);

                    if (document == null)
                        return null;

                    //==================================================================
                    // Here we insert the new document into the cache.
                    // The InsertDocument call also spawns a separate thread that will be
                    // waiting for new pages and saving them to the page cache zip.
                    // Before we unlock the new CachedDocument for other threads,
                    // we must start its generating so that the document is in a
                    // stable state (generating).
                    //==================================================================
                    cdoc = InsertDocument(caller, HttpContext.Current, di, docHolder, documentType, dependencyFile);

                    if (generate)
                    {
                        //==========================================================
                        // Here we spawn a separate thread to generate the document:
                        //==========================================================
                        CachedDocumentInfo cdocInfo = new CachedDocumentInfo(cdoc, docHolder, HttpContext.Current);
                        GenerateDoc.StartGenerateDocument(caller, cdocInfo, document, docHolder, c1reportParamValues);
                    }
                }
                return cdoc;
            }
        }

        /// <summary>
        /// Immediately removes a previously cached document from cache,
        /// releasing the associated resources.
        /// </summary>
        /// <param name="documentKey">The document key.</param>
        public static bool ClearCachedDocument(string documentKey)
        {
            var cachedDocHolder = CacheHandler.RemoveCachedC1DocHolder(KeyMaker.CachedC1DocKey(documentKey));
			var o = CacheHandler.RemoveCachedDocument(documentKey);
			bool returnVal = false;
            if (cachedDocHolder != null)
            {
                lock (cachedDocHolder.SyncRoot)
                {
                    if (!cachedDocHolder.DocHolder.IsGenerating)
                    {
						if (cachedDocHolder.DocHolder.Document != null)
						{
							cachedDocHolder.DocHolder.Document.Dispose();
							returnVal = true;
						}
						else if (cachedDocHolder.DocHolder.MultiDoc != null)
						{
							cachedDocHolder.DocHolder.MultiDoc.Dispose();
							returnVal = true;
						}
                    }
                }
            }
			KeyMaker._forgetDocKey(documentKey);
			return returnVal || o != null;
        }
        #endregion public static methods

        #region public instance methods
        public DocumentStatus GetDocumentStatus(string cookie)
        {
            DocumentStatus status = new DocumentStatus();
            status.documentKey = this.Key;
            CachedGeneratingDocument gdoc = CacheHandler.GetGeneratingDocument(KeyMaker.GeneratingDocumentKey(this.Key), HttpContext.Current);
            lock (this.SyncRoot)
            {
                if (gdoc != null)
                {
                    status.isGenerating = true; // qq-is this right? c1doc.IsGenerating;
                    status.state = GeneratingState.GeneratingPages;
                    status.percentComplete = this.PercentComplete;
                }
                else
                {
                    // qq  investigate: Debug.Assert(this.PercentComplete == 100);
                    status.isGenerating = false;
                    status.state = GeneratingState.Ready;
                    status.percentComplete = 100;
                }
                status.name = this.ReportName; // qq - what is this used for?
                status.created = this.CreationTime;
                status.pageCount = this.CurrentPageCount;
                status.type = this.ViewType;
                status.w = this.DefaultPageSize.Width;
                status.h = this.DefaultPageSize.Height;
                status.pageSizes = MakePageSizeRanges();
                status.changedPages = MakeChangedPages(ref cookie);
                status.cookie = cookie;
                if (!status.isGenerating)
                    status.exportFormats = MakeExportFormatsList();
                Debug.Assert(status.changedPages.Length <= status.pageCount);
            }
            string error = ThreadedErrors.PopErrors(this.Key);
            if (!string.IsNullOrEmpty(error))
                _lastError = error;
            lock (this.SyncRoot)
                status.error = _lastError;
            return status;
        }

        public void CacheC1d(C1DocHolder docHolder, HttpContext context)
        {
            const int c_writeC1dTimeout = 3 * 60 * 1000; // 3 mins
            try
            {
                // right away, cache C1PrintDocument in memory:
                string cc1docKey = KeyMaker.CachedC1DocKey(Key);
                CacheHandler.Add(cc1docKey, new CachedC1DocHolder(docHolder), this, context);

                // cache c1d/x on disk:
                string c1dName = KeyMaker.C1dFileName(Key, docHolder.IsMultiDoc ? c_c1mdxExt : c_c1dExt);
                string c1dPath = KeyMaker.FilePath(WorkDir, c1dName);
                string c1dUrl = KeyMaker.FileUrl(WorkDir, c1dName);
                if (docHolder.IsMultiDoc)
                    docHolder.MultiDoc.Save(c1dPath);
                else
                    docHolder.Document.Save(c1dPath, c_c1dFormat);

                if (_cachedC1dLock.TryEnterWriteLock(c_writeC1dTimeout))
                {
                    try
                    {
                        _cachedC1d = new CachedC1d(c1dPath, docHolder.IsMultiDoc, _cachedC1dLock);
                        CacheHandler.Add(_cachedC1d, Key, context);
                    }
                    finally
                    {
                        _cachedC1dLock.ExitWriteLock();
                    }
                }
                // todo: else what???
            }
            catch (Exception ex)
            {
                Debug.Assert(false, ex.Message);
            }
        }

        /// <summary>
        /// Called by GeneratingDocument_PageAdded.
        /// </summary>
        public void IncreaseCurrentPageCount()
        {
            Interlocked.Increment(ref _currentPageCount);
        }

        /// <summary>
        /// Called by GeneratingDocument_LongOperation and some other methods.
        /// </summary>
        public void SetPercentComplete(int percent)
        {
            Interlocked.Exchange(ref _percentComplete, percent);
        }

        public void RetryAddCachedPageToZip(CachedPageData cpage, HttpContext context)
        {
            // todo: retry/timeout - move elsewhere?
            const int retries = 8;
            const int timeout = 10; // msec
            for (int i = 0; i < retries; ++i)
            {
                if (AddCachedPageToZip(cpage, context))
                    return;
                Thread.Sleep(timeout);
            }
            Debug.Assert(false, "RetryAddCachedPageToZip FAILED!!!");
            throw new Exception(Strings.AddCachedPageToZipFailed);
        }

        private SearchResults SearchText(C1DocHolder docHolder, string query, bool caseSensitive)
        {
            List<SearchEntry> ses = new List<SearchEntry>();
            for (int i = 0; i < docHolder.PageCount; ++i)
                SearchPage(docHolder.GetPage(i), i, query, caseSensitive, ses);

            SearchResults sr = new SearchResults();
            sr.ses = ses.ToArray();
            sr.query = query;
            sr.isGenerating = false;
            sr.error = null;
            sr.documentKey = Key;
            sr.count = ses.Count;
            return sr;
        }

        private void SearchPage(C1Page page, int pageIndex, string query, bool caseSensitive, List<SearchEntry> ses)
        {
            List<FoundTextEntry> founds = page.SearchText(query, caseSensitive, 96);
            foreach (FoundTextEntry fte in founds)
            {
                TextRunInfo[] tris = fte.GetRuns();
                SearchEntry se = new SearchEntry();
                List<TextRunMarkup> trms = new List<TextRunMarkup>();
                foreach (TextRunInfo tri in tris)
                {
                    TextRunMarkup trm = new TextRunMarkup();
                    trm.x = (int)Math.Round(tri.Rectangle.X);
                    trm.y = (int)Math.Round(tri.Rectangle.Y);
                    trm.w = (int)Math.Round(tri.Rectangle.Width);
                    trm.h = (int)Math.Round(tri.Rectangle.Height);
                    trm.text = tri.Text;
                    trm.p = null;
                    trm.ix = null;
                    trms.Add(trm);
                }
                se.text = query;
                se.tr = trms.ToArray();
                se.p = pageIndex;
                ses.Add(se);
            }
        }

        public SearchResults SearchText(string query, bool caseSensitive, bool useRegExp)
        {
            CachedC1DocHolder cachedDocHolder = CacheHandler.GetCachedC1DocHolder(KeyMaker.CachedC1DocKey(Key));

            if (cachedDocHolder != null)
            {
                try
                {
                    lock (cachedDocHolder.SyncRoot)
                    {
                        return SearchText(cachedDocHolder.DocHolder, query, caseSensitive);
                    }
                }
                catch (Exception ex)
                {
                    // log error and try c1d/x...
                    ServiceCache.LogError(string.Format("SearchText: error searching CachedC1Doc: {0}", ex.Message));
                }
            }

            if (_cachedC1d != null && _cachedC1dLock.TryEnterReadLock(2000))
            {
                try
                {
                    C1DocHolder docHolder;
                    if (_cachedC1d.IsMultiDoc)
                    {
                        C1MultiDocument c1mdoc = new C1MultiDocument("c1v01xJ/Ducakw6/Dl8WTw6vCrcKzJnx5w7c9");
                        c1mdoc.Load(_cachedC1d.FilePath);
                        docHolder = C1DocHolder.FromC1MultiDocument(c1mdoc);
                    }
                    else
                    {
                        C1PrintDocument c1doc = new C1PrintDocument("c1v01xJ/Ducakw6/Dl8WTw6vCrcKzJnx5w7c9");
                        c1doc.Load(_cachedC1d.FilePath, c_c1dFormat);
                        docHolder = C1DocHolder.FromC1PrintDocument(c1doc);
                    }
                    return SearchText(docHolder, query, caseSensitive);
                }
                catch (Exception ex)
                {
                    ServiceCache.LogError(string.Format("SearchText: error searching CachedC1d: {0}", ex.Message));
                    SearchResults err1 = new SearchResults();
                    err1.error = string.Format(CultureInfo.InvariantCulture, "error searching document: {0}", ex.Message);
                    return err1;
                }
                finally
                {
                    _cachedC1dLock.ExitReadLock();
                }
            }

            SearchResults err2 = new SearchResults();
            err2.error = "could not load document, please try again";
            return err2;
        }

        public string ExportToFile(string format, string exportedFileName, PdfSecurity pdfSecurity)
        {
            this._pdfSecurity = pdfSecurity;
            string result = this.ExportToFile(format, exportedFileName);
            return result;
        }

		public string ExportToFile(string format, string exportedFileName)
        {
            const int c_waitForC1dTimeout = 120 * 1000; // 2 minutes
            const int c_waitForC1dRetry = 600; // msec
            for (int wait = 0; wait < c_waitForC1dTimeout; wait += c_waitForC1dRetry)
            {
                if (_cachedC1dLock.TryEnterReadLock(c_waitForC1dRetry))
                {
                    try
                    {
                        if (_cachedC1d != null)
							return ExportToFileInternal(format, exportedFileName);
                    }
                    finally
                    {
                        _cachedC1dLock.ExitReadLock();
                    }
                }
                Thread.Sleep(c_waitForC1dRetry);
            }
            return ExportToFileError("timeout waiting for document");
        }

        public static string ExportToFileError(string err)
        {
            return string.Format(CultureInfo.InvariantCulture, "error:{0}", err);
        }

		private string ExportToFileInternal(string format, string exportedFileName)
        {
            Debug.Assert(_cachedC1d != null);
            try
            {
                foreach (ExportProvider ep in ExportProviders.RegisteredProviders)
                {
                    if (ep.FormatName == format)
						return ExportToFileInternal(ep, exportedFileName);
                }
            }
            catch (Exception ex)
            {
                return ExportToFileError(ex.Message);
            }
            return ExportToFileError("Unknown export format");
        }

		private string ExportToFileInternal(ExportProvider ep, string exportedFileName)
        {
            Exporter exp = ep.NewExporter();
            if (exp is PdfExporter)
            {
                PdfSecurity security = ((PdfExporter)exp).Security;
                this.PdfSecuritySetting(security);
            }
            object document;
            if (_cachedC1d.IsMultiDoc)
            {
                C1MultiDocument mdoc = new C1MultiDocument("c1v01xJ/Ducakw6/Dl8WTw6vCrcKzJnx5w7c9");
                mdoc.Load(_cachedC1d.FilePath);
                document = mdoc;
            }
            else
            {
                C1PrintDocument c1doc = new C1PrintDocument("c1v01xJ/Ducakw6/Dl8WTw6vCrcKzJnx5w7c9");
                c1doc.Load(_cachedC1d.FilePath, c_c1dFormat);
                document = c1doc;
            }
            // Note: this should not happen:
            if (!ep.CanExportObject(document))
                throw new Exception(string.Format(Strings.CannotExportObjectFmt, ep.FormatName));

            exp.Document = document;
            exp.ShowOptions = false;
			string fn = string.Format("{0}.{1}", 
					string.IsNullOrEmpty(exportedFileName) ? Key : exportedFileName, 
					ep.DefaultExtension);
            string fpath = KeyMaker.FilePath(WorkDir, fn);
            string furl = KeyMaker.FileUrl(WorkUrl, fn);
            exp.Export(fpath);
            return furl;
        }

        private void PdfSecuritySetting(PdfSecurity security)
        {
            if (security == null || this._pdfSecurity == null)
            {
                return;
            }

            security.AllowCopyContent = this._pdfSecurity.AllowCopyContent;
            //TODO: add other security settings
            //security.AllowEditAnnotations = this._pdfSecurity.AllowEditAnnotations;
            //security.AllowEditContent = this._pdfSecurity.AllowEditContent;
            //security.AllowPrint = this._pdfSecurity.AllowPrint;
            //security.Encryption = this._pdfSecurity.Encryption;
            //security.OwnerPassword = this._pdfSecurity.OwnerPassword;
            //security.UserPassword = this._pdfSecurity.UserPassword;
        }

        /// <summary>
        /// Gets a single page image. If the page is unavailable, returns null.
        /// It is the caller's responsibility to retry the call if null is not good.
        /// </summary>
        /// <param name="dpi">Target page image resolution.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="printTarget">If true, image is cropped to page margins.</param>
        /// <returns>Image data or null.</returns>
        public byte[] GetPageImage(int dpi, int pageIndex, bool printTarget)
        {
            double zoom = (double)dpi / (double)Dpi;
            string fn = KeyMaker.PageImageFileName(Key, pageIndex, dpi, c_imageExt);
            fn = KeyMaker.FilePath(WorkDir, fn);

            byte[] bytes = Util.ReadFileData(fn);
            if (bytes == null)
            {
                List<int> pageIndices = new List<int> { pageIndex };
                List<PageImageMarkup> pages = GetPagesFromZip(pageIndices, zoom, dpi, true, false);
                if (pages.Count == 1)
                    bytes = Util.ReadFileData(fn);
            }
            if (bytes == null || bytes.Length == 0)
                return null;

            if (printTarget)
            {
                // for printing, we cut off page margins
                int mLeft, mTop, mRight, mBottom;
                Debug.Assert(pageIndex < PageInfos.Count);
                PageInfo pi = this.PageInfos[pageIndex];
                mLeft = (int)Math.Round(pi.Margins.Left * zoom);
                mTop = (int)Math.Round(pi.Margins.Top * zoom);
                mRight = (int)Math.Round(pi.Margins.Right * zoom);
                mBottom = (int)Math.Round(pi.Margins.Bottom * zoom);

                using (MemoryStream file = new MemoryStream(bytes))
                using (Image img = Image.FromStream(file))
                using (Bitmap bmp = new Bitmap(img))
                using (Bitmap crop = bmp.Clone(new Rectangle(mLeft, mTop, bmp.Width - mLeft - mRight, bmp.Height - mTop - mBottom), img.PixelFormat))
                using (MemoryStream ms = new MemoryStream())
                {
                    crop.Save(ms, c_imageFmt);
                    return ms.GetBuffer();
                }
            }
            else
            {
                return bytes;
            }
        }

		/// <summary>
		/// Get the urls of all the report pages
		/// </summary>
		/// <param name="dpi">Target page image resolution.</param>
		/// <returns>The urls array for the report pages</returns>
		public string[] GetPageImageUrls(int dpi)
		{
			string[] fUrls = new string[CurrentPageCount];
			double zoom = (double)dpi / (double)Dpi;
			for (int pageIndex = 0; pageIndex < this.CurrentPageCount; pageIndex++)
			{
				string fn = KeyMaker.PageImageFileName(Key, pageIndex, dpi, c_imageExt);
				string fnFull = KeyMaker.FilePath(WorkDir, fn);
				string fUrl = string.Empty;

				if (!File.Exists(fnFull))
				{
					List<int> pageIndices = new List<int> { pageIndex };
					List<PageImageMarkup> pages = GetPagesFromZip(pageIndices, zoom, dpi, true, false);
					if (pages.Count == 1)
						fUrl = KeyMaker.FileUrl(WorkUrl, fn);
				}
				else
				{
					fUrl = KeyMaker.FileUrl(WorkUrl, fn);
				}
				fUrls[pageIndex] = fUrl;
			}

			return fUrls;
		}

        #endregion public instance methods

        #region private methods
        /// <summary>
        /// Used to pass parameters to CacheGeneratedDoc which is called to cache C1D/X
        /// when the document was initially already generated.
        /// </summary>
        private class CacheC1dParams
        {
            public C1DocHolder DocHolder { get; private set; }
            public HttpContext Context { get; private set; }

            public CacheC1dParams(C1DocHolder docHolder, HttpContext context)
            {
                DocHolder = docHolder;
                Context = context;
            }
        }

        private void CacheGeneratedDoc(object o)
        {
            CacheC1dParams pars = o as CacheC1dParams;
            CacheC1d(pars.DocHolder, pars.Context);
        }

        private void LogError(string msg)
        {
            string m = string.Format("CachedDocument {0} ERROR: {1}", Key, msg);
            ServiceCache.LogError(m);
        }

        private void LogWarning(string msg)
        {
            string m = string.Format("CachedDocument {0} WARNING: {1}", Key, msg);
            ServiceCache.LogWarning(m);
        }

        private void LogMessage(string msg)
        {
            string m = string.Format("CachedDocument {0} Message: {1}", Key, msg);
            ServiceCache.LogMessage(m);
        }

        private PageImageMarkup GetPageImageMarkup(CachedPageData page, double zoom, int dpi, bool noTextMarkup, bool noImage)
        {
            PageImageMarkup pim = new PageImageMarkup();
            pim.idx = page.PageIndex;
            if (!noImage)
            {
                string fn = KeyMaker.PageImageFileName(Key, page.PageIndex, dpi, c_imageExt);
                string filePath = KeyMaker.FilePath(_workDir, fn);
                string fileUrl = KeyMaker.FileUrl(_workUrl, fn);

                // We try to get an already cached page image. If one could not be found,
                // we try to add it (or get it in between adding tries).
                CachedPageImage cpi = CacheHandler.GetCachedPageImage(filePath);
                if (cpi == null)
                    cpi = TryAddOrGetCachedPageImage(page, filePath, zoom, dpi);
                if (cpi == null)
                {
                    Debug.Assert(false, "Could not add or get existing page image!");
                    return null;
                }
                //pim.imageUrl = fileUrl;
                pim.h = cpi.Size.Height;
                pim.w = cpi.Size.Width;
                Debug.Assert(File.Exists(filePath));
            }
            if (!noTextMarkup)
            {
                pim.texts = GetPageMarkup(page, zoom);
                pim.links = GetPageHyperlinks(page, zoom);
            }
            return pim;
        }

        /// <summary>
        /// Makes the page bitmap and tries to save it as an image (.png) file on disk.
        /// In case of failure a number of retries are attempted, and if after a failure
        /// the CachedPageImage was added to cache by another thread, that one is returned.
        /// NULL is returned in case of fatal error.
        /// </summary>
        private CachedPageImage TryAddOrGetCachedPageImage(CachedPageData page, string filePath, double zoom, int dpi)
        {
            using (Bitmap bmp = GetPageBitmap(page, zoom, dpi))
            {
                for (int msec = 0; msec < Constants.ImageFileSaveTimeoutMsec; msec += Constants.ImageFileSaveRetryMsec)
                {
                    try
                    {
                        using (FileStream fs = File.Open(filePath, FileMode.CreateNew, FileAccess.Write, FileShare.None))
                        {
                            bmp.Save(fs, c_imageFmt);
                            Debug.Assert(File.Exists(filePath));
                            CachedPageImage cpi = new CachedPageImage(filePath, bmp.Size);
                            CacheHandler.Add(cpi, Key, KeyMaker.CachedDocumentPageKey(Key, page.PageIndex, page.Generation));
                            return cpi;
                        }
                    }
                    catch (IOException)
                    {
                        // IOException could be caused by file already being present on disk,
                        // in which case we try to fetch the image from cache, and return that
                        // if found. Otherwise we wait and try again.
                        CachedPageImage cpi = CacheHandler.GetCachedPageImage(filePath);
                        if (cpi != null)
                            return cpi;
                        Thread.Sleep(Constants.ImageFileSaveRetryMsec);
                    }
                    catch (Exception)
                    {
                        // GDI errors could cause this; wait and try again.
                        Thread.Sleep(Constants.ImageFileSaveRetryMsec);
                    }
                }
            }
            // If we got here - something is really wrong.
            Debug.Assert(false, "GetPageImageMarkup: ERROR: failed to get  " + filePath + ".");
            return null;
        }

        private static Bitmap GetPageBitmap(CachedPageData page, double zoom, int dpi)
        {
            Size s = new Size((int)Math.Round(page.Size.Width * zoom), (int)Math.Round(page.Size.Height * zoom));
            Bitmap bmp = new Bitmap(s.Width, s.Height);
            bmp.SetResolution(dpi, dpi);
            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.Clear(Color.Transparent);
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
                using (Metafile mf = GraphicsUtils.ReadMetafileFromMemory(page.MetafileData))
                    g.DrawImage(mf, new Rectangle(Point.Empty, s));
            }
            return bmp;
        }

        private static TextRunMarkup[] GetPageMarkup(CachedPageData page, double zoom)
        {
            List<TextWordsRunInfo> infos = page.TextRunInfos;
            TextRunMarkup[] markups = new TextRunMarkup[infos.Count];
            for (int i = 0; i < infos.Count; ++i)
            {
                TextWordsRunInfo tri = infos[i];
                TextRunMarkup trm = new TextRunMarkup();
                trm.x = (int)Math.Round(tri.Rectangle.Left * zoom);
                trm.y = (int)Math.Round(tri.Rectangle.Top * zoom);
                trm.w = (int)Math.Round(tri.Rectangle.Width * zoom);
                trm.h = (int)Math.Round(tri.Rectangle.Height * zoom);
                trm.text = tri.Text;
                if (tri.Words != null && tri.Words.Count > 0)
                {
                    trm.p = new int[tri.Words.Count];
                    trm.ix = new int[tri.Words.Count];
                    for (int wordIdx = 0; wordIdx < tri.Words.Count; ++wordIdx)
                    {
                        TextWordsRunInfo.WordInfo word = tri.Words[wordIdx];
                        trm.p[wordIdx] = (int)Math.Round(word.Offset * zoom);
                        trm.ix[wordIdx] = word.Position;
                    }
                }
                markups[i] = trm;
            }
            return markups;
        }

        private static HyperlinkEntry[] GetPageHyperlinks(CachedPageData page, double zoom)
        {
            if (page.Hyperlinks == null)
                return null;
            // copy hyperlinks
            HyperlinkEntry[] hes = new HyperlinkEntry[page.Hyperlinks.Count];
            // zoom (convert) areas:
            for (int i = 0; i < page.Hyperlinks.Count; ++i)
            {
                HyperlinkInfo hi = page.Hyperlinks[i];
                HyperlinkEntry he = new HyperlinkEntry();
                he.a = hi.Action;
                he.text = hi.Text;
                he.tr = new TextRunMarkup[hi.Areas.Count];
                for (int j = 0; j < hi.Areas.Count; ++j)
                {
                    RectangleD area = hi.Areas[j];
                    TextRunMarkup trm = new TextRunMarkup();
                    trm.x = (int)Math.Round(area.X * zoom);
                    trm.y = (int)Math.Round(area.Y * zoom);
                    trm.w = (int)Math.Round(area.Width * zoom);
                    trm.h = (int)Math.Round(area.Height * zoom);
                    he.tr[j] = trm;
                }
                hes[i] = he;
            }
            return hes;
        }        
        #endregion

        #region Load C1PrintDocument, C1Report or C1RdlReport (private static methods)
        /// <summary>
        /// Loads document or report from file but DOES NOT generate it.
        /// Upon return, rep or doc have the report/document to be generated.
        /// The return value indicates whether it needs to be generated.
        /// </summary>
        private static bool LoadDocument(object document, DocIdentity di, bool inputParameters,
            out object report, out DocumentType documentType, out C1DocHolder docHolder, out string dependencyFile,
            out Dictionary<string, string> c1reportParamValues,
            ref string errorDesc)
        {
            Util.AdjustNewDocOrReport(document);
            C1PrintDocument doc = document as C1PrintDocument;
            C1.C1Report.C1Report rep = document as C1.C1Report.C1Report;
            C1.C1Rdl.Rdl2008.C1RdlReport rdl = document as C1.C1Rdl.Rdl2008.C1RdlReport;
            C1.C1Preview.C1MultiDocument mdoc = document as C1.C1Preview.C1MultiDocument;
            c1reportParamValues = null;
            dependencyFile = null;

            if (rep == null && doc == null && rdl == null && mdoc == null)
            {
                List<ReportParameterInfo> parInfos;
                rdl = TryLoadRdl(di.FileName, di.Parameters, out parInfos, ref errorDesc);
                // note: this relies on rdl!=null if it was loaded but needs params:
                if (rdl == null)
                    rep = TryLoadReport(di.FileName, di.ReportName, di.Parameters, out parInfos, out c1reportParamValues, ref errorDesc);

                // if parameters are needed - get them from user and try again (this code is ugly):
                if (parInfos != null && parInfos.Count > 0 && inputParameters)
                {
                    di.ParamInfos = parInfos;
                    report = null;
                    documentType = DocumentType.None;
                    docHolder = null;
                    if (rdl != null)
                        rdl.Dispose();
                    if (rep != null)
                        rep.Dispose();
                    return false;
                }

                if (rdl == null && rep == null)
                    doc = TryLoadC1Doc(di.FileName, ref errorDesc);

                if (rdl == null && rep == null && doc == null)
                    mdoc = TryLoadC1MultiDoc(di.FileName, ref errorDesc);

                if (rdl != null || rep != null || doc != null || mdoc != null)
                    dependencyFile = di.FileName;

                // todo: provide optional ability to import C1Report into c1printdoc.
            }

            if (rdl != null)
            {
                report = rdl;
                documentType = DocumentType.C1RdlReport;
                docHolder = C1DocHolder.FromC1PrintDocument(rdl.C1DocumentInternal);
                return rdl.IsEmpty;
            }
            else if (rep != null)
            {
                report = rep;
                documentType = DocumentType.C1Report;
                docHolder = C1DocHolder.FromC1PrintDocument(rep.C1DocumentInternal);
                return rep.C1DocumentInternal.Pages.Count == 0;
            }
            else if (doc != null)
            {
                report = doc;
                documentType = DocumentType.C1PrintDocument;
                docHolder = C1DocHolder.FromC1PrintDocument(doc);
                return doc.Pages.Count == 0;
            }
            else if (mdoc != null)
            {
                report = mdoc;
                documentType = DocumentType.C1MultiDocument;
                docHolder = C1DocHolder.FromC1MultiDocument(mdoc);
                return mdoc.PageCount == 0;
            }
            else
            {
                report = null;
                documentType = DocumentType.None;
                docHolder = null;
                return false;
            }
        }

        private static C1.C1Report.C1Report TryLoadReport(string fileName, string reportName, ReportParameterValue[] parameters,
            out List<ReportParameterInfo> parInfos, out Dictionary<string, string> paramValues, ref string errorDesc)
        {
            parInfos = null;
            paramValues = null;
            try
            {
                C1.C1Report.C1Report rep = new C1.C1Report.C1Report("c1v01xKvEusWVxarCgsOid8KTdcOow7VwdGA=");
                rep.Load(fileName, reportName);
                bool paramsSetOk = false;
                if (parameters != null && parameters.Length > 0)
                    paramsSetOk = SetReportParameters(rep, parameters, out paramValues);
                if (!paramsSetOk)
                    parInfos = GetReportParametersInfo(rep);

                Util.AdjustNewDocOrReport(rep);
                return rep;
            }
            catch (Exception ex)
            {
                errorDesc = ex.Message;
                return null;
            }
        }

        private static C1PrintDocument TryLoadC1Doc(string fileName, ref string errorDesc)
        {
            try
            {
                C1PrintDocument c1doc = new C1PrintDocument("c1v01xJ/Ducakw6/Dl8WTw6vCrcKzJnx5w7c9");
                c1doc.Load(fileName);
                Util.AdjustNewDocOrReport(c1doc);
                return c1doc;
            }
            catch (Exception ex)
            {
                errorDesc = ex.Message;
                return null;
            }
        }

        private static C1MultiDocument TryLoadC1MultiDoc(string fileName, ref string errorDesc)
        {
            try
            {
                C1MultiDocument c1mdoc = new C1MultiDocument("c1v01xJ/Ducakw6/Dl8WTw6vCrcKzJnx5w7c9");
                c1mdoc.Load(fileName);
                Util.AdjustNewDocOrReport(c1mdoc);
                return c1mdoc;
            }
            catch (Exception ex)
            {
                errorDesc = ex.Message;
                return null;
            }
        }

        private static C1.C1Rdl.Rdl2008.C1RdlReport TryLoadRdl(string fileName, ReportParameterValue[] parameters,
            out List<ReportParameterInfo> parInfos, ref string errorDesc)
        {
            parInfos = null;
            try
            {
#if GENERATE_RUNTIME_KEY // here is the way to generate the magic string:
                var pl = new C1.Util.Licensing.ProductLicense();
                pl.ProductCode = "28";
                pl.ProductGuid = new Guid("C6CAB06A-DE96-4668-AFD0-C666B4379129");
                var key = pl.RuntimeKey;
#else
                var key = "c1v01xKTGk8aPxI1gxI1Ww5DCkiPCjzbErsOG";
#endif
                C1.C1Rdl.Rdl2008.C1RdlReport rdl = new C1.C1Rdl.Rdl2008.C1RdlReport(key);
                rdl.Load(fileName);
                // handle parameters:
                bool paramsSetOk = false;
                if (parameters != null && parameters.Length > 0)
                {
                    paramsSetOk = SetReportParameters(rdl, parameters);
                    if (paramsSetOk)
                        rdl.ShowParametersInputDialog = false;
                }
                if (!paramsSetOk)
                {
                    if (rdl.ShowParametersInputDialog)
                        parInfos = GetReportParametersInfo(rdl);
                }
                Util.AdjustNewDocOrReport(rdl);
                return rdl;
            }
            catch (Exception ex)
            {
                errorDesc = ex.Message;
                return null;
            }
        }

        static private List<ReportParameterInfo> GetReportParametersInfo(C1.C1Rdl.Rdl2008.C1RdlReport rdl)
        {
            List<ReportParameterInfo> parInfos = new List<ReportParameterInfo>();
            foreach (C1.C1Rdl.Rdl2008.ReportParameter par in rdl.ReportParameters)
            {
                if (par.Hidden)
                    continue;
                ReportParameterInfo parInfo = new ReportParameterInfo();
                parInfo.n = par.Name;
                parInfo.nu = par.Nullable;
                parInfo.mv = par.MultiValue;
                parInfo.p = par.Prompt;
                parInfo.rq = par.DataType == C1Rdl.DataType.String ? par.AllowBlank : true;
                switch (par.DataType)
                {
                    case C1Rdl.DataType.Boolean:
                        parInfo.t = "bool";
                        break;
                    case C1Rdl.DataType.DateTime:
                        parInfo.t = "DateTime";
                        break;
                    case C1Rdl.DataType.Float:
                        parInfo.t = "float";
                        break;
                    case C1Rdl.DataType.Integer:
                        parInfo.t = "int";
                        break;
                    case C1Rdl.DataType.String:
                        parInfo.t = "string";
                        break;
                    default:
                        Debug.Assert(false);
                        parInfo.t = "string";
                        break;
                }
                if (par.ValidValues.Defined)
                {
                    parInfo.pvs = new ReportParameterSingleValue[par.ValidValues.ParameterValues.Count];
                    for (int i = 0; i < par.ValidValues.ParameterValues.Count; ++i)
                    {
                        ReportParameterSingleValue rpsv = new ReportParameterSingleValue();
                        rpsv.l = par.ValidValues.ParameterValues[i].Label;
                        // todo: IsExpression here is a patch... not supported now:
                        if (par.ValidValues.ParameterValues[i].Value.IsNull || par.ValidValues.ParameterValues[i].Value.IsExpression)
                            rpsv.v = null;
                        else
                            rpsv.v = par.ValidValues.ParameterValues[i].Value.Value;
                        parInfo.pvs[i] = rpsv;
                    }
                }
                else
                    parInfo.pvs = null;
                if (par.DefaultValue.Values.Count > 0)
                {
                    parInfo.vs = new object[par.DefaultValue.Values.Count];
                    for (int i = 0; i < par.DefaultValue.Values.Count; ++i)
                        if (par.DefaultValue.Values[i].V.IsExpression)
                            parInfo.vs[0] = null; // todo: not sure what to do in this case.
                        else
                            parInfo.vs[0] = par.DefaultValue.Values[i].V.Value;
                }
                else
                    parInfo.vs = null;
                parInfo.vx = parInfo.pvs != null && parInfo.pvs.Length > 0;
                //
                parInfos.Add(parInfo);
            }
            if (parInfos.Count == 0)
                return null;
            return parInfos;
        }

        static private bool SetReportParameters(C1.C1Rdl.Rdl2008.C1RdlReport rdl, ReportParameterValue[] parameters)
        {
            Debug.Assert(parameters != null && parameters.Length > 0);

            if (rdl.ReportParameters == null || parameters.Length != rdl.ReportParameters.Count)
                return false;

            for (int i = 0; i < parameters.Length; ++i)
            {
                foreach (C1.C1Rdl.Rdl2008.ReportParameter par in rdl.ReportParameters)
                {
                    if (par.Name == parameters[i].n)
                    {
                        par.DefaultValue.Values.Clear();
                        for (int j = 0; j < parameters[i].vs.Length; ++j)
                            par.DefaultValue.Values.Add(new C1Rdl.RdlObject(parameters[i].vs[j]));
                        break;
                    }
                }
            }
            return true;
        }

        static private List<ReportParameterInfo> GetReportParametersInfo(C1.C1Report.C1Report rep)
        {
            List<ReportParameterInfo> parInfos = new List<ReportParameterInfo>();

            List<C1.C1Report.ReportParameter> pars = rep.GetParameters(false);
            pars.AddRange(rep.GetParameters(true));

            if (pars.Count == 0)
                return null;

            foreach (C1.C1Report.ReportParameter par in pars)
            {
                ReportParameterInfo parInfo = new ReportParameterInfo();
                parInfo.n = par.Name;
                parInfo.nu = false;
                parInfo.mv = false;
                parInfo.p = par.Name;
                parInfo.rq = false;
                if (par.Type == typeof(DateTime))
                    parInfo.t = "DateTime";
                else if (par.Type == typeof(TimeSpan))
                    parInfo.t = "TimeSpan";
                else if (par.Type == typeof(int))
                    parInfo.t = "int";
                else if (par.Type == typeof(float))
                    parInfo.t = "float";
                else if (par.Type == typeof(double))
                    parInfo.t = "double";
                else if (par.Type == typeof(bool))
                    parInfo.t = "bool";
                // else if (par.Type == typeof(IList) ???
                else
                    parInfo.t = "string";
                if (par.Options != null && par.Options.Length > 0)
                {
                    parInfo.pvs = new ReportParameterSingleValue[par.Options.Length];
                    for (int i = 0; i < par.Options.Length; ++i)
                    {
                        ReportParameterSingleValue rpsv = new ReportParameterSingleValue();
                        rpsv.v = rpsv.l = par.Options[i];
                        parInfo.pvs[i] = rpsv;
                    }
                }
                else
                    parInfo.pvs = null;
                parInfo.vs = new object[1] { par.Value };
                parInfo.vx = parInfo.pvs != null && parInfo.pvs.Length > 0;
                //
                parInfos.Add(parInfo);
            }
            return parInfos;
        }

        static private bool SetReportParameters(C1.C1Report.C1Report rep, ReportParameterValue[] parameters, out Dictionary<string, string> paramValues)
        {
            Debug.Assert(parameters != null && parameters.Length > 0);

            paramValues = null;

            List<C1.C1Report.ReportParameter> pars = rep.GetParameters(false);
            List<C1.C1Report.ReportParameter> parsFilter = rep.GetParameters(true);
            if (pars.Count + parsFilter.Count != parameters.Length)
                return false; // wrong parameters

            paramValues = new Dictionary<string, string>();
            for (int i = 0; i < parameters.Length; ++i)
            {
                ReportParameterValue rpv = parameters[i];
                bool found = false;
                for (int j = 0; j < pars.Count; ++j)
                {
                    if (rpv.n == pars[j].Name)
                    {
                        pars[j].Value = rpv.vs != null && rpv.vs.Length > 0 ? rpv.vs[0] : null;
                        paramValues.Add(pars[j].Name, pars[j].StringValue);
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    for (int j = 0; j < parsFilter.Count; ++j)
                    {
                        if (rpv.n == parsFilter[j].Name)
                        {
                            parsFilter[j].Value = rpv.vs != null && rpv.vs.Length > 0 ? rpv.vs[0] : null;
                            paramValues.Add(parsFilter[j].Name, parsFilter[j].StringValue);
                            found = true;
                            break;
                        }
                    }
                }
                if (!found)
                    return false;
            }
            rep.SetParameters(pars, false);
            rep.SetParameters(parsFilter, true);
            return true;
        }
        #endregion Load C1PrintDocument or C1Report (static methods)

        #region private instance methods
        private int[] MakeChangedPages(ref string cookie)
        {
            // Use this to avoid caching empty lists:
            const string emptyRunsCookie = "noRuns";

            List<PageGenRun> lastRuns;
            if (string.IsNullOrEmpty(cookie) || cookie == emptyRunsCookie)
                lastRuns = new List<PageGenRun>();
            else
                lastRuns = CacheHandler.GetPageGenRuns(cookie);

            List<int> lastPageGens = PageGenRun.ExpandRuns(lastRuns);
            List<PageGenRun> newRuns = PageGenRun.BuildRuns(this);
            List<int> newPageGens = PageGenRun.ExpandRuns(newRuns);
            List<int> changedPages = new List<int>();
            int n = Math.Min(lastPageGens.Count, newPageGens.Count);
            for (int i = 0; i < n; ++i)
                if (lastPageGens[i] < newPageGens[i])
                    changedPages.Add(i);
            if (newRuns.Count == 0)
            {
                cookie = emptyRunsCookie;
            }
            else
            {
                cookie = Guid.NewGuid().ToString("N");
                CacheHandler.Add(cookie, Key, newRuns);
            }
            return changedPages.ToArray();
        }

        private PageRangeSizeInfo[] MakePageSizeRanges()
        {
            List<PageRangeSizeInfo> infos = new List<PageRangeSizeInfo>();
            int pageCount = this.CurrentPageCount;

            for (int i = 0; i < pageCount; ++i)
            {
                PageRangeSizeInfo lastInfo = infos.Count == 0 ? null : infos[infos.Count - 1];
                int w = (int)Math.Round(this.PageInfos[i].Size.Width);
                int h = (int)Math.Round(this.PageInfos[i].Size.Height);
                if (infos.Count == 0 || infos[infos.Count - 1].w != w || infos[infos.Count - 1].h != h)
                {
                    // finish last range:
                    if (infos.Count != 0)
                        infos[infos.Count - 1].e = i - 1;
                    PageRangeSizeInfo info = new PageRangeSizeInfo();
                    info.s = i;
                    info.w = (int)Math.Round(this.PageInfos[i].Size.Width);
                    info.h = (int)Math.Round(this.PageInfos[i].Size.Height);
                    infos.Add(info);
                }
            }
            if (pageCount > 0)
                infos[infos.Count - 1].e = pageCount;
            return infos.ToArray();
        }

        private string[] MakeExportFormatsList()
        {
            List<string> formats = new List<string>();
            foreach (ExportProvider ep in c_exportFormats)
            {
                if (ep.CanExportType(c_documentTypes[_documentType]))
                {
                    formats.Add(ep.FormatName);
                    formats.Add(ep.DefaultExtension);
                }
            }
            return formats.ToArray();
        }

        private void InsertPagesToZip(C1DocHolder docHolder)
        {
            Thread th = new Thread(ThreadStarter.Start);
            CachedDocumentInfo tpar = new CachedDocumentInfo(this, docHolder, HttpContext.Current);
            th.Start(new ThreadStarter.Params(HttpContext.Current, AddCachedPagesToZip, tpar, this.Key));
        }

        /// <summary>
        /// Thread top method.
        /// 
        /// Adds pages to a zip in a separate thread. After each page is added,
        /// the file becomes available for reading by other threads.
        /// This method is used when the viewer starts with a generated document,
        /// and all pages are already available.
        /// </summary>
        /// <param name="p"></param>
        private static void AddCachedPagesToZip(object p)
        {
            CachedDocumentInfo param = p as CachedDocumentInfo;
            CachedDocument cdoc = param.CachedDoc;
            cdoc.AddCachedPagesToZip(param);
        }

        /// <summary>
        /// Adds pages to a zip in a separate thread. After each page is added,
        /// the file becomes available for reading by other threads.
        /// This method is used when the viewer starts with a generated document,
        /// and all pages are already available.
        /// </summary>
        /// <param name="param">The param.</param>
        private void AddCachedPagesToZip(CachedDocumentInfo param)
        {
            Debug.Assert(param.DocHolder != null);

            // This method can only work for generated documents:
            System.Diagnostics.Debug.Assert(!param.DocHolder.IsGenerating);

            int pageCount = param.DocHolder.PageCount;
            Debug.Assert(pageCount == CurrentPageCount);

            // while overall we add pages sequentially, we try to accommodate specific page requests
            // (could happen if the client clicked "last page" immediately after loading the document).
            // This array allows us to keep track of which pages were already added so as not to waste
            // time trying to add them again, true means that the page is already in the cache:
            bool[] addedPages = new bool[pageCount];

            // loop pageCount at most:
            for (int i = 0; i < pageCount; ++i)
            {
                // while specific page requests are present, accommodate those:
                int[] requestedPages = PageRequestQueue.GetQueuedPages(param.Context, this.Key);
                foreach (int pageIndex in requestedPages)
                {
                    // We must make sure requested index is valid:
                    if (pageIndex >= 0 && pageIndex < pageCount)
                    {
                        if (PageRequestQueue.AddingCachedPage(param.Context, this.Key, pageIndex, 0))
                        {
                            CachedPageData cpage = CachedPageData.Make(this, param.DocHolder, pageIndex, param.Context);
                            RetryAddCachedPageToZip(cpage, param.Context);
                            addedPages[pageIndex] = true;
                        }
                    }
                }
                // go in order if there are no specific requests:
                if (!addedPages[i])
                {
                    // We must make sure requested index is valid:
                    if (i >= 0 && i < pageCount)
                    {
                        if (PageRequestQueue.AddingCachedPage(param.Context, this.Key, i, 0))
                        {
                            CachedPageData cpage = CachedPageData.Make(this, param.DocHolder, i, param.Context);
                            RetryAddCachedPageToZip(cpage, param.Context);
                            addedPages[i] = true;
                        }
                    }
                }
            }
        }

        private bool AddCachedPageToZip(CachedPageData cpage, HttpContext context)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(ms, cpage);
                ms.Position = 0;
                if (_zipLock.TryEnterWriteLock(Constants.ZipLockTimeoutMsec))
                {
                    try
                    {
                        using (FileStream fs = new FileStream(PageZipPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None))
                        using (C1ZipFile zf = new C1ZipFile(fs, fs.Length == 0))
                        {
                            string pageEntry = KeyMaker.PageZipEntry(cpage.PageIndex);
                            if (zf.Entries.Contains(pageEntry))
                            {
                                zf.Entries.Remove(pageEntry);
                                // remove keys for stale pages. This will ensure that stale page image files are removed.
                                for (int gen = 0; gen < cpage.Generation; ++gen)
                                    CacheHandler.Remove(KeyMaker.CachedDocumentPageKey(Key, cpage.PageIndex, gen), context);
                            }
                            zf.Entries.Add(ms, pageEntry);
                            zf.Close(); // do I need to do this???
                        }
                        // we're done!
                        return true;
                    }
                    catch (Exception ex)
                    {
                        LogError(string.Format("AddCachedPageToZip failed: {0}", ex.Message));
                        return false;
                    }
                    finally
                    {
                        _zipLock.ExitWriteLock();
                    }
                }
                LogError(string.Format("AddCachedPageToZip failed to lock zip: {0}", PageZipPath));
                return false;
            }
        }

        /// <summary>
        /// Gets a set of pages from zip file in a thread-safe way.
        /// Note that the resulting list may contain less pages than what was requested via pageIndices.
        /// </summary>
        public List<PageImageMarkup> GetPagesFromZip(List<int> indices, double zoom, int dpi, bool noTextMarkup, bool noImage)
        {
            bool noZipYet = false;
            BinaryFormatter formatter = new BinaryFormatter();
            List<PageImageMarkup> markups = new List<PageImageMarkup>();
            if (_zipLock.TryEnterReadLock(Constants.ZipLockTimeoutMsec))
            {
                try
                {
                    using (FileStream fs = new FileStream(PageZipPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    using (C1ZipFile zf = new C1ZipFile(fs))
                    {
                        CachedPageData page = null;
                        PageImageMarkup pim = null;
                        while (indices.Count > 0)
                        {
                            int pageIndex = indices[0];
                            string entryName = KeyMaker.PageZipEntry(pageIndex);
                            if (zf.Entries.Contains(entryName))
                            {
                                using (Stream s = zf.OpenReader(zf.Entries[KeyMaker.PageZipEntry(pageIndex)]))
                                    page = formatter.Deserialize(s) as CachedPageData;
                                if (page != null)
                                    pim = GetPageImageMarkup(page, zoom, dpi, noTextMarkup, noImage);
                            }
                            if (pim != null)
                                markups.Add(pim);
                            else
                            {
                                Debug.Assert(HttpContext.Current != null);
                                PageRequestQueue.Push(HttpContext.Current, Key, pageIndex);
                            }
                            indices.RemoveAt(0);
                        }
                    }
                }
                catch (FileNotFoundException)
                {
                    noZipYet = true;
                }
                catch (Exception ex)
                {
                    // we should not be getting here...
                    LogError(string.Format("GetPagesFromZip: unexpected exception: {0}", ex.Message));
                    noZipYet = true;
                }
                finally
                {
                    _zipLock.ExitReadLock();
                }
                if (noZipYet)
                {
                    // if page zip has not been created yet - request pages and return
                    // (do it after releasing the lock so as to minimize writer wait time):
                    foreach (int pageIndex in indices)
                        PageRequestQueue.Push(HttpContext.Current, Key, pageIndex);
                }
                return markups;
            }
            Debug.Assert(false, "GetPagesFromZip FAILED - could not lock zip!");
            return null;
        }
        #endregion private instance methods

        #region unused
#if todo_or_remove // Dima M made thumbnails without this so maybe just remove this code?
        private void SavePageThumbnail(string documentKey, CachedDocument.Page page, Metafile pageMetafile)
        {
            const int maxLen = 120;
            Size s;
            if (page.Size.Height > page.Size.Width)
                s = new Size((int)Math.Round((page.Size.Width * maxLen) / page.Size.Height), maxLen);
            else
                s = new Size(maxLen, (int)Math.Round((page.Size.Height * maxLen) / page.Size.Width));
            using (Bitmap bmp = new Bitmap(s.Width, s.Height))
            {
                bmp.SetResolution(96, 96);
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    g.Clear(Color.White);
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighSpeed;
                    g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixel;
                    g.DrawImage(pageMetafile, new Rectangle(Point.Empty, s));
                }
                string fn = string.Format(CultureInfo.InvariantCulture, KeyMaker.PageThumbnailFileNameFormat(documentKey, c_thumbExt), page.PageIndex);
                fn = KeyMaker.FilePath(_workDir, fn);
                try
                {
                    bmp.Save(fn, c_thumbFmt);
                }
                catch
                {
                    Debug.WriteLine("ERROR saving thumbnail: " + fn);
                }
            }
        }
#endif
        #endregion unused
    }
}

