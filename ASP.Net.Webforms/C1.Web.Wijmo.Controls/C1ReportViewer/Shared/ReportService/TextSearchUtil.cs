﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Specialized;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

#if WIJMOCONTROLS
namespace C1.Web.Wijmo.Controls.C1ReportViewer.ReportService
#else
namespace C1.Web.UI.Controls.C1Report.ReportService
#endif
{
    // NOTE/todo: this code was adapted from Reports.Net\Main\Win.C1Preview\TextSearchUtil.cs,
    // must merge the two into common code!
    internal class TextSearchUtil
    {
        /// <summary>
        /// Finds instances of text in a string.
        /// </summary>
        /// <param name="what">The text to search for.</param>
        /// <param name="where">The string to search.</param>
        /// <param name="fromIdx">Index in "where" to start search at.</param>
        /// <param name="length">Length of substring to search in.</param>
        /// <param name="maxCount">Max number of instances to find.</param>
        /// <param name="forward">Search direction.</param>
        /// <param name="matchCase">Ignore case.</param>
        /// <param name="word">Whole word search.</param>
        /// <returns>Array of indexes into "where" to the found instances.</returns>
        static public List<int> FindTextInstances(string what, string where,
            int fromIdx, int length, int maxCount, bool forward, bool matchCase, bool word)
        {
            List<int> finds = new List<int>();
            StringComparison scomp = matchCase ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase;

            if (length == int.MaxValue)
                length = where.Length;

            if (forward)
            {
                int upto = Math.Min(where.Length, fromIdx + length);
                int currIdx = fromIdx;
                for (int i = 0; i < maxCount; ++i)
                {
                    int findIdx = where.IndexOf(what, currIdx, upto - currIdx, scomp);
                    if (findIdx < 0)
                        break;
                    if (!word || IsWholeWord(what, where, findIdx))
                        finds.Add(findIdx);
                    currIdx = findIdx + what.Length;
                }
            }
            else
            {
                int upto = Math.Max(-1, fromIdx - length);
                int currIdx = fromIdx;
                for (int i = 0; i < maxCount && currIdx >= 0; ++i)
                {
                    int findIdx = where.LastIndexOf(what, currIdx, currIdx - upto, scomp);
                    if (findIdx < 0)
                        break;
                    if (!word || IsWholeWord(what, where, findIdx))
                        finds.Add(findIdx);
                    currIdx = findIdx - 1;
                }
            }
            return finds;
        }

        static public bool IsWholeWord(string word, string text, int idx)
        {
            if (idx > 0 && char.IsLetterOrDigit(text, idx - 1))
                return false;
            if (idx + word.Length < text.Length && char.IsLetterOrDigit(text, idx + word.Length))
                return false;
            return true;
        }
    }
}
