﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using C1.Web.Wijmo.Controls.Base;

namespace C1.Web.Wijmo.Controls.C1ReportViewer
{

    /// <summary>
    /// C1Report serializer.
    /// </summary>
    internal class C1ReportViewerSerializer : C1BaseSerializer<C1ReportViewer, object, object>
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="serializableObject">Serializable Object.</param>
        public C1ReportViewerSerializer(object serializableObject)
            : base(serializableObject)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="componentChangeService"></param>
        /// <param name="serializableObject"></param>
		public C1ReportViewerSerializer(IComponentChangeService componentChangeService, object serializableObject)
            : base(componentChangeService, serializableObject)
        {
        }
    }
}
