﻿using System;

namespace C1.Web.Wijmo.Controls.C1ReportViewer
{

    /// <summary>
    /// Report Tools.
    /// </summary>
    public enum ReportTools
    {
        /// <summary>
        /// Outline tool.
        /// </summary>
        Outline = 0,
        /// <summary>
        /// Search tool.
        /// </summary>
        Search = 1,
        /// <summary>
        /// Thumbs view tool.
        /// </summary>
        Thumbs = 2
    }

    /// <summary>
    /// Available report tools.
    /// </summary>
	[Flags]
    public enum AvailableReportTools
    {
        /// <summary>
        /// No tools are available to user.
        /// </summary>
        None = 0,
        /// <summary>
        /// Outline pane.
        /// </summary>
        Outline = 1,
        /// <summary>
        /// Search pane.
        /// </summary>
        Search = 2,
        /// <summary>
        /// Thumbs pane.
        /// </summary>
        Thumbs = 4,
        /// <summary>
        /// All tools are available to user.
        /// </summary>
        All = 7//3//7
    }

    /// <summary>
    /// Connection Status.
    /// </summary>
    public enum ConnectionStatus
    {
        /// <summary>
        /// No active connections and no errors detected.
        /// </summary>
        Idle = 0,
        /// <summary>
        /// Normal connection. Server response time is 0-7 sec.
        /// </summary>
        Normal = 1,
        /// <summary>
        /// Low connection. Server response time is 8-30 sec.
        /// </summary>
        Low = 2,
        /// <summary>
        /// Connection warning. Server response time is 31-60 sec.
        /// </summary>
        Warning = 3,
        /// <summary>
        /// Server not responded during 61 sec.
        /// </summary>
        Fail = 4
    }

}
