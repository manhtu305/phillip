﻿
namespace C1.Web.Wijmo.Controls.C1ReportViewer
{
    /// <summary>
    /// Pdf Security setting.
    /// </summary>
    public class PdfSecuritySetting
    {

        private bool _allowCopyContent = true;

        /// <summary>
        /// Allow the pdf can be copied.
        /// </summary>
        public bool AllowCopyContent
        {
            get
            {
                return this._allowCopyContent;
            }
            set
            {
                this._allowCopyContent = value;
            }
        }
    }
}
